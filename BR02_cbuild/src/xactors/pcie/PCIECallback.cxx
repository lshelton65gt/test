// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "PCIECallback.h"
#include "CarbonXPcieTransactor.h"
#include "CarbonXPcieCommonTypes.h"
#include "CarbonXPcieTLP.h"
#include "shell/carbon_interface.h"

PCIECallback::PCIECallback(CarbonXPcieTransactor *inXtor, const CarbonXList<CarbonXPcieSym>& pkt) :
   xtor(inXtor),
   cfg(&(inXtor->getCfg())),
   _length(pkt.size()),
   mStartTime(pkt.front().mTime),
   mEndTime(pkt.back().mTime)
{
   // get a local copy of the data
   for(CarbonXList<CarbonXPcieSym>::const_iterator iter = pkt.begin(); iter != pkt.end(); ++iter) {
      mBytes.push_back(iter->mData);
   }
   
   init();
}

void PCIECallback::init()
{
    printStr = cfg->getPrintName();
    printStr += "  ";
    
    if(cfg->getVerboseRxLow()) {
       if(_length < 4) {   // need at least the header!
          UtOStream* ostr = carbonInterfaceGetCout();
          carbonInterfaceWriteStringToOStream(ostr, printStr.c_str());
          carbonInterfaceWriteStringToOStream(ostr, "ERROR: transaction created with too few bytes.");
          carbonInterfaceOStreamEndl(ostr);
       }
    }

    if(cfg->getVerboseRxHigh()) {
       UtOStream* ostr = carbonInterfaceGetCout();
       carbonInterfaceWriteStringToOStream(ostr, printStr.c_str());
       carbonInterfaceWriteStringToOStream(ostr, "Info: Received Transaction ( ");
       for(UInt32 i=0; i<_length; i++) {
          carbonInterfaceSetOStreamRadixHex(ostr);
          carbonInterfaceSetOStreamWidth(ostr, 2);
          carbonInterfaceSetOStreamFill(ostr, '0');
          carbonInterfaceWriteUInt32ToOStream(ostr, static_cast<unsigned>(mBytes[i]));
          carbonInterfaceWriteStringToOStream(ostr, " ");
       }
       carbonInterfaceWriteStringToOStream(ostr, ")");
       carbonInterfaceOStreamEndl(ostr);
    }

    processHeader();

#if 0
    UtIO::cout() << " -- Got me some data for a " << UtIO::hex << tType << UtIO::endl
	      << "    inLength = " << UtIO::dec << inLength << UtIO::endl;
#endif

   switch(tType)
   {
   case eCarbonXPcieMRd32:
   case eCarbonXPcieMRd64:
   case eCarbonXPcieMRdLk32:
   case eCarbonXPcieMRdLk64:
      processMemRead(); break;
   case eCarbonXPcieMWr32:
   case eCarbonXPcieMWr64:
      processMemWrite(); break;
   case eCarbonXPcieIORd:
      processIORead(); break;
   case eCarbonXPcieIOWr:
      processIOWrite(); break;
   case eCarbonXPcieCfgRd0:
   case eCarbonXPcieCfgRd1:
      processConfigRead(); break;
   case eCarbonXPcieCfgWr0:
   case eCarbonXPcieCfgWr1:
      processConfigWrite(); break;
   case eCarbonXPcieMsg:
   case eCarbonXPcieMsgD:
      processMessage(); break;
   case eCarbonXPcieCpl:
   case eCarbonXPcieCplD:
   case eCarbonXPcieCplLk:
   case eCarbonXPcieCplDLk:
      processCompletion(); break;
   default:
      break; // done, dont' know what to do with this, should have printed an error message already
   }
}
   

void PCIECallback::processHeader(void)
{
    // get feild information from first four bytes
    hdrFmt    = (mBytes[0] >> 5) & 0x03;               // byte0[6:5]
    hdrType   = mBytes[0] & 0x1F;                      // byte0[4:0]
    hdrTC     = (mBytes[1] >> 4) & 0x07;               // byte1[6:4]
    hdrTD     = (mBytes[2] & 0x80) && 0x1;             // byte2[7]
    hdrEP     = (mBytes[2] & 0x40) && 0x1;             // byte2[6]
    hdrAttr   = (mBytes[2] >> 4) & 0x03;               // byte2[5:4]
    hdrLength = ( ((mBytes[2] & 0x3)) << 8) | mBytes[3];   // {byte2[1:0],byte3[7:0]}
    
    if(hdrEP)  // packet poisoned
	return;

    // tType = getTLPType((hdrFmt << 5) + hdrType);
    tType = getTLPType(mBytes[0] & 0x7F);

    if(cfg->getVerboseRxHigh()) {
       UtOStream* ostr = carbonInterfaceGetCout();
       carbonInterfaceWriteStringToOStream(ostr, "     tType=");
       carbonInterfaceSetOStreamRadixHex(ostr);
       carbonInterfaceWriteUInt32ToOStream(ostr, tType);
       carbonInterfaceWriteStringToOStream(ostr, ", TC=");
       carbonInterfaceWriteUInt32ToOStream(ostr, hdrTC);
       carbonInterfaceWriteStringToOStream(ostr, ", TD=");
       carbonInterfaceWriteCharToOStream(ostr, hdrTD);
       carbonInterfaceWriteStringToOStream(ostr, ", EP=");
       carbonInterfaceWriteCharToOStream(ostr, hdrEP);
       carbonInterfaceWriteStringToOStream(ostr, ", Attr=");
       carbonInterfaceWriteUInt32ToOStream(ostr, hdrAttr);
       carbonInterfaceWriteStringToOStream(ostr, ", length=");
       carbonInterfaceWriteUInt32ToOStream(ostr, hdrLength);
       carbonInterfaceWriteStringToOStream(ostr, "(");
       carbonInterfaceSetOStreamRadixDec(ostr);
       carbonInterfaceWriteUInt32ToOStream(ostr, hdrLength);
       carbonInterfaceWriteStringToOStream(ostr, ")");
       carbonInterfaceOStreamEndl(ostr);
    }

    return;
}

CarbonXPcieCmdType PCIECallback::getTLPType(UInt32 x)
{
    CarbonXPcieCmdType y;
    switch(x)
	{
	case 0x00:
	    y= eCarbonXPcieMRd32; break;
	case 0x20:
	    y= eCarbonXPcieMRd64; break;
	case 0x01:
	    y= eCarbonXPcieMRdLk32; break;
	case 0x21:
	    y= eCarbonXPcieMRdLk64; break;
	case 0x40:
	    y= eCarbonXPcieMWr32; break;
	case 0x60:
	    y= eCarbonXPcieMWr64; break;
	case 0x02:
	    y= eCarbonXPcieIORd; break;
	case 0x42:
	    y= eCarbonXPcieIOWr; break;
	case 0x04:
	    y= eCarbonXPcieCfgRd0; break;
	case 0x44:
	    y= eCarbonXPcieCfgWr0; break;
	case 0x05:
	    y= eCarbonXPcieCfgRd1; break;
	case 0x45:
	    y= eCarbonXPcieCfgWr1; break;
	case 0x30: case 0x31: case 0x32: case 0x33: case 0x34: case 0x35: case 0x36: case 0x37:
	    y= eCarbonXPcieMsg; break;
	case 0x70: case 0x71: case 0x72: case 0x73: case 0x74: case 0x75: case 0x76: case 0x77:
	    y= eCarbonXPcieMsgD; break;
	case 0x0A:
	    y= eCarbonXPcieCpl; break;
	case 0x4A:
	    y= eCarbonXPcieCplD; break;
	case 0x0B:
	    y= eCarbonXPcieCplLk; break;
	case 0x4B:
	    y= eCarbonXPcieCplDLk; break;
	default:
           if(cfg->getVerboseRxLow()) {
              UtOStream* ostr = carbonInterfaceGetCout();
              carbonInterfaceWriteStringToOStream(ostr, printStr.c_str());
              carbonInterfaceWriteStringToOStream(ostr, "ERROR: Bad TLP Type given.");
              carbonInterfaceOStreamEndl(ostr);
           }
	    y= eCarbonXPcieMsg;  // don't know what else to do with it...
	    break;
	}
    return y;
}

void PCIECallback::processMemRead(void) {
    bool is32bit;
    UInt32 reqID;   // requester ID
    UInt32 tag;     // tag
    UInt32 fdwbe;   // first DW byte enable
    UInt32 ldwbe;   // last DW byte enable
    UInt32 addrHi;
    UInt32 addrLo;

#if 0
    UtIO::cout() << "MEMRD size: " << UtIO::dec <<  (mBytes[3] | ((mBytes[2] & 0x3) << 8)) 
	      << " " << hdrLength << UtIO::endl;
#endif

    if(hdrFmt == 0x00)
	is32bit = true;
    else
	is32bit = false;

    reqID = (mBytes[4] << 8) + mBytes[5];
    tag   = mBytes[6];
    ldwbe = (mBytes[7] >> 4) & 0x0F;
    fdwbe = mBytes[7] & 0x0F;
    if(is32bit) {
	addrHi = 0x0;
	addrLo = (mBytes[8] << 24) + (mBytes[9] << 16) + (mBytes[10] << 8) + mBytes[11];
    } else {
	addrHi = (mBytes[8]  << 24) + (mBytes[9]  << 16) + (mBytes[10] << 8) + mBytes[11];
	addrLo = (mBytes[12] << 24) + (mBytes[13] << 16) + (mBytes[14] << 8) + mBytes[15]; 
    }

    UInt64 addr = (UInt64(addrHi) << 32) | addrLo;

    CarbonXPcieTLP data;
    data.setStartTick(mStartTime);
    data.setEndTick(mEndTime);
    data.setCmdByName(tType);
    data.setAddr(addr);
    data.setDataLength(hdrLength);
    data.setFirstBE(fdwbe);
    data.setLastBE(ldwbe);
    data.setReqId(reqID);
    data.setTag(tag);
    
    data.setFcType(PCIETransaction::NonPosted);
    data.setFcHeader(1); // Less that 16 bytes
    data.setFcData(0); // This TLP has no data

    xtor->callTransCallbackFn(eCarbonXPcieReceivedTrans, &data);
}

void PCIECallback::processMemWrite(void) {
    bool is32bit;
    UInt32 reqID;   // requester ID
    UInt32 tag;     // tag
    UInt32 fdwbe;   // first DW byte enable
    UInt32 ldwbe;   // last DW byte enable
    UInt32 addrHi;
    UInt32 addrLo;
    UInt32 headerSize;
    
#if 0
    UtIO::cout() << "MEMWRITE" << UtIO::endl;
#endif

    if(hdrFmt == 0x02) {
	is32bit = true;
	headerSize = 12; // 12-byte header
    } else {
	is32bit = false;
	headerSize = 16; // 16-byte header
    }

    reqID = (mBytes[4] << 8) + mBytes[5];
    tag   = mBytes[6];
    ldwbe = (mBytes[7] >> 4) & 0x0F;
    fdwbe = mBytes[7] & 0x0F;
    if(is32bit) {
	addrHi = 0x0;
	addrLo = (mBytes[8] << 24) + (mBytes[9] << 16) + (mBytes[10] << 8) + mBytes[11];
    } else {
	addrHi = (mBytes[8]  << 24) + (mBytes[9]  << 16) + (mBytes[10] << 8) + mBytes[11];
	addrLo = (mBytes[12] << 24) + (mBytes[13] << 16) + (mBytes[14] << 8) + mBytes[15]; 
    }

    UInt64 addr = (UInt64(addrHi) << 32) | addrLo;

    CarbonXPcieTLP data;
    data.setStartTick(mStartTime);
    data.setEndTick(mEndTime);
    data.setCmdByName(tType);
    data.setAddr(addr);
    data.setDataLength(hdrLength);
    data.setFirstBE(fdwbe);
    data.setLastBE(ldwbe);
    data.setReqId(reqID);
    data.setTag(tag);

    data.setFcType(PCIETransaction::Posted);
    data.setFcHeader(1); // Less that 16 bytes
    data.setFcData( (hdrLength+3)/4); // There is 4 bytes in a DW. Each FCC has 16 bytes

    /*
    ** There are hdrLength * 4 bytes.  First data byte starts at index 12 for 32 bit 16 for 64bit on a memory write
    */
    const int firstDataByte = is32bit ? 12 : 16;
    for (unsigned int j = 0; j < hdrLength * 4; j++) {
       data.setDataByte(j, mBytes[firstDataByte + j] );
    }

    xtor->callTransCallbackFn(eCarbonXPcieReceivedTrans, &data);

    // Memory Writes are Posted, therefore no completions should be generated

    if(cfg->getVerboseRxMed()) {
       UtOStream* ostr = carbonInterfaceGetCout();

       carbonInterfaceWriteStringToOStream(ostr, printStr.c_str());
       carbonInterfaceWriteStringToOStream(ostr, "     tType=MWr");
       carbonInterfaceWriteStringToOStream(ostr, is32bit?"32":"64");
       carbonInterfaceWriteStringToOStream(ostr, ", TrafficClass=");
       carbonInterfaceSetOStreamRadixHex(ostr);
       carbonInterfaceWriteUInt32ToOStream(ostr, hdrTC);
       carbonInterfaceWriteStringToOStream(ostr, ", TlpDigest=");
       carbonInterfaceWriteCharToOStream(ostr, hdrTD);
       carbonInterfaceWriteStringToOStream(ostr, ", ErrPoisoned=");
       carbonInterfaceWriteCharToOStream(ostr, hdrEP);
       carbonInterfaceWriteStringToOStream(ostr, ", Attr=");
       carbonInterfaceWriteUInt32ToOStream(ostr, hdrAttr);
       carbonInterfaceWriteStringToOStream(ostr, ", Length=");
       carbonInterfaceWriteUInt32ToOStream(ostr, hdrAttr);
       carbonInterfaceOStreamEndl(ostr);

       carbonInterfaceWriteStringToOStream(ostr, "       Req_ID=");
       carbonInterfaceWriteUInt32ToOStream(ostr, reqID);
       carbonInterfaceWriteStringToOStream(ostr, ", Tag=");
       carbonInterfaceWriteUInt32ToOStream(ostr, tag);
       carbonInterfaceWriteStringToOStream(ostr, ", Last_DW_Byte_Ena=");
       carbonInterfaceWriteUInt32ToOStream(ostr, ldwbe);
       carbonInterfaceWriteStringToOStream(ostr, ", First_DW_Byte_Ena=");
       carbonInterfaceWriteUInt32ToOStream(ostr, fdwbe);
       carbonInterfaceOStreamEndl(ostr);

       carbonInterfaceWriteStringToOStream(ostr, "       AddressHi=");
       carbonInterfaceWriteUInt32ToOStream(ostr, addrHi);
       carbonInterfaceWriteStringToOStream(ostr, ", AddressLo=");
       carbonInterfaceWriteUInt32ToOStream(ostr, addrLo);
       carbonInterfaceOStreamEndl(ostr);
    }
}

void PCIECallback::processIORead(void) {
   UInt64 addr;
   UInt32 reqID;   // requester ID
   UInt32 tag;     // tag
   UInt32 addrLo;
   UInt32 fdwbe;
   UInt32 ldwbe;

   addr = (mBytes[8] << 24) | (mBytes[9] << 16) | (mBytes[10] << 8) | (mBytes[11]);
   reqID = (mBytes[4] << 8) + mBytes[5];
   tag   = mBytes[6];
   addrLo = (mBytes[8] << 24) + (mBytes[9] << 16) + (mBytes[10] << 8) + mBytes[11];
   fdwbe = mBytes[7] & 0xF;
   ldwbe = (mBytes[7] >> 4) & 0xF;

   CarbonXPcieTLP data;
   data.setStartTick(mStartTime);
   data.setEndTick(mEndTime);
   data.setCmdByName(tType);
   data.setAddr(addr);
   data.setReqId(reqID);
   data.setTag(tag);
   data.setDataLength(hdrLength);
   data.setFirstBE(fdwbe);
   data.setLastBE(ldwbe);

   data.setFcType(PCIETransaction::NonPosted);
   data.setFcHeader(1); // Less that 16 bytes
   data.setFcData(0); // This TLP has no data

   xtor->callTransCallbackFn(eCarbonXPcieReceivedTrans, &data);
}

void PCIECallback::processIOWrite(void) {
    UInt32 reqID;   // requester ID
    UInt32 tag;     // tag
    UInt32 fdwbe;   // first DW byte enable
    UInt32 ldwbe;
    UInt32 addrLo;

    reqID = (mBytes[4] << 8) + mBytes[5];
    tag   = mBytes[6];
    fdwbe = mBytes[7] & 0x0F;
    ldwbe = (mBytes[7] >> 4) & 0xF;
    addrLo = (mBytes[8] << 24) + (mBytes[9] << 16) + (mBytes[10] << 8) + mBytes[11];

    UInt64 addr = addrLo;
    /*
    ** IO Writes MUST have a length of 4? 
    ** Last DW BE must be 0 (PCIE Spec Fig 2-15)
    */
    CarbonXPcieTLP data;
    data.setStartTick(mStartTime);
    data.setEndTick(mEndTime);
    data.setCmdByName(tType);
    data.setAddr(addr);
    data.setDataLength(hdrLength);
    data.setFirstBE(fdwbe);
    data.setLastBE(ldwbe);
    data.setReqId(reqID);
    data.setTag(tag);

    data.setFcType(PCIETransaction::NonPosted);
    data.setFcHeader(1); // Less that 16 bytes
    data.setFcData( (hdrLength+3)/4); // There is 4 bytes in a DW. Each FCC has 16 bytes

    /*
    ** There are hdrLength * 4 bytes.  First data byte starts at index 12 on a memory write
    */
    const int firstDataByte = 12;
    for (unsigned int j = 0; j < 4; j++) {
       data.setDataByte(j, mBytes[firstDataByte + j] );
    }
    
    xtor->callTransCallbackFn(eCarbonXPcieReceivedTrans, &data);

    if(cfg->getVerboseRxMed()) {
       UtOStream* ostr = carbonInterfaceGetCout();

       carbonInterfaceWriteStringToOStream(ostr, printStr.c_str());
       carbonInterfaceWriteStringToOStream(ostr, "     tType=eCarbonXPcieIOWr, TrafficClass=");
       carbonInterfaceSetOStreamRadixHex(ostr);
       carbonInterfaceWriteUInt32ToOStream(ostr, hdrTC);
       carbonInterfaceWriteStringToOStream(ostr, ", TlpDigest=");
       carbonInterfaceWriteCharToOStream(ostr, hdrTD);
       carbonInterfaceWriteStringToOStream(ostr, ", ErrPoisoned=");
       carbonInterfaceWriteCharToOStream(ostr, hdrEP);
       carbonInterfaceWriteStringToOStream(ostr, ", Attr=");
       carbonInterfaceWriteUInt32ToOStream(ostr, hdrAttr);
       carbonInterfaceWriteStringToOStream(ostr, ", Length=");
       carbonInterfaceWriteUInt32ToOStream(ostr, hdrAttr);
       carbonInterfaceOStreamEndl(ostr);

       carbonInterfaceWriteStringToOStream(ostr, "       Req_ID=");
       carbonInterfaceWriteUInt32ToOStream(ostr, reqID);
       carbonInterfaceWriteStringToOStream(ostr, ", Tag=");
       carbonInterfaceWriteUInt32ToOStream(ostr, tag);
       carbonInterfaceWriteStringToOStream(ostr, ", First_DW_Byte_Ena=");
       carbonInterfaceWriteUInt32ToOStream(ostr, fdwbe);
       carbonInterfaceOStreamEndl(ostr);

       carbonInterfaceWriteStringToOStream(ostr, "       AddressLo=");
       carbonInterfaceWriteUInt32ToOStream(ostr, addrLo);
       carbonInterfaceOStreamEndl(ostr);
    }
}

void PCIECallback::processConfigRead(void) {

   UInt32 fdwbe;
   UInt32 ldwbe;

   fdwbe = mBytes[7] & 0x0F;
   ldwbe = (mBytes[7] >> 4) & 0xF;

   // UInt64 junk = (mBytes[8] << 20) | (mBytes[9] << 12) | ((mBytes[10] & 0xf) << 8) | (mBytes[11]);
   // UtIO::cout() << printStr << "Received a cfgrd to " << UtIO::hex << junk << UtIO::endl;
   if (hdrLength != 1) {
      UtOStream* ostr = carbonInterfaceGetCout();
      carbonInterfaceWriteStringToOStream(ostr, printStr.c_str());
      carbonInterfaceWriteStringToOStream(ostr, "ERROR: Received config read with a length not equal to 1 byte");
      carbonInterfaceOStreamEndl(ostr);
      hdrLength = 1;
   }

   UInt64 addr = (mBytes[8] << 24) | (mBytes[9] << 16) | ((mBytes[10] & 0xf) << 8) | (mBytes[11]);

   // Config reads are always 1 DW / 4 BYTE.
   CarbonXPcieTLP data;
   data.setStartTick(mStartTime);
   data.setEndTick(mEndTime);
   data.setCmdByName(tType);
   data.setAddr(addr);
   data.setDataLength(hdrLength);
   data.setFirstBE(fdwbe);
   data.setLastBE(ldwbe);
   data.setReqId((mBytes[4] << 8) | mBytes[5]);   // {bus_num[7:0], dev_num[4:0], func_num[2:0]} of requester
   data.setTag(mBytes[6]);

   data.setFcType(PCIETransaction::NonPosted);
   data.setFcHeader(1); // Less that 16 bytes
   data.setFcData(0); // This TLP has no data

   xtor->callTransCallbackFn(eCarbonXPcieReceivedTrans, &data);

}

void PCIECallback::processConfigWrite(void) {
   UInt32 fdwbe;
   UInt32 ldwbe;
   UInt32 reqID, tag, compID;
   reqID = (mBytes[4] << 8) | mBytes[5];
   tag = mBytes[6];
   compID = (mBytes[8] << 8) | mBytes[9];
   fdwbe = mBytes[7] & 0x0F;
   ldwbe = (mBytes[7] >> 4) & 0xF;

   UInt64 addr = (mBytes[8] << 24) | (mBytes[9] << 16) | ((mBytes[10] & 0xf) << 8) | (mBytes[11]);

   CarbonXPcieTLP data;
   data.setStartTick(mStartTime);
   data.setEndTick(mEndTime);
   data.setCmdByName(tType);
   data.setAddr(addr);
   data.setDataLength(hdrLength);
   data.setFirstBE(fdwbe);
   data.setLastBE(ldwbe);
   data.setReqId(reqID);
   data.setTag(tag);

   const int firstDataByte = 12;
   for (unsigned int j = 0; j < 4; j++) {
      data.setDataByte(j, mBytes[firstDataByte + j] );
   }

   data.setFcType(PCIETransaction::NonPosted);
   data.setFcHeader(1); // Less that 16 bytes
   data.setFcData( (hdrLength+3)/4); // There is 4 bytes in a DW. Each FCC has 16 bytes

#if 0
  UtIO::cout() << UtIO::hex << " byte data: " << ' ' << UInt32(mBytes[12]) << ' ' << UInt32(mBytes[13]) << ' ' 
	    << UInt32(mBytes[14]) << ' ' << UInt32(mBytes[15]) << UtIO::endl;
#endif

  xtor->callTransCallbackFn(eCarbonXPcieReceivedTrans, &data);
}

void PCIECallback::processMessage(void) {
    UInt32 reqID;   // requester ID
    UInt32 tag;     // tag
    UInt32 msgCode; // message code
    UInt32 msgRout; // message routing

    // first 4 bytes were the header
    reqID = (mBytes[4] << 8) + mBytes[5];
    tag = mBytes[6];
    msgCode = mBytes[7];
    msgRout = mBytes[0] & 0x7;

   CarbonXPcieTLP data;
   data.setStartTick(mStartTime);
   data.setEndTick(mEndTime);
   data.setCmdByName(tType);
   data.setReqId(reqID);
   data.setTag(tag);
   data.setMsgCode(msgCode);
   data.setMsgRout(msgRout);
   data.setDataLength(hdrLength);
   
   if(tType == eCarbonXPcieMsgD) {
      for (UInt32 j=0; j < (hdrLength * 4); j++) {
	 data.setDataByte(j, mBytes[12 + j] );
      }
   }

   data.setFcType(PCIETransaction::Posted);
   data.setFcHeader(1); // Less that 16 bytes
   data.setFcData( (hdrLength+3)/4); // There is 4 bytes in a DW. Each FCC has 16 bytes

   xtor->callTransCallbackFn(eCarbonXPcieReceivedTrans, &data);
   
   return;
}

void PCIECallback::processCompletion(void) {
   UInt16 cplID   = (mBytes[4] << 8) | (mBytes[5]);
   UInt16 reqID   = (mBytes[8] << 8) | (mBytes[9]);
   UInt8  tag     = (mBytes[10]);
   UInt8  lowAddr = (mBytes[11] & 0x7F);
   UInt8 cplStat = ((mBytes[6] & 0xE0) >> 5);
   UInt32 numBytes = ((mBytes[6] & 0x0F) << 8) | (mBytes[7]);
   UInt32 transID;

   transID = UInt32(reqID << 8) | UInt32(tag);

   UtOStream* ostr = carbonInterfaceGetCout();

   switch (cplStat)
      {
      case 0x0: // Successful Completion
	 if(cfg->getVerboseRxMed()) {
            carbonInterfaceWriteStringToOStream(ostr, printStr.c_str());
            carbonInterfaceWriteStringToOStream(ostr, "Info: Received Completion Type:  Successful Completion  (DW Length = 0x");
            carbonInterfaceSetOStreamRadixHex(ostr);
            carbonInterfaceWriteUInt32ToOStream(ostr, hdrLength);
            carbonInterfaceWriteStringToOStream(ostr, ")");
            carbonInterfaceOStreamEndl(ostr);
         }
	 break;
      case 0x1: // Unsupported Request
         carbonInterfaceWriteStringToOStream(ostr, printStr.c_str());
         carbonInterfaceWriteStringToOStream(ostr, "ERROR: Received Completion Type:  Unsupported Request");
         carbonInterfaceOStreamEndl(ostr);
	 break;
      case 0x2: // Configuration Request Retry Status
         carbonInterfaceWriteStringToOStream(ostr, printStr.c_str());
         carbonInterfaceWriteStringToOStream(ostr, "ERROR: Received Completion Type:  Config Request Retry Status");
         carbonInterfaceOStreamEndl(ostr);
	 break;
      case 0x4: // Completer Abort
         carbonInterfaceWriteStringToOStream(ostr, printStr.c_str());
         carbonInterfaceWriteStringToOStream(ostr, "ERROR: Received Completion Type:  Completer Abort");
         carbonInterfaceOStreamEndl(ostr);
	 break;
      default:
         carbonInterfaceWriteStringToOStream(ostr, printStr.c_str());
         carbonInterfaceWriteStringToOStream(ostr, "ERROR: Received Completion Type:  <Unknown Type>  (0x");
         carbonInterfaceSetOStreamRadixHex(ostr);
         carbonInterfaceWriteUInt32ToOStream(ostr, cplStat);
         carbonInterfaceWriteStringToOStream(ostr, ")");
         carbonInterfaceOStreamEndl(ostr);
	 break;
      }

   if(cfg->getVerboseRxHigh()) {
      carbonInterfaceWriteStringToOStream(ostr, printStr.c_str());
      carbonInterfaceWriteStringToOStream(ostr, "Info: Completion from DUT: byte data: ");
      if(hdrLength == 0)
         carbonInterfaceWriteStringToOStream(ostr, "(none)");
      else
	 for(UInt32 i=0; i < (hdrLength * 4); i++) {
            carbonInterfaceSetOStreamRadixHex(ostr);
            carbonInterfaceSetOStreamWidth(ostr, 2);
            carbonInterfaceSetOStreamFill(ostr, '0');
            carbonInterfaceWriteUInt32ToOStream(ostr, static_cast<unsigned>(mBytes[(12+i)]));
            carbonInterfaceWriteStringToOStream(ostr, " ");
         }
      carbonInterfaceOStreamEndl(ostr);
   }

   // MIKE: Must check that these values are what I spec'd
   CarbonXPcieTLP data;
   data.setStartTick(mStartTime);
   data.setEndTick(mEndTime);
   data.setCmdByName(tType);
   // data.setAddr(addr);
   data.setCplStatus(cplStat);
   data.setCompId(cplID);
   data.setReqId(reqID);
   data.setTag(tag);
   data.setLowAddr(lowAddr);
   data.setByteCount(numBytes);
   data.setDataLength(hdrLength);
   for (UInt32 j=0; j < (hdrLength * 4); j++) {
      data.setDataByte(j, mBytes[12 + j] );
   }

    data.setFcType(PCIETransaction::Completion);
    data.setFcHeader(1); // Less that 16 bytes
    data.setFcData( (hdrLength+3)/4); // There is 4 bytes in a DW. Each FCC has 16 bytes

//c UtIO::cout() << "MIKE: rx cpl for transID 0x" << UtIO::hex << ((UInt32)transID) << " (cplID=0x" << ((UInt32)cplID) << ", reqID=0x" << ((UInt32)reqID) << ", tag=0x" << ((UInt32)tag) << ", bytecount=0x" << numBytes << ") (mBytes[10]=0x" << ((UInt32)mBytes[10]) << ")" << UtIO::endl;
//c UtIO::cout() << "MIKE: Transaction bytes: ";
//c for (UInt32 k=0; k < mBytes.size(); k++) {
//c    UtIO::cout() << UtIO::hex << ((UInt32)mBytes[k]) << " ";
//c }
//c UtIO::cout() << UtIO::endl;

   xtor->callTransCallbackFn(eCarbonXPcieReturnData, &data);

   return;
}


PCIECallback::~PCIECallback(void)
{
   mBytes.clear();
}


void PCIECallback::onCompletion(CompletionFunc * func, void * user_arg)
{
   if (func == 0) {
      UtOStream* ostr = carbonInterfaceGetCout();
      carbonInterfaceWriteStringToOStream(ostr, printStr.c_str());
      carbonInterfaceWriteStringToOStream(ostr, "ERROR: onCompletion() called with a NULL function pointer");
      carbonInterfaceOStreamEndl(ostr);
   } else {
      mCompletionFunc = func;
      mCompletionFuncArg = user_arg;
   }
}


void PCIECallback::doCompletion(void)
{
    UInt32 a;
    a = 0;
    if (mCompletionFunc)
    {
	(mCompletionFunc)(a,NULL,mCompletionFuncArg);
    }
}

int PCIECallback::calcByteCount(UInt32 length, unsigned char lastBE, unsigned firstBE) {
  /*
  ** There's probably a more algorithmic approach to this...
  ** This is from table 2-21 (p86) of the PCIE spec
  */

  if (lastBE == 0) {
     if (length > 1) {
        UtOStream* ostr = carbonInterfaceGetCout();
        carbonInterfaceWriteStringToOStream(ostr, printStr.c_str());
        carbonInterfaceWriteStringToOStream(ostr, "ERROR: Found transaction to have a length > 1 and the last DW byte enable is 0.  Changing the length from ");
        carbonInterfaceSetOStreamRadixDec(ostr);
        carbonInterfaceWriteUInt32ToOStream(ostr, length);
        carbonInterfaceWriteStringToOStream(ostr, " to 1.");
        carbonInterfaceOStreamEndl(ostr);
	length = 1;
     }

    switch(firstBE) {
    case 0x0:
    case 0x8:
    case 0x4:
    case 0x2:
    case 0x1:
      return 1;
    case 0xC:
    case 0x6:
    case 0x3:
      return 2;
    case 0xE:
    case 0xA:
    case 0x5:
    case 0x7:
      return 3;
    case 0x9:
    case 0xB:
    case 0xD:
    case 0xF:
      return 4;
    default:
       UtOStream* ostr = carbonInterfaceGetCout();
       carbonInterfaceWriteStringToOStream(ostr, printStr.c_str());
       carbonInterfaceWriteStringToOStream(ostr, "ERROR: Found transaction to have an invalid first DW byte enable (0x");
       carbonInterfaceSetOStreamRadixHex(ostr);
       carbonInterfaceWriteUInt32ToOStream(ostr, firstBE);
       carbonInterfaceWriteStringToOStream(ostr, ")");
       carbonInterfaceOStreamEndl(ostr);
       return 4;
    }
  }

  int adjustment = 0;

  if (firstBE & 0x1) {
    adjustment -= 0;
  } else if (firstBE & 0x2) {
    adjustment -= 1;
  } else if (firstBE & 0x4) {
    adjustment -= 2;
  } else if (firstBE & 0x8) {
    adjustment -= 3;
  }

  if (lastBE & 0x8) {
    adjustment -= 0;
  } else if (lastBE & 0x4) {
    adjustment -= 1;
  } else if (lastBE & 0x2) {
    adjustment -= 2;
  } else if (lastBE & 0x1) {
    adjustment -= 3;
  }

  return (length * 4) + adjustment;

}


/*
** This routine adjusts the address bits of to point to the 
** starting byte based on the byte enables for the transaction
** This only needs to happen for memeory read requests.
** 
** See table 2-22 in the PCIE spec
*/
UInt32 PCIECallback::fixAddrForBE(UInt32 addr, unsigned char be)
{
   UInt32 retAddr = addr & ~UInt32(0x3);

   if (be == 0 or (be & 0x1)) {
      retAddr |= 0;
   } else if (be & 0x2) {
      retAddr |= 1;
   } else if (be & 0x4) {
      retAddr |= 2;
   } else if (be & 0x8) {
      retAddr |= 3;
   } else {
      printf("ERROR -- PCIECallBack::fixAddrForBE -- invalid be=%x\n", be);
   }

   return retAddr & 0x7F;
}

/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
