/***************************************************************************************
  Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <cstdio>
#include "carbonXPcie.h"
#include "CarbonXPcieTLP.h"

CarbonXPcieTLP::CarbonXPcieTLP(void)
  : type(eCarbonXPcieMsg), addrHi(0), addrLo(0), reqID(0xFFFF),
    tag(0xFFFFFFFF), regNum(0), compID(0), msgRout(0), msgCode(0), compStat(0),
    byteCount(0), lowAddr(0), firstDWBE(0), lastDWBE(0), seqNum(0),
    length(0), startTick(0), endTick(0)
{
   mData.clear();
}

CarbonXPcieTLP::CarbonXPcieTLP(const CarbonXPcieTLP& in) {
   copy(in);
}

void CarbonXPcieTLP::copy(const CarbonXPcieTLP& in) {
   type = in.type;
   addrHi = in.addrHi;
   addrLo = in.addrLo;
   reqID = in.reqID;
   tag = in.tag;
   regNum = in.regNum;
   compID = in.compID;
   msgRout = in.msgRout;
   msgCode = in.msgCode;
   compStat = in.compStat;
   byteCount = in.byteCount;
   lowAddr = in.lowAddr;
   firstDWBE = in.firstDWBE;
   lastDWBE = in.lastDWBE;
   seqNum = in.seqNum;
   length = in.length;
   mData = in.mData;
   startTick = in.startTick;
   endTick = in.endTick;

   mFcType   = in.mFcType;
   mFcHeader = in.mFcHeader;
   mFcData   = in.mFcData;
}

void CarbonXPcieTLP::setCmd(UInt32 cmd) {
   switch (cmd) {
   case 0x00:  type = eCarbonXPcieMRd32;   break;
   case 0x20:  type = eCarbonXPcieMRd64;   break;
   case 0x01:  type = eCarbonXPcieMRdLk32; break;
   case 0x21:  type = eCarbonXPcieMRdLk64; break;
   case 0x40:  type = eCarbonXPcieMWr32;   break;
   case 0x60:  type = eCarbonXPcieMWr64;   break;
   case 0x02:  type = eCarbonXPcieIORd;    break;
   case 0x42:  type = eCarbonXPcieIOWr;    break;
   case 0x04:  type = eCarbonXPcieCfgRd0;  break;
   case 0x44:  type = eCarbonXPcieCfgWr0;  break;
   case 0x05:  type = eCarbonXPcieCfgRd1;  break;
   case 0x45:  type = eCarbonXPcieCfgWr1;  break;
   case 0x30:  type = eCarbonXPcieMsg;     break;
   case 0x70:  type = eCarbonXPcieMsgD;    break;
   case 0x0A:  type = eCarbonXPcieCpl;     break;
   case 0x4A:  type = eCarbonXPcieCplD;    break;
   case 0x0B:  type = eCarbonXPcieCplLk;   break;
   case 0x4B:  type = eCarbonXPcieCplDLk;  break;
   default:
      printf("ERROR: Attempting to set CarbonXPcieTLP to an invalid value: 0x%02X.\n", cmd);
      break;
   }
}

UInt32 CarbonXPcieTLP::getCmd(void) const {
   return (UInt32)type;
}

void CarbonXPcieTLP::setCmdByName(CarbonXPcieCmdType cmd) {
   type = cmd;
}

CarbonXPcieCmdType CarbonXPcieTLP::getCmdByName(void) const {
   return type;
}

void CarbonXPcieTLP::setAddr(UInt64 addr) {
   addrHi = (addr >> 32) & 0xFFFFFFFF;
   //   addrLo = addr & 0xFFFFFFFF;
   addrLo = addr & 0xFFFFFFFC;  // DW addressing
}

UInt64 CarbonXPcieTLP::getAddr(void) const {
   UInt64 addr = (((UInt64)addrHi << 32) | addrLo);
   return addr;
}

void CarbonXPcieTLP::setReqId(UInt32 id) {
   reqID = id;
}

UInt32 CarbonXPcieTLP::getReqId(void) const {
   return reqID;
}

void CarbonXPcieTLP::setTag(UInt32 Tag) {
   tag = Tag;
}

UInt32 CarbonXPcieTLP::getTag(void) const {
   return tag;
}

void CarbonXPcieTLP::setRegNum(UInt32 reg) {
   regNum = (reg & 0x03FF);
}

UInt32 CarbonXPcieTLP::getRegNum(void) const {
   return regNum;
}

void CarbonXPcieTLP::setCompId(UInt32 id) {
   compID = id;
}

UInt32 CarbonXPcieTLP::getCompId(void) const {
   return compID;
}

void CarbonXPcieTLP::setMsgRout(UInt32 rout) {
   msgRout = rout;
}

UInt32 CarbonXPcieTLP::getMsgRout(void) const {
   return msgRout;
}

void CarbonXPcieTLP::setMsgCode(UInt32 code) {
   msgCode = code;
}

UInt32 CarbonXPcieTLP::getMsgCode(void) const {
   return msgCode;
}

void CarbonXPcieTLP::setLowAddr(UInt32 addr) {
   lowAddr = addr;
}

UInt32 CarbonXPcieTLP::getLowAddr(void) const {
   return lowAddr;
}

void CarbonXPcieTLP::setByteCount(UInt32 count) {
   byteCount = count;
}

UInt32 CarbonXPcieTLP::getByteCount(void) const {
   return byteCount;
}

// DEPRECATED
UInt32 CarbonXPcieTLP::getDWCount(void) const {  // number of DWs taken up by the byte count
   return ((byteCount + 3) / 4);
}

void CarbonXPcieTLP::setFirstBE(UInt4 be) {
   firstDWBE = be;
}

UInt4 CarbonXPcieTLP::getFirstBE(void) const {
   return firstDWBE;
}

void CarbonXPcieTLP::setLastBE(UInt4 be) {
   lastDWBE = be;
}

UInt4 CarbonXPcieTLP::getLastBE(void) const {
   return lastDWBE;
}

void CarbonXPcieTLP::setCplStatus(UInt32 status) {
   compStat = (status & 0x7);
}

UInt32 CarbonXPcieTLP::getCplStatus(void) const {
   return compStat;
}

void CarbonXPcieTLP::setDataByte(UInt32 index, UInt8 data) {
   if (index >= mData.size())
      mData.resize(index+1);
   mData[index] = data;
}

UInt8 CarbonXPcieTLP::getDataByte(UInt32 index) const {
   if (index < mData.size())
      return mData[index];
   else
      return 0x0;
}

// Resize the data vector to (start,end) inclusive
void CarbonXPcieTLP::resizeDataBytes(UInt32 start, UInt32 end) {
   mData.resize(end + 1, 0); // remove all after 'end' or fill in with zeros
   if (start != 0)       // remove from begining and shift down
      mData.erase(mData.begin(), mData.begin() + start);
}

UInt32 CarbonXPcieTLP::getDataByteCount(void) const {
   return mData.size();
}

void CarbonXPcieTLP::setDataLength(UInt32 len) {
   length = len & 0x3FF;
}

UInt32 CarbonXPcieTLP::getDataLength(void) const {
   return length;
}

SInt32 CarbonXPcieTLP::getTransId(void) const {
   return (((reqID & 0xFFFF) << 8) | (tag & 0x00FF));  // concatinate reqID[15:0] and tag[7:0]
}

void CarbonXPcieTLP::setStartTick(UInt64 tick) {
   startTick = tick;
}

void CarbonXPcieTLP::setEndTick(UInt64 tick) {
   endTick = tick;
}

UInt64 CarbonXPcieTLP::getStartTick(void) const {
   return startTick;
}

UInt64 CarbonXPcieTLP::getEndTick(void) const {
   return endTick;
}

bool CarbonXPcieTLP::isRead(void) const {
   switch (type) {
   case eCarbonXPcieMRd32:
   case eCarbonXPcieMRd64:
   case eCarbonXPcieMRdLk32:
   case eCarbonXPcieMRdLk64:
   case eCarbonXPcieIORd:
   case eCarbonXPcieCfgRd0:
   case eCarbonXPcieCfgRd1:
      return true;
   default:
      return false;
   }
}

bool CarbonXPcieTLP::isWrite(void) const {
   switch (type) {
   case eCarbonXPcieMWr32:
   case eCarbonXPcieMWr64:
   case eCarbonXPcieIOWr:
   case eCarbonXPcieCfgWr0:
   case eCarbonXPcieCfgWr1:
      return true;
   default:
      return false;
   }
}

CarbonXPcieTLP * carbonXPcieTLPCreate() {
   return new CarbonXPcieTLP;
}

void carbonXPcieTLPDestroy(CarbonXPcieTLP *tlp) {
   delete tlp;
}

const char* carbonXPcieTLPGetStrFromType (CarbonXPcieCmdType type){
   switch (type) {
   case eCarbonXPcieMRd32:   return "eCarbonXPcieMRd32";
   case eCarbonXPcieMRd64:   return "eCarbonXPcieMRd64";
   case eCarbonXPcieMRdLk32: return "eCarbonXPcieMRdLk32";
   case eCarbonXPcieMRdLk64: return "eCarbonXPcieMRdLk64";
   case eCarbonXPcieMWr32:   return "eCarbonXPcieMWr32";
   case eCarbonXPcieMWr64:   return "eCarbonXPcieMWr64";
   case eCarbonXPcieIORd:    return "eCarbonXPcieIORd";
   case eCarbonXPcieIOWr:    return "eCarbonXPcieIOWr";
   case eCarbonXPcieCfgRd0:  return "eCarbonXPcieCfgRd0";
   case eCarbonXPcieCfgWr0:  return "eCarbonXPcieCfgWr0";
   case eCarbonXPcieCfgRd1:  return "eCarbonXPcieCfgRd1";
   case eCarbonXPcieCfgWr1:  return "eCarbonXPcieCfgWr1";
   case eCarbonXPcieMsg:     return "eCarbonXPcieMsg";
   case eCarbonXPcieMsgD:    return "eCarbonXPcieMsgD";
   case eCarbonXPcieCpl:     return "eCarbonXPcieCpl";
   case eCarbonXPcieCplD:    return "eCarbonXPcieCplD";
   case eCarbonXPcieCplLk:   return "eCarbonXPcieCplLk";
   case eCarbonXPcieCplDLk:  return "eCarbonXPcieCplDLk";
   default:               return "Invalid Type";
   }
}
void   carbonXPcieTLPCopy         (CarbonXPcieTLP *pimpl_, const CarbonXPcieTLP *orig) {
   pimpl_->copy(*orig);
}
void   carbonXPcieTLPSetCmd       (CarbonXPcieTLP *pimpl_, UInt32 cmd) {
   pimpl_->setCmd(cmd);
}
UInt32 carbonXPcieTLPGetCmd       (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getCmd();
}
void   carbonXPcieTLPSetCmdByName (CarbonXPcieTLP *pimpl_, CarbonXPcieCmdType cmd) {
   pimpl_->setCmdByName(cmd);
}
CarbonXPcieCmdType carbonXPcieTLPGetCmdByName (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getCmdByName();
}
void   carbonXPcieTLPSetAddr      (CarbonXPcieTLP *pimpl_, UInt64 addr) {
   pimpl_->setAddr(addr);
}
UInt64 carbonXPcieTLPGetAddr      (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getAddr();
}
void   carbonXPcieTLPSetReqId     (CarbonXPcieTLP *pimpl_, UInt32 id) {
   pimpl_->setReqId(id);
}
UInt32 carbonXPcieTLPGetReqId     (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getReqId();
}
void   carbonXPcieTLPSetTag       (CarbonXPcieTLP *pimpl_, UInt32 tag) {
   pimpl_->setTag(tag);
}
UInt32 carbonXPcieTLPGetTag       (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getTag();
}
void   carbonXPcieTLPSetRegNum    (CarbonXPcieTLP *pimpl_, UInt32 reg) {
   pimpl_->setRegNum(reg);
}
UInt32 carbonXPcieTLPGetRegNum    (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getRegNum();
}
void   carbonXPcieTLPSetCompId    (CarbonXPcieTLP *pimpl_, UInt32 id) {
   pimpl_->setCompId(id);
}
UInt32 carbonXPcieTLPGetCompId    (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getCompId();
}
void   carbonXPcieTLPSetMsgRout   (CarbonXPcieTLP *pimpl_, UInt32 rout) {
   pimpl_->setMsgRout(rout);
}
UInt32 carbonXPcieTLPGetMsgRout   (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getMsgRout();
}
void   carbonXPcieTLPSetMsgCode   (CarbonXPcieTLP *pimpl_, UInt32 code) {
   pimpl_->setMsgCode(code);
}
UInt32 carbonXPcieTLPGetMsgCode   (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getMsgCode();
}
void   carbonXPcieTLPSetLowAddr   (CarbonXPcieTLP *pimpl_, UInt32 addr) {
   pimpl_->setLowAddr(addr);
}
UInt32 carbonXPcieTLPGetLowAddr   (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getLowAddr();
}
void   carbonXPcieTLPSetByteCount (CarbonXPcieTLP *pimpl_, UInt32 count) {
   pimpl_->setByteCount(count);
}
UInt32 carbonXPcieTLPGetByteCount (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getByteCount();
}
void   carbonXPcieTLPSetFirstBE   (CarbonXPcieTLP *pimpl_, UInt32 be) {
   pimpl_->setFirstBE(be);
}
UInt32 carbonXPcieTLPGetFirstBE   (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getFirstBE();
}
void   carbonXPcieTLPSetLastBE    (CarbonXPcieTLP *pimpl_, UInt32 be) {
   pimpl_->setLastBE(be);
}
UInt32 carbonXPcieTLPGetLastBE    (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getLastBE();
}
void   carbonXPcieTLPSetCplStatus (CarbonXPcieTLP *pimpl_, UInt32 status) {
    pimpl_->setCplStatus(status);
}
UInt32 carbonXPcieTLPGetCplStatus (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getCplStatus();
}
void   carbonXPcieTLPSetDataByte  (CarbonXPcieTLP *pimpl_, UInt32 index, UInt8 data) {
   pimpl_->setDataByte(index, data);
}
UInt32  carbonXPcieTLPGetDWord  (const CarbonXPcieTLP *pimpl_, UInt32 index) {
   return 
      pimpl_->getDataByte(index*4+0) << 24 |
      pimpl_->getDataByte(index*4+1) << 16 |
      pimpl_->getDataByte(index*4+2) <<  8 |
      pimpl_->getDataByte(index*4+3) <<  0;
}
void   carbonXPcieTLPSetDWord  (CarbonXPcieTLP *pimpl_, UInt32 index, UInt32 data) {
   pimpl_->setDataByte(index*4+0, data >> 24);
   pimpl_->setDataByte(index*4+1, data >> 16);
   pimpl_->setDataByte(index*4+2, data >>  8);
   pimpl_->setDataByte(index*4+3, data >>  0);
}
UInt8  carbonXPcieTLPGetDataByte  (const CarbonXPcieTLP *pimpl_, UInt32 index) {
   return pimpl_->getDataByte(index);
}
void   carbonXPcieTLPResizeDataBytes  (CarbonXPcieTLP *pimpl_, UInt32 start, UInt32 end) {
   pimpl_->resizeDataBytes(start, end);
}
UInt32 carbonXPcieTLPGetDataByteCount (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getDataByteCount();
}
void   carbonXPcieTLPSetDataLength (CarbonXPcieTLP *pimpl_, UInt32 length) {
   pimpl_->setDataLength(length);
}
UInt32 carbonXPcieTLPGetDataLength (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getDataLength();
}
SInt32 carbonXPcieTLPGetTransId   (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getTransId();
}
void   carbonXPcieTLPSetStartTick (CarbonXPcieTLP *pimpl_, UInt64 tick) {
   pimpl_->setStartTick(tick);
}
UInt64 carbonXPcieTLPGetStartTick (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getStartTick();
}
void   carbonXPcieTLPSetEndTick (CarbonXPcieTLP *pimpl_, UInt64 tick) {
   pimpl_->setEndTick(tick);
}
UInt64 carbonXPcieTLPGetEndTick (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->getEndTick();
}
UInt32   carbonXPcieTLPIsRead       (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->isRead();
}
UInt32   carbonXPcieTLPIsWrite      (const CarbonXPcieTLP *pimpl_) {
   return pimpl_->isWrite();
}

void carbonXPcieTLPMemReadReq(CarbonXPcieTLP *tlp, 
			      UInt8 tag, 
			      UInt64 addr, UInt32 num_dwords, 
			      UInt32 first_be, UInt64 last_be) {
   
   if(addr >= 0x100000000LL)
      tlp->setCmd(eCarbonXPcieMRd64);
   else
      tlp->setCmd(eCarbonXPcieMRd32);

   tlp->setTag(tag);
   tlp->setAddr(addr & 0xfffffffffffffffcLL);
   tlp->setDataLength(num_dwords);
   tlp->setFirstBE(first_be);
   tlp->setLastBE(last_be);
}
  
void carbonXPcieTLPCompleter(CarbonXPcieTLP *tlp, 
			     UInt32 req_id, UInt8 tag, 
			     const UInt32 *data, UInt32 num_dwords, 
			     UInt8  byte_count, UInt8 lower_addr) {
   num_dwords ? tlp->setCmd(eCarbonXPcieCplD) : tlp->setCmd(eCarbonXPcieCpl);
   tlp->setReqId(req_id);
   tlp->setTag(tag);
   tlp->setDataLength(num_dwords);
   tlp->setByteCount(byte_count);
   tlp->setLowAddr(lower_addr);
   
   tlp->resizeDataBytes(0, 4*num_dwords -1);
   for(UInt32 i = 0; i < num_dwords; i++) {
      tlp->setDataByte(i*4+0, data[i] >> 24);
      tlp->setDataByte(i*4+1, data[i] >> 16);
      tlp->setDataByte(i*4+2, data[i] >>  8);
      tlp->setDataByte(i*4+3, data[i] >>  0);
   }
}

void carbonXPcieTLPMemWriteReq(CarbonXPcieTLP *tlp, 
			       UInt8 tag, 
			       UInt64 addr, const UInt32 *data, UInt32 num_dwords, 
			       UInt32 first_be, UInt64 last_be) {
   if(addr >= 0x100000000LL)
      tlp->setCmd(eCarbonXPcieMWr64);
   else
      tlp->setCmd(eCarbonXPcieMWr32);

   tlp->setTag(tag);
   tlp->setAddr(addr & 0xfffffffffffffffcLL);
   tlp->setDataLength(num_dwords);
   tlp->setFirstBE(first_be);
   tlp->setLastBE(last_be);

   tlp->resizeDataBytes(0, 4*num_dwords -1);
   for(UInt32 i = 0; i < num_dwords; i++) {
      tlp->setDataByte(i*4+0, data[i] >> 24);
      tlp->setDataByte(i*4+1, data[i] >> 16);
      tlp->setDataByte(i*4+2, data[i] >>  8);
      tlp->setDataByte(i*4+3, data[i] >>  0);
   }
}
  
void carbonXPcieTLPIORead32Req(CarbonXPcieTLP *tlp, 
			       UInt8 tag, 
			       UInt64 addr,
			       UInt32 first_be) {
   tlp->setCmd(eCarbonXPcieIORd);
   tlp->setTag(tag);
   tlp->setAddr(addr & 0xfffffffffffffffcLL);
   tlp->setDataLength(1);
   tlp->setFirstBE(first_be);
   tlp->setLastBE(0);
}

void carbonXPcieTLPIOWrite32Req(CarbonXPcieTLP *tlp, 
				UInt8 tag, 
				UInt64 addr, UInt32 data, 
				UInt32 first_be) {
   tlp->setCmd(eCarbonXPcieIOWr);
   tlp->setTag(tag);
   tlp->setAddr(addr & 0xfffffffffffffffcLL);
   tlp->setDataLength(1);
   tlp->setFirstBE(first_be);
   tlp->setLastBE(0);
   
   tlp->resizeDataBytes(0, 3);
   tlp->setDataByte(0, data >> 24);
   tlp->setDataByte(1, data >> 16);
   tlp->setDataByte(2, data >>  8);
   tlp->setDataByte(3, data >>  0);
}
  
void carbonXPcieTLPCfg0ReadReq(CarbonXPcieTLP *tlp, 
			       UInt8  tag, 
			       UInt32 addr, 
			       UInt32 first_be) {
   tlp->setCmd(eCarbonXPcieCfgRd0);
   tlp->setTag(tag);
   tlp->setAddr(addr & 0xffff);
   tlp->setDataLength(1);
   tlp->setFirstBE(first_be);
   tlp->setLastBE(0);
}

void carbonXPcieTLPCfg1ReadReq(CarbonXPcieTLP *tlp, 
			      UInt8  tag, 
			      UInt32 addr, 
			      UInt32 first_be) {
   tlp->setCmd(eCarbonXPcieCfgRd0);
   tlp->setTag(tag);
   tlp->setAddr(addr & 0xffff);
   tlp->setDataLength(1);
   tlp->setFirstBE(first_be);
   tlp->setLastBE(0);
}

void carbonXPcieTLPCfg0WriteReq(CarbonXPcieTLP *tlp,
			       UInt8 tag, 
			       UInt32 addr, UInt32 data, 
			       UInt32 first_be) {
   tlp->setCmd(eCarbonXPcieCfgWr0);
   tlp->setTag(tag);
   tlp->setAddr(addr & 0xffff);
   tlp->setDataLength(1);
   tlp->setFirstBE(first_be);
   tlp->setLastBE(0);
   
   tlp->resizeDataBytes(0, 3);
   tlp->setDataByte(0, data >> 24);
   tlp->setDataByte(1, data >> 16);
   tlp->setDataByte(2, data >>  8);
   tlp->setDataByte(3, data >>  0);
}

void carbonXPcieTLPCfg1WriteReq(CarbonXPcieTLP *tlp,
			       UInt8 tag, 
			       UInt32 addr, UInt32 data, 
			       UInt32 first_be) {
   tlp->setCmd(eCarbonXPcieCfgWr1);
   tlp->setTag(tag);
   tlp->setAddr(addr & 0xffff);
   tlp->setDataLength(1);
   tlp->setFirstBE(first_be);
   tlp->setLastBE(0);
   
   tlp->resizeDataBytes(0, 3);
   tlp->setDataByte(0, data >> 24);
   tlp->setDataByte(1, data >> 16);
   tlp->setDataByte(2, data >>  8);
   tlp->setDataByte(3, data >>  0);
}
  
void carbonXPcieTLPMsgBaseReq(CarbonXPcieTLP *tlp, 
			      UInt8 tag, 
			      const UInt32 *data, UInt32 num_dwords, 
			      UInt8 rout, UInt8 msg_code) {
   if(num_dwords > 0)
      tlp->setCmd(eCarbonXPcieMsgD);
   else
      tlp->setCmd(eCarbonXPcieMsg);
      
   tlp->setTag(tag);
   tlp->setDataLength(num_dwords);
   tlp->setMsgRout(rout);
   tlp->setMsgCode(msg_code);

   tlp->resizeDataBytes(0, 4*num_dwords -1);
   for(UInt32 i = 0; i < num_dwords && data; i++) {
      tlp->setDataByte(i*4+0, data[i] >> 24);
      tlp->setDataByte(i*4+1, data[i] >> 16);
      tlp->setDataByte(i*4+2, data[i] >>  8);
      tlp->setDataByte(i*4+3, data[i] >>  0);
   }
}

/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
