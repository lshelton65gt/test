// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#ifndef _CarbonXPcieConfig_h_
#define _CarbonXPcieConfig_h_

#include "shell/carbon_shelltypes.h"
#include "CarbonXString.h"
#include "shell/carbon_interface.h"
#include "CarbonXList.h"
#include "carbonXPcie.h"

// Forward declarations
class MsgContext;
class MsgStreamIO;

//! Holds common configuration data for the transactor
struct CarbonXPcieConfig {

  CarbonXPcieConfig(UInt32 linkwidth, const char* inPrintName, FILE* stream);
  ~CarbonXPcieConfig();

  //! Current Transactor time in mCurrTimescale units
  UInt64 mCurrTick;
  
  //! Message context for error messages
  MsgStreamIO* mErrorStream;
  MsgContext*  mMsgContext;
  
  // Transactor Configuration Parameters

  //! link width that we're connecting to  
  UInt32 mLinkWidth;

  //! Device Control Register (0x08) bits [7:5] (spec 1.0a sec 7.8.4)
  //!   max data allowed to send in a single transaction
  //!   Possible Values:   [default = 128B]
  //!      128B =  32DW,  256B =  64DW,  512B =  128DW,
  //!     1024B = 256DW, 2048B = 512DW, 4096B = 1024DW
  UInt32 mMaxPayloadSizeDW;

  //! Link Control Register -- Read Completion Boundary
  UInt32 mLinkCtrlRCB;

  //! Force Elec Idle Inactive
  UInt32 mForceEIdleInactive;

  //! Are the Elec Idle nets active High (true) or Low (false)?
  bool mEIdleActiveHigh;

  //! maximum number of transactions in flight at once, 
  //! current spec max is 32 (default), use 1 to wait for 
  //! one to complete before sending the next  
  UInt32 mMaxTransInFlight;

  //! Scramble Status
  bool   mScrambleEnable;

  //! Training Bypass
  bool   mTrainingBypass;

  //! Supported Data Rates (2.5 and/or 5.0)
  UInt32 mSupportedDataRate;

  //! xtor Bus Number for Requestor ID   
  UInt32 mBusNum;

  //! xtor Device Number for Requestor ID   
  UInt32 mDevNum;

  //! xtor Function Number for Requestor ID   
  UInt32 mFuncNum;
  
  bool mUpstreamDevice;

  // Callbacks
  CarbonXPcieTransCBFn *mTransCBFn;
  void *mTransCBInfo;

  // Parameters used for Debug messages
  CarbonXString   mPrintName;                      // Name of the transactor
  bool            mCurrTimescaleValid;             // has the timescale been set
  CarbonTimescale mCurrTimescale;                  // timescale for mCurrTick
  char            mTimeFormat[sizeof "%0.06f ns"]; // Format string used to display current time
  double          mTimeFactor;
  CarbonXString   mCurrTimeString;                 // string with the mCurrTick scaled to mCurrTimescale
  bool            mVerboseReceiverHigh;            // print out a LOT of Receiver info
  bool            mVerboseReceiverMedium;          // print out a bit of Receiver info
  bool            mVerboseReceiverLow;             // print out some Receiver info
  bool            mVerboseTransmitterHigh;         // print out a LOT of Transmitter info
  bool            mVerboseTransmitterMedium;       // print out a bit of Transmitter info
  bool            mVerboseTransmitterLow;          // print out some Transmitter info

  // Config Method
  void   setTrainingBypass(bool tf);

  UInt32 getMaxPayloadSize(void);
  void   setMaxPayloadSize(UInt32 val);

  UInt32 getRCB(void);
  void   setRCB(UInt32 rcb);

  UInt32 getForceEIdleInactive();
  void   setForceEIdleInactive(UInt32 inactive);

  UInt32 getSupportedSpeeds();
  void   setSupportedSpeeds(UInt32 speeds);

  void   setScrambleEnable(bool);
  bool   getScrambleEnable();

  UInt32 getPrintLevel(void);
  void   setPrintLevel(UInt32 flags);

  void   setTimescale(CarbonTimescale ts);
  void   updateCurrTime(UInt64 tick);

  void   setTransCallbackFn(CarbonXPcieTransCBFn *transCbFn, void* transCbInfo) { mTransCBFn=transCbFn; mTransCBInfo=transCbInfo; }
  CarbonXPcieTransCBFn* getTransCallbackFn(void) { return mTransCBFn; }
  void*  getTransCallbackData(void) { return mTransCBInfo; }

  inline const char* getPrintName(void) { return mPrintName.c_str(); };
  bool getVerboseRxHigh(void) { return mVerboseReceiverHigh; }
  bool getVerboseRxMed(void) { return mVerboseReceiverMedium; }
  bool getVerboseRxLow(void) { return mVerboseReceiverLow; }
  bool getVerboseTxHigh(void) { return mVerboseTransmitterHigh; }
  bool getVerboseTxMed(void) { return mVerboseTransmitterMedium; }
  bool getVerboseTxLow(void) { return mVerboseTransmitterLow; }

private:
  void verbose(UInt32 flags);

};

class CarbonXPcieSym {
public :
  CARBONMEM_OVERRIDES
  
  CarbonXPcieSym() : mData(0), mCmd(false), mTime(0)  { }
  CarbonXPcieSym(UInt8 data, bool cmd, UInt64 time = 0) : mData(data), mCmd(cmd), mTime(time) { }
  
  // Compare Operators
  bool operator==(const CarbonXPcieSym& other) const {
    return other.mData == mData && other.mCmd == mCmd;
  }
  bool operator==(UInt8 other) const {
    return other == mData && false == mCmd;
  }
  bool operator!=(const CarbonXPcieSym& other) const {
    return !(other == *this);
  }
  bool operator!=(UInt8 other) const {
    return !(*this == other);
  }
  bool isData() const {
    return !mCmd;
  }

  // Assignment operators
  CarbonXPcieSym operator=(const CarbonXPcieSym& other) {
    if(&other != this) {
      mData = other.mData;
      mCmd  = other.mCmd;
      mTime = other.mTime;
    }
    return *this;
  }
  CarbonXPcieSym operator=(const UInt8& data) {
    mData = data;
    mCmd  = false;
    mTime = 0;
    return *this;
  }
  
  UInt8  mData;
  bool   mCmd;
  UInt64 mTime;
};

#define SYM_COM   CarbonXPcieSym(0xBC, true)
#define SYM_STP   CarbonXPcieSym(0xFB, true)
#define SYM_SDP   CarbonXPcieSym(0x5C, true)
#define SYM_END   CarbonXPcieSym(0xFD, true)
#define SYM_EDB   CarbonXPcieSym(0xFE, true)
#define SYM_PAD   CarbonXPcieSym(0xF7, true)
#define SYM_SKP   CarbonXPcieSym(0x1C, true)
#define SYM_FTS   CarbonXPcieSym(0x3C, true)
#define SYM_IDL   CarbonXPcieSym(0x7C, true)
#define SYM_RSVD1 CarbonXPcieSym(0x9C, true)
#define SYM_RSVD2 CarbonXPcieSym(0xDC, true)
#define SYM_RSVD3 CarbonXPcieSym(0xFC, true)

// Insert Operator
static inline void sWriteToOStream(UtOStream* os, const CarbonXPcieSym& obj) {
  if(obj == SYM_COM)
    carbonInterfaceWriteStringToOStream(os, "COM");
  else if(obj == SYM_STP)
    carbonInterfaceWriteStringToOStream(os, "STP");
  else if(obj == SYM_SDP)
    carbonInterfaceWriteStringToOStream(os, "SDP");
  else if(obj == SYM_END)
    carbonInterfaceWriteStringToOStream(os, "END");
  else if(obj == SYM_EDB)
    carbonInterfaceWriteStringToOStream(os, "EDB");
  else if(obj == SYM_PAD)
    carbonInterfaceWriteStringToOStream(os, "PAD");
  else if(obj == SYM_SKP)
    carbonInterfaceWriteStringToOStream(os, "SKP");
  else if(obj == SYM_FTS)
    carbonInterfaceWriteStringToOStream(os, "FTS");
  else if(obj == SYM_IDL)
    carbonInterfaceWriteStringToOStream(os, "IDL");
  else if(obj == SYM_RSVD1 || obj == SYM_RSVD2 || obj == SYM_RSVD3)
    carbonInterfaceWriteStringToOStream(os, "RES");
  else if(obj.isData()) {
    carbonInterfaceWriteStringToOStream(os, "0x");
    carbonInterfaceSetOStreamWidth(os, 2);
    carbonInterfaceSetOStreamFill(os, '0');
    carbonInterfaceSetOStreamRadixHex(os);
    carbonInterfaceWriteCharToOStream(os, obj.mData);
  } else
    carbonInterfaceWriteStringToOStream(os, "ILL");
}


class CarbonXPcieSymList {
public :
  CARBONMEM_OVERRIDES
  
  //! Constructor
  CarbonXPcieSymList() {
    // Start out with an empty list as the first element
    CarbonXList<CarbonXPcieSym> newList;
    mBuf.push_back(newList);
  }
  
  //! Add a symbol to the list
  void push(const CarbonXPcieSym& sym) {
    mBuf.back().push_back(sym);
  }

  //! Set a marker at the current end of the list
  void mark() {
    // Mark just creates a new empty list and pushes it to the buffer.
    // If there is already an empty list at the end of the buffer
    // a mark already exists so ignore.
    if(!mBuf.back().empty()) {
      CarbonXList<CarbonXPcieSym> newList;
      mBuf.push_back(newList);
    }
  }

  //! Extract the first packet in the list to the end of the given list
  bool extract(CarbonXList<CarbonXPcieSym>& list) {
    bool success = false;

    // If the number of elements in the buffer is less than 2
    // the current packet is not yet done, so just do nothing
    if( hasPacket() ) { 

      // Do the splice
      list.splice(list.end(), mBuf.front());

      // Cleanup this list
      mBuf.pop_front();
      success = true;
    }
    return success;
  }
  
  //! Extract the first packet in the list to the end of the given list
  bool extract(CarbonXPcieSymList& list) {
    
    // First check if the is a packet to extract
    if(!hasPacket()) return false;

    // If the destination list has an empty list at the end, extract to that list
    // and the push an empty list at the end.
    // If the list at the end of the destination list is not empty, create a new
    // empty list, use that one to extract and the create another empty list
    // to push at the end.
    
    if(!list.mBuf.back().empty()) {
      list.mark(); // This will put an empty list at the end
    }
    
    // Extract using the list at the end of the destination list
    bool success = extract(list.mBuf.back());
   
    // If the extraction was uccessful, put another empty list on the back
    // of the destination list.
    if(success) {
      CarbonXList<CarbonXPcieSym> newList;
      list.mBuf.push_back(newList);
    }
    return success;
  }

  //! Discard First packet
  void discard() {
    mBuf.pop_front();

    // If this left us with an empty list, push an empty list onto the list
    if(mBuf.empty()) {
      CarbonXList<CarbonXPcieSym> newList;
      mBuf.push_back(newList);
    }
  }

  // Number of query methods to figure out what type of packet
  // is first in this list
  
  //! Any packets ready.
  bool hasPacket() const {
    return mBuf.size() > 1;
  }
  
  //! Debug Print Methods
  void printFirstPacket() const {
    UtOStream* ostr = carbonInterfaceGetCout();
    carbonInterfaceWriteStringToOStream(ostr, "Packet Buffer contains ");
    carbonInterfaceWriteUInt32ToOStream(ostr, mBuf.size()-1);
    carbonInterfaceWriteStringToOStream(ostr, " packets. First Packet Data:");
    carbonInterfaceOStreamEndl(ostr);
    for(CarbonXList<CarbonXPcieSym>::const_iterator iter = mBuf.front().begin(); iter != mBuf.front().end(); ++iter) {
      sWriteToOStream(ostr, *iter);
      carbonInterfaceWriteStringToOStream(ostr, " ");
      }
    carbonInterfaceOStreamEndl(ostr);
  }

  void printAllPackets() const {
    UtOStream* ostr = carbonInterfaceGetCout();
    carbonInterfaceWriteStringToOStream(ostr, "Packet Buffer contains ");
    carbonInterfaceWriteUInt32ToOStream(ostr, mBuf.size()-1);
    carbonInterfaceWriteStringToOStream(ostr, " packets. Packet Data:");
    carbonInterfaceOStreamEndl(ostr);
    for(CarbonXList<CarbonXList<CarbonXPcieSym> >::const_iterator iter1 = mBuf.begin(); iter1 != mBuf.end(); ++iter1) {
      for(CarbonXList<CarbonXPcieSym>::const_iterator iter2 = iter1->begin(); iter2 != iter1->end(); ++iter2) {
        sWriteToOStream(ostr, *iter2);
        carbonInterfaceWriteStringToOStream(ostr, " ");
      }
    }
    carbonInterfaceOStreamEndl(ostr);
  }

protected :
  //! Buffer that holds the symbols of all received packets.
  CarbonXList<CarbonXList<CarbonXPcieSym> >          mBuf;
};

#endif
