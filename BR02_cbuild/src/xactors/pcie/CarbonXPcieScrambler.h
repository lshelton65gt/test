// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CarbonXPcieScrambler_H_
#define __CarbonXPcieScrambler_H_

#include "util/c_memmanager.h"
#include "shell/carbon_shelltypes.h"
#include "CarbonXArray.h"
#include "CarbonXPcieCommonTypes.h"

class CarbonXPcieScrambler {
public: CARBONMEM_OVERRIDES;
  CarbonXPcieScrambler(UInt32 linkwidth) : mEnable(true),
                                           mLinkWidth(linkwidth), 
					   mLfsrLane(linkwidth) {
  }
  
  void enable(bool en);
  void reset();
  inline UInt32 operator()(UInt32 inbyte ,bool kin, UInt32 lane) {
    return scramble_descramble_byte(inbyte, kin, lane);
  }
  inline CarbonXPcieSym operator()(const CarbonXPcieSym& sym, UInt32 lane) {
    return CarbonXPcieSym(scramble_descramble_byte(sym.mData, sym.mCmd, lane), sym.mCmd, sym.mTime);
  }

private:
  UInt32 scramble_descramble_byte(UInt32 inbyte ,bool kin, UInt32 lane);
  
  bool            mEnable;
  UInt32          mLinkWidth;
  CarbonXArray<UInt32> mLfsrLane;  // Linear Feedback Shift Register for scrambling (one per lane)

};

#endif
