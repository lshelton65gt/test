/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "shell/carbon_interface_xtor.h"
#include "CarbonXPciePhyLayer.h"
#include "CarbonXPcieCommonTypes.h"
#include <cstdio>

#include "CarbonXPciePhyTx.h"
#include "CarbonXPciePhyRx.h"
#include "CarbonXPciePipeIf.h"
#include "CarbonXPcie10bitIf.h"

CarbonXPciePhyLayer::CarbonXPciePhyLayer(CarbonXPcieIntfType intf, CarbonXPcieConfig& config) :
  mCfg(config), mIntfType(intf),
  mLinkNum(0), mRequestedLinkNum(0), mNFts(148), mRequestedNFts(0),
  mLinkState(Detect_Quiet), mNextLinkState(Detect_Quiet),
  mDataRate(0x2), mInitSupportedDataRate(0x2), mCurrSupportedDataRate(0x2), 
  mRequestedDataRate(0), mNegotiatedDataRate(0x2),  
  mDirectedSpeedChange(false), mChangedSpeedRecovery(false), mAckDataRateChange(false),
  mDataRateChangeRequested(false), mDataRateChCBInfo(0), mDataRateChCBFn(0)
{
  
  mPhyRx = new CarbonXPciePhyRx(mCfg, this, intf);

  if(intf == eCarbonXPciePipeIntf)
    mSignalIf = new CarbonXPciePipeIf(mCfg, *mPhyRx);
  else if(intf == eCarbonXPcie10BitIntf)
    mSignalIf = new CarbonXPcie10bitIf(mCfg, *mPhyRx);
  else {
    carbonInterfaceXtorPCIEIllegalInterfaceType(mCfg.mMsgContext, mCfg.mPrintName.c_str());
    return;
  }

  mPhyTx = new CarbonXPciePhyTx(mCfg, intf, mCfg.mLinkWidth, mCfg.mPrintName, 
                                mCfg.mCurrTimeString, *mSignalIf, mCfg.mMsgContext);
  
  mPhyTx->setLinkState(mLinkState);
}

CarbonXPciePhyLayer::~CarbonXPciePhyLayer() {
  
  // Delete Signal Interface
  delete mSignalIf;

  // Delete TX and RX
  delete mPhyTx;
  delete mPhyRx;
}

void CarbonXPciePhyLayer::reset() {
  mNextLinkState = Detect_Quiet;
  mLinkState     = Detect_Quiet;

  mSignalIf->reset();
  mPhyTx->reset();
  mPhyRx->reset();
}

void CarbonXPciePhyLayer::step() {
  mSignalIf->step();
  mPhyTx->step();
  mPhyRx->step();
  ltssm();
}

bool CarbonXPciePhyLayer::getPhysPacket(CarbonXPciePhyLayer::PhysPacket *pkt) 
{ 
  bool result = mPhyRx->getPhysPacket(pkt);
  return result;
}

bool CarbonXPciePhyLayer::getPhysPacket(CarbonXList<CarbonXPcieSym>* pkt) 
{ 
  bool result = mPhyRx->getPhysPacket(pkt);
  return result;
}

void CarbonXPciePhyLayer::setTxReqTransCBFn(CarbonXPciePhyLayer::TxReqTransCBFn *fn, void *info) {
  mPhyTx->setTxReqTransCBFn(fn, info);
}

bool CarbonXPciePhyLayer::hasPendingPacket() const {
  return mPhyTx->hasPendingPacket();
}

bool CarbonXPciePhyLayer::txPacketInProgress() const {
  return mPhyTx->packetInProgress();
}

bool CarbonXPciePhyLayer::txIsIdle(UInt32 cycles) const {
  return mPhyTx->isIdle(cycles);
}

bool CarbonXPciePhyLayer::txIsDone() const {
  return mPhyTx->isDone();
}

bool CarbonXPciePhyLayer::sendPacket(PCIETransaction *trans) {
  return mPhyTx->sendPacket(trans);
}

void CarbonXPciePhyLayer::powerSave() {
  mPhyTx->setLinkState(Tx_L0s_Entry);
}

void CarbonXPciePhyLayer::startTraining(UInt32 delay) {
  (void) delay;
  mPhyTx->setUpstreamDevice(true);
}

void CarbonXPciePhyLayer::setTrainingBypass(bool) {
  //mTrainingBypass = tf;
}

pcie_LTSM_state_t CarbonXPciePhyLayer::getLinkState() const {
  return mLinkState;
}

pcie_LTSM_state_t CarbonXPciePhyLayer::getTxLinkState() const {
  return mPhyTx->getLinkState();
}

pcie_LTSM_state_t CarbonXPciePhyLayer::getRxLinkState() const {
  return mPhyRx->getLinkState();
}

UInt32 CarbonXPciePhyLayer::getConsecIdles() {
  return mPhyRx->getConsecIdles();
}

void CarbonXPciePhyLayer::setScrambleEnable(bool tf)
{
  //mSignalIf->setScrambleEnable(tf);
  //mPhyTx->setTrainCtrl(~tf << 3);

  if(mCfg.mVerboseTransmitterLow || mCfg.mVerboseReceiverLow)
    printf("%s  Info: Scramble/Descramble of packets %s\n",mCfg.mPrintName.c_str(), (tf ? "Enabled" : "Disabled"));
}

bool CarbonXPciePhyLayer::getScrambleEnable()
{
  return mCfg.getScrambleEnable();
}

CarbonXPcieNet *CarbonXPciePhyLayer::getNet(CarbonXPcieSignalType sigType, UInt32 index) {
   return mSignalIf->getNet(sigType, index);
}

bool CarbonXPciePhyLayer::checkDataRateChange() const {
  return mLinkState == Recovery_Speed;
}

void CarbonXPciePhyLayer::ackDataRateChange() {
  mAckDataRateChange = true;
}

void CarbonXPciePhyLayer::setDataRateChangeCBFn(CarbonXPcieRateChangeCBFn *func, void *info) {
  mDataRateChCBFn   = func;
  mDataRateChCBInfo = info;
}

CarbonXPcieRateChangeCBFn * CarbonXPciePhyLayer::getDataRateChangeCBFn() {
  return mDataRateChCBFn;
}

void CarbonXPciePhyLayer::rcvdSkipOs() {
}
void CarbonXPciePhyLayer::rcvdEIdleOs() {
}
void CarbonXPciePhyLayer::rcvdFtsOs() {
}

void CarbonXPciePhyLayer::ltssm() {
  
  // Check Reset First
  if(mSignalIf->inReset()) {
    mNextLinkState = Detect_Quiet;
    if(mCfg.mVerboseTransmitterMedium) printf("%s Info: In LTSSM saw reset going to state Detect.Quiet. (at %s)\n", mCfg.mPrintName.c_str(), mCfg.mCurrTimeString.c_str());
  }
  else {
    //printf("%s  Info: In LTSSM: State = %d\n", mCfg.mPrintName.c_str(), mLinkState); 
    switch(mLinkState) {
    case Detect_Quiet :
      mNextLinkState = Detect_Active;
      break;
      
    case Detect_Active :
      if(mCfg.mUpstreamDevice || !mSignalIf->isEIdle()) {
	
	// If we should bypass training, go straight to L0
	if(mCfg.mTrainingBypass)
	  mNextLinkState = L0;
	else 
	  mNextLinkState = Polling_Active;
	
	mPhyRx->clearConsecTSs();
	mCurrSupportedDataRate = mInitSupportedDataRate;
	mPhyTx->setDataRate(mCurrSupportedDataRate);
	mSignalIf->transmitEIdle(false);
      }
      break;
      
    case Polling_Active :
      //     UtIO::cout() << mCfg.mPrintName << "  Info: In LTSSM State Polling.Active. TS = " 
      // 		 << mPhyRx->getLastTS() << " Consec TS = " << mPhyRx->getConsecTSs() << UtIO::endl;
    
      if(mPhyRx->getLastTS().checkTs1PadPad() && mPhyRx->getConsecTSs() > 1 && mPhyTx->getOsCount() > 15) {
	mNextLinkState = Polling_Config;
      }
      break;
      
    case Polling_Config :
      //     UtIO::cout() << mCfg.mPrintName << "  Info: In LTSSM State Polling.Config. TS = " 
      // 		 << mPhyRx->getLastTS() << " Consec TS = " << mPhyRx->getConsecTSs() << UtIO::endl;
      if(mPhyRx->getLastTS().checkTs2PadPad() && mPhyRx->getConsecTSs() > 1 && mPhyTx->getOsCount() > 15) {
	mNextLinkState = Config_Linkwidth_Start;
	mPhyRx->clearConsecTSs();
      }
      break;
      
    case Config_Linkwidth_Start :
      //     UtIO::cout() << mCfg.mPrintName << "  Info: In LTSSM State Config.Linkwidth.Start. TS = " 
      // 		 << mPhyRx->getLastTS() << " Consec TS = " << mPhyRx->getConsecTSs() << UtIO::endl;
      if(mCfg.mUpstreamDevice) {
	if(mPhyRx->getLastTS().checkTs1LinkPad(mLinkNum) && mPhyRx->getConsecTSs() > 1 && mPhyTx->getOsCount() > 15) {
	  mNextLinkState = Config_Linkwidth_Accept;
	  mPhyRx->clearConsecTSs();
	}
      }
      else {
	if(mPhyRx->getLastTS().checkTs1NotPadPad() && mPhyRx->getConsecTSs() > 1 && mPhyTx->getOsCount() > 15) {
	  mNextLinkState = Config_Linkwidth_Accept;
	  mPhyRx->clearConsecTSs();
	}
      }
      break;
      
    case Config_Linkwidth_Accept :
      //     UtIO::cout() << mCfg.mPrintName << "  Info: In LTSSM State Config.Linkwidth.Accept. TS = " 
      // 		 << mPhyRx->getLastTS() << " Consec TS = " << mPhyRx->getConsecTSs() << UtIO::endl;
      if(mCfg.mUpstreamDevice) {
	mNextLinkState = Config_Lanenum_Wait;
	mPhyRx->clearConsecTSs();
      }
      else /* if(mPhyRx->getLastTS().checkTs1LinkLane(mLinkNum)) */ {
	mNextLinkState = Config_Lanenum_Wait;
	mPhyRx->clearConsecTSs();
      }
      break;
      
    case Config_Lanenum_Wait :
      //     UtIO::cout() << mCfg.mPrintName << "  Info: In LTSSM State Config.Lanenum.Wait. TS = " 
      // 		 << mPhyRx->getLastTS() << " Consec TS = " << mPhyRx->getConsecTSs() << UtIO::endl;
      if(mCfg.mUpstreamDevice) {
	if(mPhyRx->getLastTS().checkTs1LinkLane(mLinkNum) && mPhyRx->getConsecTSs() > 1 && mPhyTx->getOsCount() > 15) {
	  mNextLinkState = Config_Lanenum_Accept;
	  mPhyRx->clearConsecTSs();
	}
      }
      else if( (mPhyRx->getLastTS().checkTs1LinkLane(mLinkNum) || mPhyRx->getLastTS().checkTs2LinkLane(mLinkNum)) && mPhyRx->getConsecTSs() > 1 && mPhyTx->getOsCount() > 15) {
	mNextLinkState = Config_Lanenum_Accept;
	mPhyRx->clearConsecTSs();
      }
      break;
      
    case Config_Lanenum_Accept :
      //     UtIO::cout() << mCfg.mPrintName << "  Info: In LTSSM State Config.Lanenum.Accept. TS = " 
      // 		 << mPhyRx->getLastTS() << " Consec TS = " << mPhyRx->getConsecTSs() << UtIO::endl;
      if(mCfg.mUpstreamDevice) {
	if(mPhyRx->getLastTS().checkTs1LinkLane(mLinkNum) && mPhyRx->getConsecTSs() > 1 && mPhyTx->getOsCount() > 15) {
	  mNextLinkState = Config_Complete;
	  mPhyRx->clearConsecTSs();
	}
      }
      else if(mPhyRx->getLastTS().checkTs2LinkLane(mLinkNum) && mPhyRx->getConsecTSs() > 1 && mPhyTx->getOsCount() > 15) {
	mNextLinkState = Config_Complete;
	mPhyRx->clearConsecTSs();
      }
      break;
      
    case Config_Complete :
      if(mPhyRx->getLastTS().checkTs2LinkLane(mLinkNum) && mPhyRx->getConsecTSs() > 7 && mPhyTx->getOsCount() > 15) {
	
	// Record Requested Data Rate from other side
	mRequestedDataRate  = mPhyRx->getLastTS().getDataRate(0);

	// Negotiated Data Rate is Data Rates that is supported by both us and the other side.
	mNegotiatedDataRate = mRequestedDataRate & mCurrSupportedDataRate;
	
	mNextLinkState = Config_Idle;
	mPhyTx->clearOsCount();
	mPhyRx->clearConsecTSs();
      }
      break;
    
    case Config_Idle :
      //UtIO::cout() << mPrintName << "  Info: In LTSSM State Config.Idle. Consec Idles = " << mPhyRx->getConsecIdles() << UtIO::endl;
      if(mPhyRx->getConsecIdles() > 7 && mPhyTx->getOsCount() >= 16) {
	mNextLinkState = L0;
	mPhyRx->clearConsecTSs();
      }
      break;
      
    case Recovery_RcvrLock :
      //UtIO::cout() << mPrintName << " In Recovery.RcvrLock: Have Gotten " << mPhyRx->getConsecTSs() << " of " << mPhyRx->getLastTS() << UtIO::endl;
      if(mPhyRx->getLastTS().checkTs1LinkLane(mLinkNum) && mPhyRx->getConsecTSs() > 7) {

	// If DirectedSpeedChange is set wait until we receive TS's
	// with the change rate bit set until transitioning to Recovery_RcvrCfg
	if( (mDirectedSpeedChange << 7) == (mPhyRx->getLastTS().getDataRate(0) & 0x80)) {

	  //if(mPhyRx->getLastTS().getDataRate(0) & 0x80) { // Look for Data Rate Change Bit
	    mNextLinkState = Recovery_RcvrCfg;
	    mPhyRx->clearConsecTSs();
	    //}
	}
	else {
	  // If we see the Data Rate Change Bit and we didn't
	  // initiate the speed change, set DirectedSpeedchange abd start sending
	  // TS's with the change speed bit set
	  if(mPhyRx->getLastTS().getDataRate(0) & 0x80) { // Look for Data Rate Change Bit
	    mDirectedSpeedChange = true;
	    mPhyTx->setDataRate(mCurrSupportedDataRate | 0x80);
	  }
	  
	  //mNextLinkState = Recovery_RcvrCfg;
	  mPhyRx->clearConsecTSs();
	}
      }
      break;
      
    case Recovery_RcvrCfg :
      if(mPhyRx->getLastTS().checkTs2LinkLane(mLinkNum) && mPhyRx->getConsecTSs() > 7) {
	if(mPhyRx->getLastTS().getDataRate(0) & 0x80) { // Look for Data Rate Change Bit
	  mNextLinkState = Recovery_Speed;
	  mAckDataRateChange = false;
	  mPhyRx->clearConsecTSs(); 
	}
	else {
	  mNextLinkState = Recovery_Idle;
	  mPhyRx->clearConsecTSs();
	}
      }
      break;
      
    case Recovery_Speed :
      // First Wait intil we see Electrical Idles on the receiver
      if(mSignalIf->isEIdle() && !mDataRateChangeRequested ) {
	changeDataRate(); // Request the external environment to change the clock rate
      }
      
      // Once the external Environment has acknowledged the speed change, go to Recovery_RcvrLock
      if(mAckDataRateChange) {
	mPhyTx->setDataRate(mCurrSupportedDataRate); // Turn off the data rate change bit
	mDirectedSpeedChange = false; // We Changed speed already so reset Directed Speed Change
	mNextLinkState = Recovery_RcvrLock;
      }
      break;
      
    case Recovery_Idle :
      if(mPhyRx->getConsecIdles() > 7) {
	mNextLinkState = L0;
	mPhyRx->clearConsecTSs();
      }
      break;
      
    case L0 :
      // Check to See if we should change data rate
      //UtIO::cout() << "Negotiated Rate = " << mNegotiatedDataRate << " Data Rate = " << mDataRate << UtIO::endl;
      if(mCfg.mUpstreamDevice && isDataRateLarger(mNegotiatedDataRate, mDataRate)) {
	mDirectedSpeedChange = true; 
	mPhyTx->setDataRate(mCurrSupportedDataRate | 0x80);
	mNextLinkState       = Recovery_RcvrLock;
      }
      else if(mPhyRx->getConsecTSs() > 1) { // We Received TSs in L0, go to recovery
	mNextLinkState       = Recovery_RcvrLock;
      }
      
      break;
    default :
      mNextLinkState = Polling_Active;
      mPhyRx->clearConsecTSs();
    }

  }

  // Now Update State
  if(mNextLinkState != mLinkState) {
    
    if(mCfg.mVerboseReceiverMedium) 
      printf("%s Info: LTSSM Transitioning from State %s to State %s (at %s).\n", mCfg.mPrintName.c_str(), getLTSSMStateString(mLinkState), getLTSSMStateString(mNextLinkState), mCfg.mCurrTimeString.c_str()); 
    
    mLinkState = mNextLinkState;
    
    mPhyTx->setLinkState(mLinkState);
    mPhyRx->setLinkState(mLinkState);
    
    if(mLinkState == L0) {
      if(mCfg.mVerboseReceiverLow) {
        UtOStream* ostr = carbonInterfaceGetCout();
        carbonInterfaceWriteStringToOStream(ostr, mCfg.mPrintName.c_str());
        carbonInterfaceWriteStringToOStream(ostr, "Info: Physical Link is now up (at ");
        carbonInterfaceWriteStringToOStream(ostr, mCfg.mCurrTimeString.c_str());
        carbonInterfaceWriteStringToOStream(ostr, ").");
        carbonInterfaceOStreamEndl(ostr);
      }
    }
  }
}

void CarbonXPciePhyLayer::changeDataRate() {
  mDataRate                = mNegotiatedDataRate;
  mAckDataRateChange       = false;
  mDataRateChangeRequested = true;
  if(mDataRateChCBFn) mDataRateChCBFn(mDataRateChCBInfo, mNegotiatedDataRate >> 1);
}

bool CarbonXPciePhyLayer::isDataRateLarger(UInt8 first, UInt8 second) {
  return (first & 0x7) > (second & 0x7);  
}

const char* CarbonXPciePhyLayer::getLTSSMStateString(pcie_LTSM_state_t sm) {
  switch(sm) {
  case  Detect_Quiet :
    return("Detect.Quiet");
    break;
  case  Detect_Active :
    return("Detect.Active");
    break;
  case  Polling_Active :
    return("Polling.Active");
    break;
  case  Polling_Config :
    return("Polling.Config");
    break;
  case  Polling_Compliance :
    return("Polling.Compliance");
    break;
  case  Polling_Speed :
    return("Polling.Speed");
    break;
  case  Config_Linkwidth_Start :
    return("Config.Linkwidth.Start");
    break;
  case  Config_Linkwidth_Accept :
    return("Config.Linkwidth.Accept");
    break;
  case  Config_Lanenum_Wait :
    return("Config.Lanenum.Wait");
    break;
  case  Config_Lanenum_Accept :
    return("Config.Lanenum.Accept");
    break;
  case  Config_Complete :
    return("Config.Complete");
    break;
  case  Config_Idle :
    return("Config.Idle");
    break;
  case  Recovery_RcvrLock :
    return("Recovery.RcvrLock");
    break;
  case  Recovery_RcvrCfg :
    return("Recovery.RcvrCfg");
    break;
  case  Recovery_Speed :
    return("Recovery.Speed");
    break;
  case  Recovery_Idle :
    return("Recovery.Idle");
    break;
  case  L1_Entry :
    return("L1.Entry");
    break;
  case  L1_Idle :
    return("L1.Idle");
    break;
  case  L23Ready :
    return("L2/3 Ready");
    break;
  case  L0 :
    return("L0");
    break;
  case  Tx_L0s_Entry :
    return("Tx_L0s.Entry");
    break;
  case  Tx_L0s_Idle :
    return("Tx_L0s.Idle");
    break;
  case  Tx_L0s_Fts :
    return("Tx_L0s.Fts");
    break;
  case  Rx_L0s_Entry :
    return("Rx_L0s.Entry");
    break;
  case  Rx_L0s_Idle :
    return("Rx_L0s.Idle");
    break;
  case  Rx_L0s_Fts :
    return("Rx_L0s.Ft");
    break;
  default:
    return("ERROR");
  }
}
