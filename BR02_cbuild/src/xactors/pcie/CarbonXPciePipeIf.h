// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CarbonXPciePipeIf_H_
#define __CarbonXPciePipeIf_H_

#include "util/c_memmanager.h"
#include "CarbonXPcieIfBase.h"
#include "carbonXPcie.h"
#include "CarbonXPcieNet.h"
#include "CarbonXPcieScrambler.h"
#include "CarbonX8b10b.h"

class MsgContext;
class CarbonXPcieConfig;
class CarbonXPciePhyRx;

class CarbonXPciePipeIf : public CarbonXPcieIfBase {
public :  CARBONMEM_OVERRIDES;

  //! Constructor
  CarbonXPciePipeIf(CarbonXPcieConfig& cfg, CarbonXPciePhyRx& phyRx);

  //! Destructor
  virtual ~CarbonXPciePipeIf();

  //! Reset
  void reset();

  //! Return pointer to the net object for the specified signal
  CarbonXPcieNet * getNet(CarbonXPcieSignalType sigType, UInt32 index);
  
  // Step function for anything in the interface that has to executed every symbol cycle
  void step();

  // Output Data on the interface
  void transmitData(const UInt32 *data, const bool *sym, bool scramble = true);
  void transmitLaneData(UInt32 data, bool sym, UInt32 index, bool scramble = true);
  void transmitIdleData(void);
  void transmitEIdle(bool on_off);

  // Read data from the pin interface
  void receiveData(UInt32 *data, bool *sym);
  bool isEIdle();
  bool inReset();
  
private:

  void setTransmitPins(CarbonXPcieNet** netVect, UInt32 index, UInt32 value);
  void setTransmitPins(CarbonXPcieNet* net, UInt32 value);
  void setTransmitLos(UInt32 index, UInt32 value);
  inline void setTransmitData(UInt32 idx, UInt32 val)     { setTransmitPins(mSignalRxData, idx, val); }
  inline void setTransmitKCode(UInt32 idx, UInt32 val)    { setTransmitPins(mSignalRxDataK, idx, val); }
  inline void setTransmitRxStatus(UInt32 idx, UInt32 val) { setTransmitPins(mSignalRxStatus, idx, val); }
  inline void setTransmitRxValid(UInt32 idx, UInt32 val)  { setTransmitPins(mSignalRxValid, idx, val); }
  inline void setTransmitPhyStatus(UInt32 val)            { setTransmitPins(&mSignalPhyStatus, val); }

  UInt32 getReceivePins(CarbonXPcieNet **netVect, UInt32 index);
  UInt32 getReceivePins(CarbonXPcieNet* net);
  UInt32 getReceiveLos(UInt32 index);
  inline UInt32 getReceiveData(UInt32 idx)         { return getReceivePins(mSignalTxData, idx); }
  inline UInt32 getReceiveKCode(UInt32 idx)        { return getReceivePins(mSignalTxDataK, idx); }
  inline UInt32 getReceiveTxCompliance(UInt32 idx) { return getReceivePins(mSignalTxCompliance, idx); }
  inline UInt32 getReceiveRxPolarity(UInt32 idx)   { return getReceivePins(mSignalRxPolarity, idx); }
  inline UInt32 getReceiveTxDetectRx(void)         { return getReceivePins(&mSignalTxDetectRx); }
  inline UInt32 getReceiveResetN(void)             { return getReceivePins(&mSignalResetN); }
  inline UInt32 getReceivePowerDown(void)          { return getReceivePins(&mSignalPowerDown); }

  void checkTxDetectRx(void);
  void checkReset();

  const char* getSymStringFromByte(UInt32);
  const char* getStringFromPCIESignalType(CarbonXPcieSignalType sigType);  

  // Construction Parameters
  CarbonXPcieConfig& mCfg;
  CarbonXPciePhyRx&  mPhyRx;

  // Transmit Data Interface Signals (Inputs to transactor)
  CarbonXPcieNet **            mSignalTxDataK; 
  CarbonXPcieNet **            mSignalTxData;

  // Receive Data Interface Signals (Outputs from transactor)
  CarbonXPcieNet **            mSignalRxData;
  CarbonXPcieNet **            mSignalRxDataK;

  // Command Interface Signals (Inputs to transactor)
  CarbonXPcieNet               mSignalTxDetectRx;
  CarbonXPcieNet **            mSignalTxEIdle;
  CarbonXPcieNet **            mSignalTxCompliance;
  CarbonXPcieNet **            mSignalRxPolarity;
  CarbonXPcieNet               mSignalResetN;
  CarbonXPcieNet               mSignalPowerDown;
  
  // Status Interface Signals (Outputs from transactor)
  CarbonXPcieNet **            mSignalRxEIdle;
  CarbonXPcieNet **            mSignalRxStatus;
  CarbonXPcieNet **            mSignalRxValid;
  CarbonXPcieNet               mSignalPhyStatus;
  
  CarbonXPcieScrambler         mScrambler;
  UInt32                       mDetectRxTimer;
  bool                         mInReset;
  UInt32                       mResetTimer;
  bool                         mTxEIdle;
};

#endif

