/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "CarbonXPcie10bitIf.h"
#include "CarbonXPcieCommonTypes.h"
#include "CarbonXPciePhyRx.h"
#include "shell/carbon_interface_xtor.h"
#include <cstdio>
#include <cstring>

CarbonXPcie10bitIf::CarbonXPcie10bitIf(CarbonXPcieConfig& cfg, CarbonXPciePhyRx& phyRx) :
  mCfg(cfg),
  mPhyRx(phyRx),
  mScrambler(mCfg.mLinkWidth),
  mDeScrambler(mCfg.mLinkWidth)
{
  // Creating Signal Ports
  mSignalRxData       = CARBON_ALLOC_VEC(CarbonXPcieNet*, mCfg.mLinkWidth); 
  mSignalTxData       = CARBON_ALLOC_VEC(CarbonXPcieNet*, mCfg.mLinkWidth); 
  mSignalRxEIdle      = CARBON_ALLOC_VEC(CarbonXPcieNet*, mCfg.mLinkWidth); 
  mSignalTxEIdle      = CARBON_ALLOC_VEC(CarbonXPcieNet*, mCfg.mLinkWidth); 
  
  for(UInt32 i = 0; i < mCfg.mLinkWidth; i++) {
    mSignalTxData[i]       = new CarbonXPcieNet(CarbonXPcieNet::eInput,  getStringFromPCIESignalType(eCarbonXPcieTxData)); 
    mSignalRxData[i]       = new CarbonXPcieNet(CarbonXPcieNet::eOutput, getStringFromPCIESignalType(eCarbonXPcieRxData)); 
    mSignalTxEIdle[i]      = new CarbonXPcieNet(CarbonXPcieNet::eInput,  getStringFromPCIESignalType(eCarbonXPcieTxElecIdle));
    mSignalRxEIdle[i]      = new CarbonXPcieNet(CarbonXPcieNet::eOutput, getStringFromPCIESignalType(eCarbonXPcieRxElecIdle)); 
  }

  // Setup 8b/10b Encoder/Decoder
  m8b10b = new CarbonX8b10b[mCfg.mLinkWidth];

  // Reset Signal Interface
  reset();
}

CarbonXPcie10bitIf::~CarbonXPcie10bitIf() {
  for(UInt32 i = 0; i < mCfg.mLinkWidth; i++) {
    delete mSignalRxData[i];
    delete mSignalTxData[i];
    delete mSignalRxEIdle[i];
    delete mSignalTxEIdle[i];
  }
  CARBON_FREE_VEC(mSignalRxData,  CarbonXPcieNet*, mCfg.mLinkWidth);
  CARBON_FREE_VEC(mSignalTxData,  CarbonXPcieNet*, mCfg.mLinkWidth);
  CARBON_FREE_VEC(mSignalRxEIdle, CarbonXPcieNet*, mCfg.mLinkWidth);
  CARBON_FREE_VEC(mSignalTxEIdle, CarbonXPcieNet*, mCfg.mLinkWidth);

  delete [] m8b10b;
}

void CarbonXPcie10bitIf::step() {

  // Put Data into the receive buffer
  CarbonXPcieCycleData cycData;
  for(UInt32 i = 0; i < mCfg.mLinkWidth; i++) {
    UInt32 data = 0;
    bool   sym  = false;
    
    if(!getReceiveLos(i)) { // Don't bother decoding when in Electrical Idle
      bool err = false;
      data = m8b10b[i].decoder_lut(getReceiveData(i), &sym, &err);
      if(err) {
        carbonInterfaceXtorPCIEInvalid10BitSymbol(mCfg.mMsgContext, mCfg.mPrintName.c_str(), cycData.mData[i].mData, m8b10b[i].getInputDisparity(), 
                                                 i, mCfg.mCurrTimeString.c_str(), getReceiveLos(i));
      }
    }
    
    cycData.mData[i]     = CarbonXPcieSym(data, sym, mCfg.mCurrTick);
    cycData.mElecIdle[i] = getReceiveLos(i);
  }
  mPhyRx.putCycleData(cycData);
}
  
CarbonXPcieNet *CarbonXPcie10bitIf::getNet(CarbonXPcieSignalType sigType, UInt32 index) {

   CarbonXPcieNet *net = NULL;
   
   if (index >= mCfg.mLinkWidth) {
     carbonInterfaceXtorPCIEInvalidLinkIndex(mCfg.mMsgContext, mCfg.mPrintName.c_str(), index, (mCfg.mLinkWidth-1));
     return false;
   }
   
   switch(sigType) {
   case eCarbonXPcieRxData: // DUT RxData, BFM TxData
     //printf("Found match for %s\n", getStringFromPCIESignalType(sigType));
     net = mSignalRxData[index];
     break;
     
   case eCarbonXPcieRxElecIdle: // DUT RxElecIdle, BFM TxElecIdle
     //printf("Found match for %s\n", getStringFromPCIESignalType(sigType));
     net = mSignalRxEIdle[index];
     break;

   case eCarbonXPcieClock:
     carbonInterfaceXtorPCIEInvalidClockNet(mCfg.mMsgContext, mCfg.mPrintName.c_str(), getStringFromPCIESignalType(sigType));
      break;

   case eCarbonXPcieTxData: // DUT TxData, BFM RxData
     net = mSignalTxData[index];
      break;

   case eCarbonXPcieTxElecIdle: // DUT TxElecIdle, BFM RxElecIdle
     net = mSignalTxEIdle[index];
     break;

   default:
      carbonInterfaceXtorPCIEInvalid10BitSignal(mCfg.mMsgContext, mCfg.mPrintName.c_str(), getStringFromPCIESignalType(sigType));
   }
   return net;
}

void CarbonXPcie10bitIf::transmitIdleData(void) {
  
  if (mCfg.mVerboseTransmitterHigh) {
    printf("%s  Info: Transmitting Idle data: \n", mCfg.mPrintName.c_str());
  }
  
  // put idle data on bus
  for (UInt32 i=0; i < mCfg.mLinkWidth; ++i) {
    transmitLaneData(0, false, i, true);
  }
}

void CarbonXPcie10bitIf::transmitData(const UInt32 *data, const bool *sym, bool scramble) {
    
  if ( mCfg.mVerboseTransmitterHigh) {
    printf("%s  Info: Transmitting data: ", mCfg.mPrintName.c_str());
    for (UInt32 i=0; i < mCfg.mLinkWidth; ++i) {
      if (sym[i])
	printf("  %s", CarbonXPcieIfBase::getSymStringFromByte(data[i]));
      else
	printf("  %02X", data[i]);
    }
    printf("   (at %s)\n", mCfg.mCurrTimeString.c_str());
  }
  
  for (UInt32 i=0; i < mCfg.mLinkWidth; ++i)
    transmitLaneData(data[i], sym[i], i, scramble);
 }

void CarbonXPcie10bitIf::transmitLaneData(UInt32 data, bool sym, UInt32 index, bool scramble) {

  // Don't do anything when in EIdle
  UInt32 eIdle;
  mSignalRxEIdle[index]->examine(&eIdle);

  if( !(eIdle ^ mCfg.mEIdleActiveHigh)) return;

  // Mask Off any bits that don't belong
  data &= 0xff;
  
  // Scramble Data. The scrambler should always be called
  // even if we are not outputing scramble data (Unless Scrambling is turned off).
  // Otherwise the scrambler gets out of sync
  if(mCfg.mScrambleEnable) {
    UInt32 temp = mScrambler(data, sym, index);
    if(scramble) data = temp;
  }

  data = m8b10b[index].encoder_lut(data, sym);
  setTransmitData(index, data);
}

void CarbonXPcie10bitIf::transmitEIdle(bool on_off) {
  for(UInt32 i = 0; i < mCfg.mLinkWidth; i++)
    setTransmitLos(i, on_off);
}

// Read data from the pin interface
void CarbonXPcie10bitIf::receiveData(UInt32 *data, bool *sym) {

  // Check For Electrical Idle, Check Lane 0 and assume that if that all
  // others follow, for performance reasons
  if(isEIdle()) return;

  // get packets out of the chip
  for(UInt32 i=0; i < mCfg.mLinkWidth; ++i) {
    bool err;
    data[i] = m8b10b[i].decoder_lut(getReceiveData(i), &(sym[i]), &err);
    if(err) {
      if(getReceiveLos(i)) // Don't report bad symbol when in Electrical Idle
        carbonInterfaceXtorPCIEInvalid10BitSymbol(mCfg.mMsgContext, mCfg.mPrintName.c_str(), data[i], m8b10b[i].getInputDisparity(), i, mCfg.mCurrTimeString.c_str(), getReceiveLos(i));
    }
  }
}

bool CarbonXPcie10bitIf::isEIdle() {
  return getReceiveLos(0);
}

void CarbonXPcie10bitIf::setTransmitPins(CarbonXPcieNet* net, UInt32 value) {
   net->deposit(&value);
}

void CarbonXPcie10bitIf::setTransmitPins(CarbonXPcieNet **netVect, UInt32 index, UInt32 value) {
   if (index < mCfg.mLinkWidth)
      netVect[index]->deposit(&value);
   else
     carbonInterfaceXtorPCIEInvalidLinkIndex(mCfg.mMsgContext, mCfg.mPrintName.c_str(), index, (mCfg.mLinkWidth-1));
}

void CarbonXPcie10bitIf::setTransmitLos(UInt32 index, UInt32 value) {
  UInt32 temp = !(mCfg.mEIdleActiveHigh ^ value);
  setTransmitPins(mSignalRxEIdle, index, temp);

  // When in Electrical Idle, also output 0's on the data output.
  if(value) setTransmitData(index, 0);
}

UInt32 CarbonXPcie10bitIf::getReceivePins(CarbonXPcieNet* net) {
   UInt32 value = 0;
   net->examine(&value);
   return value;
}

UInt32 CarbonXPcie10bitIf::getReceivePins(CarbonXPcieNet **netVect, UInt32 index) {
   UInt32 value = 0;
   if (index < mCfg.mLinkWidth)
      netVect[index]->examine(&value);
   else
     carbonInterfaceXtorPCIEInvalidLinkIndex(mCfg.mMsgContext, mCfg.mPrintName.c_str(), index, (mCfg.mLinkWidth-1));
   return value;
}

UInt32 CarbonXPcie10bitIf::getReceiveLos(UInt32 index) {
   UInt32 value = getReceivePins(mSignalTxEIdle, index);
   
   // Return value is active high, we need to account for the design's use of the signal
   // also look to see of the electric idle is forced inactive
   return !(mCfg.mEIdleActiveHigh ^ value);
}

const char* CarbonXPcie10bitIf::getStringFromPCIESignalType(CarbonXPcieSignalType sigType) {
   switch(sigType) {
   case eCarbonXPcieClock:
      return "eCarbonXPcieClock";
   case eCarbonXPciePhyStatus:
      return "eCarbonXPciePhyStatus";
   case eCarbonXPciePowerDown:
      return "eCarbonXPciePowerDown";
   case eCarbonXPcieResetN:
      return "eCarbonXPcieResetN";
   case eCarbonXPcieRxData:
      return "eCarbonXPcieRxData";
   case eCarbonXPcieRxDataK:
      return "eCarbonXPciRxDataK";
   case eCarbonXPcieRxElecIdle:
      return "eCarbonXPcieRxElecIdle";
   case eCarbonXPcieRxPolarity:
      return "eCarbonXPcieRxPolarity";
   case eCarbonXPcieRxStatus:
      return "eCarbonXPcieRxStatus";
   case eCarbonXPcieRxValid:
      return "eCarbonXPcieRxValid";
   case eCarbonXPcieTxCompliance:
      return "eCarbonXPcieTxCompliance";
   case eCarbonXPcieTxData:
      return "eCarbonXPcieTxData";
   case eCarbonXPcieTxDataK:
      return "eCarbonXPcieTxDataK";
   case eCarbonXPcieTxDetectRx:
      return "eCarbonXPcieTxDetectRx";
   case eCarbonXPcieTxElecIdle:
      return "eCarbonXPcieTxElecIdle";
   default:
      return "UNKNOWN";
   }
}

  
void CarbonXPcie10bitIf::reset() {
  transmitEIdle(true);
  mScrambler.reset();
  mDeScrambler.reset();
}
