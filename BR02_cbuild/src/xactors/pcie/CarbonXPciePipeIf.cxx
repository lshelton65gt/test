/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "CarbonXPciePipeIf.h"
#include "CarbonXPcieCommonTypes.h"
#include "CarbonXPciePhyRx.h"
#include "shell/carbon_interface_xtor.h"
#include "util/UtIOStream.h"
#include <cstdio>

CarbonXPciePipeIf::CarbonXPciePipeIf(CarbonXPcieConfig& cfg, CarbonXPciePhyRx& phyRx) :
  mCfg(cfg),
  mPhyRx(phyRx),
  mSignalTxDetectRx(CarbonXPcieNet::eInput, getStringFromPCIESignalType(eCarbonXPcieTxDetectRx)),
  mSignalResetN(CarbonXPcieNet::eInput,     getStringFromPCIESignalType(eCarbonXPcieResetN)), 
  mSignalPowerDown(CarbonXPcieNet::eInput,  getStringFromPCIESignalType(eCarbonXPciePowerDown)), 
  mSignalPhyStatus(CarbonXPcieNet::eOutput, getStringFromPCIESignalType(eCarbonXPciePhyStatus)),
  mScrambler(mCfg.mLinkWidth),
  mDetectRxTimer(0),
  mInReset(false), // Start out in reset
  mResetTimer(0),  // Start out in reset
  mTxEIdle(false)
{
  // Creating Signal Ports
  mSignalRxData       = CARBON_ALLOC_VEC(CarbonXPcieNet*, mCfg.mLinkWidth);  
  mSignalTxData       = CARBON_ALLOC_VEC(CarbonXPcieNet*, mCfg.mLinkWidth); 
  mSignalRxEIdle      = CARBON_ALLOC_VEC(CarbonXPcieNet*, mCfg.mLinkWidth); 
  mSignalTxEIdle      = CARBON_ALLOC_VEC(CarbonXPcieNet*, mCfg.mLinkWidth); 
  mSignalRxDataK      = CARBON_ALLOC_VEC(CarbonXPcieNet*, mCfg.mLinkWidth); 
  mSignalTxCompliance = CARBON_ALLOC_VEC(CarbonXPcieNet*, mCfg.mLinkWidth); 
  mSignalRxPolarity   = CARBON_ALLOC_VEC(CarbonXPcieNet*, mCfg.mLinkWidth); 
  mSignalTxDataK      = CARBON_ALLOC_VEC(CarbonXPcieNet*, mCfg.mLinkWidth); 
  mSignalRxStatus     = CARBON_ALLOC_VEC(CarbonXPcieNet*, mCfg.mLinkWidth); 
  mSignalRxValid      = CARBON_ALLOC_VEC(CarbonXPcieNet*, mCfg.mLinkWidth); 
  
  for(UInt32 i = 0; i < mCfg.mLinkWidth; i++) {
    mSignalTxData[i]       = new CarbonXPcieNet(CarbonXPcieNet::eInput,  getStringFromPCIESignalType(eCarbonXPcieTxData)); 
    mSignalTxDataK[i]      = new CarbonXPcieNet(CarbonXPcieNet::eInput,  getStringFromPCIESignalType(eCarbonXPcieTxDataK)); 
 
    mSignalRxData[i]       = new CarbonXPcieNet(CarbonXPcieNet::eOutput, getStringFromPCIESignalType(eCarbonXPcieRxData)); 
    mSignalRxDataK [i]     = new CarbonXPcieNet(CarbonXPcieNet::eOutput, getStringFromPCIESignalType(eCarbonXPcieRxDataK)); 

    mSignalTxEIdle[i]      = new CarbonXPcieNet(CarbonXPcieNet::eInput,  getStringFromPCIESignalType(eCarbonXPcieTxElecIdle));
    mSignalTxCompliance[i] = new CarbonXPcieNet(CarbonXPcieNet::eInput,  getStringFromPCIESignalType(eCarbonXPcieTxCompliance)); 
    mSignalRxPolarity[i]   = new CarbonXPcieNet(CarbonXPcieNet::eInput,  getStringFromPCIESignalType(eCarbonXPcieRxPolarity)); 

    mSignalRxEIdle[i]      = new CarbonXPcieNet(CarbonXPcieNet::eOutput, getStringFromPCIESignalType(eCarbonXPcieRxElecIdle)); 
    mSignalRxStatus[i]     = new CarbonXPcieNet(CarbonXPcieNet::eOutput, getStringFromPCIESignalType(eCarbonXPcieRxStatus)); 
    mSignalRxValid[i]      = new CarbonXPcieNet(CarbonXPcieNet::eOutput, getStringFromPCIESignalType(eCarbonXPcieRxValid)); 
  }

  // Start out in electrical idle mode
  transmitEIdle(true);
}

CarbonXPciePipeIf::~CarbonXPciePipeIf() {
  for(UInt32 i = 0; i < mCfg.mLinkWidth; i++) {
    delete mSignalRxData[i];
    delete mSignalTxData[i];
    delete mSignalRxEIdle[i];
    delete mSignalTxEIdle[i];
    delete mSignalRxDataK [i];
    delete mSignalTxCompliance[i];
    delete mSignalRxPolarity[i];
    delete mSignalTxDataK[i];
    delete mSignalRxStatus[i];
    delete mSignalRxValid[i];
  }
  CARBON_FREE_VEC(mSignalRxData,       CarbonXPcieNet *, mCfg.mLinkWidth);
  CARBON_FREE_VEC(mSignalTxData,       CarbonXPcieNet *, mCfg.mLinkWidth);
  CARBON_FREE_VEC(mSignalRxEIdle,      CarbonXPcieNet *, mCfg.mLinkWidth);
  CARBON_FREE_VEC(mSignalTxEIdle,      CarbonXPcieNet *, mCfg.mLinkWidth);
  CARBON_FREE_VEC(mSignalRxDataK,      CarbonXPcieNet *, mCfg.mLinkWidth);
  CARBON_FREE_VEC(mSignalTxCompliance, CarbonXPcieNet *, mCfg.mLinkWidth);
  CARBON_FREE_VEC(mSignalRxPolarity,   CarbonXPcieNet *, mCfg.mLinkWidth);
  CARBON_FREE_VEC(mSignalTxDataK,      CarbonXPcieNet *, mCfg.mLinkWidth);
  CARBON_FREE_VEC(mSignalRxStatus,     CarbonXPcieNet *, mCfg.mLinkWidth);
  CARBON_FREE_VEC(mSignalRxValid,      CarbonXPcieNet *, mCfg.mLinkWidth);
}

CarbonXPcieNet *CarbonXPciePipeIf::getNet(CarbonXPcieSignalType sigType, UInt32 index) {

  CarbonXPcieNet *net = NULL;
  
  if (index >= mCfg.mLinkWidth) {
    carbonInterfaceXtorPCIEInvalidLinkIndex(mCfg.mMsgContext, mCfg.mPrintName.c_str(), index, (mCfg.mLinkWidth-1));
    return false;
   }
  
  switch(sigType) {
  case eCarbonXPciePhyStatus:
    if (index == 0) {
      net = &mSignalPhyStatus;
    } else {
      carbonInterfaceXtorPCIEInvalidIndex(mCfg.mMsgContext, mCfg.mPrintName.c_str(), getStringFromPCIESignalType(sigType), index);
    }
    break;
    
  case eCarbonXPcieRxData: // DUT RxData, BFM TxData
    net = mSignalRxData[index];
    break;
    
  case eCarbonXPcieRxElecIdle: // DUT RxElecIdle, BFM TxElecIdle
    net = mSignalRxEIdle[index];
    break;
    
  case eCarbonXPcieRxDataK: // DUT RxDataK, BFM TxDataK
     net = mSignalRxDataK[index];
     break;
     
  case eCarbonXPcieRxValid: // DUT RxValid, BFM TxValid
    net = mSignalRxValid[index];
    break;

  case eCarbonXPcieRxStatus: // DUT RxStatus, BFM TxStatus
    net = mSignalRxStatus[index];
    break;
  case eCarbonXPcieClock:
    carbonInterfaceXtorPCIEInvalidClockNet(mCfg.mMsgContext, mCfg.mPrintName.c_str(), getStringFromPCIESignalType(sigType));
    break;
    
  case eCarbonXPciePowerDown:
    if (index == 0) {
      net = &mSignalPowerDown;
    }
    else {
      carbonInterfaceXtorPCIEInvalidIndex(mCfg.mMsgContext, mCfg.mPrintName.c_str(), getStringFromPCIESignalType(sigType), index);
    }
    break;

  case eCarbonXPcieResetN:
    if (index == 0) {
      net = &mSignalResetN;
    }
    else {
      carbonInterfaceXtorPCIEInvalidIndex(mCfg.mMsgContext, mCfg.mPrintName.c_str(), getStringFromPCIESignalType(sigType), index);
    }
    break;

  case eCarbonXPcieTxDetectRx:
    if (index == 0) {
      net = &mSignalTxDetectRx;
    }
    else {
      carbonInterfaceXtorPCIEInvalidIndex(mCfg.mMsgContext, mCfg.mPrintName.c_str(), getStringFromPCIESignalType(sigType), index);
    }
    break;

  case eCarbonXPcieTxData: // DUT TxData, BFM RxData
    net = mSignalTxData[index];
    break;

  case eCarbonXPcieTxElecIdle: // DUT TxElecIdle, BFM RxElecIdle
    net = mSignalTxEIdle[index];
    break;

  case eCarbonXPcieTxDataK: // DUT TxDataK, BFM RxDataK
    net = mSignalTxDataK[index];
    break;

   case eCarbonXPcieTxCompliance: // DUT TxCompliance, BFM RxCompliance
     net = mSignalTxCompliance[index];
     break;

   case eCarbonXPcieRxPolarity: // DUT RxPolarity, BFM RxPolarity
     net = mSignalRxPolarity[index];
     break;

   default:
     carbonInterfaceXtorPCIEInvalidPipeSignal(mCfg.mMsgContext, mCfg.mPrintName.c_str(), getStringFromPCIESignalType(sigType));
   }
   return net;
}

void CarbonXPciePipeIf::step() {
  checkTxDetectRx();
  checkReset();

  // Put Data into the receive buffer
  if(!mInReset) {
    CarbonXPcieCycleData cycData;
    for(UInt32 i = 0; i < mCfg.mLinkWidth; i++) {
      cycData.mData[i]     = CarbonXPcieSym(getReceiveData(i), getReceiveKCode(i), mCfg.mCurrTick);
      cycData.mElecIdle[i] = getReceiveLos(i);
    }
    mPhyRx.putCycleData(cycData);
  }
}

void CarbonXPciePipeIf::transmitIdleData(void) {
  
  if (mCfg.mVerboseTransmitterHigh) {
    printf("%s  Info: Transmitting Idle data: ", mCfg.mPrintName.c_str());
  }
  
  // put idle data on bus
  for (UInt32 i=0; i < mCfg.mLinkWidth; ++i) {
    transmitLaneData(0, false, i, !mTxEIdle);
  }
}

void CarbonXPciePipeIf::transmitEIdle(bool on_off) {

  mTxEIdle = on_off;

  for(UInt32 i = 0; i < mCfg.mLinkWidth; i++) {
    setTransmitLos(i, on_off);
    setTransmitRxValid(i, !on_off);
  }
}

void CarbonXPciePipeIf::transmitData(const UInt32 *data, const bool *sym, bool scramble) {
    
  if (mCfg.mVerboseTransmitterHigh) {
    printf("%s  Info: Transmitting data: ", mCfg.mPrintName.c_str());
    for (UInt32 i=0; i < mCfg.mLinkWidth; ++i) {
      if (sym[i])
	printf("  %s", CarbonXPcieIfBase::getSymStringFromByte(data[i]));
      else
	printf("  %02X", data[i]);
    }
    printf("   (at %s)\n", mCfg.mCurrTimeString.c_str());
  }
  
  for (UInt32 i=0; i < mCfg.mLinkWidth; ++i)
    transmitLaneData(data[i], sym[i], i, scramble);
 }

void CarbonXPciePipeIf::transmitLaneData(UInt32 data, bool sym, UInt32 index, bool scramble) {

  // Don't do anything when in EIdle
  UInt32 eIdle;
  mSignalRxEIdle[index]->examine(&eIdle);
  
  if( !(eIdle ^ mCfg.mEIdleActiveHigh)) {
    data     = 0;
    sym      = 0;
    scramble = false;
  }
   
  // Mask Off any bits that don't belong
  data &= 0xff;
  
  // Scramble Data. The scrambler should always be called
  // even if we are not outputing scramble data (Unless Scrambling is turned off).
  // Otherwise the scrambler gets out of sync
  if(mCfg.mScrambleEnable) {
    UInt32 temp = mScrambler(data, sym, index);
    if(scramble) data = temp;
  }
  
  setTransmitData(index, data);
  setTransmitKCode(index, sym);
}

bool CarbonXPciePipeIf::isEIdle() {
  return getReceiveLos(0);
}

bool CarbonXPciePipeIf::inReset() {
  return mInReset;
}

void CarbonXPciePipeIf::setTransmitPins(CarbonXPcieNet* net, UInt32 value) {
   net->deposit(&value);
}

void CarbonXPciePipeIf::setTransmitPins(CarbonXPcieNet **netVect, UInt32 index, UInt32 value) {
   if (index < mCfg.mLinkWidth)
      netVect[index]->deposit(&value);
   else
     carbonInterfaceXtorPCIEInvalidSignalLinkIndex(mCfg.mMsgContext, mCfg.mPrintName.c_str(), netVect[index]->getName(), index, (mCfg.mLinkWidth-1));
}

void CarbonXPciePipeIf::setTransmitLos(UInt32 index, UInt32 value) {
  UInt32 temp = !(mCfg.mEIdleActiveHigh ^ value);
  setTransmitPins(mSignalRxEIdle, index, temp);

  if(temp) {
    setTransmitPins(mSignalRxData, index, 0);
    setTransmitPins(mSignalRxDataK, index, 0);
  }
}

UInt32 CarbonXPciePipeIf::getReceivePins(CarbonXPcieNet* net) {
   UInt32 value = 0;
   net->examine(&value);
   return value;
}

UInt32 CarbonXPciePipeIf::getReceivePins(CarbonXPcieNet **netVect, UInt32 index) {
   UInt32 value = 0;
   if (index < mCfg.mLinkWidth)
      netVect[index]->examine(&value);
   else
     carbonInterfaceXtorPCIEInvalidSignalLinkIndex(mCfg.mMsgContext, mCfg.mPrintName.c_str(), netVect[index]->getName(), index, (mCfg.mLinkWidth-1));
   return value;
}

UInt32 CarbonXPciePipeIf::getReceiveLos(UInt32 index) {
   UInt32 value = getReceivePins(mSignalTxEIdle, index);
   
   // Return value is active high, we need to account for the design's use of the signal
   // also look to see of the electric idle is forced inactive
   return !(mCfg.mEIdleActiveHigh ^ value);
}

void CarbonXPciePipeIf::checkTxDetectRx(void) {
   UInt32 i;

   // Pipe Intf has a TxDetectRx signal so we know when the DUT is doing a receiver detect
   if (getReceiveTxDetectRx() == 1) {  // TxDetectRx asserted
     //UtIO::cout() << "TxDetectRx asserted. Detect Timer = " << mDetectRxTimer << UtIO::endl;
      if (mDetectRxTimer == 0) {
	 mDetectRxTimer = 12;
      } else if (mDetectRxTimer == 10) {  // wait a few cycles to do the detect
	//UtIO::cout() << "Assering Phy Status." << UtIO::endl;
	 setTransmitPhyStatus(0x1);      // signal done with detection by assert of PhyStatus
	 for (i=0; i<mCfg.mLinkWidth; ++i) {
	   //UtIO::cout() << "Asserting RxStatus for lane " << i << UtIO::endl;
	   setTransmitRxStatus(i, 0x3); // set active lanes
	 }
      } else if (mDetectRxTimer == 9) {  // go 1 cycle & deassert PhyStatus
	 setTransmitPhyStatus(0x0);
	 for (i=0; i<mCfg.mLinkWidth; ++i) {
	    setTransmitRxStatus(i, 0x0); // set active lanes
	 }
      }
      --mDetectRxTimer;
   } else if (mDetectRxTimer > 0) {
      if (mDetectRxTimer == 4) {  // go a few cycles and then say we're ready to progress (No, I don't know why)
	 setTransmitPhyStatus(0x1);
      } else if (mDetectRxTimer == 3) {  // go a few cycles and then say we're ready to progress (No, I don't know why)
	 setTransmitPhyStatus(0x0);
      }
      --mDetectRxTimer;
   }

   return;
}

void CarbonXPciePipeIf::checkReset(void) {

   UInt32 reset_n_value = getReceiveResetN();

   // Pipe Intf has a ResetN signal so we know we're in reset
   // Phy Status Will be held at 1 while reset is asserted and for 3 cycles
   // following reset
   if ((reset_n_value == 0) && (not mInReset)) {  // reset_l asserted
     mInReset = true;
     mResetTimer = 2;  // count 3 clocks after deassertion of reset pin before coming out of reset
     setTransmitPhyStatus(0x1);
   } else if ((reset_n_value != 0) && (mInReset)) {  // on deassertion of reset pin
      if (mResetTimer == 0) {
	 mInReset = false;
	 setTransmitPhyStatus(0x0);    // set PhyStatus low showing that we have clocks and are ready

      } else {
	 --mResetTimer;
      }
   }
}

const char* CarbonXPciePipeIf::getStringFromPCIESignalType(CarbonXPcieSignalType sigType) {
   switch(sigType) {
   case eCarbonXPcieClock:
      return "eCarbonXPcieClock";
   case eCarbonXPciePhyStatus:
      return "eCarbonXPciePhyStatus";
   case eCarbonXPciePowerDown:
      return "eCarbonXPciePowerDown";
   case eCarbonXPcieResetN:
      return "eCarbonXPcieResetN";
   case eCarbonXPcieRxData:
      return "eCarbonXPcieRxData";
   case eCarbonXPcieRxDataK:
      return "eCarbonXPcieCarbonXPcieRxDataK";
   case eCarbonXPcieRxElecIdle:
      return "eCarbonXPcieRxElecIdle";
   case eCarbonXPcieRxPolarity:
      return "eCarbonXPcieRxPolarity";
   case eCarbonXPcieRxStatus:
      return "eCarbonXPcieRxStatus";
   case eCarbonXPcieRxValid:
      return "eCarbonXPcieRxValid";
   case eCarbonXPcieTxCompliance:
      return "eCarbonXPcieTxCompliance";
   case eCarbonXPcieTxData:
      return "eCarbonXPcieTxData";
   case eCarbonXPcieTxDataK:
      return "eCarbonXPcieCarbonXPcieTxDataK";
   case eCarbonXPcieTxDetectRx:
      return "eCarbonXPcieTxDetectRx";
   case eCarbonXPcieTxElecIdle:
      return "eCarbonXPcieTxElecIdle";
   default:
      return "UNKNOWN";
   }
}

void CarbonXPciePipeIf::reset() {
  mScrambler.reset();
}
  
