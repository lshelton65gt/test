// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CarbonXPciePhyLayer_H_
#define __CarbonXPciePhyLayer_H_

#include "xactors/pcie/carbonXPcie.h"
#include "xactors/pcie/CarbonXPcieNet.h"
#include "xactors/pcie/CarbonXPcieIfBase.h"
#include "CarbonXArray.h"

class CarbonXPcieIfBase;
class CarbonXPcieNet;
class CarbonXPciePhyTx;
class CarbonXPciePhyRx;
class CarbonXPcieConfig;
class CarbonXPcieSym;

// State Machines
typedef enum {
    Detect_Quiet,
    Detect_Active,
    Polling_Active,
    Polling_Config,
    Polling_Compliance,
    Polling_Speed, // This State is depricated in Gen2
    Config_Linkwidth_Start,
    Config_Linkwidth_Accept,
    Config_Lanenum_Wait,
    Config_Lanenum_Accept,
    Config_Complete,
    Config_Idle,
    Recovery_RcvrLock,
    Recovery_RcvrCfg,
    Recovery_Speed, // This State is new in Gen2
    Recovery_Idle,
    L1_Entry,
    L1_Idle,
    L23Ready,

    // LinkStats after this line, indicates PhysicalLinkUp to the Link Layer
    L0,
    Tx_L0s_Entry,
    Tx_L0s_Idle,
    Tx_L0s_Fts,
    Rx_L0s_Entry,
    Rx_L0s_Idle,
    Rx_L0s_Fts
} pcie_LTSM_state_t;

class CarbonXPcieTS {
  UInt32 mLinkWidth;
  bool   mIsTs1;
  CarbonXArray<UInt32> mLinkNum, mLaneNum, mNFts, mDataRate, mCtrl;
  
public: CARBONMEM_OVERRIDES;
  CarbonXPcieTS(UInt32 linkwidth) : mLinkWidth(linkwidth), mIsTs1(true),
				    mLinkNum(linkwidth), mLaneNum(linkwidth), 
				    mNFts(linkwidth), mDataRate(linkwidth), mCtrl(linkwidth) {
  }
  
  inline void setTs1() { mIsTs1 = true; }
  inline void setTs2() { mIsTs1 = false; }
  inline void setLinkNum(UInt32 lane, UInt8 num)  { mLinkNum[lane]  = num; }
  inline void setLaneNum(UInt32 lane, UInt8 num)  { mLaneNum[lane]  = num; }
  inline void setNFts(UInt32 lane, UInt8 num)     { mNFts[lane]     = num; }
  inline void setDataRate(UInt32 lane, UInt8 num) { mDataRate[lane] = num; }
  inline void setCtrl(UInt32 lane, UInt8 num)     { mCtrl[lane]     = num; }
  
  inline UInt8 getNFts(UInt32 lane) const { return mNFts[lane]; }
  inline UInt8 getDataRate(UInt32 lane) const { return mDataRate[lane]; }
  inline UInt8 getCtrl(UInt32 lane) const { return mCtrl[lane]; }
  inline bool isTs1() { return mIsTs1; }
  inline bool isTs2() { return !mIsTs1; }

  inline bool checkTs1PadPad() {
    if(isTs2()) return false;
    for(UInt32 i = 0 ; i < mLinkWidth; i++) {
      if(mLinkNum[i] != 255 || mLaneNum[i] != 255) return false;
    }
    return true;
  }
  inline bool checkTs2PadPad() {
    if(isTs1()) return false;
    for(UInt32 i = 0 ; i < mLinkWidth; i++) {
      if(mLinkNum[i] != 255 || mLaneNum[i] != 255) return false;
    }
    return true;
  }
  inline bool checkTs1LinkPad(UInt8 link_num) {
    if(isTs2()) return false;
    for(UInt32 i = 0 ; i < mLinkWidth; i++) {
      if(mLinkNum[i] != link_num || mLaneNum[i] != 255) return false;
    }
    return true;
  }
  inline bool checkTs1NotPadPad() {
    if(isTs2()) return false;
    for(UInt32 i = 0 ; i < mLinkWidth; i++) {
      if(mLinkNum[i] == 255 || mLaneNum[i] != 255) return false;
    }
    return true;
  }
  inline bool checkTs2NotPadPad() {
    if(isTs1()) return false;
    for(UInt32 i = 0 ; i < mLinkWidth; i++) {
      if(mLinkNum[i] == 255 || mLaneNum[i] != 255) return false;
    }
    return true;
  }
  inline bool checkTs2LinkPad(UInt8 link_num) {
    if(isTs1()) return false;
    for(UInt32 i = 0 ; i < mLinkWidth; i++) {
      if(mLinkNum[i] != link_num || mLaneNum[i] != 255) return false;
    }
    return true;
  }
  inline bool checkTs1LinkLane(UInt8 link_num) {
    if(isTs2()) return false;
    for(UInt32 i = 0 ; i < mLinkWidth; i++) {
      if(mLinkNum[i] != link_num || mLaneNum[i] != i) return false;
    }
    return true;
  }
  inline bool checkTs2LinkLane(UInt8 link_num) {
    if(isTs1()) return false;
    for(UInt32 i = 0 ; i < mLinkWidth; i++) {
      if(mLinkNum[i] != link_num || mLaneNum[i] != i) return false;
    }
    return true;
  }
};

class PCIETransaction;

class CarbonXPciePhyLayer {
  
public: CARBONMEM_OVERRIDES;
  
  enum DLPacketType {
    eDLLP,
    eLLTP,
    eILLEGAL
  };

  struct PhysPacket {
    DLPacketType mType;
    UInt32       mLength;
    UInt32 *     mData;
    bool *       mSym;
    bool operator==(PhysPacket& other) {
      if(other.mType != mType) return false;
      if(other.mLength != mLength) return false;
      
      if( (other.mData != NULL && mData == NULL) || (other.mData == NULL && mData != NULL)) return false; 
      if( (other.mSym != NULL && mSym == NULL) || (other.mSym == NULL && mSym != NULL)) return false; 

      for(UInt32 i = 0; i < mLength; i++) {
        if(mData) if(other.mData[i] != mData[i]) return false; 
        if(mSym) if(other.mSym[i] != mSym[i]) return false; 
      }
      return true;
    }
  };

  typedef void (TxReqTransCBFn)(void *info);

  CarbonXPciePhyLayer(CarbonXPcieIntfType intf, CarbonXPcieConfig& config);
  
  ~CarbonXPciePhyLayer();
  
  //! The methods resets the statemachine to the Detect State
  void reset();
  
  //! This method needs to get called every symbol period
  void step();
  
  //! Return pointer to the net object for the specified signal
  /* The Net Object is owned by the transactor and an external environment
     needs to get to the net object to deposit values or examine
     the current value.
     \param sigType What signal to get
     \param index For what lane to get the signal
  */
  CarbonXPcieNet * getNet(CarbonXPcieSignalType sigType, UInt32 index);

  //! This method needs to get called ever symbol period
  bool             sendPacket(PCIETransaction *trans);

  //! Put Phy Transmitter in the L0s Link State
  void             powerSave();
  
  //! Tell the Phy to start Training
  /*  This will also imply that this instance of the transactor
      acts as the Upstream Device.
  */
  void             startTraining(UInt32 delay);

  //! Enable/Disable Bypass of Physical Link Training
  /*! \param tf Set to true to enable bypass or to false to
    disble bypass.
  */
  void             setTrainingBypass(bool tf);

  //! Check if transmitter already has a Pending Packet
  /* The Transmitter always has the packet it's currently sending
     and up to one pending packet. If there is already a pending
     packet, no packet can be send until the now pending packet has
     started to transmitting.
     \retval true if there is a packet pending
     \retval false if there is no packet pending
  */
  bool             hasPendingPacket() const;

  //! Check if transmitter is currently sending a packet
  /* 
     \retval true if a packet currently is being sent
     \retval false if a packet is not being sent
  */
  bool             txPacketInProgress() const;

  //! Check if transmitter has been idle a certain number of cycles
  bool             txIsIdle(UInt32 cycles) const;

  //! Check if transmitter has nothing to do
  bool             txIsDone() const;

  //! Get number of consecutive Idles Received
  /* Get the number if Idles recieved since last time there
     were any activity on the bus.
     \return Number of Idles
  */
  UInt32           getConsecIdles();
  
  //! Set Link Number
  /* \param link_num Link Number for this link to be used during training.
   */
  void              setLinkNum(UInt8 link_num) { mLinkNum = link_num; }

  //! Get Link Number
  /* \return Link Number for this link to be used during training.
   */
  UInt8             getLinkNum() const { return mLinkNum; }

  //! Set Number of FTS Ordered Sets
  /* Sets the number of FTS's that this link needs to when going from L0s
     Linkstate to L0. This value is used to but in one of the fields in the
     TS Ordered Set during Training.
     \param fts Number of FTS's
   */
  void              setNFts(UInt8 nfts) { mNFts = nfts; }
  
  //! Get Number of FTS's
  //! \return Number of FTS's
  UInt8             getNFts() const {return mNFts; }
  
  //! Set Supported Data Rates.
  /* This function Sets the Data Rates the should support.
     \adata_rate bit 1 means a data rate of 2.5 Gbps, and should always be supported and 
     therefor always be set to one.
     \adata_rate bit 2 means a data rate of 5.0 Gbps, and should be set to one if supported.
     \adata_rate bit 3::7 should be set to 0
  */
  void              setSupportedDataRate(UInt8 data_rate) {
    mInitSupportedDataRate = data_rate; 
  }

  //! Get Supported Data Rate
  /* Gets the supported data rate.
  */
  UInt8             getSupportedDataRate() const {return mInitSupportedDataRate; }
  
  //! Get Current Data Rate
  /* Gets the current data rate unless the checkDataRateChange() method
     returns true, when this method returns the new data rate to change to.
  */
  UInt8             getDataRate() const {return mDataRate; }

  //! Check if PHY is Requesting a Data Rate Change
  /* Will return true when the LTSSM is in the Recover.Speed state.
     The LTSSM will stay in that state until data rate change is acknoledged 
     by the ackDataRateChange method.  
     \retval true when a Data Rate Change is requested.
  */
  bool             checkDataRateChange() const;

  //! Acknoledge data rate change
  /* Should be called by the external environment when it has swithed to call
     step with the new data rate. This method will only have affect when
     the checkDataRateChange() method returns true.
  */
  void             ackDataRateChange();
  
  //! Setup Data Rate Change Request Callback function
  /* The callback function will be called when the transactor is initiating a
     data rate change. The new requested data rata will be passed to the function.
     \param func Pointer to Callback function
     \param info Pointer to a user defined structure that will be passed to the callback function.
  */
  void             setDataRateChangeCBFn(CarbonXPcieRateChangeCBFn *func, void *info);

  //! Get Data Rate Change Request Callback function
  /* Get the callback function that will be called when the transactor is initiating a
     data rate change.
     \return Pointer to Callback function
  */
  CarbonXPcieRateChangeCBFn * getDataRateChangeCBFn();

  //! Set Scramble Enable
  void              setScrambleEnable(bool);
  bool              getScrambleEnable();
  bool              isPhysicalLinkUp() const { return mLinkState == L0; }
  pcie_LTSM_state_t getLinkState() const;
  pcie_LTSM_state_t getTxLinkState() const;
  pcie_LTSM_state_t getRxLinkState() const;
  bool              getPhysPacket(CarbonXPciePhyLayer::PhysPacket *pkt);
  bool              getPhysPacket(CarbonXList<CarbonXPcieSym>* pkt);
  
  //! Setup Tx Transaction Done Callback
  /* !This callback is supposed to be used to send a new transaction
     to the Phy for immediate transmission on to the bus.
  */
  void              setTxReqTransCBFn(TxReqTransCBFn *fn, void *info);

  //! Set Debug Level
  void             verbose(UInt32 flags);

  // Phy Rx Interface
  void             rcvdSkipOs();
  void             rcvdEIdleOs();
  void             rcvdFtsOs();
  //void             rcvdTsOs(UInt32 ts, UInt8 *link_num, UInt8 *lane_num, UInt8 *nfts, UInt8 *data_rate, UInt8 *ctrl);

  static const char* getSymStringFromByte(UInt32);

private:

  // Link Training and Status State Machine
  void ltssm();
  
  // Request a data rate change from the external environment
  void changeDataRate();
  
  // Help Function to help determine the witch data rate is large
  bool isDataRateLarger(UInt8 first, UInt8 second);

  const char* getStringFromPCIESignalType(CarbonXPcieSignalType sigType);  
  const char* getLTSSMStateString(pcie_LTSM_state_t sm);
  
  // Configuration Structure
  CarbonXPcieConfig&   mCfg;

  CarbonXPcieIntfType mIntfType;
  //UInt32              mLinkWidth;
  //UtString            mPrintName;
  //const UtString &    mCurrTimeString;
  
  // Transmitter and Receiver
  CarbonXPciePhyTx *  mPhyTx;
  CarbonXPciePhyRx *  mPhyRx;

  // Transactor Parameters
  UInt8               mLinkNum;
  UInt8               mRequestedLinkNum;
  UInt8               mNFts;
  UInt8               mRequestedNFts;
  //bool                mTrainingBypass;

  // Phy Control
  pcie_LTSM_state_t   mLinkState;
  pcie_LTSM_state_t   mNextLinkState;
  //bool                mUpstreamDevice;
  
  // Data Rate Change Variables
  UInt8               mDataRate;
  UInt8               mInitSupportedDataRate;
  UInt8               mCurrSupportedDataRate;
  UInt8               mRequestedDataRate;
  UInt8               mNegotiatedDataRate;
  bool                mDirectedSpeedChange;
  bool                mChangedSpeedRecovery;
  bool                mAckDataRateChange;
  bool                mDataRateChangeRequested;
  void *              mDataRateChCBInfo;
  CarbonXPcieRateChangeCBFn *mDataRateChCBFn;
  
  // Signal Interface
  CarbonXPcieIfBase * mSignalIf;

  // Debug Parameters
  //bool VerboseReceiverHigh;       // print out a LOT of Receiver info
  //bool VerboseReceiverMedium;     // print out a bit of Receiver info
  //bool VerboseReceiverLow;        // print out some Receiver info
  //bool VerboseTransmitterHigh;    // print out a LOT of Transmitter info
  //bool VerboseTransmitterMedium;  // print out a bit of Transmitter info
  //bool VerboseTransmitterLow;     // print out some Transmitter info

  //MsgContext * mMsgContext;
};

#endif
