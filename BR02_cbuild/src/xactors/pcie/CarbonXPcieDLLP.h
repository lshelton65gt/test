// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef __CarbonXPcieDLLP_H_
#define __CarbonXPcieDLLP_H_

class CarbonXPcieDLLP {
public: CARBONMEM_OVERRIDES;
  enum LinkType {
    eAck          = 0x0,
    eNak          = 0x10,
    ePMEnterL1    = 0x20,
    ePMEnterL23   = 0x1,
    ePMRequestL1  = 0x23,
    ePMRequestAck = 0x24,
    eInitFC1P     = 0x40,
    eInitFC1N     = 0x50,
    eInitFC1Cpl   = 0x60,
    eInitFC2P     = 0xC0,
    eInitFC2N     = 0xD0,
    eInitFC2Cpl   = 0xE0,
    eUpdateFCP    = 0x80,
    eUpdateFCN    = 0x90,
    eUpdateFCCpl  = 0xA0,
    eIllegal      = 0xff
  };
  
  CarbonXPcieDLLP() {
    mLinkType  = eIllegal;
    mVC        = 0;
    mHdrFC     = 0;
    mDataFC    = 0;
    mAckNakSeq = 0;
  }

  void setLinkType(LinkType type) {
    mLinkType = type;
  }
  
  LinkType getLinkType() const {
    return (LinkType) mLinkType;
  }

  void setVC(UInt8 vc) {
    mVC = vc;
  }
  
  UInt8 getVC() const {
    return mVC;
  }

  void setHdrFC(UInt8 fc) {
    mHdrFC = fc;
  }

  UInt8 getHdrFC() const {
    return mHdrFC;
  }

  void setDataFC(UInt16 fc) {
    mDataFC = fc & 0xfff;
  }
  
  UInt16 getDataFC() const {
    return mDataFC;
  }

  void setAckNakSeq(UInt16 seq_num) {
    mAckNakSeq = seq_num & 0xfff;
  }
  
  UInt16 getAckNakSeq() const {
    return mAckNakSeq;
  }

private:
  UInt8  mLinkType;
  UInt8  mVC;
  UInt8  mHdrFC;
  UInt16 mDataFC;
  UInt16 mAckNakSeq;
};

#endif
