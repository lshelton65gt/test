// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CarbonX8b10b_H_
#define __CarbonX8b10b_H_

#include "shell/carbon_shelltypes.h"
#include "util/c_memmanager.h"

class CarbonX8b10b {
public: CARBONMEM_OVERRIDES;
  
  CarbonX8b10b();

  UInt8  decoder_lut(UInt16 data_in, bool *kout, bool *error_out);
  UInt16 encoder_lut(UInt8  data_in, bool kin);

  bool getOutputDisparity() const;
  bool getInputDisparity() const;
  bool isDisparityInNotSet() const;
  void clearInputDisparity();

 private:
  bool disparity_in;           // keep track of disparity for decoding each lane
  bool disparity_out;          // keep track of disparity for encoding each lane
  bool disparity_in_not_set;   // decide if we've figured out the correct input disparity yet
  
  // Debug Control
  bool mVerbose;

  //   Look Up Table for data bytes for 8b/10b encoding & decoding, with Positive Disparity
  static UInt32 lut_data_10bit_p[256];
  //   Look Up Table for data bytes for 8b/10b encoding & decoding, with Negative Disparity
  static UInt32 lut_data_10bit_n[256];
  //   Look Up Table for special bytes for 8b/10b encoding & decoding, with Positive Disparity
  static UInt32 lut_spec_10bit_p[256];
  //   Look Up Table for special bytes for 8b/10b encoding & decoding, with Negative Disparity
  static UInt32 lut_spec_10bit_n[256];
  static bool init_done; 

  static void init_luts(void);
  
};

#endif
