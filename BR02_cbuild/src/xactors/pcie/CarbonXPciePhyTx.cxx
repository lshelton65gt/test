/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "CarbonXPciePhyTx.h"
#include "CarbonXPcieCommonTypes.h"
#include "shell/carbon_interface_xtor.h"
#include "util/UtIO.h"
#include <cstdio>


// TODO :
// Add Check to make Sure that I don't start more than one DLLP during a symbol time.

CarbonXPciePhyTx::CarbonXPciePhyTx(CarbonXPcieConfig& cfg, CarbonXPcieIntfType intf, UInt32 linkwidth, 
				   const CarbonXString & inPrintName, const CarbonXString & currTimeString,
				   CarbonXPcieIfBase &signal_if,
                                   MsgContext * msg_context) :
  mCfg(cfg), mIntfType(intf), mLinkWidth(linkwidth), mPrintName(inPrintName), mCurrTimeString(currTimeString),
  mSignalIf(signal_if),
  mLinkNum(0), mNFts(255), mDataRate(0x2), mTrainCtrl(0),
  mReqTransCBFn(0), mReqTransCBInfo(0),
  mLinkState(Detect_Quiet),
  mUpstreamDevice(false),
  mCurrOs(linkwidth), mCurrPkt(0), mPendPkt(0), mSkipTimer(0), mSkipTimeout(1180), mIdleTimer(0),
  mPhysicalLinkUp(false), mEIdleSent(false),
  VerboseTransmitterHigh(false), VerboseTransmitterMedium(false), VerboseTransmitterLow(false),
  mMsgContext(msg_context)
{
}

void CarbonXPciePhyTx::step() {
  
  // Update Skip Timer
  if(mSkipTimer != 0) --mSkipTimer;
  
  // If We're not in a middle of either OS or Packet, figure out what to send next
  if(!mCurrOs.getData() && !packetInProgress()) {

    // See If it is time to send A Skip OS
    if(mSkipTimer == 0 && mLinkState > Detect_Active) {
      mCurrOs.skipOs();
      mSkipTimer = mSkipTimeout;
    }

    // Or see if we need to send any OS's as part of Link Training
    else {
      switch(mLinkState) {
      case Detect_Quiet :
	mSignalIf.transmitEIdle(true);
	//for(UInt32 i = 0; i < mLinkWidth; i++) setTransmitLos(i, 1);
	break;
      case Detect_Active :
	// Take us out of Electrical Idle
	mSignalIf.transmitEIdle(false);
	break;
      case Polling_Active :
	++mNumOssSent;
	mCurrOs.tsOs(true, true, true, mLinkNum, mNFts, mDataRate, ~mCfg.mScrambleEnable << 3);
	break;
      case Polling_Config :
	++mNumOssSent;
	mCurrOs.tsOs(false, true, true, mLinkNum, mNFts, mDataRate, ~mCfg.mScrambleEnable << 3);
	break;
      case Config_Linkwidth_Start :
	++mNumOssSent;
	mCurrOs.tsOs(true, false, true, mLinkNum, mNFts, mDataRate, ~mCfg.mScrambleEnable << 3);
	break;
      case Config_Linkwidth_Accept :
	++mNumOssSent;
	mCurrOs.tsOs(true, false, false, mLinkNum, mNFts, mDataRate, ~mCfg.mScrambleEnable << 3);
	break;
      case Config_Lanenum_Wait :
	++mNumOssSent;
	mCurrOs.tsOs(true, false, false, mLinkNum, mNFts, mDataRate, ~mCfg.mScrambleEnable << 3);
	break;
      case Config_Lanenum_Accept :
	++mNumOssSent;
	mCurrOs.tsOs(true, false, false, mLinkNum, mNFts, mDataRate, ~mCfg.mScrambleEnable << 3);
	break;
      case Config_Complete :
	++mNumOssSent;
	mCurrOs.tsOs(false, false, false, mLinkNum, mNFts, mDataRate, ~mCfg.mScrambleEnable << 3);
	break;
      case Config_Idle :
	mNumOssSent += 16;
	mCurrOs.logicalIdles(16);
	break;
      case Recovery_RcvrLock :
	++mNumOssSent;
	
	// If We came here from Recovery.Speed, we need to exit EIdle
	mSignalIf.transmitEIdle(false);

	mCurrOs.tsOs(true, false, false, mLinkNum, mNFts, mDataRate, ~mCfg.mScrambleEnable << 3);
	break;
      case Recovery_RcvrCfg :
	++mNumOssSent;
	mCurrOs.tsOs(false, false, false, mLinkNum, mNFts, mDataRate, ~mCfg.mScrambleEnable << 3);
	break;
      case Recovery_Speed :
	if(!mEIdleSent) {
	  mCurrOs.eIdleOs();
	  mEIdleSent = true;
	}
	else {
	  mSignalIf.transmitEIdle(true);

	  // Transmitting Logical Idles will guarantee that we stay in EIdle for a while
	  mCurrOs.logicalIdles(50);
	  
	  //for(UInt32 i = 0; i < mLinkWidth; i++) setTransmitLos(i, 1);
	}
	break;
      case Recovery_Idle :
	mNumOssSent += 16;
	mCurrOs.logicalIdles(16);
	break;
      case L0 :
// 	if(mIdleTimer > 100 && !mPendPkt && !mCurrPkt) {
// 	  mLinkState = Tx_L0s_Entry;
// 	}
	break;
      case Tx_L0s_Entry :
	if(VerboseTransmitterMedium)
	  printf("%s  Info: LTSSM Transitioning to L0s Power Save Linkstate (at %s). Idle Timer = %d\n", mPrintName.c_str(), mCurrTimeString.c_str(), mIdleTimer);
	mCurrOs.eIdleOs();
	// After Sending 1 Idle OS go to L0s_Idle State
	mLinkState = Tx_L0s_Idle;
	break;
      case Tx_L0s_Idle :
	// Keep the SkipTimer to non 0, so we won't send any skips here
	mSkipTimer = mSkipTimeout;

	// Wait here until we have packets to send again
	if(mPendPkt) {
	  mCurrOs.ftsOs(mNFts);
	  mLinkState = Tx_L0s_Fts;
	}
	break;
      case Tx_L0s_Fts :
	mCurrOs.skipOs();
	mSkipTimer = mSkipTimeout;
	mLinkState = L0;
	if(VerboseTransmitterMedium)
	  printf("%s  Info: New Packets to send. Transitioning to L0 Linkstate (at %s).\n", mPrintName.c_str(), mCurrTimeString.c_str());
	break;

      default : // If non of the above, no OS's needs to be sent
	break;
      }
    }
    
    // If there is no OS, is there a packet?
    if(!mCurrOs.getData() && mPhysicalLinkUp) {
     
      // Try to get a packet
      getPacket(0);
    }
  }

  // First Check to see if we are currently sending an Ordered Set
  if(mCurrOs.getData()) {

    // This is not an Idle, so clear Idle Time
    mIdleTimer = 0;

    // getData for Ordered set give us a full set of data for all lanes
    mSignalIf.transmitData(mCurrOs.getData(), mCurrOs.getSym(), mCurrOs.doScramble());
    ++mCurrOs; // Increment to next Byte in the OS
  }
  
  // Send Any Packet in progress. Note that it can also start a packet
  else if(packetInProgress()) {

    // This is not an Idle, so clear Idle Time
    mIdleTimer = 0;

    // getData for a Packet gives us one byte at a time since we don't know
    // what the alignment to the lanes are going to be
    bool DLLPStartedThisCycle = false;
    for(UInt32 i = 0; i < mLinkWidth; i++) {
      if(packetInProgress()) {
// 	UtIO::cout() << mPrintName << ": Sending Data on lane. " << i << " Bytes Sent:  " << mCurrPkt->mBytesSent 
// 		     << " Bytes Total: " << mCurrPkt->mBytesTotal << UtIO::endl;
	mSignalIf.transmitLaneData(mCurrPkt->getData(), mCurrPkt->getSym(), i);
	if(mCurrPkt->getData() == PCIE_SDP && mCurrPkt->getSym()) {
	  //printf("Starting DLLP Packet on Lane %d at time %s.\n", i, mCurrTimeString.c_str());
	  DLLPStartedThisCycle = true;
	}
	++(*mCurrPkt); // Increment to the next byte
	if(!mCurrPkt->inProgress()) mCurrPkt = 0; // We need to get rid of the pointer as soon as we're done to avoid trouble
      }
      
      // We're done sending the packet, we need to see if there is another packet to start sending
      // or if we need to put in PAD or IDL symbols

      // Is this a lane where we are allowed to start packets?
      else if( (i%4) == 0 ) {
	//printf("Checking if we can start a new packet on lane %d at %s\n", i, mCurrTimeString.c_str());
	// Do we have another packet to send?
	if( getPacket(i) ) {
	  //UtIO::cout() << mPrintName << "Starting New Packet " << mCurrPkt->getPrintString() << " in lane " << i << UtIO::endl;
	  mSignalIf.transmitLaneData(mCurrPkt->getData(), mCurrPkt->getSym(), i);
	  if(mCurrPkt->getData() == PCIE_SDP && mCurrPkt->getSym() && DLLPStartedThisCycle) {
            carbonInterfaceXtorPCIESecondDLLPInCycle(mMsgContext, mPrintName.c_str(), mCurrTimeString.c_str());
	  }
	  ++(*mCurrPkt); // Increment to the next byte
	  if(!mCurrPkt->inProgress()) mCurrPkt = 0; // We need to get rid of the pointer as soon as we're done to avoid trouble
	}

	// Else we need to pad or Idle, Link larger than x4 uses the PAD Symbol
	else if (mLinkWidth > 4) {
	  mSignalIf.transmitLaneData(PCIE_PAD, true, i);
	}
	else {
	  mSignalIf.transmitLaneData(0, false, i); // Idle
	}
      }
      
      // We can't start a new packet on this lane so PAD or Idle
      else {
	if (mLinkWidth > 4) {
	  mSignalIf.transmitLaneData(PCIE_PAD, true, i);
	}
	else {
	  mSignalIf.transmitLaneData(0, false, i); // Idle
	}
      }
    }
  }

  // If there is nothing else going on
  else {
    mSignalIf.transmitIdleData();
    ++mIdleTimer;
  }

}

void CarbonXPciePhyTx::setLinkState(pcie_LTSM_state_t state) {
  mLinkState = state;
  if(state == L0) mPhysicalLinkUp = true;
  else mPhysicalLinkUp = false;

  // Clear OsCount when linkstate changes
  mNumOssSent = 0;
}

pcie_LTSM_state_t CarbonXPciePhyTx::getLinkState() const {
  return mLinkState;
}

bool CarbonXPciePhyTx::hasPendingPacket() const {
  return mPendPkt != 0;
}

bool CarbonXPciePhyTx::packetInProgress() const {
  if(mCurrPkt) return mCurrPkt->inProgress();
  else         return false;
}

void CarbonXPciePhyTx::clearOsCount() {
  mNumOssSent = 0;
}

UInt32 CarbonXPciePhyTx::getOsCount() {
  return mNumOssSent;
}

bool CarbonXPciePhyTx::isDone() const {
  return !packetInProgress() && !mPendPkt;
}

void CarbonXPciePhyTx::setTxReqTransCBFn(CarbonXPciePhyLayer::TxReqTransCBFn *fn, void *info) {
  mReqTransCBFn   = fn;
  mReqTransCBInfo = info;
}

bool CarbonXPciePhyTx::sendPacket(PCIETransaction *trans) {
  bool result = false;
  
  // If there is not packet pending, copy the new packet into the pending one
  if(!mPendPkt) {
    mPendPkt = trans;
    mPendPkt->step();
    result = true;
  }

  return result;
}

bool CarbonXPciePhyTx::getPacket(UInt32 curr_lane) {
  bool result = false;
  
  mCurrPkt = 0;

  // If there is a Packet pending, copy it to the packet in progress and clear the flag
  if(mPendPkt) {
    if( (curr_lane == 0) || !mPendPkt->mustStartLane0()) {
      mCurrPkt = mPendPkt;
      mPendPkt = 0;
      result = true;
    }
    else result = false;
  }
  else {
    // Call Callback function to request a new packet.
    if(mReqTransCBFn) {
      mReqTransCBFn(mReqTransCBInfo);
    
      if(mPendPkt) {
	if( (curr_lane == 0) || !mPendPkt->mustStartLane0()) {
	  mCurrPkt = mPendPkt;
	  mPendPkt = 0;
	  result = true;
	}
	else result = false;
      }
    }
    else {
      mCurrPkt = 0;
    }
  }
  return result;
}

void CarbonXPciePhyTx::verbose(UInt32 flags)
{
    // Bits are Active LOW
    //  hi me lo tx rx
    //   x  x  x  1  0   = 0x1E = Verbose_Receiver
    //   x  x  x  0  1   = 0x1D = Verbose_Transmitter
    //   x  x  x  0  0   = 0x1C = Verbose_All
    //   1  1  1  x  x   = 0x1F = Verbosity_Off
    //   1  1  0  x  x   = 0x1B = Verbosity_Low
    //   1  0  x  x  x   = 0x17 = Verbosity_Medium
    //   0  x  x  x  x   = 0x0F = Verbosity_High

    if(!(flags & 0x02)) {    // transmitter selected
	if(!(flags & 0x10)) {         // High
	    VerboseTransmitterHigh = true;
	    VerboseTransmitterMedium = true;
	    VerboseTransmitterLow = true;
	} else if(!(flags & 0x08)) {  // Medium
	    VerboseTransmitterHigh = false;
	    VerboseTransmitterMedium = true;
	    VerboseTransmitterLow = true;
	} else if(!(flags & 0x04)) {  // Low
	    VerboseTransmitterHigh = false;
	    VerboseTransmitterMedium = false;
	    VerboseTransmitterLow = true;
	} else {                     // Off
	    VerboseTransmitterHigh = false;
	    VerboseTransmitterMedium = false;
	    VerboseTransmitterLow = false;
	}
    }
}

void CarbonXPciePhyTx::setUpstreamDevice(bool en) {
  mUpstreamDevice = en;
}

void CarbonXPciePhyTx::reset() {
  mCurrOs.clear();
  mCurrPkt         = 0;
  mPendPkt         = 0;
  mNFts            = 255;
  mDataRate        = 0x2;
  mLinkState       = Detect_Quiet;
  mUpstreamDevice  = false;
  mSkipTimer       = 0;
  mIdleTimer       = 0;
  mPhysicalLinkUp  = false;
  mEIdleSent       = false;
}
