// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CarbonXPciePhyOS_H_
#define __CarbonXPciePhyOS_H_

#include "CarbonXArray.h"

//! This Class Sets up Ordered Sets for The PCIE Transactor
class CarbonXPcieOS {
 public:
  CarbonXPcieOS(UInt32 linkwidth);
  ~CarbonXPcieOS();

  void skipOs();
  void ftsOs(UInt32 repeat);
  void eIdleOs();
  void tsOs(bool                    this_is_ts1, 
	    bool                    useLinkPad, 
	    bool                    useLanePad,
	    UInt8                   linkNum,
	    UInt8                   nFts,
	    UInt8                   dataRate,
	    UInt8                   trainCtrl);
  void logicalIdles(UInt32 repeats);

  const UInt32 * getData() const ;
  const bool *   getSym() const ;

  CarbonXPcieOS & operator++();
  bool doScramble();
  void clear() {mCurrCycle = mOsSize;}
 private:

  UInt32 mLinkWidth;
  UInt32 mCurrCycle;
  UInt32 mOsSize;
  bool   mScramble;
  CarbonXArray<UInt32> mData;
  CarbonXArray<bool>   mSym;
  
  UInt32 *mCycleData;
  bool   *mCycleSym;
};
#endif
