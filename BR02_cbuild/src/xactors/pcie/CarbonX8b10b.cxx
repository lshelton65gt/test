/*****************************************************************************

 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "CarbonX8b10b.h"
#include <cstdio>

bool CarbonX8b10b::init_done = false;
UInt32 CarbonX8b10b::lut_data_10bit_p[256];
UInt32 CarbonX8b10b::lut_data_10bit_n[256];
UInt32 CarbonX8b10b::lut_spec_10bit_p[256];
UInt32 CarbonX8b10b::lut_spec_10bit_n[256];

CarbonX8b10b::CarbonX8b10b() {

  // The Luts are static, so we only have to initialize ones
  if(!init_done) init_luts();
  init_done = true;

  disparity_in         = false;         
  disparity_out        = false;      
  disparity_in_not_set = true;

  mVerbose = false;
}

UInt16 CarbonX8b10b::encoder_lut(UInt8 data_in, bool kin) {
    UInt32 data_out;
    bool rdisp;
    int sum, i;

    rdisp = disparity_out;

    if(kin) {        // have a special char symbol
	if(rdisp)
	    data_out = lut_spec_10bit_p[data_in];
	else
	    data_out = lut_spec_10bit_n[data_in];
    } else {        // have a data char symbol
	if(rdisp)
	    data_out = lut_data_10bit_p[data_in];
	else
	    data_out = lut_data_10bit_n[data_in];
    }
    // Generate new rdisp and put in *disp.
    // disparity of true = +1, false = -1
    // if data_out has six 1s & four 0s, its disparity = +2 and is added to the input disparity
    // if data_out has five 1s & five 0s, its disparity = 0 and is added to the input disparity
    // if data_out has four 1s & six 0s, its disparity = -2 and is added to the input disparity
    for(i=0,sum=0; i<10; ++i)
	sum += ((data_out >> i) & 1);

    // printf(":: sum equals %d\n",sum);
    if (sum == 5) ;                  // dont change
    else if ((sum == 4) && rdisp)    // set to -1
	rdisp = false;
    else if ((sum == 6) && !rdisp)   // set to +1
	rdisp = true;
    else
      printf("Can't determine new disparity, Lut Returned Bad symbol. (disp=%s, K=%s, ByteIn=0x%02X, PacketOut=0x%03X).\n", (rdisp ? "(+)" : "(-)"), (kin ? "true" : "false"), data_in, data_out);
    
    disparity_out  = rdisp;  // save the new encoding disparity value
    return(data_out);
}

UInt8 CarbonX8b10b::decoder_lut(UInt16 data_in, bool *kout, bool *error_out)
{
    UInt32 data_out, i;
    bool rdisp, rk;
    bool rdisp_not_set;
    bool found_it;
    int sum,j;
    
    // Default to no error
    *error_out = false;

    if(mVerbose)
	printf("          Decoder_Lut: data_in = 0x%03X, ",data_in);

    rdisp = disparity_in;
    rdisp_not_set = disparity_in_not_set;
    rk = true;
    found_it = false;

    if(data_in > 0x3FF)
	return(0xFF);  // invalid input data!
    data_out = 0xFF;


    // Generate new rdisp and put in *disp.
    // disparity of true = +1, false = -1
    // if data_in has six 1s & four 0s, its disparity = +2 and is added to the input disparity
    // if data_in has five 1s & five 0s, its disparity = 0 and is added to the input disparity
    // if data_in has four 1s & six 0s, its disparity = -2 and is added to the input disparity
    for(j=0,sum=0; j<10; ++j)
      sum += ((data_in >> j) & 1);
    
    // printf(":: sum equals %d\n",sum);
    if (sum == 5) ;                                         // dont change
    else if ((sum == 4) && ( rdisp || rdisp_not_set)) {     // set to -1
      disparity_in = false;
      disparity_in_not_set = false;
    } else if ((sum == 6) && (!rdisp || rdisp_not_set)) {   // set to +1
      disparity_in = true;
      disparity_in_not_set = false;
    } else
      *error_out = true;
    
    if(mVerbose)
      printf(" rdisp = %d%s,",rdisp, (rdisp_not_set ? " (NOT SET)" : ""));
    if( (rdisp) || (rdisp_not_set) ) {
      for (i=0; i<=0xFF; ++i) {                       // look for special codes first
	if(lut_spec_10bit_p[i] == data_in) {
	  data_out = i;
	  rk = true;
	  found_it = true;
	  break;
	}
      }
      if(!found_it) {                                 // if not found
	for (i=0; i<=0xFF; ++i) {                   // look for data translation
	  if(lut_data_10bit_p[i] == data_in) {
	    data_out = i;
	    rk = false;
	    found_it = true;
	    break;
	  }
	}
      }
    }
    if( (!rdisp) || (rdisp_not_set && !found_it) ) {
      for (i=0; i<=0xFF; ++i) {                       // look for special codes first
	if(lut_spec_10bit_n[i] == data_in) {
	  data_out = i;
	  rk = true;
	  found_it = true;
	  break;
	}
      }
      if(!found_it) {                                 // if not found
	for (i=0; i<=0xFF; ++i) {                   // look for data translation
	  if(lut_data_10bit_n[i] == data_in) {
	    data_out = i;
	    rk = false;
	    found_it = true;
	    break;
	  }
	}
      }
    }
    if (!found_it) {
      disparity_in_not_set = true;
      *error_out = true;
    }
    
    if(mVerbose)
      printf(" K = %d, DataOut = 0x%02X\n",rk, data_out);

    (*kout) = rk;
    return data_out;
}

bool CarbonX8b10b::getOutputDisparity() const {
  return disparity_out;
}

bool CarbonX8b10b::getInputDisparity() const {
  return disparity_in;
}

bool CarbonX8b10b::isDisparityInNotSet() const {
  return disparity_in_not_set;
}

void CarbonX8b10b::clearInputDisparity() {
  disparity_in         = false;
  disparity_in_not_set = true;
}

// Static Functions
void CarbonX8b10b::init_luts()
{
    int i;

    for (i=0; i<=0xFF; ++i) {
	lut_spec_10bit_p[i] = 0xFFF;
	lut_spec_10bit_n[i] = 0xFFF;
    }
    lut_spec_10bit_p[0x1C] = 0x30B;
    lut_spec_10bit_p[0x3C] = 0x306;
    lut_spec_10bit_p[0x5C] = 0x30A;
    lut_spec_10bit_p[0x7C] = 0x30C;
    lut_spec_10bit_p[0x9C] = 0x30D;
    lut_spec_10bit_p[0xBC] = 0x305;
    lut_spec_10bit_p[0xDC] = 0x309;
    lut_spec_10bit_p[0xFC] = 0x307;
    lut_spec_10bit_p[0xF7] = 0x057;
    lut_spec_10bit_p[0xFB] = 0x097;
    lut_spec_10bit_p[0xFD] = 0x117;
    lut_spec_10bit_p[0xFE] = 0x217;
    lut_spec_10bit_n[0x1C] = 0x0F4;
    lut_spec_10bit_n[0x3C] = 0x0F9;
    lut_spec_10bit_n[0x5C] = 0x0F5;
    lut_spec_10bit_n[0x7C] = 0x0F3;
    lut_spec_10bit_n[0x9C] = 0x0F2;
    lut_spec_10bit_n[0xBC] = 0x0FA;
    lut_spec_10bit_n[0xDC] = 0x0F6;
    lut_spec_10bit_n[0xFC] = 0x0F8;
    lut_spec_10bit_n[0xF7] = 0x3A8;
    lut_spec_10bit_n[0xFB] = 0x368;
    lut_spec_10bit_n[0xFD] = 0x2E8;
    lut_spec_10bit_n[0xFE] = 0x1E8;


    // INITIALIZE LOOK-UP TABLES
	lut_data_10bit_p[0x00] = 0x18B;
	lut_data_10bit_p[0x01] = 0x22B;
	lut_data_10bit_p[0x02] = 0x12B;
	lut_data_10bit_p[0x03] = 0x314;
	lut_data_10bit_p[0x04] = 0x0AB;
	lut_data_10bit_p[0x05] = 0x294;
	lut_data_10bit_p[0x06] = 0x194;
	lut_data_10bit_p[0x07] = 0x074;
	lut_data_10bit_p[0x08] = 0x06B;
	lut_data_10bit_p[0x09] = 0x254;
	lut_data_10bit_p[0x0A] = 0x154;
	lut_data_10bit_p[0x0B] = 0x344;
	lut_data_10bit_p[0x0C] = 0x0D4;
	lut_data_10bit_p[0x0D] = 0x2C4;
	lut_data_10bit_p[0x0E] = 0x1C4;
	lut_data_10bit_p[0x0F] = 0x28B;
	lut_data_10bit_p[0x10] = 0x24B;
	lut_data_10bit_p[0x11] = 0x234;
	lut_data_10bit_p[0x12] = 0x134;
	lut_data_10bit_p[0x13] = 0x324;
	lut_data_10bit_p[0x14] = 0x0B4;
	lut_data_10bit_p[0x15] = 0x2A4;
	lut_data_10bit_p[0x16] = 0x1A4;
	lut_data_10bit_p[0x17] = 0x05B;
	lut_data_10bit_p[0x18] = 0x0CB;
	lut_data_10bit_p[0x19] = 0x264;
	lut_data_10bit_p[0x1A] = 0x164;
	lut_data_10bit_p[0x1B] = 0x09B;
	lut_data_10bit_p[0x1C] = 0x0E4;
	lut_data_10bit_p[0x1D] = 0x11B;
	lut_data_10bit_p[0x1E] = 0x21B;
	lut_data_10bit_p[0x1F] = 0x14B;
	lut_data_10bit_p[0x20] = 0x189;
	lut_data_10bit_p[0x21] = 0x229;
	lut_data_10bit_p[0x22] = 0x129;
	lut_data_10bit_p[0x23] = 0x319;
	lut_data_10bit_p[0x24] = 0x0A9;
	lut_data_10bit_p[0x25] = 0x299;
	lut_data_10bit_p[0x26] = 0x199;
	lut_data_10bit_p[0x27] = 0x079;
	lut_data_10bit_p[0x28] = 0x069;
	lut_data_10bit_p[0x29] = 0x259;
	lut_data_10bit_p[0x2A] = 0x159;
	lut_data_10bit_p[0x2B] = 0x349;
	lut_data_10bit_p[0x2C] = 0x0D9;
	lut_data_10bit_p[0x2D] = 0x2C9;
	lut_data_10bit_p[0x2E] = 0x1C9;
	lut_data_10bit_p[0x2F] = 0x289;
	lut_data_10bit_p[0x30] = 0x249;
	lut_data_10bit_p[0x31] = 0x239;
	lut_data_10bit_p[0x32] = 0x139;
	lut_data_10bit_p[0x33] = 0x329;
	lut_data_10bit_p[0x34] = 0x0B9;
	lut_data_10bit_p[0x35] = 0x2A9;
	lut_data_10bit_p[0x36] = 0x1A9;
	lut_data_10bit_p[0x37] = 0x059;
	lut_data_10bit_p[0x38] = 0x0C9;
	lut_data_10bit_p[0x39] = 0x269;
	lut_data_10bit_p[0x3A] = 0x169;
	lut_data_10bit_p[0x3B] = 0x099;
	lut_data_10bit_p[0x3C] = 0x0E9;
	lut_data_10bit_p[0x3D] = 0x119;
	lut_data_10bit_p[0x3E] = 0x219;
	lut_data_10bit_p[0x3F] = 0x149;
	lut_data_10bit_p[0x40] = 0x185;
	lut_data_10bit_p[0x41] = 0x225;
	lut_data_10bit_p[0x42] = 0x125;
	lut_data_10bit_p[0x43] = 0x315;
	lut_data_10bit_p[0x44] = 0x0A5;
	lut_data_10bit_p[0x45] = 0x295;
	lut_data_10bit_p[0x46] = 0x195;
	lut_data_10bit_p[0x47] = 0x075;
	lut_data_10bit_p[0x48] = 0x065;
	lut_data_10bit_p[0x49] = 0x255;
	lut_data_10bit_p[0x4A] = 0x155;
	lut_data_10bit_p[0x4B] = 0x345;
	lut_data_10bit_p[0x4C] = 0x0D5;
	lut_data_10bit_p[0x4D] = 0x2C5;
	lut_data_10bit_p[0x4E] = 0x1C5;
	lut_data_10bit_p[0x4F] = 0x285;
	lut_data_10bit_p[0x50] = 0x245;
	lut_data_10bit_p[0x51] = 0x235;
	lut_data_10bit_p[0x52] = 0x135;
	lut_data_10bit_p[0x53] = 0x325;
	lut_data_10bit_p[0x54] = 0x0B5;
	lut_data_10bit_p[0x55] = 0x2A5;
	lut_data_10bit_p[0x56] = 0x1A5;
	lut_data_10bit_p[0x57] = 0x055;
	lut_data_10bit_p[0x58] = 0x0C5;
	lut_data_10bit_p[0x59] = 0x265;
	lut_data_10bit_p[0x5A] = 0x165;
	lut_data_10bit_p[0x5B] = 0x095;
	lut_data_10bit_p[0x5C] = 0x0E5;
	lut_data_10bit_p[0x5D] = 0x115;
	lut_data_10bit_p[0x5E] = 0x215;
	lut_data_10bit_p[0x5F] = 0x145;
	lut_data_10bit_p[0x60] = 0x18C;
	lut_data_10bit_p[0x61] = 0x22C;
	lut_data_10bit_p[0x62] = 0x12C;
	lut_data_10bit_p[0x63] = 0x313;
	lut_data_10bit_p[0x64] = 0x0AC;
	lut_data_10bit_p[0x65] = 0x293;
	lut_data_10bit_p[0x66] = 0x193;
	lut_data_10bit_p[0x67] = 0x073;
	lut_data_10bit_p[0x68] = 0x06C;
	lut_data_10bit_p[0x69] = 0x253;
	lut_data_10bit_p[0x6A] = 0x153;
	lut_data_10bit_p[0x6B] = 0x343;
	lut_data_10bit_p[0x6C] = 0x0D3;
	lut_data_10bit_p[0x6D] = 0x2C3;
	lut_data_10bit_p[0x6E] = 0x1C3;
	lut_data_10bit_p[0x6F] = 0x28C;
	lut_data_10bit_p[0x70] = 0x24C;
	lut_data_10bit_p[0x71] = 0x233;
	lut_data_10bit_p[0x72] = 0x133;
	lut_data_10bit_p[0x73] = 0x323;
	lut_data_10bit_p[0x74] = 0x0B3;
	lut_data_10bit_p[0x75] = 0x2A3;
	lut_data_10bit_p[0x76] = 0x1A3;
	lut_data_10bit_p[0x77] = 0x05C;
	lut_data_10bit_p[0x78] = 0x0CC;
	lut_data_10bit_p[0x79] = 0x263;
	lut_data_10bit_p[0x7A] = 0x163;
	lut_data_10bit_p[0x7B] = 0x09C;
	lut_data_10bit_p[0x7C] = 0x0E3;
	lut_data_10bit_p[0x7D] = 0x11C;
	lut_data_10bit_p[0x7E] = 0x21C;
	lut_data_10bit_p[0x7F] = 0x14C;
	lut_data_10bit_p[0x80] = 0x18D;
	lut_data_10bit_p[0x81] = 0x22D;
	lut_data_10bit_p[0x82] = 0x12D;
	lut_data_10bit_p[0x83] = 0x312;
	lut_data_10bit_p[0x84] = 0x0AD;
	lut_data_10bit_p[0x85] = 0x292;
	lut_data_10bit_p[0x86] = 0x192;
	lut_data_10bit_p[0x87] = 0x072;
	lut_data_10bit_p[0x88] = 0x06D;
	lut_data_10bit_p[0x89] = 0x252;
	lut_data_10bit_p[0x8A] = 0x152;
	lut_data_10bit_p[0x8B] = 0x342;
	lut_data_10bit_p[0x8C] = 0x0D2;
	lut_data_10bit_p[0x8D] = 0x2C2;
	lut_data_10bit_p[0x8E] = 0x1C2;
	lut_data_10bit_p[0x8F] = 0x28D;
	lut_data_10bit_p[0x90] = 0x24D;
	lut_data_10bit_p[0x91] = 0x232;
	lut_data_10bit_p[0x92] = 0x132;
	lut_data_10bit_p[0x93] = 0x322;
	lut_data_10bit_p[0x94] = 0x0B2;
	lut_data_10bit_p[0x95] = 0x2A2;
	lut_data_10bit_p[0x96] = 0x1A2;
	lut_data_10bit_p[0x97] = 0x05D;
	lut_data_10bit_p[0x98] = 0x0CD;
	lut_data_10bit_p[0x99] = 0x262;
	lut_data_10bit_p[0x9A] = 0x162;
	lut_data_10bit_p[0x9B] = 0x09D;
	lut_data_10bit_p[0x9C] = 0x0E2;
	lut_data_10bit_p[0x9D] = 0x11D;
	lut_data_10bit_p[0x9E] = 0x21D;
	lut_data_10bit_p[0x9F] = 0x14D;
	lut_data_10bit_p[0xA0] = 0x18A;
	lut_data_10bit_p[0xA1] = 0x22A;
	lut_data_10bit_p[0xA2] = 0x12A;
	lut_data_10bit_p[0xA3] = 0x31A;
	lut_data_10bit_p[0xA4] = 0x0AA;
	lut_data_10bit_p[0xA5] = 0x29A;
	lut_data_10bit_p[0xA6] = 0x19A;
	lut_data_10bit_p[0xA7] = 0x07A;
	lut_data_10bit_p[0xA8] = 0x06A;
	lut_data_10bit_p[0xA9] = 0x25A;
	lut_data_10bit_p[0xAA] = 0x15A;
	lut_data_10bit_p[0xAB] = 0x34A;
	lut_data_10bit_p[0xAC] = 0x0DA;
	lut_data_10bit_p[0xAD] = 0x2CA;
	lut_data_10bit_p[0xAE] = 0x1CA;
	lut_data_10bit_p[0xAF] = 0x28A;
	lut_data_10bit_p[0xB0] = 0x24A;
	lut_data_10bit_p[0xB1] = 0x23A;
	lut_data_10bit_p[0xB2] = 0x13A;
	lut_data_10bit_p[0xB3] = 0x32A;
	lut_data_10bit_p[0xB4] = 0x0BA;
	lut_data_10bit_p[0xB5] = 0x2AA;
	lut_data_10bit_p[0xB6] = 0x1AA;
	lut_data_10bit_p[0xB7] = 0x05A;
	lut_data_10bit_p[0xB8] = 0x0CA;
	lut_data_10bit_p[0xB9] = 0x26A;
	lut_data_10bit_p[0xBA] = 0x16A;
	lut_data_10bit_p[0xBB] = 0x09A;
	lut_data_10bit_p[0xBC] = 0x0EA;
	lut_data_10bit_p[0xBD] = 0x11A;
	lut_data_10bit_p[0xBE] = 0x21A;
	lut_data_10bit_p[0xBF] = 0x14A;
	lut_data_10bit_p[0xC0] = 0x186;
	lut_data_10bit_p[0xC1] = 0x226;
	lut_data_10bit_p[0xC2] = 0x126;
	lut_data_10bit_p[0xC3] = 0x316;
	lut_data_10bit_p[0xC4] = 0x0A6;
	lut_data_10bit_p[0xC5] = 0x296;
	lut_data_10bit_p[0xC6] = 0x196;
	lut_data_10bit_p[0xC7] = 0x076;
	lut_data_10bit_p[0xC8] = 0x066;
	lut_data_10bit_p[0xC9] = 0x256;
	lut_data_10bit_p[0xCA] = 0x156;
	lut_data_10bit_p[0xCB] = 0x346;
	lut_data_10bit_p[0xCC] = 0x0D6;
	lut_data_10bit_p[0xCD] = 0x2C6;
	lut_data_10bit_p[0xCE] = 0x1C6;
	lut_data_10bit_p[0xCF] = 0x286;
	lut_data_10bit_p[0xD0] = 0x246;
	lut_data_10bit_p[0xD1] = 0x236;
	lut_data_10bit_p[0xD2] = 0x136;
	lut_data_10bit_p[0xD3] = 0x326;
	lut_data_10bit_p[0xD4] = 0x0B6;
	lut_data_10bit_p[0xD5] = 0x2A6;
	lut_data_10bit_p[0xD6] = 0x1A6;
	lut_data_10bit_p[0xD7] = 0x056;
	lut_data_10bit_p[0xD8] = 0x0C6;
	lut_data_10bit_p[0xD9] = 0x266;
	lut_data_10bit_p[0xDA] = 0x166;
	lut_data_10bit_p[0xDB] = 0x096;
	lut_data_10bit_p[0xDC] = 0x0E6;
	lut_data_10bit_p[0xDD] = 0x116;
	lut_data_10bit_p[0xDE] = 0x216;
	lut_data_10bit_p[0xDF] = 0x146;
	lut_data_10bit_p[0xE0] = 0x18E;
	lut_data_10bit_p[0xE1] = 0x22E;
	lut_data_10bit_p[0xE2] = 0x12E;
	lut_data_10bit_p[0xE3] = 0x311;
	lut_data_10bit_p[0xE4] = 0x0AE;
	lut_data_10bit_p[0xE5] = 0x291;
	lut_data_10bit_p[0xE6] = 0x191;
	lut_data_10bit_p[0xE7] = 0x071;
	lut_data_10bit_p[0xE8] = 0x06E;
	lut_data_10bit_p[0xE9] = 0x251;
	lut_data_10bit_p[0xEA] = 0x151;
	lut_data_10bit_p[0xEB] = 0x348;
	lut_data_10bit_p[0xEC] = 0x0D1;
	lut_data_10bit_p[0xED] = 0x2C8;
	lut_data_10bit_p[0xEE] = 0x1C8;
	lut_data_10bit_p[0xEF] = 0x28E;
	lut_data_10bit_p[0xF0] = 0x24E;
	lut_data_10bit_p[0xF1] = 0x231;
	lut_data_10bit_p[0xF2] = 0x131;
	lut_data_10bit_p[0xF3] = 0x321;
	lut_data_10bit_p[0xF4] = 0x0B1;
	lut_data_10bit_p[0xF5] = 0x2A1;
	lut_data_10bit_p[0xF6] = 0x1A1;
	lut_data_10bit_p[0xF7] = 0x05E;
	lut_data_10bit_p[0xF8] = 0x0CE;
	lut_data_10bit_p[0xF9] = 0x261;
	lut_data_10bit_p[0xFA] = 0x161;
	lut_data_10bit_p[0xFB] = 0x09E;
	lut_data_10bit_p[0xFC] = 0x0E1;
	lut_data_10bit_p[0xFD] = 0x11E;
	lut_data_10bit_p[0xFE] = 0x21E;
	lut_data_10bit_p[0xFF] = 0x14E;

	lut_data_10bit_n[0x00] = 0x274;
	lut_data_10bit_n[0x01] = 0x1D4;
	lut_data_10bit_n[0x02] = 0x2D4;
	lut_data_10bit_n[0x03] = 0x31B;
	lut_data_10bit_n[0x04] = 0x354;
	lut_data_10bit_n[0x05] = 0x29B;
	lut_data_10bit_n[0x06] = 0x19B;
	lut_data_10bit_n[0x07] = 0x38B;
	lut_data_10bit_n[0x08] = 0x394;
	lut_data_10bit_n[0x09] = 0x25B;
	lut_data_10bit_n[0x0A] = 0x15B;
	lut_data_10bit_n[0x0B] = 0x34B;
	lut_data_10bit_n[0x0C] = 0x0DB;
	lut_data_10bit_n[0x0D] = 0x2CB;
	lut_data_10bit_n[0x0E] = 0x1CB;
	lut_data_10bit_n[0x0F] = 0x174;
	lut_data_10bit_n[0x10] = 0x1B4;
	lut_data_10bit_n[0x11] = 0x23B;
	lut_data_10bit_n[0x12] = 0x13B;
	lut_data_10bit_n[0x13] = 0x32B;
	lut_data_10bit_n[0x14] = 0x0BB;
	lut_data_10bit_n[0x15] = 0x2AB;
	lut_data_10bit_n[0x16] = 0x1AB;
	lut_data_10bit_n[0x17] = 0x3A4;
	lut_data_10bit_n[0x18] = 0x334;
	lut_data_10bit_n[0x19] = 0x26B;
	lut_data_10bit_n[0x1A] = 0x16B;
	lut_data_10bit_n[0x1B] = 0x364;
	lut_data_10bit_n[0x1C] = 0x0EB;
	lut_data_10bit_n[0x1D] = 0x2E4;
	lut_data_10bit_n[0x1E] = 0x1E4;
	lut_data_10bit_n[0x1F] = 0x2B4;
	lut_data_10bit_n[0x20] = 0x279;
	lut_data_10bit_n[0x21] = 0x1D9;
	lut_data_10bit_n[0x22] = 0x2D9;
	lut_data_10bit_n[0x23] = 0x319;
	lut_data_10bit_n[0x24] = 0x359;
	lut_data_10bit_n[0x25] = 0x299;
	lut_data_10bit_n[0x26] = 0x199;
	lut_data_10bit_n[0x27] = 0x389;
	lut_data_10bit_n[0x28] = 0x399;
	lut_data_10bit_n[0x29] = 0x259;
	lut_data_10bit_n[0x2A] = 0x159;
	lut_data_10bit_n[0x2B] = 0x349;
	lut_data_10bit_n[0x2C] = 0x0D9;
	lut_data_10bit_n[0x2D] = 0x2C9;
	lut_data_10bit_n[0x2E] = 0x1C9;
	lut_data_10bit_n[0x2F] = 0x179;
	lut_data_10bit_n[0x30] = 0x1B9;
	lut_data_10bit_n[0x31] = 0x239;
	lut_data_10bit_n[0x32] = 0x139;
	lut_data_10bit_n[0x33] = 0x329;
	lut_data_10bit_n[0x34] = 0x0B9;
	lut_data_10bit_n[0x35] = 0x2A9;
	lut_data_10bit_n[0x36] = 0x1A9;
	lut_data_10bit_n[0x37] = 0x3A9;
	lut_data_10bit_n[0x38] = 0x339;
	lut_data_10bit_n[0x39] = 0x269;
	lut_data_10bit_n[0x3A] = 0x169;
	lut_data_10bit_n[0x3B] = 0x369;
	lut_data_10bit_n[0x3C] = 0x0E9;
	lut_data_10bit_n[0x3D] = 0x2E9;
	lut_data_10bit_n[0x3E] = 0x1E9;
	lut_data_10bit_n[0x3F] = 0x2B9;
	lut_data_10bit_n[0x40] = 0x275;
	lut_data_10bit_n[0x41] = 0x1D5;
	lut_data_10bit_n[0x42] = 0x2D5;
	lut_data_10bit_n[0x43] = 0x315;
	lut_data_10bit_n[0x44] = 0x355;
	lut_data_10bit_n[0x45] = 0x295;
	lut_data_10bit_n[0x46] = 0x195;
	lut_data_10bit_n[0x47] = 0x385;
	lut_data_10bit_n[0x48] = 0x395;
	lut_data_10bit_n[0x49] = 0x255;
	lut_data_10bit_n[0x4A] = 0x155;
	lut_data_10bit_n[0x4B] = 0x345;
	lut_data_10bit_n[0x4C] = 0x0D5;
	lut_data_10bit_n[0x4D] = 0x2C5;
	lut_data_10bit_n[0x4E] = 0x1C5;
	lut_data_10bit_n[0x4F] = 0x175;
	lut_data_10bit_n[0x50] = 0x1B5;
	lut_data_10bit_n[0x51] = 0x235;
	lut_data_10bit_n[0x52] = 0x135;
	lut_data_10bit_n[0x53] = 0x325;
	lut_data_10bit_n[0x54] = 0x0B5;
	lut_data_10bit_n[0x55] = 0x2A5;
	lut_data_10bit_n[0x56] = 0x1A5;
	lut_data_10bit_n[0x57] = 0x3A5;
	lut_data_10bit_n[0x58] = 0x335;
	lut_data_10bit_n[0x59] = 0x265;
	lut_data_10bit_n[0x5A] = 0x165;
	lut_data_10bit_n[0x5B] = 0x365;
	lut_data_10bit_n[0x5C] = 0x0E5;
	lut_data_10bit_n[0x5D] = 0x2E5;
	lut_data_10bit_n[0x5E] = 0x1E5;
	lut_data_10bit_n[0x5F] = 0x2B5;
	lut_data_10bit_n[0x60] = 0x273;
	lut_data_10bit_n[0x61] = 0x1D3;
	lut_data_10bit_n[0x62] = 0x2D3;
	lut_data_10bit_n[0x63] = 0x31C;
	lut_data_10bit_n[0x64] = 0x353;
	lut_data_10bit_n[0x65] = 0x29C;
	lut_data_10bit_n[0x66] = 0x19C;
	lut_data_10bit_n[0x67] = 0x38C;
	lut_data_10bit_n[0x68] = 0x393;
	lut_data_10bit_n[0x69] = 0x25C;
	lut_data_10bit_n[0x6A] = 0x15C;
	lut_data_10bit_n[0x6B] = 0x34C;
	lut_data_10bit_n[0x6C] = 0x0DC;
	lut_data_10bit_n[0x6D] = 0x2CC;
	lut_data_10bit_n[0x6E] = 0x1CC;
	lut_data_10bit_n[0x6F] = 0x173;
	lut_data_10bit_n[0x70] = 0x1B3;
	lut_data_10bit_n[0x71] = 0x23C;
	lut_data_10bit_n[0x72] = 0x13C;
	lut_data_10bit_n[0x73] = 0x32C;
	lut_data_10bit_n[0x74] = 0x0BC;
	lut_data_10bit_n[0x75] = 0x2AC;
	lut_data_10bit_n[0x76] = 0x1AC;
	lut_data_10bit_n[0x77] = 0x3A3;
	lut_data_10bit_n[0x78] = 0x333;
	lut_data_10bit_n[0x79] = 0x26C;
	lut_data_10bit_n[0x7A] = 0x16C;
	lut_data_10bit_n[0x7B] = 0x363;
	lut_data_10bit_n[0x7C] = 0x0EC;
	lut_data_10bit_n[0x7D] = 0x2E3;
	lut_data_10bit_n[0x7E] = 0x1E3;
	lut_data_10bit_n[0x7F] = 0x2B3;
	lut_data_10bit_n[0x80] = 0x272;
	lut_data_10bit_n[0x81] = 0x1D2;
	lut_data_10bit_n[0x82] = 0x2D2;
	lut_data_10bit_n[0x83] = 0x31D;
	lut_data_10bit_n[0x84] = 0x352;
	lut_data_10bit_n[0x85] = 0x29D;
	lut_data_10bit_n[0x86] = 0x19D;
	lut_data_10bit_n[0x87] = 0x38D;
	lut_data_10bit_n[0x88] = 0x392;
	lut_data_10bit_n[0x89] = 0x25D;
	lut_data_10bit_n[0x8A] = 0x15D;
	lut_data_10bit_n[0x8B] = 0x34D;
	lut_data_10bit_n[0x8C] = 0x0DD;
	lut_data_10bit_n[0x8D] = 0x2CD;
	lut_data_10bit_n[0x8E] = 0x1CD;
	lut_data_10bit_n[0x8F] = 0x172;
	lut_data_10bit_n[0x90] = 0x1B2;
	lut_data_10bit_n[0x91] = 0x23D;
	lut_data_10bit_n[0x92] = 0x13D;
	lut_data_10bit_n[0x93] = 0x32D;
	lut_data_10bit_n[0x94] = 0x0BD;
	lut_data_10bit_n[0x95] = 0x2AD;
	lut_data_10bit_n[0x96] = 0x1AD;
	lut_data_10bit_n[0x97] = 0x3A2;
	lut_data_10bit_n[0x98] = 0x332;
	lut_data_10bit_n[0x99] = 0x26D;
	lut_data_10bit_n[0x9A] = 0x16D;
	lut_data_10bit_n[0x9B] = 0x362;
	lut_data_10bit_n[0x9C] = 0x0ED;
	lut_data_10bit_n[0x9D] = 0x2E2;
	lut_data_10bit_n[0x9E] = 0x1E2;
	lut_data_10bit_n[0x9F] = 0x2B2;
	lut_data_10bit_n[0xA0] = 0x27A;
	lut_data_10bit_n[0xA1] = 0x1DA;
	lut_data_10bit_n[0xA2] = 0x2DA;
	lut_data_10bit_n[0xA3] = 0x31A;
	lut_data_10bit_n[0xA4] = 0x35A;
	lut_data_10bit_n[0xA5] = 0x29A;
	lut_data_10bit_n[0xA6] = 0x19A;
	lut_data_10bit_n[0xA7] = 0x38A;
	lut_data_10bit_n[0xA8] = 0x39A;
	lut_data_10bit_n[0xA9] = 0x25A;
	lut_data_10bit_n[0xAA] = 0x15A;
	lut_data_10bit_n[0xAB] = 0x34A;
	lut_data_10bit_n[0xAC] = 0x0DA;
	lut_data_10bit_n[0xAD] = 0x2CA;
	lut_data_10bit_n[0xAE] = 0x1CA;
	lut_data_10bit_n[0xAF] = 0x17A;
	lut_data_10bit_n[0xB0] = 0x1BA;
	lut_data_10bit_n[0xB1] = 0x23A;
	lut_data_10bit_n[0xB2] = 0x13A;
	lut_data_10bit_n[0xB3] = 0x32A;
	lut_data_10bit_n[0xB4] = 0x0BA;
	lut_data_10bit_n[0xB5] = 0x2AA;
	lut_data_10bit_n[0xB6] = 0x1AA;
	lut_data_10bit_n[0xB7] = 0x3AA;
	lut_data_10bit_n[0xB8] = 0x33A;
	lut_data_10bit_n[0xB9] = 0x26A;
	lut_data_10bit_n[0xBA] = 0x16A;
	lut_data_10bit_n[0xBB] = 0x36A;
	lut_data_10bit_n[0xBC] = 0x0EA;
	lut_data_10bit_n[0xBD] = 0x2EA;
	lut_data_10bit_n[0xBE] = 0x1EA;
	lut_data_10bit_n[0xBF] = 0x2BA;
	lut_data_10bit_n[0xC0] = 0x276;
	lut_data_10bit_n[0xC1] = 0x1D6;
	lut_data_10bit_n[0xC2] = 0x2D6;
	lut_data_10bit_n[0xC3] = 0x316;
	lut_data_10bit_n[0xC4] = 0x356;
	lut_data_10bit_n[0xC5] = 0x296;
	lut_data_10bit_n[0xC6] = 0x196;
	lut_data_10bit_n[0xC7] = 0x386;
	lut_data_10bit_n[0xC8] = 0x396;
	lut_data_10bit_n[0xC9] = 0x256;
	lut_data_10bit_n[0xCA] = 0x156;
	lut_data_10bit_n[0xCB] = 0x346;
	lut_data_10bit_n[0xCC] = 0x0D6;
	lut_data_10bit_n[0xCD] = 0x2C6;
	lut_data_10bit_n[0xCE] = 0x1C6;
	lut_data_10bit_n[0xCF] = 0x176;
	lut_data_10bit_n[0xD0] = 0x1B6;
	lut_data_10bit_n[0xD1] = 0x236;
	lut_data_10bit_n[0xD2] = 0x136;
	lut_data_10bit_n[0xD3] = 0x326;
	lut_data_10bit_n[0xD4] = 0x0B6;
	lut_data_10bit_n[0xD5] = 0x2A6;
	lut_data_10bit_n[0xD6] = 0x1A6;
	lut_data_10bit_n[0xD7] = 0x3A6;
	lut_data_10bit_n[0xD8] = 0x336;
	lut_data_10bit_n[0xD9] = 0x266;
	lut_data_10bit_n[0xDA] = 0x166;
	lut_data_10bit_n[0xDB] = 0x366;
	lut_data_10bit_n[0xDC] = 0x0E6;
	lut_data_10bit_n[0xDD] = 0x2E6;
	lut_data_10bit_n[0xDE] = 0x1E6;
	lut_data_10bit_n[0xDF] = 0x2B6;
	lut_data_10bit_n[0xE0] = 0x271;
	lut_data_10bit_n[0xE1] = 0x1D1;
	lut_data_10bit_n[0xE2] = 0x2D1;
	lut_data_10bit_n[0xE3] = 0x31E;
	lut_data_10bit_n[0xE4] = 0x351;
	lut_data_10bit_n[0xE5] = 0x29E;
	lut_data_10bit_n[0xE6] = 0x19E;
	lut_data_10bit_n[0xE7] = 0x38E;
	lut_data_10bit_n[0xE8] = 0x391;
	lut_data_10bit_n[0xE9] = 0x25E;
	lut_data_10bit_n[0xEA] = 0x15E;
	lut_data_10bit_n[0xEB] = 0x34E;
	lut_data_10bit_n[0xEC] = 0x0DE;
	lut_data_10bit_n[0xED] = 0x2CE;
	lut_data_10bit_n[0xEE] = 0x1CE;
	lut_data_10bit_n[0xEF] = 0x171;
	lut_data_10bit_n[0xF0] = 0x1B1;
	lut_data_10bit_n[0xF1] = 0x237;
	lut_data_10bit_n[0xF2] = 0x137;
	lut_data_10bit_n[0xF3] = 0x32E;
	lut_data_10bit_n[0xF4] = 0x0B7;
	lut_data_10bit_n[0xF5] = 0x2AE;
	lut_data_10bit_n[0xF6] = 0x1AE;
	lut_data_10bit_n[0xF7] = 0x3A1;
	lut_data_10bit_n[0xF8] = 0x331;
	lut_data_10bit_n[0xF9] = 0x26E;
	lut_data_10bit_n[0xFA] = 0x16E;
	lut_data_10bit_n[0xFB] = 0x361;
	lut_data_10bit_n[0xFC] = 0x0EE;
	lut_data_10bit_n[0xFD] = 0x2E1;
	lut_data_10bit_n[0xFE] = 0x1E1;
	lut_data_10bit_n[0xFF] = 0x2B1;

}

