// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CarbonXPciePhyTx_H_
#define __CarbonXPciePhyTx_H_

#include "carbonXPcie.h"
#include "CarbonX8b10b.h"
#include "CarbonXPcieOS.h"
#include "CarbonXPcieScrambler.h"
#include "CarbonXPciePhyLayer.h"
#include "CarbonXPcieIfBase.h"
#include "PCIETransaction.h"

class MsgContext;
class CarbonXPcieConfig;

class CarbonXPciePhyTx {
  
public: CARBONMEM_OVERRIDES;
    
  CarbonXPciePhyTx(CarbonXPcieConfig& cfg, CarbonXPcieIntfType intf, UInt32 linkwidth, 
		   const CarbonXString & inPrintName, const CarbonXString & currTimeString,
		   CarbonXPcieIfBase &signal_if,
                   MsgContext * msg_context);

  // -------------------------
  // Public Interfaces

  //! This method needs to get called ever symbol period
  void step();

  //! Tell Transmitter to send a Physival Packet DLLP or LLTP 
  bool sendPacket(PCIETransaction *trans);

  //! Set LTSSM State
  void setLinkState(pcie_LTSM_state_t state);
  
  //! Get LTSSM State
  pcie_LTSM_state_t getLinkState() const;

  //! Check if there are any packets pending for transmission
  bool hasPendingPacket() const;

  //! Check if there are any packets pending for transmission
  bool packetInProgress() const;

  //! Check if we have sent enough idles
  bool isIdle(UInt32 idles) const {
    return (mIdleTimer >= idles);
  }

  //! Set Link Number
  void setLinkNum(UInt8 num) {mLinkNum = num; }

  //! Set Number of FTSs
  void setNFts(UInt8 num) {mNFts = num; }
  
  //! Set Data Rate
  void setDataRate(UInt8 rate) {
    mDataRate = rate;
  }

  //! Set Training Control bits
  void setTrainCtrl(UInt8 val) {
    mTrainCtrl = val;
  }

  //! Clear Ordered Set Send Count
  void clearOsCount();
  
  //! Get Number of Os's sent since last State Change
  UInt32 getOsCount();
  
  //! Returns true if Transmitter has no more data to send
  bool isDone() const;
  
  //! Set Transaction Done Callback function
  void setTxReqTransCBFn(CarbonXPciePhyLayer::TxReqTransCBFn *fn, void *info);
  
  //! Reset Phy
  void reset();

  //! Set Debug Level
  void verbose(UInt32);

  // PCI Express Parameters

  //! Configure Phy as Upstream Device or DownStream Device
  /*
    /param en true will configure Phy as Upstream Device. false will configure Phy as Downstream Device
  */
  void setUpstreamDevice(bool en);

private:
  bool getPacket(UInt32 curr_lane);

  CarbonXPcieConfig&  mCfg;
  CarbonXPcieIntfType mIntfType;
  UInt32              mLinkWidth;
  const CarbonXString      mPrintName;          // Name of the transactor
  const CarbonXString &    mCurrTimeString;     // c-string with the mCurrTick scaled to mCurrTimescale

  // Signal Interface
  CarbonXPcieIfBase & mSignalIf;

  // Transactor Parameters
  UInt8             mLinkNum;
  UInt8             mNFts;
  UInt8             mDataRate;
  UInt8             mTrainCtrl;
  
  // Callback Parameters
  CarbonXPciePhyLayer::TxReqTransCBFn *mReqTransCBFn;
  void *            mReqTransCBInfo;

  // Phy Control
  bool                mEIdleActiveHigh;
  pcie_LTSM_state_t   mLinkState;
  bool                mUpstreamDevice;
  CarbonXPcieOS       mCurrOs;
  PCIETransaction *   mCurrPkt;
  PCIETransaction  *  mPendPkt;
  UInt32              mSkipTimer;
  UInt32              mSkipTimeout;
  UInt32              mIdleTimer;
  UInt32              mNumOssSent;
  bool                mPhysicalLinkUp;
  bool                mEIdleSent;

  // Debug Parameters
  bool VerboseTransmitterHigh;    // print out a LOT of Transmitter info
  bool VerboseTransmitterMedium;  // print out a bit of Transmitter info
  bool VerboseTransmitterLow;     // print out some Transmitter info

  MsgContext * mMsgContext;
};

#endif
