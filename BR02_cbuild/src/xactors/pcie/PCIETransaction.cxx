// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <cstdio>
#include <cstdlib>
#include "CarbonXPcieTLP.h"
#include "PCIETransaction.h"
#include "CarbonXPcieTransactor.h"

PCIETransaction::PCIETransaction(CarbonXPcieTransactor *xtor)
   : mXtor(xtor), mPcieCmd(0), mIsValid(false), mIsSpecial(true), mIsCpl(false), mIsAck(false), mIsNak(false), mIsTLP(false),
    mMustStartLane0(false), mDone(false), mNeedSeqNumAndLCRC(false), mBytesSent(0), mBytesTotal(0),
    runOutputState(Active), DLLP_SeqNum(0xDEADBEEF), expectAck(false),
    expectCpl(false), transID(0), canNotReplay(false), doReplay(false), numRetries(0),
    fcType(None), fcHeader(0), fcData(0), fcCreditsConsumed(true),
    mCompletionFunc(0)
{
   strcpy(printString, "");
   
   // This constructor is meant to be used by classes that inherits this class
   mIsValid = false;
}

PCIETransaction::PCIETransaction(CarbonXPcieTransactor *xtor, UInt32 idles)
  : mXtor(xtor), mPcieCmd(0), mIsValid(true), mIsSpecial(true), mIsCpl(false), mIsAck(false), mIsNak(false), mIsTLP(false),
    mMustStartLane0(false), mDone(false), mNeedSeqNumAndLCRC(false), mBytesSent(0), mBytesTotal(0),
    runOutputState(Active), DLLP_SeqNum(0xDEADBEEF), expectAck(false),
    expectCpl(false), transID(0), canNotReplay(false), doReplay(false), numRetries(0),
    fcType(None), fcHeader(0), fcData(0), fcCreditsConsumed(true),
    mCompletionFunc(0)
{
   initializeTransactionSpecial(xtor,idles);
}

PCIETransaction::PCIETransaction(CarbonXPcieTransactor *xtor, const CarbonXPcieTLP *cmd)
  : mXtor(xtor), mPcieCmd(0), mIsValid(true), mIsSpecial(false), mIsCpl(false), mIsAck(false), mIsNak(false), mIsTLP(true),
    mMustStartLane0(false), mDone(false), mNeedSeqNumAndLCRC(true), mBytesSent(0), mBytesTotal(0),
    runOutputState(Active), DLLP_SeqNum(0xDEADBEEF), expectAck(false),
    expectCpl(false), transID(0), canNotReplay(false), doReplay(false), numRetries(0),
    fcType(None), fcHeader(0), fcData(0), fcCreditsConsumed(false),
    mCompletionFunc(0)
{
   if (cmd == 0) {  // passed in a Zero pointer for the command
      if(xtor->getCfg().getVerboseRxLow())
	 printf("%s  ERROR: Problem initializing PCIExpress transaction.  Possibly a bad CarbonXPcieTLP passed in.\n", xtor->getCfg().mPrintName.c_str());
      mDone = true;
      mIsValid = false;
      return;
   }

   strcpy(printString, "");
   initializeTransaction(xtor, cmd);
   mPcieCmd = new CarbonXPcieTLP(*cmd);  // make a local copy
   return;
}

PCIETransaction::PCIETransaction(CarbonXPcieTransactor *xtor, const CarbonXPcieDLLP &cmd)
  : mXtor(xtor), mPcieCmd(0), mIsValid(true), mIsSpecial(true), mIsCpl(false), mIsAck(false), mIsNak(false), mIsTLP(false),
    mMustStartLane0(false), mDone(false), mNeedSeqNumAndLCRC(false), mBytesSent(0), mBytesTotal(0),
    runOutputState(Active), DLLP_SeqNum(0xDEADBEEF), expectAck(false),
    expectCpl(false), transID(0), canNotReplay(false), doReplay(false), numRetries(0),
    fcType(None), fcHeader(0), fcData(0), fcCreditsConsumed(true),
    mCompletionFunc(0)
{
   if (&cmd == 0) {  // passed in a Zero pointer for the command
      if(xtor->getCfg().getVerboseRxLow())
	 printf("%s  ERROR: Problem initializing PCIExpress transaction.  Possibly a bad CarbonXPcieTLP passed in.\n", xtor->getCfg().mPrintName.c_str());
      mDone = true;
      mIsValid = false;
      return;
   }

   strcpy(printString, "");
   initializeTransaction(xtor, cmd);
   return;
}


PCIETransaction::~PCIETransaction(void) {
   if (mPcieCmd != 0)       delete mPcieCmd;
}


void PCIETransaction::step() {
   UInt32 i;

   if (mBytesSent == 0) {  // first cycle
      if (mNeedSeqNumAndLCRC == true)   // first time playing transaction
	 setSeqNumAndCRC(mXtor);

      if(mXtor->getCfg().getVerboseRxMed()) {
	 printf("%s  Info: Starting Transaction '%s' ", mXtor->getCfg().mPrintName.c_str(), printString);
	 if (not mIsSpecial)
	    printf("(SeqNum=0x%03X) ", DLLP_SeqNum);
	 printf("(at %s)\n", mXtor->getCfg().mCurrTimeString.c_str());
      }
      if(mXtor->getCfg().getVerboseRxHigh()) {
	 printf("%s           Transaction Bytes: ", mXtor->getCfg().mPrintName.c_str());
	 for (i=0; i < mBytesTotal; ++i) {
	    if (phys_bytes_sym[i])
	       printf("  %s", mXtor->getSymStringFromByte(phys_bytes_dat[i]));
	    else
	       printf("  %02X", phys_bytes_dat[i]);
	 }
	 printf("\n");
      }
   }

   if (mDone && doReplay) {
      mBytesSent = 0;
      mDone = false;
      if (not mIsSpecial)
	 expectAck = true;
      numRetries++;
      doReplay = false;
   }
}


void PCIETransaction::getTransmitPackets(UInt32 num, UInt32 * datHolder, UInt32 * symHolder) {
   UInt32 i;

   if ((datHolder == 0) || (symHolder == 0)) {
      printf("%s  ERROR: Received NULL pointer for internal packet transmission buffer.\n", mXtor->getCfg().mPrintName.c_str());
      return;
   }

   // This shouldn't happen, but protect incase it does...
   if (mBytesSent == mBytesTotal) {
      // We must be waiting for something, so just output idle data
      //   The transactor has the ability of replaying this transaction if a Nak is received,
      //   so we'll just hang out until we are replayed or deleted.
      for(i=0; i < num; ++i) {
	 datHolder[i] = 0x00;
	 symHolder[i] = false;
      }
      return;
   }

   for (i = 0; i < num; ++i) {
      if ((i + mBytesSent) >= mBytesTotal) {
	 datHolder[i] = PCIE_PAD;   // fill with PADs
	 symHolder[i] = true;
      } else {
	 datHolder[i] = phys_bytes_dat[(i + mBytesSent)];
	 symHolder[i] = phys_bytes_sym[(i + mBytesSent)];
      }
   }

   mBytesSent += num;
   if (mBytesSent > mBytesTotal)
      mBytesSent = mBytesTotal;


   if (mBytesTotal == mBytesSent) {
      mDone = true;    // we put our last cycle out

      if (mXtor->getCfg().getVerboseRxMed())
	 printf("%s  Info: Finishing Transaction '%s' (at %s)\n",mXtor->getCfg().mPrintName.c_str(), printString, mXtor->getCfg().mCurrTimeString.c_str());
   }

   return;
}


void PCIETransaction::onCompletion(CompletionFunc * func, void * user_arg) {
   if (NULL == func)
      printf("ERROR -- PCIETransaction::onCompletion -- CompletionFunc is NULL\n");
   mCompletionFunc = func;
   mCompletionFuncArg = user_arg;
}


void PCIETransaction::doCompletion(void) {
   UInt32 a;
   a = 0;
   if (mCompletionFunc) {
      (mCompletionFunc)(a,NULL,mCompletionFuncArg);
   }
}


void PCIETransaction::initializeTransactionSpecial(CarbonXPcieTransactor *xtor, UInt32 idles) {

   // setup packets for Idle transactions (not user transactions)
   UInt32 linkWidth;   // link width of xtor
   
   // do not replay DLLP transactions
   canNotReplay = true;
   
   //useScramble = xtor->getCfg().mScrambleEnable;
   linkWidth = xtor->getCfg().mLinkWidth;

   allocPhysBytes(idles * linkWidth);
   strcpy(printString, "Logical Idle");
   
   for(UInt32 i=0; i < (idles * linkWidth); i++) {
      phys_bytes_dat[i] = 0x00;
      phys_bytes_sym[i] = false;
   }
   
   runOutputState = Logical_Idle;
   mMustStartLane0 = true;
}


void PCIETransaction::initializeTransaction(CarbonXPcieTransactor *xtor, const CarbonXPcieTLP *cmd) {
    // move stuff here, do all of the tlp, dllp, phys packet work here instead
    UInt32 tlp_bytes_hdr[16];
    UInt32 tlp_bytes_hdr_used;
    UInt32 tlp_bytes_total;   // # bytes total for the TLP (header + data) [no digest currently]
    UInt32 linkWidth;
    //bool useScramble;
    UInt32 i;

    // PCIE 1.0a spec 2.2.9. Completion Rules:
    // All Read Requests and Non-Posted Write Requests require Completion.
    // (Mem Writes are posted, Cfg & IO Writes are not.)
    switch(cmd->getCmdByName()) {
    case eCarbonXPcieMRd32:   strcpy(printString, "Mem Read 32-bit");             fcType=NonPosted;  break;
    case eCarbonXPcieMRd64:   strcpy(printString, "Mem Read 64-bit");             fcType=NonPosted;  break;
    case eCarbonXPcieMRdLk32: strcpy(printString, "Mem Read Lock 32-bit");        fcType=NonPosted;  break;
    case eCarbonXPcieMRdLk64: strcpy(printString, "Mem Read Lock 64-bit");        fcType=NonPosted;  break;
    case eCarbonXPcieMWr32:   strcpy(printString, "Mem Write 32-bit");            fcType=Posted;     break;
    case eCarbonXPcieMWr64:   strcpy(printString, "Mem Write 64-bit");            fcType=Posted;     break;
    case eCarbonXPcieIORd:    strcpy(printString, "IO Read");                     fcType=NonPosted;  break;
    case eCarbonXPcieIOWr:    strcpy(printString, "IO Write");                    fcType=NonPosted;  break;
    case eCarbonXPcieCfgRd0:  strcpy(printString, "Config Read Type 0");          fcType=NonPosted;  break;
    case eCarbonXPcieCfgWr0:  strcpy(printString, "Config Write Type 0");         fcType=NonPosted;  break;
    case eCarbonXPcieCfgRd1:  strcpy(printString, "Config Read Type 1");          fcType=NonPosted;  break;
    case eCarbonXPcieCfgWr1:  strcpy(printString, "Config Write Type 1");         fcType=NonPosted;  break;
    case eCarbonXPcieMsg:     strcpy(printString, "Message");                     fcType=Posted;     break;
    case eCarbonXPcieMsgD:    strcpy(printString, "Message with Data");           fcType=Posted;     break;
    case eCarbonXPcieCpl:     strcpy(printString, "Completion");                  fcType=Completion; break;
    case eCarbonXPcieCplD:    strcpy(printString, "Completion with Data");        fcType=Completion; break;
    case eCarbonXPcieCplLk:   strcpy(printString, "Completion Locked");           fcType=Completion; break;
    case eCarbonXPcieCplDLk:  strcpy(printString, "Completion Locked with Data"); fcType=Completion; break;
    default:               strcpy(printString, "Unknown");
    }

    // all 'regular' transactions need to be Acknowledged by the receiver.
    expectAck = true;
    // all TLP transactions take one flow control header unit
    fcHeader = 1;
    // posted transactions don't need completions, nor are they completions
    // nonposted transactions need completions
    // completions are completions
    switch(fcType) {
    case Posted:
       expectCpl = false;
       mIsCpl = false;
       break;
    case NonPosted:
       expectCpl = true;
       mIsCpl = false;
       break;
    case Completion:
       expectCpl = false;
       mIsCpl = true;
       break;
    default:
       break;
    }

    // Transaction Layer Protocol Packet Definition (Generic)
    //unused -  UInt32 mTLP_Fmt;    // Format[1:0]
    //unused -  UInt32 mTLP_Type;   // Type[4:0]
    UInt32 mTLP_TC;     // Traffic Class[2:0]
    UInt32 mTLP_TD;     // TLP Digest Present
    UInt32 mTLP_EP;     // TLP Error - Poisoned TLP
    UInt32 mTLP_Attr;   // Attributes[1:0]
    UInt32 mTLP_Len;    // Length[9:0] - length of data payload in 4-byte Double Words
    // Transaction Layer Protocol Packet Definition (Extended - Memory, I/O, & Configuration Request)
    UInt32 mTLP_ReqID;  // Requester ID[15:0]
    UInt32 mTLP_Tag;    // Tag[7:0]
    UInt32 mTLP_LDWBE;  // Last Double Word Byte Enable[3:0]
    UInt32 mTLP_FDWBE;  // First Double Word Byte Enable[3:0]
    UInt32 mTLP_AddrHi; // Address bits [63:32]
    UInt32 mTLP_AddrLo; // Address bits [31:00]
    // Transaction Layer Protocol Packet Definition (Extended - Completion)
    UInt32 mTLP_CompID; // Completer ID[15:0]
    UInt32 mTLP_CompSt; // Completion Status[2:0]
    UInt32 mTLP_BCM;    // Byte Count Modified (ONLY by PCIX Completers, not set by PCIExpress Completers)
    UInt32 mTLP_BC;     // Byte Count[11:0] - remaining byte count for the request
    UInt32 mTLP_LowAd;  // Lower Address[6:0] - lower byte address for starting byte of Completion
    // Transaction Layer Protocol Packet Definition (Extended - Messages)
    UInt32 mTLP_msgRout; // Message Routing Type[2:0]
    UInt32 mTLP_msgCode; // Message Code[7:0]
    ////////////////////////////////////////////

    //unused - mTLP_Fmt  = cmdParseTlpFmt(cmd->tlp_type);
    //unused - mTLP_Type = cmdParseTlpType(cmd->tlp_type);

// TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED 
    // we don't currently use multiple Traffic Classes      // TO BE COMPLETED     TO BE COMPLETED
    mTLP_TC = 0;    // TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED
    // we don't currently support sending a TLP Digest      // TO BE COMPLETED     TO BE COMPLETED
    mTLP_TD = 0;    // TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED
    // we don't currently support sending Poisoned Packets  // TO BE COMPLETED     TO BE COMPLETED
    mTLP_EP = 0;    // TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED
    // we don't currently support any Packet Attributes     // TO BE COMPLETED     TO BE COMPLETED
    mTLP_Attr = 0;  // TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED
// TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED     TO BE COMPLETED 

    if((cmd->getCmdByName() == eCarbonXPcieIORd)   || (cmd->getCmdByName() == eCarbonXPcieIOWr)   ||
       (cmd->getCmdByName() == eCarbonXPcieCfgRd0) || (cmd->getCmdByName() == eCarbonXPcieCfgWr0) || 
       (cmd->getCmdByName() == eCarbonXPcieCfgRd1) || (cmd->getCmdByName() == eCarbonXPcieCfgWr1)) {
	mTLP_Len  = 0x01;  // Must be 1 DW
	mDataLength = 1;
    } else if((cmd->getCmdByName() == eCarbonXPcieMsg) || (cmd->getCmdByName() == eCarbonXPcieCpl) ||
	      (cmd->getCmdByName() == eCarbonXPcieCplLk)) {
	mTLP_Len  = 0x00;  // No data
	mDataLength = 0;
    } else {
       //       if(cmd->getDWCount() >= 1024)  // max Data Payload = 1024 DWords
       if(cmd->getDataLength() >= 1024) {  // max Data Payload = 1024 DWords
	  mTLP_Len = 0;   // 0 = 1024 DWords
	  mDataLength = 1024;
       } else {
	  //	  mTLP_Len = cmd->getDWCount();  // set Length of Data in 4-byte Double Words
	  mTLP_Len = cmd->getDataLength();  // set Length of Data in 4-byte Double Words
	  mDataLength = mTLP_Len;
       }
    }

    // Requester ID[15:0] = {mBusNum[7:0],mDevNum[4:0],mFuncNum[2:0]}
    if((cmd->getCmdByName() == eCarbonXPcieCpl)   || (cmd->getCmdByName() == eCarbonXPcieCplD) || 
       (cmd->getCmdByName() == eCarbonXPcieCplLk) || (cmd->getCmdByName() == eCarbonXPcieCplDLk) ||
       (cmd->getCmdByName() == eCarbonXPcieMsg)   || (cmd->getCmdByName() == eCarbonXPcieMsgD)) {
       mTLP_ReqID = cmd->getReqId();
       mTLP_Tag = cmd->getTag();
    } else {
       // JJH -- 2006/05/12 -- If the cmd still has the default ReqId, then use the
       //    transactor's version, else, use the ReqId from the cmd.
       if (cmd->getReqId() == 0xFFFF) { 
          mTLP_ReqID = ((xtor->getCfg().mBusNum << 8) + (xtor->getCfg().mDevNum << 3) + xtor->getCfg().mFuncNum);
       } else {
          mTLP_ReqID = cmd->getReqId();
       }
       // GK -- 2006/07/07 -- I'm Changing this to take the tag from the transaction
       if(cmd->getTag() != 0xffffffff) mTLP_Tag = (cmd->getTag() & 0xff);
       else mTLP_Tag = xtor->getUniqueTag();   // xtor specific unique tag for this transaction
       
       //x UtIO::cout() << "MIKE, I'm Getting a new tag here!! (got seq #" << mTLP_Tag << " for a 0x" << cmd->getCmdByName() << " xaction)" << UtIO::endl;
    }
    transID = ((mTLP_ReqID & 0xFFFF) << 8) | (mTLP_Tag & 0x00FF);  // Save the Transaction ID

    UInt64 tempAddr = cmd->getAddr();
    mTLP_AddrHi = (tempAddr >> 32) & 0xFFFFFFFF;
    mTLP_AddrLo = tempAddr & 0xFFFFFFFF;

    mTLP_FDWBE = cmd->getFirstBE();
    mTLP_LDWBE = cmd->getLastBE();

//     UtIO::cout() << "MIKE MIKE: AddrLo=0x" << UtIO::hex << unsigned(mTLP_AddrLo)
// 	      << ", FDWBE=0x" << unsigned(mTLP_FDWBE)
// 	      << ", LDWBE=0x" << unsigned(mTLP_LDWBE)
// 	      << UtIO::endl;

    if((cmd->getCmdByName() == eCarbonXPcieCpl) || (cmd->getCmdByName() == eCarbonXPcieCplD) || 
       (cmd->getCmdByName() == eCarbonXPcieCplLk) || (cmd->getCmdByName() == eCarbonXPcieCplDLk)) {
	mTLP_CompID = cmd->getCompId();
    } else {
	mTLP_CompID = 0;
    }

    mTLP_CompSt = cmd->getCplStatus();

    mTLP_BCM = 0;  // MBZ for PCIExpress Completers (only used for PCIX Completers)
    
    /* 4 * len is not correct for completions of MEM Reads
    ** for the moment, I'm gonna let this get overriden by the callback routine,
    ** but the RIGHT thing to do would be to know what it is based on the request
    */
    mTLP_BC = cmd->getByteCount();
    mTLP_LowAd = cmd->getLowAddr();

    mTLP_msgRout = cmd->getMsgRout();
    mTLP_msgCode = cmd->getMsgCode();

    // first:
    //   create TLP packet bytes
    // second:
    //   update into DLLP packets (add header & CRC)
    // third:
    //   change into Phys packets (encode & scrable)
    // return:
    //   single 10bit packet of data to go onto bus

    ///////////////////////////////////////////////////////////////
    //          1)    Create TLP Packets                         //
    ///////////////////////////////////////////////////////////////

    for(i=0; i<16; i++)
	tlp_bytes_hdr[i] = 0;
                             // BYTE_0[7:0] = {1'b0, mTLP_Fmt[1:0], mTLP_Type[4:0]}
    if(   (cmd->getCmdByName() == eCarbonXPcieMsg)
       || (cmd->getCmdByName() == eCarbonXPcieMsgD)
       ) {
	// Command Type + Messge Routing
	tlp_bytes_hdr[0] = ((cmd->getCmdByName() & 0x78) + (cmd->getMsgRout()));
    } else {
	// Command Type
	tlp_bytes_hdr[0] = (cmd->getCmdByName() & 0x7F);
    }
                             // BYTE_1[7:0] = {1'b0, mTLP_TC[2:0], 4'b0000}
    tlp_bytes_hdr[1] = ((mTLP_TC & 0x07) << 4);
                             // BYTE_2[7:0] = {mTLP_TD, mTLP_EP, mTLP_Attr[1:0], 2'b00, mTLP_Len[9:8]}
    tlp_bytes_hdr[2] = (((mTLP_TD & 0x01) << 7) + ((mTLP_EP & 0x01) << 6) + ((mTLP_Attr & 0x03) << 4) + ((mTLP_Len >> 8) & 0x03));
                             // BYTE_3[7:0] = {mTLP_Len[7:0]}
    tlp_bytes_hdr[3] = (mTLP_Len & 0xFF);
    
    if(   (cmd->getCmdByName() == eCarbonXPcieCpl)
       || (cmd->getCmdByName() == eCarbonXPcieCplD)
       || (cmd->getCmdByName() == eCarbonXPcieCplLk)
       || (cmd->getCmdByName() == eCarbonXPcieCplDLk)
       ) {            // Completions
	
      /*
      ** Mark all completions successful...
      */
	// Completer ID
	tlp_bytes_hdr[4] = (mTLP_CompID >> 8) & 0xFF;
	tlp_bytes_hdr[5] = mTLP_CompID & 0xFF;
	// Completion status, Byte Count Mask, Byte Count Remaining
	tlp_bytes_hdr[6] = ((mTLP_CompSt << 5) & 0xE0) | ((mTLP_BCM << 4) & 0x10) | ((mTLP_BC >> 8) & 0x0F);
	tlp_bytes_hdr[7] = mTLP_BC & 0xFF;
	// Requester ID 
	tlp_bytes_hdr[8] = (mTLP_ReqID >> 8) & 0xFF;
	tlp_bytes_hdr[9] = mTLP_ReqID & 0xFF;
	// TAG 
	tlp_bytes_hdr[10] = mTLP_Tag;
	tlp_bytes_hdr[11] = mTLP_LowAd & 0x7F;

	tlp_bytes_hdr_used = 12;
      
    } else {
	// Requester ID 
	tlp_bytes_hdr[4] = ((mTLP_ReqID >> 8) & 0xFF);
	tlp_bytes_hdr[5] = (mTLP_ReqID & 0xFF);
	// Tag
	tlp_bytes_hdr[6] = (mTLP_Tag & 0xFF);

	if(   (cmd->getCmdByName() == eCarbonXPcieMsg)
	   || (cmd->getCmdByName() == eCarbonXPcieMsgD)
	   ) {
	    // Message Code
	    tlp_bytes_hdr[7] = mTLP_msgCode;
	} else {
	    // Last & First Double Word Byte Enables
	    tlp_bytes_hdr[7] = (((mTLP_LDWBE & 0x0F) << 4) + (mTLP_FDWBE & 0x0F));
	}

	if(   (cmd->getCmdByName() == eCarbonXPcieMRd64)
	   || (cmd->getCmdByName() == eCarbonXPcieMRdLk64)
	   || (cmd->getCmdByName() == eCarbonXPcieMWr64)
	   ) {            // 64-bit address transaction
	                      // BYTE_8  = mAddressHi[31:24] = Address[63:56]
	    tlp_bytes_hdr[8]  = ((mTLP_AddrHi >> 24) & 0xFF);
	                      // BYTE_9  = mAddressHi[23:16] = Address[55:48]
	    tlp_bytes_hdr[9]  = ((mTLP_AddrHi >> 16) & 0xFF);
	                      // BYTE_10 = mAddressHi[15:08] = Address[47:40]
	    tlp_bytes_hdr[10] = ((mTLP_AddrHi >> 8) & 0xFF);
	                      // BYTE_11 = mAddressHi[07:02] = Address[39:32]
	    tlp_bytes_hdr[11] = (mTLP_AddrHi & 0xFF);
	                      // BYTE_12 = mAddressLo[31:24] = Address[31:24]
	    tlp_bytes_hdr[12] = ((mTLP_AddrLo >> 24) & 0xFF);
	                      // BYTE_13 = mAddressLo[23:16] = Address[23:16]
	    tlp_bytes_hdr[13] = ((mTLP_AddrLo >> 16) & 0xFF);
	                      // BYTE_14 = mAddressLo[15:08] = Address[16:08]
	    tlp_bytes_hdr[14] = ((mTLP_AddrLo >> 8) & 0xFF);
	                      // BYTE_15 = mAddressLo[07:02] = Address[07:02]
	    tlp_bytes_hdr[15] = (mTLP_AddrLo & 0xFC);

	    tlp_bytes_hdr_used = 16;
	} else if ( cmd->getCmdByName() == eCarbonXPcieMsg) {
	   tlp_bytes_hdr_used = 16;
	   tlp_bytes_hdr[8] = 0;
	   tlp_bytes_hdr[9] = 0;
	   tlp_bytes_hdr[10] = 0;
	   tlp_bytes_hdr[11] = 0;
	   tlp_bytes_hdr[12] = 0;
	   tlp_bytes_hdr[13] = 0;
	   tlp_bytes_hdr[14] = 0;
	   tlp_bytes_hdr[15] = 0;
	} else {          // 32-bit address transaction
	                      // BYTE_8  = mAddressLo[31:24] = Address[31:24]
	    tlp_bytes_hdr[8]  = ((mTLP_AddrLo >> 24) & 0xFF);
	                      // BYTE_9  = mAddressLo[23:16] = Address[23:16]
	    tlp_bytes_hdr[9]  = ((mTLP_AddrLo >> 16) & 0xFF);
	                      // BYTE_10 = mAddressLo[15:08] = Address[16:08]
	    tlp_bytes_hdr[10] = ((mTLP_AddrLo >> 8) & 0xFF);
	                      // BYTE_11 = mAddressLo[07:02] = Address[07:02]
	    tlp_bytes_hdr[11] = (mTLP_AddrLo & 0xFC);
	    
	    // For Config Requesters, upper nibble of byte 10 has to be 0
	    if( (cmd->getCmdByName() == eCarbonXPcieCfgWr0)
		|| (cmd->getCmdByName() == eCarbonXPcieCfgWr1)
		|| (cmd->getCmdByName() == eCarbonXPcieCfgRd0)
		|| (cmd->getCmdByName() == eCarbonXPcieCfgRd1) )
	       tlp_bytes_hdr[10] &= 0xf;

	    tlp_bytes_hdr_used = 12;
	}
    }

    if ((cmd->getCmdByName() == eCarbonXPcieIOWr)   ||
	(cmd->getCmdByName() == eCarbonXPcieCfgWr0) ||
	(cmd->getCmdByName() == eCarbonXPcieCfgWr1)
       ) {
       fcData = 1;
    } else if ((cmd->getCmdByName() == eCarbonXPcieMWr32)  ||
	       (cmd->getCmdByName() == eCarbonXPcieMWr64)  ||
	       (cmd->getCmdByName() == eCarbonXPcieMsgD)   ||
	       (cmd->getCmdByName() == eCarbonXPcieCplD)   ||
	       (cmd->getCmdByName() == eCarbonXPcieCplDLk)
	      ) {
       // Flow Control Data Credit Unit is 4 DWs (PCIE rev1.0a, sec 2.6.1)
       fcData = ((mDataLength + 3) / 4);
    } else {
       fcData = 0;
    }

    // Add space for data payload, but only if it's a write or completion with data!
    if ((cmd->getCmdByName() == eCarbonXPcieMWr32)  || (cmd->getCmdByName() == eCarbonXPcieMWr64)  ||
	(cmd->getCmdByName() == eCarbonXPcieIOWr)   || (cmd->getCmdByName() == eCarbonXPcieCfgWr0) ||
	(cmd->getCmdByName() == eCarbonXPcieCfgWr1) || (cmd->getCmdByName() == eCarbonXPcieMsgD)   ||
	(cmd->getCmdByName() == eCarbonXPcieCplD)   || (cmd->getCmdByName() == eCarbonXPcieCplDLk)
       )
      tlp_bytes_total = tlp_bytes_hdr_used + (mTLP_Len * 4);
    else
      tlp_bytes_total = tlp_bytes_hdr_used;


    ///////////////////////////////////////////////////////////////
    //          2)    Create DLLP Packets                        //
    ///////////////////////////////////////////////////////////////
    UInt32 dllp_bytes_total = tlp_bytes_total + 6;  // DLLP header = 2, DLLP footer (LCRC) = 4
    CarbonXArray<UInt32> dllp_bytes(dllp_bytes_total, 0);  //  dynamic storage of the DLLP bytes

    // Can't add SeqNum until the tranasction is ready to be put across the bus, otherwise we can't
    //   ensure that the sequence numbers go in order.
    //m DLLP_SeqNum = xtor->getNewDLLPSeqNum();  // get a new Sequence Number from transactor
    //m dllp_bytes[0] = ((DLLP_SeqNum >> 8) & 0x0F);
    //m dllp_bytes[1] = (DLLP_SeqNum & 0x0FF);
    dllp_bytes[0] = 0;
    dllp_bytes[1] = 0;

    for(i=0; i < tlp_bytes_hdr_used; i++) {
	dllp_bytes[(i+2)] = tlp_bytes_hdr[i];
    }

    // Add data payload, but only if it's a write or completion with data!
    if ((cmd->getCmdByName() == eCarbonXPcieMWr32)  || (cmd->getCmdByName() == eCarbonXPcieMWr64)  ||
	(cmd->getCmdByName() == eCarbonXPcieIOWr)   || (cmd->getCmdByName() == eCarbonXPcieCfgWr0) ||
	(cmd->getCmdByName() == eCarbonXPcieCfgWr1) || (cmd->getCmdByName() == eCarbonXPcieMsgD)   ||
	(cmd->getCmdByName() == eCarbonXPcieCplD)   || (cmd->getCmdByName() == eCarbonXPcieCplDLk)
       ) {
       for (UInt32 j = 0; j < mDataLength; j++) {
	  dllp_bytes[tlp_bytes_hdr_used + 2 + (j*4) + 0] = cmd->getDataByte(j*4 + 0);
	  dllp_bytes[tlp_bytes_hdr_used + 2 + (j*4) + 1] = cmd->getDataByte(j*4 + 1);
	  dllp_bytes[tlp_bytes_hdr_used + 2 + (j*4) + 2] = cmd->getDataByte(j*4 + 2);
	  dllp_bytes[tlp_bytes_hdr_used + 2 + (j*4) + 3] = cmd->getDataByte(j*4 + 3);
       }
    }

    // Can't add CRC until the SeqNum is established.  Calculate & add it when SeqNum is written.
    dllp_bytes[dllp_bytes_total-4] = 0;
    dllp_bytes[dllp_bytes_total-3] = 0;
    dllp_bytes[dllp_bytes_total-2] = 0;
    dllp_bytes[dllp_bytes_total-1] = 0;

    // for(i=0; i<dllp_bytes_total; i++) {
    //     UtIO::cout() << i << ") " << dllp_bytes[i] << UtIO::endl;
    // }

    ///////////////////////////////////////////////////////////////
    //          3)    Create Phys Packets                        //
    ///////////////////////////////////////////////////////////////
    linkWidth = xtor->getCfg().mLinkWidth;
    //useScramble = xtor->getCfg().mScrambleEnable;
    allocPhysBytes(dllp_bytes_total + 2);

    phys_bytes_dat[0] = PCIE_STP;
    phys_bytes_sym[0] = true;
    for(i=0; i < dllp_bytes_total; i++) {
	phys_bytes_dat[i+1] = dllp_bytes[i];
	phys_bytes_sym[i+1] = false;
    }
    phys_bytes_dat[dllp_bytes_total+1] = PCIE_END;
    phys_bytes_sym[dllp_bytes_total+1] = true;

    return;
}

void PCIETransaction::initializeTransaction(CarbonXPcieTransactor *xtor, const CarbonXPcieDLLP &cmd) {
   
   // do not replay DLLP transactions
   canNotReplay = true;

   // DLLP's are always 8 bytes
   allocPhysBytes(8);

   switch(cmd.getLinkType()) {
   case CarbonXPcieDLLP::eAck        : strcpy(printString, "DLLP Ack"); break;
   case CarbonXPcieDLLP::eNak        : strcpy(printString, "DLLP Nak"); break;
   case CarbonXPcieDLLP::eInitFC1P   : strcpy(printString, "InitFC1-P"); break;
   case CarbonXPcieDLLP::eInitFC1N   : strcpy(printString, "InitFC1-NP"); break;
   case CarbonXPcieDLLP::eInitFC1Cpl : strcpy(printString, "InitFC1-Cpl"); break;
   case CarbonXPcieDLLP::eInitFC2P   : strcpy(printString, "InitFC2-P"); break;
   case CarbonXPcieDLLP::eInitFC2N   : strcpy(printString, "InitFC2-NP"); break;
   case CarbonXPcieDLLP::eInitFC2Cpl : strcpy(printString, "InitFC2-Cpl"); break;
   case CarbonXPcieDLLP::eUpdateFCP   : strcpy(printString, "UpdateFC-P"); break;
   case CarbonXPcieDLLP::eUpdateFCN   : strcpy(printString, "UpdateFC-NP"); break;
   case CarbonXPcieDLLP::eUpdateFCCpl : strcpy(printString, "UpdateFC-Cpl"); break;
   case CarbonXPcieDLLP::ePMEnterL1   : strcpy(printString, "PM Enter L1"); break;
   case CarbonXPcieDLLP::ePMEnterL23  : strcpy(printString, "PM Enter L2/L3"); break;
   case CarbonXPcieDLLP::ePMRequestL1 : strcpy(printString, "PM Requeset L1"); break;
   case CarbonXPcieDLLP::ePMRequestAck : strcpy(printString, "PM Request ACK"); break;
   default:               strcpy(printString, "Unknown");
   }
   
   // Is this Ack or Nak?
   mIsAck = (cmd.getLinkType() == CarbonXPcieDLLP::eAck);
   mIsNak = (cmd.getLinkType() == CarbonXPcieDLLP::eNak);

   if(cmd.getLinkType() == CarbonXPcieDLLP::eAck) {
      if(xtor->getCfg().getVerboseRxMed())
         printf("%s  Info: Adding Special Transaction: DLLP_Ack %d\n",xtor->getCfg().mPrintName.c_str(), cmd.getAckNakSeq());
   }

   // Add Start of Packet Framing
   phys_bytes_dat[0] = PCIE_SDP;
   phys_bytes_sym[0] = true;

   switch(cmd.getLinkType()) {
   case CarbonXPcieDLLP::eInitFC1P :
   case CarbonXPcieDLLP::eInitFC1N :
   case CarbonXPcieDLLP::eInitFC1Cpl :
   case CarbonXPcieDLLP::eInitFC2P :
   case CarbonXPcieDLLP::eInitFC2N :
   case CarbonXPcieDLLP::eInitFC2Cpl :
   case CarbonXPcieDLLP::eUpdateFCP    :
   case CarbonXPcieDLLP::eUpdateFCN    :
   case CarbonXPcieDLLP::eUpdateFCCpl  :
      phys_bytes_dat[1] = cmd.getLinkType() | cmd.getVC();
      phys_bytes_dat[2] = (cmd.getHdrFC() >> 2) & 0x3f;
      phys_bytes_dat[3] = ((cmd.getHdrFC() << 6) & 0xc0) | ((cmd.getDataFC() >> 8) & 0xf);
      phys_bytes_dat[4] = cmd.getDataFC() & 0xff;
      break;
   case CarbonXPcieDLLP::eAck :
   case CarbonXPcieDLLP::eNak :
      phys_bytes_dat[1] = cmd.getLinkType();
      phys_bytes_dat[3] = (cmd.getAckNakSeq() >> 8) & 0x0F;
      phys_bytes_dat[4] = cmd.getAckNakSeq() & 0xff;
      break;

   case CarbonXPcieDLLP::ePMEnterL1 :
   case CarbonXPcieDLLP::ePMEnterL23 :
   case CarbonXPcieDLLP::ePMRequestL1 :
   case CarbonXPcieDLLP::ePMRequestAck :
      break;
   case CarbonXPcieDLLP::eIllegal :
      break;
   }

   // Calculate and Insert CRC
   UInt16 crc16 = xtor->calc16bCRC(&phys_bytes_dat[1], 4);
   phys_bytes_dat[5] = (crc16 >> 8) & 0xff;
   phys_bytes_dat[6] = crc16 & 0xff;
   
   // Insert End Of Packet Framing
   phys_bytes_dat[7] = PCIE_END;
   phys_bytes_sym[7] = true;

//    printf("%s           Transaction Bytes: ", mXtor->mPrintName.c_str());
//    for (UInt32 i=0; i < mBytesTotal; ++i) {
//       if (phys_bytes_sym[i])
// 	 printf("  %s", mXtor->getSymStringFromByte(phys_bytes_dat[i]));
//       else
// 	 printf("  %02X", phys_bytes_dat[i]);
//    }
//    printf("\n");

}

void PCIETransaction::replayTransaction(void) {
   if (canNotReplay) {
                  // don't replay DLLPs
      return;
   } else if ((isStarted()) && (not isDone())) {
                  // wait until transaction-in-progress is done before replaying
      doReplay = true;
   } else {
      mBytesSent = 0;
      mDone = false;
      if (not mIsSpecial)
	 expectAck = true;
      numRetries++;
      doReplay = false;
   }
   return;
}


CarbonXPcieCmdType PCIETransaction::getTlpTypeFromStr(char *str)
{
    CarbonXPcieCmdType tlp_type;

    if (!strcmp(str, "MEMREAD32"))
	tlp_type = eCarbonXPcieMRd32;
    else if (!strcmp(str, "MEMREAD64"))
	tlp_type = eCarbonXPcieMRd64;
    else if (!strcmp(str, "MEMREADLOCK32"))
	tlp_type = eCarbonXPcieMRdLk32;
    else if (!strcmp(str, "MEMREADLOCK64"))
	tlp_type = eCarbonXPcieMRdLk64;
    else if (!strcmp(str, "MEMWRITE32"))
	tlp_type = eCarbonXPcieMWr32;
    else if (!strcmp(str, "MEMWRITE64"))
	tlp_type = eCarbonXPcieMWr64;
    else if (!strcmp(str, "IOREAD"))
	tlp_type = eCarbonXPcieIORd;
    else if (!strcmp(str, "IOWRITE"))
	tlp_type = eCarbonXPcieIOWr;
    else if (!strcmp(str, "CFGREAD0"))
	tlp_type = eCarbonXPcieCfgRd0;
    else if (!strcmp(str, "CFGWRITE0"))
	tlp_type = eCarbonXPcieCfgWr0;
    else if (!strcmp(str, "CFGREAD1"))
	tlp_type = eCarbonXPcieCfgRd1;
    else if (!strcmp(str, "CFGWRITE1"))
	tlp_type = eCarbonXPcieCfgWr1;
    else if (!strcmp(str, "MSG"))
	tlp_type = eCarbonXPcieMsg;
    else if (!strcmp(str, "MSGD"))
	tlp_type = eCarbonXPcieMsgD;
    else if (!strcmp(str, "COMPL"))
	tlp_type = eCarbonXPcieCpl;
    else if (!strcmp(str, "COMPLD"))
	tlp_type = eCarbonXPcieCplD;
    else if (!strcmp(str, "COMPLLOCK"))
	tlp_type = eCarbonXPcieCplLk;
    else if (!strcmp(str, "COMPLDLOCK"))
	tlp_type = eCarbonXPcieCplDLk;
    else {
	printf("ERROR: Received a bad command for a transaction:\n  \"%s\"\n", str);
	tlp_type = eCarbonXPcieMRd32;
    }

    return tlp_type;
}

void PCIETransaction::setSeqNumAndCRC(CarbonXPcieTransactor *xtor) {
   UInt32 lcrc = 0;
   SInt32 pos = 0;

   // Write correct Sequence Number
   DLLP_SeqNum = xtor->getNewDLLPSeqNum();  // get a new Sequence Number from transactor
   phys_bytes_dat[1] = ((DLLP_SeqNum >> 8) & 0x0F);
   phys_bytes_dat[2] = (DLLP_SeqNum & 0xFF);

   // find position to write the CRC (last 4 bytes are LCRC, then 'END', then 0+ 'PAD', so insert before 'END')
   for (pos = mBytesTotal-1; pos > 0; --pos) {
      if ((phys_bytes_dat[pos] == PCIE_END) && (phys_bytes_sym[pos] == true)) {
	 pos = pos - 4;  // move back 4 positions to the start of the 
	 break;
      }
   }

   if (pos > 0) {  // if pos <= 0 then we didn't find 'END' correctly and shouldn't try to write a CRC.
      // Calculate the proper LCRC now that we have all of the packets written
      // Don't use 'STP' or LCRC & 'END' in calc
      lcrc = xtor->calc32bCRC(&(phys_bytes_dat[1]),pos-1);  // generate 32-bit LCRC
      phys_bytes_dat[pos+0] = ((lcrc >> 24) & 0xFF);   // most significant bits
      phys_bytes_dat[pos+1] = ((lcrc >> 16) & 0xFF);
      phys_bytes_dat[pos+2] = ((lcrc >>  8) & 0xFF);
      phys_bytes_dat[pos+3] = (lcrc & 0xFF);           // least significant bits
   }

   mNeedSeqNumAndLCRC = false;
   return;
}

void PCIETransaction::allocPhysBytes(UInt32 size) {
   mBytesTotal = size;
   phys_bytes_dat.resize(size);
   phys_bytes_sym.resize(size);
}


void PCIETransaction::copy(const PCIETransaction &other) {
   mXtor              = other.mXtor;
   mPcieCmd           = other.mPcieCmd;
   mIsValid           = other.mIsValid;
   mIsSpecial         = other.mIsSpecial;
   mIsCpl             = other.mIsCpl;
   mIsAck             = other.mIsAck;
   mIsNak             = other.mIsNak;
   mIsTLP             = other.mIsTLP;
   mMustStartLane0    = other.mMustStartLane0;
   mDone              = other.mDone;
   mNeedSeqNumAndLCRC = other.mNeedSeqNumAndLCRC;    // does the transaction still need a Sequence Number and LCRC to be established
   mBytesSent         = other.mBytesSent;
   mBytesTotal        = other.mBytesTotal;
   runOutputState     = other.runOutputState;
   DLLP_SeqNum        = other.DLLP_SeqNum;
   expectAck          = other.expectAck;
   expectCpl          = other.expectCpl;
   transID            = other.transID;
   canNotReplay       = other.canNotReplay;
   doReplay           = other.doReplay;
   numRetries         = other.numRetries;
   fcType             = other.fcType;
   fcHeader           = other.fcHeader;
   fcData             = other.fcData;
   fcCreditsConsumed  = other.fcCreditsConsumed;
   mDataLength        = other.mDataLength;
   mCompletionFunc    = other.mCompletionFunc;
   mCompletionFuncArg = other.mCompletionFuncArg;  

   strcpy(printString, other.printString);

   phys_bytes_dat     = other.phys_bytes_dat;
   phys_bytes_sym     = other.phys_bytes_sym;
}

void PCIETransaction::printToOStream(UtOStream* ostr)
{
   for (UInt32 i=0; i < mBytesTotal; ++i) {
      if (phys_bytes_sym[i]) {
         carbonInterfaceWriteStringToOStream(ostr, " ");
	 carbonInterfaceWriteStringToOStream(ostr, mXtor->getSymStringFromByte(phys_bytes_dat[i]));
      } else {
         carbonInterfaceWriteStringToOStream(ostr, " 0x");
         carbonInterfaceSetOStreamRadixHex(ostr);
         carbonInterfaceWriteUInt32ToOStream(ostr, phys_bytes_dat[i]);
      }
   }
   
   carbonInterfaceOStreamEndl(ostr);
}

/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
