// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CarbonXPcieIfBase_H_
#define __CarbonXPcieIfBase_H_

#include "util/c_memmanager.h"
#include "carbonXPcie.h"
#include "CarbonXPcieCommonTypes.h"
#include "shell/carbon_shelltypes.h"
#include <cstring>

class CarbonXPcieRecvBuf;

// transaction special characters
// Define the Data Bytes associated with the Special Character Symbol Codes
#define PCIE_COM	0xBC	// K28.5 // Used for Lane and Link Initialization
#define PCIE_STP	0xFB	// K27.7 // Marks the start of a Transaction Layer Packet
#define PCIE_SDP	0x5C	// K28.2 // Marks the start of a Data Link Layer Packet
#define PCIE_END	0xFD	// K29.7 // Marks the end of a TLP or DLLP
#define PCIE_EDB	0xFE	// K30.7 // Marks the end of a nullified TLP
#define PCIE_PAD	0xF7	// K23.7 // Used in Framing and Link Width and Lane ordering negotiations
#define PCIE_SKP	0x1C	// K28.0 // Used for compensating for different bit rates for two communicating ports
#define PCIE_FTS	0x3C	// K28.1 // Used within an ordered set to exit from L0s to L0 (standby to idle)
#define PCIE_IDL	0x7C	// K28.3 // Symbol used in the Electrical Idle ordered set
#define PCIE_RSVD1	0x9C	// K28.4 // RESERVED
#define PCIE_RSVD2	0xDC	// K28.6 // RESERVED
#define PCIE_RSVD3	0xFC	// K28.7 // RESERVED

class CarbonXPcieNet;

// Cycle Transaction Class
class CarbonXPcieCycleData {
public : CARBONMEM_OVERRIDES;

  CarbonXPcieCycleData() {
    memset(mElecIdle, 0, sizeof(bool)*32);
  }
  CarbonXPcieSym mData[32];
  bool           mElecIdle[32]; 
};

class CarbonXPcieIfBase {
public: CARBONMEM_OVERRIDES;
  
  CarbonXPcieIfBase() {}
  virtual ~CarbonXPcieIfBase() {}
  
  virtual void reset()=0;

  //! Return pointer to the net object for the specified signal
  virtual CarbonXPcieNet * getNet(CarbonXPcieSignalType sigType, UInt32 index)=0;
  
  // Step function for anything in the interface that has to executed every symbol cycle
  virtual void step() {
  }

  // Output Data on the interface
  virtual void transmitData(const UInt32 *data, const bool *sym, bool scramble = true)=0;
  virtual void transmitLaneData(UInt32 data, bool sym, UInt32 index, bool scramble = true)=0;
  virtual void transmitIdleData(void)=0;
  virtual void transmitEIdle(bool on_off)=0;

  // Read data from the pin interface
  virtual bool isEIdle()=0;
  virtual bool inReset() { return false; }

  static const char* getSymStringFromByte(UInt32 byte) {
    switch(byte) {
    case PCIE_COM:   return "COM";
    case PCIE_STP:   return "STP";
    case PCIE_SDP:   return "SDP";
    case PCIE_END:   return "END";
    case PCIE_EDB:   return "EDB";
    case PCIE_PAD:   return "PAD";
    case PCIE_SKP:   return "SKP";
    case PCIE_FTS:   return "FTS";
    case PCIE_IDL:   return "IDL";
    case PCIE_RSVD1:
    case PCIE_RSVD2:
    case PCIE_RSVD3:
      return "RSVD";
    default:
      return "INVALID";
    }
  }
};

#endif

