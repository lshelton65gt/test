/*****************************************************************************

 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CRC_H__
#define __CRC_H__

template <bool> struct CTAssert;
template <> struct CTAssert<true> {};

template <int width, typename baseType = unsigned int> class crcGen
{
public:
  void doByte(unsigned int b) {	
    for(int i = 0; i < 8; ++i) {
      doBit((b >> i) & 0x1);
    }
  }

  void clr() { mIntRes = mWidthMask; }
  baseType  getResult() { return flip(~mIntRes & mWidthMask)  ; }
  baseType flip(baseType x) {
    baseType val = 0;
    
    for(int b = 0; b < mBytes; ++b) {
      for (int i = 0; i < 8; ++i) {
	val |= ((x >> i) & (0x1 << (b * 8) )) << (7 - i);
      }
    }

    return val;
  }

  crcGen (baseType poly) : mBytes (width /8),
			   mPoly (poly),
			   mWidthMask( ~((~baseType(0)) <<  width) ) {}

private:
  CTAssert<width % 8 == 0> _widthMustBeFactorOf8;
  CTAssert<width % 8 <= sizeof(baseType)> _BaseTypeTooSmall;
    
  crcGen();

  const int mBytes;
  const unsigned int mPoly;
  const unsigned int mWidthMask;
  unsigned mIntRes;

  void doBit(unsigned int bit)  
  {
    baseType xorBit = bit ^ ((mIntRes >> (width - 1)) & 0x1);
    baseType shftd = (mIntRes << 1) & mWidthMask;
    baseType masked = shftd & mPoly;
    baseType clrd = shftd & (~mPoly);

    if (xorBit) {
      mIntRes = (masked ^ mPoly) | clrd;
    } else {
      mIntRes = shftd;
    }
  }    
};

  
#endif // __CRC_GEN_FUNCTION__
