/***************************************************************************************
  Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <unistd.h>

#include "shell/carbon_interface_xtor.h"
#include "CarbonXPcieTransactor.h"
#include "CarbonXPcieTLP.h"
#include "PCIETransaction.h"
#include "PCIECallback.h"
#include "crc.h"

CarbonXPcieTransactor::CarbonXPcieTransactor(CarbonXPcieIntfType intf, UInt32 linkwidth, const char* inPrintName, FILE* stream) : 
   mCfg(linkwidth, inPrintName, stream),
   mDLLPSeqNum(0),
   mNumTransInFlight(0), 
   next_tag(0), 
   mDoneForNow(false), mInReset(false),
   mResetTimer(0), mDetectRxTimer(0), mIncomingDLLPBytesNext(0), cycle_count_skp(0),
   curPowerDnState(9999999),
   mChangeRate(false), rxDLLSM(DLLSM_Inactive),
   txDLLSM(DLLSM_Inactive), mFullyConfigured(false), mHasBeenWarnedAboutConfig(false),
   mStartTraining(0), mStartTrainingPrefaceIdleTime(5), mImLeader(false),
   mDoneCBFn(0), mDoneCBInfo(0), mTransCBFn(0), mTransCBInfo(0),
   mStartTransCBFn(0), mEndTransCBFn(0),
   mFirstCall(true), mReplayTransactions(false), 
   mPhyLayer(intf, mCfg),
   mForcedNaks(false), mLastTxAckSeqNum(0), mLastRxAckSeqNum(0), mIntfType(intf),
   mFcTxPostedHdrCreditLimit(0), mFcTxPostedDataCreditLimit(0), mFcTxNonPostedHdrCreditLimit(0),
   mFcTxNonPostedDataCreditLimit(0), mFcTxCompletionHdrCreditLimit(0), mFcTxCompletionDataCreditLimit(0),
   mFcTxPostedHdrCreditsConsumed(0), mFcTxPostedDataCreditsConsumed(0), mFcTxNonPostedHdrCreditsConsumed(0),
   mFcTxNonPostedDataCreditsConsumed(0), mFcTxCompletionHdrCreditsConsumed(0), mFcTxCompletionDataCreditsConsumed(0),
   mFcRxPostedHdrCreditsAllocated(0), mFcRxPostedDataCreditsAllocated(0), mFcRxNonPostedHdrCreditsAllocated(0),
   mFcRxNonPostedDataCreditsAllocated(0), mFcRxCompletionHdrCreditsAllocated(0), mFcRxCompletionDataCreditsAllocated(0),
   mFcRxPostedHdrCreditsProcessed(0), mFcRxPostedDataCreditsProcessed(0), mFcRxNonPostedHdrCreditsProcessed(0),
   mFcRxNonPostedDataCreditsProcessed(0), mFcRxCompletionHdrCreditsProcessed(0), mFcRxCompletionDataCreditsProcessed(0),
   mIdlesBeforeDone(1)
{
   
   /* scramble feature name and checkout license */
   CARBON_LICENSE_FEATURE(featureName, "crbn_vsp_exec_xtor_pcie", 23);
   mCarbonLicense = UtLicenseWrapperCheckout(featureName, "PCI Express Transactor.");

   //   valid link widths are x1, x2, x4, x8, x12, x16, x32
   if((linkwidth != 1) && (linkwidth != 2) && (linkwidth != 4) && (linkwidth != 8)
      && (linkwidth != 12) && (linkwidth != 16) && (linkwidth != 32))
      carbonInterfaceXtorPCIEInvLinkWidth(mCfg.mMsgContext, mCfg.mPrintName.c_str(), linkwidth);
}


CarbonXPcieTransactor::~CarbonXPcieTransactor(void)
{
   PCIETransactionList::const_iterator tx_iter;
   PCIECmdList::const_iterator cmd_iter;

   /*
   ** Clean up!
   */
   for (tx_iter = mTransactionListNew.begin(); tx_iter != mTransactionListNew.end(); ++tx_iter) {
      delete (*tx_iter);  // must explicitly delete alloced pointers
   }
   mTransactionListNew.clear();                   // clear space in list

   for (tx_iter = mTransactionListNeedAck.begin(); tx_iter != mTransactionListNeedAck.end(); ++tx_iter) {
      delete (*tx_iter);  // must explicitly delete alloced pointers
   }
   mTransactionListNeedAck.clear();               // clear space in list

   for (tx_iter = mTransactionListNeedCpl.begin(); tx_iter != mTransactionListNeedCpl.end(); ++tx_iter) {
      delete (*tx_iter);  // must explicitly delete alloced pointers
   }
   mTransactionListNeedCpl.clear();               // clear space in list

   for (cmd_iter = mReceivedTransList.begin(); cmd_iter != mReceivedTransList.end(); ++cmd_iter) {
      delete (*cmd_iter);               // must explicitly delete alloced pointers
   }
   mReceivedTransList.clear();                    // clear space in list

   for (cmd_iter = mReturnDataList.begin(); cmd_iter != mReturnDataList.end(); ++cmd_iter) {
      delete (*cmd_iter);               // must explicitly delete alloced pointers
   }
   mReturnDataList.clear();                       // clear space in list

   // release the license
   UtLicenseWrapperRelease(&mCarbonLicense);
}

void CarbonXPcieTransactor::reset() {
   
   mDLLPSeqNum                        = 0;
   mNumTransInFlight                  = 0;
   next_tag                           = 0; 
   mDoneForNow                        = false;
   mIncomingDLLPBytesNext             = 0;
   cycle_count_skp                    = 0;
   rxDLLSM                            = DLLSM_Inactive;
   txDLLSM                            = DLLSM_Inactive;
   mStartTraining                     = 0;
   mReplayTransactions                = false;
   mLastTxAckSeqNum                   = 0; 
   mLastRxAckSeqNum                   = 0;
   mFcTxPostedHdrCreditLimit          = 0;
   mFcTxPostedDataCreditLimit         = 0;
   mFcTxNonPostedHdrCreditLimit       = 0;
   mFcTxNonPostedDataCreditLimit      = 0; 
   mFcTxCompletionHdrCreditLimit      = 0;
   mFcTxCompletionDataCreditLimit     = 0;
   mFcTxPostedHdrCreditsConsumed      = 0;
   mFcTxPostedDataCreditsConsumed     = 0;
   mFcTxNonPostedHdrCreditsConsumed   = 0;
   mFcTxNonPostedDataCreditsConsumed  = 0;
   mFcTxCompletionHdrCreditsConsumed  = 0; 
   mFcTxCompletionDataCreditsConsumed = 0;
   
   deleteTransactionQueue();
   mPhyLayer.reset();
}

void CarbonXPcieTransactor::idle(UInt64 cycles) {
   addTransactionSpecial(cycles);   // add IDLE transaction for 'cycles' length
}


void CarbonXPcieTransactor::step(UInt64 tick) {

   // tell the license server we are still using the license
   UtLicenseWrapperHeartbeat(mCarbonLicense);
   
   //mCurrTick = tick;  // keep track of the time
   mCfg.updateCurrTime(tick);
   
   // if we're configured then continue
   if (!mFullyConfigured) {
      // if not, then check if we are configured
      if (!mHasBeenWarnedAboutConfig && !isConfigured()) {
	 // if we're still not configured, then print a message
         carbonInterfaceXtorPCIENotConfiged(mCfg.mMsgContext, mCfg.mPrintName.c_str());
	 mHasBeenWarnedAboutConfig = true;
      }

      // since we're not configured, don't do anything but issue our Done callback and return

      // if a DoneForNow callback has been set then call it
      if(mDoneCBFn)
	 mDoneCBFn(mDoneCBInfo);
      return;
   }

   // Execute Physical Layer
   mPhyLayer.step();
      
   CarbonXList<CarbonXPcieSym> pkt;
   if( mPhyLayer.getPhysPacket(&pkt) ) {
      if(pkt.front() == SYM_SDP)
	 checkCurrentTransactionDLLTraining(pkt);
      else if(pkt.front() == SYM_STP)
	 checkCurrentTransactionTLPs(pkt);
   }

   // Execute Data Link Layer
   if(!mPhyLayer.isPhysicalLinkUp()) {
      rxDLLSM = DLLSM_Inactive;
      txDLLSM = DLLSM_Inactive;
   } else if((rxDLLSM < DLLSM_Active) || (txDLLSM < DLLSM_Active)) {  // Data Link Layer Training to do here
      DLLSM_process();
   }
   
   if(mPhyLayer.isPhysicalLinkUp()) {
      // Insert SKP Orderd Set at regular intervals for Clock Tolerance Compensation (PCIE Spec 1.0a sec 4.2.7.1)
      ++cycle_count_skp;
      if(cycle_count_skp >= 1200) {   // Per Spec: interval must be btwn 1180 & 1538 symbol times
	 sendFCCUpdate();
	 cycle_count_skp = 0;   // reset the active cycle counter for SKP sets
      }
      
      // Get pointer to transaction to be worked on.  The transaction may be new or in progress
      // at this point.  If there are no transactions to go across the bus then 0 is returned.
      GetTransactionReturnStatus transReturnStat = getNewTransaction();
      //UtIO::cout() << mCfg.mPrintName << " Info: GetTransactionReturnStatus = " << transReturnStat << " at " << mCfg.mCurrTimeString << UtIO::endl;
 
      if (transReturnStat == eError)
	 return;
      
      if (mPhyLayer.isPhysicalLinkUp() && (transReturnStat != eNoTransAvailable)) {
	 mDoneForNow = false;
	 
	 if (transReturnStat == eNoCreditsAvailable) {
	    ;
	 } else {  // eSuccess
	    
	    // If received a Nak then move transactions back into 'new' queue
	    if (mReplayTransactions) {
	       updateTransactionsForNak();
	    }
	 }
      } else {  // either output in ElecIdle or there are NoTransAvailable
	 
	 if ((transReturnStat == eNoTransAvailable) && (mReplayTransactions)) {
	    updateTransactionsForNak();
	 }

	 if( nothingToDo() ) {   // don't stop while we're in training.
	    mDoneForNow = true;  // set flag so some controlling watcher will know
// 	    printf("%s  Info: LinkUp = %d, rxDLLSM = %s, txDLLSM = %s, Pending Packet = %d, PacketInProgress = %d Idle 10 cycles: %d\n",
// 		   mCfg.mPrintName.c_str(), mPhyLayer.isPhysicalLinkUp(), getDLLSMStateString(rxDLLSM), getDLLSMStateString(txDLLSM), 
// 		   mPhyLayer.hasPendingPacket(), mPhyLayer.txPacketInProgress(), mPhyLayer.txIsIdle(10));
	    // if a DoneForNow callback has been set then call it
	    if(mDoneCBFn)
	       mDoneCBFn(mDoneCBInfo);

	    // Since we have nothing to do at the moment, we could let the transmitter got to a Low Power State
	    // The Phy will come back automaticly when we have more packets to send
	    //if(mPhyLayer.getTxLinkState() == L0) mPhyLayer.powerSave();
	 }
      }
   }

}

void CarbonXPcieTransactor::DLLSM_process()
{
//    printf("%s  Info: In DLLSM: txDLLSM = %s, rxDLLSM = %s, mStartTraining = %d\n", mCfg.mPrintName.c_str(), getDLLSMStateString(txDLLSM), getDLLSMStateString(rxDLLSM), mStartTraining);

    if((txDLLSM == DLLSM_Inactive) && ((rxDLLSM == DLLSM_Init1) || mCfg.mUpstreamDevice )) {
       //UtIO::cout() << "Initializing Flow Control. Posted Hdr FCC = " << mFcRxPostedHdrCreditsAllocated << " Posted Data FCC = " << mFcRxPostedDataCreditsAllocated << UtIO::endl;
       CarbonXPcieDLLP fc1_p, fc1_n, fc1_cpl;
       fc1_p.setLinkType(CarbonXPcieDLLP::eInitFC1P);
       fc1_p.setHdrFC(mFcRxPostedHdrCreditsAllocated);
       fc1_p.setDataFC(mFcRxPostedDataCreditsAllocated);

       fc1_n.setLinkType(CarbonXPcieDLLP::eInitFC1N);
       fc1_n.setHdrFC(mFcRxNonPostedHdrCreditsAllocated);
       fc1_n.setDataFC(mFcRxNonPostedDataCreditsAllocated);

       fc1_cpl.setLinkType(CarbonXPcieDLLP::eInitFC1Cpl);
       fc1_cpl.setHdrFC(mFcRxCompletionHdrCreditsAllocated);
       fc1_cpl.setDataFC(mFcRxCompletionDataCreditsAllocated);
       for(UInt32 i = 0; i < 15; i++) {
	  addTransaction(fc1_p);
	  addTransaction(fc1_n);
	  addTransaction(fc1_cpl);
       }
       //       addTransactionSpecial("InitFC1");
       //addTransactionSpecial("IDLE 3");
       
	txDLLSM = DLLSM_Init1;
	mStartTraining=5;
    } else if((txDLLSM == DLLSM_Init1) && ((rxDLLSM == DLLSM_Init1) || (rxDLLSM == DLLSM_Init2))) {
       CarbonXPcieDLLP fc2_p, fc2_n, fc2_cpl;
       fc2_p.setLinkType(CarbonXPcieDLLP::eInitFC2P);
       fc2_p.setHdrFC(mFcRxPostedHdrCreditsAllocated);
       fc2_p.setDataFC(mFcRxPostedDataCreditsAllocated);
       fc2_n.setLinkType(CarbonXPcieDLLP::eInitFC2N);
       fc2_n.setHdrFC(mFcRxNonPostedHdrCreditsAllocated);
       fc2_n.setDataFC(mFcRxNonPostedDataCreditsAllocated);
       fc2_cpl.setLinkType(CarbonXPcieDLLP::eInitFC2Cpl);
       fc2_cpl.setHdrFC(mFcRxCompletionHdrCreditsAllocated);
       fc2_cpl.setDataFC(mFcRxCompletionDataCreditsAllocated);
       for(UInt32 i = 0; i < 15; i++) {
	  addTransaction(fc2_p);
	  addTransaction(fc2_n);
	  addTransaction(fc2_cpl);
       }
       //addTransactionSpecial("InitFC2");
	addTransactionSpecial(3);
	txDLLSM = DLLSM_Init2;
    } else if((txDLLSM == DLLSM_Init2) && ((rxDLLSM == DLLSM_Init2) || (rxDLLSM == DLLSM_Active))) {  // Done training
	addTransactionSpecial(1);
	txDLLSM = DLLSM_Active;
	mStartTraining=0;  // reset our tracking flag
	if(mCfg.mVerboseReceiverLow && rxDLLSM == DLLSM_Active)
	   printf("%s  Info: Link training complete (at %s).\n", mCfg.mPrintName.c_str(), mCfg.mCurrTimeString.c_str());
    } else if((rxDLLSM == DLLSM_Init2) && ((txDLLSM == DLLSM_Init2) || (txDLLSM == DLLSM_Active)) && (mPhyLayer.getConsecIdles() >= 8)) {  // Done training
       rxDLLSM = DLLSM_Active;
       addTransactionSpecial(1);
	mStartTraining=0;  // reset our tracking flag
	if(mCfg.mVerboseReceiverLow && txDLLSM == DLLSM_Active)
	   printf("%s  Info: Link training complete (at %s).\n", mCfg.mPrintName.c_str(), mCfg.mCurrTimeString.c_str());
       
    } else if((txDLLSM != DLLSM_Inactive) && (rxDLLSM == DLLSM_Inactive) && !mCfg.mUpstreamDevice) {
	txDLLSM = DLLSM_Inactive;
    }
}


SInt32 CarbonXPcieTransactor::addTransaction(const CarbonXPcieTLP *cmd) {
   PCIETransaction *tx;
   SInt32 retVal = -1;  // -1 = failure, other = transaction ID
   tx = new PCIETransaction(this, cmd);
   retVal = addTransaction(tx);
   
   // If Transaction is invalid, delete transaction
   if(retVal == -1) delete tx;
   else if (mCfg.mVerboseTransmitterMedium)
      printf("%s  Info: Adding transaction '%s' to the queue.  cmd->ByteCount = %d, cmd->DataLength = %d, Length = %d\n", 
	     mCfg.mPrintName.c_str(), tx->getPrintString(), 
	     cmd->getByteCount(),
	     cmd->getDataLength(),
	     tx->getDataLength());

   return retVal;
}

SInt32 CarbonXPcieTransactor::addTransaction(const CarbonXPcieDLLP &cmd) {
   PCIETransaction *tx;
   SInt32 retVal = -1;  // -1 = failure, other = transaction ID
   tx = new PCIETransaction(this, cmd);
   retVal = addTransaction(tx);
   
   // If Transaction is invalid, delete transaction
   if(retVal == -1) delete tx;
   else {
      if (mCfg.mVerboseTransmitterMedium)
	 printf("%s  Info: Adding transaction '%s' to the queue.\n", 
		mCfg.mPrintName.c_str(), tx->getPrintString());
      if (mCfg.mVerboseTransmitterHigh) {
         UtOStream* ostr = carbonInterfaceGetCout();
         tx->printToOStream(ostr);
      }
   }

   return retVal;
}
   
SInt32 CarbonXPcieTransactor::addTransaction(PCIETransaction *tx) {
   SInt32 retVal = -1;  // -1 = failure, other = transaction ID

   if (tx->isValid()) {
      retVal = tx->getTxID();  // find the transaction ID assigned to this transaction
      mTransactionListNew.push_back(tx);
   } 
   else {
      carbonInterfaceXtorPCIEInvTransaction(mCfg.mMsgContext, mCfg.mPrintName.c_str());
   }
   
   return retVal;
}

void CarbonXPcieTransactor::addTransactionSpecial(UInt32 idles) {
   PCIETransaction *tx;
   tx = new PCIETransaction(this, idles);
   if (tx->isValid()) {
      mTransactionListNew.push_back(tx);
   } else {
      carbonInterfaceXtorPCIEInvTransactionInternal(mCfg.mMsgContext, mCfg.mPrintName.c_str(), "");
      delete tx;
   }
   return;
}

void CarbonXPcieTransactor::addTransactionSpecialAckNak(bool isAck, UInt32 seqNum) {
   PCIETransaction* tx;
   PCIETransactionList::iterator iter;
   PCIETransactionList::iterator pointer;
   char cmdstr[20];

   // I need the Ack/Nak to come before TLPs (so we don't hit deadlock with buffers
   // all taken up).  Need Ack/Nak to come after other Acks/Naks (or all DLLPs) so
   // we don't end up getting them in the wrong order (as was happening when just
   // using addTransactionSpecialHead()

   // Search forward to first transaction not yet on the bus & save insert pointer.
   // While searching check if any Ack or Nak are seen, each one saves insert pointer.
   // Continue forward to first TLP & break.
   // If inserting a Nak, then insert after pointer.
   // If inserting an Ack, if pointer points to Ack then replace it (ack collapsing),
   //   else insert after it (pointer will either be last Nak or head of to-do list).

   if (isAck)
      sprintf(cmdstr, "DLLP_Ack %d", seqNum);
   else
      sprintf(cmdstr, "DLLP_Nak %d", seqNum);

   //tx = new PCIETransaction(this, cmdstr);
   
   CarbonXPcieDLLP dllp;
   if (isAck) dllp.setLinkType(CarbonXPcieDLLP::eAck);
   else       dllp.setLinkType(CarbonXPcieDLLP::eNak);
   dllp.setAckNakSeq(seqNum);

   tx = new PCIETransaction(this, dllp);

   if (not tx->isValid()) {
      carbonInterfaceXtorPCIEInvTransactionInternal(mCfg.mMsgContext, mCfg.mPrintName.c_str(), cmdstr);
      delete tx;
      return;
   }

   pointer = mTransactionListNew.end ();
   for (iter = mTransactionListNew.begin(); iter != mTransactionListNew.end(); ++iter) {
      if ((*iter)->isAck() || (*iter)->isNak()) {
	 pointer = iter;  // can put Ack/Nak after this
      }
   }

   if (pointer == mTransactionListNew.end ()) {
      mTransactionListNew.push_front(tx);
   } else {
      if (isAck && (*pointer)->isAck()) {     // have Ack & found Ack, collapse them
	 delete (PCIETransaction*)(*pointer);  // must explicitly delete allocated space
	 // OVERKILL:
	 // pointer = mTransactionListNew.erase(pointer);  // remove old Ack, given pointer to next element
	 // mTransactionListNew.insert(pointer, tx);       // insert *before* pointer
	 // EASIER & FASTER:
	 (*pointer) = tx;
      } else {
	 ++pointer;                                // point to next
	 mTransactionListNew.insert(pointer, tx);  // insert *before* pointer
      }
   }

   return;
}

CarbonXPcieTransactor::GetTransactionReturnStatus
 CarbonXPcieTransactor::getNewTransaction(void) {
   PCIETransactionList::iterator iter;
   GetTransactionReturnStatus retStatus = eNoTransAvailable;  // expect no transactions available, status only for first tx, not second
   bool failedCreditCheckPosted = false;
   bool failedCreditCheckNonPosted = false;
   bool failedCreditCheckCompletion = false;

   // Find first transaction to put on bus now, if that is almost done then
   // find a second transaction to put back-to-back
   typedef enum {
      eFindFirstTx,       // find first tx
      eFindSecondAsTLP,   // find second tx, must be TLP
      eFindSecondAsDLLP,  // find second tx, must be DLLP
      eFindSecondAsEither // find second tx, can be either
   } searchState;
   searchState state = eFindFirstTx;

   
   if (mPhyLayer.hasPendingPacket()) {  // If already have a transaction to work on
      return eSuccess;

   } else {                             // No current transactions to use
      if (mTransactionListNew.empty())
	 return eNoTransAvailable;         // nothing to do
      else
	 state = eFindFirstTx;
   }

   for (iter = mTransactionListNew.begin(); iter != mTransactionListNew.end(); ++iter) {

      // try to find a new transaction to start
      if ((mNumTransInFlight >= mCfg.mMaxTransInFlight) &&
	  (not (*iter)->isInternal()) &&
	  (not (*iter)->isCompletion())
	 ) {
	 if (state == eFindFirstTx) {
	    if (mCfg.mVerboseReceiverMedium) {
	       printf("%s  Info: Flow control credits unavailable(%s): Maximum number of transactions currently pending (%d) (at %s).\n", mCfg.mPrintName.c_str(), (*iter)->getPrintString(), mCfg.mMaxTransInFlight, mCfg.mCurrTimeString.c_str());
	    }
	    retStatus = eNoCreditsAvailable;  // Found a trans not done and too many already in flight
	 }
	 continue;
      }

      // If we previously failed to allocate credits for a Posted transaction
      //    then nothing passes it (except DLLPs)
      if ((failedCreditCheckPosted) &&
	  ((*iter)->getFCType() != PCIETransaction::None)) {
	 continue;
      }
      // If we previously failed to allocate credits for a NonPosted trans
      //    then we allow anything to pass it.
      //-
      // If we previously failed to allocate credits for a Completion trans
      //    then we don't allow any other Completions to pass it.
      if ((failedCreditCheckCompletion) &&
	  ((*iter)->getFCType() == PCIETransaction::Completion)) {
	 continue;
      }

      if (checkForAvailFlowControlCredits(*iter)) {
	 mPhyLayer.sendPacket( *iter );
	 retStatus = eSuccessNew;

	 if ((not (*iter)->isInternal()) && (not (*iter)->isCompletion()))
	    ++mNumTransInFlight;  // increment in-flight count for user transactions only (except cpl)
	 
	 // Add Transaction to the Retry Buffer until we get an ACK for the packet
	 if ( (*iter)->needsAck() )
	    mTransactionListNeedAck.push_back( *iter );

	 if (mCfg.mVerboseTransmitterMedium) // only print when we start the transaction
	    printf("%s  Info: Starting transaction '%s' across the bus(at %s).\n", mCfg.mPrintName.c_str(), (*iter)->getPrintString(), mCfg.mCurrTimeString.c_str());
	 (*iter) = 0;  // tag for deletion
	 
	 break;
      } else {
	 // need to check what type failed to stay within transaction
	 // ordering rules (PCIE v1.0a sec 2.4.1 (table 2-23))
	 switch ((*iter)->getFCType()) {
	 case PCIETransaction::Posted:
	    if (splitDataTransaction(false, iter)) {
	       // above function removed the current transaction and added
	       // two new ones after it if it succeeded
	    } else {
	       failedCreditCheckPosted = true;
	    }
	    break;
	 case PCIETransaction::NonPosted:
	    failedCreditCheckNonPosted = true;
	    break;
	 case PCIETransaction::Completion:
	    if (splitDataTransaction(true, iter)) {
	       // above function removed the current transaction and added
	       // two new ones after it if it succeeded
	    } else {
	       failedCreditCheckCompletion = true;
	    }
	    break;
	 default: break;
	 } // end switch
	 if (state == eFindFirstTx) {
	    retStatus = eNoCreditsAvailable;  // trans avail, but no credits for this one
	 }
      }
   } // end for loop

   // cleanup queue
   for (iter = mTransactionListNew.begin(); iter != mTransactionListNew.end(); ) {
      if ((*iter) == 0)
	 iter = mTransactionListNew.erase(iter); // remove, returns pointer to next item
      else
	 ++iter;
   }

   return retStatus;
}

void CarbonXPcieTransactor::setupBufferSizes(UInt32 vc, 
					     UInt32 posted_hdr,     UInt32 posted_data, 
					     UInt32 non_posted_hdr, UInt32 non_posted_data, 
					     UInt32 cpl_hdr,        UInt32 cpl_data) {
   if(vc != 0) {
      UtOStream* ostr = carbonInterfaceGetCout();
      carbonInterfaceWriteStringToOStream(ostr, mCfg.mPrintName.c_str());
      carbonInterfaceWriteStringToOStream(ostr, "  Error: Only VC0 is supported.");
      carbonInterfaceOStreamEndl(ostr);
      return;
   }
   
   UtOStream* ostr = carbonInterfaceGetCout();
   carbonInterfaceWriteStringToOStream(ostr, mCfg.mPrintName.c_str());
   carbonInterfaceWriteStringToOStream(ostr, " setupBufferSizes: Posted HDR = ");
   carbonInterfaceWriteUInt32ToOStream(ostr, posted_hdr);
   carbonInterfaceWriteStringToOStream(ostr, " Posted Data = ");
   carbonInterfaceWriteUInt32ToOStream(ostr, posted_data);
   carbonInterfaceOStreamEndl(ostr);
   mFcRxPostedHdrCreditsAllocated      = posted_hdr;
   mFcRxPostedDataCreditsAllocated     = posted_data;
   mFcRxNonPostedHdrCreditsAllocated   = non_posted_hdr;
   mFcRxNonPostedDataCreditsAllocated  = non_posted_data;
   mFcRxCompletionHdrCreditsAllocated  = cpl_hdr;
   mFcRxCompletionDataCreditsAllocated = cpl_data;
}

bool CarbonXPcieTransactor::checkForAvailFlowControlCredits(PCIETransaction *transaction) {
   bool flowControlAccepted = false;

   // Check Flow Control
   if ((transaction->getFCType() == PCIETransaction::None) ||  // no flow control (DLLP transaction)
       (transaction->getFCCreditsConsumed())                         // credits already consumed for this
      ) {
      flowControlAccepted = true;
   } else {
      switch (transaction->getFCType()) {
      case PCIETransaction::Posted:
	 flowControlAccepted = checkFCC(transaction, true, mFcTxPostedHdrCreditLimit, mFcTxPostedHdrCreditsConsumed, 
					mFcTxPostedDataCreditLimit, mFcTxPostedDataCreditsConsumed);
	 if(flowControlAccepted) {
	    if (mCfg.mVerboseReceiverMedium) {
	       printf("%s  Info: Flow control credits consumed: Posted HeaderFC increased by 0x%02X to 0x%02X, DataFC increased by 0x%03X to 0x%03X.\n", mCfg.mPrintName.c_str(), transaction->getFCHeaderNum(), mFcTxPostedHdrCreditsConsumed, transaction->getFCDataNum(), mFcTxPostedDataCreditsConsumed);
	       fflush(0);
	    }
	 } else {
	    if (transaction->getDataLength() > mCfg.mMaxPayloadSizeDW) {
	       if (mCfg.mVerboseReceiverMedium)
		  printf("%s  Info: %s transaction has data payload (%d bytes) that is greater than the Max_Payload_Size parameter (%d bytes).  It will be split into multiple transactions.\n", mCfg.mPrintName.c_str(), transaction->getPrintString(), (transaction->getDataLength() * 4), (mCfg.mMaxPayloadSizeDW * 4));
	    } else {
	       if (mCfg.mVerboseReceiverMedium) {
		  printf("%s  Info: Flow control credits unavailable: Posted HeaderFC requested 0x%02X with 0x%02X available, DataFC requested 0x%03X with 0x%03X available.\n", mCfg.mPrintName.c_str(), transaction->getFCHeaderNum(), ((mFcTxPostedHdrCreditLimit - mFcTxPostedHdrCreditsConsumed) & 0xFF), transaction->getFCDataNum(), ((mFcTxPostedDataCreditLimit - mFcTxPostedDataCreditsConsumed) & 0xFFF));
		  printf("%s                                          HeaderFC advertised 0x%02X, HeaderFC Consumed 0x%02X, DataFC advertised 0x%03X, DataFC consumed 0x%03X\n", mCfg.mPrintName.c_str(), mFcTxPostedHdrCreditLimit, mFcTxPostedHdrCreditsConsumed, mFcTxPostedDataCreditLimit, mFcTxPostedDataCreditsConsumed);
		  fflush(0);
	       }
	    }
	 }
	 break;
      case PCIETransaction::NonPosted:
	 flowControlAccepted = checkFCC(transaction, false, mFcTxNonPostedHdrCreditLimit, mFcTxNonPostedHdrCreditsConsumed, 
					mFcTxNonPostedDataCreditLimit, mFcTxNonPostedDataCreditsConsumed);
	 
	 if(mCfg.mVerboseReceiverMedium) {
	    if(flowControlAccepted) {
	       printf("%s  Info: Flow control credits consumed: Non-Posted HeaderFC increased by 0x%02X to 0x%02X, DataFC increased by 0x%03X to 0x%03X.\n", 
		      mCfg.mPrintName.c_str(), transaction->getFCHeaderNum(), mFcTxNonPostedHdrCreditsConsumed, transaction->getFCDataNum(), mFcTxNonPostedDataCreditsConsumed);
	    } else {
	       printf("%s  Info: Flow control credits unavailable: Non-Posted HeaderFC requested 0x%02X with 0x%02X available, DataFC requested 0x%03X with 0x%03X available.\n",
		      mCfg.mPrintName.c_str(), transaction->getFCHeaderNum(), 
		      ((mFcTxNonPostedHdrCreditLimit - mFcTxNonPostedHdrCreditsConsumed) & 0xFF), 
		      transaction->getFCDataNum(), ((mFcTxNonPostedDataCreditLimit - mFcTxNonPostedDataCreditsConsumed) & 0xFFF));
	       printf("%s                                          HeaderFC advertised 0x%02X, HeaderFC Consumed 0x%02X, DataFC advertised 0x%03X, DataFC consumed 0x%03X\n",
		      mCfg.mPrintName.c_str(), mFcTxNonPostedHdrCreditLimit, mFcTxNonPostedHdrCreditsConsumed, mFcTxNonPostedDataCreditLimit, mFcTxNonPostedDataCreditsConsumed);
	       fflush(0);
	    }
	 }
	 break;
      case PCIETransaction::Completion:
	 flowControlAccepted = checkFCC(transaction, true, mFcTxCompletionHdrCreditLimit, mFcTxCompletionHdrCreditsConsumed, 
					mFcTxCompletionDataCreditLimit, mFcTxCompletionDataCreditsConsumed);
	 if (mCfg.mVerboseReceiverMedium) {
	    if(flowControlAccepted) {
	       printf("%s  Info: Flow control credits consumed: Completion HeaderFC increased by 0x%02X to 0x%02X, DataFC increased by 0x%03X to 0x%03X.\n", 
		      mCfg.mPrintName.c_str(), transaction->getFCHeaderNum(), mFcTxCompletionHdrCreditsConsumed, transaction->getFCDataNum(), mFcTxCompletionDataCreditsConsumed);
	    }
	    else {
	       if (transaction->getDataLength() > mCfg.mMaxPayloadSizeDW) {
		  printf("%s  Info: %s transaction has data payload (%d bytes) that is greater than the Max_Payload_Size parameter (%d bytes).  It will be split into multiple transactions.\n",
			 mCfg.mPrintName.c_str(), transaction->getPrintString(), (transaction->getDataLength() * 4), (mCfg.mMaxPayloadSizeDW * 4));
	       } 
	       else {
		  printf("%s  Info: Flow control credits unavailable: Completion HeaderFC requested 0x%02X with 0x%02X available, DataFC requested 0x%03X with 0x%03X available.\n",
			 mCfg.mPrintName.c_str(), transaction->getFCHeaderNum(), ((mFcTxCompletionHdrCreditLimit - mFcTxCompletionHdrCreditsConsumed) & 0xFF), 
			 transaction->getFCDataNum(), ((mFcTxCompletionDataCreditLimit - mFcTxCompletionDataCreditsConsumed) & 0xFFF));
		  printf("%s                                          HeaderFC advertised 0x%02X, HeaderFC Consumed 0x%02X, DataFC advertised 0x%03X, DataFC consumed 0x%03X\n", mCfg.mPrintName.c_str(), mFcTxCompletionHdrCreditLimit, mFcTxCompletionHdrCreditsConsumed, mFcTxCompletionDataCreditLimit, mFcTxCompletionDataCreditsConsumed);
	       }
	    }
	 }
	 break;
      default:
	 flowControlAccepted = true; // don't block all work because of this error
         carbonInterfaceXtorPCIEUnknownFlowCtrlType(mCfg.mMsgContext, mCfg.mPrintName.c_str(), transaction->getPrintString());
	 fflush(0);
	 break;
      }
   }
   return flowControlAccepted;
}

bool CarbonXPcieTransactor::checkFCC(PCIETransaction *transaction, bool checkPayloadSize,
				     const UInt32 & HdrCreditLimit,  UInt32 & HdrCreditsConsumed, 
				     const UInt32 & DataCreditLimit, UInt32 & DataCreditsConsumed) {
   
   bool flowControlAccepted = false;

#if 0   
   // --- Debug ----
   UInt32 hdrAvailableCredits   = (HdrCreditLimit  - HdrCreditsConsumed)  & 0xFF;
   UInt32 dataAvailableCredits  = (DataCreditLimit - DataCreditsConsumed) & 0xFFF;
   
   bool hdrFlowControlAccepted  = (HdrCreditLimit  == 0xFFFFFFFF) || (hdrAvailableCredits  >= transaction->getFCHeaderNum());
   bool dataFlowControlAccepted = (DataCreditLimit == 0xFFFFFFFF) || (dataAvailableCredits >= transaction->getFCDataNum());
   bool maxPayloadAccepted      = (transaction->getDataLength() <= mMaxPayloadSizeDW);

   UtIO::cout() << mCfg.mPrintName.c_str() << " In checkFCC Hdr Avail: " << UtIO::dec << hdrAvailableCredits 
		<< " Hdr Requested: " << transaction->getFCHeaderNum()
		<< " Pass: " << hdrFlowControlAccepted << UtIO::endl;

   UtIO::cout() << mCfg.mPrintName.c_str() << " In checkFCC Data Avail: " << UtIO::dec << dataAvailableCredits 
		<< " Data Requested: " << transaction->getFCDataNum()
		<< " Pass: " << dataFlowControlAccepted << UtIO::endl;

   UtIO::cout() << mCfg.mPrintName.c_str() << " In checkFCC Max Payload: " << UtIO::dec << mMaxPayloadSizeDW 
		<< " Payload Requested: " << transaction->getDataLength()
		<< " Pass: " << maxPayloadAccepted << UtIO::endl;

   // -- End Debug ---
#endif

   if (((HdrCreditLimit == 0xFFFFFFFF) ||   // unlimited
	(((HdrCreditLimit - HdrCreditsConsumed) & 0xFF) >= transaction->getFCHeaderNum())) &&
       ((DataCreditLimit == 0xFFFFFFFF) ||  // unlimited
	(((DataCreditLimit - DataCreditsConsumed) & 0xFFF) >= transaction->getFCDataNum())) &&
       (!checkPayloadSize || (transaction->getDataLength() <= mCfg.mMaxPayloadSizeDW))
       ) {                               // consume Completion TX credits
      HdrCreditsConsumed += transaction->getFCHeaderNum();
      HdrCreditsConsumed &= 0xFF;
      DataCreditsConsumed += transaction->getFCDataNum();
      DataCreditsConsumed &= 0xFFF;
      transaction->setFCCreditsConsumed();
      flowControlAccepted = true;
   }

   return flowControlAccepted;
}
   
// Take the transaction pointed to by (**pIter) and see if there are enough credits to
// split the completion and put the first part on the bus now.  If so, remove the
// original transaction and replace it with two transactions, the part that fit on the
// bus now and the rest.
bool CarbonXPcieTransactor::splitDataTransaction(bool isCpl, PCIETransactionList::iterator iter) {
   CarbonXPcieTLP *pciecmd_orig, *pciecmd_new1, *pciecmd_new2;
   PCIETransaction *pcietx_new1, *pcietx_new2;
   bool retStatus;
   UInt32 minDWsNeeded = 0;
   UInt32 minDataCreditsNeeded = 0;
   UInt32 maxDataPayloadSizeCredits = 0;
   UInt32 maxDataCreditsAvailable = 0;
   UInt32 fullChunkCreditsUsed = 0;
   UInt32 totalDWsUsed = 0;
   UInt32 totalDataCreditsUsed = 0;

   UInt32 txHeaderCreditLimit;
   UInt32 txHeaderCreditsConsumed;
   UInt32 txDataCreditLimit;
   UInt32 txDataCreditsConsumed;

   if (isCpl) {  // transaction is completion
      txHeaderCreditLimit = mFcTxCompletionHdrCreditLimit;
      txHeaderCreditsConsumed = mFcTxCompletionHdrCreditsConsumed;
      txDataCreditLimit = mFcTxCompletionDataCreditLimit;
      txDataCreditsConsumed = mFcTxCompletionDataCreditsConsumed;
   } else {      // transaction is write
      txHeaderCreditLimit = mFcTxPostedHdrCreditLimit;
      txHeaderCreditsConsumed = mFcTxPostedHdrCreditsConsumed;
      txDataCreditLimit = mFcTxPostedDataCreditLimit;
      txDataCreditsConsumed = mFcTxPostedDataCreditsConsumed;
   }

   // no header credits - fail
   if ((txHeaderCreditLimit != 0xFFFFFFFF) &&   // not unlimited
       (((txHeaderCreditLimit - txHeaderCreditsConsumed) & 0xFF) < (*iter)->getFCHeaderNum())
       ) {
      return false;
   }

   pciecmd_orig = (*iter)->getCarbonXPcieTLP();

   // Can only split on 128 Byte boundaries for completions,
   // writes are just going to do the same

   // JJH -- 2006/05/12 -- Adding RCB to be able to switch between 64 and 128 byte boundaries

   if (isCpl) {  // transaction is completion
      minDWsNeeded = (mCfg.mLinkCtrlRCB - (pciecmd_orig->getLowAddr() & 0x7C)) / 4;
   } else {      // transaction is write
      minDWsNeeded = (mCfg.mLinkCtrlRCB - (pciecmd_orig->getAddr() & 0x7C)) / 4;
   }
   if (pciecmd_orig->getDataLength() < minDWsNeeded)
      minDWsNeeded = pciecmd_orig->getDataLength();
   minDataCreditsNeeded = (minDWsNeeded + 3) / 4;  // credits are in 4-DW units

   if (minDataCreditsNeeded == 0) {
      return false;   // this shouldn't happen, but it doesn't allow for splitting the transaction
   }
   
   maxDataPayloadSizeCredits = (mCfg.mMaxPayloadSizeDW + 3) / 4;  // max credits allowed by Max_Payload_Size
   if (txDataCreditLimit == 0xFFFFFFFF) { // unlimited
      maxDataCreditsAvailable = maxDataPayloadSizeCredits;
   } else {
      maxDataCreditsAvailable = (txDataCreditLimit - txDataCreditsConsumed) & 0xFFF;
      if (maxDataCreditsAvailable > maxDataPayloadSizeCredits)
	 maxDataCreditsAvailable = maxDataPayloadSizeCredits;
   }

   // not enough data credits - fail
   if (maxDataCreditsAvailable < minDataCreditsNeeded) {
      return false;
   }

   // FOR COMPLETIONS:
   // Have enough to get something on the bus.
   // 1) Find the largest subsection that will fit and split there
   // 2) Modify 'Length' and data of each transaction
   // 3) Modify 'Byte Count' of second transaction to be 'Byte Count'
   //    of first transaction - number of bytes sent in first transaction
   // 4) Modify LowAddr() to be 0x00 in second transaction
   // FOR WRITES:
   // Have enough to get something on the bus.
   // 1) Find the largest subsection that will fit and split there
   // 2) Modify 'Length' and data of each transaction
   // 3) Modify 'Addr' of second transaction
   // 4) Modify 'ldwbe' of first be 0xF, 'fdwbe' of second be 0xF

   // 128 Byte chunks = 32 DW chunks = 8 credit chunks are the minimum allowed granularity
   // Don't have to check if we are going over the total credits needed for this
   //   transaction, we know that we don't have enough credits for the entire
   //   transaction.

   // How many credits available as full 128 Byte chunks (after the minimum needed)?
   fullChunkCreditsUsed = (maxDataCreditsAvailable - minDataCreditsNeeded) & 0xFFFFFFF8;
   // Total Credits Used
   totalDataCreditsUsed = fullChunkCreditsUsed + minDataCreditsNeeded;
   // Total DWs Used in that span of credits
   totalDWsUsed = (fullChunkCreditsUsed * 4) + minDWsNeeded;

   //----

   pciecmd_new1 = new CarbonXPcieTLP(*pciecmd_orig);  // make copy for new smaller transaction
   pciecmd_new2 = new CarbonXPcieTLP(*pciecmd_orig);  // make copy for the rest

   // Modify the CarbonXPcieTLP objects
   //       changing the amount of data in the object will change the 'DataLength' also, not the byte count
   pciecmd_new1->resizeDataBytes(0, (totalDWsUsed * 4) - 1);                                   // keep only the first X bytes of data
   pciecmd_new1->setDataLength(totalDWsUsed);
   pciecmd_new2->resizeDataBytes((totalDWsUsed * 4), ((pciecmd_new2->getDataLength() * 4) - 1));  // keep only the rest of the data
   pciecmd_new2->setDataLength(pciecmd_new2->getDataLength() - totalDWsUsed);
   if (isCpl) {  // transaction is completion
      pciecmd_new2->setLowAddr(0x0);                                                    // second will always start on a 128B boundary
      pciecmd_new2->setByteCount( pciecmd_new2->getByteCount() - (totalDWsUsed * 4) );  // second has byte count of orig minus what is sent in first
   } else {      // transaction is write
      pciecmd_new1->setLastBE(0xF);
      pciecmd_new2->setFirstBE(0xF);
      pciecmd_new2->setAddr(pciecmd_new2->getAddr() + (totalDWsUsed * 4));
   }

   pcietx_new1 = new PCIETransaction(this, pciecmd_new1);
   pcietx_new2 = new PCIETransaction(this, pciecmd_new2);
   if (pcietx_new1->isValid() && pcietx_new2->isValid()) {
      retStatus = true;
      delete (PCIETransaction*)(*iter);  // delete old transaction from queue
//      (*iter) = pcietx_new2;                   // put second new transaction in its place
      (*iter) = 0;  // tag for removal
      ++iter;  // insert After this one
      iter = mTransactionListNew.insert(iter, pcietx_new2); // insert second new tx before 'iter'
      iter = mTransactionListNew.insert(iter, pcietx_new1); // insert first new tx before 'iter'
   } else {
      retStatus = false;
      carbonInterfaceXtorPCIESplitFailed(mCfg.mMsgContext, mCfg.mPrintName.c_str(), (*iter)->getPrintString());
      // only delete if not putting on transaction queue
      if (pcietx_new1 != 0) delete pcietx_new1;
      if (pcietx_new2 != 0) delete pcietx_new2;
   }

   //----

   if (pciecmd_new1 != 0) delete pciecmd_new1;
   if (pciecmd_new2 != 0) delete pciecmd_new2;
   return retStatus;
}


void CarbonXPcieTransactor::sendFCCUpdate() {
   //addTransactionSpecial("UpdateFC");
   CarbonXPcieDLLP dllp;
   
   // Update FCC Allocated Counter (If not infinite
   if(mFcRxPostedHdrCreditsAllocated != 0) {
      mFcRxPostedHdrCreditsAllocated = (mFcRxPostedHdrCreditsAllocated + mFcRxPostedHdrCreditsProcessed) % 256; // Rollover at 8 bits
      mFcRxPostedHdrCreditsProcessed = 0;
   }
   if(mFcRxNonPostedHdrCreditsAllocated != 0) {
      mFcRxNonPostedHdrCreditsAllocated = (mFcRxNonPostedHdrCreditsAllocated + mFcRxNonPostedHdrCreditsProcessed) % 256; // Rollover at 8 bits
      mFcRxNonPostedHdrCreditsProcessed = 0;
   }
   if(mFcRxCompletionHdrCreditsAllocated != 0) {
      mFcRxCompletionHdrCreditsAllocated = (mFcRxCompletionHdrCreditsAllocated + mFcRxCompletionHdrCreditsProcessed) % 256; // Rollover at 8 bits
      mFcRxCompletionHdrCreditsProcessed = 0;
   }
   if(mFcRxPostedDataCreditsAllocated != 0) {
      mFcRxPostedDataCreditsAllocated = (mFcRxPostedDataCreditsAllocated + mFcRxPostedDataCreditsProcessed) % 4096; // Rollover at 12 bits
      mFcRxPostedDataCreditsProcessed = 0;
   }
   if(mFcRxNonPostedDataCreditsAllocated != 0) {
      mFcRxNonPostedDataCreditsAllocated = (mFcRxNonPostedDataCreditsAllocated + mFcRxNonPostedDataCreditsProcessed) % 4096; // Rollover at 12 bits
      mFcRxNonPostedDataCreditsProcessed = 0;
   }
   if(mFcRxCompletionDataCreditsAllocated != 0) {
      mFcRxCompletionDataCreditsAllocated = (mFcRxCompletionDataCreditsAllocated + mFcRxCompletionDataCreditsProcessed) % 4096; // Rollover at 12 bits
      mFcRxCompletionDataCreditsProcessed = 0;
   }
   
   // Send UpdateFC for Posted Header and data
   dllp.setLinkType(CarbonXPcieDLLP::eUpdateFCP);
   dllp.setHdrFC(mFcRxPostedHdrCreditsAllocated);
   dllp.setDataFC(mFcRxPostedDataCreditsAllocated);
   addTransaction(dllp);
   
   // Send UpdateFC for Non Posted Header and data
   dllp.setLinkType(CarbonXPcieDLLP::eUpdateFCN);
   dllp.setHdrFC(mFcRxNonPostedHdrCreditsAllocated);
   dllp.setDataFC(mFcRxNonPostedDataCreditsAllocated);
   addTransaction(dllp);
   
   // Send UpdateFC for Completion Header and data
   dllp.setLinkType(CarbonXPcieDLLP::eUpdateFCCpl);
   dllp.setHdrFC(mFcRxCompletionHdrCreditsAllocated);
   dllp.setDataFC(mFcRxCompletionDataCreditsAllocated);
   addTransaction(dllp);
}
   
void CarbonXPcieTransactor::deleteTransactionQueue(void) {
   PCIETransactionList::const_iterator iter;
   for (iter = mTransactionListNew.begin(); iter != mTransactionListNew.end(); ++iter) {
      delete (PCIETransaction*)(*iter);  // must explicitly delete alloced pointers
   }
   mTransactionListNew.clear();

   for (iter = mTransactionListNeedAck.begin(); iter != mTransactionListNeedAck.end(); ++iter) {
      delete (PCIETransaction*)(*iter);  // must explicitly delete alloced pointers
   }
   mTransactionListNeedAck.clear();

   for (iter = mTransactionListNeedCpl.begin(); iter != mTransactionListNeedCpl.end(); ++iter) {
      delete (PCIETransaction*)(*iter);  // must explicitly delete alloced pointers
   }
   mTransactionListNeedCpl.clear();
   return;
}

void CarbonXPcieTransactor::processCompletion(CarbonXPcieTLP *cmd) {
   PCIETransactionList::iterator iter;
   bool foundIt = false;

   for (iter = mTransactionListNeedCpl.begin(); iter != mTransactionListNeedCpl.end(); ++iter) {
      if ((signed)(*iter)->getTxID() == cmd->getTransId()) {
	 foundIt = true;
	    if (mCfg.mVerboseTransmitterMedium)
	       printf("%s  Info: Matched completion transaction to '%s' from the transaction queue.\n", mCfg.mPrintName.c_str(), (*iter)->getPrintString());
	    // Don't clear the flag if we receive a split completion.  The last
	    //  completion will have the byte count equal to the data length
	    //  (minus the masked off bytes), all else will have the data length
	    //  be less than the byte count [remaining].
	    // If completion has an error status then it will be the last
	    //  completion for this transaction
	    // This works for non-data completions as well because both the data
	    //  length and byte count are zero.
	    if (((cmd->getCmdByName() == eCarbonXPcieCpl) ||              // is cpl without data
		 (cmd->getCmdByName() == eCarbonXPcieCplLk)) ||
		(cmd->getCplStatus() != 0x0) ||                        // or error completion
                (((cmd->getDataLength() * 4) - (cmd->getLowAddr() & 0x3)) >= cmd->getByteCount())) { // or last data cpl
	       if(mCfg.mVerboseTransmitterMedium)
		  printf("%s  Info: Deleting transaction '%s' from the transaction queue, sequence number 0x%03X.\n", mCfg.mPrintName.c_str(), (*iter)->getPrintString(), (*iter)->getSeqNum());
	       --mNumTransInFlight;
	       delete (PCIETransaction*)(*iter);     // must explicitly delete allocated space
	       // Here 'iter' may be invalid if we delete the first item, don't use 'iter' any more (hense the following break statement)
	       iter = mTransactionListNeedCpl.erase(iter); // return pointer to next element (or end)

	    }
	 break;
      }
   }

   if (not foundIt) {
      // This error should be printed unless the transaction was removed from the queue because
      // it didn't require a completion...
      // Again, don't print the message if we're getting the completion message only because it
      // indicates an error.
      if(cmd->getCplStatus() == 0x0)
         carbonInterfaceXtorPCIECompletionWithNoMatch(mCfg.mMsgContext, mCfg.mPrintName.c_str(), cmd->getTransId());
   }

   return;
}

void CarbonXPcieTransactor::processAck(UInt32 seqNum) {
   PCIETransactionList::iterator iter;
   bool foundIt = false;

   // error, can't have new Ack that is greater than 2048 more than
   // the last ack seq number
   if (((seqNum - mLastRxAckSeqNum) & 0xFFF) >= 0x7FF) {
      carbonInterfaceXtorPCIEInvalidAckSequenceNum(mCfg.mMsgContext, mCfg.mPrintName.c_str(), seqNum, mLastRxAckSeqNum);
      return;
   }

   // We DEPEND on the transaction list staying in order
   // so sequence numbers are in order and replays happen in order
   for (iter = mTransactionListNeedAck.begin(); iter != mTransactionListNeedAck.end(); ++iter) {
      // Need to handle 12-bit register rollover with logical equation:
      //     ((*iter)->getSeqNum() < seqNum)
      if ((((*iter)->getSeqNum() - seqNum) & 0xFFF) > 0x7FF) {
	 if (mCfg.mVerboseTransmitterHigh)
	    printf("%s  Info: Received Ack for sequence number 0x%03X, setting Ack received for sequence number 0x%03X and all earlier.\n", mCfg.mPrintName.c_str(), seqNum, (*iter)->getSeqNum());
	 (*iter)->clearNeedsAck();

	 if ((*iter)->needsCpl()) {
	    mTransactionListNeedCpl.push_back(*iter);
	 } else {
	    if (mCfg.mVerboseTransmitterMedium)
	       printf("%s  Info: Deleting transaction '%s' from the transaction queue, sequence number 0x%03X.\n", mCfg.mPrintName.c_str(), (*iter)->getPrintString(), (*iter)->getSeqNum());
	    if (not (*iter)->isCompletion())
	       --mNumTransInFlight;
	    delete (PCIETransaction*)(*iter);     // must explicitly delete allocated space
	 }
	 (*iter) = 0;  // tag for deletion

      } else if ((*iter)->getSeqNum() == seqNum) {
	 foundIt = true;

	 if (mCfg.mVerboseTransmitterHigh)
	    printf("%s  Info: Matched Ack to '%s' with sequence number 0x%03X from the transaction queue.\n", mCfg.mPrintName.c_str(), (*iter)->getPrintString(), (*iter)->getSeqNum());
	 (*iter)->clearNeedsAck();
	 mLastRxAckSeqNum = seqNum;

	 if ((*iter)->needsCpl()) {
	    mTransactionListNeedCpl.push_back(*iter);
	 } else {
	    if (mCfg.mVerboseTransmitterMedium)
	       printf("%s  Info: Deleting transaction '%s' from the transaction queue, sequence number 0x%03X.\n", mCfg.mPrintName.c_str(), (*iter)->getPrintString(), (*iter)->getSeqNum());
	    if (not (*iter)->isCompletion())
	       --mNumTransInFlight;
	    delete (PCIETransaction*)(*iter);     // must explicitly delete allocated space
	 }
	 (*iter) = 0;  // tag for deletion

      } else {
	 break;
      }
   }

   if ((not foundIt) && (seqNum != mLastRxAckSeqNum)) {  // OK to repeatedly Ack mLastRxAckSeqNum (spec rev1.0a, section 3.5.2.1)
      carbonInterfaceXtorPCIEAckWithNoMatch(mCfg.mMsgContext, mCfg.mPrintName.c_str(), seqNum, mLastRxAckSeqNum);
   }

   for (iter = mTransactionListNeedAck.begin(); iter != mTransactionListNeedAck.end(); ) {
      if ((*iter) == 0)
	 iter = mTransactionListNeedAck.erase(iter);
      else
	 ++iter;
   }

   return;
}

void CarbonXPcieTransactor::processNak(UInt32 seqNum) {
   PCIETransactionList::const_iterator iter;

   // error, can't have new Ack that is greater than 2048 more than
   // the last ack seq number
   if (((seqNum - mLastRxAckSeqNum) & 0xFFF) >= 0x7FF) {
      carbonInterfaceXtorPCIEInvalidNakSequenceNum(mCfg.mMsgContext, mCfg.mPrintName.c_str(), seqNum, mLastRxAckSeqNum);
      return;
   }

   // Nak should be with SeqNum of mLastRxAckSeqNum, but if its later...
   //   then does that mean Ack up to SeqNum and Nak afterwords?
                         // Need to handle 12-bit register rollover with logical equation:
   if (((mLastRxAckSeqNum - seqNum) & 0xFFF) > 0x7FF) {        // (seqNum > mLastRxAckSeqNum)
      processAck(seqNum);
   } else if (((seqNum - mLastRxAckSeqNum) & 0xFFF) > 0x7FF) { // (seqNum < mLastRxAckSeqNum)
      carbonInterfaceXtorPCIENakForAckTransaction(mCfg.mMsgContext, mCfg.mPrintName.c_str(), mLastRxAckSeqNum, seqNum);
      return;  // do nothing
   }

   if (mCfg.mVerboseTransmitterLow)
      printf("%s  Info: Received Nak for sequence number 0x%03X, replaying all not acknowledged transactions (at %s).\n", mCfg.mPrintName.c_str(), seqNum, mCfg.mCurrTimeString.c_str());

   mReplayTransactions = true;
   return;
}

void CarbonXPcieTransactor::updateTransactionsForNak() {

   // move all of mTransactionListNeedAck into mTransactionListNew before begin()
   // mTransactionListNeedAck ends up clear
   mTransactionListNew.splice(mTransactionListNew.begin(), mTransactionListNeedAck);

   mReplayTransactions = false;
   return;
}

void CarbonXPcieTransactor::checkCurrentTransactionDLLTraining(const CarbonXList<CarbonXPcieSym>& packet)
{
   UInt32 VC, HeaderFC, DataFC;
   UInt32 SeqNum;
   char printStr[15];

   CarbonXList<CarbonXPcieSym>::const_iterator iter = packet.begin();

   UInt32  pktslen = packet.size();
   UInt32* pkts    = CARBON_ALLOC_VEC(UInt32, pktslen);
   bool*   pktsSym = CARBON_ALLOC_VEC(bool, pktslen);

   for(UInt32 i = 0; i < pktslen; i++, ++iter) {
      pkts[i]    = iter->mData;
      pktsSym[i] = iter->mCmd;
   }
   
   if (pktslen != 8) {
      carbonInterfaceXtorPCIECorruptDLLP(mCfg.mMsgContext, mCfg.mPrintName.c_str());
      CARBON_FREE_VEC(pkts, UInt32, pktslen);
      CARBON_FREE_VEC(pktsSym, bool, pktslen);
      return;
   }

    switch(pkts[1])
       {
       case 0x00:  strcpy(printStr,"Ack");          break;
       case 0x10:  strcpy(printStr,"Nak");          break;
       case 0x20:  strcpy(printStr,"PM_Enter_L1 "); break;
       case 0x21:  strcpy(printStr,"PM_Enter_L23"); break;
       case 0x23:  strcpy(printStr,"PM_Act_ReqL1"); break;
       case 0x24:  strcpy(printStr,"PM_Req_Ack");   break;
       case 0x30:  strcpy(printStr,"Vendor_Spec");  break;
       case 0x40: case 0x41: case 0x42: case 0x43: case 0x44: case 0x45: case 0x46: case 0x47:
	  strcpy(printStr,"InitFC1-P   "); break;
       case 0x50: case 0x51: case 0x52: case 0x53: case 0x54: case 0x55: case 0x56: case 0x57:
	  strcpy(printStr,"InitFC1-NP  "); break;
       case 0x60: case 0x61: case 0x62: case 0x63: case 0x64: case 0x65: case 0x66: case 0x67:
	  strcpy(printStr,"InitFC1-Cpl "); break;
       case 0xC0: case 0xC1: case 0xC2: case 0xC3: case 0xC4: case 0xC5: case 0xC6: case 0xC7:
	  strcpy(printStr,"InitFC2-P   "); break;
       case 0xD0: case 0xD1: case 0xD2: case 0xD3: case 0xD4: case 0xD5: case 0xD6: case 0xD7:
	  strcpy(printStr,"InitFC2-NP  "); break;
       case 0xE0: case 0xE1: case 0xE2: case 0xE3: case 0xE4: case 0xE5: case 0xE6: case 0xE7:
	  strcpy(printStr,"InitFC2-Cpl "); break;
       case 0x80: case 0x81: case 0x82: case 0x83: case 0x84: case 0x85: case 0x86: case 0x87:
	  strcpy(printStr,"UpdateFC-P  "); break;
       case 0x90: case 0x91: case 0x92: case 0x93: case 0x94: case 0x95: case 0x96: case 0x97:
	  strcpy(printStr,"UpdateFC-NP "); break;
       case 0xA0: case 0xA1: case 0xA2: case 0xA3: case 0xA4: case 0xA5: case 0xA6: case 0xA7:
	  strcpy(printStr,"UpdateFC-Cpl"); break;
       default:    strcpy(printStr,"RESERVED");     break;
       }

    switch(pkts[1])
       {
       case 0x40: case 0x41: case 0x42: case 0x43: case 0x44: case 0x45: case 0x46: case 0x47:
       case 0x50: case 0x51: case 0x52: case 0x53: case 0x54: case 0x55: case 0x56: case 0x57:
       case 0x60: case 0x61: case 0x62: case 0x63: case 0x64: case 0x65: case 0x66: case 0x67:
       case 0xC0: case 0xC1: case 0xC2: case 0xC3: case 0xC4: case 0xC5: case 0xC6: case 0xC7:
       case 0xD0: case 0xD1: case 0xD2: case 0xD3: case 0xD4: case 0xD5: case 0xD6: case 0xD7:
       case 0xE0: case 0xE1: case 0xE2: case 0xE3: case 0xE4: case 0xE5: case 0xE6: case 0xE7:
	  // Virtual Channel Initialiaztion - sent in groups of three - don't remove here
	  // Virtual Channel is lower 3 bits of DLLP_Type
	  VC = (pkts[1] & 0x07);
	  // 8 bits:   Packet    |   2    |   3    |
	  //           Bit       |76543210|76543210|
	  //           HdrFC     |xx765432|10xxxxxx|
	  HeaderFC = ((pkts[2] & 0x3F) << 2) + ((pkts[3] & 0xC0) >> 6);
	  // 12 bits:  Packet    |   3    |   4    |
	  //           Bit       |76543210|76543210|
	  //           HdrFC     |xxxxBA98|76543210|
	  DataFC = ((pkts[3] & 0x0F) << 8) + pkts[4];
	  if(mCfg.mVerboseReceiverMedium)
	     printf("%s  Info: Found a DLLP Packet: %s (VC=%01X) (HdrFC=0x%02X) (DataFC=0x%03X)\n", mCfg.mPrintName.c_str(), printStr, VC, HeaderFC, DataFC);

	  if ((pkts[1] & 0x80) == 0) {
	     rxDLLSM = DLLSM_Init1;
	  } else {
	     rxDLLSM = DLLSM_Init2;
	  }

	  if (HeaderFC == 0x00) {
	     HeaderFC = 0xFFFFFFFF; // unlimited
	  }
	  if (DataFC == 0x00) {
	     DataFC = 0xFFFFFFFF; // unlimited
	  }
	  switch (pkts[1] & 0xF0)
	     {
	     case 0x40:
	     case 0xC0:
		mFcTxPostedHdrCreditLimit = HeaderFC;
		mFcTxPostedDataCreditLimit = DataFC;
		break;
	     case 0x50:
	     case 0xD0:
		mFcTxNonPostedHdrCreditLimit = HeaderFC;
		mFcTxNonPostedDataCreditLimit = DataFC;
		break;
	     case 0x60:
	     case 0xE0:
		mFcTxCompletionHdrCreditLimit = HeaderFC;
		mFcTxCompletionDataCreditLimit = DataFC;
		break;
	     }

	  //clearReceiverBuffer(currTransactionStart, currTransactionEnd);
	  break;
       case 0x80: case 0x81: case 0x82: case 0x83: case 0x84: case 0x85: case 0x86: case 0x87:
       case 0x90: case 0x91: case 0x92: case 0x93: case 0x94: case 0x95: case 0x96: case 0x97:
       case 0xA0: case 0xA1: case 0xA2: case 0xA3: case 0xA4: case 0xA5: case 0xA6: case 0xA7:
	  // Virtual Channel Updating
	  VC = (pkts[1] & 0x07);
	  HeaderFC = ((pkts[2] & 0x3F) << 2) + ((pkts[3] & 0xC0) >> 6);
	  DataFC = ((pkts[3] & 0x0F) << 8) + pkts[4];
	  if(mCfg.mVerboseReceiverMedium)
	     printf("%s  Info: Found a DLLP Packet: %s (VC=%01X) (HdrFC=0x%02X) (DataFC=0x%03X)\n", mCfg.mPrintName.c_str(), printStr, VC, HeaderFC, DataFC);
	  
	  switch (pkts[1] & 0xF0)
	     {
	     case 0x80:  // P
		if (mCfg.mVerboseReceiverMedium) {
		   printf("%s  Info: Flow control credit update: Posted ", mCfg.mPrintName.c_str());
		   if (mFcTxPostedHdrCreditLimit == 0xFFFFFFFF)
		      printf("HeaderFC unlimited, ");
		   else
		      printf("HeaderFC increased by 0x%02X to 0x%02X, ", (HeaderFC - mFcTxPostedHdrCreditLimit) & 0xFF, HeaderFC);
		   if (mFcTxPostedDataCreditLimit == 0xFFFFFFFF)
		      printf("DataFC unlimited.\n");
		   else
		      printf("DataFC increased by 0x%03X to 0x%03X.\n", (DataFC - mFcTxPostedDataCreditLimit) & 0xFFF, DataFC);
		   fflush(0);
		}

		if (mFcTxPostedHdrCreditLimit != 0xFFFFFFFF)
		   mFcTxPostedHdrCreditLimit = HeaderFC;
		if (mFcTxPostedDataCreditLimit != 0xFFFFFFFF)
		   mFcTxPostedDataCreditLimit = DataFC;
		break;
	     case 0x90:  // NP
		if (mCfg.mVerboseReceiverMedium) {
		   printf("%s  Info: Flow control credit update: Non-Posted ", mCfg.mPrintName.c_str());
		   if (mFcTxNonPostedHdrCreditLimit == 0xFFFFFFFF)
		      printf("HeaderFC unlimited, ");
		   else
		      printf("HeaderFC increased by 0x%02X to 0x%02X, ", ((HeaderFC - mFcTxNonPostedHdrCreditLimit) & 0xFF), HeaderFC);
		   if (mFcTxNonPostedDataCreditLimit == 0xFFFFFFFF)
		      printf("DataFC unlimited.\n");
		   else
		      printf("DataFC increased by 0x%03X to 0x%03X.\n", ((DataFC - mFcTxNonPostedDataCreditLimit) & 0xFFF), DataFC);
		   fflush(0);
		}

		if (mFcTxNonPostedHdrCreditLimit != 0xFFFFFFFF)
		   mFcTxNonPostedHdrCreditLimit = HeaderFC;
		if (mFcTxNonPostedDataCreditLimit != 0xFFFFFFFF)
		   mFcTxNonPostedDataCreditLimit = DataFC;
		break;
	     case 0xA0:  // eCarbonXPcieCpl
		if (mCfg.mVerboseReceiverMedium) {
		   printf("%s  Info: Flow control credit update: Completion ", mCfg.mPrintName.c_str());
		   if (mFcTxCompletionHdrCreditLimit == 0xFFFFFFFF)
		      printf("HeaderFC unlimited, ");
		   else
		      printf("HeaderFC increased by 0x%02X to 0x%02X, ", ((HeaderFC - mFcTxCompletionHdrCreditLimit) & 0xFF), HeaderFC);
		   if (mFcTxCompletionDataCreditLimit == 0xFFFFFFFF)
		      printf("DataFC unlimited.\n");
		   else
		      printf("DataFC increased by 0x%03X to 0x%03X.\n", ((DataFC - mFcTxCompletionDataCreditLimit) & 0xFFF), DataFC);
		   fflush(0);
		}

		if (mFcTxCompletionHdrCreditLimit != 0xFFFFFFFF)
		   mFcTxCompletionHdrCreditLimit = HeaderFC;
		if (mFcTxCompletionDataCreditLimit != 0xFFFFFFFF)
		   mFcTxCompletionDataCreditLimit = DataFC;
		break;
	     default:
		break;
	     }

	  //clearReceiverBuffer(currTransactionStart, currTransactionEnd);
	  break;
       case 0x00:
       case 0x10:
	  SeqNum = ((pkts[3] & 0x0F) << 8) + pkts[4];
	  if(mCfg.mVerboseReceiverMedium)
	     printf("%s  Info: Found a DLLP Packet: %s (for SeqNum=0x%03X)\n", mCfg.mPrintName.c_str(), printStr, SeqNum);

	  // register that we received an ack/nak so next step we'll process the associated transaction
	  if(pkts[1] == 0x00)         // Ack
	     processAck(SeqNum);
	  else                        // Nak
	     processNak(SeqNum);

	  //clearReceiverBuffer(currTransactionStart, currTransactionEnd);
	  break;
       default:
          carbonInterfaceXtorPCIECorruptDLLP(mCfg.mMsgContext, mCfg.mPrintName.c_str());
	  //if(mCfg.mVerboseReceiverHigh)
	  //printReceivedPacketList(currTransactionStart, currTransactionEnd);
	  // drop this, we don't know what it is.
	  //clearReceiverBuffer(currTransactionStart, currTransactionEnd);
	  break;
       }

    CARBON_FREE_VEC(pkts, UInt32, pktslen);
    CARBON_FREE_VEC(pktsSym, bool, pktslen);
    return;
}


void CarbonXPcieTransactor::checkCurrentTransactionTLPs(CarbonXList<CarbonXPcieSym>& packet) {
      
   // Pop off the SDP symbol
   packet.pop_front();

   if(packet.back() == SYM_END) {
      
      // Pop off END from end of packet
      packet.pop_back();

      // Extract CRC value from Packet
      CarbonXList<CarbonXPcieSym>::iterator crc_iter = packet.end();
      UInt32 temp  = (--crc_iter)->mData;
      temp |= (UInt32)(--crc_iter)->mData <<  8; 
      temp |= (UInt32)(--crc_iter)->mData << 16; 
      temp |= (UInt32)(--crc_iter)->mData << 24;
      
      // Remove CRC from packet
      packet.erase(crc_iter, packet.end());

      // process TLP packets (don't include the 'STP', LCRC[4], & 'END' packets)
      //  need to expand Ordered Sets of packets!!  [ expand ALL from 'STP' through 'END' ]

      //  check the TLP LCRC value is correct       [ CRC on all but 'STP' on front and LCRC[4] & 'END' on back ]
      UInt32 crc = calc32bCRC(packet.begin(), packet.end());

      CarbonXList<CarbonXPcieSym>::iterator front_iter = packet.begin();
      UInt32 trans_seq_num = (((front_iter->mData << 8) + (++front_iter)->mData) & 0x0FFF);

      if (mForcedNaks) {
	 // We must Nak all transactions until receiving a new copy of the original
	 // transaction that had an issue.  If this transaction is not the proper
	 // sequence number then it gets a Nak.  If this is the proper sequence
	 // number then we'll clear this flag and process it as normal.
	 if (trans_seq_num <= (mLastTxAckSeqNum + 1))
	    mForcedNaks = false;
	 else
	    temp = 0x00000000;
      }
      // Back to processing as normal
      if (crc != temp) {
	 if (not mForcedNaks) {
            carbonInterfaceXtorPCIEBadCRCOnTLPDoingRetry(mCfg.mMsgContext, mCfg.mPrintName.c_str(), trans_seq_num);
	    if (mCfg.mVerboseReceiverMedium) {
	       printf("         Received CRC=0x%08X; Our CRC=0x%08X\n",temp,crc);
	       printf("         TransactionPackets:(");
               for(CarbonXList<CarbonXPcieSym>::iterator iter = packet.begin(); iter != packet.end(); ++iter)
                  printf("0x%02X ",iter->mData);
	       printf(")\n");
	    }
	 }
	 // addTransactionSpecialAckNak(false, trans_seq_num);
	 addTransactionSpecialAckNak(false, mLastTxAckSeqNum);  // Nak always sends SeqNum last Ack'd until other device gets the hint and retransmits SeqNum+1
	 mForcedNaks = true;  // nak all future tx until this seq num comes back
      } else {
	 addTransactionSpecialAckNak(true, trans_seq_num);
	 mLastTxAckSeqNum = trans_seq_num;
         
         // Pop off Sequence Number from beginning of packet
         packet.pop_front(); packet.pop_front();

	 PCIECallback *tx;
	 // send in packets, minus 'STP' & SeqNum[2] on front, LCRC[4] & 'END' on back
	 if (!packet.empty()) {
	    tx = new PCIECallback(this, packet);  // initialization will process packets also
	    delete tx;
	 }
      }
   }
}

UInt32 CarbonXPcieTransactor::calc16bCRC(UInt32 *bytes, UInt32 length) {
    crcGen<16> crc(0x100B);

    // UtIO::cout() << "Bytes:";
    crc.clr();
    for(unsigned int i = 0; i < length; ++i) {
	// UtIO::cout() << UtIO::hex << " " << bytes[i] << " ";
	crc.doByte(bytes[i]);
    }
    // UtIO::cout() << UtIO::endl;

    return(crc.getResult());
}

UInt32 CarbonXPcieTransactor::calc32bCRC(UInt32 *bytes, UInt32 length) {
    crcGen<32, UInt64> crc(0x04C11DB7);
    UInt32 i;

    //UtIO::cout() << mCfg.mPrintName << "  Info: calc32bCRC: Bytes:";
    crc.clr();
    for(i=0; i < length; ++i) {
       // UtIO::cout() << UtIO::hex << " " << bytes[i] << " ";
	crc.doByte(bytes[i]);
    }
    //UtIO::cout() << "; result=" << crc.getResult() << UtIO::endl;

    return(crc.getResult());
}

UInt32 CarbonXPcieTransactor::calc32bCRC(CarbonXList<CarbonXPcieSym>::const_iterator begin, 
                                         CarbonXList<CarbonXPcieSym>::const_iterator end) {
    crcGen<32, UInt64> crc(0x04C11DB7);

    //UtIO::cout() << mCfg.mPrintName << "  Info: calc32bCRC: Bytes:";
    crc.clr();
    for(CarbonXList<CarbonXPcieSym>::const_iterator iter = begin; iter != end; ++iter) {
       //UtIO::cout() << UtIO::hex << " " << (int)iter->mData << " ";
	crc.doByte(iter->mData);
    }
    //UtIO::cout() << "; result=" << crc.getResult() << UtIO::endl;

    return(crc.getResult());
}

void CarbonXPcieTransactor::printTransactionQueue() {
   printTransactionQueueSingle(mTransactionListNeedCpl, "Trans Need Cpl Queue   ");
   printTransactionQueueSingle(mTransactionListNeedAck, "Trans Need Ack Queue   ");

//    PCIETransactionList runList;
//    if (mTransactionInProgress1 != 0)
//       runList.push_back(mTransactionInProgress1);
//    if (mTransactionInProgress2 != 0)
//       runList.push_back(mTransactionInProgress2);
//    printTransactionQueueSingle(runList,                 "Trans In Progress Queue");
//    runList.clear();

   printTransactionQueueSingle(mTransactionListNew,     "Trans Pending Queue    ");
}

void CarbonXPcieTransactor::printTransactionQueueSingle(PCIETransactionList &list, const char* name) {
   PCIETransactionList::const_iterator iter;
   UInt32 i;

   if (list.empty()) {
      printf("%s  Info: %s is Empty.\n", mCfg.mPrintName.c_str(), name);
   } else {
      // Print the transaction names
      printf("%s  Info: %s: ", mCfg.mPrintName.c_str(), name);
      for (iter = list.begin(), i=1; iter != list.end(); ++iter, ++i)
	 if ((*iter) == 0)
	    printf("[%d: ---] ", i);
	 else if ((*iter)->isTLPTransaction())
	    printf("[%d: %s, SeqNum:0x%X] ", i, (*iter)->getPrintString(), (*iter)->getSeqNum());
	 else
	    printf("[%d: %s] ", i, (*iter)->getPrintString());
      printf("\n");
   }
   return;
}

CarbonXPcieNet *CarbonXPcieTransactor::getNet(CarbonXPcieSignalType sigType, UInt32 index) {
   return mPhyLayer.getNet(sigType, index);
}


void CarbonXPcieTransactor::setElecIdleActiveState(bool tf) {
   if (mIntfType == eCarbonXPcie10BitIntf) {
      mCfg.mEIdleActiveHigh = tf;
   } else if (mIntfType == eCarbonXPciePipeIntf) {
      if(mCfg.mVerboseReceiverLow)
	 printf("%s  WARNING: Electrical idle signal's active level cannot be changed when using the eCarbonXPciePipeIntf interface.\n", mCfg.mPrintName.c_str());
   }
}

bool CarbonXPcieTransactor::isConfigured(void) {
    bool status = true;

    if ((mCfg.mLinkWidth != 1) && (mCfg.mLinkWidth != 2) && (mCfg.mLinkWidth != 4) && (mCfg.mLinkWidth != 8)
        && (mCfg.mLinkWidth != 12) && (mCfg.mLinkWidth != 16) && (mCfg.mLinkWidth != 32))
       status = false;

    if (mIntfType != eCarbonXPcie10BitIntf && mIntfType != eCarbonXPciePipeIntf)
       status = false;

    if (not mTransCBFn) {    // need to tell us where to call when we
       status = false;       // receive a transaction
    }

    // save a copy of the latest return value so we don't need to call the function again
    mFullyConfigured = status;
    // clear flag so the user can be warned again if the config is still invalid and they try to call step()
    mHasBeenWarnedAboutConfig = false;
    return status;
}

void CarbonXPcieTransactor::printConfiguration(void) {
   printf("\n");
}

bool CarbonXPcieTransactor::isLinkActive(void) {
   // return ((curInputState==Active) && (curOutputState==Active));
   if ( mPhyLayer.isPhysicalLinkUp() && (txDLLSM==DLLSM_Active) && (rxDLLSM==DLLSM_Active) ) {
      return true;
   } else {
      return false;
   }
}

void CarbonXPcieTransactor::printLinkState(void) {
   printf("**** PRINTING LINK STATE ****\n");
//    printf("Transmitter for %s: %s\n", mCfg.mPrintName.c_str(), curOutputStateStr);
//    printf("Receiver for %s:    %s\n", mCfg.mPrintName.c_str(), curInputStateStr);
//    printf("Time: %s\n", mCfg.mCurrTimeString.c_str());

//    printf(  "Transmitter Link Training State Machine:   %s", getLTSMStateString(txLTSM));
//    printf("\nReceiver Link Training State Machine:      %s", getLTSMStateString(rxLTSM));
//    printf("\nTransmitter Data Link Layer State Machine: %s", getDLLSMStateString(txDLLSM));
//    printf("\nReceiver Data Link Layer State Machine:    %s", getDLLSMStateString(rxDLLSM));
//    printf("\n");
}

bool CarbonXPcieTransactor::getReturnData(CarbonXPcieTLP *cmd) {
   bool retStatus = true;
   CarbonXPcieTLP *our_copy = 0;

   // Copy in the next Return Data CarbonXPcieTLP we have stored up
   // then delete our copy

   if (cmd == 0) {
      carbonInterfaceXtorPCIEGetReturnDataCmdIsNull(mCfg.mMsgContext, mCfg.mPrintName.c_str());
      return false;
   }

   if (mReturnDataList.empty()) {
      retStatus = false;
   } else {
      our_copy = mReturnDataList.front();
      cmd->copy(*our_copy);
      mReturnDataList.pop_front();
      mFcRxCompletionHdrCreditsProcessed  += our_copy->getFcHeader();
      mFcRxCompletionDataCreditsProcessed += our_copy->getFcData();

      if(mCfg.mVerboseReceiverMedium) {
         UtOStream* ostr = carbonInterfaceGetCout();
         carbonInterfaceWriteStringToOStream(ostr, mCfg.mPrintName.c_str());
         carbonInterfaceWriteStringToOStream(ostr, "  Info: Flow control credits processed: Completion HeaderFC increased by ");
         carbonInterfaceWriteUInt32ToOStream(ostr, our_copy->getFcHeader());
         carbonInterfaceWriteStringToOStream(ostr, " to ");
         carbonInterfaceWriteUInt32ToOStream(ostr, mFcRxCompletionHdrCreditsProcessed);
         carbonInterfaceWriteStringToOStream(ostr, " DataFC increased by ");
         carbonInterfaceWriteUInt32ToOStream(ostr, our_copy->getFcData());
         carbonInterfaceWriteStringToOStream(ostr, " to ");
         carbonInterfaceWriteUInt32ToOStream(ostr, mFcRxCompletionDataCreditsProcessed);
         carbonInterfaceOStreamEndl(ostr);
      }

      delete our_copy;
   }

   return retStatus;
}

bool CarbonXPcieTransactor::getReceivedTrans(CarbonXPcieTLP *cmd) {
   bool retStatus = true;
   CarbonXPcieTLP *our_copy = 0;

   // Copy in the next Received Data CarbonXPcieTLP we have stored up
   // then delete our copy

   if (cmd == 0) {
      carbonInterfaceXtorPCIEGetReceivedTransCmdIsNull(mCfg.mMsgContext, mCfg.mPrintName.c_str());
      return false;
   }

   if (mReceivedTransList.empty()) {
      retStatus = false;
   } else {
      our_copy = mReceivedTransList.front();
      cmd->copy(*our_copy);
      mReceivedTransList.pop_front();

      // Update Flow Control that this packet has been processed
      switch (our_copy->getFcType()) {
      case PCIETransaction::Posted :
	 mFcRxPostedHdrCreditsProcessed      += our_copy->getFcHeader();
	 mFcRxPostedDataCreditsProcessed     += our_copy->getFcData();
	 
	 if(mCfg.mVerboseReceiverMedium) {
            UtOStream* ostr = carbonInterfaceGetCout();
            carbonInterfaceWriteStringToOStream(ostr, mCfg.mPrintName.c_str());
            carbonInterfaceWriteStringToOStream(ostr, "  Info: Flow control credits processed: Posted HeaderFC increased by ");
            carbonInterfaceWriteUInt32ToOStream(ostr, our_copy->getFcHeader());
            carbonInterfaceWriteStringToOStream(ostr, " to ");
            carbonInterfaceWriteUInt32ToOStream(ostr, mFcRxPostedHdrCreditsProcessed);
            carbonInterfaceWriteStringToOStream(ostr, " DataFC increased by ");
            carbonInterfaceWriteUInt32ToOStream(ostr, our_copy->getFcData());
            carbonInterfaceWriteStringToOStream(ostr, " to ");
            carbonInterfaceWriteUInt32ToOStream(ostr, mFcRxPostedDataCreditsProcessed);
            carbonInterfaceOStreamEndl(ostr);
         }

	 break;

      case PCIETransaction::NonPosted :
	 mFcRxNonPostedHdrCreditsProcessed   += our_copy->getFcHeader();
	 mFcRxNonPostedDataCreditsProcessed  += our_copy->getFcData();

	 if(mCfg.mVerboseReceiverMedium){
            UtOStream* ostr = carbonInterfaceGetCout();
            carbonInterfaceWriteStringToOStream(ostr, mCfg.mPrintName.c_str());
            carbonInterfaceWriteStringToOStream(ostr, "  Info: Flow control credits processed: Non Posted HeaderFC increased by ");
            carbonInterfaceWriteUInt32ToOStream(ostr, our_copy->getFcHeader());
            carbonInterfaceWriteStringToOStream(ostr, " to ");
            carbonInterfaceWriteUInt32ToOStream(ostr, mFcRxNonPostedHdrCreditsProcessed);
            carbonInterfaceWriteStringToOStream(ostr, " DataFC increased by ");
            carbonInterfaceWriteUInt32ToOStream(ostr, our_copy->getFcData());
            carbonInterfaceWriteStringToOStream(ostr, " to ");
            carbonInterfaceWriteUInt32ToOStream(ostr, mFcRxNonPostedDataCreditsProcessed);
            carbonInterfaceOStreamEndl(ostr);
         }

	 break;

      case PCIETransaction::Completion :
	 mFcRxCompletionHdrCreditsProcessed  += our_copy->getFcHeader();
	 mFcRxCompletionDataCreditsProcessed += our_copy->getFcData();
	 break;

      default : // Can't happen
	 break;
      }
      delete our_copy;
   }
   
   return retStatus;
}

const char* CarbonXPcieTransactor::getDLLSMStateString(pcie_DLLSM_state_t sm) {
   switch(sm) {
   case DLLSM_Inactive: return "Inactive";
   case DLLSM_Init1:    return "Init1";
   case DLLSM_Init2:    return "Init2";
   case DLLSM_Active:   return "Active";
   default:             return "ERROR";
   }
}

bool CarbonXPcieTransactor::startTraining(void) {
   bool result;
   if (!mPhyLayer.isPhysicalLinkUp()) {
      mPhyLayer.startTraining(0);
      mCfg.mUpstreamDevice = true;
      result = true;
   } else {
      result = false;
   }
   return result;
}

bool CarbonXPcieTransactor::startTraining(UInt64 tick) {
   bool result;
   if (startTraining()) {
      mStartTrainingPrefaceIdleTime = tick;
      result = true;
   } else {
      result = false;
   }
   return result;
}

UInt32 CarbonXPcieTransactor::getCurrentSpeed() {
   return mPhyLayer.getDataRate() >> 1;
}

void   CarbonXPcieTransactor::setRateChangeCallbackFn(void *caller, CarbonXPcieRateChangeCBFn *rateChangeCbFn) {
   mPhyLayer.setDataRateChangeCBFn(rateChangeCbFn, caller);
}

CarbonXPcieRateChangeCBFn* CarbonXPcieTransactor::getRateChangeCallbackFn() {
   return mPhyLayer.getDataRateChangeCBFn();
}

void CarbonXPcieTransactor::ackDataRateChange() {
   mPhyLayer.ackDataRateChange();
}

bool CarbonXPcieTransactor::nothingToDo() {

   return (mPhyLayer.isPhysicalLinkUp() &&
	   rxDLLSM >= DLLSM_Active && txDLLSM >= DLLSM_Active && 
	   mPhyLayer.txIsDone());
}

void CarbonXPcieTransactor::callTransCallbackFn(CarbonXPcieCallbackType type, CarbonXPcieTLP *data) {
   // make Copy of 'data' object so it doesn't go out of scope or get deleted on us
   CarbonXPcieTLP *data_copy = new CarbonXPcieTLP(*data);  // deleted when pop'd off the queue
   if (type == eCarbonXPcieReceivedTrans) {
      mReceivedTransList.push_back(data_copy);
   } else if (type == eCarbonXPcieReturnData) {
      // clear completion flag from appropriate transaction
      processCompletion(data_copy);
      mReturnDataList.push_back(data_copy);
   } else {
      carbonInterfaceXtorPCIEUnknownTransactionCategory(mCfg.mMsgContext, mCfg.mPrintName.c_str(), getStrFromType(type), type);
   }

   mTransCBFn(this, type, mTransCBInfo);
}

const char* CarbonXPcieTransactor::getSymStringFromByte(UInt32 byte) {
   switch(byte) {
   case PCIE_COM:   return "COM";
   case PCIE_STP:   return "STP";
   case PCIE_SDP:   return "SDP";
   case PCIE_END:   return "END";
   case PCIE_EDB:   return "EDB";
   case PCIE_PAD:   return "PAD";
   case PCIE_SKP:   return "SKP";
   case PCIE_FTS:   return "FTS";
   case PCIE_IDL:   return "IDL";
   case PCIE_RSVD1:
   case PCIE_RSVD2:
   case PCIE_RSVD3:
                    return "RSVD";
   default:
                    return "INVALID";
   }
}

const char* CarbonXPcieTransactor::getStrFromType (CarbonXPcieIntfType type) {
   switch (type) {
   case eCarbonXPcie10BitIntf: return "eCarbonXPcie10BitIntf";
   case eCarbonXPciePipeIntf:  return "eCarbonXPciePipeIntf";
   default:                   return "Invalid Type";
   }
}

const char* CarbonXPcieTransactor::getStrFromType (CarbonXPcieCallbackType type) {
   switch (type) {
   case eCarbonXPcieReturnData:    return "eCarbonXPcieReturnData";
   case eCarbonXPcieReceivedTrans: return "eCarbonXPcieReceivedTrans";
   default:                       return "Invalid Type";
   }
}

/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
