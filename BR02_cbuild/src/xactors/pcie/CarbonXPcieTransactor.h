// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CarbonXPcieTransactor_H_
#define __CarbonXPcieTransactor_H_

#include <cstdio>
#include "carbonXPcie.h"
#include "CarbonXPcieCommonTypes.h"
#include "CarbonXPcieNet.h"
#include "PCIETransaction.h"
#include "CarbonXPciePhyLayer.h"
#include "CarbonX8b10b.h"
#include "util/c_memmanager.h"
#include "CarbonXArray.h"
#include "CarbonXList.h"
#include "util/UtCLicense.h"

// CarbonXPcieTransactor class
class CarbonXPcieTransactor {
public: CARBONMEM_OVERRIDES ;
   
   typedef enum {
      // Data Link Layer State Machine states
      DLLSM_Inactive = 0,	// DLL is not initialized
      DLLSM_Init1 = 1,	// Sent Stage 1 Init DLLPs
      DLLSM_Init2 = 2,	// Sent Stage 2 Init DLLPs
      DLLSM_Active = 3	// DLL is initialized
   } pcie_DLLSM_state_t;
   
   // Completion Status
   typedef enum {
      PCIE_Cpl_SC = 0,	// Successful Completion
      PCIE_Cpl_UR = 1,	// Unsupported Request
      PCIE_Cpl_CRS = 2,	// Configuration Request Retry Status
      PCIE_Cpl_CA = 4,	// Completer Abort
      PCIE_Cpl_UKN = 8	// Unknown / Reserved
   } pcie_cpl_status_t;
   
   // Constructor.
   CarbonXPcieTransactor(CarbonXPcieIntfType intf, UInt32 linkwidth, const char* inPrintName, FILE* stream = stderr);
   // Void Constructor.
   CarbonXPcieTransactor(void);
   // Other Constructor.
   CarbonXPcieTransactor(char* cmdfile, UInt32 linkwidth, const char* inPrintName, FILE* stream = stderr);

   // Destructor.
   ~CarbonXPcieTransactor(void);

   //! Get configuration structure
   CarbonXPcieConfig& getCfg() { return mCfg; }

   //! Reset transactor to powerup state
   void reset();

   // Submit an idle event to the the transactor.
   void idle(UInt64 cycles);

   // Tell the transactor its clock has incremented.
   void step(UInt64 tick);

   // Just to keep the linker happy
   void print(bool verbose = true, UInt32 indent = 0) { 
      (void)verbose; (void)indent;
   }

   // Set the timescale factor for the CarbonTime sent to the step() function.
   void setTimescale(CarbonTimescale timescale);

   // Set Supported Speeds
   void setSupportedSpeeds(UInt32 speeds);
   
   // Get Supported Speeds
   UInt32 getSupportedSpeeds();

   // Get Current Speeds
   UInt32 getCurrentSpeed();

   // Set Verbosity of the transactor - Receiver, Transmitter, or Both; High, Low, or Off.
   void verbose(UInt32 flags);

   // Check if we're in reset and reset the SMs if we are
   void checkReset(void);
   // Check if we're seeing TxDetectRx assert and we'll respond
   void checkTxDetectRx(void);

   inline UInt32 getNewDLLPSeqNum(void) { return(mDLLPSeqNum++ % 4096); };
   bool isDoneForNow(void) { return mDoneForNow; }  // let outside world know if we're done working for now (& clear)
   CarbonXPcieIntfType getInterfaceType(void) { return mIntfType; }

   SInt32 addTransaction(const CarbonXPcieTLP*);   // return transaction ID, or -1 for error/invalid
   SInt32 addTransaction(const CarbonXPcieDLLP &);   // return transaction ID, or -1 for error/invalid
   SInt32 addTransaction(PCIETransaction *);   // return transaction ID, or -1 for error/invalid
   //SInt32 addTransaction(const PCIETransaction & trans); // return transaction ID, or -1 for error/invalid
   bool readCfgFile(char* cmdfile);

   // Data Link Layer State Machine processing
   void DLLSM_process(void);

   inline UInt32 getUniqueTag(void) { return(next_tag++); };

   UInt32 calc16bCRC(UInt32*,UInt32);
   UInt32 calc32bCRC(UInt32*,UInt32);
   UInt32 calc32bCRC(CarbonXList<CarbonXPcieSym>::const_iterator begin, CarbonXList<CarbonXPcieSym>::const_iterator end);

   UInt8 getCplData(UInt32 idx) { return mCplData[idx]; }
   void clrCplData(void) { mCplData.clear(); }
   void setCplData(UInt32 idx, UInt8 val) {
      if( not (idx >= mCplData.size()) ) {
	 mCplData[idx] = val;
      } else {
	 printf("ERROR -- CarbonXPcieTransactor::setCplData -- Index=%d out of range\n", idx);
      }
   }
   UInt32 getCplDataSize(void) { return mCplData.size(); }
   void setCplDataSize(UInt32 size) { mCplData.resize(size,0); }
   pcie_cpl_status_t getCplStatus(void) { return mCplStatus; }
   void setCplStatus(pcie_cpl_status_t s) { mCplStatus = s; }

   bool   getDoneForNow(void) { return mDoneForNow; }
   void   setDoneCallbackFn(CarbonXPcieDoneCBFn *doneCbFn, void* doneCbInfo) { mDoneCBFn=doneCbFn; mDoneCBInfo=doneCbInfo; }
   CarbonXPcieDoneCBFn* getDoneCallbackFn(void) { return mDoneCBFn; }
   void*  getDoneCallbackData(void) { return mDoneCBInfo; }

   CarbonXPcieNet * getNet(CarbonXPcieSignalType sigType, UInt32 index);

   bool   isConfigured(void);
   void   printConfiguration(void);
   bool	  isLinkActive(void);
   void   printLinkState(void);

   // Allow the user to get their return data and received transactions
   void   setTransCallbackFn(CarbonXPcieTransCBFn *transCbFn, void* transCbInfo) { mTransCBFn=transCbFn; mTransCBInfo=transCbInfo; }
   CarbonXPcieTransCBFn* getTransCallbackFn(void) { return mTransCBFn; }
   void*  getTransCallbackData(void) { return mTransCBInfo; }

   void   setStartTransCallbackFn(void *caller, CarbonXPcieStartTransCBFn *transCbFn) { mStartTransCBInfo = caller; mStartTransCBFn=transCbFn; }
   CarbonXPcieStartTransCBFn* getStartTransCallbackFn(void) { return mStartTransCBFn; }

   void   setEndTransCallbackFn(void *caller, CarbonXPcieStartTransCBFn *transCbFn) { mEndTransCBInfo = caller; mEndTransCBFn=transCbFn; }
   CarbonXPcieStartTransCBFn* getEndTransCallbackFn(void) { return mEndTransCBFn; }

   void   setRateChangeCallbackFn(void *caller, CarbonXPcieRateChangeCBFn *rateChangeCbFn);
   CarbonXPcieRateChangeCBFn* getRateChangeCallbackFn();

   void ackDataRateChange();

   // Return head of the return data queue
   bool   getReturnData     (CarbonXPcieTLP *cmd);                        // Get CarbonXPcieTLP object with return data from transaction referred to by seqNum in the CarbonXPcieTLP object
   // Length of return data queue
   UInt32 getReturnDataQueueLength(void) { return mReturnDataList.size(); }
   // Return head of the received transaction queue
   bool   getReceivedTrans  (CarbonXPcieTLP *cmd);                        // Get CarbonXPcieTLP object with received transaction information
   // Length of received transaction queue
   UInt32 getReceivedTransQueueLength(void) { return mReceivedTransList.size(); }

   bool   startTraining(void);
   bool   startTraining(UInt64 tick);
   
   // Flow Control Setup
   void setupBufferSizes(UInt32 vc, UInt32 posted_hdr, UInt32 posted_data, UInt32 non_posted_hdr, UInt32 non_posted_data, UInt32 cpl_hdr, UInt32 cpl_data);

   // Called from other Internal PCIE classes (class PCIExpressCallback)
   void   callTransCallbackFn(CarbonXPcieCallbackType type, CarbonXPcieTLP *data);   // call the callback function with a Pointer to this data object and reason for calling
   const char* getSymStringFromByte(UInt32);

   static const char* getStrFromType (CarbonXPcieIntfType type);
   static const char* getStrFromType (CarbonXPcieCallbackType type);
   static const char* getStrFromType (CarbonXPcieSignalType type);

//    const char* getTimeString(void) { return mCurrTimeString.c_str(); }
   const char* getDLLSMStateString(pcie_DLLSM_state_t sm);

private:
   typedef enum {
      eSuccess,
      eSuccessNew,
      eSuccessDone,
      eNoTransAvailable,
      eNoCreditsAvailable,
      eError = 99
   } GetTransactionReturnStatus;

   typedef CarbonXList<PCIETransaction *> PCIETransactionList;

   CarbonXPcieConfig mCfg;

   UInt32 mDLLPSeqNum;       // xtor DLLP Sequence Number
   UInt32 mNumTransInFlight; // current number of transactions in flight, value must be between 0 and mMaxTransInFlight

   // Declare invalid constructors as private
   CarbonXPcieTransactor(const CarbonXPcieTransactor&);  
   CarbonXPcieTransactor& operator=(const CarbonXPcieTransactor&);
   UInt32 scramble_descramble_byte(UInt32,bool,bool,UInt32);

   void printTransactionQueue(void);
   void printTransactionQueueSingle(PCIETransactionList&, const char*);
   void printReceivedPacketList(void);
   void printReceivedPacketList(UInt32 start, UInt32 end);

   UInt32 next_tag;

   // Adding Training Transactions, not user transactions
   void addTransactionSpecial(UInt32 idles);
   void addTransactionSpecialHead(const char*);
   void addTransactionSpecialAckNak(bool, UInt32);

   // Get pointer to transaction to be active currently or next
   GetTransactionReturnStatus getNewTransaction(void);
   // Check transaction for flow control credits
   bool checkForAvailFlowControlCredits(PCIETransaction*);
   // Check transaction flow control credits, this function takes the limit and consumed
   // FCC for the type of the transaction
   bool checkFCC(PCIETransaction *transaction, bool checkPayloadSize,
		 const UInt32 & hdrCreditLimit,  UInt32 & hdrCreditConsumed, 
		 const UInt32 & dataCreditLimit, UInt32 & dataCreditConsumed);
   // See if completion or write can be split to go on the bus, and if so then do the split
   bool splitDataTransaction(bool, PCIETransactionList::iterator);
   // Send Update FCC DLLPs
   void sendFCCUpdate(); 
   // Remove all transactions from queue
   void deleteTransactionQueue(void);
   // Process a completion message to clear the needsCpl flag from another transaction on the queue
   void processCompletion(CarbonXPcieTLP*);
   // Process a ack message to clear the needsAck flags
   void processAck(UInt32);
   // Process a nak message to replay the transactions on the queue
   void processNak(UInt32);
   // Move transactions back onto 'new' queue to be replayed
   void updateTransactionsForNak(void);
   
   bool nothingToDo();

   // License Handle
   CarbonLicenseData *mCarbonLicense;

   //bool mScrambleEnable;           // Scramble/Descramble Enable for Receiver & Transmitter
   bool mDoneForNow;               // Are we done processing all of our commands / tasks? (IE, we don't need any more sim time right now)
   bool mInReset;                  // Are we in Reset currently?
   UInt32 mResetTimer;             // timer that counts when we leave reset
   UInt32 mDetectRxTimer;          // timer that counts ticks during TxDetectRx training session

   void checkCurrentTransactionDLLTraining(const CarbonXList<CarbonXPcieSym>& packet);  // find & parse DLLPs on queue
   void checkCurrentTransactionTLPs(CarbonXList<CarbonXPcieSym>& packet);         // find & parse TLPs on queue

   // take starting id for packets held and length, return a buffer of packets with Ordered Sets expanded (in = 5 bytes including 1 ordered set, out = 12 bytes [linkwidth=8])
   // find the number of packets/packetsets in END of newTransactionPackets to get 'x' individual packets
   UInt32 findNumPacketsinPacketSets(UInt32);

   // Array of DLLP Layer bytes to be processed once the entire transaction is received (only need 22: DLLP Header(2) + TLP(<=16) + DLLP Footer(4))
   UInt32 mIncomingDLLPBytes[22];
   UInt32 mIncomingDLLPBytesNext;    // Where to put the next byte (index var)
   // Array of packets for incoming transaction, hold packets until we see a useful transaction and act on it
   static const unsigned int TRANS_BUF_SIZE = 4200;
   
   // Keep track of cycle count for adding SKP ordered sets
   UInt32 cycle_count_skp;

   // Are our transmitters currently powered down?
   UInt32 curPowerDnState;

   // Rate Change Request
   bool mChangeRate;

   // Receiver Data Link Layer State Machine
   pcie_DLLSM_state_t rxDLLSM;
   // Transmitter Data Link Layer State Machine
   pcie_DLLSM_state_t txDLLSM;

   // Store transaction data for last Completion operation received
   CarbonXArray<UInt32> mCplData;  // vector of bytes for Completion data
   // Store status for last Completion operation received
   pcie_cpl_status_t mCplStatus;

   // Flag that the transactor has been properly configured.  We check this before
   //  executing so that we don't end up with segfaults!
   bool mFullyConfigured;
   bool mHasBeenWarnedAboutConfig;

   // Flag that we're bypassing training or not.
   bool mTrainingBypass;
   // Flag that we're going to start training from Electrical Idle
   UInt32 mStartTraining;   // 0=no_action, 1=start_training, 2=in_progress, etc
   UInt64 mStartTrainingPrefaceIdleTime;  // number of cycles to run logical idle before we start sending training packets
   bool   mImLeader;

   CarbonXPcieDoneCBFn *mDoneCBFn;
   void *mDoneCBInfo;

   CarbonXPcieTransCBFn *mTransCBFn;
   void *mTransCBInfo;

   CarbonXPcieStartTransCBFn *mStartTransCBFn;
   void *mStartTransCBInfo;

   CarbonXPcieStartTransCBFn *mEndTransCBFn;
   void *mEndTransCBInfo;

   bool mFirstCall;   // have we run our first call to step()?

   bool mReplayTransactions;  // do we need to replay all transactions

   CarbonXPciePhyLayer mPhyLayer;

   bool mForcedNaks; // set if we are forcing transactions to be Nak'd (until erroring transaction is retransmitted)
   UInt32 mLastTxAckSeqNum; // the last properly Ack'd sequence number we sent
   UInt32 mLastRxAckSeqNum; // the last properly Ack'd sequence number we received

   UInt64 mTxTransStartTick; // On what tick did the current transmitt transaction start
   UInt64 mRxTransStartTick; // On what tick did the current receive transaction start

   CarbonXPcieIntfType mIntfType;  // what is the interface type (10bit, pipe, other)

   bool isConfigured_10BitIntf(void);
   bool isConfigured_PipeIntf(void);

   const char* getStringFromPCIESignalType(CarbonXPcieSignalType);
   void setElecIdleActiveState(bool);

   PCIETransaction *mTransactionInProgress1;     // Currently being transmitted, if any
   PCIETransaction *mTransactionInProgress2;     // Currently being transmitted (Back-to-back non-padded), if any
   PCIETransactionList mTransactionListNew;      // list of PCIETransaction objects for xtor to drive onto the bus (added by user, pop'd by xtor)
   PCIETransactionList mTransactionListNeedAck;  // list of PCIETransaction objects which have been transmitted, waiting for Ack (moved by xtor from mTransactionListNew to here)
   PCIETransactionList mTransactionListNeedCpl;  // list of PCIExpressTransction objects which have been Ack'd, waiting for completion (moved by xtor from mTransactionListNeedAck to here)

   typedef CarbonXList<CarbonXPcieTLP *> PCIECmdList;  // const access to front & back, linear access to middle
   PCIECmdList mReceivedTransList;    // list of PCIECmd objects for received transactions (to be added by xtor, pop'd by user)
   PCIECmdList mReturnDataList;       // list of PCIECmd objects for return data (to be added by xtor, pop'd by user)


   // Flow Control Data
   //   Credits allocated by DUT for amt we can tx
   UInt32 mFcTxPostedHdrCreditLimit;      // P   HdrFC  Limit advertised
   UInt32 mFcTxPostedDataCreditLimit;     // P   DataFC Limit advertised
   UInt32 mFcTxNonPostedHdrCreditLimit;   // NP  HdrFC  Limit advertised
   UInt32 mFcTxNonPostedDataCreditLimit;  // NP  DataFC Limit advertised
   UInt32 mFcTxCompletionHdrCreditLimit;  // Cpl HdrFC  Limit advertised
   UInt32 mFcTxCompletionDataCreditLimit; // Cpl DataFC Limit advertised

   UInt32 mFcTxPostedHdrCreditsConsumed;      // P   HdrFC  Credits consumed
   UInt32 mFcTxPostedDataCreditsConsumed;     // P   DataFC Credits consumed
   UInt32 mFcTxNonPostedHdrCreditsConsumed;   // NP  HdrFC  Credits consumed
   UInt32 mFcTxNonPostedDataCreditsConsumed;  // NP  DataFC Credits consumed
   UInt32 mFcTxCompletionHdrCreditsConsumed;  // Cpl HdrFC  Credits consumed
   UInt32 mFcTxCompletionDataCreditsConsumed; // Cpl DataFC Credits consumed
   
   UInt32 mFcRxPostedHdrCreditsAllocated;      // P   HdrFC  Limit advertised
   UInt32 mFcRxPostedDataCreditsAllocated;     // P   DataFC Limit advertised
   UInt32 mFcRxNonPostedHdrCreditsAllocated;   // NP  HdrFC  Limit advertised
   UInt32 mFcRxNonPostedDataCreditsAllocated;  // NP  DataFC Limit advertised
   UInt32 mFcRxCompletionHdrCreditsAllocated;  // Cpl HdrFC  Limit advertised
   UInt32 mFcRxCompletionDataCreditsAllocated; // Cpl DataFC Limit advertised

   UInt32 mFcRxPostedHdrCreditsProcessed;      // P   HdrFC  Credits consumed
   UInt32 mFcRxPostedDataCreditsProcessed;     // P   DataFC Credits consumed
   UInt32 mFcRxNonPostedHdrCreditsProcessed;   // NP  HdrFC  Credits consumed
   UInt32 mFcRxNonPostedDataCreditsProcessed;  // NP  DataFC Credits consumed
   UInt32 mFcRxCompletionHdrCreditsProcessed;  // Cpl HdrFC  Credits consumed
   UInt32 mFcRxCompletionDataCreditsProcessed; // Cpl DataFC Credits consumed
   
   // How Long Transmitter should be idle before we consider there is
   // nothing more to send. This is used both for isDone and to determine
   // when to go into L0s state.
   UInt32 mIdlesBeforeDone;
};

#endif

/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
