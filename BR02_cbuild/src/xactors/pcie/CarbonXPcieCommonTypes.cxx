/***************************************************************************************
  Copyright (c) 2007-2008 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#include "CarbonXPcieCommonTypes.h"
#include "shell/carbon_interface_xtor.h"
#include "util/OSWrapper.h"

CarbonXPcieConfig::CarbonXPcieConfig(UInt32 linkwidth, const char* inPrintName, FILE* stream) :
  mCurrTick(0),
  mErrorStream(NULL),
  mMsgContext(NULL),
  mLinkWidth(linkwidth),
  mMaxPayloadSizeDW(32),
  mLinkCtrlRCB(128),
  mForceEIdleInactive(false),
  mEIdleActiveHigh(true),
  mMaxTransInFlight(32),
  mScrambleEnable(true),
  mTrainingBypass(false),
  mSupportedDataRate(0),
  mBusNum(0),
  mDevNum(0),
  mFuncNum(0),
  mUpstreamDevice(false),
  mTransCBFn(0), 
  mTransCBInfo(0),
  mPrintName(inPrintName),
  mCurrTimescaleValid(false),
  mCurrTimescale(e1ns),
  mTimeFactor(0),
  mVerboseReceiverHigh(0),
  mVerboseReceiverMedium(0),
  mVerboseReceiverLow(0),
  mVerboseTransmitterHigh(0),
  mVerboseTransmitterMedium(0),
  mVerboseTransmitterLow(0)

{
  mErrorStream = carbonInterfaceXtorAllocMsgStream(stream, true);
  mMsgContext= carbonInterfaceXtorAllocMsgContext(mErrorStream);
}

CarbonXPcieConfig::~CarbonXPcieConfig()
{
  carbonInterfaceXtorFreeMsgStream(mErrorStream);
  carbonInterfaceXtorFreeMsgContext(mMsgContext);
}

void CarbonXPcieConfig::setTrainingBypass(bool tf) {
  mTrainingBypass = tf;
  if(mVerboseTransmitterLow || mVerboseReceiverLow)
    printf("%s  Info: Link training bypass %s\n",mPrintName.c_str(), (tf ? "Enabled" : "Disabled"));
}

void CarbonXPcieConfig::setMaxPayloadSize(UInt32 val) {
  // Use encoded values from Spec 1.0a Sec 7.8.4 (Device Control Register)
  // This sets the maximum number of bytes of data allowed to be sent
  // in a single transaction.  The default is value 0x0, 128B, 32DW.
  switch (val & 0x7) {
  case 0x0: mMaxPayloadSizeDW =   32; break;  //  128B
  case 0x1: mMaxPayloadSizeDW =   64; break;  //  256B
  case 0x2: mMaxPayloadSizeDW =  128; break;  //  512B
  case 0x3: mMaxPayloadSizeDW =  256; break;  // 1024B
  case 0x4: mMaxPayloadSizeDW =  512; break;  // 2048B
  case 0x5: mMaxPayloadSizeDW = 1024; break;  // 4096B
  default:                               // Reserved
    carbonInterfaceXtorPCIEInvalidMaxPayloadSize(mMsgContext, mPrintName.c_str(), (val & 0x7));
    break;
  }
  if(mVerboseTransmitterLow || mVerboseReceiverLow)
    printf("%s  Info: Max_Payload_Size parameter set to %d bytes\n", mPrintName.c_str(), (mMaxPayloadSizeDW * 4));
  return;
}

UInt32 CarbonXPcieConfig::getMaxPayloadSize(void) {
  UInt32 val;
  switch (mMaxPayloadSizeDW) {
  case   32: val = 0x0; break;
  case   64: val = 0x1; break;
  case  128: val = 0x2; break;
  case  256: val = 0x3; break;
  case  512: val = 0x4; break;
  case 1024: val = 0x5; break;
  default: val = 0x0; break;
  }
  return val;
}

UInt32 CarbonXPcieConfig::getRCB(void) {
  return mLinkCtrlRCB;
}

void CarbonXPcieConfig::setRCB(UInt32 rcb) {
  if ((rcb == 64) || (rcb == 128)) 
    mLinkCtrlRCB = rcb;
  else
    carbonInterfaceXtorPCIEInvalidRCB(mMsgContext, mPrintName.c_str(), rcb);
}

UInt32 CarbonXPcieConfig::getForceEIdleInactive() {
  return mForceEIdleInactive;
}

void CarbonXPcieConfig::setForceEIdleInactive(UInt32 inactive) {
  mForceEIdleInactive = inactive;
}

void CarbonXPcieConfig::setSupportedSpeeds(UInt32 speeds) {
  //UtIO::cout() << "CarbonXPcieConfig::setSupportedSpeeds: set Supported Data Speeds " << speeds << UtIO::endl;

  if(speeds == 0x1 || speeds == 0x3)
    mSupportedDataRate = speeds << 1;
  else {
    carbonInterfaceXtorPCIEUnsupportedSpeed(mMsgContext, mPrintName.c_str());
  }
}

UInt32 CarbonXPcieConfig::getSupportedSpeeds() {
  return mSupportedDataRate >> 1;
}

void CarbonXPcieConfig::setScrambleEnable(bool tf)
{
  mScrambleEnable = tf;

  if(mVerboseTransmitterLow || mVerboseReceiverLow)
    printf("%s  Info: Scramble/Descramble of packets %s\n",mPrintName.c_str(), (tf ? "Enabled" : "Disabled"));
}

bool CarbonXPcieConfig::getScrambleEnable()
{
  return mScrambleEnable;
}

void CarbonXPcieConfig::setTimescale(CarbonTimescale ts) {
  mCurrTimescale = ts;
  mCurrTimescaleValid = true;

  switch(mCurrTimescale) {
  case e1fs:   strncpy(mTimeFormat,"%0.06f ns", 10); mTimeFactor=1000000; break;
  case e10fs:  strncpy(mTimeFormat,"%0.05f ns", 10); mTimeFactor=100000; break;
  case e100fs: strncpy(mTimeFormat,"%0.04f ns", 10); mTimeFactor=10000; break;
  case e1ps:   strncpy(mTimeFormat,"%0.03f ns", 10); mTimeFactor=1000; break;
  case e10ps:  strncpy(mTimeFormat,"%0.02f ns", 10); mTimeFactor=100; break;
  case e100ps: strncpy(mTimeFormat,"%0.01f ns", 10); mTimeFactor=10; break;
  case e1ns:   strncpy(mTimeFormat,"%0.0f ns", 10);  mTimeFactor=1; break;
  case e10ns:  strncpy(mTimeFormat,"%0.0f ns", 10);  mTimeFactor=0.1; break;
  case e100ns: strncpy(mTimeFormat,"%0.0f ns", 10);  mTimeFactor=0.01; break;
  case e1us:   strncpy(mTimeFormat,"%0.0f ns", 10);  mTimeFactor=0.001; break;
  case e10us:  strncpy(mTimeFormat,"%0.0f ns", 10);  mTimeFactor=0.0001; break;
  case e100us: strncpy(mTimeFormat,"%0.0f ns", 10);  mTimeFactor=0.00001; break;
  case e1ms:   strncpy(mTimeFormat,"%0.0f ns", 10);  mTimeFactor=0.000001; break;
  case e10ms:  strncpy(mTimeFormat,"%0.0f ns", 10);  mTimeFactor=0.0000001; break;
  case e100ms: strncpy(mTimeFormat,"%0.0f ns", 10);  mTimeFactor=0.00000001; break;
  case e1s:    strncpy(mTimeFormat,"%0.0f ns", 10);  mTimeFactor=0.000000001; break;
  case e10s:   strncpy(mTimeFormat,"%0.0f ns", 10);  mTimeFactor=0.0000000001; break;
  case e100s:  strncpy(mTimeFormat,"%0.0f ns", 10);  mTimeFactor=0.00000000001; break;
  default:     strncpy(mTimeFormat,"%0.0f ns", 10);  mTimeFactor=1; break;
  }
   
}

void CarbonXPcieConfig::updateCurrTime(UInt64 tick) {
  mCurrTick = tick;
  
  
  // I'm using sprintf here since I didn't know any other way to do it.
  // I tried using UtIOStringStream, but had problem linking using mingw gcc 3.4.3
  // so I switched to using sprint instead.
  char tmp_str[100];
  if (mCurrTimescaleValid) {
    sprintf(tmp_str, mTimeFormat, static_cast<double>(mCurrTick) / mTimeFactor);
  } else {
    sprintf(tmp_str, Format64(u), mCurrTick);
  }
  mCurrTimeString = tmp_str;
}

void CarbonXPcieConfig::setPrintLevel(UInt32 flags) {
  // Value:   3 = High
  //          2 = Medium
  //          1 = Low
  //          0 = Off

  if (flags > 3)
    flags = 3;
   
  switch (flags) {
  case 0x3:
    verbose(0);
    break;
  case 0x2:
    verbose(0x10);
    break;
  case 0x1:
    verbose(0x18);
    break;
  case 0x0:
    verbose(0x1c);
    break;
  }
}

UInt32 CarbonXPcieConfig::getPrintLevel(void) {
  // Value:   3 = High
  //          2 = Medium
  //          1 = Low
  //          0 = Off

  UInt32 flags;

  if (mVerboseTransmitterHigh)
    flags = 0x3;
  else if (mVerboseTransmitterMedium)
    flags = 0x2;
  else if (mVerboseTransmitterLow)
    flags = 0x1;
  else
    flags = 0x0;

  return flags;
}

void CarbonXPcieConfig::verbose(UInt32 flags)
{
  // Bits are Active LOW
  //  hi me lo tx rx
  //   x  x  x  1  0   = 0x1E = Verbose_Receiver
  //   x  x  x  0  1   = 0x1D = Verbose_Transmitter
  //   x  x  x  0  0   = 0x1C = Verbose_All
  //   1  1  1  x  x   = 0x1F = Verbosity_Off
  //   1  1  0  x  x   = 0x1B = Verbosity_Low
  //   1  0  x  x  x   = 0x17 = Verbosity_Medium
  //   0  x  x  x  x   = 0x0F = Verbosity_High

  if(!(flags & 0x01)) {    // receiver selected
    if(!(flags & 0x10)) {         // High
      mVerboseReceiverHigh = true;
      mVerboseReceiverMedium = true;
      mVerboseReceiverLow = true;
    } else if(!(flags & 0x08)) {  // Medium
      mVerboseReceiverHigh = false;
      mVerboseReceiverMedium = true;
      mVerboseReceiverLow = true;
    } else if(!(flags & 0x04)) {  // Low
      mVerboseReceiverHigh = false;
      mVerboseReceiverMedium = false;
      mVerboseReceiverLow = true;
    } else {                     // Off
      mVerboseReceiverHigh = false;
      mVerboseReceiverMedium = false;
      mVerboseReceiverLow = false;
    }
  }
  if(!(flags & 0x02)) {    // transmitter selected
    if(!(flags & 0x10)) {         // High
      mVerboseTransmitterHigh = true;
      mVerboseTransmitterMedium = true;
      mVerboseTransmitterLow = true;
    } else if(!(flags & 0x08)) {  // Medium
      mVerboseTransmitterHigh = false;
      mVerboseTransmitterMedium = true;
      mVerboseTransmitterLow = true;
    } else if(!(flags & 0x04)) {  // Low
      mVerboseTransmitterHigh = false;
      mVerboseTransmitterMedium = false;
      mVerboseTransmitterLow = true;
    } else {                     // Off
      mVerboseTransmitterHigh = false;
      mVerboseTransmitterMedium = false;
      mVerboseTransmitterLow = false;
    }
  }
}

