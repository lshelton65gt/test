/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "CarbonXPcieScrambler.h"

// transaction special characters
// Define the Data Bytes associated with the Special Character Symbol Codes
#define PCIE_COM	0xBC	// K28.5 // Used for Lane and Link Initialization
#define PCIE_STP	0xFB	// K27.7 // Marks the start of a Transaction Layer Packet
#define PCIE_SDP	0x5C	// K28.2 // Marks the start of a Data Link Layer Packet
#define PCIE_END	0xFD	// K29.7 // Marks the end of a TLP or DLLP
#define PCIE_EDB	0xFE	// K30.7 // Marks the end of a nullified TLP
#define PCIE_PAD	0xF7	// K23.7 // Used in Framing and Link Width and Lane ordering negotiations
#define PCIE_SKP	0x1C	// K28.0 // Used for compensating for different bit rates for two communicating ports
#define PCIE_FTS	0x3C	// K28.1 // Used within an ordered set to exit from L0s to L0 (standby to idle)
#define PCIE_IDL	0x7C	// K28.3 // Symbol used in the Electrical Idle ordered set
#define PCIE_RSVD1	0x9C	// K28.4 // RESERVED
#define PCIE_RSVD2	0xDC	// K28.6 // RESERVED
#define PCIE_RSVD3	0xFC	// K28.7 // RESERVED

// Data Scrambling / Descrambling one byte at a time.  This is taken directly from the
// PCI Express 1.0a spec, Appendix C.
/*
  this routine implements the serial descrabling algorithm in parallel form
  for the LSFR polynomial: x^16+x^5+x^4+x^3+1
  this advances the LSFR 8 bits every time it is called
*/

UInt32 CarbonXPcieScrambler::scramble_descramble_byte(UInt32 inbyte, bool kin, UInt32 laneNum)
{
    static int scrambit[16];   // scrabled/descrabled bits
    static int bit[16];
    static int bit_out[16];
    UInt32 & lfsr = mLfsrLane[laneNum]; // for polynomial
    int i, outbyte;

    for (i=0; i<16; ++i) {
       scrambit[i] = 0;
       bit[i] = 0;
       bit_out[i] = 0;
    }

    if ((inbyte == PCIE_COM) && kin) {    // if this is a comma
      // reset the LFSR
      lfsr = 0xFFFF;
      return(PCIE_COM);        // and return the same data
    }
    if ((inbyte == PCIE_SKP) && kin)      // don't advance or encode/decode on skip
       return(PCIE_SKP);

    for (i=0; i<16; ++i)         // convert LFSR to bit array for legibility
       bit[i] = (lfsr >> i) & 1;

    // apply the xor to the data
    if ( not kin ) { // if not a command byte

       for (i=0; i<8; ++i)          // convert byte to be scrambled/descrambled for legibility
	  scrambit[i] = (inbyte >> i) & 1;

	scrambit[0] ^= bit[15];
	scrambit[1] ^= bit[14];
	scrambit[2] ^= bit[13];
	scrambit[3] ^= bit[12];
	scrambit[4] ^= bit[11];
	scrambit[5] ^= bit[10];
	scrambit[6] ^= bit[9];
	scrambit[7] ^= bit[8];

	outbyte = 0;
	for (i=0; i<8; ++i)    // convert data back to an integer
	   outbyte += (scrambit[i] << i);
    } else {
       outbyte = inbyte;
    }

    // Now advance the LFSR 8 serial clocks
    bit_out[ 0] = bit[ 8];
    bit_out[ 1] = bit[ 9];
    bit_out[ 2] = bit[10];
    bit_out[ 3] = bit[11] ^ bit[ 8];
    bit_out[ 4] = bit[12] ^ bit[ 9] ^ bit[ 8];
    bit_out[ 5] = bit[13] ^ bit[10] ^ bit[ 9] ^ bit[ 8];
    bit_out[ 6] = bit[14] ^ bit[11] ^ bit[10] ^ bit[ 9];
    bit_out[ 7] = bit[15] ^ bit[12] ^ bit[11] ^ bit[10];
    bit_out[ 8] = bit[ 0] ^ bit[13] ^ bit[12] ^ bit[11];
    bit_out[ 9] = bit[ 1] ^ bit[14] ^ bit[13] ^ bit[12];
    bit_out[10] = bit[ 2] ^ bit[15] ^ bit[14] ^ bit[13];
    bit_out[11] = bit[ 3]           ^ bit[15] ^ bit[14];
    bit_out[12] = bit[ 4]                     ^ bit[15];
    bit_out[13] = bit[ 5];
    bit_out[14] = bit[ 6];
    bit_out[15] = bit[ 7];
    lfsr = 0;
    for (i=0; i<16; ++i)   // convert the LFSR back to an integer
	lfsr += (bit_out[i] << i);
    
    // If scrambler is enabled return the result, otherwise return the same data as was put in
    if(mEnable) return outbyte;
    else        return inbyte;
}

void CarbonXPcieScrambler::reset() {
  for(UInt32 i = 0; i < mLinkWidth; i++)
    mLfsrLane[i] = 0xffff;
}

void CarbonXPcieScrambler::enable(bool en) {
  mEnable = en;
}
