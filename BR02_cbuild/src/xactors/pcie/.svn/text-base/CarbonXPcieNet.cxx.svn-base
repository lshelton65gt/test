/***************************************************************************************
  Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "shell/carbon_capi.h"
#include "CarbonXPcieNet.h"
#include "carbonXPcie.h"
#include <cstdio>
#include <cstring>

CarbonXPcieNet::CarbonXPcieNet(CarbonXPcieNet::NetDirection direction)
{
  mBitWidth             = 32;
  mDirection            = direction;
  mState                = 0xffffffff;
  mCBFunc               = 0;
  mCBUserData           = 0;
  mCarbonDepositNet     = NULL;
  mCarbonDepositContext = NULL;
}

CarbonXPcieNet::CarbonXPcieNet(CarbonXPcieNet::NetDirection direction, const char *name) :
  mName(name)
{
  mBitWidth             = 32;
  mDirection            = direction;
  mState                = 0;
  mCBFunc               = 0;
  mCBUserData           = 0;
  mCarbonDepositNet     = NULL;
  mCarbonDepositContext = NULL;
}

const char * CarbonXPcieNet::getName(void)
{
  return mName.c_str();
}
void CarbonXPcieNet::setName(const char * name)
{
  mName = name;
}

UInt32 CarbonXPcieNet::getBitWidth(void)
{
  return mBitWidth;
}
void CarbonXPcieNet::setBitWidth(UInt32 bit_width)
{
  mBitWidth = bit_width;
}

void CarbonXPcieNet::deposit(const UInt32 * state)
{
  if(*state != mState) {
    mState = *state;
    //printf("Deposit: %s. Net = %x\n", mName.c_str(), mState);
    if(mDirection == eOutput) {
      if(mCBFunc) {
	//printf("Calling Change Callback funtion for signal: %s New State = %d\n", mName.c_str(), *state);
	mCBFunc(this, mCBUserData);
      }
      else if(mCarbonDepositNet) {
	//printf("Depositing 0x%x to Carbon Net for signal %s\n", *state, mName.c_str());
	carbonDepositFast(mCarbonDepositContext, mCarbonDepositNet, state, NULL);
      }
    }
  }
}

void CarbonXPcieNet::examine(UInt32 * state)
{
  //printf("Examine: %s. Net = %x\n", mName.c_str(), mState);
  *state = mState;
}

void CarbonXPcieNet::setChangeCBFn(CarbonXPcieNetChangeCBFn *fn, CarbonClientData userData) {
  mCBFunc = fn;
  mCBUserData = userData;
}

bool CarbonXPcieNet::bindCarbonNet(CarbonObjectID* context, CarbonNetID* carbonNet) {

  // Setup Value Change Callback Of the CarbonNet
  if(mDirection == eInput) {

    // Check if the net is constant before trying to setup a callback
    if(!carbonIsConstant(carbonNet)) {
      if(carbonAddNetValueChangeCB(context, carbonValueChangeCB, this, carbonNet) == NULL) {
	return false;
      }
    }

    // Store the initial value
    if(carbonExamine(context, carbonNet, &mState, NULL) != eCarbon_OK )
      return false;
    
  }

  // Setup CarbonNet to deposit too
  else if(mDirection == eOutput && carbonIsDepositable(context, carbonNet) == 1) {
    mCarbonDepositNet     = carbonNet;
    mCarbonDepositContext = context;

    // Deposit Initial value
    carbonDepositFast(mCarbonDepositContext, mCarbonDepositNet, &mState, NULL);
    
  }
  else if(carbonIsDepositable(context, carbonNet) == -1) {
    return false;
  }

  return true;
}

void CarbonXPcieNet::carbonValueChangeCB(CarbonObjectID* cid, CarbonNetID* netid, 
					 CarbonClientData clientData, CarbonUInt32* new_val, CarbonUInt32* new_drive) {
  (void) cid;
  (void) netid;
  (void) new_drive;
  
  CarbonXPcieNet * pcieNet = reinterpret_cast<CarbonXPcieNet *>(clientData);

  //printf("In carbonValueChangeCB: Signal %s, Value = %x\n", pcieNet->mName.c_str(), *new_val);
  pcieNet->mState = *new_val;
}

// C-API Code
void carbonXPcieNetSetValueChangeCB(CarbonXPcieNet *         net,
				    CarbonXPcieNetChangeCBFn fn,
				    CarbonClientData         userData
				    ) {
  net->setChangeCBFn(fn, userData);
}

void carbonXPcieNetDeposit(CarbonXPcieNetID * net, UInt32 value){
  if(net != 0) 
    net->deposit(&value);
}
void carbonXPcieNetExamine(CarbonXPcieNetID * net, UInt32 *value){
  if(net != 0) 
    net->examine(value);
  else
    *value = 0;
}
const char *carbonXPcieNetGetName(CarbonXPcieNetID * net){
  return net->getName();
}

bool carbonXPcieNetBindCarbonNet(CarbonXPcieNetID * pcieNet, CarbonObjectID* context, CarbonNetID* carbonNet) {
  return pcieNet->bindCarbonNet(context, carbonNet);
}
