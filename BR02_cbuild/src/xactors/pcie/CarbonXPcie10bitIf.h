// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __CarbonXPcie10bitIf_H_
#define __CarbonXPcie10bitIf_H_

#include "CarbonXPcieIfBase.h"
#include "carbonXPcie.h"
#include "CarbonXPcieNet.h"
#include "CarbonXPcieScrambler.h"
#include "CarbonX8b10b.h"

class MsgContext;
class CarbonXPcieConfig;
class CarbonXPciePhyRx;

class CarbonXPcie10bitIf : public CarbonXPcieIfBase {
public: CARBONMEM_OVERRIDES;
  
  //! Constructor
  CarbonXPcie10bitIf(CarbonXPcieConfig& cfg, CarbonXPciePhyRx& phyRx);

  //! Destructor
  virtual ~CarbonXPcie10bitIf();

  //! Reset
  void reset();

  //! Step method should be called every cycle
  void step();

  //! Return pointer to the net object for the specified signal
  CarbonXPcieNet * getNet(CarbonXPcieSignalType sigType, UInt32 index);
  
  // Output Data on the interface
  void transmitData(const UInt32 *data, const bool *sym, bool scramble = true);
  void transmitLaneData(UInt32 data, bool sym, UInt32 index, bool scramble = true);
  void transmitIdleData(void);
  void transmitEIdle(bool on_off);

  // Read data from the pin interface
  void receiveData(UInt32 *data, bool *sym);
  bool isEIdle();

  // Paramater Methods
  void setScrambleEnable(bool enable);
  bool getScrambleEnable();
  
private:

  void setTransmitPins(CarbonXPcieNet** netVect, UInt32 index, UInt32 value);
  void setTransmitPins(CarbonXPcieNet* net, UInt32 value);
  void setTransmitLos(UInt32 index, UInt32 value);
  inline void setTransmitData(UInt32 idx, UInt32 val)     { setTransmitPins(mSignalRxData, idx, val); }

  UInt32 getReceivePins(CarbonXPcieNet **netVect, UInt32 index);
  UInt32 getReceivePins(CarbonXPcieNet* net);
  UInt32 getReceiveLos(UInt32 index);
  inline UInt32 getReceiveData(UInt32 idx)         { return getReceivePins(mSignalTxData, idx); }

  const char* getSymStringFromByte(UInt32);
  const char* getStringFromPCIESignalType(CarbonXPcieSignalType sigType);  

  // Construction Parameters
  CarbonXPcieConfig& mCfg;
  CarbonXPciePhyRx&  mPhyRx;

  // Transmit Data Interface Signals (Inputs to transactor)
  CarbonXPcieNet **            mSignalTxData;
  CarbonXPcieNet **            mSignalTxEIdle;

  // Receive Data Interface Signals (Outputs from transactor)
  CarbonXPcieNet **            mSignalRxData;
  CarbonXPcieNet **            mSignalRxEIdle;
  
  CarbonXPcieScrambler         mScrambler;
  CarbonXPcieScrambler         mDeScrambler;
  CarbonX8b10b *               m8b10b;
};

#endif

