/*****************************************************************************

 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "PCIExpressHandler.h"
#include "PCIExtor.h"
#include "carbonsim/CarbonSimMaster.h"
//DEBUG #include <iostream>

PCIExpressHandler::PCIExpressHandler()
   : mSimMaster(0),
     mAreTheyDone(false),
     mUseCarbonSimInterrupt(true),
     mDriveTransactors(true)
{
   mPcieList.clear();
   mInterruptTime.clear();
}

PCIExpressHandler::~PCIExpressHandler() {
   mPcieList.clear();
   mInterruptTime.clear();
}

bool PCIExpressHandler::bindSimMaster(CarbonSimMaster *sim) {
   if (sim) {
      mSimMaster = sim;
      return true;
   } else {
      return false;
   }
}

bool PCIExpressHandler::bindTransactor(PCIEXtor *xtor) {
   if (xtor) {
      mPcieList.push_back(xtor);
      return true;
   } else {
      return false;
   }
}

bool PCIExpressHandler::isDone() {
   return mAreTheyDone;
}

void PCIExpressHandler::setUseCarbonSimInterrupt(bool tf) {
   mUseCarbonSimInterrupt = tf;
}

bool PCIExpressHandler::getUseCarbonSimInterrupt() {
   return mUseCarbonSimInterrupt;
}

bool PCIExpressHandler::addInterruptTime(CarbonTime int_time) {
   if (mSimMaster->getTick() > int_time)
      return false;

   if (mInterruptTime.empty())
      mInterruptTime.push_back(int_time);
   else if (mInterruptTime.back() < int_time)
      mInterruptTime.push_back(int_time);
   else {
      for (CarbonTimeList::iterator i = mInterruptTime.begin(); i != mInterruptTime.end(); i++) {
	 if ((*i) == int_time)
	    break;  // don't insert if its already in the list
	 else if ((*i) > int_time) {
	    mInterruptTime.insert(i, int_time);
	    break;
	 }
      }
   }
   return true;
}

void PCIExpressHandler::setDriveTransactors(bool tf) {
   mDriveTransactors = tf;
}

bool PCIExpressHandler::getDriveTransactors() {
   return mDriveTransactors;
}

void PCIExpressHandler::step(CarbonTime tick) {
   bool allAreDone = true;

   // Unconditional interrupt if we're at a time on the interrupt list
   if (not mInterruptTime.empty() && mInterruptTime.front() <= tick) {
      while (not mInterruptTime.empty() && mInterruptTime.front() <= tick)
	 mInterruptTime.pop_front();
      mSimMaster->interrupt();
      return;
   }

   if (mDriveTransactors) {
      for (PCIEXtorList::const_iterator i = mPcieList.begin(); i != mPcieList.end(); i++) {
	 (*i)->step(tick);
      }
   }

   for (PCIEXtorList::const_iterator i = mPcieList.begin(); i != mPcieList.end(); i++) {
      if (not (*i)->isDone()) {
	 allAreDone = false;
	 //DEBUG std::cout << "--PCIE Handler--  Transactor '" << (*i)->getPrintName() << "' still has work to do at " << std::dec << tick << std::endl;
	 break;
      }
   }
   mAreTheyDone = allAreDone;

   if ((mSimMaster != 0) && mUseCarbonSimInterrupt && allAreDone) {
      mSimMaster->interrupt();
      //DEBUG std::cout << "--PCIE Handler--  Calling CarbonSimMaster::interrupt()" << std::endl;
   }
}

void PCIExpressHandler::idle(UInt64 /* cycles */) {
   // I could set all transactors idle for 'cycles'...
   // But for now, we do nothing.
   ;
}


/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
