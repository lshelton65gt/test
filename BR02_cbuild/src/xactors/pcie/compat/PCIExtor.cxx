/*****************************************************************************

 Copyright (c) 2003-2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#include "PCIExtor.h"
#include "carbonsim/CarbonSimWrappedNet.h"
#include "carbonsim/CarbonSimClock.h"
#include "PCIExpressHandler.h"
#include <cstdio>

PCIECmd::PCIECmd() {
   mTLP = carbonXPcieTLPCreate();
}

PCIECmd::PCIECmd(const PCIECmd &other) {
   mTLP     = carbonXPcieTLPCreate();
   carbonXPcieTLPCopy(mTLP, other.mTLP);
}

//! Destructor.
PCIECmd::~PCIECmd() {
   carbonXPcieTLPDestroy(mTLP);
}

const char* PCIECmd::getStrFromType (PCIECmdType type) {
   return carbonXPcieTLPGetStrFromType((CarbonXPcieCmdType)type);
}

void   PCIECmd::copy(const PCIECmd& orig) {
   carbonXPcieTLPCopy(mTLP, orig.mTLP);
}
 
void   PCIECmd::copy         (const CarbonXPcieTLP& orig) {
   carbonXPcieTLPCopy(mTLP, &orig);
}

void   PCIECmd::setCmd       (UInt32 cmd) {
   carbonXPcieTLPSetCmd(mTLP, cmd);
}

UInt32 PCIECmd::getCmd() {
   return carbonXPcieTLPGetCmd(mTLP);
}

void PCIECmd::setCmdByName(PCIECmd::PCIECmdType cmd) {
   carbonXPcieTLPSetCmdByName(mTLP, (CarbonXPcieCmdType)cmd);
}

PCIECmd::PCIECmdType PCIECmd::getCmdByName() {
   return (PCIECmd::PCIECmdType)carbonXPcieTLPGetCmdByName(mTLP);
}

void PCIECmd::setAddr(UInt64 addr) {
   carbonXPcieTLPSetAddr(mTLP, addr);
}

UInt64 PCIECmd::getAddr() {
   return carbonXPcieTLPGetAddr(mTLP);
}


void PCIECmd::setReqId(UInt32 id) {
   carbonXPcieTLPSetReqId(mTLP, id);
}

UInt32 PCIECmd::getReqId() {
   return carbonXPcieTLPGetReqId(mTLP);
}

void PCIECmd::setTag(UInt32 tag) {
   carbonXPcieTLPSetTag(mTLP, tag);
}

UInt32 PCIECmd::getTag() {
   return carbonXPcieTLPGetTag(mTLP);
}

void PCIECmd::setRegNum(UInt32 reg) {
   carbonXPcieTLPSetRegNum(mTLP, reg);
}

UInt32 PCIECmd::getRegNum() {
   return carbonXPcieTLPGetRegNum(mTLP);
}

void PCIECmd::setCompId(UInt32 id) {
   carbonXPcieTLPSetCompId(mTLP, id);
}

UInt32 PCIECmd::getCompId() {
   return carbonXPcieTLPGetCompId(mTLP);
}

void PCIECmd::setMsgRout(UInt32 rout) {
   carbonXPcieTLPSetMsgRout(mTLP, rout);
}

UInt32 PCIECmd::getMsgRout() {
   return carbonXPcieTLPGetMsgRout(mTLP);
}

void PCIECmd::setMsgCode(UInt32 code) {
   carbonXPcieTLPSetMsgCode(mTLP, code);
}

UInt32 PCIECmd::getMsgCode() {
   return carbonXPcieTLPGetMsgCode(mTLP);
}

void PCIECmd::setLowAddr(UInt32 addr) {
   carbonXPcieTLPSetLowAddr(mTLP, addr);
}

UInt32 PCIECmd::getLowAddr() {
   return carbonXPcieTLPGetLowAddr(mTLP);
}

void PCIECmd::setByteCount(UInt32 count) {
   carbonXPcieTLPSetByteCount(mTLP, count);
}

UInt32 PCIECmd::getByteCount() {
   return carbonXPcieTLPGetByteCount(mTLP);
}

UInt32 getDWCount   (void);

void PCIECmd::setFirstBE(UInt32 be) {
   carbonXPcieTLPSetFirstBE(mTLP, be);
}

UInt32 PCIECmd::getFirstBE() {
   return carbonXPcieTLPGetFirstBE(mTLP);
}

void PCIECmd::setLastBE(UInt32 be) {
   carbonXPcieTLPSetLastBE(mTLP, be);
}

UInt32 PCIECmd::getLastBE() {
   return carbonXPcieTLPGetLastBE(mTLP);
}

void PCIECmd::setCplStatus(UInt32 status) {
   carbonXPcieTLPSetCplStatus(mTLP, status);
}

UInt32 PCIECmd::getCplStatus() {
   return carbonXPcieTLPGetCplStatus(mTLP);
}

void PCIECmd::setDataByte(UInt32 index, UInt8 data) {
   carbonXPcieTLPSetDataByte(mTLP, index, data);
}

UInt8 PCIECmd::getDataByte(UInt32 index) {
   return carbonXPcieTLPGetDataByte(mTLP, index);
}

void PCIECmd::resizeDataBytes(UInt32 start, UInt32 end) {
   carbonXPcieTLPResizeDataBytes(mTLP, start, end);
}

UInt32 PCIECmd::getDataByteCount(void) {
   return carbonXPcieTLPGetDataByteCount(mTLP);
}

void PCIECmd::setDataLength(UInt32 len) {
   carbonXPcieTLPSetDataLength(mTLP, len);
}

UInt32 PCIECmd::getDataLength(void) {
   return carbonXPcieTLPGetDataLength(mTLP);
}

SInt32 PCIECmd::getTransId() {
   return carbonXPcieTLPGetTransId(mTLP);
}

bool PCIECmd::isRead() {
   return carbonXPcieTLPIsRead(mTLP);
}

bool PCIECmd::isWrite() {
   return carbonXPcieTLPIsWrite(mTLP);
  }

const CarbonXPcieTLP *PCIECmd::getTLP() {
   return mTLP;
}

//#################################

PCIEHandler::PCIEHandler()
   : pimpl_(new PCIExpressHandler()) {
}

PCIEHandler::~PCIEHandler() {
   delete pimpl_;
}

bool PCIEHandler::bindSimMaster(CarbonSimMaster *sim) {
   return pimpl_->bindSimMaster(sim);
}

bool PCIEHandler::bindTransactor(PCIEXtor *xtor) {
   return pimpl_->bindTransactor(xtor);
}

bool PCIEHandler::isDone() {
   return pimpl_->isDone();
}

void PCIEHandler::setUseCarbonSimInterrupt(bool tf) {
   pimpl_->setUseCarbonSimInterrupt(tf);
}

bool PCIEHandler::getUseCarbonSimInterrupt() {
   return pimpl_->getUseCarbonSimInterrupt();
}

bool PCIEHandler::addInterruptTime(CarbonTime int_time) {
   return pimpl_->addInterruptTime(int_time);
}

void PCIEHandler::setDriveTransactors(bool tf) {
   pimpl_->setDriveTransactors(tf);
}

bool PCIEHandler::getDriveTransactors() {
   return pimpl_->getDriveTransactors();
}

void PCIEHandler::step(CarbonTime tick) {
   pimpl_->step(tick);
}

void PCIEHandler::idle(UInt64 cycles) {
   pimpl_->idle(cycles);
}

//#################################

PCIEXtor::~PCIEXtor() {
   carbonXPcieDestroy(pimpl_);
}

PCIEXtor::PCIEXtor(PCIEXtor::PCIEIntfType intf, UInt32 link_width, const char *print_name) {
   pimpl_ = carbonXPcieCreate((CarbonXPcieIntfType)intf, link_width, print_name);
}
//! Get c-style string for the PCIEXtor::PCIEIntfType value.
/*!
  This function is provided to enhance printing messages.
  \param type The type to get the print string for.
  \return The c-style string for message printing.
*/
const char* PCIEXtor::getStrFromType (PCIEXtor::PCIEIntfType type) {
   switch (type) {
   case e10BitIntf: return "e10BitIntf";
   case ePipeIntf:  return "ePipeIntf";
   default:                   return "Invalid Type";
   }
}
//! Get c-style string for the PCIEXtor::PCIECallbackType value.
/*!
  This function is provided to enhance printing messages.
  \param type The type to get the print string for.
  \return The c-style string for message printing.
*/
const char* PCIEXtor::getStrFromType (PCIEXtor::PCIECallbackType type) {
   switch (type) {
   case eReturnData:    return "eReturnData";
   case eReceivedTrans: return "eReceivedTrans";
   default:                       return "Invalid Type";
   }
}
//! Get c-style string for the PCIEXtor::PCIESignalType value.
/*!
  This function is provided to enhance printing messages.
  \param type The type to get the print string for.
  \return The c-style string for message printing.
*/
const char* PCIEXtor::getStrFromType (PCIEXtor::PCIESignalType type) {
   switch (type) {
   case eClock:        return "eClock";
   case eRxData:       return "eRxData";
   case eRxElecIdle:   return "eRxElecIdle";
   case eTxData:       return "eTxData";
   case eTxElecIdle:   return "eTxElecIdle";
   case eRxDataK:      return "eRxDataK";
   case eRxValid:      return "eRxValid";
   case eRxStatus:     return "eRxStatus";
   case eTxDataK:      return "eTxDataK";
   case eTxCompliance: return "eTxCompliance";
   case eRxPolarity:   return "eRxPolarity";
   default:                      return "Invalid Type";
   }
}
//! Set message printing level for transactor.
/*!
  The available levels are:
  - 3+ : High, print copious amounts of data about every cycle.
  - 2  : Medium, print large amount of information during all transactions.
  - 1  : Low, print basic informational messages during simulations.
  - 0  : Off, print no message except for a few configuration error messages.

  The default value is '1' when the transactor is instanciated.

  \param level The print level to be used.
*/
void   PCIEXtor::setPrintLevel      (UInt32 level) {
   carbonXPcieSetPrintLevel(pimpl_, level);
}
//! Get message printing level for transactor.
/*!
  \retval 3 If the message printing is set to High.
  \retval 2 If the message printing is set to Medium.
  \retval 1 If the message printing is set to Low.
  \retval 0 If the message printing is set to Off.
*/
UInt32 PCIEXtor::getPrintLevel      (void) {
   return carbonXPcieGetPrintLevel(pimpl_);
}
void   PCIEXtor::setBusNum          (UInt32 num) {	                 // Set PCI style Bus Number for config transactions
   carbonXPcieSetBusNum(pimpl_, num);
}
UInt32 PCIEXtor::getBusNum          (void) {                             // Get PCI style Bus Number for config transactions
   return carbonXPcieGetBusNum(pimpl_);
}
void   PCIEXtor::setDevNum          (UInt32 num) {                       // Set PCI style Device Number for config transactions
   carbonXPcieSetDevNum(pimpl_, num);
}
UInt32 PCIEXtor::getDevNum          (void) {                             // Get PCI style Device Number for config transactions
   return carbonXPcieGetDevNum(pimpl_);
}
void   PCIEXtor::setFuncNum         (UInt32 num) {                       // Set PCI style Function Number for config transactions
   carbonXPcieSetFuncNum(pimpl_, num);
}
UInt32 PCIEXtor::getFuncNum         (void) {                             // Get PCI style Function Number for config transactions
   return carbonXPcieGetFuncNum(pimpl_);
}
//! Sets whether data scrambling is enabled or disabled.
/*!
  This option will effect both the transmitter and receiver functionality, ie. they must
  both or neither be sending scrambled data across the link.  This function should be called
  before the transactor starts training the link, and if it is called during the simulation
  then the transactor will need to retrain the link before it takes effect.

  \bug
  This function should be set before the simulation starts and not changed.  The
  transactor will immediately use this status and if changed during a simulation the DUT
  will most likely not be able to handle this change, since it is not allowed in the
  PCI-Express specification.

  \bug
  The only valid value is FALSE.  The scrambling functionality does not work properly and
  should be disabled.

  \param tf The value TRUE will enable data scrambling while FALSE will disable it.
*/
void   PCIEXtor::setScrambleStatus  (bool tf) {
   carbonXPcieSetScrambleStatus(pimpl_, tf);
}
//! Gets whether data scrambling is enabled or disabled.
/*!
  This function will return the latest value set with setScrambleStatus().
  \retval true If data scrambling is enabled.
  \retval false If data scrambling is disabled.
*/
bool   PCIEXtor::getScrambleStatus  (void) {
   return carbonXPcieGetScrambleStatus(pimpl_);
}
//! Sets the maximum number of in-flight transactions
/*!
  This option will set the maximum number of transactions from the transactor that can be
  in-flight at one time.  In-flight means that it is activly being transmitted, waiting for
  an acknowledgement, or waiting for a completion message.

  Once this limit is reached, all transactions will be held in the queue and idle will be
  transmitted on the bus until one or more of the in-flight transactions completes.

  The PCI-Express 1.0a specification states that the maximum number of in-flight transactions
  is 32.  If this function sets a value of 0 or more than 32, the transactor will respect the
  limit of 32 in the specification.  Any value between those two points will be used as is.

  \param max The maximum number of in-flight transactions.
*/
void   PCIEXtor::setMaxTransInFlight (UInt32 max) {
   carbonXPcieSetMaxTransInFlight(pimpl_, max);
}
//! Gets the maximum number of in-flight transactions
/*!
  This option will return the maximum number of in-flight transactions that is allowed by
  the transactor.  This value is set by setMaxTransInFlight() and the default value is 32.

  \return The maximum number of in-flight transactions.
*/
UInt32 PCIEXtor::getMaxTransInFlight (void) {
   return carbonXPcieGetMaxTransInFlight(pimpl_);
}
//! Start link training
/*!
  Start training the link immediately if the link is currently in the electrical idle state.
  If the link is not in electrical idle then this function will have no effect.
  \retval true If the transactor will start link training.
  \retval false If the transactor will not start link training.
*/
bool   PCIEXtor::startTraining      (void) {
   return carbonXPcieStartTraining(pimpl_, 0);
}
//! Start link training
/*!
  Start training the link after time \a tick if the link is currently in the electrical
  idle state.  If the link is not in electrical idle then this function will have no effect.
  The link will immediately transition from electrical idle to logical idle, stay in that
  state for time \a tick, and then start training.

  The value \a tick is in the same unit used for the CarbonSimClock objects.

  \param tick The length of time to spend in logical idle before training starts.
  \retval true If the transactor will start link training.
  \retval false If the transactor will not start link training.
*/
bool   PCIEXtor::startTraining      (CarbonTime tick) {
   return carbonXPcieStartTraining(pimpl_, tick);
}
//! Enable or disable link training bypass
/*!
  Enabling the link training bypass will cause the transactor to go from an electrical
  idle to logical idle state with all lanes of the link trained.  The transactor will
  go into the flow control initialization state next.

  Disabling the link training bypass will cause the transactor to go through the full
  link training sequence when it is next necessary, upon transitioning either from the
  electrical idle state or to the recovery state.

  By default the training bypass is disabled.

  \param tf The value TRUE will bypass the link training while FALSE will disable the bypass.
*/
void   PCIEXtor::setTrainingBypass  (bool tf) {
   carbonXPcieSetTrainingBypass(pimpl_, tf);
}
//! Get the current state of the training bypass feature.
/*!
  Returns whether the transactor has the link training bypass enabled or disabled.

  \retval true If the link training is bypassed.
  \retval false If the link training is not bypassed.
*/
bool   PCIEXtor::getTrainingBypass  (void) {
   return carbonXPcieGetTrainingBypass(pimpl_);
}
//! Sets the maximum payload size for transmitted transactions
/*!
  Sets the Max_Payload_Size parameter in the PCI-Express Device
  Control Register (Offset 0x8).  See the PCI-Express Base
  Specification Rev 1.0a, Section 7.8.4 for a description of this
  configuration register.

  \param val The 3-bit value to be set.
*/
void   PCIEXtor::setMaxPayloadSize(UInt32 val) {
   carbonXPcieSetMaxPayloadSize(pimpl_, val);
}
//! Gets the maximum payload size for transmitted transactions
/*!
  Gets the Max_Payload_Size parameter in the PCI-Express Device
  Control Register (Offset 0x8).  See the PCI-Express Base
  Specification Rev 1.0a, Section 7.8.4 for a description of this
  configuration register.

  \return The 3-bit parameter value.
*/
UInt32 PCIEXtor::getMaxPayloadSize(void) {
   return carbonXPcieGetMaxPayloadSize(pimpl_);
}
//! Sets the callback function for received transactions and return data
/*!
  This function must be set so the transactor can notify the calling program when it
  receives a transaction from the DUT.  The \a transCbInfo parameter is passed to the
  callback exactly as it was passed to this function.

  \param transCbFn The function to be called by the transactor.
  \param transCbInfo Any information the user would like passed back to the callback function.
*/
void   PCIEXtor::setTransCallbackFn (PCIEXtor::PCIETransCBFn *transCbFn, void* transCbInfo) {
   mTransCbInfo.xtor = this;
   mTransCbInfo.fn   = transCbFn;
   mTransCbInfo.info = transCbInfo;
   carbonXPcieSetTransCallbackFn(pimpl_, transCallBack, &mTransCbInfo);
}
   
// Adapter for the callback function
void PCIEXtor::transCallBack(CarbonXPcieID *pcie, CarbonXPcieCallbackType type, void *info) {
   TransCallbackInfo *tc_info = reinterpret_cast<TransCallbackInfo *>(info);
   tc_info->fn(tc_info->xtor, (PCIECallbackType)type, tc_info->info);
}

//! Returns the callback function pointer called for received transactions and return data
/*!
  This function passes back the function pointer that was passed in as the first parameter
  to the setTransCallbackFn() function.

  \sa setTransCallbackFn(PCIETransCBFn *transCbFn, void* transCbInfo)
*/
PCIEXtor::PCIETransCBFn* PCIEXtor::getTransCallbackFn (void) {
   return mTransCbInfo.fn;
}
//! Returns the callback data pointer passed to the callback function for received transactions and return data
/*!
  This function passes back the data pointer that was passed in as the second parameter
  to the setTransCallbackFn() function.

  \sa setTransCallbackFn(PCIETransCBFn *transCbFn, void* transCbInfo)
*/
void*  PCIEXtor::getTransCallbackData (void) {
   return carbonXPcieGetTransCallbackData(pimpl_);
}
//! Gets a data return transaction in a PCIECmd object.
/*!
  The user must pass in a pointer to an allocated PCIECmd object in which the transactor
  will populate the fields with the data from the received completion transaction.  If
  \a cmd is a null pointer that will be caught and an error message given.

  This command can be called as many times as necessary in order to get each of the stored
  PCIECmd objects.

  \param cmd A pointer to an allocated PCIECmd object.
  \retval true A valid transaction was placed into \a cmd.
  \retval false An error occurred or there are no new completion transactions to be read.
*/
bool	  PCIEXtor::getReturnData      (PCIECmd *cmd) {
   CarbonXPcieTLP *tlp = carbonXPcieTLPCreate();
   bool result = carbonXPcieGetReturnData(pimpl_, tlp);
   if(result) cmd->copy(*tlp);
   carbonXPcieTLPDestroy(tlp);
   return result;
}
//! Gets the number of data return transactions which need to be processed.
/*!
  Find the number of PCIECmd objects in the queue of data return transactions (completions).

  \retval length Number of transactions in queue
*/
UInt32  PCIEXtor::getReturnDataQueueLength (void) {
   return carbonXPcieGetReturnDataQueueLength(pimpl_);
}
//! Gets a received transaction in a PCIECmd object.
/*!
  The user must pass in a pointer to an allocated PCIECmd object in which the transactor
  will populate the fields with the data from the received completion transaction.  If
  \a cmd is a null pointer that will be caught and an error message given.

  This command can be called as many times as necessary in order to get each of the stored
  PCIECmd objects.

  \param cmd A pointer to an allocated PCIECmd object.
  \retval true A valid transaction was placed into \a cmd.
  \retval false An error occurred or there are no new completion transactions to be read.
*/
bool   PCIEXtor::getReceivedTrans   (PCIECmd *cmd) {                     // Get PCIECmd object with received transaction information
   CarbonXPcieTLP *tlp = carbonXPcieTLPCreate();
   bool result = carbonXPcieGetReceivedTrans(pimpl_, tlp);
   if(result) cmd->copy(*tlp);
   carbonXPcieTLPDestroy(tlp);
   return result;
}
//! Gets the number of received transactions which need to be processed.
/*!
  Find the number of PCIECmd objects in the queue of received transactions.

  \retval length Number of transactions in queue
*/
UInt32  PCIEXtor::getReceivedTransQueueLength (void) {
   return carbonXPcieGetReceivedTransQueueLength(pimpl_);
}

//! Gets the transactor's link width
/*!
  This function gets the link width, or number of lanes in the link, as specified when the
  transactor was instantiated.  This value can not change after the PCIEXtor object is
  created.

  \return The link width used by the transactor.
*/
UInt32 PCIEXtor::getLinkWidth       (void) {
   return carbonXPcieGetLinkWidth(pimpl_);
}
//! Gets the transactor's message print name.
/*!
  This function gets the transactor's print name as specified when the transactor was
  instantiated.  This value can not change after the PCIEXtor object is created and is
  prepended to all messages printed from the transactor.

  \return The c-style string used in all printing messages.
*/
const char*  PCIEXtor::getPrintName       (void) {
   return carbonXPcieGetPrintName(pimpl_);
}
//! Gets the transactor's interface type.
/*!
  This function gets the transactor's interface type as specified when the transactor was
  instantiated.  This value can not change after the PCIEXtor object is created.

  \return The \a PCIEIntfType interface type used by this transactor instance.
*/
PCIEXtor::PCIEIntfType PCIEXtor::getInterfaceType (void) {
   return (PCIEXtor::PCIEIntfType)carbonXPcieGetInterfaceType(pimpl_);
}
//! Gets whether the transactor is currently finished processing transactions.
/*!
  \deprecated
  This function has been replaced by isDone() and may be removed at any time.

  \retval true The transactor is done processing its transactions.
  \retval false The transactor has transactions to send out or is currently receiving
  a new transaction.
  \sa isDone()
*/
bool   PCIEXtor::getDoneForNow      (void) {
   return carbonXPcieIsDone(pimpl_);
}
//! Sets the callback function for notifying that the transactor is finished.
/*!
  This function may be set so the transactor can notify the calling program when it
  no longer has any transactions to process, either on the transmitter or receiver.
  The \a doneCbInfo parameter is passed to the callback exactly as it was passed to
  this function.

  This function will be called each time the transactor is called (rising edge of the
  signal connected with type \a eClock) as long as isDone() would return \a true.

  \param doneCbFn The function to be called by the transactor.
  \param doneCbInfo Any information the user would like passed back to the callback function.
*/
void   PCIEXtor::setDoneCallbackFn  (PCIEXtor::PCIEDoneCBFn *doneCbFn, void* doneCbInfo) {
   carbonXPcieSetDoneCallbackFn(pimpl_, doneCbFn, doneCbInfo);
}
//! Returns the callback function pointer called for notification that the transactor is finished
/*!
  This function passes back the function pointer that was passed in as the first parameter
  to the setDonCallbackFn() function.

  \sa setDoneCallbackFn(PCIEDoneCBFn *doneCbFn, void* doneCbInfo)
*/
PCIEXtor::PCIEDoneCBFn* PCIEXtor::getDoneCallbackFn (void) {
   return carbonXPcieGetDoneCallbackFn(pimpl_);
}
//! Returns the callback data pointer passed to the callback function for notification that the transactor is finished
/*!
  This function passes back the data pointer that was passed in as the second parameter
  to the setDoneCallbackFn() function.

  \sa setDoneCallbackFn(PCIEDoneCBFn *doneCbFn, void* doneCbInfo)
*/
void*  PCIEXtor::getDoneCallbackData (void) {
   return carbonXPcieGetDoneCallbackData(pimpl_);
}

//! Enqueue a new transaction
/*!
  Information from the \a cmd object is used to create a new transaction, which is pushed
  onto the end of the queue.  The transation ID is returned upon success and is a unique
  identifier for this transaction.

  \param cmd The information for a new transaction.
  \retval -1 There was an error and no new transaction was added.
  \retval 0+ The new transaction was successfully enqueued and the transaction ID was returned.
*/
SInt32 PCIEXtor::addTransaction     (PCIECmd *cmd) {
   return carbonXPcieAddTransaction(pimpl_, cmd->getTLP());
}
//! Binds a CarbonSimNet to the specified transactor signal
/*!
  One of the transactor's signals will be bound to this object and remove the binding
  of any previous CarbonSimNets set to this signal.  If this function fails then the
  previous binding, if any, will remain intact.

  This function should be used to connect signals that are indexed per lane such that
  there is one of this signal type for each lane in the link, such as with the \a eRxData
  type.  Also, if the index is 0, then all of the non-indexed signals may be set with
  this function.

  \warning The CarbonSimNet object set with type \a eClock must be a net that is driven
  by the same CarbonSimClock that the transactor is bound to with
  CarbonSimClock::bindTransactor().  If this is not followed then the functionality is
  undefined.

  \param sigType The PCIESignalType this net represents.
  \param index The lane index for the specified PCIESignalType.
  \param net The CarbonSimNet to be connected.
  \retval true The signal was successfully set.
  \retval false There was an error setting the signal.

  \sa setSignal(CarbonSimClock *clock)
  \sa setSignal(PCIESignalType sigType, CarbonSimNet *net)
*/
bool   PCIEXtor::setSignal          (PCIEXtor::PCIESignalType sigType, UInt32 index, CarbonSimNet *net) {
   CarbonSimWrappedNet *wr_net = net->castWrappedNet();
   if(wr_net == 0) {
      printf("%s  ERROR: setSignal was called with an invalid CarbonSim net pointer.\n", carbonXPcieGetPrintName(pimpl_));
      return false;
   }
   
   if(sigType == PCIEXtor::eClock) {
      if(index != 0) {
	 printf("%s  ERROR: Signal type %s can only have one value, setting index %d is invalid.\n", carbonXPcieGetPrintName(pimpl_), getStrFromType(sigType), index);
	 return false;
      }
      else {
	 return setSignal(sigType, net);
      }
   }
   
   else {
      CarbonXPcieNet *pcie_net = carbonXPcieGetNet(pimpl_, (CarbonXPcieSignalType)sigType, index);
      if(pcie_net == 0) return false;
      
      return carbonXPcieNetBindCarbonNet(pcie_net, wr_net->getCarbonObject(), wr_net->getCarbonNet());
   }
}

//! Binds a CarbonSimNet to the specified transactor signal
/*!
  One of the transactor's signals will be bound to this object and remove the binding
  of any previous CarbonSimNets set to this signal.  If this function fails then the
  previous binding, if any, will remain intact.

  This function should be used to connect signals that are not indexed per lane such that
  there is only one of this signal type for the entire link, such as with the \a eClock
  type.  Also, if the link width is 1, then all of the indexed signals may be set with
  this function.

  \warning The CarbonSimNet object set with type \a eClock must be a net that is driven
  by the same CarbonSimClock that the transactor is bound to with
  CarbonSimClock::bindTransactor().  If this is not followed then the functionality is
  undefined.

  \param sigType The PCIESignalType this net represents.
  \param net The CarbonSimNet to be connected.
  \retval true The signal was successfully set.
  \retval false There was an error setting the signal.

  \sa setSignal(CarbonSimClock *clock)
  \sa setSignal(PCIESignalType sigType, UInt32 index, CarbonSimNet *net)
*/
bool   PCIEXtor::setSignal          (PCIEXtor::PCIESignalType sigType, CarbonSimNet *net) {
   
   bool status = true;

   if(sigType == PCIEXtor::eClock) {
      if (net->isScalar()) {
	 mSignalClk_SimNet = net;
	 mSignalClk_SimClock = 0;  // make sure other pointer is clear
      } else {
	 printf("%s  ERROR: Invalid CarbonSimNet sent to setSignal(%s, <net>), the CarbonSimNet must be a scalar signal.\n     Net '%s' has width of %d.\n", carbonXPcieGetPrintName(pimpl_), getStrFromType(sigType), net->getName(), net->getBitWidth());
	 status = false;
      }
   }
   else status = setSignal(sigType, 0, net);

   return status;
}
//! Binds a CarbonSimClock to the transactor's clock signal
/*!
  The transactors clock signal will be bound to this object and remove the binding
  of any previous signals set to the clock either with this function or the other
  overloaded functions with signal type \a eClock.  If this function fails then the
  previous binding, if any, will remain intact.

  \warning The CarbonSimClock object must be the same one that the transactor is
  bound to with CarbonSimClock::bindTransactor().  If this is not followed then the
  functionality is undefined.

  \param clock The CarbonSimClock to be used for the clock.
  \retval true The signal was successfully set.
  \retval false There was an error setting the signal.

  \sa setSignal(PCIESignalType sigType, CarbonSimNet *net)
  \sa setSignal(PCIESignalType sigType, UInt32 index, CarbonSimNet *net)
*/
bool   PCIEXtor::setSignal          (CarbonSimClock *clock) {            // Set CarbonSimClock for transactor's clock
   if (clock == 0) {
      printf("%s  ERROR: Invalid CarbonSimClock* passed to setSignal(CarbonSimClock *<net>).\n", carbonXPcieGetPrintName(pimpl_));
      return false;
   }

   mSignalClk_SimClock = clock;
   mSignalClk_SimNet = 0;  // make sure other pointer is clear
   return true;
}

//!Sets the electrical idle nets as active high
/*!
  This function determins whether the signals connected as \a eRxElecIdle and
  \a eTxElecIdle are active high or active low.  Both the transmitter and receiver
  will adhere to the calling of this function.  After this function is called, the
  specified signals will show that the receivers/transmitters are in the electrical
  idle state (powered down) if they have the value '1'.

  \note This is only valid for the \a e10BitIntf interface.  The \a ePipeIntf
  specifies that the signals be active high.
*/
void   PCIEXtor::setElecIdleActiveHigh (void) {
   carbonXPcieSetElecIdleActiveHigh(pimpl_);
}
//!Sets the electrical idle nets as active low
/*!
  This function determins whether the signals connected as \a eRxElecIdle and
  \a eTxElecIdle are active high or active low.  Both the transmitter and receiver
  will adhere to the calling of this function.  After this function is called, the
  specified signals will show that the receivers/transmitters are in the electrical
  idle state (powered down) if they have the value '0'.

  \note This is only valid for the \a e10BitIntf interface.  The \a ePipeIntf
  specifies that the signals be active high.
*/
void   PCIEXtor::setElecIdleActiveLow  (void) {
   carbonXPcieSetElecIdleActiveLow(pimpl_);
}
//! Gets whether the transactor fully configured and ready for use.
/*!
  This function should be called after all of the necessary configuration
  functions have been called to determine if the transactor is ready to
  interact with the DUT.  If this function returns \c true then the transactor
  will function when its CarbonSimClock transitions.  If this function
  returns \c false then the transactor will not execute when its clock
  transitions and will print a warning message to the screen.

  To determine what caused this function to return \c false, call
  printConfiguration().

  \retval true The transactor is fully configured.
  \retval false The transactor has one or more necessary connections open.

  \sa printConfiguration()
*/
bool   PCIEXtor::isConfigured       (void) {
   return true;
}
//! Print out all configuration information.
/*!
  This function can be called at any time to view the current configuration
  information.  A list of all DUT nets connected to transactor signals, the
  link width, etc., will be printed to STDOUT.

  For programability, the function isConfigured() is provided to return
  whether the transactor is fully configured or not.

  \sa isConfigured()
*/
void   PCIEXtor::printConfiguration (void) {
   carbonXPciePrintConfiguration(pimpl_);
}
//! Gets whether the transactor is currently finished processing transactions.
/*!
  \retval true The transactor is done processing its transactions.
  \retval false The transactor has transactions to send out or is currently receiving
  a new transaction.
*/
bool   PCIEXtor::isDone             (void) {                             // Is the transactor currently done doing work
   return carbonXPcieIsDone(pimpl_);
}
//! Determine if the link is in the active state.
/*!
  This function will return whether the link has been trained and is ready for use
  or not.  A return value of \c true means that the link training state machine (LTSM)
  and data link layer state machine (DLLSM) have both completed training.

  \retval true The link is trained.
  \retval false The link has not been fully trained.

  \sa printLinkState()
*/
bool   PCIEXtor::isLinkActive       (void) {
   return carbonXPcieIsLinkActive(pimpl_);
}
//! Print the current state of link training.
/*!
  This function can be called at any time to view the current state of the link
  training while debugging.  The output will be printed to STDOUT for the current
  state of the link training state machine (LTSM) and data link layer state machine
  (DLLSM).

  For programability, the function isLinkActive() is provided to return whether the
  link is fully trained or not.

  \sa isLinkActive()
*/
void   PCIEXtor::printLinkState     (void) {
   carbonXPciePrintLinkState(pimpl_);
}
//! Set the timescale for print messages.
/*!
  This function relates the CarbonTime passed to the step() function to a
  user-specified CarbonTimescale.  For consistency this should match the
  CarbonTimescale value passed to the CarbonWaveID object if one exists. If this
  function is not called then the CarbonTime will be printed in the messages
  without any related scale.

  Since the PCI-Express 1.0a interface is controlled by a clock with a 4ns period,
  messages will be printed with the time scaled from the \a timescale specified
  here to \a e1ns.

  \param timescale The user-specified timescale to be used when printing the
  simulation time.
*/
void   PCIEXtor::setTimescale       (CarbonTimescale timescale) {
   carbonXPcieSetTimescale(pimpl_, timescale);
}
//! Tell the transactor to run.
/*!
  This function needs to be called when the pertinent clock changes value, at which
  time the transctor will process its stimulus and return any results.  If this
  transctor is used within a CarbonSim environment then this function will be called
  automatically when the transactor is bound to a CarbonSimClock.

  \param tick The current simulation time.
*/
void   PCIEXtor::step       (CarbonTime tick) {
   if(getClkValue() != 0) carbonXPcieStep(pimpl_, tick);
}
//! Add an idle transaction.
/*!
  Submit a logical idle transaction that will last \c cycles clock cycles.  This
  transaction will be appended to the transaction queue and can be used to space
  out transactions or keep the transactor running while waiting for the DUT (and 
  not have isDone() return true or related callback called).

  \param cycles The number of clock cycles to keep the transmitter in logical idle.
*/
void   PCIEXtor::idle       (UInt64 cycles) {
   carbonXPcieIdle(pimpl_, cycles);
}

UInt32 PCIEXtor::getClkValue(void) {
   UInt32 val;
   if (mSignalClk_SimClock)
      val = mSignalClk_SimClock->getValue();
   else
      mSignalClk_SimNet->examine(&val);
   return(val);
}

