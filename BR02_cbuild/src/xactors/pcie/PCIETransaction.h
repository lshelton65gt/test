// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2003-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/


#ifndef __PCIEXPRESSTRANSACTION_H_
#define __PCIEXPRESSTRANSACTION_H_

#include "util/c_memmanager.h"
#include "CarbonXPcieDLLP.h"
#include "shell/carbon_interface.h"
#include "CarbonXArray.h"

#define PCIE_DLLP_CRC_KEY  0x100B
#define PCIE_DLLP_CRC_SEED 0xFFFF
#define PCIE_DLLP_LCRC_KEY  0x04C11DB7
#define PCIE_DLLP_LCRC_SEED 0xFFFFFFFF

// transactor transmitter & receiver state
typedef enum {
    Electrical_Idle = 0,   // Electrical Idle, Zero on bus
    Logical_Idle = 1,      // Logical Idle
    Link_Training = 2,     // Link Training (TS1 & TS2)
    DLL_Setup = 3,	   // Data Link Layer Setup (InitFC1 & InitFC2)
    Clock_Tolerance = 4,   // Clock Tolerance Compensation
    Skip = 5,              // Skip Ordered Set
    Power_Save = 6,        // Power Saving Modes (L0s, L1, L2)
    Fast_Training = 7,     // Fast Training Set (FTS)
    Active = 98,           // Active, ready for use
    UNKNOWN = 99
} pcie_state_enum_t;

// transaction special characters
// Define the Data Bytes associated with the Special Character Symbol Codes
#define PCIE_COM	0xBC	// K28.5 // Used for Lane and Link Initialization
#define PCIE_STP	0xFB	// K27.7 // Marks the start of a Transaction Layer Packet
#define PCIE_SDP	0x5C	// K28.2 // Marks the start of a Data Link Layer Packet
#define PCIE_END	0xFD	// K29.7 // Marks the end of a TLP or DLLP
#define PCIE_EDB	0xFE	// K30.7 // Marks the end of a nullified TLP
#define PCIE_PAD	0xF7	// K23.7 // Used in Framing and Link Width and Lane ordering negotiations
#define PCIE_SKP	0x1C	// K28.0 // Used for compensating for different bit rates for two communicating ports
#define PCIE_FTS	0x3C	// K28.1 // Used within an ordered set to exit from L0s to L0 (standby to idle)
#define PCIE_IDL	0x7C	// K28.3 // Symbol used in the Electrical Idle ordered set
#define PCIE_RSVD1	0x9C	// K28.4 // RESERVED
#define PCIE_RSVD2	0xDC	// K28.6 // RESERVED
#define PCIE_RSVD3	0xFC	// K28.7 // RESERVED

// PCIETransaction class
class PCIETransaction {
public: CARBONMEM_OVERRIDES;

   // Public Constructor by string
   PCIETransaction(CarbonXPcieTransactor *xtor, UInt32 idles);
   // Public Constructor by CarbonXPcieTLP
   PCIETransaction(CarbonXPcieTransactor *xtor, const CarbonXPcieTLP *cmd);
   PCIETransaction(CarbonXPcieTransactor *xtor, const CarbonXPcieDLLP &cmd);
   PCIETransaction(void) {}
   PCIETransaction(const PCIETransaction &other) {
      copy(other);
   }
   PCIETransaction& operator=(const PCIETransaction & other) {
      copy(other);
      return *this;
   }

   // Destructor.
   ~PCIETransaction(void);

   typedef void (CompletionFunc)(UInt32 address, UInt32 ** data, void * user_arg);

   typedef enum {   // Flow control credit type used
      Posted,
      NonPosted,
      Completion,
      None
   } FlowControlType;

   // Get a pointer to the CarbonXPcieTLP object which initialized this (if any)
   CarbonXPcieTLP* getCarbonXPcieTLP(void) { return mPcieCmd; }

   // Execute one step of the transaction.
   void step();
   // Is the transaction going across the bus?
   bool isStarted(void) { return (mBytesSent != 0);}
   // Is the transaction done going across the bus?
   bool isDone(void) { return mDone; }
   // Does this need to be replayed once finished being sent (only set if its in-progress on the bus)
   bool needsReplay(void) { return doReplay; }
   // Can this transaction be deleted?
   bool canDelete(void) { return ((not mIsValid) || ((mDone) && (not expectAck) && (not expectCpl))); }
   // Take this action when the transaction is completed.
   void onCompletion(CompletionFunc * func, void * user_arg);
   // Take any actions needed at the end of the transaction.
   void doCompletion(void);
   // Tell xtor what the transmitter state should be during this transaction
   pcie_state_enum_t getRunningOutputState(void) { return runOutputState; }
   // Retry this transaction (must have expectAck asserted and received a Nak from receiver)
   void replayTransaction(void);
   // Does this transaction require an Ack to be completed?
   bool needsAck(void) { return expectAck; }
   // Clears the 'expectAck' flag because there was an appropriate Ack received
   void clearNeedsAck() { expectAck = false; }
   // Does this transaction need a Completion to be finished?
   bool needsCpl() { return expectCpl; }
   // Clears the 'expectCpl' flag because there was an appropriate Completion received
   void clearNeedsCpl() { expectCpl = false; }
   // Return the Transaction ID
   UInt32 getTxID() const { return transID; }
   // Return the Transaction Sequence Number
   UInt32 getSeqNum() const { return DLLP_SeqNum; }
   // Return the printString to identify this transaction
   char* getPrintString() { return printString; }
   // Return whether the transacation is deemed valid or not
   bool isValid(void) const { return mIsValid; }
   // Return whether the transaction is special (internal) or not
   bool isInternal(void) const { return mIsSpecial; }
   // Return whether the transaction is a completion or not
   bool isCompletion(void) const { return mIsCpl; }
   // Return whether the transaction is a DLLP Ack
   bool isAck(void) const { return mIsAck; }
   // Return whether the transaction is a DLLP Nak
   bool isNak(void) const { return mIsNak; }
   // Return whether the transaction is a TLP transaction
   bool isTLPTransaction(void) const { return mIsTLP; }
   // Return whether the transaction is a DLLP transaction
   bool isDLLPTransaction(void) const { return (not mIsTLP); }
   // Return whether the transaction must start on lane zero
   bool mustStartLane0(void) const { return mMustStartLane0; }

   // Return the number of packets that still need to be transmitted
   UInt32 getNumPacketsRemaining(void) { return (mBytesTotal - mBytesSent); }
   // Return X packets on the UInt32* provided (must be big enough to hold data)
   void getTransmitPackets(UInt32 num, UInt32* datHolder, UInt32* symHolder);

   // Return type of Flow Control Credits consumed by this
   FlowControlType getFCType(void) const { return fcType; }
   // Return number of Flow Control Header Units taken up
   UInt32 getFCHeaderNum(void) const { return fcHeader; }
   // Return number of Flow Control Data Units taken up
   UInt32 getFCDataNum(void) const  { return fcData; }
   // Has transactor consumed the Flow Control Credits for this yet?
   bool getFCCreditsConsumed(void) const { return fcCreditsConsumed; }
   // Set that xtor has consumed the Flow Control Credits for this
   void setFCCreditsConsumed(void) { fcCreditsConsumed = true; }
   // Get the data length of the transaction (DWs)
   UInt32 getDataLength(void) const { return mDataLength; }

   // Print myself to an output stream
   void printToOStream(UtOStream* ostr);

 protected:
   PCIETransaction(CarbonXPcieTransactor *xtor);

   void initializeTransaction(CarbonXPcieTransactor*, const CarbonXPcieTLP*);
   void initializeTransaction(CarbonXPcieTransactor*, const CarbonXPcieDLLP &);
   void initializeTransactionSpecial(CarbonXPcieTransactor*, UInt32 idles);

   // Save pointer to the transactor who owns this transaction
   CarbonXPcieTransactor *mXtor;
   // Save copy of the CarbonXPcieTLP object used to initialize this object (if any)
   CarbonXPcieTLP* mPcieCmd;

   bool mIsValid;     // is this transaction valid or was there a problem at constructor time?
   bool mIsSpecial;   // is this a special (internal) transaction [T] or a user transaction [F]?
   bool mIsCpl;       // is this a completion transaction?
   bool mIsAck;       // is this a DLLP Ack transaction?
   bool mIsNak;      // is this a DLLP Nak transaction?
   bool mIsTLP;       // is this a TLP transaction
   bool mMustStartLane0; // this must start on lane 0 (OSet tx)
   bool mDone;
   bool mNeedSeqNumAndLCRC;    // does the transaction still need a Sequence Number and LCRC to be established
public:
   UInt32 mBytesSent;
   UInt32 mBytesTotal;
   // Physical Layer Packet Pointer
   CarbonXArray<UInt32> phys_bytes_dat;
   CarbonXArray<bool>  phys_bytes_sym;
   // What state should the xtor's transmitter be in when processing this transaction
   pcie_state_enum_t runOutputState;
   // Debug print string for command
   char printString[30];
   // Hold the DLLP Sequence Number for this transaction
   UInt32 DLLP_SeqNum;
   // Should we expect an Ack from the receiver before completion of the transaction?
   bool expectAck;
   // Should we expect a Completion from the receiver before completion of the transaction?
   bool expectCpl;
   UInt32 transID;  // save transaction ID (reqID + tag)
   // set if transaction should not be replayed, such as an ACK or NAK (any DLLP tx)
   bool canNotReplay;
   // set if the transaction should be replayed but is currently in progress (being put on the bus)
   bool doReplay;
   // number of retries started or to be started (incremented when replayTransaction() called)
   UInt32 numRetries;

   FlowControlType fcType;  // type of credits used
   UInt32 fcHeader;         // num of header credits used
   UInt32 fcData;           // num of data credits used
   bool fcCreditsConsumed;  // has xtor accounted for credits?
   UInt32 mDataLength;      // the data length, in DWs, needed for flow control

   CompletionFunc * mCompletionFunc;
   void * mCompletionFuncArg;  

public:
   // Methods used by new transactor
   bool inProgress() const {
      return mBytesSent < mBytesTotal;
   }
   PCIETransaction & operator++() {
      ++mBytesSent;
      return *this;
   }
   UInt32 getData() {
      return phys_bytes_dat[mBytesSent];
   }
   UInt32 getSym() {
      return phys_bytes_sym[mBytesSent];
   }
   
private:
   void copy(const PCIETransaction &other);
   CarbonXPcieCmdType getTlpTypeFromStr(char*);
   void setSeqNumAndCRC(CarbonXPcieTransactor*);
   void allocPhysBytes(UInt32);
};

#endif

/*
** Local Variables: 
** c-basic-offset: 3
** End:
*/
