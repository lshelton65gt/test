/***************************************************************************************
  Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "shell/carbon_shelltypes.h"
#include "CarbonXPcieTransactor.h"
#include "carbonXPcie.h"
#include "CarbonXPcieNet.h"
#include "CarbonXPcieCommonTypes.h"

CarbonXPcieID * carbonXPcieCreate(CarbonXPcieIntfType intf, 
				  CarbonUInt32        link_width,
				  const char *        print_name) {
  return new CarbonXPcieTransactor(intf, link_width, print_name);
}

void carbonXPcieDestroy(CarbonXPcieTransactor * xtor_id){
  delete xtor_id;
}

void carbonXPcieReset(CarbonXPcieID * xtor_id) {
  xtor_id->reset();
}

CarbonXPcieNetID * carbonXPcieGetNet(CarbonXPcieID *      xtor_id, 
				     CarbonXPcieSignalType sigType, 
				     CarbonUInt32          index) {
  return xtor_id->getNet(sigType, index);
}

void carbonXPcieSetTimescale(CarbonXPcieID *xtor, CarbonTimescale timescale) {
  xtor->getCfg().setTimescale(timescale);
}

void   carbonXPcieSetPrintLevel(CarbonXPcieID *xtor, CarbonUInt32 level) {
  xtor->getCfg().setPrintLevel(level);
}

CarbonUInt32 carbonXPcieGetPrintLevel(CarbonXPcieTransactor * xtor){
  return xtor->getCfg().getPrintLevel();
}

void   carbonXPcieSetBusNum(CarbonXPcieTransactor * xtor, CarbonUInt32 num){
  xtor->getCfg().mBusNum = num;
}

CarbonUInt32 carbonXPcieGetBusNum(CarbonXPcieTransactor * xtor){
  return xtor->getCfg().mBusNum;
}

void   carbonXPcieSetDevNum(CarbonXPcieTransactor * xtor, CarbonUInt32 num){
  xtor->getCfg().mDevNum = num;
}

CarbonUInt32 carbonXPcieGetDevNum(CarbonXPcieTransactor * xtor){
  return xtor->getCfg().mDevNum;
}

void   carbonXPcieSetFuncNum(CarbonXPcieTransactor * xtor, CarbonUInt32 num){
  xtor->getCfg().mFuncNum = num;
}

CarbonUInt32 carbonXPcieGetFuncNum(CarbonXPcieTransactor * xtor){
  return xtor->getCfg().mFuncNum;
}

void carbonXPcieSetScrambleStatus(CarbonXPcieTransactor * pimpl_, CarbonUInt32 tf) {
  pimpl_->getCfg().setScrambleEnable(tf);
}

CarbonUInt32 carbonXPcieGetScrambleStatus(CarbonXPcieTransactor * pimpl_) {
  return pimpl_->getCfg().getScrambleEnable();
}

void carbonXPcieSetMaxTransInFlight(CarbonXPcieTransactor * pimpl_, UInt32 max) {
   pimpl_->getCfg().mMaxTransInFlight = max;
}

UInt32 carbonXPcieGetMaxTransInFlight(CarbonXPcieTransactor * pimpl_) {
   return pimpl_->getCfg().mMaxTransInFlight;
}

CarbonUInt32 carbonXPcieStartTraining(CarbonXPcieTransactor * pimpl_, CarbonTime tick) {
   return pimpl_->startTraining(tick);
}

void carbonXPcieSetTrainingBypass(CarbonXPcieTransactor * pimpl_, CarbonUInt32 tf) {
   pimpl_->getCfg().setTrainingBypass(tf);
}

CarbonUInt32 carbonXPcieGetTrainingBypass(CarbonXPcieTransactor * pimpl_) {
   return pimpl_->getCfg().mTrainingBypass;
}

void carbonXPcieSetMaxPayloadSize(CarbonXPcieTransactor * pimpl_, UInt32 val) {
   pimpl_->getCfg().setMaxPayloadSize(val);
}

UInt32 carbonXPcieGetMaxPayloadSize(CarbonXPcieTransactor * pimpl_) {
   return pimpl_->getCfg().getMaxPayloadSize();
}

void carbonXPcieSetRCB(CarbonXPcieTransactor * pimpl_, UInt32 val) {
   pimpl_->getCfg().setRCB(val);
}

UInt32 carbonXPcieGetRCB(CarbonXPcieTransactor * pimpl_) {
   return pimpl_->getCfg().getRCB();
}

void carbonXPcieSetSupportedSpeeds(CarbonXPcieTransactor * pimpl_, UInt32 val) {
   pimpl_->getCfg().setSupportedSpeeds(val);
}

UInt32 carbonXPcieGetSupportedSpeeds(CarbonXPcieTransactor * pimpl_) {
   return pimpl_->getCfg().getSupportedSpeeds();
}

UInt32 carbonXPcieGetCurrentSpeed(CarbonXPcieTransactor * pimpl_) {
  return pimpl_->getCurrentSpeed();
}

void   carbonXPcieSetForceElecIdleInactive(CarbonXPcieID *pimpl_, CarbonUInt32 inactive){
  pimpl_->getCfg().setForceEIdleInactive(inactive);
}

CarbonUInt32 carbonXPcieGetForceElecIdleInactive(CarbonXPcieID *pimpl_){
  return pimpl_->getCfg().getForceEIdleInactive();  
}

void carbonXPcieSetTransCallbackFn(CarbonXPcieTransactor * pimpl_, CarbonXPcieTransCBFn *transCbFn, void* transCbInfo) {
  pimpl_->setTransCallbackFn(transCbFn, transCbInfo);
}

CarbonXPcieTransCBFn* carbonXPcieGetTransCallbackFn(CarbonXPcieTransactor * pimpl_) {
  return pimpl_->getTransCallbackFn();
}

void carbonXPcieSetStartTransCallbackFn(CarbonXPcieTransactor * pimpl_, void *caller, CarbonXPcieStartTransCBFn *transCbFn) {
   pimpl_->setStartTransCallbackFn(caller, transCbFn);
}

CarbonXPcieStartTransCBFn* carbonXPcieGetStartTransCallbackFn(CarbonXPcieTransactor * pimpl_) {
   return pimpl_->getStartTransCallbackFn();
}

void carbonXPcieSetEndTransCallbackFn(CarbonXPcieTransactor * pimpl_, void *caller, CarbonXPcieStartTransCBFn *transCbFn) {
   pimpl_->setEndTransCallbackFn(caller, transCbFn);
}

CarbonXPcieStartTransCBFn* carbonXPcieGetEndTransCallbackFn(CarbonXPcieTransactor * pimpl_) {
   return pimpl_->getEndTransCallbackFn();
}

void* carbonXPcieGetTransCallbackData(CarbonXPcieTransactor * pimpl_) {
  return pimpl_->getCfg().getTransCallbackData();
}

CarbonUInt32 carbonXPcieGetReturnData(CarbonXPcieTransactor * pimpl_, CarbonXPcieTLP *cmd) {
   return pimpl_->getReturnData(cmd);
}

UInt32 carbonXPcieGetReturnDataQueueLength(CarbonXPcieTransactor * pimpl_) {
   return pimpl_->getReturnDataQueueLength();
}

CarbonUInt32 carbonXPcieGetReceivedTrans(CarbonXPcieTransactor * pimpl_, CarbonXPcieTLP *cmd) {
   return pimpl_->getReceivedTrans(cmd);
}

UInt32 carbonXPcieGetReceivedTransQueueLength(CarbonXPcieTransactor * pimpl_) {
   return pimpl_->getReceivedTransQueueLength();
}

UInt32 carbonXPcieGetLinkWidth(CarbonXPcieTransactor * pimpl_) {
    return pimpl_->getCfg().mLinkWidth;
}

const char* carbonXPcieGetPrintName(CarbonXPcieTransactor * pimpl_) {
  return pimpl_->getCfg().mPrintName.c_str();
}

CarbonXPcieIntfType carbonXPcieGetInterfaceType(CarbonXPcieTransactor * pimpl_) {
   return pimpl_->getInterfaceType();
}

void carbonXPcieSetDoneCallbackFn(CarbonXPcieTransactor * pimpl_, CarbonXPcieDoneCBFn *doneCbFn, void* doneCbInfo) {
   pimpl_->setDoneCallbackFn(doneCbFn, doneCbInfo);
}

CarbonXPcieDoneCBFn* carbonXPcieGetDoneCallbackFn(CarbonXPcieTransactor * pimpl_) {
   return pimpl_->getDoneCallbackFn();
}

void   carbonXPcieSetRateChangeCallbackFn(CarbonXPcieID *pimpl_, void *caller, CarbonXPcieRateChangeCBFn *rateChangeCbFn) {
  pimpl_->setRateChangeCallbackFn(caller, rateChangeCbFn);
}

CarbonXPcieRateChangeCBFn* carbonXPcieGetRateChangeCallbackFn(CarbonXPcieID *pimpl_) {
  return pimpl_->getRateChangeCallbackFn();
}

void carbonXPcieAckDataRateChange(CarbonXPcieID *pimpl_) {
  pimpl_->ackDataRateChange();
}

void* carbonXPcieGetDoneCallbackData(CarbonXPcieTransactor * pimpl_) {
   return pimpl_->getDoneCallbackData();
}

CarbonSInt32 carbonXPcieAddTransaction(CarbonXPcieTransactor * pimpl_, const CarbonXPcieTLP *cmd) {
  return pimpl_->addTransaction(cmd);
}

void carbonXPcieSetElecIdleActiveHigh(CarbonXPcieTransactor * pimpl_) {
   pimpl_->getCfg().mEIdleActiveHigh = true;
}

void carbonXPcieSetElecIdleActiveLow(CarbonXPcieTransactor * pimpl_) {
   pimpl_->getCfg().mEIdleActiveHigh = false;
}

CarbonUInt32 carbonXPcieIsConfigured(CarbonXPcieTransactor * pimpl_) {
   return pimpl_->isConfigured();
}

void carbonXPciePrintConfiguration(CarbonXPcieTransactor * pimpl_) {
   pimpl_->printConfiguration();
}

CarbonUInt32 carbonXPcieIsDone(CarbonXPcieTransactor * pimpl_) {
   return pimpl_->isDoneForNow();
}

CarbonUInt32 carbonXPcieIsLinkActive(CarbonXPcieTransactor * pimpl_) {
   return pimpl_->isLinkActive();
}

void carbonXPciePrintLinkState(CarbonXPcieTransactor * pimpl_) {
   pimpl_->printLinkState();
}

void carbonXPcieStep(CarbonXPcieTransactor * pimpl_, UInt64 tick) {
   pimpl_->step(tick);
}

void carbonXPcieIdle(CarbonXPcieTransactor * pimpl_, UInt64 cycles) {
   pimpl_->idle(cycles);
}

void carbonXPcieSetupBufferSizes(CarbonXPcieTransactor * pimpl_, UInt32 vc, 
				 UInt32 posted_hdr,     UInt32 posted_data, 
				 UInt32 non_posted_hdr, UInt32 non_posted_data, 
				 UInt32 cpl_hdr,        UInt32 cpl_data) {
  pimpl_->setupBufferSizes(vc, posted_hdr, posted_data, non_posted_hdr, non_posted_data, cpl_hdr, cpl_data);
}
  

