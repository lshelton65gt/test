//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXSpi4SnkSupport.h
 *
 * This file contains non-public defines and support routines for the 
 *   Sink Transactor
 *
 * Author  : John Hoglund
 *
 * Created : Thu Sep 9 2005
 *
 *****************************************************************************
 */

#ifndef CARBONXSPI4SNKSUPPORT_H
#define CARBONXSPI4SNKSUPPORT_H


#define CARBONXSPI4SNK_MAXCALENDARLENGTH 256
#define CARBONXSPI4SNK_MAXPORTS 256

// CS Write Address Map

#define CARBONXSPI4SNK_CALENDARBASE      (0<<2)
#define CARBONXSPI4SNK_CALENDARLENGTH    (16<<2)
#define CARBONXSPI4SNK_CALENDARM         (17<<2)
#define CARBONXSPI4SNK_SHADCALBASE       (18<<2)
#define CARBONXSPI4SNK_SHADCALLENGTH     (34<<2)
#define CARBONXSPI4SNK_SHADCALM          (35<<2)
#define CARBONXSPI4SNK_STATTRAINREPS     (36<<2)
#define CARBONXSPI4SNK_FIFOMAXT          (37<<2)
#define CARBONXSPI4SNK_BPSCW             (38<<2)
#define CARBONXSPI4SNK_GAMMAVALID        
#define CARBONXSPI4SNK_GAMMAINVALID      
#define CARBONXSPI4SNK_ERRORCHECK        (39<<2)
#define CARBONXSPI4SNK_ERRORINSERT       (40<<2)
#define CARBONXSPI4SNK_LOSRESET          (41<<2)
#define CARBONXSPI4SNK_TIMEOUTVAL        (42<<2)

#define CARBONXSPI4SNK_HDLERRORS         ((0<<2)+256)
#define CARBONXSPI4SNK_STATUSBUSMODE     ((1<<2)+256)


typedef struct {
  s_int32     calendarLength;
  s_int32     shadowCalLength;
  s_int32     calendarM;
  s_int32     shadowCalM;
  s_int32     alpha;
  s_int32     dataMaxT;
  s_int32     fifoMaxT;
  u_int32     calendarState[CARBONXSPI4SNK_MAXCALENDARLENGTH];
  u_int32     shadowCalState[CARBONXSPI4SNK_MAXCALENDARLENGTH];
  s_int32     calendarPort[CARBONXSPI4SNK_MAXCALENDARLENGTH];
  s_int32     shadowCalPort[CARBONXSPI4SNK_MAXCALENDARLENGTH];
  u_int32     deltaValid;
  u_int32     deltaInvalid;
  u_int32     gammaValid;
  u_int32     gammaInvalid;
  u_int32     bpscwConfig;
  u_int32     trainSeqLen;
  u_int32     biterr;
  u_int32     shortSeq;
  s_int32     statTrainReps;
  s_int32     trainCycleCount;
  s_int32     sopCycleCount;
  s_int32     insertDip2Errors;
  s_int32     insertTrainErrors;
  s_int32     insertFrameErrors;
  s_int32     enableDip4Check;
  s_int32     Dip4ErrCnt;
} CARBONXSPI4SNK_INST_DATA_T;

CARBONXSPI4SNK_INST_DATA_T * carbonXSpi4SnkGetInstData (void);
void carbonXSpi4SnkSetInstData (CARBONXSPI4SNK_INST_DATA_T * ptr);
s_int32 carbonXSpi4SnkCheckDip4( u_int16 * rawData, 
			   u_int16 follCtrlWord,
			   u_int32 size );

u_int32 * carbonXSpi4SnkGetShadCal(void);
void    carbonXSpi4SnkSetShadCal(void);
void    carbonXSpi4SnkSetShadCalLen ( s_int32 length );
s_int32 carbonXSpi4SnkGetShadCalLen ( void );
void    carbonXSpi4SnkSetShadCalM ( s_int32 loops );
s_int32 carbonXSpi4SnkGetShadCalM ( void );
void    carbonXSpi4SnkSetShadCalPortMap ( s_int32 calLoc, 
				       s_int32 port );
s_int32 carbonXSpi4SnkGetShadCalPortMap ( s_int32 calLoc );

void carbonXSpi4SnkIncDip4ErrCnt( void );

#endif
