//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXSpi4Src.c
 *
 * This file contains the C-API for the SPI-4.2 Source Transactor
 *
 * Author  : John Hoglund
 *
 * Created : Thu Sep 8 2005
 *
 *****************************************************************************
 */

#include "tbp.h"
#include "carbonXTransaction.h"
#include "carbonXSpi4.h"
#include "carbonXSpi4Src.h"
#include "carbonXSpi4SrcSupport.h"
#include "carbonXSpi4Support.h"
#include "util/CarbonPlatform.h"
#include "util/c_memmanager.h"

// Function: carbonXSpi4SrcStartup
// Description: Configuration and sychronization function for the Source
//              transactor.  The transactor is first initialized and then
//              all the parameters are set as requested.  The function then
//              sends out training sequences on the bus until the V-side
//              signal LOS (loss of sync) goes to zero.  LOS goes to zero
//              when the (deltaValid) number of valid status frames (DIP-2
//              is correct) are received by the V-side.
//              This function must be called at the beginning of an attached
//              function.
void carbonXSpi4SrcStartup ( s_int32 calLength,
			  s_int32 calM,
			  s_int32 dataMaxT,
			  s_int32 alpha,
			  s_int32 deltaValid,
			  s_int32 deltaInvalid,
			  s_int32 fifoMaxT) {

  // Make sure DIP-2 errors are disabled
  carbonXSpi4SrcSetEnableStatDip2Errors(0);
  carbonXSpi4SrcSetCalLen(calLength);
  carbonXSpi4SrcSetCalM(calM);
  carbonXSpi4SrcSetDataMaxT(dataMaxT);
  carbonXSpi4SrcSetAlpha(alpha);
  carbonXSpi4SrcSetDeltaValid(deltaValid);
  carbonXSpi4SrcSetDeltaInvalid(deltaInvalid);
  carbonXSpi4SrcSetFifoMaxT(fifoMaxT);

  while (carbonXCsRead(CARBONXSPI4SRC_LOS) == 1) {
    carbonXSpi4SrcSendTraining(1);
  }
  // Ready to go so now enable DIP-2 errors on the V-side
  carbonXSpi4SrcSetEnableStatDip2Errors(1);
}


// Function: carbonXSpi4SrcPkt
// Description: Send all or part of a packet of bytes out of the source transactor
//              The function checks the current status of the port to decide
//              how much of the packet may be sent with this burst.  
//              The function sets the start of packet control word bit if sop
//              is one.  If the entire packet is sent, the end of packet status
//              is set in the control word.  DIP-4 is calculated and placed in
//              the control word.  
// Parameters: pkt  -- byte array of packet data
//             size -- number of bytes in pkt
//             port -- port on which to send pkt
//             sop  -- one means this is the start of pkt, zero means
//                     continuation of a partial packet
// Return: number of bytes of pkt actually sent out of the transactor
s_int32 carbonXSpi4SrcPkt( u_int8 *  pkt,
			s_int32   size,
			s_int32   port,
			u_int32   sop ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  int i;
  s_int32   ret = 0;
  u_int16   idleBuf[2];
  u_int16 * rawData;
  s_int32   rawDataCount = 0;
  u_int16   ctrlWord;
  s_int32   pktSizeInBlocks = (size/16) + ((size%16 == 0) ? 0 : 1);
  s_int32   pktSizeInWords  = (size/2)  + ((size%2  == 0) ? 0 : 1);
  s_int32   sentWords = 0;
  s_int32   ctrlFillerWords = 0;
  s_int32   cyclesToSend = 0;
  s_int32   eop;
  
  carbonXSpi4SrcUpdatePortCredits(port);
  // If credits are zero then we do nothing and return with zero bytes sent
  if (instData->credits[port] == 0) {
    return 0;
  }

  // First calculate how many cycles it's going to take to send the segment.
  // Either size or number of credits whichever is smaller.
  cyclesToSend = (size > (s_int32)(instData->credits[port]*16)) ? (s_int32)((instData->credits[port]*8) + 2)
                                                                : (s_int32)(pktSizeInWords + 2) ;

  // Figure out how big the rawData is going to be so we can that into account when
  // checking Data Max T

  // If this packet has SOP, fill in some idle control words until it has 
  // been 8 cycles since the last start of packet.
  if ((1 == sop) && (instData->sopCycleCount < 8)) {
    cyclesToSend += (8 - instData->sopCycleCount);

    // FIXME Bug5934, Adding an extra IDLE cycle if its 1, to woraround Snk Bug
    if( (8 - instData->sopCycleCount) == 1) {
      cyclesToSend++;
    }
  }
  
  // Check to see if it is time to send a training sequence
  //   If adding the number of cycles to send this full packet will run 
  //   past DATAMAXT, then do the training before the packet.
	
  if ((instData->trainCycleCount + cyclesToSend) >= (instData->dataMaxT-1)) {
    // Calculate the idle control word to lead into training
    ctrlWord = carbonXSpi4SrcBuildCtrlWord ( CARBONXSPI4SRC_IDLECTRLFLAG,
					  instData->eopsForNextCtrlWord,
					  CARBONXSPI4SRC_NOTSOPCTRLFLAG,
					  0, // sending training so port is 0
					  instData->dip16OfPreviousData );
    idleBuf[0] = ctrlWord;
    idleBuf[1] = 0x000f; // Extra Idle Word before Training sequence. Workaround for single Idle Bug

    sentWords = carbonXSpi4SrcRaw(idleBuf, 1, 1);
    // Clear the previous dip16 so other control words will be valid
    instData->dip16OfPreviousData = 0;
    carbonXSpi4SrcSendTraining( instData->alpha );
    instData->sopCycleCount = 8; // max out sopCycleCount since we just ran
    // a bunch of training.
    // Since we ran off cycles doing training, update the port 
    //   credits from the latest status.
    carbonXSpi4SrcUpdatePortCredits(port);
  }

  // Will the entire packet be sent or not. If not update size for allowed segment size
  if (pktSizeInBlocks > (int)instData->credits[port]) {  // Send entire packet
    size = instData->credits[port]*16;
    pktSizeInWords  = (size/2)  + ((size%2  == 0) ? 0 : 1);
    pktSizeInBlocks = (size/16) + ((size%16 == 0) ? 0 : 1);
    eop = 0;
  }
  else eop = 1;

  // Figure out how big the rawData is going to be so space can be malloc-ed
  ctrlFillerWords = 1;  // payload control word is always there
  // If this packet has SOP, fill in some idle control words until it has 
  // been 8 cycles since the last start of packet.
  if ((1 == sop) && (instData->sopCycleCount < 8)) {
    ctrlFillerWords += (8 - instData->sopCycleCount);
  }

  // FIXME Bug5934: Due to a bug in the Sink Transactor, we can't have a single filler Word
  // This line should be removed, when the bug in the Sink transactor has
  // been fixed.
  if(ctrlFillerWords == 2) ++ctrlFillerWords;

  rawData = carbonmem_malloc((pktSizeInWords+ctrlFillerWords) * sizeof(u_int16));
  if (NULL == rawData) {
    MSG_Panic("malloc failed in carbonXSpi4SrcPkt\n");
  }
  rawDataCount = 0;

  // rawData starts with idle control words if they are necessary for SOP
  if ((1 == sop) && (ctrlFillerWords > 1)) {
    // First idle control word must have the EOPS from the last payload
    ctrlWord = carbonXSpi4SrcBuildCtrlWord ( CARBONXSPI4SRC_IDLECTRLFLAG,
					  instData->eopsForNextCtrlWord,
					  CARBONXSPI4SRC_NOTSOPCTRLFLAG,
					  0, // port is zero for empty idles
					  instData->dip16OfPreviousData );
    rawData[rawDataCount++] = ctrlWord;  // first entry
    // Clear the previous dip16 so other control words will be valid
    instData->dip16OfPreviousData = 0;
    // Fill with more empty idle control words if necessary
    for (i=2; i < ctrlFillerWords; i++) {
      rawData[rawDataCount++] = CARBONXSPI4SRC_EMPTYIDLECTRLWORD;
    }
  }
  // Ready to do a control word and payload
  ctrlWord = carbonXSpi4SrcBuildCtrlWord ( CARBONXSPI4SRC_PAYLOADCTRLFLAG,
					instData->eopsForNextCtrlWord,
					sop,
					port,
					instData->dip16OfPreviousData );
 
  rawData[rawDataCount++] = ctrlWord;
  // Clear the previous dip16
  instData->dip16OfPreviousData = 0;
  
  // Calculate EOP for the next segment
  if ( carbonXSpi4SrcGetForceEopAbort() == 1 ) 
    instData->eopsForNextCtrlWord = 0x1;
  else if( eop && (size%2 == 0) )
    instData->eopsForNextCtrlWord = 0x2;
  else if( eop )
    instData->eopsForNextCtrlWord = 0x3;
  else
    instData->eopsForNextCtrlWord = 0x0;
  
  // Ready for payload, two bytes at a time
  for (i=0; i<size; i+=2) {
    if (i == (size-1)) {
      // Handle an odd last byte
      rawData[rawDataCount++] = pkt[i]<<8;
    } else {
      rawData[rawDataCount++] = (pkt[i] << 8) | pkt[i+1];
    }
    instData->dip16OfPreviousData = rotr(instData->dip16OfPreviousData ^
					 rawData[rawDataCount - 1]);
  }
  sentWords = carbonXSpi4SrcRaw( rawData, ctrlFillerWords, rawDataCount );
  // Update credits
  instData->credits[port] -= pktSizeInBlocks;
  
  // Update training cycles for DATA_MAX_T calculation
  instData->trainCycleCount += sentWords;
  
  // Update number of cycles since last start of packet,
  // note that filler words should not be counted since the sop
  // is put in after the filler words
  if (1 == sop) {
    instData->sopCycleCount = sentWords - ctrlFillerWords + 1;
  } else {
    instData->sopCycleCount += sentWords;
  }
  ret = size;

  carbonmem_free(rawData);

  return ret;
}

// Function: carbonXSpi4SrcIdle
// Description: Sends idle cycles on the SPI-4 bus.  The first idle gets the
//              EOPS and DIP-4 filled in from the previous transaction.
//              If it is time for training, then a training sequence is sent.
//              The total number of cycles spent in idle are returned.  This 
//              may be more than the requested number because of the addition
//              of training sequences.
// Parameters: s_int32 idleCount -- requested number of idle cycles
// Return: Actual number of cycles spent, idle and training
s_int32 carbonXSpi4SrcIdle ( s_int32 idleCount ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  u_int16   ctrlWord;
  u_int16   dip16;
  u_int16   dip8;
  u_int16   dip4;
  s_int32   sentWords = 0;
  u_int16   idleWords[2];

  // Calculate the first idle control word with EOPS and DIP-4 from the
  //   previous transaction
  ctrlWord = 0;
  ctrlWord |= ((instData->eopsForNextCtrlWord << 13) & 0x6000);
  instData->eopsForNextCtrlWord = 0;
  ctrlWord |= 0x000F; // Init DIP-4 to all one for calculation
  // dip16ofPrevousData has already been rotated by the function that calculated it
  //   so all that has to be done now is Xor.
  dip16 = instData->dip16OfPreviousData ^ ctrlWord;
  dip8 = (dip16&0xFF) ^ ((dip16>>8)&0xFF);
  dip4 = ((dip8&0xF) ^ ((dip8>>4)&0xF)) & 0xF;
  ctrlWord &= 0xFFF0;  // clear the dip-4
  ctrlWord |= dip4;
  sentWords = carbonXSpi4SrcRaw(&ctrlWord, 1, 1);
  // Clear the previous dip16 so other control words will be valid
  instData->dip16OfPreviousData = 0;
  instData->trainCycleCount++;
  instData->sopCycleCount++;
  while (sentWords < idleCount) {
    if (instData->trainCycleCount >= (instData->dataMaxT-2)) {
      
      // Workaraound for no single Idles Bug, do two extra Idle Here
      idleWords[0] = 0x000f;
      idleWords[1] = 0x000f;
      sentWords += carbonXSpi4SrcRaw(idleWords, 2, 2);
      
      // Must do training now
      carbonXSpi4SrcSendTraining( instData->alpha );
      instData->sopCycleCount += (instData->alpha * 20);
      sentWords += (instData->alpha * 20);
    } else {
      // Do empty idles up to time for training (if necessary)
      if ((instData->trainCycleCount + (idleCount - sentWords)) >= (instData->dataMaxT-2)) {
	carbonXIdle(instData->dataMaxT - instData->trainCycleCount-2);
	instData->trainCycleCount += (instData->dataMaxT - instData->trainCycleCount);
	instData->sopCycleCount   += (instData->dataMaxT - instData->trainCycleCount);
	sentWords                 += (instData->dataMaxT - instData->trainCycleCount);
      } else {
	carbonXIdle(idleCount - sentWords);
	instData->trainCycleCount += (idleCount - sentWords);
	instData->sopCycleCount   += (idleCount - sentWords);
	sentWords += (idleCount - sentWords);
      }
    }
  }
  return sentWords;
}

// Function: carbonXSpi4SrcRaw
// Description: Output data from the Source transactor.
// Parameters:
//   rawCtrlData -- array of 16-bit unsigned words to be output.  One on each
//                  edge of dclk_out
//   numCtrl -- number of words that have ctl_out set to "1" starting with 
//              the first word.  
//   size -- number of words in rawCtrlData
//   
// Return: the number of words actually sent.
s_int32 carbonXSpi4SrcRaw ( u_int16 * rawCtrlData,
		       s_int32 numCtrl,
		       s_int32 size ) {
  s_int32 bytesSent = 0;
  if (numCtrl > size) {
    MSG_Error("Number of control words (%d) cannot be greater than size (%d)\n",
	      numCtrl, size);
  } else {
    bytesSent = carbonXWriteArray16(numCtrl, rawCtrlData, size*2); // Write takes size in bytes
  }
  return size;
}

// Function: carbonXSpi4SrcGetCal
// Description: Reads the current state of the calendar from the V-side.
// Parameters: calendarContents -- gets written with a pointer to the array
//               containing the contents of the calendar.  One calendar entry
//               in the bottom two bits of each array entry.
// Return:  Latest entry in the calendarContents to be updated in the lower
//          16 bits.  Total number of calendar update frames received 
//          in the upper 16 bits.  These may saved away to show which 
//          calendar entries are updated between calls.
u_int32 carbonXSpi4SrcGetCal(u_int32 ** calendarContents) {
  int i,j;
  u_int32 readData;
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  for (i=0; i<(instData->calendarLength+15)/16; i++) {
    readData = carbonXCsRead(CARBONXSPI4SRC_CALENDARBASE+(i<<2));
    for (j=0; j<16; j++) {
      instData->calendarState[j+(i*16)] = (readData>>(j*2)) & 0x3;
    }
  }
  *calendarContents = instData->calendarState;
  return carbonXCsRead(CARBONXSPI4SRC_CALCOUNT);
}


s_int32 carbonXSpi4SrcGetCalMaxLen ( void ) {
  return CARBONXSPI4SRC_MAXCALENDARLENGTH;
}

void carbonXSpi4SrcSetCalLen ( s_int32 length ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((length >= 0) && (length <= CARBONXSPI4SRC_MAXCALENDARLENGTH)) {
    carbonXCsWrite(CARBONXSPI4SRC_CALENDARLENGTH, length);
    instData->calendarLength = length;
  } else { // invalid length parameter
    MSG_Error("Requested calendar length (%d) is illegal.  Min=%d, Max=%d\n",
	      length, 0, CARBONXSPI4SRC_MAXCALENDARLENGTH);
  }
}
s_int32 carbonXSpi4SrcGetCalLen ( void ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  return instData->calendarLength;
}
void carbonXSpi4SrcSetCalM ( s_int32 loops ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if (loops >= 0) {
    carbonXCsWrite(CARBONXSPI4SRC_CALENDARM, loops);
    instData->calendarM = loops;
  } else { // invalid loops parameter
    MSG_Error("Requested calendar M (%d) is illegal.  Min=%d\n",
	      loops, 0);
  }
}
s_int32 carbonXSpi4SrcGetCalM ( void ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  return instData->calendarM;
}
void carbonXSpi4SrcSetCalPortMap ( s_int32 calLoc, 
				s_int32 port ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((calLoc>=0) && (calLoc<CARBONXSPI4SRC_MAXCALENDARLENGTH) &&
      (port>=0) && (port<CARBONXSPI4SRC_MAXPORTS)) {
    instData->calendarPort[calLoc] = port;
  } else {
    MSG_Error("Calendar location (%d) not legal, Min=%d, Max=%d\n",
	      calLoc, 0, CARBONXSPI4SRC_MAXCALENDARLENGTH);
  }
}
s_int32 carbonXSpi4SrcGetCalPortMap ( s_int32 calLoc ) {
  s_int32 ret;
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((calLoc>=0) && (calLoc<CARBONXSPI4SRC_MAXCALENDARLENGTH)) {
    ret = instData->calendarPort[calLoc];
  } else {
    MSG_Panic("Calendar location (%d) not legal, Min=%d, Max=%d\n",
	      calLoc, 0, CARBONXSPI4SRC_MAXCALENDARLENGTH);
    ret = -1;
  }
  return ret;
}
void carbonXSpi4SrcSetCalMaxB1 ( s_int32 port, 
			      s_int32 maxB1 ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((port>=0) && (port<CARBONXSPI4SRC_MAXPORTS)) {
    instData->burstSize1[port] = maxB1;
  } else {
    MSG_Error("Illegal Port number (%d) in carbonXSpi4SrcSetCalMaxB1, min=%d, max=%d\n",
	      port, 0, CARBONXSPI4SRC_MAXPORTS);
  }
}
s_int32 carbonXSpi4SrcGetCalMaxB1 ( s_int32 port ) {
  s_int32 ret;
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((port>=0) && (port<CARBONXSPI4SRC_MAXPORTS)) {
    ret = instData->burstSize1[port];
  } else {
    MSG_Error("Illegal Port number (%d) in carbonXSpi4SrcGetCalMaxB1, min=%d, max=%d\n",
	      port, 0, CARBONXSPI4SRC_MAXPORTS);
    ret = -1;
  }
  return ret;
}
void carbonXSpi4SrcSetCalMaxB2 ( s_int32 port, 
			      s_int32 maxB2 ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((port>=0) && (port<CARBONXSPI4SRC_MAXPORTS)) {
    instData->burstSize2[port] = maxB2;
  } else {
    MSG_Error("Illegal Port number (%d) in carbonXSpi4SrcSetCalMaxB2, min=%d, max=%d\n",
	      port, 0, CARBONXSPI4SRC_MAXPORTS);
  }
}
s_int32 carbonXSpi4SrcGetCalMaxB2 ( s_int32 port ) {
  s_int32 ret;
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((port>=0) && (port<CARBONXSPI4SRC_MAXPORTS)) {
    ret = instData->burstSize2[port];
  } else {
    MSG_Error("Illegal Port number (%d) in carbonXSpi4SrcGetCalMaxB2, min=%d, max=%d\n",
	      port, 0, CARBONXSPI4SRC_MAXPORTS);
    ret = -1;
  }
  return ret;
}
s_int32 carbonXSpi4SrcGetCredits ( s_int32 port ) {
  s_int32 ret;
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((port>=0) && (port<CARBONXSPI4SRC_MAXPORTS)) {
    carbonXSpi4SrcUpdatePortCredits(port);
    ret = instData->credits[port];
  } else {
    MSG_Error("Illegal Port number (%d) in carbonXSpi4SrcGetCredits, min=%d, max=%d\n",
	      port, 0, CARBONXSPI4SRC_MAXPORTS);
    ret = -1;
  }

  return ret;
}
void carbonXSpi4SrcSetAlpha ( s_int32 alpha ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if (alpha >= 0) {
    instData->alpha = alpha;
  } else {
    MSG_Error("Illegal ALPHA (%d) in carbonXSpi4SrcSetAlpha, must be greater than zero\n",
	      alpha);
  }
}
s_int32 carbonXSpi4SrcGetAlpha ( void ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  return instData->alpha;
}
void carbonXSpi4SrcSetDataMaxT ( s_int32 dataMaxT ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if (dataMaxT >= 0) {
    instData->dataMaxT = dataMaxT;
  } else {
    MSG_Error("Illegal DATA_MAX_T (%d) in carbonXSpi4SrcSetDataMaxT, must be greater than zero\n",
	      dataMaxT);
  }
}
s_int32 carbonXSpi4SrcGetDataMaxT ( void ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  return instData->dataMaxT;
}
void carbonXSpi4SrcSetFifoMaxT ( s_int32 fifoMaxT ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if (fifoMaxT >= 0) {
    carbonXCsWrite(CARBONXSPI4SRC_FIFOMAXT, fifoMaxT);
    instData->fifoMaxT = fifoMaxT;
  } else {
    MSG_Error("Illegal FIFO_MAX_T (%d) in carbonXSpi4SrcSetFifoMaxT, must be greater than zero\n",
	      fifoMaxT);
  }
}
s_int32 carbonXSpi4SrcGetFifoMaxT ( void ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  return instData->fifoMaxT;
}
void carbonXSpi4SrcSetEnableStatDip2Errors ( s_int32 enableDip2Err ) {
  u_int32 writeData = 0;
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((enableDip2Err == 0) || (enableDip2Err == 1)) {
    if (instData->enableStatDip2Errors != enableDip2Err) {
      writeData = (((instData->enableStatFrameErrors << 2) | 
		    (instData->enableStatTrainErrors << 1) |
		    (enableDip2Err << 0)) &
		   0x7);
      carbonXCsWrite(CARBONXSPI4SRC_ERRORCONTROL, writeData);
      instData->enableStatDip2Errors = enableDip2Err;
    }
  } else {
    MSG_Error("Enable DIP-2 Error value (%d) must be 0 or 1\n", 
	      enableDip2Err);
  }
}
s_int32 carbonXSpi4SrcGetEnableStatDip2Errors ( void ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  return instData->enableStatDip2Errors;
}

void carbonXSpi4SrcSetEnableStatTrainErrors ( s_int32 enableTrainErr ) {
  u_int32 writeData = 0;
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((enableTrainErr == 0) || (enableTrainErr == 1)) {
    if (instData->enableStatTrainErrors != enableTrainErr) {
      writeData = (((instData->enableStatFrameErrors << 2) | 
		    (enableTrainErr << 1) |
		    (instData->enableStatDip2Errors << 0)) &
		   0x7);
      carbonXCsWrite(CARBONXSPI4SRC_ERRORCONTROL, writeData);
      instData->enableStatTrainErrors = enableTrainErr;
    }
  } else {
    MSG_Error("Enable Training Error value (%d) must be 0 or 1\n", 
	      enableTrainErr);
  }
}
s_int32 carbonXSpi4SrcGetEnableStatTrainErrors ( void ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  return instData->enableStatTrainErrors;
}

void carbonXSpi4SrcSetEnableStatFrameErrors ( s_int32 enableFrameErr ) {
  u_int32 writeData = 0;
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((enableFrameErr == 0) || (enableFrameErr == 1)) {
    if (instData->enableStatFrameErrors != enableFrameErr) {
      writeData = (((enableFrameErr << 2) | 
		    (instData->enableStatTrainErrors << 1) |
		    (instData->enableStatDip2Errors << 0)) &
		   0x7);
      carbonXCsWrite(CARBONXSPI4SRC_ERRORCONTROL, writeData);
      instData->enableStatFrameErrors = enableFrameErr;
    }
  } else {
    MSG_Error("Enable Framing Error value (%d) must be 0 or 1\n", 
	      enableFrameErr);
  }
}
s_int32 carbonXSpi4SrcGetEnableStatFrameErrors ( void ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  return instData->enableStatFrameErrors;
}

void carbonXSpi4SrcSetForceEopAbort ( s_int32 eopAbort ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((eopAbort == 0) || (eopAbort == 1)) {
    instData->eopAbort = eopAbort;
  } else {
    MSG_Error("Force End of Packet Abort value (%d) must be 0 or 1\n", 
	      eopAbort);
  }
}
s_int32 carbonXSpi4SrcGetForceEopAbort ( void ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  return instData->eopAbort;
}

void    carbonXSpi4SrcSetForceDip4Errors ( s_int32 dip4Errors ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((dip4Errors == 0) || (dip4Errors == 1)) {
    instData->dip4ErrorInsert = dip4Errors;
  } else {
    MSG_Error("DIP-4 Error Insert value (%d) must be 0 or 1\n", 
	      dip4Errors);
  }
}
s_int32 carbonXSpi4SrcGetForceDip4Errors ( void ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  return instData->dip4ErrorInsert;
}
void    carbonXSpi4SrcSetForceDip4ErrorMask ( s_int32 dip4ErrorMask ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((dip4ErrorMask >= 0x0) && (dip4ErrorMask <= 0xF)) {
    instData->dip4ErrorMask = dip4ErrorMask;
  } else {
    MSG_Error("DIP-4 Error Mask value (%d) must be between 0x0 and 0xF\n", 
	      dip4ErrorMask);
  }
}
s_int32 carbonXSpi4SrcGetForceDip4ErrorMask ( void ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  return instData->dip4ErrorMask;
}

s_int32 carbonXSpi4SrcGetStatDip2ErrorCount ( void ) {
  return (carbonXCsRead( CARBONXSPI4SRC_HDLERRORS ) >> 0) & 0xFF;
}
s_int32 carbonXSpi4SrcGetStatTrainErrorCount ( void ) {
  return (carbonXCsRead( CARBONXSPI4SRC_HDLERRORS ) >> 16) & 0xFF;
}
s_int32 carbonXSpi4SrcGetStatFrameErrorCount ( void ) {
  return (carbonXCsRead( CARBONXSPI4SRC_HDLERRORS ) >> 8) & 0xFF;
}

void carbonXSpi4SrcSetDeltaValid ( s_int32 deltaValid ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if (deltaValid > 0) {
    carbonXCsWrite(CARBONXSPI4SRC_DELTAVALID, deltaValid);
    instData->deltaValid = deltaValid;
  } else {
    MSG_Error("Number of requested valid DIP-2 (%d) to sync with Sink side must be greater than zero\n",
	      deltaValid);
  }
}
s_int32 carbonXSpi4SrcGetDeltaValid ( void ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  return instData->deltaValid;
}

void carbonXSpi4SrcSetDeltaInvalid ( s_int32 deltaInvalid ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if (deltaInvalid > 0) {
    carbonXCsWrite(CARBONXSPI4SRC_DELTAINVALID, deltaInvalid);
    instData->deltaInvalid = deltaInvalid;
  } else {
    MSG_Error("Number of requested invalid DIP-2 (%d) to lose sync with Sink side must be greater than zero\n",
	      deltaInvalid);
  }
}
s_int32 carbonXSpi4SrcGetDeltaInvalid ( void ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  return instData->deltaInvalid;
}

CARBONXSPI4SRC_INST_DATA_T * carbonXSpi4SrcGetInstData (void) {
  CARBONXSPI4SRC_INST_DATA_T * instData = 
    (CARBONXSPI4SRC_INST_DATA_T *) carbonXGetInstData();
  if (NULL == instData) {
    carbonXSpi4SrcInit();
    instData = (CARBONXSPI4SRC_INST_DATA_T *) carbonXGetInstData();
  }
  return instData;
}

void carbonXSpi4SrcSetInstData (CARBONXSPI4SRC_INST_DATA_T * ptr) {
  carbonXSetInstData(ptr);
}

u_int32 carbonXSpi4SrcGetPortStatus(s_int32 port) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  u_int32 ret = 0;
  s_int32 i;
  s_int32   calLen = carbonXSpi4SrcGetCalLen( );
  u_int32   calReturn;
  u_int32   calLatestUpdate = 0;
  u_int32   calTotalFrames = 0;
  u_int32 * calContents = NULL;
  u_int32   portStatus = 0;
  s_int32   foundLatestPortStatus = -1;
  calReturn = carbonXSpi4SrcGetCal( & calContents );
  calLatestUpdate = calReturn & 0xFFFF;
  calTotalFrames = (calReturn >> 16) & 0xFFFF;
  // Search backward from the latest calendar entry to be updated (calLatestUpdate)
  // until the latest status of the desired port is found.  Count down to zero
  // first and then down from calLength to loop through the entire calendar.
  for (i=calLatestUpdate; i>=0; i--) {
    if (port == carbonXSpi4SrcGetCalPortMap(i)) {
      portStatus = calContents[i];
      foundLatestPortStatus = i;
      break;
    }
  }
  if (-1 == foundLatestPortStatus) {
    for (i=calLen; i>(s_int32)calLatestUpdate; i--) {
      if (port == carbonXSpi4SrcGetCalPortMap(i)) {
	portStatus = calContents[i];
	foundLatestPortStatus = i;
	break;
      }
    }
  }
  if (-1 == foundLatestPortStatus) {
    MSG_Panic("Requested Port (%d) is not in the Calendar\n", port);
  } else {
    switch (calTotalFrames - instData->calTotalFrames[port]) 
      {
      case 0:
	// The current calendar frame has not finished so the status is only
	// updated if the latest updated is greater than the previous latest
	// update and is less than the current latest update.  
	if ((foundLatestPortStatus >= instData->calLatestUpdate[port]) &&
	    ((s_int32)calLatestUpdate >= foundLatestPortStatus)) {
	  // Port status has been updated so it is good
	  instData->calLatestUpdate[port] = calLatestUpdate;
	  instData->calTotalFrames[port] = calTotalFrames;
	  ret = portStatus;
	} else {
	  // Port status is not new so return satisfied to stop any update
	  ret = CARBONXSPI4SRC_SATISFIED;
	}
	break;
      case 1:
	// The current calendar frame has completed but the next is only 
	// partially complete. If the current update is greater than 
	// the found and the current is less than the previous then 
	// the status is has been updated.  If the current update is less
	// than the found and found is greater than previous then the status
	// has been updated.
	if ((( (s_int32)calLatestUpdate >= foundLatestPortStatus) &&
	     (calLatestUpdate <= instData->calLatestUpdate[port])) ||
	    (( (s_int32)calLatestUpdate <= foundLatestPortStatus) &&
	     (foundLatestPortStatus >= instData->calLatestUpdate[port]))) {
	  // Port status has been updated so it is good
	  instData->calLatestUpdate[port] = calLatestUpdate;
	  instData->calTotalFrames[port] = calTotalFrames;
	  ret = portStatus;
	} else {
	  // Port status is not new so return satisfied to stop any update
	  ret = CARBONXSPI4SRC_SATISFIED;
	}
	break;
      default:
	// At least one full frame has updated so the port status is valid
	instData->calLatestUpdate[port] = calLatestUpdate;
	instData->calTotalFrames[port] = calTotalFrames;
	ret = portStatus;
	break;
      }
  }
  return ret;
}

void carbonXSpi4SrcUpdatePortCredits(s_int32 port) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  
  switch(carbonXSpi4SrcGetPortStatus(port))
    {
    case CARBONXSPI4SRC_STARVING:
      if (instData->credits[port] < instData->burstSize1[port]) {
	instData->credits[port] = instData->burstSize1[port];
      } else {
	// Nothing -- credits stay the same if they are more than MAXBURST1
      }
      break;
    case CARBONXSPI4SRC_HUNGRY:
      if (instData->credits[port] < instData->burstSize2[port]) {
	instData->credits[port] = instData->burstSize2[port];
      } else {
	// Nothing -- credits stay the same if they are more than MAXBURST2
      }
      break;
    case CARBONXSPI4SRC_SATISFIED:
      // Nothing -- no more credits when satisfied
      break;
    case CARBONXSPI4SRC_BROKEN:
      // Link is broken so credits go to zero
      instData->credits[port] = 0;
      break;
    default:
      MSG_Error("Bad status for port = %d\n", port);
      break;
    }
}

u_int16 carbonXSpi4SrcBuildCtrlWord ( u_int32 idlePayload, 
				   u_int32 eops,
				   u_int32 sop,
				   u_int32 port,
				   u_int16 dip16OfPreviousData ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  u_int16   dip16;
  u_int16   dip8;
  u_int16   dip4;
  u_int16 ctrlWord = 0;
  u_int16 ctrlpredip = 0;
  ctrlWord |= ((idlePayload << 15) & 0x8000);
  ctrlWord |= ((eops << 13) & 0x6000);
  ctrlWord |= ((sop << 12) & 0x1000);
  ctrlWord |= ((port << 4) & 0x0FF0);
  ctrlWord |= 0x000F; // Init DIP-4 to all one for calculation
  // dip16ofPrevousData has already been rotated by the function that calculated it
  //   so all that has to be done now is Xor.
  ctrlpredip = ctrlWord;
  dip16 = dip16OfPreviousData ^ ctrlWord;
  dip8 = (dip16&0xFF) ^ ((dip16>>8)&0xFF);
  dip4 = ((dip8&0xF) ^ ((dip8>>4)&0xF)) & 0xF;
  if (1 == instData->dip4ErrorInsert) {
    dip4 = (dip4 ^ instData->dip4ErrorMask) & 0xF;
  }
  ctrlWord &= 0xFFF0;  // clear the dip-4
  ctrlWord |= dip4;

  return ctrlWord;
}

s_int32 carbonXSpi4SrcSendTraining ( s_int32 count ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  int i;
  u_int16 trainBuf [20];
  for (i=0; i<10; i++) {
    trainBuf[i] = 0x0fff;
    trainBuf[i+10] = 0xf000;
  }
  for (i=0; i<count; i++) {
    carbonXSpi4SrcRaw(trainBuf, 10, 20);
  }
  instData->trainCycleCount = 0; // reset count
  return (20 * count);
}

// Function: carbonXSpi4SrcInit
// Description: Initializes the state of the transactor.  This must 
//              be called at the beginning of the attached function.
void carbonXSpi4SrcInit(void) {
  u_int32 i;
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonmem_malloc(sizeof(CARBONXSPI4SRC_INST_DATA_T));
  if (instData != NULL) {
    carbonXSpi4SrcSetInstData(instData);
    carbonXSpi4SrcSetCalLen ( 0 );
    carbonXSpi4SrcSetShadCalLen ( 0 );
    carbonXSpi4SrcSetCalM ( 0 );
    carbonXSpi4SrcSetShadCalM ( 0 );
    instData->alpha = 0;
    instData->dataMaxT = 0;
    instData->fifoMaxT = 0;
    for (i=0; i<CARBONXSPI4SRC_MAXCALENDARLENGTH; i++) {
      instData->calendarState[i] = 0x3;
      instData->shadowCalState[i] = 0x3;
      instData->calendarPort[i] = 0;
      instData->shadowCalPort[i] = 0;
    }
    for (i=0; i<CARBONXSPI4SRC_MAXPORTS; i++) {
      instData->burstSize1[i] = 0;
      instData->burstSize2[i] = 0;
      instData->credits[i] = 0;
      instData->calLatestUpdate[i] = 0;
      instData->calTotalFrames[i] = 0;
    }
    carbonXSpi4SrcSetDeltaValid ( 1 );
    carbonXSpi4SrcSetDeltaInvalid ( 1 );
    instData->bpscwConfig = 0;
    instData->trainSeqLen = 0;
    instData->biterr = 0;
    instData->shortSeq = 0;
    instData->enableStatTrainErrors = 0;
    instData->enableStatFrameErrors = 0;
    instData->enableStatDip2Errors = 1; // set to 1 to force a write to v-side
    carbonXSpi4SrcSetEnableStatDip2Errors ( 0 );
    instData->enableStatTrainErrors = 1; // set to 1 to force a write to v-side
    carbonXSpi4SrcSetEnableStatTrainErrors ( 0 );
    instData->enableStatFrameErrors = 1; // set to 1 to force a write to v-side
    carbonXSpi4SrcSetEnableStatFrameErrors ( 0 );
    instData->eopAbort = 0;
    instData->trainCycleCount = 0;
    instData->sopCycleCount = 8; // No back-to-back problems at startup so max this out
    instData->eopsForNextCtrlWord = 0;
    instData->dip16OfPreviousData = 0;
    instData->dip4ErrorInsert = 0;
    instData->dip4ErrorMask = 0;
  } else { 
    MSG_Panic("Malloc failed in carbonXSpi4SrcInit\n");
  } 
}

void carbonXSpi4SrcSetShadCalLen ( s_int32 length ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((length >= 0) && (length <= CARBONXSPI4SRC_MAXCALENDARLENGTH)) {
    carbonXCsWrite(CARBONXSPI4SRC_CALENDARLENGTH, length);
    instData->shadowCalLength = length;
  } else { // invalid length parameter
    MSG_Error("Requested shadow calendar length (%d) is illegal.  Min=%d, Max=%d\n",
	      length, 1, CARBONXSPI4SRC_MAXCALENDARLENGTH);
  }
}
s_int32 carbonXSpi4SrcGetShadCalLen ( void ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  return instData->shadowCalLength;
}
void carbonXSpi4SrcSetShadCalM ( s_int32 loops ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if (loops >= 0) {
    carbonXCsWrite(CARBONXSPI4SRC_CALENDARM, loops);
    instData->shadowCalM = loops;
  } else { // invalid loops parameter
    MSG_Error("Requested calendar M (%d) is illegal.  Min=%d\n",
	      loops, 0);
  }
}
s_int32 carbonXSpi4SrcGetShadCalM ( void ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  return instData->shadowCalM;
}
void carbonXSpi4SrcSetShadCalPortMap ( s_int32 calLoc, 
				    s_int32 port ) {
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((calLoc>=0) && (calLoc<CARBONXSPI4SRC_MAXCALENDARLENGTH) &&
      (port>=0) && (port<CARBONXSPI4SRC_MAXPORTS)) {
    instData->shadowCalPort[calLoc] = port;
  } else {
    MSG_Error("Calendar location (%d) not legal, Min=%d, Max=%d\n",
	      calLoc, 0, CARBONXSPI4SRC_MAXCALENDARLENGTH);
  }
}
s_int32 carbonXSpi4SrcGetShadCalPortMap ( s_int32 calLoc ) {
  s_int32 ret;
  CARBONXSPI4SRC_INST_DATA_T * instData = carbonXSpi4SrcGetInstData();
  if ((calLoc>=0) && (calLoc<CARBONXSPI4SRC_MAXCALENDARLENGTH)) {
    ret = instData->shadowCalPort[calLoc];
  } else {
    MSG_Error("Calendar location (%d) not legal, Min=%d, Max=%d\n",
	      calLoc, 0, CARBONXSPI4SRC_MAXCALENDARLENGTH);
    ret = -1;
  }
  return ret;
}

s_int32 carbonXSpi4SrcGetStatBusMode ( void ) {
  return (carbonXCsRead( CARBONXSPI4SRC_STATUSBUSMODE ) >> 0) & 0x01;
}

s_int32 carbonXSpi4SrcGetLOS ( void ) {
  return carbonXCsRead(CARBONXSPI4SRC_LOS) & 0x01;
}

u_int16 carbonXSpi4SrcCalcDip4 ( u_int16 * rawData, 
			      s_int32   size ) {
  s_int32 i;
  u_int16 dip16 = rawData[0];
  u_int16 dip8 = 0;
  u_int16 dip4 = 0;
  for (i=1; i<size; i++) {
    dip16 = rotr(dip16) ^ rawData[i];
  }
  dip8 = (dip16&0xFF) ^ ((dip16>>8)&0xFF);
  dip4 = ((dip8&0xF) ^ ((dip8>>4)&0xF)) & 0xF;
  return dip4;
}


// These functions are support functions for the SystemC Interface
void carbonXSpi4SrcProcessConf(const struct carbonXTransactionStruct *transReq, struct carbonXTransactionStruct *transResp) {
  
  SInt32 *data;
  UInt32 i;
  
  switch(transReq->mOpcode) {
  case CARBONXSPI4_OP_STARTUP :
    carbonXSpi4SrcStartup(carbonXSpi4SrcGetCalLen(), carbonXSpi4SrcGetCalM(), carbonXSpi4SrcGetDataMaxT(),
			  carbonXSpi4SrcGetAlpha(), carbonXSpi4SrcGetDeltaValid(), carbonXSpi4SrcGetDeltaInvalid(),
			  carbonXSpi4SrcGetFifoMaxT());
    carbonXSetTransaction(transResp, transReq->mTransId, TBP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
    break;
  case CARBONXSPI4_OP_SET_CAL_PORTMAP :
    data = (SInt32 *)transReq->mData;
    carbonXSpi4SrcSetCalLen(transReq->mSize/4);
    for(i = 0; i < transReq->mSize/4; i++) {
      carbonXSpi4SrcSetCalPortMap(transReq->mAddress+i, data[i]);
    }
    carbonXSetTransaction(transResp, transReq->mTransId, TBP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
    break;
  case CARBONXSPI4_OP_SET_CAL_MAXB1 :
    data = (SInt32 *)transReq->mData;
    carbonXSpi4SrcSetCalMaxB1(transReq->mAddress, *data);
    carbonXSetTransaction(transResp, transReq->mTransId, TBP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
    break;
  case CARBONXSPI4_OP_SET_CAL_MAXB2 :
    data = (SInt32 *)transReq->mData;
    carbonXSpi4SrcSetCalMaxB2(transReq->mAddress, *data);
    carbonXSetTransaction(transResp, transReq->mTransId, TBP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
    break;
  case CARBONXSPI4_OP_SET_DATAMAXT :
    carbonXSpi4SrcSetDataMaxT(transReq->mAddress);
    carbonXSetTransaction(transResp, transReq->mTransId, TBP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
    break;
  case CARBONXSPI4_OP_SET_FIFOMAXT :
    carbonXSpi4SrcSetFifoMaxT(transReq->mAddress);
    carbonXSetTransaction(transResp, transReq->mTransId, TBP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
    break;
  default :
    carbonXSetTransaction(transResp, transReq->mTransId, TBP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
  }
}
    
