//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXSpi4Snk.h
 *
 * This file contains prototypes and address map for the C-API of the SPI-4.2
 *   Sink Transactor
 *
 * Author  : John Hoglund
 *
 * Created : Thu Sep 9 2005
 *
 *****************************************************************************
 */

#ifndef CARBONXSPI4SNK_H
#define CARBONXSPI4SNK_H

#ifdef __cplusplus
extern "C" {
#endif

// Function: carbonXSpi4SnkStartup
// Description: Configuration and sychronization function for the Sink
//              transactor.  The transactor is first initialized and then
//              all the parameters are set as requested.  The function then
//              reads data off the bus and looks for (gammaValid) number
//              of training sequences.  When the training sequences are
//              received, the loss of sync signal is set to zero in the
//              V-side.  This causes the V-side to begin sending 
//              calendar data to the source transactor.  
//              This function must be called at the beginning of an attached
//              function.
// Parameters: s_int32 calLen -- Calendar length for sink transactor
//             s_int32 calM -- Calendar M for the sink transactor
//             s_int32 dataMaxT -- DATA_MAX_T for the sink transactor
//             s_int32 alpha -- ALPHA for the sink transactor
//             s_int32 gammaValid -- number of valid training sequences that must
//                                   be seen before the sink transactor indicates
//                                   that it is in sync.
//             s_int32 gammaInvalid -- number of failing DIP-4 entries that must
//                                     be received before the sink transactor
//                                     indicates it has lost sync.
// Return: Zero return indicates successful startup.  -1 return indicates an error
//         during startup.
s_int32 carbonXSpi4SnkStartup ( s_int32 calLen,
			     s_int32 calM,
			     s_int32 dataMaxT,
			     s_int32 alpha,
			     s_int32 gammaValid,
			     s_int32 gammaInvalid );

// Function: carbonXSpi4SnkPkt
// Description: Receives the next packet (or partial packet) from the bus.
//              Checks that valid training sequences arrive at the proper rate.
//              Sequences must come no more than DATA_MAX_T cycles apart.
//              Idle cycles are ignored.  If a series of idle cycles of more
//              than the idle timeout is received, SnkPkt returns with size=0 and
//              the return value is the number of cycles during these idles.
//              This is done to stop a deadlock situation if the transactor is 
//              only receiving idles.
//              A partial packet is returned when a control word is received and
//              EOPS is "00" indicating no end of packet.  
// Parameters: u_int8  * pkt -- byte array of packet data that came into the
//                              transactor.
//             s_int32 * size -- As input, the maximum size in bytes of the pkt
//                               array. If the received packet is bigger than 
//                               size, an error is reported.
//                               As output, the size in bytes of the received
//                               packet.
//             s_int32 * sop -- Output of one means start of packet was 
//                              indicated on the preceding control word.
//                              Output of zero means start of packet was not
//                              indicated on the preceding control word.
//             s_int32 * eop -- Output of one means a normal end of packet was 
//                              indicated on the following control word.
//                              Output of zero means normal end of packet was
//                              not indicated on the following control word.
//                              Ouptut of -1 means end of packet abort was
//                              indicated.
//             s_int32 * port -- port from the preceding control word.
// Return: Number of cycles that have elapsed during this transaction.
//         This includes training, idle and packet receive cycles.
s_int32 carbonXSpi4SnkPkt ( u_int8  * pkt,
			 s_int32 * size,
			 s_int32 * sop,
			 s_int32 * eop,
			 s_int32 * port );

// Function Name: carbonXSpi4SnkRaw
// Description:  Receives the next block of control and data words from 
//               the Sink transactor.  A block is defined as one of three
//               combinations of control and data:
//               1)  Payload control word followed by payload data followed
//                   by payload or idle control word
//                   A type 1 block is identified by the previous control
//                   word being a payload control and size > 0.
//               2)  Idle control word followed by training sequences
//                   followed by a payload or idle control word
//                   A type 2 block is identified by the previous control
//                   word being an idle control and size > 0.
//               3)  Idle control word followed by more idle control words
//                   followed by a payload or idle control word that leads
//                   to the beginning of a block of type 1 or 2.
//                   This block only returns a count of the empty idle 
//                   control words in the size field along with the contents
//                   of the first and last idle control words.
//                   A type 3 block is identified by the previous control
//                   word being an idle control and size = 0.  
//               
//   rawCtrlData -- array of memory to be filled with the control and data
//                  words between the previous and following control words.
//   maxSize -- size of the rawCtrlData array
//   numCtrl -- number of rawCtrlData words that had ctl_in set to "1"
//   prevCtrl -- contents of the control word before the rawCtrlData
//   follCtrl -- contents of the control word after the rawCtrlData
//   return -- number of words in rawCtrlData.  return of -1 indicates 
//             an error while receiving
s_int32 carbonXSpi4SnkRaw ( u_int16 * rawCtrlData,
                         s_int32   maxSize,
		         u_int16 * prevCtrl,
                         u_int16 * follCtrl,
		         u_int32 * numCtrl );

// Function: carbonXSpi4SnkIdle
// Description: Passes simulation time with no action taken by the transactor.
//              To be used only with SnkRaw.  All error checking is disabled 
//              during the idle time.  Beware of using this function when using 
//              SnkPkt because the error checking will lose sync between the 
//              C-side and V-side and report incorrect errors.
// Parameters: s_int32 idleCount -- number of bus cycles to run as idle
void carbonXSpi4SnkIdle ( s_int32 idleCount );


// Function: carbonXSpi4SnkGetCal
// Description: Reads the current calendar state from the V-side, fills the
//              calendar array in the per-instance data structure, and 
//              returns a pointer to the calendar array.
//              The user may update the calendar contents as desired and
//              then call carbonXSpi4SnkSetCal to write the contents back
//              to the V-side.
// Return:  Pointer to the array containing the up-to-date calendar contents
u_int32 * carbonXSpi4SnkGetCal(void);

// Function: carbonXSpi4SnkSetCal
// Description: Writes the contents of the per-instance calendar contents
//              to the V-side.
// Return: Status of 0 means the write was successful.  Status of -1 means
//         the write was unsuccessful.
void    carbonXSpi4SnkSetCal(void);


void    carbonXSpi4SnkSetLOS ( u_int32 los );
// SS
s_int32 carbonXSpi4SnkGetLOS ( void );


s_int32 carbonXSpi4SnkGetCalMaxLen ( void );
void    carbonXSpi4SnkSetCalLen ( s_int32 length );
s_int32 carbonXSpi4SnkGetCalLen ( void );

void    carbonXSpi4SnkSetCalM ( s_int32 loops );
s_int32 carbonXSpi4SnkGetCalM ( void );

void    carbonXSpi4SnkSetCalPortMap ( s_int32 calLoc, 
				   s_int32 port );
s_int32 carbonXSpi4SnkGetCalPortMap ( s_int32 calLoc );

void    carbonXSpi4SnkSetCalPortState( s_int32 port, 
				    u_int32 state );
// Function: carbonXSpi4SnkGetCalPortState
// Description: Returns the current state set in the calendar of the requested
//              port.  A return of 0xFFFFFFFF means that the port was not
//              found in the calendar.
u_int32 carbonXSpi4SnkGetCalPortState( s_int32 port );

void    carbonXSpi4SnkSetAlpha ( s_int32 alpha );
s_int32 carbonXSpi4SnkGetAlpha ( void );

void    carbonXSpi4SnkSetDataMaxT ( s_int32 dataMaxT );
s_int32 carbonXSpi4SnkGetDataMaxT ( void );

void    carbonXSpi4SnkSetFifoMaxT ( s_int32 fifoMaxT );
s_int32 carbonXSpi4SnkGetFifoMaxT ( void );

void    carbonXSpi4SnkSetGammaValid ( s_int32 gammaValid );
s_int32 carbonXSpi4SnkGetGammaValid ( void );

void    carbonXSpi4SnkSetGammaInvalid ( s_int32 gammaInvalid );
s_int32 carbonXSpi4SnkGetGammaInvalid ( void );

void    carbonXSpi4SnkInit(void);

void carbonXSpi4SnkSetInsertDip2Errors ( s_int32 insertDip2Err );
s_int32 carbonXSpi4SnkGetInsertDip2Errors ( void );
void carbonXSpi4SnkSetInsertFrameErrors ( s_int32 insertFrameErr );
s_int32 carbonXSpi4SnkGetInsertFrameErrors ( void );
void carbonXSpi4SnkSetInsertTrainErrors ( s_int32 insertTrainErr );
s_int32 carbonXSpi4SnkGetInsertTrainErrors ( void );
u_int32 carbonXSpi4SnkGetTimeout( void );
void carbonXSpi4SnkSetTimeoutVal( u_int32 timeout);

// SS
s_int32 carbonXSpi4SnkGetStatBusMode ( void );

// Function: carbonXSpi4SnkCalcDip4
// Description: Returns the DIP-4 for the array of 16-bit words passed in.
//              Size is the number of 16-bit words, not the number of bytes.
u_int16 carbonXSpi4SnkCalcDip4 ( u_int16 * rawData, 
			      s_int32   size );

// SS
void    carbonXSpi4SnkSetEnableDip4Check ( s_int32 enableDip4Check );
s_int32 carbonXSpi4SnkGetEnableDip4Check ( void );

s_int32 carbonXSpi4SnkGetDip4ErrCnt ( void );

  void carbonXSpi4SnkProcessConf(const struct carbonXTransactionStruct *transReq, struct carbonXTransactionStruct *transResp);

#ifdef __cplusplus
}
#endif

#endif
