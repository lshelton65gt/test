//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXSpi4SrcSupport.h
 *
 * This file contains non-public defines and support routines for the 
 *   Source Transactor
 *
 * Author  : John Hoglund
 *
 * Created : Thu Sep 8 2005
 *
 *****************************************************************************
 */

#ifndef CARBONXSPI4SRCSUPPORT_H
#define CARBONXSPI4SRCSUPPORT_H

#define CARBONXSPI4SRC_STARVING     0
#define CARBONXSPI4SRC_HUNGRY       1
#define CARBONXSPI4SRC_SATISFIED    2
#define CARBONXSPI4SRC_BROKEN       3

#define CARBONXSPI4SRC_EMPTYIDLECTRLWORD 0x000F

#define CARBONXSPI4SRC_MAXCALENDARLENGTH 256
#define CARBONXSPI4SRC_MAXPORTS 256

// CS Write Address Map

#define CARBONXSPI4SRC_CALENDARLENGTH    (0<<2)
#define CARBONXSPI4SRC_CALENDARM         (1<<2)
#define CARBONXSPI4SRC_SHADCALLENGTH     (2<<2)
#define CARBONXSPI4SRC_SHADCALM          (3<<2)
#define CARBONXSPI4SRC_STATTRAINREPS     (4<<2)
#define CARBONXSPI4SRC_FIFOMAXT          (5<<2)
#define CARBONXSPI4SRC_BPSCW             (6<<2)
#define CARBONXSPI4SRC_DELTAVALID        (7<<2)
#define CARBONXSPI4SRC_DELTAINVALID      (8<<2)
#define CARBONXSPI4SRC_ERRORCONTROL      (9<<2)
#define CARBONXSPI4SRC_RESET             (10<<2)

#define CARBONXSPI4SRC_CALENDARBASE      ((0<<2)+256)
#define CARBONXSPI4SRC_CALCOUNT          ((16<<2)+256)
#define CARBONXSPI4SRC_LOS               ((17<<2)+256)
#define CARBONXSPI4SRC_HDLERRORS         ((18<<2)+256)
#define CARBONXSPI4SRC_STATUSBUSMODE     ((19<<2)+256)


typedef struct {
  s_int32     calendarLength;
  s_int32     shadowCalLength;
  s_int32     calendarM;
  s_int32     shadowCalM;
  s_int32     alpha;
  s_int32     dataMaxT;
  s_int32     fifoMaxT;
  u_int32     calendarState[CARBONXSPI4SRC_MAXCALENDARLENGTH];
  u_int32     shadowCalState[CARBONXSPI4SRC_MAXCALENDARLENGTH];
  s_int32     calendarPort[CARBONXSPI4SRC_MAXCALENDARLENGTH];
  s_int32     shadowCalPort[CARBONXSPI4SRC_MAXCALENDARLENGTH];
  u_int32     burstSize1[CARBONXSPI4SRC_MAXPORTS];
  u_int32     burstSize2[CARBONXSPI4SRC_MAXPORTS];
  u_int32     credits[CARBONXSPI4SRC_MAXPORTS];
  u_int16     calLatestUpdate[CARBONXSPI4SRC_MAXPORTS];
  u_int16     calTotalFrames[CARBONXSPI4SRC_MAXPORTS];
  u_int32     deltaValid;
  u_int32     deltaInvalid;
  u_int32     gammaValid;
  u_int32     gammaInvalid;
  u_int32     bpscwConfig;
  u_int32     trainSeqLen;
  u_int32     biterr;
  u_int32     shortSeq;
  s_int32     enableStatDip2Errors;
  s_int32     enableStatTrainErrors;
  s_int32     enableStatFrameErrors;
  s_int32     eopAbort;
  s_int32     dip4ErrorInsert;
  s_int32     dip4ErrorMask;
  s_int32     trainCycleCount;
  s_int32     sopCycleCount;
  u_int32     eopsForNextCtrlWord;
  u_int16     dip16OfPreviousData;
} CARBONXSPI4SRC_INST_DATA_T;

CARBONXSPI4SRC_INST_DATA_T * carbonXSpi4SrcGetInstData (void);
void carbonXSpi4SrcSetInstData (CARBONXSPI4SRC_INST_DATA_T * ptr);

void carbonXSpi4SrcUpdatePortCredits(s_int32 port);

#define CARBONXSPI4SRC_IDLECTRLFLAG       0x0
#define CARBONXSPI4SRC_PAYLOADCTRLFLAG    0x1
#define CARBONXSPI4SRC_NOTSOPCTRLFLAG     0x0
#define CARBONXSPI4SRC_SOPCTRLFLAG        0x1

u_int16 carbonXSpi4SrcBuildCtrlWord ( u_int32 idlePayload, 
				   u_int32 eops,
				   u_int32 sop,
				   u_int32 port,
				   u_int16 dip16OfPreviousData );

void    carbonXSpi4SrcSetShadCalLen ( s_int32 length );
s_int32 carbonXSpi4SrcGetShadCalLen ( void );
void    carbonXSpi4SrcSetShadCalM ( s_int32 loops );
s_int32 carbonXSpi4SrcGetShadCalM ( void );
void    carbonXSpi4SrcSetShadCalPortMap ( s_int32 calLoc, 
				       s_int32 port );
s_int32 carbonXSpi4SrcGetShadCalPortMap ( s_int32 calLoc );

#endif
