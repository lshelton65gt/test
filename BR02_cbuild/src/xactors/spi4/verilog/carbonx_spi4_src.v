//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


/*********************************************************************
 ** SPI4/5 TX  Transactor 
 ** This transactor is a BFM for a SPI4/5 transactor, injecting data 
 ** calculating DIP as needed, accounting for IPG 
 ** injecting tranining sequence, collecting status information 
 ** Processing some CS writes, to write to some transactor registers 
 ** Similarly doing some CS reads 
 **********************************************************************/ 

`timescale 1 ps / 1 ps

/**************************************
 *  Module specific definitions       *
 **************************************/
module carbonx_spi4_src
  (
   reset_n,              // reset port
   sclk_in,              // tx status clock
   stat_in,              // tx status 
   dclk_out,             // tx data clock 
   dat_out,              // tx data bus
   ctl_out               // tx control signal
   );
  
   /************************************
    ** Input/output/Inout Declarations **
    ************************************/
	 
   input 		     reset_n;         // reset input port
   input 		     sclk_in;         // tx status clock
   input [1:0] 	 stat_in;         // tx status bits
   
   output 		   dclk_out;        // tx data clock
   output 		   ctl_out;         // tx control bit
   output [15:0] dat_out;         // tx data bus
   
   /*************************************
    ** Wires / Regs defined for output **
    ************************************/
   reg 			     dclk_out;
   reg 			     tctl;
   reg [15:0] 	 tdat;
   wire 		     tsclk;
   wire [1:0] 	 tstat;
   

   parameter         lvds = 1'b0;     // 1 - LVDS mode, 0 - LVTTL (default)
	 
`protect
// carbon license crbn_vsp_exec_xtor_spi4

/**********************************************************
 ** Title      : SPI 4/5 Tx Transactor                   **
 ** Created    : November 21st, 2001        	         **
 ** Author     : Harish Chawla                		 **
 ** References : OIF SPI-4 June 2000 Draft               **
 **               OIF 2001 SPI-5 Draft 2.0	       	 **
 ** This module is for the SPI 4/5 Tx Bus interface      **
 **                                                      **
 **********************************************************/


   // Clock generators
   // dclk is 2X dclk_out
   // sclk is dclk or dclk/8
   
   // in LVDS mode:  dclk_out = dclk/2; sclk_in = dclk/2 = dclk_out
   // in LVTTL mode: dclk_out = dclk/2; sclk_in = dclk/8 = dclk_out/4
   
`ifdef CARBONX_NOT_RUNNING_CARBON
   parameter 	     DFREQ        =  625.0;                    // default - 625 MHz 
   parameter 	     DCLK_HPERIOD =  1000000.0/(2.0*DFREQ);    // default - 800 ps (625 MHz)
   
   reg 		     dclk; 			
   initial dclk      = 1'b0;			
   always begin
      #(DCLK_HPERIOD) dclk = !dclk;
   end	 	
`else
`endprotect
   wire 	     dclk;	  // carbon scDepositSignal
`protect
`endif
	 
   /**************************************************
    ** Generate data clock 
    **************************************************/
   
   initial dclk_out = 1'b0;
   always @(posedge dclk) begin
      dclk_out <= !dclk_out;
   end
	 
	 
   /*****************************************
    ** XIF Core Parameters                 **
    *****************************************/
   parameter 		     BFMid     = 1;
   parameter 		     ClockId   = 1;
   parameter 		     DataWidth = 32;
   parameter 		     DataAdWid = 11;	   // Max is 15
   parameter 		     GCSwid    = 32*11;  // Max is 256 bytes
   parameter 		     PCSwid    = 32*20;  // Max is 256 bytes
   
   /*****************************************
    ** Definitions for constants go here   **
    *****************************************/
   parameter 		     MAX_STATUS_LEN = 512; // 256 calendar elemants, each 2 bits
   parameter 		     lodsReps = 8;
   parameter 		     AddrWord = 0;
   parameter 		     DataWord = 1;
   
   
   /*****************************************
    * State Parameters
    *****************************************/
   parameter 		     IDLE          = 0;
   parameter 		     SEND_SEGMENT  = 1;
   
`include "xif_core.vh"   // Use the transactor template
   
   /*************************************
    ** Variables needed for transactor **
    ************************************/
   reg 			     xrun;
   
   wire 		     reset;
   wire 		     TBP_reset;
   wire 		     dclk_int;
   
   reg 			     lossOfSync;
   wire [MAX_STATUS_LEN-1:0] status;
   
   reg [MAX_STATUS_LEN-1:0]  current_status;
   
   reg [1:0] 		     statDIP2;
   wire [1:0] 		     bpscw;
   wire 		     bpscw_enable;
   wire 		     bpscw_changed;
   wire			     bpscw_next;
   
   wire			     sw_reset;

   // Config Space registers
   wire [31:0] 		     calendar_len;
   wire [31:0] 		     calendar_m;
   wire [31:0] 		     bkup_calendar_len;
   wire [31:0] 		     bkup_calendar_m;
   wire [31:0] 		     stat_train_reps;
   wire [31:0] 		     train_reps;
   wire [31:0] 		     train_seq_len;
   wire [31:0] 		     fifo_max_t;
   wire [31:0] 		     data_max_t;
   wire [31:0] 		     deltaValid;
   wire [31:0] 		     deltaInvalid;
   wire 		     dip2_error_en;
   wire 		     train_error_en;
   wire 		     frame_error_en;
   wire 		     short_transfer;

   reg [31:0] 		     deltaValidCount;
   reg [31:0] 		     deltaInValidCount;

   // Error Counts
   reg [7:0] 		     dip2ErrorCount;
   reg [7:0] 		     frameErrorCount;
   reg [7:0] 		     trainErrorCount;
      
  /*************************************
   ** XIF_core interface
   ************************************/
   assign 			   BFM_xrun           = xrun; 
   assign 			   BFM_clock          = dclk; 
   assign 			   BFM_put_operation  = BFM_NXTREQ;
   assign 			   BFM_interrupt      = 0;
   assign 			   BFM_put_status     = 0;
   assign 			   BFM_put_size       = 0;
   assign 			   BFM_put_address    = 0;
   assign 			   BFM_put_data       = 0;
   assign 			   BFM_put_cphwtosw   = 0; 
   assign 			   BFM_put_cpswtohw   = 0; 
   assign 			   BFM_put_dwe        = 0;
   assign 			   BFM_reset          = reset;
  
  /*********************************
   ** Continuous assignments       **
   *********************************/ 
  assign 			   reset = !reset_n | sw_reset;
  assign 			   TBP_reset = reset;
  
  assign 			   dclk_int = dclk;
  
  /* *******************************************************
   **  process : TX Statemachine
   **  purpose : To control Tdat and Tctl
   **  inputs  : 
   **  output  : 
   ** *****************************************************/
   // R0 Signals
   reg [31:0]  data_cnt;
   reg 	       trans_in_progress;
   wire [31:0] segment_words;

   // R1 Signals
   reg [31:0]  data_cnt_r1;
   reg [31:0]  get_address_r1;
   wire [15:0] curr_data_word_r1;
   
   // Get current data_word
   assign      BFM_gp_daddr = data_cnt[31:1];
   assign      curr_data_word_r1 = data_cnt_r1[0] ? XIF_get_data[31:16] : XIF_get_data[15:0 ];
   assign      segment_words     = (XIF_get_size+1)/2;
   
   always @(posedge dclk_int or posedge reset) begin
      
      if(reset == 1) begin
	 xrun     <= 0;
	 tdat     <= 16'h000f;
	 tctl     <= 1;
	 
	 // Signals internal to this FSM
	 data_cnt             <= 0;
	 data_cnt_r1          <= 0;
	 trans_in_progress    <= 0;
      end
      else begin

	 // Delay signals for r1 pipeline stage
	 data_cnt_r1          <= data_cnt;
	 get_address_r1       <= XIF_get_address;
	 
	 // Start Transaction
	 if(XIF_xav && XIF_get_operation == BFM_WRITE) begin
	    trans_in_progress <= 1'b1;

	    // Only assert xrun if transaction is larger that 1 cycle.
	    if(segment_words > 1) begin
	       xrun     <= 1'b1;
	       data_cnt <= 1;
	    end
	    
	    else begin
	       xrun     <= 1'b0;
	       data_cnt <= 0;
	    end
	    
	 end
	 else if(trans_in_progress) begin

	    // Turn xrun off at the last cycle
	    if(data_cnt >= (segment_words-1)) begin
	       xrun     <= 1'b0;
	       data_cnt <= 1'b0;
	    end
	    else if(data_cnt != 0)
	      data_cnt <= data_cnt +1;

	    // Deassert trans_in_progress when transaction is sent
	    if(data_cnt == 0)
	      trans_in_progress <= 1'b0;

	 end

	 // Output Data when transaction is in progress
	 if(trans_in_progress) begin
	    tdat <= curr_data_word_r1;
	    if(data_cnt_r1 < get_address_r1)
	      tctl <= 1'b1;
	    else 
	      tctl <= 1'b0;
	 end

	 // Otherwise output IDLE's
	 else begin
	    tdat <= 16'h000f;
	    tctl <= 1'b1;
	 end
	 
      end // else: !if(reset)
   
   end // always @ (posedge dclk_int or posedge reset)
   
  /* *******************************************************
   **  process : Status Clock
   **  purpose : In order to be able to use the same
   **            state machine for LVDS and TTL mode, we will
   **            devide the clock comming in when in TTL mode  
   **  inputs  : sclk_in
   **  output  : tsclk
   ** *****************************************************/
   reg  tsclk_div;
   
   initial                    tsclk_div <= 1'b0;
   always @(posedge sclk_in) tsclk_div <= !tsclk;

   assign tsclk = lvds ? sclk_in : tsclk_div;
   
   
  /* *******************************************************
   **  process : Status Capture
   **  purpose : To clock in negative and positive status 
   **            data, and to concatinate them into a 4 bit
   **            status word. Also pipeline twice to enable
   **            state machine to look at both previous and
   **            folling status words on the bus
   **  inputs  : tsclk, tstat
   **  output  : tsdat_c0, tstat_c1, tstat_c2
   ** *****************************************************/
   reg [1:0] tstat_neg_r1, tstat_pos_r1;
   reg [3:0] tstat_c0, tstat_c1, tstat_c2;
   
   // Clock in Negativ edge words
   always @(negedge tsclk)
     tstat_neg_r1 <= stat_in;
   
   // Clock in Positiv edge words, concatinate and pipeline
   always @(posedge tsclk) begin
      // Clock positiv edge words
      tstat_pos_r1 <= stat_in;

      // Concatinate the two edges into one word.
      tstat_c0 <= {tstat_neg_r1, tstat_pos_r1};

      // Pipeline signal
      tstat_c1 <= tstat_c0;
      tstat_c2 <= tstat_c1;

   end // always @ (posedge tsclk)

   /********************************************************
    **  process : Loss of Sync
    **  purpose : To detect loss of sync based on number
    **            of valid or invalid DIP2's received
    **  inputs  : reset, tsclk, deltaValidCount, deltaValid,
    **            deltaInValidCount and deltaInvalid
    **  output  : status, stat_count
    ** *****************************************************/
   always @(posedge tsclk or posedge reset)
     if(reset) begin
	lossOfSync        <= 1;
     end
     else begin
	if(deltaValidCount >= deltaValid) begin
	   lossOfSync <= 0;
	end
	if(deltaInValidCount >= deltaInvalid) begin
	   lossOfSync <= 1;
	end
     end // else: !if(reset)
   
   /********************************************************
    **  process : Training Sequence Detector
    **  purpose : To detect a training sequence on the status
    *             bus
    **            of valid or invalid DIP2's received
    **  inputs  : reset, tsclk, tstat_c0,
    **
    **  output  : stat_train_det
    ** *****************************************************/
`endprotect
   wire [1:0]  stat_train_det;  // carbon observeSignal
`protect
   reg [43:0]  stat_sr;

   // Store Status In Shift Register
   always @(posedge tsclk)
     stat_sr <= ({40'd0, tstat_neg_r1, tstat_pos_r1} << 40) | {4'd0, stat_sr[43:4]};

   // Look For Training Sequence
   assign      stat_train_det[0] = (stat_sr[39:0] == 40'hfffff_00000);
   assign      stat_train_det[1] = (stat_sr[41:2] == 40'hfffff_00000);

   
   /* *******************************************************
   **  process : StatCollect
   **  purpose : to collect status to be sent to the c-side
   **            Frame to the incoming data, to determine
   **            start of the calendar, and then continue reading
   **            status information from bus.
   **            Store the status information in the register array
   **            to be read via CSRead.
   **            Verify status training sequence integrity/validity
   **            used for LVDS and LVTTL mode.
   **  inputs  : reset, tsclk, tstat
   **  output  : status, stat_count
   ** *****************************************************/
   parameter START            = 0;
   parameter TTL_TRAIN        = 1;
   parameter LVDS_TRAIN_ONES  = 2;
   parameter LVDS_TRAIN_ZEROS = 3;
   parameter FRAMING          = 4;
   parameter BPSCW            = 5;
   parameter PORT_STAT        = 6;
   parameter STAT_DIP2        = 7;
   parameter DIP2_FRAME       = 8;

   reg [31:0] 		     stat_count, stat_count_r1;
   reg [31:0] 		     cal_reps;
   reg [3:0] 		     stat_state;
   reg [5:0] 		     stat_train_cnt;
   wire [31:0] 		     calLen = (bpscw_enable && bpscw == 2'b10) ? bkup_calendar_len : calendar_len; 
   wire [31:0] 		     calRep = (bpscw_enable && bpscw == 2'b10) ? bkup_calendar_m   : calendar_m;
   wire [15:0] 		     stat_ptr;
   reg [15:0] 		     tot_cal_repeat;
   reg [31:0] 		     train_timer;
   reg [31:0] 		     train_det_zeros;
   reg [31:0] 		     train_det_ones;
   reg [1:0] 		     train_detected;
   
   // Calculate which was the last status word written, so that the c-side can
   // know what portstatus is the most up to date
   assign 		     stat_ptr = (stat_count == 0) ? calLen-1 : stat_count -1;
		        
   always @(posedge tsclk or posedge reset)
     if(reset) begin
	stat_state        <= START;
	current_status    <= (1 << MAX_STATUS_LEN) -1 ;
	statDIP2          <= 0;
	stat_train_cnt    <= 0;
	stat_count        <= 0;
	cal_reps          <= 0;
	dip2ErrorCount    <= 0;
	frameErrorCount   <= 0;
	trainErrorCount   <= 0;
	deltaInValidCount <= 0;
	deltaValidCount   <= 0;
	tot_cal_repeat    <= 0;
	train_timer       <= 0;
     end
     else begin
		   
	// Check that we get training sequences often enough.
	train_timer <= train_timer + 2;
	if(train_timer >= fifo_max_t && fifo_max_t > 0 && lvds && stat_state != LVDS_TRAIN_ZEROS && !lossOfSync) begin
	   if(train_error_en)
	     $display("ERROR: Transmitter did not send training sequences in time according to fifo_max_t at time %d", $time);
	   else
	     $display("Transmitter did not send training sequences in time according to fifo_max_t at time %d.", $time);
	   trainErrorCount = trainErrorCount + 1;

	   //  Clear Train Timer here so we will not get an error until train_timer gets over fifo_max_t again.
	   train_timer <= 0;
	end

	case (stat_state)
	  START : begin

	     // Clear Train Timer here because we're out of sync and should
	     // not report any training sequence timing errors.
	     train_timer <= 0;

	     // Wait here until we see a start of a training sequence.
	     // Then go to the training state.

	     if(lvds) begin
		// Check for at least 2 "11"'s in a row
		if(tstat_c1 == 4'b0000 && tstat_c0 == 4'b1111) begin
		   stat_state     <= LVDS_TRAIN_ONES;
		   stat_train_cnt <= 0;
		end
		else if((tstat_c1 == 4'b1100 && tstat_c0 == 4'b1111)) begin
		   stat_state     <= LVDS_TRAIN_ONES;
		   stat_train_cnt <= 1;
		end
	     end // if (lvds)

	     else begin
		// Check for at least 2 "11"'s in a row
		if(tstat_c0 == 4'b1111) begin
		   stat_state     <= TTL_TRAIN;
		   stat_train_cnt <= 0;
		end
		else if((tstat_c1[3:2] == 2'b11 && tstat_c0[1:0] == 2'b11)) begin
		stat_state     <= TTL_TRAIN;
		   stat_train_cnt <= 1;
		end
	     end // else: !if(lvds)
	     
	     
	  end
	  
	  LVDS_TRAIN_ZEROS : begin

	     // Count the number of training words
	     stat_train_cnt <= stat_train_cnt + 2;

	     // Keep the train Counter at 0 during training sequence
	     train_timer <= 0;
	     
	     if(stat_train_cnt == 9) begin
		if(tstat_c1 != 4'b1100 && !lossOfSync) begin
		   if(train_error_en)
		     $display("ERROR: Incorrect training sequence detected at time %d.", $time);
		   else 
		     $display("Incorrect training sequence detected at time %d.", $time);
		   trainErrorCount = trainErrorCount + 1;
		end
		stat_state     <= LVDS_TRAIN_ONES;
		stat_train_cnt <= 1;
	     end

	     else if(stat_train_cnt == 8) begin
		if(tstat_c1 != 4'b0000 && !lossOfSync) begin
		   if(train_error_en)
		     $display("ERROR: Incorrect training sequence detected at time %d.", $time);
		   else 
		     $display("Incorrect training sequence detected at time %d.", $time);
		   trainErrorCount = trainErrorCount + 1;
		end
		stat_state     <= LVDS_TRAIN_ONES;
		stat_train_cnt <= 0;
	     end

	     else begin
		if(tstat_c1 != 4'b0000 && !lossOfSync) begin
		   if(train_error_en)
		     $display("ERROR: Incorrect training sequence detected at time %d.", $time);
		   else 
		     $display("Incorrect training sequence detected at time %d.", $time);
		   trainErrorCount = trainErrorCount + 1;
		end
	     end // else: !if(stat_train_cnt == 8)
	     
	  end // case: LVDS_TRAIN_ZEROS
	     
	  LVDS_TRAIN_ONES : begin

	     // Count the number of training words
	     stat_train_cnt <= stat_train_cnt + 2;
	     
	     // Keep the train Counter at 0 during training sequence
	     train_timer <= 0;

	     if(stat_train_cnt == 9) begin
		if(tstat_c1[1:0] != 4'b11 && !lossOfSync) begin
		   if(train_error_en)
		     $display("ERROR: Incorrect training sequence detected at time %d.", $time);
		   else 
		     $display("Incorrect training sequence detected at time %d.", $time);
		   trainErrorCount = trainErrorCount + 1;
		end

		// If the next word is 0, we're getting into a new training sequence
		if(tstat_c1[3:2] == 2'b00) begin
		   stat_state     <= LVDS_TRAIN_ZEROS;
		   stat_train_cnt <= 1;
		end

		// If the next word is 11 we're getting a framing signal.
		else if(tstat_c1[3:2] == 2'b11) begin
		   stat_state     <= PORT_STAT;
		   stat_count     <= 0;
		   statDIP2       <= 0;
		   train_timer    <= 1;
		end

		// Otherwise it's framing ERROR
		else begin
		   if(!lossOfSync) begin
		      if(frame_error_en)
			$display("ERROR: Wrong Framing signal detected at time %d.", $time);
		      else
			$display("Wrong Framing Signal detected at time: %d.", $time);
		      frameErrorCount = frameErrorCount + 1;
		   end
		end // else: !if(tstat_c1[3:2] == 2'b11)
	     end // if (stat_train_cnt == 9)

	     else if(stat_train_cnt == 8) begin

		// Check for Sequence Errors
		if(tstat_c1 != 4'b1111 && !lossOfSync) begin
		   if(train_error_en)
		     $display("ERROR: Incorrect training sequence detected at time %d.", $time);
		   else 
		     $display("Incorrect training sequence detected at time %d.", $time);
		   trainErrorCount = trainErrorCount + 1;
		end
 		
		if(tstat_c0[1:0] == 2'b00) begin
		   stat_state     <= LVDS_TRAIN_ZEROS;
		   stat_train_cnt <= 0;
		end
		else begin
		   stat_state  <= FRAMING;
		   train_timer <= 0;
		end
	     end

	     else begin
		if(tstat_c1 != 4'b1111 && !lossOfSync) begin
		   if(train_error_en)
		     $display("ERROR: Incorrect training sequence detected at time %d.", $time);
		   else 
		     $display("Incorrect training sequence detected at time %d.", $time);
		   trainErrorCount = trainErrorCount + 1;
		end
	     end // else: !if(stat_train_cnt == 8)
	     
	  end // case: LVDS_TRAIN_ONES

	  TTL_TRAIN : begin

	     // Look for non "11" words, then goto the PORT_STAT_STATE

	     // If LSB is non "11" then the LSB is the first status word.
	     // Capture the status word, set stat_count appropreiately and
	     // goto the PORT_STAT state
	     if(tstat_c1[1:0] != 2'b11) begin
		current_status[3:0] <= tstat_c1[3:0];
		statDIP2            <= DIP2(DIP2(0, tstat_c1[1:0]), tstat_c1[3:2]);
		stat_count          <= 2;
		stat_state          <= PORT_STAT;
	     end

	     // If MSB is non "11" then clear the stat_count and goto PORT_STAT state.	
	     else if(tstat_c1[3:2] != 2'b11) begin
		current_status[1:0] <= tstat_c1[3:2];
		statDIP2            <= DIP2(0, tstat_c1[3:2]);
		stat_count          <= 1;
		stat_state          <= PORT_STAT;
	     end
     
	  end
	  
	  PORT_STAT : begin
		
	     // Check for Framing Pulse or Disabled
	     if(tstat_c1[1:0] == 2'b11) begin

		// If two or more framing pulses in a row, Status bus is disabled
		// Cancel all credits (set all status to 11) and goto the Training state
		if(tstat_c1[3:2] == 2'b11)  begin
		   current_status    <= -1;
		   stat_state        <= START;
		   deltaValidCount   <= 0;
		   deltaInValidCount <= 0;
		end

		// If not, this is an actual Framing signal that's out of order, flag an error
		else begin
		   if(frame_error_en)
		     $display("ERROR: Framing Signal Detected out of order at time %d.", $time);
		   else
		     $display("Framing Signal Detected out of order at time: %d.", $time);
		   frameErrorCount = frameErrorCount + 1;
		end // else: !if(tstat_c1[3:2] == 2'b11)
	     end // if (tstat_c1[1:0] == 2'b11)
	     
	     // Check second word  
	     else if(tstat_c1[3:2] == 2'b11) begin

		// If two or more framing pulses in a row, Status bus is disabled
		// Cancel all credits (set all status to 11) and goto the Training state
		if(tstat_c2[1:0] == 2'b11)  begin
		   current_status    <= -1;
		   stat_state        <= lvds ? START : TTL_TRAIN;
		   deltaValidCount   <= 0;
		   deltaInValidCount <= 0;
		end

		// If not, this is an actual Framing signal that's out of order, flag an error
		else begin
		   if(frame_error_en)
		     $display("ERROR: Framing Signal Detected out of order at time %d.", $time);
		   else
		     $display("Framing Signal Detected out of order at time: %d.", $time);
		   frameErrorCount = frameErrorCount + 1;
		end // else: !if(tstat_c2[1:0] == 2'b11)
	     end // if (tstat_c1[3:2] == 2'b11)

	     // If no Framing signal detected, normal operation.
	     else begin
		
		// Update current_status
		current_status <= (current_status & StatusMask(stat_count, 4)) | (tstat_c1 << stat_count*2);
		statDIP2       <= DIP2(DIP2(statDIP2, tstat_c1[1:0]), tstat_c1[3:2]);
		
		if(stat_count >= (calLen-1)) begin
		   stat_count  <= stat_count - calLen + 2;
		   cal_reps    <= cal_reps + 1;
		end
		else begin
		   stat_count     <= stat_count + 2;
		end
		
		// If stat_count is two less than CalLen
		// The current status words are both being stored
		// but the stat_count has to be updated for the next repeat
		// If the calendar, unless calRep is reached, then goto
		// the DIP2 state
		if(stat_count == (calLen-3)) begin
		   if(cal_reps == (calRep-1)) begin
		      stat_state <= STAT_DIP2;
		      cal_reps   <= 0;
		   end
		end
		else if(stat_count == (calLen-2)) begin
		   if(cal_reps == (calRep-1)) begin
		      stat_state <= DIP2_FRAME;
		      cal_reps   <= 0;
		   end
		end
	     
	     end // else: !if(tstat_c1[3:2] == 2'b11)
	     
	  end // case: PORT_STAT

	  STAT_DIP2 : begin

	     // Save off the last status info
	     current_status <= (current_status & StatusMask(stat_count, 2)) | (tstat_c1[1:0] << stat_count*2);
	     statDIP2       <= DIP2(statDIP2, tstat_c1[1:0]);
	     stat_count     <= stat_count + 1;
	     
	     //$display("%m Checked DIP2: %d", tstat_c1[3:2]);

	     // Check DIP2
	     if(tstat_c1[3:2] != DIP2(DIP2(statDIP2, tstat_c1[1:0]), 2'b11) && !lossOfSync) begin
		if(dip2_error_en)
		  $display("ERROR: Incorrect DIP2 found at time %d.", $time);
		else
		  $display("Incorrect DIP2 found at time %d.", $time);
		$display("Expected DIP2: %h, Actual DIP2: %h", 
			 DIP2(DIP2(tstat_c1[1:0], statDIP2), 2'b11), tstat_c1[3:2]);

		dip2ErrorCount    <= dip2ErrorCount + 1;
		deltaInValidCount <= deltaInValidCount + 1;
		deltaValidCount   <= 0;
	     end // if (tstat_c1[3:2] != DIP2(DIP2(tstat_c1[1:0], statDIP2), 2'b11))
	     else begin
		deltaValidCount   <= deltaValidCount + 1;
		deltaInValidCount <= 0;
	     end // else: !if(tstat_c1[3:2] != DIP2(DIP2(tstat_c1[1:0], statDIP2), 2'b11))

	     // Check if next word is framing or possible training.
	     if(tstat_c0[1:0] == 2'b00 && lvds) begin
		stat_state     <= LVDS_TRAIN_ZEROS;
		stat_train_cnt <= 0;
	     end
	     else stat_state <= FRAMING;
	     
	  end // case: STAT_DIP2

	  DIP2_FRAME : begin

	     //$display("%m Checked DIP2: %d", tstat_c1[1:0]);
	     
	     // Check DIP2
	     if(tstat_c1[1:0] != DIP2(statDIP2, 2'b11) && !lossOfSync) begin
		if(dip2_error_en)
		  $display("ERROR: Incorrect DIP2 found at time %d.", $time);
		else
		  $display("Incorrect DIP2 found at time %d.", $time);
		$display("Expected DIP2: %h, Actual DIP2: %h", DIP2(statDIP2, 2'b11), tstat_c1[1:0]);

		dip2ErrorCount    <= dip2ErrorCount    + 1;
		deltaInValidCount <= deltaInValidCount + 1;
		deltaValidCount   <= 0;
	     end
	     else begin
		deltaValidCount   <= deltaValidCount + 1;
		deltaInValidCount <= 0;
	     end // else: !if(tstat_c1[1:0] != DIP2(statDIP2, 2'b11))

	     // This could be the start of a training sequence
	     if(lvds && tstat_c1[3:2] == 2'b00) begin
		stat_state     <= LVDS_TRAIN_ZEROS;
		stat_train_cnt <= 1;
	     end
	     
	     // Check that the next word is a framing signal
	     else if(tstat_c1[3:2] != 2'b11 && !lossOfSync) begin
		if(frame_error_en)
		  $display("ERROR: Expected Framing signal at time %d.", $time);
		else
		  $display("Expected Framing signal at time %d.", $time);
		$display("Expected 11 found %b", tstat_c1[3:2]);
		frameErrorCount = frameErrorCount + 1;

		// If next word is a 11 as well, it's a training word.
		// Goto the training sequence.
		if(tstat_c0[1:0] == 2'b11) begin
		   stat_train_cnt <= 0;
		   stat_state     <= lvds ? START : TTL_TRAIN;
		end
		
	     end

	     else begin
		stat_count <= 0;
		stat_state <= PORT_STAT;
		statDIP2   <= 0;
		
	     end // if (tstat_c1[3:2] != 2'b11)


	     // Update Total Repeat Count
	     tot_cal_repeat <= tot_cal_repeat + 1;

	  end // case: DIP2_FRAME

	  FRAMING : begin
	     
	     // Check that it is actually a framing signal
	     if(tstat_c1[1:0] != 2'b11 && !lossOfSync) begin
		if(frame_error_en)
		  $display("ERROR: Expected Framing signal at time %d.", $time);
		else
		  $display("Expected Framing signal at time %d.", $time);
		$display("Expected 11 found %b", tstat_c1[1:0]);
		frameErrorCount = frameErrorCount + 1;
	     end

	     // If next word is a 11 as well, it's a training word.
	     // Goto the training sequence.
	     if(tstat_c0[3:2] == 2'b11) begin
		stat_train_cnt <= 1;
		stat_state     <= lvds ? LVDS_TRAIN_ONES : TTL_TRAIN;
	     end
	     else begin
		current_status[1:0] <= tstat_c1[3:2];
		statDIP2            <= DIP2(0, tstat_c1[3:2]);
		stat_count          <= 1;
		stat_state          <= PORT_STAT;
	     end

	     // Update Total Repeat Count
	     tot_cal_repeat <= tot_cal_repeat + 1;

	  end // case: FRAMING
	  
	endcase // case(stat_state)

	
	// Detect Training Sequence in LVDS mode
	train_detected <= 0;
	if(lvds) begin
	   if(tstat_c0[3:2] == 2'b00 && tstat_c0[1:0] != 2'b00) train_det_zeros <= 1;
	   else if(tstat_c0 == 4'b1100) train_det_zeros    <= train_det_zeros + 1;
	   else if(tstat_c0 == 4'b0000) train_det_zeros    <= train_det_zeros + 2;
	   else if(tstat_c0 != 4'b1111) train_det_zeros    <= 0;
	   
	   if(tstat_c0 == 4'b1100)         train_det_ones   <= train_det_ones <= 1;
	   else if(tstat_c0 == 4'b1111)    train_det_ones   <= train_det_ones + 2;
	   else if(tstat_c0[1:0] == 2'b11) train_det_ones   <= train_det_ones + 1;
	   else                            train_det_ones   <= 0;
	   
	   if(train_det_ones == 9 && tstat_c0[1:0] == 2'b11 && train_det_zeros == 10) begin
	      train_detected <= 1;
	   end
	   else if(train_det_ones == 8 && tstat_c0 == 4'b1111 && train_det_zeros == 10) begin
	      train_detected <= 2;
	   end
	   
	   // Check for Training Sequence detected
	   if(stat_train_det == 1) begin
	      if(tstat_c0[1:0] == 2'b00) begin
		 stat_state     <= LVDS_TRAIN_ZEROS;
		 stat_train_cnt <= 0;
	      end
	      else begin
		 stat_state <= FRAMING;
	      end
	   end
	   else if(stat_train_det == 2) begin
	      if(tstat_c0[3:2] == 2'b00) begin
		 stat_state     <= LVDS_TRAIN_ZEROS;
		 stat_train_cnt <= 1;
	      end
	      else begin
		 stat_state     <= LVDS_TRAIN_ONES;
		 stat_train_cnt <= 9;
	      end
	   end
	   
	end // if (lvds)

     end // else: !if(reset)
      

  assign ctl_out  = tctl;
  assign dat_out  = tdat;
  
  /**************************************************
   ** Fifo Status bus
   **************************************************/
  assign tstat  = stat_in;

    /*******************************************************
   ** Function to calculate a new DIP 2 from 2 DIP values
   *******************************************************/      
  function [1:0] DIP2;
    input [1:0] oDIP2;
    input [1:0] nDIP2;
    
    reg [1:0] rotDIP2;
    
    begin
      rotDIP2 = {oDIP2[0], oDIP2[1]};
      DIP2    = rotDIP2 ^ nDIP2;
       //$display("%m Calculated DIP2 for %h,%h = %h at time: %d", oDIP2, nDIP2, DIP2, $time);
    end
  endfunction // DIP2
  
  /*******************************************************
   ** Function to calculate the mask to put status words
   ** into the status vector.
   *******************************************************/ 
   function [MAX_STATUS_LEN-1:0] StatusMask;
      input [31:0] stat_count;
      input [2:0]  width;
      
      reg [MAX_STATUS_LEN-1:0] mask;

      begin
	 mask     = (width<<2) -1;
	 StatusMask = ~(mask << stat_count*2);
      end
   endfunction // StatusMask
      
  /**********************************************************************
   ** Task to generate and inject Training Sequence to DUT
   ** Transmits training control word for first 1/2 of train_seq_len
   ** period, followed by training data word for the latter 1/2.
   ** Training sequence length is programmable by the
   ** train_seq_len register. 
   ***********************************************************************/
  
  /********************************************
   **  ControlStatus Write Task              **
   **  Update Programmable Register Values   ** 
   *******************************************/
   // Regular Calendar Registers
   assign calendar_len      = XIF_get_csdata[ 31:  0]; // Addr  0
   assign calendar_m        = XIF_get_csdata[ 63: 32]; // Addr  1

   // Backup Calendar Registers
   assign bkup_calendar_len = XIF_get_csdata[ 95: 64]; // Addr  2
   assign bkup_calendar_m   = XIF_get_csdata[127: 96]; // Addr  3

   // Fifo Status Training Parameters
   assign stat_train_reps   = XIF_get_csdata[159:128]; // Addr  4
   assign fifo_max_t        = XIF_get_csdata[191:160]; // Addr  5

   // Backup Calendar Control
   assign bpscw_enable      = XIF_get_csdata[192];     // Addr  6:0
   assign bpscw             = XIF_get_csdata[194:193]; // Addr  6:1-2

   // Status Loss of Sync Control 
   assign deltaValid        = XIF_get_csdata[255:224]; // Addr  7
   assign deltaInvalid      = XIF_get_csdata[287:256]; // Addr  8

   // Error Control
   assign dip2_error_en     = XIF_get_csdata[288];     // Addr  9:0
   assign train_error_en    = XIF_get_csdata[289];     // Addr  9:1
   assign frame_error_en    = XIF_get_csdata[290];     // Addr  9:1

   // Simulation Control
   assign sw_reset          = XIF_get_csdata[312];     // Addr 10:0
   
  
  /*************************************************
   ** CS_Read Task, reading current value of the  **
   ** programmable registers                      **
   *************************************************/
   assign BFM_put_csdata[511:  0] = current_status; // Addr 0-15
   assign BFM_put_csdata[527:512] = stat_ptr;       // Addr 16:0-15
   assign BFM_put_csdata[543:528] = tot_cal_repeat; // Addr 16:0-15
   assign BFM_put_csdata[544]     = lossOfSync;     // Addr 17:0
   assign BFM_put_csdata[575:545] = 0;              
   assign BFM_put_csdata[583:576] = dip2ErrorCount;  // Addr 18: 0- 7
   assign BFM_put_csdata[591:584] = frameErrorCount; // Addr 18: 8-15
   assign BFM_put_csdata[599:592] = trainErrorCount; // Addr 18:16-23
   assign BFM_put_csdata[607:600] = 0;
   assign BFM_put_csdata[608]     = lvds;            // Addr 19:0
   assign BFM_put_csdata[639:609] = 0;

`endprotect
endmodule // carbonx_spi4_src


/* *********************************************************************
 
 ************************************************************************/
