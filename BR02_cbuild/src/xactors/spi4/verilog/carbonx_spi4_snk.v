
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


/**********************************************************
 ** SPI4 RX  Transactor
 ** This transactor is a BFM for a SPI4 interface, 
 ** reading data off the data bus, and sending the data 
 ** to the 'c' side.
 ** DIPs are accounted for, to make the tx/rx compatible
 ** CtrlWord processing is done appropriately, to send to the 'c' side
 ** Training Sequence is also recieved and 
 ** checked for validity and timing.
 **********************************************************/

`timescale 1 ps / 1 ps

/**************************************
 *  Module specific definitions       *
 **************************************/
module carbonx_spi4_snk
  (
   reset_n,                // reset port
   dclk_in,                // rx data clock
   dat_in,                 // rx data bus
   ctl_in,                 // rx control signal
   sclk_out,               // rx status clock
   stat_out                // rx status bits
   );
   
   /************************************
    ** Input/output/Inout Declarations **
    ************************************/
   
   input 		    reset_n;        // reset input port
   input 		    dclk_in;        // rx data clock
   input 		    ctl_in;         // rx control bit
   input [15:0] dat_in;         // rx data bus
   
   output 		  sclk_out;       // rx status clock
   output [1:0] stat_out;       // rx status bits
   
   /*************************************
    ** Wires / Regs defined for output **
    ************************************/
   reg 			          rsclk;
	 
   wire [1:0] 		    rstat;
   wire 		          rctl;
   wire [15:0] 		    rdat;
   
   parameter         lvds = 1'b0;     // 1 - LVDS mode, 0 - LVTTL (default)

 `protect
// carbon license crbn_vsp_exec_xtor_spi4

/**********************************************************
 ** Title     : SPI 4 Rx Transactor                      **
 ** Created   : November 21st, 2001        	         **
 ** Author    : Harish Chawla                		 **
 ** References: OIF SPI-4 June 2000 Draft                **
 **              OIF 2001 SPI-5 Draft 2.0	       	 **
 ** This module is for the SPI 4 Rx Bus interface        **
 **                                                      **
 **********************************************************/

   // Clock generators
   // dclk is 2X dclk_in
   // sclk is dclk or dclk/8
   
   // in LVDS mode:  dclk_in = dclk/2; sclk_out = sclk/2 = dclk/2 = dclk_in
   // in LVTTL mode: dclk_in = dclk/2; sclk_out = sclk   = dclk/8 = dclk_in/4
   
`ifdef CARBONX_NOT_RUNNING_CARBON
   parameter 	     SFREQ        =  625.0/8.0;                // default - 78.1 MHz 	 
   parameter 	     SCLK_HPERIOD =  1000000.0/(2.0*SFREQ);    // default - 6400 ps (78.1 MHz)
   
   reg 		     sclk;	 
   initial sclk      = 1'b0;
   always begin
      #(SCLK_HPERIOD) sclk = !sclk;
   end
   
`else               // Prep
`endprotect   
   wire    sclk;	  // carbon scDepositSignal
   // Carbon forces dclk from cbuild.dir
`protect   
`endif
	 
	 
   /**************************************************
    *	 Generate status clocks: rsclk - lvds, sclk - lvttl 
    **************************************************/
   
   initial rsclk = 1'b0;
   always @(posedge sclk) begin
      rsclk <= !rsclk;
   end
   
   assign sclk_out = lvds ? rsclk : sclk;

   assign stat_out = rstat;
	 
	 
   /*****************************************
    ** XIF Core Parameters                 **
    *****************************************/
   parameter 		    
     BFMid     = 1,
       ClockId   = 1,
       DataWidth = 32,
       DataAdWid = 11,   // Max is 15
       GCSwid    = 32*43,
       PCSwid    = 32*2;
   
   /*****************************************
    ** Definitions for constants go here    **
    *****************************************/
   parameter 		    MAX_STATUS_LEN = 512; //256 calendar elements, each of 2 bits
   parameter 		    MAX_BURST_SIZE = 256;
   parameter 		    C_DATA_BUFFER_SIZE = 128;
   parameter 		    trainCBit = 16'hf0f0;
   parameter 		    trainCtl = 2'b00;
   parameter 		    trainWord = 2'b11;
   parameter 		    trainLength = 8;
   
   /*****************************************
    * Fifo Status State Parameters
    *****************************************/
   parameter 		    FRAMING    = 0;
   parameter 		    BPSCW      = 1;
   parameter 		    SEND_DIP2  = 2;
   parameter 		    PORT_STAT  = 3;
   parameter 		    TRAIN_SEQ  = 4;
   
`include "xif_core.vh"   // Use the transactor template

   /*************************************
    ** Variables needed for transactor **
    ************************************/
   wire 		    reset;
   wire 		    TBP_reset;
   wire 		    dclk_int;
   
   // StatSend Signals
   reg [1:0] 		     rstat_spi4;
   reg [31:0] 		     trainTimer;
   reg [1:0] 		     statDIP2;
   reg [31:0] 		     calLen;
   integer 		     stat_train_len;
   integer 		     stat_count;
   integer 		     cal_reps;

   // SPI 4 Parameters Signals
   wire [31:0] 		     calendar_len;
   wire [31:0] 		     calendar_m;
   wire [31:0] 		     bkup_calendar_len;
   wire [31:0] 		     bkup_calendar_m;
   wire [31:0] 		     fifo_max_t;
   wire [31:0] 		     stat_train_reps;

   wire [MAX_STATUS_LEN-1:0] fifo_status;
   wire [MAX_STATUS_LEN-1:0] bkup_fifo_status;
   wire [1:0] 		     bpscw;
   wire 		     bpscw_enable;

   wire 		     sw_reset;
   wire [31:0] 		     timeout_val;

   integer 		     dip4errorCount;
   wire 		     statframe_error;
   wire 		     stattrain_error;
   wire 		     dip2errorInsert;
   integer 		     no_data_train_error; // NEW - AG
   integer 		     after_sw_reset;      // NEW - AG
   wire 		     no_protocol_error;	// NEW - AG
   wire [31:0] 		     no_sop_error;        // NEW - AG
   integer 		     no_eop_error;        // NEW - AG

   // for status training sequence injection
   wire 		     lossOfSync;
   integer 		     len;
   integer 		     reps;
   
   // For DataCollect process
   integer 		     addrlen;
   integer 		     state;
   integer 		     stat_state;
   integer 		     i;
   integer 		     trainRepsRcvd;
   time 		     rd_time;

   // XIF Core signals
   reg 			     xrun;
   reg [31:0] 		     put_address;
   reg [31:0] 		     put_size;
   reg [31:0] 		     put_data;
   reg [31:0] 		     put_status;
   reg [31:0] 		     gp_daddr;
   reg 			     put_dwe;
   
   /*************************************
    ** XIF_core interface
    ************************************/
   assign 		     BFM_xrun           = xrun; 
   assign 		     BFM_clock          = dclk_int; 
   assign 		     BFM_put_operation  = BFM_NXTREQ;
   assign 		     BFM_interrupt      = 0;
   assign 		     BFM_put_status     = put_status;
   assign 		     BFM_put_size       = put_size;
   assign 		     BFM_put_address    = put_address;
   assign 		     BFM_put_data       = put_data;
   assign 		     BFM_put_cphwtosw   = 0; 
   assign 		     BFM_put_cpswtohw   = 0; 
   assign 		     BFM_put_dwe        = put_dwe;
   assign 		     BFM_reset          = reset;
   assign 		     BFM_gp_daddr       = gp_daddr;

      
   /*********************************
    ** Continuous assignments       **
    *********************************/ 
   assign 		     reset     = !reset_n | sw_reset;
   assign 		     TBP_reset = reset;
   assign 		     rstat     = rstat_spi4;

   assign 		     rdat     = dat_in;
   assign 		     rctl     = ctl_in;
   assign 		     dclk_int = dclk_in;
   
   /*********************************
    ** Initialize variables        **
    *********************************/
   initial begin
   end

   reg [15:0] rdat_neg;
   reg 	      rctl_neg;

   always @(negedge dclk_int or posedge reset)
      if(reset == 1) begin
	 rdat_neg <= 16'h000f;
	 rctl_neg <= 1;
      end
      else begin
	 rdat_neg <= rdat;
	 rctl_neg <= rctl;
      end
   
   reg [15:0] rdat_pos;
   reg 	      rctl_pos;

   always @(posedge dclk_int or posedge reset)
      if(reset == 1) begin
	 rdat_pos <= 16'h000f;
	 rctl_pos <= 1;
      end
      else begin
	 rdat_pos <= rdat;
	 rctl_pos <= rctl;
      end
   
   /* *******************************************************
    ** process : DataCollect
    ** purpose : to collect burst data from sender, to be 
    **           sent to C-side
    **           State Machine, determining the type of data
    **           received at each clock cycle
    ** inputs  : reset, dclk_int, rdat
    ** output  : 
    ** 
    ** *****************************************************/

   // Transaction Type Parameters
   parameter TT_IDLE     = 0;
   parameter TT_PAYLOAD  = 1;
   parameter TT_TRAINING = 2;
   
   reg [15:0] rdat_neg_r1;
   reg 	      rctl_neg_r1;
   reg [15:0] rdat_neg_r2;
   reg 	      rctl_neg_r2;
   reg [15:0] rdat_neg_r3;
   reg 	      rctl_neg_r3;
   reg [15:0] rdat_pos_r1;
   reg 	      rctl_pos_r1;
   reg [15:0] rdat_pos_r2;
   reg 	      rctl_pos_r2;
   reg [15:0] rdat_pos_r3;
   reg 	      rctl_pos_r3;

   reg [15:0] pre_ctrl_r1;
   reg [15:0] pre_ctrl_r2;
   reg [15:0] pre_ctrl_r3;
   reg [15:0] pre_ctrl_r4;
   reg [15:0] post_ctrl_r1;
   reg [15:0] post_ctrl_r2;
   reg [15:0] post_ctrl_r3;

   reg [1:0]  new_trans_r1;
   reg [1:0]  new_trans_r2;
   reg [1:0]  new_trans_r3;
   reg [1:0]  end_trans_r1;
   reg [1:0]  end_trans_r2;
   reg [1:0]  end_trans_r3;

   reg [15:0] ctrl_cnt_pos_r2;
   reg [15:0] ctrl_cnt_neg_r2;
   reg [15:0] ctrl_cnt_pos_r3;
   reg [15:0] ctrl_cnt_neg_r3;

   reg [15:0] word_cnt_pos_r2;
   reg [15:0] word_cnt_neg_r2;
   reg [15:0] word_cnt_pos_r3;
   reg [15:0] word_cnt_neg_r3;

   reg [1:0]  trans_type;
   reg [31:0] timeout_cnt;

   reg 	      timeout_r1;
   reg 	      timeout_r2;
   reg 	      timeout_r3;
   
   always @(posedge dclk_int or posedge reset)
     begin
	
	// on reset, initialize variables to reset state
	if (reset == 1) begin
	   new_trans_r1 <= 0;
	   new_trans_r2 <= 0;
	   new_trans_r3 <= 0;
	   end_trans_r1 <= 0;
	   end_trans_r2 <= 0;
	   end_trans_r3 <= 0;

	   rdat_neg_r1  <= 16'h000f;
	   rctl_neg_r1  <= 1;
	   rdat_neg_r2  <= 16'h000f;
	   rctl_neg_r2  <= 1;
	   rdat_neg_r3  <= 16'h000f;
	   rctl_neg_r3  <= 1;
	   rdat_pos_r1  <= 16'h000f;
	   rctl_pos_r1  <= 1;
	   rdat_pos_r2  <= 16'h000f;
	   rctl_pos_r2  <= 1;
	   rdat_pos_r3  <= 16'h000f;
	   rctl_pos_r3  <= 1;

	   pre_ctrl_r1  <= 0;
	   pre_ctrl_r2  <= 0;
	   pre_ctrl_r3  <= 0;
	   pre_ctrl_r4  <= 0;

	   post_ctrl_r1 <= 0;
	   post_ctrl_r2 <= 0;
	   post_ctrl_r3 <= 0;

	   trans_type   <= TT_IDLE;

	   word_cnt_pos_r2 <= 0;
	   word_cnt_neg_r2 <= 0;
	   word_cnt_pos_r3 <= 0;
	   word_cnt_neg_r3 <= 0;
	   ctrl_cnt_pos_r2 <= 0;
	   ctrl_cnt_neg_r2 <= 0;
	   ctrl_cnt_pos_r3 <= 0;
	   ctrl_cnt_neg_r3 <= 0;

	   timeout_cnt     <= 0;
	   timeout_r1      <= 0;
	   timeout_r2      <= 0;
	   timeout_r3      <= 0;
	   
	end
	
	else if (reset == 0) begin
	   
	   // Default values
	   new_trans_r1 <= 2'b00;
	   end_trans_r1 <= 2'b00;
	   timeout_r1   <= 0;
	   
	   // Pipeline
	   //new_trans_r1 <= new_trans;
	   new_trans_r2 <= new_trans_r1;
	   new_trans_r3 <= new_trans_r2;
	   end_trans_r2 <= end_trans_r1;
	   end_trans_r3 <= end_trans_r2;

	   rdat_neg_r1  <= rdat_neg;
	   rdat_neg_r2  <= rdat_neg_r1;
	   rdat_neg_r3  <= rdat_neg_r2;
	   rdat_pos_r1  <= rdat_pos;
	   rdat_pos_r2  <= rdat_pos_r1;
	   rdat_pos_r3  <= rdat_pos_r2;
	   rctl_neg_r1  <= rctl_neg;
	   rctl_neg_r2  <= rctl_neg_r1;
	   rctl_neg_r3  <= rctl_neg_r2;
	   rctl_pos_r1  <= rctl_pos;
	   rctl_pos_r2  <= rctl_pos_r1;
	   rctl_pos_r3  <= rctl_pos_r2;

	   pre_ctrl_r2  <= pre_ctrl_r1;
	   pre_ctrl_r3  <= pre_ctrl_r2;
	   pre_ctrl_r4  <= pre_ctrl_r3;

	   post_ctrl_r2 <= post_ctrl_r1;
	   post_ctrl_r3 <= post_ctrl_r2;

	   word_cnt_neg_r3 <= word_cnt_neg_r2;
	   ctrl_cnt_neg_r3 <= ctrl_cnt_neg_r2;

	   timeout_r2      <= timeout_r1;
	   timeout_r3      <= timeout_r2;
	   
	   // Timeout Counter
	   timeout_cnt <= timeout_cnt + 1;
	   
	   // Check for new Payload Transaction
	   if(rctl_neg_r1 == 1'b1 && rdat_neg_r1[15] == 1'b1) begin
	      new_trans_r1[0] <= 1'b1;
	      post_ctrl_r1    <= rdat_neg_r1;
	      pre_ctrl_r1     <= post_ctrl_r1;
	      trans_type      <= TT_PAYLOAD;
	      timeout_cnt     <= 0;
	   end

	   else if(rctl_pos == 1'b1 && rdat_pos[15] == 1'b1) begin
	      new_trans_r1[1] <= 1'b1;
	      post_ctrl_r1    <= rdat_pos;
	      pre_ctrl_r1     <= post_ctrl_r1;
	      trans_type      <= TT_PAYLOAD;
	      timeout_cnt     <= 0;
	   end
	   
	   // Check for new Training Transaction
	   else if( (rctl_pos == 1'b1 && rdat_pos[15] == 1'b0 && rdat_pos[11:4] == 8'hff) &&
	       !(rctl_neg_r1 == 1'b1 && rdat_neg_r1[15] == 1'b0 && rdat_neg_r1[11:4] == 8'hff)) begin
	      new_trans_r1[0] <= 1'b1;
	      post_ctrl_r1    <= rdat_neg_r1;
	      pre_ctrl_r1     <= post_ctrl_r1;
	      trans_type      <= TT_TRAINING;
	   end
	   else if( (rctl_neg == 1'b1 && rdat_neg[15] == 1'b0 && rdat_neg[11:4] == 8'hff) &&
	       !(rctl_pos == 1'b1 && rdat_pos[15] == 1'b0 && rdat_pos[11:4] == 8'hff)) begin
	      new_trans_r1[1] <= 1'b1;
	      post_ctrl_r1    <= rdat_pos;
	      pre_ctrl_r1     <= post_ctrl_r1;
	      trans_type      <= TT_TRAINING;
	   end
	   
	   // Check for new Idle Transaction
	   else if( (rctl_pos == 1'b1 && rdat_pos == 16'h000f) && // If empty Idle
	       (rctl_neg_r1 == 1'b1) &&                      // and previous cycle is a control word
	       (trans_type != TT_IDLE)) begin                // and previous cycle was not an empty Idle
	      new_trans_r1[0] <= 1'b1;
	      post_ctrl_r1    <= rdat_neg_r1;
	      pre_ctrl_r1     <= post_ctrl_r1;
	      trans_type      <= TT_IDLE;
	   end
	   else if( (rctl_neg == 1'b1 && rdat_neg == 16'h000f) && // If empty Idle                           
	       (rctl_pos == 1'b1) && 			          // and previous cycle is a control word    
	       (trans_type != TT_IDLE)) begin		          // and previous cycle was not an empty Idle
	      new_trans_r1[1] <= 1'b1;
	      post_ctrl_r1    <= rdat_pos;
	      pre_ctrl_r1     <= post_ctrl_r1;
	      trans_type      <= TT_IDLE;
	   end
	   else if((timeout_cnt*2) > timeout_val && timeout_val != 0 
		   && trans_type == TT_IDLE &&
		   rdat_pos == 16'h000f && rdat_neg == 16'h000f) begin
	      new_trans_r1[0] <= 1'b1;
	      post_ctrl_r1    <= rdat_neg_r1;
	      pre_ctrl_r1     <= post_ctrl_r1;
	      trans_type      <= TT_IDLE;
	      timeout_cnt     <= 0;
	      timeout_r1      <= 1;
	   end
	   
	   // We don't want to count a control word that's
	   // a payload control word or a control word that immediately
	   // follows a payload transaction, unless it's a training word
	   if( (rctl_pos == 1'b1 && rdat_pos[15] == 1'b1) ||
	       (rctl_pos == 1'b1 && rctl_neg_r1 == 1'b0) && rdat_pos != 16'h0fff) begin
	      end_trans_r1[0] <= 1'b1;
	   end
	   if( (rctl_neg == 1'b1 && rdat_neg[15] == 1'b1) ||
	       (rctl_neg == 1'b1 && rctl_pos == 1'b0 && rdat_neg != 16'h0fff) ) begin
	      end_trans_r1[1] <= 1'b1;
	   end
	   	   
	   // Count Words in transaction, don't count IDLE words
	   if(new_trans_r1[0] || new_trans_r2[1]) begin
	      word_cnt_pos_r2  <= (!(rctl_pos_r1 == 1'b1 && rdat_pos_r1 == 16'h000f) && !end_trans_r1[0]);
	   end
	   else begin
	      word_cnt_pos_r2  <= word_cnt_pos_r2 + (!(rctl_pos_r1 == 1'b1 && rdat_pos_r1 == 16'h000f) && !end_trans_r1[0]);
	   end
	   
	   if(new_trans_r1[1] || new_trans_r1[0]) begin
	      word_cnt_neg_r2 <=  (!(rctl_neg_r1 == 1'b1 && rdat_neg_r1 == 16'h000f) && !end_trans_r1[1]);
	   end
	   else begin
	      word_cnt_neg_r2 <= word_cnt_neg_r2 + (!(rctl_neg_r1 == 1'b1 && rdat_neg_r1 == 16'h000f) && !end_trans_r1[1]);
	   end
	   
	   // Count number of control words in transaction
	   if(new_trans_r1[0] || new_trans_r2[1]) begin
	      ctrl_cnt_pos_r2  <= (rctl_pos_r1 && !end_trans_r1[0]);
	   end
	   else begin
	      ctrl_cnt_pos_r2  <= ctrl_cnt_pos_r2 + (rctl_pos_r1 && !end_trans_r1[0]);
	   end
	   if(new_trans_r1[1] || new_trans_r1[0]) begin
	      ctrl_cnt_neg_r2 <= (rctl_neg_r1 && !end_trans_r1[1]);
	   end
	   else begin
	      ctrl_cnt_neg_r2 <= ctrl_cnt_neg_r2 + (rctl_neg_r1 && !end_trans_r1[1]);
	   end
	   	   
	end // else: !if(reset == 1)
     end // always @ (dclk_int, reset)
   
   /* *******************************************************
    ** process : DataAlign
    ** purpose : To align the 16b data to 32 bit transactions
    ** inputs  : reset, dclk_int, rdat, align_data
    ** output  : data32
    ** 
    ** *****************************************************/
   reg 	      dly_data_r2, dly_data_r3;
   reg [31:0] data32_c0;
   reg [1:0]  rctl_c0;
   reg [1:0]  new_trans_c0;
   reg [1:0]  end_trans_c0;
   reg [15:0] pre_ctrl_c0;
   reg [15:0] post_ctrl_c0;
   reg [15:0] word_cnt_c0;
   reg [15:0] ctrl_cnt_c0;
   reg        timeout_c0;
   
   always @(posedge dclk_int or posedge reset)
     if(reset == 1) begin
	dly_data_r2  <= 0;
	dly_data_r3  <= 0;
	data32_c0    <= 32'h000f000f;
	rctl_c0      <= 2'b11;
	new_trans_c0 <= 0;
	end_trans_c0 <= 0;
	pre_ctrl_c0  <= 0;
	post_ctrl_c0 <= 0;
	timeout_c0   <= 0;
     end
     else begin

	// Adjust data delay
	if(new_trans_r1 == 2'b01)
	  dly_data_r2 <= 1'b0;
	else if(new_trans_r1 == 2'b10) 
	  dly_data_r2 <= 1'b1;
	else if(new_trans_r1 == 2'b11)
	  $display("ERROR: Two back to back start of transaction detected.\n");

	dly_data_r3  <= dly_data_r2;
	data32_c0    <= dly_data_r2 ? {rdat_pos_r2, rdat_neg_r3} : {rdat_neg_r2, rdat_pos_r2} ;
	rctl_c0      <= dly_data_r2 ? {rctl_pos_r2, rctl_neg_r3} : {rctl_neg_r2, rctl_pos_r2} ;
	
	new_trans_c0 <= dly_data_r2 ? {new_trans_r2[0], new_trans_r3[1]} : new_trans_r2;
	end_trans_c0 <= dly_data_r2 ? {end_trans_r2[0], end_trans_r3[1]} : end_trans_r2;
	pre_ctrl_c0  <= dly_data_r2 ? pre_ctrl_r3   : pre_ctrl_r2;
	post_ctrl_c0 <= dly_data_r2 ? post_ctrl_r3  : post_ctrl_r2;
	word_cnt_c0  <= dly_data_r2 ? (word_cnt_pos_r2 + word_cnt_neg_r3) : (word_cnt_neg_r2 + word_cnt_pos_r2);
	ctrl_cnt_c0  <= dly_data_r2 ? (ctrl_cnt_pos_r2 + ctrl_cnt_neg_r3) : (ctrl_cnt_neg_r2 + ctrl_cnt_pos_r2);
	timeout_c0   <= dly_data_r2 ? timeout_r3 : timeout_r2;
	
     end // if (reset == 1)


   /* *******************************************************
    ** process : XIF core control
    ** purpose : To handshake with the XIF core
    ** inputs  : 
    **           
    ** output  : xrun, put_address, put_size, put_data, put_status, put_dwe
    ** 
    ** *****************************************************/
   reg [31:0] data32_c1;
   reg 	      new_trans_c1;
   reg [1:0]  end_trans_c1;
   reg [1:0]  end_trans_c2;
   reg [15:0] pre_ctrl_c1;
   reg [15:0] post_ctrl_c1;
   reg [15:0] idle_cnt_c1;
   reg [15:0] word_cnt_c1;
   reg [15:0] ctrl_cnt_c1;
   reg        timeout_c1;
   
   always @(posedge dclk_int or posedge reset)
     if(reset == 1) begin
	data32_c1        <= 32'h000f000f;
	new_trans_c1     <= 0;
	end_trans_c1     <= 0;
	end_trans_c2     <= 0;
	pre_ctrl_c1      <= 0;
	post_ctrl_c1     <= 0;
	idle_cnt_c1      <= 0;
	word_cnt_c1      <= 0;
	ctrl_cnt_c1      <= 0;
	timeout_c1       <= 0;
	xrun             <= 0;
	put_address      <= 0;
	put_size         <= 0;
	put_status       <= 0;
	put_dwe          <= 0;
	gp_daddr         <= 0;
	put_data         <= 0;
	
     end
     else begin

	// Pipeline signals
	data32_c1        <= data32_c0;
	new_trans_c1     <= |new_trans_c0;
	end_trans_c1     <= end_trans_c0;
	end_trans_c2     <= end_trans_c1;
	pre_ctrl_c1      <= pre_ctrl_c0;
	post_ctrl_c1     <= post_ctrl_c0;
	word_cnt_c1      <= word_cnt_c0;
	ctrl_cnt_c1      <= ctrl_cnt_c0;
		
	// If next cycle is the last, deassert xrun and send transaction
	if(new_trans_c0)
	  xrun <= 1'b0;
	else if(XIF_xav && XIF_get_operation == BFM_READ)
	  xrun <= 1'b1;
	//xrun        <= new_trans_c0 ? 1'b0 : 1'b1;
	put_address <= new_trans_c0 ? {pre_ctrl_c0, ctrl_cnt_c1} : put_address;
	put_size    <= new_trans_c0 ? word_cnt_c1*2 : put_size;
	put_status  <= new_trans_c0 ? {post_ctrl_c0, 15'd0, timeout_c0} : put_status;
	put_dwe     <= !end_trans_c1[0];
	gp_daddr    <= new_trans_c1 ? 0 : gp_daddr + 1;
	put_data    <= data32_c1;
 	
     end // else: !if(reset == 1)
   

   /* *******************************************************
    ** Process : StatSend LVTTL Mode
    ** Purpose : To Send FIFO status to the sending side
    **           following calendar sequence, framing
    **           also transmitting training sequence on status bu
    **           This module is valid for LVDS and LVTTL
    ** Inputs  : reset, sclk, status.
    ** Outputs : rstat
    ** *****************************************************/     
   always @(posedge sclk or posedge reset) 
     begin
	if (reset == 1) begin
	   // at reset, initialize internal registers
	   rstat_spi4          <= 2'b11;
	   cal_reps            <= 0;
	   stat_count          <= 0;
	   statDIP2            <= 0;
	   trainTimer          <= 0;
	   len                 <= 0;
	   reps                <= 0;
	   stat_train_len      <= 20;
           no_data_train_error <= 0;
	   calLen              <= 0;
	   stat_state          <= TRAIN_SEQ;	
	end
	
	else begin
	   
	   // increment status training counter every clock period.
	   trainTimer   <=  trainTimer + 1;

	   // at rising edge of status clock, frame with incoming status
	   // data, collect statgus data, and after complete calendar,
	   // calculate DIP2
	   if (lossOfSync == 1) begin
	      //stat_count    <= 0;
	      //cal_reps      <= 0;
	      //statDIP2      <= 2'b00;
	      //rstat_spi4    <= 2'b11;
	      stat_state    <= TRAIN_SEQ;
	   end
	   
	   // transmit status sequence framing bits, if not to transmit training sequence
	   if (stat_state == FRAMING) begin
	      // inject framing sequence error
	      rstat_spi4 <= (statframe_error==0) ? 2'b11 : 2'b10;
	      stat_count <= 0;
	      cal_reps   <= 0;
	      statDIP2   <= 0;
	      calLen     <= calendar_len;
	      stat_state <= (bpscw_enable ? BPSCW : PORT_STAT);
	   end
	   
	   // injecting bpscw word
	   else if (stat_state == BPSCW) begin
	      rstat_spi4    <= bpscw;
	      statDIP2      <= DIP2(statDIP2, bpscw);
	      stat_count    <=  0;
	      stat_state    <= PORT_STAT;
	      if (bpscw == 2'b10)
		calLen      <=  bkup_calendar_len;
	   end

	   // injecting dip2 value; check trainTimer
	   else if (stat_state == SEND_DIP2) begin
	      // injecting dip2 error
	      rstat_spi4   <= (dip2errorInsert==0) ? statDIP2 : ((statDIP2%2==0) ? statDIP2 ^ 2'b01 : statDIP2 ^ 2'b10);
	      //$display("%m Sent DIP2: %d at time %d", statDIP2, $time);
	      
	      stat_count   <= stat_count + 1;

	      // See if it's time for Training Sequence
	      if (fifo_max_t !=0 && (trainTimer +(calLen * calendar_m + 1))>= fifo_max_t && lvds)
		stat_state <= TRAIN_SEQ;
	      else
		stat_state <= FRAMING;

	   end
	   
	   // injecting status for pool
	   else if (stat_state == PORT_STAT) begin
	      // Status bits are output LITTLE Endian
	      // calculate DIP-2 at each status transmission
	      rstat_spi4     <= GetCurrentStatus(bpscw_enable, bpscw, fifo_status, bkup_fifo_status, stat_count);
	      statDIP2       <= DIP2(statDIP2, GetCurrentStatus(bpscw_enable, bpscw, fifo_status, bkup_fifo_status, stat_count));
	      stat_count     <= stat_count + 1;
	      	      
	      // increment calendar reps as needed
	      if ((stat_count+1) == calLen) begin
		 stat_count  <= 0;
		 cal_reps    <= cal_reps + 1;
		 stat_state  <= ((cal_reps+1) == calendar_m ? SEND_DIP2 : PORT_STAT);
		 if ((cal_reps+1) == calendar_m)
		   statDIP2 <= DIP2(DIP2(statDIP2, GetCurrentStatus(bpscw_enable, bpscw, fifo_status, bkup_fifo_status, stat_count)), 2'b11);
	      end
	   end
	   	   
	   // to transmit training sequence
	   else if (stat_state == TRAIN_SEQ) begin
	      
	      // no training sequence transmission for lvttl mode
	      if (lvds == 1'b0) begin
		 rstat_spi4 <= 2'b11;

		 if(!lossOfSync)
		   stat_state <= FRAMING;
	      end
	      
	      else begin // if lvds mode
		 // transmit training control
		 if (len <= (stat_train_len-1)/2) begin
		    rstat_spi4 <= trainCtl;
		    len <= len + 1;
		 end
		 // transmit training data
		 else if (len > (stat_train_len-1)/2) begin
		    rstat_spi4   <= trainWord;
		    len <= len + 1;
		    if (len > stat_train_len-5 && stattrain_error==1) begin
		       if (len == stat_train_len-2) begin
			  rstat_spi4 <= trainWord ^ 2'b01;
			  $display ("training word corrupted at time %d. rstat = %d", $time, trainWord ^ 2'b01);
		       end
		    end // training data
		 end // if lvds mode
		 
		 // update training reps
		 if (len >= (stat_train_len-1)) begin
		    len <= 0;
		    reps <= reps + 1;
		    if (reps == stat_train_reps) begin
		       trainTimer <= 0;

		       if(!lossOfSync)
			 stat_state <= FRAMING;
		       else
			 stat_state <= TRAIN_SEQ;
		       reps <= 0;
		    end 
		 end // len == stat_train_len
	      end // was reset & lvds
	      
	   end // if (stat_state == TRAIN_SEQ)
	   
	   else 
	     stat_state   <= FRAMING;
	   	   
	end // else: !if(reset != 0)
     end // always @ (reset, statclk)  
         

   /*******************************************************
    ** Function to calculate a new DIP 2 from 2   
    *******************************************************/      
   function [1:0] DIP2;
      input [1:0] oDIP2;
      input [1:0] nDIP2;
      
      reg [1:0] rotDIP2;
      
      begin
	 rotDIP2 = {oDIP2[0], oDIP2[1]};
	 DIP2 = rotDIP2 ^ nDIP2;
	 //$display("%m Calculated DIP2 for %h,%h = %h at time: %d", oDIP2, nDIP2, DIP2, $time);
      end
   endfunction // DIP2
   
   /*******************************************************
    ** Function to calculate current calendered status   
    *******************************************************/         
   function [1:0] GetCurrentStatus;
      input two_cal_mode;
      input [1:0] cal_select;
      input [MAX_STATUS_LEN-1:0] fifo_status1;
      input [MAX_STATUS_LEN-1:0] fifo_status2;
      input [31:0] stat_count;

      begin
	 if (!two_cal_mode || cal_select == 2'b01)
	   GetCurrentStatus  = (fifo_status1 >> (stat_count*2)) & 2'b11;
	 else if (cal_select == 2'b10)
	   GetCurrentStatus  = (fifo_status2 >> (stat_count*2)) & 2'b11;
      end
   endfunction // GetCurrentStatus
   
   /*****************************************************
    **  Control Status Write Task                      **
    **  C-side is setting value of internal registers  **
    **  Write and display the new register values      **
    *****************************************************/
   // Regular Calendar Registers
   assign fifo_status       = XIF_get_csdata[ 511:   0]; // Addr   0-15
   assign calendar_len      = XIF_get_csdata[ 543: 512]; // Addr  16
   assign calendar_m        = XIF_get_csdata[ 575: 544]; // Addr  17

   // Backup Calendar Registers
   assign bkup_fifo_status  = XIF_get_csdata[1087: 576]; // Addr  18-33
   assign bkup_calendar_len = XIF_get_csdata[1119:1088]; // Addr  34
   assign bkup_calendar_m   = XIF_get_csdata[1151:1120]; // Addr  35

   // Fifo Status Training Parameters
   assign stat_train_reps   = XIF_get_csdata[1183:1152]; // Addr  36
   assign fifo_max_t        = XIF_get_csdata[1215:1184]; // Addr  37

   // Backup Calendar Control
   assign bpscw_enable      = XIF_get_csdata[1216];      // Addr  38:0
   assign bpscw             = XIF_get_csdata[1218:1217]; // Addr  38:1-2
   
   // Error Checking Control
   assign no_protocol_error = XIF_get_csdata[1248];      // Addr  39:0
   assign no_sop_error      = XIF_get_csdata[1249];      // Addr  39:1

   // Error Insertion Control
   assign dip2errorInsert   = XIF_get_csdata[1280];      // Addr  40:0
   assign statframe_error   = XIF_get_csdata[1281];      // Addr  40:2
   assign stattrain_error   = XIF_get_csdata[1282];      // Addr  40:3

   // Simulation Control
   assign lossOfSync        = XIF_get_csdata[1312];      // Addr  41:0
   assign sw_reset          = XIF_get_csdata[1313];      // Addr  41:1
   assign timeout_val       = XIF_get_csdata[1375:1344]; // Addr  42
	    
   /*****************************************************
    **  Control Status Read task                       **
    **  Reading internal register values, and sending  **
    **  internal reg values to the C side              **
    *****************************************************/
   assign BFM_put_csdata[31:0]  = 0;                      // Addr 0:31-0
   assign BFM_put_csdata[32]    = lvds;                   // Addr 1:0
   assign BFM_put_csdata[63:33] = 0;

   /* ********************************************************************* 
    ************************************************************************/

`endprotect

endmodule // carbonx_spi4_snk
