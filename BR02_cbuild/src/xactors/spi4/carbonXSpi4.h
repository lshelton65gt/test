//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2006 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXSpi4.h
 *
 * Contains support functions for both source and sink transactors
 *
 * Author  : Goran Knutson
 *
 * Created : Tue Apr 4 2006
 *
 *****************************************************************************
 */

#ifndef CARBONXSPI4_H
#define CARBONXSPI4_H

#define CARBONXSPI4_OP_STARTUP          0x80000001
#define CARBONXSPI4_OP_SET_PORT_ID      0x80000002
#define CARBONXSPI4_OP_SET_PORT_ORDER   0x80000003
#define CARBONXSPI4_OP_SET_CAL_PORTMAP  0x80000004
#define CARBONXSPI4_OP_SET_CAL_MAXB1    0x80000005
#define CARBONXSPI4_OP_SET_CAL_MAXB2    0x80000006
#define CARBONXSPI4_OP_SET_DATAMAXT     0x80000007
#define CARBONXSPI4_OP_SET_FIFOMAXT     0x80000008
#define CARBONXSPI4_OP_SET_ALPHA        0x80000009
#define CARBONXSPI4_OP_SET_GAMMAVALID   0x8000000a
#define CARBONXSPI4_OP_SET_GAMMAINVALID 0x8000000b
#define CARBONXSPI4_OP_SET_DELTAVALID   0x8000000c
#define CARBONXSPI4_OP_SET_DELTAINVALID 0x8000000d
#define CARBONXSPI4_OP_SET_WATERMARK1   0x8000000e
#define CARBONXSPI4_OP_SET_WATERMARK2   0x8000000f

#endif
