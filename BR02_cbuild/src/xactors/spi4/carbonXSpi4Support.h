//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXSpi4Support.h
 *
 * Contains support functions for both source and sink transactors
 *
 * Author  : John Hoglund
 *
 * Created : Mon Sep 19 2005
 *
 *****************************************************************************
 */

#ifndef CARBONXSPI4SUPPORT_H
#define CARBONXSPI4SUPPORT_H

u_int16 rotr(u_int16 x);

#endif
