//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXSpi4Src.h
 *
 * This file contains prototypes and address map for the C-API of the SPI-4.2
 *   Source Transactor
 *
 * Author  : John Hoglund
 *
 * Created : Thu Sep 8 2005
 *
 *****************************************************************************
 */
#ifndef CARBONXSPI4SRC_H
#define CARBONXSPI4SRC_H

#ifdef __cplusplus
extern "C" {
#endif

// Function: carbonXSpi4SrcStartup
// Description: Configuration and sychronization function for the Source
//              transactor.  The transactor is first initialized and then
//              all the parameters are set as requested.  The function then
//              sends out training sequences on the bus until the V-side
//              signal LOS (loss of sync) goes to zero.  LOS goes to zero
//              when the (deltaValid) number of valid status frames (DIP-2
//              is correct) are received by the V-side.
//              This function must be called at the beginning of an attached
//              function.
void    carbonXSpi4SrcStartup ( s_int32 calLength,
				s_int32 calM,
				s_int32 dataMaxT,
				s_int32 alpha,
				s_int32 deltaValid,
				s_int32 deltaInvalid,
				s_int32 fifoMaxT);

// Function: carbonXSpi4SrcPkt
// Description: Send all or part of a packet of bytes out of the source transactor
//              The function checks the current status of the port to decide
//              how much of the packet may be sent with this burst.  
//              The function sets the start of packet control word bit if sop
//              is one.  If the entire packet is sent, the end of packet status
//              is set in the control word.  DIP-4 is calculated and placed in
//              the control word.  
// Parameters: pkt  -- byte array of packet data
//             size -- number of bytes in pkt
//             port -- port on which to send pkt
//             sop  -- one means this is the start of pkt, zero means
//                     continuation of a partial packet
// Return: number of bytes of pkt actually sent out of the transactor
s_int32 carbonXSpi4SrcPkt( u_int8 *  pkt,
			s_int32   size,
			s_int32   port,
			u_int32   sop );

// Function: carbonXSpi4SrcIdle
// Description: Sends idle cycles on the SPI-4 bus.  The first idle gets the
//              EOPS and DIP-4 filled in from the previous transaction.
//              If it is time for training, then a training sequence is sent.
//              The total number of cycles spent in idle are returned.  This 
//              may be more than the requested number because of the addition
//              of training sequences.
// Parameters: s_int32 idleCount -- requested number of idle cycles
// Return: Actual number of cycles spent, idle and training
s_int32 carbonXSpi4SrcIdle ( s_int32 idleCount );

// Function: carbonXSpi4SrcRaw
// Description: Output data from the Source transactor.
// Parameters:
//   rawCtrlData -- array of 16-bit unsigned words to be output.  One on each
//                  edge of dclk_out
//   numCtrl -- number of words that have ctl_out set to "1" starting with 
//              the first word.  
//   size -- number of words in rawCtrlData
//   
// Return: the number of words actually sent.
s_int32 carbonXSpi4SrcRaw ( u_int16 * rawCtrlData,
			 s_int32 numCtrl,
			 s_int32 size );

// Function: carbonXSpi4SrcGetCal
// Description: Reads the current state of the calendar from the V-side.
// Parameters: calendarContents -- gets written with a pointer to the array
//               containing the contents of the calendar.  One calendar entry
//               in the bottom two bits of each array entry.
// Return:  Latest entry in the calendarContents to be updated in the lower
//          16 bits.  Total number of calendar update frames received 
//          in the upper 16 bits.  These may saved away to show which 
//          calendar entries are updated between calls.
u_int32 carbonXSpi4SrcGetCal(u_int32 ** calendarContents);

s_int32 carbonXSpi4SrcGetCalMaxLen ( void );

void    carbonXSpi4SrcSetCalLen ( s_int32 length );
s_int32 carbonXSpi4SrcGetCalLen ( void );

void    carbonXSpi4SrcSetCalM ( s_int32 loops );
s_int32 carbonXSpi4SrcGetCalM ( void );

void    carbonXSpi4SrcSetCalPortMap ( s_int32 calLoc, 
				   s_int32 port );
s_int32 carbonXSpi4SrcGetCalPortMap ( s_int32 calLoc );

void    carbonXSpi4SrcSetCalMaxB1 ( s_int32 port, 
				 s_int32 maxB1 );
s_int32 carbonXSpi4SrcGetCalMaxB1 ( s_int32 port );

void    carbonXSpi4SrcSetCalMaxB2 ( s_int32 port, 
				 s_int32 maxB2 );
s_int32 carbonXSpi4SrcGetCalMaxB2 ( s_int32 port );

s_int32 carbonXSpi4SrcGetCredits( s_int32 port );

void    carbonXSpi4SrcSetAlpha ( s_int32 alpha );
s_int32 carbonXSpi4SrcGetAlpha ( void );

void    carbonXSpi4SrcSetDataMaxT ( s_int32 dataMaxT );
s_int32 carbonXSpi4SrcGetDataMaxT ( void );

void    carbonXSpi4SrcSetFifoMaxT ( s_int32 fifoMaxT );
s_int32 carbonXSpi4SrcGetFifoMaxT ( void );

void    carbonXSpi4SrcSetEnableStatDip2Errors ( s_int32 enableDip2Err );
s_int32 carbonXSpi4SrcGetEnableStatDip2Errors ( void );
void    carbonXSpi4SrcSetEnableStatTrainErrors ( s_int32 enableTrainErr );
s_int32 carbonXSpi4SrcGetEnableStatTrainErrors ( void );
void    carbonXSpi4SrcSetEnableStatFrameErrors ( s_int32 enableFrameErr );
s_int32 carbonXSpi4SrcGetEnableStatFrameErrors ( void );
void    carbonXSpi4SrcSetForceEopAbort ( s_int32 eopAbort );
s_int32 carbonXSpi4SrcGetForceEopAbort ( void );

// The following four functions control the insertion of DIP-4 errors in
// the payload stream generated by SrcPkt.  If dip4Errors is set to "1" then
// the correct calculated DIP-4 value is XOR-ed with dip4ErrorMask before
// being set out on the bus.
void    carbonXSpi4SrcSetForceDip4Errors ( s_int32 dip4Errors );
s_int32 carbonXSpi4SrcGetForceDip4Errors ( void );
void    carbonXSpi4SrcSetForceDip4ErrorMask ( s_int32 dip4ErrorMask );
s_int32 carbonXSpi4SrcGetForceDip4ErrorMask ( void );

s_int32 carbonXSpi4SrcGetStatDip2ErrorCount ( void );
s_int32 carbonXSpi4SrcGetStatTrainErrorCount ( void );
s_int32 carbonXSpi4SrcGetStatFrameErrorCount ( void );

void    carbonXSpi4SrcSetDeltaValid ( s_int32 deltaValid );
s_int32 carbonXSpi4SrcGetDeltaValid ( void );
void    carbonXSpi4SrcSetDeltaInvalid ( s_int32 deltaInvalid );
s_int32 carbonXSpi4SrcGetDeltaInvalid ( void );

void    carbonXSpi4SrcInit(void);

s_int32 carbonXSpi4SrcSendTraining ( s_int32 count );

s_int32 carbonXSpi4SrcGetStatBusMode ( void );

s_int32 carbonXSpi4SrcGetLOS ( void );

// Function: carbonXSpi4SrcCalcDip4
// Description: Returns the DIP-4 for the array of 16-bit words passed in.
//              Size is the number of 16-bit words, not the number of bytes.
u_int16 carbonXSpi4SrcCalcDip4 ( u_int16 * rawData, 
			      s_int32   size );

void carbonXSpi4SrcProcessConf(const struct carbonXTransactionStruct *transReq,
				 struct carbonXTransactionStruct *transResp);

#ifdef __cplusplus
}
#endif

#endif
