//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 - 2006 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *****************************************************************************
 *
 * File Name: carbonXSpi4Snk.c
 *
 * This file contains the C-API for the SPI-4.2 Sink Transactor
 *
 * Author  : John Hoglund
 *
 * Created : Thu Sep 9 2005
 *
 *****************************************************************************
 */

#include "tbp.h"
#include "carbonXTransaction.h"
#include "carbonXSpi4.h"
#include "carbonXSpi4Snk.h"
#include "carbonXSpi4SnkSupport.h"
#include "carbonXSpi4Support.h"
#include "util/CarbonPlatform.h"
#include "util/c_memmanager.h"

// Function: carbonXSpi4SnkStartup
// Description: Configuration and sychronization function for the Sink
//              transactor.  The transactor is first initialized and then
//              all the parameters are set as requested.  The function then
//              reads data off the bus and looks for (gammaValid) number
//              of training sequences.  When the training sequences are
//              received, the loss of sync signal is set to zero in the
//              V-side.  This causes the V-side to begin sending 
//              calendar data to the source transactor.  
//              This function must be called at the beginning of an attached
//              function.
// Parameters: s_int32 calLen -- Calendar length for sink transactor
//             s_int32 calM -- Calendar M for the sink transactor
//             s_int32 dataMaxT -- DATA_MAX_T for the sink transactor
//             s_int32 alpha -- ALPHA for the sink transactor
//             s_int32 gammaValid -- number of valid training sequences that must
//                                   be seen before the sink transactor indicates
//                                   that it is in sync.
//             s_int32 gammaInvalid -- number of failing DIP-4 entries that must
//                                     be received before the sink transactor
//                                     indicates it has lost sync.
// Return: Zero return indicates successful startup.  -1 return indicates an error
//         during startup.

s_int32 carbonXSpi4SnkStartup ( s_int32 calLen,
			     s_int32 calM,
			     s_int32 dataMaxT,
			     s_int32 alpha,
			     s_int32 gammaValid,
			     s_int32 gammaInvalid ) {
  CARBONXSPI4SNK_INST_DATA_T * instData;
  u_int32 trainingCount = 0;
  s_int32 size;
  u_int16 rawData [100];
  s_int32 maxSize = 100;
  u_int16 prevCtrl;
  u_int16 follCtrl;
  u_int32 numCtrl;
  //carbonXSpi4SnkInit();
  instData = carbonXSpi4SnkGetInstData();
  carbonXSpi4SnkSetCalLen       ( calLen );
  carbonXSpi4SnkSetCalM         ( calM );
  carbonXSpi4SnkSetDataMaxT     ( dataMaxT );
  carbonXSpi4SnkSetAlpha        ( alpha );
  carbonXSpi4SnkSetGammaValid   ( gammaValid );
  carbonXSpi4SnkSetGammaInvalid ( gammaInvalid );
  carbonXSpi4SnkSetLOS ( 1 );  // Set transactor into loss of sync mode
  while (trainingCount <= instData->gammaValid) {
    size = carbonXSpi4SnkRaw ( rawData, maxSize, &prevCtrl, &follCtrl, &numCtrl);
    if ((size == 20) && (numCtrl == 10)) {
      trainingCount++;
    }
  }
  carbonXSpi4SnkSetCal();
  carbonXSpi4SnkSetLOS ( 0 );  // Ready to go so turn off LOS
  return 0;
}

// Function: carbonXSpi4SnkPkt
// Description: Receives the next packet (or partial packet) from the bus.
//              Checks that valid training sequences arrive at the proper rate.
//              Sequences must come no more than DATA_MAX_T cycles apart.
//              Idle cycles are ignored.  If a series of idle cycles of more
//              than the idle timeout is received, SnkPkt returns with size=0 and
//              the return value is the number of cycles during these idles.
//              This is done to stop a deadlock situation if the transactor is 
//              only receiving idles.
//              A partial packet is returned when a control word is received and
//              EOPS is "00" indicating no end of packet.  
// Parameters: u_int8  * pkt -- byte array of packet data that came into the
//                              transactor.
//             s_int32 * size -- As input, the maximum size in bytes of the pkt
//                               array. If the received packet is bigger than 
//                               size, an error is reported.
//                               As output, the size in bytes of the received
//                               packet.
//             s_int32 * sop -- Output of one means start of packet was 
//                              indicated on the preceding control word.
//                              Output of zero means start of packet was not
//                              indicated on the preceding control word.
//             s_int32 * eop -- Output of one means a normal end of packet was 
//                              indicated on the following control word.
//                              Output of zero means normal end of packet was
//                              not indicated on the following control word.
//                              Ouptut of -1 means end of packet abort was
//                              indicated.
//             s_int32 * port -- port from the preceding control word.
// Return: Number of cycles that have elapsed during this transaction.
//         This includes training, idle and packet receive cycles.
s_int32 carbonXSpi4SnkPkt ( u_int8  * pkt,
			 s_int32 * size,
			 s_int32 * sop,
			 s_int32 * eop,
			 s_int32 * port ) {
  s_int32 i;
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  u_int32 nextPktFound = 0;
  s_int32 sizeInWords;
  u_int16 prevCtrlWord = 0;
  u_int16 follCtrlWord = 0;
  u_int32 numCtrlWords = 0;
  s_int32 cycles = 0;
  s_int32 cycleCount = 0;
  s_int32 trainingCount = 0;
  u_int32 eops;
  u_int16 * rawData = carbonmem_malloc((*size) * sizeof(u_int16));
  u_int32 timeout;

  if (NULL == rawData) {
    MSG_Panic("Malloc failed in carbonXSpi4SnkPkt\n");
  }
  while (0 == nextPktFound) {
    sizeInWords = carbonXSpi4SnkRaw ( rawData,
				   *size,
				   &prevCtrlWord,
				   &follCtrlWord,
				   &numCtrlWords );
    carbonXSpi4SnkCheckDip4( rawData, follCtrlWord, sizeInWords );
    
    // Get the timeout flag from the transactor
    timeout = carbonXSpi4SnkGetTimeout();

    if ((20 == sizeInWords) && (10 == numCtrlWords)) {

      // Found a training sequence
      cycles      = sizeInWords;      
      cycleCount += cycles;
      instData->trainCycleCount += cycles;

      trainingCount++;
      if ((trainingCount == instData->alpha) && 
	  (trainingCount > 0)) {
	trainingCount = 0;
	instData->trainCycleCount = 0;
      }
      // Update SOP counter
      instData->sopCycleCount += cycles;
    } else if ((sizeInWords == 0) && (0x0 == (prevCtrlWord & 0x8000))) {

      // Found idles
      cycles      = numCtrlWords;
      cycleCount += cycles;
      instData->trainCycleCount += cycles;

      if (trainingCount > 0) {
	// If training transactions start coming in, they should 
	// all come back to back with no idles in between before
	// alpha number of transactions.  Thus, trainingCount should 
	// never be more than zero when finding idles.
	MSG_Error("Found Idles during a training sequence.\n");
      }
      if (instData->trainCycleCount > instData->dataMaxT) {
	MSG_Error("%d cycles more than DATA_MAX_T (%d) cycles have passed since last training sequence\n",
		  instData->trainCycleCount-instData->dataMaxT, instData->dataMaxT);
      }
      // Update SOP counter
      instData->sopCycleCount += cycles;
      
      // If the Idle Transaction timed out, return from SnkPacket with a 0 transaction
      if(timeout) {
	// Set size to 0 so user environment knows that this was a timeout, and exit
	*size        = 0;
	nextPktFound = 1;
	//MSG_Milestone("Timout: Cycles = %d\n", cycleCount);
      }

    } else if ((sizeInWords > 0) && (0x8000 == (prevCtrlWord & 0x8000))) {

      // Found payload

      // Calculate how many cycles the transaction took
      cycles = (follCtrlWord & 0x8000) ? sizeInWords + 1 : sizeInWords + 2;
      
      cycleCount += cycles;
      instData->trainCycleCount += cycles;
      
      if (trainingCount > 0) {
	// If training transactions start coming in, they should 
	// all come back to back with no payload in between before
	// alpha number of transactions.  Thus, trainingCount should 
	// never be more than zero when finding payload.
	MSG_Error("Found Payload during a training sequence.\n");
      }
      if (instData->trainCycleCount > instData->dataMaxT) {
	MSG_Error("%d cycles more than DATA_MAX_T (%d) cycles have passed since last training sequence\n",
		  instData->trainCycleCount-instData->dataMaxT, instData->dataMaxT);
      }
      // Extract packet info
      *sop = (prevCtrlWord >> 12) & 0x1;
      *port = (prevCtrlWord >> 4) & 0xFF;
      eops = (follCtrlWord >>  13) & 0x3;
      switch (eops)
	{
	case 0x0:
	  *eop = 0;
	  *size = sizeInWords * 2;
	  break;
	case 0x1:
	  *eop = -1;
	  *size = sizeInWords * 2;
	  break;
	case 0x2:
	  *eop = 1;
	  *size = sizeInWords * 2;
	  break;
	case 0x3:
	  *eop = 1;
	  *size = (sizeInWords * 2) - 1;
	  break;
	}
      // Check for SOP coming too soon
      if ((1 == *sop) && (instData->sopCycleCount < 8)) {
	MSG_Error("Start of Packet is too soon after previous start of packet. (%d cycles)\n", instData->sopCycleCount);
      }
      if (1 == *sop) {
	instData->sopCycleCount = cycles;
      }
      else {
	instData->sopCycleCount += cycles;
      }
      // Check for multiple of 16-byte burst if not end of packet
      if ((0 == *eop) && (0 != (sizeInWords % 8))) {
	MSG_Error("Payload Data without end of packet not a multiple of 16 bytes.  Actual payload cycle size = %d\n",
		  sizeInWords);
      }
      // Extract payload data into the byte array
      for (i=0; i<*size; i+=2) {
	if (i == (*size - 1)) {
	  // Only grab one byte on the last word
	  pkt[i]   = (rawData[i/2] >> 8) & 0xFF;
	} else {
	  pkt[i]   = (rawData[i/2] >> 8) & 0xFF;
	  pkt[i+1] =  rawData[i/2]       & 0xFF;
	}
      }
      nextPktFound = 1;
    } else {
      MSG_Error("carbonXSpi4SnkRaw returned unintelligible stuff. size = %d, prevCtrl = %x, follCtrl = %x, numCtrl = %d\n", 
		*size, prevCtrlWord, follCtrlWord, numCtrlWords);
    }
  }
  carbonmem_free(rawData);
  return cycleCount;
}

// Function Name: carbonXSpi4SnkRaw
// Description:  Receives the next block of control and data words from 
//               the Sink transactor.  A block is defined as one of three
//               combinations of control and data:
//               1)  Payload control word followed by payload data followed
//                   by payload or idle control word
//                   A type 1 block is identified by the previous control
//                   word being a payload control and size > 0.
//               2)  Idle control word followed by training sequences
//                   followed by a payload or idle control word
//                   A type 2 block is identified by the previous control
//                   word being an idle control and size > 0.
//               3)  Idle control word followed by more idle control words
//                   followed by a payload or idle control word that leads
//                   to the beginning of a block of type 1 or 2.
//                   This block only returns a count of the empty idle 
//                   control words in the size field along with the contents
//                   of the first and last idle control words.
//                   A type 3 block is identified by the previous control
//                   word being an idle control and size = 0.  
//               
//   rawCtrlData -- array of memory to be filled with the control and data
//                  words between the previous and following control words.
//   maxSize -- size of the rawCtrlData array
//   numCtrl -- number of rawCtrlData words that had ctl_in set to "1"
//   prevCtrl -- contents of the control word before the rawCtrlData
//   follCtrl -- contents of the control word after the rawCtrlData
//   return -- number of words in rawCtrlData.  return of -1 indicates 
//             an error while receiving

s_int32 carbonXSpi4SnkRaw ( u_int16 * rawCtrlData,
                         s_int32   maxSize,
		         u_int16 * prevCtrl,
                         u_int16 * follCtrl,
		         u_int32 * numCtrl ) {
  s_int32 ret, sizeInBytes;
  TBP_OP_T *trans;
  sizeInBytes = carbonXReadArray16(0, rawCtrlData, maxSize*2);
  ret = sizeInBytes / 2;
  if (ret > maxSize) {
    MSG_Error("Receive data (%d words) was greater than maxSize (%d words).  The rawData array was written off the end.\n",
	      ret, maxSize);
    ret = -1;
  }
  trans = carbonXGetOperation();
  *prevCtrl = trans->address >> 16;
  *follCtrl = trans->status  >> 16;
  *numCtrl  = trans->address & 0xffff;
  return ret;
}

// Function: carbonXSpi4SnkIdle
// Description: Passes simulation time with no action taken by the transactor.
//              To be used only with SnkRaw.  All error checking is disabled 
//              during the idle time.  Beware of using this function when using 
//              SnkPkt because the error checking will lose sync between the 
//              C-side and V-side and report incorrect errors.
// Parameters: s_int32 idleCount -- number of bus cycles to run as idle

void carbonXSpi4SnkIdle ( s_int32 idleCount ) {
  carbonXIdle(idleCount);
}

void carbonXSpi4SnkSetLOS ( u_int32 los ) {
  u_int32 readData;
  if ((los == 0) || (los == 1)) {
    readData = carbonXCsRead(CARBONXSPI4SNK_LOSRESET);
    readData &= 0xFFFFFFFE;
    readData |= los;
    carbonXCsWrite(CARBONXSPI4SNK_LOSRESET, readData);
  } else { // invalid los
    MSG_Error("Bad los value = %x\n", los);
  }
}

s_int32 carbonXSpi4SnkGetLOS ( void ) {
  return carbonXCsRead(CARBONXSPI4SNK_LOSRESET) & 0x01;
}

// Function: carbonXSpi4SnkGetCal
// Description: Reads the current calendar state from the V-side, fills the
//              calendar array in the per-instance data structure, and 
//              returns a pointer to the calendar array.
//              The user may update the calendar contents as desired and
//              then call carbonXSpi4SnkSetCal to write the contents back
//              to the V-side.
// Return:  Pointer to the array containing the up-to-date calendar contents
u_int32 * carbonXSpi4SnkGetCal() {
  int i,j;
  u_int32 readData;
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  for (i=0; i<16; i++) {
    readData = carbonXCsRead(CARBONXSPI4SNK_CALENDARBASE+(i*4));
    for (j=0; j<16; j++) {
      instData->calendarState[j+(i*16)] = (readData>>(j*2)) & 0x3;
    }
  }
  return instData->calendarState;
}

void carbonXSpi4SnkSetCalPortState( s_int32 port, 
				 u_int32 state ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  s_int32 i;
  for (i=0; i<instData->calendarLength; i++) {
    if (port == instData->calendarPort[i]) {
      instData->calendarState[i] = state & 0x3;
    }
  }
}

// Function: carbonXSpi4SnkGetCalPortState
// Description: Returns the current state set in the calendar of the requested
//              port.  A return of 0xFFFFFFFF means that the port was not
//              found in the calendar.
u_int32 carbonXSpi4SnkGetCalPortState( s_int32 port ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  s_int32 i;
  u_int32 ret = 0xffffffff;
  for (i=0; i<instData->calendarLength; i++) {
    if (port == instData->calendarPort[i]) {
      ret = instData->calendarState[i];
      break;
    }
  }
  return ret;
}

// Function: carbonXSpi4SnkSetCal
// Description: Writes the contents of the per-instance calendar contents
//              to the V-side.
// Return: Status of 0 means the write was successful.  Status of -1 means
//         the write was unsuccessful.
void carbonXSpi4SnkSetCal() {
  int i,j;
  u_int32 writeData;
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  for (i=0; i<16; i++) {
    writeData = 0;
    // Take the bottom two bits from each entry to pack the data
    // to send to the V-side.
    for (j=0; j<16; j++) {
      writeData |= ((instData->calendarState[(i*16)+j] & 0x3) << (j*2));
    }
    carbonXCsWrite(CARBONXSPI4SNK_CALENDARBASE+(i*4), writeData);
  }
}

s_int32 carbonXSpi4SnkCheckDip4( u_int16 * rawData, 
			   u_int16 follCtrlWord,
			   u_int32 size ) {
  u_int32 i;
  u_int16 dip16;
  u_int16   dip8 = 0;
  u_int16   dip4 = 0;
  s_int32  result = 0;

  // Save the DIP-4 that came from the bus
  u_int32 actualDip4 = follCtrlWord & 0xF;
  // Calculate the DIP-16 for the data
  if (size > 0) {
    dip16 = rawData[0];
  } else {
    dip16 = 0;
  }
  for (i=1; i<size; i++) {
    dip16 = rotr(dip16) ^ rawData[i];
  }
  // Calculate the DIP-16 for data and control word
  dip16 = rotr(dip16) ^ (follCtrlWord | 0xF);
  // Calculate the DIP-8 and DIP-4
  dip8 = (dip16&0xFF) ^ ((dip16>>8)&0xFF);
  dip4 = ((dip8&0xF) ^ ((dip8>>4)&0xF)) & 0xF;

  // Compare the actual DIP-4 to the calculated DIP-4
  if (dip4 != actualDip4) {
    result = 1;  
    carbonXSpi4SnkIncDip4ErrCnt();
    if (carbonXSpi4SnkGetEnableDip4Check()) {
      MSG_Error("DIP-4 from the following control word (%x) does not equal DIP-4 calculated from the data (%x)\n",
	               actualDip4, dip4);
    }
	}

  return result;
} // carbonXSpi4SnkCheckDip4

s_int32 carbonXSpi4SnkGetCalMaxLen ( void ) {
  return CARBONXSPI4SNK_MAXCALENDARLENGTH;
}

void carbonXSpi4SnkSetCalLen ( s_int32 length ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if ((length >= 0) && (length <= CARBONXSPI4SNK_MAXCALENDARLENGTH)) {
    carbonXCsWrite(CARBONXSPI4SNK_CALENDARLENGTH, length);
    instData->calendarLength = length;
  } else { // invalid length parameter
    MSG_Error("Requested calendar length (%d) is illegal.  Min=%d, Max=%d\n",
	      length, 0, CARBONXSPI4SNK_MAXCALENDARLENGTH);
  }
}
s_int32 carbonXSpi4SnkGetCalLen ( void ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  return instData->calendarLength;
}
void carbonXSpi4SnkSetCalM ( s_int32 loops ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if (loops >= 0) {
    carbonXCsWrite(CARBONXSPI4SNK_CALENDARM, loops);
    instData->calendarM = loops;
  } else { // invalid loops parameter
    MSG_Error("Requested calendar M (%d) is illegal.  Min=%d\n",
	      loops, 0);
  }
}
s_int32 carbonXSpi4SnkGetCalM ( void ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  return instData->calendarM;
}
void carbonXSpi4SnkSetCalPortMap ( s_int32 calLoc, 
				s_int32 port ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if ((calLoc>=0) && (calLoc<CARBONXSPI4SNK_MAXCALENDARLENGTH) &&
      (port>=0) && (port<CARBONXSPI4SNK_MAXPORTS)) {
    instData->calendarPort[calLoc] = port;
  } else {
    MSG_Error("Calendar location (%d) not legal, Min=%d, Max=%d\n",
	      calLoc, 0, CARBONXSPI4SNK_MAXCALENDARLENGTH);
  }
}
s_int32 carbonXSpi4SnkGetCalPortMap ( s_int32 calLoc ) {
  s_int32 ret;
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if ((calLoc>=0) && (calLoc<CARBONXSPI4SNK_MAXCALENDARLENGTH)) {
    ret = instData->calendarPort[calLoc];
  } else {
    MSG_Error("Calendar location (%d) not legal, Min=%d, Max=%d\n",
	      calLoc, 0, CARBONXSPI4SNK_MAXCALENDARLENGTH);
    ret = -1;
  }
  return ret;
}
void carbonXSpi4SnkSetAlpha ( s_int32 alpha ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if (alpha >= 0) {
    instData->alpha = alpha;
  } else {
    MSG_Error("Illegal ALPHA (%d) in carbonXSpi4SnkSetAlpha, must be greater than zero\n",
	      alpha);
  }
}
s_int32 carbonXSpi4SnkGetAlpha ( void ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  return instData->alpha;
}
void carbonXSpi4SnkSetDataMaxT ( s_int32 dataMaxT ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if (dataMaxT >= 0) {
    instData->dataMaxT = dataMaxT;
  } else {
    MSG_Error("Illegal DATA_MAX_T (%d) in carbonXSpi4SnkSetDataMaxT, must be greater than zero\n",
	      dataMaxT);
  }
}
s_int32 carbonXSpi4SnkGetDataMaxT ( void ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  return instData->dataMaxT;
}
void carbonXSpi4SnkSetFifoMaxT ( s_int32 fifoMaxT ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if (fifoMaxT >= 0) {
    carbonXCsWrite(CARBONXSPI4SNK_FIFOMAXT, fifoMaxT);
    instData->fifoMaxT = fifoMaxT;
  } else {
    MSG_Error("Illegal FIFO_MAX_T (%d) in carbonXSpi4SnkSetFifoMaxT, must be greater than zero\n",
	      fifoMaxT);
  }
}
s_int32 carbonXSpi4SnkGetFifoMaxT ( void ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  return instData->fifoMaxT;
}
void carbonXSpi4SnkSetGammaValid ( s_int32 gammaValid ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if (gammaValid > 0) {
    instData->gammaValid = gammaValid;
  } else {
    MSG_Error("Number of requested valid training (%d) to sync with Source side must be greater than zero\n",
	      gammaValid);
  }
}
s_int32 carbonXSpi4SnkGetGammaValid ( void ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  return instData->gammaValid;
}

void carbonXSpi4SnkSetGammaInvalid ( s_int32 gammaInvalid ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if (gammaInvalid > 0) {
    instData->gammaInvalid = gammaInvalid;
  } else {
    MSG_Error("Number of requested invalid DIP-4 (%d) to lose sync with the Source side must be greater than zero\n",
	      gammaInvalid);
  }
}
s_int32 carbonXSpi4SnkGetGammaInvalid ( void ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  return instData->gammaInvalid;
}

CARBONXSPI4SNK_INST_DATA_T * carbonXSpi4SnkGetInstData (void) {
  CARBONXSPI4SNK_INST_DATA_T * instData = 
    (CARBONXSPI4SNK_INST_DATA_T *) carbonXGetInstData();
  if (NULL == instData) {
    carbonXSpi4SnkInit();
    instData = (CARBONXSPI4SNK_INST_DATA_T *) carbonXGetInstData();
  }
  return instData;
}

void carbonXSpi4SnkSetInstData (CARBONXSPI4SNK_INST_DATA_T * ptr) {
  carbonXSetInstData(ptr);
}

// Initializes the state of the transactor.
void carbonXSpi4SnkInit(void) {
  u_int32 i;
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonmem_malloc(sizeof(CARBONXSPI4SNK_INST_DATA_T));
  if (instData != NULL) {
    carbonXSpi4SnkSetInstData(instData);
    carbonXSpi4SnkSetCalLen ( 0 );
    carbonXSpi4SnkSetShadCalLen ( 0 );
    carbonXSpi4SnkSetCalM ( 0 );
    carbonXSpi4SnkSetShadCalM ( 0 );
    instData->alpha = 0;
    instData->dataMaxT = 0;
    instData->fifoMaxT = 0;
    for (i=0; i<CARBONXSPI4SNK_MAXCALENDARLENGTH; i++) {
      instData->calendarState[i] = 0x0;
      instData->shadowCalState[i] = 0x0;
      instData->calendarPort[i] = 0;
      instData->shadowCalPort[i] = 0;
    }
    instData->gammaValid = 1;
    instData->gammaInvalid = 1;
    instData->bpscwConfig = 0;
    instData->trainSeqLen = 0;
    instData->biterr = 0;
    instData->shortSeq = 0;
    carbonXSpi4SnkSetLOS ( 1 );
    instData->trainCycleCount = 0;
    instData->sopCycleCount = 8;
    instData->insertDip2Errors = 0;
    instData->insertTrainErrors = 0;
    instData->insertFrameErrors = 0;
    instData->enableDip4Check = 0;
    instData->Dip4ErrCnt = 0;
  } else { // malloc failed
    MSG_Panic("Malloc failed in carbonXSpi4SnkInit\n");
  } 
}

void carbonXSpi4SnkSetInsertDip2Errors ( s_int32 insertDip2Err ) {
  u_int32 writeData;
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if ((insertDip2Err == 0) || (insertDip2Err == 1)) {
    if (instData->insertDip2Errors != insertDip2Err) {
      writeData = (((instData->insertTrainErrors << 2) | 
		    (instData->insertFrameErrors << 1) |
		    (insertDip2Err << 0)) &
		   0x7);
      carbonXCsWrite(CARBONXSPI4SNK_ERRORINSERT, writeData);
      instData->insertDip2Errors = insertDip2Err;
    }
  } else {
    MSG_Error("Insert DIP-2 Error value (%d) must be 0 or 1\n", 
	      insertDip2Err);
  }
}
s_int32 carbonXSpi4SnkGetInsertDip2Errors ( void ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  return instData->insertDip2Errors;
}

void carbonXSpi4SnkSetInsertFrameErrors ( s_int32 insertFrameErr ) {
  u_int32 writeData;
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if ((insertFrameErr == 0) || (insertFrameErr == 1)) {
    if (instData->insertFrameErrors != insertFrameErr) {
      writeData = (((instData->insertTrainErrors << 2) | 
		    (insertFrameErr << 1) |
		    (instData->insertDip2Errors << 0)) &
		   0x7);
      carbonXCsWrite(CARBONXSPI4SNK_ERRORINSERT, writeData);
      instData->insertFrameErrors = insertFrameErr;
    }
  } else {
    MSG_Error("Insert Frame Error value (%d) must be 0 or 1\n", 
	      insertFrameErr);
  }
}
s_int32 carbonXSpi4SnkGetInsertFrameErrors ( void ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  return instData->insertFrameErrors;
}

void carbonXSpi4SnkSetInsertTrainErrors ( s_int32 insertTrainErr ) {
  u_int32 writeData;
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if ((insertTrainErr == 0) || (insertTrainErr == 1)) {
    if (instData->insertTrainErrors != insertTrainErr) {
      writeData = (((insertTrainErr << 2) | 
		    (instData->insertFrameErrors << 1) |
		    (instData->insertDip2Errors << 0)) &
		   0x7);
      carbonXCsWrite(CARBONXSPI4SNK_ERRORINSERT, writeData);
      instData->insertTrainErrors = insertTrainErr;
    }
  } else {
    MSG_Error("Insert Train Error value (%d) must be 0 or 1\n", 
	      insertTrainErr);
  }
}
s_int32 carbonXSpi4SnkGetInsertTrainErrors ( void ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  return instData->insertTrainErrors;
}

// Function: carbonXSpi4SnkGetShadCal
// Description: Reads the current shadow calendar state from the V-side, 
//              fills the calendar array in the per-instance data structure,
//              and returns a pointer to the shadow calendar array.
//              The user may update the calendar contents as desired and
//              then call carbonXSpi4SnkSetCal to write the contents back
//              to the V-side.
// Return:  Pointer to the array containing the up-to-date calendar contents
u_int32 * carbonXSpi4SnkGetShadCal() {
  int i,j;
  u_int32 readData;
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  for (i=0; i<16; i++) {
    readData = carbonXCsRead(CARBONXSPI4SNK_SHADCALBASE+(i*4));
    for (j=0; j<16; j++) {
      instData->shadowCalState[j+(i*16)] = (readData>>(j*2)) & 0x3;
    }
  }
  return instData->shadowCalState;
}


// Function: carbonXSpi4SnkSetShadCal
// Description: Writes the contents of the per-instance shadow calendar 
//              contents to the V-side.
void carbonXSpi4SnkSetShadCal() {
  int i,j;
  u_int32 writeData;
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  for (i=0; i<16; i++) {
    writeData = 0;
    // Take the bottom two bits from each entry to pack the data
    // to send to the V-side.
    for (j=0; j<16; j++) {
      writeData |= ((instData->calendarState[(i*16)+j] & 0x3) << (j*2));
    }
    carbonXCsWrite(CARBONXSPI4SNK_SHADCALBASE+(i*4), writeData);
  }
}

void carbonXSpi4SnkSetShadCalLen ( s_int32 length ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if ((length >= 0) && (length <= CARBONXSPI4SNK_MAXCALENDARLENGTH)) {
    carbonXCsWrite(CARBONXSPI4SNK_CALENDARLENGTH, length);
    instData->shadowCalLength = length;
  } else { // invalid length parameter
    MSG_Error("Shadow Calendar Error\n");
  }
}
s_int32 carbonXSpi4SnkGetShadCalLen ( void ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  return instData->shadowCalLength;
}

void carbonXSpi4SnkSetShadCalM ( s_int32 loops ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if (loops >= 0) {
    carbonXCsWrite(CARBONXSPI4SNK_CALENDARM, loops);
    instData->shadowCalM = loops;
  } else { // invalid loops parameter
    MSG_Error("Shadow Calendar Error\n");
  }
}
s_int32 carbonXSpi4SnkGetShadCalM ( void ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  return instData->shadowCalM;
}

void carbonXSpi4SnkSetShadCalPortMap ( s_int32 calLoc, 
				     s_int32 port ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if ((calLoc>=0) && (calLoc<CARBONXSPI4SNK_MAXCALENDARLENGTH) &&
      (port>=0) && (port<CARBONXSPI4SNK_MAXPORTS)) {
    instData->shadowCalPort[calLoc] = port;
  } else {
    MSG_Error("Shadow Calendar Error\n");
  }
}
s_int32 carbonXSpi4SnkGetShadCalPortMap ( s_int32 calLoc ) {
  s_int32 ret;
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  if ((calLoc>=0) && (calLoc<CARBONXSPI4SNK_MAXCALENDARLENGTH)) {
    ret = instData->shadowCalPort[calLoc];
  } else {
    MSG_Error("Shadow Calendar Error\n");
    ret = -1;
  }
  return ret;
}

u_int32 carbonXSpi4SnkGetTimeout( void ) {
  u_int32 timeout;
  timeout = carbonXGetStatus();
  return timeout & 0x1;
}
 
 void carbonXSpi4SnkSetTimeoutVal( u_int32 timeout){
  carbonXCsWrite(CARBONXSPI4SNK_TIMEOUTVAL, timeout);
}

s_int32 carbonXSpi4SnkGetStatBusMode ( void ) {
  return (carbonXCsRead( CARBONXSPI4SNK_STATUSBUSMODE ) >> 0) & 0x01;
}

u_int16 carbonXSpi4SnkCalcDip4 ( u_int16 * rawData, 
			      s_int32   size ) {
  s_int32 i;
  u_int16 dip16 = rawData[0];
  u_int16 dip8 = 0;
  u_int16 dip4 = 0;
  for (i=1; i<size; i++) {
    dip16 = rotr(dip16) ^ rawData[i];
  }
  dip8 = (dip16&0xFF) ^ ((dip16>>8)&0xFF);
  dip4 = ((dip8&0xF) ^ ((dip8>>4)&0xF)) & 0xF;
  return dip4;
}


void carbonXSpi4SnkSetEnableDip4Check ( s_int32 enableDip4Check ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();

  if ((enableDip4Check == 0) || (enableDip4Check == 1)) {
    if (instData->enableDip4Check != enableDip4Check) {
      instData->enableDip4Check = enableDip4Check;
    }
  } else {
    MSG_Error("Enable Check DIP-4 Error value (%d) must be 0 or 1\n", enableDip4Check);
  }
}

s_int32 carbonXSpi4SnkGetEnableDip4Check ( void ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  return instData->enableDip4Check;
}

void carbonXSpi4SnkIncDip4ErrCnt( void ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  instData->Dip4ErrCnt++;
  if (instData->Dip4ErrCnt >= (s_int32)instData->gammaInvalid) {
    carbonXSpi4SnkSetLOS(1);  // Set transactor into loss of sync mode
  }
}

s_int32 carbonXSpi4SnkGetDip4ErrCnt ( void ) {
  CARBONXSPI4SNK_INST_DATA_T * instData = carbonXSpi4SnkGetInstData();
  return instData->Dip4ErrCnt;
}

// These functions are support functions for the SystemC Interface
void carbonXSpi4SnkProcessConf(const struct carbonXTransactionStruct *transReq, struct carbonXTransactionStruct *transResp) {
  
  SInt32 *data;
  UInt32 i;

  switch(transReq->mOpcode) {
  case CARBONXSPI4_OP_STARTUP :
    carbonXSpi4SnkStartup(carbonXSpi4SnkGetCalLen(), carbonXSpi4SnkGetCalM(),
			  carbonXSpi4SnkGetDataMaxT(), carbonXSpi4SnkGetAlpha(),
			  carbonXSpi4SnkGetGammaValid(), carbonXSpi4SnkGetGammaInvalid());
    carbonXSetTransaction(transResp, transReq->mTransId, TBP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
    break;
  case CARBONXSPI4_OP_SET_CAL_PORTMAP :
    data = (SInt32 *)transReq->mData;
    carbonXSpi4SnkSetCalLen(transReq->mSize/4);
    for(i = 0; i < transReq->mSize/4; i++) {
      carbonXSpi4SnkSetCalPortMap(transReq->mAddress+i, data[i]);
      carbonXSpi4SnkSetCalPortState(data[i], 0); // Set to satisfied
    }
    carbonXSetTransaction(transResp, transReq->mTransId, TBP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
    break;
  case CARBONXSPI4_OP_SET_DATAMAXT :
    carbonXSpi4SnkSetDataMaxT(transReq->mAddress);
    carbonXSetTransaction(transResp, transReq->mTransId, TBP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
    break;
  case CARBONXSPI4_OP_SET_FIFOMAXT :
    carbonXSpi4SnkSetFifoMaxT(transReq->mAddress);
    carbonXSetTransaction(transResp, transReq->mTransId, TBP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
    break;
  default :
    carbonXSetTransaction(transResp, transReq->mTransId, TBP_NXTREQ, 0, NULL, 0, 0, TBP_WIDTH_32);
  }
}
