
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// *****************************************************************************
// Module Name: PcixInitiatorSm.v
//
// Module Description:
// The state machine for the PCI-X is implemented in this module.
// The state machine supports the following
// * Configuration accesses
// * Write/Read accesses (single and burst)
// * 64-bit data support
// * Dual Address cycle support
// * Parity generation and error detection
// * Termination types supported : Retry, Target Abort, Disconnect at ADB
//   Single Data phase disconnect, Split response, Master Abort 
// * Target inserted Initial wait state is also supported
//
// * A status register 'Pcix_Status' is updated with the status of the transaction
//   ( type of termination, parity errors etc), and is passed to the C-side
//   Pcix_Status[15:0]  - Used for termination type indication
//   Pcix_Status[19:16] - Used for error indication
//   Pcix_Status[31:22] - The number of DWORDS transferred
//   Pcix_Status[21:20] - Unused currently
//
// * A configuration register 'configReg' is used to control the BFM.
//   It can be written to by a CS_Write
//   configReg[0] - When set, enables auto retry mode
//   configReg[1] - When set, causes the transaction to be terminated when 
//                  target exceeds initial latency
//   configReg[2] - Causes data parity to be corrupted
//   configReg[3] - Causes address parity to be corrupted
//   configReg[4] - Causes attribute parity to be corrupted
// * configReg[16:8] - Has the master latency timer value
//
// *****************************************************************************

module carbonx_pcix_initiatorsm(
	// PCI-x signals
	frame_l,
	irdy_l,
	trdy_l,
	devsel_l,
	stop_l,
	ad,
	cbe,
	par,
	par64,
	req_l,
	gnt_l,
	req64_l,
	ack64_l,
	perr_l,
	serr_l,
	int_l,
	
	// system signals
	clk,
	rst_l,
	
	// Mode control bit
	configReg,
	mode_pcix,
	
	XIF_reset, 
	XIF_xav,
	XIF_get_operation,
	XIF_get_address,
	XIF_get_size,
	XIF_get_status,
	XIF_get_data,
	BFM_xrun,
	BFM_put_status,
	BFM_put_data,
	BFM_put_cphwtosw,
	BFM_put_cpswtohw,
	BFM_gp_daddr,
	BFM_put_dwe
	       );

`protect

  parameter    
               DataWidth =  64,
               DataAdWid =   9; // Max is 15
   
   //------------------------------------------//
   //-------  System inputs/outputs   ---------//
   //------------------------------------------//

  input clk;
  input rst_l;
   
  input mode_pcix;
  input [31:0] configReg;

  // These signals are registered at the top level.
  // PCI inputs and outputs
  input trdy_l;
  input devsel_l;
  input stop_l;
  input ack64_l;

  inout frame_l;
  inout irdy_l;
  output req64_l;
   
  inout [63:0] ad;
  inout  [7:0] cbe;
  inout 	par;
  inout 	par64;

  inout 	perr_l;
  input 	serr_l;

  input [3:0] 	int_l;
   
  // Arbitration signals
  output 	req_l;
  input 	gnt_l;

  input                 XIF_reset;
  input                 XIF_xav;
  input [31:0]          XIF_get_operation;
  input [63:0]    	XIF_get_address;
  input [31:0]          XIF_get_size;
  input [31:0]          XIF_get_status;
  input [DataWidth-1:0] XIF_get_data;

  output                 BFM_xrun;  reg BFM_xrun;
  output [31:0]          BFM_put_status;
  output [DataWidth-1:0] BFM_put_data;
  output                 BFM_put_cphwtosw;
  output                 BFM_put_cpswtohw;
  output [DataAdWid-1:0] BFM_gp_daddr;
  output                 BFM_put_dwe;

// carbon license crbn_vsp_exec_xtor_pcix
   
`include "carbonx_pcix_defines.vh"

   //------------------------------------------//
   //-------    Register definitions  ---------//
   //------------------------------------------//

   reg 		master; 		// Asserted when transaction starts
   reg 		master_1; 		// Asserted when transaction starts
   reg 		data_ph;
   reg 		addr_ph;
   reg 		addr_ph_1;
   reg 		attr_ph;
   reg 		drive_64;		// When high, this signal enables the
   // PCI 64 signals to be driven
   // Asserted with req64 & deasserted 
   // with req63 or when (!devsel & ack64) 

   reg 		frame;
   reg 		irdy;
   reg 		req;
   reg 		perr;

   reg [63:0] 	word_address;		// Keeps track of the access address
   reg [63:0] 	word_address_1;              // Pipelined version of address
   reg [1:0] 	addr_setup_cntr;	// Counter to count 4 clocks for 
   					// address setup for configuration cycles

   reg [3:0] 	byte_en1;
   reg [3:0] 	byte_en2;
   reg [3:0] 	byte_en_l;		// Lower byte enables
   reg [3:0] 	byte_en_h;		// Upper byte enables

   reg 		rd_par_en;
   reg 		rd_par_en_1;		// Control the timing for perr_l
   reg 		rd_par_en_2;
   reg 		data_ph_1;
   reg 		StartRead_1;
   reg 		StartWrite_1;
   reg 		data_chk_par;
   reg 		data_chk_par_h;
   reg 		check_rd_parity;
   reg 		drive_64_d;
   reg 		parity;			// parity for ad[31:0] & cbe[3:0]
   reg 		parity64;		// parity for ad[63:32] & cbe[7:4]

   reg [8:0] 	master_latency_timer;	// master latency timer
   reg [5:0] 	target_latency_timer;	// count number of initial wait states

   reg [3:0] 	IntStatus;

   reg [3:0] 	Pcix_State ;		// State variable for the state machine

   //reg [9:0] 	TotalNumDataPh;		// Total number of data phases in the transaction
   wire [9:0] 	TotalNumDataPh;		// Total number of data phases in the transaction
   //  - computed from start addr & byte count
   
   reg [7:0] 	adbcnt;			// Counts the number of data phases after the
   // target signals ADB
   reg [9:0] 	byte_cnt;		// Counts the number of bytes transferred
   reg [9:0] 	start_byte_cnt;		
   reg [9:0] 	dataph_cnt;		// Counts the data phases
   reg [9:0] 	stable_dataph_cnt;	
   reg [3:0] 	devsel_cnt;		// Counter for devsel timeout
   reg [9:0] 	dword_cnt;              // Count the number of DWORDs as they are transferred
   reg [9:0] 	start_dword_cnt;        
   reg [9:0] 	stable_dword_cnt;

   reg [7:0] 	dword_cnt_del;          
   reg 		transaction_done_del;

   parameter 	DATA_BUFFER_SIZE = 128;
   reg [31:0] 	wr_data[0:DATA_BUFFER_SIZE-1];		// Array for storing write data
   reg [31:0] 	rd_data[0:DATA_BUFFER_SIZE-1];		// Array for storing read data

   reg [31:0] 	Pcix_Status;		// Register to store the transaction status
   // It reports the type of termination
   // and any errors during the transaction

   // Set of inputs from the BFM tasks

   reg [63:0] 	StartAddress;		// Starting address of transaction
   reg [2:0] 	OrigAddress;
   reg [2:0] 	LastAddress;

   wire [31:0] 	Attribute;		// Attribute of transaction


//
//  These are just decodes of the inputs from TBP/PREP
//

  // calculate starting bit number (based on field width)  just to make things look nicer
   
   parameter    Cwid   = USER_OP__PCIX_CMD_H   - USER_OP__PCIX_CMD_L ;  //  3 -  0 =  3 ( 4 bits)
   parameter    CXwid  = USER_OP__PCIX_CMDX_H  - USER_OP__PCIX_CMDX_L;  //  3 -  1 =  2 ( 3 bits)
   parameter    RTwid  = USER_OP__REQ_TAG_H    - USER_OP__REQ_TAG_L;    // 28 - 24 =  4 ( 5 bits)
   parameter    RTSwid = USER_OP__REQ_TAG_SC_H - USER_OP__REQ_TAG_SC_L; // 26 - 24 =  2 ( 3 bits)
   parameter    RIDwid = USER_OP__REQ_ID_H     - USER_OP__REQ_ID_L;     // 23 -  8 = 15 (16 bits)
                
   
   wire [Cwid  :0] Command    = XIF_get_operation[USER_OP__PCIX_CMD_H  : USER_OP__PCIX_CMD_L];  
   wire [CXwid :0] CommandX   = XIF_get_operation[USER_OP__PCIX_CMDX_H : USER_OP__PCIX_CMDX_L]; 
   wire [RTwid :0] req_tag    = XIF_get_operation[USER_OP__REQ_TAG_H   : USER_OP__REQ_TAG_L];
   wire [RTSwid:0] req_tag_sc = XIF_get_operation[USER_OP__REQ_TAG_SC_H: USER_OP__REQ_TAG_SC_L];
   wire [RIDwid:0] req_id     = XIF_get_operation[USER_OP__REQ_ID_H    : USER_OP__REQ_ID_L];
   
   
					// Trigger to start read transaction
   wire 	StartWrite     = mode_pcix &   XIF_get_operation[USER_OP__VALID]
				           & ( XIF_get_operation[USER_OP__WRITE] 
                                              | (Command == PCIX_SPLIT_COMP));
   
   wire 	StartRead      = mode_pcix & XIF_get_operation[USER_OP__VALID]	& ~StartWrite;
					// Trigger to start write transaction


   wire 	Use64bitaccess = XIF_get_operation[USER_OP__64BIT];  // Start a 64 bit access
   wire 	SplitComplete  = (Command  == PCIX_SPLIT_COMP);      // Split cycle
   wire 	CfgAccess      = (CommandX == PCI_CFG_ACC);	     // Configuration access
   wire 	IOAccess       = (CommandX == PCI_IO_ACC);           // IO cycle
   wire 	IntAck         = (Command == PCI_INTR_ACK);          // Interrupt Acknowledge cycle
   wire 	DualAddrCycle  = XIF_get_address[63:32] != 0;	     // Indicates a dual address cycle
   wire [11:0] 	ByteCount      = XIF_get_size;			     // Total byte count of the transaction

   assign BFM_put_status = Pcix_Status;
   assign Attribute =
	(CommandX == PCI_CFG_ACC 
       | Command  == PCI_INTR_ACK)    ?	{3'h0, req_tag, req_id, XIF_get_address[23:16]} :
	(CommandX == PCI_IO_ACC)      ?	{3'h0, req_tag, req_id,       8'h00} :
	(Command  == PCIX_SPLIT_COMP) ?	{req_tag_sc, 5'h0, req_id, XIF_get_size[7:0]} :
                                        {3'h0, req_tag, req_id, XIF_get_size[7:0]} ;


   // Registering all PCI-X signals

   reg 		ack64;
   reg 		ack64_d;
   reg 		req64;
   reg 		trdy;
   reg 		stop;
   reg 		devsel;
   reg 		gnt;
   reg [63:0] 	ad_int;
   reg [7:0] 	cbe_int;
   reg 		par_int;
   reg 		par64_int;
   reg 		perr_int;
   reg 		serr_int;
   reg 		frame_int;
   reg 		irdy_int;

   // Other register definitions
   reg 		frame_d, irdy_d;

   reg  [3:0] 	EndAddress;		// Lower 2 bits of transaction End address
   wire [31:0] 	ad_master;
   wire [31:0]	ad_master_data;
   wire 	ad_master_en;
   wire [31:0] 	ad_h_master_data;
   wire		ad_h_master_en;
   wire 	disconnect;
   wire 	disconnect1;
   wire 	disconnect2;
   wire [4:0] 	adb_addr_frame;
   wire [4:0] 	adb_addr_irdy;
   wire [31:0] 	dac_address;
   wire [3:0] 	dac_command;

   wire 	dataparity;
   wire 	dataparity64;
   wire 	addrparity;
   wire 	addrparity64;
   wire 	attrparity;
   wire		par_data;
   wire		par_en;
   wire		par64_data;
   wire		par64_en;
   wire		perr_l_data;
   wire		perr_l_en;

   wire 	transaction_over;
   wire 	DWORDaccess;

   wire 	first_data, last_data;
   reg 		transaction_done;
   wire [9:0] 	num_data_ph_left;
   wire [9:0] 	num_ph_before_adb;
   wire 	dont_disconnect;

   wire 	master_latency_timer_exp, load_latency_timer;
   reg 		frame_delayed;
   wire 	exit_trgt_latency_exp;
   wire 	corrupt_addr_parity;
   wire 	corrupt_attr_parity;
   wire 	corrupt_data_parity;
   wire 	enableAutoRetry;
   wire [8:0] 	mstr_latency_timer_config;
   wire 	last_addr;

   integer 	i;


/*
 *  Regs/wires for read and write data rotation to/from TBP (XIF core)
 */
			// Shared by write & read datapath
   reg[13:0]	rdat_gp_daddr;
   reg[16:0]	rdat_bcount;   // Read data byte count, used to generate gp_dadder
				// daddr is bcount/8, basically...
   reg[3:0]	rdat_bincr;  	// byte increment -- amount to inc bcount by....

			// Write datapath (from xif core)
   wire[63:0]	wdat_data;
   reg[63:0]	wdat_data_reg;
   reg[63:0]	wdat_last_data;
   wire[63:0]	wdat_rotator;
   reg		ld_getdata, ld_getdata_d, ld_getdata_i;

			// Read datapath (to XIF core)



   //------------------------------------------//
   //-------    State names       -------------//
   //------------------------------------------//

   parameter 	IDLE 		  = 4'h0,
		REQ 	 	  = 4'h1,
		ADR_SETUP	  = 4'h2,
		DAC		  = 4'h3,
		ADDR		  = 4'h4,
		ATTR		  = 4'h5,
		TRN		  = 4'h6,
		WAIT_DEVSEL	  = 4'h7,
		DATA0		  = 4'h8,
		BURST		  = 4'h9,
		ADB_STATE  	  = 4'ha, 
		LATENCY_TIMER_EXP = 4'hb,
		REQ_CONT	  = 4'hc;
   


   //------------------------------------------//
   //------- Register PCI-X input signals -----//
   //------------------------------------------//

   
   always @ (posedge clk) begin
      trdy      <= ~trdy_l;
      stop      <= ~stop_l;
      devsel    <= ~devsel_l;
      ack64_d   <= ack64;
      ack64     <= ~ack64_l;
      gnt       <= ~gnt_l;
      ad_int    <= ad;
      cbe_int   <= cbe;
      par_int   <= par;
      par64_int <= par64;
      perr_int  <= ~perr_l;
      serr_int  <= ~serr_l;
      frame_int <= ~frame_l;
      irdy_int  <= ~irdy_l;
   end


   assign BFM_put_cphwtosw = 0;
   assign BFM_put_cpswtohw = 0;


   //-----------------------------------------//
   //----  Setting intr status to C-side  ----//
   //-----------------------------------------//
   always @ (posedge clk)
      if (!rst_l)
	 IntStatus[3:0] <= 4'h0;
      else
         IntStatus[3:0] <= ~int_l[3:0];


   //------------------------------------------//
   //-------- Drive the PCI-X signals ---------//
   //------------------------------------------//

   
   assign frame_l  = (master) ? ~frame : 1'bz;
   assign irdy_l   = (master_1) ? ~irdy : 1'bz;
   assign req_l    = ~req;

   assign par_data = (master & master_1) ? 
                     ((~data_ph_1 | StartWrite_1) ? parity : 1'b0) : 1'b0;
   assign par_en = (master & master_1) ?
   		   ((~data_ph_1 | StartWrite_1) ? 1'b1 : 1'b0) : 1'b0;
   assign par = par_en ? par_data : 1'bz;


   assign perr_l_data = (rd_par_en_1 | rd_par_en_2) ? ~perr : 1'b0;
   assign perr_l_en = (rd_par_en_1 | rd_par_en_2) ? 1'b1 : 1'b0;
   assign perr_l = perr_l_en ? perr_l_data : 1'bz;


   //--------- 64 bit data signals -----------//

   assign req64_l   = (master) ? ~req64 : 1'bz;

   assign par64_data = (master & drive_64_d) ? 
		       ((~data_ph_1 | StartWrite_1) ? parity64 : 1'b0) : 1'b0;
   assign par64_en = (master & drive_64_d) ? 
		     ((~data_ph_1 | StartWrite_1) ? 1'b1 : 1'b0) : 1'b0;
   assign par64 = par64_en ? par64_data : 1'bz;

   
   //----------- Internal signals -----------//

//  ****************************************
//
//  This is a passel of combinatorial logic to handle rotation and assembly
//  of read data from PCI to the XIF_core data memory.
//  This replaces all the rotate logic into the rddata array as well as packing
//  logic in the pci_bfm_read tasks
//  
//  ****************************************
   wire[63:0]    rdat_newbytes;

	// Read data valid this cycle
// wire rdat_valid = ((Pcix_State==DATA0) || (Pcix_State==BURST)) && trdy && StartRead;
   wire rdat_valid = ~trdy_l & (Pcix_State == WAIT_DEVSEL | Pcix_State == DATA0 | Pcix_State == BURST | Pcix_State == ADB_STATE);

	// Read data bus normalized for 64/32 bit operation
   wire[63:0] rdat_normdata = ack64 ? ad : {ad[31:0], ad[31:0]};

	// Rotate normalized data appropriately
   wire[63:0] rdat_rotator  = rotate(rdat_normdata, ack64 ? StartAddress[2:0] : {dword_cnt[0],StartAddress[1:0]});

	// Byte enables for save register and put data
   wire[63:0] rdat_be  = ack64 ?        64'hffffffffffffffff >> {StartAddress[2:0],3'b0} 
			      : rotate(64'h00000000ffffffff, {dword_cnt[0], StartAddress[1:0]});

// the first data transfer is special!
   wire[2:0] rdat_seldist = ~trdy_l & ~trdy ? 0 : 8 - StartAddress[2:0];


	// This represents how many bytes on the bus are valid
	// This gets messy because of the case where we need 3 cycles to fetch data to fill a dpram word (6-8 bytes)
	// eg: read 6 bytes from address 3; cy1: get byte 1 cy2: get bytes 2-5 cyc3: get byte 6
	// During that last cycle we have to be careful to NOT select new data for bytes 7,8,9
	// of ccourse none of this applies to cases < 6, so that case is excluded
   wire[1:0] rdat_nbd = 3'h4 - StartAddress[1:0];
   assign    rdat_newbytes[63:32] = 0;
   assign    rdat_newbytes[31:0]  = (dword_cnt[0] || XIF_get_size<4)? 32'hffffffff : 32'hffffffff >> {rdat_nbd, 3'b0};
	// Select which bytes come from live bus vs save reg
   wire[63:0] rdat_sel = ack64 ?        64'hffffffffffffffff << {rdat_seldist,3'b0} 
			      : rotate(rdat_newbytes, {dword_cnt[0], StartAddress[1:0]});

	// output to XIF_core RAM assemble together saved & live bus data
   reg [63:0] rdat_save;
   assign BFM_put_data = (rdat_rotator & rdat_sel) | (rdat_save & ~rdat_sel);

   reg        rdat_newbc;  // record that dword_cnt has changed, to write possible tail data
   wire       BFM_put_dwe  = (rdat_valid | rdat_newbc) & StartRead;  //!!!

	// use register directly for reading, bumped burstcount for writing
   assign  BFM_gp_daddr = StartRead ? rdat_bcount>>3 : trdy_l ? 1 : ((dword_cnt+1)>>1) + 1;

//  Data rotators/mux's for translating between C & HDL sides
	//For writing we do the opposite rotation
	// Naty , eh?      Pipeline empty           OR    every other wait state!?!?
   assign wdat_data    = (ld_getdata & ld_getdata_d) | (trdy_l & ~target_latency_timer[0]) ? XIF_get_data : wdat_data_reg;  // bypass mux

   assign wdat_rotator = shiftleft(wdat_data, wdat_last_data, {XIF_get_address[2]&ack64, XIF_get_address[1:0]});
   //assign wdat_rotator = (wdat_data << ({XIF_get_address[2]&ack64, XIF_get_address[1:0]}*8)) | (wdat_last_data >> (64-{XIF_get_address[2]&ack64, XIF_get_address[1:0]}*8));   

//  Rotate right by dist BYTES (dist*8 bits)

function[63:0] rotate;
input[63:0] a;
input[2:0] dist;
begin
   rotate = a >> (dist*8) | a << ((8-dist)*8);
end
endfunction


function[63:0] shiftleft;
input[63:0] a;
input[63:0] b;
input[2:0] dist;
begin
   shiftleft = (a << (dist*8)) | (b >> (64-dist*8));
end
endfunction



   // Address & Command during the second address phase in a DAC
   assign dac_address = StartAddress[63:32];
   assign dac_command = (DualAddrCycle) ? PCI_DAC : Command;
   
   // For 64 bit transfer starting at odd DWORD, the first DWORD and 
   // corresponding byte enables are to be applied on ad[31:0] and cbe[3:0] 
   // during first data phase. This is so that a 32 bit target still gets 
   // the first data

   assign ad_master_data = (addr_ph)          ? StartAddress[31:0] : 
		      (addr_ph_1)             ? dac_address[31:0]  :
		      (attr_ph)               ? Attribute          : 
		      (~data_ph & StartWrite) ? 32'hffffffff       :
		      (~StartWrite)           ? 32'h0              :
		      ((stable_dataph_cnt == 0) & StartAddress[2] & drive_64 & (num_data_ph_left>1))
		      			      ? 32'hdeadbeef       :  // 1st word of unaligned burst?!?
		      (!ack64 & dword_cnt[0]) ?	wdat_rotator[63:32]:
						wdat_rotator[31:0];
   assign ad_master_en = (master) 		 ?
			 ((addr_ph)              ? 1'b1 : 
		         (addr_ph_1)             ? 1'b1 :
		         (attr_ph)               ? 1'b1 : 
		         (~data_ph & StartWrite) ? 1'b1 :
		         (~StartWrite)           ? 1'b0 :
			 ((stable_dataph_cnt == 0) & StartAddress[2] & drive_64 & (num_data_ph_left>1)) ? 1'b1 :
		         (!ack64 & dword_cnt[0]) ? 1'b1 : 1'b1) : 1'b0;
   assign ad[31:0]     = (ad_master_en) ? ad_master_data : 32'bz;

   assign ad_h_master_data = ((addr_ph | addr_ph_1) & DualAddrCycle) 
						? StartAddress[63:32] : 
			attr_ph 		? 32'b0 : 
			!(data_ph & StartWrite) ? 32'b0 : 
			((stable_dataph_cnt == 0) & StartAddress[2])
						? wdat_rotator[63:32] :  // 1st word of unaligned access
						  wdat_rotator[63:32];
   assign ad_h_master_en = (master & drive_64) 				? 
			   (((addr_ph | addr_ph_1) & DualAddrCycle)     ? 1'b1 :
			   (attr_ph) 		                        ? 1'b0 :
			   (!(data_ph & StartWrite)) 	                ? 1'b0 : 
			   ((stable_dataph_cnt == 0) & StartAddress[2]) ? 1'b1 : 1'b1) : 1'b0;
   assign ad[63:32] = (ad_h_master_en) ? ad_h_master_data : 32'bz;


   // For Config writes byte enables are driven valid during attribute phase,
   // and driven high during the data phase
   // For others, byte count is sent on CBE during attribute phase

   assign DWORDaccess = CfgAccess | IntAck | IOAccess;


   wire [3:0]
     cbe_master_data = (addr_ph) ? dac_command[3:0] : 
		       (addr_ph_1) ? Command[3:0] :
		       (attr_ph) ? (DWORDaccess ? 4'h0 : ByteCount[11:8]) :
		       (data_ph & DWORDaccess) ? 4'hf :
		       !(data_ph & StartWrite) ? 
		       (StartWrite ? 4'hf : (StartRead ? 4'hf : 4'h0)) : 
		       ((stable_dataph_cnt == 0) & StartAddress[2] & drive_64 & (num_data_ph_left==1)) ?
		       byte_en_h[3:0] : byte_en_l[3:0];
   wire
     cbe_master_en   = (addr_ph) ? 1'b1 : 
		       (addr_ph_1) ? 1'b1 :
		       (attr_ph) ? (DWORDaccess ? 1'b1 : 1'b1) :
		       (data_ph & DWORDaccess) ? 1'b1 :
		       !(data_ph & StartWrite) ? 
		       (StartWrite ? 1'b1 : (StartRead ? 1'b1 : 1'b0)) : 
		       ((stable_dataph_cnt == 0) & StartAddress[2] & drive_64 & (num_data_ph_left==1)) ?
		       1'b1 : 1'b1;
   wire [3:0]
           cbe_data  = (master) ? cbe_master_data : 4'b0;
   wire
           cbe_en    = (master && cbe_master_en) ? 1'b1 : 1'b0;
   assign cbe[3:0]   = cbe_en ? cbe_data : 4'bz;

   wire [3:0]
   cbe_h_master_data = ((addr_ph | addr_ph_1) & DualAddrCycle) ?  
			      Command : attr_ph ? 4'h0 : 
			      !(data_ph & StartWrite) ? 4'b0 : byte_en_h ;
   wire
   cbe_h_master_en   = ((addr_ph | addr_ph_1) & DualAddrCycle) ?  
			      1'b1 : attr_ph ? 1'b0 : 
			      !(data_ph & StartWrite) ? 1'b0 : 1'b1 ;
   wire [3:0]
         cbe_h_data  = (master & drive_64) ? cbe_h_master_data : 4'b0;
   wire
         cbe_h_en    = (master & drive_64 & cbe_h_master_en) ? 1'b1 : 1'b0;
   assign cbe[7:4]   = cbe_h_en ? cbe_h_data : 4'bz;


   //------------------------------------------//
   //-------- Parity generation logic ---------//
   //------------------------------------------//

   assign dataparity  = ^{ad[31:0],byte_en_l[3:0]};


   assign addrparity  = addr_ph ? ^{StartAddress[31:0],dac_command[3:0]} : 
			addr_ph_1 ? ^{StartAddress[63:32],Command[3:0]} : 1'b0;

   assign attrparity  = ^{Attribute[31:0],ByteCount[11:8]};


   assign dataparity64 = ^{ad[63:32],byte_en_h[3:0]};


   assign addrparity64 = ^{StartAddress[63:32],Command[3:0]};


   //-------- parity for 32 bit transfers ------//
   always @ (posedge clk)
      begin
	 if (addr_ph | addr_ph_1)
	    parity <= corrupt_addr_parity^addrparity;
	 else if (attr_ph)
	    parity <= corrupt_attr_parity^attrparity;
	 else if (data_ph)
	    parity <= corrupt_data_parity^dataparity;
      end


   //-------- parity for 64 bit transfers ------//
   always @ (posedge clk)
      begin
	 if (addr_ph | addr_ph_1)
	    parity64 <= corrupt_addr_parity^addrparity64;
	 else if (attr_ph)
	    parity64 <= 0;
	 else if (data_ph)
	    parity64 <= corrupt_data_parity^dataparity64;
      end


   //------------------------------------------//
   //----- Parity Error detection logic -------//
   //------------------------------------------//


   always @ (posedge clk)
      begin
	 
	 // Enable driving perr line
	 if (data_ph_1 & StartRead_1 & master_1)
	    rd_par_en <= 1;
	 else
	    rd_par_en <= 0;

	 rd_par_en_2 <= rd_par_en_1;
	 rd_par_en_1 <= rd_par_en;

	 // Check read parity for error
	 if (irdy_int & trdy & devsel & StartRead_1)
	    check_rd_parity <= 1;
	 else
	    check_rd_parity <= 0;


	 // Expected parity 
	 data_chk_par <= ^{ad_int[31:0],cbe_int[3:0]};
	 data_chk_par_h <= ^{ad_int[63:32], cbe_int[7:4], ~drive_64_d};
	 

	 // Assert parity error signal
	 if (check_rd_parity & ((data_chk_par != par_int) || 
				((data_chk_par_h != par64_int) && ack64_d)))
	    perr <= 1;
	 else
	    perr <= 0;
      end



   //----- Delayed version of the PCI-X signals ----//
   
   always @ (posedge clk) begin
      if (!rst_l) begin
	 frame_d <= 0;
	 irdy_d  <= 0;
	 frame_delayed <= 0;
      end 
      else begin
	 frame_d <= frame_int;
	 irdy_d  <= irdy_int;
	 frame_delayed <= frame;
      end
   end


   //------------------------------------------//
   //----- Collect read data in an array ------//
   //------------------------------------------//

   always @ (posedge clk) begin
      if (irdy_int && trdy && StartRead_1) begin
	 if (word_address_1[0] && ack64)
	    rd_data[dword_cnt_del] <= ad_int[63:32];
	 else
	    rd_data[dword_cnt_del]     <= ad_int[31:0];
	    rd_data[dword_cnt_del + 1] <= ad_int[63:32];
      end
   end


   
   //------------------------------------------//
   //----- Byte enable Generation login  ------//
   //------------------------------------------//


   // This is high during the first data phase
   assign first_data = (dataph_cnt == 0);
   
   // This is high during the last data phase
   assign last_data  = (dataph_cnt == (TotalNumDataPh - 1) );

   assign transaction_over = (stable_dataph_cnt == TotalNumDataPh);
   
   
   // Generate the Byte Enables
   always @ (last_data or first_data or StartAddress or EndAddress)
      begin
	 // For the first data phase, all bytes below the starting
	 // address are disabled (byte enable is high)
	 
	 if (first_data) begin
	    case (StartAddress[1:0])
	      0 : byte_en1 <= 4'h0;
	      1 : byte_en1 <= 4'h1;
	      2 : byte_en1 <= 4'h3;
	      3 : byte_en1 <= 4'h7;
	    endcase
	 end
	 else  byte_en1 <= 4'h0;
	 
	 // For the last data phase, all bytes after the ending
	 // address are disabled (byte enable is high)
	 
	 if (last_data) begin
	    case (EndAddress[1:0])
	      0 : byte_en2 <= 4'he;
	      1 : byte_en2 <= 4'hc;
	      2 : byte_en2 <= 4'h8;
	      3 : byte_en2 <= 4'h0;
	    endcase
	 end
	 else byte_en2 <= 4'h0;
      end


   always @ (drive_64 or byte_en1 or byte_en2 or EndAddress or StartAddress or 
	     first_data or last_data or transaction_over or StartWrite or StartRead or
	     DWORDaccess or SplitComplete)
      begin
	 
	 // For the case when there is one data phase
	 // byte enables before the start addr and after end addr
	 // are driven high

	 // For read transactions byte enables are all asserted
	 if (StartRead ||  SplitComplete || DWORDaccess)
	    begin
	       byte_en_l <= 4'hf;
	       byte_en_h <= 4'hf;
	    end
	 else if (StartWrite && !transaction_over)
	    begin
	       if (drive_64) begin // 64 bit data transfer
		  case ({first_data, last_data})
		    2'b00 : begin
		       byte_en_l <= 4'h0;
		       byte_en_h <= 4'h0;
		    end
		    2'b01 : begin
		       if (EndAddress[2]) begin
			  byte_en_l <= 4'h0;
			  byte_en_h <= byte_en2;
		       end
		       else begin
			  byte_en_l <= byte_en2;
			  byte_en_h <= 4'hf;
		       end
		    end
		    2'b10 : begin
		       if (StartAddress[2]) begin
			  byte_en_l <= 4'hf;
			  byte_en_h <= byte_en1;
		       end
		       else begin
			  byte_en_l <= byte_en1;
			  byte_en_h <= 4'h0;
		       end
		    end
		    2'b11 : begin
		       if (StartAddress[2] == EndAddress[2]) begin
			  if (StartAddress[2]) begin
			     byte_en_h <= byte_en1 | byte_en2;
			     byte_en_l <= 4'hf;
			  end
			  else begin
			     byte_en_l <= byte_en1 | byte_en2;
			     byte_en_h <= 4'hf;
			  end
		       end
		       else begin
			  byte_en_l <= byte_en1;
			  byte_en_h <= byte_en2;
		       end
		    end
		  endcase
	       end
	       else begin // 32 bit transfer
		  byte_en_l <= byte_en1 | byte_en2;
		  byte_en_h <= 4'hf;
	       end
	    end
	 else 
	    begin
	       byte_en_l <= 4'hf;
	       byte_en_h <= 4'hf;
	    end
      end

   //------------------------------------------//
   //---------- Configuration register --------//
   //------------------------------------------//


   assign enableAutoRetry       = configReg[0];
   assign exit_trgt_latency_exp = configReg[1];
   assign corrupt_data_parity   = configReg[2];
   assign corrupt_addr_parity   = configReg[3];
   assign corrupt_attr_parity   = configReg[4];
   assign mstr_latency_timer_config[8:0]  = configReg[16:8];


   

   //------------------------------------------//
   //---------- PCIX State Machine       ------//
   //------------------------------------------//


   always @ (posedge clk) begin
      word_address_1 <= word_address;
      
      if (!rst_l) begin
	 
	 frame <= 0;
	 irdy <= 0;
	 req <= 0;
	 req64 <= 0;
	 drive_64 <= 0;
	 master <= 0;
	 master_1 <= 0;
	 data_ph <= 0;
	 data_ph_1 <= 0;
	 attr_ph <= 0;
	 addr_ph <= 0;
	 addr_ph_1 <= 0;
	 adbcnt  <= 1;
	 byte_cnt  <= 0;
	 dataph_cnt <= 0;
	 devsel_cnt <= 0;
	 dword_cnt <= 0;
	 Pcix_State <= IDLE;
         BFM_xrun   <= 0;
	 Pcix_Status <= 0;
	 transaction_done <= 0;
	 addr_setup_cntr <= 0;
	 word_address <= 0;
	 OrigAddress <= 0;
	 LastAddress <= 0;

	 ld_getdata    <= 0;
	 ld_getdata_d  <= 0;
	 ld_getdata_i  <= 0;
	 wdat_data_reg <= 0;
	 wdat_last_data<= 0;

	 rdat_gp_daddr <= 0;
	 rdat_bcount   <= 0;
	 rdat_bincr    <= 0;

         stable_dword_cnt <= 0;
         stable_dataph_cnt <= 0;

      end else begin

	 dword_cnt_del <= dword_cnt ;
	 data_ph_1     <= data_ph;
	 StartRead_1   <= StartRead;
	 StartWrite_1  <= StartWrite;
	 drive_64_d    <= drive_64;

	 if (master && !irdy_l && !trdy_l) begin
	    stable_dword_cnt  <= (DWORDaccess || (TotalNumDataPh == 1)) ? dword_cnt :
				 (drive_64 && ack64_l && (dataph_cnt==0) && StartAddress[2]) ? dword_cnt :
				 ((!ack64_l && (!StartAddress[2] || (dataph_cnt != 0))) ? (dword_cnt + 2) : (dword_cnt + 1));
	    stable_dataph_cnt <= (drive_64 && ack64_l && (dataph_cnt==0) && StartAddress[2]) ? dataph_cnt :
				 (dataph_cnt + 1);
	 end

	 master_1 <= master;
	 transaction_done_del <= transaction_done;

         ld_getdata   <= ~trdy_l & (ack64 | ~dword_cnt[0]) &
		         (Pcix_State == WAIT_DEVSEL | Pcix_State == DATA0 | Pcix_State == BURST | Pcix_State == ADB_STATE);
	 ld_getdata_d <= ld_getdata;
	 
	 if (ld_getdata_i | (ld_getdata & ld_getdata_d)) begin  // Initial data or bypass load
	    wdat_data_reg <= XIF_get_data;
	 end
	 
	 if (ld_getdata_i) begin  // Initial data only
	    wdat_last_data<= XIF_get_data;
	 end

	       // generate put_data control signals
	       if (rdat_valid) begin
//
// Addressing & writing get tricky because of unaligned accesses
// We have to figure out what address to write to next cycle, based on
// the access address, size & op-mode (64/32).  An unaligned access causes 
// us to have to do an "extra" write to the xif_core memory, since the initial
// write doesn't fill the first word, then after the second write, there could be 
// additional data that overflows into the second RAM word.  I hope that is clear....
//
		    rdat_gp_daddr <= (dword_cnt + rdat_be[63] + ack64) >> 1;  // hang onto gp_daddr until after this cycle
		    rdat_bcount   <= rdat_bcount + rdat_bincr;
		    rdat_newbc    <= (rdat_bcount[2:0] + rdat_bincr) > 4'h7; // indicates that gp_daddr will change next cycle
//
			// Load save register according to be (byte enable) bits
		    rdat_save <= (rdat_rotator & rdat_be) | (rdat_save & ~rdat_be);
    		    rdat_bincr<= ack64 ? 4'h8 : 4'h4;  // remaining xfers either 4 or 8 bytes
	       end else begin
		    rdat_newbc <= 0;
		    rdat_bincr <= ~ack64_l ? 4'h8 - StartAddress[2:0]  : 4'h4 - StartAddress[1:0]; // 1st xfer # bytes
	       end


	 case (Pcix_State)
	   IDLE  : begin
	      rdat_gp_daddr     <= 0;
	      master            <= 0;
	      adbcnt            <= 1;
	      dataph_cnt        <= 0;
	      stable_dataph_cnt <= 0;
	      devsel_cnt        <= 0;
	      byte_cnt          <= 0;
	      dword_cnt         <= 0;
	      transaction_done  <= 0;
	      StartAddress      <= XIF_get_address;

//	      ld_getdata     <= 0;
//	      wdat_last_data <= wdat_data;
//	      wdat_data_reg  <= XIF_get_data;
	      rdat_bcount    <= 0;

	      if (XIF_xav & (StartWrite | StartRead) && mode_pcix) begin
		 OrigAddress <= XIF_get_address[2:0];
		 EndAddress  <= XIF_get_address[3:0] + ByteCount - 1;
		 req <= 1;
		 Pcix_State <= REQ;
		 BFM_xrun <= 1;

		 ld_getdata_i <= 1;

	      end else begin
		 if (perr_int) Pcix_Status[DATA_PARITY_ERROR] <= 1'b1;
		 if (serr_int) Pcix_Status[ADDR_PARITY_ERROR] <= 1'b1;
		 BFM_xrun <= 0;
	      end
	   end
	   
	   REQ  : begin
	      ld_getdata_i   <= 0;

	      if (gnt && ((!frame_int & !irdy_int) || (!frame_d & irdy_d))) begin
		 
		 word_address <= StartAddress[63:2];
		 master <= 1;
		 addr_ph <= 1;
		 start_dword_cnt <= dword_cnt;
		 start_byte_cnt <= byte_cnt;
		 
		 if (CfgAccess) begin
		    Pcix_State <= ADR_SETUP;
		    word_address <= StartAddress[63:2];
		    frame <= 0;
		 end 
		 else begin
		    if (DualAddrCycle)
		       Pcix_State <= DAC ; 
		    else
		       Pcix_State <= ADDR ; 
		    
		    frame <= 1;
		    if (Use64bitaccess) req64 <= 1;
		    if (Use64bitaccess) drive_64 <= 1;

		 end
	      end
	      else begin
		 Pcix_State <= REQ;
	      end
	   end

	   REQ_CONT  : begin
	      if (gnt && ((!frame_int & !irdy_int) || (!frame_d & irdy_d))) begin
		 
		 master <= 1;
		 addr_ph <= 1;
		 start_dword_cnt <= dword_cnt;
		 start_byte_cnt  <= byte_cnt;
		 
		 if (CfgAccess) begin
		    Pcix_State <= ADR_SETUP;
		    frame <= 0;
		 end 
		 else begin
		    if (DualAddrCycle)
		       Pcix_State <= DAC ; 
		    else
		       Pcix_State <= ADDR ; 
		    
		    frame <= 1;
		    if (Use64bitaccess) req64 <= 1;
		    if (Use64bitaccess) drive_64 <= 1;

		 end
	      end
	      else begin
		 Pcix_State <= REQ_CONT;
	      end
	   end
	   
	   ADR_SETUP : begin
	      addr_setup_cntr <= addr_setup_cntr + 1;
	      
	      if (!gnt) // if grant is deasserted, stop the transaction
		 begin
		    master <= 0;
		    addr_ph <= 0;
		    req <= 0;
		    Pcix_State <= IDLE;
		    addr_setup_cntr <= 0;
		    //transaction_done <= 1; //jul31
		 end
	      else begin
		 if (addr_setup_cntr == 2'b11) 
		    begin
		       frame <= 1;
		       if (Use64bitaccess) req64 <= 1;
		       if (Use64bitaccess) drive_64 <= 1;

		       Pcix_State <= ADDR;
		    end
		 else 
		    Pcix_State <= ADR_SETUP;
	      end
	   end
	   

	   DAC : begin
	      addr_ph <= 0;
	      addr_ph_1 <= 1;
	      Pcix_State <= ADDR;
	   end
	   
	   ADDR : begin
	      req <= 0;
	      addr_ph <= 0;
	      addr_ph_1 <= 0;
	      attr_ph <= 1;
	      LastAddress <= StartAddress[2:0];
	      Pcix_State <= ATTR;
	   end
	   
	   ATTR : begin
	      attr_ph <= 0;
	      Pcix_State <= TRN;
	   end
	   
	   TRN  : begin
	      irdy <= 1;
	      attr_ph <= 0;
	      data_ph <= 1;
	      Pcix_Status <= 0;
	      stable_dword_cnt <= dword_cnt;
	      Pcix_State <= (!devsel) ? WAIT_DEVSEL : DATA0;
	      if (ack64_l) drive_64 <= 0;
	      
	   end
	   
	   WAIT_DEVSEL : 
	      begin 
		 if (!devsel) begin
		       devsel_cnt <= devsel_cnt + 1;
		       if (devsel_cnt == 4) begin
			     frame <= 0;
			     req64 <= 0;
			     irdy  <= 0;
			     req   <= 0;
			     Pcix_State <= IDLE;
			     transaction_done <= 1;
			     data_ph <= 0;
			     Pcix_Status[15:0] <= STATUS__MASTER_ABORT;
			     //$display("%m: Master abort  addr: %h", StartAddress);
		       end
		 end else begin
		       Pcix_State <= DATA0;
		       if (!ack64) drive_64 <= 0;

		       byte_cnt_func;
		       //total_data_ph(ByteCount, StartAddress[2:0], (~req64 & ~ack64),TotalNumDataPh);
		 end
		 // if (!perr_int) Pcix_Status[DATA_PARITY_ERROR] <= 1'b1;
		 if (serr_int) Pcix_Status[ADDR_PARITY_ERROR] <= 1'b1;
	      end
	   
	   
	   DATA0 : begin
              //$display("%m: DATA0 STATE ENTERED\n");
	      if (!enableAutoRetry || devsel || !stop || !trdy)
		 byte_cnt_func;

	      casex ({devsel, stop, trdy})
		3'b111 : begin		// Disconn on next adb
		   //$display("%m: DISCONNECT ADB TASK CALL\n");
		   Pcix_State <= ADB_STATE;
		   disconnect_adb;
		end
		3'b110 : begin		// Retry -- no data
		   //$display("%m: RETRY\n");
		   frame <= 0;
		   req64 <= 0;
		   drive_64 <= 0;
		   irdy <= 0;
		   data_ph <= 0;

		   if (enableAutoRetry) begin
			 req <= 1;
			 master <= 0;
			 adbcnt  <= 1;
			 dataph_cnt <= 0;
			 stable_dataph_cnt <= 0;
			 devsel_cnt <= 0;
			 byte_cnt  <= start_byte_cnt;
			 dword_cnt <= start_dword_cnt;
			 word_address <= StartAddress[63:2];
			 Pcix_State <= REQ;
//			 ld_getdata <= 1;
		   end else begin
			 req   <= 0;
			 transaction_done <= 1;
			 Pcix_State <= IDLE ;
			 Pcix_Status[15:0] <= STATUS__TARGET_RETRY;
			 if (!StartAddress[1:0])
			    Pcix_Status[31:22] <= dword_cnt[9:0] + 1;
			 else
			    Pcix_Status[31:22] <= dword_cnt[9:0];
			 //$display("%m: Target retry  addr: %h", StartAddress);
//			 ld_getdata <= 0;
		   end
		end
		3'b101 : begin		// Data xfer
	           //$display("%m: DATA XFER\n");
		   Pcix_State <= BURST;
		   normal_data_trf;
		end
		3'b100 : begin		// Wait state
	           //$display("%m: WAIT STATE\n");
		   Pcix_State <= DATA0;
//	           ld_getdata <= 0;
		   if (target_latency_timer > 16) begin
			 // If programmed to exit on target latency timer expiry
			 if (exit_trgt_latency_exp) begin
			       frame <= 0;
			       req64 <= 0;
			       drive_64 <= 0;
			       irdy <= 0;
			       req   <= 0;
			       data_ph <= 0;
			       transaction_done <= 1;
			       Pcix_State <= IDLE ;
			       if (!StartAddress[1:0])
				  Pcix_Status[31:22] <= dword_cnt[9:0] + 1;
			       else
				  Pcix_Status[31:22] <= dword_cnt[9:0];
			 end
			 Pcix_Status[TARGET_LATENCY_EXP] <= 1'b1;
			 $display("%m: Target initial latency exceeded addr: %h", StartAddress);
		   end
		end
		3'b0xx : begin		// WTF?
		   //$display("%m: WTF\n");
		   frame <= 0;
		   req64 <= 0;
		   drive_64 <= 0;
		   irdy <= 0;
		   req   <= 0;
		   data_ph <= 0;
//	           ld_getdata <= 0;
		   case ({stop, trdy})
		     2'b11 : 		// Single Data phase disconnect
			begin
			   Pcix_Status[15:0] <= STATUS__SINGLE_DATA_DISCONNECT;
			   if (enableAutoRetry && (num_data_ph_left>0)) begin
			      if (req64 && !ack64 && StartAddress[2]) begin
				 // First data did not count, restore last start address
				 StartAddress <= {word_address[61:0], 2'b00};  //!!! ??? why the heck not!!!
				 wdat_data_reg <= XIF_get_data;			//!!! used up first data!?!?
			         dword_cnt <= dword_cnt+1;
			      end else begin
				 StartAddress <= {word_address[61:0], 2'b00};
			      end
			      dataph_cnt <= 0;
			      stable_dataph_cnt <= 0;
			      req <= 1;
			      Pcix_State <= REQ_CONT;
			      master <= 0;
			   end // if (enableAutoRetry && (num_data_ph_left>0))
			   else begin
			      transaction_done <= 1;
			      Pcix_State <= IDLE ;
			   end
			end	    
		     2'b10 :		// Target abort
			begin
			   Pcix_Status[15:0] <= STATUS__TARGET_ABORT;
			   transaction_done <= 1;
			   Pcix_State <= IDLE ;
			end
		     2'b01 : 		// Split response
			begin
			   Pcix_Status[15:0] <= STATUS__TARGET_SPLIT_RESPONSE;
			   transaction_done <= 1;
			   Pcix_State <= IDLE ;
			end
		     2'b00 : 		// Not allowed!
			begin
			   Pcix_Status[15:0] <= STATUS__ILLEGAL_TARGET_RESPONSE;
			   transaction_done <= 1;
			   Pcix_State <= IDLE ;
			end
		   endcase
		   if (!StartAddress[1:0]) Pcix_Status[31:22] <= dword_cnt[9:0] + 1;
		   else  Pcix_Status[31:22] <= dword_cnt[9:0];
		end
	      endcase
	      
	      // if (!perr_int) Pcix_Status[DATA_PARITY_ERROR] <= 1'b1;
	      if (serr_int) Pcix_Status[ADDR_PARITY_ERROR] <= 1'b1;
	   end
	   
	   
	   BURST : begin
	      if (master_latency_timer_exp && !gnt && !stop) begin 
		    Pcix_State <= LATENCY_TIMER_EXP;
		    disconnect_adb;
		    byte_cnt_func;
	      end else begin
		 casex ({devsel, stop, trdy})
		   3'b111 : begin
		      Pcix_State <= ADB_STATE;
		      disconnect_adb;
		      byte_cnt_func;
		   end
		   3'b110 : begin
		      frame <= 0;
		      req64 <= 0;
		      drive_64 <= 0;
		      irdy <= 0;
		      req   <= 0;
		      data_ph <= 0;
		      transaction_done <= 1;
//	              ld_getdata <= 0;
		      Pcix_State <= IDLE ;
		      Pcix_Status[15:0] <= STATUS__ILLEGAL_TARGET_RETRY;
		      if (!StartAddress[1:0]) Pcix_Status[31:22] <= dword_cnt[9:0] + 1;
		      else  Pcix_Status[31:22] <= dword_cnt[9:0];
		      $display("Illegal retry termination");
		   end
		   3'b101 : begin
		      Pcix_State <= BURST;
		      normal_data_trf;
		      byte_cnt_func;
		   end
		   3'b100 : begin
		      frame <= 0;
		      req64 <= 0;
		      drive_64 <= 0;
		      irdy <= 0;
		      req   <= 0;
		      data_ph <= 0;
		      transaction_done <= 1;
//	              ld_getdata <= 0;
		      Pcix_State <= IDLE ;
		      Pcix_Status[15:0] <= STATUS__ILLEGAL_TARGET_WAIT;
		      if (!StartAddress[1:0]) Pcix_Status[31:22] <= dword_cnt[9:0] + 1;
		      else  Pcix_Status[31:22] <= dword_cnt[9:0];
		      $display("Illegal wait state insertion");
		   end
		   3'b0xx : begin
		      frame <= 0;
		      req64 <= 0;
		      drive_64 <= 0;
		      irdy <= 0;
		      req   <= 0;
		      data_ph <= 0;
		      transaction_done <= 1;
//	              ld_getdata <= 0;
		      Pcix_State <= IDLE ;
		      case ({stop, trdy})
			2'b11 : Pcix_Status[15:0] <= STATUS__ILLEGAL_TARGET_SDD;
			2'b10 : Pcix_Status[15:0] <= STATUS__TARGET_ABORT;
			2'b01 : Pcix_Status[15:0] <= STATUS__ILLEGAL_TARGET_SPLIT_RESP;
			2'b00 : Pcix_Status[15:0] <= STATUS__ILLEGAL_TARGET_RESPONSE;
		      endcase
		      if (!StartAddress[1:0]) Pcix_Status[31:22] <= dword_cnt[9:0] + 1;
		      else  Pcix_Status[31:22] <= dword_cnt[9:0];
		   end
		 endcase 
	      end
	      if (perr_int) Pcix_Status[DATA_PARITY_ERROR] <= 1'b1;
	      if (serr_int) Pcix_Status[ADDR_PARITY_ERROR] <= 1'b1;
	   end          
	   
	   ADB_STATE   : begin
	      case ({devsel, stop, trdy})
		3'b111 : begin
		   Pcix_State <= ADB_STATE;
		   disconnect_adb;
		   byte_cnt_func;
		end
		default : begin 
		   frame <= 0;
		   req64 <= 0;
		   drive_64 <= 0;
		   irdy <= 0;
		   req   <= 0;
		   data_ph <= 0;

		   if (enableAutoRetry && (num_data_ph_left>0)) begin
		      req <= 1;
		      StartAddress <= {word_address[61:0], 2'b00};
		      master <= 0;
		      Pcix_State <= REQ_CONT;
		   end // if (enableAutoRetry && (num_data_ph_left>0))
		   else begin
		      transaction_done <= 1;
		      Pcix_State <= IDLE ;
		   end
		   
		   case ({devsel,stop, trdy})
		     3'b110 : Pcix_Status[15:0] <= STATUS__ILLEGAL_TARGET_RETRY;
		     3'b101 : Pcix_Status[15:0] <= STATUS__ILLEGAL_BURST_AFTER_ADB;
		     3'b100 : Pcix_Status[15:0] <= STATUS__ILLEGAL_TARGET_WAIT;
		     3'b011 : Pcix_Status[15:0] <= STATUS__ILLEGAL_TARGET_SDD;
		     3'b010 : Pcix_Status[15:0] <= STATUS__TARGET_ABORT;
		     3'b001 : Pcix_Status[15:0] <= STATUS__ILLEGAL_TARGET_SPLIT_RESP;
		     3'b000 : Pcix_Status[15:0] <= STATUS__ILLEGAL_TARGET_RESPONSE;
		   endcase
		   if (!StartAddress[1:0]) Pcix_Status[31:22] <= dword_cnt[9:0] + 1;
		   else  Pcix_Status[31:22] <= dword_cnt[9:0];
		end
	      endcase
	      if (perr_int) Pcix_Status[DATA_PARITY_ERROR] <= 1'b1;
	      if (serr_int) Pcix_Status[ADDR_PARITY_ERROR] <= 1'b1;
	   end


	   LATENCY_TIMER_EXP: begin
	      Pcix_State <= LATENCY_TIMER_EXP;
	      disconnect_adb;
	      byte_cnt_func;
	   end
	 endcase
	 
      end //if (!rst_l)
   end //always
   

   //------------------------------------------//
   //-------------- Latency timer -------------//
   //------------------------------------------//

   assign load_latency_timer = frame & !frame_delayed;
   assign master_latency_timer_exp  = (master_latency_timer == 0);

   // Master latency timer logic

   always @ (posedge clk)
      begin
	 if (load_latency_timer)
	    master_latency_timer <= mstr_latency_timer_config[8:0];
	 else if (frame & (master_latency_timer != 0))
	    master_latency_timer <= master_latency_timer - 1'b1;
      end

   // Target latency timer logic

   always @ (posedge clk)
      begin
	 if (load_latency_timer)
	    target_latency_timer <= 6'h01;
	 else if (frame & !trdy)
	    target_latency_timer <= target_latency_timer + 1'b1;
      end


   
   //-----------------------------------------------------//
   //------- Function keeps track of data phase   --------//
   //------- number, word count for read/write    --------//
   //------- Enables data toggling during waits   --------// 
   //-----------------------------------------------------//

   // For config writes and memory DWORD/single data phase writes,
   // D0 should be held one the AD lines till irdy is deasserted
   
   task byte_cnt_func;
      reg correction_64noack;
      
      begin
	 correction_64noack =   (Pcix_State == WAIT_DEVSEL) && drive_64 && !ack64
                             && (StartAddress[2] || (TotalNumDataPh > 1));
	 
	 case ({trdy, dataph_cnt[0]})
	   2'b00 : begin
	      dataph_cnt <= dataph_cnt + 1 - (correction_64noack & StartAddress[2]);
	      word_address    <= ((drive_64) ? (StartAddress[2] ? 
					  (word_address + 1) : (word_address + 2)) : (word_address + 1)) - correction_64noack;
	      byte_cnt   <= byte_cnt + ((4 - word_address[1:0]) * correction_64noack);
	      dword_cnt  <= (DWORDaccess || (TotalNumDataPh == 1)) ? dword_cnt :
			    (((drive_64) ? (StartAddress[2] ? 
					    (dword_cnt + 1) : (dword_cnt + 2)) : (dword_cnt + 1)) 
			     - correction_64noack);
	   end
	   
	   2'b01 : begin
	      dataph_cnt <= dataph_cnt - 1;
	      word_address <= (drive_64) ? (StartAddress[2] ? 
				       (word_address - 1) : (word_address - 2)) : (word_address - 1);
	      byte_cnt   <= byte_cnt - (4 - word_address[1:0]);
	      dword_cnt  <= (DWORDaccess || (TotalNumDataPh == 1)) ? dword_cnt :
			    (drive_64) ? (StartAddress[2] ? 
					  (dword_cnt - 1) : (dword_cnt - 2)) : (dword_cnt - 1);
	   end

	   2'b10 : begin
	      dataph_cnt <= dataph_cnt + 1 - (correction_64noack & StartAddress[2]);
	      word_address    <= ((drive_64) ? (word_address + 2) : (word_address + 1)) - correction_64noack;
	      byte_cnt   <= byte_cnt + 4*correction_64noack;
	      dword_cnt  <= (DWORDaccess || (TotalNumDataPh == 1)) ? dword_cnt :
			    (((drive_64) ? (dword_cnt + 2) : (dword_cnt + 1)) - correction_64noack);
	   end

	   2'b11 : begin
	      dataph_cnt <= dataph_cnt + 1 - (correction_64noack & StartAddress[2]);
	      word_address    <= ((drive_64) ? (word_address + 2) : (word_address + 1)) - correction_64noack;
	      byte_cnt   <= byte_cnt+ 4*correction_64noack;
	      dword_cnt  <= (DWORDaccess || (TotalNumDataPh == 1)) ? dword_cnt :
			    (((drive_64) ? (dword_cnt + 2) : (dword_cnt + 1)) - correction_64noack);
	   end
	 endcase

	 if (~trdy_l & (ack64 | dword_cnt[0])) begin  // every xfer for 64 bit ops, else every other
	    wdat_last_data <= wdat_data;	// When in bypass mode bypass last reg too!!!
	    wdat_data_reg  <= XIF_get_data;
	 end
      end
   endtask
   

   //------------------------------------------//
   //------ Task for Disconnect at ADB  -------//
   //------ Frame & Irdy deasserted here ------//
   //------------------------------------------//
   
   // Allow disconnect if stop is asserted with trdy
   // or if stop is asserted atleast 4 phases away from ADB
   // disconnect1 - for 32 bit transfers
   // disconnect2 - for 64 bit transfers

   assign disconnect1 = (dataph_cnt > adbcnt) ? ((adbcnt == 1) ? 
	(word_address[4:0] <= 5'h1d) : disconnect) : 1'b1;
   assign disconnect2 = (dataph_cnt > adbcnt) ? ((adbcnt == 1) ? 
	(word_address[4:0] <= 5'h1b) : disconnect) : 1'b1;
   assign disconnect = (drive_64) ? disconnect2 : disconnect1;


   // Address boundary for deassertion of frame and irdy
   // For 64 bit transfers and 32 bit transfers

   assign adb_addr_frame[4:0] = (drive_64) ? 5'h19 : 5'h1c;
   assign adb_addr_irdy[4:0]  = (drive_64) ? 5'h1d : 5'h1e;


   // Dont allow disconnect if burst wont cross ADB
   // or if stop is asserted < 4 phases away from ADB
   // num_data_ph_left - Number of data phases left in the transaction
   // num_ph_before_adb - Number of phases after which ADB is reached

   assign num_data_ph_left[9:0] = (TotalNumDataPh[9:0] - dataph_cnt[9:0]);
   /*
    assign num_ph_before_adb[9:0] = (6'h20 - address[4:0]); 
    */

   assign num_ph_before_adb[9:0] = drive_64 ? (5'h10 - word_address[4:1]) : 
				   (6'h20 - word_address[4:0]); 

   /*
    assign dont_disconnect = (adbcnt == 1) ? 
    (StartAddress[6:2] == 5'h1f) ? ((TotalNumDataPh[9:0] == 1) ? 1 : 0) : 
    (num_data_ph_left[9:0] <= num_ph_before_adb[9:0]) : dont_disconnect;
    */

   // This is asserted when the start address was on an ADB, and 
   // a burst just crossed the ADB
   assign last_addr = drive_64 ? 
		      ((StartAddress[6:3] == 4'hf) && (word_address[4:1] == 4'h0)) : 
		      ((StartAddress[6:2] == 5'h1f) && (word_address[4:0] == 5'h00));

   // This is asserted if the number of data phases left in the transaction
   // is less than teh number of phases away from ADB
   // But this does not take care of stop being asserted < 4 data phases away
   // from an ADB....the signal "disconnect" takes care of that.

   assign dont_disconnect = (adbcnt == 1) ? 
			    (last_addr) ? ((TotalNumDataPh[9:0] == 1) ? 1'b1 : 1'b0) : 
			    (num_data_ph_left[9:0] <= num_ph_before_adb[9:0]) : dont_disconnect;

   
   task disconnect_adb;
      begin
	 
	 if ( (!drive_64 & (word_address[4:0] == 5'h1f)) || 
	      (drive_64 & (word_address[4:1] == 4'hf))) 
	    adbcnt <= 1;
	 else
	    adbcnt <= adbcnt + 1;


	 if (dont_disconnect | !disconnect) begin
	       normal_data_trf;
	 end else begin 
//            ld_getdata <= ack64 | dword_cnt[0];
	    if ((((dataph_cnt == 1) && (word_address[4:1] == 4'h0)) || 
		 (word_address[4:0] > adb_addr_irdy)) && disconnect) begin
		    irdy <= 0;
		    req   <= 0;
		    drive_64 <= 0;
		    data_ph <= 0;
		    transaction_done <= 1;
		    Pcix_State <= IDLE;
		    if (stop) Pcix_Status[15:0] <= STATUS__TARGET_DISCONNECT_ADB;
		    else Pcix_Status[15:0] <= STATUS__MASTER_LATENCY_EXP;
		    //##### check this logic !!!!
		    if ((StartAddress[6:2] == 5'h1f) || (|StartAddress[1:0]))
		       Pcix_Status[31:22] <= dword_cnt[9:0];
		    else
		       Pcix_Status[31:22] <= dword_cnt[9:0] + 1;
	    end
	    
	    if ((((dataph_cnt == 1) && (word_address[4:1] == 4'h0)) || 
		 (word_address[4:0] > adb_addr_frame)) && disconnect) begin
		    frame <= 0;
		    req64 <= 0;
	    end
	 end
	 
      end  
   endtask
   

   //------------------------------------------//
   //--- Frame & Irdy control for normal   ----//
   //---   data transfers                  ----//
   //------------------------------------------//
   
   task normal_data_trf;
      begin
	 if (TotalNumDataPh >= 4) begin
	       if (dataph_cnt == TotalNumDataPh - 3)
		  begin
		     frame <= 0;
		     req64 <= 0;
		  end

	       if (dataph_cnt == TotalNumDataPh - 1) begin
		     irdy  <= 0;
		     req   <= 0;
		     drive_64 <= 0;
		     data_ph <= 0;
		     transaction_done <= 1;
		     Pcix_State <= IDLE; // added on 7/11/2001
		     Pcix_Status[15:0] <= STATUS__NORMAL;
		     if (!StartAddress[1:0])
			Pcix_Status[31:22] <= dword_cnt[9:0] + 1;
		     else
			Pcix_Status[31:22] <= dword_cnt[9:0];
	       end
	 end else begin
	       if (trdy) begin
		     frame <= 0;
		     req64 <= 0;
	       end
	       
	       if ((dataph_cnt > 0) && (dataph_cnt >= TotalNumDataPh - 1)) begin
		     irdy <= 0;
		     req   <= 0;
		     drive_64 <= 0;
		     data_ph <= 0;
		     transaction_done <= 1;
		     Pcix_State <= IDLE ;
		     Pcix_Status[15:0] <= STATUS__NORMAL;
		     if (!StartAddress[1:0])
			Pcix_Status[31:22] <= dword_cnt[9:0] + 1;
		     else
			Pcix_Status[31:22] <= dword_cnt[9:0];
	       end
	 end
      end
   endtask
   

   //------------------------------------------//
   //--- Compute total number of data phases --//
   //--- in the transaction                  --//
   //------------------------------------------//

   assign TotalNumDataPh = (Pcix_State == WAIT_DEVSEL) ? total_data_ph(ByteCount, OrigAddress[2:0], (req64 & ack64)) : TotalNumDataPh;


   function [9:0] total_data_ph;
      input [11:0] ByteCount;
      input [2:0]  address;
      input 	   access64;
      
      reg [12:0]   actual_byte_cnt;
      reg [11:0]   temp;
      reg [1:0]    temp1;
      begin

	 if (ByteCount == 0)
	    actual_byte_cnt = 13'h1000;
	 else
	    actual_byte_cnt = {1'b0, ByteCount[11:0]};

	 if (access64)
	    begin
	       temp = (actual_byte_cnt + address[2:0]);
	       temp1 = temp[0] | temp[1] | temp[2];
	       total_data_ph = temp[11:3] + temp1;
	    end
	 else
	    begin
	       temp = (actual_byte_cnt + address[1:0]);
	       temp1 = temp[0] | temp[1];
	       total_data_ph = temp[11:2] + temp1;
	    end
      end
   endfunction
   
   
`endprotect
endmodule
