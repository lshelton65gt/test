
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//*****************************************************************************
// 
// Module Name: carbonx_pcix_target.v
//
// Module Description:
//
// This module is the top level of a PCI-X target. It has the PCI-X state machine
// the PCI2.2 state machine instantiated in it. At reset, the status of 
// {trdy, devsel, stop} indicate the mode of operation of the bus (PCI/PCI-X).
// This is provided as an input to the 2 state machine modules.
//
// All BFM tasks for transport of data between C and verilog are defined, and
// called in this module.
// The PCI/PCI-X state machines are snooped for transactions, and BFM tasks are called
// when required 
// (eg. at the start of a transaction to provide termination type,
//      at the start of transaction for address decode
//      during data phases to pass write data to C-de
//      during data phases to get read data from C-side etc)
//
// The following features are supported :
// * Configuration accesses
// * Write/Read accesses (single and burst)
// * 64-bit data support
// * Dual Address cycle support
// * Parity generation and error detection
// * Retry
// * Target Abort
// * Disconnect at ADB            - for PCI-X only
// * Single Data phase Disconnect - for PCI-X only
// * Split Response               - for PCI-X only
// * Initial wait state insertion 
// * Random wait state insertion between data transfer - for PCI only
//
// *****************************************************************************

module carbonx_pcix_target
   (
    frame_l,
    irdy_l,
    trdy_l,
    devsel_l,
    stop_l,
    ad,
    cbe,
    idsel,
    par,
    par64,
    perr_l,
    serr_l,
    req64_l,
    ack64_l,
    clk,
    rst_l
    );

   
   input frame_l;
   input irdy_l;
   output trdy_l;
   output stop_l;
   output devsel_l;
   input idsel;
   
   inout [63:0] ad;
   input [7:0] 	cbe;
   inout 	par;
   inout 	par64;
   
   inout 	perr_l;
   output 	serr_l;

   input 	req64_l;
   output 	ack64_l;
   
   input 	clk;
   input 	rst_l;

`protect
// carbon license crbn_vsp_exec_xtor_pcix
   
`include "carbonx_pcix_defines.vh"

   reg 		Mode_Pci;
   
   
  // We need this handful of signals to properly configure the
  // transactor interface.
  parameter    
               ClockId   =   1,
               DataWidth =  64,
               DataAdWid =   9, // Max is 15
               GCSwid    =  32, // Minimum is 1.
               PCSwid    =  32; // Minimum is 1.

`define XIF_GCS_DEFAULT_DATA 'h10ff00

`include "xif_core.vh"

  assign
    BFM_clock         =  clk,
    BFM_reset         = 0,
    BFM_interrupt     = 0;


  carbonx_pcix_targetsm PCIX_TARGET_SM(
	// PCI-x signals
	.frame_l(frame_l),  
	.irdy_l(irdy_l),
	.trdy_l(trdy_l),
	.devsel_l(devsel_l),
	.stop_l(stop_l),
	.ad(ad),
	.cbe(cbe),
	.idsel(idsel),
	.par(par),
	.par64(par64),
	.perr_l(perr_l),
	.serr_l(serr_l),
	.req64_l(req64_l),
	.ack64_l(ack64_l),
	
	// system signals
	.clk(clk),
	.rst_l(rst_l),
	.pcix_mode(1'b1),

	.XIF_reset(XIF_reset), 
	.XIF_xav(XIF_xav),
	.XIF_get_status(XIF_get_status),
	.XIF_get_data(XIF_get_data),
	.XIF_get_csdata(XIF_get_csdata),

	.BFM_xrun(BFM_xrun),
	.BFM_put_operation(BFM_put_operation),
	.BFM_put_address(BFM_put_address),
	.BFM_put_size(BFM_put_size),
	.BFM_put_status(BFM_put_status),
	.BFM_put_csdata(BFM_put_csdata),
	.BFM_put_data(BFM_put_data),
	.BFM_gp_daddr(BFM_gp_daddr),
	.BFM_put_dwe(BFM_put_dwe),
	.BFM_put_cphwtosw(BFM_put_cphwtosw),
	.BFM_put_cpswtohw(BFM_put_cpswtohw)

	);

`endprotect
endmodule
