
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// *****************************************************************************
// Module Name: PcixInitiator.v
//
// Module Description:
//
// This module is the top level of a PCI-X initiator. It has the PCI-X state 
// machine instantiated. 
//
// The following features are supported :
// * Configuration accesses
// * Write/Read accesses (single and burst)
// * 64-bit data support
// * Dual Address cycle support
// * Parity generation and error detection
// * Termination types supported : Retry, Target Abort, Disconnect at ADB
//   Single Data phase disconnect, Split response, Master Abort
// * Target inserted Initial wait state is also supported 
// * A status register is updated with the status of the transaction 
//   ( type of termination, parity errors etc), and is passed to the C-side
//
//
// *****************************************************************************

module carbonx_pcix_initiator
   (
    frame_l,
    irdy_l,
    trdy_l,
    devsel_l,
    stop_l,
    req_l,
    gnt_l,
    req64_l,
    ack64_l,
    ad,
    cbe,
    par,
    par64,
    perr_l,
    serr_l,
    int_l,
    clk,
    rst_l
    );
   
   
   /**********************************
    ** List all Input/Output signals **
    **********************************/
   
  inout frame_l;
  inout irdy_l;
  input trdy_l;
  input stop_l;
  input devsel_l;
  inout perr_l;
  
  input serr_l;
  input [3:0] int_l;
  
  output      req_l;
  input       gnt_l;
  
  output      req64_l;
  input       ack64_l;

  inout       par;
  inout       par64;
  inout [63:0] ad;
  inout [7:0] 	cbe;
  
  input 	clk;
  input 	rst_l;
   
`protect
// carbon license crbn_vsp_exec_xtor_pcix
   
`include "carbonx_pcix_defines.vh"

  /**********************************
   ** Internal wires/state
   **********************************/

  wire 	    req_pcix;
  wire 	    TBP_reset;
  reg [4:0] req_tag;

  /*************************************
   * TBP Interface                    **
   *************************************/

  // reset values for cs regs:    dft_cmd      cfg
`define XIF_GCS_DEFAULT_DATA 64'h00000067_00004000
  parameter    
               ClockId   =   1,
               DataWidth =  64,
               DataAdWid =   9, // Max is 15
               GCSwid    =  64, // Minimum is 1.
               PCSwid    =  32; // Minimum is 1.
  //
  // Include XIF.
  //

`include "xif_core.vh"
      
  assign TBP_reset = 0;
   
   
  // !! CS regs
  wire [31:0]   configReg   = XIF_get_csdata[31:00];
  wire [31:0]   default_cmd = XIF_get_csdata[63:32];

  // Assign put cs data to avoid getting warnings from the compiler
  assign BFM_put_csdata[31:0] = 32'h0;

  assign BFM_clock         = clk; 
  assign BFM_reset         = ~rst_l;
  assign BFM_interrupt     = ~int_l;
  assign BFM_put_operation = BFM_NXTREQ;
  assign BFM_put_address   = 0;
  assign BFM_put_size      = XIF_get_size;

  assign req_l = !rst_l ? 1'bz : req_pcix;
   
  // This turns default TBP read and write ops into proper PCI opcdes for both PCI & PCI-X
  wire [31:0] pci_opcode = XIF_get_operation[31]          ? XIF_get_operation :  // Pass through from user specified bits
	                   XIF_get_operation == BFM_WRITE ? {3'b100, req_tag, default_cmd[23:8],default_cmd[27:24],default_cmd[3:0]} :
	                   XIF_get_operation == BFM_READ  ? {3'b100, req_tag, default_cmd[23:8],default_cmd[27:24],default_cmd[7:4]} :
                           0;   // This handles NOP's

   // Internal initialization
   //
   initial begin
      req_tag = 0;
   end

   always @(posedge clk) begin
      if(~rst_l | XIF_reset) begin
	req_tag <= 0;
      end else if (XIF_xav & (XIF_get_operation == BFM_WRITE | XIF_get_operation == BFM_READ)) begin
	req_tag <= req_tag + 1;
      end
   end

   /******************************************
    ** Instantiation of the PCI-X initiator **
    ******************************************/

   carbonx_pcix_initiatorsm #(DataWidth, DataAdWid) PCIX_INIT_SM(
	.frame_l(frame_l),
	.irdy_l(irdy_l),
	.trdy_l(trdy_l),
	.devsel_l(devsel_l),
	.stop_l(stop_l),
	.ad(ad),
	.cbe(cbe),
	.par(par),
	.par64(par64),
	.req_l(req_pcix),
	.gnt_l(gnt_l),
	.req64_l(req64_l),
	.ack64_l(ack64_l),
	.perr_l(perr_l),
	.serr_l(serr_l),
	.int_l(int_l),
	
	.clk(clk),
	.rst_l(rst_l),
	
	.configReg(configReg),
	.mode_pcix(1'b1),
				
	.XIF_reset(XIF_reset), 
	.XIF_xav(XIF_xav),
	.XIF_get_operation(pci_opcode),
	.XIF_get_address(XIF_get_address),
	.XIF_get_size(XIF_get_size),
	.XIF_get_status(XIF_get_status),
	.XIF_get_data(XIF_get_data),

	.BFM_xrun(BFM_xrun),
	.BFM_put_status(BFM_put_status),
	.BFM_put_data(BFM_put_data),
	.BFM_put_cphwtosw(BFM_put_cphwtosw),
	.BFM_put_cpswtohw(BFM_put_cpswtohw),
	.BFM_gp_daddr(BFM_gp_daddr),
	.BFM_put_dwe(BFM_put_dwe)
  );

`endprotect
endmodule // carbonx_pcix_initiator
