
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//*****************************************************************************
// 
// Module Name: carbonx_pcix_targetsm.v
//
// Module Description:
//
//    This module implements the state machine for the PCI-X state machine. 
// Write data is passed to the C-side memory, and read data from the C-side 
// is given back to the PCI-X master. 
// The following features are supported :
// * Configuration accesses
// * Write/Read accesses (single and burst)
// * 64-bit data support
// * Dual Address cycle support
// * Parity generation and error detection
// * Retry
// * Target Abort
// * Disconnect at ADB
// * Single Data phase Disconnect 
// * Split Response
// * Initial wait state insertion
//
// The target termination type is an input from the C-side to the s/m. 
// Programmable number of wait state insertion is possible.
// At the start of the transaction, the address/attribute/command are passed
// to the C-side, and the C-side returns a response, which includes the type
// of termination, and any initial wait state information.
// For writes, data/address/byte enables are passed to C-side memory
// For reads, address/attribute are passed to C-side memory, 
// and read data is returned.
// For split response, the C-side stores the attribute, and later on initiates
// a split completion using the PCIX intitiator module.
//
// *****************************************************************************


module carbonx_pcix_targetsm(
                    // PCI-x signals
                    frame_l,
                    irdy_l,
                    trdy_l,
                    devsel_l,
                    stop_l,
                    ad,
                    cbe,
                    idsel,
                    par,
                    par64,
                    perr_l,
                    serr_l,
                    req64_l,
                    ack64_l,
                    
                    // system signals
                    clk,
                    rst_l,
                    
                    pcix_mode,

	XIF_reset, 
	XIF_xav,
	XIF_get_status,
	XIF_get_data,
	XIF_get_csdata,
	
	BFM_xrun,
	BFM_put_operation,
	BFM_put_address,
	BFM_put_size,
	BFM_put_status,
	BFM_put_csdata,
	BFM_put_data,
	BFM_gp_daddr,
	BFM_put_dwe,
	BFM_put_cphwtosw,
	BFM_put_cpswtohw

                    );

`protect
   
   /**********************************
    ** List all Input/Output signals **
    **********************************/

   input clk;
   input rst_l;
   input pcix_mode;
   
   input frame_l;
   input irdy_l;
   
   output trdy_l;
   output devsel_l;
   output stop_l;
   
   inout  perr_l;
   output serr_l;
   inout  par;
   inout  par64;
   
   inout [63:0] ad; // address of transaction
   input [7:0]  cbe;

   input        req64_l;
   output       ack64_l;

   input        idsel;

   input	XIF_reset; 
   input	XIF_xav;
   input[31:0]	XIF_get_status;
   input[63:0]	XIF_get_data;
   input[31:0]	XIF_get_csdata;
	
   output	BFM_xrun;
   output[31:0]	BFM_put_operation;
   output[63:0]	BFM_put_address;
   output[23:0]	BFM_put_size;
   output[31:0]	BFM_put_status;
   output[31:0]	BFM_put_csdata;
   output[63:0]	BFM_put_data;
   output[8:0]	BFM_gp_daddr;
   output	BFM_put_dwe;
   output	BFM_put_cphwtosw;
   output	BFM_put_cpswtohw;
   
// carbon license crbn_vsp_exec_xtor_pcix

`include "carbonx_pcix_defines.vh"

   /**********************************
    ** Internal registers and wires  **
    **********************************/

   reg          trdy;
   reg          devsel;
   reg          stop;
   reg          frame;
   reg          irdy;
   reg          irdy_dd;
   reg          trdy_clk;
   reg          trdy_dd;
   reg [63:0]   ad_int;
   reg [7:0]    cbe_int;
   reg          par_int;
   reg          par64_int;
   reg          perr_int;
   reg          idsel_int;
   
   reg          slave;
   reg          check_addr_par;
   reg          dual_addr_cycle;
   reg          check_addr_par_1;
   reg          exp_parity;
   reg          exp_parity_1;
   reg          exp_parity_2;
   
   reg          config_rd;
   reg          config_wr;
   reg          split_complete;
   reg          split_complete_1;
   reg          mem_wr;
   reg          mem_wr_d;
   reg          mem_wr_dd;
   reg          mem_rd;
   reg          wr_cmp;
   reg          wr_cmp_dc;  // hold a wr_cmp for a cycle during disconnects
   reg          perr;
   reg          serr;
   reg          perr_en;
   reg          serr_en;
   reg          par_en;
   reg          par64_en;
   reg          ack64;
   reg          ack64_d;
   reg          ack64_dd;
   reg          req64;
   reg          read_parity;
   reg          read_parity64;
   reg [3:0]    config_be;
   reg [63:0]   access_address;
   reg [63:0]   next_access_address;
   reg [31:0]   Attribute;  
   reg [2:0]    StartAddress;
   reg [10:0]   data_phase_count;
   reg [3:0]    initial_wait_cnt;
   reg [3:0]    devsel_cntr;
   reg [11:0]   ByteCount;
   wire[31:0]   simside_rd_data;
   wire[31:0]   simside_rd_data_1;
   reg [28:0]   Requester_attribute;
   
   reg [10:0]   TotalNumDataPh;
   
   wire [3:0]   Target_Response;
   wire [3:0]   NumInitialWait;
   wire [7:0]   num_data_ph;
   wire [3:0]   DevselCount;
   
   reg [3:0]    Pcix_target_state ;
   
   reg          disconnect;

   wire         devsel_int;
   wire [31:0]  cfg_address;

   wire         use_32bit;
   wire 	ad_drive_low;   
   wire		ad_drive_upp;
   
   /*************************************
    * State machine parameters         **
    *************************************/
   
   parameter    IDLE        = 4'h0,
		ADR0        = 4'h1,
                DAC         = 4'h2,
                DEVSEL_WAIT = 4'h3,
                ADDR        = 4'h4,
                WAIT_STATE  = 4'h5,
                DATA        = 4'h6,
                WAIT_FRAME  = 4'h7,
                MABORT      = 4'h8,
                ADB_STATE   = 4'h9,
                TERMINATE   = 4'hA;


   /**********************************
    ** Assigning the PCI-X signals  **
    **********************************/
   
   assign trdy_l = (slave) ? ~trdy : 1'bz;
   assign stop_l = (slave) ? ~stop : 1'bz;
   assign devsel_l = (slave) ? ~devsel : 1'bz;

   //assign ad[31:0] = (((config_rd && devsel) || (mem_rd && trdy)) && slave) ? simside_rd_data : 32'bz;
   assign ad_drive_low = (((config_rd && devsel) || (mem_rd && trdy)) && slave) ? 1'b1 : 1'b0;
   assign ad[31:0] = ad_drive_low ? simside_rd_data : 32'bz;

   //assign ad[63:32] = (mem_rd && slave & ack64 && trdy) ? simside_rd_data_1 : 32'bz;
   assign ad_drive_upp = (mem_rd && slave & ack64 && trdy) ? 1'b1 : 1'b0;
   assign ad[63:32] = ad_drive_upp ? simside_rd_data_1 : 32'bz;

   assign par = (par_en) ? read_parity : 1'bz;
   assign par64 = (par64_en) ? read_parity64 : 1'bz;
   
   assign perr_l = (perr_en) ? ~perr : 1'bz;
   assign serr_l = (serr_en) ? ~serr : 1'bz;

   assign ack64_l = (slave) ? ~ack64 : 1'bz;

   assign cfg_address[31:0] = {26'h0, access_address[5:0]};

/*******************************
 *
 *  XIF interface code
 *
 *******************************/
reg[31:0]  wbe, ldwbe, last_ad;
reg[31:0]  slavestatus;
reg[3:0]   command, addr_incr;  // hold the command nibble for the entire transaction
reg[10:0]  word_count;  // Count number of words transferred
reg[63:0]  start_address;
reg[11:0]  current_address;
wire[7:0]  bemux1 = ~{ack64 ? cbe[7:4]:cbe[3:0], cbe[3:0]}; // Handle 32/64 bit cases
wire[31:0] bemux  = {4{bemux1}};                          // just replicate above 4 times
wire[63:0] wdat_data, wdat_mdata, rdat_data;
wire[8:0]  wdat_addr, rdat_addr;
wire[3:0]  rdat_bump;
wire       addr_in_range, cause_perr;
wire wdat_we;

assign
//  Note the extra bit indicating write completion transaction
    BFM_put_operation = {1'b1,  23'h0, 3'b0, wr_cmp,  command},
//  Reads get the updated address, everything else get the original address
    BFM_put_address   = mem_rd    ? start_address + current_address + rdat_bump : {start_address[63:3], StartAddress[2:0]},
// !!!This is broken, since byte count is available for burst ops and is otherwise == 4
// !!! by the way, reads have a bit of an issue with size, since unaligned accesses might require that
// !!! more bytes be transferred:  byte_count + start_addr[2:0]
// !!! I just added a fixed extra amount just to simplify the expression
    BFM_put_size      = mem_rd    ? ({cbe[3:0], ad[7:0]} + 12'h010)  :  
                        ack64_d   ?  {21'h0, data_phase_count, 3'b0} : // Writes counts num words written * width
                                     {22'h0, data_phase_count, 2'b0}+4,

// For write data transfers the status is the corresponding BE's
// Since there is space for only 32 bits, the first 24 bytes are reigstered in bits 23:0
// bits 31:24 have the LAST 8 BE's.  Thus, any arbitrary set of BE's can be conveyed for transfers
// up to 32 bytes long.  For longer transfers, only the first 24 and last 8 can be disabled.  This
// accomodates arbitrarily aligned contiguous transfers.
    BFM_put_status    = wr_cmp ? wbe : {ad[31:8], 6'h0, slavestatus[1:0]},
    BFM_put_data      = wdat_data,

// Deasserting BFM_xrun initiates a transaction;
// Transactions are initiated at the beginning of every transaction (not DAC's -- the address is accumulated internally)
// as well as at the end of write operations, in order to send the write data up to the C-side
    BFM_xrun = ~(Pcix_target_state == ADR0 | wr_cmp), 		 	 // tail end of Write

	//!!!  No longer being used, since pcix DOES give the size of the transaction
	// This second term is a bit tricky: it is looking for the case where a read
	// is transferring the last word of data of a 32 byte block, since a transaction can hold only 32 bytes
	//		  | ~irdy_l & trdy & mem_rd & (current_address[4:3]==2'B11) & (current_address[2] | ack64)

    BFM_put_csdata    = slavestatus,
    BFM_put_cphwtosw  = mem_wr | config_wr,   // This will be true for write
    BFM_put_cpswtohw  = Pcix_target_state == IDLE,  // This will be true for reads
    BFM_gp_daddr      = mem_wr | config_wr ? wdat_addr : rdat_addr,
    BFM_put_dwe       = wdat_we,

    wdat_we = !irdy_l & trdy & (mem_wr | config_wr),

// This hack calculates whether to bump current_address for read data.
// The initial transaction (in state ADR0) does not bump the address
    rdat_bump         = ((Pcix_target_state == ADR0)) ? 4'h0 : ack64_d ? 4'h8 : 4'h4,

//    This address is used to address the XIF data ram for writes
    wdat_addr = current_address[11:3],

    wdat_data = ack64 ? ad : {ad[31:0], current_address[2] & ~config_wr ? last_ad : ad[31:0]},  // handle 32 bit writes by replication

    // This does some cheating if we are inc'ing the address this cycle...
    rdat_addr = trdy ? current_address[11:3]+(current_address[2] |req64) : current_address[11:3],
    rdat_data = XIF_get_data;



assign
  simside_rd_data   = ~ack64 & current_address[2] ? rdat_data[63:32] :
		                                    rdat_data[31: 0],

// This is ONLY used for 64 bit memory reads -- cfg accesses are always 32 bit
  simside_rd_data_1 = rdat_data[63:32];


// Tear apart the bits of the cs register & get_status word & OR together
assign
	 NumInitialWait  = XIF_get_csdata[7:0]   | XIF_get_status[7:0],  
	 num_data_ph     = XIF_get_csdata[15:8]  | XIF_get_status[15:8], 
	 Target_Response = XIF_get_csdata[19:16] | XIF_get_status[19:16],
         DevselCount     = XIF_get_csdata[23:20] | XIF_get_status[23:20],
         cause_perr      = XIF_get_csdata[29]    | XIF_get_status[29],
         use_32bit       = XIF_get_csdata[30]    | XIF_get_status[30],
	 addr_in_range   = XIF_get_csdata[31]    | XIF_get_status[31];   


   /**********************************
    ** Synchronous Logic             **
    **********************************/
   

   /**********************************
    ***    State Machine           ***
    **********************************/
   
   always @ (posedge clk) begin

      frame     <= ~frame_l;
      irdy      <= ~irdy_l;
      ad_int    <= ad;
      cbe_int   <= cbe;
      par_int   <= par;
      par64_int <= par64;
      perr_int  <= ~perr_l;
      req64     <= ~req64_l;
      idsel_int <= idsel;

      trdy_clk  <= trdy;
      trdy_dd   <= trdy_clk;
      irdy_dd   <= irdy;
      mem_wr_d  <= mem_wr;
      mem_wr_dd <= mem_wr_d;
      ack64_dd  <= ack64_d;
      ack64_d   <= ack64;
      split_complete_1 <= split_complete;

   //----- Generate read data parity -----//
      if (trdy & (mem_rd | config_rd)) begin
            read_parity <= ^{simside_rd_data[31:0], cbe_int[3:0]};
            par_en <= 1;
      end else begin
         par_en <= 0;
      end

      if (trdy & mem_rd & ack64) begin
            read_parity64 <= ^{simside_rd_data_1[31:0], cbe_int[7:4]};
            par64_en <= 1;
      end else begin
         par64_en <= 0;
      end


      if (~rst_l | ~pcix_mode) begin
            data_phase_count <= 0;
            Pcix_target_state <= IDLE;
            config_rd  <= 0;
            config_wr  <= 0;
            mem_rd    <= 0;
            mem_wr    <= 0;
	    wr_cmp    <= 0;
	    wr_cmp_dc <= 0;
            trdy      <= 0;
            devsel    <= 0;
            stop      <= 0;
            slave     <= 0;
            ack64     <= 0;
            disconnect      <= 0;
            config_be       <= 4'hf;
            devsel_cntr     <= 0;
            dual_addr_cycle <= 0;
            check_addr_par  <= 0;
            split_complete  <= 0;
            split_complete_1 <= 0;
            StartAddress    <= 0;
            access_address  <= 0;
            next_access_address = 0;
            initial_wait_cnt <= 0;
            ByteCount       <= 0;
            Requester_attribute <= 0;
            Attribute       <= 0;

	    command    <= 0;
	    word_count <= 0;
      end else begin
		// wbe, ldwbe is a mechanism to save away the first batch of byte enables
		// to be sent up to the C-side for writes.  We save the first 24 (28) byte enables
		// and the last 8 (4) by copying the live cbe into wbe in the appropriate
		// bit positions.  ldwbe tracks which bits are to be loaded during trdys		
	       if (Pcix_target_state == ADDR) begin
		  ldwbe <= (req64 & ~use_32bit) ? 8'hff :   // 64 bit
		            current_address[2]  ? 8'hf0 :   // Unaligned 32 bit
						  8'h0f ;   // Normal 32 bit
		  wbe   <= 0;
	       end else if (trdy & (word_count < (ack64 ? 4 : 8))) begin
		  ldwbe <= ldwbe << (ack64 ? 8 : 4);
	       end

	       if (trdy) begin
		  wbe <= (bemux & ldwbe)  | (wbe & ~ldwbe);
		  last_ad <= ad[31:0];
	       end

		// set on an error, clear on report to C-side
	       slavestatus  <= {30'h0, ({serr,perr} | (BFM_xrun ? slavestatus[1:0] : 2'h0))};
            case (Pcix_target_state)
	      IDLE : begin
                 slave     <= 0;
                 disconnect<= 0;
		 word_count<= 0;
                 trdy      <= 0;
                 stop      <= 0;
                 devsel    <= 0;
                 data_phase_count <= 0;
		 wr_cmp    <= 0;
		 wr_cmp_dc <= 0;

			// Just in case we start a transaction
                 access_address  <=  ad[31:0];  // clear top half of address
                 start_address   <=  {ad[31:3],3'b000};
                 StartAddress    <=  ad[2:0];	// Save away offset of 1st word
		 command         <=  cbe[3:0];  
		 current_address <= {10'b0, ad[2], 2'b0};

                 if (~frame_l) begin
                    case (cbe[3:0])
                      4'ha : begin			// CONFIG READ
		    	 config_rd <= 1;
			 Pcix_target_state <= idsel ? ADR0 : MABORT;
		    	 end
                      4'hb : begin			// CONFIG WRITE
		    	 config_wr <= 1; 
			 Pcix_target_state <= idsel ? ADR0 : MABORT;
		    	 end
                      4'h6,
                      4'h8,
                      4'he : begin			// MEMORY READ
		    	 mem_rd <= 1;
			 Pcix_target_state <= ADR0;
		    	 end
                      4'h7,
                      4'h9,
                      4'hf : begin			// MEMORY WRITE
			 mem_wr <= 1;
			 Pcix_target_state <= ADR0;
		    	 end
                      4'hc : begin			// SPLIT COMPLETION
		    	 split_complete <= 1;
			 Pcix_target_state <= ADR0;
		    	 end
		      4'hd : begin			// DAC
			 if (cbe[7:4] == 4'h7)
			   mem_wr <= 1;		
		         else
			   mem_rd <= 1;
			 Pcix_target_state <= DAC;
		         end
		      default : begin
                 	 config_rd      <= 0;
                 	 config_wr      <= 0;
                 	 mem_rd         <= 0;
                 	 mem_wr         <= 0;
                 	 split_complete <= 0;
			 Pcix_target_state <= ADR0;	// Send anything else up to C
			 end
                    endcase // case(cbe)

                 end else begin  // frame
                    config_rd <= 0;
                    config_wr <= 0;
                    mem_rd    <= 0;
                    mem_wr    <= 0;
                    split_complete <= 0;
                    Pcix_target_state <= IDLE;
                 end // else: !if(frame)
	      end  // ADR0

	      DAC : begin
                    check_addr_par        <= 1;
                    Pcix_target_state     <= ADR0;
                    dual_addr_cycle       <= 1;      //!!!!!  check this out!!!
                    access_address[63:32] <= ad[31:0];
                    start_address[63:32]  <= ad[31:0];
		    command               <= cbe[3:0];
	      end

              ADR0 : begin
                    check_addr_par  <= 1;
//!!!   	    check_address_range({32'h0, ad_int[31:0]}, cbe_int[3:0], PciAddrInRange, use_32bit);
		    if (   (((cbe_int[3:0] == 4'ha) || (cbe_int[3:0] == 4'hb)) && idsel_int)
                        || (((cbe_int[3:0] == 4'h6) || (cbe_int[3:0] == 4'h8) || (cbe_int[3:0] == 4'he) || 
                             (cbe_int[3:0] == 4'h7) || (cbe_int[3:0] == 4'h9) || (cbe_int[3:0] == 4'hf) ||
                             (cbe_int[3:0] == 4'hc)) && addr_in_range)) begin
		    
                       slave <= 1;
                       Attribute <= ad;
                       config_be <= cbe[3:0];

                       case (cbe_int[3:0])
                         4'ha : begin		// CONFIG READ
	                    ByteCount     <= 4;
			    TotalNumDataPh <= 1;
			 end
                         4'hb : begin		// CONFIG WRITE
	                    ByteCount     <= 4;
			    TotalNumDataPh <= 1;
			 end
                         4'h6,
                         4'h8,
                         4'he : begin		// MEMORY READ
                            ByteCount     <= {cbe[3:0],ad[7:0]};
   		            TotalNumDataPh <= ComputeTotalDataPhase(StartAddress[2:0], {cbe[3:0],ad[7:0]}, (req64 & ~use_32bit));
			 end
                         4'h7,
                         4'h9,
                         4'hf : begin		// MEMORY WRITE
                            ByteCount     <= {cbe[3:0],ad[7:0]};
   		            TotalNumDataPh <= ComputeTotalDataPhase(StartAddress[2:0], {cbe[3:0],ad[7:0]}, (req64 & ~use_32bit));
			 end
                         4'hc : begin		// SPLIT COMPLETION
                            Requester_attribute <= ad_int[28:0];
                            ByteCount     <= {cbe[3:0],ad[7:0]};
   		            TotalNumDataPh <= ComputeTotalDataPhase(StartAddress[2:0], {cbe[3:0],ad[7:0]}, (req64 & ~use_32bit));
			 end
                       endcase // case(cbe)
                       
		       initial_wait_cnt  <= NumInitialWait;
                       if (DevselCount < 2) begin
                             devsel    <= 1;
                             ack64     <= req64 & ~use_32bit;
			     addr_incr <= req64 & ~use_32bit ? 8 : 4;
                             Pcix_target_state <= NumInitialWait ? WAIT_STATE : ADDR;
                       end else begin
                             devsel <= 0;
                             devsel_cntr <= DevselCount - 1;
                             Pcix_target_state <= DEVSEL_WAIT;
                       end // else: !if(DevselCount < 2)
                    end else begin // Access not for me, wait for end of transaction
                       devsel <= 0;
                       Pcix_target_state <= MABORT;
		    end
              end // case: ADR0
              
              
              DEVSEL_WAIT : begin
                    check_addr_par  <= 0;
                    dual_addr_cycle <= 0;
                    devsel_cntr     <= devsel_cntr - 1;
                    if (devsel_cntr < 2) begin
                          devsel    <= 1;
                          ack64     <= req64 ? ~use_32bit : 0;
			  addr_incr <= req64 & ~use_32bit ? 8 : 4;
                          Pcix_target_state <= NumInitialWait ? WAIT_STATE : ADDR;
                    end else
                       Pcix_target_state <= DEVSEL_WAIT;
              end // case: DEVSEL_WAIT
              
              
              WAIT_STATE :begin
                    initial_wait_cnt <= initial_wait_cnt - 1;
		    Pcix_target_state <= (initial_wait_cnt > 1)  ? WAIT_STATE : ADDR;
	      end


              ADDR : begin
                 check_addr_par  <= 0;
                 dual_addr_cycle <= 0;
                 
                 case (Target_Response)
                   RETRY : begin
                      trdy   <= 0;
                      stop   <= 1;
                      devsel <= 1;
                      Pcix_target_state <= TERMINATE;
                   end // case: RETRY
		 
                   TABRT : begin
                      if (num_data_ph == 0) begin
                            trdy   <= 0;
                            stop   <= 1;
                            devsel <= 0;
                            ack64  <= 0;
                            Pcix_target_state <= TERMINATE;
                      end else begin
                            trdy   <= 1;
                            stop   <= 0;
                            devsel <= 1;
                            Pcix_target_state <= DATA;
                      end // else: !if(num_data_ph == 0)
                   end // case: TABRT
		 
                   DISCNT : begin
                      trdy   <= 1;
                      stop   <= 1;
                      devsel <= 0;
		      wr_cmp_dc <= mem_wr | config_wr;
                      Pcix_target_state <= TERMINATE;
                   end // case: DISCNT
		 
                   ADB : begin
                      if (num_data_ph == 0) begin
                            trdy   <= 1;
                            stop   <= 1;
                            devsel <= 1;
                            Pcix_target_state <= ADB_STATE;
                            disconnect <= 1;
                      end else begin
                            trdy   <= 1;
                            stop   <= 0;
                            devsel <= 1;
                            Pcix_target_state <= DATA;
                      end // else: !if(num_data_ph == 0)
                   end // case: ADB
		 
                   SPLIT : begin
                      trdy   <= 1;
                      stop   <= 0;
                      devsel <= 0;
                      ack64  <= 0;
                      Pcix_target_state <= TERMINATE;
                   end // case: SPLIT
		 
                   NORMAL : begin
                      trdy   <= 1;
                      stop   <= 0;
                      devsel <= 1;
                      Pcix_target_state <= DATA;
                   end // case: NORMAL
		 
                 endcase // case(Target_Response)

              end // case: ADDR

`ifdef NOCODE
              WAIT_STATE :begin
                 initial_wait_cnt <= initial_wait_cnt - 1;
                 if (initial_wait_cnt < 2)
                    begin

                       case (Target_Response)
                         RETRY : begin
                            trdy <= 0;
                            stop <= 1;
                            devsel <= 1;
                            Pcix_target_state <= TERMINATE;
                         end // case: RETRY

                         TABRT : begin
                            if (num_data_ph == 0)
                               begin
                                  trdy <= 0;
                                  stop <= 1;
                                  devsel <= 0;
                                  ack64  <= 0;
                                  Pcix_target_state <= TERMINATE;
                               end // if (num_data_ph == 0)
                            else
                               begin
                                  trdy <= 1;
                                  stop <= 0;
                                  devsel <= 1;
                                  Pcix_target_state <= DATA;
                               end // else: !if(num_data_ph == 0)
                         end // case: TABRT

                         DISCNT : begin
                            trdy <= 1;
                            stop <= 1;
                            devsel <= 0;
                            ack64  <= 0;
			    wr_cmp_dc <= mem_wr | config_wr;
                            Pcix_target_state <= TERMINATE;
                         end // case: DISCNT

                         ADB : begin
                            if (num_data_ph == 0)
                               begin
                                  trdy <= 1;
                                  stop <= 1;
                                  devsel <= 1;
                                  disconnect <= 1;
                                  Pcix_target_state <= ADB_STATE;
                            end else begin
                                  trdy <= 1;
                                  stop <= 0;
                                  devsel <= 1;
                                  Pcix_target_state <= DATA;
                            end // else: !if(num_data_ph == 0)
                         end // case: ADB

                         SPLIT : begin
                            trdy   <= 1;
                            stop   <= 0;
                            devsel <= 0;
                            ack64  <= 0;
                            Pcix_target_state <= TERMINATE;
                         end // case: SPLIT

                         NORMAL : begin
                            trdy   <= 1;
                            stop   <= 0;
                            devsel <= 1;
                            Pcix_target_state <= DATA;
			 end
                       endcase // case(Target_Response)

                    end // if (initial_wait_cnt == 1)
                 else
                    begin
                       Pcix_target_state <= WAIT_STATE;
                    end // else: !if(initial_wait_cnt == 1)
              end // case: WAIT_STATE
`endif

              
              DATA : begin
                       data_phase_count <= data_phase_count + 1;
                 if (frame) begin
/*
 *                      if (req64 && !ack64 && StartAddress[2] && (data_phase_count == 0))
 *                         // Exception case where req64 and !ack64 and addr[2], ignore data phase
 *                         next_access_address = next_access_address + 1;
 *                      else if ((ack64 & access_address[0]) || !ack64)
 *                         next_access_address = next_access_address + 1;
 *                      else
 *                         next_access_address = next_access_address + 2;
 */
                       access_address   <= access_address   + addr_incr;
		       current_address  <= current_address  + addr_incr;

		       // What the heck is this for?
                       if (data_phase_count == TotalNumDataPh-1) begin
                             Pcix_target_state <= WAIT_FRAME;
                             trdy   <= 0;
                             devsel <= 0;
                             ack64  <= 0;
		       	     wr_cmp <= mem_wr | config_wr;
                       end else begin
                             case (Target_Response)
                               TABRT : begin
                                  if (data_phase_count == num_data_ph-1) begin
                                        trdy   <= 0;
                                        stop   <= 1;
                                        devsel <= 0;
                                        ack64  <= 0;
					wr_cmp <= mem_wr | config_wr;
                                        Pcix_target_state <= TERMINATE;
                                  end 
			          else begin
                                        Pcix_target_state <= DATA;
                                  end 
                               end // case: TABRT

                               ADB : begin
                                  if (data_phase_count == num_data_ph-1) begin
                                        trdy   <= 1;
                                        stop   <= 1;
                                        devsel <= 1;
                                        Pcix_target_state <= ADB_STATE;

                                  if (access_address[6:0]+addr_incr-1 == 7'h7f)
                                        disconnect <= 1;
                                  end else begin
                                        Pcix_target_state <= DATA;
                                  end 
                               end // case: ADB

                               NORMAL : begin
                                  Pcix_target_state <= DATA;

                               end // case: NORMAL

                             endcase // case(Target_Response)

                          end // else: !if(data_phase_count == TotalNumDataPh - 1)
                 end else if (!frame & irdy) begin

                       access_address <= access_address + addr_incr;

                       Pcix_target_state <= IDLE;
                       trdy     <= 0;
                       devsel   <= 0;
                       ack64    <= 0;
                       //slave  <= 0;
		       wr_cmp   <= mem_wr | config_wr;
                       mem_wr   <= 0;
                       mem_rd   <= 0;
                       config_rd <= 0;
                       config_wr <= 0;
                       split_complete <= 0;
                 end // if (!frame & irdy)
              end // case: DATA
              
              
              WAIT_FRAME : 
                 begin       
                    slave     <= 0;
		    wr_cmp    <= 0;
                    mem_wr    <= 0;
                    mem_rd    <= 0;
                    config_wr <= 0;
                    config_rd <= 0;
                    split_complete <= 0;
                    if (!frame)
                       Pcix_target_state <= IDLE;
                    else
                       Pcix_target_state <= WAIT_FRAME;
                 end // case: WAIT_FRAME
              
              
              ADB_STATE : begin
                 access_address   <= access_address   + addr_incr;
                 data_phase_count <= data_phase_count + 1;
		 current_address  <= current_address  + addr_incr;

		   // If we are at the end of an ADB, disconnect now
                 if (!frame || (data_phase_count == TotalNumDataPh - 1) ||
//!!!                     (disconnect && (((access_address[4:0] == 5'h1f) & !ack64) ||
//!!!                                     ((access_address[4:1] == 4'hf) & ack64))) ) begin
			  (disconnect & (access_address[6:0]+addr_incr-1 == 7'h7f))) begin
                       if (!frame)
                          Pcix_target_state <= IDLE;
                       else
                          Pcix_target_state <= WAIT_FRAME;
                       trdy     <= 0;
                       devsel   <= 0;
                       stop     <= 0;
                       ack64    <= 0;
		       wr_cmp   <= mem_wr | config_wr;
                       mem_wr   <= 0;
                       mem_rd   <= 0;
                       config_rd <= 0;
                       config_wr <= 0;
                       split_complete <= 0;

			// Within 3 cycles of ADB
                 end else if ((!ack64 & (access_address[6:2] < 5'h1c)) ||  
                               (ack64 & (access_address[6:2] < 5'h18))) begin
                    disconnect <= 1;
                 end else begin
                 end 

              end
              
              
              TERMINATE :
                 begin
                    slave    <= 0;
                    trdy     <= 0;
                    stop     <= 0;
                    devsel   <= 0;
                    ack64    <= 0;
                    mem_wr   <= 0;
		    wr_cmp   <= wr_cmp_dc;  // handles wr_cmp's after disconnects
		    wr_cmp_dc<= 0;
                    mem_rd   <= 0;
                    config_rd <= 0;
                    config_wr <= 0;
                    Pcix_target_state <= WAIT_FRAME;
                 end // case: TERMINATE
              
// Actually, just wait for the current operation to finish before
//  looking for the beginning of another transaction.
              MABORT : begin
                 check_addr_par <= 0;
                 if (!frame) 
                    begin
                       Pcix_target_state <= IDLE;
                       slave    <= 0;
		       wr_cmp   <= 0;
                       mem_wr   <= 0;
                       mem_rd   <= 0;
                       config_rd <= 0;
                       config_wr <= 0;
                       check_addr_par <= 0;
                       dual_addr_cycle <= 0;
                    end
                 else
                    Pcix_target_state <= MABORT;
              end // case: MABORT
              
            endcase // case(Pcix_target_state)
         end // else: !if(rst_l)
   end // always @ (clk)

   
   /**********************************
    * Parity generation and checking *
    **********************************/
   
   always @ (posedge clk) begin
      check_addr_par_1 <= check_addr_par;
   end

   always @ (posedge clk) begin
      exp_parity <= ^{ad_int[31:0], cbe_int[3:0]};
      exp_parity_1 <= ^{ad_int[63:32], cbe_int[7:4], !req64};
      exp_parity_2 <= ^{ad_int[63:32], cbe_int[7:4], !ack64_d};
   end


   //---- Address parity checking ----//
   always @ (posedge clk) begin
      if ((check_addr_par | check_addr_par_1) && addr_in_range)
         begin
            if (dual_addr_cycle & req64)
               serr <= ^{par_int, exp_parity} | ^{par64_int, exp_parity_1};
            else
               serr <= ^{par_int, exp_parity};

            serr_en <= 1;
         end
      else
         begin
            serr_en <= 0;
            serr    <= 0;
         end
   end


   //---- Data parity checking ----//
   always @ (posedge clk) begin
      if (irdy_dd && trdy_dd && mem_wr_dd) 
         begin
            perr <= ^{par_int, exp_parity} | (^{par64_int, exp_parity_2} & ack64_dd) ;
            perr_en <= 1;
         end
      else
         begin
            perr <= 0;
            perr_en <= 0;
         end
   end



   /**********************************
    *****  Combinatorial logic    ****
    **********************************/
   
   // Compute the total number of data phases in a transaction

//!!!   assign TotalNumDataPh = (compute_total_data_ph) ? 
//!!!                           (config_rd | config_wr) ?  8'h01 : 
//!!!                           ComputeTotalDataPhase(StartAddress[2:0], {cbe_int[3:0],ad_int[7:0]}, 
//!!!                                                 (req64 & ack64)) : TotalNumDataPh;
   

   function [10:0] ComputeTotalDataPhase;
      input [2:0]  start_addr;
      input [11:0] byte_count;
      input        access64bit;
      reg [12:0]   actual_byte_cnt;
      reg [11:0]   temp;
      reg [1:0]    temp1;
      reg          correction_64_32;
      
      begin
         if (req64 && use_32bit && StartAddress[2])
            correction_64_32 = 1;
         else
            correction_64_32 = 0;
         
         if (byte_count == 0)
            actual_byte_cnt = 13'h1000;
         else
            actual_byte_cnt = {1'b0, byte_count[11:0]};

         if (!access64bit)
            begin
               temp = actual_byte_cnt + start_addr[1:0];
               temp1 = temp[0] | temp[1];
               ComputeTotalDataPhase = temp[11:2] + temp1 + correction_64_32;
            end
         else
            begin
               temp = actual_byte_cnt + start_addr[2:0];
               temp1 = temp[0] | temp[1] | temp[2];
               ComputeTotalDataPhase = temp[11:3] + temp1;
            end
      end
   endfunction // ComputeTotalDataPhase

   
`endprotect
endmodule // PcixTargetSm
