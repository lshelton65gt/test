/****************************************************************
 PCI command types
 ****************************************************************/
parameter PCI_INTR_ACK           = 4'b0000;
parameter PCI_SPECIAL_CYCLE      = 4'b0001;
parameter PCI_IO_ACC             = 3'b001;
parameter PCI_IO_READ            = 4'b0010;
parameter PCI_IO_WRITE           = 4'b0011;
parameter PCI_RESERVED1          = 4'b0100;
parameter PCI_RESERVED2          = 4'b0101;
parameter PCI_MEM_ACC            = 3'b011;
parameter PCI_MEM_READ           = 4'b0110;
parameter PCI_MEM_WRITE          = 4'b0111;
parameter PCI_RESERVED3          = 4'b1000;
parameter PCIX_MEM_READ_BLK_A    = 4'b1000;
parameter PCI_RESERVED4          = 4'b1001;
parameter PCIX_MEM_WRITE_BLK_A   = 4'b1001;
parameter PCI_CFG_ACC            = 3'b101;
parameter PCI_CFG_READ           = 4'b1010;
parameter PCI_CFG_WRITE          = 4'b1011;
parameter PCI_MEM_READ_MULT      = 4'b1100;
parameter PCIX_SPLIT_COMP        = 4'b1100;
parameter PCI_DAC                = 4'b1101;
parameter PCI_MEM_READ_LINE      = 4'b1110;
parameter PCIX_MEM_READ_BLK      = 4'b1110;
parameter PCI_MEM_WRITE_INV      = 4'b1111;
parameter PCIX_MEM_WRITE_BLK     = 4'b1111;

/****************************************************************
 Internal register addresses
 ****************************************************************/
parameter PCI_CONFIG_REG   = 0;
parameter DEFAULT_CMD     =  4;
parameter PCI_CDP_REG     =  8;

// !!! Replaced these with two regs
// parameter PCI_CS_REG_ZERO   0;
// parameter PCI_CS_REG_ONE    1;
// parameter PCIX_CS_REG_ONE   2;
// parameter PCIX_CS_REG_TWO   3;

// !! Replaced all these with 1 reg
// parameter DEFAULT_64BIT     4;
// parameter DEFAULT_WR_CMD    5;
// parameter DEFAULT_RD_CMD    6;
// parameter DEFAULT_REQ_TAG   7;
// parameter DEFAULT_REQ_ID    8;

/****************************************************************
 Transaction definitions
 ****************************************************************/
// Structure of the opcode word
parameter USER_OP__VALID       = 31;

// define USER_OP__REQ_TAG_SC  26:24
parameter USER_OP__REQ_TAG_SC_H =  26;
parameter USER_OP__REQ_TAG_SC_L =  24;

// define USER_OP__REQ_TAG     28:24
parameter USER_OP__REQ_TAG_H =     28;
parameter USER_OP__REQ_TAG_L =     24;

// define USER_OP__REQ_ID      23:8
parameter USER_OP__REQ_ID_H =      23;
parameter USER_OP__REQ_ID_L =       8;

parameter USER_OP__BCM         = 6;
parameter USER_OP__64BIT       =  4;

// define USER_OP__PCIX_CMD     3:0
parameter USER_OP__PCIX_CMD_H =     3;
parameter USER_OP__PCIX_CMD_L =     0;

//!!! Moved to use bit zero of cmd -- split complete is the exception to this rule
parameter USER_OP__WRITE       = 0;

// range that doesn't include the Wr/Rd bit
// define USER_OP__PCIX_CMDX    3:1  
parameter USER_OP__PCIX_CMDX_H =    3;
parameter USER_OP__PCIX_CMDX_L =    1;

// Config register defines
parameter CFG__RETRY           = 0;
parameter CFG__FORCE_DPAR      = 2;
parameter CFG__FORCE_APAR      = 3;

//------------------------------------------//
//---- Termination types in Pcix_Status ----//
//------------------------------------------//
parameter STATUS__NORMAL                    = 16'h0000;
parameter STATUS__MASTER_ABORT              = 16'h0001;
parameter STATUS__TARGET_RETRY              = 16'h0002;
parameter STATUS__TARGET_ABORT              = 16'h0004;
parameter STATUS__TARGET_SPLIT_RESPONSE     = 16'h0008; // PCIX only;
parameter STATUS__DATA_DISCONNECT_A         = 16'h0008; // PCI only;
parameter STATUS__SINGLE_DATA_DISCONNECT    = 16'h0010; // PCIX only;
parameter STATUS__DATA_DISCONNECT_B         = 16'h0010; // PCI only;
parameter STATUS__TARGET_DISCONNECT_ADB     = 16'h0020; // PCIX only;
parameter STATUS__DATA_DISCONNECT_C         = 16'h0020; // PCI only;
parameter STATUS__ILLEGAL_TARGET_RETRY      = 16'h0040; // PCIX only;
parameter STATUS__ILLEGAL_TARGET_WAIT       = 16'h0080; // PCIX only;
parameter STATUS__ILLEGAL_TARGET_SDD        = 16'h0100; // PCIX only;
parameter STATUS__ILLEGAL_TARGET_SPLIT_RESP = 16'h0200; // PCIX only;
parameter STATUS__ILLEGAL_TARGET_RESPONSE   = 16'h0400; // PCIX only;
parameter STATUS__ILLEGAL_BURST_AFTER_ADB   = 16'h0800; // PCIX only;
parameter STATUS__MASTER_LATENCY_EXP        = 16'h1000; // PCIX only;

//------------------------------------------//
//--- Error bit positions in Pcix_Status ---//
//------------------------------------------//

parameter DATA_PARITY_ERROR         = 16;
parameter ADDR_PARITY_ERROR         = 17;
parameter TARGET_LATENCY_EXP        = 18;  // PCIX only;


//  Slave transactor defines

//------------------------------------------//
//--- V->C defines -------------------------//
//------------------------------------------//
//  Opcode defs
parameter PCI_RANGE_CHECK     = 32'h000000ff;
parameter PCI_GET_RESPONSE    = 32'h000000fe;
parameter PCI_GET_WAIT        = 32'h000000fd;
parameter PCIX_GET_RESPONSE   = 32'h000000fc;
parameter PCI_PARITY_ERR      = 32'h000000fb;
parameter PCIX_DECODE_TIME    = 32'h000000f0;

// range check responses
parameter PCI_RANGE_OK        = 32'h00000001;
parameter PCI_RANGE_OK_32     = 32'h00000003;
parameter PCI_RANGE_OK_BIT    = 0;
parameter PCI_RANGE_32_BIT    = 1;

// devsel decode responses
parameter DECODE_A       = 32'h00000000;
parameter DECODE_B       = 32'h00000001;
parameter DECODE_C       = 32'h00000002;
parameter DECODE_S       = 32'h00000003;


// define STATUS__REQ_ATTR    28:8
parameter STATUS__REQ_ATTR_H = 28;
parameter STATUS__REQ_ATTR_L =  8;

// define STATUS__CBE         7:4
parameter STATUS__CBE_H               = 7;
parameter STATUS__CBE_L               = 4;

// define STATUS__SPLIT_COMP  4
parameter STATUS__SPLIT_COMP          = 4;

// define STATUS__ACCESS_TYPE 3:0
parameter STATUS__ACCESS_TYPE_H       = 3;
parameter STATUS__ACCESS_TYPE_L       = 0;

// define RET_STATUS__NUM_DATA_PH    15:8
parameter RET_STATUS__NUM_DATA_PH_H   =  15;
parameter RET_STATUS__NUM_DATA_PH_L   =  8;

// define RET_STATUS__NUM_INIT_WAIT   7:5
parameter RET_STATUS__NUM_INIT_WAIT_H = 7;
parameter RET_STATUS__NUM_INIT_WAIT_L = 5;

// define RET_STATUS__RESP_TYPE       3:0
parameter RET_STATUS__RESP_TYPE_H     =  3;
parameter RET_STATUS__RESP_TYPE_L     =  0;

// define RET_STATUS__NUM_WAIT        7:0
parameter RET_STATUS__NUM_WAIT_H      =  7;
parameter RET_STATUS__NUM_WAIT_L      =  0;

// define RET_STATUS__DEC_TIME        3:0
parameter RET_STATUS__DEC_TIME_H      =  3;
parameter RET_STATUS__DEC_TIME_L      =  0;


// response type codes
parameter NORMAL  = 0;
parameter RETRY  =  1;
parameter TABRT  =  2;
parameter DISCNT =  3;
parameter ADB    =  4;
parameter SPLIT  =  5;
