// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef APB_MASTER_RTL_CT_ADAPTER_H_
#define APB_MASTER_RTL_CT_ADAPTER_H_

template <typename MODULE, unsigned int BUSWIDTH=32, unsigned int ADDRWIDTH=32>
class APB_Master_RTL_CT_Adapter: public sc_core::sc_module
#ifdef CARBON_DEBUG_ACCESS
, public CarbonDebugAccessIF
#endif
{
public:
	
	/// TLM2 socket
	amba::amba_master_socket<BUSWIDTH> master_sock;

	///RTL signals
	sc_core::sc_in_clk m_clk;
        sc_core::sc_signal<bool> m_otherSideReset;
	sc_core::sc_in<bool> m_Reset;
	sc_core::sc_in<bool > m_psel;
	sc_core::sc_in<bool > m_penable;
	sc_core::sc_in<bool > m_pwrite;
	sc_core::sc_in<sc_dt::sc_uint<ADDRWIDTH> > m_paddr;
	sc_core::sc_in<sc_dt::sc_uint<BUSWIDTH> > m_pwdata;
	sc_core::sc_out<bool > m_pready;
	sc_core::sc_out<sc_dt::sc_uint<BUSWIDTH> > m_prdata;
	sc_core::sc_out<bool > m_pslverr;
	

	tlm::tlm_sync_enum nb_transport_bw(tlm::tlm_generic_payload & trans, tlm::tlm_phase &ph, sc_core::sc_time &delay);
    void slave_timing_listener(gs::socket::timing_info);
	void end_of_elaboration();
	
	///SystemC Methods

	void send_req_method();
	void ReadyMethod();
	void clock_tick();
	void ResetMethod();
	
	//timing info distro functions
	void set_master_timing(gs::socket::timing_info &);
	void set_master_timing(tlm::tlm_phase ph,const sc_core::sc_time &);
	gs::socket::timing_info & get_master_timing();
	sc_core::sc_time& get_master_timing(tlm::tlm_phase);
	inline void register_slave_timing_listener(MODULE* mod, void (MODULE::*fn)(gs::socket::timing_info))
	{
		owner=mod;
	    timing_cb=fn;
	}

#ifdef CARBON_DEBUG_ACCESS
	 //for Carbon debug access
	 CarbonDebugAccessStatus debugMemRead(uint64_t addr, uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0);
	 CarbonDebugAccessStatus debugMemWrite (uint64_t addr, const uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0);
#endif

	SC_HAS_PROCESS(APB_Master_RTL_CT_Adapter);
	APB_Master_RTL_CT_Adapter(sc_core::sc_module_name nm):sc_module(nm),
	master_sock("master_socket",amba::amba_APB, amba::amba_CT,false,gs::socket::GS_TXN_WITH_DATA),
	m_clk("m_clk"),
	m_Reset("APB_RESET"),
	m_psel("APB_PSEL"),
	m_penable("APB_PENABLE"),
	m_pwrite("APB_PWRITE"),
	m_paddr("APB_PADDR"),
	m_pwdata("APB_PWDATA"),
	m_pready("APB_PREADY"),
	m_prdata("APB_PRDATA"),
	m_pslverr("APB_PSLVERR"),
	current_trans(NULL),
    reset_trans(NULL),
	timing_cb(NULL),
	owner(NULL),
	time_unit(sc_core::sc_get_time_resolution()),
	selreq_sample_time(sc_core::sc_get_time_resolution()),
	ready_done(false),
	resp_pending(false),
    m_InReset(false)
	{

		SC_METHOD(ReadyMethod);
		sensitive<<pready_ev;
		dont_initialize();

		SC_METHOD(clock_tick);
		sensitive << m_clk.pos();
		dont_initialize();
		
		SC_METHOD(ResetMethod);
		sensitive<< m_Reset;
		dont_initialize();

		SC_METHOD(send_req_method);
		sensitive <<psel_ev;
		dont_initialize();
	    
		master_sock.template register_nb_transport_bw< APB_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH> >(this, & APB_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::nb_transport_bw);
		master_sock.set_timing_listener_callback(this, & APB_Master_RTL_CT_Adapter::slave_timing_listener);
	}
	~APB_Master_RTL_CT_Adapter();
private:
	tlm::tlm_generic_payload * current_trans;
	tlm::tlm_generic_payload * reset_trans;
	void (MODULE::*timing_cb)(gs::socket::timing_info);
	MODULE * owner;
	gs::socket::timing_info master_timing_info;
	sc_core::sc_time time_unit;
	sc_core::sc_time selreq_sample_time;
	bool ready_done;
	bool resp_pending;
	bool m_InReset;
	sc_core::sc_event psel_ev;
	sc_core::sc_event penable_ev;
	sc_core::sc_event pready_ev;

};

/*
 * @brief
 *
 * @details
 */
template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
APB_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::~APB_Master_RTL_CT_Adapter()
{
	if(reset_trans)
	{
		reset_trans->release();
		reset_trans=NULL;
	}
}

/*
 * @brief end_of_elaboration
 *
 * @details  This is called at the end of elaboration
 */
template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::end_of_elaboration()
{

	master_sock.activate_synchronization_protection();
	sc_core::sc_time delay=  sc_core::sc_get_time_resolution();
	if(master_timing_info.get_start_time(amba::PSELECT)<delay)
		master_timing_info.set_start_time(amba::PSELECT,delay);
	if(master_timing_info.get_start_time(tlm::BEGIN_REQ)< delay)
		master_timing_info.set_start_time(tlm::BEGIN_REQ, delay);
	if(master_timing_info.get_start_time(tlm::BEGIN_RESP)< delay)
		master_timing_info.set_start_time(tlm::BEGIN_RESP,delay);
    master_sock.set_initiator_timing(master_timing_info);
    selreq_sample_time=master_timing_info.get_start_time(amba::PSELECT);
    if(timing_cb!=NULL)
    	 (owner->*timing_cb)(master_timing_info);

}


#ifdef CARBON_DEBUG_ACCESS
template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
CarbonDebugAccessStatus  APB_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::debugMemRead(uint64_t addr, uint8_t* buf, uint32_t numBytes, uint32_t* ctrl)
{
	AMBA_DUMP("Debug Read Access, bytes to read"<< numBytes <<", address="<< addr);
	tlm::tlm_generic_payload * payload = master_sock.get_transaction();
    payload->set_command(tlm::TLM_READ_COMMAND);
    payload->set_address(addr);
    master_sock.reserve_data_size(*payload,numBytes);
    if((ctrl[0]&0x01)==0x1)
    {
    	AMBA_DUMP("Secure Access");
    	master_sock.template validate_extension<amba::amba_secure_access>(*payload);

    }

    unsigned int transfered_bytes=master_sock->transport_dbg(*payload);
    AMBA_DUMP("Actual bytes read "<<transfered_bytes);
    unsigned char * data_ptr = payload->get_data_ptr();
    memcpy(buf,data_ptr,transfered_bytes);
    if(transfered_bytes < numBytes)
    	return eCarbonDebugAccessStatusPartial;
    else
    {
    	tlm::tlm_response_status resp= payload->get_response_status();
    	switch(resp)
    	{
			case tlm::TLM_ADDRESS_ERROR_RESPONSE:
			{
				return eCarbonDebugAccessStatusOutRange;
				break;
			}
			case tlm::TLM_OK_RESPONSE:{
				return eCarbonDebugAccessStatusOk;
				break;
			}
			default:
			{
				return eCarbonDebugAccessStatusError ;
				break;
			}
    	}
    }
}


template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
CarbonDebugAccessStatus   APB_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH>::debugMemWrite(uint64_t addr, const uint8_t* buf, uint32_t numBytes, uint32_t* ctrl)
{

	AMBA_DUMP("Debug Write Access, bytes to write"<< numBytes<<", address="<< addr);
	tlm::tlm_generic_payload * payload = master_sock.get_transaction();
	payload->set_command(tlm::TLM_WRITE_COMMAND);
	payload->set_address(addr);
	master_sock.reserve_data_size(*payload,numBytes);
	if((ctrl[0]&0x01)==0x1)
	{
		AMBA_DUMP("Secure Access");
		master_sock.template validate_extension<amba::amba_secure_access>(*payload);
	}
	unsigned char * data_ptr = payload->get_data_ptr();
    memcpy(data_ptr,buf,numBytes);
	unsigned int transfered_bytes=master_sock->transport_dbg(*payload);
	AMBA_DUMP("Actual bytes written "<<transfered_bytes);

	if(transfered_bytes < numBytes)
		return eCarbonDebugAccessStatusPartial;
	else
	{
		tlm::tlm_response_status resp= payload->get_response_status();
		switch(resp)
		{
			case tlm::TLM_ADDRESS_ERROR_RESPONSE:
			{
				return eCarbonDebugAccessStatusOutRange;
				break;
			}
			case tlm::TLM_OK_RESPONSE:{
				return eCarbonDebugAccessStatusOk;
				break;
			}
			default:
			{
				return eCarbonDebugAccessStatusError ;
				break;
			}
		}
	}
}
#endif

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::set_master_timing(gs::socket::timing_info & info)
{
	bool changed=false;
	if((info.get_start_time(tlm::BEGIN_REQ)+time_unit) > master_timing_info.get_start_time(tlm::BEGIN_REQ))
	{
		master_timing_info.set_start_time(tlm::BEGIN_REQ, (info.get_start_time(tlm::BEGIN_REQ)+time_unit));
		changed=true;
	}
	if((info.get_start_time(amba::PSELECT)+time_unit) > master_timing_info.get_start_time(amba::PSELECT))
	{
			master_timing_info.set_start_time(amba::PSELECT, (info.get_start_time(amba::PSELECT)+time_unit));
			changed=true;
	}
	if(changed==true)
	{
		  master_sock.set_initiator_timing(master_timing_info);
		  selreq_sample_time=master_timing_info.get_start_time(amba::PSELECT);
	}

#define MY_MAX(a,b) ((a>b)?a:b)
	changed=false;
	sc_core::sc_time new_breq_time=MY_MAX(master_timing_info.get_start_time(tlm::BEGIN_REQ), master_timing_info.get_start_time(amba::PSELECT));
	if((new_breq_time+time_unit)> master_timing_info.get_start_time(tlm::BEGIN_REQ))
	{
		master_timing_info.set_start_time(tlm::BEGIN_REQ,(new_breq_time+time_unit));
		changed=true;
	}
	if((new_breq_time+time_unit)> master_timing_info.get_start_time(amba::PSELECT))
	{
		master_timing_info.set_start_time(amba::PSELECT,(new_breq_time+time_unit));
		changed=true;
	}
	if(changed)
	{
		master_sock.set_initiator_timing(master_timing_info);
		selreq_sample_time=master_timing_info.get_start_time(amba::PSELECT);
	}
	if((new_breq_time+time_unit)>master_timing_info.get_start_time(tlm::BEGIN_RESP))
	{
		master_timing_info.set_start_time(tlm::BEGIN_RESP,(new_breq_time+time_unit));
        if(timing_cb!=NULL)
        	 (owner->*timing_cb)(master_timing_info);
	}
#undef MY_MAX
}

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::set_master_timing(tlm::tlm_phase ph ,const sc_core::sc_time & delay)
{
	if(ph!=tlm::BEGIN_REQ && ph!= amba::PSELECT)
		return;
	bool changed=false;
	if((delay+time_unit)>master_timing_info.get_start_time(ph))
	{
		changed=true;
		master_timing_info.set_start_time(ph,delay);
	}
	if(changed==true)
	{
		master_sock.set_initiator_timing(master_timing_info);
		selreq_sample_time=master_timing_info.get_start_time(amba::PSELECT);
	}
#define MY_MAX(a,b) ((a>b)?a:b)
	changed=false;
	sc_core::sc_time new_breq_time=MY_MAX(master_timing_info.get_start_time(tlm::BEGIN_REQ), master_timing_info.get_start_time(amba::PSELECT));
	if((new_breq_time+time_unit)> master_timing_info.get_start_time(tlm::BEGIN_REQ))
	{
		master_timing_info.set_start_time(tlm::BEGIN_REQ,(new_breq_time+time_unit));
		changed=true;
	}
	if((new_breq_time+time_unit)> master_timing_info.get_start_time(amba::PSELECT))
	{
		master_timing_info.set_start_time(amba::PSELECT,(new_breq_time+time_unit));
		changed=true;
	}
	if(changed)
	{
		master_sock.set_initiator_timing(master_timing_info);
		selreq_sample_time=master_timing_info.get_start_time(amba::PSELECT);
	}
	if((new_breq_time+time_unit)>master_timing_info.get_start_time(tlm::BEGIN_RESP))
	{
		master_timing_info.set_start_time(tlm::BEGIN_RESP,(new_breq_time+time_unit));
        if(timing_cb!=NULL)
        	 (owner->*timing_cb)(master_timing_info);
	}
#undef MY_MAX
}

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
gs::socket::timing_info& APB_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::get_master_timing()
{
	return master_timing_info;
}

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
sc_core::sc_time& APB_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::get_master_timing(tlm::tlm_phase ph)
{
	return (master_timing_info.get_start_time(ph));
}

/*
 * @brief  slave_timing_listener
 *
 * @details This function is triggered when the CT slave sets the non-zero timing information
 */
template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::slave_timing_listener(gs::socket::timing_info info)
{
	if((info.get_start_time(tlm::BEGIN_RESP)+time_unit) > master_timing_info.get_start_time(tlm::BEGIN_RESP))
	{
		master_timing_info.set_start_time(tlm::BEGIN_RESP,(info.get_start_time(tlm::BEGIN_RESP)+time_unit));
		if(timing_cb!=NULL)
				 (owner->*timing_cb)(master_timing_info);
	}
}

/*
 * @brief This function sets the ready state and write on prdata port for read reqs
 *
 */


template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::ReadyMethod()
{
	 if(current_trans==NULL)
	    	return;
	if(ready_done)
	{//ok response recieved, read it and send to the RTL world
    	///Is it a read-transaction
    	AMBA_DUMP("Ready signal triggered high");
		if(current_trans->get_command()== tlm::TLM_READ_COMMAND)
		{	// we need to read the data
			unsigned char * data_ptr= current_trans->get_data_ptr();
			//unsigned int len = current_trans->get_data_length();
			sc_dt::sc_uint<BUSWIDTH> data=0;
			unsigned int i=0;
			for(i=0; i<((BUSWIDTH+7)/8);i++ )
			{
				data.range((i+1)*8-1, i*8)=data_ptr[i];
				AMBA_DUMP("data_ptr value"<< data_ptr[i]);
			}
			m_prdata.write(data);
			AMBA_DUMP("Reading the data recived ="<<data<<endl);
		}
		//read the response
		if(current_trans->get_response_status()== tlm::TLM_OK_RESPONSE)
		{
			m_pslverr.write(false);
			AMBA_DUMP("pslverr=false");
		}
		else
		{
			m_pslverr.write(true);
			AMBA_DUMP("pslverr=true");
		}

	}
    m_pready.write(ready_done);
}

/*
 * @brief ResetMethod
 * 
 * @detail This function is triggered when the Reset port is asserted or deasserted
 */

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::ResetMethod()
{
	if(reset_trans==NULL)
		reset_trans= master_sock.get_transaction();
	tlm::tlm_phase ph;
	if(m_Reset.read()==false)
	{
	  m_InReset = true;
	  //re-init these variables when entering RESET state
	  ready_done=false;
	  resp_pending=false;
          if (current_trans) {
            master_sock.release_transaction(current_trans);
            current_trans=NULL;
          }
		ph=amba::RESET_ASSERT;
	}
	else
	{	
	        m_InReset = false;
		ph= amba::RESET_DEASSERT;
	}
	sc_core::sc_time delay= sc_core::SC_ZERO_TIME;
	tlm::tlm_sync_enum retval = master_sock->nb_transport_fw(*reset_trans,ph,delay);
	assert(retval==tlm::TLM_ACCEPTED);
	if(ph==amba::RESET_DEASSERT)
	{
		//release the txn
		master_sock.release_transaction(reset_trans);
		reset_trans=NULL;
	}
}



/*
 * @brief This is a function which triggers on clock tick
 *
 *
 */

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::clock_tick()
{
	if(ready_done)
	{
		AMBA_DUMP("Clearing the pready");
        resp_pending=false;
		ready_done=false;
		pready_ev.notify();
	}
	psel_ev.notify(selreq_sample_time);
}

/*
 * @brief send_req_method
 * 
 * @details This function is called when the PSEL/PENABLE is set
 */

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::send_req_method()
{
        if (m_InReset) return;
	tlm::tlm_phase ph;
	sc_core::sc_time delay = sc_core::SC_ZERO_TIME;
	if(m_psel.read()==true  && m_penable.read()==false)
	{
			//OK we are into the SETUP state
			if(current_trans== NULL)
				current_trans= master_sock.get_transaction();

			AMBA_DUMP("pselect signal received");
			current_trans->set_address(m_paddr.read());
			master_sock.reserve_data_size(*current_trans, (BUSWIDTH/8));
            current_trans->set_streaming_width(current_trans->get_data_length());
			unsigned char * data_ptr = current_trans->get_data_ptr();
			if(m_pwrite.read()==true)
			{	
				current_trans->set_command(tlm::TLM_WRITE_COMMAND);
				sc_dt::sc_uint<BUSWIDTH> data = m_pwdata.read();
				unsigned int len = current_trans->get_data_length();
				unsigned int j=0;
				for(j=0;j<(BUSWIDTH+7)/8;j++)
				{
					data_ptr[j]=data.range((j+1)*8-1,j*8).to_uint();
				}
				AMBA_DUMP("Sending Write request data="<<data<<endl
				<<"of len="<<len<<endl);
			}
			else
				current_trans->set_command(tlm::TLM_READ_COMMAND);

			ph= amba::PSELECT;
			tlm::tlm_sync_enum retval= master_sock->nb_transport_fw(*current_trans,ph, delay);
			assert(retval== tlm::TLM_ACCEPTED && "Unexpected tlm_sync_enum returned");
			AMBA_DUMP("SETUP state");
	}
	else if(m_psel.read()==true && m_penable.read()==true && resp_pending==false)
	{
		AMBA_DUMP("penable set high, ACCESS State");

		//ok this is ACCESS state
		tlm::tlm_phase ph= tlm::BEGIN_REQ;
		
		//read RTL signals
		sc_core::sc_time delay= sc_core::SC_ZERO_TIME;
		tlm::tlm_sync_enum retval= master_sock->nb_transport_fw(*current_trans, ph, delay);
		switch(retval)
		{
			case tlm::TLM_ACCEPTED:
			{//Ok that means that slave will take some more cycles to respond
				assert(ph== tlm::BEGIN_REQ && "Unexpected phase returned");
				AMBA_DUMP("Received TLM_ACCEPTED");
				ready_done=false;
				resp_pending=true;
				break;
			}
			case tlm::TLM_UPDATED:{
				assert(ph==tlm::BEGIN_RESP && "Unexpected phase returned");
				AMBA_DUMP("Received TLM_UPDATED");
				ready_done=true;
				resp_pending=false;
				break;
			}
			default:
				assert("Wrong tlm_sync_enum returned");

		}
		pready_ev.notify();
	}
	else if(m_psel.read()==false)
	{
		//else it is an IDLE state
		if(current_trans)
		{
			AMBA_DUMP("Entering into IDLE state");
		  master_sock.release_transaction(current_trans);
		  current_trans=NULL;
		}
	}

}



/*
 * @brief nb_transport_bw
 * 
 * @details This function is the backward transport API
 * 
 */

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
tlm::tlm_sync_enum APB_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::nb_transport_bw(tlm::tlm_generic_payload & trans, tlm::tlm_phase & ph, sc_core::sc_time & delay)
{
  AMBA_DUMP("Got phase bw:"<<ph);
      if (ph==tlm::BEGIN_RESP)
      {
        ready_done=true;
        pready_ev.notify();
      }
      else  if (ph==amba::RESET_ASSERT) { //ACTIVE LOW
	  m_InReset = true;
          m_otherSideReset.write(false);
	  //re-init these variables when entering RESET state
	  ready_done=false;
	  resp_pending=false;
          if (current_trans) {
            master_sock.release_transaction(current_trans);
            current_trans=NULL;
          }
	}
        else if (ph==amba::RESET_DEASSERT) {
	  m_InReset = false;
          m_otherSideReset.write(true);
	}
        else if (!m_InReset) {
	  assert(current_trans==&trans && "Invalid transaction sent" );
	  assert(ph==tlm::BEGIN_RESP && "Invalid phase returned");
	  ready_done=true;
	  resp_pending=false;
	  pready_ev.notify();
        }
	return tlm::TLM_ACCEPTED;	
}



#endif /*APB_MASTER_RTL_CT_ADAPTER_H_*/
