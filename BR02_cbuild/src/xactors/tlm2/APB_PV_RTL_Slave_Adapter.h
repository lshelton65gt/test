// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _APB_PV_RTL_SLAVE_ADAPTER_H_
#define _APB_PV_RTL_SLAVE_ADAPTER_H_

#include "xactors/tlm2/APB_LT_RTL_Slave_Adapter.h"
#include "adapters/AMBA_PV_Master_LT_AT_Adapter.h"
#include "greensocket/monitor/green_socket_monitor.h"

template< typename MODULE, unsigned int BUSWIDTH=64,unsigned int ADDRWIDTH=32>
class APB_PV_RTL_Slave_Adapter: public sc_core::sc_module
{
public:
  //TLM port through which we will accept the TLM packet
  tlm::tlm_target_socket<BUSWIDTH, amba_pv::amba_pv_protocol_types> slave_sock;
	
  // RTL ports
  sc_core::sc_in_clk m_clk;
  sc_core::sc_in<bool> m_Reset;
  sc_core::sc_out<bool > m_psel;
  sc_core::sc_out<bool > m_penable;
  sc_core::sc_out<bool > m_pwrite;
  sc_core::sc_out<sc_dt::sc_uint<ADDRWIDTH> > m_paddr;
  sc_core::sc_out<sc_dt::sc_uint<BUSWIDTH> > m_pwdata;
  sc_core::sc_in<bool > m_pready;
  sc_core::sc_in<sc_dt::sc_uint<BUSWIDTH> > m_prdata;
  sc_core::sc_in<bool > m_pslverr;

  // Timing related APIs. Pass on to the LT RTL adapter
  void set_slave_timing(gs::socket::timing_info &timing)
  {
    apb_lt_rtl_adapter.set_slave_timing(timing);
  }
  void set_slave_timing(tlm::tlm_phase phase, const sc_core::sc_time& tm)
  {
    apb_lt_rtl_adapter.set_slave_timing(phase, tm);
  }
  void register_master_timing_listener(MODULE* mod,void (MODULE::*fn)(gs::socket::timing_info))
  {
    apb_lt_rtl_adapter.register_master_timing_listener(mod, fn);
  }

  // APIs for Carbon Debug. Pass on to the LT RTL adapter
  void registerDebugAccessCB(CarbonDebugAccessIF* dbgIf)
  {
    apb_lt_rtl_adapter.registerDebugAccessCB(dbgIf);
  }

  //! API to get and activate the monitor on the LT socket
  gs::socket::monitor<BUSWIDTH>* getMonitor(void)
  {
    lt_monitor->activated = true;
    return lt_monitor;
  }

  //! Constructor
  SC_HAS_PROCESS(APB_PV_RTL_Slave_Adapter);
  APB_PV_RTL_Slave_Adapter(sc_core::sc_module_name nm) :
    sc_core::sc_module(nm),
    slave_sock("apb_slave_socket"),
    apb_lt_rtl_adapter("apb_lt_rtl_adapter"),
    pv_lt_adapter("apb_pv_lt_adapter")
  {
    // Connect the RTL pins to the LT_RTL adapter
    apb_lt_rtl_adapter.m_clk(m_clk);
    apb_lt_rtl_adapter.m_Reset(m_Reset);
    apb_lt_rtl_adapter.m_psel(m_psel);
    apb_lt_rtl_adapter.m_penable(m_penable);
    apb_lt_rtl_adapter.m_pwrite(m_pwrite);
    apb_lt_rtl_adapter.m_paddr(m_paddr);
    apb_lt_rtl_adapter.m_pwdata(m_pwdata);
    apb_lt_rtl_adapter.m_pready(m_pready);
    apb_lt_rtl_adapter.m_prdata(m_prdata);
    apb_lt_rtl_adapter.m_pslverr(m_pslverr);

    // Connect the apb CTL RTL socket to the pv_lt_adapter socket. We
    // create a monitor but de-activate it by default
    lt_monitor = gs::socket::connect_with_monitor<BUSWIDTH, tlm::tlm_base_protocol_types>(pv_lt_adapter.master_socket, apb_lt_rtl_adapter.slave_sock);
    lt_monitor->activated = false;

    // Bind our socket to the PV LT Adapter
    slave_sock.bind(pv_lt_adapter.slave_socket);
  }

private:
  //! Function to send the debug transaction through the CarbonDebugAccessIF
  /*! This function just passed the work on to the LT RTL adapter
   */
  unsigned int transport_debug(tlm::tlm_generic_payload & trans)
  {
    apb_lt_rtl_adapter.transport_debug(trans);
  }

  //! The LT_RTL adapter
  APB_LT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH> apb_lt_rtl_adapter;

  //! The PV LT adapter
  amba::AMBA_PV_Master_LT_AT_Adapter<BUSWIDTH> pv_lt_adapter;

  //! The monitor for activity detection
  gs::socket::monitor<BUSWIDTH>* lt_monitor;
}; // class APB_PV_RTL_Slave_Adapter: public sc_core::sc_module

#endif // _APB_PV_RTL_SLAVE_ADAPTER_H_
