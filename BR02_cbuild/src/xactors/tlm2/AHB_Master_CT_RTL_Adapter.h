// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef AHB_MASTER_CT_RTL_ADAPTER_H_
#define AHB_MASTER_CT_RTL_ADAPTER_H_

#define OK_RESP 0
#define ERROR_RESP 1
#define RETRY_RESP 2
#define SPLIT_RESP 3

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT=false, bool IS_LITE=false>
class AHB_Master_CT_RTL_Adapter: public sc_core::sc_module
{
public:
	
	//socket through which we will receive the CT transaction
	amba::amba_slave_socket<BUSWIDTH> slave_sock;
	sc_core::sc_in_clk m_clk;
        sc_core::sc_signal<bool> m_otherSideReset;
	sc_core::sc_in<bool > m_Reset;
	typename amba::port_enabler<!IS_LITE, bool>::output_port_type m_HBUSREQ;
	sc_core::sc_out<bool> m_HLOCK;
	sc_core::sc_out<sc_dt::sc_uint<2> > m_HTRANS;
	sc_core::sc_out<sc_dt::sc_uint<32> >m_HADDR;
	sc_core::sc_out<sc_dt::sc_uint<3> > m_HSIZE;
	sc_core::sc_out<sc_dt::sc_uint<3> > m_HBURST;
	sc_core::sc_out<sc_dt::sc_uint<4> > m_HPROT;
	sc_core::sc_out<bool> m_HWRITE;
	sc_core::sc_out<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type> m_HWDATA;
	
	//incoming signals
	sc_core::sc_in<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type > m_HRDATA;
	sc_core::sc_in<typename amba::port_enabler<!IS_LITE>::resp_type > m_HRESP;
	typename amba::port_enabler<!IS_LITE, bool>::input_port_type m_HGRANT;
	sc_core::sc_in<bool> m_HREADYIN;
	
	///APIs for setting/getting the non-default timings
	 void set_slave_timing(gs::socket::timing_info &);
     void set_slave_timing(tlm::tlm_phase, const sc_core::sc_time&);
	 gs::socket::timing_info & get_slave_timing();
	 sc_core::sc_time & get_slave_timing(tlm::tlm_phase);
    inline void register_master_timing_listener(MODULE* mod, void (MODULE::*fn)(gs::socket::timing_info))
    {
    	owner=mod;
    	timing_cb=fn;
    }


	tlm::tlm_sync_enum nb_transport_fw(tlm::tlm_generic_payload &, tlm::tlm_phase & , sc_core::sc_time &);
	void master_timing_listener(gs::socket::timing_info);
	void end_of_elaboration();
    void start_of_simulation();
	
	
	//bus grant method
	void bus_req_method();
    void ready_method();
	
	//clock tick method
	void clock_tick();
	void write_trans_type();
    void addr_method();

	
	 ///APIs for Carbon Debug
#ifdef CARBON_DEBUG_ACCESS
	 void registerDebugAccessCB(CarbonDebugAccessIF*);
#endif
	 unsigned int transport_debug(tlm::tlm_generic_payload & trans);

	SC_HAS_PROCESS(AHB_Master_CT_RTL_Adapter);
	AHB_Master_CT_RTL_Adapter(sc_core::sc_module_name nm):sc_module(nm),
	slave_sock("AHB_Slave_socket",IS_LITE? amba::amba_AHBLite : amba::amba_AHB,amba::amba_CT, true),
	m_clk("m_clk"),
	m_Reset("AHB_RESET"),
	m_HBUSREQ("AHB_BUSREQ"),
	m_HLOCK("AHB_HLOCK"),
	m_HTRANS("AHB_HTRANS"),
	m_HADDR("AHB_HADDR"),
	m_HSIZE("AHB_HSIZE"),
	m_HBURST("AHB_HBURST"),
	m_HPROT("AHB_HPROT"),
	m_HWRITE("AHB_HWRITE"),
	m_HWDATA("AHB_HWDATA"),
	m_HRDATA("AHB_HRDATA"),
	m_HRESP("AHB_HRESP"),
	m_HGRANT("AHB_HGRANT"),
	m_HREADYIN("AHB_HREADYIN"),
        m_reset_trans(NULL),
	current_req(NULL),
	current_data(NULL),
    bus_req_txn(NULL),
    trans_type(0),
	current_lock_counter(0),
    current_data_count(0),
    req_start_index(0),
    data_start_index(0),
    m_req_size(0),
    m_data_size(0),
	txn_counter(0),
    m_addr(0),
	timing_cb(NULL),
    owner(NULL),
#ifdef CARBON_DEBUG_ACCESS
    debugIf(NULL),
#endif
	pending_end_req(false),
    pending_end_data(false),
	bus_req(0),
	ready_sample_time(sc_core::sc_get_time_resolution()),
	bus_req_start_time(sc_core::sc_get_time_resolution()),
	ready_stable_time(sc_core::sc_get_time_resolution()),
	lock_value(false),
    grant_state_at_other_side(false),
    m_InReset(false),
    m_data_addr(0),
    m_ungrant_txn(slave_sock.get_reset_txn())
	{
        if (IS_LITE){ //in case of AHBLite Grant and Busreq are signals, which we tie to values such that our adapter will work.
          amba::port_enabler<!IS_LITE, bool>::initialize(m_HGRANT, true);
          amba::port_enabler<!IS_LITE, bool>::initialize(m_HBUSREQ, false);
        }
                SC_METHOD(ResetMethod);
                sensitive << m_Reset;
                dont_initialize();

		SC_METHOD(clock_tick);
		sensitive<< m_clk.pos();
		dont_initialize();

		SC_METHOD(bus_req_method);
		sensitive<<bus_req_ev;
		dont_initialize();

		SC_METHOD(write_trans_type);
		sensitive<< trans_type_ev;
		dont_initialize();

		SC_METHOD(set_lock_method);
		sensitive<< set_lock_ev;
		dont_initialize();

		SC_METHOD(addr_method);
		sensitive<< m_addr_ev;
		dont_initialize();

        SC_METHOD(ready_method);
        sensitive<<ready_ev;
        dont_initialize();

		slave_sock.template register_nb_transport_fw< AHB_Master_CT_RTL_Adapter<BUSWIDTH,  MODULE, FORCE_BIGUINT, IS_LITE> >(this, & AHB_Master_CT_RTL_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::nb_transport_fw);
                  slave_sock.template register_transport_dbg(this, &AHB_Master_CT_RTL_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::transport_debug);
        slave_sock.set_timing_listener_callback(this, & AHB_Master_CT_RTL_Adapter::master_timing_listener);
        
        slave_timing_info.set_start_time(tlm::BEGIN_REQ, sc_core::sc_get_time_resolution());
        slave_timing_info.set_start_time(amba::BUS_REQ, sc_core::sc_get_time_resolution());
	}
	~AHB_Master_CT_RTL_Adapter();
    void set_lock_method();
private:
        void ResetMethod ();
	/*We are keeping separate buffers for the data and the address-control transactions
	 * as during pipeling, the initiator may send separate transactions in parallel
	 * in that case both the current_req and current_data will point to separate txn
	 */
	tlm::tlm_generic_payload * m_reset_trans;
	tlm::tlm_generic_payload * current_req;
	tlm::tlm_generic_payload * current_data;
    tlm::tlm_generic_payload * bus_req_txn;
	unsigned int trans_type, current_lock_counter, current_data_count, req_start_index, data_start_index, m_req_size, m_data_size, txn_counter, m_addr;
	gs::socket::timing_info slave_timing_info;
        //pointer for the debug access
#ifdef CARBON_DEBUG_ACCESS
	CarbonDebugAccessIF * debugIf;
#endif
	void(MODULE::*timing_cb)(gs::socket::timing_info);
	sc_core::sc_event bus_grant_ev, set_lock_ev, ready_ev;
	MODULE* owner;
	bool pending_end_req, pending_end_data;
	unsigned int bus_req;
	sc_core::sc_event bus_req_ev;
	sc_core::sc_event trans_type_ev, m_addr_ev;
	sc_core::sc_time ready_sample_time, bus_req_start_time,ready_stable_time;
	bool lock_value;
    std::queue<amba::lock_object_base*> m_curr_locks;
    typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type m_tmp_dt;
    bool grant_state_at_other_side;
    bool m_InReset;
    sc_dt::uint64 m_data_addr;
    tlm::tlm_generic_payload* m_ungrant_txn;
};


template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
AHB_Master_CT_RTL_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::~AHB_Master_CT_RTL_Adapter()
{
}

/*
 * @brief this fuction is triggered when the slave writes to its
 * Reset output
 * 
 */

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_CT_RTL_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::ResetMethod()
{
  tlm::tlm_phase ph;
  tlm::tlm_sync_enum retval;
  sc_core::sc_time delay = sc_core::SC_ZERO_TIME;
  if (m_reset_trans == NULL)
    m_reset_trans = slave_sock.get_reset_txn();

  if (m_Reset.read()) {
    m_InReset = false;
    ph = amba::RESET_DEASSERT;
  }
  else { //ACTIVE LOW
    m_InReset = true;
    while (!m_curr_locks.empty()) m_curr_locks.pop();
    ready_ev.cancel();
    current_data = current_req = bus_req_txn = NULL;
    m_data_size = m_req_size = data_start_index = req_start_index = 0;
    trans_type = m_data_addr = current_data_count = txn_counter = 0;
    ph = amba::RESET_ASSERT;
  }
  retval = slave_sock->nb_transport_bw(*m_reset_trans, ph, delay);
  assert(retval== tlm::TLM_ACCEPTED && "Wrong tlm_sync_enum returned");
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_CT_RTL_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::end_of_elaboration()
{
	slave_sock.activate_synchronization_protection();
	sc_core::sc_time delay= sc_core::sc_get_time_resolution();
    //guard against overriding already calculated stuff.
     if (slave_timing_info.get_start_time(tlm::BEGIN_REQ)<delay) slave_timing_info.set_start_time(tlm::BEGIN_REQ,delay);
     if (slave_timing_info.get_start_time(tlm::END_REQ)<delay) slave_timing_info.set_start_time(tlm::END_REQ,delay);
     if (slave_timing_info.get_start_time(amba::BEGIN_DATA)<delay) slave_timing_info.set_start_time(amba::BEGIN_DATA,delay);
     if (slave_timing_info.get_start_time(amba::END_DATA)<delay) slave_timing_info.set_start_time(amba::END_DATA,delay);
     if (slave_timing_info.get_start_time(tlm::BEGIN_RESP)<delay) slave_timing_info.set_start_time(tlm::BEGIN_RESP,delay);
     if (slave_timing_info.get_start_time(amba::BUS_GRANT)<delay) slave_timing_info.set_start_time(amba::BUS_GRANT,delay);
     if (slave_timing_info.get_start_time(amba::BUS_UNGRANT)<delay) slave_timing_info.set_start_time(amba::BUS_UNGRANT,delay);
     if (slave_timing_info.get_start_time(amba::BUS_REQ)<2*delay) slave_timing_info.set_start_time(amba::BUS_REQ,2*delay);
     if (slave_timing_info.get_start_time(amba::BUS_REQ_STOP)<2*delay) slave_timing_info.set_start_time(amba::BUS_REQ_STOP,2*delay);

    ready_sample_time=slave_timing_info.get_start_time(amba::END_DATA);
    AMBA_DUMP("JUST Changed my sample time! to "<<ready_sample_time);
    /*
    bus_req_start_time=(slave_timing_info.get_start_time(amba::BUS_REQ)>slave_timing_info.get_start_time(amba::BUS_REQ_STOP))?
                        slave_timing_info.get_start_time(amba::BUS_REQ): slave_timing_info.get_start_time(amba::BUS_REQ_STOP);
    */
    slave_sock.set_target_timing(slave_timing_info);
	if(timing_cb!=NULL) (owner->*timing_cb)(slave_timing_info);
    
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_CT_RTL_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::start_of_simulation()
{
    AMBA_DUMP("SoSim: ready sample time: "<<ready_sample_time);
	ready_stable_time=ready_sample_time;
    //bus_req_start_time=slave_timing_info.get_start_time(amba::BUS_REQ);
}


//The set_lock_ev is only notified in nb_fw, which wont be entered when in RESET state
template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_CT_RTL_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::set_lock_method()
{
  m_HLOCK.write(lock_value);
}


/*!
 * @breif nb_transport_fw 
 * 
 * @details This is the forward transport call
 * 
 */

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
tlm::tlm_sync_enum AHB_Master_CT_RTL_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::nb_transport_fw(tlm::tlm_generic_payload& trans, tlm::tlm_phase &ph, sc_core::sc_time & delay)
{
  AMBA_DUMP("nb_trans_fw "<<ph);
  //When in RESET state, process calls on fw interface only when the Phase is RESET_DEASSERT
  if (m_InReset && (ph != amba::RESET_DEASSERT)) return tlm::TLM_ACCEPTED;

  switch(ph){
    case tlm::BEGIN_REQ:
    {
      current_req=&trans;
      amba::amba_trans_type* trans_type_ext;
      if (slave_sock.get_extension(trans_type_ext,trans)) trans_type=(unsigned int)trans_type_ext->value;
      else trans_type=2; //NONSEQ
      
      trans_type_ev.notify();
      
      if (trans_type==2){ //NONSEQ
        amba::amba_wrap_offset* wrap_ext;
        req_start_index=0;
        if (slave_sock.get_extension(wrap_ext,trans)) req_start_index=wrap_ext->value;
        m_addr=trans.get_address()+req_start_index;
        m_addr_ev.notify();

        m_req_size=(BUSWIDTH+7)/8;
        amba::amba_burst_size* m_burst_size;
        if (slave_sock.get_extension(m_burst_size, trans)) m_req_size=m_burst_size->value;
        switch(m_req_size)
        {
            case 1:{m_HSIZE.write(0);break;}
            case 2:{m_HSIZE.write(1);break;}
            case 4: {m_HSIZE.write(2);break;}
            case 8: {m_HSIZE.write(3);break;}
            case 16:{m_HSIZE.write(4);break;}
            case 32:{m_HSIZE.write(5);break;}
            case 64:{m_HSIZE.write(6);break;}
            case 128:{m_HSIZE.write(7);break;}
            default:
                assert("Unsupported burstsize");
        }
        amba::amba_lock* lock_ext;
        if (slave_sock.get_extension(lock_ext, trans)){
          assert(m_curr_locks.size() && lock_ext->value==m_curr_locks.front());
          current_lock_counter++;
          lock_value=!(m_curr_locks.front()->number_of_txns==current_lock_counter && m_req_size==trans.get_data_length()); //unlock if single beat NONSEQ. otherwise unlock at SEQ or IDLE
        }
        else
          lock_value=false;
        set_lock_ev.notify();
        
        if(slave_sock.template get_extension<amba::amba_imprecise_burst>(trans))
        {//ok this is incrementing burst with undefined length
            m_HBURST.write(1);
            txn_counter=~0;
        }
        else
        {
            txn_counter=trans.get_data_length()/m_req_size;
            switch(txn_counter)
            {
                case 1:m_HBURST.write(0); break;//this is SINGLE Transfer
                case 4:
                    if(req_start_index)
                        m_HBURST.write(2); //WRAPPING 4 beat
                    else
                        m_HBURST.write(3); //4beat INCREMENTING
                    break;
                case 8:
                    if(req_start_index)
                        m_HBURST.write(4); //WRAPPING 8 beat
                    else
                        m_HBURST.write(5); //8 beat INCREMENTING
                    break;
                case 16:
                    if(req_start_index)
                        m_HBURST.write(6); //WRAPPING 16 beat
                    else
                        m_HBURST.write(7); //16 beat INCREMENTING
                    break;
                default:
                    assert("Invalid Burst type");

            }
            txn_counter--;
        }
        
        unsigned char protection=0;
        if(slave_sock.template get_extension<amba::amba_privileged>(trans) )
        	protection |= 0x02;
        if(!slave_sock.template get_extension<amba::amba_instruction>(trans))
        	  protection |= 0x01;
        if(slave_sock.template get_extension<amba::amba_bufferable>(trans))
            protection |= 0x04;
        if(slave_sock.template get_extension<amba::amba_cacheable>(trans))
            protection |= 0x08;
        m_HPROT.write(protection);
        bool m_write= (trans.get_command()==tlm::TLM_WRITE_COMMAND)? true:false ;
        m_HWRITE.write(m_write);
        
      }
      else
      if (trans_type==3){ //SEQ
        txn_counter--;
        if (m_curr_locks.size()){
          lock_value=!(m_curr_locks.front()->number_of_txns==current_lock_counter && txn_counter==0); //unlock if this SEQ is the last of burst
          set_lock_ev.notify();
        }
      }
      else
      if (trans_type==0){ //IDLE
        AMBA_DUMP("GOT IDLE. Will terminate burst");
        txn_counter=0;
        if (m_curr_locks.size()){
          lock_value=!(m_curr_locks.front()->number_of_txns==current_lock_counter); //UNLOCK with this IDLE if lock group is done
          set_lock_ev.notify();
        }
      }
      /*
      if (txn_counter==0){
        if (bus_req==1) bus_req=0;
        else
        if (bus_req==2) bus_req=1;
      }
      */
      if (sc_core::sc_time_stamp()>=ready_stable_time && m_HREADYIN.read()==true)   
      {// Ok the RTL arbiter is already ready to accept it
          AMBA_DUMP("Return UPD:END_REQ");
          ph=tlm::END_REQ;
          if (trans_type==0 && m_curr_locks.size()){ //we have an ongoing lock and we are answering an explicit IDLE
            if (m_curr_locks.front()->number_of_txns==current_lock_counter){ 
              //the IDLE is the unlocking TXN and since there is no data phase for IDLE we finish it off here
              m_curr_locks.front()->atomic_txn_completed();
              m_curr_locks.pop();
            }
          }          
          return tlm::TLM_UPDATED;
      }
      else
      {
          AMBA_DUMP("Return ACC");
          pending_end_req=true;
          return tlm::TLM_ACCEPTED;
      }

    }    
    break;
    default:
      if (ph==amba::BUS_REQ){
        bus_req_txn=&trans;
        //bus_req=1;
        m_HBUSREQ.write(true);
		amba::amba_lock* lock_ext;
		//Is this a locked transaction request
		if(slave_sock.template get_extension<amba::amba_lock>(lock_ext,trans))
		{//ok this is a locked txn;
			lock_ext->value->lock_is_understood_by_slave=true;
            if (m_curr_locks.size() && m_curr_locks.front()!=lock_ext->value) m_curr_locks.push(lock_ext->value);
            lock_value=true;
		}
        else
          lock_value=false;
        set_lock_ev.notify();
        if (sc_core::sc_time_stamp()>=ready_stable_time && m_HREADYIN.read() && m_HGRANT.read()){
          bus_req_txn=NULL;
          ph=amba::BUS_GRANT;
          grant_state_at_other_side=true;
          return tlm::TLM_UPDATED;
        }
        return tlm::TLM_ACCEPTED;
      }
      else
      if(ph==amba::BUS_REQ_STOP){
		amba::amba_lock* lock_ext;
		//Is this a locked transaction request
		if(slave_sock.template get_extension<amba::amba_lock>(lock_ext,trans))
		{
          //the withdrawn BUS_REQ could remove the lock from a txn that has not yet started
          if ((m_curr_locks.size()>1 && lock_ext->value==m_curr_locks.back()) || (m_curr_locks.size()==1 && (current_lock_counter==1)))
            lock_value=false;
		}
        set_lock_ev.notify();        
        //bus_req=0;
        m_HBUSREQ.write(false);
        bus_req_txn=NULL;
      }
      else
      if(ph== amba::RESET_ASSERT) {
          m_InReset = true;
          ready_ev.cancel();
	  while (!m_curr_locks.empty()) m_curr_locks.pop();
          current_data = current_req = bus_req_txn = NULL;
          m_data_size = m_req_size = data_start_index = req_start_index = 0;
          trans_type = m_data_addr = current_data_count = txn_counter = 0;
          m_otherSideReset.write(false);
      }
      else 
      if(ph== amba::RESET_DEASSERT) {
          m_InReset = false;
          m_otherSideReset.write(true);
      }
      else
      if(ph== amba::BEGIN_DATA)
      {
        assert(current_data==&trans);
          unsigned int offset=(m_data_addr%((BUSWIDTH+7)/8));     
          unsigned int array_entry=current_data_count*m_data_size;
          if (data_start_index>0){
            array_entry+=data_start_index;
            array_entry%=trans.get_data_length();
          }
          m_tmp_dt=m_HWDATA.read();
          for (unsigned int i=0; i<m_data_size; i++){
            m_tmp_dt.range((i+offset+1)*8-1,(i+offset)*8)=trans.get_data_ptr()[i+array_entry];
            AMBA_DUMP("Got "<<(m_tmp_dt.range((i+offset+1)*8-1,(i+offset)*8).to_uint())<<" fromm array index "<<(i+array_entry));            
          }
          m_HWDATA.write(m_tmp_dt);

          if(sc_core::sc_time_stamp()>=ready_stable_time && m_HREADYIN.read()==true)
          {
          //TODO: move the code below into a function that is called from here and from the ready_method
            if (m_curr_locks.size()){
              amba::amba_lock* data_lock;
              slave_sock.get_extension(data_lock, *current_data);
              if (m_curr_locks.front()->number_of_txns==current_lock_counter && current_req!=current_data && m_curr_locks.front()==data_lock->value){ 
                //the last data phase of a txn, which is the last txn in a locked group
                m_curr_locks.front()->atomic_txn_completed();
                m_curr_locks.pop();
                if (m_curr_locks.size()) current_lock_counter=1; //if there is another lock in the Q we set the counter to 1.
                else current_lock_counter=0;
              }
            }
            current_data=NULL;         
            ph= amba::END_DATA;
            return tlm::TLM_UPDATED;
          }
          else 
          {
            pending_end_data=true;
            return tlm::TLM_ACCEPTED;
          }
          
      }
      else
          assert("Invalid phase");
    }
	return tlm::TLM_ACCEPTED;
}


/*!
 * @brief SetSlaveTimings
 *
 * @details This function is called for setting the non-default timings of the slave
 *
 */

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_CT_RTL_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::set_slave_timing(tlm::tlm_phase ph, const sc_core::sc_time& delay)
{
  sc_core::sc_time time_unit=sc_core::sc_get_time_resolution();
  if (ph==tlm::BEGIN_RESP || ph==amba::END_DATA||ph==tlm::END_REQ || ph==amba::BUS_GRANT || ph==amba::BUS_UNGRANT){
    if (delay+time_unit>ready_sample_time){
      ready_sample_time=delay+time_unit;
      AMBA_DUMP("see that my sample time is "<<ready_sample_time);
      slave_timing_info.set_start_time(tlm::BEGIN_RESP,ready_sample_time);
      slave_timing_info.set_start_time(amba::END_DATA,ready_sample_time);
      slave_timing_info.set_start_time(tlm::END_REQ,ready_sample_time);
      slave_timing_info.set_start_time(amba::BUS_GRANT,ready_sample_time);
      slave_timing_info.set_start_time(amba::BUS_UNGRANT,ready_sample_time);
      slave_sock.set_target_timing(slave_timing_info);
    }
  }
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_CT_RTL_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::set_slave_timing(gs::socket::timing_info& info)
{
    sc_core::sc_time time_unit=sc_core::sc_get_time_resolution();
#define MY_MAX(a,b) ((a>b)?a:b)
    sc_core::sc_time
    new_ready_sample=MY_MAX(slave_timing_info.get_start_time(tlm::BEGIN_RESP)+time_unit,
//                              MY_MAX(slave_timing_info.get_start_time(tlm::END_REQ)+time_unit,  //because END_REQ is not really controllable by the slave and should be covered by the other timings
                                      MY_MAX(slave_timing_info.get_start_time(amba::END_DATA)+time_unit,
                                            MY_MAX(slave_timing_info.get_start_time(amba::BUS_GRANT)+time_unit,
                                                   slave_timing_info.get_start_time(amba::BUS_UNGRANT)+time_unit
                                                  )
                                            )
//                                    )
                            );
#undef MY_MAX

    if (new_ready_sample>ready_sample_time){
      ready_sample_time=new_ready_sample;
      AMBA_DUMP("see that my sample time is "<<ready_sample_time);
      slave_timing_info.set_start_time(tlm::BEGIN_RESP,ready_sample_time);
      slave_timing_info.set_start_time(amba::END_DATA,ready_sample_time);
      slave_timing_info.set_start_time(tlm::END_REQ,ready_sample_time);
      slave_timing_info.set_start_time(amba::BUS_GRANT,ready_sample_time);
      slave_timing_info.set_start_time(amba::BUS_UNGRANT,ready_sample_time);
      slave_sock.set_target_timing(slave_timing_info);
    }

}



template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
gs::socket::timing_info& AHB_Master_CT_RTL_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::get_slave_timing()
{
	return slave_timing_info;
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
sc_core::sc_time& AHB_Master_CT_RTL_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::get_slave_timing(tlm::tlm_phase ph)
{
	return (slave_timing_info.get_start_time(ph));
}

/*
 *@brief master_timing_listener
 *
 * @details This function is triggered when the CT master sets the non-default timings
 *
 */

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_CT_RTL_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::master_timing_listener(gs::socket::timing_info info)
{
	AMBA_DUMP("Master timing listener called");
	bool do_cb=false;
    sc_core::sc_time time_unit=sc_core::sc_get_time_resolution();
	if(info.get_start_time(tlm::BEGIN_REQ)+time_unit > slave_timing_info.get_start_time(tlm::BEGIN_REQ))
	{
		do_cb=true;
		slave_timing_info.set_start_time(tlm::BEGIN_REQ,info.get_start_time(tlm::BEGIN_REQ)+time_unit);
	}
	if(info.get_start_time(amba::BEGIN_DATA)+time_unit > slave_timing_info.get_start_time(amba::BEGIN_DATA))
	{
		do_cb=true;
		slave_timing_info.set_start_time(amba::BEGIN_DATA,info.get_start_time(amba::BEGIN_DATA)+time_unit);
	}
	if(info.get_start_time(amba::BUS_REQ)+2*time_unit > slave_timing_info.get_start_time(amba::BUS_REQ))
	{
			do_cb=true;
			slave_timing_info.set_start_time(amba::BUS_REQ,info.get_start_time(amba::BUS_REQ)+2*time_unit);
	}
	if(info.get_start_time(amba::BUS_REQ_STOP)+2*time_unit > slave_timing_info.get_start_time(amba::BUS_REQ_STOP))
	{
			do_cb=true;
			slave_timing_info.set_start_time(amba::BUS_REQ_STOP,info.get_start_time(amba::BUS_REQ_STOP)+2*time_unit);
	}
    
    bool inform_caller=false;
	if(slave_timing_info.get_start_time(tlm::BEGIN_REQ) > slave_timing_info.get_start_time(tlm::END_REQ))
	{
		do_cb=true;
        inform_caller=true;
		slave_timing_info.set_start_time(tlm::END_REQ,slave_timing_info.get_start_time(tlm::BEGIN_REQ));
	}
	if(slave_timing_info.get_start_time(amba::BEGIN_DATA) > slave_timing_info.get_start_time(amba::END_DATA))
	{
		do_cb=true;
        inform_caller=true;
		slave_timing_info.set_start_time(amba::END_DATA,slave_timing_info.get_start_time(amba::BEGIN_DATA));

	}
	if(slave_timing_info.get_start_time(amba::BUS_REQ) > slave_timing_info.get_start_time(amba::BUS_GRANT))
	{
		do_cb=true;
        inform_caller=true;
		slave_timing_info.set_start_time(amba::BUS_GRANT,slave_timing_info.get_start_time(amba::BUS_REQ));

	}
    
    if (inform_caller) slave_sock.set_target_timing(slave_timing_info); //we changed EREQ or EDATA start
    
	if(timing_cb!=NULL && do_cb)
		(owner->*timing_cb)(slave_timing_info);
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
unsigned int AHB_Master_CT_RTL_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::transport_debug(tlm::tlm_generic_payload& payload)
{
#ifdef CARBON_DEBUG_ACCESS
	unsigned long long address = payload.get_address();
	unsigned int length=payload.get_data_length();
	unsigned int * ctrl = new unsigned int();
	if(slave_sock.template get_extension<amba::amba_secure_access>(payload))
       	ctrl[0]=0x01;
    else
    	ctrl[0]=0x00;
    CarbonDebugAccessStatus retval;
	if(payload.get_command()==tlm::TLM_READ_COMMAND)
	{
       retval= debugIf->debugMemRead(address,(payload.get_data_ptr()),length,ctrl);
	}
	else if(payload.get_command()==tlm::TLM_WRITE_COMMAND)
	{
		retval= debugIf->debugMemWrite(address,(payload.get_data_ptr()),length,ctrl);
	}
	unsigned int transferred_bytes=0;
	switch(retval)
	{
		case eCarbonDebugAccessStatusOutRange:
		{
			payload.set_response_status(tlm::TLM_ADDRESS_ERROR_RESPONSE);
                        break;
		}
		case eCarbonDebugAccessStatusOk:{
			payload.set_response_status(tlm::TLM_OK_RESPONSE);
                        transferred_bytes=length;
			break;
		}
		case eCarbonDebugAccessStatusError:{
			payload.set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
                        break;

		}
		case eCarbonDebugAccessStatusPartial:{
                        // We should return the number of bytes transferred
                        break;
		}
		default:
			assert("Invalid DebugAccess return value");
	}
	delete(ctrl);
	return transferred_bytes;
#else
  return 0;
#endif
}


#ifdef CARBON_DEBUG_ACCESS
template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_CT_RTL_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::registerDebugAccessCB(CarbonDebugAccessIF* dbgIf)
{
 debugIf=dbgIf;
}
#endif

//This method is sensitive to bus_req_ev, which is not being notified at all
//in the current implementation so no special handling for RESET state
template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_CT_RTL_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::bus_req_method()
{
#ifdef AMBA_DEBUG
  if (m_HBUSREQ.read()!=bus_req) AMBA_DUMP("RTL_CT::Set bus req to "<<bus_req);
#endif
  m_HBUSREQ.write(bus_req);
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_CT_RTL_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::write_trans_type()
{
  AMBA_DUMP("Set Trans TYPE "<<trans_type);
  m_HTRANS.write(trans_type);
}

//m_addr_ev (on which this method is sensitive) is only notified from clock_tick and
//nb_transport_fw, which are appropriately handled for RESET state
template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_CT_RTL_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::addr_method()
{
  AMBA_DUMP("Set Addr "<<m_addr);
  m_HADDR.write(m_addr);
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_CT_RTL_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::clock_tick()
{
  if (m_InReset) return;

  if (txn_counter) //we are somewhere in a burst
    trans_type=1;
  else //no ongoing burst
    trans_type=0;
  if (m_HREADYIN.read() && m_HTRANS.read() != trans_type) trans_type_ev.notify(); //reset the trans type. either to BUSY or IDLE.
  //bus_req_ev.notify(bus_req_start_time);
  
  ready_stable_time=sc_core::sc_time_stamp()+ready_sample_time;
  ready_ev.notify(ready_sample_time);


  if (current_req  && m_HREADYIN.read() && m_HTRANS.read()>1){ //accepted NONSEQ or SEQ now we expect data
    switch(m_HSIZE.read().to_uint()){
      case 0:{m_addr=m_HADDR.read()+1;break;}
      case 1:{m_addr=m_HADDR.read()+2;break;}
      case 2:{m_addr=m_HADDR.read()+4;break;}
      case 3:{m_addr=m_HADDR.read()+8;break;}
      case 4:{m_addr=m_HADDR.read()+16;break;}
      case 5:{m_addr=m_HADDR.read()+32;break;}
      case 6:{m_addr=m_HADDR.read()+64;break;}
      case 7:{m_addr=m_HADDR.read()+128;break;}
    }
    if (m_HBURST.read()[0]==0 && m_HBURST.read()!=0) { //all wraps
      unsigned int len=current_req->get_data_length();
      if (m_addr==current_req->get_address()+len) //we fell over the end so we better wrap
        m_addr=current_req->get_address(); //now wrap to start
    }
    m_addr_ev.notify();
    current_data=current_req;
    m_data_size=m_req_size;
    data_start_index=req_start_index;
    m_data_addr=m_HADDR.read();
    current_data_count=(m_HTRANS.read()==2)?0:current_data_count+1;
  }
  
  if (!m_HGRANT && m_HREADYIN.read()) txn_counter=0; //if not granted any burst is killed


  if (current_req && m_HREADYIN.read()){ //accepted a REQ
    if (txn_counter==0){    //last of burst
      current_req=NULL; //done with the reqs
    }
  }

  
  if (bus_req_txn && m_HREADYIN.read() && m_HGRANT.read()){ //bus grant
    bus_req_txn=NULL;
  }

}

/*!
 * @brief bus_grant_method
 *
 * @details This function is triggered when the arbiter gives grant
 * for the bus request
 *
 */

//ready_ev is notified only in clock_tick and is appropriately canceled
//when RESET is entered
template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_CT_RTL_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::ready_method()
{
  tlm::tlm_phase ph;
  sc_core::sc_time delay = sc_core::SC_ZERO_TIME;
  //GRANT_BLOCK: //label
  if (!grant_state_at_other_side && m_HBUSREQ.read() && m_HGRANT.read() && m_HREADYIN.read())
  {
    ph=amba::BUS_GRANT;
    AMBA_DUMP("GRANT CHANGE "<<ph);
    grant_state_at_other_side=true;
    slave_sock->nb_transport_bw(*m_ungrant_txn,ph,delay);
  }
  else
  if (grant_state_at_other_side && !m_HGRANT.read())
  {
    ph=amba::BUS_UNGRANT;
    AMBA_DUMP("GRANT CHANGE "<<ph);
    grant_state_at_other_side=false;
    slave_sock->nb_transport_bw(*m_ungrant_txn,ph,delay);    
  }
    
  if (!m_HREADYIN.read()) {
    if (!current_data) return;
    if (m_HRESP.read()==1){
      AMBA_DUMP("ERROR RESP");
      current_data->set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
      goto E_DATA_BLOCK;
    }
    else
    if (m_HRESP.read()==2){ //retry
      AMBA_DUMP("RETRY RESP");
      current_data->set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
      ph=amba::DATA_RETRY;
      slave_sock->nb_transport_bw(*current_req, ph, delay);
    }
    else
    if (m_HRESP.read()==3){ //split
      AMBA_DUMP("SPLIT RESP");
      current_data->set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
      ph=amba::DATA_SPLIT;
      slave_sock->nb_transport_bw(*current_req, ph, delay);
    }
    return;
  }
  //E_REQ_BLOCK: //label
  {
    if (current_req && pending_end_req){
      pending_end_req=false;
      ph=tlm::END_REQ;
      AMBA_DUMP("END REQ");
      slave_sock->nb_transport_bw(*current_req, ph, delay);
      if (m_HTRANS.read()==0 && m_curr_locks.size()){ //we have an ongoing lock and we are answering an explicit IDLE
        if (m_curr_locks.front()->number_of_txns==current_lock_counter){ 
          //the IDLE is the unlocking TXN and since there is no data phase for IDLE we finish it off here
          m_curr_locks.front()->atomic_txn_completed();
          m_curr_locks.pop();
        }
      }
    }    
  }
  if (!m_HRESP.read()==0) return; 
  if (!current_data ) return;
  current_data->set_response_status(tlm::TLM_OK_RESPONSE);
  E_DATA_BLOCK:
  {
    if (m_curr_locks.size()){
      amba::amba_lock* data_lock;
      slave_sock.get_extension(data_lock, *current_data);
      if (m_curr_locks.front()->number_of_txns==current_lock_counter && current_req!=current_data && m_curr_locks.front()==data_lock->value){ 
        //the laste data phase of a txn, which is the last txn in a locked group
        m_curr_locks.front()->atomic_txn_completed();
        m_curr_locks.pop();
        if (m_curr_locks.size()) current_lock_counter=1; //if there is another lock in the Q we set the counter to 1.
        else current_lock_counter=0;
      }
    }  
    if (current_data->is_write() && pending_end_data){ //make sure we got that BEGIN_DATA before sending END_DATA
      AMBA_DUMP("END DATA");
      ph=amba::END_DATA;
      slave_sock->nb_transport_bw(*current_data, ph, delay);
      current_data=NULL;
      pending_end_data=false;
    }
    else
    if (current_data->is_read()){
        ph= tlm::BEGIN_RESP;
		unsigned char * m_data= current_data->get_data_ptr();
        unsigned int  len =current_data->get_data_length();
     
        //TODO: make endianess save.
        unsigned int offset=(m_data_addr%((BUSWIDTH+7)/8));     
        unsigned int array_entry=current_data_count*m_data_size;
        if (data_start_index>0){
          array_entry+=data_start_index;
          array_entry%=len;
        }
        for (unsigned int i=0; i<m_data_size; i++){
          m_data[i+array_entry]=(unsigned char)(m_HRDATA.read().range((i+offset+1)*8-1,(i+offset)*8).to_uint()); 
        }
        AMBA_DUMP("BEGIN RESP");
		slave_sock->nb_transport_bw(*current_data,ph,delay);
        current_data=NULL;
    }
  }
}    

#endif /*AHB_MASTER_CT_RTL_ADAPTER_H_*/
