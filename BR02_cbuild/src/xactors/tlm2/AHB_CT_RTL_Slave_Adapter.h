// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef AHB_CT_RTL_Slave_Adapter_H_
#define AHB_CT_RTL_Slave_Adapter_H_

#define TRAN_IDLE 0
#define TRAN_BUSY 1
#define TRAN_NONSEQ 2
#define TRAN_SEQ 3

#define OK_RESP 0
#define ERROR_RESP 1
#define RETRY_RESP 2
#define SPLIT_RESP 3

template<unsigned int BUSWIDTH, typename MODULE,bool FORCE_BIGUINT=false, bool IS_LITE=false>
class AHB_CT_RTL_Slave_Adapter: public sc_core::sc_module
{
public:
	amba::amba_slave_socket<BUSWIDTH> slave_sock;
	
	sc_core::sc_in_clk m_clk;
        sc_core::sc_signal<bool> m_otherSideReset;
	sc_core::sc_in<bool> m_Reset;
	//RTL ports
	  sc_core::sc_out<bool > m_HSEL;
	  sc_core::sc_out<bool > m_HWRITE;
	  sc_core::sc_out<sc_dt::sc_uint<2> > m_HTRANS;
	  sc_core::sc_out<sc_dt::sc_uint<32> > m_HADDR;
	  sc_core::sc_out<sc_dt::sc_uint<3> > m_HSIZE;
	  sc_core::sc_out<sc_dt::sc_uint<3> > m_HBURST;
	  sc_core::sc_out<sc_dt::sc_uint<4> > m_HPROT;
	  sc_core::sc_out<bool > m_HREADYIN;
	  sc_core::sc_out<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type> m_HWDATA;
	  sc_core::sc_in<bool > m_HREADYOUT;
	  sc_core::sc_in<typename amba::port_enabler<!IS_LITE >::resp_type > m_HRESP;
	  sc_core::sc_in<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type> m_HRDATA;
	  typename amba::port_enabler<!IS_LITE, sc_dt::sc_uint<4> >::output_port_type m_HMASTER;
	  sc_core::sc_out<bool> m_HMASTLOCK;
	  typename amba::port_enabler<!IS_LITE, sc_dt::sc_uint<16> >::input_port_type m_HSPLIT;
	  
	  //transport call
	  tlm::tlm_sync_enum nb_transport_fw(tlm::tlm_generic_payload &, tlm::tlm_phase &, sc_core::sc_time &);
	  void set_slave_timing(gs::socket::timing_info& info);
	  void set_slave_timing(tlm::tlm_phase, const sc_core::sc_time&);
	  gs::socket::timing_info & get_slave_timing();
	  sc_core::sc_time & get_slave_timing(tlm::tlm_phase);
	  inline void register_master_timing_listener(MODULE* mod, void (MODULE::*fn)(gs::socket::timing_info))
	  {
	   	owner=mod;
	   	timing_cb=fn;
	  }
	  
	  //systemc processes
	  void clock_tick();
	  void write_select();
	  void write_trans_type();
	  void ready_method();
	  void split_transaction();
	  void addr_method();
	  void set_lock_emthod();
	  void master_timing_listener(gs::socket::timing_info);
	  void end_of_elaboration();
	  void start_of_simulation();
	  void set_lock_method();
	  void write_ready_method();
	  
	 ///APIs for Carbon Debug
#ifdef CARBON_DEBUG_ACCESS
	 void registerDebugAccessCB(CarbonDebugAccessIF*);
#endif
	 unsigned int transport_debug(tlm::tlm_generic_payload & trans);

	  SC_HAS_PROCESS(AHB_CT_RTL_Slave_Adapter);
	  AHB_CT_RTL_Slave_Adapter(sc_core::sc_module_name nm):sc_module(nm),
	  slave_sock("AHB_Slave_RTL_CT", IS_LITE? amba::amba_AHBLite : amba::amba_AHB, amba::amba_CT,false),
	  m_clk("m_clk"),
	  m_Reset("AHB_Reset"),
	  m_HSEL("AHB_HSEL"),
	  m_HWRITE("AHB_HWRITE"),
	  m_HTRANS("AHB_HTRANS"),
	  m_HADDR("AHB_HADDR"),
	  m_HSIZE("AHB_HSIZE"),
	  m_HBURST("AHB_HBURST"),
	  m_HPROT("AHB_HPROT"),
	  m_HREADYIN("AHB_HREADYIN"),
	  m_HWDATA("AHB_HWDATA"),
	  m_HREADYOUT("AHB_HREADYOUT"),
	  m_HRESP("AHB_HRESP"),
	  m_HRDATA("AHB_HRDATA"),
	  m_HMASTER("AHB_HMASTER"),
	  m_HMASTLOCK("AHB_MASTLOCK"),
	  m_HSPLIT("AHB_HSPLIT"),
	  current_req(NULL),
	  current_data(NULL),
	  current_lock_counter(0),
	  pending_end_req(false),
	  pending_end_data(false),
	  pending_begin_resp(false),
	  data_counter(0),
	  select_val(false),
	  trans_type(0),
	  ready_value(0),
	  lock_value(false),
	  force_ready(false),
          m_InReset(false),
	  timing_cb(NULL),
	  owner(NULL),
#ifdef CARBON_DEBUG_ACCESS      
	  debugIf(NULL),
#endif
	  m_data_addr(0),
	  req_start_index(0),
	  data_start_index(0),
	  m_req_size(0),
	  m_data_size(0),
	  txn_counter(0),
	  m_addr(0)

	  {
                  SC_METHOD(ResetMethod);
                  sensitive << m_Reset;
                  dont_initialize();

		  SC_METHOD(clock_tick);
		  sensitive<< m_clk.pos();
		  dont_initialize();

		  SC_METHOD(write_select);
		  sensitive<< select_ev;
		  dont_initialize();

		  SC_METHOD(write_trans_type);
		  sensitive << trans_ev;
		  dont_initialize();

		  SC_METHOD(split_transaction);
		  sensitive<< split_ev;
		  dont_initialize();
		  
		  
		    SC_METHOD(set_lock_method);
			sensitive<< set_lock_ev;
			dont_initialize();

			SC_METHOD(addr_method);
			sensitive<< m_addr_ev;
			dont_initialize();

	        SC_METHOD(ready_method);
	        sensitive<<ready_ev;
	        dont_initialize();

	        SC_METHOD(write_ready_method);
	        sensitive<< write_ready_ev;
	        dont_initialize();


		  slave_sock.template register_nb_transport_fw< AHB_CT_RTL_Slave_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE> >(this, & AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::nb_transport_fw);
                  slave_sock.template register_transport_dbg(this, &AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::transport_debug);
		  slave_sock.set_timing_listener_callback(this, & AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::master_timing_listener);
	  }
	  ~AHB_CT_RTL_Slave_Adapter();
private:
         void ResetMethod ();
	 // sc_core::sc_time endreq_sample_time;
	  //sc_core::sc_time enddata_sample_time;
	  sc_core::sc_time dataContinue_sample_time, ready_sample_time,ready_stable_time, ready_check_time;
	  
	  //We are keeping separate buffers for the
	  //request and the data transaction due to the pipelined
	  // feature of the AHB
	  
	  tlm::tlm_generic_payload * m_reset_trans;
	  tlm::tlm_generic_payload * current_req;
	  tlm::tlm_generic_payload * current_data;
	  unsigned int current_lock_counter;
	  bool pending_end_req;
	  bool pending_end_data;
	  bool pending_begin_resp;
          bool m_InReset;
	  unsigned int data_counter;
	 // sc_core::sc_event end_req_ev;
	  //sc_core::sc_event end_data_ev;
	  //sc_core::sc_event begin_resp_ev;
	  sc_core::sc_event select_ev,ready_ev;
	  sc_core::sc_event write_ready_ev;
	  sc_core::sc_event trans_ev, m_addr_ev, set_lock_ev,split_ev;
	  bool select_val;
	  unsigned int trans_type;
	  unsigned int ready_value;
      bool lock_value, force_ready;
	  std::queue<amba::lock_object_base*> m_curr_locks;
	  void(MODULE::*timing_cb)(gs::socket::timing_info);
	  MODULE* owner;
	  gs::socket::timing_info slave_timing_info;
	  //pointer for the debug access
#ifdef CARBON_DEBUG_ACCESS
	  CarbonDebugAccessIF * debugIf;
#endif
	  typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type m_tmp_dt;
	  sc_dt::uint64 m_data_addr;
	  unsigned int req_start_index, data_start_index, m_req_size, m_data_size, txn_counter, m_addr;
};

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::~AHB_CT_RTL_Slave_Adapter()
{
}

/*
 * @brief this fuction is triggered when the slave writes to its
 * Reset output
 * 
 */
template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::ResetMethod()
{
  tlm::tlm_phase ph;
  tlm::tlm_sync_enum retval;
  sc_core::sc_time delay = sc_core::SC_ZERO_TIME;
  if (m_reset_trans == NULL)
    m_reset_trans = slave_sock.get_reset_txn();

  if (m_Reset.read()) {
    m_InReset = false;
    ph = amba::RESET_DEASSERT;
  }
  else { //ACTIVE LOW
    m_InReset = true;
    while (!m_curr_locks.empty()) m_curr_locks.pop();
    txn_counter = 0;
    trans_type = TRAN_IDLE;
    select_val = false;
    select_ev.cancel();
    split_ev.cancel();
    ready_ev.cancel();
    trans_ev.cancel();
    m_addr = 0;
    m_addr_ev.cancel();
    current_data = current_req = NULL;
    m_data_size = m_req_size = 0;
    data_start_index = req_start_index = -1;
    m_data_addr = data_counter = 0;

    ph = amba::RESET_ASSERT;
  }
  retval = slave_sock->nb_transport_bw(*m_reset_trans, ph, delay);
  assert(retval== tlm::TLM_ACCEPTED && "Wrong tlm_sync_enum returned");
}


template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::write_ready_method()
{
	m_HREADYIN.write(ready_value);

}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::end_of_elaboration()
{

	slave_sock.activate_synchronization_protection();
	sc_core::sc_time delay= sc_core::sc_get_time_resolution();
	//guard against overriding already calculated stuff.
	 if (slave_timing_info.get_start_time(tlm::BEGIN_REQ)<delay) slave_timing_info.set_start_time(tlm::BEGIN_REQ,delay);
	 if (slave_timing_info.get_start_time(tlm::END_REQ)<delay) slave_timing_info.set_start_time(tlm::END_REQ,delay);
	 if (slave_timing_info.get_start_time(amba::BEGIN_DATA)<delay) slave_timing_info.set_start_time(amba::BEGIN_DATA,delay);
	 if (slave_timing_info.get_start_time(amba::END_DATA)<delay) slave_timing_info.set_start_time(amba::END_DATA,delay);
	 if (slave_timing_info.get_start_time(tlm::BEGIN_RESP)<delay) slave_timing_info.set_start_time(tlm::BEGIN_RESP,delay);
	 if (slave_timing_info.get_start_time(amba::DATA_CONTINUE)<delay) slave_timing_info.set_start_time(amba::DATA_CONTINUE,delay);
	ready_sample_time=slave_timing_info.get_start_time(amba::END_DATA);

	slave_sock.set_target_timing(slave_timing_info);
	if(timing_cb!=NULL) (owner->*timing_cb)(slave_timing_info);
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::start_of_simulation()
{
	ready_stable_time=ready_sample_time;

}

/*!
 * @brief SetSlaveTimings
 *
 * @details This function is called for setting the non-default timings of the slave
 *
 */

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::set_slave_timing(tlm::tlm_phase ph, const sc_core::sc_time& delay)
{
  sc_core::sc_time time_unit=sc_core::sc_get_time_resolution();
  if (ph==tlm::BEGIN_RESP || ph==amba::END_DATA||ph==tlm::END_REQ || ph==amba::BUS_GRANT || ph==amba::BUS_UNGRANT){
    if (delay+time_unit>ready_sample_time){
      ready_sample_time=delay+time_unit;
      slave_timing_info.set_start_time(tlm::BEGIN_RESP,ready_sample_time);
      slave_timing_info.set_start_time(amba::END_DATA,ready_sample_time);
      slave_timing_info.set_start_time(tlm::END_REQ,ready_sample_time);
      slave_timing_info.set_start_time(amba::DATA_CONTINUE,ready_sample_time);
      slave_sock.set_target_timing(slave_timing_info);
    }
  }
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::set_slave_timing(gs::socket::timing_info& info)
{
    sc_core::sc_time time_unit=sc_core::sc_get_time_resolution();
#define MY_MAX(a,b) ((a>b)?a:b)
    sc_core::sc_time
    new_ready_sample=MY_MAX(slave_timing_info.get_start_time(tlm::BEGIN_RESP)+time_unit,
                              MY_MAX(slave_timing_info.get_start_time(tlm::END_REQ)+time_unit,
                                      MY_MAX(slave_timing_info.get_start_time(amba::END_DATA)+time_unit,
                                                   slave_timing_info.get_start_time(amba::DATA_CONTINUE)+time_unit
                                                  )

                                    )
                            );
#undef MY_MAX

    if (new_ready_sample>ready_sample_time){
      ready_sample_time=new_ready_sample;
      slave_timing_info.set_start_time(tlm::BEGIN_RESP,ready_sample_time);
      slave_timing_info.set_start_time(amba::END_DATA,ready_sample_time);
      slave_timing_info.set_start_time(tlm::END_REQ,ready_sample_time);
      slave_timing_info.set_start_time(amba::DATA_CONTINUE,ready_sample_time);
      slave_sock.set_target_timing(slave_timing_info);
    }

}



template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
gs::socket::timing_info& AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::get_slave_timing()
{
	return slave_timing_info;
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
sc_core::sc_time& AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::get_slave_timing(tlm::tlm_phase ph)
{
	return (slave_timing_info.get_start_time(ph));
}

/*
 *@brief master_timing_listener
 *
 * @details This function is triggered when the CT master sets the non-default timings
 *
 */

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::master_timing_listener(gs::socket::timing_info info)
{
	AMBA_DUMP("Master timing listener called");
	bool do_cb=false;
    sc_core::sc_time time_unit=sc_core::sc_get_time_resolution();
	if(info.get_start_time(tlm::BEGIN_REQ)+time_unit > slave_timing_info.get_start_time(tlm::BEGIN_REQ))
	{
		do_cb=true;
		slave_timing_info.set_start_time(tlm::BEGIN_REQ,info.get_start_time(tlm::BEGIN_REQ)+time_unit);
	}
	if(info.get_start_time(amba::BEGIN_DATA)+time_unit > slave_timing_info.get_start_time(amba::BEGIN_DATA))
	{
		do_cb=true;
		slave_timing_info.set_start_time(amba::BEGIN_DATA,info.get_start_time(amba::BEGIN_DATA)+time_unit);
	}

    bool inform_caller=false;
	if(slave_timing_info.get_start_time(tlm::BEGIN_REQ) > slave_timing_info.get_start_time(tlm::END_REQ))
	{
		do_cb=true;
        inform_caller=true;
		slave_timing_info.set_start_time(tlm::END_REQ,slave_timing_info.get_start_time(tlm::BEGIN_REQ));
	}
	if(slave_timing_info.get_start_time(amba::BEGIN_DATA) > slave_timing_info.get_start_time(amba::END_DATA))
	{
		do_cb=true;
        inform_caller=true;
		slave_timing_info.set_start_time(amba::END_DATA,slave_timing_info.get_start_time(amba::BEGIN_DATA));

	}
    if (inform_caller) slave_sock.set_target_timing(slave_timing_info); //we changed EREQ or EDATA start

	if(timing_cb!=NULL && do_cb)
		(owner->*timing_cb)(slave_timing_info);
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
unsigned int AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::transport_debug(tlm::tlm_generic_payload& payload)
{
#ifdef CARBON_DEBUG_ACCESS
	unsigned long long address = payload.get_address();
	unsigned int length=payload.get_data_length();
	unsigned int * ctrl = new unsigned int();
	if(slave_sock.template get_extension<amba::amba_secure_access>(payload))
       	ctrl[0]=0x01;
    else
    	ctrl[0]=0x00;
    CarbonDebugAccessStatus retval;
	if(payload.get_command()==tlm::TLM_READ_COMMAND)
	{
       retval= debugIf->debugMemRead(address,(payload.get_data_ptr()),length,ctrl);
	}
	else if(payload.get_command()==tlm::TLM_WRITE_COMMAND)
	{
		retval= debugIf->debugMemWrite(address,(payload.get_data_ptr()),length,ctrl);
	}
	unsigned int transferred_bytes=0;
	switch(retval)
	{
		case eCarbonDebugAccessStatusOutRange:
		{
			payload.set_response_status(tlm::TLM_ADDRESS_ERROR_RESPONSE);
                        break;
		}
		case eCarbonDebugAccessStatusOk:{
			payload.set_response_status(tlm::TLM_OK_RESPONSE);
                        transferred_bytes=length;
			break;
		}
		case eCarbonDebugAccessStatusError:{
			payload.set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
                        break;

		}
		case eCarbonDebugAccessStatusPartial:{
                        // We should return the number of bytes transferred
                        break;
		}
		default:
			assert("Invalid DebugAccess return value");
	}
	delete(ctrl);
	return transferred_bytes;
#else
    return 0;
#endif
}

#ifdef CARBON_DEBUG_ACCESS
template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::registerDebugAccessCB(CarbonDebugAccessIF* dbgIf)
{
 debugIf=dbgIf;
}
#endif

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::clock_tick()
{
        if (m_InReset) return;

	//Resetting the transtype
	if(txn_counter==0
		|| (m_HTRANS.read()==0)	)// in case of imprecise bursts/early bursts, we can't rely on counter
	{
		trans_type=TRAN_IDLE;
	}
	else if(txn_counter!=0)
	{
		trans_type=TRAN_BUSY;
	}
	if (m_HREADYIN.read() && m_HTRANS.read() != trans_type)
	{	//reset the trans type. either to BUSY or IDLE.
		trans_ev.notify();
	}
	ready_stable_time=sc_core::sc_time_stamp()+ready_sample_time;
	ready_ev.notify(ready_sample_time);

	if (current_req  && m_HREADYIN.read() && m_HTRANS.read()>1){ //accepted NONSEQ or SEQ now we expect data
	    switch(m_HSIZE.read().to_uint()){
	      case 0:{m_addr=m_HADDR.read()+1;break;}
	      case 1:{m_addr=m_HADDR.read()+2;break;}
	      case 2:{m_addr=m_HADDR.read()+4;break;}
	      case 3:{m_addr=m_HADDR.read()+8;break;}
	      case 4:{m_addr=m_HADDR.read()+16;break;}
	      case 5:{m_addr=m_HADDR.read()+32;break;}
	      case 6:{m_addr=m_HADDR.read()+64;break;}
	      case 7:{m_addr=m_HADDR.read()+128;break;}
	    }
	    if (m_HBURST.read()[0]==0 && m_HBURST.read()!=0) { //all wraps
	      unsigned int len=current_req->get_data_length();
	      if (m_addr==current_req->get_address()+len) //we fell over the end so we better wrap
	        m_addr=current_req->get_address(); //now wrap to start
	    }
	    m_addr_ev.notify();
	    current_data=current_req;
	    m_data_size=m_req_size;
	    data_start_index=req_start_index;
	    m_data_addr=m_HADDR.read();
	    data_counter=(m_HTRANS.read()==2)?0:data_counter+1;
	}
	if (current_req && m_HREADYIN.read()){ //accepted a REQ
	    if (txn_counter==0){    //last of burst
	      current_req=NULL; //done with the reqs
	      if(m_HSEL.read())
	      {
	    	  select_val=false;
	    	  select_ev.notify();
	      }
	    }
	}
	if(m_HRESP.read()==3 && m_HREADYIN.read()== true)
	{// If the last cycle was 2nd SPLIT cycle then we should check the HSPLIT response
		split_ev.notify(dataContinue_sample_time); //that thing is not intialized yet!
	}

}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::write_select()
{
  m_HSEL.write(select_val);
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::write_trans_type()
{
  m_HTRANS.write(trans_type);
}

/*
 *@brief nb_transport_fw 
 * @details This is non-blocking forward transport
 * carrying the request/data phase
 */

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
tlm::tlm_sync_enum  AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::nb_transport_fw(tlm::tlm_generic_payload & trans, tlm::tlm_phase & ph, sc_core::sc_time & delay)
{
	amba::amba_trans_type * type;
	AMBA_DUMP("nb_fw, phase" << ph);
        //When in RESET state, process calls on fw interface only when the Phase is RESET_DEASSERT
        if (m_InReset && (ph != amba::RESET_DEASSERT)) return tlm::TLM_ACCEPTED;

	amba::amba_burst_size* size;
	select_val=true;
	select_ev.notify();
	if(ph==tlm::BEGIN_REQ)
	{

		current_req=&trans;
		slave_sock.template get_extension<amba::amba_trans_type>(type,*current_req);
		slave_sock.template get_extension<amba::amba_burst_size>(size,*current_req);
		unsigned int len= current_req->get_data_length();
		unsigned int address = current_req->get_address();
		trans_type= (unsigned int)type->value;
		trans_ev.notify();
		/*The master doesn't send any ID to the Arbiter, but arbiter broadcasts 
		 * the MasterID which has the current access to the AHB bus, we currentl rely 
		 * that the CT arbiter will send this ID using amba::tag_id extension
		 * This id is used by the slave while slave's split/continue transcations
		 */
		amba::amba_id * id;
		if(slave_sock.template get_extension<amba::amba_id>(id, trans))
		{
			unsigned int masterid= (1<< id->value);
			m_HMASTER.write(masterid);
		}
		
		bool m_write= (current_req->get_command()== tlm::TLM_WRITE_COMMAND)? true:false;
		m_HWRITE.write(m_write);
		if(trans_type==2)
		{//non-sequential
			 amba::amba_wrap_offset* wrap_ext;
			req_start_index=0;
			if (slave_sock.get_extension(wrap_ext,trans)) req_start_index=wrap_ext->value;
			m_addr=trans.get_address()+req_start_index;
			m_addr_ev.notify();

			m_req_size=(BUSWIDTH+7)/8;
			amba::amba_burst_size* m_burst_size;
			if (slave_sock.get_extension(m_burst_size, trans)) m_req_size=m_burst_size->value;
			unsigned int burst= ((m_req_size) >> 1);
			m_HSIZE.write(burst);

		  amba::amba_lock* lock_ext;
		   if (slave_sock.get_extension(lock_ext, trans)){
			 assert(m_curr_locks.size() && lock_ext->value==m_curr_locks.front());
				  lock_value=!(m_curr_locks.front()->number_of_txns==current_lock_counter && m_req_size==trans.get_data_length()); //unlock if single beat NONSEQ. otherwise unlock at SEQ or IDLE
		   }
		   else
			  lock_value=false;
			set_lock_ev.notify();
			 if(slave_sock.template get_extension<amba::amba_imprecise_burst>(trans))
			{//ok this is incrementing burst with undefined length
				m_HBURST.write(1);
				txn_counter=~0;
			}
			else
			{
				txn_counter=trans.get_data_length()/m_req_size;
				switch(txn_counter)
				{
					case 1:m_HBURST.write(0); break;//this is SINGLE Transfer
					case 4:
						if(req_start_index)
							m_HBURST.write(2); //WRAPPING 4 beat
						else
							m_HBURST.write(3); //4beat INCREMENTING
						break;
					case 8:
						if(req_start_index)
							m_HBURST.write(4); //WRAPPING 8 beat
						else
							m_HBURST.write(5); //8 beat INCREMENTING
						break;
					case 16:
						if(req_start_index)
							m_HBURST.write(6); //WRAPPING 16 beat
						else
							m_HBURST.write(7); //16 beat INCREMENTING
						break;
					default:
						assert("Invalid Burst type");

				}
				txn_counter--;
			}


			///check the protection access
			unsigned char protection=0;
			if(slave_sock.template get_extension<amba::amba_privileged>(trans))
				protection |= 0x02;
			if(!slave_sock.template get_extension<amba::amba_instruction>(trans))
				  protection |= 0x01;
			if(slave_sock.template get_extension<amba::amba_bufferable>(trans))
                protection |= 0x04;
            if(slave_sock.template get_extension<amba::amba_cacheable>(trans))
                protection |= 0x08;
			m_HPROT.write(protection);

		}
		else
		  if (trans_type==3){ //SEQ
			txn_counter--;
			if (m_curr_locks.size()){
			  lock_value=!(m_curr_locks.front()->number_of_txns==current_lock_counter && txn_counter==0); //unlock if this SEQ is the last of burst
			  set_lock_ev.notify();
			}
		  }
		  else
		  if (trans_type==0){ //IDLE
			AMBA_DUMP("GOT IDLE. Will terminate burst");
			txn_counter=0;
			if (m_curr_locks.size()){
			  lock_value=!(m_curr_locks.front()->number_of_txns==current_lock_counter); //UNLOCK with this IDLE if lock group is done
			  set_lock_ev.notify();
			}
		  }
		if(trans_type==2 && current_req!=current_data)
		{//for the non-seq transfers we will always return tlm::updated
          //in fact, if everybody adheres to the protocol, this call happens
          //  only if we are at the stable time OR when the slave is currently not in an active data phase so is not in control of HREADYIN,
			if(true)
			{
				AMBA_DUMP("Return UPD:END_REQ");
				 ph=tlm::END_REQ;
				  if (trans_type==0 && m_curr_locks.size()){ //we have an ongoing lock and we are answering an explicit IDLE
					if (m_curr_locks.front()->number_of_txns==current_lock_counter){
					  //the IDLE is the unlocking TXN and since there is no data phase for IDLE we finish it off here
					  m_curr_locks.front()->atomic_txn_completed();
					  m_curr_locks.pop();
					}
				  }
				if (sc_core::sc_time_stamp()>=ready_check_time){ //means ready method already set HREADY_IN, so we can override it
				  ready_value=1;
				  write_ready_ev.notify();
				}
				else{ //ready method will start later, so tell it to set HREADYIN
				  force_ready=1;
				}
				  pending_end_req=false;
				  return tlm::TLM_UPDATED;
			}
			else
			{
				AMBA_DUMP("Return ACC");
				pending_end_req=true;
				return tlm::TLM_ACCEPTED;
			}

		}
		else
		{//for SEQ transfers
			AMBA_DUMP("Return UPD:END_REQ");
			pending_end_req=false;
			ph=tlm::END_REQ;
			return tlm::TLM_UPDATED;
		}
	}
	else if(ph== amba::BEGIN_DATA)
	{
		assert(current_data== &trans);
		//read the data from the TLM trasaction
		  unsigned int offset=(m_data_addr%((BUSWIDTH+7)/8));
		  unsigned int array_entry=data_counter*m_data_size;
		  if (data_start_index>0){
			array_entry+=data_start_index;
			array_entry%=trans.get_data_length();
		  }
		  m_tmp_dt=m_HWDATA.read();
		  for (unsigned int i=0; i<m_data_size; i++){
			m_tmp_dt.range((i+offset+1)*8-1,(i+offset)*8)=trans.get_data_ptr()[i+array_entry];
			AMBA_DUMP("Got "<<(m_tmp_dt.range((i+offset+1)*8-1,(i+offset)*8).to_uint())<<" fromm array index "<<(i+array_entry));
		}
		m_HWDATA.write(m_tmp_dt);
		if(sc_core::sc_time_stamp()>=ready_stable_time)
		{
			pending_end_data=false;
		  //sample the HRESP too now
			unsigned int response= m_HRESP.read();
			ready_value=m_HREADYOUT.read();
			write_ready_ev.notify();
			if(m_HREADYOUT.read()==true){
			if (m_curr_locks.size())
			{
				  amba::amba_lock* data_lock;
				  slave_sock.get_extension(data_lock, *current_data);
				  if (m_curr_locks.front()->number_of_txns==current_lock_counter && current_req!=current_data && m_curr_locks.front()==data_lock->value){
					//the last data phase of a txn, which is the last txn in a locked group
					m_curr_locks.front()->atomic_txn_completed();
					m_curr_locks.pop();
					if (m_curr_locks.size()) current_lock_counter=1; //if there is another lock in the Q we set the counter to 1.
					else current_lock_counter=0;
				  }
				}

			}

			switch(response)
			{
			case OK_RESP:{
				if(m_HREADYOUT.read()==true)
				{
					trans.set_response_status(tlm::TLM_OK_RESPONSE);
					ph=amba::END_DATA;
					return tlm::TLM_UPDATED;
				}
                else {
                  pending_end_data=true;
                  return tlm::TLM_ACCEPTED;
                }
                  
				 break;
			}
			case ERROR_RESP:{
				if(m_HREADYOUT.read()==false)
				{
					trans.set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
					ph=amba::END_DATA;
				    return tlm::TLM_UPDATED;
				}
				break;
			}
			case SPLIT_RESP:{
				if(m_HREADYOUT.read()==false)
				{
					ph=amba::DATA_SPLIT;
					return tlm::TLM_UPDATED;
				}
				break;
			}
			case RETRY_RESP:{
				if(m_HREADYOUT.read()==false)
				{
					ph= amba::DATA_RETRY;
					return tlm::TLM_UPDATED;
				}break;
			}
		  }
          current_data=NULL;
		}
		else
		{
			pending_end_data=true; //we have to copy the data in the memory.
			return tlm::TLM_ACCEPTED;
		}
	}
	else if(ph== amba::RESET_ASSERT){ //ACTIVE LOW
	        m_InReset = true;
                while (!m_curr_locks.empty()) m_curr_locks.pop();
                txn_counter = 0;
                trans_type = TRAN_IDLE;
                select_val = false;
                select_ev.cancel();
                split_ev.cancel();
                ready_ev.cancel();
                trans_ev.cancel();
                m_addr = 0;
                m_addr_ev.cancel();
                current_data = current_req = NULL;
                m_data_size = m_req_size = 0;
                data_start_index = req_start_index = -1;
                m_data_addr = data_counter = 0;
		m_otherSideReset.write(false);
	}
	else if(ph==amba::RESET_DEASSERT)
	{
	        m_InReset = false;
		m_otherSideReset.write(true);
	}
	return tlm::TLM_ACCEPTED;
}

/*
template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE>::end_req_method()
{
	if(m_HREADYOUT.read()==true)
	{
		tlm::tlm_phase ph ;
		sc_core::sc_time delay= sc_core::SC_ZERO_TIME;
		ph= tlm::END_REQ;
		tlm::tlm_sync_enum retval= slave_sock->nb_transport_bw(*current_req,ph, delay);
		assert(retval== tlm::TLM_ACCEPTED && "Invalid tlm_sync_enum returned");
		pending_end_req=false;
		req_count++;
	}
}


template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE>::end_data_method()
{

	unsigned char resp=m_HRESP.read(); //sample the response
	bool send_response=false;
	tlm::tlm_response_status response_status;
	tlm::tlm_phase ph;
	sc_core::sc_time delay=sc_core::SC_ZERO_TIME;

	unsigned int response= m_HRESP.read();
	switch(response)
	{
		case OK_RESP:{
			if(m_HREADYOUT.read()==true)
			{
				current_data->set_response_status(tlm::TLM_OK_RESPONSE);
				ph=amba::END_DATA;
				pending_end_data=false;
				send_response=true;
			}
			 break;
		}
		case ERROR_RESP:{
			if(m_HREADYOUT.read()==false)
			{
				current_data->set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
				ph=amba::END_DATA;
				pending_end_data=false;
				send_response=true;
			}
			break;
		}
		case SPLIT_RESP:{
			if(m_HREADYOUT.read()==false)
			{
				ph=amba::DATA_SPLIT;
				pending_end_data=false;
				send_response=true;
			}
			break;
		}
		case RETRY_RESP:{
			if(m_HREADYOUT.read()==false)
			{
				ph= amba::DATA_RETRY;
				pending_end_data=false;
				send_response=true;
			}break;
		}
    }
	if(send_response)
	{
		tlm::tlm_sync_enum retval= slave_sock->nb_transport_bw(*current_data,ph,delay);
		assert(retval== tlm::TLM_ACCEPTED && "Invalid tlm_sync_enum returned");
	}
}
*/

/*
 * @brief split_transaction
 *
 * @detail This method is triggered when the RTL slave wants to resume the
 * the transaction which it has splitted earlier.So now through the
 * HSPLIT signal, it informs the master-id for which it requires to resume
 * the operation.
 */

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::split_transaction()
{

	unsigned int id = m_HSPLIT.read();
	if(id==0)
		return;
	unsigned int temp= id;
	unsigned int i=0;
	for(i=0; i<16; i++)
	{
		temp = (id >>i);
		if(temp==1)
			break;
	}
	id=i;
	//compare it with the transaction buffered
	amba::amba_id * lastid;
	slave_sock.template get_extension<amba::amba_id>(lastid, *current_data);
	assert(lastid->value ==id && "the Master id doesn't belong to the last transfer's master id");
	//now we are requesting back to the arbiter to resume the transaction
	tlm::tlm_phase ph = amba::DATA_CONTINUE;
	sc_core::sc_time delay = sc_core::SC_ZERO_TIME;
	tlm::tlm_sync_enum retval= slave_sock->nb_transport_bw(*current_data,ph, delay);
	assert(retval==tlm::TLM_ACCEPTED && "Invalid tlm_sync_enum returned");
}

/*
template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH, MODULE>::begin_resp_method()
{
	AMBA_DUMP("nb_trans_fw "<<ph);
	bool send_response=false;
	tlm::tlm_response_status response_status;
	tlm::tlm_phase ph;
	sc_core::sc_time delay=sc_core::SC_ZERO_TIME;

	unsigned int response= m_HRESP.read();
	switch(response)
	{
		case OK_RESP:{
			if(m_HREADYOUT.read()==true)
			{
				current_data->set_response_status(tlm::TLM_OK_RESPONSE);
				ph=tlm::BEGIN_RESP;
				amba::burst_size* size;
				slave_sock.template get_extension<amba::burst_size>(size, *current_data);
				unsigned int data= m_HRDATA.read();
				unsigned char * data_ptr= current_data->get_data_ptr();
				unsigned char * rdata = new unsigned char[BUSWIDTH/8];
				memcpy(rdata,&data,(BUSWIDTH/8));
				if(size->value < (BUSWIDTH/8))
				{//narrow transfers?
					unsigned int index= data_counter%(BUSWIDTH/8);
					memcpy((data_ptr+data_counter),rdata+index, (BUSWIDTH/8));
				}
				else
					memcpy((data_ptr+data_counter),rdata,size->value);

				delete[] rdata;
				unsigned int len= current_data->get_data_length();
				data_counter+=size->value;
				if(len==data_counter)
					data_counter=0;//ok whole data has been copied
				pending_begin_resp=false;
				send_response=true;
			}
			 break;
		}
		case ERROR_RESP:{
			if(m_HREADYOUT.read()==false)
			{
				current_data->set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
				ph=tlm::BEGIN_RESP;
				pending_begin_resp=false;
				send_response=true;
			}
			break;
		}
		case SPLIT_RESP:{
			if(m_HREADYOUT.read()==false)
			{
				ph=amba::DATA_SPLIT;
				pending_begin_resp=false;
				send_response=true;
			}
			break;
		}
		case RETRY_RESP:{
			if(m_HREADYOUT.read()==false)
			{
				ph= amba::DATA_RETRY;
				pending_begin_resp=false;
				send_response=true;
			}break;
		}
	}
    if(send_response)
	{
    	tlm::tlm_sync_enum retval= slave_sock->nb_transport_bw(*current_data,ph,delay);
    	assert(retval==tlm::TLM_ACCEPTED && "Invalid tlm_sync_enum returned");
	}

}
*/

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::set_lock_method()
{
  m_HMASTLOCK.write(lock_value);
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::addr_method()
{
  AMBA_DUMP("Set Addr "<<m_addr);
  m_HADDR.write(m_addr);
}



template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_CT_RTL_Slave_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::ready_method()
{
  ready_check_time=sc_core::sc_time_stamp();
  ready_value=m_HREADYOUT.read() || force_ready;
  force_ready=false; //if we were forced to set HREADY we did it and can reset it
  write_ready_ev.notify();
  tlm::tlm_phase ph;
  sc_core::sc_time delay = sc_core::SC_ZERO_TIME;
  if (m_HREADYOUT.read()==false) {
    if (!current_data) return;
    if (m_HRESP.read()==1){
      AMBA_DUMP("ERROR RESP");
      current_data->set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
      goto E_DATA_BLOCK;
    }
    else
    if (m_HRESP.read()==2){ //retry
      AMBA_DUMP("RETRY RESP");
      current_data->set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
      ph=amba::DATA_RETRY;
      slave_sock->nb_transport_bw(*current_data, ph, delay);
    }
    else
    if (m_HRESP.read()==3){ //split
      AMBA_DUMP("SPLIT RESP");
      current_data->set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
      ph=amba::DATA_SPLIT;
      slave_sock->nb_transport_bw(*current_data, ph, delay);
    }
    return;
  }
  //E_REQ_BLOCK: //label
  {
    if (current_req && pending_end_req){
      pending_end_req=false;
      ph=tlm::END_REQ;
      AMBA_DUMP("END REQ");
      slave_sock->nb_transport_bw(*current_req, ph, delay);
      if (m_HTRANS.read()==0 && m_curr_locks.size()){ //we have an ongoing lock and we are answering an explicit IDLE
        if (m_curr_locks.front()->number_of_txns==current_lock_counter){
          //the IDLE is the unlocking TXN and since there is no data phase for IDLE we finish it off here
          m_curr_locks.front()->atomic_txn_completed();
          m_curr_locks.pop();
        }
      }
    }
  }
  if (!m_HRESP.read()==0) return;
  if (!current_data ) return;
  current_data->set_response_status(tlm::TLM_OK_RESPONSE);
  E_DATA_BLOCK:
  {
    if (m_curr_locks.size()){
      amba::amba_lock* data_lock;
      slave_sock.get_extension(data_lock, *current_data);
      if (m_curr_locks.front()->number_of_txns==current_lock_counter && current_req!=current_data && m_curr_locks.front()==data_lock->value){
        //the laste data phase of a txn, which is the last txn in a locked group
        m_curr_locks.front()->atomic_txn_completed();
        m_curr_locks.pop();
        if (m_curr_locks.size()) current_lock_counter=1; //if there is another lock in the Q we set the counter to 1.
        else current_lock_counter=0;
      }
    }
    if (current_data->is_write() && pending_end_data){ //make sure we got that BEGIN_DATA before sending END_DATA
      AMBA_DUMP("END DATA");
      ph=amba::END_DATA;
      slave_sock->nb_transport_bw(*current_data, ph, delay);
      current_data=NULL;
      pending_end_data=false;
    }
    else
    if (current_data->is_read()){
        ph= tlm::BEGIN_RESP;
		unsigned char * m_data= current_data->get_data_ptr();
        unsigned int  len =current_data->get_data_length();

        //TODO: make endianess save.
        unsigned int offset=(m_data_addr%((BUSWIDTH+7)/8));
        unsigned int array_entry=data_counter*m_data_size;
        if (data_start_index>0){
          array_entry+=data_start_index;
          array_entry%=len;
        }
        for (unsigned int i=0; i<m_data_size; i++){
          m_data[i+array_entry]=(unsigned char)(m_HRDATA.read().range((i+offset+1)*8-1,(i+offset)*8).to_uint());
        }
        AMBA_DUMP("BEGIN RESP");
		slave_sock->nb_transport_bw(*current_data,ph,delay);
        current_data=NULL;
    }
  }
}

#endif /*AHB_CT_RTL_Slave_Adapter_H_*/

