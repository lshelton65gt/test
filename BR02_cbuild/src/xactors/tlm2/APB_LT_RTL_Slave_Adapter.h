// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _APB_LT_RTL_SLAVE_ADAPTER_H_
#define _APB_LT_RTL_SLAVE_ADAPTER_H_

#include "xactors/tlm2/APB_CT_RTL_Slave_Adapter.h"
#include "adapters/AMBA_LT_CT_Adapter.h"
#include "greensocket/monitor/green_socket_monitor.h"

template< typename MODULE, unsigned int BUSWIDTH=64,unsigned int ADDRWIDTH=32>
class APB_LT_RTL_Slave_Adapter: public sc_core::sc_module
{
public:
  //TLM port through which we will accept the TLM packet
  amba::amba_slave_socket<BUSWIDTH> slave_sock;
	
  // RTL ports
  sc_core::sc_in_clk m_clk;
  sc_core::sc_in<bool> m_Reset;
  sc_core::sc_out<bool > m_psel;
  sc_core::sc_out<bool > m_penable;
  sc_core::sc_out<bool > m_pwrite;
  sc_core::sc_out<sc_dt::sc_uint<ADDRWIDTH> > m_paddr;
  sc_core::sc_out<sc_dt::sc_uint<BUSWIDTH> > m_pwdata;
  sc_core::sc_in<bool > m_pready;
  sc_core::sc_in<sc_dt::sc_uint<BUSWIDTH> > m_prdata;
  sc_core::sc_in<bool > m_pslverr;

  // Timing related APIs. Pass on to the CT RTL adapter
  void set_slave_timing(gs::socket::timing_info &timing)
  {
    apb_ct_rtl_adapter.set_slave_timing(timing);
  }
  void set_slave_timing(tlm::tlm_phase phase, const sc_core::sc_time& tm)
  {
    apb_ct_rtl_adapter.set_slave_timing(phase, tm);
  }
  void register_master_timing_listener(MODULE* mod,void (MODULE::*fn)(gs::socket::timing_info))
  {
    apb_ct_rtl_adapter.register_master_timing_listener(mod, fn);
  }

  // APIs for Carbon Debug. Pass on to the CT RTL adapter
  void registerDebugAccessCB(CarbonDebugAccessIF* dbgIf)
  {
    apb_ct_rtl_adapter.registerDebugAccessCB(dbgIf);
  }

  //! API to get and activate the monitor on the LT socket
  gs::socket::monitor<BUSWIDTH>* getMonitor(void)
  {
    lt_monitor->activated = true;
    return lt_monitor;
  }

  //! Constructor
  SC_HAS_PROCESS(APB_LT_RTL_Slave_Adapter);
  APB_LT_RTL_Slave_Adapter(sc_core::sc_module_name nm) :
    sc_core::sc_module(nm),
    slave_sock("apb_slave_socket", amba::amba_APB, amba::amba_LT, false),
    apb_ct_rtl_adapter("apb_ct_rtl_adapter"),
    lt_ct_adapter("apb_lt_ct_adapter", amba::amba_APB)
  {
    // Connect the RTL pins to the CT_RTL adapter
    apb_ct_rtl_adapter.m_clk(m_clk);
    apb_ct_rtl_adapter.m_Reset(m_Reset);
    apb_ct_rtl_adapter.m_psel(m_psel);
    apb_ct_rtl_adapter.m_penable(m_penable);
    apb_ct_rtl_adapter.m_pwrite(m_pwrite);
    apb_ct_rtl_adapter.m_paddr(m_paddr);
    apb_ct_rtl_adapter.m_pwdata(m_pwdata);
    apb_ct_rtl_adapter.m_pready(m_pready);
    apb_ct_rtl_adapter.m_prdata(m_prdata);
    apb_ct_rtl_adapter.m_pslverr(m_pslverr);

    // Connect the clock to the AMBA LT CT adapter
    lt_ct_adapter.clk(m_clk);

    // Connect the apb CT RTL socket to the lt_ct_adapter socket
    lt_ct_adapter.master_sock(apb_ct_rtl_adapter.slave_sock);

    // Bind our socket to the LT CT Adapter. We create a monitor but de-activate it by default
    lt_monitor = gs::socket::connect_with_monitor<BUSWIDTH, tlm::tlm_base_protocol_types>(slave_sock, lt_ct_adapter.slave_sock);
    lt_monitor->activated = false;
  }

private:
  //! The CT_RTL adapter
  APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH> apb_ct_rtl_adapter;

  //! The LT CT adapter
  amba::AMBA_LT_CT_Adapter<BUSWIDTH> lt_ct_adapter;

  //! The monitor for activity detection
  gs::socket::monitor<BUSWIDTH>* lt_monitor;
}; // class APB_LT_RTL_Slave_Adapter: public sc_core::sc_module

#endif // _APB_LT_RTL_SLAVE_ADAPTER_H_
