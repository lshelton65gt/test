// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef AHB_MASTER_RTL_CT_ADAPTER_
#define AHB_MASTER_RTL_CT_ADAPTER_

#define OK_RESP 0
#define ERROR_RESP 1
#define RETRY_RESP 2
#define SPLIT_RESP 3


template<unsigned int BUSWIDTH, typename MODULE , bool FORCE_BIGUINT=false, bool IS_LITE=false>
class AHB_Master_RTL_CT_Adapter:public sc_core::sc_module
#ifdef CARBON_DEBUG_ACCESS
, public CarbonDebugAccessIF
#endif
{
public:

	//Socket to connect with AHBCT slave/bus
	amba::amba_master_socket<BUSWIDTH> master_sock;

	sc_core::sc_in_clk m_clk;
        sc_core::sc_signal<bool> m_otherSideReset;
	sc_core::sc_in<bool> m_Reset;
	///incoming signals from AHB RTL Master 
	typename amba::port_enabler<!IS_LITE, bool>::input_port_type m_HBUSREQ;
	sc_core::sc_in<bool> m_HLOCK;
	sc_core::sc_in<sc_dt::sc_uint<2> > m_HTRANS;
	sc_core::sc_in<sc_dt::sc_uint<32> >m_HADDR;
	sc_core::sc_in<sc_dt::sc_uint<3> > m_HSIZE;
	sc_core::sc_in<sc_dt::sc_uint<3> > m_HBURST;
	sc_core::sc_in<sc_dt::sc_uint<4> > m_HPROT;
	sc_core::sc_in<bool> m_HWRITE;
	sc_core::sc_in<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type > m_HWDATA;
	
	///Outgoing signal to AHB RTL Master
	sc_core::sc_out<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type > m_HRDATA;
	sc_core::sc_out<typename amba::port_enabler<!IS_LITE>::resp_type > m_HRESP;
	typename amba::port_enabler<!IS_LITE, bool>::output_port_type m_HGRANT;
	sc_core::sc_out<bool> m_HREADYIN;
	
	

	//Processes sensitive on clock/signals
	void BusRequest();
	void RequestProcess();
	void DataProcess();
	void clock_tick();
	void BusGrant();
	void ResetProcess();
	void ReadyProcess();
	void WriteReady();
	
	//This function should be called to set the non-default timings for the mastersocket
	void set_master_timing(gs::socket::timing_info &);
    void set_master_timing(tlm::tlm_phase, const sc_core::sc_time&);
	gs::socket::timing_info & get_master_timing();
	sc_core::sc_time& get_master_timing(tlm::tlm_phase);
	inline void register_slave_timing_listener(MODULE* mod, void (MODULE::*fn)(gs::socket::timing_info))
	{
		owner=mod;
        timing_cb=fn;
	}
	
	
#ifdef CARBON_DEBUG_ACCESS
	 //for Carbon debug access
	 CarbonDebugAccessStatus debugMemRead(uint64_t addr, uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0);
	 CarbonDebugAccessStatus debugMemWrite (uint64_t addr, const uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0);
#endif

	tlm::tlm_sync_enum nb_transport_bw(tlm::tlm_generic_payload & trans, tlm::tlm_phase &ph, sc_core::sc_time &delay);
	void slave_timing_listener(gs::socket::timing_info);
	void end_of_elaboration();
	
	SC_HAS_PROCESS(AHB_Master_RTL_CT_Adapter);
	AHB_Master_RTL_CT_Adapter(sc_core::sc_module_name nm, unsigned int max_mem):sc_core::sc_module(nm),
	master_sock("AHB_Master_RTL_CT_socket",IS_LITE? amba::amba_AHBLite : amba::amba_AHB, amba::amba_CT,false,gs::socket::GS_TXN_WITH_DATA),
	m_clk("m_clk"),
	m_Reset("AHB_RESET"),
	m_HBUSREQ("AHB_HBUSREQ"),
	m_HLOCK("AHB_HLOCK"),
	m_HTRANS("AHB_HTRANS"),
	m_HADDR("AHB_HADDR"),
	m_HSIZE("AHB_HSIZE"),
	m_HBURST("AHB_HBURST"),
	m_HPROT("AHB_HPROT"),
	m_HWRITE("AHB_HWRITE"),
	m_HWDATA("AHB_HWDATA"),
	m_HRDATA("AHB_HRDATA"),
	m_HRESP("AHB_HRESP"),
	m_HGRANT("AHB_HGRANT"),
	m_HREADYIN("AHB_HREADYIN"),
	current_req(NULL),
	current_data(NULL),
	reset_txn(NULL),
    busreq_txn(NULL),
	time_unit(sc_core::sc_get_time_resolution()),
	pending_end_req(false),
	bus_grant_pending(false),
    old_bus_req(false),
    owner_of_bus(false),
	current_req_count(0),
    current_data_count(0),
    resp_value(0),
	current_lock(NULL),
	locked_txn_counter(0),
	max_mem_size(max_mem),
    timing_cb(NULL),
	bus_granted(false),
    ready_value(false),
    m_InReset(false),
    owner(NULL),
	breq_sample_time(sc_core::sc_get_time_resolution()),
	busreq_sample_time(sc_core::sc_get_time_resolution()),
	bdata_sample_time(sc_core::sc_get_time_resolution()),
    m_req_wrap_index(-1), 
    m_data_wrap_index(-1),
    m_req_size(0), 
    m_data_size(0),
    m_data_addr(0)
    
	{

        if (IS_LITE){ //in case of AHBLite Grant and Busreq are signals, which we tie to values such that our adapter will work.
          amba::port_enabler<!IS_LITE, bool>::initialize(m_HGRANT, true);
          amba::port_enabler<!IS_LITE, bool>::initialize(m_HBUSREQ, false);
        }

		SC_METHOD(clock_tick);
		sensitive<<m_clk.pos();
		dont_initialize();


		SC_METHOD(BusRequest);
		sensitive<< bus_req_ev;
		dont_initialize();
		
		SC_METHOD(BusGrant);
		sensitive<< bus_grant_ev;
		dont_initialize();

		SC_METHOD(RequestProcess);
		sensitive<< begin_req_ev;
		dont_initialize();
		
		SC_METHOD(DataProcess);
		sensitive<< begin_data_ev;
		dont_initialize();
		
		SC_METHOD(ResetProcess);
		sensitive<< m_Reset;
		//dont_initialize();
		
		SC_METHOD(WriteReady);
		sensitive << write_ready_ev;
		dont_initialize();

		master_sock.template register_nb_transport_bw< AHB_Master_RTL_CT_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE> >(this, & AHB_Master_RTL_CT_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::nb_transport_bw);
        master_sock.set_timing_listener_callback(this, & AHB_Master_RTL_CT_Adapter::slave_timing_listener);
	 }
	 struct ahb_lock_object: public amba::lock_object_base
	 {
	 	private:
			 friend class gs::socket::extension_pool<ahb_lock_object>;
	   void atomic_txn_completed()
	   {
			get_pool()->recycle(this);
	   }

	   ahb_lock_object()
	   {
			amba::lock_object_base::number_of_txns=~0x00;
	   }

	 
	 public:
		 static gs::socket::extension_pool<ahb_lock_object>* get_pool()
		 {
		       static gs::socket::extension_pool<ahb_lock_object> s_pool(5);
		       return &s_pool;
	     }
	 };
	 ~AHB_Master_RTL_CT_Adapter();
     
     void start_of_simulation();
private:
	tlm::tlm_generic_payload *current_req;
	tlm::tlm_generic_payload * current_data;
	tlm::tlm_generic_payload * reset_txn, *busreq_txn;
	sc_core::sc_event bus_grant_ev;
	sc_core::sc_event bus_req_ev;
	sc_core::sc_event begin_req_ev;
	sc_core::sc_event begin_data_ev;
	sc_core::sc_time time_unit;
	bool pending_end_req;
	bool bus_grant_pending;
    bool old_bus_req;
    bool owner_of_bus;
	unsigned int current_req_count, current_data_count, resp_value;
	ahb_lock_object * current_lock;
	unsigned int locked_txn_counter;
	unsigned int max_mem_size;
	gs::socket::timing_info master_timing_info;
	void (MODULE::*timing_cb)(gs::socket::timing_info);
	bool bus_granted;
	bool ready_value;
	bool m_InReset;
	sc_core::sc_event write_ready_ev;
	sc_core::sc_event ready_ev;
	MODULE * owner;
	sc_core::sc_time breq_sample_time;
	sc_core::sc_time busreq_sample_time;
	sc_core::sc_time bdata_sample_time;
    sc_core::sc_time ready_set_time;
    signed int   m_req_wrap_index, m_data_wrap_index;
    unsigned int m_req_size, m_data_size;
    typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type  m_tmp_dt;
	sc_core::sc_time busreq_sample_helper, m_last_data_time, m_last_tick_time;
    sc_dt::uint64 m_data_addr;
};


template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE,FORCE_BIGUINT, IS_LITE>::~AHB_Master_RTL_CT_Adapter()
{
  AMBA_DUMP("release reset txn ");if (reset_txn) reset_txn->release();
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE,FORCE_BIGUINT, IS_LITE>::end_of_elaboration()
{
	master_sock.activate_synchronization_protection();
     sc_core::sc_time delay= sc_core::sc_get_time_resolution();
     if (master_timing_info.get_start_time(tlm::BEGIN_REQ)<delay) master_timing_info.set_start_time(tlm::BEGIN_REQ,delay);
     if (master_timing_info.get_start_time(tlm::END_REQ)<2*delay) master_timing_info.set_start_time(tlm::END_REQ,2*delay);
     if (master_timing_info.get_start_time(amba::BEGIN_DATA)<delay) master_timing_info.set_start_time(amba::BEGIN_DATA,delay);
     if (master_timing_info.get_start_time(amba::END_DATA)<2*delay) master_timing_info.set_start_time(amba::END_DATA,2*delay);
     if (master_timing_info.get_start_time(tlm::BEGIN_RESP)<2*delay) master_timing_info.set_start_time(tlm::BEGIN_RESP,2*delay);
     if (master_timing_info.get_start_time(amba::BUS_GRANT)<2*delay) master_timing_info.set_start_time(amba::BUS_GRANT,2*delay);
     if (master_timing_info.get_start_time(amba::BUS_UNGRANT)<2*delay) master_timing_info.set_start_time(amba::BUS_UNGRANT,2*delay);
     if (master_timing_info.get_start_time(amba::BUS_REQ)<delay) master_timing_info.set_start_time(amba::BUS_REQ,delay);
     if (master_timing_info.get_start_time(amba::BUS_REQ_STOP)<delay) master_timing_info.set_start_time(amba::BUS_REQ_STOP,delay);
      master_sock.set_initiator_timing(master_timing_info);
      busreq_sample_time=master_timing_info.get_start_time(amba::BUS_REQ);
      breq_sample_time=master_timing_info.get_start_time(tlm::BEGIN_REQ);
      bdata_sample_time= master_timing_info.get_start_time(amba::BEGIN_DATA);
      
      //ready_set_time is the max of all times but UNGRANT. the timing distribution code in this class
      // ensures that END_REQ/END_DATA/BEGIN_RESP/BUS_GRANT are always equally set to this time, so we
      // can initialize it here to END_REQ (if there were already some changes that is the correct time)
      ready_set_time=master_timing_info.get_start_time(amba::END_DATA);
     if(timing_cb!=NULL) (owner->*timing_cb)(master_timing_info);
}


template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE,FORCE_BIGUINT, IS_LITE>::start_of_simulation()
{
}


#ifdef CARBON_DEBUG_ACCESS
template< unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE >
CarbonDebugAccessStatus  AHB_Master_RTL_CT_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::debugMemRead(uint64_t addr, uint8_t* buf, uint32_t numBytes, uint32_t* ctrl)
{
	AMBA_DUMP("Debug Read Access, bytes to read"<< numBytes <<", address="<< addr);
	tlm::tlm_generic_payload * payload = master_sock.get_transaction();
    payload->set_command(tlm::TLM_READ_COMMAND);
    payload->set_address(addr);
    master_sock.reserve_data_size(*payload,numBytes);
    if((ctrl[0]&0x01)==0x1)
    {
    	AMBA_DUMP("Secure Access");
    	master_sock.template validate_extension<amba::amba_secure_access>(*payload);

    }

    unsigned int transfered_bytes=master_sock->transport_dbg(*payload);
    AMBA_DUMP("Actual bytes read"<<transfered_bytes);
    unsigned char * data_ptr = payload->get_data_ptr();
    memcpy(buf,data_ptr,transfered_bytes);
    if(transfered_bytes < numBytes)
    	return eCarbonDebugAccessStatusPartial;
    else
    {
    	tlm::tlm_response_status resp= payload->get_response_status();
    	switch(resp)
    	{
			case tlm::TLM_ADDRESS_ERROR_RESPONSE:
			{
				return eCarbonDebugAccessStatusOutRange;
				break;
			}
			case tlm::TLM_OK_RESPONSE:{
				return eCarbonDebugAccessStatusOk;
				break;
			}
			default:
			{
				return eCarbonDebugAccessStatusError ;
				break;
			}
    	}
    }
}


template< unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE >
CarbonDebugAccessStatus AHB_Master_RTL_CT_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE>::debugMemWrite(uint64_t addr, const uint8_t* buf, uint32_t numBytes, uint32_t* ctrl)
{

	AMBA_DUMP("Debug Write Access, bytes to write"<< numBytes<<", address="<< addr);
	tlm::tlm_generic_payload * payload = master_sock.get_transaction();
	payload->set_command(tlm::TLM_WRITE_COMMAND);
	payload->set_address(addr);
	master_sock.reserve_data_size(*payload,numBytes);
	if((ctrl[0]&0x01)==0x1)
	{
		AMBA_DUMP("Secure Access");
		master_sock.template validate_extension<amba::amba_secure_access>(*payload);
	}
	unsigned char * data_ptr = payload->get_data_ptr();
    memcpy(data_ptr,buf,numBytes);
	unsigned int transfered_bytes=master_sock->transport_dbg(*payload);
	AMBA_DUMP("Actual bytes written"<<transfered_bytes);

	if(transfered_bytes < numBytes)
		return eCarbonDebugAccessStatusPartial;
	else
	{
		tlm::tlm_response_status resp= payload->get_response_status();
		switch(resp)
		{
			case tlm::TLM_ADDRESS_ERROR_RESPONSE:
			{
				return eCarbonDebugAccessStatusOutRange;
				break;
			}
			case tlm::TLM_OK_RESPONSE:{
				return eCarbonDebugAccessStatusOk;
				break;
			}
			default:
			{
				return eCarbonDebugAccessStatusError ;
				break;
			}
		}
	}
}
#endif

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE,FORCE_BIGUINT, IS_LITE>::set_master_timing(tlm::tlm_phase ph, const sc_core::sc_time& delay)
{
  //std::cout<<this->name()<<"Got start time "<<delay<< " for "<<ph<<std::endl;
  bool changed =false;
  if (ph==tlm::BEGIN_REQ || ph==amba::BEGIN_DATA){
    if((delay+time_unit) > master_timing_info.get_start_time(ph))
    {
        master_timing_info.set_start_time(ph,delay+time_unit);
        changed =true;
    }
  }
  else
  if (ph==amba::BUS_REQ || ph==amba::BUS_REQ_STOP)
  {
    if((delay+time_unit) > master_timing_info.get_start_time(amba::BUS_REQ))
    {
        master_timing_info.set_start_time(amba::BUS_REQ,delay+time_unit);
        changed =true;
    }    
    if((delay+time_unit) > master_timing_info.get_start_time(amba::BUS_REQ_STOP))
    {
        master_timing_info.set_start_time(amba::BUS_REQ_STOP,delay+time_unit);
        changed =true;
    }    
  }
  
  if(changed==true)
  {
    //std::cout<<this->name()<<"Changed my timing. Fwd to connected slave "<<std::endl;
    master_sock.set_initiator_timing(master_timing_info);
    busreq_sample_time=master_timing_info.get_start_time(amba::BUS_REQ);
    breq_sample_time=master_timing_info.get_start_time(tlm::BEGIN_REQ);
    bdata_sample_time= master_timing_info.get_start_time(amba::BEGIN_DATA);
  }
  changed=false;
#define MY_MAX(a,b) ((a>b)?a:b)  
  sc_core::sc_time 
  new_ready_set=MY_MAX(breq_sample_time,//+time_unit, 
                       // MY_MAX(busreq_sample_time+time_unit, 
                               MY_MAX(bdata_sample_time,//+time_unit, 
                                      MY_MAX(master_timing_info.get_start_time(tlm::BEGIN_RESP),
                                             MY_MAX(master_timing_info.get_start_time(amba::END_DATA),
                                                    MY_MAX(master_timing_info.get_start_time(tlm::END_REQ),
                                                           master_timing_info.get_start_time(amba::BUS_GRANT)
                                                          )
                                                    )
                                             )
                                      )
                                //)
                        );
  if (new_ready_set>ready_set_time){
    //std::cout<<this->name()<<"Changed my resp timing. Bwd to connected master (RTL) "<<new_ready_set<<std::endl;
    ready_set_time=new_ready_set;
    master_timing_info.set_start_time(tlm::END_REQ, ready_set_time);
    master_timing_info.set_start_time(tlm::BEGIN_RESP, ready_set_time);
    master_timing_info.set_start_time(amba::BUS_GRANT, ready_set_time);
    master_timing_info.set_start_time(amba::END_DATA, ready_set_time); 
    changed=true;           
  }
#undef MY_MAX
  if(ready_set_time > master_timing_info.get_start_time(amba::BUS_UNGRANT) )
  {
      master_timing_info.set_start_time(amba::BUS_UNGRANT,ready_set_time);
      changed=true;
  }
  if(changed && timing_cb!=NULL) (owner->*timing_cb)(master_timing_info);  
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE,FORCE_BIGUINT, IS_LITE>::set_master_timing(gs::socket::timing_info& info)
{
          bool changed =false;
	      if((info.get_start_time(tlm::BEGIN_REQ)+time_unit) > master_timing_info.get_start_time(tlm::BEGIN_REQ))
	      {
	    	  master_timing_info.set_start_time(tlm::BEGIN_REQ,info.get_start_time(tlm::BEGIN_REQ)+time_unit);
	    	  changed =true;
	      }
	      if((info.get_start_time(amba::BEGIN_DATA)+time_unit) > master_timing_info.get_start_time(amba::BEGIN_DATA))
	      {
	    	  master_timing_info.set_start_time(amba::BEGIN_DATA,info.get_start_time(amba::BEGIN_DATA)+time_unit);
	    	  changed =true;
	      }
          
          //make sure we use the max of bdata and blastdata
          sc_core::sc_time busreq_time=info.get_start_time(amba::BUS_REQ);
          if (busreq_time<info.get_start_time(amba::BUS_REQ_STOP)) busreq_time=info.get_start_time(amba::BUS_REQ_STOP);
          
	      if((busreq_time+time_unit)  > master_timing_info.get_start_time(amba::BUS_REQ))
	      {
	    	  master_timing_info.set_start_time(amba::BUS_REQ,busreq_time+time_unit);
	    	  changed =true;
	      }
	      if((busreq_time+time_unit) > master_timing_info.get_start_time(amba::BUS_REQ_STOP))
	      {
	    	  master_timing_info.set_start_time(amba::BUS_REQ_STOP,busreq_time+time_unit);
	    	  changed =true;
	      }
           if(changed==true)
           {  master_sock.set_initiator_timing(master_timing_info);
	    	  breq_sample_time=master_timing_info.get_start_time(tlm::BEGIN_REQ);
	    	  bdata_sample_time= master_timing_info.get_start_time(amba::BEGIN_DATA);
	    	  busreq_sample_time=master_timing_info.get_start_time(amba::BUS_REQ);
           }
          changed=false;
#define MY_MAX(a,b) ((a>b)?a:b)
          sc_core::sc_time 
          new_ready_set=MY_MAX(breq_sample_time,//+time_unit, 
                           //     MY_MAX(busreq_sample_time+time_unit, 
                                       MY_MAX(bdata_sample_time,//+time_unit, 
                                              MY_MAX(master_timing_info.get_start_time(tlm::BEGIN_RESP),
                                                     MY_MAX(master_timing_info.get_start_time(amba::END_DATA),
                                                            MY_MAX(master_timing_info.get_start_time(tlm::END_REQ),
                                                                   master_timing_info.get_start_time(amba::BUS_GRANT)
                                                                  )
                                                            )
                                                     )
                                              )
                               //         )
                                );
          if (new_ready_set>ready_set_time){
            ready_set_time=new_ready_set;
            master_timing_info.set_start_time(tlm::END_REQ, ready_set_time);
            master_timing_info.set_start_time(tlm::BEGIN_RESP, ready_set_time);
            master_timing_info.set_start_time(amba::BUS_GRANT, ready_set_time);
            master_timing_info.set_start_time(amba::END_DATA, ready_set_time); 
            changed=true;           
          }
#undef MY_MAX
          
          if(ready_set_time > master_timing_info.get_start_time(amba::BUS_UNGRANT) )
          {
              master_timing_info.set_start_time(amba::BUS_UNGRANT,ready_set_time);
              changed=true;
          }          
	      if(changed && timing_cb!=NULL) (owner->*timing_cb)(master_timing_info);

}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
gs::socket::timing_info& AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE,FORCE_BIGUINT, IS_LITE>::get_master_timing()
{
	return master_timing_info;
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
sc_core::sc_time& AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::get_master_timing(tlm::tlm_phase ph)
{
	return (master_timing_info.get_start_time(ph));
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE,FORCE_BIGUINT, IS_LITE>::slave_timing_listener(gs::socket::timing_info info)
{
	AMBA_DUMP("Slave timing listener called");
	bool do_cb=false;
    //we need 2 time units because our synch prot adds one, and then we wanna
    // "sample" the value safely hence need to wait another one
	if((info.get_start_time(tlm::END_REQ)+2*time_unit) > ready_set_time)
	{
        ready_set_time=info.get_start_time(tlm::END_REQ)+2*time_unit;
		do_cb=true;
	}
	if((info.get_start_time(tlm::BEGIN_RESP)+2*time_unit) > ready_set_time)
	{
        ready_set_time=info.get_start_time(tlm::BEGIN_RESP)+2*time_unit;
		do_cb=true;
	}
	if((info.get_start_time(amba::BUS_GRANT)+2*time_unit) > ready_set_time)
	{
        ready_set_time=info.get_start_time(amba::BUS_GRANT)+2*time_unit;
	    do_cb=true;
	}
	if((info.get_start_time(amba::END_DATA)+2*time_unit) > ready_set_time)
	{
        ready_set_time=info.get_start_time(amba::END_DATA)+2*time_unit;
	    do_cb=true;
	}
#define MY_MAX(a,b) ((a>b)?a:b)  
    if (do_cb){
      master_timing_info.set_start_time(tlm::END_REQ, ready_set_time);
      master_timing_info.set_start_time(tlm::BEGIN_RESP, ready_set_time);
      master_timing_info.set_start_time(amba::BUS_GRANT, ready_set_time);
      master_timing_info.set_start_time(amba::END_DATA, ready_set_time);
    }
#undef MY_MAX
	if((info.get_start_time(amba::BUS_UNGRANT)+time_unit) > master_timing_info.get_start_time(amba::BUS_UNGRANT))
	{
		sc_core::sc_time delay= info.get_start_time(amba::BUS_UNGRANT);
	    master_timing_info.set_start_time(amba::BUS_UNGRANT,(delay+time_unit));
	    do_cb=true;
	}
    
	if(timing_cb!=NULL && do_cb)
		(owner->*timing_cb)(master_timing_info);
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE,FORCE_BIGUINT, IS_LITE>::clock_tick()
{
    if (m_InReset) return;
    m_last_tick_time=sc_core::sc_time_stamp(); 
    
    ready_value=false; //by default we clear
    resp_value=0; //and set okay response
    write_ready_ev.notify(ready_set_time);

    
    //if granted and ready check for new req
    // we also need to check when we have IDLE or BUSY, because they can change to
    //  NONSEQ or SEQ even without HREADY
    if (m_HGRANT.read() && (m_HREADYIN.read() || m_HTRANS.read()<2))  begin_req_ev.notify(breq_sample_time); 

    if (m_HREADYIN.read() && (m_HTRANS.read()>1)){ //SEQ or NONSEQ accepted
      if (current_req->is_write()) begin_data_ev.notify(bdata_sample_time);    
      current_data=current_req; //NONSEQ or SEQ is accepted so we have pending data
      m_data_size=m_req_size;
      m_data_wrap_index=m_req_wrap_index;
      current_data_count=((m_HTRANS.read()==2)?0:(current_data_count+1));
      m_data_addr=m_HADDR.read();
    }

    if (!m_HGRANT.read() && m_HREADYIN.read()) {
      current_req_count=0; //when the bus is not granted the reqs are done
    }

    if (current_req && m_HREADYIN.read()){ //accepted a REQ
      if (current_req_count==0){    //last of burst
        if (m_HTRANS.read()<2){ //so the thing ended with an IDLE or BUSY (e.g. when grant was taken away during busy)
          current_req->release(); //so we release here as there is no data phase for IDLE or BUSY at CT
          AMBA_DUMP("TERMINATE TXN WITH AN IDLE XFER (release)");
        }
        current_req=NULL; //done with the reqs
      }
    }

    if (busreq_txn && m_HREADYIN.read() && m_HGRANT.read()){
      //current_req=busreq_txn;
      busreq_txn->release();
      busreq_txn=NULL;
    }

    if (!IS_LITE){
      old_bus_req=m_HBUSREQ;
      bus_req_ev.notify(busreq_sample_time); //check for bus_req //TODO: maybe sent a bus req with a lock change as well??
    }
        

    if (m_HRESP.read()!=0 && !m_HREADYIN.read()){ //first cycle of two cycle response is done
      ready_value=true;
      resp_value=m_HRESP.read(); //we need to keep the HRESP steady for the second cycle
    }
    else
    if(m_HRESP.read()!=0 && m_HREADYIN.read()==true) //end of two cycle resp. we need to make sure response is OK again
    {
      resp_value = 0;
    }

    //usually we do not accept idle or busy
    if (m_HREADYIN.read() && (m_HTRANS.read()<2) && owner_of_bus){ //accepted BUSY or IDLE while we are owner of bus
      ready_value=true; //then we will keep HREADY high for one more cycle (forced zero wait state response)
    }
    
    if (m_HREADYIN.read() && m_HGRANT.read()) owner_of_bus=true; //now we own the address bus
    else
    //So when do we loose the address bus? Normally when we have HREADY and !HGRANT
    // but according to http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.faqs/ka3451.html
    // the IDLE xfer that follows a transfer (if there is an IDLE at all) belongs to the burst
    // and according to my anaylsis of the ARM926EJS a master relies on seeing the zero wait state HREADY
    // in the data cylce of this IDLE. So we artificially extend the ownership of the bus to this IDLE
    // in other words. We loose ownership of the bus, only when we have HREADY no GRANT and an IDLE xfer
    if (m_HREADYIN.read() && !m_HGRANT.read() && m_HTRANS.read()==0) owner_of_bus=false; 
    
}


template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE,FORCE_BIGUINT, IS_LITE>::BusRequest()
{
  if (m_HBUSREQ.read() && !old_bus_req){
    AMBA_DUMP("RTL_CT::BusRequest");
    if(busreq_txn==NULL) {
      AMBA_DUMP("busreq_txn was null, getting trnx");
      busreq_txn= master_sock.get_transaction();
    }
    //Received Bus request from RTL Master, Generating CT transaction
    tlm::tlm_phase ph= amba::BUS_REQ;
    sc_core::sc_time delay= sc_core::SC_ZERO_TIME;
    amba::amba_lock *lock_ext;
    if(m_HLOCK.read()==true)
    {///Master has requested for the locked bus access
      if (!current_lock){
        current_lock=ahb_lock_object::get_pool()->create();
        current_lock->lock_is_understood_by_slave=false;
        current_lock->number_of_txns= ~(0x0);
        locked_txn_counter=0;
        master_sock.template get_extension<amba::amba_lock>(lock_ext, *busreq_txn);
        //set the value of locked extension
        lock_ext->value = current_lock;
        master_sock.template validate_extension<amba::amba_lock>(*busreq_txn);
        AMBA_DUMP("Enabled the lock extension in trnx (first of locked group)");
      }
      else{
        master_sock.template get_extension<amba::amba_lock>(lock_ext, *busreq_txn);
        //set the value of locked extension
        lock_ext->value = current_lock;
        master_sock.template validate_extension<amba::amba_lock>(*busreq_txn);
        AMBA_DUMP("Enabled the lock extension in trnx (continue locked group)");      
      }
    }


    tlm::tlm_sync_enum retval= master_sock->nb_transport_fw(*busreq_txn, ph, delay);
    AMBA_DUMP("Sent BUS_REQ");
    switch(retval)
    {
         case tlm::TLM_UPDATED:
         {
                if( ph == amba::BUS_GRANT)
                {
                  AMBA_DUMP("GRANTED BUS_REQ");
                  if (!m_HGRANT.read()){
                    std::cerr<<"Error: In "<<this->name()<<" @ "<<sc_core::sc_time_stamp()<<" Got a combinatorial HGRANT change!"<<std::endl;
                    abort();
                  }
                    //Received phase: BUS_GRANT, tlm_sync_enum:TLM_UPDATED
                    bus_granted=true;
                    ready_value=true;
                }
                bus_grant_ev.notify();
                break;
          }
          case tlm::TLM_ACCEPTED:
          {
            AMBA_DUMP("Return value was TLM_ACCEPTED.....BUS_REQ still to be responded");
            assert(ph== amba::BUS_REQ);
            break;
          }
          default:
              assert("Wrong tlm_sync_enum returned");
      }  
  }
  else
  if (!m_HBUSREQ.read() && old_bus_req){
    if (!(busreq_txn||current_req||current_data)){
      AMBA_DUMP("No current txn, using a new one for signalling bus request stop.");
      busreq_txn=master_sock.get_transaction();
    }
    tlm::tlm_phase ph= amba::BUS_REQ_STOP;
    sc_core::sc_time delay= sc_core::SC_ZERO_TIME;
    if (busreq_txn){
      master_sock->nb_transport_fw(*busreq_txn, ph, delay);
      amba::amba_lock* lock_ext;
      if (master_sock.get_extension(lock_ext, *busreq_txn) && locked_txn_counter==0){ //this was the first of a locked group so we discard it
        lock_ext->value->atomic_txn_completed();
      }
      busreq_txn->release();
      busreq_txn=NULL;
    }
    else
    if (current_req)
      master_sock->nb_transport_fw(*current_req, ph, delay);
    else
      master_sock->nb_transport_fw(*current_data, ph, delay);
  }

  if (!m_HBUSREQ.read() || bus_grant_pending) return;
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE,FORCE_BIGUINT, IS_LITE>::RequestProcess()
{
  //AMBA_DUMP("RTL_CT::RequestProcess");
  unsigned int transfer_type= (unsigned int) m_HTRANS.read();

  //BUSY or an IDLE that does NOT terminate a burst or a lock
  if ((transfer_type==1) || (transfer_type==0 && current_req_count==0 && current_lock==NULL)){ 
    //if (!current_data && ) ready_value=true; //no outstanding data phase so we can accept the busy
    return;
  }

  if(current_req==NULL) {
    assert(transfer_type==2); //should only happen when we are starting a new burst
    AMBA_DUMP("current_req was null....getting trnx from sckt");
    current_req = master_sock.get_transaction();
    current_req->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);
  }
  else if(current_req_count!=0 && transfer_type==2)
  {//If we still have data phases for previous request, and then we have another request here
    AMBA_DUMP("current_req was NOT null.... early end of ongoing burst. data phase for prev req pending");
    if (m_last_data_time>m_last_tick_time) current_req->release(); //means we already have seen a data transfer in this cycle, so we can release
    current_req=master_sock.get_transaction();
  }
  

  if (transfer_type==2) { //we only set up all the members at NONSEQ as they are all x2x
    unsigned int addr= (unsigned int ) m_HADDR.read();
    unsigned int size = (unsigned int) m_HSIZE.read();
    unsigned int burst= (unsigned int) m_HBURST.read();
    unsigned int protection =(unsigned int) m_HPROT.read();
    bool fwrite= m_HWRITE.read();
      
    amba::amba_lock * lock_ext;
    if(m_HLOCK.read()==true)
    {
        AMBA_DUMP("m_HLOCK is TRUE. Validated lock extension in trnx");
        if (!current_lock){
          current_lock=ahb_lock_object::get_pool()->create();
          current_lock->lock_is_understood_by_slave=false;
          current_lock->number_of_txns= ~(0x0);
          locked_txn_counter=0;
        }
        locked_txn_counter++;      
        master_sock.template get_extension<amba::amba_lock>(lock_ext, *current_req);
        lock_ext->value = current_lock;
        master_sock.template validate_extension<amba::amba_lock>(*current_req);
    }
    else
    {
        AMBA_DUMP("m_HLOCK is FALSE");
        if(current_lock)
        {//Ok this is the last unlocking txn then
          locked_txn_counter++;
          master_sock.template get_extension<amba::amba_lock>(lock_ext, *current_req);
          current_lock->number_of_txns= locked_txn_counter;
          lock_ext->value=current_lock;
          master_sock.template validate_extension<amba::amba_lock>(*current_req);
          current_lock=NULL;
          AMBA_DUMP("setting locked_txn_counter to 0");
        }
    }
      
      
    amba::amba_burst_size* m_burst_size;
    //set the transfer_size in bytes
    master_sock.template get_extension<amba::amba_burst_size>(m_burst_size, *current_req);
    
    ///set the address
    current_req->set_address(addr); AMBA_DUMP("setting addr "<<addr);
    
    AMBA_DUMP("transfer_type==2");
    unsigned int byteLen=8;
    switch(size)
    {
    case 0:{m_burst_size->value=(8/byteLen);break;}
    case 1:{m_burst_size->value=(16/byteLen);break;}
    case 2:{m_burst_size->value=(32/byteLen);break;}
    case 3:{m_burst_size->value=(64/byteLen);break;}
    case 4:{m_burst_size->value=(128/byteLen);break;}
    case 5:{m_burst_size->value=(256/byteLen);break;}
    case 6:{m_burst_size->value=(512/byteLen);break;}
    case 7:{m_burst_size->value=(1024/byteLen);break;}
    default:
        assert("wrong value of size");
    }
    m_req_size=m_burst_size->value;
    master_sock.template validate_extension<amba::amba_burst_size>(*current_req);
    
    
    unsigned int len=0;
    bool wrap=false;
    ///set the data-length and streaming width
    AMBA_DUMP("burst="<<burst);
    switch(burst)
    {
        case 0:{current_req_count=0;len=m_burst_size->value;break;}
        case 1:{len=max_mem_size;
                master_sock.template validate_extension<amba::amba_imprecise_burst>(*current_req);
                current_req_count=~((unsigned int)0);
                break;}//It is a case of incrementing burst of unspecified len
        case 2:{
                 len=4*m_burst_size->value;
                 wrap=true;
                 current_req_count=3;
                 break;
                }
        case 3:{current_req_count=3; len=4*m_burst_size->value;break;}
        case 4:{
                 len=8*m_burst_size->value;
                 wrap=true;
                 current_req_count=7;
                 break;
               }
        case 5:{current_req_count=7;len=8*m_burst_size->value;break;}
        case 6:{
                 len=16*m_burst_size->value;
                 current_req_count=15;
                 wrap=true;
                 break;
                }
        case 7:{
                len=16*m_burst_size->value;
                current_req_count=15;
                break;
                }
        default:
                assert("Wrong value of the burst");
    }
    master_sock.reserve_data_size(*current_req, len); //it will set the data-length too
    current_req->set_streaming_width(len);
    if(wrap)
    {
        amba::amba_wrap_offset* m_wrap_index;
        master_sock.template get_extension<amba::amba_wrap_offset>(m_wrap_index, *current_req);
        //wrong. calc wrapboundary set addr and index properly.
        sc_dt::uint64 lowest_addr_in_wrap = addr & ~(len - 1);
        sc_dt::uint64 tail_length =addr - lowest_addr_in_wrap;
        current_req->set_address(lowest_addr_in_wrap);
        m_wrap_index->value=tail_length;
        master_sock.template validate_extension<amba::amba_wrap_offset>( *current_req);
        m_req_wrap_index=tail_length;
        AMBA_DUMP("Added wrap extension. Entry is "<<tail_length<<" txn addr is "<<lowest_addr_in_wrap);
    }
    else
      m_req_wrap_index=-1;
    if(fwrite) {
        AMBA_DUMP("fwrite is TRUE. Setting TLM_WRITE_COMND");
        current_req->set_command(tlm::TLM_WRITE_COMMAND);
    }
    else
    {
        AMBA_DUMP("fwrite is FALSE. Setting TLM_READ_COMND");
        current_req->set_command(tlm::TLM_READ_COMMAND);
    }
    
    if((protection&0x01)==0x01)
    	master_sock.template invalidate_extension<amba::amba_instruction>(*current_req);
    else
    	master_sock.template validate_extension<amba::amba_instruction>(*current_req);

    if((protection&0x02)==0x02)
    	master_sock.template validate_extension<amba::amba_privileged>(*current_req);
    else
       master_sock.template invalidate_extension<amba::amba_privileged>(*current_req);
    

    if((protection&0x04)==0x04)
    {
      master_sock.template validate_extension<amba::amba_bufferable>(*current_req);
    }
    if((protection&0x08)==0x08)
    {
        master_sock.template validate_extension<amba::amba_cacheable> (*current_req);
    }
  }
  else
  if (transfer_type==3){
    //a SEQ access may remove the lock
    if (!m_HLOCK.read() && current_lock){
      amba::amba_lock * lock_ext;
      locked_txn_counter++;
      master_sock.template get_extension<amba::amba_lock>(lock_ext, *current_req);
      current_lock->number_of_txns= locked_txn_counter;
      lock_ext->value=current_lock;
      master_sock.template validate_extension<amba::amba_lock>(*current_req);
      current_lock=NULL;
      AMBA_DUMP("unlocking with SEQ");      
    }
    current_req_count--;
  }
  else
  if (transfer_type==0){
    if (!m_HLOCK.read() && current_lock){ //we need this IDLE to unlock
      amba::amba_lock * lock_ext;
      locked_txn_counter++;
      master_sock.template get_extension<amba::amba_lock>(lock_ext, *current_req);
      current_lock->number_of_txns= locked_txn_counter;
      lock_ext->value=current_lock;
      master_sock.template validate_extension<amba::amba_lock>(*current_req);
      current_lock=NULL;
      AMBA_DUMP("unlocking with IDLE");      
    }
    
    current_req_count=0;
  }
  
  if (current_req_count==0) {//we are about to start the last req
    AMBA_DUMP("bus granted set to false");
    bus_granted=false; //only internally so that we resend the BUS_REQ
  }
  
  //create the transaction and send it to the TLM
  amba::amba_trans_type* m_trans_type;
  master_sock.template get_extension<amba::amba_trans_type>(m_trans_type, *current_req);
  m_trans_type->value=(enum amba::ahb_transfers) transfer_type;
  master_sock.template validate_extension<amba::amba_trans_type>(*current_req);

  
  AMBA_DUMP("Sending the Transaction with BEGIN_REQ to TLM World");

  tlm::tlm_phase ph= tlm::BEGIN_REQ;
  sc_core::sc_time delay=sc_core::SC_ZERO_TIME;
  tlm::tlm_sync_enum retval= master_sock->nb_transport_fw(*current_req,ph,delay);

  switch(retval)
  {				
      case tlm::TLM_UPDATED:
      {	
          assert(ph == tlm::END_REQ && "Unexpected phase" );
          AMBA_DUMP("phase: END_REQ, retval=TLM_UPDATED" );
          //the other side will only give us an END_REQ when it will give us an END_DATA/BEGIN_RESP
          // in the same cycle if there is some pending data
          ready_value=true; 
          break;
      }
      case tlm::TLM_ACCEPTED:
      {
          assert(ph== tlm::BEGIN_REQ && "Unexpected phase");
          ready_value=false; 
          AMBA_DUMP("retval=TLM_ACCEPTED");break;
      }
      default:
          assert("Wrong tlm_sync_enum returned");
  }
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE,FORCE_BIGUINT, IS_LITE>::DataProcess()
{
  AMBA_DUMP("RTL_CT::DataProcess");
	if(current_data->get_command()==tlm::TLM_WRITE_COMMAND)
	{
		unsigned char * m_data= current_data->get_data_ptr();
        unsigned int  len =current_data->get_data_length();
		//check if the transfer-size is less than the buswidth
        unsigned int offset=(m_data_addr%((BUSWIDTH+7)/8));     
     
        //TODO: make endianess save.
        unsigned int array_entry=current_data_count*m_data_size;
        if (m_data_wrap_index>0){
          array_entry+=m_data_wrap_index;
          array_entry%=len;
        }
        for (unsigned int i=0; i<m_data_size; i++){
          AMBA_DUMP("Put "<<(m_HWDATA.read().range((i+offset+1)*8-1,(i+offset)*8).to_uint())<<" into array index "<<(i+array_entry));
          m_data[i+array_entry]=(unsigned char)(m_HWDATA.read().range((i+offset+1)*8-1,(i+offset)*8).to_uint()); 
        }        
		tlm::tlm_phase ph= amba::BEGIN_DATA;
		sc_core::sc_time delay=sc_core::SC_ZERO_TIME;
		tlm::tlm_sync_enum retval= master_sock->nb_transport_fw(*current_data,ph,delay);
		switch(retval)
		{
			case tlm::TLM_UPDATED:{
				
				//check the response status of the transaction
				if(ph==amba::DATA_SPLIT)
				{
                  AMBA_DUMP("return phase: DATA_SPLIT");
                  resp_value=SPLIT_RESP;
                  ready_value=false;
				}
				else if(ph== amba::DATA_RETRY)
				{
                  AMBA_DUMP("return phase: DATA_RETRY");
                  resp_value=RETRY_RESP;
                  ready_value=false;
				}
				else if(ph==amba::END_DATA)
				{
                                        m_last_data_time=sc_core::sc_time_stamp();
					//check the response of the transaction	
					tlm::tlm_response_status status= current_data->get_response_status();
					if(status== tlm::TLM_OK_RESPONSE) {
						resp_value=OK_RESP; 
                        ready_value=true;
                        AMBA_DUMP("TLM_OK_RESP");
                    }
					else {
                       AMBA_DUMP("TLM_ERR_RESP");
					   resp_value=ERROR_RESP;
                       ready_value=false;
                    } 
				 }
				else
					assert("invalid phase returned");
                if (current_req!=current_data){ //last data of current txn
                  AMBA_DUMP("TXN release");
                  current_data->release();
                }
                current_data=NULL;
				break;
			}
			case tlm::TLM_ACCEPTED:{
				assert(ph==amba::BEGIN_DATA);
                                 AMBA_DUMP("Returned TLM_ACCEPTED");
			}
			default:
				assert("Invalid tlm_sync_enum returned");
		}
	}
}


template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE,FORCE_BIGUINT, IS_LITE>::ResetProcess()
{
	if(reset_txn==NULL) {AMBA_DUMP("create reset txn ");reset_txn= master_sock.get_transaction();}
	tlm::tlm_phase ph;
	if(m_Reset.read()==false) { //reset is low active
	        m_InReset = true;
		write_ready_ev.cancel();
		begin_req_ev.cancel();
		begin_data_ev.cancel();
		bus_req_ev.cancel();
		current_data = current_req = busreq_txn = NULL;
		m_data_size = 0;
		m_data_wrap_index = -1;
		current_data_count = 0;
		m_data_addr = 0;
		current_req_count = 0;
		old_bus_req = ready_value = bus_granted = owner_of_bus = false;
		ph=amba::RESET_ASSERT;
	}
	else {
	        m_InReset = false;
		ph= amba::RESET_DEASSERT;
	}
	sc_core::sc_time delay= sc_core::SC_ZERO_TIME;
	tlm::tlm_sync_enum retval = master_sock->nb_transport_fw(*reset_txn,ph,delay);
	assert(retval==tlm::TLM_ACCEPTED);
}



template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE, FORCE_BIGUINT, IS_LITE>::WriteReady()
{
#ifdef AMBA_DEBUG
  if (m_HREADYIN.read()  != ready_value)  AMBA_DUMP("RTL_CT::Set HREADY "<<ready_value);
  if (m_HRESP.read()  != resp_value)  AMBA_DUMP("RTL_CT::Set HRESP "<<resp_value);
#endif
  m_HREADYIN.write(ready_value);
  m_HRESP.write(resp_value);
}


template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
void AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE,FORCE_BIGUINT, IS_LITE>::BusGrant()
{
	//Setting the grant to RTL Master
  AMBA_DUMP("RTL_CT::Setting the grant to RTL Master");
  m_HGRANT.write(bus_granted);
}

template<unsigned int BUSWIDTH, typename MODULE, bool FORCE_BIGUINT, bool IS_LITE>
tlm::tlm_sync_enum AHB_Master_RTL_CT_Adapter<BUSWIDTH,MODULE,FORCE_BIGUINT, IS_LITE>::nb_transport_bw(tlm::tlm_generic_payload & trans, tlm::tlm_phase &ph, sc_core::sc_time &delay)
{
    AMBA_DUMP("Reached RTL_CT::nb_transport_bw: "<<ph);
    //When in RESET state, process calls on bw interface only when the Phase is RESET_DEASSERT
    if (m_InReset && (ph != amba::RESET_DEASSERT)) return tlm::TLM_ACCEPTED;

    if (ph == amba::RESET_ASSERT) {
        m_InReset = true;
	write_ready_ev.cancel();
	begin_req_ev.cancel();
	begin_data_ev.cancel();
	bus_req_ev.cancel();
	current_data = current_req = busreq_txn = NULL;
	m_data_size = 0;
	m_data_wrap_index = -1;
	current_data_count = 0;
	m_data_addr = 0;
	current_req_count = 0;
	old_bus_req = ready_value = bus_granted = owner_of_bus = false;
        m_otherSideReset.write(false);
        return tlm::TLM_ACCEPTED;
    }
    else if (ph == amba::RESET_DEASSERT) {
        m_InReset = false;
        m_otherSideReset.write(true);
        return tlm::TLM_ACCEPTED;
    }
    switch(ph){
      case tlm::END_REQ : //either happens when we have no pending data, or when we have pending data and the END_DATA/BEGIN_RESP will come in the same cycle
        ready_value=true;
        break;
      case tlm::BEGIN_RESP:
      {
        m_last_data_time=sc_core::sc_time_stamp();
		//Read transaction received
        assert(current_data==&trans);
		unsigned char * m_data= current_data->get_data_ptr();
        unsigned int  len =current_data->get_data_length();
		//check if the transfer-size is less than the buswidth
        unsigned int offset=(m_data_addr%((BUSWIDTH+7)/8));     
        
        //TODO: make endianess save.
        unsigned int array_entry=current_data_count*m_data_size;
        if (m_data_wrap_index>0){
          array_entry+=m_data_wrap_index;
          array_entry%=len;
        }
        m_tmp_dt=m_HRDATA.read();
        for (unsigned int i=0; i<m_data_size; i++){
          m_tmp_dt.range((i+offset+1)*8-1,(i+offset)*8)=m_data[i+array_entry];
        }
        m_HRDATA.write(m_tmp_dt);

		//check the response status also and send to the master
		tlm::tlm_response_status status= trans.get_response_status();
		if(status== tlm::TLM_OK_RESPONSE) {
            AMBA_DUMP("ok_resp");
			resp_value=OK_RESP; //send the OKAY response
            ready_value=true;
        }
		else {
            AMBA_DUMP("Error_resp");
			resp_value=ERROR_RESP;//send the ERROR response
            ready_value=false;
        }
        if (current_req!=current_data){ //last data of current txn
          AMBA_DUMP("TXN release");
          current_data->release();
        }
        current_data=NULL;
        break;
      }
      default:
        if (ph==amba::END_DATA){
          m_last_data_time=sc_core::sc_time_stamp();
          //check the response of the transaction	
          assert(current_data==&trans);
          tlm::tlm_response_status status= current_data->get_response_status();
          if(status== tlm::TLM_OK_RESPONSE) {
              resp_value=OK_RESP; 
              ready_value=true;
              AMBA_DUMP("TLM_OK_RESP");
          }
          else {
             AMBA_DUMP("TLM_ERR_RESP");
             resp_value=ERROR_RESP;
             ready_value=false;
          }
          if (current_req!=current_data){ //last data of current txn
            AMBA_DUMP("TXN release");
            current_data->release();
          }
          current_data=NULL;          
        }
        else
        if(ph== amba::BUS_GRANT)
        {
            bus_granted=true;
            ready_value=true;
            bus_grant_ev.notify();
        }
        else 
        if(ph== amba::BUS_UNGRANT)
        {
            bus_granted=false;
            bus_grant_ev.notify();
        }
        else 
        if(ph==amba::DATA_SPLIT)
        {
            resp_value=SPLIT_RESP;
            ready_value=false;
        }
        else 
        if(ph==amba::DATA_RETRY)
        {
            resp_value=RETRY_RESP;
            ready_value=false;
        }        
        else
        {
            assert("Phase not implemented in adapter");
        }
    }
    return tlm::TLM_ACCEPTED;
}
#endif /*AHB_MASTER_RTL_CT_ADAPTER_*/
