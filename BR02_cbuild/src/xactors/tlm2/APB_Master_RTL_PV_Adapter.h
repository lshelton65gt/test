// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _APB_MASTER_RTL_PV_ADAPTER_H_
#define _APB_MASTER_RTL_PV_ADAPTER_H_

#include "xactors/tlm2/APB_Master_RTL_LT_Adapter.h"
#include "adapters/AMBA_LT_AT_PV_Slave_Adapter.h"
#include "greensocket/monitor/green_socket_monitor.h"

template<typename MODULE, unsigned int BUSWIDTH=64, unsigned int ADDRWIDTH=32>
class APB_Master_RTL_PV_Adapter: public sc_core::sc_module
#ifdef CARBON_DEBUG_ACCESS
                               , public CarbonDebugAccessIF
#endif
{
public:
  ///RTL signals
  sc_core::sc_in_clk m_clk;
  sc_core::sc_in<bool> m_Reset;
  sc_core::sc_in<bool > m_psel;
  sc_core::sc_in<bool > m_penable;
  sc_core::sc_in<bool > m_pwrite;
  sc_core::sc_in<sc_dt::sc_uint<ADDRWIDTH> > m_paddr;
  sc_core::sc_in<sc_dt::sc_uint<BUSWIDTH> > m_pwdata;
  sc_core::sc_out<bool > m_pready;
  sc_core::sc_out<sc_dt::sc_uint<BUSWIDTH> > m_prdata;
  sc_core::sc_out<bool > m_pslverr;
	
  //TLM2 based Amba initiator socket
  tlm::tlm_initiator_socket<BUSWIDTH, amba_pv::amba_pv_protocol_types> master_sock;

  // Timing APIs - pass on to the RTL LT Adapter
  void set_master_timing(gs::socket::timing_info &tm)
  {
    apb_rtl_lt_adapter.set_master_timing(tm);
  }
  void set_master_timing(tlm::tlm_phase phase, const sc_core::sc_time& tm)
  {
    apb_rtl_lt_adapter.set_master_timing(phase, tm);
  }
  void register_slave_timing_listener( MODULE* mod,void (MODULE::*fn)(gs::socket::timing_info))
  {
    apb_rtl_lt_adapter.register_slave_timing_listener(mod, fn);
  }

#ifdef CARBON_DEBUG_ACCESS
  // Carbon debug access - pass to the RTL LT Adapter
  CarbonDebugAccessStatus debugMemRead(uint64_t addr, uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0)
  {
    return apb_rtl_lt_adapter.debugMemRead(addr, buf, numBytes, ctrl);
  }

  CarbonDebugAccessStatus debugMemWrite (uint64_t addr, const uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0)
  {
    return apb_rtl_lt_adapter.debugMemWrite(addr, buf, numBytes, ctrl);
  }
#endif

  //! API to get and activate the monitor on the LT socket
  gs::socket::monitor<BUSWIDTH>* getMonitor(void)
  {
    lt_monitor->activated = true;
    return lt_monitor;
  }

  //! Constructor
  SC_HAS_PROCESS(APB_Master_RTL_PV_Adapter);
  APB_Master_RTL_PV_Adapter(sc_core::sc_module_name nm) : 
    sc_module(nm),
    master_sock("apb_master_socket"),
    apb_rtl_lt_adapter("apb_rtl_lt_adapter"),
    lt_pv_adapter("lt_pv_adapter")
  {
    // Bind the RTL pins from this adapter to the LT adapter
    apb_rtl_lt_adapter.m_clk(m_clk);
    apb_rtl_lt_adapter.m_Reset(m_Reset);
    apb_rtl_lt_adapter.m_psel(m_psel);
    apb_rtl_lt_adapter.m_penable(m_penable);
    apb_rtl_lt_adapter.m_pwrite(m_pwrite);
    apb_rtl_lt_adapter.m_paddr(m_paddr);
    apb_rtl_lt_adapter.m_pwdata(m_pwdata);
    apb_rtl_lt_adapter.m_pready(m_pready);
    apb_rtl_lt_adapter.m_prdata(m_prdata);
    apb_rtl_lt_adapter.m_pslverr(m_pslverr);

    // Connect the apb RTL LT socket to the lt_pv_adapter socket. We
    // create a monitor but de-activate it by default
    lt_monitor = gs::socket::connect_with_monitor<BUSWIDTH, tlm::tlm_base_protocol_types>(apb_rtl_lt_adapter.master_sock, lt_pv_adapter.slave_socket);
    lt_monitor->activated = false;

    // Bind our socket to the LT PV Adapter.
    lt_pv_adapter.master_socket.bind(master_sock);
  }

private:
  //! APB RTL LT Adapter
  APB_Master_RTL_LT_Adapter<MODULE, BUSWIDTH, ADDRWIDTH> apb_rtl_lt_adapter;

  //! LT to PV Adapter
  amba::AMBA_LT_AT_PV_Slave_Adapter<BUSWIDTH> lt_pv_adapter;

  //! The monitor for activity detection
  gs::socket::monitor<BUSWIDTH>* lt_monitor;
}; // #endif

#endif // _APB_MASTER_RTL_PV_ADAPTER_H_
