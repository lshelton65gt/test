// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef APB_CT_RTL_SLAVE_ADAPTER_H_
#define APB_CT_RTL_SLAVE_ADAPTER_H_

template <typename MODULE, unsigned int BUSWIDTH=32, unsigned int ADDRWIDTH=32>
class APB_CT_RTL_Slave_Adapter: public sc_core::sc_module
{
public:
	//Input tlm port
	amba::amba_slave_socket<BUSWIDTH> slave_sock;
	
	//clk
	sc_core::sc_in_clk m_clk;
        sc_core::sc_signal<bool> m_otherSideReset;
	sc_core::sc_in<bool> m_Reset;
	
	///output sc_signals
	sc_core::sc_out<bool > m_psel;
	sc_core::sc_out<bool > m_penable;
	sc_core::sc_out<bool > m_pwrite;
	sc_core::sc_out<sc_dt::sc_uint<ADDRWIDTH> > m_paddr;
	sc_core::sc_out<sc_dt::sc_uint<BUSWIDTH> > m_pwdata;
	sc_core::sc_in<bool > m_pready;
	sc_core::sc_in<sc_dt::sc_uint<BUSWIDTH> > m_prdata;
	sc_core::sc_in<bool > m_pslverr;
	
	//
	tlm::tlm_sync_enum nb_transport_fw(tlm::tlm_generic_payload & trans, tlm::tlm_phase &ph, sc_core::sc_time &delay);
	void master_timing_listener(gs::socket::timing_info);
	void end_of_elaboration();
    ///timing-info-distro setters/getters
	void set_slave_timing(gs::socket::timing_info &);
	void set_slave_timing(tlm::tlm_phase ph,const sc_core::sc_time &);
	gs::socket::timing_info & get_slave_timing();
	sc_core::sc_time get_slave_timing(tlm::tlm_phase);
	inline void register_master_timing_listener(MODULE* mod, void (MODULE::*fn)(gs::socket::timing_info))
	{
		owner=mod;
	    timing_cb=fn;
	}

	
	//method
	void ResponseMethod();
	void clock_tick();
	void SelectMethod();
	void EnableMethod();

#ifdef CARBON_DEBUG_ACCESS
	 ///APIs for Carbon Debug
	 void registerDebugAccessCB(CarbonDebugAccessIF*);
#endif
	 unsigned int transport_debug(tlm::tlm_generic_payload & trans);

    SC_HAS_PROCESS(APB_CT_RTL_Slave_Adapter);
    APB_CT_RTL_Slave_Adapter(sc_core::sc_module_name nm):sc_core::sc_module(nm),
    slave_sock("slave_socket",amba::amba_APB, amba::amba_CT,false),
    m_clk("CLOCK"),
    m_Reset("APB_RESET"),
    m_psel("APB_PSEL"),
    m_penable("APB_PENABLE"),
    m_pwrite("APB_PWRITE"),
    m_paddr("APB_PADDR"),
    m_pwdata("APB_PWDATA"),
    m_pready("APB_PREADY"),
    m_prdata("APB_PRDATA"),
    m_pslverr("APB_PSLVERR"),
    current_trans(NULL),
    m_reset_trans(NULL),    
    timing_cb(0),
    owner(NULL),
#ifdef CARBON_DEBUG_ACCESS        
    debugIf(NULL),
#endif
    ready_done(false),
    p_selected(false),
    p_enabled(false),
    m_InReset(false),    
    time_unit(sc_core::sc_get_time_resolution()),
    bresp_sample_time(sc_core::sc_get_time_resolution()),
    begin_resp_time(sc_core::sc_get_time_resolution()),
    resp_pending(false)

    {
        SC_METHOD(ResetMethod);
        sensitive << m_Reset;
        dont_initialize();

    	SC_METHOD(ResponseMethod);
    	sensitive<< pready_ev;
    	dont_initialize();
    	
    	SC_METHOD(clock_tick);
    	sensitive<< m_clk.pos();
    	dont_initialize();

    	SC_METHOD(SelectMethod);
    	sensitive<<psel_ev;
    	dont_initialize();

    	SC_METHOD(EnableMethod);
    	sensitive<<penable_ev;
    	dont_initialize();

    	slave_sock.template register_nb_transport_fw<APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH> >(this, & APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH>::nb_transport_fw);
        slave_sock.template register_transport_dbg< APB_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH> >(this, &APB_CT_RTL_Slave_Adapter< MODULE,BUSWIDTH,ADDRWIDTH>::transport_debug);
    	slave_sock.set_timing_listener_callback(this, & APB_CT_RTL_Slave_Adapter::master_timing_listener);
    }
    ~APB_CT_RTL_Slave_Adapter();
private:
        void ResetMethod ();
	tlm::tlm_generic_payload * current_trans;
	tlm::tlm_generic_payload * m_reset_trans;
	void (MODULE::*timing_cb)(gs::socket::timing_info);
	MODULE * owner;
	//pointer for the debug access
#ifdef CARBON_DEBUG_ACCESS    
	CarbonDebugAccessIF * debugIf;
#endif
	bool ready_done;
	bool p_selected;
	bool p_enabled;
	bool m_InReset;
	sc_core::sc_event psel_ev;
	sc_core::sc_event penable_ev;
	sc_core::sc_event pready_ev;
	gs::socket::timing_info slave_timing_info;
	sc_core::sc_time time_unit;
	sc_core::sc_time bresp_sample_time;
	sc_core::sc_time begin_resp_time;
	bool resp_pending;
	
};

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH>::end_of_elaboration()
{
	slave_sock.activate_synchronization_protection();
	sc_core::sc_time delay=  sc_core::sc_get_time_resolution();
	if(slave_timing_info.get_start_time(amba::PSELECT)<delay)
		slave_timing_info.set_start_time(amba::PSELECT,delay);
	if(slave_timing_info.get_start_time(tlm::BEGIN_REQ)< delay)
		slave_timing_info.set_start_time(tlm::BEGIN_REQ, delay);
	if(slave_timing_info.get_start_time(tlm::BEGIN_RESP)< delay)
		slave_timing_info.set_start_time(tlm::BEGIN_RESP,delay);
	slave_sock.set_target_timing(slave_timing_info);
	bresp_sample_time= slave_timing_info.get_start_time(tlm::BEGIN_RESP);
	if(timing_cb!=NULL)
		 (owner->*timing_cb)(slave_timing_info);
}

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH>
unsigned int APB_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::transport_debug(tlm::tlm_generic_payload& payload)
{
#ifdef CARBON_DEBUG_ACCESS    
	unsigned long long address = payload.get_address();
	unsigned int length=payload.get_data_length();
	unsigned int * ctrl = new unsigned int();
	if(slave_sock.template get_extension<amba::amba_secure_access>(payload))
       	ctrl[0]=0x01;
    else
    	ctrl[0]=0x00;
    CarbonDebugAccessStatus retval;
	if(payload.get_command()==tlm::TLM_READ_COMMAND)
	{
       retval= debugIf->debugMemRead(address,(payload.get_data_ptr()),length,ctrl);
	}
	else if(payload.get_command()==tlm::TLM_WRITE_COMMAND)
	{
		retval= debugIf->debugMemWrite(address,(payload.get_data_ptr()),length,ctrl);
	}
	unsigned int transferred_bytes=0;
	switch(retval)
	{
		case eCarbonDebugAccessStatusOutRange:
		{
			payload.set_response_status(tlm::TLM_ADDRESS_ERROR_RESPONSE);
                        break;
		}
		case eCarbonDebugAccessStatusOk:{
			payload.set_response_status(tlm::TLM_OK_RESPONSE);
                        transferred_bytes=length;
			break;
		}
		case eCarbonDebugAccessStatusError:{
			payload.set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
                        break;

		}
		case eCarbonDebugAccessStatusPartial:{
                        // We should return the number of bytes transferred
                        break;
		}
		default:
			assert("Invalid DebugAccess return value");
	}
	delete(ctrl);
	return transferred_bytes;
#else
  return 0;
#endif
}

#ifdef CARBON_DEBUG_ACCESS    
template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH>
void APB_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH>::registerDebugAccessCB(CarbonDebugAccessIF* dbgIf)
{
 debugIf=dbgIf;
}
#endif

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH>::set_slave_timing(gs::socket::timing_info & info)
{

	if((info.get_start_time(tlm::BEGIN_RESP)+time_unit) > slave_timing_info.get_start_time(tlm::BEGIN_RESP))
	{
		slave_timing_info.set_start_time(tlm::BEGIN_RESP, (info.get_start_time(tlm::BEGIN_RESP)+time_unit));
		slave_sock.set_target_timing(slave_timing_info);
		bresp_sample_time=slave_timing_info.get_start_time(tlm::BEGIN_RESP);
	}

}

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH>::set_slave_timing(tlm::tlm_phase ph, const sc_core::sc_time & delay)
{
	if(ph== tlm::BEGIN_RESP &&
	((delay+time_unit) > slave_timing_info.get_start_time(tlm::BEGIN_RESP)))
	{
		slave_timing_info.set_start_time(tlm::BEGIN_RESP,(delay+time_unit));
		slave_sock.set_target_timing(slave_timing_info);
		bresp_sample_time=slave_timing_info.get_start_time(tlm::BEGIN_RESP);
	}

}

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
gs::socket::timing_info& APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH>::get_slave_timing()
{
	return slave_timing_info;
}

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
sc_core::sc_time APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH>::get_slave_timing(tlm::tlm_phase ph)
{
	return slave_timing_info.get_start_time(ph);
}

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH>::master_timing_listener(gs::socket::timing_info info)
{
	bool changed=false;
	if((info.get_start_time(tlm::BEGIN_REQ)+time_unit) > slave_timing_info.get_start_time(tlm::BEGIN_REQ))
	{
		slave_timing_info.set_start_time(tlm::BEGIN_REQ,(info.get_start_time(tlm::BEGIN_REQ)+time_unit));
		changed=true;
	}
	if((info.get_start_time(amba::PSELECT)+time_unit) > slave_timing_info.get_start_time(amba::PSELECT))
	{
		slave_timing_info.set_start_time(amba::PSELECT,(info.get_start_time(amba::PSELECT)+time_unit));
		changed=true;
	}
	if(changed && timing_cb!=NULL)
		 (owner->*timing_cb)(slave_timing_info);
}

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH>::~APB_CT_RTL_Slave_Adapter()
{
}

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH>::SelectMethod()
{
	m_psel.write(p_selected);
}

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH>::EnableMethod()
{
	m_penable.write(p_enabled);
}



/*
 * @brief This function triggers on each +ve edge of clk
 */
template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH>::clock_tick()
{
	if (m_InReset) return;

	if(ready_done)
	{
		//ok we have got the response from the slave too
		ready_done=false;
		p_enabled=false;
		p_selected=false;
		penable_ev.notify();
		psel_ev.notify();
	}
	if(m_psel.read()==true)
	{//Ok last cycle was a enable cycle and selected
		pready_ev.notify(bresp_sample_time);
	}
	begin_resp_time= sc_core::sc_time_stamp()+bresp_sample_time;
}

/*
 * @brief this fuction is triggered when the slave writes to its
 * Reset output
 * 
 */

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH>::ResetMethod()
{
  if (m_reset_trans == NULL)
    m_reset_trans = slave_sock.get_reset_txn();

  tlm::tlm_phase ph;
  if (m_Reset.read()) {
    m_InReset = false;
    ph = amba::RESET_DEASSERT;
  }
  else { //ACTIVE LOW
    m_InReset = true;
    ready_done = false;
    pready_ev.cancel();
    penable_ev.cancel();
    psel_ev.cancel();
    current_trans = 0;
    ph = amba::RESET_ASSERT;
  }

  sc_core::sc_time delay = sc_core::SC_ZERO_TIME;
  tlm::tlm_sync_enum retval = slave_sock->nb_transport_bw(*m_reset_trans, ph, delay);
  assert(retval== tlm::TLM_ACCEPTED && "Wrong tlm_sync_enum returned");
}
/*
 * @brief this fuction is triggered when the slave responds with
 * pready signal
 * 
 */

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
void APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH>::ResponseMethod()
{
	 if(current_trans==NULL)
	    	return;
	if(!resp_pending)
		return;
	if(m_pready.read()==false)
		return;
	AMBA_DUMP("PReady triggered");
	//check the command
	if(current_trans->get_command()== tlm::TLM_READ_COMMAND)
	{
		sc_dt::sc_uint<BUSWIDTH> data = m_prdata.read();
		unsigned char * data_ptr = current_trans->get_data_ptr();
		unsigned int len= current_trans->get_data_length();
		unsigned int i=0;
		for(i=0; i<((BUSWIDTH+7)/8); i++)
		{
			data_ptr[i]=data.range((i+1)*8-1, i*8).to_uint();
		}
		AMBA_DUMP("Read data"<<data<<" from address"<<(unsigned int)current_trans->get_address()<<std::endl);
	}
	if(m_pslverr.read()==true)
	{
		current_trans->set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
		AMBA_DUMP("pslverr=true"<<std::endl);
	}
	else
	{
		current_trans->set_response_status(tlm::TLM_OK_RESPONSE);
		AMBA_DUMP("pslverr=false"<<std::endl);
	}
	AMBA_DUMP("Sending Response through bw_transport ");
	tlm::tlm_phase ph= tlm::BEGIN_RESP;
	sc_core::sc_time delay = sc_core::SC_ZERO_TIME;
	tlm::tlm_sync_enum retval= slave_sock->nb_transport_bw(*current_trans, ph,delay);
	assert(retval== tlm::TLM_ACCEPTED && "Wrong tlm_sync_enum returned");
	ready_done=true;
	resp_pending=false;
}

/*
 * @brief : This function is forward transport function called from
 * master to upload the addr and control signals 
 */

template <typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH>
tlm::tlm_sync_enum  APB_CT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH>::nb_transport_fw(tlm::tlm_generic_payload & trans, tlm::tlm_phase & ph, sc_core::sc_time &delay)
{
	//When in RESET state, process calls on fw interface only when the Phase is RESET_DEASSERT
        if (m_InReset && (ph != amba::RESET_DEASSERT)) return tlm::TLM_ACCEPTED;
	sc_core::sc_time current_time= sc_core::sc_time_stamp();
	if(ph== amba::PSELECT)
	{
	    AMBA_DUMP("phase: PSELECT");
		//OK So this is the SETUP state
		current_trans= &trans;
		//read the data in access state
		unsigned int address= current_trans->get_address();
		bool m_write=false;
		if(current_trans->get_command()==tlm::TLM_WRITE_COMMAND)
			m_write=true;
		else
			m_write=false;
		m_pwrite.write(m_write);
		m_paddr.write(address);
		p_selected=true;
		psel_ev.notify();

		sc_dt::sc_uint<BUSWIDTH> data=0;

		if(current_trans->get_command()==tlm::TLM_WRITE_COMMAND)
		{

			unsigned char * data_ptr= current_trans->get_data_ptr();
			unsigned int len= current_trans->get_data_length();
			unsigned int k=0;
			for(k=0; k<((BUSWIDTH+7)/8); k++)
			{
				data.range((k+1)*8-1,k*8)=data_ptr[k];
			}
			m_pwdata.write(data);
			AMBA_DUMP("Write request received with data ="<<(unsigned int)data<<"of length="<<len<<std::endl);
		}

	}
	else if(ph== tlm::BEGIN_REQ)
	{
		AMBA_DUMP("phase:BEGIN_REQ");
		//ok so this is the ACCESS state
		assert(current_trans == &trans && "Invalid transaction");

		p_enabled=true;
		penable_ev.notify();

		if(current_time>= begin_resp_time)
		{
			if(m_pready.read()==true)
			{
				AMBA_DUMP("pready already high, reading the data in the same cycle"<<std::endl);
				ph= tlm::BEGIN_RESP;
				if(current_trans->get_command()== tlm::TLM_READ_COMMAND)
				{
					sc_dt::sc_uint<BUSWIDTH> tmpdata = m_prdata.read();
					unsigned char * data_ptr = current_trans->get_data_ptr();
					unsigned int k=0;
					for(k=0; k< ((BUSWIDTH+7)/8); k++)
					{
						data_ptr[k]= tmpdata.range((k+1)*8-1, k*8).to_uint();
					}
					AMBA_DUMP("data read ="<<tmpdata<<std::endl);
				}

				if(m_pslverr.read()==true)
				{
					current_trans->set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
					AMBA_DUMP("pslverr=true");
				}
				else
				{
					current_trans->set_response_status(tlm::TLM_OK_RESPONSE);
					AMBA_DUMP("pslverr=false");
				}
				ready_done=true;
				resp_pending=false;
				return tlm::TLM_UPDATED;
			}
			else
			{
				ready_done=false;
				resp_pending=true;
				return tlm::TLM_ACCEPTED;
			}
		}
		else
		{
			ready_done=false;
			resp_pending=true;
			return tlm::TLM_ACCEPTED;
		}
	}
	else if(ph== amba::RESET_ASSERT) //ACTIVE_LOW
	{
	        m_InReset = true;
                ready_done = false;
                pready_ev.cancel();
                penable_ev.cancel();
                psel_ev.cancel();
                current_trans = 0;
		m_otherSideReset.write(false);
	}
	else if(ph== amba::RESET_DEASSERT)
	{
	        m_InReset = false;
		m_otherSideReset.write(true);
	}
	else
		assert("Wrong phase returned ");
	return tlm::TLM_ACCEPTED;
	
}

#endif /*APB_CT_RTL_SLAVE_ADAPTER_H_*/
