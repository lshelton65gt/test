// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _AHB_PV_RTL_SLAVE_ADAPTER_H_
#define _AHB_PV_RTL_SLAVE_ADAPTER_H_

#include "xactors/tlm2/AHB_LT_RTL_Slave_Adapter.h"

#include "adapters/AMBA_PV_Master_LT_AT_Adapter.h"

#include "greensocket/monitor/green_socket_monitor.h"

template<unsigned int BUSWIDTH, typename MODULE,bool FORCE_BIGUINT=false, bool IS_LITE=false>
class AHB_PV_RTL_Slave_Adapter: public sc_core::sc_module
{
public:
  // TLM port
  tlm::tlm_target_socket<BUSWIDTH, amba_pv::amba_pv_protocol_types> slave_sock;
	
  // RTL ports
  sc_core::sc_in_clk m_clk;
  sc_core::sc_in<bool> m_Reset;
  sc_core::sc_out<bool > m_HSEL;
  sc_core::sc_out<bool > m_HWRITE;
  sc_core::sc_out<sc_dt::sc_uint<2> > m_HTRANS;
  sc_core::sc_out<sc_dt::sc_uint<32> > m_HADDR;
  sc_core::sc_out<sc_dt::sc_uint<3> > m_HSIZE;
  sc_core::sc_out<sc_dt::sc_uint<3> > m_HBURST;
  sc_core::sc_out<sc_dt::sc_uint<4> > m_HPROT;
  sc_core::sc_out<bool > m_HREADYIN;
  sc_core::sc_out<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type> m_HWDATA;
  sc_core::sc_in<bool > m_HREADYOUT;
  sc_core::sc_in<typename amba::port_enabler<!IS_LITE>::resp_type > m_HRESP;
  sc_core::sc_in<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type> m_HRDATA;
  typename amba::port_enabler<!IS_LITE, sc_dt::sc_uint<4> >::output_port_type m_HMASTER;
  sc_core::sc_out<bool> m_HMASTLOCK;
  typename amba::port_enabler<!IS_LITE, sc_dt::sc_uint<16> >::input_port_type m_HSPLIT;
	  
  // Timing related APIs. Pass on to the LT RTL adapter
  void set_slave_timing(gs::socket::timing_info& info)
  {
    ahb_lt_rtl_adapter.set_slave_timing(info);
  }
  void set_slave_timing(tlm::tlm_phase phase, const sc_core::sc_time& tm)
  {
    ahb_lt_rtl_adapter.set_slave_timing(phase, tm);
  }
  void register_master_timing_listener(MODULE* mod, void (MODULE::*fn)(gs::socket::timing_info))
  {
    ahb_lt_rtl_adapter.register_master_timing_listener(mod, fn);
  }

  // APIs for Carbon Debug. Pass on to the LT RTL adapter
  void registerDebugAccessCB(CarbonDebugAccessIF* dbgIf)
  {
    ahb_lt_rtl_adapter.registerDebugAccessCB(dbgIf);
  }

  //! API to get and activate the monitor on the LT socket
  gs::socket::monitor<BUSWIDTH>* getMonitor(void)
  {
    lt_monitor->activated = true;
    return lt_monitor;
  }

  //! Constructor
  SC_HAS_PROCESS(AHB_PV_RTL_Slave_Adapter);
  AHB_PV_RTL_Slave_Adapter(sc_core::sc_module_name nm) :
    sc_module(nm),
    slave_sock("AHB_Slave_RTL_LT"),
    ahb_lt_rtl_adapter("ahb_lt_rtl_adapter"),
    pv_lt_adapter("ahb_pv_lt_adapter")
  {
    // Connect the RTL pins to the LT_RTL adapter
    ahb_lt_rtl_adapter.m_clk(m_clk);
    ahb_lt_rtl_adapter.m_Reset(m_Reset);
    ahb_lt_rtl_adapter.m_HSEL(m_HSEL);
    ahb_lt_rtl_adapter.m_HWRITE(m_HWRITE);
    ahb_lt_rtl_adapter.m_HTRANS(m_HTRANS);
    ahb_lt_rtl_adapter.m_HADDR(m_HADDR);
    ahb_lt_rtl_adapter.m_HSIZE(m_HSIZE);
    ahb_lt_rtl_adapter.m_HBURST(m_HBURST);
    ahb_lt_rtl_adapter.m_HPROT(m_HPROT);
    ahb_lt_rtl_adapter.m_HREADYIN(m_HREADYIN);
    ahb_lt_rtl_adapter.m_HWDATA(m_HWDATA);
    ahb_lt_rtl_adapter.m_HREADYOUT(m_HREADYOUT);
    ahb_lt_rtl_adapter.m_HRESP(m_HRESP);
    ahb_lt_rtl_adapter.m_HRDATA(m_HRDATA);
    ahb_lt_rtl_adapter.m_HMASTER(m_HMASTER);
    if (!IS_LITE) {
      ahb_lt_rtl_adapter.m_HMASTLOCK(m_HMASTLOCK);
      ahb_lt_rtl_adapter.m_HSPLIT(m_HSPLIT);
    }

    // Connect the AHB LT RTL socket to the pv_lt_adapter socket. We
    // create a monitor but de-activate it by default
    lt_monitor = gs::socket::connect_with_monitor<BUSWIDTH, tlm::tlm_base_protocol_types>(pv_lt_adapter.master_socket, ahb_lt_rtl_adapter.slave_sock);
    lt_monitor->activated = false;

    // Bind our socket to the PV LT/AT Adapter.
    slave_sock.bind(pv_lt_adapter.slave_socket);
  } // AHB_PV_RTL_Slave_Adapter

private:
  //! The LT_RTL adapter
  AHB_LT_RTL_Slave_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE> ahb_lt_rtl_adapter;

  //! The PV LT/AT adapter
  amba::AMBA_PV_Master_LT_AT_Adapter<BUSWIDTH> pv_lt_adapter;

  //! The monitor for activity detection
  gs::socket::monitor<BUSWIDTH>* lt_monitor;
}; // class AHB_PV_RTL_Slave_Adapter: public sc_core::sc_module

#endif // _AHB_PV_RTL_SLAVE_ADAPTER_H_
