// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _AHB_MASTER_LT_RTL_ADAPTER_H_
#define _AHB_MASTER_LT_RTL_ADAPTER_H_

#include "xactors/tlm2/AHB_Master_CT_RTL_Adapter.h"

#include "adapters/AMBA_LT_CT_Adapter.h"

#include "greensocket/monitor/green_socket_monitor.h"

template<unsigned int BUSWIDTH, typename MODULE,bool FORCE_BIGUINT=false, bool IS_LITE=false>
class AHB_Master_LT_RTL_Adapter: public sc_core::sc_module
{
public:
  // TLM port
  amba::amba_slave_socket<BUSWIDTH> slave_sock;
	
  // RTL ports
  sc_core::sc_in_clk m_clk;
  sc_core::sc_in<bool> m_Reset;
  typename amba::port_enabler<!IS_LITE>::output_port_type m_HBUSREQ;
  sc_core::sc_out<bool> m_HLOCK;
  sc_core::sc_out<sc_dt::sc_uint<2> > m_HTRANS;
  sc_core::sc_out<sc_dt::sc_uint<32> >m_HADDR;
  sc_core::sc_out<sc_dt::sc_uint<3> > m_HSIZE;
  sc_core::sc_out<sc_dt::sc_uint<3> > m_HBURST;
  sc_core::sc_out<sc_dt::sc_uint<4> > m_HPROT;
  sc_core::sc_out<bool> m_HWRITE;
  sc_core::sc_out<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type> m_HWDATA;
  sc_core::sc_in<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type > m_HRDATA;
  sc_core::sc_in<typename amba::port_enabler<!IS_LITE>::resp_type > m_HRESP;
  typename amba::port_enabler<!IS_LITE>::input_port_type m_HGRANT;
  sc_core::sc_in<bool> m_HREADYIN;
	  
  // Timing related APIs. Pass on to the CT RTL adapter
  void set_slave_timing(gs::socket::timing_info& info)
  {
    ahb_ct_rtl_adapter.set_slave_timing(info);
  }
  void set_slave_timing(tlm::tlm_phase phase, const sc_core::sc_time& tm)
  {
    ahb_ct_rtl_adapter.set_slave_timing(phase, tm);
  }
  void register_master_timing_listener(MODULE* mod, void (MODULE::*fn)(gs::socket::timing_info))
  {
    ahb_ct_rtl_adapter.register_master_timing_listener(mod, fn);
  }

  // APIs for Carbon Debug. Pass on to the CT RTL adapter
  void registerDebugAccessCB(CarbonDebugAccessIF* dbgIf)
  {
    ahb_ct_rtl_adapter.registerDebugAccessCB(dbgIf);
  }

  //! API to get and activate the monitor on the LT socket
  gs::socket::monitor<BUSWIDTH>* getMonitor(void)
  {
    lt_monitor->activated = true;
    return lt_monitor;
  }

  //! Constructor
  SC_HAS_PROCESS(AHB_Master_LT_RTL_Adapter);
  AHB_Master_LT_RTL_Adapter(sc_core::sc_module_name nm) :
    sc_module(nm),
    slave_sock("AHB_Slave_RTL_LT", IS_LITE ? amba::amba_AHBLite : amba::amba_AHB, amba::amba_LT, true),
    ahb_ct_rtl_adapter("ahb_ct_rtl_adapter"),
    lt_ct_adapter("ahb_lt_ct_adapter", IS_LITE ? amba::amba_AHBLite : amba::amba_AHB, true)
  {
    // Connect the RTL pins to the CT_RTL adapter
    ahb_ct_rtl_adapter.m_clk(m_clk);
    ahb_ct_rtl_adapter.m_Reset(m_Reset);
    amba::port_enabler<!IS_LITE, bool>::connectOutput(ahb_ct_rtl_adapter.m_HBUSREQ, m_HBUSREQ);
    ahb_ct_rtl_adapter.m_HLOCK(m_HLOCK);
    ahb_ct_rtl_adapter.m_HTRANS(m_HTRANS);
    ahb_ct_rtl_adapter.m_HADDR(m_HADDR);
    ahb_ct_rtl_adapter.m_HSIZE(m_HSIZE);
    ahb_ct_rtl_adapter.m_HBURST(m_HBURST);
    ahb_ct_rtl_adapter.m_HPROT(m_HPROT);
    ahb_ct_rtl_adapter.m_HWRITE(m_HWRITE);
    ahb_ct_rtl_adapter.m_HWDATA(m_HWDATA);
    ahb_ct_rtl_adapter.m_HRDATA(m_HRDATA);
    ahb_ct_rtl_adapter.m_HRESP(m_HRESP);
    amba::port_enabler<!IS_LITE, bool>::connectInput(ahb_ct_rtl_adapter.m_HGRANT, m_HGRANT);
    ahb_ct_rtl_adapter.m_HREADYIN(m_HREADYIN);

    // Connect the clock to the AMBA LT CT adapter
    lt_ct_adapter.clk(m_clk);

    // Connect the AHB CT RTL socket to the lt_ct_adapter socket
    lt_ct_adapter.master_sock(ahb_ct_rtl_adapter.slave_sock);

    // Bind our socket to the LT CT Adapter. We create a monitor but de-activate it by default
    lt_monitor = gs::socket::connect_with_monitor<BUSWIDTH, tlm::tlm_base_protocol_types>(slave_sock, lt_ct_adapter.slave_sock);
    lt_monitor->activated = false;
  } // AHB_Master_LT_RTL_Adapter

private:
  //! The CT_RTL adapter
  AHB_Master_CT_RTL_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE> ahb_ct_rtl_adapter;

  //! The LT CT adapter
  amba::AMBA_LT_CT_Adapter<BUSWIDTH> lt_ct_adapter;

  //! The monitor for activity detection
  gs::socket::monitor<BUSWIDTH>* lt_monitor;
}; // class AHB_Master_LT_RTL_Adapter: public sc_core::sc_module

#endif // _AHB_MASTER_LT_RTL_ADAPTER_H_
