// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _AXI_MASTER_RTL_LT_ADAPTER_H_
#define _AXI_MASTER_RTL_LT_ADAPTER_H_

#include "xactors/tlm2/AXI_Master_RTL_CT_Adapter.h"
#include "adapters/AMBA_CT_LT_Adapter.h"
#include "greensocket/monitor/green_socket_monitor.h"

template<typename MODULE, unsigned int BUSWIDTH=64, unsigned int ADDRWIDTH=32, unsigned int WIDWIDTH=4, unsigned int RIDWIDTH=4,
         int SIZEWIDTH=amba::log2_amba<(BUSWIDTH/8)>::value, bool FORCE_BIGUINT=false>
class AXI_Master_RTL_LT_Adapter: public sc_core::sc_module
#ifdef CARBON_DEBUG_ACCESS
                               , public CarbonDebugAccessIF
#endif
{
public:
  sc_core::sc_in_clk m_clk;
  sc_core::sc_in<bool> m_Reset;

  //ports for signals coming from RTL Master
  //write address channel signals
  sc_core::sc_in<sc_dt::sc_uint<WIDWIDTH> > m_AWID;
  sc_core::sc_in<sc_dt::sc_uint<ADDRWIDTH> > m_AWADDR;
  sc_core::sc_in<sc_dt::sc_uint<4> > m_AWLEN;
  sc_core::sc_in<sc_dt::sc_uint<SIZEWIDTH> > m_AWSIZE;
  sc_core::sc_in<sc_dt::sc_uint<2> > m_AWBURST;
  sc_core::sc_in<sc_dt::sc_uint<2> > m_AWLOCK;
  sc_core::sc_in<sc_dt::sc_uint<4> > m_AWCACHE;
  sc_core::sc_in<sc_dt::sc_uint<3> > m_AWPROT;
  sc_core::sc_in<bool> m_AWVALID;
  sc_core::sc_out<bool> m_AWREADY;
	
  //write data channel signals 
  sc_core::sc_in<sc_dt::sc_uint<WIDWIDTH> > m_WID;
  sc_core::sc_in<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type > m_WDATA;
  sc_core::sc_in<sc_dt::sc_uint<(BUSWIDTH+7)/8> > m_WSTRB;
  sc_core::sc_in<bool> m_WLAST;
  sc_core::sc_in<bool> m_WVALID;
  sc_core::sc_out<bool> m_WREADY;
	  
  //write channel response
  sc_core::sc_out<sc_dt::sc_uint<WIDWIDTH> > m_BID;
  sc_core::sc_out<sc_dt::sc_uint<2> > m_BRESP;
  sc_core::sc_out<bool> m_BVALID;
  sc_core::sc_in<bool> m_BREADY;
	
  //Read address 
  sc_core::sc_in<sc_dt::sc_uint<RIDWIDTH> > m_ARID;
  sc_core::sc_in<sc_dt::sc_uint<ADDRWIDTH> > m_ARADDR;
  sc_core::sc_in<sc_dt::sc_uint<4> > m_ARLEN;
  sc_core::sc_in<sc_dt::sc_uint<SIZEWIDTH> > m_ARSIZE;
  sc_core::sc_in<sc_dt::sc_uint<2> > m_ARBURST;
  sc_core::sc_in<sc_dt::sc_uint<2> > m_ARLOCK;
  sc_core::sc_in<sc_dt::sc_uint<4> > m_ARCACHE;
  sc_core::sc_in<sc_dt::sc_uint<3> > m_ARPROT;
  sc_core::sc_in<bool> m_ARVALID;
  sc_core::sc_out<bool> m_ARREADY;
	
  //Read Data ports
  sc_core::sc_out<sc_dt::sc_uint<RIDWIDTH> > m_RID;
  sc_core::sc_out<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT) >::uint_type > m_RDATA;
  sc_core::sc_out<sc_dt::sc_uint<2> > m_RRESP;
  sc_core::sc_out<bool> m_RLAST;
  sc_core::sc_out<bool> m_RVALID;
  sc_core::sc_in<bool> m_RREADY;
	
  //TLM2 based Amba initiator socket
  amba::amba_master_socket<BUSWIDTH> master_sock;

  // Timing APIs - pass on to the RTL LT Adapter
  void set_master_timing(gs::socket::timing_info &tm)
  {
    axi_rtl_ct_adapter.set_master_timing(tm);
  }
  void set_master_timing(tlm::tlm_phase phase, const sc_core::sc_time& tm)
  {
    axi_rtl_ct_adapter.set_master_timing(phase, tm);
  }
  void register_slave_timing_listener( MODULE* mod,void (MODULE::*fn)(gs::socket::timing_info))
  {
    axi_rtl_ct_adapter.register_slave_timing_listener(mod, fn);
  }

#ifdef CARBON_DEBUG_ACCESS
  // Carbon debug access - pass to the RTL CT Adapter
  CarbonDebugAccessStatus debugMemRead(uint64_t addr, uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0)
  {
    return axi_rtl_ct_adapter.debugMemRead(addr, buf, numBytes, ctrl);
  }

  CarbonDebugAccessStatus debugMemWrite (uint64_t addr, const uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0)
  {
    return axi_rtl_ct_adapter.debugMemWrite(addr, buf, numBytes, ctrl);
  }
#endif

  //! API to get and activate the monitor on the LT socket
  gs::socket::monitor<BUSWIDTH>* getMonitor(void)
  {
    lt_monitor->activated = true;
    return lt_monitor;
  }

  //! Constructor
  SC_HAS_PROCESS(AXI_Master_RTL_LT_Adapter);
  AXI_Master_RTL_LT_Adapter(sc_core::sc_module_name nm) : 
    sc_module(nm),
    master_sock("axi_master_socket", amba::amba_AXI, amba::amba_LT, false),
    axi_rtl_ct_adapter("axi_rtl_ct_adapter"),
    ct_lt_adapter("ct_lt_adapter", amba::amba_AXI)
  {
    // Bind the RTL pins from this adapter to the CT adapter
    axi_rtl_ct_adapter.m_clk(m_clk);
    axi_rtl_ct_adapter.m_Reset(m_Reset);
    axi_rtl_ct_adapter.m_ARADDR(m_ARADDR);
    axi_rtl_ct_adapter.m_ARBURST(m_ARBURST);
    axi_rtl_ct_adapter.m_ARCACHE(m_ARCACHE);
    axi_rtl_ct_adapter.m_ARID(m_ARID);
    axi_rtl_ct_adapter.m_ARLEN(m_ARLEN);
    axi_rtl_ct_adapter.m_ARLOCK(m_ARLOCK);
    axi_rtl_ct_adapter.m_ARPROT(m_ARPROT);
    axi_rtl_ct_adapter.m_ARSIZE(m_ARSIZE);
    axi_rtl_ct_adapter.m_ARVALID(m_ARVALID);
    axi_rtl_ct_adapter.m_AWADDR(m_AWADDR);
    axi_rtl_ct_adapter.m_AWBURST(m_AWBURST);
    axi_rtl_ct_adapter.m_AWCACHE(m_AWCACHE);
    axi_rtl_ct_adapter.m_AWID(m_AWID);
    axi_rtl_ct_adapter.m_AWLEN(m_AWLEN);
    axi_rtl_ct_adapter.m_AWLOCK(m_AWLOCK);
    axi_rtl_ct_adapter.m_AWPROT(m_AWPROT);
    axi_rtl_ct_adapter.m_AWSIZE(m_AWSIZE);
    axi_rtl_ct_adapter.m_AWVALID(m_AWVALID);
    axi_rtl_ct_adapter.m_BREADY(m_BREADY);
    axi_rtl_ct_adapter.m_RREADY(m_RREADY);
    axi_rtl_ct_adapter.m_WDATA(m_WDATA);
    axi_rtl_ct_adapter.m_WID(m_WID);
    axi_rtl_ct_adapter.m_WLAST(m_WLAST);
    axi_rtl_ct_adapter.m_WSTRB(m_WSTRB);
    axi_rtl_ct_adapter.m_WVALID(m_WVALID);
    axi_rtl_ct_adapter.m_ARREADY(m_ARREADY);
    axi_rtl_ct_adapter.m_AWREADY(m_AWREADY);
    axi_rtl_ct_adapter.m_BID(m_BID);
    axi_rtl_ct_adapter.m_BRESP(m_BRESP);
    axi_rtl_ct_adapter.m_BVALID(m_BVALID);
    axi_rtl_ct_adapter.m_RDATA(m_RDATA);
    axi_rtl_ct_adapter.m_RID(m_RID);
    axi_rtl_ct_adapter.m_RLAST(m_RLAST);
    axi_rtl_ct_adapter.m_RRESP(m_RRESP);
    axi_rtl_ct_adapter.m_RVALID(m_RVALID);
    axi_rtl_ct_adapter.m_WREADY(m_WREADY);

    // Connect the clock to the AMBA CT LT adapter
    ct_lt_adapter.clk(m_clk);

    // Connect the axi RTL CT socket to the ct_lt_adapter socket
    axi_rtl_ct_adapter.master_sock(ct_lt_adapter.slave_sock);

    // Bind our socket to the CT LT Adapter.  We create a monitor but de-activate it by default
    lt_monitor = gs::socket::connect_with_monitor<BUSWIDTH, tlm::tlm_base_protocol_types>(ct_lt_adapter.master_sock, master_sock);
    lt_monitor->activated = false;
  }


private:
  //! AXI RTL CT Adapter
  AXI_Master_RTL_CT_Adapter<MODULE, BUSWIDTH, ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT> axi_rtl_ct_adapter;

  //! CT to LT Adapter
  amba::AMBA_CT_LT_Adapter<BUSWIDTH> ct_lt_adapter;

  //! The monitor for activity detection
  gs::socket::monitor<BUSWIDTH>* lt_monitor;
}; // #endif

#endif // _AXI_MASTER_RTL_LT_ADAPTER_H_
