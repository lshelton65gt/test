// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _AHB_MASTER_RTL_PV_ADAPTER_H_
#define _AHB_MASTER_RTL_PV_ADAPTER_H_

#include "xactors/tlm2/AHB_Master_RTL_LT_Adapter.h"

#include "adapters/AMBA_LT_AT_PV_Adapter.h"

#include "greensocket/monitor/green_socket_monitor.h"

template<unsigned int BUSWIDTH, typename MODULE,bool FORCE_BIGUINT=false, bool IS_LITE=false>
class AHB_Master_RTL_PV_Adapter: public sc_core::sc_module
#ifdef CARBON_DEBUG_ACCESS
                               , public CarbonDebugAccessIF
#endif
{
public:
  // RTL ports
  sc_core::sc_in_clk m_clk;
  sc_core::sc_in<bool> m_Reset;
  typename amba::port_enabler<!IS_LITE>::input_port_type m_HBUSREQ;
  sc_core::sc_in<bool> m_HLOCK;
  sc_core::sc_in<sc_dt::sc_uint<2> > m_HTRANS;
  sc_core::sc_in<sc_dt::sc_uint<32> >m_HADDR;
  sc_core::sc_in<sc_dt::sc_uint<3> > m_HSIZE;
  sc_core::sc_in<sc_dt::sc_uint<3> > m_HBURST;
  sc_core::sc_in<sc_dt::sc_uint<4> > m_HPROT;
  sc_core::sc_in<bool> m_HWRITE;
  sc_core::sc_in<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type > m_HWDATA;
  sc_core::sc_out<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type > m_HRDATA;
  sc_core::sc_out<typename amba::port_enabler<!IS_LITE>::resp_type > m_HRESP;
  typename amba::port_enabler<!IS_LITE>::output_port_type m_HGRANT;
  sc_core::sc_out<bool> m_HREADYIN;

  //TLM2 based Amba initiator socket
  tlm::tlm_initiator_socket<BUSWIDTH, amba_pv::amba_pv_protocol_types> master_sock;

  // Timing APIs - pass on to the RTL LT Adapter
  void set_master_timing(gs::socket::timing_info &tm)
  {
    ahb_rtl_lt_adapter.set_master_timing(tm);
  }
  void set_master_timing(tlm::tlm_phase phase, const sc_core::sc_time& tm)
  {
    ahb_rtl_lt_adapter.set_master_timing(phase, tm);
  }
  void register_slave_timing_listener( MODULE* mod,void (MODULE::*fn)(gs::socket::timing_info))
  {
    ahb_rtl_lt_adapter.register_slave_timing_listener(mod, fn);
  }

#ifdef CARBON_DEBUG_ACCESS
  // Carbon debug access - pass to the RTL LT Adapter
  CarbonDebugAccessStatus debugMemRead(uint64_t addr, uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0)
  {
    return ahb_rtl_lt_adapter.debugMemRead(addr, buf, numBytes, ctrl);
  }

  CarbonDebugAccessStatus debugMemWrite (uint64_t addr, const uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0)
  {
    return ahb_rtl_lt_adapter.debugMemWrite(addr, buf, numBytes, ctrl);
  }
#endif

  //! API to get and activate the monitor on the LT socket
  gs::socket::monitor<BUSWIDTH>* getMonitor(void)
  {
    lt_monitor->activated = true;
    return lt_monitor;
  }

  //! Constructor
  SC_HAS_PROCESS(AHB_Master_RTL_PV_Adapter);
  AHB_Master_RTL_PV_Adapter(sc_core::sc_module_name nm, unsigned int max_mem) : 
    sc_module(nm),
    master_sock("ahb_master_socket"),
    ahb_rtl_lt_adapter("ahb_rtl_lt_adapter", max_mem),
    lt_pv_adapter("lt_pv_adapter")
  {
    // Bind the RTL pins from this adapter to the LT adapter
    ahb_rtl_lt_adapter.m_clk(m_clk);
    ahb_rtl_lt_adapter.m_Reset(m_Reset);
    if (!IS_LITE) {
      ahb_rtl_lt_adapter.m_HBUSREQ(m_HBUSREQ);
    }
    ahb_rtl_lt_adapter.m_HLOCK(m_HLOCK);
    ahb_rtl_lt_adapter.m_HTRANS(m_HTRANS);
    ahb_rtl_lt_adapter.m_HADDR(m_HADDR);
    ahb_rtl_lt_adapter.m_HSIZE(m_HSIZE);
    ahb_rtl_lt_adapter.m_HBURST(m_HBURST);
    ahb_rtl_lt_adapter.m_HPROT(m_HPROT);
    ahb_rtl_lt_adapter.m_HWRITE(m_HWRITE);
    ahb_rtl_lt_adapter.m_HWDATA(m_HWDATA);
    ahb_rtl_lt_adapter.m_HRDATA(m_HRDATA);
    ahb_rtl_lt_adapter.m_HRESP(m_HRESP);
    if (!IS_LITE) {
      ahb_rtl_lt_adapter.m_HGRANT(m_HGRANT);
    }
    ahb_rtl_lt_adapter.m_HREADYIN(m_HREADYIN);

    // Connect the ahb RTL LT socket to the lt_pv_adapter socket
    lt_monitor = gs::socket::connect_with_monitor<BUSWIDTH, tlm::tlm_base_protocol_types>(ahb_rtl_lt_adapter.master_sock, lt_pv_adapter.slave_socket);
    lt_monitor->activated = false;

    // Bind our socket to the LT PV Adapter.  We create a monitor but de-activate it by default
    lt_pv_adapter.master_socket.bind(master_sock);
  }

private:
  //! AHB RTL LT Adapter
  AHB_Master_RTL_LT_Adapter<BUSWIDTH, MODULE, FORCE_BIGUINT, IS_LITE> ahb_rtl_lt_adapter;

  //! LT to PV Adapter
  amba::AMBA_LT_AT_PV_Adapter<BUSWIDTH> lt_pv_adapter;

  //! The monitor for activity detection
  gs::socket::monitor<BUSWIDTH>* lt_monitor;
};

#endif // _AHB_MASTER_RTL_PV_ADAPTER_H_
