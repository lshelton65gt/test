// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef AXI_CT_RTL_SLAVE_ADAPTER_H_
#define AXI_CT_RTL_SLAVE_ADAPTER_H_
using namespace amba;
template< typename MODULE, unsigned int BUSWIDTH=64,unsigned int ADDRWIDTH=32, unsigned int WIDWIDTH=4, unsigned int RIDWIDTH=4, int SIZEWIDTH=amba::log2_amba<(BUSWIDTH/8)>::value, bool FORCE_BIGUINT=false >
class AXI_CT_RTL_Slave_Adapter: public sc_core::sc_module
{
public:
	

	//TLM port through which we will accept the TLM packet
	amba::amba_slave_socket<BUSWIDTH> slave_sock;
	
	sc_core::sc_in_clk m_clk;
	//systemc ports
        sc_core::sc_signal<bool> m_otherSideReset;
	sc_core::sc_in<bool> m_Reset;
    //ports for signals coming from RTL Master
	
	//write address channel signals
	sc_core::sc_out<sc_dt::sc_uint<WIDWIDTH> > m_AWID;
	sc_core::sc_out<sc_dt::sc_uint<ADDRWIDTH> > m_AWADDR;
	sc_core::sc_out<sc_dt::sc_uint<4> > m_AWLEN;
	sc_core::sc_out<sc_dt::sc_uint<SIZEWIDTH> > m_AWSIZE;
	sc_core::sc_out<sc_dt::sc_uint<2> > m_AWBURST;
	sc_core::sc_out<sc_dt::sc_uint<2> > m_AWLOCK;
	sc_core::sc_out<sc_dt::sc_uint<4> > m_AWCACHE;
	sc_core::sc_out<sc_dt::sc_uint<3> > m_AWPROT;
	sc_core::sc_out<bool> m_AWVALID;
	sc_core::sc_in<bool> m_AWREADY;
	
	//write data channel signals 
	sc_core::sc_out<sc_dt::sc_uint<WIDWIDTH> > m_WID;
	sc_core::sc_out<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT) >::uint_type> m_WDATA;
	sc_core::sc_out<sc_dt::sc_uint<(BUSWIDTH+7)/8> > m_WSTRB;
	sc_core::sc_out<bool> m_WLAST;
	sc_core::sc_out<bool> m_WVALID;
	sc_core::sc_in<bool> m_WREADY;
	  
	//write channel response
	sc_core::sc_in<sc_dt::sc_uint<WIDWIDTH> > m_BID;
	sc_core::sc_in<sc_dt::sc_uint<2> > m_BRESP;
	sc_core::sc_in<bool> m_BVALID;
	sc_core::sc_out<bool> m_BREADY;
	
	//Read address 
	sc_core::sc_out<sc_dt::sc_uint<RIDWIDTH> > m_ARID;
	sc_core::sc_out<sc_dt::sc_uint<ADDRWIDTH> > m_ARADDR;
	sc_core::sc_out<sc_dt::sc_uint<4> > m_ARLEN;
	sc_core::sc_out<sc_dt::sc_uint<SIZEWIDTH> > m_ARSIZE;
	sc_core::sc_out<sc_dt::sc_uint<2> > m_ARBURST;
	sc_core::sc_out<sc_dt::sc_uint<2> > m_ARLOCK;
	sc_core::sc_out<sc_dt::sc_uint<4> > m_ARCACHE;
	sc_core::sc_out<sc_dt::sc_uint<3> > m_ARPROT;
	sc_core::sc_out<bool> m_ARVALID;
	sc_core::sc_in<bool> m_ARREADY;
	
	 //Read Data ports
	 sc_core::sc_in<sc_dt::sc_uint<RIDWIDTH> > m_RID;
	 sc_core::sc_in<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT) >::uint_type > m_RDATA;
	 sc_core::sc_in<sc_dt::sc_uint<2> > m_RRESP;
	 sc_core::sc_in<bool> m_RLAST;
	 sc_core::sc_in<bool> m_RVALID;
	 sc_core::sc_out<bool> m_RREADY;
	 //timing related APIs
	 void set_slave_timing(gs::socket::timing_info &);
     void set_slave_timing(tlm::tlm_phase, const sc_core::sc_time&);
	 gs::socket::timing_info & get_slave_timing();
	 sc_core::sc_time & get_slave_timing(tlm::tlm_phase);
	 void register_master_timing_listener(MODULE* mod,void (MODULE::*fn)(gs::socket::timing_info))
	 {
		 owner= mod;
		 timing_cb=fn;
	 }
	 
	 void master_timing_listener(gs::socket::timing_info);
	 void end_of_elaboration();
	 
	 //methods
	 void WriteDataReady();
	 void WriteRespValid();
	 void ReadAddrReady();
	 void WriteAddrReady();
	 void ReadDataValid();
	 void clock_tick();
	 
	 void WriteAddrValid();
	 void WriteDataValid();
	 void ReadAddrValid();

	 void ReadDataReady();
	 void WriteResponseReady();
	 void end_data_method();
	 void begin_resp_method();
	 void end_req_method();


	 ///APIs for Carbon Debug
#ifdef CARBON_DEBUG_ACCESS     
	 void registerDebugAccessCB(CarbonDebugAccessIF*);
#endif
	 unsigned int transport_debug(tlm::tlm_generic_payload & trans);



	 //forward Transport API 
	 tlm::tlm_sync_enum nb_fw_transport(tlm::tlm_generic_payload & trans, tlm::tlm_phase & ph, sc_core::sc_time &delay);
	 
	 SC_HAS_PROCESS(AXI_CT_RTL_Slave_Adapter);
	 AXI_CT_RTL_Slave_Adapter(sc_core::sc_module_name nm):sc_core::sc_module(nm),
	 slave_sock("Slave_socket",amba::amba_AXI, amba::amba_CT,false),
	 m_clk("m_clk"),
	 m_Reset("AXI_RESET"),
	 m_AWID("AXI_AWID"),
	 m_AWADDR("AXI_AWADDR"),
	 m_AWLEN("AXI_AWLEN"),
	 m_AWSIZE("AXI_AWSIZE"),
	 m_AWBURST("AXI_AWBURST"),
	 m_AWLOCK("AXI_AWLOCK"),
	 m_AWCACHE("AXI_AWCACHE"),
	 m_AWPROT("AXI_AWPROT"),
	 m_AWVALID("AXI_AWVALID"),
	 m_AWREADY("AXI_AWREADY"),
	 m_WID("AXI_WID"),
	 m_WDATA("AXI_WDATA"),
	 m_WSTRB("AXI_WSTRB"),
	 m_WLAST("AXI_WLAST"),
	 m_WVALID("AXI_WVALID"),
	 m_WREADY("AXI_WREADY"),
	 m_BID("AXI_BID"),
	 m_BRESP("AXI_BRESP"),
	 m_BVALID("AXI_BVALID"),
	 m_BREADY("AXI_BREADY"),
	 m_ARID("AXI_ARID"),
	 m_ARADDR("AXI_ARADDR"),
	 m_ARLEN("AXI_ARLEN"),
	 m_ARSIZE("AXI_ARSIZE"),
	 m_ARBURST("AXI_ARBURST"),
	 m_ARLOCK("AXI_ARLOCK"),
	 m_ARCACHE("AXI_ARCACHE"),
	 m_ARPROT("AXI_ARPROT"),
	 m_ARVALID("AXI_ARVALID"),
	 m_ARREADY("AXI_ARREADY"),
     m_RID("AXI_RID"),
	 m_RDATA("AXI_RDATA"),
	 m_RRESP("AXI_RRESP"),
	 m_RLAST("AXI_RLAST"),
	 m_RVALID("AXI_RVALID"),
	 m_RREADY("AXI_RREADY"),
	 lock_counter(0),
	 unlock_object(NULL),
	 w_addr_ready_pending_id(0),
	 w_data_ready_pending_id(0),
	 r_addr_ready_pending_id(0),
	 last_resp(false),
	 w_address_accepted(false),
	 w_data_accepted(false),
	 r_address_accepted(false),
	 w_address_valid(false),
	 w_data_valid(false),
	 r_address_valid(false),
	 r_valid(false),
	 b_valid(false),
	 w_resp_done(false),
	 read_done(true),
	 write_last(false),
	 m_InReset(false),
	 w_bytesTransferred(0),
	 r_bytesTransferred(0),
	 timing_cb(NULL),
	 owner(NULL),
#ifdef CARBON_DEBUG_ACCESS     
	 debugIf(NULL),
#endif
	 time_unit(sc_core::sc_get_time_resolution()),
	 ereq_sample_time(sc_core::sc_get_time_resolution()),
	 edata_sample_time(sc_core::sc_get_time_resolution()),
	 bresp_sample_time(sc_core::sc_get_time_resolution()),
	 end_req_time(sc_core::SC_ZERO_TIME),
	 end_data_time(sc_core::SC_ZERO_TIME)
	 {
            SC_METHOD(ResetMethod);
            sensitive << m_Reset;
            dont_initialize();

		SC_METHOD(WriteAddrValid);
        sensitive << waddr_valid_ev;
        dont_initialize();

        SC_METHOD(ReadAddrValid);
        sensitive << raddr_valid_ev;
        dont_initialize();

        SC_METHOD(WriteDataValid);
        sensitive<< wdata_valid_ev;
        dont_initialize();

        SC_METHOD(ReadDataReady);
        sensitive<< rdata_ready_ev;
        dont_initialize();

        SC_METHOD(WriteResponseReady);
        sensitive<<wresp_ready_ev;
        dont_initialize();

        SC_METHOD(clock_tick);
        sensitive << m_clk.pos();
        dont_initialize();
        read_done=true;
        
        SC_METHOD(end_req_method);
        sensitive<< end_req_ev;
        dont_initialize();

        SC_METHOD(begin_resp_method);
        sensitive<< begin_resp_ev;
        dont_initialize();

        SC_METHOD(end_data_method);
        sensitive<< end_data_ev;
        dont_initialize();

		slave_sock.template register_nb_transport_fw< AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH,SIZEWIDTH,FORCE_BIGUINT> >(this, & AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH,SIZEWIDTH, FORCE_BIGUINT>::nb_fw_transport);
		slave_sock.template register_transport_dbg< AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH,SIZEWIDTH,FORCE_BIGUINT> >(this, &AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH,SIZEWIDTH,FORCE_BIGUINT>::transport_debug);
        slave_sock.set_timing_listener_callback(this, & AXI_CT_RTL_Slave_Adapter::master_timing_listener);
        m_burst_size.value=(BUSWIDTH+7)/8;
        m_default_id.value=0;
	 }
	 virtual ~AXI_CT_RTL_Slave_Adapter();
private:
        void ResetMethod ();
	tlm::tlm_generic_payload * m_reset_trans;
	unsigned int lock_counter;
	amba::lock_object_base* unlock_object;
	unsigned int w_addr_ready_pending_id;
	unsigned int w_data_ready_pending_id;
	unsigned int r_addr_ready_pending_id;

        amba::amba_burst_size m_burst_size;
        amba::amba_id m_default_id;
	/*
	* So we will buffer up the transcation and the amount of data it has written/read
	* we dont need to buffer the locked_txn_counter separately for each transaction.
	*/
	struct payload_info
	{
		tlm::tlm_generic_payload *trans;
		unsigned int data_counter;
	};
	std::map<unsigned int,std::deque<payload_info> > write_payload_list;
	std::map<unsigned int,std::deque<payload_info> > read_payload_list;
	bool last_resp;
	bool w_address_accepted;
	bool w_data_accepted;
	bool r_address_accepted;
	bool w_address_valid;
	bool w_data_valid;
	bool r_address_valid;
	bool r_valid;
	bool b_valid;
	bool w_resp_done;
	bool read_done;
	bool write_last;
	bool m_InReset;
	sc_core::sc_event waddr_valid_ev;
	sc_core::sc_event raddr_valid_ev;
	sc_core::sc_event wdata_valid_ev;
	sc_core::sc_event rdata_ready_ev;
	sc_core::sc_event wresp_ready_ev;
	sc_core::sc_event end_req_ev;
	sc_core::sc_event end_data_ev;
	sc_core::sc_event begin_resp_ev;
	unsigned int w_bytesTransferred;
	unsigned int r_bytesTransferred;

	void (MODULE::*timing_cb)(gs::socket::timing_info) ;
	MODULE* owner;
	gs::socket::timing_info slave_timing_info;
	//pointer for the debug access
#ifdef CARBON_DEBUG_ACCESS    
	CarbonDebugAccessIF * debugIf;
#endif
	//total bytes transferred for read/write
	sc_core::sc_time time_unit;
	sc_core::sc_time ereq_sample_time;
	sc_core::sc_time edata_sample_time;
	sc_core::sc_time bresp_sample_time;
	sc_core::sc_time end_req_time;
	sc_core::sc_time end_data_time;

};

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::~AXI_CT_RTL_Slave_Adapter()
{
}

/*
 * @brief this fuction is triggered when the slave writes to its
 * Reset output
 * 
 */

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::ResetMethod()
{
  tlm::tlm_phase ph;
  tlm::tlm_sync_enum retval;
  sc_core::sc_time delay = sc_core::SC_ZERO_TIME;
  if (m_reset_trans == NULL)
    m_reset_trans = slave_sock.get_reset_txn();

  if (m_Reset.read()) {
    m_InReset = false;
    ph = amba::RESET_DEASSERT;
  }
  else { //ACTIVE LOW
    m_InReset = true;
    b_valid = w_resp_done = r_valid = w_address_accepted = r_address_accepted = w_data_accepted = false;
    w_address_valid = r_address_valid = w_data_valid = write_last = false;
    read_done = true;
    end_req_ev.cancel();
    end_data_ev.cancel();
    begin_resp_ev.cancel();
    write_payload_list.clear();
    read_payload_list.clear();
    ph = amba::RESET_ASSERT;
  }
  retval = slave_sock->nb_transport_bw(*m_reset_trans, ph, delay);
  assert(retval== tlm::TLM_ACCEPTED && "Wrong tlm_sync_enum returned");
}

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter< MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT >::end_of_elaboration()
{
	
	slave_sock.activate_synchronization_protection();
	sc_core::sc_time delay= sc_core::sc_get_time_resolution();
    //guard against overriding already calculated stuff.
    if (slave_timing_info.get_start_time(tlm::BEGIN_REQ)<delay) slave_timing_info.set_start_time(tlm::BEGIN_REQ,delay);
	if (slave_timing_info.get_start_time(tlm::END_REQ)<delay)	 slave_timing_info.set_start_time(tlm::END_REQ,delay);
	if (slave_timing_info.get_start_time(amba::BEGIN_DATA)<delay)	 slave_timing_info.set_start_time(amba::BEGIN_DATA,delay);
	if (slave_timing_info.get_start_time(amba::BEGIN_LAST_DATA)<delay)	 slave_timing_info.set_start_time(amba::BEGIN_LAST_DATA,delay);
	if (slave_timing_info.get_start_time(amba::END_DATA)<delay)	 slave_timing_info.set_start_time(amba::END_DATA,delay);
	if (slave_timing_info.get_start_time(tlm::BEGIN_RESP)<delay)	 slave_timing_info.set_start_time(tlm::BEGIN_RESP,delay);
	if (slave_timing_info.get_start_time(amba::BEGIN_LAST_RESP)<delay)	 slave_timing_info.set_start_time(amba::BEGIN_LAST_RESP,delay);
	if (slave_timing_info.get_start_time(tlm::END_RESP)<delay)	 slave_timing_info.set_start_time(tlm::END_RESP,delay);
    bresp_sample_time= slave_timing_info.get_start_time(tlm::BEGIN_RESP);
    edata_sample_time = slave_timing_info.get_start_time(amba::END_DATA);
    ereq_sample_time = slave_timing_info.get_start_time(tlm::END_REQ);    
    slave_sock.set_target_timing(slave_timing_info);
	if(timing_cb!=NULL) (owner->*timing_cb)(slave_timing_info);
}

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::master_timing_listener(gs::socket::timing_info info)
{
	AMBA_DUMP("Master timing listener called");
	bool do_cb=false;
	if(info.get_start_time(tlm::BEGIN_REQ)+time_unit > slave_timing_info.get_start_time(tlm::BEGIN_REQ))
	{
		do_cb=true;
		slave_timing_info.set_start_time(tlm::BEGIN_REQ,info.get_start_time(tlm::BEGIN_REQ)+time_unit);
	}
	if(info.get_start_time(tlm::END_RESP)+time_unit > slave_timing_info.get_start_time(tlm::END_RESP))
	{
		do_cb=true;
		slave_timing_info.set_start_time(tlm::END_RESP,info.get_start_time(tlm::END_RESP)+time_unit);
	}
	if(info.get_start_time(amba::BEGIN_DATA)+time_unit > slave_timing_info.get_start_time(amba::BEGIN_DATA))
	{
		do_cb=true;
		slave_timing_info.set_start_time(amba::BEGIN_DATA,info.get_start_time(amba::BEGIN_DATA)+time_unit);
	}
	if(info.get_start_time(amba::BEGIN_LAST_DATA)+time_unit > slave_timing_info.get_start_time(amba::BEGIN_LAST_DATA))
	{
			do_cb=true;
			slave_timing_info.set_start_time(amba::BEGIN_LAST_DATA,info.get_start_time(amba::BEGIN_LAST_DATA)+time_unit);
	}
    
    bool inform_caller=false;
	if(slave_timing_info.get_start_time(tlm::BEGIN_REQ) > slave_timing_info.get_start_time(tlm::END_REQ))
	{
		do_cb=true;
        inform_caller=true;
		slave_timing_info.set_start_time(tlm::END_REQ,slave_timing_info.get_start_time(tlm::BEGIN_REQ));
	}
	if(slave_timing_info.get_start_time(amba::BEGIN_DATA) > slave_timing_info.get_start_time(amba::END_DATA))
	{
		do_cb=true;
        inform_caller=true;
		slave_timing_info.set_start_time(amba::END_DATA,slave_timing_info.get_start_time(amba::BEGIN_DATA));

	}
	if(slave_timing_info.get_start_time(amba::BEGIN_LAST_DATA) > slave_timing_info.get_start_time(amba::END_DATA))
	{
		do_cb=true;
        inform_caller=true;
		slave_timing_info.set_start_time(amba::END_DATA,slave_timing_info.get_start_time(amba::BEGIN_LAST_DATA));

	}
    if (inform_caller) slave_sock.set_target_timing(slave_timing_info); //we changed EREQ or EDATA start
      
	if(timing_cb!=NULL && do_cb)
		(owner->*timing_cb)(slave_timing_info);
}

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
unsigned int AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::transport_debug(tlm::tlm_generic_payload& payload)
{
#ifdef CARBON_DEBUG_ACCESS
	unsigned long long address = payload.get_address();
	unsigned int length=payload.get_data_length();
	unsigned int * ctrl = new unsigned int();
	if(slave_sock.template get_extension<amba::amba_secure_access>(payload))
       	ctrl[0]=0x01;
    else
    	ctrl[0]=0x00;
    CarbonDebugAccessStatus retval;
	if(payload.get_command()==tlm::TLM_READ_COMMAND)
	{
       retval= debugIf->debugMemRead(address,(payload.get_data_ptr()),length,ctrl);
	}
	else if(payload.get_command()==tlm::TLM_WRITE_COMMAND)
	{
		retval= debugIf->debugMemWrite(address,(payload.get_data_ptr()),length,ctrl);
	}
	unsigned int transferred_bytes=0;
	switch(retval)
	{
		case eCarbonDebugAccessStatusOutRange:
		{
			payload.set_response_status(tlm::TLM_ADDRESS_ERROR_RESPONSE);
			break;
		}
		case eCarbonDebugAccessStatusOk:{
			payload.set_response_status(tlm::TLM_OK_RESPONSE);
			transferred_bytes=length;
			break;
		}
		case eCarbonDebugAccessStatusError:{
			payload.set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
			break;
		}
		case eCarbonDebugAccessStatusPartial:{
			//We should return the number of bytes transferred
			break;
		}
		default:
			assert("Invalid DebugAccess return value");
	}
	delete(ctrl);
	return transferred_bytes;
#else
    return 0;
#endif
}

#ifdef CARBON_DEBUG_ACCESS
template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::registerDebugAccessCB(CarbonDebugAccessIF* dbgIf)
{
 debugIf=dbgIf;
}
#endif

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::set_slave_timing(tlm::tlm_phase ph, const sc_core::sc_time& delay)
{
  bool changed=false;
  if (ph==tlm::BEGIN_RESP || ph==amba::BEGIN_LAST_RESP){
    if ((delay+time_unit)>slave_timing_info.get_start_time(tlm::BEGIN_RESP)){
      slave_timing_info.set_start_time(tlm::BEGIN_RESP,(delay+time_unit) );
      changed=true;
    }
    if ((delay+time_unit)>slave_timing_info.get_start_time(amba::BEGIN_LAST_RESP)){
      slave_timing_info.set_start_time(amba::BEGIN_LAST_RESP,(delay+time_unit) );
      changed=true;
    }
  }
  else
  if (ph==tlm::END_REQ || ph==amba::END_DATA){
    if ((delay+time_unit)>slave_timing_info.get_start_time(ph)){
      slave_timing_info.set_start_time(ph,(delay+time_unit) );
      changed=true;
    }    
  }
  if(changed=true)
  {
      bresp_sample_time= slave_timing_info.get_start_time(tlm::BEGIN_RESP);
      edata_sample_time = slave_timing_info.get_start_time(amba::END_DATA);
      ereq_sample_time = slave_timing_info.get_start_time(tlm::END_REQ);
      slave_sock.set_target_timing(slave_timing_info);
  }
  if(slave_timing_info.get_start_time(tlm::BEGIN_RESP) > slave_timing_info.get_start_time(tlm::END_RESP))
  {
    slave_timing_info.set_start_time(tlm::END_RESP, slave_timing_info.get_start_time(tlm::BEGIN_RESP));
    if(timing_cb!=NULL) (owner->*timing_cb)(slave_timing_info);
  }  
}

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::set_slave_timing(gs::socket::timing_info& info)
{
	bool changed =false;
        //make sure we use the max of bresp and blastresp
        sc_core::sc_time bresptime=info.get_start_time(tlm::BEGIN_RESP);
        if (bresptime<info.get_start_time(amba::BEGIN_LAST_RESP)) bresptime=info.get_start_time(amba::BEGIN_LAST_RESP);
        
		if( (bresptime+time_unit) > slave_timing_info.get_start_time(tlm::BEGIN_RESP))
	    {
			slave_timing_info.set_start_time(tlm::BEGIN_RESP,(bresptime+time_unit) );
			changed=true;
	    }
        
		if( (bresptime+time_unit) > slave_timing_info.get_start_time(amba::BEGIN_LAST_RESP))
		{
			slave_timing_info.set_start_time(amba::BEGIN_LAST_RESP,(bresptime+time_unit) );
			changed=true;
		}
		if((info.get_start_time(tlm::END_REQ)+time_unit) > slave_timing_info.get_start_time(tlm::END_REQ))
		{
			slave_timing_info.set_start_time(tlm::END_REQ,(info.get_start_time(tlm::END_REQ)+time_unit));
			changed=true;

		}
		if((info.get_start_time(amba::END_DATA)+time_unit) > slave_timing_info.get_start_time(amba::END_DATA))
		{
			slave_timing_info.set_start_time(amba::END_DATA, (info.get_start_time(amba::END_DATA)+time_unit));
			changed =true;
		}
		if(changed=true)
		{
			bresp_sample_time= slave_timing_info.get_start_time(tlm::BEGIN_RESP);
			edata_sample_time = slave_timing_info.get_start_time(amba::END_DATA);
			ereq_sample_time = slave_timing_info.get_start_time(tlm::END_REQ);
			slave_sock.set_target_timing(slave_timing_info);
		}
		if(slave_timing_info.get_start_time(tlm::BEGIN_RESP) > slave_timing_info.get_start_time(tlm::END_RESP))
		{
			slave_timing_info.set_start_time(tlm::END_RESP, slave_timing_info.get_start_time(tlm::BEGIN_RESP));
            if(timing_cb!=NULL) (owner->*timing_cb)(slave_timing_info);
		}
}



template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
gs::socket::timing_info& AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::get_slave_timing()
{
	return slave_timing_info;
}

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
sc_core::sc_time& AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::get_slave_timing(tlm::tlm_phase ph)
{
	return (slave_timing_info.get_start_time(ph));
}



template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::clock_tick()
{
    if (m_InReset) return;

    if(b_valid && w_resp_done)
    {
    	b_valid=false;
    	w_resp_done=false;
       // wresp_ready_ev.notify();
    }
    if(r_valid && read_done)
    {
       r_valid=false;
       read_done=false;
     //  rdata_ready_ev.notify();
    }

    if(w_address_accepted)
	{
		w_address_valid=false;
		w_address_accepted=false;
		//either AWVALID or ARVALID will be high, so lower them after a cycle
		waddr_valid_ev.notify();
	}
	if(r_address_accepted)
	{
		r_address_valid=false;
		r_address_accepted=false;
		raddr_valid_ev.notify();
	}
	if(w_data_accepted)
	{
	    AMBA_DUMP("WDATA accepted so setting WDATA_VALID low. ");
	    //lower the valid signal after a cycle as it has been accepted
		w_data_accepted=false;
		w_data_valid=false;
		write_last=false;
		wdata_valid_ev.notify();
	}

	end_req_ev.notify(ereq_sample_time);
    end_data_ev.notify(edata_sample_time);
    begin_resp_ev.notify(bresp_sample_time);

    end_req_time= sc_core::sc_time_stamp()+ereq_sample_time;
    end_data_time=sc_core::sc_time_stamp()+edata_sample_time;

}


template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::begin_resp_method()
{
	 if(m_RVALID.read()==true)
	 {
		 if(r_valid==false)
		 {
			 r_valid=true;
			 ReadDataValid();
		 }
	 }
	 if(m_BVALID.read()==true)
	 {
		if(b_valid==false)
		{
			b_valid=true;
			WriteRespValid();
		}
	 }
}

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::end_data_method()
{
	 if(m_WREADY.read()==true)
	 {
		 WriteDataReady();
	 }
}

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::end_req_method()
{
	  if(m_AWREADY.read()==true)
	 {
		 WriteAddrReady();
	 }
	 if(m_ARREADY.read()==true )
	 {
		 ReadAddrReady();
	 }
}

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
tlm::tlm_sync_enum AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::nb_fw_transport(tlm::tlm_generic_payload & payload, tlm::tlm_phase & phase, sc_core::sc_time & delay)
{
	AMBA_DUMP("Received request from TLM world, phase="<< phase);

        //When in RESET state, process calls on fw interface only when the Phase is RESET_DEASSERT
        if (m_InReset && (phase != amba::RESET_DEASSERT)) return tlm::TLM_ACCEPTED;

	sc_core::sc_time current_time= sc_core::sc_time_stamp();

	///extract the fields from the payload
	amba::amba_id * id;
	if (!slave_sock.template get_extension<amba::amba_id>(id,payload)) {
          id = &m_default_id;
        }

	sc_dt::uint64 address = payload.get_address();

        // Get the burst size from the transaction. If they don't
        // provide one, determine the optimal burst size based on the
        // transaction data length.
	amba::amba_burst_size *size;
	if (!slave_sock.template get_extension<amba::amba_burst_size>(size,payload)) {
          m_burst_size.value = (BUSWIDTH+7)/8;
          if (m_burst_size.value > payload.get_data_length()) {
            m_burst_size.value = payload.get_data_length();
          }
          size=&m_burst_size;
        }

	if(phase== amba::RESET_ASSERT) { //ACTIVE LOW
	        m_InReset = true;
                b_valid = w_resp_done = r_valid = w_address_accepted = r_address_accepted = w_data_accepted = false;
                w_address_valid = r_address_valid = w_data_valid = write_last = false;
                read_done = true;
                end_req_ev.cancel();
                end_data_ev.cancel();
                begin_resp_ev.cancel();
                write_payload_list.clear();
                read_payload_list.clear();
		m_otherSideReset.write(false);
	}
	else if(phase == amba::RESET_DEASSERT) {
	        m_InReset = false;
		m_otherSideReset.write(true);
	}
	else if(phase == tlm::BEGIN_REQ)
	{
		tlm::tlm_command cmd = payload.get_command();
		payload_info info;
		payload.acquire();
 		info.trans=&payload;
		info.data_counter=0;
		//Store this transaction for the Data-phase usage

		if(payload.is_read())
			read_payload_list[id->value].push_back(info);
		else if(payload.is_write())
			write_payload_list[id->value].push_back(info);
        unsigned int burst_size=0;
		unsigned int temp= size->value;
        unsigned int offset=payload.get_address()%(size->value); //unaligned offset
		unsigned int i=0;
		for(i=0;;i++)
		{
			temp=temp/2;
			if(temp==0)
				break;
		}
		burst_size=i;

		unsigned int datalen= payload.get_data_length();
		unsigned int burstlen= ((datalen+offset+(size->value)-1)/size->value)-1;
		//if ((size->value+offset)>((BUSWIDTH+7)/8)) burstlen++; //first transfer is only partially

		unsigned char burst_type=1; //incrementing-burst bydefault
		amba::amba_wrap_offset *wrap_ext=NULL;
		unsigned int req_start_index=0;
		if(payload.get_streaming_width()< datalen)
		{///it is fixed type burst
			burst_type=0;
		}
		else if( slave_sock.template get_extension<amba::amba_wrap_offset>(wrap_ext,payload))
		{
			AMBA_DUMP("Wrapping bursts");
			req_start_index=wrap_ext->value;
			address+=req_start_index;
			burst_type=2;
		}
		//lock extension
		amba::amba_lock * lock_ext;
		unsigned char lock=0;
		if(slave_sock.template get_extension<amba::amba_lock>(lock_ext,payload))
		{
			lock_ext->value->lock_is_understood_by_slave=true;
			lock_counter++;
			lock |= 0x02; //set the 2nd bit
			if(lock_counter== lock_ext->value->number_of_txns)
			{//Ok so this is the last unlocking transaction
				lock &= 0xfd;// clear the second bit
				//store this txn, so that as soon as it is accepted by the slave, we will release this lock
				unlock_object= lock_ext->value;
				lock_counter=0;
			}
		}

		///check the cache support
		unsigned char cache=0;
		if(slave_sock.template get_extension<amba::amba_bufferable>(payload))
		{
			cache |=0x01;
		}
		if(slave_sock.template get_extension<amba::amba_cacheable>(payload))
		{
			cache |=0x02;//ok we have cacheable_access
			if(slave_sock.template get_extension<amba::amba_read_allocate>(payload))
				cache =cache|0x04;
			if(slave_sock.template get_extension<amba::amba_write_allocate>(payload))
			    cache=cache|0x08;

		}

		///check the protection support
		unsigned char protection=0;
		if(slave_sock.template get_extension<amba::amba_privileged>(payload))
			protection |= 0x01;
		if(slave_sock.template get_extension<amba::amba_instruction>(payload))
		   protection  |=0x04;

		if(slave_sock.template get_extension<amba::amba_secure_access>(payload))
			protection |=0x02;

		amba::amba_exclusive *exclusive;
		if(cmd== tlm::TLM_WRITE_COMMAND)
		{
			if(slave_sock.template get_extension<amba::amba_exclusive>(exclusive,payload))
			{//Ok we have exclusive access too
				lock =lock & 0xfe;
			}
			m_AWID.write(id->value);
			m_AWADDR.write(address);
			m_AWLEN.write(burstlen);
			m_AWSIZE.write(burst_size);
			m_AWBURST.write(burst_type);
			m_AWLOCK.write(lock);
			m_AWCACHE.write(cache);
			m_AWPROT.write(protection);
			  AMBA_DUMP("This is a Write Address request with AWID="<<(unsigned int )id->value<<endl<<
			 "AWADDR="<<(unsigned long long )address<< endl<<
			 "AWLEN= "<<(unsigned int)burstlen <<endl <<
			 "AWSIZE="<<(unsigned int)burst_size<<endl<<
			 "AWBURST="<<(unsigned int)burst_type<<endl<<
			 "AWLOCK="<<(unsigned int)lock<<endl<<
			 "AWCACHE="<<(unsigned int)cache<<endl<<
			 "AWPROT="<<(unsigned int)protection <<endl);


			w_address_valid=true;
			waddr_valid_ev.notify();
			if(current_time >= end_req_time)
			{
				if(m_AWREADY.read()==true)
				{//ok so the slave is ready to accept the request in the same cycle
					//we can safely assume it to be END_REQ
					phase = tlm::END_REQ;

					if(unlock_object!=NULL)
					{
						unlock_object->atomic_txn_completed();
						unlock_object=NULL;
					}
					w_address_accepted=true;
					return tlm::TLM_UPDATED;
				}
				else
				{// buffer this payload for later response
					w_address_accepted=false;
					w_addr_ready_pending_id=id->value;
					return tlm::TLM_ACCEPTED;
				}
			}
			else
			{
				// buffer this payload for later response
				w_address_accepted=false;
				w_addr_ready_pending_id=id->value;
				return tlm::TLM_ACCEPTED;
    		}
		}
		else
		{

			if(slave_sock.template get_extension<amba::amba_exclusive>(exclusive, payload))
			{//Ok we have exclusive access too
				lock =lock |0x01;
			}
			m_ARID.write(id->value);
			m_ARADDR.write(address);
			m_ARLEN.write(burstlen);
			m_ARSIZE.write(burst_size);
			m_ARBURST.write(burst_type);
			m_ARLOCK.write(lock);
			m_ARCACHE.write(cache);
			m_ARPROT.write(protection);
			AMBA_DUMP("This is a read address request with ARID="<<(unsigned int )id->value<<endl<<
				 "ARADDR="<<(unsigned long long )address<< endl<<
				 "ARLEN= "<<(unsigned int)burstlen <<endl <<
				 "ARSIZE="<<(unsigned int)burst_size<<endl<<
				 "ARBURST="<<(unsigned int)burst_type<<endl<<
				 "ARLOCK="<<(unsigned int)lock<<endl<<
				 "ARCACHE="<<(unsigned int)cache<<endl<<
				 "ARPROT="<<(unsigned int)protection <<endl);

			r_address_valid=true;
			raddr_valid_ev.notify();
			if(current_time >= end_req_time)
			{
				if(m_ARREADY.read()==true)
				{
					//ok so the slave will accept it as soon as we validate it
					//we can safely assume the end of request here
					phase = tlm::END_REQ;
					if(unlock_object!=NULL)
					{
						unlock_object->atomic_txn_completed();
						unlock_object=NULL;
					}
					r_address_accepted=true;
					return tlm::TLM_UPDATED;
				}
				else
				{///buffer this payload for later response
					r_address_accepted=false;
					r_addr_ready_pending_id= id->value;
					return tlm::TLM_ACCEPTED;
				}
			}
			else
			{///buffer this payload for later response
				r_address_accepted=false;
				r_addr_ready_pending_id= id->value;
				return tlm::TLM_ACCEPTED;

			}
		}

	}
	else if(phase == amba::BEGIN_DATA ||
			phase == amba::BEGIN_LAST_DATA)
	{//Ok We have received the Data phase of the write transactions
		///extract the fields from the payload
		unsigned int index=0;
		unsigned int write_data_counter=0;
		for(index=0; index < write_payload_list[id->value].size(); index++ )
		{
		  if(((write_payload_list[id->value])[index]).trans == &payload )
		  {//We are considering only that transaction for which data transfer is pending not the one which is waiting for brespValid
			  write_data_counter=write_payload_list[id->value][index].data_counter;
			  break;
		  }
		}
		assert((index !=write_payload_list[id->value].size()) && "No correct txn found in the deque");


		sc_dt::uint64 addr= payload.get_address();
		assert(((payload.get_data_length()+(addr%((BUSWIDTH+7)/8))-write_data_counter) >= size->value) && "Memory allocation error");
		 unsigned int offset=((addr+write_data_counter)%((BUSWIDTH+7)/8));
		 unsigned int array_entry=write_data_counter;
		 amba::amba_wrap_offset * m_wrap_index;
		 if(slave_sock.template get_extension<amba::amba_wrap_offset>(m_wrap_index,payload))
		{
			array_entry+=m_wrap_index->value;
			array_entry%=payload.get_data_length();
		}
		 typename amba::type_chooser<BUSWIDTH,(BUSWIDTH>64 || FORCE_BIGUINT) >::uint_type m_tmp_dt;
		 m_tmp_dt =m_WDATA.read();

        // If they byte enable is NULL, set the strobe to all 1's. Otherwise we compute
        // it from the byte enable in the data loop below
		unsigned char write_strobe= 0;
		 for(w_bytesTransferred=0; (w_bytesTransferred< size->value) && (offset+w_bytesTransferred)<((BUSWIDTH+7)/8) && (w_bytesTransferred+array_entry<payload.get_data_length());  w_bytesTransferred++)
		 {
			 m_tmp_dt.range((w_bytesTransferred+offset+1)*8-1,(w_bytesTransferred+offset)*8)=payload.get_data_ptr()[w_bytesTransferred+array_entry];
             if (payload.get_byte_enable_ptr() && payload.get_byte_enable_length()){
              if(payload.get_byte_enable_ptr()[(w_bytesTransferred+array_entry)%payload.get_byte_enable_length()]==0xff)
                write_strobe |= (1<< w_bytesTransferred);
             }
             else
                write_strobe |= (1<< w_bytesTransferred); //w/o byte enable ptr each transferred byte is enabled
			 AMBA_DUMP("Got "<<(m_tmp_dt.range((w_bytesTransferred+offset+1)*8-1,(w_bytesTransferred+offset)*8).to_uint())<<" from array index "<<(w_bytesTransferred+array_entry));
		}
        AMBA_DUMP("shift write strobes by "<<(offset*8));
        write_strobe<<=offset; //if we are not aligned, move write strobes into correct place
        


		//read the WLAST
		if(phase == amba::BEGIN_LAST_DATA)
			write_last=true;
		else
			write_last=false;


		//Ok now put all the signals on the RTL data channel to the slave
		m_WID.write(id->value);
		m_WDATA.write(m_tmp_dt);
		m_WSTRB.write(write_strobe);

		AMBA_DUMP("WSTRB="<<(unsigned int )write_strobe);

		w_data_valid=true;
		wdata_valid_ev.notify();
		w_data_accepted=false;
		if(current_time >= end_data_time)
		{
			if(m_WREADY.read()==true)
			{
				//ok slave is ready to accept the data validated
				//we can safely assume this txn completed
				w_data_accepted=true;
				write_data_counter+=w_bytesTransferred; //increment the counter by how much more bytes have been copied into ptr
				///buffer the updated transaction
				write_payload_list[id->value][index].data_counter=write_data_counter;
				w_bytesTransferred=0;
				phase = amba::END_DATA;
				return tlm::TLM_UPDATED;
			}
			else
			{
				w_data_accepted=false;
				w_data_ready_pending_id=id->value;
				return tlm::TLM_ACCEPTED;
			}
		}
		else
		{
			w_data_accepted=false;
			w_data_ready_pending_id=id->value;
			return tlm::TLM_ACCEPTED;
		}

	}
	else if(phase== tlm::END_RESP)
	{/// It may be the END_RESP for Write or for Read Data-phase
		if(payload.get_command()== tlm::TLM_WRITE_COMMAND)
		{//Ok so this is the END_RESP of the Write_command
			//intimate the slave

			w_resp_done=true;
			wresp_ready_ev.notify();
			//remove from buffer
			payload.release();
			write_payload_list[id->value].pop_front();
			if(write_payload_list[id->value].empty())
			{
				write_payload_list.erase(id->value);

			}
		}
		else if(payload.get_command()==tlm::TLM_READ_COMMAND)
		{//OK so this END_RESP, we need to intimate the slave through RREADY
			read_done=true;
			unsigned int read_data_counter=read_payload_list[id->value].front().data_counter;
     		read_data_counter += r_bytesTransferred;
			read_payload_list[id->value].front().data_counter= read_data_counter;
			r_bytesTransferred=0;

			if(last_resp)
			{

				payload.release();
				read_payload_list[id->value].pop_front();
				if((read_payload_list[id->value].empty())==true)
				{
					read_payload_list.erase(id->value);
				}
				last_resp=false;
			}
			rdata_ready_ev.notify();

		}

		return tlm::TLM_ACCEPTED;

	}
	else
		assert(0 && "Unknown phase");

	return tlm::TLM_ACCEPTED;
}


template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::WriteAddrValid()
{
	 m_AWVALID.write(w_address_valid);
}

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::WriteAddrReady()
{

	tlm::tlm_generic_payload *trans=NULL;
	tlm::tlm_phase ph;
	tlm::tlm_sync_enum retval;
	sc_core::sc_time delay= sc_core::SC_ZERO_TIME;
	if(m_AWREADY.read()==true)
	{
		if(w_address_valid==true && w_address_accepted==false)
		{
			assert((write_payload_list.find(w_addr_ready_pending_id)!= write_payload_list.end()) &&
					"There isn't any queue for this id");
			assert((write_payload_list[w_addr_ready_pending_id].empty()==false) &&
					"This txn doesn't exist in the queue");

			trans= write_payload_list[w_addr_ready_pending_id].back().trans;
			AMBA_DUMP("recieved AWREADY, send END_REQ for the Write transaction with id="<<w_addr_ready_pending_id<<endl);
			ph= tlm::END_REQ;
			w_address_accepted= true;
			retval=slave_sock->nb_transport_bw(*trans, ph,delay);
			assert( retval==tlm::TLM_ACCEPTED && "Invalid return value");

		}
	}
}

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::WriteDataValid()
{
	m_WVALID.write(w_data_valid);
	m_WLAST.write(write_last);
	AMBA_DUMP( "Written on WVALID, value="<<(unsigned int)w_data_valid<<endl);

}

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::WriteDataReady()
{

	tlm::tlm_generic_payload *trans=NULL;

	tlm::tlm_phase ph;
	tlm::tlm_sync_enum retval;
	sc_core::sc_time delay= sc_core::SC_ZERO_TIME;
	if( m_WREADY.read()==true)
	{
		if(w_data_valid==true && w_data_accepted==false )
		{
			assert((write_payload_list.find(w_data_ready_pending_id)!= write_payload_list.end()) &&
				 (write_payload_list[w_data_ready_pending_id].empty()==false) &&
				 "This txn doesn't exist in the buffer");

			unsigned int index=0;
			for(index=0; index< write_payload_list[w_data_ready_pending_id].size(); index++)
			{//we don't have to consider th txns which have finished but waiting for b_respValid
				trans= write_payload_list[w_data_ready_pending_id][index].trans;

				if(write_payload_list[w_data_ready_pending_id][index].data_counter < (unsigned int) trans->get_data_length() )
				{// ok this is the txn for which we need to send the END_DATA still
					break;
				}

			}
			assert((index!=write_payload_list[w_data_ready_pending_id].size()) && "No valid txn found in deque");
			write_payload_list[w_data_ready_pending_id][index].data_counter+=w_bytesTransferred;
			w_bytesTransferred=0;
			AMBA_DUMP("recieved WREADY, send END_DATA for the Write transaction with id="<<w_data_ready_pending_id<<endl);
			ph= amba::END_DATA;
			w_data_accepted=true;
			retval=slave_sock->nb_transport_bw(*trans, ph,delay);
			assert( retval==tlm::TLM_ACCEPTED && "Invalid return value");

		}
	}
}

//This function schedules wresp_ready_ev, and is called from begin_resp_method. When in
//RESET state begin_resp_method won't be entered hence wresp_ready_ev wont be schduled
//hence WriteResponseReady wont be schduled
template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::WriteRespValid()
{///OK response to the Write transaction has been sent, send it to the Master
	unsigned int id=m_BID.read();
	unsigned int response =m_BRESP.read();
	assert(write_payload_list.find(id)!=write_payload_list.end() && "There isn't any queue for this id");
	assert((write_payload_list[id].empty() ==false) && "There isn't any txn in the queue");
	tlm::tlm_generic_payload * trans = ((write_payload_list[id]).front()).trans;
	assert(trans!=NULL && "transaction doesn't exist in buffer");
	AMBA_DUMP("Received the BVALID signal from RTL world, with BID="<<id<<endl<<"BRESP="<<response<<endl);

	amba::amba_exclusive *exclusive;
	switch(response)
	{
		case 0:{
			trans->set_response_status(tlm::TLM_OK_RESPONSE);
			if(slave_sock.template get_extension<amba::amba_exclusive>(exclusive,*trans))
			{//ok this was an exclusive access which failed
				AMBA_DUMP("Exclusive write access error response");
				exclusive->value=false;			
			}
			break;
		}
		case 1:{
	        trans->set_response_status(tlm::TLM_OK_RESPONSE);
			if(slave_sock.template get_extension<amba::amba_exclusive>(exclusive,*trans))
			{//ok this was an exclusive access which passed
				AMBA_DUMP("Exclusive write access ok response");
				exclusive->value=true;			
			}
			break;		
		}
		case 2:{
			trans->set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);break;
		}
		case 3:{
			trans->set_response_status(tlm::TLM_ADDRESS_ERROR_RESPONSE);break;
		}
		default:
			assert(0 && "Response not supported");
	}
	tlm::tlm_phase ph = tlm::BEGIN_RESP;
	sc_core::sc_time delay = sc_core::SC_ZERO_TIME;
	AMBA_DUMP("Sending the BEGIN_RESP(BVALID) signal");
	tlm::tlm_sync_enum retval=slave_sock->nb_transport_bw(*trans, ph, delay);
	w_resp_done=false;
	switch(retval){
	case tlm::TLM_UPDATED:
	{
		AMBA_DUMP("return value= TLM_UPDATED");
		assert(ph== tlm::END_RESP && "Wrong phase transfered");
		w_resp_done=true;
		trans->release();
		write_payload_list[id].pop_front();
        AMBA_DUMP("releasing the transaction and removing from queue");
        if(write_payload_list[id].empty()==true)
        {
        	AMBA_DUMP("Queue is empty so removing it");
        	write_payload_list.erase(id);
        }
		break;
	}
	case tlm::TLM_ACCEPTED:{
		AMBA_DUMP("return value= TLM_ACCEPTED");
		w_resp_done=false;
        break;
	}
	default:
		assert(0 && "Wrong return value transferred");
	}
	wresp_ready_ev.notify();

}

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::WriteResponseReady()
{
 m_BREADY.write(w_resp_done);
}


template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::ReadAddrValid()
{
 m_ARVALID.write(r_address_valid);

}


template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::ReadAddrReady()
{


	tlm::tlm_generic_payload *trans=NULL;
	tlm::tlm_phase ph;
	tlm::tlm_sync_enum retval;
	sc_core::sc_time delay= sc_core::SC_ZERO_TIME;
    if(m_ARREADY.read()==true)
	{
		if(r_address_valid==true && r_address_accepted==false)
		{
			assert((read_payload_list.find(r_addr_ready_pending_id)!= read_payload_list.end()) &&
					"There isn't any queue for this id");
			assert((read_payload_list[r_addr_ready_pending_id].empty()==false) &&
					"This queue is empty.");
			trans = read_payload_list[r_addr_ready_pending_id].back().trans;
			AMBA_DUMP("recieved ARREADY, send END_REQ for the Read transaction with id="<<r_addr_ready_pending_id);
			ph=tlm::END_REQ;
			r_address_accepted=true;

			retval=slave_sock->nb_transport_bw(*trans, ph,delay);
			assert( retval==tlm::TLM_ACCEPTED && "Invalid return value");

		}
	}
}



//This function schedules rdata_ready_ev, and is called from begin_resp_method. When in
//RESET state begin_resp_method won't be entered hence rdata_ready_ev wont be schduled
//hence ReadDataReady wont be schduled
template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::ReadDataValid()
{

	///Read all the ports
	unsigned int id= m_RID.read();
	unsigned int response= m_RRESP.read();
	AMBA_DUMP("Received Read data Valid from RTL world, id="<<id
	<<"mRDATA="<<m_RDATA.read());
	assert((read_payload_list.find(id)!= read_payload_list.end()) &&
			"This txn doesn't exist in the payload_list");
	assert((read_payload_list[id].empty()==false) &&
			"queue is empty");
	tlm::tlm_generic_payload * trans= read_payload_list[id].front().trans;
	unsigned int read_data_counter= read_payload_list[id].front().data_counter;

	unsigned char * data_ptr= trans->get_data_ptr();
	unsigned int len=trans->get_data_length();

        // Get the burst size from the transaction. If they don't
        // provide one, determine the optimal burst size based on the
        // transaction data length.
	amba::amba_burst_size * size;
	if (!slave_sock.template get_extension<amba::amba_burst_size>(size,*trans)) {
          m_burst_size.value = (BUSWIDTH+7)/8;
          if (m_burst_size.value > trans->get_data_length()) {
            m_burst_size.value = trans->get_data_length();
          }
          size = &m_burst_size;
        }
	sc_dt::uint64 addr=trans->get_address();
	assert(((len+(addr%((BUSWIDTH+7)/8))-read_data_counter) >= (size->value)) && "Memory allocation error");
	r_bytesTransferred=0;
	//is it a narrow transfer
	unsigned int offset=(addr+read_data_counter)%((BUSWIDTH+7)/8);
	unsigned int array_entry=read_data_counter;
	amba::amba_wrap_offset *m_wrap_index;
	if(slave_sock.template get_extension<amba::amba_wrap_offset>(m_wrap_index,*trans))
	{
		AMBA_DUMP("Wrapping Bursts");
		array_entry+=m_wrap_index->value;
		array_entry%=len;
	}
	for(r_bytesTransferred=0;(r_bytesTransferred< size->value) && ((offset+r_bytesTransferred)<((BUSWIDTH+7)/8)) && (r_bytesTransferred+array_entry<trans->get_data_length()); r_bytesTransferred++)
	{
		 AMBA_DUMP("Put "<<(m_RDATA.read().range((r_bytesTransferred+offset+1)*8-1,(r_bytesTransferred+offset)*8).to_uint())<<" into array index "<<(r_bytesTransferred+array_entry));
		 data_ptr[array_entry+r_bytesTransferred]=(unsigned char)(m_RDATA.read().range((r_bytesTransferred+offset+1)*8-1,(r_bytesTransferred+offset)*8).to_uint());
	}

	AMBA_DUMP("RRESP="<<response);
	amba::amba_exclusive *exclusive;
	switch(response)
	{
		case 0:{
			trans->set_response_status(tlm::TLM_OK_RESPONSE);
			if(slave_sock.template get_extension<amba::amba_exclusive>(exclusive,*trans))
			{//ok this was an exclusive access which failed
				exclusive->value=false;
			}
			break;
		}
		case 1:{
			trans->set_response_status(tlm::TLM_OK_RESPONSE);
			if(slave_sock.template get_extension<amba::amba_exclusive>(exclusive,*trans))
			{//ok this was an exclusive access which succeed
				exclusive->value=true;
			}
			break;
		}
		case 2:{
			trans->set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);break;
		}
		case 3:{
			trans->set_response_status(tlm::TLM_ADDRESS_ERROR_RESPONSE);break;
		}
		default:
			assert(0 && "Response not supported");
	}

	tlm::tlm_phase ph;
	last_resp=false;
	if(m_RLAST.read()==true)
	{
		ph=  amba::BEGIN_LAST_RESP;
		last_resp=true;
	}
	else
		ph=tlm::BEGIN_RESP;

	sc_core::sc_time delay= sc_core::SC_ZERO_TIME;
	tlm::tlm_sync_enum retval=slave_sock->nb_transport_bw(*trans,ph, delay);
	read_done=false;
	switch(retval)
	{
	case tlm::TLM_UPDATED:{
		assert(ph==tlm::END_RESP && "Wrong phase returned");
		read_done=true;
		read_data_counter += r_bytesTransferred;
		if(last_resp)
		{//ok remove this txn from the buffer now

			trans->release();
			read_payload_list[id].pop_front();
			if(read_payload_list[id].empty()==true)
			{
				read_payload_list.erase(id);
			}
			last_resp=false;
		}
		else
		{
			read_payload_list[id].front().data_counter=read_data_counter;
		}
		break;
	}
	case tlm::TLM_ACCEPTED:{
		read_done=false;
		break;
	}
	default:
		assert(0 && "Wrong tlm_sync_enum returned");
	}
	rdata_ready_ev.notify();
}

template< typename MODULE, unsigned int BUSWIDTH,unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH,int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_CT_RTL_Slave_Adapter<MODULE,BUSWIDTH,ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::ReadDataReady()
{
  if(m_RREADY.read()!=read_done)
  {
	  AMBA_DUMP("value changing from "<< m_RREADY.read()<<" to " <<read_done);
  }
  m_RREADY.write(read_done);
}


#endif /*AXI_CT_RTL_SLAVE_ADAPTER_H_*/
