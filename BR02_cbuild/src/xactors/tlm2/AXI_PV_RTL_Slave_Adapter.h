// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _AXI_PV_RTL_SLAVE_ADAPTER_H_
#define _AXI_PV_RTL_SLAVE_ADAPTER_H_

#include "xactors/tlm2/AXI_LT_RTL_Slave_Adapter.h"
#include "adapters/AMBA_PV_Master_LT_AT_Adapter.h"
#include "greensocket/monitor/green_socket_monitor.h"

template< typename MODULE, unsigned int BUSWIDTH=64,unsigned int ADDRWIDTH=32, unsigned int WIDWIDTH=4, unsigned int RIDWIDTH=4,
          int SIZEWIDTH=amba::log2_amba<(BUSWIDTH/8)>::value, bool FORCE_BIGUINT=false >
class AXI_PV_RTL_Slave_Adapter: public sc_core::sc_module
{
public:
  //TLM port through which we will accept the TLM packet
  tlm::tlm_target_socket<BUSWIDTH, amba_pv::amba_pv_protocol_types> slave_sock;
	
  sc_core::sc_in_clk m_clk;
  //systemc ports
  sc_core::sc_signal<bool> m_otherSideReset;
  sc_core::sc_in<bool> m_Reset;
  //ports for signals coming from RTL Master
	
  //write address channel signals
  sc_core::sc_out<sc_dt::sc_uint<WIDWIDTH> > m_AWID;
  sc_core::sc_out<sc_dt::sc_uint<ADDRWIDTH> > m_AWADDR;
  sc_core::sc_out<sc_dt::sc_uint<4> > m_AWLEN;
  sc_core::sc_out<sc_dt::sc_uint<SIZEWIDTH> > m_AWSIZE;
  sc_core::sc_out<sc_dt::sc_uint<2> > m_AWBURST;
  sc_core::sc_out<sc_dt::sc_uint<2> > m_AWLOCK;
  sc_core::sc_out<sc_dt::sc_uint<4> > m_AWCACHE;
  sc_core::sc_out<sc_dt::sc_uint<3> > m_AWPROT;
  sc_core::sc_out<bool> m_AWVALID;
  sc_core::sc_in<bool> m_AWREADY;
	
  //write data channel signals 
  sc_core::sc_out<sc_dt::sc_uint<WIDWIDTH> > m_WID;
  sc_core::sc_out<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT) >::uint_type> m_WDATA;
  sc_core::sc_out<sc_dt::sc_uint<(BUSWIDTH+7)/8> > m_WSTRB;
  sc_core::sc_out<bool> m_WLAST;
  sc_core::sc_out<bool> m_WVALID;
  sc_core::sc_in<bool> m_WREADY;
	  
  //write channel response
  sc_core::sc_in<sc_dt::sc_uint<WIDWIDTH> > m_BID;
  sc_core::sc_in<sc_dt::sc_uint<2> > m_BRESP;
  sc_core::sc_in<bool> m_BVALID;
  sc_core::sc_out<bool> m_BREADY;
	
  //Read address 
  sc_core::sc_out<sc_dt::sc_uint<RIDWIDTH> > m_ARID;
  sc_core::sc_out<sc_dt::sc_uint<ADDRWIDTH> > m_ARADDR;
  sc_core::sc_out<sc_dt::sc_uint<4> > m_ARLEN;
  sc_core::sc_out<sc_dt::sc_uint<SIZEWIDTH> > m_ARSIZE;
  sc_core::sc_out<sc_dt::sc_uint<2> > m_ARBURST;
  sc_core::sc_out<sc_dt::sc_uint<2> > m_ARLOCK;
  sc_core::sc_out<sc_dt::sc_uint<4> > m_ARCACHE;
  sc_core::sc_out<sc_dt::sc_uint<3> > m_ARPROT;
  sc_core::sc_out<bool> m_ARVALID;
  sc_core::sc_in<bool> m_ARREADY;
	
  // Read Data ports
  sc_core::sc_in<sc_dt::sc_uint<RIDWIDTH> > m_RID;
  sc_core::sc_in<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT) >::uint_type > m_RDATA;
  sc_core::sc_in<sc_dt::sc_uint<2> > m_RRESP;
  sc_core::sc_in<bool> m_RLAST;
  sc_core::sc_in<bool> m_RVALID;
  sc_core::sc_out<bool> m_RREADY;

  // Timing related APIs. Pass on to the LT RTL adapter
  void set_slave_timing(gs::socket::timing_info &timing)
  {
    axi_lt_rtl_adapter.set_slave_timing(timing);
  }
  void set_slave_timing(tlm::tlm_phase phase, const sc_core::sc_time& tm)
  {
    axi_lt_rtl_adapter.set_slave_timing(phase, tm);
  }
  void register_master_timing_listener(MODULE* mod,void (MODULE::*fn)(gs::socket::timing_info))
  {
    axi_lt_rtl_adapter.register_master_timing_listener(mod, fn);
  }

  // APIs for Carbon Debug. Pass on to the LT RTL adapter
  void registerDebugAccessCB(CarbonDebugAccessIF* dbgIf)
  {
    axi_lt_rtl_adapter.registerDebugAccessCB(dbgIf);
  }

  //! API to get and activate the monitor on the LT socket
  gs::socket::monitor<BUSWIDTH>* getMonitor(void)
  {
    lt_monitor->activated = true;
    return lt_monitor;
  }

  //! Constructor
  SC_HAS_PROCESS(AXI_PV_RTL_Slave_Adapter);
  AXI_PV_RTL_Slave_Adapter(sc_core::sc_module_name nm) :
    sc_core::sc_module(nm),
    slave_sock("axi_slave_socket"),
    axi_lt_rtl_adapter("axi_lt_rtl_adapter"),
    pv_lt_adapter("axi_pv_lt_adapter")
  {
    // Connect the RTL pins to the LT_RTL adapter
    axi_lt_rtl_adapter.m_clk(m_clk);
    axi_lt_rtl_adapter.m_Reset(m_Reset);
    axi_lt_rtl_adapter.m_ARREADY(m_ARREADY);
    axi_lt_rtl_adapter.m_AWREADY(m_AWREADY);
    axi_lt_rtl_adapter.m_BID(m_BID);
    axi_lt_rtl_adapter.m_BRESP(m_BRESP);
    axi_lt_rtl_adapter.m_BVALID(m_BVALID);
    axi_lt_rtl_adapter.m_RDATA(m_RDATA);
    axi_lt_rtl_adapter.m_RID(m_RID);
    axi_lt_rtl_adapter.m_RLAST(m_RLAST);
    axi_lt_rtl_adapter.m_RRESP(m_RRESP);
    axi_lt_rtl_adapter.m_RVALID(m_RVALID);
    axi_lt_rtl_adapter.m_WREADY(m_WREADY);
    axi_lt_rtl_adapter.m_ARADDR(m_ARADDR);
    axi_lt_rtl_adapter.m_ARBURST(m_ARBURST);
    axi_lt_rtl_adapter.m_ARCACHE(m_ARCACHE);
    axi_lt_rtl_adapter.m_ARID(m_ARID);
    axi_lt_rtl_adapter.m_ARLEN(m_ARLEN);
    axi_lt_rtl_adapter.m_ARLOCK(m_ARLOCK);
    axi_lt_rtl_adapter.m_ARPROT(m_ARPROT);
    axi_lt_rtl_adapter.m_ARSIZE(m_ARSIZE);
    axi_lt_rtl_adapter.m_ARVALID(m_ARVALID);
    axi_lt_rtl_adapter.m_AWADDR(m_AWADDR);
    axi_lt_rtl_adapter.m_AWBURST(m_AWBURST);
    axi_lt_rtl_adapter.m_AWCACHE(m_AWCACHE);
    axi_lt_rtl_adapter.m_AWID(m_AWID);
    axi_lt_rtl_adapter.m_AWLEN(m_AWLEN);
    axi_lt_rtl_adapter.m_AWLOCK(m_AWLOCK);
    axi_lt_rtl_adapter.m_AWPROT(m_AWPROT);
    axi_lt_rtl_adapter.m_AWSIZE(m_AWSIZE);
    axi_lt_rtl_adapter.m_AWVALID(m_AWVALID);
    axi_lt_rtl_adapter.m_BREADY(m_BREADY);
    axi_lt_rtl_adapter.m_RREADY(m_RREADY);
    axi_lt_rtl_adapter.m_WDATA(m_WDATA);
    axi_lt_rtl_adapter.m_WID(m_WID);
    axi_lt_rtl_adapter.m_WLAST(m_WLAST);
    axi_lt_rtl_adapter.m_WSTRB(m_WSTRB);
    axi_lt_rtl_adapter.m_WVALID(m_WVALID);

    // Connect the axi CTL RTL socket to the pv_lt_adapter socket. We
    // create a monitor but de-activate it by default
    lt_monitor = gs::socket::connect_with_monitor<BUSWIDTH, tlm::tlm_base_protocol_types>(pv_lt_adapter.master_socket, axi_lt_rtl_adapter.slave_sock);
    lt_monitor->activated = false;

    // Bind our socket to the PV LT Adapter
    slave_sock.bind(pv_lt_adapter.slave_socket);
  }

private:
  //! Function to send the debug transaction through the CarbonDebugAccessIF
  /*! This function just passed the work on to the LT RTL adapter
   */
  unsigned int transport_debug(tlm::tlm_generic_payload & trans)
  {
    axi_lt_rtl_adapter.transport_debug(trans);
  }

  //! The LT_RTL adapter
  AXI_LT_RTL_Slave_Adapter<MODULE, BUSWIDTH, ADDRWIDTH, WIDWIDTH, RIDWIDTH, SIZEWIDTH> axi_lt_rtl_adapter;

  //! The PV LT adapter
  amba::AMBA_PV_Master_LT_AT_Adapter<BUSWIDTH> pv_lt_adapter;

  //! The monitor for activity detection
  gs::socket::monitor<BUSWIDTH>* lt_monitor;
}; // class AXI_PV_RTL_Slave_Adapter: public sc_core::sc_module

#endif // _AXI_PV_RTL_SLAVE_ADAPTER_H_
