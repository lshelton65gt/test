// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef AXI_MASTER_RTL_CT_ADAPTER_H_
#define AXI_MASTER_RTL_CT_ADAPTER_H_

template< typename MODULE, unsigned int BUSWIDTH=64, unsigned int ADDRWIDTH=32, unsigned int WIDWIDTH=4, unsigned int RIDWIDTH=4, int SIZEWIDTH=amba::log2_amba<(BUSWIDTH/8)>::value, bool FORCE_BIGUINT=false>
class AXI_Master_RTL_CT_Adapter:public sc_core::sc_module
#ifdef CARBON_DEBUG_ACCESS
, public CarbonDebugAccessIF
#endif
{
public:
    sc_core::sc_in_clk m_clk;
    sc_core::sc_signal<bool> m_otherSideReset;
    sc_core::sc_in<bool> m_Reset;
    //ports for signals coming from RTL Master
    //write address channel signals
    sc_core::sc_in<sc_dt::sc_uint<WIDWIDTH> > m_AWID;
	sc_core::sc_in<sc_dt::sc_uint<ADDRWIDTH> > m_AWADDR;
	sc_core::sc_in<sc_dt::sc_uint<4> > m_AWLEN;
	sc_core::sc_in<sc_dt::sc_uint<SIZEWIDTH> > m_AWSIZE;
	sc_core::sc_in<sc_dt::sc_uint<2> > m_AWBURST;
	sc_core::sc_in<sc_dt::sc_uint<2> > m_AWLOCK;
	sc_core::sc_in<sc_dt::sc_uint<4> > m_AWCACHE;
	sc_core::sc_in<sc_dt::sc_uint<3> > m_AWPROT;
	sc_core::sc_in<bool> m_AWVALID;
	sc_core::sc_out<bool> m_AWREADY;
	
	//write data channel signals 
	sc_core::sc_in<sc_dt::sc_uint<WIDWIDTH> > m_WID;
	sc_core::sc_in<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT)>::uint_type > m_WDATA;
	sc_core::sc_in<sc_dt::sc_uint<(BUSWIDTH+7)/8> > m_WSTRB;
	sc_core::sc_in<bool> m_WLAST;
	sc_core::sc_in<bool> m_WVALID;
	sc_core::sc_out<bool> m_WREADY;
	  
	//write channel response
	sc_core::sc_out<sc_dt::sc_uint<WIDWIDTH> > m_BID;
	sc_core::sc_out<sc_dt::sc_uint<2> > m_BRESP;
	sc_core::sc_out<bool> m_BVALID;
	sc_core::sc_in<bool> m_BREADY;
	
	//Read address 
	sc_core::sc_in<sc_dt::sc_uint<RIDWIDTH> > m_ARID;
	sc_core::sc_in<sc_dt::sc_uint<ADDRWIDTH> > m_ARADDR;
	sc_core::sc_in<sc_dt::sc_uint<4> > m_ARLEN;
	sc_core::sc_in<sc_dt::sc_uint<SIZEWIDTH> > m_ARSIZE;
	sc_core::sc_in<sc_dt::sc_uint<2> > m_ARBURST;
	sc_core::sc_in<sc_dt::sc_uint<2> > m_ARLOCK;
	sc_core::sc_in<sc_dt::sc_uint<4> > m_ARCACHE;
	sc_core::sc_in<sc_dt::sc_uint<3> > m_ARPROT;
    sc_core::sc_in<bool> m_ARVALID;
	sc_core::sc_out<bool> m_ARREADY;
	
	//Read Data ports
	 sc_core::sc_out<sc_dt::sc_uint<RIDWIDTH> > m_RID;
	 sc_core::sc_out<typename amba::type_chooser<BUSWIDTH, ((BUSWIDTH>64)||FORCE_BIGUINT) >::uint_type > m_RDATA;
	 sc_core::sc_out<sc_dt::sc_uint<2> > m_RRESP;
	 sc_core::sc_out<bool> m_RLAST;
	 sc_core::sc_out<bool> m_RVALID;
	 sc_core::sc_in<bool> m_RREADY;
	
	 //TLM2 based Amba initiator socket
	 amba::amba_master_socket<BUSWIDTH> master_sock;
	 void slave_timing_listener(gs::socket::timing_info);
	 void end_of_elaboration();
	 
	 
	 //Lock object pool
	 struct axi_lock_object:public amba::lock_object_base
	 {
		 private:
			 friend class gs::socket::extension_pool<axi_lock_object>;
		 void atomic_txn_completed()
		 {
			       get_pool()->recycle(this);
		 }

		axi_lock_object()
		{
			amba::lock_object_base::number_of_txns=~0x00;
		}
		 
	 
		public:
		static gs::socket::extension_pool<axi_lock_object>* get_pool(){
		static gs::socket::extension_pool<axi_lock_object> s_pool(5);
		return &s_pool;
		}
    };
	 //setters and getters for the timing-info distro
	 void set_master_timing(gs::socket::timing_info &);
     void set_master_timing(tlm::tlm_phase, const sc_core::sc_time&);
	 gs::socket::timing_info & get_master_timing();
	 sc_core::sc_time& get_master_timing(tlm::tlm_phase);
	 void register_slave_timing_listener( MODULE* mod,void (MODULE::*fn)(gs::socket::timing_info))
     {
		 owner= mod;
    	 timing_cb=fn;
     }
	 
	 ///functions for the write channel
	 void WriteAddrValid();
	 void WriteDataValid();
	 void WriteRespReady();
	 void WriteAddrReady();
	 void WriteDataReady();
	 void WriteRespValid();
	 
	 //functions for the read channel
	 void ReadAddrValid();
	 void ReadRespReady();
	 void ReadAddrReady();
	 void ReadRespValid();
     void begin_req_method();
     void end_resp_method();
     void begin_data_method();
	
	 //Reset signal
	 void ResetMethod();
	 void clock_tick();
	 
#ifdef CARBON_DEBUG_ACCESS
	 //for Carbon debug access
	 CarbonDebugAccessStatus debugMemRead(uint64_t addr, uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0);
	 CarbonDebugAccessStatus debugMemWrite (uint64_t addr, const uint8_t* buf, uint32_t numBytes, uint32_t* ctrl = 0);
#endif

	 ///Transport API
	 tlm::tlm_sync_enum nb_transport_bw(tlm::tlm_generic_payload &, tlm::tlm_phase &, sc_core::sc_time &);
	 
	 SC_HAS_PROCESS(AXI_Master_RTL_CT_Adapter);
	 AXI_Master_RTL_CT_Adapter(sc_core::sc_module_name nm):sc_module(nm),
	 m_clk("m_clk"),
	 m_Reset("AXI_RESET"),
	 m_AWID("AXI_AWID"),
	 m_AWADDR("AXI_AWADDR"),
	 m_AWLEN("AXI_AWLEN"),
	 m_AWSIZE("AXI_AWSIZE"),
	 m_AWBURST("AXI_AWBURST"),
	 m_AWLOCK("AXI_AWLOCK"),
	 m_AWCACHE("AXI_AWCACHE"),
	 m_AWPROT("AXI_AWPROT"),
	 m_AWVALID("AXI_AWVALID"),
	 m_AWREADY("AXI_AWREADY"),
	 m_WID("AXI_WID"),
	 m_WDATA("AXI_WDATA"),
	 m_WSTRB("AXI_WSTRB"),
	 m_WLAST("AXI_WLAST"),
	 m_WVALID("AXI_WVALID"),
	 m_WREADY("AXI_WREADY"),
	 m_BID("AXI_BID"),
	 m_BRESP("AXI_BRESP"),
	 m_BVALID("AXI_BVALID"),
	 m_BREADY("AXI_BREADY"),
	 m_ARID("AXI_ARID"),
	 m_ARADDR("AXI_ARADDR"),
	 m_ARLEN("AXI_ARLEN"),
	 m_ARSIZE("AXI_ARSIZE"),
	 m_ARBURST("AXI_ARBURST"),
	 m_ARLOCK("AXI_ARLOCK"),
	 m_ARCACHE("AXI_ARCACHE"),
	 m_ARPROT("AXI_ARPROT"),
	 m_ARVALID("AXI_ARVALID"),
	 m_ARREADY("AXI_ARREADY"),
	 m_RID("AXI_RID"),
	 m_RDATA("AXI_RDATA"),
	 m_RRESP("AXI_RRESP"),
	 m_RLAST("AXI_RLAST"),
	 m_RVALID("AXI_RVALID"),
	 m_RREADY("AXI_RREADY"),
	 master_sock("axi_master_socket",amba::amba_AXI, amba::amba_CT,false,gs::socket::GS_TXN_WITH_BE_AND_DATA),
	 current_lock(NULL),
	 locked_txn_counter(0),
	 reset_trans(NULL),
	 b_id(0),
	 b_resp_valid(false),
	 b_resp_accepted(false),
	 r_id(0),
	 r_resp_valid(false),
	 r_resp_accepted(false),
	 aw_ready(true),
	 aw_valid(false),
	 w_ready(true),
	 w_valid(false),
	 ar_ready(true),
	 ar_valid(false),
	 last_read_resp(false),
         m_InReset(false),
	 time_unit(sc_core::sc_get_time_resolution()),
	 timing_cb(NULL),
	 owner(NULL),
	 r_bytesTransferred(0),
	 w_bytesTransferred(0),
	 breq_sample_time(sc_core::sc_get_time_resolution()),
	 bdata_sample_time(sc_core::sc_get_time_resolution()),
	 eresp_sample_time(sc_core::sc_get_time_resolution()),
	 end_resp_time(sc_core::SC_ZERO_TIME)
	 {
		 SC_METHOD(clock_tick);
		 sensitive<< m_clk.pos();
		 dont_initialize();
		 
		 SC_METHOD(ReadAddrReady);
		 sensitive << ar_ready_ev;
		 dont_initialize();

		 SC_METHOD(ReadRespValid);
		 sensitive<< r_valid_ev;
		 dont_initialize();
		 	 

		 SC_METHOD(WriteAddrReady);
		 sensitive<< aw_ready_ev;
		 dont_initialize();


		 SC_METHOD(WriteDataReady);
		 sensitive << w_ready_ev;
		 dont_initialize();

		 SC_METHOD(WriteRespValid);
		 sensitive << b_valid_ev;
		 dont_initialize();

		 SC_METHOD(ResetMethod);
		 sensitive << m_Reset;
		// dont_initialize();
		 
		 SC_METHOD(begin_req_method);
		 sensitive<< begin_req_ev;
		 dont_initialize();

		 SC_METHOD(end_resp_method);
		 sensitive<< end_resp_ev;
		 dont_initialize();

		 SC_METHOD(begin_data_method);
		 sensitive<< begin_data_ev;
		 dont_initialize();

		 master_sock.template register_nb_transport_bw<  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH,FORCE_BIGUINT > >(this, & AXI_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::nb_transport_bw);
         master_sock.set_timing_listener_callback(this, & AXI_Master_RTL_CT_Adapter::slave_timing_listener);
		 
	 }
	 virtual ~AXI_Master_RTL_CT_Adapter();
private:
	/*So we will buffer up the transcation and the amount of data it has written/read
	 * We are buffering with the Tag-id as key, but at a time two txns also can come up
	 * with the same id.So we are having a map with a queue of txns.
	 */
	struct payload_info
	{
		tlm::tlm_generic_payload * trans;
		unsigned int data_counter;
	};

	std::map<unsigned int,std::deque<payload_info> > write_payload_list;
	std::map<unsigned int,std::deque<payload_info> > read_payload_list;

	axi_lock_object * current_lock;
	unsigned int locked_txn_counter;
	tlm::tlm_generic_payload * reset_trans;
	
	unsigned int b_id;
	bool b_resp_valid;
	bool b_resp_accepted;
	unsigned int r_id;
	bool r_resp_valid;
	bool r_resp_accepted;
	bool aw_ready;
	bool aw_valid;
	bool w_ready;
	bool w_valid;
	bool ar_ready;
	bool ar_valid;
	bool last_read_resp;
        bool m_InReset;
	sc_core::sc_event aw_ready_ev;
	sc_core::sc_event w_ready_ev;
	sc_core::sc_event ar_ready_ev;
	sc_core::sc_event b_valid_ev;
	sc_core::sc_event r_valid_ev;
	sc_core::sc_event begin_data_ev;
	sc_core::sc_event end_resp_ev;
	sc_core::sc_event begin_req_ev;
	sc_core::sc_time time_unit;
	void (MODULE::*timing_cb)(gs::socket::timing_info) ;
	MODULE* owner;
	gs::socket::timing_info master_timing_info;
	unsigned int r_bytesTransferred;
	unsigned int w_bytesTransferred;
	sc_core::sc_time breq_sample_time;
	sc_core::sc_time bdata_sample_time;
	sc_core::sc_time eresp_sample_time;
	sc_core::sc_time end_resp_time;
};

template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT >::~AXI_Master_RTL_CT_Adapter()
{
	if(reset_trans!=NULL) {
		reset_trans->release();
		reset_trans=NULL;
	}

}



template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::end_of_elaboration()
{
     master_sock.activate_synchronization_protection();
     sc_core::sc_time delay= sc_core::sc_get_time_resolution();
     if (master_timing_info.get_start_time(tlm::BEGIN_REQ)<delay) master_timing_info.set_start_time(tlm::BEGIN_REQ,delay);
     if (master_timing_info.get_start_time(tlm::END_REQ)<delay) master_timing_info.set_start_time(tlm::END_REQ,delay);
     if (master_timing_info.get_start_time(amba::BEGIN_DATA)<delay) master_timing_info.set_start_time(amba::BEGIN_DATA,delay);
     if (master_timing_info.get_start_time(amba::BEGIN_LAST_DATA)<delay) master_timing_info.set_start_time(amba::BEGIN_LAST_DATA,delay);
     if (master_timing_info.get_start_time(amba::END_DATA)<delay) master_timing_info.set_start_time(amba::END_DATA,delay);
     if (master_timing_info.get_start_time(tlm::BEGIN_RESP)<delay) master_timing_info.set_start_time(tlm::BEGIN_RESP,delay);
     if (master_timing_info.get_start_time(amba::BEGIN_LAST_RESP)<delay) master_timing_info.set_start_time(amba::BEGIN_LAST_RESP,delay);
     if (master_timing_info.get_start_time(tlm::END_RESP)<delay) master_timing_info.set_start_time(tlm::END_RESP,delay);
     breq_sample_time=master_timing_info.get_start_time(tlm::BEGIN_REQ);
     bdata_sample_time= master_timing_info.get_start_time(amba::BEGIN_DATA);
     eresp_sample_time=master_timing_info.get_start_time(tlm::END_RESP);
     master_sock.set_initiator_timing(master_timing_info);
     if(timing_cb!=NULL) (owner->*timing_cb)(master_timing_info);
}


template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::slave_timing_listener(gs::socket::timing_info info)
{
	AMBA_DUMP("Slave timing listener called");
	bool do_cb=false;
	if((info.get_start_time(tlm::END_REQ)+time_unit) > master_timing_info.get_start_time(tlm::END_REQ))
	{
		sc_core::sc_time delay= info.get_start_time(tlm::END_REQ);
		master_timing_info.set_start_time(tlm::END_REQ,(delay+time_unit));
		do_cb=true;
	}
	if((info.get_start_time(tlm::BEGIN_RESP)+time_unit) > master_timing_info.get_start_time(tlm::BEGIN_RESP))
	{
		sc_core::sc_time delay= info.get_start_time(tlm::BEGIN_RESP);
		master_timing_info.set_start_time(tlm::BEGIN_RESP,(delay+time_unit));
		do_cb=true;
	}
	if((info.get_start_time(amba::BEGIN_LAST_RESP)+time_unit) > master_timing_info.get_start_time(amba::BEGIN_LAST_RESP))
	{
		sc_core::sc_time delay= info.get_start_time(amba::BEGIN_LAST_RESP);
	    master_timing_info.set_start_time(amba::BEGIN_LAST_RESP,(delay+time_unit));
	    do_cb=true;
	}
	if((info.get_start_time(amba::END_DATA)+time_unit) > master_timing_info.get_start_time(amba::END_DATA))
	{
		sc_core::sc_time delay= info.get_start_time(amba::END_DATA);
	    master_timing_info.set_start_time(amba::END_DATA,(delay+time_unit));
	    do_cb=true;
	}
    
    bool inform_caller=false;
    
	if(master_timing_info.get_start_time(tlm::BEGIN_RESP) > master_timing_info.get_start_time(tlm::END_RESP))
	{
		master_timing_info.set_start_time(tlm::END_RESP,master_timing_info.get_start_time(tlm::BEGIN_RESP));
		do_cb=true;
        inform_caller=true;
	}
	if(master_timing_info.get_start_time(amba::BEGIN_LAST_RESP) > master_timing_info.get_start_time(tlm::END_RESP))
	{
		master_timing_info.set_start_time(tlm::END_RESP,master_timing_info.get_start_time(amba::BEGIN_LAST_RESP));
		do_cb=true;
        inform_caller=true;
	}
    
    if (inform_caller) master_sock.set_initiator_timing(master_timing_info);
    
	if(timing_cb!=NULL && do_cb)
		(owner->*timing_cb)(master_timing_info);
}

template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::ResetMethod()
{
	if(reset_trans==NULL)
		reset_trans= master_sock.get_transaction();
	tlm::tlm_phase ph;
	if(m_Reset.read()==false)
	{
	        m_InReset = true;
		begin_req_ev.cancel();
		end_resp_ev.cancel();
		begin_data_ev.cancel();
		aw_valid = ar_valid = w_valid = false;
		aw_ready = ar_ready = w_ready = true;
		b_resp_valid = b_resp_accepted = r_resp_valid = r_resp_accepted = false;
		write_payload_list.clear();
		read_payload_list.clear();
		ph=amba::RESET_ASSERT;
	}
	else {
	        m_InReset = false;
		ph= amba::RESET_DEASSERT;
	}
	sc_core::sc_time delay= sc_core::SC_ZERO_TIME;
	tlm::tlm_sync_enum retval = master_sock->nb_transport_fw(*reset_trans,ph,delay);
	assert(retval==tlm::TLM_ACCEPTED && "Invalid tlm_sync_enum");
	if(ph==amba::RESET_DEASSERT)
	{
		master_sock.release_transaction(reset_trans);
		reset_trans=NULL;
	}
}

#ifdef CARBON_DEBUG_ACCESS
template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
CarbonDebugAccessStatus  AXI_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::debugMemRead(uint64_t addr, uint8_t* buf, uint32_t numBytes, uint32_t* ctrl)
{
	AMBA_DUMP("Debug Read Access, bytes to read"<< numBytes <<", address="<< addr);
	tlm::tlm_generic_payload * payload = master_sock.get_transaction();
    payload->set_command(tlm::TLM_READ_COMMAND);
    payload->set_address(addr);
    master_sock.reserve_data_size(*payload,numBytes);
    if((ctrl[0]&0x01)==0x1)
    {
    	AMBA_DUMP("Secure Access");
    	master_sock.template validate_extension<amba::amba_secure_access>(*payload);

    }

    unsigned int transfered_bytes=master_sock->transport_dbg(*payload);
    AMBA_DUMP("Actual bytes read"<<transfered_bytes);
    unsigned char * data_ptr = payload->get_data_ptr();
    memcpy(buf,data_ptr,transfered_bytes);
    if(transfered_bytes < numBytes)
    	return eCarbonDebugAccessStatusPartial;
    else
    {
    	tlm::tlm_response_status resp= payload->get_response_status();
    	switch(resp)
    	{
			case tlm::TLM_ADDRESS_ERROR_RESPONSE:
			{
				return eCarbonDebugAccessStatusOutRange;
				break;
			}
			case tlm::TLM_OK_RESPONSE:{
				return eCarbonDebugAccessStatusOk;
				break;
			}
			default:
			{
				return eCarbonDebugAccessStatusError ;
				break;
			}
    	}
    }
}


template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
CarbonDebugAccessStatus   AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::debugMemWrite(uint64_t addr, const uint8_t* buf, uint32_t numBytes, uint32_t* ctrl)
{

	AMBA_DUMP("Debug Write Access, bytes to write"<< numBytes<<", address="<< addr);
	tlm::tlm_generic_payload * payload = master_sock.get_transaction();
	payload->set_command(tlm::TLM_WRITE_COMMAND);
	payload->set_address(addr);
	master_sock.reserve_data_size(*payload,numBytes);
	if((ctrl[0]&0x01)==0x1)
	{
		AMBA_DUMP("Secure Access");
		master_sock.template validate_extension<amba::amba_secure_access>(*payload);
	}
	unsigned char * data_ptr = payload->get_data_ptr();
    memcpy(data_ptr,buf,numBytes);
	unsigned int transfered_bytes=master_sock->transport_dbg(*payload);
	AMBA_DUMP("Actual bytes written"<<transfered_bytes);

	if(transfered_bytes < numBytes)
		return eCarbonDebugAccessStatusPartial;
	else
	{
		tlm::tlm_response_status resp= payload->get_response_status();
		switch(resp)
		{
			case tlm::TLM_ADDRESS_ERROR_RESPONSE:
			{
				return eCarbonDebugAccessStatusOutRange;
				break;
			}
			case tlm::TLM_OK_RESPONSE:{
				return eCarbonDebugAccessStatusOk;
				break;
			}
			default:
			{
				return eCarbonDebugAccessStatusError ;
				break;
			}
		}
	}
}
#endif

template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::set_master_timing(tlm::tlm_phase ph, const sc_core::sc_time& delay)
{
  bool changed =false;
  if (ph==tlm::BEGIN_REQ || ph==tlm::END_RESP){
    if((delay+time_unit) > master_timing_info.get_start_time(ph))
    {
        master_timing_info.set_start_time(ph,delay+time_unit);
        changed =true;
    }
  }
  else
  if (ph==amba::BEGIN_DATA || ph==amba::BEGIN_LAST_DATA)
  {
    if((delay+time_unit) > master_timing_info.get_start_time(amba::BEGIN_DATA))
    {
        master_timing_info.set_start_time(amba::BEGIN_DATA,delay+time_unit);
        changed =true;
    }    
    if((delay+time_unit) > master_timing_info.get_start_time(amba::BEGIN_LAST_DATA))
    {
        master_timing_info.set_start_time(amba::BEGIN_LAST_DATA,delay+time_unit);
        changed =true;
    }    
  }
  if(changed==true)
  {  
    master_sock.set_initiator_timing(master_timing_info);
    breq_sample_time=master_timing_info.get_start_time(tlm::BEGIN_REQ);
    bdata_sample_time= master_timing_info.get_start_time(amba::BEGIN_DATA);
    eresp_sample_time=master_timing_info.get_start_time(tlm::END_RESP);
  }
  changed=false;
  if(master_timing_info.get_start_time(tlm::BEGIN_REQ)> master_timing_info.get_start_time(tlm::END_REQ))
  {
      sc_core::sc_time delay= master_timing_info.get_start_time(tlm::BEGIN_REQ);
      master_timing_info.set_start_time(tlm::END_REQ,delay);
      changed=true;
  }
  if(master_timing_info.get_start_time(amba::BEGIN_DATA) > master_timing_info.get_start_time(amba::END_DATA) )
  {
      sc_core::sc_time delay= master_timing_info.get_start_time(amba::BEGIN_DATA);
      master_timing_info.set_start_time(amba::END_DATA,delay);
      changed=true;
  }
  if(changed && timing_cb!=NULL) (owner->*timing_cb)(master_timing_info);  
}

template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::set_master_timing(gs::socket::timing_info& info)
{
          bool changed =false;
	      if((info.get_start_time(tlm::BEGIN_REQ)+time_unit) > master_timing_info.get_start_time(tlm::BEGIN_REQ))
	      {
	    	  master_timing_info.set_start_time(tlm::BEGIN_REQ,info.get_start_time(tlm::BEGIN_REQ)+time_unit);
	    	  changed =true;
	      }
          
          //make sure we use the max of bdata and blastdata
          sc_core::sc_time bdata_time=info.get_start_time(amba::BEGIN_DATA);
          if (bdata_time<info.get_start_time(amba::BEGIN_LAST_DATA)) bdata_time=info.get_start_time(amba::BEGIN_LAST_DATA);
          
	      if((bdata_time+time_unit)  > master_timing_info.get_start_time(amba::BEGIN_DATA))
	      {
	    	  master_timing_info.set_start_time(amba::BEGIN_DATA,bdata_time+time_unit);
	    	  changed =true;
	      }
	      if((bdata_time+time_unit) > master_timing_info.get_start_time(amba::BEGIN_LAST_DATA))
	      {
	    	  master_timing_info.set_start_time(amba::BEGIN_LAST_DATA,bdata_time+time_unit);
	    	  changed =true;
	      }
	      if((info.get_start_time(tlm::END_RESP)+time_unit) > master_timing_info.get_start_time(tlm::END_RESP)
	    	)
	      {
    	  	  master_timing_info.set_start_time(tlm::END_RESP,info.get_start_time(tlm::END_RESP)+time_unit);
    	  	changed =true;
	      }
           if(changed==true)
           {  master_sock.set_initiator_timing(master_timing_info);
	    	  breq_sample_time=master_timing_info.get_start_time(tlm::BEGIN_REQ);
	    	  bdata_sample_time= master_timing_info.get_start_time(amba::BEGIN_DATA);
	    	  eresp_sample_time=master_timing_info.get_start_time(tlm::END_RESP);
           }
          changed=false;
	      if(master_timing_info.get_start_time(tlm::BEGIN_REQ)> master_timing_info.get_start_time(tlm::END_REQ))
	      {
	    	  sc_core::sc_time delay= master_timing_info.get_start_time(tlm::BEGIN_REQ);
	    	  master_timing_info.set_start_time(tlm::END_REQ,delay);
	    	  changed=true;
	      }
	      if(master_timing_info.get_start_time(amba::BEGIN_DATA) > master_timing_info.get_start_time(amba::END_DATA) )
	      {
	    	  sc_core::sc_time delay= master_timing_info.get_start_time(amba::BEGIN_DATA);
	    	  master_timing_info.set_start_time(amba::END_DATA,delay);
	    	  changed=true;
	      }
	      if(changed && timing_cb!=NULL) (owner->*timing_cb)(master_timing_info);

}

template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
gs::socket::timing_info&  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::get_master_timing()
{
	return master_timing_info;
}

template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
sc_core::sc_time& AXI_Master_RTL_CT_Adapter<MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH, FORCE_BIGUINT>::get_master_timing(tlm::tlm_phase ph)
{
	return (master_timing_info.get_start_time(ph));
}

template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::clock_tick()
{
        if (m_InReset) return;

	if(aw_valid && aw_ready)
	{
		aw_valid=false;
        aw_ready=false;
    //    aw_ready_ev.notify(); //clear the ready signals
	}
	if(ar_valid && ar_ready)
	{
		ar_valid=false;
        ar_ready=false;
      //  ar_ready_ev.notify(); //clear the ready signals
	}
	if(w_valid && w_ready)
	{
		w_valid=false;
        w_ready=false;
     //   w_ready_ev.notify(); //clear the ready signals
	}
	if(r_resp_accepted)
	{
		r_resp_accepted=false;
		r_resp_valid=false;
		last_read_resp=false;
		r_valid_ev.notify();
	}
	if(b_resp_accepted)
	{
		b_resp_accepted=false;
		b_resp_valid=false;
		b_valid_ev.notify();
	}

	begin_req_ev.notify(breq_sample_time);
	end_resp_ev.notify(eresp_sample_time);
	begin_data_ev.notify(bdata_sample_time);

	end_resp_time= sc_core::sc_time_stamp()+eresp_sample_time;
}



template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::end_resp_method()
{
	if(m_BREADY.read()==true)
		WriteRespReady();
	if(m_RREADY.read()==true)
		ReadRespReady();
}

template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::begin_data_method()
{
	if(m_WVALID.read()==true )
	{
		if(w_valid==false)
		{
			w_valid=true;
			WriteDataValid();
		}
	}
}

template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::begin_req_method()
{

	if(m_AWVALID.read()==true)
	{
		if(aw_valid==false)
		{
			aw_valid=true;
			WriteAddrValid();
		}

	}

	if((m_ARVALID.read()==true))
	{
		if(ar_valid==false)
		{
			ar_valid=true;
			ReadAddrValid();
		}
	}

}


/*!
 * @brief nb_transport_bw It is a backward transport call from the target
 * 
 * @details This functions receives signals from the target end and then
 * converts the TLM2 transaction into the RTL signals
 */

template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
tlm::tlm_sync_enum  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::nb_transport_bw(tlm::tlm_generic_payload & payload, tlm::tlm_phase &ph,sc_core::sc_time & delay)
{
    
        //[Handling Reset signal from the bus/slave which is to be
        //communicated to the RTL master
        if (ph==amba::RESET_ASSERT) { //ACTIVE LOW
	  m_InReset = true;
	  begin_req_ev.cancel();
	  end_resp_ev.cancel();
	  begin_data_ev.cancel();
	  aw_valid = ar_valid = w_valid = false;
	  aw_ready = ar_ready = w_ready = true;
	  b_resp_valid = b_resp_accepted = r_resp_valid = r_resp_accepted = false;
	  write_payload_list.clear();
	  read_payload_list.clear();
          m_otherSideReset.write(false);
          return tlm::TLM_ACCEPTED;
        }
        else if (ph==amba::RESET_DEASSERT) {
	  m_InReset = false;
          m_otherSideReset.write(true);
          return tlm::TLM_ACCEPTED;
        }
        //]
	//if in RESET then no need to process any other phase
	if (m_InReset) return tlm::TLM_ACCEPTED;

	//We got some backward phase from the slave
    sc_core::sc_time current_time=sc_core::sc_time_stamp();
	amba::amba_id * id;
	master_sock.template get_extension<amba::amba_id>(id, payload);
	amba::amba_burst_size *size;
    master_sock.template get_extension<amba::amba_burst_size>(size, payload);

    AMBA_DUMP("Received backward transport call: trans_id="<<(unsigned int) (id->value)<< "phase:"<<ph);

	//this id should be into the buffer
    if(payload.is_read())
    {	assert((read_payload_list.find(id->value)!=read_payload_list.end()) && "No txn with this id stored in buffer");
		assert((read_payload_list[id->value]).empty()==false && "The deque doesn't have any transaction");
    }
    else if(payload.is_write())
    {
    	assert((write_payload_list.find(id->value)!=write_payload_list.end()) && "No txn with this id stored in buffer");
    	assert((write_payload_list[id->value]).empty()==false && "The deque doesn't have any transaction");
    }
    if(ph==tlm::END_REQ)
	{
		//Ok the address/control request has been accepted by the slave, intimate master
		if(payload.get_command()==tlm::TLM_WRITE_COMMAND)
		{
			//Ok set the READY signal for the write address channel
			aw_ready=true;
			aw_ready_ev.notify();

		}
		else
		{
			//set the READY signal for the read address channel
			ar_ready=true;
			ar_ready_ev.notify();

		}

		return tlm::TLM_ACCEPTED;
	
	}
	else if(ph==amba::END_DATA)
	{
		//Ok the data has been accepted by the slave, intimate master to send next data phase
		///We know this phase is always for the Write transactions

		w_ready=true;
		w_ready_ev.notify();
		return tlm::TLM_ACCEPTED;
			
	}
	else if(ph== tlm::BEGIN_RESP || ph == amba::BEGIN_LAST_RESP)
	{
			///Either it is the response for the Write Transactions or the Read Data
			if(payload.get_command()==tlm::TLM_WRITE_COMMAND)
			{
				//Ok so it is the response received after the last write transfer of the transaction
				//read the response
				tlm::tlm_response_status resp= payload.get_response_status();
				unsigned int b_resp=0;
				switch(resp)
				{
					case tlm::TLM_ADDRESS_ERROR_RESPONSE:
					{
						b_resp=0x03; //DECERR
						break;
					}
					case tlm::TLM_OK_RESPONSE:{
						amba::amba_exclusive *exclusive;
						//Now check is it a response to exclusive write
						if(master_sock.template get_extension<amba::amba_exclusive> (exclusive, payload)==true)
						{//if true then it means it was an exclusive write operation
							
							//now check the value of this extension
							if(exclusive->value==true)
							{//great! target has sent the EXOKAY response
								b_resp=0x01; //EXOKAY
							}
							else
							{//oops it is an OKAY response, exclusive access error
								b_resp=0x00; //OKAY
							}
						}
						else
						{
							//it is a normal OKAY response
							b_resp=0x00; //OKAY
						}
						
						break;
					}
					case tlm::TLM_GENERIC_ERROR_RESPONSE:
					{
						b_resp=0x02; //SLVERR
						break;
					}
					default:
						assert(0 && "Unsupported error response");
					
				}
				b_id= id->value;
				///write on the RTL BRESP channel
				m_BRESP.write(b_resp);
				m_BID.write(b_id);
				b_resp_valid=true;
				b_valid_ev.notify();
				if(current_time>= end_resp_time)
				{
					//check if we can consider it finished or not yet
					if((m_BREADY.read()) == true)
					{//Ok Master is ready to take the response, so return END_RESP
						b_resp_accepted=true;
						ph=tlm::END_RESP;
						//remove from the buffer too
						write_payload_list[b_id].pop_front();

						//release this transaction now
						if((write_payload_list[b_id]).empty()==true)
						{//There has been only one entry in the queue with this id, so remove it fom list
							write_payload_list.erase(b_id);
						}
						master_sock.release_transaction(&payload);
						return tlm::TLM_UPDATED;
					}
					else
					{

						b_resp_accepted=false;
						return tlm::TLM_ACCEPTED;
					}
				}
				else
				{
					b_resp_accepted=false;
					return tlm::TLM_ACCEPTED;
				}
			
			}
			else
			{ //Ok So the Read data has been arrived from the slave
			  //decode the data and send it to the RTL master
				r_id=id->value;

				unsigned int read_data_counter=(read_payload_list[r_id].front()).data_counter;
				unsigned char * data_ptr = payload.get_data_ptr();
				unsigned int length= payload.get_data_length();
				assert(((length-read_data_counter) >= size->value) && "Memory allocation error");
				//check if the transfer-size is less than the buswidth
				sc_dt::uint64 address=payload.get_address();
				unsigned int offset=(address+read_data_counter)%((BUSWIDTH+7)/8);
				unsigned int array_entry=read_data_counter;
				amba::amba_wrap_offset *m_wrap_index;
				if(master_sock.template get_extension<amba::amba_wrap_offset>(m_wrap_index,payload))
				{
					array_entry+=m_wrap_index->value;
					array_entry%=length;
				}
				 //TODO: make endianess save.

				typename amba::type_chooser<BUSWIDTH, (BUSWIDTH>64 || FORCE_BIGUINT)>::uint_type m_tmp_dt;
				m_tmp_dt=m_RDATA.read();
				for(r_bytesTransferred=0;(r_bytesTransferred<size->value) && ((offset+r_bytesTransferred)<((BUSWIDTH+7)/8)) && (r_bytesTransferred+array_entry<payload.get_data_length());r_bytesTransferred++ )
				{
					m_tmp_dt.range((r_bytesTransferred+offset+1)*8-1,(r_bytesTransferred+offset)*8)=(unsigned char )data_ptr[array_entry+r_bytesTransferred];
					AMBA_DUMP("Got "<<(m_tmp_dt.range((r_bytesTransferred+offset+1)*8-1,(r_bytesTransferred+offset)*8).to_uint())<<" from array index "<<(r_bytesTransferred+array_entry));
				}
				AMBA_DUMP("total bytes transferred="<<r_bytesTransferred);

				unsigned int r_resp=0;
				///read the response of the tansaction
				tlm::tlm_response_status resp= payload.get_response_status();
				switch(resp)
				{
					case tlm::TLM_ADDRESS_ERROR_RESPONSE:{
						r_resp=0x03; //DECERR
						break;
					}
					case tlm::TLM_OK_RESPONSE:{
						amba::amba_exclusive *exclusive;
						//Now check is it a response to exclusive read
						if(master_sock.template get_extension<amba::amba_exclusive> (exclusive, payload)==true)
						{//if true then it means it was an exclusive read operation
							
							//now check the value of this extension
							if(exclusive->value==true)
							{//great! target has sent the EXOKAY response
								r_resp=0x01; //EXOKAY
							}
							else
							{//oops it is an OKAY response, exclusive access error
								r_resp=0x00; //OKAY
							}
						}
						else
						{
							//it is a normal OKAY response
							r_resp=0x00; //OKAY
						}
						break;
					}
					case tlm::TLM_GENERIC_ERROR_RESPONSE:
					{
						r_resp=0x02; //SLVERR
						break;
					}
					default:
						assert(0 && "Unsupported error response");
					
				}
				///Send the Signals on the Read channel to RTL Master
				m_RID.write(r_id);
				m_RRESP.write(r_resp);
				m_RDATA.write(m_tmp_dt);
				if(ph== amba::BEGIN_LAST_RESP)
				  last_read_resp=true;

				r_resp_valid=true;
				r_valid_ev.notify();
				if(current_time >= end_resp_time)
				{

					if(m_RREADY.read()==true)
					{//Ok Master is ready to take the response, so return END_RESP
						AMBA_DUMP("m_RREADY is high, Sending End_RESP, TLM_UPDATED");
						r_resp_accepted=true;
						read_data_counter+=r_bytesTransferred;
						unsigned int index=0;
						for(index=0; index< read_payload_list[r_id].size(); index++)
						{
							if(read_payload_list[r_id][index].trans == &payload)
								break;
						}

						if(last_read_resp)
						{
							read_payload_list[r_id].pop_front();
							if(read_payload_list[r_id].empty()==true)
							{
								read_payload_list.erase(r_id);
							}
							master_sock.release_transaction(&payload);
						}
						else
						{
							(read_payload_list[r_id][index]).data_counter=read_data_counter;
							r_bytesTransferred=0;
						}
						ph=tlm::END_RESP;
						return tlm::TLM_UPDATED;
					}
					else
					{
						r_resp_accepted=false;
						//We will signal the target about End of this txn later
						return tlm::TLM_ACCEPTED;
					}
				}
				else
				{
					r_resp_accepted=false;
					//We will signal the target about End of this txn later
					return tlm::TLM_ACCEPTED;
				}
				
			}
			
		}
	else
		assert("Unsupported phase");
    //dummy return value for non-void returntype function
    return tlm::TLM_ACCEPTED;
}

/*
 * @brief ReadAddrValid
 * 
 * @details This function is triggered when the Read address
 * channel data is uploaded and validated by RTL master
 */
template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::ReadAddrValid()
{
		tlm::tlm_generic_payload * current_trans;
		//Ok so the ReadAddress channel has been validated
		
		current_trans=master_sock.get_transaction();

		//read the control information
		amba::amba_id * m_id;
		master_sock.template get_extension<amba::amba_id>(m_id,*current_trans);
		m_id->value=m_ARID.read();
		master_sock.template validate_extension<amba::amba_id>(*current_trans);


       	current_trans->set_command(tlm::TLM_READ_COMMAND);
		sc_dt::uint64 address=m_ARADDR.read();

		current_trans->set_address(address);

                // Clear the byte enable ptr so we don't get an error response.
                current_trans->set_byte_enable_ptr(NULL);
                current_trans->set_byte_enable_length(0);
		
		unsigned int ar_size=0;
		ar_size=m_ARSIZE.read();

		amba::amba_burst_size * m_burst_size;
	        unsigned int temp=0;
		master_sock.template get_extension<amba::amba_burst_size>(m_burst_size, *current_trans);
		temp=0x1<<(ar_size);
		m_burst_size->value =temp;
		master_sock.template validate_extension<amba::amba_burst_size>(*current_trans);
		
		unsigned int burst_len=0;
		burst_len=m_ARLEN.read()+1;

		unsigned int offset=address %(m_burst_size->value);
		unsigned int data_len= burst_len* (m_burst_size->value)-offset;
		master_sock.reserve_data_size(*current_trans, data_len);
		
		current_trans->set_streaming_width(data_len);
		unsigned int burst_type= m_ARBURST.read();

		///Check the burst type
		switch(burst_type)
		{
		case 0:{///Fixed address burst
			     current_trans->set_streaming_width(m_burst_size->value);break;
		       }
		case 2:{///Wrapping burst
					amba::amba_wrap_offset* m_wrap_index;
					master_sock.template get_extension<amba::amba_wrap_offset>(m_wrap_index, *current_trans);
					//wrong. calc wrapboundary set addr and index properly.
					sc_dt::uint64 lowest_addr_in_wrap = address & ~(data_len+offset - 1);
					sc_dt::uint64 tail_length =address - lowest_addr_in_wrap;
					current_trans->set_address(lowest_addr_in_wrap);
					m_wrap_index->value=tail_length;
					master_sock.template validate_extension<amba::amba_wrap_offset>( *current_trans);
					AMBA_DUMP("Added wrap extension. Entry is "<<tail_length<<" txn addr is "<<lowest_addr_in_wrap);
					break;
		      }
		}
		
		///Check the Lock support lines
		unsigned int lock=m_ARLOCK.read();

		amba::amba_lock *lock_ext;
		switch(lock)
		{
			case 0:
			{
				if(locked_txn_counter!=0) //ok so this is an unlocking txn
				{//Is this txn a part of locked transactions
					locked_txn_counter++;
					master_sock.template get_extension<amba::amba_lock>(lock_ext, *current_trans);
					lock_ext->value = current_lock;
					lock_ext->value->number_of_txns=locked_txn_counter;
					master_sock.template validate_extension<amba::amba_lock>(*current_trans);
					locked_txn_counter=0;		
				}
				break;
			}
			case 1:{
					amba::amba_exclusive *exclusive;
					master_sock.template get_extension<amba::amba_exclusive>(exclusive, *current_trans);
					exclusive->value=true;
					master_sock.template validate_extension<amba::amba_exclusive>(*current_trans);
				    break;
				}
			case 2:{
				     if(locked_txn_counter==0)
    				 {//ok so this is the first locked txn
    					 current_lock=axi_lock_object::get_pool()->create();
    					 current_lock->lock_is_understood_by_slave=false;
    					 current_lock->number_of_txns= ~(0x0);
    				 }
				     assert(current_lock != NULL && "Current lock is NULL");
				     //else it is an intermediate locking txns
				     master_sock.template get_extension<amba::amba_lock>(lock_ext, *current_trans);
				     //set the value of locked extension
				     lock_ext->value = current_lock;
				     master_sock.template validate_extension<amba::amba_lock>(*current_trans);
    				 locked_txn_counter++;;
					break;
				}
			default:
				assert(0 && "Lock value not supported");
		}
		
		///Check the Cache support lines
		unsigned int cache = m_ARCACHE.read();

		if((cache&0x01)==0x01)
		{
			master_sock.template validate_extension<amba::amba_bufferable>(*current_trans);
		}
		if((cache&0x02)==0x02)
		{
			master_sock.template validate_extension<amba::amba_cacheable>(*current_trans);
		
			if((cache&0x04)==0x04)
			{
			master_sock.template validate_extension<amba::amba_read_allocate>(*current_trans);
				//This is a data only extension
			}
			if((cache&0x08)==0x08)
			{
				master_sock.template validate_extension<amba::amba_write_allocate>(*current_trans);
				//This is a data-only extension
			}
		}
		//check the protection support lines
		unsigned int prot = m_ARPROT.read();

		if((prot&0x01)==0x01)
			master_sock.template validate_extension<amba::amba_privileged>(*current_trans);
		else
			master_sock.template invalidate_extension<amba::amba_privileged>(*current_trans);

		if((prot&0x04)==0x04)
			master_sock.template validate_extension<amba::amba_instruction>(*current_trans);
		else
			master_sock.template invalidate_extension<amba::amba_instruction>(*current_trans);

		if((prot & 0x02)==0x02)
		{
			master_sock.template validate_extension<amba::amba_secure_access>(*current_trans);
		}
		AMBA_DUMP("Received Read Address Channel request with following attributes: ARId="<<(unsigned int) m_id->value<<
						"Address=" <<address<<", ARSIZE ="<<ar_size<<", ARLEN="<<(unsigned int )(m_ARLEN.read())<<
						"ARBURST="<<burst_type<<", ARLOCK="<<lock<<", ARCACHE= "<< cache<<", ARPROT="<<prot);

		tlm::tlm_phase ph= tlm::BEGIN_REQ;
		sc_core::sc_time delay= sc_core::SC_ZERO_TIME;
		//put this transaction into the map
		payload_info info;
		info.trans=current_trans;
		info.data_counter=0;

		if(read_payload_list.find(m_id->value)== read_payload_list.end())
			AMBA_DUMP("New transaction in map with this ID");
		else
			AMBA_DUMP("Adding to the Queue exists with same id.");
		read_payload_list[m_id->value].push_back(info);


		tlm::tlm_sync_enum retval= master_sock->nb_transport_fw(*current_trans, ph, delay);
		switch(retval)
		{
			case tlm::TLM_ACCEPTED:{
					assert((ph== tlm::BEGIN_REQ) && "Unexpected phase");
					//Ok lower the AWREADY low until the current Write-request is in progress
					ar_ready=false;
					break;
				}
			case tlm::TLM_UPDATED:{
				assert((ph == tlm::END_REQ) && "Unexpected phase" );
				//ok the slave has accepted the write-addr-request, ready for the next
				ar_ready=true;
				break;
			}
			default:
				assert(0 && "Wrong tlm_sync_enum returned");
		}
		ar_ready_ev.notify();

}





template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::ReadAddrReady()
{
 m_ARREADY.write(ar_ready);
 AMBA_DUMP("Written on ARREADY, value="<<(unsigned int)ar_ready);
}



/*
 * @brief ReadRespReady
 *
 * @detail This function is triggered when RTL master
 * sets the RREADY signal to the slave.
 *
 */

template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::ReadRespReady()
{
	if(r_resp_valid==true && r_resp_accepted==false )
	{	//ok the master is now accpeted the response we proposed, intimate the slave for it

		assert((read_payload_list.find(r_id)!=read_payload_list.end()) && "This rid doesn't exist in the map");
		assert((read_payload_list[r_id].empty()==false) && "The queue is empty");
		tlm::tlm_generic_payload * payload = ((read_payload_list[r_id]).front()).trans;
		amba::amba_burst_size * size;
		master_sock.template get_extension<amba::amba_burst_size>(size,*payload);
		//increment the counter now
		((read_payload_list[r_id]).front()).data_counter +=r_bytesTransferred;
		r_bytesTransferred=0;
		tlm::tlm_phase ph= tlm::END_RESP;
		sc_core::sc_time delay=sc_core::SC_ZERO_TIME;
		AMBA_DUMP("sending end_resp for"<<r_id);
		tlm::tlm_sync_enum retval=master_sock->nb_transport_fw(*payload, ph, delay);
		assert(retval== tlm::TLM_ACCEPTED && "Unexpected tlm_sync_enum");
		r_resp_accepted=true;

		if(last_read_resp)
		{
			//release this transaction now and remove from the buffer too
			(read_payload_list[r_id]).pop_front();
			if((read_payload_list[r_id]).empty()==true)
			{
				read_payload_list.erase(r_id);
			}
		    master_sock.release_transaction(payload);
		}
	}
}

//This Method won't be scheduled while m_InReset is true
template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::ReadRespValid()
{
	 m_RVALID.write(r_resp_valid);
	 m_RLAST.write(last_read_resp);
	AMBA_DUMP("Written on RVALID, value="<<(unsigned int)r_resp_valid<<std::endl<< "Written on RLAST, value="<<(unsigned int)last_read_resp<<std::endl);
}



/*!
 * @brief WriteAddressValid
 * 
 * @detail This function is triggered hen the ADDRESS VALID 
 * signal is set high by the RTL Master idicating that it has load
 * the address and control signals.
 */

template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::WriteAddrValid()
{
	    //Ok so the WriteAddress channel has been validated.
		tlm::tlm_generic_payload * current_trans=master_sock.get_transaction();
		


		//read the control information
		amba::amba_id * m_id;
		master_sock.template get_extension<amba::amba_id>(m_id,*current_trans);
		m_id->value= m_AWID.read();
		master_sock.template validate_extension<amba::amba_id>(*current_trans);
		


		///It is a write command
		current_trans->set_command(tlm::TLM_WRITE_COMMAND);
		sc_dt::uint64 address=m_AWADDR.read();
		current_trans->set_address(address);
		

		//Check the burst size
		unsigned int aw_size=0;
		aw_size=m_AWSIZE.read();
		amba::amba_burst_size * m_burst_size;
	    unsigned int temp=0;
		master_sock.template get_extension<amba::amba_burst_size>(m_burst_size, *current_trans);
		temp=0x1<<(aw_size);
		m_burst_size->value =temp;

		master_sock.template validate_extension<amba::amba_burst_size>(*current_trans);
		

		//Check the burst_length, it will decide the total data length
		unsigned int burst_len=0;
		burst_len=(m_AWLEN.read()+1);
		unsigned int offset=address %(m_burst_size->value);
		unsigned int data_len= burst_len*(m_burst_size->value)-offset;
		master_sock.reserve_data_size(*current_trans, data_len);
		current_trans->set_streaming_width(data_len);

		// CARBON
		// Reserve the byte enable field at this time as well - The byte enable length must match the data length as each
		// beat of a burst can have its own WSTRB values (we cannot assume the same WSTRB for each beat)
		master_sock.reserve_be_size(*current_trans, data_len);
		// END CARBON

		///Check the burst type
		unsigned int burst_type= m_AWBURST.read();
		switch(burst_type)
		{
		case 0:{///Fixed address burst, ok on the same address, keep writing the bytes
			     current_trans->set_streaming_width(temp);break;
		       }
		case 2:{///Wrapping burst
			    AMBA_DUMP("Wrapping bursts");
				amba::amba_wrap_offset* m_wrap_index;
				master_sock.template get_extension<amba::amba_wrap_offset>(m_wrap_index, *current_trans);
				//wrong. calc wrapboundary set addr and index properly.
				sc_dt::uint64 lowest_addr_in_wrap = address & ~(data_len+offset - 1);
				sc_dt::uint64 tail_length =address - lowest_addr_in_wrap;
				current_trans->set_address(lowest_addr_in_wrap);
				m_wrap_index->value=tail_length;
				master_sock.template validate_extension<amba::amba_wrap_offset>( *current_trans);
				AMBA_DUMP("Added wrap extension. Entry is "<<tail_length<<" txn addr is "<<lowest_addr_in_wrap);
				break;
		     }
		}

		///Check the Lock support lines
		unsigned int lock=m_AWLOCK.read();

		amba::amba_lock *lock_ext;
		switch(lock)
		{
			case 0:
			{
				if(locked_txn_counter!=0) //ok so this is an unlocking txn
				{//Is this txn a part of locked transactions
					locked_txn_counter++;
					master_sock.template get_extension<amba::amba_lock>(lock_ext, *current_trans);
					lock_ext->value->number_of_txns=locked_txn_counter;
					lock_ext->value = current_lock;
					master_sock.template validate_extension<amba::amba_lock>(*current_trans);
					locked_txn_counter=0;		
				}
				//else we don't need to do anything
				break;
			}
			case 1:{
				    //This is an exclusive write 
					amba::amba_exclusive *exclusive;
					master_sock.template get_extension<amba::amba_exclusive>(exclusive, *current_trans);
					exclusive->value=true;
					master_sock.template validate_extension<amba::amba_exclusive>(*current_trans);
				    break;
				}
			case 2:{
    				 if(locked_txn_counter==0)
    				 {//ok so this is the first locked txn
    					 current_lock=axi_lock_object::get_pool()->create();
    					 current_lock->lock_is_understood_by_slave=false;
    					 current_lock->number_of_txns= ~(0x0);
    				 }
    				 //else it is an intermediate locked txn
    				 assert(current_lock != NULL && "current lock is NULL ");
    				 master_sock.template get_extension<amba::amba_lock>(lock_ext, *current_trans);
    				 //set the value of locked extension
    				 lock_ext->value = current_lock;
    				 master_sock.template validate_extension<amba::amba_lock>(*current_trans);
    				 locked_txn_counter++;
    				 break;
				 }
			default:
				assert("Lock value not supported");
		}
		
		///Check the Cache support lines
		unsigned int cache = m_AWCACHE.read();

		if((cache&0x01)==0x01)
		{
			master_sock.template validate_extension<amba::amba_bufferable>(*current_trans);
		}
		if((cache&0x02)==0x02)
		{
			master_sock.template validate_extension<amba::amba_cacheable>(*current_trans);
			if((cache&0x04)==0x04)
			{
				master_sock.template validate_extension<amba::amba_read_allocate>(*current_trans);
				//This is a data-only extension
			}
			if((cache&0x08)==0x08)
			{
				master_sock.template validate_extension<amba::amba_write_allocate>(*current_trans);
				//This is a data-only extension
			}
		}
		

		//check the protection support lines
		unsigned int prot = m_AWPROT.read();
		AMBA_DUMP("Got the Write address request with following attributes: "<<std::endl<<
				"AWID=" <<(unsigned int) m_id->value<<std::endl<<
				"Address=" <<address<<", AWSIZE ="<<aw_size<<", AWLEN="<<(unsigned int )(m_AWLEN.read())<<std::endl<<
				"AWBURST="<<burst_type<<", AWLOCK="<<lock<<", AWCACHE= "<< cache<<", AWPROT="<<prot);
		if((prot&0x01)==0x01)
			master_sock.template validate_extension<amba::amba_privileged>(*current_trans);
		else
			master_sock.template invalidate_extension<amba::amba_privileged>(*current_trans);
		if((prot&0x04)==0x04)
			master_sock.template validate_extension<amba::amba_instruction>(*current_trans);
		else
			master_sock.template invalidate_extension<amba::amba_instruction>(*current_trans);

		if((prot&0x02)==0x02)
		{
			master_sock.template validate_extension<amba::amba_secure_access>(*current_trans);
		}
		tlm::tlm_phase ph= tlm::BEGIN_REQ;
		sc_core::sc_time delay= sc_core::SC_ZERO_TIME;

		//put this transaction into the map, with data_counter=0 as no data has been sent till now, only req is being sent
		payload_info info;
		info.trans=current_trans;
		info.data_counter=0;

		if(write_payload_list.find(m_id->value)==write_payload_list.end())
			AMBA_DUMP("It is a request with new tagid");
		else
			AMBA_DUMP("Request with same tag-id exists");
		write_payload_list[m_id->value].push_back(info);
		//Sending the write req
		aw_ready=false;

		tlm::tlm_sync_enum retval= master_sock->nb_transport_fw(*current_trans, ph, delay);
		switch(retval)
		{
			case tlm::TLM_ACCEPTED:{
				assert(ph== tlm::BEGIN_REQ && "Unexpected phase");
    			///Ok lower the AWREADY low until the current Write-request is in progress
				aw_ready=false;
				break;
			}
			case tlm::TLM_UPDATED:{
				assert(ph == tlm::END_REQ && "Unexpected phase" );
				//ok the slave has accepted the write-addr-request, ready for the next
				aw_ready=true;
				break;
			}
			default:
				assert("Wrong tlm_sync_enum returned");
		}
		aw_ready_ev.notify();
}

//This method is sensitive to aw_ready_ev which is notified in WriteAddrValid,
//WriteAddrValid is called from begin_req_method which wont be scheduled if in RESET state
//Hence this function wont get called in RESET state
template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::WriteAddrReady()
{
  m_AWREADY.write(aw_ready);
  AMBA_DUMP("Written on AWREADY, value="<<(unsigned int)aw_ready);

  ///Now check if the WDataValid is high already?
  if((m_AWID.read()==m_WID.read()) //if both the address and data phase rises together
  && (aw_ready==true && w_valid==true)
   )
  {
	  unsigned int data_counter=((write_payload_list[m_WID.read()]).front()).data_counter;
	  if(data_counter==0)//check if we don't have sent the DATA phase already
	  {	//Ok this is a case where WVALID was high before AWVALID
		  AMBA_DUMP("WVALID is high already before AWVALID, so send the data also");
		  WriteDataValid();
	  }
  }
}


/*
 * @brief WriteDataValid
 * 
 * @details This function is triggered when the RTL master  
 * uploads the data on the data lines.Adapter has to read and fwd it
 * 
 */
template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::WriteDataValid()
{
	unsigned int m_dataid= m_WID.read();
	unsigned int index=0;
	if(write_payload_list.find(m_dataid) == write_payload_list.end()
		||	write_payload_list[m_dataid].empty()==true)
	{
		AMBA_DUMP("No Address and Control phase sent for this transaction, so will send it later");
		return;
	}
	tlm::tlm_generic_payload *payload= NULL;
	unsigned int write_data_counter=0;
    for(index=0; index < write_payload_list[m_dataid].size(); index++ )
    {
	  //Read the tagid and find it in the bufferd transactions
	  payload= ((write_payload_list[m_dataid])[index]).trans;
	  write_data_counter= ((write_payload_list[m_dataid])[index]).data_counter;
	  if(write_data_counter < (unsigned int)payload->get_data_length() )
	  {//We are considering only that transaction for which data transfer is pending not the one which is waiting for brespValid
		  break;
	  }
    }
    assert((index !=write_payload_list[m_dataid].size() )&& "No correct txn found in the deque");
	// CARBON
// 	unsigned int byte_len=0;

// 		if(write_data_counter==0)
// 		{//ok this is the first transfer/beat of the burst
// 			byte_len=(BUSWIDTH/8);
// 			master_sock.reserve_be_size(*payload,byte_len);
// 		}
// 		else
// 		{
// 			byte_len=payload->get_byte_enable_length();
// 		}
	// END CARBON

		AMBA_DUMP("Write Data Validate request");
		//Read the Data channel signals


		///Write-strobe will be used for the
		// CARBON unsigned char write_strobe= m_WSTRB.read();
		unsigned int write_strobe= m_WSTRB.read();
		AMBA_DUMP("tag-id= "<<m_dataid <<", WSTRB= "<< (unsigned int)write_strobe);

		// CARBON
// 		for(unsigned int j=0; j<byte_len; j++)
// 		{
// 		        // CARBON unsigned char temp=(1<<j);
// 			unsigned int temp=(1<<j);
// 			if((write_strobe & temp)== temp)
// 			  payload->get_byte_enable_ptr()[j]= 0xff;
// 			else
// 			  payload->get_byte_enable_ptr()[j]=0x00;
// 		}
		// END CARBON
		unsigned char * dataptr= payload->get_data_ptr();
		unsigned int len= payload->get_data_length();
		sc_dt::uint64 addr=payload->get_address();
		//is it a narrow transfer
		unsigned int offset=(addr+write_data_counter)%((BUSWIDTH+7)/8);
		unsigned int array_entry=write_data_counter;
		amba::amba_wrap_offset *m_wrap_index;
		if(master_sock.template get_extension<amba::amba_wrap_offset>(m_wrap_index,*payload))
		{
			AMBA_DUMP("Wrapping Bursts");
			array_entry+=m_wrap_index->value;
			array_entry%=len;
		}
		amba::amba_burst_size* size;
		master_sock.template get_extension<amba::amba_burst_size>(size,*payload);
		assert(((payload->get_data_length()-write_data_counter)>= size->value) && "Memory allocation error");

        write_strobe>>=offset;

		for(w_bytesTransferred=0;(w_bytesTransferred< size->value) && ((offset+w_bytesTransferred)<((BUSWIDTH+7)/8)) && (w_bytesTransferred+array_entry<payload->get_data_length()); w_bytesTransferred++)
		{
			 AMBA_DUMP("Put "<<(m_WDATA.read().range((w_bytesTransferred+offset+1)*8-1,(w_bytesTransferred+offset)*8).to_uint())<<" into array index "<<(w_bytesTransferred+array_entry));
			 dataptr[array_entry+w_bytesTransferred]=(unsigned char)(m_WDATA.read().range((w_bytesTransferred+offset+1)*8-1,(w_bytesTransferred+offset)*8).to_uint());
			 // CARBON
			 if (write_strobe & (1 << w_bytesTransferred)) 
			   payload->get_byte_enable_ptr()[array_entry+w_bytesTransferred]=0xff;
			 else 
			   payload->get_byte_enable_ptr()[array_entry+w_bytesTransferred]=0x00;
			 // END CARBON
		}
		AMBA_DUMP("Total bytes Written in this burst="<<w_bytesTransferred);

		//read the WLAST
		tlm::tlm_phase ph;
		bool m_last= m_WLAST.read();
		AMBA_DUMP("WLAST= "<<(unsigned int) m_last);
		if(m_last)
		{
			ph = amba::BEGIN_LAST_DATA;
		}
		else
		{
			ph = amba::BEGIN_DATA;
		}

		sc_core::sc_time delay=sc_core::SC_ZERO_TIME;
		//with all these settings put the transaction into the buffer again;
		tlm::tlm_sync_enum retval= master_sock->nb_transport_fw(*payload, ph, delay);
		write_data_counter+=w_bytesTransferred; //increment the counter by how much more bytes have been copied into ptr
		((write_payload_list[m_dataid])[index]).data_counter=write_data_counter;
		w_bytesTransferred=0;
		w_ready=false;
		switch(retval)
		{
			case tlm::TLM_UPDATED:{
				assert((ph==amba::END_DATA) && "Unexpected phase");
				AMBA_DUMP("TLM_UPDATED, Wready=true");
				w_ready=true;
				break;
			}
			case tlm::TLM_ACCEPTED:{
			        assert((ph== amba::BEGIN_DATA || ph==amba::BEGIN_LAST_DATA) && "Unexpected phase");
				AMBA_DUMP("TLM_ACCEPTED, Wready=false");
				w_ready=false;
				break;
			}
		default: { // CARBON prevent warning
		  break;
		}
		}
		w_ready_ev.notify();
}


//The event on which this method is sensitive is scheduled from WriteDataValid,
//which is called from begin_data_method which will not be scheduled when in RESET state
template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::WriteDataReady()
{
  m_WREADY.write(w_ready);
  AMBA_DUMP("Written on WREADY, value="<<(unsigned int)w_ready);

}

/*
 * @brief WriteRespReady
 *
 * @details This funtion is triggered when Master sets the
 * BREADY high indicating that it has accepeted the response
 * or it is ready to accept it
 */

template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void  AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::WriteRespReady()
{
	if((b_resp_accepted==false) && (b_resp_valid==true))
	{	//ok the master is now accpeted the response we proposed, intimate the slave for it

		assert((write_payload_list.find(b_id)!=write_payload_list.end()) && "This bid doesn't exist in the map.");
		assert((write_payload_list[b_id].empty()==false) && "The queue is empty");
		tlm::tlm_generic_payload * payload =  ((write_payload_list[b_id]).front()).trans;
		assert(payload != NULL && "NULL Transaction");

		AMBA_DUMP("Sending the END_RESP for the pending BRESP for write txn");
		tlm::tlm_phase ph= tlm::END_RESP;
		sc_core::sc_time delay=sc_core::SC_ZERO_TIME;
		tlm::tlm_sync_enum retval=master_sock->nb_transport_fw(*payload, ph, delay);
		assert((retval== tlm::TLM_ACCEPTED) && "Unexpected tlm_sync_enum");

		//release this transaction now and remove from the buffer too
		//(write_payload_list[b_id]).erase(index);
		(write_payload_list[b_id]).pop_front();
		if((write_payload_list[b_id].empty())== true)
		{
			write_payload_list.erase(b_id);
		}
		master_sock.release_transaction(payload);
		b_resp_accepted=true;
	}
}

template< typename MODULE, unsigned int BUSWIDTH, unsigned int ADDRWIDTH, unsigned int WIDWIDTH, unsigned int RIDWIDTH, int SIZEWIDTH, bool FORCE_BIGUINT>
void AXI_Master_RTL_CT_Adapter< MODULE,BUSWIDTH,ADDRWIDTH,WIDWIDTH,RIDWIDTH, SIZEWIDTH ,FORCE_BIGUINT>::WriteRespValid()
{
	m_BVALID.write(b_resp_valid);
	AMBA_DUMP( "Written on BVALID, value="<<(unsigned int)b_resp_valid);
}

#endif /*AXI_MASTER_RTL_CT_ADAPTER_H_*/
