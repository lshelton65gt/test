// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _CARBONTCMS2T_H_
#define _CARBONTCMS2T_H_

#include "maxsim.h"
#include "carbon_arm_adaptor.h"

class CarbonXtorEvent;
class CarbonTCMS2T
{
public:
  CarbonTCMS2T(sc_mx_module* carbonComp,
               const char *xtorName,
               CarbonObjectID **carbonObj,
               CarbonPortFactory *portFactory);
  ~CarbonTCMS2T();
  sc_mx_signal_slave *carbonGetPort(const char *name);

  void communicate();
  void update();
  void init();
  void terminate();
  void reset(MxResetLevel level, const MxFileMapIF *filelist);

  void setParameter(const string &name, const string &value);
  CarbonDebugAccessStatus debugAccess(CarbonDebugAccessDirection dir, MxU64 startAddress, MxU32 numBytes, MxU8 *data, MxU32 *numBytesProcessed, MxU32 *ctrl);

  CASIPortIF* getTmPort();

  bool saveData(eslapi::CASIODataStream& os);
  bool restoreData(eslapi::CASIIDataStream& is);

private:
  void *mArmAdaptor;
};

#endif // _CARBONTCMS2T_H_
