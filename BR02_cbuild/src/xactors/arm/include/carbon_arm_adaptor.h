#ifndef _carbon_arm_adaptor_h_
#define _carbon_arm_adaptor_h_ 1

#include "maxsim.h"
#include "carbon/CarbonPlatform.h"
#include "carbon/CarbonDebugAccess.h"

struct carbon_model_descr;

typedef struct carbon_model_descr CarbonObjectID;
typedef class CarbonNet CarbonNetID;

class CarbonXtorAdaptorToVhmPort 
{
public:
  virtual void driveSignal(MxU32 value, MxU32* extValue) = 0;
  void setProperties(MxSignalProperties *signalprop) {mProp = *signalprop; };
  MxSignalProperties* getProperties() {return &mProp; }
private:
  MxSignalProperties mProp;
};

typedef CarbonDebugAccessStatus (CarbonDebugAccessFunction)(sc_mx_m_base *comp, CarbonDebugAccessDirection dir, MxU64 addr, MxU32 numBytes, MxU8* data, MxU32* numBytesProcessed, MxU32* ctrl);

typedef CarbonXtorAdaptorToVhmPort *(CarbonPortFactory)(sc_mx_m_base *owner, const char *xtor, const char *port, CarbonObjectID **carbonObject);
typedef MxStatus (CarbonDebugReadWriteFunction)(sc_mx_m_base *comp, MxU64 addr, MxU32* value, MxU32* ctrl);
typedef MxStatus (CarbonDebugTransactionFunction)(sc_mx_m_base *comp, MxTransactionInfo* info);
typedef void (CarbonCombFunction)(void *comp, bool, bool, bool, bool );

struct CarbonDebugAccessCallbacks {
  sc_mx_m_base*                 owner;
  CarbonDebugAccessFunction*    debugAccess;
  CarbonDebugReadWriteFunction* writeDbg;
  CarbonDebugReadWriteFunction* readDbg;
  CarbonDebugTransactionFunction* debugTransaction;
};

MxStatus CarbonDebugFunctionsToDebugAccess(const CarbonDebugAccessCallbacks& cb, CarbonDebugAccessDirection dir, uint32_t buswidth, MxU64 addr, MxU32* value, MxU32* ctrl);
MxStatus CarbonDebugFunctionsToDebugAccess(const CarbonDebugAccessCallbacks& cb, MxTransactionInfo* info);

#endif
