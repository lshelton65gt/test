// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef _CARBONTCMT2S_H_
#define _CARBONTCMT2S_H_

#include "maxsim.h"
#include "carbon_arm_adaptor.h"

class CarbonXtorEvent;
class CarbonTCMT2S
{
public:
  CarbonTCMT2S(sc_mx_module* carbonComp,
               const char *xtorName,
               CarbonObjectID **carbonObj,
               CarbonPortFactory *portFactory,
               CarbonDebugReadWriteFunction* readDebug, 
               CarbonDebugReadWriteFunction* writeDebug,
               CarbonDebugTransactionFunction* debugTrans);
  CarbonTCMT2S(sc_mx_module* carbonComp,
               const char *xtorName,
               CarbonObjectID **carbonObj,
               CarbonPortFactory *portFactory,
               CarbonDebugAccessFunction* debugAccess);
  ~CarbonTCMT2S();
  sc_mx_signal_slave *carbonGetPort(const char *name);

  void communicate();
  void update();
  void init();
  void terminate();
  void reset(MxResetLevel level, const MxFileMapIF *filelist);

  void setParameter(const string &name, const string &value);

  bool saveData(eslapi::CASIODataStream& os);
  bool restoreData(eslapi::CASIIDataStream& is);

private:
  void *mArmAdaptor;
};

#endif // _CARBONTCMT2S_H_
