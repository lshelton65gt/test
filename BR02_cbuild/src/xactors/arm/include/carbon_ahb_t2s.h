
#ifndef _carbon_ahb_t2s_h_
#define _carbon_ahb_t2s_h_ 1

#ifdef CARBON
#undef CARBON
#endif
#define CARBON 1

#include "xactors/arm/include/carbon_arm_adaptor.h"
#include "xactors/arm/AHB/AHBT2S/AHBT2S.h"

#endif // _carbon_ahb_t2s_h_
