#ifndef _CarbonAxiFS2T_h_
#define _CarbonAxiFS2T_h_ 1

#include "maxsim.h"
#include "carbon_arm_adaptor.h"

class CarbonXtorEvent;
class CarbonAxiFS2T
{
public:
	CarbonAxiFS2T(sc_mx_module* carbonComp,
		const char *xtorName,
		CarbonObjectID **carbonObj,
		CarbonPortFactory *portFactory,
		CarbonCombFunction* combFunc);
	~CarbonAxiFS2T();
	sc_mx_signal_slave *carbonGetPort(const char *name);

	void communicate() {}
	void update();
	void sendDrive();
	void sendARDrive();
	void sendAWDrive();
	void sendWDrive();
	void sendNotify();
	void sendRNotify();
	void sendBNotify();

	void forwardValid();
	void forwardReady();

	void init();
	void terminate();
	void reset(MxResetLevel level, const MxFileMapIF *filelist);

	void setParameter(const string &name, const string &value);

	MxStatus readDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
	MxStatus writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl);

        
	CASIPortIF* getTmPort();
	MxStatus debugTransaction(MxTransactionInfo*);

        MxStatus debugMemWrite(MxU64 startAddress, MxU32 unitsToWrite, MxU32 unitSizeInBytes, const MxU8 *data, MxU32 *actualNumOfUnitsWritten, MxU32 secureFlag);
        MxStatus debugMemRead(MxU64 startAddress, MxU32 unitsToRead, MxU32 unitSizeInBytes, MxU8 *data, MxU32 *actualNumOfUnitsRead, MxU32 secureFlag);
	CarbonDebugAccessStatus debugAccess(CarbonDebugAccessDirection dir, MxU64 startAddress, MxU32 numBytes, MxU8 *data, MxU32 *numBytesProcessed, MxU32 *ctrl);

	int getNumRegions();
	void getAddressRegions(uint64_t* start, uint64_t* size, std::string* name);
        void carbonSetPortWidth(const char* name, uint32_t bitWidth);

        bool saveData(eslapi::CASIODataStream& os);
        bool restoreData(eslapi::CASIIDataStream& is);

	bool doneDrive;
	bool doneNotify;

	bool awChanged;
	bool arChanged;
	bool wChanged;
	bool bReadyChanged;
	bool rReadyChanged;

	CarbonXtorEvent* awChangedFunc;
	CarbonXtorEvent* arChangedFunc;
	CarbonXtorEvent* wChangedFunc;
	CarbonXtorEvent* rReadyChangedFunc; 
	CarbonXtorEvent* bReadyChangedFunc;

private:
	void *mArmAdaptor;
};

#endif // _CarbonAxiFS2T_h_ 1
