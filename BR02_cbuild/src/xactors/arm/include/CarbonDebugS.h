#ifndef _CarbonDebugS_h_
#define _CarbonDebugS_h_ 1

#include "maxsim.h"
#include "carbon_arm_adaptor.h"

class DEBUGs;

class CarbonDebugS
{
 public:
    // Constructor for used with writeDbg/readDbg
    CarbonDebugS(sc_mx_module* carbonComp,
                      const char *xtorName,
                      CarbonObjectID **carbonObj,
                      CarbonPortFactory *portFactory, 
                      CarbonDebugReadWriteFunction *readDebug,
                      CarbonDebugReadWriteFunction *writeDebug);

    // Constructor for used with debugAccess
    CarbonDebugS(sc_mx_module* carbonComp,
                      const char *xtorName,
                      CarbonObjectID **carbonObj,
                      CarbonPortFactory *portFactory, 
                      CarbonDebugAccessFunction *debugAccess);

    ~CarbonDebugS();
    sc_mx_signal_slave *carbonGetPort(const char *name);

    void communicate();
    void update();

    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);

    void setParameter(const string &name, const string &value);

    int getNumRegions();
    void getAddressRegions(uint64_t* start, uint64_t* size, string* name);

    bool saveData(eslapi::CASIODataStream& os);
    bool restoreData(eslapi::CASIIDataStream& is);

 private:
    DEBUGs *mArmAdaptor;
};

#endif // _CarbonDebugS_h_ 1
