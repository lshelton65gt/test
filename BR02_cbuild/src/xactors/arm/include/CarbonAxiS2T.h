#ifndef _CarbonAxiS2T_h_
#define _CarbonAxiS2T_h_ 1

#include "maxsim.h"
#include "carbon_arm_adaptor.h"

class CarbonAxiS2T
{
 public:
    CarbonAxiS2T(sc_mx_module* carbonComp,
                       const char *xtorName,
                       CarbonObjectID **carbonObj,
                       CarbonPortFactory *portFactory);
    ~CarbonAxiS2T();
    sc_mx_signal_slave *carbonGetPort(const char *name);

    void communicate();
    void update();

    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);

    void setParameter(const string &name, const string &value);

    MxStatus readDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
    MxStatus writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
    MxStatus debugTransaction(MxTransactionInfo*);
    // The purpose of the following methods is to be used by the CADI interface to provide backdoor memory access to components connected by this port
    int getNumRegions ();
    void getAddressRegions (MxU64* start, MxU64* size, string * name);
    MxStatus debugMemWrite(MxU64 startAddress, MxU32 unitsToWrite, MxU32 unitSizeInBytes, const MxU8 *data, MxU32 *actualNumOfUnitsWritten, MxU32 secureFlag);
    MxStatus debugMemRead(MxU64 startAddress, MxU32 unitsToRead, MxU32 unitSizeInBytes, MxU8 *data, MxU32 *actualNumOfUnitsRead, MxU32 secureFlag);
    CarbonDebugAccessStatus debugAccess(CarbonDebugAccessDirection dir, MxU64 startAddress, MxU32 numBytes, MxU8 *data, MxU32 *numBytesProcessed, MxU32 *ctrl);
    void carbonSetPortWidth(const char* name, uint32_t bitWidth);

 private:
    void *mArmAdaptor;
};

#endif // _CarbonAxiS2T_h_ 1
