#ifndef _CarbonAxiT2S_h_
#define _CarbonAxiT2S_h_ 1

#include "maxsim.h"
#include "carbon_arm_adaptor.h"

class CarbonAxiT2S
{
 public:
    CarbonAxiT2S(sc_mx_module* carbonComp,
                 const char *xtorName,
                 CarbonObjectID **carbonObj,
                 CarbonPortFactory *portFactory,
                 CarbonDebugReadWriteFunction* readDebug, 
                 CarbonDebugReadWriteFunction* writeDebug,
                 CarbonDebugTransactionFunction* debugTrans);
    CarbonAxiT2S(sc_mx_module* carbonComp,
                 const char *xtorName,
                 CarbonObjectID **carbonObj,
                 CarbonPortFactory *portFactory,
                 CarbonDebugAccessFunction* debugAccess);
    ~CarbonAxiT2S();
    sc_mx_signal_slave *carbonGetPort(const char *name);

    void communicate();
    void update();

    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);

    void setParameter(const string &name, const string &value);

    int getNumRegions();
    void getAddressRegions(uint64_t* start, uint64_t* size, string* name);
    void carbonSetPortWidth(const char* name, uint32_t bitWidth);

 private:
    void *mArmAdaptor;
};

#endif // _CarbonAxiT2S_h_ 1
