
#ifndef _carbon_ahb_s2t_h_
#define _carbon_ahb_s2t_h_ 1

#ifdef CARBON
#undef CARBON
#endif
#define CARBON 1

#include "xactors/arm/include/carbon_arm_adaptor.h"
#include "xactors/arm/AHB/AHBS2T/AHBS2T.h"

#endif // _carbon_ahb_s2t_h_
