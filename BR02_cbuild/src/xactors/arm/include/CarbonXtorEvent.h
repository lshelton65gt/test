#ifndef XTOREVENT_H
#define XTOREVENT_H

class CarbonXtorEvent
{
public:
	CarbonXtorEvent(void) {}
	virtual ~CarbonXtorEvent(void) {}

	virtual void operator()() = 0;
};

#endif
