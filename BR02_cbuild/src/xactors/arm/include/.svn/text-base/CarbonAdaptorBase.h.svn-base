// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#include "carbon/CarbonPlatform.h"
#ifndef CARBON_NO_LICENSE
#include "util/UtCLicense.h"
#endif
#include "carbon_arm_adaptor.h"
#include <stdio.h>
#include <stdarg.h>
#include "maxsim.h"

#ifndef _CarbonAdaptorBase_
#define _CarbonAdaptorBase_

// Base Class for ARM adaptors. 
class CarbonAdaptorBase {
public :

  //! Constructor
  CarbonAdaptorBase(sc_mx_module* carbon_component, const string& xtor_name, 
                    CarbonObjectID** p_carbon_obj ) : 
    mCarbonComponent(carbon_component), mXtorName(xtor_name), mpCarbonObject(p_carbon_obj),
    mTSlavePort(0), mTMasterPort(0)
#ifndef CARBON_NO_LICENSE
, mCarbonLicense(0)
#endif
 {}
   
  //! Destructor
  virtual ~CarbonAdaptorBase() {}

  //! Dummy init method for Carbon model ports.
  /*!
    The reason for this dummy method is that the adaptor code 
    from arm calls the method. Adding it to the base class avoids some ifdefs in the code.
  */
  void init() {}

  //! Dummy reset method for Carbon model ports.
  /*!
    The reason for this dummy method is that the adaptor code 
    from arm calls the method. Adding it to the base class avoids some ifdefs in the code.
  */
  void reset(MxResetLevel, const MxFileMapIF*) {}

  //! Dummy terminate method for Carbon model ports.
  /*!
    The reason for this dummy method is that the adaptor code 
    from arm calls the method. Adding it to the base class avoids some ifdefs in the code.
  */
  void terminate() {}

  //! This implements the Components message method
  /*!
    \param type Severity of message
    \param fmt Format string
  */
  void message( MxMessageType type, const char *fmt, ... )
  {
    va_list ap;
    char buff[256];
    va_start(ap, fmt);
    vsnprintf(buff, sizeof(buff), fmt, ap);
    mCarbonComponent->message(type, buff);
    va_end(ap);
  }

  //! Register Transaction Port. Signal ports will be connected using the port factory, so we ignore those
  /*!
    \param port Handle to port object
    \param name Port name
  */
  void registerPort(sc_mx_p_base* port, const string& name)
  {
    (void) name;
    MxInterfaceType portType = port->getType();
    if ( portType == MX_TRANSACTION_SLAVE || portType == MX_TRANSACTION_MASTER ) {
      // register the port with the owning Carbon component
      mCarbonComponent->registerPort(port, mXtorName.c_str());
    }

    if ( portType == MX_TRANSACTION_SLAVE ) {
      // Save Slave port, so we can make some generic calls from the adaptor base class
      mTSlavePort = dynamic_cast<CASITransactionSlave*>(port);
    }
    else if ( portType == MX_TRANSACTION_MASTER ) {
      mTMasterPort = dynamic_cast<MxTransactionMasterPort*>(port);
    }
  }
  
  //! Dummy registerPort method for Carbon model ports.
  /*!
    The reason for the dummy registerPort method is that the adaptor code 
    from arm calls the method. Adding it to the base class avoids some ifdefs in the code.
  */
  void registerPort(CarbonXtorAdaptorToVhmPort*, const string&)
  { }
  
  //! Returns instance ID of the Carbon component
  /*!
    \return Instance ID of the Carbon component
  */
  string getInstanceID()
  {
    return mCarbonComponent->getInstanceID();
  }

  //! Dummy defineParameter method
  /*!
    The reason for the dummy defineParameter method is that the adaptor code 
    from arm calls the method. Adding it to the base class avoids some ifdefs in the code.
  */
  void defineParameter(const string &, const string &, 
                       const MxParameterProperties *prop = NULL)
  { (void)prop; }
  void defineParameter(const string &, const string &, 
                       MxParameterType, bool is_runtime = true,
                       bool is_private = false, bool is_readonly = false)
  { (void) is_runtime; (void) is_private; (void) is_readonly; }
  
  //! Dummy setParameter method
  /*!
    The reason for the dummy setParameter method is that the adaptor code 
    from arm calls the method. Adding it to the base class avoids some ifdefs in the code.
  */
  void setParameter(const string&, const string&)
  { }

  //! Number of slave memory regions
  virtual int getNumSlaveRegions()
  {
    if (mTSlavePort)
      return mTSlavePort->getNumRegions();
    return 0;
  }

  //! Slave Address regions
  virtual void getSlaveAddressRegions(uint64_t* start, uint64_t* size, string* name)
  {
    if (mTSlavePort)
      mTSlavePort->getAddressRegions(start, size, name);
  }

  //! Number of slave memory regions
  virtual int getNumMasterRegions()
  {
    if (mTMasterPort)
      return mTMasterPort->getNumRegions();
    return 0;
  }

  //! Slave Address regions
  virtual void getMasterAddressRegions(uint64_t* start, uint64_t* size, string* name)
  {
    if (mTMasterPort)
      mTMasterPort->getAddressRegions(start, size, name);
  }

  //! Utility function to calculate the next power of 2
  static uint32_t nextPo2(uint32_t k)
  {
    if (k == 0) return 1;
    k--;
    for (int i=1; i<32; i<<=1)
      k = k | k >> i;
    return k+1;
  }
  

protected :
  //! Pointer to the owning Carbon component
  sc_mx_module*      mCarbonComponent;
  //! Name of the Adaptor instance
  string             mXtorName;
  //! Handle to the Carbon model
  CarbonObjectID**   mpCarbonObject;
  //! Transaction slave port if any
  CASITransactionSlave* mTSlavePort;
  //! Transaction master port if any
  MxTransactionMasterPort* mTMasterPort;

#ifndef CARBON_NO_LICENSE
  //! Handle to the Carbon Lincense object
  CarbonLicenseData* mCarbonLicense;
#endif
};

#endif
