#ifndef _CarbonAhbMasterS2T_h_
#define _CarbonAhbMasterS2T_h_ 1

#include "maxsim.h"
#include "carbon_arm_adaptor.h"

class AHBS2T;

class CarbonAhbMasterS2T
{
 public:
    CarbonAhbMasterS2T(sc_mx_module* carbonComp,
                       const char *xtorName,
                       CarbonObjectID **carbonObj,
                       CarbonPortFactory *portFactory,
                       bool isAhbLite);
    ~CarbonAhbMasterS2T();
    sc_mx_signal_slave *carbonGetPort(const char *name);

    void communicate();
    void update();

    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);

    void setParameter(const string &name, const string &value);

    MxStatus readDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
    MxStatus writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl);

    CarbonDebugAccessStatus debugAccess(CarbonDebugAccessDirection dir,
                                        uint64_t startAddress, 
                                        uint32_t numBytes, 
                                        uint8_t* data, 
                                        uint32_t* numBytesProcessed,
                                        uint32_t*);

    MxStatus debugMemWrite(MxU64 startAddress, MxU32 unitsToWrite, MxU32 unitSizeInBytes, const MxU8 *data, MxU32 *actualNumOfUnitsWritten, MxU32 secureFlag);
    MxStatus debugMemRead(MxU64 startAddress, MxU32 unitsToRead, MxU32 unitSizeInBytes, MxU8 *data, MxU32 *actualNumOfUnitsRead, MxU32 secureFlag);
    MxTransactionMasterPort* getTmPort();
    void carbonSetPortWidth(const char* name, uint32_t bitWidth);
    int getNumRegions();
    void getAddressRegions(uint64_t* start, uint64_t* size, string* name);

 private:
    AHBS2T *mArmAdaptor;
};

#endif // _CarbonAhbMasterS2T_h_ 1
