#ifndef _CarbonAxiFT2S_h_
#define _CarbonAxiFT2S_h_ 1

#include "maxsim.h"
#include "carbon_arm_adaptor.h"

class CarbonXtorEvent;
class CarbonAxiFT2S
{
 public:
    CarbonAxiFT2S(sc_mx_module* carbonComp,
                 const char *xtorName,
                 CarbonObjectID **carbonObj,
                 CarbonPortFactory *portFactory,
                 CarbonDebugReadWriteFunction* readDebug, 
                 CarbonDebugReadWriteFunction* writeDebug,
                 CarbonDebugTransactionFunction* debugTrans,
                 CarbonCombFunction* combFunc);
    CarbonAxiFT2S(sc_mx_module* carbonComp,
                 const char *xtorName,
                 CarbonObjectID **carbonObj,
                 CarbonPortFactory *portFactory,
                 CarbonDebugAccessFunction* debugAccess, 
                 CarbonCombFunction* combFunc);
    ~CarbonAxiFT2S();
    sc_mx_signal_slave *carbonGetPort(const char *name);

    void backwardValid();
    void backwardReady();

    void communicate() {}
    void update();
    void sendDrive();
    void sendRDrive();
    void sendBDrive();
    void sendNotify();
    void sendARNotify();
    void sendAWNotify();
    void sendWNotify();

    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);

    void setParameter(const string &name, const string &value);

    int getNumRegions();
    void getAddressRegions(uint64_t* start, uint64_t* size, string* name);
    void carbonSetPortWidth(const char* name, uint32_t bitWidth);

    bool saveData(eslapi::CASIODataStream& os);
    bool restoreData(eslapi::CASIIDataStream& is);

    bool doneDrive;
    bool doneNotify;

    bool awReadyChanged;
    bool arReadyChanged;
    bool wReadyChanged;
    bool rChanged;
    bool bChanged;

    CarbonXtorEvent* awReadyChangedFunc;
    CarbonXtorEvent* arReadyChangedFunc;
    CarbonXtorEvent* wReadyChangedFunc;
    CarbonXtorEvent* rChangedFunc;
    CarbonXtorEvent* bChangedFunc;

private:
    void *mArmAdaptor;
};

#endif // _CarbonAxiFT2S_h_ 1
