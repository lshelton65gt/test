#ifndef _CarbonApbS2T_h_
#define _CarbonApbS2T_h_ 1

#include "maxsim.h"
#include "carbon_arm_adaptor.h"

class APBS2T;

class CarbonApbS2T
{
 public:
    CarbonApbS2T(sc_mx_module* carbonComp,
                      const char *xtorName,
                      CarbonObjectID **carbonObj,
                      CarbonPortFactory *portFactory,
                      bool apb3,
                      CarbonDebugReadWriteFunction *readDebug,
                      CarbonDebugReadWriteFunction *writeDebug);
    ~CarbonApbS2T();
    sc_mx_signal_slave *carbonGetPort(const char *name);

    void communicate();
    void update();

    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);

    void setParameter(const string &name, const string &value);

    int getNumRegions();
    void getAddressRegions(uint64_t* start, uint64_t* size, string* name);

    MxTransactionMasterPort* getTmPort();

    CarbonDebugAccessStatus debugAccess(CarbonDebugAccessDirection dir,
                                        uint64_t startAddress, 
                                        uint32_t numBytes, 
                                        uint8_t* data, 
                                        uint32_t* numBytesProcessed,
                                        uint32_t* ctrl); 

    bool saveData(eslapi::CASIODataStream& os);
    bool restoreData(eslapi::CASIIDataStream& is);

 private:
    APBS2T *mArmAdaptor;
};

#endif // _CarbonApbS2T_h_ 1
