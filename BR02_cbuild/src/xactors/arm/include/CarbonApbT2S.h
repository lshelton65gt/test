#ifndef _CarbonApbT2S_h_
#define _CarbonApbT2S_h_ 1

#include "maxsim.h"
#include "carbon_arm_adaptor.h"

class APBT2S;

class CarbonApbT2S
{
 public:
    // Constructor for used with writeDbg/readDbg
    CarbonApbT2S(sc_mx_module* carbonComp,
                      const char *xtorName,
                      CarbonObjectID **carbonObj,
                      CarbonPortFactory *portFactory, 
                      CarbonDebugReadWriteFunction *readDebug,
                      CarbonDebugReadWriteFunction *writeDebug);

    // Constructor for used with debugAccess
    CarbonApbT2S(sc_mx_module* carbonComp,
                      const char *xtorName,
                      CarbonObjectID **carbonObj,
                      CarbonPortFactory *portFactory, 
                      CarbonDebugAccessFunction *debugAccess);

    ~CarbonApbT2S();
    sc_mx_signal_slave *carbonGetPort(const char *name);

    void communicate();
    void update();

    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);

    void setParameter(const string &name, const string &value);

    int getNumRegions();
    void getAddressRegions(uint64_t* start, uint64_t* size, string* name);

    bool saveData(eslapi::CASIODataStream& os);
    bool restoreData(eslapi::CASIIDataStream& is);

 private:
    APBT2S *mArmAdaptor;
};

#endif // _CarbonApbT2S_h_ 1
