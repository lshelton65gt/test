// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2006 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
//

// Persister classes to make it possible to use a single block of code
// to save or restore status - eliminating the problems that occur 
// when a save function gets out of step with a restore function.
//
//  Note that some C compatible cores use #define to change bool to int, this
//       causes an operator overload clash as two persist(int & item) get
//       defined. The workaround is to use #ifndef bool around persist(bool & item)
//
//  Note that GCC requires the use of an extra 'typename' for iterators
//
// Intended for use like this...
//
#if 0
// Save the simulation status
bool
AHB::saveData(MxODataStream &data)
{
    persistOStream p(data);
    return persist(p);
}

// Restore simulation status
bool
AHB::restoreData(MxIDataStream &data)
{
    persistIStream p(data);
    return persist(p);
}

// Same code for both save and restore, so that they can never get out of step!
bool
AHB::persist(persister & p)
{
    if (! p.persistVersion(SRversion))
    {
        return false;
    }

    p.persist(acc);
    p.persist(bLastBeat);
    ......
    return true;
}
#endif

#ifndef persister_H
#define persister_H

#include <stdexcept>
#include <assert.h>
#include <map>
#include <vector>
#include <list>

/* 
   this is defined in SoCD, but the location moved since 7.0 and so including newer header files necessitated this to 
   point to the location below, instead of systemc/datatypes/int/sc_uint.h
*/

#include "sysc/datatypes/int/sc_uint.h"

class persister
{
protected:
    persister() {};
public:
    virtual ~persister() {}
    virtual void comment(string text) = 0;

    // Aggressive, versions must match exactly
    virtual bool persistVersion(MxU32 version) = 0;
    virtual bool persistVersion(std::string version) = 0;

    virtual void persist(MxU8 &item) = 0;
    virtual void persist(MxS8 &item) = 0;

    virtual void persist(MxU16 &item) = 0;
    virtual void persist(MxS16 &item) = 0;

    virtual void persist(MxU32 &item) = 0;
    virtual void persist(MxS32 &item) = 0;

    virtual void persist(MxU64 &item) = 0;
    virtual void persist(MxS64 &item) = 0;

#ifndef bool
    virtual void persist(bool &item) = 0;
#endif

    virtual void persist(float &item) = 0;
    virtual void persist(double &item) = 0;

    // Rather than (const char *) go for std::string
    // to encapsulate the allocation.
    virtual void persist(std::string &item) = 0;

    // Which Direction save or restore?
    virtual bool Saving(void) const = 0;
    virtual bool Restoring(void) const = 0;

    /*! \brief Break out the underlying MxIDataStream.
     *
     * \b PLEASE do \b NOT use this.
     * This only exists as a get out clause when dealing with existing restoreData
     * function implemented by code beyond our reach.
     *
     * \attention Only call this iff Restoring()
     * \see Restoring
     */
    virtual MxIDataStream& AsIStream(void) = 0;

    /*! \brief Break out the underlying MxODataStream.
    *
    * \b PLEASE do \b NOT use this.
    * This only exists as a get out clause when dealing with existing saveData
    * function implemented by code beyond our reach.
    *
    * \attention Only call this iff Saving()
    * \see Saving
    */
    virtual MxODataStream& AsOStream(void) = 0;

    // Generic block persist for simple types
    template <typename T> void persist(T item[], unsigned int itemCount)
    {
        for (unsigned int count = 0; count < itemCount; ++count)
        {
            persist(item[count]);
        }
    }

    // Generic list persist. (Obviously not pointers)
    template <typename T> void persist(std::list<T> & item)
    {
        MxU32 size = item.size(); // *not* const - following persist() can change it
        persist(size);

        if (Saving())
        {
            typename std::list<T>::iterator iter;
            for (iter = item.begin(); iter != item.end(); ++iter)
            {
                iter->persist(*this);
            }
        }
        else
        {
            item.clear();

            for (MxU32 count = 0; count < size; ++count)
            {
                T tmp;
                tmp.persist(*this);
                item.push_back(tmp);
            }
        }
    }

    // A type safe way of persisting enum
    template <typename T> void persistEnum(T & item)
    {
        MxU32 tmpItem = (MxU32)item;
        persist(tmpItem);
        item = (T)tmpItem;
    }

    // Support for SystemC arbitrary length unsigned int
    // There is an implicit maximum of 64 bits
    template <int W> void persist(sc_uint<W> & item)
    {
        if (Saving())
        {
            MxU64 tmp = item;
            persist(tmp);
        }
        else
        {
            MxU64 tmp(0);
            persist(tmp);
            item = tmp;
        }
    }

    // Persisting a vector
    // Note that GCC required the extra 'typename' on 'std::vector<VT>::iterator'
    template <typename VT> void persist(std::vector<VT> & item)
    {
        MxU32 size = item.size(); // *not* const - following persist() can change it
        persist(size);

        if (Saving())
        {
            typename std::vector<VT>::iterator iter;
            for (iter = item.begin(); iter != item.end(); ++iter)
            {
                persist(*iter);
            }
        }
        else
        {
            item.clear();

            for (MxU32 count = 0; count < size; ++count)
            {
                VT tmp;
                persist(tmp);
                item.push_back(tmp);
            }
        }
    }

    /*! \brief Type safe way to persist a pointer.
    *
    *  \attention Up to the caller to determine if persisting a pointer is sensible!
    *
    *  \param  item        Pointer to persist
    *
    *  \return true for success, false if map lookup failed.
    */
    template <typename T> void persist(T* & item)
    {
        MxU32 tmpItem = (MxU32)item;
        persist(tmpItem);
        item = (T*)tmpItem;
    }

    /*! \brief Type safe way to persist a pointer.
     *
     *  \attention Up to the caller to determine if persisting a pointer is sensible!
     *
     *  \param  item        Pointer to persist
     *
     *  \return true for success, false if map lookup failed.
     */
    template <typename T> void persistPointer(T* & item)
    {
        MxU32 tmpItem = (MxU32)item;
        persist(tmpItem);
        item = (T*)tmpItem;
    }

    // Persisting a list of pointers
    // Note that GCC required the extra 'typename' on 'std::list<T*>::iterator'
    /*! \brief Type safe way to persist a list of pointers.
     *
     *  \attention Up to the caller to determine if persisting pointers is sensible!
     *
     *  \attention When restoring, the list is cleared first, so be sure anything 
     *              you need to delete is done before calling this.
     *
     *  \param  item        List of pointers to persist
     *
     *  \return true for success
     */
    template <typename T> bool persistPointers(std::list<T*> & item)
    {
        MxU32 size = item.size(); // *not* const - following persist() can change it
        persist(size);

        if (Saving())
        {
            typename std::list<T*>::iterator iter;
            for (iter = item.begin(); iter != item.end(); ++iter)
            {
                persistPointer(*iter);
            }
        }
        else
        {
            item.clear();   // Be careful to delete pointers if needed before calling this!

            for (MxU32 count = 0; count < size; ++count)
            {
                T* tmp;
                persistPointer(tmp);
                item.push_back(tmp);
            }
        }

        return true;
    }


    /*! \brief Persist a pointer where on restore a new value is looked up in a map.
     *
     *  \param  mapOldToNew Map from old pointer to new pointer
     *  \param  item        Pointer to persist
     *
     *  \return true for success, false if map lookup failed.
     */
    template <typename T> bool persistMappedPointer(const std::map<T*,T*> & mapOldToNew, T* & item)
    {
        persistPointer(item);

        if (Restoring())
        {
            return hookupMappedPointer(mapOldToNew, item);
        }
        else
        {
            return true;
        }
    }

    // Persisting a list of mapped pointers
    // Note that GCC required the extra 'typename' on 'std::list<T*>::iterator'
    template <typename T> bool persistMappedPointers(const std::map<T*,T*> & mapOldToNew, std::list<T*> & item)
    {
        bool result(true);

        MxU32 size = item.size(); // *not* const - following persist() can change it
        persist(size);

        if (Saving())
        {
            typename std::list<T*>::iterator iter;
            for (iter = item.begin(); result && (iter != item.end()); ++iter)
            {
                result = persistMappedPointer(mapOldToNew, *iter);
            }
        }
        else
        {
            item.clear();

            for (MxU32 count = 0; result && (count < size); ++count)
            {
                T* tmp;
                result = persistMappedPointer(mapOldToNew, tmp);
                item.push_back(tmp);
            }
        }

        return result;
    }

    /*! \brief Persist an object that might be pointed to.
    *
    *  \param  mapOldToNew Map from old pointer to new pointer
    *  \param  item        Object to persist
    *
    *  \return true for success, false if object persist failed.
    */
    template <typename T> bool persistMappedObject(std::map<T*,T*> & mapOldToNew, T & item)
    {
        bool result(true);

        T * const newAddress(&item);
        T * oldAddress(&item);

        persistPointer(oldAddress);

        result = item.persist(*this);

        if (result && Restoring())
        {
            addMappedPointer(mapOldToNew, oldAddress, newAddress);
        }

        return result;
    }

    template <typename T> bool persistMappedObject(std::map<T*,T*> & mapOldToNew, T item[], MxU32 itemCount)
    {
        bool result(true);

        for (unsigned int count = 0; result && (count < itemCount); ++count)
        {
            result = persistMappedObject(mapOldToNew, item[count]);
        }

        return result;
    }

    // Generic block persist for objects that know how to persist
    template <typename T> bool persistObjects(T item[], unsigned int itemCount)
    {
        bool result(true);

        for (unsigned int count = 0; result && (count < itemCount); ++count)
        {
            result = item[count].persist(*this);
        }

        return result;
    }

    template <typename T> bool persistMappedPointer(const std::map<T*,T*> & mapOldToNew, T* item[], unsigned int itemCount)
    {
        bool result(true);

        for (unsigned int count = 0; result && (count < itemCount); ++count)
        {
            result = persistMappedPointer(mapOldToNew, item[count]);
        }

        return result;
    }

    // Generic block persist for objects that know how to persist
    template <typename VT> bool persistObjects(std::vector<VT> & item)
    {
        bool result(true);

        MxU32 size = item.size(); // *not* const - following persist() can change it
        persist(size);

        if (Saving())
        {
            typename std::vector<VT>::iterator iter;
            for (iter = item.begin(); result && (iter != item.end()); ++iter)
            {
                result = (*iter).persist(*this);
            }
        }
        else
        {
            item.clear();

            for (MxU32 count = 0; result && (count < size); ++count)
            {
                VT tmp;
                result = tmp.persist(*this);
                if (result)
                {
                    item.push_back(tmp);
                }
            }
        }

        return result;
    }

    /*! \brief Persist a pointer to an object that knows how to persist.
     *
     *  NB: Uses 'new' to create an object on restore (unless NULL)
     *
     *  \param  item        Pointer to persist and object.
     *
     *  \return true for success
     */
    template <typename T> bool persistPointedObject(T* & item)
    {
        bool result(true);

        persistPointer(item);

        if (NULL != item)
        {
            if (Restoring())
            {
                item = new T;
            }

            result = item->persist(*this);
        }

        return result;
    }
};

/*! \brief Add to pointer map
 *
 *  \param  mapOldToNew Map from old pointer to new pointer
 *  \param  oldPointer  Old pointer value
 *  \param  newPointer  New pointer value
 *
 *  \see persistMappedPointer
 */
template <typename T> void addMappedPointer(std::map<T * ,T * > & mapOldToNew, T* oldPointer, T* newPointer)
{
    if (NULL != oldPointer)
    {
        // Note that GCC required the extra 'typename' on 'map<T* , T * >::const_iterator'
        typename std::map<T* , T * >::const_iterator const iter = mapOldToNew.find(oldPointer);
        if (mapOldToNew.end() == iter)
        {
            (void)mapOldToNew.insert(std::pair<T*,T*>(oldPointer, newPointer));
        }
        else
        {
            assert(newPointer == iter->second);
        }
    }
}

/*! \brief Hook up a pointer where on restore a new value is looked up in a map.
 *
 *  \param  mapOldToNew Map from old pointer to new pointer
 *  \param  item        Pointer to hookup, value may be NULL
 *
 *  \return true for success, false if map lookup failed.
 */
template <typename T> bool hookupMappedPointer(const std::map<T*,T*> & mapOldToNew, T* & item)
{
    bool result(true);

    if (NULL != item)
    {
        // Note that GCC required the extra 'typename' on 'map<T*,T*>::const_iterator'
        typename std::map<T*,T*>::const_iterator const iter = mapOldToNew.find(item);
        if (mapOldToNew.end() != iter)
        {
            item = iter->second;
        }
        else
        {
            result = false;
        }
    }

    return result;
}

/*! \brief Hookup for an object that is pointed to and knows how to hookup.
 *
 *  \param  mapOldToNew Map from old pointer to new pointer
 *  \param  item        Pointer to object.
 *
 *  \return true for success
 */
template <typename T, typename I> bool hookupPointedObject(const std::map<T*,T*> & mapOldToNew, I* & item)
{
    bool result(true);

    if (NULL != item)
    {
        result = item->hookupPointers(mapOldToNew);
    }

    return result;
}


class persistIStream: public persister
{
protected:
    MxIDataStream & m_data;

    class dummyMxODataStream : public MxODataStream
    {
    public:
        dummyMxODataStream():MxODataStream(NULL) {}
    };
    static dummyMxODataStream dummyStream;

public:
    persistIStream(MxIDataStream & data):m_data(data)    {    }    

    virtual void comment(string text)   { persist(text); }

    virtual bool persistVersion(MxU32 version)
    {
        MxU32 restoredSRversion;                          
        m_data >> restoredSRversion;
        return (version == restoredSRversion);
    }
    virtual bool persistVersion(std::string version)
    {
        std::string ver;
        persist(ver);
        return (version == ver);
    }

    virtual void persist(MxU8 &item)    {   m_data >> item;   }
    virtual void persist(MxS8 &item)    {   m_data >> item;   }

    virtual void persist(MxU16 &item)   {   m_data >> item;   }
    virtual void persist(MxS16 &item)   {   m_data >> item;   }

    virtual void persist(MxU32 &item)   {   m_data >> item;   }
    virtual void persist(MxS32 &item)   {   m_data >> item;   }

    virtual void persist(MxU64 &item)   {   m_data >> item;   }
    virtual void persist(MxS64 &item)   {   m_data >> item;   }

#ifndef bool
    virtual void persist(bool &item)    {   m_data >> item;   }
#endif

    virtual void persist(float &item)   {   m_data >> item;   }
    virtual void persist(double &item)  {   m_data >> item;   }

    virtual void persist(std::string &item)  
    {   
        char * strItem;
        m_data >> strItem;   // *MUST* delete[] this
        item = strItem;
        delete[] strItem;
    }

    virtual bool Saving(void) const {return false;}
    virtual bool Restoring(void) const {return true;}
    virtual MxIDataStream& AsIStream(void) { return m_data; }
    virtual MxODataStream& AsOStream(void) 
    { 
        assert(false); 
        throw std::runtime_error("Misuse of persistIStream.");
        return dummyStream;
    }

private:    //lint !e1736
    // Scott Meyers Effective C++
    // Every class should have a default constructor.
    // The constructor is private to stop anyone using it, and not defined either.
    // Item 27:  Explicitly disallow use of implicitly generated member functions you don't want
    persistIStream(); //lint !e1704 constructor having private access
    // Scott Meyers Effective C++
    // Item 11:  Declare a copy constructor and an assignment operator for classes with dynamically allocated memory
    persistIStream(const persistIStream& rhs); //lint !e1704 constructor having private access
    // Declare an assignment operator
    persistIStream& operator=(const persistIStream& rhs);
};

class persistOStream: public persister
{
protected:
    MxODataStream & m_data;

public:
    persistOStream(MxODataStream & data):m_data(data)    {    }    

    virtual void comment(string text)   { persist(text); }

    virtual bool persistVersion(MxU32 version)
    {
        m_data << version;
        return true;
    }
    virtual bool persistVersion(std::string version)
    {
        m_data << version.c_str() ;
        return true;
    }

    virtual void persist(MxU8 &item)    {   m_data << item;   }
    virtual void persist(MxS8 &item)    {   m_data << item;   }

    virtual void persist(MxU16 &item)   {   m_data << item;   }
    virtual void persist(MxS16 &item)   {   m_data << item;   }

    virtual void persist(MxU32 &item)   {   m_data << item;   }
    virtual void persist(MxS32 &item)   {   m_data << item;   }

    virtual void persist(MxU64 &item)   {   m_data << item;   }
    virtual void persist(MxS64 &item)   {   m_data << item;   }

#ifndef bool
    virtual void persist(bool &item)    {   m_data << item;   }
#endif

    virtual void persist(float &item)   {   m_data << item;   }
    virtual void persist(double &item)  {   m_data << item;   }

    virtual void persist(std::string &item)    {    m_data << item.c_str(); }

    virtual bool Saving(void) const {return true;}
    virtual bool Restoring(void) const {return false;}
    virtual MxIDataStream& AsIStream(void) { assert(false);  throw std::runtime_error("Misuse of persistOStream."); }
    virtual MxODataStream& AsOStream(void) { return m_data; }


private:    //lint !e1736
    // Scott Meyers Effective C++
    // Every class should have a default constructor.
    // The constructor is private to stop anyone using it, and not defined either.
    // Item 27:  Explicitly disallow use of implicitly generated member functions you don't want
    persistOStream(); //lint !e1704 constructor having private access
    // Scott Meyers Effective C++
    // Item 11:  Declare a copy constructor and an assignment operator for classes with dynamically allocated memory
    persistOStream(const persistOStream& rhs); //lint !e1704 constructor having private access
    // Declare an assignment operator
    persistOStream& operator=(const persistOStream& rhs);
};

class debugPersistIStream: public persistIStream
{
protected:
    
    class dummyMxODataStream : public MxODataStream
    {
    public:
        dummyMxODataStream() : MxODataStream(NULL) {}
    };
    dummyMxODataStream dummyStream;

public:
    debugPersistIStream(MxIDataStream & data):persistIStream(data)    {    }    

    virtual bool persistVersion(MxU32 version)
    {
        MxU32 restoredSRversion;                          
        m_data >> restoredSRversion;
        return (version == restoredSRversion);
    }
    virtual bool persistVersion(std::string version)
    {
        std::string ver;
        persist(ver);
        return (version == ver);
    }

    virtual void comment(string text)   { persist(text); }

    virtual void persist(MxU8 &item)    {   m_data >> item;   }
    virtual void persist(MxS8 &item)    {   m_data >> item;   }

    virtual void persist(MxU16 &item)   {   m_data >> item;   }
    virtual void persist(MxS16 &item)   {   m_data >> item;   }

    virtual void persist(MxU32 &item)   {   m_data >> item;   }
    virtual void persist(MxS32 &item)   {   m_data >> item;   }

    virtual void persist(MxU64 &item)   {   m_data >> item;   }
    virtual void persist(MxS64 &item)   {   m_data >> item;   }

#ifndef bool
    virtual void persist(bool &item)    {   m_data >> item;   }
#endif

    virtual void persist(float &item)   {   m_data >> item;   }
    virtual void persist(double &item)  {   m_data >> item;   }

    virtual void persist(std::string &item)  
    {   
        char * strItem;
        m_data >> strItem;   // *MUST* delete[] this
        item = strItem;
        delete[] strItem;
    }

    virtual bool Saving(void) const {return false;}
    virtual bool Restoring(void) const {return true;}
    virtual MxIDataStream& AsIStream(void) { return m_data; }
    virtual MxODataStream& AsOStream(void) 
    { 
        assert(false); 
        throw std::runtime_error("Misuse of persistIStream.");
        return dummyStream;
    }

private:    //lint !e1736
    // Scott Meyers Effective C++
    // Every class should have a default constructor.
    // The constructor is private to stop anyone using it, and not defined either.
    // Item 27:  Explicitly disallow use of implicitly generated member functions you don't want
    debugPersistIStream(); //lint !e1704 constructor having private access
    // Scott Meyers Effective C++
    // Item 11:  Declare a copy constructor and an assignment operator for classes with dynamically allocated memory
    debugPersistIStream(const debugPersistIStream& rhs); //lint !e1704 constructor having private access
    // Declare an assignment operator
    debugPersistIStream& operator=(const debugPersistIStream& rhs);
};

class debugPersistOStream: public persistOStream
{
protected:
    ofstream outfile;

public:
    debugPersistOStream(MxODataStream & data, string filename = "PersistOStream.txt"):persistOStream(data)    
    { 
        outfile.open(filename.c_str(), ofstream::out | ofstream::app); 
        outfile << "---< Start >---\n";
    }    

    ~debugPersistOStream() 
    { 
        outfile << "---< Stop >---\n";
        outfile.close(); 
    }

    virtual void comment(string text) { persist(text); }

    virtual bool persistVersion(MxU32 version)
    {
        outfile << "version " << version << "\n";
        return persistOStream::persistVersion(version);
    }
    virtual bool persistVersion(std::string version)
    {
        outfile << "version " << version << "\n";
        return persistOStream::persistVersion(version);
    }

    virtual void persist(MxU8 &item)    {   outfile << item << "\n"; persistOStream::persist(item); }
    virtual void persist(MxS8 &item)    {   outfile << item << "\n"; persistOStream::persist(item); }

    virtual void persist(MxU16 &item)   {   outfile << item << "\n"; persistOStream::persist(item); }
    virtual void persist(MxS16 &item)   {   outfile << item << "\n"; persistOStream::persist(item); }

    virtual void persist(MxU32 &item)   {   outfile << item << "\n"; persistOStream::persist(item); }
    virtual void persist(MxS32 &item)   {   outfile << item << "\n"; persistOStream::persist(item); }

    virtual void persist(MxU64 &item)   {   outfile << item << "\n"; persistOStream::persist(item); }
    virtual void persist(MxS64 &item)   {   outfile << item << "\n"; persistOStream::persist(item); }

#ifndef bool
    virtual void persist(bool &item)    {   outfile << item << "\n"; persistOStream::persist(item); }
#endif

    virtual void persist(float &item)   {   outfile << item << "\n"; persistOStream::persist(item); }
    virtual void persist(double &item)  {   outfile << item << "\n"; persistOStream::persist(item); }

    virtual void persist(std::string &item) { outfile << item << "\n"; persistOStream::persist(item); }

    virtual bool Saving(void) const {return persistOStream::Saving();}
    virtual bool Restoring(void) const {return persistOStream::Restoring();}
    virtual MxIDataStream& AsIStream(void) { return persistOStream::AsIStream(); }
    virtual MxODataStream& AsOStream(void) { return persistOStream::AsOStream(); }

private:    //lint !e1736
    // Scott Meyers Effective C++
    // Every class should have a default constructor.
    // The constructor is private to stop anyone using it, and not defined either.
    // Item 27:  Explicitly disallow use of implicitly generated member functions you don't want
    debugPersistOStream(); //lint !e1704 constructor having private access
    // Scott Meyers Effective C++
    // Item 11:  Declare a copy constructor and an assignment operator for classes with dynamically allocated memory
    debugPersistOStream(const debugPersistOStream& rhs); //lint !e1704 constructor having private access
    // Declare an assignment operator
    debugPersistOStream& operator=(const debugPersistOStream& rhs);
};

#endif
