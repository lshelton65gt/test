#ifndef _CarbonAhbSlaveT2S_h_
#define _CarbonAhbSlaveT2S_h_ 1

#include "maxsim.h"
#include "carbon_arm_adaptor.h"

class AHBT2S;

class CarbonAhbSlaveT2S
{
 public:
    // Constructor for used with writeDbg/readDbg
    CarbonAhbSlaveT2S(sc_mx_module* carbonComp,
                      const char *xtorName,
                      CarbonObjectID **carbonObj,
                      CarbonPortFactory *portFactory, 
                      CarbonDebugReadWriteFunction *readDebug,
                      CarbonDebugReadWriteFunction *writeDebug,
                      bool isAhbLite);

    // Constructor for used with debugAccess
    CarbonAhbSlaveT2S(sc_mx_module* carbonComp,
                      const char *xtorName,
                      CarbonObjectID **carbonObj,
                      CarbonPortFactory *portFactory, 
                      CarbonDebugAccessFunction *debugAccess,
                      bool isAhbLite);
    
    ~CarbonAhbSlaveT2S();
    sc_mx_signal_slave *carbonGetPort(const char *name);

    void communicate();
    void update();

    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);

    void setParameter(const string &name, const string &value);

    int getNumRegions();
    void getAddressRegions(uint64_t* start, uint64_t* size, string* name);
    void carbonSetPortWidth(const char* name, uint32_t bitWidth);

 private:
    AHBT2S *mArmAdaptor;
};

#endif // _CarbonAhbSlaveT2S_h_ 1
