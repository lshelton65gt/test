// note this file exists in both CVS(acton) and SVN(irvine), if you change one you should change the other
#include "ProtocolUtils.h"
#include "maxsimCompatibility.h"
#include "AHB_TLM.h"
#include "AXI_TLM.h"
#include "APB_Globals.h"
// #include "APB_Transaction.h"
#include "AHB_Transaction.h"
//#include "AXI_Transaction.h"

#ifndef AXI_PROTOCOL_ID
#define AXI_PROTOCOL_ID 0x0001A3E1 // A 3E 1
#endif

class ProtocolUtilsPrivate
{
public:
  std::map<uint32_t, std::string> _id2name;
};

ProtocolUtils::ProtocolUtils()
 : p(new ProtocolUtilsPrivate())
{
        // for known Protocol IDs
        p->_id2name[AHB_M_PROTOCOL_ID] = "AHBv2 Master";
        p->_id2name[AHB_S_PROTOCOL_ID] = "AHBv2 Slave";
        p->_id2name[AHB_LITE_M_PROTOCOL_ID] = "AHBv2-Lite Master";
        p->_id2name[AHB_LITE_S_PROTOCOL_ID] = "AHBv2-Lite Slave";
        p->_id2name[AXI2_PROTOCOL_ID] = "AXIv2";
        p->_id2name[1] = "MX";
        p->_id2name[APB_PROTOCOL_ID] = "APB";
        p->_id2name[2] = "Generic driveTransaction";
        p->_id2name[AHB_PROTOCOL_ID] = "AHBv1 (deprecated)";
        p->_id2name[AHBLITE_PROTOCOL_ID] = "AHBv1-Lite (deprecated)";
        p->_id2name[AXI_PROTOCOL_ID] = "AXIv1 (deprecated)";
}

string
ProtocolUtils::getProtocolName( const uint32_t protocolID )
{
        std::map<uint32_t, string>::iterator it;
        it = p->_id2name.find( protocolID );
        if( it == p->_id2name.end() )
                return "Unknown";
        return it->second;
}
