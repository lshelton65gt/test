// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#ifndef AHBM_T2S_H
#define AHBM_T2S_H

#if CARBON
#include "CarbonAdaptorBase.h"
#include "carbon_ahb.h"
#include "util/UtCLicense.h"
#endif

#include "maxsim.h"
#if MAXSIM_MAJOR_VERSION >= 7
#include "AHB_Transaction7.h"
#include "AHB_Transaction_Ext.h"
#else
#include "AHB_Transaction.h"
#endif

typedef enum
{
    HGRANT_SS = 0,
    HRESP_SS,
    HREADYOUT_SS,
    HRDATA_SS_31_0,
    HRDATA_SS_63_32,
    HRDATA_SS_95_64,
    HRDATA_SS_127_96,
    AHBM_T2S_SS_END
} ahbRtlSSenum;

typedef enum
{
    XTOR_TYPE_AHBM_T2S = 0,
    XTOR_TYPE_AHBLITEM_T2S
} ahbXtorTypeEnum;

#define HRDATA_SS HRDATA_SS_31_0

#define AHB_DEFAULT_BUSWIDTH 32

// supporting up to 128bit data bus
#define MAX_DATA_WIDTH_IN_WORDS 4

class AHBM_T2S_MxDI;

#if CARBON
class AHBM_T2S : public CarbonAdaptorBase
#else
class AHBM_T2S : public sc_mx_module
#endif
{
    friend class AHBM_T2S_RTL_SS;
    friend class AHBM_T2S_TS;
    friend class AHBM_T2S_MxDI;
    
public:
    AHBM_T2S_TS* ahb_TSlave;

    AHBM_T2S_RTL_SS* fromRTL[AHBM_T2S_SS_END];
    
    RTL_PORT *HBUSREQ_SMaster;    
    RTL_PORT *HADDR_SMaster;    
    RTL_PORT *HWDATA_SMaster;
    RTL_PORT *HWRITE_SMaster;
    RTL_PORT *HTRANS_SMaster;
    RTL_PORT *HBURST_SMaster;
    RTL_PORT *HSIZE_SMaster;
    RTL_PORT *HPROT_SMaster;
    RTL_PORT *HLOCK_SMaster;
    RTL_PORT *HBSTRB_SMaster; // ARM11 AMBA2 ext
    RTL_PORT *HUNALIGN_SMaster; // ARM11 AMBA2 ext
    RTL_PORT *HDOMAIN_SMaster; // ARM11 AMBA2 ext

    // constructor / destructor
#if CARBON
    AHBM_T2S(sc_mx_module* carbonComp, const char *xtorName, ahbXtorTypeEnum xtorType,
             CarbonObjectID **carbonObj, CarbonPortFactory *portFactory, const CarbonDebugAccessCallbacks& dbaCb);
#else
    AHBM_T2S(sc_mx_m_base* c, const string &s, int xtorType);
#endif
    virtual ~AHBM_T2S();    

    // overloaded methods for clocked components
    void communicate();
    void update();

    // overloaded sc_mx_module methods
#if (!CARBON)
    string getName();
#endif
    void setParameter(const string &name, const string &value);
#if (!CARBON)
    string getProperty( MxPropertyType property );
#endif
    void setPortWidth(const char* name, uint32_t bitWidth);
    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);

#if (!CARBON)
    MXDI* getMxDI();
#endif

#if CARBON
    CarbonDebugReadWriteFunction *mReadDebug;
    CarbonDebugReadWriteFunction *mWriteDebug;
#endif

private:
    MxU32   addressMask;        //!< Set in init() from p_Size

    bool  m_initComplete; 
    bool  m_enableDbgMsg;
    MxU32 m_ahbmPortID;
    AHBM_T2S_MxDI* m_mxdi;
    bool  m_bigEndian;
    bool  m_alignData;
    int   m_xtorType;
    bool  m_subtractBaseAddr;
    bool  m_subtractBaseAddrDbg;

    MxU32 m_dataWidthInWords;
    MxU32 m_addrWidthInWords;

    MxU64 m_Size;
    MxU64 m_Base;

    // Values for signals to RTL 
    MxU32 hbusreq;
    MxU64 haddr;
    MxU32 hwdata[ MAX_DATA_WIDTH_IN_WORDS ];
    MxU32 hwrite;
    MxU32 htrans;
    MxU32 hburst;
    MxU32 hsize;    
    MxU32 hprot;
    MxU32 hlock;
    MxU32 hstrb; // ARM11 AMBA2 ext
    MxU32 hunalign; // ARM11 AMBA2 ext
    MxU32 hdomain; // ARM11 AMBA2 ext
    
    bool m_busRequest;
    bool m_driveBusRequest;
    bool m_driveAddrPhase;
    
public:

    MxU32 latchedFromRTL[AHBM_T2S_SS_END];
    
    TAHBSignals signals;
    CarbonDebugAccessCallbacks mDbaCb;
};

#endif
