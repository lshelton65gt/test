
#include "CarbonAhbMasterT2S.h"

#define CARBON 1
#include "AHBM_T2S.h"
#include "AHBM_T2S_RTL_SS.h"

CarbonAhbMasterT2S::CarbonAhbMasterT2S(sc_mx_module* carbonComp,
                                       const char *xtorName,
                                       CarbonObjectID **carbonObj,
                                       CarbonPortFactory *portFactory,
                                       CarbonDebugReadWriteFunction* readDebug,
                                       CarbonDebugReadWriteFunction* writeDebug)
{
  CarbonDebugAccessCallbacks cb = {carbonComp, NULL, writeDebug, readDebug, NULL};
  mArmAdaptor = new AHBM_T2S(carbonComp, xtorName, XTOR_TYPE_AHBM_T2S, carbonObj, portFactory, cb);
}

CarbonAhbMasterT2S::CarbonAhbMasterT2S(sc_mx_module* carbonComp,
                                       const char *xtorName,
                                       CarbonObjectID **carbonObj,
                                       CarbonPortFactory *portFactory, 
                                       CarbonDebugAccessFunction *debugAccess)
{
  CarbonDebugAccessCallbacks cb = {carbonComp, debugAccess, NULL, NULL, NULL};
  mArmAdaptor = new AHBM_T2S(carbonComp, xtorName, XTOR_TYPE_AHBM_T2S, carbonObj, portFactory, cb);
}

sc_mx_signal_slave *CarbonAhbMasterT2S::carbonGetPort(const char *name)
{
  sc_mx_signal_slave *port;

  if (strcmp("hresp", name) == 0) {
    port = mArmAdaptor->fromRTL[HRESP_SS];

  } else if (strcmp("hreadyout", name) == 0) {
    port = mArmAdaptor->fromRTL[HREADYOUT_SS];

  } else if (strcmp("hrdata", name) == 0) {
    port = mArmAdaptor->fromRTL[HRDATA_SS];

  } else if (strcmp("hgrant", name) == 0) {
    port = mArmAdaptor->fromRTL[HGRANT_SS];

  } else {
    port = NULL;
    mArmAdaptor->message(MX_MSG_FATAL_ERROR, "Carbon AHB Master S2T has no port named \"%s\"", name);
  }
  return port;
}

CarbonAhbMasterT2S::~CarbonAhbMasterT2S()
{
  delete mArmAdaptor;
}

void CarbonAhbMasterT2S::communicate()
{
  mArmAdaptor->communicate();
}

void CarbonAhbMasterT2S::update()
{
  mArmAdaptor->update();
}

void CarbonAhbMasterT2S::init()
{
  mArmAdaptor->init();
}

void CarbonAhbMasterT2S::terminate()
{
  mArmAdaptor->terminate();
}

void CarbonAhbMasterT2S::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  mArmAdaptor->reset(level, filelist);
}

void CarbonAhbMasterT2S::setParameter(const string &name, const string &value)
{
  mArmAdaptor->setParameter(name, value);
}

//! Number of slave memory regions
int CarbonAhbMasterT2S::getNumRegions()
{
  return mArmAdaptor->getNumSlaveRegions();
}

//! Slave Address regions
void CarbonAhbMasterT2S::getAddressRegions(uint64_t* start, uint64_t* size, string* name)
{
  mArmAdaptor->getSlaveAddressRegions(start, size, name);
}

void CarbonAhbMasterT2S::carbonSetPortWidth(const char* name, uint32_t bitWidth)
{
  mArmAdaptor->setPortWidth(name, bitWidth);
}
