// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#include <stdio.h>

#include "AHBM_T2S.h"
#include "AHBM_T2S_RTL_SS.h"
#include "AHBM_T2S_TS.h"

#include "AHBM_T2S_MxDI.h"

#ifdef CARBON
#include <stdarg.h>
#define AHBM_T2S_RTL_SM(NAME) portFactory(mCarbonComponent, xtorName, NAME, mpCarbonObject)
#else
#define AHBM_T2S_RTL_SM(NAME) new sc_port<sc_mx_signal_if>(this, NAME)
#endif


#if CARBON
AHBM_T2S::AHBM_T2S(sc_mx_module* carbonComp, const char *xtorName, ahbXtorTypeEnum xtorType,
                   CarbonObjectID **carbonObj, CarbonPortFactory *portFactory, const CarbonDebugAccessCallbacks& dbaCb) :
  CarbonAdaptorBase(carbonComp, xtorName, carbonObj),
  m_dataWidthInWords(1),
  m_addrWidthInWords(1),
  mDbaCb(dbaCb)
#else
AHBM_T2S::AHBM_T2S(sc_mx_m_base* c, const string &s, int xtorType) : sc_mx_module(c, s)
#endif
{
    m_xtorType = xtorType;

    for ( int i = 0; i < AHBM_T2S_SS_END; i++ )
        fromRTL[i] = NULL;

    ahb_TSlave = new AHBM_T2S_TS( this );
    registerPort(ahb_TSlave, "ahb" );

    maxsim::MxSignalProperties *signalprop = new maxsim::MxSignalProperties;
    signalprop->isOptional = false;

    // signals from RTL
    if (m_xtorType == XTOR_TYPE_AHBM_T2S)
    {
        fromRTL[HGRANT_SS] = new AHBM_T2S_RTL_SS( this, HGRANT_SS );
        signalprop->bitwidth = 1;
        fromRTL[HGRANT_SS]->setProperties(signalprop);
        registerPort( fromRTL[HGRANT_SS], "hgrant" );
    }
    else
        fromRTL[HGRANT_SS] = NULL;

    fromRTL[HRESP_SS] = new AHBM_T2S_RTL_SS( this, HRESP_SS );
    signalprop->bitwidth = 3; // hresp[2] = EXRESP
    fromRTL[HRESP_SS]->setProperties(signalprop);
    registerPort( fromRTL[HRESP_SS], "hresp" );

    fromRTL[HREADYOUT_SS] = new AHBM_T2S_RTL_SS( this, HREADYOUT_SS );
    signalprop->bitwidth = 1;
    fromRTL[HREADYOUT_SS]->setProperties(signalprop);
    registerPort( fromRTL[HREADYOUT_SS], "hreadyout" );

    fromRTL[HRDATA_SS] = new AHBM_T2S_RTL_SS( this, HRDATA_SS );
    signalprop->bitwidth = 32;
    fromRTL[HRDATA_SS]->setProperties(signalprop);
    registerPort( fromRTL[HRDATA_SS], "hrdata" );
  
    // signals to RTL
    if (m_xtorType == XTOR_TYPE_AHBM_T2S)
    {
        HBUSREQ_SMaster = AHBM_T2S_RTL_SM( "HBUSREQ" );
        signalprop->bitwidth = 1;
        HBUSREQ_SMaster->setProperties(signalprop);
        registerPort( HBUSREQ_SMaster, "hbusreq" );
    }
    else
        HBUSREQ_SMaster = NULL;

    HADDR_SMaster = AHBM_T2S_RTL_SM( "HADDR" );
    signalprop->bitwidth = 32;
    HADDR_SMaster->setProperties(signalprop);
    registerPort( HADDR_SMaster, "haddr" );

    HWDATA_SMaster = AHBM_T2S_RTL_SM( "HWDATA" );
    signalprop->bitwidth = 32;
    HWDATA_SMaster->setProperties(signalprop);
    registerPort( HWDATA_SMaster, "hwdata" );

    HWRITE_SMaster = AHBM_T2S_RTL_SM( "HWRITE" );
    signalprop->bitwidth = 1;
    HWRITE_SMaster->setProperties(signalprop);
    registerPort( HWRITE_SMaster, "hwrite" );

    HTRANS_SMaster = AHBM_T2S_RTL_SM( "HTRANS" );
    signalprop->bitwidth = 2;
    HTRANS_SMaster->setProperties(signalprop);
    registerPort( HTRANS_SMaster, "htrans" );

    HBURST_SMaster = AHBM_T2S_RTL_SM( "HBURST" );
    signalprop->bitwidth = 3;
    HBURST_SMaster->setProperties(signalprop);
    registerPort( HBURST_SMaster, "hburst" );

    HSIZE_SMaster = AHBM_T2S_RTL_SM( "HSIZE" );
    signalprop->bitwidth = 3;
    HSIZE_SMaster->setProperties(signalprop);
    registerPort( HSIZE_SMaster, "hsize" );

    HPROT_SMaster = AHBM_T2S_RTL_SM( "HPROT" );
    signalprop->bitwidth = 6; // hprot[4] = ALLOC, hprot[5] = EXREQ
    HPROT_SMaster->setProperties(signalprop);
    registerPort( HPROT_SMaster, "hprot" );

    HLOCK_SMaster = AHBM_T2S_RTL_SM( "HLOCK" );
    signalprop->bitwidth = 1;
    HLOCK_SMaster->setProperties(signalprop);
    registerPort( HLOCK_SMaster, "hlock" );

    HBSTRB_SMaster = AHBM_T2S_RTL_SM( "HBSTRB" );
    signalprop->bitwidth = 16; // supports up to 128 bit data width
    HBSTRB_SMaster->setProperties(signalprop);
    registerPort( HBSTRB_SMaster, "hstrb" );

    HUNALIGN_SMaster = AHBM_T2S_RTL_SM( "HUNALIGN" );
    signalprop->bitwidth = 1;
    HUNALIGN_SMaster->setProperties(signalprop);
    registerPort( HUNALIGN_SMaster, "hunalign" );

    HDOMAIN_SMaster = AHBM_T2S_RTL_SM( "HDOMAIN" );
    signalprop->bitwidth = 1;
    HDOMAIN_SMaster->setProperties(signalprop);
    registerPort( HDOMAIN_SMaster, "hdomain" );
#if (!CARBON)
    SC_MX_CLOCKED();
    registerPort( dynamic_cast< sc_mx_clock_slave_p_base* >( this ), "clk-in");
#endif

    m_initComplete = false;
    m_mxdi = NULL;
    m_Base = 0;

#if (!CARBON)
    defineParameter( "Enable Debug Messages", "false", MX_PARAM_BOOL, true);
    defineParameter( "Base Address", "0x0", MX_PARAM_VALUE, false);
    defineParameter( "Size", "0x100000000", MX_PARAM_VALUE, false);
    defineParameter("Data Bus Width", "32", MX_PARAM_VALUE, false);
    defineParameter("ahbm port ID", "0", MX_PARAM_VALUE, false);
    defineParameter("Big Endian", "false", MX_PARAM_BOOL, false);
    defineParameter("Address Bus Width", "32", MX_PARAM_VALUE, false);
    defineParameter("Subtract Base Address", "true", MX_PARAM_BOOL, false);
#endif
}

AHBM_T2S::~AHBM_T2S()
{
    delete ahb_TSlave;
    for ( int i = 0; i < AHBM_T2S_SS_END; i++ )
    {
        if ( fromRTL[i] )
        {
            delete fromRTL[i];
            fromRTL[i] = NULL;
        }
    }
    if (HBUSREQ_SMaster)
        delete HBUSREQ_SMaster;
    delete HADDR_SMaster;
    delete HWDATA_SMaster;
    delete HWRITE_SMaster;
    delete HTRANS_SMaster;
    delete HBURST_SMaster;
    delete HSIZE_SMaster;
    delete HPROT_SMaster;
    delete HLOCK_SMaster;
    delete HBSTRB_SMaster;
    delete HUNALIGN_SMaster;
    delete HDOMAIN_SMaster;
}

void 
AHBM_T2S::communicate()
{
    signals.hready = latchedFromRTL[HREADYOUT_SS];  
}

void
AHBM_T2S::update()
{
    HLOCK_SMaster->driveSignal(hlock, NULL);

    if (HBUSREQ_SMaster && m_driveBusRequest)
    {
        if (m_busRequest)
        {
            HBUSREQ_SMaster->driveSignal(1, NULL);
        }
        else
        {
            HBUSREQ_SMaster->driveSignal(0, NULL);
        }
        
        m_driveBusRequest = false;
    }

    if (m_driveAddrPhase)
    {
        if (m_addrWidthInWords > 1)
#ifdef SUNOS
            HADDR_SMaster->driveSignal(((MxU32*)&haddr)[1], &((MxU32*)&haddr)[0]);
#else
            HADDR_SMaster->driveSignal(((MxU32*)&haddr)[0], &((MxU32*)&haddr)[1]);
#endif
        else
#ifdef SUNOS
            HADDR_SMaster->driveSignal(((MxU32*)&haddr)[1], NULL);
#else
            HADDR_SMaster->driveSignal(((MxU32*)&haddr)[0], NULL);
#endif
        HWRITE_SMaster->driveSignal(hwrite, NULL);
        HTRANS_SMaster->driveSignal(htrans, NULL);
        HBURST_SMaster->driveSignal(hburst, NULL);
        HSIZE_SMaster->driveSignal(hsize, NULL);
        HPROT_SMaster->driveSignal(hprot, NULL);
        HWDATA_SMaster->driveSignal(hwdata[0], &hwdata[1]);
        HBSTRB_SMaster->driveSignal(hstrb, NULL);
        HUNALIGN_SMaster->driveSignal(hunalign, NULL);
        HDOMAIN_SMaster->driveSignal(hdomain, NULL);

        m_driveAddrPhase = false;
    }
    else
    {
        HTRANS_SMaster->driveSignal(AHB_IDLE, NULL);

        // Ensure last data phase is driven        
        HWDATA_SMaster->driveSignal(hwdata[0], &hwdata[1]);
    }
   
    // update the signals structure 
    signals.hready_z1 = signals.hready;
    signals.hgrant_z1 = signals.hgrant;
}

void AHBM_T2S::setPortWidth(const char* name, uint32_t bitWidth)
{
  if (strcmp(name, "hwdata") == 0 || strcmp(name, "hrdata") == 0 || (strcmp(name, "haddr") == 0)) {

    // Calculate word count based on the next power of 2 above the given bit width.
    int wordCount = nextPo2(bitWidth)>>5;

    if (strcmp(name, "haddr") == 0)
      m_addrWidthInWords = wordCount & 0x3; // Limit address to 64 bit max
    else
      m_dataWidthInWords = wordCount;
  }
}

void 
AHBM_T2S::init()
{
    if ( m_dataWidthInWords != 1 )
    {
        MxSignalProperties prop;
        
        ahb_TSlave->setDataBusWidth(m_dataWidthInWords);

#if (!CARBON)        
        prop = *(HWDATA_SMaster->getProperties());
        prop.bitwidth = m_dataWidthInWords * 32;
        HWDATA_SMaster->setProperties(&prop);
#endif

        prop = *(fromRTL[HRDATA_SS]->getProperties());
        prop.bitwidth = m_dataWidthInWords * 32;
        fromRTL[HRDATA_SS]->setProperties(&prop);
    }

    if ( m_addrWidthInWords != 1 )
    {
        MxSignalProperties prop;

        ahb_TSlave->setAddrBusWidth(m_addrWidthInWords);

        prop = *(HADDR_SMaster->getProperties());
        prop.bitwidth = m_addrWidthInWords * 32;
        HADDR_SMaster->setProperties(&prop);
    }

#if (!CARBON)
    m_mxdi = new AHBM_T2S_MxDI(this);
    
    if (m_mxdi == NULL)
    {
        message(MX_MSG_FATAL_ERROR, "%s::init(): Failed to allocate memory for MxDI interface. Aborting Simulation!", getInstanceID().c_str());
    }

    // Call the base class after this has been initialized.
    sc_mx_module::init();
#endif
    m_initComplete = true;
}

void
AHBM_T2S::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  (void)level; (void)filelist;

    memset(latchedFromRTL, 0, sizeof(MxU32) * AHBM_T2S_SS_END);
    memset( hwdata, 0, sizeof(MxU32) * MAX_DATA_WIDTH_IN_WORDS );
    haddr = 0;

    if (HBUSREQ_SMaster)
        HBUSREQ_SMaster->driveSignal(0, NULL);
    if (m_addrWidthInWords > 1)
        HADDR_SMaster->driveSignal(0, (MxU32*)&haddr);
    else
        HADDR_SMaster->driveSignal(0, NULL);
    HWDATA_SMaster->driveSignal(hwdata[0], &hwdata[1]);
    HWRITE_SMaster->driveSignal(AHB_READ, NULL);
    HTRANS_SMaster->driveSignal(AHB_IDLE, NULL);
    HBURST_SMaster->driveSignal(AHB_SINGLE, NULL);
    HSIZE_SMaster->driveSignal(0, NULL);
    HPROT_SMaster->driveSignal(0, NULL);
    HLOCK_SMaster->driveSignal(0, NULL);
    HBSTRB_SMaster->driveSignal(0, NULL);
    HUNALIGN_SMaster->driveSignal(0, NULL);
    HDOMAIN_SMaster->driveSignal(0, NULL);

    m_busRequest      = false;
    m_driveAddrPhase  = false;
    m_driveBusRequest = false;
    
    hlock = 0;

#if (!CARBON)
    // Call the base class after this has been reset.
    sc_mx_module::reset(level, filelist);
#endif

    
    signals.hready = 1;     // HREADY signal
    signals.hready_z1 = 1;      // HREADY signal
    if (m_xtorType == XTOR_TYPE_AHBM_T2S)
        signals.hgrant = (MxU32)-1;
    else
        signals.hgrant = m_ahbmPortID;
    signals.hgrant_z1 = signals.hgrant;
}



void 
AHBM_T2S::terminate()
{
#if (!CARBON)
  // Call the base class first.
  sc_mx_module::terminate();
#endif

  if (m_mxdi != NULL)
  {
    // Release the MXDI Interface
    delete m_mxdi;
    m_mxdi = NULL;
  }
}


void
AHBM_T2S::setParameter( const string &name, const string &value )
{
    MxConvertErrorCodes status = MxConvert_SUCCESS;

    if (name == "Enable Debug Messages")
    {
        status = MxConvertStringToValue(value, &m_enableDbgMsg);
    }
    else if ( name == "ahbm port ID" )
    {
        if (m_initComplete == false)
        {
            status = MxConvertStringToValue( value, &m_ahbmPortID );
        }
        else
        {
            message(MX_MSG_WARNING, "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.", 
                    getInstanceID().c_str(), name.c_str() );            
        }
    }
    else if (name == "Base Address")
    {
        if (m_initComplete == false)
        {
            status = MxConvertStringToValue(value, &m_Base);
        }
        else
        {
            message(MX_MSG_WARNING, "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.", 
                    getInstanceID().c_str(), name.c_str() );            
        }
    }
    else if (name == "Size")
    {
        if (m_initComplete == false)
        {
            status = MxConvertStringToValue(value, &m_Size);
        }
        else
        {
            message(MX_MSG_WARNING, "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.", 
                    getInstanceID().c_str(), name.c_str() );            
        }
    }
    else if (name == "Data Bus Width")
    {
        if (m_initComplete == false)
        {
            MxU32 tmp = 0;
            status = MxConvertStringToValue(value, &tmp);
            
            if (status == MxConvert_SUCCESS)
            {
                switch ( tmp )
                {
                case 32:  m_dataWidthInWords = 1; break;
                case 64:  m_dataWidthInWords = 2; break;
                case 128: m_dataWidthInWords = 4; break;
                default:
                    if (tmp < 32)
                    {
                        message( MX_MSG_WARNING, "%s::setParameter(): Rounding value %s for parameter: %s to 32.", getInstanceID().c_str(), value.c_str(), name.c_str() );
                        m_dataWidthInWords = 1;
                    }
                    else if (tmp < 64)
                    {
                        message( MX_MSG_WARNING, "%s::setParameter(): Rounding value %s for parameter: %s to 64.", getInstanceID().c_str(), value.c_str(), name.c_str() );
                        m_dataWidthInWords = 2;
                    }
                    else
                    {
                        message( MX_MSG_WARNING, "%s::setParameter(): Rounding value %s for parameter: %s to 128.", getInstanceID().c_str(), value.c_str(), name.c_str() );
                        m_dataWidthInWords = 4;
                    }
                    break;
                }
            }
        }
        else
        {
            message(MX_MSG_WARNING, 
                    "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.", 
                    getInstanceID().c_str(), name.c_str() );            
        }
    }
    else if (name == "Address Bus Width")
    {
        if (m_initComplete == false)
        {
            MxU32 tmp = 0;
            status = MxConvertStringToValue(value, &tmp);
            
            if (status == MxConvert_SUCCESS)
            {
                if (tmp > 64)
                {
                    message( MX_MSG_WARNING, "%s::setParameter(): Cannot set parameter <%s> higher than 64. Using 64 instead.", getInstanceID().c_str(), name.c_str() );
                    m_addrWidthInWords = 2;
                }
                else
                    m_addrWidthInWords = 1;
            }
        }
        else
        {
            message(MX_MSG_WARNING, 
                    "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.", 
                    getInstanceID().c_str(), name.c_str() );            
        }
    }
    else if (name == "Big Endian")
    {
        if (m_initComplete == false)
        {
            status = MxConvertStringToValue(value, &m_bigEndian);
        }
        else
        {
            message(MX_MSG_WARNING, "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.", 
                    getInstanceID().c_str(), name.c_str() );            
        }
    }
    else if (name == "Subtract Base Address")
    {
        if (m_initComplete == false)
        {
            status = MxConvertStringToValue(value, &m_subtractBaseAddr);
        }
        else
        {
            message(MX_MSG_WARNING, "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.", 
                    getInstanceID().c_str(), name.c_str() );            
        }
    }
    else if (name == "Subtract Base Address Dbg")
    {
        if (m_initComplete == false)
        {
            status = MxConvertStringToValue(value, &m_subtractBaseAddrDbg);
        }
        else
        {
            message(MX_MSG_WARNING, "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.", 
                    getInstanceID().c_str(), name.c_str() );            
        }
    }
    else if (name == "Align Data")
    {
        status = MxConvertStringToValue(value, &m_alignData);
    }


    if ( status == MxConvert_SUCCESS )
    {
#if (!CARBON)
        sc_mx_module::setParameter(name, value);
#endif
    }
    else
    {
        message( MX_MSG_WARNING, "%s::setParameter(): Illegal value <%s> "
                 "passed for parameter <%s>. Assignment ignored.", getInstanceID().c_str(), value.c_str(), name.c_str() );
    }
}

#if (!CARBON)
string
AHBM_T2S::getProperty( MxPropertyType property )
{
    string description; 
    switch ( property ) 
    {    
    case MX_PROP_IP_PROVIDER:
        return "ARM";
    case MX_PROP_LOADFILE_EXTENSION:
        return "";
    case MX_PROP_COMPONENT_TYPE:
        return MxComponentTypes[MX_TYPE_TRANSACTOR];
    case MX_PROP_COMPONENT_VERSION:
        return "2.1.003";
    case MX_PROP_MSG_PREPEND_NAME:
        return "yes"; 
    case MX_PROP_DESCRIPTION:
        switch (m_xtorType)
        {
        case XTOR_TYPE_AHBM_T2S:
            description = "AHB MaxSim Transaction to HDL Master Signals Converter";
            break;
        case XTOR_TYPE_AHBLITEM_T2S:
            description = "AHB-Lite MaxSim Transaction to HDL Master Signals Converter";
            break;
        }
        return description + " Compiled on " + __DATE__ + ", " + __TIME__; 
    case MX_PROP_MXDI_SUPPORT:
        return "yes";
    case MX_PROP_SAVE_RESTORE:
        return "no";
    default:
        return "";
    }
    return "";
}

MXDI*
AHBM_T2S::getMxDI()
{
  return m_mxdi;
}

string
AHBM_T2S::getName(void)
{
    switch (m_xtorType)
    {
    case XTOR_TYPE_AHBM_T2S:
        return "AHBM_T2S";
    case XTOR_TYPE_AHBLITEM_T2S:
        return "AHBLiteM_T2S";
    }
    return "";
}

/************************
 * AHBM_T2S Factory class
 ***********************/

class AHBM_T2SFactory : public MxFactory
{
public:
    AHBM_T2SFactory() : MxFactory ( "AHBM_T2S" ) {}
    sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
    { 
        return new AHBM_T2S(c, id, XTOR_TYPE_AHBM_T2S); 
    }
};

/************************
 * AHBLiteM_T2S Factory class
 ***********************/

class AHBLiteM_T2SFactory : public MxFactory
{
public:
    AHBLiteM_T2SFactory() : MxFactory ( "AHBLiteM_T2S" ) {}
    sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
    { 
        return new AHBM_T2S(c, id, XTOR_TYPE_AHBLITEM_T2S); 
    }
};

/**
 *  Entry point into the memory components (from MaxSim)
 */
extern "C" void 
MxInit(void)
{
    new AHBM_T2SFactory();
    new AHBLiteM_T2SFactory();
}
#endif
