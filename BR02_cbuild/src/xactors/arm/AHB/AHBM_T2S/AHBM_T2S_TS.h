// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#ifndef AHBM_T2S_TS_H_
#define AHBM_T2S_TS_H_

#if CARBON
#include "carbon_arm_adaptor.h"
#endif

#include "maxsim.h"
#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
#define MAX_REGIONS 0x1
#endif

class AHBM_T2S;

class AHBM_T2S_TS: public sc_mx_transaction_slave
{
    AHBM_T2S *owner;


public:
    AHBM_T2S_TS( AHBM_T2S *_owner );
    virtual ~AHBM_T2S_TS() {}

public:
    /* Synchronous access functions */
    virtual MxStatus read(MxU64 addr, MxU32* value, MxU32* ctrl);
    virtual MxStatus write(MxU64 addr, MxU32* value, MxU32* ctrl);
    virtual MxStatus readDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
    virtual MxStatus writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
    
    /* Asynchronous access functions */
    virtual MxStatus readReq(MxU64 addr, MxU32* value, MxU32* ctrl,
                             MxTransactionCallbackIF* callback);
    virtual MxStatus writeReq(MxU64 addr, MxU32* value, MxU32* ctrl,
                              MxTransactionCallbackIF* callback);

    /* Memory map functions */
    virtual int getNumRegions();
    virtual void getAddressRegions(MxU64* start, MxU64* size, string* name);
#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
    virtual void setAddressRegions(MxU64* start, MxU64* size, string* name);
    virtual MxMemoryMapConstraints* getMappingConstraints();
#endif
    
    bool firstError;

    void setDataBusWidth( MxU32 dataWidthInWords ) {
        prop.dataBitwidth = dataWidthInWords * 32;
        setProperties( &prop );
    }
    
    void setAddrBusWidth( MxU32 addrWidthInWords ) {
        prop.addressBitwidth = addrWidthInWords * 32;
        setProperties( &prop );
    }
    
	MxGrant requestAccess(MxU64);
	MxGrant checkForGrant(MxU64);

private:
	MxTransactionProperties prop;
#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
    // initialize Slave Constraints
    void initMemoryMapConstraints();
    MxMemoryMapConstraints puMemoryMapConstraints;
    string regionName[MAX_REGIONS];
#endif
};

#endif

