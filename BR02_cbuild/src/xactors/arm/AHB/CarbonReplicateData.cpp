//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "xactors/arm/include/carbon_arm_adaptor.h"
#include "AHB_Transaction.h"
#include "CarbonReplicateData.h"

//
// If the data operation is smaller than the data bus, grab
// the LSB  byte, half-word, or word out of value and replicate it
// back into the other portions of the value.
//
// Fix for 6067.
// Parameters:
//    ctrl - transaction control field
//    dataWidthInWords 
//    value - array holding original and transformed data
//
void CarbonReplicateData( MxU32* ctrl, MxU32 dataWidthInWords, MxU32* value )
{
  MxU32 tmpData, wordData;

  // replicate the data across all byte lanes only if size < data bus width
  if( (MxU32) ( 1 << AHB_ACC_DECODE_HSIZE(ctrl[AHB_IDX_ACC]) ) >= 
              (dataWidthInWords * 4) )
    return;
    
  switch( ctrl[AHB_IDX_TYPE] ) {
    case AHB_TYPE_BYTE:
      tmpData = value[0] & 0xFF;
                           
      // replicate across whole of data bus
      memset(value, tmpData, sizeof(MxU32) * dataWidthInWords);
      break;

    case AHB_TYPE_HWORD:
      tmpData = value[0] & 0xFFFF;

      // replicate across word
      wordData = tmpData | (tmpData << 16); 
                				           
      // replicate across whole of data bus
      for (MxU32 i=0; i < dataWidthInWords; i++) 
        value[i] = wordData;	
      break;

    case AHB_TYPE_WORD:
      // determine which word to replicate
      wordData = value[0];
                           
      // replicate across whole of data bus
      for (MxU32 i=1; i < dataWidthInWords; i++)
         value[i] = wordData;	
      break;

    case AHB_TYPE_DWORD:
        value[2] = value[0];	
        value[3] = value[1];	
      break;
                
    default:
      // FIXME: add error msg
      break;
  }
}
