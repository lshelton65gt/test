// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2006 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#ifndef _AHB_acc__h
#define _AHB_acc__h


#define acc_Nrw     0x10        /* not read/write */
#define acc_seq     0x20        /* sequential */
#define acc_Nmreq   0x40        /* not memory request */
#define acc_Nopc   0x100        /* not opcode fetch */
#define acc_rlw    0x400        /* read-lock-write */
#define acc_kill   0x800        /* [I/D]KIll signal for ARM9EJ-S */
#define acc_burst0  0x1000      /* AMBA BURST[0:1] pins set on ARM 9xxT */
#define acc_burst1  0x2000
#define acc_burst2  0x4000      /* AMBA AHB HBURST[2]; HBURST[0:1] uses acc_burst{0,1} */
#ifndef NOJAVA
#define acc_burst3  0x8000      /* ARM9EJ-S uses BURST[0:3] for burst lengthin LDM/STM. */
#define ACC_BURST_POS   12      /* Bit position of burst access type */
#endif
#define acc_dmore   0x10000      /* DMORE pin on the ARM9TDMI */
#define acc_Nareq   0x20000      /* No AMBA request - ASB and AHB processors */
#define acc_Ntrans   0x200
#define acc_nTRANS(w)   ((w) & acc_Ntrans)

/* AMBA ASB Burst encodings */
#define acc_BURST_TYPE(w)   (w & (acc_burst0 | acc_burst1))
#define acc_burst_none      0
#define acc_burst_4word     acc_burst0
#define acc_burst_8word     acc_burst1
#define acc_burst_reserved  (acc_burst0 | acc_burst1)
#define acc_burst_tlb_walk  (acc_burst0 | acc_burst1)
#define acc_burst_buffered  (acc_burst0 | acc_burst1)

/* AMBA AHB Burst encodings */
#define acc_AHB_BURST_TYPE(w)   (w & (acc_burst0 | acc_burst1 | acc_burst2))
#define acc_ahb_burst_single    0
#define acc_ahb_burst_incr      acc_burst0
#define acc_ahb_burst_wrap4     acc_burst1
#define acc_ahb_burst_incr4     (acc_burst0 | acc_burst1)
#define acc_ahb_burst_wrap8     acc_burst2
#define acc_ahb_burst_incr8     (acc_burst2 | acc_burst0)
#define acc_ahb_burst_wrap16    (acc_burst2 | acc_burst1)
#define acc_ahb_burst_incr16    (acc_burst2 | acc_burst1 | acc_burst0)

/* AMBA AHB HPROT[3:2] are used to encode cacheable and bufferable accesses */

#define acc_cacheable   0x40000
#define acc_bufferable  0x80000

#ifndef FAST
#define acc_physical   0x100000
#define acc_spec       0x200000 /* speculative instruction fetch (ARM810) */
#define acc_new        0x400000 /* a new request (StrongARM) */
#endif

#define acc_CACHEABLE(w)    ((w) & acc_cacheable)
#define acc_BUFFERABLE(w)   ((w) & acc_bufferable)

#define acc_BURST0(w)       ((w) & acc_burst0)
#define acc_BURST1(w)       ((w) & acc_burst1)
#define acc_BURST2(w)       ((w) & acc_burst2)
#define acc_BURST3(w)       ((w) & acc_burst3)

#define acc_DMORE(w)   ((w) & acc_dmore)

#define acc_NAREQ(w)   ((w) & acc_Nareq)
#define acc_AREQ(w)    (!acc_NAREQ(w))

#define acc_WRITE(w)   ((w) & acc_Nrw)
#define acc_SEQ(w)     ((w) & acc_seq)
#define acc_nMREQ(w)   ((w) & acc_Nmreq)
#define acc_nOPC(w)    ((w) & acc_Nopc)
#define acc_LOCK(w)    ((w) & acc_rlw)
/* #define acc_nTRANS(w)  ((w) & acc_Ntrans) */
#define acc_SPEC(w)    ((w) & acc_spec)
#define acc_NEW(w)     ((w) & acc_new)
#define acc_KILL(w)    ((w) & acc_kill)

#define acc_READ(w)    (!acc_WRITE(w))
#define acc_nSEQ(w)    (!acc_SEQ(w))
#define acc_MREQ(w)    (!acc_nMREQ(w))
#define acc_OPC(w)     (!acc_nOPC(w))
#define acc_nLOCK(w)   (!acc_LOCK(w))
/* #define acc_TRANS(w)   (!acc_nTRANS(w)) */
#define acc_nSPEC(w)   (!acc_SPEC(w))
#define acc_nNEW(w)    (!acc_NEW(w))

#define acc_typeN  0
#define acc_typeS  acc_seq
#define acc_typeI  acc_Nmreq
#define acc_typeC  (acc_Nmreq | acc_seq)

#define acc_CYCLE(w)  (w & (acc_Nmreq | acc_seq))

/*
 * The bottom four bits of the access word define the width of the access.
 * The number used is LOG_2(number-of-bits)
 */

#define BITS_8  3
#define BITS_16 4
#define BITS_32 5
#define BITS_64 6
#define WIDTH_MASK 0x0f

#define acc_WIDTH(w) ((w) & WIDTH_MASK)

/*
 * Or it's byte lanes - 1 bit per byte
 * NB ADS1.1 doesn't use ByteLanes at all.
 *   StrongMMU can be configured to emit them, but this is not supported.
 */

#define acc_byte_0 0x1
#define acc_byte_1 0x2
#define acc_byte_2 0x4
#define acc_byte_3 0x8
#define BYTELANE_MASK 0xf

#define acc_BYTELANE(w) ((w) & BYTELANE_MASK)


#define acc_BYTE0(acc) ((acc) & acc_byte_0)
#define acc_BYTE1(acc) ((acc) & acc_byte_1)
#define acc_BYTE2(acc) ((acc) & acc_byte_2)
#define acc_BYTE3(acc) ((acc) & acc_byte_3)

#define acc_LoadInstrS    (BITS_32 | acc_typeS)
#define acc_LoadInstrS2   (BITS_64 | acc_typeS)
#define acc_LoadInstrS2Spec   (BITS_64 | acc_typeS | acc_spec)
#define acc_LoadInstrN    (BITS_32 | acc_typeN)
#define acc_LoadInstrN2   (BITS_64 | acc_typeN)
#define acc_LoadInstrN2Spec   (BITS_64 | acc_typeN | acc_spec)
#define acc_LoadInstr16S  (BITS_16 | acc_typeS)
#define acc_LoadInstr16N  (BITS_16 | acc_typeN)
#define acc_LoadWordS     (BITS_32 | acc_typeS | acc_Nopc)
#define acc_LoadWordS2    (BITS_64 | acc_typeS | acc_Nopc)
#define acc_LoadWordN     (BITS_32 | acc_typeN | acc_Nopc)
#define acc_LoadWordN2    (BITS_64 | acc_typeN | acc_Nopc)
#define acc_LoadByte      (BITS_8  | acc_typeN | acc_Nopc)
#define acc_LoadHalfWord  (BITS_16 | acc_typeN | acc_Nopc)

#define acc_StoreWordS    (BITS_32 | acc_typeS | acc_Nrw | acc_Nopc)
#define acc_StoreWordN    (BITS_32 | acc_typeN | acc_Nrw | acc_Nopc)
#define acc_StoreByte     (BITS_8  | acc_typeN | acc_Nrw | acc_Nopc)
#define acc_StoreHalfWord (BITS_16 | acc_typeN | acc_Nrw | acc_Nopc)

#define acc_NoFetch (acc_typeI)

#define acc_Icycle (acc_typeI | acc_Nopc)
#define acc_Ccycle (acc_typeC | acc_Nopc)

/* DRS 2000-08-22 Align with ARMISS */
#ifdef OldCode
# define acc_NotAccount    (1u<<31)
#else
# define acc_NotAccount    0x0080
#endif

#define acc_nACCOUNT(x)   ((x) & acc_NotAccount)
#define acc_ACCOUNT(x)    (!acc_nACCOUNT(x))

#define acc_DontAccount(X)  ((X) | acc_NotAccount)

#define acc_ReadWord      acc_DontAccount(acc_LoadWordN)
#define acc_ReadHalfWord  acc_DontAccount(acc_LoadHalfWord)
#define acc_ReadByte      acc_DontAccount(acc_LoadByte)

#define acc_WriteWord     acc_DontAccount(acc_StoreWordN)
#define acc_WriteHalfWord acc_DontAccount(acc_StoreHalfWord)
#define acc_WriteByte     acc_DontAccount(acc_StoreByte)

#define acc_Mode(acc)     ((acc & 0x80000000) ? 1 : 0)

#ifndef UNUSEDARG
#if defined(__STDC__) || defined(__cplusplus)
#define UNUSEDARG(X) ((void)(X)) /* Silence compiler warnings for unused arguments */
#else
#define UNUSEDARG(X) ((X) = (X))
#endif
#endif


//***********************************************************
// Define MxAHB constants
//***********************************************************

// Array offsets for readReq/writeReq ctrlPtr parameters

#define	AHB_MODE 0
#define	AHB_HDATA 2
#define	AHB_HWRITE 3
#define	AHB_HBURST 4
#define	AHB_HTRANS 5
#define	AHB_HSIZE 6
#define	AHB_HPROT 7
#define	AHB_NUM_SIGNALS 8
#define	AHB_NUM_MASTERS 16
#define	AHB_NUM_SLAVES 16
#define AHB_DEFAULT_MASTER 4


//*****************************************************
// AHB Signal allowable values
//*****************************************************

enum hbusreqValues
{
	AHB_NOREQ = 0,
	AHB_REQ = 1
};

enum hgrantValues
{
	AHB_NOGRANT = 0,
	AHB_GRANT = 1
};

enum hreadyValues
{
	AHB_WAIT = 0,
	AHB_READY = 1
};

enum hwriteValues
{
	AHB_READ = 0,
	AHB_WRITE = 1
};

enum hlockValues
{
	AHB_NOLOCK = 0,
	AHB_LOCK = 1
};

enum htransValues
{
	AHB_IDLE = 0,
	AHB_BUSY = 1,
	AHB_NONSEQ = 2,
	AHB_SEQ = 3
};

enum hsizeValues
{
	AHB_DATA8    = 0,
	AHB_DATA16   = 1,
	AHB_DATA32   = 2,
	AHB_DATA64   = 3,
	AHB_DATA128  = 4,
	AHB_DATA256  = 5,
	AHB_DATA512  = 6,
	AHB_DATA1024 = 7
};

enum hburstValues
{
	AHB_SINGLE = 0,
	AHB_INCR = 1,
	AHB_WRAP4 = 2,
	AHB_INCR4 = 3,
	AHB_WRAP8 = 4,
	AHB_INCR8 = 5,
	AHB_WRAP16 = 6,
	AHB_INCR16 = 7,
	AHB_BURST_LAST
};

enum hprotValues
{
	AHB_OPC_FETCH = 0,
	AHB_DATA_ACCESS = 1,
	AHB_USER_ACCESS = 2,
	AHB_PRIV_ACCESS = 3,
	AHB_NOT_BUFFERABLE = 4,
	AHB_BUFFERABLE = 5,
	AHB_NOT_CACHEABLE = 6,
	AHB_CACHEABLE = 7
};

enum hrespValues
{
	AHB_OK = 0,
	AHB_ERROR = 1,
	AHB_RETRY = 2,
	AHB_SPLIT = 3
};

enum hsplitValues
{
	AHB_SPLIT_NOTREADY = 0,
	AHB_SPLIT_READY = 1
};

//*****************************************************
// FSM states
//*****************************************************

enum  FSM_STATES
{
	AHB_FSM_ARBITRATE = 0,
	AHB_FSM_WAIT_FOR_HANDOVER = 1,
	AHB_FSM_WAIT_FOR_BURST = 2,
	AHB_FSM_CONTINUE_BURST = 3,
	AHB_FSM_ACCESS_DATA = 4,
	AHB_FSM_WAIT_FOR_DATA = 5
};

// Coding scheme for ctrl[MX_IDX_ACC]
//
// 3          2         1         0
// 10987654321098765432109876543210
// AuuuuuuuuuuuuuuuuNASSSPPPPTTBBBL
// Sn               CSI  R   R U  O
// Bu               MTZ  O   A R  C
//  s               ABE  T   N S  K
//  e               H        S T
//  d               B
//



#define AHB_ACC_DECODE_HLOCK(acc)	(((acc) >>  0) & 0x1)
#define AHB_ACC_DECODE_HBURST(acc)	(((acc) >>  1) & 0x7)
#define AHB_ACC_DECODE_HTRANS(acc)	(((acc) >>  4) & 0x3)
#define AHB_ACC_DECODE_HPROT(acc)	(((acc) >>  6) & 0xf)
#define AHB_ACC_DECODE_HSIZE(acc)	(((acc) >> 10) & 0x7)
#define AHB_ACC_DECODE_ASTB(acc)	(((acc) >> 13) & 0x1)
#define AHB_ACC_DECODE_NCMAHB(acc)	(((acc) >> 14) & 0x1)
#define AHB_ACC_DECODE_HMASTER(acc)	(((acc) >> 15) & 0xf)
#define AHB_ACC_DECODE_ASB(acc) 	(((acc) >> 31) & 0x1)


#define AHB_ACC_ENCODE_HLOCK(tmp)	(((tmp) & 0x1) <<  0)
#define AHB_ACC_ENCODE_HBURST(tmp)	(((tmp) & 0x7) <<  1)
#define AHB_ACC_ENCODE_HTRANS(tmp)	(((tmp) & 0x3) <<  4)
#define AHB_ACC_ENCODE_HPROT(tmp)	(((tmp) & 0xf) <<  6)
#define AHB_ACC_ENCODE_HSIZE(tmp)	(((tmp) & 0x7) << 10)
#define AHB_ACC_ENCODE_ASTB(tmp)	(((tmp) & 0x1) << 13)
#define AHB_ACC_ENCODE_NCMAHB(tmp) 	(((tmp) & 0x1) << 14)
#define AHB_ACC_ENCODE_ASB(tmp) 	(((tmp) & 0x1) << 31)
#define AHB_ACC_ENCODE_HMASTER(tmp)	(((tmp) & 0xf) << 15)

#define AHB_ACC_SET_HLOCK(dst, tmp)	 (dst) &= ~AHB_ACC_ENCODE_HLOCK  (0x1); (dst) |=  AHB_ACC_ENCODE_HLOCK  (((tmp) & 0x1));
#define AHB_ACC_SET_HBURST(dst, tmp) (dst) &= ~AHB_ACC_ENCODE_HBURST (0x7); (dst) |=  AHB_ACC_ENCODE_HBURST (((tmp) & 0x7));
#define AHB_ACC_SET_HTRANS(dst, tmp) (dst) &= ~AHB_ACC_ENCODE_HTRANS (0x3); (dst) |=  AHB_ACC_ENCODE_HTRANS (((tmp) & 0x3));
#define AHB_ACC_SET_HPROT(dst, tmp)	 (dst) &= ~AHB_ACC_ENCODE_HPROT  (0xF); (dst) |=  AHB_ACC_ENCODE_HPROT  (((tmp) & 0xF));
#define AHB_ACC_SET_HSIZE(dst, tmp)	 (dst) &= ~AHB_ACC_ENCODE_HSIZE  (0x7); (dst) |=  AHB_ACC_ENCODE_HSIZE  (((tmp) & 0x7));
#define AHB_ACC_SET_ASTB(dst, tmp)	 (dst) &= ~AHB_ACC_ENCODE_ASTB   (0x1); (dst) |=  AHB_ACC_ENCODE_ASTB   (((tmp) & 0x1));
#define AHB_ACC_SET_NCMAHB(dst, tmp) (dst) &= ~AHB_ACC_ENCODE_NCMAHB (0x1); (dst) |=  AHB_ACC_ENCODE_NCMAHB (((tmp) & 0x1));
#define AHB_ACC_SET_HMASTER(dst, tmp)(dst) &= ~AHB_ACC_ENCODE_HMASTER(0x1); (dst) |=  AHB_ACC_ENCODE_HMASTER(((tmp) & 0x1));
#define AHB_ACC_SET_ASB(dst, tmp)	 (dst) &= ~AHB_ACC_ENCODE_ASB    (0x1); (dst) |=  AHB_ACC_ENCODE_ASB    (((tmp) & 0x1));

#define AHB_ACC_DECODE_RESERVED_MASK 	\
( AHB_ACC_ENCODE_HLOCK(0x1)				\
|  AHB_ACC_ENCODE_HBURST(0x7)			\
|  AHB_ACC_ENCODE_HTRANS(0x3)			\
|  AHB_ACC_ENCODE_HPROT(0xf)			\
|  AHB_ACC_ENCODE_HSIZE(0x7)			\
|  AHB_ACC_ENCODE_HMASTER(0xf)			\
|  AHB_ACC_ENCODE_ASTB(0x1)				\
|  AHB_ACC_ENCODE_NCMAHB(0x1)			\
|  AHB_ACC_ENCODE_ASB(0x1)				\
	)

#define AHB_ACC_DECODE_RESERVED(acc)  ((acc) & ~AHB_ACC_DECODE_RESERVED_MASK)

#endif /* ifndef _acc__h */
