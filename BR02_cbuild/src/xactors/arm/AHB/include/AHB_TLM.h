#ifndef _AHB2_TLM_H_
#define _AHB2_TLM_H_

#include "maxsimCompatibility.h"

#ifdef WIN32
#ifdef MAKE_PORT_DLL
#define WEXP_PORT __declspec(dllexport)
#define TEMPLATE_PORT_EXP
#else
#define WEXP_PORT __declspec(dllimport)
#define TEMPLATE_PORT_EXP extern
#endif
#else
#define WEXP_PORT
#endif

#define AHB_PROBE_PROTOCOL_ID  0x22A8BC1
#define AHB_M_PROTOCOL_ID      0x22A8BC1
#define AHB_S_PROTOCOL_ID      0x22A8B5
#define AHB_LITE_M_PROTOCOL_ID 0x22A8B1C1
#define AHB_LITE_S_PROTOCOL_ID 0x22A8B15

#define AHB2_DATA_WIDTH 32

#define AHB2_INIT_TRANSACTION_PROPERTIES(props, PROT_ID)  \
    (props).casiVersion = eslapi::CASI_VERSION_1_1;    /* this protocol uses standard read/write */ \
    (props).useMultiCycleInterface = true;    /* synchronous access functions     */ \
    (props).addressBitwidth = 32;            /* address bitwidth used for addressing of resources  */ \
    (props).mauBitwidth = 8;                /* minimal addressable unit  */ \
    (props).dataBitwidth = AHB2_DATA_WIDTH;    /* maximum data bitwidth transferred in a single cycle */ \
    (props).isLittleEndian = true;            /* alignment of MAUs */ \
    (props).isOptional = false;                /* if true this port can be disabled */ \
    (props).supportsAddressRegions = true;    /* M/S can not negotiate address mapping */ \
    (props).numCtrlFields = 4;                /* # of ctrl elements used */ \
    (props).dataBeats = 16;                    /* # of data beats */ \
    (props).protocolID = PROT_ID;            /* magic number of the protocol (AHB Protocol!) */ \
    (props).numSlaveFlags = 0;                /* not used but initialized */ \
    (props).numMasterFlags = 0;                /* not used but initialized */ \
    (props).numTransactionSteps = 0;        /* not used but initialized */ \
    (props).supportsBurst = false;          /* not used but initialized */ \
    (props).supportsSplit = false;            /* not used but initialized */ \
    (props).supportsNotify = false;         /* not used but initialized */ \
    (props).validTimingTable = NULL;        /* not used but initialized */ \
    (props).details = NULL;                 /* not used but initialized */ \
    (props).forwardAddrRegionToMasterPort = NULL;   /* master port of the  same component to which this slave port's addr region is forwarded */ \
    (props).isAddrRegionForwarded = false;          /* MxSI 6.0: slave port address regions forwarding */

/*!
 * This enum is used to specify if a port is implementing the AHB or AHB-lite protocol.
 */
enum AHBBusType 
{
    AHBTypeFull, /*!< AHB */
    AHBTypeLite  /*!< AHB-lite */
};

/*!
 * This enum is used to specify if a port is implementing the master side of the bus
 * or the slave side. 
 */
enum AHBBusSide
{
    AHBSideMaster, /*!< Master side of the bus*/
    AHBSideSlave   /*!< Slave side of the bus*/
};

/*! 
 * This enum provides the possible HTRANS (2 bits) values for specifying the transfer type.
 */
enum AHB2_TRANS
{
    AHB2_TRANS_IDLE   = 0, /*!< Idle transfer */
    AHB2_TRANS_BUSY   = 1, /*!< Busy transfer */
    AHB2_TRANS_NONSEQ = 2, /*!< NonSeq transfer */
    AHB2_TRANS_SEQ    = 3  /*!< Seg transfer */
};

/*! 
 * This enum provides the possible HWRITE (1 bit) values for specifying read/write access. 
 */
enum AHB2_ACCESS
{
    AHB2_ACCESS_READ  = 0, /*!< Read access */
    AHB2_ACCESS_WRITE = 1  /*!< Write access */
};

/*! 
 * This enum provides the possible HSIZE (3 bits) values for specifying data size. 
 *
 * \remarks
 * data size of transferred items (3 bits)
 * NOTE that the encoding is different from AHB2_ACCESS_TYPE
 * the information is redundant
 */
enum AHB2_SIZE
{
    AHB2_SIZE_DATA8    = 0,
    AHB2_SIZE_DATA16   = 1,
    AHB2_SIZE_DATA32   = 2,
    AHB2_SIZE_DATA64   = 3,
    AHB2_SIZE_DATA128  = 4,
    AHB2_SIZE_DATA256  = 5,
    AHB2_SIZE_DATA512  = 6,
    AHB2_SIZE_DATA1024 = 7
};


/*! 
 * This enum provides the possible HBURST (3 bits) values for specifying burst type. 
 */
enum AHB2_BURST
{
    AHB2_BURST_SINGLE = 0,
    AHB2_BURST_INCR   = 1,
    AHB2_BURST_WRAP4  = 2,
    AHB2_BURST_INCR4  = 3,
    AHB2_BURST_WRAP8  = 4,
    AHB2_BURST_INCR8  = 5,
    AHB2_BURST_WRAP16 = 6,
    AHB2_BURST_INCR16 = 7
};

/*! 
 * This enum provides the possible HPROT (4 bits) flags for specifying protection flags. 
 *
 * hprot can be assembled from these constants/flags
 * for decoding the constants need to be used as masks
 * e.g. 
 * if (hprot & AHB2_PROT_IS_DATA_ACCESS)
 */
enum AHB2_PROT
{
    AHB2_PROT_IS_DATA_ACCESS = 1,    // versus is instruction fetch if this bit position is 0 (hprot[0])
    AHB2_PROT_IS_PRIVILEGED  = 2,    // versus is user mode if this bit position is 0 (hprot[1])
    AHB2_PROT_IS_BUFFERABLE  = 4,    // versus is non-bufferable if this bit position is 0 (hprot[2])
    AHB2_PROT_IS_CACHEABLE   = 8        // versus is non-cacheable if this bit position is 0 (hprot[3])
};

/*! 
 * This enum provides the possible HREADY & HREADYOUT (1 bit) values. 
 */
enum AHB2_hreadyValues
{
    AHB2_READY_WAIT = 0,
    AHB2_READY = 1
};

/*! 
 * This enum provides the possible HBUSREQ (1 bit) values. 
 *
 * \remarks
 * hbusreq is an array of 1 bit request signals that keeps the request status per master id (0 - 31)
 */
enum AHB2_hbusreqValues
{
    AHB2_NOREQ = 0,
    AHB2_REQ = 1
};

/*! 
 * This enum provides the possible HLOCK & HMASTLOCK (1 bit) values. 
 *
 * \remarks
 * hlock is an array of 1 bit lock signals that keeps the lock status per master id (0 - 31)
 */
enum AHB2_hlockValues
{
    AHB2_NOLOCK = 0,
    AHB2_LOCK = 1
};

/*! 
 * This enum provides the possible HGRANT (1 bit) values. 
 *
 * \remarks
 * hgrant provides the grant status for the master with the ID selected by hmaster
 */
enum AHB2_hgrantValues
{
    AHB2_NOGRANT = 0,
    AHB2_GRANT = 1
};

enum AHB2_RESP
{
    AHB2_RESP_OKAY = 0,
    AHB2_RESP_ERROR = 1,
    AHB2_RESP_RETRY = 2,
    AHB2_RESP_SPLIT = 3
};

/*! 
 * This enum is used as a global index for referencing an AHB signal. 
 *
 * \remarks
 * The possible enum values is a superset of all AHB protocols and
 * may not necessarily exist for some AHB protocols, e.g. HMASTLOCK only
 * exists on AHBTypeFull, AHBSideSlave 
 */
enum AHB2_SIGNAL_IDX
{
    // Common AHB2_Master_PortBase Out Signals
    HADDR = 0,
    HTRANS,
    HWRITE,
    HSIZE,
    HBURST,
    HPROT,
    HWDATA,
    HWDATA1,
    HWDATA2,
    HWDATA3,

    // Common Out Signals to AHB2_Master_Port, AHBLite_Master_Port, AHBLite_Master_BusPort
    HLOCK,               // Aliased to HMASTLOCK
    HMASTLOCK,

    // Common Out Signals to AHB2_Master_BusPort and AHBLite_Master_BusPort
    HSEL,

    // AHB2_Master_BusPort Out Signals
    HMASTER,

    // AHB2_Master_Port Out Signal
    HGRANT,
    HBUSREQ,

    // Common AHB2_Slave_PortBase Out Signals
    HRESP,
    HRDATA,
    HRDATA1,
    HRDATA2,
    HRDATA3,

    // Common Out Signals to AHB2_Slave_BusPort and AHBLite_Slave_BusPort 
    HREADY,

    // Common Out Signals to AHB2_Slave_Port and AHBLite_Slave_Port 
    HREADYOUT,

    // ARM11 extensions
    HBSTRB,
    HUNALIGN,
    HDOMAIN,

    SIDEBAND0, 
    SIDEBAND1, 
    SIDEBAND2, 
    SIDEBAND3,
    SIDEBAND4,
    SIDEBAND5,
    SIDEBAND6,
    SIDEBAND7,
    SIDEBAND8,
    SIDEBAND9,

    NUM_AHB2_SIGNALS
};
inline void operator++(AHB2_SIGNAL_IDX& eVal)
{
    eVal = AHB2_SIGNAL_IDX(eVal+1);
}
inline void operator--(AHB2_SIGNAL_IDX& eVal)
{
    eVal = AHB2_SIGNAL_IDX(eVal-1);
}

struct WEXP_PORT AHB2Signal
{
    string name;
    string shortname;
};

extern const AHB2Signal WEXP_PORT ahbSignals[];

#endif // _AHB2_TLM_H_
