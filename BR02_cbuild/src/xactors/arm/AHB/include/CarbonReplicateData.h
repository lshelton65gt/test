//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef CARBONREPLICATEDATA__H
#define CARBONREPLICATEDATA__H

extern void CarbonReplicateData( MxU32* ctrl, MxU32 dataWidthInWords, MxU32* value );

#endif

