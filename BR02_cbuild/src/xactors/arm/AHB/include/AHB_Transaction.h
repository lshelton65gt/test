// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2006 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.


#ifndef _AHB_TRANSACTION__H_
#define _AHB_TRANSACTION__H_

#define AHB_PROTOCOL_ID 0x10A8B
#define AHBLITE_PROTOCOL_ID 0x10A8B1

#include "maxsim.h"

//***********************************************************
// Define MxAHB constants
//***********************************************************

// Array offsets for readReq/writeReq ctrlPtr parameters

#define	AHB_MODE 0
#define	AHB_HDATA 2
#define	AHB_HWRITE 3
#define	AHB_HBURST 4
#define	AHB_HTRANS 5
#define	AHB_HSIZE 6
#define	AHB_HPROT 7
#define	AHB_NUM_SIGNALS 8
#define	AHB_NUM_MASTERS 16
#define	AHB_NUM_SLAVES 16
#define AHB_DEFAULT_MASTER 4

#define AHB_DATA_WIDTH 32

#define AHB_INIT_TRANSACTION_PROPERTIES(props)  \
	(props).mxsiVersion = MXSI_VERSION_6;	/* this protocol uses standard read/write */ \
    (props).useMultiCycleInterface = false;	/* synchronous access functions	 */ \
	(props).addressBitwidth = 32;			/* address bitwidth used for addressing of resources  */ \
    (props).mauBitwidth = 8;				/* minimal addressable unit  */ \
    (props).dataBitwidth = AHB_DATA_WIDTH;	/* maximum data bitwidth transferred in a single cycle */ \
    (props).isLittleEndian = true;			/* alignment of MAUs */ \
    (props).isOptional = false;				/* if true this port can be disabled */ \
    (props).supportsAddressRegions = true;	/* M/S can not negotiate address mapping */ \
    (props).numCtrlFields = 4;		        /* # of ctrl elements used */ \
    (props).dataBeats = 16;		        	/* # of data beats */ \
    (props).protocolID = AHB_PROTOCOL_ID;	/* magic number of the protocol (AHB Protocol!) */ \
    (props).numSlaveFlags = 0;				/* not used but initialized */ \
    (props).numMasterFlags = 0;				/* not used but initialized */ \
    (props).numTransactionSteps = 0;		/* not used but initialized */ \
    (props).supportsBurst = false;          /* not used but initialized */ \
    (props).supportsSplit = false;			/* not used but initialized */ \
    (props).supportsNotify = false;         /* not used but initialized */ \
    (props).validTimingTable = NULL;	    /* not used but initialized */ \
    (props).details = NULL;                 /* not used but initialized */ \
    (props).forwardAddrRegionToMasterPort = NULL;   /* master port of the  same component to which this slave port's addr region is forwarded */ \
    (props).isAddrRegionForwarded = false;          /* MxSI 6.0: slave port address regions forwarding */


//
// Enumerated indices for ctrl[] array (*ctrl) of MxU32 quantities,
// passed as third argument of MaxSim's read(),write() methods
//
typedef enum 
{
    AHB_IDX_TYPE = 0,   // Access type/size
    AHB_IDX_CYCLE = 1,  // Cycle type address/data
    AHB_IDX_ACC = 2,    // Access information
    AHB_IDX_ACK = 3,    // The response/acknowledgment     
    AHB_IDX_END = 4     // Add your new indices before this (always represent the last index)
} AHB_CTRL_IDX;

//
// Access type:
// ctrl[AHB_IDX_TYPE]
// to be set by requester (master)
//
typedef enum 
{
    AHB_TYPE_NONE    = 0,    // -
    AHB_TYPE_BYTE    = 1,    // 8bit
    AHB_TYPE_HWORD   = 2,    // 16bit
    AHB_TYPE_WORD    = 3,    // 32bit
    AHB_TYPE_DWORD   = 4,    // 64bit
	AHB_TYPE_128BIT  = 5,    // 128bit
	AHB_TYPE_256BIT  = 6,    // 256bit
	AHB_TYPE_512BIT  = 7,    // 512bit
	AHB_TYPE_1024BIT = 8     // 1024bit
} AHB_ACCESS_TYPE;

//
// ahb bus protocol cycle information
//
typedef enum 
{
	AHB_CYCLE_NONE = 0,
	AHB_CYCLE_ADDR = 1,
	AHB_CYCLE_DATA = 3,
	AHB_CYCLE_GRNT = 4
} AHB_CYCLE_TYPE;

//
// Response field:
// ctrl[AHB_IDX_ACK]
// to be set by slave (memory or peripheral)
//
typedef enum 
{
    AHB_ACK_DONE  = 0,  // An Okay response (used in conjunction with hready)
    AHB_ACK_WAIT  = 1,  // A wait response 
	AHB_ACK_ABORT = 3,  // An abort response	
    AHB_ACK_RETRY = 4,  // A retry response (must be a two-cycle response)
    AHB_ACK_SPLIT = 5   // A split response (must be a two-cycle response)
} AHB_ACK_TYPE;

// Coding scheme for ctrl[AHB_IDX_ACC]
//
// 3          2         1         0
// 10987654321098765432109876543210
// AuuuuuuuuuuuuuuuuNASSSPPPPTTBBBL
// Sn               CSI  R   R U  O
// Bu               MTZ  O   A R  C
//  s               ABE  T   N S  K
//  e               H        S T
//  d               B
//

#define AHB_ACC_DECODE_HLOCK(acc)	(((acc) >>  0) & 0x1)
#define AHB_ACC_DECODE_HBURST(acc)	(((acc) >>  1) & 0x7)
#define AHB_ACC_DECODE_HTRANS(acc)	(((acc) >>  4) & 0x3)
#define AHB_ACC_DECODE_HPROT(acc)	(((acc) >>  6) & 0xf)
#define AHB_ACC_DECODE_HSIZE(acc)	(((acc) >> 10) & 0x7)
#define AHB_ACC_DECODE_ASTB(acc)	(((acc) >> 13) & 0x1)
#define AHB_ACC_DECODE_NCMAHB(acc)	(((acc) >> 14) & 0x1)
#define AHB_ACC_DECODE_HMASTER(acc)	(((acc) >> 15) & 0xf)
#define AHB_ACC_DECODE_ASB(acc) 	(((acc) >> 31) & 0x1)


#define AHB_ACC_ENCODE_HLOCK(tmp)	(((tmp) & 0x1) <<  0)
#define AHB_ACC_ENCODE_HBURST(tmp)	(((tmp) & 0x7) <<  1)
#define AHB_ACC_ENCODE_HTRANS(tmp)	(((tmp) & 0x3) <<  4)
#define AHB_ACC_ENCODE_HPROT(tmp)	(((tmp) & 0xf) <<  6)
#define AHB_ACC_ENCODE_HSIZE(tmp)	(((tmp) & 0x7) << 10)
#define AHB_ACC_ENCODE_ASTB(tmp)	(((tmp) & 0x1) << 13)
#define AHB_ACC_ENCODE_NCMAHB(tmp) 	(((tmp) & 0x1) << 14)
#define AHB_ACC_ENCODE_ASB(tmp) 	(((tmp) & 0x1) << 31)
#define AHB_ACC_ENCODE_HMASTER(tmp)	(((tmp) & 0xf) << 15)

#define AHB_ACC_SET_HLOCK(dst, tmp)	 (dst) &= ~AHB_ACC_ENCODE_HLOCK  (0x1); (dst) |=  AHB_ACC_ENCODE_HLOCK  (((tmp) & 0x1));
#define AHB_ACC_SET_HBURST(dst, tmp) (dst) &= ~AHB_ACC_ENCODE_HBURST (0x7); (dst) |=  AHB_ACC_ENCODE_HBURST (((tmp) & 0x7));
#define AHB_ACC_SET_HTRANS(dst, tmp) (dst) &= ~AHB_ACC_ENCODE_HTRANS (0x3); (dst) |=  AHB_ACC_ENCODE_HTRANS (((tmp) & 0x3));
#define AHB_ACC_SET_HPROT(dst, tmp)	 (dst) &= ~AHB_ACC_ENCODE_HPROT  (0xF); (dst) |=  AHB_ACC_ENCODE_HPROT  (((tmp) & 0xF));
#define AHB_ACC_SET_HSIZE(dst, tmp)	 (dst) &= ~AHB_ACC_ENCODE_HSIZE  (0x7); (dst) |=  AHB_ACC_ENCODE_HSIZE  (((tmp) & 0x7));
#define AHB_ACC_SET_ASTB(dst, tmp)	 (dst) &= ~AHB_ACC_ENCODE_ASTB   (0x1); (dst) |=  AHB_ACC_ENCODE_ASTB   (((tmp) & 0x1));
#define AHB_ACC_SET_NCMAHB(dst, tmp) (dst) &= ~AHB_ACC_ENCODE_NCMAHB (0x1); (dst) |=  AHB_ACC_ENCODE_NCMAHB (((tmp) & 0x1));
#define AHB_ACC_SET_HMASTER(dst, tmp)(dst) &= ~AHB_ACC_ENCODE_HMASTER(0x1); (dst) |=  AHB_ACC_ENCODE_HMASTER(((tmp) & 0x1));
#define AHB_ACC_SET_ASB(dst, tmp)	 (dst) &= ~AHB_ACC_ENCODE_ASB    (0x1); (dst) |=  AHB_ACC_ENCODE_ASB    (((tmp) & 0x1));

#define AHB_ACC_DECODE_RESERVED_MASK 	\
( AHB_ACC_ENCODE_HLOCK(0x1)				\
|  AHB_ACC_ENCODE_HBURST(0x7)			\
|  AHB_ACC_ENCODE_HTRANS(0x3)			\
|  AHB_ACC_ENCODE_HPROT(0xf)			\
|  AHB_ACC_ENCODE_HSIZE(0x7)			\
|  AHB_ACC_ENCODE_HMASTER(0xf)			\
|  AHB_ACC_ENCODE_ASTB(0x1)				\
|  AHB_ACC_ENCODE_NCMAHB(0x1)			\
|  AHB_ACC_ENCODE_ASB(0x1)				\
	)

#define AHB_ACC_DECODE_RESERVED(acc)  ((acc) & ~AHB_ACC_DECODE_RESERVED_MASK)

#define AHB_MAX_MASTERS 32

// complete set of AHB signals
// currently not all are used
// assuming maximum of 128bit data bus bitwidth
// assuming maximum of 32 connected master ports
typedef struct TAHBSignals
{
	MxU32 hclk;									 // not modeled, always 1
	MxU32 hreset;								 // not modeled, always 1
	MxU32 haddr;								 // value of address bus         (can safely be read in update())
	MxU32 htrans;								 // transfer type                (can safely be read in update())
	MxU32 hwrite;								 // write strobe                 (can safely be read in update())
	MxU32 hsize;								 // data size info               (can safely be read in update())
	MxU32 hburst;								 // burst info                   (can safely be read in update())
	MxU32 hprot;								 // protection info              (can safely be read in update())
	MxU32 hwdata[4];							 // write data bus               (can safely be read in update())
	MxU32 hrdata[4];							 // read data bus                (can safely be read in update())
	MxU32 hsel;					 				 // not modeled, always 0
	MxU32 hready;								 // handshake signal             (can safely be read in update())
	MxU32 hresp;								 // not modeled, always 0        (AHB_OK -> AHB_ACK_xxx)
	MxU32 hbusreq[AHB_MAX_MASTERS];				 // request vector               (can safely be read in update())
	MxU32 hlock[AHB_MAX_MASTERS];				 // lock vector                  (can safely be read in update())
	MxU32 hgrant;								 // bus grant                    (can safely be read in communicate(), use checkForGrant())
	MxU32 hmaster;								 // selected master id           (can safely be read in communicate())
	MxU32 hmastlock;							 // lock of the granted master   (can safely be read in update())
	MxU16 hsplit[AHB_MAX_MASTERS];				 // not modeled, always 0
	MxU32 hready_z1;							 // hready delayed by one cycle, (can safely be read in communicate(), maintained by AHB bus)
	MxU32 hgrant_z1;							 // hgrant delayed by one cycle, (can safely be read in communicate(), maintained by AHB bus)
} TAHBSignals;

// *****************************************************
// AHB Signal allowable values
// *****************************************************

// transfer type values (2 bits)
enum htransValues
{
	AHB_TRANS_IDLE   = 0,
	AHB_TRANS_BUSY   = 1,
	AHB_TRANS_NONSEQ = 2,
	AHB_TRANS_SEQ    = 3
};

// read/write access select called hwrite (1 bit)
enum hwriteValues
{
	AHB_ACCESS_READ  = 0,
	AHB_ACCESS_WRITE = 1
};

// data size of transferred items (3 bits)
// NOTE that the encoding is different from AHB_ACCESS_TYPE
// the information is redundant
enum hsizeValues
{
	AHB_SIZE_DATA8    = 0,
	AHB_SIZE_DATA16   = 1,
	AHB_SIZE_DATA32   = 2,
	AHB_SIZE_DATA64   = 3,
	AHB_SIZE_DATA128  = 4,
	AHB_SIZE_DATA256  = 5,
	AHB_SIZE_DATA512  = 6,
	AHB_SIZE_DATA1024 = 7
};

// burst type information (3 bits)
enum hburstValues
{
	AHB_BURST_SINGLE = 0,
	AHB_BURST_INCR   = 1,
	AHB_BURST_WRAP4  = 2,
	AHB_BURST_INCR4  = 3,
	AHB_BURST_WRAP8  = 4,
	AHB_BURST_INCR8  = 5,
	AHB_BURST_WRAP16 = 6,
	AHB_BURST_INCR16 = 7
};

// protection flags for the transaction (4 bits)
// hprot can be assembled from these constants/flags
// for decoding the constants need to be used as masks
// e.g. 
// if (hprot & AHB_PROT_IS_DATA_ACCESS)
enum hprotValues
{
	AHB_PROT_IS_DATA_ACCESS = 1,	// versus is instruction fetch if this bit position is 0 (hprot[0])
	AHB_PROT_IS_PRIVILEGED  = 2,	// versus is user mode if this bit position is 0 (hprot[1])
	AHB_PROT_IS_BUFFERABLE  = 4,	// versus is non-bufferable if this bit position is 0 (hprot[2])
	AHB_PROT_IS_CACHEABLE   = 8		// versus is non-cacheable if this bit position is 0 (hprot[3])
};

#define AHB_PROT_IS_INSN_FETCH     0
#define AHB_PROT_IS_USER           0
#define AHB_PROT_IS_NON_BUFFERABLE 0
#define AHB_PROT_IS_NON_CACHEABLE  0

// single bit handshake signal indicates whether the slave completes the data cycle
enum hreadyValues
{
	AHB_READY_WAIT = 0,
	AHB_READY = 1
};

// hbusreq is an array of 1 bit request signals that keeps the request status per master id (0 - 31)
enum hbusreqValues
{
	AHB_NOREQ = 0,
	AHB_REQ = 1
};

// hlock is an array of 1 bit lock signals that keeps the lock status per master id (0 - 31)
enum hlockValues
{
	AHB_NOLOCK = 0,
	AHB_LOCK = 1
};

// hgrant provides the grant status for the master with the ID selected by hmaster
enum hgrantValues
{
	AHB_NOGRANT = 0,
	AHB_GRANT = 1
};


#if 1 //CARBON
/*
  These things are from AHB_Globals.h and/or AHB_acc.h.

  They are added here so we can replace #include of those files sith this one.

  This is so that the generated component can #include both the T2S and S2T
  headers without the rest of the stuff being defined twice.
*/
#define AHB_SINGLE AHB_BURST_SINGLE
#define AHB_IDLE   AHB_TRANS_IDLE
#define AHB_WAIT   AHB_READY_WAIT

#define MX_IDX_TYPE AHB_IDX_TYPE
#define MX_IDX_CYCLE AHB_IDX_CYCLE
#define MX_IDX_ACC AHB_IDX_ACC
#define MX_IDX_ACK AHB_IDX_ACK
#define MX_IDX_END AHB_IDX_END

#define MX_ACK_DONE AHB_ACK_DONE
#define MX_ACK_WAIT AHB_ACK_WAIT
#define MX_ACK_ABORT AHB_ACK_ABORT
#define MX_ACK_RETRY AHB_ACK_RETRY
#define MX_ACK_SPLIT AHB_ACK_SPLIT

#define MX_CYCLE_NONE AHB_CYCLE_NONE
#define MX_CYCLE_ADDR AHB_CYCLE_ADDR
#define MX_CYCLE_DATA AHB_CYCLE_DATA
#define MX_CYCLE_GRNT AHB_CYCLE_GRNT

enum hrespValues
{
	AHB_OK = 0,
	AHB_ERROR = 1,
	AHB_RETRY = 2,
	AHB_SPLIT = 3
};


#define AHB_READ  AHB_ACCESS_READ
#define AHB_WRITE AHB_ACCESS_WRITE

#define MX_TYPE_NONE AHB_TYPE_NONE
#define MX_TYPE_BYTE AHB_TYPE_BYTE
#define MX_TYPE_HWORD AHB_TYPE_HWORD
#define MX_TYPE_WORD AHB_TYPE_WORD
#define MX_TYPE_DWORD AHB_TYPE_DWORD
#define MX_TYPE_128BIT AHB_TYPE_128BIT
#define MX_TYPE_256BIT AHB_TYPE_256BIT
#define MX_TYPE_512BIT AHB_TYPE_512BIT
#define MX_TYPE_1024BIT AHB_TYPE_1024BIT

#endif

#endif // #ifndef _AHB_TRANSACTION__H_
