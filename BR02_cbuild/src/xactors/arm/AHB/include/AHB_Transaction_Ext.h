#ifndef _AHB_TRANSACTION_EXT__H_
#define _AHB_TRANSACTION_EXT__H_

// Coding scheme for ctrl[AHB_IDX_SIDEBAND]
// Includes changes to accomodate ARM11 AMBA2 AHB extensions
//
// 3          2         1         0
// 10987654321098765432109876543210
// uuuuuuuSSSSSSSSSSSSSSSSDDDDUEESA
//       n               T   ONXXHL
//       u               R   MARRAL
//       s               B   ALEERO
//       e                   IIQSEC
//       d                   NG PA
//                            N  B
//                               L
//                               E

#define AHB_SIDEBAND_DECODE_HUNALIGN(sideband) (((sideband) >> 4) & 0x1)
#define AHB_SIDEBAND_DECODE_HDOMAIN(sideband)  (((sideband) >> 5) & 0xf)
#define AHB_SIDEBAND_DECODE_HBSTRB(sideband)    (((sideband) >> 9) & 0xffff)
#define AHB_HPROT_DECODE_ALLOC(hprot)          (((hprot) >> 4) & 0x1)
#define AHB_HPROT_DECODE_EXREQ(hprot)          (((hprot) >> 5) & 0x1)
#define AHB_HRESP_DECODE_EXRESP(hresp)         (((hresp) >> 2) & 0x1)

#define AHB_SIDEBAND_ENCODE_HUNALIGN(tmp) (((tmp) & 0x1) << 4)
#define AHB_SIDEBAND_ENCODE_HDOMAIN(tmp)  (((tmp) & 0x1) << 5)
#define AHB_SIDEBAND_ENCODE_HBSTRB(tmp)    (((tmp) & 0xffff) << 9)
#define AHB_HPROT_ENCODE_ALLOC(tmp)       (((tmp) & 0x1) << 4)
#define AHB_HPROT_ENCODE_EXREQ(tmp)       (((tmp) & 0x1) << 5)
#define AHB_HRESP_ENCODE_EXRESP(tmp)      (((tmp) & 0x1) << 2)

#define AHB_SIDEBAND_SET_HUNALIGN(dst, tmp) (dst) &= ~AHB_SIDEBAND_ENCODE_HUNALIGN(0x1); (dst) |= AHB_SIDEBAND_ENCODE_HUNALIGN((tmp) & 0x1);
#define AHB_SIDEBAND_SET_HDOMAIN(dst, tmp)  (dst) &= ~AHB_SIDEBAND_ENCODE_HDOMAIN(0xf); (dst) |= AHB_SIDEBAND_ENCODE_HDOMAIN((tmp) & 0xf);
#define AHB_SIDEBAND_SET_HBSTRB(dst, tmp)    (dst) &= ~AHB_SIDEBAND_ENCODE_HBSTRB(0xffff); (dst) |= AHB_SIDEBAND_ENCODE_HBSTRB((tmp) & 0xffff);
#define AHB_HPROT_SET_ALLOC(dst, tmp)       (dst) &= ~AHB_HPROT_ENCODE_ALLOC(0x1); (dst) |= AHB_HPROT_ENCODE_ALLOC((tmp) & 0x1);
#define AHB_HPROT_SET_EXREQ(dst, tmp)       (dst) &= ~AHB_HPROT_ENCODE_EXREQ(0x1); (dst) |= AHB_HPROT_ENCODE_EXREQ((tmp) & 0x1);
#define AHB_HRESP_SET_EXRESP(dst, tmp)      (dst) &= ~AHB_HRESP_ENCODE_EXRESP(0x1); (dst) |= AHB_HRESP_ENCODE_EXREQ((tmp) & 0x1);

#endif // ifndef _AHB_TRANSACTION_EXT__H_

// end of file AHB_Transaction_Ext.h
