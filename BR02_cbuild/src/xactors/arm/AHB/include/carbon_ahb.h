#ifndef _carbon_ahb_h_
#define _carbon_ahb_h_ 1

#include "carbon_arm_adaptor.h"

#ifdef CARBON
#define MASTER_PORT CarbonXtorAdaptorToVhmPort
#define RTL_PORT CarbonXtorAdaptorToVhmPort
#else
#define MASTER_PORT sc_port<sc_mx_signal_if>
#define RTL_PORT sc_port<sc_mx_signal_if>
#endif

#endif
