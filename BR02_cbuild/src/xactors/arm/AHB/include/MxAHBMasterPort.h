// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2005 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
//


/*! \file MxAHBMasterPort.h
 *
 *  \brief MxAHBMasterPort header file (interface specification)
 */

#ifndef _MxAHBMasterPort_h_
#define _MxAHBMasterPort_h_

#include  <queue>

#include "MxPortCheck.h"

#define AHB_ADDR_INVALID 0xBAD0BAD0
#define AHB_DATA_INVALID 0xDEADBEEF

#if MAXSIM_MAJOR_VERSION >= 7
#include "AHB_Transaction7.h"
#else
#include "AHB_Transaction.h"
#endif

// ahb master version
#define MX_AHB_MASTER_VERSION_MAJOR 2
#define MX_AHB_MASTER_VERSION_MINOR 1
#define MX_AHB_MASTER_VERSION_STRING "2.1"

#define MAX_AHB_TRANSACTION_DATA     32

/*! \struct _TPipeStage
 *
 *  \brief An AHB bus stage.
 */
 struct TPipeCtrl
{
	MxU64 id;
	MxU64 addr;
	MxU32 ctrl[AHB_IDX_END];

	MxU32 hburst;
	MxU32 htrans;
	MxU32 hprot;
	MxU32 hsize;

	bool bValid;
	bool bWrite;
	
	bool bAddressDriven;
	bool bDataValidThisCycle;
	bool bEBT;

	MxU32 totalBeats;
	MxU32 beat;
	MxU32 wrapSize;
    
	MxU64 wrapBoundary;
    
	MxU32 sizeBeatInBytes;

    bool bInitialized;
};

struct  TPipeStage
{
    MxU32 data[MAX_AHB_TRANSACTION_DATA];            

    TPipeCtrl c;
};

typedef TPipeStage TAHBTransaction;

#define NUM_AHB_CYCLES 4

typedef enum
{
	AHB_STAGE_ARB = 0,
	AHB_STAGE_GRNT,
	AHB_STAGE_ADDR,
	AHB_STAGE_DATA
} AHB_STAGES_ENUM;

#define MESSAGE maxsim::MxOutput::getListener()->message

class PrepAddrDataPhaseCallback;
class persister;

/*! \class MxAHBMasterPort
 *
 *  \brief AHB master transaction port.
 */
class MxAHBMasterPort : public MxTransactionMasterPort
{
public:
#if CARBON
	MxAHBMasterPort(bool isAhbLite, sc_mx_m_base *_owner, const string& _portName, MxTransactionProperties *prop=NULL, MxU32 _busWidthInBytes=4);
#else
	MxAHBMasterPort(sc_mx_m_base *_owner, const string& _portName, MxTransactionProperties *prop=NULL, MxU32 _busWidthInBytes=4);
#endif
	virtual ~MxAHBMasterPort();

	/*! \brief Get AHB Master version */
	virtual string getVersion(void) const { return MX_AHB_MASTER_VERSION_STRING; }

	void reset(void);
	bool isAvailable(void);
	bool isIdle(void) const;
	bool isTransactionFinished(void) const;
	bool isIdleFinished(void) { return m_bIdleFinished; };

	MxU32* getReadData(void);
	MxStatus setWriteData(MxU32* pData);
	void startRead (MxU32 addr, MxU32 acc);
	void startWrite(MxU32 addr, MxU32 acc);
	void startIdle(MxU32 acc);
	MxStatus driveBus(void);
	TAHBTransaction* getNextStage(void) { return m_nextStage; };
	TAHBTransaction* getGrntStage(void) { return m_grntStage; };
	TAHBTransaction* getAddrStage(void) { return m_addrStage; };
	TAHBTransaction* getDataStage(void) { return m_dataStage; };
	bool getHReady(void) const { return m_pSignals->hready_z1; };
	bool getGranted(void) const { return m_bGranted; } ;

	void setDataBusWidth(const MxU32 busWidthInBytes);
    
	void decodeACC(TAHBTransaction* const accSetupPipeStage) const;
        
	TAHBSignals *m_pSignals;
    MxU32 m_portID;

    PrepAddrDataPhaseCallback* m_prepAddrDataPhaseCB;
    void registerPrepAddrDataPhaseCB( PrepAddrDataPhaseCallback* cbxface ) {
        m_prepAddrDataPhaseCB = cbxface;
    }
    void SetAlignData( bool alignData ) { p_alignData = alignData; }
    bool persist(persister & p);

private:
	MxStatus driveNextStage(void);
	MxStatus driveGrntStage(void);
	MxStatus driveAddrStage(void);
	MxStatus driveDataStage(void);

	TAHBTransaction* startAHB(void);
	void promoteNextStageToGrntStage(void);
	void promoteGrntStageToAddrStage(void);
	void promoteAddrStageToDataStage(void);
	void decodeACC(void);
	void setAcc(TAHBTransaction * const pStage, const MxU32 acc) const;
	bool getHGrant(void);

	void releaseStage(TAHBTransaction * const pTrans) const;
	void releaseBuffers(void) const;

    bool persistTransaction(persister & p, TAHBTransaction & transaction);
    bool persistTransactionPointer(persister & p, TAHBTransaction* & pTransaction);
    bool persistQueue(persister & p, std::deque<MxU32*> & q);
    void resetTransaction(TAHBTransaction & transaction) const;

private:
	sc_mx_m_base *m_owner;      //!< Reference to owner
	bool p_alignData;
	MxU32 m_busWidthInBytes;    //!< Byte width 4, 8, 16 or 32

	bool m_bGranted;
	bool m_bDrive1stAddr;
	bool m_bReseted;
	bool m_bIsRequested;
	bool m_bBusreq;
	bool m_bLock;
    bool m_bIdleFinished;
    bool m_defaultMaster;
    
	MxU32 m_RequestAddr;
	MxU32 m_RequestAcc;
	bool m_RequestIsWrite;

	std::deque<MxU32*> m_qReadData;
	std::deque<MxU32*> m_qWriteData;

	TAHBTransaction m_pipeStages[NUM_AHB_CYCLES];  //!< AHB Transactions pointed in to. \see m_nextStage m_grntStage m_addrStage m_dataStage

	TAHBTransaction *m_nextStage, *m_grntStage, *m_addrStage, *m_dataStage; //!< NULL or point in to m_pipeStages

    TAHBTransaction *m_startStage;  //!< NULL or points to m_nextStage
    TAHBTransaction *m_tmpStage;    //!< NULL or points to same place as m_addrStage, m_nextStage, m_grntStage or m_dataStage

	MxU64 m_idCounter;

	static const MxU32 m_numOfBeats[8];
	static const MxU32 m_wrapFactor[8];
	static const MxU32 m_transferSize[8];
    
	bool m_bInBurst;
	MxU32 m_bGrantCheckedThisCycle;
        bool mIsAhbLite;

	MxU32 mLastAddrStageHDrivenHtrans;
};

class PrepAddrDataPhaseCallback
{
public:
	virtual void prepAddrPhaseCB( TAHBTransaction* ) = 0;
	virtual void prepDataPhaseCB(void) = 0;
};


#endif

