// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2005-2006 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.


#ifndef _AHB_GLOBALS__H_
#define _AHB_GLOBALS__H_

/*
 * Enumerated indices for ctrl[] array (*ctrl) of MxU32 quantities,
 * passed as third argument of MaxSim's read(),write() methods
 */
typedef enum {
    MX_IDX_TYPE = 0,   // Access type/size
    MX_IDX_CYCLE = 1,  // Cycle type address/data
    MX_IDX_ACC = 2,    // Access information
    MX_IDX_ACK = 3,    // The response/acknowledgement     
    MX_IDX_END = 4     // Add your new indices before this (always represent the last index)
} MX_CTRL_IDX;


/*
 * Access type:
 * ctrl[MX_IDX_TYPE]
 * to be set by requester (master)
 */
typedef enum {
    MX_TYPE_NONE = 0,   // -
    MX_TYPE_BYTE = 1,   // 8bit
    MX_TYPE_HWORD = 2,  // 16bit
    MX_TYPE_WORD = 3,   // 32bit
    MX_TYPE_DWORD = 4,   // 64bit
	MX_TYPE_128BIT = 5,	// 128bit
	MX_TYPE_256BIT = 6,	// 256bit
	MX_TYPE_512BIT = 7,	// 512bit
	MX_TYPE_1024BIT = 8	// 1024bit
} MX_ACCESS_TYPE;

typedef enum {
	MX_CYCLE_NONE = 0,
	MX_CYCLE_ADDR = 1,
	MX_CYCLE_DATA = 3,
	MX_CYCLE_GRNT = 4
} MX_CYCLE_TYPE;

/*
 * Response field:
 * ctrl[MX_IDX_ACK]
 * to be set by slave (memory or peripheral)
 */
typedef enum {
    MX_ACK_DONE = 0,  // An Okay response (used in conjunction with hready)
    MX_ACK_WAIT = 1,  // A wait response 
	MX_ACK_ABORT = 3,  // An abort response	
    MX_ACK_RETRY = 4, // A retry response (must be a two-cycle response)
    MX_ACK_SPLIT = 5  // A split response (must be a two-cycle response)
} MX_ACK_TYPE;


#define AHB_DATA_WIDTH 32


#define AHB_INIT_TRANSACTION_PROPERTIES(props)  \
	(props).mxsiVersion = MXSI_VERSION_6;	/* this protocol uses standard read/write */ \
    (props).useMultiCycleInterface = false;	/* synchronous access functions	 */ \
	(props).addressBitwidth = 32;			/* address bitwidth used for addressing of resources  */ \
    (props).mauBitwidth = 8;				/* minimal addressable unit  */ \
    (props).dataBitwidth = AHB_DATA_WIDTH;	/* maximum data bitwidth transferred in a single cycle */ \
    (props).isLittleEndian = true;			/* alignment of MAUs */ \
    (props).isOptional = false;				/* if true this port can be disabled */ \
    (props).supportsAddressRegions = true;	/* M/S can not negotiate address mapping */ \
    (props).numCtrlFields = 4;		        /* # of ctrl elements used */ \
    (props).dataBeats = 16;		        /* # of data beats */ \
    (props).protocolID = AHB_PROTOCOL_ID;	/* magic number of the protocol (AHB Protocol!) */ \
    (props).numSlaveFlags = 0;				/* not used but initialized */ \
    (props).numMasterFlags = 0;				/* not used but initialized */ \
    (props).numTransactionSteps = 0;		/* not used but initialized */ \
    (props).supportsBurst = false;          /* not used but initialized */ \
    (props).supportsSplit = false;			/* not used but initialized */ \
    (props).supportsNotify = false;         /* not used but initialized */ \
    (props).validTimingTable = NULL;	    /* not used but initialized */ \
    (props).details = NULL;                 /* not used but initialized */ \
    (props).forwardAddrRegionToMasterPort = NULL;   /* master port of the  same component to which this slave port's addr region is forwarded */ \
    (props).isAddrRegionForwarded = false;          /* MxSI 6.0: slave port address regions forwarding */

#define AHB_PROTOCOL_ID 0x10A8B

struct MxB_Access
{
	char port;
	MxU32 address;
};

struct MxB_Delayed_Write
{
	bool write_request;
	MxU32 segment;
	MxU32 location;
	bool unaligned_access;
	MxU32 new_value;
	MxU32 new_value2;
};

#define AHB_MAX_MASTERS 32
#define AHB_MAX_SLAVES	32
// complete set of AHB signals
// currently not all are used
typedef struct TAHBSignals
{
    MxU32 hclk;									 // not modelled, always 1
    MxU32 hreset;								 // not modelled, always 1
    MxU32 haddr;								 // value of address bus         (can safely be read in update())
    MxU32 htrans;								 // transfer type                (can safely be read in update())
    MxU32 hwrite;								 // write strobe                 (can safely be read in update())
    MxU32 hsize;								 // data size info               (can safely be read in update())
    MxU32 hburst;								 // burst info                   (can safely be read in update())
    MxU32 hprot;								 // protection info              (can safely be read in update())
    MxU32 hwdata[4];							 // write data bus               (can safely be read in update())
    MxU32 hrdata[4];							 // read data bus                (can safely be read in update())
    MxU32 hsel;					 				 // not modelled, always 0
    MxU32 hready;								 // handshake signal             (can safely be read in update())
    MxU32 hresp;								 // not modelled, always 0       (AHB_OK -> AHB_ACK_xxx)
    MxU32 hbusreq[AHB_MAX_MASTERS];				 // request vector               (can safely be read in update())
    MxU32 hlock[AHB_MAX_MASTERS];				 // lock vector                  (can safely be read in update())
    MxU32 hgrant;								 // bus grant                    (can safely be read in communicate(), use checkForGrant())
    MxU32 hmaster;								 // selected master id           (can safely be read in communicate())
    MxU32 hmastlock;							 // lock of the granted master   (can safely be read in update())
    MxU16 hsplit[AHB_MAX_MASTERS];				 // not modelled, always 0
    MxU32 hready_z1;							 // hready delayed by one cycle, (can safely be read in commuicate(), maintained by AHB bus)
    MxU32 hgrant_z1;							 // hgrant delayed by one cycle, (can safely be read in commuicate(), maintained by AHB bus)
} TAHBSignals;

#undef UNUSEDARG
#if defined(__STDC__) || defined(__cplusplus)
#define UNUSEDARG(X) ((void)(X)) /* Silence compiler warnings for unused arguments */
#else
#define UNUSEDARG(X) ((X) = (X))
#endif

#include <AHB_acc.h>

#endif // ifndef _AHB_GLOBALS__H_
