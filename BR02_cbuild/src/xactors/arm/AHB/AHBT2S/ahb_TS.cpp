// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#include "ahb_TS.h"

#if CARBON
#include "carbon_ahb.h"
#endif
#include "AHBT2S.h"

#if MAXSIM_MAJOR_VERSION >= 7
#include "AHB_Transaction7.h"
#include "AHB_Transaction_Ext.h"
#else
#include "AHB_Transaction.h"
#endif

#if CARBON
ahb_TS::ahb_TS( AHBT2S *_owner, bool isAhbLite ) : sc_mx_transaction_slave( "ahb_TS" )
#else
ahb_TS::ahb_TS( AHBT2S *_owner ) : sc_mx_transaction_slave( "ahb_TS" )
#endif
{
    owner = _owner;
#if CARBON
    mIsAhbLite = isAhbLite;
    setMxOwner(owner->mCarbonComponent);
#else
    mIsAhbLite = false;
    setMxOwner(owner);
#endif

    AHB_INIT_TRANSACTION_PROPERTIES(prop);
    setProperties(&prop);

    firstError = true;
#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
    initMemoryMapConstraints();
#endif

}

#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
// *****************************************************************
// ahb_TS::initMemoryMapconstraints() -- initialize constraints
// *****************************************************************
void
ahb_TS::initMemoryMapConstraints()
{
    const MxTransactionProperties *mxProps = getProperties();
    puMemoryMapConstraints.minRegionSize = 0x0;
    puMemoryMapConstraints.maxRegionSize = (MxU64CONST(0x1) << mxProps->addressBitwidth);
    puMemoryMapConstraints.minAddress = 0x0;
    puMemoryMapConstraints.maxAddress = (MxU64CONST(0x1) << mxProps->addressBitwidth) - 1;
    puMemoryMapConstraints.minNumSupportedRegions = 0x1;
    puMemoryMapConstraints.maxNumSupportedRegions = REG_REGIONS;
    puMemoryMapConstraints.alignmentBlockSize = 0x400;
    puMemoryMapConstraints.numFixedRegions = 0;
    puMemoryMapConstraints.fixedRegionList = NULL;
    puMemoryMapConstraints.details = NULL;
}
#endif

/* If no regions are defined, numRegions returns 0, indicating that the
 * port wants to be called for any MxU64 address */
int
ahb_TS::getNumRegions()
{
    AddressRegion reg;
    char name[10];
    if (mAddressRegions.size() == 0) // not using MME
    {
        for (int i = 0; i < MAX_PARAM_REGIONS; ++i)
        {
            if (owner->p_size[i] != 0)
            {
                reg.mStart = owner->p_start[i];
                reg.mSize = owner->p_size[i];
                sprintf(name, "ahb_TS_%d", i);
                reg.mName = name;
                mAddressRegions.push_back(reg);
            }
        }        
    }
    return mAddressRegions.size();
}

void
ahb_TS::getAddressRegions(MxU64* start, MxU64* size, string* name)
{
    for (unsigned int i = 0; i < mAddressRegions.size(); ++i)
    {
        start[i] = mAddressRegions[i].mStart;
        size[i] = mAddressRegions[i].mSize;
        name[i] = mAddressRegions[i].mName;
    }
}

#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
void
ahb_TS::setAddressRegions(MxU64* start, MxU64* size, string* name)
{
    AddressRegion reg;
    if (start && size && name)
    {
        mAddressRegions.clear();
        if (size[0] != 0)
        {
            owner->p_Base = start[0];
            owner->p_Size = size[0];
        }
        if (owner->p_enableDbgMsg)
        {
            string port_name = this->getPortInstanceName();
            owner->message(MX_MSG_INFO, "Set Address Region Called for Port %s", port_name.c_str());
        }
        for (unsigned int i = 0; i < REG_REGIONS; i++)
        {
            if (size[i] != 0)
            {
                reg.mStart = start[i];
                reg.mSize = size[i];
                reg.mName = name[i];
                mAddressRegions.push_back(reg);
                if (owner->p_enableDbgMsg)
                    owner->message(MX_MSG_INFO, "Address Region: start= 0x" MxFMT64x " size = 0x" MxFMT64x " Name= %s",
                                   start[i], size[i],
                                   name[i].c_str());
            }
            else
            {
                if (owner->p_enableDbgMsg)
                    owner->message(MX_MSG_INFO, "Address Region (ignored due to size): start= 0x" MxFMT64x " size = 0x" MxFMT64x " Name= %s",
                                   start[i], size[i],
                                   name[i].c_str());
                break;
            }
        }
    }
    else
    {
#if (!CARBON)
        owner->message(MX_MSG_ERROR, "%s: Uninitialized data structures passed in setAddressRegions call",
                       owner->getInstanceID().c_str());
#else
        owner->message(MX_MSG_ERROR, "AHB T2S: Uninitialized data structures passed in setAddressRegions call");
#endif
    }
}

// ************************************************************
// ahb_TS::getMappingConstraints() -- get slave constraints
// ************************************************************

MxMemoryMapConstraints*
ahb_TS::getMappingConstraints()
{
    return &puMemoryMapConstraints;
}
#endif

MxStatus
ahb_TS::read(MxU64 addr, MxU32* value, MxU32* ctrl)
{
    MxStatus status = MX_STATUS_OK;
    
    if (ctrl[AHB_IDX_CYCLE] == MX_CYCLE_ADDR)
    {
        // Latch address and control info used to drive signals in update phase
        owner->haddr  = owner->p_subtractBaseAddr ? addr - owner->p_Base : addr;
        owner->hwrite = AHB_READ;
        owner->htrans = AHB_ACC_DECODE_HTRANS(ctrl[AHB_IDX_ACC]);
        owner->hburst = AHB_ACC_DECODE_HBURST(ctrl[AHB_IDX_ACC]);
        owner->hprot  = AHB_ACC_DECODE_HPROT(ctrl[AHB_IDX_ACC]);
        owner->hsize  = AHB_ACC_DECODE_HSIZE(ctrl[AHB_IDX_ACC]);
#if MAXSIM_MAJOR_VERSION >= 7
        owner->hprot |= AHB_SIDEBAND_DECODE_ALLOC(ctrl[AHB_IDX_SIDEBAND]); // hprot[4]
        owner->hprot |= AHB_SIDEBAND_DECODE_EXREQ(ctrl[AHB_IDX_SIDEBAND]); // hprot[5]
        owner->hstrb  = AHB_SIDEBAND_DECODE_HBSTRB(ctrl[AHB_IDX_SIDEBAND]);
        owner->hunalign = AHB_SIDEBAND_DECODE_HUNALIGN(ctrl[AHB_IDX_SIDEBAND]);
        owner->hdomain  = AHB_SIDEBAND_DECODE_HDOMAIN(ctrl[AHB_IDX_SIDEBAND]);
#endif

        owner->p_slaveSelected = true;
    }
    else if (ctrl[AHB_IDX_CYCLE] == MX_CYCLE_DATA)
    {
        // Determine the read data
        memcpy( value, &owner->latchedFromRTL[ HRDATA_SS_31_0 ],
                sizeof(MxU32) * owner->dataWidthInWords );

        // replicate the data across all byte lanes, if size < data bus width
        if ((MxU32)(1 << AHB_ACC_DECODE_HSIZE(ctrl[AHB_IDX_ACC])) <
            (owner->dataWidthInWords * 4))
        {
            MxU32 index, offset, mask, tmpData, wordData;

            // determine word index
            index = (addr & ((owner->dataWidthInWords - 1) << 2)) >> 2;

            switch(ctrl[AHB_IDX_TYPE])
            {
                case MX_TYPE_BYTE:
                    // determine which byte to replicate
                    offset = (addr & 0x03);
                    if (owner->bigEndian) offset ^= 3;
                    offset *= 8;
                    mask   = 0xFF << offset;
                    tmpData = (value[index] & mask) >> offset;

                    // replicate across whole of data bus
                    memset(value, tmpData,
                        sizeof(MxU32) * owner->dataWidthInWords);
                    break;

                case MX_TYPE_HWORD:
                    // determine which halfword to replicate
                    offset = (addr & 0x03);
                    if (owner->bigEndian) offset ^= 2;
                    offset *= 8;
                    mask   = 0xFFFF << offset;
                    tmpData = (value[index] & mask) >> offset;

                    // replicate across word
                    wordData = tmpData | (tmpData << 16);

                    // replicate across whole of data bus
                    for (MxU32 i= 0; i < owner->dataWidthInWords; i++)
                    {
                        value[i] = wordData;
                    }
                    break;

                case MX_TYPE_WORD:
                    // determine which word to replicate
                    wordData = value[index];

                    // replicate across whole of data bus
                    for (MxU32 i= 0; i < owner->dataWidthInWords; i++)
                    {
                        value[i] = wordData;
                    }
                    break;

                case MX_TYPE_DWORD:
                    switch (owner->dataWidthInWords)
                    {
                    case 1: // bus is 32-bits
                        owner->message(MX_MSG_ERROR, "%s::read(): Data is too large for bus.", owner->getInstanceID().c_str());
                        break;
                    case 2: // bus and data width are both 64-bit, leave it alone
                        break;
                    case 4: // bus is 128-bits
                        // replicate according to dword index
                        if ((addr & 0x08) >> 3)
                        {
                            value[0] = value[2];
                            value[1] = value[3];
                        }
                        else
                        {
                            value[2] = value[0];
                            value[3] = value[1];
                        }
                        break;
                    }
                    break;

                case MX_TYPE_128BIT:
                    switch (owner->dataWidthInWords)
                    {
                    case 1:
                    case 2:
                        owner->message(MX_MSG_ERROR, "%s::read(): Data is too large for bus.", owner->getInstanceID().c_str());
                        break;
                    case 4:
                        // 128-bit width is largest supported, no need to replicate
                        break;
                    }
                    break;

                default:
                {
                    owner->message(MX_MSG_ERROR, "%s::read(): Data is too large for bus.", owner->getInstanceID().c_str());
                    break;
                }

            }
        }

        // Determine the response
        switch(owner->latchedFromRTL[HRESP_SS])
        {
        case AHB_OK:
            ctrl[AHB_IDX_ACK] = MX_ACK_DONE;
            break;
        case AHB_ERROR:
            ctrl[AHB_IDX_ACK] = MX_ACK_ABORT;
            break;
        case AHB_RETRY:
          if (!mIsAhbLite) {
            ctrl[AHB_IDX_ACK] = MX_ACK_RETRY;
          }
            break;
        case AHB_SPLIT:
          if (!mIsAhbLite) {
            ctrl[AHB_IDX_ACK] = MX_ACK_SPLIT;
          }
            break;
        default:
            break;
        }
#if MAXSIM_MAJOR_VERSION >= 7
        if (!mIsAhbLite)
            AHB_SIDEBAND_SET_ALLOC(ctrl[AHB_IDX_SIDEBAND], AHB_HRESP_DECODE_EXRESP(owner->latchedFromRTL[HRESP_SS])); // hresp[2];
#endif

        // Determine whether the response is waited
        if (!owner->latchedFromRTL[HREADYOUT_SS])
            status = MX_STATUS_WAIT;
    }

    return status;
}



MxStatus
ahb_TS::write(MxU64 addr, MxU32* value, MxU32* ctrl)
{
    MxStatus status = MX_STATUS_OK;

    if (ctrl[AHB_IDX_CYCLE] == MX_CYCLE_ADDR)
    {

        // Latch address and control info used to drive signals in update phase
        owner->haddr  = owner->p_subtractBaseAddr ? addr - owner->p_Base : addr;
        owner->hwrite = AHB_WRITE;
        owner->hburst = AHB_ACC_DECODE_HBURST(ctrl[AHB_IDX_ACC]);
        owner->htrans = AHB_ACC_DECODE_HTRANS(ctrl[AHB_IDX_ACC]);
        owner->hprot  = AHB_ACC_DECODE_HPROT(ctrl[AHB_IDX_ACC]);
        owner->hsize  = AHB_ACC_DECODE_HSIZE(ctrl[AHB_IDX_ACC]);
#if MAXSIM_MAJOR_VERSION >= 7
        owner->hprot |= AHB_SIDEBAND_DECODE_ALLOC(ctrl[AHB_IDX_SIDEBAND]); // hprot[4]
        owner->hprot |= AHB_SIDEBAND_DECODE_EXREQ(ctrl[AHB_IDX_SIDEBAND]); // hprot[5]
        owner->hstrb  = AHB_SIDEBAND_DECODE_HBSTRB(ctrl[AHB_IDX_SIDEBAND]);
        owner->hunalign = AHB_SIDEBAND_DECODE_HUNALIGN(ctrl[AHB_IDX_SIDEBAND]);
        owner->hdomain  = AHB_SIDEBAND_DECODE_HDOMAIN(ctrl[AHB_IDX_SIDEBAND]);
#endif

        owner->p_slaveSelected = true;
    }
    else if (ctrl[AHB_IDX_CYCLE] == MX_CYCLE_DATA)
    {
	// replicate the data across all byte lanes, if size < data bus width
        if ((MxU32)(1 << AHB_ACC_DECODE_HSIZE(ctrl[AHB_IDX_ACC])) <
            (owner->dataWidthInWords * 4))
        {
            MxU32 index, offset, mask, tmpData, wordData;

            // determine word index
#if CARBON
            // When writing, always use the low-order bytes of the low-order word.
            index = 0;
#else
            index = (addr & ((owner->dataWidthInWords - 1) << 2)) >> 2;
#endif

            switch(ctrl[AHB_IDX_TYPE])
            {
                case MX_TYPE_BYTE:
                    // determine which byte to replicate
                    offset = 0;
                    if (owner->bigEndian) offset ^= 3;
                    offset *= 8;
                    mask   = 0xFF << offset;
                    tmpData = (value[index] & mask) >> offset;

                    // replicate across whole of data bus
                    memset(value, tmpData,
                        sizeof(MxU32) * owner->dataWidthInWords);
                    break;

                case MX_TYPE_HWORD:
                    // determine which halfword to replicate
                    offset = 0;
                    if (owner->bigEndian) offset ^= 2;
                    offset *= 8;
                    mask   = 0xFFFF << offset;
                    tmpData = (value[index] & mask) >> offset;

                    // replicate across word
                    wordData = tmpData | (tmpData << 16);

                    // replicate across whole of data bus
                    for (MxU32 i= 0; i < owner->dataWidthInWords; i++)
                    {
                        value[i] = wordData;
                    }
                    break;

                case MX_TYPE_WORD:
                    // determine which word to replicate
                    wordData = value[index];

                    // replicate across whole of data bus
                    for (MxU32 i= 0; i < owner->dataWidthInWords; i++)
                    {
                        value[i] = wordData;
                    }
                    break;

                case MX_TYPE_DWORD:
                    switch (owner->dataWidthInWords)
                        {
                        case 1: // bus is 32-bits
                            owner->message(MX_MSG_ERROR, "%s::write(): Data is too large for bus.", owner->getInstanceID().c_str());
                            break;
                        case 2: // bus and data widths are both 64-bit, leave it alone
                            break;
                        case 4: // bus is 128-bits
                            // replicate according to dword index
                            if ((addr & 0x08) >> 3)
                            {
                                value[0] = value[2];
                                value[1] = value[3];
                            }
                            else
                            {
                                value[2] = value[0];
                                value[3] = value[1];
                            }
                            break;
                        }
                        break;

                    case MX_TYPE_128BIT:
                        switch (owner->dataWidthInWords)
                        {
                        case 1:
                        case 2:
                            owner->message(MX_MSG_ERROR, "%s::write(): Data is too large for bus.", owner->getInstanceID().c_str());
                            break;
                        case 4: // bus and data widths are both 128-bit, leave it alone
                            break;
                        }
                        break;

                    default:
                    {
                        owner->message(MX_MSG_ERROR, "%s::write(): Data is too large for bus.", owner->getInstanceID().c_str());
                        break;
                    }
            }
        }

	memcpy( owner->hwdata, &value[0], sizeof(MxU32) * owner->dataWidthInWords ); 

        // Determine the response
        switch(owner->latchedFromRTL[HRESP_SS])
        {
        case AHB_OK:
            ctrl[AHB_IDX_ACK] = MX_ACK_DONE;
            break;
        case AHB_ERROR:
            ctrl[AHB_IDX_ACK] = MX_ACK_ABORT;
            break;
        case AHB_RETRY:
          if (!mIsAhbLite) {
            ctrl[AHB_IDX_ACK] = MX_ACK_RETRY;
          }
            break;
        case AHB_SPLIT:		
          if (!mIsAhbLite) {
            ctrl[AHB_IDX_ACK] = MX_ACK_SPLIT;
          }
            break;
        default:
            break;
        }
#if MAXSIM_MAJOR_VERSION >= 7
        if (!mIsAhbLite)
            AHB_SIDEBAND_SET_ALLOC(ctrl[AHB_IDX_SIDEBAND], AHB_HRESP_DECODE_EXRESP(owner->latchedFromRTL[HRESP_SS])); // hresp[2];
#endif

        // Determine whether the response is waited
        if (!owner->latchedFromRTL[HREADYOUT_SS])
        {
            status = MX_STATUS_WAIT;
        }
    }

    return status;
}


MxStatus 
ahb_TS::readDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{   
#if CARBON
  if (owner->p_subtractBaseAddrDbg)
    addr -= owner->p_Base;
  return CarbonDebugFunctionsToDebugAccess(owner->mDbaCb, eCarbonDebugAccessRead, owner->dataWidthInWords*32, addr, value, ctrl);
#else
  if (firstError)
  {
    pmessage(MX_MSG_WARNING, "readDbg from addr:0x%08x is not supported on the HDL memory",
             (MxU32)addr);
    firstError = false;    
  }     
  return MX_STATUS_OK;
#endif
}


MxStatus 
ahb_TS::writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
#if CARBON
  if (owner->p_subtractBaseAddrDbg)
    addr -= owner->p_Base;
  return CarbonDebugFunctionsToDebugAccess(owner->mDbaCb, eCarbonDebugAccessWrite, owner->dataWidthInWords*32, addr, value, ctrl);
#else
  if (firstError)
  {
    pmessage(MX_MSG_WARNING, "writeDbg from addr:0x%08x is not supported on the HDL memory",
             (MxU32)addr);
    firstError = false;    
  }     
  return MX_STATUS_OK;
#endif
}


MxStatus
ahb_TS::readReq(MxU64 addr, MxU32* value, MxU32* ctrl, MxTransactionCallbackIF* callback)
{
    MxStatus ret = MX_STATUS_NOTSUPPORTED;
    UNUSEDARG(addr);
    UNUSEDARG(value);
    UNUSEDARG(callback);

    if (ctrl)
    {
        // get a pointer to the central AHB signals structure
        // can be safely read in update()
        owner->pSignals = (TAHBSignals*) ctrl;
        ret = MX_STATUS_OK;
    }

    return ret;
}

MxStatus
ahb_TS::writeReq(MxU64 addr, MxU32* value, MxU32* ctrl, MxTransactionCallbackIF* callback)
{
    UNUSEDARG(addr);
    UNUSEDARG(value);
    UNUSEDARG(ctrl);
    UNUSEDARG(callback);

    return MX_STATUS_NOTSUPPORTED;
}
