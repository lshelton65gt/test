// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#ifndef CARBON
#define CARBON 0
#endif

#include "AHBT2S_RTL_SS.h"

#if CARBON
#include "carbon_ahb.h"
#endif

#include "AHBT2S.h"


AHBT2S_RTL_SS::AHBT2S_RTL_SS( AHBT2S *_owner, MxU32 _id ) : sc_mx_signal_slave( "AHBT2S_RTL_SS" )
{
	id = _id;
    owner = _owner;
#if CARBON
    setMxOwner(owner->mCarbonComponent);
#else
    setMxOwner(owner); 
#endif
}

void
AHBT2S_RTL_SS::driveSignal(MxU32 value, MxU32* extValue)
{
	assert(id < AHBT2S_SS_END);
    if ( id < HRDATA_SS )
    {
        owner->latchedFromRTL[id] = value;
        return;
    }

    // HRDATA handling
    owner->latchedFromRTL[ HRDATA_SS_31_0 ] = value;
    if ( owner->dataWidthInWords > 1 )
    {
        if ( extValue == NULL )
        {
            owner->message( MX_MSG_ERROR, "invalid hrdata received from RTL (extValue == NULL)" );
            return;
        }
        memcpy( &owner->latchedFromRTL[ HRDATA_SS_63_32 ], extValue, sizeof(MxU32) * (owner->dataWidthInWords - 1) );
    }
}

MxU32
AHBT2S_RTL_SS::readSignal()
{
    return 0;
}

void
AHBT2S_RTL_SS::readSignal(MxU32* value, MxU32* extValue)
{
  (void)value; (void)extValue;
}
