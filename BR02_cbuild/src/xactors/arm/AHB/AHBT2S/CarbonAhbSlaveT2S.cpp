#include "CarbonAhbSlaveT2S.h"

#define CARBON 1
#include "carbon_ahb.h"
#include "AHBT2S.h"
#include "AHBT2S_RTL_SS.h"

CarbonAhbSlaveT2S::CarbonAhbSlaveT2S(sc_mx_module* carbonComp,
                                     const char *xtorName,
                                     CarbonObjectID **carbonObj,
                                     CarbonPortFactory *portFactory, 
                                     CarbonDebugReadWriteFunction *readDebug,
                                     CarbonDebugReadWriteFunction *writeDebug,
                                     bool isAhbLite)
{
  CarbonDebugAccessCallbacks cb = {carbonComp, NULL, writeDebug, readDebug, NULL};
  mArmAdaptor = new AHBT2S(carbonComp, xtorName, isAhbLite ? XTOR_TYPE_AHBLITES_T2S : XTOR_TYPE_AHBS_T2S , carbonObj, portFactory, cb);
}

CarbonAhbSlaveT2S::CarbonAhbSlaveT2S(sc_mx_module* carbonComp,
                                     const char *xtorName,
                                     CarbonObjectID **carbonObj,
                                     CarbonPortFactory *portFactory, 
                                     CarbonDebugAccessFunction *debugAccess,
                                     bool isAhbLite)
{
  CarbonDebugAccessCallbacks cb = {carbonComp, debugAccess, NULL, NULL, NULL};
  mArmAdaptor = new AHBT2S(carbonComp, xtorName, isAhbLite ? XTOR_TYPE_AHBLITES_T2S : XTOR_TYPE_AHBS_T2S , carbonObj, portFactory, cb);
}

sc_mx_signal_slave *CarbonAhbSlaveT2S::carbonGetPort(const char *name)
{
  sc_mx_signal_slave *port;

  if (strcmp("hresp", name) == 0) {
    port = mArmAdaptor->fromRTL[HRESP_SS];
  }
  else if (strcmp("hreadyout", name) == 0) {
    port = mArmAdaptor->fromRTL[HREADYOUT_SS];
  }
  else if (strcmp("hrdata", name) == 0) {
    port = mArmAdaptor->fromRTL[HRDATA_SS];
  }
  else {
    port = NULL;
    mArmAdaptor->message(MX_MSG_FATAL_ERROR, "Carbon AHB Slave T2S has no port named \"%s\"", name);
  }
  return port;
}

CarbonAhbSlaveT2S::~CarbonAhbSlaveT2S()
{
  delete mArmAdaptor;
}

void CarbonAhbSlaveT2S::communicate()
{
  mArmAdaptor->communicate();
}

void CarbonAhbSlaveT2S::update()
{
  mArmAdaptor->update();
}

void CarbonAhbSlaveT2S::init()
{
  mArmAdaptor->init();
}

void CarbonAhbSlaveT2S::terminate()
{
  mArmAdaptor->terminate();
}

void CarbonAhbSlaveT2S::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  mArmAdaptor->reset(level, filelist);
}

void CarbonAhbSlaveT2S::setParameter(const string &name, const string &value)
{
  mArmAdaptor->setParameter(name, value);
}

//! Number of slave memory regions
int CarbonAhbSlaveT2S::getNumRegions()
{
  return mArmAdaptor->getNumSlaveRegions();
}

//! Slave Address regions
void CarbonAhbSlaveT2S::getAddressRegions(uint64_t* start, uint64_t* size, string* name)
{
  mArmAdaptor->getSlaveAddressRegions(start, size, name);
}

void CarbonAhbSlaveT2S::carbonSetPortWidth(const char* name, uint32_t bitWidth)
{
  mArmAdaptor->setPortWidth(name, bitWidth);
}
