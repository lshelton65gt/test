// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#ifndef _AHBT2S_RTL_SS_H_
#define _AHBT2S_RTL_SS_H_

#include "carbon_arm_adaptor.h"

class AHBT2S;

class AHBT2S_RTL_SS: public sc_mx_signal_slave
{
    AHBT2S *owner;
	MxU32 id;

public:
    AHBT2S_RTL_SS( AHBT2S *_owner, MxU32 _id );
    virtual ~AHBT2S_RTL_SS() {}

public:
    virtual void driveSignal(MxU32 value, MxU32* extValue);
    virtual MxU32 readSignal();
    virtual void readSignal(MxU32* value, MxU32* extValue);
};

#endif

