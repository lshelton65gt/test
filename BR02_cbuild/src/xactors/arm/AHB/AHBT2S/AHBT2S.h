// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#ifndef AHBT2S__H
#define AHBT2S__H

#include "maxsim.h"
#if MAXSIM_MAJOR_VERSION >= 7
#include "AHB_Transaction7.h"
#else
#include "AHB_Transaction.h"
#endif

#include "MxPortCheck.h"

#if CARBON
#include "CarbonAdaptorBase.h"
#include "util/UtCLicense.h"
#endif

typedef enum
{
    HRESP_SS = 0,
    HREADYOUT_SS,
    HRDATA_SS_31_0,
    HRDATA_SS_63_32,
    HRDATA_SS_95_64,
    HRDATA_SS_127_96,
    AHBT2S_SS_END
} AHBT2S_RTL_ENUM;

typedef enum
{
    XTOR_TYPE_AHBT2S = 0,
    XTOR_TYPE_AHBS_T2S,
    XTOR_TYPE_AHBLITES_T2S
} ahbXtorTypeEnum;

#define HRDATA_SS HRDATA_SS_31_0

#define AHB_DEFAULT_BUSWIDTH 32

// supporting up to 128bit data bus
#define MAX_DATA_WIDTH_IN_WORDS 4

#define MAX_PARAM_REGIONS 2

class AHBT2S_MxDI;

#if CARBON
class AHBT2S : public CarbonAdaptorBase
#else
class AHBT2S : public sc_mx_module
#endif
{
    friend class AHBT2S_RTL_SS;
    friend class ahb_TS;
    friend class AHBT2S_MxDI;

public:
    ahb_TS* ahb_TSlave;

    AHBT2S_RTL_SS* fromRTL[AHBT2S_SS_END];
    
    RTL_PORT *HADDR_SMaster;    
    RTL_PORT *HWDATA_SMaster;
    RTL_PORT *HWRITE_SMaster;
    RTL_PORT *HREADY_SMaster;
    RTL_PORT *HSEL_SMaster;
    RTL_PORT *HTRANS_SMaster;
    RTL_PORT *HBURST_SMaster;
    RTL_PORT *HSIZE_SMaster;
    RTL_PORT *HPROT_SMaster;
    RTL_PORT *HMASTER_SMaster;
    RTL_PORT *HMASTLOCK_SMaster;
    RTL_PORT *HDOMAIN_SMaster; // ARM11 AMBA2 ext
    RTL_PORT *HBSTRB_SMaster; // ARM11 AMBA2 ext
    RTL_PORT *HUNALIGN_SMaster; // ARM11 AMBA2 ext

    // constructor / destructor
#if CARBON
    AHBT2S(sc_mx_module* carbonComp, const char *xtorName, ahbXtorTypeEnum xtorType, CarbonObjectID **carbonObj, CarbonPortFactory *portFactory, 
           const CarbonDebugAccessCallbacks& dbaCb);
#else
    AHBT2S(sc_mx_m_base* c, const string &s, int xtorType);
#endif
    virtual ~AHBT2S();    

    // overloaded methods for clocked components
    void communicate();
    void update();

    // overloaded sc_mx_module methods
#if (!CARBON)
    string getName();
#endif
    void setParameter(const string &name, const string &value);
#if (!CARBON)
    string getProperty( MxPropertyType property );
#endif
    void setPortWidth(const char* name, uint32_t bitWidth);
    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);

#if CARBON
    CarbonDebugReadWriteFunction *mReadDebug;
    CarbonDebugReadWriteFunction *mWriteDebug;
#else
    MXDI* getMxDI();
#endif

private:
    MxU32   addressMask;        //!< Set in init() from p_Size

    bool p_initComplete;
    bool p_enableDbgMsg;
    AHBT2S_MxDI* p_mxdi;
    bool bigEndian;
    bool p_subtractBaseAddr;
    bool p_subtractBaseAddrDbg;
    int m_xtorType;
    bool p_alignData;
    bool p_filterHreadyIn;

    MxU32 dataWidthInWords;
    MxU32 addrWidthInWords;

    MxU64 p_Size;
    MxU64 p_Base;
    MxU64 p_start[MAX_PARAM_REGIONS];
    MxU64 p_size[MAX_PARAM_REGIONS];

    // Values for signals to RTL
    MxU64 haddr;
    MxU32 hwdata[ MAX_DATA_WIDTH_IN_WORDS ];
    MxU32 hwrite;
    MxU32 hready;
    MxU32 htrans;
    MxU32 hburst;
    MxU32 hsize;
    MxU32 hprot;
    MxU32 hmaster;
    MxU32 hmastlock;
    MxU32 hdomain; // ARM11 AMBA2 ext
    MxU32 hstrb; // ARM11 AMBA2 ext
    MxU32 hunalign; // ARM11 AMBA2 ext

    bool p_slaveSelected;
    bool mSlaveSelectedPrev;
    bool mDriveHreadyIn;
    CarbonDebugAccessCallbacks mDbaCb;
public:

    MxU32 latchedFromRTL[AHBT2S_SS_END];

    TAHBSignals *pSignals;
};

#endif
