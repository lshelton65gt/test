// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#include <stdio.h>

#ifndef CARBON
#define CARBON 0
#endif

#include "carbon_ahb.h"
#include "AHBT2S.h"

#include "AHBT2S_RTL_SS.h"
#include "ahb_TS.h"

#include "AHBT2S_MxDI.h"
// #include "lc.h"


#if CARBON
#include <stdarg.h>
#define AHBT2S_RTL_SM(NAME) portFactory(mCarbonComponent, xtorName, NAME, mpCarbonObject)
#else
#define AHBT2S_RTL_SM(NAME) new sc_port<sc_mx_signal_if>(this, NAME)
#endif


#if (!CARBON)
AHBT2S::AHBT2S(sc_mx_m_base* c, const string &s, int xtorType) : sc_mx_module(c, s)
#else
AHBT2S::AHBT2S(sc_mx_module* carbonComp, const char *xtorName, ahbXtorTypeEnum xtorType, CarbonObjectID **carbonObj, CarbonPortFactory *portFactory,
               const CarbonDebugAccessCallbacks& dbaCb)
  : CarbonAdaptorBase(carbonComp, xtorName, carbonObj), mDbaCb(dbaCb)
#endif
{
    m_xtorType = xtorType;

    for ( int i = 0; i < AHBT2S_SS_END; i++ )
        fromRTL[i] = NULL;

#if CARBON
    ahb_TSlave = new ahb_TS( this, xtorType == XTOR_TYPE_AHBLITES_T2S );
#else
    ahb_TSlave = new ahb_TS( this );
#endif
    registerPort(ahb_TSlave, "ahb" );

    maxsim::MxSignalProperties *signalprop = new maxsim::MxSignalProperties;
    signalprop->isOptional = false;

    // signals from RTL
    fromRTL[HRESP_SS] = new AHBT2S_RTL_SS( this, HRESP_SS );
    if (m_xtorType == XTOR_TYPE_AHBLITES_T2S) {
    signalprop->bitwidth = 1;
    } else {
    signalprop->bitwidth = 3; // hresp[2] = EXRESP
    }
    fromRTL[HRESP_SS]->setProperties(signalprop);
    registerPort( fromRTL[HRESP_SS], "hresp" );

    fromRTL[HREADYOUT_SS] = new AHBT2S_RTL_SS( this, HREADYOUT_SS );
    signalprop->bitwidth = 1;
    fromRTL[HREADYOUT_SS]->setProperties(signalprop);
    registerPort( fromRTL[HREADYOUT_SS], "hreadyout" );

    fromRTL[HRDATA_SS] = new AHBT2S_RTL_SS( this, HRDATA_SS );
    signalprop->bitwidth = 32;
    fromRTL[HRDATA_SS]->setProperties(signalprop);
    registerPort( fromRTL[HRDATA_SS], "hrdata" );

    // signals to RTL
    HADDR_SMaster = AHBT2S_RTL_SM( "HADDR" );
    signalprop->bitwidth = 32;
    HADDR_SMaster->setProperties(signalprop);
    registerPort( HADDR_SMaster, "haddr" );

    HWDATA_SMaster = AHBT2S_RTL_SM( "HWDATA" );
    signalprop->bitwidth = 32;
    HWDATA_SMaster->setProperties(signalprop);
    registerPort( HWDATA_SMaster, "hwdata" );

    HWRITE_SMaster = AHBT2S_RTL_SM( "HWRITE" );
    signalprop->bitwidth = 1;
    HWRITE_SMaster->setProperties(signalprop);
    registerPort( HWRITE_SMaster, "hwrite" );

    HTRANS_SMaster = AHBT2S_RTL_SM( "HTRANS" );
    signalprop->bitwidth = 2;
    HTRANS_SMaster->setProperties(signalprop);
    registerPort( HTRANS_SMaster, "htrans" );

    HBURST_SMaster = AHBT2S_RTL_SM( "HBURST" );
    signalprop->bitwidth = 3;
    HBURST_SMaster->setProperties(signalprop);
    registerPort( HBURST_SMaster, "hburst" );

    HSIZE_SMaster = AHBT2S_RTL_SM( "HSIZE" );
    signalprop->bitwidth = 3;
    HSIZE_SMaster->setProperties(signalprop);
    registerPort( HSIZE_SMaster, "hsize" );

    HPROT_SMaster = AHBT2S_RTL_SM( "HPROT" );
    signalprop->bitwidth = 6; // hprot[4] = ALLOC, hprot[5] = EXREQ
    HPROT_SMaster->setProperties(signalprop);
    registerPort( HPROT_SMaster, "hprot" );

    HMASTLOCK_SMaster = AHBT2S_RTL_SM( "HMASTLOCK" );
    signalprop->bitwidth = 1;
    HMASTLOCK_SMaster->setProperties(signalprop);
    if (m_xtorType == XTOR_TYPE_AHBT2S || m_xtorType == XTOR_TYPE_AHBS_T2S)
        registerPort( HMASTLOCK_SMaster, "hmastlock" );
    else
        registerPort( HMASTLOCK_SMaster, "hlock" );

    if (m_xtorType == XTOR_TYPE_AHBT2S || m_xtorType == XTOR_TYPE_AHBS_T2S)
    {
        HMASTER_SMaster = AHBT2S_RTL_SM( "HMASTER" );
        signalprop->bitwidth = 4;
        HMASTER_SMaster->setProperties(signalprop);
        registerPort( HMASTER_SMaster, "hmaster" );

        HDOMAIN_SMaster = AHBT2S_RTL_SM( "HDOMAIN" );
        signalprop->bitwidth = 4;
        HDOMAIN_SMaster->setProperties(signalprop);
        registerPort( HDOMAIN_SMaster, "hdomain");
    }
    else
        HMASTER_SMaster = NULL;

    HREADY_SMaster = AHBT2S_RTL_SM( "HREADY" );
    signalprop->bitwidth = 1;
    HREADY_SMaster->setProperties(signalprop);
    registerPort( HREADY_SMaster, "hready" );

    HSEL_SMaster = AHBT2S_RTL_SM( "HSEL" );
    signalprop->bitwidth = 1;
    HSEL_SMaster->setProperties(signalprop);
    registerPort( HSEL_SMaster, "hsel" );

    HBSTRB_SMaster = AHBT2S_RTL_SM( "HBSTRB" );
    signalprop->bitwidth = 16; // supports up to 128 bit data width
    HBSTRB_SMaster->setProperties(signalprop);
    registerPort( HBSTRB_SMaster, "hstrb" );

    HUNALIGN_SMaster = AHBT2S_RTL_SM( "HUNALIGN" );
    signalprop->bitwidth = 1;
    HUNALIGN_SMaster->setProperties(signalprop);
    registerPort( HUNALIGN_SMaster, "hunalign" );

#if (!CARBON)
    SC_MX_CLOCKED();
    registerPort( dynamic_cast< sc_mx_clock_slave_p_base* >( this ), "clk-in");
#endif

    p_initComplete = false;
    p_mxdi = NULL;
    p_Size = 0;

#if CARBON
    // initialize to NULL so we can avoid core dump by checking for NULL later.
    pSignals = NULL;
#else
    // these are defined in the owning Carbon component
    defineParameter( "Enable Debug Messages", "false", MX_PARAM_BOOL, true);
    defineParameter( "Base Address", "0x00000000", MX_PARAM_VALUE, false);
    defineParameter( "Size", "0x100000000", MX_PARAM_VALUE, false);
    defineParameter( "Data Bus Width", "32", MX_PARAM_VALUE, false);
    defineParameter( "Address Bus Width", "32", MX_PARAM_VALUE, false);
    defineParameter( "Big Endian", "false", MX_PARAM_BOOL, false);
    defineParameter( "Subtract Base Address", "true", MX_PARAM_BOOL, false);
    defineParameter( "ahb_start1", "0x00000000", MX_PARAM_VALUE, false);
    defineParameter( "ahb_size1", "0x0", MX_PARAM_VALUE, false);
#endif
}

AHBT2S::~AHBT2S()
{
    delete ahb_TSlave;
    for ( int i = 0; i < AHBT2S_SS_END; i++ )
    {
        if ( fromRTL[i] )
        {
            delete fromRTL[i];
            fromRTL[i] = NULL;
        }
    }
    delete HADDR_SMaster;
    delete HWDATA_SMaster;
    delete HWRITE_SMaster;
    delete HTRANS_SMaster;
    delete HBURST_SMaster;
    delete HSIZE_SMaster;
    delete HPROT_SMaster;
    delete HMASTLOCK_SMaster;
    delete HREADY_SMaster;
    delete HSEL_SMaster;
    if (HMASTER_SMaster)
    {
        delete HMASTER_SMaster;
        delete HDOMAIN_SMaster;
    }
    delete HBSTRB_SMaster;
    delete HUNALIGN_SMaster;
}

void
AHBT2S::communicate()
{
}

void
AHBT2S::update()
{
#if CARBON
  // If not connected, ahb_TS::readReq will not have been called to initialize
  // pSignals yet.  Instead of just crashing, we return without doing anything.
  if (!pSignals) { return; }
#endif
    // Drive HREADY from System HREADY
    if (pSignals->hready == 0)
    {
        hready = AHB_WAIT;
    }
    else
    {
        hready = AHB_READY;
    }

    if (p_slaveSelected)
    {
        HSEL_SMaster->driveSignal(1, NULL);
        if ( addrWidthInWords > 1)
#ifdef SUNOS
            HADDR_SMaster->driveSignal(((MxU32*)&haddr)[1], &((MxU32*)&haddr)[0]);
#else
            HADDR_SMaster->driveSignal(((MxU32*)&haddr)[0], &((MxU32*)&haddr)[1]);
#endif
        else
#ifdef SUNOS
            HADDR_SMaster->driveSignal(((MxU32*)&haddr)[1], NULL);
#else
            HADDR_SMaster->driveSignal(((MxU32*)&haddr)[0], NULL);
#endif
        HWRITE_SMaster->driveSignal(hwrite, NULL);
        HTRANS_SMaster->driveSignal(htrans, NULL);
        HBURST_SMaster->driveSignal(hburst, NULL);
        HSIZE_SMaster->driveSignal(hsize, NULL);
        HPROT_SMaster->driveSignal(hprot, NULL);
        HWDATA_SMaster->driveSignal(hwdata[0], &hwdata[1]);
        HBSTRB_SMaster->driveSignal(hstrb, NULL);
        HUNALIGN_SMaster->driveSignal(hunalign, NULL);

        if ( HMASTER_SMaster )
        {
            HMASTER_SMaster->driveSignal(pSignals->hmaster, NULL);
            HDOMAIN_SMaster->driveSignal(hdomain, NULL);
        }
        HMASTLOCK_SMaster->driveSignal(pSignals->hmastlock, NULL);

        // Always drive HREADYIN when selected
        mDriveHreadyIn = true;
    }
    else if (mSlaveSelectedPrev)
    {
        // If the slave has just been deselected, drive these
        // signals to their idle values
        HSEL_SMaster->driveSignal(0, NULL);
        HTRANS_SMaster->driveSignal(AHB_IDLE, NULL);

        // Ensure last data phase   is driven
        HWDATA_SMaster->driveSignal(hwdata[0], &hwdata[1]);

        // Always drive HREADYIN as well
        mDriveHreadyIn = true;
    }

    if (mDriveHreadyIn) {
      if (hready == AHB_READY) {
        HREADY_SMaster->driveSignal(1, NULL);

        // If HREADY filtering is enabled, then we can stop
        // driving it once it changes from WAIT to READY.
        // This ensures that the Carbon Model sees its own HREADY assertion
        // if it had delayed the last data phase of a transaction.
        if (p_filterHreadyIn)
          mDriveHreadyIn = false;
      } else
        HREADY_SMaster->driveSignal(0, NULL);
    }

    mSlaveSelectedPrev = p_slaveSelected;
    p_slaveSelected = false;
}

void AHBT2S::setPortWidth(const char* name, uint32_t bitWidth)
{
  if (strcmp(name, "hwdata") == 0 || strcmp(name, "hrdata") == 0 || (strcmp(name, "haddr") == 0)) {

    // Calculate word count based on the next power of 2 above the given bit width.
    int wordCount = nextPo2(bitWidth)>>5;

    if (strcmp(name, "haddr") == 0)
      addrWidthInWords = wordCount & 0x3; // Limit address to 64 bit max
    else
      dataWidthInWords = wordCount;
  }
}

void
AHBT2S::init()
{
    if ( dataWidthInWords != 1 )
    {
        MxSignalProperties prop;

        ahb_TSlave->setDataBusWidth(dataWidthInWords);

#if (!CARBON)        
        prop = *(HWDATA_SMaster->getProperties());
        prop.bitwidth = dataWidthInWords * 32;
        HWDATA_SMaster->setProperties(&prop);
#endif
        prop = *(fromRTL[HRDATA_SS]->getProperties());
        prop.bitwidth = dataWidthInWords * 32;
        fromRTL[HRDATA_SS]->setProperties(&prop);
    }

    if ( addrWidthInWords != 1 )
    {
        MxSignalProperties prop;

        ahb_TSlave->setAddrBusWidth(addrWidthInWords);

        prop = *(HADDR_SMaster->getProperties());
        prop.bitwidth = addrWidthInWords * 32;
        HADDR_SMaster->setProperties(&prop);
    }

#if (!CARBON)
    p_mxdi = new AHBT2S_MxDI(this);

    if (p_mxdi == NULL)
    {
        message(MX_MSG_FATAL_ERROR,"%s::init(): Failed to allocate memory for MxDI interface. Aborting Simulation!", getInstanceID().c_str());
    }

    // Call the base class after this has been initialized.
    sc_mx_module::init();
#endif
    p_initComplete = true;
}

void
AHBT2S::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  (void)level; (void)filelist;

    memset(latchedFromRTL, 0, sizeof(MxU32) * AHBT2S_SS_END);
    memset( hwdata, 0, sizeof(MxU32) * MAX_DATA_WIDTH_IN_WORDS );
    haddr = 0;

    if (addrWidthInWords > 1)
        HADDR_SMaster->driveSignal(0, (MxU32*)&haddr);
    else
        HADDR_SMaster    -> driveSignal(0, NULL);
    HWDATA_SMaster   ->driveSignal(hwdata[0], &hwdata[1]);
    HWRITE_SMaster   -> driveSignal(AHB_READ, NULL);
    HTRANS_SMaster   -> driveSignal(AHB_IDLE, NULL);
    HBURST_SMaster   -> driveSignal(AHB_SINGLE, NULL);
    HSIZE_SMaster    -> driveSignal(0, NULL);
    HPROT_SMaster    -> driveSignal(0, NULL);
    HMASTLOCK_SMaster ->driveSignal(0, NULL);
    HBSTRB_SMaster    -> driveSignal(0, NULL);
    HUNALIGN_SMaster -> driveSignal(0, NULL);
    switch ( m_xtorType )
    {
    case XTOR_TYPE_AHBT2S:
    case XTOR_TYPE_AHBS_T2S:
        HMASTER_SMaster   ->driveSignal(0, NULL);
        HDOMAIN_SMaster   ->driveSignal(0, NULL);
    case XTOR_TYPE_AHBLITES_T2S:
        HREADY_SMaster   -> driveSignal(AHB_READY, NULL);
        HSEL_SMaster     -> driveSignal(0, NULL);
        break;
    }

    p_slaveSelected = false;
    mSlaveSelectedPrev = false;
    mDriveHreadyIn = true;

    // Check that protocol ID & data width match those of AHB Bus
    if (!MxPortCheck::check((ahb_TS*)ahb_TSlave))
    {
        message(MX_MSG_WARNING, "Incompatible ports connected.");
    }

#if (!CARBON)
    // Call the base class after this has been reset.
    sc_mx_module::reset(level, filelist);
#endif
}



void
AHBT2S::terminate()
{
#if (!CARBON)
    // Call the base class first.
    sc_mx_module::terminate();
#endif

    if (p_mxdi != NULL)
    {
        // Release the MXDI Interface
        delete p_mxdi;
        p_mxdi = NULL;
    }
}


void
AHBT2S::setParameter( const string &name, const string &value )
{
    MxConvertErrorCodes status = MxConvert_SUCCESS;

    if (name == "Enable Debug Messages")
    {
        status = MxConvertStringToValue(value, &p_enableDbgMsg);
    }
    else if (name == "Base Address")
    {
        if (p_initComplete == false)
        {
            status = MxConvertStringToValue(value, &p_Base);
            p_start[0] = p_Base;
        }
        else
        {
            message(MX_MSG_WARNING, "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.",
                    getInstanceID().c_str(), name.c_str() );
        }
    }
    else if (name == "Size")
    {
        if (p_initComplete == false)
        {
            status = MxConvertStringToValue(value, &p_Size);
            p_size[0] = p_Size;
        }
        else
        {
            message(MX_MSG_WARNING, "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.",
                    getInstanceID().c_str(), name.c_str() );
        }
    }
    else if (name == "Data Bus Width")
    {
        if (p_initComplete == false)
        {
            MxU32 tmp = 0;
            status = MxConvertStringToValue(value, &tmp);

            if (status == MxConvert_SUCCESS)
            {
                switch ( tmp )
                {
                case 32:  dataWidthInWords = 1; break;
                case 64:  dataWidthInWords = 2; break;
		        case 128: dataWidthInWords = 4; break;
                default:
                    if (tmp < 32)
                    {
                        message( MX_MSG_WARNING, "%s::setParameter(): Rounding value %s for parameter: %s to 32.", getInstanceID().c_str(), value.c_str(), name.c_str() );
                        dataWidthInWords = 1;
                    }
                    else if (tmp < 64)
                    {
                        message( MX_MSG_WARNING, "%s::setParameter(): Rounding value %s for parameter: %s to 64.", getInstanceID().c_str(), value.c_str(), name.c_str() );
                        dataWidthInWords = 2;
                    }
                    else
                    {
                        message( MX_MSG_WARNING, "%s::setParameter(): Rounding value %s for parameter: %s to 128.", getInstanceID().c_str(), value.c_str(), name.c_str() );
                        dataWidthInWords = 4;
                    }
                    break;
                }
            }
        }
        else
        {
            message(MX_MSG_WARNING,
                    "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.",
                    getInstanceID().c_str(), name.c_str() );
        }
    }
    else if (name == "Address Bus Width")
    {
        if (p_initComplete == false)
        {
            MxU32 tmp = 0;
            status = MxConvertStringToValue(value, &tmp);

            if (status == MxConvert_SUCCESS)
            {
                if (tmp > 64)
                {
                    message( MX_MSG_WARNING, "%s::setParameter(): Cannot set parameter <%s> higher than 64. Using 64 instead.", getInstanceID().c_str(), name.c_str() );
                    addrWidthInWords = 2;
                }
                else
                    addrWidthInWords = 1;
            }
        }
        else
        {
            message(MX_MSG_WARNING,
                    "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.",
                    getInstanceID().c_str(), name.c_str() );
        }
    }
    else if (name == "Subtract Base Address")
    {
        status = MxConvertStringToValue(value, &p_subtractBaseAddr);
    }
    else if (name == "Subtract Base Address Dbg")
    {
        status = MxConvertStringToValue(value, &p_subtractBaseAddrDbg);
    }
    else if (name == "Big Endian")
    {
        status = MxConvertStringToValue(value, &bigEndian);
    }
    else if (string::npos != name.find("ahb_start"))
    {
        if ( p_initComplete == false )
        {   
            MxU32 nr;
            if (sscanf(name.c_str(), "ahb_start%d", &nr))
                status = MxConvertStringToValue( value, &p_start[nr] );
            else
                status = MxConvert_ILLEGAL_VALUE;
        }
        else
        {
            message( MX_MSG_WARNING, "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.", getInstanceID().c_str(), name.c_str() );
            return;
        }
    }
    else if (string::npos != name.find("ahb_size"))
    {
        if ( p_initComplete == false )
        {
            MxU32 nr;
            if (sscanf(name.c_str(), "ahb_size%d", &nr))
                status = MxConvertStringToValue( value, &p_size[nr] );
            else
                status = MxConvert_ILLEGAL_VALUE;
        }
        else
        {
            message( MX_MSG_WARNING, "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.", getInstanceID().c_str(), name.c_str() );
            return;
        }
    }
    else if (name == "Align Data")
    {
        status = MxConvertStringToValue(value, &p_alignData);
    }
    else if (name == "Filter HREADYIN")
    {
        status = MxConvertStringToValue(value, &p_filterHreadyIn);
    }



    if ( status == MxConvert_SUCCESS )
    {
#if (!CARBON)
        sc_mx_module::setParameter(name, value);
#endif
    }
    else
    {
        message( MX_MSG_WARNING, "%s::setParameter(): Illegal value <%s> "
                 "passed for parameter <%s>. Assignment ignored.", getInstanceID().c_str(), value.c_str(), name.c_str() );
    }
}

#if (!CARBON)
string
AHBT2S::getProperty( MxPropertyType property )
{
    string description;
    switch ( property )
    {
    case MX_PROP_IP_PROVIDER:
        return "ARM";
    case MX_PROP_LOADFILE_EXTENSION:
        return "";
    case MX_PROP_COMPONENT_TYPE:
        return MxComponentTypes[MX_TYPE_TRANSACTOR];
    case MX_PROP_COMPONENT_VERSION:
        switch ( m_xtorType )
        {
        case XTOR_TYPE_AHBT2S:
        case XTOR_TYPE_AHBS_T2S:
            return "2.1.006";
        case XTOR_TYPE_AHBLITES_T2S:
            return "2.1.004";
        }
    case MX_PROP_MSG_PREPEND_NAME:
        return "yes";
    case MX_PROP_DESCRIPTION:
        switch ( m_xtorType )
        {
        case XTOR_TYPE_AHBT2S:
        case XTOR_TYPE_AHBS_T2S:
            description = "AHB MaxSim Transaction to HDL Slave Signals Converter";
            break;
        case XTOR_TYPE_AHBLITES_T2S:
            description = "AHB-Lite MaxSim Transaction to HDL Slave Signals Converter";
            break;
        }
        return description + " Compiled on " + __DATE__ + ", " + __TIME__;
    case MX_PROP_MXDI_SUPPORT:
        return "yes";
    case MX_PROP_SAVE_RESTORE:
        return "no";
    default:
        return "";
    }
    return "";
}

MXDI*
AHBT2S::getMxDI()
{
    return p_mxdi;
}

string
AHBT2S::getName(void)
{
    switch ( m_xtorType )
    {
    case XTOR_TYPE_AHBT2S:
        return "AHBT2S";
    case XTOR_TYPE_AHBS_T2S:
        return "AHBS_T2S";
    case XTOR_TYPE_AHBLITES_T2S:
        return "AHBLiteS_T2S";
    }
    return "";
}

/************************
 * AHBS_T2S Factory class
 ***********************/

class AHBS_T2SFactory : public MxFactory
{
public:
    AHBS_T2SFactory() : MxFactory ( "AHBS_T2S" ) {}
    sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
    {
        return new AHBT2S(c, id, XTOR_TYPE_AHBS_T2S);
    }
};

/************************
 * AHBT2S Factory class
 ***********************/

class AHBT2SFactory : public MxFactory
{
public:
    AHBT2SFactory() : MxFactory ( "AHBT2S" ) {}
    sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
    {
        return new AHBT2S(c, id, XTOR_TYPE_AHBT2S);
    }
};

/************************
 * AHBLiteS_T2S Factory class
 ***********************/

class AHBLiteS_T2SFactory : public MxFactory
{
public:
    AHBLiteS_T2SFactory() : MxFactory ( "AHBLiteS_T2S" ) {}
    sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
    {
        return new AHBT2S(c, id, XTOR_TYPE_AHBLITES_T2S);
    }
};

/**
 *  Entry point into the memory components (from MaxSim)
 */
extern "C" void
MxInit(void)
{
    new AHBS_T2SFactory();
    new AHBT2SFactory();    
    new AHBLiteS_T2SFactory();
}

#endif
