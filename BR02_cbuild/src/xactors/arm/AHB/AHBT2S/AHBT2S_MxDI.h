// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#ifndef AHBT2S_MxDI_H
#define AHBT2S_MxDI_H

#include "maxsim.h"

class AHBT2S;

class AHBT2S_MxDI : public MxDIBase
{
 public:
    AHBT2S_MxDI(AHBT2S* c);
    virtual ~AHBT2S_MxDI();

 public:

    // Register access functions
    MxdiReturn_t	MxdiRegGetGroups(   MxU32 groupIndex
                                        , MxU32 desiredNumOfRegGroups
                                        , MxU32* actualNumOfRegGroups
                                        , MxdiRegGroup_t* reg );

    MxdiReturn_t	MxdiRegGetMap(      MxU32 groupID
                                        , MxU32 regIndex
                                        , MxU32 registerSlots
                                        , MxU32* registerCount
                                        , MxdiRegInfo_t* reg );

	MxdiReturn_t 	MxdiRegGetCompound(	MxU32 reg,
										MxU32 componentIndex, 
										MxU32 desiredNumOfComponents, 
										MxU32 *actualNumOfcomponents, 
										MxU32 *components );

    MxdiReturn_t	MxdiRegWrite(       MxU32 regCount
                                        , MxdiReg_t* reg
                                        , MxU32* numRegsWritten
                                        , MxU8 doSideEffects );

    MxdiReturn_t	MxdiRegRead(        MxU32 regCount
                                        , MxdiReg_t* reg
                                        , MxU32* numRegsRead
                                        , MxU8 doSideEffects );

 private:
    AHBT2S*		target;

    // Register related info
    MxdiRegInfo_t*		regInfo;
    MxdiRegGroup_t*		regGroup;
	MxU32*				groupStartIndex;
	MxU32 groupCount;
	MxU32 totalRegisterCount;
};

#endif

