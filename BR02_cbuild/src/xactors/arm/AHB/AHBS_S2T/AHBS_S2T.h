// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#ifndef AHBS_S2T_H
#define AHBS_S2T_H

#include "maxsim.h"

#include "MxSaveRestore.h"
#include "MxAHBMasterPort.h"

#if CARBON
#include "CarbonAdaptorBase.h"
#include "carbon_ahb.h"
#include "util/UtCLicense.h"
#endif

typedef enum
{
    HSEL_SS = 0,
    HADDR_SS_31_0,
    HADDR_SS_63_32,
    HBURST_SS,
    HPROT_SS,
    HSIZE_SS,
    HTRANS_SS,
    HWDATA_SS_31_0,
    HWDATA_SS_63_32,
    HWDATA_SS_95_64,
    HWDATA_SS_127_96,
    HWRITE_SS,
    HMASTLOCK_SS,
    HMASTER_SS,
    HREADY_SS,
    HBSTRB_SS,
    HUNALIGN_SS,
    HDOMAIN_SS,
    AHBS_S2T_SS_END
} ahbRtlSSenum;

typedef enum
{
    XTOR_TYPE_AHBS_S2T = 0,
    XTOR_TYPE_AHBLITES_S2T
} ahbXtorTypeEnum;

typedef struct TDataStage
{
    MxU64 addr;
    MxU32 ctrl[AHB_IDX_END];
    bool  write;
    bool  valid;
} TDataStage;


#define HWDATA_SS HWDATA_SS_31_0
#define HADDR_SS HADDR_SS_31_0
#define MAX_NUM_TRANSFERS 16 

// supporting up to 128bit data bus
#define MAX_DATA_WIDTH_IN_WORDS 4


#if (!CARBON)
class AHBS_S2T_MxDI;
#endif

#if CARBON
class AHBS_S2T : public CarbonAdaptorBase
#else
class AHBS_S2T : public sc_mx_module, public MxSaveRestore
#endif
{
    friend class AHBS_S2T_RTL_SS;
#if (!CARBON)
    friend class AHBS_S2T_MxDI;
#endif

public:
    AHBS_S2T_RTL_SS* fromRTL[AHBS_S2T_SS_END];
    
    MxU32 latchedFromRTL[AHBS_S2T_SS_END];

    TAHBSignals signals;

    RTL_PORT *HRDATA_SMaster;
    RTL_PORT *HREADYout_SMaster;
    RTL_PORT *HRESP_SMaster;

    // Master port instance declaration
    sc_port<sc_mx_transaction_if> *AHB_TMaster;
    
    // constructor / destructor
#if CARBON
    AHBS_S2T(sc_mx_module* carbonComp, const char *xtorName,
             CarbonObjectID **carbonObj, CarbonPortFactory *portFactory,
             int xtorType);
#else
    AHBS_S2T(sc_mx_m_base* c, const string &s, int xtorType);
#endif
    virtual ~AHBS_S2T();

    // overloaded methods for clocked components
    void communicate();
    void update();

    // Implementation of MxSaveRestore interface methods
    virtual bool saveData( MxODataStream &data );
    virtual bool restoreData( MxIDataStream &data );

    // overloaded sc_mx_module methods
#if (!CARBON)
    string getName();
#endif
    void setParameter(const string &name, const string &value);
#if (!CARBON)
    string getProperty( MxPropertyType property );
#endif
    void setPortWidth(const char* name, uint32_t bitWidth);
    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);

#if (!CARBON)
    // The MxDI interface.
    MXDI* getMxDI();
#endif

#if CARBON
    MxStatus readDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
    MxStatus writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
#endif

private:

    void setupWriteData();
    
    // Component Parameters
    bool   m_enableDebugMessages;
    MxU32  m_dataWidthInWords;
    MxU32  m_addrWidthInWords;
    MxU32* m_rdata;
    MxU32* m_wdata;
    bool   m_bigEndian;
    int    m_xtorType;
 
    // The MxDI interface.
    MXDI* m_mxdi;

    // Flag to indicate init() complete    
    bool m_initComplete;
    
    TDataStage m_dataStage;    
   
};

#endif
