#include "CarbonAhbSlaveS2T.h"

#define CARBON 1
#include "AHBS_S2T.h"
#include "AHBS_S2T_RTL_SS.h"

CarbonAhbSlaveS2T::CarbonAhbSlaveS2T(sc_mx_module* carbonComp,
                                     const char *xtorName,
                                     CarbonObjectID **carbonObj,
                                     CarbonPortFactory *portFactory,
                                     bool isAhbLite)
{
  int xtorType = isAhbLite ? XTOR_TYPE_AHBLITES_S2T : XTOR_TYPE_AHBS_S2T;
  mArmAdaptor = new AHBS_S2T(carbonComp, xtorName, carbonObj, portFactory,
                             xtorType);
}

sc_mx_signal_slave *CarbonAhbSlaveS2T::carbonGetPort(const char *name)
{
  sc_mx_signal_slave *port;

  if (strcmp("haddr", name) == 0) {
    port = mArmAdaptor->fromRTL[HADDR_SS];

  } else if (strcmp("hburst", name) == 0) {
    port = mArmAdaptor->fromRTL[HBURST_SS];

  } else if (strcmp("hprot", name) == 0) {
    port = mArmAdaptor->fromRTL[HPROT_SS];

  } else if (strcmp("hsize", name) == 0) {
    port = mArmAdaptor->fromRTL[HSIZE_SS];

  } else if (strcmp("htrans", name) == 0) {
    port = mArmAdaptor->fromRTL[HTRANS_SS];

  } else if (strcmp("hwdata", name) == 0) {
    port = mArmAdaptor->fromRTL[HWDATA_SS];

  } else if (strcmp("hwrite", name) == 0) {
    port = mArmAdaptor->fromRTL[HWRITE_SS];

  } else if (strcmp("hmastlock", name) == 0) {
    port = mArmAdaptor->fromRTL[HMASTLOCK_SS];

  } else if (strcmp("hmaster", name) == 0) {
    port = mArmAdaptor->fromRTL[HMASTER_SS];

  } else if (strcmp("hready", name) == 0) {
    port = mArmAdaptor->fromRTL[HREADY_SS];

  } else if (strcmp("hsel", name) == 0) {
    port = mArmAdaptor->fromRTL[HSEL_SS];
  } else if (strcmp("hbstrb", name) == 0) {
    port = mArmAdaptor->fromRTL[HBSTRB_SS];
  } else if (strcmp("hunalign", name) == 0) {
    port = mArmAdaptor->fromRTL[HUNALIGN_SS];
  } else if (strcmp("hdomain", name) == 0) {
    port = mArmAdaptor->fromRTL[HDOMAIN_SS];

  } else {
    port = NULL;
    mArmAdaptor->message(MX_MSG_FATAL_ERROR, "Carbon AHB Slave S2T has no port named \"%s\"", name);
  }
  return port;
}

CarbonAhbSlaveS2T::~CarbonAhbSlaveS2T()
{
  delete mArmAdaptor;
}

void CarbonAhbSlaveS2T::communicate()
{
  mArmAdaptor->communicate();
}

void CarbonAhbSlaveS2T::update()
{
  mArmAdaptor->update();
}

void CarbonAhbSlaveS2T::init()
{
  mArmAdaptor->init();
}

void CarbonAhbSlaveS2T::terminate()
{
  mArmAdaptor->terminate();
}

void CarbonAhbSlaveS2T::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  mArmAdaptor->reset(level, filelist);
}

void CarbonAhbSlaveS2T::setParameter(const string &name, const string &value)
{
  mArmAdaptor->setParameter(name, value);
}
//! Number of master memory regions
int CarbonAhbSlaveS2T::getNumRegions()
{
  return mArmAdaptor->getNumMasterRegions();
}

//! Master Address regions
void CarbonAhbSlaveS2T::getAddressRegions(uint64_t* start, uint64_t* size, string* name)
{
  mArmAdaptor->getMasterAddressRegions(start, size, name);
}

MxStatus CarbonAhbSlaveS2T::readDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return mArmAdaptor->readDbg(addr, value, ctrl);
}

MxStatus CarbonAhbSlaveS2T::writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return mArmAdaptor->writeDbg(addr, value, ctrl);
}

CarbonDebugAccessStatus CarbonAhbSlaveS2T::debugAccess(CarbonDebugAccessDirection dir,
                                                       uint64_t startAddress, 
                                                       uint32_t numBytes, 
                                                       uint8_t* data, 
                                                       uint32_t* numBytesProcessed,
                                                       uint32_t*)
{
  // We're performing only byte accesses for simplicity
  uint32_t ctrl[AHB_IDX_END];
  ctrl[AHB_IDX_TYPE]  = AHB_TYPE_BYTE;
  ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_DATA;
  
  *numBytesProcessed = 0;
  for(MxU32 i = 0; i < numBytes; ++i) {
    eslapi::CASIStatus status = eslapi::CASI_STATUS_OK;
    if (dir == eCarbonDebugAccessWrite) {
      uint32_t value = 0;
      memset(&value, data[i], 4);
      status = mArmAdaptor->writeDbg(startAddress+i, &value, ctrl);
    }
    else {
      uint32_t value = 0;
      status = mArmAdaptor->readDbg(startAddress+i, &value, ctrl);
      data[i] = value & 0xFF;
    }
    
    // Check return status
    switch (status) {
    case eslapi::CASI_STATUS_ERROR:
    case eslapi::CASI_STATUS_NOTSUPPORTED:
      return eCarbonDebugAccessStatusError;
    case eslapi::CASI_STATUS_NOMEMORY:
      if (i == 0) return eCarbonDebugAccessStatusPartial;
      else return eCarbonDebugAccessStatusPartial;
    default:
      // Update how many bytes we have written so far
      *numBytesProcessed = i+1;
    }
  }

  // If we made it all the way here the entire transaction was successful
  return eCarbonDebugAccessStatusOk;
}

void CarbonAhbSlaveS2T::carbonSetPortWidth(const char* name, uint32_t bitWidth)
{
  mArmAdaptor->setPortWidth(name, bitWidth);
}
