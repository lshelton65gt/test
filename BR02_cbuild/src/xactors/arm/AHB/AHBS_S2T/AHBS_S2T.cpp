// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#include <stdio.h>

#include "xactors/arm/include/carbon_arm_adaptor.h"
#include "CarbonReplicateData.h"

#include "AHBS_S2T.h"
#include "AHBS_S2T_RTL_SS.h"
#if MAXSIM_MAJOR_VERSION >= 7
#include "AHB_Transaction_Ext.h"
#endif

#if (!CARBON)
#include "AHBS_S2T_MxDI.h"
#include "lc.h"
#endif

#ifdef CARBON
#include <stdarg.h>
#define AHBS_S2T_RTL_SM(NAME) portFactory(mCarbonComponent, xtorName, NAME, mpCarbonObject)
#else
#define AHBS_S2T_RTL_SM(NAME) new sc_port<sc_mx_signal_if>(this, NAME)
#endif


#if CARBON
AHBS_S2T::AHBS_S2T(sc_mx_module* carbonComp, const char *xtorName,
                   CarbonObjectID **carbonObj, CarbonPortFactory *portFactory,
                   int xtorType)
  : CarbonAdaptorBase(carbonComp, xtorName, carbonObj)
#else
AHBS_S2T::AHBS_S2T(sc_mx_m_base* c, const string &s, int xtorType) : sc_mx_module(c, s)
#endif
{
    m_xtorType = xtorType;

    for ( int i = 0; i < AHBS_S2T_SS_END; i++ )
        fromRTL[i] = NULL;

    // AHB Master transaction port
    MxTransactionProperties prop;
    AHB_INIT_TRANSACTION_PROPERTIES(prop);

#if CARBON
    AHB_TMaster = new sc_port<sc_mx_transaction_if>(carbonComp, "ahb_out" );
#else
    AHB_TMaster = new sc_port<sc_mx_transaction_if>(this, "ahb_out" );   
#endif
    AHB_TMaster->setProperties(&prop);    
    registerPort( AHB_TMaster, "ahb_out" );

    maxsim::MxSignalProperties *signalprop = new maxsim::MxSignalProperties;
    signalprop->isOptional = false;

    // signals from RTL
    
    // HADDR bits [11:2] into DMAC AHB Slave Interface
    fromRTL[HADDR_SS] = new AHBS_S2T_RTL_SS( this, HADDR_SS );
    signalprop->bitwidth = 32;
    fromRTL[HADDR_SS]->setProperties(signalprop);
    registerPort( fromRTL[HADDR_SS], "haddr" );

    fromRTL[HBURST_SS] = new AHBS_S2T_RTL_SS( this, HBURST_SS );
    signalprop->bitwidth = 3;
    fromRTL[HBURST_SS]->setProperties(signalprop);
    registerPort( fromRTL[HBURST_SS], "hburst" );

    fromRTL[HPROT_SS] = new AHBS_S2T_RTL_SS( this, HPROT_SS );
    signalprop->bitwidth = 6; // hprot[4] = ALLOC, hprot[5] = EXREQ
    fromRTL[HPROT_SS]->setProperties(signalprop);
    registerPort( fromRTL[HPROT_SS], "hprot" );

    fromRTL[HSIZE_SS] = new AHBS_S2T_RTL_SS( this, HSIZE_SS );
    signalprop->bitwidth = 3;
    fromRTL[HSIZE_SS]->setProperties(signalprop);
    registerPort( fromRTL[HSIZE_SS], "hsize" );

    // HTRANS only 1 bit into DMAC AHB Slave Interface
    fromRTL[HTRANS_SS] = new AHBS_S2T_RTL_SS( this, HTRANS_SS );
    signalprop->bitwidth = 2;
    fromRTL[HTRANS_SS]->setProperties(signalprop);
    registerPort( fromRTL[HTRANS_SS], "htrans" );

    fromRTL[HWDATA_SS] = new AHBS_S2T_RTL_SS( this, HWDATA_SS );
    signalprop->bitwidth = 32;
    fromRTL[HWDATA_SS]->setProperties(signalprop);
    registerPort( fromRTL[HWDATA_SS], "hwdata" );

    fromRTL[HWRITE_SS] = new AHBS_S2T_RTL_SS( this, HWRITE_SS );
    signalprop->bitwidth = 1;
    fromRTL[HWRITE_SS]->setProperties(signalprop);
    registerPort( fromRTL[HWRITE_SS], "hwrite" );

    fromRTL[HMASTLOCK_SS] = new AHBS_S2T_RTL_SS( this, HMASTLOCK_SS );
    signalprop->bitwidth = 1;
    fromRTL[HMASTLOCK_SS]->setProperties(signalprop);
    if (m_xtorType == XTOR_TYPE_AHBS_S2T)
        registerPort( fromRTL[HMASTLOCK_SS], "hmastlock" );
    else
        registerPort( fromRTL[HMASTLOCK_SS], "hlock" );

    fromRTL[HREADY_SS] = new AHBS_S2T_RTL_SS( this, HREADY_SS );
    signalprop->bitwidth = 1;
    fromRTL[HREADY_SS]->setProperties(signalprop);
    registerPort( fromRTL[HREADY_SS], "hready" );

    fromRTL[HSEL_SS] = new AHBS_S2T_RTL_SS( this, HSEL_SS );
    signalprop->bitwidth = 1;
    fromRTL[HSEL_SS]->setProperties(signalprop);
    registerPort( fromRTL[HSEL_SS], "hsel" );

    fromRTL[HBSTRB_SS] = new AHBS_S2T_RTL_SS( this, HBSTRB_SS );
    signalprop->bitwidth = 16; // supports up to 128 bit data width
    fromRTL[HBSTRB_SS]->setProperties(signalprop);
    registerPort( fromRTL[HBSTRB_SS], "hstrb" );

    fromRTL[HUNALIGN_SS] = new AHBS_S2T_RTL_SS( this, HUNALIGN_SS );
    signalprop->bitwidth = 1;
    fromRTL[HUNALIGN_SS]->setProperties(signalprop);
    registerPort( fromRTL[HUNALIGN_SS], "hunalign" );

    if (m_xtorType == XTOR_TYPE_AHBS_S2T)
    {
        fromRTL[HMASTER_SS] = new AHBS_S2T_RTL_SS( this, HMASTER_SS );
        signalprop->bitwidth = 4;
        fromRTL[HMASTER_SS]->setProperties(signalprop);
        registerPort( fromRTL[HMASTER_SS], "hmaster" );

        fromRTL[HDOMAIN_SS] = new AHBS_S2T_RTL_SS( this, HDOMAIN_SS );
        signalprop->bitwidth = 4;
        fromRTL[HDOMAIN_SS]->setProperties(signalprop);
        registerPort( fromRTL[HDOMAIN_SS], "hdomain" );
    }
    else
    {
        fromRTL[HMASTER_SS] = NULL;
        fromRTL[HDOMAIN_SS] = NULL;
    }
    
    // signals to RTL
    HRDATA_SMaster = AHBS_S2T_RTL_SM( "HRDATA" );
    signalprop->bitwidth = 32;
    HRDATA_SMaster->setProperties(signalprop);
    registerPort( HRDATA_SMaster, "hrdata" );

    HREADYout_SMaster = AHBS_S2T_RTL_SM( "HREADYOUT" );
    signalprop->bitwidth = 1;
    HREADYout_SMaster->setProperties(signalprop);
    registerPort( HREADYout_SMaster, "hreadyout" );

    HRESP_SMaster = AHBS_S2T_RTL_SM( "HRESP" );
    signalprop->bitwidth = 3; // hresp[2] = EXRESP
    HRESP_SMaster->setProperties(signalprop);
    registerPort( HRESP_SMaster, "hresp" );

#if (!CARBON)
    SC_MX_CLOCKED();
    registerPort( dynamic_cast< sc_mx_clock_slave_p_base* >( this ), "clk-in");
#endif

    m_initComplete = false;

#if (!CARBON)
    defineParameter("Enable Debug Messages", "false", MX_PARAM_BOOL, true);
    defineParameter("Data Bus Width", "32", MX_PARAM_VALUE, false);
    defineParameter("Address Bus Width", "32", MX_PARAM_VALUE, false);
    defineParameter("Big Endian", "false", MX_PARAM_BOOL, false);
#endif

    m_mxdi = NULL;  
}

AHBS_S2T::~AHBS_S2T()
{
    delete AHB_TMaster;
    for ( MxU32 i = 0; i < AHBS_S2T_SS_END; i++ )
    {
        if ( fromRTL[i] )
        {
            delete fromRTL[i];
            fromRTL[i] = NULL;
        }
    }
    delete HRDATA_SMaster;
    delete HREADYout_SMaster;
    delete HRESP_SMaster;
}

#if CARBON
MxStatus AHBS_S2T::readDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return AHB_TMaster->readDbg(addr, value, ctrl);
}

MxStatus AHBS_S2T::writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return AHB_TMaster->writeDbg(addr, value, ctrl);
}

#endif


void
AHBS_S2T::communicate()
{
    // -------------------------------------------------------------------------
    //  Update global AHB signals structure
    // -------------------------------------------------------------------------
    signals.hmaster   = latchedFromRTL[HMASTER_SS];
    signals.hready    = latchedFromRTL[HREADY_SS];
    signals.hmastlock = latchedFromRTL[HMASTLOCK_SS];

    // -------------------------------------------------------------------------
    //  Default state of HRESP & HREADY signals when not in data phase
    // -------------------------------------------------------------------------
    if (!m_dataStage.valid)
    {
        //HRESP_SMaster->driveSignal(AHB_OK, NULL);
        HRESP_SMaster->driveSignal(AHB_ACK_DONE, NULL);
        HREADYout_SMaster->driveSignal(1, NULL);
    }

    // -------------------------------------------------------------------------
    //  Handle data phase
    // -------------------------------------------------------------------------
    
    if (m_dataStage.valid)
    {
        MxStatus ret;
            
        // send transaction
        if (m_dataStage.write)
        {
            setupWriteData();
            ret = AHB_TMaster->write(m_dataStage.addr, m_wdata, m_dataStage.ctrl);
        }
        else
        {
            ret = AHB_TMaster->read(m_dataStage.addr, m_rdata, m_dataStage.ctrl);
        }
        
        switch (ret)
        {
            case MX_STATUS_OK:
                HREADYout_SMaster->driveSignal(1, NULL);
                signals.hready    = 1;
                  
                if (!m_dataStage.write)
                {
                    // Fix for bug 6067. Align (also replicate) read data.
                    CarbonReplicateData( m_dataStage.ctrl, m_dataWidthInWords, &m_rdata[0] );
                  

                    HRDATA_SMaster->driveSignal(m_rdata[0],
                              ( m_dataWidthInWords > 1 ) ? &m_rdata[1] : NULL );
                }
                
                m_dataStage.valid = false;                  
                break;

            case MX_STATUS_WAIT:
                HREADYout_SMaster->driveSignal(0, NULL);  
                signals.hready    = 0;
                break;
                
            case MX_STATUS_ERROR:
                HREADYout_SMaster->driveSignal(1, NULL);
                break;

            default:
                break;
        }
                
        // Update HRESP 
        // Determine the response
        switch(m_dataStage.ctrl[AHB_IDX_ACK])
        {
            case AHB_ACK_DONE:      
                //HRESP_SMaster->driveSignal(AHB_OK, NULL);
#if MAXSIM_MAJOR_VERSION >= 7
                HRESP_SMaster->driveSignal(AHB_ACK_DONE | AHB_HRESP_ENCODE_EXRESP(AHB_SIDEBAND_DECODE_EXRESP(m_dataStage.ctrl[AHB_IDX_SIDEBAND])), NULL);
#else
                HRESP_SMaster->driveSignal(AHB_ACK_DONE, NULL);
#endif
                break;
            case AHB_ACK_ABORT:     
                //HRESP_SMaster->driveSignal(AHB_ERROR, NULL);
#if MAXSIM_MAJOR_VERSION >= 7
                HRESP_SMaster->driveSignal(AHB_ACK_ABORT | AHB_HRESP_ENCODE_EXRESP(AHB_SIDEBAND_DECODE_EXRESP(m_dataStage.ctrl[AHB_IDX_SIDEBAND])), NULL);
#else
                HRESP_SMaster->driveSignal(AHB_ACK_ABORT, NULL);
#endif
                break;
            case AHB_ACK_RETRY:
                //HRESP_SMaster->driveSignal(AHB_RETRY, NULL);
#if MAXSIM_MAJOR_VERSION >= 7
                HRESP_SMaster->driveSignal(AHB_ACK_RETRY | AHB_HRESP_ENCODE_EXRESP(AHB_SIDEBAND_DECODE_EXRESP(m_dataStage.ctrl[AHB_IDX_SIDEBAND])), NULL);
#else
                HRESP_SMaster->driveSignal(AHB_ACK_RETRY, NULL);
#endif
                break;
            case AHB_ACK_SPLIT:     
                //HRESP_SMaster->driveSignal(AHB_SPLIT, NULL);
#if MAXSIM_MAJOR_VERSION >= 7
                HRESP_SMaster->driveSignal(AHB_ACK_SPLIT | AHB_HRESP_ENCODE_EXRESP(AHB_SIDEBAND_DECODE_EXRESP(m_dataStage.ctrl[AHB_IDX_SIDEBAND])), NULL);
#else
                HRESP_SMaster->driveSignal(AHB_ACK_SPLIT, NULL);
#endif
                break;
            default:
                break;
        }
    }
    
    
    // --------------------------------------------------------------------------
    //  Handle address phase
    // --------------------------------------------------------------------------

    // DMAC HTRANS input is single bit: BUSY/IDLE -> 0, NSEQ/SEQ -> 1
    if (latchedFromRTL[HSEL_SS] && latchedFromRTL[HTRANS_SS] > 1 )
    {        
        MxU32 invalidData = AHB_ADDR_INVALID; // data is not valid in addr cycle
        MxStatus ret;
        MxU64 addr;
        MxU32 ctrl[AHB_IDX_END];

        // copy address from RTL
        MxU64 addr_h = latchedFromRTL[HADDR_SS_63_32];
        addr = addr_h << 32 | latchedFromRTL[HADDR_SS_31_0];
               
        // Encode the access control info from RTL signals  
        ctrl[AHB_IDX_TYPE]   = latchedFromRTL[HSIZE_SS] + 1;
        ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_ADDR;        
        ctrl[AHB_IDX_ACC]   = AHB_ACC_ENCODE_ASTB(0) |
                              AHB_ACC_ENCODE_NCMAHB(0) |
                              AHB_ACC_ENCODE_ASB(0) |
                              AHB_ACC_ENCODE_HBURST(latchedFromRTL[HBURST_SS])|
                              AHB_ACC_ENCODE_HSIZE(latchedFromRTL[HSIZE_SS])  |
                              AHB_ACC_ENCODE_HPROT(latchedFromRTL[HPROT_SS])  |
                              AHB_ACC_ENCODE_HTRANS(latchedFromRTL[HTRANS_SS])|   
                              AHB_ACC_ENCODE_HLOCK(latchedFromRTL[HMASTLOCK_SS])|
                              AHB_ACC_ENCODE_HMASTER(latchedFromRTL[HMASTER_SS]);                      
#if MAXSIM_MAJOR_VERSION >= 7
        ctrl[AHB_IDX_SIDEBAND] = AHB_SIDEBAND_ENCODE_ALLOC(AHB_HPROT_DECODE_ALLOC(latchedFromRTL[HPROT_SS]))|
                                 AHB_SIDEBAND_ENCODE_EXREQ(AHB_HPROT_DECODE_EXREQ(latchedFromRTL[HPROT_SS]))|
                                 AHB_SIDEBAND_ENCODE_HBSTRB(latchedFromRTL[HBSTRB_SS])|
                                 AHB_SIDEBAND_ENCODE_HUNALIGN(latchedFromRTL[HUNALIGN_SS])|
                                 AHB_SIDEBAND_ENCODE_HDOMAIN(latchedFromRTL[HDOMAIN_SS]);
#endif
        
        // send transaction
        if (latchedFromRTL[HWRITE_SS])
        {
            ret = AHB_TMaster->write(addr, &invalidData, ctrl);
        }
        else
        {
            ret = AHB_TMaster->read(addr, &invalidData, ctrl);
        }
               
        // if not in waited data phase    
        if (!m_dataStage.valid)
        {
            m_dataStage.valid = true;
            m_dataStage.addr  = addr;
            m_dataStage.write = latchedFromRTL[HWRITE_SS];
            m_dataStage.ctrl[AHB_IDX_TYPE]  = ctrl[AHB_IDX_TYPE]; 
            m_dataStage.ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_DATA;
            m_dataStage.ctrl[AHB_IDX_ACC]   = ctrl[AHB_IDX_ACC];      
#if MAXSIM_MAJOR_VERSION >= 7
            m_dataStage.ctrl[AHB_IDX_SIDEBAND] = ctrl[AHB_IDX_SIDEBAND];
#endif
        }
    }    
}

void 
AHBS_S2T::setupWriteData()
{
    MxU32 index, offset, mask, tmpData, wordData;
    
    // copy write data from RTL
    memcpy( m_wdata, &latchedFromRTL[HWDATA_SS_31_0], 
            sizeof(MxU32) * m_dataWidthInWords );
                       
    // replicate the data across all byte lanes, if size < data bus width
    if ((MxU32)(1 << AHB_ACC_DECODE_HSIZE(m_dataStage.ctrl[AHB_IDX_ACC])) < 
        (m_dataWidthInWords * 4))
    {
        // determine word index
        index = (m_dataStage.addr & ((m_dataWidthInWords - 1) << 2)) >> 2;
    
        switch(m_dataStage.ctrl[AHB_IDX_TYPE])
        {
            case AHB_TYPE_BYTE:
                // determine which byte to replicate
                offset = (m_dataStage.addr & 0x03);
                if (m_bigEndian) offset ^= 3;
                offset *= 8;
                mask   = 0xFF << offset;
                tmpData = (m_wdata[index] & mask) >> offset;
                           
                // replicate across whole of data bus
                memset(m_wdata, tmpData, 
                       sizeof(MxU32) * m_dataWidthInWords);
                break;

            case AHB_TYPE_HWORD:
                // determine which halfword to replicate
                offset = (m_dataStage.addr & 0x03);
                if (m_bigEndian) offset ^= 2;
                offset *= 8;
                mask   = 0xFFFF << offset;
                tmpData = (m_wdata[index] & mask) >> offset;

                // replicate across word
                wordData = tmpData | (tmpData << 16); 
                                           
                // replicate across whole of data bus
                for (MxU32 i= 0; i < m_dataWidthInWords; i++)
                {
                    m_wdata[i] = wordData;  
                }
                break;

            case AHB_TYPE_WORD:
                // determine which word to replicate
                wordData = m_wdata[index];
                           
                // replicate across whole of data bus
                for (MxU32 i= 0; i < m_dataWidthInWords; i++)
                {
                    m_wdata[i] = wordData;  
                }
                break;

            case AHB_TYPE_DWORD:
                switch (m_dataWidthInWords)
                {
                case 1:
                    message(MX_MSG_ERROR, "%s::write(): Data is too large for bus.", getInstanceID().c_str());
                    break;
                //case 2: // bus and data widths are both 64-bit, leave it alone
                    //break;
                case 4: // bus is 128-bit
                    // replicate according to dword index                                
                    if ((m_dataStage.addr & 0x08) >> 3)
                    {
                        m_wdata[0] = m_wdata[2];    
                        m_wdata[1] = m_wdata[3];    
                    }
                    else
                    {
                        m_wdata[2] = m_wdata[0];    
                        m_wdata[3] = m_wdata[1];    
                    }
                    break;
                }
                break;
                
            case AHB_TYPE_128BIT:
                switch (m_dataWidthInWords)
                {
                case 1:
                case 2:
                    message(MX_MSG_ERROR, "%s::write(): Data is too large for bus.", getInstanceID().c_str());
                    break;
                //case 4: // bus and data widths are both 128-bit, leave it alone
                    //break;
                }
                break;
                
            default:
                message(MX_MSG_ERROR, "%s::write(): Data is too large for bus.", getInstanceID().c_str());
                break;
        }
    }
}

void 
AHBS_S2T::update()
{
    // update the signals structure
    signals.hready_z1 = signals.hready;
}

void AHBS_S2T::setPortWidth(const char* name, uint32_t bitWidth)
{
  if (strcmp(name, "hwdata") == 0 || strcmp(name, "hrdata") == 0 || (strcmp(name, "haddr") == 0)) {

    // Calculate word count based on the next power of 2 above the given bit width.
    int wordCount = nextPo2(bitWidth)>>5;

    if (strcmp(name, "haddr") == 0)
      m_addrWidthInWords = wordCount & 0x3; // Limit address to 64 bit max
    else
      m_dataWidthInWords = wordCount;
  }
}

void 
AHBS_S2T::init()
{
    if ( m_dataWidthInWords > 1 )
    {
        MxSignalProperties prop;
                
        prop = *(fromRTL[HWDATA_SS]->getProperties());
        prop.bitwidth = m_dataWidthInWords * 32;
        fromRTL[HWDATA_SS]->setProperties(&prop);

#if (!CARBON)
        prop = *(HRDATA_SMaster->getProperties());
        prop.bitwidth = m_dataWidthInWords * 32;
        HRDATA_SMaster->setProperties(&prop);
#endif

        MxTransactionProperties propT;

        propT = *(AHB_TMaster->getProperties());
        propT.dataBitwidth = m_dataWidthInWords * 32;
        AHB_TMaster->setProperties(&propT);
    }

    if ( m_addrWidthInWords > 1 )
    {
        MxSignalProperties prop;

        prop = *(fromRTL[HADDR_SS]->getProperties());
        prop.bitwidth = m_addrWidthInWords * 32;
        fromRTL[HADDR_SS]->setProperties(&prop);

        MxTransactionProperties propT;

        propT = *(AHB_TMaster->getProperties());
        propT.addressBitwidth = m_addrWidthInWords * 32;
        AHB_TMaster->setProperties(&propT);
    }

    m_rdata = new MxU32[ m_dataWidthInWords ];
    m_wdata = new MxU32[ m_dataWidthInWords ];

#if (!CARBON)
    m_mxdi = new AHBS_S2T_MxDI(this);
    
    if (m_mxdi == NULL)
    {
        message(MX_MSG_FATAL_ERROR,
                "%s::init(): Failed to allocate memory for MxDI interface. Aborting Simulation!",
                getInstanceID().c_str());
    }

    // Initialize ourself for save/restore.
    initStream( this );

    // Call the base class after this has been initialized.
    sc_mx_module::init();
#endif
    m_initComplete = true;
}

void
AHBS_S2T::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  (void)level; (void)filelist;

    MxU32 hrdata[ MAX_DATA_WIDTH_IN_WORDS ];

    memset(latchedFromRTL, 0, sizeof(MxU32) * AHBS_S2T_SS_END);
    memset(hrdata, 0, sizeof(MxU32) * MAX_DATA_WIDTH_IN_WORDS );
    
    m_dataStage.valid = false;    

    HREADYout_SMaster -> driveSignal(AHB_READY, NULL);
    //HRESP_SMaster     -> driveSignal(AHB_OK, NULL);
    HRESP_SMaster     -> driveSignal(AHB_ACK_DONE, NULL);
    HRDATA_SMaster    -> driveSignal(hrdata[0], &hrdata[1]);

#if (!CARBON)
    // Call the base class after this has been reset.
    sc_mx_module::reset(level, filelist);
#endif

//    AHB_TMaster->reset();

    signals.hready    = 1;      
    signals.hready_z1 = 1;      
    signals.hmaster   = 0;
    signals.hmastlock = 0;

    AHB_TMaster->readReq( (MxU64) 0, (MxU32*) NULL, (MxU32*) &signals, (maxsim::MxTransactionCallbackIF*) NULL );
}

void 
AHBS_S2T::terminate()
{
    if (m_mxdi != NULL)
    {
        // Release the MXDI Interface
        delete m_mxdi;
        m_mxdi = NULL;
    }

#if (!CARBON)
    // Call the base class first.
    sc_mx_module::terminate();
#endif
}

void
AHBS_S2T::setParameter( const string &name, const string &value )
{
    MxConvertErrorCodes status = MxConvert_SUCCESS;

    if (name == "Enable Debug Messages")
    {
        status = MxConvertStringToValue(value, &m_enableDebugMessages);
    }
    else if (name == "Data Bus Width")
    {
        if (m_initComplete == false)
        {
            MxU32 tmp = 0;
            status = MxConvertStringToValue(value, &tmp);
            
            if (status == MxConvert_SUCCESS)
            {
                switch ( tmp )
                {
                case 32:  m_dataWidthInWords = 1; break;
                case 64:  m_dataWidthInWords = 2; break;
		        case 128: m_dataWidthInWords = 4; break;
                default:
                    if (tmp < 32)
                    {
                        message( MX_MSG_WARNING, "%s::setParameter(): Rounding value %s for parameter: %s to 32.", getInstanceID().c_str(), value.c_str(), name.c_str() );
                        m_dataWidthInWords = 1;
                    }
                    else if (tmp < 64)
                    {
                        message( MX_MSG_WARNING, "%s::setParameter(): Rounding value %s for parameter: %s to 64.", getInstanceID().c_str(), value.c_str(), name.c_str() );
                        m_dataWidthInWords = 2;
                    }
                    else
                    {
                        message( MX_MSG_WARNING, "%s::setParameter(): Rounding value %s for parameter: %s to 128.", getInstanceID().c_str(), value.c_str(), name.c_str() );
                        m_dataWidthInWords = 4;
                    }
                    break;
                }
            }
        }
        else
        {
            message(MX_MSG_WARNING, "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.", getInstanceID().c_str(), name.c_str() );
        }
    }
    else if (name == "Address Bus Width")
    {
        if (m_initComplete == false)
        {
            MxU32 tmp = 0;
            status = MxConvertStringToValue(value, &tmp);
            
            if (status == MxConvert_SUCCESS)
            {
                if (tmp > 64)
                {
                    message( MX_MSG_WARNING, "%s::setParameter(): Cannot set parameter <%s> higher than 64. Using 64 instead.", getInstanceID().c_str(), name.c_str() );
                    m_addrWidthInWords = 2;
                }
                else
                    m_addrWidthInWords = 1;
            }
        }
        else
        {
            message(MX_MSG_WARNING, "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.", getInstanceID().c_str(), name.c_str() );
        }
    }
    else if (name == "Big Endian")
    {
        if (m_initComplete == false)
        {
            status = MxConvertStringToValue(value, &m_bigEndian);
        }
        else
        {
            message(MX_MSG_WARNING, "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.", getInstanceID().c_str(), name.c_str() );            
        }
    }

    if ( status == MxConvert_SUCCESS )
    {
#if (!CARBON)
        sc_mx_module::setParameter(name, value);
#endif
    }
    else
    {
        message( MX_MSG_WARNING, "%s::setParameter: Illegal value <%s> "
                 "passed for parameter <%s>. Assignment ignored.", getInstanceID().c_str(), value.c_str(), name.c_str() );
    }
}

#if (!CARBON)
string
AHBS_S2T::getProperty( MxPropertyType property )
{
    string description; 
    switch ( property ) 
    {    
    case MX_PROP_IP_PROVIDER:
        return "ARM";
    case MX_PROP_LOADFILE_EXTENSION:
        return "";
    case MX_PROP_REPORT_FILE_EXT:
        return "yes";
    case MX_PROP_COMPONENT_TYPE:
        return MxComponentTypes[MX_TYPE_TRANSACTOR];
    case MX_PROP_COMPONENT_VERSION:
        switch ( m_xtorType )
        {
        case XTOR_TYPE_AHBS_S2T:
        case XTOR_TYPE_AHBLITES_S2T:
            return "2.1.001";
        }
    case MX_PROP_MSG_PREPEND_NAME:
        return "yes"; 
    case MX_PROP_DESCRIPTION:
        switch ( m_xtorType )
        {
        case XTOR_TYPE_AHBS_S2T:
            description = "AHB HDL Slave Signals to MaxSim Transaction Converter";
            break;
        case XTOR_TYPE_AHBLITES_S2T:
            description = "AHB-Lite HDL Slave Signals to MaxSim Transaction Converter";
            break;
        }
        return description + " Compiled on " + __DATE__ + ", " + __TIME__; 
    case MX_PROP_MXDI_SUPPORT:
        return "yes";
    case MX_PROP_SAVE_RESTORE:
        return "yes";
    default:
        return "";
    }
    return "";
}

MXDI*
AHBS_S2T::getMxDI()
{
  return m_mxdi;
}

string
AHBS_S2T::getName(void)
{
    switch ( m_xtorType )
    {
    case XTOR_TYPE_AHBS_S2T:
        return "AHBS_S2T";
    case XTOR_TYPE_AHBLITES_S2T:
        return "AHBLiteS_S2T";
    }
    return "";
}
#endif

bool
AHBS_S2T::saveData( MxODataStream &data )
{
  (void)data;
    // TODO:  Add your save code here.
    // This shows how the example state variable is saved.
    // data << exampleStateVariable;

    // TODO:  Add verification if applicable.
    // return save was successful
    return true;
}

bool
AHBS_S2T::restoreData( MxIDataStream &data )
{
  (void)data;
    // TODO:  Add your restore code here.
    // This shows how the example state variable is restored.
    // data >> exampleStateVariable;

    // TODO:  Add verification if applicable.
    // return restore was successful
    return true;
}


#if (!CARBON)
/************************
 * AHBS_S2T Factory class
 ***********************/

class AHBS_S2TFactory : public MxFactory
{
public:
    AHBS_S2TFactory() : MxFactory ( "AHBS_S2T" ) {}
    sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
    { 
      return new AHBS_S2T(c, id, XTOR_TYPE_AHBS_S2T); 
    }
};


/************************
 * AHBLiteS_S2T Factory class
 ***********************/

class AHBLiteS_S2TFactory : public MxFactory
{
public:
    AHBLiteS_S2TFactory() : MxFactory ( "AHBLiteS_S2T" ) {}
    sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
    { 
      return new AHBS_S2T(c, id, XTOR_TYPE_AHBLITES_S2T); 
    }
};



/**
 *  Entry point into the memory components (from MaxSim)
 */
extern "C" void 
MxInit(void)
{
  new AHBS_S2TFactory();
  new AHBLiteS_S2TFactory();
}
#endif
