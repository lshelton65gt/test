#include "CarbonAhbMasterS2T.h"

#define CARBON 1
#include "carbon_ahb.h"
#include "AHBS2T.h"
#include "AHBRTL_SS.h"


CarbonAhbMasterS2T::CarbonAhbMasterS2T(sc_mx_module* carbonComp,
                                     const char *xtorName,
                                     CarbonObjectID **carbonObj,
                                     CarbonPortFactory *portFactory, 
                                     bool isAhbLite)
{
  mArmAdaptor = new AHBS2T(carbonComp, xtorName, isAhbLite ? XTOR_TYPE_AHBLITEM_S2T : XTOR_TYPE_AHBM_S2T, carbonObj, portFactory);
}

sc_mx_signal_slave *CarbonAhbMasterS2T::carbonGetPort(const char *name)
{
  sc_mx_signal_slave *port;

  if (strcmp("hbreq", name) == 0) {
    port = mArmAdaptor->fromRTL[HBREQ_SS];
  }
  else if (strcmp("haddr", name) == 0) {
    port = mArmAdaptor->fromRTL[HADDR_SS];
  }
  else if (strcmp("hburst", name) == 0) {
    port = mArmAdaptor->fromRTL[HBURST_SS];
  }
  else if (strcmp("hprot", name) == 0) {
    port = mArmAdaptor->fromRTL[HPROT_SS];
  }
  else if (strcmp("hsize", name) == 0) {
    port = mArmAdaptor->fromRTL[HSIZE_SS];
  }
  else if (strcmp("htrans", name) == 0) {
    port = mArmAdaptor->fromRTL[HTRANS_SS];
  }
  else if (strcmp("hwdata", name) == 0) {
    port = mArmAdaptor->fromRTL[HWDATA_SS];
  }
  else if (strcmp("hwrite", name) == 0) {
    port = mArmAdaptor->fromRTL[HWRITE_SS];
  }
  else if (strcmp("hlock", name) == 0) {
    port = mArmAdaptor->fromRTL[HLOCK_SS];
  } else if (strcmp("hbstrb", name) == 0) {
    port = mArmAdaptor->fromRTL[HBSTRB_SS];
  } else if (strcmp("hunalign", name) == 0) {
    port = mArmAdaptor->fromRTL[HUNALIGN_SS];
  } else if (strcmp("hdomain", name) == 0) {
    port = mArmAdaptor->fromRTL[HDOMAIN_SS];


  }
  else {
    port = NULL;
    mArmAdaptor->message(MX_MSG_FATAL_ERROR, "Carbon AHB Master S2T has no port named \"%s\"", name);
  }
  return port;
}

CarbonAhbMasterS2T::~CarbonAhbMasterS2T()
{
  delete mArmAdaptor;
}

void CarbonAhbMasterS2T::communicate()
{
  mArmAdaptor->communicate();
}

void CarbonAhbMasterS2T::update()
{
  mArmAdaptor->update();
}

void CarbonAhbMasterS2T::init()
{
  mArmAdaptor->init();
}

void CarbonAhbMasterS2T::terminate()
{
  mArmAdaptor->terminate();
}

void CarbonAhbMasterS2T::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  mArmAdaptor->reset(level, filelist);
}

void CarbonAhbMasterS2T::setParameter(const string &name, const string &value)
{
  mArmAdaptor->setParameter(name, value);
}

//! Number of master memory regions
int CarbonAhbMasterS2T::getNumRegions()
{
  return mArmAdaptor->getNumMasterRegions();
}

//! Master Address regions
void CarbonAhbMasterS2T::getAddressRegions(uint64_t* start, uint64_t* size, string* name)
{
  mArmAdaptor->getMasterAddressRegions(start, size, name);
}


MxStatus CarbonAhbMasterS2T::readDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return mArmAdaptor->readDbg(addr, value, ctrl);
}

MxStatus CarbonAhbMasterS2T::writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return mArmAdaptor->writeDbg(addr, value, ctrl);
}

CarbonDebugAccessStatus CarbonAhbMasterS2T::debugAccess(CarbonDebugAccessDirection dir,
                                                        uint64_t startAddress, 
                                                        uint32_t numBytes, 
                                                        uint8_t* data, 
                                                        uint32_t* numBytesProcessed,
                                                        uint32_t*)
{
  // We're performing only byte accesses for simplicity
  uint32_t ctrl[AHB_IDX_END];
  ctrl[AHB_IDX_TYPE]  = AHB_TYPE_BYTE;
  ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_DATA;
  
  *numBytesProcessed = 0;
  for(MxU32 i = 0; i < numBytes; ++i) {
    eslapi::CASIStatus status = eslapi::CASI_STATUS_OK;
    if (dir == eCarbonDebugAccessWrite) {
      uint32_t value = 0;
      memset(&value, data[i], 4);
      status = mArmAdaptor->writeDbg(startAddress+i, &value, ctrl);
    }
    else {
      uint32_t value = 0;
      status = mArmAdaptor->readDbg(startAddress+i, &value, ctrl);
      data[i] = value & 0xFF;
    }
    
    // Check return status
    switch (status) {
    case eslapi::CASI_STATUS_ERROR:
    case eslapi::CASI_STATUS_NOTSUPPORTED:
      return eCarbonDebugAccessStatusError;
    case eslapi::CASI_STATUS_NOMEMORY:
      if (i == 0) return eCarbonDebugAccessStatusOutRange;
      else return eCarbonDebugAccessStatusPartial;
    default:
      // Update how many bytes we have written so far
      *numBytesProcessed = i+1;
    }
  }

  // If we made it all the way here the entire transaction was successful
  return eCarbonDebugAccessStatusOk;
}

MxStatus CarbonAhbMasterS2T::debugMemWrite(MxU64 startAddress, MxU32 unitsToWrite, MxU32 unitSizeInBytes, const MxU8 *data, MxU32 *actualNumOfUnitsWritten, MxU32 secureFlag) 
{
  return mArmAdaptor->debugMemWrite(startAddress, unitsToWrite, unitSizeInBytes, data, actualNumOfUnitsWritten, secureFlag);
}
  
MxStatus CarbonAhbMasterS2T::debugMemRead(MxU64 startAddress, MxU32 unitsToRead, MxU32 unitSizeInBytes, MxU8 *data, MxU32 *actualNumOfUnitsRead, MxU32 secureFlag)
{
  return mArmAdaptor->debugMemRead(startAddress, unitsToRead, unitSizeInBytes, data, actualNumOfUnitsRead, secureFlag);
}

MxTransactionMasterPort* CarbonAhbMasterS2T::getTmPort()
{
  return mArmAdaptor->ahb_TMaster;
}

void CarbonAhbMasterS2T::carbonSetPortWidth(const char* name, uint32_t bitWidth)
{
  mArmAdaptor->setPortWidth(name, bitWidth);
}
