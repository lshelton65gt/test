// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2005 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
//

/*! \file MxAHBMasterPort.cpp
 *
 *  \brief MxAHBMasterPort implementation
 */


#ifndef CARBON
#define CARBON 0
#endif

//#define MX_AHBMASTER_C
#include "maxsim.h"
#include "persister.h"
#include "MxAHBMasterPort.h"
#include "CarbonReplicateData.h"

const MxU32 MxAHBMasterPort::m_numOfBeats[8]= 
{
	1,
	2,
	4,
	4,
	8,
	8,
	16,
	16
};

const MxU32 MxAHBMasterPort::m_wrapFactor[8]= 
{
	0,
	0,
	4,
	0,
	8,
	0,
	16,
	0
};

const MxU32 MxAHBMasterPort::m_transferSize[8]= 
{
	8,
	16,
	32,
	64,
	128,
	256,
	512,
	1024
};

// constructor
#if CARBON
MxAHBMasterPort::MxAHBMasterPort(bool isAhbLite, sc_mx_m_base *_owner, const string& _portName, MxTransactionProperties *prop, MxU32 _busWidthInBytes)
#else
MxAHBMasterPort::MxAHBMasterPort(sc_mx_m_base *_owner, const string& _portName, MxTransactionProperties *prop, MxU32 _busWidthInBytes)
#endif
    : MxTransactionMasterPort(_owner, _portName, prop)
,m_pSignals(NULL)
,m_portID(0)
,m_prepAddrDataPhaseCB(NULL)
,m_owner(_owner)
,m_bGranted(false)
,m_bDrive1stAddr(false)
,m_bReseted(false)
,m_bIsRequested(false)
,m_bBusreq(false)
,m_bLock(false)
,m_bIdleFinished(false)
,m_defaultMaster(false)
,m_RequestAddr(0)
,m_RequestAcc(0)
,m_RequestIsWrite(false)
,m_startStage(NULL)
,m_idCounter(0)
,m_bInBurst(false)
,m_bGrantCheckedThisCycle(0)
#if CARBON
,mIsAhbLite(isAhbLite)
#else
,mIsAhbLite(false)
#endif
,mLastAddrStageHDrivenHtrans(0)
{
	if ((_busWidthInBytes ==  4) ||
        (_busWidthInBytes ==  8) ||
        (_busWidthInBytes == 16) ||
        (_busWidthInBytes == 32))
    {
        m_busWidthInBytes = _busWidthInBytes;
    }
	else
	{
        getOutputListener()->message(MX_MSG_WARNING, "AHB master port:%s: valid bus byte widths are: 4, 8, 16 and 32 (requested bus width = %u). Defaulting to 4 bytes"
                                     , _portName.c_str() , _busWidthInBytes);
		m_busWidthInBytes = 4;
	}

	// set up AHB pipeline
// 	m_pipeStages = new TAHBTransaction[NUM_AHB_CYCLES];
// 	for (int i = 0; i <= AHB_STAGE_DATA; i++)
// 	{
// 		memset(&m_pipeStages[i], 0, sizeof(m_pipeStages[i]));
// 	}
    memset(m_pipeStages, 0, sizeof(m_pipeStages));

	m_nextStage = &m_pipeStages[AHB_STAGE_ARB];
	m_grntStage = &m_pipeStages[AHB_STAGE_GRNT];
	m_addrStage = &m_pipeStages[AHB_STAGE_ADDR];
	m_dataStage = &m_pipeStages[AHB_STAGE_DATA];

	m_tmpStage = m_nextStage;

        if (mIsAhbLite) {
          m_bGranted = true;
        }

	// set up port properties
	if (prop == NULL)
	{
		MxTransactionProperties props;
		AHB_INIT_TRANSACTION_PROPERTIES(props);
		props.supportsBurst = true;
		setProperties(&props);
	}
	else
	{
		prop->dataBitwidth = _busWidthInBytes * 8;
		setProperties(prop);
	}
}

// destructor
MxAHBMasterPort::~MxAHBMasterPort()
{
	releaseBuffers();

    m_pSignals = NULL;
    m_prepAddrDataPhaseCB = NULL;
    m_owner = NULL;
    m_nextStage = NULL;
    m_grntStage = NULL;
    m_addrStage = NULL;
    m_dataStage = NULL;
    m_tmpStage = NULL;
    m_startStage = NULL;
}

/*! \brief Mark a transaction's data as invalid
 *
 *  This does not delete the data storage.
 */
void
MxAHBMasterPort::releaseStage(TAHBTransaction * const pTrans) const
{
	for (MxU32 i = 0; i < m_busWidthInBytes/4; ++i)
	{
		pTrans->data[i] = AHB_DATA_INVALID;
	}
	pTrans->c.beat = 1;
}

void
MxAHBMasterPort::releaseBuffers(void) const
{
	releaseStage(m_nextStage);
	releaseStage(m_grntStage);
	releaseStage(m_addrStage);
	releaseStage(m_dataStage);
}

/*! \brief Set the data bus byte width
 *
 *  \param busWidthInBytes 4, 8, 16 or 32
 */
void
MxAHBMasterPort::setDataBusWidth(const MxU32 busWidthInBytes)
{
	if ((busWidthInBytes ==  4) ||
        (busWidthInBytes ==  8) ||
        (busWidthInBytes == 16) ||
        (busWidthInBytes == 32))
	{
		m_busWidthInBytes = busWidthInBytes;
	}
	else
	{
        getOutputListener()->message(MX_MSG_WARNING, "AHB master port:%s: valid bus widths are: 32, 64, 128 and 256 (requested bus width = %u). Defaulting to %u"
                                        ,getName().c_str() , busWidthInBytes * 8, 32);
		m_busWidthInBytes = 4;
	}
	mxProperties.dataBitwidth = busWidthInBytes * 8;
	setProperties(&mxProperties);
}

void
MxAHBMasterPort::reset(void)
{
	for (unsigned int i = 0; i < NUM_AHB_CYCLES; ++i)
	{
		memset(m_pipeStages[i].data, 0, m_busWidthInBytes/4);
	}

	m_pSignals = NULL;
	if (isPortEnabled())
	{
		(void)readReq(0, &m_portID, (MxU32*) &m_pSignals, NULL);
	}

    for (unsigned int index = 0; index < NUM_AHB_CYCLES; ++index)
    {
        resetTransaction(m_pipeStages[index]);
    }

    m_nextStage = &m_pipeStages[AHB_STAGE_ARB];
    m_grntStage = &m_pipeStages[AHB_STAGE_GRNT];
    m_addrStage = &m_pipeStages[AHB_STAGE_ADDR];
    m_dataStage = &m_pipeStages[AHB_STAGE_DATA];
	m_tmpStage = m_addrStage;
	m_startStage = m_nextStage;

	releaseBuffers();

	m_bDrive1stAddr = false;
	m_bReseted = true;
        if (mIsAhbLite) {
          m_bGranted = true;
        } else {
          m_bGranted = false;
        }
	m_bIsRequested = false;
	m_RequestAddr = 0xffffffff;
	m_bBusreq = false;
	m_bLock = false;
	m_bIdleFinished = false;	


	m_bInBurst = false;
        if (mIsAhbLite) {
	m_bGrantCheckedThisCycle = true;
        } else {
	m_bGrantCheckedThisCycle = false;
        }
	m_qWriteData.clear();
	m_qReadData.clear();
	m_defaultMaster = false;


	if (isPortEnabled() && (NULL == m_pSignals))
	{
        MESSAGE(MX_MSG_FATAL_ERROR, "%s:%s: AHB master port:%s: has to be connected to an MxAHB bus component or it has to be disabled. Aborting ..."
				, m_owner->getInstanceName().c_str(), m_owner->getName().c_str(), getName().c_str());
	}
	
	// Check that protocol ID & data width match those of AHB Bus   
	if (!MxPortCheck::check((MxTransactionMasterPort*)this))
	{
		getOutputListener()->message(MX_MSG_WARNING, "Incompatible ports connected.");
	}
}

bool
MxAHBMasterPort::isAvailable(void)
{
	bool bRet = true;

	assert(m_bInBurst == (m_nextStage->c.bValid && (m_nextStage->c.hburst > AHB_BURST_INCR) && (m_nextStage->c.totalBeats > m_nextStage->c.beat)));

	if (m_bInBurst)
	{
		bRet = false;
	}
	else if (m_bIsRequested)
	{
		bRet = false;
	}
	else if (m_nextStage->c.bValid)
	{
		if (!(getHGrant() && m_pSignals->hready_z1)) // no grant
		{
			bRet = false;
		}
		else if ( ( (m_nextStage->c.hburst > AHB_BURST_INCR) || ((m_nextStage->c.hburst == AHB_BURST_INCR) && m_nextStage->c.bEBT) )
                   && (m_nextStage->c.beat < m_nextStage->c.totalBeats)) // currently in a beat
		{
			bRet = false;
		}
	}

	return bRet;
}

bool
MxAHBMasterPort::isIdle(void) const
{
	bool bRet = 
		!m_nextStage->c.bValid 
		&& !m_grntStage->c.bValid 
		&& !m_addrStage->c.bValid 
		&& !m_dataStage->c.bValid 
		&& !m_dataStage->c.bDataValidThisCycle 
		&& !m_bIsRequested;

	return bRet;
}

void
MxAHBMasterPort::startRead(MxU32 addr, MxU32 acc)
{
	m_bIsRequested = true;
	m_RequestAddr = addr;
	m_RequestAcc = acc | AHB_ACC_ENCODE_HTRANS(AHB_TRANS_NONSEQ);  
	m_RequestIsWrite = false;
}

void
MxAHBMasterPort::startWrite(MxU32 addr, MxU32 acc)
{
	m_bIsRequested = true;
	m_RequestAddr = addr;
	m_RequestAcc = acc | AHB_ACC_ENCODE_HTRANS(AHB_TRANS_NONSEQ);  
	m_RequestIsWrite = true;
}

void
MxAHBMasterPort::startIdle(MxU32 acc)
{
	m_bIsRequested = true;
	m_RequestAddr = 0;
	m_RequestAcc = acc | AHB_ACC_ENCODE_HTRANS(AHB_TRANS_IDLE) | AHB_ACC_ENCODE_HBURST(AHB_BURST_SINGLE);  
	m_RequestIsWrite = false;
}

TAHBTransaction*
MxAHBMasterPort::startAHB(void)
{
	assert(m_bReseted);

	TAHBTransaction *pStage = m_startStage;

    assert(pStage);
	assert(!pStage->c.bInitialized);
	assert(pStage->c.bValid);

	releaseStage(pStage);

	pStage->c.bInitialized = true;
	pStage->c.id     = m_idCounter++;
	pStage->c.bEBT = false;

	pStage->c.ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_NONE;

	return pStage;
}

void
MxAHBMasterPort::setAcc(TAHBTransaction * const pStage, const MxU32 acc) const
{
	// control field initialization is be done by the user
	pStage->c.ctrl[AHB_IDX_ACC] = acc;

	// initialize acc field with some defaults.
	// this can be overridden by the user
	pStage->c.ctrl[AHB_IDX_ACC] |= AHB_ACC_ENCODE_ASTB(0);
	pStage->c.ctrl[AHB_IDX_ACC] |= AHB_ACC_ENCODE_NCMAHB(0);
	pStage->c.ctrl[AHB_IDX_ACC] |= AHB_ACC_ENCODE_ASB(0);

	decodeACC(pStage);
}

void
MxAHBMasterPort::promoteNextStageToGrntStage(void)
{
	assert(!m_grntStage->c.bValid);
	assert(m_nextStage->c.bValid);

	m_tmpStage  = m_grntStage;
	m_grntStage = m_nextStage;
	m_nextStage = m_tmpStage;
	m_nextStage->c.bValid = false;

	m_grntStage->c.ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_GRNT;

	// inject the next beat into 'next' stage
	// next address for INCR explicitly requested, rather than relying on checking the number of beats
    // unless INCR has been created because of Early Burst Termination
	if ( ( (m_grntStage->c.hburst > AHB_BURST_INCR) || ((m_grntStage->c.hburst == AHB_BURST_INCR) && m_grntStage->c.bEBT) ) && 
         (m_grntStage->c.beat < m_grntStage->c.totalBeats) )
	{

		//MxU32 * const pTmpData = m_nextStage->data;
		m_nextStage->c = m_grntStage->c;	// copy the AHB Transaction structure but not the data
		//m_nextStage->data = pTmpData;

		// these 2 lines are only to visualize the beat in the monitor
#ifdef _DEBUG_
		if (sizeof(*m_nextStage->data) >= 4)
		{
			m_nextStage->data[0] = 0xBEA10000 | m_nextStage->c.beat;
		}
#endif
		m_nextStage->c.ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_NONE;
		m_nextStage->c.bAddressDriven = false;
		m_nextStage->c.beat++;

        // If the burst has been broken, do not use AHB_SEQ as we cannot know 
        // whether the remaining beats in the burst are SEQ       
        if (m_grntStage->c.bEBT)
        {
			m_nextStage->c.htrans = AHB_TRANS_NONSEQ;
        }
        else
        {
			m_nextStage->c.htrans = AHB_TRANS_SEQ;
        }

		if (sizeof(*m_nextStage->data) >= 4)
		{
			m_nextStage->data[0] = 0xBEA10000 | m_nextStage->c.beat;
		}
		m_nextStage->c.ctrl[AHB_IDX_ACC] &= ~AHB_ACC_ENCODE_HTRANS(0xff);
		m_nextStage->c.ctrl[AHB_IDX_ACC] |= AHB_ACC_ENCODE_HTRANS(m_nextStage->c.htrans);

		m_nextStage->c.addr += m_nextStage->c.sizeBeatInBytes;

		// calculation of wrapping address
		if (m_nextStage->c.wrapSize)
		{							
			if (m_nextStage->c.addr == (m_nextStage->c.wrapBoundary + m_nextStage->c.wrapSize)) 
			{
				m_nextStage->c.addr = m_nextStage->c.wrapBoundary;
			}
		}
	}
	else if ((AHB_BURST_INCR == m_grntStage->c.hburst) && !m_grntStage->c.bEBT)
	{
		if (!m_bIsRequested)
		{
			assert(m_grntStage->c.beat == 1);
			assert(m_grntStage->c.totalBeats == 2);
			m_grntStage->c.beat = m_grntStage->c.totalBeats;
		}

		// Set next transaction to SEQ if address is sequential and all control signals match
		if ( m_addrStage->c.bValid && (AHB_BURST_INCR == m_addrStage->c.hburst) && 
             (m_grntStage->c.addr   == (m_addrStage->c.addr+m_addrStage->c.sizeBeatInBytes)) &&
             (m_grntStage->c.hburst == m_addrStage->c.hburst) &&
             (m_grntStage->c.bWrite == m_addrStage->c.bWrite) &&
             (m_grntStage->c.hprot  == m_addrStage->c.hprot)  &&
             (m_grntStage->c.hsize  == m_addrStage->c.hsize) 
            )
		{
			m_grntStage->c.htrans = AHB_TRANS_SEQ;
			m_grntStage->c.ctrl[AHB_IDX_ACC] &= ~AHB_ACC_ENCODE_HTRANS(0xff);
			m_grntStage->c.ctrl[AHB_IDX_ACC] |= AHB_ACC_ENCODE_HTRANS(m_grntStage->c.htrans);
		}
	}
}

void
MxAHBMasterPort::promoteGrntStageToAddrStage(void)
{
	assert(!m_addrStage->c.bValid);
	assert(m_grntStage->c.bValid);

	m_tmpStage  = m_addrStage;
	m_addrStage = m_grntStage;
	m_grntStage = m_tmpStage;
	m_grntStage->c.bValid = false;

	m_addrStage->c.ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_ADDR;

	m_addrStage->c.bAddressDriven = false;
}

void
MxAHBMasterPort::promoteAddrStageToDataStage(void)
{
	assert(!m_dataStage->c.bValid || (m_tmpStage == m_dataStage));

	m_tmpStage  = m_dataStage;
	m_dataStage = m_addrStage;
	m_tmpStage->c.bInitialized = false;
	m_addrStage = m_tmpStage;
	m_addrStage->c.ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_NONE;
	m_dataStage->c.ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_DATA;

	if (m_dataStage->c.bWrite)
	{
		assert(m_qWriteData.size() > 0);
		const MxU32* const pData = m_qWriteData.front();
		m_qWriteData.pop_front();
		//  must copy the complete bus width
		//	MxU32 sizeInBytes = 1 << AHB_ACC_DECODE_HSIZE(m_dataStage->ctrl[AHB_IDX_ACC]);
		//	memcpy(m_dataStage->data, pData, sizeInBytes);
		memcpy(m_dataStage->data, pData, m_busWidthInBytes);
	}
}

MxStatus
MxAHBMasterPort::driveBus(void)
{
    assert(m_pSignals != NULL);

	MxStatus status = MX_STATUS_WAIT;
	bool bNextStageInitiallyValid = m_nextStage->c.bValid || m_bIsRequested;

	m_startStage = m_nextStage;

	m_dataStage->c.bDataValidThisCycle = false;
	m_bIdleFinished = false;	

	if (m_pSignals->hready_z1)
	{
		if (m_addrStage->c.bValid)
		{
			// handle IDLE transfer
			if (m_addrStage->c.htrans == AHB_TRANS_IDLE)
			{
				// An IDLE transfer does not progress any further along the pipeline 
				m_addrStage->c.bValid = false;
				m_addrStage->c.bInitialized = false;
				m_bIdleFinished = true;	
			}
			else
			{
                if ( m_prepAddrDataPhaseCB && m_addrStage->c.bWrite )
                {
                    m_prepAddrDataPhaseCB->prepDataPhaseCB();
                }
				promoteAddrStageToDataStage();
				status = driveDataStage();
			}
		}

		if (m_grntStage->c.bValid)
		{
            if ( m_prepAddrDataPhaseCB )
            {
                m_prepAddrDataPhaseCB->prepAddrPhaseCB( m_grntStage );
            }

			MxU32 acc = m_grntStage->c.ctrl[AHB_IDX_ACC];

			// de-assert lock		  		  
			if (m_bLock && !AHB_ACC_DECODE_HLOCK(acc))
			{
				m_bLock = false;
			}
                       
			// initial ADDR drive (grant obtained from previous cycle)
			// means HMASTER just changed to this master
			// => we can start driving the ADDR
			promoteGrntStageToAddrStage();
			(void)driveAddrStage();
		}

		if (m_nextStage->c.bValid)
		{
			(void)driveNextStage();
			// wait until the next cycle to drive ADDR
			// m_nextStage->bValid is still true, prohibiting any new transactions
			// until ADDR is driven
			assert(!m_grntStage->c.bValid);
			if (m_bGranted)
			{
				promoteNextStageToGrntStage();
				(void)driveGrntStage();
				m_startStage = m_nextStage;
			}
		}
	}
	else // !m_pSignals->hready_z1
	{
		if (m_dataStage->c.bValid)
		{
			status = driveDataStage();
		}

		if (m_addrStage->c.bValid)
		{
			(void)driveAddrStage();
		}

		if (m_nextStage->c.bValid)
		{
			(void)driveNextStage();
            
            if (!m_grntStage->c.bValid && m_bGranted)
            {
				promoteNextStageToGrntStage();
				(void)driveGrntStage();
				m_startStage = m_nextStage;
            }
		}
	}

	// 1. n nextStage->valid: yes/no
	// 2. g granted: yes/no
	// 3. r requested: yes/no
	// ngr | startStage
	// --------------------
	// 000 | -
	// 001 | n
	// 010 | -
	// 011 | n
	// 100 | -
	// 101 | -
	// 110 | -
	// 111 | 
	// 

	// now meet 2 situations:
	// 1) last next stage just went to grnt stage, so drive req for the new one
	// 2) there was no last next stage, so drive this one
	if (!m_nextStage->c.bValid && m_bIsRequested)
	{
		m_nextStage->c.bValid = true;
		m_nextStage->c.ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_NONE;
		m_nextStage->c.addr = m_RequestAddr;
		m_nextStage->c.bWrite = m_RequestIsWrite;
		setAcc(m_nextStage, m_RequestAcc);
		(void)driveNextStage();
        
		m_bIsRequested = false;
		(void)startAHB();
        
		if( !m_grntStage->c.bValid )
        {
            if ( m_defaultMaster )
            {            
                // default master: should be able to drive ADDR in the same cycle hreq is made
                if ( !m_addrStage->c.bValid && m_pSignals->hready_z1 )
                {
                    if ( m_prepAddrDataPhaseCB )
                    {
                        m_prepAddrDataPhaseCB->prepAddrPhaseCB( m_nextStage );
                    }
                    promoteNextStageToGrntStage();
                    (void)driveGrntStage();
                    
                    MxU32 acc = m_grntStage->c.ctrl[AHB_IDX_ACC];
                    if ( AHB_ACC_DECODE_HTRANS(acc) == AHB_TRANS_IDLE )
                    {   // master not ready yet....
                    }
                    else
                    {   // drive ADDR
                        promoteGrntStageToAddrStage();
                        (void)driveAddrStage();
                        // if defaultMaster is a burst, nextStage is setup during
                        //     promoteNextStageToGrntStage, so just driveNextStage()
                        if ( m_nextStage->c.bValid)
                        {   
	                        (void)driveNextStage();
	                        if ( m_bGranted )
	                        {
	                            promoteNextStageToGrntStage();
	                            (void)driveGrntStage();
	                        }
                        }
                    }
                }
	            else if ( m_bGranted )
	            {
	                promoteNextStageToGrntStage();
	                (void)driveGrntStage();
	            }
                
                m_defaultMaster = false;
            }
            else if ( m_bGranted )
            {
                promoteNextStageToGrntStage();
                (void)driveGrntStage();
            }
		}
	}
        		
      if (!mIsAhbLite) {
	if (!bNextStageInitiallyValid && m_bBusreq)
	{
		MxU32 request = 0;

		m_bBusreq = false;

		if (m_bLock)
		{
			request |= (1 << 1);  // lock (coded in bit 1 )
		} 

		(void)requestAccess(request);

		if (!m_bGrantCheckedThisCycle)
		{
			(void)getHGrant();
		}
	}
      }

      if (mIsAhbLite) {
	m_bGrantCheckedThisCycle = true;
      } else {
	m_bGrantCheckedThisCycle = false;
      }
	m_bInBurst = m_nextStage->c.bValid && (m_nextStage->c.hburst > AHB_BURST_INCR) && (m_nextStage->c.totalBeats > m_nextStage->c.beat);

	return status;
}


 // NEXT (REQUEST) stage
 // First, check for grant on the bus.  If not granted, request for the bus
 // and wait (stay in this stage) until granted.

MxStatus
MxAHBMasterPort::driveNextStage(void)
{
	MxStatus status = MX_STATUS_WAIT;
	MxU32 acc = m_nextStage->c.ctrl[AHB_IDX_ACC];
	MxU32 request = 0;
	MxU32 newbLock = AHB_ACC_DECODE_HLOCK(acc);
    
      if (mIsAhbLite) {
        m_defaultMaster = ( m_pSignals->hgrant_z1 == m_portID ) ? true : false;
      } else {
	if (!m_bBusreq || (newbLock && !m_bLock))
	{
		m_bBusreq = true;
		request = 1;     // Bit 1 for bus request
		if (newbLock)
		{
			request |= (1<<1);  // Bit 2 for lock status		
			m_bLock = true;
		}
		(void)requestAccess(request);
        m_defaultMaster = ( m_pSignals->hgrant_z1 == m_portID ) ? true : false;
	}
      }
    // Default Master situation with SINGLE and INCR bursts	must be
    // determined every cycle
    if (m_nextStage->c.hburst == AHB_BURST_SINGLE || m_nextStage->c.hburst == AHB_BURST_INCR)
    {
      m_defaultMaster = ( m_pSignals->hgrant_z1 == m_portID ) ? true : false;
    }

	getHGrant();

	if (!m_bGranted && (m_nextStage->c.hburst > AHB_BURST_INCR) && (m_nextStage->c.beat > 1))
	{
		assert(m_nextStage->c.beat >= 2); // otherwise grant cannot get lost during burst
		// early burst termination
		m_nextStage->c.hburst = AHB_BURST_INCR;
		m_nextStage->c.ctrl[AHB_IDX_ACC] &= ~AHB_ACC_ENCODE_HBURST(0xff);
		m_nextStage->c.ctrl[AHB_IDX_ACC] |= AHB_ACC_ENCODE_HBURST(m_nextStage->c.hburst);
		m_nextStage->c.bEBT = true;
		m_nextStage->c.htrans = AHB_TRANS_NONSEQ;
		m_nextStage->c.ctrl[AHB_IDX_ACC] &= ~AHB_ACC_ENCODE_HTRANS(0xff);
		m_nextStage->c.ctrl[AHB_IDX_ACC] |= AHB_ACC_ENCODE_HTRANS(m_nextStage->c.htrans);
	}

	assert(AHB_CYCLE_NONE == m_nextStage->c.ctrl[AHB_IDX_CYCLE]);

	return status;
}


// GRNT stage

MxStatus
MxAHBMasterPort::driveGrntStage(void)
{
	// the following is done indirectly in the bus by checking acc field during address stage.
	// there should not be an additional call to requestAccess() for this
	return MX_STATUS_OK;
}


MxStatus
MxAHBMasterPort::driveAddrStage(void)
{
	assert(AHB_CYCLE_ADDR == m_addrStage->c.ctrl[AHB_IDX_CYCLE]);

	if (!m_addrStage->c.bAddressDriven || !m_pSignals->hready_z1)
	{
		// either the bus was granted or we're stuck in address stage
		// because there has been a waitstate in data stage during the last cycle

		assert(AHB_CYCLE_ADDR == m_addrStage->c.ctrl[AHB_IDX_CYCLE]); // address cycle

		MxU32 invalidData = AHB_ADDR_INVALID; // data is not valid in the addr cycle
		MxStatus tmpStatus;

		if (m_addrStage->c.bWrite)
		{
			tmpStatus = write(m_addrStage->c.addr, &invalidData, m_addrStage->c.ctrl);
		}
		else if(m_addrStage->c.htrans != AHB_TRANS_IDLE || mLastAddrStageHDrivenHtrans != AHB_TRANS_IDLE)
		{
			tmpStatus = read(m_addrStage->c.addr, &invalidData, m_addrStage->c.ctrl);
			mLastAddrStageHDrivenHtrans = m_addrStage->c.htrans;
		}

		m_addrStage->c.bAddressDriven = true;
	}
	else
	{
		// the arbitration && hready mechanism guarantees that data stage is available now
		assert(!m_dataStage->c.bValid);

		// release the old data buffer now
		releaseStage(m_dataStage);

		m_tmpStage  = m_dataStage;
		m_dataStage = m_addrStage;
		m_addrStage = m_tmpStage;
		m_addrStage->c.ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_NONE;
		m_dataStage->c.ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_DATA; // this is the important one!
	}
	return MX_STATUS_WAIT; // transfer is not yet completed so return waitstate
}


// DATA stage read/write call.
// mainly responsible for retrieving hready/hresp back from AHB slaves.
// TODO: need more work for handling 2-cycle hresp (i.e., ERROR/SPLIT/RETRY)

MxStatus
MxAHBMasterPort::driveDataStage(void)
{
	assert(AHB_CYCLE_DATA == m_dataStage->c.ctrl[AHB_IDX_CYCLE]);

	MxStatus status;

	if (m_dataStage->c.bWrite)
	{
		status = write(m_dataStage->c.addr, m_dataStage->data, m_dataStage->c.ctrl);
	}
	else
	{
		status = read(m_dataStage->c.addr, m_dataStage->data, m_dataStage->c.ctrl);
	}

	if (status == MX_STATUS_OK)
	{
		// only set this flag if not in waited state
		m_dataStage->c.bDataValidThisCycle = true;
		m_dataStage->c.bValid = false;
	}
	else if (status == MX_STATUS_WAIT)
	{
	}
	else
	{
		switch (m_dataStage->c.ctrl[AHB_IDX_ACK])
		{
			case AHB_ACK_ABORT:
				status = MX_STATUS_ERROR;
				//lint -fallthrough
				//	todo!
				//	case MX_ACK_ERROR;
				//		break;
				//	case MX_ACK_RETRY;
				//		break;
			default:
				MESSAGE(MX_MSG_ERROR, "%s%s%s: MxAHBMasterPort::driveDataStage: DATA_CYCLE Invalid acknowledgment from AHB slave. Addr is 0x%08X ACK=0x%08X"
						, m_owner->getInstanceName().c_str(), ":", m_owner->getName().c_str()
						, m_dataStage->c.addr, m_dataStage->c.ctrl[AHB_IDX_ACK]);
				assert(0);
				break;
		}
	}

	// store valid read data and indicate that it is available
	if (m_dataStage->c.bDataValidThisCycle &&  !m_dataStage->c.bWrite)
	{
		assert(m_qReadData.size() < 4 * 16); // try to detect programming errors
	        if (p_alignData) {
		  CarbonReplicateData( m_dataStage->c.ctrl, (m_busWidthInBytes+3)/4, &m_dataStage->data[0]);
		}
		m_qReadData.push_back(&m_dataStage->data[0]);
	}

	return status;
}

/*! \brief Retrieve HRDATA from the data bus.
 *
 *  This is a destructive data fetch - i.e. the data is removed from the head
 *  of an internal queue.
 *  The pointer directly references the data of an AHB transaction.
 *
 *  \see m_pipeStages
 *
 *  \attention The caller must NOT delete the returned pointer.
 *
 *  \return NULL or pointer to data.
 */
MxU32*
MxAHBMasterPort::getReadData(void)
{
	MxU32* pData = NULL;

	if (m_qReadData.size() > 0)
	{
		pData = m_qReadData.front();
		m_qReadData.pop_front();
	}

	return pData;
}

MxStatus
MxAHBMasterPort::setWriteData(MxU32* pData)
{
	MxStatus ret = MX_STATUS_OK;

	if (m_qWriteData.size() <= (4 * 16)) // security check: check for max. 4 stages with each 16 beat bursts
	{
		m_qWriteData.push_back(pData);
	}
	else
	{
		ret = MX_STATUS_ERROR;
	}

	assert(m_qWriteData.size() <= 4 * 16);

	return ret;
}

bool
MxAHBMasterPort::isTransactionFinished(void) const
{
	bool bRet = m_dataStage->c.bDataValidThisCycle && 
		(	(m_dataStage->c.beat == m_dataStage->c.totalBeats)
		 || ((AHB_BURST_INCR  == m_dataStage->c.hburst)  && !m_dataStage->c.bEBT)
		 || (AHB_BURST_SINGLE == m_dataStage->c.hburst));

	return bRet;
}

void
MxAHBMasterPort::decodeACC(TAHBTransaction* const accSetupPipeStage) const
{
	const MxU32 acc = accSetupPipeStage->c.ctrl[AHB_IDX_ACC];

	accSetupPipeStage->c.hburst = AHB_ACC_DECODE_HBURST(acc);
	accSetupPipeStage->c.htrans = AHB_ACC_DECODE_HTRANS(acc);
	accSetupPipeStage->c.hprot  = AHB_ACC_DECODE_HPROT (acc);
	accSetupPipeStage->c.hsize  = AHB_ACC_DECODE_HSIZE (acc);

	if (AHB_TRANS_SEQ != accSetupPipeStage->c.htrans)
	{
		accSetupPipeStage->c.beat = 1;
	}
	accSetupPipeStage->c.totalBeats = m_numOfBeats[accSetupPipeStage->c.hburst];
	accSetupPipeStage->c.sizeBeatInBytes = 1 << accSetupPipeStage->c.hsize; // hsize -> bytes
	accSetupPipeStage->c.ctrl[AHB_IDX_TYPE] = accSetupPipeStage->c.hsize + 1;

	// calculations for wrapping bursts changed	
	accSetupPipeStage->c.wrapSize = (m_wrapFactor[accSetupPipeStage->c.hburst] * accSetupPipeStage->c.sizeBeatInBytes);
	if (accSetupPipeStage->c.wrapSize != 0)
	{
		accSetupPipeStage->c.wrapBoundary = ((accSetupPipeStage->c.addr / accSetupPipeStage->c.wrapSize)
				* accSetupPipeStage->c.wrapSize);
	}
}

bool
MxAHBMasterPort::getHGrant(void)
{
  if (mIsAhbLite) {
  m_bGranted = true;
  } else {
	if (!m_bGrantCheckedThisCycle)
	{
// 		bool inIncrBurst = m_addrStage->bValid && (AHB_BURST_INCR == m_addrStage->hburst);

		m_bGranted = (MX_GRANT_OK == checkForGrant(m_nextStage->c.addr));
		m_bGrantCheckedThisCycle = true;
	}
  }
	return m_bGranted;
}

/*! \brief Persist the AHB master port status.
 *
 *  Having a single function for both save and restore ensures the same order.
 */
bool MxAHBMasterPort::persist(persister & p)
{
//     p.persist(dataBufferInitialized);
    
    bool result = persistTransactionPointer(p, m_addrStage);
    p.persist(m_bBusreq);
    p.persist(m_bDrive1stAddr);
    p.persist(m_bGrantCheckedThisCycle);
    p.persist(m_bGranted);
    p.persist(m_bIdleFinished);
    p.persist(m_bInBurst);
    p.persist(m_bIsRequested);
    p.persist(m_bLock);
    p.persist(m_bReseted);
    p.persist(m_busWidthInBytes);
    result = result && persistTransactionPointer(p, m_dataStage);
    p.persist(m_defaultMaster);
    result = result && persistTransactionPointer(p, m_grntStage);
    p.persist(m_idCounter);
    result = result && persistTransactionPointer(p, m_nextStage);
    for (unsigned int index = 0; result && index < NUM_AHB_CYCLES; ++index)
    {
        result = persistTransaction(p, m_pipeStages[index]);
    }
    p.persist(m_portID);
    // m_prepAddrDataPhaseCB - callback pointer cannot be persisted
    // m_pSignals - set up during reset()
    result = result && persistQueue(p, m_qReadData);
    result = result && persistQueue(p, m_qWriteData);
    p.persist(m_RequestAcc);
    p.persist(m_RequestAddr);
    p.persist(m_RequestIsWrite);
    result = result && persistTransactionPointer(p, m_startStage);
    result = result && persistTransactionPointer(p, m_tmpStage);

    return result;
}

/*! \brief Persist a transaction, complete with its data.
 */
bool MxAHBMasterPort::persistTransaction(persister & p, TAHBTransaction & transaction)
{
    p.persist(transaction.data, MAX_AHB_TRANSACTION_DATA);

    p.persist(transaction.c.id);
    p.persist(transaction.c.addr);
    p.persist(transaction.c.ctrl, AHB_IDX_END);
    p.persist(transaction.c.hburst);
    p.persist(transaction.c.htrans);
    p.persist(transaction.c.hprot);
    p.persist(transaction.c.hsize);
    p.persist(transaction.c.bValid);
    p.persist(transaction.c.bWrite);
    p.persist(transaction.c.bAddressDriven);
    p.persist(transaction.c.bDataValidThisCycle);
    p.persist(transaction.c.bEBT);
    p.persist(transaction.c.totalBeats);
    p.persist(transaction.c.beat);
    p.persist(transaction.c.wrapSize);
    p.persist(transaction.c.wrapBoundary);
    p.persist(transaction.c.sizeBeatInBytes);
    p.persist(transaction.c.bInitialized);

    return true;
}

/*! \brief Persist a pointer in to the transaction array.
 *
 *  m_pipeStages is a block of transactions that the transaction pointers point in to.
 *  Obviously the address cannot be saved and restored successfully, so some
 *  way of referring back to the same element or NULL is needed.
 *
 *  One way might be to use the offset from the start of the block, but that
 *  would be fragile and break if the transaction size changed. So a simple
 *  scheme is used to compare the known addresses against the pointer to
 *  produce the transaction number index or anything bigger than NUM_AHB_CYCLES for NULL.
 *
 *  \see m_pipeStages
 */
bool MxAHBMasterPort::persistTransactionPointer(persister & p, TAHBTransaction * & pTransaction)
{
    if (p.Restoring())
    {
        MxU32 index(~0);
        p.persist(index);
        pTransaction = (index < NUM_AHB_CYCLES) ? &m_pipeStages[index] : NULL;
    }
    else
    {
        MxU32 index(~0);
        if (NULL != pTransaction)
        {
            index = (pTransaction - m_pipeStages) % sizeof(TAHBTransaction);
        }
        p.persist(index);
    }

    return true;
}

/*! \brief Persist of transaction data queue
 *
 *  \attention Saving is nondestructive on the queue
 *
 *  The queue holds pointers in to m_pipeStages data
 *
 *  \see m_pipeStages
 */
bool MxAHBMasterPort::persistQueue(persister & p, std::deque<MxU32*> & q)
{
    if (p.Restoring())
    {
        MxU32 count(0);
        p.persist(count);

        q.clear();

        for (MxU32 index = 0; index < count; ++index)
        {
            MxU32 pipeStageIndex(0);
            p.persist(pipeStageIndex);
            
            MxU32 * pData = (pipeStageIndex < NUM_AHB_CYCLES) ? &m_pipeStages[pipeStageIndex].data[0] : NULL;
            q.push_back(pData);
        }
    }
    else
    {
        MxU32 count(q.size());
        p.persist(count);

        for (MxU32 index = 0; index < count; ++index)
        {
            MxU32 * const pData = q[index];
            MxU32 pipeStageIndex = (pData - &m_pipeStages[0].data[0]) % sizeof(TAHBTransaction);
            p.persist(pipeStageIndex);
        }
    }

    return true;
}

/*! \brief Return transaction to fresh reset state.
 */
void MxAHBMasterPort::resetTransaction(TAHBTransaction & transaction) const
{
    transaction.c.bValid              = false;
    transaction.c.id                  = 0;
    transaction.c.bInitialized        = false;
    transaction.c.bDataValidThisCycle = false;
    transaction.c.bEBT                = false;
}
