// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#include "AHBRTL_SS.h"

#if CARBON
#include "carbon_ahb.h"
#endif

#include "AHBS2T.h"


AHBRTL_SS::AHBRTL_SS( AHBS2T *_owner, MxU32 _id ) : sc_mx_signal_slave( "AHBRTL_SS" )

{
	id = _id;
    owner = _owner;
#if CARBON
    setMxOwner(owner->mCarbonComponent);
#else
    setMxOwner(owner); 
#endif
}

/* Access functions */
void
AHBRTL_SS::driveSignal(MxU32 value, MxU32* extValue)
{
	assert(id < AHB_SS_END);

    switch (id)
    {
    case HWDATA_SS:
        // HWDATA handling
        owner->latchedFromRTL[ HWDATA_SS_31_0 ] = value;
        if ( owner->dataWidthInWords > 1 )
        {
            if ( extValue == NULL )
            {
                owner->message( MX_MSG_ERROR, 
                    "%s::driveSignal(): Invalid HWDATA received from RTL (extValue == NULL)",
                    owner->getInstanceID().c_str() );
                return;
            }
            memcpy( &owner->latchedFromRTL[ HWDATA_SS_63_32 ], extValue,
                    sizeof(MxU32) * (owner->dataWidthInWords - 1) );
        }
        break;
    case HADDR_SS:
        owner->latchedFromRTL[ HADDR_SS_31_0 ] = value;
        if ( owner->addrWidthInWords > 1 )
        {
            if ( extValue == NULL )
            {
                owner->message( MX_MSG_ERROR,
                    "%s::driveSignal(): Invalid HADDR received from RTL (extValue == NULL)",
                    owner->getInstanceID().c_str() );
                return;
            }
            memcpy( &owner->latchedFromRTL[ HADDR_SS_63_32 ], extValue,
                    sizeof(MxU32) * (owner->addrWidthInWords - 1) );
        }
        break;
    default:
        owner->latchedFromRTL[id] = value;
        break;
    }
}

MxU32
AHBRTL_SS::readSignal()
{
    return 0;
}

void
AHBRTL_SS::readSignal(MxU32* value, MxU32* extValue)
{
  (void)value; (void)extValue;
}
