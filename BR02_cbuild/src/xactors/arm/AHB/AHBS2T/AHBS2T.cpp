// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.


#include <stdio.h>

#include "carbon_ahb.h"
#include "CarbonReplicateData.h"
#include "AHBS2T.h"
#include "AHBRTL_SS.h"
#if MAXSIM_MAJOR_VERSION >= 7
#include "AHB_Transaction_Ext.h"
#endif

#if (!CARBON)
#include "AHBS2T_MxDI.h"
//#include "lc.h"
#endif

#ifdef CARBON
#include <stdarg.h>
#define AHBS2T_RTL_SM(NAME) portFactory(mCarbonComponent, xtorName, NAME, mpCarbonObject)
#else
#define AHBS2T_RTL_SM(NAME) new sc_port<sc_mx_signal_if>(this, NAME)
#endif

const MxU32 AHBS2T::m_numOfBeats[8]= 
{
    1,
    2,
    4,
    4,
    8,
    8,
    16,
    16
};

void
AHBS2T::prepAddrPhaseCB( TAHBTransaction* pipeStage)
{
    MxU64 addr_h = latchedFromRTL[HADDR_SS_63_32];
    pipeStage->c.addr = addr_h << 32 | latchedFromRTL[HADDR_SS_31_0];
    pipeStage->c.bWrite  = latchedFromRTL[HWRITE_SS];
    // Encode access control fields   
    encodeACC(pipeStage);  

    // Decode access control fields to update pipeline fields   
    ahb_TMaster->decodeACC(pipeStage);  
}

void
AHBS2T::prepDataPhaseCB()
{
    // Setup the data transaction from RTL
    // Note the master port promotes the address stage to data stage before
    // the data stage is driven.
    //TAHBTransaction* p_AddrTrans = ahb_TMaster->getAddrStage();
    replicateWriteData( ahb_TMaster->getAddrStage() );
    ahb_TMaster->setWriteData(wdata);
}

#if CARBON
AHBS2T::AHBS2T(sc_mx_module* carbonComp, const char *xtorName, ahbXtorTypeEnum xtorType, CarbonObjectID **carbonObj, CarbonPortFactory *portFactory)
  : CarbonAdaptorBase(carbonComp, xtorName, carbonObj)
#else
AHBS2T::AHBS2T(sc_mx_m_base* c, const string &s, int xtorType) : sc_mx_module(c, s)
#endif
{
    m_xtorType = xtorType;

    for ( int i = 0; i < AHB_SS_END; i++ )
        fromRTL[i] = NULL;

#if CARBON
    ahb_TMaster = new MxAHBMasterPort(xtorType == XTOR_TYPE_AHBLITEM_S2T, carbonComp, "ahb_out");
#else
    ahb_TMaster = new MxAHBMasterPort(this, "ahb_out" );
#endif
    registerPort(ahb_TMaster, "ahb_out");

    ahb_TMaster->registerPrepAddrDataPhaseCB( this );

    maxsim::MxSignalProperties *signalprop = new maxsim::MxSignalProperties;
    signalprop->isOptional = false;

    // signals from RTL
    if (m_xtorType == XTOR_TYPE_AHBM_S2T)
    {
        fromRTL[HBREQ_SS] = new AHBRTL_SS( this, HBREQ_SS );
        signalprop->bitwidth = 1;
        fromRTL[HBREQ_SS]->setProperties(signalprop);
        registerPort( fromRTL[HBREQ_SS], "hbusreq" );
    }
    else
        fromRTL[HBREQ_SS] = NULL;

    fromRTL[HADDR_SS] = new AHBRTL_SS( this, HADDR_SS );
    signalprop->bitwidth = 32;
    fromRTL[HADDR_SS]->setProperties(signalprop);
    registerPort( fromRTL[HADDR_SS], "haddr" );

    fromRTL[HBURST_SS] = new AHBRTL_SS( this, HBURST_SS );
    signalprop->bitwidth = 3;
    fromRTL[HBURST_SS]->setProperties(signalprop);
    registerPort( fromRTL[HBURST_SS], "hburst" );

    fromRTL[HPROT_SS] = new AHBRTL_SS( this, HPROT_SS );
    signalprop->bitwidth = 6; // hprot[4] = ALLOC, hprot[5] = EXREQ
    fromRTL[HPROT_SS]->setProperties(signalprop);
    registerPort( fromRTL[HPROT_SS], "hprot" );

    fromRTL[HSIZE_SS] = new AHBRTL_SS( this, HSIZE_SS );
    signalprop->bitwidth = 3;
    fromRTL[HSIZE_SS]->setProperties(signalprop);
    registerPort( fromRTL[HSIZE_SS], "hsize" );

    fromRTL[HTRANS_SS] = new AHBRTL_SS( this, HTRANS_SS );
    signalprop->bitwidth = 2;
    fromRTL[HTRANS_SS]->setProperties(signalprop);
    registerPort( fromRTL[HTRANS_SS], "htrans" );

    fromRTL[HWDATA_SS] = new AHBRTL_SS( this, HWDATA_SS );
    signalprop->bitwidth = 32;
    fromRTL[HWDATA_SS]->setProperties(signalprop);
    registerPort( fromRTL[HWDATA_SS], "hwdata" );

    fromRTL[HWRITE_SS] = new AHBRTL_SS( this, HWRITE_SS );
    signalprop->bitwidth = 1;
    fromRTL[HWRITE_SS]->setProperties(signalprop);
    registerPort( fromRTL[HWRITE_SS], "hwrite" );

    fromRTL[HLOCK_SS] = new AHBRTL_SS( this, HLOCK_SS );
    signalprop->bitwidth = 1;
    fromRTL[HLOCK_SS]->setProperties(signalprop);
    registerPort( fromRTL[HLOCK_SS], "hlock" );

    fromRTL[HBSTRB_SS] = new AHBRTL_SS( this, HBSTRB_SS );
    signalprop->bitwidth = 16; // supports up to 128 bit data width
    fromRTL[HBSTRB_SS]->setProperties(signalprop);
    registerPort( fromRTL[HBSTRB_SS], "hstrb" );

    fromRTL[HUNALIGN_SS] = new AHBRTL_SS( this, HUNALIGN_SS );
    signalprop->bitwidth = 1;
    fromRTL[HUNALIGN_SS]->setProperties(signalprop);
    registerPort( fromRTL[HUNALIGN_SS], "hunalign" );

    // signals to RTL
    HRDATA_SMaster = AHBS2T_RTL_SM( "HRDATA" );
    signalprop->bitwidth = 32;
    HRDATA_SMaster->setProperties(signalprop);
    registerPort( HRDATA_SMaster, "hrdata" );

    HREADY_SMaster = AHBS2T_RTL_SM( "HREADY" );
    signalprop->bitwidth = 1;
    HREADY_SMaster->setProperties(signalprop);
    registerPort( HREADY_SMaster, "hready" );

    HRESP_SMaster = AHBS2T_RTL_SM( "HRESP" );
    if (mIsAhbLite) {
      signalprop->bitwidth = 1;
    } else {
      signalprop->bitwidth = 3; // hresp[2] = EXRESP
    }
    HRESP_SMaster->setProperties(signalprop);
    registerPort( HRESP_SMaster, "hresp" );

    if (m_xtorType == XTOR_TYPE_AHBM_S2T)
    {
        HGRANT_SMaster = AHBS2T_RTL_SM( "HGRANT" );
        signalprop->bitwidth = 1;
        HGRANT_SMaster->setProperties(signalprop);
        registerPort( HGRANT_SMaster, "hgrant" );
    }
    else
        HGRANT_SMaster = NULL;

#if (!CARBON)
    SC_MX_CLOCKED();
    registerPort( dynamic_cast< sc_mx_clock_slave_p_base* >( this ), "clk-in");
#endif
    
    p_initComplete = false;

#if (!CARBON)
    defineParameter("Enable Debug Messages", "false", MX_PARAM_BOOL, true);
    defineParameter("Data Bus Width", "32", MX_PARAM_VALUE, false);
    defineParameter("Address Bus Width", "32", MX_PARAM_VALUE, false);
    defineParameter("Big Endian", "false", MX_PARAM_BOOL, false);
    defineParameter("Align Data", "false", MX_PARAM_BOOL, false);
#endif

    p_mxdi = NULL;  
}

AHBS2T::~AHBS2T()
{
    delete ahb_TMaster;
    for ( MxU32 i = 0; i < AHB_SS_END; i++ )
    {
        if ( fromRTL[i] )
        {
            delete fromRTL[i];
            fromRTL[i] = NULL;
        }
    }
    delete HRDATA_SMaster;
    delete HREADY_SMaster;
    delete HRESP_SMaster;
    if (HGRANT_SMaster)
        delete HGRANT_SMaster;
}

#if CARBON
MxStatus AHBS2T::readDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return ahb_TMaster->readDbg(addr, value, ctrl);
}

MxStatus AHBS2T::writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return ahb_TMaster->writeDbg(addr, value, ctrl);
}

MxStatus AHBS2T::debugMemWrite(MxU64 startAddress, 
			       MxU32 unitsToWrite, 
			       MxU32 unitSizeInBytes, 
			       const MxU8 *data, 
			       MxU32 *actualNumOfUnitsWritten, 
			       MxU32 /*secureFlag*/) 
{
  MxU32 ctrl[AHB_IDX_END];
  ctrl[AHB_IDX_TYPE]  = AHB_TYPE_BYTE;
  ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_DATA;
  
  MxU32 value;
  MxStatus status;
  *actualNumOfUnitsWritten = unitsToWrite;

  for(MxU32 i = 0; i < unitsToWrite*unitSizeInBytes; ++i) {
    memset(&value, data[i], 4);
    if ((status = ahb_TMaster->writeDbg(startAddress+i, &value, ctrl)) != MX_STATUS_OK) {
      return status;
    }
  }
  return MX_STATUS_OK;
}
  
MxStatus AHBS2T::debugMemRead(MxU64 startAddress, 
			      MxU32 unitsToRead, 
			      MxU32 unitSizeInBytes, 
			      MxU8 *data, 
			      MxU32 *actualNumOfUnitsRead, 
			      MxU32 /*secureFlag*/)
{
  MxU32 ctrl[AHB_IDX_END];
  ctrl[AHB_IDX_TYPE] = AHB_TYPE_BYTE;
  ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_DATA;

  MxU32 value;
  MxStatus status;
  *actualNumOfUnitsRead = unitsToRead;

  for (MxU32 i = 0; i < unitsToRead*unitSizeInBytes; ++i) {
    if ((status = ahb_TMaster->readDbg(startAddress+i, &value, ctrl)) != MX_STATUS_OK) {
      return status;
    }
    data[i] = value & 0xFF;
  }

  return MX_STATUS_OK;
}


#endif


void 
AHBS2T::communicate()
{
    // Get pointers to stages of the master port pipeline
    TAHBTransaction* pNextTrans    = ahb_TMaster->getNextStage();
    TAHBTransaction* pAddrTrans    = ahb_TMaster->getAddrStage();

    // Track the beat number, needed in case HBUSREQ is deasserted while granted
    static int beat, totalBeats;
    static bool removeGrant;
    if ( latchedFromRTL[HTRANS_SS] == AHB_TRANS_NONSEQ ) // new series, restart count
    {
        beat = 1;
        totalBeats = m_numOfBeats[latchedFromRTL[HBURST_SS]];
        removeGrant = false;
    }
    else if ( p_currentReady && latchedFromRTL[HTRANS_SS] == AHB_TRANS_SEQ ) // continuation
        beat++;

    // --------------------------------------------------------------------------
    //  Update the Master Port Pipline
    // --------------------------------------------------------------------------
    
    // Handle bus request if arbitration cycle is empty
    if (ahb_TMaster->isAvailable())
    {
        MxU32 acc = 0;

        if (latchedFromRTL[HBREQ_SS])
        {
            // dummy call to place request for bus 
            // encode lock field 
            acc = AHB_ACC_ENCODE_HLOCK(latchedFromRTL[HLOCK_SS]) & 1;
            ahb_TMaster->startIdle(acc);            
            p_currentLock = latchedFromRTL[HLOCK_SS];
        }
        else if (p_currentGrant && !p_currentLock && latchedFromRTL[HLOCK_SS])
        {
            // ensure assertion of HLOCK prior to start of address phase is
            // communicated to bus, even if HBUSREQ is not asserted because
            // the master is already granted 
            acc = AHB_ACC_ENCODE_HLOCK(latchedFromRTL[HLOCK_SS]) & 1;
            ahb_TMaster->startIdle(acc);            
            p_currentLock = latchedFromRTL[HLOCK_SS];
        }
        else if (p_currentGrant && p_currentLock && !latchedFromRTL[HLOCK_SS])
        {
            // ensure assertion of HLOCK prior to start of address phase is
            // communicated to bus, even if HBUSREQ is not asserted because
            // the master is already granted 
            acc = AHB_ACC_ENCODE_HLOCK(latchedFromRTL[HLOCK_SS]) & 1;
            ahb_TMaster->startIdle(acc);            
            p_currentLock = latchedFromRTL[HLOCK_SS];
        }
        else if (!latchedFromRTL[HBREQ_SS] && p_currentGrant && 
                  latchedFromRTL[HTRANS_SS] != AHB_TRANS_IDLE &&
                  latchedFromRTL[HBURST_SS] != AHB_BURST_INCR )
        {
            // if HBUSREQ is deasserted during a burst, a dummy call
            // still needs to be made to the Master Port to ensure that
            // the remaining beats are entered into the pipeline.
            // In the case of AHB_INCR, HBUSREQ should remain asserted 
            // until the address phase for the last transfer has been 
            // started. Thus this case is excluded
            if ( totalBeats - beat > 1 ) // make dummy call until penultimate beat
            {
                acc = AHB_ACC_ENCODE_HLOCK(latchedFromRTL[HLOCK_SS]) & 1;
                ahb_TMaster->startIdle(acc);            
                p_currentLock = latchedFromRTL[HLOCK_SS];
            }
            if ( totalBeats - beat == 0 )
                removeGrant = true;
        }
    }

    if ( !p_currentReady && pAddrTrans->c.bValid && 
         (pAddrTrans->c.htrans == AHB_TRANS_IDLE) &&
         (latchedFromRTL[HTRANS_SS] == AHB_TRANS_NONSEQ)
       )
    {
        // Catch the case where there is a waited data phase and the RTL master  
        // changes from an IDLE to NONSEQ.  

        MxU64 addr_h = latchedFromRTL[HADDR_SS_63_32];
        pAddrTrans->c.addr = addr_h << 32 | latchedFromRTL[HADDR_SS_31_0];
        pAddrTrans->c.bWrite  = latchedFromRTL[HWRITE_SS];

        // Encode access control fields   
        encodeACC(pAddrTrans);  

        // Decode access control fields to update pipeline fields   
        ahb_TMaster->decodeACC(pAddrTrans); 
    }

    // --------------------------------------------------------------------------
    //  Drive the Master Port Pipline   and update RTL ouputs
    // --------------------------------------------------------------------------
    
    if (!ahb_TMaster->isIdle())
    {
        // Forcing update to current HLOCK value in case of change
        if ( pNextTrans->c.bValid ) // May contain stale HLOCK value which is driven without update
            pNextTrans->c.ctrl[AHB_IDX_ACC] = pNextTrans->c.ctrl[AHB_IDX_ACC] & ~1 | AHB_ACC_ENCODE_HLOCK(latchedFromRTL[HLOCK_SS]);
        if ( pAddrTrans->c.bValid ) // Because prepDataPhaseCB() not always called before driveDataStage()
            pAddrTrans->c.ctrl[AHB_IDX_ACC] = pAddrTrans->c.ctrl[AHB_IDX_ACC] & ~1 | AHB_ACC_ENCODE_HLOCK(latchedFromRTL[HLOCK_SS]);

        MxStatus ret = ahb_TMaster->driveBus();
        
        if (ret ==  MX_STATUS_ERROR) assert(0);
    
        // Update HREADY 
        // This is done in update() 

        // Update HRDATA
        MxU32* pRdata = ahb_TMaster->getReadData();
        if (pRdata)
        {
            HRDATA_SMaster->driveSignal(pRdata[0], ( dataWidthInWords > 1 ) ? &pRdata[1] : NULL );
        }
        
        // Update HRESP 
        // MasterPort only handles OK response unless using ARM11 AMBA2 ext,
        // then it is possible to encode XFAIL in HRESP
#if MAXSIM_MAJOR_VERSION >= 7
        HRESP_SMaster->driveSignal(AHB_ACK_DONE | AHB_HRESP_ENCODE_EXRESP(AHB_SIDEBAND_DECODE_EXRESP(pNextTrans->c.ctrl[AHB_IDX_SIDEBAND])), NULL);
#else
        HRESP_SMaster->driveSignal(AHB_ACK_DONE, NULL);
#endif
    }
    
    // update the grant
    if (m_xtorType != XTOR_TYPE_AHBM_S2T) {
      p_currentGrant = true;
    } else {
    if ( !removeGrant )
    {
        p_currentGrant = (MX_GRANT_OK == 
            ((ahb_TMaster->m_pSignals->hgrant == ahb_TMaster->m_portID) ? MX_GRANT_OK : MX_GRANT_DENIED));
        if (m_xtorType == XTOR_TYPE_AHBM_S2T)
            HGRANT_SMaster->driveSignal(p_currentGrant, NULL);
    }
    else // HBUSREQ was deasserted and beat=totalBeats, release the grant
    {
        if (m_xtorType == XTOR_TYPE_AHBM_S2T)
            HGRANT_SMaster->driveSignal(false, NULL);
        removeGrant = false;
    }
    }
}

void 
AHBS2T::update()
{
    // Latch HREADY, this is only allowed in update
    p_currentReady = ahb_TMaster->m_pSignals->hready;
    HREADY_SMaster->driveSignal(p_currentReady, NULL);  
}


void 
AHBS2T::encodeACC(TAHBTransaction* pipeStage)
{

    // Remove any encoding that the master port may have done,
    // except for the fields below   
    pipeStage->c.ctrl[AHB_IDX_ACC] &= (AHB_ACC_ENCODE_ASTB(0x1)  |      
                                            AHB_ACC_ENCODE_NCMAHB(0x1)|
                                            AHB_ACC_ENCODE_ASB(0x1) 
                                           );   

    // Encode the access control info from RTL signals  
    pipeStage->c.ctrl[AHB_IDX_ACC] |= AHB_ACC_ENCODE_HTRANS(latchedFromRTL[HTRANS_SS]);   
    pipeStage->c.ctrl[AHB_IDX_ACC] |= AHB_ACC_ENCODE_HLOCK(latchedFromRTL[HLOCK_SS]);   
    if ( AHB_TRANS_IDLE != latchedFromRTL[HTRANS_SS] )
    {
        pipeStage->c.ctrl[AHB_IDX_ACC] |= AHB_ACC_ENCODE_HBURST(latchedFromRTL[HBURST_SS]);
        pipeStage->c.ctrl[AHB_IDX_ACC] |= AHB_ACC_ENCODE_HSIZE (latchedFromRTL[HSIZE_SS]);
        pipeStage->c.ctrl[AHB_IDX_ACC] |= AHB_ACC_ENCODE_HPROT (latchedFromRTL[HPROT_SS]);
#if MAXSIM_MAJOR_VERSION >= 7
        AHB_SIDEBAND_SET_HBSTRB(pipeStage->c.ctrl[AHB_IDX_SIDEBAND], latchedFromRTL[HBSTRB_SS]);
        AHB_SIDEBAND_SET_HUNALIGN(pipeStage->c.ctrl[AHB_IDX_SIDEBAND], latchedFromRTL[HUNALIGN_SS]);
        AHB_SIDEBAND_SET_ALLOC(pipeStage->c.ctrl[AHB_IDX_SIDEBAND], AHB_HPROT_DECODE_ALLOC(latchedFromRTL[HPROT_SS])); // hprot[4]
        AHB_SIDEBAND_SET_EXREQ(pipeStage->c.ctrl[AHB_IDX_SIDEBAND], AHB_HPROT_DECODE_EXREQ(latchedFromRTL[HPROT_SS])); // hprot[5] 
#endif
    }
}

void 
AHBS2T::replicateWriteData(TAHBTransaction* pipeStage)
{
    MxU32 index, offset, mask, tmpData, wordData;
    
    // copy write data from RTL
    memcpy( wdata, &latchedFromRTL[HWDATA_SS_31_0], 
            sizeof(MxU32) * dataWidthInWords );
                       
    // replicate the data across all byte lanes, if size < data bus width
    if ((MxU32)(1 << AHB_ACC_DECODE_HSIZE(pipeStage->c.ctrl[AHB_IDX_ACC])) < 
        (dataWidthInWords * 4))
    {
        // determine word index
        index = (MxU32(pipeStage->c.addr) & ((dataWidthInWords - 1) << 2)) >> 2;
    
        switch(pipeStage->c.ctrl[AHB_IDX_TYPE])
        {
            case AHB_TYPE_BYTE:
                // determine which byte to replicate               
                offset = (MxU32(pipeStage->c.addr) & 0x03);               
                if (p_bigEndian) offset ^= 3;              
                offset *= 8;
                mask   = 0xFF << offset;
                tmpData = (wdata[index] & mask) >> offset;
                           
                // replicate across whole of data bus
                memset(wdata, tmpData, 
                       sizeof(MxU32) * dataWidthInWords);
                break;

            case AHB_TYPE_HWORD:
                // determine which halfword to replicate
                offset = (MxU32(pipeStage->c.addr) & 0x03);               
                if (p_bigEndian) offset ^= 2;              
                offset *= 8;
                mask   = 0xFFFF << offset;
                tmpData = (wdata[index] & mask) >> offset;

                // replicate across word
                wordData = tmpData | (tmpData << 16); 
                                           
                // replicate across whole of data bus
                for (MxU32 i= 0; i < dataWidthInWords; i++)
                {
                    wdata[i] = wordData;    
                }
                break;

            case AHB_TYPE_WORD:
                // determine which word to replicate
                wordData = wdata[index];
                           
                // replicate across whole of data bus
                for (MxU32 i= 0; i < dataWidthInWords; i++)
                {
                    wdata[i] = wordData;    
                }
                break;

            case AHB_TYPE_DWORD:
                switch (dataWidthInWords)
                {
                case 1:
                    message(MX_MSG_ERROR, "%s::write(): Data is too large for bus.", getInstanceID().c_str());
                    break;
                //case 2: // bus and data widths are both 64-bit, leave it alone
                    //break;
                case 4: // bus is 128-bit
                    // replicate according to dword index                                
                    if ((pipeStage->c.addr & 0x08) >> 3)
                    {
                        wdata[0] = wdata[2];    
                        wdata[1] = wdata[3];    
                    }
                    else
                    {
                        wdata[2] = wdata[0];    
                        wdata[3] = wdata[1];    
                    }
                    break;
                }
                break;

            case AHB_TYPE_128BIT:
                switch (dataWidthInWords)
                {
                case 1:
                case 2:
                    message(MX_MSG_ERROR, "%s::write(): Data is too large for bus.", getInstanceID().c_str());
                    break;
                //case 4: // bus and data widths are both 128-bit, leave it alone
                    //break;
                }
                break;
                
            default:
                message(MX_MSG_ERROR, "%s::write(): Data is too large for bus.", getInstanceID().c_str());
                break;
        }
    }
}


void AHBS2T::setPortWidth(const char* name, uint32_t bitWidth)
{
  if (strcmp(name, "hwdata") == 0 || strcmp(name, "hrdata") == 0 || (strcmp(name, "haddr") == 0)) {

    // Calculate word count based on the next power of 2 above the given bit width.
    int wordCount = nextPo2(bitWidth)>>5;

    if (strcmp(name, "haddr") == 0)
      addrWidthInWords = wordCount & 0x3; // Limit address to 64 bit max
    else
      dataWidthInWords = wordCount;
  }
}

void 
AHBS2T::init()
{
    if ( dataWidthInWords > 1 )
    {
        MxSignalProperties prop;
        
        ahb_TMaster->setDataBusWidth( dataWidthInWords * 4 );   // in bytes
        
        prop = *(fromRTL[HWDATA_SS]->getProperties());
        prop.bitwidth = dataWidthInWords * 32;
        fromRTL[HWDATA_SS]->setProperties(&prop);

#if (!CARBON)
        prop = *(HRDATA_SMaster->getProperties());
        prop.bitwidth = dataWidthInWords * 32;
        HRDATA_SMaster->setProperties(&prop);
#endif
    }

    if ( addrWidthInWords > 1 )
    {
        MxSignalProperties prop;

        prop = *(fromRTL[HADDR_SS]->getProperties());
        prop.bitwidth = addrWidthInWords * 32;
        fromRTL[HADDR_SS]->setProperties(&prop);
    }

    wdata = new MxU32[ dataWidthInWords ];

#if (!CARBON)
    p_mxdi = new AHBS2T_MxDI(this);
    
    if (p_mxdi == NULL)
    {
        message(MX_MSG_FATAL_ERROR,
                "%s::init(): Failed to allocate memory for MxDI interface. Aborting Simulation!",
                getInstanceID().c_str());
    }

    // Initialize ourself for save/restore.
    initStream( this );

    // Call the base class after this has been initialized.
    sc_mx_module::init();
#endif
    p_initComplete = true;
}

void
AHBS2T::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  (void)level; (void)filelist;

    MxU32 hrdata[ MAX_DATA_WIDTH_IN_WORDS ];

    memset(latchedFromRTL, 0, sizeof(MxU32) * AHB_SS_END);
    memset(hrdata, 0, sizeof(MxU32) * MAX_DATA_WIDTH_IN_WORDS );    
    if (m_xtorType == XTOR_TYPE_AHBLITEM_S2T)
        latchedFromRTL[HBREQ_SS] = 1; // tie to high

    HREADY_SMaster -> driveSignal(AHB_READY, NULL);
    HRESP_SMaster     -> driveSignal(AHB_ACK_DONE, NULL);
    if (m_xtorType == XTOR_TYPE_AHBM_S2T)
        HGRANT_SMaster    -> driveSignal(AHB_NOGRANT, NULL);
    HRDATA_SMaster    -> driveSignal(hrdata[0], &hrdata[1]);
    
#if (!CARBON)
    // Call the base class after this has been reset.
    sc_mx_module::reset(level, filelist);
#endif

    ahb_TMaster->reset();
    
    p_currentGrant = (m_xtorType == XTOR_TYPE_AHBLITEM_S2T);
    p_currentReady = false;
    p_currentLock = false;
}

void 
AHBS2T::terminate()
{
    if (p_mxdi != NULL)
    {
        // Release the MXDI Interface
        delete p_mxdi;
        p_mxdi = NULL;
    }
    
    delete [] wdata;
    
#if (!CARBON)
    // Call the base class first.
    sc_mx_module::terminate();
#endif
}

void
AHBS2T::setParameter( const string &name, const string &value )
{
    MxConvertErrorCodes status = MxConvert_SUCCESS;

    if (name == "Enable Debug Messages")
    {
        status = MxConvertStringToValue(value, &p_enableDebugMessages);
    }
    else if (name == "Big Endian")
    {
        status = MxConvertStringToValue(value, &p_bigEndian);
    }
    else if (name == "Align Data")
    {
        status = MxConvertStringToValue(value, &p_alignData);
	ahb_TMaster->SetAlignData( p_alignData );
    }
    else if (name == "Data Bus Width")
    {
        if (p_initComplete == false)
        {
            MxU32 tmp = 0;
            status = MxConvertStringToValue(value, &tmp);
            
            if (status == MxConvert_SUCCESS)
            {
                switch ( tmp )
                {
                case 32: dataWidthInWords = 1; break;
                case 64: dataWidthInWords = 2; break;
		        case 128: dataWidthInWords = 4; break;
                default:
                    if (tmp < 32)
                    {
                        message( MX_MSG_WARNING, "%s::setParameter(): Rounding value %s for parameter: %s to 32.", getInstanceID().c_str(), value.c_str(), name.c_str() );
                        dataWidthInWords = 1;
                    }
                    else if (tmp < 64)
                    {
                        message( MX_MSG_WARNING, "%s::setParameter(): Rounding value %s for parameter: %s to 64.", getInstanceID().c_str(), value.c_str(), name.c_str() );
                        dataWidthInWords = 2;
                    }
                    else
                    {
                        message( MX_MSG_WARNING, "%s::setParameter(): Rounding value %s for parameter: %s to 128.", getInstanceID().c_str(), value.c_str(), name.c_str() );
                        dataWidthInWords = 4;
                    }
                    break;
                }
            }
        }
        else
        {
            message(MX_MSG_WARNING, "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.", getInstanceID().c_str(), name.c_str() );
        }
    }
    else if (name == "Address Bus Width")
    {
        if (p_initComplete == false)
        {
            MxU32 tmp = 0;
            status = MxConvertStringToValue(value, &tmp);
            
            if (status == MxConvert_SUCCESS)
            {
                if (tmp > 64)
                {
                    message( MX_MSG_WARNING, "%s::setParameter(): Cannot set parameter <%s> higher than 64. Using 64 instead.", getInstanceID().c_str(), name.c_str() );
                    addrWidthInWords = 2;
                }
                else
                    addrWidthInWords = 1;
            }
        }
        else
        {
            message(MX_MSG_WARNING, "%s::setParameter(): Cannot change parameter <%s> at runtime. Assignment ignored.", getInstanceID().c_str(), name.c_str() );
        }
    }

    if ( status == MxConvert_SUCCESS )
    {
#if (!CARBON)
        sc_mx_module::setParameter(name, value);
#endif
    }
    else
    {
        message( MX_MSG_WARNING, "%s::setParameter(): Illegal value <%s> "
                 "passed for parameter <%s>. Assignment ignored.", getInstanceID().c_str(), value.c_str(), name.c_str() );
    }
}

#if (!CARBON)
string
AHBS2T::getProperty( MxPropertyType property )
{
    string description; 
    switch ( property ) 
    {
    case MX_PROP_IP_PROVIDER:
        return "ARM";
    case MX_PROP_LOADFILE_EXTENSION:
        return "";
    case MX_PROP_REPORT_FILE_EXT:
        return "yes";
    case MX_PROP_COMPONENT_TYPE:
        return MxComponentTypes[MX_TYPE_TRANSACTOR];
    case MX_PROP_COMPONENT_VERSION:
        return "2.1.001";
    case MX_PROP_MSG_PREPEND_NAME:
        return "yes"; 
    case MX_PROP_DESCRIPTION:
         switch ( m_xtorType )
        {
        case XTOR_TYPE_AHBM_S2T:
            description = "AHB HDL Master Signals to MaxSim Transaction Converter";
            break;
        case XTOR_TYPE_AHBLITEM_S2T:
            description = "AHB-Lite HDL Master Signals to MaxSim Transaction Converter";
            break;
        }
        
        return description + " Compiled on " + __DATE__ + ", " + __TIME__; 
    case MX_PROP_MXDI_SUPPORT:
        return "yes";
    case MX_PROP_SAVE_RESTORE:
        return "yes";
    default:
        return "";
    }
    return "";
}

MXDI*
AHBS2T::getMxDI()
{
  return p_mxdi;
}

string
AHBS2T::getName(void)
{
    switch ( m_xtorType )
    {
    case XTOR_TYPE_AHBS2T:
        return "AHBS2T";
    case XTOR_TYPE_AHBM_S2T:
        return "AHBM_S2T";
    case XTOR_TYPE_AHBLITEM_S2T:
        return "AHBLiteM_S2T";
    }
    return "";
}
#endif


bool
AHBS2T::saveData( MxODataStream &data )
{
  (void)data;

    // TODO:  Add your save code here.
    // This shows how the example state variable is saved.
    // data << exampleStateVariable;

    // TODO:  Add verification if applicable.
    // return save was successful
    return true;
}

bool
AHBS2T::restoreData( MxIDataStream &data )
{
  (void)data;

    // TODO:  Add your restore code here.
    // This shows how the example state variable is restored.
    // data >> exampleStateVariable;

    // TODO:  Add verification if applicable.
    // return restore was successful
    return true;
}

#if (!CARBON)
/************************
 * AHBS2T Factory class
 ***********************/

class AHBS2TFactory : public MxFactory
{
public:
    AHBS2TFactory() : MxFactory ( "AHBS2T" ) {}
    sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
    { 
      return new AHBS2T(c, id, XTOR_TYPE_AHBS2T); 
    }
};


/************************
 * AHBM_S2T Factory class
 ***********************/

class AHBM_S2TFactory : public MxFactory
{
public:
    AHBM_S2TFactory() : MxFactory ( "AHBM_S2T" ) {}
    sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
    { 
      return new AHBS2T(c, id, XTOR_TYPE_AHBM_S2T); 
    }
};


/************************
 * AHBLiteM_S2T Factory class
 ***********************/

class AHBLiteM_S2TFactory : public MxFactory
{
public:
    AHBLiteM_S2TFactory() : MxFactory ( "AHBLiteM_S2T" ) {}
    sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
    { 
      return new AHBS2T(c, id, XTOR_TYPE_AHBLITEM_S2T); 
    }
};

/**
 *  Entry point into the memory components (from MaxSim)
 */
extern "C" void 
MxInit(void)
{
  new AHBS2TFactory();
  new AHBM_S2TFactory();
  new AHBLiteM_S2TFactory();
}
#endif
