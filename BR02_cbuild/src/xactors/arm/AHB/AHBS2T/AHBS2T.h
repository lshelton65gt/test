// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#ifndef AHBS2T__H
#define AHBS2T__H

#if CARBON
#include "CarbonAdaptorBase.h"
#include "util/UtCLicense.h"
#endif

#include "maxsim.h"
#include "MxSaveRestore.h"
#include "MxAHBMasterPort.h"

typedef enum
{
    HBREQ_SS = 0,
    HADDR_SS_31_0,
    HADDR_SS_63_32,
    HBURST_SS,
    HPROT_SS,
    HSIZE_SS,
    HTRANS_SS,
    HWDATA_SS_31_0,
    HWDATA_SS_63_32,
    HWDATA_SS_95_64,
    HWDATA_SS_127_96,
    HWRITE_SS,
    HLOCK_SS,
    HBSTRB_SS, /* ARM11 AMBA2 ext */
    HUNALIGN_SS, /* ARM11 AMBA2 ext */
    HDOMAIN_SS,
    AHB_SS_END
} ahbRtlSSenum;

typedef enum
{
    XTOR_TYPE_AHBS2T = 0,
    XTOR_TYPE_AHBM_S2T,
    XTOR_TYPE_AHBLITEM_S2T
} ahbXtorTypeEnum;

#define HWDATA_SS HWDATA_SS_31_0
#define HADDR_SS HADDR_SS_31_0
#define MAX_NUM_TRANSFERS 16 

// supporting up to 128bit data bus
#define MAX_DATA_WIDTH_IN_WORDS 4


#if (!CARBON)
class AHBS2T_MxDI;
#endif

#if CARBON
class AHBS2T : public CarbonAdaptorBase, public PrepAddrDataPhaseCallback
#else
class AHBS2T : public sc_mx_module, public MxSaveRestore, public PrepAddrDataPhaseCallback
#endif
{
    friend class AHBRTL_SS;
#if (!CARBON)
    friend class AHBS2T_MxDI;
#endif

public:
    MxAHBMasterPort* ahb_TMaster;

    AHBRTL_SS* fromRTL[AHB_SS_END];

    RTL_PORT *HRDATA_SMaster;
    RTL_PORT *HREADY_SMaster;
    RTL_PORT *HRESP_SMaster;
    RTL_PORT *HGRANT_SMaster;
	
    // constructor / destructor
#if CARBON
    AHBS2T(sc_mx_module* carbonComp, const char *xtorName, ahbXtorTypeEnum xtorType, CarbonObjectID **carbonObj, CarbonPortFactory *portFactory);
#else
    AHBS2T(sc_mx_m_base* c, const string &s, int xtorType);
#endif
    virtual ~AHBS2T();

    // overloaded methods for clocked components
    void communicate();
    void update();

    // Implementation of MxSaveRestore interface methods
    virtual bool saveData( MxODataStream &data );
    virtual bool restoreData( MxIDataStream &data );

    // overloaded sc_mx_module methods
#if (!CARBON)
    string getName();
#endif
    void setParameter(const string &name, const string &value);
#if (!CARBON)
    string getProperty( MxPropertyType property );
#endif
    void setPortWidth(const char* name, uint32_t bitWidth);
    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);

#if (!CARBON)
    // The MxDI interface.
    MXDI* getMxDI();
#endif

    void prepAddrPhaseCB( TAHBTransaction* );
    void prepDataPhaseCB();

#if CARBON
    MxStatus readDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
    MxStatus writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl);

    MxStatus debugMemWrite(MxU64 startAddress, MxU32 unitsToWrite, MxU32 unitSizeInBytes, const MxU8 *data, MxU32 *actualNumOfUnitsWritten, MxU32 secureFlag);
    MxStatus debugMemRead(MxU64 startAddress, MxU32 unitsToRead, MxU32 unitSizeInBytes, MxU8 *data, MxU32 *actualNumOfUnitsRead, MxU32 secureFlag);
#endif

private:

    void encodeACC(TAHBTransaction* pipeStage);
    void replicateWriteData(TAHBTransaction* pipeStage);

    // Component Parameters
    bool  p_enableDebugMessages;
    MxU32 dataWidthInWords;
    MxU32 addrWidthInWords;
    MxU32* wdata;
    bool p_bigEndian;
    bool p_alignData;
    
    // The MxDI interface.
    MXDI* p_mxdi;

    // Flag to indicate init() complete    
    bool p_initComplete;

    // Signals recorded from previous cycle
    bool p_currentGrant;
    bool p_currentReady;
    bool p_currentLock;
    bool mIsAhbLite;

    int m_xtorType;

    static const MxU32 m_numOfBeats[8];

public:

    MxU32 latchedFromRTL[AHB_SS_END];
    
};

#endif
