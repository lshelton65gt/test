// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2006 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.


#ifndef _T_Signals_
#define _T_Signals_

#include "maxsim.h"


class T_SignalsBase
{
public:

	T_SignalsBase () {}
	virtual ~T_SignalsBase () {}
};

template <MxU32 _NUM_SIGNALS, MxU32 _NUM_MASTERS> class T_Signals : public T_SignalsBase
{
public:

	T_Signals ();
	~T_Signals ();

	static string	getBaseName					();

	static MxU32	getNumSignals				();
	static MxU32	getNumMasters				();
	static MxU32	getNumSlaves				();

	static string	getName						(MxU32 id);
	static bool		isMaster					(MxU32 id);	
	static MxU32	getBitWidth					(MxU32 id);
	static bool		isConfigurableDataBitWidth	(MxU32 id);

private:

	struct Info 
	{
		bool	mIsMaster;
		MxU32	mBitWidth;
		bool	mIsConfigurableDataBitWidth;
		char*	mName;
	};
	static const Info	mInfo[_NUM_SIGNALS];
	static const string	mBaseName;
};	


template <class PORT> class T_SignalSlave : public MxSignalSlave
{

public:
	T_SignalSlave (const string& name, MxU32 id, PORT *port);
	~T_SignalSlave ();
  
	void	driveSignal (MxU32  value, MxU32* extValue);
	MxU32	readSignal ();

private:
	MxU32 mId;
	PORT* mPort;
};



// implementation -------------------------------------------------------------------


template <MxU32 _NUM_SIGNALS, MxU32 _NUM_MASTERS>
T_Signals<_NUM_SIGNALS, _NUM_MASTERS>::T_Signals ()
{
}

template <MxU32 _NUM_SIGNALS, MxU32 _NUM_MASTERS>
T_Signals<_NUM_SIGNALS, _NUM_MASTERS>::~T_Signals ()
{
}

template <MxU32 _NUM_SIGNALS, MxU32 _NUM_MASTERS>
MxU32 T_Signals<_NUM_SIGNALS, _NUM_MASTERS>::getNumSignals ()
{
	return _NUM_SIGNALS;
}

template <MxU32 _NUM_SIGNALS, MxU32 _NUM_MASTERS>
MxU32 T_Signals<_NUM_SIGNALS, _NUM_MASTERS>::getNumMasters ()
{
	return _NUM_MASTERS;
}

template <MxU32 _NUM_SIGNALS, MxU32 _NUM_MASTERS>
MxU32 T_Signals<_NUM_SIGNALS, _NUM_MASTERS>::getNumSlaves ()
{
	return (_NUM_SIGNALS-_NUM_MASTERS);
}

template <MxU32 _NUM_SIGNALS, MxU32 _NUM_MASTERS>
string T_Signals<_NUM_SIGNALS, _NUM_MASTERS>::getName (MxU32 id)
{	
	return mInfo[id].mName;
}

template <MxU32 _NUM_SIGNALS, MxU32 _NUM_MASTERS>
bool T_Signals<_NUM_SIGNALS, _NUM_MASTERS>::isMaster (MxU32 id)
{
	return mInfo[id].mIsMaster;
}

template <MxU32 _NUM_SIGNALS, MxU32 _NUM_MASTERS>
MxU32 T_Signals<_NUM_SIGNALS, _NUM_MASTERS>::getBitWidth (MxU32 id)
{
	return mInfo[id].mBitWidth;
}

template <MxU32 _NUM_SIGNALS, MxU32 _NUM_MASTERS>
bool T_Signals<_NUM_SIGNALS, _NUM_MASTERS>::isConfigurableDataBitWidth (MxU32 id)
{
	return mInfo[id].mIsConfigurableDataBitWidth;
}


template <MxU32 _NUM_SIGNALS, MxU32 _NUM_MASTERS>
string T_Signals<_NUM_SIGNALS, _NUM_MASTERS>::getBaseName ()
{
	return mBaseName;
}



template <class PORT>
T_SignalSlave<PORT>::T_SignalSlave (const string& name, MxU32 id, PORT *port) : MxSignalSlave(name)
{
	mId = id;
	mPort = port;
}

template <class PORT>
T_SignalSlave<PORT>::~T_SignalSlave ()
{}

  
template <class PORT>
void T_SignalSlave<PORT>::driveSignal (MxU32 value, MxU32* extValue)
{
	mPort->setSlaveSignal(mId, value, extValue);
}

template <class PORT>
MxU32 T_SignalSlave<PORT>::readSignal ()
{
	return 0;
}

#endif // _T_Signals_
