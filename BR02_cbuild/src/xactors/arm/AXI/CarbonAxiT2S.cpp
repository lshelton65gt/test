
#include "CarbonAxiT2S.h"

#define CARBON 1
#include "MxAXISignals.h"
#include "T_T2S.h"

typedef T_T2S<AXISignals, MxAXIPortT2S_Conv> AXIT2S;

CarbonAxiT2S::CarbonAxiT2S(sc_mx_module* carbonComp,
                           const char *xtorName,
                           CarbonObjectID **carbonObj,
                           CarbonPortFactory *portFactory,
                           CarbonDebugReadWriteFunction* readDebug, 
                           CarbonDebugReadWriteFunction* writeDebug,
                           CarbonDebugTransactionFunction* debugTrans)
{
  // we ignore readDbg and writeDbg, they are not used for axi
  CarbonDebugAccessCallbacks cb = {carbonComp, NULL, NULL, NULL, debugTrans}; 
  mArmAdaptor = new AXIT2S(carbonComp, xtorName, "2.1.003", carbonObj, portFactory, cb);
}
CarbonAxiT2S::CarbonAxiT2S(sc_mx_module* carbonComp,
                           const char *xtorName,
                           CarbonObjectID **carbonObj,
                           CarbonPortFactory *portFactory,
                           CarbonDebugAccessFunction* debugAccess)
{
  CarbonDebugAccessCallbacks cb = {carbonComp, debugAccess, NULL, NULL, NULL};
  mArmAdaptor = new AXIT2S(carbonComp, xtorName, "2.1.003", carbonObj, portFactory, cb);
}

sc_mx_signal_slave *CarbonAxiT2S::carbonGetPort(const char *name)
{
  sc_mx_signal_slave *port;

  if (strcmp("awready", name) == 0) {
    port = ((AXIT2S *) mArmAdaptor)->mSSignalPorts[AXI_AWREADY];
  }
  else if (strcmp("wready", name) == 0) {
    port = ((AXIT2S *) mArmAdaptor)->mSSignalPorts[AXI_WREADY];
  }
  else if (strcmp("bid", name) == 0) {
    port = ((AXIT2S *) mArmAdaptor)->mSSignalPorts[AXI_BID];
  }
  else if (strcmp("bresp", name) == 0) {
    port = ((AXIT2S *) mArmAdaptor)->mSSignalPorts[AXI_BRESP];
  }
  else if (strcmp("buser", name) == 0) {
    port = ((AXIT2S *) mArmAdaptor)->mSSignalPorts[AXI_BUSER];
  }
  else if (strcmp("bvalid", name) == 0) {
    port = ((AXIT2S *) mArmAdaptor)->mSSignalPorts[AXI_BVALID];
  }
  else if (strcmp("arready", name) == 0) {
    port = ((AXIT2S *) mArmAdaptor)->mSSignalPorts[AXI_ARREADY];
  }
  else if (strcmp("rid", name) == 0) {
    port = ((AXIT2S *) mArmAdaptor)->mSSignalPorts[AXI_RID];
  }
  else if (strcmp("rdata", name) == 0) {
    port = ((AXIT2S *) mArmAdaptor)->mSSignalPorts[AXI_RDATA];
  }
  else if (strcmp("rresp", name) == 0) {
    port = ((AXIT2S *) mArmAdaptor)->mSSignalPorts[AXI_RRESP];
  }
  else if (strcmp("rlast", name) == 0) {
    port = ((AXIT2S *) mArmAdaptor)->mSSignalPorts[AXI_RLAST];
  }
  else if (strcmp("ruser", name) == 0) {
    port = ((AXIT2S *) mArmAdaptor)->mSSignalPorts[AXI_RUSER];
  }
  else if (strcmp("rvalid", name) == 0) {
    port = ((AXIT2S *) mArmAdaptor)->mSSignalPorts[AXI_RVALID];
  }
  else {
    port = NULL;
    ((AXIT2S *) mArmAdaptor)->message(MX_MSG_FATAL_ERROR, "Carbon AXI T2S has no port named \"%s\"", name);
  }
  return port;
}

CarbonAxiT2S::~CarbonAxiT2S()
{
  delete ((AXIT2S *) mArmAdaptor);
}

void CarbonAxiT2S::communicate()
{
  ((AXIT2S *) mArmAdaptor)->communicate();
}

void CarbonAxiT2S::update()
{
  ((AXIT2S *) mArmAdaptor)->update();
}

void CarbonAxiT2S::init()
{
  ((AXIT2S *) mArmAdaptor)->init();
}

void CarbonAxiT2S::terminate()
{
  ((AXIT2S *) mArmAdaptor)->terminate();
}

void CarbonAxiT2S::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  ((AXIT2S *) mArmAdaptor)->reset(level, filelist);
}

void CarbonAxiT2S::setParameter(const string &name, const string &value)
{
  ((AXIT2S *) mArmAdaptor)->setParameter(name, value);
}

//! Number of slave memory regions
int CarbonAxiT2S::getNumRegions()
{
  return ((AXIT2S *) mArmAdaptor)->getNumSlaveRegions();
}

//! Slave Address regions
void CarbonAxiT2S::getAddressRegions(uint64_t* start, uint64_t* size, string* name)
{
  ((AXIT2S *) mArmAdaptor)->getSlaveAddressRegions(start, size, name);
}

void CarbonAxiT2S::carbonSetPortWidth(const char* name, uint32_t bitWidth)
{
  ((AXIT2S *) mArmAdaptor)->setPortWidth(name, bitWidth);
}

