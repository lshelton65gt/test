// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#include "MxAXISignals.h"
#include "T_S2T.h"
#include "T_T2S.h"

typedef T_S2T<AXISignals, MxAXIPortS2T_Conv> AXI_S2T;
typedef T_T2S<AXISignals, MxAXIPortT2S_Conv> AXI_T2S;



/************************
 * AXI_S2T Factory class
 ***********************/
class AXI_S2TFactory : public MxFactory
{
public:
    AXI_S2TFactory() : MxFactory ( "AXI_S2T" ) {}
    sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
    { 
        return new AXI_S2T("AXI_S2T", c, id, "2.1.002"); 
    }
};


class AXI_T2SFactory : public MxFactory
{
public:
    AXI_T2SFactory() : MxFactory ( "AXI_T2S" ) {}
    sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
    { 
        return new AXI_T2S("AXI_T2S", c, id, "2.1.003"); 
    }
};

/**
 *  Entry point into the memory components (from MaxSim)
 */
extern "C" void 
MxInit(void)
{
	new AXI_S2TFactory();
    new AXI_T2SFactory();
}
