// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.


#ifndef _MxAXISignals_
#define _MxAXISignals_

#ifdef CARBON
#define CarbonSignalMasterPort CarbonXtorAdaptorToVhmPort
class CarbonXtorAdaptorToVhmPort;
#include "carbon_arm_adaptor.h"
#else
#define CarbonSignalMasterPort MxSignalMasterPort
#endif

#include "maxsim.h"
#include "T_Signals.h"
#include "MxAXIPort.h"
#include "ContainerAXITransactionNextCts.h"
#include "ContainerAXITransactionInactivateChannelAW.h"
#include <vector>

#define MAX_NUM_AXI_TRANSACTIONS_ON_HEAP 256


enum AXI_SIGNALS
{
		// write address channel signals

		AXI_AWID = 0,	// write address id
		AXI_AWADDR,		// write address
		AXI_AWLEN,		// burst length
		AXI_AWSIZE,		// burst size
		AXI_AWBURST,	// burst type
		AXI_AWLOCK,		// lock type
		AXI_AWCACHE,	// cache type
		AXI_AWPROT,		// protection type
		AXI_AWUSER,		// user data
		AXI_AWVALID,	// write address valid
		AXI_AWREADY,	// write address ready

	
		// write data channel signals

		AXI_WID,		// write id tag
		AXI_WDATA,		// write data
		AXI_WSTRB,		// write strobes
		AXI_WLAST,		// write last
		AXI_WUSER,		// user data
		AXI_WVALID,		// write valid
		AXI_WREADY,		// write ready


		// write response channel signals

		AXI_BID,		// response id
		AXI_BRESP,		// write response
		AXI_BUSER,		// user data
		AXI_BVALID,		// write response valid
		AXI_BREADY,		// response ready


		// read address channel signals

		AXI_ARID,		// read address id
		AXI_ARADDR,		// read address
		AXI_ARLEN,		// burst length
		AXI_ARSIZE,		// burst size
		AXI_ARBURST,	// burst type
		AXI_ARLOCK,		// lock type
		AXI_ARCACHE,	// cache type
		AXI_ARPROT,		// protection type
		AXI_ARUSER,		// user data
		AXI_ARVALID,	// read address valid
		AXI_ARREADY,	// read address ready


		// read data channel signals

		AXI_RID,		// read id tag
		AXI_RDATA,		// read data
		AXI_RRESP,		// read response
		AXI_RLAST,		// read last
		AXI_RUSER,		// user data
		AXI_RVALID,		// read valid
		AXI_RREADY,		// read ready

		AXI_LAST_TAG
};

#define AXI_NUM_SIGNALS		AXI_LAST_TAG
#define AXI_NUM_MASTERS		28
#define AXI_NUM_MXU32_DATA	(1024/32)
#define AXI_NUM_MXU32_ADDR  2
#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
#define MAX_REGIONS         1024
#endif

class AXISignals : public T_Signals<AXI_NUM_SIGNALS, AXI_NUM_MASTERS>
{
public:
//	static void convertDataMx2Hw (const AXITransaction* axi, const MxU32* srcData, MxU32* dstData, MxU32 srcStrb, MxU32& dstStrb);
//	static void convertDataHw2Mx (const AXITransaction* axi, const MxU32* srcData, MxU32* dstData, MxU32 srcStrb, MxU32& dstStrb);

	static void copyData  (const AXITransaction* axi, const MxU32* srcData, MxU32* dstData);
	static void mergeData (const AXITransaction* axi, const MxU32* srcData, const MxU32* refData, MxU32* dstData);
};


class MxAXIPortS2T : public MxTransactionMasterPort
{

public:
	MxAXIPortS2T (sc_mx_module* owner, const string& name);
	~MxAXIPortS2T ();

	virtual void		init			(MxU32 dataBitWidth);
	virtual void		reset			();
	virtual void		terminate		();
	
	MxU32				getDataBitWidth () const { return mDataBitWidth; }
	MxU32				getDataNumMxU32	() const { return mDataNumMxU32; }

	virtual void		communicate		();			
	virtual void		update			();

protected:	
	void				set_aw			(MxU32 id, MxU64 addr, MxU32 len, MxU32 size, MxU32 burst, MxU32 lock, MxU32 cache, MxU32 prot, MxU32 user, MxU32 valid);
	void				set_ar			(MxU32 id, MxU64 addr, MxU32 len, MxU32 size, MxU32 burst, MxU32 lock, MxU32 cache, MxU32 prot, MxU32 user, MxU32 valid);
	void				set_w			(MxU32 id, MxU32* data, MxU32 strb, MxU32 last, MxU32 user, MxU32 valid);
	void				set_bready		(MxU32 value);
	void				set_rready		(MxU32 value);

	bool				mWasNotifiedAW;
	bool				mWasNotifiedAR;
	bool				mWasNotifiedW;
	bool				mWasNotifiedR;
	bool				mWasNotifiedB;

	bool				mClearValidR;
	bool				mClearValidDelayedR;
	bool				mClearValidB;
	bool				mClearValidDelayedB;

private:
	virtual void		set_rvalid		(AXITransaction* axi) = 0;
	virtual void		set_bvalid		(AXITransaction* axi) = 0;
	virtual void		set_awready		(MxU32 ready) = 0;
	virtual void		set_wready		(MxU32 ready) = 0;
	virtual void		set_arready		(MxU32 ready) = 0;


	void				remTransaction	(std::vector<AXITransaction*>& channel, MxU32 id);
  
	AXITransaction*		getTransaction	(std::vector<AXITransaction*>& channel, MxU32 id, bool forAW) const;


//r	enum TransactionFlagsEnum
//r	{
//r		FLAGS_NONE = 0,
//r		FLAGS_INCR_CTS = 1
//r	};
	ContainerAXITransactionNextCts<MAX_NUM_AXI_TRANSACTIONS_ON_HEAP>			 containerAXITransactionNextCts;
	ContainerAXITransactionInactivateChannelAW<MAX_NUM_AXI_TRANSACTIONS_ON_HEAP> containerAXITransactionInactivateChannelAW;
	bool					mNextCtsAndInactivateChannelAwAlreadyDone;


	class NotifyHandler : public MxNotifyHandlerIF
	{
	public:
		NotifyHandler() { mOwner = NULL; }
		virtual ~NotifyHandler() {}

		void			setOwner	(MxAXIPortS2T* owner) { mOwner = owner; }
		virtual void	notifyEvent	(MxTransactionInfo* info);

	private:
		MxAXIPortS2T* mOwner;
	};
	friend class NotifyHandler;

	SimpleHeap<AXITransaction, MAX_NUM_AXI_TRANSACTIONS_ON_HEAP> mHeap;
	
	NotifyHandler			mNotifyHandler;
	AXITransaction			mEmptyTransaction;

	AXITransaction*			mActiveAW;
	AXITransaction*			mActiveAR;
	AXITransaction*			mActiveW;
	AXITransaction*			mActiveB;
	AXITransaction*			mActiveR;
		
	std::vector<AXITransaction*>	mActiveWrite;
	std::vector<AXITransaction*>	mActiveRead;

	AXITransaction*			mAxiDeleteR;
	AXITransaction*			mAxiDeleteB;

	MxU32					mDataBitWidth;
	MxU32					mDataNumMxU32;
};

class MxAXIPortS2T_Conv : public MxAXIPortS2T
{
public:
	MxAXIPortS2T_Conv (sc_mx_module* owner, const string& name);
	~MxAXIPortS2T_Conv ();

	void setSignalMasterPort	(MxU32 id, CarbonSignalMasterPort* port);
	void setSlaveSignal			(MxU32 id, MxU32 value, MxU32* extValues);
    void init                   (MxU32 dataBitWidth, MxU32 addrBitWidth);
	void reset					();

	void communicate			();
	void update					();

    MxU32 getAddrBitWidth       () const { return mAddrBitWidth; }
    MxU32 getAddrNumMxU32       () const { return mAddrNumMxU32; }

private:
	void set_rvalid		(AXITransaction* axi);
	void set_bvalid		(AXITransaction* axi);
	void set_awready	(MxU32 ready);
	void set_wready		(MxU32 ready);
	void set_arready	(MxU32 ready);
	
	MxU32				mValues		[AXI_NUM_SIGNALS];
	MxU32*				mValuesExt	[AXI_NUM_SIGNALS];
	bool				mSet		[AXI_NUM_SIGNALS];
	CarbonSignalMasterPort* mSPorts		[AXI_NUM_SIGNALS];

	MxU32				mDataBufferWrite[AXI_NUM_MXU32_DATA];
	MxU32				mDataBufferRead[AXI_NUM_MXU32_DATA];

    MxU32               mAddrBitWidth;
    MxU32               mAddrNumMxU32;

    MxU32               mAddrBufferAW[AXI_NUM_MXU32_ADDR];
    MxU32               mAddrBufferAR[AXI_NUM_MXU32_ADDR];
};


class MxAXIPortS2T_Trace : public MxAXIPortS2T
{
public:
	MxAXIPortS2T_Trace (sc_mx_module* owner, const string& name);
	~MxAXIPortS2T_Trace ();

	void setPin			(MxU32 id, MxU32* pin);
	void setPinRef		(MxU32 id, MxU32* pin);

	virtual void init	(MxU32 dataBitWidth, bool mergeData = false);

	void communicate	();
	void update			();

private:
	void set_rvalid		(AXITransaction* axi);
	void set_bvalid		(AXITransaction* axi);
	void set_awready	(MxU32 ready);
	void set_wready		(MxU32 ready);
	void set_arready	(MxU32 ready);

	MxU32*	mPins	[AXI_NUM_SIGNALS];
	MxU32*  mPinsRef[AXI_NUM_SIGNALS];

	bool	mMergeData;
};






class MxAXIPortT2S : public MxTransactionSlave
{

public:
#ifdef CARBON
  MxAXIPortT2S (sc_mx_module* owner, const string& name,
		const CarbonDebugAccessCallbacks& dbaCb);
#else
  MxAXIPortT2S (sc_mx_module* owner, const string& name);
#endif
	virtual ~MxAXIPortT2S ();

    /* Synchronous access functions */
    MxStatus				read					(MxU64 addr, MxU32* value, MxU32* ctrl);
    MxStatus				write					(MxU64 addr, MxU32* value, MxU32* ctrl);
    MxStatus				readDbg					(MxU64 addr, MxU32* value, MxU32* ctrl);
    MxStatus				writeDbg				(MxU64 addr, MxU32* value, MxU32* ctrl);

    /* Asynchronous access functions */
    MxStatus				readReq					(MxU64 addr, MxU32* value, MxU32* ctrl, MxTransactionCallbackIF* callback);
    MxStatus				writeReq				(MxU64 addr, MxU32* value, MxU32* ctrl, MxTransactionCallbackIF* callback);

    /* Arbitration functions */
    MxGrant					requestAccess(MxU64 addr);
    MxGrant					checkForGrant(MxU64 addr);

    /* MxSI 3.0: new shared-memory based asynchronous transaction functions */
    void					driveTransaction		(MxTransactionInfo* info);
    void					cancelTransaction		(MxTransactionInfo* info);
    MxStatus				debugTransaction		(MxTransactionInfo* info);

    /* Memory map functions */
    virtual int				getNumRegions			();
    virtual void			getAddressRegions		(MxU64* start, MxU64* size, string* name);

#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
    /* MxSI 3.0: now the address regions can be set from the outside! */
    void					setAddressRegions		(MxU64* start, MxU64* size, string* name);
    MxMemoryMapConstraints* getMappingConstraints	();
#endif

	virtual void	init			(MxU32 dataBitWidth, MxU32 numAddressRegions = 0, MxU64* start = NULL, MxU64* size = NULL, string* name = NULL);
	virtual void	reset			();
	virtual void	terminate		();

	virtual void	communicate		();
	virtual	void	update			();

	MxU32			getDataBitWidth () const { return mDataBitWidth; }
	MxU32			getDataNumMxU32	() const { return mDataNumMxU32; }


protected:
	void			set_awready		(MxU32 value);
	void			set_arready		(MxU32 value);
	void			set_wready		(MxU32 value);
	void			set_bvalid		(MxU32 id, MxU32 resp, MxU32 user, MxU32 valid);
	void			set_rvalid		(MxU32 id, MxU32* data, MxU32 resp, MxU32 last, MxU32 user, MxU32 valid);

	bool			mWasTransactionAW;
	bool			mWasTransactionAR;
	bool			mWasTransactionW;
	bool			mClearValidAW;
	bool			mClearValidDelayedAW;
	bool			mClearValidAR;
	bool			mClearValidDelayedAR;
	bool			mClearValidW;
	bool			mClearValidDelayedW;

	AXITransaction* getActiveAW     () { return mActiveAW; }
	AXITransaction* getActiveAR     () { return mActiveAR; }
    AXITransaction* getActiveW      () { return mActiveW; }

private:
	virtual void	set_aw			(AXITransaction* axi) = 0;
	virtual void	set_ar			(AXITransaction* axi) = 0;
	virtual void	set_w			(AXITransaction* axi) = 0;	
	virtual void	set_bready		(MxU32 ready) = 0;
	virtual void	set_rready		(MxU32 ready) = 0;
		
	void			addTransaction	(std::vector<AXITransaction*>& buffer, AXITransaction* axi);
	void			remTransaction	(std::vector<AXITransaction*>& buffer, MxU32 id);
	AXITransaction*	getTransaction	(std::vector<AXITransaction*>& buffer, MxU32 id) const;

#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
    void            initMemoryMapConstraints();
#endif
//r	enum TransactionFlagsEnum
//r	{
//r		FLAGS_NONE = 0,
//r		FLAGS_INCR_CTS = 1
//r	};
	ContainerAXITransactionNextCts<MAX_NUM_AXI_TRANSACTIONS_ON_HEAP> containerAXITransactionNextCts;
	bool					mNextCtsAlreadyDone;

	struct AddressRegion
	{
		MxU64	mStart;
		MxU64	mSize;
		string	mName;
	};
private:
	AXITransaction*			mActiveAW;
	AXITransaction*			mActiveAR;
	AXITransaction*			mActiveW;

	std::vector<AXITransaction*>	mActiveWrite;	
	std::vector<AXITransaction*>	mActiveRead;

	AXITransaction			mEmptyTransactionAW;
	AXITransaction			mEmptyTransactionAR;
	AXITransaction			mEmptyTransactionW;
	
	AXITransaction*			mDriveBufAW;
	AXITransaction*			mDriveBufAR;
	AXITransaction*			mDriveBufW;
	MxU32					mDriveChannelCount;

	MxU32					mDataBitWidth;
	MxU32					mDataNumMxU32;

	std::vector<AddressRegion>	mAddressRegions;

#ifdef CARBON
	CarbonDebugAccessCallbacks mDbaCb;
#endif

#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
    MxMemoryMapConstraints  puMemoryMapConstraints;
#endif
};

class MxAXIPortT2S_Conv : public MxAXIPortT2S
{	
public:
#ifdef CARBON
  MxAXIPortT2S_Conv (sc_mx_module* owner, const string& name, const CarbonDebugAccessCallbacks& dbaCb );
#else
	MxAXIPortT2S_Conv (sc_mx_module* owner, const string& name);
#endif
	virtual ~MxAXIPortT2S_Conv ();

	void setSignalMasterPort	(MxU32 id, CarbonSignalMasterPort* port);
	void setSlaveSignal			(MxU32 id, MxU32 value, MxU32* extValues);
    void init                   (MxU32 dataBitWidth, MxU32 addrBitWidth, MxU32 numAddressRegions = 0, MxU64* start = NULL, MxU64* size = NULL, string* name = NULL);
	void reset					();

	void communicate			();
	void update					();


    MxU32                       getAddrBitWidth () const { return mAddrBitWidth; }
    MxU32                       getAddrNumMxU32 () const { return mAddrNumMxU32; }

private:
	void set_aw			(AXITransaction* axi);
	void set_ar			(AXITransaction* axi);
	void set_w			(AXITransaction* axi);	
	void set_bready		(MxU32 ready);
	void set_rready		(MxU32 ready);

	CarbonSignalMasterPort* mSPorts[AXI_NUM_SIGNALS];
	bool				mSet   [AXI_NUM_SIGNALS];
	MxU32				mValues[AXI_NUM_SIGNALS];
	MxU32				mDataBufferRead [AXI_NUM_MXU32_DATA];
	MxU32				mDataBufferWrite[AXI_NUM_MXU32_DATA];

    MxU32               mAddrBitWidth;
    MxU32               mAddrNumMxU32;
};

class MxAXIPortT2S_Trace : public MxAXIPortT2S
{	
public:
#ifdef CARBON
  MxAXIPortT2S_Trace (sc_mx_module* owner, const string& name, const CarbonDebugAccessCallbacks& dbaCb );
#else
	MxAXIPortT2S_Trace (sc_mx_module* owner, const string& name);
#endif
	virtual ~MxAXIPortT2S_Trace ();

	void setPin			(MxU32 id, MxU32* pin);
	void setPinRef		(MxU32 id, MxU32* pin);
	
	virtual void init	(MxU32 dataBitWidth, MxU32 numAddressRegions = 0, MxU64* start = NULL, MxU64* size = NULL, string* name = NULL, bool mergeData = false);

	void communicate	();
	void update			();

	// methods to use by MxAXIPortT2S	
	void set_aw			(AXITransaction* axi);
	void set_ar			(AXITransaction* axi);
	void set_w			(AXITransaction* axi);	
	void set_bready		(MxU32 ready);
	void set_rready		(MxU32 ready);

private:
	MxU32*	mPins	[AXI_NUM_SIGNALS];
	MxU32*  mPinsRef[AXI_NUM_SIGNALS];

	bool	mMergeData;
};


#endif // _MxAXISignals_
