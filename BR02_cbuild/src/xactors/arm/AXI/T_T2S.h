// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2006 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#ifndef T_T2S__H
#define T_T2S__H

#include "maxsim.h"
#include "MxSaveRestore.h"

#include "xactors/arm/include/carbon_arm_adaptor.h"

#include "MxPortCheck.h"

#ifdef CARBON
#include <stdarg.h>
#include "CarbonAdaptorBase.h"
#include "util/UtCLicense.h"
#define ADAPTOR_BASE CarbonAdaptorBase
#else
#define ADAPTOR_BASE sc_mx_module
#endif


template <class SIGNALS, class TSLAVE> class T_T2S : public ADAPTOR_BASE
    , public MxSaveRestore
{

public: 
    // constructor / destructor
#ifdef CARBON
  T_T2S(sc_mx_module* carbonComp, const string &name, 
        const string &compVersion, CarbonObjectID **carbonObj, CarbonPortFactory *portFactory,
	const CarbonDebugAccessCallbacks& dbaCb);
#else
    T_T2S (const string& name, sc_mx_m_base* c, const string &s, const string &compVersion, const char* featureStrIndi, const char* featureStrPool, const char* versionStr);
#endif
    virtual ~T_T2S ();

    // Implementation of MxSaveRestore interface methods
    virtual bool            saveData    (MxODataStream &data);
    virtual bool            restoreData (MxIDataStream &data);


    // overloaded sc_mx_module methods
    string                  getName     ();
    void                    setPortWidth(const char* name, uint32_t bitWidth);
    void                    setParameter(const string &name, const string &value);
    string                  getProperty (MxPropertyType property);
    void                    init        ();
    void                    terminate   ();
    void                    reset       (MxResetLevel level, const MxFileMapIF *filelist);

    void                    communicate ();
    void                    update      ();


private:
    string                  mName;

    bool                    initComplete; 
    bool                    p_enableDbgMsg;
    MxU32                   p_DataBitWidth;
    MxU32                   p_AddrBitWidth;
    MxU64                   p_start[6];
    MxU64                   p_size[6];

    TSLAVE*                 mTPort;

#ifdef CARBON
 public:
#endif 
    CarbonSignalMasterPort** mMSignalPorts;
    T_SignalSlave<TSLAVE>**  mSSignalPorts;

    // component version
    string                  mCompVersion;
};



template <class SIGNALS, class TSLAVE>
#ifdef CARBON
  T_T2S<SIGNALS, TSLAVE>::T_T2S(sc_mx_module* carbonComp, const string &name, const string &compVersion, CarbonObjectID **carbonObj, CarbonPortFactory *portFactory,
				const CarbonDebugAccessCallbacks& dbaCb)
     : CarbonAdaptorBase(carbonComp, name, carbonObj)
     
#else
T_T2S<SIGNALS, TSLAVE>::T_T2S(const string& name, sc_mx_m_base* c, const string &s, const string &compVersion) : sc_mx_module(c, s)
#endif
{
    initComplete = false;

    mName = name;
    mCompVersion = compVersion;
    string bn = SIGNALS::getBaseName();

#ifndef CARBON
    // Register ourself as a clock slave port.
    SC_MX_CLOCKED();
    registerPort( dynamic_cast< sc_mx_clock_slave_p_base* >( this ), "clk-in");
#endif

#ifdef CARBON
    mTPort = new TSLAVE(mCarbonComponent, name.c_str(), dbaCb);
    registerPort(mTPort, name.c_str());
#else
    mTPort = new TSLAVE(this, bn + "_s");
    registerPort(mTPort, bn + "_s");
    
#endif	
    mMSignalPorts = new CarbonSignalMasterPort* [SIGNALS::getNumSignals()];
    mSSignalPorts = new T_SignalSlave<TSLAVE>* [SIGNALS::getNumSignals()];

    MxU32               id;
    MxSignalProperties  sp;

    for (id = 0; id < SIGNALS::getNumSignals(); id++)
    {
        if (SIGNALS::isMaster(id))
        {
            mSSignalPorts[id] = NULL;
#ifdef CARBON
            mMSignalPorts[id] = portFactory(mCarbonComponent, name.c_str(), SIGNALS::getName(id).c_str(), mpCarbonObject);
#else
            mMSignalPorts[id] = new MxSignalMasterPort(SIGNALS::getName(id));   
            registerPort(mMSignalPorts[id], SIGNALS::getName(id));
#endif	
            mTPort->setSignalMasterPort(id, mMSignalPorts[id]);

            // set properties
            sp = *(mMSignalPorts[id]->getProperties());
            sp.bitwidth = SIGNALS::getBitWidth(id);
            mMSignalPorts[id]->setProperties(&sp);      
        }
        else
        {
            mMSignalPorts[id] = NULL;
            mSSignalPorts[id] = new T_SignalSlave<TSLAVE>(SIGNALS::getName(id), id, mTPort);
            registerPort(mSSignalPorts[id], SIGNALS::getName(id));

            // set properties
            sp = *(mSSignalPorts[id]->getProperties());
            sp.bitwidth = SIGNALS::getBitWidth(id);
            mSSignalPorts[id]->setProperties(&sp);      
        }
    }


    // Place parameter definitions here:
    defineParameter("Enable Debug Messages", "false",       MX_PARAM_BOOL);
    defineParameter("Data Width",        "32",          MX_PARAM_VALUE, false);
    defineParameter("Address Width",     "32",          MX_PARAM_VALUE, false);
    defineParameter(bn + "_start0",          "0x00000000",  MX_PARAM_VALUE, false);
    defineParameter(bn + "_size0",           "0x100000000", MX_PARAM_VALUE, false);
    defineParameter(bn + "_start1",          "0x00000000",  MX_PARAM_VALUE, false);
    defineParameter(bn + "_size1",           "0x0",         MX_PARAM_VALUE, false);
    defineParameter(bn + "_start2",          "0x00000000",  MX_PARAM_VALUE, false);
    defineParameter(bn + "_size2",           "0x0",         MX_PARAM_VALUE, false);
    defineParameter(bn + "_start3",          "0x00000000",  MX_PARAM_VALUE, false);
    defineParameter(bn + "_size3",           "0x0",         MX_PARAM_VALUE, false);
    defineParameter(bn + "_start4",          "0x00000000",  MX_PARAM_VALUE, false);
    defineParameter(bn + "_size4",           "0x0",         MX_PARAM_VALUE, false);
    defineParameter(bn + "_start5",          "0x00000000",  MX_PARAM_VALUE, false);
    defineParameter(bn + "_size5",           "0x0",         MX_PARAM_VALUE, false);
}

template <class SIGNALS, class TSLAVE>
T_T2S<SIGNALS, TSLAVE>::~T_T2S()
{
    MxU32 i;
    
    for (i = 0; i < SIGNALS::getNumSignals(); i++)
    {
        if (mMSignalPorts[i] != NULL)
        {
            delete mMSignalPorts[i];
        }
        else
        {
            delete mSSignalPorts[i];
        }
    }
    delete []mMSignalPorts;
    delete []mSSignalPorts;

    delete mTPort;
}


template <class SIGNALS, class TSLAVE>
void T_T2S<SIGNALS, TSLAVE>::init()
{
    MxU32 i;
    
    // Initialize ourself for save/restore.
    // initStream( this );

    // Allocate memory and initialize data here:
    string names[6] = { "Range_0", "Range_1", "Range_2", "Range_3", "Range_4", "Range_5" };
    mTPort->init(p_DataBitWidth, p_AddrBitWidth, 6, p_start, p_size, names);
  
    MxSignalProperties sp;
    for (i = 0; i < SIGNALS::getNumSignals(); i++)
    {       
        if (SIGNALS::isConfigurableDataBitWidth(i))
        {
            if (mMSignalPorts[i] != NULL)
            {
                sp = *(mMSignalPorts[i]->getProperties());
                mMSignalPorts[i]->setProperties(&sp);
            }
            else
            {
                sp = *(mSSignalPorts[i]->getProperties());
                sp.bitwidth = p_DataBitWidth;
                mSSignalPorts[i]->setProperties(&sp);
            }
        }
    }

    // Call the base class after this has been initialized.
    ADAPTOR_BASE::init();

    // Set a flag that init stage has been completed
    initComplete = true;
}

template <class SIGNALS, class TSLAVE> 
void T_T2S<SIGNALS, TSLAVE>::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
    // Add your reset behavior here:
    // ...
    MxPortCheck::check(mTPort);
    mTPort->reset();    

    // Call the base class after this has been reset.
    ADAPTOR_BASE::reset(level, filelist);
}

template <class SIGNALS, class TSLAVE>
void T_T2S<SIGNALS, TSLAVE>::terminate()
{
    // Call the base class first.
    ADAPTOR_BASE::terminate();

    mTPort->terminate();
}

template <class SIGNALS, class TSLAVE>
void T_T2S<SIGNALS, TSLAVE>::setPortWidth(const char* name, uint32_t bitWidth)
{
  if (strcmp(name, "wdata") == 0 || strcmp(name, "rdata") == 0) {
    p_DataBitWidth = bitWidth;
  }
  else if (strcmp(name, "araddr") == 0 || strcmp(name, "awaddr") == 0) {
    p_AddrBitWidth = bitWidth;
    if (p_AddrBitWidth > 64) {
      message( MX_MSG_WARNING, "T_T2S::setPortWidth: Cannot set port <%s> width higher than 64. Using 64 instead.", name );
      p_AddrBitWidth = 64;
    }
  }
}

template <class SIGNALS, class TSLAVE>
void T_T2S<SIGNALS, TSLAVE>::setParameter( const string &name, const string &value )
{
    MxConvertErrorCodes status = MxConvert_SUCCESS;
    string bn = SIGNALS::getBaseName();

    if (name == "Enable Debug Messages")
    {
        status = MxConvertStringToValue( value, &p_enableDbgMsg  );
    }
    else if (name == "Data Width")
    {
        if ( initComplete == false )
        {
            status = MxConvertStringToValue( value, &p_DataBitWidth  );
        }
        else
        {
            message( MX_MSG_WARNING, "T_T2S::setParameter: Cannot change parameter <%s> at runtime. Assignment ignored.", name.c_str() );
            return;
        }
    }
    else if (name == "Address Width")
    {
        if ( initComplete == false )
        {
            status = MxConvertStringToValue( value, &p_AddrBitWidth  );
            if (status == MxConvert_SUCCESS)
            {
                if (p_AddrBitWidth > 64)
                {
                    message( MX_MSG_WARNING, "T_S2T::setParameter: Cannot set parameter <%s> higher than 64. Using 64 instead.", name.c_str() );
                    p_AddrBitWidth = 64;
                }
            }
        }
        else
        {
            message( MX_MSG_WARNING, "T_T2S::setParameter: Cannot change parameter <%s> at runtime. Assignment ignored.", name.c_str() );
            return;
        }
    }
    else if (string::npos != name.find(bn + "_start"))
    {
        if ( initComplete == false )
        {   
            MxU32 nr;
            if (sscanf(name.c_str(), (bn + "_start%d").c_str(), &nr))
                status = MxConvertStringToValue( value, &p_start[nr] );
            else
                status = MxConvert_ILLEGAL_VALUE;
        }
        else
        {
            message( MX_MSG_WARNING, "T_T2S::setParameter: Cannot change parameter <%s> at runtime. Assignment ignored.", name.c_str() );
            return;
        }
    }
    else if (string::npos != name.find(bn + "_size"))
    {
        if ( initComplete == false )
        {
            MxU32 nr;
            if (sscanf(name.c_str(), (bn + "_size%d").c_str(), &nr))
                status = MxConvertStringToValue( value, &p_size[nr] );
            else
                status = MxConvert_ILLEGAL_VALUE;
        }
        else
        {
            message( MX_MSG_WARNING, "T_T2S::setParameter: Cannot change parameter <%s> at runtime. Assignment ignored.", name.c_str() );
            return;
        }
    }

    if ( status == MxConvert_SUCCESS )
    {
        ADAPTOR_BASE::setParameter(name, value);
    }
    else
    {
        message( MX_MSG_WARNING, "T_T2S::setParameter: Illegal value <%s> "
         "passed for parameter <%s>. Assignment ignored.", value.c_str(), name.c_str() );
    }
}

template <class SIGNALS, class TSLAVE> 
string T_T2S<SIGNALS, TSLAVE>::getProperty( MxPropertyType property )
{
    string bn = SIGNALS::getBaseName();
    string description; 
    switch ( property ) 
    {    
     case MX_PROP_IP_PROVIDER:
         return "ARM";
     case MX_PROP_LOADFILE_EXTENSION:
        return "";
     case MX_PROP_REPORT_FILE_EXT:
        return "yes";
     case MX_PROP_COMPONENT_TYPE:
        return MxComponentTypes[MX_TYPE_TRANSACTOR];
     case MX_PROP_COMPONENT_VERSION:
        return mCompVersion;
     case MX_PROP_MSG_PREPEND_NAME:
        return "yes"; 
     case MX_PROP_DESCRIPTION:
        description = "MxSI3.0 " +bn+ " transaction slave to MxSI " +bn+ " signal converter\n";
        return description + "Compiled on " + __DATE__ + ", " + __TIME__; 
     case MX_PROP_MXDI_SUPPORT:
        return "no";
     case MX_PROP_SAVE_RESTORE:
        return "no";
     default:
        return "";
    }
    return "";
}

template <class SIGNALS, class TSLAVE>
string T_T2S<SIGNALS, TSLAVE>::getName(void)
{
    return mName;
}


template <class SIGNALS, class TSLAVE>
bool T_T2S<SIGNALS, TSLAVE>::saveData( MxODataStream &data )
{
    return false;
}

template <class SIGNALS, class TSLAVE>
bool T_T2S<SIGNALS, TSLAVE>::restoreData( MxIDataStream &data )
{
    return false;
}

template <class SIGNALS, class TSLAVE>
void T_T2S<SIGNALS, TSLAVE>::communicate ()
{
    mTPort->communicate();
}

template <class SIGNALS, class TSLAVE>
void T_T2S<SIGNALS, TSLAVE>::update ()
{
    mTPort->update();
}



#endif
