#ifndef _carbon_axi_h_
#define _carbon_axi_h_

#include "util/CarbonPlatform.h"
#include "carbon_arm_adaptor.h"

#ifdef CARBON
#define CarbonSignalMasterPort CarbonXtorAdaptorToVhmPort
#else
#define CarbonSignalMasterPort MxSignalMasterPort
#endif

#endif // _carbon_axi_h_
