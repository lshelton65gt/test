
#include "CarbonAxiS2T.h"

#define CARBON 1
#include "MxAXISignals.h"
#include "T_S2T.h"

typedef T_S2T<AXISignals, MxAXIPortS2T_Conv> AXIS2T;

CarbonAxiS2T::CarbonAxiS2T(sc_mx_module* carbonComp,
                                     const char *xtorName,
                                     CarbonObjectID **carbonObj,
                                     CarbonPortFactory *portFactory)
{
  mArmAdaptor = new AXIS2T(carbonComp, xtorName, "2.1.002", carbonObj, portFactory);
}

sc_mx_signal_slave *CarbonAxiS2T::carbonGetPort(const char *name)
{
  sc_mx_signal_slave *port;

  if (strcmp("awid", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_AWID];
  }
  else if (strcmp("awaddr", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_AWADDR];
  }
  else if (strcmp("awlen", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_AWLEN];
  }
  else if (strcmp("awsize", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_AWSIZE];
  }
  else if (strcmp("awburst", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_AWBURST];
  }
  else if (strcmp("awlock", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_AWLOCK];
  }
  else if (strcmp("awcache", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_AWCACHE];
  }
  else if (strcmp("awprot", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_AWPROT];
  }
  else if (strcmp("awuser", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_AWUSER];
  }
  else if (strcmp("awvalid", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_AWVALID];
  }
  else if (strcmp("wid", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_WID];
  }
  else if (strcmp("wdata", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_WDATA];
  }
  else if (strcmp("wstrb", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_WSTRB];
  }
  else if (strcmp("wlast", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_WLAST];
  }
  else if (strcmp("wuser", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_WUSER];
  }
  else if (strcmp("wvalid", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_WVALID];
  }
  else if (strcmp("bready", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_BREADY];
  }
  else if (strcmp("arid", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_ARID];
  }
  else if (strcmp("araddr", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_ARADDR];
  }
  else if (strcmp("arlen", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_ARLEN];
  }
  else if (strcmp("arsize", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_ARSIZE];
  }
  else if (strcmp("arburst", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_ARBURST];
  }
  else if (strcmp("arlock", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_ARLOCK];
  }
  else if (strcmp("arcache", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_ARCACHE];
  }
  else if (strcmp("arprot", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_ARPROT];
  }
  else if (strcmp("aruser", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_ARUSER];
  }
  else if (strcmp("arvalid", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_ARVALID];
  }
  else if (strcmp("rready", name) == 0) {
    port = ((AXIS2T *) mArmAdaptor)->mSSignalPorts[AXI_RREADY];
  }
  else {
    port = NULL;
    ((AXIS2T *) mArmAdaptor)->message(MX_MSG_FATAL_ERROR, "Carbon AXI S2T has no port named \"%s\"", name);
  }
  return port;
}

CarbonAxiS2T::~CarbonAxiS2T()
{
  delete ((AXIS2T *) mArmAdaptor);
}

void CarbonAxiS2T::communicate()
{
  ((AXIS2T *) mArmAdaptor)->communicate();
}

void CarbonAxiS2T::update()
{
  ((AXIS2T *) mArmAdaptor)->update();
}

void CarbonAxiS2T::init()
{
  ((AXIS2T *) mArmAdaptor)->init();
}

void CarbonAxiS2T::terminate()
{
  ((AXIS2T *) mArmAdaptor)->terminate();
}

void CarbonAxiS2T::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  ((AXIS2T *) mArmAdaptor)->reset(level, filelist);
}

void CarbonAxiS2T::setParameter(const string &name, const string &value)
{
  ((AXIS2T *) mArmAdaptor)->setParameter(name, value);
}

MxStatus CarbonAxiS2T::readDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return ((AXIS2T *) mArmAdaptor)->readDbg(addr, value, ctrl);
}

MxStatus CarbonAxiS2T::writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return ((AXIS2T *) mArmAdaptor)->writeDbg(addr, value, ctrl);
}

MxStatus CarbonAxiS2T::debugTransaction(MxTransactionInfo* info)
{
  return ((AXIS2T *) mArmAdaptor)->debugTransaction(info);
}

int CarbonAxiS2T::getNumRegions()
{
  return ((AXIS2T *) mArmAdaptor)->getNumRegions();
}

void CarbonAxiS2T::getAddressRegions (MxU64* start, MxU64* size, string * name)
{
  ((AXIS2T *) mArmAdaptor)->getAddressRegions(start, size, name);
}

MxStatus CarbonAxiS2T::debugMemWrite(MxU64 startAddress, MxU32 unitsToWrite, MxU32 unitSizeInBytes, const MxU8 *data, MxU32 *actualNumOfUnitsWritten, MxU32 secureFlag)
{
  return ((AXIS2T *) mArmAdaptor)->debugMemWrite(startAddress, unitsToWrite, unitSizeInBytes, data, actualNumOfUnitsWritten, secureFlag);
}

MxStatus CarbonAxiS2T::debugMemRead(MxU64 startAddress, MxU32 unitsToRead, MxU32 unitSizeInBytes, MxU8 *data, MxU32 *actualNumOfUnitsRead, MxU32 secureFlag)
{
  return ((AXIS2T *) mArmAdaptor)->debugMemRead(startAddress, unitsToRead, unitSizeInBytes, data, actualNumOfUnitsRead, secureFlag);
}

CarbonDebugAccessStatus CarbonAxiS2T::debugAccess(CarbonDebugAccessDirection dir, MxU64 startAddress, MxU32 numBytes, MxU8 *data, MxU32 *numBytesProcessed, MxU32 *ctrl)
{
  MxStatus status;
  MxU32 secureFlag = 0;
  if (ctrl) {
    secureFlag = ctrl[0]&0x1;
  }
  if ( dir == eCarbonDebugAccessRead ){
    status = ((AXIS2T *) mArmAdaptor)->debugMemRead( startAddress, 1, numBytes, data, numBytesProcessed, secureFlag);
  } else {
    status = ((AXIS2T *) mArmAdaptor)->debugMemWrite( startAddress, 1, numBytes, data, numBytesProcessed, secureFlag);
  }
  if (status == MX_STATUS_OK)
    return eCarbonDebugAccessStatusOk;
  else
    return eCarbonDebugAccessStatusError;
}

void CarbonAxiS2T::carbonSetPortWidth(const char* name, uint32_t bitWidth)
{
  ((AXIS2T *) mArmAdaptor)->setPortWidth(name, bitWidth);
}
