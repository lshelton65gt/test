// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.


#include "MxAXISignals.h"

template<>
const T_Signals<AXI_NUM_SIGNALS, AXI_NUM_MASTERS>::Info T_Signals<AXI_NUM_SIGNALS, AXI_NUM_MASTERS>::mInfo[AXI_NUM_SIGNALS] =
{
	{ true,		32,	false,	"awid"		},
	{ true,		32,	true,	"awaddr"	},
	{ true,		 4, false,	"awlen"		},
	{ true,		 3, false,	"awsize"	},
	{ true,		 2, false,	"awburst"	},
	{ true,		 2, false,	"awlock"	},
	{ true,		 4, false,	"awcache"	},
	{ true,		 3, false,	"awprot"	},
	{ true,		32, false,	"awuser"	},
	{ true,		 1, false,	"awvalid"	},
	{ false,	 1, false,	"awready"	},

	{ true,		32, false,	"wid"		},
	{ true,	    32, true,	"wdata"		},
	{ true,		32, false,	"wstrb"		},
	{ true,		 1, false,	"wlast"		},
	{ true,		32, false,	"wuser"		},
	{ true,		 1, false,	"wvalid"	},
	{ false,	 1, false,	"wready"	},

	{ false,	32, false,	"bid"		},
	{ false,	 2, false,	"bresp"		},
	{ false,	32, false,	"buser"		},
	{ false,	 1, false,	"bvalid"	},
	{ true,		 1, false,	"bready"	},

	{ true,		32, false,	"arid"		},
	{ true,		32, true,	"araddr"	},
	{ true,		 4, false,	"arlen"		},
	{ true,		 3, false,	"arsize"	},
	{ true,		 2, false,	"arburst"	},
	{ true,		 2, false,	"arlock"	},
	{ true,		 4, false,	"arcache"	},
	{ true,		 3, false,	"arprot"	},
	{ true,		32, false,	"aruser"	},
	{ true,		 1, false,	"arvalid"	},
	{ false,	 1, false,	"arready"	},

	{ false,	32, false,	"rid"		},
	{ false,	32, true,	"rdata"		},
	{ false,	 2, false,	"rresp"		},
	{ false,	 1, false,	"rlast"		},
	{ false,	32, false,	"ruser"		},
	{ false,	 1, false,	"rvalid"	},
	{ true,		 1, false,	"rready"	}
};

template<>
const string T_Signals<AXI_NUM_SIGNALS, AXI_NUM_MASTERS>::mBaseName	= string("axi");

#if 0 // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void AXISignals::convertDataMx2Hw (const AXITransaction* axi, const MxU32* srcData, MxU32* dstData, MxU32 srcStrb, MxU32& dstStrb) 
{
	MxU32 dataBitWidth, busBitWidth, address, shl;

	dataBitWidth	= axi->dataSize << 3;
	busBitWidth		= axi->getDataBitwidth();
	address			= axi->get_addrOfCurrentBeat();

	switch (busBitWidth)
	{
	case 32:
	{
		MxU32 mask, tmp;

		shl = (address & 3);
		dstStrb = srcStrb << shl;
		shl<<= 3;
		dataBitWidth-= (((dataBitWidth >> 3)-1) & address) << 3;			
		mask = dataBitWidth < 32 ? ((MxU32)(-1)) << dataBitWidth : 0;
		mask = ~mask << shl;
		tmp = (srcData[0] << shl) & mask;
		tmp = (dstData[0] & ~mask) | tmp;
		dstData[0] = tmp;
		break;
	}
	case 64:
	{
		MxU64 mask, src, dst;

		shl = (address & 7);
		dstStrb = srcStrb << shl;
		shl<<= 3;
		dataBitWidth-= (((dataBitWidth >> 3)-1) & address) << 3;			
		mask = dataBitWidth < 64 ? MxU64CONST(0xFFFFFFFFFFFFFFFF) << dataBitWidth : 0;
		mask = ~mask << shl;

		src = ((MxU64)(srcData[1])) << 32;
		src|= ((MxU64)(srcData[0]));		
		src = (src << shl) & mask;

		dst = ((MxU64)(dstData[1])) << 32;
		dst|= ((MxU64)(dstData[0]));		
		dst = (dst & ~mask) | src;

		dstData[0] = dst;
		dstData[1] = dst >> 32;
		break;
	}
	default:
		assert(0);
		break;
	}
}

void AXISignals::convertDataHw2Mx (const AXITransaction* axi, const MxU32* srcData, MxU32* dstData, MxU32 srcStrb, MxU32& dstStrb) 
{
	MxU32 dataBitWidth, busBitWidth, address, shr;

	dataBitWidth	= axi->dataSize << 3;
	busBitWidth		= axi->getDataBitwidth();
	address			= axi->get_addrOfCurrentBeat();

	switch (busBitWidth)
	{
	case 32:
	{
		shr = (address & 3);
		dstStrb = srcStrb >> shr;
		shr<<= 3;
		dstData[0] = srcData[0] >> shr;
		break;
	}
	case 64:
	{
		MxU64 tmp;
		
		shr = (address & 7);
		dstStrb = srcStrb >> shr;
		shr<<= 3;
		tmp = (((MxU64)(srcData[1])) << 32) | srcData[0];
		tmp>>= shr;
		dstData[0] = tmp;
		dstData[1] = tmp >> 32;
		break;
	}
	default:
		assert(0);
		break;
	}
}
#endif //0 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


void AXISignals::copyData (const AXITransaction* axi, const MxU32* srcData, MxU32* dstData)
{
	for (MxU32 i = 0; i < axi->getDataMxU32width(); i++)
	{
		dstData[i] = srcData[i];
	}
}

void AXISignals::mergeData (const AXITransaction* axi, const MxU32* srcData, const MxU32* refData, MxU32* dstData)
{
	MxU64 dataBitWidth, busBitWidth, address;/*, shl*/;

	dataBitWidth	= axi->dataSize << 3;
	busBitWidth		= axi->getDataBitwidth();
	address			= axi->get_addrOfCurrentBeat();

	switch (busBitWidth)
	{
	case 32:
	{
		MxU32 mask, tmp;

		//shl = (address & 3);
		//dstStrb = srcStrb << shl;
		//shl<<= 3;
		dataBitWidth-= (((dataBitWidth >> 3)-1) & address) << 3;			
		mask = dataBitWidth < 32 ? ((MxU32)(-1)) << dataBitWidth : 0;
		mask = ~mask /*<< shl*/;
		tmp = srcData[0] & mask;
		tmp = (refData[0] & ~mask) | tmp;
		dstData[0] = tmp;
		break;
	}
	case 64:
	{
		MxU64 mask, src, dst;

		//shl = (address & 7);
		//dstStrb = srcStrb << shl;
		//shl<<= 3;
		dataBitWidth-= (((dataBitWidth >> 3)-1) & address) << 3;			
		mask = dataBitWidth < 64 ? MxU64CONST(0xFFFFFFFFFFFFFFFF) << dataBitWidth : 0;
		mask = ~mask /*<< shl*/;

		src = ((MxU64)(srcData[1])) << 32;
		src|= ((MxU64)(srcData[0]));		
		src = src & mask;

		dst = ((MxU64)(refData[1])) << 32;
		dst|= ((MxU64)(refData[0]));		
		dst = (dst & ~mask) | src;

		dstData[0] = MxU32(dst);
		dstData[1] = MxU32(dst >> 32);
		break;
	}
	default:
		assert(!"Invalid buswidth!");
		break;
	}

}
