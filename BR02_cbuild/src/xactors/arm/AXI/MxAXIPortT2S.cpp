

#include "MxAXISignals.h"
#include "carbon_axi.h"

// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
//
//	MxAXIPortT2S
//
// ----------------------------------------------------------------------------------------------------------------------

#ifdef CARBON
MxAXIPortT2S::MxAXIPortT2S (sc_mx_module* owner, const string& name, const CarbonDebugAccessCallbacks& dbaCb)
  : MxTransactionSlave(owner, name), mDbaCb(dbaCb)
#else
MxAXIPortT2S::MxAXIPortT2S (sc_mx_module* owner, const string& name)
  : MxTransactionSlave(owner, name)
#endif
{
	setMxOwner(owner);

#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
    initMemoryMapConstraints();
#endif
}

MxAXIPortT2S::~MxAXIPortT2S()
{
}

MxStatus MxAXIPortT2S::read (MxU64 addr, MxU32* value, MxU32* ctrl)
{
	return MX_STATUS_NOTSUPPORTED;
}

MxStatus MxAXIPortT2S::write (MxU64 addr, MxU32* value, MxU32* ctrl)
{
	return MX_STATUS_NOTSUPPORTED;
}

MxStatus MxAXIPortT2S::readDbg (MxU64 addr, MxU32* value, MxU32* ctrl)
{
#if CARBON
        return CarbonDebugFunctionsToDebugAccess(mDbaCb, eCarbonDebugAccessRead, getDataBitWidth(), addr, value, ctrl);
#else
	return MX_STATUS_NOTSUPPORTED;
#endif
}

MxStatus MxAXIPortT2S::writeDbg	(MxU64 addr, MxU32* value, MxU32* ctrl)
{
#if CARBON
        return CarbonDebugFunctionsToDebugAccess(mDbaCb, eCarbonDebugAccessWrite, getDataBitWidth(), addr, value, ctrl);
#else
	return MX_STATUS_NOTSUPPORTED;
#endif
}

MxStatus MxAXIPortT2S::readReq (MxU64 addr, MxU32* value, MxU32* ctrl, MxTransactionCallbackIF* callback)
{
	return read(addr, value, ctrl);
}

MxStatus MxAXIPortT2S::writeReq	(MxU64 addr, MxU32* value, MxU32* ctrl, MxTransactionCallbackIF* callback)
{
	return write(addr, value, ctrl);
}

MxGrant	MxAXIPortT2S::requestAccess(MxU64 addr)
{
	return MX_GRANT_OK;	
}

MxGrant	MxAXIPortT2S::checkForGrant(MxU64 addr)
{
	return MX_GRANT_OK;
}


void MxAXIPortT2S::driveTransaction	(MxTransactionInfo* info)
{
	if (!mNextCtsAlreadyDone)
	{
		containerAXITransactionNextCts.adoptAllNextCts();
		mNextCtsAlreadyDone = true;
	}

	AXITransaction* axi = static_cast<AXITransaction*>(info);
	
	if (axi->isChannelAW())
	{
		if (axi->isValidTransaction())
		{
			mActiveAW = axi;
			addTransaction(mActiveWrite, axi);
 			set_aw(axi);	
		}
		else
		{
			set_aw(NULL);
		}
		mWasTransactionAW = true;
	}
	if (axi->isChannelW())
	{
		if (axi->isValidTransaction())
		{
			mActiveW = axi;
			addTransaction(mActiveWrite, axi);
			set_w(axi);
		}
		else
		{
			set_w(NULL);
		}
		mWasTransactionW = true;
	}
	if (axi->isChannelAR())
	{
		if (axi->isValidTransaction())
		{
			mActiveAR = axi;
			addTransaction(mActiveRead, axi);
			set_ar(axi);	
		}
		else
		{
			set_ar(NULL);
		}
		mWasTransactionAR = true;
	}

	if (!axi->isLastCall())
	{
		return;
	}

	if (!mWasTransactionAW)
	{
		set_aw(NULL);
		mWasTransactionAW = true;
	}		
	if (!mWasTransactionW)
	{
		set_w(NULL);
		mWasTransactionW = true;
	}
	if (!mWasTransactionAR)
	{
		set_ar(NULL);
		mWasTransactionAR = true;
	}
}

void MxAXIPortT2S::addTransaction (std::vector<AXITransaction*>& buffer, AXITransaction* axi)
{
	MxU32 i;
	for (i = 0; i < buffer.size(); i++)
	{
		if (buffer[i] == axi)
		{
			return;
		}
	}
	buffer.push_back(axi);
}

void MxAXIPortT2S::remTransaction (std::vector<AXITransaction*>& buffer, MxU32 id)
{
	std::vector<AXITransaction*>::iterator pos;

	for (pos = buffer.begin(); pos != buffer.end(); pos++)
	{
		if ((*pos)->get_id() == id)
		{
			buffer.erase(pos);
			return;
		}
	}
}

AXITransaction* MxAXIPortT2S::getTransaction (std::vector<AXITransaction*>& buffer, MxU32 id) const
{
	MxU32 i;
	for (i = 0; i < buffer.size(); i++)
	{
		if (buffer[i]->get_id() == id)
		{
			return buffer[i];
		}
	}
	return NULL;
}

void MxAXIPortT2S::set_awready (MxU32 value)
{
	if (mActiveAW != NULL && value > 0)
	{
		mActiveAW->status[AXI_STEP_ADDRESS] = MX_SLAVE_READY;

		// handle aw finished after last w step finished
		if ((mActiveAW->cts == (mActiveAW->nts -2)) && mActiveAW->isActiveReady())
		{
			containerAXITransactionNextCts.add(mActiveAW);
		}

		mActiveAW = NULL;
		mClearValidAW = true;
	}
}
	
void MxAXIPortT2S::set_arready (MxU32 value)
{
	if (mActiveAR != NULL && value > 0)
	{
		mActiveAR->status[AXI_STEP_ADDRESS] = MX_SLAVE_READY;
//r		mActiveAR->foreignFlags() |= FLAGS_INCR_CTS;
		containerAXITransactionNextCts.add(mActiveAR);
		mActiveAR = NULL;
		mClearValidAR = true;
	}
}

void MxAXIPortT2S::set_wready (MxU32 value)
{
	if (mActiveW != NULL && value > 0)
	{
		mActiveW->status[mActiveW->cts] = MX_SLAVE_READY;
		if (mActiveW->get_wlast())
		{
//r			mActiveW->foreignFlags() |= FLAGS_INCR_CTS;
			containerAXITransactionNextCts.add(mActiveW);
		}
		mActiveW = NULL;
		mClearValidW = true;
	}
}

void MxAXIPortT2S::set_bvalid (MxU32 id, MxU32 resp, MxU32 user, MxU32 valid)
{
	AXITransaction* axi = getTransaction(mActiveWrite, id);
	if ((axi != NULL) && (valid > 0))
	{
//r		if (axi->foreignFlags() & FLAGS_INCR_CTS)
//r		{
//r			axi->cts++;
			axi->masterFlags[AXI_MF_CHANNEL] = AXI_CHANNEL_B;
//r			axi->foreignFlags()&= ~FLAGS_INCR_CTS;
//r		}		
		axi->set_resp(resp);
		axi->set_buser(user);
		axi->status[axi->cts] = MX_MASTER_READY;

		getMaster()->getNotifyHandler()->notifyEvent(axi);
		if (axi->status[axi->cts] >= MX_SLAVE_READY)
		{
			remTransaction(mActiveWrite, id);
			set_bready(1);
		}
		else
		{
			set_bready(0);
		}
	}
}

void MxAXIPortT2S::set_rvalid(MxU32 id, MxU32* data, MxU32 resp, MxU32 last,
                              MxU32 user, MxU32 valid)
{
	AXITransaction* axi = getTransaction(mActiveRead, id);
	if ((axi != NULL) && (valid > 0))
	{
//r		if (axi->foreignFlags() & FLAGS_INCR_CTS)
//r		{
//r			axi->cts++;
		axi->masterFlags[AXI_MF_CHANNEL] = AXI_CHANNEL_R;
//r			axi->foreignFlags()&= ~FLAGS_INCR_CTS;
//r		}

		//MxU32 dummy;
		//AXISignals::convertDataHw2Mx(axi, data, axi->getP_setReadData(axi->getCurrentBeat()), (MxU32)-1, dummy);
				
		axi->set_resp(resp);
		// rlast doesn't need to be set
		axi->set_duser(user);
		axi->status[axi->cts] = MX_MASTER_READY;
		// getCurrentBeat considers status == MX_MASTER_WAIT, so we copy data after setting status
		AXISignals::copyData(axi, data, axi->getP_setReadData(axi->getCurrentBeat()));

		getMaster()->getNotifyHandler()->notifyEvent(axi);
		if (axi->status[axi->cts] >= MX_SLAVE_READY)
		{
			if (axi->get_rlast())
			{		
				remTransaction(mActiveRead, id);
			}
			else
			{
//r				axi->foreignFlags() |= FLAGS_INCR_CTS;
				containerAXITransactionNextCts.add(axi);
			}
			set_rready(1);
		}
		else
		{
			set_rready(0);
		}
	}	
}

void MxAXIPortT2S::cancelTransaction (MxTransactionInfo*)
{
}

MxStatus MxAXIPortT2S::debugTransaction	(MxTransactionInfo* info)
{
#if CARBON
        return CarbonDebugFunctionsToDebugAccess(mDbaCb, info);
#else
	return MX_STATUS_NOTSUPPORTED;
#endif
}

int	MxAXIPortT2S::getNumRegions	()
{
	return mAddressRegions.size();
}

void MxAXIPortT2S::getAddressRegions (MxU64* start, MxU64* size, string* name)
{
	MxU32 i;

	for (i = 0; i < mAddressRegions.size(); i++)
	{
		start[i] = mAddressRegions[i].mStart;
		size [i] = mAddressRegions[i].mSize;
		name [i] = mAddressRegions[i].mName;
	}	
}

#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
void MxAXIPortT2S::initMemoryMapConstraints()
{
    MxTransactionProperties mxProps;
    AXI_INIT_TRANSACTION_PROPERTIES(mxProps, 32);
	setProperties(&mxProps); // this is required but will be overwritten
	puMemoryMapConstraints.minRegionSize = 0x0;
    puMemoryMapConstraints.maxRegionSize = (MxU64CONST(0x1) << mxProps.addressBitwidth);
    puMemoryMapConstraints.minAddress = 0x0;
    puMemoryMapConstraints.maxAddress = (MxU64CONST(0x1) << mxProps.addressBitwidth) - 1;
    puMemoryMapConstraints.minNumSupportedRegions = 0x1;
    puMemoryMapConstraints.maxNumSupportedRegions = MAX_REGIONS;
    puMemoryMapConstraints.alignmentBlockSize = 0x400;
    puMemoryMapConstraints.numFixedRegions = 0;
    puMemoryMapConstraints.fixedRegionList = NULL;
    puMemoryMapConstraints.details = NULL;
}

void MxAXIPortT2S::setAddressRegions (MxU64* start, MxU64* size, string* name)
{
	AddressRegion reg;
    if (start && size && name)
    {
		mAddressRegions.clear();
		
        for (unsigned int i = 0; i < MAX_REGIONS; i++)
        {
            if (size[i] > 0)
            {				
				reg.mStart = start[i];
				reg.mSize = size[i];
				reg.mName = name[i];
				mAddressRegions.push_back(reg);
            }
            else
            {
                break;
            }
        }
    }
}

MxMemoryMapConstraints* MxAXIPortT2S::getMappingConstraints	()
{
	return &puMemoryMapConstraints;
}
#endif

void MxAXIPortT2S::init	(MxU32 dataBitWidth, MxU32 numAddressRegions, MxU64* start, MxU64* size, string* name)
{
	mDataBitWidth = dataBitWidth;
	mDataNumMxU32 = (mDataBitWidth+31)/32;

	MxTransactionProperties prop;
	AXI_INIT_TRANSACTION_PROPERTIES(prop, mDataBitWidth);
	setProperties(&prop);

	mEmptyTransactionAW.initialize(getProperties());
	mEmptyTransactionAW.reset();
	mEmptyTransactionAW.nts = 1;
	mEmptyTransactionAW.cts = 2;
	mEmptyTransactionAW.masterFlags[AXI_MF_CHANNEL] = (AXI_CHANNEL_AW | AXI_TRANSFER_EMPTY);

	mEmptyTransactionAR.initialize(getProperties());
	mEmptyTransactionAR.reset();
	mEmptyTransactionAR.nts = 1;
	mEmptyTransactionAR.cts = 2;
	mEmptyTransactionAR.masterFlags[AXI_MF_CHANNEL] = (AXI_CHANNEL_AR | AXI_TRANSFER_EMPTY);

	mEmptyTransactionW.initialize(getProperties());
	mEmptyTransactionW.reset();
	mEmptyTransactionW.nts = 1;
	mEmptyTransactionW.cts = 2;
	mEmptyTransactionW.masterFlags[AXI_MF_CHANNEL] = (AXI_CHANNEL_W | AXI_TRANSFER_EMPTY);

	MxU32			i;
	AddressRegion	reg;

	if (numAddressRegions > 0)
	{
		assert(start && size && name);
		for (i = 0; i < numAddressRegions; i++)
		{
			if (size[i] != 0)
			{
				reg.mStart	= start[i];
				reg.mSize	= size [i];
				reg.mName	= name [i];
				mAddressRegions.push_back(reg);
			}
		}
	}
	else
	{
		reg.mStart = 0;
		reg.mSize  = MxU64CONST(0x100000000);
		reg.mName  = "";
		mAddressRegions.push_back(reg);
	}
}

void MxAXIPortT2S::reset ()
{
	mActiveAW	= NULL;
	mActiveAR	= NULL;
	mActiveW	= NULL;

	mWasTransactionAW	= false;
	mWasTransactionAR	= false;
	mWasTransactionW	= false;

	mClearValidAW			= false;
	mClearValidDelayedAW	= false;
	mClearValidAR			= false;
	mClearValidDelayedAR	= false;
	mClearValidW			= false;
	mClearValidDelayedW		= false;

	mDriveBufAW	= &mEmptyTransactionAW;
	mDriveBufW	= &mEmptyTransactionW;
	mDriveBufAR	= &mEmptyTransactionAR;
	mDriveChannelCount = 0;

	mActiveWrite.clear();	
	mActiveRead.clear();

	containerAXITransactionNextCts.reset();
	mNextCtsAlreadyDone = false;
}

void MxAXIPortT2S::terminate ()
{
}

void MxAXIPortT2S::communicate ()
{
	if (!mNextCtsAlreadyDone)
	{
		containerAXITransactionNextCts.adoptAllNextCts();
		mNextCtsAlreadyDone = true;
	}

	if (mClearValidDelayedAW && !mWasTransactionAW)
	{
		set_aw(NULL);
		mClearValidDelayedAW = false;
	}	
	if (mClearValidDelayedAR && !mWasTransactionAR)
	{
		set_ar(NULL);	
		mClearValidDelayedAR = false;
	}
	if (mClearValidDelayedW && !mWasTransactionW)
	{		set_w(NULL);
		mClearValidDelayedW = false;
	}
}

void MxAXIPortT2S::update ()
{
	mWasTransactionAW = false;
	mWasTransactionAR = false;
	mWasTransactionW  = false;

	mClearValidDelayedAW = mClearValidAW;
	mClearValidAW = false;

	mClearValidDelayedAR = mClearValidAR;
	mClearValidAR = false;

	mClearValidDelayedW = mClearValidW;
	mClearValidW = false;

	mNextCtsAlreadyDone = false;
}


// ----------------------------------------------------------------------------------------------------------------------
//
//	MxAXIPortT2S_Conv
//
// ----------------------------------------------------------------------------------------------------------------------

#ifdef CARBON
MxAXIPortT2S_Conv::MxAXIPortT2S_Conv (sc_mx_module* owner, const string& name,
                                      const CarbonDebugAccessCallbacks& dbaCb) : MxAXIPortT2S(owner, name, dbaCb)
#else
MxAXIPortT2S_Conv::MxAXIPortT2S_Conv (sc_mx_module* owner, const string& name) : MxAXIPortT2S(owner, name)
#endif

{
	MxU32 i;

	for (i = 0; i < AXI_NUM_SIGNALS; i++)
	{
		mSPorts[i] = NULL;
	}
}

MxAXIPortT2S_Conv::~MxAXIPortT2S_Conv ()
{
}

void MxAXIPortT2S_Conv::setSignalMasterPort	(MxU32 id, CarbonSignalMasterPort* port)
{
	mSPorts[id] = port;
}

void MxAXIPortT2S_Conv::setSlaveSignal (MxU32 id, MxU32 value, MxU32* extValues)
{
	assert(!AXISignals::isMaster(id));

	if (id != AXI_RDATA)
	{
        mValues[id] = value;
	}
	else
	{
		mDataBufferRead[0] = value;
		for (MxU32 i = 0; i < getDataNumMxU32()-1; i++)
		{
			assert(extValues != NULL);
			mDataBufferRead[i+1] = extValues[i];				
		}
	}
}

void MxAXIPortT2S_Conv::init(MxU32 dataBitWidth, MxU32 addrBitWidth, MxU32 numAddressRegions, MxU64* start, MxU64* size, string* name)
{
    MxAXIPortT2S::init(dataBitWidth, numAddressRegions, start, size, name);

    mAddrBitWidth = addrBitWidth;
    mAddrNumMxU32 = (mAddrBitWidth+31)/32;
}

void MxAXIPortT2S_Conv::reset ()
{
	MxAXIPortT2S::reset();

	MxU32 i;
	
	for (i = 0; i < AXI_NUM_SIGNALS; i++)
	{
		mSet   [i] = false;
		mValues[i] = 0;
	}
	for (i = 0; i < AXI_NUM_MXU32_DATA; i++)
	{
		mDataBufferRead [i] = 0;
		mDataBufferWrite[i] = 0;
	}

    // Initialise RTL signals
	set_aw(NULL);
	set_ar(NULL);
	set_w(NULL);
	set_bready(1);
	set_rready(1);
}

void MxAXIPortT2S_Conv::set_aw (AXITransaction* axi)
{
    MxU64 extAWADDR = 0;
	if (axi == NULL)
	{
		mSPorts[AXI_AWID   ]->driveSignal(0, NULL);
		mSPorts[AXI_AWADDR ]->driveSignal(0, (MxU32*)&extAWADDR);
		mSPorts[AXI_AWLEN  ]->driveSignal(0, NULL);
		mSPorts[AXI_AWSIZE ]->driveSignal(0, NULL);
		mSPorts[AXI_AWBURST]->driveSignal(0, NULL);
		mSPorts[AXI_AWLOCK ]->driveSignal(0, NULL);
		mSPorts[AXI_AWCACHE]->driveSignal(0, NULL);
		mSPorts[AXI_AWPROT ]->driveSignal(0, NULL);
		mSPorts[AXI_AWUSER ]->driveSignal(0, NULL);
		mSPorts[AXI_AWVALID]->driveSignal(0, NULL);
	}
	else
	{
		mSPorts[AXI_AWID   ]->driveSignal(axi->get_id	(), NULL);
		if (getAddrNumMxU32() > 1)
		{
			extAWADDR = axi->get_addr();
			mSPorts[AXI_AWADDR]->driveSignal(((MxU32*)&extAWADDR)[0], &((MxU32*)&extAWADDR)[1]);
		}
		else
			mSPorts[AXI_AWADDR]->driveSignal(axi->get_addr(), NULL);
		mSPorts[AXI_AWLEN  ]->driveSignal(axi->get_len	(),	NULL);
		mSPorts[AXI_AWSIZE ]->driveSignal(axi->get_size	(),	NULL);
		mSPorts[AXI_AWBURST]->driveSignal(axi->get_burst(),	NULL);
		mSPorts[AXI_AWLOCK ]->driveSignal(axi->get_lock	(),	NULL);
		mSPorts[AXI_AWCACHE]->driveSignal(axi->get_cache(),	NULL);
		mSPorts[AXI_AWPROT ]->driveSignal(axi->get_prot	(),	NULL);
		mSPorts[AXI_AWUSER ]->driveSignal(axi->get_auser(),	NULL);
		mSPorts[AXI_AWVALID]->driveSignal(1,				NULL);
	}
	set_awready(mValues[AXI_AWREADY]);
}

void MxAXIPortT2S_Conv::set_ar (AXITransaction* axi)
{
    MxU64 extARADDR = 0;
	if (axi == NULL)
	{
		mSPorts[AXI_ARID   ]->driveSignal(0, NULL);
		mSPorts[AXI_ARADDR ]->driveSignal(0, (MxU32*)&extARADDR);
		mSPorts[AXI_ARLEN  ]->driveSignal(0, NULL);
		mSPorts[AXI_ARSIZE ]->driveSignal(0, NULL);
		mSPorts[AXI_ARBURST]->driveSignal(0, NULL);
		mSPorts[AXI_ARLOCK ]->driveSignal(0, NULL);
		mSPorts[AXI_ARCACHE]->driveSignal(0, NULL);
		mSPorts[AXI_ARPROT ]->driveSignal(0, NULL);
		mSPorts[AXI_ARUSER ]->driveSignal(0, NULL);
		mSPorts[AXI_ARVALID]->driveSignal(0, NULL);
	}
	else
	{
		mSPorts[AXI_ARID   ]->driveSignal(axi->get_id	(), NULL);
		if (getAddrNumMxU32() > 1)
		{
			extARADDR = axi->get_addr();
			mSPorts[AXI_ARADDR]->driveSignal(((MxU32*)&extARADDR)[0], &((MxU32*)&extARADDR)[1]);
		}
		else
			mSPorts[AXI_ARADDR]->driveSignal(axi->get_addr(),	NULL);
		mSPorts[AXI_ARLEN  ]->driveSignal(axi->get_len	(),	NULL);
		mSPorts[AXI_ARSIZE ]->driveSignal(axi->get_size	(),	NULL);
		mSPorts[AXI_ARBURST]->driveSignal(axi->get_burst(),	NULL);
		mSPorts[AXI_ARLOCK ]->driveSignal(axi->get_lock	(),	NULL);
		mSPorts[AXI_ARCACHE]->driveSignal(axi->get_cache(),	NULL);
		mSPorts[AXI_ARPROT ]->driveSignal(axi->get_prot	(),	NULL);
		mSPorts[AXI_ARUSER ]->driveSignal(axi->get_auser(),	NULL);
		mSPorts[AXI_ARVALID]->driveSignal(1,				NULL);
	}
	set_arready(mValues[AXI_ARREADY]);
}

void MxAXIPortT2S_Conv::set_w (AXITransaction* axi)
{
	MxU32* ext;

	if (axi == NULL)
	{
		ext = getDataNumMxU32() > 1 ? mDataBufferWrite : NULL;

		mSPorts[AXI_WID    ]->driveSignal(0, NULL);
		mSPorts[AXI_WDATA  ]->driveSignal(0, ext);
		mSPorts[AXI_WSTRB  ]->driveSignal(0, NULL);
		mSPorts[AXI_WLAST  ]->driveSignal(0, NULL);
		mSPorts[AXI_WUSER  ]->driveSignal(0, NULL);
		mSPorts[AXI_WVALID ]->driveSignal(0, NULL);
	}
	else
	{
		const MxU32*	srcData = axi->getP_getWriteData(axi->getCurrentBeat());
		MxU32*			dstData = mDataBufferWrite;
		MxU32			srcStrb = axi->get_strb();
		MxU32			dstStrb;

		//AXISignals::convertDataMx2Hw(axi, srcData, dstData, srcStrb, dstStrb);
		AXISignals::copyData(axi, srcData, dstData);
		dstStrb = srcStrb;

		ext = getDataNumMxU32() > 1 ? &(dstData[1]) : NULL;

		mSPorts[AXI_WID    ]->driveSignal(axi->get_id(),	NULL);
		mSPorts[AXI_WDATA  ]->driveSignal(dstData[0],		ext);
		mSPorts[AXI_WSTRB  ]->driveSignal(dstStrb,			NULL);
		mSPorts[AXI_WLAST  ]->driveSignal(axi->get_wlast(), NULL);
		mSPorts[AXI_WUSER  ]->driveSignal(axi->get_duser(),	NULL);
		mSPorts[AXI_WVALID ]->driveSignal(1,				NULL);
	}
	set_wready(mValues[AXI_WREADY]);
}

void MxAXIPortT2S_Conv::set_bready (MxU32 ready)
{
	mSPorts[AXI_BREADY ]->driveSignal(ready, NULL);
}

void MxAXIPortT2S_Conv::set_rready (MxU32 ready)
{
	mSPorts[AXI_RREADY ]->driveSignal(ready, NULL);
}

void MxAXIPortT2S_Conv::communicate ()
{
	MxAXIPortT2S::communicate();

	set_rvalid(mValues[AXI_RID], mDataBufferRead, mValues[AXI_RRESP], mValues[AXI_RLAST], mValues[AXI_RUSER], mValues[AXI_RVALID]);
	set_bvalid(mValues[AXI_BID], mValues[AXI_BRESP], mValues[AXI_BUSER], mValues[AXI_BVALID]);
}

void MxAXIPortT2S_Conv::update ()
{
	MxAXIPortT2S::update();
}



// ----------------------------------------------------------------------------------------------------------------------
//
//	MxAXIPortT2S_Trace
//
// ----------------------------------------------------------------------------------------------------------------------

#ifdef CARBON
MxAXIPortT2S_Trace::MxAXIPortT2S_Trace (sc_mx_module* owner, const string& name, const CarbonDebugAccessCallbacks& dbaCb)
  : MxAXIPortT2S(owner, name, dbaCb)
#else
MxAXIPortT2S_Trace::MxAXIPortT2S_Trace (sc_mx_module* owner, const string& name): MxAXIPortT2S(owner, name)
#endif
{
	MxU32 i;

	for (i = 0; i < AXI_NUM_SIGNALS; i++)
	{
		mPins	[i] = NULL;
		mPinsRef[i] = NULL;
	}
}

MxAXIPortT2S_Trace::~MxAXIPortT2S_Trace ()
{
}

void MxAXIPortT2S_Trace::setPin	(MxU32 id, MxU32* pin)
{
	mPins[id] = pin;
}

void MxAXIPortT2S_Trace::setPinRef	(MxU32 id, MxU32* pin)
{
	mPinsRef[id] = pin;
}

void MxAXIPortT2S_Trace::init (MxU32 dataBitWidth, MxU32 numAddressRegions, MxU64* start, MxU64* size, string* name, bool mergeData)
{
	MxAXIPortT2S::init(dataBitWidth, numAddressRegions, start, size, name);
	mMergeData = mergeData;
}

void MxAXIPortT2S_Trace::communicate ()
{
	MxAXIPortT2S::communicate();

	set_rvalid(*(mPins[AXI_RID]),  (mPins[AXI_RDATA]), *(mPins[AXI_RRESP]), *(mPins[AXI_RLAST]), *(mPins[AXI_RUSER]), *(mPins[AXI_RVALID]));
	set_bvalid(*(mPins[AXI_BID]), *(mPins[AXI_BRESP]), *(mPins[AXI_BUSER]), *(mPins[AXI_BVALID]));
}

void MxAXIPortT2S_Trace::update ()
{
	MxAXIPortT2S::update();
}

void MxAXIPortT2S_Trace::set_aw	(AXITransaction* axi)
{
	if (axi == NULL)
	{
		*(mPins[AXI_AWID   ]) = 0;
		*(mPins[AXI_AWADDR ]) = 0;
		*(mPins[AXI_AWLEN  ]) = 0;
		*(mPins[AXI_AWSIZE ]) = 0;
		*(mPins[AXI_AWBURST]) = 0;
		*(mPins[AXI_AWLOCK ]) = 0;
		*(mPins[AXI_AWCACHE]) = 0;
		*(mPins[AXI_AWPROT ]) = 0;
		*(mPins[AXI_AWUSER ]) = 0;
		*(mPins[AXI_AWVALID]) = 0;
	}
	else
	{
		*(mPins[AXI_AWID   ]) = axi->get_id		();
		*(mPins[AXI_AWADDR ]) = axi->get_addr	();
		*(mPins[AXI_AWLEN  ]) = axi->get_len	();
		*(mPins[AXI_AWSIZE ]) = axi->get_size	();
		*(mPins[AXI_AWBURST]) = axi->get_burst	();
		*(mPins[AXI_AWLOCK ]) = axi->get_lock	();
		*(mPins[AXI_AWCACHE]) = axi->get_cache	();
		*(mPins[AXI_AWPROT ]) = axi->get_prot	();
		*(mPins[AXI_AWUSER ]) = axi->get_auser	();
		*(mPins[AXI_AWVALID]) = 1;
	}
	set_awready(*(mPins[AXI_AWREADY]));
}

void MxAXIPortT2S_Trace::set_ar	(AXITransaction* axi)
{
	if (axi == NULL)
	{
		*(mPins[AXI_ARID   ]) = 0;
		*(mPins[AXI_ARADDR ]) = 0;
		*(mPins[AXI_ARLEN  ]) = 0;
		*(mPins[AXI_ARSIZE ]) = 0;
		*(mPins[AXI_ARBURST]) = 0;
		*(mPins[AXI_ARLOCK ]) = 0;
		*(mPins[AXI_ARCACHE]) = 0;
		*(mPins[AXI_ARPROT ]) = 0;
		*(mPins[AXI_ARUSER ]) = 0;
		*(mPins[AXI_ARVALID]) = 0;
	}
	else
	{
		*(mPins[AXI_ARID   ]) = axi->get_id		();
		*(mPins[AXI_ARADDR ]) = axi->get_addr	();
		*(mPins[AXI_ARLEN  ]) = axi->get_len	();
		*(mPins[AXI_ARSIZE ]) = axi->get_size	();
		*(mPins[AXI_ARBURST]) = axi->get_burst	();
		*(mPins[AXI_ARLOCK ]) = axi->get_lock	();
		*(mPins[AXI_ARCACHE]) = axi->get_cache	();
		*(mPins[AXI_ARPROT ]) = axi->get_prot	();
		*(mPins[AXI_ARUSER ]) = axi->get_auser	();
		*(mPins[AXI_ARVALID]) = 1;
	}
	set_arready(*(mPins[AXI_ARREADY]));
}

void MxAXIPortT2S_Trace::set_w (AXITransaction* axi)
{
	if (axi == NULL)
	{
		*(mPins[AXI_WID    ]) = 0;
		*(mPins[AXI_WSTRB  ]) = 0;
		*(mPins[AXI_WLAST  ]) = 0;
		*(mPins[AXI_WUSER  ]) = 0;
		*(mPins[AXI_WVALID ]) = 0;
	}
	else
	{
		const MxU32*	srcData = axi->getP_getWriteData(axi->getCurrentBeat());
		MxU32*			dstData = mPins[AXI_WDATA];
		MxU32			srcStrb = axi->get_strb();
		MxU32			dstStrb; // = *(mPinsRef[AXI_WSTRB]);
	
		//memcpy(dstData, mPinsRef[AXI_WDATA], sizeof(MxU32)*axi->getDataMxU32width());
		//AXISignals::convertDataMx2Hw(axi, srcData, dstData, srcStrb, dstStrb);
		if (mMergeData)
		{
			AXISignals::mergeData(axi, srcData, mPinsRef[AXI_WDATA], dstData);
		}
		else
		{
			AXISignals::copyData(axi, srcData, dstData);
		}
		dstStrb = srcStrb;
		
		*(mPins[AXI_WID    ]) = axi->get_id();
		*(mPins[AXI_WSTRB  ]) = dstStrb;
		*(mPins[AXI_WLAST  ]) = axi->get_wlast();
		*(mPins[AXI_WUSER  ]) = axi->get_duser();
		*(mPins[AXI_WVALID ]) = 1;
	}
	set_wready(*(mPins[AXI_WREADY]));
}

void MxAXIPortT2S_Trace::set_bready	(MxU32 ready)
{
	*(mPins[AXI_BREADY ]) = ready;	
}

void MxAXIPortT2S_Trace::set_rready	(MxU32 ready)
{
	*(mPins[AXI_RREADY ]) = ready;	
}

