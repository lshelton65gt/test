// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2006 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#ifndef T_S2T__H
#define T_S2T__H

#include "maxsim.h"
#include "MxSaveRestore.h"

#include "xactors/arm/include/carbon_arm_adaptor.h"

#include "MxPortCheck.h"

#ifdef CARBON
#include <stdarg.h>
#include "CarbonAdaptorBase.h"
#include "util/UtCLicense.h"
#define ADAPTOR_BASE CarbonAdaptorBase
#else
#define ADAPTOR_BASE sc_mx_module
#endif

template <class SIGNALS, class TMASTER> class T_S2T : public ADAPTOR_BASE
    , public MxSaveRestore
{

public:
    // Place instance declarations for the Ports here:
    
    // constructor / destructor
#ifdef CARBON
    T_S2T(sc_mx_module* carbonComp, const string &name, const string &compVersion,
          CarbonObjectID **carbonObj, CarbonPortFactory *portFactory);
#else
    T_S2T(const string& name, sc_mx_m_base* c, const string &s, const string &compVersion, const char* featureStrIndi, const char* featureStrPool, const char* versionStr);
#endif
    virtual ~T_S2T();

    // Implementation of MxSaveRestore interface methods
    virtual bool                saveData    (MxODataStream &data);
    virtual bool                restoreData (MxIDataStream &data);


    // overloaded sc_mx_module methods
    string                      getName     ();
    void                        setPortWidth(const char* name, uint32_t bitWidth);
    void                        setParameter(const string &name, const string &value);
    string                      getProperty (MxPropertyType property);
    void                        init        ();
    void                        terminate   ();
    void                        reset       (MxResetLevel level, const MxFileMapIF *filelist);

    void                        communicate ();
    void                        update      ();

private:    
    string                      mName;
    bool                        initComplete; 
    bool                        p_enableDbgMsg;
    MxU32                       p_DataBitWidth;
    MxU32                       p_AddrBitWidth;

    TMASTER*                    mTPort;
    
    // required for license checking
    char                        mFeatureStrIndi[30];
    char                        mFeatureStrPool[30];
    char                        mVersionStr[5];

    // component version
    string                      mCompVersion;

#ifdef CARBON
 public:
    MxStatus readDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
    MxStatus writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
    MxStatus debugTransaction(MxTransactionInfo*);
    int getNumRegions();
    void getAddressRegions(MxU64* start, MxU64* size, string * name);
    MxStatus debugMemWrite(MxU64 startAddress, MxU32 unitsToWrite, MxU32 unitSizeInBytes, const MxU8 *data, MxU32 *actualNumOfUnitsWritten, MxU32 secureFlag);
    MxStatus debugMemRead(MxU64 startAddress, MxU32 unitsToRead, MxU32 unitSizeInBytes, MxU8 *data, MxU32 *actualNumOfUnitsRead, MxU32 secureFlag);
#endif 	

    CarbonSignalMasterPort**	mMSignalPorts;
    T_SignalSlave<TMASTER>**    mSSignalPorts;
};



template <class SIGNALS, class TMASTER>
#ifdef CARBON
T_S2T<SIGNALS, TMASTER>::T_S2T(sc_mx_module* carbonComp, const string &name, const string &compVersion, CarbonObjectID **carbonObj, CarbonPortFactory *portFactory)
  : CarbonAdaptorBase(carbonComp, name, carbonObj)
#else
T_S2T<SIGNALS, TMASTER>::T_S2T(const string& name, sc_mx_m_base* c, const string &s, const string &compVersion) : sc_mx_module(c, s)
#endif
{
    initComplete = false;

    mName = name;
    mCompVersion = compVersion;
    string bn = SIGNALS::getBaseName();

#ifndef CARBON
    // Register ourself as a clock slave port.
    SC_MX_CLOCKED();
    registerPort( dynamic_cast< sc_mx_clock_slave_p_base* >( this ), "clk-in");
#endif

#ifdef CARBON
    mTPort = new TMASTER(mCarbonComponent, name.c_str());
    registerPort(mTPort, name.c_str());
#else
    mTPort = new TMASTER(this, bn + "_m");
    registerPort(mTPort, bn + "_m");
#endif
    
    mMSignalPorts = new CarbonSignalMasterPort* [SIGNALS::getNumSignals()];
    mSSignalPorts = new T_SignalSlave<TMASTER>* [SIGNALS::getNumSignals()];
    
    MxU32               id;
    MxSignalProperties  sp;

    for (id = 0; id < SIGNALS::getNumSignals(); id++)
    {
        if (!SIGNALS::isMaster(id))
        {
            mSSignalPorts[id] = NULL;
#ifdef CARBON
            mMSignalPorts[id] = portFactory(mCarbonComponent, name.c_str(), SIGNALS::getName(id).c_str(), mpCarbonObject);
#else
            mMSignalPorts[id] = new MxSignalMasterPort(SIGNALS::getName(id));   
            registerPort(mMSignalPorts[id], SIGNALS::getName(id));
#endif	
            mTPort->setSignalMasterPort(id, mMSignalPorts[id]);

            // set properties       
            sp = *(mMSignalPorts[id]->getProperties());
            sp.bitwidth = SIGNALS::getBitWidth(id);
            mMSignalPorts[id]->setProperties(&sp);
        }
        else
        {
            mMSignalPorts[id] = NULL;
            mSSignalPorts[id] = new T_SignalSlave<TMASTER>(SIGNALS::getName(id), id, mTPort);
            registerPort(mSSignalPorts[id], SIGNALS::getName(id));

            // set properties       
            sp = *(mSSignalPorts[id]->getProperties());
            sp.bitwidth = SIGNALS::getBitWidth(id);
            mSSignalPorts[id]->setProperties(&sp);      
        }
    }

    // Place parameter definitions here:
    p_enableDbgMsg = false;
    // defineParameter("Enable Debug Messages", "false", MX_PARAM_BOOL);
    defineParameter("Data Width",        "32",    MX_PARAM_VALUE, false);
    defineParameter("Address Width",     "32",    MX_PARAM_VALUE, false);
}

template <class SIGNALS, class TMASTER>
T_S2T<SIGNALS, TMASTER>::~T_S2T()
{
    MxU32 i;
    
    for (i = 0; i < SIGNALS::getNumSignals(); i++)
    {
        if (mMSignalPorts[i] != NULL)
        {
            delete mMSignalPorts[i];
        }
        else
        {
            delete mSSignalPorts[i];
        }
    }
    delete []mMSignalPorts;
    delete []mSSignalPorts;
    
    delete mTPort;
}

template <class SIGNALS, class TMASTER>
void T_S2T<SIGNALS, TMASTER>::init()
{
    MxU32 i;

    // Initialize ourself for save/restore.
    // initStream( this );

    // Allocate memory and initialize data here:
    mTPort->init(p_DataBitWidth, p_AddrBitWidth);

    MxSignalProperties sp;
    for (i = 0; i < SIGNALS::getNumSignals(); i++)
    {
        if (SIGNALS::isConfigurableDataBitWidth(i))
        {
            if (mMSignalPorts[i] != NULL)
            {
                sp = *(mMSignalPorts[i]->getProperties());
                sp.bitwidth = p_DataBitWidth;
                mMSignalPorts[i]->setProperties(&sp);
            }
            else
            {
                sp = *(mSSignalPorts[i]->getProperties());
                mSSignalPorts[i]->setProperties(&sp);
            }
        }
    }

    // Call the base class after this has been initialized.
    ADAPTOR_BASE::init();

    // Set a flag that init stage has been completed
    initComplete = true;
}

template <class SIGNALS, class TMASTER>
void T_S2T<SIGNALS, TMASTER>::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
    // Add your reset behavior here:
    // ...
    MxPortCheck::check(mTPort);
    mTPort->reset();

    // Call the base class after this has been reset.
    ADAPTOR_BASE::reset(level, filelist);
}

template <class SIGNALS, class TMASTER>
void T_S2T<SIGNALS, TMASTER>::terminate()
{
    // Call the base class first.
    ADAPTOR_BASE::terminate();

    mTPort->terminate();
}

template <class SIGNALS, class TSLAVE>
void T_S2T<SIGNALS, TSLAVE>::setPortWidth(const char* name, uint32_t bitWidth)
{
  if (strcmp(name, "wdata") == 0 || strcmp(name, "rdata") == 0) {
    p_DataBitWidth = bitWidth;
  }
  else if (strcmp(name, "araddr") == 0 || strcmp(name, "awaddr") == 0) {
    p_AddrBitWidth = bitWidth;
    if (p_AddrBitWidth > 64) {
      message( MX_MSG_WARNING, "T_S2T::setPortWidth: Cannot set port <%s> width higher than 64. Using 64 instead.", name );
      p_AddrBitWidth = 64;
    }
  }
}

template <class SIGNALS, class TMASTER>
void T_S2T<SIGNALS, TMASTER>::setParameter( const string &name, const string &value )
{
    MxConvertErrorCodes status = MxConvert_SUCCESS;

    if (name == "Enable Debug Messages")
    {
        status = MxConvertStringToValue( value, &p_enableDbgMsg  );
    }
    else if (name == "Data Width")
    {
        if ( initComplete == false )
        {
            status = MxConvertStringToValue( value, &p_DataBitWidth  );
        }
        else
        {
            message( MX_MSG_WARNING, "T_S2T::setParameter: Cannot change parameter <%s> at runtime. Assignment ignored.", name.c_str() );
            return;
        }
    }
    else if (name == "Address Width")
    {
        if ( initComplete == false )
        {
            status = MxConvertStringToValue( value, &p_AddrBitWidth  );
            if (p_AddrBitWidth > 64)
            {
                message( MX_MSG_WARNING, "T_S2T::setParameter: Cannot set parameter <%s> higher than 64. Using 64 instead.", name.c_str() );
                p_AddrBitWidth = 64;
            }
        }
        else
        {
            message( MX_MSG_WARNING, "T_S2T::setParameter: Cannot change parameter <%s> at runtime. Assignment ignored.", name.c_str() );
            return;
        }
    }

    if ( status == MxConvert_SUCCESS )
    {
        ADAPTOR_BASE::setParameter(name, value);
    }
    else
    {
        message( MX_MSG_WARNING, "T_S2T::setParameter: Illegal value <%s> "
         "passed for parameter <%s>. Assignment ignored.", value.c_str(), name.c_str() );
    }
}

template <class SIGNALS, class TMASTER>
string T_S2T<SIGNALS, TMASTER>::getProperty( MxPropertyType property )
{
    string description; 
    string bn = SIGNALS::getBaseName();

    switch ( property ) 
    {    
     case MX_PROP_IP_PROVIDER:
         return "ARM";
     case MX_PROP_LOADFILE_EXTENSION:
        return "";
     case MX_PROP_REPORT_FILE_EXT:
        return "yes";
     case MX_PROP_COMPONENT_TYPE:
        return MxComponentTypes[MX_TYPE_TRANSACTOR];
     case MX_PROP_COMPONENT_VERSION:
        return mCompVersion;
     case MX_PROP_MSG_PREPEND_NAME:
        return "yes"; 
     case MX_PROP_DESCRIPTION:
        description = "MxSI " +bn+ " signal to MxSI3.0 " +bn+ " transaction master converter\n";
        return description + "Compiled on " + __DATE__ + ", " + __TIME__; 
     case MX_PROP_MXDI_SUPPORT:
        return "no";
     case MX_PROP_SAVE_RESTORE:
        return "no";
     default:
        return "";
    }
    return "";
}

template <class SIGNALS, class TMASTER>
string T_S2T<SIGNALS, TMASTER>::getName(void)
{
    return mName;
}


template <class SIGNALS, class TMASTER>
bool T_S2T<SIGNALS, TMASTER>::saveData( MxODataStream &data )
{
    // TODO:  Add your save code here.
    // This shows how the example state variable is saved.
    // data << exampleStateVariable;

    // TODO:  Add verification if applicable.
    // return save was successful
    return false;
}

template <class SIGNALS, class TMASTER>
bool T_S2T<SIGNALS, TMASTER>::restoreData( MxIDataStream &data )
{
    // TODO:  Add your restore code here.
    // This shows how the example state variable is restored.
    // data >> exampleStateVariable;

    // TODO:  Add verification if applicable.
    // return restore was successful
    return false;
}


template <class SIGNALS, class TMASTER>
void T_S2T<SIGNALS, TMASTER>::communicate ()
{
    mTPort->communicate();
}

template <class SIGNALS, class TMASTER>
void T_S2T<SIGNALS, TMASTER>::update ()
{
    mTPort->update();
}

#ifdef CARBON
template <class SIGNALS, class TMASTER>
MxStatus T_S2T<SIGNALS, TMASTER>::readDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return mTPort->readDbg(addr, value, ctrl);
}

template <class SIGNALS, class TMASTER>
MxStatus T_S2T<SIGNALS, TMASTER>::writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return mTPort->writeDbg(addr, value, ctrl);
}

template <class SIGNALS, class TMASTER>
MxStatus T_S2T<SIGNALS, TMASTER>::debugTransaction(MxTransactionInfo* info)
{
  return mTPort->debugTransaction(info);
}

template <class SIGNALS, class TMASTER>
int T_S2T<SIGNALS, TMASTER>::getNumRegions()
{
  return mTPort->getNumRegions();
}

template <class SIGNALS, class TMASTER>
void T_S2T<SIGNALS, TMASTER>::getAddressRegions(MxU64* start, MxU64* size, string * name)
{
  return mTPort->getAddressRegions(start, size, name);
}

template <class SIGNALS, class TMASTER>
MxStatus T_S2T<SIGNALS, TMASTER>::debugMemWrite(MxU64 startAddress, MxU32 unitsToWrite, MxU32 unitSizeInBytes, const MxU8 *data, MxU32 *actualNumOfUnitsWritten, MxU32 secureFlag)
{
  // Assume that we write all the units requested
  *actualNumOfUnitsWritten = unitsToWrite;

  AXITransaction trans;
  MxU32 masterFlags[AXI_STEP_LAST];
  trans.masterFlags = masterFlags;
  trans.initialize(mTPort->getProperties());
  trans.setInitiator(mTPort);
  trans.init(p_DataBitWidth);
  trans.clear();
  trans.nts = AXI_STEP_LAST;
  trans.reset();
  trans.access = MX_ACCESS_WRITE;
  trans.masterFlags[AXI_MF_CHANNEL] = AXI_CHANNEL_AW;
  if (secureFlag)
    trans.set_prot(trans.get_prot() | AXI_PROTECTION_SEC);
  else
    trans.set_prot(trans.get_prot() & ~AXI_PROTECTION_SEC);
  trans.set_id(0);
  trans.set_len(0); // remember: AXI length starts at 0, 1 less than the units to write
  trans.set_size(0); // remember: the number of bytes is 2**(AXI size)
                     // so a zero arg here means 1 byte, a 2 here means 4 bytes, 


  // now split up the accesses into single byte transactions to
  // simplify the setting up of data
  MxStatus status = MX_STATUS_OK;
  
  for (MxU32 curByte = 0; curByte < unitSizeInBytes; curByte++){
    MxU64 curAddress = startAddress+curByte;
    trans.set_addr(curAddress);
    memset(trans.dataWr, 0, 8);


    MxU32 dataMask[1024/32];
    MxU64 addr = trans.get_addrOfBeat(0);
    MxU32 replData[2];
    memset(replData, data[curByte], 8);

    AXITransaction::getValidDataMaskOnBus(dataMask, addr, trans.getDataBitwidth(), trans.dataSize);
    for (MxU32 i = 0; i < trans.getDataMxU32width(); i++)
    {
      MxU32* ptr = trans.getP_setWriteData(0);
      ptr[i] = replData[i] & dataMask[i];                        
      addr+= 4;
    }
    MxStatus tmp_status = debugTransaction(&trans);
    if ( ( status == MX_STATUS_OK ) && ( tmp_status != MX_STATUS_OK )){
      status = tmp_status;
      break;
    }
  }
  return status;
}

template <class SIGNALS, class TMASTER>
MxStatus T_S2T<SIGNALS, TMASTER>::debugMemRead(MxU64 startAddress, MxU32 unitsToRead, MxU32 unitSizeInBytes, MxU8 *data, MxU32 *actualNumOfUnitsRead, MxU32 secureFlag)
{
  // Assume that we read all the units requested
  *actualNumOfUnitsRead = unitsToRead;

  AXITransaction trans;
  MxU32 masterFlags[AXI_STEP_LAST];
  trans.masterFlags = masterFlags;
  trans.initialize(mTPort->getProperties());
  trans.setInitiator(mTPort);
  trans.init(p_DataBitWidth);
  trans.clear();
  trans.nts = AXI_STEP_LAST;
  trans.reset();
  trans.access = MX_ACCESS_READ;
  trans.masterFlags[AXI_MF_CHANNEL] = AXI_CHANNEL_AW;
  if (secureFlag)
    trans.set_prot(trans.get_prot() | AXI_PROTECTION_SEC);
  else
    trans.set_prot(trans.get_prot() & ~AXI_PROTECTION_SEC);

  trans.set_id(0);
  trans.set_len(0); // remember: AXI length starts at 0, 1 less than the units to read
  trans.set_size(0); // remember: the number of bytes is 2**(AXI size)
                     // so a zero arg here means 1 byte, a 2 here means 4 bytes, 
                     // we are going to do a series of single byte
                     // transactions below so we set the size to zero here
  MxU32* ptr = trans.getP_getReadData(0);

  // now split up the access into single byte accesses to simplify the
  // packing of results into data
  for (MxU32 curByte = 0; curByte < unitSizeInBytes; curByte++){
    MxU64 curAddress = startAddress+curByte;
    trans.set_addr(curAddress);
    memset(trans.dataRd, 0, 8);

    mTPort->debugTransaction(&trans);

    MxU32 offset = curAddress % ((trans.getDataBitwidth()+7)/8);

    if (offset >= 4) 
      data[curByte] = ptr[1] >> ((offset-4)*8);
    else
      data[curByte] = ptr[0] >> (offset*8);
  }
  return MX_STATUS_OK;
}


#endif

#endif
