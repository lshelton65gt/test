// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2006 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#ifndef _MxAXIPort_h_
#define _MxAXIPort_h_

#include "maxsim.h"
#include "persister.h"

#include <assert.h>
#include <string>
#include <stdio.h>

#include "AXI_Transaction.h"
#include "SimpleHeap.h"






// ****************************************************************************************************
// Convention
// ----------
// [C] = Communicate phase, [U] = Update phase
//
//
// Timing information
// ------------------
//
// [C] ----------------------------------------
//  :
//  :.. Component
//  :  	:
//  :  	: - Master: newTransaction() (if not already done in previous [U])
//  :  	: - Master: setNext_wvalid() (if not already done in previous [U])
//  :  	: - Slave:  setNext_rvalid() (if not already done in previous [U])
//  :  	: - Slave:  setNext_bvalid() (if not already done in previous [U])
//  :  	:
//  :  	:
//  :  	:
//  :  	:
//  :  	:
//  :  	:.......... Master/Slave port
//  :  				:
//  :  				: - Master: Increment the cts for the 'valid' transactions in channel w if marked for cts-incr in previous [U]
//  :  				:           or if the same transaction is also in channel aw and the cts=0 (i.e. it occupies both aw and w for
//  :               :           the first time).
//  :  				: - Master: Call driveTransaction() for all 'valid' transactions in channels aw, ar, w.
//  :  				:           Call driveTransaction() with an empty (dummy) transaction for non-'valid' channels aw, ar or w.
//  :  				:
//  :  				: - Slave:  Increment the cts for the 'valid' transactions if marked for cts-incr in previous [U].
//  :  				: - Slave:  Call notifyEvent() for all 'valid' transactions in channels r, b.
//  :  				:           Call notifyEvent() with an empty (dummy) transaction for non-'valid' channels r, b.
//  :  				:
//  :  				:
//  :  				:
//  :  				:
//  :  				:
//  :  				:
//  :  				:
//  :  	............:
//  :  	:
//  :  	:
//  :  	:
//  :  	:
//  :  	:
//  :  	:
//  :  	:
//  :...:
//  :
// ---
// driveTransaction()/notifyEvent() -- can be invoked at any time before or after components [C]
//            :
//            : - Slave:  If driveTransaction()/notifyEvent() is being called for the first time regarding this transaction
//            :           and this transaction step then store it to the according channel stage, additionally for channel w
//            :           store it to an incoming transaction struct. In case there is no space free in the according channel
//            :           stage don't store.
//            :
// ---
//  :
//  :
//  :
// [U] ----------------------------------------
//  :
//  :.. Component
//  :  	:
//  :  	: - Channel stages are as for current cycle
//  :  	:
//  :  	:
//  :  	:
//  :  	:
//  :  	:.......... Master/Slave port (except for MxAXISlavePort which lets the component do these things)
//  :  				:
//  :  				: - Master: Mark transactions which finished their transfer for cts increment in case the incremented cts
//  :               :           belongs to channel w and there is space in channel stage w (if not already in channel stage w).
//  :  				: - Slave : Mark transactions which finished their transfer for cts increment in case the incremented cts
//  :  				:           belongs to channels r or b and there is space in channel stages r or b (if not already in the
//  :  				:           right channel stage).
//  :  				:
//  :  				:
//  :  	............:
//  :  	:
//  :  	: - Master: setNext_rready()
//  :  	: - Master: setNext_bready()
//  :  	: - Slave:  setNext_arready()
//  :  	: - Slave : setNext_awready()
//  :  	: - Slave : setNext_wready()
//  :  	:
//  :  	: - Master: newTransaction() (can also be done in next [C])
//  :  	: - Master: setNext_wvalid() (can also be done in next [C])
//  :  	: - Slave:  setNext_rvalid() (can also be done in next [C])
//  :  	: - Slave:  setNext_bvalid() (can also be done in next [C])
//  :  	:
//  :  	:
//  :  	:
//  :...:
//  :
//  :
// next [C] ----------------------------------------
//
//
//
//
//
// ****************************************************************************************************








#ifndef AXI_PROTOCOL_VERSION_2
#  error "Incompatible AXI transaction protocol versions"
#endif

// ==========================================================================================
class AXITransaction : public MxTransactionInfo
// ==========================================================================================
{
public:
	inline AXITransaction();
	inline virtual ~AXITransaction();

	inline const string getVersion() const;

	inline void init(MxU32 dataBitwidth);
	inline void clear();


	// ------------------------------------------------------------
	// related to bus width
	// ------------------------------------------------------------
	inline MxU32 getDataBitwidth()   const;	// data bus width
	inline MxU32 getDataMxU32width() const;	// data bus width in numbers of 32 Bits elements (MxU32)



	// ------------------------------------------------------------
	// related to main transaction settings
	// ------------------------------------------------------------

	inline MxTransactionAccess getAccessType() const;

	// read/write address channel
	inline void		set_id			(const MxU32 id);			// constant during life time of transaction
	inline MxU32	get_id			() const;

	inline void		set_addr		(const MxU64 _addr);		// constant during life time of transaction
	inline MxU64	get_addr		() const;

	inline MxU64	get_addrOfBeat	(MxU32 beat) const;
	inline MxU64	get_addrOfCurrentBeat () const;
	inline MxU64	get_addrOfNextBeat () const;

	inline void		set_len			(const MxU32 len);			// constant during life time of transaction
	inline MxU32	get_len			() const;
	// alternative one can use the two following functions
	inline void		setNumDataBeats	(const MxU32 numDataBeats);	// constant during life time of transaction
	inline MxU32	getNumDataBeats	() const;
	//
	inline void		set_size		(const MxU32 size);			// constant during life time of transaction
	inline MxU32	get_size		() const;
	inline void		set_burst		(const MxU32 burst);		// constant during life time of transaction
	inline MxU32	get_burst		() const;
	inline void		set_lock		(const MxU32 lock);			// constant during life time of transaction
	inline MxU32	get_lock		() const;
	inline void		set_cache		(const MxU32 cache);		// constant during life time of transaction
	inline MxU32	get_cache		() const;
	inline void		set_prot		(const MxU32 prot);			// constant during life time of transaction
	inline MxU32	get_prot		() const;

	inline MxU32	get_wlast		() const;				// the most recent step the transaction is or has been active is the last data step
	inline MxU32	get_rlast		() const;				// the most recent step the transaction is or has been active is the last data step
	inline MxU32	getNext_wlast	() const;				// the next step the transaction will be active will be the last data step
	inline MxU32	getNext_rlast	() const;				// the next step the transaction will be active will be the last data step

	// write data channel
	// Master [C]
	inline void		set_strb		(const MxU32 strb);
	// Slave [U]
	inline MxU32	get_strb		() const;

	// read channel, write response channel
	// Slave [C]
	inline void		set_resp		(const MxU32 resp);
	// Master [U]
	inline MxU32	get_resp		() const;

	// read/write data channel
	// Master at write [C]
	inline MxU32*	getP_setWriteData	(const MxU32 beat);
	// Master at read [U]
	inline MxU32*	getP_getReadData	(const MxU32 beat);


	// read/write data channel
	// Slave at write [U]
	inline MxU32*	getP_getWriteData	(const MxU32 beat);
	// Slave at read [C]
	inline MxU32*	getP_setReadData	(const MxU32 beat);


	// user signals
	inline void		set_auser(const MxU32 auser);
	inline MxU32	get_auser() const;
	inline void		set_duser(const MxU32 duser);
	inline MxU32	get_duser() const;
	inline void		set_buser(const MxU32 buser);
	inline MxU32	get_buser() const;




	// ------------------------------------------------------------
	// related to most recent active transaction steps/beats
	// ------------------------------------------------------------

	inline MxU32 lastActiveCts() const;	// the cts of the most recent active step


	// Return the current step/beat as it was set in the last Master/Slave port [C] the according step/beat became 'valid'.
	// valid[C] |____[U]____([C]___[U]____)| valid[C]
	inline MxU32 getCurrentStep() const;	// get the most recent step the transaction is or has been active
	inline MxU32 getCurrentBeat() const;	// get the most recent beat the transaction is or has been active

	// Return the next step/beat (see also getCurrentBeat()/getCurrentStep()) as it is needed to provide data
	// for the next step/beat where the transaction still is at the current step/beat.
	inline MxU32 getNextStep() const;	// get the next step the transaction will be active
	inline MxU32 getNextBeat() const;	// get the next beat the transaction will be active



	// ------------------------------------------------------------
	// related to driveTransaction()/notifyEvent() function calls
	// ------------------------------------------------------------

	inline MxU32 getChannel() const;	// get channel flags -- valid only during driveTransaction()/notifyEvent() call (else undefined)
	inline bool isChannelAR() const;	// driveTransaction()/notifyEvent() call for channel ar
	inline bool isChannelAW() const;	// driveTransaction()/notifyEvent() call for channel aw
	inline bool isChannelR () const;	// driveTransaction()/notifyEvent() call for channel r
	inline bool isChannelW () const;	// driveTransaction()/notifyEvent() call for channel w
	inline bool isChannelB () const;	// driveTransaction()/notifyEvent() call for channel b

	// Are there for this port no further calls to DriveTransaction/notify to be expected?
	inline bool isLastCall() const;
	inline void setLastCall();
	inline void unsetLastCall();

	// Does this MxTransctionInfo contain valid transaction info or is it just
	// used to indicate that nothing is going on on this channel on this port
	inline void setInvalidTransaction();	// mark transaction as 'empty' transaction
	inline bool isValidTransaction() const;	// transaction is not valid in the meaning of not containing a real transaction (it's an empty transaction)



	// ------------------------------------------------------------
	// related to handshake signals (in hardware: xxvalid, xxready)
	// ------------------------------------------------------------
	inline bool		isActiveValid		() const;	// current step (cts) is valid = '1' ( >= MX_MASTER_READY )
	inline bool		isActiveReady		() const;	// current step (cts) is ready = '1' and valid = '1' ( >= MX_SLAVE_READY )

	inline bool		isActiveValidAW		() const;	// write address step is valid = '1' ( >= MX_MASTER_READY ), true only for the cycle aw is active
	inline bool		isActiveReadyAW		() const;	// write address step is ready = '1' and valid = '1' ( >= MX_SLAVE_READY ),  true only for the cycle aw is active


	inline bool		isValid			() const;	// the most recent step the transaction is or has been active is valid = '1' ( >= MX_MASTER_READY ) => always true
	inline bool		isReady			() const;	// the most recent step the transaction is or has been active is ready = '1' ( >= MX_SLAVE_READY )

	// [U]
	inline bool	isTransferFinished_aw() const;  // true if channel aw transfer is finished this cycle or in a cycle before (actually looks whether address step is finished)
	inline bool	isTransferFinished_ar() const;	// true if channel ar transfer is finished this cycle or in a cycle before (actually looks whether address step is finished)
	inline bool	isTransferFinished_r() const;	// true if a channel r transfer is finished this cycle or in a cycle before
	inline bool	isTransferFinished_w() const;	// true if a channel w transfer is finished this cycle or in a cycle before
	inline bool	isTransferFinished_b() const;	// true if channel b transfer is finished (equivalent to isTransactionFinished())

	inline bool	isTransferFinishedFirstTime_r() const;	// true if channel r step is active and finished this cycle
	inline bool	isTransferFinishedFirstTime_w() const;	// true if channel w step is active and finished this cycle

	// complete transaction [U]
	inline bool isTransactionFinished() const;	// transaction completely finished (last transfer finished), at write this implies write response is finished


	inline void	activateChannelAW();	// cleared with clear()
	inline void inactivateChannelAW();	// cleared with clear()




	// ------------------------------------------------------------
	// Helpers for preparing values in update and adopting them in communicate
	// ------------------------------------------------------------

	inline void  setNextStrb(MxU32 data);
	inline MxU32 getNextStrb() const;

	inline void  prepareNextCts();
	inline void  adoptNextCts();


	// [U] Can transaction move to next channel?
	inline bool isReadyFor_r() const;	// ar -> r
	inline bool isReadyFor_b() const;   // (aw) w -> b


	// ------------------------------------------------------------
	// related to data and strobe alignment
	// ------------------------------------------------------------
	inline void alignWrDataAndStrobeSw2Hw	(const MxU32 beat, const MxU32* srcData, const MxU32 srcStrb);
	inline void alignWrDataSw2Hw			(const MxU32 beat, const MxU32* srcData);
	inline void alignRdDataSw2Hw			(const MxU32 beat, const MxU32* srcData);
	inline void alignStrobeSw2Hw			(const MxU32 beat, const MxU32 srcStrb);
	inline void alignWrDataAndStrobeHw2Sw	(const MxU32 beat, MxU32* dstData, MxU32& dstStrb);
	inline void alignWrDataHw2Sw			(const MxU32 beat, MxU32* dstData);
	inline void alignRdDataHw2Sw			(const MxU32 beat, MxU32* dstData);
	inline void alignStrobeHw2Sw			(const MxU32 beat, MxU32& dstStrb) const;


    static void getValidDataMaskOnBus (MxU32* dataMask, MxU64& addr64, MxU32 busBitwidth, MxU32 dataSize);
    static void getValidDataMaskOnBus (MxU32* dataMask, MxU64& addr64, MxU32 busBitwidth, MxU32 dataSize, MxU32 strb);


	// ------------------------------------------------------------
	// debugging related
	// ------------------------------------------------------------

	// only for debugging purpose
	static inline string getDbgInfo_headline();
	inline string getDbgInfo_infoline();

    // Save & Restore
    inline bool persist(persister & p);
    inline bool hookup(std::map<AXITransaction*,AXITransaction*> & lookup, persister & p);

	// ------------------------------------------------------------
	// exclusive pointers for component port related extensions
	// ------------------------------------------------------------
	void * m_pExclusiveMasterPortRelated;		// pointer to something used exclusively by the master port
	void * m_pExclusiveSlavePortRelated;		// pointer to something used exclusively by the slave port


	// ------------------------------------------------------------
	// related to SimpleHeap
	// ------------------------------------------------------------
	AXITransaction	* nextFreeP;	// needed by SimpleHeap, must be public


	// ------------------------------------------------------------
	// related to ACI
	// ------------------------------------------------------------
	 // used for chaining in ACI
	// 'Int' postfix because shared memory is allocated this component and shared with external component
	AXITransaction	*m_pACIrelatedSPortInfoInt;

	// used for chaining in ACI
	// 'Ext' postfix because shared memory is allocated by external component
	AXITransaction	*m_pACIrelatedMPortInfoExt;

	MxU32 m_ACISPortIdxInt, m_ACIMPortIdxInt;
	MxU32 m_ACISPortIdxExt, m_ACIMPortIdxExt;


	// ------------------------------------------------------------
	// reserved for extensions
	// ------------------------------------------------------------
	void * m_pPad;			// for preliminary internal extensions
	MxU32  m_padVersion;	// implementations can adapt their behavior according to the value of the pad version

private:

	MxU32 m_dataBitwidth;
	MxU32 m_dataMxU32width;	// = (dataBitwidth + 31)/32


	MxU32 m_nextCts;
	MxU32 m_nextStrb;
};


// moved table from inline method to here because of problems with gcc
const MxU32 _AXITransaction_mxDataSize2axsize[129] = {
	// 0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
	0, 0, 1, 0, 2, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0,	// 0
	4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	// 1
	5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	// 2
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	// 3
	6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	// 4
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	// 5
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	// 6
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	// 7
	7                                               // 8
};






AXITransaction::AXITransaction() : MxTransactionInfo()
,m_pExclusiveMasterPortRelated(0)
,m_pExclusiveSlavePortRelated(0) 
,nextFreeP(NULL)
,m_pACIrelatedSPortInfoInt(NULL)
,m_pACIrelatedMPortInfoExt(NULL)
,m_ACISPortIdxInt(0xDEADBEAF)
,m_ACIMPortIdxInt(0xDEADBEAF)
,m_ACISPortIdxExt(0xDEADBEAF)
,m_ACIMPortIdxExt(0xDEADBEAF)
,m_pPad(NULL)
,m_padVersion(0)
,m_dataBitwidth(0)
,m_dataMxU32width(0)
,m_nextCts(0)
,m_nextStrb(0)
{ 
    // nothing to do - all already initialised
}

AXITransaction::~AXITransaction() 
{
    m_pExclusiveMasterPortRelated = NULL; // Not ours to delete
    m_pExclusiveSlavePortRelated  = NULL; // Not ours to delete
    nextFreeP = NULL;
    m_pACIrelatedSPortInfoInt = NULL;
    m_pACIrelatedMPortInfoExt = NULL;
    m_pPad = NULL; // Not ours to delete
}

const string AXITransaction::getVersion() const { return string("2.0"); }

void AXITransaction::init(MxU32 dataBitwidth)
{
	nts = AXI_STEP_LAST;
	MxTransactionInfo::reset();

	m_dataBitwidth = dataBitwidth;
	m_dataMxU32width = (dataBitwidth + 31)/32;
	m_pACIrelatedSPortInfoInt = NULL;
	m_pACIrelatedMPortInfoExt = NULL;
	m_ACISPortIdxInt = 0xDEADBEAF;
	m_ACIMPortIdxInt = 0xDEADBEAF;
	m_ACISPortIdxExt = 0xDEADBEAF;
	m_ACIMPortIdxExt = 0xDEADBEAF;
}



// ------------------------------------------------------------
// related to most recent active transaction steps/beats
// ------------------------------------------------------------

MxU32	AXITransaction::lastActiveCts() const	{ return (status[cts] == MX_MASTER_WAIT) ? cts - 1 : cts; }	// the cts of the most recent active step

MxU32	AXITransaction::getCurrentStep() const	{ return lastActiveCts(); }
MxU32	AXITransaction::getCurrentBeat() const	{ return (lastActiveCts() > 0) ? lastActiveCts() - 1 : 0; }

MxU32	AXITransaction::getNextStep() const		{ return lastActiveCts() + 1; }
MxU32	AXITransaction::getNextBeat() const		{ return lastActiveCts(); }


// ------------------------------------------------------------
// related to bus width
// ------------------------------------------------------------
MxU32 AXITransaction::getDataBitwidth()   const { return m_dataBitwidth; }
MxU32 AXITransaction::getDataMxU32width() const { return m_dataMxU32width; }




// ------------------------------------------------------------
// related to main transaction settings
// ------------------------------------------------------------

void	AXITransaction::set_len			(const MxU32 len)
{
	dataBeats = len + 1;
	nts =
		1  											// address phase
		+ dataBeats									// data beats
		+ ((MX_ACCESS_WRITE == access) ? 1 : 0);	// write response
}

MxU32	AXITransaction::get_len			() const	{ return dataBeats - 1; }
// alternative one can use the two following functions
void	AXITransaction::setNumDataBeats	(const MxU32 numDataBeats)
{
	dataBeats = numDataBeats;
	nts =
		1  											// address phase
		+ dataBeats									// data beats
		+ ((MX_ACCESS_WRITE == access) ? 1 : 0);	// write response
}

MxU32	AXITransaction::getNumDataBeats	() const	{ return dataBeats; }
//
void	AXITransaction::set_size		(const MxU32 size)	{ dataSize = (1 << size); }
MxU32	AXITransaction::get_size		() const
{
	//                  0                  1                  2                  3                   4                   5                   6                    7
	assert((dataSize == 1) || (dataSize == 2) || (dataSize == 4) || (dataSize == 8) || (dataSize == 16) || (dataSize == 32) || (dataSize == 64) || (dataSize == 128));
	MxU32 size = _AXITransaction_mxDataSize2axsize[dataSize];

#ifdef _DEBUG_
	MxU32 tmp_size = 0;
	MxU32 mxDataSize = dataSize;
	while ((mxDataSize & 0x1) == 0)
	{
		mxDataSize >>= 1;
		tmp_size++;
	}
	assert(size == tmp_size);
#endif

	return size;
}

void	AXITransaction::set_burst		(const MxU32 burst)	{ masterFlags[AXI_MF_BURST_TYPE] = burst; }
MxU32	AXITransaction::get_burst		() const
{
	MxU32 ret = masterFlags[AXI_MF_BURST_TYPE];
	return ret;
}

void	AXITransaction::set_lock		(const MxU32 lock)	{ masterFlags[AXI_MF_LOCK] = lock; }
MxU32	AXITransaction::get_lock		() const
{
	MxU32 ret = masterFlags[AXI_MF_LOCK];
	return ret;
}

void	AXITransaction::set_cache		(const MxU32 cache)	{ masterFlags[AXI_MF_CACHE] = cache; }
MxU32	AXITransaction::get_cache		() const
{
	MxU32 ret = masterFlags[AXI_MF_CACHE]; 
	return ret;
}

void	AXITransaction::set_prot		(const MxU32 prot)	{ masterFlags[AXI_MF_PROTECTION] = prot; }
MxU32	AXITransaction::get_prot		() const
{
	MxU32 ret = masterFlags[AXI_MF_PROTECTION]; 
	return ret;
}

MxU32	AXITransaction::get_wlast		() const
{
	MxU32 ret = lastActiveCts() == dataBeats; 
	return ret;
}
MxU32	AXITransaction::get_rlast		() const
{ 
	MxU32 ret = lastActiveCts() == dataBeats; 
	return ret;
}

MxU32	AXITransaction::getNext_wlast	() const
{ 
	MxU32 ret = (lastActiveCts() == (dataBeats - 1)); 
	return ret;
}

MxU32	AXITransaction::getNext_rlast	() const
{ 
	MxU32 ret = (lastActiveCts() == (dataBeats - 1)); 
	return ret;
}

// write data channel
void	AXITransaction::set_strb		(const MxU32 strb)	{ masterFlags[AXI_MF_DATA_STROBE] = strb; }
MxU32	AXITransaction::get_strb		() const
{ 
	MxU32 ret = masterFlags[AXI_MF_DATA_STROBE]; 
	return ret;
}


// read channel, write response channel
void	AXITransaction::set_resp		(const MxU32 resp)	{ masterFlags[AXI_MF_RESPONSE] = resp; }
MxU32	AXITransaction::get_resp		() const
{ 
	MxU32 ret = masterFlags[AXI_MF_RESPONSE]; 
	return ret;
}


// read/write data channel
MxU32*	AXITransaction::getP_setWriteData (const MxU32 beat) { return &dataWr[beat * m_dataMxU32width]; }
MxU32*	AXITransaction::getP_getReadData  (const MxU32 beat) { return &dataRd[beat * m_dataMxU32width]; }
MxU32*	AXITransaction::getP_getWriteData (const MxU32 beat) { return &dataWr[beat * m_dataMxU32width]; }
MxU32*	AXITransaction::getP_setReadData  (const MxU32 beat) { return &dataRd[beat * m_dataMxU32width]; }


// user signals
void	AXITransaction::set_auser(const MxU32 auser)	{ masterFlags[AXI_MF_AUSER] = auser; }
MxU32	AXITransaction::get_auser() const
{
	MxU32 ret = masterFlags[AXI_MF_AUSER];
	return ret;
}

void	AXITransaction::set_duser(const MxU32 duser)	{ masterFlags[AXI_MF_DUSER] = duser; }
MxU32	AXITransaction::get_duser() const
{
	MxU32 ret = masterFlags[AXI_MF_DUSER];
	return ret;
}

void	AXITransaction::set_buser(const MxU32 buser)	{ masterFlags[AXI_MF_BUSER] = buser; }
MxU32	AXITransaction::get_buser() const
{
	MxU32 ret = masterFlags[AXI_MF_BUSER];
	return ret;
}

void	AXITransaction::activateChannelAW()			{ masterFlags[AXI_MF_AW_INFO] |= AXI_ACTIVE_CHANNEL_AW; }
void	AXITransaction::inactivateChannelAW()		{ masterFlags[AXI_MF_AW_INFO] &= ~AXI_ACTIVE_CHANNEL_AW; }


void AXITransaction::clear()
{
	inactivateChannelAW();
}





// ------------------------------------------------------------
// related to driveTransaction()/notifyEvent() function calls
// ------------------------------------------------------------

MxU32	AXITransaction::getChannel() const	{ return masterFlags[AXI_MF_CHANNEL]; }
bool	AXITransaction::isChannelAR() const	{ return (masterFlags[AXI_MF_CHANNEL] & AXI_CHANNEL_AR) != 0; }
bool	AXITransaction::isChannelAW() const	{ return (masterFlags[AXI_MF_CHANNEL] & AXI_CHANNEL_AW) != 0; }
bool	AXITransaction::isChannelR () const	{ return (masterFlags[AXI_MF_CHANNEL] & AXI_CHANNEL_R ) != 0; }
bool	AXITransaction::isChannelW () const	{ return (masterFlags[AXI_MF_CHANNEL] & AXI_CHANNEL_W ) != 0; }
bool	AXITransaction::isChannelB () const	{ return (masterFlags[AXI_MF_CHANNEL] & AXI_CHANNEL_B ) != 0; }

bool	AXITransaction::isLastCall() const	{ return (masterFlags[AXI_MF_CHANNEL] & AXI_TRANSFER_LAST) != 0; }
void	AXITransaction::setLastCall()		{ masterFlags[AXI_MF_CHANNEL] |= AXI_TRANSFER_LAST; }
void	AXITransaction::unsetLastCall()		{ masterFlags[AXI_MF_CHANNEL] &= ~AXI_TRANSFER_LAST; }

void	AXITransaction::setInvalidTransaction()				{ masterFlags[AXI_MF_CHANNEL] |= AXI_TRANSFER_EMPTY; }
bool	AXITransaction::isValidTransaction() const			{ return (masterFlags[AXI_MF_CHANNEL] & AXI_TRANSFER_EMPTY) == 0; }



// ------------------------------------------------------------
// related to main transaction settings (cont.)
// ------------------------------------------------------------

MxTransactionAccess AXITransaction::getAccessType() const { return access; }

// read/write address channel
void	AXITransaction::set_id			(const MxU32 id)	{ masterFlags[AXI_MF_ID] = id; }
MxU32	AXITransaction::get_id			() const			{ return masterFlags[AXI_MF_ID]; }

void	AXITransaction::set_addr		(const MxU64 _addr) { addr = _addr; }
MxU64	AXITransaction::get_addr		() const			{ return addr; }

MxU64	AXITransaction::get_addrOfBeat	(MxU32 beat) const
{
	MxU64 ret;


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// From AMBA AXI Protocol v1.0 Specification:
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// 4.5 Burst address
	//
	// This section provides some simple formulas for determining the address and
	// byte lanes of transfers within a burst. The formulas use the following variables:
	//
	//  * Start_Address
	//    The start address issued by the master.
	//
	//  * Number_Bytes
	//    The maximum number of bytes in each data transfer.
	//
	//  * Data_Bus_Bytes
	//    The number of byte lanes in the data bus.
	//
	//  * Aligned_Address
	//    The aligned version of the start address.
	//
	//  * Burst_Length
	//    The total number of data transfers within a burst.
	//
	//  * Address_N
	//    The address of transfer N within a burst. N is an integer from 2-16.
	//
	//  * Wrap_Boundary
	//    The lowest address within a wrapping burst.
	//
	//  * Lower_Byte_Lane
	//    The byte lane of the lowest addressed byte of a transfer.
	//
	//  * Upper_Byte_Lane
	//    The byte lane of the highest addressed byte of a transfer.
	//
	//  * INT(x) The rounded-down integer value of x.
	// 
	// Use these equations to determine addresses of transfers within a burst:
	//
	//  * Start_Address = ADDR
	//  * Number_Bytes = 2^SIZE
	//  * Burst_Length = LEN + 1
	//  * Aligned_Address = (INT(Start_Address / Number_Bytes) ) x Number_Bytes.
	// 
	// Use this equation to determine the address of the first transfer in a burst:
	// 
	//  * Address_1 = Start_Address.
	// 
	// Use this equation to determine the address of any transfer after the first transfer in a burst:
	// 
	//  * Address_N = Aligned_Address + (N - 1) x Number_Bytes.
	// 
	// For wrapping bursts, the Wrap_Boundary variable is extended to account for the wrapping boundary:
	//
	//  * Wrap_Boundary = (INT(Start_Address / (Number_Bytes x Burst_Length))) x (Number_Bytes x Burst_Length).
	// 
	// If Address_N = Wrap_Boundary + (Number_Bytes x Burst_Length), use this equation:
	//
	//  * Address_N = Wrap_Boundary.
	// 
	// Use these equations to determine which byte lanes to use for the first transfer in a burst:
	//
	//  * Lower_Byte_Lane = Start_Address - (INT(Start_Address / Data_Bus_Bytes)) x Data_Bus_Bytes
	//  * Upper_Byte_Lane = Aligned_Address + (Number_Bytes - 1) - (INT(Start_Address / Data_Bus_Bytes)) x Data_Bus_Bytes.
	// 
	// Use these equations to determine which byte lanes to use for all transfers after the first transfer in a burst:
	//
	//  * Lower_Byte_Lane = Address_N - (INT(Address_N / Data_Bus_Bytes)) x Data_Bus_Bytes
	//  * Upper_Byte_Lane = Lower_Byte_Lane + Number_Bytes - 1.
	// 
	// Data is transferred on:
	//
	//  * DATA[(8 x Upper_Byte_Lane) + 7 : (8 x Lower_Byte_Lane)].
	//
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	if (beat == 0)
	{
		// the easier case...
		ret = get_addr();
	}
	else
	{
		// beat > 0

		//
		// ---------------------------------------------------------------
		//   dataSize                 alignMask
		// ---------------------------------------------------------------
		//    1 Byte  = 0x01 Bytes   0xFFFFFFFF = ~0x00 = ~(dataSize - 1)
		//    2 Bytes = 0x02 Bytes   0xFFFFFFFE = ~0x01 = ~(dataSize - 1)
		//    4 Bytes = 0x04 Bytes   0xFFFFFFFC = ~0x03 = ~(dataSize - 1)
		//    8 Bytes = 0x08 Bytes   0xFFFFFFF8 = ~0x07 = ~(dataSize - 1)
		//   16 Bytes = 0x10 Bytes   0xFFFFFFF0 = ~0x0F = ~(dataSize - 1)
		//   32 Bytes = 0x20 Bytes   0xFFFFFFE0 = ~0x1F = ~(dataSize - 1)
		//   64 Bytes = 0x40 Bytes   0xFFFFFFC0 = ~0x3F = ~(dataSize - 1)
		//  128 Bytes = 0x80 Bytes   0xFFFFFF80 = ~0x7F = ~(dataSize - 1)
		// ---------------------------------------------------------------
		//

		switch (get_burst())
		{
		case AXI_BURST_FIXED:	// no address increment (FIFO type)
			ret = get_addr();
			break;
		case AXI_BURST_INCR:	// address increment
		{
			MxU64 aligned_addr = get_addr() & ~(((MxU64)dataSize) - 1);
			ret = aligned_addr + dataSize * beat;
			break;
		}
		case AXI_BURST_WRAP:	// wrapped address increment (Cache type)
		{
			// AXI restriction: The start address must be aligned to the size of the transfer! (Thus we need not to check!)/home/lfenneke/pub/abecker
			assert(get_addr() == (get_addr() & ~(((MxU64)dataSize) - 1)));

            MxU64 wrap_region_size = dataSize * dataBeats;
            MxU64 wrap_region_addr = get_addr() & (wrap_region_size-1);
            MxU64 offset = dataSize*beat;

            if (wrap_region_addr+offset >= wrap_region_size)
            {
                ret = get_addr()-(wrap_region_size-offset);
            }
            else
            {
                ret = get_addr()+offset;
            }
			break;
		}
		default:				// reserved or invalid!
			assert(!"Invalid burst type!");
			ret = MxU64CONST(0xdeadbeefdeadbeef);
			break;
		}
	}

	return ret;
}




MxU64	AXITransaction::get_addrOfCurrentBeat () const
{
	MxU32 beat = getCurrentBeat();
	MxU64 ret = get_addrOfBeat(beat);

	return ret;
}

MxU64	AXITransaction::get_addrOfNextBeat () const
{
	MxU32 beat = getNextBeat();
	MxU64 ret = get_addrOfBeat(beat);

	return ret;
}



// ------------------------------------------------------------
// related to handshake signals (in hardware: xxvalid, xxready)
// ------------------------------------------------------------

bool	AXITransaction::isActiveValid		() const	{ return (status[cts] != MX_MASTER_WAIT); }
bool	AXITransaction::isActiveReady		() const	{ return (status[cts] >= MX_SLAVE_READY); }
bool	AXITransaction::isActiveValidAW		() const	{ return (masterFlags[AXI_MF_AW_INFO] != 0); }
bool	AXITransaction::isActiveReadyAW		() const	{ return (masterFlags[AXI_MF_AW_INFO] != 0) && (status[AXI_STEP_ADDRESS] >= MX_SLAVE_READY); }

bool	AXITransaction::isValid			() const
{
	bool bRet = (status[lastActiveCts()] >= MX_MASTER_READY);
	return bRet;
}

bool	AXITransaction::isReady			() const
{
	MxU32 ret = (status[lastActiveCts()] >= MX_SLAVE_READY); 
	return ret;
}


bool	AXITransaction::isTransferFinished_aw() const
{
	bool bRet = (status[AXI_STEP_ADDRESS] >= MX_SLAVE_READY);
	return bRet;
}

bool	AXITransaction::isTransferFinished_ar() const
{
	bool bRet = (status[AXI_STEP_ADDRESS] >= MX_SLAVE_READY);
	return bRet;
}

bool	AXITransaction::isTransferFinished_r() const
{
	bool bRet = (access == MX_ACCESS_READ) && (lastActiveCts() > 0) && (status[lastActiveCts()] >= MX_SLAVE_READY);
	return bRet;
}

bool	AXITransaction::isTransferFinished_w() const
{
	bool bRet = (access == MX_ACCESS_WRITE) && (lastActiveCts() > 0) && (status[lastActiveCts()] >= MX_SLAVE_READY);
	return bRet;
}
bool	AXITransaction::isTransferFinished_b() const
{
	bool bRet = (status[nts - 1] >= MX_SLAVE_READY);
	return bRet;
}

bool	AXITransaction::isTransferFinishedFirstTime_r() const
{
	bool bRet = (access == MX_ACCESS_READ) && (cts > 0) && (status[cts] >= MX_SLAVE_READY);
	return bRet;
}

bool	AXITransaction::isTransferFinishedFirstTime_w() const
{
	const bool bRet = (access == MX_ACCESS_WRITE) && (cts > 0) && (status[cts] >= MX_SLAVE_READY);
	return bRet;
}

bool	AXITransaction::isTransactionFinished() const
{
	const bool bRet = (status[nts - 1] >= MX_SLAVE_READY);
	assert(!bRet || (cts == (nts - 1)));
	return bRet;
}





// ------------------------------------------------------------
// Helpers for preparing values in update and adopting them in communicate
// ------------------------------------------------------------

void	AXITransaction::setNextStrb(MxU32 data)		{ m_nextStrb = data; }
MxU32	AXITransaction::getNextStrb() const		    { return m_nextStrb; }

void	AXITransaction::prepareNextCts()			{ m_nextCts = cts + 1; }
void	AXITransaction::adoptNextCts()				{ cts = m_nextCts; }

bool	AXITransaction::isReadyFor_r() const		{ return (status[AXI_STEP_ADDRESS] >= MX_SLAVE_READY); }
bool	AXITransaction::isReadyFor_b() const		{ return ((status[nts-2] >= MX_SLAVE_READY) && (status[AXI_STEP_ADDRESS] >= MX_SLAVE_READY)); }


// ------------------------------------------------------------
// related to data and strobe alignment
// ------------------------------------------------------------
void	AXITransaction::alignWrDataAndStrobeSw2Hw		(const MxU32 beat, const MxU32* srcData, const MxU32 srcStrb)
{
	MxU32	dataBitWidth	= dataSize << 3;
	MxU32	busBitWidth		= getDataBitwidth();
	MxU32	address			= (MxU32)(get_addrOfBeat(beat));
	MxU32	shl;
	MxU32*	dstData			= getP_setWriteData(beat);
	MxU32&	dstStrb			= masterFlags[AXI_MF_DATA_STROBE];

	switch (busBitWidth)
	{
	case 32:
	{
		MxU32 mask, tmp;

		shl = (address & 3);
		dstStrb = srcStrb << shl;
		shl<<= 3;
		dataBitWidth-= (((dataBitWidth >> 3)-1) & address) << 3;			
		mask = dataBitWidth < 32 ? ((MxU32)(-1)) << dataBitWidth : 0;
		mask = ~mask << shl;
		tmp = (srcData[0] << shl) & mask;
		//tmp = (dstData[0] & ~mask) | tmp;
		dstData[0] = tmp;
		break;
	}
	case 64:
	{
		MxU64 mask, src/*, dst*/;

		shl = (address & 7);
		dstStrb = srcStrb << shl;
		shl<<= 3;
		dataBitWidth-= (((dataBitWidth >> 3)-1) & address) << 3;			
		mask = dataBitWidth < 64 ? MxU64CONST(0xFFFFFFFFFFFFFFFF) << dataBitWidth : 0;
		mask = ~mask << shl;

		src = ((MxU64)(srcData[1])) << 32;
		src|= ((MxU64)(srcData[0]));		
		src = (src << shl) & mask;

		//dst = ((MxU64)(dstData[1])) << 32;
		//dst|= ((MxU64)(dstData[0]));		
		//dst = (dst & ~mask) | src;

		//dstData[0] = dst;
		//dstData[1] = dst >> 32;
		dstData[0] = MxU32(src);
		dstData[1] = MxU32(src >> 32);
		break;
	}
    case 128:
    {
        MxU64 src[2];

        src[0] = ((MxU64)(srcData[1])) << 32;
    	src[0]|= ((MxU64)(srcData[0]));		
   		src[1] = ((MxU64)(srcData[3])) << 32;
       	src[1]|= ((MxU64)(srcData[2]));		

        shl = (address & 15);
        dstStrb = srcStrb;

        if (shl > 0)
        {
            dstStrb<<= shl;
            shl<<= 3;
            if (shl < 64)
            {
                src[1] = (src[0] >> (64-shl)) | (src[1] << shl);
                src[0]<<= shl;
            }
            else
            {
                src[1] = src[0] << (shl-64);
                src[0] = 0;
            }
        }
		dstData[0] = MxU32(src[0]);
		dstData[1] = MxU32(src[0] >> 32);
		dstData[2] = MxU32(src[1]);
		dstData[3] = MxU32(src[1] >> 32);
        break;
    }        
	default:
		assert(!"Unsupported bus width!");
		break;
	}
}
void	AXITransaction::alignWrDataSw2Hw			(const MxU32 beat, const MxU32* srcData)
{
	MxU32	dataBitWidth	= dataSize << 3;
	MxU32	busBitWidth		= getDataBitwidth();
	MxU32	address			= (MxU32)(get_addrOfBeat(beat));
	MxU32	shl;
	MxU32*	dstData			= getP_setWriteData(beat);

	switch (busBitWidth)
	{
	case 32:
	{
		MxU32 mask, tmp;

		shl = (address & 3);
		shl<<= 3;
		dataBitWidth-= (((dataBitWidth >> 3)-1) & address) << 3;			
		mask = dataBitWidth < 32 ? ((MxU32)(-1)) << dataBitWidth : 0;
		mask = ~mask << shl;
		tmp = (srcData[0] << shl) & mask;
		dstData[0] = tmp;
		break;
	}
	case 64:
	{
		MxU64 mask, src/*, dst*/;

		shl = (address & 7);
		shl<<= 3;
		dataBitWidth-= (((dataBitWidth >> 3)-1) & address) << 3;			
		mask = dataBitWidth < 64 ? MxU64CONST(0xFFFFFFFFFFFFFFFF) << dataBitWidth : 0;
		mask = ~mask << shl;

		src = ((MxU64)(srcData[1])) << 32;
		src|= ((MxU64)(srcData[0]));		
		src = (src << shl) & mask;

		dstData[0] = MxU32(src);
		dstData[1] = MxU32(src >> 32);
		break;
	}
    case 128:
    {
        MxU64 src[2];

        src[0] = ((MxU64)(srcData[1])) << 32;
    	src[0]|= ((MxU64)(srcData[0]));		
   		src[1] = ((MxU64)(srcData[3])) << 32;
       	src[1]|= ((MxU64)(srcData[2]));		

        shl = (address & 15);
        if (shl > 0)
        {
            shl<<= 3;
            if (shl < 64)
            {
                src[1] = (src[0] >> (64-shl)) | (src[1] << shl);
                src[0]<<= shl;
            }
            else
            {
                src[1] = src[0] << (shl-64);
                src[0] = 0;
            }
        }
		dstData[0] = MxU32(src[0]);
		dstData[1] = MxU32(src[0] >> 32);
		dstData[2] = MxU32(src[1]);
		dstData[3] = MxU32(src[1] >> 32);
        break;
    }        
	default:
		assert(!"Unsupported bus width!");
		break;
	}
}
void	AXITransaction::alignRdDataSw2Hw			(const MxU32 beat, const MxU32* srcData)
{
	MxU32	dataBitWidth	= dataSize << 3;
	MxU32	busBitWidth		= getDataBitwidth();
	MxU32	address			= (MxU32)(get_addrOfBeat(beat));
	MxU32	shl;
	MxU32*	dstData			= getP_setReadData(beat);

	switch (busBitWidth)
	{
	case 32:
	{
		MxU32 mask, tmp;

		shl = (address & 3);
		shl<<= 3;
		dataBitWidth-= (((dataBitWidth >> 3)-1) & address) << 3;			
		mask = dataBitWidth < 32 ? ((MxU32)(-1)) << dataBitWidth : 0;
		mask = ~mask << shl;
		tmp = (srcData[0] << shl) & mask;
		dstData[0] = tmp;
		break;
	}
	case 64:
	{
		MxU64 mask, src/*, dst*/;

		shl = (address & 7);
		shl<<= 3;
		dataBitWidth-= (((dataBitWidth >> 3)-1) & address) << 3;			
		mask = dataBitWidth < 64 ? MxU64CONST(0xFFFFFFFFFFFFFFFF) << dataBitWidth : 0;
		mask = ~mask << shl;

		src = ((MxU64)(srcData[1])) << 32;
		src|= ((MxU64)(srcData[0]));		
		src = (src << shl) & mask;

		dstData[0] = MxU32(src);
		dstData[1] = MxU32(src >> 32);
		break;
	}
    case 128:
    {
        MxU64 src[2];

        src[0] = ((MxU64)(srcData[1])) << 32;
    	src[0]|= ((MxU64)(srcData[0]));		
   		src[1] = ((MxU64)(srcData[3])) << 32;
       	src[1]|= ((MxU64)(srcData[2]));		

        shl = (address & 15);
        if (shl > 0)
        {
            shl<<= 3;
            if (shl < 64)
            {
                src[1] = (src[0] >> (64-shl)) | (src[1] << shl);
                src[0]<<= shl;
            }
            else
            {
                src[1] = src[0] << (shl-64);
                src[0] = 0;
            }
        }
		dstData[0] = MxU32(src[0]);
		dstData[1] = MxU32(src[0] >> 32);
		dstData[2] = MxU32(src[1]);
		dstData[3] = MxU32(src[1] >> 32);
        break;
    }        
	default:
		assert(!"Unsupported bus width!");
		break;
	}
}
void	AXITransaction::alignStrobeSw2Hw		(const MxU32 beat, const MxU32 srcStrb)
{
	MxU32	busBitWidth		= getDataBitwidth();
	MxU32	address			= (MxU32)(get_addrOfBeat(beat));
	MxU32	shl;
	MxU32&	dstStrb			= masterFlags[AXI_MF_DATA_STROBE];

	switch (busBitWidth)
	{
	case 32:
	{
		shl = (address & 3);
		dstStrb = srcStrb << shl;
		break;
	}
	case 64:
	{
		shl = (address & 7);
		dstStrb = srcStrb << shl;
		break;
	}
    case 128:
    {
		shl = (address & 15);
		dstStrb = srcStrb << shl;
        break;
    }
	default:
		assert(!"Unsupported bus width!");
		break;
	}
}

void	AXITransaction::alignWrDataAndStrobeHw2Sw		(const MxU32 beat, MxU32* dstData, MxU32& dstStrb)
{
	MxU32	busBitWidth		= getDataBitwidth();
	MxU32	address			= (MxU32)(get_addrOfBeat(beat));
	MxU32	shr;
	MxU32*	srcData			= getP_getWriteData(beat);
	MxU32	srcStrb			= masterFlags[AXI_MF_DATA_STROBE];

	switch (busBitWidth)
	{
	case 32:
	{
		shr = (address & 3);
		dstStrb = srcStrb >> shr;
		shr<<= 3;
		dstData[0] = srcData[0] >> shr;
		break;
	}
	case 64:
	{
		MxU64 tmp;
		
		shr = (address & 7);
		dstStrb = srcStrb >> shr;
		shr<<= 3;
		tmp = (((MxU64)(srcData[1])) << 32) | srcData[0];
		tmp>>= shr;
		dstData[0] = MxU32(tmp);
		dstData[1] = MxU32(tmp >> 32);
		break;
	}
    case 128:
    {
		MxU64 tmp[2];
		
        tmp[0] = (((MxU64)(srcData[1])) << 32) | srcData[0];
        tmp[1] = (((MxU64)(srcData[3])) << 32) | srcData[2];
        dstStrb = srcStrb;

		shr = (address & 15);
        if (shr > 0)
        {
            dstStrb>>= shr;
		    shr<<= 3;

            if (shr < 64)
            {
                tmp[0] = (tmp[0] >> shr) | (tmp[1] << (64-shr));  
                tmp[1]>>= shr;
            }
            else
            {
                tmp[0] = tmp[1] >> (shr-64);
                tmp[1] = 0;
            }
        }
		dstData[0] = MxU32(tmp[0]);
		dstData[1] = MxU32(tmp[0] >> 32);
		dstData[2] = MxU32(tmp[1]);
		dstData[3] = MxU32(tmp[1] >> 32);
        break;
    }
	default:
		assert(!"Unsupported bus width!");
		break;
	}
}
void	AXITransaction::alignWrDataHw2Sw			(const MxU32 beat, MxU32* dstData)
{
	MxU32	busBitWidth		= getDataBitwidth();
	MxU32	address			= (MxU32)(get_addrOfBeat(beat));
	MxU32	shr;
	MxU32*	srcData			= getP_getWriteData(beat);

	switch (busBitWidth)
	{
	case 32:
	{
		shr = (address & 3);
		//dstStrb = srcStrb >> shr;
		shr<<= 3;
		dstData[0] = srcData[0] >> shr;
		break;
	}
	case 64:
	{
		MxU64 tmp;
		
		shr = (address & 7);
		//dstStrb = srcStrb >> shr;
		shr<<= 3;
		tmp = (((MxU64)(srcData[1])) << 32) | srcData[0];
		tmp>>= shr;
		dstData[0] = MxU32(tmp);
		dstData[1] = MxU32(tmp >> 32);
		break;
	}
    case 128:
    {
		MxU64 tmp[2];
		
        tmp[0] = (((MxU64)(srcData[1])) << 32) | srcData[0];
        tmp[1] = (((MxU64)(srcData[3])) << 32) | srcData[2];

		shr = (address & 15);
        if (shr > 0)
        {
		    shr<<= 3;
            if (shr < 64)
            {
                tmp[0] = (tmp[0] >> shr) | (tmp[1] << (64-shr));  
                tmp[1]>>= shr;
            }
            else
            {
                tmp[0] = tmp[1] >> (shr-64);
                tmp[1] = 0;
            }
        }
		dstData[0] = MxU32(tmp[0]);
		dstData[1] = MxU32(tmp[0] >> 32);
		dstData[2] = MxU32(tmp[1]);
		dstData[3] = MxU32(tmp[1] >> 32);
        break;
    }
    default:
		assert(!"Unsupported bus width!");
		break;
	}
}
void	AXITransaction::alignRdDataHw2Sw			(const MxU32 beat, MxU32* dstData)
{
	MxU32	busBitWidth		= getDataBitwidth();
	MxU32	address			= (MxU32)(get_addrOfBeat(beat));
	MxU32	shr;
	MxU32*	srcData			= getP_getReadData(beat);

	switch (busBitWidth)
	{
	case 32:
	{
		shr = (address & 3);
		//dstStrb = srcStrb >> shr;
		shr<<= 3;
		dstData[0] = srcData[0] >> shr;
		break;
	}
	case 64:
	{
		MxU64 tmp;
		
		shr = (address & 7);
		//dstStrb = srcStrb >> shr;
		shr<<= 3;
		tmp = (((MxU64)(srcData[1])) << 32) | srcData[0];
		tmp>>= shr;
		dstData[0] = MxU32(tmp);
		dstData[1] = MxU32(tmp >> 32);
		break;
	}
    case 128:
    {
		MxU64 tmp[2];
		
        tmp[0] = (((MxU64)(srcData[1])) << 32) | srcData[0];
        tmp[1] = (((MxU64)(srcData[3])) << 32) | srcData[2];

		shr = (address & 15);
        if (shr > 0)
        {
		    shr<<= 3;
            if (shr < 64)
            {
                tmp[0] = (tmp[0] >> shr) | (tmp[1] << (64-shr));  
                tmp[1]>>= shr;
            }
            else
            {
                tmp[0] = tmp[1] >> (shr-64);
                tmp[1] = 0;
            }
        }
		dstData[0] = MxU32(tmp[0]);
		dstData[1] = MxU32(tmp[0] >> 32);
		dstData[2] = MxU32(tmp[1]);
		dstData[3] = MxU32(tmp[1] >> 32);
        break;
    }
	default:
		assert(!"Unsupported bus width!");
		break;
	}
}
void	AXITransaction::alignStrobeHw2Sw(const MxU32 beat, MxU32& dstStrb) const
{
	MxU32	busBitWidth		= getDataBitwidth();
	MxU32	address			= (MxU32)(get_addrOfBeat(beat));
	MxU32	shr;
	MxU32	srcStrb			= masterFlags[AXI_MF_DATA_STROBE];

	switch (busBitWidth)
	{
	case 32:
	{
		shr = (address & 3);
		dstStrb = srcStrb >> shr;
		break;
	}
	case 64:
	{
		shr = (address & 7);
		dstStrb = srcStrb >> shr;
		break;
	}
	case 128:
	{
		shr = (address & 15);
		dstStrb = srcStrb >> shr;
		break;
	}
	default:
		assert(!"Unsupported bus width!");
		break;
	}
}



// ------------------------------------------------------------
// debugging related
// ------------------------------------------------------------

// only for debugging purpose
string AXITransaction::getDbgInfo_headline()
{
	return "V R channel cts nts r/w addr     id       data    ";
}

inline string AXITransaction::getDbgInfo_infoline()
{
	string s;
	char cstr[32];
///		MxU32 cts = cts;
	s += (status[cts] >= MX_MASTER_READY) ? "V" : "-";	// valid
	s += " ";
	s += (status[cts] >= MX_SLAVE_READY)  ? "R" : "-";	// ready
	s += "  ";

	MxU32 mf_channel = masterFlags[AXI_MF_CHANNEL];
	// longest aw+w EL
	//         1234567
	switch (mf_channel & ~(AXI_TRANSFER_LAST | AXI_TRANSFER_EMPTY))	// take the real channel information only
	{
	case AXI_CHANNEL_AW:
		s += "aw  ";
		break;
	case (AXI_CHANNEL_AW|AXI_CHANNEL_W):
		s += "aw+w";
		break;
	case AXI_CHANNEL_AR:
		s += "ar  ";
		break;
	case AXI_CHANNEL_W:
		s += " w  ";
		break;
	case AXI_CHANNEL_R:
		s += " r  ";
		break;
	case AXI_CHANNEL_B:
		s += " b  ";
		break;
	default:
		s += "????";
		break;
	}
	s += (mf_channel & AXI_TRANSFER_EMPTY) ? "E" : " ";
	s += (mf_channel & AXI_TRANSFER_LAST)  ? "L" : " ";
	s += "  ";
	sprintf(cstr, "%2d", cts);									// cts
	s += cstr;
	s += "  ";
	sprintf(cstr, "%2d", nts);							// nts
	s += cstr;
	s += " ";
	s += (access == MX_ACCESS_WRITE) ? "Wr" : ((access == MX_ACCESS_READ) ? "Rd" : "??");
	s += "  ";
#ifdef WIN32
	sprintf(cstr, "%016I64X", addr);							// addr
#else
	sprintf(cstr, "%016llX", addr);							// addr
#endif    
    s += cstr;
	s += " ";
	sprintf(cstr, "%08X", masterFlags[AXI_MF_ID]);							// id
	s += cstr;
	s += " ";
	if ((cts > AXI_STEP_ADDRESS) && (cts < ((access == MX_ACCESS_WRITE) ? (nts - 1) : nts)))
	{
		// data to be displayed
		sprintf(cstr, "%08X", (access == MX_ACCESS_WRITE) ? dataWr[cts - 1] : dataRd[cts - 1]);							// addr
		s += cstr;
	}
	else
	{
		// no data for current cts
		s += "        ";
	}
	//s += " ";

	return s;
}

// ------------------------------------------------------------
// Persistence related
// ------------------------------------------------------------
bool AXITransaction::persist(persister & p)
{
    if (p.Saving())
    {
        saveData(p.AsOStream());
    }
    else
    {
        restoreData(p.AsIStream());
    }

// Copied and mangled from  bool MxTransactionInfo::saveData( MxODataStream & out )
// 
//     p.persist(nts);
//     p.persist(cts);
//     for (MxU32 step = 0; step < numSteps; ++step)
//     {
//         p.persistEnum(status[step]);
//     }
//     p.persistEnum(notify);
//     p.persistEnum(access);
//     p.persist(addr);
//     p.persist(dataSize);
//     p.persist(dataBeats);
//     p.persist(dataRd, numData);
//     p.persist(dataWr, numData);
//     p.persist(slaveFlags, numSFlags);
//     p.persist(masterFlags, numMFlags);

    // Own variables

    p.persistPointer(m_pACIrelatedSPortInfoInt);
    p.persistPointer(m_pACIrelatedMPortInfoExt);

    p.persist(m_ACISPortIdxInt);
    p.persist(m_ACIMPortIdxInt);
    p.persist(m_ACISPortIdxExt);
    p.persist(m_ACIMPortIdxExt);

    p.persistPointer(m_pPad);
    p.persist(m_padVersion);

    p.persist(m_dataBitwidth);
    p.persist(m_dataMxU32width);

    p.persist(m_nextCts);
    p.persist(m_nextStrb);

    return true;
}

bool AXITransaction::hookup(std::map<AXITransaction*,AXITransaction*> & mapOldToNew, persister & p)
{
    // SimpleHeap related
    bool result = hookupMappedPointer(mapOldToNew, nextFreeP);                    

    // ACI related
    // result = result && hookupMappedPointer(mapOldToNew, m_pACIrelatedSPortInfoInt);    
    // result = result && hookupMappedPointer(mapOldToNew, m_pACIrelatedMPortInfoExt);    

    return result;
}

inline void AXITransaction::getValidDataMaskOnBus (MxU32* dataMask, MxU64& addr64, MxU32 busBitwidth, MxU32 dataSize)
{
    MxU32 mask              = ~((busBitwidth >> 3)-1);
    MxU32 addr              = (MxU32)addr64;
    MxU32 addrAlignedBus    = addr & mask;
    MxU32 addrAligned32     = addr & ~0x3;
    MxU32 addrAlignedSize   = addr & ~(dataSize-1);
    MxU32 index             = (addrAligned32-addrAlignedBus) >> 2;
    MxU32 mask32            = 0xffffffff;
    MxU32 num32             = (dataSize+3) >> 2;

    if (dataSize < 4)
    {
        mask32 =~(mask32 << (dataSize << 3));
    }
    mask32<<= (addr-addrAligned32) << 3;
    if (addrAlignedSize < addrAligned32)
    {
        num32-= (addrAligned32-addrAlignedSize) >> 2;
    }
    memset(dataMask, 0, busBitwidth >> 3);
    dataMask[index] = mask32;
    for (MxU32 i = 1; i < num32; i++)
    {
        dataMask[++index] = 0xffffffff;
    }
    addr64&= (MxU64)mask;
}

inline void AXITransaction::getValidDataMaskOnBus (MxU32* dataMask, MxU64& addr64, MxU32 busBitwidth, MxU32 dataSize, MxU32 strb)
{
    static const MxU32 strb2Mask[16] = { 0x00000000, 0x000000FF, 0x0000FF00, 0x0000FFFF,
                                         0x00FF0000, 0x00FF00FF, 0x00FFFF00, 0x00FFFFFF,
                                         0xFF000000, 0xFF0000FF, 0xFF00FF00, 0xFF00FFFF,
                                         0xFFFF0000, 0xFFFF00FF, 0xFFFFFF00, 0xFFFFFFFF };

    MxU32 mask              = ~((busBitwidth >> 3)-1);
    MxU32 addr              = (MxU32)addr64;
    MxU32 addrAlignedBus    = addr & mask;
    MxU32 addrAligned32     = addr & ~0x3;
    MxU32 addrAlignedSize   = addr & ~(dataSize-1);
    MxU32 index             = (addrAligned32-addrAlignedBus) >> 2;
    MxU32 mask32            = 0xffffffff;
    MxU32 num32             = (dataSize+3) >> 2;

    if (dataSize < 4)
    {
        mask32 =~(mask32 << (dataSize << 3));
    }
    mask32<<= (addr-addrAligned32) << 3;
    if (addrAlignedSize < addrAligned32)
    {
        num32-= (addrAligned32-addrAlignedSize) >> 2;
    }
    memset(dataMask, 0, busBitwidth >> 3);

    strb>>= (index << 2);
    mask32&= strb2Mask[strb & 0xf];
    dataMask[index] = mask32;
    for (MxU32 i = 1; i < num32; i++)
    {
        strb>>= 4;
        mask32 = strb2Mask[strb & 0xf];
        dataMask[++index] = mask32;
    }
    addr64&= (MxU64)mask;
}



/*! \class AXI_SR_Transaction
*  \brief AXI save and restore master passes this over debugTransaction to the slave to set up pointers.
*/
class AXI_SR_Transaction : public MxTransactionInfo
{
public:
    typedef enum { AXI_SR_START = 0, AXI_SR_OLD_NEW, AXI_SR_STOP } AXI_SR;

    AXI_SR              channel;        //!< Which AXI channel the transaction belongs in
    AXITransaction *    oldTransaction; //!< Address when saved
    AXITransaction *    newTransaction; //!< Address after restore
};

#include "ContainerAXITransactionNextCts.h"

// ==========================================================================================
// Master port
// ==========================================================================================
// AXI master port class
class MxAXISingleMasterPort : public MxTransactionMasterPort
{
public:
	enum Misc { MAX_NUM_AXI_CHANNELS_ON_HEAP = 5 };

	struct MasterSingleChannelStages
	{
		SimpleHeap<AXITransaction, MAX_NUM_AXI_CHANNELS_ON_HEAP> heap;
		AXITransaction * aw;
		AXITransaction * ar;
		AXITransaction * w;
		AXITransaction * r;
		AXITransaction * b;
		AXITransaction * finished_write;	// for master to avoid allocating new transaction with a pointer which is still in use
		AXITransaction * finished_read;		// for master to avoid allocating new transaction with a pointer which is still in use
        MasterSingleChannelStages() { reset(); }
		void reset() { aw = ar = w = r = b = finished_write = finished_read = 0; heap.reset(); }
        bool mappedPersist(std::map<AXITransaction*,AXITransaction*> & mapOldToNew, persister & p)
        {
            bool result = heap.persistMappedObject(mapOldToNew, p);

            result = result && p.persistMappedPointer(mapOldToNew, aw);
            result = result && p.persistMappedPointer(mapOldToNew, ar);
            result = result && p.persistMappedPointer(mapOldToNew, w);
            result = result && p.persistMappedPointer(mapOldToNew, r);
            result = result && p.persistMappedPointer(mapOldToNew, b);
            result = result && p.persistMappedPointer(mapOldToNew, finished_write);
            result = result && p.persistMappedPointer(mapOldToNew, finished_read);

            return result;
        }
	};


	MxAXISingleMasterPort(sc_mx_module *_owner, const string& name, MxTransactionProperties *_prop=NULL/*, MxU32 _IDWidth=4*/);
	virtual ~MxAXISingleMasterPort();

	// get AXI Master version
	virtual const string getVersion() const { return string("2-0-001"); }

	void init(MxU32 dataBitwidth);

	void reset();
	void terminate();
	void communicate();
	void update();

	// 'Create' a new transaction for aw or ar channel, implicit this will be 'valid'
	AXITransaction * newTransaction(const MxTransactionAccess accessType);
	// Transaction in channel aw also in channel w, implicit this will be 'valid'
	AXITransaction * dupToW(AXITransaction * aw);



	class NotifyHandler : public MxNotifyHandlerIF
	{
	private:
		MxAXISingleMasterPort * owner;
	public:
		NotifyHandler() { owner = 0; }
		void setOwner(MxAXISingleMasterPort * _owner) { owner = _owner; }
		virtual ~NotifyHandler() { owner = NULL; }

		virtual void notifyEvent(MxTransactionInfo * mxTransactionInfoP);
	};
	friend class NotifyHandler;

	NotifyHandler notifyHandler;

private:
	sc_mx_module* m_owner;

	bool isConnected;

	// 'pipeline'
	MasterSingleChannelStages channelStages;

	ContainerAXITransactionNextCts<MAX_NUM_AXI_CHANNELS_ON_HEAP> containerAXITransactionNextCts;
	AXITransaction* awToBeInactivated; 

	struct NextHandshake
	{
		AXITransaction * awvalid;
		AXITransaction * arvalid;
		AXITransaction * wvalid;
		MxU8			 rready;
		MxU8			 bready;
        NextHandshake() 
        :awvalid(NULL)
        ,arvalid(NULL)
        ,wvalid(NULL)
        ,rready(0)
        ,bready(0)
        { }
		inline void reset() { awvalid = arvalid = wvalid = 0; rready = bready = 0; }
        bool mappedPersist(std::map<AXITransaction*,AXITransaction*> & mapOldToNew, persister & p)
        {
            p.persist(rready);
            p.persist(bready);

            bool result = p.persistMappedPointer(mapOldToNew, awvalid);
            result = result && p.persistMappedPointer(mapOldToNew, arvalid);
            result = result && p.persistMappedPointer(mapOldToNew, wvalid);

            return result;
        }
	};
	NextHandshake nextHandshake;

	struct IncomingTransactions
	{
		AXITransaction * r;
		AXITransaction * b;

        IncomingTransactions()
        :r(NULL)
        ,b(NULL)
        { }

		inline void clear() { r = b = 0; }
        bool mappedPersist(std::map<AXITransaction*,AXITransaction*> & mapOldToNew, persister & p)
        {
            bool result = p.persistMappedPointer(mapOldToNew, r);
            result = result && p.persistMappedPointer(mapOldToNew, b);

            return result;
        }
	};
	IncomingTransactions incomingTransactions;

	AXITransaction emptyMxTransactionInfo;	// for 'dummy' calls at empty channels


	// [C]
	inline void set_awvalid(AXITransaction* const axiTransactionP) const
	{
		assert(axiTransactionP);
		{
			axiTransactionP->status[AXI_STEP_ADDRESS] = MX_MASTER_READY;
			axiTransactionP->activateChannelAW();
		}
	}
	// [C]
	inline void set_arvalid(const AXITransaction* const axiTransactionP) const
	{
		assert(axiTransactionP);
		{
			axiTransactionP->status[AXI_STEP_ADDRESS] = MX_MASTER_READY;
		}
	}
	// [C]
	inline void set_wvalid(const AXITransaction* const axiTransactionP) const
	{
		assert(axiTransactionP);
		{
			axiTransactionP->status[axiTransactionP->cts] = MX_MASTER_READY;
		}
	}
	// [C]
	inline void set_rready(MxU8 data)
	{
		assert(data < 2);
		if (data)
		{
			assert(channelStages.r);
			AXITransaction * axiInfoP = channelStages.r;
			assert(axiInfoP->status[axiInfoP->cts] == MX_MASTER_READY);
			axiInfoP->status[axiInfoP->cts] = MX_SLAVE_READY;
		}
	}
	// [C]
	inline void set_bready(MxU8 data)
	{
		assert(data < 2);
		if (data)
		{
			assert(channelStages.b);
			AXITransaction * axiInfoP = channelStages.b;
			assert(axiInfoP->cts == (axiInfoP->nts - 1));
			assert(axiInfoP->status[axiInfoP->cts] == MX_MASTER_READY);
			axiInfoP->status[axiInfoP->cts] = MX_SLAVE_READY;
		}
	}

public:
	// [C]
	inline AXITransaction *	getNext_awvalid() { return nextHandshake.awvalid; }
	inline AXITransaction *	getNext_arvalid() { return nextHandshake.arvalid; }
	inline AXITransaction *	getNext_wvalid()  { return nextHandshake.wvalid;  }
	inline MxU8				getNext_rready() const  { return nextHandshake.rready;  }
	inline MxU8				getNext_bready() const  { return nextHandshake.bready;  }

	// ====== set the potential valid/ready flags (depending on pipeline they are really activated or not) ======
	// aw and ar are valid by default when the new transaction is requested by newTransaction(), also they don't need any cts increment
	// [U], [C] before this communicate is being executed
	inline void setNext_wvalid(AXITransaction* axiTransactionP) { nextHandshake.wvalid = axiTransactionP; }
	// [U]
	inline void setNext_rready(MxU8 data) { assert(data < 2); nextHandshake.rready = data; }
	// [U]
	inline void setNext_bready(MxU8 data) { assert(data < 2); nextHandshake.bready = data; }



	// [U]
	///inline MxU32 get_awready()	=> use isTransferFinished_aw()
	///inline MxU32 get_arready()	=> use isTransferFinished_ar()
	inline MxU32 get_rvalid() const
	{
		return (channelStages.r
				&& channelStages.r == incomingTransactions.r	// we must consider only the currently active transaction in channel r
				&& (channelStages.r->status[channelStages.r->cts] >= MX_MASTER_READY));
	}
	///inline MxU32 get_wready()	=> use isTransferFinished_w()
	inline MxU32 get_bvalid() const  { return (channelStages.b && (channelStages.b->status[channelStages.b->nts - 1] >= MX_MASTER_READY)); }

	// [U]
	inline bool	isTransferFinished_aw() const { return (channelStages.aw && (channelStages.aw->status[AXI_STEP_ADDRESS] >= MX_SLAVE_READY));}
	inline bool	isTransferFinished_ar() const { return (channelStages.ar && (channelStages.ar->status[AXI_STEP_ADDRESS] >= MX_SLAVE_READY));}
	inline bool	isTransferFinished_r() const
	{
		return (channelStages.r
				&& (channelStages.r->lastActiveCts() > 0)
				&& (channelStages.r->status[channelStages.r->lastActiveCts()] >= MX_SLAVE_READY));
	}
	inline bool	isTransferFinishedFirstTime_r() const
	{
		return (channelStages.r
				&& channelStages.r == incomingTransactions.r
				&& (channelStages.r->cts > 0)
				&& (channelStages.r->status[channelStages.r->cts] >= MX_SLAVE_READY));
	}
	inline bool	isTransferFinished_w() const
	{
		return (channelStages.w
				&& (channelStages.w->lastActiveCts() > 0)
				&& (channelStages.w->status[channelStages.w->lastActiveCts()] >= MX_SLAVE_READY));
	}
	inline bool	isTransferFinishedFirstTime_w()
	{
		return (channelStages.w
				&& channelStages.w == getNext_wvalid()
				&& (channelStages.w->cts > 0)
				&& (channelStages.w->status[channelStages.w->cts] >= MX_SLAVE_READY));
	}
	inline bool	isTransferFinished_b() const  
	{ 
		return (channelStages.b && (channelStages.b->status[channelStages.b->nts - 1] >= MX_SLAVE_READY)); 
	}


	inline AXITransaction* getChannel_aw() { return (channelStages.aw && (true) ? channelStages.aw : 0); }
	inline AXITransaction* getChannel_ar() { return (channelStages.ar && (true) ? channelStages.ar : 0); }
	inline AXITransaction* getChannel_w()  { return (channelStages.w  && (true) ? channelStages.w  : 0); }
	inline AXITransaction* getChannel_r()  { return (channelStages.r  && (true) ? channelStages.r  : 0); }
	inline AXITransaction* getChannel_b()  { return (channelStages.b  && (true) ? channelStages.b  : 0); }


	// Quick check
	inline bool isActive() const { return (channelStages.heap.getNumAllocated() != 0); }

	inline MasterSingleChannelStages& getChannelStages() { return channelStages; }

    /*! \brief Persist AXITransactions and pointers to them.
     *
     *  After setting this house in order this drives the slave via debugTransaction.
     */
    inline bool mappedPersist(std::map<AXITransaction*,AXITransaction*> & mapOldToNew, persister & p)
    {
        bool result = channelStages.mappedPersist(mapOldToNew, p);
        result = result && nextHandshake.mappedPersist(mapOldToNew, p);
        result = result && incomingTransactions.mappedPersist(mapOldToNew, p);
        result = result && p.persistMappedPointer(mapOldToNew, awToBeInactivated);

        return result;
    }

    /*! \brief Let AXI slave know this is the start of a restore
     *  \see driveSetupItems
     *  \see driveSetupStop
     */
    inline bool driveSetupStart(void)
    {
        AXI_SR_Transaction start;
        start.access = MX_ACCESS_USER;
        start.channel = AXI_SR_Transaction::AXI_SR_START;
        return MX_STATUS_OK == debugTransaction(&start);
    }

    inline bool driveSetupItem(AXITransaction* oldTransaction, AXITransaction* newTransaction)
    {
        AXI_SR_Transaction start;
        start.access = MX_ACCESS_USER;
        start.channel = AXI_SR_Transaction::AXI_SR_OLD_NEW;
        start.oldTransaction = oldTransaction;
        start.newTransaction = newTransaction;

        return MX_STATUS_OK == debugTransaction(&start);
    }

    /*! \brief Pass items on to the AXI slave for later hookup.
     */
    inline bool driveSetupItems(std::map<AXITransaction*,AXITransaction*> & mapOldToNew)
    {
        bool result(true);
        for (std::map<AXITransaction*,AXITransaction*>::const_iterator iter = mapOldToNew.begin(); result && iter != mapOldToNew.end(); ++iter)
        {
            result = driveSetupItem(iter->first, iter->second);
        }

        return result;
    }

    /*! \brief Let AXI slave know that all restored items have been passed over - slave can hookup pointers.
     *  \see driveSetupStart
     *  \see driveSetupItems
     */
    inline bool driveSetupStop(void)
    {
        AXI_SR_Transaction stop;
        stop.access = MX_ACCESS_USER;
        stop.channel = AXI_SR_Transaction::AXI_SR_STOP;
        return MX_STATUS_OK == debugTransaction(&stop);
    }
};







// ==========================================================================================
// Slave port (single)
// ==========================================================================================
// AXI slave port class
class MxAXISingleSlavePort : public MxTransactionSlave
{
public:
	enum Misc { MAX_NUM_TRANSACTIONS = 5 };

	struct SlaveSingleChannelStages
	{
        int numEntries;     //!< Quick check for activity, look for greater than 0
		AXITransaction * aw;
		AXITransaction * ar;
		AXITransaction * w;
		AXITransaction * r;
		AXITransaction * b;

        SlaveSingleChannelStages()
        :numEntries(0)
        ,aw(NULL)
        ,ar(NULL)
        ,w(NULL)
        ,r(NULL)
        ,b(NULL)
        { }

		void reset() { aw = ar = w = r = b = 0; numEntries = 0; }
		void add(AXITransaction * src, AXITransaction *& dst)
		{
			assert( src);
			assert(!dst);
			//
			dst = src;
			numEntries++;
		}
        void move(AXITransaction *& src, AXITransaction *& dst) const
		{
			assert( src);
			assert(!dst);
			//
			dst = src;
			src = 0;
		}
        void link(AXITransaction *& src, AXITransaction *& dst) const
		{
			assert( src);
			assert(!dst);
			//
			dst = src;
		}
        void unlink(AXITransaction *& element) const
		{
			assert(element);
			//
			element = 0;
		}
		void remove(AXITransaction *& element)
		{
			assert(element);
			//
			element = 0;
			numEntries--;
		}
        void persist(persister & p)
        {
            p.persist(numEntries);
            p.persistPointer(aw);
            p.persistPointer(ar);
            p.persistPointer(w);
            p.persistPointer(r);
            p.persistPointer(b);
        }
        bool hookupMappedPointers(std::map<AXITransaction*,AXITransaction*> & hookup)
        {
            bool result = hookupMappedPointer(hookup, aw);
            result = result && hookupMappedPointer(hookup, ar);
            result = result && hookupMappedPointer(hookup, w);
            result = result && hookupMappedPointer(hookup, r);
            result = result && hookupMappedPointer(hookup, b);
            return result;
        }
	};

	MxAXISingleSlavePort(sc_mx_module *_owner, const string& name); 
	virtual ~MxAXISingleSlavePort();

	// get AXI Slave version
	virtual const string getVersion(void) const { return string("2-0-001"); }

	 /* Synchronous access functions */
    virtual MxStatus read(MxU64 addr, MxU32* value, MxU32* ctrl);
    virtual MxStatus write(MxU64 addr, MxU32* value, MxU32* ctrl);
    virtual MxStatus readDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
    virtual MxStatus writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl);

    /* Asynchronous access functions */
    virtual MxStatus readReq(MxU64 addr, MxU32* value, MxU32* ctrl,
                             MxTransactionCallbackIF* callback);
    virtual MxStatus writeReq(MxU64 addr, MxU32* value, MxU32* ctrl,
                              MxTransactionCallbackIF* callback);

    /* Arbitration functions */
    virtual MxGrant requestAccess(MxU64 addr);
    virtual MxGrant checkForGrant(MxU64 addr);

    /* MxSI 3.0: new shared-memory based asynchronous transaction functions */
    virtual void driveTransaction(MxTransactionInfo* info);
    virtual void cancelTransaction(MxTransactionInfo* info);
    virtual MxStatus debugTransaction(MxTransactionInfo* info);

    /* Memory map functions */
    virtual int getNumRegions();
    virtual void getAddressRegions(MxU64* start, MxU64* size, string* name);

    /* MxSI 3.0: now the address regions can be set from the outside! */
    virtual void setAddressRegions(MxU64* start, MxU64* size, string* name);
    virtual MxMemoryMapConstraints* getMappingConstraints();

	void init(MxU32 dataBitwidth);
	void reset();
	void terminate();
	void communicate();
	void update();

protected:
    sc_mx_module* m_owner;

private:
	// 'pipeline'
	SlaveSingleChannelStages channelStages;

	struct NextHandshake
	{
		MxU8			 awready;
		MxU8			 arready;
		MxU8			 wready;
		AXITransaction * rvalid;
		AXITransaction * bvalid;
		inline void reset(void) { rvalid = bvalid = NULL; awready = arready = wready = 0; }
        NextHandshake() :awready(0), arready(0), wready(0), rvalid(NULL), bvalid(NULL) {}
        void persist(persister & p)
        {
            p.persist(awready);
            p.persist(arready);
            p.persist(wready);
            p.persistPointer(rvalid);
            p.persistPointer(bvalid);
        }
        bool hookupMappedPointers(std::map<AXITransaction*,AXITransaction*> & hookup)
        {
            bool result = hookupMappedPointer(hookup, rvalid);
            result = result && hookupMappedPointer(hookup, bvalid);
            return result;
        }
	};
	NextHandshake nextHandshake;

	struct IncomingTransactions
	{
		AXITransaction * aw;
		AXITransaction * ar;
		AXITransaction *  w;
        IncomingTransactions() { clear(); }
        inline void clear(void) { aw = ar = w = 0; }
        void persist(persister & p)
        {
            p.persistPointer(aw);
            p.persistPointer(ar);
            p.persistPointer(w);
        }
        bool hookupMappedPointers(std::map<AXITransaction*,AXITransaction*> & hookup)
        {
            bool result = hookupMappedPointer(hookup, aw);
            result = result && hookupMappedPointer(hookup, ar);
            result = result && hookupMappedPointer(hookup, w);
            return result;
        }
	};

    IncomingTransactions incomingTransactions;

	AXITransaction emptyMxTransactionInfo;	// for 'dummy' calls at empty channels

	bool isPortEnabled;

	ContainerAXITransactionNextCts<MAX_NUM_TRANSACTIONS> containerAXITransactionNextCts;

public:
	inline SlaveSingleChannelStages& getChannelStages() { return channelStages; }


private:
	// [C]
	inline void set_awready(MxU8 data)
	{
		assert(data < 2);
		if (data)
		{
			assert(channelStages.aw);
			AXITransaction * axiP = channelStages.aw;
			assert(axiP->status[AXI_STEP_ADDRESS] == MX_MASTER_READY);
			axiP->status[AXI_STEP_ADDRESS] = MX_SLAVE_READY;
		}
	}
	// [C]
	inline void set_arready(MxU8 data)
	{
		assert(data < 2);
		if (data)
		{
			assert(channelStages.ar);
			AXITransaction * axiP = channelStages.ar;
			assert(axiP->status[AXI_STEP_ADDRESS] == MX_MASTER_READY);
			axiP->status[AXI_STEP_ADDRESS] = MX_SLAVE_READY;
		}
	}
	// [C]
	inline void set_rvalid(const AXITransaction* const axiTransactionP) const
	{
		assert(axiTransactionP);
		axiTransactionP->status[axiTransactionP->cts] = MX_MASTER_READY;	// assume that cts probably has been incremented 
	}
	// [C]
	inline void set_wready(MxU8 data)
	{
		assert(data < 2);
		if (data)
		{
			assert(channelStages.w);
			MxTransactionInfo * axiP = channelStages.w;
			assert(axiP->status[axiP->cts] == MX_MASTER_READY);
			axiP->status[axiP->cts] = MX_SLAVE_READY;
		}
	}
	// [C]
	inline void set_bvalid(const AXITransaction* const axiTransactionP) const
	{
		assert(axiTransactionP);
		axiTransactionP->status[axiTransactionP->cts] = MX_MASTER_READY;	// assume that cts probably has been incremented 
	}

public:
	// [C]
	inline MxU32			getNext_awready() const { return nextHandshake.awready; }
	inline MxU32			getNext_arready() const { return nextHandshake.arready; }
	inline AXITransaction *	getNext_rvalid()  { return nextHandshake.rvalid;  }
	inline MxU32			getNext_wready() const  { return nextHandshake.wready;  }
	inline AXITransaction *	getNext_bvalid()  { return nextHandshake.bvalid;  }

	// ====== set the potential valid/ready flags (depending on pipeline they are really activated or not) ======
	// [U]
	inline void setNext_awready(MxU8 data) { assert(data < 2); nextHandshake.awready = data; }
	// [U]
	inline void setNext_arready(MxU8 data) { assert(data < 2); nextHandshake.arready = data; }
	// [U], [C] before this communicate is being executed
	inline void setNext_rvalid(AXITransaction* axiTransactionP)
	{
		assert(!nextHandshake.rvalid || (nextHandshake.rvalid->status[nextHandshake.rvalid->lastActiveCts()] >= MX_SLAVE_READY)); // previous transfer must be finished!
		nextHandshake.rvalid = axiTransactionP;
	}
	// [U]
	inline void setNext_wready(MxU8 data) { assert(data < 2); nextHandshake.wready = data; }
	// [U], [C] before this communicate is being executed
	inline void setNext_bvalid(AXITransaction* axiTransactionP)
	{
		assert(   !nextHandshake.bvalid																			// new in 'b' (no old one)
			   || ((axiTransactionP == nextHandshake.bvalid) && !nextHandshake.bvalid->isTransactionFinished())	// new == old AND not finished, yet
			   || nextHandshake.bvalid->isTransactionFinished());												// old finished
		nextHandshake.bvalid = axiTransactionP;
	}


	// [U]
	inline MxU32 get_awvalid() const { return (channelStages.aw && (channelStages.aw->status[AXI_STEP_ADDRESS] >= MX_MASTER_READY)); }
	inline MxU32 get_arvalid() const { return (channelStages.ar && (channelStages.ar->status[AXI_STEP_ADDRESS] >= MX_MASTER_READY)); }
///	inline MxU32 get_rready()  => use isTransferFinished_r()
	inline MxU32 get_wvalid() const  { return (channelStages.w && (channelStages.w->status[channelStages.w->lastActiveCts()] >= MX_MASTER_READY)); }
///	inline MxU32 get_bready()  => use isTransferFinished_b()


	// [U]
	inline bool	isTransferFinished_aw() const { return (channelStages.aw && (channelStages.aw->status[AXI_STEP_ADDRESS] >= MX_SLAVE_READY));}
	inline bool	isTransferFinished_ar() const { return (channelStages.ar && (channelStages.ar->status[AXI_STEP_ADDRESS] >= MX_SLAVE_READY));}
	inline bool	isTransferFinished_r() const
	{
		return (channelStages.r
				&& (channelStages.r->lastActiveCts() > 0)
				&& (channelStages.r->status[channelStages.r->lastActiveCts()] >= MX_SLAVE_READY));
	}
	inline bool	isTransferFinishedFirstTime_r()
	{
		return (channelStages.r
				&& (channelStages.r == getNext_rvalid())
				&& (channelStages.r->cts > 0)
				&& (channelStages.r->status[channelStages.r->cts] >= MX_SLAVE_READY));
	}
	inline bool	isTransferFinished_w() const
	{
		return (channelStages.w
				&& (channelStages.w->lastActiveCts() > 0)
				&& (channelStages.w->status[channelStages.w->lastActiveCts()] >= MX_SLAVE_READY));
	}
	inline bool	isTransferFinishedFirstTime_w() const
	{
		return (channelStages.w
				&& (channelStages.w == incomingTransactions.w)
				&& (channelStages.w->cts > 0)
				&& (channelStages.w->status[channelStages.w->cts] >= MX_SLAVE_READY));
	}
	inline bool	isTransferFinished_b() const  { return (channelStages.b && (channelStages.b->status[channelStages.b->nts-1] >= MX_SLAVE_READY)); }



	// Quick check [U]
	inline bool isActive() const { return (channelStages.numEntries != 0); }

    virtual bool persist(persister & p)
    {
        incomingTransactions.persist(p);
        nextHandshake.persist(p);
        channelStages.persist(p);
        containerAXITransactionNextCts.persist(p);
        return true;
    }

    /*! \brief Hook up old pointer addresses with their new restored equivalents.
     *
     *  \param hookup (map<AXITransaction *,AXITransaction *> &) map of old pointers to new pointers
     *
     *  \return (bool) true for success
     */
    virtual bool hookupPointers(std::map<AXITransaction*,AXITransaction*> & hookup)
    {
        bool result = incomingTransactions.hookupMappedPointers(hookup);
        result = result && nextHandshake.hookupMappedPointers(hookup);
        result = result && channelStages.hookupMappedPointers(hookup);
        result = result && containerAXITransactionNextCts.hookupPointers(hookup);
        return result;
    }
};













// ==========================================================================================
// Slave port (multi)
// ==========================================================================================
// AXI slave port class
class MxAXISlavePort : public MxTransactionSlave
{
public:
	enum Misc { MAX_NUM_TRANSACTIONS = 100 }; // currently, TODO..., something like parameter...

	MxAXISlavePort(sc_mx_module *_owner, const string& name);
	virtual ~MxAXISlavePort();

	// get AXI Slave version
	virtual const string getVersion(void) const { return string("2-0-001"); }

	 /* Synchronous access functions */
    virtual MxStatus read(MxU64 addr, MxU32* value, MxU32* ctrl);
    virtual MxStatus write(MxU64 addr, MxU32* value, MxU32* ctrl);
    virtual MxStatus readDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
    virtual MxStatus writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl);

    /* Asynchronous access functions */
    virtual MxStatus readReq(MxU64 addr, MxU32* value, MxU32* ctrl,
                             MxTransactionCallbackIF* callback);
    virtual MxStatus writeReq(MxU64 addr, MxU32* value, MxU32* ctrl,
                              MxTransactionCallbackIF* callback);

    /* Arbitration functions */
    virtual MxGrant requestAccess(MxU64 addr);
    virtual MxGrant checkForGrant(MxU64 addr);

    /* MxSI 3.0: new shared-memory based asynchronous transaction functions */
    virtual void driveTransaction(MxTransactionInfo* info);
    virtual void cancelTransaction(MxTransactionInfo* info);
    virtual MxStatus debugTransaction(MxTransactionInfo* info);

    /* Memory map functions */
    virtual int getNumRegions();
    virtual void getAddressRegions(MxU64* start, MxU64* size, string* name);

    /* MxSI 3.0: now the address regions can be set from the outside! */
    virtual void setAddressRegions(MxU64* start, MxU64* size, string* name);
    virtual MxMemoryMapConstraints* getMappingConstraints();

	void init(MxU32 dataBitwidth);
	void reset();
	void terminate();
	void communicate();
	void update();

protected:
	sc_mx_module* m_owner;
private:
	// 'pipeline' // handled completely by component

	struct IncomingTransactions
	{
		AXITransaction * aw;
		AXITransaction * ar;
		AXITransaction *  w;
        IncomingTransactions() { clear(); }
		inline void clear(void) { aw = ar = w = 0; }
        void persist(persister & p)
        {
            p.persistPointer(aw);
            p.persistPointer(ar);
            p.persistPointer(w);
        }
        bool hookupMappedPointers(std::map<AXITransaction*,AXITransaction*> & hookup)
        {
            bool result = hookupMappedPointer(hookup, aw);
            result = result && hookupMappedPointer(hookup, ar);
            result = result && hookupMappedPointer(hookup, w);
            return result;
        }
	};

	IncomingTransactions incomingTransactions;

	struct NextHandshake
	{
		MxU8			 awready;
		MxU8			 arready;
		MxU8			 wready;
		AXITransaction * rvalid;
		AXITransaction * bvalid;
        NextHandshake() :awready(0), arready(0), wready(0), rvalid(NULL) , bvalid(NULL) {}
        void persist(persister & p)
        {
            p.persist(awready);
            p.persist(arready);
            p.persist(wready);
            p.persistPointer(rvalid);
            p.persistPointer(bvalid);
        }
        bool hookupMappedPointers(std::map<AXITransaction*,AXITransaction*> & hookup)
        {
            bool result = hookupMappedPointer(hookup, rvalid);
            result = result && hookupMappedPointer(hookup, bvalid);
            return result;
        }
	};
	NextHandshake nextHandshake;

	AXITransaction emptyMxTransactionInfo;	// for 'dummy' calls at empty channels

	bool isPortEnabled;

	ContainerAXITransactionNextCts<MAX_NUM_TRANSACTIONS> containerAXITransactionNextCts;

public:
	inline AXITransaction* getIncoming_aw() { return incomingTransactions.aw; }
	inline AXITransaction* getIncoming_ar() { return incomingTransactions.ar; }
    inline AXITransaction* getIncoming_w()  { return incomingTransactions.w;  }

private:
	// [C]
	inline static void set_awready(const AXITransaction * const axiTransactionP)
	{
		assert(axiTransactionP);
		assert(axiTransactionP->status[AXI_STEP_ADDRESS] == MX_MASTER_READY);
		axiTransactionP->status[AXI_STEP_ADDRESS] = MX_SLAVE_READY;
	}
	// [C]
	inline static void set_arready(const AXITransaction * const axiTransactionP)
	{
		assert(axiTransactionP);
		assert(axiTransactionP->status[AXI_STEP_ADDRESS] == MX_MASTER_READY);
		axiTransactionP->status[AXI_STEP_ADDRESS] = MX_SLAVE_READY;
	}
	// [C]
	inline static void set_rvalid(const AXITransaction * const axiTransactionP)
	{
		assert(axiTransactionP);
		assert(axiTransactionP->cts > 0);
		assert(axiTransactionP->status[axiTransactionP->cts] < MX_SLAVE_READY);
		axiTransactionP->status[axiTransactionP->cts] = MX_MASTER_READY;	// assume that cts probably has been incremented 
	}
	// [C]
	inline static void set_wready(const AXITransaction * const axiTransactionP)
	{
		assert(axiTransactionP);
		assert((axiTransactionP->cts == 0) || (axiTransactionP->status[axiTransactionP->cts] == MX_MASTER_READY));
		axiTransactionP->status[axiTransactionP->cts] = MX_SLAVE_READY;
	}
	// [C]
	inline static void set_bvalid(const AXITransaction * const axiTransactionP)
	{
		assert(axiTransactionP);
		assert(axiTransactionP->cts == (axiTransactionP->nts - 1));
		assert(axiTransactionP->status[axiTransactionP->cts] < MX_SLAVE_READY);
		axiTransactionP->status[axiTransactionP->cts] = MX_MASTER_READY;	// assume that cts probably has been incremented 
	}


public:
	// ====== set the potential valid/ready flags (depending on pipeline they are really activated or not) ======
	// [U]
	inline void setNext_awready(MxU8 data) { assert(data < 2); nextHandshake.awready = data; }
	// [U]
	inline void setNext_arready(MxU8 data) { assert(data < 2); nextHandshake.arready = data; }
	// [U], [C] before this communicate is being executed
	inline void setNext_rvalid(AXITransaction* axiTransactionP) { nextHandshake.rvalid = axiTransactionP; }
	// [U]
	inline void setNext_wready(MxU8 data) { assert(data < 2); nextHandshake.wready = data; }
	// [U], [C] before this communicate is being executed
	inline void setNext_bvalid(AXITransaction* axiTransactionP) { nextHandshake.bvalid = axiTransactionP; }


	// [C]
	inline MxU32			getNext_awready() const { return nextHandshake.awready; }
	inline MxU32			getNext_arready() const { return nextHandshake.arready; }
	inline AXITransaction *	getNext_rvalid()  { return nextHandshake.rvalid;  }
	inline MxU32			getNext_wready() const  { return nextHandshake.wready;  }
	inline AXITransaction *	getNext_bvalid()  { return nextHandshake.bvalid;  }


	// Quick check [U]
///	inline bool isActive() { return (channelStages.heap.getNumAllocated() != 0); }

	// For debugging only:
	NextHandshake getNextHandshake() const { return nextHandshake; }

    virtual bool persist(persister & p)
    {
        incomingTransactions.persist(p);
        nextHandshake.persist(p);
        containerAXITransactionNextCts.persist(p);
        return true;
    }

    virtual bool hookupPointers(std::map<AXITransaction*,AXITransaction*> & hookup)
    {
        bool result = incomingTransactions.hookupMappedPointers(hookup);
        result = result && nextHandshake.hookupMappedPointers(hookup);
        result = result && containerAXITransactionNextCts.hookupPointers(hookup);
        return result;
    }
};



#endif // ! _MxAXIPort_h_

