// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2006 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.


#ifndef _ContainerAXITransactionNextCts_h_
#define _ContainerAXITransactionNextCts_h_

class persister;

template <int MAX_NUM_ELEMENTS>
class ContainerAXITransactionNextCts
{
public:
	ContainerAXITransactionNextCts(void);
	inline void reset(void);
	inline bool empty(void);
	inline void add( AXITransaction* pAXI);
	inline void remove( AXITransaction* pAXI);
	inline void adoptAllNextCts(void);
    inline void persist(persister & p);
    inline bool hookupPointers(const std::map<AXITransaction*,AXITransaction*> & hookup);

private:
	int m_numElements;      //!< Number of transactions in m_data (0..MAX_NUM_ELEMENTS)
	AXITransaction* m_data[MAX_NUM_ELEMENTS];
	
	bool m_isResetBeingCalled;  //!< for debugging only:
};

// ======================================================================
// Definition
// ======================================================================

template <int MAX_NUM_ELEMENTS>
ContainerAXITransactionNextCts<MAX_NUM_ELEMENTS>::ContainerAXITransactionNextCts(void) 
:m_numElements(0)
,m_isResetBeingCalled(false)
{   // All initialized
}

template <int MAX_NUM_ELEMENTS>
void
ContainerAXITransactionNextCts<MAX_NUM_ELEMENTS>::reset(void)
{
	m_numElements = 0;
	m_isResetBeingCalled = true;
}

template <int MAX_NUM_ELEMENTS>
bool
ContainerAXITransactionNextCts<MAX_NUM_ELEMENTS>::empty(void)
{
	return (m_numElements == 0);
}

template <int MAX_NUM_ELEMENTS>
void
ContainerAXITransactionNextCts<MAX_NUM_ELEMENTS>::add(AXITransaction* pAXI)
{
	assert(m_isResetBeingCalled);
	assert(m_numElements < MAX_NUM_ELEMENTS);
	assert(pAXI);
	assert(pAXI->cts < (pAXI->nts - 1));
	assert((pAXI->status[pAXI->cts] >= MX_SLAVE_READY) || ((pAXI->cts == AXI_STEP_ADDRESS) && (pAXI->access == MX_ACCESS_WRITE)));

	pAXI->prepareNextCts();
	m_data[m_numElements++] = pAXI;
}

template <int MAX_NUM_ELEMENTS>
void
ContainerAXITransactionNextCts<MAX_NUM_ELEMENTS>::remove(AXITransaction* pAXI)
{
	assert(m_isResetBeingCalled);

	for (int i = 0; i < m_numElements; i++ )
	{
		if (m_data[i] == pAXI)
		{
			assert(m_numElements > 0);
			m_numElements--;
			m_data[i] = m_data[m_numElements];
		}
	}
}

template <int MAX_NUM_ELEMENTS>
void
ContainerAXITransactionNextCts<MAX_NUM_ELEMENTS>::adoptAllNextCts(void)
{
	for (int i = 0; i < m_numElements; i++ )
	{
		m_data[i]->adoptNextCts();
	}
	m_numElements = 0;
}

/*! \brief Persist the transaction addresses.
 *
 *  Obviously the things pointed to will move over a save/restore cycle but
 *  we can use the old address to hook up the new address later. Just have
 *  to trust that we don't use the old address!
 *
 *  \param p (persister&) Wrapped stream for persisting
 */
template <int MAX_NUM_ELEMENTS>
void
ContainerAXITransactionNextCts<MAX_NUM_ELEMENTS>::persist(persister & p)
{
    p.persist(m_numElements);

    for (int i = 0; i < m_numElements; i++ )
    {
        p.persistPointer(m_data[i]);
    }
}

/*! \brief Hook up every pointer from old transaction to new transaction.
 *
 *  When restoring an AXITransaction the address will have changed, so use the
 *  old pointer to look up what the new pointer is.
 *
 *  \param hookup A map of old transaction pointers to new transaction pointers.
 *
 *  \return true for success, false if an old pointer is not in the map.
 */
template <int MAX_NUM_ELEMENTS>
bool 
ContainerAXITransactionNextCts<MAX_NUM_ELEMENTS>::hookupPointers(const std::map<AXITransaction*,AXITransaction*> & hookup)
{
    for (int i = 0; (i < m_numElements); ++i )
    {
        std::map<AXITransaction*,AXITransaction*>::const_iterator iter = hookup.find(m_data[i]);

        if (hookup.end() == iter)
        {
            return false;   // Pointer was not found = ERROR
        }

        m_data[i] = iter->second;
    }

    return true;
}

#endif // ! _ContainerAXITransactionNextCts_h_

