// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2006 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#ifndef AXI_TRANSACTION_H
#define AXI_TRANSACTION_H

/** file purpose:
 * In this file all necessary information shall be collected that is
 * necessary to share an identical parameterization of the AXI-Bus
 * protocol within the MaxSim System Transaction Interface 
 * (MxSI 3.0 and higher)
 * 
 * Used MxSI Methods are:
 * shared-memory based asynchronous access transaction functions
 * void driveTransaction(MxTransactionInfo* info);		// initiate transaction
 * void cancelTransaction(MxTransactionInfo* info);		// stop transaction
 * MxStatus debugTransaction(MxTransactionInfo* info); 	// debug access
 */

/** Signal to Transaction Translation
 * !Please note that negative logic is always converted to positive logic for modeling!
 *
 * AW	= Write Address Channel (info.access == MX_ACCESS_WRITE) && (info.cts == AXI_STEP_ADDRESS)
 * W    = Write Data Channel	(info.access == MX_ACCESS_WRITE) && (info.cts >= AXI_STEP_DATA0)
 * B    = Write Response Channel (info.access == MX_ACCESS_WRITE) && (info.cts == info.nts - 1)
 * AR   = Read Address Channel (info.access == MX_ACCESS_READ) && (info.cts == AXI_STEP_ADDRESS)
 * R    = Read Data Channel (info.access == MX_ACCESS_READ) && (info.cts >= AXI_STEP_DATA0
 * 
 * HW Signal		Meaning	Modeling
 *
 * Global Signal Group:
 *----------------------
 * ACLK				bus clock (all components with AXI transaction master or slave ports need to 
 * 					be clocked, however the clock is not part of the transaction itself)
 * ARESETn			reset (in case of a reset all masters, slaves and the bus clear pending 
 *					transactions, the reset signal is using a dedicated signal slave port)
 *
 * Write Address Channel Signal Group (driven by Master):
 *--------------------------------------------------------
 *								 					info.access = MX_ACCESS_WRITE;
 * AWID[x:0]		ID for the write address group:	info.masterFlags[AXI_MF_ID]
 * AWADDR[31:0]  	address is reflected by:		info.addr
 * AWLEN[3:0]		exact number of data transfers associated with this address channel:
 *													info.dataBeats [1-16]
 * AWSIZE[2:0]		size indicator of data transfer size in each beat:
 *													info.dataSize (in # of bytes [1,2,4,8,16,32,64,128])
 * AWBURST[1:0]		burst type selector : 			info.masterFlags[AXI_MF_BURST_TYPE]
 * AWLOCK[1:0]		atomic characteristics: 		info.masterFlags[AXI_MF_LOCK]
 * AWCACHE[3:0]		cache type information: 		info.masterFlags[AXI_MF_CACHE]
 * AWPROT[2:0]		protection type information: 	info.masterFlags[AXI_MF_PROTECTION]
 * AWVALID			master information valid  flag for address channel: 
 *													info.status[AXI_WA_STEP] >= MX_MASTER_READY
 * ***
 * AWREADY			slave ready indication for address channel: 
 *													info.status[AXI_WA_STEP] >= MX_SLAVE_READY
 * 
 *
 * Read Address Channel Signal Group (driven by Master):
 *-------------------------------------------------------
 * 													info.access = MX_ACCESS_READ;
 * ARID[3:0]		ID for the read address group:	info.masterFlags[AXI_MF_ID]
 * ARADDR[31:0]  	address is reflected by: 		info.addr
 * ARLEN[3:0]		exact number of data transfers associated with this address channel: 
 *													info.dataBeats [1-16]
 * ARSIZE[2:0]		size indicator of data transfer size in each beat: 
 *													info.dataSize (in # of bytes [1,2,4,8,16,32,64,128])
 * ARBURST[1:0]		burst type selector : 			info.masterFlags[AXI_MF_BURST_TYPE] 
 * ARLOCK[1:0]		atomic characteristics: 		info.masterFlags[AXI_MF_LOCK]
 * ARCACHE[3:0]		cache type information: 		info.masterFlags[AXI_MF_CACHE]
 * ARPROT[2:0]		protection type information: 	info.masterFlags[AXI_MF_PROTECTION]
 * ARVALID			master information valid  flag for address channel: 
 * 													info.status[AXI_RA_STEP] >= MX_MASTER_READY
 * ***
 * ARREADY			slave ready indication for address channel: 
 *													info.status[AXI_RA_STEP] >= MX_SLAVE_READY
 *
 *
 * Write Data Channel Signal Group: (driven by master):
 *------------------------------------------------------
 * 													info.access = MX_ACCESS_WRITE;
 * WID[3:0]			ID for the write data group:	info.masterFlags[AXI_MF_ID]
 * WDATA[31:0]		write data being sent to slave:	info.dataWr[n] 
 *													(n = current data beat = info.cts - AXI_WA_STEP)
 * WSTRB[3:0]	    write data byte-lane enable strobe: 
 *													info.masterFlags[AXI_MF_DATA_STROBE]
 * WLAST			last write data beat indicator: (implicit) 
 *													info.dataBeats == (info.cts - AXI_RA_STEP)
 * WVALID			master information valid flag for data channel:	 
 * 													info.status[AXI_WD_STEP[0-15]] >= MX_MASTER_READY
 * ***
 * WREADY			slave ready indication for data channel: 
 *													info.status[AXI_WD_STEP[0-15]] >= MX_SLAVE_READY
 *
 *
 * Read Data Channel Signal Group: (driven by slave):
 *----------------------------------------------------
 * 													info.access = MX_ACCESS_READ;
 * RID[3:0]			ID for the read data group:		info.masterFlags[AXI_MF_ID]
 * RDATA[31:0]		read data being sent to master:	info.dataRd[n] 
 *													(n = current data beat = info.cts - AXI_RA_STEP)
 * RRESP[1:0]	    read status response: 			info.masterFlags[AXI_MF_RESPONSE]
 * RLAST			last read data beat indicator: (implicit)
 *													info.dataBeats == (info.cts - AXI_RA_STEP)
 * RVALID			slave information valid flag for data channel:	 
 *													info.status[AXI_RD_STEP[0-15]] >= MX_MASTER_READY
 * ***
 * RREADY			master ready indication for data channel: 
 *													info.status[AXI_RD_STEP[0-15]] >= MX_SLAVE_READY
 *
 *
 * Write Response Channel Signal Group: (driven by slave):
 *---------------------------------------------------------
 *								 					info.access = MX_ACCESS_WRITE;
 * BID[3:0]			ID for the read data group:		info.masterFlags[AXI_MF_ID]
 * BRESP[1:0]	    read status response: 			info.masterFlags[AXI_MF_RESPONSE]
 * BVALID			slave information valid flag for data channel:	 
 *													info.status[AXI_BR_STEP] >= MX_MASTER_READY
 * ***
 * BREADY			master ready indication for data channel: 
 *													info.status[AXI_BR_STEP] >= MX_SLAVE_READY
 *
 * Low Power Signal Group: 
 *-------------------------
 * CSYSREQ			system low-power request => use dedicated signal slave port
 * CSYSACK			system low-power acknowledge => use dedicated signal master port
 * CACTIVE			clock request signal => use dedicated signal master port
 *
 */

enum AXITransactionStepEnum
{
	AXI_STEP_ADDRESS = 0,			// Read / Write address channel
	AXI_STEP_DATA0,					// Read / Write data channel beat 0
	AXI_STEP_DATA1,					// Read / Write data channel beat 1
	AXI_STEP_DATA2,					// Read / Write data channel beat 2
	AXI_STEP_DATA3,					// Read / Write data channel beat 3
	AXI_STEP_DATA4,					// Read / Write data channel beat 4
	AXI_STEP_DATA5,					// Read / Write data channel beat 5
	AXI_STEP_DATA6,					// Read / Write data channel beat 6
	AXI_STEP_DATA7,					// Read / Write data channel beat 7
	AXI_STEP_DATA8,					// Read / Write data channel beat 8
	AXI_STEP_DATA9,					// Read / Write data channel beat 9
	AXI_STEP_DATA10,				// Read / Write data channel beat 10
	AXI_STEP_DATA11,				// Read / Write data channel beat 11
	AXI_STEP_DATA12,				// Read / Write data channel beat 12
	AXI_STEP_DATA13,				// Read / Write data channel beat 13
	AXI_STEP_DATA14,				// Read / Write data channel beat 14
	AXI_STEP_DATA15,				// Read / Write data channel beat 15
	AXI_STEP_RESPONSE,				// Write response channel for 16 beat transfer only
	AXI_STEP_LAST
};

// usually the AXI_STEP_RESPONSE is reached if: cts == (nts - 1)


enum AXIMasterFlagBurstTypeEnum
{
	AXI_BURST_FIXED = 0,			// no address increment by Slave
	AXI_BURST_INCR,					// address increment by Slave
	AXI_BURST_WRAP,					// wrapped address increment by Slave
	AXI_BURST_RESERVED				// reserved
};

enum AXIMasterFlagResponseEnum
{
	AXI_RESP_OKAY = 0,
	AXI_RESP_EXOKAY,
	AXI_RESP_SLVERR,
	AXI_RESP_DECERR,
	AXI_RESP_RESERVED
};

enum AXIMasterFlagLockEnum
{
	AXI_LOCK_NORMAL = 0,
	AXI_LOCK_EXCLUSIVE,
	AXI_LOCK_LOCKED,
	AXI_LOCK_RESERVED
};

enum AXIMasterFlagCacheEnum
{
	AXI_CACHE_BUFFERABLE  = 0x1,
	AXI_CACHE_CACHEABLE   = 0x2,
	AXI_CACHE_READ_ALLOC  = 0x4,
	AXI_CACHE_WRITE_ALLOC = 0x8
};

enum AXIMasterFlagProtectionEnum
{
	AXI_PROTECTION_PRIV   = 0x1,
	AXI_PROTECTION_SEC    = 0x2,
	AXI_PROTECTION_INSN   = 0x4
};

// Number of bytes in transfer
enum AXIBurstSizeEnum
{
	AXI_BURST_SIZE_1   = 0,
	AXI_BURST_SIZE_2   = 1,
	AXI_BURST_SIZE_4   = 2,
	AXI_BURST_SIZE_8   = 3,
	AXI_BURST_SIZE_16  = 4,
	AXI_BURST_SIZE_32  = 5,
	AXI_BURST_SIZE_64  = 6,
	AXI_BURST_SIZE_128 = 7,
	AXI_BURST_SIZE_LAST
};


/*
 * In order to detect compliant component ports in a design ready for interconnection
 * this ID has been reserved for this AXYS Design Implementation of the AXI-Bus Transaction
 */
///#define AXI_PROTOCOL_ID 0x0000A3E1 // A 3E 1
#define AXI_PROTOCOL_ID 0x0001A3E1 // A 3E 1
#define AXI_PROTOCOL_VERSION_2 1

enum AXISlaveFlagEnum
{	// <TYPE>=AXI, <FLAG>=SF=SlaveFlag, <IDENTIFIER>=name
	AXI_SF_LAST = 0
};

enum AXIChannelEnum
{
	AXI_CHANNEL_AR     =  1,
	AXI_CHANNEL_R      =  2,
	AXI_CHANNEL_AW     =  4,
	AXI_CHANNEL_W      =  8,
	AXI_CHANNEL_B      = 16,
	//
	AXI_TRANSFER_EMPTY = 32,	// Indicates a 'dummy' call (driveTransaction()/notifyEvent()) (no 'valid' transfer
								// in the given channel) so ACI can collect them
	AXI_TRANSFER_LAST  = 64		// Indicates the last call per port (last driveTransaction() or last notifyEvent())
								// so the number of dummy calls can be reduced
};

enum AXIMasterFlagAWInfoEnum
{
	AXI_ACTIVE_CHANNEL_AW	= 1	// Channel AW is active (equivalent to awvalid = '1'); needed since not extractable from cts and status
};


enum AXITransactionMasterFlagEnum
{	// <TYPE>=AXI, <FLAG>=MF=MasterFlag, <IDENTIFIER>=name
	AXI_MF_ID = 0,		// transaction ID
	AXI_MF_BURST_TYPE,	// address auto increment mode
	AXI_MF_LOCK,		// lock signals
	AXI_MF_CACHE,		// cache flag signals
	AXI_MF_PROTECTION,	// protection flag signals
	AXI_MF_DATA_STROBE,	// data strobe signals
	AXI_MF_RESPONSE,	// response signal
	AXI_MF_CHANNEL,		// non-architectural: short cut to analyzing access, cts and respective status
	AXI_MF_AW_INFO,		// non-architectural: additional information regarding channel aw
	AXI_MF_AUSER,		// user flag for address channel (read/write)
	AXI_MF_DUSER,		// user flag for data channel (read/write)
	AXI_MF_BUSER,		// user flag for write response channel
	AXI_MF_LAST			// total number of Master Flags
};

/** Transaction Properties:
 *
 * Each port registers itself providing the following initialized data 
 * structure MxTransactionProperties props specific for the AXI Transaction
 * implementation by AXYS Design:
 * The macro takes two parameters:
 * _PROP is an instance of the MxTransactionProperties data type
 * _DATA_BITWIDTH_ specifies the maximum number of bits that can be 
 * transferred during a single data beat. 
 * 
 */


#if ((MAXSIM_MAJOR_VERSION < 5) || ((MAXSIM_MAJOR_VERSION == 5) && (MAXSIM_MINOR_VERSION < 1)))
/*
 * MaxSim < 5.1.x
 * --------------
 */
#define MXSI_VERSION MXSI_VERSION_3
#elif (MAXSIM_MAJOR_VERSION < 6)
/*
 * MaxSim < 6
 * ----------
 */
#define MXSI_VERSION MXSI_VERSION_3_1
#else
/*
 * MaxSim >= 6
 * -----------
 */
#define MXSI_VERSION MXSI_VERSION_6
#endif



#if (MAXSIM_MAJOR_VERSION < 6)
#	define PROPERTY_EXTENSION(PROP__) (PROP__).details = NULL;	/* not used but initialized */
#else
#	define PROPERTY_EXTENSION(PROP__) sprintf((PROP__).protocolName, "AXI"); \
		(PROP__).isAddrRegionForwarded = false; \
		(PROP__).forwardAddrRegionToMasterPort = NULL; \
		(PROP__).details                = NULL;			/* not used but initialized */
#endif

/* see also: AXI_INIT_TRANSACTION_PROPERTIES for a macro that does a complete initialization */
#define AXI_INIT_TRANSACTION_PROPERTIES_BASE(PROP__, DATA_BITWIDTH__)  \
        (PROP__).mxsiVersion            = MXSI_VERSION;         /* using the MXSI 3.1 */ \
        (PROP__).useMultiCycleInterface = true;                 /* use driveX */ \
        (PROP__).addressBitwidth        = 32;                   /* AXI Address Bus Bitwidth */ \
        (PROP__).mauBitwidth            = 8;                    /* AXI Bus is Byte Addressable */ \
        (PROP__).dataBitwidth           = (DATA_BITWIDTH__);    /* AXI Data Bus Bitwidth  */ \
        (PROP__).dataBeats              = 16;                   /* AXI Max Number of Data Beats */ \
        (PROP__).isLittleEndian         = true;                 /* AXI Byte Ordering */ \
        (PROP__).isOptional             = false;                /* AXI Component Specific */ \
        (PROP__).supportsAddressRegions = true;                 /* AXI Bus supports Address Regions */ \
        (PROP__).numCtrlFields          = 0;                    /* not used but initialized */ \
        (PROP__).numSlaveFlags          = AXI_SF_LAST;          /* AXI_Transaction_Slave_Flag_Enum */ \
        (PROP__).numMasterFlags         = AXI_MF_LAST;          /* AXI_Transaction_Master_Flag_Enum */ \
        (PROP__).numTransactionSteps    = AXI_STEP_LAST;        /* maximum num. of transaction steps */ \
        (PROP__).protocolID             = AXI_PROTOCOL_ID;      /* specific ID for this protocol */ \
        (PROP__).supportsNotify         = true;                 /* needed for fast response forwarding */ \
        (PROP__).supportsBurst          = true;                 /* burst transactions are supported */ \
        (PROP__).supportsSplit          = false;                /* split transactions are not defined */ \
        PROPERTY_EXTENSION(PROP__)

/* if you use AXI_INIT_TRANSACTION_PROPERTIES, then you should also call AXI_DESTRUCT_TRANSACTION_PROPERTIES when you are done with the structure */
#define AXI_INIT_TRANSACTION_PROPERTIES(PROP__, DATA_BITWIDTH__)  \
        AXI_INIT_TRANSACTION_PROPERTIES_BASE(PROP__, DATA_BITWIDTH__)  \
        (PROP__).validTimingTable       = new MxU32[4 + \
                                                    2 * (PROP__).dataBeats + \
                                                    (PROP__).numMasterFlags + \
                                                    (PROP__).numSlaveFlags]; \
	(PROP__).validTimingTable[0]  = AXI_STEP_ADDRESS; 	/* access */  \
	(PROP__).validTimingTable[1]  = AXI_STEP_ADDRESS; 	/* addr */  \
	(PROP__).validTimingTable[2]  = AXI_STEP_ADDRESS; 	/* dataSize */  \
	(PROP__).validTimingTable[3]  = AXI_STEP_ADDRESS; 	/* dataBeats */  \
	(PROP__).validTimingTable[4]  = AXI_STEP_DATA0; 	/* dataWr (valid also during beat 0)  */  \
	(PROP__).validTimingTable[5]  = AXI_STEP_DATA1; 	/* dataWr (valid also during beat 1)  */  \
	(PROP__).validTimingTable[6]  = AXI_STEP_DATA2; 	/* dataWr (valid also during beat 2)  */  \
	(PROP__).validTimingTable[7]  = AXI_STEP_DATA3; 	/* dataWr (valid also during beat 3)  */  \
	(PROP__).validTimingTable[8]  = AXI_STEP_DATA4; 	/* dataWr (valid also during beat 4)  */  \
	(PROP__).validTimingTable[9]  = AXI_STEP_DATA5; 	/* dataWr (valid also during beat 5)  */  \
	(PROP__).validTimingTable[10] = AXI_STEP_DATA6; 	/* dataWr (valid also during beat 6)  */  \
	(PROP__).validTimingTable[11] = AXI_STEP_DATA7; 	/* dataWr (valid also during beat 7)  */  \
	(PROP__).validTimingTable[12] = AXI_STEP_DATA8; 	/* dataWr (valid also during beat 8)  */  \
	(PROP__).validTimingTable[13] = AXI_STEP_DATA9; 	/* dataWr (valid also during beat 9)  */  \
	(PROP__).validTimingTable[14] = AXI_STEP_DATA10;	/* dataWr (valid also during beat 10) */  \
	(PROP__).validTimingTable[15] = AXI_STEP_DATA11;	/* dataWr (valid also during beat 11) */  \
	(PROP__).validTimingTable[16] = AXI_STEP_DATA12;	/* dataWr (valid also during beat 12) */  \
	(PROP__).validTimingTable[17] = AXI_STEP_DATA13;	/* dataWr (valid also during beat 13) */  \
	(PROP__).validTimingTable[18] = AXI_STEP_DATA14;	/* dataWr (valid also during beat 14) */  \
	(PROP__).validTimingTable[19] = AXI_STEP_DATA15;	/* dataWr (valid also during beat 15) */  \
	(PROP__).validTimingTable[20] = AXI_STEP_DATA0; 	/* dataRd (valid also during beat 0)  */  \
	(PROP__).validTimingTable[21] = AXI_STEP_DATA1; 	/* dataRd (valid also during beat 1)  */  \
	(PROP__).validTimingTable[22] = AXI_STEP_DATA2; 	/* dataRd (valid also during beat 2)  */  \
	(PROP__).validTimingTable[23] = AXI_STEP_DATA3; 	/* dataRd (valid also during beat 3)  */  \
	(PROP__).validTimingTable[24] = AXI_STEP_DATA4; 	/* dataRd (valid also during beat 4)  */  \
	(PROP__).validTimingTable[25] = AXI_STEP_DATA5; 	/* dataRd (valid also during beat 5)  */  \
	(PROP__).validTimingTable[26] = AXI_STEP_DATA6; 	/* dataRd (valid also during beat 6)  */  \
	(PROP__).validTimingTable[27] = AXI_STEP_DATA7; 	/* dataRd (valid also during beat 7)  */  \
	(PROP__).validTimingTable[28] = AXI_STEP_DATA8; 	/* dataRd (valid also during beat 8)  */  \
	(PROP__).validTimingTable[29] = AXI_STEP_DATA9; 	/* dataRd (valid also during beat 9)  */  \
	(PROP__).validTimingTable[30] = AXI_STEP_DATA10;	/* dataRd (valid also during beat 10) */  \
	(PROP__).validTimingTable[31] = AXI_STEP_DATA11;	/* dataRd (valid also during beat 11) */  \
	(PROP__).validTimingTable[32] = AXI_STEP_DATA12;	/* dataRd (valid also during beat 12) */  \
	(PROP__).validTimingTable[33] = AXI_STEP_DATA13; 	/* dataRd (valid also during beat 13) */  \
	(PROP__).validTimingTable[34] = AXI_STEP_DATA14;	/* dataRd (valid also during beat 14) */  \
	(PROP__).validTimingTable[35] = AXI_STEP_DATA15;	/* dataRd (valid also during beat 15) */  \
	(PROP__).validTimingTable[36] = AXI_STEP_ADDRESS; 	/* AXI_MF_ID */  \
	(PROP__).validTimingTable[37] = AXI_STEP_ADDRESS; 	/* AXI_MF_BURST_TYPE */  \
	(PROP__).validTimingTable[38] = AXI_STEP_ADDRESS; 	/* AXI_MF_LOCK */  \
	(PROP__).validTimingTable[39] = AXI_STEP_ADDRESS; 	/* AXI_MF_CACHE */ \
	(PROP__).validTimingTable[40] = AXI_STEP_ADDRESS; 	/* AXI_MF_PROTECTION */ \
	(PROP__).validTimingTable[41] = AXI_STEP_ADDRESS; 	/* AXI_MF_DATA_STROBE */ \
	(PROP__).validTimingTable[42] = AXI_STEP_DATA0; 	/* AXI_MF_RESPONSE */ \
	(PROP__).validTimingTable[43] = AXI_STEP_LAST;	 	/* AXI_MF_CHANNEL */ \
	(PROP__).validTimingTable[44] = AXI_STEP_ADDRESS;	/* AXI_MF_AW_INFO */ \
	(PROP__).validTimingTable[45] = AXI_STEP_ADDRESS; 	/* AXI_MF_AUSER */ \
	(PROP__).validTimingTable[46] = AXI_STEP_DATA0; 	/* AXI_MF_DUSER */ \
	(PROP__).validTimingTable[47] = AXI_STEP_DATA1; 	/* AXI_MF_BUSER */


#define AXI_DESTRUCT_TRANSACTION_PROPERTIES(PROP__)  \
	delete [](PROP__).validTimingTable;




// ********************************
// **** defines for Mx protocol ***
// ********************************

// transaction information are stored in ctrl fields

// data sizes equal to Mx data sizes
enum AXIMxSizeEnum
{
	AXI_MX_SIZE_1  = 1,		// byte
	AXI_MX_SIZE_2  = 2,		// hword
	AXI_MX_SIZE_4  = 3,		// word
	AXI_MX_SIZE_8  = 4,		// dword
	AXI_MX_SIZE_16 = 5
};

// indices for ctrl[]
enum AXIMxIdxEnum
{
	AXI_MX_IDX_SIZE = 0,	// mx data size of transfers: AXI_MX_SIZE_1, AXI_MX_SIZE_2, AXI_MX_SIZE_4, AXI_MX_SIZE_8
	AXI_MX_IDX_BEATS,		// number of beats
	AXI_MX_IDX_VA,			// virtual address
	AXI_MX_IDX_PORT,		// master port number to use

	AXI_MX_IDX_ID,			// transaction ID
	AXI_MX_IDX_BURST_TYPE,	// address auto increment mode
	AXI_MX_IDX_LOCK,		// lock signals
	AXI_MX_IDX_CACHE,		// cache flag signals
	AXI_MX_IDX_PROTECTION,	// protection flag signals
	AXI_MX_IDX_DATA_STROBE, // data strobe signals
	AXI_MX_IDX_RESPONSE,	// response signal
	AXI_MX_IDX_AUSER,		// generic user data address channel
	AXI_MX_IDX_DUSER,		// generic user data data channel
	AXI_MX_IDX_BUSER,		// generic user data response channel

	AXI_MX_IDX_LAST
};


#define AXI_MX_INIT_TRANSACTION_PROPERTIES(PROP__, _DATA_BITWIDTH_)  \
	(PROP__).mxsiVersion            = MXSI_VERSION;					\
	(PROP__).useMultiCycleInterface = false;						\
	(PROP__).addressBitwidth        = 32;							\
	(PROP__).mauBitwidth            = 8;							\
	(PROP__).dataBitwidth           = (_DATA_BITWIDTH_);			\
	(PROP__).dataBeats              = 16;							\
	(PROP__).isLittleEndian         = true;  						\
	(PROP__).isOptional             = false;						\
	(PROP__).supportsAddressRegions = true; 						\
	(PROP__).numCtrlFields          = AXI_MX_IDX_LAST;				\
	(PROP__).numSlaveFlags          = 0;							\
	(PROP__).numMasterFlags         = 0;							\
	(PROP__).numTransactionSteps    = 0;							\
	(PROP__).protocolID             = AXI_PROTOCOL_ID;				\
	(PROP__).supportsBurst          = false;						\
	(PROP__).supportsSplit          = false;						\
	(PROP__).supportsNotify         = false;						\
	(PROP__).details                = NULL;							\
	(PROP__).validTimingTable       = NULL;                         \
    PROPERTY_EXTENSION(PROP__)


#endif // #ifdef AXI_TRANSACTION_H
