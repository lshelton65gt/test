// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2006 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.


#ifndef _SIMPLEHEAP_H_
#define _SIMPLEHEAP_H_

//------------
// SimpleHeap
//------------

// suitable to be used as pipe stage heap

// Requires object being in held in SimpleHeap to have elements 'nextFreeP' and 'firstFreeP'
template <class ElementT, int NUM_ELEMENTS>
class SimpleHeap
{
protected:
	ElementT    element[NUM_ELEMENTS];
	MxU32       numAllocated;
	ElementT *  firstFreeP;

public:

	SimpleHeap()
	{
		reset();
	}

	void reset()
	{
        // Cannot just memset element[] as they are real objects.

        // Daisy chain the elements together
		element[0].nextFreeP = 0;

		for (int i=1; i<NUM_ELEMENTS; i++)
		{
			element[i].nextFreeP = &element[i-1];
		}

		firstFreeP = &element[NUM_ELEMENTS-1];

		numAllocated = 0;
	}

	void alloc(ElementT *& element)
	{
		// check for duplicate alloc
		assert(!element);
		// check for free element
		assert(firstFreeP);
		assert(numAllocated < NUM_ELEMENTS);
		//
		element = firstFreeP;
		firstFreeP = firstFreeP->nextFreeP;
		numAllocated++;
	}

	bool allocWithCheck(ElementT *& element)
	{
		if (numAllocated >= NUM_ELEMENTS)
		{
			return false;
		}
		alloc(element);
		return true;
	}

	void move(ElementT *& src, ElementT *& dst)
	{
		assert( src);
		assert(!dst);
		//
		dst = src;
		src = 0;
	}

	void link(ElementT *& src, ElementT *& dst)
	{
		assert( src);
		assert(!dst);
		//
		dst = src;
	}

	void unlink(ElementT *& element)
	{
		assert(element);
		//
		element = 0;
	}

	void free(ElementT *& element)
	{
		assert(element);
		assert(numAllocated > 0 && numAllocated <= NUM_ELEMENTS);
		//
		element->nextFreeP = firstFreeP;
		firstFreeP = element;
		element = 0;
		numAllocated--;
	}

	MxU32 getNumAllocated() const { return numAllocated; }

	ElementT &operator[](int idx)
	{
		assert(idx >= 0 && idx < NUM_ELEMENTS);
		return element[idx];
	}
    
    bool persistMappedObject(std::map<ElementT*,ElementT*> &mapOldToNew, persister & p)
    {
        bool result(true);

        p.persist(numAllocated);

        for (unsigned index = 0; result && index < NUM_ELEMENTS; ++index)
        {
            result = p.persistMappedObject(mapOldToNew, element[index]);
        }

        p.persistMappedPointer(mapOldToNew, firstFreeP);

        return result;
    }
};//class SimpleHeap

#endif //!_SIMPLEHEAP_H_
