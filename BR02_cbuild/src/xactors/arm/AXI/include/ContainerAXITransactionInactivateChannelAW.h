// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2006 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.


#ifndef _ContainerAXITransactionInactivateChannelAW_h_
#define _ContainerAXITransactionInactivateChannelAW_h_

#include "MxAXIPort.h"

class persister;

template <int MAX_NUM_ELEMENTS>
class ContainerAXITransactionInactivateChannelAW
{
public:
	ContainerAXITransactionInactivateChannelAW();
	inline void reset();
	inline bool empty();
	inline void add(AXITransaction* pAXI);
	inline void inactivateAllChannelAW();
    inline bool persist(persister & p);
    inline bool hookupPointers(const std::map<AXITransaction*,AXITransaction*> & hookup);

private:
	int m_numElements;
	AXITransaction* m_data[MAX_NUM_ELEMENTS];

	bool m_isResetBeingCalled;	//!< for debugging only:

};

// ======================================================================
// Definition
// ======================================================================

template <int MAX_NUM_ELEMENTS>
ContainerAXITransactionInactivateChannelAW<MAX_NUM_ELEMENTS>::ContainerAXITransactionInactivateChannelAW() 
:m_numElements(0)
,m_isResetBeingCalled(false)
{
}

template <int MAX_NUM_ELEMENTS>
void
ContainerAXITransactionInactivateChannelAW<MAX_NUM_ELEMENTS>::reset()
{
	m_numElements = 0;
	m_isResetBeingCalled = true;
}

template <int MAX_NUM_ELEMENTS>
bool
ContainerAXITransactionInactivateChannelAW<MAX_NUM_ELEMENTS>::empty()
{
	return (m_numElements == 0);
}

template <int MAX_NUM_ELEMENTS>
void
ContainerAXITransactionInactivateChannelAW<MAX_NUM_ELEMENTS>::add(AXITransaction* pAXI)
{
	assert(m_isResetBeingCalled);
	assert(m_numElements < MAX_NUM_ELEMENTS);
	assert(pAXI);
	assert(pAXI->access == MX_ACCESS_WRITE);
//	assert(pAXI->cts < (pAXI->nts - 1)); // probably cts already incremented
	assert(pAXI->status[AXI_STEP_ADDRESS] >= MX_SLAVE_READY);

	m_data[m_numElements++] = pAXI;
}

template <int MAX_NUM_ELEMENTS>
void
ContainerAXITransactionInactivateChannelAW<MAX_NUM_ELEMENTS>::inactivateAllChannelAW()
{
	for (int i = 0; i < m_numElements; i++ )
	{
		m_data[i]->inactivateChannelAW();
	}
	m_numElements = 0;
}

/*! \brief Persist the transaction addresses.
*
*  Obviously the things pointed to will move over a save/restore cycle but
*  we can use the old address to hook up the new address later. Just have
*  to trust that we don't use the old address!
*
*  \param p (persister&) Wrapped stream for persisting
*
*  \return true for success
*/
template <int MAX_NUM_ELEMENTS>
bool
ContainerAXITransactionInactivateChannelAW<MAX_NUM_ELEMENTS>::persist(persister & p)
{
    p.persist(m_numElements);

    for (int i = 0; i < m_numElements; i++ )
    {
        p.persistPointer(m_data[i]);
    }

    return true;
}

/*! \brief Hook up every pointer from old transaction to new transaction.
*
*  When restoring an AXITransaction the address will have changed, so use the
*  old pointer to look up what the new pointer is.
*
*  \param hookup A map of old transaction pointers to new transaction pointers.
*
*  \return true for success, false if an old pointer is not in the map.
*/
template <int MAX_NUM_ELEMENTS>
bool 
ContainerAXITransactionInactivateChannelAW<MAX_NUM_ELEMENTS>::hookupPointers(const std::map<AXITransaction*,AXITransaction*> & hookup)
{
    for (int i = 0; (i < m_numElements); ++i )
    {
        std::map<AXITransaction*,AXITransaction*>::const_iterator iter = hookup.find(m_data[i]);

        if (hookup.end() == iter)
        {
            return false;   // Pointer was not found = ERROR
        }

        m_data[i] = iter->second;
    }

    return true;
}

#endif // ! _ContainerAXITransactionInactivateChannelAW_h_

