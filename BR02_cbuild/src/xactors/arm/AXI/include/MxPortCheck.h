// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2006 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.

#ifndef _MxPortCheck_h_
#define _MxPortCheck_h_

#include "maxsim.h"

class MxPortCheck
{
public:
	inline static bool check(MxTransactionMasterPort* pMasterPort);	// true means OK
	inline static bool check(sc_mx_transaction_slave* pSlavePort);	// true means OK
};


bool MxPortCheck::check(MxTransactionMasterPort* pMasterPort)
{
	bool ok = true;

	if (NULL != pMasterPort)
	{
		MxU32 nrSlaves = pMasterPort->getSlaves().size();
		MxU32 i;
			
		for (i = 0; i < nrSlaves; i++)
		{
			// Check my master port connected to another slave port
			sc_mx_transaction_if *pConnectedSlavePort = pMasterPort->getSlaves()[i];

			if (pConnectedSlavePort != NULL)
			{
				sc_mx_m_base* pConnectedSlaveOwner = pConnectedSlavePort->getMxOwner();

				if (pConnectedSlaveOwner != NULL) // owner != NULL => is connected
				{
					// Check for the same AXI protocol ID of master and slave
					MxU32 master_protocolID = pMasterPort->getProperties()->protocolID;
					MxU32 slave_protocolID  = pConnectedSlavePort->getProperties()->protocolID;

					if (master_protocolID != slave_protocolID)
					{	
						ok = false;
						pMasterPort->pmessage(MX_MSG_WARNING, "Connection %s::%s <--> %s::%s: Protocol IDs of master and slave ports are not equivalent (master: 0x%08X, slave: 0x%08X)!",
								pMasterPort->getMxOwner()->getInstanceName().c_str(),
								pMasterPort->getPortInstanceName().c_str(),
								pConnectedSlaveOwner->getInstanceName().c_str(),
								pConnectedSlavePort->getPortInstanceName().c_str(),
								master_protocolID,
								slave_protocolID);
					}

					// Check for the same data bitwidth of master and slave
					MxU32 master_dataBitwidth = pMasterPort->getProperties()->dataBitwidth;
					MxU32 slave_dataBitwidth  = pConnectedSlavePort->getProperties()->dataBitwidth;

					if (master_dataBitwidth != slave_dataBitwidth)
					{
						ok = false;
						pMasterPort->pmessage(MX_MSG_WARNING, "Connection %s::%s <--> %s::%s: Data Bitwidth of master and slave ports are not equivalent (master: %d, slave: %d)!",
								pMasterPort->getMxOwner()->getInstanceName().c_str(),
								pMasterPort->getPortInstanceName().c_str(),
								pConnectedSlaveOwner->getInstanceName().c_str(),
								pConnectedSlavePort->getPortInstanceName().c_str(),
								master_dataBitwidth,
								slave_dataBitwidth);
					}
				}
			}
		}
	}
	return ok;
}

bool MxPortCheck::check(sc_mx_transaction_slave* pSlavePort)
{
	bool ok = true;

	// Check my slave port connected to another master port
	if ((NULL != pSlavePort) && pSlavePort->getMaster())	// connected...
	{
		sc_mx_transaction_p_base*	pConnectedMasterPort	= pSlavePort->getMaster();
		sc_mx_m_base*				pSlaveOwner				= pSlavePort->getMxOwner();

		// Check for the same AXI protocol ID of master and slave
		MxU32 master_protocolID = pConnectedMasterPort->getProperties()->protocolID;
		MxU32 slave_protocolID  = pSlavePort->getProperties()->protocolID;

		if (master_protocolID != slave_protocolID)
		{
			ok = false;
			pSlavePort->pmessage(MX_MSG_WARNING, "Connection %s::%s <--> %s::%s: Protocol IDs of master and slave ports are not equivalent (master: 0x%08X, slave: 0x%08X)!",
								 pConnectedMasterPort->getMxOwner()->getInstanceName().c_str(),
								 pConnectedMasterPort->getPortInstanceName().c_str(),
								 pSlaveOwner->getInstanceName().c_str(),
								 pSlavePort->getPortInstanceName().c_str(),
								 master_protocolID,
								 slave_protocolID);
		}

		// Check for the same data bitwidth of master and slave
		MxU32 master_dataBitwidth = pConnectedMasterPort->getProperties()->dataBitwidth;
		MxU32 slave_dataBitwidth  = pSlavePort->getProperties()->dataBitwidth;

		if (master_dataBitwidth != slave_dataBitwidth)
		{
			ok = false;
			pSlavePort->pmessage(MX_MSG_WARNING, "Connection %s::%s <--> %s::%s: Data Bitwidth of master and slave ports are not equivalent (master: %d, slave: %d)!",
								 pConnectedMasterPort->getMxOwner()->getInstanceName().c_str(),
								 pConnectedMasterPort->getPortInstanceName().c_str(),
								 pSlaveOwner->getInstanceName().c_str(),
								 pSlavePort->getPortInstanceName().c_str(),
								 master_dataBitwidth,
								 slave_dataBitwidth);
		}
	}

	return ok;
}




#endif // ! _MxPortCheck_h_
