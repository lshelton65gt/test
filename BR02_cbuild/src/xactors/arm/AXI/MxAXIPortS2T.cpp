

#include "MxAXISignals.h"
#include "carbon_axi.h"

// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2000-2004 AXYS GmbH, Herzogenrath, Germany
//                and AXYS Design Automation, Inc., Irvine, CA, USA
//            (C) COPYRIGHT 2004-2007 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
//
//	MxAXIPortS2T
//
// ----------------------------------------------------------------------------------------------------------------------

MxAXIPortS2T::MxAXIPortS2T (sc_mx_module* owner, const string& name) 
			: MxTransactionMasterPort(owner, name)
{
	setMxOwner(owner);

	mAxiDeleteR = NULL;
	mAxiDeleteB = NULL;
}

MxAXIPortS2T::~MxAXIPortS2T()
{
}


void MxAXIPortS2T::set_aw (MxU32 id, MxU64 addr, MxU32 len, MxU32 size, MxU32 burst, MxU32 lock, MxU32 cache, MxU32 prot, MxU32 user, MxU32 valid)
{	
	if (valid && mActiveAW == NULL)
	{
		AXITransaction* axi;

		// Probably the incoming new 'aw' belongs to something currently in 'w'
		axi = getTransaction(mActiveWrite, id, true);
		if (axi && (axi->status[AXI_STEP_ADDRESS] == MX_MASTER_WAIT))
		{
			// yes, it belongs to a transaction in 'w'
			axi->masterFlags[AXI_MF_CHANNEL] |= AXI_CHANNEL_AW;
		}
		else
		{
			// no, it belongs to nothing in 'w', it's a complete new transaction
			axi = NULL;
			if (!mHeap.allocWithCheck(axi))
			{
				set_awready(0);	// block this transaction
				pmessage(MX_MSG_WARNING, "Limit of active transactions reached. Blocking further transactions by setting awready = '0'.");
			}
			axi->setInitiator(this);
			axi->init(mDataBitWidth);
		
			axi->clear();
			axi->nts = AXI_STEP_LAST;
			axi->reset();
			axi->access = MX_ACCESS_WRITE;
			axi->masterFlags[AXI_MF_CHANNEL] = AXI_CHANNEL_AW;

			mActiveWrite.push_back(axi);
		}
		axi->status[AXI_STEP_ADDRESS] = MX_MASTER_READY; //valid
		axi->activateChannelAW();

		axi->set_id(id);
		axi->set_addr(addr);
		axi->set_len(len);
		axi->set_size(size);
		axi->set_burst(burst);
		axi->set_lock(lock);
		axi->set_cache(cache);
		axi->set_prot(prot);
		axi->set_auser(user);

		mActiveAW = axi;
	}
	if (mActiveAW != NULL)
	{
//		MxU32 oldChannelFlags = mActiveAW->masterFlags[AXI_MF_CHANNEL];
		mActiveAW->masterFlags[AXI_MF_CHANNEL] = AXI_CHANNEL_AW;
		driveTransaction(mActiveAW);
//		mActiveAW->masterFlags[AXI_MF_CHANNEL] = oldChannelFlags;

		if (mActiveAW->isTransferFinished_aw())
		{
			containerAXITransactionInactivateChannelAW.add(mActiveAW);
//			mActiveAW->masterFlags[AXI_MF_CHANNEL]&= ~AXI_CHANNEL_AW;
			if (mActiveAW->cts == 0)
			{
//r				mActiveAW->foreignFlags()|= FLAGS_INCR_CTS;
				containerAXITransactionNextCts.add(mActiveAW);
			}
			// else w channel has control over cts incrementing
 
 			// Pop off mActiveWrite list when address & data complete           
			if (mActiveAW->get_wlast() && mActiveAW->isTransferFinished_w())
			{
				remTransaction(mActiveWrite, mActiveAW->get_id());
            }
            

			mActiveAW = NULL;			
			set_awready(1);			
		}
		else
		{
			set_awready(0);			
		}
	}
	else
	{
		mEmptyTransaction.masterFlags[AXI_MF_CHANNEL] = (AXI_CHANNEL_AW | AXI_TRANSFER_EMPTY);
		driveTransaction(&mEmptyTransaction);
	}
}

void MxAXIPortS2T::set_ar (MxU32 id, MxU64 addr, MxU32 len, MxU32 size, MxU32 burst, MxU32 lock, MxU32 cache, MxU32 prot, MxU32 user, MxU32 valid)
{
	if (valid && mActiveAR == NULL)
	{
		AXITransaction* axi = NULL;
		if (!mHeap.allocWithCheck(axi))
		{
			set_arready(0);	// block this transaction
			pmessage(MX_MSG_WARNING, "Limit of active transactions reached. Blocking further transactions by setting arready = '0'.");
		}
		axi->setInitiator(this);
		axi->init(mDataBitWidth);
		
		axi->clear();
		axi->nts = AXI_STEP_LAST;
		axi->reset();
		axi->access = MX_ACCESS_READ;
		axi->masterFlags[AXI_MF_CHANNEL] = AXI_CHANNEL_AR;
		axi->status[AXI_STEP_ADDRESS] = MX_MASTER_READY;		

		axi->set_id(id);
		axi->set_addr(addr);
		axi->set_len(len);
		axi->set_size(size);
		axi->set_burst(burst);
		axi->set_lock(lock);
		axi->set_cache(cache);
		axi->set_prot(prot);
		axi->set_auser(user);

		mActiveAR = axi;
		mActiveRead.push_back(axi);
	}
	if (mActiveAR != NULL)
	{	
		driveTransaction(mActiveAR);
		if (mActiveAR->isTransferFinished_ar())
		{
			mActiveAR = NULL;
			set_arready(1);			
		}
		else
		{
			set_arready(0);			
		}
	}
	else
	{
		mEmptyTransaction.masterFlags[AXI_MF_CHANNEL] = (AXI_CHANNEL_AR | AXI_TRANSFER_EMPTY);
		driveTransaction(&mEmptyTransaction);
	}
}

void MxAXIPortS2T::set_w (MxU32 id, MxU32* data, MxU32 strb, MxU32 last, MxU32 user, MxU32 valid)
{
	if (valid && mActiveW == NULL)
	{
		AXITransaction* axi;

		// Probably the incoming new 'w' belongs to something currently in 'aw' or 'w'
		axi = getTransaction(mActiveWrite, id, false);
		if (axi)
		{
			if (axi->cts == AXI_STEP_ADDRESS)
			{
				// yes, it belongs to a transaction in 'aw'
//r				axi->foreignFlags() |= FLAGS_INCR_CTS;
				containerAXITransactionNextCts.remove(axi); // we will the cts to 1 immediately
				axi->cts = 1;
			}
			// it probably also belongs to 'w'
		}
		else
		{
			// no, it belongs to nothing in 'aw' or 'w', it's a complete new transaction
			axi = NULL;
			if (!mHeap.allocWithCheck(axi))
			{
				set_wready(0);	// block this transaction
				pmessage(MX_MSG_WARNING, "Limit of active transactions reached. Blocking further transactions by setting wready = '0'.");
			}

			axi->setInitiator(this);
			axi->init(mDataBitWidth);
		
			axi->clear();
			
            // Length of burst unknown; set to max to prevent early WLAST
			axi->set_len(15);            
			axi->nts = AXI_STEP_LAST;
			axi->reset();
			axi->access = MX_ACCESS_WRITE;
			axi->masterFlags[AXI_MF_CHANNEL] = AXI_CHANNEL_W;
			axi->masterFlags[AXI_MF_ID]		 = id;

			axi->cts = 1;	// we are in data phase already

			mActiveWrite.push_back(axi);
		}
		mActiveW = axi;
	}

	if (mActiveW)
	{
//r		if (mActiveW->foreignFlags() & FLAGS_INCR_CTS)
//r		{
//r			mActiveW->cts++;
//r			mActiveW->masterFlags[AXI_MF_CHANNEL] |= AXI_CHANNEL_W;
//r			mActiveW->foreignFlags()&= ~FLAGS_INCR_CTS;
//r		}
		mActiveW->status[mActiveW->cts] = MX_MASTER_READY;	// getCurrentBeat considers status == MX_MASTER_WAIT, so we set status first here
		
		MxU32*	srcData = data;
		MxU32*	dstData = mActiveW->getP_setWriteData(mActiveW->getCurrentBeat());
		MxU32	srcStrb = strb;
		MxU32	dstStrb;

		dstStrb = srcStrb;
		mActiveW->set_strb(dstStrb);
        
        // If last data transfer & address not yet received, set len to ensure 
        // WLAST recognised by slave
        if (last && (mActiveW->status[AXI_STEP_ADDRESS] != MX_SLAVE_READY)) 
        {
        	mActiveW->set_len(mActiveW->getCurrentBeat());
        }
        
        
		mActiveW->set_duser(user);

		//AXISignals::convertDataHw2Mx(mActiveW, srcData, dstData, srcStrb, dstStrb);
		AXISignals::copyData(mActiveW, srcData, dstData);
		
//		MxU32 oldChannelFlags = mActiveW->masterFlags[AXI_MF_CHANNEL];
		mActiveW->masterFlags[AXI_MF_CHANNEL] = AXI_CHANNEL_W;

		driveTransaction(mActiveW);

		if (!mWasNotifiedW)
		{
			// we must do this here, since if mWasNotifiedW then mActiveW will be NULL !!!
			// mActiveW->masterFlags[AXI_MF_CHANNEL] = oldChannelFlags;

			if (mActiveW->isTransferFinished_w())
			{
				if (!mActiveW->get_wlast())
				{
//r					mActiveW->foreignFlags()|= FLAGS_INCR_CTS;
					containerAXITransactionNextCts.add(mActiveW);
				}
				else
				{
 					// Pop off mActiveWrite list when address & data complete 
                    if (mActiveW->isTransferFinished_aw())
                    {          
						remTransaction(mActiveWrite, mActiveW->get_id());
                    }
				}
				mActiveW = NULL;			
				set_wready(1);
			}
			else
			{
				set_wready(0);
			}
		}
	}
	else
	{
		mEmptyTransaction.masterFlags[AXI_MF_CHANNEL] = (AXI_CHANNEL_W | AXI_TRANSFER_EMPTY);
		driveTransaction(&mEmptyTransaction);
	}
}

void MxAXIPortS2T::set_bready (MxU32 value)
{
	if (value && mActiveB != NULL)
	{
		mActiveB->status[mActiveB->cts] = MX_SLAVE_READY;
		
		if (mAxiDeleteB)
		{
			mHeap.free(mAxiDeleteB);
		}
		mAxiDeleteB = mActiveB;

		mActiveB = NULL;
		mClearValidB = true;
	}
}

void MxAXIPortS2T::set_rready (MxU32 value)
{
	if (value && mActiveR != NULL)
	{
		mActiveR->status[mActiveR->cts] = MX_SLAVE_READY;
		if (mActiveR->get_rlast())
		{	
			remTransaction(mActiveRead, mActiveR->get_id());
			
			if (mAxiDeleteR)
			{
				mHeap.free(mAxiDeleteR);
			}
			mAxiDeleteR = mActiveR;			
		}
		mActiveR = NULL;
		mClearValidR = true;
	}
}


void MxAXIPortS2T::remTransaction (std::vector<AXITransaction*>& buffer, MxU32 id)
{
	std::vector<AXITransaction*>::iterator pos;

	for (pos = buffer.begin(); pos != buffer.end(); pos++) 
	{
		if ((*pos)->get_id() == id)
		{
			buffer.erase(pos);
			return;
		}
	}
}

AXITransaction* MxAXIPortS2T::getTransaction (std::vector<AXITransaction*>& buffer, MxU32 id, bool forAW) const
{
	MxU32 i;
	for (i = 0; i < buffer.size(); i++)
	{
        // Look for Transaction to put AW in
        if (forAW &&  (buffer[i]->get_id() == id) && !buffer[i]->isTransferFinished_aw())
        {
			return buffer[i];
        }
        
        // Look for Transaction to put W in
        if (!forAW &&  (buffer[i]->get_id() == id) && !buffer[i]->get_wlast())
		{
			return buffer[i];
		}
	}
	return NULL;
}


void MxAXIPortS2T::NotifyHandler::notifyEvent (MxTransactionInfo* info)
{
	if (!mOwner->mNextCtsAndInactivateChannelAwAlreadyDone)
	{
		mOwner->containerAXITransactionNextCts.adoptAllNextCts();
		mOwner->containerAXITransactionInactivateChannelAW.inactivateAllChannelAW();
		mOwner->mNextCtsAndInactivateChannelAwAlreadyDone = true;
	}

	AXITransaction* axi = static_cast<AXITransaction*>(info);
	MxU32 mf_channel = axi->masterFlags[AXI_MF_CHANNEL];

	assert(axi->isValidTransaction());

	switch (mf_channel & ~AXI_TRANSFER_LAST & ~AXI_TRANSFER_EMPTY)
	{
		case AXI_CHANNEL_W:
			assert(axi->isTransferFinished_w());

			mOwner->mWasNotifiedW = true;	
			if (!axi->get_wlast())
			{
//r				axi->foreignFlags()|= FLAGS_INCR_CTS;
				mOwner->containerAXITransactionNextCts.add(axi);
			}
			else
			{
				mOwner->remTransaction(mOwner->mActiveWrite, axi->get_id());
			}
			mOwner->mActiveW = NULL;			
			mOwner->set_wready(1);
			break;

		case AXI_CHANNEL_R:
			mOwner->mWasNotifiedR = true;
			mOwner->mActiveR = axi;
			mOwner->set_rvalid(axi);
			break;

		case AXI_CHANNEL_B:
			mOwner->mWasNotifiedB = true;
			mOwner->mActiveB = axi;
			mOwner->set_bvalid(axi);
			break;

		default:
			break;
	}	
}

void MxAXIPortS2T::init	(MxU32 dataBitWidth)
{
	mDataBitWidth = dataBitWidth;
	mDataNumMxU32 = (mDataBitWidth+31)/32;

	MxTransactionProperties prop;
	AXI_INIT_TRANSACTION_PROPERTIES(prop, mDataBitWidth);
	setProperties(&prop);

	mEmptyTransaction.initialize(getProperties());
	mEmptyTransaction.reset();
	mEmptyTransaction.nts = 1;
	mEmptyTransaction.cts = 2;

	for (int i = 0; i < MAX_NUM_AXI_TRANSACTIONS_ON_HEAP; i++)
	{
		mHeap[i].initialize(getProperties());
	}

	mNotifyHandler.setOwner(this);
	setNotifyHandler(&mNotifyHandler);
}

void MxAXIPortS2T::reset ()
{	
	mWasNotifiedAW		= false;
	mWasNotifiedAR		= false;
	mWasNotifiedW		= false;
	mWasNotifiedR		= false;
	mWasNotifiedB		= false;

	mClearValidR		= false;
	mClearValidB		= false;
	mClearValidDelayedR	= false;
	mClearValidDelayedB = false;

	mActiveAW			= NULL;
	mActiveAR			= NULL;
	mActiveW			= NULL;
	mActiveB			= NULL;
	mActiveR			= NULL;
	
	mHeap.reset();

	mActiveWrite.clear();
	mActiveRead.clear();

	mAxiDeleteB = NULL;
	mAxiDeleteR = NULL;

	containerAXITransactionNextCts.reset();
	containerAXITransactionInactivateChannelAW.reset();
	mNextCtsAndInactivateChannelAwAlreadyDone = false;
}


void MxAXIPortS2T::terminate ()
{
}


void MxAXIPortS2T::communicate ()
{
	if (!mNextCtsAndInactivateChannelAwAlreadyDone)
	{
		containerAXITransactionNextCts.adoptAllNextCts();
		containerAXITransactionInactivateChannelAW.inactivateAllChannelAW();
		mNextCtsAndInactivateChannelAwAlreadyDone = true;
	}

	if (mClearValidDelayedR && !mWasNotifiedR)
	{
		set_rvalid(NULL);
		mClearValidDelayedR = false;
	}
	if (mClearValidDelayedB && !mWasNotifiedB)
	{
		set_bvalid(NULL);
		mClearValidDelayedB = false;
	}
}


void MxAXIPortS2T::update ()
{
	mWasNotifiedAW = false;
	mWasNotifiedAR = false;
	mWasNotifiedW  = false;
	mWasNotifiedR  = false;
	mWasNotifiedB  = false;

	mClearValidDelayedR = mClearValidR;
	mClearValidR = false;

	mClearValidDelayedB = mClearValidB;
	mClearValidB = false;

	mNextCtsAndInactivateChannelAwAlreadyDone = false;
}


// ----------------------------------------------------------------------------------------------------------------------
//
//	MxAXIPortS2T_Conv
//
// ----------------------------------------------------------------------------------------------------------------------

MxAXIPortS2T_Conv::MxAXIPortS2T_Conv (sc_mx_module* owner, const string& name) : MxAXIPortS2T(owner, name)
{
	MxU32 i;

	for (i = 0; i < AXI_NUM_SIGNALS; i++)
	{
		mSPorts[i] = NULL;
	}	
}

MxAXIPortS2T_Conv::~MxAXIPortS2T_Conv ()
{
}

void MxAXIPortS2T_Conv::setSignalMasterPort	(MxU32 id, CarbonSignalMasterPort* port)
{
	mSPorts[id] = port;
}

void MxAXIPortS2T_Conv::setSlaveSignal (MxU32 id, MxU32 value, MxU32* extValues)
{
	assert(AXISignals::isMaster(id));

	switch (id)
	{
	case AXI_WDATA:
		mDataBufferWrite[0] = value;
		for (MxU32 i = 0; i < getDataNumMxU32()-1; i++)
		{
			assert(extValues != NULL);
			mDataBufferWrite[i+1] = extValues[i];
		}
		break;
	case AXI_ARADDR:
		mAddrBufferAR[0] = value;
		for (MxU32 i = 0; i < getAddrNumMxU32()-1; ++i)
		{
			assert(extValues != NULL);
			mAddrBufferAR[i+1] = extValues[i];
		}
		mValues[id] = value; // still needed for 32-bit mode
		break;
	case AXI_AWADDR:
		mAddrBufferAW[0] = value;
		for (MxU32 i = 0; i < getAddrNumMxU32()-1; ++i)
		{
			assert(extValues != NULL);
			mAddrBufferAW[i+1] = extValues[i];
		}
		mValues[id] = value; // still needed for 32-bit mode
		break;
	default:
		mValues[id] = value;
		break;
	}
}

void MxAXIPortS2T_Conv::init(MxU32 dataBitWidth, MxU32 addrBitWidth)
{
    MxAXIPortS2T::init(dataBitWidth);

	mAddrBitWidth = addrBitWidth;
	mAddrNumMxU32 = (mAddrBitWidth+31)/32;
}

void MxAXIPortS2T_Conv::reset ()
{
	MxAXIPortS2T::reset();
	
	MxU32 i;
	
	for (i = 0; i < AXI_NUM_SIGNALS; i++)
	{
		mSet		[i]	= false;
		mValues		[i]	= 0;
		mValuesExt	[i] = NULL;
	}
	for (i = 0; i < AXI_NUM_MXU32_DATA; i++)
	{
		mDataBufferWrite[i] = 0;
		mDataBufferRead [i] = 0;
	}
	for (i = 0; i < AXI_NUM_MXU32_ADDR; ++i)
	{
		mAddrBufferAR[i] = 0;
		mAddrBufferAW[i] = 0;
	}

    // Initialise RTL out signals
	set_rvalid(NULL);
	set_bvalid(NULL);
	set_awready(1);
	set_wready(1);
	set_arready(1);

}

void MxAXIPortS2T_Conv::set_rvalid (AXITransaction* axi)
{
	MxU32*	ext;

	if (axi == NULL)
	{	
		ext = getDataNumMxU32() > 1 ? ext = mDataBufferRead : NULL;

		mSPorts[AXI_RID   ]->driveSignal(0,	NULL);
		mSPorts[AXI_RDATA ]->driveSignal(0,	ext);
		mSPorts[AXI_RRESP ]->driveSignal(0,	NULL);
		mSPorts[AXI_RLAST ]->driveSignal(0,	NULL);
		mSPorts[AXI_RUSER ]->driveSignal(0,	NULL);
		mSPorts[AXI_RVALID]->driveSignal(0,	NULL);
	}
	else
	{
		MxU32*	srcData = axi->getP_getReadData(axi->getCurrentBeat());
		MxU32*	dstData = mDataBufferRead;
		
		//AXISignals::convertDataMx2Hw(axi, srcData, dstData, (MxU32)-1, dummy);
		AXISignals::copyData(axi, srcData, dstData);

		ext = getDataNumMxU32() > 1 ? ext = &(dstData[1]) : NULL;

		mSPorts[AXI_RID   ]->driveSignal(axi->get_id(),		NULL);
		mSPorts[AXI_RDATA ]->driveSignal(dstData[0],		ext);
		mSPorts[AXI_RRESP ]->driveSignal(axi->get_resp(),	NULL);
		mSPorts[AXI_RLAST ]->driveSignal(axi->get_rlast(),	NULL);
		mSPorts[AXI_RUSER ]->driveSignal(axi->get_duser(),	NULL);
		mSPorts[AXI_RVALID]->driveSignal(1,					NULL);
	}
	set_rready(mValues[AXI_RREADY]);
}

void MxAXIPortS2T_Conv::set_bvalid (AXITransaction* axi)
{
	if (axi == NULL)
	{
		mSPorts[AXI_BID   ]->driveSignal(0,	NULL);
		mSPorts[AXI_BRESP ]->driveSignal(0,	NULL);
		mSPorts[AXI_BUSER ]->driveSignal(0,	NULL);
		mSPorts[AXI_BVALID]->driveSignal(0,	NULL);
	}
	else
	{
		mSPorts[AXI_BID   ]->driveSignal(axi->get_id	(),	NULL);
		mSPorts[AXI_BRESP ]->driveSignal(axi->get_resp	(),	NULL);
		mSPorts[AXI_BUSER ]->driveSignal(axi->get_buser	(),	NULL);
		mSPorts[AXI_BVALID]->driveSignal(1,					NULL);
	}
	set_bready(mValues[AXI_BREADY]);
}

void MxAXIPortS2T_Conv::set_awready	(MxU32 ready)
{
	mSPorts[AXI_AWREADY]->driveSignal(ready, NULL);
}

void MxAXIPortS2T_Conv::set_wready (MxU32 ready)
{
	mSPorts[AXI_WREADY ]->driveSignal(ready, NULL);
}

void MxAXIPortS2T_Conv::set_arready	(MxU32 ready)
{
	mSPorts[AXI_ARREADY]->driveSignal(ready, NULL);
}

void MxAXIPortS2T_Conv::communicate	()
{
	MxAXIPortS2T::communicate();

	MxU64 extAWADDR = 0, extARADDR = 0;
	for ( int i = 0; i < AXI_NUM_MXU32_ADDR; i++)
        {
            ((MxU32*)&extAWADDR)[i] = mAddrBufferAW[i];
            ((MxU32*)&extARADDR)[i] = mAddrBufferAR[i];
        }

	set_aw(mValues[AXI_AWID],    extAWADDR,            mValues[AXI_AWLEN],
		   mValues[AXI_AWSIZE],  mValues[AXI_AWBURST], mValues[AXI_AWLOCK], 
		   mValues[AXI_AWCACHE], mValues[AXI_AWPROT],  mValues[AXI_AWUSER], mValues[AXI_AWVALID]);

	set_ar(mValues[AXI_ARID],    extARADDR,            mValues[AXI_ARLEN],
		   mValues[AXI_ARSIZE],  mValues[AXI_ARBURST], mValues[AXI_ARLOCK], 
		   mValues[AXI_ARCACHE], mValues[AXI_ARPROT],  mValues[AXI_ARUSER], mValues[AXI_ARVALID]);

	set_w (mValues[AXI_WID], mDataBufferWrite, mValues[AXI_WSTRB], mValues[AXI_WLAST], mValues[AXI_WUSER], mValues[AXI_WVALID]);
}

void MxAXIPortS2T_Conv::update ()
{
	MxAXIPortS2T::update();
}


// ----------------------------------------------------------------------------------------------------------------------
//
//	MxAXIPortS2T_Trace
//
// ----------------------------------------------------------------------------------------------------------------------

MxAXIPortS2T_Trace::MxAXIPortS2T_Trace (sc_mx_module* owner, const string& name) : MxAXIPortS2T(owner, name)
{
	MxU32 i;

	for (i = 0; i < AXI_NUM_SIGNALS; i++)
	{
		mPins	[i]	= NULL;
		mPinsRef[i]	= NULL;
	}
}

MxAXIPortS2T_Trace::~MxAXIPortS2T_Trace ()
{
}

void MxAXIPortS2T_Trace::setPin	(MxU32 id, MxU32* pin)
{
	mPins[id] = pin;
}

void MxAXIPortS2T_Trace::setPinRef (MxU32 id, MxU32* pin)
{
	mPinsRef[id] = pin;
}

void MxAXIPortS2T_Trace::init (MxU32 dataBitWidth, bool mergeData)
{
	MxAXIPortS2T::init(dataBitWidth);
	mMergeData = mergeData;
}

void MxAXIPortS2T_Trace::communicate ()
{
	MxAXIPortS2T::communicate();

	set_aw(*(mPins[AXI_AWID]),		*(mPins[AXI_AWADDR]),  *(mPins[AXI_AWLEN]), 
		   *(mPins[AXI_AWSIZE]),	*(mPins[AXI_AWBURST]), *(mPins[AXI_AWLOCK]), 
		   *(mPins[AXI_AWCACHE]),	*(mPins[AXI_AWPROT]),  *(mPins[AXI_AWUSER]), *(mPins[AXI_AWVALID]));
	set_ar(*(mPins[AXI_ARID]),		*(mPins[AXI_ARADDR]),  *(mPins[AXI_ARLEN]), 
		   *(mPins[AXI_ARSIZE]),	*(mPins[AXI_ARBURST]), *(mPins[AXI_ARLOCK]), 
		   *(mPins[AXI_ARCACHE]),	*(mPins[AXI_ARPROT]),  *(mPins[AXI_ARUSER]), *(mPins[AXI_ARVALID]));
	set_w (*(mPins[AXI_WID]),		 (mPins[AXI_WDATA]),   *(mPins[AXI_WSTRB]), 
		   *(mPins[AXI_WLAST]),		*(mPins[AXI_WUSER]),   *(mPins[AXI_WVALID]));
}

void MxAXIPortS2T_Trace::update ()
{
	MxAXIPortS2T::update();
}

void MxAXIPortS2T_Trace::set_rvalid	(AXITransaction* axi)
{
	if (axi == NULL)
	{
		*(mPins[AXI_RID   ]) = 0;
		*(mPins[AXI_RRESP ]) = 0;
		*(mPins[AXI_RLAST ]) = 0;
		*(mPins[AXI_RUSER ]) = 0;
		*(mPins[AXI_RVALID]) = 0;
	}
	else
	{
		MxU32*	srcData = axi->getP_getReadData(axi->getCurrentBeat());
		MxU32*	dstData = mPins[AXI_RDATA];
		
		//memcpy(dstData, mPinsRef[AXI_RDATA], sizeof(MxU32)*axi->getDataMxU32width());
		//AXISignals::convertDataMx2Hw(axi, srcData, dstData, (MxU32)-1, dummy);
		if (mMergeData)
		{
			AXISignals::mergeData(axi, srcData, mPinsRef[AXI_RDATA], dstData);
		}
		else
		{
			AXISignals::copyData(axi, srcData, dstData);
		}

		*(mPins[AXI_RID   ]) = axi->get_id		();
		*(mPins[AXI_RRESP ]) = axi->get_resp	();
		*(mPins[AXI_RLAST ]) = axi->get_rlast	();
		*(mPins[AXI_RUSER ]) = axi->get_duser	();
		*(mPins[AXI_RVALID]) = 1;
	}
	set_rready(*(mPins[AXI_RREADY]));
}

void MxAXIPortS2T_Trace::set_bvalid	(AXITransaction* axi)
{
	if (axi == NULL)
	{
		*(mPins[AXI_BID   ]) = 0;
		*(mPins[AXI_BRESP ]) = 0;
		*(mPins[AXI_BUSER ]) = 0;
		*(mPins[AXI_BVALID]) = 0;
	}
	else
	{
		*(mPins[AXI_BID   ]) = axi->get_id		();
		*(mPins[AXI_BRESP ]) = axi->get_resp	();
		*(mPins[AXI_BUSER ]) = axi->get_buser	();
		*(mPins[AXI_BVALID]) = 1;
	}
	set_bready(*(mPins[AXI_BREADY]));
}

void MxAXIPortS2T_Trace::set_awready (MxU32 ready)
{
	*(mPins[AXI_AWREADY]) = ready;
}

void MxAXIPortS2T_Trace::set_wready	(MxU32 ready)
{
	*(mPins[AXI_WREADY]) = ready;
}

void MxAXIPortS2T_Trace::set_arready (MxU32 ready)
{
	*(mPins[AXI_ARREADY]) = ready;
}
