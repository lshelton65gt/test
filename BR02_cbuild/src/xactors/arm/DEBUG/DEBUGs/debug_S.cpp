/***************************************************************************************
  Copyright (c) 2006-2009 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "debug_S.h"
#include "DEBUGs.h"

#define DEBUG_DATA_WIDTH 32
#define DEBUG_PROTOCOL_ID 0x10
#define DEBUG_PROTOCOL_NAME "CarbonDebug"



#ifdef CARBON
void debug_S::setMxOwner(DEBUGs *owner)
{
  sc_mx_transaction_slave::setMxOwner(dynamic_cast<maxsim::sc_mx_m_base*>(owner->mCarbonComponent));
}
#endif

debug_S::debug_S( DEBUGs *_owner ) : sc_mx_transaction_slave( "debug_S" )
{
	owner = _owner;
	setMxOwner(owner);
	// most of these are don't care - only the protocol ID is checked
	prop.mxsiVersion = MXSI_VERSION_6;
	prop.useMultiCycleInterface = false;
	prop.addressBitwidth=64;
	prop.mauBitwidth = 8;
	prop.dataBitwidth = 32;
	prop.dataBeats = 1;
	prop.isLittleEndian = true;
	prop.isOptional = false;
	prop.supportsAddressRegions = true;
	prop.numCtrlFields = 2;
	prop.numSlaveFlags = 0;
	prop.numMasterFlags = 0; 
	prop.numTransactionSteps = 2; 
	prop.validTimingTable = NULL;
	prop.protocolID = DEBUG_PROTOCOL_ID;
	sprintf(prop.protocolName, DEBUG_PROTOCOL_NAME);
	prop.supportsNotify = false;
	prop.supportsBurst = false;
	prop.supportsSplit = false;
	prop.isAddrRegionForwarded = false;
	prop.forwardAddrRegionToMasterPort = NULL;
	setProperties(&prop); 
	
	firstError = true;    	     
#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
    initMemoryMapConstraints();
#endif
}


#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
// *****************************************************************
// debug_S::initMemoryMapconstraints() -- initialize constraints
// *****************************************************************
void
debug_S::initMemoryMapConstraints()
{
    puMemoryMapConstraints.minRegionSize = 0x1;
    puMemoryMapConstraints.maxRegionSize = MxU64CONST(0x100000000);
    puMemoryMapConstraints.minAddress = 0x0;
    puMemoryMapConstraints.maxAddress = MxU64CONST(0xffffffff);
    puMemoryMapConstraints.minNumSupportedRegions = 0x1;        
    puMemoryMapConstraints.maxNumSupportedRegions = MAX_REGIONS;
    puMemoryMapConstraints.alignmentBlockSize = 0x400 ; //the min block size where this slave's regions can be mapped 
    puMemoryMapConstraints.numFixedRegions = 0;
    puMemoryMapConstraints.fixedRegionList = NULL;

}
#endif

/* If no regions are defined, numRegions returns 0, indicating that the 
 * port wants to be called for any MxU64 address */
int
debug_S::getNumRegions()
{
  return 1;
}

void 
debug_S::getAddressRegions(MxU64* start, MxU64* size, string* name)
{
  start[0] = owner->p_Base;
  size[0]  = owner->p_Size;
  name[0]  = "DEBUGs";
}


#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
void 
debug_S::setAddressRegions(MxU64* start, MxU64* size, string* name)
{

  if (start && size && name)
    {
      if (owner->p_enableDbgMsg)
        {
            string port_name = this->getPortInstanceName();
            owner->mCarbonComponent->message(MX_MSG_INFO, "Set Address Region Called for Port %s", port_name.c_str());
        }
      for (unsigned int i = 0; i < MAX_REGIONS; i++)
	{
	  if (size[i] > 0)
	    {
	      owner->p_Base = start[i]; // be careful when changing MAX_REGIONS
	      owner->p_Size = size[i];
	      regionName[i] = name[i];
	      if (owner->p_enableDbgMsg)
		owner->mCarbonComponent->message(MX_MSG_INFO, "Address Region: start= 0x" MxFMT64x " size = 0x" MxFMT64x " Name= %s", 
			       start[i], size[i], 
			       name[i].c_str());
	    }
	  else
	    {
	      if (owner->p_enableDbgMsg)
                    owner->mCarbonComponent->message(MX_MSG_INFO, "Address Region (ignored due to size): start= 0x" MxFMT64x " size = 0x" MxFMT64x " Name= %s", 
                                   start[i], size[i], 
                                   name[i].c_str());
	      break;
	    }
	}
    }
  else
    {
#if (!CARBON)
      owner->mCarbonComponent->message(MX_MSG_ERROR, "%s: Uninitialized data structures passed in setAddressRegions call",
		     owner->getInstanceID().c_str());
#else
      owner->mCarbonComponent->message(MX_MSG_ERROR, "DEBUG_SLAVE: Uninitialized data structures passed in setAddressRegions call");
#endif
    }
}

// ************************************************************
// debug_S::getMappingConstraints() -- get slave constraints
// ************************************************************

MxMemoryMapConstraints* 
debug_S::getMappingConstraints() 
{
  //owner->mCarbonComponent->message(MX_MSG_INFO, "In getMappingConstraints function.\n");
  return &puMemoryMapConstraints;
}

#endif

MxStatus
debug_S::read(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  MxStatus status = MX_STATUS_NOTSUPPORTED;
  UNUSEDARG(addr);
  UNUSEDARG(value);
  UNUSEDARG(ctrl);
  return status;
}  
  


MxStatus 
debug_S::write(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  MxStatus status = MX_STATUS_NOTSUPPORTED;
  UNUSEDARG(addr);
  UNUSEDARG(value);
  UNUSEDARG(ctrl);
  return status;
}  
  


MxStatus
debug_S::readDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{   
#ifdef CARBON
  return CarbonDebugFunctionsToDebugAccess(owner->mDbaCb, eCarbonDebugAccessRead, prop.dataBitwidth, addr, value, ctrl);
#else
  return owner->memdbg->readDbg( addr, value, ctrl );
#endif
}


MxStatus
debug_S::writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
#ifdef CARBON
  return CarbonDebugFunctionsToDebugAccess(owner->mDbaCb, eCarbonDebugAccessWrite, prop.dataBitwidth, addr, value, ctrl);
#else
  return owner->memdbg->writeDbg( addr, value, ctrl );
#endif
}


MxStatus
debug_S::readReq(MxU64 addr, MxU32* value, MxU32* ctrl, MxTransactionCallbackIF* callback)
{
  MxStatus ret = MX_STATUS_NOTSUPPORTED;
  UNUSEDARG(addr);
  UNUSEDARG(value);
  UNUSEDARG(callback);
  UNUSEDARG(ctrl);
    
  return ret;
}

MxStatus
debug_S::writeReq(MxU64 addr, MxU32* value, MxU32* ctrl, MxTransactionCallbackIF* callback)
{
  UNUSEDARG(addr);
  UNUSEDARG(value);
  UNUSEDARG(ctrl);
  UNUSEDARG(callback);
    
  return MX_STATUS_NOTSUPPORTED;
}
