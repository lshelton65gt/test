/***************************************************************************************
  Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/carbon_capi.h"
#include "DEBUGs.h"
#include "debug_S.h"
#include "DEBUGs_MxDI.h"

#define DEBUG_DATA_WIDTH 32
#define DEBUG_PROTOCOL_ID 0x10
#define DEBUG_PROTOCOL_NAME "CarbonDebug"

#ifdef CARBON
#include <stdarg.h>
#define DEBUGs_RTL_SM(NAME) portFactory(mCarbonComponent, xtorName, NAME, mpCarbonObject)
#else
#define DEBUGs_RTL_SM(NAME) new sc_port<sc_mx_signal_if>(this, NAME)
#endif

#ifdef CARBON
#define PORT_OWNER mCarbonComponent
DEBUGs::DEBUGs(sc_mx_module* carbonComp, const char *xtorName, CarbonObjectID **carbonObj, CarbonPortFactory *portFactory,
               const CarbonDebugAccessCallbacks& dbaCb)
  : CarbonAdaptorBase(carbonComp, xtorName, carbonObj), mDbaCb(dbaCb)
#else
#define PORT_OWNER this
DEBUGs::DEBUGs(sc_mx_m_base* c, const string &s) : sc_mx_module(c, s)
#endif
{
  debug_Slave = new debug_S( this );
  registerPort(debug_Slave, "debug" );

  maxsim::MxSignalProperties *signalprop = new maxsim::MxSignalProperties;
  signalprop->isOptional = false;

#ifndef CARBON
    memdbg = new sc_port<sc_mx_transaction_if>(PORT_OWNER, "memdbg");
    registerPort( memdbg, "memdbg" );
    MxTransactionProperties tprop;
    tprop.mxsiVersion = MXSI_VERSION_6;
    tprop.useMultiCycleInterface = false;
    tprop.addressBitwidth=64;
    tprop.mauBitwidth = 8;
    tprop.dataBitwidth = 32;
    tprop.dataBeats = 1;
    tprop.isLittleEndian = true;
    tprop.isOptional = false;
    tprop.supportsAddressRegions = true;
    tprop.numCtrlFields = 2;
    tprop.numSlaveFlags = 0;
    tprop.numMasterFlags = 0; 
    tprop.numTransactionSteps = 2; 
    tprop.validTimingTable = NULL;
    tprop.protocolID = DEBUG_PROTOCOL_ID;
    sprintf(tprop.protocolName, DEBUG_PROTOCOL_NAME);
    tprop.supportsNotify = false;
    tprop.supportsBurst = false;
    tprop.supportsSplit = false;
    tprop.isAddrRegionForwarded = false;
    tprop.forwardAddrRegionToMasterPort = NULL;
    memdbg->setProperties( &tprop );
#endif

#ifndef CARBON
    SC_MX_CLOCKED();
    registerPort( dynamic_cast< sc_mx_clock_slave_p_base* >( this ), "clk-in");
#endif

    p_initComplete = false;
    p_mxdi = NULL;

#ifndef CARBON
    defineParameter( "Enable Debug Messages", "false", MX_PARAM_BOOL, true);
    defineParameter( "Base Address", "0x0", MX_PARAM_VALUE, false);
    defineParameter( "Size", "0x100000000", MX_PARAM_VALUE, false);
    defineParameter( "Big Endian", "false", MX_PARAM_BOOL, false );
#endif
}

DEBUGs::~DEBUGs()
{
  delete debug_Slave;
}

void 
DEBUGs::init()
{
  p_mxdi = new DEBUGs_MxDI(this);
  
  if (p_mxdi == NULL)
    {
      mCarbonComponent->message(MX_MSG_FATAL_ERROR,"init: Failed to allocate memory for MxDI interface. Aborting Simulation!");
    }

#ifndef CARBON
  // Call the base class after this has been initialized.
  sc_mx_module::init();
#endif
  p_initComplete = true;
}

void
DEBUGs::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  (void)level; (void)filelist;

  // Check that protocol ID & data width match
  if (!MxPortCheck::check((debug_S*)debug_Slave))
    {
      mCarbonComponent->message(MX_MSG_WARNING, "Incompatible ports connected.");
    }
      
#ifndef CARBON
    // Call the base class after this has been reset.
    sc_mx_module::reset(level, filelist);
#endif
}



void 
DEBUGs::terminate()
{
#ifndef CARBON
  // Call the base class first.
  sc_mx_module::terminate();
#endif

  if (p_mxdi != NULL)
  {
    // Release the MXDI Interface
    delete p_mxdi;
    p_mxdi = NULL;
  }
}

void DEBUGs::driveModel() {
}

void
DEBUGs::setParameter( const string &name, const string &value )
{
  MxConvertErrorCodes status = MxConvert_SUCCESS;
  
  if (name == "Enable Debug Messages")
    {
      status = MxConvertStringToValue(value, &p_enableDbgMsg);
    }
  else if (name == "Base Address")
    {
      if (p_initComplete == false)
        {
          status = MxConvertStringToValue(value, &p_Base);
        }
      else
        {
          message(MX_MSG_WARNING, "DEBUGs::setParameter: cannot change parameter <%s> at runtime. Assignment ignored.", 
                  name.c_str() );            
        }
    }
  else if (name == "Size")
    {
      if (p_initComplete == false)
        {
          status = MxConvertStringToValue(value, &p_Size);
        }
      else
        {
          message(MX_MSG_WARNING, "DEBUGs::setParameter: cannot change parameter <%s> at runtime. Assignment ignored.", 
                  name.c_str() );            
        }
    }
  else if (name == "Big Endian")
    {
      status = MxConvertStringToValue(value, &bigEndian);
    }
  
  
  if ( status == MxConvert_SUCCESS )
    {
#ifndef CARBON
      sc_mx_module::setParameter(name, value);
#endif
    }
  else
    {
      message( MX_MSG_WARNING, "DEBUGs::setParameter: illegal value <%s> "
	       "passed for parameter <%s>. Assignment ignored.", value.c_str(), name.c_str() );
    }
}

bool DEBUGs::saveData(eslapi::CASIODataStream& os)
{
  return true;
}

bool DEBUGs::restoreData(eslapi::CASIIDataStream& is)
{
  return true;
}

#ifndef CARBON
string
DEBUGs::getProperty( MxPropertyType property )
{
  string description; 
  switch ( property ) 
  {    
    case MX_PROP_LOADFILE_EXTENSION:
      return "";
    case MX_PROP_COMPONENT_TYPE:
      return "Other"; 
    case MX_PROP_COMPONENT_VERSION:
      return "2.0.002";
    case MX_PROP_MSG_PREPEND_NAME:
      return "yes"; 
    case MX_PROP_DESCRIPTION:
      description = "Debug Transaction Slave Interface";
      return description + " Compiled on " + __DATE__ + ", " + __TIME__; 
    case MX_PROP_MXDI_SUPPORT:
    	return "yes";
    case MX_PROP_SAVE_RESTORE:
    	return "no";     
    default:
      return "";
  }
  return "";
}

MXDI*
DEBUGs::getMxDI()
{
  return p_mxdi;
}

string
DEBUGs::getName(void)
{
  return "DEBUGs";
}

/************************
 * DEBUGs Factory class
 ***********************/
class DEBUGsFactory : public MxFactory
{
public:
  DEBUGsFactory() : MxFactory ( "DEBUGs" ) {}
  sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
  { 
    return new DEBUGs(c, id); 
  }
};

/**
 *  Entry point into the memory components (from MaxSim)
 */
extern "C" void 
MxInit(void)
{
  new DEBUGsFactory();
}

#endif
