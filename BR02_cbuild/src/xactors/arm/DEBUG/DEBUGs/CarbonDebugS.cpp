
#include "CarbonDebugS.h"

#define CARBON 1
#include "DEBUGs.h"

CarbonDebugS::CarbonDebugS(sc_mx_module* carbonComp,
                           const char *xtorName,
                           CarbonObjectID **carbonObj,
                           CarbonPortFactory *portFactory, 
                           CarbonDebugReadWriteFunction *readDebug,
                           CarbonDebugReadWriteFunction *writeDebug)
{
  CarbonDebugAccessCallbacks cb = {carbonComp, NULL, writeDebug, readDebug, NULL};
  mArmAdaptor = new DEBUGs(carbonComp, xtorName, carbonObj, portFactory, cb);
}

CarbonDebugS::CarbonDebugS(sc_mx_module* carbonComp,
                           const char *xtorName,
                           CarbonObjectID **carbonObj,
                           CarbonPortFactory *portFactory, 
                           CarbonDebugAccessFunction *debugAccess)
{
  CarbonDebugAccessCallbacks cb = {carbonComp, debugAccess, NULL, NULL, NULL};
  mArmAdaptor = new DEBUGs(carbonComp, xtorName, carbonObj, portFactory, cb);
}

sc_mx_signal_slave *CarbonDebugS::carbonGetPort(const char *name)
{
  sc_mx_signal_slave *port;
  port = NULL;
  mArmAdaptor->message(MX_MSG_FATAL_ERROR, "Carbon Debug Slave has no port named \"%s\"", name);
  return port;
}

CarbonDebugS::~CarbonDebugS()
{
  delete mArmAdaptor;
}

void CarbonDebugS::communicate()
{
}

void CarbonDebugS::update()
{
}

void CarbonDebugS::init()
{
  mArmAdaptor->init();
}

void CarbonDebugS::terminate()
{
  mArmAdaptor->terminate();
}

void CarbonDebugS::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  mArmAdaptor->reset(level, filelist);
}

void CarbonDebugS::setParameter(const string &name, const string &value)
{
  mArmAdaptor->setParameter(name, value);
}

//! Number of slave memory regions
int CarbonDebugS::getNumRegions()
{
  return mArmAdaptor->getNumSlaveRegions();
}

//! Slave Address regions
void CarbonDebugS::getAddressRegions(uint64_t* start, uint64_t* size, string* name)
{
  mArmAdaptor->getSlaveAddressRegions(start, size, name);
}

bool CarbonDebugS::saveData(eslapi::CASIODataStream& os)
{
  return mArmAdaptor->saveData(os);
}

bool CarbonDebugS::restoreData(eslapi::CASIIDataStream& is)
{
  return mArmAdaptor->restoreData(is);
}
