// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef DEBUGs__H
#define DEBUGs__H

#include "maxsim.h"
#include "MxPortCheck.h"

#include "carbon_arm_adaptor.h"

#ifdef CARBON
#include "CarbonAdaptorBase.h"
#include "util/UtCLicense.h"
#endif

class DEBUGs_MxDI;

#ifdef CARBON
class DEBUGs : public CarbonAdaptorBase
#else
class DEBUGs : public sc_mx_module
#endif
{
    friend class debug_S;
    friend class DEBUGs_MxDI;
    
public:
    debug_S* debug_Slave;

    sc_port<sc_mx_transaction_if> *memdbg;

    // constructor / destructor
#ifdef CARBON
    DEBUGs(sc_mx_module* carbonComp, const char *xtorName, CarbonObjectID **carbonObj, CarbonPortFactory *portFactory, 
           const CarbonDebugAccessCallbacks& dbaCb);
    
    // Member variables used when used in a Carbon Model component
    CarbonDebugReadWriteFunction *mReadDebug;
    CarbonDebugReadWriteFunction *mWriteDebug;
#else
    DEBUGs(sc_mx_m_base* c, const string &s);
    string getName();
    string getProperty( MxPropertyType property );
    MXDI* getMxDI();
#endif
    virtual ~DEBUGs();    

    // overloaded sc_mx_module methods
    void setParameter(const string &name, const string &value);
    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);
  
    // Drive Carbon Model, needed for Read Data Phase;
    void driveModel();

    bool saveData(eslapi::CASIODataStream& os);
    bool restoreData(eslapi::CASIIDataStream& is);

private:
    bool p_initComplete; 
    bool p_enableDbgMsg;
    MXDI* p_mxdi;
    bool bigEndian;

    MxU64 p_Size;
    MxU64 p_Base;
    
    bool p_slaveSelected;
    CarbonDebugAccessCallbacks mDbaCb;
    
};

#endif
