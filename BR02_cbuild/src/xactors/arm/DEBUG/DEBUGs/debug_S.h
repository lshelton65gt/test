// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef _debug_S_H_
#define _debug_S_H_

#include "maxsim.h"

#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
#define MAX_REGIONS 0x1
#endif

class DEBUGs;

class debug_S: public sc_mx_transaction_slave
{
    DEBUGs *owner;

public:
    debug_S( DEBUGs *_owner );
    virtual ~debug_S() {}

public:
    /* Synchronous access functions */
    virtual MxStatus read(MxU64 addr, MxU32* value, MxU32* ctrl);
    virtual MxStatus write(MxU64 addr, MxU32* value, MxU32* ctrl);
    virtual MxStatus readDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
    virtual MxStatus writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
    
    /* Asynchronous access functions */
    virtual MxStatus readReq(MxU64 addr, MxU32* value, MxU32* ctrl,
                             MxTransactionCallbackIF* callback);
    virtual MxStatus writeReq(MxU64 addr, MxU32* value, MxU32* ctrl,
                              MxTransactionCallbackIF* callback);

    /* Memory map functions */
    virtual int  getNumRegions();
    virtual void getAddressRegions(MxU64* start, MxU64* size, string* name);

#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
    virtual void setAddressRegions(MxU64* start, MxU64* size, string* name);
    virtual MxMemoryMapConstraints* getMappingConstraints();
#endif

    bool firstError;

    void setDataBusWidth( MxU32 dataWidthInWords ) {
        prop.dataBitwidth = dataWidthInWords * 32;
        setProperties( &prop );
    }

#ifdef CARBON
    void setMxOwner(DEBUGs *owner);
#endif

private:
	MxTransactionProperties prop;

#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
    // initialize Slave Constraints
    void initMemoryMapConstraints();
    MxMemoryMapConstraints puMemoryMapConstraints;
    string regionName[MAX_REGIONS];
#endif

};

#endif

