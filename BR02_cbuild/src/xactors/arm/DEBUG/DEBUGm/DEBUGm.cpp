/***************************************************************************************
  Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <stdio.h>
#include "DEBUGm.h"
#include "DEBUGm_MxDI.h"

#define DEBUG_DATA_WIDTH 32
#define DEBUG_PROTOCOL_ID 0x10
#define DEBUG_PROTOCOL_NAME "CarbonDebug"

#ifdef CARBON
#include <stdarg.h>
#endif

#ifdef CARBON
#define PORT_OWNER mCarbonComponent
DEBUGm::DEBUGm(sc_mx_module* carbonComp, const char *xtorName, CarbonObjectID **carbonObj, CarbonPortFactory *portFactory,
               CarbonDebugReadWriteFunction *readDebug, CarbonDebugReadWriteFunction *writeDebug)
  : CarbonAdaptorBase(carbonComp, xtorName, carbonObj), mReadDebug(readDebug), mWriteDebug(writeDebug)
#else
#define PORT_OWNER this
DEBUGm::DEBUGm(sc_mx_m_base* c, const string &s) : sc_mx_module(c, s)
#endif
{
  debug_TMaster = new MxTransactionMasterPort( "debug" );
  MxTransactionProperties prop;
  memset(&prop,0,sizeof(prop));
  prop.mxsiVersion = MXSI_VERSION_6;
  prop.useMultiCycleInterface = false;
  prop.addressBitwidth=64;
  prop.mauBitwidth = 8;
  prop.dataBitwidth = 32;
  prop.dataBeats = 1;
  prop.isLittleEndian = true;
  prop.isOptional = false;
  prop.supportsAddressRegions = false;
  prop.numCtrlFields = 2;
  prop.numSlaveFlags = 0;
  prop.numMasterFlags = 0; 
  prop.numTransactionSteps = 2; 
  prop.validTimingTable = NULL;
  prop.protocolID = DEBUG_PROTOCOL_ID;
  sprintf(prop.protocolName, DEBUG_PROTOCOL_NAME);
  prop.supportsNotify = false;
  prop.supportsBurst = false;
  prop.supportsSplit = false;
  prop.isAddrRegionForwarded = false;
  prop.forwardAddrRegionToMasterPort = NULL;
  debug_TMaster->setProperties(&prop);
  registerPort(debug_TMaster, "debug" );

  maxsim::MxSignalProperties *signalprop = new maxsim::MxSignalProperties;
  signalprop->isOptional = false;

#ifndef CARBON
    SC_MX_CLOCKED();
    registerPort( dynamic_cast< sc_mx_clock_slave_p_base* >( this ), "clk-in");
#endif

    p_initComplete = false;
    p_mxdi = NULL;

#ifndef CARBON
    defineParameter( "Enable Debug Messages", "false", MX_PARAM_BOOL, true);
    defineParameter( "Base Address", "0x0", MX_PARAM_VALUE, false);
    defineParameter( "Size", "0x100000000", MX_PARAM_VALUE, false);
    defineParameter( "Big Endian", "false", MX_PARAM_BOOL, false );
#endif
}

DEBUGm::~DEBUGm()
{
  delete debug_TMaster;
}

void 
DEBUGm::init()
{
  p_mxdi = new DEBUGm_MxDI(this);
  
  if (p_mxdi == NULL)
    {
      message(MX_MSG_FATAL_ERROR,"init: Failed to allocate memory for MxDI interface. Aborting Simulation!");
    }

#ifndef CARBON
  // Call the base class after this has been initialized.
  sc_mx_module::init();
#endif
  p_initComplete = true;
}

void
DEBUGm::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  (void)level; (void)filelist;

  // Check that protocol ID & data width match 
  if (!MxPortCheck::check(debug_TMaster))
    {
      message(MX_MSG_WARNING, "Incompatible ports connected.");
    }
      
#ifndef CARBON
    // Call the base class after this has been reset.
    sc_mx_module::reset(level, filelist);
#endif
}



void 
DEBUGm::terminate()
{
#ifndef CARBON
  // Call the base class first.
  sc_mx_module::terminate();
#endif

  if (p_mxdi != NULL)
  {
    // Release the MXDI Interface
    delete p_mxdi;
    p_mxdi = NULL;
  }
}

void
DEBUGm::setParameter( const string &name, const string &value )
{
  MxConvertErrorCodes status = MxConvert_SUCCESS;
  
  if (name == "Enable Debug Messages")
    {
      status = MxConvertStringToValue(value, &p_enableDbgMsg);
    }
  
  if ( status == MxConvert_SUCCESS )
    {
#ifndef CARBON
      sc_mx_module::setParameter(name, value);
#endif
    }
  else
    {
      message( MX_MSG_WARNING, "DEBUGm::setParameter: illegal value <%s> "
	       "passed for parameter <%s>. Assignment ignored.", value.c_str(), name.c_str() );
    }
}

#ifndef CARBON
string
DEBUGm::getProperty( MxPropertyType property )
{
  string description; 
  switch ( property ) 
  {    
    case MX_PROP_LOADFILE_EXTENSION:
      return "";
    case MX_PROP_COMPONENT_TYPE:
      return "Other"; 
    case MX_PROP_COMPONENT_VERSION:
      return "2.0.002";
    case MX_PROP_MSG_PREPEND_NAME:
      return "yes"; 
    case MX_PROP_DESCRIPTION:
      description = "Debug Transaction Master";
      return description + " Compiled on " + __DATE__ + ", " + __TIME__; 
    case MX_PROP_MXDI_SUPPORT:
    	return "yes";
    case MX_PROP_SAVE_RESTORE:
    	return "no";     
    default:
      return "";
  }
  return "";
}

MXDI*
DEBUGm::getMxDI()
{
  return p_mxdi;
}

string
DEBUGm::getName(void)
{
  return "DEBUGm";
}

/************************
 * DEBUGm Factory class
 ***********************/
class DEBUGmFactory : public MxFactory
{
public:
  DEBUGmFactory() : MxFactory ( "DEBUGm" ) {}
  sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
  { 
    return new DEBUGm(c, id); 
  }
};

/**
 *  Entry point into the memory components (from MaxSim)
 */
extern "C" void 
MxInit(void)
{
  new DEBUGmFactory();
}

#endif

MxStatus DEBUGm::readDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return debug_TMaster->readDbg(addr, value, ctrl);
}

MxStatus DEBUGm::writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return debug_TMaster->writeDbg(addr, value, ctrl);
}

bool DEBUGm::saveData(eslapi::CASIODataStream& os)
{
  bool ok = true;
  return ok;
}

bool DEBUGm::restoreData(eslapi::CASIIDataStream& is)
{
  bool ok = true;
  return ok;
}
