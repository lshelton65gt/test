// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef DEBUGm__H
#define DEBUGm__H

#include "maxsim.h"
#include "MxPortCheck.h"

#include "carbon_arm_adaptor.h"

#ifdef CARBON
#include "CarbonAdaptorBase.h"
#include "util/UtCLicense.h"
#endif

class DEBUGm_MxDI;

#ifdef CARBON
class DEBUGm : public CarbonAdaptorBase
#else
class DEBUGm : public sc_mx_module
#endif
{
    friend class DEBUGm_MxDI;
    
public:
    MxTransactionMasterPort* debug_TMaster;

    // constructor / destructor
#ifdef CARBON
    DEBUGm(sc_mx_module* carbonComp, const char *xtorName, CarbonObjectID **carbonObj, CarbonPortFactory *portFactory, 
           CarbonDebugReadWriteFunction *readDebug, CarbonDebugReadWriteFunction *writeDebug);
    
    // Member variables used when used in a Carbon Model component
    CarbonDebugReadWriteFunction *mReadDebug;
    CarbonDebugReadWriteFunction *mWriteDebug;
#else
    DEBUGm(sc_mx_m_base* c, const string &s);
    string getName();
    string getProperty( MxPropertyType property );
    MXDI* getMxDI();
#endif
    virtual ~DEBUGm();    

    // overloaded sc_mx_module methods
    void setParameter(const string &name, const string &value);
    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);

    MxStatus readDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
    MxStatus writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl);

    bool saveData(eslapi::CASIODataStream& os);
    bool restoreData(eslapi::CASIIDataStream& is);

private:
  bool p_initComplete; 
  bool p_enableDbgMsg;

    MXDI* p_mxdi;

};

#endif
