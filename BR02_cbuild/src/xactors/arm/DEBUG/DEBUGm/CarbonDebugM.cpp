#include "CarbonDebugM.h"
#include "AHB_Transaction7.h"

#define CARBON 1
#include "DEBUGm.h"

CarbonDebugM::CarbonDebugM(sc_mx_module* carbonComp,
                           const char *xtorName,
                           CarbonObjectID **carbonObj,
                           CarbonPortFactory *portFactory, 
                           CarbonDebugReadWriteFunction *readDebug,
                           CarbonDebugReadWriteFunction *writeDebug)
{
  mArmAdaptor = new DEBUGm(carbonComp, xtorName, carbonObj, portFactory, readDebug, writeDebug);
}

sc_mx_signal_slave *CarbonDebugM::carbonGetPort(const char *name)
{
  sc_mx_signal_slave *port;

  // There are no ports
  port = NULL;
  mArmAdaptor->message(MX_MSG_FATAL_ERROR, "Carbon DEBUGm has no port named \"%s\"", name);

  return port;
}

CarbonDebugM::~CarbonDebugM()
{
  delete mArmAdaptor;
}

void CarbonDebugM::communicate()
{
}

void CarbonDebugM::update()
{
}

void CarbonDebugM::init()
{
  mArmAdaptor->init();
}

void CarbonDebugM::terminate()
{
  mArmAdaptor->terminate();
}

void CarbonDebugM::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  mArmAdaptor->reset(level, filelist);
}

void CarbonDebugM::setParameter(const string &name, const string &value)
{
  mArmAdaptor->setParameter(name, value);
}

//! Number of slave memory regions
int CarbonDebugM::getNumRegions()
{
  return mArmAdaptor->getNumMasterRegions();
}

//! Slave Address regions
void CarbonDebugM::getAddressRegions(uint64_t* start, uint64_t* size, string* name)
{
  mArmAdaptor->getMasterAddressRegions(start, size, name);
}

MxTransactionMasterPort* CarbonDebugM::getTmPort()
{
  return mArmAdaptor->debug_TMaster;
}

CarbonDebugAccessStatus CarbonDebugM::debugAccess(CarbonDebugAccessDirection dir,
                                                  uint64_t startAddress, 
                                                  uint32_t numBytes, 
                                                  uint8_t* data, 
                                                  uint32_t* numBytesProcessed,
                                                  uint32_t*)
{
  // We're performing only byte accesses for simplicity
  uint32_t ctrl[AHB_IDX_END];
  ctrl[AHB_IDX_TYPE]  = AHB_TYPE_BYTE;
  ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_DATA;
  
  *numBytesProcessed = 0;
  for(MxU32 i = 0; i < numBytes; ++i) {
    eslapi::CASIStatus status = eslapi::CASI_STATUS_OK;
    if (dir == eCarbonDebugAccessWrite) {
      uint32_t value = 0;
      memset(&value, data[i], 4);
      status = mArmAdaptor->writeDbg(startAddress+i, &value, ctrl);
    }
    else {
      uint32_t value = 0;
      status = mArmAdaptor->readDbg(startAddress+i, &value, ctrl);
      data[i] = value & 0xFF;
    }
    
    // Check return status
    switch (status) {
    case eslapi::CASI_STATUS_ERROR:
    case eslapi::CASI_STATUS_NOTSUPPORTED:
      return eCarbonDebugAccessStatusError;
    case eslapi::CASI_STATUS_NOMEMORY:
      if (i == 0) return eCarbonDebugAccessStatusPartial;
      else return eCarbonDebugAccessStatusPartial;
    default:
      // Update how many bytes we have written so far
      *numBytesProcessed = i+1;
    }
  }

  // If we made it all the way here the entire transaction was successful
  return eCarbonDebugAccessStatusOk;
}

bool CarbonDebugM::saveData(eslapi::CASIODataStream& os)
{
  return mArmAdaptor->saveData(os);
}

bool CarbonDebugM::restoreData(eslapi::CASIIDataStream& is)
{
  return mArmAdaptor->restoreData(is);
}
