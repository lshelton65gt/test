/***************************************************************************************
  Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include <stdio.h>
#include "APBS2T.h"
#include "APBS2T_RTL_SS.h"
#include "APBS2T_MxDI.h"
#include "AHB_Transaction.h"

#define APB_READ  0
#define APB_WRITE 1

#ifdef CARBON
#include <stdarg.h>
#define APBS2T_RTL_SM(NAME) portFactory(mCarbonComponent, xtorName, NAME, mpCarbonObject)
#else
#define APBS2T_RTL_SM(NAME) new sc_port<sc_mx_signal_if>(this, NAME)
#endif

#ifdef CARBON
#define PORT_OWNER mCarbonComponent
APBS2T::APBS2T(sc_mx_module* carbonComp, const char *xtorName, CarbonObjectID **carbonObj, CarbonPortFactory *portFactory,
               bool apb3, CarbonDebugReadWriteFunction *readDebug, CarbonDebugReadWriteFunction *writeDebug)
  : CarbonAdaptorBase(carbonComp, xtorName, carbonObj), mReadDebug(readDebug), mWriteDebug(writeDebug)
#else
#define PORT_OWNER this
APBS2T::APBS2T(sc_mx_m_base* c, const string &s) : sc_mx_module(c, s)
#endif
{
  mWriteData = 0;
  mReadData = 0;
  mControl[0] = 0;
  mControl[1] = 0;
  p_defaultPready = 1;

  apb_TMaster = new MxTransactionMasterPort( "apb" );
  MxTransactionProperties prop;
  memset(&prop,0,sizeof(prop));
  prop.mxsiVersion = MXSI_VERSION_6;
  prop.useMultiCycleInterface = false;
  prop.addressBitwidth=64;
  prop.mauBitwidth = 8;
  prop.dataBitwidth = 32;
  prop.dataBeats = 1;
  prop.isLittleEndian = true;
  prop.isOptional = false;
  prop.supportsAddressRegions = false;
  prop.numCtrlFields = 2;
  prop.numSlaveFlags = 0;
  prop.numMasterFlags = 0; 
  prop.numTransactionSteps = 2; 
  prop.validTimingTable = NULL;
  prop.protocolID = 0x00010A9B;
  sprintf(prop.protocolName,"CarbonAPB");
  prop.supportsNotify = false;
  prop.supportsBurst = false;
  prop.supportsSplit = false;
  prop.isAddrRegionForwarded = false;
  prop.forwardAddrRegionToMasterPort = NULL;
  apb_TMaster->setProperties(&prop);
  registerPort(apb_TMaster, "apb" );

  maxsim::MxSignalProperties *signalprop = new maxsim::MxSignalProperties;
  signalprop->isOptional = false;

    // signals from RTL
    PADDR_SSlave = new APBS2T_RTL_SS( this);
    signalprop->bitwidth = 32;
    PADDR_SSlave->setProperties(signalprop);
    registerPort( PADDR_SSlave, "paddr" );
  
    PWRITE_SSlave = new APBS2T_RTL_SS( this);
    signalprop->bitwidth = 1;
    PWRITE_SSlave->setProperties(signalprop);
    registerPort( PWRITE_SSlave, "pwrite" );
  
    PSEL_SSlave = new APBS2T_RTL_SS( this);
    signalprop->bitwidth = 1;
    PSEL_SSlave->setProperties(signalprop);
    registerPort( PSEL_SSlave, "psel" );
  
    PENABLE_SSlave = new APBS2T_RTL_SS( this);
    signalprop->bitwidth = 1;
    PENABLE_SSlave->setProperties(signalprop);
    registerPort( PENABLE_SSlave, "penable" );
  
    PWDATA_SSlave = new APBS2T_RTL_SS( this);
    signalprop->bitwidth = 32;
    PWDATA_SSlave->setProperties(signalprop);
    registerPort( PWDATA_SSlave, "pwdata" );
  
    // signals to RTL
    PRDATA_SMaster = APBS2T_RTL_SM( "PRDATA" );
    signalprop->bitwidth = 32;
    PRDATA_SMaster->setProperties(signalprop);
    registerPort( PRDATA_SMaster, "prdata" );
    
    // Only APB3 has PReady and PSlvErr
    if (apb3) {
      PREADY_SMaster = APBS2T_RTL_SM( "PREADY" );
      PSLVERR_SMaster = APBS2T_RTL_SM( "PSLVERR" );
    } else {
      PREADY_SMaster = NULL;
      PSLVERR_SMaster = NULL;
    }

#ifndef CARBON
    SC_MX_CLOCKED();
    registerPort( dynamic_cast< sc_mx_clock_slave_p_base* >( this ), "clk-in");
#endif

    p_initComplete = false;
    p_mxdi = NULL;

#ifndef CARBON
    defineParameter( "Enable Debug Messages", "false", MX_PARAM_BOOL, true);
    defineParameter( "Base Address", "0x0", MX_PARAM_VALUE, false);
    defineParameter( "Size", "0x100000000", MX_PARAM_VALUE, false);
    defineParameter( "Data Bus Width", "32", MX_PARAM_VALUE, false);
    defineParameter( "Big Endian", "false", MX_PARAM_BOOL, false );
#endif
}

APBS2T::~APBS2T()
{
  delete apb_TMaster;
  delete PRDATA_SMaster;
  delete PREADY_SMaster;
  delete PSLVERR_SMaster;
  delete PADDR_SSlave;
  delete PWDATA_SSlave;
  delete PWRITE_SSlave;
  delete PSEL_SSlave;
  delete PENABLE_SSlave;
}

void 
APBS2T::communicate()
{

  // If psel then call transaction write or read with
  // address or data depending on penable.  
  // Fill in the control array

  if (PSEL_SSlave->readSignal() == 1) {
    MxStatus status = MX_STATUS_OK;
    if (PWRITE_SSlave->readSignal() == 1) {
      // Doing a write
      if (PENABLE_SSlave->readSignal() == 0) {
        // Address cycle
        mWriteData = PWDATA_SSlave->readSignal();
        mControl[MX_IDX_TYPE] = MX_TYPE_WORD;
        mControl[MX_IDX_CYCLE] = MX_CYCLE_ADDR;
        status = apb_TMaster->write(PADDR_SSlave->readSignal(),
                                    &mWriteData,
                                    mControl);
      } else {
        // Data cycle
        mWriteData = PWDATA_SSlave->readSignal();
        mControl[MX_IDX_TYPE] = MX_TYPE_WORD;
        mControl[MX_IDX_CYCLE] = MX_CYCLE_DATA;
        status = apb_TMaster->write(PADDR_SSlave->readSignal(),
                                    &mWriteData,
                                    mControl);
      }
    } else {
      // Doing a read
      if (PENABLE_SSlave->readSignal() == 0) {
        // Address cycle
        mControl[MX_IDX_TYPE] = MX_TYPE_WORD;
        mControl[MX_IDX_CYCLE] = MX_CYCLE_ADDR;
        status = apb_TMaster->read(PADDR_SSlave->readSignal(),
                                   &mReadData,
                                   mControl);
      } else {
        // Data cycle
        mControl[MX_IDX_TYPE] = MX_TYPE_WORD;
        mControl[MX_IDX_CYCLE] = MX_CYCLE_DATA;
        status = apb_TMaster->read(PADDR_SSlave->readSignal(),
                                   &mReadData,
                                   mControl);
      }
    }

    // Drive PReady and PSlvErr depending on the return status.
    mPReady = (status != MX_STATUS_WAIT);
    mPSlvErr = (status == MX_STATUS_ERROR);
  }
}

void
APBS2T::update()
{
  // If psel and read and penable, drive read data to the Carbon Model
  if ((PSEL_SSlave->readSignal() == 1) &&
      (PENABLE_SSlave->readSignal() == 1) &&
      (PWRITE_SSlave->readSignal() == 0)) {
    PRDATA_SMaster->driveSignal(mReadData, NULL);
  }
  
  // Drive PReady returned from slave in the enable phase, otherwise drive default PReady
  if(PREADY_SMaster) {
    if (PSEL_SSlave->readSignal() == 1 && PENABLE_SSlave->readSignal() == 1)
      PREADY_SMaster->driveSignal(mPReady, NULL);
    else
      PREADY_SMaster->driveSignal(p_defaultPready, NULL);
  }
  // Drive PSlvErr, if it exists
  if (PSLVERR_SMaster) {
    PSLVERR_SMaster->driveSignal(mPSlvErr, NULL);
  }

}

void 
APBS2T::init()
{
  p_mxdi = new APBS2T_MxDI(this);
  
  if (p_mxdi == NULL)
    {
      message(MX_MSG_FATAL_ERROR,"init: Failed to allocate memory for MxDI interface. Aborting Simulation!");
    }

#ifndef CARBON
  // Call the base class after this has been initialized.
  sc_mx_module::init();
#endif
  p_initComplete = true;
}

void
APBS2T::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  (void)level; (void)filelist;

  mWriteData = 0;
  mReadData = 0;
  mPReady   = 1; // Default PReady to 1
  mPSlvErr  = 0;
  mControl[0] = 0;
  mControl[1] = 0;

  PRDATA_SMaster    -> driveSignal(0, NULL);

  // Check that protocol ID & data width match those of APB Bus   
  if (!MxPortCheck::check(apb_TMaster))
    {
      message(MX_MSG_WARNING, "Incompatible ports connected.");
    }
      
#ifndef CARBON
    // Call the base class after this has been reset.
    sc_mx_module::reset(level, filelist);
#endif
}



void 
APBS2T::terminate()
{
#ifndef CARBON
  // Call the base class first.
  sc_mx_module::terminate();
#endif

  if (p_mxdi != NULL)
  {
    // Release the MXDI Interface
    delete p_mxdi;
    p_mxdi = NULL;
  }
}

void
APBS2T::setParameter( const string &name, const string &value )
{
  MxConvertErrorCodes status = MxConvert_SUCCESS;
  
  if (name == "Enable Debug Messages")
    {
      status = MxConvertStringToValue(value, &p_enableDbgMsg);
    }
  
  else if (name == "PReady Default High")
    {
      status = MxConvertStringToValue(value, &p_defaultPready);
    }
  
  if ( status == MxConvert_SUCCESS )
    {
#ifndef CARBON
      sc_mx_module::setParameter(name, value);
#endif
    }
  else
    {
      message( MX_MSG_WARNING, "APBS2T::setParameter: illegal value <%s> "
	       "passed for parameter <%s>. Assignment ignored.", value.c_str(), name.c_str() );
    }
}

#ifndef CARBON
string
APBS2T::getProperty( MxPropertyType property )
{
  string description; 
  switch ( property ) 
  {    
    case MX_PROP_LOADFILE_EXTENSION:
      return "";
    case MX_PROP_COMPONENT_TYPE:
      return "Other"; 
    case MX_PROP_COMPONENT_VERSION:
      return "2.0.002";
    case MX_PROP_MSG_PREPEND_NAME:
      return "yes"; 
    case MX_PROP_DESCRIPTION:
      description = "APB MaxSim Transaction to HDL Signals Converter";
      return description + " Compiled on " + __DATE__ + ", " + __TIME__; 
    case MX_PROP_MXDI_SUPPORT:
    	return "yes";
    case MX_PROP_SAVE_RESTORE:
    	return "no";     
    default:
      return "";
  }
  return "";
}

MXDI*
APBS2T::getMxDI()
{
  return p_mxdi;
}

string
APBS2T::getName(void)
{
  return "APBS2T";
}

/************************
 * APBS2T Factory class
 ***********************/
class APBS2TFactory : public MxFactory
{
public:
  APBS2TFactory() : MxFactory ( "APBS2T" ) {}
  sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
  { 
    return new APBS2T(c, id); 
  }
};

/**
 *  Entry point into the memory components (from MaxSim)
 */
extern "C" void 
MxInit(void)
{
  new APBS2TFactory();
}

#endif

MxStatus APBS2T::readDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return apb_TMaster->readDbg(addr, value, ctrl);
}

MxStatus APBS2T::writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  return apb_TMaster->writeDbg(addr, value, ctrl);
}

bool APBS2T::saveData(eslapi::CASIODataStream& os)
{
  os << mControl[0] << mControl[1] << mWriteData << mReadData << mPReady << mPSlvErr;

  bool ok = true;
  ok &= PADDR_SSlave->saveData(os);
  ok &= PWRITE_SSlave->saveData(os);
  ok &= PSEL_SSlave->saveData(os);
  ok &= PENABLE_SSlave->saveData(os);
  ok &= PWDATA_SSlave->saveData(os);

  return ok;
}

bool APBS2T::restoreData(eslapi::CASIIDataStream& is)
{
  is >> mControl[0] >> mControl[1] >> mWriteData >> mReadData >> mPReady >> mPSlvErr;

  bool ok = true;
  ok &= PADDR_SSlave->restoreData(is);
  ok &= PWRITE_SSlave->restoreData(is);
  ok &= PSEL_SSlave->restoreData(is);
  ok &= PENABLE_SSlave->restoreData(is);
  ok &= PWDATA_SSlave->restoreData(is);

  return ok;
}
