/*
 *    Copyright (c) 2004-2005 ARM Limited, 2000-2004 AXYS Design Automation Incorporated.
 *    ARM Limited proprietary and confidential information.
 *    All rights reserved.
 */

#include "APBS2T_RTL_SS.h"

#ifdef CARBON
#include "xactors/arm/include/carbon_arm_adaptor.h"
#endif
#include "APBS2T.h"


APBS2T_RTL_SS::APBS2T_RTL_SS( APBS2T *_owner ) : sc_mx_signal_slave( "APBS2T_RTL_SS" )
{
  mOwner = _owner;
  mValue = 0;
#ifdef CARBON
  setMxOwner(mOwner->mCarbonComponent);
#else
  setMxOwner(mOwner); 
#endif
}

void
APBS2T_RTL_SS::driveSignal(MxU32 value, MxU32* extValue)
{
  (void)extValue;
  mValue = value;
}

MxU32
APBS2T_RTL_SS::readSignal()
{
  return mValue;
}

void
APBS2T_RTL_SS::readSignal(MxU32* value, MxU32* extValue)
{
  (void)extValue;
  *value = mValue;
}

bool APBS2T_RTL_SS::saveData(eslapi::CASIODataStream& os)
{
  os << mValue;
  return true;
}

bool APBS2T_RTL_SS::restoreData(eslapi::CASIIDataStream& is)
{
  is >> mValue;
  return true;
}
