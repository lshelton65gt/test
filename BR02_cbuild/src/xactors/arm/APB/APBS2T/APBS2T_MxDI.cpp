/***************************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "APBS2T_MxDI.h"
#include "APBS2T.h"

// APBS2T core register definitions
// local Register Information Structure for constant initiallization
struct RegInfo_t
{
	MxU32					level;	// group and compound level
	char*					name;
	MxU32					resourceId;
	MxU32					bitsWide;
	MxU32					lsbOffset;
	MxdiRegDisplay_t 		display;
	bool  					isPseudoReg;
	char* 					description;
	char*					fpFormat;
	MxU32					numSymbols;
 	char** 					symbolList;
};

/*
 * 1 : level		// 0 register groups, 1 registers, >=2  components of regs and comp
 * 2 : name	 		// name of the register (displayed in first column of register win)
 * 3 : resourceId	// identification for access of data (e.g. index into register array)
 * 4 : bitsWide		// bit width of the displayed register or component
 * 5 : lsbOffset	// lsb position when accessing the resource
 * 6 : display		// MXDI_REGTYPE_[HEX, UINT, INT, BOOL, FLOAT, SYMBOL, STRING]
 * 7 : isPseudoReg	// indicates that this is no hardware register
 * 8 : description	// Register description being displayed as tool tip for that register
 * 9: fpFormat		// In case of REGTYPE_FLOAT refer to structure of MxdiRegFloatFormat_t (printf)
 * 10: numSymbols	// number of entries in list of symbols 
 * 11: symbolList	// In case of REGTYPE_SYMBOL refer to structure of MxdiRegSymbols_t
 * 
 */

#define NO_SYMBOLS_FLOAT 0, 0, 0
#define NO_FLOAT_HRESP_SYMBOLS 0, 4, hresp_symbols
#define NO_FLOAT_HBURST_SYMBOLS 0, 8, hburst_symbols
#define NO_FLOAT_HTRANS_SYMBOLS 0, 4, htrans_symbols

/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////

static const RegInfo_t apbt2s_reg_info[] =
{
  { 0, "RTL Inputs", 0, 32, 0, MXDI_REGTYPE_HEX, false, "signals latched from RTL", NO_SYMBOLS_FLOAT }
// 	{ 0, "RTL Inputs", 0, 32, 0, MXDI_REGTYPE_HEX, false, "signals latched from RTL", NO_SYMBOLS_FLOAT },
// 	{ 1, "hreadyout", HREADYOUT_SS, 1, 0, MXDI_REGTYPE_BOOL, false, "hreadyout", NO_SYMBOLS_FLOAT },
// 	{ 1, "hresp", HRESP_SS, 2, 0, MXDI_REGTYPE_SYMBOL, false, "hresp", NO_FLOAT_HRESP_SYMBOLS },
// 	{ 1, "hrdata[31:0]", HRDATA_SS_31_0, 32, 0, MXDI_REGTYPE_HEX, false, "hrdata[31:0]", NO_SYMBOLS_FLOAT },
// 	{ 1, "hrdata[63:32]", HRDATA_SS_63_32, 32, 0, MXDI_REGTYPE_HEX, false, "hrdata[63:32]", NO_SYMBOLS_FLOAT },
// 	{ 1, "hrdata[95:64]", HRDATA_SS_95_64, 32, 0, MXDI_REGTYPE_HEX, false, "hrdata[95:64]", NO_SYMBOLS_FLOAT },
// 	{ 1, "hrdata[127:96]", HRDATA_SS_127_96, 32, 0, MXDI_REGTYPE_HEX, false, "hrdata[127:96]", NO_SYMBOLS_FLOAT }
};


static const unsigned int REGINFO_COUNT	= sizeof( apbt2s_reg_info    ) / sizeof( apbt2s_reg_info[0]   );


APBS2T_MxDI::APBS2T_MxDI(APBS2T* c) : target(c)
{
	int		i;
	MxU32 maxLevel = 0;
	groupCount = 0;
	totalRegisterCount = 0;

	// Register related info
	// Count group entries (level == 0)
	// the info needs to start with a group !!!
	assert(apbt2s_reg_info[0].level == 0);

	for ( i = 0; i < (MxS32)REGINFO_COUNT; i++ )
	{
		if (apbt2s_reg_info[i].level == 0)
		{
			groupCount++;
		}
		if (maxLevel < apbt2s_reg_info[i].level)
		{
			maxLevel = apbt2s_reg_info[i].level;
		}
	}

	totalRegisterCount = REGINFO_COUNT - groupCount;

	regGroup         =	new MxdiRegGroup_t[ groupCount    ];
	groupStartIndex  = 	new MxU32         [ groupCount    ];
	regInfo          =	new MxdiRegInfo_t [ totalRegisterCount + 1 ];

	MxU32 g = groupCount - 1;	// current group counter
	MxU32 r = totalRegisterCount - 1;				// current register counter

	MxU32 componentCount[256];	// count how many registers are within a compound

	// loop over all entries of the apbt2s_reg_info table and enter information into
	// according local data structures. Start with the last entry !!!
	MxU32 level;
	MxU32 previousLevel = 0;
    // getRegMap does not care about compounds or not,
	// it simply wants to get all of them regardless of the level
	MxU32 numRegsInGroup = 0;	
	for ( i = REGINFO_COUNT - 1; i >= 0; i-- )
	{
		level = apbt2s_reg_info[i].level;
		if (level == 0)
		{	// group level -> all previous registers are part of this group
			groupStartIndex[g] = r + 1;
			regGroup[g].groupID	= g;
			strcpy( regGroup[g].name,		apbt2s_reg_info[i].name );
			strcpy( regGroup[g].description, apbt2s_reg_info[i].description );
			regGroup[g].numRegsInGroup = numRegsInGroup;
			regGroup[g].isPseudoRegister = apbt2s_reg_info[i].isPseudoReg;
			if (g > 0)
				g --;
			numRegsInGroup = 0;
		}
		else	// all others are registers or compound registers
		{
			if (level > previousLevel)
			{	// entering a component level
				// initialize/reset the levels that have been skipped when entering new level (e.g 0 -> 2)
				for (unsigned int t = previousLevel + 1; t < level; t++) componentCount[t] = 0;
				regInfo[r].details.type = MXDI_REGTYPE_Simple;	
				componentCount[level] = 1;
			}
			else if (level < previousLevel)
			{	// leaving a component level
				regInfo[r].details.type = MXDI_REGTYPE_Compound;	
				// save componentCount from previous level
				regInfo[r].details.u.compound.count = componentCount[level + 1];
				componentCount[level]++;
			}
			else
			{	// level == previousLevel
				regInfo[r].details.type = MXDI_REGTYPE_Simple;
				componentCount[level]++;
			}
			regInfo[r].regNumber			= i;	// index in table!
			regInfo[r].bitsWide				= apbt2s_reg_info[i].bitsWide;

			if (!strcmp(apbt2s_reg_info[i].name, "PC"))
			{	// found PC, set nPCRegNum...
				features.nPCRegNum = i;
			}

			switch (apbt2s_reg_info[i].display)
			{
			case MXDI_REGTYPE_FLOAT:
				strcpy(	regInfo[r].fpFormat.format, apbt2s_reg_info[i].fpFormat );
				break;
			case MXDI_REGTYPE_SYMBOL:
				regInfo[r].symbols.numSymbols = apbt2s_reg_info[i].numSymbols;
				regInfo[r].symbols.Symbols = apbt2s_reg_info[i].symbolList;
				break;
			default:
					break;
			}
			regInfo[r].display				= apbt2s_reg_info[i].display;
			regInfo[r].hasSideEffects		= 0;
			strcpy( regInfo[r].name,		  apbt2s_reg_info[i].name );
			strcpy( regInfo[r].description,	  apbt2s_reg_info[i].description );
			r--;
			numRegsInGroup++;
		}
		previousLevel = level;
		// maximum register bitwidth is 256!
		assert(apbt2s_reg_info[i].bitsWide <= 256);
	}

	// initialize MxDI feature entries
	strcpy( features.targetName, "APBS2T" );
	strcpy( features.targetVersion, "1.0" );
	features.nrRegisterGroups = groupCount;
}

APBS2T_MxDI::~APBS2T_MxDI()
{
    if ( regInfo )			
		delete [] regInfo;
	if ( groupStartIndex )
		delete [] groupStartIndex;
    if ( regGroup )		
		delete [] regGroup;
}


// Register related functions
MxdiReturn_t
APBS2T_MxDI::MxdiRegGetGroups( MxU32 groupIndex
                                      , MxU32 desiredNumOfRegGroups
                                      ,	MxU32* actualNumOfRegGroups
                                      , MxdiRegGroup_t* grp )
{
	if ( groupIndex >= groupCount )
	{
		return MXDI_STATUS_IllegalArgument;
	}

	MxU32	i, j;
	for( j = 0, i = groupIndex; ( i < groupIndex + desiredNumOfRegGroups ) && ( i < groupCount ); i++, j++ )
	{
		grp[j] = regGroup[i];
	}

	*actualNumOfRegGroups = j;
	return MXDI_STATUS_OK;
}

MxdiReturn_t
APBS2T_MxDI::MxdiRegGetMap( MxU32 groupID
                                   , MxU32 regStartIndex
                                   , MxU32 desiredNumberOfRegisters
                                   , MxU32* actualNumberOfRegisters
                                   , MxdiRegInfo_t* reg )
{
	if ( groupID >= groupCount )
	{
		return MXDI_STATUS_IllegalArgument;
	}

	MxU32 i, j;	// j: index into provided reg buffer, i: index into local register info array structure
	for ( j = 0, i = groupStartIndex[groupID]+regStartIndex; (j < desiredNumberOfRegisters) && (i < totalRegisterCount) ; i++, j++ )
	{
		reg[j] = regInfo[i];
	}
	// actual number of register info provided
	*actualNumberOfRegisters = j;
	return MXDI_STATUS_OK;
}

// get register map for compound registers (which bitfields belongs to which register)
MxdiReturn_t
APBS2T_MxDI::MxdiRegGetCompound(
	MxU32 reg,
	MxU32 componentIndex, 
	MxU32 desiredNumOfComponents, 
	MxU32 *actualNumOfcomponents, 
	MxU32 *components) 
{
	// find reg
	MxU32 i, j = 0, ignore = 0;
	MxU32 compoundLevel = apbt2s_reg_info[reg].level + 1;
	for ( i = reg + 1; ((j < desiredNumOfComponents) && (i < REGINFO_COUNT)); i++)
	{
		if ( compoundLevel == apbt2s_reg_info[i].level)
		{
			if (componentIndex >= ignore)
			{
				components[j] = i;
				j++;
			}
			else
			{
				ignore ++;
			}
		}
		else if (compoundLevel > apbt2s_reg_info[i].level)
		{
			break;
		}
	}
	*actualNumOfcomponents = j;	
	return MXDI_STATUS_OK;
}


MxdiReturn_t
APBS2T_MxDI::MxdiRegWrite( MxU32 regCount
                                  , MxdiReg_t* reg
                                  , MxU32* numRegsWritten
                                  , MxU8 doSideEffects )
{
  (void)doSideEffects;

	MxU32	i;
	MxU32 index;
	MxU32 resourceId;
	MxU32 lsbOffset;
	MxU64 masked;
	MxU32 bitsWide;
	MxU32 failed = 0;
	MxU64 tmp64, tmp64_rd, tmp;

	for ( i = 0; i < regCount; i++ )
	{
		index = reg[i].regNumber;
		resourceId = apbt2s_reg_info[index].resourceId; 	
		lsbOffset = apbt2s_reg_info[index].lsbOffset;
		bitsWide = apbt2s_reg_info[index].bitsWide;
		masked = ~((MxU64CONST(0xFFFFFFFFFFFFFFFF)) << bitsWide);

		//tmp64_rd = target->latchedFromRTL[resourceId];
		tmp64_rd &= ~(masked << lsbOffset);
		tmp64 =         (reg[i].bytes[0]      );
		tmp64 = tmp64 | (reg[i].bytes[1] << 8 );
		tmp64 = tmp64 | (reg[i].bytes[2] << 16);
		tmp64 = tmp64 | (reg[i].bytes[3] << 24);
		tmp = reg[i].bytes[4];
		tmp64 = tmp64 | (tmp << 32);
		tmp = reg[i].bytes[5];
		tmp64 = tmp64 | (tmp << 40);
		tmp = reg[i].bytes[6];
		tmp64 = tmp64 | (tmp << 48);
		tmp = reg[i].bytes[7];
		tmp64 = tmp64 | (tmp << 56);
		tmp64 = tmp64_rd | ((tmp64 & masked) << lsbOffset);
		//target->latchedFromRTL[resourceId] = tmp64;
	}

	*numRegsWritten = regCount - failed;

	return MXDI_STATUS_OK;
}

MxdiReturn_t
APBS2T_MxDI::MxdiRegRead( MxU32 regCount
                                , MxdiReg_t* reg
                                , MxU32* numRegsRead
                                , MxU8 doSideEffects )
{
  (void)doSideEffects;

	MxU32 i;
	MxU32 index;
	MxU32 resourceId;
	MxU32 lsbOffset;
	MxU64 mask;
	MxU32 failed = 0;
	MxU64 tmp64 = 0;
	for( i = 0; i < regCount; i++ )
	{
		index = reg[i].regNumber;
		resourceId = apbt2s_reg_info[index].resourceId; 	
		lsbOffset = apbt2s_reg_info[index].lsbOffset;
		mask = ~(MxU64CONST(0xFFFFFFFFFFFFFFFF) << apbt2s_reg_info[index].bitsWide); 	
		// Resource Access
		//tmp64 = target->latchedFromRTL[resourceId];
		tmp64 = (tmp64 >> lsbOffset) & mask;
		reg[i].bytes[0] = (tmp64 >> 0)  & 0xff;
		reg[i].bytes[1] = (tmp64 >> 8)  & 0xff;
		reg[i].bytes[2] = (tmp64 >> 16) & 0xff;
		reg[i].bytes[3] = (tmp64 >> 24) & 0xff;
		reg[i].bytes[4] = (tmp64 >> 32) & 0xff;
		reg[i].bytes[5] = (tmp64 >> 40) & 0xff;
		reg[i].bytes[6] = (tmp64 >> 48) & 0xff;
		reg[i].bytes[7] = (tmp64 >> 56) & 0xff;
	}
	*numRegsRead = regCount - failed;

	return MXDI_STATUS_OK;
}
