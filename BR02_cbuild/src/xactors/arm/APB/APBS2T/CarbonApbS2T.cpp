#include "CarbonApbS2T.h"
#include "AHB_Transaction7.h"

#define CARBON 1
#include "APBS2T.h"
#include "APBS2T_RTL_SS.h"

CarbonApbS2T::CarbonApbS2T(sc_mx_module* carbonComp,
                           const char *xtorName,
                           CarbonObjectID **carbonObj,
                           CarbonPortFactory *portFactory, 
                           bool apb3,
                           CarbonDebugReadWriteFunction *readDebug,
                           CarbonDebugReadWriteFunction *writeDebug)
{
  mArmAdaptor = new APBS2T(carbonComp, xtorName, carbonObj, portFactory, apb3, readDebug, writeDebug);
}

sc_mx_signal_slave *CarbonApbS2T::carbonGetPort(const char *name)
{
  sc_mx_signal_slave *port;

  if (strcmp("paddr", name) == 0) {
    port = mArmAdaptor->PADDR_SSlave;
  }
  else if (strcmp("pwrite", name) == 0) {
    port = mArmAdaptor->PWRITE_SSlave;
  }
  else if (strcmp("psel", name) == 0) {
    port = mArmAdaptor->PSEL_SSlave;
  }
  else if (strcmp("penable", name) == 0) {
    port = mArmAdaptor->PENABLE_SSlave;
  }
  else if (strcmp("pwdata", name) == 0) {
    port = mArmAdaptor->PWDATA_SSlave;
  }
  else {
    port = NULL;
    mArmAdaptor->message(MX_MSG_FATAL_ERROR, "Carbon APB S2T has no port named \"%s\"", name);
  }
  return port;
}

CarbonApbS2T::~CarbonApbS2T()
{
  delete mArmAdaptor;
}

void CarbonApbS2T::communicate()
{
  mArmAdaptor->communicate();
}

void CarbonApbS2T::update()
{
  mArmAdaptor->update();
}

void CarbonApbS2T::init()
{
  mArmAdaptor->init();
}

void CarbonApbS2T::terminate()
{
  mArmAdaptor->terminate();
}

void CarbonApbS2T::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  mArmAdaptor->reset(level, filelist);
}

void CarbonApbS2T::setParameter(const string &name, const string &value)
{
  mArmAdaptor->setParameter(name, value);
}

//! Number of slave memory regions
int CarbonApbS2T::getNumRegions()
{
  return mArmAdaptor->getNumMasterRegions();
}

//! Slave Address regions
void CarbonApbS2T::getAddressRegions(uint64_t* start, uint64_t* size, string* name)
{
  mArmAdaptor->getMasterAddressRegions(start, size, name);
}

MxTransactionMasterPort* CarbonApbS2T::getTmPort()
{
  return mArmAdaptor->apb_TMaster;
}

CarbonDebugAccessStatus CarbonApbS2T::debugAccess(CarbonDebugAccessDirection dir,
                                                  uint64_t startAddress, 
                                                  uint32_t numBytes, 
                                                  uint8_t* data, 
                                                  uint32_t* numBytesProcessed,
                                                  uint32_t*)
{
  // We're performing only byte accesses for simplicity
  uint32_t ctrl[AHB_IDX_END];
  ctrl[AHB_IDX_TYPE]  = AHB_TYPE_BYTE;
  ctrl[AHB_IDX_CYCLE] = AHB_CYCLE_DATA;
  
  *numBytesProcessed = 0;
  for(MxU32 i = 0; i < numBytes; ++i) {
    eslapi::CASIStatus status = eslapi::CASI_STATUS_OK;
    if (dir == eCarbonDebugAccessWrite) {
      uint32_t value = 0;
      memset(&value, data[i], 4);
      status = mArmAdaptor->writeDbg(startAddress+i, &value, ctrl);
    }
    else {
      uint32_t value = 0;
      status = mArmAdaptor->readDbg(startAddress+i, &value, ctrl);
      data[i] = value & 0xFF;
    }
    
    // Check return status
    switch (status) {
    case eslapi::CASI_STATUS_ERROR:
    case eslapi::CASI_STATUS_NOTSUPPORTED:
      return eCarbonDebugAccessStatusError;
    case eslapi::CASI_STATUS_NOMEMORY:
      if (i == 0) return eCarbonDebugAccessStatusPartial;
      else return eCarbonDebugAccessStatusPartial;
    default:
      // Update how many bytes we have written so far
      *numBytesProcessed = i+1;
    }
  }

  // If we made it all the way here the entire transaction was successful
  return eCarbonDebugAccessStatusOk;
}

bool CarbonApbS2T::saveData(eslapi::CASIODataStream& os)
{
  return mArmAdaptor->saveData(os);
}

bool CarbonApbS2T::restoreData(eslapi::CASIIDataStream& is)
{
  return mArmAdaptor->restoreData(is);
}
