// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef APBS2T__H
#define APBS2T__H

#include "maxsim.h"
#include "APB_Globals.h"
#include "MxPortCheck.h"

#include "carbon_arm_adaptor.h"

#ifdef CARBON
#include "CarbonAdaptorBase.h"
#include "util/UtCLicense.h"
#endif

#define RTL_PORT CarbonXtorAdaptorToVhmPort
#define HRDATA_SS HRDATA_SS_31_0

#define APB_DEFAULT_BUSWIDTH 32

// supporting up to 128bit data bus
#define MAX_DATA_WIDTH_IN_WORDS 4

class APBS2T_MxDI;

#ifdef CARBON
class APBS2T : public CarbonAdaptorBase
#else
class APBS2T : public sc_mx_module
#endif
{
    friend class APBS2T_RTL_SS;
    friend class APBS2T_MxDI;
    
public:
    MxTransactionMasterPort* apb_TMaster;

    RTL_PORT      *PRDATA_SMaster;
    RTL_PORT      *PREADY_SMaster;
    RTL_PORT      *PSLVERR_SMaster;
    
    APBS2T_RTL_SS *PADDR_SSlave;    
    APBS2T_RTL_SS *PWRITE_SSlave;
    APBS2T_RTL_SS *PSEL_SSlave;
    APBS2T_RTL_SS *PENABLE_SSlave;
    APBS2T_RTL_SS *PWDATA_SSlave;

    // constructor / destructor
#ifdef CARBON
    APBS2T(sc_mx_module* carbonComp, const char *xtorName, CarbonObjectID **carbonObj, CarbonPortFactory *portFactory, 
           bool apb3, CarbonDebugReadWriteFunction *readDebug, CarbonDebugReadWriteFunction *writeDebug);
    
    // Member variables used when used in a Carbon Model component
    CarbonDebugReadWriteFunction *mReadDebug;
    CarbonDebugReadWriteFunction *mWriteDebug;
#else
    APBS2T(sc_mx_m_base* c, const string &s);
    string getName();
    string getProperty( MxPropertyType property );
    MXDI* getMxDI();
#endif
    virtual ~APBS2T();    

	// overloaded methods for clocked components
    void communicate();
    void update();

    // overloaded sc_mx_module methods
    void setParameter(const string &name, const string &value);
    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);

    MxStatus readDbg(MxU64 addr, MxU32* value, MxU32* ctrl);
    MxStatus writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl);

    bool saveData(eslapi::CASIODataStream& os);
    bool restoreData(eslapi::CASIIDataStream& is);

private:
  bool p_initComplete; 
  bool p_enableDbgMsg;
  bool p_defaultPready;

    MXDI* p_mxdi;

public:
  // Control array to send to write and read functions
  MxU32 mControl[2];
  MxU32 mWriteData;
  MxU32 mReadData;
  MxU32 mPReady;
  MxU32 mPSlvErr;
};

#endif
