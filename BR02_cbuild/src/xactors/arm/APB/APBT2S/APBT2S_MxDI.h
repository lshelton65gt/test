// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef APBT2S_MxDI_H
#define APBT2S_MxDI_H

#include "maxsim.h"

class APBT2S;

class APBT2S_MxDI : public MxDIBase
{
 public:
    APBT2S_MxDI(APBT2S* c);
    virtual ~APBT2S_MxDI();

 public:

    // Register access functions
    MxdiReturn_t	MxdiRegGetGroups(   MxU32 groupIndex
                                        , MxU32 desiredNumOfRegGroups
                                        , MxU32* actualNumOfRegGroups
                                        , MxdiRegGroup_t* reg );

    MxdiReturn_t	MxdiRegGetMap(      MxU32 groupID
                                        , MxU32 regIndex
                                        , MxU32 registerSlots
                                        , MxU32* registerCount
                                        , MxdiRegInfo_t* reg );

	MxdiReturn_t 	MxdiRegGetCompound(	MxU32 reg,
										MxU32 componentIndex, 
										MxU32 desiredNumOfComponents, 
										MxU32 *actualNumOfcomponents, 
										MxU32 *components );

    MxdiReturn_t	MxdiRegWrite(       MxU32 regCount
                                        , MxdiReg_t* reg
                                        , MxU32* numRegsWritten
                                        , MxU8 doSideEffects );

    MxdiReturn_t	MxdiRegRead(        MxU32 regCount
                                        , MxdiReg_t* reg
                                        , MxU32* numRegsRead
                                        , MxU8 doSideEffects );

 private:
#ifdef APBLITE
    APBT2SLITE*		target;
#else
    APBT2S*		target;
#endif

    // Register related info
    MxdiRegInfo_t*		regInfo;
    MxdiRegGroup_t*		regGroup;
	MxU32*				groupStartIndex;
	MxU32 groupCount;
	MxU32 totalRegisterCount;
};

#endif

