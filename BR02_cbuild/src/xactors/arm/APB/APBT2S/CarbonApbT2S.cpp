
#include "CarbonApbT2S.h"

#define CARBON 1
#include "APBT2S.h"
#include "APBT2S_RTL_SS.h"

CarbonApbT2S::CarbonApbT2S(sc_mx_module* carbonComp,
                           const char *xtorName,
                           CarbonObjectID **carbonObj,
                           CarbonPortFactory *portFactory, 
                           CarbonDebugReadWriteFunction *readDebug,
                           CarbonDebugReadWriteFunction *writeDebug)
{
  CarbonDebugAccessCallbacks cb = {carbonComp, NULL, writeDebug, readDebug, NULL};
  mArmAdaptor = new APBT2S(carbonComp, xtorName, carbonObj, portFactory, cb);
}

CarbonApbT2S::CarbonApbT2S(sc_mx_module* carbonComp,
                           const char *xtorName,
                           CarbonObjectID **carbonObj,
                           CarbonPortFactory *portFactory, 
                           CarbonDebugAccessFunction *debugAccess)
{
  CarbonDebugAccessCallbacks cb = {carbonComp, debugAccess, NULL, NULL, NULL};
  mArmAdaptor = new APBT2S(carbonComp, xtorName, carbonObj, portFactory, cb);
}

sc_mx_signal_slave *CarbonApbT2S::carbonGetPort(const char *name)
{
  sc_mx_signal_slave *port;

  if (strcmp("prdata", name) == 0) {
    port = mArmAdaptor->fromPRData;
  }
  else if (strcmp("pready", name) == 0) {
    port = mArmAdaptor->fromPReady;
  }
  else if (strcmp("pslverr", name) == 0) {
    port = mArmAdaptor->fromPSlvErr;
  }
  else {
    port = NULL;
    mArmAdaptor->message(MX_MSG_FATAL_ERROR, "Carbon APB T2S has no port named \"%s\"", name);
  }
  return port;
}

CarbonApbT2S::~CarbonApbT2S()
{
  delete mArmAdaptor;
}

void CarbonApbT2S::communicate()
{
  mArmAdaptor->communicate();
}

void CarbonApbT2S::update()
{
  mArmAdaptor->update();
}

void CarbonApbT2S::init()
{
  mArmAdaptor->init();
}

void CarbonApbT2S::terminate()
{
  mArmAdaptor->terminate();
}

void CarbonApbT2S::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  mArmAdaptor->reset(level, filelist);
}

void CarbonApbT2S::setParameter(const string &name, const string &value)
{
  mArmAdaptor->setParameter(name, value);
}

//! Number of slave memory regions
int CarbonApbT2S::getNumRegions()
{
  return mArmAdaptor->getNumSlaveRegions();
}

//! Slave Address regions
void CarbonApbT2S::getAddressRegions(uint64_t* start, uint64_t* size, string* name)
{
  mArmAdaptor->getSlaveAddressRegions(start, size, name);
}

bool CarbonApbT2S::saveData(eslapi::CASIODataStream& os)
{
  return mArmAdaptor->saveData(os);
}

bool CarbonApbT2S::restoreData(eslapi::CASIIDataStream& is)
{
  return mArmAdaptor->restoreData(is);
}
