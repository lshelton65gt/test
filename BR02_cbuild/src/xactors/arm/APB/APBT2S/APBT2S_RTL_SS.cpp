/***************************************************************************************
  Copyright (c) 2006-2009 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "APBT2S_RTL_SS.h"
#include "APBT2S.h"

APBT2S_RTL_SS::APBT2S_RTL_SS( APBT2S *_owner, MxU32 _id ) : sc_mx_signal_slave( "APBT2S_RTL_SS" )
{
  id    = _id;
  owner = _owner;
#ifdef CARBON
  setMxOwner(owner->mCarbonComponent);
#else
  setMxOwner(owner); 
#endif
}

void
APBT2S_RTL_SS::driveSignal(MxU32 value, MxU32* extValue)
{
  (void)extValue;
  owner->latchedFromRTL[id] = value;
}

MxU32
APBT2S_RTL_SS::readSignal()
{
  return 0;
}

void
APBT2S_RTL_SS::readSignal(MxU32* value, MxU32* extValue)
{
  (void)value; (void)extValue;
}
