// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef APBT2S__H
#define APBT2S__H

#include "maxsim.h"
#include "APB_Globals.h"
#include "MxPortCheck.h"

#include "carbon_arm_adaptor.h"

#ifdef CARBON
#include "CarbonAdaptorBase.h"
#include "util/UtCLicense.h"
#endif

#define RTL_PORT CarbonXtorAdaptorToVhmPort
#define PRDATA_SS 0
#define PREADY_SS 1
#define PSLVERR_SS 2
#define APB_DEFAULT_BUSWIDTH 32

// supporting up to 128bit data bus
#define MAX_DATA_WIDTH_IN_WORDS 4

class APBT2S_MxDI;

#ifdef CARBON
class APBT2S : public CarbonAdaptorBase
#else
class APBT2S : public sc_mx_module
#endif
{
    friend class APBT2S_RTL_SS;
    friend class apb_TS;
    friend class APBT2S_MxDI;
    
public:
    apb_TS* apb_TSlave;

    APBT2S_RTL_SS* fromPRData;
    APBT2S_RTL_SS* fromPReady;
    APBT2S_RTL_SS* fromPSlvErr;
    
    RTL_PORT *PADDR_SMaster;    
    RTL_PORT *PWRITE_SMaster;
    RTL_PORT *PSEL_SMaster;
    RTL_PORT *PENABLE_SMaster;
    RTL_PORT *PWDATA_SMaster;

    sc_port<sc_mx_transaction_if> *memdbg;

    // constructor / destructor
#ifdef CARBON
    APBT2S(sc_mx_module* carbonComp, const char *xtorName, CarbonObjectID **carbonObj, CarbonPortFactory *portFactory, 
           const CarbonDebugAccessCallbacks& dbaCb);
    
    // Member variables used when used in a Carbon Model component
    CarbonDebugReadWriteFunction *mReadDebug;
    CarbonDebugReadWriteFunction *mWriteDebug;
#else
    APBT2S(sc_mx_m_base* c, const string &s);
    string getName();
    string getProperty( MxPropertyType property );
    MXDI* getMxDI();
#endif
    virtual ~APBT2S();    

	// overloaded methods for clocked components
    void communicate();
    void update();

    // overloaded sc_mx_module methods
    void setParameter(const string &name, const string &value);
    void init();
    void terminate();
    void reset(MxResetLevel level, const MxFileMapIF *filelist);
  
    // Drive Carbon Model, needed for Read Data Phase;
    void driveModel();

    bool saveData(eslapi::CASIODataStream& os);
    bool restoreData(eslapi::CASIIDataStream& is);

private:
    bool p_initComplete; 
    bool p_enableDbgMsg;
    MXDI* p_mxdi;
    bool bigEndian;

    MxU32 dataWidthInWords;

    MxU64 p_Size;
    MxU64 p_Base;
    
    // Values for signals to RTL 
    MxU32 paddr;
    MxU32 pwdata;
    MxU32 pwrite;
    MxU32 psel;
    MxU32 penable;

    bool p_slaveSelected;
    CarbonDebugAccessCallbacks mDbaCb;
    
public:
    MxU32 latchedFromRTL[3];
    
};

#endif
