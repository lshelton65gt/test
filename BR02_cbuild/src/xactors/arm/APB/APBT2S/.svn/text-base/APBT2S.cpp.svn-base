/***************************************************************************************
  Copyright (c) 2006-2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "shell/carbon_capi.h"
#include "APBT2S.h"
#include "APBT2S_RTL_SS.h"
#include "apb_TS.h"
#include "APBT2S_MxDI.h"

#define APB_READ  0
#define APB_WRITE 1

#ifdef CARBON
#include <stdarg.h>
#define APBT2S_RTL_SM(NAME) portFactory(mCarbonComponent, xtorName, NAME, mpCarbonObject)
#else
#define APBT2S_RTL_SM(NAME) new sc_port<sc_mx_signal_if>(this, NAME)
#endif

#ifdef CARBON
#define PORT_OWNER mCarbonComponent
APBT2S::APBT2S(sc_mx_module* carbonComp, const char *xtorName, CarbonObjectID **carbonObj, CarbonPortFactory *portFactory,
               const CarbonDebugAccessCallbacks& dbaCb)
  : CarbonAdaptorBase(carbonComp, xtorName, carbonObj), mDbaCb(dbaCb)
#else
#define PORT_OWNER this
APBT2S::APBT2S(sc_mx_m_base* c, const string &s) : sc_mx_module(c, s)
#endif
{
  fromPRData = NULL;
  fromPReady = NULL;
  fromPSlvErr = NULL;
  
  apb_TSlave = new apb_TS( this );
  registerPort(apb_TSlave, "apb" );

  maxsim::MxSignalProperties *signalprop = new maxsim::MxSignalProperties;
  signalprop->isOptional = false;

#ifndef CARBON
    memdbg = new sc_port<sc_mx_transaction_if>(PORT_OWNER, "memdbg");
    registerPort( memdbg, "memdbg" );
    MxTransactionProperties tprop;
    APB_INIT_TRANSACTION_PROPERTIES( tprop );
    memdbg->setProperties( &tprop );
#endif

    // signals from RTL
    fromPRData = new APBT2S_RTL_SS( this, PRDATA_SS);
    signalprop->bitwidth = 32;
    fromPRData->setProperties(signalprop);
    registerPort( fromPRData, "prdata" );
  
    fromPReady = new APBT2S_RTL_SS( this, PREADY_SS);
    signalprop->bitwidth = 32;
    fromPReady->setProperties(signalprop);
    registerPort( fromPReady, "pready" );
  
    fromPSlvErr = new APBT2S_RTL_SS( this, PSLVERR_SS);
    signalprop->bitwidth = 32;
    fromPSlvErr->setProperties(signalprop);
    registerPort( fromPSlvErr, "pslverr" );

    // signals to RTL
    PADDR_SMaster = APBT2S_RTL_SM( "PADDR" );
    signalprop->bitwidth = 32;
    PADDR_SMaster->setProperties(signalprop);
    registerPort( PADDR_SMaster, "paddr" );

    PWDATA_SMaster = APBT2S_RTL_SM( "PWDATA" );
    signalprop->bitwidth = 32;
    PWDATA_SMaster->setProperties(signalprop);
    registerPort( PWDATA_SMaster, "pwdata" );

    PWRITE_SMaster = APBT2S_RTL_SM( "PWRITE" );
    signalprop->bitwidth = 1;
    PWRITE_SMaster->setProperties(signalprop);
    registerPort( PWRITE_SMaster, "pwrite" );

    PSEL_SMaster = APBT2S_RTL_SM( "PSEL" );
    signalprop->bitwidth = 1;
    PSEL_SMaster->setProperties(signalprop);
    registerPort( PSEL_SMaster, "psel" );

    PENABLE_SMaster = APBT2S_RTL_SM( "PENABLE" );
    signalprop->bitwidth = 1;
    PENABLE_SMaster->setProperties(signalprop);
    registerPort( PENABLE_SMaster, "penable" );
     
#ifndef CARBON
    SC_MX_CLOCKED();
    registerPort( dynamic_cast< sc_mx_clock_slave_p_base* >( this ), "clk-in");
#endif

    p_initComplete = false;
    p_mxdi = NULL;

#ifndef CARBON
    defineParameter( "Enable Debug Messages", "false", MX_PARAM_BOOL, true);
    defineParameter( "Base Address", "0x0", MX_PARAM_VALUE, false);
    defineParameter( "Size", "0x100000000", MX_PARAM_VALUE, false);
    defineParameter( "Data Bus Width", "32", MX_PARAM_VALUE, false);
    defineParameter( "Big Endian", "false", MX_PARAM_BOOL, false );
#endif
}

APBT2S::~APBT2S()
{
  delete apb_TSlave;
  delete fromPRData;
  delete fromPReady;
  delete fromPSlvErr;
  delete PADDR_SMaster;
  delete PWDATA_SMaster;
  delete PWRITE_SMaster;
  delete PSEL_SMaster;
  delete PENABLE_SMaster;
}

void 
APBT2S::communicate()
{
}

void
APBT2S::update()
{
  if (p_slaveSelected)
    {
      PSEL_SMaster->driveSignal(1, NULL);
      PENABLE_SMaster->driveSignal(penable, NULL);
      PADDR_SMaster->driveSignal(paddr, NULL);
      PWRITE_SMaster->driveSignal(pwrite, NULL);
      PWDATA_SMaster->driveSignal(pwdata, NULL);
    }
  else
    {
      PSEL_SMaster->driveSignal(0, NULL);
      PENABLE_SMaster->driveSignal(0, NULL);
    }
  
  p_slaveSelected = false; 
}

void 
APBT2S::init()
{
  p_mxdi = new APBT2S_MxDI(this);
  
  if (p_mxdi == NULL)
    {
      mCarbonComponent->message(MX_MSG_FATAL_ERROR,"init: Failed to allocate memory for MxDI interface. Aborting Simulation!");
    }

#ifndef CARBON
  // Call the base class after this has been initialized.
  sc_mx_module::init();
#endif
  p_initComplete = true;
}

void
APBT2S::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  (void)level; (void)filelist;

  latchedFromRTL[PRDATA_SS] = 0;

  // Initialize PREADY to 1, so "classic APB" works seamlessly
  latchedFromRTL[PREADY_SS] = 1;
  // Same for PSLVERR
  latchedFromRTL[PSLVERR_SS] = 0;
  pwdata            = 0;

  PADDR_SMaster    -> driveSignal(0, NULL);
  PWDATA_SMaster   -> driveSignal(pwdata, NULL);
  PWRITE_SMaster   -> driveSignal(APB_READ, NULL);
  PSEL_SMaster     -> driveSignal(0, NULL);

  p_slaveSelected = false;
    
  // Check that protocol ID & data width match those of APB Bus   
  if (!MxPortCheck::check((apb_TS*)apb_TSlave))
    {
      mCarbonComponent->message(MX_MSG_WARNING, "Incompatible ports connected.");
    }
      
#ifndef CARBON
    // Call the base class after this has been reset.
    sc_mx_module::reset(level, filelist);
#endif
}



void 
APBT2S::terminate()
{
#ifndef CARBON
  // Call the base class first.
  sc_mx_module::terminate();
#endif

  if (p_mxdi != NULL)
  {
    // Release the MXDI Interface
    delete p_mxdi;
    p_mxdi = NULL;
  }
}

void APBT2S::driveModel() {
#ifdef CARBON
  CarbonObjectID *model = *mpCarbonObject;
  CarbonTime currtime   = carbonGetSimulationTime(*mpCarbonObject);
  
  // Since We only use this method because penable is
  // asyncrounous I only drive penable here. This method
  // is only called in the Data Phase, and all other signals
  // should already be set to their correct values.
  PENABLE_SMaster->driveSignal(penable, NULL);
  carbonSchedule(model, currtime);

#endif
}

void
APBT2S::setParameter( const string &name, const string &value )
{
  MxConvertErrorCodes status = MxConvert_SUCCESS;
  
  if (name == "Enable Debug Messages")
    {
      status = MxConvertStringToValue(value, &p_enableDbgMsg);
    }
  else if (name == "Base Address")
    {
      if (p_initComplete == false)
        {
          status = MxConvertStringToValue(value, &p_Base);
        }
      else
        {
          message(MX_MSG_WARNING, "APBT2S::setParameter: cannot change parameter <%s> at runtime. Assignment ignored.", 
                  name.c_str() );            
        }
    }
  else if (name == "Size")
    {
      if (p_initComplete == false)
        {
          status = MxConvertStringToValue(value, &p_Size);
        }
      else
        {
          message(MX_MSG_WARNING, "APBT2S::setParameter: cannot change parameter <%s> at runtime. Assignment ignored.", 
                  name.c_str() );            
        }
    }
  else if (name == "Data Bus Width")
    {
      if (p_initComplete == false)
        {
          MxU32 tmp = 0;
          status = MxConvertStringToValue(value, &tmp);
          
          if (status == MxConvert_SUCCESS)
            {
              switch ( tmp )
                {
                case 32:  dataWidthInWords = 1; break;
                case 64:  dataWidthInWords = 2; break;
                case 128: dataWidthInWords = 4; break;
                default:
                  message( MX_MSG_WARNING, "invalid value %s for parameter: %s. Accepted valued are: 32, 64 or 128", value.c_str(), name.c_str() );
                  break;
                }
            }
        }
      else
        {
         message(MX_MSG_WARNING, 
                  "APBT2S::setParameter: cannot change parameter <%s> at runtime. Assignment ignored.", 
                  name.c_str() );
        }
    }
  else if (name == "Big Endian")
    {
      status = MxConvertStringToValue(value, &bigEndian);
    }
  
  
  if ( status == MxConvert_SUCCESS )
    {
#ifndef CARBON
      sc_mx_module::setParameter(name, value);
#endif
    }
  else
    {
      message( MX_MSG_WARNING, "APBT2S::setParameter: illegal value <%s> "
	       "passed for parameter <%s>. Assignment ignored.", value.c_str(), name.c_str() );
    }
}

bool APBT2S::saveData(eslapi::CASIODataStream& os)
{
  os << paddr << pwdata << pwrite << psel << penable << p_slaveSelected;
  os << latchedFromRTL[0] << latchedFromRTL[1] << latchedFromRTL[2];
  return true;
}

bool APBT2S::restoreData(eslapi::CASIIDataStream& is)
{
  is >> paddr >> pwdata >> pwrite >> psel >> penable >> p_slaveSelected;
  is >> latchedFromRTL[0] >> latchedFromRTL[1] >> latchedFromRTL[2];
  return true;
}

#ifndef CARBON
string
APBT2S::getProperty( MxPropertyType property )
{
  string description; 
  switch ( property ) 
  {    
    case MX_PROP_LOADFILE_EXTENSION:
      return "";
    case MX_PROP_COMPONENT_TYPE:
      return "Other"; 
    case MX_PROP_COMPONENT_VERSION:
      return "2.0.002";
    case MX_PROP_MSG_PREPEND_NAME:
      return "yes"; 
    case MX_PROP_DESCRIPTION:
      description = "APB MaxSim Transaction to HDL Signals Converter";
      return description + " Compiled on " + __DATE__ + ", " + __TIME__; 
    case MX_PROP_MXDI_SUPPORT:
    	return "yes";
    case MX_PROP_SAVE_RESTORE:
    	return "no";     
    default:
      return "";
  }
  return "";
}

MXDI*
APBT2S::getMxDI()
{
  return p_mxdi;
}

string
APBT2S::getName(void)
{
  return "APBT2S";
}

/************************
 * APBT2S Factory class
 ***********************/
class APBT2SFactory : public MxFactory
{
public:
  APBT2SFactory() : MxFactory ( "APBT2S" ) {}
  sc_mx_m_base *createInstance(sc_mx_m_base *c, const string &id)
  { 
    return new APBT2S(c, id); 
  }
};

/**
 *  Entry point into the memory components (from MaxSim)
 */
extern "C" void 
MxInit(void)
{
  new APBT2SFactory();
}

#endif
