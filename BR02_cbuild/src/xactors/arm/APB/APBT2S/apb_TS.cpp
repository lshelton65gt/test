/***************************************************************************************
  Copyright (c) 2006-2009 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "apb_TS.h"
#include "APBT2S.h"
#include "APB_Globals.h"
#include "AHB_Transaction.h"
//#include "AHB_acc.h"

#define APB_READ  0
#define APB_WRITE 1

#ifdef CARBON
void apb_TS::setMxOwner(APBT2S *owner)
{
  sc_mx_transaction_slave::setMxOwner(dynamic_cast<maxsim::sc_mx_m_base*>(owner->mCarbonComponent));
}
#endif

apb_TS::apb_TS( APBT2S *_owner ) : sc_mx_transaction_slave( "apb_TS" )
{
	owner = _owner;
	setMxOwner(owner);

	APB_INIT_TRANSACTION_PROPERTIES(prop);
	setProperties(&prop); 
  
	firstError = true;    	     
#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
    initMemoryMapConstraints();
#endif
}


#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
// *****************************************************************
// apb_TS::initMemoryMapconstraints() -- initialize constraints
// *****************************************************************
void
apb_TS::initMemoryMapConstraints()
{
    puMemoryMapConstraints.minRegionSize = 0x1;
    puMemoryMapConstraints.maxRegionSize = MxU64CONST(0x100000000);
    puMemoryMapConstraints.minAddress = 0x0;
    puMemoryMapConstraints.maxAddress = MxU64CONST(0xffffffff);
    puMemoryMapConstraints.minNumSupportedRegions = 0x1;        
    puMemoryMapConstraints.maxNumSupportedRegions = MAX_REGIONS;
    puMemoryMapConstraints.alignmentBlockSize = 0x400 ; //the min block size where this slave's regions can be mapped 
    puMemoryMapConstraints.numFixedRegions = 0;
    puMemoryMapConstraints.fixedRegionList = NULL;

}
#endif

/* If no regions are defined, numRegions returns 0, indicating that the 
 * port wants to be called for any MxU64 address */
int
apb_TS::getNumRegions()
{
  return 1;
}

void 
apb_TS::getAddressRegions(MxU64* start, MxU64* size, string* name)
{
  start[0] = owner->p_Base;
  size[0]  = owner->p_Size;
  name[0]  = "APBT2S";
}


#if (((MAXSIM_MAJOR_VERSION == 6) && (MAXSIM_MINOR_VERSION > 0)) || (MAXSIM_MAJOR_VERSION > 6))
void 
apb_TS::setAddressRegions(MxU64* start, MxU64* size, string* name)
{

  if (start && size && name)
    {
      if (owner->p_enableDbgMsg)
        {
            string port_name = this->getPortInstanceName();
            owner->mCarbonComponent->message(MX_MSG_INFO, "Set Address Region Called for Port %s", port_name.c_str());
        }
      for (unsigned int i = 0; i < MAX_REGIONS; i++)
	{
	  if (size[i] > 0)
	    {
	      owner->p_Base = start[i]; // be careful when changing MAX_REGIONS
	      owner->p_Size = size[i];
	      regionName[i] = name[i];
	      if (owner->p_enableDbgMsg)
		owner->mCarbonComponent->message(MX_MSG_INFO, "Address Region: start= 0x" MxFMT64x " size = 0x" MxFMT64x " Name= %s", 
			       start[i], size[i], 
			       name[i].c_str());
	    }
	  else
	    {
	      if (owner->p_enableDbgMsg)
                    owner->mCarbonComponent->message(MX_MSG_INFO, "Address Region (ignored due to size): start= 0x" MxFMT64x " size = 0x" MxFMT64x " Name= %s", 
                                   start[i], size[i], 
                                   name[i].c_str());
	      break;
	    }
	}
    }
  else
    {
#if (!CARBON)
      owner->mCarbonComponent->message(MX_MSG_ERROR, "%s: Uninitialized data structures passed in setAddressRegions call",
		     owner->getInstanceID().c_str());
#else
      owner->mCarbonComponent->message(MX_MSG_ERROR, "APB T2S: Uninitialized data structures passed in setAddressRegions call");
#endif
    }
}

// ************************************************************
// apb_TS::getMappingConstraints() -- get slave constraints
// ************************************************************

MxMemoryMapConstraints* 
apb_TS::getMappingConstraints() 
{
  //owner->mCarbonComponent->message(MX_MSG_INFO, "In getMappingConstraints function.\n");
  return &puMemoryMapConstraints;
}

#endif

MxStatus
apb_TS::read(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  MxStatus status = MX_STATUS_OK;
  
  if (ctrl[MX_IDX_CYCLE] == MX_CYCLE_ADDR) {
    // Latch address and control info used to drive signals in update phase
    owner->paddr   = addr;
    owner->pwrite  = APB_READ;
    owner->psel    = 1;
    owner->penable = 0;
    owner->p_slaveSelected = true;
  }

  else if (ctrl[MX_IDX_CYCLE] == MX_CYCLE_DATA) {
    owner->penable = 1;
    owner->p_slaveSelected = true;
    // Since there is an asyncronous path from penable to prdata,
    // we need to drive penable into the model here before we can get prdata
    owner->driveModel();
    value[0] = owner->latchedFromRTL[PRDATA_SS];
    // pslverr is only valid when pready is asserted, so check it first
    if (owner->latchedFromRTL[PREADY_SS] == 0) {
      status = MX_STATUS_WAIT;
    } else if (owner->latchedFromRTL[PSLVERR_SS] == 1) {
      status = MX_STATUS_ERROR;
    } else {
      status = MX_STATUS_OK;
    }
  }
  else {
    owner->psel    = 0;
  }
  
  return status;
}  
  


MxStatus 
apb_TS::write(MxU64 addr, MxU32* value, MxU32* ctrl)
{
  MxStatus status = MX_STATUS_OK;
  
  if (ctrl[MX_IDX_CYCLE] == MX_CYCLE_ADDR) {
    // Latch address and control info used to drive signals in update phase
    owner->paddr   = addr;
    owner->pwrite  = APB_WRITE;
    owner->psel    = 1;
    owner->penable = 0;
	if( value != NULL )
		owner->pwdata = value[0];
    owner->p_slaveSelected = true;
  }
  else if (ctrl[MX_IDX_CYCLE] == MX_CYCLE_DATA) {
    owner->p_slaveSelected = true;
    owner->pwdata = value[0];
    owner->penable = 1;
    // There may be an asyncronous path from penable to pready,
    // we need to drive penable into the model here before we can get pready
    owner->driveModel();
    // pslverr is only valid when pready is asserted, so check it first
    if (owner->latchedFromRTL[PREADY_SS] == 0) {
      status = MX_STATUS_WAIT;
    } else if (owner->latchedFromRTL[PSLVERR_SS] == 1) {
      status = MX_STATUS_ERROR;
    } else {
      status = MX_STATUS_OK;
    }
  }
  else {
    owner->psel    = 0;
    owner->penable = 0;
  }

  return status;
}  
  


MxStatus
apb_TS::readDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{   
#ifdef CARBON
  return CarbonDebugFunctionsToDebugAccess(owner->mDbaCb, eCarbonDebugAccessRead, prop.dataBitwidth, addr, value, ctrl);
#else
  return owner->memdbg->readDbg( addr, value, ctrl );
#endif
}


MxStatus
apb_TS::writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
#ifdef CARBON
  return CarbonDebugFunctionsToDebugAccess(owner->mDbaCb, eCarbonDebugAccessWrite, prop.dataBitwidth, addr, value, ctrl);
#else
  return owner->memdbg->writeDbg( addr, value, ctrl );
#endif
}


MxStatus
apb_TS::readReq(MxU64 addr, MxU32* value, MxU32* ctrl, MxTransactionCallbackIF* callback)
{
  MxStatus ret = MX_STATUS_NOTSUPPORTED;
  UNUSEDARG(addr);
  UNUSEDARG(value);
  UNUSEDARG(callback);
  UNUSEDARG(ctrl);
    
  return ret;
}

MxStatus
apb_TS::writeReq(MxU64 addr, MxU32* value, MxU32* ctrl, MxTransactionCallbackIF* callback)
{
  UNUSEDARG(addr);
  UNUSEDARG(value);
  UNUSEDARG(ctrl);
  UNUSEDARG(callback);
    
  return MX_STATUS_NOTSUPPORTED;
}
