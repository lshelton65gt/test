// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef _APBT2S_RTL_SS_H_
#define _APBT2S_RTL_SS_H_

#include "maxsim.h"


class APBT2S;

class APBT2S_RTL_SS: public sc_mx_signal_slave
{
  APBT2S *owner;
  MxU32 id;

public:
    APBT2S_RTL_SS( APBT2S *_owner, MxU32 _id );
    virtual ~APBT2S_RTL_SS() {}

public:
    virtual void driveSignal(MxU32 value, MxU32* extValue);
    virtual MxU32 readSignal();
    virtual void readSignal(MxU32* value, MxU32* extValue);
};

#endif

