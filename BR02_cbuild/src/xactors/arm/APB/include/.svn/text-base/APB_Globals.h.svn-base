#ifndef _APB_GLOBALS__H_
#define _APB_GLOBALS__H_

typedef enum {
    APB_IDLE = 0,   	// Idle state (PSEL=0, PENABLE=0)
    APB_SETUP = 1,  	// Setup state (PSEL=1, PENABLE=0)
    APB_ENABLE = 2,    	// Enable state (PSEL=1, PENABLE=1)
    APB_STATE_END = 3   // Add your new indices before this (always represent the last index)
} APB_STATE;

// Transfer parameters
typedef struct {
	MxU32 paddr;
	MxU32 pdata;
	bool  pwrite;
} TRANS_PARAM;
#define APB_DATA_WIDTH 32

#define APB_PROTOCOL_ID 0x10A9B

#define APB_INIT_TRANSACTION_PROPERTIES(props)  \
    (props).mxsiVersion = MXSI_VERSION_6;           /* this protocol uses standard read/write */ \
    (props).useMultiCycleInterface = false;         /* synchronous access functions	 */ \
    (props).addressBitwidth = 32;                   /* address bitwidth used for addressing of resources  */ \
    (props).mauBitwidth = 8;                        /* minimal addressable unit  */ \
    (props).dataBitwidth = APB_DATA_WIDTH;          /* maximum data bitwidth transferred in a single cycle */ \
    (props).isLittleEndian = true;                  /* alignment of MAUs */ \
    (props).isOptional = false;                     /* if true this port can be disabled */ \
    (props).supportsAddressRegions = true;          /* M/S can negotiate address mapping */ \
    (props).numCtrlFields = 4;                      /* # of ctrl elements used */ \
    (props).protocolID = APB_PROTOCOL_ID;           /* magic number of the protocol (APB Protocol!) */ \
    (props).numSlaveFlags = 0;                      /* not used but initialized */ \
    (props).numMasterFlags = 0;                     /* not used but initialized */ \
    (props).numTransactionSteps = 0;                /* not used but initialized */ \
    (props).supportsBurst = false;                  /* not used but initialized */ \
    (props).supportsSplit = false;                  /* not used but initialized */ \
    (props).supportsNotify = false;                 /* not used but initialized */ \
    (props).validTimingTable = NULL;	            /* not used but initialized */ \
    (props).details = NULL;                         /* not used but initialized */ \
    (props).forwardAddrRegionToMasterPort = NULL;   /* master port of the  same component to which this slave port's addr region is forwarded */ \
    (props).isAddrRegionForwarded = false;          /* MxSI 6.0: slave port address regions forwarding */

#endif
