/***************************************************************************************
  Copyright (c) 2009-2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
#include "carbon_arm_adaptor.h"
#include "AHB_Transaction7.h"
#include "CarbonReplicateData.h"
#include "MxAXIPort.h"

static eslapi::CASIStatus CarbonCommonDebugFunctionsToDebugAccess(const CarbonDebugAccessCallbacks& cb, CarbonDebugAccessDirection dir, uint32_t buswidth, uint64_t addr, uint32_t* value, uint32_t* ctrl, MxTransactionInfo* info)
{
  // figure out how this was called, and how to dispatch
  // the following choices are mutually exclusive:
  //   readDbg or writeDbg
  //   debugTransaction
  //   debugAccess
  if ( info && cb.debugTransaction ) {
    return cb.debugTransaction(cb.owner, info); 
  } else if (dir == eCarbonDebugAccessRead && cb.readDbg) {
    return cb.readDbg(cb.owner, addr, value, ctrl);
  } else if (dir == eCarbonDebugAccessWrite && cb.writeDbg) {
    return cb.writeDbg(cb.owner, addr, value, ctrl);
  } else if( (info==NULL) && cb.debugAccess) {
    uint8_t buf[128]; // Max 1024 bits
    uint32_t numBytes = 1;
    switch(ctrl[AHB_IDX_TYPE]) {
    case AHB_TYPE_BYTE :
      numBytes = 1;
      break;
    case AHB_TYPE_HWORD :
      numBytes = 2;
      break;
    case AHB_TYPE_WORD :
      numBytes = 4;
      break;
    case AHB_TYPE_DWORD :
      numBytes = 8;
      break;
    default:
      return eslapi::CASI_STATUS_ERROR; // Only support up to 64 bit accesses
    }

    // Write Operation
    if (dir == eCarbonDebugAccessWrite) {
      for (uint32_t i = 0; i < numBytes; ++i) {
        buf[i] = value[i/4] >> ((i&3)*8);
      }
      
      // Send the Access to debugAccess function
      uint32_t numBytesProcessed;
      CarbonDebugAccessStatus result = cb.debugAccess(cb.owner, eCarbonDebugAccessWrite, addr, numBytes, buf, &numBytesProcessed, NULL);
      switch(result) {
      case eCarbonDebugAccessStatusOk:
        return eslapi::CASI_STATUS_OK;
      case eCarbonDebugAccessStatusOutRange:
        return eslapi::CASI_STATUS_NOMEMORY;
      default:
        return eslapi::CASI_STATUS_ERROR;
      }
          
    }

    // Read Operation
    else {
      // Perform read operation
      uint32_t numBytesProcessed;
      CarbonDebugAccessStatus result = cb.debugAccess(cb.owner, eCarbonDebugAccessRead, addr, numBytes, buf, &numBytesProcessed, NULL);
      switch(result) {
      case eCarbonDebugAccessStatusOk:
        
        // Copy bytes to value and replicate data
        memset(value, 0, ((buswidth+31)/32)*4);
        for (uint32_t i = 0; i < numBytes; ++i) {
          uint32_t mask = 0xff << ((i%4)*8);
          value[i/4] = (value[i/4] & ~mask) | (((uint32_t)buf[i]) << (i*8)) & mask;
        }
        // Need to set HSIZE to a small value before calling Replicate function 
        // or it will exit. HSIZE is not actually used in the replication process
        // So its only need to be small enough to pass the check.
        AHB_ACC_SET_HSIZE(ctrl[AHB_IDX_ACC], 1);

        CarbonReplicateData(ctrl, (buswidth+31)/32, value);
        return eslapi::CASI_STATUS_OK;
      case eCarbonDebugAccessStatusOutRange:
        return eslapi::CASI_STATUS_NOMEMORY;
      case eCarbonDebugAccessStatusError:
        return eslapi::CASI_STATUS_ERROR;
      default:
        return eslapi::CASI_STATUS_ERROR;
      }
    }
  } else if ( info && cb.debugAccess) {
    // axi debugTransaction converted to debugAccess
    const CarbonDebugAccessDirection direction = (info->access == MX_ACCESS_WRITE) ? eCarbonDebugAccessWrite : eCarbonDebugAccessRead;
    uint32_t ctrl[1];  // control flags
    uint64_t startAddress = info->addr;
    uint32_t unitSizeInBytes = info->dataSize;

    uint8_t       buf[128];   // this needs to be large enough to hold all possible data, up to 1024 bits per burst, only one burst allowed
    uint32_t numBytesProcessed;

    AXITransaction* axi = static_cast<AXITransaction*>(info); // this code counts on this begin true

    ctrl[0] = 0;
    if ( (axi->get_prot() & AXI_PROTECTION_SEC) != 0 ) {  
      ctrl[0] |= 0x1;  // bit 0 is the security bit, we use same polarity as axi protocol, 1==non-secure, 0==secure
    }
   
    if ( direction == eCarbonDebugAccessWrite ) {

      MxU32 tmpData[32];  // large enough to hold all data that could be in info
      axi->alignWrDataHw2Sw(0,tmpData); // move data to tmpData, and convert organization from hardware to software

      // copy data out of tmpData array into buf
      uint32_t curSrcIdx = 0;
      uint32_t curDataIdx = 0;
      uint32_t srcData = tmpData[curSrcIdx];

      for ( curDataIdx = 0; curDataIdx < unitSizeInBytes; curDataIdx++ )
      {
        buf[curDataIdx] = srcData & 0xFF;        // transfer a byte

        // advance srcData to next byte
        if ( ( ( curDataIdx+1) % 4 ) == 0 ) {
          curSrcIdx++;
          srcData = tmpData[curSrcIdx];
        } else {
          srcData = srcData >> 8;
        }
      }
    }

    CarbonDebugAccessStatus result = cb.debugAccess(cb.owner, direction, startAddress, 
                                                    unitSizeInBytes, buf, &numBytesProcessed, ctrl);

    if ( ( direction == eCarbonDebugAccessRead ) &&
         ( result == eCarbonDebugAccessStatusOk) || ( result == eCarbonDebugAccessStatusPartial) ) {
      MxU32 tmpData[32];  // large enough to hold all data that could be in info

      // copy data value back into info structure
      for (uint32_t i = 0; i < (numBytesProcessed+3)/4; i++)
      {
        switch ( unitSizeInBytes % 4 ) {
        case 1:
          tmpData[i] = (                                                        buf[(i*4)+0]);
          break;
        case 2:
          tmpData[i] = (                                      buf[(i*4)+1]<<8 | buf[(i*4)+0]);
          break;
        case 3:
          tmpData[i] = (                   buf[(i*4)+2]<<16 | buf[(i*4)+1]<<8 | buf[(i*4)+0]);
          break;
        case 0:
          tmpData[i] = (buf[(i*4)+3]<<24 | buf[(i*4)+2]<<16 | buf[(i*4)+1]<<8 | buf[(i*4)+0]);
          break;
        }
      }
      axi->alignRdDataSw2Hw(0,tmpData);// move data from tmpData, and convert organization from software to hardware
    }

    switch(result) {
    case eCarbonDebugAccessStatusOk:
      return eslapi::CASI_STATUS_OK;
    case eCarbonDebugAccessStatusOutRange:
      return eslapi::CASI_STATUS_NOMEMORY;
    default:
      return eslapi::CASI_STATUS_ERROR;
    }
  }
  return eslapi::CASI_STATUS_ERROR;
}

eslapi::CASIStatus CarbonDebugFunctionsToDebugAccess(const CarbonDebugAccessCallbacks& cb, CarbonDebugAccessDirection dir, uint32_t buswidth, uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  return CarbonCommonDebugFunctionsToDebugAccess(cb, dir, buswidth, addr, value, ctrl, NULL);
}
eslapi::CASIStatus CarbonDebugFunctionsToDebugAccess(const CarbonDebugAccessCallbacks& cb, MxTransactionInfo* info)
{
  return CarbonCommonDebugFunctionsToDebugAccess(cb,
                                           eCarbonDebugAccessRead, // unused
                                           0,                      // unused
                                           0,                      // unused
                                           NULL,                   // unused
                                           NULL,                   // unused
                                           info ); // used
}

