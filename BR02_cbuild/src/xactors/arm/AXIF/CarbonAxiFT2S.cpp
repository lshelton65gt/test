
#include "CarbonAxiFT2S.h"

#define CARBON 1
#include "AXI_T2S.h"
#include "ss_SS.h"

#include "CarbonXtorEvent.h"
#include "AXIFXtorEvents.h"

#include <iostream>
using namespace std;

// refactor the common code from both of these ctors into a single function
CarbonAxiFT2S::CarbonAxiFT2S(sc_mx_module* carbonComp,
		const char *xtorName,
		CarbonObjectID **carbonObj,
		CarbonPortFactory *portFactory,
		CarbonDebugReadWriteFunction* readDebug,  // should only allow readDebug/writeDebug or debugTrans to be set
		CarbonDebugReadWriteFunction* writeDebug,
		CarbonDebugTransactionFunction* debugTrans,
		CarbonCombFunction* combFunc)
{
	// should have assert here: assert (((debugTrans) ^ readDebug) && (debugTrans ^ writeDebug))
	CarbonDebugAccessCallbacks cb = {carbonComp, NULL, writeDebug, readDebug, debugTrans};
	AXI_T2S* temp = new AXI_T2S(carbonComp, xtorName, "2.1.003", carbonObj, portFactory, cb, combFunc);
	mArmAdaptor = temp;
	doneDrive = doneNotify = false;

	awReadyChanged = false;
	arReadyChanged = false;
	wReadyChanged = false;
	rChanged = false;
	bChanged = false;

	arReadyChangedFunc = new NotifyAREvent(temp);
	awReadyChangedFunc = new NotifyAWEvent(temp);
	wReadyChangedFunc  = new NotifyWEvent (temp);
	rChangedFunc       = new DriveREvent  (temp);
	bChangedFunc       = new DriveBEvent  (temp);
}

CarbonAxiFT2S::CarbonAxiFT2S(sc_mx_module* carbonComp,
		const char *xtorName,
		CarbonObjectID **carbonObj,
		CarbonPortFactory *portFactory,
		CarbonDebugAccessFunction *debugAccess,
		CarbonCombFunction* combFunc)
{
	CarbonDebugAccessCallbacks cb = {carbonComp, debugAccess, NULL, NULL, NULL};
	AXI_T2S* temp = new AXI_T2S(carbonComp, xtorName, "2.1.003", carbonObj, portFactory, cb, combFunc);
	mArmAdaptor = temp;
	doneDrive = doneNotify = false;

	awReadyChanged = false;
	arReadyChanged = false;
	wReadyChanged = false;
	rChanged = false;
	bChanged = false;

	arReadyChangedFunc = new NotifyAREvent(temp);
	awReadyChangedFunc = new NotifyAWEvent(temp);
	wReadyChangedFunc  = new NotifyWEvent (temp);
	rChangedFunc       = new DriveREvent  (temp);
	bChangedFunc       = new DriveBEvent  (temp);
}


sc_mx_signal_slave *CarbonAxiFT2S::carbonGetPort(const char *name)
{
#define FIND_PORT(CHANNEL) \
	if (strcmp(#CHANNEL, name) == 0) \
		return ((AXI_T2S *) mArmAdaptor)->CHANNEL ## _SSlave

	FIND_PORT(rvalid);
	FIND_PORT(ruser );
	FIND_PORT(rid   );
	FIND_PORT(rresp );
	FIND_PORT(rlast );
	FIND_PORT(rdata );

	FIND_PORT(bvalid);
	FIND_PORT(buser );
	FIND_PORT(bid   );
	FIND_PORT(bresp );

	FIND_PORT(awready);
	FIND_PORT(arready);
	FIND_PORT(wready );

	((AXI_T2S *) mArmAdaptor)->message(MX_MSG_FATAL_ERROR, "Carbon AXI T2S has no port named \"%s\"", name);

	return NULL;
}

CarbonAxiFT2S::~CarbonAxiFT2S()
{
	delete ((AXI_T2S *) mArmAdaptor);

	delete awReadyChangedFunc;
	delete arReadyChangedFunc;
	delete wReadyChangedFunc;
	delete rChangedFunc; 
	delete bChangedFunc;
}

void CarbonAxiFT2S::backwardValid()
{
	((AXI_T2S *) mArmAdaptor)->backwardValid();
}
void CarbonAxiFT2S::backwardReady()
{
	((AXI_T2S *) mArmAdaptor)->backwardReady();
}

void CarbonAxiFT2S::sendDrive()
{
	((AXI_T2S *) mArmAdaptor)->sendDrive();
}
void CarbonAxiFT2S::sendRDrive()
{
	((AXI_T2S *) mArmAdaptor)->sendRDrive();
}
void CarbonAxiFT2S::sendBDrive()
{
	((AXI_T2S *) mArmAdaptor)->sendBDrive();
}

void CarbonAxiFT2S::sendNotify()
{
	((AXI_T2S *) mArmAdaptor)->sendNotify();
}
void CarbonAxiFT2S::sendARNotify()
{
	((AXI_T2S *) mArmAdaptor)->sendARNotify();
}
void CarbonAxiFT2S::sendAWNotify()
{
	((AXI_T2S *) mArmAdaptor)->sendAWNotify();
}
void CarbonAxiFT2S::sendWNotify()
{
	((AXI_T2S *) mArmAdaptor)->sendWNotify();
}

void CarbonAxiFT2S::init()
{
	((AXI_T2S *) mArmAdaptor)->init();
}

void CarbonAxiFT2S::update()
{
  ((AXI_T2S *) mArmAdaptor)->update();
}

void CarbonAxiFT2S::terminate()
{
	((AXI_T2S *) mArmAdaptor)->terminate();
}

void CarbonAxiFT2S::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
	((AXI_T2S *) mArmAdaptor)->reset(level, filelist);
	doneDrive = doneNotify = false;

	awReadyChanged = false;
	arReadyChanged = false;
	wReadyChanged = false;
	rChanged = false;
	bChanged = false;
}

void CarbonAxiFT2S::setParameter(const string &name, const string &value)
{
	((AXI_T2S *) mArmAdaptor)->setParameter(name, value);
}

//! Number of slave memory regions
int CarbonAxiFT2S::getNumRegions()
{
  return ((AXI_T2S *) mArmAdaptor)->getNumSlaveRegions();
}

//! Slave Address regions
void CarbonAxiFT2S::getAddressRegions(uint64_t* start, uint64_t* size, string* name)
{
  ((AXI_T2S *) mArmAdaptor)->getSlaveAddressRegions(start, size, name);
}

void CarbonAxiFT2S::carbonSetPortWidth(const char* name, uint32_t bitWidth)
{
  ((AXI_T2S *) mArmAdaptor)->setPortWidth(name, bitWidth);
}

bool CarbonAxiFT2S::saveData(eslapi::CASIODataStream& os)
{
  return ((AXI_T2S *) mArmAdaptor)->saveData(os);
}

bool CarbonAxiFT2S::restoreData(eslapi::CASIIDataStream& is)
{
  return ((AXI_T2S *) mArmAdaptor)->restoreData(is);
}

