
#include "CarbonAxiFS2T.h"

#define CARBON 1
#include "MxAXIPort.h"
#include "AXI_S2T.h"
#include "axi_TM.h"
#include "ss_SS.h"

#include "AXIFXtorEvents.h"

CarbonAxiFS2T::CarbonAxiFS2T(sc_mx_module* carbonComp,
                                     const char *xtorName,
                                     CarbonObjectID **carbonObj,
                                     CarbonPortFactory *portFactory,
                                     CarbonCombFunction *combFunc)
{
	AXI_S2T *temp = new AXI_S2T(carbonComp, xtorName, "2.1.002", carbonObj, portFactory, combFunc); 
	mArmAdaptor = temp;
	doneDrive = doneNotify = false;

	awChanged = false;
	arChanged = false;
	wChanged  = false;
	bReadyChanged = false;
	rReadyChanged = false;

	arChangedFunc     = new DriveAREvent(temp);
    awChangedFunc     = new DriveAWEvent(temp);
	wChangedFunc      = new DriveWEvent (temp);
	rReadyChangedFunc = new NotifyREvent(temp);
	bReadyChangedFunc = new NotifyBEvent(temp);
}

sc_mx_signal_slave *CarbonAxiFS2T::carbonGetPort(const char *name)
{
#define FIND_PORT(CHANNEL) \
	if (strcmp(#CHANNEL, name) == 0) \
		return ((AXI_S2T *) mArmAdaptor)->CHANNEL ## _SSlave

	FIND_PORT(awvalid);
	FIND_PORT(awuser );
	FIND_PORT(awid   );
	FIND_PORT(awaddr );
	FIND_PORT(awlen  );
	FIND_PORT(awsize );
	FIND_PORT(awburst);
	FIND_PORT(awlock );
	FIND_PORT(awcache);
	FIND_PORT(awprot );

	FIND_PORT(arvalid);
	FIND_PORT(aruser );
	FIND_PORT(arid   );
	FIND_PORT(araddr );
	FIND_PORT(arlen  );
	FIND_PORT(arsize );
	FIND_PORT(arburst);
	FIND_PORT(arlock );
	FIND_PORT(arcache);
	FIND_PORT(arprot );

	FIND_PORT(wvalid);
	FIND_PORT(wuser );
	FIND_PORT(wid   );  
	FIND_PORT(wstrb );
	FIND_PORT(wlast );
	FIND_PORT(wdata );

	FIND_PORT(rready);

	FIND_PORT(bready);

    ((AXI_S2T *) mArmAdaptor)->message(MX_MSG_FATAL_ERROR, "Carbon AXI S2T has no port named \"%s\"", name);
  
  return NULL;
}

CarbonAxiFS2T::~CarbonAxiFS2T()
{
	delete ((AXI_S2T *) mArmAdaptor);
	
	delete awChangedFunc;
	delete arChangedFunc;
	delete wChangedFunc;
	delete rReadyChangedFunc; 
	delete bReadyChangedFunc;
}

void CarbonAxiFS2T::forwardValid()
{
  ((AXI_S2T *) mArmAdaptor)->forwardValid();
}
void CarbonAxiFS2T::forwardReady()
{
  ((AXI_S2T *) mArmAdaptor)->forwardReady();
}

void CarbonAxiFS2T::sendDrive()
{
  ((AXI_S2T *) mArmAdaptor)->sendDrive();
}
void CarbonAxiFS2T::sendARDrive()
{
  ((AXI_S2T *) mArmAdaptor)->sendARDrive();
}
void CarbonAxiFS2T::sendAWDrive()
{
  ((AXI_S2T *) mArmAdaptor)->sendAWDrive();
}
void CarbonAxiFS2T::sendWDrive()
{
  ((AXI_S2T *) mArmAdaptor)->sendWDrive();
}

void CarbonAxiFS2T::sendNotify()
{
  ((AXI_S2T *) mArmAdaptor)->sendNotify();
}
void CarbonAxiFS2T::sendRNotify()
{
  ((AXI_S2T *) mArmAdaptor)->sendRNotify();
}
void CarbonAxiFS2T::sendBNotify()
{
  ((AXI_S2T *) mArmAdaptor)->sendBNotify();
}

void CarbonAxiFS2T::update()
{
  ((AXI_S2T *) mArmAdaptor)->update();
}

void CarbonAxiFS2T::init()
{
    ((AXI_S2T *) mArmAdaptor)->init();
}

void CarbonAxiFS2T::terminate()
{
  ((AXI_S2T *) mArmAdaptor)->terminate();
}

void CarbonAxiFS2T::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  ((AXI_S2T *) mArmAdaptor)->reset(level, filelist);
	doneDrive = doneNotify = false;

	awChanged = false;
	arChanged = false;
	wChanged  = false;
	bReadyChanged = false;
	rReadyChanged = false;
}

int CarbonAxiFS2T::getNumRegions()
{
  return ((AXI_S2T *) mArmAdaptor)->getNumRegions();
}
void CarbonAxiFS2T::getAddressRegions(uint64_t* start, uint64_t* size, std::string* name)
{
	((AXI_S2T *) mArmAdaptor)->getAddressRegions(start, size, name);
}


void CarbonAxiFS2T::setParameter(const string &name, const string &value)
{
  ((AXI_S2T *) mArmAdaptor)->setParameter(name, value);
}

MxStatus CarbonAxiFS2T::readDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
//  return ((AXI_S2T *) mArmAdaptor)->readDbg(addr, value, ctrl);
return eslapi::CASI_STATUS_OK;
}

MxStatus CarbonAxiFS2T::writeDbg(MxU64 addr, MxU32* value, MxU32* ctrl)
{
// return ((AXI_S2T *) mArmAdaptor)->writeDbg(addr, value, ctrl);
return eslapi::CASI_STATUS_OK;
}

CASIPortIF* CarbonAxiFS2T::getTmPort()
{
  return ((AXI_S2T *) mArmAdaptor)->axi_tm;
}

MxStatus CarbonAxiFS2T::debugTransaction(MxTransactionInfo* info)
{
  AXITransaction* axi = static_cast<AXITransaction*>(info); // this code counts on this begin true
  MxStatus status = MX_STATUS_ERROR;

  // check to see if resizing is needed
  uint32_t requestSize = 8*(axi->dataSize);
  axi_TM*  axi_tm = ((AXI_S2T *) mArmAdaptor)->axi_tm;
  if (! axi_tm->isConnected() ){
    return MX_STATUS_ERROR;
  }

  uint32_t destSize = ((AXI_S2T *) mArmAdaptor)->p_Data_Width;
  if ( requestSize == destSize ) {
    status = axi_tm->debugTransaction(axi);    // no size change needed, just pass it through
  } else {
    // resizing needed

    AXITransaction tempInfo;    // a temp block we will pass on
    // don't copy the AXIV2 properties from axi, instead we create a v1 version
    eslapi::CASITransactionProperties v1Props;
    AXI_INIT_TRANSACTION_PROPERTIES_BASE(v1Props, destSize); // using AXI_INIT_TRANSACTION_PROPERTIES_BASE to avoid the new/delete hidden in AXI_INIT_TRANSACTION_PROPERTIES
    tempInfo.initialize(&v1Props);
    tempInfo.init(destSize);
    tempInfo.access    = axi->access;
    tempInfo.addr      = axi->addr;
    tempInfo.dataBeats = 1;

    assert(axi->dataBeats == 1);

    if (requestSize > destSize ){
      // need to downsize
      MxStatus    status0, status1 = MX_STATUS_OK;
      MxU32       tmpData[2];

      if (axi->access == MX_ACCESS_READ)
      {
        if (axi->dataSize <= 4)
        {
          tempInfo.dataSize = axi->dataSize;
          status0 = axi_tm->debugTransaction(&tempInfo);
          tempInfo.alignRdDataHw2Sw(0, tmpData);
        }
        else
        {
          tempInfo.dataSize = 4;
          status0 = axi_tm->debugTransaction(&tempInfo);
          tempInfo.alignRdDataHw2Sw(0, &(tmpData[0]));
          tempInfo.addr+= 4;
          status1 = axi_tm->debugTransaction(&tempInfo);
          tempInfo.alignRdDataHw2Sw(0, &(tmpData[1]));
        }
        axi->alignRdDataSw2Hw(0, tmpData);
      }
      else
      {
        MxU32 strb = 0;
        axi->alignWrDataAndStrobeHw2Sw(0, tmpData, strb);
        if (axi->dataSize <= 4)
        {
          tempInfo.dataSize = axi->dataSize;
          tempInfo.alignWrDataAndStrobeSw2Hw(0, tmpData, strb);
          status0 = axi_tm->debugTransaction(&tempInfo);
        }
        else
        {
          tempInfo.dataSize = 4;
          tempInfo.alignWrDataAndStrobeSw2Hw(0, &(tmpData[0]), strb);
          status0 = axi_tm->debugTransaction(&tempInfo);
          tempInfo.addr+= 4;
          tempInfo.alignWrDataAndStrobeSw2Hw(0, &(tmpData[1]), strb);
          status1 = axi_tm->debugTransaction(&tempInfo);
        }
      }
      status = ((status0 == MX_STATUS_OK) ? status1 : status0);
    } else {
      // need to upsize/expansion
      MxU32       tmpData[4];

      tempInfo.dataSize  = axi->dataSize;

      if (axi->access == MX_ACCESS_READ)
      {
        status = axi_tm->debugTransaction(&tempInfo);
        tempInfo.alignRdDataHw2Sw(0, tmpData);
        axi->alignRdDataSw2Hw(0, tmpData);
      }
      else
      {
        MxU32 strb = 0;
        axi->alignWrDataAndStrobeHw2Sw(0, tmpData, strb);
        tempInfo.alignWrDataAndStrobeSw2Hw(0, tmpData, strb);
        status = axi_tm->debugTransaction(&tempInfo);
      }
    }
  }

  return status;
}

MxStatus CarbonAxiFS2T::debugMemWrite(MxU64 startAddress, MxU32 unitsToWrite, MxU32 unitSizeInBytes, const MxU8 *data, MxU32 *actualNumOfUnitsWritten, MxU32 secureFlag )
{
  // Assume that we write all the units requested
  *actualNumOfUnitsWritten = unitsToWrite;

  AXITransaction trans;
  MxU32 masterFlags[AXI_STEP_LAST];
  trans.masterFlags = masterFlags;
  trans.initialize(((AXI_S2T *) mArmAdaptor)->axi_tm->getProperties());
  trans.setInitiator(((AXI_S2T *) mArmAdaptor)->axi_tm);
  trans.init(((AXI_S2T *) mArmAdaptor)->p_Data_Width);
  trans.clear();
  trans.nts = AXI_STEP_LAST;
  trans.reset();
  trans.access = MX_ACCESS_WRITE;
  trans.masterFlags[AXI_MF_CHANNEL] = AXI_CHANNEL_AW;
  if (secureFlag)
    trans.set_prot(trans.get_prot() | AXI_PROTECTION_SEC);
  else
    trans.set_prot(trans.get_prot() & ~AXI_PROTECTION_SEC);
  trans.set_id(0);
  trans.set_len(0); // remember: AXI length starts at 0, 1 less than the units to write
  trans.set_size(0); // remember: the number of bytes is 2**(AXI size)
                     // so a zero arg here means 1 byte, a 2 here means 4 bytes, 


  // now split up the accesses into single byte transactions to
  // simplify the setting up of data
  MxStatus status = MX_STATUS_OK;
  MxU32 strb = 0;
  MxU32 tmpData[4] = {0};
  
  for (MxU32 curByte = 0; curByte < unitSizeInBytes; curByte++){
    MxU64 curAddress = startAddress+curByte;
    trans.set_addr(curAddress);
    tmpData[0] = data[curByte];
    trans.alignWrDataAndStrobeSw2Hw(0, tmpData, strb);
    MxStatus tmp_status = debugTransaction(&trans);
    if ( ( status == MX_STATUS_OK ) && ( tmp_status != MX_STATUS_OK )){
      status = tmp_status;
      break;
    }
  }
  return status;
}

MxStatus CarbonAxiFS2T::debugMemRead(MxU64 startAddress, MxU32 unitsToRead, MxU32 unitSizeInBytes, MxU8 *data, MxU32 *actualNumOfUnitsRead, MxU32 secureFlag)
{
  // Assume that we read all the units requested
  *actualNumOfUnitsRead = unitsToRead;

  AXITransaction trans;
  MxU32 masterFlags[AXI_STEP_LAST];
  trans.masterFlags = masterFlags;
  trans.initialize(((AXI_S2T *) mArmAdaptor)->axi_tm->getProperties());
  trans.setInitiator(((AXI_S2T *) mArmAdaptor)->axi_tm);
  trans.init(((AXI_S2T *) mArmAdaptor)->p_Data_Width);
  trans.clear();
  trans.nts = AXI_STEP_LAST;
  trans.reset();
  trans.access = MX_ACCESS_READ;
  trans.masterFlags[AXI_MF_CHANNEL] = AXI_CHANNEL_AW;
  if (secureFlag)
    trans.set_prot(trans.get_prot() | AXI_PROTECTION_SEC);
  else
    trans.set_prot(trans.get_prot() & ~AXI_PROTECTION_SEC);
  trans.set_id(0);
  trans.set_len(0); // remember: AXI length starts at 0, 1 less than the units to read
  trans.set_size(0); // remember: the number of bytes is 2**(AXI size)
                     // so a zero arg here means 1 byte, a 2 here means 4 bytes, 
                     // we are going to do a series of single byte
                     // transactions below so we set the size to zero here
  MxU32* ptr = trans.getP_getReadData(0);

  // now split up the access into single byte accesses to simplify the
  // packing of results into data
  for (MxU32 curByte = 0; curByte < unitSizeInBytes; curByte++){
    MxU64 curAddress = startAddress+curByte;
    trans.set_addr(curAddress);
    memset(trans.dataRd, 0, 8);

    debugTransaction(&trans);

    MxU32 offset = curAddress % ((trans.getDataBitwidth()+7)/8);
    MxU32 word = offset/4;
    MxU32 byte = offset%4;
    data[curByte] = ptr[word] >> (byte*8);
  }
  return MX_STATUS_OK;
}

CarbonDebugAccessStatus CarbonAxiFS2T::debugAccess(CarbonDebugAccessDirection dir, MxU64 startAddress, MxU32 numBytes, MxU8 *data, MxU32 *numBytesProcessed, MxU32 *ctrl)
{
  MxStatus status;  
  MxU32 secureFlag = 0;
  if (ctrl) {
    secureFlag = ctrl[0]&0x1;
  }
  MxU32 numUnitsProcessed = 0;
  if ( dir == eCarbonDebugAccessRead ){
    status = debugMemRead( startAddress, 1, numBytes,data, &numUnitsProcessed, secureFlag);
  } else {
    status = debugMemWrite( startAddress, 1, numBytes,data, &numUnitsProcessed, secureFlag);
  }
  *numBytesProcessed = numUnitsProcessed * numBytes;
  if (status == MX_STATUS_OK)
    return eCarbonDebugAccessStatusOk;
  else
    return eCarbonDebugAccessStatusError;
}

void CarbonAxiFS2T::carbonSetPortWidth(const char* name, uint32_t bitWidth)
{
  ((AXI_S2T *) mArmAdaptor)->setPortWidth(name, bitWidth);
}

bool CarbonAxiFS2T::saveData(eslapi::CASIODataStream& os)
{
  return ((AXI_S2T *) mArmAdaptor)->saveData(os);
}

bool CarbonAxiFS2T::restoreData(eslapi::CASIIDataStream& is)
{
  return ((AXI_S2T *) mArmAdaptor)->restoreData(is);
}

