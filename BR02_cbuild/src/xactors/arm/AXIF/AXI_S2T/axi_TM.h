/*
 *    Class definition of axi_tm port for component <AXI_S2T>
 *
 *    This code has been generated by the SoC Designer Component Wizard.
 *    Copyright (c) 2004-2007 by ARM Limited, 2000-2004 by AXYS Design Automation Inc., Irvine, CA, USA and AXYS GmbH, Herzongenrath
 *
 */

#ifndef _axi_TM_H_
#define _axi_TM_H_

#include "maxsimCompatibility.h"
#include "AXI_Master_Port.h"

class AXI_S2T;
//class Axi_Master_Port;

class axi_TM : public AXI_Master_Port
{
private:
	AXI_S2T* owner;

public:
	axi_TM(CASIModule* owner, AXI_S2T* xtor, std::string name);
	virtual ~axi_TM() {}

	void reset();

	virtual void driveTransactionCB_R();
	virtual void driveTransactionCB_B();
	virtual void notifyEventCB_AR();
	virtual void notifyEventCB_AW();
	virtual void notifyEventCB_W();

	uint32_t lastARReady;
	uint32_t lastAWReady;
	uint32_t lastWReady;
};

#endif

