/*
 *    Class definition of ss port for component <signalslave>
 *
 *    This code has been generated by the SoC Designer Component Wizard.
 *    Copyright (c) 2004-2007 by ARM Limited, 2000-2004 by AXYS Design Automation Inc., Irvine, CA, USA and AXYS GmbH, Herzongenrath
 *
 */

#ifndef _ss_SSReady_H_
#define _ss_SSReady_H_

#include "maxsimCompatibility.h"
#include "AXI_TLM.h"
#include "ss_SS.h"

class AXI_Receiver_PortBase;

class ss_SSReady: public ss_SS
{
public:
    ss_SSReady( CASIModule *_owner, uint32_t width, AXI_Receiver_PortBase* port, bool &gChange);
    virtual ~ss_SSReady() {}

public:
    /* Access functions */
    virtual void driveSignal(uint32_t value, uint32_t* extValue);

private:
	AXI_Receiver_PortBase* connectedPortReady;
};

#endif

