#include "AXIFXtorEvents.h"

#include "AXI_S2T.h"
#include "AXI_T2S.h"

#define EVENTCLASSIMP(Channel, XtorType, EventType) \
EventType ## Channel ## Event::EventType ## Channel ## Event(XtorType* obj) : \
AXIXtorEvent<XtorType> (obj) \
{} \
EventType ## Channel ## Event::~EventType ## Channel ## Event() {} \
void EventType ## Channel ## Event::operator()() \
{ \
	xtor->send ## Channel ## EventType(); \
} \

EVENTCLASSIMP(AR, AXI_S2T, Drive)
EVENTCLASSIMP(AW, AXI_S2T, Drive)
EVENTCLASSIMP(W,  AXI_S2T, Drive)
EVENTCLASSIMP(R,  AXI_S2T, Notify)
EVENTCLASSIMP(B,  AXI_S2T, Notify)
				  
EVENTCLASSIMP(AR, AXI_T2S, Notify)
EVENTCLASSIMP(AW, AXI_T2S, Notify)
EVENTCLASSIMP(W,  AXI_T2S, Notify)
EVENTCLASSIMP(R,  AXI_T2S, Drive)
EVENTCLASSIMP(B,  AXI_T2S, Drive)

