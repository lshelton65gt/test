//
// Copyright (c)  2008 Carbon Design Systems Inc., All rights reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE
//
#ifndef AXI_TLM_H
#define AXI_TLM_H

#include "maxsimCompatibility.h"

// axi channel type
enum AXI_CHANNEL
{
	AXI_AR = 0,
	AXI_AW,
	AXI_R,
	AXI_W,
	AXI_B
};
typedef enum AXI_CHANNEL AXI_CHANNEL;

// response signal consts
enum AXI_RESP
{
	RESP_OKAY = 0, // 0x0
	RESP_EXOKAY,   // 0x1
	RESP_SLVERR,   // 0x2
	RESP_DECERR    // 0x3
};
typedef enum AXI_RESP AXI_RESP;

// size signal consts
enum AXI_SIZE
{
	SIZE_BYTE1 = 0 , // 0x0
	SIZE_BYTE2     , // 0x1
	SIZE_BYTE4     , // 0x2
	SIZE_BYTE8     , // 0x3
	SIZE_BYTE16    , // 0x4 
	SIZE_BYTE32    , // 0x5
	SIZE_BYTE64    , // 0x6
	SIZE_BYTE128     // 0x7
};
typedef enum AXI_SIZE AXI_SIZE;

// burst signal consts
enum AXI_BURST
{
	BURST_FIXED = 0, // 0x0
	BURST_INCR     , // 0x1
	BURST_WRAP     , // 0x2
	BURST_RESERVED   // 0x3
};
typedef enum AXI_BURST AXI_BURST;

enum AXI_SIGNAL_IDX
{
	// AW signals
	AW_READY = 0,
	AW_VALID,
	AW_USER ,
	AW_ID   ,
	AW_ADDR ,
	AW_LEN  ,
	AW_SIZE ,
	AW_BURST,
	AW_LOCK ,
	AW_CACHE,
	AW_PROT ,

	// AR signals
	AR_READY,
	AR_VALID,
	AR_USER ,
	AR_ID   ,
	AR_ADDR ,
	AR_LEN  ,
	AR_SIZE ,
	AR_BURST,
	AR_LOCK ,
	AR_CACHE,
	AR_PROT ,

	// R signals
	R_READY,
	R_VALID,
	R_USER ,
	R_ID   ,
	R_RESP ,
	R_LAST ,

	// W signals
	W_READY ,
	W_VALID ,
	W_USER  ,
	W_ID    ,
	W_STRB  ,
	W_LAST  ,

	// B signals
	B_READY,
	B_VALID,
	B_USER ,
	B_ID   ,
	B_RESP ,

	R_DATA ,
	R_DATA1,
	R_DATA2,
	R_DATA3,
	R_DATA4,
	R_DATA5,
	R_DATA6,
	R_DATA7,
	R_DATA8,
	R_DATA9,
	R_DATA10,
	R_DATA11,
	R_DATA12,
	R_DATA13,
	R_DATA14,
	R_DATA15,
	R_DATA16,
	R_DATA17,
	R_DATA18,
	R_DATA19,
	R_DATA20,
	R_DATA21,
	R_DATA22,
	R_DATA23,
	R_DATA24,
	R_DATA25,
	R_DATA26,
	R_DATA27,
	R_DATA28,
	R_DATA29,
	R_DATA30,
	R_DATA31,

	W_DATA  ,
	W_DATA1 ,
	W_DATA2 ,
	W_DATA3 ,
	W_DATA4 ,
	W_DATA5 ,
	W_DATA6 ,
	W_DATA7 ,
	W_DATA8 ,
	W_DATA9 ,
	W_DATA10,
	W_DATA11,
	W_DATA12,
	W_DATA13,
	W_DATA14,
	W_DATA15,
	W_DATA16,
	W_DATA17,
	W_DATA18,
	W_DATA19,
	W_DATA20,
	W_DATA21,
	W_DATA22,
	W_DATA23,
	W_DATA24,
	W_DATA25,
	W_DATA26,
	W_DATA27,
	W_DATA28,
	W_DATA29,
	W_DATA30,
	W_DATA31,

	NUM_AXI_SIGNALS	
};
typedef enum AXI_SIGNAL_IDX AXI_SIGNAL_IDX;
inline void operator++(AXI_SIGNAL_IDX& eVal)
{
    eVal = AXI_SIGNAL_IDX(eVal+1);
}

inline void operator--(AXI_SIGNAL_IDX& eVal)
{
    eVal = AXI_SIGNAL_IDX(eVal-1);
}

struct Signal
{
	string name;
	string shortname;
	uint8_t maxBitWidth;
	bool masterCanWrite;
};

const Signal axiChannelSignals[] =
{
	// AW signals
	{ "AW_READY", "awready", 1 , false },
	{ "AW_VALID", "awvalid", 1 , true  },
	{ "AW_USER" , "awuser" , 32, true  },
	{ "AW_ID"   , "awid"   , 32, true  },
	{ "AW_ADDR" , "awaddr" , 64, true  },
	{ "AW_LEN"  , "awlen"  , 4 , true  },
	{ "AW_SIZE" , "awsize" , 3 , true  },
	{ "AW_BURST", "awburst", 2 , true  },
	{ "AW_LOCK" , "awlock" , 2 , true  },
	{ "AW_CACHE", "awcache", 4 , true  },
	{ "AW_PROT" , "awprot" , 3 , true  },
				  
	// AR signals  
	{ "AR_READY", "arready", 1 , false },
	{ "AR_VALID", "arvalid", 1 , true  },
	{ "AR_USER" , "aruser" , 32, true  },
	{ "AR_ID"   , "arid"   , 32, true  },
	{ "AR_ADDR" , "araddr" , 64, true  },
	{ "AR_LEN"  , "arlen"  , 4 , true  },
	{ "AR_SIZE" , "arsize" , 3 , true  },
	{ "AR_BURST", "arburst", 2 , true  },
	{ "AR_LOCK" , "arlock" , 2 , true  },
	{ "AR_CACHE", "arcache", 4 , true  },
	{ "AR_PROT" , "arprot" , 3 , true  },
				  
	// R signals   
	{ "R_READY" , "rready" , 1 , true  },
	{ "R_VALID" , "rvalid" , 1 , false },
	{ "R_USER"  , "ruser"  , 32, true  },
	{ "R_ID"    , "rid"    , 32, false },
	{ "R_RESP"  , "rresp"  , 2 , false },
	{ "R_LAST"  , "rlast"  , 1 , false },
				  
	// W signals   
	{ "W_READY" , "wready" , 1 , false },
	{ "W_VALID" , "wvalid" , 1 , true  },
	{ "W_USER"  , "wuser"  , 32, true  },
	{ "W_ID"    , "wid"    , 32, true  },
	{ "W_STRB"  , "wstrb"  , 32, true  },
	{ "W_LAST"  , "wlast"  , 1 , true  },
				  
	// B signals   
	{ "B_READY" , "bready" , 1 , true  },
	{ "B_VALID" , "bvalid" , 1 , false },
	{ "B_USER"  , "buser"  , 32, true  },
	{ "B_ID"    , "bid"    , 32, false },
	{ "B_RESP"  , "bresp"  , 2 , false },

	{ "R_DATA"  , "rdata"  , 32, false },
	{ "R_DATA1" , "rdata1" , 32, false },
	{ "R_DATA2" , "rdata2" , 32, false },
	{ "R_DATA3" , "rdata3" , 32, false },
	{ "R_DATA4" , "rdata4" , 32, false },
	{ "R_DATA5" , "rdata5" , 32, false },
	{ "R_DATA6" , "rdata6" , 32, false },
	{ "R_DATA7" , "rdata7" , 32, false },
	{ "R_DATA8" , "rdata8" , 32, false },
	{ "R_DATA9" , "rdata9" , 32, false },
	{ "R_DATA10", "rdata10", 32, false },
	{ "R_DATA11", "rdata11", 32, false },
	{ "R_DATA12", "rdata12", 32, false },
	{ "R_DATA13", "rdata13", 32, false },
	{ "R_DATA14", "rdata14", 32, false },
	{ "R_DATA15", "rdata15", 32, false },
	{ "R_DATA16", "rdata16", 32, false },
	{ "R_DATA17", "rdata17", 32, false },
	{ "R_DATA18", "rdata18", 32, false },
	{ "R_DATA19", "rdata19", 32, false },
	{ "R_DATA20", "rdata20", 32, false },
	{ "R_DATA21", "rdata21", 32, false },
	{ "R_DATA22", "rdata22", 32, false },
	{ "R_DATA23", "rdata23", 32, false },
	{ "R_DATA24", "rdata24", 32, false },
	{ "R_DATA25", "rdata25", 32, false },
	{ "R_DATA26", "rdata26", 32, false },
	{ "R_DATA27", "rdata27", 32, false },
	{ "R_DATA28", "rdata28", 32, false },
	{ "R_DATA29", "rdata29", 32, false },
	{ "R_DATA30", "rdata30", 32, false },
	{ "R_DATA31", "rdata31", 32, false },

	{ "W_DATA"  , "wdata"  , 32, true  },
	{ "W_DATA1" , "wdata1" , 32, true  },
	{ "W_DATA2" , "wdata2" , 32, true  },
	{ "W_DATA3" , "wdata3" , 32, true  },
	{ "W_DATA4" , "wdata4" , 32, true  },
	{ "W_DATA5" , "wdata5" , 32, true  },
	{ "W_DATA6" , "wdata6" , 32, true  },
	{ "W_DATA7" , "wdata7" , 32, true  },
	{ "W_DATA8" , "wdata8" , 32, true  },
	{ "W_DATA9" , "wdata9" , 32, true  },
	{ "W_DATA10", "wdata10", 32, true  },
	{ "W_DATA11", "wdata11", 32, true  },
	{ "W_DATA12", "wdata12", 32, true  },
	{ "W_DATA13", "wdata13", 32, true  },
	{ "W_DATA14", "wdata14", 32, true  },
	{ "W_DATA15", "wdata15", 32, true  },
	{ "W_DATA16", "wdata16", 32, true  },
	{ "W_DATA17", "wdata17", 32, true  },
	{ "W_DATA18", "wdata18", 32, true  },
	{ "W_DATA19", "wdata19", 32, true  },
	{ "W_DATA20", "wdata20", 32, true  },
	{ "W_DATA21", "wdata21", 32, true  },
	{ "W_DATA22", "wdata22", 32, true  },
	{ "W_DATA23", "wdata23", 32, true  },
	{ "W_DATA24", "wdata24", 32, true  },
	{ "W_DATA25", "wdata25", 32, true  },
	{ "W_DATA26", "wdata26", 32, true  },
	{ "W_DATA27", "wdata27", 32, true  },
	{ "W_DATA28", "wdata28", 32, true  },
	{ "W_DATA29", "wdata29", 32, true  },
	{ "W_DATA30", "wdata30", 32, true  },
	{ "W_DATA31", "wdata31", 32, true  }
};

#define AXI2_PROTOCOL_ID 0x0002A3E1
#ifndef MXSI_VERSION
#define MXSI_VERSION eslapi::CASI_VERSION_1_1
#endif
#define PROPERTY_EXTENSION(PROP__) sprintf((PROP__).protocolName, "AXI"); \
    (PROP__).isAddrRegionForwarded = false; \
    (PROP__).forwardAddrRegionToMasterPort = NULL; \
    (PROP__).details                = NULL;         /* not used but initialized */

#define AXI2_INIT_TRANSACTION_PROPERTIES(PROP__, ADDR_BITWIDTH__, DATA_BITWIDTH__)  \
    (PROP__).casiVersion            = MXSI_VERSION;     /* using the MXSI 3.1 */ \
    (PROP__).useMultiCycleInterface = true;             /* use driveX */ \
    (PROP__).addressBitwidth        = (ADDR_BITWIDTH__);/* AXI Address Bus Bitwidth */ \
    (PROP__).mauBitwidth            = 8;                /* AXI Bus is Byte Addressable */ \
    (PROP__).dataBitwidth           = (DATA_BITWIDTH__);/* AXI Data Bus Bitwidth  */ \
    (PROP__).dataBeats              = 16;               /* AXI Max Number of Data Beats */ \
    (PROP__).isLittleEndian         = true;             /* AXI Byte Ordering */ \
    (PROP__).isOptional             = false;            /* AXI Component Specific */ \
    (PROP__).supportsAddressRegions = true;             /* AXI Bus supports Address Regions */ \
    (PROP__).numCtrlFields          = 0;                /* not used but initialized */ \
    (PROP__).protocolID             = AXI2_PROTOCOL_ID;  /* specific ID for this protocol */ \
    (PROP__).supportsNotify         = true;             /* needed for fast response forwarding */ \
    (PROP__).supportsBurst          = true;             /* burst transactions are supported */ \
    (PROP__).supportsSplit          = false;            /* split transactions are not defined */ \
    PROPERTY_EXTENSION(PROP__)

#endif
