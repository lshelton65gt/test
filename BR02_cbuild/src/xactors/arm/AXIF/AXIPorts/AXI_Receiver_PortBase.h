//
// Copyright (c)  2008 Carbon Design Systems Inc., All rights reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE
//
#ifndef _AXI_RECEIVER_PORTBASE_H_
#define _AXI_RECEIVER_PORTBASE_H_

#include "AXI_Channel_Port.h"
#include "AXITransactionInfo.h"
#include "AXIPortIF.h"

class AXI_Receiver_PortBase: public CASITransactionSlave, public AXI_Channel_Port
{
public:
    CASIModule *owner;

    //Memory Map Editor Support
    CASIMemoryMapConstraints puMemoryMapConstraints;

public:
    AXI_Receiver_PortBase( AXIPortIF* p, CASIModule *_owner, string n, AXI_CHANNEL chan );
    virtual ~AXI_Receiver_PortBase() {}

	virtual void driveTransaction(CASITransactionInfo* info) = 0;

	void reset();

        bool saveData(eslapi::CASIODataStream& os);
        bool restoreData(eslapi::CASIIDataStream& is);

	bool nextReady;

	AXITransactionInfo dummyCrtAxiInfo;

	//**************************************************************************
	//******************* IGNORE EVERYTHING BELOW THIS LINE ********************
	//**************************************************************************
public:
    /* Synchronous access functions */
    virtual eslapi::CASIStatus read(uint64_t addr, uint32_t* value, uint32_t* ctrl);
    virtual eslapi::CASIStatus write(uint64_t addr, uint32_t* value, uint32_t* ctrl);
    virtual eslapi::CASIStatus readDbg(uint64_t addr, uint32_t* value, uint32_t* ctrl);
    virtual eslapi::CASIStatus writeDbg(uint64_t addr, uint32_t* value, uint32_t* ctrl);

    /* Asynchronous access functions */
    virtual eslapi::CASIStatus readReq(uint64_t addr, uint32_t* value, uint32_t* ctrl,
                             CASITransactionCallbackIF* callback);
    virtual eslapi::CASIStatus writeReq(uint64_t addr, uint32_t* value, uint32_t* ctrl,
                              CASITransactionCallbackIF* callback);

    /* Arbitration functions */
    virtual eslapi::CASIGrant requestAccess(uint64_t addr);
    virtual eslapi::CASIGrant checkForGrant(uint64_t addr);
    
	/* CASI : new shared-memory based asynchronous transaction functions */
	virtual void cancelTransaction(CASITransactionInfo* info);
	virtual eslapi::CASIStatus debugTransaction(CASITransactionInfo* info);

    /* Memory map functions */
    virtual int getNumRegions();
    virtual void getAddressRegions(uint64_t* start, uint64_t* size, string* name);

    /* CASI : now the address regions can be set from the outside! */
    virtual void setAddressRegions(uint64_t* start, uint64_t* size, string* name);
    virtual CASIMemoryMapConstraints* getMappingConstraints();
};

#endif

