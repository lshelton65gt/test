#include "AXI_Channel_Port.h"

#include "AXI_TLM.h"
#include "AXIPortIF.h"

AXI_Channel_Port::AXI_Channel_Port(AXIPortIF* p, AXI_CHANNEL chan) :
	topPort(p), channel(chan)
{
}

AXI_Channel_Port::~AXI_Channel_Port()
{
}

