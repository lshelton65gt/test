//
// Copyright (c)  2008 Carbon Design Systems Inc., All rights reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE
//
#ifndef _AXI_SENDER_PORT_H_
#define _AXI_SENDER_PORT_H_

#include "AXI_Sender_PortBase.h"

#if 0 // goes into SD 7.2.SP2 since we can't force recompile
template <typename T>
class AXI_Sender_Port : public AXI_Sender_PortBase
{
public:
	AXI_Sender_Port(AXIPortIF* p, CASIModule* o, std::string n, AXI_CHANNEL chan) :
		AXI_Sender_PortBase(p,o,n,chan)
	{
		mNotifyHandler.setOwner(this);
		setNotifyHandler(&mNotifyHandler);
	}
	virtual ~AXI_Sender_Port() {}

private:
	class NotifyHandler : public MxNotifyHandlerIF
	{
	public:
		NotifyHandler() : mOwner(NULL) {}
		~NotifyHandler() {}

		void setOwner(AXI_Sender_PortBase* owner) { mOwner = owner; }

 		void notifyEvent (CASITransactionInfo* info)
		{
			T::notifyEvent(mOwner->topPort);
		}

	private:
		AXI_Sender_PortBase* mOwner;
	};
	friend class NotifyHandler;

	NotifyHandler mNotifyHandler;

};
#else
#define SENDER_PORT_DECL(CHANNEL) \
class CHANNEL ## _Sender_Port : public AXI_Sender_PortBase \
{ \
public: \
	CHANNEL ## _Sender_Port(AXIPortIF* p, CASIModule* o, std::string n, AXI_CHANNEL chan) : \
		AXI_Sender_PortBase(p,o,n,chan) \
	{ \
		mNotifyHandler.setOwner(this); \
		setNotifyHandler(&mNotifyHandler); \
	} \
	virtual ~CHANNEL ## _Sender_Port() {} \
\
private: \
	class NotifyHandler : public MxNotifyHandlerIF \
	{ \
	public: \
		NotifyHandler() : mOwner(NULL) {} \
		~NotifyHandler() {} \
\
		void setOwner(AXI_Sender_PortBase* owner) { mOwner = owner; } \
\
 		void notifyEvent (CASITransactionInfo* info) \
		{ \
			mOwner->topPort->notifyEventCB_ ## CHANNEL(); \
		} \
\
	private: \
		AXI_Sender_PortBase* mOwner; \
	}; \
	friend class NotifyHandler; \
\
	NotifyHandler mNotifyHandler; \
\
}

SENDER_PORT_DECL(AR);
SENDER_PORT_DECL(AW);
SENDER_PORT_DECL(W);
SENDER_PORT_DECL(R);
SENDER_PORT_DECL(B);
#endif
#endif

