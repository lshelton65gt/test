#include "AXI_Sender_PortBase.h"
#include "AXI_TLM.h"
#include "AXIPortIF.h"
#include "AXITransactionInfo.h"
#include <iostream>

using namespace std;

AXI_Sender_PortBase::AXI_Sender_PortBase(AXIPortIF* p, CASIModule* o, std::string n, AXI_CHANNEL chan) : 
sc_port<CASITransactionIF>(o,n),
AXI_Channel_Port(p, chan)
{
	owner = o;

	switch (chan)
	{
	case AXI_AR:
		startIdx = AR_VALID;
		endIdx   = AR_PROT;
		break;
	case AXI_AW:
		startIdx = AW_VALID;
		endIdx   = AW_PROT;
		break;
	case AXI_R:
		startIdx = R_VALID;
		endIdx   = R_LAST;
		break;
	case AXI_W:
		startIdx = W_VALID;
		endIdx   = W_LAST;
		break;
	case AXI_B:
		startIdx = B_VALID;
		endIdx   = B_RESP;
		break;
	}

	crtAxiInfo = new AXITransactionInfo;
	setConnected(false);
}

void AXI_Sender_PortBase::driveTransaction(CASITransactionInfo*)
{
	if (!isConnected())
		return;

	crtAxiInfo->numNewSignals = nextSignals.size();
	for (int i=0; i<crtAxiInfo->numNewSignals; ++i)
	{
		crtAxiInfo->signals[nextSignals[i].first] = nextSignals[i].second;
	}
    nextSignals.clear(); // avoid driving signals we already drove to improve speed
    sc_port<CASITransactionIF>::driveTransaction(crtAxiInfo);
}

void AXI_Sender_PortBase::reset()
{
	nextSignals.clear();
	memset(crtAxiInfo->signals,0,sizeof(uint32_t)*NUM_AXI_SIGNALS);
	crtAxiInfo->numNewSignals = 0;
	crtAxiInfo->isReadyNew = false;
}

void AXI_Sender_PortBase::connect(CASITransactionIF* iface)
{
	setConnected(true);
	sc_port<CASITransactionIF>::connect(iface);
}

bool AXI_Sender_PortBase::saveData(eslapi::CASIODataStream& os)
{
  bool ok = true;

  // Save the list of next signal values as well as the
  // AXITransactionInfo structure.
  uint32_t count = nextSignals.size();
  os << count;
  for (uint32_t i = 0; i < count; ++i) {
    uint32_t index = nextSignals[i].first;
    uint64_t value = nextSignals[i].second;
    os << index << value;
  }

  ok &= crtAxiInfo->saveData(os);

  return ok;
}

bool AXI_Sender_PortBase::restoreData(eslapi::CASIIDataStream& is)
{
  bool ok = true;

  // Clear any existing next signal values before restoring
  nextSignals.clear();
  uint32_t count;
  is >> count;
  while (count > 0) {
    uint32_t index;
    uint64_t value;
    is >> index >> value;
    nextSignals.push_back(std::pair<AXI_SIGNAL_IDX,uint64_t>(static_cast<AXI_SIGNAL_IDX>(index), value));
    --count;
  }

  ok &= crtAxiInfo->restoreData(is);

  return ok;
}
