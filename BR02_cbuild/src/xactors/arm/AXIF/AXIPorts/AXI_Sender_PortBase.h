//
// Copyright (c)  2008 Carbon Design Systems Inc., All rights reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE
//
#ifndef _AXI_SENDER_PORTBASE_H_
#define _AXI_SENDER_PORTBASE_H_

#include <vector>
#include "AXI_Channel_Port.h"
#include "AXIPortIF.h"

class AXI_Sender_PortBase : 
	public sc_port<CASITransactionIF>, 
	public AXI_Channel_Port
{
public:
	AXI_Sender_PortBase(AXIPortIF* p, CASIModule* o, std::string n, AXI_CHANNEL chan);

	void driveTransaction(CASITransactionInfo*);

	void reset();

	void connect(CASITransactionIF* iface);

        bool saveData(eslapi::CASIODataStream& os);
        bool restoreData(eslapi::CASIIDataStream& is);

	// represents the signals that will be driven out in the communicate phase
	std::vector<std::pair<AXI_SIGNAL_IDX,uint64_t> > nextSignals;

private:
	CASIModule* owner;
};

#endif

