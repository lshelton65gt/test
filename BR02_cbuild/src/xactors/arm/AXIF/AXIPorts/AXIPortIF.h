//
// Copyright (c)  2008 Carbon Design Systems Inc., All rights reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE
//
#ifndef _AXIPORTIF_H_
#define _AXIPORTIF_H_

#include "AXI_TLM.h"

/**
 * Base AXIv2 port interface 
 */
class AXIPortIF
{
public:
	AXIPortIF() {}
	virtual ~AXIPortIF() {}

	// these can be optionally overrided by 
	// the user's port classes
	virtual void driveTransactionCB_AR() {};
	virtual void driveTransactionCB_AW() {};
	virtual void driveTransactionCB_W () {};
	virtual void driveTransactionCB_R () {};
	virtual void driveTransactionCB_B () {};

	// these can be optionally overrided by 
	// the user's port classes
	virtual void notifyEventCB_AR() {};
	virtual void notifyEventCB_AW() {};
	virtual void notifyEventCB_W () {};
	virtual void notifyEventCB_R () {};
	virtual void notifyEventCB_B () {};

	// these need to be implemented by the base port classes
	virtual bool     setSig(AXI_SIGNAL_IDX sigIdx, uint64_t val) = 0;
	virtual uint64_t getSig(AXI_SIGNAL_IDX sigIdx) = 0;
	virtual void     clear() = 0;
};



#endif

