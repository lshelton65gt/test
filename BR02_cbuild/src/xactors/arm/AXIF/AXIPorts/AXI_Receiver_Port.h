//
// Copyright (c)  2008 Carbon Design Systems Inc., All rights reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE
//
#ifndef _AXI_RECEIVER_PORT_H_
#define _AXI_RECEIVER_PORT_H_

#include "AXI_Receiver_PortBase.h"

#if 0 // goes into SD 7.2.SP2 since we can't force recompile
template <typename T>
class AXI_Receiver_Port : public AXI_Receiver_PortBase
{ 
public: 
	AXI_Receiver_Port( AXIPortIF* p, CASIModule *_owner, string n, AXI_CHANNEL chan ) : 
	  AXI_Receiver_PortBase(p,_owner,n,chan) 
	  {} 

	virtual ~AXI_Receiver_Port() {} 
	void driveTransaction(CASITransactionInfo* info) 
	{ 
		T::driveTransaction(topPort);

		crtAxiInfo->isReadyNew = (bool(crtAxiInfo->signals[T::readyIdx()]) ^ nextReady); 
		crtAxiInfo->signals[T::readyIdx()] = nextReady; 

		crtAxiInfo->numNewSignals = 0; 
		getMaster()->getNotifyHandler()->notifyEvent(info); 
		crtAxiInfo->isReadyNew = false; 
	}
};
#else
#define RECEIVER_PORT_DECL(CHANNEL) \
class CHANNEL ## _Receiver_Port: public AXI_Receiver_PortBase \
{ \
public: \
	CHANNEL ## _Receiver_Port( AXIPortIF* p, CASIModule *_owner, string n, AXI_CHANNEL chan ) : \
	  AXI_Receiver_PortBase(p,_owner,n,chan) \
	  {} \
\
	virtual ~CHANNEL ## _Receiver_Port() {} \
	void driveTransaction(CASITransactionInfo* info) \
	{ \
		topPort->driveTransactionCB_ ## CHANNEL(); \
\
		crtAxiInfo->isReadyNew = (bool(crtAxiInfo->signals[CHANNEL ## _READY]) ^ nextReady); \
		crtAxiInfo->signals[CHANNEL ## _READY] = nextReady; \
\
		crtAxiInfo->numNewSignals = 0; \
		getMaster()->getNotifyHandler()->notifyEvent(info); \
		crtAxiInfo->isReadyNew = false; \
	} \
\
}

RECEIVER_PORT_DECL(AR);
RECEIVER_PORT_DECL(AW);
RECEIVER_PORT_DECL(W);
RECEIVER_PORT_DECL(R);
RECEIVER_PORT_DECL(B);
#endif

#endif

