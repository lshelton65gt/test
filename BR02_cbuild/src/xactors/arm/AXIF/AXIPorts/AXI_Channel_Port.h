//
// Copyright (c)  2008 Carbon Design Systems Inc., All rights reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE
//
#ifndef _AXI_CHANNEL_PORT_H_
#define _AXI_CHANNEL_PORT_H_

#include "AXI_TLM.h"
#if 0 // goes into SD 7.2.SP2 since we can't force recompile
#include "AXIPortIF.h"
#endif

class AXITransactionInfo;
#if 1 // goes into SD 7.2.SP2 since we can't force recompile
class AXIPortIF;
#endif

/** 
 * Base class for AXI channel ports.  
 */
class AXI_Channel_Port
{
public:
	AXI_Channel_Port(AXIPortIF* p, AXI_CHANNEL chan);

	virtual ~AXI_Channel_Port();

	// each channel port can access the main port using this pointer
	AXIPortIF* topPort;

	// each channel port is designated a channel it represents
	AXI_CHANNEL channel;

	AXI_SIGNAL_IDX startIdx;
	AXI_SIGNAL_IDX endIdx;

	// represents the current signal values on the channel
	// should only be created/destroyed by the sender port
	AXITransactionInfo* crtAxiInfo;

private:
	AXI_Channel_Port();
	AXI_Channel_Port(const AXI_Channel_Port&);
	AXI_Channel_Port& operator=(const AXI_Channel_Port&);
};
#if 0 // goes into SD 7.2.SP2 since we can't force recompile
// delegate classes
class AR 
{ 
public: 
	static void driveTransaction(AXIPortIF* p) { p->driveTransactionCB_AR(); } 
	static void notifyEvent     (AXIPortIF* p) { p->notifyEventCB_AR(); } 
	static AXI_SIGNAL_IDX readyIdx() { return AR_READY; }
};
class AW 
{ 
public: 
	static void driveTransaction(AXIPortIF* p) { p->driveTransactionCB_AW(); } 
	static void notifyEvent     (AXIPortIF* p) { p->notifyEventCB_AW(); } 
	static AXI_SIGNAL_IDX readyIdx() { return AW_READY; }
};
class W  
{ 
public: 
	static void driveTransaction(AXIPortIF* p) { p->driveTransactionCB_W(); } 
	static void notifyEvent     (AXIPortIF* p) { p->notifyEventCB_W(); } 
	static AXI_SIGNAL_IDX readyIdx() { return W_READY; }
};
class R  
{ 
public: 
	static void driveTransaction(AXIPortIF* p) { p->driveTransactionCB_R(); } 
	static void notifyEvent     (AXIPortIF* p) { p->notifyEventCB_R(); } 
	static AXI_SIGNAL_IDX readyIdx() { return R_READY; }
};
class B  
{ 
public: 
	static void driveTransaction(AXIPortIF* p) { p->driveTransactionCB_B(); } 
	static void notifyEvent     (AXIPortIF* p) { p->notifyEventCB_B(); } 
	static AXI_SIGNAL_IDX readyIdx() { return B_READY; }
};
#endif
#endif
