#include "AXITransactionInfo.h"

void 
AXITransactionInfo::getValidDataMaskOnBus (MxU32* dataMask, MxU64& addr64, MxU32 busBitwidth, MxU32 dataSize)
{
    dataSize = 0x1 << dataSize; // AR_SIZE is encoded 0x2 is 4 bytes, this method expects dataSize in number of bytes.
    MxU32 mask              = ~((busBitwidth >> 3)-1);
    MxU32 addr              = (MxU32)addr64;
    MxU32 addrAlignedBus    = addr & mask;
    MxU32 addrAligned32     = addr & ~0x3;
    MxU32 addrAlignedSize   = addr & ~(dataSize-1);
    MxU32 index             = (addrAligned32-addrAlignedBus) >> 2;
    MxU32 mask32            = 0xffffffff;
    MxU32 num32             = (dataSize+3) >> 2;

    if (dataSize < 4)
    {
        mask32 = ~(mask32 << (dataSize << 3));
    }
    mask32 <<= (addr-addrAligned32) << 3;
    if (addrAlignedSize < addrAligned32)
    {
        num32 -= (addrAligned32-addrAlignedSize) >> 2;
    }
    memset(dataMask, 0, busBitwidth >> 3);
    dataMask[index] = mask32;
    for (MxU32 i = 1; i < num32; i++)
    {
        dataMask[++index] = 0xffffffff;
    }
    addr64 &= (MxU64)mask;
}

void 
AXITransactionInfo::getValidDataMaskOnBus (MxU32* dataMask, MxU64& addr64, MxU32 busBitwidth, MxU32 dataSize, MxU32 strb)
{
    dataSize = 0x1 << dataSize; // AW_SIZE is encoded 0x2 is 4 bytes, this method expects dataSize in number of bytes.
    static const MxU32 strb2Mask[16] = { 0x00000000, 0x000000FF, 0x0000FF00, 0x0000FFFF,
                                         0x00FF0000, 0x00FF00FF, 0x00FFFF00, 0x00FFFFFF,
                                         0xFF000000, 0xFF0000FF, 0xFF00FF00, 0xFF00FFFF,
                                         0xFFFF0000, 0xFFFF00FF, 0xFFFFFF00, 0xFFFFFFFF };

    MxU32 mask              = ~((busBitwidth >> 3)-1);
    MxU32 addr              = (MxU32)addr64;
    MxU32 addrAlignedBus    = addr & mask;
    MxU32 addrAligned32     = addr & ~0x3;
    MxU32 addrAlignedSize   = addr & ~(dataSize-1);
    MxU32 index             = (addrAligned32-addrAlignedBus) >> 2;
    MxU32 mask32            = 0xffffffff;
    MxU32 num32             = (dataSize+3) >> 2;

    if (dataSize < 4)
    {
        mask32 = ~(mask32 << (dataSize << 3));
    }
    mask32 <<= (addr-addrAligned32) << 3;
    if (addrAlignedSize < addrAligned32)
    {
        num32 -= (addrAligned32-addrAlignedSize) >> 2;
    }
    memset(dataMask, 0, busBitwidth >> 3);

    strb >>= (index << 2);
    mask32 &= strb2Mask[strb & 0xf];
    dataMask[index] = mask32;
    for (MxU32 i = 1; i < num32; i++)
    {
        strb >>= 4;
        mask32 = strb2Mask[strb & 0xf];
        dataMask[++index] = mask32;
    }
    addr64 &= (MxU64)mask;
}


bool AXITransactionInfo::saveData(eslapi::CASIODataStream& os)
{
  // Only a few members of the AXITransactionInfo object are actually used
  os.writeBytes(reinterpret_cast<const char*>(signals), NUM_AXI_SIGNALS * sizeof(uint32_t));
  os << static_cast<uint32_t>(crtChannel) << crtBeat;
  return true;
}

bool AXITransactionInfo::restoreData(eslapi::CASIIDataStream& is)
{
  // Only a few members of the AXITransactionInfo object are actually used
  char* data;
  uint32_t numBytes;
  is.readBytes(data, numBytes);
  if (numBytes != NUM_AXI_SIGNALS * sizeof(uint32_t)) {
    return false;
  }
  memcpy(signals, data, numBytes);
  delete [] data;
  uint32_t crtChannelTemp;
  is >> crtChannelTemp >> crtBeat;
  crtChannel = static_cast<AXI_CHANNEL>(crtChannelTemp);
  return true;
}
