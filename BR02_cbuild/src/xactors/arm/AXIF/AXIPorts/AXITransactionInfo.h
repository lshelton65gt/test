//
// Copyright (c)  2008 Carbon Design Systems Inc., All rights reserved.
//
// THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
// DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
// THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
// APPEARS IN ALL COPIES OF THIS SOFTWARE
//
#ifndef _AXITRANSACTIONINFO_H_
#define _AXITRANSACTIONINFO_H_

#include "AXI_TLM.h"

class AXITransactionInfo : public eslapi::CASITransactionInfo 
{
public:
	AXITransactionInfo() : numNewSignals(0), isReadyNew(false) {}
	virtual ~AXITransactionInfo() {}

    static void getValidDataMaskOnBus (MxU32* dataMask, MxU64& addr64, MxU32 busBitwidth, MxU32 dataSize);
    static void getValidDataMaskOnBus (MxU32* dataMask, MxU64& addr64, MxU32 busBitwidth, MxU32 dataSize, MxU32 strb);

	int numNewSignals;
	bool isReadyNew;
	uint32_t signals[NUM_AXI_SIGNALS];

	AXI_CHANNEL crtChannel;
	uint8_t crtBeat;
	uint8_t numBeats;

        bool saveData(eslapi::CASIODataStream& os);
        bool restoreData(eslapi::CASIIDataStream& is);
};


#endif

