#ifndef AXIFXTOREVENTS_H
#define AXIFXTOREVENTS_H 

#include "CarbonXtorEvent.h"

class AXI_S2T;
class AXI_T2S;

template<typename T>
class AXIXtorEvent : public CarbonXtorEvent
{
public:
	AXIXtorEvent(T* obj) : xtor(obj) {}
	virtual ~AXIXtorEvent() {}
protected:
	T* xtor;
};

#define EVENTCLASS(Channel, XtorType, EventType) \
class EventType ## Channel ## Event : public AXIXtorEvent<XtorType> \
{ \
public: \
	EventType ## Channel ## Event(XtorType* obj); \
	virtual ~EventType ## Channel ## Event(); \
	virtual void operator()(); \
}

EVENTCLASS(AR, AXI_S2T, Drive);
EVENTCLASS(AW, AXI_S2T, Drive);
EVENTCLASS(W,  AXI_S2T, Drive);
EVENTCLASS(R,  AXI_S2T, Notify);
EVENTCLASS(B,  AXI_S2T, Notify);

EVENTCLASS(AR, AXI_T2S, Notify);
EVENTCLASS(AW, AXI_T2S, Notify);
EVENTCLASS(W,  AXI_T2S, Notify);
EVENTCLASS(R,  AXI_T2S, Drive);
EVENTCLASS(B,  AXI_T2S, Drive);

#endif
