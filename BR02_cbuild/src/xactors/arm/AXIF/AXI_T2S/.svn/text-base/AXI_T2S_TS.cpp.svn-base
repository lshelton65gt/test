#include "AXI_T2S.h"
#include "AXI_T2S_TS.h"
#include "AXI_TLM.h"
#include "AXITransactionInfo.h"
#include "AXI_Sender_Port.h"

#include <map>

using namespace std;

AXI_T2S_TS::AXI_T2S_TS(CASIModule* _owner, AXI_T2S* xtor, std::string name) : AXI_Slave_Port(_owner, name)
{
	owner = xtor; 
}

void AXI_T2S_TS::reset()
{
	lastRReady = 0xFFFFFFFF;
	lastBReady  = 0xFFFFFFFF;

	AXI_Slave_Port::reset();
}

#define DRIVE_SMASTER(CHANNEL, IDX, CH) \
	owner->CHANNEL ## _SMaster->driveSignal(CH ## _TSlave->crtAxiInfo->signals[IDX], NULL)

void AXI_T2S_TS::driveTransactionCB_AR()
{
	if (AR_TSlave->crtAxiInfo->numNewSignals == 0)
		return;

	DRIVE_SMASTER(arvalid, AR_VALID, AR);
	DRIVE_SMASTER(aruser , AR_USER , AR);
	DRIVE_SMASTER(arid   , AR_ID   , AR);
	DRIVE_SMASTER(araddr , AR_ADDR , AR);
	DRIVE_SMASTER(arlen  , AR_LEN  , AR);
	DRIVE_SMASTER(arsize , AR_SIZE , AR);
	DRIVE_SMASTER(arburst, AR_BURST, AR);
	DRIVE_SMASTER(arlock , AR_LOCK , AR);
	DRIVE_SMASTER(arcache, AR_CACHE, AR);
	DRIVE_SMASTER(arprot , AR_PROT , AR);
#ifdef CARBON
	(*owner->mCombFunc)(owner->mCarbonComponent, true, false, false, true);
#endif
}
void AXI_T2S_TS::driveTransactionCB_AW()
{
	if (AW_TSlave->crtAxiInfo->numNewSignals == 0)
		return;

	DRIVE_SMASTER(awvalid, AW_VALID, AW);
	DRIVE_SMASTER(awuser , AW_USER , AW);
	DRIVE_SMASTER(awid   , AW_ID   , AW);
	DRIVE_SMASTER(awaddr , AW_ADDR , AW);
	DRIVE_SMASTER(awlen  , AW_LEN  , AW);
	DRIVE_SMASTER(awsize , AW_SIZE , AW);
	DRIVE_SMASTER(awburst, AW_BURST, AW);
	DRIVE_SMASTER(awlock , AW_LOCK , AW);
	DRIVE_SMASTER(awcache, AW_CACHE, AW);
	DRIVE_SMASTER(awprot , AW_PROT , AW);
#ifdef CARBON
	(*owner->mCombFunc)(owner->mCarbonComponent, true, false, false, true);
#endif
}
void AXI_T2S_TS::driveTransactionCB_W ()
{
	if (W_TSlave->crtAxiInfo->numNewSignals == 0)
		return;

	DRIVE_SMASTER(wvalid, W_VALID, W);
	DRIVE_SMASTER(wuser , W_USER , W);
	DRIVE_SMASTER(wid   , W_ID   , W);
	DRIVE_SMASTER(wstrb , W_STRB , W);
	DRIVE_SMASTER(wlast , W_LAST , W);
	unsigned numDataWords=((W_TSlave->getProperties()->dataBitwidth + 1)/32 + 1);
	uint32_t *data = new uint32_t[numDataWords];

	for (unsigned i=0; i<numDataWords; ++i)
		data[i] = W_TSlave->crtAxiInfo->signals[AXI_SIGNAL_IDX(W_DATA+i)];

	owner->wdata_SMaster->driveSignal(data[0], ((numDataWords > 1) ? &data[1] : 0));
	delete [] data;
#ifdef CARBON
	(*owner->mCombFunc)(owner->mCarbonComponent, true, false, false, true);
#endif
}

void AXI_T2S_TS::notifyEventCB_R()
{
	if (lastRReady == R_TMaster->crtAxiInfo->signals[R_READY])
		return;
	lastRReady = R_TMaster->crtAxiInfo->signals[R_READY];

	owner->rready_SMaster->driveSignal(static_cast<uint32_t>(this->R_TMaster->crtAxiInfo->signals[R_READY]), NULL);
#ifdef CARBON
	(*owner->mCombFunc)(owner->mCarbonComponent, false, false, true, false);
#endif
}

void AXI_T2S_TS::notifyEventCB_B()
{
	if (lastBReady == B_TMaster->crtAxiInfo->signals[B_READY])
		return;
	lastBReady = B_TMaster->crtAxiInfo->signals[B_READY];

	owner->bready_SMaster->driveSignal(static_cast<uint32_t>(this->B_TMaster->crtAxiInfo->signals[B_READY]), NULL);
#ifdef CARBON
	(*owner->mCombFunc)(owner->mCarbonComponent, false, false, true, false);
#endif
}

eslapi::CASIStatus AXI_T2S_TS::debugTransaction(eslapi::CASITransactionInfo *info)
{
#ifdef CARBON
  return CarbonDebugFunctionsToDebugAccess(owner->mDbaCb, info);
#endif
}


int AXI_T2S_TS::getNumRegions()
{
	AddressRegion reg;
	char name[20];
	if( _addrRegions.size() == 0 )
	{
		for( unsigned i=0; i<AXIF_MAX_REGIONS; ++i )
		{
			if( owner->_size[i] != 0 )
			{
				reg.start = owner->_start[i];
				reg.size = owner->_size[i];
				sprintf( name, "AXIF_T2S_TS_%d", i );
				reg.name = name;
				_addrRegions.push_back( reg );
			}
		}
	}
	if( owner->p_enableDbgMsg )
		owner->message( eslapi::CASI_MSG_INFO, "getNumRegions(): returning %d", _addrRegions.size() );
	return _addrRegions.size();
}

void AXI_T2S_TS::getAddressRegions(uint64_t* start, uint64_t* size, string* name)
{
	for( unsigned i=0; i<_addrRegions.size(); ++i )
	{
		start[i] = _addrRegions[i].start;
		size[i] = _addrRegions[i].size;
		name[i] = _addrRegions[i].name;
		if( owner->p_enableDbgMsg )
			owner->message( eslapi::CASI_MSG_INFO, "getAddressRegions(): region[%d]: start=0x%llx, size=0x%llx", i, start[i], size[i] );
	}
}

void AXI_T2S_TS::setAddressRegions(uint64_t* start, uint64_t* size, string* name)
{
	AddressRegion reg;
	if( start && size && name )
	{
		_addrRegions.clear();
		for( unsigned i=0; i<AXIF_MAX_REGIONS; ++i )
		{
			if( size[0] != 0 )
			{
				owner->_start[i] = start[0];
				owner->_size[i] = size[0];
				reg.start = start[0];
				reg.size = size[0];
				_addrRegions.push_back( reg );
			}
		}
	}
}
