/*
 *    Class definition of the SoC Designer component <AXI_T2S>
 *
 *    This code has been generated by the SoC Designer Component Wizard.
 *    Copyright (c) 2004-2007 by ARM Limited, 2000-2004 by AXYS Design Automation Inc., Irvine, CA, USA and AXYS GmbH, Herzongenrath
 *
 */

#ifndef AXI_T2S__H
#define AXI_T2S__H

#include "maxsimCompatibility.h"

//#include <map>
#include <vector>
#include "AXI_TLM.h"

#ifdef CARBON
#define CarbonSignalMasterPort CarbonXtorAdaptorToVhmPort
class CarbonXtorAdaptorToVhmPort;
#include "carbon_arm_adaptor.h"
#else
#define CarbonSignalMasterPort MxSignalMasterPort
#endif

#ifdef CARBON
#include <stdarg.h>
#include "CarbonAdaptorBase.h"
#ifndef CARBON_NO_LICENSE
#include "util/UtCLicense.h"
#endif
#define ADAPTOR_BASE CarbonAdaptorBase
#else
#  include "lc.h"
#  include "cmm_reg_defines.h"
#  include "SlaveableSimulatorAPI.h"
#  define ADAPTOR_BASE sc_mx_module
#endif

#define AXIF_MAX_REGIONS 6

// Place global class declarations here:

class AXI_T2S_CADI;
class AXI_T2S_TS;

#ifdef CARBON
//typedef CarbonSignalMasterPort ss_SM;
#define ss_SM CarbonSignalMasterPort
#else
//typedef sc_port<CASISignalIF> ss_SM;
#define ss_SM sc_port<CASISignalIF>
#endif

class AXI_T2S : 
#ifdef CARBON
public CarbonAdaptorBase
#else
public CASIModule
#endif
{
    // Declare your friends here:

	friend class ss_SS;
	friend class AXI_T2S_TS;
    friend class AXI_T2S_CADI;
	friend class PL301;

public:
    // Place instance declarations for the Ports here:
	AXI_T2S_TS* axi_TS;
#ifndef CARBON
	eslapi::MxTransactionMasterPort*  memdbg;
#endif

#define SIGNAL_MASTER_DECL(SIG) \
	ss_SM* SIG ## _SMaster

	SIGNAL_MASTER_DECL(awvalid);
	SIGNAL_MASTER_DECL(awuser );
	SIGNAL_MASTER_DECL(awid   );
	SIGNAL_MASTER_DECL(awaddr );
	SIGNAL_MASTER_DECL(awlen  );
	SIGNAL_MASTER_DECL(awsize );
	SIGNAL_MASTER_DECL(awburst);
	SIGNAL_MASTER_DECL(awlock );
	SIGNAL_MASTER_DECL(awcache);
	SIGNAL_MASTER_DECL(awprot );

	SIGNAL_MASTER_DECL(arvalid);
	SIGNAL_MASTER_DECL(aruser );
	SIGNAL_MASTER_DECL(arid   );
	SIGNAL_MASTER_DECL(araddr );
	SIGNAL_MASTER_DECL(arlen  );
	SIGNAL_MASTER_DECL(arsize );
	SIGNAL_MASTER_DECL(arburst);
	SIGNAL_MASTER_DECL(arlock );
	SIGNAL_MASTER_DECL(arcache);
	SIGNAL_MASTER_DECL(arprot );

	SIGNAL_MASTER_DECL(wvalid);
	SIGNAL_MASTER_DECL(wuser );
	SIGNAL_MASTER_DECL(wid   );  
	SIGNAL_MASTER_DECL(wstrb );
	SIGNAL_MASTER_DECL(wlast );
	SIGNAL_MASTER_DECL(wdata );

	SIGNAL_MASTER_DECL(rready);

	SIGNAL_MASTER_DECL(bready);

#define SIGNAL_SLAVE_DECL(SIG) \
	ss_SS* SIG ## _SSlave
	SIGNAL_SLAVE_DECL(rvalid);
	SIGNAL_SLAVE_DECL(ruser );
	SIGNAL_SLAVE_DECL(rid   );
	SIGNAL_SLAVE_DECL(rresp );
	SIGNAL_SLAVE_DECL(rlast );
	SIGNAL_SLAVE_DECL(rdata );

	SIGNAL_SLAVE_DECL(bvalid);
	SIGNAL_SLAVE_DECL(buser );
	SIGNAL_SLAVE_DECL(bid   );
	SIGNAL_SLAVE_DECL(bresp );

	SIGNAL_SLAVE_DECL(awready);
	SIGNAL_SLAVE_DECL(arready);
	SIGNAL_SLAVE_DECL(wready );

    // constructor / destructor
#ifdef CARBON
	AXI_T2S(sc_mx_module* carbonComp, const string& name, 
        	const string &compVersion, 
			CarbonObjectID **carbonObj, 
			CarbonPortFactory *portFactory,
			const CarbonDebugAccessCallbacks& dbaCb, 
			CarbonCombFunction* combFunc);
#else
	AXI_T2S(CASIModuleIF* c, const string &s);
#endif
	virtual ~AXI_T2S();

	// overloaded CASIModule methods
	string getName();
        void setPortWidth(const char* name, uint32_t bitWidth);
	void setParameter(const string &name, const string &value);
	string getProperty( eslapi::CASIPropertyType property );
	void init();
	void terminate();
#ifndef CARBON
	void communicate();
#endif
    void update();
	void reset(eslapi::CASIResetLevel level, const CASIFileMapIF *filelist);
	// The CADI interface.
	CADI* getCADI();

	//Initialize the Port Properties
	void initSignalPort(CASISignalMasterIF* signalIf, uint32_t bitwidth = 32);
	void initTransactionPort(CASITransactionMasterIF* transIf);

	virtual void sendDrive();
	virtual void sendRDrive();
	virtual void sendBDrive();
	virtual void sendNotify();
	virtual void sendARNotify();
	virtual void sendAWNotify();
	virtual void sendWNotify();

	void backwardValid();
	void backwardReady();

        bool saveData(eslapi::CASIODataStream& os);
        bool restoreData(eslapi::CASIIDataStream& is);

	bool awReadyChanged;
	bool arReadyChanged;
	bool wReadyChanged;
	bool rChanged;
	bool bChanged;

private:
	bool initComplete; 
	bool p_enableDbgMsg;

	// User defined parameters:
	int p_Data_Width;
	int p_Address_Width;
	bool p_useCosimConfig;

	// Declare CADI Interface
	CADI* cadi;

	uint16_t r_reg0;
	uint16_t r_reg1;


	// memory mapping
	uint64_t _start[AXIF_MAX_REGIONS];
	uint64_t _size[AXIF_MAX_REGIONS];

	// place your private functions and data members here:
	// ...
public:

#ifdef CARBON
	CarbonDebugAccessCallbacks   mDbaCb;
	CarbonCombFunction* mCombFunc;
#endif

};

#endif
