#ifndef __AXI_T2S_TS_H__
#define __AXI_T2S_TS_H__

#include "AXI_Slave_Port.h"
#include "AXI_Receiver_Port.h"

class AXI_T2S;

class AXI_T2S_TS : public AXI_Slave_Port
{
public:
	AXI_T2S_TS(CASIModule* _owner, AXI_T2S* xtor, std::string name);
	virtual ~AXI_T2S_TS() {}

	AXI_T2S* owner;

	void reset();

	void driveTransactionCB_AR();
	void driveTransactionCB_AW();
	void driveTransactionCB_W ();

	void notifyEventCB_R();
	void notifyEventCB_B();

	virtual eslapi::CASIStatus debugTransaction(eslapi::CASITransactionInfo *info);

	/* Memory map functions */
	virtual int getNumRegions();
	virtual void getAddressRegions(uint64_t* start, uint64_t* size, string* name);

	/* CASI : now the address regions can be set from the outside! */
	virtual void setAddressRegions(uint64_t* start, uint64_t* size, string* name);

	uint32_t lastRReady;
	uint32_t lastBReady;

private:
	struct AddressRegion
	{
		uint64_t start;
		uint64_t size;
		string name;
	};
	std::vector<AddressRegion> _addrRegions;
};

#endif
