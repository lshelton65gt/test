/*
 *    Class implementation of the CADI interface for component <AXI_T2S>
 *
 *    This code has been generated by the SoC Designer Component Wizard.
 *    Copyright (c) 2004-2007 by ARM Limited, 2000-2004 by AXYS Design Automation Inc., Irvine, CA, USA and AXYS GmbH, Herzongenrath
 *
 */

#include "AXI_T2S_CADI.h"
#include "AXI_T2S.h"
#include "assert.h" 


using namespace eslapi;

struct SimpleRegInfo_t
{
    const char*             name;
    unsigned int            bitsWide;
    CADIRegDisplay_t        display;
    const char*             description;
};

struct GroupInfo_t
{
    const char*             description;
    const char*             name;
    int	                    start;
    int                     end;
};

struct MemSpaceInfo_t
{
    const char*             memSpaceName;
    unsigned int            bitsPerMau;
    CADIAddrSimple_t        maxAddress;
    int                     isProgramMemory;
    unsigned int            cyclesToAccess;
    CADIMemReadWrite_t      readWrite;
    const char*             description;
};

// TODO: Define your registers here.


static const SimpleRegInfo_t AXI_T2S_reg_info[] =
{


    // "Group1" SoC Designer pre-defined registers
    {   "reg0", 16, eslapi::CADI_REGTYPE_HEX, "Example register of size 16bit, displayed as hex value" }
    , { "reg1", 16, eslapi::CADI_REGTYPE_HEX, "Example register of size 16bit, displayed as hex value" }


};

// TODO: Define your groups here.

static const GroupInfo_t AXI_T2S_group_info[] =
{

    {   "Reg description", "Reg name", 0, 1 }


};

// TODO: Define your memory spaces here.

static const MemSpaceInfo_t AXI_T2S_mem_info[] =
{
    { "RAM", 8, 0xFFFF, 0, 0, CADI_MEM_ReadWrite, "Generic Memory" }
};



static const uint32_t REGISTER_COUNT	= sizeof( AXI_T2S_reg_info    ) / sizeof( AXI_T2S_reg_info[0]   );
static const uint32_t GROUP_COUNT	= sizeof( AXI_T2S_group_info  ) / sizeof( AXI_T2S_group_info[0] );
static const uint32_t MEMORY_COUNT   = sizeof( AXI_T2S_mem_info    ) / sizeof( AXI_T2S_mem_info[0]   );


AXI_T2S_CADI::AXI_T2S_CADI(AXI_T2S* c) : target(c)
{
    uint32_t	i;
    
    strcpy( features.targetName, "AXI_T2S" );
    strcpy( features.targetVersion, "1.0" );
    
    // Register related info
    features.nrRegisterGroups = GROUP_COUNT;
    
    regInfo = new CADIRegInfo_t[ REGISTER_COUNT ];
    for ( i = 0; i < REGISTER_COUNT; i++ )
    {
        regInfo[i].regNumber                    = i;
        regInfo[i].bitsWide                     = AXI_T2S_reg_info[i].bitsWide;
        regInfo[i].display                      = AXI_T2S_reg_info[i].display;
        regInfo[i].hasSideEffects               = 0;
        regInfo[i].details.type                 = CADI_REGTYPE_Simple;

        strcpy( regInfo[i].name,                AXI_T2S_reg_info[i].name );
        strcpy( regInfo[i].description,         AXI_T2S_reg_info[i].description );
        
        // maximum register bitwidth is 256!
        assert(AXI_T2S_reg_info[i].bitsWide <= 256);
    }


    
    regGroup = new CADIRegGroup_t[ GROUP_COUNT ];
    for ( i = 0; i < GROUP_COUNT; i++ )
    {
        regGroup[i].groupID                     = i;
        regGroup[i].numRegsInGroup              = AXI_T2S_group_info[i].end - AXI_T2S_group_info[i].start + 1;

        strcpy( regGroup[i].description,        AXI_T2S_group_info[i].description );
        strcpy( regGroup[i].name,				AXI_T2S_group_info[i].name );
    }
    
    // Memory related info
    features.nrMemSpaces = MEMORY_COUNT;
    
    memSpaceInfo = new CADIMemSpaceInfo_t[ MEMORY_COUNT ];
    memBlockInfo = new CADIMemBlockInfo_t[ MEMORY_COUNT ];
    for ( i = 0; i < MEMORY_COUNT; i++ )
    {
        memSpaceInfo[i].memSpaceId            = i;
        memSpaceInfo[i].bitsPerMau            = AXI_T2S_mem_info[i].bitsPerMau;
        memSpaceInfo[i].maxAddress            = AXI_T2S_mem_info[i].maxAddress;
        memSpaceInfo[i].nrMemBlocks           = 1;
        memSpaceInfo[i].isProgramMemory       = AXI_T2S_mem_info[i].isProgramMemory;
        
        strcpy( memSpaceInfo[i].memSpaceName, AXI_T2S_mem_info[i].memSpaceName );
        strcpy( memSpaceInfo[i].description,  AXI_T2S_mem_info[i].description );
        
        memBlockInfo[i].id                    = (uint16_t) i;
        memBlockInfo[i].parentID              = (uint16_t) i;
        memBlockInfo[i].startAddr             = 0;
        memBlockInfo[i].endAddr               = AXI_T2S_mem_info[i].maxAddress;
        memBlockInfo[i].cyclesToAccess        = AXI_T2S_mem_info[i].cyclesToAccess;
        memBlockInfo[i].readWrite             = AXI_T2S_mem_info[i].readWrite;
        
        strcpy( memBlockInfo[i].name,         AXI_T2S_mem_info[i].memSpaceName );
    }
}

AXI_T2S_CADI::~AXI_T2S_CADI()
{
    if ( regInfo )
        delete [] regInfo;
    if ( regGroup )		
        delete [] regGroup;
    if ( memSpaceInfo )	
        delete [] memSpaceInfo;
    if ( memBlockInfo )	
        delete [] memBlockInfo;
}


// Register related functions
CADIReturn_t
AXI_T2S_CADI::CADIRegGetGroups( uint32_t groupIndex
                                      , uint32_t desiredNumOfRegGroups
                                      ,	uint32_t* actualNumOfRegGroups
                                      , CADIRegGroup_t* grp )
{
    if ( groupIndex >= GROUP_COUNT )
    {
        return CADI_STATUS_IllegalArgument;
    }
    
    uint32_t	i;
    for( i = groupIndex; ( i < groupIndex + desiredNumOfRegGroups ) && ( i < 
        GROUP_COUNT ); i++ )
    {
        grp[i] = regGroup[i];
    }
    
    *actualNumOfRegGroups = i - groupIndex;
    
    return CADI_STATUS_OK;
}

CADIReturn_t
AXI_T2S_CADI::CADIRegGetMap( uint32_t groupID
                                   , uint32_t regIndex
                                   , uint32_t registerSlots
                                   , uint32_t* registerCount
                                   , CADIRegInfo_t* reg )
{
    if ( groupID >= GROUP_COUNT )
    {
        return CADI_STATUS_IllegalArgument;
    }
    
    uint32_t	i;
    uint32_t	start = regIndex + AXI_T2S_group_info[groupID].start;
    uint32_t	end = regIndex + AXI_T2S_group_info[groupID].end;
    for ( i = 0; ( i < registerSlots ) && ( start + i <= end ); i++ )
    {
        reg[i] = regInfo[start + i];
    }
    
    *registerCount = i;
    
    return CADI_STATUS_OK;
}


CADIReturn_t
AXI_T2S_CADI::CADIRegWrite( uint32_t regCount
                                  , CADIReg_t* reg
                                  , uint32_t* numRegsWritten
                                  , uint8_t doSideEffects )
{
    UNUSEDARG(doSideEffects);

    uint32_t	i;
    for ( i = 0; i < regCount; i++ )
    {
        
        // TODO:  Assign the value to the appropriate "register".
		uint16_t tmp16 = 0;
 


        switch (reg[i].regNumber)
        {

        case 0:
            //16-bit Register
            tmp16 = (tmp16<<8) | reg[i].bytes[1];
            tmp16 = (tmp16<<8) | reg[i].bytes[0];
            target->r_reg0 = tmp16;
            break;

        case 1:
            //16-bit Register
            tmp16 = (tmp16<<8) | reg[i].bytes[1];
            tmp16 = (tmp16<<8) | reg[i].bytes[0];
            target->r_reg1 = tmp16;
            break;

        default:
            break;

        }


            
    }

    *numRegsWritten = regCount;
    
    return CADI_STATUS_OK;
}

CADIReturn_t
AXI_T2S_CADI::CADIRegRead( uint32_t regCount
                                , CADIReg_t* reg
                                , uint32_t* numRegsRead
                                , uint8_t doSideEffects )
{
    UNUSEDARG(doSideEffects);

    uint32_t	i;
    for ( i = 0; i < regCount; i++ )
    {
        
        // TODO:  Assign the value to the appropriate "register".
		uint16_t tmp16 = 0;
 


        switch (reg[i].regNumber)
        {

        case 0:
            //16-bit Register
            tmp16 = target->r_reg0;
            reg[i].bytes[0] = (uint8_t)((tmp16 >> 0) & 0xff);
            reg[i].bytes[1] = (uint8_t)((tmp16 >> 8) & 0xff);
            break;

        case 1:
            //16-bit Register
            tmp16 = target->r_reg1;
            reg[i].bytes[0] = (uint8_t)((tmp16 >> 0) & 0xff);
            reg[i].bytes[1] = (uint8_t)((tmp16 >> 8) & 0xff);
            break;

        default:
            break;

        }


            
    }
    *numRegsRead = regCount;
    
    return CADI_STATUS_OK;
}


// Memory related functions
CADIReturn_t
AXI_T2S_CADI::CADIMemGetSpaces( uint32_t spaceIndex
                                      , uint32_t memSpaceSlots
                                      , uint32_t* memSpaceCount
                                      , CADIMemSpaceInfo_t* memSpace )
{
    if ( spaceIndex >= MEMORY_COUNT )
        return CADI_STATUS_IllegalArgument;
    
    uint32_t	i;
    uint32_t	start = spaceIndex;
    for( i = 0; ( i < memSpaceSlots ) && ( start + i < MEMORY_COUNT ); i++ )
    {
        memSpace[i] = memSpaceInfo[start + i];
    }
    
    *memSpaceCount = i;
    
    return CADI_STATUS_OK;
}


CADIReturn_t
AXI_T2S_CADI::CADIMemGetBlocks( uint32_t memorySpace
                                      , uint32_t blockIndex
                                      , uint32_t memBlockSlots
                                      , uint32_t* memBlockCount
                                      , CADIMemBlockInfo_t* memBlock )
{
    if ( memorySpace >= MEMORY_COUNT )
        return CADI_STATUS_IllegalArgument;
    
    
    uint32_t	i;
    uint32_t	start = blockIndex;
    for ( i = 0; ( i < memBlockSlots ) && ( start + i < memSpaceInfo[memorySpace].nrMemBlocks ); i++ )
        memBlock[i] = memBlockInfo[start + i];
    
    *memBlockCount = i;
    
    return CADI_STATUS_OK;
}


CADIReturn_t
AXI_T2S_CADI::CADIMemWrite( CADIAddrComplete_t startAddress
                                  , uint32_t unitsToWrite
                                  , uint32_t unitSizeInBytes
                                  , const uint8_t *data
                                  , uint32_t *actualNumOfUnitsWritten
                                  , uint8_t doSideEffects )
{
    UNUSEDARG(doSideEffects);

    uint32_t i = 0;
    
    for ( i = 0; i < unitsToWrite * unitSizeInBytes; )
    {
        uint32_t tmp;
        
        if ( unitSizeInBytes == 1 )
        {
            tmp = data[i++];
        }
        else if ( unitSizeInBytes == 2 )
        {
            tmp  = data[i++];
            tmp |= data[i++] << 8;
        }
        else if ( unitSizeInBytes == 4 )
        {
            tmp  = data[i++];
            tmp |= data[i++] << 8;
            tmp |= data[i++] << 16;
            tmp |= data[i++] << 24;
        }
        
        // TODO:  Write the data to memory.
    }
    
    *actualNumOfUnitsWritten = i / unitSizeInBytes;
    return CADI_STATUS_OK;
}


CADIReturn_t
AXI_T2S_CADI::CADIMemRead( CADIAddrComplete_t startAddress
                                 , uint32_t unitsToRead
                                 , uint32_t unitSizeInBytes
                                 , uint8_t *data
                                 , uint32_t *actualNumOfUnitsRead
                                 , uint8_t doSideEffects )
{
    UNUSEDARG(doSideEffects);

    uint32_t i = 0;
    
    for ( i = 0; i < unitsToRead * unitSizeInBytes; )
    {    
        uint32_t tmp = 0;
        
        // TODO:  Read the data from memory.
        
        if ( unitSizeInBytes == 1 )
        {
            data[i++] = (uint8_t)( tmp & 0xFF);
        }
        else if ( unitSizeInBytes == 2 )
        {
            data[i++] = (uint8_t)(tmp & 0xFF);
            data[i++] = (uint8_t)(( tmp >> 8 ) & 0xFF);
        }
        else if ( unitSizeInBytes == 4 )
        {
            data[i++] = (uint8_t)(tmp & 0xFF);
            data[i++] = (uint8_t)(( tmp >> 8 ) & 0xFF);
            data[i++] = (uint8_t)(( tmp >> 16 ) & 0xFF);
            data[i++] = (uint8_t)(( tmp >> 24 ) & 0xFF);
        }
    }
    
    *actualNumOfUnitsRead = i / unitSizeInBytes;
    return CADI_STATUS_OK;
}

