/*
 *    Class implementation of ss port for component <signalslave>
 *
 *    This code has been generated by the SoC Designer Component Wizard.
 *    Copyright (c) 2004-2007 by ARM Limited, 2000-2004 by AXYS Design Automation Inc., Irvine, CA, USA and AXYS GmbH, Herzongenrath
 *
 */

#include "ss_SS.h"
#include "AXI_Channel_Port.h"
#include "AXI_Sender_Port.h"

ss_SS::ss_SS( CASIModule *_owner, uint32_t width, AXI_Sender_PortBase* port, AXI_SIGNAL_IDX sigIdx, bool &gChange ) : 
	CASISignalSlave( "ss_SS" ),
	mChanged(gChange),
	lastCycle(0),
	mVal(0),
    connectedPort(port),
    signalIdx(sigIdx)
{
    owner = _owner;
    setCASIOwner(owner);
    CASISignalProperties prop;
    memset(&prop,0,sizeof(prop));
    prop.isOptional=false;
	prop.bitwidth=width;
    setProperties(&prop);
	
	
    // TODO:  Add any additional constructor code here.
}
ss_SS::ss_SS( CASIModule *_owner, uint32_t width, bool &gChange) :
	CASISignalSlave( "ss_SS" ),
	mChanged(gChange),
	lastCycle(0),
	mVal(0),
    connectedPort(NULL),
    signalIdx(NUM_AXI_SIGNALS)
{
    owner = _owner;
    setCASIOwner(owner);
    CASISignalProperties prop;
    memset(&prop,0,sizeof(prop));
    prop.isOptional=false;
	prop.bitwidth=width;
    setProperties(&prop);
	
	
    // TODO:  Add any additional constructor code here.
}

/* Access functions */
void
ss_SS::driveSignal(uint32_t value, uint32_t* extValue)
{
    // TODO: Add your code here.
	connectedPort->nextSignals.push_back(std::pair<AXI_SIGNAL_IDX, uint64_t>(signalIdx, value));
	mChanged = true;
	mVal = value;
}

	uint32_t
ss_SS::readSignal()
{
	// TODO: Add your code here.

	return 0;
}

	void
ss_SS::readSignal(uint32_t* value, uint32_t* extValue)
{
	// TODO: Add your code here.

}

