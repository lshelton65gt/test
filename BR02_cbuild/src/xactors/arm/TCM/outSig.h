// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _OUTSIG_H_
#define _OUTSIG_H_

#include "maxsimCompatibility.h"

/*
** This signal slave can handle signals up to 32 bits.  It ignores
** the extValue parameters, so anything more than 32 bits will get dropped
*/
class outSig: public CASISignalSlave
{
//

public:
    outSig(std::string _name) : CASISignalSlave(_name), data(0) {}
    virtual ~outSig() {}

public:
    /* Access functions */
    virtual void driveSignal(uint32_t value, uint32_t* ) {data = value;}
    
    virtual uint32_t readSignal() {return data;}
    virtual void readSignal(uint32_t* value, uint32_t* ) {*value = data;}
private:
    uint32_t data;
};

#endif // _OUTSIG_H_
