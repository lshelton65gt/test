// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/


#include "CarbonTCMS2T.h"

#define CARBON 1
#include "TCMMasterXtor.h"
CarbonTCMS2T::CarbonTCMS2T(sc_mx_module* carbonComp,
                           const char *xtorName,
                           CarbonObjectID **carbonObj,
                           CarbonPortFactory *portFactory)
{
  TCMMasterXtor *temp = new TCMMasterXtor(carbonComp, xtorName, carbonObj, portFactory); 
  mArmAdaptor = temp;
}

sc_mx_signal_slave *CarbonTCMS2T::carbonGetPort(const char *name)
{
  return ((TCMMasterXtor *) mArmAdaptor)->carbonGetPort(name);
}

CarbonTCMS2T::~CarbonTCMS2T()
{
  delete ((TCMMasterXtor *) mArmAdaptor);
}

void CarbonTCMS2T::communicate()
{
  ((TCMMasterXtor *) mArmAdaptor)->communicate();
}

void CarbonTCMS2T::update()
{
  ((TCMMasterXtor *) mArmAdaptor)->update();
}

void CarbonTCMS2T::init()
{
  ((TCMMasterXtor *) mArmAdaptor)->init();
}

void CarbonTCMS2T::terminate()
{
  ((TCMMasterXtor *) mArmAdaptor)->terminate();
}

void CarbonTCMS2T::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  ((TCMMasterXtor *) mArmAdaptor)->reset(level, filelist);
}

void CarbonTCMS2T::setParameter(const string &name, const string &value)
{
  ((TCMMasterXtor *) mArmAdaptor)->setParameter(name, value);
}

CASIPortIF* CarbonTCMS2T::getTmPort()
{
  return ((TCMMasterXtor *) mArmAdaptor)->carbonGetTransactionPort();
}

bool CarbonTCMS2T::saveData(eslapi::CASIODataStream& os)
{
  return ((TCMMasterXtor *) mArmAdaptor)->saveData(os);
}

bool CarbonTCMS2T::restoreData(eslapi::CASIIDataStream& is)
{
  return ((TCMMasterXtor *) mArmAdaptor)->restoreData(is);
}

CarbonDebugAccessStatus
CarbonTCMS2T::debugAccess(CarbonDebugAccessDirection dir, MxU64 startAddress, MxU32 numBytes, MxU8 *data,
                          MxU32 *numBytesProcessed, MxU32 *ctrl)
{
  TCMMasterXtor* xtor = (TCMMasterXtor *)mArmAdaptor;
  return xtor->debugAccess(dir, startAddress, numBytes, data, numBytesProcessed, ctrl);
}
