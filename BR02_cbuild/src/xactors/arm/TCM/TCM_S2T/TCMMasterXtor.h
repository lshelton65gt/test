// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _TCMMASTERXTOR_H_
#define _TCMMASTERXTOR_H_

// required for SoC Designer classes including
// CASISignalSlave and CASITransactionIF
#include "maxsimCompatibility.h"

// required for CarbonPortFactory
#include "xactors/arm/include/carbon_arm_adaptor.h"

// required for CarbonObjectID
#include "carbon/carbon_capi.h"

// Needed for debug access
#include "carbon/CarbonDebugAccess.h"

// Transactor specific derivative of CASISignalSlave
#include "outSig.h"

class TCMMasterXtor
{
    /*
    ** Transactor outputs that connect to RTL inputs
    */
    CarbonXtorAdaptorToVhmPort *rrd_SMaster;        // read data
    CarbonXtorAdaptorToVhmPort *rwait_SMaster;      // wait

    /*
    ** Transactor inputs that connect to RTL outputs
    */
    outSig* raddr_SSlave;     // address
    outSig* rnrw_SSlave;      // read/write
    outSig* rwd_SSlave;       // write data
    outSig* ridle_SSlave;     // idle cycle
    outSig* rcs_SSlave;       // chip select
    outSig* rwbl_SSlave;      // byte enables for write data
    outSig* rseq_SSlave;      // sequential data access

    /*
    ** The transaction port that we'll create on the component
    */
    sc_port<CASITransactionIF>* tcm_tm;
    void initTransactionPort(CASITransactionMasterIF* transIf);  // init code for the port

public:
    /*
    ** the following functions are the require interface for a transactor.
    */
    TCMMasterXtor(CASIModuleIF*, const char* s, CarbonObjectID**, CarbonPortFactory*);
    virtual ~TCMMasterXtor();

    void communicate();
    void update();

    string getName();
    void setParameter(const string &name, const string &value);

    void init();
    void terminate();
    void reset(eslapi::CASIResetLevel level, const CASIFileMapIF *filelist);

    sc_mx_signal_slave *carbonGetPort(const char *name);
    sc_port<CASITransactionIF>* carbonGetTransactionPort() { return tcm_tm; }

    CarbonDebugAccessStatus debugAccess(CarbonDebugAccessDirection dir, MxU64 startAddress, MxU32 numBytes, MxU8 *data, MxU32 *numBytesProcessed, MxU32 *ctrl);

    bool saveData(eslapi::CASIODataStream& os);
    bool restoreData(eslapi::CASIIDataStream& is);

private:
    CASIModuleIF* owner;

    uint32_t addr, data;
    uint32_t ctrl[3];

    bool writeInProgress;
    bool readInProgress;

};

#endif // _TCMMASTERXTOR_H_
