// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010-2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "TCM.h"
#include "TCMMasterXtor.h"

TCMMasterXtor::TCMMasterXtor(CASIModuleIF* c, const char* name,
                             CarbonObjectID**carbonModel, CarbonPortFactory* portFactory) :
   owner(c)
{
   /*
    ** get the transactor outputs from the portFactor that we were provided
    */
   rrd_SMaster = portFactory(owner, name, "RRD", carbonModel);
   rwait_SMaster = portFactory(owner, name, "RWAIT", carbonModel);

   /*
    ** Instantiate our input ports - later the component will ask us for these pointers
    **  by name
    */
   raddr_SSlave = new outSig("RADDR");
   rnrw_SSlave = new outSig("RNRW");
   rwd_SSlave = new outSig("RWD");
   ridle_SSlave = new outSig("RIDLE");
   rcs_SSlave = new outSig("RCS");
   rwbl_SSlave = new outSig("RWBL");
   rseq_SSlave = new outSig("RSEQ");
   /*
    ** Create a transaction port and register it
    */
   tcm_tm = new sc_port<CASITransactionIF> (owner, name);
   initTransactionPort((CASITransactionMasterIF*) tcm_tm);
   owner->registerPort(tcm_tm, name);

}

void TCMMasterXtor::update()
{
}

void TCMMasterXtor::communicate()
{
  uint32_t rcs = rcs_SSlave->readSignal();
  if ((rcs != 0) || readInProgress || writeInProgress) {
    // Always get the address and read/write mode
    addr = raddr_SSlave->readSignal();
    uint32_t rnrw = rnrw_SSlave->readSignal();

    // Assume we are not going to wait
    uint32_t rwait = 0;

    // Check if a read is occuring. Note we can have overlaping last phase of read and writes
    if (readInProgress || (rnrw == 0)) { // read
      // Set the current state
      TCMPhases phase = TCM_CYCLE_IDLE;
      if (!readInProgress) {
        // Must be the address phase
        phase = TCM_CYCLE_ADDR;
        readInProgress = true;

      } else if (rcs_SSlave->readSignal() && (rnrw == 0)) {
        // Data, but not the last since CS is high and we didn't
        // initiate a write.  Note that SEQ seems to just be a hint so
        // we can't count on it being high. For some reason the 926
        // doesn't always set the SEQ port (DMA).
        phase = TCM_CYCLE_DATA;

      } else {
        // Last data phase because either CS went away or we initiated
        // a write.
        phase = TCM_CYCLE_DATA_LAST;
        readInProgress = false;
      }

      // Do the Read and drive any responses necessary (data and wait)
      ctrl[1] = phase;
      eslapi::CASIStatus status = tcm_tm->read(addr*4, &data, ctrl);
      switch (status) {
        case eslapi::CASI_STATUS_OK:
          switch(phase) {
            case TCM_CYCLE_ADDR:
              // Address phase, nothing to do
              break;

            case TCM_CYCLE_DATA:
            case TCM_CYCLE_DATA_LAST:
              // Data phase, return the information
              rrd_SMaster->driveSignal(data, 0);
              break;

            case TCM_CYCLE_IDLE:
              // Should not happen, should we error out?
              break;
          }
          break;

        case eslapi::CASI_STATUS_WAIT:
          // Delay, driven below
          rwait = 1;
          break;

        default:
          // TODO put an error here
          break;
      } // switch
    } // if

    // Check if a write is in progress
    if (writeInProgress || (rnrw == 1)) {
      // For writes, we run each write as a single cycle
      writeInProgress = true;
      data = rwd_SSlave->readSignal();
      ctrl[0] = rwbl_SSlave->readSignal();
      eslapi::CASIStatus status = tcm_tm->write(addr*4, &data, ctrl);
      if (eslapi::CASI_STATUS_OK == status) {
        writeInProgress = false;

      } else if (eslapi::CASI_STATUS_WAIT == status) {
        rwait = 1;
      } else {
        // TODO need to put an error here
      }
    }

    // Drive the rwait based on the read/write state machines above
    rwait_SMaster->driveSignal(rwait, 0);
  } // if
} // void TCMMasterXtor::communicate

sc_mx_signal_slave *TCMMasterXtor::carbonGetPort(const char* name)
{
   if (0 == strcmp(name, "rnrw"))
   {
      return rnrw_SSlave;
   }
   else if (0 == strcmp(name, "raddr"))
   {
      return raddr_SSlave;
   }

   else if (0 == strcmp(name, "rwd"))
   {
      return rwd_SSlave;
   }
   else if (0 == strcmp(name, "ridle"))
   {
      return ridle_SSlave;
   }
   else if (0 == strcmp(name, "rcs"))
   {
      return rcs_SSlave;
   }
   else if (0 == strcmp(name, "rwbl"))
   {
      return rwbl_SSlave;
   }
   else if (0 == strcmp(name, "rseq"))
   {
      return rseq_SSlave;
   }

   return 0;
}

void
TCMMasterXtor::init()
{
   readInProgress = writeInProgress = false;
}

void
TCMMasterXtor::reset(eslapi::CASIResetLevel, const CASIFileMapIF *)
{
}

void
TCMMasterXtor::terminate()
{
}

void
TCMMasterXtor::setParameter( const string &name, const string &value )
{
    //CASIConvertErrorCodes status = CASIConvert_SUCCESS;


}

string
TCMMasterXtor::getName(void)
{
    return "TCMMasterXtor";
}

void TCMMasterXtor::initTransactionPort(CASITransactionMasterIF* transIf)
{
   CASITransactionProperties prop;
   memset(&prop, 0, sizeof(prop));
   prop.casiVersion = eslapi::CASI_VERSION_1_1; //the transaction version used for this transaction
   prop.useMultiCycleInterface = false; // using read/write interface methods
   prop.addressBitwidth = 32; // address bitwidth used for addressing of resources
   prop.mauBitwidth = 8; // minimal addressable unit
   prop.dataBitwidth = 32; // maximum data bitwidth transferred in a single cycle
   prop.isLittleEndian = true; // alignment of MAUs
   prop.isOptional = false; // if true this port can be disabled
   prop.supportsAddressRegions = true; // M/S can negotiate address mapping
   prop.numCtrlFields = 3; // # of ctrl elements used / entry is used for transfer size
   prop.protocolID = TCM_PROTOCOL_ID; // magic number of the protocol (Vendor/Protocol-ID):
   //The upper 16 bits = protocol implementation version
   //The lower 16 bits = actual protocol ID
   sprintf(prop.protocolName, "TCM"); // The name of the  protocol being used
   // CASI : slave port address regions forwarding
   prop.isAddrRegionForwarded = false; //true if addr region for this slave port is actually
   //forwarded to a master port, false otherwise
   prop.forwardAddrRegionToMasterPort = NULL; //master port of the  same component to which this slave port's addr region is forwarded
   transIf->setProperties(&prop);
}

TCMMasterXtor::~TCMMasterXtor()
{
   delete rnrw_SSlave;
   delete rwd_SSlave;
   delete ridle_SSlave;
   delete rcs_SSlave;
   delete rwbl_SSlave;
   delete rseq_SSlave;
   delete tcm_tm;
}

CarbonDebugAccessStatus TCMMasterXtor::debugAccess(CarbonDebugAccessDirection dir, MxU64 startAddress,
                                                   MxU32 numBytes, MxU8 *data, MxU32 *numBytesProcessed,
                                                   MxU32 *ctrl)
{
  eslapi::CASIStatus status = eslapi::CASI_STATUS_OK;
  *numBytesProcessed = 0;
  // Needs to be at least 3 words, because
  // CarbonCommonDebugFunctionsToDebugAccess() manipulates index 2.
  // The first entry is 1 which is AHB_TYPE_BYTE
  uint32_t localCtrl[3] = {1, 0, 0};
  uint32_t tmpData;
  for (uint32_t i = 0; (i < numBytes) && (status == eslapi::CASI_STATUS_OK); ++i) {
    MxU64 address = startAddress + i;
    if (dir == eCarbonDebugAccessRead) {
      status = tcm_tm->readDbg(address, &tmpData, localCtrl);
      data[i] = tmpData & 0xFF;
    } else {
      memset(&tmpData, data[i], 4);
      status = tcm_tm->writeDbg(address, &tmpData, localCtrl);
    }
    if (status == eslapi::CASI_STATUS_OK) {
      ++(*numBytesProcessed);
    }
  }
  if (status == eslapi::CASI_STATUS_OK) {
    return eCarbonDebugAccessStatusOk;
  } else {
    return eCarbonDebugAccessStatusError;
  }
}

bool TCMMasterXtor::saveData(eslapi::CASIODataStream& os)
{
  os << addr << data << ctrl[0] << ctrl[1] << ctrl[2] << writeInProgress << readInProgress;
  return true;
}

bool TCMMasterXtor::restoreData(eslapi::CASIIDataStream& is)
{
  is >> addr >> data >> ctrl[0] >> ctrl[1] >> ctrl[2] >> writeInProgress >> readInProgress;
  return true;
}
