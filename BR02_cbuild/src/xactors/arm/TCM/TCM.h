// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _TCM_H_
#define _TCM_H_

enum TCMPhases
{
  TCM_CYCLE_IDLE,
  TCM_CYCLE_ADDR,
  TCM_CYCLE_DATA,
  TCM_CYCLE_DATA_LAST
};

#define TCM_PROTOCOL_ID 0x7C300001

#endif // _TCM_H_
