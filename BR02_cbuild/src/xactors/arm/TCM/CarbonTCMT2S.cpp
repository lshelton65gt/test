// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/


#include "CarbonTCMT2S.h"

#define CARBON 1
#include "TCMSlaveXtor.h"

#include <iostream>
using namespace std;

// refactor the common code from both of these ctors into a single function
CarbonTCMT2S::CarbonTCMT2S(sc_mx_module* carbonComp,
                           const char *xtorName,
                           CarbonObjectID **carbonObj,
                           CarbonPortFactory *portFactory,
                           CarbonDebugReadWriteFunction* readDebug,  // should only allow readDebug/writeDebug or debugTrans to be set
                           CarbonDebugReadWriteFunction* writeDebug,
                           CarbonDebugTransactionFunction* debugTrans)
{
  // should have assert here: assert (((debugTrans) ^ readDebug) && (debugTrans ^ writeDebug))
  CarbonDebugAccessCallbacks cb = {carbonComp, NULL, writeDebug, readDebug, debugTrans};
  TCMSlaveXtor* temp = new TCMSlaveXtor(carbonComp, xtorName, carbonObj, portFactory, cb);
  mArmAdaptor = temp;
}

CarbonTCMT2S::CarbonTCMT2S(sc_mx_module* carbonComp,
                           const char *xtorName,
                           CarbonObjectID **carbonObj,
                           CarbonPortFactory *portFactory,
                           CarbonDebugAccessFunction *debugAccess)
{
  CarbonDebugAccessCallbacks cb = {carbonComp, debugAccess, NULL, NULL, NULL};
  TCMSlaveXtor* temp = new TCMSlaveXtor(carbonComp, xtorName, carbonObj, portFactory, cb);
  mArmAdaptor = temp;
}


sc_mx_signal_slave *CarbonTCMT2S::carbonGetPort(const char *name)
{
  return ((TCMSlaveXtor *) mArmAdaptor)->carbonGetPort(name);
}

CarbonTCMT2S::~CarbonTCMT2S()
{
  delete ((TCMSlaveXtor *) mArmAdaptor);
}

void CarbonTCMT2S::init()
{
  ((TCMSlaveXtor *) mArmAdaptor)->init();
}

void CarbonTCMT2S::communicate()
{
  ((TCMSlaveXtor *) mArmAdaptor)->communicate();
}

void CarbonTCMT2S::update()
{
  ((TCMSlaveXtor *) mArmAdaptor)->update();
}

void CarbonTCMT2S::terminate()
{
  ((TCMSlaveXtor *) mArmAdaptor)->terminate();
}

void CarbonTCMT2S::reset(MxResetLevel level, const MxFileMapIF *filelist)
{
  ((TCMSlaveXtor *) mArmAdaptor)->reset(level, filelist);
}

void CarbonTCMT2S::setParameter(const string &name, const string &value)
{
  ((TCMSlaveXtor *) mArmAdaptor)->setParameter(name, value);
}

bool CarbonTCMT2S::saveData(eslapi::CASIODataStream& os)
{
  return ((TCMSlaveXtor *) mArmAdaptor)->saveData(os);
}

bool CarbonTCMT2S::restoreData(eslapi::CASIIDataStream& is)
{
  return ((TCMSlaveXtor *) mArmAdaptor)->restoreData(is);
}


