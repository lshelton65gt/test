// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "TCM.h"
#include "TCMSlaveXtor.h"

TCMSlaveXtor::TCMSlaveXtor(CASIModuleIF* c, const char* name,
                           CarbonObjectID**carbonModel, CarbonPortFactory* portFactory,
                           CarbonDebugAccessCallbacks& cb) :
  Mx_TSlave(this), owner(c), mDebugAccessCB(cb)
{
  /*
  ** get the transactor outputs from the portFactor that we were provided
  */
  raddr = portFactory(owner, name, "RADDR", carbonModel);
  rnrw = portFactory(owner, name, "RNRW", carbonModel);
  rwd = portFactory(owner, name, "RWD", carbonModel);
  ridle = portFactory(owner, name, "RIDLE", carbonModel);
  rcs = portFactory(owner, name, "RCS", carbonModel);
  rwbl = portFactory(owner, name, "RWBL", carbonModel);
  rseq = portFactory(owner, name, "RSEQ", carbonModel);

  /*
  ** Instantiate our input ports - later the component will ask us for these pointers
  **  by name
  */
  rrd_Slave = new outSig("RRD");
  rwait_Slave = new outSig("RWAIT");
  /*
  ** Create a transaction port and register it
  */
  //Mx_TSlave = new sc_port<CASITransactionIF> (owner, name);
  initTransactionPort(&Mx_TSlave);
  owner->registerPort(&Mx_TSlave, name);

}

void TCMSlaveXtor::update()
{

  if (!writeInProgress) {
    rnrw->driveSignal(0, 0);
  }
  if(!readInProgress && !writeInProgress)
  {
    rcs->driveSignal(0, 0);
  }
  writeInProgress = false;
}

void TCMSlaveXtor::communicate()
{
}

sc_mx_signal_slave *TCMSlaveXtor::carbonGetPort(const char* name)
{
  if (0 == strcmp(name, "rrd"))
  {
    return rrd_Slave;
  }
  else if (0 == strcmp(name, "rwait"))
  {
    return rwait_Slave;
  }

  else
  {
    // TODO: issue error
  }

  return 0;
}

void TCMSlaveXtor::init()
{

}

void TCMSlaveXtor::reset(eslapi::CASIResetLevel, const CASIFileMapIF *)
{
  readInProgress = false;
  writeInProgress = false;
  readDataAvail = false;

}

void TCMSlaveXtor::terminate()
{
}

void TCMSlaveXtor::setParameter(const string &name, const string &value)
{
  //CASIConvertErrorCodes status = CASIConvert_SUCCESS;


}

string TCMSlaveXtor::getName(void)
{
  return "TCMSlaveXtor";
}

void TCMSlaveXtor::initTransactionPort(CASITransactionIF* transIf)
{
  CASITransactionProperties prop;
  memset(&prop, 0, sizeof(prop));
  prop.casiVersion = eslapi::CASI_VERSION_1_1; //the transaction version used for this transaction
  prop.useMultiCycleInterface = false; // using read/write interface methods
  prop.addressBitwidth = 32; // address bitwidth used for addressing of resources
  prop.mauBitwidth = 8; // minimal addressable unit
  prop.dataBitwidth = 32; // maximum data bitwidth transferred in a single cycle
  prop.isLittleEndian = true; // alignment of MAUs
  prop.isOptional = false; // if true this port can be disabled
  prop.supportsAddressRegions = true; // M/S can negotiate address mapping
  prop.numCtrlFields = 3; // # of ctrl elements used / entry is used for transfer size
  prop.protocolID = TCM_PROTOCOL_ID; // magic number of the protocol (Vendor/Protocol-ID):
  //The upper 16 bits = protocol implementation version
  //The lower 16 bits = actual protocol ID
  sprintf(prop.protocolName, "TCM"); // The name of the  protocol being used
  // CASI : slave port address regions forwarding
  prop.isAddrRegionForwarded = false; //true if addr region for this slave port is actually
  //forwarded to a master port, false otherwise
  prop.forwardAddrRegionToMasterPort = NULL; //master port of the  same component to which this slave port's addr region is forwarded
  transIf->setProperties(&prop);
}

eslapi::CASIStatus TCMSlaveXtor::read(uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  eslapi::CASIStatus returnStatus = eslapi::CASI_STATUS_ERROR;

  // Figure out which state we are in and process the read
  TCMPhases phase = (TCMPhases)ctrl[1];
  switch(phase) {
    case TCM_CYCLE_ADDR:
      // Drive the address phase
      raddr->driveSignal(addr/4, 0);
      rnrw->driveSignal(0, 0);
      rwd->driveSignal(*value, 0);
      ridle->driveSignal(0, 0);
      rcs->driveSignal(1, 0);
      rwbl->driveSignal(0xF, 0);
      rseq->driveSignal(0, 0);
      returnStatus = eslapi::CASI_STATUS_OK;
      readInProgress = true;
      break;

    case TCM_CYCLE_DATA:
      // This is not the last data phase so set the seq signal
      raddr->driveSignal(addr/4, 0);
      rseq->driveSignal(1,0);
      if (rwait_Slave->readSignal() == 0) {
        *value = rrd_Slave->readSignal();
        returnStatus = eslapi::CASI_STATUS_OK;
      } else {
        returnStatus = eslapi::CASI_STATUS_WAIT;
      }
      readInProgress = true;
      break;

    case TCM_CYCLE_DATA_LAST:
      // This is the last data phase so clear the seq signal
      rseq->driveSignal(0,0);
      if (rwait_Slave->readSignal() == 0) {
        *value = rrd_Slave->readSignal();
        returnStatus = eslapi::CASI_STATUS_OK;
      } else {
        returnStatus = eslapi::CASI_STATUS_WAIT;
      }
      readInProgress = false;
      break;

    default:
      break;
  } // switch

  return returnStatus;
}
eslapi::CASIStatus TCMSlaveXtor::write(uint64_t addr, uint32_t* value,
                                       uint32_t* ctrl)
{
  // Treat writes like a single cycle
  raddr->driveSignal(addr/4, 0);
  rnrw->driveSignal(1, 0);
  rwd->driveSignal(*value, 0);
  ridle->driveSignal(0, 0);
  rcs->driveSignal(1, 0);
  rwbl->driveSignal(ctrl[0] & 0xF, 0);
  rseq->driveSignal(0, 0);
  writeInProgress = true;
  if (rwait_Slave->readSignal() == 1)
  {
    return eslapi::CASI_STATUS_WAIT;
  }
  else
  {
    return eslapi::CASI_STATUS_OK;
  }
}



TCMSlaveXtor::~TCMSlaveXtor()
{
  delete rnrw;
  delete rwd;
  delete ridle;
  delete rcs;
  delete rwbl;
  delete rseq;
//delete Mx_TSlave;
}

bool TCMSlaveXtor::saveData(eslapi::CASIODataStream& os)
{
  os << addr << data << ctrl << writeInProgress << readInProgress << readDataAvail;
  return true;
}

bool TCMSlaveXtor::restoreData(eslapi::CASIIDataStream& is)
{
  is >> addr >> data >> ctrl >> writeInProgress >> readInProgress >> readDataAvail;
  return true;
}
