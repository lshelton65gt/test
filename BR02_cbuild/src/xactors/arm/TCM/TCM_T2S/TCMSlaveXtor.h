// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _TCMSLAVEXTOR_H_
#define _TCMSLAVEXTOR_H_

// required for SoC Designer classes including
// CASISignalSlave and CASITransactionIF
#include "maxsimCompatibility.h"

// required for CarbonPortFactory
#include "xactors/arm/include/carbon_arm_adaptor.h"

// required for CarbonObjectID
#include "carbon/carbon_capi.h"

// Transactor specific derivative of CASISignalSlave
#include "outSig.h"
#include "TCM_TS.h"

class TCMSlaveXtor
{
    /*
    ** Transactor outputs that connect to RTL inputs
    */
    outSig *rrd_Slave;        // read data
    outSig *rwait_Slave;      // wait

    /*
    ** Transactor inputs that connect to RTL outputs
    */


    CarbonXtorAdaptorToVhmPort* raddr;     // address
    CarbonXtorAdaptorToVhmPort* rnrw;      // read/write
    CarbonXtorAdaptorToVhmPort* rwd;      // write data
    CarbonXtorAdaptorToVhmPort* ridle;     // idle cycle
    CarbonXtorAdaptorToVhmPort* rcs;      // chip select
    CarbonXtorAdaptorToVhmPort* rwbl;      // byte enables for write data
    CarbonXtorAdaptorToVhmPort* rseq;      // sequential data access

    /*
    ** The transaction port that we'll create on the component
    */

    TCM_TS Mx_TSlave;
    void initTransactionPort(CASITransactionIF* transIf);  // init code for the port

public:
    /*
    ** the following functions are the require interface for a transactor.
    */
    TCMSlaveXtor(CASIModuleIF*, const char* s, CarbonObjectID**, CarbonPortFactory*, 
                 CarbonDebugAccessCallbacks& cb);
    virtual ~TCMSlaveXtor();

    void communicate();
    void update();

    string getName();
    void setParameter(const string &name, const string &value);

    void init();
    void terminate();
    void reset(eslapi::CASIResetLevel level, const CASIFileMapIF *filelist);

    sc_mx_signal_slave *carbonGetPort(const char *name);

    eslapi::CASIStatus read(uint64_t addr, uint32_t* value, uint32_t* ctrl);
    eslapi::CASIStatus write(uint64_t addr, uint32_t* value, uint32_t* ctrl);

    const CarbonDebugAccessCallbacks& getDebugAccessCB() const { return mDebugAccessCB; }

    bool saveData(eslapi::CASIODataStream& os);
    bool restoreData(eslapi::CASIIDataStream& is);

private:
    CASIModuleIF* owner;
    CarbonDebugAccessCallbacks mDebugAccessCB;

    uint32_t addr, data, ctrl;

    //bool writeInProgress;
    bool writeInProgress;
    bool readInProgress;
    bool readDataAvail;
};

#endif // _TCMSLAVEXTOR_H_
