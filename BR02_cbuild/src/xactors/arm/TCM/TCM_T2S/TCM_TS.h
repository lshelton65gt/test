// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#ifndef _TCM_TS_H_
#define _TCM_TS_H_

#include "maxsimCompatibility.h"


class TCMSlaveXtor;

class TCM_TS: public CASITransactionSlave
{
    TCMSlaveXtor *owner;

    //Memory Map Editor Support
    CASIMemoryMapConstraints puMemoryMapConstraints;

public:
  TCM_TS( TCMSlaveXtor *_owner );
    virtual ~TCM_TS() {}

    // TBD: Is this always constant?
    static const uint32_t scBusWidth = 32;

public:
    /* Synchronous access functions */
    virtual eslapi::CASIStatus read(uint64_t addr, uint32_t* value, uint32_t* ctrl);
    virtual eslapi::CASIStatus write(uint64_t addr, uint32_t* value, uint32_t* ctrl);
    virtual eslapi::CASIStatus readDbg(uint64_t addr, uint32_t* value, uint32_t* ctrl);
    virtual eslapi::CASIStatus writeDbg(uint64_t addr, uint32_t* value, uint32_t* ctrl);

    /* Asynchronous access functions */
    virtual eslapi::CASIStatus readReq(uint64_t addr, uint32_t* value, uint32_t* ctrl,
                             CASITransactionCallbackIF* callback);
    virtual eslapi::CASIStatus writeReq(uint64_t addr, uint32_t* value, uint32_t* ctrl,
                              CASITransactionCallbackIF* callback);

    /* Arbitration functions */
    virtual eslapi::CASIGrant requestAccess(uint64_t addr);
    virtual eslapi::CASIGrant checkForGrant(uint64_t addr);
    
             /* CASI : new shared-memory based asynchronous transaction functions */
             virtual void driveTransaction(CASITransactionInfo* info);
             virtual void cancelTransaction(CASITransactionInfo* info);
             virtual eslapi::CASIStatus debugTransaction(CASITransactionInfo* info);

    /* Memory map functions */
    virtual int getNumRegions();
    virtual void getAddressRegions(uint64_t* start, uint64_t* size, string* name);

    /* CASI : now the address regions can be set from the outside! */
    virtual void setAddressRegions(uint64_t* start, uint64_t* size, string* name);
    virtual CASIMemoryMapConstraints* getMappingConstraints();

    
};

#endif // _TCM_TS_H_
