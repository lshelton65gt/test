// -*-C++-*-
/*****************************************************************************
  Copyright (c) 2010 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS PROPRIETARY, CONFIDENTIAL AND TRADE SECRET
  INFORMATION OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION ,
  COPYING AND/OR DISTRIBUTION OF THIS FILE IS PROHIBITED WITHOUT THE
  EXPRESS WRITTEN CONSENT OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

/*!
  \file
  TBD
*/

#include "TCM.h"
#include "TCM_TS.h"
#include "TCMSlaveXtor.h"


TCM_TS::TCM_TS( TCMSlaveXtor *_owner ) : CASITransactionSlave( "TCM_TS" )

{
	owner = _owner;
	setCASIOwner(dynamic_cast<eslapi::CASIModuleIF*>(owner));
             CASITransactionProperties prop;
             memset(&prop,0,sizeof(prop));
             prop.casiVersion = eslapi::CASI_VERSION_1_1;     //the transaction version used for this transaction
    prop.useMultiCycleInterface = false;    // using read/write interface methods
    prop.addressBitwidth = 32;      // address bitwidth used for addressing of resources
    prop.mauBitwidth = 8;           // minimal addressable unit
    prop.dataBitwidth = scBusWidth; // maximum data bitwidth transferred in a single cycle
    prop.isLittleEndian = true;     // alignment of MAUs
    prop.isOptional = false;                                // if true this port can be disabled
    prop.supportsAddressRegions = true;     // M/S can negotiate address mapping
    prop.numCtrlFields = 1;         // # of ctrl elements used / entry is used for transfer size
    prop.protocolID = TCM_PROTOCOL_ID;           // magic number of the protocol (Vendor/Protocol-ID):
    //The upper 16 bits = protocol implementation version
    
      //The lower 16 bits = actual protocol ID
      sprintf(prop.protocolName,"TCM");         // The name of the  protocol being used
      // CASI : slave port address regions forwarding
      prop.isAddrRegionForwarded = false; //true if addr region for this slave port is actually
    
      //forwarded to a master port, false otherwise
      prop.forwardAddrRegionToMasterPort = NULL; //master port of the  same component to which this slave port's addr region is forwarded
    
      setProperties(&prop);

	
	
    // Memory Map Support. TODO Add Memory Map Constraints for Slave Port
    puMemoryMapConstraints.minRegionSize = 0x1000;
    puMemoryMapConstraints.maxRegionSize = CASIU64CONST(0x100000000);
    puMemoryMapConstraints.minAddress = 0x0;
    puMemoryMapConstraints.maxAddress = CASIU64CONST(0xffffffff);
    puMemoryMapConstraints.minNumSupportedRegions = 0x1;
    puMemoryMapConstraints.maxNumSupportedRegions = 0x1;
    puMemoryMapConstraints.alignmentBlockSize = 0x400 ; //the min block size where this slave's regions can be mapped 
    puMemoryMapConstraints.numFixedRegions = 0;
    puMemoryMapConstraints.fixedRegionList = NULL;

	// TODO:  Add any additional constructor code here.
}

/* If no regions are defined, numRegions returns 0, indicating that the
 * port wants to be called for any uint64_t address */
int
TCM_TS::getNumRegions()
{

    // TODO:  Add your code here.
  return 0;
}

/* This function is also not pure virtual, so that the user
 * does not need to implement it if she/he doesn't want to use
 * address mapping */
void
TCM_TS::getAddressRegions(uint64_t* start, uint64_t* size, string* name)
{

    // TODO: Add your code here.

}

/* Method to set the address regions */
void
TCM_TS::setAddressRegions(uint64_t* start, uint64_t* size, string* name)
{
    //Memory Map Editor Support
    if (start && size && name)
    {
        int i = 0;
	     while (size[i] != 0 )
        {
           //TODO: Add your code here
           pmessage(eslapi::CASI_MSG_INFO, "Address Region: start= 0x%I64x size = 0x%I64x Name= %s",
           start[i], size[i],
           name[i].c_str());
		    i++;
        }
    }

}

CASIMemoryMapConstraints*
TCM_TS::getMappingConstraints()
{
    return &puMemoryMapConstraints;

}

eslapi::CASIStatus
TCM_TS::read(uint64_t addr, uint32_t* value, uint32_t* ctrl)
{

    return owner->read(addr, value, ctrl);
           //return eslapi::CASI_STATUS_NOTSUPPORTED;

}

eslapi::CASIStatus
TCM_TS::write(uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
   return owner->write(addr, value, ctrl);
    // TODO: Add your code here.
             //return eslapi::CASI_STATUS_NOTSUPPORTED;

}

eslapi::CASIStatus
TCM_TS::readDbg(uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // Use the common debug access function.
  const CarbonDebugAccessCallbacks& cb = owner->getDebugAccessCB();
  eslapi::CASIStatus status = CarbonDebugFunctionsToDebugAccess(cb, eCarbonDebugAccessRead, scBusWidth, addr, value, ctrl);
  return status;
}

eslapi::CASIStatus
TCM_TS::writeDbg(uint64_t addr, uint32_t* value, uint32_t* ctrl)
{
  // Use the common debug access function.
  const CarbonDebugAccessCallbacks& cb = owner->getDebugAccessCB();
  eslapi::CASIStatus status = CarbonDebugFunctionsToDebugAccess(cb, eCarbonDebugAccessWrite, scBusWidth, addr, value, ctrl);
  return status;
}

eslapi::CASIStatus
TCM_TS::readReq(uint64_t addr, uint32_t* value, uint32_t* ctrl, CASITransactionCallbackIF* callback)
{
	UNUSEDARG(addr);
	UNUSEDARG(value);
	UNUSEDARG(ctrl);
	UNUSEDARG(callback);
    // TODO: Add your code here.

    return eslapi::CASI_STATUS_NOTSUPPORTED;
}

eslapi::CASIStatus
TCM_TS::writeReq(uint64_t addr, uint32_t* value, uint32_t* ctrl, CASITransactionCallbackIF* callback)
{
	UNUSEDARG(addr);
	UNUSEDARG(value);
	UNUSEDARG(ctrl);
	UNUSEDARG(callback);
    // TODO: Add your code here.

    return eslapi::CASI_STATUS_NOTSUPPORTED;
}

eslapi::CASIGrant
TCM_TS::requestAccess(uint64_t addr)
{
	UNUSEDARG(addr);
    // TODO: Add your code here.

    return eslapi::CASI_GRANT_NOTSUPPORTED;
}

eslapi::CASIGrant
TCM_TS::checkForGrant(uint64_t addr)
{
	UNUSEDARG(addr);
    // TODO: Add your code here.

    return eslapi::CASI_GRANT_NOTSUPPORTED;
}

/* shared-memory based asynchronous transaction functions */
void
TCM_TS::driveTransaction(CASITransactionInfo* info)
{
	UNUSEDARG(info);
	// TODO: Add your code here.

}

void
TCM_TS::cancelTransaction(CASITransactionInfo* info)
{
	UNUSEDARG(info);
	// TODO: Add your code here.

}

eslapi::CASIStatus
TCM_TS::debugTransaction(CASITransactionInfo* info)
{
  // The master transactor calls readDbg()/writeDbg() directly, so
  // this currently isn't used, but the implementation is trivial.
  const CarbonDebugAccessCallbacks& cb = owner->getDebugAccessCB();
  eslapi::CASIStatus status = CarbonDebugFunctionsToDebugAccess(cb, info);
  return status;
}
