//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005-2007 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_USR_H
#define TBP_USR_H

#ifndef __carbon_capi_h_
#include "carbon/carbon_capi.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \brief Connect transactor signal by name to Carbon Model signal by name
 *
 * This structure will describe mapping between transactor signal name to Carbon
 * Model signal name. When this is specified to the connect API of a
 * transactor, to access a signal, transactor will access the corresponding
 * Carbon model signal.
 */
typedef struct {
    const char *TransactorSignalName;          /*!< Transactor signal name */
    const char *ModelSignalName;               /*!< Carbon model signal name */
} CarbonXInterconnectNameNamePair;

/*!
 * \brief Connect transactor signal by name to Carbon Model signal by CarbonNetID
 *
 * This structure will describe mapping between transactor signal name to Carbon
 * Model signal handle. When this is specified to the connect API of a
 * transactor, to access a signal, transactor will access the corresponding
 * Carbon Model signal handle.
 */
typedef struct {
    const char *TransactorSignalName;          /*!< Transactor signal name */
    CarbonNetID *ModelSignalHandle;            /*!< Carbon Model signal handle */
} CarbonXInterconnectNameHandlePair;

/*!
 * \brief Model input/output signal change callback type
 *
 * This type specifies the model signal change callback function pointer type.
 * First parameter will hold a pointer to \a CarbonUInt32, which will be
 * considered as value to be deposited for output type of signal and as
 * value after the signal is examined for input type. The second parameter
 * will be used to pass the user data specified as \a UserPointer in
 * CarbonXInterconnectNameCallbackTrio.
 */
typedef CarbonStatus(*CarbonXSignalChangeCB)(CarbonUInt32*, void*);

/*!
 * \brief Connect transactor signal by name to input/output change callbacks
 *
 * This structure will describe mapping between transactor signal name to
 * model signal callbacks. When this is specified to the connect API of a
 * transactor, to access a signal, transactor will call the corresponding
 * callbacks.
 */
typedef struct {
    const char *TransactorSignalName;          /*!< Transactor signal name */
    CarbonXSignalChangeCB ModelInputChangeCB;  /*!< Outputs of transactor,
                  called during refresh when value changes (xactor pushes) */
    CarbonXSignalChangeCB ModelOutputChangeCB; /*!< Inputs to transactor,
                  called during refresh always (xactor pulls) */
    void *UserPointer;                         /*!< User data to be passed in
                                                    CarbonXSignalChangeCB */
} CarbonXInterconnectNameCallbackTrio;

  // For Backwards compatibility
typedef CarbonUInt8  u_int8;
typedef CarbonUInt16 u_int16;
typedef CarbonUInt32 u_int32;
typedef CarbonSInt8  s_int8;
typedef CarbonSInt16 s_int16;
typedef CarbonSInt32 s_int32;

typedef CarbonUInt64 u_int64;
typedef CarbonSInt64 s_int64;

#ifndef TBP_UTILS_H
#define TBP_WIDTH_8        0x01
#define TBP_WIDTH_16       0x02
#define TBP_WIDTH_32       0x04
#define TBP_WIDTH_64       0x08
#define TBP_WIDTH_64P      0x18
#define TBP_NOP     	1
#define TBP_IDLE    	2
#define TBP_SLEEP   	3
#define TBP_READ    	4
#define TBP_WRITE   	5
#define TBP_CS_READ 	6
#define TBP_CS_WRITE	7
#define TBP_NXTREQ      8
#define TBP_INT_DONE    9
#define TBP_FREAD       10
#define TBP_FWRITE      11
#define TBP_WAKEUP      12
#define TBP_CS_RMW      13           // Read Modify Write
#define TBP_RESET       14           // Read Modify Write
#define TBP_USER_OP     TBP_RESET+1  // user-defined opcodes, MUST be larger codes
#endif

#define TRUE 1
#define FALSE 0
typedef int BOOL;
typedef CarbonUInt64 simt_t;

#ifndef __GNUC__   /* gnu-compiler-specific macros */
#define __PRETTY_FUNCTION__ "<unknown>" 
  #ifndef WIN32
   #define __FUNCTION__        "<unknown>" 
  #endif
#endif


  // Transaction Structures for Request/Response type transactions
struct carbonXTransactionStruct {
  CarbonUInt32  mTransId;       // Transaction ID
  CarbonUInt32  mRepeatCnt;     // Number of times the transaction should be executed
  CarbonUInt32  mOpcode;        // read/write etc..
  CarbonUInt64  mAddress;       // 64bit operation address
  CarbonUInt8 * mData;          // data being written to the hdl
  CarbonUInt32  mSize;          // size in "bytes"
  CarbonUInt32  mStatus;        // user/bfm defined
  CarbonUInt32  mWidth;         // 1/2/4/8 byte data width
  CarbonUInt32  mInterrupt;     // Interupt from Simulation (for response only)
  CarbonUInt32  mHasData;       // Flag to tell wether structure holds data. If it does, size is mSize.
  CarbonUInt64  mStartTime;     // starttime of transaction (for response only)
  CarbonUInt64  mEndTime;       // endtime of transation (for response only)
  CarbonUInt32  mBufferSize;    // Size of allocated Data Buffer
};

//    TOP LEVEL Routines

#define TBP_FFL (char *) __FILE__, (char *) __PRETTY_FUNCTION__, (int) __LINE__
#define FFL_ARGS char *file, char *func, int line

/* Macro to attach a threaded function to a 'hdl' transactor */
#define carbonXAttach setTbpPathInfo(TBP_FFL), tbpi_new_path_attach
#define carbonXGenXtorHandle(_model_, _path_) tbp_gen_unique_handle((void*)_model_, _path_)
typedef void (*CarbonXAttachFuncT)();

//  SIM Transactions

#define carbonXIdle(cycles)	tbpi_sim_idle(cycles,TBP_FFL)
#define carbonXResetTrans()     tbpi_sim_reset(TBP_FFL)

/* Macros to support write/read to transactors */
#define carbonXWrite8(a,d)	   tbpi_sim_write8 ((CarbonUInt64)a,d,TBP_FFL)
#define carbonXWrite16(a,d)	   tbpi_sim_write16((CarbonUInt64)a,d,TBP_FFL)
#define carbonXWrite32(a,d)	   tbpi_sim_write32((CarbonUInt64)a,d,TBP_FFL)
#define carbonXWrite64(a,d)        tbpi_sim_write64((CarbonUInt64)a,d,TBP_FFL)
#define carbonXWriteArray8(a,d,s)  tbpi_sim_write  ((CarbonUInt64)a,d,s,TBP_WIDTH_8,TBP_FFL)
#define carbonXWriteArray16(a,d,s) tbpi_sim_write  ((CarbonUInt64)a,d,s,TBP_WIDTH_16,TBP_FFL)
#define carbonXWriteArray32(a,d,s) tbpi_sim_write  ((CarbonUInt64)a,d,s,TBP_WIDTH_32,TBP_FFL)
#define carbonXWriteArray64(a,d,s) tbpi_sim_write  ((CarbonUInt64)a,d,s,TBP_WIDTH_64,TBP_FFL)
  /* carbonXWrite takes an array of 32-bit words.  This must remain this way because
     the current test suites assume 32-bit arrays.  */
#define carbonXWrite(a,d,s)	tbpi_sim_write  ((CarbonUInt64)a,(CarbonUInt32*)d,s,TBP_WIDTH_32,TBP_FFL)
#define carbonXWriteZX(addr,data,datax,size)  tbpi_sim_write_zx (addr,data,datax,size,TBP_WIDTH_32,TBP_FFL)

#define carbonXRead8(a)	           tbpi_sim_read8  ((CarbonUInt64)a,TBP_FFL)
#define carbonXRead16(a)           tbpi_sim_read16 ((CarbonUInt64)a,TBP_FFL)
#define carbonXRead32(a)           tbpi_sim_read32 ((CarbonUInt64)a,TBP_FFL)
#define carbonXRead64(a)           tbpi_sim_read64 ((CarbonUInt64)a,TBP_FFL)
#define carbonXReadArray8(a,d,s)   tbpi_sim_read   ((CarbonUInt64)a,d,s,TBP_WIDTH_8,TBP_FFL)
#define carbonXReadArray16(a,d,s)  tbpi_sim_read   ((CarbonUInt64)a,d,s,TBP_WIDTH_16,TBP_FFL)
#define carbonXReadArray32(a,d,s)  tbpi_sim_read   ((CarbonUInt64)a,d,s,TBP_WIDTH_32,TBP_FFL)
#define carbonXReadArray64(a,d,s)  tbpi_sim_read   ((CarbonUInt64)a,d,s,TBP_WIDTH_64,TBP_FFL)
  /* carbonXRead fills an array of 32-bit words.  This must remain this way because
     the current test suites assume 32-bit arrays.  */
#define carbonXRead(a,d,s) tbpi_sim_read   ((CarbonUInt64)a,d,s,TBP_WIDTH_32,TBP_FFL)
#define carbonXReadZX(addr,data,datax,size)   tbpi_sim_read_zx  (addr,data,datax,size,TBP_WIDTH_32,TBP_FFL)

#define carbonXCsRead(a)    tbpi_cpf_cs_read ((CarbonUInt64)a,TBP_FFL)
#define carbonXCsWrite(a,d) tbpi_cpf_cs_write((CarbonUInt64)a,(CarbonUInt64)d,TBP_FFL)

/* Return the status after the previous transaction */
#define carbonXGetStatus()         tbp_sim_status();

/* Set interrupt handler function */
#define carbonXSetInterruptHandler(i) tbpi_cpf_set_interrupt_handler((void *)i, (char *)#i, TBP_FFL)

#define carbonXTestCleanup tbpi_test_cleanup

void    tbpi_sim_write8  (CarbonUInt64 addr, CarbonUInt8  data, FFL_ARGS);
void    tbpi_sim_write16 (CarbonUInt64 addr, CarbonUInt16 data, FFL_ARGS);
void    tbpi_sim_write32 (CarbonUInt64 addr, CarbonUInt32 data, FFL_ARGS);
void    tbpi_sim_write64 (CarbonUInt64 addr, CarbonUInt64 data, FFL_ARGS);

CarbonUInt8  tbpi_sim_read8   (CarbonUInt64 addr,               FFL_ARGS);
CarbonUInt16 tbpi_sim_read16  (CarbonUInt64 addr,               FFL_ARGS);
CarbonUInt32 tbpi_sim_read32  (CarbonUInt64 addr,               FFL_ARGS);
CarbonUInt64 tbpi_sim_read64  (CarbonUInt64 addr,               FFL_ARGS);

/*  Note that these 4 return the actual number of bytes successfully transferred */
CarbonUInt32 tbpi_sim_write(CarbonUInt64 addr, void *data, CarbonUInt32 size, CarbonUInt32 width, FFL_ARGS);
CarbonUInt32 tbpi_sim_read(CarbonUInt64 addr, void *data, CarbonUInt32 size, CarbonUInt32 width, FFL_ARGS);
CarbonUInt32 tbpi_sim_write_zx(CarbonUInt64 addr, void *data, void *datax, CarbonUInt32 *size, CarbonUInt32 width, FFL_ARGS);
CarbonUInt32 tbpi_sim_read_zx(CarbonUInt64 addr, void *data, void *datax, CarbonUInt32 *size, CarbonUInt32 width, FFL_ARGS);

CarbonUInt32 tbpi_cpf_cs_read (CarbonUInt64 addr, FFL_ARGS);
void    tbpi_cpf_cs_write(CarbonUInt64 addr, CarbonUInt32 data, FFL_ARGS);

/* user defined operation */
void tbpi_sim_userop(CarbonUInt32 opcode, CarbonUInt64 addr, CarbonUInt64 *data, CarbonUInt64 *datax, 
		     CarbonUInt32 *size, CarbonUInt32 *status, FFL_ARGS);

/* function to return the status of the previous operation */
CarbonUInt32 tbp_sim_status(void);

/* function used to assign an interrupt handler to a given thread */
void tbpi_sim_set_interrupt_handler(CarbonUInt32 id, void (*int_func), FFL_ARGS);

/* function used to assign an reset handler to a given thread id */
void tbpi_sim_set_reset_handler(CarbonUInt32 id, void (*rst_func), FFL_ARGS);

void setTbpPathInfo(char* _file, char* _func, int _line);
int tbpi_new_path_attach(const char *path, void (*func_ptr)(), CarbonUInt32 num_args, ...);
char *tbp_gen_unique_handle(const void *obj_ptr, const char *str);

void tbpi_test_cleanup();

//    EVENTS
// For use by the top level routine only
#define carbonXSetWaitTest(sx,to)          tbpi_evt_set_waittest   (sx,     to, TBP_FFL)
#define carbonXCheckWait                   tbp_evt_check_my_thread_wait

#define carbonXSetSignal(sx)               tbpi_evt_signal     (sx,         TBP_FFL)
#define carbonXWaitSignal(sx, to)          tbpi_evt_wait       (sx,     to, TBP_FFL)
#define carbonXSetSignalState(sx, st)      tbpi_evt_signalstate(sx, st,     TBP_FFL)
#define carbonXWaitSignalState(sx, st, to) tbpi_evt_waitstate  (sx, st, to, TBP_FFL)
#define carbonXCheckSignalState(sx)        tbpi_evt_checkstate (sx,         TBP_FFL)

#define carbonXSleep(to)		tbpi_sim_sleep(to,   TBP_FFL)


void tbpi_evt_signal       (const char *event,                             FFL_ARGS);
void tbpi_evt_signalstate  (const char *event, int state,                  FFL_ARGS);
int  tbpi_evt_wait         (const char *event,            CarbonSInt64 timeout, FFL_ARGS);
int  tbpi_evt_waitstate    (const char *event, int state, CarbonSInt64 timeout, FFL_ARGS);
int  tbpi_evt_checkstate   (const char *event,                             FFL_ARGS);
void tbpi_evt_set_waittest (const char *event,            CarbonSInt64 timeout, FFL_ARGS);
int  tbp_evt_check_my_thread_wait(void);

void tbpi_sim_idle (CarbonUInt32 idle_cnt, FFL_ARGS);	/* function to idle N cycles */
void tbpi_sim_sleep(CarbonUInt64 timeout,  FFL_ARGS);	/* function to idle until timeout */
void tbpi_sim_reset(FFL_ARGS);                          /* function to reset transactor */

#define TEST_COMPLETE "test_complete"


//    MESSAGES

/* main messaging function determines whhich messages are printed to which locations */ 
void setTbpMsgInfo(const char* _file, const char* _func, int _line, const char* _group, int _level);
int tbp_msg(const char *file, const char *func, int line, const char *group, int level, ...);
int tbp_new_msg(const char *fmt, ...);
int tbp_new_dmsg(const char* group, int level, const char *fmt,  ...);
int tbp_new_pdmsg( int level, const char *fmt, ...);

#define TBP_MSG_PRINTF_GRP "tbp_msg_printf_dbg"
#define TBP_MSG_INFO_GRP   "tbp_msg_info_dbg"
#define TBP_MSG_DEBUG_GRP  "tbp_msg_debug_dbg"
#define TBP_MSG_WARN_GRP   "tbp_msg_warn_dbg"
#define TBP_MSG_ERROR_GRP  "tbp_msg_error_dbg"
#define TBP_MSG_PANIC_GRP  "tbp_msg_panic_dbg"

/*
 * define some built-in levels
 */
#define MSG_LVL_FORCE -1
#define MSG_LVL_INFO   0
#define MSG_LVL_DEBUG 10
#define MSG_LVL_TRACE 40

/* definition for use with tbp_message */
#define TBP_PRINTF TBP_FFL, TBP_MSG_PRINTF_GRP, MSG_LVL_FORCE
#define TBP_INFO   TBP_FFL, TBP_MSG_INFO_GRP,   MSG_LVL_FORCE
#define TBP_WARN   TBP_FFL, TBP_MSG_WARN_GRP,   MSG_LVL_FORCE
#define TBP_ERROR  TBP_FFL, TBP_MSG_ERROR_GRP,  MSG_LVL_FORCE
#define TBP_PANIC  TBP_FFL, TBP_MSG_PANIC_GRP,  MSG_LVL_FORCE
#define TBP_DEBUG  TBP_FFL, TBP_MSG_DEBUG_GRP,  MSG_LVL_FORCE

#define MSG_Panic      setTbpMsgInfo(TBP_PANIC),  tbp_new_msg
#define MSG_Error      setTbpMsgInfo(TBP_ERROR),  tbp_new_msg
#define MSG_Warn       setTbpMsgInfo(TBP_WARN),   tbp_new_msg
#define MSG_Milestone  setTbpMsgInfo(TBP_INFO),   tbp_new_msg
#define MSG_Printf     setTbpMsgInfo(TBP_PRINTF), tbp_new_msg
#define MSG_Debug      setTbpMsgInfo(TBP_DEBUG),  tbp_new_msg
#define MSG_PrintfDebug     setTbpMsgInfo(TBP_FFL, TBP_MSG_PRINTF_GRP, 0),  tbp_new_pdmsg
#define MSG_LibDebug        setTbpMsgInfo(TBP_DEBUG),     tbp_new_dmsg 

/*
 *   Routines to manipulate the operation of the MSG Features
 */
void tbp_set_max_error_count  (int maxerr);	/* Set trigger points to stop test */
void tbp_set_max_warning_count(int maxwarn);

int  tbp_get_error_count    ();			/* Query error counts */
int  tbp_get_c_error_count  ();
int  tbp_get_sim_error_count(); 
int  tbp_get_warning_count  ();

void tbp_flush_logfile(void);			/* function to actually flush the log file */

BOOL tbp_get_suppress_warnings(void);		/* set/get suppress warnings state */
void tbp_set_suppress_warnings(BOOL flag);


void tbp_set_debug_level    (int debug_level);	/* set the screen debug level */
void tbp_set_log_debug_level(int debug_level);	/* set the log debug level */

int  tbp_get_debug_level    (void);		/* get the screen debug level */
int  tbp_get_log_debug_level(void);   		/* get the log debug level */

void tbp_add_debug_group  (char *group);	/* Manipulate debug groups */
int  tbp_del_debug_group  (char *group);
void tbp_list_debug_groups(void);

void tbp_set_sim_screen_print(BOOL print);	/* Turn printing on and off */
void tbp_set_sim_log_print   (BOOL print);

CarbonUInt64 tbp_get_sim_time(void);

void carbonXReqRespTransaction(const struct carbonXTransactionStruct *req, struct carbonXTransactionStruct *resp);
void carbonXGetTransaction(struct carbonXTransactionStruct *trans);
void carbonXPutTransaction(struct carbonXTransactionStruct *trans);
void carbonXInitTransactionStruct(struct carbonXTransactionStruct *obj, CarbonUInt32 buf_size);
void carbonXDestroyTransactionStruct(struct carbonXTransactionStruct *obj);
void carbonXCopyTransactionStruct(struct carbonXTransactionStruct *dest, const struct carbonXTransactionStruct *src);
void carbonXReallocateTransactionBuffer(struct carbonXTransactionStruct *trans);
CarbonUInt32 carbonXCalculateActualTransactionSize(const struct carbonXTransactionStruct *trans);  
void   carbonXSetTransaction(struct carbonXTransactionStruct *trans,
			     CarbonUInt32 trans_id,
			     CarbonUInt32 opcode, CarbonUInt64 address, const void * data, 
			     CarbonUInt32 size,   CarbonUInt32 status,  CarbonUInt32 width);

#define carbonXGetSimTime          tbp_get_sim_time

#ifdef __cplusplus
}
#endif

#endif
