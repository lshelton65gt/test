
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//*****************************************************************************
//  Description:  Synthesizable Ethernet Receiver BFM and attaches to the GMII Tx
//		  interface of the DUT.This test runs in Full mode and 10Mb/s. 
//                 This transactor module is a C-side initiated transactor and consist  
//		   of XIF_core and BFM. The rx transactor receives configuration data
//		   from XIF_core first to set up the duplex, gigamode, clk_10, clk_100
//                 tbi and rmii mode. Then it receives the data from the MAC.
//		   The transactor then process the incoming flits of data and generates
//		   different frame or transmission and collision error signals. 
//*******************************************************************************

// 
// * CRS, COL valid only in case of half_duplex operation. The function
// of these pins is undefined for full_duplex operation
// ----------------------------------------------------------------------


`timescale 1ns / 100 ps

module  carbonx_enetgmii_rx
  (sys_clk,   
   sys_reset_l,
   TXD,      
   TX_ER, 
   TX_EN, 
   CRS, 
   COL, 
   TX_CLK 
   );

  // GMII signals
  input        sys_clk, sys_reset_l;
  input [7:0]  TXD;
  input        TX_ER;
  input        TX_EN;
  input        TX_CLK;
  output       CRS;
  output       COL;

`protect
// carbon license crbn_vsp_exec_xtor_e1g
   
//**************************************************************************
//                      Parameter Definition
//**************************************************************************
  parameter    
	       BFMid     =  1,
	       ClockId   =  1,	 // Which clock I am connected to.
	       DataWidth = 32,
	       DataAdWid = 11,   // Max is 15
	       GCSwid    = 64,  // Max is 256 bytes
	       PCSwid    = 16;  // Max is 256 bytes

  parameter    IDLE         = 3'h0;
  parameter    GET_PREAM_H  = 3'h1;
  parameter    GET_PREAM_L  = 3'h2;
  parameter    GET_DATA     = 3'h3;
  parameter    CRS_EXTEND   = 3'h4;

//***************************************************************************
//                      Include C Interface File
//***************************************************************************  

`include "xif_core.vh"        // Use the transactor template

//  File Name - carbonx_enetgmii_rx.v 
//  Author    - Rajat Mohan
//  Date      - March 10th 2003
// 
   
// ----------------------------------------------------------------------
//  Register definition
//  get_csdata[31:0]   -> rx_config
//  get_csdata[63:32]  -> err_insertion
//  put_csdata[31:0]   <- rx_err_status
//
//  err_insertion[14:0]  -> loss_crs_byte_num 
//  err_insertion[15]    -> force_loss_crs
//  err_insertion[30:16] -> col_byte_num
//  err_insertion[31]    -> force_col
//
//  rx_config[0]     -> sel_clk_10     
//  rx_config[1]     -> sel_clk_100 --> (_config[1:0]==00) -> gigmode    
//  rx_config[2]     -> half_duplex    
//  rx_config[3]     -> fcs_enable
//  rx_config[4]     -> rmii_mode 	// Reserved for future  
//  rx_config[5]     -> rgmii_mode	// Reserved for future   
//  rx_config[6]     -> tbi_en   	// Reserved for future 
//  rx_config[7]     -> rtbi_en		// Reserved for future       

 //**************************************************************************
 //                      Internal Reg/Wire Signals 
 //************************************************************************** 
  reg [DataAdWid -1:0] gp_daddr, gp_daddr_clkd;
  reg [2:0] 	       curr_state;
  reg 		       CRS;

  reg [31:0] 	       txd_int;
  reg 		       flag_crs_err_extend, flag_crs_extend; 
  reg [15:0] 	       tx_ipg;
  reg [31:0] 	       got_fcs; 

  reg [2:0] 	       nxt_state;
  reg 		       BFM_xrun_int;
  wire [15:0] 	       rx_err_status; // report back err status to C-side
  reg [1:0] 	       fourB_counter; // count 8 bytes preamble

  reg [2:0] 	       pream_counter;
  reg [7:0] 	       txd_clkd;
  reg 		       tx_en_clkd;

  reg [12:0] 	       calc_pkt_length;
  reg [15:0] 	       got_pkt_length;
  reg 		       COL_int;
  reg 		       eop_clkd;
  reg 		       BFM_xrun_i;

  reg 		       enet_short_ipg;
  reg 		       enet_short_frame;
  reg 		       enet_bad_pream;
  reg 		       enet_no_spd;
  reg 		       enet_short_pream ;
  reg 		       enet_tx_err_recv;  
  reg [3:0]      rgmii_status;
  
//***************************************************************************
//                      Continuous Assignments
//***************************************************************************    
  wire 		       enet_fcs_err; 

  wire 		       eop,sop;
  wire 		       force_loss_crs;
  wire 		       force_col;
  wire [14:0] 	       loss_crs_byte_num; 
  wire [14:0] 	       col_byte_num;

  wire 		       fourB_cnt_done;
  wire 		       gigmode;
  wire [31:0] 	       rx_config, err_insertion;
  wire 		       sel_clk_10, sel_clk_100, half_duplex, fcs_enable;
  wire 		       rmii_mode, rgmii_mode, tbi_en, rtbi_en;
  wire 		       reset_l;
  wire 		       last_Dword;

  assign 	       reset_l   = ~(~sys_reset_l | XIF_reset);
  assign 	       COL       = COL_int;

  assign 	       fourB_cnt_done = (fourB_counter == 2'h3)? 1:0; 
  assign 	       gigmode = !(sel_clk_100 | sel_clk_10);

  assign 	       rx_config     = XIF_get_csdata[31:0];
  assign 	       err_insertion = XIF_get_csdata[63:32];

  assign 	       loss_crs_byte_num = err_insertion[14:0];
  assign 	       force_loss_crs    = err_insertion[15];
  assign 	       col_byte_num      = err_insertion[30:16];
  assign 	       force_col         = err_insertion[31];
  
  assign 	       sel_clk_10  = rx_config[0];
  assign 	       sel_clk_100 = rx_config[1];
  assign 	       half_duplex = rx_config[2];    
  assign 	       fcs_enable  = rx_config[3];
  assign 	       rmii_mode   = rx_config[4];
  assign 	       rgmii_mode  = rx_config[5];
  assign 	       tbi_en      = rx_config[6];
  assign 	       rtbi_en     = rx_config[7];    

  // XIF interface related
  assign 	       BFM_put_operation= BFM_NXTREQ;
  assign 	       BFM_put_status   = rx_err_status;
  assign 	       BFM_put_size     = (calc_pkt_length >4) ? (fcs_enable?(calc_pkt_length-4):calc_pkt_length):0;
  assign 	       BFM_put_address  = 0;
  assign 	       BFM_put_csdata   = rx_err_status;
  assign 	       BFM_put_data     = txd_int;
  assign 	       BFM_put_cphwtosw = 0; 
  assign 	       BFM_put_cpswtohw = 0; 
  assign 	       BFM_interrupt    = 0;

  assign 	       BFM_reset    = ~sys_reset_l;
  assign 	       BFM_clock    = sys_clk; 
  assign 	       BFM_gp_daddr = gp_daddr;
  assign 	       BFM_put_dwe  = (((TX_EN) && (curr_state==GET_DATA) && fourB_cnt_done) | eop);
  //assign enet_fcs_err = (eop_clkd && (got_fcs != 32'he320bbde) && (fcs_enable == 1)) ?1:0;
   assign enet_fcs_err = (((TX_ER && TX_EN) ||(eop_clkd && (got_fcs != 32'he320bbde) && (fcs_enable == 1))) ?1:0);


  assign rx_err_status = {5'b0,
                          rgmii_status,
                          enet_no_spd, 
                          enet_short_ipg, 
                          enet_short_pream,
                          enet_fcs_err,
                          enet_bad_pream, 
                          enet_short_frame, 
                          enet_tx_err_recv }; 

 //***************************************************************************
 //                      Always Loops
 //***************************************************************************
  //----------------------------------------------------------------------
  //-- internal 4_Byte counter --
  //----------------------------------------------------------------------
  always@(posedge sys_clk or negedge reset_l) begin
    if(!reset_l) begin
      fourB_counter  <= 2'h0;
    end else begin
      if((curr_state==GET_PREAM_H) && (nxt_state==GET_DATA)) 
        fourB_counter  <= 2'h0 ;
      else if((curr_state==GET_PREAM_H) || (curr_state==GET_PREAM_L) || 
              (curr_state==GET_DATA)) begin
        if(fourB_counter != 2'h3) 
          fourB_counter <= fourB_counter + 1;
        else 
          fourB_counter  <= 2'h0 ;
      end else
        fourB_counter  <= 2'h0 ;
    end   // (reset_l==1)
  end      // always

  //----------------------------------------------------------------------
  // -- gp_daddr --
  // calcuates the FCS too
  // Addr to be valid with data for put_data cycle
  //----------------------------------------------------------------------
  always@(posedge sys_clk or negedge reset_l)
  begin
    if (!reset_l) begin
      gp_daddr <= 11'h0;
      got_fcs  <= 32'hffff_ffff;
      gp_daddr_clkd <= 11'h0;
    end else begin 
      gp_daddr_clkd <= gp_daddr;
      if((curr_state == GET_DATA) || eop_clkd)  begin
        got_fcs <= calculate_fcs(got_fcs, txd_clkd);
        if(fourB_counter == 2'b11) 
          gp_daddr <= gp_daddr + 1;
      end else begin
        got_fcs  <= 32'hffff_ffff;
        gp_daddr <= 11'h0;
      end 
    end // reset_l
  end // always@(...

  //----------------------------------------------------------------------
  //-- get TXD --
  //-------------
  // *check for sfd in the last byte of PREAMBLE: SFD
  // * Increment pktB_cnt whein in get_data_state 
  // * pack data in 32 bit words
  //----------------------------------------------------------------------
  always@(posedge sys_clk or negedge reset_l) begin
    if(!reset_l) begin
      txd_int  <= 1'b0;
    end else 
      if((curr_state==GET_DATA) ||
        ((curr_state==GET_PREAM_L) && fourB_cnt_done))begin 
        case(fourB_counter) 
          2'h3 : txd_int[7:0] <= TXD; 
          2'h0 : txd_int[15:8] <= TXD; 
          2'h1 : txd_int[23:16]  <= TXD; 
          2'h2 : txd_int[31:24]   <= TXD;
        endcase
      end else
        if ((nxt_state==IDLE) && rgmii_mode)
          rgmii_status <= TXD[3:0];
        else if (!rgmii_mode)
          rgmii_status <= 0;
  end // always@(....

  assign BFM_xrun = BFM_xrun_i;

  //----------------------------------------------------------------------
  // main state machine to recive TX_signals from G/MAC
  //
  // * extend xrun to gen err_status
  //----------------------------------------------------------------------
  always@(posedge sys_clk or negedge reset_l)
  begin
    if (!reset_l) begin 
      curr_state <= IDLE; 
      BFM_xrun_i   <= 1'b0;
      flag_crs_err_extend = 0;
      flag_crs_extend = 0;
    end else begin 
      curr_state <= nxt_state; 
      if((curr_state == GET_DATA) && (nxt_state == IDLE)) 
        BFM_xrun_i <= 1'b0;
      else
        BFM_xrun_i <= BFM_xrun_int;
    end
  end

  always@(curr_state or TX_EN or TXD or COL or TX_ER or XIF_get_operation or 
          fourB_cnt_done) 
  begin
    nxt_state = IDLE;
    case(curr_state)
      IDLE: begin
        BFM_xrun_int = 1'b0;
        flag_crs_err_extend = 0;
        flag_crs_extend = 0;
        case (XIF_get_operation) // decode the transaction opcode
          BFM_READ: begin
            BFM_xrun_int = 1'b1;
            if(TX_EN && !TX_ER) 
              nxt_state = GET_PREAM_H;
            else 
              nxt_state = IDLE;
          end

	  BFM_NOP: begin
            nxt_state = IDLE;
            BFM_xrun_int = 1'b0; end

          default : begin // debug
	    $display("%d operation not supported \n", XIF_get_operation);
            nxt_state = IDLE;
            BFM_xrun_int = 1'b0; end
        endcase
      end // idle_state

      GET_PREAM_H: begin
        BFM_xrun_int = 1'b1;
        if(fourB_cnt_done && !TX_ER) 
          nxt_state = GET_PREAM_L;
        else if(TX_ER) begin 
          nxt_state = IDLE;      // should never get it 
          BFM_xrun_int = 1'b0;
        end else 
          nxt_state = GET_PREAM_H;  
      end

      GET_PREAM_L: begin
        BFM_xrun_int = 1'b1;
        if((fourB_cnt_done) && !TX_ER) 
          nxt_state = GET_DATA;
        else if(TX_ER) begin 
          nxt_state = IDLE;      // should never get it 
          BFM_xrun_int = 1'b0;
        end else 
          nxt_state = GET_PREAM_L;  
      end
`ifdef OLD
      GET_DATA: begin
        BFM_xrun_int = 1'b1;
        if(TX_ER && (TXD == (8'h0f||8'h1f))) // crs extend
          nxt_state = CRS_EXTEND;  
        else if(!TX_EN) begin
          nxt_state = IDLE;                 // normal pkt termination
          BFM_xrun_int = 1'b0;
        end else
          nxt_state = GET_DATA;             // receiving pkt
      end

      CRS_EXTEND: begin
        BFM_xrun_int = 1'b1;
        if(!TX_EN && (TXD == 8'h0f) && TX_ER) begin 
          nxt_state = CRS_EXTEND;             
          flag_crs_extend = 1;
        end else if(!TX_EN && (TXD == 8'h1f) && TX_ER) begin 
          nxt_state = CRS_EXTEND;             
          flag_crs_err_extend = 1;
        end else if(!TX_EN && (TXD != (8'h1f||8'h0f)) && TX_ER) begin
          nxt_state = IDLE;      // should never come here
          BFM_xrun_int = 1'b0;
        end else if(!TX_ER) begin
          nxt_state = IDLE;             
          BFM_xrun_int = 1'b0;
        end
      end
`else
     
       GET_DATA: begin // debug work on this more
             BFM_xrun_int = 1'b1;
//             if(TX_ER && (TXD == (4'h0||4'h1)))    // crs extend
             if(TX_ER && TX_EN)    // crs extend
                nxt_state = CRS_EXTEND;
             else if(!TX_EN) 
		begin
                  nxt_state = IDLE;                 // normal pkt termination
                  BFM_xrun_int = 1'b0;   
                end 
	     else
                nxt_state = GET_DATA;               // receiving pkt
       end // GET_DATA

       CRS_EXTEND: begin
             BFM_xrun_int = 1'b1;
              if(TX_EN) 
		begin 
                  nxt_state = CRS_EXTEND;             
                  flag_crs_extend = 1;     	    // debug
                end 
	     else 
		begin
                  nxt_state = IDLE;             
                  BFM_xrun_int = 1'b0;
             	end
        end // CRS_EXTEND
`endif

      default : BFM_xrun_int = 1'b0;
    endcase
  end //always@(....

  // end_of_pkt : single cycle pulse
  assign eop = (!TX_EN && curr_state==GET_DATA) ? 1:0;

  // start_of_pkt : single cycle pulse
  assign sop = (TX_EN & !tx_en_clkd) ? 1:0; 

  //----------------------------------------------------------------------
  // Generate TX_IPG, calc_pkt_length
  // -- IPG must be 64 before the very first pkt gap. For a real DUT_MAC
  // this this time will be taken care of automatically since the test must
  // wait for the DUT to be configured via the slower MDIO interface.
  // * in case the ipg counter overflows, it might indicate false ipg_err
  // * ipg is not part of pkt_length 
  //----------------------------------------------------------------------
  always@(posedge sys_clk or negedge reset_l) begin
    if (!reset_l) begin 
      tx_ipg <= 0;          // preload ipg
      calc_pkt_length <= 0;
      got_pkt_length <= 0;
    end else begin 
      if (~TX_EN) begin
	// debug : remove
        //tx_ipg <= tx_ipg + (gigmode ? 8 : 4); //Count ipg
	tx_ipg <= tx_ipg + 1;
      end
      else 
        tx_ipg <= 0;

      if(curr_state==GET_DATA)
        calc_pkt_length <= calc_pkt_length + 1;
      else
        calc_pkt_length <= 13'h0;

      if((calc_pkt_length==11) && (curr_state==GET_DATA))
        got_pkt_length[15:8] <= TXD;
      else if((calc_pkt_length==12) && (curr_state==GET_DATA))
        got_pkt_length[7:0] <= TXD;
      else if (curr_state==IDLE)
        got_pkt_length <= 16'h0;
      
    end // (reset_l....
  end

 
  //----------------------------------------------------------------------
  // -- Err_status --
  // ----------------
  //                       bit val
  // ENET_GOOD_PKT           0
  // ENET_SHORT_IPG          [0]  1
  // ENET_SHORT_FRAME        [1]  2
  // ENET_BAD_PREAM          [2]  4
  // ENET_SHORT_PREAM        [3]  8
  //
  // ENET_FCS_ERR           [4]  16
  // ENET_TX_ERR_RECV       [5]  32
  // ENET_NO_PKT_RECV       [6]  64  // not implemented
  // ENET_CONF_ERR          [7]  128 // not implemented
  //
  // ENET_INVALID_CODEGROUP [8]  256 // not implemented
  // ENET_NO_SPD            [9]  512
  // ENET_RX_BAD_LENGTH     [10] 1024 // not implemented
  // 
  // rx_err_status[15:0] == 0 --> good_pkt_rcvd
  //
  // * check pkt_lenght against received pkt_length at eop.
  // * bad ipg will be reported for false sop too
  // * short_frame is generated if pkt_length < 64 Bytes
  // * in pream_check txd_clkd is used to match the state_machine timing
  //----------------------------------------------------------------------
  always@(posedge sys_clk or negedge reset_l) begin
    if (!reset_l) begin 
      pream_counter      <= 3'h7;
      txd_clkd           <= 8'h0;
      tx_en_clkd         <= 1'h0;
      enet_short_ipg     <= 1'h0;
      enet_short_frame   <= 1'h0;
      enet_bad_pream     <= 1'h0;
      enet_no_spd        <= 1'h0;
      enet_short_pream   <= 1'h0;
      enet_tx_err_recv   <= 1'h0;       
      eop_clkd           <= 1'h0;
      rgmii_status       <= 4'hd; // link=UP, clk=125MHz, full duplex
    end else begin
      tx_en_clkd <= TX_EN;
      eop_clkd <= eop;

      if(XIF_xav) begin 
        enet_short_ipg     <= 1'h0;
        enet_short_frame   <= 1'h0;
        enet_bad_pream     <= 1'h0;
        enet_no_spd        <= 1'h0;
        enet_short_pream   <= 1'h0;
        enet_tx_err_recv   <= 1'h0;       
      end else begin

        //------ 
        if(sop && ((tx_ipg*8)<96))
          enet_short_ipg <= 1'b1; 

        //------ 
        // Less than 63 because calc_pkt_length is counting one cycle behind eop
        if((eop && (calc_pkt_length<63))|| (curr_state ==  GET_DATA && TX_EN && TX_ER ))   
          enet_short_frame <= 1'b1; 

        //------ 
        txd_clkd <= TXD;
        if((curr_state==GET_PREAM_H && (txd_clkd != 8'h55)) || 
           (curr_state==GET_PREAM_L && (txd_clkd != 8'h55) && (fourB_counter!=3)))
          enet_bad_pream <= 1'b1;

        if(curr_state==GET_PREAM_L && (txd_clkd != 8'h5d) && (fourB_counter==3))
          enet_no_spd <= 1'b1;

        //------ 
        //if((curr_state==GET_PREAM_L) || (curr_state==GET_PREAM_H))
        // ((curr_state==IDLE) && (nxt_state==GET_PREAM_H)))
        if((TX_EN && TXD==8'h55) && (curr_state!=GET_DATA))
          pream_counter <= pream_counter - 1;
        else if(curr_state==IDLE)
          pream_counter <= 3'h7;

        if(eop && (pream_counter!=0))
          enet_short_pream <= 1'b1;
	
        //------ 
        if((curr_state==GET_DATA) && TX_ER)
          enet_tx_err_recv <= 1'b1;       


      end //(!xav)
    end // reset_l
  end // alwasy@(.....

  reg [12:0] col_delay_count; // size same as pkt_length reg  

  // ----------------------------------------------------------------------
  // -- COL gen --
  // -------------
  // force collision while in data_state after col_byte_num of clks
  // ----------------------------------------------------------------------
  always@(posedge sys_clk or negedge reset_l) begin
    if(!reset_l) begin
      COL_int <= 1'b0;
      col_delay_count <= 13'h0;
    end else begin
      if(XIF_xav)
        col_delay_count <= col_byte_num;
      else if(((curr_state==GET_DATA) && (col_delay_count==0)) && force_col) 
        COL_int <= 1'b1;
      else if((curr_state==GET_DATA) && force_col) begin 
        col_delay_count <= col_delay_count - 1;  
        COL_int <= 1'b0;
      end else begin
        col_delay_count <= 0;
        COL_int <= 1'b0;
      end 
    end
  end // always
  
  // ----------------------------------------------------------------------
  // -- CRS gen --
  // -------------
  //* Driving crs. In Half duplex mode -> CRS is asserted when either Tx or
  //  rx is active. Deasserted when both of them are non-active. 
    //* crs=1 throughout a collision
    //* tx_crs is input from tx_xactor. The final CRS is a wired-OR of the 
      //  rx_crs and tx_crs
      // ----------------------------------------------------------------------
      always@(posedge sys_clk or negedge reset_l) begin
	if(!reset_l) begin
          CRS  <= 1'b0;
	end else begin
          CRS  <= 1'b1;
	end
      end // always
  
  //----------------------------------------------------------------------
  // -- Packet FCS Calculation
  //----------------------------------------------------------------------
  function [31:0] calculate_fcs;
    input [31:0] fcs;
    input [7:0]  byte;
    
    integer 	   index;
    reg 	   feedback;
    
    begin
      calculate_fcs = fcs;
      
      for (index=0; index<=7; index=index+1)
      begin
	feedback = calculate_fcs[24] ^ byte[index];
	
	calculate_fcs[24] = calculate_fcs[25];
	calculate_fcs[25] = calculate_fcs[26];
	calculate_fcs[26] = calculate_fcs[27];
	calculate_fcs[27] = calculate_fcs[28];
	calculate_fcs[28] = calculate_fcs[29];
	calculate_fcs[29] = calculate_fcs[30]  ^ feedback;
	calculate_fcs[30] = calculate_fcs[31];
	calculate_fcs[31] = calculate_fcs[16];
	
	calculate_fcs[16] = calculate_fcs[17]  ^ feedback;
	calculate_fcs[17] = calculate_fcs[18]  ^ feedback;
	calculate_fcs[18] = calculate_fcs[19];
	calculate_fcs[19] = calculate_fcs[20];
	calculate_fcs[20] = calculate_fcs[21];
	calculate_fcs[21] = calculate_fcs[22];
	calculate_fcs[22] = calculate_fcs[23];
	calculate_fcs[23] = calculate_fcs[8]   ^ feedback;
	
	calculate_fcs[8]  = calculate_fcs[9];
	calculate_fcs[9]  = calculate_fcs[10];
	calculate_fcs[10] = calculate_fcs[11];
	calculate_fcs[11] = calculate_fcs[12]  ^ feedback;
	calculate_fcs[12] = calculate_fcs[13]  ^ feedback;
	calculate_fcs[13] = calculate_fcs[14]  ^ feedback;
	calculate_fcs[14] = calculate_fcs[15];
	calculate_fcs[15] = calculate_fcs[0]   ^ feedback;
	
	calculate_fcs[0]  = calculate_fcs[1]   ^ feedback;
	calculate_fcs[1]  = calculate_fcs[2];
	calculate_fcs[2]  = calculate_fcs[3]   ^ feedback;
	calculate_fcs[3]  = calculate_fcs[4]   ^ feedback;
	calculate_fcs[4]  = calculate_fcs[5];
	calculate_fcs[5]  = calculate_fcs[6]   ^ feedback;
	calculate_fcs[6]  = calculate_fcs[7]   ^ feedback;
	calculate_fcs[7]  =                      feedback;
      end
    end
  endfunction
   
`endprotect
endmodule
// -- eof --
