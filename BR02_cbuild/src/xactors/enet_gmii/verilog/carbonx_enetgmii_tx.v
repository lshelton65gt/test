//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//  Description - Synthesizable Ethernet Transmitter BFM. Attaches to the Rx 
//                interface of the DUT

`timescale 1ns / 100 ps

module  carbonx_enetgmii_tx
  (sys_clk,   
   sys_reset_l,
   // RX_EN, not used any more 
   RXD,       
   RX_DV,     
   RX_ER,     
   RX_CLK    
   );
   
   // Primary IO
   input        sys_clk, sys_reset_l;
   output       RX_CLK;   // Receive clock - 125MHz
   output       RX_DV;    // Receive data valid
   output       RX_ER;    // Receive error
   output [7:0] RXD;      // Receive data

`protect
   // carbon license crbn_vsp_exec_xtor_e1g

   //**************************************************************************
   //                      Parameter Defines
   //**************************************************************************
   parameter    
     BFMid     = 1,
       ClockId   = 1,
       DataWidth = 32,
       DataAdWid = 11,	    // Max is 15
       GCSwid    = 32*4,  // Max is 256 bytes
       // debug we don't pcswid  set it to very small value but non-Zero
       PCSwid    = 32*2;  // Max is 256 bytes
   
   parameter    LONG_FRAME_DATA = 8'h55;

   parameter    IDLE         	= 3'h0;
   parameter    SEND_PREAM_H 	= 3'h1;
   parameter    SEND_PREAM_L 	= 3'h2;
   parameter    SEND_DATA    	= 3'h3;
   parameter    SEND_FCS     	= 3'h4;
   parameter    SEND_IPG     	= 3'h5;

   parameter    PREAM_H      = 32'h5555_5555;
   parameter    PREAM_L_SFD  = 32'h5555_555d;
   
`include "xif_core.vh"   // Use the transactor template
   
   //==========================================================================
   //  File Name - carbonx_enetgmii_tx.v 
   //  Author    - Rajat Mohan
   //  Date      - March 10th 2003
   // 
   //  Description - Synthesizable Ethernet Transmitter BFM. Attaches to the Rx 
   //                interface of the DUT
   //  Revision History
   //*******************************************************************************


   // ----------------------------------------------------------------------
   // TODO -
   // D 1. byte ordering in the 32 bit data from C-side
   // X 3. Mgt register access
   // X 4. Prevent CS_Read before CS_Write, maintain a c-image of reg
   //   5. Long frame error is for more thatn 1518 bytes of data
   //   6. use model of tx_crs
   //   7. table(35-2) [physical-page 1149] - extend, false_crs etc
   // D 8. RXD is piplined to allow FCS calculation in last cycle
   // ----------------------------------------------------------------------
   
   wire [31:0] 	err_control;
   wire [31:0] 	tx_config;
   wire [31:0] 	preamble_sfd_l, preamble_sfd_h; 
   
   wire 	Inject_Rcv_Err, 
		Inject_Short_Event, 
		Inject_Align_Err, 
		Inject_Bad_FCS,    
		Inject_Frame_long; 

   wire 	reset_l, reset_i;
   wire 	fourB_cnt_done;   // 4 bytes of preamble   
   
   // count to delay the error insertion from SOP   
   // -> increase the size for delay > 64 Bytes
   wire [4:0] 	err_drv_delay; 
   
   wire 	gmii_en;       
   wire 	sel_clk_10,  sel_clk_100;
   wire 	half_duplex, fcs_enable; 
   wire [1:0] 	valid_bytes;
   wire 	last_addr;
   wire 	last_1_3_bytes;
   wire 	phy_link; // RGMII mode
   
   // -- register definition
   reg [2:0] 	curr_state;     
   reg [1:0] 	fourB_counter;    // 4 Byte counter
   
   reg [7:0] 	rxd_int, rxd_clkd;
   reg 		rx_dv_int, rx_dv_clkd;
   reg 		rx_er_int, rx_er_clkd;
   
   reg [2:0] 	nxt_state;
   reg [31:0] 	got_fcs;
   reg 		BFM_xrun_int;     
   reg 		send_short_frame;
   
   reg 		error_done;
   reg 		tx_crs;           // debug use-model
   reg [4:0] 	err_delay_count;
   reg 		sending_frame;
   reg 		sending_frame_clkd; 
   reg 		send_long_frame;
   reg 		eop;
   reg 		sop;
   reg 		BFM_xrun_i;
   reg 		stay_in_fcs_state;
   reg [7:0] 	ipg_count;
   
   
   reg [DataAdWid -1:0] gp_daddr;
   
   
   assign 		RXD   = rxd_clkd;
   assign 		RX_DV = rx_dv_clkd;
   assign 		RX_ER = rx_er_clkd;
   
   // ------------------------------
   // XIF_core interface 
   // ------------------------------
   assign 		BFM_clock          = sys_clk; 
   assign 		BFM_put_operation  = BFM_NXTREQ;
   assign 		BFM_interrupt      = 0;
   assign 		BFM_put_status     = 0;
   assign 		BFM_put_size       = 0; // debug remove size 
   assign 		BFM_put_address    = 0;
   assign 		BFM_put_data       = 0;
   assign 		BFM_put_cphwtosw   = 0; 
   assign 		BFM_put_cpswtohw   = 0; 
   assign 		BFM_put_dwe        = 0;
   assign 		BFM_reset          = ~sys_reset_l;

   // ----------------------------------------------------------------------
   //  Register definition
   //  get_csdata[31:0]    -> tx_config
   //  get_csdata[63:32]   -> err_control
   //  get_csdata[95:64]   -> preamble_sfd_l
   //  get_csdata[127:96]  -> preamble_sfd_h
   //  get_csdata[383:128] -> mgt_reg_(0 to 15)  // not any more
   //
   //  err_control[0]     -> Inject_Rcv_Err 
   //  err_control[1]     -> Inject_Short_Event 
   //  err_control[2]     -> Inject_Align_Err  // in 10/100 mode only  
   //  err_control[3]     -> Inject_Bad_FCS    // Inject error in FCS  
   //  err_control[4]     -> Inject_Frame_long 
   //  err_control[5]     -> Reserved
   //  err_control[11:6]  -> err_drv_delay   // 64 byte counter
   //  err_control[17:15] -> force_cg_error  // for TBI
   //  err_control[21:18] -> bad_cg_count    // for TBI
   //  err_control[31:22] -> reserved
   //
   //  tx_config[0]     -> sel_clk_10     
   //  tx_config[1]     -> sel_clk_100 --> (_config[1:0]==00) -> gigmode    
   //  tx_config[2]     -> half_duplex    
   //  tx_config[3]     -> fcs_enable     
   //  tx_config[4]     -> phy_link    
   //  tx_config[31:5]  -> reserved
   // 
   // ----------------------------------------------------------------------

   //----------------------------------------------------------------------
   // -- CS_Write --
   //----------------------------------------------------------------------
   assign 		tx_config   = XIF_get_csdata[31:0];
   assign 		err_control = XIF_get_csdata[63:32];

   assign 		preamble_sfd_l = ((XIF_get_csdata[95:64] !=0)  ? 
					  XIF_get_csdata[95:64] : PREAM_L_SFD);

   assign 		preamble_sfd_h = ((XIF_get_csdata[127:96] !=0) ? 
					  XIF_get_csdata[127:96] : PREAM_H); 

   //----------------------------------------------------------------------
   // -- CS_Read --
   // address for mgt_reg_index is taken from tx_config
   // -- Management reg access
   //----------------------------------------------------------------------

   assign 		BFM_put_csdata[63:32]  = err_control;
   assign 		BFM_put_csdata[31:0]   = tx_config;

   assign 		Inject_Rcv_Err     = err_control[0];
   assign 		Inject_Short_Event = err_control[1];
   assign 		Inject_Align_Err   = err_control[2];
   assign 		Inject_Bad_FCS     = err_control[3];
   assign 		Inject_Frame_long  = err_control[4];
   assign 		err_drv_delay      = err_control[11:6];

   assign 		reset_i     = ~sys_reset_l | XIF_reset;
   assign 		reset_l       = ~reset_i;

   // We need these signals to configure the transactor interface

   assign 		RX_CLK       = sys_clk;
   assign 		sel_clk_10   = tx_config[0];
   assign 		sel_clk_100  = tx_config[1];
   assign 		half_duplex  = tx_config[2];
   assign 		fcs_enable   = tx_config[3];
   assign 		phy_link     = tx_config[4];


   // same as gigmode selection
   assign 		gmii_en = (!sel_clk_10 && !sel_clk_100) ? 1: 0; 

   assign 		BFM_gp_daddr = gp_daddr;  // only for get data

   assign 		fourB_cnt_done = (fourB_counter == 2'h3)? 1:0; 

   //----------------------------------------------------------------------
   // -- gen tx_crs --
   // ---------------
   // tx_crs is driven by tx-Xactor. The rx_xactor takes this as input and then
   // depending on full_dplex mode generates the CRS for the G/MAC
   // tx_crs must be active during error indication too 
   //----------------------------------------------------------------------

   // debug -- might be same as BFM_xrun_int
   always@(posedge sys_clk or negedge reset_l)
     begin
	if (!reset_l) begin 
	   tx_crs <= 1'b0;
	end else begin
	   if((curr_state == SEND_DATA) || (curr_state == SEND_FCS) || 
              (curr_state == SEND_PREAM_H) || (curr_state == SEND_PREAM_L)) 
             tx_crs <= 1'b1;
	   else
             tx_crs <= 1'b0;
	end
     end //always@(....

   //--------------------------------------------------
   // -- drive RXD --
   // ---------------
   // * generate signal for sending long frame
   // * send_fcs state drives rxd_clkd (not rxd_int) to take care of 1 cycle 
   //   fcs generation delay
   //--------------------------------------------------

   always@(posedge sys_clk or negedge reset_l)
     begin
	if (!reset_l) begin 
	   rxd_int  <= 8'b0;
	   rxd_clkd <= 8'b0;
	   send_long_frame <= 1'b0;
	end else begin 
	   if(send_long_frame && nxt_state == SEND_FCS && fourB_counter==2'b10) begin
              send_long_frame <= 1'b0;
              rxd_clkd <= LONG_FRAME_DATA;
	   end else if(!eop && curr_state==SEND_FCS) begin
	      case(fourB_counter) // debug invert got_fcs
		2'b00 : rxd_clkd <= ~got_fcs[31:24]^Inject_Bad_FCS;
		2'b01 : rxd_clkd <= ~got_fcs[23:16]^Inject_Bad_FCS; 
		2'b10 : begin
		   rxd_clkd <= ~got_fcs[15:8] ^Inject_Bad_FCS;
		   if((Inject_Frame_long) && (nxt_state == SEND_FCS) && (!send_long_frame)) 
		     send_long_frame <= 1'b1;	
		end
		2'b11 : begin 
		   rxd_clkd <= ~got_fcs[7:0]  ^Inject_Bad_FCS;
		   
		end // 2'b11 
              endcase 
	   end else // state != send_fcs
             rxd_clkd <= rxd_int;

	   if(curr_state == IDLE) begin
              rxd_int   <= 8'b0;
	   end else if(curr_state == SEND_PREAM_H) begin 
	      case(fourB_counter) 
		2'b00 : rxd_int <= preamble_sfd_h[31:24]; //MSB first
		2'b01 : rxd_int <= preamble_sfd_h[23:16]; 
		2'b10 : rxd_int <= preamble_sfd_h[15:8]; 
		2'b11 : rxd_int <= preamble_sfd_h[7:0];
	      endcase
	   end else if(curr_state == SEND_PREAM_L) begin 
	      case(fourB_counter) 
		2'b00 : rxd_int <= preamble_sfd_l[31:24];
		2'b01 : rxd_int <= preamble_sfd_l[23:16]; 
		2'b10 : rxd_int <= preamble_sfd_l[15:8]; 
		2'b11 : rxd_int <= preamble_sfd_l[7:0];
              endcase
	   end else if(curr_state == SEND_DATA) begin 
	      case(fourB_counter) 
		2'b11 : rxd_int <= XIF_get_data[31:24];
		2'b10 : rxd_int <= XIF_get_data[23:16]; 
		2'b01 : rxd_int <= XIF_get_data[15:8]; 
		2'b00 : rxd_int <= XIF_get_data[7:0];
              endcase 
	   end 
	end
     end // always @(...


   //----------------------------------------------------------------------
   // -- 4-Byte counter --
   // internal 4 Byte counter
   // * clear counter at eop so that it counts 0->3 in send_fcs state
   //----------------------------------------------------------------------

   always@(posedge sys_clk or negedge reset_l) begin
      if(!reset_l) begin
	 fourB_counter  <= 2'h0;
	 eop <= 0;
      end else begin
	 eop <= (sending_frame_clkd ^ sending_frame);

	 if(eop && (curr_state==SEND_FCS)) 
           fourB_counter  <= 2'h0 ;
	 else if((curr_state==SEND_PREAM_H)||(curr_state==SEND_PREAM_L)||
		 (curr_state==SEND_FCS) || (curr_state==SEND_DATA)) begin
            if(fourB_counter != 2'h3) 
              fourB_counter <= fourB_counter + 1;
            else 
              fourB_counter  <= 2'h0 ;
	 end else
           fourB_counter  <= 2'h0 ;
      end   // (reset_l==1)
   end      // always

   //----------------------------------------------------------------------
   // -- Byte alignment --
   // Get the valid bytes from last cycle of 32 bit data
   // This block also controls the SEND_DATA state 
   //----------------------------------------------------------------------
   assign valid_bytes = (XIF_get_size[1:0]); //get remainder for non_word_data 

   assign last_addr = (gp_daddr == ((XIF_get_size>>2)-1)) ? 1 :0; 

   assign last_1_3_bytes = (last_addr & (valid_bytes!=0)) ? 1:0; 

   always@(posedge sys_clk or negedge reset_l)
     begin
	if (!reset_l) begin 
	   sending_frame   <= 1'b0;
	   sending_frame_clkd <= 1'b0; 
	end else begin
	   sending_frame_clkd <= sending_frame;

	   if((curr_state == SEND_PREAM_L) && (nxt_state == SEND_DATA))
             sending_frame <= 1'b1;
	   else if((last_addr) && (!last_1_3_bytes) && (fourB_counter==2'b10))
             sending_frame <= 1'b0;
	   else if((gp_daddr==(XIF_get_size>>2)) && (fourB_counter==(valid_bytes -2)) &&
		   (valid_bytes > 1)) 
             sending_frame <= 1'b0;
	   else if((gp_daddr==(XIF_get_size>>2)) && (fourB_counter==2'b11) &&
		   (valid_bytes==1)) 
             sending_frame <= 1'b0;
	end
     end // always@(.... 

   //--------------------------------------------------------------------
   // -- fcs gen--
   // delay the start of fcs_calculation at sop
   // -- call fcs calculate --
   //--------------------------------------------------------------------
   always@(posedge sys_clk or negedge reset_l) begin
      if(!reset_l) begin 
	 got_fcs  <= 32'hffff_ffff;
	 sop      <= 1'b0;
      end else begin 
	 if((curr_state == SEND_PREAM_L) && (nxt_state == SEND_DATA))
           sop <= 1'b1;
	 else if(sop && (rxd_int==8'h55))
           sop <= 1'b0;
	 else 
           sop <= 1'b0;

	 if((!sop && (curr_state == SEND_DATA)) || eop) 
	   got_fcs <= #1 calculate_fcs(got_fcs, rxd_int);
	 else if(curr_state == IDLE)
           got_fcs  <= 32'hffff_ffff;

      end
   end // always

   //--------------------------------------------------------------------
   // -- gp_daddr gen--
   // increment only once per 4 clks
   // daddr must be valid 1 clk before the data is expected
   //
   // For MII(nibble) - incr once per 8 clks 
   //--------------------------------------------------------------------
   always@(posedge sys_clk or negedge reset_l) begin
      if(!reset_l) 
	gp_daddr <= 11'h0;
      else if(curr_state == SEND_DATA) begin 
	 if (fourB_counter == 2'b10)
           gp_daddr <= gp_daddr + 1;
      end else
	gp_daddr <= 0;
   end // always
   
   //-------------------------------------------------------------------
   // -- RX_ER gen --
   // ----------------
   //* rx_er is 1 clk pulse generated when inject_bad_frame is set
   // The pulse is sent after the specified err_drv_delay from c-side
   // The count starts during the first preamble byte. Max delay value
   // is 64. To increase it, simple increase the err_drv_delay reg size
   // 
   //* RX_ER to be asserted only during send_data state
   //--------------------------------------------------------------------

   always@(posedge sys_clk or negedge reset_l)
     begin
	if (!reset_l) begin 
           rx_er_int  <= 1'b0; 
           rx_er_clkd <= 1'b0; 
	   error_done <= 1'b0;
           err_delay_count <= 5'b0;
	end else begin 
           rx_er_clkd <= rx_er_int;
           if(XIF_xav)
             error_done <= 1'b0;  
           else if(sending_frame && Inject_Rcv_Err && !error_done) begin
              if(err_delay_count < err_drv_delay) 
		err_delay_count <= err_delay_count + 1;
              else begin
		 rx_er_int  <= 1'b1; 
		 error_done <= 1'b1;  
		 err_delay_count <= 5'b0;  
              end
           end else begin
              rx_er_int  <= 1'b0; 
              err_delay_count <= 5'b0;  
           end // if(curr_state...
	end
     end  // always @(.....

   //-------------------------------------------------------------------
   // -- RX_DV gen --
   // ----------------
   // * extended 1 cycle for long frame error generation
   //--------------------------------------------------------------------

   always@(posedge sys_clk or negedge reset_l)
     begin
	if (!reset_l) begin 
	   rx_dv_int  <= 1'b0;
	   rx_dv_clkd <= 1'b0;
	   send_short_frame <= 1'b0;
	end else begin
	   rx_dv_clkd <= (rx_dv_int | send_long_frame);

	   if((curr_state == SEND_FCS) && (fourB_counter==2'b11) && !stay_in_fcs_state) 
             rx_dv_int <= 1'b0;
	   else if((curr_state == SEND_DATA) || (curr_state == SEND_FCS) || 
		   (curr_state == SEND_PREAM_H)||(curr_state == SEND_PREAM_L))  begin 
              rx_dv_int <= 1'b1;
              if((curr_state == SEND_PREAM_H) && (Inject_Short_Event))
		send_short_frame <= 1'b1;
	   end else begin 
              rx_dv_int <= 1'b0;
              send_short_frame <= 1'b0;
	   end
	end
     end // always @(...


   assign BFM_xrun = BFM_xrun_i;

   //-------------------------------------------------------------------
   // Don't look at 4B_count_done in the very first cycle of send_FCS
   //-------------------------------------------------------------------
   always@(posedge sys_clk or negedge reset_l)
     begin
	if (!reset_l) 
	  stay_in_fcs_state <= 1'b0;
	else if((curr_state==SEND_DATA) && (nxt_state==SEND_FCS) &&
		(valid_bytes == 3)) 
	  stay_in_fcs_state <= 1'b1;
	else 
	  stay_in_fcs_state <= 1'b0;
     end // always

   //-------------------------------------------------------------------
   // -- IPG -- 
   // -2 is done to take care of processing delay
   //-------------------------------------------------------------------
   always@(posedge sys_clk or negedge reset_l)
     begin
	if (!reset_l) 
          ipg_count <= 0;
	else if((!ipg_count) && (XIF_get_address != 0) && (curr_state == IDLE))
          ipg_count <= XIF_get_address[7:0] - 2;     
	else if((curr_state == SEND_IPG) && (ipg_count != 0))
          ipg_count <= ipg_count - 1;
	else 
          ipg_count <= 0;
     end // always
   
   //-------------------------------------------------------------------
   // state machine to toggle the state of the 
   // Enet TX-Xactor 
   // 1. The state m/c must keep Xrun asserted in all states except idle
   // 2. XIF_get_ope.. is decoded in idle state only
   //
   // NITS
   // 1. in send_FCS state added valid_bytes=!3 to currect bug where state
   //    transition to Idle prevents xmit of FCS
   //-------------------------------------------------------------------

   always@(posedge sys_clk or negedge reset_l)
     begin
	if (!reset_l) begin 
	   curr_state <= IDLE; 
	   BFM_xrun_i   <= 1'b0;
	end else begin 
	   curr_state <= nxt_state; 
	   BFM_xrun_i   <= BFM_xrun_int;
	end
     end

   // check sensitivity list 
   always@(XIF_get_address or XIF_get_operation or curr_state or fcs_enable
	   or fourB_cnt_done or gmii_en or ipg_count or send_long_frame
	   or send_short_frame or sending_frame or stay_in_fcs_state)
     begin
	nxt_state = 3'b0;
	BFM_xrun_int = 1'b0;
	//drive_defaults;           // task to drive all the default outputs
	case(curr_state) 
	  IDLE : begin
	     BFM_xrun_int = 1'b0;

             case (XIF_get_operation) // decode the transaction opcode
               BFM_WRITE: begin
		  if(gmii_en) begin 
		     nxt_state = SEND_IPG;
		     BFM_xrun_int = 1'b1;
		  end else begin 
		     nxt_state = IDLE;
		     BFM_xrun_int = 1'b0;
		  end
               end

	       BFM_NOP: begin
		  nxt_state = IDLE;
		  BFM_xrun_int = 1'b0;
               end

               default : begin 
		  nxt_state = IDLE;
		  BFM_xrun_int = 1'b0;
	       end
             endcase
	  end // idle_state

	  SEND_IPG : begin 
	     BFM_xrun_int  = 1'b1;

	     if((XIF_get_address != 0) && (ipg_count != 0))
               nxt_state = SEND_IPG;
	     else
               nxt_state = SEND_PREAM_H; 
	  end
	  
	  SEND_PREAM_H : begin	 
	     BFM_xrun_int  = 1'b1;

	     if(fourB_cnt_done)
               nxt_state = SEND_PREAM_L; 
	     else
               nxt_state = SEND_PREAM_H;
	  end

	  SEND_PREAM_L : begin	 
	     BFM_xrun_int  = 1'b1;

	     if(fourB_cnt_done && send_short_frame) begin 
		nxt_state = IDLE;                //send only preamble for short_event
		BFM_xrun_int  = 1'b0;
             end else if(fourB_cnt_done)
               nxt_state = SEND_DATA; 
             else begin
		nxt_state = SEND_PREAM_L;
             end
	  end

	  SEND_DATA : begin
	     BFM_xrun_int  = 1'b1;

	     if(sending_frame)                    // stil sending data
               nxt_state = SEND_DATA;
             else if(fcs_enable)
               nxt_state = SEND_FCS;            // go send fcs
             else begin
		nxt_state = IDLE;                // don't send fcs 
		BFM_xrun_int  = 1'b0;
             end
	  end
	  
	  // added logic to prevent state change for (valid_bytes==3)	  
	  SEND_FCS : begin	 
	     BFM_xrun_int  = 1'b1;
             if(send_long_frame) // 2 extra Bytes sent after FCS  
               nxt_state = SEND_FCS;
	     else if(fourB_cnt_done && !stay_in_fcs_state)begin
		nxt_state = IDLE; 
		BFM_xrun_int  = 1'b0;
             end else begin 
		nxt_state = SEND_FCS;
             end
	  end

	endcase // case(curr_state)
     end // always
   
   //----------------------------------------------------------------------
   // -- CRC gen --
   // crc_calculated is inverted before sending out on the FCS-field
   //----------------------------------------------------------------------

   // synthesis tool must expand and optimise this crc-calculator
   function [31:0] calculate_fcs;
      input [31:0] fcs;
      input [7:0]  byte;
      integer 	   index;
      reg 	   feedback;

      begin
	 calculate_fcs = fcs;

	 for (index=0; index<=7; index=index+1)
	   begin
	      feedback = calculate_fcs[24] ^ byte[index];

	      calculate_fcs[24] = calculate_fcs[25];
	      calculate_fcs[25] = calculate_fcs[26];
	      calculate_fcs[26] = calculate_fcs[27];
	      calculate_fcs[27] = calculate_fcs[28];
	      calculate_fcs[28] = calculate_fcs[29];
	      calculate_fcs[29] = calculate_fcs[30]  ^ feedback;
	      calculate_fcs[30] = calculate_fcs[31];
	      calculate_fcs[31] = calculate_fcs[16];

	      calculate_fcs[16] = calculate_fcs[17]  ^ feedback;
	      calculate_fcs[17] = calculate_fcs[18]  ^ feedback;
	      calculate_fcs[18] = calculate_fcs[19];
	      calculate_fcs[19] = calculate_fcs[20];
	      calculate_fcs[20] = calculate_fcs[21];
	      calculate_fcs[21] = calculate_fcs[22];
	      calculate_fcs[22] = calculate_fcs[23];
	      calculate_fcs[23] = calculate_fcs[8]   ^ feedback;

	      calculate_fcs[8]  = calculate_fcs[9];
	      calculate_fcs[9]  = calculate_fcs[10];
	      calculate_fcs[10] = calculate_fcs[11];
	      calculate_fcs[11] = calculate_fcs[12]  ^ feedback;
	      calculate_fcs[12] = calculate_fcs[13]  ^ feedback;
	      calculate_fcs[13] = calculate_fcs[14]  ^ feedback;
	      calculate_fcs[14] = calculate_fcs[15];
	      calculate_fcs[15] = calculate_fcs[0]   ^ feedback;

	      calculate_fcs[0]  = calculate_fcs[1]   ^ feedback;
	      calculate_fcs[1]  = calculate_fcs[2];
	      calculate_fcs[2]  = calculate_fcs[3]   ^ feedback;
	      calculate_fcs[3]  = calculate_fcs[4]   ^ feedback;
	      calculate_fcs[4]  = calculate_fcs[5];
	      calculate_fcs[5]  = calculate_fcs[6]   ^ feedback;
	      calculate_fcs[6]  = calculate_fcs[7]   ^ feedback;
	      calculate_fcs[7]  =                      feedback;
	   end
      end
   endfunction

`endprotect
endmodule

// -- eof --
