//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "tbp_internal.h"
#include "tbp_pli.h"
#include "tbp_legacy_utils.h"

static int g_tbp_legacy_util_debug = FALSE;
#define tbp_legacy_util_debug if(g_tbp_legacy_util_debug) tbp_printf

/* OLD tbp kept this around as a global variable */
static struct queue_transaction_struct tbp_transaction_struct;

/* keep track of any modification of the QTSP */
static BOOL queue_trans_struct_modified = FALSE;

/* print buffer for legacy tbp_message function */
static char print_data_buffer[TBP_MAX_PRINT_STRING];

/*
 * function to en/disable debug printing within this module
 */
void tbp_set_legacy_debug_flag(int flag) { g_tbp_legacy_util_debug = flag; }

/* tbp3x returned the opposite of the underlying acc call */
int tbpi_set_signal_value(char *signame, char *sigval, FFL_ARGS)
{
  if(tbpi_set_value(signame,(char*)sigval,TBP_SET_BIN,iFFL_DATA)!=0)
    return FALSE;
  else
    return TRUE;
}

/* tbp3x returned the opposite of the underlying acc call */
int tbpi_get_signal_value(char *signame, char *sigval, FFL_ARGS)
{
  char *sig_ret;

  sig_ret = (char *) tbpi_get_value(signame,TBP_SET_BIN,iFFL_DATA);
  if(sig_ret==NULL) {
    return 0;
  }
  else {
    strcpy(sigval,sig_ret);
    return 1;
  }
}

/* tbp3x returned the opposite of the underlying acc call */
int tbpi_force_signal_value(char *signame, char *sigval, FFL_ARGS)
{
  if(tbpi_force_value(signame,(char*)sigval,TBP_SET_BIN,iFFL_DATA)!=0)
    return FALSE;
  else
    return TRUE;
}

/* tbp3x returned the opposite of the underlying acc call */
int tbpi_release_signal_value(char *signame, FFL_ARGS)
{
  if(tbpi_release_value(signame,iFFL_DATA)!=0)
    return FALSE;
  else
    return TRUE;
}

/* functions to modify and querry the state of the QTSP modification flag */
void tbp_set_qtsp_state(BOOL state)
{
  queue_trans_struct_modified = state;
}

BOOL tbp_get_qtsp_state(void)
{
  return queue_trans_struct_modified;
}

/* function to walk through the list of transactors */
int tbp_get_next_path(int *index, char *path)
{
  TBP_CONF_T *tc=tbp_get_conf();
  
  if((*index < 0) || (*index >= (int)tc->trans_cnt)) {
    *index += 1;
    *path = 0;
    return 0;
  }
  else {
    strcpy(path,tc->trans[*index].full_name);
    *index += 1;
    return 1;
  }
}

/* function to copy over the QTSP to the TBP_OP_T */
void tbp_copy_qstruct_to_operation(void)
{
  u_int32 trans_id = tbp_get_my_trans_id();
  TBP_OP_T *current_op;
  QTSP trans_struct = &tbp_transaction_struct;

  if(NULL==(current_op = tbp_get_operation())){
    tbp_panic("tbp_attach_buffer could not find the current operation\n");
  }
  else{
    current_op->address     = tbp_pack64(trans_struct->addr_hi,trans_struct->addr_lo);
    
#if 1
    /* make sure there is room for this data */
    tbpi_set_buffer_size(trans_id,trans_struct->byte_count,FFL_DATA);
    /* copy it in */
    memcpy(current_op->write_data,trans_struct->data,trans_struct->byte_count);
    memcpy(current_op->read_data,trans_struct->ret_data,trans_struct->byte_count);
    memcpy(current_op->read_datax,trans_struct->ret_datax,trans_struct->byte_count);
#else
    current_op->write_data  = trans_struct->data;
    current_op->write_datax = NULL;
    current_op->read_data   = trans_struct->ret_data;
    current_op->read_datax  = trans_struct->ret_datax;
#endif
    current_op->opcode      = trans_struct->operation;  
    current_op->status      = trans_struct->status;	  
    current_op->size        = trans_struct->byte_count;	  
    current_op->interrupt   = trans_struct->int_status;
    current_op->starttime   = trans_struct->starttime;
    current_op->endtime     = trans_struct->endtime;
    switch(current_op->opcode)
      {
      case TBP_NOP:
      case TBP_IDLE:
      case TBP_SLEEP:
      case TBP_NXTREQ:
      case TBP_INT_DONE:
	current_op->direction = TBP_OP_NODATA;
	break;
      case TBP_READ:
      case TBP_CS_READ:
      case TBP_FREAD:
	current_op->direction   = TBP_OP_READ;
	break;
      case TBP_WRITE:
      case TBP_CS_WRITE:
      case TBP_FWRITE:
	current_op->direction   = TBP_OP_WRITE;
	break;
      default:	
	current_op->direction   = TBP_OP_BIDIR;
	break;
      }
    current_op->width       = TBP_WIDTH_32;
  }
}

/* function to copy over the TBP_OP_T to the QTSP */
void tbp_copy_operation_to_qstruct(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread = tbp_get_current_thread();
  TBP_TRANS_T *trans  = &tc->trans[tbp_get_my_trans_id()];
  TBP_OP_T *current_op;
  QTSP trans_struct = &tbp_transaction_struct;

  if(NULL==(current_op = tbp_get_operation())){
    tbp_panic("tbp_attach_buffer could not find the current operation\n");
  }
  else{
    trans_struct->valid		    = TRUE;
#ifdef __cplusplus
    trans_struct->type		    = 0;
#else
    trans_struct->class		    = 0;
#endif
    trans_struct->transactor_id	    = trans->id;
    trans_struct->test_id	    = 0;
    trans_struct->transaction_id    = 0;
    trans_struct->active	    = TRUE;
    trans_struct->addr_lo	    = (u_int32) (current_op->address & 0x00000000FFFFFFFFLL);
    trans_struct->addr_hi	    = (u_int32) ((current_op->address >> 32) & 0x00000000FFFFFFFFLL);
    trans_struct->data	            = current_op->write_data;
    trans_struct->ret_data	    = current_op->read_data;
    trans_struct->ret_datax	    = current_op->read_datax;
    trans_struct->operation	    = current_op->opcode;
    trans_struct->status	    = current_op->status;
    trans_struct->ret_status	    = current_op->status;
    trans_struct->byte_count	    = current_op->size;
    trans_struct->int_status	    = current_op->interrupt;
    trans_struct->starttime	    = current_op->starttime;
    trans_struct->endtime	    = current_op->endtime;
    trans_struct->timeout	    = thread->timeout;
    trans_struct->pli_resp_expected = FALSE;  // FIXME - response/request functional to be added?
    trans_struct->pli_resp_received = FALSE;  // FIXME - response/request functionity to be added?
    trans_struct->to_c_data_size    = current_op->size;  // FIXME - need to model returning size other than that requested
    trans_struct->to_pli_data_size  = current_op->size;
  }
}

/* return a pointer to the current operation with the struct used by tbp < 3x */
QTSP tbp_attach_buffer(void)
{
  /* report access to this structure */
  tbp_set_qtsp_state(TRUE);
  /* copy the TBP_OP_T contents to the transaction_struct */
  tbp_copy_operation_to_qstruct();
  /* now return the address of the transaction struct */
  return &tbp_transaction_struct;
}

/* transport function used for non-cpf and sim-side transactors */
void tbpi_transport(FFL_ARGS)
{
  TBP_OP_T *current_op = tbp_get_operation();

  /* this backward compatibility should/could be conditional */
  if(tbp_get_qtsp_state())
    tbp_copy_qstruct_to_operation();
  
  /* send this operation */
  tbpi_send_operation(tbp_get_my_trans_id(),current_op,iFFL_DATA);
  
  /* conditionally copy the data out to the qtsp */
  if(tbp_get_qtsp_state())
    tbp_copy_operation_to_qstruct();
  
  /* clear the qtsp state */
  tbp_set_qtsp_state(FALSE);
}

/* function to perform any initialization specific to the legacy code */
void tbp_legacy_init(void)
{
  tbp_set_qtsp_state(FALSE);
}

/* stub function for get_class */
int tbp_cpf_get_class(void)
{
  return 0;
}

/* stub function */
int tbp_get_class_by_path(char *path)
{
  (void)path;
  return 0;
}

/* stub function */
int tbp_get_class_id_by_path(int *class, int *id, char *path)
{
  *class = 0;
  *id = tbp_get_trans_id(path);
  return *id!=-1 ? TRUE: FALSE;
}

/* set debug regions (multiple regions within a single string */
void tbp_set_debug_regions(char *regions)
{
  (void)regions;
  //  The following was removed to resolve a problem with glibc and ctype.h.  isprint and isspace are from ctype.h.
  //  This function is not used anymore for messaging
// JJH -- remove for carbon   char new_region[1024];
// JJH -- remove for carbon   int i,c,string_length=0;
// JJH -- remove for carbon   
// JJH -- remove for carbon   /* walk through the provided string looking for white-space delimited strings */
// JJH -- remove for carbon   new_region[0] = '\0';
// JJH -- remove for carbon   for(i=0;i<strlen(regions);i++){
// JJH -- remove for carbon     c = regions[i];
// JJH -- remove for carbon     /* is it printable and non-whitespace? */
// JJH -- remove for carbon     if(isprint(c) && ~isspace(c)){
// JJH -- remove for carbon       new_region[string_length++] = c;
// JJH -- remove for carbon     }
// JJH -- remove for carbon     else {
// JJH -- remove for carbon       new_region[string_length] = '\0'; // terminate the string
// JJH -- remove for carbon       tbp_add_debug_group(new_region);   // now add it to the debug groups 
// JJH -- remove for carbon       string_length = 0;
// JJH -- remove for carbon       new_region[0] = '\0';
// JJH -- remove for carbon     }
// JJH -- remove for carbon   }
}

/* sleep any thread */
void tbpi_evt_sleep_thread(s_int64 timeout,FFL_ARGS)
{
  tbpi_evt_wait(TBP_EVT_SLEEP,timeout,iFFL_DATA);
}

/* legacy tbp_messsage (new function is tbp_msg) */
int tbp_message(tbp_msg_type type, char *func, char *file, int line, ...) 
{
  va_list argp;
  char *fmt;
  char *group;
  int level;
  char *group_string[] = { 
    "tbp_msg_panic_dbg",
    "tbp_msg_error_dbg",
    "tbp_msg_warn_dbg",
    "tbp_msg_banner_dbg",
    "tbp_msg_info_dbg",
    "tbp_msg_debug_dbg",
    "tbp_msg_assert_dbg",
    "tbp_msg_user_grp"
  };

  va_start(argp, line);
  if(type==TBP_MSG_DEBUG_ID) {    
    group = va_arg(argp, char *);
    level = va_arg(argp, int);
  }
  else {
    level = type==TBP_MSG_INFO_ID ? 0 : -1;
    group = group_string[type&0x7];
  }
  
  fmt = va_arg(argp, char *);
  vsprintf(print_data_buffer,fmt,argp);
  va_end(argp);

  return tbp_msg(file,func,line,group,level,print_data_buffer);
}

/* stub function which will alert users of obsolete code if so configured */
void *tbp_unsupported(char *func_name, FFL_ARGS)
{
  if(g_tbp_legacy_util_debug)
    tbp_msg(iFFL_DATA,TBP_MSG_WARN_GRP,MSG_LVL_FORCE,
		"function (%s) is obsolete and may be removed\n",func_name);
  return NULL;
}

