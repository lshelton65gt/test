//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_PIPE_UTILS_H
#define TBP_PIPE_UTILS_H

#define TBP_PIPE_SEND_SIMTIME      1
#define TBP_PIPE_SEND_TRANS_TABLE  2
#define TBP_PIPE_ATTACH_TRANS      3
#define TBP_PIPE_DETACH_TRANS      4
#define TBP_PIPE_TRANSPORT         5
#define TBP_PIPE_THREAD_WAKE       6
#define TBP_PIPE_TRANS_WAKE        7
#define TBP_PIPE_SET_THREAD_ID     8
#define TBP_PIPE_SET_SIGNAL_CMD    9
#define TBP_PIPE_SET_SIGNAL_RTN   10
#define TBP_PIPE_GET_SIGNAL_CMD   11
#define TBP_PIPE_GET_SIGNAL_RTN   12
#define TBP_PIPE_PRINT            13
#define TBP_PIPE_PRINT_RTN        14
#define TBP_PIPE_MSG_CONFIG       15
#define TBP_PIPE_SIM_ERROR        16
#define TBP_PIPE_RUN_SIM          18
#define TBP_PIPE_RUN_CMODEL       19
#define TBP_PIPE_STOP_SIM         20
#define TBP_PIPE_SHUTDOWN         21
#define TBP_PIPE_RETURN           22

/* response code (1 & 2 patterns are used for pipe init) */
#define TBP_PIPE_ACK      0x33333333
#define TBP_PIPE_NACK     0x44444444

#ifdef __cplusplus
extern "C" {
#endif

/*
 * function to retrieve configuration information from the 
 * other side of the pipe pair
 */
int tbp_pipe_get_config(int caller);

/* wakeup function to send a wakeup message to a transactor */
void tbp_pipe_wakeup(int trans_id);

/* function to disconnect the pipe communication */
void tbp_disconnect(void);

#ifdef __cplusplus
}
#endif
#endif
