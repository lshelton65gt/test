//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
 * Description: This file holds the functions required
 * to wakeup (register) and talk to the the simulator
 *
 * INFO: any $call is required to update the current
 * sim time stored in the tc to minimize pli calls
 * this is done by calling tbp_store_pli_sim_time();
 */

// NOTE - memory put/get needs 64/32 endian fix implimented for registers
// NOTE - can we make registration entirely implicit with the detection
// NOTE - of $tbp calls?

#include "util/CarbonPlatform.h"
#include "acc_user.h"
#include "veriuser.h"
#include "tbp_internal.h"
#include "tbp_pli.h"
#include "tbp_utils.h"
#include "tbp_pipe_utils.h"
#include "tbp_config.h"
#ifdef TBP_MTI
#include "mti.h"
#include "acc_vhdl.h"
#endif
#ifdef TBP_CVER
#include "cv_veriuser.h"
#endif

/* flag to limit message thrashing in non-debug mode */
static BOOL g_tbp_pli_debug = FALSE;
#define tbp_pli_debugp if(g_tbp_pli_debug) tbp_printf

/* global return storage for dual process mode that reports the error on the C-side */
static int g_pli_setget_return=0;

/*
 * function to en/disable debug printing within this module
 */
void tbp_set_sim_debug_flag(int flag) { g_tbp_pli_debug = flag; }

/*
 * function to retrieve the current sim-time from the PLI
 * This should be called only once each time the C-side wakes
 * up from the pli both to speed this up and to prevent problems
 * with some simulators when tf_getlongtime is accessed 
 * multiple times
 */
void tbp_pli_store_sim_time(void) 
{
  u_int32 time_hi, time_lo;
  
  /* get the new sim time */
  time_lo = tf_getlongtime((int *)&time_hi);
  tbpi_set_current_hdl_time(tbp_pack64(time_hi,time_lo));
}

/* generic function called by utils during an async wakeup */
void tbp_store_sim_time(void)
{
  tbp_pli_store_sim_time();
}

/*
 * function associated with $tbp_register in the hdl
 */
int tbp_pli_register(void)
{
  char *full_name;
  int trans_id;
  
  /* save off the current sim time for later access */
  tbp_pli_store_sim_time();

  if(NULL != (full_name = tbp_get_current_module_name(NULL))){
    trans_id = tbpi_register(TBP_PLI_FFL_DATA,TBP_TRANS_TYPE_HDL,full_name);
    tbp_pli_debugp("transport: register module : %s id = %d\n",full_name,trans_id);
   
    if(tbp_check_all_registered()) {
      tbp_pli_debugp("transport: all transactors are now registered\n\n");
      
      /* initialize and then call the users main */
      tbp_init(TBP_SIMSIDE);
      tbp_pli_debugp("\ntransport: tbp init is done\n\n");
    }
  }
  else {
    tbp_pli_panic_internal("transport: register could not determine the name of the calling module\n");
  }

  tf_putp(1,trans_id);         /* return result */
  
  return 0;
}

/* 
 * function which reqeusts an operation from the hdl side 
 */
int tbp_pli_transport(void)
{ 
  TBP_OP_T operation;
  BOOL reset_arg_present;
  int hi,lo,current_trans;
  
  /* save off the pli sim time for later access */
  tbp_pli_store_sim_time();
  
  /* get the identity of the calling transactor */
  current_trans = tf_getp(1);

  /* check to make sure this is a valid/registered transactor */
  tbp_check_trans_id(current_trans); // FIXME - would it be more readable to test a return? it panics to who cares...
  tbpi_set_current_trans(current_trans);
  
  /* now fill in the fields of the "current" operation (existing sleep overrides) */
  operation.opcode = tf_getp(2);
  lo = tf_getlongp(&hi,3);
  operation.address = (((u_int64)hi) << 32) | ((u_int64) lo & 0x00000000ffffffffLL);
  operation.size = tf_getp(4);
  operation.status = tf_getp(5);
  operation.interrupt = tf_getp(6);

  /* check for the optional reset argument - if not it is encoded in "interrupt" */
  reset_arg_present = tf_nump()==9 ? TRUE : FALSE;
  operation.reset = reset_arg_present ? 
                     tf_getp(7) : ((operation.interrupt&0x2)!=0 ? TRUE : FALSE);

  lo = tf_getlongp(&hi,reset_arg_present ? 8 : 7);
  operation.starttime = (((u_int64)hi) << 32) | ((u_int64) lo & 0x00000000ffffffffLL);

  lo = tf_getlongp(&hi,reset_arg_present ? 9 : 8);
  operation.endtime = (((u_int64)hi) << 32) | ((u_int64) lo & 0x00000000ffffffffLL);
  
  /* process this operation - push back resultant operation if indicated */
  if(tbpi_sim_transport(current_trans,&operation)==TRUE){
    tf_putp(2,operation.opcode);
    lo = (u_int32) (operation.address & 0x00000000ffffffffLL);
    hi = (u_int32) ((operation.address >> 32) & 0x00000000ffffffffLL);
    tf_putlongp(3,lo,hi);
    tf_putp(4,operation.size);
    tf_putp(5,operation.status);
    tf_putp(6,operation.interrupt);
    if(reset_arg_present==TRUE)
      tf_putp(7,operation.reset);
  }
  else {
    tf_putp(2,operation.opcode);
  }

  /* return to the simulation*/
  return 0;
}

/*
 * function to send data from the from hdl to C
 * this can be called 3 different ways (for back/foreward compatibility):
 * 1. id,data  // single word put to address 0
 * 2. id,data,size // put N words from location data
-- * 3. id,addr,data // put 1 word into the array at location addr
 * 4. id,addr,data,size // put N words into the array at location addr
 */
int tbp_pli_put_data(void)
{
  TBP_OP_T *operation;
  s_tfnodeinfo data_info;
  s_tfexprinfo tf_reg_info;
  int buf_size;  // buffer required to suport this operation
  int num_args;  // number of arguments to this pli call
  int start_addr;
  int word_cnt;
  int data_offset=0;
  int hi,lo,hix,lox;
  int data_index = 0;
  int current_trans;
  /* typed pointers to fill the "void" typed read/readx trans data buffers */
  u_int64 *read_data64,*read_datax64;
  u_int32 *read_data32,*read_datax32;
  u_int16 *read_data16,*read_datax16;
  u_int8 *read_data8,*read_datax8;
  
  /* save the current pli sim time for later access */
  tbp_pli_store_sim_time();

  /* fetch the number of args with which we were called */
  num_args  = tf_nump();
  /* make sure we have enough to work with */
  if(num_args<2){
    tbp_pli_error_internal("$tbp_put_data requires either: (single word) id,data or (block) id,data,size)\n");
    return -1;
  }
  
  /* get the identity of the calling transactor */
  current_trans = tf_getp(1);
  /* check that this is a valid identity */
  tbp_check_trans_id(current_trans);
  tbpi_set_current_trans(current_trans);

  /* assign local pointers for subsequent access */
  operation = tbpi_trans_operation(current_trans);

  /* 
   * grab the remainder of the arguments 
   * this has changed from old tbp and would need a compaitilbity flag to
   * rearrange these args
   */
  switch(num_args)
    {
    case 2: /* simple single word put */
      start_addr = 0;
      tf_nodeinfo (2, &data_info);
      word_cnt = 1;
      data_offset = 2;
      break;
    case 3: /* block data write starting at location 0 */
      start_addr = 0;
      tf_nodeinfo (2, &data_info);
      word_cnt = tf_getp(3);
      data_offset = 2;
      break;
    case 4: /* put N words into location M */
      start_addr = tf_getp(2);
      tf_nodeinfo (3, &data_info);
      word_cnt = tf_getp(4);
      data_offset = 3;
      break;
    default:
      tbp_pli_error_internal("$tbp_put_data: called with an incorrect number of arguments (%d)\n",num_args);
      return -1;
      break; // redundant
    }
      
  /* verify the word_cnt is  non negative */
  if(word_cnt <= 0) {
    tbp_pli_error_internal("$tbp_put_data word_cnt is invalid %d\n",word_cnt); return -1;
  }

  /* now fetch the data */
  data_index = 0;
  
  /* select memory or register based read */
  if (data_info.node_type != tf_memory_node) {
    /* register based read */
    if (!tf_exprinfo(data_offset, &tf_reg_info)) {
      tbp_pli_panic_internal("TBP internal: $tbp_put_data can't access reg info!\n");
    }
      
    /* 
     * tf calls are normally 32 bits, so we have to fetch things according to 
     * the size of the buffer which was used to store this data
     */
    if (tf_reg_info.expr_vec_size > 32){
      /* force the operation to be 64bit with regard to the buffer alignment */
      operation->width = TBP_WIDTH_64;

      /* make sure we have enough memory to hold the incoming data */
      buf_size = ((start_addr+word_cnt)*8)+8;  // worst case requried buffer
      tbpi_set_buffer_size(current_trans,buf_size,TBP_PLI_FFL_DATA);

      lo = tf_reg_info.expr_value_p->avalbits;
      lox = tf_reg_info.expr_value_p->bvalbits;
      
      hi = ((tf_reg_info . expr_value_p) + 1)->avalbits;
      hix = ((tf_reg_info . expr_value_p) + 1)->bvalbits;

      read_data64 = (u_int64 *)operation->read_data;
      read_datax64 = (u_int64 *)operation->read_datax;
      
      read_data64[start_addr] = tbp_pack64(hi,lo);
      read_datax64[start_addr] = tbp_pack64(hix,lox);

      tbp_pli_debugp("$tbp_put_data64[%d] = 0x%16.16llx (width %d)\n",
	       start_addr,read_data64[start_addr],operation->width);
    }
    else if (tf_reg_info.expr_vec_size > 16) { /* 32 bit calling buffer */
      /* adjust pointer for endianess */
      if(operation->width==TBP_WIDTH_64P) 
	start_addr ^= 1;
      else
	operation->width=TBP_WIDTH_32;
      /* make sure we have enough memory to hold the incoming data */
      buf_size = ((start_addr+word_cnt)*4)+8;  // worst case requried buffer

      tbpi_set_buffer_size(current_trans,buf_size,TBP_PLI_FFL_DATA);

      read_data32 = (u_int32 *)operation->read_data;
      read_datax32 = (u_int32 *)operation->read_datax;
     
      read_data32[start_addr] = tf_reg_info.expr_value_p->avalbits;
      read_datax32[start_addr] = tf_reg_info.expr_value_p->bvalbits;

      tbp_pli_debugp("$tbp_put_data32[%d] = 0x%8.8x (width %d)\n",
	       start_addr,read_data32[start_addr],operation->width);
    }
    else if (tf_reg_info.expr_vec_size > 8) { /* 16 bit calling buffer */
      operation->width=TBP_WIDTH_16;
      /* make sure we have enough memory to hold the incoming data */
      buf_size = ((start_addr+word_cnt)*2)+8;  // worst case requried buffer

      tbpi_set_buffer_size(current_trans,buf_size,TBP_PLI_FFL_DATA);

      read_data16  = (u_int16 *)operation->read_data;
      read_datax16 = (u_int16 *)operation->read_datax;
     
      read_data16[start_addr]  = (u_int16) (tf_reg_info.expr_value_p->avalbits & 0x0000FFFF);
      read_datax16[start_addr] = (u_int16) (tf_reg_info.expr_value_p->bvalbits & 0x0000FFFF);

      tbp_pli_debugp("$tbp_put_data16[%d] = 0x%4.4x (width %d)\n",
	       start_addr,read_data16[start_addr],operation->width);
    }
    else { /* 8 bit calling buffer */
      operation->width=TBP_WIDTH_8;
      /* make sure we have enough memory to hold the incoming data */
      buf_size = start_addr+word_cnt+8;  // worst case requried buffer

      tbpi_set_buffer_size(current_trans,buf_size,TBP_PLI_FFL_DATA);

      read_data8  = (u_int8 *)operation->read_data;
      read_datax8 = (u_int8 *)operation->read_datax;
     
      read_data8[start_addr]  = (u_int8) (tf_reg_info.expr_value_p->avalbits & 0x000000FF);
      read_datax8[start_addr] = (u_int8) (tf_reg_info.expr_value_p->bvalbits & 0x000000FF);

      tbp_pli_debugp("$tbp_put_data8[%d] = 0x%2.2x (width %d)\n",
	       start_addr,read_data8[start_addr],operation->width);
    }
  }
  else { /* memory based read */
#if 0
    char *memval;
    int i,j;

    // FIXME - add above 64/32 memory buffer handling
    for(i=0;i<word_cnt;i++){
      /* make sure there is enough memory to read */
      if(word_cnt > data_info.node_mem_size) {
	tbp_pli_error_internal("$tbp_put_data: read count (%d) is larger than the memory array (%d)\n",
		  word_cnt,data_info.node_mem_size);
	return -1;
      }
      
      memval = data_info.node_value.memoryval_p;
      lo = lox = 0;
      hi = hix = 0;
      if (data_info.node_vec_size > 32){
	for (j=0;j<4;j++) {	
	  lo |= (((u_int32) memval [8 * data_index + j]) & 0xff) << j*8;
	  hi |= (((u_int32) memval [8 * data_index + 4 + j]) & 0xff) << j*8;
	}
	data_index += 1;
	for (j=0;j<4;j++) {	
	  lox |= (((u_int32) memval [8 * data_index + j]) & 0xff) << j*8;
	  hix |= (((u_int32) memval [8 * data_index + 4 + j]) & 0xff) << j*8;
	}	
	data_index +=1;
      }
      else {
	for (j=0;j<4;j++) {
	  lo |= (((u_int32) memval [8 * data_index + j]) && 0xff) << j*8;
	  lox |= (((u_int32) memval [8 * data_index + j]) && 0xff) << j*8;
	}
	data_index += 1;
	hi = 0;
	hix = 0;
      }

      operation->read_data[start_addr + i] = tbp_pack64(hi,lo);
      operation->read_datax[start_addr + i] = tbp_pack64(hix,lox);
      
    }
#endif
  }
  
  return 0;
}

/*
 * function to send data from the from C to HDL
 * this can be called 3 different ways:
 * 1. id,data  // single word get from address 0
 * 2. id,data,size // write N words to location data starting from 0
 * 3. id,addr,data,size // write N words into the array starting from addr
 */
int tbp_pli_get_data(void)
{
  TBP_OP_T *operation;
  s_tfnodeinfo data_info;
  s_tfexprinfo tf_reg_info;
  int num_args;  // number of arguments to this pli call
  int start_addr;
  int word_cnt;
  int buf_size; // buffer required to support this operation
  int data_offset=0;
  int hi,lo,hix,lox;
  int data_index = 0;
  int current_trans;
  /* typed pointers to fill the "void" typed write/writex trans data buffers */
  u_int64 *write_data64,*write_datax64;
  u_int32 *write_data32,*write_datax32;
  u_int16 *write_data16,*write_datax16;
  u_int8 *write_data8,*write_datax8;
  
  /* save off the current pli sim time for later access */
  tbp_pli_store_sim_time();
  
  /* fetch the number of args with which we were called */
  num_args  = tf_nump();
  /* make sure we have enough to work with */
  if(num_args<2){
    tbp_pli_error_internal("$tbp_get_data requires either: (single word) id,data or (block) id,data,size)\n");
    return -1;
  }
  
  /* get the identity of the calling transactor */
  current_trans = tf_getp(1);
  /* check that this is a valid identity */
  tbp_check_trans_id(current_trans);
  tbpi_set_current_trans(current_trans);

  /* assign local pointers for subsequent access */
  operation = tbpi_trans_operation(current_trans);

  /* 
   * grab the remainder of the arguments 
   * this has changed from old tbp and would need a compaitilbity flag to
   * rearrange these args
   */
  switch(num_args)
    {
    case 2: /* simple single word put */
      start_addr = 0;
      tf_nodeinfo (2, &data_info);
      word_cnt = 1;
      data_offset = 2;
      break;
    case 3: /* block data write starting at location 0 */
      start_addr = 0;
      tf_nodeinfo (2, &data_info);
      word_cnt = tf_getp(3);
      data_offset = 2;
      break;
    case 4: /* put N words into location M */
      start_addr = tf_getp(2);
      tf_nodeinfo (3, &data_info);
      word_cnt = tf_getp(4);
      data_offset = 3;
      break;
    default:
      tbp_pli_error_internal("$tbp_get_data: called with an incorrect number of arguments (%d)\n",num_args);
      return -1;
      break; // redundant
    }
      
  /* double check some of the values we have collected */
  if(start_addr < 0) {
    tbp_pli_error_internal("$tbp_get_data start address is invalid %d\n",start_addr); return -1;
  }
  if(word_cnt <= 0) {
    tbp_pli_error_internal("$tbp_get_data word_cnt is invalid %d\n",word_cnt); return -1;
  }
  
  /* now read the data out of the buffer and pass it to the HDL */
  data_index = 0;
  
  /* select memory or register based read */
  if (data_info.node_type != tf_memory_node) {  
   
    /* fetch the register information */
    if (!tf_exprinfo(data_offset, &tf_reg_info)) {
      tbp_pli_panic_internal("TBP internal: $tbp_get_data can't access reg info!\n");
    }
    
    /* change the put depending on the target size (32 or 64) */
    if (tf_reg_info.expr_vec_size > 32) { /* 64 data is not packed */
      operation->width=TBP_WIDTH_64;
      /* check to see if we have enough buffer for this operation */
      buf_size = (start_addr+word_cnt)*8;
      /* FIXME - this seems like a better thing to do - but it not compatible! (with tbp3x) */
      /* if((buf_size > operation->size) || (buf_size > operation->buffer_size)){ */
      if(buf_size > (operation->buffer_size+16)){
	tbp_pli_error_internal("$tbp_get_data: Not enough data to support this write:\n"
		  "\t%d bytes available %d bytes requested (index = %d word_cnt %d)\n",
		  operation->size,buf_size,start_addr,word_cnt);
      }
     
      write_data64 = (u_int64 *) operation->write_data;
      write_datax64 = (u_int64 *) operation->write_datax;      
      
      tbp_pli_debugp("$tbp_get_data64[%d] = 0x%16.16llx (width %d)\n",
	       start_addr,write_data64[start_addr],operation->width);

      hi = (u_int32)  (write_data64[start_addr] >> 32);
      hix = (u_int32)  (write_datax64[start_addr] >> 32);
      lo = (u_int32)  (write_data64[start_addr] & 0x00000000ffffffffLL);
      lox = (u_int32)  (write_datax64[start_addr] & 0x00000000ffffffffLL);      
      tf_putlongp(data_offset,lo,hi);
    }
    else if(tf_reg_info.expr_vec_size > 16) { /* 32 bit calling buffer - data is packed */
      /* adjust pointer for endianess */
      /* adjust pointer for endianess */
      if(operation->width==TBP_WIDTH_64P) 
	start_addr ^= 1;
      else
	operation->width=TBP_WIDTH_32;

      /* check to see if we have enough buffer for this operation */
      buf_size = (start_addr+word_cnt)*4;
      /* FIXME - this seems like a better thing to do - but it not compatible! (with tbp3x) */
      /* if((buf_size > operation->size) || (buf_size > operation->buffer_size)){ */
      if(buf_size > (operation->buffer_size+16)){
	tbp_pli_error_internal("$tbp_get_data: Not enough data to support this write:\n"
		  "\t%d bytes available %d bytes requested (index = %d word_cnt %d)\n",
		  operation->size,buf_size,start_addr,word_cnt);
      }
      
      write_data32 = (u_int32 *) operation->write_data;
      write_datax32 = (u_int32 *) operation->write_datax;

      lo = write_data32[start_addr];
      lox = write_datax32[start_addr];
      
      tf_putp(data_offset,lo);
      tbp_pli_debugp("$tbp_get_data32[%d] = 0x%8.8x (width %d)\n",
	       start_addr,write_data32[start_addr],operation->width);

      // FIXME - any way to "put" a x/z?  Would be nice
      //tf_reg_info.expr_value_p->avalbits
      //tf_reg_info.expr_value_p->bvalbits
    }
    else if(tf_reg_info.expr_vec_size > 8) { /* 16 bit calling buffer - data is packed */
      operation->width=TBP_WIDTH_16;

      /* check to see if we have enough buffer for this operation */
      buf_size = (start_addr+word_cnt)*2;
      /* FIXME - this seems like a better thing to do - but it not compatible! (with tbp3x) */
      /* if((buf_size > operation->size) || (buf_size > operation->buffer_size)){ */
      if(buf_size > (operation->buffer_size+16)){
	tbp_pli_error_internal("$tbp_get_data: Not enough data to support this write:\n"
		  "\t%d bytes available %d bytes requested (index = %d word_cnt %d)\n",
		  operation->size,buf_size,start_addr,word_cnt);
      }
      
      write_data16  = (u_int16 *) operation->write_data;
      write_datax16 = (u_int16 *) operation->write_datax;

      lo  = (u_int32) write_data16[start_addr];
      lox = (u_int32) write_datax16[start_addr];
      
      tf_putp(data_offset,lo);
      tbp_pli_debugp("$tbp_get_data16[%d] = 0x%8.8x (width %d)\n",
	       start_addr,write_data16[start_addr],operation->width);
    }
    else { /* 8 bit calling buffer - data is packed */
      operation->width=TBP_WIDTH_16;

      /* check to see if we have enough buffer for this operation */
      buf_size = start_addr+word_cnt;
      /* FIXME - this seems like a better thing to do - but it not compatible! (with tbp3x) */
      /* if((buf_size > operation->size) || (buf_size > operation->buffer_size)){ */
      if(buf_size > (operation->buffer_size+16)){
	tbp_pli_error_internal("$tbp_get_data: Not enough data to support this write:\n"
		  "\t%d bytes available %d bytes requested (index = %d word_cnt %d)\n",
		  operation->size,buf_size,start_addr,word_cnt);
      }
      
      write_data8  = (u_int8 *) operation->write_data;
      write_datax8 = (u_int8 *) operation->write_datax;

      lo  = (u_int32) write_data8[start_addr];
      lox = (u_int32) write_datax8[start_addr];
      
      tf_putp(data_offset,lo);
      tbp_pli_debugp("$tbp_get_data8[%d] = 0x%8.8x (width %d)\n",
	       start_addr,write_data8[start_addr],operation->width);
    }
  }
  else { /* memory based get */
#if 0
    char *memval;
    int i,j;

    /* check to see that the destrination of this "get" is large enough for the transfer */
    if(word_cnt > data_info.node_mem_size) {
      tbp_pli_error_internal("$tbp_get_data: write count (%d) is larger than the memory array (%d)\n",
		word_cnt,data_info.node_mem_size);
      return -1;
    }

    memval = data_info.node_value.memoryval_p; 
    data_index = 0;
    for(i=0;i<word_cnt;i++){
      hi = (u_int32)  (operation->write_data[start_addr+i] >> 32);
      hix = (u_int32)  (operation->write_datax[start_addr+i] >> 32);
      lo = (u_int32)  (operation->write_data[start_addr+i] & 0x00000000ffffffffLL);
      lox = (u_int32)  (operation->write_datax[start_addr+i] & 0x00000000ffffffffLL);

      if (data_info.node_vec_size > 32){
	for (j=0;j<4;j++) {	
	  memval [8 * data_index + j] = (char) ((lo >> (j * 8)) & 0xff);
	  memval [8 * data_index + 4 + j] = (char) ((hi >> (j * 8)) & 0xff);
	}
	data_index += 1;
       
	for (j=0;j<4;j++) {	
	  memval [8 * data_index + j] = (char) ((lox >> (j * 8)) & 0xff);
	  memval [8 * data_index + 4 + j] = (char) ((hix >> (j * 8)) & 0xff); 
	}	
        data_index +=1;
	
      }
      else {
	for (j=0;j<4;j++) { 
	  memval [8 * data_index + j] = (char) ((lo >> (j * 8)) & 0xff);
	  memval [8 * data_index + 4 + j] = (char) ((lox >> (j * 8)) & 0xff);
	}
	data_index += 1;
      }
    }
#endif
  }

  return 0;
}

/*  pli version of finish - which calls the C-side shutdown... */
int tbp_pli_finish(void) { tbp_shutdown(0); return 0; }

/* placeholder functions for tbp3x */
int tbp_pli_request(void) { tbp_info("$tbp_request not supported\n"); return 0; }

/* placeholder functions for tbp3x */
int tbp_pli_response(void) { tbp_info("$tbp_response not supported\n"); return 0; }

/*
 * hold a given signal during critical operations
 */
int tbp_pli_sighold(void)
{
  int signal = tf_getp(1);
  tbp_sighold(signal);

  return 0;
}

/* 
 * function to request that the simulation finish - must be called
 * from within the sim-thread!
 */
void tbp_sim_finish(void)
{
  if(tbpi_get_finish_flag()==FALSE){
    tbpi_set_finish_flag(TRUE);
    tbp_pli_debugp("tbp_sim_finish has been called - shutting down...\n");    
#ifdef TBP_MTI
    mti_Quit();
#else
    tf_dofinish();
#endif
  }
}

/* function to stop the simulator (to get to a cli> prompt inside the simulator) */
void tbp_sim_stop(void)
{
  if(tbp_is_ctrans())
    return;
#ifdef TBP_MTI
  mti_Break();
#else
  tf_dostop();
#endif
  tbp_wait_for_sim();
}

/* helper functions to search the argument arrays for keys */
BOOL tbp_sim_get_argv_key_present(char *key) 
{ return NULL==mc_scan_plusargs(key) ? FALSE : TRUE; }

char *tbp_sim_get_argv_key(char *key)
{
  char *key_value;
  key_value = mc_scan_plusargs(key);

  if(key_value==NULL)
    return NULL;
  else if(strlen(key_value) > 1) {
    /* stip off the leading "=" */
    if(key_value[0] == '=')
      return key_value + 1;
    else
      return key_value;
  } 
  else {
    return key_value;
  }
}

/* 
 * return the current module name - copy it into the
 * provided pointer if it is non-null
 */
char *tbp_get_current_module_name(char *name)
{
#ifdef TBP_MTI 
  char *module_name;
  char *instance;
  int region_kind = mti_GetRegionKind(mti_GetCallingRegion());
  if(region_kind!=accModule && region_kind!=accFunction && region_kind!=accArchitecture) // VHDL
    module_name = mti_GetRegionFullName(mti_GetCallingRegion());
  else { // Verilog
    if(NULL==(instance = tf_getinstance()))
      module_name = mti_GetRegionFullName(mti_GetCallingRegion());
    else
      module_name = tf_imipname(instance);
  }
#else
  char *module_name = tf_mipname();  
#endif

  if(NULL != module_name) {
    if(name!=NULL)
      strcpy(name,module_name);
    return module_name;
  }
  else {
    if(name!=NULL)
      strcpy(name,"none");
    return "none";
  }
}

/* 
 * functions used by messaging to display the location in the hdl of the
 * calling instance - first one gets the calling filename
 */
char *tbp_get_current_file(void)
{
  s_location location;
  handle file_handle;

#ifdef TBP_MTI
  char *source_name;
  int region_kind = mti_GetRegionKind(mti_GetCallingRegion());
  if(region_kind!=accModule && region_kind!=accFunction) { // VHDL
    source_name = mti_GetRegionSourceName(mti_GetCallingRegion());
    return source_name!=NULL ? source_name : "unknown";
  }
#endif

#ifdef BROKEN_GETINSTANCE
  if((file_handle=acc_handle_by_name(tf_spname(),NULL)))
    acc_fetch_location(&location,file_handle);
#else
  if((file_handle=(handle)tf_getinstance()))
    acc_fetch_location(&location,file_handle);
#endif
  return location.filename !=NULL  ? location.filename : "unknown" ;    
}

/* get the calling/current line for print statements */
int tbp_get_current_line(void)
{
  s_location location;
  handle line_handle=NULL;

#ifdef TBP_MTI
  int region_kind = mti_GetRegionKind(mti_GetCallingRegion());
  if(region_kind!=accModule && region_kind!=accFunction) // VHDL
    line_handle = mti_GetCallingRegion();
  else // Verilog
    line_handle = (handle)tf_getinstance();
#else
#ifdef BROKEN_GETINSTANCE
  line_handle = (handle)acc_handle_by_name(tf_mipname(),NULL);
#else
  line_handle = (handle)tf_getinstance();
#endif
#endif    
  if(line_handle) {
    acc_fetch_location(&location,line_handle);
    return location.line_no;
  }
  else {
    return 0;
  }
}

/* get the calling/current module/task name for print statements */
char *tbp_get_current_module(void)
{
  return tbp_get_current_module_name(NULL);
}

/* function used to fetch a handle using a full path */
handle tbpi_get_handle_by_name(char *path)
{
  handle check_handle;

  check_handle = acc_handle_by_name(path,NULL);
  if(NULL==check_handle){
    /* seems overkill, but try skipping 'path_sperator' - usually '.' or '/' */
    if(path[0] == tbpi_get_path_seperator())
      check_handle = acc_handle_by_name((path+1),NULL);
  }

  return check_handle;
}

/*
 * function to return the most recent result of a set/get operation - used for dual process
 */
int tbp_sim_setget_return(void) { return g_pli_setget_return; }
void tbp_sim_setget_flag(int flag) {g_pli_setget_return = flag; }

/*
 * pli set_value - broken out for pli/fli combination
 */
int tbpi_pli_set_value(char *path, void *value, int type, int force, char *scope, FFL_ARGS)
{
  handle set_handle; 
  s_setval_value setv;
  s_setval_delay setd;
  int size;
  int *int_value;  
  int ivalue;
  double *real_value;  
  double rvalue;
  char *char_value;
  int set_type[] = {accNoDelay,accForceFlag,accReleaseFlag};
  int ret;

  acc_configure(accDisplayErrors,"false");

  if(NULL==(set_handle = tbpi_get_handle_by_name(path))){
    tbp_setget_error(TBP_SETGET_BAD_PATH,"set_value: %s not found\n**** Check The following:\n"
		     "**** 1. signal must exist within the simulation\n"
		     "**** 2. simulator must be configured to allow write/force access (see simulator's 'acc' documentation)\n",
		     
		     path);
    return 1;
  }

  size = acc_fetch_size (set_handle);

  switch(type)
    {
    case TBP_SET_SCL:
    case TBP_SET_INT:
      int_value = (int *)value;
      ivalue = *int_value;
      setv.format = accIntVal;
      setv.value.integer = ivalue;
      break;
    case TBP_SET_REAL: 
      real_value = (double *)value;
      rvalue = *real_value;
      setv.format = accRealVal;
      setv.value.real = rvalue;
      break;
    case TBP_SET_BIN:
      /* check to make sure it is the right length */
      char_value = (char *)value;
      if((size!=strlen(char_value)) && (set_type[force]!=accReleaseFlag)){
	tbp_setget_error(TBP_SETGET_BAD_SIZE,"set_value: provided signal value (%s) size is incorrect\n"
			 "**** signal value size (%d) must match the size of the hdl signal (%d)\n",
			 char_value,strlen(char_value),size);
      }
      setv.format = accBinStrVal;
      setv.value.str = char_value;
      break;
    case TBP_SET_OCT:
      char_value = (char *)value;
      setv.format = accOctStrVal;
      setv.value.str = char_value;
      break;
    case TBP_SET_DEC:
      char_value = (char *)value;
      setv.format = accDecStrVal;
      setv.value.str = char_value;
      break;
    case TBP_SET_HEX:
      char_value = (char *)value;
      setv.format = accHexStrVal;
      setv.value.str = char_value;
      break;
    default:
      tbp_setget_error(TBP_SETGET_BAD_TYPE,"invalid set argument type (%d)\n",type);
      return -1;
      break;
    }
  
  setd.model = set_type[force];
  setd.time.type = accTime;
  setd.time.low = 0;
  setd.time.high = 0;


  ret = acc_set_value(set_handle,&setv,&setd);
    
  if(ret!=0) {
    tbp_setget_error(TBP_SETGET_BAD_LOGIC,"set_value: failed to modify signal (%s)\n**** Check The following:\n"
		       "**** 1. Simulator must be configured to allow access to this signal (see simulator's 'acc' documentation)\n"
		       "**** 2. Provided logic value must match the width of the value to be modified\n"
		       "**** 3. Provided logic value must match the specific type (bin/oct/hex/int/real etc...)\n",
		       path);
  }
  return ret;
}

void *tbpi_pli_get_value(char *path, int type, char *scope, FFL_ARGS)
{
  handle get_handle;
  void *ret;
  char *format_string[] = {"%d","%d","%g","%b","%o","%d","%x"};

  acc_configure(accDisplayErrors,"false");

 if(NULL==(get_handle = tbpi_get_handle_by_name(path))){
    tbp_setget_error(TBP_SETGET_BAD_PATH,"get_value: %s not found\n**** Check The following:\n"
		     "**** 1. signal must exist within the simulation\n"
		     "**** 2. simulator must be configured to allow read access (see simulator's 'acc' documentation)\n",

		     path);
    return NULL;
  }
  else {
    ret = acc_fetch_value(get_handle,format_string[type],NULL);
    
    if(acc_error_flag!=0 && tbpi_get_acc_error_enable()==TRUE) {
      tbp_setget_error(TBP_SETGET_BAD_LOGIC,"get_value: failed to retrieve signal (%s)\n**** Check The following:\n"
		       "**** 1. Simulator must be configured to allow access to this signal (see simulator's 'acc' documentation)\n"
		       "**** 2. Requested signal must be match request type (bin/oct/hex/int/real etc...)\n",
		       path);
    }
    return ret;
  }
}

/* 
 * set a simulation variable with a given argument - type is:
 * integer (int number)
 * binary  (char/string)
 * hex     (char/string)
 * octal   (char/string)
 * force is encoded: 
 *      0 - normal  = accNoDelay
 *      1 - force   = accForceFlag
 *      2 - release = accReleaseFlag
 */
// FIXME - need error/type checking for various types! 
int tbpi_sim_set_value(char *path, void *value, int type, int force, char *scope, FFL_ARGS)
{
#ifdef TBP_MTI
  int region_kind;
#endif /* TBP_MTI */

  /* clear the global flag */
  tbp_sim_setget_flag(TBP_SETGET_NOERR);
  
  /* placeholder for multiple set/get */
  if(scope!=NULL) {
    tbp_setget_error(TBP_SETGET_BAD_SCOPE,"scope is not yet supported with set_value\n");
    return 1;
  }

  /* check force flag */
  if(force < 0 || force > 2){
    tbp_setget_error(TBP_SETGET_BAD_FORCE,"invalid force argument (%d)\n",force);
    return 1;
  }

  if(path==NULL) {
    tbp_setget_error(TBP_SETGET_NULL_PATH,"path is null\n");
    return 1;
  }

#ifdef TBP_MTI
  region_kind = mti_GetRegionKind(mti_GetCallingRegion());
  if(region_kind!=accModule && region_kind!=accFunction)  // VHDL
    return tbpi_fli_set_value(path,value,type,force,scope,iFFL_DATA);
  else
    return tbpi_pli_set_value(path,value,type,force,scope,iFFL_DATA);
#else
  return tbpi_pli_set_value(path,value,type,force,scope,iFFL_DATA);
#endif
}

void *tbpi_sim_get_value(char *path, int type, char *scope, FFL_ARGS)
{
#ifdef TBP_MTI
  int region_kind;
#endif /* TBP_MTI */

  /* clear the global flag */
  tbp_sim_setget_flag(TBP_SETGET_NOERR);

  if(type < 0 || type > 6) {
    tbp_setget_error(TBP_SETGET_BAD_TYPE,"invalid type argument (%d)\n",type);
    return NULL;
  }
  
  if(path==NULL) {
    tbp_setget_error(TBP_SETGET_NULL_PATH,"path is null\n");
    return NULL;
  }

#ifdef TBP_MTI
  region_kind = mti_GetRegionKind(mti_GetCallingRegion());
  if(region_kind!=accModule && region_kind!=accFunction)  // VHDL
    return tbpi_fli_get_value(path,type,scope,iFFL_DATA);
  else
    return tbpi_pli_get_value(path,type,scope,iFFL_DATA);
#else
  return tbpi_pli_get_value(path,type,scope,iFFL_DATA);
#endif
}

/*
 * function to "reset" all registered transactors - "set/get" are called 2x
 * to account for simulator's support for leading "." or "/"
 */
void tbpi_sim_reset_transactors(FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();
  char *trans_name;
  int i;
  int acc_error_enable = tbpi_get_acc_error_enable();
  char *reset_value;

  if(!tbp_is_main()){
    tbp_error_internal("reset_transactors cannot be called outside of the main thread\n");
    return;
  }

  tbpi_set_acc_error_enable(FALSE);
  for(i=0;i<tc->trans_cnt;i++){
    if(tc->trans[i].type==TBP_TRANS_TYPE_HDL) {
      /* allocate space for the signal name */
      trans_name = (char *)tbp_allocate_buffer(strlen(tc->trans[i].full_name)+30,
					       "Transactor reset signal name");
      sprintf(trans_name,"%s%cTBP_reset_toggle",tc->trans[i].full_name,tbp_get_path_seperator());
      /* get the current state of the signal */
      reset_value = tbpi_sim_get_value(trans_name,TBP_GET_BIN,(char *)NULL,iFFL_DATA);
      if(reset_value!=NULL) {
	if(strcmp(reset_value,"0")==0)
	  tbpi_sim_set_value(trans_name,"1",TBP_SET_BIN,TBP_SET_NORMAL,(char *)NULL,iFFL_DATA);
	else
	  tbpi_sim_set_value(trans_name,"0",TBP_SET_BIN,TBP_SET_NORMAL,(char *)NULL,iFFL_DATA);
      }
      tbp_free(trans_name);
    }
  }

  tbpi_set_acc_error_enable(acc_error_enable);
  
  /* reliable cancellation of the tasks requires that we pass some time */
  tbpi_main_sleep(1,iFFL_DATA);
  
}

int tbp_misc(int data, int reason) 
{
  /* save off the current pli sim time for later access */
  tbp_pli_store_sim_time();

  switch (reason) {
    
    /* $save: save a simulation */
  case reason_save:
    tbp_pli_debugp("transport: simulation save called\n");
    break; /* reason_save */
    
    /* $restart: restart a simulation */
  case reason_restart:
    tbp_pli_debugp("transport: simulation restart called\n");
    break; /* reason_restart */
    
    /* when a task or named block containing the task is disabled */
  case reason_disable:
    tbp_pli_debugp("transport: simulation task disabled called\n");
    break; /* reason_disable */
    
    /* when a parameter value changeds (enabled by tf_asynchon routine */
  case reason_paramvc:
    tbp_pli_debugp("transport: simulation parameter value changed called\n");
    break; /* reason_paramvc */
    
    /* at end of time slot, enabled by tf_syncronize */
  case reason_synch:
    tbp_pli_debugp("transport: simulation sync called\n");
    break; /* reason_synch */
    
    /* $finish: end of a simulation */
  case reason_finish:
    tbp_pli_debugp("transport: simulation finish called\n");
    tbp_flush_logfile();
#ifdef DUAL_PROC
    tbpi_pipe_send_cmd(FFL_DATA,TBP_PIPE_SHUTDOWN);
#endif
    break; /* reason_finish */
    
    /* reactivate - delayed reactivation occurs set up by tf_setdelay */
  case reason_reactivate:
    tbp_pli_debugp("transport: simulation reactivate called\n");
    break; /* reason_reactivate */
    
    /* end of simulation time slot, enabled by tf_rosynchronize */
  case reason_rosynch:
    tbp_pli_debugp("transport: simulation rosync called\n");
    break; /* reason_rosynch */
    
    /* driver of a parameter to a system task changes value */
  case reason_paramdrc:
    tbp_pli_debugp("transport: simulation paramdc called\n");
    break; /* reason_paramdrc */
    
    /* End of compile - handled by an end of compile routine */
  case reason_endofcompile:
    /* initialize everything (blocked after the first call)*/
    tbp_init_config(TBP_SIMSIDE);
    tbp_pli_debugp("transport: simulation endofcompile called (time %lld module %s)\n",
    	      tbp_get_sim_time(),tbp_get_current_module_name(NULL));
    /* keep track of all calling modules to find out when rgistration is complete */
    tbp_record_module_presence(tbp_get_current_module_name((char *)NULL));
    break; /* reason_endofcompile */
    /* scope changes value */
  case reason_scope:
    tbp_pli_debugp("transport: simulation scope called\n");
    break; /* reason_scope */

    /* interactive simulation mode has been entered */
  case reason_interactive:
    tbp_pli_debugp("transport: simulation interactive called\n");
    break; /* reason_interactive */
    
    /* $reset */
  case reason_reset:
    tbp_pli_debugp("transport: simulation reset called\n");
    break; /* reason_reset */
    
    /* $reset processing has completed */
  case reason_endofreset:
    tbp_pli_debugp("transport: simulation endofreset called\n");
    break; /* reason_endofreset */
    
    /* start of $save */
  case reason_startofsave:
    tbp_pli_debugp("transport: simulation startofsave called\n");
    break; /* reason_startofsave */
    
    /* $restart is invoked */
  case reason_startofrestart:
    tbp_pli_debugp("transport: simulation startofreset called\n");
    break; /* reason_startofrestart */
    
  } 
  
  return 0;
  
} /* tbp_misc */

/* function to strip off leading spaces from tf strings */
static char *tbp_pli_string_strip_leading_space(char *string)
{
  char *chk_char;

  /* 
   * loop through the provided string and return a pointer to the first non-
   * whitespace char
   */
  for(chk_char = string; *chk_char; chk_char++){
    if(*chk_char!=' ')
      return chk_char;
  }

  /* 
   * if none were found, just return the string as provided 
   * (should we ever get here?) 
   */
  return string;
}
    
/* parse and format the printf string */
static int tbp_pli_make_string(char *local_string) {
  char *fmt;
  char *expr_string;
  int  num_args, arg = 0;
  int  tf_type;

  /* NULL out the local string as a precaution */
  local_string[0] = '\0';
  
  num_args = tf_nump();

  if (num_args < 1) {
    tbp_pli_panic_internal("$tbp_printf called with no arguments\n");
  }
  
  /* 
   * walk through the arguments printing as we go arguments may either be fmt 
   * or values - fmt requires values, but values do not need fmt! $display style
   */
  while(arg < num_args){
    arg += 1;

    /* fetch the type */
    if(tf_nullparam==(tf_type = tf_typep(arg)))
      tbp_pli_panic_internal("display/print could not retrieve argument %d\n",arg);

    /* if this is not a string, check for real and print it real or decimal format */
    if (tf_type != tf_string) {
      /* if it is real, then we can safely fetch it as a real (as it cannot be >64bits */
      if((tf_type==tf_readonlyreal) || (tf_type==tf_readwritereal)) {
	sprintf(local_string,"%s%f",local_string,tf_getrealp(arg));
      }
      else { /* it is not a "real" - print it as decimal */
	if(NULL==(expr_string=tf_strgetp(arg,'d')))
	  tbp_pli_panic_internal("display/print argument %d could not be retrieved\n",arg);
	else {
	  expr_string = tbp_pli_string_strip_leading_space(expr_string);
	  sprintf(local_string,"%s%s",local_string,expr_string);
	}
      }
    }
    else {
      /* 
       * this is a string which may contain formatting - requiring us to parse 
       * through and fetch arguments as needed
       */
      if(NULL==(expr_string = tf_getcstringp(arg)))
	tbp_pli_panic_internal("display/print argument %d could not be retrieved\n",arg);
      
      /* if the string is valid - walk through and parse */
      for(fmt = expr_string; *fmt; fmt++) {
	if (*fmt != '%') {
	  strncat(local_string, fmt, 1);
	  continue;
	}
	fmt++;
	
	if (*fmt != 'm') {  /* we need to add an argument, so first go get it */
	  arg +=1;

	  /* fetch the type */
	  if(tf_nullparam==(tf_type = tf_typep(arg)))
	    tbp_pli_panic_internal("display/print could not retrieve argument %d\n",arg);
	}

	/* now add it to the string according to the format command */
	// FIXME - we should trap "ll"
	switch (*fmt) {
	case 'D':
	case 'T':
	case 'd':
	case 't':
	  if ((tf_type != tf_readonly) &&
	      (tf_type != tf_readwrite) &&
	      (tf_type != tf_rwbitselect) &&
	      (tf_type != tf_rwpartselect) &&
	      (tf_type != tf_rwmemselect)) {
	    tbp_pli_error_internal("display/print arg %d is not integer type\n", arg);
	  }

	  if(NULL==(expr_string=tf_strgetp(arg,'d')))
	    tbp_pli_panic_internal("display/print could not retrieve argument %d\n",arg);
	  else {
	    expr_string = tbp_pli_string_strip_leading_space(expr_string);
	    sprintf(local_string,"%s%s",local_string,expr_string);
	  }
	 break;
	case 'F':
	case 'f':
	  if ((tf_type != tf_readonly) &&
	      (tf_type != tf_readwrite) &&
	      (tf_type != tf_readonlyreal) &&
	      (tf_type != tf_readwritereal)) {
	    tbp_pli_panic_internal("display/print arg %d is not float type (type %d)\n", arg,tf_type);
	  }
	  sprintf(local_string,"%s%f",local_string,tf_getrealp(arg));
	  break;
	case 'G':
	case 'g':
	  if ((tf_type != tf_readonly) &&
	      (tf_type != tf_readwrite) &&
	      (tf_type != tf_readonlyreal) &&
	      (tf_type != tf_readwritereal)) {
	    tbp_pli_panic_internal("display/print arg %d is not double type (type %d)\n", arg,tf_type);
	  }
	  sprintf(local_string,"%s%g",local_string,tf_getrealp(arg));
	  break;
	case 'E':
	case 'e':
	  if ((tf_type != tf_readonly) &&
	      (tf_type != tf_readwrite) &&
	      (tf_type != tf_readonlyreal) &&
	      (tf_type != tf_readwritereal)) {
	    tbp_pli_panic_internal("display/print arg %d is not float type (type %d)\n", arg,tf_type);
	  }
	  sprintf(local_string,"%s%e",local_string,tf_getrealp(arg));
	  break;
	case 'X':
	case 'H':
	case 'x':
	case 'h':
	  if ((tf_type != tf_readonly) &&
	      (tf_type != tf_readwrite) &&
	      (tf_type != tf_rwbitselect) &&
	      (tf_type != tf_rwpartselect) &&
	      (tf_type != tf_rwmemselect)) {
	    tbp_pli_panic_internal("display/print arg %d is not integer type (type %d)\n", arg,tf_type);
	  }  
	  
	  if(NULL==(expr_string=tf_strgetp(arg,'h')))
	    tbp_pli_panic_internal("display/print coult not retrieve argument %d\n",arg);
	  else {
	    expr_string = tbp_pli_string_strip_leading_space(expr_string);
	    sprintf(local_string,"%s%s",local_string,expr_string);
	  }

	  break;
	case 'S':
	case 's':
	  if ((tf_type !=tf_string) && 
	      (tf_type !=tf_readwrite) &&
	      (tf_type !=tf_readonly)) {
	    tbp_pli_panic_internal("display/print arg %d is not printable as a string (type = %d)\n", 
				   arg,tf_type);
	  }
	  
	  /* fetch the argument as a string */
	  if(NULL==(expr_string=tf_getcstringp(arg)))
	    tbp_pli_panic_internal("display/print coult not retrieve argument %d\n",arg);
	  else {
	    expr_string = tbp_pli_string_strip_leading_space(expr_string);
	    sprintf(local_string,"%s%s",local_string,expr_string);
	  }

	  break;
	case 'M':
	case 'm':
	  sprintf(local_string,"%s%s", local_string, tbp_get_current_module_name(NULL));
	  break;
	default:
	  strncat(local_string, fmt, 1);
	  continue;
	}
      }
    }	  
  }

  return(TRUE);
}

/************************************************************
 * tbp_pli_print - provide one common function and prototypes
 * for PLI's benifit for each of the variants...
 *  provides functions identical to those seen on the C-side
************************************************************/
static int tbpi_pli_message(char *group, int level, FFL_ARGS)
{
  static char     local_string[4096];

  tbp_pli_store_sim_time();
  tbp_pli_make_string(local_string);
  /* "error" messages and printf need a newline to match $display behavior */
  tbp_msg(TBP_PLI_FFL_DATA,group,level,(level==-1 ? "%s\n" : "%s"),local_string);
  
  return 0;
}

/* functions called by the pli */
int tbp_pli_printf(void) { return tbpi_pli_message(TBP_MSG_PLI_PRINTF, -1,TBP_PLI_FFL_DATA); }
int tbp_pli_info(void)   { return tbpi_pli_message(TBP_MSG_PLI_INFO,    0,TBP_PLI_FFL_DATA); }
int tbp_pli_debug(void)  { return tbpi_pli_message(TBP_MSG_PLI_DEBUG,  10,TBP_PLI_FFL_DATA); }
int tbp_pli_warn(void)   { return tbpi_pli_message(TBP_MSG_PLI_WARN,    0,TBP_PLI_FFL_DATA); }
int tbp_pli_error(void)  { return tbpi_pli_message(TBP_MSG_PLI_ERROR,  -1,TBP_PLI_FFL_DATA); }
int tbp_pli_panic(void)  { return tbpi_pli_message(TBP_MSG_PLI_PANIC,  -1,TBP_PLI_FFL_DATA); }

/* function to retrieve the argc from the simulator */
static void tbp_get_pli_args(void)
{ tbpi_set_sim_args(acc_fetch_argc(),(char **)acc_fetch_argv()); }

/* function used to perform any per simulator configuration */
void tbp_simulator_specific_init()
{
#ifdef TBP_MTI
  char *path_seperator;
#endif /* TBP_MTI */
#ifndef DUAL_PROC
  char *tbp_tty;
  FILE *instream, *outstream;
#endif /* !DUAL_PROC */

  /* this may be veistigial, but docs suggest it is still required to init pli 1.0 */
  acc_initialize();

  /* supress acc access errors */
  acc_configure(accDisplayErrors,"false");

  /* store argv/c away for internal use */
  tbp_get_pli_args();

#ifdef DUAL_PROC
  /* we are linked into a simulator, but we have a pipe transport to the "C side" */
  tbpi_set_transport_mode(TBP_TRANSPORT_MODE_DUAL);
#else  
  /* tbp is linked into a simulator (among other things impacts argv) */
  tbpi_set_transport_mode(TBP_TRANSPORT_MODE_SINGLE); 

  /* FIXME - do I need to check if this is a valid tty? */
  if(NULL != (tbp_tty = tbp_get_argv_key("tbptty"))) {
    
    //if(NULL==freopen(tbp_tty,"w+",stdin))
    //  tbp_panic("TBP Internal: Failed to redirect stdin\n");
    
    if(NULL==(instream= fopen(tbp_tty,"r+")))
      tbp_panic("TBP Internal: Failed to redirect stdin\n");
    else
      tbp_set_screen_input_stream(instream);
    
    //if(NULL==freopen(tbp_tty,"w+",stdout))
    //  tbp_panic("TBP Internal: Failed to redirect stdin\n");
    
    if(NULL==(outstream = fopen(tbp_tty,"w+")))
      tbp_panic("TBP Internal: Failed to redirect stdout\n");
    else
      tbp_set_screen_output_stream(outstream);
    
    
    if(NULL==freopen(tbp_tty,"w+",stderr))
      tbp_panic("TBP Internal: Failed to redirect stderr\n");
  }
#endif

#ifdef TBP_MTI
  path_seperator =  mti_FindProjectEntry ("vsim", "PathSeparator", 0);
  if(path_seperator==NULL)
    tbpi_set_path_seperator('/');  // MTI default
  else
    tbpi_set_path_seperator(path_seperator[0]);  // we only need the first charactor
#else
  tbpi_set_path_seperator('.');
#endif
}

#ifndef TBP_VCS

#if defined(TBP_NC)
#include "vxl_veriuser.h"
#endif

/*
 * This structure is used by nc/xl/mti to load the pli functions
 */
static s_tfcell 
tbpi_veriusertfs[] ={
  /***************************************************************/
  /* TestBenchPlus Phase 0 usertask entries */
  { usertask, 0, 0, 0, tbp_pli_register,  tbp_misc, "$tbp_register",  1},
  { usertask, 0, 0, 0, tbp_pli_transport, 0,        "$tbp_transport", 1},
  { usertask, 0, 0, 0, tbp_pli_get_data,  0,        "$tbp_get_data",  1},
  { usertask, 0, 0, 0, tbp_pli_put_data,  0,        "$tbp_put_data",  1},
  { usertask, 0, 0, 0, tbp_pli_finish,    0,        "$tbp_finish",    1},
  { usertask, 0, 0, 0, tbp_pli_printf,    0,        "$tbp_printf",    1},
  { usertask, 0, 0, 0, tbp_pli_info,      0,        "$tbp_info",      1},
  { usertask, 0, 0, 0, tbp_pli_debug,     0,        "$tbp_debug",     1},
  { usertask, 0, 0, 0, tbp_pli_warn,      0,        "$tbp_warn",      1},
  { usertask, 0, 0, 0, tbp_pli_error,     0,        "$tbp_error",     1},
  { usertask, 0, 0, 0, tbp_pli_panic,     0,        "$tbp_panic",     1},
  { usertask, 0, 0, 0, tbp_pli_printf,    0,        "$display",       1},
  { usertask, 0, 0, 0, tbp_pli_request,   0,        "$tbp_request",   1},
  { usertask, 0, 0, 0, tbp_pli_response,  0,        "$tbp_response",  1},
  { usertask, 0, 0, 0, tbp_pli_sighold,   0,        "$tbp_sighold",   1},
  /***************************************************************/
  {0}
};

#endif

/*
 ********************************************************
 * NC
 ********************************************************
 */
#if defined(TBP_NC) || defined(TBP_CVER)

p_tfcell tbp_pli_init()
{
  tbp_message_module_init();
  return(tbpi_veriusertfs);
}

#endif

/*
 ********************************************************
 * MTI
 ********************************************************
 */

#ifdef TBP_MTI

void init_usertfs() {
  p_tfcell usertf;
  
  for (usertf = tbpi_veriusertfs; usertf; usertf ++) {
    if (usertf -> type == 0) return;
    mti_RegisterUserTF(usertf);
  }
}
#endif

