//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "tbp_internal.h"
/* FIXME: Include tbp_pli.h to get tbp_get_current_module_name().  This
   will be fixed when the TBM branch is merged
   into main.  */
#include "tbp_pli.h"
#include "tbp_pipe_utils.h"

// NOTE - need comments here

/* flag to prevent message thrashing in normal mode */
static BOOL g_tbp_msg_debug= FALSE;
#define tbp_msg_debug if(g_tbp_msg_debug) tbp_info

/* internal static print buffers used to simplify/speed messaging */
static char print_data_buffer[TBP_MAX_PRINT_STRING];
static char print_output_buffer[TBP_MAX_OUTPUT_STRING];
static char print_debug_buffer[TBP_MAX_DEBUG_STRING];

/* strings used to simplify/speed printing code */
static char print_err_module_fmt_string[]   = {"***%s #%3d: %s: %s"};
static char print_err_nomodule_fmt_string[] = {"***%s #%3d: %s"};
static char print_err_info_fmt_string[]     = {"***%s    From: %s In: %s Line: %d Time: %lld\n\n"};
static char print_module_fmt_string[]       = {"%s: %s"};
static char print_nomodule_fmt_string[]     = {"%s"};
static char print_info_fmt_string[]         = {"***group: %s level: %d From: %s In: %s Line: %d Time: %lld\n\n"};

/*
 * function to initialize the message module
 */
void tbp_message_module_init(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  tc->screen_output_stream = stdout;
  tc->screen_input_stream = stdin;
  tc->detect_sim_errors = TRUE;
  tc->suppress_warnings = FALSE;
#ifdef DUAL_SIMSIDE
  tc->pli_screen_print = TRUE;
#else
  tc->pli_screen_print = FALSE;
#endif
  tc->pli_log_print = TRUE;
  tc->pli_cside_screen_print = FALSE;
  tc->pli_cside_log_print = FALSE;
  tc->message_debug_print = FALSE;
  tc->flush_logfile = TRUE;
  tc->log_level = 10;
  tc->screen_level = 0;
  tc->debug_grp_cnt = 0;
  tc->max_error_cnt = 0; // default to allowing infinite errors
  tc->max_warn_cnt = 0; // default to allowing infinite warnings
  tc->msg_user_error_func = NULL;  

  /* clear out all of the error and warning counters */
  tbp_error_clear();
  tbp_clear_accumulated_errors(); // multi-test error counter
  tbp_clear_accumulated_warnings(); // multi-test error counter

  /* add the basic groups to the group debug list */
  tbp_add_debug_group("tbp_msg_printf_dbg");
  tbp_add_debug_group("tbp_msg_info_dbg");
  tbp_add_debug_group("tbp_msg_debug_dbg");
  tbp_add_debug_group("tbp_msg_warn_dbg");
  tbp_add_debug_group("tbp_msg_error_dbg");
  tbp_add_debug_group("tbp_msg_panic_dbg");
  tbp_add_debug_group("tbp_msg_pli_printf_dbg");
  tbp_add_debug_group("tbp_msg_pli_info_dbg");
  tbp_add_debug_group("tbp_msg_pli_debug_dbg");
  tbp_add_debug_group("tbp_msg_pli_warn_dbg");
  tbp_add_debug_group("tbp_msg_pli_error_dbg");
  tbp_add_debug_group("tbp_msg_pli_panic_dbg");
}

/* functgion used to pass configuration information to the sim-side in dual mode */
void tbp_message_send_config(void)
{
#ifdef DUAL_CSIDE
  tbpi_pipe_send_cmd(FFL_DATA,TBP_PIPE_MSG_CONFIG);
#endif
  return;
}

void tbp_set_max_error_count(int maxerr) { tbp_get_conf()->max_error_cnt = maxerr; }
int tbp_get_max_error_count(void)        { return tbp_get_conf()->max_error_cnt; }

void tbp_set_max_warning_count(int maxwarn)  { tbp_get_conf()->max_warn_cnt = maxwarn; }
int tbp_get_max_warning_count(void)          { return tbp_get_conf()->max_warn_cnt; }

/* set/get the detect_sim_error state */
BOOL tbp_get_detect_sim_errors(void) { return tbp_get_conf()->detect_sim_errors; }
void tbp_set_detect_sim_errors(BOOL flag) { tbp_get_conf()->detect_sim_errors = flag; }

/* set/get suppress warnings state */
BOOL tbp_get_suppress_warnings(void) { return tbp_get_conf()->suppress_warnings; }
void tbp_set_suppress_warnings(BOOL flag) { tbp_get_conf()->suppress_warnings = flag; }

// FIXME - add detect sim-error flag!
int tbp_get_error_count() 
{
  TBP_CONF_T *tc=tbp_get_conf();
  return tc->detect_sim_errors ? tc->c_error_cnt + tc->sim_error_cnt : tc->c_error_cnt; 
}

/* get the C-side error count only  */
int tbp_get_c_error_count()   { return tbp_get_conf()->c_error_cnt; }

/* get the Sim-side error count only */
int tbp_get_sim_error_count() { return tbp_get_conf()->sim_error_cnt; }

/* get the warning count */
int tbp_get_warning_count()   
{ 
  TBP_CONF_T *tc=tbp_get_conf();  
  return tc->suppress_warnings ? 0 : tc->warn_cnt; 
}

/* function used to en/disable flushing of the logfiles on each write */
void tbp_set_flush_logfile(int flush) { 
  tbp_get_conf()->flush_logfile = flush; 
  tbp_message_send_config();
}

/* function to retrieve the state of the flush flag */
BOOL tbp_get_flush_logfile(void) { return tbp_get_conf()->flush_logfile; }

/* function to actually flush the log file */
void tbp_flush_logfile(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  if(tc->c_logfile!=NULL)
    fflush(tc->c_logfile);
  if(tc->sim_logfile!=NULL)
    fflush(tc->sim_logfile);
}

/* function used by outside libararies to get a pointer to the existing tbp logfile */
FILE* tbp_get_logfile() { return tbp_get_conf()->c_logfile; }

/* function used by outside libraries to force tbp to use an existing logfile pointer */
void tbp_set_c_logfile(FILE *tbp_logfile) { tbp_get_conf()->c_logfile = tbp_logfile; }

/* function used by outside libraries to force tbo to use an existing sim-side log-file */
void tbp_set_sim_logfile(FILE *tbp_logfile) { tbp_get_conf()->sim_logfile = tbp_logfile; }

/* increment the C-side error count */
int tbp_c_error_inc (void) 
{
  TBP_CONF_T *tc=tbp_get_conf();
  tc->c_error_cnt +=1;
  tc->accumulated_errors +=1;
  return tc->c_error_cnt;
}

/* increment the sim-side error count */
int tbp_sim_error_inc (void) 
{
  TBP_CONF_T *tc=tbp_get_conf();
  tc->sim_error_cnt +=1;
  tc->accumulated_errors +=1;
  return tc->sim_error_cnt;
}

/* increment the warning count */
int tbp_warn_inc(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  tc->warn_cnt +=1;
  tc->accumulated_warnings += 1;
  return tc->warn_cnt;
}

/* clear the error and warning count */
void tbp_error_clear(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  tc->c_error_cnt = 0;
  tc->sim_error_cnt = 0;
  tc->warn_cnt = 0;
  tbp_message_send_config();
}

/* get the accumulated error count (assumes multiple inc/clear cycles (tests) */
int tbp_get_error_count_all(void) { return tbp_get_conf()->accumulated_errors; }

/* get the accumulated warning count (assumes multiple inc/clear cycles (tests) */
int tbp_get_warning_count_all(void) { return tbp_get_conf()->accumulated_warnings; }

/* force the accumulated error counter to 0 */
void tbp_clear_accumulated_errors(void) { tbp_get_conf()->accumulated_errors = 0; }

/* force the accumulated error counter to 0 */
void tbp_clear_accumulated_warnings(void) { tbp_get_conf()->accumulated_warnings =0; }

/* set the screen debug level */
void tbp_set_debug_level(int debug_level) { tbp_get_conf()->screen_level = debug_level; }

/* set the log debug level */
void tbp_set_log_debug_level(int debug_level) { tbp_get_conf()->log_level = debug_level; }

/* get the screen debug level */
int tbp_get_debug_level(void) { return tbp_get_conf()->screen_level; }

/* get the log debug level */
int tbp_get_log_debug_level(void) { return tbp_get_conf()->log_level; }

/* remove a debug group from the table, if it exists */
int tbp_del_debug_group(char *group)
{
  TBP_CONF_T *tc=tbp_get_conf();
  int i,j;
  for(i=0;i<tc->debug_grp_cnt;i++){
    if(strcmp(tc->debug_groups[i],group)==0) {
      /* remove this item */
      tbp_free(tc->debug_groups[i]);
      /* collapse the list */
      if(i==(tc->debug_grp_cnt-1)) { // this is the last one in the list
	tbp_free(tc->debug_groups[i]);
      }
      else {
	/* copy all of the remaining items one location down */
	for(j=i+1;j<tc->debug_grp_cnt-1;j++) {
	  tc->debug_groups[i] = tc->debug_groups[i+1];
	}
      }
      /* now collapse the list of lists by one */
      tc->debug_grp_cnt -= 1;
      
      if(NULL==(tc->debug_groups = (char **)tbp_realloc(tc->debug_groups, sizeof(char *)*(tc->debug_grp_cnt), sizeof(char *)*(tc->debug_grp_cnt+1)))){
	tbp_panic("Failed to allocate space while removing a group %s (1)\n",group);
	return -1;
      }
      
      return 1;
    }
  }
  
  /* no match was found, inform the caller */
  return -1;
}

/* add a debug group to the list of active debug groups */
void tbp_add_debug_group(char *group)
{
  TBP_CONF_T *tc=tbp_get_conf();
  int i;
  
  /* walk through the array looking for a match - if none is found, add this one */
  for(i=0;i<tc->debug_grp_cnt;i++){
    if(strcmp(tc->debug_groups[i],group)==0) {
      return;  // do nothing, this group already exists 
    }
  }
  
  /* 
   * if we are here, then this group is new, add it to the list 
   * first expand the list itself 
   */
  if(NULL==(tc->debug_groups = (char **)tbp_realloc(tc->debug_groups, sizeof(char *)*(tc->debug_grp_cnt), sizeof(char *)*(tc->debug_grp_cnt+1)))){
    tbp_panic("Failed to allocate space required to add debug group %s (1)\n",group);
  }
  else {
    /* now allocate space for the group string */
    if(NULL==(tc->debug_groups[tc->debug_grp_cnt] = (char *)tbp_malloc(sizeof(char *)*(strlen(group)+1)))){
      tbp_panic("Failed to allocate space required to add debug group %s (2)\n",group);
    }
    else {
      /* now copy it in */
      strcpy(tc->debug_groups[tc->debug_grp_cnt],group);
      tc->debug_grp_cnt += 1; // increment the total count
      tbp_msg_debug("Added group %s to the debug groups (total groups %d)\n",
		 tc->debug_groups[tc->debug_grp_cnt-1],tc->debug_grp_cnt);
    }
  }
}
 
/* dump the current list of debug groups */
void tbp_list_debug_groups(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  int i;

  tbp_printf("\nlisting all active debug groups:\n");
  for(i=0;i<tc->debug_grp_cnt;i++){
    tbp_printf("%d: %s\n",i,tc->debug_groups[i]);
  }
}

/* function to enable/disable pli printing to the screen */
void tbp_set_sim_screen_print(BOOL print) { 
  tbp_get_conf()->pli_screen_print = print;  
  tbp_message_send_config();
}

/* function to enable/disable pli printing to the log */
void tbp_set_sim_log_print(BOOL print) { 
  tbp_get_conf()->pli_log_print = print;   
  tbp_message_send_config();
}

/* function to enable/disable pli printing to the c-side screen */
void tbp_set_sim_cside_screen_print(BOOL print) { 
  tbp_get_conf()->pli_cside_screen_print = print;  
  tbp_message_send_config();
}

/* function to enable/disable pli printing to the c-side log */
void tbp_set_sim_cside_log_print(BOOL print)    { 
  tbp_get_conf()->pli_cside_log_print = print;  
  tbp_message_send_config();
}

/* function to enable/diable adding the group,level,file,func,line debug to all messages */
void tbp_set_message_debug(BOOL enable) { 
  tbp_get_conf()->message_debug_print = enable;  
  tbp_message_send_config();
}

/* check to see if a given group is active (exists) */
BOOL tbp_check_debug_group(const char *group)
{
  TBP_CONF_T *tc=tbp_get_conf();
  int i;

  for(i=0;i<tc->debug_grp_cnt;i++){
    if(strcmp(group,tc->debug_groups[i])==0)
      return TRUE;
  }
  
  /* if we are here, then no match was found */
  return FALSE;
}

/* 
 * this function assigns the user error function which is called
 * anytime a warn/error/panic message is encountered
 * the type (warn/error/panic) is encoded as the input:
 * 2 = warn 1 = panic 0 = error
 */
void tbp_set_user_error_function(void (*func)(int)) 
{ tbp_get_conf()->msg_user_error_func = func; }

/* call the user error function (if one exists) */
void tbp_user_error_function(int type)
{
  TBP_CONF_T *tc=tbp_get_conf();
  
  if(tc->msg_user_error_func!=NULL) {
    (tc->msg_user_error_func)(type);
  }
}

/* function to assign the screen output stream */
void tbp_set_screen_output_stream(FILE *stream) 
{ tbp_get_conf()->screen_output_stream = stream; }

/* function to assign the screen input stream */
void tbp_set_screen_input_stream(FILE *stream) 
{ tbp_get_conf()->screen_input_stream = stream; }

/* functions to retrieve the in/out streams */
FILE *tbp_get_stdout(void) { return tbp_get_conf()->screen_output_stream; }
FILE *tbp_get_stdin(void) { return tbp_get_conf()->screen_input_stream; }

/*
 * messaging primitive function checks group and level to decide whether
 * or not ot print a given message.  This also allows masking od sim-side
 * messages (classified as "pli").  Lastly it calls the error functions 
 * provided by the user (if provided) and checks for exit/panic status
 */
int tbp_va_list_msg(const char *file, const char *func, int line, const char *group, int level, const char * fmt, va_list argp)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread = tbp_get_current_thread();
  u_int32 id;
  u_int64 current_time;
  BOOL pli=FALSE;
  BOOL log_print=FALSE,screen_print=FALSE;
  BOOL c_log_print=FALSE,c_screen_print=FALSE;
  BOOL sim_log_print=FALSE,sim_screen_print=FALSE;
  BOOL panic=FALSE,error=FALSE,warn=FALSE;
  BOOL group_exists=FALSE; 
  char *level_string[] = {"ERROR","PANIC","WARNING","NORMAL"};
  char *module_name=NULL;
  int level_index = 0;
  int error_cnt;

  /* check to see if this is a pli or C message */
  pli = strstr(group,TBP_MSG_PLI_TAG)==NULL ? FALSE : TRUE;
  
  /* Make sure that we are initialized before trying to output something */
  if(pli) tbp_init_config(TBP_SIMSIDE);
  else tbp_init(TBP_CSIDE);
  /*
   * Check the print level
   */
  group_exists = tbp_check_debug_group(group);  /*make sure this group exists*/
  log_print = ((level <= tc->log_level) && (group_exists==TRUE)) ? TRUE : FALSE;
  screen_print = ((level <= tc->screen_level) && (group_exists==TRUE))  ? TRUE : FALSE;

  /* start off printing everything to the screen */
  c_screen_print = sim_screen_print = screen_print;

  /* start by printing sim-side to the sim log and c-side to the c log*/
  c_log_print   = pli==TRUE ? FALSE : log_print;
  sim_log_print = pli==TRUE ? log_print : FALSE;
  

  /* 
   * if we are called from the pli side, we have 4 potential places to send our data
   * single process mode reduces this to 3, but we will account for that later 
   */
  if(pli==TRUE) {
    c_screen_print   = tc->pli_cside_screen_print ? sim_screen_print : FALSE;
    sim_screen_print = tc->pli_screen_print ? sim_screen_print : FALSE;
    
    c_log_print   = tc->pli_cside_log_print ? sim_log_print : FALSE;
    sim_log_print = tc->pli_log_print ? sim_log_print : FALSE;
  }

  /* set the debug, error and panic flags 0 for*/
  if(strstr(group,TBP_MSG_WARN_DBG_TAG)!=NULL){
    warn = TRUE;
    level_index = TBP_MSG_WARN_LEVEL;
    if(tc->suppress_warnings) {
      log_print = c_log_print = sim_log_print = FALSE;
      screen_print = c_screen_print = sim_screen_print = FALSE;
    }
    else {
      log_print = c_log_print = sim_log_print = TRUE;
      screen_print = c_screen_print = sim_screen_print = TRUE;
    }
  }
  else if(strstr(group,TBP_MSG_ERROR_DBG_TAG)!=NULL){
    error = TRUE;
    level_index = TBP_MSG_ERROR_LEVEL;
    log_print = c_log_print = sim_log_print = TRUE;
    screen_print = c_screen_print = sim_screen_print = TRUE;
  }
  else if(strstr(group,TBP_MSG_PANIC_DBG_TAG)!=NULL){
    panic = TRUE;
    level_index = TBP_MSG_PANIC_LEVEL;
    log_print = c_log_print = sim_log_print = TRUE;
    screen_print = c_screen_print = sim_screen_print = TRUE;
  }
  else {
    level_index = TBP_MSG_NORMAL_LEVEL;
  }
  
  /* is there any actual print io requested? */
  if(log_print || screen_print) {
    /* form the format print string (fmt, args.. from user) */
    vsprintf(print_data_buffer, fmt, argp);
    va_end(argp);
    
    /* get the current time */
    current_time = tbp_get_sim_time();

    /* print out the registered name if possible and allowed by the group */
    if(strstr(group,TBP_MSG_PRINTF_TAG)==NULL){
      if(pli==TRUE){
	/* 
	 * we must fetch the name directly from the sim as this function
	 * unlike most others is called form the pli without an id
	 */
	if((~0u)!=(id = tbp_get_trans_id_full(tbp_get_current_module_name(NULL))))
	  module_name = tc->trans[id].attached ? tc->trans[id].reg_name : tc->trans[id].full_name;
	else 
	  module_name = NULL;
      }
      else {
	if(tbp_is_main() || tbp_is_sim())
	  module_name = NULL;
	else 
	  module_name = tc->trans[thread->trans_id].reg_name;
      }
    }
    else {
      module_name = NULL;
    }

    /* error/panic/warn comes with additional formatting */
    if(error || panic || warn) {
      /* increment the error counters */
      if(warn) {
	tbp_warn_inc();
	error_cnt = tbp_get_warning_count();
      }
      else {
	if(pli) tbp_sim_error_inc();
	else tbp_c_error_inc();
	error_cnt = tbp_get_error_count();
      }

      /* setup the prefix string */
      if(module_name==NULL){
	/* print out the first "line" of the fully formated message */
	sprintf(print_output_buffer,print_err_nomodule_fmt_string,
		level_string[level_index],error_cnt,print_data_buffer);
      }
      else {
	/* print out the first "line" of the fully formated message - including the module name */
	sprintf(print_output_buffer,print_err_module_fmt_string,
		level_string[level_index],error_cnt,module_name,print_data_buffer);
      }
      
      /* print out the "debug" string of the fully formatted message */
      sprintf(print_debug_buffer,print_err_info_fmt_string,
	      level_string[level_index],func,file,line,current_time);
    }
    else {  /* normal messages add modue and potentially debug info */
      /* non error/warn/panic printing has no prefix other than potentially module_name */
      if(module_name==NULL){
	/* print out the first "line" of the fully formated message */
	sprintf(print_output_buffer,print_nomodule_fmt_string,print_data_buffer);
      }
      else {
	/* print out the first "line" of the fully formated message - including the module name */
	sprintf(print_output_buffer,print_module_fmt_string,module_name,print_data_buffer);
      }
      
      /* conditionally print out the "debug" string of the fully formatted message */
      if(tc->message_debug_print)
	sprintf(print_debug_buffer,print_info_fmt_string,group,level,func,file,line,current_time);
      else
	print_debug_buffer[0] = '\0';
    }

    /* 
     * Now send the message to each of the possible locations 
     */
    if(c_log_print==TRUE && tc->c_logfile!=NULL) 
      fprintf(tc->c_logfile,"%s%s",print_output_buffer,print_debug_buffer);
    if(sim_log_print==TRUE && tc->sim_logfile!=NULL) 
      fprintf(tc->sim_logfile,"%s%s",print_output_buffer,print_debug_buffer);
    if(c_screen_print==TRUE || sim_screen_print==TRUE)
      fprintf(tc->screen_output_stream,"%s%s",print_output_buffer,print_debug_buffer);

#ifdef DUAL_SIMSIDE
    //if(error || panic || warn)
    if(c_screen_print || c_log_print)
      tbpi_pipe_send_cmd(FFL_DATA,TBP_PIPE_PRINT,group,level_index,print_output_buffer,print_debug_buffer);
#endif
    
    /* "screen" flushing is unconditional */
    fflush(tc->screen_output_stream);
    
    /* allow disable of log flushing for performance tuning */
    if(tbp_get_flush_logfile())
      tbp_flush_logfile();
  }

  /* should check here for panic/error/warning shutdown */
  if(panic) { /* force a shutdown if we have paniced */
    tbp_user_error_function(TBP_MSG_PANIC_LEVEL);
    tbp_shutdown(-1);
    return tbp_get_error_count();
  }
  else if(error) {
    tbp_user_error_function(TBP_MSG_ERROR_LEVEL);
    /* 0 means never stop */
    if(tc->max_error_cnt > 0 && (tbp_get_error_count() >= tc->max_error_cnt)) {
      tbp_shutdown(0);
    }
    return tbp_get_error_count();
  }
  else if(warn) {
    tbp_user_error_function(TBP_MSG_WARN_LEVEL);
    /* 0 means never stop */
    if(tc->max_warn_cnt > 0 && (tbp_get_warning_count() >= tc->max_warn_cnt)) {
      tbp_shutdown(0);
    }
    return tbp_get_warning_count();
  }

  /* return true if we printed to anything... */
  return (log_print || screen_print) ? TRUE : FALSE;
}

static struct {
  char file[256];
  char function[256];
  int  line;
  char group[64];
  int  level;
} gMsgInfo;


void setTbpMsgInfo(const char* _file, const char* _func, int _line, const char* _group, int _level)
{
  strcpy(gMsgInfo.file, _file);
  strcpy(gMsgInfo.function, _func);
  gMsgInfo.line     = _line;
  strcpy(gMsgInfo.group, _group);
  gMsgInfo.level    = _level;

  if (g_tbp_msg_debug) 
    printf("settbMsgInfo: file= %s  function= %s  line= %d  group= %s  level= %d\n",
	   (( _file != NULL) ? _file: "NULL"),
	   (( _func != NULL) ? _func: "NULL"),
	   _line, 
	   (( _group != NULL) ? _group: "NULL"),
	   _level );
}

int tbp_msg(const char *file, const char *func, int line, const char *group, int level, ...)
{
  char *fmt;
  va_list _list;
  va_start(_list, level);
  fmt = va_arg(_list, char *);
  return tbp_va_list_msg(file, 
			 func, 
			 line, 
			 group, 
			 level, 
			 fmt, _list);
}

int tbp_new_msg(const char *fmt, ...)
{
  va_list _list;
  va_start(_list, fmt);
  return tbp_va_list_msg(gMsgInfo.file, 
			 gMsgInfo.function, 
			 gMsgInfo.line, 
			 gMsgInfo.group, 
			 gMsgInfo.level, 
			 fmt, _list);
}

int tbp_new_dmsg(const char* group, int level, const char *fmt,  ...)
{
  va_list _list;
  va_start(_list, fmt);
  return tbp_va_list_msg(gMsgInfo.file, 
			 gMsgInfo.function, 
			 gMsgInfo.line, 
			 group, 
			 level, 
			 fmt, _list);
}

int tbp_new_pdmsg( int level, const char *fmt, ...)
{
  va_list _list;
  va_start(_list, fmt);
  return tbp_va_list_msg(gMsgInfo.file, 
			 gMsgInfo.function, 
			 gMsgInfo.line, 
			 gMsgInfo.group, 
			 level, 
			 fmt, _list);
}

