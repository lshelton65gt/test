//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_EVT_H
#define TBP_EVT_H

#include "tbp_utils.h"

#ifdef __cplusplus
extern "C" {
#endif

/* wait event timeout types */
#define TBP_EVT_WAIT_NONE          0  // no event pending
#define TBP_EVT_WAIT_TIMEOUT_ERR   1  // issue an error on event timeout 
#define TBP_EVT_WAIT_TIMEOUT_NOERR 2  // no erorr on event timeout
#define TBP_EVT_WAIT_NO_TIMEOUT    3  // wait indefinitely
#define TBP_EVT_WAIT_DONE          4  // indicate event has been detected
#define TBP_EVT_WAIT_TIMEDOUT      5  // indicate event has timed out

/* wait event types (signal/state) */
#define TBP_EVT_WAIT_SIGNAL 1
#define TBP_EVT_WAIT_STATE  2

/* special event used to wait on the test */
#define TBP_EVT_HALT          "tbp_event_halt" // old is "halt"
#define TBP_EVT_TEST_COMPLETE "tbp_test_complete"
#define TBP_EVT_SLEEP         "tbp_evt_sleep"

/*
 * structure used for a given event
 */
typedef struct {
  int id;         // the id of this event (cosmetic only)
  char *name;     // the string name of this event
  int state;      // the state of the event
  int wait_cnt;   // the number of seperate thread waiting on this event
  int *wait_list; // a list of the threads waiting on this event
  void *nxt;      // linked list nxt ptr
  void *prev;     // linked list prev ptr (used to collapse)
} TBP_EVT_T;

/* function used to signal a sleeping transactor to wakeup */
void tbpi_wakeup(int trans_id, FFL_ARGS);

/* function to wake all waters */
void tbpi_evt_wakeup_all(FFL_ARGS);
#define tbp_wakeup_all() tbpi_evt_wakeup_all(FFL_DATA)

/* function used to assign the "wakeup function" */
void tbpi_set_wakeup_func(void (*wakeup_func)(int), FFL_ARGS);
#define tbp_set_wakeup_func(_func_) tbpi_set_wakeup_func(_func_, FFL_DATA)

/* list all outstanding events for debug */
void tbp_list_all_events(void);

/* trigger an event causing the first thread waiting for this event to wakeup */ 
void tbpi_evt_signal(const char *event, FFL_ARGS);
#define tbp_evt_signal(_event_) tbpi_evt_signal(_event_, FFL_DATA)

/* modify an event's state to match the provided value */
void tbpi_evt_signalstate(const char *event, int state, FFL_ARGS);
#define tbp_evt_signalstate(_event_,_state_) tbpi_evt_signalstate(_event_, _state_, FFL_DATA)

/* wait (blocking) for an event to occur - first come first served */
int tbpi_evt_wait(const char *event, s_int64 timeout, FFL_ARGS);
#define tbp_evt_wait(_event_, _timeout_) tbpi_evt_wait(_event_, (s_int64)_timeout_, FFL_DATA)

/* wait (blocking) for a given event to reach a given state */
int tbpi_evt_waitstate(const char *event, int state, s_int64 timeout, FFL_ARGS);
#define tbp_evt_waitstate(_event_, _state_, _timeout_) tbpi_evt_waitstate(_event_, _state_, (s_int64)_timeout_, FFL_DATA)

/* non-blocking check of an event's state - returns the current state */
int tbpi_evt_checkstate(const char *event, FFL_ARGS);
#define tbp_evt_checkstate(_event_) tbpi_evt_checkstate(_event_, FFL_DATA)

/* function to wait for either a test to complete, timeout error or halt */
void tbpi_evt_waittest(const char *event, s_int64 timeout, FFL_ARGS);
#define tbp_evt_waittet(_event_,_timeout_) tbpi_evt_waittet(_event_,_timeout_, FFL_DATA)

/* function to delete all outstanding events */
void tbpi_evt_delete_all(FFL_ARGS);
#define tbp_evt_delete_all() tbpi_evt_delete_all(FFL_DATA)

/* internal function to initialize the event module */
void tbp_evt_module_init(void);

/* internal function used to asign the name of the test completion event */
void tbp_set_test_complete_event(const char *event);
char *tbp_get_test_complete_event(void);

/* internal function to add / remove waiters due to event occurance */
void tbpi_evt_add_waiter(TBP_EVT_T *evt, int thread_index, FFL_ARGS);
void tbpi_evt_rm_waiter(TBP_EVT_T *evt, int thread_index, FFL_ARGS);

/* internal function to put a thread to sleep waiting for a given event to occur */
int tbpi_evt_sleep(TBP_EVT_T *evt, int thread_index, int wait_type,
		   int wait_evt_state, s_int64 timeout, FFL_ARGS);
/*
 * function used to put main to sleep (called by sim_sleep/waittest)
 */
void tbpi_main_sleep(u_int64 timeout, FFL_ARGS);
#define tbp_main_sleep(_timeout_) tbpi_main_sleep(_timeout_,FFL_DATA)

/* internal function to return an index to the event table holding a given event */
TBP_EVT_T *tbp_evt_table_search(const char *event);

/* internal function to retrieve the index of given event */
TBP_EVT_T *tbpi_evt_get(const char *event, FFL_ARGS);
#define tbp_evt_get(_event_) tbpi_evt_get(_event_, FFL_DATA)

/* internal function to delete the given index from the event table */
void tbpi_evt_delete(TBP_EVT_T *evt, FFL_ARGS);
#define tbp_evt_delete(_evt_) tbpi_evt_delete(_evt_,FFL_DATA)

/* internal functions to wakeup waiting threads */
void tbpi_evt_wake_one(TBP_EVT_T *evt, FFL_ARGS);
void tbpi_evt_wake_any(TBP_EVT_T *evt, int state, FFL_ARGS);

/* internal function used by the scheduler to see if a thread is waiting */
int tbp_evt_check_thread_wait(u_int32 thread_index);

#ifdef __cplusplus
}
#endif

#endif


