//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/* C/C++ interface to cmodel. */
#ifndef __CDS_CM_tbp_carbonif_H_
#define __CDS_CM_tbp_carbonif_H_

//#define TBP_SIMSIDE 1

#ifdef __cplusplus
extern "C" {
#endif

  /* @fixme The following three declarations are copied from other TBP
     include files.  They are included here to avoid having to include
     these other TBP include files in tbp_carbonif.c.  Note, we use
     UInt64 as a synonym/equivalent to u_int64.  */
  //  void tbp_init_config(int caller);
  //  void tbpi_set_current_hdl_time(UInt64 time);

  //  void tbp_record_module_presence_carbon(char * myinst);

  int tbp_cds_register   (void *name, UInt32 *id);
  int tbp_cds_transport(const UInt32 *id,
			const UInt32 *put_op,  const UInt64 *put_ad,  const UInt32 *put_sz,   const UInt32 *put_st,
			const UInt32 *put_int, const UInt32 *put_rst, const UInt64 *put_strt, const UInt64 *put_end,
			UInt32 *get_op,  UInt64 *get_ad,  UInt32 *get_sz,   UInt32 *get_st,
			UInt32 *get_int, UInt32 *get_rst);
  int tbp_cds_put_data32 (const UInt32 *id, const UInt32 *index, const UInt32 *data);
  int tbp_cds_put_data64 (const UInt32 *id, const UInt32 *index, const UInt64 *data);
  int tbp_cds_get_data32 (const UInt32 *id, const UInt32 *index, UInt32 *data);
  int tbp_cds_get_data64 (const UInt32 *id, const UInt32 *index, UInt64 *data);
  int tbp_cds_pli_error  (const char *msg);

#ifdef __cplusplus
}
#endif

#endif // __CDS_CM_tbp_carbonif_H_
