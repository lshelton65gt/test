//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include <errno.h>
#include <signal.h>
#include "tbp_internal.h"
#include "util/c_util.h"

/* flag to limit message thrashing in non-debug mode */
static u_int32 g_tbp_thread_debug = FALSE;
#define tbp_thread_debug if(g_tbp_thread_debug) tbp_printf

static void *tbp_thread(void *thread_index);

/* return our thread id */
CONTEXT *tbp_thread_self(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  return tbp_get_thread(tc->current_thread)->thread_id;
}

/* allocate/initialize cond and mutex */
void tbp_thread_allocate(u_int32 thread_index)
{
  TBP_THREAD_T *thread = tbp_get_thread(thread_index);
  thread->thread_id = (CONTEXT *) tbp_allocate_buffer(sizeof(CONTEXT),"Thread Context");
}

/* function used to copy the contents of a thread from one location to another */
void tbp_thread_copy(u_int32 dst_index, u_int32 src_index)
{
  TBP_THREAD_T *dst_thread = tbp_get_thread(dst_index);
  TBP_THREAD_T *src_thread = tbp_get_thread(src_index);
  
  dst_thread->id             = src_thread->id;     
  dst_thread->thread_id      = src_thread->thread_id;
  dst_thread->stk_ptr        = src_thread->stk_ptr;
  dst_thread->trans_id       = src_thread->trans_id;      
  dst_thread->type	     = src_thread->type;         
  dst_thread->status         = src_thread->status;        
  dst_thread->wait_evt	     = src_thread->wait_evt;   
  dst_thread->wait_evt_state = src_thread->wait_evt_state;
  dst_thread->wait_evt_type  = src_thread->wait_evt_type; 
  dst_thread->timeout	     = src_thread->timeout;       
  dst_thread->func_ptr	     = src_thread->func_ptr;      
  dst_thread->int_func_ptr   = src_thread->int_func_ptr;  
  dst_thread->rst_func_ptr   = src_thread->rst_func_ptr;  
  dst_thread->usr_ptr	     = src_thread->usr_ptr;      
  dst_thread->num_args       = src_thread->num_args; 
}

/* free any resources associated with this thread */
void tbp_thread_free(u_int32 thread_index)
{
  TBP_THREAD_T *thread = tbp_get_thread(thread_index);
 
  tbp_free(thread->thread_id);
  tbp_free(thread->stk_ptr);
}

/* cleanup/destroy any state associated with this thread as it shutsdown */	
void tbp_thread_cleanup(u_int32 thread_index)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread = tbp_get_thread(thread_index);
  BOOL thread_killed = thread->status==TBP_THREAD_STATUS_KILL ? TRUE:FALSE;
  
  tbp_thread_debug("thread %d: cleanup\n",thread_index);

  /* if this is the "sim" thread, then we can skip the cleanup and exit gracefully */
  if(thread_index == tc->sim_thread_id){
    tbp_shutdown(0); /* note - this code will be ignored for now */
  }
  else {

    /* clear out any state from this thread for future use */
    tbp_thread_struct_cleanup(thread_index);
    
    /* 
     * wakeup the "sim" thread" if we are just exiting, otherwise, we have
     * been killed and we need to wakeup main
     */
    CONTEXT* context = thread_killed ? tbp_get_thread(tc->main_thread_id)->thread_id : tbp_get_thread(tc->sim_thread_id)->thread_id;
    context->ContextFlags = CONTEXT_FULL;
    if(SetThreadContext(GetCurrentThread(), context) == 0)
      tbp_panic("tbp_thread_cleanup: Failed to set thread context.\n");
  }
}

/* implimentation specific wrapper for the thread */
static void *tbp_thread(void *thread_index)
{
  /* lower level function is common to p/qthreads */
  return tbp_thread_wrapper(thread_index);
}

/*
 * create a new thread - wait for it to return control after its initial execution
 */
int tbp_thread_create(u_int32 thread_index)
{
  char errBuf[1024];
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread = tbp_get_thread(thread_index);

  tbp_thread_debug("thread %d: thread_create new thread %d start\n",tbp_get_my_thread_id(),thread_index); 

  thread->status = TBP_THREAD_STATUS_RUN;

  /* initialize this new context with the current context's state - will be overwritten */
  thread->thread_id->ContextFlags = CONTEXT_FULL;
  if(GetThreadContext(GetCurrentThread(), thread->thread_id) == 0)
    tbp_panic("tbp_thread_create: Failed to get thread context during initialization (%s)\n",carbon_lasterror(errBuf, sizeof(errBuf)));

  /* allocate/align the stack pointer (128bit alignment) */
  tc->stack_size = ((tc->stack_size+15)/16)*16;
  thread->stk_ptr = (void *)tbp_allocate_buffer(tc->stack_size,"Thread Stack");
  
  /* Calculate stack pointer. Reserve space for function argument.
     Stack grows down */
  char* sp = (char *) (size_t) thread->stk_ptr + tc->stack_size - sizeof(u_int32*);	

  /* Setup context to execute tbp_thread(&thread->id); */
  /* Set Instruction Pointer */
  thread->thread_id->Eip = (unsigned long)tbp_thread;

  /* Set Stack Pointer */
  thread->thread_id->Esp = (unsigned long) sp - 4;
  
  /* Copy function argument to the stack */
  *((u_int32**)sp) = &thread->id;

  /* wake the thread up to do one handshake (allows passage of the index) */
  tbp_thread_debug("thread %d: waking up thread %d to allow it to obtain its index\n",
		   tbp_get_my_thread_id(),thread_index);
  tbp_thread_activate(tbp_get_my_thread_id(),thread_index);
  tbp_thread_debug("thread %d: thread %d has returned from its initial run\n",tbp_get_my_thread_id(),thread_index);

  return 0;
}

/*
 * sleep the calling thread and wake the target thread
 */
void tbp_thread_activate(u_int32 calling_thread, u_int32 target_thread)
{
  TBP_CONF_T *tc=tbp_get_conf();

  tbp_thread_debug("Calling thread %d is activating target_thread %d\n",calling_thread, target_thread);

  /* switch to the target thread */
  tbp_get_thread(calling_thread)->thread_id->ContextFlags = CONTEXT_FULL;
  if(GetThreadContext(GetCurrentThread(), tbp_get_thread(calling_thread)->thread_id) == 0)
    tbp_panic("tbp_thread_activate: Failed to get thread context.\n");
  else {
    tbp_get_thread(target_thread)->thread_id->ContextFlags = CONTEXT_FULL;
    if(SetThreadContext(GetCurrentThread(), tbp_get_thread(target_thread)->thread_id) == 0)
      tbp_panic("tbp_thread_activate: Failed to set thread context.\n");
  }
  /* Need to set the current thread after returning to this context */
  tc->current_thread = calling_thread;

  /* assign the global for fast access later - reassigns the id if relocated */
  tbp_set_current_thread(&calling_thread);

  /* now check to see if we need to complete a thread relocation */
  tbp_check_thread_relocate(calling_thread);

  /* check to see if this thread has been asked to die */
  tbp_check_thread_exit(calling_thread);

  tbp_thread_debug("thread %d: thread running again\n",calling_thread);
  
  if(calling_thread > tc->main_thread_id) {
    /* if reset is !=0, call the reset handler prior to waking up the thread */
    tbp_check_reset(calling_thread);
    
    /* if interrupt is !=0, call the interrupt handler prior to waking up the thread */
    tbp_check_interrupt(calling_thread);
  }
}

/* special wait used in thread relocation */
void tbp_thread_wakeup_main(u_int32 calling_thread)
{
  TBP_CONF_T *tc=tbp_get_conf();
  
  tbp_thread_debug("thread_wakeup_main\n"); 

  /* switch to the main thread */
  tbp_get_thread(calling_thread)->thread_id->ContextFlags = CONTEXT_FULL;
  if(GetThreadContext(GetCurrentThread(), tbp_get_thread(calling_thread)->thread_id) == 0)
    tbp_panic("tbp_thread_activate: Failed to get thread context.\n");
  else {
    tbp_get_thread(tc->main_thread_id)->thread_id->ContextFlags = CONTEXT_FULL;
    if(SetThreadContext(GetCurrentThread(), tbp_get_thread(tc->main_thread_id)->thread_id) == 0)
      tbp_panic("tbp_thread_activate: Failed to set thread context.\n");
  }

  tbp_set_current_thread(&calling_thread);
}
  
/* 
 * return the id of the transactor with the matching name, if NULL
 * then get the "current" name
 */
u_int32 tbp_get_thread_id(char *name)
{
  TBP_CONF_T *tc=tbp_get_conf();
  
  /* if the name is null - just get the current id */
  if(name==NULL) {
    return tc->current_thread;
  }
  else {
    tbp_printf("id lookup by name not yet supported\n");
    /* look up the name */
    return 0;
  }
}

