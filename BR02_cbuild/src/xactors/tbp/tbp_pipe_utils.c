//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include <signal.h>
#include "tbp_internal.h"
#include "tbp_pli.h"
#include "tbp_pipe.h"
#include "tbp_pipe_utils.h"

typedef void (*sighandler_t)(int);

static int g_tbp_pipe_debug = FALSE;
#ifdef DUAL_SIMSIDE
#define tbp_pipe_debug(args...) \
        if(g_tbp_pipe_debug==TRUE) fprintf(stderr,##args); \
        if(g_tbp_pipe_debug==TRUE) fprintf(tbp_get_conf()->sim_logfile, ##args)
#else
#define tbp_pipe_debug(args...) \
        if(g_tbp_pipe_debug==TRUE) fprintf(stderr,##args); \
        if(g_tbp_pipe_debug==TRUE) fprintf(tbp_get_conf()->c_logfile, ##args)
#endif

static char *pipe_cmd_name[] ={
  "TBP_PIPE_INVALID",
  "TBP_PIPE_SEND_SIMTIME",   
  "TBP_PIPE_SEND_TRANS_TABLE", 
  "TBP_PIPE_ATTACH_TRANS",
  "TBP_PIPE_DETACH_TRANS",    
  "TBP_PIPE_TRANSPORT",     
  "TBP_PIPE_THREAD_WAKE",    
  "TBP_PIPE_TRANS_WAKE",    
  "TBP_PIPE_SET_THREAD_ID", 
  "TBP_PIPE_SET_SIGNAL_CMD",
  "TBP_PIPE_SET_SIGNAL_RTN",
  "TBP_PIPE_GET_SIGNAL_CMD",
  "TBP_PIPE_GET_SIGNAL_RTN",
  "TBP_PIPE_PRINT",       
  "TBP_PIPE_PRINT_RTN",       
  "TBP_PIPE_MSG_CONFIG", 
  "TBP_PIPE_SIM_ERROR",     
  "TBP_PIPE_RUN_SIM",       
  "TBP_PIPE_RUN_CMODEL",     
  "TBP_PIPE_STOP_SIM",       
  "TBP_PIPE_SHUTDOWN",
  "TBP_PIPE_RETURN"};      

static void *g_pipe_signal_value=NULL;

/*
 * function to en/disable debug printing within this module
 */
void tbp_set_pipe_debug_flag(int flag) { g_tbp_pipe_debug = flag; }

/* 
 * function to send a structured req/cmd throug the pipe
 */
int tbpi_pipe_send_cmd(FFL_ARGS, int cmd ,...)
{  
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_OP_T *operation=NULL;
  va_list ap;
  int i;
  int write_size;
  int trans_id;
  int thread_id;
  char *trans_name;
  u_int64 current_sim_time;
  u_int64 main_timeout;
  int ctrans_cnt;
  int path_length;
  int signal_type;
  int signal_force;
  char *signal_path;
  void *signal_value;
  void **signal_ptr;
  int signal_ptr_size;
  int ret_val = 0;
  char path_seperator;
  int main_is_waiting;
  //char print_buf[4096];
  char *print_buf;
  char *print_debug_buf;
  char *print_group;
  int print_length;
  int print_level_index;
  int print_debug_length;
  int pli_print;
  int ret_code;
  int acc_error_enable;
  sigset_t blockInt;
  
  /* do nothing if we are in no-hdl mode */
  if(tbp_get_no_hdl_flag()) return 0;
  
  /* do nothing if we are a ctrans */
  if(tbp_is_ctrans()) return 0;

  /* if the pipe is not connected - do nothing */
  if(!tbp_pipe_connected()) return 0;

  va_start(ap, cmd);
  //va_arg(ap, void *); /* fetch the function pointer */
  //va_end(ap);

  /* The following three lines should be the equivalent of sighold()
     which is being phased out on Linux.  */
  //  sighold(SIGINT);
  sigemptyset(&blockInt);
  sigaddset(&blockInt, SIGINT);
  sigprocmask(SIG_BLOCK, &blockInt, NULL);

  /* send the command */
  tbp_pipe_write((void *) &cmd, sizeof(int));

  tbp_pipe_debug("send_cmd (%d %s) starting (%s %s %d)\n",cmd,pipe_cmd_name[cmd],iFFL_DATA);
  
  /* now decode what to send as the body of this command if anything... */
  switch(cmd)
    {    
    case TBP_PIPE_SEND_SIMTIME:
      /* send the sim-time */
      current_sim_time = tbp_get_sim_time();
      tbp_pipe_write((void *) &current_sim_time, sizeof(u_int64));
      break;
    case TBP_PIPE_SEND_TRANS_TABLE:
      /* send the trans count */
      tbp_pipe_write((void *) &tc->trans_cnt, sizeof(int));
      
      /* now loop through and send each of the transactor's full_names */
      for(i=0;i<tc->trans_cnt;i++) {
	/* send the string length first */
	write_size = strlen(tc->trans[i].full_name)+1;
	tbp_pipe_write((void *) &write_size, sizeof(int));
	tbp_pipe_write((void *)tc->trans[i].full_name,write_size);
      }

      /* send the path seperator */
      path_seperator = tbp_get_path_seperator();
      tbp_pipe_write((void *) &path_seperator,sizeof(char));
      break;
    case TBP_PIPE_ATTACH_TRANS:
      /* send the trans_id */
      trans_id = va_arg(ap, int);      
      tbp_pipe_write((void *) &trans_id, sizeof(int));
      
      /* send the thread id */
      thread_id = va_arg(ap, int);
      tbp_pipe_write((void *) &thread_id, sizeof(int));
      
      /* send the length of the name used to attach */
      trans_name = va_arg(ap, char *);
      write_size = strlen(trans_name)+1;
      tbp_pipe_write((void *) &write_size, sizeof(int));
      
      /* send the name used to attach */
      tbp_pipe_write((void *) trans_name, write_size);
      break;
    case TBP_PIPE_DETACH_TRANS:
      /* send the trans id */
      trans_id = va_arg(ap, int);
      tbp_pipe_write((void *) &trans_id, sizeof(int));
      break;
    case TBP_PIPE_TRANSPORT: /* send an operation down to the simulator */
      /* send the trans id */
      trans_id = va_arg(ap, int);
      tbp_pipe_write((void *) &trans_id, sizeof(int));

      /* send the operation struct */
      operation = tbpi_trans_operation(trans_id);
      tbp_pipe_write((void *) operation, sizeof(TBP_OP_T));
	
      /* depending on the state of the operation - move the data buffers */
      if(operation->direction==TBP_OP_BIDIR || operation->direction==TBP_OP_WRITE) {
	/* move the write data down from the C-side */
	write_size = (((operation->size+7)/8) * 8) + 16; /* 64bit aligned buffer + tbp3x padding */
	tbp_pipe_write((void *) operation->write_data, write_size);
	/* datax */
	tbp_pipe_write((void *) operation->write_datax, write_size);
      }
      break;
    case TBP_PIPE_THREAD_WAKE: /* send an operation up to the C-side */
      /* send the current time */
      current_sim_time = tbp_get_sim_time();
      tbp_pipe_write((void *) &current_sim_time, sizeof(u_int64));

      /* send the thread id */
      thread_id = va_arg(ap, int);
      tbp_pipe_write((void *) &thread_id, sizeof(int));
      
      /* if this is an attached thread (i.e. not main!) - move op/data */
      if(thread_id > tbpi_main_thread_id()){
	/* send the operation struct */
	trans_id = va_arg(ap, int);
	operation = tbpi_trans_operation(trans_id);
	tbp_pipe_write((void *) operation, sizeof(TBP_OP_T));
	
	tbp_pipe_debug("waking up thread %d (attached to trans %d - %s) at time %lld\n",
		       thread_id,trans_id,tbp_get_trans_name(trans_id),tbp_get_sim_time());
	
	/* depending on the state of the operation - move the data buffers */
	if(operation->direction==TBP_OP_BIDIR || operation->direction== TBP_OP_READ) {
	  /* move the read data up to the C-side */
	  write_size = (((operation->size+7)/8) * 8) + 16; /* 64bit aligned buffer + tbp3x padding */
	  tbp_pipe_write((void *) operation->read_data, write_size);
	  /* datax */
	  tbp_pipe_write((void *) operation->read_datax, write_size);
	}
      }
      else {
	tbp_pipe_debug("waking up thread %d at time %lld\n",
		       thread_id,tbp_get_sim_time());
      }
      break;
    case TBP_PIPE_TRANS_WAKE:
      /* send the trans_id */
      trans_id = va_arg(ap, int);
      tbp_pipe_write((void *) &trans_id, sizeof(int));
      break;
    case TBP_PIPE_SET_THREAD_ID: /* used to update the pli-side trans table when a thread moves... */
      /* send the trans_id */
      trans_id = va_arg(ap, int);
      tbp_pipe_write((void *) &trans_id, sizeof(int));

      /* send the new thread_id */
      thread_id = va_arg(ap, int);
      tbp_pipe_write((void *) &thread_id, sizeof(int));

      tbp_pipe_debug("updating trans id %d with new thread id %d\n",trans_id,thread_id);
      break;
    case TBP_PIPE_SET_SIGNAL_CMD:
      /* send the acc error enable flag */
      acc_error_enable = tbpi_get_acc_error_enable();
      tbp_pipe_write((void *) &acc_error_enable, sizeof(int));

      /* send the path length */
      signal_path = va_arg(ap, char *);
      path_length = strlen(signal_path) + 1;
      tbp_pipe_write((void *)&path_length, sizeof(int));

      /* send the path */
      tbp_pipe_write((void *) signal_path, path_length);

      /* clear the signal_path */
      signal_path = NULL;
      
      /* get the signal value */
      signal_value = va_arg(ap, void *);

      /* get the type - send it later! */
      signal_type = va_arg(ap, int);

      if(signal_value==NULL) {
	signal_ptr_size = 0;
      }
      else {
	/* send the size of the pointer */
	switch(signal_type)
	  {
	  case TBP_SET_SCL:
	  case TBP_SET_INT:
	    signal_ptr_size = sizeof(int);
	    break;
	  case TBP_SET_REAL: 
	    signal_ptr_size = sizeof(double);
	    break;
	  case TBP_SET_BIN:
	  case TBP_SET_OCT:
	  case TBP_SET_DEC:
	  case TBP_SET_HEX:
	    signal_ptr_size = strlen(signal_value)+1;
	    break;
	  default:
	    signal_ptr_size = 0;
	    tbp_error_internal("set_signal_value: invalid argument type (%d)\n",signal_type);
	  break;
	  }
      }

      /* send the size */
      tbp_pipe_write((void *) &signal_ptr_size, sizeof(int));
      
      /* send the pointer itself */
      if(signal_ptr_size != 0)
	tbp_pipe_write((void *) signal_value,signal_ptr_size);

      /* send the signal type */
      tbp_pipe_write((void *) &signal_type, sizeof(int));

      /* get the force flag and send it */
      signal_force = va_arg(ap, int);
      tbp_pipe_write((void *) &signal_force, sizeof(int));
      break;
    case TBP_PIPE_SET_SIGNAL_RTN:
      /* write the return value */
      ret_val = va_arg(ap, int);
      tbp_pipe_write((void *) &ret_val, sizeof(int));

      /* send the error return value */
      ret_code = tbp_sim_setget_return();
      tbp_pipe_write((void *) &ret_code, sizeof(int));
      break;
    case TBP_PIPE_GET_SIGNAL_CMD:
      /* send the acc error enable flag */
      acc_error_enable = tbpi_get_acc_error_enable();
      tbp_pipe_write((void *) &acc_error_enable, sizeof(int));

      /* send the path length */
      signal_path = va_arg(ap, char *);
      path_length = strlen(signal_path) + 1;
      tbp_pipe_write((void *)&path_length, sizeof(int));
      
      /* send the path */
      tbp_pipe_write((void *) signal_path, path_length);
      
      /* clear the signal_path */
      signal_path = NULL;

      /* send the type */
      signal_type = va_arg(ap, int);
      tbp_pipe_write((void *) &signal_type, sizeof(int));
      break;
    case TBP_PIPE_GET_SIGNAL_RTN:
      /* get/send the size of the signal */
      signal_ptr_size = va_arg(ap, int);
      tbp_pipe_write((void *) &signal_ptr_size, sizeof(int));

      /* get/send the signal value */
      if(signal_ptr_size > 0) {
	signal_ptr = va_arg(ap, void *);
	tbp_pipe_write((void *) signal_ptr, signal_ptr_size);
      }
      
      /* send the error return value */
      ret_code = tbp_sim_setget_return();
      tbp_pipe_write((void *) &ret_code, sizeof(int));
      break;
    case TBP_PIPE_PRINT:
      /* get/send the current sim_time */
      current_sim_time = tbp_get_sim_time();
      tbp_pipe_write((void *) &current_sim_time, sizeof(u_int64));
      
      /* is pli group? */
      print_group = va_arg(ap, char *);
      pli_print = strstr(print_group,"pli") ? TRUE : FALSE;
      tbp_pipe_write((void *) &pli_print, sizeof(int));
      
      /* get/send the level index */
      print_level_index = va_arg(ap, int);
      tbp_pipe_write((void *) &print_level_index, sizeof(int));

      /* get the string we are going to print */ 
      print_buf = va_arg(ap, char *);

      /* get the debug string we are going to print */ 
      print_debug_buf = va_arg(ap, char *);

      /* send the length */
      print_length = strlen(print_buf)+1;
      tbp_pipe_write((void *) &print_length, sizeof(int));
      
      /* send the length of the debug buf */
      print_debug_length = strlen(print_debug_buf)+1;
      tbp_pipe_write((void *) &print_debug_length, sizeof(int));

      /* now send the string */
      if(print_length > 0)
	tbp_pipe_write((void *) print_buf, print_length);

      /* now send the debug string */
      if(print_debug_length > 0)
	tbp_pipe_write((void *) print_debug_buf, print_debug_length);

      /* once we are done, we will return to the simulation from the caller of $tbp_print */    
      tbpi_pipe_get_cmd(iFFL_DATA);
      break;
    case TBP_PIPE_PRINT_RTN:
      /* send the main timeout value - this may have ben altered during the print */
      main_timeout = tbpi_get_main_timeout();
      tbp_pipe_write((void *) &main_timeout, sizeof(u_int64));
      break;
    case TBP_PIPE_MSG_CONFIG:
      /* send the following */
      //tbp_pipe_write((void *) &tc->pli_screen_print, sizeof(int));
      tbp_pipe_write((void *) &tc->pli_log_print, sizeof(int));
      tbp_pipe_write((void *) &tc->pli_cside_screen_print, sizeof(int));
      tbp_pipe_write((void *) &tc->pli_cside_log_print, sizeof(int));
      tbp_pipe_write((void *) &tc->message_debug_print, sizeof(int));
      tbp_pipe_write((void *) &tc->flush_logfile, sizeof(int));
      tbp_pipe_write((void *) &tc->c_error_cnt, sizeof(int));
      tbp_pipe_write((void *) &tc->sim_error_cnt, sizeof(int));
      tbp_pipe_write((void *) &tc->warn_cnt, sizeof(int)); 
      break; 
    case TBP_PIPE_SIM_ERROR:
      /* get/send the level index */
      print_level_index = va_arg(ap, int);
      tbp_pipe_write((void *) &print_level_index, sizeof(int));
      break;
    case TBP_PIPE_RUN_SIM: 
      /* called by main to pass main timeout and return to the simulator context*/
      /* send the main timeout value */
      main_timeout = tbpi_get_main_timeout();
      tbp_pipe_write((void *) &main_timeout, sizeof(u_int64));

      /* send the state of the main_is_waiting flag */
      main_is_waiting = tbpi_main_is_waiting();
      //fprintf(stderr,"run_sim: main_is waiting %d %d\n",main_is_waiting,main_timeout);
      tbp_pipe_write((void *) &main_is_waiting, sizeof(int));

      /* send the current ctrans_cnt */
      ctrans_cnt = tbp_get_ctrans_count();
      tbp_pipe_write((void *) &ctrans_cnt, sizeof(int));      
      break;
    case TBP_PIPE_RUN_CMODEL:
    case TBP_PIPE_STOP_SIM:
    case TBP_PIPE_SHUTDOWN:
      break;
    case TBP_PIPE_RETURN:
      /* send the return value, whatever it is */
      ret_val = va_arg(ap, int);
      tbp_pipe_write((void *) &ret_val, sizeof(int));
      break;
    default:
      tbp_panic_internal("unknown pipe command! (%d)\n",cmd);
      break;
    }

  //sigrelse(SIGINT);
  sigprocmask(SIG_UNBLOCK, &blockInt, NULL);

  tbp_pipe_debug("send_cmd (%d %s) finished\n",cmd,pipe_cmd_name[cmd]);
  va_end(ap);
  
  return ret_val;
}

int tbpi_pipe_get_cmd(FFL_ARGS,...)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_TRANS_T *trans;
  TBP_OP_T operation, *op;
  va_list ap;
  BOOL done=FALSE;
  int i;
  int cmd;
  int cnt;
  char read_buf[4096];
  int read_size;
  int trans_id;
  int thread_id;
  u_int64 current_sim_time;
  u_int64 main_timeout;
  int ctrans_cnt;
  int path_length;
  int signal_type;
  int signal_force;
  char *signal_path;
  void *signal_value; 
  void **signal_ptr;
  int signal_ptr_size;
  int signal_rtn;
  char path_seperator;
  int main_is_waiting;
  int print_length;
  char *print_buf = NULL;
  int print_level_index;
  int print_debug_length;
  int pli_print;
  void *ret_ptr;
  void *ret_code;
  int acc_error_enable;
  char *level_string[] = {"ERROR","PANIC","WARNING","NORMAL"};
  sigset_t blockInt;

  /* do nothing if we are in no-hdl mode */
  if(tbp_get_no_hdl_flag()) return 0;

  /* loop through looking for something to do.. */
  while(!done && !tc->exit_flag){ 
    /* if the pipe is not (or no longer) connected - do nothing */
    if(!tbp_pipe_connected()) return 0;

    /* The following three lines should be the equivalent of sighold()
       which is being phased out on Linux.  */
    //  sighold(SIGINT);
    sigemptyset(&blockInt);
    sigaddset(&blockInt, SIGINT);
    sigprocmask(SIG_BLOCK, &blockInt, NULL);
    
    /* get a command */    
    tbp_pipe_debug("getting a command (%s %s %d)\n",iFFL_DATA);
    tbp_pipe_read(&cmd);
    tbp_pipe_debug("get_cmd (%d %s) starting\n",cmd,pipe_cmd_name[cmd]);

    /* interpret this command */
    switch(cmd)
      { 
      case TBP_PIPE_SEND_SIMTIME:
	/* fetch the current sim-time */
	tbp_pipe_read(&current_sim_time);
	/* store it */
	tbpi_set_current_hdl_time(current_sim_time);
	done = FALSE;
	break;
      case TBP_PIPE_SEND_TRANS_TABLE:
	/* get the trans count */
	tbp_pipe_read(&cnt);
	
	/* loop through recording and registering the transactors */
	for(i=0;i<cnt;i++){
	  /* get the read_length first */
	  tbp_pipe_read(&read_size);
	  /* now read the string */
	  tbp_pipe_read(read_buf);
	  
	  /* if it is valid, record it and register it */
	  tbp_pipe_debug("pipe Registering %s\n",read_buf);
	  tbpi_record_module_presence(read_buf,iFFL_DATA);
	  tbpi_register(FFL_DATA,TBP_TRANS_TYPE_HDL,read_buf);
	}

	/* get the path seperator */
	tbp_pipe_read(&path_seperator);
	tbpi_set_path_seperator(path_seperator);

	done = TRUE;
	break;
      case TBP_PIPE_ATTACH_TRANS:
	/* get the id of the transactor to be cleared */
	tbp_pipe_read(&trans_id);
	tbp_check_trans_id(trans_id);
	
	/* get the thread id  */
	tbp_pipe_read(&thread_id);
	
	/* get the length of the trans name */
	tbp_pipe_read(&read_size);
	
	/* now read the string */
	tbp_pipe_read(read_buf);
	
	/* clear out the trans struct */
	tbpi_clear_trans_struct(trans_id,iFFL_DATA);
	trans = &tc->trans[trans_id];
	
	tbp_pipe_debug("attaching trans %d (%s) to thread %d\n",trans_id,read_buf,thread_id);
	
	/* assign the name and set flags to indicate that it is attached */
	tbp_free(trans->reg_name);
	trans->reg_name = (char *) tbp_allocate_buffer(read_size,"trans attached name");
	strcpy(trans->reg_name,read_buf);
	trans->thread_id = thread_id;
	trans->attached = TRUE;
	done = FALSE;
	break;
      case TBP_PIPE_DETACH_TRANS:
	/* get the id of the transactor to be cleared */
	tbp_pipe_read(&trans_id);
	tbp_check_trans_id(trans_id);
	tbp_pipe_debug("detaching trans %d (%s)\n",trans_id,tc->trans[trans_id].full_name);
	tbpi_clear_trans_struct(trans_id,iFFL_DATA);
	done = FALSE;
	break; 
      case TBP_PIPE_TRANSPORT:
	/* get the trans id */
	tbp_pipe_read(&trans_id);
	
	/* get the operation struct */
	tbp_pipe_read(&operation);
	
	op = tbpi_trans_operation(trans_id); // pointer to real operation struct 

	/* copy the important elements of the trans struct from the buffer */
	op->opcode    = operation.opcode;
	op->address   = operation.address;
	op->size      = operation.size;
	op->status    = operation.status;
	op->interrupt = operation.interrupt;
	op->reset     = operation.reset;
	op->idle_cnt  = operation.idle_cnt;
	op->direction = operation.direction;
	op->width     = operation.width;
	op->starttime = operation.starttime;
	op->endtime   = operation.endtime;
	
	/* depending on the state of the operation - move the data buffers */
	if(operation.direction==TBP_OP_BIDIR || operation.direction==TBP_OP_WRITE) {
	  /* make sure there is enough room for the incoming data */
	  read_size = (((operation.size+7)/8) * 8) + 16; /* 64bit aligned buffer + tbp3x padding*/
	  tbpi_set_buffer_size(trans_id,read_size, iFFL_DATA);
	  
	  /* read the data/datax */
	  tbp_pipe_read(op->write_data);
	  tbp_pipe_read(op->write_datax);
	}
	done = FALSE;
	break;
      case TBP_PIPE_THREAD_WAKE:

	/* check license status (will only check once for many calls) */
	tbp_check_license();

	/* get the current sim-time */
	tbp_pipe_read(&current_sim_time);
	tbpi_set_current_hdl_time(current_sim_time);
	
	/* get the thread id */
	tbp_pipe_read(&thread_id);
	//fprintf(stderr,"wakeup: thread_id %d\n",thread_id);
	
	/* if this is an attached thread (i.e. not main!) - move op/data */
	if(thread_id > tbpi_main_thread_id()){
	  /* get the operation struct */
	  tbp_pipe_read(&operation);
	  
	  trans_id = tbp_get_thread(thread_id)->trans_id;
	  op = tbpi_trans_operation(trans_id); // pointer to the real opreation struct
	  
	  /* copy the important elements of the trans struct from the buffer */	
	  op->opcode    = operation.opcode;
	  op->address   = operation.address;
	  op->size      = operation.size;
	  op->status    = operation.status;
	  op->interrupt = operation.interrupt;
	  op->reset     = operation.reset;
	  op->idle_cnt  = operation.idle_cnt;
	  op->direction = operation.direction;
	  op->width     = operation.width;
	  op->starttime = operation.starttime;
	  op->endtime   = operation.endtime;
	  
	  /* depending on the state of the operation struct, get the read data buffers */
	  if(operation.direction==TBP_OP_BIDIR || operation.direction==TBP_OP_READ) {
	    /* make sure there is enough room for the incoming data */
	    read_size = (((operation.size+7)/8) * 8) + 16; /* 64bit aligned buffer + tbp3x padding*/
	    tbpi_set_buffer_size(trans_id,read_size, iFFL_DATA);
	    
	    /* read the data/datax */
	    tbp_pipe_read(op->read_data);
	    tbp_pipe_read(op->read_datax);
	  }
	  tbp_pipe_debug("waking up thread %d (attached to trans %d %s) at time %lld\n",
			 thread_id,trans_id,tbp_get_trans_name(trans_id),tbp_get_sim_time());
	}
	else {
	  tbp_pipe_debug("waking up thread %d at time %lld\n",
			 thread_id,tbp_get_sim_time());
	}

	/* now wakeup the requested thread */
	tbp_thread_activate(tbp_get_my_thread_id(),thread_id);
	//done = tbp_get_trans_count()!=0 ? FALSE : TRUE;
	done = TRUE;
	
	//if(!done)
	//  tbpi_pipe_send_cmd(FFL_DATA,TBP_PIPE_RUN_SIM);
	break;
      case TBP_PIPE_TRANS_WAKE:
	/* get the trans id */
	tbp_pipe_read(&trans_id);
	tbp_check_trans_id(trans_id);
	
	/* clear the sleep flag of this transactor */
	op = tbpi_trans_operation(trans_id); // pointer to the real opreation struct
	if(op->opcode == TBP_SLEEP) {
	  /* first remove the sleep opcode */
	  op->opcode = TBP_NOP;
	}
	
	done = FALSE;
	break;
      case TBP_PIPE_SET_THREAD_ID: /* used to update the pli-side trans table when a thread moves... */
	/* get the trans id */
	tbp_pipe_read(&trans_id);
	
	/* get the thread id */
	tbp_pipe_read(&thread_id);
	tbp_pipe_debug("updating trans id %d with new thread id %d\n",trans_id,thread_id);
	
	tbp_check_trans_id(trans_id);
	/* update this transctors thread id */
	tc->trans[trans_id].thread_id = thread_id;
	done = FALSE;
	break;
      case TBP_PIPE_SET_SIGNAL_CMD:
	/* get the acc error_enble flag */
	tbp_pipe_read(&acc_error_enable);
	tbpi_set_acc_error_enable(acc_error_enable);

	/* get the path length */
	tbp_pipe_read(&path_length);

	/* allocate space for the path */
	signal_path = (char *)tbp_allocate_buffer(sizeof(char)*(path_length+1),"PIPE_SET_SIGNAL");
	/* get the path itself */
	tbp_pipe_read(signal_path);

	/* get the value ptr size */
	tbp_pipe_read(&signal_ptr_size);
	
	/* allocate space for the value */
	tbp_free(g_pipe_signal_value);
	g_pipe_signal_value = (void *)tbp_allocate_buffer(sizeof(void)*signal_ptr_size,"PIPE_SET_SIGNAL");

	/* read the value */
	tbp_pipe_read(g_pipe_signal_value);

	/* get the type */
	tbp_pipe_read(&signal_type);
	
	/* get the force flag */
	tbp_pipe_read(&signal_force);

	/* call the set funtion */
	signal_rtn = tbpi_sim_set_value(signal_path,g_pipe_signal_value,signal_type,signal_force,NULL,iFFL_DATA);
	
	/* free the pointers */
	tbp_free(signal_path);
	tbp_free(g_pipe_signal_value);
	
	/* send the return value */
	tbpi_pipe_send_cmd(iFFL_DATA,TBP_PIPE_SET_SIGNAL_RTN,signal_rtn);

	done=FALSE;
	break;
      case TBP_PIPE_SET_SIGNAL_RTN:
	/* get the return ptr from the caller */
	va_start(ap, line);
	ret_ptr = (void *)va_arg(ap, int *);
	ret_code = (void *)va_arg(ap, int *);
	va_end(ap);
	
	/* get the return value */
	tbp_pipe_read(ret_ptr);
	done = TRUE;

	/* get the return error code */
	tbp_pipe_read(ret_code);
	break;
      case TBP_PIPE_GET_SIGNAL_CMD:
	/* get the acc error_enble flag */
	tbp_pipe_read(&acc_error_enable);
	tbpi_set_acc_error_enable(acc_error_enable);

	/* get the path length */
	tbp_pipe_read(&path_length);
	/* allocate space for the path */
	signal_path = (char *)tbp_allocate_buffer(sizeof(char)*(path_length+1),"PIPE_GET_SIGNAL");
	/* get the path itself */
	tbp_pipe_read(signal_path);
	
	/* get the type */
	tbp_pipe_read(&signal_type);
	
	/* call the get function */
	signal_value = tbp_get_value(signal_path,signal_type);
	
	/* free the path ptr */
	tbp_free(signal_path);
	
	if(signal_value==NULL) {
	  signal_ptr_size = 0;
	}
	else {
	  /* now we have to figure out how big the return value is... */
	  switch(signal_type)
	    {
	    case TBP_GET_SCL:
	    case TBP_GET_INT:
	      signal_ptr_size = sizeof(int);
	      break;
	    case TBP_GET_REAL: 
	      signal_ptr_size = sizeof(double);
	      break;
	    case TBP_GET_BIN:
	    case TBP_GET_OCT:
	    case TBP_GET_DEC:
	    case TBP_GET_HEX:
	      signal_ptr_size = strlen(signal_value)+1;
	      break;
	    default:
	    signal_ptr_size = 0;
	    tbp_error_internal("set_signal_value: invalid argument type (%d)\n",signal_type);
	    break;
	    }
	}
	
	/* send back the return value */
	tbpi_pipe_send_cmd(iFFL_DATA,TBP_PIPE_GET_SIGNAL_RTN,signal_ptr_size,signal_value);
	
	done = FALSE;
	break;
      case TBP_PIPE_GET_SIGNAL_RTN:
	/* get the size of the signal */
	tbp_pipe_read(&signal_ptr_size);
	
	/* get the users pointer */
	va_start(ap, line);
	signal_ptr = va_arg(ap, void **);
	ret_code = va_arg(ap, int *);
	va_end(ap);

	/* free the pointer from any previous activity */
	tbp_free(g_pipe_signal_value);
	
	/* check it for an 0 then get it */
	if(signal_ptr_size != 0){
	  /* allocate space for the return value */
	  g_pipe_signal_value = (void *) tbp_allocate_buffer(sizeof(void)*signal_ptr_size,"PIPE_GET_SIGNAL");
	  /* get the pointer */
	  
	  tbp_pipe_read(g_pipe_signal_value);
	  
	  /* assign it to the return value */
	  *signal_ptr = (void *)g_pipe_signal_value;
	}
	else {
	  *signal_ptr = NULL;
	}

	/* get the return code */
	tbp_pipe_read(ret_code);

	done = TRUE;
	break;
      case TBP_PIPE_PRINT:
	/* fetch the current sim-time */
	tbp_pipe_read(&current_sim_time);
	/* store it */
	tbpi_set_current_hdl_time(current_sim_time);

	/* check for pli print (includes hdl module/file/line) */
	tbp_pipe_read(&pli_print);

	/* get the level_index - error/warn/panic */
	tbp_pipe_read(&print_level_index);
	
	/* get the message length */
	tbp_pipe_read(&print_length);
	
	/* get the debug length */
	tbp_pipe_read(&print_debug_length);

	/* get the message string */
	if(print_length > 0){
	  tbp_free(print_buf);
	  print_buf = (char *) tbp_allocate_buffer(print_length,"TBP_PIPE_PRINT");
	  tbp_pipe_read(print_buf);

	  /* print it! */
	  if((print_level_index < 3) || (tc->pli_cside_screen_print==TRUE)) {
	    fprintf(tc->screen_output_stream,"%s",print_buf);
	    fflush(tc->screen_output_stream);
	  }
	  if(((print_level_index < 3) || (tc->pli_cside_log_print==TRUE)) && tc->c_logfile!=NULL) {
	    fprintf(tc->c_logfile,"%s",print_buf);
	    fflush(tc->c_logfile);
	  }
	  /* free the buffer */
	  tbp_free(print_buf);
	}

	/* print the debug string if there is one */
	if(print_debug_length > 0) {
	  tbp_free(print_buf);
	  print_buf = (char *) tbp_allocate_buffer(print_debug_length,"TBP_PIPE_PRINT");
	  tbp_pipe_read(print_buf);
	  
	  if(pli_print==TRUE){
	    /* print it! */
	    if((print_level_index < 3) || (tc->pli_cside_screen_print==TRUE)) {
	      fprintf(tc->screen_output_stream,"%s",print_buf);
	      fflush(tc->screen_output_stream);
	    }
	    if(((print_level_index < 3) || (tc->pli_cside_log_print==TRUE)) && tc->c_logfile!=NULL) {
	      fprintf(tc->c_logfile,"%s",print_buf);
	      fflush(tc->c_logfile);
	    }
	  }
	  else {
	    fprintf(tc->screen_output_stream,"***%s    From: %s In: %s Line: %d Time: %lld\n\n",level_string[print_level_index],iFFL_DATA,tbp_get_sim_time());
	  }
	  
	  /* free the buffer */
	  tbp_free(print_buf);
	}
	
	if(print_level_index==TBP_MSG_PANIC_LEVEL || print_level_index==TBP_MSG_ERROR_LEVEL) {
	  if(pli_print)
	    tbp_sim_error_inc();
	  else
	    tbp_c_error_inc();
	}
	else if(print_level_index==TBP_MSG_WARN_LEVEL) {
	  tbp_warn_inc();
	}
	
	/* call the user error function */
	if(print_level_index < 3)
	  tbp_user_error_function(print_level_index);

	/* handle the case where the user_error_function detached all the threads! */
	//done = tc->trans_attach_cnt !=0 ? FALSE : TRUE;

	/* send the print return */
	//tbpi_pipe_send_cmd(iFFL_DATA,TBP_PIPE_PRINT_RTN);
	done = TRUE;
	break;
      case TBP_PIPE_PRINT_RTN:
	/* get main timeout, it may have been altered during the print! */
	tbp_pipe_read(&main_timeout);
	tbpi_set_main_timeout(main_timeout);
	done = TRUE;
	break;
      case TBP_PIPE_MSG_CONFIG:
	/* get the following */
	//tbp_pipe_read(&tc->pli_screen_print);
	tbp_pipe_read(&tc->pli_log_print);
	tbp_pipe_read(&tc->pli_cside_screen_print);
	tbp_pipe_read(&tc->pli_cside_log_print);
	tbp_pipe_read(&tc->message_debug_print);
	tbp_pipe_read(&tc->flush_logfile);
	tbp_pipe_read(&tc->c_error_cnt);
	tbp_pipe_read(&tc->sim_error_cnt); 
	tbp_pipe_read(&tc->warn_cnt); 
	done = FALSE;
	break;
      case TBP_PIPE_SIM_ERROR:
	/* get the error type */
	tbp_pipe_read(&print_level_index);
	
	/* decode and increment as needed */
	if(print_level_index==TBP_MSG_ERROR_LEVEL)
	  tbp_sim_error_inc();
	else if(print_level_index==TBP_MSG_WARN_LEVEL)
	  tbp_warn_inc();
	done = FALSE;
	break;
      case TBP_PIPE_RUN_SIM:
	/* get the main timeout value */
	tbp_pipe_read(&main_timeout);
	tbpi_set_main_timeout(main_timeout);

	/* get the main_is_waiting flag */
	tbp_pipe_read(&main_is_waiting);
	
	/* set the main waiting flag */
	tbpi_set_main_is_waiting(main_is_waiting);
	//fprintf(stderr,"run_sim: main is waiting %d %d\n",main_is_waiting,main_timeout);
	
	/* get the ctrans count */
	tbp_pipe_read(&ctrans_cnt);
	tc->ctrans_cnt = ctrans_cnt;
	done = TRUE;
	break;
      case TBP_PIPE_RUN_CMODEL:
	/* execute any cmodels */
	tbp_cmodel_check_execute();
	done = TRUE;
	break;
      case TBP_PIPE_STOP_SIM:
	tbp_sim_stop();
	done = TRUE;
	break;
      case TBP_PIPE_SHUTDOWN:
	tbp_shutdown(0);
	done = TRUE;
	break;
      case TBP_PIPE_RETURN:
	/* get the return value */
	tbp_pipe_read(&cmd);
	return cmd;
	done = TRUE;
	break;
      default:
	done = TRUE;
	tbp_panic("unknown pipe command! (%d)\n",cmd);
      }
    
    sigprocmask(SIG_UNBLOCK, &blockInt, NULL);
    //sigrelse(SIGINT);
    tbp_pipe_debug("get_cmd (%d %s) finished\n",cmd,pipe_cmd_name[cmd]);
  }

  return cmd;
}

/* wakeup function to send a wakeup message to a transactor */
void tbp_pipe_wakeup(int trans_id)
{
  TBP_CONF_T *tc=tbp_get_conf();
  /* check to see if this is a cmodel transactor */
  if(tc->trans[trans_id].type!=TBP_TRANS_TYPE_CMODEL) {
    /* send the wakeup message */
    tbpi_pipe_send_cmd(FFL_DATA,TBP_PIPE_TRANS_WAKE,trans_id);
  }
}

/*******************************************************
 * some pipe utilities that are only used by the C-side
 *******************************************************/
#ifdef DUAL_CSIDE
/*
 * function to en/disable debug printing within this module
 */
void tbp_set_sim_debug_flag(int flag) { return; }

/* stub function - should never be called on the c-side */
char *tbp_get_current_module_name(char *name)
{
  tbp_panic("TBP Internal: This function and should never be called in this mode\n");
  return NULL;
}

/* */
void tbp_sim_finish(void)
{
  tbpi_pipe_send_cmd(FFL_DATA,TBP_PIPE_SHUTDOWN);
}

/* */
void tbp_sim_stop(void)
{
  tbpi_pipe_send_cmd(FFL_DATA, TBP_PIPE_STOP_SIM);
  tbpi_pipe_get_cmd(FFL_DATA);
}

/* function used to performance any per simulator configuration */
void tbp_simulator_specific_init() {
  /* "sleep" functionality dicates far fewer transactions/sec */
  tbp_set_license_interval(TBP_LICENSE_INTERVAL/100);
  tbpi_set_path_seperator('.');
  tbpi_set_transport_mode(TBP_TRANSPORT_MODE_DUAL);
}

BOOL tbp_sim_get_argv_key_present(char *key) {
  TBP_CONF_T *tc=tbp_get_conf();
  int i;
  
  /* walk through the array looking for a match */
  for(i=0;i<tc->sim_argc;i++){
    if(tc->sim_argv[i]!=NULL){	
      if((tc->sim_argv[i][0]=='-') || (tc->sim_argv[i][0]=='+')){
	if(strcmp((tc->sim_argv[i]+1),key)==0)
	  return TRUE;
      }
      else {
	if(strcmp(tc->sim_argv[i],key)==0)
	  return TRUE;
      }
    }
  }
 
  /* nothing found */
  return FALSE;
}

char *tbp_sim_get_argv_key(char *key) {
  TBP_CONF_T *tc=tbp_get_conf();
  int i;
  
  /* walk through the array looking for a match */
  for(i=0;i<tc->sim_argc-1;i++){
    if(tc->sim_argv[i]!=NULL && tc->sim_argv[i+1]!=NULL){
      if((tc->sim_argv[i][0]=='-') || (tc->sim_argv[i][0]=='+')){
	if(strcmp((tc->sim_argv[i]+1),key)==0)
	  return tc->sim_argv[i+1];
      }
      else {
	if(strcmp(tc->sim_argv[i],key)==0)
	  return tc->sim_argv[i+1];
      }
    }
  }
 
  /* nothing found */
  return NULL;
}

/* generic function called by utils during an async wakeup */
void tbp_store_sim_time(void)
{
  return;
}

/* 
 * set a simulation variable with a given argument - type is:
 * integer (int number)
 * binary  (char/string)
 * hex     (char/string)
 * octal   (char/string)
 * force is encoded: 
 *      0 - normal = accNoDelay
 *      1 - force  = accForceFlag
 *      2 - release = accReleaseFlag
 */
int tbpi_sim_set_value(char *path, void *value, int type, int force, char *scope, FFL_ARGS)
{
  int ret_val,ret_code;
  char *char_value = (char *)value;

  tbpi_pipe_send_cmd(iFFL_DATA,TBP_PIPE_SET_SIGNAL_CMD,path,value,type,force);
  tbpi_pipe_get_cmd(iFFL_DATA,&ret_val,&ret_code);
  
  if(tbpi_get_acc_error_enable()==TRUE){
    switch(ret_code)
      {
      case TBP_SETGET_NOERR: 
	break;
      case TBP_SETGET_NULL_PATH:
	tbp_error_internal("set_value: path is null\n");
	break;
      case TBP_SETGET_BAD_PATH: 
	tbp_error_internal("set_value: %s not found\n**** Check The following:\n"
			   "**** 1. signal must exist within the simulation\n"
			   "**** 2. simulator must be configured to allow write/force access (see simulator's 'acc' documentation)\n",
			   path);
	break;
      case TBP_SETGET_BAD_SIZE:
	tbp_error_internal("set_value: provided signal value (%s) size is incorrect\n"
			   "**** signal value size (%d) must match the size of the hdl signal\n",
			   char_value,strlen(char_value));  
	break;
      case TBP_SETGET_BAD_TYPE:
	tbp_error_internal("set_value: invalid argument type (%d)\n",type);
	break;
      case TBP_SETGET_BAD_LOGIC:
	tbp_error_internal("set_value: failed to modify signal (%s)\n**** Check The following:\n"
			   "**** 1. simulator must be configured to allow access to this signal (see simulator's 'acc' documentation)\n"
			   "**** 2. provided logic value must match the width of the value to be modified\n"
			   "**** 3. provided logic value must match the specific type (bin/oct/hex/int/real etc...)\n",
			   path); 
	break;
      case TBP_SETGET_BAD_SCOPE: 
	tbp_error_internal("set_value: scope is not yet supported with set_value\n");
	break;
      case TBP_SETGET_BAD_FORCE:
	tbp_error_internal("set_value: invalid force argument (%d)\n",force); 
	break;
      default:
	break;
      }
  }
  return ret_val;
}

void *tbpi_sim_get_value(char *path, int type, char *scope, FFL_ARGS)
{
  void *signal_rtn;
  int ret_code;

  tbpi_pipe_send_cmd(iFFL_DATA,TBP_PIPE_GET_SIGNAL_CMD,path,type);
  tbpi_pipe_get_cmd(iFFL_DATA,&signal_rtn,&ret_code);
  
  if(tbpi_get_acc_error_enable()==TRUE){
    switch(ret_code)
      {
      case TBP_SETGET_NOERR: 
	break;
      case TBP_SETGET_NULL_PATH:
	tbp_error_internal("get_value: path is null\n");
	break;
      case TBP_SETGET_BAD_PATH: 
	tbp_error_internal("get_value: %s not found\n**** Check The following:\n"
			   "**** 1. signal must exist within the simulation\n"
			   "**** 2. simulator must be configured to allow read access (see simulator's 'acc' documentation)\n",
			   path);
	break;
      case TBP_SETGET_BAD_SIZE:
	break;
      case TBP_SETGET_BAD_TYPE:
	tbp_error_internal("get_value: invalid argument type (%d)\n",type);
	break;
      case TBP_SETGET_BAD_LOGIC:
	tbp_error_internal("get_value: failed to retrieve signal (%s)\n**** Check The following:\n"
		       "**** 1. Simulator must be configured to allow access to this signal (see simulator's 'acc' documentation)\n"
		       "**** 2. Requested signal must be match request type (bin/oct/hex/int/real etc...)\n",
		       path);
	break;
      case TBP_SETGET_BAD_SCOPE: 
	tbp_error_internal("get_value: scope is not yet supported with get_value\n");
	break;
      case TBP_SETGET_BAD_FORCE:
	break;
      default:
	break;
      }
  }
  return signal_rtn;
}

/*
 * function to "reset" all registered transactors - "set/get" are called 2x
 * to account for simulator's support for leading "." or "/"
 */
void tbpi_sim_reset_transactors(FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();
  char *trans_name;
  int i;
  int acc_error_enable = tbpi_get_acc_error_enable();
  char *reset_value;

  if(tbp_get_no_hdl_flag()) return;

  if(!tbp_is_main()){
    tbp_error_internal("reset_transactors cannot be called outside of the main thread\n");
    return;
  }

  tbpi_set_acc_error_enable(FALSE);

  for(i=0;i<tc->trans_cnt;i++){
    if(tc->trans[i].type==TBP_TRANS_TYPE_HDL) {
      /* allocate space for the signal name */
      trans_name = (char *)tbp_allocate_buffer(strlen(tc->trans[i].full_name)+30,
					       "Transactor reset signal name");
      sprintf(trans_name,"%s%cTBP_reset_toggle",tc->trans[i].full_name,tbp_get_path_seperator());
      /* get the current state of the signal */
      reset_value = tbpi_sim_get_value(trans_name,TBP_GET_BIN,(char *)NULL,iFFL_DATA);
      if(reset_value!=NULL) {
	if(strcmp(reset_value,"0")==0)
	  tbpi_sim_set_value(trans_name,"1",TBP_SET_BIN,TBP_SET_NORMAL,(char *)NULL,iFFL_DATA);
	else
	  tbpi_sim_set_value(trans_name,"0",TBP_SET_BIN,TBP_SET_NORMAL,(char *)NULL,iFFL_DATA);
      }
      tbp_free(trans_name);
    }
  }

  tbpi_set_acc_error_enable(acc_error_enable);
  
  /* reliable cancellation of the tasks requires that we pass some time */
  tbpi_main_sleep(1,iFFL_DATA);
}

/*
 * dummy functions to facilitate C/pli code sharing 
 */
int tbp_sim_setget_return(void) { return 0; }
void tbp_sim_setget_flag(int flag) { return; }

#endif

/*********************************************************
 * some pipe utilities that are only used by the SIM-side
 *********************************************************/
#ifdef DUAL_SIMSIDE
void tbp_main(int argc, char *argv[])
{
  // panic
}
#endif
