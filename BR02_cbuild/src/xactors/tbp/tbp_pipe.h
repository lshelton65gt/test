//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_PIPE_H
#define TBP_PIPE_H

/* allow stand-alone compile for testing */
#ifdef TBP_PIPE_TEST
#define tbp_error(fmt, args...) printf("Error: "); printf(fmt, ##args); exit(-1)
#define tbp_info(fmt, args...)  printf(fmt, ##args) 
#define tbp_panic(fmt, args...) printf("Panic: "); printf(fmt, ##args); exit(-1)
#define tbp_allocate_buffer(size,errstr) calloc(1,size)
#define TRUE  0x1ul
#define FALSE 0x0ul
typedef int BOOL;
#else
#include "tbp_internal.h"
#endif

/* defines for initialization */
#define TBP_PIPE_MASTER_READY   0x11111111
#define TBP_PIPE_SLAVE_READY    0x22222222

/* pipe filename base string - uid/ident to be added */
#define TBP_TOSIM_PIPE_FILENAME_BASE "/var/tmp/tbp_a"
#define TBP_TOC_PIPE_FILENAME_BASE   "/var/tmp/tbp_b"

/* defines used to detemrine which side of the pipe we are on */
#define TBP_PIPE_MASTER 0
#define TBP_PIPE_SLAVE  1

/* number of sigint signals to allow during a single read before shutting down */
#define PIPE_SIGINT_THRESHOLD 2

#ifdef __cplusplus
extern "C" {
#endif

/* Pipe initialization function */
int tbp_pipe_init(int mode, char *tbp_ident_string);

/* base function used to write buffer (sends size prior to the provided buffer) */
int tbp_pipe_write(void *buf, int size);

/* base function to read (reads the size first followed by the buffer) */
int tbp_pipe_read(void *buf);

/* Function to be called outside of tbp_pipe to cleanup a given tbp_ident */
void tbp_pipe_clean(void);

/* function to forceably clean out files given an ident */
void tbp_pipe_delete_ident(char *tbp_ident_string);

/* function "flush" the pipe */
void tbp_pipe_flush(void);

/* function to return the state of pipe_conencted */
int tbp_pipe_connected(void);

#ifdef __cplusplus
}
#endif
#endif
