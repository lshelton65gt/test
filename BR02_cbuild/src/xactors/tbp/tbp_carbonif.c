//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005-2007 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
 *   Carbon C model C++ interface to the C tbp routines
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "carbon/carbon_shelltypes.h"
#include "carbon/carbon_capi.h"
#include "tbp_internal.h"
#include "tbp_carbonif.h"
//#include "tbp_malloc.h"

#define MAX_STRING_SIZE 50

static CarbonObjectID *mymodel;

void* cds_tbp_register_create (int numParams, CModelParam* cmodelParams, const char* inst) {
  int inst_length = strlen(inst)+1;
  char *myinst;
  (void)numParams; (void)cmodelParams;
  myinst = (char *)tbp_malloc(inst_length);
  memcpy(myinst, inst, inst_length);
  
  tbp_init_config(TBP_SIMSIDE);

  return myinst;
}

void  cds_tbp_register_misc   (void* hndl, CarbonCModelReason reason, void* cmodelData) {
    (void)hndl;

    if (reason == eCarbonCModelID)
      mymodel = (CarbonObjectID *)cmodelData;
}
void  cds_tbp_register_destroy(void* hndl) {tbp_free(hndl);}
void  cds_tbp_register_run    (void* hndl, 
			       UInt32* xid,  // Output, size = 1 word(s)
			       const UInt32* csgetsize, // Input, size = 1 word(s)
			       const UInt32* csdat // Input, size = 64 words(s) 
			       ) {
  TBP_CONF_T *tc=tbp_get_conf();
  char *path;
  UInt32   i;

  // Add the pointer of the Carbon Model to the string
  // so we can get a unique identifier for each instance
  // of the Carbon Model
  path = tbp_gen_unique_handle((void*)mymodel,(char *)hndl);
  
  // Register Transactor
  tbp_record_module_presence_carbon(path);
  tbp_cds_register(path, (UInt32 *)xid);
    
  //printf("Registered module %s  ID: %x Carbon Hndl %x\n", path, *xid, mymodel);
  
  // Set Default Data For CS Get Registers
  for(i = 0; i < *csgetsize; i++) {
    tc->trans[*xid].cs_get_data[i] = csdat[i];
  }
}


void* cds_tbp_transport_create (int numParams, CModelParam* cmodelParams, const char* inst) {
  (void)numParams; (void)cmodelParams; (void)inst;
  return NULL; }
void  cds_tbp_transport_misc   (void* hndl, CarbonCModelReason reason, void* cmodelData) {
  (void)hndl; (void)reason; (void)cmodelData;
}
void  cds_tbp_transport_destroy(void* hndl) {(void) hndl;}
void  cds_tbp_transport_run    (void* hndl,
			   const UInt32* xid,    // Input, size = 1 word(s)
			   const UInt32* opin,   // Input, size = 1 word(s)
			   const UInt32* adin,   // Input, size = 2 word(s)
			   const UInt32* szin,   // Input, size = 1 word(s)
			   const UInt32* stin,   // Input, size = 1 word(s)
			   const UInt32* intin,  // Input, size = 1 word(s)
			   const UInt32* rstin,  // Input, size = 1 word(s)
			   const UInt32* strtin, // Input, size = 2 word(s)
			   const UInt32* endin,  // Input, size = 2 word(s)
			   UInt32* opout,  // Output, size = 1 word(s)
			   UInt32* adout,  // Output, size = 2 word(s)
			   UInt32* szout,  // Output, size = 1 word(s)
			   UInt32* stout,  // Output, size = 1 word(s)
			   UInt32* intout, // Output, size = 1 word(s)
			   UInt32* rstout){// Output, size = 1 word(s)
	
       CarbonTime  simtime = carbonGetSimulationTime(mymodel);
       UInt64      addrOut = 0;

       // Convert 64bit values
       UInt64 addrIn = (((UInt64)(adin[1])) << 32) | adin[0];
       UInt64 start  = (((UInt64)(strtin[1])) << 32) | strtin[0];
       UInt64 end  = (((UInt64)(endin[1])) << 32) | endin[0];

       (void)hndl;
       tbpi_set_current_hdl_time((UInt64)simtime);
       
       //printf("in cds_tbp_transport_run: xid = %d, simtime = %lld\n", *xid, simtime);

       // printf("calling tbp_cds_transport - xid: %x = adout: %08x_%08x = opout: %x\n", 
       //       *xid, *(adout+1), *adout, *opout);
       
       tbp_cds_transport(  xid,      // Input, size = 1 word(s)
			   opin,     // Input, size = 1 word(s)
			   &addrIn,  // Input, size = 2 word(s)
			   szin,     // Input, size = 1 word(s)
			   stin,     // Input, size = 1 word(s)
			   intin,    // Input, size = 1 word(s)
			   rstin,    // Input, size = 1 word(s)
			   &start,   // Input, size = 2 word(s)
			   &end,     // Input, size = 2 word(s)
			   
			   opout,    // Output, size = 1 word(s)
			   &addrOut, // Output, size = 2 word(s)
			   szout,    // Output, size = 1 word(s)
			   stout,    // Output, size = 1 word(s)
			   intout,   // Output, size = 1 word(s)
			   rstout);  // Output, size = 1 word(s)
       
       // Convert 64bit values
       adout[0] = addrOut & 0xffffffff;
       adout[1] = (addrOut >> 32) & 0xffffffff;

	// printf("called  tbp_cds_transport - xid: %x = adout: %08x_%08x = opout: %x\n", 
	//	       *xid, *(adout+1), *adout, *opout);
}

void* cds_tbp_get_data64_create (int numParams, CModelParam* cmodelParams, const char* inst) {
  (void)numParams; (void)cmodelParams; (void)inst;
  return NULL; }
void  cds_tbp_get_data64_misc   (void* hndl, CarbonCModelReason reason, void* cmodelData) {
  (void)hndl; (void)reason; (void)cmodelData;
}
void  cds_tbp_get_data64_destroy(void* hndl) { (void)hndl; }
void  cds_tbp_get_data64_run    (void* hndl,
				 const   UInt32* xid, // Input, size = 1 word(s)
				 const   UInt32* idx, // Input, size = 1 word(s)
				 UInt32* xdat) {      // Output, size = 2 word(s)
	
  CarbonTime  simtime = carbonGetSimulationTime(mymodel);
  UInt64      dataOut = 0;
  (void)hndl;

  tbpi_set_current_hdl_time((UInt64)simtime);

  tbp_cds_get_data64(xid, idx, &dataOut);

  // Convert 64bit values
  xdat[0] = dataOut & 0xffffffff;
  xdat[1] = (dataOut >> 32) & 0xffffffff;

  // printf("called tbp_cds_get_data64 - %x  X: %03x  D: %08x_%08x\n", *xid, *idx, *(xdat+1),*xdat);
}


void* cds_tbp_get_data32_create (int numParams, CModelParam* cmodelParams, const char* inst) {
  (void)numParams; (void)cmodelParams; (void)inst;
  return NULL; }
void  cds_tbp_get_data32_misc   (void* hndl, CarbonCModelReason reason, void* cmodelData) {
  (void)hndl; (void)reason; (void)cmodelData;
}
void  cds_tbp_get_data32_destroy(void* hndl) { (void) hndl; }
void  cds_tbp_get_data32_run    (void* hndl,
			     const   UInt32* xid, // Input, size = 1 word(s)
			     const   UInt32* idx, // Input, size = 1 word(s)
			     UInt32* xdat) {      // Output, size = 2 word(s)
	
  CarbonTime  simtime = carbonGetSimulationTime(mymodel);
  (void)hndl;
  tbpi_set_current_hdl_time((UInt64)simtime);

  //	printf("calling tbp_cds_get_data - %x\n", *xid);
  tbp_cds_get_data32(xid, idx, xdat);
}

void* cds_tbp_get_cs_data_create (int numParams, CModelParam* cmodelParams, const char* inst) {
  (void)numParams; (void)cmodelParams; (void)inst;
  return NULL; }
void  cds_tbp_get_cs_data_misc   (void* hndl, CarbonCModelReason reason, void* cmodelData) {
  (void)hndl; (void)reason; (void)cmodelData;
}
void  cds_tbp_get_cs_data_destroy(void* hndl) { (void) hndl; }
void  cds_tbp_get_cs_data_run    (void* hndl,
				  const   UInt32* xid,  // Input, size = 1 word(s)
				  const   UInt32* size, // Input, size = 1 word(s)
				  UInt32* xdat) {       // Output, size = 32 word(s)
	
  TBP_CONF_T *tc=tbp_get_conf();
  UInt32 i;
  (void)hndl;
  
  for(i = 0; i < *size; i++) {
    xdat[i] = tc->trans[*xid].cs_get_data[i];
    //printf("Trans %d: GetData[%d] = %x\n", *xid, i<<2, tc->trans[*xid].cs_get_data[i]);
  }

  //printf("CS Get Data Trans: %d Data[0] = %d\n", *xid, xdat[0]);
}

void* cds_tbp_put_cs_data_create (int numParams, CModelParam* cmodelParams, const char* inst) {
  (void)numParams; (void)cmodelParams; (void)inst;
  return NULL; }
void  cds_tbp_put_cs_data_misc   (void* hndl, CarbonCModelReason reason, void* cmodelData) {
  (void)hndl; (void)reason; (void)cmodelData;
}
void  cds_tbp_put_cs_data_destroy(void* hndl) { (void) hndl; }
void  cds_tbp_put_cs_data_run    (void* hndl,
				  const   UInt32* xid,  // Input, size = 1 word(s)
				  const   UInt32* size, // Input, size = 1 word(s)
				  UInt32* xdat) {       // Input, size = 32 word(s)
	
  TBP_CONF_T *tc=tbp_get_conf();
  UInt32 i;
  (void)hndl;
  
  for(i = 0; i < *size; i++) {
    tc->trans[*xid].cs_put_data[i] = xdat[i];
  }
  
}


void* cds_tbp_put_data64_create (int numParams, CModelParam* cmodelParams, const char* inst) {
  (void) numParams; (void)cmodelParams; (void)inst;
  return NULL; }
void  cds_tbp_put_data64_misc   (void* hndl, CarbonCModelReason reason, void* cmodelData) {
  (void)hndl; (void)reason; (void)cmodelData;
}
void  cds_tbp_put_data64_destroy(void* hndl) { (void)hndl; }
void  cds_tbp_put_data64_run    (void* hndl,
			     const   UInt32* xid,     // Input, size = 1 word(s)
			     const   UInt32* idx,     // Input, size = 1 word(s)
			     const   UInt32* xdat) {  // Input, size = 2 word(s)
	
       CarbonTime  simtime = carbonGetSimulationTime(mymodel);

       // Convert 64bit value
       UInt64 data = (((UInt64)(xdat[1])) << 32) | xdat[0];

       (void)hndl;
       tbpi_set_current_hdl_time((UInt64)simtime);


       //printf("calling tbp_cds_put_data64 - %x  X: %03x  D: %016llx\n", *xid, *idx, data);
       tbp_cds_put_data64(xid,  idx, &data);
}


void* cds_tbp_put_data32_create (int numParams, CModelParam* cmodelParams, const char* inst) {
  (void)numParams; (void)cmodelParams; (void)inst;
  return NULL; }
void  cds_tbp_put_data32_misc   (void* hndl, CarbonCModelReason reason, void* cmodelData) {
  (void)hndl; (void)reason; (void)cmodelData;
}
void  cds_tbp_put_data32_destroy(void* hndl) { (void)hndl; }
void  cds_tbp_put_data32_run    (void* hndl,
			     const   UInt32* xid,     // Input, size = 1 word(s)
			     const   UInt32* idx,     // Input, size = 1 word(s)
			     const   UInt32* xdat) {  // Input, size = 1 word(s)
	
       CarbonTime  simtime = carbonGetSimulationTime(mymodel);
       (void) hndl;
       tbpi_set_current_hdl_time((UInt64)simtime);

  //	printf("calling tbp_cds_put_data - %x\n", *xid);
  tbp_cds_put_data32(xid, idx, xdat);
}



void* cds_tbp_error_create (int numParams, CModelParam* cmodelParams, const char* inst) {
  (void)numParams; (void)cmodelParams; (void)inst;
  return NULL; }
void  cds_tbp_error_misc   (void* hndl, CarbonCModelReason reason, void* cmodelData) {
  (void) hndl; (void) reason; (void)cmodelData;
 }
void  cds_tbp_error_destroy(void* hndl) {(void) hndl; }
void  cds_tbp_error_run    (void* hndl, const UInt32* argin_1 ) { // Input, size = 1 word(s)
  (void)hndl;
  tbp_cds_pli_error((char*)argin_1);
}
