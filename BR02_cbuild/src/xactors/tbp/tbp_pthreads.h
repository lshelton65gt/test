//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_PTHREADS_H
#define TBP_PTHREADS_H

#include <pthread.h>
#include "tbp_internal.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
  u_int32 id;             // index nto the array of threads
  pthread_t thread_id;    // id of this thread
  pthread_mutex_t *mutex; // the actual mutex for this thread
  pthread_cond_t *cond;   // the conditional which this thread uses to wait
  BOOL condp;             // global variable to prevent conditional race
  u_int32 trans_id;       // id of the transactor to which we are attached
  u_int32 type;           // main, user, cmodel?
  u_int32 status;         // RUN,KILL,DONE,INVALID
  void *wait_evt;        // event on which we are waiting, if any (-1) means no wait
  int wait_evt_state;     // this is the state which must be reached to end the wait
  int wait_evt_type;      // none, assertion, state
  u_int64 timeout;        // timeout for this event
  void (*func_ptr)();     // mapped function
  void (*int_func_ptr)(u_int32); // interrupt function
  void (*rst_func_ptr)(u_int32); // interrupt function
#ifdef TBP_64
  u_int64 *usr_ptr;
#else
  u_int32 *usr_ptr;
#endif
  u_int32 num_args;
} TBP_THREAD_T;

/* return our thread id */
pthread_t tbp_thread_self(void);

/* allocate/initialize cond and mutex */
void tbp_thread_allocate(u_int32 thread_index);

/* function used to copy the contents of a thread from one location to another */
void tbp_thread_copy(u_int32 dst_index, u_int32 src_index);

void tbp_thread_cleanup(u_int32 thread_index);
int tbp_thread_create(u_int32 thread_index);

/* function which removes a thread from the list of threads */
void tbp_thread_remove(u_int32 thread_cnt, u_int32 thread_index);

void tbp_thread_activate(u_int32 calling_thread, u_int32 target_thread);

void tbp_thread_lock(u_int32 thread_index);
void tbp_thread_unlock(u_int32 thread_index);
void tbp_thread_cond_signal(u_int32 thread_index);
void tbp_thread_cond_wait(u_int32 thread_index);

/* special wait used in thread relocation */
void tbp_thread_wakeup_main(u_int32 thread_index);

/* functions to destroy the threads */
void tbp_delete_thread(u_int32 thread_index);

#ifdef __cplusplus
}
#endif

#endif
