/***************************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

// This file holds the functions that handles the request response style transactions

#include "tbp.h"
#include "util/CarbonPlatform.h"
#include "util/c_memmanager.h"
#include "carbonXTransaction.h"

// #define MEM_DBG

static void print_buffer(UInt32 mSize, UInt32 mWidth, UInt8 * mData) __attribute__ ((unused));

void carbonXReqRespTransaction(const struct carbonXTransactionStruct *req, struct carbonXTransactionStruct *resp) {
  carbonXPutTransaction(req);
  carbonXGetTransaction(resp);
  
  // Copy Transaction ID From the Request Transaction
  resp->mTransId   = req->mTransId;
}

void carbonXGetTransaction(struct carbonXTransactionStruct *trans) {
  TBP_OP_T * tbp_trans = tbp_get_operation();

  trans->mTransId   = 0;
  trans->mOpcode    = tbp_trans->opcode;
  trans->mAddress   = tbp_trans->address;
  trans->mSize      = tbp_trans->size;
  trans->mStatus    = tbp_trans->status;
  trans->mInterrupt = tbp_trans->interrupt;
  trans->mWidth     = tbp_trans->width;
  trans->mStartTime = tbp_trans->starttime;
  trans->mEndTime   = tbp_trans->endtime;
  trans->mHasData   = (tbp_trans->direction == TBP_OP_BIDIR) || (tbp_trans->direction == TBP_OP_READ);

  // Copy Data Buffer 
  carbonXReallocateTransactionBuffer(trans);
  if(trans->mSize != 0 && trans->mData != NULL)
    memcpy(trans->mData, tbp_trans->read_data, carbonXCalculateActualTransactionSize(trans));
}

void carbonXPutTransaction(const struct carbonXTransactionStruct *trans) {
  TBP_OP_T * tbp_trans = tbp_get_operation();

  tbp_trans->opcode     = trans->mOpcode;
  tbp_trans->address    = trans->mAddress;
  tbp_trans->size       = trans->mSize;
  tbp_trans->status     = trans->mStatus;
  tbp_trans->width      = trans->mWidth;
  
  if(trans->mSize != 0 && trans->mData != NULL)
    memcpy(tbp_trans->write_data, trans->mData, carbonXCalculateActualTransactionSize(trans));
  
  // Perform Transaction
  carbonXSendOperation(tbp_trans);
}

void carbonXInitTransactionStruct(struct carbonXTransactionStruct *obj, UInt32 buf_size) {
  obj->mTransId    = 0;
  obj->mRepeatCnt  = 1;
  obj->mOpcode     = 0;
  obj->mAddress    = 0;
  obj->mSize       = 0;
  obj->mStatus     = 0;
  obj->mWidth      = 0;
  obj->mInterrupt  = 0;
  obj->mStartTime  = 0;
  obj->mEndTime    = 0;
  obj->mBufferSize = buf_size;
  obj->mHasData    = 0;
  if(buf_size != 0) {
    obj->mData = carbonmem_malloc(buf_size*sizeof(UInt8));
#ifdef MEM_DBG
    printf("Mem Debug: Trans ID: %08x, Allocated Pointer: %x\n", obj->mTransId, (unsigned int)obj->mData);
#endif
  }
  else {
    obj->mData = NULL;
  }
}

void carbonXDestroyTransactionStruct(struct carbonXTransactionStruct *obj) {
  obj->mTransId    = 0;
  obj->mOpcode     = 0;
  obj->mAddress    = 0;
  obj->mSize       = 0;
  obj->mStatus     = 0;
  obj->mWidth      = 0;
  obj->mInterrupt  = 0;
  obj->mStartTime  = 0;
  obj->mEndTime    = 0;
  obj->mBufferSize = 0;
  obj->mHasData    = 0;
#ifdef MEM_DBG
  printf("Mem Debug: Trans ID: %08x, Freeing Pointer: %x\n", obj->mTransId, (unsigned int)obj->mData);
#endif
  if(obj->mData != NULL) carbonmem_free(obj->mData);
  obj->mData       = NULL;
}

UInt32 carbonXCalculateActualTransactionSize(const struct carbonXTransactionStruct *trans) {    
  return tbp_calc_copy_size(trans->mSize, trans->mWidth);
}

void carbonXCopyTransactionStruct(struct carbonXTransactionStruct *dest, const struct carbonXTransactionStruct *src) {
  
  // Copy all Non Pointer Members
  dest->mTransId    = src->mTransId;
  dest->mRepeatCnt  = src->mRepeatCnt;
  dest->mOpcode     = src->mOpcode;
  dest->mAddress    = src->mAddress;
  dest->mSize       = src->mSize;
  dest->mStatus     = src->mStatus;
  dest->mWidth      = src->mWidth;
  dest->mInterrupt  = src->mInterrupt;
  dest->mHasData    = src->mHasData;
  dest->mStartTime  = src->mStartTime;
  dest->mEndTime    = src->mEndTime;
  // If Source Transaction has memory allocated, copy the contents
  if(src->mData != NULL && src->mSize != 0) {
    // Allocate Memory for Buffer
    carbonXReallocateTransactionBuffer(dest);

    // Copy Memory
    memcpy(dest->mData, src->mData, carbonXCalculateActualTransactionSize(dest));
  }
  
}

void carbonXReallocateTransactionBuffer(struct carbonXTransactionStruct *trans) {

  // We want to make sure we make room for the complete
  // word, even if the Size indicates a partial one.
  UInt32 allocSize = carbonXCalculateActualTransactionSize(trans);

  //printf("In carbonXReallocateTransactionBuffer: allocSize = %d bufsize = %d, mData = %x\n", allocSize, trans->mBufferSize, trans->mData);

  // Check if we need to allocate new/more memory
  if(allocSize > trans->mBufferSize) {
    if(trans->mData != NULL) {
#ifdef MEM_DBG
      printf("Mem Debug: Trans ID: %08x, Freeing Pointer: %x\n", trans->mTransId, (unsigned int)trans->mData);
#endif
      carbonmem_free(trans->mData);
      trans->mData = NULL;
      trans->mBufferSize = 0;
    }
  }
  
  // Allocate Memory if we need
  if(trans->mData == NULL) {
    trans->mData = carbonmem_malloc(allocSize*sizeof(UInt8));
#ifdef MEM_DBG
    printf("Mem Debug: Trans ID: %08x, Allocated Pointer: %x\n", trans->mTransId, (unsigned int)trans->mData);
#endif
    trans->mBufferSize = allocSize;
  }
}

void carbonXSetTransaction(struct carbonXTransactionStruct *trans,
			   UInt32 trans_id,
			   UInt32 opcode, UInt64 address, const void * data, 
			   UInt32 size,   UInt32 status,  UInt32       width) {

  trans->mTransId   = trans_id;
  trans->mRepeatCnt = 1;
  trans->mOpcode    = opcode;
  trans->mAddress   = address;
  trans->mStatus    = status;
  trans->mSize      = size;
  trans->mWidth     = width;
  trans->mInterrupt = 0; 
  trans->mHasData   = 0;
  trans->mStartTime = 0;
  trans->mEndTime   = 0;

  // Allocated memory for the Data buffer
  carbonXReallocateTransactionBuffer(trans);
  
  if(trans->mData && data) { // Only Copy if both buffers are valid
    memcpy(trans->mData, data, carbonXCalculateActualTransactionSize(trans));
    trans->mHasData = 1;
  }
}

static void print_buffer(UInt32 mSize, UInt32 mWidth, UInt8 * mData)
{

  UInt32 i, j;

  for(i = 0; i < (mSize + ((mWidth&0xf)-1))/(mWidth&0xf); i++) {
    printf("Word[0x%04x] = ",i); 
    for(j = 0; j < (mWidth&0xf); j++)
      printf("%x ", mData[i*(mWidth&0xf)+j]);
    printf("\n");
  }
}
