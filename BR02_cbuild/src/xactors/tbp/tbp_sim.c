//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "tbp_internal.h"

// FIXME - now that width is back can the "check" operation be a little more
// FIXME - helpful regarding which word and byte?

/* check return status */
void tbpi_check_transaction(u_int32 direction, u_int32 smask, FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf(); 
  TBP_OP_T *operation = &tc->trans[tbp_get_my_trans_id()].operation;
  u_int8  *read_datax;
  u_int32 i;
 
  /* is this a read transaction? then check the read data for X/Z */
  if(direction==TBP_OP_READ || direction==TBP_OP_BIDIR) {
    read_datax= (u_int8 *) operation->read_datax;
    for(i=0;i<operation->size;i++){
      if(read_datax[i] != 0)
	tbp_error_internal("X/Z detected in byte %d during a read operation\n",i);
    }
  }
  
  if(tbp_get_status_error_enable()){
    /* now check the status field */
    if((operation->status & smask) != 0)
      tbp_error_internal("Non-Zero status (%d) returned\n",operation->status);
  }
}
  
/* basic write functions (recasting of write data is to preserve little endian byte order of tbp 3x)*/
void tbpi_sim_write8(u_int64 addr, u_int8 data, FFL_ARGS)
{
  u_int32 status=0, size=1, write_data = (u_int32)data;
  u_int32 opcode = TBP_WRITE;

  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,&write_data,NULL,&size,&status,TBP_OP_WRITE,TBP_WIDTH_32,iFFL_DATA);
  tbpi_check_transaction(TBP_OP_WRITE,0,iFFL_DATA);
}

void tbpi_sim_write16(u_int64 addr, u_int16 data, FFL_ARGS)
{
  u_int32 status=0, size=2, write_data = (u_int32)data;
  u_int32 opcode = TBP_WRITE;
    
  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,&write_data,NULL,&size,&status,TBP_OP_WRITE,TBP_WIDTH_32,iFFL_DATA);
}

void tbpi_sim_write32(u_int64 addr, u_int32 data, FFL_ARGS)
{
  u_int32 status=0, size=4, write_data = (u_int32)data;
  u_int32 opcode = TBP_WRITE;

  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,&write_data,NULL,&size,&status,TBP_OP_WRITE,TBP_WIDTH_32,iFFL_DATA);
  tbpi_check_transaction(TBP_OP_WRITE,0,iFFL_DATA);
}

void tbpi_sim_write64(u_int64 addr, u_int64 data, FFL_ARGS)
{
  u_int32 status=0, size=8;
  u_int32 opcode = TBP_WRITE;
    
  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,&data,NULL,&size,&status,TBP_OP_WRITE,TBP_WIDTH_64P,iFFL_DATA);
  tbpi_check_transaction(TBP_OP_WRITE,0,iFFL_DATA);
}

/* basic read functions */
u_int8 tbpi_sim_read8(u_int64 addr, FFL_ARGS)
{
  u_int32 read_data=0;
  u_int32 status=0, size=1;
  u_int32 opcode = TBP_READ;

  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,&read_data,NULL,&size,&status,TBP_OP_READ,TBP_WIDTH_32,iFFL_DATA);
  tbpi_check_transaction(TBP_OP_READ,0,iFFL_DATA);
  return (u_int8) read_data;
}

u_int16 tbpi_sim_read16(u_int64 addr, FFL_ARGS)
{
  u_int32 read_data=0;
  u_int32 status=0, size=2;
  u_int32 opcode = TBP_READ;

  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,&read_data,NULL,&size,&status,TBP_OP_READ,TBP_WIDTH_32,iFFL_DATA);
  tbpi_check_transaction(TBP_OP_READ,0,iFFL_DATA);
  return (u_int16) read_data;
}

u_int32 tbpi_sim_read32(u_int64 addr, FFL_ARGS)
{
  u_int32 read_data=0;
  u_int32 status=0, size=4;
  u_int32 opcode = TBP_READ;

  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,&read_data,NULL,&size,&status,TBP_OP_READ,TBP_WIDTH_32,iFFL_DATA);
  tbpi_check_transaction(TBP_OP_READ,0,iFFL_DATA);
  return read_data;
}

u_int64 tbpi_sim_read64(u_int64 addr, FFL_ARGS)
{
  u_int64 read_data=0;
  u_int32 status=0, size=8;
  u_int32 opcode = TBP_READ;

  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,&read_data,NULL,&size,&status,TBP_OP_READ,TBP_WIDTH_64P,iFFL_DATA);
  tbpi_check_transaction(TBP_OP_READ,0,iFFL_DATA);
  return read_data;
}

/* burst write */
u_int32 tbpi_sim_write(u_int64 addr, void *data, u_int32 size, u_int32 width, FFL_ARGS)
{
  u_int32 status=0;
  u_int32 write_size = size;
  u_int32 opcode = TBP_WRITE;
 
  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,data,NULL,&write_size,&status,TBP_OP_WRITE,width,iFFL_DATA);
  tbpi_check_transaction(TBP_OP_WRITE,0,iFFL_DATA);
  return write_size;
}

/* burst read */
u_int32 tbpi_sim_read(u_int64 addr, void *data, u_int32 size, u_int32 width, FFL_ARGS)
{
  u_int32 status=0;
  u_int32 read_size = size;
  u_int32 opcode = TBP_READ;

  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,data,NULL,&read_size,&status,TBP_OP_READ,width,iFFL_DATA);
  tbpi_check_transaction(TBP_OP_READ,0,iFFL_DATA);
  return read_size;
}

/* burst write zx */
u_int32 tbpi_sim_write_zx(u_int64 addr, void *data, void *datax, u_int32 *size, u_int32 width, FFL_ARGS)
{
  u_int32 status=0;
  u_int32 opcode = TBP_WRITE;
 
  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,data,datax,size,&status,TBP_OP_WRITE,width,iFFL_DATA);
  tbpi_check_transaction(TBP_OP_WRITE,0,iFFL_DATA); /* use write to avoid checking X/Z */
  return *size;
}

/* burst read zx */
u_int32 tbpi_sim_read_zx(u_int64 addr, void *data, void *datax, u_int32 *size, u_int32 width, FFL_ARGS)
{
  u_int32 status=0;
  u_int32 opcode = TBP_READ;

  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,data,datax,size,&status,TBP_OP_READ,width,iFFL_DATA);
  tbpi_check_transaction(TBP_OP_WRITE,0,iFFL_DATA); /* use write to avoid checking X/Z */
  return *size;
}

/* configuration space write */
void tbpi_sim_cs_write(u_int64 addr, u_int64 data, FFL_ARGS)
{
  u_int32 status=0, size=8;
  u_int32 opcode = TBP_CS_WRITE;

  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,&data,NULL,&size,&status,TBP_OP_WRITE,TBP_WIDTH_64P,iFFL_DATA);
  tbpi_check_transaction(TBP_OP_WRITE,0,iFFL_DATA);
}

/* configuration space read */
u_int64 tbpi_sim_cs_read(u_int64 addr, FFL_ARGS)
{
  u_int64 read_data;
  u_int32 status=0, size=8;
  u_int32 opcode = TBP_CS_READ;
    
  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,&read_data,NULL,&size,&status,TBP_OP_READ,TBP_WIDTH_64P,iFFL_DATA);
  tbpi_check_transaction(TBP_OP_READ,0,iFFL_DATA);
  return read_data;
}

/* user defined operation */
void tbpi_sim_userop(u_int32 opcode, u_int64 addr, u_int64 *data, u_int64 *datax, 
		     u_int32 *size, u_int32 *status, FFL_ARGS)
{
  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,data,datax,size,status,TBP_OP_BIDIR,TBP_WIDTH_64P,iFFL_DATA);
  tbpi_check_transaction(TBP_OP_BIDIR,0,iFFL_DATA);
}

/* function to return the status of the previous operation */
u_int32 tbp_sim_status(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  
  return tc->trans[tbp_get_my_trans_id()].operation.status;
}

/* function to idle N cycles */
void tbpi_sim_idle(u_int32 idle_cnt, FFL_ARGS)
{
  u_int32 size=0,status=0;
  u_int32 opcode = TBP_IDLE;
  u_int64 address = ((u_int64) idle_cnt) & 0x00000000FFFFFFFFLL;
  
  if(tbp_is_user())
    tbpi_transaction(tbp_get_my_trans_id(),&opcode,&address,NULL,NULL,&size,&status,TBP_OP_NODATA,TBP_WIDTH_64P,iFFL_DATA);
  else 
    tbpi_main_sleep(address,iFFL_DATA);
}

void tbpi_sim_reset(FFL_ARGS)
{
  u_int32 size=0,status=0;
  u_int32 opcode = TBP_RESET;
  u_int64 address = 0;
  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&address,NULL,NULL,&size,&status,TBP_OP_NODATA,TBP_WIDTH_64P,iFFL_DATA);
}

/* function to put a transactor to sleep */
void tbpi_sim_sleep(u_int64 timeout, FFL_ARGS)
{
  u_int32 size=0,status=0;
  u_int32 opcode = TBP_SLEEP;
  u_int64 address = timeout;

  if(tbp_is_user())
    tbpi_transaction(tbp_get_my_trans_id(),&opcode,&address,NULL,NULL,&size,&status,TBP_OP_NODATA,TBP_WIDTH_64P,iFFL_DATA);
  else 
    tbpi_main_sleep(address,iFFL_DATA);
}
  
/* function used to assign an interrupt handler to a given thread id */
void tbpi_sim_set_interrupt_handler(u_int32 id, void (*int_func)(u_int32), FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();

  if(tc->trans[id].attached!=TRUE){
    tbp_error_internal("Failed to assign interrupt handler to transactor %d (%s)\n"
		       "*** Transactor is not attached\n");
  }
  else {
    tbp_info("thread %d setting an interrupt handler for trans %d\n",tbp_get_my_thread_id(),id);
    tbpi_thread_set_interrupt_handler(tc->trans[id].thread_id,int_func,iFFL_DATA);
  }
}

/* function used to assign an reset handler to a given thread id */
void tbpi_sim_set_reset_handler(u_int32 id, void (*rst_func)(u_int32), FFL_ARGS)
{
  (void) func; (void) line; (void) file;
  tbp_get_thread(id)->rst_func_ptr = rst_func;
}
