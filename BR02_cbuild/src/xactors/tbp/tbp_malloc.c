//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "tbp_internal.h"
// #include "c_memmanager.h"

#include "carbon/c_memmanager.h"
#include "util/c_util.h"

#define TBP_MALLOC_LOG "tbp_malloc.log"

extern int errno;

static FILE *g_tbp_malloc_log=NULL;
static int g_tbp_malloc_debug=FALSE;

/* functions to set/get the tbp_malloc_debug flag */
void tbp_set_malloc_debug(int debug) { g_tbp_malloc_debug = debug; }
int tbp_get_malloc_debug(void) { return g_tbp_malloc_debug; }

/* function to open the malloc log if it is not already open */
static void tbp_malloc_open_log(FFL_ARGS)
{
  char errBuf[1024];

  if(g_tbp_malloc_log==NULL){
    if(NULL==(g_tbp_malloc_log=fopen(TBP_MALLOC_LOG,"w+"))){
      tbp_panic_internal("tbp_malloc: Failed to open the logfile (%s): %s\n",
			 TBP_MALLOC_LOG,carbon_lasterror(errBuf, sizeof(errBuf)));
    }
  }
}
  
void *tbpi_malloc(int size, FFL_ARGS)
{
  void *mptr;
  if(NULL==(mptr=carbonmem_malloc(size))){
    tbp_panic_internal("tbp_malloc: malloc of %d bytes failed (file: %s func: %s line: %d)\n",
	      size,iFFL_DATA);
    return NULL;
  }
  else {
    if(g_tbp_malloc_debug==TRUE) {
      tbp_malloc_open_log(iFFL_DATA);
      fprintf(g_tbp_malloc_log,"tbp_malloc -  malloc: %8d bytes at %p (file: %s func: %s line: %d)\n",
	      size,mptr,iFFL_DATA);
    } 
    return mptr;
  }
}

void *tbpi_calloc(int num, int size, FFL_ARGS)
{
  void *mptr;
  if(NULL==(mptr=carbonmem_malloc(num * size))){
    tbp_panic_internal("tbp_malloc: malloc (doing calloc) of %d - %d byte elements failed (file: %s func: %s line: %d)\n",
		       num, size,iFFL_DATA);
    return NULL;
  }
  else {
    memset(mptr, 0, num * size);
    if(g_tbp_malloc_debug==TRUE) {
      tbp_malloc_open_log(iFFL_DATA);
      fprintf(g_tbp_malloc_log,"tbp_malloc -  calloc: %8d bytes at %p (%d element(s) x %d byte(s)) (file: %s func: %s line: %d)\n",
	      num*size,mptr,num,size,iFFL_DATA);
    }
    return mptr;
  }
}

void *tbpi_realloc(void *ptr, int orig_size, int new_size, FFL_ARGS)
{
  void *orig_ptr;
  
  orig_ptr = ptr;
  // for cleanlyness - calloc the first request...
  if(ptr==NULL) {
    ptr=carbonmem_malloc(new_size);
    memset(ptr, 0, new_size);
  }
  else {
    ptr=carbonmem_malloc(new_size);
    if (new_size >= orig_size) 
      memcpy(ptr, orig_ptr, orig_size);
    else
      memcpy(ptr, orig_ptr, new_size);
  }
  carbonmem_free(orig_ptr);

  if(ptr==NULL){
    tbp_panic_internal("tbp_malloc: realloc of %d bytes failed (file: %s func: %s line: %d)\n",
	      new_size,file,iFFL_DATA);
    return NULL;
  }    
  else {
    if(g_tbp_malloc_debug==TRUE) {
      tbp_malloc_open_log(iFFL_DATA);
      if(orig_ptr!=NULL) {
	fprintf(g_tbp_malloc_log,"tbp_malloc -    free:                at %p (realloc file: %s func: %s line: %d)\n",
		orig_ptr,iFFL_DATA);
      }
      fprintf(g_tbp_malloc_log,"tbp_malloc - realloc: %8d bytes at %p (file: %s func: %s line: %d)\n",
	      new_size,ptr,iFFL_DATA);
    }
    return ptr;
  }
}


void tbpi_free(void **ptr, FFL_ARGS)
{
  if(g_tbp_malloc_debug==TRUE)
    tbp_malloc_open_log(iFFL_DATA);
  
  if(ptr!=NULL){
    if(g_tbp_malloc_debug==TRUE) {
      fprintf(g_tbp_malloc_log,"tbp_malloc -    free:                at %p (file: %s func: %s line: %d)\n",
	      *ptr,iFFL_DATA);
    }
    /* Note, it is perfectly safe to call free() on a NULL pointer.
       In that case, free() performs no operation.  See `man 3
       free'. */
    carbonmem_free(*ptr);
    *ptr = NULL;
  }
}
