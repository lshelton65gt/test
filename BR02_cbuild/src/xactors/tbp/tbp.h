//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_H
#define TBP_H
#define TBP_USR_H // If tbp.h is loaded carbonX.h should not be loaded

/* for public consumption - no patch level information is provided */
#ifndef TBP_MAJOR
#define TBP_MAJOR 4
#endif
#ifndef TBP_MINOR
#define TBP_MINOR 1
#endif

#include "util/CarbonPlatform.h"

#include <stdio.h>
#include <stdarg.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
/* includes the user is used to getting from tbp */
#include <sys/stat.h>
#include <sys/types.h>

typedef unsigned char u_int8;
typedef unsigned short u_int16;
typedef unsigned int u_int32;
typedef char s_int8;
typedef short s_int16;
typedef int s_int32;

#if pfLP64
typedef unsigned long u_int64;
typedef long s_int64;
#else
typedef unsigned long long u_int64;
typedef long long s_int64;
#endif

#define TRUE 1
#define FALSE 0
typedef int BOOL;
typedef u_int64 simt_t;

typedef void (*CarbonXAttachFuncT)();

#if pfSPARCWORKS
#  define __PRETTY_FUNCTION__ __func__
#  define __FUNCTION__ __func__
#else
# if pfWINDOWS
#    define __PRETTY_FUNCTION__ "<unknown>" 
# else
#  ifndef __GNUC__   /* gnu-compiler-specific macros */
#    define __PRETTY_FUNCTION__ "<unknown>" 
#    define __FUNCTION__        "<unknown>" 
#   endif
# endif
#endif

#include "tbp_message.h"
#include "tbp_events.h"
#include "tbp_utils.h"
#include "tbp_threads.h"
#include "tbp_cpf.h"
#include "tbp_sim.h"
#include "tbp_cmodel.h"
#include "tbp_config.h"
#include "tbp_legacy_utils.h"
#include "tbp_malloc.h"

/* these defines intercept functions and redirect them as needed */
#define main tbp_main
#define exit(_code_) tbpi_exit(_code_,FFL_DATA)

#endif
