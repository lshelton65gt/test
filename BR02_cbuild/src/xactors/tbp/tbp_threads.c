//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "tbp_internal.h"
#ifdef DUAL_PROC
#include "tbp_pipe_utils.h"
#endif

#include "util/CarbonPlatform.h"

// FIXME - review shutdown paths - redunncy exists between pli.c and threads.c
// FIXME - do we want to be more precise about event deletion vs thread deletion?
// FIXME - right now, it is all or nothing

/* flag to limit message thrashing in non-debug mode */
static int g_tbp_thread_debug = FALSE;
//#define tbp_thread_debug if(g_tbp_thread_debug) tbp_printf
#define tbp_thread_debug if(g_tbp_thread_debug) printf

/*
 * thread pool
 */
static TBP_THREAD_T *g_thread=NULL;
static u_int32 g_thread_cnt=0;

/*
 * function to en/disable debug printing within this module
 */
void tbp_set_thread_debug_flag(int flag) { g_tbp_thread_debug = flag; }

/*
 * function to return the current thread count
 */
u_int32 tbp_get_thread_count(void)
{
  return g_thread_cnt;
}

/*
 * function to decriment the thread count 
 */
void tbp_dec_thread_count(void)
{
  g_thread_cnt -= g_thread_cnt > 0 ? 1 : 0;
}

/*
 * function to intialize the thread module
 */
void tbp_thread_module_init(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  g_thread_cnt = 0;
  tc->trans_attach_cnt = 0; 
  tc->current_thread = 0;
  tc->stack_size = TBP_THREAD_DEFAULT_STACK_SIZE;
}

/* 
 * return the id of the calling thread 
 */
u_int32 tbp_get_my_thread_id(void)
{
  return tbp_get_thread_id(NULL);
}

/*
 * function to set the interrupt handler for a given thread 
 */
void tbpi_thread_set_interrupt_handler(u_int32 thread_id,  void (*int_func)(u_int32), FFL_ARGS)
{
  (void) func; (void) line; (void) file;
  g_thread[thread_id].int_func_ptr = int_func;
}

/*
 * return a pointer to the thread corresponding to the given index
 */
TBP_THREAD_T *tbp_get_thread(u_int32 thread_id)
{
  if(g_thread==NULL)
    return NULL;
  else
    return &g_thread[thread_id];
}

/* initialize a given thread */
int tbp_thread_init(void)
{
  TBP_THREAD_T *thread;

  /* allocate space for the new thread */
  if(NULL==(g_thread=(TBP_THREAD_T *)tbp_realloc(g_thread,sizeof(TBP_THREAD_T)*(tbp_get_thread_count()), sizeof(TBP_THREAD_T)*(tbp_get_thread_count()+1)))){
    tbp_panic("tbp_thread_init: Could not allocate space for a new thread\n");
  }

  /* assign local pointer for subsequent access */
  thread = &g_thread[tbp_get_thread_count()];

  /* allocate resources as required for threading */
  tbp_thread_allocate(tbp_get_thread_count());

  thread->type = TBP_THREAD_TYPE_SIM;
  thread->status = TBP_THREAD_STATUS_RUN;
  thread->wait_evt = NULL;
  thread->wait_evt_state = 0;
  thread->wait_evt_type = TBP_EVT_WAIT_NONE;
  thread->timeout = 0LL;
  thread->func_ptr = NULL;
  thread->int_func_ptr = NULL;
  thread->rst_func_ptr = NULL;
  thread->usr_ptr = NULL;
  thread->trans_id = -1; 
  thread->id = tbp_get_thread_count();

  /* this default is intended for the sim-thread - will be overwritten by others */
  //if(tbp_get_thread_count() == 0)
  //  thread->thread_id = tbp_thread_self();
 
  g_thread_cnt +=1;
  
  return (tbp_get_thread_count() - 1);
}

/* function to clear out the thread struct as it destroyed */
void tbp_thread_struct_cleanup(u_int32 thread_index)
{
  TBP_THREAD_T *thread = &g_thread[thread_index];
  
  /* free the user args */
  tbp_free(thread->usr_ptr);

  /* clear out the data buffers */
  if(thread->type==TBP_THREAD_TYPE_USER) {
    tbp_dec_attach_cnt(thread->trans_id);

    tbp_thread_debug("clearing out data buffers for trans %d\n", thread->trans_id); 
  
    /* release the transactor to allow for re-attachment */
    tbpi_clear_trans_struct(thread->trans_id, FFL_DATA);
  }

  /* report "done" status */
  thread->status = TBP_THREAD_STATUS_DONE;

  tbp_thread_debug("thread %d: exiting...\n",thread_index);
}

/*
 * function which removes a thread from the list of threads
 * this requires that we collapse the thread list
 * we can assume that any allocated resources associated with 
 * this thread have already been destroyed
 */
void tbp_thread_remove(u_int32 thread_cnt, u_int32 thread_index)
{
  TBP_CONF_T *tc=tbp_get_conf();
  char *status_string[] = {"INVALID","RUN","WAIT","DONE","KILL"};
  int i;
  
  tbp_thread_debug("Removing thread[%d] status = %s\n",thread_index,status_string[g_thread[thread_index].status]);

  /* loop through the list of available threads and remove this one */
  for(i=thread_index;i< (int)thread_cnt-1;i++){
    /* move this thread down by one - must map to threading type */
    tbp_thread_copy(i,i+1);

    /* 
     * if there is a transactor assocated with this thread - then update
     * it to match the movement above - make sure it is a valid thread first
     */
    if(g_thread[i].status != TBP_THREAD_STATUS_INVALID){
      /* if this is a cmodel thread, then modify the ctrans_id of this transactor */
      if(g_thread[i].type == TBP_THREAD_TYPE_CMODEL) {
	tc->trans[g_thread[i].trans_id].ctrans_id = i;
      }
      else if(g_thread[i].type == TBP_THREAD_TYPE_USER){
	tc->trans[g_thread[i].trans_id].thread_id = i;
#ifdef DUAL_PROC
	/* update the trans struct on the pli side of the pipe */
	if(tc->trans[g_thread[i].trans_id].type!=TBP_TRANS_TYPE_CMODEL)
	  tbpi_pipe_send_cmd(FFL_DATA,TBP_PIPE_SET_THREAD_ID,g_thread[i].trans_id,i);
#endif
      }
    }
  }

  /* invalidate the final entry */
  g_thread[thread_cnt-1].id = -1;
  g_thread[thread_cnt-1].status = TBP_THREAD_TYPE_INVALID;
  g_thread[thread_cnt-1].status = TBP_THREAD_STATUS_INVALID;
}

/* common thread wrapper for q/pthreads */
void *tbp_thread_wrapper(void *thread_index)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread;
  u_int32 index,parent_thread;
  u_int32 *index_ptr;

  index_ptr = (u_int32 *) thread_index;
  index = *index_ptr;

  /* assign a local pointer for subsequent access */
  thread = &g_thread[index];

#if pfLP64
  tbp_thread_debug("thread %d: is alive with thread_id = 0x%lx\n",index, (u_int64)thread->thread_id);
#else
  tbp_thread_debug("thread %d: is alive with thread_id = 0x%x\n",index, (u_int32)thread->thread_id);
#endif
  /* choose which thread to re-activate based on our thread_type */
  parent_thread = g_thread[index].type==TBP_THREAD_TYPE_MAIN ? tc->sim_thread_id : tc->main_thread_id;

  /* signal back to the parent thread */
  tbp_thread_debug("thread %d signaling parent thread %d\n",index,parent_thread);
  tbp_thread_activate(index,parent_thread);
  
  /* reassign the pointer any time we switch contexts */
  thread = &g_thread[tbp_get_my_thread_id()];

  tbp_thread_debug("thread %d: awake and running mapped function\n",index);  
  switch(thread->num_args)
    {
    case 0: (thread->func_ptr)(); break;  
    case 1: (thread->func_ptr)(thread->usr_ptr[0]); break;  
    case 2: (thread->func_ptr)(thread->usr_ptr[0],thread->usr_ptr[1]); break;  
    case 3: (thread->func_ptr)(thread->usr_ptr[0],thread->usr_ptr[1],thread->usr_ptr[2]); break;  
    case 4: (thread->func_ptr)(thread->usr_ptr[0],thread->usr_ptr[1],thread->usr_ptr[2],thread->usr_ptr[3]); break; 
    case 5: (thread->func_ptr)(thread->usr_ptr[0],thread->usr_ptr[1],thread->usr_ptr[2],thread->usr_ptr[3],thread->usr_ptr[4]); break;  
    case 6: (thread->func_ptr)(thread->usr_ptr[0],thread->usr_ptr[1],thread->usr_ptr[2],thread->usr_ptr[3],thread->usr_ptr[4],thread->usr_ptr[5]); break;  
    case 7: (thread->func_ptr)(thread->usr_ptr[0],thread->usr_ptr[1],thread->usr_ptr[2],thread->usr_ptr[3],thread->usr_ptr[4],thread->usr_ptr[5],thread->usr_ptr[6]); break;  
    case 8: (thread->func_ptr)(thread->usr_ptr[0],thread->usr_ptr[1],thread->usr_ptr[2],thread->usr_ptr[3],thread->usr_ptr[4],thread->usr_ptr[5],thread->usr_ptr[6],thread->usr_ptr[7]); break;  
    default: (thread->func_ptr)();
    }
  
  /* reassign the pointer */
  thread = &g_thread[tbp_get_my_thread_id()];

  tbp_thread_debug("thread %d: function has returned - cleaning up the thread\n",thread->id);

  /* once main has shutdown - we need to exit */
  if(tbp_is_main()) tbp_shutdown(0);

  tbp_thread_cleanup(thread->id);

  return NULL;
}

void tbp_check_thread_exit(u_int32 thread_index)
{
  if(g_thread[thread_index].status==TBP_THREAD_STATUS_KILL)
    tbp_thread_cleanup(thread_index);
}

/*
 * this function will be called prior to thread activation
 * if there is an interrupt pending and we are not already inside
 * the interrupt handler, then we vector off into the interrupt
 * handler function mapped by the user and it retains control
 * until it returns - when it returns, control continues within
 * the thread which was interrupted with the interrupted operation
 * state restored
 */
void tbp_check_interrupt(u_int32 thread_index)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread = &g_thread[thread_index];
  TBP_TRANS_T *trans = &tc->trans[thread->trans_id];
  TBP_OP_T *operation = &tc->trans[thread->trans_id].operation;
  TBP_OP_T saved_operation,int_done;
  
  // clear struct
  memset(&int_done, 0, sizeof(TBP_OP_T));

  if(tc->interrupt_enable==FALSE || trans->interrupt_enable==FALSE){
    return; // interrupts disabled in this thread or globally
  }
  else {
    if((operation->interrupt!=0) && (trans->int_state==TBP_INT_NONE)) {
      tbp_thread_debug("interrupt detected - calling service routine\n");
      /* now call the interrupt handler function  if one exists */   
      if(thread->int_func_ptr!=NULL) {

	/* save off a copy of the original operation */
	memcpy(&saved_operation,operation,sizeof(TBP_OP_T));
	
	/* push the data buffers aside as well */
	operation->write_data = NULL;
	operation->read_data = NULL;
	operation->write_datax = NULL;
	operation->read_datax = NULL;
	operation->buffer_size = 0;

	/* reset the default buffers.. */
	tbpi_set_buffer_size(thread->trans_id, tc->default_buffer_size,FFL_DATA);
	
	/* 
	 * set the state of this operation to reflect the fact that we are 
	 * processing an interrupt
	 */
	trans->int_state = TBP_INT_PROC;
	
	/* call the user provided function */
	(thread->int_func_ptr)(operation->interrupt);
	tbp_thread_debug("interrupt handler has returned\n");
	
	/* send an int_done opcode to signal our completion */
	int_done.opcode = TBP_INT_DONE;
	int_done.size = 0;
	int_done.direction = TBP_OP_NODATA;
	int_done.idle_cnt = 0;
	int_done.address = 0;
	int_done.interrupt = 0;
	int_done.reset = 0;
	tbpi_send_operation(thread->trans_id,&int_done,FFL_DATA);

	/* restore the original operation */
	if(operation->buffer_size!=0){
	  tbp_free(operation->read_data);
	  tbp_free(operation->write_data);
	  tbp_free(operation->read_datax);
	  tbp_free(operation->write_datax);
	  operation->buffer_size = 0;
	}
	memcpy(operation,&saved_operation,sizeof(TBP_OP_T));
      }

      /* clear the interrupt status */
      operation->interrupt = 0;
      trans->int_state = TBP_INT_NONE;
    } 
  }
}

/*
 * function similar to interrupt check, but for resets 
 */
void tbp_check_reset(u_int32 thread_index)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread = &g_thread[thread_index];
  TBP_TRANS_T *trans = &tc->trans[thread->trans_id];
  TBP_OP_T *operation = &tc->trans[thread->trans_id].operation;
  TBP_OP_T saved_operation;

  if((operation->reset!=0) && (trans->rst_state==TBP_RST_NONE)) {
    tbp_thread_debug("reset detected - calling service routine\n");
      
    /* save off a copy of the original operation */
    memcpy(&saved_operation,operation,sizeof(TBP_OP_T));
    
    /* 
     * set the state of this operation to reflect the fact that we are 
     * processing an interrupt
     */
    trans->rst_state = TBP_RST_PROC;
    
    /* now call the interrupt handler function  if one exists */   
    if(thread->rst_func_ptr!=NULL) {
      (thread->rst_func_ptr)(operation->reset);
      tbp_thread_debug("reset handler has returned\n");
    }
    
    /* restore the original operation */
    memcpy(operation,&saved_operation,sizeof(TBP_OP_T));
    
      /* clear the interrupt status */
    operation->reset = 0;
    trans->rst_state = TBP_RST_NONE;
  } 
}

/*
 * function to set the "kill" state of a thread
 */
void tbpi_thread_kill(u_int32 thread_index,FFL_ARGS)
{
  (void) func; (void) line; (void) file;
  g_thread[thread_index].status=TBP_THREAD_STATUS_KILL;
}

/* 
 * this function deletes all of the "user" threads - NOT main
 */
void tbpi_delete_user_threads(FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();
  u_int32 i,thread_cnt,thread_index=tc->main_thread_id+1;

  if(tbp_is_user()){
    tbp_error_internal("delete_user_threads cannot be called outside of the main thread\n");
    return;
  }

  /* are there any user threads left? */
  if(tbp_get_thread_count() - tc->ctrans_cnt <= 2)
    return;

  tbp_thread_debug("\nThere are %d total threads alive\n\n",tbp_get_thread_count());
  
  if(g_tbp_thread_debug) tbp_print_thread_status();

  thread_cnt = tbp_get_thread_count();
  /* walk through the thread table issuing the kill command */
  for(i=tc->main_thread_id+1;i<thread_cnt;i++){
    if(g_thread[i].type==TBP_THREAD_TYPE_USER && 
       g_thread[i].status==TBP_THREAD_STATUS_RUN){
 
      g_thread[i].status = TBP_THREAD_STATUS_KILL;

      /* clear out the transactor struct */
      tbpi_clear_trans_struct(g_thread[i].trans_id, iFFL_DATA);
    }
  }

  /* now walk through them and let them die */
  for(i=tc->main_thread_id+1;i<thread_cnt;i++){
    if(g_thread[i].type==TBP_THREAD_TYPE_USER && 
       g_thread[i].status==TBP_THREAD_STATUS_KILL){
  
      tbp_thread_debug("killing thread %d\n",i);
      /* now wake them up to allow them to die */ 
      tbp_thread_activate(tc->main_thread_id,i);
    }
  }

  /* now walk through and remove the dead thread */
  tbp_thread_debug("thread_cnt %d\n",thread_cnt);
  
  if(g_tbp_thread_debug) tbp_print_thread_status();

  while(thread_index < thread_cnt){
    if(g_thread[thread_index].type==TBP_THREAD_TYPE_USER && 
       g_thread[thread_index].status==TBP_THREAD_STATUS_DONE) {
  
      tbp_thread_free(thread_index);
      tbp_thread_remove(thread_cnt, thread_index);
      thread_cnt -= 1; 
      /* reduce the thread count */
      g_thread_cnt -= 1;
    }
    else
      thread_index += 1;
  }

  /* wakeup cmodel threads and inform them of thier new id due to movement above */
  tbpi_cmodel_thread_relocate(iFFL_DATA);

  /* delete all events - we are removing threads which the event table is tracking */
  tbpi_evt_delete_all(iFFL_DATA);
      
  /* this should have beein done one-by-one but force here to be safe */
  tc->trans_attach_cnt = 0;
  tc->ctrans_attach_cnt = 0;
}


static struct {
  char  file[256];
  char  function[256];
  int   line;
}  gPathInfo;


void setTbpPathInfo(char* _file, char* _func, int _line)
{
  strcpy(gPathInfo.file, _file);
  strcpy(gPathInfo.function, _func);
  gPathInfo.line     = _line;
}


/* user space function used to attach a c thread to a transactor */
int tbpi_va_list_path_attach(FFL_ARGS, const char *path, void (*func_ptr)(), u_int32 num_args, va_list ap)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread;
  TBP_TRANS_T *trans;
  TBP_OP_T *operation;
  u_int32 index;
  int trans_id;
  u_int32 i;
  
  /* if we are attaching threads, then reset the test_complete flag */
  tc->test_complete_flag = FALSE;

  // tbp_printf("tbp_path_attach: %s\n", path);

#ifdef TBP_CDS
  /* Under carbon we need to do a default init */
   tbp_init(TBP_SIMSIDE);
  /* And we record this module's presence */
  //  tbpi_record_module_presence(path, iFFL_DATA);
#endif
  /* Note that under carbon, this check will only fail if the user tries 
   *  to attach the same module more than once
   */
  /* check to see if the path we have been given is sufficiently unique and exists */
  if((trans_id = tbpi_check_path_name(path, iFFL_DATA))<0){
    tbp_error_internal("thread %d: path (%s) is not valid\n",tbp_get_my_thread_id(),path);
  } else {
    
    /* Now check if this transactor id has already been attached */
    trans = &tc->trans[trans_id];
    /* thread_id gets initialized to 0, so if it's not 0, this transactor is already attached. */
    if(trans->thread_id) {
      tbp_panic_internal("trans %s is already attached to thread %d\n", trans->full_name, trans->thread_id);
    } else {
      /* increment the attachment count */
      tbp_inc_attach_cnt(trans_id);
      
      /* initialize this new thread */
      index = tbp_thread_init();
      
      /* assign local pointers for subsequent access */
      thread = &g_thread[index];
      trans = &tc->trans[trans_id];
      operation = &tc->trans[trans_id].operation;
      
      /* fill in some of the more important variables */
      thread->trans_id = (u_int32) trans_id;
      
      trans->thread_id = index;
      trans->attached = TRUE;
      
      /* make sure the buffers are at least "default_buffer_size" */
      if(tc->default_buffer_size != 0)
	tbpi_set_buffer_size(trans_id,tc->default_buffer_size,iFFL_DATA);
      
      tbp_thread_debug("thread %d: attaching trans %d to thread %d\n",
		       tbp_get_my_thread_id(),trans_id,trans->thread_id);
      
      if(NULL==(trans->reg_name = (char *)tbp_malloc(sizeof(char)*(strlen(path)+1))))
	tbp_panic_internal("thread %d: Failed to allocate space for registered path name for thread %d (Path: %s)\n",
			   tbp_get_my_thread_id(),index, path);
      else 
	strcpy(trans->reg_name,path);
      
      thread->type = TBP_THREAD_TYPE_USER;
      thread->func_ptr = func_ptr;
      thread->num_args = num_args;
      if(num_args != 0){
#if pfLP64
	thread->usr_ptr = (u_int64 *)tbp_malloc(sizeof(u_int64)*num_args);
#else
	thread->usr_ptr = (u_int32 *)tbp_malloc(sizeof(u_int32)*num_args);
#endif
	if(NULL==thread->usr_ptr) {
	  tbp_panic_internal("thread %d: Failed to allocate space for thread user arguments for thread %d (Path: %s)\n",
			     tbp_get_my_thread_id(),index, path);
	}
	
      }
      
      for (i = 0; i < num_args; i++) {
#if pfLP64
	thread->usr_ptr[i] = va_arg(ap,u_int64);
#else
	thread->usr_ptr[i] = va_arg(ap,u_int32);
#endif
      }
      va_end(ap);
      
      /* create the new thread and wait for it to return */
      tbp_thread_create(trans->thread_id);
      
#ifdef DUAL_PROC
      /* if we are in dual process mode - send notice to the sim-side */
      if(tbpi_get_transport_mode()==TBP_TRANSPORT_MODE_DUAL){
	if(trans->type==TBP_TRANS_TYPE_HDL) { /* this does not need to be done for cmodels */
	  tbpi_pipe_send_cmd(iFFL_DATA,TBP_PIPE_ATTACH_TRANS,trans_id,trans->thread_id,path);
	}
      }
#endif
    }
  }
  
  return 0;
}


int tbpi_path_attach(FFL_ARGS, const char *path, void (*func_ptr)(), u_int32 num_args, ...)
{
  va_list _list;
  va_start(_list, num_args);
  return tbpi_va_list_path_attach(file, 
				 func, 
				 line, 
				 path, 
				 func_ptr, 
				 num_args,
				 _list);
}

int tbpi_new_path_attach(const char *path, void (*func_ptr)(), u_int32 num_args, ...)
{
  va_list _list;
  va_start(_list, num_args);
  return tbpi_va_list_path_attach(gPathInfo.file, 
				 gPathInfo.function, 
				 gPathInfo.line, 
				 path, 
				 func_ptr, 
				 num_args,
				 _list);
}


/*
 * functgion to return a pointer to the current thread
 */
TBP_THREAD_T *tbp_get_current_thread()
{
   return &g_thread[tbp_get_my_thread_id()];
}

/*
 * function used to set the global (current_thread) and also to
 * allow cmodel thread relocation
 */
void tbp_set_current_thread(u_int32 *thread_index)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread;
  u_int32 new_thread = 0;
  int thread_cnt = tbp_get_thread_count();
  int i;
  
  /* if we are relocating cmodel threads - then *thread_index is not accurate */
  if(tc->cmodel_thread_relocate){
    /* walk through the list of threas looking for this index */
    for(i=0;i<thread_cnt;i++){
      thread = tbp_get_thread(i);
      if(thread->id == *thread_index){
	/* reassign the thread_index */
	*thread_index = i;
	/* assign the global */
	new_thread = i;
      }
    }
  }
  else {
    /* during normal operation - we trust this index */
    new_thread = *thread_index;
  }

  tc->current_thread = new_thread;
  tbp_thread_debug("set_current_thread (old %d new %d)\n",*thread_index,new_thread);
}

/*
 * function to conditional complete a thread relocation sequence
 */
void tbp_check_thread_relocate(u_int32 thread_index)
{
  TBP_CONF_T *tc=tbp_get_conf();
 
  /* if we are doing a relocaton - then wakeup main to compelte the process */
  if(tc->cmodel_thread_relocate && thread_index > tc->main_thread_id) {
    tbp_thread_debug("check_thread_relocate (thread %d)\n",thread_index);
    tbp_thread_wakeup_main(thread_index);
  }
}

void tbp_print_thread_status(void)
{
  TBP_THREAD_T *thread;
  char *status_string[] = {"INVALID","RUN","WAIT","DONE","KILL"};
  char *type_string[] = {"INVALID","SIM","MAIN","USER","CMODEL","OTHER"};
  int thread_cnt = tbp_get_thread_count();
  int i;
  
  tbp_printf("\nPrinting Thread Status:\n");
  for(i=0;i<thread_cnt;i++){
    thread = tbp_get_thread(i);
    tbp_printf("thread[%d]: id %d type %s status %s\n",
	       i,
	       thread->id,type_string[thread->type],
	       status_string[thread->status]);
  }
  tbp_printf("\n");
}
