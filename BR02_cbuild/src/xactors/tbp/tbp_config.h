//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_CONFIG_H
#define TBP_CONFIG_H

#include "tbp_utils.h"
#include "tbp_events.h"

#ifdef __cplusplus
extern "C" {
#endif

/* function to set the ident string - first !=NULL ident cannot be replaced... */
char *tbpi_set_ident_string(FFL_ARGS,char *tbp_ident_string);
#define tbp_set_ident_string(_ident_) tbpi_set_ident_string(FFL_DATA,_ident_)

/* function get the current ident string */
char *tbp_get_ident_string(void);

/* parse the comment line arguments looking for configuration */
void tbp_parse_args(int caller);

/* function to set the global interrupt enable/disable flag*/
void tbp_set_interrupt_enable(BOOL enable);

/*
 * functions to set the interrupt enable flag on a per thread basis
 * called from within a thread
 */
void tbpi_set_my_trans_interrupt_enable(BOOL enable, FFL_ARGS);
#define tbp_set_my_trans_interrupt_enable(_enable_) \
        tbpi_set_my_trans_interrupt_enable(_enable_,FFL_DATA)

#define tbp_enable_interrupts() tbpi_set_my_trans_interrupt_enable(TRUE,FFL_DATA)
#define tbp_disable_interrupts() tbpi_set_my_trans_interrupt_enable(FALSE,FFL_DATA)

/* set/get the status error_enable flag */
void tbp_set_status_error_enable(BOOL enable);
BOOL tbp_get_status_error_enable(void);

void tbp_set_c_debug(BOOL debug);

/* dynamically determine endian configuration */
BOOL tbp_is_big_endian(void);

/* function to setup a default buffer for each transactor - default is 64K */
void tbpi_set_default_buffer_size(u_int32 size,FFL_ARGS);
#define tbp_set_default_buffer_size(_size_) tbpi_set_default_buffer_size(_size_,FFL_DATA)

/* initialize the global configuration structure */
void tbp_init_config(int caller);

/* function to retrieve the path seperator */
char tbp_get_path_seperator(void);

/* function to set the override exit flag */
void tbp_set_override_exit(BOOL enable);

/* functions to enable each module's debugging output */
void tbp_set_util_debug_flag(int);
void tbp_set_sim_debug_flag(int);
void tbp_set_pipe_debug_flag(int);
void tbp_set_thread_debug_flag(int);
void tbp_set_config_debug_flag(int);
void tbp_set_legacy_debug_flag(int);
void tbp_set_evt_debug_flag(int);

#ifdef __cplusplus
}
#endif
#endif
