//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "tbp_queue.h"

/* queue manager... */
static TBP_QM_T *g_tbp_qm=NULL;

/* build a hash table to find the queues by name */
void tbp_queue_build_hash_table(void)
{
  if(g_tbp_qm->hash==NULL)
    g_tbp_qm->hash = tbp_hash_create();
}

/* build the queue manager which will manage multiple queues */
void tbp_queue_build_qm(void)
{
  if(g_tbp_qm==NULL) {
    if(NULL==(g_tbp_qm = (TBP_QM_T *)tbp_calloc(1,sizeof(TBP_QM_T))))
      tbp_panic("Failed to allocate space required to build the queue manager\n");
  }
  tbp_queue_build_hash_table();
}
  
/* return a pointer to the queue */
void *tbpi_queue_ptr(char *name, FFL_ARGS) {
  (void) func; (void) line; (void) file;
  return name ? (void *)tbp_hash(g_tbp_qm->hash,name) : NULL;
}

/* return the queue's (associated with "name") depth */
int tbpi_queue_depth(char *name, FFL_ARGS) {
  return tbpi_queue_depth_direct((TBP_QUEUE_T *)tbpi_queue_ptr(name,iFFL_DATA),iFFL_DATA);
}

/* return the given queue's depth */
int tbpi_queue_depth_direct(TBP_QUEUE_T *queue, FFL_ARGS) {
  (void) func; (void) line; (void) file;
  return queue ? queue->depth : 0;
}

/* create a new queue */
TBP_QUEUE_T *tbpi_queue_create(char *name,FFL_ARGS)
{
  TBP_QUEUE_T *new_queue,*old_queue;
  
  /* make sure the queue manager and queue table exist */
  tbp_queue_build_qm();

  // make sure this is not a duplicate queue...
  if(name) {
    old_queue = (TBP_QUEUE_T *)tbp_hash(g_tbp_qm->hash,name);
    // prevent creation of duplicate queues
    if(old_queue) {
      tbp_error_internal("Duplicate queue creation request for %s (queue already exists)\n",name);
      return old_queue;
    }

    // make room for this queue in the queue manager
    if(NULL==(new_queue=(TBP_QUEUE_T *)tbp_calloc(1,sizeof(TBP_QUEUE_T))))
      tbp_panic_internal("Failed to allocate space required to create queue %s\n",name);
    
    if(NULL==(new_queue->name=(char *)tbp_calloc(strlen(name)+1,sizeof(char))))
      tbp_panic_internal("Failed to allocate space required to create queue %s\n",name);
    
    // store the name
    strcpy(new_queue->name,name);
    
    // assign the previous pointer
    new_queue->prev = g_tbp_qm->tail;
    
    // add this entry to the table
    if(g_tbp_qm->tail) { // not the first entry
      g_tbp_qm->tail->nxt = new_queue;
      g_tbp_qm->tail = new_queue;
    }
    else { // the first entry
      g_tbp_qm->queue = g_tbp_qm->tail  = new_queue;
    }
    
    // initialize
    new_queue->depth = 0;
    new_queue->head = NULL;
    new_queue->tail = NULL;
    
    tbp_hash_add(g_tbp_qm->hash,name,(void *)new_queue);
    
    return new_queue;
  }
  else {
    return NULL;
  }
}

/* destroy a given queue */
void tbpi_queue_destroy(char *name, FFL_ARGS)
{
  (void) func; (void) line; (void) file;
  TBP_QE_T *entry;
  TBP_QUEUE_T *prev,*nxt;
  TBP_QUEUE_T *queue = (TBP_QUEUE_T *)tbp_hash(g_tbp_qm->hash,name);
  
  if(queue) {
    // free the linked list for this queue
    entry = queue->head;
    while(entry) {
      // move the pointer
      queue->head = (TBP_QE_T *)entry->nxt;
      // free the current entry
      tbp_free(entry);
      // reassign the ptr
      entry = queue->head;
    }

    // free the queue name
    if(queue->name)
      tbp_free(queue->name);

    /* remove our entry from the linked list (collapse) */
    prev = (TBP_QUEUE_T *)queue->prev;
    if(prev)
      prev->nxt = queue->nxt;
    else // we must have been the head
      g_tbp_qm->queue = queue->nxt;
    
    nxt = (TBP_QUEUE_T *)queue->nxt;
    if(nxt)
      nxt->prev = prev;
    else // we must have been the tail...
      g_tbp_qm->tail = queue->prev;

    /* free this queue */
    tbp_free(queue);
  }
}

/* destroy all managed queues */
void tbpi_queue_destroy_all(FFL_ARGS)
{
  TBP_QUEUE_T *queue,*nxt;

  if(g_tbp_qm) {
    queue = g_tbp_qm->queue;
    while(queue) {
      nxt = queue->nxt;
      tbpi_queue_destroy(queue->name,iFFL_DATA);
      queue = nxt;
    }
    
    if(g_tbp_qm) {
      tbp_hash_destroy(g_tbp_qm->hash);
      g_tbp_qm->hash = NULL;
      
      tbp_free(g_tbp_qm);
      g_tbp_qm = NULL;
    }
  }
}

    
/* add an antry into a queue */
void tbpi_queue_push(char *name, void *data, FFL_ARGS)
{ 
  TBP_QUEUE_T *queue;
  /* if this queue does not exist - create it... */
  if(NULL==(queue = (TBP_QUEUE_T *)tbpi_queue_ptr(name,iFFL_DATA)))
    queue = tbpi_queue_create(name,iFFL_DATA);
  else
    tbpi_queue_push_direct(queue,data,iFFL_DATA);
}

/* remove the oldest entry from the queue */
void *tbpi_queue_pop(char *name, FFL_ARGS)
{
  TBP_QUEUE_T *queue = (TBP_QUEUE_T *)tbpi_queue_ptr(name,iFFL_DATA);
  if(queue==NULL) {
    tbp_error_internal("Could not find requested queue - %s\n",name);
    return NULL;
  }
  else 
    return tbpi_queue_pop_direct(queue,iFFL_DATA);
}

/* get the next entry ona queue without popping it... */
void *tbpi_queue_view(char *name, FFL_ARGS)
{
  TBP_QUEUE_T *queue = (TBP_QUEUE_T *)tbpi_queue_ptr(name,iFFL_DATA);
  if(queue==NULL) {
    tbp_error_internal("Could not find requested queue - %s\n",name);
    return NULL;
  }
  else 
    return tbpi_queue_view_direct(queue,iFFL_DATA);
}


/* add an antry into a queue */
void tbpi_queue_push_direct(TBP_QUEUE_T *queue, void *data, FFL_ARGS)
{
  TBP_QE_T *entry;
  
  if(!queue) {
    tbp_error_internal("Invalid (NULL) queue specified\n");
  }
  else {
    // allocate space for this new item (additional time for "calloc" is lost in the noise)
    if(NULL==(entry=(TBP_QE_T *)tbp_calloc(1,sizeof(TBP_QE_T))))
      tbp_panic_internal("Failed to allocate space required to add to queue\n");
    else {
      // assign the user provided pointer
      entry->ptr = data;
      // add this element to linked list
      if(queue->head==NULL) { // first item in the list
	queue->head = entry;
	queue->tail = entry;
      }
      else {
	queue->tail->nxt = entry; // move the current tail's next ptr
	queue->tail = entry;      // add our new entry as the tail (move tail forward)
      }
      // increment the depth
      queue->depth += 1;
    }
  }
}

/* remove the oldest entry from the queue */
void *tbpi_queue_pop_direct(TBP_QUEUE_T *queue, FFL_ARGS)
{
  void *entry;
  void *cur_ptr=0;

  if(!queue) {
    tbp_error_internal("Invalid (NULL) queue specified\n");
  }
  else {
    // is this a value queue that has something to pop?
    if(!queue->head) {
      cur_ptr = NULL;
      queue->depth = 0;
    }
    else {
      // set the "current" pointer to reflec this data
      cur_ptr = queue->head->ptr;
      // move the linked list forward by one
      entry = queue->head ; // save for free
      queue->head = (TBP_QE_T *)queue->head->nxt; // step forward
      // free the prior entry
      tbp_free(entry);
      queue->depth -= 1;
    }
  }
  // return the current "poped" data
  return cur_ptr;
}

/* view the next pointer without popping it... */
void *tbpi_queue_view_direct(TBP_QUEUE_T *queue, FFL_ARGS)
{
  void *cur_ptr = NULL;

  if(!queue) {
    tbp_error_internal("Invalid (NULL) queue specified\n");
  }
  else {
    // is this a value queue that has something to pop?
    if(!queue->head) {
      cur_ptr = NULL;
      queue->depth = 0;
    }
    else {
      // set the "current" pointer to reflec this data
      cur_ptr = queue->head->ptr;
    }
  }
  // return the current "poped" data
  return cur_ptr;
}

/* does the given queue exist? */
BOOL tbp_queue_exists(char *name)
{
  if(tbpi_queue_ptr(name, FFL_DATA))
    return TRUE;
  else
    return FALSE;
}
