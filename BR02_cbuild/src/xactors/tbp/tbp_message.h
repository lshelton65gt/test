//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_MESSAGE_H
#define TBP_MESSAGE_H

#include "tbp_internal.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {KB=0, MB=1, GB=2} file_size_unit;

#define TBP_MSG_PRINTF_GRP "tbp_msg_printf_dbg"
#define TBP_MSG_INFO_GRP   "tbp_msg_info_dbg"
#define TBP_MSG_DEBUG_GRP  "tbp_msg_debug_dbg"
#define TBP_MSG_WARN_GRP   "tbp_msg_warn_dbg"
#define TBP_MSG_ERROR_GRP  "tbp_msg_error_dbg"
#define TBP_MSG_PANIC_GRP  "tbp_msg_panic_dbg"

#define TBP_MSG_PLI_PRINTF "tbp_msg_pli_printf_dbg"
#define TBP_MSG_PLI_INFO   "tbp_msg_pli_info_dbg"
#define TBP_MSG_PLI_DEBUG  "tbp_msg_pli_debug_dbg"
#define TBP_MSG_PLI_WARN   "tbp_msg_pli_warn_dbg"
#define TBP_MSG_PLI_ERROR  "tbp_msg_pli_error_dbg"
#define TBP_MSG_PLI_PANIC  "tbp_msg_pli_panic_dbg"

#define TBP_DEFAULT_C_LOG_FILE "c-side.log"
#define TBP_DEFAULT_SIM_LOG_FILE "sim-side.log"


/*
 * this are accellerators to cut the string comparison down in the 
 * messaging function - this prevents checks for both pli and non
 * pli copies of a debug group - these need to track the real
 * debug groups
 */
#define TBP_MSG_ERROR_DBG_TAG "error_dbg"
#define TBP_MSG_PANIC_DBG_TAG "panic_dbg"
#define TBP_MSG_WARN_DBG_TAG  "warn_dbg"
#define TBP_MSG_PLI_TAG "tbp_msg_pli"
#define TBP_MSG_PRINTF_TAG "printf_dbg"

/*
 * internal static print buffer sizees
 */
#define TBP_MAX_PRINT_STRING  16384
#define TBP_MAX_PRINT_FORMAT_STRING 128
#define TBP_MAX_OUTPUT_STRING 32768
#define TBP_MAX_DEBUG_STRING  16384
/*
 * define some built-in levels
 */
#define MSG_LVL_FORCE -1
#define MSG_LVL_INFO   0
#define MSG_LVL_DEBUG 10
#define MSG_LVL_TRACE 40

/*
 * defines for the error callback function 
 */
#define TBP_MSG_ERROR_LEVEL  0
#define TBP_MSG_PANIC_LEVEL  1
#define TBP_MSG_WARN_LEVEL   2
#define TBP_MSG_NORMAL_LEVEL 3
 
/*
 * prototypes
 */
void tbp_message_module_init(void);

void tbp_set_max_error_count(int maxerr);
int tbp_get_c_error_count();
int tbp_get_sim_error_count(); 
int tbp_get_error_count(); 
void tbp_set_max_warning_count(int maxwarn);
int tbp_get_max_warning_count(void);
int tbp_get_warning_count();
void tbp_set_flush_logfile(int flush);
BOOL tbp_get_flush_logfile(void);

/* function to actually flush the log file */
void tbp_flush_logfile(void);

FILE* tbp_get_logfile();
void tbp_set_c_logfile(FILE *tbp_logfile);
void tbp_set_sim_logfile(FILE *tbp_logfile);
int tbp_c_error_inc (void);
int tbp_sim_error_inc (void);
int tbp_warn_inc(void);
void tbp_error_clear(void);
void tbp_clear_accumulated_errors(void);
void tbp_clear_accumulated_warnings(void);
int tbp_get_error_count_all(void);
int tbp_get_warning_count_all(void);

/* set/get the detect_sim_error state */
BOOL tbp_get_detect_sim_errors(void);
void tbp_set_detect_sim_errors(BOOL flag);

/* set/get suppress warnings state */
BOOL tbp_get_suppress_warnings(void);
void tbp_set_suppress_warnings(BOOL flag);

/* set the screen debug level */
void tbp_set_debug_level(int debug_level);

/* set the log debug level */
void tbp_set_log_debug_level(int debug_level);

/* get the screen debug level */
int tbp_get_debug_level(void);

/* get the log debug level */
int tbp_get_log_debug_level(void);   
void tbp_add_debug_group(char *group);
int tbp_del_debug_group(char *group);
void tbp_list_debug_groups(void);
void tbp_set_sim_screen_print(BOOL print);
void tbp_set_sim_log_print(BOOL print);
/* function to enable/disable pli printing to the c-side screen */
void tbp_set_sim_cside_screen_print(BOOL print);
/* function to enable/disable pli printing to the c-side log */
void tbp_set_sim_cside_log_print(BOOL print);
/* function to enable/diable adding the group,level,file,func,line debug to all messages */
void tbp_set_message_debug(BOOL enable);

BOOL tbp_check_debug_group(const char *group);

/* 
 * this function assigns the user error function which is called
 * anytime a warn/error/panic message is encountered
 * the type (warn/error/panic) is encoded as the input:
 * 2 = warn 1 = panic 0 = error
 */
void tbp_set_user_error_function(void (*func)(int));

/* Call the user error function if one has been registered.  */
void tbp_user_error_function(int type);

/* function to assign the screen output stream */
void tbp_set_screen_output_stream(FILE *stream);

/* function to assign the screen input stream */
void tbp_set_screen_input_stream(FILE *stream);

/* functions to retrieve the in/out streams */
FILE *tbp_get_stdout(void);
FILE *tbp_get_stdin(void);

/* main messaging functions determine which messages are printed to which locations */ 
int tbp_va_list_msg(const char *file, const char *func, int line, const char *group, int level, const char * fmt, va_list argp);
void setTbpMsgInfo(const char* _file, const char* _func, int _line, const char* _group, int _level);
int tbp_msg(const char *file, const char *func, int line, const char *group, int level, ...);
int tbp_new_msg(const char *fmt, ...);
int tbp_new_dmsg(const char* group, int level, const char *fmt,  ...);
int tbp_new_pdmsg( int level, const char *fmt, ...);


#if pfSPARCWORKS
#define __PRETTY_FUNCTION__ __func__
#endif

#define TBP_FFL (const char *) __FILE__, (const char *) __PRETTY_FUNCTION__, (int) __LINE__
#define CPF_FFL (const char *) __FILE__, (const char *) __PRETTY_FUNCTION__, (int) __LINE__

/* definition for use with tbp_message */
#define TBP_PRINTF TBP_FFL, TBP_MSG_PRINTF_GRP, MSG_LVL_FORCE
#define TBP_INFO   TBP_FFL, TBP_MSG_INFO_GRP,   MSG_LVL_FORCE
#define TBP_WARN   TBP_FFL, TBP_MSG_WARN_GRP,   MSG_LVL_FORCE
#define TBP_ERROR  TBP_FFL, TBP_MSG_ERROR_GRP,  MSG_LVL_FORCE
#define TBP_PANIC  TBP_FFL, TBP_MSG_PANIC_GRP,  MSG_LVL_FORCE

/* definition of the standard print types */
#define tbp_printf     setTbpMsgInfo(TBP_PRINTF), tbp_new_msg
#define tbp_info       setTbpMsgInfo(TBP_INFO),   tbp_new_msg
#define tbp_warn       setTbpMsgInfo(TBP_WARN),   tbp_new_msg
#define tbp_error      setTbpMsgInfo(TBP_ERROR),  tbp_new_msg
#define tbp_panic      setTbpMsgInfo(TBP_PANIC),  tbp_new_msg

#define tbp_debug      setTbpMsgInfo(TBP_FFL, NULL, 0), tbp_new_dmsg
#define tbp_lib_debug  setTbpMsgInfo(TBP_FFL, NULL, 0), tbp_new_dmsg


#define MSG_Panic      setTbpMsgInfo(TBP_PANIC),  tbp_new_msg
#define MSG_Error      setTbpMsgInfo(TBP_ERROR),  tbp_new_msg
#define MSG_Warn       setTbpMsgInfo(TBP_WARN),   tbp_new_msg
#define MSG_Milestone  setTbpMsgInfo(TBP_INFO),   tbp_new_msg
#define MSG_Printf     setTbpMsgInfo(TBP_PRINTF), tbp_new_msg
#define MSG_Debug      setTbpMsgInfo(TBP_DEBUG),  tbp_new_msg

#define MSG_PrintfDebug     setTbpMsgInfo(TBP_FFL, TBP_MSG_PRINTF_GRP, 0),  tbp_new_pdmsg

#define MSG_LibDebug        setTbpMsgInfo(TBP_DEBUG),     tbp_new_dmsg 

#ifdef __cplusplus
}
#endif

#endif
