//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_HASH_H
#define TBP_HASH_H

#include "tbp_internal.h"

#ifdef __cplusplus
extern "C" {
#endif

#define TBP_HASH_BUCKETS 257
#define hash_panic tbp_panic
#define hash_error tbp_error

/* malloc/copy the user provided string */
#ifndef TBP_HASH_SAVE_STRING
#define TBP_HASH_SAVE_STRING
#endif
#ifdef TBP_HASH_DEBUG
#define hash_print(fmt,args...) tbp_printf(fmt,##args)
#define tbp_print_hash_structure(_table_) tbpi_print_hash_structure(_table_)
#else
#define hash_print(args...)
#define tbp_print_hash_structure(_table_)
#endif

typedef struct {
  char *string;    // string name being tested by the hash
  void *ptr;       // value being sought by the hash
  void *link;      // link ptr to the next duplicate
  void *link_prev; // link ptr to the prior duplicate
  void *prev;      // link ptr to the prior chain entry
  void *nxt;       // link ptr to the next chain entry
} TBP_HASH_CHAIN_T;

typedef struct {
  //TBP_HASH_CHAIN_T *bucket[TBP_HASH_BUCKETS];
  TBP_HASH_CHAIN_T **bucket;
  TBP_HASH_CHAIN_T *chain;
  TBP_HASH_CHAIN_T *chain_tail;
} TBP_HASH_T;

/* create a hash table */
TBP_HASH_T *tbp_hash_create(void);

/* destroy a hash table */
void tbp_hash_destroy(TBP_HASH_T *table);
       
/* add an entry into a hash table */
void tbp_hash_add(TBP_HASH_T *table, const char *name, void *ptr);
  
/* remove an entry from a hash table */
void tbp_hash_remove(TBP_HASH_T *table, const char *name);

/* hash a given name and return a pointer to the data in the table */
void *tbp_hash(TBP_HASH_T *table, const char *name);

#ifdef __cplusplus
}
#endif
#endif
