//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_THREADS_H
#define TBP_THREADS_H

#include "tbp_utils.h"
#include "tbp_message.h"

#ifdef USE_PTHREADS
#include "tbp_pthreads.h"
#endif
#ifdef USE_CTHREADS
#include "tbp_cthreads.h"
#endif
#ifdef USE_QTHREADS
#include "tbp_qthreads.h"
#endif
#ifdef USE_WINTHREADS
#include "tbp_winthreads.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define TBP_THREAD_TYPE_INVALID 0
#define TBP_THREAD_TYPE_SIM     1
#define TBP_THREAD_TYPE_MAIN    2
#define TBP_THREAD_TYPE_USER    3
#define TBP_THREAD_TYPE_CMODEL  4
#define TBP_THREAD_TYPE_OTHER   5

#define TBP_THREAD_STATUS_INVALID 0
#define TBP_THREAD_STATUS_RUN     1
#define TBP_THREAD_STATUS_WAIT    2
#define TBP_THREAD_STATUS_DONE    3 
#define TBP_THREAD_STATUS_KILL    4

#define TBP_THREAD_DEFAULT_STACK_SIZE 1000000

/* function to intialize the thread module */
void tbp_thread_module_init(void);

/* function to return the current thread count */
u_int32 tbp_get_thread_count(void);

/* function to decriment the thread count */
void tbp_dec_thread_count(void);

/* return the id of the calling thread */
u_int32 tbp_get_my_thread_id(void);

/* initialize a given thread prior to creation */
int tbp_thread_init(void);

/* function to "kill" a thread */
void tbpi_thread_kill(u_int32 thread_index,FFL_ARGS);

/* Free thread structure.  */
void tbp_thread_free(u_int32 thread_index);

/* function to clear out the thread struct as it destroyed */
void tbp_thread_struct_cleanup(u_int32 thread_index);

/* function which removes a thread from the list of threads */
void tbp_thread_remove(u_int32 thread_cnt, u_int32 thread_index);

/* thread wrapper for q/pthreads */
void *tbp_thread_wrapper(void *thread_index);

/* exit state check */
void tbp_check_thread_exit(u_int32 thread_index);

/* check for a pending interrupt */
void tbp_check_interrupt(u_int32 thread_index);

/* function to set the interrupt handler for a given thread */
void tbpi_thread_set_interrupt_handler(u_int32 thread_id,  void (*int_func)(u_int32), FFL_ARGS);

/* function similar to interrupt check, but for resets  */
void tbp_check_reset(u_int32 thread_index);

/* this function deletes all of the "user" threads - NOT main */
void tbpi_delete_user_threads(FFL_ARGS);
#define tbp_delete_user_threads() tbpi_delete_user_threads(FFL_DATA)

/* user space function used to attach a c thread to a transactor */
void setTbpPathInfo(char* _file, char* _func, int _line);
int tbpi_va_list_path_attach(FFL_ARGS, const char *path, void (*func_ptr)(), u_int32 num_args, va_list ap);
int tbpi_path_attach(FFL_ARGS, const char *path, void (*func_ptr)(), u_int32 num_args, ...);
int tbpi_new_path_attach(const char *path, void (*func_ptr)(), u_int32 num_args, ...);

#define tbp_path_attach(_path_,_func_,_num_args_,...) \
        tbpi_path_attach(FFL_DATA,_path_,_func_,_num_args_, __VA_ARGS__)

/*
 * function used to set the globcl (current_thread) and also to
 * allow cmodel thread relocation
 */
void tbp_set_current_thread(u_int32 *thread_index);

/*
 * function to conditional complete a thread relocation sequence
 */
void tbp_check_thread_relocate(u_int32 thread_index);

/* display thread status */
void tbp_print_thread_status(void);

#ifdef __cplusplus
}
#endif

#endif
