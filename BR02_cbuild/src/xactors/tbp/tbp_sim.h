//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_SIM_H
#define TBP_SIM_H

#ifdef __cplusplus
extern "C" {
#endif

/* basic write functions */
void tbpi_sim_write8(u_int64 addr, u_int8 data, FFL_ARGS);
void tbpi_sim_write16(u_int64 addr, u_int16 data, FFL_ARGS);
void tbpi_sim_write32(u_int64 addr, u_int32 data, FFL_ARGS);
void tbpi_sim_write64(u_int64 addr, u_int64 data, FFL_ARGS);

/* basic read functions */
u_int8 tbpi_sim_read8(u_int64 addr, FFL_ARGS);
u_int16 tbpi_sim_read16(u_int64 addr, FFL_ARGS);
u_int32 tbpi_sim_read32(u_int64 addr, FFL_ARGS);
u_int64 tbpi_sim_read64(u_int64 addr, FFL_ARGS);

/* burst write */
u_int32 tbpi_sim_write(u_int64 addr, void *data, u_int32 size, u_int32 width, FFL_ARGS);

/* burst read */
u_int32 tbpi_sim_read(u_int64 addr, void *data, u_int32 size, u_int32 width, FFL_ARGS);

/* burst write zx */
u_int32 tbpi_sim_write_zx(u_int64 addr, void *data, void *datax, u_int32 *size, u_int32 width, FFL_ARGS);

/* burst read zx */
u_int32 tbpi_sim_read_zx(u_int64 addr, void *data, void *datax, u_int32 *size, u_int32 width, FFL_ARGS);

/* configuration space write */
void tbpi_sim_cs_write(u_int64 addr, u_int64 data, FFL_ARGS);

/* configuration space read */
u_int64 tbpi_sim_cs_read(u_int64 addr, FFL_ARGS);

/* user defined operation */
void tbpi_sim_userop(u_int32 opcode, u_int64 addr, u_int64 *data, u_int64 *datax, 
		     u_int32 *size, u_int32 *status, FFL_ARGS);

/* function to return the status of the previous operation */
u_int32 tbp_sim_status(void);

/* function to idle N cycles */
void tbpi_sim_idle(u_int32 idle_cnt, FFL_ARGS);

/* function to idle N cycles */
void tbpi_sim_sleep(u_int64 timeout, FFL_ARGS);

/* Reset Transaction */
void tbpi_sim_reset(FFL_ARGS);

/* function used to assign an interrupt handler to a given thread */
void tbpi_sim_set_interrupt_handler(u_int32 id, void (*int_func)(u_int32), FFL_ARGS);

/* function used to assign an reset handler to a given thread id */
void tbpi_sim_set_reset_handler(u_int32 id, void (*rst_func)(u_int32), FFL_ARGS);

/* basic write functions */
#define tbp_sim_write8(_addr_,_data_)  tbpi_sim_write8((u_int64)_addr_,(u_int8)_data_, FFL_DATA)
#define tbp_sim_write16(_addr_, _data_) tbpi_sim_write16((u_int64)_addr_,(u_int16)_data_, FFL_DATA)
#define tbp_sim_write32(_addr_, _data_) tbpi_sim_write32((u_int64)_addr_,(u_int32)_data_, FFL_DATA)
#define tbp_sim_write64(_addr_, _data_) tbpi_sim_write64((u_int64)_addr_,(u_int64)_data_, FFL_DATA)

/* basic read functions */
#define tbp_sim_read8(_addr_)  tbpi_sim_read8 ((u_int64)_addr_, FFL_DATA)
#define tbp_sim_read16(_addr_) tbpi_sim_read16((u_int64)_addr_, FFL_DATA)
#define tbp_sim_read32(_addr_) tbpi_sim_read32((u_int64)_addr_, FFL_DATA)
#define tbp_sim_read64(_addr_) tbpi_sim_read64((u_int64)_addr_, FFL_DATA)

/* burst write */
#define tbp_sim_write(_addr_, _data_, _size_) tbpi_sim_write((u_int64)_addr_,(u_int64 *)_data_,(u_int32)_size_, TBP_WIDTH_64P, FFL_DATA)

/* burst read */
#define tbp_sim_read(_addr_,_data_,_size_) tbpi_sim_read((u_int64)_addr_,(u_int64 *)_data_,(u_int32)_size_, TBP_WIDTH_64P, FFL_DATA)

/* burst write zx */
#define tbp_sim_write_zx(_addr_,_data_,datax_,_size_) \
tbpi_sim_write_zx((u_int64)_addr_,(u_int64 *)_data_,(u_int64)_datax_,(u_int32)_size_, TBP_WIDTH_64P, FFL_DATA)

/* burst read zx */
#define tbp_sim_read_zx(_addr_,_data_,_datax_,_size_) \
tbpi_sim_read_zx((u_int64)_addr_,(u_int64 *)_data_,(u_int64)_datax_,(u_int32)_size_, TBP_WIDTH_64P, FFL_DATA)

/* configuration space write */
#define tbp_sim_cs_write(_addr_,_data_) tbpi_sim_cs_write((u_int64)_addr_,(u_int64 *)_data_, FFL_DATA)

/* configuration space read */
#define tbp_sim_cs_read(_addr_) tbpi_sim_cs_read((u_int64)_addr_, FFL_DATA)

/* user defined operation */
#define tbp_sim_userop(_opcode_,_addr_,_data_,_datax_,_size_,_status_) \
tbpi_sim_userop((u_int32)_opcode_,(u_int64)_addr_,(u_int64 *)_data_,(u_int64)_datax_,(u_int32 *)_size_,(u_int32 *)_status_, FFL_DATA)

/* function to idle N cycles */
#define tbp_sim_idle(_idle_cnt_) tbpi_sim_idle(_idle_cnt_, FFL_DATA)

/* function to sleep a transactor */
#define tbp_sim_sleep(_timeout_) tbpi_sim_sleep(_timeout_, FFL_DATA)

/* function used to assign an interrupt handler to a given thread */
#define tbp_sim_set_interrupt_handler(_id_,_func_) tbpi_sim_set_interrupt_handler(_id_,_func_, FFL_DATA)

/* function used to assign an reset handler to a given thread */
#define tbp_sim_set_reset_handler(_id_,_func_) tbpi_sim_set_reset_handler(_id_,_func_, FFL_DATA)

/* check return status */
void tbpi_check_transaction(u_int32 direction, u_int32 smask, FFL_ARGS);
#define tbp_check_transaction(_direction_,_smask_) tbpi_check_transaction(_direction_,_smask_,FFL_DATA)


/* Macro to attach a threaded function to a 'hdl' transactor */
#define carbonXAttach(_path_,_func_,_num_args_,...) tbpi_path_attach(TBP_FFL,_path_,_func_,_num_args_, __VA_ARGS__)

/* Macros to support write/read to transactors */
#define carbonXWrite8(a,d)	        tbpi_sim_write8 ((u_int64)a,d,FFL_DATA)
#define carbonXWrite16(a,d)	tbpi_sim_write16((u_int64)a,d,FFL_DATA)
#define carbonXWrite32(a,d)	tbpi_sim_write32((u_int64)a,d,FFL_DATA)
#define carbonXWrite64(a,d)        tbpi_sim_write64((u_int64)a,d,FFL_DATA)
#define carbonXWriteArray8(a,d,s)  tbpi_sim_write  ((u_int64)a,d,s,TBP_WIDTH_8,FFL_DATA)
#define carbonXWriteArray16(a,d,s) tbpi_sim_write  ((u_int64)a,d,s,TBP_WIDTH_16,FFL_DATA)
#define carbonXWriteArray32(a,d,s) tbpi_sim_write  ((u_int64)a,d,s,TBP_WIDTH_32,FFL_DATA)
#define carbonXWriteArray64(a,d,s) tbpi_sim_write  ((u_int64)a,d,s,TBP_WIDTH_64,FFL_DATA)
  /* carbonXWrite takes an array of 32-bit words.  This must remain this way because
     prep and current test suites assume 32-bit arrays.  */
#define carbonXWrite(a,d,s)	tbpi_sim_write  ((u_int64)a,(u_int32*)d,s,TBP_WIDTH_32,FFL_DATA)
#define carbonXWriteZX(addr,data,datax,size)  tbpi_sim_write_zx (addr,data,datax,size,TBP_WIDTH_32,FFL_DATA)

#define carbonXRead8(a)	        tbpi_sim_read8  ((u_int64)a,FFL_DATA)
#define carbonXRead16(a)		tbpi_sim_read16 ((u_int64)a,FFL_DATA)
#define carbonXRead32(a)		tbpi_sim_read32 ((u_int64)a,FFL_DATA)
#define carbonXRead64(a)           tbpi_sim_read64 ((u_int64)a,FFL_DATA)
#define carbonXReadArray8(a,d,s)   tbpi_sim_read   ((u_int64)a,d,s,TBP_WIDTH_8,FFL_DATA)
#define carbonXReadArray16(a,d,s)  tbpi_sim_read   ((u_int64)a,d,s,TBP_WIDTH_16,FFL_DATA)
#define carbonXReadArray32(a,d,s)  tbpi_sim_read   ((u_int64)a,d,s,TBP_WIDTH_32,FFL_DATA)
#define carbonXReadArray64(a,d,s)  tbpi_sim_read   ((u_int64)a,d,s,TBP_WIDTH_64,FFL_DATA)
  /* carbonXRead fills an array of 32-bit words.  This must remain this way because
     prep and current test suites assume 32-bit arrays.  */
#define carbonXRead(a,d,s)	        tbpi_sim_read   ((u_int64)a,d,s,TBP_WIDTH_32,FFL_DATA)
#define carbonXReadZX(addr,data,datax,size)   tbpi_sim_read_zx  (addr,data,datax,size,TBP_WIDTH_32,FFL_DATA)

#define carbonXTransaction(opcode,addr,data,datax,size,status,direction,width) \
        tbpi_transaction(tbp_get_my_trans_id(),opcode,addr,data,datax,size,status,direction,width,FFL_DATA)

#define carbonXSleep(to)		tbpi_sim_sleep(to,   FFL_DATA)
#define carbonXIdle(cycles)	tbpi_sim_idle(cycles,FFL_DATA)
#define carbonXEvtSleep(to)	tbpi_evt_sleep(to,   FFL_DATA)


#define carbonXCsRead(a)	        tbpi_cpf_cs_read(a,FFL_DATA)
#define carbonXCsWrite(a,d)	tbpi_cpf_cs_write(a,d,FFL_DATA)

/* Macro to return the status after the previous transaction */
#define carbonXGetStatus()         tbp_sim_status();

/* Macro to set interrupt handler function */
#define carbonXSetInterruptHandler(i) tbpi_cpf_set_interrupt_handler((void *)i, (char *)#i, FFL_DATA)

/* Macro for generic, user-defined functions */
#define carbonXUserOp(opcode,addr_h,addr_l,data,datax,size,status)             \
	tbpi_cpf_userop((u_int32)opcode,(u_int32)addr_h,(u_int32)addr_l,(u_int32 *)data,(u_int32 *)datax,(u_int32 *)size,(u_int32 *)status,FFL_DATA)

/* Macro to get the current operation (new preffered method) */
#define carbonXGetOperation() tbp_get_operation()

/* Macro to send/complete a sim-side transaction (new prefered method) */
#define carbonXSendOperation(_op_) tbpi_send_operation(tbp_get_my_trans_id(),_op_,FFL_DATA)

//    EVENTS
// For use by the top level routine only
#define carbonXSetWaitTest(sx,to)          tbpi_evt_set_waittest   (sx,     to, TBP_FFL)
#define carbonXCheckWait                   tbp_evt_check_my_thread_wait

#define carbonXSetSignal(sx)               tbpi_evt_signal     (sx,         TBP_FFL)
#define carbonXWaitSignal(sx, to)          tbpi_evt_wait       (sx,     to, TBP_FFL)
#define carbonXSetSignalState(sx, st)      tbpi_evt_signalstate(sx, st,     TBP_FFL)
#define carbonXWaitSignalState(sx, st, to) tbpi_evt_waitstate  (sx, st, to, TBP_FFL)
#define carbonXCheckSignalState(sx)        tbpi_evt_checkstate (sx,         TBP_FFL)

/* Macro to set/get the current inst_data pointer */
#define carbonXSetInstData(ptr) tbpi_set_inst_data((void *)ptr, TBP_FFL)
#define carbonXGetInstData() tbpi_get_inst_data(TBP_FFL)


#ifdef __cplusplus
}
#endif

#endif
