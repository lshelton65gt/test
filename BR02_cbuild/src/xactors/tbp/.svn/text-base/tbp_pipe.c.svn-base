//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include "tbp_pipe.h"

/* the "pipes" to which we will read/write */
static int toc_pipe = 0;
static int tosim_pipe = 0;

static char *tbp_tosim_file_name=NULL;
static char *tbp_toc_file_name=NULL;

static int pipe_direction = 0;
static int pipe_init_phase = 0;
static int pipe_connected = 0;
static int pipe_sigint_cnt = 0;

/*
 * function to return the state of pipe_conencted
 */
int tbp_pipe_connected(void)
{
  return pipe_connected;
}

/*
 * function to set the pipe direction - direction detemrines which pipe is read or write
 */
void tbp_pipe_set_direction(int direction)
{
  pipe_direction = direction;
}

/*
 * function to return the pipe filenames given an ident string 
 */
void tbp_pipe_get_filenames(char *ident_string, char **toc_file_name, char **tosim_file_name)
{
  int ident_string_length = ident_string != NULL ? strlen(ident_string) : 0;
  int toc_name_length = ident_string_length + strlen(TBP_TOC_PIPE_FILENAME_BASE)+10;
  int tosim_name_length = ident_string_length + strlen(TBP_TOSIM_PIPE_FILENAME_BASE)+10;
  
  /* allocate space for pipe names - use tbp_allocate later */
  *toc_file_name=(char *)tbp_allocate_buffer(toc_name_length, "C->SIM PIPE Name");
  *tosim_file_name=(char *)tbp_allocate_buffer(tosim_name_length, "SIM->C PIPE Name");

  if(ident_string_length != 0) {
    sprintf(*toc_file_name,"%s_%d_%s",TBP_TOC_PIPE_FILENAME_BASE,(int) getuid(),ident_string);
    sprintf(*tosim_file_name,"%s_%d_%s",TBP_TOSIM_PIPE_FILENAME_BASE,(int) getuid(),ident_string);
  }
  else {
    sprintf(*toc_file_name,"%s_%d",TBP_TOC_PIPE_FILENAME_BASE,(int) getuid());
    sprintf(*tosim_file_name,"%s_%d",TBP_TOSIM_PIPE_FILENAME_BASE,(int) getuid());
  }
}

/* 
 * function to forceably clean out files given an ident 
 */
void tbp_pipe_delete_ident(char *tbp_ident_string)
{
  char *toc_file_name, *tosim_file_name;
  
  /* clear out our own setup */
  tbp_pipe_clean();
  
  /* now remove the requested resources - in case they do not match! */
  tbp_pipe_get_filenames(tbp_ident_string, &toc_file_name, &tosim_file_name);
  remove(toc_file_name);
  remove(tosim_file_name);
  tbp_free(toc_file_name);
  tbp_free(tosim_file_name);
}

/*
 * function to cleanup the pipes (delete the pipe files)
 */
void tbp_pipe_clean(void)
{
  /* clear the connected state */
  pipe_connected = 0;

  /* clear out our global info if it is set resources */
  if(toc_pipe)
    close(toc_pipe);
  if(tosim_pipe)
    close(tosim_pipe);
  if(tbp_toc_file_name)
    remove(tbp_toc_file_name);
  if(tbp_tosim_file_name)
    remove(tbp_tosim_file_name);
  toc_pipe = 0;
  tosim_pipe = 0;
  //tbp_free(tbp_tosim_file_name);
  //tbp_free(tbp_toc_file_name);
}

/*
 * function "flush" the pipe
 */
void tbp_pipe_flush(void)
{
  int read_pipe = pipe_direction==TBP_PIPE_MASTER ? toc_pipe : tosim_pipe;
  int pipe_fflags=0;
  int buf;
  int flush_cnt=0;
 
  /* furce the pipe to be non-blocking */
  pipe_fflags = fcntl(read_pipe,F_GETFL);
  pipe_fflags |= O_NONBLOCK;
  fcntl(read_pipe,F_SETFL,pipe_fflags);

  while(read(read_pipe,&buf,1) > 0)
    flush_cnt += 1;

  /* return the pipe to its normal blocking state */
  pipe_fflags &= ~O_NONBLOCK;
  fcntl(read_pipe,F_SETFL,pipe_fflags);

  pipe_fflags = fcntl(read_pipe,F_GETFL);
}

/*
 * Create 2 unidirection pipes (tosim/tohdl) with given files names
 * either process may create them, so we need to check to see if they 
 * already exist
 */
int tbp_pipe_create(char *tosim_pipe_name, char *toc_pipe_name)
{
  char errBuf[1024];
  BOOL pipe_exists = FALSE;
  
  umask(0);
  if (mkfifo(tosim_pipe_name, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)){
    if(errno==EEXIST) {
      pipe_exists = TRUE;
    }
    else {
      tbp_panic("tbp_pipe_create: fifo creation returned an error: %s\n",carbon_lasterror(errBuf, sizeof(errBuf)));
    }
  }
  
  if (mkfifo(toc_pipe_name, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)){
    if(errno==EEXIST) {
      //printf("fifo already exists - we must be second in line!\n");
    }
    else {
      tbp_panic("tbp_pipe_create: fifo creation returned an error: %s\n",carbon_lasterror(errBuf, sizeof(errBuf)));
    }
  }

  return 0;
}

/*
 * function to open the pipes - to pipes are opened read/write
 * we impose discipline upon ourselves to NOT read/write the wrong
 * pipe
 */
int tbp_pipe_open(char *tbp_ident_string)
{
  int ret_val=0;
  
  /* setup a safty-net for bad shutdowns to limit tbp_ident orphaning */
  atexit(tbp_pipe_clean);

  /* function to build filename based on uid+ident */
  tbp_pipe_get_filenames(tbp_ident_string, &tbp_toc_file_name, &tbp_tosim_file_name);

  /* create the pipes */
  if(0 != (ret_val = tbp_pipe_create(tbp_tosim_file_name,tbp_toc_file_name)))
    return ret_val;
  
  /* open the read pipe */
  if(0==(toc_pipe = open(tbp_toc_file_name,O_RDWR))){
    tbp_panic("\nFailed to open the sim->c pipe (%s)\n",tbp_toc_file_name);
  }
  /* open the write pipe */
  if(0==(tosim_pipe = open(tbp_tosim_file_name,O_RDWR))){
    tbp_panic("Failed to open the c->sim pipe (%s)\n",tbp_tosim_file_name);
  }
  
  return 0;
}

static void tbp_pipe_sigint_handler(int sig)
{
  /* interrupt during intitialization is an immediate failure */
  if(pipe_init_phase==1) {
    tbp_printf("\ntbp: interrupt detected - ipc init terminating\n");
    tbp_pipe_clean();
    exit(-1);
  }
  /* interrupt during a read requires multiple occurances to actually stop */
  else {
    if(pipe_sigint_cnt > PIPE_SIGINT_THRESHOLD) {
      tbp_c_error_inc();
      tbp_printf("\ntbp: read operation interrupted - shutting down\n"); 
      tbp_shutdown(-1);
    }
    else {
      pipe_sigint_cnt +=1 ;
    }
  }
}

static struct sigaction *sigact,*prev_sigact;

/*
 * pipe initialization function - mode decides direction of pipes
 */
int tbp_pipe_init(int mode, char *tbp_ident_string)
{
  int init_msg_data = mode==TBP_PIPE_MASTER ? 
    TBP_PIPE_MASTER_READY: TBP_PIPE_SLAVE_READY;
  int rcv_init_data;
  int expected_rcv_response = mode==TBP_PIPE_MASTER ? TBP_PIPE_SLAVE_READY : TBP_PIPE_MASTER_READY;
  BOOL rcv_init_error = FALSE;
  //struct sigaction sigact,prev_sigact;

  /* do nothing if we are in no-hdl mode */
  if(tbp_get_no_hdl_flag()) return 0;

  /* 
   * allocate glocal (to this module) sigaction - primarily to make memory
   * checkers happy as we can/will calloc to avoid UMRs
   */
  sigact = (struct sigaction *) tbp_allocate_buffer(sizeof(struct sigaction),
						    "Pipe sigaction");
  prev_sigact = (struct sigaction *) tbp_allocate_buffer(sizeof(struct sigaction),
							 "Pipe sigaction (previous)");


  /* prevent UMR in memory checking tools */
  sigact->sa_handler = prev_sigact->sa_handler = NULL;
  sigact->sa_flags   = prev_sigact->sa_flags = 0;

  /* signal that this is intialization in which we want to allow ^Cs */
  pipe_init_phase = 1;
  tbp_printf("tbp: initializing ipc... ");

  /* limit possibility of race with the other process */
  if(mode==TBP_PIPE_MASTER)
    sleep(1);

  /* install a temporary signal handler during init */
  sigact->sa_handler = tbp_pipe_sigint_handler;
  sigact->sa_flags = SA_NODEFER | SA_RESTART;
  sigaction(SIGINT,sigact,prev_sigact);
  
  /* set the direction bit to determine which pipe is to be read and which is to be written */
  tbp_pipe_set_direction(mode);

  /* open our pipes */
  tbp_pipe_open(tbp_ident_string);
  
  /* send the initialization message */
  if(sizeof(int)!=tbp_pipe_write(&init_msg_data,sizeof(int))){
    tbp_printf("\n");
    tbp_panic("Failed to send pipe initialization message\n");
  }
  else {
    if(sizeof(int)!=tbp_pipe_read(&rcv_init_data)) { 
      tbp_printf("\n");
      tbp_panic("Initialization read failed\n");
    }
    else {
      /* if there was an error - report failure */
      if(rcv_init_data!=expected_rcv_response) { 
	tbp_printf("\n");
	tbp_panic("Invalid pipe initialization response (0x%8.8x) (rcv_init_error %d)\n",
		  rcv_init_data,rcv_init_error);
      }
    }
  }

  tbp_printf("done\n");

  /* from now on, ^C will be blocked! */
  pipe_init_phase = 0;
  
  /* restore the previous signal handler */
  sigaction(SIGINT,prev_sigact,NULL);

  /* set the connected flag */
  pipe_connected = 1;

  return 0;
}

/* 
 * base function used to write buffer (sends size prior to the provided buffer)
 */
int tbp_pipe_write(void *buf, int size)
{
  sigset_t blockInt;
  int write_size = size;
  int pipe = pipe_direction==TBP_PIPE_MASTER ? tosim_pipe : toc_pipe;
  
  if(pipe_init_phase==0) {
    /* The following three lines should be the equivalent of sighold()
       which is being phased out on Linux.  */
    sigemptyset(&blockInt);
    sigaddset(&blockInt, SIGALRM);
    sigprocmask(SIG_BLOCK, &blockInt, NULL);
    //sighold(SIGALRM);
  }
  
  if(pipe_connected==0 && pipe_init_phase==0) return 0;

  /* write are 2 stage - write the msg header, then the body */
  if(write(pipe,&size,sizeof(int))==0){
    tbp_panic("tbp_pipe_write: header write failed - cannot transmit the remainder of the message\n");
  }

  /* now write the body of the message */
  if(size !=0){
    if(pipe_connected==0 && pipe_init_phase==0) return 0;
    if(0==(write_size=write(pipe,buf,write_size))){
      tbp_panic("tbp_pipe_write: body write failed - could not transmit the message\n");
    }
  } 

  if(pipe_init_phase==0) {
    sigprocmask(SIG_UNBLOCK, &blockInt, NULL);
    //sigrelse(SIGALRM);
  }
  return write_size;
}

/* 
 * base function to read (reads the size first followed by the buffer)
 */
int tbp_pipe_read(void *buf)
{
  sigset_t blockInt;
  int read_size = 0;
  int total_bytes_read = 0, bytes_read = 0;
  int pipe = pipe_direction==TBP_PIPE_MASTER ? toc_pipe : tosim_pipe;
  //struct sigaction sigact,prev_sigact;

  if(pipe_init_phase==0) {
    /* The following three lines should be the equivalent of sighold()
       which is being phased out on Linux.  */
    sigemptyset(&blockInt);
    sigaddset(&blockInt, SIGALRM);
    sigprocmask(SIG_BLOCK, &blockInt, NULL);
    //sighold(SIGALRM); 
    /* install a temporary signal handler during init */
    sigact->sa_handler = tbp_pipe_sigint_handler;
    sigact->sa_flags = SA_NODEFER | SA_RESTART;
    sigaction(SIGINT,sigact,prev_sigact);
    pipe_sigint_cnt = 0;
  }

  if(pipe_connected==0 && pipe_init_phase==0) return 0;

  /* reads are 2 stage - read the msg header, then the body */
  while(total_bytes_read < 4){
    if(pipe_connected==0 && pipe_init_phase==0) return 0;
    
    if(0==(bytes_read = read(pipe,(&read_size+total_bytes_read),(4-total_bytes_read)))){
      tbp_panic("tbp_pipe_read: header read failed - cannot retrieve the remainder of the message\n");
    }
    else {
      total_bytes_read += bytes_read;
    }
  }

  total_bytes_read = 0;
  bytes_read = 0;

  /* now read the body of the message */
  if(read_size != 0) {
    /* allow for fragmented/interrupted reads */
    while(total_bytes_read < read_size){
      if(pipe_connected==0 && pipe_init_phase==0) return 0;
     
      if(0==(bytes_read = read(pipe,(buf+total_bytes_read),(read_size-total_bytes_read)))){
	tbp_panic("tbp_pipe_read: body read failed - could not retrieve the message\n");
      }
      else {
	total_bytes_read += bytes_read;
      }
    }
  }
 
  if(pipe_init_phase==0) { 
    sigprocmask(SIG_UNBLOCK, &blockInt, NULL);
    //sigrelse(SIGALRM);
    /* restore the previous signal handler */
    sigaction(SIGINT,prev_sigact,NULL);
  }
  return read_size;
}
