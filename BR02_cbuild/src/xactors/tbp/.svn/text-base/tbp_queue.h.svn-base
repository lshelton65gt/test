//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_QUEUE_H
#define TBP_QUEUE_H

#include "tbp_internal.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "tbp_hash.h"

typedef struct {
  void *ptr;
  void *nxt;
} TBP_QE_T;

typedef struct {
  char *name;
  TBP_QE_T *head;
  TBP_QE_T *tail;
  void *nxt;
  void *prev;
  u_int32 depth;
} TBP_QUEUE_T;

typedef struct {
  TBP_QUEUE_T *queue;
  TBP_QUEUE_T *tail;
  TBP_HASH_T *hash;
} TBP_QM_T;

/* default hash "buckets" used to accelerate name lookup */
#define TBP_QUEUE_HASH_BUCKETS 273

/* add an antry into a queue */
void tbpi_queue_push(char *name, void *data, FFL_ARGS);
#define tbp_queue_push(_q_,_d_) tbpi_queue_push((char *)_q_,(void *)_d_,FFL_DATA)

/* remove the oldest entry from the queue */
void *tbpi_queue_pop(char *name, FFL_ARGS);
#define tbp_queue_pop(_q_) tbpi_queue_pop((char *)_q_,FFL_DATA)

/* add an antry into a queue */
void tbpi_queue_push_direct(TBP_QUEUE_T *queue, void *data, FFL_ARGS);
#define tbp_queue_push_direct(_q_,_d_) tbpi_queue_push_direct((TBP_QUEUE_T *)_q_,(void *)_d_,FFL_DATA)

/* remove the oldest entry from the queue */
void *tbpi_queue_pop_direct(TBP_QUEUE_T *queue, FFL_ARGS);
#define tbp_queue_pop_direct(_q_) tbpi_queue_pop_direct((TBP_QUEUE_T *)_q_,FFL_DATA)

// view the next pointer without popping it...
void *tbpi_queue_view(char *name, FFL_ARGS);
#define tbp_queue_veiw(_name_) tbpi_queue_view(_name_,iFFL_DATA)

// view the next pointer without popping it...
void *tbpi_queue_view_direct(TBP_QUEUE_T *queue, FFL_ARGS);
#define tbp_queue_view_direct(_q_) tbpi_queue_view_direct(_q_,iFFL_DATA)

/* find a queue's pointer - "void" type to allow recasting */
void *tbpi_queue_ptr(char *name, FFL_ARGS);
#define tbp_queue_ptr(_name_) tbpi_queue_ptr(_name_,FFL_DATA)

/* get the current depth of a given queue */
int tbpi_queue_depth(char *name, FFL_ARGS);
#define tbp_queue_depth(_name_) tbpi_queue_depth(_name_,FFL_DATA)

/* get the current depth of a given queue */
int tbpi_queue_depth_direct(TBP_QUEUE_T *queue, FFL_ARGS);
#define tbp_queue_depth_direct(_q_) tbpi_queue_depth_direct(_q_,FFL_DATA)

/* create an empty queue */
TBP_QUEUE_T *tbpi_queue_create(char *name, FFL_ARGS);
#define tbp_queue_create(_name_) tbpi_queue_create(_name_,FFL_DATA)

/* destroy a given queue */
void tbpi_queue_destroy(char *name, FFL_ARGS);
#define tbp_queue_destroy(_name_) tbpi_queue_destroy(_name_,FFL_DATA)

/* destroy all managed queues */
void tbpi_queue_destroy_all(FFL_ARGS);
#define tbp_queue_destroy_all() tbpi_queue_destroy_all(FFL_DATA)

/* check to see if a given queue exists */
BOOL tbp_queue_exists(char *name);

#ifdef __cplusplus
}
#endif

#endif
