//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "util/CarbonPlatform.h"

#ifndef TBP_H
#define TBP_H
#include <stdio.h>
#include <stdarg.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#ifndef __USE_GNU
#define __USE_GNU
#endif
// JJH -- may need this back for dual mode #include <dlfcn.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#ifdef DMALLOC
#include <dmalloc.h>
#endif

#ifndef TBP_MAJOR
#define TBP_MAJOR 4
#endif
#ifndef TBP_MINOR
#define TBP_MINOR 5
#endif
#ifndef TBP_PATCH
#define TBP_PATCH ""
#endif
#ifndef TBP_BUILD_DATE
#define TBP_BUILD_DATE "2005"
#endif

#ifndef u_int8
typedef unsigned char u_int8;
typedef unsigned short u_int16;
typedef unsigned int u_int32;
typedef char s_int8;
typedef short s_int16;
typedef int s_int32;

#if pfLP64
typedef unsigned long u_int64;
typedef long s_int64;
#else
typedef unsigned long long u_int64;
typedef long long s_int64;
#endif

#define TRUE 1
#define FALSE 0
typedef int BOOL;
typedef u_int64 simt_t;
#endif

#if 0
#include "tbp_message.h"
#ifdef USE_PTHREADS
#include "tbp_pthreads.h"
#endif
#ifdef USE_CTHREADS
#include "tbp_cthreads.h"
#endif
#ifdef USE_QTHREADS
#include "tbp_qthreads.h"
#endif
#endif
#include "tbp_threads.h"
#include "tbp_events.h"
#include "tbp_config.h"
#include "tbp_hash.h"
#include "tbp_utils.h"
#include "tbp_malloc.h"
#include "tbp_pipe_utils.h"
#include "tbp_cmodel.h"
#include "tbp_cpf.h"
#include "tbp_sim.h"

#define tbp_error_internal(args...)  tbp_msg(iFFL_DATA, TBP_MSG_ERROR_GRP, MSG_LVL_FORCE, args)
#define tbp_panic_internal(args...)  tbp_msg(iFFL_DATA, TBP_MSG_PANIC_GRP, MSG_LVL_FORCE, args)

typedef struct {
  TBP_TRANS_T *trans;
  u_int32 trans_cnt;
  u_int32 *ctrans_list;
  u_int32 ctrans_cnt;
  u_int32 trans_attach_cnt;  // # of transactors attached
  u_int32 ctrans_attach_cnt; // # of ctransactors attached
  u_int32 current_thread;    // executing q/pthread index
  u_int32 prior_thread;    // prior q/pthread index
  u_int32 current_trans;     // transactor associated with the executing thread
  u_int32 current_ctrans;    // current ctrans index used used to advance cmodels
  u_int32 sim_thread_id;
  u_int32 main_thread_id;
  // Environment flags
  int stack_size;              // thread stack size
  int transport_mode;          // are we in a simulator or a standalone executable?
  BOOL setget_sleep;           // sleep 1 sim-cycle when doing set/get op's (tbp3x compatibility)
  BOOL pipe_manual;            // (temp?) manually connect pipes on the c-side in user-space
  BOOL test_complete_flag;     // track/block multiple exit requests
  BOOL finish_flag;            // allow one and only one call to simulation "finish" cmd
  BOOL cmodel_thread_relocate; // cmodel threads may persist between tests - move them carefully
  BOOL debug;
  BOOL detect_sim_errors;      // allow masking of hdl errors with regard to screen print and test failure
  BOOL detect_sim_warnings;    // allow masking of hdl warnings with regard to screen print and test failure
  BOOL suppress_warnings;       // allow global suppression of ($)tbp_warn output
  BOOL acc_error_enable;       // en/disable acc errors
  BOOL pli_screen_print;       // print sim-side messages to the screen (stdout) (default is FALSE)
  BOOL pli_log_print;          // print sim-side messages to the log (default is TRUE)
  BOOL pli_cside_screen_print; // print sim-side messages to the C-side screen (default is FALSE)
  BOOL pli_cside_log_print;    // print sim-side messages to the C-side log (default is FALSE)
  BOOL message_debug_print;    // append group,level,file,func,line,time to all messages
  BOOL flush_logfile;          // optionally flush the logfile with each message (default TRUE)
  BOOL cmodel_run;             // en/disable for cmodel thread execution
  BOOL hdl_run;                // en/disable for hdl execution (extend cmodel exec)
  BOOL interrupt_enable;       // provide a mechanism to disable interrupts
  BOOL status_error_enable;    // supress/report status and X/Z errors during transactions
  BOOL main_is_waiting;        // status flag for thread handling
  BOOL dll_debug;              // flag for use when running as a shared object under a debugger
  BOOL override_exit;          // override exit() calls witth tbp_shutdown
  BOOL no_hdl;                 // run without HDL (cmodel only mode)
  // run-time flag computed at init time to decide endianess
  BOOL big_endian;
  // flags used to exit cleanly
  BOOL exit_flag;              // used to indicate that "exit" or tbp_shutdown has been called
  int exit_code;               // store the exit code to be passed to the OS
  // some house-keeping variables
  char *test_lib_name;    // filename holding the dynamic test object
  char *ident_string;          // used for dual process
  char *test_complete_event;   // modifiable string to signal the end of a test
  int main_argc;               // argc extracted form the command intended for main
  char **main_argv;            // argv to match above
  int sim_argc;                // sim (raw) argc from the simulator invokation
  char **sim_argv;             // sim argv to match above
  char *simulator_name;        // the name of the parent simulator
  FILE *screen_output_stream;  // allow for redirection of screen output
  FILE *screen_input_stream;   // allow for redirection of screen input
  char *c_logfile_name;        // C output log name
  FILE *c_logfile;             // C output log file handle
  char *sim_logfile_name;      // simulation log name
  FILE *sim_logfile;           // simulation log file handle
  int log_level;               // log print threshold
  int screen_level;            // screen print threshold
  char **debug_groups;         // print groups
  int debug_grp_cnt;           // number of debug groups
  int c_error_cnt;
  int sim_error_cnt;
  int accumulated_errors;      // track errors for the entire run
  int accumulated_warnings;    // track warnings for the entire run
  int max_error_cnt;
  int max_warn_cnt;
  int warn_cnt;
  char path_seperator;         // delimiting character in hierarchical paths
  int simratio;
  u_int64 main_timeout;        // store a copy of main's timeout for dual process mode
  u_int32 default_buffer_size; // default per transactor buffer size
  u_int32 cmodel_time_unit;    // amount by which the ctime is incremented
  u_int64 current_c_time;      // cmodel time
  u_int64 (*cmodel_sim_time_func)(void); // user defined function for returning cmodel time
  void (*msg_user_error_func)(int);      // user defined function call with each error/warn/panic
  void (*wakeup_func)(int);              // function executed with each "wakeup" from a sleep
  int (*tbp_main_func)(int,char**);      // "tbp_main" called from inside shared object
} TBP_CONF_T;

/* function used to return a pointer to the global configuration struct */
#ifdef TBP_GET_CONF_AS_FUNCTION
TBP_CONF_T *tbp_get_conf(void);
#else /* !TBP_GET_CONF_AS_FUNCTION */
extern TBP_CONF_T g_tbp_conf;
#define tbp_get_conf() (&g_tbp_conf)
#endif /* !TBP_GET_CONF_AS_FUNCTION */

/* 
 * return a pointer to the thread corresponding to the given index 
 * keep this here to isolate the user from the choice of threading
 */
TBP_THREAD_T *tbp_get_thread(u_int32 thread_id);

/* 
 * functgion to return a pointer to the current thread 
 * see above regarding this prototype location
 */
TBP_THREAD_T *tbp_get_current_thread();

/*
 * Make available
 */
void tbp_record_module_presence_carbon (char*);
#endif
