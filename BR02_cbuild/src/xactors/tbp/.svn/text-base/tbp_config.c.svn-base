//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "tbp_internal.h"
#include "tbp_pli.h"
#include "fcntl.h"

#include "util/CarbonPlatform.h"

/* flag to limit message thrashing in non-debug mode */
static int g_tbp_config_debug = FALSE;
#define tbp_config_debug if(g_tbp_config_debug) tbp_printf

#ifndef tbp_get_conf
static
#else /* !tbp_get_conf */
TBP_CONF_T g_tbp_conf;
#endif /* !tbp_get_conf */
static BOOL g_tbp_config_initialized=FALSE;

#ifndef tbp_get_conf
TBP_CONF_T *tbp_get_conf(void) { return &g_tbp_conf; }
#endif /* !tbp_get_conf */

/*
 * function to en/disable debug printing within this module
 */
void tbp_set_config_debug_flag(int flag) { g_tbp_config_debug = flag; }

/*
 * function to set the ident string 
 */
char *tbpi_set_ident_string(FFL_ARGS,char *tbp_ident_string)
{
  TBP_CONF_T *tc=tbp_get_conf();
  (void) func; (void) line; (void) file;
  
  if(tc->ident_string!=NULL) {
    tbp_config_debug("tbp_set_ident_string: tbp_ident already set (%s), leaving unchanged\n",tc->ident_string);
  }
  else {
    /* allocate space for the ident string */
    if(tbp_ident_string!=NULL){
      tbp_config_debug("tbp: ident is %s.\n", tbp_ident_string);
      tc->ident_string = (char *)tbp_allocate_buffer(sizeof(char)*(strlen(tbp_ident_string)+1),
						     "TBP Ident");
      strcpy(tc->ident_string,tbp_ident_string);
    }
    else {
      tbp_config_debug("tbp: ident defaulting to empty string\n");
      tbp_free(tc->ident_string);
      tc->ident_string = NULL;
    }
  }

  return tc->ident_string;
}

/* function get the current ident string */
char *tbp_get_ident_string(void) { return tbp_get_conf()->ident_string; }

/*
 * function to set the global interrupt enable/disable flag
 */
void tbp_set_interrupt_enable(BOOL enable)
{
  TBP_CONF_T *tc=tbp_get_conf();
  tc->interrupt_enable = enable;
}

/* function to set a given transactor's interrupt enable */
void tbpi_set_my_trans_interrupt_enable(BOOL enable, FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();

  if(!tbp_is_user()){
    tbp_error_internal("This function must be called from within a transactor thread (not main)\n");
    return;
  }
  else {
    tc->trans[tbp_get_my_trans_id()].interrupt_enable = enable;
  }
}
/* set/get the status error_enable flag */
void tbp_set_status_error_enable(BOOL enable)
{
  TBP_CONF_T *tc=tbp_get_conf();
  tc->status_error_enable = enable;
}
BOOL tbp_get_status_error_enable(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  return tc->status_error_enable;
}

void tbp_set_c_debug(BOOL debug)
{
  TBP_CONF_T *tc=tbp_get_conf();
  tc->debug = debug;
}

/* dynamically determine endian configuration */
BOOL tbp_is_big_endian(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
#if pfLP64
  u_int64 word64;
  u_int8  ptr8[8];
#else
  u_int32 word32;
  u_int8 ptr8[4];
#endif

  /* if it is not yet initializeed */
  if(tc->big_endian==-1) {
#if pfLP64
    word64 = 0x0102030405060708UL;
    memcpy(ptr8,&word64,8);
#else
    word32 = 0x01020304;
    memcpy(ptr8,&word32,4);
#endif
    if(ptr8[0]==0x01) {
      tbp_config_debug("**** Configuring for BIG ENDIAN host ****\n");
      tc->big_endian = TRUE;
    }
    else {
      tbp_config_debug("**** Configuring for LITTLE ENDIAN host ****\n");
      tc->big_endian = FALSE;
    }
  }

  return tc->big_endian;
}
    
/*
 * function to setup a default buffer for each transactor
 * needed to emulate tbp3x queue_trans_struct behavior
 */
void tbpi_set_default_buffer_size(u_int32 size,FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();
  (void) func; (void) line; (void) file;

  tc->default_buffer_size = size;
}

/* 
 * function used to parse the argc/argv string to find any configuration 
 * relavent to our operation
 */
void tbp_parse_args(int caller)
{
  TBP_CONF_T *tc=tbp_get_conf();
  char default_c_logfile_name[] = TBP_DEFAULT_C_LOG_FILE;
  char default_sim_logfile_name[] = TBP_DEFAULT_SIM_LOG_FILE;
  char *c_logfile_name;
  char *sim_logfile_name;
  char *tbpargs;
// JJH -- remove for carbon   char *arg_ptr = NULL;
// JJH -- remove for carbon   int arg_len = 0;
// JJH -- remove for carbon   int i;
  FILE *tbp_cmd;
  char *tbp_cmd_filename;
  int tbpargs_len =0;
  char *tbp_ident_string;
  char *tbp_test_lib;
  char *stack_size;

  /* check for an ident string */
  if(tbpi_get_transport_mode()==TBP_TRANSPORT_MODE_DUAL) {
    tbp_ident_string = tbp_get_argv_key("tbp_ident");
    tbpi_set_ident_string(FFL_DATA,tbp_ident_string);
  }

  tc->dll_debug = tbp_get_argv_key_present("tbpdebug");

  /* if we are not inside asimulator - pass argv/argv through untouched */
  if(tbpi_get_transport_mode()!=TBP_TRANSPORT_MODE_SINGLE){
    tc->main_argc = tc->sim_argc; // could we shunt this off earlier?
    tc->main_argv = tc->sim_argv;
  }

  /* get the test args "tbpargs" from the command line */
  else if(NULL != (tbpargs = tbp_get_argv_key("tbpargs"))) {
    tbpargs_len = strlen(tbpargs) + 1;
  }
  /* or get them from a file */
  else if(NULL != (tbp_cmd_filename = tbp_get_argv_key("tbpcmd"))) {
    if(NULL==(tbp_cmd=fopen(tbp_cmd_filename,"r"))){
      tbp_panic("Failed to open command file (%s)\n", 
		tbp_cmd_filename != NULL ? tbp_cmd_filename : "bad filename");
    }
    else {
// JJH -- remove for carbon         while(EOF!=(c=fgetc(tbp_cmd))){
// JJH -- remove for carbon   	tbpargs_len += 1;
// JJH -- remove for carbon   	if(NULL==(tbpargs=(char *)tbp_realloc(tbpargs,sizeof(char)*tbpargs_len)))
// JJH -- remove for carbon   	  tbp_panic("Failed to allocate space required for the command file contents\n");
// JJH -- remove for carbon   	else
// JJH -- remove for carbon   	  tbpargs[tbpargs_len - 1] = c;
// JJH -- remove for carbon         }
    }
  }

  //  The following was removed to resolve a problem with glibc and ctype.h.  isprint and isspace are from ctype.h.
  //  This code will not do anything in Carbon because tbp_sim_specific_init sets argc to 0 and argv to NULL
// JJH -- remove for carbon   if(tbpargs_len !=0 ){
// JJH -- remove for carbon     /* allocate space for 1k test args */
// JJH -- remove for carbon     tc->main_argv = (char **)tbp_allocate_buffer(sizeof(char **)*1024,
// JJH -- remove for carbon 						 "Test Arguments (+tbpargs)");
// JJH -- remove for carbon 
// JJH -- remove for carbon     /* walk through the array looking for whitespace delimited strings */
// JJH -- remove for carbon     tc->main_argc = 0;
// JJH -- remove for carbon     
// JJH -- remove for carbon     for(i=0;i<tbpargs_len;i++){
// JJH -- remove for carbon       c = (int) tbpargs[i];
// JJH -- remove for carbon       if(isprint(c) && !isspace(c)){
// JJH -- remove for carbon 	/* check for a new string (point to the head of this string */
// JJH -- remove for carbon 	arg_ptr = (arg_len++==0) ? &tbpargs[i] : arg_ptr;
// JJH -- remove for carbon       }
// JJH -- remove for carbon       else { // either whitespace or non-printing, delimit the string
// JJH -- remove for carbon 	if(arg_len > 0 && arg_ptr != NULL){  
// JJH -- remove for carbon 	  tc->main_argv[tc->main_argc] = tbp_allocate_buffer((sizeof(char **) * (arg_len+1)),
// JJH -- remove for carbon 							     "Test Arguments");
// JJH -- remove for carbon 
// JJH -- remove for carbon 	  strncpy(tc->main_argv[tc->main_argc],arg_ptr,arg_len);
// JJH -- remove for carbon 
// JJH -- remove for carbon 	  tc->main_argv[tc->main_argc++][arg_len] = '\0'; // terminate this string
// JJH -- remove for carbon 	  arg_ptr = NULL;
// JJH -- remove for carbon 	  arg_len = 0;
// JJH -- remove for carbon 	}
// JJH -- remove for carbon       } 
// JJH -- remove for carbon     }
// JJH -- remove for carbon   }

  /* check for debug flags */
  tbp_set_util_debug_flag(tbp_get_argv_key_present("tbp_util_debug"));
  tbp_set_sim_debug_flag(tbp_get_argv_key_present("tbp_sim_debug"));
  tbp_set_pipe_debug_flag(tbp_get_argv_key_present("tbp_pipe_debug"));
  tbp_set_thread_debug_flag(tbp_get_argv_key_present("tbp_thread_debug"));
  tbp_set_config_debug_flag(tbp_get_argv_key_present("tbp_config_debug"));
  tbp_set_legacy_debug_flag(tbp_get_argv_key_present("tbp_legacy_debug"));
  tbp_set_evt_debug_flag(tbp_get_argv_key_present("tbp_evt_debug"));

  /* suppress warnings? */
  tbp_set_suppress_warnings(tbp_get_argv_key_present("tbp_suppress_warnings"));

  /* check for malloc debug (simsde/single only) */
  tbp_set_malloc_debug(tbp_get_argv_key_present("tbp_malloc_debug"));
  
  /* check for manual pipe mode */
  tc->pipe_manual = tbp_get_argv_key_present("tbp_ipc_manual");
  
  /* check for a testlib */
  if(NULL != (tbp_test_lib = tbp_get_argv_key("tbptestlib"))){
    tc->test_lib_name = (char *)tbp_allocate_buffer(sizeof(char)*(strlen(tbp_test_lib)+1),
						    "+tbptestlib");
    strcpy(tc->test_lib_name,tbp_test_lib);
  }

  /* check for stack size directives */
  if(NULL != (stack_size = tbp_get_argv_key("tbp_stack_size"))){
    tbp_printf("tbp: stack size changed from %d bytes to %s bytes\n",
	       tbp_get_stack_size(),stack_size);
    tbp_set_stack_size(strtoul(stack_size,NULL,10));
  }

  /* find the log names - default if none are found */
  c_logfile_name = tbpi_get_transport_mode()!=TBP_TRANSPORT_MODE_SINGLE
                   ? tbp_get_argv_key("l") : tbp_get_argv_key("tbplog"); 
  c_logfile_name = c_logfile_name==NULL ? default_c_logfile_name : c_logfile_name;

  sim_logfile_name = tbp_get_argv_key("tbpsimlog");
  sim_logfile_name = sim_logfile_name==NULL ? default_sim_logfile_name : sim_logfile_name;

  /* 
   * single process: 2 logs
   *  - option Clog==Simlog
   */
  if(tbpi_get_transport_mode()==TBP_TRANSPORT_MODE_SINGLE) {
    /* allocate space for the logfile names */
    tc->c_logfile_name = (char *)tbp_allocate_buffer(sizeof(char)*(strlen(c_logfile_name)+1),
						     "C-side Logfile Name");
    strcpy(tc->c_logfile_name,c_logfile_name);

    /* open the C_side logfile */
    if(NULL==(tc->c_logfile=fopen(c_logfile_name,"w+")))
      tbp_panic("Failed to open the C-side Logfile (%s)\n",c_logfile_name);
    
    /* check to see if the smae arg was used for the sim and C-sides */
    if(strcmp(tc->c_logfile_name,sim_logfile_name)==0) {
      tc->sim_logfile = tc->c_logfile;
      tc->sim_logfile_name = tc->c_logfile_name;
    }
    else {
      /* we only open the simside log while inside a simulator! */
      if(tbpi_get_transport_mode()==TBP_TRANSPORT_MODE_SINGLE) {
	/* if not, then allocatee space and copy in the sim-side name */
	tc->sim_logfile_name = (char *)tbp_allocate_buffer(sizeof(char)*(strlen(sim_logfile_name)+1),
							   "Sim-side Logfile Name");
	
	/* copy the name into our config struct for later retrieval */
	strcpy(tc->sim_logfile_name,sim_logfile_name);
	
	/* open the SIM_side logfile */
	if(NULL==(tc->sim_logfile=fopen(sim_logfile_name,"w+")))
	  tbp_panic("Failed to open the Sim-side Logfile (%s)\n",sim_logfile_name);
      }
    }
  }
  else {
    
    /* 
     * dual process: 1 log
     * - cside = cside
     * - simside = simside
     */
     if(caller==TBP_CSIDE) {
       tc->sim_logfile = NULL;
       /* allocate space for the logfile names */
       tc->c_logfile_name = (char *)tbp_allocate_buffer(sizeof(char)*(strlen(c_logfile_name)+1),
							 "C-side Logfile Name");
       strcpy(tc->c_logfile_name,c_logfile_name);
       
       /* open the C_side logfile */
       if(NULL==(tc->c_logfile=fopen(c_logfile_name,"w+"))) {
	 tbp_panic("Failed to open the C-side Logfile (%s)\n",c_logfile_name);
       }
       
       /* no sim-side log */
       tc->sim_logfile = NULL;
       tc->sim_logfile_name = NULL;
     }
     else if(caller==TBP_SIMSIDE) {  
       tc->c_logfile = NULL;
       /* if not, then allocatee space and copy in the sim-side name */
       tc->sim_logfile_name = (char *)tbp_allocate_buffer(sizeof(char)*(strlen(sim_logfile_name)+1),
							  "Sim-side Logfile Name");
       
       /* copy the name into our config struct for later retrieval */
       strcpy(tc->sim_logfile_name,sim_logfile_name);
       
       /* open the SIM_side logfile */
       if(NULL==(tc->sim_logfile=fopen(sim_logfile_name,"w+"))){
	 tbp_panic("Failed to open the Sim-side Logfile (%s)\n",sim_logfile_name);
       }
       
       /* no c-side log */
       tc->c_logfile = NULL;
       tc->c_logfile_name = NULL;
     }
  }

  tbp_config_debug("opening log files (c-side = %s sim-side = %s)\n",
		   tc->c_logfile_name,sim_logfile_name);
	
}

/*
 * function to set the override exit flag
 */
void tbp_set_override_exit(BOOL enable)
{
  TBP_CONF_T *tc=tbp_get_conf();
  tc->override_exit = enable;
}
  
/* initialize the global configuration structure */
void tbp_init_config(int caller)
{
  TBP_CONF_T *tc=tbp_get_conf();

  if(g_tbp_config_initialized==FALSE) { 
    g_tbp_config_initialized = TRUE;
    tc->setget_sleep = FALSE;
    tc->pipe_manual = FALSE;
    tc->ident_string = NULL;
    tc->override_exit = TRUE;
    tc->dll_debug = FALSE;
    tc->test_complete_flag = FALSE;
    tbpi_set_finish_flag(FALSE);
    tc->exit_flag = FALSE;
    tc->exit_code = 0;
    tc->debug = TRUE;  
    tc->hdl_run = TRUE;
    tc->no_hdl = FALSE;
    tc->interrupt_enable = TRUE; 
    tc->status_error_enable = FALSE; // FIXME ? tbp 3.x default is TRUE;
    tc->main_is_waiting = FALSE;
    tc->wakeup_func = NULL;
    tc->big_endian = -1;
    tc->main_timeout = 0;

    /* call module initialize functions */
    tbpi_set_acc_error_enable(TRUE);
    tbp_message_module_init();
    tbp_thread_module_init();
    tbp_cmodel_module_init();
    tbp_evt_module_init();
    
    // these will be part of tbp_util_module_init() when it shows up...
    /* create a default buffer for each transactor - used to model tbp3x's behavior */
    tbp_set_default_buffer_size(65536);
    /* set the big endian flag (brute force test, then store the result) */
    tbp_is_big_endian();
    
    /* perform any per simulator configuration here */
    tbp_simulator_specific_init(); 
 
    /* parse the argc/argv values from the command line */
    tbp_parse_args(caller);
  }
}

/* function to retrieve the path seperator */
char tbp_get_path_seperator(void)
{ 
  TBP_CONF_T *tc=tbp_get_conf();
  return tc->path_seperator;
}

#ifndef DUAL_PROC
/*
 * function to en/disable debug printing within this module
 */
void tbp_set_pipe_debug_flag(int flag) { (void)flag; return; }
#endif
