//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "tbp_internal.h"
#include "tbp_events.h"

/* prevent message thrashing in normal mode */
static int g_tbp_evt_debug = FALSE;
#define tbp_evt_debug if(g_tbp_evt_debug) printf

/* event structures */
static TBP_HASH_T *g_event_hash_table;    // hahs table for events
static TBP_EVT_T *g_event;                // table of events
static TBP_EVT_T *g_event_tail;
static u_int32   g_event_cnt = 0;         // total event count
static u_int32   g_event_counter = 0;     // counter used to id the events - accumulates between delete_all calls
static u_int32   g_event_array_size = 0;  // track the total size of the event array buffer for acceration

/* flag used for simplified test wait/halt/complete */
static char *g_test_complete_event = NULL;

/*
 * function to en/disable debug printing within this module
 */
void tbp_set_evt_debug_flag(int flag) { g_tbp_evt_debug = flag; }

/*
 * function to initialize the event module
 */
void tbp_evt_module_init(void)
{
  g_event_array_size =0;
  g_event = NULL;  
  g_event_tail = NULL;
  g_event_cnt =0;
  /* default test completion signal */
  tbp_set_test_complete_event(TBP_EVT_TEST_COMPLETE);
}

/* function used to asign the name of the test completion event */
void tbp_set_test_complete_event(const char *event)
{
  int event_length = sizeof(char)*(strlen(event)+1);

  /* free the current event */
  tbp_free(g_test_complete_event);
  
  /* allocate space for the new event */
  g_test_complete_event = (char *)tbp_allocate_buffer(event_length,"test completion string");
  /* copy it in */
  memcpy(g_test_complete_event,event,event_length);
}

/* function to get the test complete event */
char *tbp_get_test_complete_event(void) { return g_test_complete_event; }

/* 
 * this function modifies an event (refered to as "signaling")
 * this signaling causes the thread thread which first began to wait
 * for this event to wakeup.
 *
 * This function is non-blocking with regard to the simulation/cmodel
 */
void tbpi_evt_signal(const char *event, FFL_ARGS)
{
  TBP_EVT_T *evt = tbpi_evt_get(event,iFFL_DATA);

  /* "set" increments the state while wait decriments */
  evt->state += 1;
  
#if pfLP64
  tbp_evt_debug("tbp_evt_signal: signal %s state is now %d at time %lu\n",
		evt->name,evt->state,tbp_get_sim_time());
#else
  tbp_evt_debug("tbp_evt_signal: signal %s state is now %d at time %llu\n",
		evt->name,evt->state,tbp_get_sim_time());
#endif
  /* call the wakeup mechanism to signal this event */
  tbpi_evt_wake_one(evt,iFFL_DATA);
}
 
/*
 * Same basic idea as setting a signal, but allows the user
 * to decide which value is assigned and therfore allow others
 * to wait for a particular value rather than simple assertion
 */
void tbpi_evt_signalstate(const char *event, int state, FFL_ARGS)
{
  TBP_EVT_T *evt= tbpi_evt_get(event,iFFL_DATA);
  
  /* assign an arbitrary state to this event */
  evt->state = state;

  tbp_evt_debug("tbp_evt_signalstate: signal %s state is now %d\n",
		evt->name,evt->state);
  /* call the wakeup mechanism to signal this event */
  tbpi_evt_wake_any(evt,state,iFFL_DATA);
}

/* 
 * wait or the assertion of a particular signal
 * timeout:
 * 1. N > 0 => timeout at current time + N
 * 2. N < 0 => timeout at current time + |N| without error
 * 3. N = 0 => never timeout
 */
int tbpi_evt_wait(const char *event, s_int64 timeout, FFL_ARGS)
{
  TBP_EVT_T *evt = tbpi_evt_get(event,iFFL_DATA);
  int thread_index = tbp_get_my_thread_id();
  int wait_type = TBP_EVT_WAIT_SIGNAL;  /* wait for the occurance of a signal rather than a state */

  /* decriment the state to reflect the fact that we are waiting */
  evt->state -= 1;

  return tbpi_evt_sleep(evt,thread_index,wait_type,0,timeout,iFFL_DATA);
}

/* 
 * same as above, but wait for a particular state 
 */
int tbpi_evt_waitstate(const char *event, int state, s_int64 timeout, FFL_ARGS)
{
  TBP_EVT_T *evt = tbpi_evt_get(event,iFFL_DATA);
  int thread_index = tbp_get_my_thread_id();
  int wait_type = TBP_EVT_WAIT_STATE;  /* wait for the occurance of state */
  
  return tbpi_evt_sleep(evt,thread_index,wait_type,state,timeout,iFFL_DATA);
}

/* 
 * non-blocking check of the state of a given signal
 */
int tbpi_evt_checkstate(const char *event, FFL_ARGS)
{
  TBP_EVT_T *evt = tbpi_evt_get(event,iFFL_DATA);
  if(evt)
    return evt->state;
  else
    return -1;
}

/* 
 * function to wait for either a test to complete, 
 * timeout error or halt
 */
void tbpi_evt_waittest(const char *event, s_int64 timeout, FFL_ARGS)
{
  TBP_EVT_T *evt;
  s_int64 negative_timeout = timeout < 0 ? timeout : 0 - timeout;
  u_int64 positive_timeout = timeout < 0 ? 0 - timeout : timeout;
  u_int64 test_timeout = positive_timeout + tbp_get_sim_time();

#if pfLP64
  tbp_evt_debug("thread %d: is now in evt_waittest (timeout = %lu)\n",
		tbp_get_my_thread_id(),test_timeout);
#else
  tbp_evt_debug("thread %d: is now in evt_waittest (timeout = %lld)\n",
		tbp_get_my_thread_id(),test_timeout);
#endif
  /* save off the string on whcih main is waiting */
  tbp_set_test_complete_event(event);

  /* wait for test_complete */
  tbpi_evt_wait(tbp_get_test_complete_event(),negative_timeout,iFFL_DATA);

  /* check for a timeout - we may be awake either due to halt or test_complete */
  if(test_timeout < tbp_get_sim_time()){
    if(timeout < 0) 
      tbp_info("\n**** Test timeout detected at time %lld ****\n\n",
	       tbp_get_sim_time());
    if(timeout > 0) 
      tbp_error_internal("**** Test timeout detected at time %lld ****\n",
			 tbp_get_sim_time());
  }

  /* clear out the test complete event in case we woke up via a halt */
  evt = tbp_evt_get(tbp_get_test_complete_event());
  tbpi_evt_delete(evt,iFFL_DATA); 
}

/*
 * Do everything to wait for an event *EXCEPT* wait for sim to go by
 */
void tbpi_evt_set_waittest(const char *event, s_int64 timeout, FFL_ARGS) {
  TBP_EVT_T   *evt;  // the requested event
  TBP_EVT_T   *evth; // extra event for halts

  int thread_index     = tbp_get_my_thread_id();
  TBP_THREAD_T *thread = tbp_get_thread(thread_index);

  // the simulation time at which we should stop
  u_int64 test_timeout = (timeout>0) ?  (tbp_get_sim_time() + timeout) :
                         (timeout<0) ?  (tbp_get_sim_time() - timeout) :  0;

#if pfLP64
  tbp_evt_debug("thread %d: is now in set_waittest (timeout = %lu)\n",
		thread_index, test_timeout);
#else
  tbp_evt_debug("thread %d: is now in set_waittest (timeout = %lld)\n",
		thread_index, test_timeout);
#endif
  /* save off the string on which main is waiting */
  tbp_set_test_complete_event(event);

  /* wait for test_complete */
  /*  WTF:  negative timeout, get the event from where I just put it */
      /* --> tbpi_evt_wait(tbp_get_test_complete_event(),negative_timeout,iFFL_DATA); */

  /* decrement the state to reflect the fact that we are waiting */
  evt         = tbpi_evt_get(event,iFFL_DATA);
  evt->state -= 1;


  /*  int wait_type = TBP_EVT_WAIT_SIGNAL; */ /* wait for the occurance of a signal rather than a state */
  /*   --> return tbpi_evt_sleep(evt,thread_index,wait_type,0,timeout,iFFL_DATA);  */

  /* add this thread to the wait list for this event */
  tbpi_evt_add_waiter(evt,thread_index,iFFL_DATA);

  /* register the fact that this thread is waiting for this event */
  thread->wait_evt       = evt;
  thread->wait_evt_state = 0; // wait for this value to satisfy the event
  thread->timeout        = test_timeout;

  /* determine the type from the given timeout value */
  if(timeout==0)
    thread->wait_evt_type = TBP_EVT_WAIT_NO_TIMEOUT;
  else if(timeout < 0)
    thread->wait_evt_type = TBP_EVT_WAIT_TIMEOUT_NOERR; 
  else 
    thread->wait_evt_type = TBP_EVT_WAIT_TIMEOUT_ERR;
  
#if pfLP64
  tbp_evt_debug("tbp_evt_set_watittest: thread %d is now going to wait for an event (%s - id %d timeout = %lu state = %d)\n",
		thread_index,evt->name,evt->id,thread->timeout,evt->state);
#else
  tbp_evt_debug("tbp_evt_set_watittest: thread %d is now going to wait for an event (%s - id %d timeout = %lld state = %d)\n",
		thread_index,evt->name,evt->id,thread->timeout,evt->state);
#endif
  
  /* decide whether or not we are actually going to sleep */
  /* sleep if the state of this event indicates that it has been set fewer times than polled */
  if(evt->state < 0) {
    /* -->      tbpi_sim_sleep(thread->timeout,iFFL_DATA); */

    if(tbp_is_user())
      tbp_panic_internal("Cannot call set_waittest from an svc control function\n");
    /* --> tbpi_main_sleep(timeout,iFFL_DATA); */
    /* 
     * use the halt event as means of sleeping such that we can wakeup due 
     * to an error in the sim-thread (or anywhere else for that matter)
     */
    tbpi_set_main_is_waiting(TRUE);
    tbpi_set_main_timeout(test_timeout);
    evth = tbp_evt_get(TBP_EVT_HALT);

    tbpi_evt_add_waiter(evth, 0, iFFL_DATA);

    /* wakeup the simulation thread and wait until we are called again */
    /* --> tbp_wait_for_sim(); */
    return;
  }
}
/*
 * display all the current events for debug purposes 
 */
void tbp_list_all_events(void)
{
  TBP_EVT_T *evt;

  tbp_printf("\n**** tbp_list_all_events: listing all events ****\n");
  evt = g_event;
  while(evt) {
    tbp_printf("\tsignal %d: state %d wait_cnt %d name %s\n",
		  evt->id, evt->state,
		  evt->wait_cnt,evt->name);
    evt = evt->nxt;
  } 
}

/* 
 * find the index of a given event (primitive search function)
 * returns index if found -1 if not
 */
TBP_EVT_T *tbp_evt_table_search(const char *event)
{
  /* hash the given string looking for an event */
  if(g_event_hash_table) // is there a hash table?
    return (TBP_EVT_T *)tbp_hash(g_event_hash_table,event);
  else
    return NULL;
}

/*
 * add a given event to the event hash table - will create the hash table
 * if it does not already exist
 */
void tbpi_evt_add_to_hash(const char *name, TBP_EVT_T *evt)
{
  /* if there is no hash table - create one */
  if(g_event_hash_table==NULL)
    g_event_hash_table = tbp_hash_create();
  /* add this name to the hash table */
  tbp_hash_add(g_event_hash_table,name,(void *)evt);
}

/* 
 * return an index to the event matching the given signal,
 * if none is found, add it to the list - this list grows
 * up, so that older items remain intact as they reference
 * malloc'd buffers (for event name storage)
 */
TBP_EVT_T *tbpi_evt_get(const char *event, FFL_ARGS)
{
  TBP_EVT_T *evt;

  /* if there is a matching event, return a pointer to this event */
  if(NULL!=(evt = tbp_evt_table_search(event))) {
    tbp_evt_debug("tbp_evt_get: found an existing event %s\n",evt->name);
    return evt;
  }
  
  /* 
   * if we are here, no match was found, add this event to the table, 
   * reallocate the array only if we need more space 
   */
  if(NULL==(evt = (TBP_EVT_T *)tbp_calloc(1,sizeof(TBP_EVT_T)))) {
    tbp_panic_internal("tbp_evt_get: Failed to allocate space require to add this event (%s)\n",
		       event);
    return NULL;
  }

  if(NULL==(evt->name = (char *)tbp_calloc((strlen(event)+1),sizeof(char)))){
    tbp_panic_internal("tbp_evt_get: Failed to allocate space require to add this event name (%s)\n",
	      event);
    return NULL;
  }

  /* add this event to the linked list */
  evt->prev = g_event_tail; // assign the previous pointer

  /* add this event to the hash table */
  tbpi_evt_add_to_hash(event,evt);

  if(g_event_tail) { // not the first entry
    g_event_tail->nxt = evt;
    g_event_tail = evt;
  }
  else { // the first entry
    g_event_tail = g_event = evt;
  }
  
  /* add the event name/handle and init the other variables */
  strcpy(evt->name,event);
  evt->id = g_event_counter;
  /* event state, either yes/no or 0-N  wait exist for 1/0 evt_wait / or 1-N evt_waitstate */
  evt->state = 0;     // intitial value - will be decrimented for waits
  evt->wait_cnt = 0;  // higher level code should decide when to change this #
  evt->wait_list = NULL;
  
  /* increment the global count to reflect our new event */
  g_event_cnt +=1;
  /* increment the global counter to help Id this event */
  g_event_counter +=1;
  return evt;
}
 
/*
 * Function called by pli layer to see if a given transactor is waiting
 * - A thread is either not waiting for an event, waiting for an event
 * - or timed out while waiting for an event.  This code is concerned 
 * - only with waiting/not and timeout.  Satisfaction of the event is 
 * - handled elsewhere.
 */
int tbp_evt_check_thread_wait(u_int32 thread_index)
{
  TBP_THREAD_T *thread = tbp_get_thread(thread_index);
  u_int64 cur_time = tbp_get_sim_time(); 
  int wait_type;
  u_int64 timeout;

  /* make sure the thread is valid */
  if(thread==NULL) {
    tbp_evt_debug("tbp_evt_check_thread_wait: NULL thread for index: %d\n", thread_index);
    return 0;
  }
  else {
    wait_type = thread->wait_evt_type;
    timeout = thread->timeout;

    /* 
     * check for:
     * 1. no event pending or event has been detected
     * 2. timeout during an event in which this is an error condition
     * 3. no timeout - event not satisfied 
     */
    if ((wait_type==TBP_EVT_WAIT_NONE) || (wait_type==TBP_EVT_WAIT_DONE)) {
      //TEMP tbp_evt_debug("tbp_evt_check_thread_wait: thread %d reports event type none, or done (%d)\n",thread_index,wait_type);
      tbp_evt_debug("tbp_evt_check_thread_wait: NO WAIT-- wait type: %d\n", wait_type);
      return TRUE;
    }
    else if(wait_type!=TBP_EVT_WAIT_NO_TIMEOUT && (timeout < cur_time)) {
#if pfLP64
      tbp_evt_debug("tbp_evt_check_thread_wait: NO WAIT-- timeout: %lu curr: %lu\n",
		    timeout, cur_time);
#else
      tbp_evt_debug("tbp_evt_check_thread_wait: NO WAIT-- timeout: %lld curr: %lld\n",
		    timeout, cur_time);
#endif
      tbp_msg("internal", "", 0, TBP_MSG_ERROR_GRP, MSG_LVL_FORCE, "Timeout detected in carbonXCheckWait() timeout: %lld curr: %lld\n",
	      timeout, cur_time);

      return TRUE;
    }
    else {
      tbp_evt_debug("tbp_evt_check_thread_wait: Waiting on thread: %d\n", thread_index);
      return 0; // thread is still waiting
    }
  }
}

int tbp_evt_check_my_thread_wait(void) {
  return tbp_evt_check_thread_wait(tbp_get_my_thread_id());
}


/*
 * function to add a new waiter to an event's wait list
 */
void tbpi_evt_add_waiter(TBP_EVT_T *evt, int thread_index, FFL_ARGS)
{
  /* increase the wait counter */
  evt->wait_cnt += 1;
  
  /* add this thread to the wait list - grow this wait list by one */
  if(NULL==(evt->wait_list=(int *)tbp_realloc(evt->wait_list, (sizeof(int)*(evt->wait_cnt - 1)), (sizeof(int)*evt->wait_cnt))))
    tbp_panic_internal("Failed to allocate space required to add thread %d to the event (%s) wait_list\n",thread_index,evt->name);
  else
    evt->wait_list[evt->wait_cnt-1] = thread_index;
}

/* 
 * function to remove a waiter from an event's wait list 
 */
void tbpi_evt_rm_waiter(TBP_EVT_T *evt, int thread_index, FFL_ARGS)
{
  int i,index=-1;
  (void) func; (void) line; (void) file;

  /* find this index in the wait_list */
  for(i=0;i<evt->wait_cnt;i++) {
    if(evt->wait_list[i]==thread_index) {
      index = i; 
      break;
    }
  }
  
  /* make sure we have a valid index */
  if(index==-1)
    return;

  /* now strip out this entry */
  evt->wait_cnt -= 1; // decriment the count to reflect this one's removal
  for(i=index;i<evt->wait_cnt;i++){
    evt->wait_list[i] = evt->wait_list[i+1];
  }
  
  /* null out the highest entry */
  evt->wait_list[evt->wait_cnt] = -1;
}

/*
 * Sleep primative function called by waitstate and wait which 
 * 1. creates the event if it does not exist - through the call to evt_get
 * 2. assigns the calling thread's wait evet to this new or existing event
 * 3. if the event has not been satisfied already - sleep this thread 
 *
 * Inputs:
 * evt_index    - index to the event for which we are waitingg
 * thread_index - the thread which is about to wait
 * wait_type    - wait for the assertion of an event, or the event to reach a state
 * timeout      - the desired time which, if passed, will result in this wait returning
 * 
 * Outputs:
 * status       - did the event occur within the timeout? 1 = yes, 0 = no
 */
int tbpi_evt_sleep(TBP_EVT_T *evt, int thread_index, int wait_type, 
		   int wait_evt_state, s_int64 timeout, FFL_ARGS)
{
  TBP_THREAD_T *thread = tbp_get_thread(thread_index);
  u_int64 timeout_value = (u_int64) (timeout < 0 ? 0 - timeout : timeout);
  int ret_val = 0;

  /* add this thread to the wait list for this event */
  tbpi_evt_add_waiter(evt,thread_index,iFFL_DATA);

  /* register the fact that this thread is waiting for this event */
  thread->wait_evt = evt;
  thread->wait_evt_state = wait_evt_state; // wait for this value to satisfy the event
  thread->timeout = timeout_value != 0 ? (tbp_get_sim_time() + timeout_value) : 0;

  /* determine the type from the given timeout value */
  if(timeout==0)
    thread->wait_evt_type = TBP_EVT_WAIT_NO_TIMEOUT;
  else if(timeout < 0)
    thread->wait_evt_type = TBP_EVT_WAIT_TIMEOUT_NOERR; 
  else 
    thread->wait_evt_type = TBP_EVT_WAIT_TIMEOUT_ERR;
  
#if pfLP64
  tbp_evt_debug("tbp_evt_sleep: thread %d is now going to wait for an event (%s - id %d timeout = %lu state = %d)\n",
		thread_index,evt->name,evt->id,thread->timeout,evt->state);
#else
  tbp_evt_debug("tbp_evt_sleep: thread %d is now going to wait for an event (%s - id %d timeout = %lld state = %d)\n",
		thread_index,evt->name,evt->id,thread->timeout,evt->state);
#endif
  
  /* decide whether or not we are actually going to sleep */
  if(wait_type==TBP_EVT_WAIT_STATE) {
    /* unless our desired state has already occured - sleep */
    if(evt->state!=thread->wait_evt_state)
      tbpi_sim_sleep(thread->timeout,iFFL_DATA);
  }
  else { 
    /* sleep if the state of this event indicates that it has been set few times than polled */
    if(evt->state < 0)
      tbpi_sim_sleep(thread->timeout,iFFL_DATA);
  }

  /* now update the event itself - it may have moved within the event array  */
  thread = tbp_get_thread(thread_index);
  evt  = thread->wait_evt;

  /* remove this thread from this event's wait list */
  tbpi_evt_rm_waiter(evt,thread_index,iFFL_DATA);
 
  /* check to see if the event or a timeout occured */
  if(tbp_get_sim_time() > thread->timeout) {
    /* return 0 if any timeout occured */
    ret_val = thread->wait_evt_type == TBP_EVT_WAIT_NO_TIMEOUT ? 1 : 0;

    /* report only if a !=0 time out was requested and error only if it was > 0 */
    if(thread->wait_evt_type == TBP_EVT_WAIT_TIMEOUT_ERR)
      tbp_error_internal("Timeout detected while waiting for an event (%s)\n",evt->name);
    else if(thread->wait_evt_type == TBP_EVT_WAIT_TIMEOUT_NOERR)
#if pfLP64
      tbp_evt_debug("tbp_evt_sleep: thread %d has timed-out while waiting for an event (%s) at time %lu\n",
		    thread_index,evt->name,tbp_get_sim_time());
#else
      tbp_evt_debug("tbp_evt_sleep: thread %d has timed-out while waiting for an event (%s) at time %lld\n",
		    thread_index,evt->name,tbp_get_sim_time());
#endif
    //else - nothing to do
  }
  else {
    ret_val = 1;
#if pfLP64
    tbp_evt_debug("tbp_evt_sleep: thread %d has detected an event (%s) at time %lu\n",
		  thread_index,evt->name,tbp_get_sim_time());
#else
    tbp_evt_debug("tbp_evt_sleep: thread %d has detected an event (%s) at time %lld\n",
		  thread_index,evt->name,tbp_get_sim_time());
#endif
  }

  /* if there are no other waiters for this event, clear it out */
  if(evt->wait_cnt < 1)
    tbpi_evt_delete(evt,iFFL_DATA);
  
  /* we are awake and done with this event - clear the event from this thread*/ 
  thread->wait_evt = NULL;
  thread->wait_evt_state = 0;
  thread->wait_evt_type = TBP_EVT_WAIT_NONE;
  thread->timeout = 0LL;

  return ret_val;
}

/*
 * specialized sleep function for main - adds "halt" event even if
 * the main thread is waiting on another event (as it is with waittest)
 */
void tbpi_main_sleep(u_int64 timeout, FFL_ARGS)
{
  TBP_EVT_T *evt;
  u_int64 test_timeout = (timeout != 0) ? (timeout+tbp_get_sim_time()) : 0;

#if pfLP64
  tbp_evt_debug("thread %d: is now in main_sleep (timeout = %lu)\n",
		tbp_get_my_thread_id(), timeout);
#else
  tbp_evt_debug("thread %d: is now in main_sleep (timeout = %lld)\n",
		tbp_get_my_thread_id(), timeout);
#endif
  /* 
   * use the halt event as means of sleeping such that we can wakeup due 
   * to an error in the sim-thread (or anywhere else for that matter)
   */
  tbpi_set_main_is_waiting(TRUE);
  tbpi_set_main_timeout(test_timeout);
  evt = tbp_evt_get(TBP_EVT_HALT);

  tbpi_evt_add_waiter(evt,0,iFFL_DATA);

  /* wakeup the simulation thread and wait until we are called again */
  tbp_wait_for_sim();

  /* we are awake */
  tbpi_set_main_is_waiting(FALSE);
  tbpi_set_main_timeout(0);

  /* check for a halt */
  if(tbp_evt_checkstate(TBP_EVT_HALT)==1) {
    tbp_printf("\n**** Test Halt Detected at Time %lld ****\n\n",tbp_get_sim_time());

    /* clear out any potential incoming IPC data as we are aborting this test run! */
    tbp_ipc_flush();

    /* clear out the event */
    evt = tbp_evt_get(TBP_EVT_HALT);
    tbpi_evt_delete(evt,iFFL_DATA);

    /* call the user error function to allow them to jump around as needed */
    tbp_user_error_function(TBP_MSG_ERROR_LEVEL);
  }

  /* clear out the event */
  evt = tbp_evt_get(TBP_EVT_HALT);
  tbpi_evt_delete(evt,iFFL_DATA);
}

/*
 * loop through the waiters for this event and signal wakeup as needed
 * wakeup only the first (oldest waiter)
 */
void tbpi_evt_wake_one(TBP_EVT_T *evt, FFL_ARGS)
{
  TBP_THREAD_T *thread;
  int i;

  /* loop through the waiters for this event and signal wakeup as needed */
  for(i=0;i<evt->wait_cnt;i++){
    /* check to make sure the thread index is valid */
    if(evt->wait_list[i] != -1) {
      /* get a pointer to the thread that is waiting */
      thread = tbp_get_thread(evt->wait_list[i]);

      /* modify the state of this event to reflect satisfaction of the wait */	
      thread->wait_evt_type = TBP_EVT_WAIT_DONE;
      
      /* special case if the thread we are waking is main (primarily for dual process) */
      if(evt->wait_list[i]==tbpi_main_thread_id()) {
	tbpi_set_main_is_waiting(FALSE);
      }
      else {
	/* send the programmable wakeup to the pli (allow more complex wakeup if needed) */
	tbpi_wakeup(thread->trans_id, iFFL_DATA);
      }
      /* wake_one releases only the first waiter */
      break; // wake_one releases only the oldest (first) matching entry
    }
  }    
}

/*
 * loop through the waiters for this event and signal wakeup as needed
 * wakeup all waiters
 */
void tbpi_evt_wake_any(TBP_EVT_T *evt,int state, FFL_ARGS)
{
  TBP_THREAD_T *thread;
  int i;

  /* loop through the waiters for this event and signal wakeup as needed */
  for(i=0;i<evt->wait_cnt;i++){
    /* check to make sure the thread index is valid */
    if(evt->wait_list[i] != -1) {
      /* get a pointer to the thread we are going to wake */
      thread = tbp_get_thread(evt->wait_list[i]);
      
      /* check to see that the state matches the given value, if so wake it up */
      if(thread->wait_evt_state == state) {
	/* modify the state of this event to reflect satisfaction of the wait */	
	thread->wait_evt_type = TBP_EVT_WAIT_DONE;
	/* special case if the thread we are waking is main (primarily for dual process) */
	if(evt->wait_list[i]==tbpi_main_thread_id()) {
	  tbpi_set_main_is_waiting(FALSE); 
	}
	else {
	  /* send the programmable wakeup to the pli (allow more complex wakeup if needed) */
	  tbpi_wakeup(thread->trans_id, iFFL_DATA);
	}
      }
    }
  }
}    

/*
 * wakeup all waiters unconditionaly
 */
void tbpi_evt_wakeup_all(FFL_ARGS)
{
  TBP_EVT_T *evt;
  TBP_THREAD_T *thread;
  int j;

  evt = g_event;
  while(evt) {
    /* loop through the waiters for this event and signal wakeup as needed */
    for(j=0;j<evt->wait_cnt;j++){
      /* check to make sure the thread index is valid */
      if(evt->wait_list[j] != -1) {
	thread = tbp_get_thread(evt->wait_list[j]);
	/* modify the state of this event to reflect satisfaction of the wait */	
	thread->wait_evt_type = TBP_EVT_WAIT_DONE;
	/* send the programmable wakeup to the pli (allow more complex wakeup if needed) */
	tbpi_wakeup(thread->trans_id, iFFL_DATA);
      }
    }
  }
}

/* function used to signal a sleeping transactor to wakeup */
void tbpi_wakeup(int trans_id, FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_OP_T *operation = tbpi_trans_operation(trans_id);

  (void) func; (void) line; (void) file;
  if(trans_id >= 0) {  // only wake up attached transactor threads
    tbp_evt_debug("Waking up trans %d (%s)\n",trans_id,tbp_get_trans_name(trans_id)); 

    //    if(operation->opcode == TBP_SLEEP) {
      /* first remove the sleep opcode */
      operation->opcode = TBP_NOP;
      /* now call the programmable wakeup function to do anything else that needs to be done */
      if(tc->wakeup_func!=NULL) (tc->wakeup_func)(trans_id);
    //    }
  }
}

/* function used to assign the "wakeup function" */
void tbpi_set_wakeup_func(void (*wakeup_func)(int), FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();
  (void) func; (void) line; (void) file;
  tc->wakeup_func = wakeup_func;
}

/*
 * this function is called to remove an event from the event table
 * this function will free the memory used by this event and then
 * collapse the event table to include only the outstanding events.
 * no checking is done by this function with regard to whether or
 * not the event is ready to be deleted - that is assumed to be done
 * at a higher level
 */
void tbpi_evt_delete(TBP_EVT_T *evt, FFL_ARGS)
{
  TBP_EVT_T *prev,*nxt;

  if(evt==NULL) {
    tbp_list_all_events();
    tbp_error_internal("tbp_evt_delete: event is invalid, there is no corresponding event to delete\n");
    return;
  }
  else {
    /* remove the hash entry for this event */
    tbp_hash_remove(g_event_hash_table,evt->name);
    
    /* free the pointers assoicated with this event */
    tbp_free(evt->name);
    tbp_free(evt->wait_list);

    /* remove our entry from the linked list (collapse) */
    prev = (TBP_EVT_T *)evt->prev;
    if(prev)
      prev->nxt = evt->nxt;
    else // we must have been the head
      g_event = evt->nxt;
    
    nxt = (TBP_EVT_T *)evt->nxt;
    if(nxt)
      nxt->prev = prev;
    else // we must have been the tail...
      g_event_tail = evt->prev;

    /* free this event */
    tbp_free(evt);
    
    g_event_cnt-=1;
  }
}

/*
 * delete all events - used for cleanup between tests
 * care should be taken to ensure that all threads are
 * killed prior to performing this operation as it could
 * cause a segfault or deadlock if an event is removed while
 * threads are active 
 */
void tbpi_evt_delete_all(FFL_ARGS)
{
  (void) func; (void) line; (void) file;
  TBP_THREAD_T *thread;
  TBP_EVT_T *evt,*evt_ptr;
  u_int32 i;

  evt = g_event;
  while(evt) {
    /* free the name and wait list */
    tbp_free(evt->name);
    tbp_free(evt->wait_list);
    evt_ptr = evt;
    evt = evt->nxt;
    /* free the event itself */
    tbp_free(evt_ptr);
  }
  
  /* NULL out the global head/tail pointers */
  g_event = NULL;
  g_event_tail = NULL;
  
  /* zero out the counters which track this array */
  g_event_cnt = 0;
  g_event_array_size =0;

  tbp_hash_destroy(g_event_hash_table);
  g_event_hash_table = NULL;

  /* 
   * try to help the user from hanging things by deleting all of the 
   * timeout counts from any active threads
   */
  for(i=0;i<tbp_get_thread_count();i++){
    thread = tbp_get_thread(i);
    thread->timeout = 0LL;
    thread->wait_evt = NULL;
    thread->wait_evt_state = 0;
    thread->wait_evt_type = TBP_EVT_WAIT_NONE;
  }
}
