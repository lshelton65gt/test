//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_MALLOC_H
#define TBP_MALLOC_H

#include "tbp_internal.h"

#ifdef __cplusplus
extern "C" {
#endif

/* functions to set/get the tbp_malloc_debug flag */
void tbp_set_malloc_debug(int debug);
int tbp_get_malloc_debug(void);

void *tbpi_malloc(int size, FFL_ARGS);
#define tbp_malloc(_size_) tbpi_malloc(_size_,FFL_DATA)

void *tbpi_calloc(int num, int size, FFL_ARGS);
#define tbp_calloc(_num_,_size_) tbpi_calloc(_num_,_size_,FFL_DATA)

void *tbpi_realloc(void *ptr, int orig_size, int new_size, FFL_ARGS);
#define tbp_realloc(_ptr_,_osize_,_nsize_) tbpi_realloc(_ptr_,_osize_,_nsize_,FFL_DATA)

void tbpi_free(void **ptr, FFL_ARGS);
#define tbp_free(_ptr_) { void* _tbp_free_ptr_ = _ptr_; tbpi_free(&_tbp_free_ptr_, FFL_DATA); _ptr_ = NULL; }

#ifdef __cplusplus
}
#endif
#endif
