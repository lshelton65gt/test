//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_CMODEL_H
#define TBP_CMODEL_H

#include "tbp_internal.h"

#ifdef __cplusplus
extern "C" {
#endif

/* function to intialize cmodel module variables */
void tbp_cmodel_module_init(void);

/* functions to start/stop hdl execution */
void tbp_hdl_run();
void tbp_hdl_stop();

/* functions to start/stop cmodel execution */
void tbp_cmodel_run();
void tbp_cmodel_stop();

/* function to change the cmodel time unit */
void tbp_cmodel_set_time_unit(u_int32 time_unit);

/* reassignable functtion to get the cmodel sim-time */
u_int64 tbp_cmodel_get_sim_time(void);

/* function used to assign a user-space function to control sim-time */
void tbp_cmodel_set_sim_time_func(u_int64 (*user_sim_time_func)());

/* pos/negedge function for cmodels/ctrans */
void tbp_posedge(u_int32 *value);
void tbp_negedge(u_int32 *value);

/* Internal function to test cmodel state to see if execution is needed */
void tbp_cmodel_check_execute(void);

/* function used to execute all cmodel threads once in a single loop */
void tbp_cmodel_execute(void);

/*
 * create a thread which will hold the ctrans function - mimmics
 * an HDL transactor template
 */
int tbpi_cmodel_thread_create(u_int32 trans_id, void (*func_ptr), void *usr_ptr, FFL_ARGS);

/* this function kills all cmodel threads */
void tbpi_delete_cmodel_threads(FFL_ARGS);
#define tbp_delete_cmodel_threads() tbpi_delete_cmodel_threads(FFL_DATA)

/*
 * function used to rebuild the ctrans list after a thread deletion
 * also reinitializes the ctrans count at the same time
 */
void tbpi_cmodel_build_ctrans_list(FFL_ARGS);

/* wakeup cmodel threads and inform them of thier new id due to movement above */
void tbpi_cmodel_thread_relocate(FFL_ARGS);

/*
 * function used by a ctrans to get/put an operation from/to the C-side
 * this function is intended to be called after put/get activity
 * which applies to this operation
 *
 * If attached it will wakeup the attached thread, if not it will
 * return to the sim/top thread which will move to the next ctrans
 */
void tbp_operation(u_int32 trans_id, 
		   u_int32 *opcode,u_int64 *address,
		   u_int32 *size,u_int32 *status,
		   u_int32 *interrupt,u_int32 *reset,
		   u_int64 starttime,u_int64 endtime);
/* 
 * function used to determine if calling thread OR transactor
 * transactor attached to the calling thread is a cmodel_transactor
 */
BOOL tbp_is_ctrans(void);

/* function used to fetch the current ctrans_cnt */
int tbp_get_ctrans_count(void);

/* macros around put/get trans data for various transaction widths */
#define tbp_put_data8(_id_,_index_,_data_,_size_) \
        tbpi_put_trans_data(_id_,_index_,(void *)_data_,NULL,_size_*TBP_WIDTH_8,TBP_WIDTH_8,FFL_DATA)
#define tbp_put_data16(_id_,_index_,_data_,_size_) \
        tbpi_put_trans_data(_id_,_index_,(void *)_data_,NULL,_size_*TBP_WIDTH_16,TBP_WIDTH_16,FFL_DATA)
#define tbp_put_data32(_id_,_index_,_data_,_size_) \
        tbpi_put_trans_data(_id_,_index_,(void *)_data_,NULL,_size_*TBP_WIDTH_32,TBP_WIDTH_32,FFL_DATA)
#define tbp_put_data64(_id_,_index_,_data_,_size_) \
        tbpi_put_trans_data(_id_,_index_,(void *)_data_,NULL,_size_*TBP_WIDTH_64,TBP_WIDTH_64,FFL_DATA)

#define tbp_get_data8(_id_,_index_,_data_,_size_) \
        tbpi_get_trans_data(_id_,_index_,(void *)_data_,NULL,_size_*TBP_WIDTH_8,TBP_WIDTH_8,FFL_DATA)
#define tbp_get_data16(_id_,_index_,_data_,_size_) \
        tbpi_get_trans_data(_id_,_index_,(void *)_data_,NULL,_size_*TBP_WIDTH_16,TBP_WIDTH_16,FFL_DATA)
#define tbp_get_data32(_id_,_index_,_data_,_size_) \
        tbpi_get_trans_data(_id_,_index_,(void *)_data_,NULL,_size_*TBP_WIDTH_32,TBP_WIDTH_32,FFL_DATA)
#define tbp_get_data64(_id_,_index_,_data_,_size_) \
        tbpi_get_trans_data(_id_,_index_,(void *)_data_,NULL,_size_*TBP_WIDTH_64,TBP_WIDTH_64,FFL_DATA)
/* 
 * ********************************
 * LEGACY FUNCTIONS
 * ********************************
 */
int tbp_is_ctrans_path(char *path);

#define tbp_cputdata(_index_,_data_) \
        tbpi_put_trans_data(tbp_get_my_trans_id(),_index_,(void *)_data_,NULL,1*TBP_WIDTH_32,TBP_WIDTH_32,FFL_DATA)

#define tbp_cgetdata(_index_,_data_) \
        tbpi_get_trans_data(tbp_get_my_trans_id(),_index_,(void *)_data_,NULL,1*TBP_WIDTH_32,TBP_WIDTH_32,FFL_DATA)

#define tbp_cregister(args...)      tbpi_register(FFL_DATA,TBP_TRANS_TYPE_CMODEL,args)
#define tbp_ctransport              tbp_transport 
#define tbp_ctrans_transport        tbp_transport 
#define tbp_cmodel_stop_hdl         tbp_hdl_stop
#define tbp_cmodel_resume_hdl       tbp_hdl_run
#define tbp_ctrans_max_data_size(args...)  tbp_unsupported("tbp_ctrans_max_data_size",FFL_DATA)

#define tbp_ctrans_attach_buffer    tbp_attach_buffer
#define tbp_cmodel_get_simtime_t    tbp_cmodel_get_sim_time
#define tbp_cmodel_set_simtime_func tbp_cmodel_set_sim_time_func
#define tbp_set_cmodeltime_t(args...)  tbp_unsupported("tbp_set_cmodeltime_t",FFL_DATA)
#define tbp_cposedge                tbp_posedge
#define tbp_cnegedge                tbp_negedge
#define tbp_cmodel_thread_kill_all  tbp_delete_cmodel_threads

#ifdef __cplusplus
}
#endif
#endif
