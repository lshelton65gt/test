//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*
 * Description: This file holds the functions required
 * to wakeup (register) and talk to the the simulator
 *
 * INFO: any $call is required to update the current
 * sim time stored in the tc to minimize pli calls
 * this is done by calling tbp_store_pli_sim_time();
 */

// NOTE - memory put/get needs 64/32 endian fix implimented for registers
// NOTE - can we make registration entirely implicit with the detection
// NOTE - of $tbp calls?

//!!! #include "acc_user.h"
//!!! #include "veriuser.h"
#include "util/CarbonPlatform.h"
#include "tbp_internal.h"
#include "tbp_pli.h"
#include "tbp_utils.h"
#include "tbp_pipe_utils.h"
#include "tbp_config.h"
#ifdef TBP_MTI
#include "mti.h"
#include "acc_vhdl.h"
#endif
#ifdef TBP_CVER
#include "cv_veriuser.h"
#endif

/* flag to limit message thrashing in non-debug mode */
static BOOL g_tbp_pli_debug = TRUE;
#define tbp_pli_debugp if(g_tbp_pli_debug) tbp_printf

/* global return storage for dual process mode that reports the error on the C-side */
static int g_pli_setget_return=0;
static char * g_carbonmodulename = NULL;
/*
 * function to en/disable debug printing within this module
 */
void tbp_set_sim_debug_flag(int flag) { g_tbp_pli_debug = flag; }

/*
 *    ***V E S T I G A L ***
 *
 * function to retrieve the current sim-time from the PLI
 * This should be called only once each time the C-side wakes
 * up from the pli both to speed this up and to prevent problems
 * with some simulators when tf_getlongtime is accessed 
 * multiple times
 */
void tbp_pli_store_sim_time() {

  /*
   *  This version for use under Carbon does NOTHING
   *  the funcitonality has been moved up to the cds interface layer
   *  just before the pli routines are called
   */
}

/* generic function called by utils during an async wakeup */
void tbp_store_sim_time(void)
{

}

/*
 * function associated with $tbp_register in the hdl
 */
int tbp_cds_register(char *full_name, u_int32 *id)
{
  int trans_id=0;

  // Save off the module name
  g_carbonmodulename = full_name;
  
  if(NULL != full_name) {
    trans_id = tbpi_register(TBP_PLI_FFL_DATA,TBP_TRANS_TYPE_HDL,full_name);
    tbp_pli_debugp("register: register module : %s id = %d\n",full_name,trans_id);
   
    if(tbp_check_all_registered()) {
      tbp_pli_debugp("register: all transactors are now registered\n\n");
      
      /* initialize just about everything else! */
      tbp_init(TBP_SIMSIDE);
      tbp_pli_debugp("\ntransport: tbp init is done\n\n");
    }
  }
  else {
    tbp_pli_panic_internal("transport: register could not determine the name of the calling module\n");
  }

  //  tf_putp(1,trans_id);         /* return result */
  *id = trans_id;
  return 0;
}

/* 
 * function which reqeusts an operation from the hdl side 
 */
int tbp_cds_transport(u_int32 *id,
		      u_int32 *put_op,  u_int64 *put_ad,   u_int32 *put_sz,   u_int32 *put_st,
		      u_int32 *put_int, u_int32 *put_rst, u_int64 *put_strt, u_int64 *put_end,
		      u_int32 *get_op,  u_int64 *get_ad,  u_int32 *get_sz,   u_int32 *get_st,
		      u_int32 *get_int, u_int32 *get_rst)
{ 
  TBP_OP_T operation;
  //BOOL reset_arg_present;
  int current_trans;

  tbp_pli_debugp("transport: id:%d\n", *id);

  /* get the identity of the calling transactor */
  current_trans = *id;

  /* check to make sure this is a valid/registered transactor */
  tbp_check_trans_id(current_trans); // FIXME - would it be more readable to test a return? it panics to who cares...
  tbpi_set_current_trans(current_trans);
  
  /* now fill in the fields of the "current" operation (existing sleep overrides) */
  operation.opcode  = *put_op;  	//tf_getp(2);
  //  lo = tf_getlongp(&hi,3);
  operation.address = *put_ad;  	//(((u_int64)hi) << 32) | ((u_int64) lo & 0x00000000ffffffffLL);
  operation.size    = *put_sz; 		//tf_getp(4);
  operation.status  = *put_st;  	//tf_getp(5);
  operation.interrupt = *put_int;	//tf_getp(6);

  /* check for the optional reset argument - if not it is encoded in "interrupt" */
  //reset_arg_present = tf_nump()==9 ? TRUE : FALSE;
  operation.reset   = *put_rst; 	//reset_arg_present ? 
					// tf_getp(7) : ((operation.interrupt&0x2)!=0 ? TRUE : FALSE);

  //  lo = tf_getlongp(&hi,reset_arg_present ? 8 : 7);
  operation.starttime = *put_strt;	// (((u_int64)hi) << 32) | ((u_int64) lo & 0x00000000ffffffffLL);

  // lo = tf_getlongp(&hi,reset_arg_present ? 9 : 8);
  operation.endtime   = *put_end;	// (((u_int64)hi) << 32) | ((u_int64) lo & 0x00000000ffffffffLL);
  
  /* process this operation - push back resultant operation if indicated */
  if(tbpi_sim_transport(current_trans,&operation)==TRUE){
    *get_op  = operation.opcode;	// tf_putp(2,operation.opcode);
    // lo = (u_int32) (operation.address & 0x00000000ffffffffLL);
    // hi = (u_int32) ((operation.address >> 32) & 0x00000000ffffffffLL);
    *get_ad  = operation.address;	//tf_putlongp(3,lo,hi);
    *get_sz  = operation.size;		// tf_putp(4,operation.size);
    *get_st  = operation.status;	// tf_putp(5,operation.status);
    *get_int = operation.interrupt;	// tf_putp(6,operation.interrupt);
    //    if(reset_arg_present==TRUE)
    *get_rst  = operation.reset;	// tf_putp(7,operation.reset);
  } else {
    *get_op   = operation.opcode;	// tf_putp(2,operation.opcode);
  }

  /* return to the simulation*/
  return 0;
}

/*
 * function to send data from the from hdl to C
 */
int tbp_cds_put_data64(u_int32 *id, u_int32 *index, u_int64 *data)
{
  TBP_OP_T *operation;
  int buf_size;  // buffer required to suport this operation
  int dindex;
  int tid;	// my transator id

  /* typed pointers to fill the "void" typed read/readx trans data buffers */
  u_int64 *read_data64,*read_datax64;
  
  /* get the identity of the calling transactor */
  tid = *id;
  /* check that this is a valid identity */
  tbp_check_trans_id(tid);

  /* assign local pointers for subsequent access */
  tbpi_set_current_trans(tid);  // dumbassed way to pass a parameter?!
  operation = tbpi_trans_operation(tid);

  dindex = *index;		//tf_getp(2);

  /* make sure we have enough memory to hold the incoming data */
  buf_size = ((dindex+1)*8)+8;  // worst case requried buffer
  tbpi_set_buffer_size(tid,buf_size,TBP_PLI_FFL_DATA);
  
  read_data64 = (u_int64 *)operation->read_data;
  read_datax64 = (u_int64 *)operation->read_datax;
  
  tbp_pli_debugp("$tbp_put_data64[%d] = 0x%16.16llx (width %d)\n",
                 dindex,read_data64[dindex],operation->width);

  if ((operation->width == TBP_WIDTH_64) || (operation->width == TBP_WIDTH_64P)) {
    read_data64[dindex] = *data;
  }
  if ((operation->width == TBP_WIDTH_32)) {
    read_data64[dindex] = *data;
  }
  if ((operation->width == TBP_WIDTH_16)) {
    read_data64[dindex] = *data;
  }
  if ((operation->width == TBP_WIDTH_8)) {
    read_data64[dindex] = *data;
  }
  return 0;
}

/*
 * function to send data from the from hdl to C
 */
int tbp_cds_put_data32(u_int32 *id, u_int32 *index, u_int32 *data)
{
  TBP_OP_T *operation;
  int buf_size;  // buffer required to suport this operation
  int dindex;
  int tid;	// my transator id

  /* typed pointers to fill the "void" typed read/readx trans data buffers */
  u_int32 *read_data32,*read_datax32;
  
  /* get the identity of the calling transactor */
  tid = *id;
  /* check that this is a valid identity */
  tbp_check_trans_id(tid);

  /* assign local pointers for subsequent access */
  tbpi_set_current_trans(tid);  // dumbassed way to pass a parameter?!
  operation = tbpi_trans_operation(tid);

  dindex = *index;		//tf_getp(2);

  if (operation->width == TBP_WIDTH_64P) {
    dindex ^= 1;
  }

  /* now fetch the data */
  
  /* make sure we have enough memory to hold the incoming data */
  buf_size = ((dindex+1)*4)+4;  // worst case requried buffer
  tbpi_set_buffer_size(tid,buf_size,TBP_PLI_FFL_DATA);
  
  read_data32 = (u_int32 *)operation->read_data;
  read_datax32 = (u_int32 *)operation->read_datax;
  
  tbp_pli_debugp("$tbp_put_data32[%d] = 0x%16.16llx (width %d)\n",
		 dindex,read_data32[dindex],operation->width);
  
  if ((operation->width == TBP_WIDTH_32) || (operation->width == TBP_WIDTH_64) || (operation->width == TBP_WIDTH_64P)) {
    read_data32[dindex] = *data;
    read_datax32[dindex] = 0LL;
  }

  if ((operation->width == TBP_WIDTH_16)) {
    read_data32[dindex] = *data;
    read_datax32[dindex] = 0LL;
  }
  
  if ((operation->width == TBP_WIDTH_8)) {
    read_data32[dindex] = *data;
    read_datax32[dindex] = 0LL;
  }
  
  return 0;
}

/*
 * function to send data from the from C to HDL
 */
int tbp_cds_get_data64(u_int32 *id, u_int32 *index, u_int64 *data)
{
  TBP_OP_T *operation;
  int dindex;
  u_int32 buf_size; // buffer required to support this operation
  int tid;
  u_int64 *write_data64,*write_datax64;
  
  /* get the identity of the calling transactor */
  tid = *id;	// tf_getp(1);

  /* check that this is a valid identity */
  tbp_check_trans_id(tid);

  /* assign local pointers for subsequent access */
  tbpi_set_current_trans(tid);
  operation = tbpi_trans_operation(tid);

  dindex = *index;	// tf_getp(2);
      
  /* double check some of the values we have collected */
  if(dindex < 0) {
    tbp_pli_error_internal("$tbp_get_data start address is invalid %d\n",dindex);
    return 0;
  }
  
  /* check to see if we have enough buffer for this operation */
  buf_size = (dindex+1)*8;
  if(buf_size > (operation->buffer_size+16)){
    tbp_pli_error_internal("$tbp_get_data: Not enough data to support this write:\n"
                           "\t%d bytes available %d bytes requested (index = %d)\n",
                           operation->size,buf_size,dindex);
  }
  
  write_data64 = (u_int64 *) operation->write_data;
  write_datax64 = (u_int64 *) operation->write_datax;      
      
  tbp_pli_debugp("$tbp_get_data64[%d] = 0x%16.16llx (width %d)\n",
                 dindex,write_data64[dindex],operation->width);

  if ((operation->width == TBP_WIDTH_64) || (operation->width == TBP_WIDTH_64P)) {
    *data = write_data64[dindex];
  }
  if ((operation->width == TBP_WIDTH_32)) {
    *data = write_data64[dindex];
  }
  if ((operation->width == TBP_WIDTH_16)) {
    *data = write_data64[dindex];
  }
  if ((operation->width == TBP_WIDTH_8)) {
    *data = write_data64[dindex];
  }
  return 0;
}

/*
 * function to send data from the from C to HDL
 */
int tbp_cds_get_data32(u_int32 *id, u_int32 *index, u_int32 *data)
{
  TBP_OP_T *operation;
  int dindex;
  u_int32 buf_size; // buffer required to support this operation
  int tid;
  u_int32 *write_data32,*write_datax32;
  
  /* get the identity of the calling transactor */
  tid = *id;	// tf_getp(1);

  /* check that this is a valid identity */
  tbp_check_trans_id(tid);

  /* assign local pointers for subsequent access */
  tbpi_set_current_trans(tid);
  operation = tbpi_trans_operation(tid);

  dindex = *index;	// tf_getp(2);

  /* double check some of the values we have collected */
  if(dindex < 0) {
    tbp_pli_error_internal("$tbp_get_data start address is invalid %d\n",dindex);
    return 0;
  }
  
  /* check to see if we have enough buffer for this operation */
  buf_size = (dindex+1)*8;
  if(buf_size > (operation->buffer_size+16)){
    tbp_pli_error_internal("$tbp_get_data: Not enough data to support this write:\n"
			   "\t%d bytes available %d bytes requested (index = %d)\n",
			   operation->size,buf_size,dindex);
  }
  
  write_data32 = (u_int32 *) operation->write_data;
  write_datax32 = (u_int32 *) operation->write_datax;      
  
  tbp_pli_debugp("$tbp_get_data32[%d] = 0x%x (width %d)\n",
                 dindex,write_data32[dindex],operation->width);
  
  if (operation->width == TBP_WIDTH_64P) {
    dindex ^= 1;
  }
  if ((operation->width == TBP_WIDTH_32) || (operation->width == TBP_WIDTH_64) || (operation->width == TBP_WIDTH_64P)) {
    *data = write_data32[dindex];
  }

  if ((operation->width == TBP_WIDTH_16)) {
    *data = write_data32[dindex];
  }
  
  if ((operation->width == TBP_WIDTH_8)) {
    *data = write_data32[dindex];
  }
  
  return 0;
}

/*  pli version of finish - which calls the C-side shutdown... */
int tbp_pli_finish(void) { exit(0); return 0; }

/* placeholder functions for tbp3x */
int tbp_pli_request(void) { tbp_info("$tbp_request not supported\n"); return 0; }

/* placeholder functions for tbp3x */
int tbp_pli_response(void) { tbp_info("$tbp_response not supported\n"); return 0; }

/*
 * hold a given signal during critical operations
 */
int tbp_pli_sighold(void)
{
  int signal = 0;	//tf_getp(1);
  tbp_sighold(signal);
  return 0;
}

/* 
 * function to request that the simulation finish - must be called
 * from within the sim-thread!
 */
void tbp_sim_finish(void)
{
  if(tbpi_get_finish_flag()==FALSE){
    tbpi_set_finish_flag(TRUE);
    tbp_printf("tbp_sim_finish has been called - shutting down...\n");    

    //!!!  do something to make me stop!!!
  }
}

/* function to stop the simulator (to get to a cli> prompt inside the simulator) */
void tbp_sim_stop(void)
{
  if(tbp_is_ctrans())
    return;

    //!!!  do something to make me stop!!!

  tbp_wait_for_sim();
}

/* helper functions to search the argument arrays for keys */
BOOL tbp_sim_get_argv_key_present(char *key) {
  (void) key;
  return FALSE;
 }

char *tbp_sim_get_argv_key(char *key) {
  (void) key;
  return NULL;
}

/* 
 * return the current module name - copy it into the
 * provided pointer if it is non-null
 */
char *tbp_get_current_module_name(char *name)
{
  static char empty_string[] = {""};
#ifdef TBP_MTI 
  char *module_name;
  char *instance;
  int region_kind = mti_GetRegionKind(mti_GetCallingRegion());
  if(region_kind!=accModule && region_kind!=accFunction && region_kind!=accArchitecture) // VHDL
    module_name = mti_GetRegionFullName(mti_GetCallingRegion());
  else { // Verilog
    if(NULL==(instance = tf_getinstance()))
      module_name = mti_GetRegionFullName(mti_GetCallingRegion());
    else
      module_name = tf_imipname(instance);
  }
#else
#ifdef TBP_CDS
  char *module_name = g_carbonmodulename;
  if(module_name == NULL) module_name = empty_string;
#else
  char *module_name = tf_mipname();  
#endif
#endif

  if(NULL != module_name) {
    if(name!=NULL)
      strcpy(name,module_name);
    return module_name;
  }
  else {
    if(name!=NULL)
      strcpy(name,"none");
    return "none";
  }
}

/* 
 * functions used by messaging to display the location in the hdl of the
 * calling instance - first one gets the calling filename
 */
char *tbp_get_current_file(void)
{
  return  "unknown" ;    
}

/* get the calling/current line for print statements */
int tbp_get_current_line(void)
{
    return 0;
}

/* get the calling/current module/task name for print statements */
char *tbp_get_current_module(void)
{
  return tbp_get_current_module_name(NULL);
}

/* function used to fetch a handle using a full path */
//!!!handle tbpi_get_handle_by_name(char *path) {
  /********************************
//!!!  handle check_handle;

//!!!  check_handle = acc_handle_by_name(path,NULL);
//!!!  if(NULL==check_handle){
//!!!    if(path[0] == tbpi_get_path_seperator())
//!!!      check_handle = acc_handle_by_name((path+1),NULL);
//!!!  }
 *********************************/
//!!!  return NULL;	//!!! check_handle;
//!!!}

/*
 * function to return the most recent result of a set/get operation - used for dual process
 */
int tbp_sim_setget_return(void) { return g_pli_setget_return; }
void tbp_sim_setget_flag(int flag) {g_pli_setget_return = flag; }

/*
 * pli set_value - broken out for pli/fli combination
 */
int tbpi_pli_set_value(char *path, void *value, int type, int force, char *scope, FFL_ARGS)
{
  (void) func; (void) line; (void) file;
  (void) path; (void) value; (void) type; (void) force; (void) scope;
  return 0;
}

void *tbpi_pli_get_value(char *path, int type, char *scope, FFL_ARGS)
{
  (void) func; (void) line; (void) file;
  (void) path; (void) type; (void) scope;
return NULL;
}

/* 
 * set a simulation variable with a given argument - type is:
 * integer (int number)
 * binary  (char/string)
 * hex     (char/string)
 * octal   (char/string)
 * force is encoded: 
 *      0 - normal  = accNoDelay
 *      1 - force   = accForceFlag
 *      2 - release = accReleaseFlag
 */
// FIXME - need error/type checking for various types! 
int tbpi_sim_set_value(char *path, void *value, int type, int force, char *scope, FFL_ARGS)
{
#ifdef TBP_MTI
  int region_kind;
#endif /* TBP_MTI */

  /* clear the global flag */
  tbp_sim_setget_flag(TBP_SETGET_NOERR);
  
  /* placeholder for multiple set/get */
  if(scope!=NULL) {
    tbp_setget_error(TBP_SETGET_BAD_SCOPE,"scope is not yet supported with set_value\n");
    return 1;
  }

  /* check force flag */
  if(force < 0 || force > 2){
    tbp_setget_error(TBP_SETGET_BAD_FORCE,"invalid force argument (%d)\n",force);
    return 1;
  }

  if(path==NULL) {
    tbp_setget_error(TBP_SETGET_NULL_PATH,"path is null\n");
    return 1;
  }

#ifdef TBP_MTI
  region_kind = mti_GetRegionKind(mti_GetCallingRegion());
  if(region_kind!=accModule && region_kind!=accFunction)  // VHDL
    return tbpi_fli_set_value(path,value,type,force,scope,iFFL_DATA);
  else
    return tbpi_pli_set_value(path,value,type,force,scope,iFFL_DATA);
#else
  return tbpi_pli_set_value(path,value,type,force,scope,iFFL_DATA);
#endif
}

void *tbpi_sim_get_value(char *path, int type, char *scope, FFL_ARGS)
{
#ifdef TBP_MTI
  int region_kind;
#endif /* TBP_MTI */

  /* clear the global flag */
  tbp_sim_setget_flag(TBP_SETGET_NOERR);

  if(type < 0 || type > 6) {
    tbp_setget_error(TBP_SETGET_BAD_TYPE,"invalid type argument (%d)\n",type);
    return NULL;
  }
  
  if(path==NULL) {
    tbp_setget_error(TBP_SETGET_NULL_PATH,"path is null\n");
    return NULL;
  }

#ifdef TBP_MTI
  region_kind = mti_GetRegionKind(mti_GetCallingRegion());
  if(region_kind!=accModule && region_kind!=accFunction)  // VHDL
    return tbpi_fli_get_value(path,type,scope,iFFL_DATA);
  else
    return tbpi_pli_get_value(path,type,scope,iFFL_DATA);
#else
  return tbpi_pli_get_value(path,type,scope,iFFL_DATA);
#endif
}

/*
 * function to "reset" all registered transactors - "set/get" are called 2x
 * to account for simulator's support for leading "." or "/"
 */
void tbpi_sim_reset_transactors(FFL_ARGS)
{
    
  (void) func; (void) line; (void) file;
  //!!! this looks tricky to do!!!
  
}

/* parse and format the printf string */

//!!!  dont support printfs
//!!!  static int tbp_pli_make_string(char *local_string) {

/************************************************************
 * tbp_pli_print - provide one common function and prototypes
 * for PLI's benifit for each of the variants...
 *  provides functions identical to those seen on the C-side
************************************************************/
#define MAX_STRING_SIZE 1024

static int tbpi_cds_pli_message(char *group, int level, char *msg, FFL_ARGS)
{
  
  static char local_string[MAX_STRING_SIZE];
  static char empty_string[2] = {""};
  const char *file_ptr, *func_ptr;
  int i, len;
  
  // Check String Length
  len = strlen(msg);
  if(len > MAX_STRING_SIZE) len = MAX_STRING_SIZE;
  
  // The string is reversed when it comes from verilog, so we need to reverse it
  if(level == -1) {
    for(i = 0; i < len; i++)
      local_string[i] = (msg)[len-i-1];
    local_string[len] = '\0';
  }
  else {
    // Using memcpy instead of strcpy to make sure that we don't go out of bounds
    memcpy(local_string, msg, len);
  }

  /* "error" messages and printf need a newline to match $display behavior */

  // Check if any of the FFL_ARGS are null, if so exchange with a empty string instead
  if(file == NULL) file_ptr = empty_string;
  else file_ptr = file;
  if(func == NULL) func_ptr = empty_string;
  else func_ptr = func;
  tbp_msg(file_ptr,func_ptr,line,group,level,(level==-1 ? "%s\n" : "%s"),local_string);
  
  return 0;
}

/* functions called by the pli */
int tbp_cds_pli_printf(char *msg) { return tbpi_cds_pli_message(TBP_MSG_PLI_PRINTF, -1,msg,TBP_PLI_FFL_DATA); }
int tbp_cds_pli_info(char *msg)   { return tbpi_cds_pli_message(TBP_MSG_PLI_INFO,    0,msg,TBP_PLI_FFL_DATA); }
int tbp_cds_pli_debug(char *msg)  { return tbpi_cds_pli_message(TBP_MSG_PLI_DEBUG,  10,msg,TBP_PLI_FFL_DATA); }
int tbp_cds_pli_warn(char *msg)   { return tbpi_cds_pli_message(TBP_MSG_PLI_WARN,   -1,msg,TBP_PLI_FFL_DATA); }
int tbp_cds_pli_error(char *msg)  { return tbpi_cds_pli_message(TBP_MSG_PLI_ERROR,  -1,msg,TBP_PLI_FFL_DATA); }
int tbp_cds_pli_panic(char *msg)  { return tbpi_cds_pli_message(TBP_MSG_PLI_PANIC,  -1,msg,TBP_PLI_FFL_DATA); }

/* function to retrieve the argc from the simulator */
static void tbp_get_pli_args(void)
{ tbpi_set_sim_args(0,NULL); }

/* function used to perform any per simulator configuration */
void tbp_simulator_specific_init()
{
#ifdef TBP_MTI
  char *path_seperator;
#endif /* TBP_MTI */
  char *tbp_tty;
  FILE *instream, *outstream;
  

  /* this may be veistigial, but docs suggest it is still required to init pli 1.0 */
  //!!! acc_initialize();

  /* supress acc access errors */
  //!!! acc_configure(accDisplayErrors,"false");

  /* store argv/c away for internal use */
  tbp_get_pli_args();

#ifdef DUAL_PROC
  /* we are linked into a simulator, but we have a pipe transport to the "C side" */
  tbpi_set_transport_mode(TBP_TRANSPORT_MODE_DUAL);
#else  
  /* tbp is linked into a simulator (among other things impacts argv) */
  tbpi_set_transport_mode(TBP_TRANSPORT_MODE_SINGLE); 

  /* FIXME - do I need to check if this is a valid tty? */
  if(NULL != (tbp_tty = tbp_get_argv_key("tbptty"))) {
    
    //if(NULL==freopen(tbp_tty,"w+",stdin))
    //  tbp_panic("TBP Internal: Failed to redirect stdin\n");
    
    if(NULL==(instream= fopen(tbp_tty,"r+")))
      tbp_panic("TBP Internal: Failed to redirect stdin\n");
    else
      tbp_set_screen_input_stream(instream);
    
    //if(NULL==freopen(tbp_tty,"w+",stdout))
    //  tbp_panic("TBP Internal: Failed to redirect stdin\n");
    
    if(NULL==(outstream = fopen(tbp_tty,"w+")))
      tbp_panic("TBP Internal: Failed to redirect stdout\n");
    else
      tbp_set_screen_output_stream(outstream);
    
    
    if(NULL==freopen(tbp_tty,"w+",stderr))
      tbp_panic("TBP Internal: Failed to redirect stderr\n");
  }
#endif

#ifdef TBP_MTI
  path_seperator =  mti_FindProjectEntry ("vsim", "PathSeparator", 0);
  if(path_seperator==NULL)
    tbpi_set_path_seperator('/');  // MTI default
  else
    tbpi_set_path_seperator(path_seperator[0]);  // we only need the first charactor
#else
  tbpi_set_path_seperator('.');
#endif
}

/*
 ********************************************************
 * NC
 ********************************************************
 */
#if defined(TBP_NC) || defined(TBP_CVER)

p_tfcell tbp_pli_init()
{
  tbp_message_module_init();
  return(veriusertfs);
}

#endif

/*
 ********************************************************
 * MTI
 ********************************************************
 */

#ifdef TBP_MTI

void init_usertfs() {
  p_tfcell usertf;
  
  for (usertf = veriusertfs; usertf; usertf ++) {
    if (usertf -> type == 0) return;
    mti_RegisterUserTF(usertf);
  }
}
#endif

int tbp_main(int argc, char *argv[]) {
  (void) argc; (void) argv;
  tbp_panic("How did I ever get here: tbp_main\n");
  return 0;
}
