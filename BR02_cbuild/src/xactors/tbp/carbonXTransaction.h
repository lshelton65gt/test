// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#ifndef _CARBONXTRANSACTION_H_
#define _CARBONXTRANSACTION_H_

#include "shell/carbon_shelltypes.h"

#ifdef __cplusplus
extern "C" {
#endif

  struct carbonXTransactionStruct {
    UInt32  mTransId;       // Transaction ID
    UInt32  mRepeatCnt;     // Number of times the transaction should be executed
    UInt32  mOpcode;        // read/write etc..
    UInt64  mAddress;       // 64bit operation address
    UInt8 * mData;          // data being written to the hdl
    UInt32  mSize;          // size in "bytes"
    UInt32  mStatus;        // user/bfm defined
    UInt32  mWidth;         // 1/2/4/8 byte data width
    UInt32  mInterrupt;     // Interupt from Simulation (for response only)
    UInt32  mHasData;       // Flag to tell wether structure holds data. If it does, size is mSize.
    UInt64  mStartTime;     // starttime of transaction (for response only)
    UInt64  mEndTime;       // endtime of transation (for response only)
    UInt32  mBufferSize;    // Size of allocated Data Buffer
  };
  
  // Function Declarations
  void   carbonXReqRespTransaction(const struct carbonXTransactionStruct *req, struct carbonXTransactionStruct *resp);
  void   carbonXPutTransaction(const struct carbonXTransactionStruct *trans);
  void   carbonXGetTransaction(struct carbonXTransactionStruct *trans);
  void   carbonXInitTransactionStruct(struct carbonXTransactionStruct *obj, CarbonUInt32 buf_size);
  void   carbonXDestroyTransactionStruct(struct carbonXTransactionStruct *obj);
  void   carbonXCopyTransactionStruct(struct carbonXTransactionStruct *dest, const struct carbonXTransactionStruct *src);
  void   carbonXReallocateTransactionBuffer(struct carbonXTransactionStruct *trans);
  UInt32 carbonXCalculateActualTransactionSize(const struct carbonXTransactionStruct *trans);  
  void   carbonXSetTransaction(struct carbonXTransactionStruct *trans,
			       UInt32 trans_id,
			       UInt32 opcode, UInt64 address, const void * data, 
			       UInt32 size,   UInt32 status,  UInt32       width);
#ifdef __cplusplus
}
#endif

#endif
