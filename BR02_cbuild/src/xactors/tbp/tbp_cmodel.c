//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "tbp_internal.h"
#include "tbp_pipe_utils.h"

#include "util/CarbonPlatform.h"

// FIXME - hdl_stop not implimented
// FIXME - transport/send/operation has gotten confusing and round-about - cleanup needed
// FIXME - 64bit endianess is likely broken in cmodels - not tested

u_int32 g_tbp_cmodel_debug = FALSE;
#define tbp_cmodel_debug if(g_tbp_cmodel_debug) tbp_printf

/*
 * function to intialize cmodel module variables
 */
void tbp_cmodel_module_init(void)
{
  TBP_CONF_T *tc=tbp_get_conf();  
  tc->cmodel_time_unit = 1;
  tc->current_ctrans = 0;
  tc->ctrans_cnt = 0;
  tc->ctrans_attach_cnt = 0;
  tc->cmodel_sim_time_func = NULL;
  tc->current_c_time = 0LL;  
  tc->cmodel_thread_relocate = FALSE;
  tc->cmodel_run = TRUE;
}

/*
 * function to change the cmodel time unit
 */
void tbp_cmodel_set_time_unit(u_int32 time_unit)
{
  TBP_CONF_T *tc=tbp_get_conf(); 
  tc->cmodel_time_unit = time_unit > 0 ? time_unit : 1;
}

/*
 * pos/negedge function for cmodels/ctrans
 */
void tbp_posedge(u_int32 *value)
{
  /* wait for transition from false->true */
  while(*value==TRUE)
    tbp_wait_for_sim();
  while(*value==FALSE)
    tbp_wait_for_sim();
}
void tbp_negedge(u_int32 *value)
{
  /* wait for transition from true->false */
  while(*value==FALSE)
    tbp_wait_for_sim();
  while(*value==TRUE)
    tbp_wait_for_sim();
}

/*
 * function used to assign a user-space function to 
 * control sim-time
 */
void tbp_cmodel_set_sim_time_func(u_int64 (*user_sim_time_func) ())
{
 TBP_CONF_T *tc=tbp_get_conf();
 tc->cmodel_sim_time_func = (u_int64 (*)(void))user_sim_time_func;
}

/*
 * reassignable functtion to get the cmodel sim-time
 */
u_int64 tbp_cmodel_get_sim_time(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  if(tc->cmodel_sim_time_func==NULL) {
    /* are we running without hdl? */
    if(tc->trans_cnt == tc->ctrans_cnt)
      return tc->current_c_time;
    else // if there is hdl time and no user time mapped - use hdl
      return tbpi_get_current_hdl_time();
  }
  else 
    return (tc->cmodel_sim_time_func)();
}

/*
 * functions to start/stop cmodel execution
 */
void tbp_cmodel_run()
{
  TBP_CONF_T *tc=tbp_get_conf();
  tc->cmodel_run = TRUE;
}

void tbp_cmodel_stop()
{
  TBP_CONF_T *tc=tbp_get_conf();
  tc->cmodel_run = FALSE;
}

/*
 * functions to start/stop hdl execution
 */
void tbp_hdl_run()
{
  TBP_CONF_T *tc=tbp_get_conf();
  tc->hdl_run = TRUE;
}

void tbp_hdl_stop()
{
  TBP_CONF_T *tc=tbp_get_conf();
  tc->hdl_run = FALSE;
}

/* 
 * Internal function to test cmodel state to see if execution is needed
 */
void tbp_cmodel_check_execute(void)
{
#ifndef DUAL_CSIDE
  u_int64 current_time, previous_time;
#endif /* !DUAL_CSIDE */
  
  /* all modes do nothing if no cmodels are present */
  if(tbp_get_ctrans_count() < 1)
    return;
  
#ifndef DUAL_CSIDE /* C-side of dual process executes unconditionaly */
  /* if time has advanced call cmodels - return if not */
  current_time = tbpi_get_current_hdl_time();
  previous_time = tbpi_get_previous_hdl_time();
  
  if(current_time > previous_time)
    tbpi_set_previous_hdl_time(current_time);
  else
    return;
#endif
  
#ifndef DUAL_SIMSIDE /* pli-side of dual process cannot call this directly */
  tbp_cmodel_execute();
#endif

#ifdef DUAL_SIMSIDE /* only the pli-side of the dual process needs to send this */
  /* send an instruction to the C-side that it is time to update the cmodel */
  tbpi_pipe_send_cmd(FFL_DATA,TBP_PIPE_RUN_CMODEL);
  /* block on cmodel execution - wait for a command */
  tbpi_pipe_get_cmd(FFL_DATA);
#endif
}

/*
 * function used to execute all cmodel threads once in a single loop
 */
void tbp_cmodel_execute(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  BOOL cmodel_exec_done = FALSE;
  u_int32 current_trans;
  int i;
  
  /* save the current hdl transactor value - restore it after we are done */
  current_trans = tc->current_trans;

  /* loop here if hdl run has been disabled before or during this cmodel exec */
  while(cmodel_exec_done == FALSE) {
    if(tc->cmodel_run==TRUE){
      /* go through and execute all register ctrans/cmodel threads */
      for(i=0;i<(int)tc->ctrans_cnt;i++){
	/* check for exit - may have been set while the prior thread executed */
	if(tc->exit_flag) return;

	/* check for a test timeout */
	if(!tbp_check_main_sleep())
	  tbp_thread_activate(tc->sim_thread_id, tc->main_thread_id);
	
	/* set this prior to activate the c-transactor thread */
	tc->current_trans = tc->current_ctrans = i;

	/* check for exit - may have been set while the thread above executed */
	if(tc->exit_flag) return;

	/* execute this transactor thread - double check that a transactor has not been removed */
	if(i < (int)tc->ctrans_cnt) /* is this a valid ctrans? */
	  tbp_thread_activate(tbp_get_my_thread_id(),tc->ctrans_list[i]);
	else
	  break;
      }
    }
    else {
      /* check timeout in case of a dead lock (no hdl or cmodel threads running) */
      if(!tbp_check_main_sleep())
	tbp_thread_activate(tc->sim_thread_id, tc->main_thread_id);
    }
    
    /* increment the cmodel time default */
    tc->current_c_time += tc->cmodel_time_unit;

    /* double check for a dead-lock condition */
    if(tc->hdl_run==FALSE && tc->cmodel_run==FALSE)
      tc->hdl_run = TRUE; // override the setting

    /* check to see if there are any attached hdl transactors - if not keep going */
    if(tc->trans_attach_cnt==0 && tc->ctrans_cnt!=0)
      cmodel_exec_done = FALSE;
    else
      cmodel_exec_done = tc->hdl_run;
  }
  
  /* restore the pre-cmodel hdl current_trans value */
  tc->current_trans = current_trans;
}

/*
 * wrapper function which executes the ctrans/cmodel thread
 */
void tbp_cmodel_thread_wrapper(void (*func_ptr)(), void *usr_ptr)
{
  TBP_CONF_T *tc=tbp_get_conf();
  
  /* now run for eve rexecuting the mapped function */
  while(1){
    if(tc->cmodel_run)
      (func_ptr)(usr_ptr);
    tbp_wait_for_sim();
  }
}

/*
 * function to add an index to the ctrans list
 */
void tbpi_cmodel_add_ctrans_to_list(u_int32 index, FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();
  
  if(NULL==(tc->ctrans_list = (u_int32 *)tbp_realloc(tc->ctrans_list,sizeof(u_int32)*(tc->ctrans_cnt), sizeof(u_int32)*(tc->ctrans_cnt+1))))
    tbp_panic_internal("Failed to allocate space required to add ctrans %d to the list\n",index);
  else
    tc->ctrans_list[tc->ctrans_cnt++] = index;
}

/*
 * create a thread which will hold the ctrans function - mimmics
 * an HDL transactor template
 */
int tbpi_cmodel_thread_create(u_int32 trans_id, void (*func_ptr), void *usr_ptr, FFL_ARGS)
{
  TBP_THREAD_T *thread;
  int index;

  /* create the new cmodel thread */
  index = tbp_thread_init();
  thread = tbp_get_thread(index); // fetch our new thread ptr
  thread->type = TBP_THREAD_TYPE_CMODEL;
  thread->func_ptr = tbp_cmodel_thread_wrapper;
  thread->num_args = 2;
#if pfLP64
  thread->usr_ptr = (void *)tbp_malloc(sizeof(u_int64 *)*2);
  thread->usr_ptr[0] = (u_int64) func_ptr;
  thread->usr_ptr[1] = (u_int64) usr_ptr;
#else
  thread->usr_ptr = (void *)tbp_malloc(sizeof(u_int32 *)*2);
  thread->usr_ptr[0] = (u_int32) func_ptr;
  thread->usr_ptr[1] = (u_int32) usr_ptr;
#endif
  thread->trans_id = trans_id;
  
  /* now actually spawn the thread */
  tbp_thread_create(index);

  /* now add this to the list of ctrans - for fast indexing later */
  tbpi_cmodel_add_ctrans_to_list(index,iFFL_DATA);

  return index;
}

/*
 * this function kills all cmodel threads
 */
void tbpi_delete_cmodel_threads(FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread;
  u_int32 i,thread_cnt,ctrans_cnt,thread_index=tc->main_thread_id+1;
  BOOL cmodel_run_state = tc->cmodel_run;

  if(tbp_is_main()==0){
    tbp_error_internal("This function cannot be called called from within thread (only from main)\n");
    return;
  }

  /* prevent any further execution within the cmodel while we kill the threads */
  tc->cmodel_run = FALSE;

  /* delete any remaining user threads */
  tbp_delete_user_threads();

  /* reconstruct the cmodel trans list */
  tbpi_cmodel_build_ctrans_list(iFFL_DATA);

  tbp_cmodel_debug("\nThere are %d cmodel threads alive (%d total threads)\n\n",
		   tc->ctrans_cnt,tbp_get_thread_count());

  thread_cnt = tbp_get_thread_count();
  /* walk through the c_thread table issuing the kill command */
  for(i=0;i<tc->ctrans_cnt;i++){
    thread = tbp_get_thread(tc->ctrans_list[i]);
    if(thread->type==TBP_THREAD_TYPE_CMODEL &&
       thread->status==TBP_THREAD_STATUS_RUN){ 
      thread->status = TBP_THREAD_STATUS_KILL;
    }
  }

  /* now walk through them and let them die */
  ctrans_cnt = tc->ctrans_cnt;
  for(i=0;i<ctrans_cnt;i++){
    thread = tbp_get_thread(tc->ctrans_list[i]);
    if(thread->type==TBP_THREAD_TYPE_CMODEL &&
       thread->status==TBP_THREAD_STATUS_KILL){
      
      tbp_cmodel_debug("(cmodel) killing thread %d (%s)\n",
		       tc->ctrans_list[i],
		       tc->trans[thread->trans_id].full_name);
      /* now wake them up to allow them to die */ 
      tbp_thread_activate(tc->main_thread_id,tc->ctrans_list[i]);
      tc->ctrans_cnt -= 1;
    }
  }

  for(i=0;i<ctrans_cnt;i++){ 
    thread = tbp_get_thread(tc->ctrans_list[i]);
    /* remove this transactor from the list of registered transactors */
    tbp_cmodel_debug("(cmodel) removing ctrans %d (%s) from the list\n",
		     tc->ctrans_list[i],
		     tc->trans[thread->trans_id].full_name);
    
    tbpi_remove_module(thread->trans_id,iFFL_DATA);
    
  }

  /* now walk through and remove the dead thread */
  tbp_cmodel_debug("(cmodel) thread_cnt %d\n",thread_cnt);

  if(g_tbp_cmodel_debug) tbp_print_thread_status();

  while(thread_index < thread_cnt){
    thread = tbp_get_thread(thread_index);
    if(thread->type==TBP_THREAD_TYPE_CMODEL && 
       thread->status==TBP_THREAD_STATUS_DONE) {

      tbp_thread_free(thread_index);
      tbp_thread_remove(thread_cnt, thread_index);
      thread_cnt -= 1;
      /* reduce the thread count */
      tbp_dec_thread_count();
    }
    else
      thread_index += 1;
  }

  /* free the list of ctransactors used for fast indexing through the thread list */
  tbp_free(tc->ctrans_list);

  /* restore the cmodel run state to whatever it was when we were called */
  tc->cmodel_run = cmodel_run_state;
  /* make sure that we do not have a dead-lock */
  tc->hdl_run = TRUE;
  
  /* cleanup overkill */
  tc->ctrans_attach_cnt = 0;
}

/*
 * function used to rebuild the ctrans list after a thread deletion
 * also reinitializes the ctrans count at the same time
 */
void tbpi_cmodel_build_ctrans_list(FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread;
  u_int32 i;
  
  /* free the list of ctransactors used for fast indexing through the thread list */
  tbp_free(tc->ctrans_list);
  /* zero out the ctrans_cnt */
  tc->ctrans_cnt = 0;

  for(i=0;i<tbp_get_thread_count();i++){
    thread = tbp_get_thread(i);
    /* if this thread is a cmodel thread and it is running - then add it to the new list */
    if(thread->type==TBP_THREAD_TYPE_CMODEL && 
       thread->status==TBP_THREAD_STATUS_RUN)
      tbpi_cmodel_add_ctrans_to_list(i,iFFL_DATA);
  }
}

/*
 * function to wakeup cmodel threads after a user_thread kill and
 * inform them of thier relocation
 */
void tbpi_cmodel_thread_relocate(FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread;
  BOOL cmodel_run_state = FALSE;
  int i;
  
  tbpi_cmodel_build_ctrans_list(iFFL_DATA);
 
  /* cmodel threads must now be relocated */
  cmodel_run_state = tc->cmodel_run; // save this state
  tc->cmodel_run = FALSE; // stop any thread execution

  /* set the global flag to indicazte we are moving things around */
  tc->cmodel_thread_relocate = TRUE;
  
  /* now walk through the cmodel threads and wake them up to get thier new id */
  for(i=0;i<(int)tc->ctrans_cnt;i++){
    thread = tbp_get_thread(tc->ctrans_list[i]);
    if(thread->id != tc->ctrans_list[i]){
      tbp_cmodel_debug("Relocating cmodel thread %d (new id %d)\n",
		       thread->id,tc->ctrans_list[i]);
      tbp_thread_activate(tc->main_thread_id,tc->ctrans_list[i]);
      tbp_cmodel_debug("Thread %d has returned from relocation\n",tc->ctrans_list[i]);
    }
  }
  
  /* walk through and update thier stored id's for future comparison */
  for(i=0;i<(int)tc->ctrans_cnt;i++){
    thread = tbp_get_thread(tc->ctrans_list[i]);
    thread->id = tc->ctrans_list[i];
  }
  
  /* clear the flag and restore the run state */
  tc->cmodel_thread_relocate = FALSE;
  tc->cmodel_run = cmodel_run_state;
}

/*
 * function used by a ctrans to get/put an operation from/to the C-side
 * this function is intended to be called after put/get activity
 * which applies to this operation
 *
 * If attached it will wakeup the attached thread, if not it will
 * return to the sim/top thread which will move to the next ctrans
 */
void tbp_operation(u_int32 trans_id, 
		   u_int32 *opcode,u_int64 *address,
		   u_int32 *size,u_int32 *status,
		   u_int32 *interrupt,u_int32 *reset,
		   u_int64 starttime,u_int64 endtime)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_TRANS_T *trans = &tc->trans[trans_id];
  TBP_OP_T *operation = &tc->trans[trans_id].operation;
  BOOL wake_thread;

  operation->address   = *address;
  operation->size      = *size;
  operation->status    = *status;
  operation->interrupt = *interrupt;
  operation->starttime = starttime;
  operation->endtime   = endtime;
  operation->reset     = *reset;
  
  /* if we are attached to something, wake it up */
  if(trans->attached==TRUE) {
    /* update the opcode only if there is no pending sleep */
    operation->opcode = operation->opcode != TBP_SLEEP ? *opcode : TBP_SLEEP;

    /* reset and interrupt take priority over sleep/idle */
    if(operation->reset!=0 || operation->interrupt!=0)
      wake_thread = TRUE;
    else if(operation->idle_cnt > 0) /* check for ongoing idle */
      wake_thread = FALSE;
    else if(!tbp_check_trans_sleep(trans_id)) /* sleeping? */
      wake_thread = TRUE; /* if not - wake it up */
    else
      wake_thread = FALSE;
    
    /* update idle cnt regardless of reset/interrupt */
    if((operation->idle_cnt > 0) && (operation->opcode != TBP_SLEEP)) {
      operation->opcode = TBP_IDLE;
      operation->idle_cnt -= 1;
    }

    /* wakeup the thread if so indicated and get a new operation */
    if(wake_thread==TRUE) {
      tbp_thread_activate(tbp_get_my_thread_id(), trans->thread_id);
      /* push back an opcode (replace sleep/idle with NOP) */
      if(operation->opcode==TBP_SLEEP || operation->opcode==TBP_IDLE)
	*opcode = TBP_NOP;
      else
	*opcode = operation->opcode;

      /* push back the other data from the transport struct */
      *address   = operation->address;
      *size      = operation->size;
      *status    = operation->status;
      *interrupt = operation->interrupt;
      *reset     = operation->reset;
    }
    else { 
      /* push back an opcode (replace sleep/idle with NOP) */
      if(operation->opcode==TBP_SLEEP || operation->opcode==TBP_IDLE)
	*opcode = TBP_NOP;
      else
	*opcode = operation->opcode;
    }
  }
  else {
    /* push back a noop */
    *opcode   = TBP_NOP;
  }

  /* return to the simulation/top thread */
  tbp_wait_for_sim();
}

/* 
 * function used to determine if calling thread OR transactor
 * transactor attached to the calling thread is a cmodel_transactor
 */
BOOL tbp_is_ctrans(void)
{ 
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread;
  TBP_TRANS_T *trans;

  /* not meaningful if called from main or sim-thread */
  if(tbp_is_main() || tbp_is_sim())
    return FALSE;
  
  thread = tbp_get_current_thread();
  trans = &tc->trans[thread->trans_id];
  
  if(thread->type==TBP_THREAD_TYPE_CMODEL || trans->type==TBP_TRANS_TYPE_CMODEL){
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/*
 * function used to fetch the current ctrans_cnt
 */
int tbp_get_ctrans_count(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  return tc->ctrans_cnt;
}

/*
 * *************************************************
 * LEGACY FUNCTIONS 
 * *************************************************
 */
int tbp_is_ctrans_path(char *path)
{
  TBP_CONF_T *tc=tbp_get_conf();
  int trans_id = tbp_get_trans_id_full(path);
  
  if(trans_id!=-1)
    return tc->trans[trans_id].type==TBP_TRANS_TYPE_CMODEL ? TRUE : FALSE;
  else
    return FALSE;
}

