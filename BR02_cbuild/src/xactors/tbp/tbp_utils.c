//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005-2007 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "tbp_internal.h"
#include "tbp_legacy_utils.h"
#include "tbp_pli.h" 
#include "tbp_pipe_utils.h"
#ifdef DUAL_PROC
#include "tbp_pipe.h"
#endif /* DUAL_PROC */

#include "util/CarbonPlatform.h"

static int g_tbp_util_debug = FALSE;
#define tbp_util_debug if(g_tbp_util_debug) tbp_info

/* Most of the licensing code is only active when DUAL_SIMSIDE is
   *not* defined, so we keep it conditionally compiled in to avoid
   unused variable warnings.  */
#ifndef DUAL_SIMSIDE
#ifdef LICENSE_MANAGER
#include <sys/timeb.h>
#ifdef __GNUC__
/* The lp variable is a static variable defined in lmpolicy.h that is
   not used by this module.  Adding the unused attribute will quiet
   unused variable warnings from GCC.  Note, we rename the variable to
   avoid defining a recursive macro, although it appears GCC has
   protection against direct recursion.  Instead, we duplicate the
   declaration of lp below once lp has been #undefined.  */
#define lp lmpolicy_lp __attribute__ ((unused))
#endif /* __GNUC__ */
#include "lmpolicy.h"
#undef lp
static LP_HANDLE_PTR lp __attribute__ ((unused));
static const char *flexlm_error_strings[] = {
#include "lmerrors.h"
};
static const int flexlm_error_strings_count = 
	sizeof(flexlm_error_strings) / sizeof(char *);

static LP_HANDLE *g_tbp_lp_handle = NULL;
static s_int64 g_tbp_license_timer = 0LL;

// This string determines what feture name to use for licensing
// TBP Licensing is disabled in Carbon.  
static char *g_license_string = "ZaiqTransactorLibrary";
static char *g_license_version = "5.0";
#endif
#endif /* !DUAL_SIMSIDE */

/* provide for one time only initialization */
static BOOL g_tbp_initialized = FALSE;

/* keep a static global opertion struct - to isolate transactor specific struct */
static u_int64 g_previous_hdl_time = 0LL;
static u_int64 g_current_hdl_time = 0LL;

static s_int64 g_tbp_license_interval = 30; //TBP_LICENSE_INTERVAL;
static void (*g_tbp_lic_heartbeat_func)() = NULL;
static void (*g_tbp_lic_checkout_func)() = NULL;

/*
 * function to en/disable debug printing within this module
 */
void tbp_set_util_debug_flag(int flag) { g_tbp_util_debug = flag; }

/* functions to set/get the license checking interval */
void tbp_set_license_interval(s_int64 license_interval) { g_tbp_license_interval = license_interval; }
s_int64 tbp_get_license_interval(void)                  { return g_tbp_license_interval; }

/* function to set the setget_sleep flag */
void tbp_setget_sleep(BOOL flag) { tbp_get_conf()->setget_sleep = flag; }

/* internal functions to set/get the main timeout value */
void tbpi_set_main_timeout(u_int64 timeout) { tbp_get_conf()->main_timeout = timeout; }
u_int64 tbpi_get_main_timeout(void)         { return tbp_get_conf()->main_timeout; }

/* internal functions used to set/get the current time values */
void tbpi_set_current_hdl_time(u_int64 time)  { g_current_hdl_time = time;  }
void tbpi_set_previous_hdl_time(u_int64 time) { g_previous_hdl_time = time; }

u_int64 tbpi_get_current_hdl_time(void)  { return g_current_hdl_time;  }
u_int64 tbpi_get_previous_hdl_time(void) { return g_previous_hdl_time; }

/* function to report our initizlization status */
BOOL tbp_is_initialized(void) { return g_tbp_initialized;}

/* Internal functions to retrieve the sim/main thread id's */
int tbpi_main_thread_id(void) { return tbp_get_conf()->main_thread_id; }
int tbpi_sim_thread_id(void)  { return tbp_get_conf()->sim_thread_id; }

/* functions to set/get the thread stack size */
void tbp_set_stack_size(int size) { tbp_get_conf()->stack_size = size; }
int tbp_get_stack_size(void)      { return tbp_get_conf()->stack_size; }

/* Internal function to check/set main is waiting */
BOOL tbpi_main_is_waiting(void) { return tbp_get_conf()->main_is_waiting; }
void tbpi_set_main_is_waiting(BOOL flag) { tbp_get_conf()->main_is_waiting = flag; }

/* functions to set/get the current transactor id */
void tbpi_set_current_trans(int trans_id) { tbp_get_conf()->current_trans = trans_id; }
int tbpi_get_current_trans(void) { return tbp_get_conf()->current_trans; }

/* function to get the current transactor count */
int tbp_get_trans_count(void) { return tbp_get_conf()->trans_cnt; }

/* function to check the attached state of a transactor */
BOOL tbpi_trans_attached(int trans_id) 
{ return tbp_get_conf()->trans[trans_id].attached; }

/* function to retrieve the thread attached to a given transactor */
u_int32 tbpi_trans_thread_id(int trans_id) 
{ return tbp_get_conf()->trans[trans_id].thread_id; }

/* function to retrieve the operation associated with a given transactor */
TBP_OP_T *tbpi_trans_operation(int trans_id) 
{ return &tbp_get_conf()->trans[trans_id].operation; }

/* function to retrieve the inst_data pointer associated with a given transactor */
void *tbpi_trans_inst_data(int trans_id) 
{ return &tbp_get_conf()->trans[trans_id].inst_data; }

/* function to retrieve the trans struct associated with a given transactor */
TBP_TRANS_T *tbpi_get_trans(int trans_id) 
{ return &tbp_get_conf()->trans[trans_id]; }

/* function to store sim_argv/argc */
void tbpi_set_sim_args(int argc, char **argv) 
{ 
  tbp_get_conf()->sim_argc = argc;
  tbp_get_conf()->sim_argv = argv;
}

/* functions to set/get the finish flag */
BOOL tbpi_get_finish_flag(void)        { return tbp_get_conf()->finish_flag; }
void tbpi_set_finish_flag(BOOL finish) { tbp_get_conf()->finish_flag = finish; }

/* functions to set/get the path seperator value */
char tbpi_get_path_seperator(void)           { return tbp_get_conf()->path_seperator; }
void tbpi_set_path_seperator(char seperator) { tbp_get_conf()->path_seperator = seperator; }

/* function to set/get the transport mode (dual/single/nohdl etc) */
int tbpi_get_transport_mode(void)      { return tbp_get_conf()->transport_mode; }
void tbpi_set_transport_mode(int mode) { tbp_get_conf()->transport_mode = mode; }

/* function to set/get the acc error flag */
void tbpi_set_acc_error_enable(BOOL enable) { tbp_get_conf()->acc_error_enable = enable; }
BOOL tbpi_get_acc_error_enable(void) { return tbp_get_conf()->acc_error_enable; }

/* function to set/get the no_hdl flag */
void tbp_set_no_hdl_flag(BOOL flag) { tbp_get_conf()->no_hdl = flag; }
BOOL tbp_get_no_hdl_flag(void)      { return tbp_get_conf()->no_hdl; }

/* function to assign an external licenses heartbeat */
void tbp_set_lic_heartbeat_func(void (*func)()) { g_tbp_lic_heartbeat_func = func; }

/* function to assign an external license checkout */
void tbp_set_lic_checkout_func(void (*func)()) { g_tbp_lic_checkout_func = func; }

/* function to check the status of a given license key */
#ifndef DUAL_SIMSIDE
#ifdef LICENSE_MANAGER
static void tbp_license_heartbeat_key(LP_HANDLE *lp_handle)
{
    int i, num_reconnects, num_minutes=1;

    if(lp_handle==NULL) return;

    for (i=0; i<5; i++) {
      if (lp_heartbeat(lp_handle, &num_reconnects, num_minutes) == 0)
	break;
      sleep(5);
    }
    if (i == 5) {
      fprintf(stderr, "License server crash -- exiting");
      exit(1);
    }
}
#endif
#endif
/* function to check for a license (heartbeat) */
void tbp_license_heartbeat(void)
{
#ifndef DUAL_SIMSIDE
#ifdef LICENSE_MANAGER
  //struct timeb tm;
  //ftime(&tm);
  tbp_license_heartbeat_key(g_tbp_lp_handle);
  tbp_util_debug("License HeartBeat\n");
  
  if(g_tbp_lic_heartbeat_func!=NULL) {
    tbp_util_debug("Calling External licenses heartbeat function\n");
    (g_tbp_lic_heartbeat_func)();
  }
#endif
#endif
}

#ifndef DUAL_SIMSIDE
#ifdef LICENSE_MANAGER
/* function to checkout a license */
static int tbp_license_checkout(char *key, char *version, LP_HANDLE **lp_handle)
{
    int status;

    if (*lp_handle != NULL) {  // license has already been checked out
	tbp_printf("Trying to check out key %s twice, skipping...\n", key);
	return(0);
    }

    status = lp_checkout(LPCODE, LM_RESTRICTIVE | LM_MANUAL_HEARTBEAT, key, version, 1, NULL, lp_handle);
    
    if ((status == LM_MAXUSERS)) {
      tbp_printf("All %s Version %s licenses are in use, waiting...\n", key, version);
      status = lp_checkout(LPCODE, LM_QUEUE | LM_MANUAL_HEARTBEAT, key, version, 1, NULL, lp_handle);
    }
    if (status == 0) {
      tbp_util_debug("License checked out: %s Version %s\n", key, version);
    }
    else {
      if (status < 0)
	status = 0 - status;
      tbp_printf("License checkout failed, status = -%d: %s\n",
		 status, ( (status < flexlm_error_strings_count) ?
			   flexlm_error_strings[status] : "<unknown>") );
      exit(1);
    }
    
    // set an alarm to handle the heartbeat
    return(status);
}
#endif
#endif

/* function to get the license server started */
void tbp_start_license(void)
{
#ifndef DUAL_SIMSIDE
#ifdef LICENSE_MANAGER
  char *license_file_setting;
  if (tbp_license_checkout(g_license_string, g_license_version, &g_tbp_lp_handle) !=0){
    license_file_setting = getenv("LM_LICENSE_FILE");
    fprintf(stderr,"Cannot get license: %s Version %s\nLM_LICENSE_FILE: %s\n",
	    g_license_string, g_license_version,
	    license_file_setting==NULL ? " " : license_file_setting);
    exit(1);
  }
#endif
#ifdef LICENSE_TIME_BOMB
  Dl_info dl_info;
  char *file_name;
  struct stat file_stat;
  struct tm *tm;
 
  /* get the current time */
  time_t current_time = time(NULL);
  tm = localtime(&current_time);
  
  /* get the current file_name */
  dladdr(tbp_init,&dl_info);
  file_name = (char *)dl_info.dli_fname;
  
  if(file_name==NULL){
    //printf("Failed to extract filename\n");
  }
  else {
    //printf("file %s\n",file_name);
    stat(file_name,&file_stat);
    //printf("file date = %s\n",ctime(&file_stat.st_mtime));
    
    /* is the current time greater than the timestamp of the file - it better be! */
    if(current_time < file_stat.st_mtime) {
      tbp_printf("License checkout failed: %s Version %s\n", g_license_string, g_license_version);
      exit(-1);
    }
  }

  /* has the license time expired */
  if((tm->tm_mon+1) <= 1 && (tm->tm_mday <= 31) && ((1900+tm->tm_year) <= 2004)){
    //tbp_printf("License checked out: %s Version %s\n", g_license_string, g_license_version);
  }
  else{
    tbp_printf("License checkout failed: %s Version %s\n", g_license_string, g_license_version);
    exit(1);
  }
#endif
#endif
}

/* function to check the license(s) every N calls to limit BW */
void tbp_check_license(void)
{
#ifndef DUAL_SIMSIDE
#ifdef LICENSE_MANAGER
  if(g_tbp_license_timer > g_tbp_license_interval) {
    g_tbp_license_timer = 0;
    tbp_license_heartbeat();
  }
  else {
    g_tbp_license_timer += 1LL;
  }
#endif /* LICENSE_MANAGER */
#endif /* DUAL_SIMSIDE */
}

/*
 * function to send data to a transactor
 *   put_data and put_datax pointers are char* and the data/datax pointers are cast
 *     to char* so the pointer arithmetic with offset does not cause warnings and
 *     works properly.
 */
void tbpi_put_trans_data(u_int32 trans_id, u_int32 index, 
			 void *data, void *datax, 
			 u_int32 size, u_int32 width,
			 FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread = tbp_get_current_thread();
  TBP_OP_T *operation = &tc->trans[trans_id].operation;
  char *put_data, *put_datax;
  int put_size = size;
  int offset = index*(width&0xf);
  u_int32 *data_ptr32;

  tbp_util_debug("tbp_put_data: size = %d width = %d index = %d offset = %d\n",
		 put_size,width&0xf,index,offset);  

  /* make sure that the input pointers are valid */
  if(data==NULL || size==0) {tbp_util_debug("null data or size (%d %d)\n",data,size); return;}

  /* make sure there is enough buffer to write this data */
  tbpi_set_buffer_size(trans_id, (put_size+offset),iFFL_DATA);

  /* set the operation to the provided width */
  //  operation->width = width!=0 ? width : TBP_WIDTH_32; // default to 32bit

  /*
   * Normal threads are moving data into the transactor
   * Cmodel threads are moving data from a ctrans to the C-test
   */
  if(thread->type==TBP_THREAD_TYPE_CMODEL || thread->type==TBP_THREAD_TYPE_SIM){
    put_data  = (char *) operation->read_data;
    put_datax = (char *) operation->read_datax;
  }
  else { 
    put_data  = (char *) operation->write_data;
    put_datax = (char *) operation->write_datax;
  }

  /* offset the pointer by the incoming index */
  put_data  += offset;
  put_datax += offset;
  
  /* calculate how many bytes we need to copy, based on the size and width of the transaction. */
  put_size = tbp_calc_copy_size(size, width);
  
  /* copy in the put data - assuming we have 2 unique pointers */
  if(put_data!=data)
    memcpy(put_data,(char *) data,put_size);

  /* if a datax pointer is provided fill it in as well - otherwise clear it out */
  if(datax!=NULL && put_datax!=datax)
    memcpy(put_datax,(char *) datax,put_size);
  else
    memset(put_datax,0,put_size);

  if(g_tbp_util_debug){
    u_int32 i;
    data_ptr32 = (u_int32 *)data;
    for(i=0;i<((size+3)/4);i++)
      tbp_util_debug("put_data32[%d] = 0x%8.8x\n",i,data_ptr32[i]);
  }
}

/*
 * function to get data from the simulation 
 *
 *   get_data and get_datax pointers are char* and the data/datax pointers are cast
 *     to char* so the pointer arithmetic with offset does not cause warnings and
 *     works properly.
 */
void tbpi_get_trans_data(u_int32 trans_id, u_int32 index, 
			 void *data, void *datax, 
			 u_int32 size, u_int32 width,
			 FFL_ARGS)
{
  (void) func; (void) line; (void) file;
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread = tbp_get_current_thread();
  TBP_OP_T *operation =  &tc->trans[trans_id].operation;
  char *get_data, *get_datax;
  int get_size = size;
  int offset = index*(width&0xf);
  u_int32 *data_ptr32;
  
  tbp_util_debug("tbp_get_data: size = %d width = %d index = %d offset = %d\n",
		 get_size,width&0xf,index,offset);

  /* make sure that the input pointers are valid */
  if(data==NULL || size==0) return;

  /* make sure there is enough data in the trans buffer to perform this read */
  if((get_size+offset) > (int) operation->buffer_size)
    tbp_panic_internal("tbp_get_data: request (%d) is larger than the available buffer (%d)\n",
	      get_size,operation->buffer_size);

  /* set the operation to the provided width */
  //operation->width = width!=0 ? width : TBP_WIDTH_32; // default to 32bit

  /*
   * Normal threads are moving data out of the transactor
   * Cmodel threads are moving data out C-test
   */
  if(thread->type==TBP_THREAD_TYPE_CMODEL || thread->type==TBP_THREAD_TYPE_SIM){
    get_data  = (char *) operation->write_data;
    get_datax = (char *) operation->write_datax;
  }
  else {
    get_data  = (char *) operation->read_data;
    get_datax = (char *) operation->read_datax;
  }

  /* offset the pointer by the incoming index */
  get_data  += offset;
  get_datax += offset; 

  /* align the size to 32bit boundary - using integer divide */
  get_size = ((get_size+3)/4)*4;

  /* copy the data out of the buffer - assuming we have 2 unique pointers */
  if(get_data!=(char *) data)
    memcpy((char *) data,get_data,get_size);

  /* if there is xdata, then copy it in too */
  if(datax!=NULL && get_datax!=(char *) datax)
    memcpy((char *) datax,get_datax,get_size);  

  if(g_tbp_util_debug){
    u_int32 i;
    data_ptr32 = (u_int32 *)get_data;
    for(i=0;i<((size+3)/4);i++)
      tbp_util_debug("get_data32[%d] = 0x%8.8x\n",i,data_ptr32[i]);
  }
}

/* 
 * operation primitive function copies the contents of a given 
 * operation struct into the "current" operation and waits
 * for the simulation to return
 */
void tbpi_send_operation(u_int32 trans_id, TBP_OP_T *operation, FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread = tbp_get_current_thread();
  TBP_OP_T *current_op = &tc->trans[trans_id].operation;
  
  /*
   * Configuration Space Operations is handled by the c-side, so don't pass them to verilog
   */
  if(operation->opcode == TBP_CS_WRITE) {
    tc->trans[trans_id].cs_get_data[(operation->address&0xff)>>2] = *((u_int32 *)(operation->write_data));
  }
  else if(operation->opcode == TBP_CS_READ) {
    if( operation->address & 0x100) {
      *((u_int32 *)(current_op->read_data)) = tc->trans[trans_id].cs_put_data[(operation->address&0xff)>>2];
    }
    else {
      *((u_int32 *)(current_op->read_data)) = tc->trans[trans_id].cs_get_data[(operation->address&0xff)>>2];
    }
  }
  else {
    /* 
     * check to see if we really need to copy the data from the provided 
     * pointer to the current transactors operation struct
     */
    if(operation!=current_op){
      current_op->opcode    = operation->opcode;
      current_op->address   = operation->address;   
      current_op->size      = operation->size;
      current_op->status    = operation->status;    
      current_op->interrupt = operation->interrupt;
      current_op->reset     = operation->reset;
      current_op->starttime = operation->starttime; 
      current_op->endtime   = operation->endtime;   
      current_op->idle_cnt  = operation->idle_cnt;  
      current_op->direction = operation->direction;
      current_op->width     = operation->width;
      
      /* detect/alter behavior for calls from cmodel transactors */
      if(thread->type==TBP_THREAD_TYPE_CMODEL) {
	tbp_operation(trans_id,
		      &current_op->opcode,
		      &current_op->address,
		      &current_op->size,
		      &current_op->status,
		      &current_op->interrupt,
		      &current_op->reset,
		      current_op->starttime,
		      current_op->endtime);
      }
      else {
	/* if so indicated - send the data to the sim */
	if(operation->direction==TBP_OP_BIDIR || operation->direction==TBP_OP_WRITE)
	  tbpi_put_trans_data(trans_id,0,operation->write_data,operation->write_datax,
			      operation->size,operation->width,FFL_DATA);
	
#ifdef DUAL_PROC      
	tbpi_pipe_send_cmd(iFFL_DATA,TBP_PIPE_TRANSPORT,trans_id);
#else
	(void) func; (void) line; (void) file;
#endif
	tbp_wait_for_sim();
	
	/* if so indicated - get the sim data */
	if(operation->direction==TBP_OP_BIDIR || operation->direction==TBP_OP_READ)
	  tbpi_get_trans_data(trans_id,0,operation->read_data,operation->read_datax,
			      operation->size,operation->width,FFL_DATA);
	/* reset */
	operation->opcode    = current_op->opcode;
	operation->address   = current_op->address;   
	operation->size      = current_op->size;
	operation->status    = current_op->status;    
	operation->interrupt = current_op->interrupt;
	operation->reset     = current_op->reset;
	operation->starttime = current_op->starttime; 
	operation->endtime   = current_op->endtime;   
	operation->idle_cnt  = current_op->idle_cnt;  
	operation->direction = TBP_OP_BIDIR;
	operation->width     = TBP_WIDTH_32;
      }
    }
    else { /* we have been passed a pointer to our own struct - nothing to do */ 
      if(thread->type==TBP_THREAD_TYPE_CMODEL)
	tbp_operation(trans_id,
		      &operation->opcode,
		      &operation->address,
		      &operation->size,
		      &operation->status,
		      &operation->interrupt,
		      &operation->reset,
		      operation->starttime,
		      operation->endtime);
      else {
#ifdef DUAL_PROC      
	tbpi_pipe_send_cmd(iFFL_DATA,TBP_PIPE_TRANSPORT,trans_id);
#endif
	tbp_wait_for_sim();
      }
    }
  }
}

/* base function used to execute the requested action */
int tbpi_transaction(u_int32 trans_id, u_int32 *opcode, u_int64 *address, 
		     void *data, void *datax, u_int32 *size, u_int32 *status, 
		     u_int32 direction, u_int32 width, FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_OP_T *operation = &tc->trans[trans_id].operation;
  u_int32 idle_cnt;

  //printf("in tbpi_transaction: trans_id = %d, simtime = %lld, status = %x\n", trans_id, tbp_get_sim_time(), *status);

  operation->address   = *address;
  operation->size      = *size;
  operation->status    = *status;
  operation->interrupt = 0;
  operation->reset     = 0;
  operation->direction = direction;
  operation->width     = width;

  /* special cases for idle/noop */
  if(*opcode==TBP_IDLE || *opcode==TBP_NOP){
    idle_cnt = (u_int32) (operation->address & 0x00000000ffffffffLL); 
    operation->opcode    = idle_cnt > 0 ? *opcode : TBP_NOP;
    operation->idle_cnt  = idle_cnt > 0 ? idle_cnt - 1 : 0;
  }
  else { 
    operation->opcode = *opcode;
    operation->idle_cnt = 0;
  }

  /* if so indicated - send the data to the sim */
  if(*opcode != TBP_CS_WRITE && *opcode != TBP_CS_READ) {
    if(direction==TBP_OP_BIDIR || direction==TBP_OP_WRITE)
      tbpi_put_trans_data(trans_id,0,data,datax,operation->size,operation->width,FFL_DATA);
    
#ifdef DUAL_PROC   
    if(tc->trans[trans_id].type!=TBP_TRANS_TYPE_CMODEL)
      tbpi_pipe_send_cmd(iFFL_DATA,TBP_PIPE_TRANSPORT,trans_id);
#else
    (void) func; (void) line; (void) file;
#endif
    /* now wait for the simulation to execute */
    tbp_wait_for_sim();
  }

  if(*opcode == TBP_CS_WRITE) {
    tc->trans[trans_id].cs_get_data[((*address)&0xff)>>2] = *((u_int32 *)data);
  }
  else if(*opcode == TBP_CS_READ) {
    if( *address & 0x100) {
      *((u_int32 *)data) = tc->trans[trans_id].cs_put_data[((*address)&0xff)>>2];
    }
    else {
      *((u_int32 *)data) = tc->trans[trans_id].cs_get_data[((*address)&0xff)>>2];
    }
  }
  else {
    /* Back from the simulation */
    
    /* if so indicated - get the sim data */
    if(direction==TBP_OP_BIDIR || direction==TBP_OP_READ)
      tbpi_get_trans_data(trans_id,0,data,datax,operation->size,operation->width,FFL_DATA);
    
    /* send back the resultant feilds */
    *opcode  = operation->opcode;
    *address = operation->address;
    *size    = operation->size;
    *status  = operation->status;
  }

  /* clear out control feilds */
  operation->direction = TBP_OP_BIDIR;
  operation->width     = TBP_WIDTH_32;

  return *status;
}

/*
 * function to activate a given thread from the sim-side 
 */
void tbp_sim_wakeup_thread(int trans_id, int calling_thread, int target_thread)
{
#ifndef DUAL_PROC
  (void) trans_id;
  /* simple case in single process mode */
  tbp_thread_activate(calling_thread, target_thread);
#else /* dual process mode */
  TBP_CONF_T *tc=tbp_get_conf();

  if(tc->trans[trans_id].type!=TBP_TRANS_TYPE_CMODEL) {
    /* first send the wakeup command */
    tbpi_pipe_send_cmd(FFL_DATA,TBP_PIPE_THREAD_WAKE,target_thread,trans_id);
    /* now wait for a response */
    tbpi_pipe_get_cmd(FFL_DATA);
  }
#endif
}

/*
 * this function process an opeation from pli_transport to decide what
 * if anything to do with the attached thread - return value indicates
 * whether or not the operation struct has been modified
 *
 * The operation provided should be a local copy - not the given
 * transactor's actual operation struct
 */
BOOL tbpi_sim_transport(u_int32 trans_id, TBP_OP_T *sim_op)
{  
  TBP_OP_T *operation;
  BOOL wake_thread = FALSE;

  /* debug */
  tbp_util_debug("tbp_transport called for %s\n",tbp_get_trans_name(trans_id));

  /* check license status (will only check once for many calls) */
  tbp_check_license();

  /* check to make sure we have been initialized */
  if(tbp_is_initialized()==FALSE) {
    sim_op->opcode = TBP_NOP;
    return FALSE; // just return - nothing to do yet
  }

  /* check for a test timeout/complete */
  if(!tbp_check_main_sleep())
    tbp_sim_wakeup_thread(trans_id,tbp_get_my_thread_id(), tbpi_main_thread_id());

  /* if time has advanced call cmodels if any exist - this will just return if not */
  tbp_cmodel_check_execute();
  
  /* if we are attached to something, wake it up */
  if(tbpi_trans_attached(trans_id)==TRUE) { 
    /* if so, grab pointers to its data structures */
    /* assign local pointers for subsequent access */
    operation = tbpi_trans_operation(trans_id);
    
    /* copy over the elements of the operation that are unconditional */
    operation->size      = sim_op->size;
    operation->status    = sim_op->status;
    operation->interrupt = sim_op->interrupt;
    operation->reset     = sim_op->reset;
    operation->starttime = sim_op->starttime;
    operation->endtime   = sim_op->endtime;

    /* update the opcode only if there is no pending sleep */
    operation->opcode  = operation->opcode != TBP_SLEEP ? sim_op->opcode : TBP_SLEEP;
    operation->address = operation->opcode != TBP_SLEEP ? sim_op->address : operation->address;
    
    /* reset and interrupt take priority over sleep/idle */
    if(operation->reset!=0 || operation->interrupt!=0)
      wake_thread = TRUE;
    else if(operation->idle_cnt > 0) /* check for ongoing idle */
      wake_thread = FALSE;
    else if(!tbp_check_trans_sleep(trans_id)) /* sleeping? */
      wake_thread = TRUE; /* if not - wake it up */
    else
      wake_thread = FALSE;
    
    /* update idle cnt regardless of reset/interrupt */
    if((operation->idle_cnt > 0) && (operation->opcode != TBP_SLEEP)) {
      operation->opcode = TBP_IDLE;
      operation->idle_cnt -= 1;
    }
    
    /* wakeup the thread if so indicated and get a new operation */
    if(wake_thread==TRUE){
      tbp_sim_wakeup_thread(trans_id, tbp_get_my_thread_id(), tbpi_trans_thread_id(trans_id));
      
      /* push back an opcode (replace sleep/idle with NOP) */
      if(operation->opcode==TBP_SLEEP || operation->opcode==TBP_IDLE)
	sim_op->opcode = TBP_NOP;
      else
	sim_op->opcode = operation->opcode;
      
      /* push back the other data from the transport struct */
      sim_op->address   = operation->address;
      sim_op->size      = operation->size;
      sim_op->status    = operation->status;
      sim_op->interrupt = operation->interrupt;
      sim_op->reset     = operation->reset;
    }
    else {
      /* push back a noop/idle (replace sleep/idle with NOP) */
      if(operation->opcode==TBP_SLEEP || operation->opcode==TBP_IDLE)
	sim_op->opcode = TBP_NOP;
      else
	sim_op->opcode = operation->opcode;
    }
  }
  else {
    sim_op->opcode = TBP_NOP;
  }
  
#ifndef DUAL_PROC
  /* check for shutdown due to insufficient running threads */
  if((tbp_get_thread_count() - tbp_get_ctrans_count()) < 2) {
    // FIXME - seems like there is a possiblity that we should have a way to let the sim go
    tbp_util_debug("there are no more user threads left running, sim thread will now shutdown\n");
    tbp_shutdown(0);
  }
  
  /* 
   * if the "main" thread is waiting and there is noone to wake it up 
   * we need to wake it up and let it die/shutdown
   */
  if((tbpi_main_is_waiting()==TRUE) && (tbp_get_thread_count() <= 2) && (tbpi_get_main_timeout()==0))
    tbp_sim_wakeup_thread(trans_id, tbp_get_my_thread_id(), tbpi_main_thread_id());
#endif

  /* if we have not shutdown, then return our status */
  return wake_thread;
}

/* 
 * This function is called by the top level of the C-test to allow
 * the simulation to begin running after all thread association is done
 * **** This is an alternate method to evt_waittest ****
 * It removes the need to assign a signal and use:
 * tbp_test_wait(timeout); old -> tbp_evt_waittest("test_complete",timeout);
 * tbp_test_complete();    old -> tbp_evt_signal("test_complete");
 * OR (if halting - which produces an error)
 * tbp_test_halt();        old -> tbp_evt_signal(TBP_EVT_HALT);
 */
void tbpi_test_wait(s_int64 timeout,FFL_ARGS)
{ 
  tbpi_evt_waittest(tbp_get_test_complete_event(),timeout,iFFL_DATA);
}

/* this function tells the main function to wakeup */
// FIXME - do we need to do this brute force? - might be dangerous
void tbp_test_complete(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  u_int32 calling_thread_id=tbp_get_my_thread_id();
  
  if(tc->test_complete_flag==FALSE) {
    tc->test_complete_flag = TRUE;
    tbp_evt_signalstate(tbp_get_test_complete_event(),1);
    tbp_thread_activate(calling_thread_id,tc->main_thread_id);
  }
  else {
    tbp_thread_cleanup(calling_thread_id);
  }
}

/*
 * function used to halt a test (killing a main_wait)
 */  
void tbp_test_halt(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  
  /* set the signal regardless of the calling thread */
  tbp_evt_signalstate(TBP_EVT_HALT,1);
  
  /* if we are not in the main thread - we need to get there */
  if(!tbp_is_main())
    tbp_thread_activate(tbp_get_my_thread_id(),tc->main_thread_id);
}

/*
 * function to check the sleep status of a transactor
 */
BOOL tbpi_check_trans_sleep(u_int32 trans_id, FFL_ARGS)
{
  (void) func; (void) line; (void) file;
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_OP_T *operation = &tc->trans[trans_id].operation;
  
  if(operation->opcode==TBP_SLEEP){
    /* sleep operation's timeout is passed in the address */
    if((operation->address!=0) && (tbp_get_sim_time() > operation->address))
      return FALSE;
    else 
      return TRUE;
  }
  else {
    return FALSE;
  }
}

/*
 * function to check the timeout/halt state of the main thread 
 */
BOOL tbpi_check_main_sleep(FFL_ARGS)
{
  (void) func; (void) line; (void) file;
  TBP_CONF_T *tc=tbp_get_conf();
  u_int64 main_timeout = tbpi_get_main_timeout();

  if((main_timeout!=0) && (tbp_get_sim_time() > main_timeout))
    tc->main_is_waiting = FALSE; /* clear the flag */

  return tc->main_is_waiting;
}
  
/*
 * function to cleanup after the test 
 */
void tbpi_test_cleanup(FFL_ARGS)
{
  tbpi_evt_delete_all(iFFL_DATA);
  tbpi_delete_user_threads(iFFL_DATA);
}

/*
 * function to clear any pending incoming IPC data for a test reset 
 */
void tbp_ipc_flush(void)
{
#ifdef DUAL_CSIDE
  tbp_pipe_flush();
#endif
}

/*
 * print error status 
 */
void tbp_print_error_status(void)
{
#ifndef DUAL_SIMSIDE
  tbp_printf("\n\n**********************************************************\n");
  if(tbp_get_error_count_all()==0 && tbp_get_warning_count_all()==0){
    tbp_printf("         No Errors Detected\n");
  }
  else {
    tbp_printf(" %8d total warnings in this test\n",tbp_get_warning_count());
    tbp_printf(" %8d total warnings in all tests run\n",tbp_get_warning_count_all());
    tbp_printf(" %8d total errors in this test (C: %d, Sim: %d)\n",
	       tbp_get_error_count(), tbp_get_c_error_count(),tbp_get_sim_error_count());
    tbp_printf(" %8d total errors in all tests run\n",tbp_get_error_count_all());
  }
  tbp_printf("**********************************************************\n\n");
#endif
}

/* pack two 32bit values into a single 64bit */
u_int64 tbp_pack64(u_int32 hi, u_int32 lo)
{
  u_int64 data_hi = (u_int64)hi;
  u_int64 data_lo = (u_int64)lo;

  return ((data_hi << 32) & 0xffffffff00000000LL) | (data_lo & 0x00000000ffffffffLL);
}

/* function to get the current sim time */
u_int64 tbp_get_sim_time(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  
  /* are there an cmodels? - fast decision first for HDL */
  if(tc->ctrans_cnt==0)
    return tbpi_get_current_hdl_time();
  /* are we running without hdl? */
  else if(tc->trans_cnt == tc->ctrans_cnt)
    return tbp_cmodel_get_sim_time();
  /* are we in a cmodel thread? */ 
  else if(tbp_is_ctrans())
    return tbp_cmodel_get_sim_time();
  /* are there no hdl threads attached, but some ctrans attached? */
  else if(tc->ctrans_attach_cnt != 0 && tc->trans_attach_cnt ==0)
    return tbp_cmodel_get_sim_time();
  /* if all else fails - return hdl time */
  else
    return tbpi_get_current_hdl_time();
}

/* check to see if this is a valid trans_id */
void tbp_check_trans_id(int id)
{
  TBP_CONF_T *tc=tbp_get_conf();
  /* check to see if this transactor is registered */
  if(id >= (int)tc->trans_cnt){
    tbp_panic("Unregistered module id = %d detected\n",id);
  }
}
  
/*
 * function to check to see if this is the main thread 
 */
BOOL tbp_is_main(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  return tbp_get_my_thread_id()==tc->main_thread_id ? TRUE : FALSE;
}

/*
 * function to check to see if this is the "sim" thread 
 */
BOOL tbp_is_sim(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  return tbp_get_my_thread_id()==tc->sim_thread_id ? TRUE : FALSE;
}

/*
 * function to check to see if this is a user thread
 */
BOOL tbp_is_user(void)
{
  return tbp_get_current_thread()->type == TBP_THREAD_TYPE_USER ? TRUE : FALSE;
}

/* 
 * function which searches the trans table for an entry and returns its index
 * if it find one
 */
int tbp_get_trans_id_full(char *full_name)
{
  TBP_CONF_T *tc=tbp_get_conf();
  u_int32 i;
  
  for(i=0; i<tc->trans_cnt; i++){
    if(tc->trans[i].full_name != NULL){
      if(strcmp(full_name,tc->trans[i].full_name)==0) {
	return i;
      }
    }
  }
  return  -1;
}

/*
 * function to return the transactor name
 */
char *tbp_get_trans_name(int trans_id)
{
  TBP_CONF_T *tc=tbp_get_conf();

  tbp_check_trans_id(trans_id);

  if(tc->trans[trans_id].reg_name!=NULL)
    return tc->trans[trans_id].reg_name;
  else if(tc->trans[trans_id].full_name!=NULL)
    return tc->trans[trans_id].full_name;
  else
    return "unknown";
}

/*
 * function to return the current path name in an attached thread 
 * NULL is returned for sim/main threads
 */
char *tbp_get_current_path(void)
{
  TBP_CONF_T *tc=tbp_get_conf();

  /* check to see that we are in an attached thread */
  if(tbp_is_main() || tbp_is_sim()){
    return NULL;
  }
  else {
    return tc->trans[tbp_get_my_trans_id()].reg_name;
  }
}

/*
 * function to create a unique path for a transactor if more
 * that one simulation is running in the same executable.
 * This function was created specificly for Carbon so that you
 * can have several different Carbon Models instantiated in the
 * same simulation.
 * Returns the new string.
 */
char *tbp_gen_unique_handle(const void *obj_ptr, const char *str)
{
  static char new_path[1024];
#if pfLP64
  sprintf(new_path, "$vhm%lx:%s",(u_int64)obj_ptr,str);
#else
  sprintf(new_path, "$vhm%x:%s",(u_int32)obj_ptr,str);
#endif
  return new_path;
}

/* 
 *  Return a trans_id given a substring of a full hierarchical path
 *  This is for the user's convenience, since by specifying a minimum
 *  of the full string, changes in the rest of the hierarchy will
 *  not force these strings to be changed as well.
 * Returns:
 * a. TBP_PATH_INVALID  -1
 * b. TBP_PATH_DUPLICATE -2  !!! misnomer: non-unique
 * c. id associated with the unique path
 *
         jjh -- 2005/07/07 -- Add back the check for exact match
         If there are two transactors named:
           a.b.foo
           a.b.foo_1
         and path is passed in as "b.foo", then a duplicate match will
         be returned and a.b.foo could never be properly matched.
 */
int tbpi_check_path_name(const char *path, FFL_ARGS)
{
  (void) func; (void) line; (void) file;
  TBP_CONF_T *tc=tbp_get_conf();
  u_int32 match_count = 0, match_index = 0;
  u_int32 i,j;
  char *path_match, *new_path_match;
  /* walk through the trans table looking for an exact match */
  for(i=0;i<tc->trans_cnt;i++){ 
    /* is it in there at all? */
    if(NULL!=(path_match=strstr(tc->trans[i].full_name,path))){
      /* is it an exact match? */
      if(strcmp(path_match,path)==0) {
	if(match_count==0)
	  match_index = i;
	match_count +=1;
      }
      if(match_count > 1) {
	tbp_info("Duplicate or ambiguous path found  for %s (%s - %s)\n",
		 path,tc->trans[match_index].full_name,
		 tc->trans[i].full_name);
	return TBP_PATH_DUPLICATE;
      }
    }
  }
  if(match_count==1) 
    return match_index;
  match_count = match_index = 0;
  /* if we have not returned - look for a unique string */
  for(i=0;i<tc->trans_cnt;i++){ 
    /* is it in there at all? */
    if(NULL!=(path_match=strstr(tc->trans[i].full_name,path))){
      /* if so walk through all of the other strings looking for a match */
      for(j=0;j<tc->trans_cnt;j++){
	if(NULL!=(new_path_match=strstr(tc->trans[j].full_name,path_match))){
	  if(match_count==0)
	    match_index = j;
	  match_count +=1;
	}
	if(match_count > 1) {
	  tbp_info("Duplicate or ambiguous path found  for %s (%s - %s)\n",
		   path,tc->trans[match_index].full_name,
		   tc->trans[j].full_name);
	  return TBP_PATH_DUPLICATE;
	}
      }
    }
  }
  return match_count==1 ? (int)match_index : TBP_PATH_INVALID;
}


/*
 *  This function returns a trans_id similarly to the above, except it
 *  uses the current reg_name of the trans as the substring.
 *  This is called by the record_presence_carbon routine; the attaches
 *  may have already caused the trans to be recorded with just a substring
 */
int tbp_get_trans_id_attached(char *path)
{
  TBP_CONF_T *tc=tbp_get_conf();
  int match_index = TBP_PATH_INVALID;
  int match_dup   = FALSE;
  u_int32 i;

  for(i=0; i<tc->trans_cnt; i++) { 
    if(strstr(path, tc->trans[i].full_name)) {
      if(match_index == TBP_PATH_INVALID) {  // have I already recorded a match?
	match_index = i;  // First match is OK
      } else {
	tbp_info("Duplicate or ambiguous path found  for %s (%s - %s)\n",
		 path, tc->trans[match_index].full_name,
		 tc->trans[i].full_name);
	match_dup = TRUE;  // need extra flag just to be able to print msgs
      }
    }
  }
  if (match_dup)
    return TBP_PATH_DUPLICATE;
  else
    return match_index;
}

/* function called from a C-thread to get the id of the attached transactor */
int tbp_get_my_trans_id(void)
{
  return tbp_get_current_thread()->trans_id;
}


//!!! I think this is a remnant.  called only from legacy_utils    
int tbp_get_trans_id(char *name)
{
  TBP_CONF_T *tc=tbp_get_conf();
  u_int32 id;
  char *path_name;

  /* 
   * if the argument is null, then we have to figure out what 
   * transactor called this function (via the simulation thread)
   * if we are in the simulation thread - then we can only find our
   * registered name by doing a lookup on the current path
   */
  if(name==NULL) {
    id = tbp_get_my_thread_id();
    if(id==tc->sim_thread_id){
      path_name = tbp_get_current_module_name(NULL);
      /* this function returns the transactor id if found and -1 if not */
      return tbp_check_path_name(path_name);
    }
    else if(id==tc->main_thread_id){
      return -1;
    }
    else {
      return tbp_get_thread(id)->trans_id;
    }
  } else {
    /* this function returns the transactor id if found and -1 if not */
    return tbp_check_path_name(name);
  }
}

/*
 * function which waits for the simulation thread (sleeping the calling thread)
 */
void tbp_wait_for_sim(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_TRANS_T *trans;
  int calling_thread_id = tbp_get_my_thread_id();
  int target_thread_id;
  TBP_THREAD_T *thread = tbp_get_thread(calling_thread_id);
  BOOL qtsp_modified =  tbp_get_qtsp_state();
  
  /* make sure there is a valid trans struct to check */
  if(thread->type==TBP_THREAD_TYPE_USER){
    trans = &tc->trans[tbp_get_thread(calling_thread_id)->trans_id];
    /* check for cmodels which return to a cmodel thread not the sim */
    if(trans->type==TBP_TRANS_TYPE_CMODEL) {
      target_thread_id = trans->ctrans_id;
    }
    else {
      target_thread_id = tc->sim_thread_id;
    }
  }
  else {
    /* default behavior - wakeup the simulation */
    target_thread_id = tc->sim_thread_id;
  }

  /* wakeup the sim thread or in the case of cmodels, the attached transactor */
  tbp_thread_activate(calling_thread_id,target_thread_id);
  /* 
   * Conditionally restore the global QTSP * when returning
   * but we only do so if we are being used by 3x code to avoid the
   * performance penalty it imposes...
   */
  if(!tbp_is_sim() && !tbp_is_main() && (qtsp_modified==TRUE))
     tbp_attach_buffer();
}

/*
 * function to "reset" all registered transactors
 */
void tbpi_reset_transactors(FFL_ARGS)
{
  tbpi_sim_reset_transactors(iFFL_DATA);
}

/* print tbp banner as we startup */
void tbp_print_banner(void)
{
  tbp_printf("\n**********************************************************\n");
  tbp_printf("   Carbon Transactor Library Version %d.%d%s (gcc-%d.%d)\n",
	     TBP_MAJOR,TBP_MINOR,TBP_PATCH,__GNUC__,__GNUC_MINOR__);
  tbp_printf("   Copyright 2006-2007 Carbon Design Systems (%s)\n",TBP_BUILD_DATE);
  tbp_printf("**********************************************************\n");
}

static void sig_handler(int sig) { (void) sig; return; }

/*
 * function to issue a signal to facilitate debug of tbp/test as
 * a shared (dynamic link) library
 */
void tbp_dll_debug(void)
{
  TBP_CONF_T *tc=tbp_get_conf();

  if(tc->dll_debug) {
    tbp_printf("\n" 
	       "**********************************************************\n"
	       "**** Issuing a signal to stop the debugger with test  ****\n"
	       "**** symbols now loaded from the dynamic library.     ****\n"
	       "**** After you have set initial break-points and/or   ****\n"
	       "**** examined the executable, continue using the      ****\n"
	       "**** debugger's \"continue\" or \"c\" command.            ****\n"
	       "**********************************************************\n\n");

    /* clear out any handlers installed elsewhere for this signal */
    (void) sig_handler;
    //signal(SIGUSR1,sig_handler);
    /* raise the signal */
    //raise(SIGUSR1);
  }
}

/* 
 * just as it says - report error status and initiate a pli shutdown
 */
void tbp_shutdown(int code)
{
  TBP_CONF_T *tc=tbp_get_conf();
  
  /* store the exit state and report status on the first call */
  if(tc->exit_flag==FALSE){  
    tbp_util_debug("shutdown called from thread %d (exit flag = %d exit code %d)\n",
 		   tbp_get_my_thread_id(),tc->exit_flag,tc->exit_code);
    tbp_print_error_status();
    tc->exit_code = code;
    tc->exit_flag = TRUE;  
  }
  
  /* exit if we are in the sim thread - otherwise - get to the sim-thread */
  if(tbp_is_sim()==TRUE) {
    if(tc->override_exit==TRUE)
      tbp_sim_finish();
    else {
      exit(tc->exit_code);
    }
  }
  else {
    tbpi_thread_kill(tc->sim_thread_id,FFL_DATA);
    tbp_thread_activate(tbp_get_my_thread_id(),tc->sim_thread_id);
  }

  tbp_evt_signal(tbp_get_test_complete_event());
}

/* function used to shutdown the C->SIM communication mechanism if needed */
int tbp_finish(void)
{
#ifdef DUAL_CSIDE
  tbp_sim_finish();
#endif
  /* in single process - this does not need to do anything - just return error_cnt */
  return tbp_get_error_count_all();
}

/* user callable stop function */
void tbp_stop(void)
{
  return tbp_sim_stop();
}

/* 
 * stand-in function for calls to exit() - which generaly prevent clean
 * simulator shutdown (waves/logs not flushed
 */
void tbpi_exit(int code, FFL_ARGS)
{
  tbp_util_debug("exit(%d) called from %s %s %d\n",code,file,func,line);
  tbp_shutdown(code);
}  

/*
 * function called by OS exit handler - one last chance to exit
 * gracefully
 */
static void tbpi_atexit(void)
{
  TBP_CONF_T *tc=tbp_get_conf();

  /* if no clean shutdown has been called - do so now */
  if(tc->exit_flag==FALSE) {
    tc->exit_code = 0;
    tc->exit_flag = TRUE;
    //tbp_print_error_status();
    tbp_sim_finish();  
  }
}  

/*
 * Function to assign a pointer to the test's "main" function
 */
void tbp_set_test_main(void)
{
  TBP_CONF_T *tc=tbp_get_conf();

#ifdef DYN_TEST
  void *test_handle;
  char *error;
  
  if(tc->test_lib_name!=NULL){
    if(NULL==(test_handle = dlopen(tc->test_lib_name, RTLD_NOW | RTLD_GLOBAL)))
      tbp_panic("TBP: Failed to open the test object (%s):\n\t%s\n",
		tc->test_lib_name,dlerror());
  }
  else {
    /* allow static link - map tbp_main from inside the "current" object */
    test_handle = (void *)NULL;
  }

  /* atempt to map tbp_main */
  tc->tbp_main_func = dlsym(test_handle,"tbp_main");
  
  /* check for an error */
  if(NULL != (error = dlerror())){
    tbp_panic("TBP: Failed to map the test's 'main':\n"
	      "\t%s\n\n"
	       "\tYou must either statically link an object holding 'tbp_main' or\n"
	      "\tuse '+tbptestlib=<filename>' to specify an object to be loaded\n",
	      error!=NULL ? error : "symbol not found");
  }
#else
  extern int tbp_main(int argc, char *argv[]);
  tc->tbp_main_func = tbp_main;
  //tc->tbp_main_func = (void *)dlsym((void*)NULL,"tbp_main");
#endif

  /* check to make sure we have a main! */
  if(tc->tbp_main_func==NULL)
    tbp_panic("TBP: No test main function found! - cannot continue\n");
}

/*
 *  Just for carbon
 */
int bogus_main_func(int argc, char *argv[]) {
  (void) argc; (void) argv;
  //  This simply ensures that the main thread never shows itself!
  while (1) tbpi_evt_waittest("Bogus wait", 0LL, FFL_DATA);

  return 0;
}

/*
 * Main initialization function called once registration is complete
 */
void tbp_init(int caller)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread;

  if(g_tbp_initialized==FALSE) {
    g_tbp_initialized = TRUE;
    
    /* add cleanup function to exit stack */
    atexit(tbpi_atexit);
    
    tbp_util_debug("**** TBP INIT ****\n");
    
    /* initialize the configuration */
    tbp_init_config(caller);

    /* start the license */
    tbp_start_license();

    /* assign the test's "main" pointer - check for dynamic mode */
    tbp_set_test_main();
    
    /* check for shared-lib debug mode */
    tbp_dll_debug();
    
    /* initialize legacy functions */
    tbp_legacy_init();
    
    /* create the sim thread structures */
    tc->sim_thread_id = tbp_thread_init();
    
    /* we need to create the main thread primarily to make the timeout code work */
    tc->main_thread_id = tbp_thread_init();
    
    /* assign the "user space main" function as the associated function */
    thread = tbp_get_thread(tc->main_thread_id);
    thread->type = TBP_THREAD_TYPE_MAIN;
#ifdef TBP_CDS
    thread->func_ptr = (void (*)())bogus_main_func;
#else
    thread->func_ptr = (void *)tc->tbp_main_func;
#endif
    thread->num_args = 2;
#if pfLP64
    thread->usr_ptr = (void *)tbp_allocate_buffer(sizeof(u_int64)*2,"main argv/argc");
    thread->usr_ptr[0] = (u_int64) tc->main_argc;
    thread->usr_ptr[1] = (u_int64) tc->main_argv;
#else
    thread->usr_ptr = (void *)tbp_allocate_buffer(sizeof(u_int32)*2,"main argv/argc");
    thread->usr_ptr[0] = tc->main_argc;
    thread->usr_ptr[1] = (u_int32) tc->main_argv;
#endif

#ifdef DUAL_PROC
    if(tbpi_get_transport_mode()==TBP_TRANSPORT_MODE_DUAL) {
      /* assign the pipe wakeup function */
      tbp_set_wakeup_func(tbp_pipe_wakeup);

      if(caller == TBP_SIMSIDE) { 
	/* caller defines point to pipe master/slave, so just pass it through */
	tbp_pipe_init(caller,tc->ident_string);

	/* send the current sim-time - for initialization purposes */
	tbpi_pipe_send_cmd(FFL_DATA, TBP_PIPE_SEND_SIMTIME);
	
	/* send the trans table to get sim/c in synch */
	tbpi_pipe_send_cmd(FFL_DATA, TBP_PIPE_SEND_TRANS_TABLE);
	
	/* now wait for the C-side to finish its configuration */
	tbpi_pipe_get_cmd(FFL_DATA);

	/* init is done - allow the sim to run */
	return;
      }
      else {
	/* 
	 * special mode flag that may go away - delay the pipe connection
	 * to allow C-side configuration prior to connection 
	 */
	if(tc->pipe_manual==FALSE) {
	  /* caller defines point to pipe master/slave, so just pass it through */
	  tbp_pipe_init(caller,tc->ident_string);
	  /* wait for the simside to send its configuration */
	  tbpi_pipe_get_cmd(FFL_DATA);
	}
      }
    }
#endif

#ifndef DUAL_SIMSIDE
    /* create the "main" thread'*/
    tbp_thread_create(tc->main_thread_id);
    
    /* activate the main thread */
    tbp_thread_activate(tc->sim_thread_id,tc->main_thread_id);
#endif    
  }
  return;
}

/*
 * function to check and adjust, if needed, the size of the 
 * read/write data buffers associated with each transactor
 */
void tbpi_set_buffer_size(u_int32 trans_id, u_int32 new_size, FFL_ARGS)
{
  (void) func; (void) line; (void) file;
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_OP_T *operation = &tc->trans[trans_id].operation;
  u_int32 orig_size = operation->buffer_size;

  if(new_size==0) {
    tbp_util_debug("tbp_set_buffer_size: 0 byte read/write buffer size requested\n");
    return;
  }

  if(new_size > operation->buffer_size) {
    tbp_util_debug("tbp_set_buffer_size: trans_id = %d old %d new %d\n",trans_id, operation->buffer_size, new_size);
    /* maintain 64bit alignment on this size - add 16 bytes for tbp3x compatibility */
    operation->buffer_size = 16 + (((new_size+7)/8)*8);
    operation->write_data =  (void *)tbp_realloc(operation->write_data, orig_size, operation->buffer_size);
    operation->read_data =   (void *)tbp_realloc(operation->read_data, orig_size, operation->buffer_size);
    operation->write_datax = (void *)tbp_realloc(operation->write_datax, orig_size, operation->buffer_size);
    operation->read_datax =  (void *)tbp_realloc(operation->read_datax, orig_size, operation->buffer_size);

    // Clear Buffers
    memset(operation->write_data,  0, operation->buffer_size);
    memset(operation->read_data,   0, operation->buffer_size);
    memset(operation->write_datax, 0, operation->buffer_size);
    memset(operation->read_datax,  0, operation->buffer_size);
  }  
  
  /* check the read/write pointers */
  if(operation->write_data==NULL || operation->read_data==NULL || 
     operation->write_datax==NULL || operation->read_datax==NULL){
    tbp_error_internal("tbp_set_buffer_size: read/write buffer is invalid (NULL) - cannot continue\n");
  }

}

/* 
 * keep track of all calling modules to find out when rgistration is complete 
 */
void tbpi_record_module_presence(char *full_name, FFL_ARGS)
{
  (void) func; (void) line; (void) file;
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_TRANS_T *trans;
  TBP_OP_T *operation;
 
  /* 
   * if it is not a duplicate, then add it to the new list and increment the count 
   */
  //  tbp_printf("tbp_record_module_presence: %s\n", full_name);
  if(tbp_get_trans_id_full(full_name)==-1){

    /* create a new space in the list for this module */
    if(NULL==(tc->trans=(TBP_TRANS_T *)tbp_realloc(tc->trans,sizeof(TBP_TRANS_T)*(tc->trans_cnt), sizeof(TBP_TRANS_T)*(tc->trans_cnt+1)))){
      tbp_panic_internal("tbp_record_module_presence: Could not allocate space in the trans list\n");
    }
  
    /* initialize this entry */
    trans = &tc->trans[tc->trans_cnt];
    operation = &tc->trans[tc->trans_cnt].operation;
    
    /* clear out the operation struct associated with this transactor */
    operation->opcode      = TBP_NOP;
    operation->address     = 0;
    operation->read_data   = NULL;
    operation->write_data  = NULL;
    operation->read_datax  = NULL;
    operation->write_datax = NULL;
    operation->buffer_size = 0;
    operation->size        = 0;
    operation->status      = 0;
    operation->interrupt   = 0;
    operation->reset       = 0;
    operation->idle_cnt    = 0;
    operation->direction   = 0;
    operation->width       = TBP_WIDTH_32;
    operation->starttime   = 0LL;
    operation->endtime     = 0LL;
 
    /* clear out the trans struct */
    trans->id = 0;
    trans->registered = FALSE;
    trans->attached = FALSE;
    trans->thread_id = 0;
    trans->ctrans_id = 0;
    trans->interrupt_enable = TRUE;
    trans->reg_name = NULL;
    /* create space for the module name */
    trans->full_name = tbp_malloc(strlen(full_name)+1);
    memcpy(trans->full_name, full_name, strlen(full_name)+1);
    
    tbp_util_debug("tbp_record_module_presence: New transactor added (%s) total count = %d\n",
		   trans->full_name,tc->trans_cnt+1);
    tc->trans_cnt +=1; // increment the counter
  }
}


/*
 *  This version checks for a previously recorded version from a
 *  user attach call.....
 */
void tbp_record_module_presence_carbon(char *full_name) {
  TBP_CONF_T  *tc=tbp_get_conf();
  TBP_TRANS_T *trans;
  int trans_id;

  tbp_util_debug("tbp_record_module_presence_carbon: %s\n", full_name);
  if ((trans_id = tbp_get_trans_id_attached(full_name)) < 0) {
    // No previous record of this module
    tbp_record_module_presence(full_name);
  } else {
    // This module was previously recorded by an attach
    trans = &tc->trans[tc->trans_cnt];
    // Check for an ambiguous reg_name: if there is already
    // a full_name different from the reg_name we have a problem!
    if (strcmp(trans->full_name, trans->reg_name)) {
      tbp_error("Ambiguous pathname used on an Attach: %s  -- (%s vs %s)\n",
			 trans->reg_name, trans->full_name, full_name);
    } else {
      // Replace the previously recorded short path with the full path
      trans->full_name = tbp_malloc(strlen(full_name)+1);
      memcpy(trans->full_name, full_name, strlen(full_name)+1);
    }
  }
}

/*
 * function to remove a registered module from the list (primarily cmodel)
 */
void tbpi_remove_module(u_int32 trans_id, FFL_ARGS)
{
  (void) func; (void) line; (void) file;
  TBP_CONF_T *tc=tbp_get_conf();
  
  if(tbp_is_main()==0 && tbp_is_sim()==0){
    tbp_error_internal("This function cannot be called outside of the main thread\n");
    return;
  }
  
  /* Remove this transactor from the list and collapse the list */
  /* mark this transactor as invalid */
  tc->trans[trans_id].type = 0;
  tc->trans[trans_id].id = 0;
  tc->trans[trans_id].registered = FALSE;
  tc->trans[trans_id].attached = FALSE;
  
  /* free any memory associated with this transactor */
  tbp_free(tc->trans[trans_id].full_name);
  tbp_free(tc->trans[trans_id].reg_name);
  tbp_free(tc->trans[trans_id].operation.read_data);
  tbp_free(tc->trans[trans_id].operation.read_datax);
  tbp_free(tc->trans[trans_id].operation.write_data);
  tbp_free(tc->trans[trans_id].operation.write_datax);
  tc->trans_cnt -= 1;    
}

/* function used to return the current transactor table */
TBP_TRANS_T *tbp_get_trans_table(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  return tc->trans;
}

/*
 * function to used to list the available (registered) transactors
 */
void tbp_list_avail_trans(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  u_int32 i;
  for(i=0;i<tc->trans_cnt;i++)
    tbp_info("trans[%d] = %s\n",i,tc->trans[i].full_name);
}

/* 
 * Function which actually records a given transactor as registered
 * and returns its "id"
 */
int tbpi_register(FFL_ARGS, int trans_type, char *full_name, ...)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_TRANS_T *trans;
  va_list ap;
  u_int32 trans_id,i;
  void (*cmodel_func);
  void *cmodel_usr_ptr;
  int index;
  
  /* cmodel transactors must mimmic the behavior of the HDL transactors */
  if(trans_type == TBP_TRANS_TYPE_CMODEL)
    tbp_record_module_presence(full_name); // hdl does this at EOC (end of compile)
  
  /* now get the index - new or duplicate */
  index = tbp_get_trans_id_full(full_name);

  /* check to make sure this transactor has reported its presence */
  if(index==-1) {
    tbp_info("tbp_register: transactor is not correctly configured"
	     " (does not exist in presence table)\n");
    
    tbp_util_debug("dumping trans_table:\n");
    for(i=0;i<tc->trans_cnt;i++)
      tbp_util_debug("trans[%d] - %s\n",i,tc->trans[i].full_name);
  
    tbp_panic_internal("invalid use of tbp_register\n");
  }

  /* check for duplicate registeration return current id */
  if(tc->trans[index].registered==TRUE){
    tbp_util_debug("Duplicate registration call for %s\n",
		   tc->trans[index].full_name);
    return tc->trans[index].id;
  }

  /* if new, then initialize its struct and operation */
  trans_id = (u_int32)index;
  trans = &tc->trans[trans_id];

  /* record that this transactor has registered */
  trans->registered = TRUE;
  trans->type = trans_type;
  trans->id = trans_id;

  /* reset/initialize the rest of the trans struct */
  tbpi_init_trans_struct(trans_id, iFFL_DATA);
  

  if(trans_type == TBP_TRANS_TYPE_CMODEL) {
    /* if this is a cmodel transactor, then we need to spawn a thread */
    va_start(ap, full_name);  
    cmodel_func = va_arg(ap, void *); /* fetch the function pointer */
    cmodel_usr_ptr = va_arg(ap, void *); /* fetch the user pointer */
    va_end(ap);
    
    /* create the new cmodel thread and store this info in the trans struct */
    trans->ctrans_id = tbpi_cmodel_thread_create(trans_id, cmodel_func,cmodel_usr_ptr,iFFL_DATA);
  }
 
  return trans_id;
}

/*
 * funtion to check that all present transactors have registered 
 */
BOOL tbp_check_all_registered(void)
{
  u_int32 i;
  TBP_CONF_T *tc=tbp_get_conf();

  for(i=0;i<tc->trans_cnt;i++){
    if(tc->trans[i].registered==FALSE) {
      return FALSE;
    }
  }
 
  /* if we get through the list without returning - all must be registered */
  return TRUE;
}

/* increment the attach count for trans or ctrans as needed */
void tbp_inc_attach_cnt(u_int32 trans_id)
{
  TBP_CONF_T *tc=tbp_get_conf();
  if(tc->trans[trans_id].type==TBP_TRANS_TYPE_CMODEL)
    tc->ctrans_attach_cnt += 1;
  else
    tc->trans_attach_cnt += 1;
}

/* decrement the attach count for trans or ctrans as needed */
void tbp_dec_attach_cnt(u_int32 trans_id)
{
  TBP_CONF_T *tc=tbp_get_conf();
  if(tc->trans[trans_id].type==TBP_TRANS_TYPE_CMODEL)
    tc->ctrans_attach_cnt -= tc->ctrans_attach_cnt > 0 ? 1 : 0;
  else
    tc->trans_attach_cnt -= tc->trans_attach_cnt > 0 ? 1 : 0;
}

/* 
 * return a pointer to the current operation to allow user-space
 * replacement/customization of the transaction layer
 */
TBP_OP_T *tbp_get_operation(void)
{
  TBP_CONF_T *tc=tbp_get_conf();
  int trans_id;

  if(tc->current_thread < 2) {
    return NULL;
  }
  else {
    trans_id = tbp_get_my_trans_id();    
    return &tc->trans[trans_id].operation;
  }
}

/* 
 * set the pointer to the current inst_data
 */
void tbpi_set_inst_data(void * ptr, FFL_ARGS) 
{
  (void) func; (void) line; (void) file;
  TBP_CONF_T *tc=tbp_get_conf();
  int trans_id;

  if(tc->current_thread < 2) {
    tbp_error_internal("Attempting to set inst_data pointer while not attached to a transactor\n");
  }
  else {
    trans_id = tbp_get_my_trans_id();    
    tc->trans[trans_id].inst_data = ptr;
  }
}


/* 
 * return a pointer to the current inst_data
 */
void *tbpi_get_inst_data(FFL_ARGS)
{
  (void) func; (void) line; (void) file;
  TBP_CONF_T *tc=tbp_get_conf();
  int trans_id;

  if(tc->current_thread < 2) {
    return NULL;
  }
  else {
    trans_id = tbp_get_my_trans_id();    
    return tc->trans[trans_id].inst_data;
  }
}

/* function to reset the the state of a transactor to pre-attachment */
void tbpi_clear_trans_struct(int trans_id, FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_TRANS_T *trans =  &tc->trans[trans_id];
  TBP_OP_T *operation = &tc->trans[trans_id].operation;

  if (trans->inst_data != NULL) {
    tbp_free(trans->inst_data);
    trans->inst_data = NULL;
  }

  trans->id   = trans_id;
  trans->thread_id = 0;
  trans->ctrans_id = 0;
  trans->registered = TRUE;
  trans->attached = FALSE;
  trans->interrupt_enable = TRUE;
  trans->int_state = TBP_INT_NONE;
  trans->rst_state = TBP_RST_NONE;

  tbp_free(trans->reg_name);

  if(operation->buffer_size!=0){
    tbp_free(operation->read_data);
    tbp_free(operation->write_data);
    tbp_free(operation->read_datax);
    tbp_free(operation->write_datax);
    operation->buffer_size = 0;
  }
  
  /* reset the default buffers.. */
  tbpi_set_buffer_size(trans_id, tc->default_buffer_size, FFL_DATA);

  /* set the operation to NOP to prevent spurious task calls on the HDL side */
  operation->opcode = TBP_NOP;
  operation->address = 0;
  operation->size = 0;
  operation->status = 0;
  operation->interrupt = 0;
  operation->reset     = 0;
  operation->starttime = 0;
  operation->endtime = 0;
  operation->idle_cnt = 0;
  operation->direction = TBP_OP_BIDIR;
  operation->width = TBP_WIDTH_32;

#ifdef DUAL_CSIDE
  if(trans->type!=TBP_TRANS_TYPE_CMODEL)
    tbpi_pipe_send_cmd(iFFL_DATA,TBP_PIPE_DETACH_TRANS,trans_id);
#else
  (void) func; (void) line; (void) file;
#endif
  
}

/* function to initialize the the state of a transactor to pre-attachment */
void tbpi_init_trans_struct(int trans_id, FFL_ARGS)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_TRANS_T *trans =  &tc->trans[trans_id];
  TBP_OP_T *operation = &tc->trans[trans_id].operation;
  u_int32 buffer_size;

  trans->id   = trans_id;
  trans->thread_id = 0;
  trans->ctrans_id = 0;
  trans->registered = TRUE;
  trans->attached = FALSE;
  trans->interrupt_enable = TRUE;
  trans->int_state = TBP_INT_NONE;
  trans->rst_state = TBP_RST_NONE;
  trans->inst_data = NULL;
  trans->cs_get_data = (u_int32 *)tbp_malloc(64 * sizeof(u_int32));
  trans->cs_put_data = (u_int32 *)tbp_malloc(64 * sizeof(u_int32));
  memset(trans->cs_get_data, 0, 64*sizeof(u_int32));
  memset(trans->cs_put_data, 0, 64*sizeof(u_int32));
  tbp_free(trans->reg_name);

  /* reset/init the default buffers.. (calloc to zero them out) */
  buffer_size = tc->default_buffer_size;
  /* maintain 64bit alignment on this size - add 16 bytes for tbp3x compatibility */
  operation->buffer_size = 16 + (((tc->default_buffer_size+7)/8) * 8);
  operation->write_data =  (void *)tbp_calloc(operation->buffer_size,1);
  operation->read_data =   (void *)tbp_calloc(operation->buffer_size,1);
  operation->write_datax = (void *)tbp_calloc(operation->buffer_size,1);
  operation->read_datax =  (void *)tbp_calloc(operation->buffer_size,1);
  
  /* check the read/write pointers */
  if(operation->write_data==NULL || operation->read_data==NULL || 
     operation->write_datax==NULL || operation->read_datax==NULL){
    tbp_error_internal("tbp_set_buffer_size: read/write buffer is invalid (NULL) - cannot continue\n");
  }

  /* set the operation to NOP to prevent spurious task calls on the HDL side */
  operation->opcode = TBP_NOP;
  operation->address = 0;
  operation->size = 0;
  operation->status = 0;
  operation->interrupt = 0;
  operation->reset     = 0;
  operation->starttime = 0;
  operation->endtime = 0;
  operation->idle_cnt = 0;
  operation->direction = TBP_OP_BIDIR;
  operation->width = TBP_WIDTH_32;

#ifdef DUAL_CSIDE
  if(trans->type!=TBP_TRANS_TYPE_CMODEL)
    tbpi_pipe_send_cmd(iFFL_DATA,TBP_PIPE_DETACH_TRANS,trans_id);
#else
  (void) func; (void) line; (void) file;
#endif
  
}

/* fuction used internally to cut down on typing when allocating a buffer */
void *tbpi_allocate_buffer(int size, char *err_string, FFL_ARGS)
{
  void *ptr;

  /* check for mis-use */
  if(size < 1)
    return NULL;
  else {
    if(NULL==(ptr=(void *)tbpi_calloc(size,1,iFFL_DATA))){
      tbp_panic_internal("INTERNAL: Failed to allocate space [%s] - at file: %s func: %s line: %d\n",
		err_string,file,func,line);
      return NULL;
    }
    else {
      memset(ptr, 0, size);
      return ptr;
    }
  }
}

/* function to block signals */
void tbp_sighold(int sig)
{
  (void) sig;
  // FIXME! need a sighold function....
}

/* functions to set/get the simulation ratio for emu mode */
void tbp_set_sim_ratio(int ratio) {
  TBP_CONF_T *tc=tbp_get_conf();
  tc->simratio = ratio;
  tbp_printf("Setting emulation time ratio to %d\n", ratio);
}

int tbp_get_sim_ratio() { return tbp_get_conf()->simratio; }

/* set/get functions - conditional "sleep" for tbp3x compatibility */
int tbpi_set_value(char *path, void *value, int type, FFL_ARGS)
{ 
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  return tbpi_sim_set_value(path,value,type,TBP_SET_NORMAL,NULL,iFFL_DATA); 
}

int tbpi_set_value_int(char *path, int value, FFL_ARGS)
{
  int int_value = value;
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  return tbpi_sim_set_value(path,(void *)&int_value,TBP_SET_INT,TBP_SET_NORMAL,NULL,iFFL_DATA);  
}

int tbpi_set_value_bin(char *path, char *value, FFL_ARGS)
{
  char *char_value = value;
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  return tbpi_sim_set_value(path,(void *)char_value,TBP_SET_BIN,TBP_SET_NORMAL,NULL,iFFL_DATA);  
}

int tbpi_set_value_hex(char *path, char *value, FFL_ARGS)
{
  char *char_value = value;
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  return tbpi_sim_set_value(path,(void *)char_value,TBP_SET_HEX,TBP_SET_NORMAL,NULL,iFFL_DATA);  
}

int tbpi_set_value_real(char *path, double value, FFL_ARGS)
{
  double real_value = value;
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  return tbpi_sim_set_value(path,(void *)&real_value,TBP_SET_REAL,TBP_SET_NORMAL,NULL,iFFL_DATA);  
}
/* get */
void *tbpi_get_value(char *path, int type, FFL_ARGS) 
{ 
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  return (void*)tbpi_sim_get_value(path,type,NULL,iFFL_DATA); 
}

int tbpi_get_value_int(char *path, FFL_ARGS) 
{ 
  int *int_value;
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  int_value = (int *)tbpi_sim_get_value(path,TBP_GET_INT,NULL,iFFL_DATA);
  return *int_value; 
}

char *tbpi_get_value_bin(char *path, FFL_ARGS) 
{ 
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  return (char *)tbpi_sim_get_value(path,TBP_GET_BIN,NULL,iFFL_DATA);
}

char *tbpi_get_value_hex(char *path, FFL_ARGS) 
{ 
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  return (char *)tbpi_sim_get_value(path,TBP_GET_HEX,NULL,iFFL_DATA);
}

int tbpi_get_value_real(char *path, FFL_ARGS) 
{ 
  double *real_value;
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  real_value = (double *)tbpi_sim_get_value(path,TBP_GET_REAL,NULL,iFFL_DATA);
  return *real_value; 
}
/* force */
int tbpi_force_value(char *path, void *value, int type, FFL_ARGS)
{ 
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  return tbpi_sim_set_value(path,value,type,TBP_SET_FORCE,NULL,iFFL_DATA); 
}

int tbpi_force_value_int(char *path, int value, FFL_ARGS)
{
  int int_value = value;
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  return tbpi_sim_set_value(path,(void *)&int_value,TBP_SET_INT,TBP_SET_FORCE,NULL,iFFL_DATA);  
}

int tbpi_force_value_bin(char *path, char *value, FFL_ARGS)
{
  char *char_value = value;
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  return tbpi_sim_set_value(path,(void *)char_value,TBP_SET_BIN,TBP_SET_FORCE,NULL,iFFL_DATA);  
}

int tbpi_force_value_hex(char *path, char *value, FFL_ARGS)
{
  char *char_value = value;
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  return tbpi_sim_set_value(path,(void *)char_value,TBP_SET_HEX,TBP_SET_FORCE,NULL,iFFL_DATA);  
}

int tbpi_force_value_real(char *path, double value, FFL_ARGS)
{
  double real_value = value;
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  return tbpi_sim_set_value(path,(void *)&real_value,TBP_SET_REAL,TBP_SET_FORCE,NULL,iFFL_DATA);  
}

int tbpi_release_value(char *path, FFL_ARGS)
{ 
  char dummy_value[4096];
  if(tbp_get_conf()->setget_sleep==TRUE) tbpi_sim_sleep(1,iFFL_DATA);
  return tbpi_sim_set_value(path,dummy_value,TBP_SET_BIN,TBP_SET_RELEASE,NULL,iFFL_DATA); 
}

/* function used in dual process mode to manually connect the pipes */
int tbpi_transport_start(FFL_ARGS)
{
#ifdef DUAL_CSIDE
  TBP_CONF_T *tc=tbp_get_conf();
  if(tc->pipe_manual==TRUE){
    /* caller defines point to pipe master/slave, so just pass it through */
    tbp_pipe_init(TBP_CSIDE,tc->ident_string);
    /* wait for the simside to send its configuration */
    tbpi_pipe_get_cmd(iFFL_DATA);
  }
#else
  (void) func; (void) line; (void) file;
#endif
  
  return 1;
}

/* functions to retrieve arguments from the command line */
BOOL tbp_get_argv_key_present(char *key)
{
  TBP_CONF_T *tc=tbp_get_conf();
  BOOL ret = FALSE;
  int i;

  /* check both the sim argv and the main_argv */
  ret = tbp_sim_get_argv_key_present(key);

  if(tc->main_argc!=0){
    for(i=0;i<tc->main_argc;i++){
      /* check for leading - or + and ignore it */
      if((tc->main_argv[i][0]=='-') || (tc->main_argv[i][0]=='+')){
	if(strcmp((tc->main_argv[i]+1),key)==0)
	  ret |= TRUE;
      }
      else {
	if(strcmp(tc->main_argv[i],key)==0)
	  ret |= TRUE;
      }
	
    }
  }
  return ret;
}

char *tbp_get_argv_key(char *key)
{
  TBP_CONF_T *tc=tbp_get_conf();
  char *ret = NULL;
  int i;

  /* check both the sim argv and the main_argv */
  if(NULL==(ret = tbp_sim_get_argv_key(key))){
    if(tc->main_argc!=0){
      for(i=0;i<tc->main_argc-1;i++){
	if((tc->main_argv[i][0]=='-') || (tc->main_argv[i][0]=='+')){
	  if(strcmp((tc->main_argv[i]+1),key)==0)
	    return tc->main_argv[i+1];
	}
	else {
	  if(strcmp((tc->main_argv[i]),key)==0)
	    return tc->main_argv[i+1];
	}
      }
    }
  }
  
  return ret;
}
  

/* 
 * clean and clear are distinc for shutdown cleanlyness:
 * tbp_clean removes known resources tracked internally
 * tbp_clear removes any resources associated with a 
 * given ident which may or may not be the one we own.
 * tbp_clear allocates memory to do so and is therfore
 * inappropriate during panic shutdowns wheras tbp_clean
 * is less likely to fail (or will fail silently)
 */
void tbpi_clean(FFL_ARGS, char *ident_string)
{
  (void) func; (void) line; (void) file;
  (void) ident_string;
#ifdef DUAL_CSIDE
  tbp_pipe_clean();
#else
  return;
#endif
}

/* see above - brute force removal of ident files */
void tbpi_ipc_clear(FFL_ARGS, char *ident_string)
{
  (void) func; (void) line; (void) file;
#ifdef DUAL_CSIDE
  tbp_pipe_delete_ident(ident_string);
#else
  (void) ident_string;
  return;
#endif
}

/*
 * create a hash table
 */






