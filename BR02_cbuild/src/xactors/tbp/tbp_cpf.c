//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "tbp_internal.h"

// NOTE - some more comments here might be nice

/* 
 * those functions which are identical to their sim coutnerparts are provided
 * for link/define purposes (ex: #define my_cpf_func tbpi_cpf_*)
 */
void tbpi_cpf_write8(u_int64 addr, u_int8 data, FFL_ARGS)
{ tbpi_sim_write8(addr,data,iFFL_DATA); }

void tbpi_cpf_write16(u_int64 addr, u_int16 data, FFL_ARGS)
{ tbpi_sim_write16(addr,data,iFFL_DATA); }

void tbpi_cpf_write32(u_int64 addr, u_int32 data, FFL_ARGS)
{ tbpi_sim_write32(addr,data,iFFL_DATA); }

void tbpi_cpf_write64(u_int64 addr, u_int32 data_hi, u_int32 data_lo, FFL_ARGS)
{
  u_int64 write_data64 = ((u_int64)data_hi << 32) | ((u_int64) data_lo & 0x00000000ffffffffLL);

  tbpi_sim_write64(addr,write_data64,iFFL_DATA);
}

u_int8 tbpi_cpf_read8(u_int64 addr, FFL_ARGS)
{ return tbpi_sim_read8(addr,iFFL_DATA); }

u_int16 tbpi_cpf_read16(u_int64 addr, FFL_ARGS)
{ return tbpi_sim_read16(addr,iFFL_DATA); }

u_int32 tbpi_cpf_read32(u_int64 addr, FFL_ARGS)
{ return tbpi_sim_read32(addr,iFFL_DATA); }

void tbpi_cpf_read64(u_int64 addr, u_int32 *data_hi, u_int32 *data_lo, FFL_ARGS)
{
  u_int64 read_data;

  read_data = tbpi_sim_read64(addr,iFFL_DATA);

  *data_hi = (read_data >> 32 ) & 0x00000000ffffffffLL;
  *data_lo = read_data & 0x00000000ffffffffLL;
  return;
}

/* burst write */
u_int32 tbpi_cpf_write(u_int64 addr, u_int32 *data, u_int32 *size, FFL_ARGS)
{
  u_int32 status=0;
  u_int32 opcode = TBP_WRITE;

  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,data,NULL,size,&status,TBP_OP_WRITE,TBP_WIDTH_32,FFL_DATA);
  tbpi_check_transaction(TBP_OP_WRITE,0,iFFL_DATA);
  return *size;
}

/* burst read */
u_int32 tbpi_cpf_read(u_int64 addr, u_int32 *data, u_int32 *size, FFL_ARGS)
{
  u_int32 status=0;
  u_int32 opcode = TBP_READ;

  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,data,NULL,size,&status,TBP_OP_READ,TBP_WIDTH_32,FFL_DATA);
  tbpi_check_transaction(TBP_OP_READ,0,iFFL_DATA);
  return *size;
}

/* burst write zx */
u_int32 tbpi_cpf_write_zx(u_int64 addr, u_int32 *data, u_int32 *datax, u_int32 *size,
			  u_int32 *status, u_int32 smask, FFL_ARGS)
{
  u_int32 opcode = TBP_WRITE;

  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,data,datax,size,status,TBP_OP_WRITE,TBP_WIDTH_32,FFL_DATA);
  tbpi_check_transaction(TBP_OP_WRITE,smask,iFFL_DATA);
  return *size;
}

/* burst read zx */
u_int32 tbpi_cpf_read_zx(u_int64 addr, u_int32 *data, u_int32 *datax, u_int32 *size,
			 u_int32 *status, u_int32 smask, FFL_ARGS)
{
  u_int32 opcode = TBP_READ;

  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,data,datax,size,status,TBP_OP_READ,TBP_WIDTH_32,FFL_DATA);  
  tbpi_check_transaction(TBP_OP_WRITE,smask,iFFL_DATA); /* use write to avoid checking X/Z */
  return *size;
}

/* configuration space write */
void tbpi_cpf_cs_write(u_int64 addr, u_int32 data, FFL_ARGS)
{
  u_int32 status=0, size=4;
  u_int32 opcode = TBP_CS_WRITE;

  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,&data,NULL,&size,&status,TBP_OP_WRITE,TBP_WIDTH_32,FFL_DATA);
  tbpi_check_transaction(TBP_OP_WRITE,0,iFFL_DATA);
}

/* configuration space read */
u_int32 tbpi_cpf_cs_read(u_int64 addr, FFL_ARGS)
{
  u_int32 read_data;
  u_int32 status=0, size=4;
  u_int32 opcode = TBP_CS_READ;

  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr,&read_data,NULL,&size,&status,TBP_OP_READ,TBP_WIDTH_32,FFL_DATA);
  tbpi_check_transaction(TBP_OP_READ,0,iFFL_DATA);
  return read_data;
}

/* user defined operation */
void tbpi_cpf_userop(u_int32 opcode, u_int32 addr_hi, u_int32 addr_lo, u_int32 *data, u_int32 *datax, 
		     u_int32 *size, u_int32 *status, FFL_ARGS)
{
  (void)file; (void)func; (void)line;
  u_int64 addr64 = tbp_pack64(addr_hi,addr_lo);
  tbpi_transaction(tbp_get_my_trans_id(),&opcode,&addr64,data,datax,size,status,TBP_OP_BIDIR,TBP_WIDTH_32,FFL_DATA);
}

void tbpi_cpf_idle(u_int32 idle_cnt, FFL_ARGS)
{ tbpi_sim_idle(idle_cnt,iFFL_DATA); }


/* function used to assign an interrupt handler to a given thread id */
void tbpi_cpf_set_interrupt_handler(void (*int_func)(u_int32), char *func_name, FFL_ARGS)
{
  (void) func_name;
  tbpi_sim_set_interrupt_handler(tbp_get_my_trans_id(),int_func,iFFL_DATA);
}

/* function used to assign an reset handler to a given thread id */
void tbpi_cpf_set_reset_handler(void (*rst_func)(u_int32), char *func_name, FFL_ARGS)
{
  (void) func_name;
  tbpi_sim_set_reset_handler(tbp_get_my_trans_id(),rst_func,iFFL_DATA);
}

