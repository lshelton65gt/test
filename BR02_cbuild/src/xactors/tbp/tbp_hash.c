//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "tbp_hash.h"

#ifdef TBP_HASH_DEBUG
void tbpi_print_hash_structure(TBP_HASH_T *table)
{
#if 0
  TBP_HASH_CHAIN_T *chain;
  int i;  

  hash_print("\nPrinting hash structure:\n");
  hash_print("head = 0x%8.8x tail = 0x%8.8x\n",table->chain, table->chain_tail);
  for(i=0;i<TBP_HASH_BUCKETS;i++) {
    hash_print("hash_bucket[%d] = 0x%8.8x\n",i,table->bucket[i]);
  }
  hash_print("\nchain_table:\n");
  chain = table->chain;
  while(chain) {
    hash_print("name = %s chain = 0x%8.8x nxt = 0x%8.8x prev = 0x%8.8x link = 0x%8.8x link_prev = 0x%8.8x\n",
	   chain->string,chain,chain->nxt,chain->prev,chain->link,chain->link_prev);
    chain = chain->nxt;
  }
  hash_print("\n");
#endif
}
#endif

// hash a string (queue name) - taken from elf_hash
u_int32 tbpi_hash_string(const char *s) 
{
  u_int32 h=0;
  u_int32 g=0;
  if( ! s )
    return 0;
  while( *s )
    if( (g = (h = (h << 4) + *s++)) & 0xf0000000UL )
      h = (h ^ (g >> 24) ) & 0x0fffffffUL;
  return h;
} 

// create a hash table
TBP_HASH_T *tbp_hash_create(void)
{
  TBP_HASH_T *table;
  
  // allocate space for the table
  if(NULL==(table = (TBP_HASH_T *)tbp_calloc(1,sizeof(TBP_HASH_T))))
    hash_panic("Failed to allocate space required to create the hash table\n");
  else {
    if(NULL==(table->bucket=(TBP_HASH_CHAIN_T **)tbp_calloc(TBP_HASH_BUCKETS,sizeof(TBP_HASH_CHAIN_T *))))
      hash_panic("Failed to allocate space required to create the hash bucket\n");
    else {
      table->chain = NULL;
      table->chain_tail = NULL;
    }
  }

  return table;
}

// destroy a hash table
void tbp_hash_destroy(TBP_HASH_T *table)
{
  TBP_HASH_CHAIN_T *chain,*chain_ptr;
  if(table) {
    chain = table->chain;
    // walk through and destroy the chains
    while(chain) {
      chain_ptr = chain;
      chain = (TBP_HASH_CHAIN_T *)chain->nxt;
#ifdef TBP_HASH_SAVE_STRING
      // free the string ptr
      if(chain_ptr->string) {
	tbp_free(chain_ptr->string);
	chain_ptr->string = NULL;
      }
#endif
      // free the chain entry
      if(chain_ptr) {
	tbp_free(chain_ptr);
	chain_ptr = NULL;
      }
    }
    
    // free the buckets
    if(table->bucket) {
      tbp_free(table->bucket);
      table->bucket= NULL;
    }

    // free the table
    tbp_free(table);
    table = NULL;
  }
}

// add an entry into a hash table
void tbp_hash_add(TBP_HASH_T *table, const char *name, void *ptr)
{
  TBP_HASH_CHAIN_T *new_chain;
  TBP_HASH_CHAIN_T *last_chain, *chain;
  int hash_value = tbpi_hash_string(name)%TBP_HASH_BUCKETS;
  
  if(table) {
    hash_print("Adding %s to the hash table (hash = %d)\n",name,hash_value);
    
    // allocate space for an antry in the chain table
    if(NULL==(new_chain = (TBP_HASH_CHAIN_T *)tbp_calloc(1,sizeof(TBP_HASH_CHAIN_T))))
      hash_panic("Failed to allocate space required to create the hash chain table\n");
    else {
      // assign the pointer (other values are NULL due to calloc)
      new_chain->ptr = ptr;
      // assign the name
#ifdef TBP_HASH_SAVE_STRING
      if(NULL==(new_chain->string = (char *)tbp_calloc(1,strlen(name)+1)))
	hash_panic("Failed to allocate space required to create hash entry name\n");
      else {
	hash_print("tbp_hash_add: copying hash name %s to 0x%8.8x\n",name,new_chain->string); 
	strcpy(new_chain->string,name);
      }
#else
      new_chain->string = name;
#endif
      // assign the previous ptr
      new_chain->prev = table->chain_tail;
      
      // add this entry to the table
      if(table->chain_tail) { // not the first entry...
	table->chain_tail->nxt = new_chain;
	table->chain_tail = new_chain;
      }
      else { // the first entry...
	table->chain = table->chain_tail = new_chain;
      }

      // check for a duplicate hash
      chain = table->bucket[hash_value];

      if(chain==NULL) { // no matching entry - this hash is unique
	table->bucket[hash_value] = new_chain;
      }
      else { //follow the chain and place this duplicate at the end of the chain
	while(chain) {
	  // store the prior chain
	  last_chain = chain;
	  // get the next chain
	  chain = chain->link;
	}
	
	// point to the prior entry (used for removal)
	new_chain->link_prev = last_chain;

	// tell the prior duplicate to point to our new entry
	last_chain->link = new_chain;
      }
    }
    
    tbp_print_hash_structure(table);
  }
}

// get a pointer to the hash chain within a table
TBP_HASH_CHAIN_T *tbpi_hash_get_chain(TBP_HASH_T *table, const char *name)
{
  TBP_HASH_CHAIN_T *chain;
  int hash_value;
  
  if(name) {
    hash_value = tbpi_hash_string(name)%TBP_HASH_BUCKETS;
    if(NULL!=(chain = table->bucket[hash_value])) {
      while(chain) {
	if(strcmp(name,chain->string)==0)
	  break;
	// get the next index to test...
	chain = chain->link;
      }
    }
    
    if(chain==NULL) {
      hash_error("get_chain: could not find %s\n",name);
      return NULL;
    }
    else {
      return chain;
    }
  }
  else {
    return NULL;
  }
}

// remove an entry from a hash table
void tbp_hash_remove(TBP_HASH_T *table, const char *name)
{
  TBP_HASH_CHAIN_T *chain,*prev,*nxt;
  int hash_value;

  if(table) {
    hash_value = tbpi_hash_string(name)%TBP_HASH_BUCKETS;
    if(NULL!=(chain = tbpi_hash_get_chain(table,name))){
      hash_print("Removing hash entry %s\n",name);
#ifdef TBP_HASH_SAVE_STRING
      // free the string
      if(chain->string) {
	tbp_free(chain->string);
	chain->string = NULL;
      }
#endif
      // linked list linkage...
      // remove our entry from the linked list itself 
      prev = (TBP_HASH_CHAIN_T *)chain->prev;
      if(prev)
	prev->nxt = chain->nxt;
      else // we must have been the head...
	table->chain = chain->nxt;

      nxt = (TBP_HASH_CHAIN_T *)chain->nxt;
      if(nxt)
	nxt->prev = prev;
      else // we must have been the tail...
	table->chain_tail = chain->prev;

      // chain linkage
      // is their a prior hash chain link (duplicate)? - if so re-assign to skip us...
      prev = (TBP_HASH_CHAIN_T *)chain->link_prev;
      if(prev)
	prev->link = chain->link;
      else // no prior - remove ourselves from the hash_bucket
	table->bucket[hash_value] = chain->link;

      // remove reference to us in the next chain-link
      if(chain->link) {
	nxt = (TBP_HASH_CHAIN_T *)chain->link;
	if(nxt)
	  nxt->link_prev = prev ? prev : NULL;
      }

      // now free this entry...
      tbp_free(chain);
    }
    
    tbp_print_hash_structure(table);
  
  }
}

// hash a given name and return a pointer to the data in the table
void *tbp_hash(TBP_HASH_T *table, const char *name)
{
  TBP_HASH_CHAIN_T *chain;
  int hash_value;
  
  if(name) {
    hash_value = tbpi_hash_string(name)%TBP_HASH_BUCKETS;
    if(NULL!=(chain = table->bucket[hash_value])) {
      while(chain) {
	if(strcmp(name,chain->string)==0)
	  break;
	// get the next index to test...
	chain = chain->link;
      }
    }
    
    if(chain==NULL) {
      hash_print("get_ptr: could not find %s\n",name);
      return NULL;
    }
    else {
      hash_print("get_ptr: found %s (0x%8.8x)\n",name,chain);
      return chain->ptr;
    }
  }
  else {
    return NULL;
  }
}
