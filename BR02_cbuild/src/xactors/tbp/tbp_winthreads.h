//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_WINTHREADS_H
#define TBP_WINTHREADS_H

#include "util/CarbonPlatform.h"

#include <windows.h>
#ifdef MPROTECT
#include <sys/mman.h>
#include <limits.h>    /* for PAGESIZE */
#ifndef PAGESIZE
#define PAGESIZE 4096
#endif
#endif

#include "tbp_internal.h"
#include "tbp_events.h"

#ifdef __cplusplus
extern "C" {
#endif

/* FIXME - this should/will be programmable in context threads */
#define STKSIZE (0x100000)

typedef struct {
  u_int32 id;              // index nto the array of threads
  CONTEXT *thread_id;      // thread context struct
  void *stk_ptr;           // store ptr for safe cleanup under solaris (broken makecontext)
  u_int32 trans_id;        // id of the transactor to which we are attached
  u_int32 type;            // main, user, cmodel?
  u_int32 status;          // ?
  void *wait_evt;          // event on which we are waiting, if any (-1) means no wait
  int wait_evt_state;      // this is the state which must be reached to end the wait
  int wait_evt_type;       // none, assertion, state
  u_int64 timeout;         // timeout for this event
  void (*func_ptr)();      // mapped function
  void (*int_func_ptr)(u_int32); // interrupt function
  void (*rst_func_ptr)(u_int32); // reset function
#if pfLP64
  u_int64 *usr_ptr;
#else
  u_int32 *usr_ptr;
#endif
  u_int32 num_args;
} TBP_THREAD_T;

/* return our thread id */
CONTEXT *tbp_thread_self(void);

/* allocate/initialize stk_ptr */
void tbp_thread_allocate(u_int32 thread_index);

/* function used to copy the contents of a thread from one location to another */
void tbp_thread_copy(u_int32 dst_index, u_int32 src_index);

int tbp_thread_init(void);
void tbp_thread_cleanup(u_int32 thread_index);
int tbp_thread_create(u_int32 thread_index);

void tbp_thread_activate(u_int32 calling_thread, u_int32 target_thread);

/* special wait used in thread relocation */
void tbp_thread_wakeup_main(u_int32 calling_thread);

/* functions to destroy the threads */

#ifdef __cplusplus
}
#endif

#endif
