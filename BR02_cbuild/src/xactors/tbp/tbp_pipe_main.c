//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "tbp_internal.h"
#include "tbp_pipe_utils.h"

/* flag to limit message thrashing in non-debug mode */
static BOOL g_tbp_pipe_main_debug = FALSE;
#define tbp_pipe_main_debug if(g_tbp_pipe_main_debug) tbp_printf

int main(int argc, char *argv[])
{
  TBP_CONF_T *tc=tbp_get_conf();

  /* store the argc/argv for later */
  tc->sim_argc = argc;
  tc->sim_argv = argv;
  
  tbp_init(TBP_CSIDE);

  /* tbp_main has gone to sleep - loop looking for something to do */
  while(!tc->exit_flag) {
   
    /* skip the checks below - we are done */
    if(tc->exit_flag) break;
    
    /* check for shutdown due to insufficient running threads */
    if((tbp_get_thread_count() - tbp_get_ctrans_count()) < 2) {
      // FIXME - seems like there is a possiblity that we should have a way to let the sim go
      tbp_pipe_main_debug("pipe_main: there are no more user threads left running, sim thread will now shutdown\n");
      tbp_shutdown(0);
    }
    
    /* 
     * if the "main" thread is waiting and there is noone to wake it up 
     * we need to wake it up and let it die/shutdown
     */
    if((tbpi_main_is_waiting()==TRUE) && (tbp_get_thread_count() <= 2) && (tbpi_get_main_timeout()==0))
      tbp_thread_activate(tbp_get_my_thread_id(), tbpi_main_thread_id());

    tbp_pipe_main_debug("pipe_main: waiting for a command from the simulator\n");  
    if(tbp_get_trans_count()!=0 && tbp_get_no_hdl_flag()==FALSE) {
      tbpi_pipe_send_cmd(FFL_DATA,TBP_PIPE_RUN_SIM);
      tbpi_pipe_get_cmd(FFL_DATA);
    }
    else {
      /* execute the cmodels, if present */
      tbp_cmodel_check_execute();
    }
  }

  tbp_pipe_main_debug("pipe_main has exited\n");
  return tc->exit_code;
}
