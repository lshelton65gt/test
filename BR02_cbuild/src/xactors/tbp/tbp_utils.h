//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005-2008 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_UTILS_H
#define TBP_UTILS_H

#include "tbp_internal.h"

#ifdef __cplusplus
extern "C" {
#endif

#if pfSPARCWORKS
#define FFL_DATA (const char *) __FILE__, (const char *) __func__, (int) __LINE__
#else
#define FFL_DATA (const char *) __FILE__, (const char *) __PRETTY_FUNCTION__, (int) __LINE__
#endif
#define FFL_VALUES (const char *)file,(const char*)func,(int)line
#define iFFL_DATA (const char *)file,(const char*)func,(int)line
#define FFL_ARGS const char *file, const char *func, int line

/* default alarm interval */
#define TBP_ALARM_INTERVAL 300

/* indicate operational mode */
#define TBP_TRANSPORT_MODE_SINGLE 0
#define TBP_TRANSPORT_MODE_DUAL   1
#define TBP_TRANSPORT_MODE_NOHDL  2
#define TBP_TRANSPORT_MODE_EMU    3

/* defines used to detemrine which side of the pipe we are on (duplicate - see tbp_pipe.h) */
#define TBP_PIPE_MASTER 0
#define TBP_PIPE_SLAVE  1

/* defines to determine who called tbp_init */
#define TBP_CSIDE   TBP_PIPE_MASTER
#define TBP_SIMSIDE TBP_PIPE_SLAVE

#define TBP_PATH_INVALID   -1
#define TBP_PATH_DUPLICATE -2

#define TBP_REQ_HDL_TO_C 1
#define TBP_REQ_C_TO_HDL 2
#define TBP_REQ_IDLE     3

#define TBP_INT_NONE     0  // no interrupts pending
#define TBP_INT_PROC     1  // processing an interrupt

#define TBP_RST_NONE     0  // no reset pending
#define TBP_RST_PROC     1  // processing a reset

/* types to differentiate between HDL or cmodel threads */
#define TBP_TRANS_TYPE_HDL    1
#define TBP_TRANS_TYPE_CMODEL 2

/* bit to define read or write without specifying type */
#define TBP_READ_OP
#define TBP_WRITE_OP
#define TBP_WIDTH_8        0x01
#define TBP_WIDTH_16       0x02
#define TBP_WIDTH_32       0x04
#define TBP_WIDTH_64       0x08
#define TBP_WIDTH_64P      0x18

#define TBP_NOP     	1
#define TBP_IDLE    	2
#define TBP_SLEEP   	3
#define TBP_READ    	4
#define TBP_WRITE   	5
#define TBP_CS_READ 	6
#define TBP_CS_WRITE	7
#define TBP_NXTREQ      8
#define TBP_INT_DONE    9
#define TBP_FREAD       10
#define TBP_FWRITE      11
#define TBP_WAKEUP      12
#define TBP_CS_RMW      13  // Read Modify Write
#define TBP_RESET       14

#define TBP_USER_OP     TBP_RESET+1    // user-defined opcodes, MUST be larger codes

/* defines to match those on the verilog (used in c-transactors)*/
#define BFM_Write TBP_WRITE
#define BFM_WRITE TBP_WRITE
#define BFM_Read  TBP_READ
#define BFM_READ  TBP_READ
#define CS_Read   TBP_CS_READ
#define CS_READ   TBP_CS_READ
#define CS_Write  TBP_CS_WRITE
#define CS_WRITE  TBP_CS_WRITE
#define BFM_Nop   TBP_NOP
#define BFM_NOP   TBP_NOP
#define User_Op   TBP_USER_OP
#define USER_OP   TBP_USER_OP
#define Check_for_int TBP_INT_DONE
#define CHECK_FOR_INT TBP_INT_DONE
/* 
 * "direction" bits in the operation type are used
 * to accelerate transactions by transferring data only
 * in the direction requested - the default is to copy
 * data in both directions for every transaction 
 */
#define TBP_OP_BIDIR  0
#define TBP_OP_WRITE  1
#define TBP_OP_READ   2 
#define TBP_OP_NODATA 3

/* set/get signal defines */
#define TBP_SET_SCL  0
#define TBP_SET_INT  1
#define TBP_SET_REAL 2
#define TBP_SET_BIN  3
#define TBP_SET_OCT  4
#define TBP_SET_DEC  5
#define TBP_SET_HEX  6

#define TBP_GET_SCL  0
#define TBP_GET_INT  1
#define TBP_GET_REAL 2
#define TBP_GET_BIN  3
#define TBP_GET_OCT  4
#define TBP_GET_DEC  5
#define TBP_GET_HEX  6

#define TBP_SET_NORMAL  0
#define TBP_SET_FORCE   1
#define TBP_SET_RELEASE 2

#define TBP_SETGET_NOERR     0
#define TBP_SETGET_NULL_PATH 1
#define TBP_SETGET_BAD_PATH  2
#define TBP_SETGET_BAD_SIZE  3
#define TBP_SETGET_BAD_TYPE  4
#define TBP_SETGET_BAD_LOGIC 5 /* or ACC configuration */
#define TBP_SETGET_BAD_SCOPE 6
#define TBP_SETGET_BAD_FORCE 7

#define TBP_LICENSE_INTERVAL 0x00100000

typedef struct {
  u_int32 opcode;         // read/write etc..
  u_int64 address;        // 64bit operation address
  void *  read_data;      // data returning from the sim/hdl
  void *  write_data;     // data being written to the hdl
  void *  read_datax;     // data returning from the sim/hdl
  void *  write_datax;    // data being written to the hdl
  u_int32 size;           // size in "bytes"
  u_int32 status;         // user/bfm defined
  u_int32 interrupt;      // 32bit interrupt vector !=32'h0 == interrupt
  u_int32 reset;          // reset (sim initiated)
  u_int32 idle_cnt;       // counter used by the pli to idle the C-side
  u_int32 direction;      // data is moving to/from sim (optional for optimization)
  u_int32 width;          // 1/2/4/8 byte data width
  u_int64 starttime;      // 64bit start time for a transaction
  u_int64 endtime;        // 64bit end time for a transaction
  u_int32 buffer_size;    // internal buffer size
} TBP_OP_T;


  // Here is a nice little hack for you to unravel!  Have fun!
  typedef TBP_OP_T SS_OP_T;
#define data_from_sim read_data
#define datax_from_sim read_datax 
#define data_to_sim write_data


typedef struct {
  u_int32  type;           // hdl or cmodel transactor type
  u_int32  id;             // id of this transactor
  u_int32  thread_id;      // c-thread id if attached
  u_int32  ctrans_id;      // c-trans id if this is a cmodel thread
  BOOL     registered;     // is this transactor registered?
  BOOL     attached;       // is this transactor attached to a c-thread
  BOOL     interrupt_enable;  // per transactor interrupt supression
  // current operation information is kept on a per transactor basis
  TBP_OP_T operation;      // struct holding the current transport operation
  void *   inst_data;      // per transactor instance pointer
  u_int32  int_state;      // processing an interrupt?
  u_int32  rst_state;      // have we ben reset?
  char    *full_name;      // full hierarchical name
  char    *reg_name;       // name used to attach this to a c-function
  int      ckrationum;     // EMU clock ratio numerator (used to convert time to cycles)
  int      ckratioden;     // EMU clock ratio denominator
  u_int32 *cs_get_data;    // Data Buffer for configuration space data
  u_int32 *cs_put_data;    // Data Buffer for status space data
} TBP_TRANS_T;

/* functions to set/get the license checking interval */
void tbp_set_license_interval(s_int64 license_interval);
s_int64 tbp_get_license_interval(void);

/* function to assign an external licenses heartbeat */
void tbp_set_lic_heartbeat_func(void (*func)());
/* function to assign an external license checkout */
void tbp_set_lic_checkout_func(void (*func)());

/* set/get control TRUE (tbp3x) => sleep with each set/get FALSE (tbp4x) => no sleep */ 
void tbp_setget_sleep(BOOL flag);

/* internal functions to set/get the main timeout value */
void tbpi_set_main_timeout(u_int64 timeout);
u_int64 tbpi_get_main_timeout(void);

/* internal functions used to set/get the current time values */
void tbpi_set_current_hdl_time(u_int64 time);    
void tbpi_set_previous_hdl_time(u_int64 time);   
u_int64 tbpi_get_current_hdl_time(void); 
u_int64 tbpi_get_previous_hdl_time(void);

/* Internal functions to retrieve the sim/main thread id's */
int tbpi_main_thread_id(void);
int tbpi_sim_thread_id(void);

/* functions to set/get the thread stack size */
void tbp_set_stack_size(int size);
int tbp_get_stack_size(void);

/* function to check to see if main is waiting */
BOOL tbpi_main_is_waiting(void);
void tbpi_set_main_is_waiting(BOOL flag);

/* functions to set/get the current transactor id */
void tbpi_set_current_trans(int trans_id);
int tbpi_get_current_trans(void);

/* function to get the current transactor count */
int tbp_get_trans_count(void);

/* function to check the attached state of a transactor */
BOOL tbpi_trans_attached(int trans_id);

/* function to retrieve the thread attached to a given transactor */
u_int32 tbpi_trans_thread_id(int trans_id);

/* function to retrieve the operation associated with a given transactor */
TBP_OP_T *tbpi_trans_operation(int trans_id);

/* function to retrieve the inst_data pointer associated with a given transactor */
void *tbpi_trans_inst_data(int trans_id);

/* function to retrieve the trans struct associated with a given transactor */
TBP_TRANS_T *tbpi_get_trans(int trans_id);

/* function to store sim_argv/argc */
void tbpi_set_sim_args(int argc, char **argv);

/* functions to query available args */
BOOL tbp_get_argv_key_present(char *key);
char *tbp_get_argv_key(char *key);

/* functions to set/get the finish flag */
BOOL tbpi_get_finish_flag(void);
void tbpi_set_finish_flag(BOOL finish);

/* functions to set/get the path seperator value */
char tbpi_get_path_seperator(void);
void tbpi_set_path_seperator(char seperator);

/* function to set/get the transport mode (dual/single/nohdl etc) */
int tbpi_get_transport_mode(void);
void tbpi_set_transport_mode(int mode);

/* function to set/get the acc error flag */
void tbpi_set_acc_error_enable(BOOL enable);
BOOL tbpi_get_acc_error_enable(void);
#define tbp_set_acc_error_enable(_enable_) tbpi_set_acc_error_enable(_enable_)
#define tbp_get_acc_error_enable() tbpi_get_acc_error_enable()

/* function to set/get the no_hdl flag */
void tbp_set_no_hdl_flag(BOOL flag);
BOOL tbp_get_no_hdl_flag(void);

/* function to report our initizlization status */
BOOL tbp_is_initialized(void);

/* pack two 32bit ints into a single 64bit */
u_int64 tbp_pack64(u_int32 hi, u_int32 lo);

/* function to get the current sim time */
u_int64 tbp_get_sim_time(void);

/* generic function called by utils during an async wakeup */
void tbp_store_sim_time(void);

/* print error status */
void tbp_print_error_status(void);

/* set/get the status error_enable flag */
void tbp_set_status_error_enable(BOOL enable);
BOOL tbp_get_status_error_enable(void);

/* 
 * function which searches the trans table for an entry and returns its index
 * if it find one
 */
int tbp_get_id_full(char *full_name);

/* function used in dual process mode to manually connect the pipes */
int tbpi_transport_start(FFL_ARGS);
#define tbp_transport_start(args...) tbpi_transport_start(FFL_DATA)

/* internal initizlization function "caller" inidicates calling from the C or hdl side */
void tbp_init(int caller);

/* finish (shutdown the simulation) */
int tbp_finish(void);

/* shutdown the simulator - (copy for visability - see tbp_pli.c/h) */
void tbp_sim_finish(void);

/* issue a "stop" command to the simulator (copy - see tbp_pli.c/h) */
void tbp_sim_stop(void);

/* user callable stop function */
void tbp_stop(void);

/* function to check the sleep status of a transactor */
BOOL tbpi_check_trans_sleep(u_int32 trans_id, FFL_ARGS);
#define tbp_check_trans_sleep(_trans_id_) tbpi_check_trans_sleep(_trans_id_,FFL_DATA)

/* function to check the timeout/halt state of the main thread */
BOOL tbpi_check_main_sleep(FFL_ARGS);
#define tbp_check_main_sleep() tbpi_check_main_sleep(FFL_DATA)

/* delete events and threads in prep for another test run */
void tbpi_test_cleanup();
#define tbp_test_cleanup() tbpi_test_cleanup(FFL_DATA)

/* function to clear any pending incoming IPC data for a test reset */
void tbp_ipc_flush(void);

/* used internally and/or externally instead of "exit" to exit a test */ 
void tbp_shutdown(int code);

/* stand-in function for calls to exit() */
void tbpi_exit(int code,FFL_ARGS);
#define tbp_exit(_code_) tbpi_exit(_code_,FFL_DATA)

/* check to see that a given path name exists and is unique */
int tbpi_check_path_name(const char *path, FFL_ARGS);
#define tbp_check_path_name(_path_) \
        tbpi_check_path_name(_path_,FFL_DATA)

u_int32 tbp_get_my_thread_id(void);
int tbp_get_my_trans_id(void);

/* check to see if this is a valid trans_id */
void tbp_check_trans_id(int id);

/* function to check to see if this is the main thread */
BOOL tbp_is_main(void);

/* function to check to see if this is the "sim" thread */
BOOL tbp_is_sim(void);

/* function to check to see if this is a user thread */
BOOL tbp_is_user(void);

/* Return the transactor ID from the full path name.  */
int tbp_get_trans_id_full(char *full_name);

/* 
 * these will return the id associated with a given path
 * if the path is NULL, the current id is returned 
 */
int tbp_get_trans_id(char *name);
u_int32 tbp_get_thread_id(char *name);

/* function to return the current path */
char *tbp_get_current_path(void);

/* function to generate a transactor path from a pointer and a string */
char *tbp_gen_unique_handle(const void *obj_ptr, const char *str);
  
/* function to return the transactor name */
char *tbp_get_trans_name(int trans_id);

/* function which waits for the simulation thread (sleeping the calling thread) */
void tbp_wait_for_sim(void);

/* put the calling thread to sleep -- same as above */
#define tbp_sleep tbp_wait_for_sim

/* function to "reset" all registered transactors */
void tbpi_reset_transactors(FFL_ARGS);
#define tbp_reset_transactors() tbpi_reset_transactors(FFL_DATA)

/* wait for attached threads to run - until test_complete is issued */
void tbpi_test_wait(s_int64 timeout,FFL_ARGS);
#define tbp_test_wait(_timeout_) tbpi_test_wait(_timeout_,FFL_DATA)

/* this function tells the main function to wakeup */
void tbp_test_complete(void);

/* function used to halt a test (killing a main_wait */  
void tbp_test_halt(void);

/*
 * function to check and adjust, if needed, the size of the 
 * read/write data buffers associated with each transactor
 */
void tbpi_set_buffer_size(u_int32 trans_id, u_int32 new_size, FFL_ARGS);

/* functions to calculate how many bytes needs to be copied when copying the transaction buffer
   Since the transaction buffer can be an array of either 8, 16, 32 or 64 bits words care
   has to be taken when copying the buffer, especially on big endian systems. The function
   returns the number of bytes align to the current word size of the buffer.

   For example: For a transaction of 11 bytes with a word width of 4 (32 bits), the function
   returns 12. (11 + 1). If the size is already aligned with a word boundary the function
   returns size.
*/
inline static u_int32 tbp_calc_copy_size(u_int32 size, u_int32 width) {    
  u_int32 copy_size = size + ((size%(width&0xf)) ? (width&0xf)-(size%(width&0xf)) : 0);
  return copy_size;
}

/* C-side functions to get/put data to the simulation */
void tbpi_put_trans_data(u_int32 trans_id, u_int32 index, 
			 void *data, void *datax, 
			 u_int32 size, u_int32 width,
			 FFL_ARGS);

void tbpi_get_trans_data(u_int32 trans_id, u_int32 index, 
			 void *data, void *datax, 
			 u_int32 size, u_int32 width,
			 FFL_ARGS);
/* 
 * operation primitive function copies the contents of a given 
 * operation struct into the "current" operation and waits
 * for the simulation to return
 */
void tbpi_send_operation(u_int32 trans_id, TBP_OP_T *operation,FFL_ARGS);
#define tbp_send_operation(_trans_id_,_operation_) tbpi_send_operation(_trans_id_,_operation_,FFL_DATA)

int tbpi_transaction(u_int32 trans_id, u_int32 *opcode, u_int64 *address, 
		     void *data, void *datax, u_int32 *size, u_int32 *status, 
		     u_int32 direction, u_int32 width, FFL_ARGS);

#define tbp_transaction(_id_,_opcode_,_addr_,_data_,_datax_,_size_,_status_,_direction_,_width_) \
        tbpi_transaction(_id_,_opcode_,_addr_,(void *)_data_,(void *)_datax_,_size_,_status_,_direction_,_width_,FFL_DATA)
/*
 * this function process an opeation from pli_transport to decide what
 * if anything to do with the attached thread - return value indicates
 * whether or not the operation struct has been modified
 *
 * The operation provided should be a local copy - not the given
 * transactor's actual operation struct
 */
BOOL tbpi_sim_transport(u_int32 trans_id, TBP_OP_T *sim_op);

/* function to activate a given thread from the sim-side */
void tbp_sim_wakeup_thread(int trans_id, int calling_thread, int target_thread);

/* keep track of all calling modules to find out when rgistration is complete */
void tbpi_record_module_presence(char *full_name, FFL_ARGS);
#define tbp_record_module_presence(_full_name_) tbpi_record_module_presence(_full_name_,FFL_DATA)

void tbp_record_module_presence_carbon(char * myinst);

/* function to remove a registered module from the list (primarily cmodel) */
void tbpi_remove_module(u_int32 trans_id, FFL_ARGS);
#define tbp_remove_module(_trans_id_) tbpi_remove_module(_trans_id_,FFL_DATA)

/* 
 * Function which actually records a given transactor as registered
 * and returns its "id" variable argument list allows for cmodel 
 * registration
 */
int tbpi_register(FFL_ARGS, int trans_type, char *full_name, ...);
#define tbp_register(_path_, args...) tbpi_register(FFL_DATA,TBP_TRANS_TYPE_CMODEL,_path_,## args)

/* funtion to check that all present transactors have registered */
BOOL tbp_check_all_registered(void);

/* function used to attach a C-thread to a transactor */
void tbp_attach(char *path, void (*func), u_int32 num_args, ...);

/* function used to return the current transactor table */
TBP_TRANS_T *tbp_get_trans_table(void);

/* function to used to list the available (registered) transactors */
void tbp_list_avail_trans(void);

/* increment the attach count for trans or ctrans as needed */
void tbp_inc_attach_cnt(u_int32 trans_id);
/* decrement the attach count for trans or ctrans as needed */
void tbp_dec_attach_cnt(u_int32 trans_id);

/* 
 * return a pointer to the current operation to allow user-space
 * replacement/customization of the transaction layer
 */
TBP_OP_T *tbp_get_operation(void);

/* 
 * return a pointer to the current inst_data
 */
void *tbpi_get_inst_data(FFL_ARGS);

/* 
 * set the pointer to the current inst_data
 */
void tbpi_set_inst_data(void * ptr, FFL_ARGS);

/* function to reset the state of a transactor to pre-attachment */
void tbpi_clear_trans_struct(int trans_id, FFL_ARGS);

/* function to initialize state of a transactor (at registration) */
void tbpi_init_trans_struct(int trans_id, FFL_ARGS);

/* set a simulation variable with a given argument */
int tbpi_set_value(char *path, void *value, int type, FFL_ARGS);
#define tbp_set_value(_path_,_value_,_type_) tbpi_set_value(_path_,_value_,_type_,FFL_DATA)

/* get a simulation variable */
void *tbpi_get_value(char *path, int type, FFL_ARGS); 
#define tbp_get_value(_path_,_type_) tbpi_get_value(_path_,_type_,FFL_DATA)

/* "typed" variants to avoid passing a "void" pointer */
int  tbpi_set_value_int(char *path, int value, FFL_ARGS);
int  tbpi_set_value_bin(char *path, char *value, FFL_ARGS);
int  tbpi_set_value_hex(char *path, char *value, FFL_ARGS);
int  tbpi_set_value_real(char *path, double value, FFL_ARGS);
int  tbpi_get_value_int(char *path, FFL_ARGS); 
char *tbpi_get_value_bin(char *path, FFL_ARGS);
char *tbpi_get_value_hex(char *path, FFL_ARGS);
int  tbpi_get_value_real(char *path, FFL_ARGS);

/* variants for each available type */
#define tbp_set_value_int( _path_,_value_) tbpi_set_value_int (_path_,_value_,FFL_DATA)
#define tbp_set_value_real(_path_,_value_) tbpi_set_value_real(_path_,_value_,FFL_DATA)
#define tbp_set_value_bin( _path_,_value_) tbpi_set_value_bin (_path_,_value_,FFL_DATA)
#define tbp_set_value_hex( _path_,_value_) tbpi_set_value_hex (_path_,_value_,FFL_DATA)
#define tbp_get_value_int( _path_) tbpi_get_value_int (_path_,FFL_DATA)
#define tbp_get_value_real(_path_) tbpi_get_value_real(_path_,FFL_DATA)
#define tbp_get_value_bin( _path_) tbpi_get_value_bin (_path_,FFL_DATA)
#define tbp_get_value_hex( _path_) tbpi_get_value_hex (_path_,FFL_DATA)

/* force a simulation variable to a given value */
int tbpi_force_value(char *path, void *value, int type, FFL_ARGS);
#define tbp_force_value(_path_,_value_,_type_) tbpi_force_value(_path_,_value_,_type_,FFL_DATA)

int tbpi_force_value_int(char *path, int value, FFL_ARGS);
int tbpi_force_value_bin(char *path, char *value, FFL_ARGS);
int tbpi_force_value_hex(char *path, char *value, FFL_ARGS);
int tbpi_force_value_real(char *path, double value, FFL_ARGS);

/* variants for each available type */
#define tbp_force_value_int( _path_,_value_) tbpi_force_value_int (_path_,_value_,FFL_DATA)
#define tbp_force_value_real(_path_,_value_) tbpi_force_value_real(_path_,_value_,FFL_DATA)
#define tbp_force_value_bin( _path_,_value_) tbpi_force_value_bin (_path_,_value_,FFL_DATA)
#define tbp_force_value_hex( _path_,_value_) tbpi_force_value_hex (_path_,_value_,FFL_DATA) 

/* release a forced simulation value */
int tbpi_release_value(char *path, FFL_ARGS);
#define tbp_release_value(_path_) tbpi_release_value(_path_,FFL_DATA)

/* fuction used internally to cut down on typing when allocating a buffer */
void *tbpi_allocate_buffer(int size, char *err_string, FFL_ARGS);
#define tbp_allocate_buffer(_size_, _string_) tbpi_allocate_buffer(_size_,_string_,FFL_DATA)

/* function to block signals */
void tbp_sighold(int sig);

/* function to assign a signal hanlder (rather than an action) */
int tbp_set_signal_handler(int sig, void (handler)(int));

/* function to check for a license (heartbeat) */
void tbp_license_heartbeat(void);

/* function to check the license(s) every N calls to limit BW */
void tbp_check_license(void);

/* functions to set/get the simulation/emu time ratio */
void tbp_set_sim_ratio(int ratio);
int  tbp_get_sim_ratio(void);

/* functions to send a structured req/cmd throug the pipe */
int tbpi_pipe_send_cmd(FFL_ARGS, int cmd, ...);
int tbpi_pipe_get_cmd(FFL_ARGS, ...);

/* cleanup function used in dual process mode */
void tbpi_clean(FFL_ARGS, char *ident_string);
#define tbp_clean(ident_string) tbpi_clean(FFL_DATA,ident_string)

/* clear function used in dual process (precautionary clear of ipc) */
void tbpi_ipc_clear(FFL_ARGS, char *ident_string);
#define tbp_ipc_clear(ident_string) tbpi_ipc_clear(FFL_DATA,ident_string)

#ifdef TBP_DMALLOC
#define malloc(_s_)  tbpi_malloc(_s_,FFL_DATA)
#define calloc(_n_,_s_)  tbpi_calloc(_n_,_s_,FFL_DATA)
#define realloc(_p_,_s_) tbpi_realloc(_p_,_s_,FFL_DATA)
#define free(_p_)    tbpi_free((void **) &_p_,FFL_DATA)
#endif

#ifdef __cplusplus
}
#endif
#endif
