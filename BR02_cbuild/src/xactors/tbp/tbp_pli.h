//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_PLI_H
#define TBP_PLI_H


#ifdef __cplusplus
extern "C" {
#endif

#define TBP_PLI_SET_SCL  0
#define TBP_PLI_SET_INT  1
#define TBP_PLI_SET_REAL 2
#define TBP_PLI_SET_BIN  3
#define TBP_PLI_SET_OCT  4
#define TBP_PLI_SET_DEC  5
#define TBP_PLI_SET_HEX  6

#define TBP_PLI_GET_SCL  0
#define TBP_PLI_GET_INT  1
#define TBP_PLI_GET_REAL 2
#define TBP_PLI_GET_BIN  3
#define TBP_PLI_GET_OCT  4
#define TBP_PLI_GET_DEC  5
#define TBP_PLI_GET_HEX  6

#define TBP_PLI_SET_NORMAL  0
#define TBP_PLI_SET_FORCE   1
#define TBP_PLI_SET_RELEASE 2

/* function to annouce the presense of a transactor */
int tbp_pli_register(void);
int tbp_pli_transport(void);
void tbp_sim_finish(void);
void tbp_sim_stop(void);


char *tbp_get_current_module_name(char *name);

/* function called by various simulator events (registered for all tbp functions) */
int tbp_misc(int data, int reason);

int tbp_pli_printf (void);

/* functions used to retrieve the current file/line number of the calling hdl */
char *tbp_get_current_file(void);
char *tbp_get_current_module(void);
int tbp_get_current_line(void);


/* set a simulation variable with a given argument */
int tbp_sim_set_value(char *path, void *value, int type, int force, char *scope, FFL_ARGS);
void *tbp_sim_get_value(char *path, int type, char *scope, FFL_ARGS);

/* function to "reset" all registered transactors - emu mode simply wakes them up */
void tbpi_sim_reset_transactors(FFL_ARGS);

/* function to retrieve the current sim-time from the PLI */
u_int64 tbp_get_pli_sim_time(void);

/* function used to performance any per simulator configuration */
void tbp_simulator_specific_init();

/* function to retrieve +args */
BOOL tbp_sim_get_argv_key_present(char *key);
char *tbp_sim_get_argv_key(char *key);

#define TBP_PLI_FFL_DATA tbp_get_current_file(),tbp_get_current_module(),tbp_get_current_line()
#define tbp_pli_error_internal(args...) tbp_msg(TBP_PLI_FFL_DATA, TBP_MSG_PLI_ERROR, MSG_LVL_FORCE, ##args)
#define tbp_pli_panic_internal(args...) tbp_msg(TBP_PLI_FFL_DATA, TBP_MSG_PLI_PANIC, MSG_LVL_FORCE, ##args)

/* functions to set/get the pli error global (similar to "errno" usage) */
int tbp_sim_setget_return(void);
void tbp_sim_setget_flag(int flag);

/* macro to handle seperate methodology for dual/single process for hdl-level acc errors */
#ifdef DUAL_PROC
#define tbp_setget_error(_type_,args...) \
        g_pli_setget_return = _type_
#else
#define tbp_setget_error(_type_,args...)  \
        if(tbpi_get_acc_error_enable()) { \
           tbp_msg(iFFL_DATA, TBP_MSG_PLI_ERROR, MSG_LVL_FORCE, ##args); \
        }                                 \
	g_pli_setget_return = _type_
#endif

/* Set HDL signal.  */
int tbpi_sim_set_value(char *path, void *value, int type, int force,
		       char *scope, FFL_ARGS);

/* Get HDL signal.  */
void *tbpi_sim_get_value(char *path, int type, char *scope, FFL_ARGS);

#ifdef TBP_MTI
/* set signal value using FLI */
int tbpi_fli_set_value(char *path, void *value, int type, int force, char *scope, FFL_ARGS);
/* get signal using FLI */
void *tbpi_fli_get_value(char *path, int type, char *scope, FFL_ARGS);
#endif

#ifdef __cplusplus
}
#endif

#endif
