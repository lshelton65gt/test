//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "tbp_internal.h"
#include <stdlib.h>             /* rand_r */

/* flag to limit message thrashing in non-debug mode */
static u_int32 g_tbp_thread_debug = FALSE;
#define tbp_thread_debug if(g_tbp_thread_debug) tbp_printf

/* return our thread id */
pthread_t tbp_thread_self(void)
{
  return pthread_self();
}

/* allocate/initialize cond and mutex */
void tbp_thread_allocate(u_int32 thread_index)
{
  TBP_THREAD_T *thread = tbp_get_thread(thread_index);
  
  /* allocate space for a new mutex for this thread */
  if(NULL==(thread->mutex = (pthread_mutex_t *)tbp_malloc(sizeof(pthread_mutex_t)))){
    tbp_panic("Failed to allocate mutex\n");
  }
  else {
    /* initialize the mutext to the default os values */
    pthread_mutex_init(thread->mutex,NULL);  
    /* now lock this mutex */
    pthread_mutex_lock(thread->mutex);
  }
  
  /* allocate space for a new conditional for this thread */
  if(NULL==(thread->cond = (pthread_cond_t *)tbp_malloc(sizeof(pthread_cond_t)))){
    tbp_panic("Failed to allocate conditional\n");
  }
  else {
    /* initialize this conditional to the os default values (and initialize the predicate) */
    thread->condp = FALSE; /* predicate acts as a gate for spurious wakeups */
    pthread_cond_init(thread->cond,NULL);
  }
}

/* function used to copy the contents of a thread from one location to another */
void tbp_thread_copy(u_int32 dst_index, u_int32 src_index)
{
  TBP_THREAD_T *dst_thread = tbp_get_thread(dst_index);
  TBP_THREAD_T *src_thread = tbp_get_thread(src_index);

  dst_thread->id             = src_thread->id;     
  dst_thread->thread_id      = src_thread->thread_id;     
  dst_thread->mutex          = src_thread->mutex;         
  dst_thread->cond	     = src_thread->cond;          
  dst_thread->condp          = src_thread->condp;         
  dst_thread->trans_id       = src_thread->trans_id;      
  dst_thread->type	     = src_thread->type;         
  dst_thread->status         = src_thread->status;        
  dst_thread->wait_evt	     = src_thread->wait_evt;   
  dst_thread->wait_evt_state = src_thread->wait_evt_state;
  dst_thread->wait_evt_type  = src_thread->wait_evt_type; 
  dst_thread->timeout	     = src_thread->timeout;       
  dst_thread->func_ptr	     = src_thread->func_ptr;      
  dst_thread->int_func_ptr   = src_thread->int_func_ptr;  
  dst_thread->rst_func_ptr   = src_thread->rst_func_ptr;  
  dst_thread->usr_ptr	     = src_thread->usr_ptr;      
  dst_thread->num_args       = src_thread->num_args; 
}

/* free any resources associated with this thread */
void tbp_thread_free(u_int32 thread_index)
{
  TBP_THREAD_T *thread = tbp_get_thread(thread_index);

  /* destroy the conditional */
  thread->condp = FALSE;  /* overkill cleanup */
  pthread_cond_destroy(thread->cond);
  
  /* release and destroy our own mutex */
  pthread_mutex_unlock(thread->mutex);
  pthread_mutex_destroy(thread->mutex);
  pthread_cond_destroy(thread->cond);
  
  tbp_free(thread->mutex);
  tbp_free(thread->cond);
}

/* cleanup/destroy any state associated with this thread as it shutsdown */					    
void tbp_thread_cleanup(u_int32 thread_index)
{
  TBP_CONF_T *tc=tbp_get_conf();
  BOOL thread_killed = tbp_get_thread(thread_index)->status==TBP_THREAD_STATUS_KILL ? TRUE:FALSE;
  int retval=0;

  /* if this is the "sim" thread, then we can skip the cleanup and exit gracefully */
  if(thread_index == tc->sim_thread_id){
    tbp_shutdown(0); /* note - this code will be ignored for now */
  }
  else {
    /* cleanup/free/release any data in the thread struct */
    tbp_thread_struct_cleanup(thread_index);
    
    /* 
     * wakeup the "sim" thread" if we are just exiting, otherwise, we have
     * been killed and we need to wakeup main
     */  
    if(thread_killed)
      tbp_thread_cond_signal(tc->main_thread_id);
    else
      tbp_thread_cond_signal(tc->sim_thread_id);

    pthread_exit((void *)&retval);
  }
}

/* function pointer passed to thread_create - executes the provided user function */
void *tbp_thread(void *thread_index)
{
  TBP_CONF_T *tc=tbp_get_conf();
  pthread_attr_t attr;
  size_t stack_size;

  /* set the thread stack-size */
  pthread_attr_init(&attr);
  pthread_attr_setstacksize(&attr,tc->stack_size);
  
  pthread_attr_getstacksize(&attr,&stack_size);
  
  /* lower level function is common to p/qthreads */
  return tbp_thread_wrapper(thread_index);
}

/*
 * create a new thread - wait for it to return control after its initial execution
 */
int tbp_thread_create(u_int32 thread_index)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread = tbp_get_thread(thread_index);
  pthread_attr_t attr;

  /* set the thread stack-size */
  pthread_attr_init(&attr);
  pthread_attr_setstacksize(&attr,tc->stack_size);

  tbp_thread_debug("thread %d: thread_create new thread %d start\n",tbp_get_my_thread_id(),thread_index);
  pthread_create(&thread->thread_id,NULL,tbp_thread,&thread->id); 
  pthread_detach(thread->thread_id);

  /* wait for the new thread to return */
  tbp_thread_debug("thread %d: waiting for new thread %d to return\n",tbp_get_my_thread_id(),thread_index);
  tbp_thread_cond_wait(tbp_get_my_thread_id());

  tbp_thread_debug("thread %d: new thread %d has been created and returned\n",tbp_get_my_thread_id(),thread_index);

  return 0;
}

/*
 * this function puts the calling thread to sleep by
 * issuing a "cond_wait" for the calling thread and
 * issuing a "cond_signal for the target thread
 */
void tbp_thread_activate(u_int32 calling_thread, u_int32 target_thread)
{
  tbp_thread_debug("thread %d: (pthread %d) activating thread %d (pthread %d)\n",
		   calling_thread, pthread_self(), target_thread, tbp_get_thread(target_thread)->thread_id);
  /* signal the target thread to wake it up */
  tbp_thread_cond_signal(target_thread);
  /* wait on our conditional to put us to sleep */
  tbp_thread_cond_wait(calling_thread);
  tbp_thread_debug("thread %d: thread condition set - running (pthread %d)\n",calling_thread,pthread_self());
}

/* modify a given thread's conditional as a precurser to switching context*/
void tbp_thread_cond_signal(u_int32 thread_index)
{
  TBP_THREAD_T *thread = tbp_get_thread(thread_index);

  pthread_mutex_lock(thread->mutex);
  pthread_cond_signal(thread->cond);
  thread->condp = TRUE;
  tbp_thread_debug("thread %d: (pthread %d) Signaling thread %d (pthread %d)\n",
		   tbp_get_my_thread_id(), pthread_self(), thread_index, thread->thread_id);
  pthread_mutex_unlock(thread->mutex);
}

/* wait on a conditional - puts the calling thread to sleep to allow next therad to activate */
void tbp_thread_cond_wait(u_int32 thread_index)
{
  sigset_t blockInt;
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread = tbp_get_thread(thread_index);

#if pfWINDOWS
  (void) blockInt;
#else
  /* The following three lines should be the equivalent of sighold()
     which is being phased out on Linux.  */
  sigemptyset(&blockInt);
  sigaddset(&blockInt, SIGINT);
  sigprocmask(SIG_BLOCK, &blockInt, NULL);
  //sighold(SIGINT);
#endif
  /* wait for the conditional to occur */
  while(thread->condp==FALSE) {
    pthread_cond_wait(thread->cond,thread->mutex);
    // reset thread pointer to be safe
    thread = tbp_get_thread(thread_index);
    tbp_set_current_thread(&thread_index); // must be reset for the conditional to work
  }

  /* now check to see if we need to complete a thread relocation */
  tbp_check_thread_relocate(thread_index);

  /* reset thread pointer to be safe */
  thread = tbp_get_thread(thread_index);

  /* clear the conditional */
  thread->condp = FALSE;

#if pfWINDOWS
#else
  sigprocmask(SIG_UNBLOCK, &blockInt, NULL);
  //sigrelse(SIGINT);
#endif
  /* check to see if this thread has been asked to die */
  tbp_check_thread_exit(thread_index);

  if(thread_index > tc->main_thread_id) {
    /* if reset is !=0, call the reset handler prior to waking up the thread */
    tbp_check_reset(thread_index);
    
    /* if interrupt is !=0, call the interrupt handler prior to waking up the thread */
    tbp_check_interrupt(thread_index);
  }
}

/* special wait used in thread relocation */
void tbp_thread_wakeup_main(u_int32 thread_index)
{
  sigset_t blockInt;
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread = tbp_get_thread(thread_index);

  tbp_thread_debug("thread_wakeup_main (thread %d pthread %d)\n",thread_index,pthread_self());

  /* signal the main thread */
  tbp_thread_cond_signal(tc->main_thread_id);
  
#if pfWINDOWS
  (void) blockInt;
#else
  /* The following three lines should be the equivalent of sighold()
     which is being phased out on Linux.  */
  sigemptyset(&blockInt);
  sigaddset(&blockInt, SIGINT);
  sigprocmask(SIG_BLOCK, &blockInt, NULL);
  //sighold(SIGINT);
#endif
  /* wait for the conditional to occur (sleep) */
  while(thread->condp==FALSE) {
    pthread_cond_wait(thread->cond,thread->mutex);
    // reset thread pointer to be safe
    thread = tbp_get_thread(thread_index);
    tbp_set_current_thread(&thread_index); // must be reset for the conditional to work 
  }

  /* clear the conditional */
  thread->condp = FALSE;

#if pfWINDOWS
#else
  sigprocmask(SIG_UNBLOCK, &blockInt, NULL);
  //sigrelse(SIGINT);
#endif
  tbp_thread_debug("thread %d: thread running again (wakeup_main pthread %d)\n",thread_index,pthread_self());

}

/* 
 * return the thread id of either the current thread (if NULL)
 * or the thread attached to the path given
 */
u_int32 tbp_get_thread_id(char *name)
{
  TBP_CONF_T *tc=tbp_get_conf();
  TBP_THREAD_T *thread;
  pthread_t pt_self;
  u_int32 i;
  
  /* if the name is null - just get the current id */
  if(name==NULL) {
    /* current thread id may be unreliable at times since threading may
     * cause interleaving which momentarily corrupts this variable
     * This __should__ only happen during start up and thread switching
     * to accelerate things, we trust it first, check it and then if
     * it is wrong, go through the full lookup which gets more costly
     * as the thread count goes up.
     */
    if(NULL==(thread = tbp_get_thread(tc->current_thread)))
      return 0; // this happens furing init time
    if(tc->current_thread >= tbp_get_thread_count())
      return 0; 

    pt_self = pthread_self(); // avoid repeated lookup if we have to search the table

    if(thread->thread_id==pt_self){
      return tc->current_thread;
    }
    else { /* do a full search of the thread table as we cannot trust "current" */
      for(i=0;i<tbp_get_thread_count();i++){
	if(tbp_get_thread(i)->thread_id==pt_self){
	  return i;
	}
      }

      /* if we have gotten here, then this thread was not found - */
      return 0; // FIXME could/should be -1
    }
  }
  else {
    tbp_printf("id lookup by name not yet supported\n");
    /* look up the name */
    return 0;
  }
}
