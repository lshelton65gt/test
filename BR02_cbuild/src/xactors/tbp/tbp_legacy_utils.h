//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_LEGACY_UTILS_H
#define TBP_LEGACY_UTILS_H

#include "tbp_internal.h"

#ifdef __cplusplus
extern "C" {
#endif

#define TBP_DATA_ARRAY_SIZE 1024

struct queue_transaction_struct {
  int     valid;
#ifdef __cplusplus
  int     type;
#else
  int     class;
#endif
  int     transactor_id;
  int     test_id;
  int     transaction_id;
  int     active;
  //  ulong   addr_lo;
  //  ulong   addr_hi;
  //  ulong   * data;
  //  ulong   * ret_data;
  //  ulong   * ret_datax;
  u_int32   addr_lo;
  u_int32   addr_hi;
  u_int32   * data;
  u_int32   * ret_data;
  u_int32   * ret_datax;
  int     operation;
  int     status;
  int     ret_status;
  int     byte_count;
  //  ulong   int_status;
  u_int32   int_status;
  simt_t  starttime;
  simt_t  endtime;
  simt_t  timeout;
  int     pli_resp_expected;
  int     pli_resp_received;
  int     c_resp_expected;
  int     to_c_data_size;
  int     to_pli_data_size;
  //  ulong   data_intern[3*TBP_DATA_ARRAY_SIZE];
  u_int32   data_intern[3*TBP_DATA_ARRAY_SIZE];
};

typedef enum { TBP_MSG_PANIC_ID, TBP_MSG_ERROR_ID, TBP_MSG_WARN_ID,
               TBP_MSG_BANNER_ID, TBP_MSG_INFO_ID, TBP_MSG_DEBUG_ID,
               TBP_MSG_ASSERT_ID } tbp_msg_type;

/* legacy definitions for message "type" (for use with tbp_message) */
#define TBP_MSG_PANIC    TBP_MSG_PANIC_ID,  TBP_FFL 
#define TBP_MSG_ERROR    TBP_MSG_ERROR_ID,  TBP_FFL 
#define TBP_MSG_WARN     TBP_MSG_WARN_ID,   TBP_FFL
#define TBP_MSG_BANNER   TBP_MSG_BANNER_ID, TBP_FFL
#define TBP_MSG_INFO     TBP_MSG_INFO_ID,   TBP_FFL
#define TBP_MSG_DEBUG    TBP_MSG_DEBUG_ID,  TBP_FFL
#define TBP_MSG_ASSERT   TBP_MSG_ASSERT_ID, TBP_FFL
 
typedef struct queue_transaction_struct * QTSP;
typedef void (* TBP_CPF_INTERRUPT_PTR)(u_int32);

/* This is the datatype for users to describe an event */
typedef char * evtdex;

/* legacy tbp_messsage (new function is tbp_msg) */
int tbp_message(tbp_msg_type type, char *func, char *file, int line, ...);

/* get/set signal functions */
int tbpi_set_signal_value(char *signame, char *sigval, FFL_ARGS);
#define tbp_set_signal_value(_signame_,_sigval_) tbpi_set_signal_value(_signame_,_sigval_,FFL_DATA)

int tbpi_get_signal_value(char *signame, char *sigval, FFL_ARGS);
#define tbp_get_signal_value(_signame_,_sigval_) tbpi_get_signal_value(_signame_,_sigval_,FFL_DATA)

int tbpi_force_signal_value(char *signame, char *sigval, FFL_ARGS);
#define tbp_force_signal_value(_signame_,_value_) tbpi_force_signal_value(_signame_,_value_,FFL_DATA)

int tbpi_release_signal_value(char *signame, FFL_ARGS);
#define tbp_release_signal_value(_signame_) tbpi_release_signal_value(_signame_,FFL_DATA)

#define tbp_attach(args...)                     tbp_unsupported("tbp_attach",FFL_DATA)
#define tbp_initialize(args...)                 tbp_unsupported("tbp_initialize",FFL_DATA)
#define tbp_connects(args...)                   tbp_unsupported("tbp_connects",FFL_DATA)
#define tbp_registration(args...)               tbp_unsupported("tbp_registration",FFL_DATA)
#define tbp_require(args...)                    tbp_unsupported("tbp_require",FFL_DATA)      // FIXME - use this as a check function?
#define tbp_set_simtime_t(args...)              tbp_unsupported("tbp_set_simtime_t",FFL_DATA)
#define tbp_is_ctrans_class(args...)            tbp_unsupported("tbp_is_ctrans_class",FFL_DATA)
  //#define tbp_add_to_queue(args...)               tbp_unsupported("tbp_add_to_queue",FFL_DATA)           
  //#define tbp_put_on_queue(args...)               tbp_unsupported("tbp_put_on_queue",FFL_DATA)             
  //#define tbp_queue_empty(args...)                tbp_unsupported("tbp_queue_empty",FFL_DATA)             
  //#define tbp_queue_depth(args...)                tbp_unsupported("tbp_queue_depth",FFL_DATA)              
#define tbp_set_max_queue_depth(args...)        tbp_unsupported("tbp_set_max_queue_depth",FFL_DATA)    
#define tbp_find_queue_trans(args...)           tbp_unsupported("tbp_find_queue_trans",FFL_DATA)        
#define tbp_take_off_queue(args...)             tbp_unsupported("tbp_take_off_queue",FFL_DATA)         
#define tbp_fork(args...)                       tbp_unsupported("tbp_fork",FFL_DATA)             
#define tbp_fork_wait(args...)                  tbp_unsupported("tbp_fork_wait",FFL_DATA)             
#define tbp_fork_kill(args...)                  tbp_unsupported("tbp_fork_kill",FFL_DATA)             
#define tbp_fork_status(args...)                tbp_unsupported("tbp_fork_status",FFL_DATA)             
#define tbp_init_queue_trans(args...)           tbp_unsupported("tbp_init_queue_trans",FFL_DATA)         
#define tbp_init_queue_trans_class(args...)     tbp_unsupported("tbp_init_queue_trans_class",FFL_DATA)  
#define tbp_release_queue_trans_class(args...)  tbp_unsupported("tbp_release_queue_trans_class",FFL_DATA)
#define tbp_map_transactor(args...)             tbp_unsupported("tbp_map_transactor",FFL_DATA)     
#define tbp_set_logfile_size(args...)           tbp_unsupported("tbp_set_logfile_size",FFL_DATA)
#define tbp_set_logfile_number(args...)         tbp_unsupported("tbp_set_logfile_number",FFL_DATA)
#define tbp_set_print_handle(args...)           tbp_unsupported("tbp_set_print_handle",FFL_DATA)
#define tbp_cpf_set_idstring(args...)           tbp_unsupported("tbp_cpf_set_idstring",FFL_DATA)

#define tbp_finish_all(args...)                 tbp_finish()
#define tbp_disconnect(args...)                 tbp_finish()
#define tbp_disconnect_all(args...)             tbp_finish()
#define tbp_is_main_thread(args...)             tbp_is_main()
#define tbp_thread_kill_all(args...)            tbpi_delete_user_threads(FFL_DATA)
#define tbp_error_count(args...)                tbp_get_c_error_count()
#define tbp_error_count_all(args...)            tbp_get_error_count()
#define tbp_get_simtime_t(args...)              tbp_get_sim_time()
#define tbp_getsimtime(args...)                 tbp_get_sim_time() // FIXME - is this right?
#define tbp_set_xport_print_disable(args...)    tbp_set_sim_screen_print(FALSE)
#define tbp_get_log_file(args...)               tbp_get_logfile()
#define tbp_cpf_get_id(args...)                 tbp_get_my_trans_id()
#define tbp_cpf_get_path()                      tbp_get_current_path()
#define tbp_thread_sleep(args...)               tbp_wait_for_sim()
#define tbp_get_id_by_path(_path_)              tbp_get_trans_id(_path_)
#define tbp_wait(args...)                       tbp_wait_for_sim()
#define tbp_cpf_sleep(args...)                  tbpi_evt_sleep_thread(0,FFL_DATA)
#define tbp_evt_sleep(_to_)                     tbpi_evt_sleep_thread(_to_,FFL_DATA)
#define tbp_cpf_enable_status_errors(args...)   tbp_set_status_error_enable(TRUE)
#define tbp_cpf_suppress_status_errors(args...) tbp_set_status_error_enable(FALSE)
#define tbp_max_data_size(_size_)               tbpi_set_default_buffer_size(_size_,FFL_DATA);

void tbpi_evt_waittest(const char *event, s_int64 timeout, FFL_ARGS);
#define tbp_evt_waittest(_sx_,_to_) tbpi_evt_waittest(_sx_,_to_,FFL_DATA)


/* function to walk through the list of trasnactors */
int tbp_get_next_path(int *index, char *path);

/* return a pointer to the current operation */
QTSP tbp_attach_buffer(void);

/* transport function used for non-cpf and sim-side transactors */
void tbpi_transport(FFL_ARGS);
#define tbp_transport() tbpi_transport(FFL_DATA)

/* functions to modify and querry the state of the QTSP modification flag */
void tbp_set_qtsp_state(BOOL state);
BOOL tbp_get_qtsp_state(void);

/* function to perform any initialization specific to the legacy code */
void tbp_legacy_init(void);

/* stub function for get_class */
int tbp_cpf_get_class(void);

/* stub function */
int tbp_get_class_by_path(char *path);

/* stub function - arguments not named to avoid c++ issues */
int tbp_get_class_id_by_path(int *, int *, char *);  // class, id, path

/* set debug regions (multiple regions within a single string */
void tbp_set_debug_regions(char *regions);

void *tbp_unsupported(char *func_name, FFL_ARGS);

#ifdef __cplusplus
}
#endif

#endif
