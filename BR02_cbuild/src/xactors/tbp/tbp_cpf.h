//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef TBP_CPF_H
#define TBP_CPF_H

#ifdef __cplusplus
extern "C" {
#endif

/* basic write functions */
void tbpi_cpf_write8(u_int64 addr, u_int8 data, FFL_ARGS);
void tbpi_cpf_write16(u_int64 addr, u_int16 data, FFL_ARGS);
void tbpi_cpf_write32(u_int64 addr, u_int32 data, FFL_ARGS);
void tbpi_cpf_write64(u_int64 addr, u_int32 data_hi, u_int32 data_lo, FFL_ARGS);

/* basic read functions */
u_int8 tbpi_cpf_read8(u_int64 addr, FFL_ARGS);
u_int16 tbpi_cpf_read16(u_int64 addr, FFL_ARGS);
u_int32 tbpi_cpf_read32(u_int64 addr, FFL_ARGS);
void tbpi_cpf_read64(u_int64 addr, u_int32 *data_hi, u_int32 *data_lo, FFL_ARGS);

/* burst write */
u_int32 tbpi_cpf_write(u_int64 addr, u_int32 *data, u_int32 *size, FFL_ARGS);

/* burst read */
u_int32 tbpi_cpf_read(u_int64 addr, u_int32 *data, u_int32 *size, FFL_ARGS);

/* burst write zx */
u_int32 tbpi_cpf_write_zx(u_int64 addr, u_int32 *data, u_int32 *datax, u_int32 *size, u_int32 *status, u_int32 smask, FFL_ARGS);

/* burst read zx */
u_int32 tbpi_cpf_read_zx(u_int64 addr, u_int32 *data, u_int32 *datax, u_int32 *size, u_int32 *status, u_int32 smask, FFL_ARGS);

/* configuration space write */
void tbpi_cpf_cs_write(u_int64 addr, u_int32 data, FFL_ARGS);

/* configuration space read */
u_int32 tbpi_cpf_cs_read(u_int64 addr, FFL_ARGS);

/* user defined operation */
void tbpi_cpf_userop(u_int32 opcode, u_int32 addr_hi, u_int32 addr_lo, u_int32 *data, u_int32 *datax, 
		    u_int32 *size, u_int32 *status, FFL_ARGS);

/* function to idle N cycles */
void tbpi_cpf_idle(u_int32 idle_cnt, FFL_ARGS);

/* function used to assign an interrupt handler to a given thread */
//void tbpi_cpf_set_interrupt_handler(u_int32 id, void (*int_func), FFL_ARGS);
void tbpi_cpf_set_interrupt_handler(void (*int_func)(u_int32), char *func_name, FFL_ARGS);

/* function used to assign an reset handler to a given thread id */
//void tbpi_cpf_set_reset_handler(u_int32 id, void (*rst_func), FFL_ARGS);
void tbpi_cpf_set_reset_handler(void (*rst_func)(u_int32), char *func_name, FFL_ARGS);

/* basic write functions */
#define tbp_cpf_write8(_addr_, _data_) tbpi_cpf_write8((u_int64)_addr_,(u_int8)_data_, FFL_DATA)
#define tbp_cpf_write16(_addr_, _data_) tbpi_cpf_write16((u_int64)_addr_,(u_int16)_data_, FFL_DATA)
#define tbp_cpf_write32(_addr_, _data_) tbpi_cpf_write32((u_int64)_addr_,(u_int32)_data_, FFL_DATA)
#define tbp_cpf_write64(_addr_, _data_hi_,_data_lo_) tbpi_cpf_write64((u_int64)_addr_,(u_int32)_data_hi_,(u_int32)_data_lo_, FFL_DATA)

/* basic read functions */
#define tbp_cpf_read8(_addr_) tbpi_cpf_read8((u_int64)_addr_, FFL_DATA)
#define tbp_cpf_read16(_addr_) tbpi_cpf_read16((u_int64)_addr_, FFL_DATA)
#define tbp_cpf_read32(_addr_) tbpi_cpf_read32((u_int64)_addr_, FFL_DATA)
#define tbp_cpf_read64(_addr_,_data_hi_,_data_lo_) tbpi_cpf_read64((u_int64)_addr_,(u_int32 *)_data_hi_,(u_int32 *)_data_lo_, FFL_DATA)

/* burst write */
#define tbp_cpf_write(_addr_, _data_, _size_) tbpi_cpf_write((u_int64)_addr_,(u_int32 *)_data_,(u_int32 *)_size_, FFL_DATA)

/* burst read */
#define tbp_cpf_read(_addr_,_data_,_size_) tbpi_cpf_read((u_int64)_addr_,(u_int32 *)_data_,(u_int32 *)_size_, FFL_DATA)

/* burst write zx */
#define tbp_cpf_write_zx(_addr_,_data_,datax_,_size_,_status_,_smask_) \
tbpi_cpf_write_zx((u_int64)_addr_,(u_int32 *)_data_,(u_int32 *)_datax_,(u_int32 *)_size_,(u_int32 *)_status_,(u_int32)_smask_, FFL_DATA)

/* burst read zx */
#define tbp_cpf_read_zx(_addr_,_data_,_datax_,_size_,_status_,_smask_) \
tbpi_cpf_read_zx((u_int64)_addr_,(u_int32 *)_data_,(u_int32 *)_datax_,(u_int32 *)_size_,(u_int32 *)_status_,(u_int32)_smask_, FFL_DATA)

/* configuration space write */
#define tbp_cpf_cs_write(_addr_,_data_) tbpi_cpf_cs_write((u_int64)_addr_,(u_int32)_data_, FFL_DATA)

/* configuration space read */
#define tbp_cpf_cs_read(_addr_) tbpi_cpf_cs_read((u_int64)_addr_, FFL_DATA)

/* user defined operation */
#define tbp_cpf_userop(_opcode_,_addr_hi_,_addr_lo_,_data_,_datax_,_size_,_status_) \
tbpi_cpf_userop((u_int32)_opcode_,(u_int32)_addr_hi_,(u_int32)_addr_lo_,(u_int32 *)_data_,(u_int32 *)_datax_,(u_int32 *)_size_,(u_int32 *)_status_, FFL_DATA)

/* function to retrieve the previous operations status */
#define tbp_cpf_status() tbp_sim_status()

/* function to idle N cycles */
#define tbp_cpf_idle(_idle_cnt_) tbpi_cpf_idle(_idle_cnt_, FFL_DATA)

/* function used to assign an interrupt handler to a given thread */
#define tbp_cpf_set_interrupt_handler(_func_,_func_name_) tbpi_cpf_set_interrupt_handler((void (*)(u_int32))_func_,(char *)_func_name_,FFL_DATA)

/* function used to assign an interrupt handler to a given thread */
#define tbp_cpf_set_reset_handler(_func_,_func_name_) tbpi_cpf_set_reset_handler((void (*)(u_int32))_func_,(char *)_func_name_, FFL_DATA)

#ifdef __cplusplus
}
#endif

#endif
