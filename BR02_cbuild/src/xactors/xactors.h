//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Copyright 2005 Carbon Design Systems, Inc.  All Rights Reserved. 
//  Portions of this software code are licensed to Carbon Design Systems 
//  and are protected by copyrights of its licensors. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// Only include this file the first time it is referenced
//
#ifndef CARBONX_XACTORS__H
#define CARBONX_XACTORS__H


// for C++ compiles add the extern "C" {
// 

 #ifdef __cplusplus
extern "C" {
 #endif
 
  #include "xactors/carbonX.h"
  #include "xactors/ahb/carbonXAhbMaster.h"
  #include "xactors/ahb/carbonXAhbMonitor.h"
  #include "xactors/ahb/carbonXAhbSlave.h"
  #include "xactors/pci/carbonXPciMaster.h"
  #include "xactors/pci/carbonXPciSlave.h"
  #include "xactors/enet_mii/carbonXEnet.h"
  #include "xactors/spi3/carbonXSpi3LnkTx.h"
  #include "xactors/spi3/carbonXSpi3LnkRx.h"
  #include "xactors/spi4/carbonXSpi4Src.h"
  #include "xactors/spi4/carbonXSpi4Snk.h"
  #include "xactors/utopia2/carbonXUtopia2AtmTx.h"
  #include "xactors/utopia2/carbonXUtopia2AtmRx.h"
  #include "xactors/utopia2/carbonXUtopia2PhyTx.h"
  #include "xactors/utopia2/carbonXUtopia2PhyRx.h"
  #include "xactors/utopia2/carbonXUtopia2Support.h"
  #include "xactors/ddr/carbonXDdrsdram16Megx16.h"
  #include "xactors/ddr/carbonXDdrsdramCtrl.h"
  #include "xactors/sparse_mem/carbonXSparseMem.h"

 #ifdef __cplusplus
}
 #endif

#endif
