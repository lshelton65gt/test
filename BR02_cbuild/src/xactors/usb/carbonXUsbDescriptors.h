// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __carbonXUsbDescriptors_h_
#define __carbonXUsbDescriptors_h_

#ifndef __carbonX_h_
#include "xactors/carbonX.h"
#endif

#ifndef __carbonXUsbConfig_h_
#include "xactors/usb/carbonXUsbConfig.h"
#endif

#include "carbon/c_memmanager.h"

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * \addtogroup Usb
 * @{
 */

/*!
 * \file xactors/usb/carbonXUsbDescriptors.h
 * \brief Definition of all the usb descriptors.
 *
 * This file provides definitions and APIs of all the different usb descriptors.
 */

/*!
 * \brief Descriptor type
 */
typedef enum {
    CarbonXUsbDescriptor_DEVICE        = 0x1,    /*!< Device descriptor */
    CarbonXUsbDescriptor_CONFIGURATION = 0x2,    /*!< Configuration descriptor*/
    CarbonXUsbDescriptor_INTERFACE     = 0x4,    /*!< Interface descriptor */
    CarbonXUsbDescriptor_ENDPOINT      = 0x5,    /*!< Endpoint descriptor */
    CarbonXUsbDescriptor_OTG           = 0x9     /*!< OTG descriptor */
} CarbonXUsbDescriptorType;

/*!
 * \brief Standard Device Descriptor
 */
typedef struct {
    CarbonUInt16 bcdUSB;             /*!< USB Specification Release Number in
                                          Binary-Coded Decimal (i.e., 2.10 is
                                          210H). This field identifies the
                                          release of the USB Specification with
                                          which the device and its descriptors
                                          are compliant. */
    CarbonUInt8  bDeviceClass;       /*!< Class code of the device */
    CarbonUInt8  bDeviceSubClass;    /*!< Subclass code of the device */
    CarbonUInt8  bDeviceProtocol;    /*!< Protocol code for the device */
    CarbonUInt8  bMaxPacketSize0;    /*!< Maximum packet size for endpoint
                                          zero */
    CarbonUInt16 idVendor;           /*!< Vendor ID for the device */
    CarbonUInt16 idProduct;          /*!< Product ID for the device */
    CarbonUInt16 bcdDevice;          /*!< Device release number in BCD format */
    CarbonUInt8  iManufacturer;      /*!< Index of string descriptor
                                          describing manufacturer */
    CarbonUInt8  iProduct;           /*!< Index of string descriptor
                                          describing product */
    CarbonUInt8  iSerialNumber;      /*!< Index of string descriptor describing
                                          the device's serial number */
    CarbonUInt8  bNumConfigurations; /*!< Number of possible configurations */

} CarbonXUsbDeviceDescriptor;

/*!
 * \brief Standard Endpoint Descriptor
 */
typedef struct {
    CarbonUInt8  bEndpointAddress; /*!< The address of the endpoint on the USB
                                        device described by this descriptor */
    CarbonUInt8  bmAttributes;     /*!< This field describes the endpoint's
                                        attributes when it is configured using
                                        the bConfigurationValue */
    CarbonUInt16 wMaxPacketSize;   /*!< Maximum packet size this endpoint is
                                        capable of sending or receiving when
                                        this configuration is selected */
    CarbonUInt8  bInterval;        /*!< Interval for polling endpoint for data
                                        transfers */
} CarbonXUsbEndpointDescriptor;

/*!
 * \brief Standard Interface Descriptor
 */
typedef struct {
    CarbonUInt8 bInterfaceNumber;   /*!< Number of this interface  */
    CarbonUInt8 bAlternateSetting;  /*!< Value used to select this alternate
                                         setting for the interface identified
                                         in the prior field */
    CarbonUInt8 bNumEndpoints;      /*!< Number of endpoints used by this
                                         interface (excluding endpoint zero) */
    CarbonUInt8 bInterfaceClass;    /*!< Class code (assigned by the USB-IF) */
    CarbonUInt8 bInterfaceSubClass; /*!< Subclass code (assigned by the
                                         USB-IF) */
    CarbonUInt8 bInterfaceProtocol; /*!< Protocol code (assigned by the USB) */
    CarbonUInt8 iInterface;         /*!< Index of string descriptor describing
                                         this interface */
    CarbonXUsbEndpointDescriptor
        **epDescriptor;             /*!< Array of Endpoint descriptors */
} CarbonXUsbInterfaceDescriptor;

/*!
 * \brief Standard Configuration Descriptor
 */
typedef struct {
    CarbonUInt16 wTotalLength;        /*!< Total length of data returned for
                                           this configuration. Includes the
                                           combined length of all descriptors
                                           (configuration, interface, endpoint,
                                           and class- or vendor-specific)
                                           returned for this configuration */
    CarbonUInt8  bNumInterfaces;      /*!< Number of interfaces supported by
                                           this configuration */
    CarbonUInt8  bConfigurationValue; /*!< Value to use as an argument to the
                                           SetConfiguration() request to select
                                           this configuration */
    CarbonUInt8  iConfiguration;      /*!< Index of string descriptor describing
                                           this configuration */
    CarbonUInt8  bmAttributes;        /*!< Bitmap information to describe
                                           configuration characteristics */
    CarbonUInt8  bMaxPower;           /*!< Maximum power consumption of the USB
                                           device from the bus in this specific
                                           configuration when the device is
                                           fully operational */
    CarbonXUsbInterfaceDescriptor
        **intfDescriptor;             /*!< Array of interface descriptors */
} CarbonXUsbConfigurationDescriptor;

/*!
 * \brief Standard OTG Descriptor
 */
typedef struct {
    CarbonUInt8  bmAttributes;     /*!< This field describes the OTG's
                                        attributes regarding HNP and SRP */
} CarbonXUsbOtgDescriptor;

/*!
 * \brief Creates byte array from a standard device descriptor
 *
 * This API creates a byte array from a standard device descriptor and
 * returns the handle in the form of a CarbonUInt8 array. The memory
 * ownership is retained to the user.
 * Note: User need to call carbonmem_free to deallocate
 * the memory consumed by the descriptor.
 *
 * \param descriptor pointer to standard device descriptor structure
 *
 * \returns Pointer to the created byte array
 */
CarbonUInt8 *carbonXUsbCreateBytesFromDeviceDescriptor(
        const CarbonXUsbDeviceDescriptor *descriptor);

/*!
 * \brief Gets standard device descriptor from byte array
 *
 * This API creates a standard device descriptor from byte array. The memory
 * ownership is retained to the user.
 * Note: User need to call carbonXUsbDestroyDeviceDescriptor to deallocate
 * the memory consumed by the descriptor.
 *
 * \param byteArray Device descriptor in the form of byte array
 *
 * \returns Standard device descriptor structure
 */
CarbonXUsbDeviceDescriptor *carbonXUsbGetDeviceDescriptorFromBytes(
        const CarbonUInt8 *byteArray);

/*!
 * \brief Destroys a standard device descriptor.
 *
 * This API deletes a standard device descriptor.
 *
 * \param descriptor Pointer to the standard device descriptor
 *
 * \retval 1 For success
 * \retval 0 For failure
 */
CarbonUInt32 carbonXUsbDestroyDeviceDescriptor(
        CarbonXUsbDeviceDescriptor *descriptor);

/*!
 * \brief Gets byte array from a standard configuration descriptor
 *
 * This API creates a byte array from a standard configuration descriptor and
 * returns the handle in the form of a CarbonUInt8 array. The memory
 * ownership is retained to the user.
 * Note: User need to call carbonmem_free to deallocate
 * the memory consumed by the descriptor.
 *
 * \param descriptor Pointer to standard configuration descriptor structure
 *
 * \returns Pointer to the created byte array
 */
CarbonUInt8 *carbonXUsbCreateBytesFromConfigurationDescriptor(
        const CarbonXUsbConfigurationDescriptor *descriptor);

/*!
 * \brief Gets standard configuration descriptor from byte array
 *
 * This API returns a standard configuration descriptor from configuration
 * descriptor in the form of byte array. The memory ownership is retained to
 * the user.
 * Note: User need to call carbonXUsbDestroyConfigurationDescriptor to
 * deallocate the memory consumed by the descriptor.
 *
 * \param byteArray Pointer to configuration descriptor byte array
 *
 * \returns Standard configuration descriptor structure
 */
CarbonXUsbConfigurationDescriptor
    *carbonXUsbGetConfigurationDescriptorFromBytes(
            const CarbonUInt8 *byteArray);

/*!
 * \brief Destroys a standard configuration descriptor with all of its
 *        interfaces
 *
 * This API deletes a standard configuration descriptor along with each
 * of its interfaces and their endpoints.
 *
 * \param descriptor Pointer to the standard configuration descriptor
 *
 * \retval 1 For success
 * \retval 0 For failure
 */
CarbonUInt32 carbonXUsbDestroyConfigurationDescriptor(
        CarbonXUsbConfigurationDescriptor *descriptor);

/*!
 * \brief Gets byte array from a standard interface descriptor
 *
 * This API creates a byte array from a standard interface descriptor and
 * returns the handle in the form of a CarbonUInt8 array. The memory
 * ownership is retained to the user.
 * Note: User need to call carbonmem_free to deallocate
 * the memory consumed by the descriptor.
 *
 * \param descriptor Pointer to standard interface descriptor structure
 *
 * \returns Pointer to the created byte array
 */
CarbonUInt8 *carbonXUsbCreateBytesFromInterfaceDescriptor(
        const CarbonXUsbInterfaceDescriptor *descriptor);

/*!
 * \brief Gets standard interface descriptor from byte array
 *
 * This API returns a standard interface descriptor from interface
 * descriptor in the form of byte array. The memory ownership is retained
 * to the user.
 * Note: User need to call carbonXUsbDestroyInterfaceDescriptor to
 * deallocate the memory consumed by the descriptor.
 *
 * \param byteArray Pointer to interface descriptor byte array
 *
 * \returns Standard interface descriptor structure
 */
CarbonXUsbInterfaceDescriptor *carbonXUsbGetInterfaceDescriptorFromBytes(
        const CarbonUInt8 *byteArray);

/*!
 * \brief Destroys a standard interface descriptor with all of its endpoints
 *
 * This API deletes a standard interface descriptor along with each
 * of its endpoints.
 *
 * \param descriptor Pointer to the standard interface descriptor
 *
 * \retval 1 For success
 * \retval 0 For failure
 */
CarbonUInt32 carbonXUsbDestroyInterfaceDescriptor(
        CarbonXUsbInterfaceDescriptor *descriptor);

/*!
 * \brief Gets byte array from a standard endpoint descriptor
 *
 * This API creates a byte array from a standard endpoint descriptor and
 * returns the handle in the form of a CarbonUInt8 array. The memory
 * ownership is retained to the user.
 * Note: User need to call carbonmem_free to deallocate
 * the memory consumed by the descriptor.
 *
 * \param descriptor Pointer to standard endpoint descriptor structure
 *
 * \returns Pointer to the created byte array
 */
CarbonUInt8 *carbonXUsbCreateBytesFromEndpointDescriptor(
        const CarbonXUsbEndpointDescriptor *descriptor);

/*!
 * \brief Gets standard endpoint descriptor from byte array
 *
 * This API returns a endpoint interface descriptor from endpoint
 * descriptor in the form of byte array. The memory ownership is retained
 * to the user.
 * Note: User need to call carbonXUsbDestroyEndpointDescriptor to
 * deallocate the memory consumed by the descriptor.
 *
 * \param byteArray Pointer to endpoint descriptor byte array
 *
 * \returns Standard endpoint descriptor structure
 */
CarbonXUsbEndpointDescriptor *carbonXUsbGetEndpointDescriptorFromBytes(
        const CarbonUInt8 *byteArray);

/*!
 * \brief Destroys a standard endpoint descriptor with all of its endpoints
 *
 * This API deletes a standard endpoint descriptor.
 *
 * \param descriptor Pointer to the standard endpoint descriptor
 *
 * \retval 1 For success
 * \retval 0 For failure
 */
CarbonUInt32 carbonXUsbDestroyEndpointDescriptor(
        CarbonXUsbEndpointDescriptor *descriptor);

/*!
 * \brief Creates byte array from a standard OTG descriptor
 *
 * This API creates a byte array from a standard OTG descriptor and
 * returns the handle in the form of a CarbonUInt8 array. The memory
 * ownership is retained to the user.
 * Note: User need to call carbonmem_free to deallocate
 * the memory consumed by the descriptor.
 *
 * \param descriptor pointer to standard OTG descriptor structure
 *
 * \returns Pointer to the created byte array
 */
CarbonUInt8 *carbonXUsbCreateBytesFromOtgDescriptor(
        const CarbonXUsbOtgDescriptor *descriptor);

/*!
 * \brief Gets standard OTG descriptor from byte array
 *
 * This API creates a standard OTG descriptor from byte array. The memory
 * ownership is retained to the user.
 * Note: User need to call carbonXUsbDestroyOtgDescriptor to deallocate
 * the memory consumed by the descriptor.
 *
 * \param byteArray OTG descriptor in the form of byte array
 *
 * \returns Standard OTG descriptor structure
 */
CarbonXUsbOtgDescriptor *carbonXUsbGetOtgDescriptorFromBytes(
        const CarbonUInt8 *byteArray);

/*!
 * \brief Destroys a standard OTG descriptor.
 *
 * This API deletes a standard OTG descriptor.
 *
 * \param descriptor Pointer to the standard OTG descriptor
 *
 * \retval 1 For success
 * \retval 0 For failure
 */
CarbonUInt32 carbonXUsbDestroyOtgDescriptor(
        CarbonXUsbOtgDescriptor *descriptor);

/*!
 * \brief Checks whether a configuration descriptor is consistant
 *
 * \param byteArray Pointer to configuration descriptor byte array
 * \param speed     Speed of the transactor
 *
 * \retval 1 For success
 * \retval 0 For failure
 */
CarbonUInt32 carbonXUsbCheckDescriptor(CarbonUInt8 *byteArray,
        CarbonXUsbSpeedType speed);
/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif
