/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_PIPE_H
#define USBX_PIPE_H

#include "usbXQueue.h"
#include "usbXPacket.h"
#include "xactors/usb/carbonXUsbPipe.h"

/*!
 * \file usbXPipe.h
 * \brief Header file for pipe definition.
 *
 * This file provides the class definition for generic pipe.
 */

/*!
 * \brief Class to represent transactor's generic pipe.
 */
class CarbonXUsbPipeClass
{
public: CARBONMEM_OVERRIDES

private:

    const UsbXtor *_xtor;                   //!< Transactor reference
    CarbonUInt7 _deviceAddress;             //!< Device address
    UsbxByte *_epDescriptor;                //!< Endpoint descriptor
    UsbxQueue<UsbxTrans> *_transQueue;      //!< Transaction queue
    UsbxPacketType _nextDataPacketType;     //!< Corresponding data packet type
    UInt _frameOffset;                      //!< Frame Offset for Periodic

public:

    // Constructor and destructor
    CarbonXUsbPipeClass(UsbXtor *xtor, CarbonUInt7 deviceAddress,
            const UsbxByte *epDescriptor);
            // Note, when endpoint descriptor (epDescriptor) is NULL,
            // that will indicate endpoint zero i.e. EP0 before configuration.
            // Also note, for EP0 epDescriptor will be set later by
            // the retrieved device descriptor during configuration.
    ~CarbonXUsbPipeClass();

    // Member access functions
    const UsbXtor *getTransactor() const { return _xtor; }
    CarbonUInt7 getDeviceAddress() const { return _deviceAddress; }
    void setDeviceAddress(CarbonUInt7 deviceAddress)
    { _deviceAddress = deviceAddress; }
    const UsbxByte *getEpDescriptor() const { return _epDescriptor; }
    UsbxBool setEp0Descriptor(const UsbxByte *ep0Descriptor,
            UInt ep0DescriptorSize = 0); // ep0DescriptorSize > 0 indicates
                                         // exact size
    UsbxQueue<UsbxTrans> *getTransQueue() const { return _transQueue; }
    UsbxPacketType getNextDataPacketType() const
    { return _nextDataPacketType; }
    void setNextDataPacketType(UsbxPacketType nextDataPacketType)
    { _nextDataPacketType = nextDataPacketType; }
    UInt getFrameOffset() const { return _frameOffset; }
    void setFrameOffset(UInt frameOffset)
    { _frameOffset = frameOffset; }

    // Information extraction functions
    CarbonXUsbPipeType getType() const;
    UsbxBool isPeriodic() const;
    CarbonUInt4 getEpNumber() const;
    CarbonXUsbPipeDirection getDirection() const;
    CarbonUInt32 getMaxPayloadSize() const;
    // Gets actual interval calculating from bInterval in EP descriptor
    UInt getPeriodicInterval() const;
    UInt getRepeatCount() const;
    static UsbxBool checkDescriptorForPipe(const CarbonUInt8 *epDescriptor);

    // Debugging function
    UsbxConstString getTypeStr() const;
    UsbxConstString getDirectionStr() const;
    void print() const;
};

#endif
