/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    carbonXUsb.cpp
    Purpose: This file provides definition of creation and connection APIs for
             USB transactor object.
*/

#include "usbXtor.h"

// Create transactor
CarbonXUsbID carbonXUsbCreate(const char* transactorName,
        CarbonXUsbType transactorType, const CarbonXUsbConfig *config,
        void (*reportTransaction)(CarbonXUsbTrans*, void*),
        void (*setDefaultResponse)(CarbonXUsbTrans*, void*),
        void (*setResponse)(CarbonXUsbTrans*, void*),
        void (*refreshResponse)(CarbonXUsbTrans*, void*),
        void (*reportCommand)(CarbonXUsbCommand, void*),
        void (*notify)(CarbonXUsbErrorType, const char*,
            const CarbonXUsbTrans*, void*),
        void (*notify2)(CarbonXUsbErrorType, const char*, void*),
        void *stimulusInstance)
{
    CarbonXUsbClass *xtor = NULL;
    switch (transactorType)
    {
        case CarbonXUsb_Host:
        {
            xtor = new UsbxHostXtor(transactorName, config);
            break;
        }
        case CarbonXUsb_Device:
        {
            xtor = new UsbxDeviceXtor(transactorName, config);
            break;
        }
        case CarbonXUsb_OtgDevice:
        {
            xtor = new UsbxOtgDeviceXtor(transactorName, config);
            break;
        }
        default: ASSERT(0, NULL);
    }
    // Check for memory allocation error
    if (xtor == NULL)
    {
        UsbXtor::error(xtor, CarbonXUsbError_NoMemory,
                "Can't allocate memory for transactor.");
        return NULL;
    }
    // Check if transactor creation got some error internally
    CarbonXUsbErrorType error = xtor->getError();
    if (error != CarbonXUsbError_NoError)
    {
        UsbxConstString errorMesg = xtor->getErrorMessage();
        UsbXtor::error(xtor, error, errorMesg);
        delete xtor;
        xtor = NULL;
        return NULL;
    }

#ifndef _NO_LICENSE
    /* scramble feature name and checkout license */
    CARBON_LICENSE_FEATURE(featureName, "crbn_vsp_exec_xtor_usb", 22);
    xtor->_carbonLicense = UtLicenseWrapperCheckout(featureName,
            "USB Transactor");
#endif

    // binding
    xtor->registerCallbacks(stimulusInstance, reportTransaction,
            setDefaultResponse, setResponse, reportCommand, notify, notify2);
    return xtor;

    // to remove compile time warning
    refreshResponse = NULL;
    notify = NULL;
}

CarbonUInt32 carbonXUsbDestroy(CarbonXUsbID xtor)
{
    if (xtor == NULL)
        return 0;
#ifndef _NO_LICENSE
    /* release the license */
    UtLicenseWrapperRelease(&(xtor->_carbonLicense));
#endif

    delete xtor;
    xtor = NULL;
    return 1;
}

// Set get function wrappers
const char *carbonXUsbGetName(CarbonXUsbID xtor)
{
    if (xtor == NULL)
        return NULL;
    return xtor->getName();
}

CarbonXUsbType carbonXUsbGetType(CarbonXUsbID xtor)
{
    if (xtor == NULL)
        return CarbonXUsb_Host;
    return xtor->getType();
}

CarbonUInt11 carbonXUsbGetFrameNumber(const CarbonXUsbID xtor)
{
    if (xtor == NULL)
        return 0;
    return xtor->getFrameNumber();
}

CarbonUInt32 carbonXUsbStartCommand(CarbonXUsbID xtor,
        CarbonXUsbCommand command)
{
    if (xtor == NULL)
        return 0;
    switch (command)
    {
        case CarbonXUsbCommand_DeviceAttach: return xtor->attach();
        case CarbonXUsbCommand_DeviceDetach: return xtor->detach();
        case CarbonXUsbCommand_Suspend: return xtor->suspend();
        case CarbonXUsbCommand_Resume: return xtor->resume();
        case CarbonXUsbCommand_OtgStartSession: return xtor->startOtgSession();
        case CarbonXUsbCommand_OtgStopSession: return xtor->stopOtgSession();
        case CarbonXUsbCommand_OtgStartHnp: return xtor->startOtgHnp();
        case CarbonXUsbCommand_OtgStopHnp: return xtor->stopOtgHnp();
        default: return 0;
    }
    return 0;
}

CarbonUInt32 carbonXUsbStartBusEnumeration(CarbonXUsbID xtor)
{
    if (xtor == NULL)
        return 0;
    return xtor->startBusEnumeration();
}

CarbonXUsbPipe* carbonXUsbCreatePipe(CarbonXUsbID xtor,
        CarbonUInt7 deviceAddress, const CarbonUInt8 *epDescriptor)
{
    if (xtor == NULL || !xtor->isHost())
        return NULL;
    if (!CarbonXUsbPipeClass::checkDescriptorForPipe(epDescriptor))
        return NULL;
    return xtor->createPipe(deviceAddress, epDescriptor);
}

CarbonUInt32 carbonXUsbDestroyPipe(CarbonXUsbID xtor, CarbonXUsbPipe *pipe)
{
    if (xtor == NULL)
        return 0;
    return xtor->destroyPipe(pipe);
}

CarbonUInt32 carbonXUsbGetCurrentPipeCount(const CarbonXUsbID xtor)
{
    if (xtor == NULL)
        return 0;
    return xtor->getPipeCount();
}

CarbonXUsbPipe * const *carbonXUsbGetCurrentPipes(CarbonXUsbID xtor)
{
    if (xtor == NULL)
        return NULL;
    return xtor->getPipes();
}

CarbonUInt32 carbonXUsbStartNewTransaction(CarbonXUsbID xtor,
        CarbonXUsbTrans *trans)
{
    if (xtor == NULL || trans == NULL)
        return 0;
    return xtor->startNewTransaction(trans);
}

CarbonUInt32 carbonXUsbConnectToLocalSignalMethods(CarbonXUsbID xtor,
        CarbonXInterconnectNameCallbackTrio *nameList)
{
    if (xtor == NULL || nameList == NULL)
        return 0;

    /* Iterate until the last item is reach for which TransactorSignalName
     * field has NULL */
    for (UInt i = 0; nameList[i].TransactorSignalName != NULL; i++)
    {
        CarbonXInterconnectNameCallbackTrio trio = nameList[i];
        /* get the transactor signal wrapper */
        UsbxPin *pin = xtor->getPinByName(trio.TransactorSignalName);
        if (pin == NULL)
            return 0;

        if (!pin->bind(trio))
            return 0;
    }
    return 1;
}

CarbonUInt32 carbonXUsbConnectByModelSignalHandle(CarbonXUsbID xtor,
        CarbonXInterconnectNameHandlePair *nameList,
        const char *carbonModelName, CarbonObjectID *carbonModelHandle)
{
    if (xtor == NULL || nameList == NULL ||
            carbonModelName == NULL || carbonModelHandle == NULL)
        return 0;

    UInt i;
    /* iterate until the last item is reach for which TransactorSignalName
     * field has NULL */
    for (i = 0; nameList[i].TransactorSignalName != NULL; i++)
    {
        CarbonXInterconnectNameHandlePair pair = nameList[i];
        /* get the transactor signal wrapper */
        UsbxPin *pin = xtor->getPinByName(pair.TransactorSignalName);
        /* connect the net handle with transactor's respective net */
        if (!pin->bind(carbonModelHandle, pair.ModelSignalHandle))
            return 0; /* Failure in connection */
    }
    return 1;
    carbonModelName = NULL; /* remove warning */
}

CarbonUInt32 carbonXUsbConnectByModelSignalName(CarbonXUsbID xtor,
        CarbonXInterconnectNameNamePair *nameList,
        const char *carbonModelName, CarbonObjectID *carbonModelHandle)
{
    if (xtor == NULL || nameList == NULL ||
            carbonModelName == NULL || carbonModelHandle == NULL)
        return 0;

    static char hdlPath[1024];
    CarbonNetID *net;
    UInt i;
    /* iterate until the last item is reach for which TransactorSignalName
     * field has NULL */
    for (i = 0; nameList[i].TransactorSignalName != NULL; i++)
    {
        CarbonXInterconnectNameNamePair pair = nameList[i];
        sprintf(hdlPath, "%s.%s", carbonModelName, pair.ModelSignalName);
        /* get the net handle */
        net = carbonFindNet(carbonModelHandle, hdlPath);
        if (net == NULL)
            return 0;
        /* get the transactor signal wrapper */
        UsbxPin *pin = xtor->getPinByName(pair.TransactorSignalName);
        /* connect the net handle with transactor's respective net */
        if (!pin->bind(carbonModelHandle, net))
            return 0;
    }
    return 1;
}

CarbonUInt32 carbonXUsbEvaluate(CarbonXUsbID xtor, CarbonTime currentTime)
{
    if (xtor == NULL)
        return 0;

#ifndef _NO_LICENSE
    /* tell the license server we are still using the license */
    UtLicenseWrapperHeartbeat(xtor->_carbonLicense);
#endif

    return (xtor->evaluate(currentTime) ? 1 : 0);
}

CarbonUInt32 carbonXUsbRefreshInputs(CarbonXUsbID xtor,
       CarbonTime currentTime)
{
    if (xtor == NULL)
        return 0;

    return (xtor->refreshInputs(currentTime) ? 1 : 0);
}

CarbonUInt32 carbonXUsbRefreshOutputs(CarbonXUsbID xtor,
        CarbonTime currentTime)
{
    if (xtor == NULL)
        return 0;

    return (xtor->refreshOutputs(currentTime) ? 1 : 0);
}

void carbonXUsbDump(CarbonXUsbID xtor)
{
    if (xtor == NULL)
        printf("NULL TRANSACTOR\n");
    else
        xtor->print();
}

#ifdef _VCD
extern "C" CarbonUInt32 carbonXUsbEnableVcd(const char *vcdFileName)
{
    return CarbonXUsbClass::enableVcdDump(vcdFileName);
}
#endif

