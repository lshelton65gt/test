/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_QUEUE_H
#define USBX_QUEUE_H

#include "usbXDefines.h"

/*!
 * \file usbXTransQueue.h
 * \brief Header file for queue implementation.

 * This file provides the class definition for templatized queue.
 */

//! \brief Queue class (templatized)
//         The queue is implemented using doubly linked list.
template <typename QueueData>
class UsbxQueue
{
public: CARBONMEM_OVERRIDES

private:

    //! \brief Queue node class
    //         This type defines each node of queue.
    class UsbxQueueNode
    {
    public: CARBONMEM_OVERRIDES

    public:

        QueueData * const _data;//!< Data data
        UsbxQueueNode *_prev;   //!< Link to previous node
        UsbxQueueNode *_next;   //!< Link to next node

        UsbxQueueNode(UsbxQueueNode* prev, QueueData *data);
        ~UsbxQueueNode();
    };

public:

    //! \brief Queue interator class
    //         This type defines each node of queue.
    class UsbxQueueIterator
    {
    public: CARBONMEM_OVERRIDES

    private:

        const UsbxQueueNode *_node;

    public:

        UsbxQueueIterator(): _node(NULL) {}
        UsbxQueueIterator(UsbxQueueNode *node): _node(node) {}
        void operator++(int unused)
        { if (_node) _node = _node->_next; unused = 0; }
        QueueData *operator*()
        { return (_node) ? _node->_data : NULL; }
        UsbxBool operator==(const UsbxQueueIterator &iter) const
        { return _node == iter._node; }
        UsbxBool operator!=(const UsbxQueueIterator &iter) const
        { return _node != iter._node; }
    };


private:

    UInt _count;                //!< No of datas in queue
    UInt _size;                 //!< Max. no of datas in queue
    UsbxQueueNode *_head;       //!< Head of queue
    UsbxQueueNode *_tail;       //!< Tail of queue

public:

    UsbxQueue(UInt size = 0);   // make size = 0 for infinite queue
    ~UsbxQueue();

    // Member access functions
    void setSize(const UInt size) { _size = size; }
    UInt getSize() const { return _size; }

    UInt getCount() const { return _count; } // Gets fill level
    UsbxBool isEmpty() const { return (_count == 0); }
    UsbxBool isFull() const
    { return (_count == 0) ? UsbxFalse : (_count == _size); }

    // Pushes new data
    UsbxBool push(QueueData *data);
    // Pops oldest data
    QueueData *pop();
    // Gets the oldest data without poping
    QueueData *top();
    // Gets the oldest data without poping
    QueueData *bottom();
    // Removes a particular data from queue
    UsbxBool remove(QueueData *data);

    // Iterator functions
    const UsbxQueueIterator begin() const
    { return UsbxQueueIterator(_head); }
    const UsbxQueueIterator end() const
    { return UsbxQueueIterator(); }
};

template <typename QueueData>
UsbxQueue<QueueData>::UsbxQueueNode::UsbxQueueNode(UsbxQueueNode* prev,
        QueueData *data): _data(data), _prev(prev), _next(NULL)
{
    if (_prev != NULL)
        _prev->_next = this;
}

template <typename QueueData>
UsbxQueue<QueueData>::UsbxQueueNode::~UsbxQueueNode()
{
    if (_next != NULL)
        _next->_prev = NULL;
}

template <typename QueueData>
UsbxQueue<QueueData>::UsbxQueue(UInt size): _count(0), _size(size),
    _head(NULL), _tail(NULL)
{}

template <typename QueueData>
UsbxQueue<QueueData>::~UsbxQueue()
{
    // Free up all nodes
    while (_head != NULL)
    {
        UsbxQueueNode *nextHead = _head->_next;
        delete _head;
        _head = nextHead;
    }
}

template <typename QueueData>
UsbxBool UsbxQueue<QueueData>::push(QueueData *data)
{
    if (_size > 0 && _count >= _size) // If no more datas allowed
        return UsbxFalse;

    // Create the node
    _tail = new UsbxQueueNode(_tail, data);
    if (_tail == NULL)
        return UsbxFalse;
    if (_count == 0) // When queue is empty
        _head = _tail;

    _count++;
    return UsbxTrue;
}

template <typename QueueData>
QueueData* UsbxQueue<QueueData>::pop()
{
    if (_count == 0) // When queue is empty
        return NULL;

    // Data is always poped from the head of the queue
    UsbxQueueNode *node = _head;
    _count--;

    if (_count == 0) // If queue becomes empty after pop
        _head = _tail = NULL;
    else // If queue has some node after pop
        _head = node->_next;

    // Retrieve the data and free the memory for queue node
    QueueData *data = node->_data;
    delete node;
    return data;
}

template <typename QueueData>
QueueData* UsbxQueue<QueueData>::top()
{
    if (_count == 0) // queue is empty
        return NULL;

    // Get the data corresponding to head
    return _head->_data;
}


template <typename QueueData>
QueueData* UsbxQueue<QueueData>::bottom()
{
    if (_count == 0) // queue is empty
        return NULL;

    // Get the data corresponding to head
    return _tail->_data;
}

// Deletes a particular dataaction if exists in queue
template <typename QueueData>
UsbxBool UsbxQueue<QueueData>::remove(QueueData *data)
{
    // Search for the dataaction in queue
    UsbxQueueNode *node = _head;
    while (data != NULL && node->_data != data)
        node = node->_next;

    // If not found
    if (node == NULL)
        return UsbxFalse;

    // Delete node containing the dataaction
    _count--;
    if (_count == 0) // If queue becomes empty after delete
        _head = _tail = NULL;
    else if (node == _head) // If its at head
    {
        _head = _head->_next;
    }
    else if (node == _tail) // If its at tail
    {
        _tail = _tail->_prev;
        _tail->_next = NULL;
    }
    else // When dataaction is at middle
    {
        node->_next->_prev = node->_prev;
        node->_prev->_next = node->_next;
        node->_next = NULL;
    }
    delete node;
    return UsbxTrue;
}

#endif
