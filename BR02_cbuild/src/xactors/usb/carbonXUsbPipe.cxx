/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    carbonXUsbPipe.cpp
    Purpose: This file provides definition of creation and connection APIs for
             USB Pipe.
*/

#include "xactors/usb/carbonXUsbPipe.h"
#include "usbXPipe.h"

CarbonXUsbPipeType carbonXUsbPipeGetType(const CarbonXUsbPipe* pipe)
{
    if (pipe == NULL)
        ASSERT(0, NULL);
    return pipe->getType();
}

CarbonUInt7 carbonXUsbPipeGetDeviceAddress(const CarbonXUsbPipe* pipe)
{
    if (pipe == NULL)
        ASSERT(0, NULL);
    return pipe->getDeviceAddress();
}

CarbonUInt4 carbonXUsbPipeGetEpNumber(const CarbonXUsbPipe* pipe)
{
    if (pipe == NULL)
        ASSERT(0, NULL);
    return pipe->getEpNumber();
}

CarbonXUsbPipeDirection carbonXUsbPipeGetDirection(const CarbonXUsbPipe* pipe)
{
    if (pipe == NULL)
        ASSERT(0, NULL);
    return pipe->getDirection();
}

CarbonUInt32 carbonXUsbPipeGetMaxPayloadSize(const CarbonXUsbPipe* pipe)
{
    if (pipe == NULL)
        ASSERT(0, NULL);
    return pipe->getMaxPayloadSize();
}
