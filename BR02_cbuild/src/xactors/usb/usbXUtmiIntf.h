/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_UTMIINTF_H
#define USBX_UTMIINTF_H

#include "usbXIntf.h"
#include "usbXConfig.h"

/*!
 * \file usbXUtmiIntf.h
 * \brief Header file for UTMI LINK and PHY pin level interface definition.
 */

/*!
 * \brief Class to represent UTMI pin level interface
 */
class UsbxUtmiIntf: public UsbxIntf
{
public: CARBONMEM_OVERRIDES

protected:

    //! \brief USB UTMI Line State Types
    enum UsbxUtmiLineStateType {
        UsbxUtmiLineState_SE0 = 0,
        UsbxUtmiLineState_J   = 1,
        UsbxUtmiLineState_K   = 2,
        UsbxUtmiLineState_SE1 = 3
    };

    //! \brief UTMI FSM states
    enum UsbxUtmiFsmStateType {
        UsbxUtmiFsm_IDLE,
        UsbxUtmiFsm_TRANSMIT,
        UsbxUtmiFsm_RECEIVE,
        UsbxUtmiFsm_DETACHED
    };

    //! \brief UTMI chirp states
    enum UsbxUtmiChirpStateType {
        UsbxUtmiChirp_IDLE,
        // For Host
        UsbxUtmiChirp_CheckDeviceK,
        UsbxUtmiChirp_DriveHostHsChirps,
        UsbxUtmiChirp_DriveHostK,
        UsbxUtmiChirp_DriveHostJ,
        // For Device
        UsbxUtmiChirp_WaitForResetFromHost,
        UsbxUtmiChirp_DriveDeviceK,
        UsbxUtmiChirp_CheckHostJ,
        UsbxUtmiChirp_CheckHostK,
        // OTG SRP timing
        UsbxUtmiChirp_SrpDataLinePulse,
        UsbxUtmiChirp_SrpVbusCharge,
        UsbxUtmiChirp_SrpVbusDischarge
    };

    //! \brief UTMI suspend resume states
    enum UsbxUtmiSuspendStateType {
        UsbxUtmiSuspend_Idle,
        UsbxUtmiSuspend_DriveSuspend,
        UsbxUtmiSuspend_HostSuspended,
        UsbxUtmiSuspend_DriveResumeChirpK,
        UsbxUtmiSuspend_DriveEOR,
        UsbxUtmiSuspend_DriveIdleAfterEOR,
        UsbxUtmiSuspend_DriveWakeUpChirpK,
        UsbxUtmiSuspend_DeviceSuspended,
        UsbxUtmiSuspend_DetectEOR,
        UsbxUtmiSuspend_DetectFsIdleAfterEOR,
        UsbxUtmiSuspend_CheckSuspendOrReset
    };

    //! \brief UTMI Vbus voltage values
    enum UsbxUtmiVbusValue {
        UsbxUtmiVbus_VB_SESS_END = 0,     //!< 0.2 V < VBUS < 0.8 V
        UsbxUtmiVbus_VA_SESS_VLD,         //!< 0.8 V < VBUS < 2 V
        UsbxUtmiVbus_VB_SESS_VLD,         //!< 0.8 V < VBUS < 4 V
        UsbxUtmiVbus_VA_VBUS_VLD          //!< VBUS > 4.4 V
    };

    // Data bus size
    UInt _dataBusSize;
    CarbonXUsbUtmiPinLevel _interfaceLevel;

    // FSM state
    UsbxUtmiFsmStateType _state;

    // Packet buffer to be accessed by USB FSM
    UsbxPacket *_txPacket;
    UsbxPacket *_rxPacket;
    UInt _byteIndex;
    UsbxByte _rxBuffer[1+1024+2];
    UsbxBool _rxError;
    UInt _waitCount;

    // Timing buffer to be accessed by USB FSM
    UsbxTimingType _driveTiming;
    UsbxTimingType _sensedTiming;

    // Chirp timing variables
    UInt _clockCountForTiming;          //!< Clock count for timing
    UsbxUtmiChirpStateType _chirpState; //!< Chirp state
    UInt _clockCountForChirp;           //!< Clock count for chirp
    UInt _hostChirpCount;               //!< Clock count for host KJ chirps
    UInt _numHostChirps;                //!< Number of host chirps to drive
    UInt _lineStatePeriod;                  //!< Line state period
    UsbxUtmiLineStateType _lastLineState;   //!< Last line state

    // Suspend - resume timing
    UsbxUtmiSuspendStateType _suspendState;
    UInt _clockCountForSuspendTiming;
    UInt _kDriveCount;
    UInt _suspendWait;

    // Vbus charge - discharge for OTG
    UsbxUtmiVbusValue _vbusValue;
    UInt _clockCountForVbusCharge;
    UInt _clockCountForVbusDischarge;

    // USB parameters
    static const UInt _clocksForInterPacketDelay;        // 16 clocks
    static const UInt _clocksPerFSBit;                   //  5 clocks
    static const UInt _clocksForAttach;                  // 2.5 us
    static const UInt _clocksForResetSE0;                // 10 ms
    static const UInt _clocksToStartDeviceChirpK;        // 4 ms
    static const UInt _clocksForDeviceChirpDetect;       // 1-7 ms
    static const UInt _clocksForDeviceChirpKMin;         // 1 ms
    static const UInt _clocksForDeviceChirpK;            // 3 ms
    static const UInt _clocksToStartHostChirp;           // 100 us
    static const UInt _clocksForHostChirpMin;            // 40 us
    static const UInt _clocksForHostChirpMax;            // 60 us
    static const UInt _numHostChirpsMin;                 // 6 chirps
    static const UInt _clocksForSE0AfterHostChirp;       // 300 us
    static const UInt _clocksForHostChirpDetect;         // 2.5 ms
    static const UInt _clocksForDeviceSuspendDetect;     // 3.0 ms
    static const UInt _clocksToDriveSuspend;             // 10.0 ms
    static const UInt _clocksForResumeChirpK;            // 20.0 ms
    static const UInt _clocksForSE0AfterResume;          // 1.25 to 1.5 us
    static const UInt _clocksToDecipherSuspendReset;     // 100 to 875 us
    static const UInt _clocksForHSDeviceToEnableFSTerminations;// max 125 us
    static const UInt _clocksForHostToDetectRemoteWakeUp;// 900 us
    static const UInt _clocksForRemoteWakeupChirpK;      // 1 to 15 ms
    static const UInt _minClocksBeforeRemoteWakeupDrive; // 5 ms
    static const UInt _clocksForSrp;                     // 100 ms
    static const UInt _clocksForSrpDataLinePulse;        // 10 ms
    static const UInt _clocksForSrpVbusCharge;           // 30 ms
    static const UInt _clocksForSrpVbusDischarge;        // 50 ms

    // ULPI connection
    friend class UsbxUlpiLinkIntf;
    friend class UsbxUlpiPhyIntf;

    UsbxBool _isUlpi;

public:

    // UTMI level 0 pins
    UsbxPin *_Reset;
    UsbxPin *_DataBus16_8;
    UsbxPin *_SuspendM;

    UsbxPin *_DataInLo;
    UsbxPin *_DataInHi;
    UsbxPin *_TXValid;
    UsbxPin *_TXValidH;
    UsbxPin *_TXReady;

    UsbxPin *_DataOutLo;
    UsbxPin *_DataOutHi;
    UsbxPin *_RXValid;
    UsbxPin *_RXValidH;
    UsbxPin *_RXActive;
    UsbxPin *_RXError;

    UsbxPin *_XcvrSelect;
    UsbxPin *_TermSelect;
    UsbxPin *_OpMode;
    UsbxPin *_LineState;

    // Level 1 pins
    UsbxPin *_DpPulldown;
    UsbxPin *_DmPulldown;
    UsbxPin *_HostDisconnect;

    UsbxPin *_IdPullup;
    UsbxPin *_IdDig;

    UsbxPin *_AValid;
    UsbxPin *_BValid;
    UsbxPin *_VbusValid;
    UsbxPin *_SessEnd;

    UsbxPin *_DrvVbus;
    UsbxPin *_ChrgVbus;
    UsbxPin *_DischrgVbus;

public:

    // Constructor and destructor
    UsbxUtmiIntf(UsbXtor *xtor, UInt dataBusSize,
            CarbonXUsbUtmiPinLevel intfLevel);
    virtual ~UsbxUtmiIntf();

    // Packet access functions
    UsbxBool transmitPacket(UsbxPacket *packet);
    UsbxBool transmitComplete() const;
    UsbxPacket *receivePacket();
    virtual UsbxBool receiving(CarbonTime currentTime) const = 0;

    // Timing access functions
    UsbxBool startTiming(UsbxTimingType timing);
    UsbxBool timingComplete() const;
    UsbxBool senseTiming(UsbxTimingType expectedTiming,
            UsbxBool flush = UsbxTrue);

    // Vbus access routines for OTG UTMI level 1 pins
    virtual UsbxBool driveVbus(UsbxBool isOn) = 0;
    UsbxBool isVbusValid() const;

    //! Reset the interface for detach
    virtual void resetForDetach();

    // Refresh functions
    virtual UsbxBool refreshInputs(CarbonTime currentTime) = 0;
    virtual UsbxBool refreshOutputs(CarbonTime currentTime) = 0;

    // Pin access functions
    UsbxPin *getPin(UsbxConstString pinName) const;
};

/*!
 * \brief Class to represent UTMI LINK pin level interface
 */
class UsbxUtmiLinkIntf: public UsbxUtmiIntf
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor and destructor
    UsbxUtmiLinkIntf(UsbXtor *xtor, UInt dataBusSize = 8,
            CarbonXUsbUtmiPinLevel intfLevel = CarbonXUsbUtmiPinLevel_0);
    ~UsbxUtmiLinkIntf() {}

    void setSpeedFS(UsbxBool isFS, CarbonTime currentTime);

    // Member function
    UsbxBool receiving(CarbonTime currentTime) const
    { return _RXActive->read(currentTime); }
    UsbxBool driveVbus(UsbxBool isOn);

    //! Reset the interface for detach
    void resetForDetach();

    // Evaluates the interface FSM
    UsbxBool evaluate(CarbonTime currentTime);

    // Refresh functions
    UsbxBool refreshInputs(CarbonTime currentTime);
    UsbxBool refreshOutputs(CarbonTime currentTime);
};

/*!
 * \brief Class to represent UTMI PHY pin level interface
 */
class UsbxUtmiPhyIntf: public UsbxUtmiIntf
{
public: CARBONMEM_OVERRIDES

public:

    //*! \brief Class to represent Line State encoder
    class UsbxLineStateLogic
    {
    public: CARBONMEM_OVERRIDES

    private:

        //! \brief UTMI Line State FSM states
        enum UsbxUtmiLineStateFsmStateType {
            UsbxUtmiLineStateFsm_IDLE,
            UsbxUtmiLineStateFsm_RECEIVE
        };

        class UsbxLineEncoder
        {
        public: CARBONMEM_OVERRIDES

        private:

            UsbxBool _noEncoding;
            UInt _numContiguousOnes;
            UsbxBit _nrziBit;
            // Max packet bit stream size would be =
            //     (1 byte SYNC + 1024 Payload bytes) * Bit Stuffing Factor
            enum { _maxLineStateQueueSize = 2 * 8 * 1025 };
            UsbxUtmiLineStateType _lineStateQueue[_maxLineStateQueueSize];
            Int _queueBack, _queueFront;

        public:

            // Constructor
            UsbxLineEncoder();

            // FSM functions
            void reset();
            void disableEncoding()
            { _noEncoding = UsbxTrue; }

            void pushFsSync()
            { pushDataByte(0x80); }
            void pushDataByte(UsbxByte dataByte);
            void pushFsEop();

            void pushLineState(UsbxUtmiLineStateType lineStateValue);
            UsbxUtmiLineStateType popLineState();
            UsbxBool hasPendingLineState() const
            { return (_queueBack >= 0); }
        };

    private:

        UsbxUtmiIntf * _intf;
        UsbxUtmiLineStateFsmStateType _state;
        UsbxLineEncoder _encoder;
        UInt _clockCountForFSBit;
        UInt _waitForHsDataEnd;
        UsbxBool _syncPushed;

    public:

        // Constructor
        UsbxLineStateLogic();
        void setIntf(UsbxUtmiIntf *intf) { _intf = intf; }

        // Utility functions
        void reset()
        { _encoder.reset(); _state = UsbxUtmiLineStateFsm_IDLE; }
        void pushFsSync()
        { _encoder.pushFsSync(); }
        void pushDataByte(UsbxByte dataByte)
        { _encoder.pushDataByte(dataByte); }
        void pushFsEop()
        { _encoder.pushFsEop(); }

        // Evaluation function
        UsbxBool evaluate(CarbonTime currentTime);
    };

private:
    //*! \brief Class to represent bit stuffer
    class UsbxUtmiBitStuffer
    {
        public: CARBONMEM_OVERRIDES

        private:

            UInt _dataBusSize;
            UInt _bitStuffCount;
            UInt _contiguousOnes;

        public:

            // Constructor
            UsbxUtmiBitStuffer(UInt _dataBusSize = 8);

            // Functions
            void reset() { _bitStuffCount = 0; _contiguousOnes = 1; }
            void update(UsbxByte dataByte);
            UInt getFSBitStuffCount();     // For FS
            UsbxBool needForHSByteStuff(); // For HS
    };

    UsbxUtmiBitStuffer _txBitStuffer;   //!< Bit stuffer for DataIn
    UsbxUtmiBitStuffer _rxBitStuffer;   //!< Bit stuffer for DataOut
    UInt _clockCountForFSByte;          //!< Clock count for FS bits
    UsbxLineStateLogic _lineStateLogic; //!< Logic for driving LineState

public:

    // Constructor and destructor
    UsbxUtmiPhyIntf(UsbXtor *xtor, UInt dataBusSize = 8,
            CarbonXUsbUtmiPinLevel intfLevel = CarbonXUsbUtmiPinLevel_0);
    ~UsbxUtmiPhyIntf() {}

    // Member function
    UsbxBool receiving(CarbonTime currentTime) const
    { return _TXValid->read(currentTime); }
    UsbxBool driveVbus(UsbxBool isOn);

    //! Reset the interface for detach
    void resetForDetach();

    // Evaluates the interface FSM
    UsbxBool evaluate(CarbonTime currentTime);

    // Refresh functions
    UsbxBool refreshInputs(CarbonTime currentTime);
    UsbxBool refreshOutputs(CarbonTime currentTime);
};

#endif
