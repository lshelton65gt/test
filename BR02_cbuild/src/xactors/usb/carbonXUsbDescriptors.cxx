/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:      carbonXUsbDescriptors.cpp
    Purpose:   This file provides definition of APIs for USB descriptors
               Object Routines.
*/

#include "usbXDescriptors.h"
#include "xactors/usb/carbonXUsbDescriptors.h"

CarbonUInt8 *carbonXUsbCreateBytesFromDeviceDescriptor(
            const CarbonXUsbDeviceDescriptor *descriptor)
{
    CarbonUInt8 *bytes = NEWARRAY(CarbonUInt8, 18);
    CarbonUInt8 *base = bytes; // storing the base address of the array
    if (bytes == NULL)
        return NULL;
    usbxGetBytesFromDescriptor(bytes, descriptor); // 'bytes' gets incremented
                                               //  after this routine call
    return base; // returning the base address of the descriptor byte array
}

CarbonXUsbDeviceDescriptor *carbonXUsbGetDeviceDescriptorFromBytes(
            const CarbonUInt8 *byteArray)
{
    if (byteArray == NULL)
        return NULL;
    CarbonXUsbDeviceDescriptor *descriptor = NEW(CarbonXUsbDeviceDescriptor);
    if (descriptor == NULL)
        return NULL;
    usbxGetDescriptorFromBytes(byteArray, descriptor);
    return descriptor;
}

CarbonUInt32 carbonXUsbDestroyDeviceDescriptor(
                CarbonXUsbDeviceDescriptor *descriptor)
{
    if (descriptor == NULL)
        return 0;
    usbxDestroyDescriptor(descriptor);
    return 1;
}

CarbonUInt8 *carbonXUsbCreateBytesFromConfigurationDescriptor(
        const CarbonXUsbConfigurationDescriptor *descriptor)
{
    if (descriptor == NULL)
        ASSERT(0, NULL);
    CarbonUInt8 *bytes = NEWARRAY(CarbonUInt8, descriptor->wTotalLength);
    CarbonUInt8 *base = bytes; // storing the base address of the array
    if (bytes == NULL)
        return NULL;
    usbxGetBytesFromDescriptor(bytes, descriptor); // 'bytes' gets incremented
                                                     //  after this routine call
    return base; // returning the base address of the descriptor byte array
}

CarbonXUsbConfigurationDescriptor
    *carbonXUsbGetConfigurationDescriptorFromBytes(
            const CarbonUInt8 *byteArray)
{
    if (byteArray == NULL)
        ASSERT(0, NULL);
    CarbonXUsbConfigurationDescriptor *descriptor =
        NEW(CarbonXUsbConfigurationDescriptor);
    if (descriptor == NULL)
        return NULL;
    usbxGetDescriptorFromBytes(byteArray, descriptor);
    return descriptor;
}

CarbonUInt32 carbonXUsbDestroyConfigurationDescriptor(
        CarbonXUsbConfigurationDescriptor *descriptor)
{
    if (descriptor == NULL)
        return 0;
    usbxDestroyDescriptor(descriptor);
    return 1;
}

CarbonUInt8 *carbonXUsbCreateBytesFromInterfaceDescriptor(
        const CarbonXUsbInterfaceDescriptor *descriptor)
{
    if (descriptor == NULL)
        return 0;
    // size of USB interface descriptor =
    // 9 bytes for the interface descriptor members +
    // 7 bytes for each of the endpoint descriptors
    CarbonUInt32 size = 9 + 7 * descriptor->bNumEndpoints;
    CarbonUInt8 *bytes = NEWARRAY(CarbonUInt8, size);
    if (bytes == NULL)
        return NULL;
    CarbonUInt8 *base = bytes; // creating backup for the allocated array
    usbxGetBytesFromDescriptor(bytes, descriptor);// bytes gets incremented
    return base;
}

CarbonXUsbInterfaceDescriptor *carbonXUsbGetInterfaceDescriptorFromBytes(
        const CarbonUInt8 *byteArray)
{
    if (byteArray == NULL)
        ASSERT(0, NULL);
    CarbonXUsbInterfaceDescriptor *descriptor =
        NEW(CarbonXUsbInterfaceDescriptor);
    if (descriptor == NULL)
        return NULL;
    usbxGetDescriptorFromBytes(byteArray, descriptor);
    return descriptor;
}

CarbonUInt32 carbonXUsbDestroyInterfaceDescriptor(
        CarbonXUsbInterfaceDescriptor *descriptor)
{
    if (descriptor == NULL)
        return 0;
    usbxDestroyDescriptor(descriptor);
    return 1;
}

CarbonUInt8 *carbonXUsbCreateBytesFromEndpointDescriptor(
        const CarbonXUsbEndpointDescriptor *descriptor)
{
    if (descriptor == NULL)
        ASSERT(0, NULL);
    CarbonUInt8 *bytes = NEWARRAY(CarbonUInt8, 7);
    if (bytes == NULL)
        return NULL;
    CarbonUInt8 *base = bytes; // creating backup for the allocated array
    usbxGetBytesFromDescriptor(bytes, descriptor); // bytes gets incremented
    return base;
}

CarbonXUsbEndpointDescriptor *carbonXUsbGetEndpointDescriptorFromBytes(
        const CarbonUInt8 *byteArray)
{
    if (byteArray == NULL)
        ASSERT(0, NULL);
    CarbonXUsbEndpointDescriptor *descriptor =
        NEW(CarbonXUsbEndpointDescriptor);
    if (descriptor == NULL)
        return NULL;
    usbxGetDescriptorFromBytes(byteArray, descriptor);
    return descriptor;
}

CarbonUInt32 carbonXUsbDestroyEndpointDescriptor(
                CarbonXUsbEndpointDescriptor *descriptor)
{
    if (descriptor == NULL)
        return 0;
    usbxDestroyDescriptor(descriptor);
    return 1;
}

CarbonUInt8 *carbonXUsbCreateBytesFromOtgDescriptor(
        const CarbonXUsbOtgDescriptor *descriptor)
{
    if (descriptor == NULL)
        ASSERT(0, NULL);
    CarbonUInt8 *bytes = NEWARRAY(CarbonUInt8, 3);
    if (bytes == NULL)
        return NULL;
    CarbonUInt8 *base = bytes; // creating backup for the allocated array
    usbxGetBytesFromDescriptor(bytes, descriptor); // bytes gets incremented
    return base;
}

CarbonXUsbOtgDescriptor *carbonXUsbGetOtgDescriptorFromBytes(
        const CarbonUInt8 *byteArray)
{
    if (byteArray == NULL)
        ASSERT(0, NULL);
    CarbonXUsbOtgDescriptor *descriptor = NEW(CarbonXUsbOtgDescriptor);
    if (descriptor == NULL)
        return NULL;
    usbxGetDescriptorFromBytes(byteArray, descriptor);
    return descriptor;
}

CarbonUInt32 carbonXUsbDestroyOtgDescriptor(
        CarbonXUsbOtgDescriptor *descriptor)
{
    if (descriptor == NULL)
        return 0;
    usbxDestroyDescriptor(descriptor);
    return 1;
}

CarbonUInt32 carbonXUsbCheckDescriptor(CarbonUInt8 *byteArray,
        CarbonXUsbSpeedType speed)
{
    if (byteArray == NULL)
        ASSERT(0, NULL);
    if (!usbxCheckDescriptor(byteArray, speed))
        return 0;
    return 1;
}
