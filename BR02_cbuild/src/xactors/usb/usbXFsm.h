/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_FSM_H
#define USBX_FSM_H

#include "usbXDefines.h"

/*!
 * \file usbXFsm.h
 * \brief Header file for FSM (Finite State Machine).
 *
 * This file provides the base class definition for FSM
 */

/*!
 * \brief Timing drive type
 */
enum UsbxDriveTimingType
{
    UsbxDriveTiming_None,
    UsbxDriveTiming_Suspend,
    UsbxDriveTiming_Resume,
    UsbxDriveTiming_RemoteWakeUp
};

/*!
 * \brief Base class for FSM.
 */
class UsbxFsm
{
public: CARBONMEM_OVERRIDES

protected:

    UsbxDriveTimingType _driveTiming;

public:

    // Constructor and destructor
    UsbxFsm(): _driveTiming(UsbxDriveTiming_None) {}
    virtual ~UsbxFsm() {}

    // Evaluation
    virtual UsbxBool evaluate(CarbonTime currentTime) = 0;

    // FSM related utility functions
    static UInt getBusTurnaroundTime(UsbxBool isFS, UInt busWidth)
    /* Bus turn-around time: Host or device transmits a packet and will
     * timeout within this time range if a response is not received
     * For HS, Bus Turnaround Time = 736-816 HS bit times = 92-102 clock
     * For FS, Bus Turnaround Time = 16-18   FS bit times = 80-90  clock
     * Since, for FS this is the time between EOP to SYNC, this will
     * be added with two 8 bit times i.e. 2 * 8 * 5 = 80 clocks.
     * Note that 8 HS bit times in one clock cycle, and 5 clock cycles
     * in one FS bit time. Clock is considered to be 60 MHz for HS/FS UTMI.
     * For 30 MHz clock this time will be divided by 2. */
    { return (isFS ? (90 + 80) : 102)/(busWidth/8); }
    void setDriveTiming(UsbxDriveTimingType timing)
    { _driveTiming = timing; }

    //! Reset for detach
    virtual void resetForDetach() = 0;

    // Debug function that dumps the FSM state
    virtual void print(CarbonTime currentTime) const = 0;
};

#endif
