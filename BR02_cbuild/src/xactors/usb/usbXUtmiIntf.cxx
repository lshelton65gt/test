/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
      File    :  usbXUtmiIntf.cpp
      Purpose :  Implements UTMI Link and PHY interface pins and FSMs.
*/

#include "usbXUtmiIntf.h"
#include "usbXtor.h"

// #define DEBUG_PACKET
// #define DEBUG
// #define DRIVE_SMALL_CHIRPS
// #define DEBUG_SUSPEND_RESUME

#define _NO_TXREADY_FOR_CHIRP

/* ----------------------------------------------------------------------
 * Name             HS          FS              Definition
 *              (bit times) (bit times)
 * =====================================================================-
 * Transmit       88-192      2-6.5     When a host sends two consecutive
 * -Transmit   (11-24 clk) (10-32 clk)  USB packets to a device, it must
 * (host only)          ####            meet these timings.
 * ----------------------------------------------------------------------
 * Receive        32-192      2-6.5     Device will timeout if consecutive
 * -Receive     (4-24 clk) (10-32 clk)  USB inter-packet timing falls
 * (device only)        ####            outside this range.
 * ----------------------------------------------------------------------
 * Receive         8-192      2-6.5     Host or device receives a packet
 * -Transmit    (1-24 clk) (10-32 clk)  and must transmit a response within
 * (host or device)     ####            the given timing range.
 * ----------------------------------------------------------------------
 * Transmit       736-816     16-18     Host or device transmits a packet
 * -Receive    (92-102 clk)(80-90 clk)  and will timeout within this range
 * (host or device)                     if a response is not received.
 *                                      (**** Bus Turn-around Time ****)
 * ----------------------------------------------------------------------
 * Note that 8 HS bit times in one clock cycle, and 5 clock cycles,
 * in one FS bit time. Clock is considered to be 60 MHz for HS/FS UTMI.
 * For 30 MHz clock this time will be divided by 2.
 */
// All below time values are w.r.t. 60 MHz i.e. 1 us = 60 clocks
const UInt UsbxUtmiIntf::_clocksForInterPacketDelay = 16; // #### Range: 11 - 20
const UInt UsbxUtmiIntf::_clocksPerFSBit = 5;
// Reset timings
const UInt UsbxUtmiIntf::_clocksForAttach = 150; // 2.5 us
const UInt UsbxUtmiIntf::_clocksForResetSE0 = 600000; // 10 ms
const UInt UsbxUtmiIntf::_clocksToStartDeviceChirpK = 240000; // 4 ms
const UInt UsbxUtmiIntf::_clocksForDeviceChirpDetect = 420000; // 7 ms
const UInt UsbxUtmiIntf::_clocksForDeviceChirpKMin = 60000; // 1 ms
const UInt UsbxUtmiIntf::_clocksForDeviceChirpK = // 1-7 ms
#ifdef DRIVE_SMALL_CHIRPS
            _clocksForDeviceChirpKMin;
#else
            _clocksForDeviceChirpDetect - _clocksToStartDeviceChirpK;
#endif
const UInt UsbxUtmiIntf::_clocksToStartHostChirp = 6000; // 100 us
const UInt UsbxUtmiIntf::_clocksForHostChirpMin = 2400; // 40 us
const UInt UsbxUtmiIntf::_clocksForHostChirpMax = 3600; // 60 us
const UInt UsbxUtmiIntf::_numHostChirpsMin = 6; // 6 chirps
const UInt UsbxUtmiIntf::_clocksForSE0AfterHostChirp = 18000; // 300 us
                                // - should be between 100 us to 500 us
const UInt UsbxUtmiIntf::_clocksForHostChirpDetect = 150000; // 2.5 ms
// Suspend Resume timings
const UInt UsbxUtmiIntf::_clocksForDeviceSuspendDetect = 180000; // 3.0 ms
const UInt UsbxUtmiIntf::_clocksToDriveSuspend = 600000; // 10.0 ms
const UInt UsbxUtmiIntf::_clocksForResumeChirpK = 1200000; // 20.0 ms
const UInt UsbxUtmiIntf::_clocksForSE0AfterResume = 90; // 1.25 to 1.5 us
const UInt UsbxUtmiIntf::_clocksToDecipherSuspendReset = 6000; // 100 to 875 us
const UInt UsbxUtmiIntf::_clocksForHSDeviceToEnableFSTerminations = 7500;
                                                                // max 125 us
const UInt UsbxUtmiIntf::_clocksForHostToDetectRemoteWakeUp = 54000; // 900 us
const UInt UsbxUtmiIntf::_clocksForRemoteWakeupChirpK = 600000; // 1 to 15 ms
const UInt UsbxUtmiIntf::_minClocksBeforeRemoteWakeupDrive = 300000; // 5 ms
// OTG SRP timing
const UInt UsbxUtmiIntf::_clocksForSrp = 6000000; // 100 ms
const UInt UsbxUtmiIntf::_clocksForSrpDataLinePulse = 600000; // 10 ms
const UInt UsbxUtmiIntf::_clocksForSrpVbusCharge = 1800000; // 30 ms
const UInt UsbxUtmiIntf::_clocksForSrpVbusDischarge = 3000000; // 50 ms

UsbxUtmiIntf::UsbxUtmiIntf(UsbXtor *xtor, UInt dataBusSize,
        CarbonXUsbUtmiPinLevel intfLevel):
    UsbxIntf(xtor), _dataBusSize(dataBusSize), _interfaceLevel(intfLevel),
    _state(UsbxUtmiFsm_IDLE),
    _txPacket(NULL), _rxPacket(NULL), _byteIndex(0),
    _rxError(UsbxFalse), _waitCount(0),
    _driveTiming(UsbxTiming_None), _sensedTiming(UsbxTiming_None),
    _clockCountForTiming(0),
    _chirpState(UsbxUtmiChirp_IDLE), _clockCountForChirp(0),
    _hostChirpCount(0), _numHostChirps(_numHostChirpsMin * 2),
    _lineStatePeriod(0), _lastLineState(UsbxUtmiLineState_SE0),
    _suspendState(UsbxUtmiSuspend_Idle), _clockCountForSuspendTiming(0),
    _kDriveCount(0), _suspendWait(0),
    _vbusValue(UsbxUtmiVbus_VB_SESS_END),
    _clockCountForVbusCharge(0), _clockCountForVbusDischarge(0),
    _isUlpi(UsbxFalse),
    _Reset(NULL), _DataBus16_8(NULL), _SuspendM(NULL),
    _DataInLo(NULL), _DataInHi(NULL),
    _TXValid(NULL), _TXValidH(NULL), _TXReady(NULL),
    _DataOutLo(NULL), _DataOutHi(NULL),
    _RXValid(NULL), _RXValidH(NULL), _RXActive(NULL), _RXError(NULL),
    _XcvrSelect(NULL), _TermSelect(NULL), _OpMode(NULL), _LineState(NULL),
    _DpPulldown(NULL), _DmPulldown(NULL), _HostDisconnect(NULL),
    _IdPullup(NULL), _IdDig(NULL),
    _AValid(NULL), _BValid(NULL), _VbusValid(NULL), _SessEnd(NULL),
    _DrvVbus(NULL), _ChrgVbus(NULL), _DischrgVbus(NULL)
{
}

UsbxUtmiIntf::~UsbxUtmiIntf()
{
    delete _rxPacket;

    delete _Reset;
    delete _DataBus16_8;
    delete _SuspendM;
    delete _DataInLo;
    delete _DataInHi;
    delete _TXValid;
    delete _TXValidH;
    delete _TXReady;
    delete _DataOutLo;
    delete _DataOutHi;
    delete _RXValid;
    delete _RXValidH;
    delete _RXActive;
    delete _RXError;
    delete _XcvrSelect;
    delete _TermSelect;
    delete _OpMode;
    delete _LineState;
    delete _DpPulldown;
    delete _DmPulldown;
    delete _HostDisconnect;
    delete _IdPullup;
    delete _IdDig;
    delete _AValid;
    delete _BValid;
    delete _VbusValid;
    delete _SessEnd;
    delete _DrvVbus;
    delete _ChrgVbus;
    delete _DischrgVbus;
}

// Packet access functions
UsbxBool UsbxUtmiIntf::transmitPacket(UsbxPacket *packet)
{
    if (_txPacket != NULL)
        return UsbxFalse;
#ifdef DEBUG_PACKET
    if (_xtor != NULL)
    {
        printf("DEBUG: %s -> %s", _xtor->getName(),
                packet->getPacketTypeString());
        if (packet->getType() == UsbxPacket_SETUP ||
                packet->getType() == UsbxPacket_OUT ||
                packet->getType() == UsbxPacket_IN ||
                packet->getType() == UsbxPacket_PING)
        {
            UsbxTokenPacket *tokenPacket =
                static_cast<UsbxTokenPacket*>(packet);
            printf(" (EP %d)", tokenPacket->getEndpointNumber());
        }
        printf("\n");
    }
#endif
    _txPacket = packet;
    return UsbxTrue;
}

UsbxBool UsbxUtmiIntf::transmitComplete() const
{
    return (_txPacket == NULL);
}

UsbxPacket* UsbxUtmiIntf::receivePacket()
{
    UsbxPacket *returnPacket = _rxPacket;
    _rxPacket = NULL;
    return returnPacket;
}

// Timing access functions
UsbxBool UsbxUtmiIntf::startTiming(UsbxTimingType timing)
{
    if (_driveTiming != UsbxTiming_None)
        return UsbxFalse;
    _driveTiming = timing;
    return UsbxTrue;
}

UsbxBool UsbxUtmiIntf::timingComplete() const
{
    return (_driveTiming == UsbxTiming_None);
}

UsbxBool UsbxUtmiIntf::senseTiming(UsbxTimingType expectedTiming,
        UsbxBool flush)
{
    if (_sensedTiming == expectedTiming)
    {
        _sensedTiming = UsbxTiming_None;
        return UsbxTrue;
    }
    if (flush)
        _sensedTiming = UsbxTiming_None;
    return UsbxFalse;
}

UsbxBool UsbxUtmiIntf::isVbusValid() const
{
    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_0)
        return UsbxTrue;
    return _VbusValid->read(0);
}

void UsbxUtmiIntf::resetForDetach()
{
    _state = UsbxUtmiFsm_IDLE;
    _txPacket = NULL; _rxPacket = NULL; _byteIndex = 0;
    _rxError = UsbxFalse; _waitCount = 0;
    _driveTiming = UsbxTiming_None; _sensedTiming = UsbxTiming_None;
    _clockCountForTiming = 0;
    _chirpState = UsbxUtmiChirp_IDLE; _clockCountForChirp = 0;
    _hostChirpCount = 0; _numHostChirps = _numHostChirpsMin * 2;
    _lineStatePeriod = 0; _lastLineState = UsbxUtmiLineState_SE0;
    _suspendState = UsbxUtmiSuspend_Idle; _clockCountForSuspendTiming = 0;
    _kDriveCount = 0; _suspendWait = 0;
    _vbusValue = UsbxUtmiVbus_VB_SESS_END;
    _clockCountForVbusCharge = 0; _clockCountForVbusDischarge = 0;
}

UsbxPin* UsbxUtmiIntf::getPin(UsbxConstString pinName) const
{
    if (strcmp(pinName, "Reset") == 0)
        return _Reset;
    if (strcmp(pinName, "DataBus16_8") == 0)
        return _DataBus16_8;
    if (strcmp(pinName, "SuspendM") == 0)
        return _SuspendM;
    if (strcmp(pinName, "DataInLo") == 0)
        return _DataInLo;
    if (strcmp(pinName, "DataInHi") == 0)
        return _DataInHi;
    if (strcmp(pinName, "TXValid") == 0)
        return _TXValid;
    if (strcmp(pinName, "TXValidH") == 0)
        return _TXValidH;
    if (strcmp(pinName, "TXReady") == 0)
        return _TXReady;
    if (strcmp(pinName, "DataOutLo") == 0)
        return _DataOutLo;
    if (strcmp(pinName, "DataOutHi") == 0)
        return _DataOutHi;
    if (strcmp(pinName, "RXValid") == 0)
        return _RXValid;
    if (strcmp(pinName, "RXValidH") == 0)
        return _RXValidH;
    if (strcmp(pinName, "RXActive") == 0)
        return _RXActive;
    if (strcmp(pinName, "RXError") == 0)
        return _RXError;
    if (strcmp(pinName, "XcvrSelect") == 0)
        return _XcvrSelect;
    if (strcmp(pinName, "TermSelect") == 0)
        return _TermSelect;
    if (strcmp(pinName, "OpMode") == 0)
        return _OpMode;
    if (strcmp(pinName, "LineState") == 0)
        return _LineState;
    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_0)
        return NULL;

    // Level 1 pins
    if (strcmp(pinName, "DpPulldown") == 0)
        return _DpPulldown;
    if (strcmp(pinName, "DmPulldown") == 0)
        return _DmPulldown;
    if (strcmp(pinName, "HostDisconnect") == 0)
        return _HostDisconnect;
    if (strcmp(pinName, "IdPullup") == 0)
        return _IdPullup;
    if (strcmp(pinName, "IdDig") == 0)
        return _IdDig;
    if (strcmp(pinName, "AValid") == 0)
        return _AValid;
    if (strcmp(pinName, "BValid") == 0)
        return _BValid;
    if (strcmp(pinName, "VbusValid") == 0)
        return _VbusValid;
    if (strcmp(pinName, "SessEnd") == 0)
        return _SessEnd;
    if (strcmp(pinName, "DrvVbus") == 0)
        return _DrvVbus;
    if (strcmp(pinName, "ChrgVbus") == 0)
        return _ChrgVbus;
    if (strcmp(pinName, "DischrgVbus") == 0)
        return _DischrgVbus;
    return NULL;
}

UsbxUtmiLinkIntf::UsbxUtmiLinkIntf(UsbXtor *xtor, UInt dataBusSize,
        CarbonXUsbUtmiPinLevel intfLevel):
    UsbxUtmiIntf(xtor, dataBusSize, intfLevel)
{
    _Reset = createPin(xtor, "Reset", UsbxDirection_Output);
    _DataBus16_8 = createPin(xtor, "DataBus16_8", UsbxDirection_Output);
    _SuspendM = createPin(xtor, "SuspendM", UsbxDirection_Output);
    _DataInLo = createPin(xtor, "DataInLo", UsbxDirection_Output, 8);
    _DataInHi = createPin(xtor, "DataInHi", UsbxDirection_Output, 8);
    _TXValid = createPin(xtor, "TXValid", UsbxDirection_Output);
    _TXValidH = createPin(xtor, "TXValidH", UsbxDirection_Output);

    _TXReady = createPin(xtor, "TXReady", UsbxDirection_Input);
    _DataOutLo = createPin(xtor, "DataOutLo", UsbxDirection_Input, 8);
    _DataOutHi = createPin(xtor, "DataOutHi", UsbxDirection_Input, 8);
    _RXValid = createPin(xtor, "RXValid", UsbxDirection_Input);
    _RXValidH = createPin(xtor, "RXValidH", UsbxDirection_Input);
    _RXActive = createPin(xtor, "RXActive", UsbxDirection_Input);
    _RXError = createPin(xtor, "RXError", UsbxDirection_Input);

    _XcvrSelect = createPin(xtor, "XcvrSelect", UsbxDirection_Output);
    _TermSelect = createPin(xtor, "TermSelect", UsbxDirection_Output);
    _OpMode = createPin(xtor, "OpMode", UsbxDirection_Output, 2);

    _LineState = createPin(xtor, "LineState", UsbxDirection_Input, 2);

    // Drive with initial value
    _XcvrSelect->write(1, 0);
    _TermSelect->write(1, 0);
    _OpMode->write(0, 0);
    _SuspendM->write(1, 0);

    // Level 1 UTMI pins
    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_1)
    {
        _DpPulldown = createPin(xtor, "DpPulldown", UsbxDirection_Output);
        _DmPulldown = createPin(xtor, "DmPulldown", UsbxDirection_Output);
        _HostDisconnect = createPin(xtor, "HostDisconnect",
                UsbxDirection_Input);

        _IdPullup = createPin(xtor, "IdPullup", UsbxDirection_Output);
        _IdDig = createPin(xtor, "IdDig", UsbxDirection_Input);

        _AValid = createPin(xtor, "AValid", UsbxDirection_Input);
        _BValid = createPin(xtor, "BValid", UsbxDirection_Input);
        _VbusValid = createPin(xtor, "VbusValid", UsbxDirection_Input);
        _SessEnd = createPin(xtor, "SessEnd", UsbxDirection_Input);

        _DrvVbus = createPin(xtor, "DrvVbus", UsbxDirection_Output);
        _ChrgVbus = createPin(xtor, "ChrgVbus", UsbxDirection_Output);
        _DischrgVbus = createPin(xtor, "DischrgVbus", UsbxDirection_Output);
    }
}

void UsbxUtmiLinkIntf::setSpeedFS(UsbxBool isFS, CarbonTime currentTime)
{
    UsbxIntf::setSpeedFS(isFS, currentTime);
    _XcvrSelect->write(isFS, currentTime);
    _TermSelect->write(isFS, currentTime);
}

UsbxBool UsbxUtmiLinkIntf::driveVbus(UsbxBool isOn)
{
    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_0)
        return UsbxFalse;
    return _DrvVbus->write(isOn, 0);
    return UsbxTrue;
}

void UsbxUtmiLinkIntf::resetForDetach()
{
    UsbxUtmiIntf::resetForDetach();

    _XcvrSelect->write(1, 0);
    _TermSelect->write(1, 0);
    _OpMode->write(0, 0);
    _SuspendM->write(1, 0);
}

// UTMI Link FSM
UsbxBool UsbxUtmiLinkIntf::evaluate(CarbonTime currentTime)
{
    UInt speedFactor = _dataBusSize / 8;

    // Drives for Dp/Dm Pulldown
    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_1)
    {
        switch (_xtor->getType())
        {
            case CarbonXUsb_Host:
            {
                _DpPulldown->write(1, currentTime);
                _DmPulldown->write(1, currentTime);
                _IdPullup->write(0, currentTime);
                _DrvVbus->write(1, currentTime);
                _ChrgVbus->write(0, currentTime);
                _DischrgVbus->write(0, currentTime);
                break;
            }
            case CarbonXUsb_Device:
            {
                _DpPulldown->write(0, currentTime);
                _DmPulldown->write(0, currentTime);
                _IdPullup->write(0, currentTime);
                _DrvVbus->write(0, currentTime);
                _ChrgVbus->write(0, currentTime);
                _DischrgVbus->write(0, currentTime);
                break;
            }
            case CarbonXUsb_OtgDevice:
            {
                _DpPulldown->write(_xtor->getOtgDeviceType() == UsbxOtgDevice_A,
                        currentTime); 
                _DmPulldown->write(_xtor->getOtgDeviceType() == UsbxOtgDevice_A,
                        currentTime); 

                if (_IdPullup->read(currentTime))
                    const_cast<UsbXtor*>(_xtor)->setOtgDeviceType(
                            _IdDig->read(currentTime) ? UsbxOtgDevice_B :
                            UsbxOtgDevice_A);
                else
                    _IdPullup->write(1, currentTime);

                if (_xtor->getOtgDeviceType() == UsbxOtgDevice_A)
                {
                    _ChrgVbus->write(0, currentTime); 
                    _DischrgVbus->write(0, currentTime); 
                }

                // Sense SRP using Vbus pulsing
                if (!_VbusValid->read(currentTime) &&
                        _AValid->read(currentTime))
                    _sensedTiming = UsbxTiming_OtgSrp;
                break;
            }
            default: ASSERT(0, _xtor); break;
        }
    }

    // Timing detection
    UsbxUtmiLineStateType lineState = static_cast<UsbxUtmiLineStateType>(
            _LineState->read(currentTime));
    if (lineState == _lastLineState)
        _lineStatePeriod++;
    else
        _lineStatePeriod = 1;
    _lastLineState = lineState;

    if (_isFS && _lineStatePeriod == _clocksForAttach / speedFactor)
    {
        // For FS, detect SE0 for 2.5 us for device detach
        if (lineState == UsbxUtmiLineState_SE0) // SE0
        {
            _sensedTiming = UsbxTiming_DeviceDetach;
            _isFS = UsbxTrue;
            _state = UsbxUtmiFsm_DETACHED;
        }
        else // For FS, detect non-SE0 for 2.5 us for device attach
        {
            _sensedTiming = UsbxTiming_DeviceAttach;
            if (_driveTiming == UsbxTiming_None)
            {
                _isFS = UsbxTrue;
                _state = UsbxUtmiFsm_IDLE;
                _XcvrSelect->write(1, currentTime);
                _TermSelect->write(1, currentTime);
                _OpMode->write(0, currentTime);
            }
        }
    }
    else if (!_isFS && _interfaceLevel == CarbonXUsbUtmiPinLevel_1)
    {
        // For HS and UTMI level 1, check for HostDisconnect to sense detach
        if (_DpPulldown->read(currentTime) && _DmPulldown->read(currentTime) &&
                _HostDisconnect->read(currentTime))
        {
            _sensedTiming = UsbxTiming_DeviceDetach;
            _isFS = UsbxTrue;
            _state = UsbxUtmiFsm_DETACHED;
        }
    }

    // Drive timing
    if (_driveTiming != UsbxTiming_None)
    {
        _state = UsbxUtmiFsm_IDLE;

        // Chirp state machine
        switch (_chirpState)
        {
            case UsbxUtmiChirp_IDLE:
            {
                _clockCountForChirp = 0;
                _hostChirpCount = 0;
                _OpMode->write(0, currentTime);
                break;
            }
            // Chirp FSM for HOST LINK interface
            case UsbxUtmiChirp_CheckDeviceK:
            {
                if (lineState == UsbxUtmiLineState_K && _lineStatePeriod >=
                    (_clocksForDeviceChirpKMin / _timingFactor) / speedFactor)
                {
                    // Host has: a device detected as HS
#ifdef DEBUG
                    printf("DEBUG[%d]: HOST: Device detected as HS.\n",
                            static_cast<UInt>(currentTime));
#endif
                    _isFS = UsbxFalse;
                    _sensedTiming = UsbxTiming_HsDetected;
                    _chirpState = UsbxUtmiChirp_DriveHostHsChirps;
                }
                else if (lineState == UsbxUtmiLineState_SE0)
                {
                    if (!_isFS)
                        _chirpState = UsbxUtmiChirp_DriveHostHsChirps;
                    else if (_lineStatePeriod >= (_clocksForDeviceChirpDetect /
                            _timingFactor) / speedFactor)
                        _chirpState = UsbxUtmiChirp_IDLE;
                }
                break;
            }
            case UsbxUtmiChirp_DriveHostHsChirps:
            {
                if (lineState == UsbxUtmiLineState_SE0 && _lineStatePeriod >=
                    (_clocksToStartHostChirp / _timingFactor) / speedFactor
                    - speedFactor)
                {
                    _chirpState = UsbxUtmiChirp_DriveHostK;
                    _hostChirpCount = 0;
                    _numHostChirps = (_clockCountForTiming * _timingFactor
                            * speedFactor - _clocksForSE0AfterHostChirp) /
#ifdef DRIVE_SMALL_CHIRPS
                            (_clocksForHostChirpMin * 2);
#else
                            (_clocksForHostChirpMax * 2);
#endif
#ifdef DEBUG
                    printf("DEBUG[%d]: Host to drive %d K-J chirps.\n",
                            static_cast<UInt>(currentTime), _numHostChirps);
#endif
                }
                break;
            }
            case UsbxUtmiChirp_DriveHostK:
            {
                // Generate Chirp K by driving all zeros at OpMode=2
                _XcvrSelect->write(0, currentTime);
                _TermSelect->write(0, currentTime);
                _OpMode->write(2, currentTime);
                _DataInLo->write(0, currentTime);
                _TXValid->write(1, currentTime);
                if (lineState == UsbxUtmiLineState_K && _lineStatePeriod >=
#ifdef DRIVE_SMALL_CHIRPS
                    (_clocksForHostChirpMin / _timingFactor) / speedFactor - 2)
#else
                    (_clocksForHostChirpMax / _timingFactor) / speedFactor - 2)
#endif
                    _chirpState = UsbxUtmiChirp_DriveHostJ;
                break;
            }
            case UsbxUtmiChirp_DriveHostJ:
            {
                // Generate Chirp K by driving all ones at OpMode=2
                _XcvrSelect->write(0, currentTime);
                _TermSelect->write(0, currentTime);
                _OpMode->write(2, currentTime);
                _DataInLo->write(0xFF, currentTime);
                _TXValid->write(1, currentTime);
                if (lineState == UsbxUtmiLineState_J && _lineStatePeriod >=
#ifdef DRIVE_SMALL_CHIRPS
                    (_clocksForHostChirpMin / _timingFactor) / speedFactor - 2)
#else
                    (_clocksForHostChirpMax / _timingFactor) / speedFactor - 2)
#endif
                {
                    _hostChirpCount++;
                    if (_hostChirpCount >= _numHostChirps)
                    {
                        // HS detection complete
                        _OpMode->write(0, currentTime);
                        _DataInLo->write(0, currentTime);
                        _TXValid->write(0, currentTime);
                        _chirpState = UsbxUtmiChirp_IDLE;
                        _numHostChirps = _numHostChirpsMin * 2;
                    }
                    else
                        _chirpState = UsbxUtmiChirp_DriveHostK;
                }
                break;
            }
            // Chirp FSM for DEVICE LINK interface
            case UsbxUtmiChirp_WaitForResetFromHost:
            {
                if (lineState == UsbxUtmiLineState_SE0 && _lineStatePeriod >=
                    (_clocksToStartDeviceChirpK / _timingFactor) / speedFactor
                    - 2)
                {
                    // Drive K chirp
                    _XcvrSelect->write(0, currentTime);
                    _TermSelect->write(1, currentTime);
                    _OpMode->write(2, currentTime);
                    _DataInLo->write(0, currentTime);
                    _TXValid->write(1, currentTime);
                    _chirpState = UsbxUtmiChirp_DriveDeviceK;
                }
                break;
            }
            case UsbxUtmiChirp_DriveDeviceK:
            {
                if (lineState == UsbxUtmiLineState_K && _lineStatePeriod >=
                    (_clocksForDeviceChirpK / _timingFactor) / speedFactor - 1)
                {
                    _TXValid->write(0, currentTime);
                    _hostChirpCount = 0;
                    _chirpState = UsbxUtmiChirp_CheckHostK;
                }
                break;
            }
            case UsbxUtmiChirp_CheckHostK:
            {
                if (lineState == UsbxUtmiLineState_K && _lineStatePeriod >=
                    (_clocksForHostChirpMin / _timingFactor) / speedFactor)
                    _chirpState = UsbxUtmiChirp_CheckHostJ;
                break;
            }
            case UsbxUtmiChirp_CheckHostJ:
            {
                if (lineState == UsbxUtmiLineState_J && _lineStatePeriod >=
                    (_clocksForHostChirpMin / _timingFactor) / speedFactor)
                {
                    _hostChirpCount++;
                    if (_hostChirpCount >= _numHostChirpsMin)
                    {
                        // Device: host detected as HS
#ifdef DEBUG
                        printf("DEBUG[%d]: DEVICE: Host detected as HS.\n",
                                static_cast<UInt>(currentTime));
#endif
                        _isFS = UsbxFalse;
                        _sensedTiming = UsbxTiming_HsDetected;
                        _chirpState = UsbxUtmiChirp_IDLE;
                    }
                    else
                        _chirpState = UsbxUtmiChirp_CheckHostK;
                }
                break;
            }
            // OTG SRP timing FSM
            case UsbxUtmiChirp_SrpDataLinePulse:
            {
                if (_clockCountForChirp >= (_clocksForSrpDataLinePulse /
                            _timingFactor) / speedFactor - 1)
                {
                    _chirpState = UsbxUtmiChirp_SrpVbusCharge;

                    // Stop data line pulsing
                    _XcvrSelect->write(0, currentTime);
                    _TermSelect->write(0, currentTime);

                    // Start Vbus pulsing
                    _ChrgVbus->write(1, currentTime);
                    _DischrgVbus->write(0, currentTime);
                    _clockCountForChirp = 0;
                }
                else
                {
                    // Data line pulsing
                    _XcvrSelect->write(0, currentTime);
                    _TermSelect->write(1, currentTime);

                    _clockCountForChirp++;
                }
                break;
            }
            case UsbxUtmiChirp_SrpVbusCharge:
            {
                if (_clockCountForChirp >=
                        (((_clocksForSrpVbusCharge * 12) / 10) // 20% tol
                        / _timingFactor) / speedFactor - 1)
                {
                    _chirpState = UsbxUtmiChirp_SrpVbusDischarge;
                    _ChrgVbus->write(0, currentTime);
                    _DischrgVbus->write(1, currentTime);
                    _clockCountForChirp = 0;
                }
                else
                    _clockCountForChirp++;
                break;
            }
            case UsbxUtmiChirp_SrpVbusDischarge:
            {
                if (_clockCountForChirp >=
                        (((_clocksForSrpVbusDischarge * 12) / 10) // 20% tol
                            / _timingFactor) / speedFactor - 1)
                {
                    _chirpState = UsbxUtmiChirp_IDLE;
                    _ChrgVbus->write(0, currentTime);
                    _DischrgVbus->write(0, currentTime);
                    _clockCountForChirp = 0;
                }
                else
                    _clockCountForChirp++;
                break;
            }
            default: break;
        }

        // Wait for timing to complete
        if (_clockCountForTiming > 0)
        {
            _clockCountForTiming--;
            if (_clockCountForTiming == 0)
            {
                _driveTiming = UsbxTiming_None;
                _OpMode->write(0, currentTime);
                _chirpState = UsbxUtmiChirp_IDLE;
            }
            return UsbxTrue;
        }
        else if (_isFS)
        {
            switch (_driveTiming)
            {
                case UsbxTiming_Suspend: break;
                case UsbxTiming_Resume: break;
                case UsbxTiming_RemoteWakeUp: break;
                case UsbxTiming_HostHsReset:
                { 
                    _chirpState = UsbxUtmiChirp_CheckDeviceK;
                    _OpMode->write(2, currentTime);
                    // No break to be provided
                }
                case UsbxTiming_HostFsReset:
                {
                    _isFS = UsbxTrue;
                    // Drive SE0 by XcvrSelect = TermSelect = 0
                    _XcvrSelect->write(0, currentTime);
                    _TermSelect->write(0, currentTime);
                    _clockCountForTiming = (_clocksForResetSE0 / _timingFactor)
                        / speedFactor - 1;
                    break;
                }
                case UsbxTiming_DeviceHsReset:
                {
                    _chirpState = UsbxUtmiChirp_WaitForResetFromHost;
                    _clockCountForTiming = (_clocksForResetSE0 / _timingFactor)
                        / speedFactor - 1;
                    break;
                }
                case UsbxTiming_DeviceFsReset:
                {
                    _isFS = UsbxTrue;
                    _driveTiming = UsbxTiming_None;
                    break;
                }
                // OTG SRP timing drive
                case UsbxTiming_OtgSrp:
                {
                    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_0)
                        break;

                    _isFS = UsbxTrue;
                    _chirpState = UsbxUtmiChirp_SrpDataLinePulse;
                    _clockCountForChirp = 0;
                    _clockCountForTiming = (((_clocksForSrp * 12) / 10)//20% tol
                        / _timingFactor) / speedFactor;
                    break;
                }
                case UsbxTiming_OtgHnp:
                {
                    _TermSelect->write(0, currentTime);
                    _driveTiming = UsbxTiming_None;
                    break;
                }
                default:
                {
                    _driveTiming = UsbxTiming_None;
                    break;
                }
            }
        }
    }

    // Suspend, Resume and Remote Wake Up LINK FSM
    switch (_suspendState)
    {
        case UsbxUtmiSuspend_Idle:
        {
            // Check for Suspend signal
            if (_lineStatePeriod < _clocksForDeviceSuspendDetect / speedFactor)
                break;
            if (_isFS && lineState == UsbxUtmiLineState_J)
            {
                // FS Suspend detected
                _sensedTiming = UsbxTiming_Suspend;
                _suspendState = UsbxUtmiSuspend_DeviceSuspended;
                // Assert SuspendM(active low)
                _SuspendM->write(0, currentTime);
#ifdef DEBUG_SUSPEND_RESUME
                printf("DEBUG[%d]: DEVICE: Detected FS Suspend\n",
                        static_cast<UInt>(currentTime));
#endif
            }
            else if (!_isFS && lineState == UsbxUtmiLineState_SE0)
            {
                // If driven SE0 is on bus, then the HOST is asserting
                // reset, else if the HOST is asserting soft SE0, it is
                // suspend. To decipher between an analogous reset, the
                // DEVICE LINK should enable FS terminations within
                // another 0.125 ms and then sample linestate within
                // next 100 to 875 us. If it is a J then the HOST is
                // driving soft SE0 and hence a suspend, else if it is
                // SE0 then it means a reset.
                if (_lineStatePeriod >= (_clocksForDeviceSuspendDetect +
                        _clocksForHSDeviceToEnableFSTerminations) / speedFactor)
                {
                    // Enable FS terminations
                    _TermSelect->write(1, currentTime);
                    _XcvrSelect->write(1, currentTime);
                    _suspendState = UsbxUtmiSuspend_CheckSuspendOrReset;
                    _suspendWait = _clocksToDecipherSuspendReset /
                        speedFactor - 1;
#ifdef DEBUG_SUSPEND_RESUME
                    printf("DEBUG[%d]: DEVICE: Enabling FS terminations\n",
                            static_cast<UInt>(currentTime));
#endif
                }
            }
            break;
        }
        // Host states
        case UsbxUtmiSuspend_DriveSuspend:
        {
            // If Suspend drive is complete, go to Suspended state
            if (lineState == UsbxUtmiLineState_J &&
                _lineStatePeriod >= (_clocksToDriveSuspend - (_isFS ? 0 :
                    _clocksForDeviceSuspendDetect)) / speedFactor)
            {
                _driveTiming = UsbxTiming_None;
                _clockCountForSuspendTiming = 0;
                _suspendState = UsbxUtmiSuspend_HostSuspended;
                // Assert SuspendM(active low)
                _SuspendM->write(0, currentTime);
#ifdef DEBUG_SUSPEND_RESUME
                printf("DEBUG[%d]: HOST: Suspend drive complete\n",
                        static_cast<UInt>(currentTime));
#endif
            }
            // Fall through to UsbxUtmiSuspend_Suspended state
        }
        case UsbxUtmiSuspend_HostSuspended:
        {
            // Check for any Remote Wake Up call from Device side
            if (lineState == UsbxUtmiLineState_K &&
                _lineStatePeriod >= (_clocksForHostToDetectRemoteWakeUp /
                    _timingFactor) / speedFactor)
            {
                _sensedTiming = UsbxTiming_RemoteWakeUp;
                // If remote wakeup detected while driving suspend
                if (_driveTiming == UsbxTiming_Suspend)
                {
                    _driveTiming = UsbxTiming_None;
                    _clockCountForSuspendTiming = 0;
#ifdef DEBUG_SUSPEND_RESUME
                    printf("DEBUG[%d]: HOST: Detected Remote Wakeup\n",
                            static_cast<UInt>(currentTime));
#endif
                }
            }
            // OTG HNP specific timing
            else if (_interfaceLevel == CarbonXUsbUtmiPinLevel_1 &&
                    _xtor->getOtgDeviceType() == UsbxOtgDevice_A &&
                    lineState == UsbxUtmiLineState_SE0)
            {
                _sensedTiming = UsbxTiming_OtgHnp;
            }
            break;
        }
        case UsbxUtmiSuspend_DriveResumeChirpK:
        {
            // Keep track of the driven K in terms of clock
#ifdef _NO_TXREADY_FOR_CHIRP
            _kDriveCount++;
#else
            if (_TXReady->read(currentTime))
                _kDriveCount += 8 * _clocksPerFSBit;
#endif
            // Drive Resume K chirp for a specified time in OpMode 2
            if (_kDriveCount < (_clocksForResumeChirpK / _timingFactor) /
                    speedFactor)
            {
                _OpMode->write(2, currentTime);
                _DataInLo->write(0, currentTime);
                _TXValid->write(1, currentTime);
#ifndef _NO_TXREADY_FOR_CHIRP
                if (_dataBusSize == 16)
                {
                    _DataInHi->write(0, currentTime);
                    _TXValidH->write(1, currentTime);
                }
#endif
                if (!_isFS)
                {
                    _TermSelect->write(1, currentTime);
                    _XcvrSelect->write(1, currentTime);
                }
            }
            // If enough K has been driven, go to drive End of Resume
            else
            {
#ifdef _NO_TXREADY_FOR_CHIRP
                _suspendState = UsbxUtmiSuspend_DriveEOR;
                _OpMode->write(2, currentTime);
                _suspendWait = 0;
#else
                _OpMode->write(0, currentTime);
                _TXValid->write(0, currentTime);
                _TXValidH->write(0, currentTime);
                _suspendState = UsbxUtmiSuspend_DriveEOR;
                _suspendWait = 8 * _clocksPerFSBit - 2;
#endif
            }
            break;
        }
        case UsbxUtmiSuspend_DriveEOR:
        {
            // Wait for timing to complete
            if (_suspendWait > 0)
            {
                _suspendWait--;
                break;
            }
#ifdef _NO_TXREADY_FOR_CHIRP
            _OpMode->write(0, currentTime);
            _TXValid->write(0, currentTime);
            _TXValidH->write(0, currentTime);
#endif
            // Drive soft SE0 by driving XcvrSelect = TermSelect = 0
            _XcvrSelect->write(0, currentTime);
            _TermSelect->write(0, currentTime);
            _suspendWait = _clocksForSE0AfterResume / speedFactor - 1;
            _suspendState = UsbxUtmiSuspend_DriveIdleAfterEOR;
            break;
        }
        case UsbxUtmiSuspend_DriveIdleAfterEOR:
        {
            // Wait for timing to complete
            if (_suspendWait > 0)
            {
                _suspendWait--;
                break;
            }
            // If Host has identified Device as HS previously, it should not
            // drive FS idle on bus
            _XcvrSelect->write(_isFS, currentTime);
            _TermSelect->write(_isFS, currentTime);
            _driveTiming = UsbxTiming_None;
            _clockCountForSuspendTiming = 0;
            _suspendState = UsbxUtmiSuspend_Idle;
            break;
        }
        // Device states
        case UsbxUtmiSuspend_DriveWakeUpChirpK:
        {
            // Keep track of the driven K in terms of clock
#ifdef _NO_TXREADY_FOR_CHIRP
            _kDriveCount++;
#else
            if (_TXReady->read(currentTime))
                _kDriveCount += 8 * _clocksPerFSBit;
#endif
            // Drive Resume K chirp for a specified time in OpMode 2
            if (_kDriveCount < (_clocksForRemoteWakeupChirpK / _timingFactor) /
                    speedFactor)
            {
                _OpMode->write(2, currentTime);
                _DataInLo->write(0, currentTime);
                _TXValid->write(1, currentTime);
#ifndef _NO_TXREADY_FOR_CHIRP
                if (_dataBusSize == 16)
                {
                    _DataInHi->write(0, currentTime);
                    _TXValidH->write(1, currentTime);
                }
#endif
                if (!_isFS)
                {
                    _TermSelect->write(1, currentTime);
                    _XcvrSelect->write(1, currentTime);
                }
            }
            // If enough K has been driven, go back to DeviceSuspended state
            else
            {
                _OpMode->write(0, currentTime);
                _TXValid->write(0, currentTime);
                _TXValidH->write(0, currentTime);
                _driveTiming = UsbxTiming_None;
                _kDriveCount = 0;
                _suspendWait = 0;
                _suspendState = UsbxUtmiSuspend_DeviceSuspended;
                _suspendWait = 8 * _clocksPerFSBit - 2;
            }
            break;
        }
        case UsbxUtmiSuspend_DeviceSuspended:
        {
            // Wait for timing to complete
            if (_suspendWait > 0)
            {
                _suspendWait--;
                break;
            }
            // Check for Resume chirp from Host
            if (lineState == UsbxUtmiLineState_K && _lineStatePeriod >=
                    (_clocksForResumeChirpK / _timingFactor) / speedFactor)
            {
                _suspendState = UsbxUtmiSuspend_DetectEOR;
#ifdef DEBUG_SUSPEND_RESUME
                printf("DEBUG[%d]: DEVICE: Detected Resume K chirp\n",
                        static_cast<UInt>(currentTime));
#endif
            }
            // De-assert SuspendM signal if K chirp is detected
            if (lineState == UsbxUtmiLineState_K)
                _SuspendM->write(1, currentTime);
            else
                _SuspendM->write(0, currentTime);
            break;
        }
        case UsbxUtmiSuspend_DetectEOR:
        {
            // Check for SE0 of EOR on bus
            if (lineState == UsbxUtmiLineState_SE0 &&
                _lineStatePeriod >= _clocksForSE0AfterResume / speedFactor)
            {
                // If the Device was operating previously in HS mode, it should
                // detect End of Resume after SE0 for two LS bit times.
                if (!_isFS)
                {
                    // Revert back to HS mode
                    _XcvrSelect->write(0, currentTime);
                    _TermSelect->write(0, currentTime);
                    _sensedTiming = UsbxTiming_Resume;
                    _suspendState = UsbxUtmiSuspend_Idle;
#ifdef DEBUG_SUSPEND_RESUME
                    printf("DEBUG[%d]: DEVICE: Detected Resume\n",
                            static_cast<UInt>(currentTime));
#endif
                }
                else
                    _suspendState = UsbxUtmiSuspend_DetectFsIdleAfterEOR;
            }
            break;
        }
        case UsbxUtmiSuspend_DetectFsIdleAfterEOR:
        {
            // Check for FS idle state on bus
            if (lineState == UsbxUtmiLineState_J)
            {
                _sensedTiming = UsbxTiming_Resume;
                _suspendState = UsbxUtmiSuspend_Idle;
#ifdef DEBUG_SUSPEND_RESUME
                printf("DEBUG[%d]: DEVICE: Detected Resume\n",
                        static_cast<UInt>(currentTime));
#endif
            }
            break;
        }
        case UsbxUtmiSuspend_CheckSuspendOrReset:
        {
            if (_suspendWait > 0)
            {
                _suspendWait--;
                break;
            }
            // If lineState is FS Idle, then Host is asserting Suspend
            if (lineState == UsbxUtmiLineState_J)
            {
                _sensedTiming = UsbxTiming_Suspend;
                _suspendState = UsbxUtmiSuspend_DeviceSuspended;
#ifdef DEBUG_SUSPEND_RESUME
                printf("DEBUG[%d]: DEVICE: Detected Suspend\n",
                        static_cast<UInt>(currentTime));
#endif
            }
            // If lineState is SE0, then Host is asserting Reset(strong SE0)
            else if (lineState == UsbxUtmiLineState_SE0)
            {
                // Go to drive Device chirp
                _isFS = UsbxTrue;
                _sensedTiming = UsbxTiming_DeviceHsReset;
                _suspendState = UsbxUtmiSuspend_Idle;
#ifdef DEBUG_SUSPEND_RESUME
                printf("DEBUG[%d]: DEVICE: Detected Reset\n",
                        static_cast<UInt>(currentTime));
#endif
            }
            break;
        }
        default: break;
    }

    // Wait for timing to complete
    if (_clockCountForSuspendTiming > 0)
    {
        _clockCountForSuspendTiming--;
        if (_clockCountForSuspendTiming == 0)
            _driveTiming = UsbxTiming_None;
        return UsbxTrue;
    }

    // Drive timing for Suspend, Resume and Remote Wakeup
    switch (_driveTiming)
    {
        case UsbxTiming_Suspend:
        {
            if (_state != UsbxUtmiFsm_IDLE)
                break;
            _kDriveCount = 0;
            _suspendState = UsbxUtmiSuspend_DriveSuspend;
            _clockCountForSuspendTiming = _clocksToDriveSuspend / speedFactor;
#ifdef DEBUG_SUSPEND_RESUME
            printf("DEBUG[%d]: HOST: Driving Suspend\n",
                    static_cast<UInt>(currentTime));
#endif
            break;
        }
        case UsbxTiming_Resume:
        {
            _kDriveCount = 0;
            // De-assert SuspendM(active low)
            _SuspendM->write(1, currentTime);
            _suspendState = UsbxUtmiSuspend_DriveResumeChirpK;
            _clockCountForSuspendTiming = ((_clocksForResumeChirpK /
                    _timingFactor) + _clocksForSE0AfterResume) / speedFactor;
#ifdef DEBUG_SUSPEND_RESUME
            printf("DEBUG[%d]: HOST: Driving Resume\n",
                    static_cast<UInt>(currentTime));
#endif
            break;
        }
        case UsbxTiming_RemoteWakeUp:
        {
            // De-assert SuspendM(active low)
            _SuspendM->write(1, currentTime);
            // Do not drive remote wakeup until bus has not been idle for at
            // least 5 ms recomended duration.
            if (lineState == UsbxUtmiLineState_J &&
                _lineStatePeriod < (_minClocksBeforeRemoteWakeupDrive -
                    (_isFS ? 0 : (_clocksForDeviceSuspendDetect +
                                  _clocksForHSDeviceToEnableFSTerminations))) /
                speedFactor)
                break;
            _kDriveCount = 0;
            _suspendState = UsbxUtmiSuspend_DriveWakeUpChirpK;
            _clockCountForSuspendTiming = (_clocksForRemoteWakeupChirpK /
                _timingFactor) / speedFactor;
#ifdef DEBUG_SUSPEND_RESUME
            printf("DEBUG[%d]: DEVICE: Driving Remote Wake Up\n",
                    static_cast<UInt>(currentTime));
#endif
            break;
        }
        default: break;
    }

    if (_suspendState != UsbxUtmiSuspend_Idle)
        return UsbxTrue;

    // Data transmit - receive FSM
    switch (_state)
    {
        case UsbxUtmiFsm_IDLE:
        {
            // Outputs - drive all to default values
            _Reset->write(0, currentTime);
            _DataBus16_8->write((_dataBusSize == 16), currentTime);
            _DataInLo->write(0, currentTime);
            _DataInHi->write(0, currentTime);
            _TXValid->write(0, currentTime);
            _TXValidH->write(0, currentTime);

            // Reset internal variables
            _byteIndex = 0;
            _rxError = UsbxFalse;

            // State transitions
            if (_RXActive->read(currentTime))
            {
                _state = UsbxUtmiFsm_RECEIVE;
                _waitCount = _clocksForInterPacketDelay;
                // For FS, after receive next transmit delay would be:
                // 2 SE0 of EOP + interpacket delay - 5 FSM delay
                if (_isFS)
                    _waitCount += 2 * _clocksPerFSBit;
                _waitCount /= speedFactor;
                // RX TX InterPacket Delay Adjustment
                if (_isFS)
                    _waitCount -= 4;
                else
                    _waitCount -= ((speedFactor == 1) ? 5 : 4);
            }
            // Put inter-packet delay between transmits
            else if (_waitCount > 0)
            {
                _waitCount--;
                break;
            }
            else if (_txPacket != NULL)
            {
                _state = UsbxUtmiFsm_TRANSMIT;
                _waitCount = _clocksForInterPacketDelay;
                // For FS, after transmit next transmit delay would be:
                // 1 byte last data + 2 SE0 of EOP + interpacket delay
                // - 3 FSM delay
                if (_isFS)
                    _waitCount += (8 + 2) * _clocksPerFSBit;
                _waitCount /= speedFactor;
                // TX TX InterPacket Delay Adjustment
                if (_isFS)
                    _waitCount -= 3;
                else
                    _waitCount -= 1 * speedFactor;
            }

            break;
        }
        case UsbxUtmiFsm_TRANSMIT:
        {
            // State transitions
            if (_TXReady->read(currentTime)) // if TXReady is HIGH
            {
                _byteIndex += _DataBus16_8->read(currentTime) + 1;
                if (_byteIndex >= _txPacket->getSizeInBytes())
                {
                    // Transmission complete
                    _state = UsbxUtmiFsm_IDLE;
                    // Remove data drives
                    _DataInLo->write(0, currentTime);
                    _DataInHi->write(0, currentTime);
                    _TXValid->write(0, currentTime);
                    _TXValidH->write(0, currentTime);
                    _txPacket = NULL;
                    break;
                }
            }

            // Outputs - send data for low byte
            const UsbxByte *byteArray = _txPacket->getByteArray();
            _DataInLo->write(byteArray[_byteIndex], currentTime);
            _TXValid->write(1, currentTime);
            // Send data for high byte
            if (_DataBus16_8->read(currentTime)) // for 16 bit data transfers
            {
                if (_byteIndex + 1 < _txPacket->getSizeInBytes())
                {
                    _DataInHi->write(byteArray[_byteIndex + 1], currentTime);
                    _TXValidH->write(1, currentTime);
                }
                else // last data has been pushed in low bytes
                {
                    _DataInHi->write(0, currentTime);
                    _TXValidH->write(0, currentTime);
                }
            }
            break;
        }
        case UsbxUtmiFsm_RECEIVE:
        {
            // State transitions
            if (!_RXActive->read(currentTime))
            {
                // Reception complete
                if (!_rxError)
                    _rxPacket = UsbxPacket::getPacket(_byteIndex, _rxBuffer);
                _state = UsbxUtmiFsm_IDLE;
                break;
            }

            // Data reception and error handling
            _rxError |= _RXError->read(currentTime);
            if (_rxError)
            {
                _rxError = UsbxTrue;
                break;
            }
            // Receive data for low byte
            if (_RXActive->read(currentTime) && _RXValid->read(currentTime))
            {
                if (_byteIndex < (1+1024+2))
                    _rxBuffer[_byteIndex++] = _DataOutLo->read(currentTime);
                else
                {
                    _rxError = UsbxTrue;
                    break;
                }
            }
            // Receive data for high byte
            if (_DataBus16_8->read(currentTime) && _RXActive->read(currentTime)
                    && _RXValidH->read(currentTime))
            {
                if (_byteIndex < (1+1024+2))
                    _rxBuffer[_byteIndex++] = _DataOutHi->read(currentTime);
                else
                {
                    _rxError = UsbxTrue;
                    break;
                }
            }
            break;
        }
        case UsbxUtmiFsm_DETACHED:
        {
            if (_txPacket)
            {
                delete _txPacket;
                _txPacket = NULL;
            }
            _XcvrSelect->write(1, currentTime);
            _TermSelect->write(1, currentTime);
            _OpMode->write(0, currentTime);
            _DataInLo->write(0, currentTime);
            _DataInHi->write(0, currentTime);
            _TXValid->write(0, currentTime);
            _TXValidH->write(0, currentTime);
            break;
        }
        default: ASSERT(0, NULL);
    }
    return UsbxTrue;
}

UsbxBool UsbxUtmiLinkIntf::refreshInputs(CarbonTime currentTime)
{
    UsbxBool retValue = UsbxTrue;
    retValue &= _TXReady->refresh(currentTime);
    retValue &= _DataOutLo->refresh(currentTime);
    retValue &= _DataOutHi->refresh(currentTime);
    retValue &= _RXValid->refresh(currentTime);
    retValue &= _RXValidH->refresh(currentTime);
    retValue &= _RXActive->refresh(currentTime);
    retValue &= _RXError->refresh(currentTime);
    retValue &= _LineState->refresh(currentTime);
    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_1)
    {
        retValue &= _HostDisconnect->refresh(currentTime);
        retValue &= _IdDig->refresh(currentTime);
        retValue &= _AValid->refresh(currentTime);
        retValue &= _BValid->refresh(currentTime);
        retValue &= _VbusValid->refresh(currentTime);
        retValue &= _SessEnd->refresh(currentTime);
    }
    return retValue;
}

UsbxBool UsbxUtmiLinkIntf::refreshOutputs(CarbonTime currentTime)
{
    UsbxBool retValue = UsbxTrue;
    retValue &= _Reset->refresh(currentTime);
    retValue &= _DataBus16_8->refresh(currentTime);
    retValue &= _SuspendM->refresh(currentTime);
    retValue &= _DataInLo->refresh(currentTime);
    retValue &= _DataInHi->refresh(currentTime);
    retValue &= _TXValid->refresh(currentTime);
    retValue &= _TXValidH->refresh(currentTime);
    retValue &= _XcvrSelect->refresh(currentTime);
    retValue &= _TermSelect->refresh(currentTime);
    retValue &= _OpMode->refresh(currentTime);
    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_1)
    {
        retValue &= _DpPulldown->refresh(currentTime);
        retValue &= _DmPulldown->refresh(currentTime);
        retValue &= _IdPullup->refresh(currentTime);
        retValue &= _DrvVbus->refresh(currentTime);
        retValue &= _ChrgVbus->refresh(currentTime);
        retValue &= _DischrgVbus->refresh(currentTime);
    }
    return retValue;
}

UsbxUtmiPhyIntf::UsbxLineStateLogic::UsbxLineEncoder::UsbxLineEncoder():
    _noEncoding(UsbxFalse), _numContiguousOnes(0), _nrziBit(1),
    _queueBack(-1), _queueFront(-1)
{}

void UsbxUtmiPhyIntf::UsbxLineStateLogic::UsbxLineEncoder::reset()
{
    _noEncoding = UsbxFalse;
    _numContiguousOnes = 0;
    _nrziBit = 1;
    _queueBack = _queueFront = -1;
}

void UsbxUtmiPhyIntf::UsbxLineStateLogic::UsbxLineEncoder::pushDataByte(
        UsbxByte dataByte)
{
    // Process bits from LSB to MSB
    for (UInt i = 0; i < 8; i++)
    {
        UsbxBit lsb = dataByte & 0x1;
        if (_noEncoding)
            pushLineState(lsb ? UsbxUtmiLineState_J : UsbxUtmiLineState_K);
        else
        {
            // NRZI encoding
            if (lsb)
                _numContiguousOnes++;
            else
            {
                _numContiguousOnes = 0;
                _nrziBit = ~_nrziBit & 0x1;
            }
            pushLineState(_nrziBit ? UsbxUtmiLineState_J : UsbxUtmiLineState_K);

            // Bit stuffing
            if (_numContiguousOnes == 6)
            {
                // Insert 0 in bit stream
                _nrziBit = ~_nrziBit & 0x1;
                _numContiguousOnes = 0;
                pushLineState(_nrziBit ?
                        UsbxUtmiLineState_J : UsbxUtmiLineState_K);
            }
        }
        dataByte >>= 1;
    }
}

void UsbxUtmiPhyIntf::UsbxLineStateLogic::UsbxLineEncoder::pushFsEop()
{
    pushLineState(UsbxUtmiLineState_SE0);
    pushLineState(UsbxUtmiLineState_SE0);
    pushLineState(UsbxUtmiLineState_J);
}

void UsbxUtmiPhyIntf::UsbxLineStateLogic::UsbxLineEncoder::pushLineState(
        UsbxUtmiLineStateType lineStateValue)
{
    ASSERT((_queueBack + 1) % _maxLineStateQueueSize != _queueFront, NULL);
    if (_queueBack == -1) // Empty queue
        _queueBack = _queueFront = 0;
    else if (_queueBack == _maxLineStateQueueSize) // Roll back
        _queueBack = 0;
    else
        _queueBack++;
    _lineStateQueue[_queueBack] = lineStateValue;
}

UsbxUtmiIntf::UsbxUtmiLineStateType
UsbxUtmiPhyIntf::UsbxLineStateLogic::UsbxLineEncoder::popLineState()
{
    ASSERT(_queueFront >= 0, NULL);
    UsbxUtmiLineStateType lineStateValue = _lineStateQueue[_queueFront];
    if (_queueBack == _queueFront) // Had single entry
        _queueBack = _queueFront = -1;
    else if (_queueFront == _maxLineStateQueueSize) // Roll back
        _queueFront = 0;
    else
        _queueFront++;
    return lineStateValue;
}

UsbxUtmiPhyIntf::UsbxLineStateLogic::UsbxLineStateLogic(): _intf(NULL),
            _state(UsbxUtmiLineStateFsm_IDLE),
            _encoder(), _clockCountForFSBit(0), _waitForHsDataEnd(0),
            _syncPushed(UsbxFalse)
{}

UsbxBool UsbxUtmiPhyIntf::UsbxLineStateLogic::evaluate(CarbonTime currentTime)
{
    UsbxBool isFSXcvr = _intf->_XcvrSelect->read(currentTime);

    UsbxBool isDataBusWidth16 = _intf->_DataBus16_8->read(currentTime);
    CarbonUInt8 dataInHi = _intf->_DataInHi->read(currentTime);
    CarbonUInt8 dataInLo = _intf->_DataInLo->read(currentTime);
    UsbxBit txValidH = _intf->_TXValidH->read(currentTime);
    UsbxBit txValid = _intf->_TXValid->read(currentTime);
    UsbxBit txReady = _intf->_TXReady->read(currentTime);
    CarbonUInt2 opMode = _intf->_OpMode->read(currentTime);

    switch (_state)
    {
        case UsbxUtmiLineStateFsm_IDLE:
        {
            _syncPushed = UsbxFalse;
            if (isFSXcvr) // For FS
            {
                if (txValid)
                {
                    _encoder.reset();
                    // Default state transition
                    _state = UsbxUtmiLineStateFsm_RECEIVE;

                    if (opMode == 0) // Normal Operation
                    {
                        _encoder.pushFsSync();
                        _syncPushed = UsbxTrue;
                    }
                    else if (opMode == 2) // Disable Bit Stuffing and
                                          // NRZI encoding
                        _encoder.disableEncoding();
                    else // Stay idle for other operating modes
                        _state = UsbxUtmiLineStateFsm_IDLE;
                }
            }
            else // For HS
            {
                if (txValid)
                {
                    if (opMode == 0) // Normal mode
                    {
                        if (isDataBusWidth16)
                            _intf->_LineState->write(UsbxUtmiLineState_J,
                                    currentTime);
                        _state = UsbxUtmiLineStateFsm_RECEIVE;
                    }
                    else if (opMode == 2)
                        _state = UsbxUtmiLineStateFsm_RECEIVE;
                }
                else if (_waitForHsDataEnd > 0)
                {
                    _waitForHsDataEnd--;
                    if (_waitForHsDataEnd == 0)
                        // Drive SE0 once on line state
                        _intf->_LineState->write(UsbxUtmiLineState_SE0,
                                currentTime);
                }
            }
            // Under FS OpMode 2 fall through
            if (isFSXcvr && opMode == 2 &&
                    _state == UsbxUtmiLineStateFsm_RECEIVE)
                _clockCountForFSBit = 1; // Continue execution for next state
                                         // in same clock
            else
                break; // Normal break
        }
        case UsbxUtmiLineStateFsm_RECEIVE:
        {
            if (isFSXcvr) // For FS
            {
                // When data should be taken from UTMI bus and pushed to
                // line state
                if (txValid && txReady)
                {
                    _encoder.pushDataByte(dataInLo);
                    if (isDataBusWidth16 && txValidH)
                        _encoder.pushDataByte(dataInHi);
                }
                else if (!txValid)
                {
                    // Push EOP to line state
                    if (opMode == 0 && _syncPushed)
                    {
                        _encoder.pushFsEop();
                        _syncPushed = UsbxFalse;
                    }
                    _state = UsbxUtmiLineStateFsm_IDLE;
                }
            }
            else // For HS
            {
                if (opMode == 0) // Normal mode
                {
                    if (txValid)
                        _intf->_LineState->write(UsbxUtmiLineState_J,
                                currentTime);
                    else
                    {
                        if (isDataBusWidth16)
                            _intf->_LineState->write(UsbxUtmiLineState_SE0,
                                    currentTime);
                        else
                            _waitForHsDataEnd = 2;
                        _state = UsbxUtmiLineStateFsm_IDLE;
                    }
                }
                else if (opMode == 2) // No bit stuff and NRZI mode
                {
                    if (txValid && txReady)
                        // Drive J on line state
                        _intf->_LineState->write(
                                _intf->_DataInLo->read(currentTime) ?
                                UsbxUtmiLineState_J : UsbxUtmiLineState_K,
                                currentTime);
                    else
                    {
                        // Drive SE0 on line state
                        _intf->_LineState->write(UsbxUtmiLineState_SE0,
                                currentTime);
                        _state = UsbxUtmiLineStateFsm_IDLE;
                    }
                }
            }
            break;
        }
    }

    // Drive LineState accordingly
    if (_clockCountForFSBit == 0)
    {
        if (_encoder.hasPendingLineState())
            _clockCountForFSBit = 5;
    }
    if (_clockCountForFSBit == 5)
        _intf->_LineState->write(_encoder.popLineState(), currentTime);
    else if (isDataBusWidth16 && _clockCountForFSBit == 2 &&
            _encoder.hasPendingLineState())
        _intf->_LineState->write(_encoder.popLineState(), currentTime);

    if (_clockCountForFSBit > 0)
        _clockCountForFSBit--;
    return UsbxTrue;
}

UsbxUtmiPhyIntf::UsbxUtmiBitStuffer::UsbxUtmiBitStuffer(UInt dataBusSize):
    _dataBusSize(dataBusSize), _bitStuffCount(0), _contiguousOnes(1)
    // Note, _contiguousOnes starts from 1 as last 1 in SYNC will be considered
{}

void UsbxUtmiPhyIntf::UsbxUtmiBitStuffer::update(UsbxByte dataByte)
{
    // _bitStuffCount = _bitStuffCount % _dataBusSize;
    for (UInt i = 0; i < 8; i++)
    {
        if (dataByte & 0x1)
            _contiguousOnes++;
        else
            _contiguousOnes = 0;
        dataByte = dataByte >> 1;

        if (_contiguousOnes == 6)
        {
            _bitStuffCount++;
            _contiguousOnes = 0;
        }
    }
}

// For FS
UInt UsbxUtmiPhyIntf::UsbxUtmiBitStuffer::getFSBitStuffCount()
{
    UInt bitCount = _bitStuffCount / (_dataBusSize / 8);
    _bitStuffCount %= (_dataBusSize / 8);
    return bitCount;
}

// For HS
UsbxBool UsbxUtmiPhyIntf::UsbxUtmiBitStuffer::needForHSByteStuff()
{
    if (_bitStuffCount < _dataBusSize)
        return UsbxFalse;
    _bitStuffCount = _bitStuffCount % _dataBusSize;
    return UsbxTrue;
}

UsbxUtmiPhyIntf::UsbxUtmiPhyIntf(UsbXtor *xtor, UInt dataBusSize,
        CarbonXUsbUtmiPinLevel intfLevel):
    UsbxUtmiIntf(xtor, dataBusSize, intfLevel),
    _txBitStuffer(_dataBusSize), _rxBitStuffer(_dataBusSize),
    _clockCountForFSByte(0), _lineStateLogic()
{
    _lineStateLogic.setIntf(this);

    _Reset = createPin(xtor, "Reset", UsbxDirection_Input);
    _DataBus16_8 = createPin(xtor, "DataBus16_8", UsbxDirection_Input);
    _SuspendM = createPin(xtor, "SuspendM", UsbxDirection_Input);

    _DataInLo = createPin(xtor, "DataInLo", UsbxDirection_Input, 8);
    _DataInHi = createPin(xtor, "DataInHi", UsbxDirection_Input, 8);
    _TXValid = createPin(xtor, "TXValid", UsbxDirection_Input);
    _TXValidH = createPin(xtor, "TXValidH", UsbxDirection_Input);
    _TXReady = createPin(xtor, "TXReady", UsbxDirection_Output);

    _DataOutLo = createPin(xtor, "DataOutLo", UsbxDirection_Output, 8);
    _DataOutHi = createPin(xtor, "DataOutHi", UsbxDirection_Output, 8);
    _RXValid = createPin(xtor, "RXValid", UsbxDirection_Output);
    _RXValidH = createPin(xtor, "RXValidH", UsbxDirection_Output);
    _RXActive = createPin(xtor, "RXActive", UsbxDirection_Output);
    _RXError = createPin(xtor, "RXError", UsbxDirection_Output);

    _XcvrSelect = createPin(xtor, "XcvrSelect", UsbxDirection_Input);
    _TermSelect = createPin(xtor, "TermSelect", UsbxDirection_Input);
    _OpMode = createPin(xtor, "OpMode", UsbxDirection_Input, 2);

    _LineState = createPin(xtor, "LineState", UsbxDirection_Output, 2);

    // Drive with initial value
    _LineState->write(UsbxUtmiLineState_SE0, 0);

    // Level 1 UTMI pins
    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_1)
    {
        _DpPulldown = createPin(xtor, "DpPulldown", UsbxDirection_Input);
        _DmPulldown = createPin(xtor, "DmPulldown", UsbxDirection_Input);
        _HostDisconnect = createPin(xtor, "HostDisconnect",
                UsbxDirection_Output);

        _IdPullup = createPin(xtor, "IdPullup", UsbxDirection_Input);
        _IdDig = createPin(xtor, "IdDig", UsbxDirection_Output);

        _AValid = createPin(xtor, "AValid", UsbxDirection_Output);
        _BValid = createPin(xtor, "BValid", UsbxDirection_Output);
        _VbusValid = createPin(xtor, "VbusValid", UsbxDirection_Output);
        _SessEnd = createPin(xtor, "SessEnd", UsbxDirection_Output);

        _DrvVbus =  createPin(xtor, "DrvVbus", UsbxDirection_Input);
        _ChrgVbus =  createPin(xtor, "ChrgVbus", UsbxDirection_Input);
        _DischrgVbus =  createPin(xtor, "DischrgVbus", UsbxDirection_Input);

        // Drive with initial value
        _IdDig->write(1, 0);
    }
}

UsbxBool UsbxUtmiPhyIntf::driveVbus(UsbxBool isOn)
{
    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_0)
        return UsbxFalse;
    _vbusValue = isOn ? UsbxUtmiVbus_VA_VBUS_VLD : UsbxUtmiVbus_VB_SESS_END;
    return UsbxTrue;
}

void UsbxUtmiPhyIntf::resetForDetach()
{
    UsbxUtmiIntf::resetForDetach();

    // Drive with initial value
    _LineState->write(UsbxUtmiLineState_SE0, 0);
}

// UTMI PHY FSM
UsbxBool UsbxUtmiPhyIntf::evaluate(CarbonTime currentTime)
{
    UInt speedFactor = _dataBusSize / 8;

    // OTG normal mode timing
    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_1)
    {
        // Do not drive HostDisconnect, as it is driven by PHY FSM from inside
        switch (_xtor->getType())
        {
            case CarbonXUsb_Host:
            {
                if (_IdPullup->read(currentTime))
                    _IdDig->write(0, currentTime);
                _AValid->write(1, currentTime);
                _BValid->write(1, currentTime);
                _VbusValid->write(1, currentTime);
                _SessEnd->write(0, currentTime);
                break;
            }
            case CarbonXUsb_Device:
            {
                if (_IdPullup->read(currentTime))
                    _IdDig->write(!_xtor->getConfig()->getMiniAPhyPlug(),
                        currentTime);
                if (_DrvVbus->read(currentTime))
                {
                    _AValid->write(1, currentTime);
                    _BValid->write(1, currentTime);
                    _VbusValid->write(1, currentTime);
                    _SessEnd->write(0, currentTime);
                }
                else
                {
                    _AValid->write(0, currentTime);
                    _BValid->write(0, currentTime);
                    _VbusValid->write(0, currentTime);
                    _SessEnd->write(1, currentTime);
                }
                break;
            }
            case CarbonXUsb_OtgDevice:
            {
                if (_IdPullup->read(currentTime))
                    _IdDig->write(!_xtor->getConfig()->getMiniAPhyPlug(),
                        currentTime);

                // Vbus charge and discharge
                if (_ChrgVbus->read(currentTime))
                    _clockCountForVbusCharge++;
                else
                    _clockCountForVbusCharge = 0;
                if (_DischrgVbus->read(currentTime))
                    _clockCountForVbusDischarge++;
                else
                    _clockCountForVbusDischarge = 0;

                // Vbus value calculation
                UsbxUtmiVbusValue oldVbusValue = _vbusValue;
                if (_DrvVbus->read(currentTime))
                    _vbusValue = UsbxUtmiVbus_VA_VBUS_VLD;
                else if (_ChrgVbus->read(currentTime))
                {
                    if (_clockCountForVbusCharge >=
                            ((_clocksForSrpVbusCharge / _timingFactor)
                             / speedFactor))
                        _vbusValue = UsbxUtmiVbus_VA_SESS_VLD;
                }
                else if (_DischrgVbus->read(currentTime))
                {
                    if (_clockCountForVbusDischarge >=
                            ((_clocksForSrpVbusDischarge / _timingFactor)
                             / speedFactor))
                        _vbusValue = UsbxUtmiVbus_VB_SESS_END;
                }
                else if (_xtor->getOtgDeviceType() == UsbxOtgDevice_B &&
                        _driveTiming != UsbxTiming_OtgSrp)
                    _vbusValue = UsbxUtmiVbus_VB_SESS_END;

                // Pin values depending on Vbus value
                _AValid->write(_vbusValue >= UsbxUtmiVbus_VA_SESS_VLD,
                        currentTime);
                _BValid->write(_vbusValue >= UsbxUtmiVbus_VB_SESS_VLD,
                        currentTime);
                _VbusValid->write(_vbusValue >= UsbxUtmiVbus_VA_VBUS_VLD,
                        currentTime);
                _SessEnd->write(_vbusValue <= UsbxUtmiVbus_VB_SESS_END,
                        currentTime);

                // Session end event occurred - Vbus is powered down
                // As a result disconnect USB with detach event
                if (oldVbusValue == UsbxUtmiVbus_VA_VBUS_VLD &&
                        _vbusValue == UsbxUtmiVbus_VB_SESS_END)
                    _driveTiming = UsbxTiming_DeviceDetach;
                // Sense Vbus pulsing
                else if (oldVbusValue == UsbxUtmiVbus_VB_SESS_END &&
                        _vbusValue == UsbxUtmiVbus_VA_SESS_VLD)
                    _sensedTiming = UsbxTiming_OtgSrp;
                else if (_lastLineState == UsbxUtmiLineState_J &&
                    _lineStatePeriod >= _clocksForAttach / speedFactor)
                    _sensedTiming = UsbxTiming_DeviceAttach;

                // OTG B device specific LineState drive for Data Line Pulsing
                if (_xtor->getOtgDeviceType() == UsbxOtgDevice_A &&
                        _driveTiming == UsbxTiming_None &&
                        _vbusValue == UsbxUtmiVbus_VB_SESS_END)
                {
                    if (_TermSelect->read(currentTime))
                        _LineState->write(UsbxUtmiLineState_J, currentTime);
                    else if (!_XcvrSelect->read(currentTime))
                        _LineState->write(UsbxUtmiLineState_SE0, currentTime);
                }
                break;
            }
            default: ASSERT(0, _xtor); break;
        }
    }

    // Timing detection
    UsbxUtmiLineStateType lineState = static_cast<UsbxUtmiLineStateType>(
            _LineState->read(currentTime));
    if (lineState == _lastLineState)
        _lineStatePeriod++;
    else
        _lineStatePeriod = 1;
    _lastLineState = lineState;

    // Drive timing
    if (_driveTiming != UsbxTiming_None)
    {
        // Chirp state machine
        switch (_chirpState)
        {
            case UsbxUtmiChirp_IDLE:
            {
                _clockCountForChirp = 0;
                _hostChirpCount = 0;
                break;
            }
            // Chirp FSM for DEVICE PHY interface
            case UsbxUtmiChirp_WaitForResetFromHost:
            {
                if (!_XcvrSelect->read(currentTime) &&
                        !_TermSelect->read(currentTime))
                {
                    _LineState->write(UsbxUtmiLineState_SE0, currentTime);
                    if (_clockCountForChirp >= (_clocksToStartDeviceChirpK /
                                _timingFactor) / speedFactor)
                    {
                        _LineState->write(UsbxUtmiLineState_K, currentTime);
                        _chirpState = UsbxUtmiChirp_DriveDeviceK;
                        _clockCountForChirp = 1;
                    }
                    else
                        _clockCountForChirp++;
                    }
                break;
            }
            case UsbxUtmiChirp_DriveDeviceK:
            {
                if (_clockCountForChirp >= (_clocksForDeviceChirpK /
                            _timingFactor) / speedFactor)
                {
                    _LineState->write(UsbxUtmiLineState_SE0, currentTime);
                    _chirpState = UsbxUtmiChirp_CheckHostK;
                    _clockCountForChirp = 0;
                    _hostChirpCount = 0;
                }
                else
                    _clockCountForChirp++;
                break;
            }
            case UsbxUtmiChirp_CheckHostK:
            {
                UsbxByte txData = _DataInLo->read(currentTime) & 0xFF;
                if (_TXValid->read(currentTime) &&
#ifndef _NO_TXREADY_FOR_CHIRP
                        _TXReady->read(currentTime) &&
#endif
                        txData == 0x00) // Chirp K
                {
                    _clockCountForChirp++;
                    if (_clockCountForChirp >= (_clocksForHostChirpMin /
                                _timingFactor) / speedFactor)
                    {
                        _chirpState = UsbxUtmiChirp_CheckHostJ;
                        _clockCountForChirp = 0;
                    }
                }
                else
                    _clockCountForChirp = 0;
                break;
            }
            case UsbxUtmiChirp_CheckHostJ:
            {
                UsbxByte txData = _DataInLo->read(currentTime) & 0xFF;
                if (_TXValid->read(currentTime) &&
#ifndef _NO_TXREADY_FOR_CHIRP
                        _TXReady->read(currentTime) &&
#endif
                        txData == 0xFF) // Chirp J
                {
                    _clockCountForChirp++;
                    if (_clockCountForChirp >= (_clocksForHostChirpMin /
                                _timingFactor) / speedFactor)
                    {
                        _hostChirpCount++;
                        if (_hostChirpCount >= _numHostChirpsMin)
                        {
                            // Device: host detected as HS
#ifdef DEBUG
                            printf("DEBUG[%d]: DEVICE: Host detected as HS.\n",
                                    static_cast<UInt>(currentTime));
#endif
                            _isFS = UsbxFalse;
                            _sensedTiming = UsbxTiming_HsDetected;
                            _chirpState = UsbxUtmiChirp_IDLE;
                        }
                        else
                            _chirpState = UsbxUtmiChirp_CheckHostK;
                        _clockCountForChirp = 0;
                    }
                }
                else
                    _clockCountForChirp = 0;
                break;
            }
            // Chirp FSM for HOST PHY interface
            case UsbxUtmiChirp_CheckDeviceK:
            {
                UsbxByte txData = _DataInLo->read(currentTime) & 0xFF;
                if (_TXValid->read(currentTime) &&
#ifndef _NO_TXREADY_FOR_CHIRP
                        _TXReady->read(currentTime) &&
#endif
                        txData == 0x00) // Chirp K
                {
                    _clockCountForChirp++;
                    if (_clockCountForChirp >= (_clocksForDeviceChirpKMin /
                                _timingFactor) / speedFactor)
                    {
                        // Device: host detected as HS
#ifdef DEBUG
                        printf("DEBUG[%d]: HOST: Device detected as HS.\n",
                                static_cast<UInt>(currentTime));
#endif
                        _isFS = UsbxFalse;
                        _sensedTiming = UsbxTiming_HsDetected;
                        _chirpState = UsbxUtmiChirp_DriveHostHsChirps;
                        _clockCountForChirp = 0;
                    }
                }
                else
                    _clockCountForChirp = 0;
                break;
            }
            case UsbxUtmiChirp_DriveHostHsChirps:
            {
                // Wait for Device chirp to complete
#ifdef _NO_TXREADY_FOR_CHIRP
                if (_TXValid->read(currentTime))
#else
                if (_TXValid->read(currentTime) || _TXReady->read(currentTime))
#endif
                    break;
                _LineState->write(UsbxUtmiLineState_SE0, currentTime);
                if (_clockCountForChirp >= (_clocksToStartHostChirp /
                            _timingFactor) / speedFactor - 1)
                {
                    _chirpState = UsbxUtmiChirp_DriveHostK;
                    _LineState->write(UsbxUtmiLineState_K, currentTime);
                    _clockCountForChirp = 0;
                    _hostChirpCount = 0;
                    _numHostChirps = (_clockCountForTiming * _timingFactor
                            * speedFactor - _clocksForSE0AfterHostChirp) /
#ifdef DRIVE_SMALL_CHIRPS
                            (_clocksForHostChirpMin * 2);
#else
                            (_clocksForHostChirpMax * 2);
#endif
#ifdef DEBUG
                    printf("DEBUG[%d]: Host to drive %d K-J chirps.\n",
                            static_cast<UInt>(currentTime), _numHostChirps);
#endif
                }
                else
                    _clockCountForChirp++;
                break;
            }
            case UsbxUtmiChirp_DriveHostK:
            {
                if (_clockCountForChirp >= (_clocksForHostChirpMax /
                            _timingFactor) / speedFactor - 1)
                {
                    _chirpState = UsbxUtmiChirp_DriveHostJ;
                    _LineState->write(UsbxUtmiLineState_J, currentTime);
                    _clockCountForChirp = 0;
                }
                else
                    _clockCountForChirp++;
                break;
            }
            case UsbxUtmiChirp_DriveHostJ:
            {
                if (_clockCountForChirp >= (_clocksForHostChirpMax /
                            _timingFactor) / speedFactor - 1)
                {
                    _hostChirpCount++;
                    if (_hostChirpCount >= _numHostChirps)
                    {
                        _chirpState = UsbxUtmiChirp_IDLE;
                        _LineState->write(UsbxUtmiLineState_SE0, currentTime);
                        _clockCountForChirp = 0;
                        _hostChirpCount = 0;
                        _numHostChirps = _numHostChirpsMin * 2;
                        break;
                    }
                    _chirpState = UsbxUtmiChirp_DriveHostK;
                    _LineState->write(UsbxUtmiLineState_K, currentTime);
                    _clockCountForChirp = 0;
                }
                else
                    _clockCountForChirp++;
                break;
            }
            // OTG SRP timing FSM
            case UsbxUtmiChirp_SrpDataLinePulse:
            {
                if (_clockCountForChirp >= (_clocksForSrpDataLinePulse /
                            _timingFactor) / speedFactor - 1)
                {
                    _chirpState = UsbxUtmiChirp_SrpVbusCharge;
                    _LineState->write(UsbxUtmiLineState_SE0, currentTime);
                    _vbusValue = UsbxUtmiVbus_VA_SESS_VLD;
                    _clockCountForChirp = 0;
                }
                else
                    _clockCountForChirp++;
                break;
            }
            case UsbxUtmiChirp_SrpVbusCharge:
            {
                if (_clockCountForChirp >=
                        (((_clocksForSrpVbusCharge * 12) / 10) // 20% tol
                         / _timingFactor) / speedFactor - 1)
                {
                    _chirpState = UsbxUtmiChirp_IDLE;
                    _vbusValue = UsbxUtmiVbus_VB_SESS_END;
                    _clockCountForChirp = 0;
                }
                else
                    _clockCountForChirp++;
                break;
            }
            default: break;
        }

        // Wait for timing to complete
        if (_clockCountForTiming > 0)
        {
            _clockCountForTiming--;
            if (_clockCountForTiming == 0)
            {
                _driveTiming = UsbxTiming_None;
                _chirpState = UsbxUtmiChirp_IDLE;
            }
            else if (_driveTiming != UsbxTiming_HostFsReset &&
                    _driveTiming != UsbxTiming_HostHsReset &&
                    _driveTiming != UsbxTiming_DeviceHsReset)
                return UsbxTrue;
        }
        else
        {
            switch (_driveTiming)
            {
                case UsbxTiming_Suspend: break;
                case UsbxTiming_Resume: break;
                case UsbxTiming_RemoteWakeUp: break;
                case UsbxTiming_DeviceAttach:
                {
                    _LineState->write(UsbxUtmiLineState_J, currentTime);
                    _clockCountForTiming = _clocksForAttach / speedFactor;

                    // For UTMI level 1, reset HostDisconnect
                    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_1)
                        _HostDisconnect->write(0, currentTime);

                    _isFS = UsbxTrue;
                    _state = UsbxUtmiFsm_IDLE;
                    break;
                }
                case UsbxTiming_DeviceDetach:
                {
                    _LineState->write(UsbxUtmiLineState_SE0, currentTime);
                    _clockCountForTiming = _clocksForAttach / speedFactor;

                    // For Device PHY UTMI level 1, drive HostDisconnect to 1
                    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_1 &&
                            _DpPulldown->read(currentTime) &&
                            _DmPulldown->read(currentTime))
                        _HostDisconnect->write(1, currentTime);

                    _isFS = UsbxTrue;
                    _lineStateLogic.reset();
                    _state = UsbxUtmiFsm_DETACHED;

                    break;
                }
                case UsbxTiming_HostHsReset:
                {
                    _chirpState = UsbxUtmiChirp_CheckDeviceK;
                    // No break to be provided
                }
                case UsbxTiming_HostFsReset:
                {
                    _isFS = UsbxTrue;
                    _LineState->write(UsbxUtmiLineState_SE0, currentTime);
                    _clockCountForTiming = (_clocksForResetSE0 / _timingFactor)
                        / speedFactor;
                    break;
                }
                case UsbxTiming_DeviceHsReset:
                {
                    _isFS = UsbxTrue;
                    _clockCountForChirp = 1;
                    _chirpState = UsbxUtmiChirp_WaitForResetFromHost;
                    _clockCountForTiming = (_clocksForResetSE0 / _timingFactor)
                        / speedFactor;
                    break;
                }
                case UsbxTiming_DeviceFsReset:
                {
                    _isFS = UsbxTrue;
                    // Drive SE0 whenever
                    if (!_XcvrSelect->read(currentTime) &&
                            !_TermSelect->read(currentTime))
                    {
                        _LineState->write(UsbxUtmiLineState_SE0, currentTime);
                        _driveTiming = UsbxTiming_None;
                    }
                    break;
                }
                // OTG SRP timing drive
                case UsbxTiming_OtgSrp:
                {
                    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_0)
                        break;

                    _LineState->write(UsbxUtmiLineState_J, currentTime);
                    _isFS = UsbxTrue;
                    _clockCountForChirp = 0;
                    _chirpState = UsbxUtmiChirp_SrpDataLinePulse;
                    _clockCountForTiming = (((_clocksForSrp * 12) / 10)//20% tol
                            / _timingFactor) / speedFactor;
                    break;
                }
                case UsbxTiming_OtgHnp:
                {
                    _LineState->write(UsbxUtmiLineState_SE0, currentTime);
                    _driveTiming = UsbxTiming_None;
                    break;
                }
                default:
                {
                    _driveTiming = UsbxTiming_None;
                    break;
                }
            }
        }
    }

    // Suspend, Resume and Remote Wake Up PHY FSM
    switch (_suspendState)
    {
        case UsbxUtmiSuspend_Idle:
        {
            // Check for Suspend signal
            if (_lineStatePeriod < _clocksForDeviceSuspendDetect / speedFactor)
                break;
            // For FS Device
            if (_isFS && lineState == UsbxUtmiLineState_J)
            {
                // FS Suspend detected
                _sensedTiming = UsbxTiming_Suspend;
                _suspendState = UsbxUtmiSuspend_DeviceSuspended;
#ifdef DEBUG_SUSPEND_RESUME
                printf("DEBUG[%d]: DEVICE: Detected FS Suspend\n",
                        static_cast<UInt>(currentTime));
#endif
            }
            // For HS Device
            else if (!_isFS && lineState == UsbxUtmiLineState_SE0)
            {
                // If _TermSelect is high, the LINK is asserting
                // driven SE0, which implies a reset else the LINK is
                // asserting soft SE0, which implies a suspend
                if (_lineStatePeriod >= (_clocksForDeviceSuspendDetect +
                        _clocksForHSDeviceToEnableFSTerminations) / speedFactor)
                {
                    _suspendState = UsbxUtmiSuspend_CheckSuspendOrReset;
                    _suspendWait = _clocksToDecipherSuspendReset /
                        speedFactor - 1;
                }
            }
            break;
        }
        // Host states
        case UsbxUtmiSuspend_DriveSuspend:
        {
            if (!_isFS && _TermSelect->read(currentTime) &&
                _XcvrSelect->read(currentTime))
                _LineState->write(UsbxUtmiLineState_J, currentTime);
            // If Suspend drive is complete, go to Suspended state
            if (lineState == UsbxUtmiLineState_J &&
                _lineStatePeriod >= (_clocksToDriveSuspend - (_isFS ? 0 :
                    _clocksForDeviceSuspendDetect +
                    _clocksForHSDeviceToEnableFSTerminations)) / speedFactor)
            {
                _driveTiming = UsbxTiming_None;
                _clockCountForSuspendTiming = 0;
                _suspendWait = 0;
                _suspendState = UsbxUtmiSuspend_HostSuspended;
            }
            // Fall through to UsbxUtmiSuspend_HostSuspended state
        }
        case UsbxUtmiSuspend_HostSuspended:
        {
            UsbxBool txValid = _TXValid->read(currentTime);
            // Wait for timing to complete
            if (_suspendWait > 0)
            {
                _TXReady->write(0, currentTime);
                _suspendWait--;
            }
            else if (txValid)
            {
#ifdef _NO_TXREADY_FOR_CHIRP
                _TXReady->write(0, currentTime);
#else
                _TXReady->write(1, currentTime);
#endif
                _suspendWait = 8 * _clocksPerFSBit - 1;
                // Check for K drive from Device and update count for it
#ifdef _NO_TXREADY_FOR_CHIRP
                if (_DataInLo->read(currentTime) == 0 &&
                        _OpMode->read(currentTime) == 2)
#else
                if (!_DataInLo->read(currentTime) &&
                    _OpMode->read(currentTime) == 2 &&
                    (_DataBus16_8->read(currentTime) &&
                     _TXValidH->read(currentTime)) ?
                    !_DataInHi->read(currentTime) : 1)
#endif
                {
                    _kDriveCount += 8 * _clocksPerFSBit;
#ifndef _NO_TXREADY_FOR_CHIRP
                    if (_clockCountForSuspendTiming)
#endif
                        _LineState->write(UsbxUtmiLineState_K, currentTime);
                }
            }
            // Wait until enough remote wakeup K chirp is driven from Device
            if (lineState == UsbxUtmiLineState_K && _kDriveCount >=
                    (_clocksForHostToDetectRemoteWakeUp / _timingFactor) /
                    speedFactor)
            {
                if (_sensedTiming != UsbxTiming_RemoteWakeUp)
                {
                    _sensedTiming = UsbxTiming_RemoteWakeUp;
                    _driveTiming = UsbxTiming_None;
                    _suspendState = UsbxUtmiSuspend_HostSuspended;
                    _clockCountForSuspendTiming = 0;
                    _kDriveCount = 0;
#ifdef DEBUG_SUSPEND_RESUME
                    printf("DEBUG[%d]: HOST: Detected Remote Wakeup\n",
                            static_cast<UInt>(currentTime));
#endif
                }
            }
            // OTG HNP specific timing
            else if (_interfaceLevel == CarbonXUsbUtmiPinLevel_1 &&
                    _xtor->getOtgDeviceType() == UsbxOtgDevice_A &&
                    !_TermSelect->read(currentTime))
            {
                _LineState->write(UsbxUtmiLineState_SE0, currentTime);
                _sensedTiming = UsbxTiming_OtgHnp;
            }
            break;
        }
        case UsbxUtmiSuspend_DriveResumeChirpK:
        {
            UsbxBool txValid = _TXValid->read(currentTime);
            // Wait for timing to complete
            if (_suspendWait > 0)
            {
                _TXReady->write(0, currentTime);
                _suspendWait--;
            }
            else if (txValid)
            {
#ifdef _NO_TXREADY_FOR_CHIRP
                _TXReady->write(0, currentTime);
#else
                _TXReady->write(1, currentTime);
#endif
                _suspendWait = 8 * _clocksPerFSBit - 1;
                _kDriveCount += 8 * _clocksPerFSBit;
            }
            // Wait until enough resume K chirp is driven
            else
            {
                if (_kDriveCount >= (_clocksForResumeChirpK / _timingFactor) /
                        speedFactor)
                {
                    _LineState->write(UsbxUtmiLineState_SE0, currentTime);
                    _suspendState = UsbxUtmiSuspend_DriveEOR;
                    _suspendWait = _clocksForSE0AfterResume / speedFactor - 2;
                }
                else
                {
                    _LineState->write(UsbxUtmiLineState_K, currentTime);
                    _kDriveCount++;
                }
            }
            break;
        }
        case UsbxUtmiSuspend_DriveEOR:
        {
            if (_suspendWait > 0)
            {
                _LineState->write(UsbxUtmiLineState_SE0, currentTime);
                _suspendWait--;
            }
            else
                _suspendState = UsbxUtmiSuspend_DriveIdleAfterEOR;
            break;
        }
        case UsbxUtmiSuspend_DriveIdleAfterEOR:
        {
            if (_isFS)
                _LineState->write(UsbxUtmiLineState_J, currentTime);
            _driveTiming = UsbxTiming_None;
            _clockCountForSuspendTiming = 0;
            _suspendState = UsbxUtmiSuspend_Idle;
            break;
        }
        // Device states
        case UsbxUtmiSuspend_DriveWakeUpChirpK:
        {
            _LineState->write(UsbxUtmiLineState_K, currentTime);
            _kDriveCount++;
            // Go to UsbxUtmiSuspend_DeviceSuspended state if Host has started
            // driving resume K chirp or if full length remote wakeup chirp has
            // been driven
            if ((_TXValid->read(currentTime) && !_DataInLo->read(currentTime) &&
                _OpMode->read(currentTime) == 2) || ((_kDriveCount >=
                (_clocksForRemoteWakeupChirpK / _timingFactor) / speedFactor)))
            {
                _kDriveCount = 0;
                _suspendWait = 0;
                _suspendState = UsbxUtmiSuspend_DeviceSuspended;
                _driveTiming = UsbxTiming_None;
                // Fall through to UsbxUtmiSuspend_DeviceSuspended
            }
            else
                break;
        }
        case UsbxUtmiSuspend_DeviceSuspended:
        {
            UsbxBool txValid = _TXValid->read(currentTime);
            // Wait for timing to complete
            if (_suspendWait > 0)
            {
                _TXReady->write(0, currentTime);
                _suspendWait--;
            }
            else if (txValid)
            {
#ifdef _NO_TXREADY_FOR_CHIRP
                _TXReady->write(0, currentTime);
#else
                _TXReady->write(1, currentTime);
#endif
                _suspendWait = 8 * _clocksPerFSBit - 1;
                // Check for K drive from Host and update count for it
#ifdef _NO_TXREADY_FOR_CHIRP
                if ((_DataInLo->read(currentTime) == 0) &&
                        (_OpMode->read(currentTime) == 2))
#else
                if (!_DataInLo->read(currentTime) &&
                    (_OpMode->read(currentTime) == 2) &&
                    (_DataBus16_8->read(currentTime) &&
                     _TXValidH->read(currentTime)) ?
                    !_DataInHi->read(currentTime) : 1)
#endif
                {
                    _kDriveCount += 8 * _clocksPerFSBit;
#ifdef _NO_TXREADY_FOR_CHIRP
                    _LineState->write(UsbxUtmiLineState_K, currentTime);
#endif
                }
                else
                    _kDriveCount = 0;
            }
            // Wait until enough resume K chirp is driven from Host
            else if (lineState == UsbxUtmiLineState_K && _kDriveCount >=
                    (_clocksForResumeChirpK / _timingFactor) / speedFactor - 1)
            {
#ifdef _NO_TXREADY_FOR_CHIRP
                // Check for SE0 of EOR on bus
                if (!_XcvrSelect->read(currentTime) &&
                    !_TermSelect->read(currentTime))
                    _LineState->write(UsbxUtmiLineState_SE0, currentTime);
                else
                    _LineState->write(UsbxUtmiLineState_J, currentTime);
#endif
                _suspendState = UsbxUtmiSuspend_DetectEOR;
            }
            // If Host drove K chirp for insufficient clocks
            else
            {
                _LineState->write(UsbxUtmiLineState_J, currentTime);
                _kDriveCount = 0;
            }
            break;
        }
        case UsbxUtmiSuspend_DetectEOR:
        {
            // Check for SE0 of EOR on bus
            if (!_XcvrSelect->read(currentTime) &&
                !_TermSelect->read(currentTime))
                _LineState->write(UsbxUtmiLineState_SE0, currentTime);
            else
                _LineState->write(UsbxUtmiLineState_J, currentTime);
            if (lineState == UsbxUtmiLineState_SE0 &&
                _lineStatePeriod >= _clocksForSE0AfterResume / speedFactor - 1)
            {
                // If the Device was operating previously in FS mode, it
                // should detect EOR after SE0 for two LS bit times.
                _suspendState = !_isFS ? UsbxUtmiSuspend_Idle :
                    UsbxUtmiSuspend_DetectFsIdleAfterEOR;
                if (_suspendState == UsbxUtmiSuspend_Idle)
                {
                    _sensedTiming = UsbxTiming_Resume;
#ifdef DEBUG_SUSPEND_RESUME
                    printf("DEBUG[%d]: DEVICE: Detected RESUME\n",
                            static_cast<UInt>(currentTime));
#endif
                }
            }
            break;
        }
        case UsbxUtmiSuspend_DetectFsIdleAfterEOR:
        {
            // Check for FS idle state on bus
            if (_XcvrSelect->read(currentTime) &&
                _TermSelect->read(currentTime))
            {
                _LineState->write(UsbxUtmiLineState_J, currentTime);
                _sensedTiming = UsbxTiming_Resume;
                _suspendState = UsbxUtmiSuspend_Idle;
#ifdef DEBUG_SUSPEND_RESUME
                printf("DEBUG[%d]: DEVICE: Detected RESUME\n",
                        static_cast<UInt>(currentTime));
#endif
            }
            break;
        }
        case UsbxUtmiSuspend_CheckSuspendOrReset:
        {
            // Set linestate status according to termselect input
            if (!_TermSelect->read(currentTime))
                _LineState->write(UsbxUtmiLineState_J, currentTime);
            // Wait for timing to complete
            if (_suspendWait > 0)
            {
                _suspendWait--;
                break;
            }
            // A SE0 here is indicated by asserted TermSelect implying HS Reset
            if (_TermSelect->read(currentTime))
            {
                // Go to drive Device chirp
                _isFS = UsbxTrue;
                _driveTiming = UsbxTiming_DeviceHsReset;
                _suspendState = UsbxUtmiSuspend_Idle;
            }
            // A J here is indicated  by deaserted TermSelect implying Suspend
            else
            {
                _sensedTiming = UsbxTiming_Suspend;
                _suspendState = UsbxUtmiSuspend_DeviceSuspended;
#ifdef DEBUG_SUSPEND_RESUME
                printf("DEBUG[%d]: DEVICE: Detected HS Suspend\n",
                        static_cast<UInt>(currentTime));
#endif
            }
            break;
        }
        default: break;
    }

    // Wait for timing to complete
    if (_clockCountForSuspendTiming > 0)
    {
        _clockCountForSuspendTiming--;
        if (_clockCountForSuspendTiming == 0)
            _driveTiming = UsbxTiming_None;
        return UsbxTrue;
    }

    // Drive timing for Suspend, Resume and Remote Wakeup
    switch (_driveTiming)
    {
        case UsbxTiming_Suspend:
        {
            // Wait until data tranciever FSM reaches Idle state and LineState
            // comes to Idle state
            if (_state != UsbxUtmiFsm_IDLE || lineState != (_isFS ?
                    UsbxUtmiLineState_J : UsbxUtmiLineState_SE0))
                break;
            _kDriveCount = 0;
            _suspendState = UsbxUtmiSuspend_DriveSuspend;
            _clockCountForSuspendTiming = _clocksToDriveSuspend / speedFactor;
#ifdef DEBUG_SUSPEND_RESUME
            printf("DEBUG[%d]: HOST: Driving Suspend\n",
                    static_cast<UInt>(currentTime));
#endif
            break;
        }
        case UsbxTiming_Resume:
        {
            _kDriveCount = 0;
            _suspendState = UsbxUtmiSuspend_DriveResumeChirpK;
            _clockCountForSuspendTiming = ((_clocksForResumeChirpK /
                    _timingFactor) + _clocksForSE0AfterResume) / speedFactor;
            _lineStateLogic.reset();
#ifdef DEBUG_SUSPEND_RESUME
            printf("DEBUG[%d]: HOST: Driving Resume\n",
                    static_cast<UInt>(currentTime));
#endif
            break;
        }
        case UsbxTiming_RemoteWakeUp:
        {
            // Do not drive remote wakeup until bus has not been idle for at
            // least 5 ms recomended duration.
            if (lineState == UsbxUtmiLineState_J &&
                _lineStatePeriod < (_minClocksBeforeRemoteWakeupDrive -
                    (_isFS ? 0 : (_clocksForDeviceSuspendDetect +
                                  _clocksForHSDeviceToEnableFSTerminations))) /
                speedFactor)
                break;
            _kDriveCount = 0;
            _suspendState = UsbxUtmiSuspend_DriveWakeUpChirpK;
            _clockCountForSuspendTiming = (_clocksForRemoteWakeupChirpK /
                _timingFactor) / speedFactor;
#ifdef DEBUG_SUSPEND_RESUME
            printf("DEBUG[%d]: DEVICE: Driving Remote Wake Up\n",
                    static_cast<UInt>(currentTime));
#endif
            break;
        }
        default: break;
    }

#ifdef _NO_TXREADY_FOR_CHIRP
    if (_suspendState != UsbxUtmiSuspend_Idle)
        return UsbxTrue;
#endif

    if (_state != UsbxUtmiFsm_DETACHED)
        _lineStateLogic.evaluate(currentTime);

    if (_suspendState != UsbxUtmiSuspend_Idle)
        return UsbxTrue;

    CarbonUInt2 opMode = _OpMode->read(currentTime) & 0x3;
    switch (_state)
    {
        case UsbxUtmiFsm_IDLE:
        {
            // Outputs - drive all to default values
            _TXReady->write(0, currentTime);
            _DataOutLo->write(0, currentTime);
            _DataOutHi->write(0, currentTime);
            _RXValid->write(0, currentTime);
            _RXValidH->write(0, currentTime);
            _RXActive->write(0, currentTime);
            _RXError->write(0, currentTime);

            // Reset internal variables
            _byteIndex = 0;
            _txBitStuffer.reset();
            _rxBitStuffer.reset();
            _rxError = UsbxFalse;
            _clockCountForFSByte = 0;

            // State transitions
            if (_TXValid->read(currentTime))
            {
                _state = UsbxUtmiFsm_RECEIVE;
                if (opMode == 2)
                {
                    _TXReady->write(1, currentTime);
                    _waitCount = 0;
                    break;
                }
                if (_isFS)
                    _waitCount = _clocksPerFSBit * 8; // For 1 byte SYNC
                else
                    _waitCount = 4; // 4 bytes for SYNC
                _waitCount /= speedFactor;
                // TX PHY SYNC Delay Adjustment
                _waitCount -= 2; // FSM's default delay is 2
            }
            // Put inter-packet delay between transmits
            else if (_waitCount > 0)
            {
                _waitCount--;
                break;
            }
            else if (_txPacket != NULL)
            {
                _state = UsbxUtmiFsm_TRANSMIT;
                if (_isFS)
                    _waitCount = _clocksPerFSBit * 8; // For 1 byte SYNC
                else
                    _waitCount = 2;
                _waitCount /= speedFactor;

                if (_isFS) // For FS, push the packet bytes in the encoder
                {
                    _lineStateLogic.reset();
                    _lineStateLogic.pushFsSync();
                    const UsbxByte *byteArray = _txPacket->getByteArray();
                    for (UInt i = 0; i < _txPacket->getSizeInBytes(); i++)
                        _lineStateLogic.pushDataByte(byteArray[i]);
                    _lineStateLogic.pushFsEop();
                }
                else // For HS, drive J on line state
                {
                    _LineState->write(UsbxUtmiLineState_J, currentTime);
                    if (speedFactor == 1)
                        _waitCount += 2;
                }
            }
            break;
        }
        case UsbxUtmiFsm_TRANSMIT:
        {
            // Wait during SYNC and first byte transmission
            UsbxBool isFirst = UsbxFalse;
            if (_waitCount > 0)
            {
                _waitCount--;
                if (_isUlpi && !_isFS && (_waitCount == 1))
                    _RXActive->write(1, currentTime);
                if (_waitCount == 0)
                {
                    _RXActive->write(1, currentTime);
                    isFirst = UsbxTrue;
                }
                else
                    break;
            }

            // State transitions
            if (_RXActive->read(currentTime) && _RXValid->read(currentTime) &&
                    _byteIndex >= _txPacket->getSizeInBytes())
            {
                // Transmission complete
                _state = UsbxUtmiFsm_IDLE;
                _waitCount = _clocksForInterPacketDelay;
                if (_isFS)
                    _waitCount += _clocksPerFSBit * 2;
                _waitCount /= speedFactor;
                // RX RX InterPacket Delay Adjustment
                if (_isFS)
                    _waitCount -= 2;
                else
                    _waitCount -= 1;
                // Remove data drives
                _DataOutLo->write(0, currentTime);
                _DataOutHi->write(0, currentTime);
                _RXActive->write(0, currentTime);
                _RXValid->write(0, currentTime);
                _RXValid->write(0, currentTime);
                _txPacket = NULL;
                if (!_isFS)
                    _LineState->write(UsbxUtmiLineState_SE0, currentTime);
                break;
            }

            // Update for bit stuffing
            const UsbxByte *byteArray = _txPacket->getByteArray();
            if (isFirst || _RXValid->read(currentTime))
            {
                _rxBitStuffer.update(byteArray[_byteIndex]);
                _clockCountForFSByte = _clocksPerFSBit * 8;
                if (_DataBus16_8->read(currentTime))
                {
                    if (_byteIndex + 1 < _txPacket->getSizeInBytes())
                        _rxBitStuffer.update(byteArray[_byteIndex + 1]);
                    else
                        _clockCountForFSByte /= speedFactor;
                }
                if (_isFS) // Find bit stuffing period for FS
                    _clockCountForFSByte += _clocksPerFSBit *
                        _rxBitStuffer.getFSBitStuffCount() / speedFactor;
            }
            if (isFirst)
                break;

            UsbxBool transferData = UsbxTrue;
            if (_isFS) // Find bit stuffing period for FS
            {
                if (_clockCountForFSByte > 0)
                    _clockCountForFSByte--;
                transferData = _clockCountForFSByte == 0;
            }
            else // For HS
                transferData = !_rxBitStuffer.needForHSByteStuff();
            // Skip if hold is needed
            if (transferData)
            {
                // Outputs - send low bytes
                _DataOutLo->write(byteArray[_byteIndex], currentTime);
                _RXValid->write(1, currentTime);
                // Send high bytes
                if (_DataBus16_8->read(currentTime))
                {
                    if (_byteIndex + 1 >= _txPacket->getSizeInBytes())
                    {
                        _DataOutHi->write(0, currentTime);
                        _RXValidH->write(0, currentTime);
                    }
                    else // when last data has not been sent over low byte lane
                    {
                        _DataOutHi->write(byteArray[_byteIndex + 1],
                                currentTime);
                        _RXValidH->write(1, currentTime);
                    }
                }
                _byteIndex += _DataBus16_8->read(currentTime) + 1;
            }
            else
            {
                _DataOutLo->write(0, currentTime);
                _DataOutHi->write(0, currentTime);
                _RXValid->write(0, currentTime);
                _RXValidH->write(0, currentTime);
                break;
            }
            break;
        }
        case UsbxUtmiFsm_RECEIVE:
        {
            // Wait during SYNC transmission
            if (_waitCount > 0)
            {
                _waitCount--;
                break;
            }

            // State transitions
            if (!_TXValid->read(currentTime)) // receive complete
            {
                // Create packet from received buffer and go to IDLE
                if (!_rxError)
                    _rxPacket = UsbxPacket::getPacket(_byteIndex, _rxBuffer);
                _TXReady->write(0, currentTime);
                _state = UsbxUtmiFsm_IDLE;
                _waitCount = _clocksForInterPacketDelay;
                if (_isFS) // Add delay for self and 2 SE0 of EOP
                    _waitCount += _clockCountForFSByte +
                        _clocksPerFSBit * 2;
                _waitCount /= speedFactor;
                // TX RX InterPacket Delay Adjustment
                if (_isFS)
                    _waitCount -= 2;
                else
                    _waitCount -= ((speedFactor == 1) ? -1 : 1);
                break;
            }

            // Data reception at TXValid and buffered TXReady
            if (_TXValid->read(currentTime) && _TXReady->read(currentTime))
            {
                if (_byteIndex < (1+1024+2))
                {
                    _rxBuffer[_byteIndex] = _DataInLo->read(currentTime);
                    if (opMode != 2)
                        _txBitStuffer.update(_rxBuffer[_byteIndex]);
                    _byteIndex++;
                }
                else
                {
                    // If max. buffer is overrun, invalid size is coming
                    _rxError = UsbxTrue;
                    break;
                }
            }
            // Data high part reception
            if (_DataBus16_8->read(currentTime) && _TXValidH->read(currentTime)
                    && _TXReady->read(currentTime))
            {
                if (_byteIndex < (1+1024+2))
                {
                    _rxBuffer[_byteIndex] = _DataInHi->read(currentTime);
                    if (opMode != 2)
                        _txBitStuffer.update(_rxBuffer[_byteIndex]);
                    _byteIndex++;
                }
                else
                {
                    // If max. buffer is overrun, invalid size is coming
                    _rxError = UsbxTrue;
                    break;
                }
            }

            // Driving UTM should read the buffered TXReady signal
            // i.e. at the next clock - so this signal should be written
            // after read by above statements
            if (!_XcvrSelect->read(currentTime) && opMode == 2) // HS chirp mode
                _TXReady->write(1, currentTime);
            else if (_isFS) // For FS
            {
                if (_TXValid->read(currentTime) && _TXReady->read(currentTime))
                {
                    _clockCountForFSByte = _clocksPerFSBit * 8;
                    if (opMode != 2)
                        _clockCountForFSByte += _clocksPerFSBit *
                            _txBitStuffer.getFSBitStuffCount();
                }
                if (_clockCountForFSByte > 0)
                    _clockCountForFSByte--;
                _TXReady->write(_clockCountForFSByte == 0, currentTime);
            }
            else // For HS
            {
                if (opMode == 2)
                    _TXReady->write(1, currentTime);
                else
                    _TXReady->write(!_txBitStuffer.needForHSByteStuff(),
                            currentTime);
            }
            break;
        }
        case UsbxUtmiFsm_DETACHED:
        {
            if (_rxPacket)
            {
                delete _rxPacket;
                _rxPacket = NULL;
            }
            _TXReady->write(0, currentTime);
            _DataOutLo->write(0, currentTime);
            _DataOutHi->write(0, currentTime);
            _RXValid->write(0, currentTime);
            _RXValidH->write(0, currentTime);
            _RXActive->write(0, currentTime);
            _RXError->write(0, currentTime);
            break;
        }
        default: ASSERT(0, NULL);
    }
    return UsbxTrue;
}

UsbxBool UsbxUtmiPhyIntf::refreshInputs(CarbonTime currentTime)
{
    UsbxBool retValue = UsbxTrue;
    retValue &= _Reset->refresh(currentTime);
    retValue &= _DataBus16_8->refresh(currentTime);
    retValue &= _SuspendM->refresh(currentTime);
    retValue &= _DataInLo->refresh(currentTime);
    retValue &= _DataInHi->refresh(currentTime);
    retValue &= _TXValid->refresh(currentTime);
    retValue &= _TXValidH->refresh(currentTime);
    retValue &= _XcvrSelect->refresh(currentTime);
    retValue &= _TermSelect->refresh(currentTime);
    retValue &= _OpMode->refresh(currentTime);
    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_1)
    {
        retValue &= _DpPulldown->refresh(currentTime);
        retValue &= _DmPulldown->refresh(currentTime);
        retValue &= _IdPullup->refresh(currentTime);
        retValue &= _DrvVbus->refresh(currentTime);
        retValue &= _ChrgVbus->refresh(currentTime);
        retValue &= _DischrgVbus->refresh(currentTime);
    }
    return retValue;
}

UsbxBool UsbxUtmiPhyIntf::refreshOutputs(CarbonTime currentTime)
{
    UsbxBool retValue = UsbxTrue;
    retValue &= _TXReady->refresh(currentTime);
    retValue &= _DataOutLo->refresh(currentTime);
    retValue &= _DataOutHi->refresh(currentTime);
    retValue &= _RXValid->refresh(currentTime);
    retValue &= _RXValidH->refresh(currentTime);
    retValue &= _RXActive->refresh(currentTime);
    retValue &= _RXError->refresh(currentTime);
    retValue &= _LineState->refresh(currentTime);
    if (_interfaceLevel == CarbonXUsbUtmiPinLevel_1)
    {
        retValue &= _HostDisconnect->refresh(currentTime);
        retValue &= _IdDig->refresh(currentTime);
        retValue &= _AValid->refresh(currentTime);
        retValue &= _BValid->refresh(currentTime);
        retValue &= _VbusValid->refresh(currentTime);
        retValue &= _SessEnd->refresh(currentTime);
    }
    return retValue;
}

