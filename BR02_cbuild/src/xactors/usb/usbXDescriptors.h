/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_DESCRIPTORS_H
#define USBX_DESCRIPTORS_H

#include "usbXDefines.h"
#include "xactors/usb/carbonXUsbDescriptors.h"

/*!
 * \brief Copies standard USB device descriptor structure to byte array
 *
 * This function copies a standard device descriptor structure to a byte array
 * which is passed via a pointer reference. The pointer is incremented as the
 * descriptor elements are copied.
 *
 * \param byteArray Reference to a pointer to the byte array
 * \param descriptor Pointer to the standard USB device descriptor structure
 */
void usbxGetBytesFromDescriptor(UsbxByte* &byteArray,
        const CarbonXUsbDeviceDescriptor *descriptor);

/*!
 * \brief Copies device descriptor byte array to standard device descriptor
 *        structure
 *
 * This function copies a USB device descriptor byte array to a standard USB
 * device descriptor structure. The pointer is incremented as the descriptor
 * elements are copied.
 *
 * \param byteArray  Pointer to the USB device descriptor byte array
 * \param descriptor Pointer to the standard USB device descriptor structure
 */
void usbxGetDescriptorFromBytes(const UsbxByte* &byteArray,
        CarbonXUsbDeviceDescriptor *descriptor);

/*!
 * \brief Destroys standard USB device descriptor
 *
 * \param descriptor Pointer to the standard device descriptor
 */
void usbxDestroyDescriptor(CarbonXUsbDeviceDescriptor *descriptor);

/*!
 * \brief Copies standard USB configuration descriptor structure to byte array
 *
 * This function copies a standard configuration descriptor structure to a
 * byte array which is passed via a pointer reference. The pointer is
 * incremented as the descriptor elements are copied.
 *
 * \param byteArray  Reference to a pointer to the byte array
 * \param descriptor Pointer to the standard USB configuration descriptor
 *                   structure
 */
void usbxGetBytesFromDescriptor(UsbxByte* &byteArray,
        const CarbonXUsbConfigurationDescriptor *descriptor);

/*!
 * \brief Copies configuration descriptor byte array to standard USB
 *        configuration descriptor structure
 *
 * This function copies a USB configuration descriptor byte array to a
 * standard USB configuration descriptor structure. The pointer is incremented
 * as the descriptor elements are copied.
 *
 * \param byteArray  Pointer to the USB configuration descriptor byte array
 * \param descriptor Pointer to the standard USB configuration descriptor
 *                   structure
 */
void usbxGetDescriptorFromBytes(const UsbxByte* &byteArray,
        CarbonXUsbConfigurationDescriptor *descriptor);

/*!
 * \brief Deletes configuration descriptor structure
 *
 * This function deletes the configuration descriptor structure along with
 * all of its interface descriptors and with their endpoints.
 *
 * \param descriptor Pointer to the standard USB configuration descriptor
 */
void usbxDestroyDescriptor(CarbonXUsbConfigurationDescriptor *descriptor);

/*!
 * \brief Copies standard USB interface descriptor structure to byte array
 *
 * This function copies a standard interface descriptor structure to a byte
 * array which is passed via a pointer reference. The pointer is incremented
 * as the descriptor elements are copied.
 *
 * \param byteArray Reference to a pointer to the byte array
 * \param descriptor Pointer to the standard USB interface descriptor structure
 */
void usbxGetBytesFromDescriptor(UsbxByte* &byteArray,
        const CarbonXUsbInterfaceDescriptor *descriptor);

/*!
 * \brief Copies interface descriptor byte array to standard USB interface
 *        descriptor structure
 *
 * This function copies a USB interface descriptor byte array to a standard USB
 * interface descriptor structure. The pointer is incremented as the
 * descriptor elements are copied.
 *
 * \param byteArray  Pointer to the USB interface descriptor byte array
 * \param descriptor Pointer to the standard USB interface descriptor structure
 */
void usbxGetDescriptorFromBytes(const UsbxByte* &byteArray,
        CarbonXUsbInterfaceDescriptor *descriptor);
/*!
 * \brief Deletes configuration descriptor structure
 *
 * This function deletes the configuration descriptor structure along with
 * all of its interface descriptors and with their endpoints
 *
 * \param descriptor Pointer to the standard USB configuration descriptor
 */
void usbxDestroyDescriptor(CarbonXUsbInterfaceDescriptor *descriptor);

/*!
 * \brief Copies standard USB endpoint descriptor structure to byte array
 *
 * This function copies a standard endpoint descriptor structure to a byte array
 * which is passed via a pointer reference. The pointer is incremented as the
 * descriptor elements are copied.
 *
 * \param byteArray Reference to a pointer to the byte array
 * \param descriptor Pointer to the standard USB endpoint descriptor structure
 */
void usbxGetBytesFromDescriptor(UsbxByte* &byteArray,
        const CarbonXUsbEndpointDescriptor *descriptor);

/*!
 * \brief Copies endpoint descriptor byte array to standard USB endpoint
 *        descriptor structure
 *
 * This function copies a USB endpoint descriptor byte array to a standard USB
 * endpoint descriptor structure. The pointer is incremented as the descriptor
 * elements are copied.
 *
 * \param byteArray  Pointer to the USB endpoint descriptor byte array
 * \param descriptor Pointer to the standard USB endpoint descriptor structure
 */
void usbxGetDescriptorFromBytes(const UsbxByte* &byteArray,
        CarbonXUsbEndpointDescriptor *descriptor);

/*!
 * \brief Destroys standard USB endpoint descriptor
 *
 * \param descriptor Pointer to the standard endpoint descriptor
 */
void usbxDestroyDescriptor(CarbonXUsbEndpointDescriptor *descriptor);

/*!
 * \brief Copies standard USB OTG descriptor structure to byte array
 *
 * This function copies a standard OTG descriptor structure to a byte array
 * which is passed via a pointer reference. The pointer is incremented as the
 * descriptor elements are copied.
 *
 * \param byteArray Reference to a pointer to the byte array
 * \param descriptor Pointer to the standard USB OTG descriptor structure
 */
void usbxGetBytesFromDescriptor(UsbxByte* &byteArray,
        const CarbonXUsbOtgDescriptor *descriptor);

/*!
 * \brief Copies OTG descriptor byte array to standard OTG descriptor
 *        structure
 *
 * This function copies a USB OTG descriptor byte array to a standard USB
 * OTG descriptor structure. The pointer is incremented as the descriptor
 * elements are copied.
 *
 * \param byteArray  Pointer to the USB OTG descriptor byte array
 * \param descriptor Pointer to the standard USB OTG descriptor structure
 */
void usbxGetDescriptorFromBytes(const UsbxByte* &byteArray,
        CarbonXUsbOtgDescriptor *descriptor);

/*!
 * \brief Destroys standard USB OTG descriptor
 *
 * \param descriptor Pointer to the standard OTG descriptor
 */
void usbxDestroyDescriptor(CarbonXUsbOtgDescriptor *descriptor);

/*!
 * \brief Checks standard device descriptor for errors
 *
 * This function checks for the validity or consistancy of a standard
 * device descriptor structure.
 *
 * \params descriptor Pointer to the descriptor structure
 * \params speed USB speed for which descriptor is to be checked, default is FS.
 *
 * \returns UsbxTrue  if check passed
 * \returns UsbxFalse if check failed
 */
UsbxBool usbxCheckDescriptor(CarbonXUsbDeviceDescriptor *descriptor,
        CarbonXUsbSpeedType speed = CarbonXUsbSpeed_FS);

/*!
 * \brief Checks standard Endpoint descriptor for errors
 *
 * This function checks for the validity or consistancy of a standard
 * Endpoint descriptor structure.
 *
 * \params descriptor Pointer to the descriptor structure
 * \params speed USB speed for which descriptor is to be checked, default is FS.
 *
 * \returns UsbxTrue  if check passed
 * \returns UsbxFalse if check failed
 */
UsbxBool usbxCheckDescriptor(CarbonXUsbEndpointDescriptor *descriptor,
        CarbonXUsbSpeedType speed = CarbonXUsbSpeed_FS);

/*!
 * \brief Checks standard Interface descriptor for errors
 *
 * This function checks for the validity or consistancy of a standard
 * Interface descriptor structure.
 *
 * \params descriptor Pointer to the descriptor structure
 *
 * \returns UsbxTrue  if check passed
 * \returns UsbxFalse if check failed
 */
UsbxBool usbxCheckDescriptor(CarbonXUsbInterfaceDescriptor *descriptor,
        CarbonXUsbSpeedType speed = CarbonXUsbSpeed_FS);

/*!
 * \brief Checks standard Configuration descriptor for errors
 *
 * This function checks for the validity or consistancy of a standard
 * configuration descriptor structure.
 *
 * \params descriptor Pointer to the descriptor structure
 *
 * \returns UsbxTrue  if check passed
 * \returns UsbxFalse if check failed
 */
UsbxBool usbxCheckDescriptor(CarbonXUsbConfigurationDescriptor *descriptor,
        CarbonXUsbSpeedType speed = CarbonXUsbSpeed_FS);

/*!
 * \brief Checks standard OTG descriptor for errors
 *
 * This function checks for the validity or consistancy of a standard
 * OTG descriptor structure.
 *
 * \params descriptor Pointer to the descriptor structure
 * \params speed USB speed for which descriptor is to be checked, default is FS.
 *
 * \returns UsbxTrue  if check passed
 * \returns UsbxFalse if check failed
 */
UsbxBool usbxCheckDescriptor(CarbonXUsbOtgDescriptor *descriptor,
        CarbonXUsbSpeedType speed = CarbonXUsbSpeed_FS);

/*!
 * \brief Checks Descriptor byte array for errors
 *
 * This function checks for the validity or consistancy of standard
 * descriptors provided in the form of a byte array.
 *
 * \params byteArray Pointer to the byte array
 * \params speed USB speed for which descriptor is to be checked, default is FS.
 *
 * \returns UsbxTrue  if check passed
 * \returns UsbxFalse if check failed
 */
UsbxBool usbxCheckDescriptor(const UsbxByte *byteArray,
        CarbonXUsbSpeedType speed = CarbonXUsbSpeed_FS);

/*!
 * \brief Gets size of a descriptor in terms of bytes
 *
 * This function returns the length of a descriptor(device or configuration)
 * in terms of bytes.
 *
 * \params byteArray Pointer to the descriptor byte array
 *
 * \returns Size of the descriptor in bytes.
 */
UInt usbxGetDescriptorSize(const UsbxByte *byteArray);

/*!
 * \brief Gets the configuration descriptor number
 *
 * This function returns the bConfigurationValue that is used as an argument
 * to the USB SetConfiguration() request to select this configuration.
 *
 * \params configDesc Pointer to the configuration descriptor byte array
 *
 * \returns Configuration descriptor number
 */
UInt usbxGetConfigDescNumber(const UsbxByte *configDesc);

/*!
 * \brief Returns the number of endpoint descriptors from a configuration
 *
 * \params configDesc Pointer to the configuration descriptor byte array
 *
 * \returns Number of endpoints
 */
UInt usbxGetNumEpsFromConfig(const UsbxByte *configDesc);

/*!
 * \brief Gets the handle of next endpoint descriptor within a configuration
 *
 * This routine searches for and returns the handle to next endpoint descriptor
 * within the configuration descriptor byte area, the handle of which is
 * supplied as the argument.
 *
 * \params configDescArea Pointer to the configuration descriptor byte area
 * \params continueIfEp   Flag to indicate whether to continue search if the
 *                        current descriptor is an endpoint descriptor.
 *                        Default is set to UsbxTrue.
 *
 * \returns Pointer to the next endpoint descriptor, if exists, else NULL.
 */
const UsbxByte *usbxGetNextEpDescriptor(const UsbxByte *configDescArea,
        UsbxBool continueIfEp = UsbxTrue);

#endif
