/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_PIN_H
#define USBX_PIN_H

#include "usbXDefines.h"
#include "xactors/carbonX.h"

/*!
 * \file usbXPin.h
 * \brief Header file for pin definition.
 *
 * This file provides the class definition for generic address, control and
 * data pins.
 */

/*!
 * \brief Direction type
 *
 * This type defines the direction of pin. This will control the direction
 * of data flow to and from the transactor during refresh. Normal read and
 * write will return or update an internal value, which actually becomes
 * operational during refresh cycle.
 */
enum UsbxDirection {
    UsbxDirection_Unknown,
    UsbxDirection_Input,
    UsbxDirection_Output,
    UsbxDirection_Inout
};

/*!
 * \brief Class to represent transactor's generic pin.
 *
 * This represents UTMI or ULPI data and handshake pins, which can be connected
 * with VHM by a CarbonNet or through read/write callback.
 */
class UsbxPin
{
public: CARBONMEM_OVERRIDES

private:

    const UsbXtor *_xtor;                   //!< Transactor reference
    UsbxConstString _name;                  //!< Pin name
    const UsbxDirection _direction;         //!< Pin direction
    UsbxDirection _inoutDirControl;         //!< Inout direction control
    const CarbonUInt32 _size;               //!< Pin width <= 32
    CarbonUInt32 _currValue;                //!< Buffered value of net, which
                                            //   updated during referesh cycle.

#ifdef _VCD
    CarbonUInt32 _lastValue;                //!< Last value
    static UsbxBool _enableBusDump;         //!< enable bus dump
#endif

    /* VHM connection mode */
    CarbonObjectID *_module;                //!< Connected VHM module
    CarbonNetID *_net;                      //!< Connected VHM net

    /* Callback connection mode */
    CarbonXInterconnectNameCallbackTrio _callbackTrio; //!< Callback trio

public:

    // Constructor and destructor
    UsbxPin(UsbXtor *xtor, UsbxConstString name,
            const UsbxDirection direction, const CarbonUInt32 size = 1);
    ~UsbxPin();

    // Member access functions
    UsbxConstString getName() const { return _name; }
    UsbxDirection getDirection() const { return _direction; }
    void setInoutDirControl(UsbxDirection control) // Call from FSM evaluation
    { _inoutDirControl = control; }
    UsbxDirection getInoutDirControl() const { return _inoutDirControl; }
    CarbonUInt32 getSize() const { return _size; }

    // Environment binding
    UsbxBool bind(CarbonObjectID *module, CarbonNetID *net);
    UsbxBool bind(CarbonXInterconnectNameCallbackTrio trio);

    // Read/Write access during evaluation
    UsbxBool write(const CarbonUInt32 data, CarbonTime currentTime);
    UsbxBool read(CarbonUInt32 &data, CarbonTime currentTime) const;
    CarbonUInt32 read(CarbonTime currentTime) const; // same as above

    // Refresh
    UsbxBool refresh(CarbonTime currentTime);

    // Utility functions
    // Dumps the pin state
    void print(CarbonTime currentTime) const;
#ifdef _VCD
    UsbxBool dumpVcd(UsbxBool forceDump = UsbxFalse);
    static UsbxBool enableVcdDump(UsbxConstString fileName = NULL);
#endif
};

#endif
