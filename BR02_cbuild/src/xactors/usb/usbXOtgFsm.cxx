/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    usbXOtgFsm.cpp
    Purpose: This file provides implementation for OTG FSM.
*/

#include "usbXOtgFsm.h"

// #define DEBUG

const UInt UsbxOtgFsm::_clocksForSrp = (6000000 * 12) / 10; // 100 ms + 20% tol
const UInt UsbxOtgFsm::_clocksForHnp = 12000000; // 200 ms
const UInt UsbxOtgFsm::_clocksForHnpStop = 180000; // 3 - 200 ms, take 3 ms

UsbxOtgFsm::UsbxOtgFsm(UsbxOtgDeviceXtor *xtor,
        UsbxHostFsm *hostFsm, UsbxDeviceFsm *deviceFsm): UsbxFsm(), _xtor(xtor),
    _hostFsm(hostFsm), _deviceFsm(deviceFsm), _state(UsbxOtgState_reset),
    _clockCountForWait(0)
{}

UsbxOtgFsm::~UsbxOtgFsm()
{
}

UsbxBool UsbxOtgFsm::startSession()
{
    // Get commonly used variables
    UsbxIntf *interface = _xtor->getInterface();
    ASSERT(interface, _xtor);

    if (_xtor->isHost() && _state == UsbxOtgState_a_idle)
    {
        _state = UsbxOtgState_a_wait_vrise;
        interface->driveVbus(UsbxTrue);
        return UsbxTrue;
    }
    else if (!_xtor->isHost() && _state == UsbxOtgState_b_idle &&
            _xtor->getConfig()->isOtgSrpSupported())
    {
        _state = UsbxOtgState_b_srp_init;
        return interface->startTiming(UsbxTiming_OtgSrp);
    }
    return UsbxFalse;
}

UsbxBool UsbxOtgFsm::stopSession()
{
    if (_xtor->isHost() && (_state == UsbxOtgState_a_host) &&
            (_hostFsm->isSuspended() || _hostFsm->isDeviceDetached()))
    {
        // Get commonly used variables
        UsbxIntf *interface = _xtor->getInterface();
        ASSERT(interface, _xtor);

        _state = UsbxOtgState_a_wait_vfall;
        interface->driveVbus(UsbxFalse);
        return UsbxTrue;
    }
    return UsbxFalse;
}

void UsbxOtgFsm::resetForDetach()
{
    _hostFsm->resetForDetach();
    _deviceFsm->resetForDetach();
}

UsbxBool UsbxOtgFsm::evaluate(CarbonTime currentTime)
{
    UsbxBool retStatus = UsbxTrue;

    // Get commonly used variables
    UsbxIntf *interface = _xtor->getInterface();
    ASSERT(interface, _xtor);
    UInt timingFactor = interface->getTimingFactor();
    UInt speedFactor = _xtor->getConfig()->getUtmiDataBusWidth() / 8;

#ifdef DEBUG
    UsbxOtgState lastState = _state;
#endif

    // Otg Finite State Machine
    switch (_state)
    {
        case UsbxOtgState_reset:
        {
            if (_xtor->getOtgDeviceType() == UsbxOtgDevice_A)
            {
                _xtor->setHost(UsbxTrue);
                _state = UsbxOtgState_a_idle;
            }
            else
            {
                _xtor->setHost(UsbxFalse);
                _state = UsbxOtgState_b_idle;
            }
            break;
        }
        // FSM for A Device having mini-A plug attached
        case UsbxOtgState_a_idle:
        {
            if (_xtor->getOtgDeviceType() == UsbxOtgDevice_B)
            {
                _xtor->setHost(UsbxFalse);
                _state = UsbxOtgState_b_idle;
            }
            else if (!_xtor->getConfig()->isOtgSrpSupported())
                ;
            else if (_clockCountForWait > 0)
            {
                _clockCountForWait--;
                if (_clockCountForWait == 0)
                {
                    _state = UsbxOtgState_a_wait_vrise;
                    interface->driveVbus(UsbxTrue);
                }
            }
            else if (((_xtor->getConfig()->getOtgSrpDetectionSupport() ==
                        CarbonXUsbOtgSrpDetection_VbusPulsing) &&
                    interface->senseTiming(UsbxTiming_OtgSrp, UsbxFalse))
                    || ((_xtor->getConfig()->getOtgSrpDetectionSupport() ==
                     CarbonXUsbOtgSrpDetection_DataLinePulsing) &&
                    interface->senseTiming(UsbxTiming_DeviceAttach, UsbxFalse)))
                _clockCountForWait = (_clocksForSrp / timingFactor) /
                    speedFactor;
            break;
        }
        case UsbxOtgState_a_wait_vrise:
        {
            if (interface->isVbusValid())
            {
                _state = UsbxOtgState_a_host;
                _xtor->reportCommand(CarbonXUsbCommand_OtgStartSession);

                // Flush data received unwantedly
                UsbxPacket *packet = interface->receivePacket();
                if (packet)
                {
                    ASSERT(packet->hasReceiveError() !=
                            UsbxPacketReceiveError_None, _xtor);
                    delete packet;
                }
            }
            break;
        }
        case UsbxOtgState_a_wait_vfall:
        {
            if (!interface->isVbusValid())
            {
                _state = UsbxOtgState_a_idle;
                _xtor->setHost(_xtor->isHost());
                _xtor->reportCommand(CarbonXUsbCommand_OtgStopSession);
            }
            break;
        }
        case UsbxOtgState_a_host:
        {
            if (_xtor->isInHnp() && _hostFsm->isSuspended())
                _state = UsbxOtgState_a_suspend;
            _hostFsm->evaluate(currentTime);
            break;
        }
        case UsbxOtgState_a_suspend:
        {
            if (!_hostFsm->isSuspended()) // resume detected
                _state = UsbxOtgState_a_host;
            else if (interface->senseTiming(UsbxTiming_OtgHnp, UsbxFalse))
            {
                _xtor->setHost(UsbxFalse);
                _state = UsbxOtgState_a_peripheral;
#ifndef HNP_DETACH
                _xtor->attach();
#endif
                _xtor->reportCommand(CarbonXUsbCommand_OtgStartHnp);
                break;
            }
            _hostFsm->evaluate(currentTime);
            break;
        }
        case UsbxOtgState_a_peripheral:
        {
            // HNP stopped - go back to a_host
            if (_clockCountForWait > 0)
            {
                _clockCountForWait--;
                if (_clockCountForWait == 0)
                {
                    _xtor->setHost(UsbxTrue);
                    interface->driveVbus(UsbxFalse);
                    _state = UsbxOtgState_a_idle;
                    _xtor->setInHnp(UsbxFalse);
                    _xtor->reportCommand(CarbonXUsbCommand_OtgStopHnp);
                    break;
                }
            }
            else if (_xtor->isInHnp() && _deviceFsm->isSuspended())
                _clockCountForWait = (_clocksForHnpStop /
                        (timingFactor < 60 ? 1 : (timingFactor / 60))) /
                    speedFactor;
            _deviceFsm->evaluate(currentTime);
            break;
        }
        // FSM for B Device having mini-B plug attached
        case UsbxOtgState_b_idle:
        {
            if (_xtor->getOtgDeviceType() == UsbxOtgDevice_A)
            {
                _xtor->setHost(UsbxTrue);
                _state = UsbxOtgState_a_idle;
            }
            else if (interface->isVbusValid())
                _state = UsbxOtgState_b_peripheral;
            break;
        }
        case UsbxOtgState_b_srp_init:
        {
            if (interface->isVbusValid())
                _state = UsbxOtgState_b_peripheral;
            break;
        }
        case UsbxOtgState_b_peripheral:
        {
            if (!interface->isVbusValid())
            {
                _state = UsbxOtgState_b_idle;
                _xtor->setHost(_xtor->isHost());
                break;
            }
            else if (_clockCountForWait > 0)
            {
                if (!_deviceFsm->isSuspended()) // resume detected
                    _clockCountForWait = 0;
                else
                {
                    _clockCountForWait--;
                    if (_clockCountForWait == 0)
                    {
                        _xtor->setHost(UsbxTrue);
                        _state = UsbxOtgState_b_host;
                        interface->driveVbus(UsbxTrue);
                        interface->startTiming(UsbxTiming_OtgHnp);
                        _xtor->reportCommand(CarbonXUsbCommand_OtgStartHnp);
                        break;
                    }
                }
            }
            else if (_xtor->isInHnp() && _deviceFsm->isSuspended())
                _clockCountForWait = (_clocksForHnp /
                        (timingFactor < 60 ? 1 : (timingFactor / 60))) /
                    speedFactor;
            _deviceFsm->evaluate(currentTime);
            break;
        }
        case UsbxOtgState_b_host:
        {
            // HNP stopped - go back to b_peripheral
            if (_xtor->isInHnp() && _hostFsm->isSuspended())
            {
                _xtor->setHost(UsbxFalse);
                _state = UsbxOtgState_b_idle;
#ifndef HNP_DETACH
                _xtor->attach();
#endif
                _xtor->setInHnp(UsbxFalse);
                _xtor->reportCommand(CarbonXUsbCommand_OtgStopHnp);
                break;
            }
            _hostFsm->evaluate(currentTime);
            break;
        }
        default: ASSERT(0, _xtor);
    }

#ifdef DEBUG
    if (_state != lastState)
        printf("DEBUG[%d]: %s: OTG %s FSM: State: %d\n",
                static_cast<UInt>(currentTime), _xtor->getName(),
                _xtor->isHost() ? "HOST" : "DEVICE", _state);
#endif
    return retStatus;
    currentTime = 0; // To remove warning
}

void UsbxOtgFsm::print(CarbonTime currentTime) const
{
    printf("OTG FSM %s transactor:\n", _xtor->getName());
    printf("Current time: %d\n", static_cast<UInt>(currentTime));
    printf("State number: %d\n", _state);
    return;
}

