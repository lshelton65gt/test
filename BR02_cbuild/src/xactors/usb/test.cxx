//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

// Internal test 1.file

#include "usbXQueue.h"
#include "usbXPacket.h"
#include "usbXDescriptors.h"
#include "xactors/usb/carbonXUsbDescriptors.h"
#include "testConnectCallbacks.cpp"
#include "usbXUtmiIntf.h"
#include "usbXUlpiIntf.h"
#include "usbXtor.h"

int main()
{
#ifndef _ULPI
    // test 0.1
    {
        printf("---- test 0.1 ----\n");
        UsbxQueue<int> queue;
        int a = 1, b = 2, c = 3;
        queue.push(&a); queue.push(&b); queue.push(&c);
        UsbxQueue<int>::UsbxQueueIterator iter;
        for (iter = queue.begin(); iter != queue.end(); iter++)
            printf("%d\n", **iter);
    }

    // test 1.1
    {
        printf("---- test 1.1 ----\n");
        UsbxSetupPacket packet(0x15, 0xE);
        packet.print();
        UsbxPacket *receivedPacket = UsbxPacket::getPacket(
                packet.getSizeInBytes(), packet.getByteArray());
        receivedPacket->print();
        delete receivedPacket;
    }
    // test 1.2
    {
        printf("---- test 1.2 ----\n");
        UsbxOutPacket packet(0x3A, 0xA);
        packet.print();
        UsbxPacket *receivedPacket = UsbxPacket::getPacket(
                packet.getSizeInBytes() - 1, packet.getByteArray());
        receivedPacket->print();
        delete receivedPacket;
    }
    // test 1.3
    {
        printf("---- test 1.3 ----\n");
        UsbxInPacket packet(0x70, 0x4);
        packet.print();
        UsbxByte *byteArray = const_cast<UsbxByte*>(packet.getByteArray());
        byteArray[1] = ~byteArray[1]; // corrupting data
        UsbxPacket *receivedPacket = UsbxPacket::getPacket(
                packet.getSizeInBytes(), byteArray);
        receivedPacket->print();
        delete receivedPacket;
    }
    // test 1.4
    {
        printf("---- test 1.4 ----\n");
        UsbxSofPacket packet(100);
        packet.print();
        UsbxPacket *receivedPacket = UsbxPacket::getPacket(
                packet.getSizeInBytes(), packet.getByteArray());
        receivedPacket->print();
        delete receivedPacket;
    }
    // test 1.5
    {
        printf("---- test 1.5 ----\n");
        UsbxByte byteArray[4];
        byteArray[0] = 0x00;
        byteArray[1] = 0x01;
        byteArray[2] = 0x02;
        byteArray[3] = 0x03;
        UsbxData0Packet packet(4, byteArray);
        packet.print();
        UsbxPacket *receivedPacket = UsbxPacket::getPacket(
                packet.getSizeInBytes(), packet.getByteArray());
        receivedPacket->print();
        delete receivedPacket;
    }
    // test 1.6
    {
        printf("---- test 1.6 ----\n");
        UsbxByte byteArray[4];
        byteArray[0] = 0x23;
        byteArray[1] = 0x45;
        byteArray[2] = 0x67;
        byteArray[3] = 0x89;
        UsbxData1Packet packet(4, byteArray);
        packet.print();
        UsbxPacket *receivedPacket = UsbxPacket::getPacket(
                packet.getSizeInBytes(), packet.getByteArray());
        receivedPacket->print();
        delete receivedPacket;
    }
    // test 1.7
    {
        printf("---- test 1.7 ----\n");
        UsbxAckPacket packet;
        packet.print();
        UsbxPacket *receivedPacket = UsbxPacket::getPacket(
                packet.getSizeInBytes(), packet.getByteArray());
        receivedPacket->print();
        delete receivedPacket;
    }
    // test 1.8
    {
        printf("---- test 1.8 ----\n");
        UsbxNakPacket packet;
        packet.print();
        UsbxPacket *receivedPacket = UsbxPacket::getPacket(
                packet.getSizeInBytes(), packet.getByteArray());
        receivedPacket->print();
        delete receivedPacket;
    }
    // test 1.9
    {
        printf("---- test 1.9 ----\n");
        UsbxStallPacket packet;
        packet.print();
        UsbxPacket *receivedPacket = UsbxPacket::getPacket(
                packet.getSizeInBytes(), packet.getByteArray());
        receivedPacket->print();
        delete receivedPacket;
    }
    // test 1.10
    {
        printf("---- test 1.10 ----\n");
        UsbxNyetPacket packet;
        packet.print();
        UsbxPacket *receivedPacket = UsbxPacket::getPacket(
                packet.getSizeInBytes(), packet.getByteArray());
        receivedPacket->print();
        delete receivedPacket;
    }
    // test 1.11
    {
        printf("---- test 1.11 ----\n");
        UsbxSplitPacket packet(0x1, 0x1, 0x4, 0x1, 0x0, 0x3);
        packet.print();
        UsbxPacket *receivedPacket = UsbxPacket::getPacket(
                packet.getSizeInBytes(), packet.getByteArray());
        receivedPacket->print();
        delete receivedPacket;
    }
    // test 1.12
    {
        printf("---- test 1.12 ----\n");
        UsbxErrPacket packet;
        packet.print();
        UsbxPacket *receivedPacket = UsbxPacket::getPacket(
                packet.getSizeInBytes(), packet.getByteArray());
        receivedPacket->print();
        delete receivedPacket;
    }
    // test 1.13
    {
        printf("---- test 1.13 ----\n");
        UsbxPingPacket packet(0x15, 0xE);
        packet.print();
        UsbxPacket *receivedPacket = UsbxPacket::getPacket(
                packet.getSizeInBytes(), packet.getByteArray());
        receivedPacket->print();
        delete receivedPacket;
    }

    // test 2.1 for device descriptor
    {
        printf("---- test 2.1 ----\n");
        CarbonXUsbDeviceDescriptor *dDesc = NEW(CarbonXUsbDeviceDescriptor);
        dDesc->bcdUSB = 0x0210;
        dDesc->bDeviceClass = 0xE0; // Wireless Controller
        dDesc->bDeviceSubClass = 0x1;
        dDesc->bDeviceProtocol = 0x1;
        dDesc->bMaxPacketSize0 = 0x40;
        dDesc->idVendor = 0x1131;
        dDesc->idProduct = 0x1001;
        dDesc->bcdDevice = 0x373;
        dDesc->iManufacturer = 0x1;
        dDesc->iProduct = 0x2;
        dDesc->iSerialNumber = 0x0;
        dDesc->bNumConfigurations = 0x1;

        CarbonUInt8 *bytes = carbonXUsbCreateBytesFromDeviceDescriptor(dDesc);

        printf("Device Descriptor\n");
        if (carbonXUsbCheckDescriptor(bytes, CarbonXUsbSpeed_HS) == 0)
            printf("ERROR in descriptor\n");
        for (int i = 0; i < 18; i++)
            printf("byte[%d] = 0x%X\n", i, bytes[i]);
        DELETE(bytes);
        carbonXUsbDestroyDeviceDescriptor(dDesc);
    }
    // test 2.2 for endpoint descriptor
    {
        printf("---- test 2.2 ----\n");
        CarbonXUsbEndpointDescriptor *epDesc =
            NEW(CarbonXUsbEndpointDescriptor);
        epDesc->bEndpointAddress = 0x81;
        epDesc->bmAttributes = 0x3;
        epDesc->wMaxPacketSize = 0x10;
        epDesc->bInterval = 0x1;

        CarbonUInt8 *bytes =
            carbonXUsbCreateBytesFromEndpointDescriptor(epDesc);
        printf("Endpoint Descriptor\n");
        if (carbonXUsbCheckDescriptor(bytes, CarbonXUsbSpeed_HS) == 0)
            printf("ERROR in descriptor\n");
        for (int i = 0; i < 7; i++)
            printf("byte[%d] = 0x%X\n", i, bytes[i]);
        DELETE(bytes);
        carbonXUsbDestroyEndpointDescriptor(epDesc);
    }
    // test 2.3 configuration descriptor
    {
        printf("---- test 2.3 ----\n");
        CarbonXUsbConfigurationDescriptor *configDesc =
            NEW(CarbonXUsbConfigurationDescriptor);
        configDesc->wTotalLength = 32;
        configDesc->bNumInterfaces = 0x1;
        configDesc->bConfigurationValue = 0x1;
        configDesc->iConfiguration = 0x0;
        configDesc->bmAttributes = 0x80;
        configDesc->bMaxPower = 0x1;
        configDesc->intfDescriptor =
            NEWARRAY(CarbonXUsbInterfaceDescriptor*, 1);
        configDesc->intfDescriptor[0] = NEW(CarbonXUsbInterfaceDescriptor);
        CarbonXUsbInterfaceDescriptor *intfDesc = configDesc->intfDescriptor[0];
        intfDesc->bInterfaceNumber = 0;
        intfDesc->bAlternateSetting = 0;
        intfDesc->bNumEndpoints = 2;
        intfDesc->bInterfaceClass = 0x0;
        intfDesc->bInterfaceSubClass = 0x0;
        intfDesc->bInterfaceProtocol = 0x0;
        intfDesc->iInterface = 0x2;
        intfDesc->epDescriptor = NEWARRAY(CarbonXUsbEndpointDescriptor*, 2);
        intfDesc->epDescriptor[0] = NEW(CarbonXUsbEndpointDescriptor);
        intfDesc->epDescriptor[1] = NEW(CarbonXUsbEndpointDescriptor);

        CarbonXUsbEndpointDescriptor *ep1 = intfDesc->epDescriptor[0];
        CarbonXUsbEndpointDescriptor *ep2 = intfDesc->epDescriptor[1];

        ep1->bEndpointAddress = 0x2;
        ep1->bmAttributes = 0x2;
        ep1->wMaxPacketSize = 0x40;
        ep1->bInterval = 0x1;

        ep2->bEndpointAddress = 0x82;
        ep2->bmAttributes = 0x2;
        ep2->wMaxPacketSize = 0x40;
        ep2->bInterval = 0x1;
        CarbonUInt8 *bytes =
            carbonXUsbCreateBytesFromConfigurationDescriptor(configDesc);
        printf("Configuration Descriptor\n");
        if (carbonXUsbCheckDescriptor(bytes, CarbonXUsbSpeed_HS) == 0)
            printf("ERROR in descriptor\n");
        for (int i = 0; i < 32; i++)
            printf("byte[%d] = 0x%X\n", i, bytes[i]);

        // getting handle to the endpoint descriptor from the configuration
        {
            printf("---- test 2.3.1 ----\n");
            printf("searching endpoint descriptors from configuration\n");
            int numEp = usbxGetNumEpsFromConfig(bytes);
            printf("Number of Endpoint Descriptors = %d\n", numEp);
            if (numEp > 1)
            {
                // printing the 1st ep
                const CarbonUInt8 *epDesc = usbxGetNextEpDescriptor(bytes);
                printf("Endpoint Descriptor\n");
                for (int i = 0; i < 7; i++)
                    printf("byte[%d] = 0x%X\n", i, epDesc[i]);
                // printing the other eps
                for (int i = 0; i < numEp - 1; i++)
                {
                    epDesc = usbxGetNextEpDescriptor(epDesc);
                    printf("Endpoint Descriptor\n");
                    for (int i = 0; i < 7; i++)
                        printf("byte[%d] = 0x%X\n", i, epDesc[i]);
                }
            }
            else if (numEp == 1)
            {
                // printing the 1st ep
                const CarbonUInt8 *epDesc = usbxGetNextEpDescriptor(bytes);
                printf("Endpoint Descriptor\n");
                for (int i = 0; i < 7; i++)
                    printf("byte[%d] = 0x%X\n", i, epDesc[i]);
            }
        }

        // getting handle to the endpoint descriptor from the interface
        {
            printf("---- test 2.3.2 ----\n");
            CarbonUInt8 *intfDesc = bytes + 9;
            printf("searching endpoint descriptors from interface\n");
            int numEp = usbxGetNumEpsFromConfig(bytes);
            printf("Number of Endpoint Descriptors = %d\n", numEp);
            if (numEp > 1)
            {
                // printing the 1st ep
                const CarbonUInt8 *epDesc = usbxGetNextEpDescriptor(intfDesc);
                printf("Endpoint Descriptor\n");
                for (int i = 0; i < 7; i++)
                    printf("byte[%d] = 0x%X\n", i, epDesc[i]);
                // printing the other eps
                for (int i = 0; i < numEp - 1; i++)
                {
                    epDesc = usbxGetNextEpDescriptor(epDesc);
                    printf("Endpoint Descriptor\n");
                    for (int i = 0; i < 7; i++)
                        printf("byte[%d] = 0x%X\n", i, epDesc[i]);
                }
            }
            else if (numEp == 1)
            {
                // printing the 1st ep
                const CarbonUInt8 *epDesc = usbxGetNextEpDescriptor(intfDesc);
                printf("Endpoint Descriptor\n");
                for (int i = 0; i < 7; i++)
                    printf("byte[%d] = 0x%X\n", i, epDesc[i]);
            }
        }

        DELETE(bytes);
        carbonXUsbDestroyConfigurationDescriptor(configDesc);
    }
    // test 2.4 for device descriptor
    {
        printf("---- test 2.4 ----\n");
        CarbonXUsbOtgDescriptor *otgDesc = NEW(CarbonXUsbOtgDescriptor);
        otgDesc->bmAttributes = 0x1;

        CarbonUInt8 *bytes = carbonXUsbCreateBytesFromOtgDescriptor(otgDesc);

        printf("OTG Descriptor\n");
        if (carbonXUsbCheckDescriptor(bytes, CarbonXUsbSpeed_HS) == 0)
            printf("ERROR in descriptor\n");
        for (int i = 0; i < 3; i++)
            printf("byte[%d] = 0x%X\n", i, bytes[i]);
        DELETE(bytes);
        carbonXUsbDestroyOtgDescriptor(otgDesc);
    }
    // test 3.1
    {
        printf("---- test 3.1 ----\n");
#ifdef _VCD
        UsbxPin::enableVcdDump("test-3.1.vcd");
#endif
        UsbxUtmiLinkIntf linkIntf(NULL);
        linkIntf.setSpeedFS(UsbxFalse, 0);
        linkIntf.BIND("Reset", Link_Reset);
        linkIntf.BIND("DataBus16_8", Link_DataBus16_8);
        linkIntf.BIND("SuspendM", Link_SuspendM);
        linkIntf.BIND("DataInLo", Link_DataInLo);
        linkIntf.BIND("DataInHi", Link_DataInHi);
        linkIntf.BIND("TXValid", Link_TXValid);
        linkIntf.BIND("TXValidH", Link_TXValidH);
        linkIntf.BIND("TXReady", Link_TXReady);
        linkIntf.BIND("DataOutLo", Link_DataOutLo);
        linkIntf.BIND("DataOutHi", Link_DataOutHi);
        linkIntf.BIND("RXValid", Link_RXValid);
        linkIntf.BIND("RXValidH", Link_RXValidH);
        linkIntf.BIND("RXActive", Link_RXActive);
        linkIntf.BIND("RXError", Link_RXError);
        linkIntf.BIND("XcvrSelect", Link_XcvrSelect);
        linkIntf.BIND("TermSelect", Link_TermSelect);
        linkIntf.BIND("OpMode", Link_OpMode);
        linkIntf.BIND("LineState", Link_LineState);

        UsbxUtmiPhyIntf phyIntf(NULL);
        phyIntf.setSpeedFS(UsbxFalse, 0);
        phyIntf.BIND("Reset", Phy_Reset);
        phyIntf.BIND("DataBus16_8", Phy_DataBus16_8);
        phyIntf.BIND("SuspendM", Phy_SuspendM);
        phyIntf.BIND("DataInLo", Phy_DataInLo);
        phyIntf.BIND("DataInHi", Phy_DataInHi);
        phyIntf.BIND("TXValid", Phy_TXValid);
        phyIntf.BIND("TXValidH", Phy_TXValidH);
        phyIntf.BIND("TXReady", Phy_TXReady);
        phyIntf.BIND("DataOutLo", Phy_DataOutLo);
        phyIntf.BIND("DataOutHi", Phy_DataOutHi);
        phyIntf.BIND("RXValid", Phy_RXValid);
        phyIntf.BIND("RXValidH", Phy_RXValidH);
        phyIntf.BIND("RXActive", Phy_RXActive);
        phyIntf.BIND("RXError", Phy_RXError);
        phyIntf.BIND("XcvrSelect", Phy_XcvrSelect);
        phyIntf.BIND("TermSelect", Phy_TermSelect);
        phyIntf.BIND("OpMode", Phy_OpMode);
        phyIntf.BIND("LineState", Phy_LineState);

        CarbonTime t = 0;
#define RUN                             \
        {                               \
            linkIntf.refreshInputs(t);  \
            phyIntf.refreshInputs(t);   \
            linkIntf.evaluate(t);       \
            phyIntf.evaluate(t);        \
            linkIntf.refreshOutputs(t); \
            phyIntf.refreshOutputs(t);  \
            t += 10;                    \
        }

        // Set the UTM in HS mode
        linkIntf.getPin("XcvrSelect")->write(0, t);

        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxAckPacket txPacket;
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxNakPacket txPacket;
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[30];
            for (int i = 0; i < 30; i++)
                byteArray[i] = 0xFF;
            UsbxData0Packet txPacket(30, byteArray);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[30];
            for (int i = 0; i < 30; i++)
                byteArray[i] = 0xFF;
            UsbxData0Packet txPacket(30, byteArray);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
    }
    // test 3.2
    {
        printf("---- test 3.2 ----\n");
#ifdef _VCD
        UsbxPin::enableVcdDump("test-3.2.vcd");
#endif
        UsbxUtmiLinkIntf linkIntf(NULL, 16);
        linkIntf.setSpeedFS(UsbxFalse, 0);
        linkIntf.BIND("Reset", Link_Reset);
        linkIntf.BIND("DataBus16_8", Link_DataBus16_8);
        linkIntf.BIND("SuspendM", Link_SuspendM);
        linkIntf.BIND("DataInLo", Link_DataInLo);
        linkIntf.BIND("DataInHi", Link_DataInHi);
        linkIntf.BIND("TXValid", Link_TXValid);
        linkIntf.BIND("TXValidH", Link_TXValidH);
        linkIntf.BIND("TXReady", Link_TXReady);
        linkIntf.BIND("DataOutLo", Link_DataOutLo);
        linkIntf.BIND("DataOutHi", Link_DataOutHi);
        linkIntf.BIND("RXValid", Link_RXValid);
        linkIntf.BIND("RXValidH", Link_RXValidH);
        linkIntf.BIND("RXActive", Link_RXActive);
        linkIntf.BIND("RXError", Link_RXError);
        linkIntf.BIND("XcvrSelect", Link_XcvrSelect);
        linkIntf.BIND("TermSelect", Link_TermSelect);
        linkIntf.BIND("OpMode", Link_OpMode);
        linkIntf.BIND("LineState", Link_LineState);

        UsbxUtmiPhyIntf phyIntf(NULL, 16);
        phyIntf.setSpeedFS(UsbxFalse, 0);
        phyIntf.BIND("Reset", Phy_Reset);
        phyIntf.BIND("DataBus16_8", Phy_DataBus16_8);
        phyIntf.BIND("SuspendM", Phy_SuspendM);
        phyIntf.BIND("DataInLo", Phy_DataInLo);
        phyIntf.BIND("DataInHi", Phy_DataInHi);
        phyIntf.BIND("TXValid", Phy_TXValid);
        phyIntf.BIND("TXValidH", Phy_TXValidH);
        phyIntf.BIND("TXReady", Phy_TXReady);
        phyIntf.BIND("DataOutLo", Phy_DataOutLo);
        phyIntf.BIND("DataOutHi", Phy_DataOutHi);
        phyIntf.BIND("RXValid", Phy_RXValid);
        phyIntf.BIND("RXValidH", Phy_RXValidH);
        phyIntf.BIND("RXActive", Phy_RXActive);
        phyIntf.BIND("RXError", Phy_RXError);
        phyIntf.BIND("XcvrSelect", Phy_XcvrSelect);
        phyIntf.BIND("TermSelect", Phy_TermSelect);
        phyIntf.BIND("OpMode", Phy_OpMode);
        phyIntf.BIND("LineState", Phy_LineState);

        CarbonTime t = 0;
#define RUN                             \
        {                               \
            linkIntf.refreshInputs(t);  \
            phyIntf.refreshInputs(t);   \
            linkIntf.evaluate(t);       \
            phyIntf.evaluate(t);        \
            linkIntf.refreshOutputs(t); \
            phyIntf.refreshOutputs(t);  \
            t += 10;                    \
        }

        // Set the UTM in HS mode
        linkIntf.getPin("XcvrSelect")->write(0, t);

        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxAckPacket txPacket;
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxNakPacket txPacket;
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[50];
            for (int i = 0; i < 50; i++)
                byteArray[i] = 0xFE;
            UsbxData0Packet txPacket(50, byteArray);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[50];
            for (int i = 0; i < 50; i++)
                byteArray[i] = 0xFE;
            UsbxData0Packet txPacket(50, byteArray);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
    }
    // test 3.3
    {
        printf("---- test 3.3 ----\n");
#ifdef _VCD
        UsbxPin::enableVcdDump("test-3.3.vcd");
#endif
        UsbxUtmiLinkIntf linkIntf(NULL);
        linkIntf.setSpeedFS(UsbxTrue, 0);
        linkIntf.BIND("Reset", Link_Reset);
        linkIntf.BIND("DataBus16_8", Link_DataBus16_8);
        linkIntf.BIND("SuspendM", Link_SuspendM);
        linkIntf.BIND("DataInLo", Link_DataInLo);
        linkIntf.BIND("DataInHi", Link_DataInHi);
        linkIntf.BIND("TXValid", Link_TXValid);
        linkIntf.BIND("TXValidH", Link_TXValidH);
        linkIntf.BIND("TXReady", Link_TXReady);
        linkIntf.BIND("DataOutLo", Link_DataOutLo);
        linkIntf.BIND("DataOutHi", Link_DataOutHi);
        linkIntf.BIND("RXValid", Link_RXValid);
        linkIntf.BIND("RXValidH", Link_RXValidH);
        linkIntf.BIND("RXActive", Link_RXActive);
        linkIntf.BIND("RXError", Link_RXError);
        linkIntf.BIND("XcvrSelect", Link_XcvrSelect);
        linkIntf.BIND("TermSelect", Link_TermSelect);
        linkIntf.BIND("OpMode", Link_OpMode);
        linkIntf.BIND("LineState", Link_LineState);

        UsbxUtmiPhyIntf phyIntf(NULL);
        phyIntf.setSpeedFS(UsbxTrue, 0);
        phyIntf.BIND("Reset", Phy_Reset);
        phyIntf.BIND("DataBus16_8", Phy_DataBus16_8);
        phyIntf.BIND("SuspendM", Phy_SuspendM);
        phyIntf.BIND("DataInLo", Phy_DataInLo);
        phyIntf.BIND("DataInHi", Phy_DataInHi);
        phyIntf.BIND("TXValid", Phy_TXValid);
        phyIntf.BIND("TXValidH", Phy_TXValidH);
        phyIntf.BIND("TXReady", Phy_TXReady);
        phyIntf.BIND("DataOutLo", Phy_DataOutLo);
        phyIntf.BIND("DataOutHi", Phy_DataOutHi);
        phyIntf.BIND("RXValid", Phy_RXValid);
        phyIntf.BIND("RXValidH", Phy_RXValidH);
        phyIntf.BIND("RXActive", Phy_RXActive);
        phyIntf.BIND("RXError", Phy_RXError);
        phyIntf.BIND("XcvrSelect", Phy_XcvrSelect);
        phyIntf.BIND("TermSelect", Phy_TermSelect);
        phyIntf.BIND("OpMode", Phy_OpMode);
        phyIntf.BIND("LineState", Phy_LineState);

        CarbonTime t = 0;
#define RUN                             \
        {                               \
            linkIntf.refreshInputs(t);  \
            phyIntf.refreshInputs(t);   \
            linkIntf.evaluate(t);       \
            phyIntf.evaluate(t);        \
            linkIntf.refreshOutputs(t); \
            phyIntf.refreshOutputs(t);  \
            t += 10;                    \
        }

        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxAckPacket txPacket;
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxNakPacket txPacket;
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[30];
            for (int i = 0; i < 30; i++)
                byteArray[i] = 0xFF;
            UsbxData0Packet txPacket(30, byteArray);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[30];
            for (int i = 0; i < 30; i++)
                byteArray[i] = 0xFF;
            UsbxData0Packet txPacket(30, byteArray);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
    }
    // test 3.4
    {
        printf("---- test 3.4 ----\n");
#ifdef _VCD
        UsbxPin::enableVcdDump("test-3.4.vcd");
#endif
        UsbxUtmiLinkIntf linkIntf(NULL, 16);
        linkIntf.setSpeedFS(UsbxTrue, 0);
        linkIntf.BIND("Reset", Link_Reset);
        linkIntf.BIND("DataBus16_8", Link_DataBus16_8);
        linkIntf.BIND("SuspendM", Link_SuspendM);
        linkIntf.BIND("DataInLo", Link_DataInLo);
        linkIntf.BIND("DataInHi", Link_DataInHi);
        linkIntf.BIND("TXValid", Link_TXValid);
        linkIntf.BIND("TXValidH", Link_TXValidH);
        linkIntf.BIND("TXReady", Link_TXReady);
        linkIntf.BIND("DataOutLo", Link_DataOutLo);
        linkIntf.BIND("DataOutHi", Link_DataOutHi);
        linkIntf.BIND("RXValid", Link_RXValid);
        linkIntf.BIND("RXValidH", Link_RXValidH);
        linkIntf.BIND("RXActive", Link_RXActive);
        linkIntf.BIND("RXError", Link_RXError);
        linkIntf.BIND("XcvrSelect", Link_XcvrSelect);
        linkIntf.BIND("TermSelect", Link_TermSelect);
        linkIntf.BIND("OpMode", Link_OpMode);
        linkIntf.BIND("LineState", Link_LineState);

        UsbxUtmiPhyIntf phyIntf(NULL, 16);
        phyIntf.setSpeedFS(UsbxTrue, 0);
        phyIntf.BIND("Reset", Phy_Reset);
        phyIntf.BIND("DataBus16_8", Phy_DataBus16_8);
        phyIntf.BIND("SuspendM", Phy_SuspendM);
        phyIntf.BIND("DataInLo", Phy_DataInLo);
        phyIntf.BIND("DataInHi", Phy_DataInHi);
        phyIntf.BIND("TXValid", Phy_TXValid);
        phyIntf.BIND("TXValidH", Phy_TXValidH);
        phyIntf.BIND("TXReady", Phy_TXReady);
        phyIntf.BIND("DataOutLo", Phy_DataOutLo);
        phyIntf.BIND("DataOutHi", Phy_DataOutHi);
        phyIntf.BIND("RXValid", Phy_RXValid);
        phyIntf.BIND("RXValidH", Phy_RXValidH);
        phyIntf.BIND("RXActive", Phy_RXActive);
        phyIntf.BIND("RXError", Phy_RXError);
        phyIntf.BIND("XcvrSelect", Phy_XcvrSelect);
        phyIntf.BIND("TermSelect", Phy_TermSelect);
        phyIntf.BIND("OpMode", Phy_OpMode);
        phyIntf.BIND("LineState", Phy_LineState);

        CarbonTime t = 0;
#define RUN                             \
        {                               \
            linkIntf.refreshInputs(t);  \
            phyIntf.refreshInputs(t);   \
            linkIntf.evaluate(t);       \
            phyIntf.evaluate(t);        \
            linkIntf.refreshOutputs(t); \
            phyIntf.refreshOutputs(t);  \
            t += 10;                    \
        }

        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxAckPacket txPacket;
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxNakPacket txPacket;
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[50];
            for (int i = 0; i < 50; i++)
                byteArray[i] = 0xFE;
            UsbxData0Packet txPacket(50, byteArray);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[50];
            for (int i = 0; i < 50; i++)
                byteArray[i] = 0xFE;
            UsbxData0Packet txPacket(50, byteArray);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
    }
    // test 4.1
    {
        printf("---- test 4.1 ----\n");
#ifdef _VCD
        UsbxPin::enableVcdDump("test-4.1.vcd");
#endif
        UsbxUtmiLinkIntf linkIntf(NULL);
        linkIntf.setSpeedFS(UsbxFalse, 0);
        linkIntf.BIND("Reset", Link_Reset);
        linkIntf.BIND("DataBus16_8", Link_DataBus16_8);
        linkIntf.BIND("SuspendM", Link_SuspendM);
        linkIntf.BIND("DataInLo", Link_DataInLo);
        linkIntf.BIND("DataInHi", Link_DataInHi);
        linkIntf.BIND("TXValid", Link_TXValid);
        linkIntf.BIND("TXValidH", Link_TXValidH);
        linkIntf.BIND("TXReady", Link_TXReady);
        linkIntf.BIND("DataOutLo", Link_DataOutLo);
        linkIntf.BIND("DataOutHi", Link_DataOutHi);
        linkIntf.BIND("RXValid", Link_RXValid);
        linkIntf.BIND("RXValidH", Link_RXValidH);
        linkIntf.BIND("RXActive", Link_RXActive);
        linkIntf.BIND("RXError", Link_RXError);
        linkIntf.BIND("XcvrSelect", Link_XcvrSelect);
        linkIntf.BIND("TermSelect", Link_TermSelect);
        linkIntf.BIND("OpMode", Link_OpMode);
        linkIntf.BIND("LineState", Link_LineState);

        UsbxUtmiPhyIntf phyIntf(NULL);
        phyIntf.setSpeedFS(UsbxFalse, 0);
        phyIntf.BIND("Reset", Phy_Reset);
        phyIntf.BIND("DataBus16_8", Phy_DataBus16_8);
        phyIntf.BIND("SuspendM", Phy_SuspendM);
        phyIntf.BIND("DataInLo", Phy_DataInLo);
        phyIntf.BIND("DataInHi", Phy_DataInHi);
        phyIntf.BIND("TXValid", Phy_TXValid);
        phyIntf.BIND("TXValidH", Phy_TXValidH);
        phyIntf.BIND("TXReady", Phy_TXReady);
        phyIntf.BIND("DataOutLo", Phy_DataOutLo);
        phyIntf.BIND("DataOutHi", Phy_DataOutHi);
        phyIntf.BIND("RXValid", Phy_RXValid);
        phyIntf.BIND("RXValidH", Phy_RXValidH);
        phyIntf.BIND("RXActive", Phy_RXActive);
        phyIntf.BIND("RXError", Phy_RXError);
        phyIntf.BIND("XcvrSelect", Phy_XcvrSelect);
        phyIntf.BIND("TermSelect", Phy_TermSelect);
        phyIntf.BIND("OpMode", Phy_OpMode);
        phyIntf.BIND("LineState", Phy_LineState);

        CarbonTime t = 0;
#define RUN                             \
        {                               \
            linkIntf.refreshInputs(t);  \
            phyIntf.refreshInputs(t);   \
            linkIntf.evaluate(t);       \
            phyIntf.evaluate(t);        \
            linkIntf.refreshOutputs(t); \
            phyIntf.refreshOutputs(t);  \
            t += 10;                    \
        }

        // Set the UTM in HS mode
        linkIntf.getPin("XcvrSelect")->write(0, t);

        // Set opMode to mode 2, i.e., No Bit Stuff & NRZI Encoding
        linkIntf.getPin("OpMode")->write(2, t);

        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxAckPacket txPacket;
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxNakPacket txPacket;
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[30];
            for (int i = 0; i < 30; i++)
                byteArray[i] = 0xFF;
            UsbxData0Packet txPacket(30, byteArray);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[30];
            for (int i = 0; i < 30; i++)
                byteArray[i] = 0xFF;
            UsbxData0Packet txPacket(30, byteArray);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
    }
    // test 4.2
    {
        printf("---- test 4.2 ----\n");
#ifdef _VCD
        UsbxPin::enableVcdDump("test-4.2.vcd");
#endif
        UsbxUtmiLinkIntf linkIntf(NULL, 16);
        linkIntf.setSpeedFS(UsbxFalse, 0);
        linkIntf.BIND("Reset", Link_Reset);
        linkIntf.BIND("DataBus16_8", Link_DataBus16_8);
        linkIntf.BIND("SuspendM", Link_SuspendM);
        linkIntf.BIND("DataInLo", Link_DataInLo);
        linkIntf.BIND("DataInHi", Link_DataInHi);
        linkIntf.BIND("TXValid", Link_TXValid);
        linkIntf.BIND("TXValidH", Link_TXValidH);
        linkIntf.BIND("TXReady", Link_TXReady);
        linkIntf.BIND("DataOutLo", Link_DataOutLo);
        linkIntf.BIND("DataOutHi", Link_DataOutHi);
        linkIntf.BIND("RXValid", Link_RXValid);
        linkIntf.BIND("RXValidH", Link_RXValidH);
        linkIntf.BIND("RXActive", Link_RXActive);
        linkIntf.BIND("RXError", Link_RXError);
        linkIntf.BIND("XcvrSelect", Link_XcvrSelect);
        linkIntf.BIND("TermSelect", Link_TermSelect);
        linkIntf.BIND("OpMode", Link_OpMode);
        linkIntf.BIND("LineState", Link_LineState);

        UsbxUtmiPhyIntf phyIntf(NULL, 16);
        phyIntf.setSpeedFS(UsbxFalse, 0);
        phyIntf.BIND("Reset", Phy_Reset);
        phyIntf.BIND("DataBus16_8", Phy_DataBus16_8);
        phyIntf.BIND("SuspendM", Phy_SuspendM);
        phyIntf.BIND("DataInLo", Phy_DataInLo);
        phyIntf.BIND("DataInHi", Phy_DataInHi);
        phyIntf.BIND("TXValid", Phy_TXValid);
        phyIntf.BIND("TXValidH", Phy_TXValidH);
        phyIntf.BIND("TXReady", Phy_TXReady);
        phyIntf.BIND("DataOutLo", Phy_DataOutLo);
        phyIntf.BIND("DataOutHi", Phy_DataOutHi);
        phyIntf.BIND("RXValid", Phy_RXValid);
        phyIntf.BIND("RXValidH", Phy_RXValidH);
        phyIntf.BIND("RXActive", Phy_RXActive);
        phyIntf.BIND("RXError", Phy_RXError);
        phyIntf.BIND("XcvrSelect", Phy_XcvrSelect);
        phyIntf.BIND("TermSelect", Phy_TermSelect);
        phyIntf.BIND("OpMode", Phy_OpMode);
        phyIntf.BIND("LineState", Phy_LineState);

        CarbonTime t = 0;
#define RUN                             \
        {                               \
            linkIntf.refreshInputs(t);  \
            phyIntf.refreshInputs(t);   \
            linkIntf.evaluate(t);       \
            phyIntf.evaluate(t);        \
            linkIntf.refreshOutputs(t); \
            phyIntf.refreshOutputs(t);  \
            t += 10;                    \
        }

        // Set the UTM in HS mode
        linkIntf.getPin("XcvrSelect")->write(0, t);

        // Set opMode to mode 2, i.e., No Bit Stuff & NRZI Encoding
        linkIntf.getPin("OpMode")->write(2, t);

        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxAckPacket txPacket;
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxNakPacket txPacket;
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[50];
            for (int i = 0; i < 50; i++)
                byteArray[i] = 0xFE;
            UsbxData0Packet txPacket(50, byteArray);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[50];
            for (int i = 0; i < 50; i++)
                byteArray[i] = 0xFE;
            UsbxData0Packet txPacket(50, byteArray);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
    }
    // test 4.3
    {
        printf("---- test 4.3 ----\n");
#ifdef _VCD
        UsbxPin::enableVcdDump("test-4.3.vcd");
#endif
        UsbxUtmiLinkIntf linkIntf(NULL);
        linkIntf.setSpeedFS(UsbxTrue, 0);
        linkIntf.BIND("Reset", Link_Reset);
        linkIntf.BIND("DataBus16_8", Link_DataBus16_8);
        linkIntf.BIND("SuspendM", Link_SuspendM);
        linkIntf.BIND("DataInLo", Link_DataInLo);
        linkIntf.BIND("DataInHi", Link_DataInHi);
        linkIntf.BIND("TXValid", Link_TXValid);
        linkIntf.BIND("TXValidH", Link_TXValidH);
        linkIntf.BIND("TXReady", Link_TXReady);
        linkIntf.BIND("DataOutLo", Link_DataOutLo);
        linkIntf.BIND("DataOutHi", Link_DataOutHi);
        linkIntf.BIND("RXValid", Link_RXValid);
        linkIntf.BIND("RXValidH", Link_RXValidH);
        linkIntf.BIND("RXActive", Link_RXActive);
        linkIntf.BIND("RXError", Link_RXError);
        linkIntf.BIND("XcvrSelect", Link_XcvrSelect);
        linkIntf.BIND("TermSelect", Link_TermSelect);
        linkIntf.BIND("OpMode", Link_OpMode);
        linkIntf.BIND("LineState", Link_LineState);

        UsbxUtmiPhyIntf phyIntf(NULL);
        phyIntf.setSpeedFS(UsbxTrue, 0);
        phyIntf.BIND("Reset", Phy_Reset);
        phyIntf.BIND("DataBus16_8", Phy_DataBus16_8);
        phyIntf.BIND("SuspendM", Phy_SuspendM);
        phyIntf.BIND("DataInLo", Phy_DataInLo);
        phyIntf.BIND("DataInHi", Phy_DataInHi);
        phyIntf.BIND("TXValid", Phy_TXValid);
        phyIntf.BIND("TXValidH", Phy_TXValidH);
        phyIntf.BIND("TXReady", Phy_TXReady);
        phyIntf.BIND("DataOutLo", Phy_DataOutLo);
        phyIntf.BIND("DataOutHi", Phy_DataOutHi);
        phyIntf.BIND("RXValid", Phy_RXValid);
        phyIntf.BIND("RXValidH", Phy_RXValidH);
        phyIntf.BIND("RXActive", Phy_RXActive);
        phyIntf.BIND("RXError", Phy_RXError);
        phyIntf.BIND("XcvrSelect", Phy_XcvrSelect);
        phyIntf.BIND("TermSelect", Phy_TermSelect);
        phyIntf.BIND("OpMode", Phy_OpMode);
        phyIntf.BIND("LineState", Phy_LineState);

        CarbonTime t = 0;
#define RUN                             \
        {                               \
            linkIntf.refreshInputs(t);  \
            phyIntf.refreshInputs(t);   \
            linkIntf.evaluate(t);       \
            phyIntf.evaluate(t);        \
            linkIntf.refreshOutputs(t); \
            phyIntf.refreshOutputs(t);  \
            t += 10;                    \
        }

        // Set opMode to mode 2, i.e., No Bit Stuff & NRZI Encoding
        linkIntf.getPin("OpMode")->write(2, t);

        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxAckPacket txPacket;
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxNakPacket txPacket;
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[1];
            for (int i = 0; i < 1; i++)
                byteArray[i] = 0xFF;
            UsbxData0Packet txPacket(1, byteArray);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[30];
            for (int i = 0; i < 30; i++)
                byteArray[i] = 0xFF;
            UsbxData0Packet txPacket(30, byteArray);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
    }
    // test 4.4
    {
        printf("---- test 4.4 ----\n");
#ifdef _VCD
        UsbxPin::enableVcdDump("test-4.4.vcd");
#endif
        UsbxUtmiLinkIntf linkIntf(NULL, 16);
        linkIntf.setSpeedFS(UsbxTrue, 0);
        linkIntf.BIND("Reset", Link_Reset);
        linkIntf.BIND("DataBus16_8", Link_DataBus16_8);
        linkIntf.BIND("SuspendM", Link_SuspendM);
        linkIntf.BIND("DataInLo", Link_DataInLo);
        linkIntf.BIND("DataInHi", Link_DataInHi);
        linkIntf.BIND("TXValid", Link_TXValid);
        linkIntf.BIND("TXValidH", Link_TXValidH);
        linkIntf.BIND("TXReady", Link_TXReady);
        linkIntf.BIND("DataOutLo", Link_DataOutLo);
        linkIntf.BIND("DataOutHi", Link_DataOutHi);
        linkIntf.BIND("RXValid", Link_RXValid);
        linkIntf.BIND("RXValidH", Link_RXValidH);
        linkIntf.BIND("RXActive", Link_RXActive);
        linkIntf.BIND("RXError", Link_RXError);
        linkIntf.BIND("XcvrSelect", Link_XcvrSelect);
        linkIntf.BIND("TermSelect", Link_TermSelect);
        linkIntf.BIND("OpMode", Link_OpMode);
        linkIntf.BIND("LineState", Link_LineState);

        UsbxUtmiPhyIntf phyIntf(NULL, 16);
        phyIntf.setSpeedFS(UsbxTrue, 0);
        phyIntf.BIND("Reset", Phy_Reset);
        phyIntf.BIND("DataBus16_8", Phy_DataBus16_8);
        phyIntf.BIND("SuspendM", Phy_SuspendM);
        phyIntf.BIND("DataInLo", Phy_DataInLo);
        phyIntf.BIND("DataInHi", Phy_DataInHi);
        phyIntf.BIND("TXValid", Phy_TXValid);
        phyIntf.BIND("TXValidH", Phy_TXValidH);
        phyIntf.BIND("TXReady", Phy_TXReady);
        phyIntf.BIND("DataOutLo", Phy_DataOutLo);
        phyIntf.BIND("DataOutHi", Phy_DataOutHi);
        phyIntf.BIND("RXValid", Phy_RXValid);
        phyIntf.BIND("RXValidH", Phy_RXValidH);
        phyIntf.BIND("RXActive", Phy_RXActive);
        phyIntf.BIND("RXError", Phy_RXError);
        phyIntf.BIND("XcvrSelect", Phy_XcvrSelect);
        phyIntf.BIND("TermSelect", Phy_TermSelect);
        phyIntf.BIND("OpMode", Phy_OpMode);
        phyIntf.BIND("LineState", Phy_LineState);

        CarbonTime t = 0;
#define RUN                             \
        {                               \
            linkIntf.refreshInputs(t);  \
            phyIntf.refreshInputs(t);   \
            linkIntf.evaluate(t);       \
            phyIntf.evaluate(t);        \
            linkIntf.refreshOutputs(t); \
            phyIntf.refreshOutputs(t);  \
            t += 10;                    \
        }

        // Set opMode to mode 2, i.e., No Bit Stuff & NRZI Encoding
        linkIntf.getPin("OpMode")->write(2, t);

        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxAckPacket txPacket;
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxNakPacket txPacket;
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[50];
            for (int i = 0; i < 50; i++)
                byteArray[i] = 0xFE;
            UsbxData0Packet txPacket(50, byteArray);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxByte byteArray[50];
            for (int i = 0; i < 50; i++)
                byteArray[i] = 0xFE;
            UsbxData0Packet txPacket(50, byteArray);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
    }

#else // For ULPI
    {
        printf("---- test 3.1 ----\n");
#ifdef _VCD
        UsbxPin::enableVcdDump("test-3.1.vcd");
#endif
        UsbxUlpiLinkIntf linkIntf(NULL);
#ifdef _HS
        linkIntf.setSpeedFS(UsbxFalse, 0);
#else
        linkIntf.setSpeedFS(UsbxTrue, 0);
#endif
        linkIntf.BIND("dir", Link_dir);
        linkIntf.BIND("nxt", Link_nxt);
        linkIntf.BIND("stp", Link_stp);
        linkIntf.BIND("data", Link_data);

        UsbxUlpiPhyIntf phyIntf(NULL);
#ifdef _HS
        phyIntf.setSpeedFS(UsbxFalse, 0);
#else
        phyIntf.setSpeedFS(UsbxTrue, 0);
#endif
        phyIntf.BIND("dir", Phy_dir);
        phyIntf.BIND("nxt", Phy_nxt);
        phyIntf.BIND("stp", Phy_stp);
        phyIntf.BIND("data", Phy_data);

        CarbonTime t = 0;
        #define RUN                             \
        {                               \
            linkIntf.refreshInputs(t);  \
            phyIntf.refreshInputs(t);   \
            linkIntf.evaluate(t);       \
            phyIntf.evaluate(t);        \
            linkIntf.refreshOutputs(t); \
            phyIntf.refreshOutputs(t);  \
            t += 10;                    \
        }

        for (int i = 0; i < 10; i++) RUN;

#if 1
        for (int i = 0; i < 4; i++) RUN;
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 20; i++) RUN;

#endif
#if 1
        for (int i = 0; i < 100; i++) RUN;
        // Send the same packet back to link
        {
            UsbxOutPacket txPacket(0x3A, 0xA);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;

#endif

#if 1
        for (int i = 0; i < 100; i++) RUN;
        // Send In Packet
        {
            UsbxInPacket txPacket(0x70, 0x4);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }

        for (int i = 0; i < 20; i++) RUN;
#endif

#if 1
        // Send the same packet back to Link
        {
            UsbxInPacket txPacket(0x70, 0x4);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;

            receivePacket->print();
            delete receivePacket;
        }
#endif 

#if 1
        for (int i = 0; i < 4; i++) RUN;
        // Send a NAK Packet
        {
            UsbxNakPacket txPacket;
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
#endif 
#if 1
        // Send the same packet back to Link
        {
            UsbxNakPacket txPacket;
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }

        for (int i = 0; i < 4; i++) RUN;
#endif
#if 1
        // Send large data packet
        {
            UsbxByte byteArray[30];
            for (int i = 0; i < 30; i++)
                byteArray[i] = 0xFF;
            UsbxData0Packet txPacket(30, byteArray);
            linkIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!linkIntf.transmitComplete() ||
                    (receivePacket = phyIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }
        for (int i = 0; i < 4; i++) RUN;
#endif

#if 1
        {
            UsbxByte byteArray[30];
            for (int i = 0; i < 30; i++)
                byteArray[i] = 0xFF;
            UsbxData0Packet txPacket(30, byteArray);
            phyIntf.transmitPacket(&txPacket);
            UsbxPacket *receivePacket = NULL;
            while (!phyIntf.transmitComplete() ||
                    (receivePacket = linkIntf.receivePacket()) == NULL)
                RUN;
            receivePacket->print();
            delete receivePacket;
        }

#endif
        for (int i = 0; i < 40; i++) RUN;
   }
#endif
    return 0;
}
