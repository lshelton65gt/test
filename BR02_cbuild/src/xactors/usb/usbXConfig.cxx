/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    usbXConfig.cpp
    Purpose: This file contains the definition of the base class of
             usb transaction.
*/

#include "usbXConfig.h"
#include "usbXTrans.h"
#include "xactors/usb/carbonXUsbDescriptors.h"

const CarbonUInt32 CarbonXUsbConfigClass::_maxTimingFactor = 600;

CarbonXUsbConfigClass::CarbonXUsbConfigClass():
    _speed(CarbonXUsbSpeed_FS), _intfType(CarbonXUsbInterface_UTMI),
    _intfSide(CarbonXUsbIntfSide_PHY), _utmiWidth(CarbonXUsbUtmiDataBusWidth_8),
    _utmiPinLevel(CarbonXUsbUtmiPinLevel_0), 
    _deviceDesc(NULL), _otgDesc(NULL), _configDescList(NULL),
    _timingFactor(_maxTimingFactor),
    _miniAPhyPlug(UsbxFalse),
    _srpDetectionSupport(CarbonXUsbOtgSrpDetection_VbusPulsing),
    _isHub(UsbxFalse)
{
    _configDescList = new UsbxQueue<UsbxByte>;
}

CarbonXUsbConfigClass::CarbonXUsbConfigClass(
        const CarbonXUsbConfigClass &config):
    _speed(config._speed), _intfType(config._intfType),
    _intfSide(config._intfSide), _utmiWidth(config._utmiWidth),
    _utmiPinLevel(config._utmiPinLevel),
    _deviceDesc(copyDescriptor(config._deviceDesc)),
    _otgDesc(copyDescriptor(config._otgDesc)),
    _configDescList(new UsbxQueue<UsbxByte>),
    _timingFactor(config._timingFactor),
    _miniAPhyPlug(config._miniAPhyPlug),
    _srpDetectionSupport(config._srpDetectionSupport),
    _isHub(config._isHub)
{
    if (_deviceDesc == NULL || _configDescList == NULL)
        return;
    const UsbxQueue<UsbxByte> *configDescList =
        config.getConfigDescriptorList();
    UsbxQueue<UsbxByte>::UsbxQueueIterator iter;
    for (iter = configDescList->begin(); iter != configDescList->end(); iter++)
        _configDescList->push(copyDescriptor(*iter));
}

CarbonXUsbConfigClass::~CarbonXUsbConfigClass()
{
    DELETE(_deviceDesc);
    DELETE(_otgDesc);
    if (_configDescList)
    {
        UsbxByte *configDesc = NULL;
        while ((configDesc = _configDescList->pop()))
            DELETE(configDesc);
        delete _configDescList;
    }
}

// Copy the descriptor
void CarbonXUsbConfigClass::setDeviceDescriptor(const UsbxByte *descriptor)
{
    _deviceDesc = copyDescriptor(descriptor);
}

void CarbonXUsbConfigClass::setOtgDescriptor(const UsbxByte *descriptor)
{
    _otgDesc = copyDescriptor(descriptor);
}

void CarbonXUsbConfigClass::addConfigDescriptor(const UsbxByte *descriptor)
{
    UsbxByte *configDesc = copyDescriptor(descriptor);
    if (configDesc != NULL)
        _configDescList->push(configDesc);
}

UsbxByte *CarbonXUsbConfigClass::copyDescriptor(const UsbxByte *descriptor,
        UInt descriptorSize)
{
    if (descriptor == NULL)
        return NULL;
    UInt size;
    if (descriptorSize > 0)
        size = descriptorSize;
    else if (CarbonXUsbDescriptorType(descriptor[1]) ==
            CarbonXUsbDescriptor_CONFIGURATION)
        size = descriptor[2];
    else
        size = descriptor[0];
    UsbxByte *bytes = CarbonXUsbTransClass::copyData(size, descriptor);
    return bytes;
}

void CarbonXUsbConfigClass::setTimingFactor(CarbonUInt32 factor)
{
    if (factor < 1)
        _timingFactor = 1;
    else if (factor > _maxTimingFactor)
        _timingFactor = _maxTimingFactor;
    else
        _timingFactor = factor;
}

