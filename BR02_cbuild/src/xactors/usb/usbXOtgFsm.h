/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_OTGFSM_H
#define USBX_OTGFSM_H

#include "usbXFsm.h"
#include "usbXHostFsm.h"
#include "usbXDeviceFsm.h"
#include "usbXtor.h"

/*!
 * \file usbXOtgFsm.h
 * \brief Header file for OTG FSM (Finite State Machine).
 *
 * This file provides the class definition for OTG FSM
 */

/*!
 * \brief Class for OTG FSM.
 */
class UsbxOtgFsm: public UsbxFsm
{
public: CARBONMEM_OVERRIDES

private:

    UsbxOtgDeviceXtor *_xtor;              //!< Transactor reference

    UsbxHostFsm *_hostFsm;                 //!< Host FSM
    UsbxDeviceFsm *_deviceFsm;             //!< Device FSM

    //! Otg FSM states
    enum UsbxOtgState {
        UsbxOtgState_reset,
        // A Device states
        UsbxOtgState_a_idle,
        UsbxOtgState_a_wait_vrise,
        UsbxOtgState_a_wait_vfall,
        UsbxOtgState_a_host, // Includes a_wait_bcon also
        UsbxOtgState_a_suspend,
        UsbxOtgState_a_peripheral,
        // B Device states
        UsbxOtgState_b_idle,
        UsbxOtgState_b_srp_init,
        UsbxOtgState_b_peripheral,
        UsbxOtgState_b_host // Includes b_wait_acon
    };

    UsbxOtgState _state;                   //!< FSM state
    UInt _clockCountForWait;               //!< Wait timer for interface

    static const UInt _clocksForSrp;       // 100 ms
    static const UInt _clocksForHnp;       // 200 ms
    static const UInt _clocksForHnpStop;   // 3 - 200 ms, take 3 ms

public:

    // Constructor and destructor
    UsbxOtgFsm(UsbxOtgDeviceXtor *xtor,
            UsbxHostFsm *hostFsm, UsbxDeviceFsm *deviceFsm);
    ~UsbxOtgFsm();

    //! Session start and stop
    UsbxBool startSession();
    UsbxBool stopSession();

    //! Reset for detach
    void resetForDetach();

    // Evaluation
    UsbxBool evaluate(CarbonTime currentTime);

    void print(CarbonTime currentTime) const;
};

#endif
