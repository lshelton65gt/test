/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    usbXScheduler.cpp
    Purpose: This file contains the definition of the base class of
             usb transaction.
*/

#include "usbXScheduler.h"
#include "usbXPipe.h"
#include "usbXtor.h"

// #define USE_VARIANCE

// #define DEBUG
// #define DEBUG_STATE
// #define IGNORE_BIT_STUFF_OVERLOADING

// From table at usbXUtmiIntf.cpp
const UInt UsbxScheduler::_clocksPerFSBit         = 5;
const UInt UsbxScheduler::_maxInterPacketDelayFS  = 32;
const UInt UsbxScheduler::_maxInterPacketDelayHS  = 24;
const UInt UsbxScheduler::_maxBusTurnAroundTimeFS = 90;
const UInt UsbxScheduler::_maxBusTurnAroundTimeHS = 102;

UsbxScheduler::UsbxScheduler(UsbXtor *xtor,
        CarbonXUsbUtmiDataBusWidthType utmiWidth):
    _xtor(xtor), _speedFactor(utmiWidth / 8), _frameNumber(0),
    _frameClockCount(0), _stateClockCount(0), _remainingNpFrameClocks(0),
    _nextNonPeriodicPipeIndex(0), _nextPeriodicPipeIndex(0),
    _repeatCountForHsPeriodic(0), _state(UsbxSchedulerState_SOF),
    _schedulerStarted(UsbxFalse), _busReclaimedNPState(UsbxFalse),
    _sofPacket(NULL)
{
    // Initialize the allocated bandwidth for each frame to 0
    for (UInt i = 0; i < _maxFrameIndex; i++)
        _periodicAllocTable[i] = 0;
}

void UsbxScheduler::startScheduler()
{
    _schedulerStarted = UsbxTrue;
    // Reset all members
    _frameNumber = _frameClockCount = _stateClockCount =
        _remainingNpFrameClocks = _nextNonPeriodicPipeIndex =
        _nextPeriodicPipeIndex = _repeatCountForHsPeriodic = 0;
    _state = UsbxSchedulerState_SOF;
    _busReclaimedNPState = UsbxFalse;
    if (_sofPacket)
        delete _sofPacket;
    _sofPacket = NULL;
}

void UsbxScheduler::stopScheduler()
{
    _schedulerStarted = UsbxFalse;
    _state = UsbxSchedulerState_SOF;
}

void UsbxScheduler::evaluate(CarbonTime currentTime)
{
    // Check if scheduler has been started before proceeding
    if (!_schedulerStarted)
        return;

    // Get the interface
    UsbxIntf *interface = _xtor->getInterface();
    ASSERT(interface, _xtor);

#ifdef DEBUG_STATE
    UsbxSchedulerState lastState = _state;
#endif

    // Scheduler Finite State Machine
    switch (_state)
    {
        case UsbxSchedulerState_SOF:
        {
            // Check for no ongoing bus activity
            ASSERT(interface->transmitComplete() &&
                    interface->receivePacket() == NULL &&
                    !interface->receiving(currentTime), _xtor);

            // Issue SOF packet
            _sofPacket =
                new UsbxSofPacket(_frameNumber & 0x7FF); // 11-bit Frame No.
            if (_sofPacket == NULL)
            {
                UsbXtor::error(_xtor, CarbonXUsbError_NoMemory,
                    "Can't allocate memory for SOF packet");
                return;
            }
            interface->transmitPacket(_sofPacket);
#ifdef DEBUG
            printf("DEBUG[%d]: SCHEDULER: SOF %d issued.\n",
                    static_cast<UInt>(currentTime),
                    static_cast<UInt>(_frameNumber));
#endif

            // Change state
            _state = UsbxSchedulerState_Wait;
            break;
        }
        case UsbxSchedulerState_Wait:
        {
            // Check if SOF packet transmission is still going on
            if (!interface->transmitComplete())
                break;
#ifdef DEBUG_STATE
            printf("DEBUG[%d]: SCHEDULER: SOF completed.\n",
                    static_cast<UInt>(currentTime));
#endif
            // Free up SOF Packet
            delete _sofPacket;
            _sofPacket = NULL;

            // Initialize NP and P state pipe indices for each frame
            _nextNonPeriodicPipeIndex = _nextPeriodicPipeIndex = 0;

            // Find remaining clocks in frame after SOF
            UInt totalFrameClocks = getClocksPerFrame() - _frameClockCount -
                (_xtor->isFS() ? _maxInterPacketDelayFS :
                        _maxInterPacketDelayHS) / _speedFactor;

            // Find NP and P state times
            _remainingNpFrameClocks = totalFrameClocks / 10; // 10%

            // Set counter to count non periodic slice
            _stateClockCount = 0;

            // Change State
            _state = UsbxSchedulerState_NonPeriodic;
            break;
        }
        case UsbxSchedulerState_NonPeriodic:
        {
            if (_stateClockCount >= _remainingNpFrameClocks &&
                    !_busReclaimedNPState)
            {
                // Change State
                _state = UsbxSchedulerState_Periodic;
                _stateClockCount = 0;
            }
            break;
        }
        case UsbxSchedulerState_Periodic:
        {
            _busReclaimedNPState = UsbxTrue;
            break;
        }
        default: ASSERT(0, _xtor);
    }

#ifdef DEBUG_STATE
    if (_state != lastState)
        printf("DEBUG[%d]: SCHEDULER: State changed to %d.\n",
                static_cast<UInt>(currentTime), _state);
#endif

    // Update the frame clock counters
    _stateClockCount++;
    _frameClockCount++;
    if (_frameClockCount == getClocksPerFrame())
    {
        _frameClockCount = 0;
        _state = UsbxSchedulerState_SOF;
        _frameNumber = (_frameNumber + 1) & 0x7FF;
    }
    return;
    currentTime = 0; // To remove warning
}

UsbxPipe* UsbxScheduler::schedulePipe(CarbonTime currentTime)
{
    if (_xtor->getPipeCount() == 0)
        return NULL;

    if (_schedulerStarted == UsbxFalse)
        return NULL;

#ifdef DEBUG_STATE
    UsbxSchedulerState lastState = _state;
#endif

    UsbxPipe *pipe = NULL;
    switch (_state)
    {
        case UsbxSchedulerState_SOF:
        case UsbxSchedulerState_Wait:
        {
            _busReclaimedNPState = UsbxFalse;
            break;
        }
        case UsbxSchedulerState_NonPeriodic:
        {
            // Attempt to schedule a control pipe
            if ((pipe = getNextNonPeriodicPipe(CarbonXUsbPipe_CONTROL)))
                break;

            // When no schedulable control pipe exists, attempt for bulk
            if ((pipe = getNextNonPeriodicPipe(CarbonXUsbPipe_BULK)))
                break;

            // When no schedulable non-periodic pipe exists, switch to periodic
            if (!_busReclaimedNPState)
            {
                _state = UsbxSchedulerState_Periodic;
                _stateClockCount = 0;
            }
            break;
        }
        case UsbxSchedulerState_Periodic:
        {
            _busReclaimedNPState = UsbxTrue;

            // Check for a periodic transaction
            if ((pipe = getNextPeriodicPipe()))
                break;

            // When no schedulable periodic transaction exists, switch to NP
            _state = UsbxSchedulerState_NonPeriodic; // Change state
            _stateClockCount = 0;
            _remainingNpFrameClocks = getClocksPerFrame() - _frameClockCount;
            break;
        }
        default: ASSERT(0, _xtor);
    }

#ifdef DEBUG_STATE
    if (_state != lastState)
        printf("DEBUG[%d]: SCHEDULER: State changed to %d.\n",
                static_cast<UInt>(currentTime), _state);
    if (pipe)
    {
        printf("DEBUG[%d]: Pipe scheduled:\n", static_cast<UInt>(currentTime));
        pipe->print();
    }
#endif
    return pipe;
    currentTime = 0; // To remove warning
}

UsbxPipe* UsbxScheduler::getNextNonPeriodicPipe(CarbonXUsbPipeType pipeType)
{
    UsbxPipe * const *pipes = _xtor->getPipes();
    if (pipes == NULL)
        return NULL;

    // If some pipes are destroyed in between
    if (_nextNonPeriodicPipeIndex >= _xtor->getPipeCount())
        _nextNonPeriodicPipeIndex = 0;

    // Iterate over pipe list and search for a schedulable NP transfer
    for (UInt i = 0, pipeIndex = _nextNonPeriodicPipeIndex;
            i < _xtor->getPipeCount(); i++, // loop pipe number times
            pipeIndex = (pipeIndex + 1) % _xtor->getPipeCount())
    {
        // Check if of desired type
        if (pipes[pipeIndex]->getType() != pipeType)
            continue;

        // A candidate pipe found - check for schedulability
        UInt clocksRequiredToTransfer = getClocksForNextTransUnit(
                pipes[pipeIndex]);
        if (clocksRequiredToTransfer > 0 && clocksRequiredToTransfer <=
                _remainingNpFrameClocks - _stateClockCount)
        {
            _nextNonPeriodicPipeIndex = pipeIndex + 1;
            return pipes[pipeIndex];
        }
    }
    return NULL;
}

UsbxPipe* UsbxScheduler::getNextPeriodicPipe()
{
    UsbxPipe * const *pipes = _xtor->getPipes();
    if (pipes == NULL)
        return NULL;

    // Iterate over pipe list and search for a schedulable NP transfer
    for (UInt pipeIndex = _nextPeriodicPipeIndex;
            pipeIndex < _xtor->getPipeCount(); pipeIndex++)
    {
        UsbxPipe *pipe = pipes[pipeIndex];

        // Check if not periodic type
        if (!pipe->isPeriodic())
            continue;

        // If the pipe is not to be scheduled in this frame
        if ((_frameNumber % pipe->getPeriodicInterval()) !=
                pipe->getFrameOffset())
            continue;

        // Update repeat count for HS periodic transfer
        if (!_xtor->isFS())
        {
            if (_repeatCountForHsPeriodic > 0)
                _repeatCountForHsPeriodic--;
            else
                _repeatCountForHsPeriodic = pipe->getRepeatCount();
        }
        if (_xtor->isFS() || _repeatCountForHsPeriodic == 0)
            _nextPeriodicPipeIndex = pipeIndex + 1;

        // Check when no periodic transfer is in pipe
        ASSERT(pipe->getTransQueue(), _xtor);
        if (pipe->getTransQueue()->getCount() == 0)
        {
            // For smaller transaction, do not start next transfer in same frame
            if (!_xtor->isFS() &&
                    pipe->getRepeatCount() > _repeatCountForHsPeriodic)
                return NULL;

            if (pipe->getDirection() == CarbonXUsbPipeDirection_INPUT)
            {
                // For periodic input transfer, create transfer automatically
                UsbxStreamTrans *inputTrans = new UsbxStreamTrans(pipe);
                if (inputTrans == NULL)
                {
                    UsbXtor::error(_xtor, CarbonXUsbError_NoMemory,
                        "Can't allocate memory for periodic input transfer"
                        " from within scheduler automatically");
                    return NULL;
                }
                pipe->getTransQueue()->push(inputTrans);
            }
            else
            {
                UsbXtor::error(_xtor, CarbonXUsbError_NoPeriodicOutTrans,
                    "No transfer is available on periodic output pipe");
                return NULL;
            }
        }

        return pipe;
    }
    return NULL;
}

UInt UsbxScheduler::getClocksPerFrame() const
{
    return (_xtor->isFS() ? 60000 : 7500) / _speedFactor;
}

UInt UsbxScheduler::getClocksForMaxPeriodicTransUnit(const UsbxPipe *pipe) const
{
    if (pipe == NULL && !pipe->isPeriodic())
        return 0;

    // Transaction unit bytes = token packet + data packet + handshake packet
    UInt tokenPacketSize = 3;
    UInt dataPacketSize = pipe->getMaxPayloadSize(); // max. data packet size
    UInt handshakePacketSize = 1;
    UInt transUnitBytes = tokenPacketSize + dataPacketSize +
                                    handshakePacketSize;

    // Bit Stuff Count variables
    UInt maxBitStuffCount = 0, maxBitStuffClockCount = 0;

    UInt clockCount;
    if (pipe->getTransactor()->isFS()) // For FS
    {
#ifndef IGNORE_BIT_STUFF_OVERLOADING
        // Calculate maximum number of bits for bit stuffing
        // For every 6 contiguous ones, 1 bit is added, hence for x
        // bytes, (x * 8) / 6 additional bits are added for bit stuffing
        maxBitStuffCount = (transUnitBytes * 8) / 6;
#endif
        // Add up the sizes with 3 SYNC + 3 EOP (2 SE0) + Max Bit stuffing
        // Overhead + 2 Interpacket delays + 1 Bus Turnaround time
        clockCount = ((transUnitBytes + 3) * 8 + 2 * 3 + maxBitStuffCount) *
            _clocksPerFSBit + 2 * _maxInterPacketDelayFS +
            _maxBusTurnAroundTimeFS;
    }
    else // For HS
    {
#ifndef IGNORE_BIT_STUFF_OVERLOADING
        // Calculate worst case clock count for bit stuffing
        // For every 6 contiguous ones, 1 bit is added, hence for x bytes,
        // ceil[(x * 8) / (8 * 6)] HS clocks are needed for bit stuffing
        // Code below is a clever implementation of ceil in this context
        maxBitStuffClockCount = transUnitBytes / 6 +
            (transUnitBytes % 6 ? 1 : 0);
#endif

        clockCount = transUnitBytes + maxBitStuffClockCount +
            2 * _maxInterPacketDelayHS + _maxBusTurnAroundTimeHS;
    }
    return clockCount / _speedFactor;
}

UInt UsbxScheduler::getClocksForNextTransUnit(const UsbxPipe *pipe) const
{
    if (pipe == NULL)
        return 0;
    UsbxQueue<UsbxTrans> *transQueue = pipe->getTransQueue();
    if (transQueue == NULL || transQueue->getCount() == 0)
        return 0;

    UsbxTrans *trans = transQueue->top();
    UInt transUnitBytes = trans->getNextTransUnitSizeInBytes();

    // Bit Stuff Count variables
    UInt maxBitStuffCount = 0, maxBitStuffClockCount = 0;

    UInt clockCount;
    if (pipe->getTransactor()->isFS()) // For FS
    {
#ifndef IGNORE_BIT_STUFF_OVERLOADING
        // Calculate maximum number of bits for bit stuffing
        // For every 6 contiguous ones, 1 bit is added, hence for x
        // bytes, (x * 8) / 6 additional bits are added for bit stuffing
        maxBitStuffCount = (transUnitBytes * 8) / 6;
#endif
        // Add up the sizes with 3 SYNC + 3 EOP (2 SE0) + Max Bit stuffing
        // overhead + 2 Interpacket delays + 1 Bus Turnaround time
        clockCount = ((transUnitBytes + 3) * 8 + 2 * 3 + maxBitStuffCount) *
            _clocksPerFSBit + 2 * _maxInterPacketDelayFS +
            _maxBusTurnAroundTimeFS;
    }
    else // For HS
    {
        // Calculate worst case clock count for bit stuffing
#ifndef IGNORE_BIT_STUFF_OVERLOADING
        // Calculate worst case clock count for bit stuffing
        // For every 6 contiguous ones, 1 bit is added, hence for x bytes,
        // ceil[(x * 8) / (8 * 6)] HS clocks are needed for bit stuffing
        // Code below is a clever implementation of ceil in this context
        maxBitStuffClockCount = transUnitBytes / 6 +
        (transUnitBytes % 6 ? 1 : 0);
#endif

        clockCount = transUnitBytes + maxBitStuffClockCount +
            2 * _maxInterPacketDelayHS + _maxBusTurnAroundTimeHS;
    }
    return clockCount / _speedFactor;
}

UsbxBool UsbxScheduler::hasRetried(const UsbxPipe *pipe) const
{
    if (pipe == NULL)
        return UsbxFalse;
    UsbxQueue<UsbxTrans> *transQueue = pipe->getTransQueue();
    if (transQueue == NULL || transQueue->getCount() == 0)
        return UsbxFalse;
    UsbxTrans *trans = transQueue->top();
    return trans->hasRetried();
}

// **** Periodic scheduling policy ****

// Checks if a given pipe is schedulable in any frame
// Consider bandwidth allocation conflicts keeping periodicity in consideration
UsbxBool UsbxScheduler::isPeriodicPipeSchedulable(UsbxPipe *pipe)
{
    if (pipe == NULL || !pipe->isPeriodic())
        return UsbxFalse;

    UsbxPipe * const *pipes = _xtor->getPipes();
    ASSERT (pipes, _xtor);

    UsbxBool isSchedulable = UsbxFalse;

    // Get the pipe periodic interval
    UInt periodicInterval = pipe->getPeriodicInterval();

    // Try to schedule this pipe in any frame between current frame number to
    // (periodicInterval - 1) frames. Starting from frameOffset, try to assign
    // a frame offset to pipe. For each frame , find allocation
    // status in that frame. If (current allocation in this frame + bandwidth
    // needed for pipe) exceeds the allowed bandwidth for periodic
    // pipes per frame, proceed to the next frame number. Otherwise, attempt
    // to assign this frame index as frame offset to this pipe after checking
    // bandwidth conflicts in all frames where this pipe will again be repeated

    UInt maxPeriodicClocksPerFrame;
    if (_xtor->isFS())
        maxPeriodicClocksPerFrame = (getClocksPerFrame() * 9) / 10; // FS: 90%
    else
        maxPeriodicClocksPerFrame = (getClocksPerFrame() * 8) / 10; // HS: 80%

    // Best-fit scheduling policy. Find out the frame assignment for which
    // the minimum variance is achieved.

    UInt minVariance = 0;
    UInt bestFrameOffset = 0; // To track best frame offset

    // Start from current frame number when the pipe arrived
    for (UInt i = 0, frameOffset = _frameNumber;
            i < periodicInterval; i++, // loop periodic interval times
            frameOffset = frameOffset + 1)
    {
        // Clocks required should also consider repeat count for HS periodic
        UInt clocksRequiredToTransfer = getClocksForMaxPeriodicTransUnit(pipe) *
            (pipe->getRepeatCount() + 1);

        if (_periodicAllocTable[frameOffset] + clocksRequiredToTransfer
                <= maxPeriodicClocksPerFrame) // Can assign in this frame
        {
            // Check if there are any periodicity conflicts. If yes, do not
            // schedule in this frame, else update the current minimum
            // variance by comparing with the variance obtained by adopting
            // this assignment

            UsbxBool schedulableInFuture = UsbxTrue;
            UInt varianceOfFrame = _periodicAllocTable[frameOffset] +
                            clocksRequiredToTransfer;
            if (frameOffset + periodicInterval <= _maxFrameIndex)
#ifndef USE_VARIANCE
                varianceOfFrame = computeFrameMean(frameOffset, pipe,
                    schedulableInFuture);
#else
                varianceOfFrame = computeFrameVariance(frameOffset, pipe,
                    schedulableInFuture);
#endif
            
            // Update the minimum variance and best frame offset
            // if the pipe is schedulable in every frame where it repeats
            // and if a lesser variance is achieved
            if (schedulableInFuture)
            {
                if (!isSchedulable || minVariance > varianceOfFrame)
                {
                    minVariance = varianceOfFrame;
                    bestFrameOffset = frameOffset;
                }
                isSchedulable = UsbxTrue;
            }
        }
    }

    // Set the frame offset to the current frame index
    if (isSchedulable)
        pipe->setFrameOffset(bestFrameOffset);

    return isSchedulable;
}

// Computes the mean of the distribution of frame bandwidth starting from
// frame index as the frame offset and periodic repetitions. Checks for
// bandwidth allocation conflicts at each location where the pipe repeats
UInt UsbxScheduler::computeFrameMean(UInt frameOffset, UsbxPipe *pipe,
        UsbxBool &schedulableInFuture) const
{
    UInt sum = 0;
    UInt maxPeriodicClocksPerFrame;
    schedulableInFuture = UsbxTrue;

    if (_xtor->isFS())
        maxPeriodicClocksPerFrame = (getClocksPerFrame() * 9) / 10; // FS: 90%
    else
        maxPeriodicClocksPerFrame = (getClocksPerFrame() * 8) / 10; // HS: 80%

    // Clocks required should also consider repeat count for HS periodic
    UInt clocksRequiredToTransfer = getClocksForMaxPeriodicTransUnit(pipe) *
        (pipe->getRepeatCount() + 1);

    // Get the pipe periodic interval
    UInt periodicInterval = pipe->getPeriodicInterval();

    // Number of times this pipe will repeat if started from here. This is
    // required for computing the variance
    ASSERT(periodicInterval > 0, _xtor);
    UInt noOfFrames = (_maxFrameIndex - frameOffset) / periodicInterval;

    // Check for all frames where this pipe may be repeated and compute
    // the variance
    for (UInt j = frameOffset; j < _maxFrameIndex; j += periodicInterval)
    {
        // Check if there are bandwidth allocation conflicts in this frame
        if ((_periodicAllocTable[j] + clocksRequiredToTransfer)
                > maxPeriodicClocksPerFrame)
        {
            schedulableInFuture = UsbxFalse;
            return 0;
        }
        sum += _periodicAllocTable[j] + clocksRequiredToTransfer;
    }

    // Compute the variance using the standard formula
    ASSERT(noOfFrames > 0, _xtor);
    //return ((sumOfSquares - (sum * sum) / noOfFrames) / noOfFrames);
    return sum / noOfFrames;
}

// Computes the variance of the distribution of frame bandwidth starting from
// frame index as the frame offset and periodic repetitions. Checks for
// bandwidth allocation conflicts at each location where the pipe repeats
UInt UsbxScheduler::computeFrameVariance(UInt frameOffset, UsbxPipe *pipe,
        UsbxBool &schedulableInFuture) const
{
    UInt sum = 0;
    UInt sumOfSquares = 0;
    UInt maxPeriodicClocksPerFrame;
    schedulableInFuture = UsbxTrue;

    if (_xtor->isFS())
        maxPeriodicClocksPerFrame = (getClocksPerFrame() * 9) / 10; // FS: 90%
    else
        maxPeriodicClocksPerFrame = (getClocksPerFrame() * 8) / 10; // HS: 80%

    // Clocks required should also consider repeat count for HS periodic
    UInt clocksRequiredToTransfer = getClocksForMaxPeriodicTransUnit(pipe) *
        (pipe->getRepeatCount() + 1);

    // Get the pipe periodic interval
    UInt periodicInterval = pipe->getPeriodicInterval();

    // Number of times this pipe will repeat if started from here. This is
    // required for computing the variance
    ASSERT(periodicInterval > 0, _xtor);
    UInt noOfFrames = (_maxFrameIndex - frameOffset) / periodicInterval;

    // Check for all frames where this pipe may be repeated and compute
    // the variance
    for (UInt j = frameOffset; j < _maxFrameIndex; j += periodicInterval)
    {
        // Check if there are bandwidth allocation conflicts in this frame
        if ((_periodicAllocTable[j] + clocksRequiredToTransfer)
                > maxPeriodicClocksPerFrame)
        {
            schedulableInFuture = UsbxFalse;
            return 0;
        }

        sumOfSquares += (_periodicAllocTable[j] + clocksRequiredToTransfer)
            * (_periodicAllocTable[j] + clocksRequiredToTransfer);
        sum += _periodicAllocTable[j] + clocksRequiredToTransfer;
    }

    // Compute the variance using the standard formula
    ASSERT(noOfFrames > 0, _xtor);
    return ((sumOfSquares - (sum * sum) / noOfFrames) / noOfFrames);
}

UsbxBool UsbxScheduler::updatePeriodicPolicy(UsbxPipe *pipe, UsbxBool isCreate)
{
    ASSERT(pipe, _xtor);

    UInt periodicInterval = pipe->getPeriodicInterval();

    if (isCreate) // Change policy for pipe creation
    {
        if (isPeriodicPipeSchedulable(pipe))
        {
             // Update the frame bandwidth allocation table for each frame
             // where this pipe will repeat, add the allocated bandwidth value
             // considering repeat count for HS periodic
             for (UInt frameIndex = pipe->getFrameOffset();
                     frameIndex < _maxFrameIndex;
                     frameIndex += periodicInterval)
             {
                 // Pipe will repeat in this frame, update allocation status
                 _periodicAllocTable[frameIndex] +=
                     getClocksForMaxPeriodicTransUnit(pipe) *
                     (pipe->getRepeatCount() + 1);
             }
        }
        else
            return UsbxFalse;
    }
    else // Change policy for pipe deletion
    {
        // Update the frameBandwidth allocation table for each frame where this
        // pipe will repeat, subtract the allocated bandwidth value considering
        // repeat count for HS periodic
        for (UInt frameIndex = pipe->getFrameOffset();
                frameIndex < _maxFrameIndex; frameIndex += periodicInterval)
        {
            // Pipe will repeat in this frame, update allocation status
            _periodicAllocTable[frameIndex] -=
                    getClocksForMaxPeriodicTransUnit(pipe) *
                    (pipe->getRepeatCount() + 1);
        }
    }
    return UsbxTrue;
}
