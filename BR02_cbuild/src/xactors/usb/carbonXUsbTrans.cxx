/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    carbonXUsbTrans.cpp
    Purpose: This file provides definition of APIs for transaction object.
*/

#include "usbXPipe.h"
#include "usbXTrans.h"
#include "usbXtor.h"
#include "xactors/usb/carbonXUsbTrans.h"
#include "xactors/usb/carbonXUsb.h"

#define NULL_TRANS_ERROR 1

static UsbxBool checkForNonNullTransfer(const CarbonXUsbTrans *trans);
static UsbxBool checkForControlTransfer(CarbonXUsbTrans *trans);

// Create and destroy functions
CarbonXUsbTrans* carbonXUsbTransCreate(CarbonXUsbPipe *pipe)
{
    CarbonXUsbTrans *trans = NULL;
    if (pipe->getType() == CarbonXUsbPipe_CONTROL)
        trans = new UsbxMessageTrans(pipe);
    else
        trans = new UsbxStreamTrans(pipe);
    if (trans == NULL)
        UsbXtor::error(pipe->getTransactor(), CarbonXUsbError_NoMemory,
                "Can't allocate memory for transaction object");
    return trans;
}

CarbonUInt32 carbonXUsbTransDestroy(CarbonXUsbTrans *trans)
{
    if (!checkForNonNullTransfer(trans))
        return 0;
    delete trans;
    return 1;
}

// Access function wrappers
CarbonXUsbPipe *carbonXUsbTransGetPipe(CarbonXUsbTrans *trans)
{
    if (!checkForNonNullTransfer(trans))
        return 0;
    return trans->getPipe();
}

CarbonUInt32 carbonXUsbTransSetRequestType(CarbonXUsbTrans *trans,
        CarbonUInt8 bmRequestType)
{
    if (!checkForControlTransfer(trans))
        return 0;
    static_cast<UsbxMessageTrans*>(trans)->setRequestType(bmRequestType);
    return 1;
}

CarbonUInt8 carbonXUsbTransGetRequestType(CarbonXUsbTrans *trans)
{
    if (!checkForControlTransfer(trans))
        return 0;
    return static_cast<UsbxMessageTrans*>(trans)->getRequestType();
}

CarbonUInt32 carbonXUsbTransSetRequest(CarbonXUsbTrans *trans,
        CarbonUInt8 bRequest)
{
    if (!checkForControlTransfer(trans))
        return 0;
    static_cast<UsbxMessageTrans*>(trans)->setRequest(bRequest);
    return 1;
}

CarbonUInt8 carbonXUsbTransGetRequest(CarbonXUsbTrans *trans)
{
    if (!checkForControlTransfer(trans))
        return 0;
    return static_cast<UsbxMessageTrans*>(trans)->getRequest();
}

CarbonUInt32 carbonXUsbTransSetRequestValue(CarbonXUsbTrans *trans,
        CarbonUInt16 wValue)
{
    if (!checkForControlTransfer(trans))
        return 0;
    static_cast<UsbxMessageTrans*>(trans)->setRequestValue(wValue);
    return 1;
}

CarbonUInt16 carbonXUsbTransGetRequestValue(CarbonXUsbTrans *trans)
{
    if (!checkForControlTransfer(trans))
        return 0;
    return static_cast<UsbxMessageTrans*>(trans)->getRequestValue();
}

CarbonUInt32 carbonXUsbTransSetRequestIndex(CarbonXUsbTrans *trans,
        CarbonUInt16 wIndex)
{
    if (!checkForControlTransfer(trans))
        return 0;
    static_cast<UsbxMessageTrans*>(trans)->setRequestIndex(wIndex);
    return 1;
}

CarbonUInt16 carbonXUsbTransGetRequestIndex(CarbonXUsbTrans *trans)
{
    if (!checkForControlTransfer(trans))
        return 0;
    return static_cast<UsbxMessageTrans*>(trans)->getRequestIndex();
}

CarbonUInt32 carbonXUsbTransSetRequestLength(CarbonXUsbTrans *trans,
        CarbonUInt16 wLength)
{
    if (!checkForControlTransfer(trans))
        return 0;
    static_cast<UsbxMessageTrans*>(trans)->setRequestLength(wLength);
    return 1;
}

CarbonUInt16 carbonXUsbTransGetRequestLength(CarbonXUsbTrans *trans)
{
    if (!checkForControlTransfer(trans))
        return 0;
    return static_cast<UsbxMessageTrans*>(trans)->getRequestLength();
}

CarbonUInt32 carbonXUsbTransSetData(CarbonXUsbTrans *trans,
        CarbonUInt32 dataSize, const CarbonUInt8 *data)
{
    if (!checkForNonNullTransfer(trans))
        return 0;
    trans->setData(dataSize, data);
    return 1;
}

CarbonUInt32 carbonXUsbTransGetDataSize(const CarbonXUsbTrans *trans)
{
    if (!checkForNonNullTransfer(trans))
        return 0;
    return trans->getDataSize();
}

const CarbonUInt8* carbonXUsbTransGetData(const CarbonXUsbTrans *trans)
{
    if (!checkForNonNullTransfer(trans))
        return 0;
    return trans->getData();
}

CarbonUInt32 carbonXUsbTransSetRetryStates(CarbonXUsbTrans *trans,
        CarbonUInt32 retrySize, const CarbonXUsbTransRetry *retryStates)
{
    if (!checkForNonNullTransfer(trans))
        return 0;
    trans->setRetryStates(retrySize, retryStates);
    return 1;
}

CarbonUInt32 carbonXUsbTransSetStallState(CarbonXUsbTrans *trans,
        CarbonUInt32 stallIndex)
{
    if (!checkForNonNullTransfer(trans))
        return 0;
    trans->setStallState(stallIndex);
    return 1;
}

CarbonXUsbStatusType carbonXUsbTransGetStatus(const CarbonXUsbTrans *trans)
{
    if (!checkForNonNullTransfer(trans))
        return CarbonXUsbStatus_HALT;
    return trans->getStatus();
}

void carbonXUsbTransDump(const CarbonXUsbTrans *trans)
{
    if (trans == NULL)
        return;
    trans->print();
}

// Utility functions
UsbxBool checkForNonNullTransfer(const CarbonXUsbTrans *trans)
{
    if (trans == NULL)
    {
#if NULL_TRANS_ERROR
        UsbXtor::error(NULL, CarbonXUsbError_InvalidTrans,
                "NULL transaction provided as argument.");
#endif
        return UsbxFalse;
    }
    return UsbxTrue;
}

UsbxBool checkForControlTransfer(CarbonXUsbTrans *trans)
{
    if (!checkForNonNullTransfer(trans))
        return UsbxFalse;
    if (trans->getType() != CarbonXUsbPipe_CONTROL)
    {
        UsbXtor::error(trans->getPipe()->getTransactor(),
                CarbonXUsbError_InvalidTrans,
                "Invalid transaction provided as argument.");
        return UsbxFalse;
    }
    return UsbxTrue;
}
