/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_HOSTFSM_H
#define USBX_HOSTFSM_H

#include "usbXFsm.h"
#include "usbXTransUnit.h"

/*!
 * \file usbXHostFsm.h
 * \brief Header file for host FSM.
 *
 * This file provides the class definition for host FSM.
 */

/*!
 * \brief Class to represent host FSM
 */
class UsbxHostFsm: public UsbxFsm
{
public: CARBONMEM_OVERRIDES

private:

    UsbxHostXtor *_xtor;                //!< Transactor reference

    //! Host FSM states
    enum UsbxHostState {
        // Timing states
        UsbxHostState_NoDeviceAttached,
        UsbxHostState_Reset,
        UsbxHostState_HsDetect,
        UsbxHostState_Suspend,
        UsbxHostState_Wait_Suspend,
        // Active idle state
        UsbxHostState_IDLE,
        // States for HC_Do_BCINTO and HC_HS_BCO
        UsbxHostState_Do_BCINTO_Do_token,
        UsbxHostState_Do_BCINTO_Do_data,
        UsbxHostState_Do_BCINTO_Wait_resp,
        UsbxHostState_Do_BCINTO_BCI_error,
        UsbxHostState_HS_BCO_Ping, // Not in spec
        UsbxHostState_HS_BCO_Ping_Wait_resp, // Not in spec
        // States for HC_Do_BCINTI
        UsbxHostState_Do_BCINTI_Do_token, // Not in spec
        UsbxHostState_Do_BCINTI_Wait_data,
        UsbxHostState_Do_BCINTI_Dopkt,
        UsbxHostState_Do_BCINTI_BCII_error,
        // States for HC_Do_IsochO
        UsbxHostState_Do_IsochO_IDotoken, // Not in spec
        UsbxHostState_Do_IsochO_IDodata,
        // States for HC_Do_IsochI
        UsbxHostState_Do_IsochI_IDotoken, // Not in spec
        UsbxHostState_Do_IsochI_Wait_resp
    };

    // State and transaction related states
    UsbxHostState _state;               //!< FSM state
    UsbxTransUnit *_currentTransUnit;   //!< Transaction unit under process
    UInt _timeWaited;                   //!< Wait time for checking timeout
    UsbxPacket *_handshakePacket;       //!< Handshake packet for freeing up

public:

    // Constructor and destructor
    UsbxHostFsm(UsbxHostXtor *xtor);
    ~UsbxHostFsm();

    // Evaluation
    UsbxBool evaluate(CarbonTime currentTime);

    // State change related functions
    void updateForNextTransUnit(CarbonTime currentTime);
    void updateForSameTransUnit(CarbonTime currentTime);
    void updateForHalt(CarbonTime currentTime);

    // Functions to drive and check suspend
    UsbxBool suspend();
    UsbxBool isSuspended() const
    { return (_state == UsbxHostState_Suspend); }
    UsbxBool isDeviceDetached() const
    { return (_state == UsbxHostState_NoDeviceAttached); }
    // Function to resume from suspend
    UsbxBool resume();

    //! Reset for detach
    void resetForDetach();

    // Utility function that dumps the host FSM state
    void print(CarbonTime currentTime) const;
};

#endif
