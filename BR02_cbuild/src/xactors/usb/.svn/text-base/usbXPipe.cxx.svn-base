/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    usbXPipe.cpp
    Purpose: This file provides the member function definition for pipe.
*/

#include "usbXPipe.h"
#include "usbXtor.h"
#include "xactors/usb/carbonXUsbDescriptors.h"

CarbonXUsbPipeClass::CarbonXUsbPipeClass(UsbXtor *xtor,
        CarbonUInt7 deviceAddress, const UsbxByte *epDescriptor):
    _xtor(xtor), _deviceAddress(deviceAddress), _epDescriptor(NULL),
    // Note, for device singleton queues will be created
    _transQueue(new UsbxQueue<UsbxTrans>(
                _xtor->getType() == CarbonXUsb_Device)),
    _nextDataPacketType(UsbxPacket_DATA0), _frameOffset(0)
{
    if (epDescriptor)
    {
        ASSERT(checkDescriptorForPipe(epDescriptor), _xtor);
        _epDescriptor = CarbonXUsbConfig::copyDescriptor(epDescriptor);
        if (_epDescriptor == NULL)
        {
            if (xtor->allMemoryAllocated())
                UsbXtor::error(_xtor, CarbonXUsbError_NoMemory,
                        "Can't allocate memory for pipe descriptor");
            else
                xtor->errorForNoMemory("pipe descriptor");
            return;
        }
    }
    if (_transQueue == NULL)
    {
        if (xtor->allMemoryAllocated())
            UsbXtor::error(_xtor, CarbonXUsbError_NoMemory,
                    "Can't allocate memory for transaction queue for pipe");
        else
            xtor->errorForNoMemory("transaction queue for pipe");
        return;
    }
}

CarbonXUsbPipeClass::~CarbonXUsbPipeClass()
{
    if (_epDescriptor)
        DELETE(_epDescriptor);
    delete _transQueue;
}

UsbxBool CarbonXUsbPipeClass::setEp0Descriptor(const UsbxByte *ep0Descriptor,
        UInt ep0DescriptorSize)
{
    if (_epDescriptor != NULL)
        DELETE(_epDescriptor);
    _epDescriptor = CarbonXUsbConfig::copyDescriptor(ep0Descriptor,
            ep0DescriptorSize);
    if (_epDescriptor == NULL)
    {
        UsbXtor::error(_xtor, CarbonXUsbError_NoMemory,
                "Can't allocate memory for transaction queue for pipe");
        return UsbxFalse;
    }
    return UsbxTrue;
}

// Info extraction functions

CarbonXUsbPipeType CarbonXUsbPipeClass::getType() const
{
    if (_epDescriptor == NULL)
        return CarbonXUsbPipe_CONTROL;
    CarbonXUsbDescriptorType descriptorType =
            static_cast<CarbonXUsbDescriptorType>(_epDescriptor[1]);
    // For default control pipe
    if (descriptorType == CarbonXUsbDescriptor_DEVICE)
        return CarbonXUsbPipe_CONTROL;
    return static_cast<CarbonXUsbPipeType>(_epDescriptor[3] & 0x03); // Bits 1:0
}

UsbxBool CarbonXUsbPipeClass::isPeriodic() const
{
    return (getType() == CarbonXUsbPipe_INTR ||
            getType() == CarbonXUsbPipe_ISO);
}

CarbonUInt4 CarbonXUsbPipeClass::getEpNumber() const
{
    if (_epDescriptor == NULL)
        return 0;
    CarbonXUsbDescriptorType descriptorType =
            static_cast<CarbonXUsbDescriptorType>(_epDescriptor[1]);
    // For default control pipe
    if (descriptorType == CarbonXUsbDescriptor_DEVICE)
        return 0;
    return _epDescriptor[2] & 0x0F; // Bits 3:0
}

CarbonXUsbPipeDirection CarbonXUsbPipeClass::getDirection() const
{
    if (_epDescriptor == NULL)
        return CarbonXUsbPipeDirection_UNDEF;
    CarbonXUsbDescriptorType descriptorType =
            static_cast<CarbonXUsbDescriptorType>(_epDescriptor[1]);
    // For default control pipe
    if (descriptorType == CarbonXUsbDescriptor_DEVICE ||
            // or other control endpoints
            static_cast<CarbonXUsbPipeType>(_epDescriptor[3] & 0x03) ==
            CarbonXUsbPipe_CONTROL)
        return CarbonXUsbPipeDirection_UNDEF;
    // Bit 7
    return static_cast<CarbonXUsbPipeDirection>(
            (_epDescriptor[2] & 0x80) >> 7);
}

CarbonUInt32 CarbonXUsbPipeClass::getMaxPayloadSize() const
{
    if (_epDescriptor == NULL)
        return 8;
    CarbonXUsbDescriptorType descriptorType =
            static_cast<CarbonXUsbDescriptorType>(_epDescriptor[1]);
    // For default control pipe
    if (descriptorType == CarbonXUsbDescriptor_DEVICE)
        return _epDescriptor[7];
    return (_epDescriptor[5] << 8 | _epDescriptor[4]) & 0x07FF; // Bits 10:0
}

UInt CarbonXUsbPipeClass::getPeriodicInterval() const
{
    if (_epDescriptor == NULL)
        return 0;

    if (!isPeriodic())
        return 0;

    UsbxByte bInterval = _epDescriptor[6] & 0xFF;
    if (_xtor->isFS() && (getType() == CarbonXUsbPipe_INTR))
    {
        // Limiting to 1 to 255
        if (bInterval < 1)
            bInterval = 1;
        return bInterval;
    }

    // Limiting range to 1 to 16
    if (bInterval < 1)
        bInterval = 1;
    else if (bInterval > 16)
        bInterval = 16;
    return (1 << (bInterval - 1));
}

UInt CarbonXUsbPipeClass::getRepeatCount() const
{
    if (_epDescriptor == NULL)
        return 0;

    if (_xtor->isFS() || !isPeriodic())
        return 0;

    return (_epDescriptor[5] >> 3) & 0x3; // Bits 12:11
}

UsbxBool CarbonXUsbPipeClass::checkDescriptorForPipe(
        const CarbonUInt8 *_epDescriptor)
{
    if (_epDescriptor == NULL)
        return UsbxTrue;
    CarbonXUsbDescriptorType descriptorType =
            static_cast<CarbonXUsbDescriptorType>(_epDescriptor[1]);
    if (descriptorType != CarbonXUsbDescriptor_DEVICE &&
            descriptorType != CarbonXUsbDescriptor_ENDPOINT)
        return UsbxFalse;
    return UsbxTrue;
}

UsbxConstString CarbonXUsbPipeClass::getTypeStr() const
{
    switch (getType())
    {
        case CarbonXUsbPipe_CONTROL: return "Control";
        case CarbonXUsbPipe_BULK:    return "Bulk";
        case CarbonXUsbPipe_INTR:    return "Interrupt";
        case CarbonXUsbPipe_ISO:     return "Isochronous";
    }
    return NULL;
}

UsbxConstString CarbonXUsbPipeClass::getDirectionStr() const
{
    switch (getDirection())
    {
        case CarbonXUsbPipeDirection_UNDEF:  return NULL;
        case CarbonXUsbPipeDirection_OUTPUT: return "Output";
        case CarbonXUsbPipeDirection_INPUT:  return "Input";
        default: return NULL;
    }
    return NULL;
}

void CarbonXUsbPipeClass::print() const
{
    printf("%s ", getTypeStr());
    if (getDirectionStr())
        printf("%s ", getDirectionStr());
    printf("Pipe: Device Address: %d, Endpoint Number: %d\n",
            _deviceAddress, getEpNumber());
    printf("Maximum data packet size = %d\n", getMaxPayloadSize());
}
