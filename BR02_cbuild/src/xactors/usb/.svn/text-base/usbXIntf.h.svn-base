/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_INTF_H
#define USBX_INTF_H

#include "usbXPin.h"
#include "usbXPacket.h"

/*!
 * \file usbXIntf.h
 * \brief Header file for pin level interface definition.
 *
 * This file provides the class definition for pin level interface and APIs
 * for accessing such interface from USB FSM.
 */

/*!
 * \brief Timing type
 */
enum UsbxTimingType {
    UsbxTiming_None,
    // Device attachment
    UsbxTiming_DeviceAttach,  //!< Device attach from PHY device
    UsbxTiming_DeviceDetach,  //!< Device detach from PHY device
    // Reset and HS detection
    UsbxTiming_HostFsReset,   //!< Reset from Host having only FS capability
    UsbxTiming_HostHsReset,   //!< Reset from Host having HS/FS capability
    UsbxTiming_DeviceFsReset, //!< Reset from Device having only FS capability
    UsbxTiming_DeviceHsReset, //!< Reset from Device having HS/FS capability
    UsbxTiming_HsDetected,    //!< HS detected response to both FSM
    // Suspend resume
    UsbxTiming_Suspend,       //!< Suspend from Host
    UsbxTiming_Resume,        //!< Resume from suspended Host
    UsbxTiming_RemoteWakeUp,  //!< Resume from suspended Device
    // OTG timing
    UsbxTiming_OtgSrp,        //!< SRP for OTG
    UsbxTiming_OtgHnp         //!< HNP for OTG
};

/*!
 * \brief Class to represent pin level interface
 */
class UsbxIntf
{
public: CARBONMEM_OVERRIDES

protected:

    const UsbXtor *_xtor;               //!< Transactor reference
    UsbxBool _isFS;                     //!< USB speed
    CarbonUInt32 _timingFactor;         //!< Timing factor
    static const CarbonUInt32 _maxTimingFactor; //!< Max. and default
                                                //   timing factor = 600

public:

    // Constructor and destructor
    UsbxIntf(UsbXtor *xtor): _xtor(xtor), _isFS(UsbxTrue),
        _timingFactor(_maxTimingFactor) {}
    virtual ~UsbxIntf() {}

    // Speed related functions
    UsbxBool isFS() const { return _isFS; }
    virtual void setSpeedFS(UsbxBool isFS, CarbonTime currentTime)
    { _isFS = isFS; currentTime = 0; }
    virtual void setTimingFactor(CarbonUInt32 factor);
    CarbonUInt32 getTimingFactor() const { return _timingFactor; }

    virtual UsbxBool transmitPacket(UsbxPacket *packet) = 0;
    virtual UsbxBool transmitComplete() const = 0;
    virtual UsbxPacket *receivePacket() = 0;
    virtual UsbxBool receiving(CarbonTime currentTime) const = 0;

    virtual UsbxBool startTiming(UsbxTimingType timing) = 0;
    virtual UsbxBool timingComplete() const = 0;
    virtual UsbxBool senseTiming(UsbxTimingType expectedTiming,
            UsbxBool flush = UsbxTrue) = 0;

    virtual UsbxBool driveVbus(UsbxBool isOn) = 0;
    virtual UsbxBool isVbusValid() const = 0;

    //! Reset the interface for detach
    virtual void resetForDetach() = 0;

    // Refresh and evaluate
    virtual UsbxBool refreshInputs(CarbonTime currentTime) = 0;
    virtual UsbxBool refreshOutputs(CarbonTime currentTime) = 0;
    virtual UsbxBool evaluate(CarbonTime currentTime) = 0;

    // Utility functions
    static UsbxPin *createPin(UsbXtor *xtor, UsbxConstString pinName,
            const UsbxDirection direction, const CarbonUInt32 size = 1);
    virtual UsbxPin *getPin(UsbxConstString pinName) const = 0;
};

#endif
