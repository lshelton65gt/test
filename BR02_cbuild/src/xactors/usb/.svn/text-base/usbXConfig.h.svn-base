/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_CONFIG_H
#define USBX_CONFIG_H

#include "usbXDefines.h"
#include "usbXQueue.h"
#include "xactors/usb/carbonXUsbConfig.h"

/*!
 * \file usbXConfig.h
 * \brief Header file for configuration class.
 *
 * This file provides the class definition for configuration object.
 */

/*!
 * \brief Class to represent configuration object.
 *
 * This configuration class encapsulates following parameters:
 * - USB speed i.e. FS or HS (LS not supported)
 * - Interface type i.e. UTMI or ULPI
 * - Interface side i.e. PHY or LINK
 * - UTMI data bus width i.e. 8 or 16 (both unidirectional)
 * - Device Descriptor (for Device transactor only)
 * - Configuration Descriptors (for Device transactor only)
 */
class CarbonXUsbConfigClass
{
public: CARBONMEM_OVERRIDES

private:

    CarbonXUsbSpeedType _speed;              //!< USB speed
    CarbonXUsbInterfaceType _intfType;       //!< Interface type
    CarbonXUsbIntfSide _intfSide;            //!< Interface side
    CarbonXUsbUtmiDataBusWidthType _utmiWidth; //!< UTMI data width
    CarbonXUsbUtmiPinLevel _utmiPinLevel;    //!< UTMI pin level
    UsbxByte *_deviceDesc;                   //!< Device descriptor
    UsbxByte *_otgDesc;                      //!< OTG descriptor
    UsbxQueue<UsbxByte> *_configDescList;    //!< Configuration descriptor list

    CarbonUInt32 _timingFactor;              //!< Timing factor - default 600
    static const CarbonUInt32 _maxTimingFactor;
                                             //!< Default timing factor = 600
    UsbxBool _miniAPhyPlug;                  //!< Mini-A PHY plug offering
    CarbonXUsbOtgSrpDetectionSupport _srpDetectionSupport; //! SRP detection
    UsbxBool _isHub;                         //!< Device acts as hub

public:

    // Constructor and destructor
    CarbonXUsbConfigClass();
    CarbonXUsbConfigClass(const CarbonXUsbConfigClass &config);
    ~CarbonXUsbConfigClass();

    // Set get functions for USB bus speed
    void setSpeed(CarbonXUsbSpeedType speed) { _speed = speed; }
    CarbonXUsbSpeedType getSpeed() const { return _speed; }

    // Set get functions for interface type
    void setIntfType(CarbonXUsbInterfaceType intfType)
    { _intfType = intfType; }
    CarbonXUsbInterfaceType getIntfType() const { return _intfType; }

    // Set get functions for interface side
    void setIntfSide(CarbonXUsbIntfSide intfSide)
    { _intfSide = intfSide; }
    CarbonXUsbIntfSide getIntfSide() const { return _intfSide; }

    // Set get UTM interface data width
    void setUtmiDataBusWidth(CarbonXUsbUtmiDataBusWidthType utmiWidth)
    { _utmiWidth = utmiWidth; }
    CarbonXUsbUtmiDataBusWidthType getUtmiDataBusWidth() const
    { return _utmiWidth; }

    // Set get UTM interface pin level
    void setUtmiPinLevel(CarbonXUsbUtmiPinLevel utmiPinLevel)
    { _utmiPinLevel = utmiPinLevel; }
    CarbonXUsbUtmiPinLevel getUtmiPinLevel() const
    { return _utmiPinLevel; }

    // Set get functions for device descriptor
    void setDeviceDescriptor(const UsbxByte *descriptor);
    const UsbxByte *getDeviceDescriptor() const
    { return _deviceDesc; }

    // Set get functions for OTG descriptor
    void setOtgDescriptor(const UsbxByte *descriptor);
    const UsbxByte *getOtgDescriptor() const
    { return _otgDesc; }
    UsbxBool isOtgSrpSupported() const
    { return (_otgDesc == NULL) || (_otgDesc[2] & 0x1); }
    UsbxBool isOtgHnpSupported() const
    { return (_otgDesc == NULL) || ((_otgDesc[2] >> 1) & 0x1); }

    // Set get functions for configuration descriptor
    void addConfigDescriptor(const UsbxByte *descriptor);
    const UsbxQueue<UsbxByte> *getConfigDescriptorList() const
    { return _configDescList; }

    // Descriptor copy function
    static UsbxByte *copyDescriptor(const UsbxByte *descriptor,
            UInt descriptorSize = 0); // descriptorSize > 0 indicates exact size

    // Set get functions for timing factor
    void setTimingFactor(CarbonUInt32 factor);
    CarbonUInt32 getTimingFactor() const
    { return _timingFactor; }

    // Set get functions for OTG PHY plug offering
    void setMiniAPhyPlug() { _miniAPhyPlug = UsbxTrue; }
    UsbxBool getMiniAPhyPlug() const { return _miniAPhyPlug; }

    // Set get functions for OTG SRP detection support
    void setOtgSrpDetectionSupport(CarbonXUsbOtgSrpDetectionSupport srpSupport)
    { _srpDetectionSupport = srpSupport; }
    CarbonXUsbOtgSrpDetectionSupport getOtgSrpDetectionSupport() const
    { return _srpDetectionSupport; }

    // Set get function for device as hub
    void setDeviceAsHub() { _isHub = UsbxTrue; }
    UsbxBool isHub() const { return _isHub; }
};

#endif
