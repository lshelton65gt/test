/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_DEVICEFSM_H
#define USBX_DEVICEFSM_H

#include "usbXFsm.h"
#include "usbXTrans.h"

/*!
 * \file usbXDeviceFsm.h
 * \brief Header file for device FSM (Finite State Machine).
 *
 * This file provides the class definition for device FSM
 */

/*!
 * \brief Class for device FSM.
 */
class UsbxDeviceFsm: public UsbxFsm
{
public: CARBONMEM_OVERRIDES

private:

    UsbxDeviceXtor *_xtor;              //!< Transactor reference

    //! Device FSM states
    enum UsbxDeviceState {
        // Timing states
        UsbxDeviceState_Detached,
        UsbxDeviceState_Attached,
        UsbxDeviceState_Suspended,
        UsbxDeviceState_Reset,
        UsbxDeviceState_HsDetect,
        // Active idle state
        UsbxDeviceState_IDLE,
        // States for Dev_Do_BCINTO, Dev_HS_BCO and Dev_HS_ping
        UsbxDeviceState_Do_BCINTO_Wait_Odata,
        UsbxDeviceState_Do_BCINTO_Dopkt,
        UsbxDeviceState_HS_Ping,
        // States for Dev_Do_BCINTI
        UsbxDeviceState_Do_BCINTI_Data,
        UsbxDeviceState_Do_BCINTI_Resp,
        UsbxDeviceState_Do_BCINTI_Dopkt,
        // States for Dev_Do_IsochO
        UsbxDeviceState_Do_IsochO_Wait_data,
        // States for Dev_Do_IsochI
        UsbxDeviceState_Do_IsochI_Data, // Not in spec
        UsbxDeviceState_Do_IsochI_Done // Not in spec
    };

    UsbxDeviceState _state;             //!< FSM state
    UsbxTransUnit *_currentTransUnit;   //!< Current transaction unit
    UInt _timeWaited;                   //!< Wait time for checking timeout
    UsbxPacket *_handshakePacket;       //!< Handshake packet for freeing up

public:

    // Constructor and destructor
    UsbxDeviceFsm(UsbxDeviceXtor *xtor);
    ~UsbxDeviceFsm();

    // Attach and detach functions
    UsbxBool attach();
    UsbxBool detach();

    // Evaluation
    UsbxBool evaluate(CarbonTime currentTime);

    // State change related functions
    void updateForNextTransUnit(CarbonTime currentTime);
    void updateForSameTransUnit(CarbonTime currentTime);
    void updateForHalt(CarbonTime currentTime);

    // Function to check suspend
    UsbxBool isSuspended() const
    { return (_state == UsbxDeviceState_Suspended); }
    // Function to drive remote wake up
    UsbxBool remoteWakeUp();

    //! Reset for detach
    void resetForDetach();

    // Utility function that dumps the device FSM state
    void print(CarbonTime currentTime) const;
};

#endif
