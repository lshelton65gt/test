/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_DEFINES_H
#define USBX_DEFINES_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*!
 * \file usbXDefines.h
 * \brief Header file for project specific type definitions.
 *
 * This file provides the type definitions to be used globally in the project.
 * Define all local types in this file.
 */

/* Unified type definition for integer and unsigned integer */
/*! \brief Type for integer */
typedef int           Int;
/*! \brief Type of unsigned integer */
typedef unsigned int  UInt;

/* Macros for memory management */
#include "carbon/c_memmanager.h"
/*! \brief Checks if memory is available
 *
 * This routine is provided for emulation of no memory case.
 * \param size Memory need to be allocated
 * \retval 1 If memory can be allocated.
 * \retval 0 Otherwise
 */
UInt UsbxIsMemAvail(size_t size);
/*! \brief Macro to allocate memory */
#define NEW(type) static_cast<type*>(UsbxIsMemAvail(sizeof(type))?carbonmem_malloc(sizeof(type)):NULL)
/*! \brief Macro to allocate memory for array */
#define NEWARRAY(type, size) static_cast<type*>(UsbxIsMemAvail(sizeof(type)*(size))?carbonmem_malloc(sizeof(type)*(size)):NULL)
/*! \brief Macro to free memory */
#define DELETE(ptr) carbonmem_free(const_cast<void*>(static_cast<const void*>(ptr)))

/* Macros for debug purpose */
/*! \brief Macro to print a variable value */
#define PRINT(var) printf(#var" = %u\n",var)

/* Data types */
/*! \brief String type */
typedef char*         UsbxString;
/*! \brief Constant string type */
typedef const char*   UsbxConstString;
/*! \brief Bool type */
typedef char          UsbxBool;

#include "carbon/carbon_shelltypes.h"
/*! \brief Bit type */
typedef CarbonUInt1   UsbxBit;
/*! \brief Byte type */
typedef CarbonUInt8   UsbxByte;

/* Definition of true and false
 * Note, macro 'DEF_BOOL_CONST' is defined only in usbXDefines.cpp so that
 * usbXDefines.cpp can have the definition while other files will use the
 * extern forward declaration. */
/* Declaration of true and false */
/*! \brief Definition of False */
extern const UsbxBool UsbxFalse;
/*! \brief Definition of True */
extern const UsbxBool UsbxTrue;
#ifdef DEF_BOOL_CONST
/*! \brief Definition of false */
const UsbxBool UsbxFalse = 0;
/*! \brief Definition of true */
const UsbxBool UsbxTrue  = 1;
#endif

/* Declaration for UsbXtor */
class CarbonXUsbClass;
class CarbonXUsbPipeClass;
class CarbonXUsbTransClass;
/*! \brief Type for Carbon USB Transactor */
typedef class CarbonXUsbClass UsbXtor;
class UsbxHostXtor; class UsbxDeviceXtor;
typedef class CarbonXUsbPipeClass UsbxPipe;
typedef class CarbonXUsbTransClass UsbxTrans;

/*! \brief Function to string duplication */
UsbxString UsbxStrdup(UsbxConstString str);
/*! \brief Function to get a large buffer for string message creation */
UsbxString UsbxGetStringBuffer();
/*! \brief Assert function specific to this project */
UInt UsbxAssertFail(const UsbXtor *xtor, UsbxConstString failExpr,
        UsbxConstString file, UInt line);
/*! \brief Assert Macro */
#define ASSERT(x,y) ((x)?0:UsbxAssertFail((y),#x,__FILE__,__LINE__))

#endif
