/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    usbXTrans.cpp
    Purpose: This file contains the definition of USB transfer classes.
*/

#include "usbXTrans.h"
#include "usbXFsm.h"
#include "usbXtor.h"

CarbonXUsbTransClass::CarbonXUsbTransClass(UsbxPipe *pipe):
    _pipe(pipe),
    _dataSize(0), _data(NULL),
    _nextDataIndex(0),
    _status(CarbonXUsbStatus_SUCCESS),
    _retrySize(0), _retryStates(NULL), _sendPing(UsbxFalse),
    _stallIndex(static_cast<CarbonUInt32>(-1)),
    _currentTransUnit(NULL),
    _receiveDataQueue(NULL),
    _transUnitIndex(0)
{}

CarbonXUsbTransClass::~CarbonXUsbTransClass()
{
    if (_data != NULL)
        DELETE(_data);
    if (_retryStates != NULL)
        DELETE(_retryStates);
    delete _currentTransUnit;
    if (_receiveDataQueue)
    {
        while (!_receiveDataQueue->isEmpty())
            delete _receiveDataQueue->pop();
        delete _receiveDataQueue;
    }
}

UsbxBool CarbonXUsbTransClass::setData(CarbonUInt32 dataSize,
        const CarbonUInt8 *data)
{
    _dataSize = dataSize;
    _data = copyData(_dataSize, data);
    if (_data == NULL)
    {
        _dataSize = 0;
        UsbXtor::error(_pipe->getTransactor(), CarbonXUsbError_NoMemory,
                "Can't set transaction data", this);
        return UsbxFalse;
    }
    return UsbxTrue;
}

UsbxBool CarbonXUsbTransClass::setRetryStates(CarbonUInt32 retrySize,
            const CarbonXUsbTransRetry *retryStates)
{
    _retrySize = retrySize;
    _retryStates = NEWARRAY(CarbonXUsbTransRetry, _retrySize);
    if (_retryStates == NULL)
    {
        _retrySize = 0;
        UsbXtor::error(_pipe->getTransactor(), CarbonXUsbError_NoMemory,
                "Can't copy retry states", this);
        return UsbxFalse;
    }
    for (UInt i = 0; i < _retrySize; i++)
        _retryStates[i] = retryStates[i];
    return UsbxTrue;
}

const CarbonXUsbTransRetry* CarbonXUsbTransClass::hasRetry(
        UInt transUnitIndex) const
{
    if (_retrySize == 0 || _retryStates == NULL)
        return NULL;
    for (UInt i = 0; i < _retrySize; i++)
    {
        if (_retryStates[i].transUnitIndex == transUnitIndex)
            return _retryStates + i;
    }
    return NULL;
}

// Pops data for output transaction unit
UsbxDataPacket* CarbonXUsbTransClass::popDataForTransUnit()
{
    if (_dataSize == 0 || _data == NULL)
    {
        UsbXtor::error(_pipe->getTransactor(), CarbonXUsbError_InvalidTrans,
                "No data is provided in transaction", this);
        return NULL;
    }

    if (_nextDataIndex > _dataSize) // Last transUnit has been popped
        return NULL;

    CarbonUInt32 maxPayloadSize = _pipe->getMaxPayloadSize();
    CarbonUInt32 transUnitSize = maxPayloadSize;
    if (_dataSize - _nextDataIndex < maxPayloadSize) // Last data unit
        transUnitSize = _dataSize - _nextDataIndex;

    // For high speed high bandwidth isochronous specific data packet generation
    setDataPacketTypeForHsRepeat();

    UsbxDataPacket *dataPacket = NULL;
    switch (_pipe->getNextDataPacketType())
    {
        case UsbxPacket_DATA0:
        {
            dataPacket = new UsbxData0Packet(transUnitSize,
                    _data + _nextDataIndex);
            _pipe->setNextDataPacketType(UsbxPacket_DATA1);
            break;
        }
        case UsbxPacket_DATA1:
        {
            dataPacket = new UsbxData1Packet(transUnitSize,
                    _data + _nextDataIndex);
            _pipe->setNextDataPacketType(UsbxPacket_DATA0);
            break;
        }
        case UsbxPacket_DATA2:
        {
            dataPacket = new UsbxData2Packet(transUnitSize,
                    _data + _nextDataIndex);
            break;
        }
        case UsbxPacket_MDATA:
        {
            dataPacket = new UsbxMDataPacket(transUnitSize,
                    _data + _nextDataIndex);
            break;
        }
        default: ASSERT(0, _pipe->getTransactor());
    }
    if (dataPacket == NULL)
    {
        UsbXtor::error(_pipe->getTransactor(), CarbonXUsbError_NoMemory,
                "Can't create data packet", this);
        return NULL;
    }

    // For last packet in data stage of control transfer
    if (_pipe->getType() == CarbonXUsbPipe_CONTROL &&
            transUnitSize < maxPayloadSize)
        _pipe->setNextDataPacketType(UsbxPacket_DATA1);

    // Update data index
    _nextDataIndex += maxPayloadSize;

    return dataPacket;
}

// Pushes data for input transaction unit
UsbxBool CarbonXUsbTransClass::pushDataForTransUnit(UsbxDataPacket *dataPacket)
{
    if (dataPacket == NULL)
        return UsbxFalse;

    // Check the data packet to be ok
    if (dataPacket->getPayloadSize() > _pipe->getMaxPayloadSize() ||
            dataPacket->getType() != _pipe->getNextDataPacketType())
    {
        UsbXtor::error(_pipe->getTransactor(), CarbonXUsbError_ProtocolError,
                "Invalid (type or size) data packet received", this);
        return UsbxFalse;
    }

    // Push the data packet into data packet queue
    if (_receiveDataQueue == NULL)
        _receiveDataQueue = new UsbxQueue<UsbxDataPacket>;
    _receiveDataQueue->push(dataPacket);

    // Update next expected data packet type
    if (_pipe->getNextDataPacketType() == UsbxPacket_DATA0)
        _pipe->setNextDataPacketType(UsbxPacket_DATA1);
    else if (_pipe->getNextDataPacketType() == UsbxPacket_DATA1)
        _pipe->setNextDataPacketType(UsbxPacket_DATA0);

    // When last data packet is received, create the whole data for the transfer
    if (dataPacket->getPayloadSize() < _pipe->getMaxPayloadSize())
    {
        // For last packet in data stage of control transfer
        if (_pipe->getType() == CarbonXUsbPipe_CONTROL)
            _pipe->setNextDataPacketType(UsbxPacket_DATA1);
        return createReceiveData();
    }

    _nextDataIndex += dataPacket->getPayloadSize();

    return UsbxTrue;
}

// Creates data array for entire transaction, when input transaction units are
// all complete or aborted at a point
UsbxBool CarbonXUsbTransClass::createReceiveData()
{
    if (_receiveDataQueue == NULL)
        return UsbxFalse;

    // Create data array
    _dataSize = (_receiveDataQueue->getCount() - 1) * _pipe->getMaxPayloadSize()
        + _receiveDataQueue->bottom()->getPayloadSize();
    _data = NEWARRAY(UsbxByte, _dataSize);
    if (_data == NULL)
    {
        UsbXtor::error(_pipe->getTransactor(), CarbonXUsbError_NoMemory,
                "Can't create transaction data", this);
        return UsbxFalse;
    }

    // Copy data from data packets
    UInt dataIndex = 0;
    while (UsbxDataPacket *dataPacket = _receiveDataQueue->pop())
    {
        const UsbxByte *packetData = dataPacket->getPayloadData();
        for (UInt i = 0; i < dataPacket->getPayloadSize(); i++, dataIndex++)
            _data[dataIndex] = packetData[i];
        delete dataPacket;
    }
    delete _receiveDataQueue;
    _receiveDataQueue = NULL;
    return UsbxTrue;
}

UsbxTransUnit* CarbonXUsbTransClass::popTransUnitForRetry()
{
    UsbxTransUnit *returnTransUnit = _currentTransUnit;
    _currentTransUnit = NULL;
    return returnTransUnit;
}

CarbonUInt8* CarbonXUsbTransClass::copyData(CarbonUInt32 size,
        const CarbonUInt8 *data)
{
    if (size == 0 || data == NULL)
        return NULL;

    CarbonUInt8 *newData = NEWARRAY(CarbonUInt8, size);
    if (newData == NULL)
        return NULL;

    for (UInt i = 0; i < size; i++)
        newData[i] = data[i];
    return newData;
}

void CarbonXUsbTransClass::print() const
{
    printf("%s ", _pipe->getTypeStr());
    if (_pipe->getType() == CarbonXUsbPipe_CONTROL)
    {
        const UsbxMessageTrans *controlTrans =
            static_cast<const UsbxMessageTrans*>(this);
        printf("%s ", (controlTrans->getDirection() ==
                    CarbonXUsbPipeDirection_OUTPUT) ? "Output" : "Input");
    }
    else if (_pipe->getDirectionStr())
        printf("%s ", _pipe->getDirectionStr());
    printf("Transfer:\nDevice Address: %d, Endpoint Number: %d.\n",
            _pipe->getDeviceAddress(), _pipe->getEpNumber());
    printFields();
    for (UInt i = 0; i < _dataSize; i++)
    {
        if (i % 8 == 0)
            printf("Data[%04d:%04d]: ", i,
                    ((i + 8) > _dataSize) ? (_dataSize - 1) : (i + 7));
        printf("%3d ", _data[i]);
        if ((i + 1) % 8 == 0 || i == (_dataSize - 1))
            printf("\n");
    }
}

void UsbxMessageTrans::printFields() const
{
    printf("bmRequestType: %X, bRequest: %X, "
            "wValue: %X, wIndex: %X, wLength: %X\n",
            getRequestType(), getRequest(),
            getRequestValue(), getRequestIndex(), getRequestLength());
}

UsbxMessageTrans::UsbxMessageTrans(UsbxPipe *pipe):
    CarbonXUsbTransClass(pipe), _nextStage(UsbxMessageStage_SETUP)
{
    for (UInt i = 0; i < 8; i++)
        _setupData[i] = 0;
}

// Pops the next transaction unit for control transfer
UsbxTransUnit* UsbxMessageTrans::popNextTransUnit()
{
    CarbonUInt7 deviceAddress = _pipe->getDeviceAddress();
    CarbonUInt4 epNumber = _pipe->getEpNumber();

    UsbxBool isLast = UsbxFalse;
    UsbxTokenPacket *tokenPacket = NULL;
    UsbxDataPacket *dataPacket = NULL;
    switch (_nextStage)
    {
        case UsbxMessageStage_SETUP:
        {
            tokenPacket = new UsbxSetupPacket(deviceAddress, epNumber);
            if (_pipe->getTransactor()->isHost())
            {
                dataPacket = new UsbxData0Packet(8, _setupData);
                if (dataPacket == NULL)
                {
                    UsbXtor::error(_pipe->getTransactor(),
                            CarbonXUsbError_NoMemory,
                            "Can't create data packet", this);
                    return NULL;
                }
                if (getRequestLength() == 0) // For no data control transfer
                    _nextStage = UsbxMessageStage_STATUS;
                else
                    _nextStage = UsbxMessageStage_DATA;

            }
            _pipe->setNextDataPacketType(UsbxPacket_DATA1);
            break;
        }
        case UsbxMessageStage_DATA:
        {
            // Create token packet as per direction
            if (getDirection() == CarbonXUsbPipeDirection_OUTPUT)
            {
                tokenPacket = new UsbxOutPacket(deviceAddress, epNumber);
                if (_pipe->getTransactor()->isHost())
                {
                    dataPacket = popDataForTransUnit();
                    if (dataPacket == NULL) // No memory error come from inside
                        return NULL;
                    // When last data unit has been sent
                    if (dataPacket->getPayloadSize() <
                            _pipe->getMaxPayloadSize() ||
                            (_nextDataIndex == _dataSize &&
                            getRequestLength() == _dataSize))
                    {
                        _pipe->setNextDataPacketType(UsbxPacket_DATA1);
                        _nextStage = UsbxMessageStage_STATUS;
                    }
                }
            }
            else // if (CarbonXUsbPipeDirection_INPUT)
            {
                tokenPacket = new UsbxInPacket(deviceAddress, epNumber);
                if (!_pipe->getTransactor()->isHost())
                {
                    dataPacket = popDataForTransUnit();
                    if (dataPacket == NULL) // No memory error come from inside
                        return NULL;
                    // When last data unit has been sent
                    if (dataPacket->getPayloadSize() <
                            _pipe->getMaxPayloadSize() ||
                            (_nextDataIndex == _dataSize &&
                            getRequestLength() == _dataSize))
                    {
                        _pipe->setNextDataPacketType(UsbxPacket_DATA1);
                        _nextStage = UsbxMessageStage_STATUS;
                    }
                }
            }
            break;
        }
        case UsbxMessageStage_STATUS:
        {
            // Create token packet as per opposite direction, i.e.
            // IN token packet for output and OUT token packet for input
            // Note, for zero data control status uses always IN token packet
            if (getRequestLength() == 0 ||
                    getDirection() == CarbonXUsbPipeDirection_OUTPUT)
            {
                tokenPacket = new UsbxInPacket(deviceAddress, epNumber);
                if (!_pipe->getTransactor()->isHost())
                {
                    dataPacket = new UsbxData1Packet(0, NULL);
                    if (dataPacket == NULL)
                    {
                        UsbXtor::error(_pipe->getTransactor(),
                                CarbonXUsbError_NoMemory,
                                "Can't create data packet", this);
                        return NULL;
                    }
                }
            }
            else // if (CarbonXUsbPipeDirection_INPUT)
            {
                tokenPacket = new UsbxOutPacket(deviceAddress, epNumber);
                if (_pipe->getTransactor()->isHost())
                {
                    dataPacket = new UsbxData1Packet(0, NULL);
                    if (dataPacket == NULL)
                    {
                        UsbXtor::error(_pipe->getTransactor(),
                                CarbonXUsbError_NoMemory,
                                "Can't create data packet", this);
                        return NULL;
                    }
                }
            }
            isLast = UsbxTrue;
            break;
        }
        default: ASSERT(0, _pipe->getTransactor());
    }
    if (tokenPacket == NULL)
    {
        UsbXtor::error(_pipe->getTransactor(), CarbonXUsbError_NoMemory,
                "Can't create token packet", this);
        return NULL;
    }

    UsbxTransUnit *transUnit = new UsbxTransUnit(this,
            tokenPacket, dataPacket, isLast);
    if (transUnit == NULL)
    {
        UsbXtor::error(_pipe->getTransactor(), CarbonXUsbError_NoMemory,
                "Can't create transcation unit", this);
        return NULL;
    }

    // Put retry and stall state in transaction unit
    transUnit->setRetryStallState(hasRetry(_transUnitIndex),
            hasStall(_transUnitIndex));
    _transUnitIndex++;

    return transUnit;
}

// Pushes next transaction unit for control input transfer
UsbxBool UsbxMessageTrans::pushNextTransUnit(UsbxTransUnit *transUnit)
{
    if (transUnit == NULL)
        return UsbxFalse;

    switch (_nextStage)
    {
        case UsbxMessageStage_SETUP: // Only for device
        {
            // Check
            if (transUnit->getTokenPacket()->getType() != UsbxPacket_SETUP)
                return UsbxFalse;

            UInt dataSize = transUnit->getDataPacket()->getPayloadSize();
            if (dataSize != 8) // For setup data size should be 8
            {
                // Protocol error
                UsbXtor::error(_pipe->getTransactor(),
                        CarbonXUsbError_ProtocolError,
                        "Data size is not 8 at setup stage of "
                        "a control transfer", this);
                return UsbxFalse;
            }

            // Copy the setup data
            const UsbxByte *dataBytes =
                transUnit->getDataPacket()->getPayloadData();
            for (UInt i = 0; i < 8; i++)
                _setupData[i] = dataBytes[i];

            if (getRequestLength() == 0) // For no data control transfer
                _nextStage = UsbxMessageStage_STATUS;
            else
                _nextStage = UsbxMessageStage_DATA;

            _pipe->setNextDataPacketType(UsbxPacket_DATA1);
            break;
        }
        case UsbxMessageStage_DATA:
        {
            // Check
            if (transUnit->getDirection() != getDirection())
            {
                // Protocol error
                UsbXtor::error(_pipe->getTransactor(),
                        CarbonXUsbError_ProtocolError,
                        "Direction mismatch at status stage of "
                        "a control transfer", this);
                return UsbxFalse;
            }

            // When last data unit has been sent
            if (transUnit->getDataPacket()->getPayloadSize() <
                    _pipe->getMaxPayloadSize())
                _nextStage = UsbxMessageStage_STATUS;

            // Copy the data
            UsbxBool status = pushDataForTransUnit(transUnit->getDataPacket());
            if (!status)
                return UsbxFalse;
            transUnit->setDataPacket(NULL);

            // When all data has been received
            if (_nextDataIndex == getRequestLength())
            {
                _nextStage = UsbxMessageStage_STATUS;
                _pipe->setNextDataPacketType(UsbxPacket_DATA1);
                createReceiveData();
            }
            break;
        }
        case UsbxMessageStage_STATUS:
        {
            // Check
            if (getRequestLength() == 0 &&
                    transUnit->getDirection() == CarbonXUsbPipeDirection_INPUT)
                ;
            else if (transUnit->getDirection() == getDirection())
            {
                // Protocol error
                UsbXtor::error(_pipe->getTransactor(),
                        CarbonXUsbError_ProtocolError,
                        "Direction mismatch at status stage of "
                        "a control transfer", this);
                return UsbxFalse;
            }

            // Zero length data packet as input means complete
            if (transUnit->getDataPacket()->getPayloadSize() == 0)
                transUnit->setLast();
            else
            {
                // Protocol error
                UsbXtor::error(_pipe->getTransactor(),
                        CarbonXUsbError_ProtocolError,
                        "Non-zero length data packet returned at status stage "
                        "of control transfer", this);
                return UsbxFalse;
            }
            break;
        }
        default: ASSERT(0, _pipe->getTransactor());
    }

    return UsbxTrue;
}

UInt UsbxMessageTrans::getNextTransUnitSizeInBytes() const
{
    // Size in bytes
    UInt tokenPacketSize = 3;
    UInt dataPacketSize = 0;
    UInt handshakePacketSize = 1;
    if (_currentTransUnit != NULL && _currentTransUnit->getDataPacket() != NULL)
        dataPacketSize = _currentTransUnit->getDataPacket()->getSizeInBytes();
    else
    {
        switch (_nextStage)
        {
            case UsbxMessageStage_SETUP:
            {
                dataPacketSize = 8;
                break;
            }
            case UsbxMessageStage_DATA:
            {
                if (getDirection() == CarbonXUsbPipeDirection_OUTPUT &&
                        _dataSize - _nextDataIndex < _pipe->getMaxPayloadSize())
                    dataPacketSize = _dataSize - _nextDataIndex;
                else
                    // Consider the maximum payload
                    dataPacketSize = _pipe->getMaxPayloadSize();
                break;
            }
            case UsbxMessageStage_STATUS:
            {
                dataPacketSize = 0;
                break;
            }
            default: ASSERT(0, _pipe->getTransactor());
        }
        dataPacketSize += 3; // 1 byte PID + 2 bytes CRC16
    }
    return tokenPacketSize + dataPacketSize + handshakePacketSize;
}

// Pops the next transaction unit for non-control transfer
UsbxTransUnit* UsbxStreamTrans::popNextTransUnit()
{
    CarbonUInt7 deviceAddress = _pipe->getDeviceAddress();
    CarbonUInt4 epNumber = _pipe->getEpNumber();

    UsbxTokenPacket *tokenPacket = NULL;
    UsbxDataPacket *dataPacket = NULL;
    // Create token packet as per direction
    if (_pipe->getDirection() == CarbonXUsbPipeDirection_OUTPUT)
    {
        tokenPacket = new UsbxOutPacket(deviceAddress, epNumber);
        if (_pipe->getTransactor()->isHost())
        {
            dataPacket = popDataForTransUnit();
            if (dataPacket == NULL) // No memory error come from inside
                return NULL;
        }
    }
    else // if (CarbonXUsbPipeDirection_INPUT)
    {
        tokenPacket = new UsbxInPacket(deviceAddress, epNumber);
        if (!_pipe->getTransactor()->isHost())
        {
            dataPacket = popDataForTransUnit();
            if (dataPacket == NULL) // No memory error come from inside
                return NULL;
        }
    }
    if (tokenPacket == NULL)
    {
        UsbXtor::error(_pipe->getTransactor(), CarbonXUsbError_NoMemory,
                "Can't create token packet", this);
        return NULL;
    }

    UsbxTransUnit *transUnit = new UsbxTransUnit(this,
            tokenPacket, dataPacket, (dataPacket != NULL) &&
            (dataPacket->getPayloadSize() < _pipe->getMaxPayloadSize()));
    if (transUnit == NULL)
    {
        UsbXtor::error(_pipe->getTransactor(), CarbonXUsbError_NoMemory,
                "Can't create transcation unit", this);
        return NULL;
    }

    // Put retry and stall state in transaction unit
    transUnit->setRetryStallState(hasRetry(_transUnitIndex),
            hasStall(_transUnitIndex));
    _transUnitIndex++;

    return transUnit;
}

// Pushes transaction unit for non-control transfer
UsbxBool UsbxStreamTrans::pushNextTransUnit(UsbxTransUnit *transUnit)
{
    if (transUnit == NULL)
        return UsbxFalse;

    // Check
    if (transUnit->getDirection() != _pipe->getDirection())
        return UsbxFalse;

    // For last transaction unit, set last
    if (transUnit->getDataPacket()->getPayloadSize() <
            _pipe->getMaxPayloadSize())
        transUnit->setLast();

    // Copy data
    UsbxBool status = pushDataForTransUnit(transUnit->getDataPacket());
    if (!status)
        return UsbxFalse;
    transUnit->setDataPacket(NULL);

    return UsbxTrue;
}

UInt UsbxStreamTrans::getNextTransUnitSizeInBytes() const
{
    // Size in bytes
    UInt tokenPacketSize = 3;
    UInt dataPacketSize = 0;
    UInt handshakePacketSize = 1;
    if (_currentTransUnit != NULL && _currentTransUnit->getDataPacket() != NULL)
        dataPacketSize = _currentTransUnit->getDataPacket()->getSizeInBytes();
    else if (_pipe->getDirection() == CarbonXUsbPipeDirection_OUTPUT &&
            _dataSize - _nextDataIndex < _pipe->getMaxPayloadSize())
        dataPacketSize = _dataSize - _nextDataIndex + 3;
    else
        // Consider the maximum payload + 1 byte PID + 2 bytes CRC16
        dataPacketSize = _pipe->getMaxPayloadSize() + 3;
    return tokenPacketSize + dataPacketSize + handshakePacketSize;
}

// High speed high bandwidth isochronous specific data packet generation
UsbxBool CarbonXUsbTransClass::setDataPacketTypeForHsRepeat()
{
    if (_pipe->getTransactor()->isFS() ||
            _pipe->getType() != CarbonXUsbPipe_ISO ||
            _pipe->getRepeatCount() == 0)
        return UsbxFalse;

    CarbonUInt32 maxPayloadSize = _pipe->getMaxPayloadSize();
    ASSERT(maxPayloadSize > 0, _pipe->getTransactor());
    UInt repeatCount = _pipe->getRepeatCount();
    UInt repeatIndex = (_nextDataIndex / maxPayloadSize) % (repeatCount + 1);
    if (repeatIndex == 0 && _dataSize > 0)
    {
        UInt packetsToSend = (_dataSize - _nextDataIndex) / maxPayloadSize;
        if (packetsToSend < repeatCount)
            repeatCount = packetsToSend;
    }

    UsbxPacketType dataPacketType = UsbxPacket_UNKNOWN;
    if (_pipe->getDirection() == CarbonXUsbPipeDirection_OUTPUT)
    {
        if (repeatIndex == repeatCount)
        {
            if (repeatCount == 0)
                dataPacketType = UsbxPacket_DATA0;
            else if (repeatCount == 1)
                dataPacketType = UsbxPacket_DATA1;
            else if (repeatCount == 2)
                dataPacketType = UsbxPacket_DATA2;
        }
        else
            dataPacketType = UsbxPacket_MDATA;
    }
    else
    {
        if (repeatIndex == repeatCount)
            dataPacketType = UsbxPacket_DATA0;
        else if (repeatIndex + 1 == repeatCount)
            dataPacketType = UsbxPacket_DATA1;
        else if (repeatIndex + 2 == repeatCount)
            dataPacketType = UsbxPacket_DATA2;
    }

    ASSERT(dataPacketType != UsbxPacket_UNKNOWN, _pipe->getTransactor());
    _pipe->setNextDataPacketType(dataPacketType);

    return UsbxTrue;
}
