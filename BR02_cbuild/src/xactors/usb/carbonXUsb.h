// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __carbonXUsb_h_
#define __carbonXUsb_h_

#ifndef __carbonXUsbConfig_h_
#include "xactors/usb/carbonXUsbConfig.h"
#endif

#ifndef __carbonXUsbPipe_h_
#include "xactors/usb/carbonXUsbPipe.h"
#endif

#ifndef __carbonXUsbTrans_h_
#include "xactors/usb/carbonXUsbTrans.h"
#endif

#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
  /*!
   * \brief Macro to allow both C and C++ compilers to use this include file in
   *        a type-safe manner.
   * \hideinitializer
   */
#define STRUCT struct
#endif

/*!
 * \defgroup Usb USB C-Interface
 * \brief C-Linkable Interface to the USB Transactor
 */

/*!
 * \addtogroup Usb
 * @{
 */

/*!
 * \file xactors/usb/carbonXUsb.h
 * \brief Definition of Carbon USB Transactor Object.
 *
 * This file provides definition of USB transactor object and related APIs.
 *
 * Transactor update semantics: Following sequence of operation will be
 * followed to run the transactor for each clock cycle
 *    -# Evaluate all transactors and the model. This should call
 *       carbonXUsbEvaluate().
 *    -# Call carbonXUsbRefreshInputs() and carbonXUsbRefreshOutputs()
 *       for every transactor to update the input and output connections
 *       between the model and the transactors.
 */

/*!
 * \brief Carbon USB Transactor Class */
STRUCT CarbonXUsbClass;

/*!
 * \brief Carbon USB Transactor Handle
 *
 * This type provides a handle or pointer to transactor object. All transactor
 * APIs need this handle to be passed as its argument.
 */
typedef STRUCT CarbonXUsbClass* CarbonXUsbID;

/*!
 * \brief Carbon USB transactor type
 *
 * This type specifies the transctor type i.e. host, device or OTG device.
 */
typedef enum {
    CarbonXUsb_Host,                    /*!< Host transactor      */
    CarbonXUsb_Device,                  /*!< Device transactor    */
    CarbonXUsb_OtgDevice                /*!< OTG Device transactor */
} CarbonXUsbType;

/*!
 * \brief Carbon USB transactor Control Command type
 *
 * This type provides the commands for transactor, which can used to control
 * transactor for different timing or modes. When such control command, started
 * by user, succeeds \a reportCommand() callback is called.
 */
typedef enum {
    CarbonXUsbCommand_DeviceAttach,    /*!< Attachment from PHY device only  */
    CarbonXUsbCommand_DeviceDetach,    /*!< Detachment from PHY device only  */
    CarbonXUsbCommand_Suspend,         /*!< Suspend from host                */
    CarbonXUsbCommand_Resume,          /*!< Resume started from host or
                                            remote wakeup started from device*/
    // OTG device specific commands
    CarbonXUsbCommand_OtgStartSession, /*!< Start a session from OTG device  */
    CarbonXUsbCommand_OtgStopSession,  /*!< Stop a session from OTG device   */
    CarbonXUsbCommand_OtgStartHnp,     /*!< HNP from a OTG A device          */
    CarbonXUsbCommand_OtgStopHnp       /*!< HNP stopped from a OTG B device  */
} CarbonXUsbCommand;

/*!
 * \brief Carbon USB transactor error notification type
 *
 * This type provides the error type that can be signaled by the transactor
 * for any unmanagable case, that it might see.
 */
typedef enum {
    CarbonXUsbError_NoError = 0,        /*!< No error                     */
    CarbonXUsbError_NoMemory,           /*!< Memory allocation error      */
    CarbonXUsbError_ConnectionError,    /*!< Carbon model connection error         */
    CarbonXUsbError_ProtocolError,      /*!< Protocol violation error     */
    CarbonXUsbError_DeviceFull,         /*!< All 127 devices are attached */
    CarbonXUsbError_NoBandwidth,        /*!< No bandwidth is available    */
    CarbonXUsbError_NoPeriodicOutTrans, /*!< Periodic OUT pipe is empty   */
    CarbonXUsbError_InvalidDescriptor,  /*!< Invalid descriptor specified */
    CarbonXUsbError_InvalidTrans,       /*!< Invalid transaction pushed   */
    CarbonXUsbError_InternalError       /*!< Internal code error          */
} CarbonXUsbErrorType;

/*!
 * \brief Creates a transactor Object.
 *
 * \param transactorName     Specifies transactor name.
 *
 * \param transactorType     Specifies transactor type i.e. Host, Device or OTG
 *                           Device.
 *
 * \param config             Specifies transactor configuration structure to
 *                           create and initialize the transactor. This
 *                           structure need to be populated beforehand by
 *                           configuration APIs. Transactor will copy this
 *                           configuration object. For USB host, if NULL is
 *                           passed, default values of configuration parameters
 *                           will be considered. For USB device, it is mandatory
 *                           to set this with proper Device Descriptor.
 *
 * \param reportTransaction  Callback to indicate completion of transaction to
 *                           host transactor. Not called for device.
 *
 * \param setDefaultResponse Callback to indicate device when an incoming
 *                           transaction is being received. Device needs to
 *                           provide the retry and stall states (sometimes data
 *                           also) here. Not called for host.
 *                           This callback will be used to provide read data of
 *                           Interrupt IN and Isochronous IN transfers,
 *                           initiated by the host automatically from inside.
 *                           User will NOT be responsible to free the
 *                           transaction memory.
 *
 * \param setResponse        This callback will also be used when a OUT
 *                           transaction needs to be responded by the device.
 *                           User need retrieve data at this stage.
 *                           This callback is also relevant for host, where
 *                           host internally starts transactions e.g. periodic
 *                           IN transactions and transactions during bus
 *                           enumeration. In such cases, host transactor calls
 *                           this callback instead of reportTransaction callback
 *                           and user is NOT supposed to free up the transaction
 *                           memory.
 *
 * \param refreshResponse    Ignored.
 *
 * \param reportCommand      Callback to indicate completion of command to
 *                           host or device transactor.
 *
 * \param notify             Callback when error happened for particular
 *                           transaction.
 *
 * \param notify2            Callback when error happened at any other
 *                           circumstance.
 *
 * \param stimulusInstance   Some user data that the above callbacks will
 *                           provide as argument. Through this user can register
 *                           the reference to the test environment (containing
 *                           the transactor), which can be used during different
 *                           callbacks.
 *
 * \returns                  Created transactor handle.
 */
CarbonXUsbID carbonXUsbCreate(const char* transactorName,
        CarbonXUsbType transactorType, const CarbonXUsbConfig *config,
        void (*reportTransaction)(CarbonXUsbTrans*, void*),
        void (*setDefaultResponse)(CarbonXUsbTrans*, void*),
        void (*setResponse)(CarbonXUsbTrans*, void*),
        void (*refreshResponse)(CarbonXUsbTrans*, void*),
        void (*reportCommand)(CarbonXUsbCommand, void*),
        void (*notify)(CarbonXUsbErrorType, const char*,
            const CarbonXUsbTrans*, void*),
        void (*notify2)(CarbonXUsbErrorType, const char*, void*),
        void *stimulusInstance);

/*!
 * \brief Destroys a transactor object
 *
 * \param xtor Transactor object.
 *
 * \retval 1 For successful destruction.
 * \retval 0 For failure in destruction.
 */
CarbonUInt32 carbonXUsbDestroy(const CarbonXUsbID xtor);

/*!
 * \brief This API retrieves transactor name
 *
 * \param xtor Transactor object.
 *
 * \returns Name of the transactor.
 */
const char* carbonXUsbGetName(const CarbonXUsbID xtor);

/*!
 * \brief API Gets transactor type
 *
 * This API Gets transactor type i.e. Host, Device or OTG Device.
 *
 * \param xtor Transactor object.
 *
 * \returns Type of the transactor.
 */
CarbonXUsbType carbonXUsbGetType(const CarbonXUsbID xtor);

/*!
 * \brief API Gets current frame number
 *
 * This API Gets current frame number tracking the SOF packet in USB
 *
 * \param xtor Transactor object.
 *
 * \returns Frame number of SOF packet
 */
CarbonUInt11 carbonXUsbGetFrameNumber(const CarbonXUsbID xtor);

/*!
 * \brief Starts a command on a transactor
 *
 * This API will start a command on a transactor. Note that, the command
 * will be applied if relevant, instantaneously and will not be queued.
 *
 * \param xtor Transactor object.
 * \param command Command to the transactor.
 *
 * \retval 1 If the command is relevant and successfully applied.
 * \retval 0 Otherwise.
 */
CarbonUInt32 carbonXUsbStartCommand(CarbonXUsbID xtor,
        CarbonXUsbCommand command);

/*!
 * \brief Starts enumeration for a newly attached device
 *
 * This API will start enumeration for a newly attached device. This involves
 * setting a unique device address to the attached device. Relevant for host.
 *
 * \param xtor Transactor object.
 *
 * \retval 1 For successful enumeration.
 * \retval 0 For failure in enumeration.
 */
CarbonUInt32 carbonXUsbStartBusEnumeration(CarbonXUsbID xtor);

/*!
 * \brief This API will create a new pipe, with endpoint descriptor.
 *
 * \param xtor Transactor object.
 * \param deviceAddress Device address.
 * \param epDescriptor Pointer to the endpoint descriptor.
 *
 * \returns Type of the pipe object.
 */
CarbonXUsbPipe* carbonXUsbCreatePipe(CarbonXUsbID xtor,
        CarbonUInt7 deviceAddress, const CarbonUInt8 *epDescriptor);

/*!
 * \brief This API will destroy a pipe.
 *
 * \param xtor Transactor object.
 * \param pipe Pointer to the pipe object.
 *
 * \retval 1 For success.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXUsbDestroyPipe(CarbonXUsbID xtor, CarbonXUsbPipe *pipe);

/*!
 * \brief Gets the number of transactor's currently active pipes.
 *
 * This API retrieves total number transactor's currently active pipes.
 * Before any device attachment, this is ONE for the Root Hub's status
 * change pipe. (It shall ignore Root Hub's default control pipe).
 *
 * \param xtor Transactor object.
 *
 * \returns Number of currently active pipes.
 */
CarbonUInt32 carbonXUsbGetCurrentPipeCount(const CarbonXUsbID xtor);

/*!
 * \brief Gets a list of transactor's current pipes.
 *
 * This API retrieves a list of transactor's current pipes. The
 * returned list is read-only and will be of size as provided by
 * carbonXUsbGetCurrentPipeCount API call, and will change for new
 * device attach/de-attach and SetConfiguration control transfers.
 * The memory ownership is to the transactor.
 *
 * \param xtor Transactor object.
 *
 * \returns List of pipes.
 */
CarbonXUsbPipe * const *carbonXUsbGetCurrentPipes(CarbonXUsbID xtor);

/*!
 * \brief Pushes a transaction in appropriate transaction table
 *
 * This API will push a transaction object in the appropriate
 * transaction table of a transactor.
 *
 * \param xtor Transactor object.
 * \param trans Pointer to the transaction object.
 *
 * \retval 1 If the transaction is successfully added to the input buffer.
 * \retval 0 Otherwise.
 */
CarbonUInt32 carbonXUsbStartNewTransaction(CarbonXUsbID xtor,
        CarbonXUsbTrans *trans);

/*!
 * \brief This API will connect the transactor signal and DUT signal pairs by
 *        name
 *
 * When connecting the transactor signal and DUT signal, end of nameList will be
 * indicated by putting NULL as transactor port name in the last structure item.
 *
 * \param xtor Transactor object.
 * \param nameList Transactor to model signal mapping list. Note, last element
 *                 in the list will have \a TransactorName field as NULL.
 * \param carbonModelName Carbon model name.
 * \param carbonModelHandle Carbon model handle.
 *
 * \retval 1 For successful connection.
 * \retval 0 For failure in connection.
 */
CarbonUInt32 carbonXUsbConnectByModelSignalName(
        CarbonXUsbID xtor,
        CarbonXInterconnectNameNamePair *nameList,
        const char *carbonModelName, CarbonObjectID *carbonModelHandle);

/*!
 * \brief Connects transactor signals by name to Carbon model signals by name.
 *
 * This API will connect the transactor signal and DUT signal pairs by Carbon model
 * net handle. When connecting the transactor signal and DUT signal, end of
 * nameList will be indicated by putting NULL as transactor port name in the
 * last structure item.
 *
 * \param xtor Transactor object.
 * \param nameList Transactor to model signal mapping list. Note, last element
 *                 in the list will have \a TransactorName field as NULL.
 * \param carbonModelName Carbon model name.
 * \param carbonModelHandle Carbon model handle.
 *
 * \retval 1 For successful connection.
 * \retval 0 For failure in connection.
 */
CarbonUInt32 carbonXUsbConnectByModelSignalHandle(
        CarbonXUsbID xtor,
        CarbonXInterconnectNameHandlePair *nameList,
        const char *carbonModelName, CarbonObjectID *carbonModelHandle);

/*!
 * \brief Connects transactor signals by name to Carbon model signals by
 *        input/output change callbacks.
 *
 * This API will connect the transactor signal and DUT signal pairs by
 * model net i/o callbacks. When connecting the transactor signal and DUT
 * signal, end of nameList will be indicated by putting NULL as transactor
 * port name in the last structure item.
 *
 * \param xtor Transactor object.
 * \param nameList Transactor to model signal mapping list. Note, last element
 *                 in the list will have \a TransactorName field as NULL.
 *
 * \retval 1 For successful connection.
 * \retval 0 For failure in connection.
 */
CarbonUInt32 carbonXUsbConnectToLocalSignalMethods(
        CarbonXUsbID xtor,
        CarbonXInterconnectNameCallbackTrio *nameList);

/*!
 * \brief This API will dump the transactor state into console.
 *
 * \param xtor Transactor object.
 */
void carbonXUsbDump(const CarbonXUsbID xtor);

/*!
 * \brief Evaluates transactor.
 *
 * This API will be used to evaluate and update the transactor. At time of
 * evaluation the read and write on transactor ports will check or update port
 * values which will be refreshed with respect to Carbon model.
 *
 * \param xtor Transactor object.
 * \param currentTime Current time at which evaluation is happening
 *
 * \retval 1 For successful evaluation.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXUsbEvaluate(CarbonXUsbID xtor,
        CarbonUInt64 currentTime);

/*!
 * \brief Refreshes transactor input signals.
 *
 * This API will be used to refresh the transactor's input connection with
 * Carbon model, i.e. the input ports will be updated with Carbon model net
 * value at this time.
 *
 * \param xtor Transactor object.
 * \param currentTime Current time at which evaluation is happening
 *
 * \retval 1 For successful refresh.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXUsbRefreshInputs(CarbonXUsbID xtor,
        CarbonUInt64 currentTime);

/*!
 * \brief Refreshes transactor output signals.
 *
 * This API will be used to refresh the transactor's output connection with
 * Carbon model, i.e. the output port values will be be used to update the
 * Carbon model nets at this time.
 *
 * \param xtor Transactor object.
 * \param currentTime Current time at which evaluation is happening.
 *
 * \retval 1 For successful refresh.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXUsbRefreshOutputs(CarbonXUsbID xtor,
        CarbonUInt64 currentTime);

/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif
