// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __carbonXUsbPipe_h_
#define __carbonXUsbPipe_h_

#ifndef __carbonX_h_
#include "xactors/carbonX.h"
#endif

#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
  /*!
   * \brief Macro to allow both C and C++ compilers to use this include file in
   *        a type-safe manner.
   * \hideinitializer
   */
#define STRUCT struct
#endif

/*!
 * \addtogroup Usb
 * @{
 */

/*!
 * \file xactors/usb/carbonXUsbPipe.h
 * \brief Definition of Carbon USB Pipe Object.
 *
 * This file provides definition of pipe object.
 */

/*! Forward declaration */
STRUCT CarbonXUsbPipeClass;

/*!
 * \brief Carbon USB transaction pipe object declaration.
 *
 * Transaction pipe object. It majorly includes pipe type,
 * Device address, Endpoint number and direction.
 */
typedef STRUCT CarbonXUsbPipeClass CarbonXUsbPipe;

/*!
 * \brief Carbon USB transfer pipe type
 */
typedef enum {
    CarbonXUsbPipe_CONTROL = 0,     /*!< Control transfer Pipe     */
    CarbonXUsbPipe_ISO     = 1,     /*!< Isochronous transfer Pipe */
    CarbonXUsbPipe_BULK    = 2,     /*!< Bulk transfer Pipe        */
    CarbonXUsbPipe_INTR    = 3      /*!< Interrupt transfer Pipe   */
} CarbonXUsbPipeType;

/*!
 * \brief USB pipe direction.
 *
 * This specifies possible USB pipe directions with respect to Host.
 */
typedef enum {
    CarbonXUsbPipeDirection_OUTPUT = 0, /*!< Direction of an output pipe */
    CarbonXUsbPipeDirection_INPUT  = 1, /*!< Direction of an input pipe */
    CarbonXUsbPipeDirection_UNDEF  = 2  /*!< Undefined direction for
                                             control pipe */
} CarbonXUsbPipeDirection;


/*!
 * \brief Gets the pipe type.
 *
 * This API will provide the pipe type.
 *
 * \param pipe Pointer to the pipe object.
 *
 * \returns Type of the pipe object.
 */
CarbonXUsbPipeType carbonXUsbPipeGetType(const CarbonXUsbPipe* pipe);

/*!
 * \brief Gets the device address form a pipe
 *
 * This API will provide the device address for a pipe - 1 to 127.
 * '0'’address is reserved for newly attached devices which is not yet
 * configured.
 *
 * \param pipe Pointer to the pipe object.
 *
 * \returns Address of the device with which the pipe is established.
 */
CarbonUInt7 carbonXUsbPipeGetDeviceAddress(const CarbonXUsbPipe* pipe);

/*!
 * \brief Gets the pipe's Endpoint Number
 *
 * This API will provide the pipe's EP number - 0 to 15. '0' is always
 * for default control pipe.
 *
 * \param pipe Pointer to the pipe object.
 *
 * \returns EP number of a pipe.
 */
CarbonUInt4 carbonXUsbPipeGetEpNumber(const CarbonXUsbPipe* pipe);

/*!
 * \brief Gets the pipe direction of a pipe.
 *
 * This API will provide the pipe's EP direction i.e. IN or OUT.
 *
 * \param pipe Pointer to the pipe object.
 *
 * \returns Pipe direction.
 */
CarbonXUsbPipeDirection carbonXUsbPipeGetDirection(const CarbonXUsbPipe* pipe);

/*!
 * \brief Gets the maximum data payload size of a pipe.
 *
 * This API will provide the pipe maximum data payload size, which is
 * related to wMaxPacketSize of the EP descriptor.
 *
 * \param pipe Pointer to the pipe object.
 *
 * \returns Maximum data payload size.
 */
CarbonUInt32 carbonXUsbPipeGetMaxPayloadSize(const CarbonXUsbPipe* pipe);

/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif
