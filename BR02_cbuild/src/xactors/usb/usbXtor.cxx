/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    usbXtor.cpp
    Purpose: This file contains the definition of the base class of
             usb transactor.
*/

#include <stdlib.h>
#include "usbXDescriptors.h"
#include "usbXTrans.h"
#include "usbXUtmiIntf.h"
#include "usbXUlpiIntf.h"
#include "usbXScheduler.h"
#include "usbXHostFsm.h"
#include "usbXDeviceFsm.h"
#include "usbXOtgFsm.h"
#include "usbXtor.h"

// #define DEBUG
#define OTG_SAVE_DEVICE_DEFAULT_CONTROL_PIPE

// Static member initialization
#ifdef _VCD
FILE* CarbonXUsbClass::_vcdFile = NULL;
UsbxBool CarbonXUsbClass::_vcdDumpStarted = UsbxFalse;
CarbonTime CarbonXUsbClass::_vcdLastTime = 0;
#endif

// Constructor
CarbonXUsbClass::CarbonXUsbClass(UsbxConstString name,
        const CarbonXUsbConfig *config):
    _name(UsbxStrdup(name)), _isFS(UsbxTrue), _config(NULL),
    // initialization of building units
    _pipeList(new UsbxArray<UsbxPipe>), _fsm(NULL), _intf(NULL),
    _error(CarbonXUsbError_NoError), _errorMessage(NULL),
    _errorCallback(NULL), _userContext(NULL)
{
    // Copy the name
    if (_name == NULL)
    {
        errorForNoMemory("transactor name");
        return;
    }

    // Create default configuration
    if (config == NULL)
        _config = new CarbonXUsbConfig();
    else
        _config = new CarbonXUsbConfig(*config);
    if (_config == NULL)
    {
        errorForNoMemory("transactor configuration");
        return;
    }

    // Create building units - do it after configuration
    // Check pipe list and scheduler
    if (_pipeList == NULL)
    {
        errorForNoMemory("transactor pipes");
        return;
    }

    // Create pin level interface
#ifdef _VCD
    _vcdModuleName = NULL;
    if (_vcdFile)
    {
        UsbxConstString intfStr;
        if (_config->getIntfType() == CarbonXUsbInterface_UTMI)
        {
            if (_config->getIntfSide() == CarbonXUsbIntfSide_PHY)
                intfStr = "UTMI:PHY";
            else // if (CarbonXUsbIntfSide_Link)
                intfStr = "UTMI:LINK";
        }
        else // if (CarbonXUsbInterface_ULPI)
        {
            if (_config->getIntfSide() == CarbonXUsbIntfSide_PHY)
                intfStr = "ULPI:PHY";
            else // if (CarbonXUsbIntfSide_Link)
                intfStr = "ULPI:LINK";
        }
        _vcdModuleName = UsbxStrdup(_name);
        for (UInt i = 0; _vcdModuleName[i] != 0; i++)
        {
            if (_vcdModuleName[i] == ' ')
                _vcdModuleName[i] = '_';
        }
        fprintf(_vcdFile, "$scope module %s(%s) $end\n",
                _vcdModuleName, intfStr);
    }
#endif
    if (_config->getIntfType() == CarbonXUsbInterface_UTMI)
    {
        if (_config->getIntfSide() == CarbonXUsbIntfSide_PHY)
            _intf = new UsbxUtmiPhyIntf(this, _config->getUtmiDataBusWidth(),
                    _config->getUtmiPinLevel());
        else // if (CarbonXUsbIntfSide_Link)
            _intf = new UsbxUtmiLinkIntf(this, _config->getUtmiDataBusWidth(),
                    _config->getUtmiPinLevel());
    }
    else if (_config->getIntfType() == CarbonXUsbInterface_ULPI)
    {
        if (_config->getIntfSide() == CarbonXUsbIntfSide_PHY)
            _intf = new UsbxUlpiPhyIntf(this);
        else // if (CarbonXUsbIntfSide_Link)
            _intf = new UsbxUlpiLinkIntf(this);
        // ULPI uses UTMI 8 bit bus, irrespective of the configuration
        _config->setUtmiDataBusWidth(CarbonXUsbUtmiDataBusWidth_8);
    }
    else // Unsupported interface type
    {
        ASSERT(_intf, this);
    }

#ifdef _VCD
    if (_vcdFile)
        fprintf(_vcdFile, "$upscope $end\n");
#endif
    if (_intf == NULL)
    {
        errorForNoMemory("transactor pin level interface");
        return;
    }
    _intf->setTimingFactor(_config->getTimingFactor());
}

CarbonXUsbClass::~CarbonXUsbClass()
{
    DELETE(_name);
    delete _config;
    // delete building units
    UsbxPipe * const *pipes = _pipeList->getArray();
    for (UInt i = 0; i < _pipeList->getCount(); i++)
        delete pipes[i];
    delete _pipeList;
    delete _intf;
#ifdef _VCD
    if (_vcdModuleName)
        DELETE(_vcdModuleName);
    if (_vcdFile)
        fclose(_vcdFile);
    _vcdFile = NULL;
    _vcdDumpStarted = UsbxFalse;
    _vcdLastTime = 0;
#endif
}

void CarbonXUsbClass::errorForNoMemory(UsbxConstString objectName)
{
    ASSERT(objectName, this);
    _error = CarbonXUsbError_NoMemory;
    static char errorMessage[1024];
    sprintf(errorMessage, "Can't allocate memory for %s.", objectName);
    _errorMessage = errorMessage;
}

// Get pin pointer from pin name
UsbxPin* CarbonXUsbClass::getPinByName(UsbxConstString netName)
{
    return _intf->getPin(netName);
}

// Evaluate FSMs
UsbxBool CarbonXUsbClass::evaluate(CarbonTime currentTime)
{
    UsbxBool retValue = UsbxTrue;
    retValue &= _fsm->evaluate(currentTime);
    retValue &= _intf->evaluate(currentTime);
    return retValue;
}

// Refresh channels
UsbxBool CarbonXUsbClass::refreshInputs(CarbonTime currentTime)
{
#ifdef _VCD
    dumpVcdTime(currentTime);
#endif
    return _intf->refreshInputs(currentTime);
}

// Refresh channels
UsbxBool CarbonXUsbClass::refreshOutputs(CarbonTime currentTime)
{
#ifdef _VCD
    dumpVcdTime(currentTime);
#endif
    return _intf->refreshOutputs(currentTime);
}

// Register callbacks
void CarbonXUsbClass::registerCallbacks(void *userContext,
        void (*reportTransaction)(CarbonXUsbTrans*, void*),
        void (*setDefaultResponse)(CarbonXUsbTrans*, void*),
        void (*setResponse)(CarbonXUsbTrans*, void*),
        void (*reportCommand)(CarbonXUsbCommand, void*),
        void (*errorTransCallback)(CarbonXUsbErrorType, const char*,
            const CarbonXUsbTrans*, void*),
        void (*errorCallback)(CarbonXUsbErrorType, const char*, void*))
{
    _userContext = userContext;
    _errorTransCallback = errorTransCallback;
    _errorCallback = errorCallback;

    _setDefaultResponse = setDefaultResponse;
    _setResponse = setResponse;
    _reportTransaction = reportTransaction;
    _reportCommand = reportCommand;
}

// Callback wrappers
void CarbonXUsbClass::setDefaultResponse(CarbonXUsbTrans *trans)
{
    if (processTrans(trans, UsbxTrue))
        return;
    if (_setDefaultResponse)
        (*_setDefaultResponse)(trans, _userContext);
}

void CarbonXUsbClass::setResponse(CarbonXUsbTrans *trans)
{
    if (processTrans(trans))
        return;
    if (_setResponse)
        (*_setResponse)(trans, _userContext);
}

void CarbonXUsbClass::reportTransaction(CarbonXUsbTrans *trans)
{
    CarbonUInt7 transDeviceAddress = trans->getPipe()->getDeviceAddress();
    if (processTrans(trans) && transDeviceAddress == 0)
    {
        if (_setResponse)
            (*_setResponse)(trans, _userContext);
        delete trans;
        return;
    }
    if (_reportTransaction)
        (*_reportTransaction)(trans, _userContext);
}

void CarbonXUsbClass::reportCommand(CarbonXUsbCommand command)
{
    if (_reportCommand)
        (*_reportCommand)(command, _userContext);
}

// Error callback wrapper
void CarbonXUsbClass::error(const CarbonXUsbClass *xtor,
        CarbonXUsbErrorType errorType, UsbxConstString errorMessage,
        const CarbonXUsbTrans *trans)
{
    if (xtor != NULL)
    {
        // if transaction for which error happened is available and
        // notify callback is registered
        if (trans != NULL && xtor->_errorTransCallback != NULL)
            (*(xtor->_errorTransCallback))(errorType, errorMessage,
                                        trans, xtor->_userContext);
        // if notify2 callback is registered
        else if (xtor->_errorCallback != NULL)
            (*(xtor->_errorCallback))(errorType, errorMessage,
                                      xtor->_userContext);
        // when notify and notify2 callbacks are not available
        // show the error message in console
        else if (xtor->_name != NULL)
        {
            printf("CarbonXUsb: ERROR[%u]: Transactor %s: %s\n",
                    (UInt) errorType, xtor->_name, errorMessage);
            exit(1);
        }
        else
        {
            printf("CarbonXUsb: ERROR[%u]: %s\n",
                    (UInt) errorType, errorMessage);
            exit(1);
        }
    }
    else
    {
        printf("CarbonXUsb: ERROR[%u]: %s\n", (UInt) errorType, errorMessage);
        exit(1);
    }
}

void CarbonXUsbClass::setSpeedFS(UsbxBool isFS, CarbonTime currentTime)
{
    _isFS = isFS;
    if (_intf)
        _intf->setSpeedFS(_isFS, currentTime);
}

// Create pipe
UsbxPipe* CarbonXUsbClass::createPipe(CarbonUInt7 deviceAddress,
        const UsbxByte *epDescriptor)
{
    UsbxPipe *pipe = new UsbxPipe(this, deviceAddress, epDescriptor);
    if (pipe == NULL) // Memory full error message is given internally
        return NULL;

    // Put the pipe in transactor's pipe list
    _pipeList->add(pipe);
    return pipe;
}

UsbxBool CarbonXUsbClass::destroyPipe(UsbxPipe *pipe)
{
    if (_pipeList == NULL || pipe == NULL)
        return UsbxFalse;

    // Remove the pipe from transactor's pipe list and free the memory
    _pipeList->remove(pipe);
    delete pipe;

    return UsbxTrue;
}

// Find a pipe
UsbxPipe* CarbonXUsbClass::getPipe(CarbonUInt4 epNumber,
        CarbonXUsbPipeDirection epDir)
{
    UsbxPipe * const *pipes = _pipeList->getArray();
    for (UInt i = 0; i < _pipeList->getCount(); i++)
    {
        if (pipes[i]->getEpNumber() == epNumber &&
                (pipes[i]->getDirection() == CarbonXUsbPipeDirection_UNDEF ||
                  pipes[i]->getDirection() == epDir))
            return pipes[i];
    }
    return NULL;
}

// Debug routine
void CarbonXUsbClass::print() const
{
    UsbxConstString typeStr = NULL;
    if (getType() == CarbonXUsb_Host)
        typeStr = "Host";
    else if (getType() == CarbonXUsb_Device)
        typeStr = "Device";
    else // CarbonXUsb_OtgDevice
        typeStr = "OTG Device";
    printf("USB %s Transactor: %s\n", typeStr, _name);
    printf("Pin interface: %s %s\n",
        _config->getIntfType() == CarbonXUsbInterface_UTMI ? "UTMI" : "ULPI",
        _config->getIntfSide() == CarbonXUsbIntfSide_PHY ? "PHY" : "LINK");
    printf("Speed capacity: %s, Current operating speed: %s\n",
        _config->getSpeed() == CarbonXUsbSpeed_FS ? "FS" : "HS",
        _isFS ? "FS" : "HS");
    // Print pipes
    printf("Active pipes:\n");
    UsbxPipe * const *pipes = getPipes();
    for (UInt i = 0; i < getPipeCount(); i++)
    {
        printf("%d: ", i + 1);
        pipes[i]->print();
    }
}

// Definition of UsbxAssertFail used as a replacement of assert()
UInt UsbxAssertFail(const UsbXtor *xtor, UsbxConstString failExpr,
        UsbxConstString file, UInt line)
{
    UsbxString msg = UsbxGetStringBuffer();
    sprintf(msg, "INTERNAL ERROR - assertion failed on %s at file %s line %u.",
            failExpr, file, line);
    CarbonXUsbClass::error(xtor, CarbonXUsbError_InternalError, msg);
    return 1;
}

#ifdef _VCD
void CarbonXUsbClass::dumpVcdTime(CarbonTime currentTime)
{
    if (_vcdFile == NULL)
        return;
    if (!_vcdDumpStarted)
    {
        _vcdDumpStarted = UsbxTrue;
        fprintf(_vcdFile, "$enddefinitions $end\n");
        fprintf(_vcdFile, "#0\n");
        fprintf(_vcdFile, "0Clock\n");
    }
    if (currentTime != _vcdLastTime)
    {
        // toggle the clock and put time stamp
        fprintf(_vcdFile, "#%u\n", static_cast<UInt>(_vcdLastTime +
                    (currentTime - _vcdLastTime) / 2));
        fprintf(_vcdFile, "0Clock\n");
        fprintf(_vcdFile, "#%u\n", static_cast<UInt>(currentTime));
        fprintf(_vcdFile, "1Clock\n");
        _vcdLastTime = currentTime;
    }
}

UsbxBool CarbonXUsbClass::enableVcdDump(UsbxConstString fileName)
{
    if (fileName == NULL)
        fileName = "carbonXUsb.vcd";
    if (_vcdFile)
        fclose(_vcdFile);
    _vcdFile = OSFOpen(fileName, "w",NULL);
    if (_vcdFile == NULL)
        return UsbxFalse;
    fprintf(_vcdFile, "$date $end\n");
    fprintf(_vcdFile, "$version Carbon USB Transactor $end\n");
    fprintf(_vcdFile, "$timescale 1ns $end\n");
    fprintf(_vcdFile, "$var wire 1 Clock Clock $end\n");
    _vcdDumpStarted = UsbxFalse;
    _vcdLastTime = 0;
    return UsbxTrue;
}
#endif

// ****************************************************
// ************** USB HOST IMPLEMENTATION *************
// ****************************************************

UsbxHostXtor::UsbxHostXtor(UsbxConstString name,
        const CarbonXUsbConfig *config): CarbonXUsbClass(name, config),
        _hostFsm(NULL), _scheduler(NULL)
{
    _scheduler = new UsbxScheduler(this, (config == NULL) ?
            CarbonXUsbUtmiDataBusWidth_8 : config->getUtmiDataBusWidth());
    if (_scheduler == NULL)
    {
        errorForNoMemory("transactor scheduler");
        return;
    }
    initAddressTable();
    // Create FSM unit
    _fsm = _hostFsm = new UsbxHostFsm(this);
    if (_fsm == NULL)
    {
        errorForNoMemory("transactor finite state machine (FSM)");
        return;
    }
}

UsbxHostXtor::~UsbxHostXtor()
{
    delete _scheduler;
    delete _hostFsm;

    // Delete transactions created internally by device as response
    UsbxPipe * const *pipes = getPipes();
    for (UInt i = 0; i < getPipeCount(); i++)
    {
        if (pipes[i]->isPeriodic() &&
                pipes[i]->getDirection() == CarbonXUsbPipeDirection_INPUT)
        {
            UsbxQueue<UsbxTrans> *transQueue = pipes[i]->getTransQueue();
            while (!transQueue->isEmpty())
                delete transQueue->pop();
            // Note, do not delete the queue here - it will be deleted from pipe
        }
    }
}

void UsbxHostXtor::initAddressTable()
{
    for (UInt i = 0; i < 127; i++)
        _deviceAddressTable[i] = UsbxFalse;
}

CarbonUInt7 UsbxHostXtor::getNewDeviceAddress()
{
    UInt deviceAddress;
    for (deviceAddress = 1; deviceAddress <= 127; deviceAddress++)
    {
        if (!_deviceAddressTable[deviceAddress - 1])
        {
            _deviceAddressTable[deviceAddress - 1] = UsbxTrue;
            return deviceAddress;
        }
    }
    return 0;
}

UsbxPipe* UsbxHostXtor::createPipe(CarbonUInt7 deviceAddress,
        const UsbxByte *epDescriptor)
{
    UsbxPipe *pipe = new UsbxPipe(this, deviceAddress, epDescriptor);
    if (pipe == NULL) // Memory full error message is given internally
        return NULL;

    // Check for interrupt or isochronous pipe in host for bandwidth availablity
    if (pipe->isPeriodic())
    {
        UsbxScheduler *scheduler = getScheduler();
        ASSERT(scheduler, this);
        if (!scheduler->updatePeriodicPolicy(pipe, UsbxTrue))
        {
            error(this, CarbonXUsbError_NoBandwidth,
                    "No bandwidth is available for desired pipe");
            return NULL;
        }
    }

    // Put the pipe in transactor's pipe list
    _pipeList->add(pipe);
    return pipe;
}

UsbxBool UsbxHostXtor::destroyPipe(UsbxPipe *pipe)
{
    if (_pipeList == NULL || pipe == NULL)
        return UsbxFalse;

    if (pipe->isPeriodic())
    {
        UsbxScheduler *scheduler = getScheduler();
        ASSERT(scheduler, this);
        ASSERT(scheduler->updatePeriodicPolicy(pipe, UsbxFalse), this);
    }

    // Free device address, if this is the only pipe of a device
    UInt i;
    for (i = 0; i < _pipeList->getCount(); i++)
    {
        UsbxPipe *iterPipe = _pipeList->getArray()[i];
        if (iterPipe == pipe)
            continue;
        if (iterPipe->getDeviceAddress() == pipe->getDeviceAddress())
            break;
    }
    if (i == _pipeList->getCount())
        freeDeviceAddress(pipe->getDeviceAddress());

    // Remove the pipe from transactor's pipe list and free the memory
    _pipeList->remove(pipe);
    delete pipe;

    return UsbxTrue;
}

UsbxBool UsbxHostXtor::startBusEnumeration()
{
    // Check address table
    CarbonUInt7 addressAlloted = getNewDeviceAddress();
    if (addressAlloted == 0)
    {
        UsbXtor::error(this, CarbonXUsbError_DeviceFull,
                "No more device can be attached");
        return UsbxFalse;
    }

    // Create default control pipe for newly attached device
    UsbxPipe *defaultControlPipe = createPipe(0, NULL);
    if (defaultControlPipe == NULL)
        return UsbxFalse;

    // Start GET_DESCRIPTOR(DeviceDescriptor, 8 Bytes)
    UsbxMessageTrans *trans = new UsbxMessageTrans(defaultControlPipe);
    if (trans == NULL)
    {
        UsbXtor::error(this, CarbonXUsbError_NoMemory,
                "Can't create post-attachment GET_DESCRIPTOR transaction");
        return UsbxFalse;
    }
    trans->setRequestType(0x80);
    trans->setRequest(6);
    trans->setRequestValue(0x0100);
    trans->setRequestLength(8);
    if (!startNewTransaction(trans))
        return UsbxFalse;

    // Start SET_ADDRESS
    trans = new UsbxMessageTrans(defaultControlPipe);
    if (trans == NULL)
    {
        UsbXtor::error(this, CarbonXUsbError_NoMemory,
                "Can't create post-attachment SET_ADDRESS transaction");
        return UsbxFalse;
    }
    trans->setRequestType(0x00);
    trans->setRequest(5);
    trans->setRequestValue(addressAlloted);
    if (!startNewTransaction(trans))
        return UsbxFalse;

    return UsbxTrue;
}

// Start new transaction
UsbxBool UsbxHostXtor::startNewTransaction(UsbxTrans *trans)
{
    if (trans == NULL)
        return UsbxFalse;
    UsbxPipe *pipe = trans->getPipe();
    if (pipe == NULL || pipe->getTransQueue() == NULL || (pipe->isPeriodic() &&
            pipe->getDirection() == CarbonXUsbPipeDirection_INPUT))
        return UsbxFalse;
    trans->getPipe()->getTransQueue()->push(trans);
    return UsbxTrue;
}

UsbxBool UsbxHostXtor::processTrans(UsbxTrans *trans, UsbxBool defaultResp)
{
    ASSERT(trans, this);
    // If transaction is not targetted to default control pipe, ignore
    if (trans->getType() != CarbonXUsbPipe_CONTROL ||
            trans->getPipe()->getEpNumber() != 0)
        return UsbxFalse;
    UsbxMessageTrans *controlTrans = static_cast<UsbxMessageTrans*>(trans);
    // If it is not standard transaction for device, ignore
    if ((controlTrans->getRequestType() & 0x60) != 0x00) // bmRequestType[6:5]=0
        return UsbxFalse;
    switch (controlTrans->getRequest())
    {
        case 6: // GET_DESCRIPTOR
        {
            if (controlTrans->getRequestValue() >> 8 == 1 &&
                    controlTrans->getDataSize() >= 8)
            {
                // Check for error
                if (controlTrans->getStatus() == CarbonXUsbStatus_HALT)
                {
                    UsbXtor::error(this, CarbonXUsbError_ProtocolError,
                            "Control transfer GET_DESCRIPTOR halted",
                            controlTrans);
                    return UsbxFalse;
                }
                // Device descriptor
                controlTrans->getPipe()->setEp0Descriptor(
                        controlTrans->getData(), controlTrans->getDataSize());
            }
            else
                return UsbxFalse;
            break;
        }
        case 5: // SET_ADDRESS
        {
            // Check for error
            if (controlTrans->getStatus() == CarbonXUsbStatus_HALT)
            {
                UsbXtor::error(this, CarbonXUsbError_ProtocolError,
                        "Control transfer SET_ADDRESS halted",
                        controlTrans);
                return UsbxFalse;
            }
            controlTrans->getPipe()->setDeviceAddress(
                    controlTrans->getRequestValue() & 0x7F);
            break;
        }
        default: return UsbxFalse;
    }
#ifdef DEBUG
    controlTrans->getPipe()->print();
#endif
    return UsbxTrue;
    defaultResp = UsbxFalse;
}

UsbxBool UsbxHostXtor::suspend()
{
    if (_fsm == NULL)
        return UsbxFalse;
    return _hostFsm->suspend();
}

UsbxBool UsbxHostXtor::resume()
{
    if (_fsm == NULL)
        return UsbxFalse;
    return _hostFsm->resume();
}

void UsbxHostXtor::resetForDetach()
{
    // Initialize speed and addresses
    setSpeedFS(UsbxTrue, 0);
    initAddressTable();

    // Destroy all pipes
    UsbxPipe * const *pipes = _pipeList->getArray();
    UInt pipeCount = _pipeList->getCount();
    while (pipeCount > 0)
        destroyPipe(pipes[--pipeCount]);

    // Stop the scheduler
    _scheduler->stopScheduler();
}

// ****************************************************
// ************* USB DEVICE IMPLEMENTATION ************
// ****************************************************

UsbxDeviceXtor::UsbxDeviceXtor(UsbxConstString name,
        const CarbonXUsbConfig *config): CarbonXUsbClass(name, config),
    _deviceFsm(NULL), _deviceAddress(0),
    _defaultConfigNumber(static_cast<CarbonUInt17>(-1)), _frameNumber(0)
{
    // For device create the default control pipe with 0 address initially
    if (_config->getDeviceDescriptor() == NULL)
        errorForNoMemory("as no device descriptor specified");
    else
        createPipe(0, _config->getDeviceDescriptor());
    // Create FSM unit
    _fsm = _deviceFsm = new UsbxDeviceFsm(this);
    if (_fsm == NULL)
    {
        errorForNoMemory("transactor finite state machine (FSM)");
        return;
    }
}

UsbxDeviceXtor::~UsbxDeviceXtor()
{
    delete _deviceFsm;

    // Delete transactions created internally by device as response
    UsbxPipe * const *pipes = getPipes();
    for (UInt i = 0; i < getPipeCount(); i++)
    {
        UsbxQueue<UsbxTrans> *transQueue = pipes[i]->getTransQueue();
        while (!transQueue->isEmpty())
            delete transQueue->pop();
        // Note, do not delete the queue here - it will be deleted from pipe
    }
}

void UsbxDeviceXtor::setDeviceAddress(CarbonUInt7 deviceAddress)
{
    _deviceAddress = deviceAddress;
    UsbxPipe * const *pipes = getPipes();
    for (UInt i = 0; i < getPipeCount(); i++)
        pipes[i]->setDeviceAddress(deviceAddress);
}

UsbxBool UsbxDeviceXtor::attach()
{
    if (_fsm == NULL)
        return UsbxFalse;
    return _deviceFsm->attach();
}

UsbxBool UsbxDeviceXtor::detach()
{
    if (_fsm == NULL)
        return UsbxFalse;
    return _deviceFsm->detach();
}

UsbxBool UsbxDeviceXtor::resume()
{
    if (_fsm == NULL)
        return UsbxFalse;
    return _deviceFsm->remoteWakeUp();
}

UsbxBool UsbxDeviceXtor::processTrans(UsbxTrans *trans, UsbxBool defaultResp)
{
    ASSERT(trans, this);
    // If transaction is not targetted to default control pipe, ignore
    if (trans->getType() != CarbonXUsbPipe_CONTROL ||
            trans->getPipe()->getEpNumber() != 0)
        return UsbxFalse;
#if 0
    // For device as hub
    if (_config->isHub() &&
            trans->getPipe()->getDeviceAddress() != getDeviceAddress())
        return UsbxFalse;
#endif
    UsbxMessageTrans *controlTrans = static_cast<UsbxMessageTrans*>(trans);
    // If it is not standard transaction for device, ignore
    if ((controlTrans->getRequestType() & 0x60) != 0x00) // bmRequestType[6:5]=0
        return UsbxFalse;
    switch (controlTrans->getRequest())
    {
        case 6: // GET_DESCRIPTOR
        {
            if (!defaultResp)
                break;
            const UsbxByte *descriptor = NULL;
            UsbxByte descType = controlTrans->getRequestValue() >> 8;
            CarbonUInt16 dataLength = 0;
            if (descType == 1) // Device descriptor
            {
                descriptor = _config->getDeviceDescriptor();
                if (_config->getDeviceDescriptor() == NULL)
                {
                    UsbXtor::error(this, CarbonXUsbError_InvalidDescriptor,
                        "No device descriptor specified");
                    return UsbxFalse;
                }
                dataLength = descriptor[0];
            }
            else if (descType == 2) // Configuration descriptor
            {
                const UsbxQueue<UsbxByte> *configDescList =
                    _config->getConfigDescriptorList();
                if (configDescList == NULL)
                {
                    UsbXtor::error(this, CarbonXUsbError_InvalidDescriptor,
                        "No configuration descriptor specified");
                    return UsbxFalse;
                }
                UsbxQueue<UsbxByte>::UsbxQueueIterator iter;
                UInt index = 1;
                for (iter = configDescList->begin();
                        iter != configDescList->end(); iter++, index++)
                {
                    UsbxByte *configDescriptor = *iter;
                    if (configDescriptor == NULL ||
#if 1
                            index
#else
                            (controlTrans->getRequestValue() & 0x00FF)
#endif
                            != usbxGetConfigDescNumber(configDescriptor))
                        continue;
                    descriptor = configDescriptor;
                    dataLength = descriptor[2];
                    break;
                }
            }
            else
                return UsbxFalse;
            if (descriptor == NULL)
                return UsbxFalse;
            if (controlTrans->getRequestLength() < dataLength)
                dataLength = controlTrans->getRequestLength();
            controlTrans->setData(dataLength, descriptor);
            break;
        }
        case 5: // SET_ADDRESS
        {
            if (defaultResp)
                break;
            setDeviceAddress(controlTrans->getRequestValue() & 0x7F);
            break;
        }
        case 9: // SET_CONFIGURATION
        {
            if (defaultResp)
                break;
            // If same config is set ignore
            if (controlTrans->getRequestValue() == _defaultConfigNumber)
                break;
            // Add new pipes
            const UsbxQueue<UsbxByte> *configDescList =
                _config->getConfigDescriptorList();
            if (configDescList == NULL)
            {
                UsbXtor::error(this, CarbonXUsbError_InvalidDescriptor,
                    "No configuration descriptor specified");
                return UsbxFalse;
            }
            UsbxQueue<UsbxByte>::UsbxQueueIterator iter;
            for (iter = configDescList->begin(); iter != configDescList->end();
                    iter++)
            {
                UsbxByte *configDescriptor = *iter;
                if (configDescriptor == NULL ||
                        usbxGetConfigDescNumber(configDescriptor) !=
                        controlTrans->getRequestValue())
                    continue;
                _defaultConfigNumber = controlTrans->getRequestValue();
                // Remove older pipes except the default control pipe
                UsbxPipe * const *pipes = getPipes();
                for (UInt i = getPipeCount() - 1; i > 1; i--)
                    _pipeList->remove(pipes[i]);
                UInt epNum = usbxGetNumEpsFromConfig(configDescriptor);
                const UsbxByte *epDescriptor = configDescriptor;
                for (UInt j = 0; j < epNum; j++)
                {
                    epDescriptor = usbxGetNextEpDescriptor(epDescriptor);
                    createPipe(_deviceAddress, epDescriptor);
                }
                break;
            }
#ifdef DEBUG
            print();
#endif
            break;
        }
        default: return UsbxFalse;
    }
    return UsbxTrue;
}

void UsbxDeviceXtor::resetForDetach()
{
    // Initialize speed and addresses
    setSpeedFS(UsbxTrue, 0);
    _deviceAddress = 0;
    _defaultConfigNumber = static_cast<CarbonUInt17>(-1);

    // Destroy all pipes except default control pipe
    UsbxPipe * const *pipes = _pipeList->getArray();
    UInt pipeCount = _pipeList->getCount();
    while (pipeCount > 1)
        destroyPipe(pipes[--pipeCount]);
}

// ****************************************************
// ************* OTG DEVICE IMPLEMENTATION ************
// ****************************************************

UsbxOtgDeviceXtor::UsbxOtgDeviceXtor(UsbxConstString name,
        const CarbonXUsbConfig *config): CarbonXUsbClass(name, config),
    UsbxHostXtor(name, config), UsbxDeviceXtor(name, config),
    _otgDeviceType(UsbxOtgDevice_B), _isHostMode(UsbxFalse),
    _deviceDefaultControlPipe(NULL), _isInHnp(UsbxFalse)
{
    _fsm = new UsbxOtgFsm(this, _hostFsm, _deviceFsm);
    if (_fsm == NULL)
    {
        errorForNoMemory("transactor finite state machine (FSM)");
        return;
    }

    // For PHY transactor, if Mini-A plug is not connected, it is host
    if (_config->getIntfSide() == CarbonXUsbIntfSide_PHY &&
            !_config->getMiniAPhyPlug())
        setOtgDeviceType(UsbxOtgDevice_A);

    _intf->resetForDetach();
}

UsbxOtgDeviceXtor::~UsbxOtgDeviceXtor()
{
    delete _fsm;
    if (_isHostMode)
        delete _deviceDefaultControlPipe;
}

void UsbxOtgDeviceXtor::setHost(UsbxBool isHost)
{
    // Initialize FSM and interface
    resetForDetach();

    _isHostMode = isHost; // DO NOT shift this above resetForDetach() call

    if (!_isHostMode)
#ifdef OTG_SAVE_DEVICE_DEFAULT_CONTROL_PIPE
    {
        ASSERT(_deviceDefaultControlPipe, this);
        _pipeList->add(_deviceDefaultControlPipe);
    }
#else
    // For device create the default control pipe with 0 address initially
    if (_config->getDeviceDescriptor() == NULL)
    {
        UsbXtor::error(this, CarbonXUsbError_InvalidDescriptor,
            "No device descriptor specified");
        return UsbxFalse;
    }
    createPipe(0, _config->getDeviceDescriptor());
#endif
}

CarbonUInt11 UsbxOtgDeviceXtor::getFrameNumber() const
{
    if (_isHostMode)
        return UsbxHostXtor::getFrameNumber();
    else
        return UsbxDeviceXtor::getFrameNumber();
    return 0;
}

UsbxBool UsbxOtgDeviceXtor::resume()
{
    if (_isHostMode)
        return _hostFsm->resume();
    else
        return _deviceFsm->remoteWakeUp();
    return UsbxFalse;
}

UsbxBool UsbxOtgDeviceXtor::processTrans(UsbxTrans *trans, UsbxBool defaultResp)
{
    ASSERT(trans, this);
    // If transaction is not targetted to default control pipe, ignore
    if (trans->getType() != CarbonXUsbPipe_CONTROL ||
            trans->getPipe()->getEpNumber() != 0)
        return UsbxFalse;
    UsbxMessageTrans *controlTrans = static_cast<UsbxMessageTrans*>(trans);

    if (_isHostMode)
    {
        if (UsbxHostXtor::processTrans(trans, defaultResp))
            return UsbxTrue;

        // Process HNP request from a OTG A Device
        if (_otgDeviceType == UsbxOtgDevice_A &&
                controlTrans->getRequestType() == 0x00 &&
                controlTrans->getRequest() == 3)
        {
            // Check for HNP refusal case
            if (controlTrans->getStatus() == CarbonXUsbStatus_HALT)
                return UsbxTrue;

            // Suspend bus for HNP
            suspend();
            _isInHnp = UsbxTrue;
            return UsbxTrue;
        }
    }
    else
    {
        if (UsbxDeviceXtor::processTrans(trans, defaultResp))
            return UsbxTrue;

        // Process HNP request came to a OTG B Device
        if (_otgDeviceType == UsbxOtgDevice_B &&
                controlTrans->getRequestType() == 0x00 &&
                controlTrans->getRequest() == 3)
        {
            // If HNP is not supported, set stall state in default respone
            if (defaultResp && !_config->isOtgHnpSupported())
                controlTrans->setStallState(1);

            // Accept HNP by checking OTG descriptor
            if (!defaultResp && _config->isOtgHnpSupported())
                _isInHnp = UsbxTrue;

            return UsbxTrue;
        }
    }
    return UsbxFalse;
}

void UsbxOtgDeviceXtor::resetForDetach()
{
    // Initialize speed and addresses
    setSpeedFS(UsbxTrue, 0);
    _deviceAddress = 0;
    _defaultConfigNumber = static_cast<CarbonUInt17>(-1);
    initAddressTable();

    // Initialize FSM and interface
    _intf->resetForDetach();
    _fsm->resetForDetach();

    // Destroy all pipes except default control pipe
    UsbxPipe * const *pipes = _pipeList->getArray();
    UInt pipeCount = _pipeList->getCount();
    while (pipeCount > 1)
        destroyPipe(pipes[--pipeCount]);

#ifdef OTG_SAVE_DEVICE_DEFAULT_CONTROL_PIPE
    if (_isHostMode)
        destroyPipe(pipes[0]); // Destroy host default control pipe
    else
        // Save device default control pipe at the first place
        _deviceDefaultControlPipe = pipes[0];
#else
    destroyPipe(pipes[0]); // Destroy the default control pipe also
#endif

    // Reset pipe list
    _pipeList->reset();
}

UsbxBool UsbxOtgDeviceXtor::startOtgSession()
{
    return static_cast<UsbxOtgFsm*>(_fsm)->startSession();
}

UsbxBool UsbxOtgDeviceXtor::stopOtgSession()
{
    return static_cast<UsbxOtgFsm*>(_fsm)->stopSession();
}

UsbxBool UsbxOtgDeviceXtor::startOtgHnp()
{
    if (_isHostMode && getOtgDeviceType() == UsbxOtgDevice_A &&
            _config->isOtgHnpSupported())
    {
        // Find default control pipe
        UsbxPipe *defaultControlPipe = *(_pipeList->getArray());
        if (defaultControlPipe == NULL)
            return UsbxFalse;

        // Start SET_FEATURE(b_hnp_enable) for HNP
        UsbxMessageTrans *trans = new UsbxMessageTrans(defaultControlPipe);
        if (trans == NULL)
        {
            UsbXtor::error(this, CarbonXUsbError_NoMemory,
                    "Can't create post-attachment GET_DESCRIPTOR transaction");
            return UsbxFalse;
        }
        trans->setRequestType(0x00);
        trans->setRequest(3); // SET_FEATURE
        trans->setRequestValue(3); // b_hnp_enable
        return startNewTransaction(trans);
    }
    return UsbxFalse;
}

UsbxBool UsbxOtgDeviceXtor::stopOtgHnp()
{
    if (_isHostMode && _isInHnp && getOtgDeviceType() == UsbxOtgDevice_B)
    {
        // Suspend bus for HNP
        suspend();
        return UsbxTrue;
    }
    return UsbxFalse;
}
