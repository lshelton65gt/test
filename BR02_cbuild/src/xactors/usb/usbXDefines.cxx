/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
      File    :  usbXDefines.cpp
      Purpose :  Provides some utility function for usb transactor.
*/

#define DEF_BOOL_CONST /* This file actually defines UsbxTrue/False */
#include "usbXDefines.h"
#include <stdlib.h>

/* This routine is used to check if memory is available - used for emulation
 * of no memory situation
 * UsbxMemoryIndex: Memory allocated till now thru NEW macro
 * CarbonXUsbMaxMemory: Max memory can be allocated - This can be
 *                      modified to emulate no memory situation */
UInt UsbxMemoryIndex = 0;
CarbonUInt32 CarbonXUsbMaxMemory = 0;
UInt UsbxIsMemAvail(size_t size)
{
    if (CarbonXUsbMaxMemory <= 0) // Infinite memory - allocation can
                                  // fail depending on system
        return 1;
    // say no memory when CarbonXUsbMaxMemory is reached
    if (UsbxMemoryIndex + size > CarbonXUsbMaxMemory)
        return 0;
    UsbxMemoryIndex += static_cast<UInt>(size); // Increment memory count
    return 1;
}

// Project specific strdup
UsbxString UsbxStrdup(UsbxConstString str)
{
    UsbxString dup = NEWARRAY(char, strlen(str) + 1);
    if (dup == NULL)
        return NULL;
    strcpy(dup, str);
    return dup;
}

// Gets a temporary string buffer for creating name, user messages etc.
UsbxString UsbxGetStringBuffer()
{
    static char buffer[1024];
    *buffer = 0;
    return buffer;
}

#if 0
// Definition of UsbxAssertFail used as a replacement of assert()
UInt UsbxAssertFail(const UsbXtor *xtor, UsbxConstString failExpr,
        UsbxConstString file, UInt line)
{
    printf("ASSERT: %s %d", file, line);
    if (failExpr)
        printf(": %s ...\n", failExpr);
    else
        printf("\n");
    exit(1);
    return (xtor == NULL); // ignore warning
}
#endif
