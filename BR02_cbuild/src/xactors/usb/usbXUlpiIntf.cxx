/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
      File    :  usbXUlpiIntf.cpp
      Purpose :  Implements ULPI Link and PHY interface pins and FSMs.
*/

#include "usbXUlpiIntf.h"
#include "usbXtor.h"

#define USE_UTMI_LEVEL_1
#define NO_DUMP_FOR_UTMI

UsbxUlpiIntf::UsbxUlpiIntf(UsbXtor *xtor, UsbxConstString moduleName):
    UsbxIntf(xtor), _utmiIntf(NULL), _state(UsbxUlpiFsm_IDLE),
    _dir(NULL), _nxt(NULL), _stp(NULL), _data(NULL), _moduleName(moduleName),
    _isPidData(UsbxFalse), _isFunctionControlUpdate(UsbxFalse)
{
}

UsbxUlpiIntf::~UsbxUlpiIntf()
{
    // Delete the UTMI Interface
    delete _utmiIntf;

    delete _dir;
    delete _nxt;
    delete _stp;
    delete _data;
}

void UsbxUlpiIntf::setTimingFactor(CarbonUInt32 factor)
{
    UsbxIntf::setTimingFactor(factor);
    _utmiIntf->setTimingFactor(factor);
}

void UsbxUlpiIntf::resetForDetach()
{
    _utmiIntf->resetForDetach();
    // Note, DO NOT change _state to UsbxUlpiFsm_IDLE here
}

UsbxPin* UsbxUlpiIntf::getPin(UsbxConstString pinName) const
{
    if (strcmp(pinName, "dir") == 0)
        return _dir;
    if (strcmp(pinName, "nxt") == 0)
        return _nxt;
    if (strcmp(pinName, "stp") == 0)
        return _stp;
    if (strcmp(pinName, "data") == 0)
        return _data;
    return NULL;
}

UsbxUlpiLinkIntf::UsbxUlpiLinkIntf(UsbXtor *xtor): UsbxUlpiIntf(xtor,
    "UTMI:LINK"), _functionCtrlReg(0x41), _otgCtrlReg(0), _lineStateLogic()
{
    _utmiIntf = new UsbxUtmiLinkIntf(xtor, 8,
#ifdef USE_UTMI_LEVEL_1
    CarbonXUsbUtmiPinLevel_1);
#else
    CarbonXUsbUtmiPinLevel_0);
#endif

    // Link up lineState logic
    _lineStateLogic.setIntf(_utmiIntf);

    // Set the isUlpi flag for UTMI -- needed for RxRActive delay adjustment
    _utmiIntf->_isUlpi = UsbxTrue;

    // Create the pins
    _dir = createPin(xtor, "dir", UsbxDirection_Input);
    _nxt = createPin(xtor, "nxt", UsbxDirection_Input);
    _stp = createPin(xtor, "stp", UsbxDirection_Output);
    _data = createPin(xtor, "data", UsbxDirection_Inout, 8);

    // Set the direction control to input by default
    _data->setInoutDirControl(UsbxDirection_Input);

    // Drive default values to UTMI
    _utmiIntf->_LineState->write(UsbxUtmiIntf::UsbxUtmiLineState_SE0, 0);

    // OTG normal mode timing
    if (_utmiIntf->_interfaceLevel == CarbonXUsbUtmiPinLevel_1)
        _utmiIntf->_IdDig->write(1, 0);

#ifdef _VCD
#ifndef NO_DUMP_FOR_UTMI
    FILE *vcdFile = UsbXtor::getVcdFile();
    fprintf(vcdFile, "$scope module UTMI:LINK $end\n");

    vcdDecl(_utmiIntf->_SuspendM);
    vcdDecl(_utmiIntf->_DataInLo);
    vcdDecl(_utmiIntf->_TXValid);
    vcdDecl(_utmiIntf->_TXReady);
    vcdDecl(_utmiIntf->_DataOutLo);
    vcdDecl(_utmiIntf->_RXValid);
    vcdDecl(_utmiIntf->_RXActive);
    vcdDecl(_utmiIntf->_XcvrSelect);
    vcdDecl(_utmiIntf->_TermSelect);
    vcdDecl(_utmiIntf->_OpMode);
    vcdDecl(_utmiIntf->_LineState);
    vcdDecl(_utmiIntf->_DpPulldown);
    vcdDecl(_utmiIntf->_DmPulldown);
    vcdDecl(_utmiIntf->_HostDisconnect);
    vcdDecl(_utmiIntf->_AValid);
    vcdDecl(_utmiIntf->_BValid);
    vcdDecl(_utmiIntf->_SessEnd);
    vcdDecl(_utmiIntf->_VbusValid);
    vcdDecl(_utmiIntf->_DrvVbus);
    vcdDecl(_utmiIntf->_IdDig);
    vcdDecl(_utmiIntf->_IdPullup);

    fprintf(vcdFile, "$upscope $end\n");
#endif
#endif
}

void UsbxUlpiLinkIntf::setSpeedFS(UsbxBool isFS, CarbonTime currentTime)
{
    _utmiIntf->setSpeedFS(isFS, currentTime);
}

// ULPI Link FSM
UsbxBool UsbxUlpiLinkIntf::evaluate(CarbonTime currentTime)
{
    UsbxBool retVal = UsbxTrue;

    // Read the input pins from the UTMI Interface
    retVal &= readInterface(currentTime);

    // Evaluate the UTMI Link Interface
    retVal &= _utmiIntf->evaluate(currentTime);

    // Write the output pins to the ULPI bus
    retVal &= writeInterface(currentTime);

#ifndef NO_DUMP_FOR_UTMI
    // Dump UTMI pins for VCD
    dumpUtmiIntfPins(currentTime);
#endif

    return retVal;
    currentTime = 0; // To remove warning
}

UsbxBool UsbxUlpiLinkIntf::readInterface(CarbonTime currentTime)
{
    if (_dir->read(currentTime))
        _data->setInoutDirControl(UsbxDirection_Input);
    else
        _data->setInoutDirControl(UsbxDirection_Output);

    // Update FSM
    switch (_state)
    {
        case UsbxUlpiFsm_IDLE:
        {
            _utmiIntf->_TXReady->write(0, currentTime);

            // Drive RxActive if dir and nxt are both high
            if (_dir->read(currentTime) && _nxt->read(currentTime))
                _utmiIntf->_RXActive->write(1, currentTime);
            break;
        }
        case UsbxUlpiFsm_RECEIVE:
        {
            _utmiIntf->_RXValid->write(_nxt->read(currentTime), currentTime);

            // Update data if nxt is high
            if (_nxt->read(currentTime))
                _utmiIntf->_DataOutLo->write(_data->read(currentTime),
                        currentTime);
            else if (_dir->read(currentTime))
                // Intermittent RxCmd packets
                processRxCmd(currentTime);
            else // dir and nxt are both low
                _utmiIntf->_RXActive->write(0, currentTime);
            break;
        }
        case UsbxUlpiFsm_TRANSMIT:
        {
            _utmiIntf->_TXReady->write(_nxt->read(currentTime),
                        currentTime);
            break;
        }
        case UsbxUlpiFsm_WaitForEOP:
        {
            _utmiIntf->_TXReady->write(0, currentTime);
            break;
        }
        case UsbxUlpiFsm_ReceiveEOP:
        {
            // Check for EOP before moving back to IDLE
            if (_dir->read(currentTime) && !_nxt->read(currentTime))
                processRxCmd(currentTime);
            break;
        }
        default:
            break;
    }

    return UsbxTrue;
}

UsbxBool UsbxUlpiLinkIntf::writeInterface(CarbonTime currentTime)
{
    // Data transmit and Receive
    switch (_state)
    {
        case UsbxUlpiFsm_IDLE:
        {
            // Outputs - drive all ULPI pins to default values
            _stp->write(0, currentTime);

            // Go to receive state if dir is high
            if (_dir->read(currentTime))
            {
                _state = UsbxUlpiFsm_RECEIVE;
                break;
            }
            else // Link drives data to 0 by default when dir is low
                _data->write(0, currentTime);

            // Priority of Transmit/Receive is highest followed by
            // LineState updates and RegWrite is lowest
            if (_utmiIntf->_TXValid->read(currentTime))
            {
                // Write TX CMD depending on PID/NOPID data
                if (_utmiIntf->_OpMode->read(currentTime) == 0) // PID data
                {
                    _data->write(0x40 |
                            (_utmiIntf->_DataInLo->read(currentTime) & 0x0F),
                             currentTime);
                    _isPidData = UsbxTrue;

                    _lineStateLogic.reset();
                    _state = UsbxUlpiFsm_TRANSMIT;
                }
                // Chirp -- No PID data
                else // if (_utmiIntf->_OpMode->read(currentTime) == 2)
                {
                    ASSERT(_utmiIntf->_OpMode->read(currentTime) == 2, _xtor);

                    // Send FunctionControlReg before starting
                    // chirp if any update is is available
                    if (_functionCtrlReg != updateFunctionCtrlReg(currentTime))
                    {
                        // Write TX CMD RegWrite
                        _data->write(0x84, currentTime);
                        _isFunctionControlUpdate = UsbxTrue;
                        _state = UsbxUlpiFsm_RegWrite;
                    }
                    else
                    {
                        _data->write(0x40, currentTime);
                        _isPidData = UsbxFalse;
                        _state = UsbxUlpiFsm_SendNOPID;
                    }
                }
            }
            // Else check for updates in XcvrSelect, TermSelect, OpMode,
            // Reset and SuspendM through FunctionCtrlReg and
            // IdPullup, DpPulldown, DmPulldown, DischrgVbus, ChrgVbus
            // and DrvVbus through otgControlReg
            else if (_functionCtrlReg != updateFunctionCtrlReg(currentTime))
            {
                // Write TX CMD RegWrite
                _data->write(0x84, currentTime);
                _isFunctionControlUpdate = UsbxTrue;
                _state = UsbxUlpiFsm_RegWrite;
            }
            else if (_otgCtrlReg != updateOtgCtrlReg(currentTime))
            {
                // Write TX CMD RegWrite on a different address so
                // that PHY understands this is OTG and not func ctrl
                _data->write(0x8A, currentTime);
                _isFunctionControlUpdate = UsbxFalse;
                _state = UsbxUlpiFsm_RegWrite;
            }
            break;
        }
        case UsbxUlpiFsm_SendNOPID:
        {
            if (_nxt->read(currentTime))
            {
                _isPidData = UsbxFalse;
                _data->write(_utmiIntf->_DataInLo->read(currentTime),
                        currentTime);
                _state = UsbxUlpiFsm_TRANSMIT;
            }
            break;
        }
        case UsbxUlpiFsm_TRANSMIT:
        {
            if (_isPidData &&
                    _utmiIntf->_HostDisconnect->read(currentTime) == 0)
                _lineStateLogic.evaluate(currentTime);
            else if (_utmiIntf->_TXValid->read(currentTime) &&
                    _utmiIntf->_OpMode->read(currentTime) == 2)
                _utmiIntf->_LineState->write(
                        (_utmiIntf->_DataInLo->read(currentTime) == 0) ?
                        UsbxUtmiIntf::UsbxUtmiLineState_K :
                        UsbxUtmiIntf::UsbxUtmiLineState_J, currentTime);

            if (_nxt->read(currentTime))
            {
                if (_utmiIntf->_TXValid->read(currentTime))
                    _data->write(_utmiIntf->_DataInLo->read(currentTime),
                        currentTime);
                else // Last byte of data
                {
                    _data->write(0, currentTime);
                    _stp->write(1, currentTime);
                    _state = _isPidData ? UsbxUlpiFsm_WaitForEOP:
                        UsbxUlpiFsm_IDLE;
                }
            }
            break;
        }
        case UsbxUlpiFsm_WaitForEOP:
        {
            _stp->write(0, currentTime);
            _utmiIntf->_TXReady->write(0, currentTime);
            if (_dir->read(currentTime) && !_nxt->read(currentTime))
                _state = UsbxUlpiFsm_ReceiveEOP;
            break;
        }
        case UsbxUlpiFsm_ReceiveEOP:
        {
            // Check for EOP before moving back to IDLE
            if (_dir->read(currentTime) && !_nxt->read(currentTime))
                _state = UsbxUlpiFsm_IDLE;
            break;
        }
        case UsbxUlpiFsm_RECEIVE:
        {
            if (!_dir->read(currentTime) && !_nxt->read(currentTime))
                _state = UsbxUlpiFsm_IDLE;
            break;
        }
        case UsbxUlpiFsm_RegWrite:
        {
            // Check if dir is high and regWrite is not done
            if (_dir->read(currentTime))
            {
                // Abort Phy but process RxCmd if any available
                _stp->write(1, currentTime);
                _state = UsbxUlpiFsm_ABORT;
            }
            else if (_nxt->read(currentTime))
            {
                // Check if Function Control Register Update
                // or OTG Control Register Update
                if (_isFunctionControlUpdate)
                {
                    _functionCtrlReg = updateFunctionCtrlReg(currentTime);
                    _data->write(_functionCtrlReg, currentTime);
                }
                else
                {
                    _otgCtrlReg = updateOtgCtrlReg(currentTime);
                    _data->write(_otgCtrlReg, currentTime);
                }

                // Wait for PHY to accept the update
                _state = UsbxUlpiFsm_RegWriteData;
            }
            break;
        }
        case UsbxUlpiFsm_RegWriteData:
        {
            if (_nxt->read(currentTime))
            {
                _stp->write(1, currentTime);
                _state = UsbxUlpiFsm_IDLE;
            }
            break;
        }
        case UsbxUlpiFsm_ABORT:
        {
            if (!_dir->read(currentTime))
            {
                if (_isFunctionControlUpdate)
                    _data->write(0x84, currentTime);
                else
                    _data->write(0x8A, currentTime);
                _state = UsbxUlpiFsm_RegWrite;
            }
            else // _dir is high - check if any updates from PHY
                processRxCmd(currentTime);

            // Phy aborted, lower stp
            _stp->write(0, currentTime);
            break;
        }
        default:
            break;
    }

    return UsbxTrue;
}

void UsbxUlpiLinkIntf::processRxCmd(CarbonTime currentTime)
{
    CarbonUInt8 rxCmd = _data->read(currentTime);

    // Update LineState
    _utmiIntf->_LineState->write(rxCmd & 0x03, currentTime);

    // Update SessEnd, AValid and VBusValid
    CarbonUInt4 vbusValue = (rxCmd & 0x0C) >> 2;

    _utmiIntf->_SessEnd->write(vbusValue == 0, currentTime);
    _utmiIntf->_AValid->write(vbusValue >= 2, currentTime);
    _utmiIntf->_BValid->write(vbusValue >= 2, currentTime);
    _utmiIntf->_VbusValid->write(vbusValue == 3, currentTime);

    // Update RXActive, RXError and HostDisconnect
    CarbonUInt4 rxEvent = (rxCmd & 0x30) >> 4;
    _utmiIntf->_RXActive->write(
            (rxEvent == 1) || (rxEvent == 3), currentTime);
    _utmiIntf->_RXError->write(rxEvent == 3, currentTime);
    _utmiIntf->_HostDisconnect->write(rxEvent == 2, currentTime);

    // Update IdDig
    _utmiIntf->_IdDig->write(rxCmd & 0x40 ? 1 : 0, currentTime);
}

CarbonUInt8 UsbxUlpiLinkIntf::updateFunctionCtrlReg(CarbonTime currentTime)
{
    CarbonUInt8 functionControlReg = 0;

    // SuspendM pin updation
    functionControlReg |= _utmiIntf->_SuspendM->read(currentTime) << 6;

    // Reset pin updation
    functionControlReg |= _utmiIntf->_Reset->read(currentTime) << 5;

    // OpMode pin updation
    functionControlReg |= _utmiIntf->_OpMode->read(currentTime) << 3;

    // TermSelect pin updation
    functionControlReg |= _utmiIntf->_TermSelect->read(currentTime) << 2;

    // XcvrSelect pin updation
    functionControlReg |= _utmiIntf->_XcvrSelect->read(currentTime);

    return functionControlReg;
}

CarbonUInt8 UsbxUlpiLinkIntf::updateOtgCtrlReg(CarbonTime currentTime)
{
    CarbonUInt8 otgControlReg = 0;

    // DrvVBus pin updation
    otgControlReg |= _utmiIntf->_DrvVbus->read(currentTime) << 5;

    // ChrgVbus pin updation
    otgControlReg |= _utmiIntf->_ChrgVbus->read(currentTime) << 4;

    // DischrgVbus pin updation
    otgControlReg |= _utmiIntf->_DischrgVbus->read(currentTime) << 3;

    // DmPulldown pin updation
    otgControlReg |= _utmiIntf->_DmPulldown->read(currentTime) << 2;

    // DpPulldown pin updation
    otgControlReg |= _utmiIntf->_DpPulldown->read(currentTime) << 1;

    // IdPullup pin updation
    otgControlReg |= _utmiIntf->_IdPullup->read(currentTime);

    return otgControlReg;
}

UsbxBool UsbxUlpiLinkIntf::refreshInputs(CarbonTime currentTime)
{
    UsbxBool retValue = UsbxTrue;
    retValue &= _dir->refresh(currentTime);
    retValue &= _nxt->refresh(currentTime);
    retValue &= _data->refresh(currentTime);
    return retValue;
}

UsbxBool UsbxUlpiLinkIntf::refreshOutputs(CarbonTime currentTime)
{
    UsbxBool retValue = UsbxTrue;
    retValue &= _stp->refresh(currentTime);
    retValue &= _data->refresh(currentTime);
    return retValue;
}

#ifdef _VCD
UsbxBool UsbxUlpiLinkIntf::dumpUtmiIntfPins(CarbonTime currentTime) const
{
    // Dump UTMI Pins using the dump function to resolve scope
    vcdDump(_utmiIntf->_SuspendM);
    vcdDump(_utmiIntf->_DataInLo);
    vcdDump(_utmiIntf->_TXValid);
    vcdDump(_utmiIntf->_TXReady);
    vcdDump(_utmiIntf->_DataOutLo);
    vcdDump(_utmiIntf->_RXValid);
    vcdDump(_utmiIntf->_RXActive);
    vcdDump(_utmiIntf->_XcvrSelect);
    vcdDump(_utmiIntf->_TermSelect);
    vcdDump(_utmiIntf->_OpMode);
    vcdDump(_utmiIntf->_LineState);
    vcdDump(_utmiIntf->_DpPulldown);
    vcdDump(_utmiIntf->_DmPulldown);
    vcdDump(_utmiIntf->_HostDisconnect);
    vcdDump(_utmiIntf->_AValid);
    vcdDump(_utmiIntf->_BValid);
    vcdDump(_utmiIntf->_SessEnd);
    vcdDump(_utmiIntf->_VbusValid);
    vcdDump(_utmiIntf->_DrvVbus);
    vcdDump(_utmiIntf->_IdDig);
    vcdDump(_utmiIntf->_IdPullup);

    return UsbxTrue;
    currentTime = 0;
}

void UsbxUlpiIntf::vcdDecl(UsbxPin *pin)
{
    FILE *vcdFile = UsbXtor::getVcdFile();
    if (vcdFile)
        fprintf(vcdFile, "$var wire %d %s.%s %s $end\n", pin->getSize(),
                _moduleName, pin->getName(), pin->getName());
}

void UsbxUlpiLinkIntf::vcdDump(UsbxPin *pin) const
{
    FILE *vcdFile = UsbXtor::getVcdFile();
    if (vcdFile)
    {
        if (pin->getSize() > 1)
        {
            fprintf(vcdFile, "b");
            for (Int i = pin->getSize() - 1; i >= 0; i--)
            /* Don't make 'i' UInt */
            {
                CarbonUInt32 mask = 0x1 << i;
                fprintf(vcdFile, "%u", (pin->read(0) & mask) >> i);
            }
            fprintf(vcdFile, " ");
        }
        else
            fprintf(vcdFile, "%u", pin->read(0));
        fprintf(vcdFile, "%s.%s\n", _moduleName, pin->getName());
    }
}
#endif

UsbxUlpiPhyIntf::UsbxUlpiPhyIntf(UsbXtor *xtor):
    UsbxUlpiIntf(xtor, "UTMI:PHY"), _pidByteReceivedByUtmi(UsbxFalse),
    _rxCmdSent(UsbxFalse), _isAborted(UsbxFalse),
    _rxCmd(0), _abortedFromState(UsbxUlpiFsm_IDLE)
{
    _utmiIntf = new UsbxUtmiPhyIntf(xtor, 8,
#ifdef USE_UTMI_LEVEL_1
    CarbonXUsbUtmiPinLevel_1);
#else
    CarbonXUsbUtmiPinLevel_0);
#endif

    // Set the isUlpi flag for UTMI -- needed for RxRActive delay adjustment
    _utmiIntf->_isUlpi = UsbxTrue;

    // Create the pins
    _dir = createPin(xtor, "dir", UsbxDirection_Output);
    _nxt = createPin(xtor, "nxt", UsbxDirection_Output);
    _stp = createPin(xtor, "stp", UsbxDirection_Input);
    _data = createPin(xtor, "data", UsbxDirection_Inout, 8);

    // Set the direction control of data bus to Input by default
    // since Phy listens to the data bus by keeping dir low
    _data->setInoutDirControl(UsbxDirection_Input);

    // Drive default values to UTMI
    _utmiIntf->_XcvrSelect->write(1, 0);
    _utmiIntf->_TermSelect->write(1, 0);
    _utmiIntf->_OpMode->write(0, 0);
    _utmiIntf->_SuspendM->write(1, 0);

#ifdef _VCD
#ifndef NO_DUMP_FOR_UTMI
    FILE *vcdFile = UsbXtor::getVcdFile();
    fprintf(vcdFile, "$scope module UTMI:PHY $end\n");

    vcdDecl(_utmiIntf->_SuspendM);
    vcdDecl(_utmiIntf->_DataInLo);
    vcdDecl(_utmiIntf->_TXValid);
    vcdDecl(_utmiIntf->_TXReady);
    vcdDecl(_utmiIntf->_DataOutLo);
    vcdDecl(_utmiIntf->_RXValid);
    vcdDecl(_utmiIntf->_RXActive);
    vcdDecl(_utmiIntf->_XcvrSelect);
    vcdDecl(_utmiIntf->_TermSelect);
    vcdDecl(_utmiIntf->_OpMode);
    vcdDecl(_utmiIntf->_LineState);
    vcdDecl(_utmiIntf->_IdPullup);
    vcdDecl(_utmiIntf->_IdDig);
    vcdDecl(_utmiIntf->_DpPulldown);
    vcdDecl(_utmiIntf->_DmPulldown);
    vcdDecl(_utmiIntf->_HostDisconnect);
    vcdDecl(_utmiIntf->_AValid);
    vcdDecl(_utmiIntf->_BValid);
    vcdDecl(_utmiIntf->_SessEnd);
    vcdDecl(_utmiIntf->_VbusValid);
    vcdDecl(_utmiIntf->_DrvVbus);

    fprintf(vcdFile, "$upscope $end\n");
#endif
#endif
}

void UsbxUlpiPhyIntf::setSpeedFS(UsbxBool isFS, CarbonTime currentTime)
{
    _utmiIntf->setSpeedFS(isFS, currentTime);
}

// ULPI PHY FSM
UsbxBool UsbxUlpiPhyIntf::evaluate(CarbonTime currentTime)
{
    UsbxBool retVal = UsbxTrue;

    // Read the input pins from the UTMI Interface
    retVal &= readInterface(currentTime);

    // Evaluate the UTMI Link Interface
    retVal &= _utmiIntf->evaluate(currentTime);

    // Write the output pins to the ULPI bus
    retVal &= writeInterface(currentTime);

#ifndef NO_DUMP_FOR_UTMI
    // Dump UTMI pins for VCD
    dumpUtmiIntfPins(currentTime);
#endif

    return retVal;
    currentTime = 0; // To remove warning
}

UsbxBool UsbxUlpiPhyIntf::readInterface(CarbonTime currentTime)
{
    // Data transmit and Receive
    switch (_state)
    {
        case UsbxUlpiFsm_IDLE:
        {
            _utmiIntf->_TXValid->write(0, currentTime);

            // Receiving NO-PID/PID data
            if (_data->read(currentTime) && !_dir->read(currentTime))
            {
                CarbonUInt8 data = _data->read(currentTime);
                CarbonUInt4 commandCode = (data & 0xC0) >> 6;

                // Only for receive mode, UTMI PHY interface has to be
                // driven with values of OpMode, Data and TxValid
                if (commandCode == 1) // PHY Receive Mode
                {
                    // Check if PID data
                    if (data & 0x0F) // PID Data
                    {
                        data = ((~data & 0x0F) << 4) | (data & 0x0F);
                        _utmiIntf->_DataInLo->write(data, currentTime);
                        _utmiIntf->_TXValid->write(1, currentTime);
                        _utmiIntf->_OpMode->write(0, currentTime);
                    }
                    // Ignore first byte for other modes
                }
            }
            break;
        }
        case UsbxUlpiFsm_ReceivingNOPIDByte:
        {
            _utmiIntf->_TXValid->write(1, currentTime);
            _utmiIntf->_OpMode->write(2, currentTime);
            break;
        }
        case UsbxUlpiFsm_ReceiveChirp:
        {
            if (!_dir->read(currentTime))
                _utmiIntf->_DataInLo->write(_data->read(currentTime),
                        currentTime);
            break;
        }
        case UsbxUlpiFsm_RECEIVE:
        {
            if (_pidByteReceivedByUtmi)
                _utmiIntf->_DataInLo->write(_data->read(currentTime),
                        currentTime);
            break;
        }
        case UsbxUlpiFsm_RegRead:
        {
            break;
        }
        default:
            break;
    }

    // De-assert TXValid if stp is high
    if (_stp->read(currentTime) && !_dir->read(currentTime))
        _utmiIntf->_TXValid->write(0, currentTime);

    return UsbxTrue;
}

UsbxBool UsbxUlpiPhyIntf::writeInterface(CarbonTime currentTime)
{
    // Data transmit and Receive
    switch (_state)
    {
        case UsbxUlpiFsm_IDLE:
        {
            _isPidData = UsbxTrue;
            _rxCmdSent = UsbxFalse;
            _registerAddress = 0;

            // Receive Transmit has highest priority
            // Check whether PID or NOPID and change state
            if (_data->read(currentTime) && !_dir->read(currentTime))
            {
                CarbonUInt8 data = _data->read(currentTime);
                CarbonUInt4 commandCode = (data & 0xC0) >> 6;

                if (commandCode == 1) // PHY receiving PID / No-PID data
                {
                    // Check if NoPID data
                    if (data & 0x0F) // PID Data
                    {
                        _nxt->write(_utmiIntf->_TXReady->read(currentTime),
                                currentTime);
                        _pidByteReceivedByUtmi = UsbxFalse;
                        _state = UsbxUlpiFsm_ReceivingPIDByte;
                    }
                    else // For NOPID data
                    {
                        _pidByteReceivedByUtmi = UsbxFalse;
                        // Forcing nxt signal to HIGH without checking TXReady
                        _nxt->write(1, currentTime);
                        _state = UsbxUlpiFsm_ReceivingNOPIDByte;
                    }
                    break;
                }
            }

            // Go to transmit if RxActive is high
            if (_utmiIntf->_RXActive->read(currentTime))
            {
                // Lower nxt and send RxCMD if dir was previously high
                if (_dir->read(currentTime))
                {
                    _nxt->write(0, currentTime);

                    // Send RxCMD with RxActive information, update _rxCmd
                    _rxCmd = updateRxCmd(currentTime);
                    _data->write(_rxCmd, currentTime);
                }
                else // Assert dir and nxt when dir was previously low
                {
                    _dir->write(1, currentTime);
                    _nxt->write(1, currentTime);
                }
                _state = UsbxUlpiFsm_TRANSMIT;
                break;
            }

            // Check if there are lineState updates
            if (_rxCmd != updateRxCmd(currentTime))
            {
                // Check whether LineState has changed and send if needed

                _rxCmd = updateRxCmd(currentTime);
                _dir->write(1, currentTime);
                _rxCmdSent = UsbxFalse;
                _state = UsbxUlpiFsm_SendRXCMD;
            }
            else if (_data->read(currentTime)) // Check if RegWrite from Link
            {
                CarbonUInt8 data = _data->read(currentTime);
                CarbonUInt4 commandCode = (data & 0xC0) >> 6;
                if (commandCode == 2) // Reg Write
                {
                    // Check which type of register is being accessed
                    // The last 6 bits contain the register address
                    _registerAddress = data & 0x2F;
                    _nxt->write(1, currentTime);
                    _state = (_registerAddress == 0x2F) ?
                        UsbxUlpiFsm_ExtendedRegWrite : UsbxUlpiFsm_RegWrite;
                }
                else if (commandCode == 3) // Reg Read
                {
                    // Check which type of register is being accessed
                    // The last 6 bits contain the register address
                    _registerAddress = data & 0x2F;
                    _nxt->write(1, currentTime);
                    _state = (_registerAddress == 0x2F) ?
                        UsbxUlpiFsm_ExtendedRegRead : UsbxUlpiFsm_RegRead;
                }
            }
            break;
        }
        case UsbxUlpiFsm_TRANSMIT:
        {
            _dir->write(1, currentTime);
            _nxt->write(_utmiIntf->_RXValid->read(currentTime), currentTime);

            // If nxt is high, write data
            if (_utmiIntf->_RXValid->read(currentTime))
                _data->write(_utmiIntf->_DataOutLo->read(currentTime),
                        currentTime);
            // For last byte of data when RXActive and RXValid are both low
            else if (!_utmiIntf->_RXActive->read(currentTime))
            {
                // Lower dir and nxt and write data 0
                _nxt->write(0, currentTime);
                _data->write(0, currentTime);
                _dir->write(0, currentTime);

                // Go to IDLE
                _state = UsbxUlpiFsm_IDLE;

                // Utilize turnaround and set dir control once cycle later
                return UsbxTrue;
            }
            // For bit stuffing, RXValid goes low, send LineState updates
            else // Here RXActive is HIGH and RXValid is LOW
            {
                // Update the RX CMD and send
                _rxCmd = updateRxCmd(currentTime);
                _data->write(_rxCmd, currentTime);
            }
            break;
        }
        case UsbxUlpiFsm_ReceivingPIDByte:
        {
            _nxt->write(_utmiIntf->_TXReady->read(currentTime), currentTime);

            // Check when the PID byte is received -- when TXReady comes
            if (_utmiIntf->_TXReady->read(currentTime))
                _state = UsbxUlpiFsm_RECEIVE;
            break;
        }
        case UsbxUlpiFsm_ReceivingNOPIDByte:
        {
            // Drive logic for nxt (HIGH followed by LOW) for first byte
            // of TX CMD (NOPID)
            if (_nxt->read(currentTime))
            {
                _nxt->write(0, currentTime);
                _pidByteReceivedByUtmi = UsbxTrue;
                _isPidData = UsbxFalse;
                _state = UsbxUlpiFsm_ReceiveChirp;
            }
            else
                _nxt->write(1, currentTime);
            break;
        }
        case UsbxUlpiFsm_RECEIVE:
        {
            _nxt->write(_utmiIntf->_TXReady->read(currentTime), currentTime);
            _pidByteReceivedByUtmi = UsbxTrue;

            // Monitor LineState for SE0 to send RX CMD for EOP
            if (!_utmiIntf->_LineState->read(currentTime))
            {
                // Assert dir and take control of data bus
                _dir->write(1, currentTime);
                _state = UsbxUlpiFsm_SendRXCMD;
                _rxCmdSent = UsbxFalse;
            }
            break;
        }
        case UsbxUlpiFsm_RegWrite:
        {
            // Ignore first byte and go for processing the next data
            _state = UsbxUlpiFsm_RegWriteData;
            break;
        }
        case UsbxUlpiFsm_RegWriteData:
        {
            // Update the register depending on address
            updateRegister(currentTime);

            if (_stp->read(currentTime))
            {
                // Phy must de-assert _nxt if stp is high
                _nxt->write(0, currentTime);

                // If Phy was aborted, it should go back to aborted From state
                _state = _isAborted ? _abortedFromState : UsbxUlpiFsm_IDLE;
                _isAborted = UsbxFalse;
            }
            break;
        }
        case UsbxUlpiFsm_ExtendedRegWrite:
        {
            _nxt->write(1, currentTime);
            _state = UsbxUlpiFsm_ExtendedRegWriteAddress;
            break;
        }
        case UsbxUlpiFsm_ExtendedRegWriteAddress:
        {
            // Assert nxt so that Link can put register address
            _nxt->write(1, currentTime);

            // Get the extended register address
            _registerAddress = _data->read(currentTime) & 0x2F;
            _state = UsbxUlpiFsm_RegWriteData;
            break;
        }
        case UsbxUlpiFsm_RegRead:
        {
            _nxt->write(0, currentTime);
            _dir->write(1, currentTime);
            _state = UsbxUlpiFsm_RegReadData;
            break;
        }
        case UsbxUlpiFsm_RegReadData:
        {
            _data->write(readRegisterData(currentTime),
                    currentTime);
            break;
        }
        case UsbxUlpiFsm_ExtendedRegRead:
        {
            _nxt->write(1, currentTime);
            _state = UsbxUlpiFsm_ExtendedRegReadAddress;
            break;
        }
        case UsbxUlpiFsm_ExtendedRegReadAddress:
        {
            _nxt->write(0, currentTime);
            _dir->write(1, currentTime);
            _state = UsbxUlpiFsm_RegReadData;
            break;
        }
        case UsbxUlpiFsm_ReceiveChirp:
        {
            _nxt->write(1, currentTime);
            _pidByteReceivedByUtmi = UsbxTrue;

            // Chirp ended
            if (_stp->read(currentTime))
            {
                _utmiIntf->_OpMode->write(0, currentTime);
                _utmiIntf->_TXValid->write(0, currentTime);
                _rxCmd = updateRxCmd(currentTime);
                _state = UsbxUlpiFsm_IDLE;
            }
            break;
        }
        case UsbxUlpiFsm_SendRXCMD:
        {
            _pidByteReceivedByUtmi = UsbxTrue;
            _dir->write(1, currentTime);

            // Wait for one cycle before de-asserting dir
            if (_rxCmdSent)
            {
                // Keep dir asserted if back-to-back changes are detected
                if ((_rxCmd != updateRxCmd(currentTime)) && _isPidData)
                {
                    _rxCmd = updateRxCmd(currentTime);
                    _dir->write(1, currentTime);
                    _nxt->write(0, currentTime);
                    _data->write(_rxCmd, currentTime);
                }
                else
                {
                    _dir->write(0, currentTime);
                    _rxCmdSent = UsbxFalse;
                    _data->write(0, currentTime);
                    _state = UsbxUlpiFsm_IDLE;

                    // Return from here, else data will not be written
                    // since dir is low
                    return UsbxTrue;
                }
            }
            else
            {
                // Update the RX CMD and send
                _rxCmd = updateRxCmd(currentTime);
                _data->write(_rxCmd, currentTime);
                _nxt->write(0, currentTime);
                _rxCmdSent = UsbxTrue;
            }
            break;
        }
        case UsbxUlpiFsm_ABORT:
        {
            // To kill time
            _isAborted = UsbxTrue;
            _state = UsbxUlpiFsm_IDLE;
            break;
        }
        default:
            break;
    }

    // De-assert TXValid if stp is high
    if (_stp->read(currentTime))
    {
        if (_dir->read(currentTime))
        {
            _abortedFromState = _state;
            _isAborted = UsbxTrue;
            _dir->write(0, currentTime);
            _nxt->write(0, currentTime);
            _state = UsbxUlpiFsm_ABORT;
        }
        else
            _nxt->write(0, currentTime);
    }

    if (_dir->read(currentTime))
        _data->setInoutDirControl(UsbxDirection_Output);
    else
        _data->setInoutDirControl(UsbxDirection_Input);

    return UsbxTrue;
}

// Updates the LineState, VBus state for sending from PHY to Link
CarbonUInt8 UsbxUlpiPhyIntf::updateRxCmd(CarbonTime currentTime)
{
    CarbonUInt8 rxCmd = 0xFF;

    // set the LineState field
    rxCmd &= 0xFC | _utmiIntf->_LineState->read(currentTime);

#ifdef USE_UTMI_LEVEL_1
    // Set the Vbus State field
    CarbonUInt2 VbusState = 0;
    if (_utmiIntf->_VbusValid->read(currentTime))
        VbusState = 3;
    else if (_utmiIntf->_AValid->read(currentTime))
        VbusState = 2;
    else
        VbusState = !(_utmiIntf->_SessEnd->read(currentTime));

    rxCmd &= 0xF3 | (VbusState << 2);
#endif

    // set the RxEvent field
    CarbonUInt2 rxEvent = 0;
    if (_utmiIntf->_RXActive->read(currentTime))
        rxEvent = _utmiIntf->_RXError->read(currentTime) ? 3 : 1;
#ifdef USE_UTMI_LEVEL_1
    else if (_utmiIntf->_DpPulldown->read(currentTime) &&
            _utmiIntf->_DmPulldown->read(currentTime) &&
            _utmiIntf->_HostDisconnect->read(currentTime))
        rxEvent = 2;
#endif

    rxCmd &= 0xCF | (rxEvent << 4);

#ifdef USE_UTMI_LEVEL_1
    // set the ID field
    rxCmd &= 0xBF | (_utmiIntf->_IdDig->read(currentTime) << 6);

    // reset the alt_int field
    rxCmd &= 0x7F;
#endif

    return rxCmd;
}

// Returns the Immediate register depending on register address
CarbonUInt8 UsbxUlpiPhyIntf::readRegisterData(CarbonTime currentTime)
{
    CarbonUInt8 regReadData = 0;

    switch(_registerAddress)
    {
        case 0x00:
        {
            regReadData = readVendorIdLow(currentTime);
            break;
        }
        case 0x01:
        {
            regReadData = readVendorIdHigh(currentTime);
            break;
        }
        case 0x02:
        {
            regReadData = readProductIdLow(currentTime);
            break;
        }
        case 0x03:
        {
            regReadData = readProductIdHigh(currentTime);
            break;
        }
        case 0x04:
        case 0x05:
        case 0x06:
        {
            regReadData = readFunctionCtrlReg(currentTime);
            break;
        }
        case 0x07:
        case 0x08:
        case 0x09:
        {
            regReadData = readInterfaceCtrlReg(currentTime);
            break;
        }
        case 0x0A:
        case 0x0B:
        case 0x0C:
        {
            regReadData = readOtgCtrlReg(currentTime);
            break;
        }
        case 0x0D:
        case 0x0E:
        case 0x0F:
        {
            regReadData = readInterruptEnableRisingReg(currentTime);
            break;
        }
        case 0x10:
        case 0x11:
        case 0x12:
        {
            regReadData = readInterruptEnableFallingReg(currentTime);
            break;
        }
        case 0x13:
        {
            regReadData = readInterruptStatusReg(currentTime);
            break;
        }
        case 0x14:
        {
            regReadData = readInterruptLatchReg(currentTime);
            break;
        }
        case 0x15:
        {
            regReadData = readDebugReg(currentTime);
            break;
        }
        case 0x16:
        case 0x17:
        case 0x18:
        {
            regReadData = readScratchReg(currentTime);
            break;
        }
        case 0x19:
        case 0x1A:
        case 0x1B:
        {
            regReadData = readCarkitCtrlReg(currentTime);
            break;
        }
        case 0x1C:
        {
            regReadData = readCarkitIntrDelayReg(currentTime);
            break;
        }
        case 0x1D:
        case 0x1E:
        case 0x1F:
        {
            regReadData = readCarkitIntrEnableReg(currentTime);
            break;
        }
        case 0x20:
        {
            regReadData = readCarkitIntrStatusReg(currentTime);
            break;
        }
        case 0x21:
        {
            regReadData = readCarkitIntrLatchReg(currentTime);
            break;
        }
        case 0x22:
        case 0x23:
        case 0x24:
        {
            regReadData = readCarkitPulseCtrlReg(currentTime);
            break;
        }
        case 0x25:
        {
            regReadData = readTransmitPositiveWidthReg(currentTime);
            break;
        }
        case 0x26:
        {
            regReadData = readTransmitNegativeWidthReg(currentTime);
            break;
        }
        case 0x27:
        {
            regReadData = readReceivePolarityReg(currentTime);
            break;
        }
        case 0x2F:
        {
            regReadData = readExtendedReg(currentTime);
            break;
        }
        default: break;
    }
    return regReadData;
}

// Updates the Immediate register depending on register address
void UsbxUlpiPhyIntf::updateRegister(CarbonTime currentTime)
{
    switch(_registerAddress)
    {
        case 0x04:
        {
            processFunctionCtrlReg(currentTime);
            break;
        }
        case 0x05:
        {
            setFunctionCtrlReg(currentTime);
            break;
        }
        case 0x06:
        {
            clearFunctionCtrlReg(currentTime);
            break;
        }
        case 0x07:
        {
            processInterfaceCtrlReg(currentTime);
            break;
        }
        case 0x08:
        {
            setInterfaceCtrlReg(currentTime);
            break;
        }
        case 0x09:
        {
            clearInterfaceCtrlReg(currentTime);
            break;
        }
        case 0x0A:
        {
            processOtgCtrlReg(currentTime);
            break;
        }
        case 0x0B:
        {
            setOtgCtrlReg(currentTime);
            break;
        }
        case 0x0C:
        {
            clearOtgCtrlReg(currentTime);
            break;
        }
        case 0x0D:
        {
            processInterruptEnableRisingReg(currentTime);
            break;
        }
        case 0x0E:
        {
            setInterruptEnableRisingReg(currentTime);
            break;
        }
        case 0x0F:
        {
            clearInterruptEnableRisingReg(currentTime);
            break;
        }
        case 0x10:
        {
            processInterruptEnableFallingReg(currentTime);
            break;
        }
        case 0x11:
        {
            setInterruptEnableFallingReg(currentTime);
            break;
        }
        case 0x12:
        {
            clearInterruptEnableFallingReg(currentTime);
            break;
        }
        case 0x16:
        {
            processScratchReg(currentTime);
            break;
        }
        case 0x17:
        {
            setScratchReg(currentTime);
            break;
        }
        case 0x18:
        {
            clearScratchReg(currentTime);
            break;
        }
        case 0x19:
        {
            processCarkitCtrlReg(currentTime);
            break;
        }
        case 0x1A:
        {
            setCarkitCtrlReg(currentTime);
            break;
        }
        case 0x1B:
        {
            clearCarkitCtrlReg(currentTime);
            break;
        }
        case 0x1C:
        {
            processCarkitIntrDelayReg(currentTime);
            break;
        }
        case 0x1D:
        {
            processCarkitIntrEnableReg(currentTime);
            break;
        }
        case 0x1E:
        {
            setCarkitIntrEnableReg(currentTime);
            break;
        }
        case 0x1F:
        {
            clearCarkitIntrEnableReg(currentTime);
            break;
        }
        case 0x22:
        {
            processCarkitPulseCtrlReg(currentTime);
            break;
        }
        case 0x23:
        {
            setCarkitPulseCtrlReg(currentTime);
            break;
        }
        case 0x24:
        {
            clearCarkitPulseCtrlReg(currentTime);
            break;
        }
        case 0x25:
        {
            processTransmitPositiveWidthReg(currentTime);
            break;
        }
        case 0x26:
        {
            processTransmitNegativeWidthReg(currentTime);
            break;
        }
        case 0x27:
        {
            processReceivePolarityReg(currentTime);
            break;
        }
        case 0x2F:
        {
            processExtendedReg(currentTime);
            break;
        }
        default: break;
    }
}

// The following routines implement the read, write, set, clear operations 
// on the immediate register set. For write operation, pattern on the data
// bus will be written over all bits of the register. For set, operation
// pattern on the data bus is OR'd with and written to the register. For
// clear, pattern on the data bus is a mask. If a bit in the mask is set,
// then the corresponding register bit will be set to zero (cleared)

CarbonUInt8 UsbxUlpiPhyIntf::readFunctionCtrlReg(CarbonTime currentTime)
{
    CarbonUInt8 functionControlReg = 0;

    // SuspendM pin updation
    functionControlReg |= _utmiIntf->_SuspendM->read(currentTime) << 6;

    // Reset pin updation
    functionControlReg |= _utmiIntf->_Reset->read(currentTime) << 5;

    // OpMode pin updation
    functionControlReg |= _utmiIntf->_OpMode->read(currentTime) << 3;

    // TermSelect pin updation
    functionControlReg |= _utmiIntf->_TermSelect->read(currentTime) << 2;

    // XcvrSelect pin updation
    functionControlReg |= _utmiIntf->_XcvrSelect->read(currentTime);

    return functionControlReg;
}

// Updates the Function control register with Link Data
void UsbxUlpiPhyIntf::processFunctionCtrlReg(CarbonTime currentTime)
{
    CarbonUInt8 funcCtrlReg = _data->read(currentTime);

    // Update Function Control Registers
    _utmiIntf->_XcvrSelect->write(funcCtrlReg & 0x03, currentTime);
    _utmiIntf->_TermSelect->write((funcCtrlReg & 0x04) >> 2, currentTime);
    _utmiIntf->_OpMode->write((funcCtrlReg & 0x18) >> 3, currentTime);
    _utmiIntf->_Reset->write((funcCtrlReg & 0x20) >> 5, currentTime);
    _utmiIntf->_SuspendM->write((funcCtrlReg & 0x40) >> 6, currentTime);
}

// Clears the OTG control register 
void UsbxUlpiPhyIntf::clearFunctionCtrlReg(CarbonTime currentTime)
{
    CarbonUInt8 funcCtrlReg = _data->read(currentTime);

    if ((funcCtrlReg & 0x03) == 0x03)
        _utmiIntf->_XcvrSelect->write(0, currentTime);

    if (funcCtrlReg & 0x04)
        _utmiIntf->_TermSelect->write(0, currentTime);

    if ((funcCtrlReg & 0x18) == 0x18)
        _utmiIntf->_OpMode->write(0, currentTime);

    if (funcCtrlReg & 0x20)
        _utmiIntf->_Reset->write(0, currentTime);

    if (funcCtrlReg & 0x40)
        _utmiIntf->_SuspendM->write(0, currentTime);
}

// Sets the Function control register 
void UsbxUlpiPhyIntf::setFunctionCtrlReg(CarbonTime currentTime)
{
    CarbonUInt8 funcCtrlReg = _data->read(currentTime);

    _utmiIntf->_XcvrSelect->write((funcCtrlReg & 0x03) ||
            _utmiIntf->_XcvrSelect->read(currentTime), currentTime);
    _utmiIntf->_TermSelect->write(((funcCtrlReg & 0x04) >> 2) ||
            _utmiIntf->_TermSelect->read(currentTime), currentTime);
    _utmiIntf->_OpMode->write(((funcCtrlReg & 0x18) >> 3) ||
            _utmiIntf->_OpMode->read(currentTime), currentTime);
    _utmiIntf->_Reset->write(((funcCtrlReg & 0x20) >> 5) ||
            _utmiIntf->_Reset->read(currentTime), currentTime);
    _utmiIntf->_SuspendM->write(((funcCtrlReg & 0x40) >> 6) ||
            _utmiIntf->_SuspendM->read(currentTime), currentTime);
}

// Returns the OTG control register
CarbonUInt8 UsbxUlpiPhyIntf::readOtgCtrlReg(CarbonTime currentTime)
{
    CarbonUInt8 otgControlReg = 0;

    // DrvVBus pin updation
    otgControlReg |= _utmiIntf->_DrvVbus->read(currentTime) << 5;

    // ChrgVbus pin updation
    otgControlReg |= _utmiIntf->_ChrgVbus->read(currentTime) << 4;

    // DischrgVbus pin updation
    otgControlReg |= _utmiIntf->_DischrgVbus->read(currentTime) << 3;

    // DmPulldown pin updation
    otgControlReg |= _utmiIntf->_DmPulldown->read(currentTime) << 2;

    // DpPulldown pin updation
    otgControlReg |= _utmiIntf->_DpPulldown->read(currentTime) << 1;

    // IdPullup pin updation
    otgControlReg |= _utmiIntf->_IdPullup->read(currentTime);

    return otgControlReg;
}

// Updates the PHY OTG Control register with Link Data
void UsbxUlpiPhyIntf::processOtgCtrlReg(CarbonTime currentTime)
{
    CarbonUInt8 otgCtrlReg = _data->read(currentTime);

    _utmiIntf->_IdPullup->write(otgCtrlReg & 0x01, currentTime);
    _utmiIntf->_DpPulldown->write((otgCtrlReg & 0x02) >> 1, currentTime);
    _utmiIntf->_DmPulldown->write((otgCtrlReg & 0x04) >> 2, currentTime);
    _utmiIntf->_DischrgVbus->write((otgCtrlReg & 0x08) >> 3, currentTime);
    _utmiIntf->_ChrgVbus->write((otgCtrlReg & 0x10) >> 4, currentTime);
    _utmiIntf->_DrvVbus->write((otgCtrlReg & 0x20) >> 5, currentTime);
}

// Clears the OTG control register 
void UsbxUlpiPhyIntf::clearOtgCtrlReg(CarbonTime currentTime)
{
    CarbonUInt8 otgCtrlReg = _data->read(currentTime);

    if (otgCtrlReg & 0x01)
        _utmiIntf->_IdPullup->write(0, currentTime);

    if (otgCtrlReg & 0x02)
        _utmiIntf->_DpPulldown->write(0, currentTime);

    if (otgCtrlReg & 0x04)
        _utmiIntf->_DmPulldown->write(0, currentTime);

    if (otgCtrlReg & 0x08)
        _utmiIntf->_DischrgVbus->write(0, currentTime);

    if (otgCtrlReg & 0x10)
        _utmiIntf->_ChrgVbus->write(0, currentTime);

    if (otgCtrlReg & 0x20)
        _utmiIntf->_DrvVbus->write(0, currentTime);
    // Need to set DrvVbus External and UseExternal 
    // VbusIndicator pins as well
}

// Sets the OTG control register 
void UsbxUlpiPhyIntf::setOtgCtrlReg(CarbonTime currentTime)
{
    CarbonUInt8 otgCtrlReg = _data->read(currentTime);

    _utmiIntf->_IdPullup->write((otgCtrlReg & 0x01) ||
            _utmiIntf->_IdPullup->read(currentTime), currentTime);
    _utmiIntf->_DpPulldown->write(((otgCtrlReg & 0x02) >> 1) ||
            _utmiIntf->_DpPulldown->read(currentTime), currentTime);
    _utmiIntf->_DmPulldown->write(((otgCtrlReg & 0x04) >> 2) ||
            _utmiIntf->_DmPulldown->read(currentTime), currentTime);
    _utmiIntf->_DischrgVbus->write(((otgCtrlReg & 0x08) >> 3) ||
            _utmiIntf->_DischrgVbus->read(currentTime), currentTime);
    _utmiIntf->_ChrgVbus->write(((otgCtrlReg & 0x10) >> 4) ||
            _utmiIntf->_ChrgVbus->read(currentTime), currentTime);
    _utmiIntf->_DrvVbus->write(((otgCtrlReg & 0x20) >> 5) ||
            _utmiIntf->_DrvVbus->read(currentTime), currentTime);
    // Need to set DrvVbus External and UseExternal 
    // VbusIndicator pins as well
}

CarbonUInt8 UsbxUlpiPhyIntf::readVendorIdLow(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readVendorIdHigh(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readProductIdLow(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readProductIdHigh(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readInterfaceCtrlReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::processInterfaceCtrlReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::clearInterfaceCtrlReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::setInterfaceCtrlReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readInterruptEnableRisingReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}
    
void UsbxUlpiPhyIntf::processInterruptEnableRisingReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::clearInterruptEnableRisingReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::setInterruptEnableRisingReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readInterruptEnableFallingReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::processInterruptEnableFallingReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::clearInterruptEnableFallingReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::setInterruptEnableFallingReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readInterruptStatusReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readInterruptLatchReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readDebugReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readScratchReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::processScratchReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::clearScratchReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::setScratchReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readCarkitCtrlReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::processCarkitCtrlReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::clearCarkitCtrlReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::setCarkitCtrlReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readCarkitIntrDelayReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::processCarkitIntrDelayReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readCarkitIntrEnableReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::processCarkitIntrEnableReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::clearCarkitIntrEnableReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::setCarkitIntrEnableReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readCarkitIntrStatusReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readCarkitIntrLatchReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readCarkitPulseCtrlReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::processCarkitPulseCtrlReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::clearCarkitPulseCtrlReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::setCarkitPulseCtrlReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readTransmitPositiveWidthReg(CarbonTime currentTime)
{
    return 0;
    currentTime= 0;
}

void UsbxUlpiPhyIntf::processTransmitPositiveWidthReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readTransmitNegativeWidthReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::processTransmitNegativeWidthReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readReceivePolarityReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::processReceivePolarityReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

CarbonUInt8 UsbxUlpiPhyIntf::readExtendedReg(CarbonTime currentTime)
{
    return 0;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::processExtendedReg(CarbonTime currentTime)
{
    return;
    currentTime = 0;
}

UsbxBool UsbxUlpiPhyIntf::refreshInputs(CarbonTime currentTime)
{
    UsbxBool retValue = UsbxTrue;
    retValue &= _stp->refresh(currentTime);
    retValue &= _data->refresh(currentTime);
    return retValue;
}

UsbxBool UsbxUlpiPhyIntf::refreshOutputs(CarbonTime currentTime)
{
    UsbxBool retValue = UsbxTrue;
    retValue &= _dir->refresh(currentTime);
    retValue &= _nxt->refresh(currentTime);
    retValue &= _data->refresh(currentTime);
    return retValue;
}

#ifdef _VCD
UsbxBool UsbxUlpiPhyIntf::dumpUtmiIntfPins(CarbonTime currentTime) const
{
    // Dump UTMI Pins using the dump function to resolve scope
    vcdDump(_utmiIntf->_SuspendM);
    vcdDump(_utmiIntf->_DataInLo);
    vcdDump(_utmiIntf->_TXValid);
    vcdDump(_utmiIntf->_TXReady);
    vcdDump(_utmiIntf->_DataOutLo);
    vcdDump(_utmiIntf->_RXValid);
    vcdDump(_utmiIntf->_RXActive);
    vcdDump(_utmiIntf->_XcvrSelect);
    vcdDump(_utmiIntf->_TermSelect);
    vcdDump(_utmiIntf->_OpMode);
    vcdDump(_utmiIntf->_LineState);
    vcdDump(_utmiIntf->_DpPulldown);
    vcdDump(_utmiIntf->_DmPulldown);
    vcdDump(_utmiIntf->_HostDisconnect);
    vcdDump(_utmiIntf->_AValid);
    vcdDump(_utmiIntf->_BValid);
    vcdDump(_utmiIntf->_SessEnd);
    vcdDump(_utmiIntf->_VbusValid);
    vcdDump(_utmiIntf->_DrvVbus);
    vcdDump(_utmiIntf->_IdDig);
    vcdDump(_utmiIntf->_IdPullup);

    return UsbxTrue;
    currentTime = 0;
}

void UsbxUlpiPhyIntf::vcdDump(UsbxPin *pin) const
{
    FILE *vcdFile = UsbXtor::getVcdFile();
    if (vcdFile)
    {
        if (pin->getSize() > 1)
        {
            fprintf(vcdFile, "b");
            for (Int i = pin->getSize() - 1; i >= 0; i--)
            /* Don't make 'i' UInt */
            {
                CarbonUInt32 mask = 0x1 << i;
                fprintf(vcdFile, "%u", (pin->read(0) & mask) >> i);
            }
            fprintf(vcdFile, " ");
        }
        else
            fprintf(vcdFile, "%u", pin->read(0));
        fprintf(vcdFile, "%s.%s\n", _moduleName, pin->getName());
    }
}
#endif
