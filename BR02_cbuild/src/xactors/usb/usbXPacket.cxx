/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    usbXPacket.cpp
    Purpose: This file provides the member function definition for packets,
             packet encoding and decoding logic.
*/

#include "usbXPacket.h"
#include "usbXtor.h"

void UsbxPacket::print() const
{
    printf("%s packet:\n", getPacketTypeString());
    printFields();
    if (_receiveError != UsbxPacketReceiveError_None)
        printf("Packet has a %s error.\n", getPacketErrorString());
    const UsbxByte *byteArray = _byteArray;
    if (byteArray == NULL)
        return;
    printf("Bit stream (LSB:MSB): ");
    UInt byteArraySize = getSizeInBytes();
    for (UInt i = 0; i < byteArraySize; i++)
        printBits(byteArray[i]);
    printf("\n");
}

UsbxPacketType UsbxPacket::getPacketType(const UsbxByte *byteArray)
{
    if (byteArray == NULL)
        return UsbxPacket_UNKNOWN;

    return UsbxPacketType(byteArray[0] & 0xF);
}

UsbxPacket* UsbxPacket::getPacket(UInt sizeInBytes, const UsbxByte *byteArray)
{
    UsbxPacket *packet = NULL;
    switch (getPacketType(byteArray))
    {
        case UsbxPacket_OUT:
        {
            packet = new UsbxOutPacket(sizeInBytes, byteArray);
            break;
        }
        case UsbxPacket_IN:
        {
            packet = new UsbxInPacket(sizeInBytes, byteArray);
            break;
        }
        case UsbxPacket_SETUP:
        {
            packet = new UsbxSetupPacket(sizeInBytes, byteArray);
            break;
        }
        case UsbxPacket_SOF:
        {
            packet = new UsbxSofPacket(sizeInBytes, byteArray);
            break;
        }
        case UsbxPacket_DATA0:
        {
            packet = new UsbxData0Packet(sizeInBytes, byteArray, UsbxTrue);
            break;
        }
        case UsbxPacket_DATA1:
        {
            packet = new UsbxData1Packet(sizeInBytes, byteArray, UsbxTrue);
            break;
        }
        case UsbxPacket_DATA2:
        {
            packet = new UsbxData2Packet(sizeInBytes, byteArray, UsbxTrue);
            break;
        }
        case UsbxPacket_MDATA:
        {
            packet = new UsbxMDataPacket(sizeInBytes, byteArray, UsbxTrue);
            break;
        }
        case UsbxPacket_ACK:
        {
            packet = new UsbxAckPacket(sizeInBytes, byteArray);
            break;
        }
        case UsbxPacket_NAK:
        {
            packet = new UsbxNakPacket(sizeInBytes, byteArray);
            break;
        }
        case UsbxPacket_STALL:
        {
            packet = new UsbxStallPacket(sizeInBytes, byteArray);
            break;
        }
        case UsbxPacket_NYET:
        {
            packet = new UsbxNyetPacket(sizeInBytes, byteArray);
            break;
        }
        case UsbxPacket_SPLIT:
        {
            packet = new UsbxSplitPacket(sizeInBytes, byteArray);
            break;
        }
        case UsbxPacket_PING:
        {
            packet = new UsbxPingPacket(sizeInBytes, byteArray);
            break;
        }
        case UsbxPacket_ERR:
        {
            packet = new UsbxErrPacket(sizeInBytes, byteArray);
            break;
        }
        default:
            // ASSERT(0, NULL);
            return NULL;
    }
    if (packet->hasReceiveError())
    {
        DELETE(packet->_byteArray);
        packet->_byteArray = NULL;
    }
    return packet;
}

UsbxConstString UsbxPacket::getPacketTypeString() const
{
    switch (getType())
    {
        case UsbxPacket_OUT:
            return "OUT";
        case UsbxPacket_IN:
            return "IN";
        case UsbxPacket_SOF:
            return "SOF";
        case UsbxPacket_SETUP:
            return "SETUP";
        case UsbxPacket_DATA0:
            return "DATA0";
        case UsbxPacket_DATA1:
            return "DATA1";
        case UsbxPacket_DATA2:
            return "DATA2";
        case UsbxPacket_MDATA:
            return "MDATA";
        case UsbxPacket_ACK:
            return "ACK";
        case UsbxPacket_NAK:
            return "NAK";
        case UsbxPacket_STALL:
            return "STALL";
        case UsbxPacket_NYET:
            return "NYET";
        case UsbxPacket_SPLIT:
            return "SPLIT";
        case UsbxPacket_PING:
            return "PING";
        case UsbxPacket_ERR:
            return "ERR";
        default:
            ASSERT(0, NULL);
            return NULL;
    }
    return NULL;
}

UsbxConstString UsbxPacket::getPacketErrorString() const
{
    switch (_receiveError)
    {
        case UsbxPacketReceiveError_None:
            return "no";
        case UsbxPacketReceiveError_Pid:
            return "PID";
        case UsbxPacketReceiveError_Size:
            return "size";
        case UsbxPacketReceiveError_Crc:
            return "CRC";
        default:
            return "unknown";
    }
    return NULL;
}

void UsbxPacket::printBits(UsbxByte dataByte, UInt size, UsbxBool lsb2msb)
{
    for (UInt i = 0; i < size; i++)
    {
        if (lsb2msb)
        {
            printf("%d", dataByte & 0x1);
            dataByte = dataByte >> 1;
        }
        else
        {
            printf("%d", (dataByte & (1 << (size - 1))) != 0);
            dataByte = dataByte << 1;
        }
    }
}

#define Crc5ReversedCalculation // optimization

CarbonUInt5 UsbxTokenPacket::UsbxCrc5::getRevCrc() const
{
#ifdef Crc5ReversedCalculation
    return (~_crcLfsr & 0x1F); // invert and provide current CRC
#else
    CarbonUInt5 crc = 0x0, lfsr = _crcLfsr;
    for (UInt i = 0; i < 5; i++) // reversing the LSB to MSB order
    {
        crc = (crc << 1) | (lfsr & 0x1);
        lfsr = lfsr >> 1;
    }
    return (~crc & 0x1F);
#endif
}

CarbonUInt5 UsbxTokenPacket::UsbxCrc5::update(UsbxByte data, UInt dataSize)
{
#ifdef Crc5ReversedCalculation
    // all LFSR sifting would be reversed i.e. MSB to LSB,
    // as CRC is transmitted in USB in reverse order i.e. MSB to LSB.
    for (UInt i = 0; i < dataSize; i++) // data should be fed from LSB
    {
        UsbxBit feedback = (_crcLfsr ^ data) & 0x1; // feedback
        // if feedback is '1' then only internal XOR happens
        if (feedback)
            _crcLfsr = _crcLfsr ^ 0x08; // inverted G(X) = 01000
        // shifting of LFSR (note the reverse order)
        _crcLfsr = (feedback << 4) | (_crcLfsr >> 1);
        data = data >> 1;
    }
#else
    // LFSR sifting for one full byte i.e. 8 bits from LSB to MSB
    for (UInt i = 0; i < dataSize; i++) // data should be fed from LSB
    {
        UsbxBit feedback = (_crcLfsr >> 4) ^ (data & 0x1); // feedback
        // if feedback is '1' then only internal XOR happens
        if (feedback)
            _crcLfsr = _crcLfsr ^ 0x02; // G(X) = X^5 + X^2 + 1 = 00010
        // shifting of LFSR
        _crcLfsr = ((_crcLfsr << 1) | feedback) & 0x1F;
        data = data >> 1;

    }
#endif
    return getRevCrc();
}

UsbxTokenPacket::UsbxTokenPacket(CarbonUInt4 pid, CarbonUInt7 deviceAddress,
        CarbonUInt4 endpointNumber):
    UsbxPacket(NEWARRAY(UsbxByte, getSizeInBytes()))
{
    if (_byteArray == NULL)
    {
        UsbXtor::error(NULL, CarbonXUsbError_NoMemory,
                "Can't allocate memory for token packet bit stream");
        return;
    }

    _byteArray[0] = (~pid << 4) | pid;
    _byteArray[1] = ((endpointNumber & 0x1) << 7) | (deviceAddress & 0x7F);
    UsbxCrc5 tokenCrc5;
    tokenCrc5.update(_byteArray[1]);
    _byteArray[2] = (endpointNumber & 0xE) >> 1;
    tokenCrc5.update(_byteArray[2], 3);
    _byteArray[2] = ((tokenCrc5.getRevCrc() & 0x1F) << 3) | _byteArray[2];
}

UsbxTokenPacket::UsbxTokenPacket(CarbonUInt4 pid, UInt sizeInBytes,
        const UsbxByte *byteArray)
{
    if (byteArray == NULL)
    {
        _receiveError = UsbxPacketReceiveError_Unknown;
        return;
    }

    // check PID error
    if (((~byteArray[0] & 0xF) != (byteArray[0] >> 4)) || // PID error
            ((byteArray[0] & 0xF) != pid)) // PID mismatch
    {
        _receiveError = UsbxPacketReceiveError_Pid;
        return;
    }

    // check size error
    if (sizeInBytes != getSizeInBytes())
    {
        _receiveError = UsbxPacketReceiveError_Size;
        return;
    }

    // check CRC error
    UsbxCrc5 expCrc5;
    for (UInt i = 1; i < getSizeInBytes() - 1; i++)
        expCrc5.update(byteArray[i]);
    expCrc5.update(byteArray[getSizeInBytes() - 1], 3);
    if (expCrc5.getRevCrc() != (byteArray[2] >> 3))
    {
        _receiveError = UsbxPacketReceiveError_Crc;
        return;
    }

    _byteArray = NEWARRAY(UsbxByte, sizeInBytes);
    if (_byteArray == NULL)
    {
        UsbXtor::error(NULL, CarbonXUsbError_NoMemory,
                "Can't allocate memory for token packet bit stream");
        return;
    }

    for (UInt i = 0; i < getSizeInBytes(); i++)
        _byteArray[i] = byteArray[i];
}

CarbonUInt7 UsbxTokenPacket::getDeviceAddress() const
{
    if (_byteArray == NULL)
        return 0;
    return (_byteArray[1] & 0x7F);
}

CarbonUInt4 UsbxTokenPacket::getEndpointNumber() const
{
    if (_byteArray == NULL)
        return 0;
    return ((_byteArray[2] << 1) & 0xE) | (_byteArray[1] >> 7);
}

void UsbxTokenPacket::printFields() const
{
    printf("ADDR[7] (Device Address): 0x%X, ENDP[4] (Endpoint Number): 0x%X\n",
            getDeviceAddress(), getEndpointNumber());
}

UsbxSofPacket::UsbxSofPacket(CarbonUInt11 frameNumber):
    UsbxPacket(NEWARRAY(UsbxByte, getSizeInBytes()))
{
    if (_byteArray == NULL)
    {
        UsbXtor::error(NULL, CarbonXUsbError_NoMemory,
                "Can't allocate memory for token packet bit stream");
        return;
    }

    CarbonUInt4 pid = getType();
    _byteArray[0] = (~pid << 4) | pid;
    _byteArray[1] = frameNumber & 0xFF;
    UsbxTokenPacket::UsbxCrc5 tokenCrc5;
    tokenCrc5.update(_byteArray[1]);
    _byteArray[2] = (frameNumber >> 8) & 0x7;
    tokenCrc5.update(_byteArray[2], 3);
    _byteArray[2] = ((tokenCrc5.getRevCrc() & 0x1F) << 3) | _byteArray[2];
}

UsbxSofPacket::UsbxSofPacket(UInt sizeInBytes, const UsbxByte *byteArray)
{
    if (byteArray == NULL)
    {
        _receiveError = UsbxPacketReceiveError_Unknown;
        return;
    }

    // check PID error
    CarbonUInt4 pid = getType();
    if (((~byteArray[0] & 0xF) != (byteArray[0] >> 4)) || // PID error
            ((byteArray[0] & 0xF) != pid)) // PID mismatch
    {
        _receiveError = UsbxPacketReceiveError_Pid;
        return;
    }

    // check size error
    if (sizeInBytes != getSizeInBytes())
    {
        _receiveError = UsbxPacketReceiveError_Size;
        return;
    }

    // check CRC error
    UsbxTokenPacket::UsbxCrc5 expCrc5;
    for (UInt i = 1; i < getSizeInBytes() - 1; i++)
        expCrc5.update(byteArray[i]);
    expCrc5.update(byteArray[getSizeInBytes() - 1], 3);
    if (expCrc5.getRevCrc() != (byteArray[2] >> 3))
    {
        _receiveError = UsbxPacketReceiveError_Crc;
        return;
    }

    _byteArray = NEWARRAY(UsbxByte, sizeInBytes);
    if (_byteArray == NULL)
    {
        UsbXtor::error(NULL, CarbonXUsbError_NoMemory,
                "Can't allocate memory for token packet bit stream");
        return;
    }

    for (UInt i = 0; i < getSizeInBytes(); i++)
        _byteArray[i] = byteArray[i];
}

CarbonUInt7 UsbxSofPacket::getFrameNumber() const
{
    return ((_byteArray[2] & 0x7) << 8 | _byteArray[1]);
}

void UsbxSofPacket::printFields() const
{
    printf("FrameNumber[11]: 0x%X\n", getFrameNumber());
}

#define Crc16ReversedCalculation // optimization

CarbonUInt16 UsbxDataPacket::UsbxCrc16::getRevCrc() const
{
#ifdef Crc16ReversedCalculation
    return (~_crcLfsr & 0xFFFF); // invert and provide current CRC
#else
    CarbonUInt16 crc = 0x0, lfsr = _crcLfsr;
    for (UInt i = 0; i < 16; i++) // reversing the LSB to MSB order
    {
        crc = (crc << 1) | (lfsr & 0x1);
        lfsr = lfsr >> 1;
    }
    return (~crc & 0xFFFF);
#endif
}

CarbonUInt16 UsbxDataPacket::UsbxCrc16::update(UsbxByte data, UInt dataSize)
{
    dataSize = (dataSize > 8) ? 8 : dataSize;
#ifdef Crc16ReversedCalculation
    // all LFSR sifting would be reversed i.e. MSB to LSB,
    // as CRC is transmitted in USB in reverse order i.e. MSB to LSB.
    for (UInt i = 0; i < dataSize; i++) // data should be fed from LSB
    {
        UsbxBit feedback = (_crcLfsr ^ data) & 0x1; // feedback
        // if feedback is '1' then only internal XOR happens
        if (feedback)
            _crcLfsr = _crcLfsr ^ 0x4002; // inverted G(X) = 0100000000000010
        // shifting of LFSR (note the reverse order)
        _crcLfsr = (feedback << 15) | (_crcLfsr >> 1);
        data = data >> 1;
    }
#else
    // LFSR sifting for one full byte i.e. 8 bits from LSB to MSB
    for (UInt i = 0; i < dataSize; i++) // data should be fed from LSB
    {
        UsbxBit feedback = (_crcLfsr >> 15) ^ (data & 0x1); // feedback
        // if feedback is '1' then only internal XOR happens
        if (feedback)
            _crcLfsr = _crcLfsr ^ 0x4002; // G(X) = X^16 + X^15 + X^2 + 1
                                          // = 0100000000000010
        // shifting of LFSR
        _crcLfsr = ((_crcLfsr << 1) | feedback) & 0xFFFF;
        data = data >> 1;
    }
#endif
    return getRevCrc();
}

UsbxDataPacket::UsbxDataPacket(CarbonUInt4 pid, UInt sizeInBytes,
        const UsbxByte *byteArray, UsbxBool reconstruct)
{
    if (reconstruct)
    {
        _dataPayloadSizeInBytes = sizeInBytes - 3;
        if (byteArray == NULL)
        {
            _receiveError = UsbxPacketReceiveError_Unknown;
            return;
        }

        // check PID error
        if (((~byteArray[0] & 0xF) != (byteArray[0] >> 4)) || // PID error
                ((byteArray[0] & 0x3) != 0x3) || // PID not of type for data
           ((byteArray[0] & 0xF) != pid))
        {
            _receiveError = UsbxPacketReceiveError_Pid;
            return;
        }

        // check for data payload size error
        if ((getSizeInBytes() > 1027) || (sizeInBytes < 3))
        /* 1024 byte data payload (max) + 1 byte PID + 2 byte CRC */
        {
            _receiveError = UsbxPacketReceiveError_Size;
            return;
        }

        // calculate and check CRC
        UsbxCrc16 expCrc16;
        for (UInt i = 1; i < getSizeInBytes() - 2; i++)
            expCrc16.update(byteArray[i]); // excluding the PID and CRC
#ifdef Crc16ReversedCalculation
        if ((byteArray[getSizeInBytes() - 2] != (expCrc16.getRevCrc() & 0xFF))||
                ((byteArray[getSizeInBytes() - 1]) !=
                 ((expCrc16.getRevCrc() >> 8) & 0xFF)))
        {
            _receiveError = UsbxPacketReceiveError_Crc;
            return;
        }
#else
        CarbonUInt16 crc = 0x0, lfsr = expCrc16.getRevCrc();
        for (UInt i = 0; i < 16; i++) // reversing the LSB to MSB order
        {
            crc = (crc << 1) | (lfsr & 0x1);
            lfsr = lfsr >> 1;
        }

        if ((byteArray[getSizeInBytes() - 2] != (crc & 0xFF)) ||
        (byteArray[getSizeInBytes() - 1] != ((crc >> 8) & 0xFF)))
        {
            _receiveError = UsbxPacketReceiveError_Crc;
            return;
        }
#endif

        // Copy bytes
        _byteArray = NEWARRAY(UsbxByte, sizeInBytes);
        if (_byteArray == NULL)
        {
            UsbXtor::error(NULL, CarbonXUsbError_NoMemory,
                    "Can't allocate memory for data packet bit stream");
            return;
        }

        for (UInt i = 0; i < getSizeInBytes(); i++)

        _byteArray[i] = byteArray[i];
    }
    else
    {
        _dataPayloadSizeInBytes = sizeInBytes;
        _byteArray = NEWARRAY(UsbxByte, getSizeInBytes());
        if (_byteArray == NULL)
        {
            UsbXtor::error(NULL, CarbonXUsbError_NoMemory,
                    "Can't allocate memory for data packet bit stream");
            return;
        }

        _byteArray[0] = (~pid << 4) | pid;

        // check for data payload size error
        if ((getSizeInBytes() > 1027) || (getSizeInBytes() < 3))
        /* 1024 byte data payload (max) + 1 byte PID + 2 byte CRC */
        {
            _receiveError = UsbxPacketReceiveError_Size;
            return;
        }

        UsbxCrc16 tokenCrc16;

        for (UInt i = 0; i < sizeInBytes; i++)
        {
            _byteArray[i + 1] = byteArray[i];
            tokenCrc16.update(byteArray[i]);
        }
#ifdef Crc16ReversedCalculation
        _byteArray[getSizeInBytes() - 2] = tokenCrc16.getRevCrc() & 0xFF;
        _byteArray[getSizeInBytes() - 1] = (tokenCrc16.getRevCrc() >> 8) & 0xFF;
#else
        CarbonUInt16 crc = 0x0, lfsr = tokenCrc16.getRevCrc();
        for (UInt i = 0; i < 16; i++) // reversing the LSB to MSB order
        {
            crc = (crc << 1) | (lfsr & 0x1);
            lfsr = lfsr >> 1;
        }
        _byteArray[getSizeInBytes() - 2] = crc & 0xFF;
        _byteArray[getSizeInBytes() - 1] = (crc >> 8) & 0xFF;
#endif
    }
}

void UsbxDataPacket::printFields() const
{
    printf("Data size: %d\n", getPayloadSize());
    if (_byteArray == NULL)
        return;
    for(UInt i = 1; i < getSizeInBytes() - 2; i++)
        printf("DATA[%d] : 0x%X\n", i, _byteArray[i]);
}

UsbxHandshakePacket::UsbxHandshakePacket(CarbonUInt4 pid) :
    UsbxPacket(NEWARRAY(UsbxByte, getSizeInBytes()))
{
    if (_byteArray == NULL)
    {
        UsbXtor::error(NULL, CarbonXUsbError_NoMemory,
                "Can't allocate memory for handshake packet bit stream");
        return;
    }
    _byteArray[0] = (~pid << 4) | pid;
}

UsbxHandshakePacket::UsbxHandshakePacket(CarbonUInt4 pid, UInt sizeInBytes,
        const UsbxByte *byteArray)
{
    if (byteArray == NULL)
    {
        _receiveError = UsbxPacketReceiveError_Unknown;
        return;
    }

    // check PID error
    if (((~byteArray[0] & 0xF) != (byteArray[0] >> 4)) || // PID error
            ((byteArray[0] & 0xF) != pid)) // PID mismatch
    {
        _receiveError = UsbxPacketReceiveError_Pid;
        return;
    }

    // check size error
    if (sizeInBytes != getSizeInBytes())
    {
        _receiveError = UsbxPacketReceiveError_Size;
        return;
    }

    _byteArray = NEWARRAY(UsbxByte, sizeInBytes);
    if (_byteArray == NULL)
    {
        UsbXtor::error(NULL, CarbonXUsbError_NoMemory,
                "Can't allocate memory for handshake packet bit stream");
        return;
    }

    for (UInt i = 0; i < getSizeInBytes(); i++)
        _byteArray[i] = byteArray[i];
}

UsbxSplitPacket::UsbxSplitPacket(CarbonUInt7 hubAddress, CarbonUInt1 sc,
        CarbonUInt7 port, CarbonUInt1 speed, CarbonUInt1 end,
        CarbonUInt2 endPointType):
    UsbxPacket(NEWARRAY(UsbxByte, getSizeInBytes()))
{
    if (_byteArray == NULL)
    {
        UsbXtor::error(NULL, CarbonXUsbError_NoMemory,
                "Can't allocate memory for token packet bit stream");
        return;
    }
    CarbonUInt4 pid = getType();
    _byteArray[0] = (~pid << 4) | pid;
    _byteArray[1] = ((sc & 0x1) << 7) | (hubAddress & 0x7F);
    UsbxTokenPacket::UsbxCrc5 tokenCrc5;
    tokenCrc5.update(_byteArray[1]);
    _byteArray[2] = ((speed & 0x1) << 7) | (port & 0x7F);
    tokenCrc5.update(_byteArray[2]);
    _byteArray[3] = ((endPointType & 0x3) << 1) | (end & 0x1);
    tokenCrc5.update(_byteArray[3], 3);
    _byteArray[3] = ((tokenCrc5.getRevCrc() & 0x1F) << 3) | _byteArray[3];
}

UsbxSplitPacket::UsbxSplitPacket(UInt sizeInBytes, const UsbxByte *byteArray)
{
    CarbonUInt4 pid = getType();
    if (byteArray == NULL)
    {
        _receiveError = UsbxPacketReceiveError_Unknown;
        return;
    }

    // check PID error
    if (((~byteArray[0] & 0xF) != (byteArray[0] >> 4)) || // PID error
            ((byteArray[0] & 0xF) != pid)) // PID mismatch
    {
        _receiveError = UsbxPacketReceiveError_Pid;
        return;
    }

    // check size error
    if (sizeInBytes != getSizeInBytes())
    {
        _receiveError = UsbxPacketReceiveError_Size;
        return;
    }

    // check CRC error
    UsbxTokenPacket::UsbxCrc5 expCrc5;
    for (UInt i = 1; i < getSizeInBytes() - 1; i++)
        expCrc5.update(byteArray[i]);
    expCrc5.update(byteArray[getSizeInBytes() - 1], 3);
    if (expCrc5.getRevCrc() != (byteArray[3] >> 3))
    {
        _receiveError = UsbxPacketReceiveError_Crc;
        return;
    }

    _byteArray = NEWARRAY(UsbxByte, sizeInBytes);
    if (_byteArray == NULL)
    {
        UsbXtor::error(NULL, CarbonXUsbError_NoMemory,
                "Can't allocate memory for token packet bit stream");
        return;
    }

    for (UInt i = 0; i < getSizeInBytes(); i++)
        _byteArray[i] = byteArray[i];
}

void UsbxSplitPacket::printFields() const
{
    printf("HUB_ADDR[7] (Hub Address): 0x%X, ", getHubAddress());
    printf("SC[1] (Start or Complete Split): 0x%X\n", getSC());
    printf("PORT[7] (Port Number): 0x%X, ", getPortNumber());
    printf("S[1] (Speed): 0x%X, ", getSpeed());
    printf("E[1] (End): 0x%X\n", getEnd());
    printf("ET[2] (Endpoint Type): 0x%X\n", getEndpointType());
}

