/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_ARRAY_H
#define USBX_ARRAY_H

#include "usbXDefines.h"

/*!
 * \file usbXTransArray.h
 * \brief Header file for array implementation.

 * This file provides the class definition for templatized array.
 */

//! \brief Array class (templatized)
template <typename ArrayData>
class UsbxArray
{
public: CARBONMEM_OVERRIDES

private:

    UInt _count;                //!< No of datas in array
    UInt _size;                 //!< Max. no of datas in array
    ArrayData **_array;         //!< Actual array

public:

    UsbxArray(UInt size = 10);  // default size
    ~UsbxArray();
    void reset() { _count = 0; }

    // Member access functions
    UInt getCount() const { return _count; }
    ArrayData * const *getArray() const { return _array; }

    // Adds new data
    UsbxBool add(ArrayData *data);

    // Removes a particular data from array
    UsbxBool remove(ArrayData *data);
};

template <typename ArrayData>
UsbxArray<ArrayData>::UsbxArray(UInt size): _count(0), _size(size),
    _array(NEWARRAY(ArrayData*, _size))
{}

template <typename ArrayData>
UsbxArray<ArrayData>::~UsbxArray()
{
    DELETE(_array);
}

template <typename ArrayData>
UsbxBool UsbxArray<ArrayData>::add(ArrayData *data)
{
    if (_count == _size) // If more array size is required
    {
        _size *= 2;
        ArrayData **newArray = NEWARRAY(ArrayData*, _size);
        if (newArray == NULL)
            return UsbxFalse;
        for (UInt i = 0; i < _count; i++)
            newArray[i] = _array[i];
        DELETE(_array);
        _array = newArray;
    }

    // Add new data
    _array[_count++] = data;
    return UsbxTrue;
}

// Deletes a particular dataaction if exists in array
template <typename ArrayData>
UsbxBool UsbxArray<ArrayData>::remove(ArrayData *data)
{
    UsbxBool matched = UsbxFalse;
    for (UInt i = 0; i < _count; i++)
    {
        if (_array[i] == data)
        {
            matched = UsbxTrue;
            _count--;
        }
        if (matched)
            _array[i] = _array[i + 1];
    }
    return matched;
}

#endif
