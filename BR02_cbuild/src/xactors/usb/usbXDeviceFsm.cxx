/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    usbXDeviceFsm.cpp
    Purpose: This file provides implementation for device FSM.
*/

#include "usbXDeviceFsm.h"
#include "usbXTrans.h"
#include "usbXtor.h"

// #define DEBUG
// #define RANDOM_RETRY

UsbxDeviceFsm::UsbxDeviceFsm(UsbxDeviceXtor *xtor): UsbxFsm(), _xtor(xtor),
    _state(_xtor->getConfig()->getIntfSide() == CarbonXUsbIntfSide_PHY ?
            UsbxDeviceState_Detached : UsbxDeviceState_Reset),
    _currentTransUnit(NULL), _timeWaited(0), _handshakePacket(NULL)
{}

UsbxDeviceFsm::~UsbxDeviceFsm()
{
    delete _currentTransUnit;
    delete _handshakePacket;
}

UsbxBool UsbxDeviceFsm::attach()
{
    UsbxIntf *interface = _xtor->getInterface();
    if (interface == NULL || _state != UsbxDeviceState_Detached ||
            _xtor->getConfig()->getIntfSide() != CarbonXUsbIntfSide_PHY)
        return UsbxFalse;
    interface->startTiming(UsbxTiming_DeviceAttach);
    _state = UsbxDeviceState_Attached;
    // Call reportCommand(Attach) at device
    _xtor->reportCommand(CarbonXUsbCommand_DeviceAttach);
    return UsbxTrue;
}

UsbxBool UsbxDeviceFsm::detach()
{
    UsbxIntf *interface = _xtor->getInterface();
    if (interface == NULL || _state == UsbxDeviceState_Detached ||
            _xtor->getConfig()->getIntfSide() != CarbonXUsbIntfSide_PHY)
        return UsbxFalse;

    // Halt the state machine
    if (_state > UsbxDeviceState_IDLE)
        updateForHalt(0);
    else if (_currentTransUnit != NULL)
        delete _currentTransUnit->getTrans();
    _state = UsbxDeviceState_Detached;
    delete _currentTransUnit;
    _currentTransUnit = NULL;

    // Reset the transactor
    _xtor->resetForDetach();
    interface->startTiming(UsbxTiming_DeviceDetach);

    // Call reportCommand(Detach) at device
    _xtor->reportCommand(CarbonXUsbCommand_DeviceDetach);

    return UsbxTrue;
}

UsbxBool UsbxDeviceFsm::evaluate(CarbonTime currentTime)
{
    UsbxBool retStatus = UsbxTrue;

    // Get commonly used variables
    UsbxIntf *interface = _xtor->getInterface();
    ASSERT(interface, _xtor);
    UsbxBool isFS = _xtor->isFS();
    UInt busWidth = _xtor->getConfig()->getUtmiDataBusWidth();
    UsbxPacket *packet = NULL;

#ifdef DEBUG
    UsbxDeviceState lastState = _state;
#endif

    // Device Finite State Machine
    switch (_state)
    {
        case UsbxDeviceState_Detached:
        {
            break;
        }
        case UsbxDeviceState_Attached:
        {
            if (interface->timingComplete())
                _state = UsbxDeviceState_Reset;
            break;
        }
        case UsbxDeviceState_Reset:
        {
            if (!interface->timingComplete())
                break;
            // Start reset and HS detection timing
            if (_xtor->getConfig()->getSpeed() == CarbonXUsbSpeed_FS)
                interface->startTiming(UsbxTiming_DeviceFsReset);
            else
                interface->startTiming(UsbxTiming_DeviceHsReset);
            _state = UsbxDeviceState_HsDetect;
            break;
        }
        case UsbxDeviceState_HsDetect:
        {
            if (!interface->timingComplete())
                break;
            // If HS detected, set current operating speed to HS
            _xtor->setSpeedFS(!interface->senseTiming(UsbxTiming_HsDetected),
                    currentTime);
            _state = UsbxDeviceState_IDLE;
            break;
        }
        case UsbxDeviceState_Suspended:
        {
            if (interface->senseTiming(UsbxTiming_Resume))
            {
                _state = UsbxDeviceState_IDLE;
#ifdef _DEBUG
                printf("DEBUG[%d]: Device Fsm: Resuming\n",
                    static_cast<UInt>(currentTime));
#endif
                // Call reportCommand(Resume) at device
                _xtor->reportCommand(CarbonXUsbCommand_Resume);
            }
            else if (_driveTiming == UsbxDriveTiming_RemoteWakeUp &&
                    interface->timingComplete())
            {
                interface->startTiming(UsbxTiming_RemoteWakeUp);
                _driveTiming = UsbxDriveTiming_None;
            }
            break;
        }
        case UsbxDeviceState_IDLE:
        {
            // Initialize state related variables
            _currentTransUnit = NULL;
            _timeWaited = 0;

            if (interface->senseTiming(UsbxTiming_Suspend))
            {
                _state = UsbxDeviceState_Suspended;
#ifdef _DEBUG
                printf("DEBUG[%d]: Device Fsm: Suspending\n",
                    static_cast<UInt>(currentTime));
#endif
                // Call reportCommand(Suspend) at device
                _xtor->reportCommand(CarbonXUsbCommand_Suspend);
                break;
            }

            // Wait for token or PING packet
            packet = interface->receivePacket();
            if (packet == NULL ||
                    packet->hasReceiveError() != UsbxPacketReceiveError_None)
            {
                delete packet;
                break;
            }

            // Check for token packet and extract packet information
            UsbxPacketType packetType = packet->getType();
            CarbonXUsbPipeDirection pipeDir = CarbonXUsbPipeDirection_UNDEF;
            if (packetType == UsbxPacket_SETUP)
                pipeDir = CarbonXUsbPipeDirection_UNDEF;
            else if (packetType == UsbxPacket_OUT)
                pipeDir = CarbonXUsbPipeDirection_OUTPUT;
            else if (packetType == UsbxPacket_IN)
                pipeDir = CarbonXUsbPipeDirection_INPUT;
            else if (!isFS && packetType == UsbxPacket_PING)
                pipeDir = CarbonXUsbPipeDirection_OUTPUT;
            else
            {
                if (packetType == UsbxPacket_SOF)
                    _xtor->setFrameNumber(
                        static_cast<UsbxSofPacket*>(packet)->getFrameNumber());
                delete packet;
                break;
            }
            UsbxTokenPacket *tokenPacket =
                static_cast<UsbxTokenPacket*>(packet);
            CarbonUInt7 deviceAddress = tokenPacket->getDeviceAddress();
            CarbonUInt4 epNumber = tokenPacket->getEndpointNumber();
            UsbxBool packetError = (packet->hasReceiveError()
                                        != UsbxPacketReceiveError_None);
            delete packet;

            // If packet is in error skip
            if (packetError)
                break;

            // Check device address
            if (!_xtor->getConfig()->isHub() &&
                    deviceAddress != _xtor->getDeviceAddress())
                break;

            // Find pipe for endpoint address and direction
            UsbxPipe *pipe = _xtor->getPipe(epNumber, pipeDir);
            if (pipe == NULL) // error
            {
                UsbXtor::error(_xtor, CarbonXUsbError_InvalidTrans,
                "Transaction with invalid endpoint address appeared at device");
                return UsbxFalse;
            }
            CarbonXUsbPipeType pipeType = pipe->getType();

            // Find transaction and create current transcation unit
            // Note, for device pipes, sigleton queues will be used
            UsbxTrans *trans = pipe->getTransQueue()->top();
            if (trans == NULL)
            {
                if (pipeType == CarbonXUsbPipe_CONTROL)
                {
                    if (packetType == UsbxPacket_OUT)
                    // Host retries with toggle bit mismatch at status stage
                    {
                        _currentTransUnit = new UsbxTransUnit(NULL,
                                new UsbxOutPacket(deviceAddress, epNumber));
                        if (_currentTransUnit == NULL)
                        {
                            UsbXtor::error(_xtor, CarbonXUsbError_NoMemory,
                                "Can't allocate memory for transaction unit");
                            return UsbxFalse;
                        }
                        _state = UsbxDeviceState_Do_BCINTO_Wait_Odata;
                        break;
                    }
                    trans = new UsbxMessageTrans(pipe);
                }
                else
                    trans = new UsbxStreamTrans(pipe);
                if (trans == NULL)
                {
                    UsbXtor::error(_xtor, CarbonXUsbError_NoMemory,
                            "Can't allocate memory for incoming transaction");
                    return UsbxFalse;
                }
                // When a new transaction is created, push it into device
                // pipe and call setDefaultResponse
                pipe->getTransQueue()->push(trans);
                if (pipeType != CarbonXUsbPipe_CONTROL)
                    _xtor->setDefaultResponse(trans);
            }

            // Pop the transaction unit
            _currentTransUnit = trans->popTransUnitForRetry();
            if (_currentTransUnit == NULL)
                _currentTransUnit = trans->popNextTransUnit();
            else if (pipeType == CarbonXUsbPipe_CONTROL &&
                packetType == UsbxPacket_SETUP && _currentTransUnit->isLast() &&
                trans->getDataSize() > 0)
            // For ACK corruption at previous status stage of IN control
            {
                updateForNextTransUnit(currentTime);
                trans = new UsbxMessageTrans(pipe);
                if (trans == NULL)
                {
                    UsbXtor::error(_xtor, CarbonXUsbError_NoMemory,
                            "Can't allocate memory for incoming transaction");
                    return UsbxFalse;
                }
                pipe->getTransQueue()->push(trans);
                _currentTransUnit = trans->popNextTransUnit();
            }
            // When no data provided, errored out from inside
            if (_currentTransUnit == NULL)
                return UsbxFalse;

            // Check for protocol error
            UsbxPacketType expPacketType =
                _currentTransUnit->getTokenPacket()->getType();
            if (packetType != expPacketType &&
                    // Consider PING and OUT packets are similar
                    !(packetType == UsbxPacket_PING &&
                        expPacketType == UsbxPacket_OUT) &&
                    // ACK corruption at setup stage
                    !(pipeType == CarbonXUsbPipe_CONTROL &&
                        packetType == UsbxPacket_SETUP) &&
                    // Last control transaction should have special handling
                    !(pipeType == CarbonXUsbPipe_CONTROL &&
                        _currentTransUnit->isLast() &&
                        (packetType == UsbxPacket_PING ||
                         packetType == UsbxPacket_OUT) &&
                        expPacketType == UsbxPacket_IN))
            {
                UsbXtor::error(_xtor, CarbonXUsbError_ProtocolError,
                        "Packet type mismatch with expected");
                return UsbxFalse;
            }

            // Change state
            if ((pipeType == CarbonXUsbPipe_CONTROL &&
                    packetType == UsbxPacket_SETUP) ||
                    packetType == UsbxPacket_OUT)
            {
                // Device_Do_OUT
                if (pipeType == CarbonXUsbPipe_ISO)
                {
                    // Dev_DO_IsochO
                    _state = UsbxDeviceState_Do_IsochO_Wait_data;
                }
                else
                    // Dev_Do_BCINTO
                    _state = UsbxDeviceState_Do_BCINTO_Wait_Odata;
                // Note, Dev_HS_BCO is integrated within Dev_Do_BCINTO
                // under condition HS AND (pipeType == CONTROL || BULK)
            }
            else if (packetType == UsbxPacket_IN)
            {
                if (pipeType == CarbonXUsbPipe_ISO)
                {
                    // Dev_DO_IsochI
                    _state = UsbxDeviceState_Do_IsochI_Data;
                }
                else
                {
                    // Dev_Do_BCINTI
                    _state = UsbxDeviceState_Do_BCINTI_Data;
                }
            }
            else if (!isFS && packetType == UsbxPacket_PING)
            {
                // Dev_HS_ping
                _state = UsbxDeviceState_HS_Ping;
            }

            break;
        }
        //************ Dev_Do_BCINTO ************
        case UsbxDeviceState_Do_BCINTO_Wait_Odata:
        {
            // Wait for data packet
            packet = interface->receivePacket();
            if (packet == NULL)
                break;

#ifdef DEBUG
            printf("DEBUG[%d]: DEVICE FSM: BCINTO FSM started.\n",
                    static_cast<UInt>(currentTime));
#endif

            // If data packet is in error
            if (packet->hasReceiveError() != UsbxPacketReceiveError_None)
            {
                _state = UsbxDeviceState_IDLE;
                delete packet;
                break;
            }

            // Change state for different scenarios

            // Check for data packet
            if (packet->getType() != UsbxPacket_DATA0 &&
                    packet->getType() != UsbxPacket_DATA1)
            {
                // Not data packet
                _state = UsbxDeviceState_IDLE;
                delete packet;
                break;
            }

            UsbxDataPacket *dataPacket = static_cast<UsbxDataPacket*>(packet);

            // Check for wait state
            UsbxBool retryState = _currentTransUnit->hasRetryState();
            UsbxBool stallState = _currentTransUnit->hasStallState();

            // For setup stage of control transfer, check data to be DATA0
            if (_currentTransUnit->getTokenPacket()->getType() ==
                    UsbxPacket_SETUP)
            {
                if (retryState) // If buffer space is not avaiable
                {
                    _state = UsbxDeviceState_IDLE;
                    _currentTransUnit->incrRetryCount();
                }
                else if (packet->getType() == UsbxPacket_DATA0)
                {
                    if (_currentTransUnit->getDataPacket())
                        delete _currentTransUnit->getDataPacket();
                    _currentTransUnit->setDataPacket(dataPacket);
                    dataPacket = NULL;

                    // Accept data
                    ASSERT(_currentTransUnit->getTrans()
                        ->pushNextTransUnit(_currentTransUnit), _xtor);
                    // Call setDefaultResponse when setup information is ready
                    if (_currentTransUnit->getTrans()->getPipe()->getType() ==
                            CarbonXUsbPipe_CONTROL)
                        _xtor->setDefaultResponse(
                                _currentTransUnit->getTrans());
                    interface->transmitPacket(
                            _handshakePacket = new UsbxAckPacket());
                    _state = UsbxDeviceState_Do_BCINTO_Dopkt;
                }
                else
                    _state = UsbxDeviceState_IDLE;
            }
            else if (_currentTransUnit->getTokenPacket()->getType() ==
                    UsbxPacket_OUT)
            // For data transfer check toggle bit
            {
                if (_currentTransUnit->getTrans()->getPipe()
                        ->getNextDataPacketType() != dataPacket->getType())
                {
                    interface->transmitPacket(
                            _handshakePacket = new UsbxAckPacket());
                    updateForSameTransUnit(currentTime);
                    _state = UsbxDeviceState_Do_BCINTO_Dopkt;
                }
                else if (stallState)
                {
                    updateForHalt(currentTime);
                    delete _currentTransUnit;
                    _currentTransUnit = NULL;

                    interface->transmitPacket(
                            _handshakePacket = new UsbxStallPacket());
                    _state = UsbxDeviceState_Do_BCINTO_Dopkt;
                }
#ifdef RANDOM_RETRY
                else if ((retryState = rand() % 2))
#else
                else if (retryState)
#endif
                {
                    interface->transmitPacket(
                            _handshakePacket = new UsbxNakPacket());
                    _currentTransUnit->incrRetryCount();
                    updateForSameTransUnit(currentTime);
                    _state = UsbxDeviceState_Do_BCINTO_Dopkt;
                }
                else // Accept data
                {
                    if (_currentTransUnit->getDataPacket())
                        delete _currentTransUnit->getDataPacket();
                    _currentTransUnit->setDataPacket(dataPacket);
                    dataPacket = NULL;

                    ASSERT(_currentTransUnit->getTrans()
                        ->pushNextTransUnit(_currentTransUnit), _xtor);
                    updateForNextTransUnit(currentTime);

#ifndef _NO_HS_PING
                    // Find if NYET has to be sent for HS
                    UsbxBool sendNYet = UsbxFalse;
                    if (!isFS && !_currentTransUnit->isLast())
                    {
                        UsbxTrans *trans = _currentTransUnit->getTrans();
                        if (!trans->getPipe()->isPeriodic() &&
                            trans->hasRetry(trans->getNextTransUnitIndex()) !=
                            NULL)
                            sendNYet = UsbxTrue;
                    }
                    if (sendNYet)
                        interface->transmitPacket(
                            _handshakePacket = new UsbxNyetPacket());
                    else
#endif
                        interface->transmitPacket(
                            _handshakePacket = new UsbxAckPacket());
                    _state = UsbxDeviceState_Do_BCINTO_Dopkt;
                }
            }
            else // Host false retry at status stage
            {
                if (_currentTransUnit->getTrans()->getPipe()->getType()
                        == CarbonXUsbPipe_CONTROL &&
                        _currentTransUnit->isLast())
                    _currentTransUnit->setDataPacket(
                            new UsbxData1Packet(0, NULL));
                delete dataPacket;
                updateForSameTransUnit(currentTime);
                interface->transmitPacket(
                        _handshakePacket = new UsbxAckPacket());
                _state = UsbxDeviceState_Do_BCINTO_Dopkt;
                break;
            }
            delete _currentTransUnit;
            _currentTransUnit = NULL;
            delete dataPacket;
            break;
        }
        case UsbxDeviceState_Do_BCINTO_Dopkt:
        {
            // Wait for the interface to complete packet transmission
            if (!interface->transmitComplete())
                break;

            // Free up handshake packet
            delete _handshakePacket;
            _handshakePacket = NULL;

            // Change state
            _state = UsbxDeviceState_IDLE;
            break;
        }
        case UsbxDeviceState_HS_Ping:
        {
            // Check for wait state
            UsbxBool retryState = _currentTransUnit->hasRetryState();
            UsbxBool stallState = _currentTransUnit->hasStallState();

            if (stallState)
            {
                updateForHalt(currentTime);
                delete _currentTransUnit;
                _currentTransUnit = NULL;

                interface->transmitPacket(
                        _handshakePacket = new UsbxStallPacket());
                _state = UsbxDeviceState_Do_BCINTO_Dopkt;
            }
#ifdef RANDOM_RETRY
            else if ((retryState = rand() % 2))
#else
            else if (retryState)
#endif
            {
                _currentTransUnit->incrRetryCount();
                updateForSameTransUnit(currentTime);
                interface->transmitPacket(
                        _handshakePacket = new UsbxNakPacket());
                _state = UsbxDeviceState_Do_BCINTO_Dopkt;
            }
            else
            {
                updateForSameTransUnit(currentTime);
                interface->transmitPacket(
                        _handshakePacket = new UsbxAckPacket());
                _state = UsbxDeviceState_Do_BCINTO_Dopkt;
            }
            break;
        }
        //************ Dev_Do_BCINTI ************
        case UsbxDeviceState_Do_BCINTI_Data:
        {
#ifdef DEBUG
            printf("DEBUG[%d]: DEVICE FSM: BCINTI FSM started.\n",
                    static_cast<UInt>(currentTime));
#endif

            // Check for wait state
            UsbxBool retryState = _currentTransUnit->hasRetryState();
            UsbxBool stallState = _currentTransUnit->hasStallState();

            if (stallState)
            {
                updateForHalt(currentTime);
                delete _currentTransUnit;
                _currentTransUnit = NULL;

                interface->transmitPacket(
                        _handshakePacket = new UsbxStallPacket());
                _state = UsbxDeviceState_Do_BCINTI_Dopkt;
            }
#ifdef RANDOM_RETRY
            else if ((retryState = rand() % 2))
#else
            else if (retryState)
#endif
            {
                _currentTransUnit->incrRetryCount();
                updateForSameTransUnit(currentTime);
                interface->transmitPacket(
                        _handshakePacket = new UsbxNakPacket());
                _state = UsbxDeviceState_Do_BCINTI_Dopkt;
            }
            else
            {
                // Send data packet over interface
                packet = _currentTransUnit->getDataPacket();
                ASSERT(packet, _xtor);
                interface->transmitPacket(packet);

                // Change state
                _state = UsbxDeviceState_Do_BCINTI_Resp;
            }
            break;
        }
        case UsbxDeviceState_Do_BCINTI_Resp:
        {
            // Wait for the interface to complete packet transmission
            if (!interface->transmitComplete())
                break;

            // Wait for the interface to receive a packet
            packet = interface->receivePacket();
            if (packet == NULL ||
                    packet->hasReceiveError() != UsbxPacketReceiveError_None)
            {
                // Timeout for receive
                if (!interface->receiving(currentTime))
                {
                    if (_timeWaited++ >= getBusTurnaroundTime(isFS, busWidth))
                    {
#ifdef DEBUG
                        printf("DEBUG[%d]: DEVICE FSM: BCINTI timeout.\n",
                                static_cast<UInt>(currentTime));
#endif
                        updateForSameTransUnit(currentTime);
                        _state = UsbxDeviceState_IDLE;
                    }
                }
                delete packet;
                break;
            }

            // When responded ok
            if (packet->getType() == UsbxPacket_ACK)
            {
                updateForNextTransUnit(currentTime);
                delete _currentTransUnit;
                _currentTransUnit = NULL;
            }
            else
                updateForSameTransUnit(currentTime);

            // Free up memory
            delete packet;

            // Change state
            _state = UsbxDeviceState_IDLE;
            break;
        }
        case UsbxDeviceState_Do_BCINTI_Dopkt:
        {
            // Wait for the interface to complete packet transmission
            if (!interface->transmitComplete())
                break;

            // Free up handshake packet
            delete _handshakePacket;
            _handshakePacket = NULL;

            // Change state
            _state = UsbxDeviceState_IDLE;
            break;
        }
        //************ Dev_Do_IsochO ************
        case UsbxDeviceState_Do_IsochO_Wait_data:
        {
            // Wait for data packet
            packet = interface->receivePacket();
            if (packet == NULL)
                break;

#ifdef DEBUG
            printf("DEBUG[%d]: DEVICE FSM: IsochO FSM started.\n",
                    static_cast<UInt>(currentTime));
#endif

            // If data packet is in error
            if (packet->hasReceiveError() != UsbxPacketReceiveError_None)
            {
                _state = UsbxDeviceState_IDLE;
                delete packet;
                break;
            }

            // Check for data toggle bit
            if (_currentTransUnit->getTokenPacket()->getType() !=
                    UsbxPacket_OUT)
            {
                _state = UsbxDeviceState_IDLE;
                delete packet;
                break;
            }

            UsbxDataPacket *dataPacket = static_cast<UsbxDataPacket*>(packet);
            // For HS periodic having repeat count, check for data packet type
            if (_currentTransUnit->getTrans()->setDataPacketTypeForHsRepeat())
            {
                UsbxPacketType expDataPacketType = _currentTransUnit
                    ->getTrans()->getPipe()->getNextDataPacketType();
                if (dataPacket->getType() != expDataPacketType &&
                        // If smaller data is coming
                        dataPacket->getType() != UsbxPacket_MDATA &&
                        expDataPacketType != UsbxPacket_MDATA)
                {
                    _state = UsbxDeviceState_IDLE;
                    break;
                }
                else
                    _currentTransUnit->getTrans()->getPipe()
                        ->setNextDataPacketType(dataPacket->getType());
            }

            if (_currentTransUnit->getDataPacket())
                delete _currentTransUnit->getDataPacket();
            _currentTransUnit->setDataPacket(dataPacket);
            ASSERT(_currentTransUnit->getTrans()
                ->pushNextTransUnit(_currentTransUnit), _xtor);
            updateForNextTransUnit(currentTime);
            delete _currentTransUnit;
            _currentTransUnit = NULL;

            _state = UsbxDeviceState_IDLE;
            break;
        }
        //************ Dev_Do_IsochI ************
        case UsbxDeviceState_Do_IsochI_Data:
        {
#ifdef DEBUG
            printf("DEBUG[%d]: DEVICE FSM: IsochI FSM started.\n",
                    static_cast<UInt>(currentTime));
#endif

            // Send data packet over interface
            packet = _currentTransUnit->getDataPacket();
            ASSERT(packet, _xtor);
            interface->transmitPacket(packet);

            updateForNextTransUnit(currentTime);

            // Change state
            _state = UsbxDeviceState_Do_IsochI_Done;
            break;
        }
        case UsbxDeviceState_Do_IsochI_Done:
        {
            if (!interface->transmitComplete())
                break;

            delete _currentTransUnit;
            _currentTransUnit = NULL;

            // Change state
            _state = UsbxDeviceState_IDLE;
            break;
        }
        default: ASSERT(0, _xtor);
    }

#ifdef DEBUG
    if (_state != lastState)
        printf("DEBUG[%d]: DEVICE FSM: State: %d\n",
                static_cast<UInt>(currentTime), _state);
#endif
    return retStatus;
    currentTime = 0; // To remove warning
}

// Function for RespondDev(Do_next_data): This function will be called
// for successful transfer of a transaction unit, i.e. when ACK is sent
void UsbxDeviceFsm::updateForNextTransUnit(CarbonTime currentTime)
{
#ifdef DEBUG
    printf("DEBUG[%d]: DEVICE FSM: Current transaction unit complete.\n",
            static_cast<UInt>(currentTime));
#endif

    if (_currentTransUnit->isLast()) // For last unit of transfer
    {
        // This means success status
        _currentTransUnit->getTrans()->setStatus(CarbonXUsbStatus_SUCCESS);
        // Pop from queue and call setResponse() callback
        _currentTransUnit->getTrans()->getPipe()->getTransQueue()->pop();
        // Call set response
        _xtor->setResponse(_currentTransUnit->getTrans());
        delete _currentTransUnit->getTrans();
    }

    currentTime = 0; // To remove warning
}

// Function when NAK is sent or ACK doesn't come from host. This function
// will be called when a transaction unit need a future retry
void UsbxDeviceFsm::updateForSameTransUnit(CarbonTime currentTime)
{
#ifdef DEBUG
    printf("DEBUG[%d]: DEVICE FSM: Transaction unit will be retried\n",
            static_cast<UInt>(currentTime));
#endif

    // Push the current transaction unit into pipe, for future pop to retry
    _currentTransUnit->getTrans()->pushTransUnitForRetry(_currentTransUnit);
    _currentTransUnit = NULL; // This is essential

    currentTime = 0; // To remove warning
}

// Function when STALL is sent: This function will be called
// when an entire transfer is to be stalled
void UsbxDeviceFsm::updateForHalt(CarbonTime currentTime)
{
#ifdef DEBUG
    printf("DEBUG[%d]: DEVICE FSM: Transaction halted.\n",
            static_cast<UInt>(currentTime));
#endif

    // Stop transfer and call reportTransaction() callback
    _currentTransUnit->getTrans()->setStatus(CarbonXUsbStatus_HALT);
    // Pop from queue and call reportTransaction() callback
    _currentTransUnit->getTrans()->getPipe()->getTransQueue()->pop();
    // Create receive data till halt for input transfers
    _currentTransUnit->getTrans()->createReceiveData();
    // Call setResponse
    _xtor->setResponse(_currentTransUnit->getTrans());
    delete _currentTransUnit->getTrans();

    currentTime = 0; // To remove warning
}

UsbxBool UsbxDeviceFsm::remoteWakeUp()
{
    if (_state != UsbxDeviceState_Suspended)
        return UsbxFalse;
    _driveTiming = UsbxDriveTiming_RemoteWakeUp;
    return UsbxTrue;
}

void UsbxDeviceFsm::resetForDetach()
{
    _state = _xtor->getConfig()->getIntfSide() == CarbonXUsbIntfSide_PHY ?
        UsbxDeviceState_Detached : UsbxDeviceState_Reset;
    _currentTransUnit = NULL; _timeWaited = 0; _handshakePacket = NULL;
}

void UsbxDeviceFsm::print(CarbonTime currentTime) const
{
    printf("Device FSM %s transactor:\n", _xtor->getName());
    printf("Current time: %d\n", static_cast<UInt>(currentTime));
    printf("State number: %d\n", _state);
    if (_currentTransUnit)
    {
        printf("Current ");
        _currentTransUnit->print();
    }
    return;
}

