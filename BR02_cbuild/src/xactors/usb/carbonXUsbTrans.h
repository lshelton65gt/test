// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __carbonXUsbTrans_h_
#define __carbonXUsbTrans_h_

#ifndef __carbonX_h_
#include "xactors/carbonX.h"
#endif

#ifndef __carbonXUsbPipe_h_
#include "xactors/usb/carbonXUsbPipe.h"
#endif

#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
  /*!
   * \brief Macro to allow both C and C++ compilers to use this include file in
   *        a type-safe manner.
   * \hideinitializer
   */
#define STRUCT struct
#endif

/*!
 * \addtogroup Usb
 * @{
 */

/*!
 * \file xactors/usb/carbonXUsbTrans.h
 * \brief Definition of Carbon USB Transaction Object.
 *
 * This file provides definition of transaction object.
 */

/*! Forward declaration */
STRUCT CarbonXUsbTransClass;

/*!
 * \brief Carbon USB IRP Transfer Object
 */
typedef STRUCT CarbonXUsbTransClass CarbonXUsbTrans;

/*!
 * \brief Retry state for a USB transaction unit
 *
 * Host transactor will use toggle bit mismatch or forced ACK corruption to
 * emulate retry. Device transactor will use NAK (or NYET for HS) for retry.
 */
typedef struct {
    CarbonUInt32 transUnitIndex;    /*!< Index which will be retried */
    CarbonUInt32 retryCount;        /*!< Number of times retry will be done */
} CarbonXUsbTransRetry;

/*!
 * \brief Carbon USB Transfer status
 */
typedef enum {
    CarbonXUsbStatus_SUCCESS,       /*!< Transfer successful (ACK) even after
                                         long retries */
    CarbonXUsbStatus_HALT           /*!< Device halted (STALL) */
} CarbonXUsbStatusType;

/*!
 * \brief Creates a new transaction for a specific pipe (i.e. EP).
 *
 * This API will create a new transaction for a specific pipe (i.e. EP).
 * User can retrieve all established pipes using API carbonXUsbGetCurrentPipes.
 * This API is relevant for Host, and undefined for Device.
 *
 * \param pipe Pipeid for which the new transaction will be created.
 *
 * \returns Pointer to the Transaction object created.
 */
CarbonXUsbTrans* carbonXUsbTransCreate(CarbonXUsbPipe *pipe);

/*!
 * \brief Destroys a transaction object.
 *
 * This API will destroy a created transaction object.
 *
 * \param trans Pointer to the transaction object to be destroyed.
 *
 * \retval 1 For success.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXUsbTransDestroy(CarbonXUsbTrans *trans);

/*!
 * \brief returns associated pipe of the received transaction.
 *
 * This API will return associated pipe of the received transaction.
 *
 * \param trans Pointer to the transaction object .
 */
CarbonXUsbPipe *carbonXUsbTransGetPipe(CarbonXUsbTrans *trans);

/*!
 * \brief Sets the bmRequestType field of a control transaction.
 *
 * This API will set the request type i.e. bmRequestType field.
 * Relevant only for control transfers. Default value is 0x00
 * i.e. Host to Device Standard Request.
 *
 * \param trans Pointer to the transaction object.
 * \param bmRequestType bmRequestType field of a control type transaction.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXUsbTransSetRequestType(CarbonXUsbTrans *trans,
          CarbonUInt8 bmRequestType);

/*!
 * \brief Returns the bmRequestType field of a control transaction
 *
 * This API will return the request type i.e. bmRequestType of received
 * transaction. Relevant only for control transfers.
 *
 * \param trans Pointer to the transaction object.
 *
 * \returns bmRequestType field of a control type transaction.
 */
CarbonUInt8 carbonXUsbTransGetRequestType(CarbonXUsbTrans *trans);

/*!
 * \brief Sets the bRequest field of a control type transaction
 *
 * This API will set the request i.e. bRequest field. Relevant only for
 * control transfers. Default value is 0x00.
 *
 * \param trans Pointer to the transaction object.
 * \param bRequest BRequest field of a control type transaction.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXUsbTransSetRequest(CarbonXUsbTrans *trans,
          CarbonUInt8 bRequest);

/*!
 * \brief Returns the bRequest field of a control type transaction.
 *
 * This API will return the request i.e. bRequest of received transaction.
 * Relevant only for control transfers.
 *
 * \param trans Pointer to the transaction object.
 *
 * \returns bRequest field of a control type transaction.
 */
CarbonUInt8 carbonXUsbTransGetRequest(CarbonXUsbTrans *trans);

/*!
 * \brief Sets the wValue field of a control type transaction.
 *
 * This API will set the wValue field. Relevant only for control transfers.
 * Default value is 0x00
 *
 * \param trans  Pointer to the transaction object.
 * \param wValue wValue field of a control type transaction.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXUsbTransSetRequestValue(CarbonXUsbTrans *trans,
          CarbonUInt16 wValue);

/*!
 * \brief Returns the wValue of received transaction.
 *
 * This API will return the wValue of received transaction.
 * Relevant only for control transfers.
 *
 * \param trans Pointer to the transaction object.
 *
 * \returns wValue of received transaction.
 */
CarbonUInt16 carbonXUsbTransGetRequestValue(CarbonXUsbTrans *trans);

/*!
 * \brief Sets the wIndex field of a control type transaction.
 *
 * This API will set the wIndex field. Relevant only for control transfers.
 * Default value is 0x00.
 *
 * \param trans Pointer to the transaction object.
 * \param wIndex wIndex field  of a transaction.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXUsbTransSetRequestIndex(CarbonXUsbTrans *trans,
          CarbonUInt16 wIndex);

/*!
 * \brief Returns the wIndex of a received transaction.
 *
 * This API will return the wIndex of received transaction.
 * Relevant only for control transfers.
 *
 * \param trans Pointer to the transaction object.
 *
 * \returns wIndex of  a received transaction.
 */
CarbonUInt16 carbonXUsbTransGetRequestIndex(CarbonXUsbTrans *trans);

/*!
 * \brief Sets the wLength field of a control type transaction.
 *
 * This API will set the wLength field. Relevant only for control
 * transfers.Default value is 0x00.
 *
 * \param trans Pointer to the transaction object.
 * \param wLength wLength field  of a control type transaction.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXUsbTransSetRequestLength(CarbonXUsbTrans *trans,
          CarbonUInt16 wLength);

/*!
 * \brief Returns the wLength field of a control type transaction.
 *
 * This API will return the wLength field of received transaction.
 * Relevant only for control transfers.
 *
 * \param trans Pointer to the transaction object.
 *
 * \returns wLength field of a control type transaction.
 */
CarbonUInt16 carbonXUsbTransGetRequestLength(CarbonXUsbTrans *trans);

/*!
 * \brief Sets the data payload in byte array
 *
 * This API will set the data payload in byte array (size of the array is
 * provided by dataSize). This array will be sent using DATA packets’payload
 * parts. Data size (i.e. dataSize) can range from 0 to 1023 for FS and 0 to
 * 1024 for HS transactor. Default value of data will be null array, which will
 * be signaled as error from host transactor or device transactor if pushed. So
 * calling this API for transfer is must before host's
 * carbonXUsbStartNewTransfer and at device’s setDefaultResponse/setResponse.
 *
 * \param trans Pointer to the transaction object.
 * \param dataSize Data payload
 * \param data Pointer to the data object.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXUsbTransSetData(CarbonXUsbTrans *trans,
        CarbonUInt32 dataSize, const CarbonUInt8 *data);

/*!
 * \brief Returns the data size for the transfer
 *
 * This API will return the data size for the transfer. One can get this at
 * setResponse() callback at host/device and at reportTransaction() callback at
 * host depending on transfer type.
 *
 * \param trans Pointer to the transaction object.
 *
 * \returns Data size.
 */
CarbonUInt32 carbonXUsbTransGetDataSize(const CarbonXUsbTrans *trans);

/*!
 * \brief Returns the data for the transfer
 *
 * This API will return the data for the transfer. One can get this at
 * setDefaultResponse() callback at device and at reportTransaction() callback
 * at host depending on transfer type.
 *
 * \param trans Pointer to the transaction object.
 *
 * \returns Pointer to the data array.
 */
const CarbonUInt8* carbonXUsbTransGetData(const CarbonXUsbTrans *trans);

/*!
 * \brief Sets the retry state array
 *
 * This API will set the retry state array (size of the array is provided by
 * retrySize). Default is NULL array which means no retry for all packets. Also
 * note, retry array, larger than number of transfer packets, will be truncated.
 *
 * \param trans Pointer to the transaction object.
 * \param retrySize Size of the retry state array.
 * \param retryStates Pointer to the retry state array.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXUsbTransSetRetryStates(CarbonXUsbTrans *trans,
        CarbonUInt32 retrySize, const CarbonXUsbTransRetry *retryStates);

/*!
 * \brief Sets the stall state from device
 *
 * This API will set the stall state from device. STALL will terminate the
 * transfer, which means retry states having packet index more than specified
 * stall index will not be simulatable. By default, there is no STALL.
 *
 * \param trans Pointer to the transaction object.
 * \param stallIndex Packet index of stall state.
 *
 * \retval 1 For Success.
 * \retval 0 For Failure.
 */
CarbonUInt32 carbonXUsbTransSetStallState(CarbonXUsbTrans *trans,
        CarbonUInt32 stallIndex);

/*!
 * \brief This API will return the status for a transfer.
 *
 * \param trans Pointer to the transaction object.
 *
 * \returns Transaction status
 */
CarbonXUsbStatusType carbonXUsbTransGetStatus(const CarbonXUsbTrans *trans);

/*!
 * \brief Dumps the transaction information
 *
 * This routine dumps the transaction info in console.
 * One can use this routine for debug purpose.
 *
 * \param trans Transaction object.
 */
void carbonXUsbTransDump(const CarbonXUsbTrans *trans);

/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif
