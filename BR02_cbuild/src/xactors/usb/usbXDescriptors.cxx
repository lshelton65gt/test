/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:      usbXDescriptors.cpp
    Purpose:   This file provides the member function definitions for device
               descriptors. 
*/

#include "usbXDefines.h"
#include "usbXDescriptors.h"
#include "xactors/usb/carbonXUsbDescriptors.h"

#define DEBUG_DESCRIPTORS
#define MIN_PACKET_SIZE_CHECK

void usbxGetBytesFromDescriptor(UsbxByte* &byteArray,
        const CarbonXUsbDeviceDescriptor *descriptor)
{
    // check for invalid inputs
    if (byteArray == NULL || descriptor == NULL)
        return;
    // standard device descriptor is always 18 bytes long
    byteArray[0]  = 0x12;
    byteArray[1]  = CarbonXUsbDescriptor_DEVICE;
    byteArray[2]  = (descriptor->bcdUSB) & 0xFF;
    byteArray[3]  = ((descriptor->bcdUSB) >> 8) & 0xFF;
    byteArray[4]  = (descriptor->bDeviceClass) & 0xFF;
    byteArray[5]  = (descriptor->bDeviceSubClass) & 0xFF;
    byteArray[6]  = (descriptor->bDeviceProtocol) & 0xFF;
    byteArray[7]  = (descriptor->bMaxPacketSize0) & 0xFF;
    byteArray[8]  = (descriptor->idVendor) & 0xFF;
    byteArray[9]  = ((descriptor->idVendor) >> 8) & 0xFF;
    byteArray[10] = (descriptor->idProduct) & 0xFF;
    byteArray[11] = ((descriptor->idProduct) >> 8) & 0xFF;
    byteArray[12] = (descriptor->bcdDevice) & 0xFF;
    byteArray[13] = ((descriptor->bcdDevice) >> 8) & 0xFF;
    byteArray[14] = (descriptor->iManufacturer) & 0xFF;
    byteArray[15] = (descriptor->iProduct) & 0xFF;
    byteArray[16] = (descriptor->iSerialNumber) & 0xFF;
    byteArray[17] = (descriptor->bNumConfigurations) & 0xFF;
    // incrementing pointer by 18 bytes to the end of device descriptor
    byteArray += 18;
}     

void usbxGetDescriptorFromBytes(const UsbxByte* &byteArray,
        CarbonXUsbDeviceDescriptor *descriptor) 
{
    // check for invalid inputs
    if (byteArray == NULL || descriptor == NULL)
        return;
    // check for the standard length of a device descriptor
    if (byteArray[0] != 18)
        return;
    // check if the descriptor is of type CarbonXUsbDescriptor_DEVICE
    if (CarbonXUsbDescriptorType(byteArray[1]) !=
        CarbonXUsbDescriptor_DEVICE)
    return;
    // standard device descriptor is always 18 bytes long
    descriptor->bcdUSB = ((byteArray[3] << 8) | byteArray[2]) & 0xFFFF;
    descriptor->bDeviceClass = byteArray[4] & 0xFF;
    descriptor->bDeviceSubClass = byteArray[5] & 0xFF;
    descriptor->bDeviceProtocol = byteArray[6] & 0xFF;
    descriptor->bMaxPacketSize0 = byteArray[7] & 0xFF;
    descriptor->idVendor = ((byteArray[9] << 8) | byteArray[8]) & 0xFFFF;
    descriptor->idProduct = ((byteArray[11] << 8) | byteArray[10]) & 0xFFFF;
    descriptor->bcdDevice = ((byteArray[13] << 8) | byteArray[12]) & 0xFFFF;
    descriptor->iManufacturer = byteArray[14] & 0xFF;
    descriptor->iProduct = byteArray[15] & 0xFF;
    descriptor->iSerialNumber = byteArray[16] & 0xFF;
    descriptor->bNumConfigurations = byteArray[17] & 0xFF;
    // incrementing pointer by 18 bytes to the end of device descriptor
    byteArray += 18;
}

void usbxDestroyDescriptor(CarbonXUsbDeviceDescriptor *descriptor)
{
    if (descriptor == NULL)
        return;
    DELETE(descriptor);
}

void usbxGetBytesFromDescriptor(UsbxByte* &byteArray,
        const CarbonXUsbConfigurationDescriptor *descriptor)
{
    // check for invalid inputs
    if (byteArray == NULL || descriptor == NULL)
        return;
    // standard configuration descriptor is always 9 bytes long
    byteArray[0] = 0x9;
    byteArray[1] = CarbonXUsbDescriptor_CONFIGURATION;
    byteArray[2] = (descriptor->wTotalLength) & 0xFF;
    byteArray[3] = ((descriptor->wTotalLength) >> 8) & 0xFF;
    byteArray[4] = (descriptor->bNumInterfaces) & 0xFF;
    byteArray[5] = (descriptor->bConfigurationValue) & 0xFF;
    byteArray[6] = (descriptor->iConfiguration) & 0xFF;
    byteArray[7] = (descriptor->bmAttributes) & 0xFF;
    byteArray[8] = (descriptor->bMaxPower) & 0xFF;
    // incrementing pointer by 9 bytes to the end of configuration descriptor
    byteArray += 9;
    // reading information from the interface descriptors
    for (UInt i = 0; i < descriptor->bNumInterfaces; i++)
    {
        CarbonXUsbInterfaceDescriptor *intf = descriptor->intfDescriptor[i];
        usbxGetBytesFromDescriptor(byteArray, intf);
    }
}

void usbxGetDescriptorFromBytes(const UsbxByte* &byteArray,
        CarbonXUsbConfigurationDescriptor *descriptor)
{
    // check for invalid inputs
    if (byteArray == NULL || descriptor == NULL)
        return;
    // check for the standard length of a configuration descriptor
    if (byteArray[0] != 9)
        return;
    // check if the descriptor is of type CarbonXUsbDescriptor_CONFIGURATION
    if (CarbonXUsbDescriptorType(byteArray[1]) !=
        CarbonXUsbDescriptor_CONFIGURATION)
    return;
    // standard configuration descriptor is always 9 bytes long
    descriptor->wTotalLength = ((byteArray[3] << 8) | byteArray[2]) & 0xFFFF;
    descriptor->bNumInterfaces = byteArray[4] & 0xFF;
    descriptor->bConfigurationValue = byteArray[5] & 0xFF;
    descriptor->iConfiguration = byteArray[6] & 0xFF;
    descriptor->bmAttributes = byteArray[7] & 0xFF;
    descriptor->bMaxPower = byteArray[8] & 0xFF;
    descriptor->intfDescriptor =
        NEWARRAY(CarbonXUsbInterfaceDescriptor*, descriptor->bNumInterfaces);
    if (descriptor->intfDescriptor == NULL)
        return;
    // incrementing pointer by 9 bytes to the end of configuration descriptor
    byteArray += 9;
    for (UInt i = 0; i < descriptor->bNumInterfaces; i++)
    {
        descriptor->intfDescriptor[i] = NEW(CarbonXUsbInterfaceDescriptor);
        usbxGetDescriptorFromBytes(byteArray,
                descriptor->intfDescriptor[i]);
    }
}

void usbxDestroyDescriptor(CarbonXUsbConfigurationDescriptor *descriptor)
{
    if (descriptor == NULL)
        return;
    for (UInt intf = 0; intf < descriptor->bNumInterfaces; intf++)
    {
        CarbonXUsbInterfaceDescriptor *intfPtr =
            descriptor->intfDescriptor[intf];
        usbxDestroyDescriptor(intfPtr);
    }
    DELETE(descriptor->intfDescriptor);
    DELETE(descriptor);
}

void usbxGetBytesFromDescriptor(UsbxByte* &byteArray,
        const CarbonXUsbInterfaceDescriptor *descriptor)
{
    // check for invalid inputs
    if (byteArray == NULL || descriptor == NULL)
        return;
    // standard interface descriptor is always 9 bytes long
    byteArray[0] = 0x9;
    byteArray[1] = CarbonXUsbDescriptor_INTERFACE;
    byteArray[2] = (descriptor->bInterfaceNumber) & 0xFF;
    byteArray[3] = (descriptor->bAlternateSetting) & 0xFF;
    byteArray[4] = (descriptor->bNumEndpoints) & 0xFF;
    byteArray[5] = (descriptor->bInterfaceClass) & 0xFF;
    byteArray[6] = (descriptor->bInterfaceSubClass) & 0xFF;
    byteArray[7] = (descriptor->bInterfaceProtocol) & 0xFF;
    byteArray[8] = (descriptor->iInterface) & 0xFF;
    // incrementing pointer by 9 bytes to the end of interface descriptor
    byteArray += 9;
    // reading information from the endpoint descriptors
    for (UInt i = 0; i < descriptor->bNumEndpoints; i++)
    {
        CarbonXUsbEndpointDescriptor *ep = descriptor->epDescriptor[i];
        usbxGetBytesFromDescriptor(byteArray, ep);
    }
}

void usbxGetDescriptorFromBytes(const UsbxByte* &byteArray,
        CarbonXUsbInterfaceDescriptor *descriptor)
{
    // check for invalid inputs
    if (byteArray == NULL || descriptor == NULL)
        return;
    // check for the standard length of a interface descriptor
    if (byteArray[0] != 9)
        return;
    // check if the descriptor is of type CarbonXUsbDescriptor_INTERFACE
    if (CarbonXUsbDescriptorType(byteArray[1]) !=
        CarbonXUsbDescriptor_INTERFACE)
        return;
    // standard interface descriptor is always 9 bytes long
    descriptor->bInterfaceNumber = byteArray[2] & 0xFF;
    descriptor->bAlternateSetting = byteArray[3] & 0xFF;
    descriptor->bNumEndpoints = byteArray[4] & 0xFF;
    descriptor->bInterfaceClass = byteArray[5] & 0xFF;
    descriptor->bInterfaceSubClass = byteArray[6] & 0xFF;
    descriptor->bInterfaceProtocol = byteArray[7] & 0xFF;
    descriptor->iInterface = byteArray[8] & 0xFF;
    // check if there are any endpoints or not
    if (descriptor->bNumEndpoints == 0)
    {
        descriptor->epDescriptor = NULL;
        return;
    }
    descriptor->epDescriptor =
        NEWARRAY(CarbonXUsbEndpointDescriptor*, descriptor->bNumEndpoints);
    if (descriptor->epDescriptor == NULL)
        return;
    // incrementing pointer by 9 bytes to the end of interface descriptor
    byteArray += 9;
    for (UInt i = 0; i < descriptor->bNumEndpoints; i++)
    {
        descriptor->epDescriptor[i] = NEW(CarbonXUsbEndpointDescriptor);
        usbxGetDescriptorFromBytes(byteArray,
                descriptor->epDescriptor[i]);
    }
}

void usbxDestroyDescriptor(CarbonXUsbInterfaceDescriptor *descriptor)
{
    if (descriptor == NULL)
        return;
    if (descriptor->epDescriptor == NULL)
        return;
    for (UInt ep = 0; ep < descriptor->bNumEndpoints; ep++)
    {
        CarbonXUsbEndpointDescriptor *epPtr = descriptor->epDescriptor[ep];
        if (epPtr == NULL)
            continue;
        DELETE(epPtr);
    }
    DELETE(descriptor->epDescriptor);
    DELETE(descriptor);
}

void usbxGetBytesFromDescriptor(UsbxByte* &byteArray,
        const CarbonXUsbEndpointDescriptor *descriptor)
{
    // check for invalid inputs
    if (byteArray == NULL || descriptor == NULL)
        return;
    // standard endpoint descriptor is always 7 bytes long
    byteArray[0] = 0x7;
    byteArray[1] = CarbonXUsbDescriptor_ENDPOINT;
    byteArray[2] = (descriptor->bEndpointAddress) & 0xFF;
    byteArray[3] = (descriptor->bmAttributes) & 0xFF;
    byteArray[4] = (descriptor->wMaxPacketSize) & 0xFF;
    byteArray[5] = ((descriptor->wMaxPacketSize) >> 8) & 0xFF;
    byteArray[6] = (descriptor->bInterval) & 0xFF;
    // incrementing pointer by 7 bytes to the end of endpoint descriptor
    byteArray += 7;
}

void usbxGetDescriptorFromBytes(const UsbxByte* &byteArray,
        CarbonXUsbEndpointDescriptor *descriptor)
{
    // check for invalid inputs
    if (byteArray == NULL || descriptor == NULL)
        return;
    // check for the standard length of a endpoint descriptor
    if (byteArray[0] != 7)
        return;
    // check if the descriptor is of type CarbonXUsbDescriptor_ENDPOINT
    if (CarbonXUsbDescriptorType(byteArray[1]) !=
        CarbonXUsbDescriptor_ENDPOINT)
    return;
    // standard endpoint descriptor is always 7 bytes long
    descriptor->bEndpointAddress = byteArray[2] & 0xFF;
    descriptor->bmAttributes = byteArray[3] & 0xFF;
    descriptor->wMaxPacketSize = ((byteArray[5] << 8) | byteArray[4]) & 0xFFFF;
    descriptor->bInterval = byteArray[6] & 0xFF;
    // incrementing pointer by 7 bytes to the end of endpoint descriptor
    byteArray += 7;
}

void usbxDestroyDescriptor(CarbonXUsbEndpointDescriptor *descriptor)
{
    if (descriptor == NULL)
        return;
    DELETE(descriptor);
}

void usbxGetBytesFromDescriptor(UsbxByte* &byteArray,
        const CarbonXUsbOtgDescriptor *descriptor)
{
    // check for invalid inputs
    if (byteArray == NULL || descriptor == NULL)
        return;
    // standard OTG descriptor is always 3 bytes long
    byteArray[0]  = 0x3;
    byteArray[1]  = CarbonXUsbDescriptor_OTG;
    byteArray[2]  = (descriptor->bmAttributes) & 0xFF;
    // incrementing pointer by 3 bytes to the end of OTG descriptor
    byteArray += 3;
}

void usbxGetDescriptorFromBytes(const UsbxByte* &byteArray,
        CarbonXUsbOtgDescriptor *descriptor)
{
    // check for invalid inputs
    if (byteArray == NULL || descriptor == NULL)
        return;
    // check for the standard length of a OTG descriptor
    if (byteArray[0] != 3)
        return;
    // check if the descriptor is of type CarbonXUsbDescriptor_OTG
    if (CarbonXUsbDescriptorType(byteArray[1]) !=
        CarbonXUsbDescriptor_OTG)
    return;
    // standard OTG descriptor is always 3 bytes long
    descriptor->bmAttributes = byteArray[2] & 0xFF;
    // incrementing pointer by 3 bytes to the end of OTG descriptor
    byteArray += 3;
}

void usbxDestroyDescriptor(CarbonXUsbOtgDescriptor *descriptor)
{
    if (descriptor == NULL)
        return;
    DELETE(descriptor);
}

UsbxBool usbxCheckDescriptor(CarbonXUsbDeviceDescriptor *descriptor,
        CarbonXUsbSpeedType speed)
{
    // check for null pointer
    if (descriptor == NULL)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckDeviceDescriptor: NULL pointer argument passed.\n");
#endif
        return UsbxFalse;
    }
    // check for bDeviceSubClass field
    if ((descriptor->bDeviceClass == 0) && (descriptor->bDeviceSubClass != 0))
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckDeviceDescriptor: Non-zero value '%d' for bDeviceSubClass "
               "is not allowed while bDeviceClass is zero\n",
               descriptor->bDeviceSubClass);
#endif
        return UsbxFalse;
    }
    // check for permissible packet sizes for endpoint 0
    if ((descriptor->bMaxPacketSize0 != 8) &&
        (descriptor->bMaxPacketSize0 != 16) &&
        (descriptor->bMaxPacketSize0 != 32) &&
        (descriptor->bMaxPacketSize0 != 64))
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckDeviceDescriptor: The value '%d' for bMaxPacketSize0 is "
               "not among the permissible values '8, 16, 32 or 64'\n",
               descriptor->bMaxPacketSize0);
#endif
        return UsbxFalse;
    }
    // check for the only permissible packet size for endpoint 0 in case of HS
    if ((speed == CarbonXUsbSpeed_HS) && (descriptor->bMaxPacketSize0 != 64))
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckDeviceDescriptor: The value '%d' for bMaxPacketSize0 "
               "is not allowed in High Speed Devices. The only allowed value "
               "is 64\n", descriptor->bMaxPacketSize0);
#endif
        return UsbxFalse;
    }
    return UsbxTrue;
}

UsbxBool usbxCheckDescriptor(CarbonXUsbEndpointDescriptor *descriptor,
        CarbonXUsbSpeedType speed)
{
    // check for null pointer
    if (descriptor == NULL)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckEndpointDescriptor: NULL pointer argument passed.\n");
#endif
        return UsbxFalse;
    }
    // check for reserved bits 6..4 of bEndpointAddress
    if (descriptor->bEndpointAddress & 0x70)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckEndpointDescriptor: Bits bEndpointAddress[6:4] are "
               "reserved and should be reset to zero.\n");
#endif
        return UsbxFalse;
    }
    // check for illegal endpoint address, 0
    if ((descriptor->bEndpointAddress & 0xF) == 0)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckEndpointDescriptor: The value '0' for bEndpointAddress "
               "is reserved and should not be used.\n");
#endif
        return UsbxFalse;
    }
    // check for reserved bits 7..6 of bmAttributes
    if (descriptor->bmAttributes & 0xC0)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckEndpointDescriptor: Bits bmAttributes[7:6] are reserved "
               "and should be reset to zero.\n");
#endif
        return UsbxFalse;
    }
    // check for reserved bits 5..2 of bmAttributes in case of Transfer type
    // other than isochronous
    if (((descriptor->bmAttributes & 0x3) != 0x01) &&
        (descriptor->bmAttributes & 0x3C))
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckEndpointDescriptor: For non Isochronous endpoints, bits "
               "bmAttributes[5:2] should be reset to zero.\n");
#endif
        return UsbxFalse;
    }
#ifdef MIN_PACKET_SIZE_CHECK
    // For high speed isochronous and interrupt endpoints, check for allowed
    // wMaxPacketSize values for diffrent numbers of transactions per
    // microframe as per bits 12..11.
    // Ref. Table 9-14 Universal Serial Bus Specification Revision 2.0
    if ((speed == CarbonXUsbSpeed_HS) &&    // for high speed
        (descriptor->bmAttributes & 0x1))   // for isochronous & interrupt
    {
        UInt packetSize = descriptor->wMaxPacketSize & 0x07FF;
        switch ((descriptor->wMaxPacketSize & 0x1800) >> 11)
        {
            case 0:
            {
                if ((packetSize < 1) || (packetSize > 1024))
                {
#ifdef DEBUG_DESCRIPTORS
                    printf("CheckEndpointDescriptor: In case of High speed "
                           "periodic endpoints the value '%d' for "
                           "wMaxPacketSize[10:0] is not allowed. "
                           "It should be within the range 1 to 1024 if "
                           "wMaxPacketSize[12:11] is reset to zero.\n",
                           packetSize);
#endif
                    return UsbxFalse;
                }
                break;
            }
            case 1:
            {
                if ((packetSize < 513) || (packetSize > 1024))
                {
#ifdef DEBUG_DESCRIPTORS
                    printf("CheckEndpointDescriptor: In case of High speed "
                           "periodic endpoints the value '%d' for "
                           "wMaxPacketSize[10:0] is not allowed. "
                           "It should be within the range 513 to 1024 if "
                           "wMaxPacketSize[12:11] is set to 1.\n",
                           packetSize);
#endif
                    return UsbxFalse;
                }
                break;
            }
            case 2:
            {
                if ((packetSize < 683) || (packetSize > 1024))
                {
#ifdef DEBUG_DESCRIPTORS
                    printf("CheckEndpointDescriptor: In case of High speed "
                           "periodic endpoints the value '%d' for "
                           "wMaxPacketSize[10:0] is not allowed. "
                           "It should be within the range 683 to 1024 if "
                           "wMaxPacketSize[12:11] is set to 2.\n",
                           packetSize);
#endif
                    return UsbxFalse;
                }
                break;
            }
            default: break;
        }
    }
#endif
    // check for reserved bits 15..13 of wMaxPacketSize
    if (descriptor->wMaxPacketSize & 0xE000)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckEndpointDescriptor: Bits wMaxPacketSize[15:13] are "
               "reserved and should be reset to zero.\n");
#endif
        return UsbxFalse;
    }
    // check for reserved value 0b11 of bits 12..11 of wMaxPacketSize
    if ((descriptor->wMaxPacketSize & 0x1800) == 0x1800)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckEndpointDescriptor: The value '3' for bits "
               "wMaxPacketSize[12:11] is reserved and should not be used.\n");
#endif
        return UsbxFalse;
    }
    // check for bInterval field at the time when Xtor is instantiated
    // so that we could know the speed.
    // NOTE:-
    // For full-/high-speed isochronous endpoints, this value
    // must be in the range from 1 to 16.
    if (((descriptor->bmAttributes & 0x3) == 0x01) &&
        ((descriptor->bInterval < 1) || (descriptor->bInterval > 16)))
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckEndpointDescriptor: For Isochronous endpoints, the value "
               "'%d' for bInterval is not allowed. It should be within the "
               "range 1 to 16.\n", descriptor->bInterval);
#endif
        return UsbxFalse;
    }
    // For full-/low-speed interrupt endpoints, the value of
    // this field may be from 1 to 255.
    if (((descriptor->bmAttributes & 0x3) == 0x3) &&
        (speed != CarbonXUsbSpeed_HS) && (descriptor->bInterval == 0))
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckEndpointDescriptor: For full speed Interrupt endpoints, "
               "the value '%d' for bInterval is not allowed. It should be "
               "within the range 1 to 255.\n", descriptor->bInterval);
#endif
        return UsbxFalse;
    }
    // For high-speed interrupt endpoints, the bInterval value
    // must be from 1 to 16.
    if (((descriptor->bmAttributes & 0x3) == 0x3) &&
        (speed == CarbonXUsbSpeed_HS) && ((descriptor->bInterval < 1) ||
        (descriptor->bInterval > 16)))
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckEndpointDescriptor: For high speed Interrupt endpoints, "
               "the value '%d' for bInterval is not allowed. It should be "
               "within the range 1 to 16.\n", descriptor->bInterval);
#endif
        return UsbxFalse;
    }
    return UsbxTrue;
}

UsbxBool usbxCheckDescriptor(CarbonXUsbInterfaceDescriptor *descriptor,
        CarbonXUsbSpeedType speed)
{
    // check for null pointer
    if (descriptor == NULL)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckInterfaceDescriptor: NULL pointer argument passed.\n");
#endif
        return UsbxFalse;
    }
    // check if the bInterfaceClass field is reset to zero,
    // bInterfaceSubClass field must also be reset to zero.
    if ((descriptor->bInterfaceClass == 0) &&
        (descriptor->bInterfaceSubClass != 0))
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckInterfaceDescriptor: Non-zero value '%d' for "
               "bInterfaceSubClass is not allowed while bInterfaceClass "
               "is zero.\n", descriptor->bInterfaceSubClass);
#endif
        return UsbxFalse;
    }
    UsbxBool status = UsbxFalse;
    for (UInt i = 0; i < descriptor->bNumEndpoints; i++)
    {
        if (!descriptor->epDescriptor[i])
        {
#ifdef DEBUG_DESCRIPTORS
            printf("CheckInterfaceDescriptor: NULL epDescriptor passed at "
                    "index '%d'.\n", i);
#endif
            return UsbxFalse;
        }
        status = usbxCheckDescriptor(descriptor->epDescriptor[i], speed);
        if (status == UsbxFalse)
            return UsbxFalse;
    }
    return UsbxTrue;
}

UsbxBool usbxCheckDescriptor(CarbonXUsbConfigurationDescriptor *descriptor,
        CarbonXUsbSpeedType speed)
{
    // check for null pointer
    if (descriptor == NULL)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckConfigurationDescriptor: NULL pointer argument passed.\n");
#endif
        return UsbxFalse;
    }
    // check if bNumInterfaces is greater than 0, since at least 1 interface
    // descriptor should be there per configuration descriptor
    if (descriptor->bNumInterfaces == 0)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckConfigurationDescriptor: The value '0' for bNumInterfaces "
               "is not allowed.\n");
#endif
        return UsbxFalse;
    }
    // check if bConfigurationValue is within range 1 to bNumInterfaces
    if ((descriptor->bConfigurationValue < 1) ||
        (descriptor->bConfigurationValue > descriptor->bNumInterfaces))
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckConfigurationDescriptor: The value '%d' for "
               "bConfigurationValue is not not allowed. It should be within "
               "the range 1 to bNumInterfaces.\n", descriptor->bNumInterfaces);
#endif
        return UsbxFalse;
    }
    // check for reserved bmAttributes bit 7, set to 1
    if ((descriptor->bmAttributes & 0x80) == 0)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckConfigurationDescriptor: Bit bmAttributes[7] is reserved "
               "and should be set to one.\n");
#endif
        return UsbxFalse;
    }
    // check for reserved bmAttributes bits 4..0, reset to 0
    if (descriptor->bmAttributes & 0x1F)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckConfigurationDescriptor: Bits bmAttributes[4:0] are "
               "reserved and should be reset to zero.\n");
#endif
        return UsbxFalse;
    }
    UsbxBool status = UsbxFalse;
    UInt byteCount = 9; // standard USB configuration descriptor length
    for (UInt i = 0; i < descriptor->bNumInterfaces; i++)
    {
        if (!descriptor->intfDescriptor[i])
        {
#ifdef DEBUG_DESCRIPTORS
            printf("CheckConfigurationDescriptor: NULL intfDescriptor passed "
                   "at index '%d'.", i);
#endif
            return UsbxFalse;
        }
        status = usbxCheckDescriptor(descriptor->intfDescriptor[i], speed);
        if (status == UsbxFalse)
            return UsbxFalse;
        // calculate total length of configuration descriptor
        byteCount += 9; // standard USB interface descriptor length
        byteCount += 7 * descriptor->intfDescriptor[i]->bNumEndpoints;
    }
    // check for total length of configuration descriptor
    if (descriptor->wTotalLength != byteCount)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckConfigurationDescriptor: The value '%d' for wTotalLength "
               "is incorrect as per the number of bNumEndpoints provided in "
               "each of '%d' intfDescriptors provided. The value calculated "
               "accordingly results in '%d'.\n", descriptor->wTotalLength,
               descriptor->bNumInterfaces, byteCount);
#endif
        return UsbxFalse;
    }
    return UsbxTrue;
}

UsbxBool usbxCheckDescriptor(CarbonXUsbOtgDescriptor *descriptor,
        CarbonXUsbSpeedType speed)
{
    // check for null pointer
    if (descriptor == NULL)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckOtgDescriptor: NULL pointer argument passed.\n");
#endif
        return UsbxFalse;
    }
    // check for reserved bmAttributes bits 7..2, reset to 0
    if (descriptor->bmAttributes & 0xFC)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckOtgDescriptor: Bits bmAttributes[7:2] are reserved "
               "and should be reset to zero .\n");
#endif
        return UsbxFalse;
    }
    // check if the hnp_support bit is TRUE, then srp_support bit must
    // also be TRUE
    if ((descriptor->bmAttributes & 0x3) == 0x2)
    {
#ifdef DEBUG_DESCRIPTORS
        printf("CheckOtgDescriptor: If the hnp_support bit is set, then "
               "srp_support bit must also be set.\n");
#endif
        return UsbxFalse;
    }
    return UsbxTrue;
    // to remove compile time warning
    speed = CarbonXUsbSpeed_FS;
}

UsbxBool usbxCheckDescriptor(const UsbxByte *byteArray,
        CarbonXUsbSpeedType speed)
{
    // check for null pointer
    if (byteArray == NULL)
        return UsbxFalse;
    UsbxBool status = UsbxFalse;
    switch (CarbonXUsbDescriptorType(byteArray[1]))
    {
        case CarbonXUsbDescriptor_DEVICE:
        {
            CarbonXUsbDeviceDescriptor *descriptor =
                NEW(CarbonXUsbDeviceDescriptor);
            usbxGetDescriptorFromBytes(byteArray, descriptor);
            status = usbxCheckDescriptor(descriptor, speed);
            usbxDestroyDescriptor(descriptor);
            return status;
        }
        case CarbonXUsbDescriptor_CONFIGURATION:
        {
            CarbonXUsbConfigurationDescriptor *descriptor =
                NEW(CarbonXUsbConfigurationDescriptor);
            usbxGetDescriptorFromBytes(byteArray, descriptor);
            status = usbxCheckDescriptor(descriptor, speed);
            usbxDestroyDescriptor(descriptor);
            return status;
        }
        case CarbonXUsbDescriptor_INTERFACE:
        {
            CarbonXUsbInterfaceDescriptor *descriptor =
                NEW(CarbonXUsbInterfaceDescriptor);
            usbxGetDescriptorFromBytes(byteArray, descriptor);
            status = usbxCheckDescriptor(descriptor, speed);
            usbxDestroyDescriptor(descriptor);
            return status;
        }
        case CarbonXUsbDescriptor_ENDPOINT:
        {
            CarbonXUsbEndpointDescriptor *descriptor =
                NEW(CarbonXUsbEndpointDescriptor);
            usbxGetDescriptorFromBytes(byteArray, descriptor);
            status = usbxCheckDescriptor(descriptor, speed);
            usbxDestroyDescriptor(descriptor);
            return status;
        }
        case CarbonXUsbDescriptor_OTG:
        {
            CarbonXUsbOtgDescriptor *descriptor =
                NEW(CarbonXUsbOtgDescriptor);
            usbxGetDescriptorFromBytes(byteArray, descriptor);
            status = usbxCheckDescriptor(descriptor, speed);
            usbxDestroyDescriptor(descriptor);
            return status;
        }
        default: return UsbxFalse;
    }
    return UsbxFalse;
}

UInt usbxGetDescriptorSize(const UsbxByte *byteArray)
{
    // check if invalid pointer passed
    if (byteArray == NULL)
        ASSERT(0, NULL);

    /* get the descriptor type */
    CarbonXUsbDescriptorType descType = CarbonXUsbDescriptorType(byteArray[1]);

    // check for descriptor type
    if (descType == CarbonXUsbDescriptor_DEVICE ||
        descType == CarbonXUsbDescriptor_OTG)
        // returning bLength in case of device and OTG descriptor
        return byteArray[0];
    else if (descType == CarbonXUsbDescriptor_CONFIGURATION)
        // returning wTotalLength in case of a configuration descriptor
        return (byteArray[3] << 8) | byteArray[2];
    else
        // size of other descriptors are not returned
        ASSERT(0, NULL);
    return 0;
}

UInt usbxGetConfigDescNumber(const UsbxByte *configDesc)
{
    // check if invalid pointer passed
    if (configDesc == NULL)
        ASSERT(0, NULL);

    // check for configuration descriptor
    if (CarbonXUsbDescriptorType(configDesc[1]) ==
            CarbonXUsbDescriptor_CONFIGURATION)
        return configDesc[5];
    else
        // other descriptors are not allowed
        ASSERT(0, NULL);
    return 0;
}

UInt usbxGetNumEpsFromConfig(const UsbxByte *configDesc)
{
    // check if invalid pointer passed
    if (configDesc == NULL)
        ASSERT(0, NULL);

    if (CarbonXUsbDescriptorType(configDesc[1]) !=
            CarbonXUsbDescriptor_CONFIGURATION)
        ASSERT(0, NULL);

    // finding out the number of interfaces
    UInt numIntf = configDesc[4];

    // configuration should have at least one interface
    if (!numIntf)
        ASSERT(0, NULL);

    // jumping to the first interface descriptor location
    const UsbxByte *intfDesc = configDesc + 9;

    UInt epCount = 0;
    for (UInt i = 0; i < numIntf; i++)
    {
        if (CarbonXUsbDescriptorType(intfDesc[1]) !=
                CarbonXUsbDescriptor_INTERFACE)
            ASSERT(0, NULL);
        // accumulating the number of endpoints at interfaces
        epCount += intfDesc[4];
        // jumping to the next interface descriptor
        intfDesc += 7 * intfDesc[4];
    }
    return epCount;
}

const UsbxByte *usbxGetNextEpDescriptor(const UsbxByte *configDescArea,
        UsbxBool continueIfEp)
{
    // check if invalid pointer passed
    if (configDescArea == NULL)
        ASSERT(0, NULL);

    // byte pointers
    const UsbxByte *nextEp = configDescArea;

    switch (CarbonXUsbDescriptorType(configDescArea[1]))
    {
        case CarbonXUsbDescriptor_CONFIGURATION:
        {
            // shift to the start of next standard descriptor after this
            // Configuration descriptor member space
            // Note: Length of a standard Configuration descriptor is 9 Bytes
            nextEp += 9;
            // Configuration should have at least one interface
            if (CarbonXUsbDescriptorType(nextEp[1]) !=
                    CarbonXUsbDescriptor_INTERFACE)
                ASSERT(0, NULL);
            return usbxGetNextEpDescriptor(nextEp, UsbxFalse);
        }
        case CarbonXUsbDescriptor_INTERFACE:
        {
            // Reading the number of endpoints in this interface descriptor
            UInt numEp = nextEp[4];
            if (!numEp)
                return NULL;
            // shift to the start of next standard descriptor after this
            // Interface descriptor member space
            // Note: Length of a standard Interface descriptor is 9 Bytes
            nextEp += 9;
            return nextEp;
        }
        case CarbonXUsbDescriptor_ENDPOINT:
        {
            if (continueIfEp == UsbxFalse)
                return nextEp;
            // shift to the start of next standard descriptor after this
            // Endpoint descriptor member space
            // Note: Length of standard Endpoint descriptor is 7 Bytes
            nextEp += 7;
            return usbxGetNextEpDescriptor(nextEp, UsbxFalse);
        }
        default:
        {
            if (continueIfEp == UsbxFalse)
                return NULL;
            else
                ASSERT(0, NULL);
        }
    }
    return NULL;
}
