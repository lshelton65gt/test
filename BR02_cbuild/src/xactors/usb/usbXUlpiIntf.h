/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_ULPIINTF_H
#define USBX_ULPIINTF_H

#include "usbXIntf.h"
#include "usbXUtmiIntf.h"
#include "usbXConfig.h"
#include "xactors/usb/carbonXUsb.h"

/*!
 * \file usbXUlpiIntf.h
 * \brief Header file for ULPI LINK and PHY pin level interface definition.
 */

/*!
 * \brief Class to represent ULPI pin level interface
 */
class UsbxUlpiIntf: public UsbxIntf
{
public: CARBONMEM_OVERRIDES

protected:

    // UTMI Interface
    UsbxUtmiIntf *_utmiIntf;

    /*!
     * \brief ULPI FSM states
     */
    enum UsbxUlpiFsmStateType {
        UsbxUlpiFsm_IDLE,
        UsbxUlpiFsm_SendNOPID,
        UsbxUlpiFsm_RegWrite,
        UsbxUlpiFsm_ExtendedRegWrite,
        UsbxUlpiFsm_ExtendedRegWriteAddress,
        UsbxUlpiFsm_RegWriteData,
        UsbxUlpiFsm_RegRead,
        UsbxUlpiFsm_RegReadData,
        UsbxUlpiFsm_ExtendedRegRead,
        UsbxUlpiFsm_ExtendedRegReadAddress,
        UsbxUlpiFsm_TRANSMIT,
        UsbxUlpiFsm_ReceivingPIDByte,
        UsbxUlpiFsm_ReceivingNOPIDByte,
        UsbxUlpiFsm_RECEIVE,
        UsbxUlpiFsm_SendRXCMD,
        UsbxUlpiFsm_WaitForEOP,
        UsbxUlpiFsm_ReceiveEOP,
        UsbxUlpiFsm_ReceiveChirp,
        UsbxUlpiFsm_ABORT
    };

    // FSM state
    UsbxUlpiFsmStateType _state;

protected:

    // ULPI pins
    UsbxPin *_dir;
    UsbxPin *_nxt;
    UsbxPin *_stp;
    UsbxPin *_data;

    // VCD dump related
    UsbxConstString _moduleName;

    UsbxBool _isPidData;                //!< PID data or NO-PID data
    UsbxBool _isFunctionControlUpdate;  //!< Function Control or OTG update

public:

    // Constructor and destructor
    UsbxUlpiIntf(UsbXtor *xtor, UsbxConstString moduleName);
    virtual ~UsbxUlpiIntf();

    // Packet access functions
    UsbxBool transmitPacket(UsbxPacket *packet)
    { return _utmiIntf->transmitPacket(packet); }
    UsbxBool transmitComplete() const
    { return _utmiIntf->transmitComplete(); }
    UsbxPacket *receivePacket()
    { return _utmiIntf->receivePacket(); }

    virtual UsbxBool receiving(CarbonTime currentTime) const = 0;

    // Timing access functions
    UsbxBool startTiming(UsbxTimingType timing)
    { return _utmiIntf->startTiming(timing); }
    UsbxBool timingComplete() const
    { return _utmiIntf->timingComplete(); }
    UsbxBool senseTiming(UsbxTimingType expectedTiming,
                        UsbxBool flush = UsbxTrue)
    { return _utmiIntf->senseTiming(expectedTiming, flush); }

    // Vbus access functions
    UsbxBool driveVbus(UsbxBool isOn)
    { return _utmiIntf->driveVbus(isOn); }
    UsbxBool isVbusValid() const
    { return _utmiIntf->isVbusValid(); }

    void setTimingFactor(CarbonUInt32 factor);

    virtual void resetForDetach();

    // Refresh functions
    virtual UsbxBool refreshInputs(CarbonTime currentTime) = 0;
    virtual UsbxBool refreshOutputs(CarbonTime currentTime) = 0;

    // Pin access functions
    UsbxPin *getPin(UsbxConstString pinName) const;

#ifdef _VCD
    // VCD dump related
    virtual UsbxBool dumpUtmiIntfPins(CarbonTime currentTime) const = 0;
    void vcdDecl(UsbxPin *pin);
    virtual void vcdDump(UsbxPin *pin) const = 0;
#endif
};

/*!
 * \brief Class to represent ULPI LINK pin level interface
 */
class UsbxUlpiLinkIntf: public UsbxUlpiIntf
{
public: CARBONMEM_OVERRIDES

protected:

    CarbonUInt8 _functionCtrlReg;       //!< Function Control Registers
    CarbonUInt8 _otgCtrlReg;            //!< Function Control Registers
    UsbxUtmiPhyIntf::UsbxLineStateLogic _lineStateLogic; //!< Logic for driving
                                                         //   LineState

public:

    // Constructor and destructor
    UsbxUlpiLinkIntf(UsbXtor *xtor);
    ~UsbxUlpiLinkIntf() {}

    void setSpeedFS(UsbxBool isFS, CarbonTime currentTime);

    // Member function
    UsbxBool receiving(CarbonTime currentTime) const
    { return _utmiIntf->receiving(currentTime); }
    
    // Evaluates the interface
    UsbxBool evaluate(CarbonTime currentTime);

    // Refresh functions
    UsbxBool refreshInputs(CarbonTime currentTime);
    UsbxBool refreshOutputs(CarbonTime currentTime);

#ifdef _VCD
    // VCD dump related
    UsbxBool dumpUtmiIntfPins(CarbonTime currentTime) const;
    void vcdDump(UsbxPin *pin) const;
#endif

protected:

    // Interface Read and Write functions for communicating with UTMI
    UsbxBool readInterface(CarbonTime currentTime);
    UsbxBool writeInterface(CarbonTime currentTime);
    void processRxCmd(CarbonTime currentTime);
    CarbonUInt8 updateFunctionCtrlReg(CarbonTime currentTime);
    CarbonUInt8 updateOtgCtrlReg(CarbonTime currentTime);
};

/*!
 * \brief Class to represent ULPI PHY pin level interface
 */
class UsbxUlpiPhyIntf: public UsbxUlpiIntf
{
public: CARBONMEM_OVERRIDES

protected:

    UsbxBool _pidByteReceivedByUtmi;
    UsbxBool _rxCmdSent;
    UsbxBool _isAborted;
    CarbonUInt8 _rxCmd;
    CarbonUInt8 _registerAddress;           //!< Register Address for RegWrite
    UsbxUlpiFsmStateType _abortedFromState;

public:

    // Constructor and destructor
    UsbxUlpiPhyIntf(UsbXtor *xtor);
    ~UsbxUlpiPhyIntf() {}

    void setSpeedFS(UsbxBool isFS, CarbonTime currentTime);

    // Member function
    UsbxBool receiving(CarbonTime currentTime) const
    { return _utmiIntf->receiving(currentTime); }

    // Evaluates the interface FSM
    UsbxBool evaluate(CarbonTime currentTime);

    // Refresh functions
    UsbxBool refreshInputs(CarbonTime currentTime);
    UsbxBool refreshOutputs(CarbonTime currentTime);

#ifdef _VCD
    // VCD dump related
    UsbxBool dumpUtmiIntfPins(CarbonTime currentTime) const;
    void vcdDump(UsbxPin *pin) const;
#endif

protected:

    // Interface Read and Write functions for communicating with UTMI
    UsbxBool readInterface(CarbonTime currentTime);
    UsbxBool writeInterface(CarbonTime currentTime);
    CarbonUInt8 updateRxCmd(CarbonTime currentTime);
    void updateRegister(CarbonTime currentTime);
    CarbonUInt8 readRegisterData(CarbonTime currentTime);

    // Immediate Register clear, set, read and write operations
    CarbonUInt8 readVendorIdLow(CarbonTime currentTime);
    CarbonUInt8 readVendorIdHigh(CarbonTime currentTime);
    CarbonUInt8 readProductIdLow(CarbonTime currentTime);
    CarbonUInt8 readProductIdHigh(CarbonTime currentTime);

    CarbonUInt8 readFunctionCtrlReg(CarbonTime currentTime);
    void processFunctionCtrlReg(CarbonTime currentTime);
    void clearFunctionCtrlReg(CarbonTime currentTime);
    void setFunctionCtrlReg(CarbonTime currentTime);

    CarbonUInt8 readInterfaceCtrlReg(CarbonTime currentTime);
    void processInterfaceCtrlReg(CarbonTime currentTime);
    void clearInterfaceCtrlReg(CarbonTime currentTime);
    void setInterfaceCtrlReg(CarbonTime currentTime);

    CarbonUInt8 readOtgCtrlReg(CarbonTime currentTime);
    void processOtgCtrlReg(CarbonTime currentTime);
    void clearOtgCtrlReg(CarbonTime currentTime);
    void setOtgCtrlReg(CarbonTime currentTime);

    CarbonUInt8 readInterruptEnableRisingReg(CarbonTime currentTime);
    void processInterruptEnableRisingReg(CarbonTime currentTime);
    void clearInterruptEnableRisingReg(CarbonTime currentTime);
    void setInterruptEnableRisingReg(CarbonTime currentTime);

    CarbonUInt8 readInterruptEnableFallingReg(CarbonTime currentTime);
    void processInterruptEnableFallingReg(CarbonTime currentTime);
    void clearInterruptEnableFallingReg(CarbonTime currentTime);
    void setInterruptEnableFallingReg(CarbonTime currentTime);

    CarbonUInt8 readInterruptStatusReg(CarbonTime currentTime);
    CarbonUInt8 readInterruptLatchReg(CarbonTime currentTime);
    CarbonUInt8 readDebugReg(CarbonTime currentTime);

    CarbonUInt8 readScratchReg(CarbonTime currentTime);
    void processScratchReg(CarbonTime currentTime);
    void clearScratchReg(CarbonTime currentTime);
    void setScratchReg(CarbonTime currentTime);

    CarbonUInt8 readCarkitCtrlReg(CarbonTime currentTime);
    void processCarkitCtrlReg(CarbonTime currentTime);
    void clearCarkitCtrlReg(CarbonTime currentTime);
    void setCarkitCtrlReg(CarbonTime currentTime);

    CarbonUInt8 readCarkitIntrDelayReg(CarbonTime currentTime);
    void processCarkitIntrDelayReg(CarbonTime currentTime);

    CarbonUInt8 readCarkitIntrEnableReg(CarbonTime currentTime);
    void processCarkitIntrEnableReg(CarbonTime currentTime);
    void clearCarkitIntrEnableReg(CarbonTime currentTime);
    void setCarkitIntrEnableReg(CarbonTime currentTime);

    CarbonUInt8 readCarkitIntrStatusReg(CarbonTime currentTime);
    CarbonUInt8 readCarkitIntrLatchReg(CarbonTime currentTime);

    CarbonUInt8 readCarkitPulseCtrlReg(CarbonTime currentTime);
    void processCarkitPulseCtrlReg(CarbonTime currentTime);
    void clearCarkitPulseCtrlReg(CarbonTime currentTime);
    void setCarkitPulseCtrlReg(CarbonTime currentTime);

    CarbonUInt8 readTransmitPositiveWidthReg(CarbonTime currentTime);
    void processTransmitPositiveWidthReg(CarbonTime currentTime);

    CarbonUInt8 readTransmitNegativeWidthReg(CarbonTime currentTime);
    void processTransmitNegativeWidthReg(CarbonTime currentTime);

    CarbonUInt8 readReceivePolarityReg(CarbonTime currentTime);
    void processReceivePolarityReg(CarbonTime currentTime);

    CarbonUInt8 readExtendedReg(CarbonTime currentTime);
    void processExtendedReg(CarbonTime currentTime);
};

#endif
