/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_TRANSUNIT_H
#define USBX_TRANSUNIT_H

#include "usbXPacket.h"
#include "usbXPipe.h"
#include "xactors/usb/carbonXUsbTrans.h"

/*!
 * \file usbXTransUnit.h
 * \brief Header file for transaction unit class.
 */

/*
 * \brief Class representing USB transaction unit.
 *
 * This class represents actual USB transactions within a transfer. Each such
 * USB transactions will be accompanied with a token packet and a data payload
 * packet.
 */
class UsbxTransUnit
{
public: CARBONMEM_OVERRIDES

private:

    UsbxTrans *_trans;                    //!< Transfer for which this unit is
                                          //   is created
    UsbxTokenPacket *_tokenPacket;        //!< Token packet associated
    UsbxDataPacket *_dataPacket;          //!< Data packet associated
    UsbxBool _isLast;                     //!< Indicates last transaction unit
    UInt _errorCount;                     //!< Error count

    const CarbonXUsbTransRetry *_retry;   //!< Retry state
    UsbxBool _stall;                      //!< Stall state
    UInt _retryCount;                     //!< Retries already provided

public:

    UsbxTransUnit(UsbxTrans *trans, UsbxTokenPacket *tokenPacket,
            UsbxDataPacket *dataPacket = NULL, UsbxBool isLast = UsbxFalse);
    ~UsbxTransUnit();

    // Member access functions
    UsbxTrans *getTrans() { return _trans; }
    UsbxTokenPacket *getTokenPacket() const { return _tokenPacket; }
    UsbxDataPacket *getDataPacket() const { return _dataPacket; }
    void setDataPacket(UsbxDataPacket *dataPacket) { _dataPacket = dataPacket; }
    UsbxBool isLast() const { return _isLast; }
    void setLast() { _isLast = UsbxTrue; }
    UInt incrErrorCount() { return _errorCount++; }

    void setRetryStallState(const CarbonXUsbTransRetry *retry, UsbxBool stall)
    { _retry = retry; _stall = stall; }
    UsbxBool hasRetryState() const;
    void incrRetryCount() { _retryCount++; }
    UsbxBool hasStallState() const { return _stall; }

    // Info functions
    CarbonXUsbPipeDirection getDirection() const;

    // Debugging routine
    void print() const;
};

#endif
