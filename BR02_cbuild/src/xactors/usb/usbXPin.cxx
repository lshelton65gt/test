/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    usbXPin.cpp
    Purpose: This file provides the member function definition for pins.
*/

#include "usbXPin.h"
#include "usbXtor.h"

#ifdef _VCD
UsbxBool UsbxPin::_enableBusDump = UsbxFalse;
#endif

// Constructor
UsbxPin::UsbxPin(UsbXtor *xtor, UsbxConstString name,
        const UsbxDirection direction, const CarbonUInt32 size):
    _xtor(xtor), _name(UsbxStrdup(name)),
    _direction(direction), _inoutDirControl(UsbxDirection_Unknown),
    _size(size), _currValue(0),
#ifdef _VCD
    _lastValue(0),
#endif
    _module(NULL), _net(NULL)
{
    // Check for memory allocation error
    if (_name == NULL)
    {
        xtor->errorForNoMemory(name);
        return;
    }

    // Initialize callback trio
    _callbackTrio.TransactorSignalName = _name;
    _callbackTrio.ModelInputChangeCB = NULL;
    _callbackTrio.ModelOutputChangeCB = NULL;
    _callbackTrio.UserPointer = NULL;

#ifdef _VCD
    FILE *vcdFile = UsbXtor::getVcdFile();
    if (vcdFile && (_direction == UsbxDirection_Output ||
                _direction == UsbxDirection_Inout))
    {
        if (_xtor == NULL)
            fprintf(vcdFile, "$var wire %d %s %s $end\n",
                _size, _name, _name);
        else
            fprintf(vcdFile, "$var wire %d %s.%s %s $end\n",
                _size, _xtor->_vcdModuleName, _name, _name);
    }
#endif
}

UsbxPin::~UsbxPin()
{
    DELETE(_name);
}

// Bind functions
UsbxBool UsbxPin::bind(CarbonObjectID *module, CarbonNetID *net)
{
    _module = module;
    _net = net;
    return UsbxTrue;
}

UsbxBool UsbxPin::bind(CarbonXInterconnectNameCallbackTrio trio)
{
    _callbackTrio = trio;
    return UsbxTrue;
}

// Write
UsbxBool UsbxPin::write(const CarbonUInt32 data, CarbonTime currentTime)
{
    ASSERT(_size <= 32, _xtor);
    _currValue = data & (static_cast<UInt>(-1) >> (32 - _size));
    return UsbxTrue;

    currentTime = 0; /* Identifier assigned with any value to remove
                        compile time warning during normal compilation */
}

// Read
CarbonUInt32 UsbxPin::read(CarbonTime currentTime) const
{
    CarbonUInt32 data;
    ASSERT(_size <= 32, _xtor);
    data = _currValue & (static_cast<UInt>(-1) >> (32 - _size));
    return data;

    currentTime = 0; /* Identifier assigned with any value to remove
                        compile time warning during normal compilation */
}

// Refresh function to transfer data between transactor pin and VHM pin
UsbxBool UsbxPin::refresh(CarbonTime currentTime)
{
    UsbxBool retValue = UsbxFalse;
    if (_direction == UsbxDirection_Input ||
            (_direction == UsbxDirection_Inout &&
             _inoutDirControl == UsbxDirection_Input)) // Read
    {
        if (_module && _net) // Use VHM functions
        {
            if (carbonExamine(_module, _net, &_currValue, NULL) == eCarbon_OK)
                retValue = UsbxTrue;
        }
        else if (_callbackTrio.ModelOutputChangeCB != NULL &&
                _callbackTrio.UserPointer != NULL) // Use user callbacks
        {
            if ((*(_callbackTrio.ModelOutputChangeCB))
                    (&_currValue, _callbackTrio.UserPointer) == eCarbon_OK)
                retValue = UsbxTrue;
        }
    }
    else if (_direction == UsbxDirection_Output ||
            (_direction == UsbxDirection_Inout &&
             _inoutDirControl == UsbxDirection_Output)) // Write
    {
        if (_module && _net) // Use VHM functions
        {
            if (carbonDeposit(_module, _net, &_currValue, NULL) == eCarbon_OK)
                retValue = UsbxTrue;
        }
        else if (_callbackTrio.ModelInputChangeCB != NULL &&
                _callbackTrio.UserPointer != NULL) // Use user callbacks
        {
            if ((*(_callbackTrio.ModelInputChangeCB))
                    (&_currValue, _callbackTrio.UserPointer) == eCarbon_OK)
                retValue = UsbxTrue;
        }
#ifdef _VCD
        if (_enableBusDump)
            UsbXtor::dumpVcdTime(currentTime);
        dumpVcd(currentTime == 0);
#endif
    }
    if (!retValue)
    {
        // Error in connection
        UsbxString buffer = UsbxGetStringBuffer();
        sprintf(buffer, "Connection error "
                "during refresh for '%s' pin at time %u.", _name,
                static_cast<UInt>(currentTime));
        UsbXtor::error(_xtor, CarbonXUsbError_ConnectionError, buffer);
    }
    return retValue;
}

/* This routines prints the pin details */
void UsbxPin::print(CarbonTime currentTime) const
{
    printf("%s[%u] = ", _name, static_cast<UInt>(currentTime));
    for (Int i = _size - 1; i >= 0; i--) /* Don't make 'i' UInt */
    {
        CarbonUInt32 mask = 0x1 << i;
        printf("%u", (_currValue & mask) >> i);
    }
    printf("\n");
}

#ifdef _VCD
UsbxBool UsbxPin::dumpVcd(UsbxBool forceDump)
{
    FILE *vcdFile = UsbXtor::getVcdFile();
    if (vcdFile == NULL)
        return UsbxFalse;
    if (!forceDump && _currValue == _lastValue)
        return UsbxFalse;
    if (_size > 1)
    {
        fprintf(vcdFile, "b");
        for (Int i = _size - 1; i >= 0; i--) /* Don't make 'i' UInt */
        {
            CarbonUInt32 mask = 0x1 << i;
            fprintf(vcdFile, "%u", (_currValue & mask) >> i);
        }
        fprintf(vcdFile, " ");
    }
    else
        fprintf(vcdFile, "%u", _currValue);
    if (_xtor == NULL)
        fprintf(vcdFile, "%s\n", _name);
    else
        fprintf(vcdFile, "%s.%s\n", _xtor->_vcdModuleName, _name);
    _lastValue = _currValue;
    return UsbxTrue;
}

UsbxBool UsbxPin::enableVcdDump(UsbxConstString fileName)
{
    UsbXtor::enableVcdDump(fileName);
    FILE *vcdFile = UsbXtor::getVcdFile();
    if (vcdFile == NULL)
        return UsbxFalse;
    fprintf(UsbXtor::getVcdFile(), "$scope module CarbonXUsb $end\n");
    _enableBusDump = UsbxTrue;
    return UsbxTrue;
}
#endif
