/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    usbXTransUnit.cpp
    Purpose: This file contains the definition of USB transaction unit.
*/

#include "usbXTransUnit.h"
#include "usbXTrans.h"
#include "usbXtor.h"

UsbxTransUnit::UsbxTransUnit(UsbxTrans *trans, UsbxTokenPacket *tokenPacket,
        UsbxDataPacket *dataPacket, UsbxBool isLast):
    _trans(trans), _tokenPacket(tokenPacket),
    _dataPacket(dataPacket), _isLast(isLast), _errorCount(0),
    _retry(NULL), _stall(UsbxFalse), _retryCount(0)
{}

UsbxTransUnit::~UsbxTransUnit()
{
    delete _tokenPacket;
    delete _dataPacket;
}

CarbonXUsbPipeDirection UsbxTransUnit::getDirection() const
{
    ASSERT(_tokenPacket, _trans->getPipe()->getTransactor());
    if (_tokenPacket->getType() == UsbxPacket_IN)
        return CarbonXUsbPipeDirection_INPUT;
    else
        return CarbonXUsbPipeDirection_OUTPUT;
}

UsbxBool UsbxTransUnit::hasRetryState() const
{
    if (_retry == NULL || _retryCount >= _retry->retryCount)
        return UsbxFalse;
    return UsbxTrue;
}

void UsbxTransUnit::print() const
{
    printf("Transaction Unit:\n");
    if (_tokenPacket)
        _tokenPacket->print();
    if (_dataPacket)
        _dataPacket->print();
    if (_isLast)
        printf("Last transaction unit.\n");
}
