/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_SCHEDULER_H
#define USBX_SCHEDULER_H

#include "usbXDefines.h"
#include "usbXPipe.h"
#include "usbXConfig.h"

/*!
 * \file usbXScheduler.h
 * \brief Header file for scheduler class.
 *
 * This file provides the class definition for scheduler object.
 */

/*!
 * \brief Class to represent scheduler object.
 *
 * This scheduler class when asked to schedule, will provide pointer to
 * a pipe. FSM will consider the transfer pending or under process from
 * from of the pipe's transfer queue and send next USB transaction in the
 * transfer.
 */
class UsbxScheduler
{
public: CARBONMEM_OVERRIDES

private:


    UsbXtor *_xtor;                           //!< Transactor reference
    UInt _speedFactor;                        //!< Speed factor for UTMI bus
                                              //   width
    UInt _frameNumber;                        //!< Frame number

    // Frame time monitoring using clocks
    UInt _frameClockCount;                    //!< To track frame time
    UInt _stateClockCount;                    //!< Clock for P or NP state
    UInt _remainingNpFrameClocks;             //!< Available non-periodic
                                              //   frame clocks

    // Pipe Tracking members
    UInt _nextNonPeriodicPipeIndex;           //!< Next non-periodic pipe id
    UInt _nextPeriodicPipeIndex;              //!< Next periodic pipe id
    UInt _repeatCountForHsPeriodic;           //!< Repeat number for HS
                                              //   periodic transfer i.e.
                                              //   wMaxPacketSize[12:11]

    //! Scheduler States
    enum UsbxSchedulerState {
        UsbxSchedulerState_SOF,               //!< Send SOF
        UsbxSchedulerState_Wait,              //!< Wait for SOF completion
        UsbxSchedulerState_NonPeriodic,       //!< Non-periodic state
        UsbxSchedulerState_Periodic           //!< Periodic state
    };

    UsbxSchedulerState _state;                //!< Scheduler FSM state
    UsbxBool _schedulerStarted;               //!< To track if scheduler active
    UsbxBool _busReclaimedNPState;            //!< NP state after bus reclaiming
    UsbxPacket *_sofPacket;                   //!< SOF packet to manage

    // Scheduler timing parameters
    static const UInt _clocksPerFSBit;
    static const UInt _maxInterPacketDelayFS;
    static const UInt _maxInterPacketDelayHS;
    static const UInt _maxBusTurnAroundTimeFS;
    static const UInt _maxBusTurnAroundTimeHS;

    // Maximum Frame Index number possible is 2^15
    enum { _maxFrameIndex = 32768 };
    // To keep track of bandwidth allocation per frame for periodic pipes
    UInt _periodicAllocTable[_maxFrameIndex]; //!< Periodic BW allocation

public:

    // Constructor
    UsbxScheduler(UsbXtor *xtor, CarbonXUsbUtmiDataBusWidthType utmiWidth);

    // Schedule start and stop function
    CarbonUInt11 getFrameNumber() const { return (_frameNumber & 0x7FF); }
    void startScheduler();
    void stopScheduler();

    // Time calculation
    void evaluate(CarbonTime currentTime);

    // Update scheduler for pipe creation or deletion
    UsbxBool updatePeriodicPolicy(UsbxPipe *pipe, UsbxBool isCreate);

    // Schedule function to be called by Host
    UsbxPipe *schedulePipe(CarbonTime currentTime);

protected:

    // Scheduler functions
    UsbxPipe *getNextNonPeriodicPipe(CarbonXUsbPipeType pipeType);
    UsbxPipe *getNextPeriodicPipe();
    UsbxBool isPeriodicPipeSchedulable(UsbxPipe *pipe);

    // Calculation of variance for best-fit scheduler policy
    UInt computeFrameVariance(UInt frameOffset, UsbxPipe *pipe,
            UsbxBool &schedulableInFuture) const;
    UInt computeFrameMean(UInt frameOffset, UsbxPipe *pipe,
            UsbxBool &schedulableInFuture) const;

    // Utility functions
    UInt getClocksPerFrame() const;
    UInt getClocksForMaxPeriodicTransUnit(const UsbxPipe *pipe) const;
    UInt getClocksForNextTransUnit(const UsbxPipe *pipe) const;
    UsbxBool hasRetried(const UsbxPipe *pipe) const;
};

#endif
