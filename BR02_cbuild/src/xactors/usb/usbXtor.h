/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_XTOR_H
#define USBX_XTOR_H

#include "usbXArray.h"
#include "usbXQueue.h"
#include "usbXPin.h"
#include "usbXConfig.h"
#include "usbXTrans.h"
#include "usbXPipe.h"
#include "usbXIntf.h"
#include "usbXScheduler.h"
#include "usbXFsm.h"
#include "usbXHostFsm.h"
#include "usbXDeviceFsm.h"
#include "xactors/usb/carbonXUsb.h"

#ifndef _NO_LICENSE
#include "util/UtCLicense.h"
#endif

/*!
 * \file usbXtor.h
 * \brief Header file for transactor definition
 *
 * This file provides the definition of the class of transactor and
 * declares the functions used to update the FSM and to make a transaction.
 */

//! OTG Device Type
enum UsbxOtgDeviceType {
    UsbxOtgDevice_B = 0,                   //!< B Device
    UsbxOtgDevice_A = 1                    //!< A Device
};

/*!
 * \brief Base class for transactor.
 *
 * This class contains the functions for initializing and updating the
 * transactor and for compliting a transaction.
 */
class CarbonXUsbClass
{
public: CARBONMEM_OVERRIDES

protected:

    UsbxConstString _name;                 //!< Name of the transactor
    UsbxBool _isFS;                        //!< USB speed of current operation
    CarbonXUsbConfig *_config;             //!< Pointer to the configuration
                                           //   object for this transactor

    //!******** Building units ********
    UsbxArray<UsbxPipe> *_pipeList;        //!< Pipes
    UsbxFsm *_fsm;                         //!< FSM
    UsbxIntf *_intf;                       //!< Pin level interface

    //!******** Transaction callbacks ********
    void (*_setDefaultResponse)(CarbonXUsbTrans*, void*);
    void (*_setResponse)(CarbonXUsbTrans*, void*);
    void (*_reportTransaction)(CarbonXUsbTrans*, void*);
    void (*_reportCommand)(CarbonXUsbCommand, void*);

    CarbonXUsbErrorType _error;            //!< Holds the type of error
                                           //   encountered
    UsbxConstString _errorMessage;         //!< Corresponding error message

    //! Callback when error happened for a transaction
    void (*_errorTransCallback)(CarbonXUsbErrorType, const char*,
            const CarbonXUsbTrans*, void*);
    //! Callback when error happened otherwise
    void (*_errorCallback)(CarbonXUsbErrorType, const char*, void*);

    //! Some user data to provide the callback functions as argument
    void *_userContext;

public:
#ifndef _NO_LICENSE
    /* Carbon licensing parameter */
    CarbonLicenseData *_carbonLicense;     //!< Carbon license object
#endif

#ifdef _VCD
    //!******** VCD dump related ********
    UsbxString _vcdModuleName;             //!< VCD module name
    static FILE *_vcdFile;                 //!< VCD file handle
    static UsbxBool _vcdDumpStarted;       //!< VCD dump started indicator
    static CarbonTime _vcdLastTime;        //!< VCD last dump time
#endif

public:

    //! Constructor
    CarbonXUsbClass(UsbxConstString name, const CarbonXUsbConfig *config);
    //! Destructor
    virtual ~CarbonXUsbClass();

    //! This function returns transactor's name
    UsbxConstString getName() const { return _name;}
    //! Checks if the speed if FS, otherwise HS
    UsbxBool isFS() const { return _isFS;}
    void setSpeedFS(UsbxBool isFS, CarbonTime currentTime);
    //! Returns the current configuration
    CarbonXUsbConfig* getConfig() const {return _config;}
    //! This function returns transactor's type
    virtual CarbonXUsbType getType() const = 0;
    virtual UsbxBool isHost() const = 0;
    virtual void setHost(UsbxBool isHost) { isHost = UsbxFalse; }
    //! This function prints the current status of the transactor
    void print() const;
    //! Returns the current error encountered
    CarbonXUsbErrorType getError() const { return _error; }
    //! Returns the string message for current error encountered
    UsbxConstString getErrorMessage() const { return _errorMessage; }
    //! Sets error and error message for no memory error
    void errorForNoMemory(UsbxConstString objectName);
    //! Checks if all memory for transactor is created
    UsbxBool allMemoryAllocated() const { return (_fsm) && (_intf); }

    //! Get interface
    UsbxIntf *getInterface() const { return _intf; }
    //! Given the pin name this function returns the pin handle
    UsbxPin *getPinByName(UsbxConstString netName);

    // SOF tracking
    virtual CarbonUInt11 getFrameNumber() const = 0;

    //! Evaluation and refresh functions
    UsbxBool evaluate(CarbonTime currentTime);
    UsbxBool refreshInputs(CarbonTime currentTime);
    UsbxBool refreshOutputs(CarbonTime currentTime);

    //! Callback registation function
    virtual void registerCallbacks(void *userContext,
            void (*reportTransaction)(CarbonXUsbTrans*, void*),
            void (*setDefaultResponse)(CarbonXUsbTrans*, void*),
            void (*setResponse)(CarbonXUsbTrans*, void*),
            void (*reportCommand)(CarbonXUsbCommand, void*),
            void (*errorTransCallback)(CarbonXUsbErrorType, const char*,
                const CarbonXUsbTrans*, void*),
            void (*errorCallback)(CarbonXUsbErrorType, const char*, void*));
    void setDefaultResponse(CarbonXUsbTrans *trans);
    void setResponse(CarbonXUsbTrans *trans);
    void reportTransaction(CarbonXUsbTrans *trans);
    void reportCommand(CarbonXUsbCommand command);

    //! Function for setting the error type encountered
    static void error(const CarbonXUsbClass *xtor,
            CarbonXUsbErrorType errorType, UsbxConstString errorMessage = NULL,
            const CarbonXUsbTrans *trans = NULL);

    //! Pipe routines
    virtual UsbxPipe *createPipe(CarbonUInt7 deviceAddress,
            const UsbxByte *epDescriptor);
            // Note, when endpoint descriptor (epDescriptor) is NULL,
            // that will indicate endpoint zero i.e. EP0 before configuration.
            // Also note, for EP0 epDescriptor will be set later by
            // the retrieved device descriptor during configuration.
    virtual UsbxBool destroyPipe(UsbxPipe *pipe);
    UInt getPipeCount() const { return _pipeList->getCount(); }
    UsbxPipe * const *getPipes() const { return _pipeList->getArray(); }
    UsbxPipe *getPipe(CarbonUInt4 epNumber, CarbonXUsbPipeDirection epDir);

    //! Device attach and detach functions
    virtual UsbxBool attach() { return UsbxFalse; }
    virtual UsbxBool detach() { return UsbxFalse; }

    //! Enumerate bus for newly attached device
    virtual UsbxBool startBusEnumeration() { return UsbxFalse; }

    //! Starts a new transaction
    virtual UsbxBool startNewTransaction(UsbxTrans *trans)
    { return UsbxFalse; trans = NULL; }

    //! Process transaction internally from within transactor
    virtual UsbxBool processTrans(UsbxTrans *trans,
            UsbxBool defaultResp = UsbxFalse) = 0;

    //! Request attached Device transactor to enter suspend state
    virtual UsbxBool suspend() { return UsbxFalse; }
    //! Request attached suspended transactor to resume
    virtual UsbxBool resume() { return UsbxFalse; }

    //! Reset transactor for detachment
    virtual void resetForDetach() = 0;

    //**** OTG specific ****
    //! OTG A or B device detection
    virtual UsbxOtgDeviceType getOtgDeviceType() const = 0;
    virtual void setOtgDeviceType(UsbxOtgDeviceType otgDeviceType)
    { otgDeviceType = UsbxOtgDevice_B; }
    //! Start and stop OTG session
    virtual UsbxBool startOtgSession() { return UsbxFalse; }
    virtual UsbxBool stopOtgSession() { return UsbxFalse; }
    virtual UsbxBool startOtgHnp() { return UsbxFalse; }
    virtual UsbxBool stopOtgHnp() { return UsbxFalse; }

#ifdef _VCD
    //! VCD related functions
    static void dumpVcdTime(CarbonTime currentTime);
    static FILE *getVcdFile() { return _vcdFile; }
    static UsbxBool enableVcdDump(UsbxConstString fileName = NULL);
#endif
};

/*!
 * \brief Class for Host transactor.
 */
class UsbxHostXtor: virtual public CarbonXUsbClass
{
public: CARBONMEM_OVERRIDES

protected:

    UsbxHostFsm *_hostFsm;                 //!< Host FSM
    UsbxScheduler *_scheduler;             //!< Scheduler
    UsbxBool _deviceAddressTable[127];     //!< Address table

public:

    //! Constructor and desctructor
    UsbxHostXtor(UsbxConstString name, const CarbonXUsbConfig *config);
    ~UsbxHostXtor();

    //! Member access functions
    virtual CarbonXUsbType getType() const { return CarbonXUsb_Host; }
    virtual UsbxBool isHost() const { return UsbxTrue; }

    //! Scheduler related functions
    UsbxScheduler *getScheduler() { return _scheduler; }
    virtual CarbonUInt11 getFrameNumber() const
    { return _scheduler->getFrameNumber(); }

    //! Address table updation functions
    void initAddressTable();
    CarbonUInt7 getNewDeviceAddress(); // returns 0 when address table is full
    void freeDeviceAddress(CarbonUInt7 deviceAddress)
    { _deviceAddressTable[(deviceAddress & 0x7F) - 1] = UsbxFalse; }

    //! Pipe create / destroy implementation overridden for scheduler
    UsbxPipe *createPipe(CarbonUInt7 deviceAddress,
            const UsbxByte *epDescriptor);
    UsbxBool destroyPipe(UsbxPipe *pipe);

    //! Enumerate bus for newly attached device
    UsbxBool startBusEnumeration();

    //! Starts a new transaction
    UsbxBool startNewTransaction(UsbxTrans *trans);

    //! Process internally generated transactions
    UsbxBool processTrans(UsbxTrans *trans, UsbxBool defaultResp = UsbxFalse);

    //! Request attached Device transactor to enter suspend state
    UsbxBool suspend();

    //! Request attached suspended Device transactor to resume
    UsbxBool resume();

    //! Reset transactor for detachment
    void resetForDetach();

    //! OTG specific A or B device detection
    virtual UsbxOtgDeviceType getOtgDeviceType() const
    { return UsbxOtgDevice_A; }
};

/*!
 * \brief Class for Device transactor.
 */
class UsbxDeviceXtor: virtual public CarbonXUsbClass
{
public: CARBONMEM_OVERRIDES

protected:

    UsbxDeviceFsm *_deviceFsm;          //!< Device FSM
    CarbonUInt7 _deviceAddress;         //!< Device address
    CarbonUInt17 _defaultConfigNumber;  //!< Default configuration number
    CarbonUInt11 _frameNumber;          //!< Frame number tracking from SOF

public:

    //! Constructor and desctructor
    UsbxDeviceXtor(UsbxConstString name, const CarbonXUsbConfig *config);
    ~UsbxDeviceXtor();

    //! Member access functions
    virtual CarbonXUsbType getType() const { return CarbonXUsb_Device; }
    virtual UsbxBool isHost() const { return UsbxFalse; }

    CarbonUInt7 getDeviceAddress() const { return _deviceAddress; }
    void setDeviceAddress(CarbonUInt7 deviceAddress);

    virtual CarbonUInt11 getFrameNumber() const { return _frameNumber; }
    void setFrameNumber(CarbonUInt11 frameNumber)
    { _frameNumber = frameNumber; }

    //! Device attach and detach functions
    UsbxBool attach();
    UsbxBool detach();

    //! Request attached suspended Host transactor to wake up
    UsbxBool resume(); // Remote wakeup

    //! Process internally generated transactions
    UsbxBool processTrans(UsbxTrans *trans, UsbxBool defaultResp = UsbxFalse);

    //! Reset transactor for detachment
    void resetForDetach();

    //! OTG specific A or B device detection
    virtual UsbxOtgDeviceType getOtgDeviceType() const
    { return UsbxOtgDevice_B; }
};

#ifdef _MSC_VER
// Disable MSVC7 Compiler Warning::
// 'UsbxOtgDeviceXtor' : inherits 'UsbxHostXtor::<function>' via dominance
#pragma warning(disable:4250)
#endif

/*!
 * \brief Class for OTG Device transactor.
 */
class UsbxOtgDeviceXtor: public UsbxHostXtor, public UsbxDeviceXtor
{
public: CARBONMEM_OVERRIDES

private:

    UsbxOtgDeviceType _otgDeviceType;    //!< OTG device type - A or B Device
    UsbxBool _isHostMode;                //!< OTG acting as host
    UsbxPipe *_deviceDefaultControlPipe; //!< Device mode default control pipe
    UsbxBool _isInHnp;                   //!< HNP in process

public:

    //! Constructor and desctructor
    UsbxOtgDeviceXtor(UsbxConstString name, const CarbonXUsbConfig *config);
    ~UsbxOtgDeviceXtor();

    //! Member access functions
    CarbonXUsbType getType() const { return CarbonXUsb_OtgDevice; }

    UsbxBool isHost() const { return _isHostMode; }
    void setHost(UsbxBool isHost);

    CarbonUInt11 getFrameNumber() const;

    //! Request attached suspended transactor to resume
    UsbxBool resume();

    //! Process internally generated transactions
    UsbxBool processTrans(UsbxTrans *trans, UsbxBool defaultResp = UsbxFalse);

    //! Reset transactor for detachment
    void resetForDetach();

    //! OTG specific A or B device detection
    UsbxOtgDeviceType getOtgDeviceType() const { return _otgDeviceType; }
    void setOtgDeviceType(UsbxOtgDeviceType otgDeviceType)
    { _otgDeviceType = otgDeviceType; }

    //! Start and stop OTG session
    UsbxBool startOtgSession();
    UsbxBool stopOtgSession();
    UsbxBool startOtgHnp();
    UsbxBool stopOtgHnp();
    void setInHnp(UsbxBool isInHnp) { _isInHnp = isInHnp; }
    UsbxBool isInHnp() const { return _isInHnp; }
};

#endif
