/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_PACKET_H
#define USBX_PACKET_H

#include "usbXDefines.h"

/*!
 * \file usbXPacket.h
 * \brief Header file for packet definition and packet encoding &
 *        decoding logic.
 *
 * This file provides the class definition for different token, data,
 * handshake and special packets.
 *
 * This file also includes encoding and decoding logic.
 */

/*!
 * \brief Packet type
 */
enum UsbxPacketType {
    UsbxPacket_UNKNOWN = 0x0,  //!< Unknown packet
    UsbxPacket_OUT = 0x1,      //!< Address + endpoint number in
                               //   host-to-function transaction
    UsbxPacket_IN = 0x9,       //!< Address + endpoint number in
                               //   function-to-host transaction
    UsbxPacket_SOF = 0x5,      //!< Start-of-Frame marker and frame number
    UsbxPacket_SETUP = 0xD,    //!< Address + endpoint number in
                               //   host-to-function transaction for SETUP to
                               //   a control pipe
    UsbxPacket_DATA0 = 0x3,    //!< Data packet PID even
    UsbxPacket_DATA1 = 0xB,    //!< Data packet PID odd
    UsbxPacket_DATA2 = 0x7,    //!< Data packet PID high-speed, high bandwidth
                               //   isochronous transaction in a microframe
    UsbxPacket_MDATA = 0xF,    //!< Data packet PID high-speed for split and
                               //   high bandwidth isochronous transactions
    UsbxPacket_ACK = 0x2,      //!< Receiver accepts error-free data packet
    UsbxPacket_NAK = 0xA,      //!< Receiving device cannot accept data or
                               //   transmitting device cannot send data
    UsbxPacket_STALL = 0xE,    //!< Endpoint is halted or a control pipe request
                               //   is not supported
    UsbxPacket_NYET = 0x6,     //!< No response yet from receiver
    UsbxPacket_ERR = 0xC,      //!< Split Transaction Error Handshake
    UsbxPacket_SPLIT = 0x8,    //!< (Token) High-speed Split Transaction Token
    UsbxPacket_PING = 0x4      //!< (Token) High-speed flow control probe for a
                               //   bulk/control endpoint
};

/*!
 * \brief Packet error type during reception
 */
enum UsbxPacketReceiveErrorType {
    UsbxPacketReceiveError_None = 0, //!< No error
    UsbxPacketReceiveError_Pid,      //!< PID error
    UsbxPacketReceiveError_Size,     //!< Invalid packet size
    UsbxPacketReceiveError_Crc,      //!< CRC error
    UsbxPacketReceiveError_Unknown   //!< Unknown error
};

/*!
 * \brief Split transaction type
 */
enum UsbxSplitTransType {
    UsbxSplitTransType_START = 0,    //!< Indicate a Start-split Transaction
    UsbxSplitTransType_COMPLETE = 1  //!< Indicate a Complete-split Transaction
};

/*!
 * \brief Base class to represent packet.
 */
class UsbxPacket
{
public: CARBONMEM_OVERRIDES

protected:

    UsbxByte *_byteArray;
    UsbxPacketReceiveErrorType _receiveError;

public:

    // Constructor and destructor
    UsbxPacket(UsbxByte *byteArray = NULL):
        _byteArray(byteArray), _receiveError(UsbxPacketReceiveError_None) {}
    virtual ~UsbxPacket() { DELETE(_byteArray); }

    // Member functions
    virtual UsbxPacketType getType() const = 0;

    virtual UInt getSizeInBytes() const = 0;
    const UsbxByte *getByteArray() const { return _byteArray; }

    UsbxPacketReceiveErrorType hasReceiveError() const
    { return _receiveError; }

    void print() const;

protected:

    virtual void printFields() const = 0;

public:

    // Utility functions
    static UsbxPacketType getPacketType(const UsbxByte *byteArray);
    static UsbxPacket *getPacket(UInt sizeInBytes, const UsbxByte *byteArray);
    UsbxConstString getPacketTypeString() const;
    UsbxConstString getPacketErrorString() const;
    static void printBits(UsbxByte dataByte, UInt size = 8,
            UsbxBool lsb2msb = UsbxTrue);
};

// Forward declaration
class UsbxSofPacket;
class UsbxSplitPacket;

/*!
 * \brief Base class to represent token packets except SOF.
 *
 * This includes OUT, IN and SETUP packet functionalities.
 */
class UsbxTokenPacket: public UsbxPacket
{
public: CARBONMEM_OVERRIDES

private:

    class UsbxCrc5
    {
    public: CARBONMEM_OVERRIDES

    private:

        CarbonUInt5 _crcLfsr;

    public:

        UsbxCrc5() { _crcLfsr = 0x1F; }

        CarbonUInt5 getRevCrc() const;
        CarbonUInt5 update(UsbxByte data, UInt dataSize = 8);
    };

    friend class UsbxSofPacket;
    friend class UsbxSplitPacket;

public:

    // Constructor and destructor
    UsbxTokenPacket(CarbonUInt4 pid, CarbonUInt7 deviceAddress,
            CarbonUInt4 endpointNumber);
    UsbxTokenPacket(CarbonUInt4 pid, UInt sizeInBytes,
            const UsbxByte *byteArray);

    // Member functions
    UInt getSizeInBytes() const
    { return 3; /* 8 bit PID + 7 bit ADDR + 4 bit ENDP + 5 bit CRC */ }
    CarbonUInt7 getDeviceAddress() const;
    CarbonUInt4 getEndpointNumber() const;

protected:

    void printFields() const;
};

/*!
 * \brief Class to represent OUT token packet.
 */
class UsbxOutPacket: public UsbxTokenPacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor and destructor
    UsbxOutPacket(CarbonUInt7 deviceAddress, CarbonUInt4 endpointNumber):
        UsbxTokenPacket(UsbxPacket_OUT, deviceAddress, endpointNumber) {}
    UsbxOutPacket(UInt sizeInBytes, const UsbxByte *byteArray):
        UsbxTokenPacket(UsbxPacket_OUT, sizeInBytes, byteArray) {}

    // Member functions
    UsbxPacketType getType() const { return UsbxPacket_OUT; }
};

/*!
 * \brief Class to represent IN token packet.
 */
class UsbxInPacket: public UsbxTokenPacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor and destructor
    UsbxInPacket(CarbonUInt7 deviceAddress, CarbonUInt4 endpointNumber):
        UsbxTokenPacket(UsbxPacket_IN, deviceAddress, endpointNumber) {}
    UsbxInPacket(UInt sizeInBytes, const UsbxByte *byteArray):
        UsbxTokenPacket(UsbxPacket_IN, sizeInBytes, byteArray) {}

    // Member functions
    UsbxPacketType getType() const { return UsbxPacket_IN; }
};

/*!
 * \brief Class to represent SETUP token packet.
 */
class UsbxSetupPacket: public UsbxTokenPacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor and destructor
    UsbxSetupPacket(CarbonUInt7 deviceAddress, CarbonUInt4 endpointNumber):
        UsbxTokenPacket(UsbxPacket_SETUP, deviceAddress, endpointNumber) {}
    UsbxSetupPacket(UInt sizeInBytes, const UsbxByte *byteArray):
        UsbxTokenPacket(UsbxPacket_SETUP, sizeInBytes, byteArray) {}

    // Member functions
    UsbxPacketType getType() const { return UsbxPacket_SETUP; }
};

/*!
 * \brief Class to represent SOF packet
 */
class UsbxSofPacket: public UsbxPacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor and destructor
    UsbxSofPacket(CarbonUInt11 frameNumber);
    UsbxSofPacket(UInt sizeInBytes, const UsbxByte *byteArray);

    // Member functions
    UsbxPacketType getType() const { return UsbxPacket_SOF; }
    UInt getSizeInBytes() const
    { return 3; /* 8 bit PID + 11 bit Frame Number + 5 bit CRC */ }
    CarbonUInt7 getFrameNumber() const;

protected:

    void printFields() const;
};

/*!
 * \brief Base class to represent data packets.
 *
 * This includes DATA0, DATA1, DATA2 and MDATA packet functionalities.
 */
class UsbxDataPacket: public UsbxPacket
{
public: CARBONMEM_OVERRIDES

public:

    CarbonUInt16 _dataPayloadSizeInBytes;

private:

    class UsbxCrc16
    {
    public: CARBONMEM_OVERRIDES

    private:

        CarbonUInt16 _crcLfsr;

    public:

        UsbxCrc16() { _crcLfsr = 0xFFFF; }

        CarbonUInt16 getRevCrc() const;
        CarbonUInt16 update(UsbxByte data, UInt dataSize = 8);
    };

public:

    // Constructor
    UsbxDataPacket(CarbonUInt4 pid, UInt sizeInBytes, const UsbxByte *byteArray,
            UsbxBool reconstruct = UsbxFalse);

    // Member functions
    UInt getSizeInBytes() const
    {
        /* 8 bit PID + (_dataPayloadSizeInBytes * 8) bits data + 16 bit CRC */
        return _dataPayloadSizeInBytes + 3;
    }
    UInt getPayloadSize() const { return _dataPayloadSizeInBytes; }
    const UsbxByte *getPayloadData() const { return getByteArray() + 1; }

protected:

    void printFields() const;
};

/*!
 * \brief Class to represent DATA0 token packet.
 */
class UsbxData0Packet: public UsbxDataPacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor
    UsbxData0Packet(UInt sizeInBytes, const UsbxByte *byteArray,
            UsbxBool reconstruct = UsbxFalse):
        UsbxDataPacket(UsbxPacket_DATA0, sizeInBytes, byteArray, reconstruct) {}

    // Member functions
    UsbxPacketType getType() const { return UsbxPacket_DATA0; }
};

/*!
 * \brief Class to represent DATA1 token packet.
 */
class UsbxData1Packet: public UsbxDataPacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor
    UsbxData1Packet(UInt sizeInBytes, const UsbxByte *byteArray,
            UsbxBool reconstruct = UsbxFalse):
        UsbxDataPacket(UsbxPacket_DATA1, sizeInBytes, byteArray, reconstruct) {}

    // Member functions
    UsbxPacketType getType() const { return UsbxPacket_DATA1; }
};

/*!
 * \brief Class to represent DATA2 token packet.
 */
class UsbxData2Packet: public UsbxDataPacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructors
    UsbxData2Packet(UInt sizeInBytes, const UsbxByte *byteArray,
            UsbxBool reconstruct = UsbxFalse):
        UsbxDataPacket(UsbxPacket_DATA2, sizeInBytes, byteArray, reconstruct) {}

    // Member functions
    UsbxPacketType getType() const { return UsbxPacket_DATA2; }
};

/*!
 * \brief Class to represent MDATA token packet.
 */
class UsbxMDataPacket: public UsbxDataPacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructors
    UsbxMDataPacket(UInt sizeInBytes, const UsbxByte *byteArray,
            UsbxBool reconstruct = UsbxFalse):
        UsbxDataPacket(UsbxPacket_MDATA, sizeInBytes, byteArray, reconstruct) {}

    // Member functions
    UsbxPacketType getType() const { return UsbxPacket_MDATA; }
};

/*!
 * \brief Base class to represent handshake packets.
 *
 * This includes ACK, NAK, STALL and NYET packet functionalities.
 */
class UsbxHandshakePacket: public UsbxPacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor and destructor
    UsbxHandshakePacket(CarbonUInt4 pid);
    UsbxHandshakePacket(CarbonUInt4 pid, UInt sizeInBytes,
            const UsbxByte *byteArray);

    // Member functions
    UInt getSizeInBytes() const
    { return 1; /* 8 bit PID */ }

protected:

    void printFields() const {}
};

/*!
 * \brief Class to represent ACK token packet.
 */
class UsbxAckPacket: public UsbxHandshakePacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor and destructor
    UsbxAckPacket(): UsbxHandshakePacket(UsbxPacket_ACK) {}
    UsbxAckPacket(UInt sizeInBytes, const UsbxByte *byteArray):
        UsbxHandshakePacket(UsbxPacket_ACK, sizeInBytes, byteArray) {}

    // Member functions
    UsbxPacketType getType() const { return UsbxPacket_ACK; }
};

/*!
 * \brief Class to represent NAK token packet.
 */
class UsbxNakPacket: public UsbxHandshakePacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor and destructor
    UsbxNakPacket(): UsbxHandshakePacket(UsbxPacket_NAK) {}
    UsbxNakPacket(UInt sizeInBytes, const UsbxByte *byteArray):
        UsbxHandshakePacket(UsbxPacket_NAK, sizeInBytes, byteArray) {}

    // Member functions
    UsbxPacketType getType() const { return UsbxPacket_NAK; }
};

/*!
 * \brief Class to represent STALL token packet.
 */
class UsbxStallPacket: public UsbxHandshakePacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor and destructor
    UsbxStallPacket(): UsbxHandshakePacket(UsbxPacket_STALL) {}
    UsbxStallPacket(UInt sizeInBytes, const UsbxByte *byteArray):
        UsbxHandshakePacket(UsbxPacket_STALL, sizeInBytes, byteArray) {}

    // Member functions
    UsbxPacketType getType() const { return UsbxPacket_STALL; }
};

/*!
 * \brief Class to represent NYET token packet.
 */
class UsbxNyetPacket: public UsbxHandshakePacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor and destructor
    UsbxNyetPacket(): UsbxHandshakePacket(UsbxPacket_NYET) {}
    UsbxNyetPacket(UInt sizeInBytes, const UsbxByte *byteArray):
        UsbxHandshakePacket(UsbxPacket_NYET, sizeInBytes, byteArray) {}

    // Member functions
    UsbxPacketType getType() const { return UsbxPacket_NYET; }
};

/*!
 * \brief Class to represent special packets.
 *
 * This represents SPLIT special packet functionalities.
 */
class UsbxSplitPacket: public UsbxPacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor and destructor
    UsbxSplitPacket(CarbonUInt7 hubAddress, CarbonUInt1 sc, CarbonUInt7 port,
            CarbonUInt1 speed, CarbonUInt1 end, CarbonUInt2 endPointType);
    UsbxSplitPacket(UInt sizeInBytes, const UsbxByte *byteArray);

    // Member functions
    UInt getSizeInBytes() const
    {
        return 4; /* 8 bit PID + 7 bit HUB_ADDR + 1 bit SC + 7 bit PORT +
                   1 bit S + 1 bit E + 2 bit ET + 5 bit CRC5 */
    }
    // Member functions
    UsbxPacketType getType() const { return UsbxPacket_SPLIT; }
    CarbonUInt7 getHubAddress() const { return _byteArray[1] & 0x7F; }
    CarbonUInt1 getSC() const { return (_byteArray[1] >> 7) & 0x1; }
    CarbonUInt7 getPortNumber() const { return _byteArray[2] & 0x7F; }
    CarbonUInt1 getSpeed() const { return (_byteArray[2] >> 7) & 0x1; }
    CarbonUInt1 getEnd() const { return _byteArray[3] & 0x1; }
    CarbonUInt2 getEndpointType() const { return (_byteArray[3] >> 1) & 0x3; }

protected:

    void printFields() const;
};

/*!
 * \brief Class to represent ERR special handshake packet.
 */
class UsbxErrPacket: public UsbxHandshakePacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor and destructor
    UsbxErrPacket() : UsbxHandshakePacket(UsbxPacket_ERR) {}
    UsbxErrPacket(UInt sizeInBytes, const UsbxByte *byteArray) :
        UsbxHandshakePacket(UsbxPacket_ERR, sizeInBytes, byteArray) {}

    // Member functions
    UsbxPacketType getType() const { return UsbxPacket_ERR; }
};

/*!
 * \brief Class to represent PING special token packet.
 */
class UsbxPingPacket: public UsbxTokenPacket
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor and destructor
    UsbxPingPacket(CarbonUInt7 deviceAddress, CarbonUInt4 endpointNumber):
        UsbxTokenPacket(UsbxPacket_PING, deviceAddress, endpointNumber) {}
    UsbxPingPacket(UInt sizeInBytes, const UsbxByte *byteArray):
        UsbxTokenPacket(UsbxPacket_PING, sizeInBytes, byteArray) {}

    // Member functions
    UsbxPacketType getType() const { return UsbxPacket_PING; }
};

#endif
