/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:      carbonXUsbConfig.cpp
    Purpose:   This file provides definition of USB Configuration
               Object Routines.
*/
#include "usbXConfig.h"
#include "xactors/usb/carbonXUsbConfig.h"

// Create and destroy functions
CarbonXUsbConfig *carbonXUsbConfigCreate(void)
{
    CarbonXUsbConfig *configObj = new CarbonXUsbConfig();
    return configObj;
}

CarbonUInt32 carbonXUsbConfigDestroy(CarbonXUsbConfig *config)
{
    delete config;
    config = NULL;
    return 1;
}

// Set get functions
void carbonXUsbConfigSetSpeed(CarbonXUsbConfig *config,
        CarbonXUsbSpeedType speed)
{
    config->setSpeed(speed);
}

CarbonXUsbSpeedType carbonXUsbConfigGetSpeed(const CarbonXUsbConfig *config)
{
    return config->getSpeed();
}

void carbonXUsbConfigSetIntfType(CarbonXUsbConfig *config,
        CarbonXUsbInterfaceType intfType)
{
    config->setIntfType(intfType);
}

CarbonXUsbInterfaceType carbonXUsbConfigGetIntfType(
        const CarbonXUsbConfig *config)
{
    return config->getIntfType();
}

void carbonXUsbConfigSetIntfSide(CarbonXUsbConfig *config,
        CarbonXUsbIntfSide intfSide)
{
    config->setIntfSide(intfSide);
}

CarbonXUsbIntfSide carbonXUsbConfigGetIntfSide(const CarbonXUsbConfig *config)
{
    return config->getIntfSide();
}

void carbonXUsbConfigSetUtmiDataBusWidth(CarbonXUsbConfig *config,
        CarbonXUsbUtmiDataBusWidthType busWidth)
{
    config->setUtmiDataBusWidth(busWidth);
}

CarbonXUsbUtmiDataBusWidthType carbonXUsbConfigGetUtmiDataBusWidth(
        const CarbonXUsbConfig *config)
{
    return config->getUtmiDataBusWidth();
}

void carbonXUsbConfigSetUtmiPinLevel(CarbonXUsbConfig *config,
        CarbonXUsbUtmiPinLevel utmiPinLevel)
{
    config->setUtmiPinLevel(utmiPinLevel);
}

CarbonXUsbUtmiPinLevel carbonXUsbConfigGetUtmiPinLevel(
        const CarbonXUsbConfig *config)
{
    return config->getUtmiPinLevel();
}

void carbonXUsbConfigSetDeviceDescriptor(CarbonXUsbConfig *config,
        CarbonUInt8* descriptor)
{
    config->setDeviceDescriptor(descriptor);
}

void carbonXUsbConfigSetOtgDescriptor(CarbonXUsbConfig *config,
        CarbonUInt8* descriptor)
{
    config->setOtgDescriptor(descriptor);
}

void carbonXUsbConfigAddConfigDescriptor(CarbonXUsbConfig *config,
        CarbonUInt8* descriptor)
{
    config->addConfigDescriptor(descriptor);
}

void carbonXUsbConfigSetTimingFactor(CarbonXUsbConfig *config,
        CarbonUInt32 factor)
{
    config->setTimingFactor(factor);
}

void carbonXUsbConfigSetMiniAOtgPhyPlug(CarbonXUsbConfig *config)
{
    config->setMiniAPhyPlug();
}

void carbonXUsbConfigSetOtgSrpDetectionSupport(CarbonXUsbConfig *config,
        CarbonXUsbOtgSrpDetectionSupport srpSupport)
{
    config->setOtgSrpDetectionSupport(srpSupport);
}

// When device is used as hub, then device will be renumerated with new device
// address, though a new set of host pipes will be created for the same device
// pipes. So do not push simultaneous transfers from host to pipes having same
// endpoint number, as interleaving transactions might create problem with
// USB protocol as all they are targetted to same device pipe
extern "C" void carbonXUsbConfigSetDeviceAsHub(CarbonXUsbConfig *config)
{
    config->setDeviceAsHub();
}
