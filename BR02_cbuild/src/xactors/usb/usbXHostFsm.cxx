/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
    File:    usbXHostFsm.cpp
    Purpose: This file provides implementation for device FSM.
*/

#include "usbXHostFsm.h"
#include "usbXTrans.h"
#include "usbXtor.h"

// #define DEBUG

UsbxHostFsm::UsbxHostFsm(UsbxHostXtor *xtor): UsbxFsm(), _xtor(xtor),
    _state(_xtor->getConfig()->getIntfSide() == CarbonXUsbIntfSide_PHY ?
            UsbxHostState_Reset : UsbxHostState_NoDeviceAttached),
    _currentTransUnit(NULL), _timeWaited(0), _handshakePacket(NULL)
{
    if (_xtor->isHost() && _state == UsbxHostState_Reset)
        _xtor->getInterface()->startTiming(UsbxTiming_DeviceAttach);
}

UsbxHostFsm::~UsbxHostFsm()
{
    delete _currentTransUnit;
    delete _handshakePacket;
}

UsbxBool UsbxHostFsm::evaluate(CarbonTime currentTime)
{
    UsbxBool retStatus = UsbxTrue;

    // Get commonly used variables
    UsbxIntf *interface = _xtor->getInterface();
    ASSERT(interface, _xtor);
    UsbxBool isFS = _xtor->isFS();
    UInt busWidth = _xtor->getConfig()->getUtmiDataBusWidth();
    UsbxPacket *packet = NULL;

#ifdef DEBUG
    UsbxHostState lastState = _state;
#endif

    // State transition for device detachment from IDLE state onwards
    if (_state >= UsbxHostState_IDLE &&
            interface->senseTiming(UsbxTiming_DeviceDetach))
    {
        // Halt the state machine
        _state = UsbxHostState_NoDeviceAttached;
        // Do not delete _currentTransUnit
        _currentTransUnit = NULL;
        _handshakePacket = NULL;

        // Reset the transactor
        _xtor->resetForDetach();

        // Call reportCommand(DeviceDetach) at host
        _xtor->reportCommand(CarbonXUsbCommand_DeviceDetach);
    }

    // Evaluate the scheduler
    _xtor->getScheduler()->evaluate(currentTime);

    // Host Finite State Machine
    switch (_state)
    {
        case UsbxHostState_NoDeviceAttached:
        {
            // State transition for device attachment
            if (interface->senseTiming(UsbxTiming_DeviceAttach))
            {
                _xtor->setSpeedFS(UsbxTrue, currentTime);
                _state = UsbxHostState_Reset;

                // Call reportCommand(DeviceAttach) at host
                _xtor->reportCommand(CarbonXUsbCommand_DeviceAttach);
            }
            break;
        }
        case UsbxHostState_Reset:
        {
            if (!interface->timingComplete())
                break;
            // Start reset and HS detection timing
            if (_xtor->getConfig()->getSpeed() == CarbonXUsbSpeed_FS)
                interface->startTiming(UsbxTiming_HostFsReset);
            else
                interface->startTiming(UsbxTiming_HostHsReset);
            _state = UsbxHostState_HsDetect;
            break;
        }
        case UsbxHostState_HsDetect:
        {
            if (!interface->timingComplete())
                break;
            // If HS detected, set current operating speed to HS
            _xtor->setSpeedFS(!interface->senseTiming(UsbxTiming_HsDetected),
                    currentTime);
            _state = UsbxHostState_IDLE;

            // Start the scheduler
            _xtor->getScheduler()->startScheduler();

            break;
        }
        case UsbxHostState_Wait_Suspend:
        {
            if (interface->timingComplete())
            {
                if (_driveTiming == UsbxDriveTiming_Suspend)
                {
                    _state = UsbxHostState_Suspend;
#ifdef _DEBUG
                    printf("DEBUG[%d]: HOST FSM: Suspending\n",
                        static_cast<UInt>(currentTime));
#endif
                    // Call reportCommand(Suspend) at host
                    _xtor->reportCommand(CarbonXUsbCommand_Suspend);
                }
                else if (_driveTiming == UsbxDriveTiming_Resume)
                {
                    // Start the scheduler
                    _xtor->getScheduler()->startScheduler();
                    _state = UsbxHostState_IDLE;
#ifdef _DEBUG
                    printf("DEBUG[%d]: HOST FSM: Resuming\n",
                            static_cast<UInt>(currentTime));
#endif
                    // Call reportCommand(Resume) at host
                    _xtor->reportCommand(CarbonXUsbCommand_Resume);
                }
                _driveTiming = UsbxDriveTiming_None;
            }
            else if (interface->senseTiming(UsbxTiming_RemoteWakeUp) &&
                    _driveTiming == UsbxDriveTiming_Suspend)
            {
                interface->startTiming(UsbxTiming_Resume);
                _driveTiming = UsbxDriveTiming_Resume;
            }
            break;
        }
        case UsbxHostState_Suspend:
        {
            if ((_driveTiming == UsbxDriveTiming_Resume) ||
                interface->senseTiming(UsbxTiming_RemoteWakeUp))
            {
                interface->startTiming(UsbxTiming_Resume);
                _state = UsbxHostState_Wait_Suspend;
                _driveTiming = UsbxDriveTiming_Resume;
            }
            break;
        }
        case UsbxHostState_IDLE:
        {
            // Check if a packet transmission is still going on
            if (!interface->transmitComplete())
                break;
            delete _currentTransUnit;
            _currentTransUnit = NULL;

            // State transition for device detachment
            if (interface->senseTiming(UsbxTiming_DeviceDetach))
            {
                _xtor->initAddressTable(); // Free up all device addresses
                _state = UsbxHostState_NoDeviceAttached;

                // Stop the scheduler
                _xtor->getScheduler()->stopScheduler();
                break;
            }

            // Check for driving suspend/resume timing
            if (_driveTiming == UsbxDriveTiming_Suspend)
            {
                // Stop the scheduler
                _xtor->getScheduler()->stopScheduler();
                // Push asynchronous timing for suspend
                interface->startTiming(UsbxTiming_Suspend);
                // Go to wait state while Device suspends
                _state = UsbxHostState_Wait_Suspend;
                break;
            }

            // Initialize state related variables
            _timeWaited = 0;

            // Schedule new pipe
            UsbxPipe *pipe = _xtor->getScheduler()->schedulePipe(currentTime);

            // Check if any transaction is pending
            if (pipe && pipe->getTransQueue()->getCount() > 0)
            {
                // Get pending transaction unit in pipe
                UsbxTrans *trans = pipe->getTransQueue()->top();
                _currentTransUnit = trans->popTransUnitForRetry();
                if (_currentTransUnit == NULL)
                    _currentTransUnit = trans->popNextTransUnit();

                // Get type and direction of transfer
                CarbonXUsbPipeType pipeType = pipe->getType();
                CarbonXUsbPipeDirection dir = _currentTransUnit->getDirection();

                // Change the state
                if (dir == CarbonXUsbPipeDirection_INPUT)
                {
                    if (pipeType == CarbonXUsbPipe_ISO)
                        // Change state to HC_Do_IsochI
                        _state = UsbxHostState_Do_IsochI_IDotoken;
                    else
                        // Change state to HC_Do_BCINTI
                        _state = UsbxHostState_Do_BCINTI_Do_token;
                }
                else // if (CarbonXUsbPipeDirection_OUTPUT)
                {
                    if (pipeType == CarbonXUsbPipe_ISO)
                        // Change state to HC_Do_IsochO
                        _state = UsbxHostState_Do_IsochO_IDotoken;
#ifndef _NO_HS_PING
                    else if (!isFS && trans->sendPing()) // Ping of HC_HS_BCO
                        // Change state to HC_HS_BCO_Ping
                        _state = UsbxHostState_HS_BCO_Ping;
#endif
                    else
                        // Change state to HC_Do_BCINTO
                        _state = UsbxHostState_Do_BCINTO_Do_token;
                    // Note, HC_HS_BCO is integrated within HC_Do_BCINTO
                    // under condition HS AND (pipeType == CONTROL || BULK)
                }
            }
            break;
        }
        //************ HC_Do_BCINTO ************
        case UsbxHostState_Do_BCINTO_Do_token:
        {
            // Wait for the interface to complete packet transmission
            if (!interface->transmitComplete())
                break;

#ifdef DEBUG
            printf("DEBUG[%d]: HOST FSM: BCINTO FSM started.\n",
                    static_cast<UInt>(currentTime));
#endif

            // Send token packet over interface
            packet = _currentTransUnit->getTokenPacket();
            interface->transmitPacket(packet);

            // Change state
            _state = UsbxHostState_Do_BCINTO_Do_data;
            break;
        }
        case UsbxHostState_Do_BCINTO_Do_data:
        {
            // Wait for the interface to complete packet transmission
            if (!interface->transmitComplete())
                break;

            // Send data packet over interface
            packet = _currentTransUnit->getDataPacket();
            interface->transmitPacket(packet);

            // Change state
            _state = UsbxHostState_Do_BCINTO_Wait_resp;
            break;
        }
        case UsbxHostState_Do_BCINTO_Wait_resp:
        {
            // Wait for the interface to complete packet transmission
            if (!interface->transmitComplete())
                break;

            // Wait for the interface to receive a packet
            packet = interface->receivePacket();
            if (packet == NULL ||
                    packet->hasReceiveError() != UsbxPacketReceiveError_None)
            {
                // Timeout for receive
                if (!interface->receiving(currentTime))
                {
                    if (_timeWaited++ >= getBusTurnaroundTime(isFS, busWidth))
                        // Change state
                        _state = UsbxHostState_Do_BCINTO_BCI_error;
                }
                delete packet;
                break;
            }

            switch (packet->getType())
            {
                case UsbxPacket_NYET:
                {
                    if (!isFS) // Enable ping based flow control
                        _currentTransUnit->getTrans()->setPingFc(UsbxTrue);
                    // No break to be provided
                }
                case UsbxPacket_ACK:
                {
                    if (!_currentTransUnit->isLast() &&
                            _currentTransUnit->hasRetryState())
                    {
                        // Retry considering same data toggle
                        _currentTransUnit->incrRetryCount();
                        updateForSameTransUnit(currentTime);
                    }
                    else
                        updateForNextTransUnit(currentTime);
                    // Change state
                    _state = UsbxHostState_IDLE;
                    break;
                }
                case UsbxPacket_NAK:
                {
                    if (!isFS) // Enable ping based flow control
                        _currentTransUnit->getTrans()->setPingFc(UsbxTrue);
                    updateForSameTransUnit(currentTime);
                    // Change state
                    _state = UsbxHostState_IDLE;
                    break;
                }
                case UsbxPacket_STALL:
                {
                    updateForHalt(currentTime);
                    // Change state
                    _state = UsbxHostState_IDLE;
                    break;
                }
                default:
                {
                    // Change state
                    _state = UsbxHostState_Do_BCINTO_BCI_error;
                    break;
                }
            }
            delete packet;
            break;
        }
        case UsbxHostState_Do_BCINTO_BCI_error:
        {
#ifdef DEBUG
            printf("DEBUG[%d]: HOST FSM: BCINTO TIMEOUT.\n",
                    static_cast<UInt>(currentTime));
#endif
            if (_currentTransUnit->incrErrorCount() < 3)
                updateForSameTransUnit(currentTime);
            else // Halt after 3 retries
                updateForHalt(currentTime);
            // Change state
            _state = UsbxHostState_IDLE;
            break;
        }
        // Ping handling for HC_HS_BCO
        case UsbxHostState_HS_BCO_Ping:
        {
            UsbxPipe *pipe = _currentTransUnit->getTrans()->getPipe();
            interface->transmitPacket(_handshakePacket =
                    new UsbxPingPacket(pipe->getDeviceAddress(),
                            pipe->getEpNumber()));
            _state = UsbxHostState_HS_BCO_Ping_Wait_resp;
            break;
        }
        case UsbxHostState_HS_BCO_Ping_Wait_resp:
        {
            // Wait for the interface to complete packet transmission
            if (!interface->transmitComplete())
                break;
            delete _handshakePacket;
            _handshakePacket = NULL;

            // Wait for the interface to receive a packet
            packet = interface->receivePacket();
            if (packet == NULL ||
                    packet->hasReceiveError() != UsbxPacketReceiveError_None)
            {
                // Timeout for receive
                if (!interface->receiving(currentTime))
                {
                    if (_timeWaited++ >= getBusTurnaroundTime(isFS, busWidth))
                    {
                        updateForSameTransUnit(currentTime);
                        // Change state
                        _state = UsbxHostState_IDLE;
                    }
                }
                delete packet;
                break;
            }

            switch (packet->getType())
            {
                case UsbxPacket_ACK:
                {
                    if (!isFS) // Disable ping based flow control
                        _currentTransUnit->getTrans()->setPingFc(UsbxFalse);
                    updateForSameTransUnit(currentTime);
                    // Change state
                    _state = UsbxHostState_IDLE;
                    break;
                }
                case UsbxPacket_NYET:
                case UsbxPacket_NAK:
                {
                    updateForSameTransUnit(currentTime);
                    // Change state
                    _state = UsbxHostState_IDLE;
                    break;
                }
                case UsbxPacket_STALL:
                {
                    updateForHalt(currentTime);
                    // Change state
                    _state = UsbxHostState_IDLE;
                    break;
                }
                default:
                {
                    // Change state
                    _state = UsbxHostState_Do_BCINTO_BCI_error;
                    break;
                }
            }
            delete packet;
            break;
        }
        //************ HC_Do_BCINTI ************
        case UsbxHostState_Do_BCINTI_Do_token:
        {
            // Wait for the interface to complete packet transmission
            if (!interface->transmitComplete())
                break;

#ifdef DEBUG
            printf("DEBUG[%d]: HOST FSM: BCINTI FSM started.\n",
                    static_cast<UInt>(currentTime));
#endif

            // Send token packet over interface
            packet = _currentTransUnit->getTokenPacket();
            interface->transmitPacket(packet);

            // Change state
            _state = UsbxHostState_Do_BCINTI_Wait_data;
            break;
        }
        case UsbxHostState_Do_BCINTI_Wait_data:
        {
            // Wait for the interface to complete packet transmission
            if (!interface->transmitComplete())
                break;

            // Wait for the interface to receive a packet
            packet = interface->receivePacket();
            if (packet == NULL ||
                    packet->hasReceiveError() != UsbxPacketReceiveError_None)
            {
                // Timeout for receive
                if (!interface->receiving(currentTime))
                {
                    if (_timeWaited++ >= getBusTurnaroundTime(isFS, busWidth))
                        // Change state
                        _state = UsbxHostState_Do_BCINTI_BCII_error;
                }
                delete packet;
                break;
            }

            // Change state for different packets received
            switch (packet->getType())
            {
                case UsbxPacket_STALL:
                {
                    delete packet;
                    // Halt the transfer
                    updateForHalt(currentTime);
                    // Change state
                    _state = UsbxHostState_IDLE;
                    break;
                }
                case UsbxPacket_NAK:
                {
                    delete packet;
                    // Update the pipe with current transaction unit for retry
                    updateForSameTransUnit(currentTime);
                    // Change state
                    _state = UsbxHostState_IDLE;
                    break;
                }
                case UsbxPacket_DATA0:
                case UsbxPacket_DATA1:
                {
                    // Check for retry state
                    UsbxBool retryState = _currentTransUnit->hasRetryState();
                    if (retryState)
                        _currentTransUnit->incrRetryCount();

                    if (_currentTransUnit->getTrans()->getPipe()
                            ->getNextDataPacketType() == packet->getType())
                    {
                        if (_currentTransUnit->getDataPacket())
                            delete _currentTransUnit->getDataPacket();
                        // Accept data - set data packet in transaction unit
                        // and push that unit to transaction structure
                        _currentTransUnit->setDataPacket(static_cast<
                                UsbxDataPacket*>(packet));
                        _currentTransUnit->getTrans()
                            ->pushNextTransUnit(_currentTransUnit);
                        // Complete this transaction unit
                        updateForNextTransUnit(currentTime);
                    }
                    else
                    {
                        delete packet;
                        // Update the pipe for retry
                        updateForSameTransUnit(currentTime);
                    }
                    // Force ACK corruption to retry
                    if (retryState && 0)
                    {
                        _state = UsbxHostState_IDLE;
                        break;
                    }

                    interface->transmitPacket(
                            _handshakePacket = new UsbxAckPacket());
                    _state = UsbxHostState_Do_BCINTI_Dopkt;
                    break;
                }
                default:
                {
                    delete packet;
                    _state = UsbxHostState_Do_BCINTI_BCII_error;
                }
            }
            break;
        }
        case UsbxHostState_Do_BCINTI_Dopkt:
        {
            // Wait for the interface to complete packet transmission
            if (!interface->transmitComplete())
                break;
            delete _handshakePacket;
            _handshakePacket = NULL;

            // Change state
            _state = UsbxHostState_IDLE;
            break;
        }
        case UsbxHostState_Do_BCINTI_BCII_error:
        {
#ifdef DEBUG
            printf("DEBUG[%d]: HOST FSM: BCINTI TIMEOUT.\n",
                    static_cast<UInt>(currentTime));
#endif
            if (_currentTransUnit->incrErrorCount() < 3)
                updateForSameTransUnit(currentTime);
            else // Halt after 3 retries
                updateForHalt(currentTime);
            // Change state
            _state = UsbxHostState_IDLE;
            break;
        }
        //************ HC_Do_IsochO ************
        case UsbxHostState_Do_IsochO_IDotoken:
        {
            // Wait for the interface to complete packet transmission
            if (!interface->transmitComplete())
                break;

#ifdef DEBUG
            printf("DEBUG[%d]: HOST FSM: IsochO FSM started.\n",
                    static_cast<UInt>(currentTime));
#endif

            // Send token packet over interface
            packet = _currentTransUnit->getTokenPacket();
            interface->transmitPacket(packet);

            // Change state
            _state = UsbxHostState_Do_IsochO_IDodata;
            break;
        }
        case UsbxHostState_Do_IsochO_IDodata:
        {
            // Wait for the interface to complete packet transmission
            if (!interface->transmitComplete())
                break;

            // Send data packet over interface
            packet = _currentTransUnit->getDataPacket();
            interface->transmitPacket(packet);

            // Go for next packet
            updateForNextTransUnit(currentTime);

            // Change state
            _state = UsbxHostState_IDLE;
            break;
        }
        //************ HC_Do_IsochI ************
        case UsbxHostState_Do_IsochI_IDotoken:
        {
            // Wait for the interface to complete packet transmission
            if (!interface->transmitComplete())
                break;

#ifdef DEBUG
            printf("DEBUG[%d]: HOST FSM: IsochI FSM started.\n",
                    static_cast<UInt>(currentTime));
#endif

            // Send token packet over interface
            packet = _currentTransUnit->getTokenPacket();
            interface->transmitPacket(packet);

            // Change state
            _state = UsbxHostState_Do_IsochI_Wait_resp;
            break;
        }
        case UsbxHostState_Do_IsochI_Wait_resp:
        {
            // Wait for the interface to complete packet transmission
            if (!interface->transmitComplete())
                break;

            // Wait for the interface to receive a packet
            packet = interface->receivePacket();
            if (packet == NULL ||
                    packet->hasReceiveError() != UsbxPacketReceiveError_None)
            {
                // Timeout for receive
                if (!interface->receiving(currentTime))
                {
                    if (_timeWaited++ >= getBusTurnaroundTime(isFS, busWidth))
                        // Change state
                        _state = UsbxHostState_IDLE;
                }
                delete packet;
                break;
            }

            // Change state for different packets received
            switch (packet->getType())
            {
                case UsbxPacket_DATA0:
                case UsbxPacket_DATA1:
                case UsbxPacket_DATA2:
                case UsbxPacket_MDATA:
                {
                    // For HS periodic having repeat count,
                    // check for data packet type
                    if (_currentTransUnit->getTrans()
                            ->setDataPacketTypeForHsRepeat())
                    {
                        UsbxPacketType expDataPacketType = _currentTransUnit
                            ->getTrans()->getPipe()->getNextDataPacketType();
                        if (packet->getType() != expDataPacketType &&
                                // If smaller packet is coming
                                (packet->getType() == UsbxPacket_DATA2 ||
                                 expDataPacketType == UsbxPacket_DATA0))
                        {
                            delete packet;
                            break;
                        }
                        else
                            _currentTransUnit->getTrans()->getPipe()
                                ->setNextDataPacketType(packet->getType());
                    }

                    if (_currentTransUnit->getDataPacket())
                        delete _currentTransUnit->getDataPacket();
                    // Accept data - set data packet in transaction unit
                    // and push that unit to transaction structure
                    _currentTransUnit->setDataPacket(static_cast<
                            UsbxDataPacket*>(packet));
                    _currentTransUnit->getTrans()
                        ->pushNextTransUnit(_currentTransUnit);
                    // Complete this transaction unit
                    updateForNextTransUnit(currentTime);

                    break;
                }
                default:
                {
                    delete packet;
                }
            }
            _state = UsbxHostState_IDLE;
            break;
        }
        default: ASSERT(0, _xtor);
    }

#ifdef DEBUG
    if (_state != lastState)
        printf("DEBUG[%d]: HOST FSM: State: %d\n",
                static_cast<UInt>(currentTime), _state);
#endif
    return retStatus;
}

// Function for RespondHC(Do_next_cmd): This function will be called
// for successful transfer of a transaction unit
void UsbxHostFsm::updateForNextTransUnit(CarbonTime currentTime)
{
#ifdef DEBUG
    printf("DEBUG[%d]: HOST FSM: Current transaction unit complete.\n",
            static_cast<UInt>(currentTime));
#endif

    if (_currentTransUnit->isLast()) // For last unit of transfer
    {
        // This means success status
        _currentTransUnit->getTrans()->setStatus(CarbonXUsbStatus_SUCCESS);
        // Pop from queue and call reportTransaction() callback
        _currentTransUnit->getTrans()->getPipe()->getTransQueue()->pop();

        // Callbacks
        UsbxPipe *pipe = _currentTransUnit->getTrans()->getPipe();
        if (pipe->isPeriodic() &&
                pipe->getDirection() == CarbonXUsbPipeDirection_INPUT)
        // Transactor generated
        {
            // Call setResponse at host
            _xtor->setResponse(_currentTransUnit->getTrans());
            delete _currentTransUnit->getTrans();
        }
        else
            // Call reportTransaction at host
            _xtor->reportTransaction(_currentTransUnit->getTrans());
    }

    // Do not make _currentTransUnit NULL here

    currentTime = 0; // To remove warning
}

// Function for RespondHC(Do_same_cmd): This function will be called
// when a transaction unit has to be retried - the current transaction
// will be pushed to pipe, so that it can be poped for retry in future
void UsbxHostFsm::updateForSameTransUnit(CarbonTime currentTime)
{
#ifdef DEBUG
    printf("DEBUG[%d]: HOST FSM: Transaction unit will be retried.\n",
            static_cast<UInt>(currentTime));
#endif

    // Push the current transaction unit to the pipe, so that it can be poped
    // in future for retry
    _currentTransUnit->getTrans()->pushTransUnitForRetry(_currentTransUnit);
    _currentTransUnit = NULL; // This is essential

    currentTime = 0; // To remove warning
}

// Function for RespondHC(Do_halt): This function will be called
// when an entire transfer is to be stalled
void UsbxHostFsm::updateForHalt(CarbonTime currentTime)
{
#ifdef DEBUG
    printf("DEBUG[%d]: HOST FSM: Transaction halted.\n",
            static_cast<UInt>(currentTime));
#endif

    // Stop transfer and call reportTransaction() callback
    _currentTransUnit->getTrans()->setStatus(CarbonXUsbStatus_HALT);
    // Pop from queue and call reportTransaction() callback
    _currentTransUnit->getTrans()->getPipe()->getTransQueue()->pop();
    // Create receive data till halt for input transfers
    _currentTransUnit->getTrans()->createReceiveData();
    _xtor->reportTransaction(_currentTransUnit->getTrans());

    // Do not make _currentTransUnit NULL here

    currentTime = 0; // To remove warning
}

UsbxBool UsbxHostFsm::suspend()
{
    if ((_driveTiming != UsbxDriveTiming_None) ||
            (_state == UsbxHostState_Suspend))
        return UsbxFalse;
    _driveTiming = UsbxDriveTiming_Suspend;
    return UsbxTrue;
}

UsbxBool UsbxHostFsm::resume()
{
    if ((_driveTiming != UsbxDriveTiming_None) ||
            (_state != UsbxHostState_Suspend))
        return UsbxFalse;
    _driveTiming = UsbxDriveTiming_Resume;
    return UsbxTrue;
}

void UsbxHostFsm::resetForDetach()
{
    _state = _xtor->getConfig()->getIntfSide() == CarbonXUsbIntfSide_PHY ?
        UsbxHostState_Reset : UsbxHostState_NoDeviceAttached;
    _currentTransUnit = NULL; _timeWaited = 0; _handshakePacket = NULL;
    if (_xtor->isHost() && _state == UsbxHostState_Reset)
        _xtor->getInterface()->startTiming(UsbxTiming_DeviceAttach);
}

void UsbxHostFsm::print(CarbonTime currentTime) const
{
    printf("Host FSM %s transactor:\n", _xtor->getName());
    printf("Current time: %d\n", static_cast<UInt>(currentTime));
    printf("State number: %d\n", _state);
    if (_currentTransUnit)
    {
        printf("Current ");
        _currentTransUnit->print();
    }
    return;
}

