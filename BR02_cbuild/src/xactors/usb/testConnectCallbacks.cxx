//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

/**** Connect Callbacks ****/
#include "xactors/carbonX.h"

#ifndef _ULPI // For UTMI -- default
/* Connections Net Objects */
CarbonUInt32 net_Xtor_Reset       = 0;
CarbonUInt32 net_Xtor_DataBus16_8 = 0;
CarbonUInt32 net_Xtor_SuspendM    = 0;
CarbonUInt32 net_Xtor_DataInLo    = 0;
CarbonUInt32 net_Xtor_DataInHi    = 0;
CarbonUInt32 net_Xtor_TXValid     = 0;
CarbonUInt32 net_Xtor_TXValidH    = 0;
CarbonUInt32 net_Xtor_TXReady     = 0;
CarbonUInt32 net_Xtor_DataOutLo   = 0;
CarbonUInt32 net_Xtor_DataOutHi   = 0;
CarbonUInt32 net_Xtor_RXValid     = 0;
CarbonUInt32 net_Xtor_RXValidH    = 0;
CarbonUInt32 net_Xtor_RXActive    = 0;
CarbonUInt32 net_Xtor_RXError     = 0;
CarbonUInt32 net_Xtor_XcvrSelect  = 0;
CarbonUInt32 net_Xtor_TermSelect  = 0;
CarbonUInt32 net_Xtor_OpMode      = 0;
CarbonUInt32 net_Xtor_LineState   = 0;

#else // For ULPI
/* Connections Net Objects */
CarbonUInt32 net_Xtor_dir       = 0;
CarbonUInt32 net_Xtor_nxt       = 0;
CarbonUInt32 net_Xtor_stp       = 0;
CarbonUInt32 net_Xtor_data      = 0;
#endif

/* Model callback definitions */
CarbonStatus ModelInputChange(CarbonUInt32 *value, void *netObject)
{
    if (value == NULL || netObject == NULL)
        return eCarbon_ERROR;
    *((CarbonUInt32*) netObject) = *value;
    return eCarbon_OK;
}

CarbonStatus ModelOutputChange(CarbonUInt32 *value, void *netObject)
{
    if (value == NULL || netObject == NULL)
        return eCarbon_ERROR;
    *value = *((CarbonUInt32*) netObject);
    return eCarbon_OK;
}

CarbonXInterconnectNameCallbackTrio createCbTrio(const char * signalName,
        void *callbackContext)
{
    CarbonXInterconnectNameCallbackTrio trio;
    trio.TransactorSignalName = signalName;
    trio.ModelInputChangeCB = ModelInputChange;
    trio.ModelOutputChangeCB = ModelOutputChange;
    trio.UserPointer = callbackContext;
    return trio;
}

#define BIND(pinName, net) getPin(pinName)->bind(createCbTrio(pinName, net))

const CarbonXSignalChangeCB ModelInputChangeCB = &ModelInputChange;
const CarbonXSignalChangeCB ModelOutputChangeCB = &ModelOutputChange;

#ifndef _ULPI
/* Interface Connections */
CarbonUInt32 *Link_Reset       = &net_Xtor_Reset;
CarbonUInt32 *Link_DataBus16_8 = &net_Xtor_DataBus16_8;
CarbonUInt32 *Link_SuspendM    = &net_Xtor_SuspendM;
CarbonUInt32 *Link_DataInLo    = &net_Xtor_DataInLo;
CarbonUInt32 *Link_DataInHi    = &net_Xtor_DataInHi;
CarbonUInt32 *Link_TXValid     = &net_Xtor_TXValid;
CarbonUInt32 *Link_TXValidH    = &net_Xtor_TXValidH;
CarbonUInt32 *Link_TXReady     = &net_Xtor_TXReady;
CarbonUInt32 *Link_DataOutLo   = &net_Xtor_DataOutLo;
CarbonUInt32 *Link_DataOutHi   = &net_Xtor_DataOutHi;
CarbonUInt32 *Link_RXValid     = &net_Xtor_RXValid;
CarbonUInt32 *Link_RXValidH    = &net_Xtor_RXValidH;
CarbonUInt32 *Link_RXActive    = &net_Xtor_RXActive;
CarbonUInt32 *Link_RXError     = &net_Xtor_RXError;
CarbonUInt32 *Link_XcvrSelect  = &net_Xtor_XcvrSelect;
CarbonUInt32 *Link_TermSelect  = &net_Xtor_TermSelect;
CarbonUInt32 *Link_OpMode      = &net_Xtor_OpMode;
CarbonUInt32 *Link_LineState   = &net_Xtor_LineState;

CarbonUInt32 *Phy_Reset        = &net_Xtor_Reset;
CarbonUInt32 *Phy_DataBus16_8  = &net_Xtor_DataBus16_8;
CarbonUInt32 *Phy_SuspendM     = &net_Xtor_SuspendM;
CarbonUInt32 *Phy_DataInLo     = &net_Xtor_DataInLo;
CarbonUInt32 *Phy_DataInHi     = &net_Xtor_DataInHi;
CarbonUInt32 *Phy_TXValid      = &net_Xtor_TXValid;
CarbonUInt32 *Phy_TXValidH     = &net_Xtor_TXValidH;
CarbonUInt32 *Phy_TXReady      = &net_Xtor_TXReady;
CarbonUInt32 *Phy_DataOutLo    = &net_Xtor_DataOutLo;
CarbonUInt32 *Phy_DataOutHi    = &net_Xtor_DataOutHi;
CarbonUInt32 *Phy_RXValid      = &net_Xtor_RXValid;
CarbonUInt32 *Phy_RXValidH     = &net_Xtor_RXValidH;
CarbonUInt32 *Phy_RXActive     = &net_Xtor_RXActive;
CarbonUInt32 *Phy_RXError      = &net_Xtor_RXError;
CarbonUInt32 *Phy_XcvrSelect   = &net_Xtor_XcvrSelect;
CarbonUInt32 *Phy_TermSelect   = &net_Xtor_TermSelect;
CarbonUInt32 *Phy_OpMode       = &net_Xtor_OpMode;
CarbonUInt32 *Phy_LineState    = &net_Xtor_LineState;

#else
CarbonUInt32 *Link_dir      = &net_Xtor_dir;
CarbonUInt32 *Link_nxt      = &net_Xtor_nxt;
CarbonUInt32 *Link_stp      = &net_Xtor_stp;
CarbonUInt32 *Link_data     = &net_Xtor_data;

CarbonUInt32 *Phy_dir      = &net_Xtor_dir;
CarbonUInt32 *Phy_nxt      = &net_Xtor_nxt;
CarbonUInt32 *Phy_stp      = &net_Xtor_stp;
CarbonUInt32 *Phy_data     = &net_Xtor_data;
#endif
