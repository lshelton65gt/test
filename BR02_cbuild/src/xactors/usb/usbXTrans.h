/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef USBX_TRANS_H
#define USBX_TRANS_H

#include "usbXPipe.h"
#include "usbXTransUnit.h"
#include "xactors/usb/carbonXUsbTrans.h"

/*!
 * \file usbXTrans.h
 * \brief Header file for transaction classes.
 */

/*!
 * \brief Base class to represent transaction object.
 */
class CarbonXUsbTransClass
{
public: CARBONMEM_OVERRIDES

protected:

    // Transaction information
    UsbxPipe *_pipe;                        //!< Pipe ID

    // Data information
    CarbonUInt32 _dataSize;                 //!< Data size provided by user
    CarbonUInt8 *_data;                     //!< Data byte array
    UInt _nextDataIndex;                    //!< Next data index to be sent in
                                            //   next FSM transaction unit

    // Status information
    CarbonXUsbStatusType _status;           //!< Transaction status

    // Retry information
    CarbonUInt32 _retrySize;                //!< Retry size provided by user
    CarbonXUsbTransRetry *_retryStates;     //!< Retry array
    UsbxBool _sendPing;                     //!< Ping based flow control flag

    // Stall information
    CarbonUInt32 _stallIndex;               //!< Stall packet Index

    // FSM related
    UsbxTransUnit *_currentTransUnit;       //!< Transaction unit under process
    UsbxQueue<UsbxDataPacket> *_receiveDataQueue; //!< Receive data queue
    UInt _transUnitIndex;                   //!< Transaction unit index

public:

    // Constructor and destructor
    CarbonXUsbTransClass(UsbxPipe *pipe);
    virtual ~CarbonXUsbTransClass();

    // Get type
    CarbonXUsbPipeType getType() const { return _pipe->getType(); }

    // Member access functions
    UsbxPipe *getPipe() { return _pipe; }

    // Access functions for data, memory is copied
    UsbxBool setData(CarbonUInt32 dataSize, const CarbonUInt8 *data);
    CarbonUInt32 getDataSize() const { return _dataSize; }
    const CarbonUInt8 *getData() const { return _data; }

    // Access functions for retry ans stall, memory is copied
    UsbxBool setRetryStates(CarbonUInt32 retrySize,
            const CarbonXUsbTransRetry *retryStates);
    const CarbonXUsbTransRetry *hasRetry(UInt transUnitIndex) const;
    void setPingFc(UsbxBool isOn) { _sendPing = isOn; }
    UsbxBool sendPing() const { return _sendPing; }
    UsbxBool setStallState(CarbonUInt32 stallIndex)
    { _stallIndex = stallIndex; return UsbxTrue; }
    UsbxBool hasStall(UInt transUnitIndex) const
    { return transUnitIndex == _stallIndex; }

    // Access functions for status
    UsbxBool setStatus(CarbonXUsbStatusType status)
    { _status = status; return UsbxTrue; }
    CarbonXUsbStatusType getStatus() const { return _status; }

    // Debugging function
    void print() const;
    virtual void printFields() const = 0;

    // FSM related functions
    virtual UsbxTransUnit *popNextTransUnit() = 0;
    virtual UsbxBool pushNextTransUnit(UsbxTransUnit *transUnit) = 0;
    UInt getNextTransUnitIndex() const { return _transUnitIndex; }

protected:

    // FSM related functions
    UsbxDataPacket *popDataForTransUnit();
    UsbxBool pushDataForTransUnit(UsbxDataPacket *dataPacket);

public:

    UsbxBool setDataPacketTypeForHsRepeat();

    // FSM related
    UsbxBool createReceiveData();
    void pushTransUnitForRetry(UsbxTransUnit *transUnit)
    { _currentTransUnit = transUnit; }
    UsbxTransUnit *popTransUnitForRetry();

    // Scheduler related functions
    virtual UInt getNextTransUnitSizeInBytes() const = 0;
    UsbxBool hasRetried() const { return _currentTransUnit != NULL; }

    // Utility functions
    static CarbonUInt8* copyData(CarbonUInt32 size, const CarbonUInt8 *data);
};

/*!
 * \brief Class to represent Message Transaction i.e. Control Transfers.
 */
class UsbxMessageTrans: public CarbonXUsbTransClass
{
public: CARBONMEM_OVERRIDES

private:

    // Transaction information
    UsbxByte _setupData[8];                 //!< Setup data

    //! Control tranfer stages
    enum UsbxMessageStageType {
        UsbxMessageStage_SETUP,             //!< Setup stage
        UsbxMessageStage_DATA,              //!< Data stage
        UsbxMessageStage_STATUS,            //!< Status stage
    };

    UsbxMessageStageType _nextStage;        //!< Next stage of transaction

public:

    // Constructor and destructor
    UsbxMessageTrans(UsbxPipe *pipe);

    // Member access functions
    void setRequestType(CarbonUInt8 bmRequestType)
    { _setupData[0] = bmRequestType; }
    CarbonUInt8 getRequestType() const { return _setupData[0]; }
    CarbonXUsbPipeDirection getDirection() const
    { return static_cast<CarbonXUsbPipeDirection>(getRequestType() >> 7); }

    void setRequest(CarbonUInt8 bRequest) { _setupData[1] = bRequest; }
    CarbonUInt8 getRequest() const { return _setupData[1]; }

    void setRequestValue(CarbonUInt16 wValue)
    { _setupData[2] = wValue & 0xFF; _setupData[3] = wValue >> 8; }
    CarbonUInt16 getRequestValue() const
    { return _setupData[3] << 8 | _setupData[2]; }

    void setRequestIndex(CarbonUInt16 wIndex)
    { _setupData[4] = wIndex & 0xFF; _setupData[5] = wIndex >> 8; }
    CarbonUInt16 getRequestIndex() const
    { return _setupData[4] << 8 | _setupData[5]; }

    void setRequestLength(CarbonUInt16 wLength)
    { _setupData[6] = wLength & 0xFF; _setupData[7] = wLength >> 8; }
    CarbonUInt16 getRequestLength() const
    { return _setupData[7] << 8 | _setupData[6]; }

    // Debugging function
    void printFields() const;

    // FSM related functions
    UsbxTransUnit *popNextTransUnit();
    UsbxBool pushNextTransUnit(UsbxTransUnit *transUnit);

    // Scheduler related function
    UInt getNextTransUnitSizeInBytes() const;
};

/*!
 * \brief Class to represent Stream Transaction that includes
 *        Bulk, Interrupt and Isochronous Transfers.
 */
class UsbxStreamTrans: public CarbonXUsbTransClass
{
public: CARBONMEM_OVERRIDES

public:

    // Constructor and destructor
    UsbxStreamTrans(UsbxPipe *pipe): CarbonXUsbTransClass(pipe) {}

    // Debugging function
    void printFields() const {}

    // FSM related functions
    UsbxTransUnit *popNextTransUnit();
    UsbxBool pushNextTransUnit(UsbxTransUnit *transUnit);

    // Scheduler related function
    UInt getNextTransUnitSizeInBytes() const;
};

#endif
