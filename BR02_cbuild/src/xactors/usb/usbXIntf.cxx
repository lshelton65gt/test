/*******************************************************************************
  Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
/*
      File    :  usbXIntf.cpp
      Purpose :  Implements generic interface related functions
*/

#include "usbXIntf.h"
#include "usbXtor.h"

const CarbonUInt32 UsbxIntf::_maxTimingFactor = 600;

UsbxPin* UsbxIntf::createPin(UsbXtor *xtor, UsbxConstString pinName,
        const UsbxDirection direction, const CarbonUInt32 size)
{
    // Create pin
    UsbxPin *pin = new UsbxPin(xtor, pinName, direction, size);
    // Check for memory allocation error
    if (pin == NULL)
        xtor->errorForNoMemory(pinName);
    return pin;
}

void UsbxIntf::setTimingFactor(CarbonUInt32 factor)
{
    if (factor < 1)
        _timingFactor = 1;
    else if (factor > _maxTimingFactor)
        _timingFactor = _maxTimingFactor;
    else
        _timingFactor = factor;
}
