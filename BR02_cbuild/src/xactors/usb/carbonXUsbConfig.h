// -*-C++-*-
/*****************************************************************************

 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
#ifndef __carbonXUsbConfig_h_
#define __carbonXUsbConfig_h_

#ifndef __carbonX_h_
#include "xactors/carbonX.h"
#endif

#ifdef __cplusplus
#define STRUCT class
extern "C" {
#else
  /*!
   * \brief Macro to allow both C and C++ compilers to use this include file in
   *        a type-safe manner.
   * \hideinitializer
   */
#define STRUCT struct
#endif

/*!
 * \addtogroup Usb
 * @{
 */

/*!
 * \file xactors/usb/carbonXUsbConfig.h
 * \brief Definition of Carbon USB Configuration Object Routines.
 *
 * User need to create configuration object and set the configuration
 * parameters on the object. User then need to pass the configuration object
 * to the transactor's create API. The memory ownership will remain to
 * the user - transactor will copy the configuration parameters.
 */

/*! Forward declaration */
STRUCT CarbonXUsbConfigClass;

/*!
 * \brief Carbon USB Configuration Object declaration.
 */
typedef STRUCT CarbonXUsbConfigClass CarbonXUsbConfig;

/*!
 * \brief USB Speed types.
 *
 * Following USB speeds are supported:
 *    -# Full speed (1 ms frame)
 *    -# High speed (125 us micro-frame).
 */
typedef enum {
    CarbonXUsbSpeed_HS = 0,/*!< USB High speed (125 us micro-frame) */
    CarbonXUsbSpeed_FS = 1 /*!< USB Full speed (1 ms frame)         */
} CarbonXUsbSpeedType;

/*!
 * \brief Interface types.
 *
 * Following interface are:
 *    -# UTMI (USB Transeceiver Macrocell Interface)
 *    -# ULPI (Universal Low Pin Interface)
 */
typedef enum {
    CarbonXUsbInterface_UTMI,     /*!< UTMI interface */
    CarbonXUsbInterface_ULPI      /*!< ULPI interface */
} CarbonXUsbInterfaceType;

/*!
 * \brief Interface side
 *
 * Interface side could be LINK or PHY.
 */
typedef enum {
    CarbonXUsbIntfSide_PHY,       /*!< PHY side interface */
    CarbonXUsbIntfSide_LINK       /*!< Link side interface */
} CarbonXUsbIntfSide;

/*!
 * \brief UTMI interface data width.
 *
 * UTMI interface data width type 8 or 16 bits.
 */
typedef enum {
    CarbonXUsbUtmiDataBusWidth_8 = 8,  /*!< UTMI interface data width 8 bit  */
    CarbonXUsbUtmiDataBusWidth_16 = 16 /*!< UTMI interface data width 16 bit */
} CarbonXUsbUtmiDataBusWidthType;

/*!
 * \brief UTMI interface pin level.
 *
 * UTMI interface pin level i.e. level 0 or level 1.
 */
typedef enum {
    CarbonXUsbUtmiPinLevel_0 = 0,     /*!< UTMI interface pin level 0       */
    CarbonXUsbUtmiPinLevel_1 = 1      /*!< UTMI interface pin level 1       */
} CarbonXUsbUtmiPinLevel;

/*!
 * \brief SRP Detection Support for OTG A device
 *
 * Session Request Protocol (SRP) detection support
 * at A device for OTG protocol.
 */
typedef enum {
    CarbonXUsbOtgSrpDetection_VbusPulsing,    /*!< VBUS pulsing support      */
    CarbonXUsbOtgSrpDetection_DataLinePulsing /*!< Data line pulsing support */
} CarbonXUsbOtgSrpDetectionSupport;

/*!
 * \brief Creates USB configuration object.
 *
 * \returns Pointer to the created configuration object.
 */
CarbonXUsbConfig *carbonXUsbConfigCreate(void);

/*!
 * \brief Destroys USB configuration object.
 *
 * \param config Pointer to the configuration object to be destroyed.
 *
 * \retval 1 For success.
 * \retval 0 For failure.
 */
CarbonUInt32 carbonXUsbConfigDestroy(CarbonXUsbConfig *config);

/*!
 * \brief Sets USB speed in the configuration object.
 *
 * This API will set speed in the configuration object. Default is FS,
 * if not set.
 *
 * \param config Pointer to the configuration object.
 * \param speed USB speed.
 */
void carbonXUsbConfigSetSpeed(CarbonXUsbConfig *config,
        CarbonXUsbSpeedType speed);

/*!
 * \brief Gets USB speed from a configuration object.
 *
 * This API will return the USB speed from the configuration object.
 *
 * \param config Pointer to the configuration object.
 *
 * \returns USB speed from the configuration object.
 */
CarbonXUsbSpeedType carbonXUsbConfigGetSpeed(const CarbonXUsbConfig *config);

/*!
 * \brief Sets USB interface type in the configuration object.
 *
 * This API will set interface type in the configuration object.
 * Default is UTMI, if not set.
 *
 * \param config Pointer to the configuration object.
 * \param intfType Interface type.
 */
void carbonXUsbConfigSetIntfType(CarbonXUsbConfig *config,
        CarbonXUsbInterfaceType intfType);

/*!
 * \brief Gets USB interface type from a configuration object.
 *
 * This API will return the USB interface type from the configuration object.
 *
 * \param config Pointer to the configuration object.
 *
 * \returns USB interface type from the configuration object.
 */
CarbonXUsbInterfaceType carbonXUsbConfigGetIntfType(
        const CarbonXUsbConfig *config);

/*!
 * \brief Sets USB interface side in the configuration object.
 *
 * This API will set interface side in the configuration object. Default is
 * PHY (physical UTM) side, if not set.
 *
 * \param config Pointer to the configuration object.
 * \param intfSide Interface side.
 */
void carbonXUsbConfigSetIntfSide(CarbonXUsbConfig *config,
        CarbonXUsbIntfSide intfSide);

/*!
 * \brief Gets USB interface side from a configuration object.
 *
 * This API will return the USB interface side from the configuration object.
 *
 * \param config Pointer to the configuration object.
 *
 * \returns USB interface side.
 */
CarbonXUsbIntfSide carbonXUsbConfigGetIntfSide(const CarbonXUsbConfig *config);

/*!
 * \brief Sets USB UTMI data bus width in the configuration object.
 *
 * This API will set UTMI data bus width in the configuration object.
 * Default is 8, if not set. This API is relevant if UTMI interface is used.
 *
 * \param config Pointer to the configuration object.
 * \param busWidth UTMI data bus width.
 */
void carbonXUsbConfigSetUtmiDataBusWidth(CarbonXUsbConfig *config,
        CarbonXUsbUtmiDataBusWidthType busWidth);

/*!
 * \brief Gets USB UTMI data bus width from a configuration object.
 *
 * This API will return the UTMI data bus width from the configuration object.
 * This API is relevant if UTMI interface is used.
 *
 * \param config Pointer to the configuration object.
 *
 * \returns UTMI data bus width from the configuration object.
 */
CarbonXUsbUtmiDataBusWidthType carbonXUsbConfigGetUtmiDataBusWidth(
        const CarbonXUsbConfig *config);

/*!
 * \brief Sets USB UTMI pin level in the configuration object.
 *
 * This API will set UTMI pin level in the configuration object.
 * Default is CarbonXUsbUtmiPinLevel_0, if not set. This API is
 * relevant if UTMI interface is used.
 *
 * \param config Pointer to the configuration object.
 * \param utmiPinLevel UTMI pin level.
 */
void carbonXUsbConfigSetUtmiPinLevel(CarbonXUsbConfig *config,
        CarbonXUsbUtmiPinLevel utmiPinLevel);

/*!
 * \brief Gets USB UTMI pin level from a configuration object.
 *
 * This API will return the UTMI pin level from the configuration object.
 * This API is relevant if UTMI interface is used.
 *
 * \param config Pointer to the configuration object.
 *
 * \returns UTMI pin level from the configuration object.
 */
CarbonXUsbUtmiPinLevel carbonXUsbConfigGetUtmiPinLevel(
        const CarbonXUsbConfig *config);

/*!
 * \brief Sets device descriptor.
 *
 * This API will set device descriptor. This is mandatory for device and
 * undefined for host.
 *
 * \param config Pointer to the configuration object.
 * \param descriptor Device descriptor to be set.
 */
void carbonXUsbConfigSetDeviceDescriptor(CarbonXUsbConfig *config,
        CarbonUInt8* descriptor);

/*!
 * \brief Sets OTG descriptor.
 *
 * This API will set OTG descriptor. This is mandatory for an OTG device and
 * undefined for host or normal device.
 *
 * \param config Pointer to the configuration object.
 * \param descriptor OTG descriptor to be set.
 */
void carbonXUsbConfigSetOtgDescriptor(CarbonXUsbConfig *config,
        CarbonUInt8* descriptor);

/*!
 * \brief Adds Configuration descriptor to a configuration object.
 *
 * This API will add configuration (including interface and endpoint
 * descriptors) descriptor. This is mandatory for device and undefined
 * for host.
 *
 * \param config Pointer to the configuration object.
 * \param descriptor Device descriptor to be added.
 */
void carbonXUsbConfigAddConfigDescriptor(CarbonXUsbConfig *config,
        CarbonUInt8* descriptor);

/*!
 * \brief Sets USB timing factor in the configuration object.
 *
 * This API will set timing factor for USB timings. Thus all timing intervals
 * will be divided by this factor, to reduce total simulation time and
 * advance the actual packet based transfers.
 *
 * Following timings will be affected by this factor:
 *    -# Reset from host driving SE0
 *    -# HS detection using chirps from host and device
 *    -# Suspend and resume
 *    -# USB OTG timings e.g. SRP (VBUS and Data Line Pulsing) and HNP
 *
 * Note, this factor will not affect device attach, detach, suspend and normal
 * packet communication timings.
 *
 * Note, this will also not affect small chirps or inter-chirp delays in
 * resume timing e.g. SE0 in EOR (End Of Resume) sequence.
 *
 * Note, for OTG this factor will affect HNP suspend timing in 1/60 times.
 *
 * \param config Pointer to the configuration object.
 * \param factor Timing factor. This factor can range from 1 to 600.
 *               Default is 600. If any value outside this interval is
 *               provided, nearest of 1 or 600 will be considered.
 */
void carbonXUsbConfigSetTimingFactor(CarbonXUsbConfig *config,
        CarbonUInt32 factor);

/*!
 * \brief Sets the USB PHY side plug of OTG Device to Mini-A type
 *
 * This API will set plug type connected to PHY UTM, offered to connect to the
 * LINK side of other transactor, as Mini-A type. This provides a grounded ID
 * pin to PHY UTM, and thus allows a USB OTG device to be connected as the other
 * LINK transactor initially as A-device i.e. as host.
 *
 * Default is Standard-A, Standard-B or Mini-B type for OTG device, which
 * provides open i.e. pulled-up ID pin to PHY UTM. This will allow to connect
 * a B-device (i.e. Device or OTG Device) to it.
 *
 * For Host, this plug is considered as Standard-B or Mini-B, and for Device,
 * it is considered as Standard-A or Mini-A. In such cases, this API is ignored.
 *
 * \param config Pointer to the configuration object.
 */
void carbonXUsbConfigSetMiniAOtgPhyPlug(CarbonXUsbConfig *config);

/*!
 * \brief Sets SRP detection support for OTG A device
 *
 * Default support is VBUS pulsing. This API is relevant only for OTG
 * A device.
 */
void carbonXUsbConfigSetOtgSrpDetectionSupport(CarbonXUsbConfig *config,
        CarbonXUsbOtgSrpDetectionSupport srpSupport);

/*!
 * @}
 */

#ifdef __cplusplus
}
#endif

#endif
