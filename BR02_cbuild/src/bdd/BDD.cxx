// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "nucleus/NUExpr.h"
#include "symtab/STBranchNode.h"
#include "symtab/STAliasedLeafNode.h"
#include "util/StringAtom.h"
#include "util/UtHashMap.h"
#include "nucleus/NUExprFactory.h"
#include "util/UtIOStream.h"
#include "nucleus/NUCost.h"
#include "bdd/BDD.h"

#if BDD_USE_CUDD
#include "cudd.h"
#include "util.h"
#endif
#if BDD_USE_ATT
extern "C" {
#include "bdduser.h"
#ifdef CDB
#include <stdio.h>
#endif

}
#endif


/*!
  \file
  Implementation of BDD Package.
*/


// Empirically, test/cust/star/ar/atop finds 28 master clocks and
// clock analysis runs in 5 seconds.  Increasing this cost limit
// makes clock analysis take a lot longer but it still winds up
// with 28 clocks.
static int sDefaultMaxCost = 100;

// An alternative metric is for expressions with high gate count but
// low operation count, like test/fold/consteval.v which has
//      input [149:0] c;
//	o[46] = ((c === 0) == (~|c));
// which BDDs to 'true' really quickly.  Probably the uniformity of
// the expression makes it an easy BDD, despite the fact that, after
// bit-blasting, there are 150 variables and 300 gates or so.  So even
// if our max gate-count is exceeded we will try the BDD if there are
// 8 operations or less
static int sDefaultMaxOpCost = 8;

// How many extra BDD nodes are we willing to grow with each expression?
static int sDefaultBDDMaxDelta = 20000;

//! Maximum bdd size
/*! As BDD's get bigger the package gets slower as it adds on to the
 *  bdd. This limit will cause us to fail to create a bdd if a sub
 *  expression's bdd gets too big.
 *
 *  The value 2000 was chosen empirically using
 *  test/cust/sicortex/ice9/carbon/cwtb/m5kf and
 *  test/cust/matrox/phoenix/carbon/projects/regression. 2000 was the
 *  minimum necessary to get most of the pre-existing clock
 *  equivalences and that value still allowed for a fast compile on
 *  ice9. We may want to tweak this some more over time.
 */
static int sDefaultMaxBDDSize = 2000;

BDDContext::BDDContext(UInt32 nodeLimit)
{
  BDD_CUDD(mManager = Cudd_Init(0, 0, CUDD_UNIQUE_SLOTS, CUDD_CACHE_SLOTS, 0)); // note no memory limit specified
  BDD_ATT(mManager = bdd_init());
  BDD_ATT(bdd_node_limit(mManager, nodeLimit));
  init();

  BDD_ATT(mZero = mkbdd(bdd_zero(mManager)));
  BDD_CUDD(mZero = mkbdd(Cudd_ReadLogicZero(mManager)));
  BDD_ATT(mOne = mkbdd(bdd_one(mManager)));
  BDD_CUDD(mOne = mkbdd(Cudd_ReadOne(mManager)));
  mInvalid = mkbdd(0);
  mExprCaching = false;
  mExprFactory = NULL;
  mOwnFactory = false;
  mCreateMuxes = false;
  mIsConservative = false;
  mReduceExpressions = true;
  mMakeIdentsForInvalidBDDs = false;

  mMaxCost = sDefaultMaxCost;
  mMaxOpCost = sDefaultMaxOpCost;
  mBDDMaxDelta = sDefaultBDDMaxDelta;
  mMaxBDDSize = sDefaultMaxBDDSize;
}

void BDDContext::init() {
  mBDDIdents = new BDDIdents;
  mReverseMap = new ReverseMap;

#if defined(CDB) || defined(BDD_PRINT_DETAIL)
  mDbgStringMap = new DbgStringMap;
#endif

  mExprBDDMap = NULL;
  mBDDExprMap = NULL;
  mCostContext = new NUCostContext;
}

void BDDContext::destroy() {
  if (mBDDExprMap != NULL)
    delete mBDDExprMap;
  if (mExprBDDMap != NULL)
    delete mExprBDDMap;
  if (mOwnFactory)
    delete mExprFactory;

  delete mReverseMap;
#if defined(CDB) || defined(BDD_PRINT_DETAIL)
  DbgStringMap::iterator p;
  for (p = mDbgStringMap->begin(); p != mDbgStringMap->end(); ++p)
    delete p->second;
  delete mDbgStringMap;
#endif
  BDDIdents::iterator i;
  for (i = mBDDIdents->begin(); i != mBDDIdents->end(); ++i)
    delete *i;
  delete mBDDIdents;
  mBDDMap.clear();

  delete mCostContext;
} // void BDDContext::destroy

BDDContext::~BDDContext()
{
  destroy();

#ifdef CDB
  BDD_CUDD(INFO_ASSERT((0 == Cudd_DebugCheck(mManager)), "Inconsistency during bdd cleanup"));
  BDD_CUDD(INFO_ASSERT(( 0 == Cudd_CheckZeroRef(mManager)), "Inconsistency during bdd cleanup"));
#endif
  BDD_CUDD(Cudd_Quit(mManager));
  BDD_ATT(bdd_quit(mManager));

  // Manually invalidate these BDDs, since the BDD manager has been shut down
  mZero.mRep = 0;
  mOne.mRep = 0;
}


// Special method for reducing the effectiveness of comparing
// expression BDDs, used for -fredo.
void BDDContext::cripple() {
  // We still need some very basic BDD analysis so reset analysis works.
  // This was lowered from 65 to 5 when the optimization to reduce
  // (reset==32'b0) a single-bit comparison was implemented.
  sDefaultMaxCost = 5;
}


// Open up our thresholds
void BDDContext::openWide() {
  mMaxCost   = 5000;
  mMaxOpCost = 1000;
}


BDD BDDContext::var()
{
  BDD_CUDD(return mkbdd(Cudd_bddNewVar(mManager)));
  BDD_ATT(return mkbdd(bdd_new_var_last(mManager)));
}

BDD BDDContext::opNot(const BDD& expr)
{
  if (expr.isValid()) {
    BDD_CUDD(return mkbdd(Cudd_Not(expr.rep())));
    BDD_ATT(return mkbdd(bdd_not(mManager, expr.rep())));
  } else {
    return invalid();
  }
}


BDD BDDContext::opEqual(const BDD& expr1, const BDD& expr2)
{
  return opXnor(expr1, expr2);
/*

I'm not sure this works for the AT&T bdd library, because we don't
create mtbdds anywhere.  I cannot find an analogous routine for Cudd
either.

  if (expr1.isValid() and expr2.isValid()) {
    return mkbdd(mtbdd_equal(mManager, expr1.rep(), expr2.rep()));
  } else {
    return invalid();
  }
*/
}



BDD BDDContext::opAnd(const BDD& expr1, const BDD& expr2)
{
  if (expr1 == mZero or expr2 == mZero)
    return mZero;
  if (expr1.isValid() and expr2.isValid()) {
    BDD_CUDD(return mkbdd(Cudd_bddAnd(mManager, expr1.rep(), expr2.rep())));
    BDD_ATT(return mkbdd(bdd_and(mManager, expr1.rep(), expr2.rep())));
  } else {
    return invalid();
  }
}


BDD BDDContext::opNand(const BDD& expr1, const BDD& expr2)
{
  if (expr1 == mZero or expr2 == mZero)
    return mOne;
  if (expr1.isValid() and expr2.isValid()) {
    BDD_CUDD(return mkbdd(Cudd_bddNand(mManager, expr1.rep(), expr2.rep())));
    BDD_ATT(return mkbdd(bdd_nand(mManager, expr1.rep(), expr2.rep())));
  } else {
    return invalid();
  }
}


BDD BDDContext::opOr(const BDD& expr1, const BDD& expr2)
{
  if (expr1 == mOne or expr2 == mOne)
    return mOne;
  if (expr1.isValid() and expr2.isValid()) {
    BDD_CUDD(return mkbdd(Cudd_bddOr(mManager, expr1.rep(), expr2.rep())));
    BDD_ATT(return mkbdd(bdd_or(mManager, expr1.rep(), expr2.rep())));
  } else {
    return invalid();
  }
}


BDD BDDContext::opNor(const BDD& expr1, const BDD& expr2)
{
  if (expr1 == mOne or expr2 == mOne)
    return mZero;
  if (expr1.isValid() and expr2.isValid()) {
    BDD_CUDD(return mkbdd(Cudd_bddNor(mManager, expr1.rep(), expr2.rep())));
    BDD_ATT(return mkbdd(bdd_nor(mManager, expr1.rep(), expr2.rep())));
  } else {
    return invalid();
  }
}


BDD BDDContext::opXor(const BDD& expr1, const BDD& expr2)
{
  if (expr1.isValid() and expr2.isValid()) {
    BDD_CUDD(return mkbdd(Cudd_bddXor(mManager, expr1.rep(), expr2.rep())));
    BDD_ATT(return mkbdd(bdd_xor(mManager, expr1.rep(), expr2.rep())));
  } else {
    return invalid();
  }
}


BDD BDDContext::opXnor(const BDD& expr1, const BDD& expr2)
{
  if (expr1.isValid() and expr2.isValid()) {
    BDD_CUDD(return mkbdd(Cudd_bddXnor(mManager, expr1.rep(), expr2.rep())));
    BDD_ATT(return mkbdd(bdd_xnor(mManager, expr1.rep(), expr2.rep())));
  } else {
    return invalid();
  }
}


BDD BDDContext::opCond(const BDD& cond, const BDD& expr1, const BDD& expr2)
{
  if (cond == mOne and expr1.isValid())
    return expr1;
  if (cond == mZero and expr2.isValid())
    return expr2;
  if (expr1.isValid() and expr2.isValid() and (expr1 == expr2))
    return expr1;
  if (cond.isValid() and expr1.isValid() and expr2.isValid()) {
    BDD_CUDD(return mkbdd(Cudd_bddIte(mManager, cond.rep(), expr1.rep(), expr2.rep())));
    BDD_ATT(return mkbdd(bdd_ite(mManager, cond.rep(), expr1.rep(), expr2.rep())));
  } else {
    return invalid();
  }
}


BDD BDDContext::bdd(NUNet *net)
{
  BDDIdent* ident = getIdent(net);
  return genericCreateBDD(ident);
}


BDD BDDContext::bdd(NUNet* net, SInt32 bit)
{
  if (bit == 0 && net->getBitSize() == 1 ){
    // this is vector of size one bit, and so the whole net being accessed
    return bdd(net);
  } else {
    BDDIdent* ident = getIdent(net, bit);
    return genericCreateBDD(ident);
  }
}


void BDDContext::assign(const BDD& bdd, const NUExpr* expr)
{
  NU_ASSERT(mExprCaching, expr);
  (*mExprBDDMap)[expr] = bdd;
}

bool BDDContext::isCached(const NUExpr* expr, BDD* retBDD)
{
  if (mExprCaching) {
    ExprBDDMap::iterator p = mExprBDDMap->find(expr);
    if (p != mExprBDDMap->end()) {
      *retBDD = p->second;
      return true;
    }
  }
  return false;
}

BDD BDDContext::storeCache(const NUExpr* expr, BDD bdd)
{
  if (mExprCaching) {
    if (!bdd.isValid()) {
      bdd = var();        // uniquify & save so isomorphic expressions compare
    }
    (*mExprBDDMap)[expr] = bdd;
  }
  return bdd;
}

BDD BDDContext::bdd(const NUExpr* expr, STBranchNode *scope)
{
  BDD bdd = invalid();
  if (isCached(expr, &bdd)) {
    return bdd;
  }

#ifdef CDB
#define DBG_BDD 0 // 1
#else
#define DBG_BDD 0
#endif
  // something in AT&T BDDs makes them go haywire when they get big.
  NUCost cost;
  expr->calcCost(&cost, mCostContext);
  bool lowCost = true;
  if(cost.mAsicGates >= 10000)
  {
    lowCost = false;
    return bdd;
  }
  else if((cost.mAsicGates >= mMaxCost) && (cost.mOps >= mMaxOpCost))
    lowCost = false;

#if DBG_BDD
  if (isLowCost) {
    long oldVars = bdd_vars(mManager);
    long oldSize = bdd_total_size(mManager);
    bdd = expr->bdd(this, scope);
    UtIO::cout() << "BDD vars = " << SInt32(bdd_vars(mManager) - oldVars)
                 << "/" << SInt32(bdd_vars(mManager))
                 << " size = " << SInt32(bdd_total_size(mManager) - oldSize)
                 << "/" << SInt32(bdd_total_size(mManager)) << " "
                 << "bdd = " << (void*)(bdd.rep())
                 << (mExprCaching? " (cache) ": " (nocache) ");
  }
  else
    UtIO::cout() << "punting BDD ";
  expr->printVerilog(true);
#else
  if (lowCost) {
    SInt32 prevNumEntries = numEntries();
    bdd = expr->bdd(this, scope);

    if (bdd.isValid()) {
      SInt32 newNumEntries = numEntries();
      // If the node-count got wacky then let's forget the BDD in hopes that
      // the BDD package will recover all the nodes it lost
      if ((newNumEntries - prevNumEntries) > mBDDMaxDelta) {
#if BDD_ATT
        // See if garbage collecting will help
        bdd_gc(mManager);
        newNumEntries = numEntries();
        if ((newNumEntries - prevNumEntries) > mBDDMaxDelta) {
          // otherwise, kill the result & recover the memory
          bdd = invalid();
          bdd_gc(mManager);
        }
#else
        bdd = invalid();
#endif
      }

      // Test if the bdd got too big
      if (bdd.isValid() && (size(bdd) > mMaxBDDSize)) {
        bdd = invalid();
      }
    }
  }
#endif

  if (!bdd.isValid() && mMakeIdentsForInvalidBDDs && (expr->getBitSize() == 1)
      && !expr->drivesZ())
  {
    BDDIdent* ident = getIdent(const_cast<NUExpr*>(expr));
    bdd = genericCreateBDD(ident);
  }

  // Save this in the cache (it may modify the bdd in the process)
  return storeCache(expr, bdd);
} // BDD BDDContext::bdd

BDD BDDContext::bddRecurse(const NUExpr* expr, STBranchNode* scope)
{
  // Check the cache first
  BDD bdd = invalid();
  if (isCached(expr, &bdd)) {
    return bdd;
  }

  // Create the bdd but test if the bdd got too big
  bdd = expr->bdd(this, scope);
  if (bdd.isValid()) {
    if (size(bdd) > mMaxBDDSize) {
      bdd = invalid();
    }
  }
  else if (mMakeIdentsForInvalidBDDs && (expr->getBitSize() == 1)
           && !expr->drivesZ())
  {
    BDDIdent* ident = getIdent(const_cast<NUExpr*>(expr));
    bdd = genericCreateBDD(ident);
  }

  // Save this in the cache (it may modify the bdd in the process)
  return storeCache(expr, bdd);
}

BDD BDDContext::bdd(NUNetElab *net_elab)
{
  BDDIdent* ident = getIdent(net_elab);
  return genericCreateBDD(ident);
}

BDD BDDContext::bdd(NUNetElab* netElab, SInt32 bit)
{
  BDDIdent* ident = getIdent(netElab, bit);
  return genericCreateBDD(ident);
}

BDD BDDContext::bdd(const UtString& str)
{
  BDDIdent* ident = getIdent(str);
  return genericCreateBDD(ident);
}

BDD BDDContext::genericCreateBDD(BDDIdent* ident)
{
  BDDMap::iterator iter = mBDDMap.find(ident);
  if (iter == mBDDMap.end()) {
    BDD new_bdd = var();
    mBDDMap.insert(BDDMap::value_type(ident, new_bdd));
    mReverseMap->insert(ReverseMap::value_type(new_bdd, ident));
    return new_bdd;
  } else {
    return iter->second;
  }
}

void BDDContext::assign(const BDD& bdd, NUNet *net)
{
  BDDIdent* ident = getIdent(net);
  genericAssign(bdd, ident);
}


void BDDContext::assign(const BDD& bdd, NUNetElab *net_elab)
{
  BDDIdent* ident = getIdent(net_elab);
  genericAssign(bdd, ident);
}


void BDDContext::assign(const BDD& bdd, NUNetElab* netElab, SInt32 bit)
{
  BDDIdent* ident = getIdent(netElab, bit);
  genericAssign(bdd, ident);
}


void BDDContext::deassign(NUNet *net)
{
  BDDIdent* ident = getIdent(net);
  genericDeassign(ident);
}


void BDDContext::deassign(NUNetElab *net_elab)
{
  BDDIdent* ident = getIdent(net_elab);
  genericDeassign(ident);
}


void BDDContext::deassign(NUNetElab* netElab, SInt32 bit)
{
  BDDIdent* ident = getIdent(netElab, bit);
  genericDeassign(ident);
}


bool BDDContext::isAssigned(NUNet *net) const
{
  BDDIdent* ident = getIdent(net);
  return genericIsAssigned(ident);
}


bool BDDContext::isAssigned(NUNetElab *net_elab) const
{
  BDDIdent* ident = getIdent(net_elab);
  return genericIsAssigned(ident);
}


bool BDDContext::isAssigned(NUNetElab* netElab, SInt32 bit) const
{
  BDDIdent* ident = getIdent(netElab, bit);
  return genericIsAssigned(ident);
}


void BDDContext::genericAssign(const BDD& bdd, BDDIdent* ident)
{
  mBDDMap[ident] = bdd;

  // Move the reverse map to point to this new elaborated net
  ReverseMap::iterator pos = mReverseMap->find(bdd);
  if (pos == mReverseMap->end())
    // Didn't think this could happen, but lets just assign it then
    mReverseMap->insert(ReverseMap::value_type(bdd, ident));
  else if (pos->second != ident)
  {
    // Set the new value for the reverse map
    pos->second = ident;
  }
}


void BDDContext::genericDeassign(BDDIdent* ident)
{
  BDDMap::iterator iter = mBDDMap.find(ident);
  if (iter != mBDDMap.end()) {
    mBDDMap.erase(iter);
  }
}


bool BDDContext::genericIsAssigned(BDDIdent* ident) const
{
  BDDMap::const_iterator iter = mBDDMap.find(ident);
  return (iter != mBDDMap.end());
}

BDD BDDContext::topVariable(const BDD& bdd)
{
  if (bdd == mZero || bdd == mOne || bdd == invalid())
    return invalid();
  BDD_CUDD(return invalid());
  BDD_ATT(return mkbdd(bdd_if(mManager,bdd.rep())));
}

BDD BDDContext::thenVariable(const BDD& bdd)
{
  if (bdd == mZero || bdd == mOne || bdd == invalid())
    return invalid();
  BDD_CUDD(return invalid());
  BDD_ATT(return mkbdd(bdd_then(mManager,bdd.rep())));
}

BDD BDDContext::elseVariable(const BDD& bdd)
{
  if (bdd == mZero || bdd == mOne || bdd == invalid())
    return invalid();
  BDD_CUDD(return invalid());
  BDD_ATT(return mkbdd(bdd_else(mManager,bdd.rep())));
}

bool BDDContext::isInversion(const BDD& bdd)
{
  bool ret = false;
  if ((bdd != mZero) && (bdd != mOne) && (bdd != invalid())) {
    BDD_ATT(ret = bdd_type(mManager, bdd.rep()) == BDD_TYPE_NEGVAR);
  }
  return ret;
}

bool BDDContext::dependsOn(const BDD& condition, const BDD& var)
{
  BDD_CUDD(return true);
  BDD_ATT(return bdd_depends_on(mManager, condition.rep(), var.rep()) != 0);
}

bool BDDContext::implies(const BDD& assumption, const BDD& conclusion)
{
  // the implication operator 'assumption => conclusion' is defined as
  // '!assumption or conclusion', meaning that when the assumption is
  // true, the conclusion must also be true, and when the assumption is
  // false, the conclusion does not have to be true.
  if (assumption == invalid() || conclusion == invalid())
    return false;
  BDD result = opOr(opNot(assumption),conclusion);
  return (result == mOne);
}


void BDDContext::decodeVariable(const BDD& bdd, NUNet** netPtr, SInt32* bitPtr)
{
  ReverseMap::iterator pos = mReverseMap->find(bdd);
  if (pos == mReverseMap->end())
  {
    *netPtr = NULL;
  }
  else
  {
    // we found the BDDIdent, so decode it
    BDDIdent* ident = pos->second;
    switch (ident->getType())
    {
      case BDDIdent::eBDDNet:
        *netPtr = ident->getNet();
        *bitPtr = 0;
        break;
      case BDDIdent::eBDDNetBit:
        *netPtr = ident->getNetBit(bitPtr);
        break;
      case BDDIdent::eBDDNetElab:
      case BDDIdent::eBDDNetElabBit:
        INFO_ASSERT(0, "Decoding elaborated net with unelaborated method!");
      default:
        *netPtr = NULL;        
    }
  }
}

void BDDContext::decodeVariable(const BDD& bdd, NUNetElab** netPtr, SInt32* bitPtr)
{
  ReverseMap::iterator pos = mReverseMap->find(bdd);
  if (pos == mReverseMap->end())
  {
    *netPtr = NULL;
  }
  else
  {
    // we found the BDDIdent, so decode it
    BDDIdent* ident = pos->second;
    switch (ident->getType())
    {
      case BDDIdent::eBDDNetElab:
        *netPtr = ident->getNetElab();
        *bitPtr = 0;
        break;
      case BDDIdent::eBDDNetElabBit:
        *netPtr = ident->getNetElabBit(bitPtr);
        break;
      case BDDIdent::eBDDNet:
      case BDDIdent::eBDDNetBit:
        INFO_ASSERT(0, "Decoding unelaborated net with elaborated method!");
      default:
        *netPtr = NULL;        
    }
  }
}

BDD BDDContext::substitute(const BDD& bdd, const BDD& var, const BDD& value)
{
#if BDD_USE_ATT
  BDD::__bdd substArray[4];
  substArray[0] = var.rep();
  substArray[1] = value.rep();
  substArray[2] = 0;
  substArray[3] = 0;
  bdd_temp_assoc(mManager,substArray,1); // set the temporary association array
  bdd_assoc(mManager,-1); // tell the bdd package to use the temporary association
  return mkbdd(bdd_substitute(mManager,bdd.rep())); // make the substitution
#endif
#if BDD_USE_CUDD
#abort "Implement BDDContext::substitute() for CUDD"
#endif
}


UInt32 BDDContext::size(const BDD& bdd) const
{
  BDD_CUDD(return Cudd_DagSize(bdd.rep()));
  BDD_ATT(return bdd_size(mManager, bdd.rep(), 0));
}

#if defined(CDB) || defined(BDD_PRINT_DETAIL)
const UtString* BDDContext::str(const BDD& bdd)
{
  ReverseMap::iterator pos = mReverseMap->find(bdd);
  if (pos == mReverseMap->end())
    return NULL;
  else
    return identName(pos->second);
}
#endif

#if defined(CDB) || defined(BDD_PRINT_DETAIL)
char*
BDDContext::namingFn(BDDContext::__bdd_manager /* manager */, BDD::__bdd bdd, void* env)
{
  BDDContext* context = reinterpret_cast<BDDContext*>(env);
  const UtString* str = context->str(context->mkbdd(bdd));
  if (str == NULL)
    return NULL;
  else
    return const_cast<char*>(str->c_str());
}
#endif

//! Compose a textual representation of the BDD
void BDDContext::compose(UtString* str, BDD* bdd, bool escapeNewlines)
{
  // this is written funny to make escapeNewlines be always used
  bool showBDD = escapeNewlines ? false : false; 
#if defined(CDB) || defined(BDD_PRINT_DETAIL)
#ifdef BDD_USE_ATT
  showBDD = true;
#endif
#endif

  if (*bdd == mZero)
    str->append("0");
  else if (*bdd == mOne)
    str->append("1");
  else if (*bdd == invalid())
    str->append("(notvalid)");
  else if (!showBDD)
    *str << "(BDD of size " << size(*bdd) << " - " << bdd->hash() << ")";
#if defined(CDB) || defined(BDD_PRINT_DETAIL)
#ifdef BDD_USE_ATT
  else
  {
    BDD::__bdd bddrep = bdd->rep();
    if (bddrep) {
      FILE* tmp = tmpfile();
      if (tmp == NULL)
        str->append("(file error)");
      else
      {
        bdd_print_bdd(mManager, bddrep, BDDContext::namingFn, NULL, (void*)this, tmp);
        char buffer[128];
        rewind(tmp);
        while(fgets(buffer,127,tmp))
        {
          int n = strlen(buffer);
          if (escapeNewlines && (n > 0) && (buffer[n-1] == '\n'))
          {
            buffer[n-1] = '\\';
            buffer[n] = 'n';
            buffer[n+1] = '\0';
          }
          str->append(buffer);
        }
        fclose(tmp);
      }
    } else {
      str->append("(null)");
    }
  }
#endif
#endif
}

void BDDContext::print(BDD* bdd, FILE* file)
{
  if (file == NULL) {
    file = stdout;
  }

#if BDD_USE_ATT
  BDD::__bdd bddrep = bdd->rep();
  if (bddrep) {
#if defined(CDB) || defined(BDD_PRINT_DETAIL)
    bdd_print_bdd(mManager, bddrep, BDDContext::namingFn, NULL, (void*)this, file);
#else
    fprintf(file, "<<BDD>>\n");
#endif
  } else {
    fprintf(file,"(null)\n");
  }
#endif
#if BDD_USE_CUDD
  BDD::__bdd bddrep = bdd->rep();
  if (bddrep) {
    int size = Cudd_ReadSize(mManager);
#if defined(CDB) || defined(BDD_PRINT_DETAIL)
    Cudd_PrintDebug(mManager, bddrep, size, 4);
#else
    fprintf(file, "<<BDD of size %d>>\n", size);
#endif

  } else {
    fprintf(file,"(null)\n");
  }
#endif
}

#if defined(CDB) || defined(BDD_PRINT_DETAIL)
const UtString* BDDContext::identName(BDDIdent* ident)
{
  // Check if we already have one
  DbgStringMap::iterator pos = mDbgStringMap->find(ident);
  const UtString* str;
  if (pos == mDbgStringMap->end())
  {
    str = ident->makeString();
    mDbgStringMap->insert(DbgStringMap::value_type(ident, str));
  }
  else
    str = pos->second;
  return str;
}
#endif

BDDIdent* BDDContext::getIdent(const UtString& str) const
{
  BDDStringIdent srchIdent(str);
  BDDIdents::iterator pos = mBDDIdents->find(&srchIdent);
  BDDIdent* ident;
  if (pos == mBDDIdents->end())
  {
    // Doesn't exist yet, create it
    ident = new BDDStringIdent(str);
    mBDDIdents->insert(ident);
  }
  else
    ident = *pos;

  return ident;
}

BDDIdent* BDDContext::getIdent(NUNet* net) const
{
  BDDNetIdent srchIdent(net);
  BDDIdents::iterator pos = mBDDIdents->find(&srchIdent);
  BDDIdent* ident;
  if (pos == mBDDIdents->end())
  {
    // Doesn't exist yet, create it
    ident = new BDDNetIdent(net);
    mBDDIdents->insert(ident);
  }
  else
    ident = *pos;

  return ident;
}

BDDIdent* BDDContext::getIdent(NUNet* net, SInt32 bit) const
{
  BDDNetBitIdent srchIdent(net, bit);
  BDDIdents::iterator pos = mBDDIdents->find(&srchIdent);
  BDDIdent* ident;
  if (pos == mBDDIdents->end())
  {
    // Doesn't exist yet, create it
    ident = new BDDNetBitIdent(net,bit);
    mBDDIdents->insert(ident);
  }
  else
    ident = *pos;

  return ident;
}

BDDIdent* BDDContext::getIdent(NUNetElab* netElab) const
{
  BDDNetElabIdent srchIdent(netElab);
  BDDIdents::iterator pos = mBDDIdents->find(&srchIdent);
  BDDIdent* ident;
  if (pos == mBDDIdents->end())
  {
    // Doesn't exist yet, create it
    ident = new BDDNetElabIdent(netElab);
    mBDDIdents->insert(ident);
  }
  else
    ident = *pos;

  return ident;
}

BDDIdent* BDDContext::getIdent(NUNetElab* netElab, SInt32 bit) const
{
  BDDNetElabBitIdent srchIdent(netElab, bit);
  BDDIdents::iterator pos = mBDDIdents->find(&srchIdent);
  BDDIdent* ident;
  if (pos == mBDDIdents->end())
  {
    // Doesn't exist yet, create it
    ident = new BDDNetElabBitIdent(netElab, bit);
    mBDDIdents->insert(ident);
  }
  else
    ident = *pos;

  return ident;
}

BDDIdent* BDDContext::getIdent(const NUExpr* expr) const
{
  BDDExprIdent srchIdent(expr);
  BDDIdents::iterator pos = mBDDIdents->find(&srchIdent);
  BDDIdent* ident;
  if (pos == mBDDIdents->end())
  {
    // Doesn't exist yet, create it
    CopyContext cc(NULL, NULL);
    NU_ASSERT(expr->getBitSize() == 1, expr);
    if (!expr->isManaged()) {
      CopyContext cc(NULL, NULL);
      expr = mExprFactory->insert(expr->copy(cc));
    }
    ident = new BDDExprIdent(expr);
    UInt32 sz = mBDDIdents->size();
    mBDDIdents->insert(ident);
    NU_ASSERT(sz + 1 == mBDDIdents->size(), expr);
  }
  else
    ident = *pos;

  return ident;
}

const UtString* BDDIdent::getString() const
{
  INFO_ASSERT(0, "Not a string");
  return NULL;
}

NUNet* BDDIdent::getNet() const
{
  INFO_ASSERT(0, "Not a net");
  return NULL;
}

NUNet* BDDIdent::getNetBit(SInt32*) const
{
  INFO_ASSERT(0, "Not a net bit");
  return NULL;
}

NUNetElab* BDDIdent::getNetElab() const
{
  INFO_ASSERT(0, "Not a net elab");
  return NULL;
}

NUNetElab* BDDIdent::getNetElabBit(SInt32*) const
{
  INFO_ASSERT(0, "Not a net elab bit");
  return NULL;
}

const NUExpr* BDDIdent::getExpr() const
{
  INFO_ASSERT(0, "Not an expr");
  return NULL;
}

BDDIdent::Type BDDStringIdent::getType() const
{
  return eBDDString;
}

const UtString* BDDStringIdent::makeString() const
{
  return new UtString(mString);
}

const UtString* BDDStringIdent::getString() const
{
  return &mString;
}

BDDIdent::Type BDDNetIdent::getType() const
{
  return eBDDNet;
}

const UtString* BDDNetIdent::makeString() const
{
  UtString* str = new UtString;
  *str << mNet->getName()->str();
  return str;
}

NUNet* BDDNetIdent::getNet() const
{
  return mNet;
}

BDDIdent::Type BDDNetBitIdent::getType() const
{
  return eBDDNetBit;
}

const UtString* BDDNetBitIdent::makeString() const
{
  UtString* str = new UtString;
  *str << mNet->getName()->str() << "[" << mBit << "]";
  return str;
}

NUNet* BDDNetBitIdent::getNetBit(SInt32* bit) const
{
  *bit = mBit;
  return mNet;
}

BDDIdent::Type BDDNetElabIdent::getType() const
{
  return eBDDNetElab;
}

const UtString* BDDNetElabIdent::makeString() const
{
  UtString* str = new UtString;
  mNetElab->compose(str, NULL);
  return str;
}

NUNetElab* BDDNetElabIdent::getNetElab() const
{
  return mNetElab;
}

BDDIdent::Type BDDNetElabBitIdent::getType() const
{
  return eBDDNetElabBit;
}

const UtString* BDDNetElabBitIdent::makeString() const
{
  UtString* str = new UtString;
  mNetElab->compose(str, NULL);
  *str << "[" << mBit << "]";
  return str;
}

NUNetElab* BDDNetElabBitIdent::getNetElabBit(SInt32* bit) const
{
  *bit = mBit;
  return mNetElab;
}

BDDIdent::Type BDDExprIdent::getType() const
{
  return eBDDExpr;
}

const UtString* BDDExprIdent::makeString() const
{
  UtString* str = new UtString;
  mExpr->compose(str, NULL);
  return str;
}

const NUExpr* BDDExprIdent::getExpr() const
{
  return mExpr;
}

// constructor
BDD::BDD(__bdd rep, BDDContext* ctx)
  : mRep(rep), mBDDContext(ctx)
{
  if (mRep != 0)
  {
    BDD_CUDD(Cudd_Ref(mRep));
    // AT&T BDD's get created with a ref-count of 1, so do not inc here
  }
}

//! copy constructor
BDD::BDD(const BDD& bdd)
{
  mRep = bdd.mRep;
  mBDDContext = bdd.mBDDContext;
  if (mRep != 0)
  {
    BDD_CUDD(Cudd_Ref(mRep));
    BDD_ATT(bdd_unfree(mBDDContext->getManager(), mRep));
  }
}

//! assign operator
BDD& BDD::operator=(const BDD& bddRHS)
{
  if (this != &bddRHS )          // if ( LHS != RHS ) then we need to do the copy
  {
    mBDDContext = bddRHS.mBDDContext; // if mBDDContext is not yet set, then set it here

    if (mRep != bddRHS.mRep)    // if the lhs.mRep is different from
                                // the rhs.mRep then we need to adjust
                                // ref counts, and actualy do the transfer
                                // (should this test use Cudd_Regular(mRep?))
    {
      if (mRep != 0)
      {
        // decrease ref count of original LHS
        BDD_CUDD(Cudd_RecursiveDeref(mBDDContext->getManager(), mRep));
        BDD_ATT(bdd_free(mBDDContext->getManager(), mRep));
      }
      mRep = bddRHS.mRep;
      if (mRep != 0)
      {
        // increase ref count for the RHS
        BDD_CUDD(Cudd_Ref(mRep));
        BDD_ATT(bdd_unfree(mBDDContext->getManager(), mRep));
      }
    }
  }
  return *this;
} // BDD& BDD::operator=

//! destructor
BDD::~BDD() {
  if (mRep != 0)
  {
    BDD_CUDD(Cudd_RecursiveDeref(mBDDContext->getManager(), mRep));
    BDD_ATT(bdd_free(mBDDContext->getManager(), mRep));
  }
}

void BDD::print(FILE* file) {
  if (file == NULL) {
    file = stdout;
  }

  if (mRep != 0){
    mBDDContext->print(this, file);
  } else{
    fprintf(file,"(notvalid)\n");
  }
}

//! Compose a textual representation of the BDD
void BDD::compose(UtString* str, bool escapeNewlines)
{
  if (mRep != 0) {
    mBDDContext->compose(str, this, escapeNewlines);
  } else {
    str->append("(notvalid)");
  }
}



void BDDContext::putExpressionFactory(NUExprFactory* exprFactory)
{
  INFO_ASSERT((mExprFactory==NULL), "Tried to set expression factory with a null argument");
  mExprFactory = exprFactory;
  mOwnFactory = false;
}

void BDDContext::putMakeIdentsForInvalidBDDs(bool onOff) {
  mMakeIdentsForInvalidBDDs = onOff;
}

void BDDContext::putIsConservative(bool onOff) {
  mIsConservative = onOff;
}

void BDDContext::putExpressionCaching(bool onOff)
{
  if (onOff) {
    if (mExprBDDMap == NULL) {
      if (mExprFactory == NULL) {
        mExprFactory = new NUExprFactory;
        mOwnFactory = true;
      }
      mExprBDDMap = new ExprBDDMap;
      INFO_ASSERT(( mBDDExprMap == NULL), "Expr map already set");
      mBDDExprMap = new BDDExprMap;
    }
  }
  else {
    if (mExprBDDMap != NULL) {
      delete mExprBDDMap;
      mExprBDDMap = NULL;
      INFO_ASSERT(mBDDExprMap, "Expr map not defined");
      delete mBDDExprMap;
      mBDDExprMap = NULL;
      if (mOwnFactory) {
        delete mExprFactory;
        mExprFactory = NULL;
        mOwnFactory = false;
      }
    }
  }

  mExprCaching = onOff;
}

bool BDDContext::managerNearingInternalLimits ()
{
  bool nearLimit = false;

  // here is a test for the internal limit of indexindex, (an internal
  // limit in the att package, cudd apparently can reuse these unique identifiers.
  // (perhaps because of a better ref count implementation?)
  BDD_ATT(nearLimit = (bdd_percent_full(mManager) > 90)); // 90% is an arbitrary value
  BDD_CUDD(nearLimit = false);  // no such limit in cudd.

  // other tests would go here:

  // TBD Review comment: this limit of 40000 should be a class variable
  return nearLimit || (mIsConservative && (numEntries() > 40000));
}

#if BDD_USE_CUDD
const NUExpr* BDDContext::bddToExpr(const BDD&, const SourceLocator&) {
  return NULL;
}
#else


NUExpr* BDDContext::bddInversionToExpr(const BDD& bdd,
                                       const SourceLocator& loc)
{
  const NUExpr* subExpr = bddToExpr(opNot(bdd), loc);
  if (subExpr != NULL) {
    return new NUUnaryOp(NUOp::eUnLogNot, subExpr, loc);
  }
  return NULL;
}

NUExpr* BDDContext::binaryExpr(NUOp::OpT op,
                               const BDD& bdd1, 
                               const BDD& bdd2,
                               const SourceLocator& loc)
{
  const NUExpr* e1 = bddToExpr(bdd1, loc);
  if (e1 != NULL) {
    const NUExpr* e2 = bddToExpr(bdd2, loc);
    if (e2 != NULL) {
      NUExpr* e = new NUBinaryOp(op, e1, e2, loc);
      return e;
    }
  }
  return NULL;
}

NUExpr* BDDContext::bddCondOpToExpr(const BDD& bdd,
                                    const SourceLocator& loc)
{
  NUExpr* ret = NULL;
  BDD condVar = topVariable(bdd);
  BDD thenVar = thenVariable(bdd);
  BDD elseVar = elseVariable(bdd);

  if (condVar.isValid() && thenVar.isValid() && elseVar.isValid()) {
    if (mReduceExpressions) {
      // Rather than waiting for fold to simplify ?: exprs from
      // BDDs to simpler gates, try and recognize these up front.

      // 1. a?b:~b --> a^(~b)
      // 2. a?~b:b --> a^b;     (this is really the same as #1.
      // 3. a?1:b  --> a|b
      // 4. a?0:b  --> (~a)&b or ~(a|~b)
      // 5. a?b:1  --> (~a)|b or ~(a&~b)
      // 6. a?b:0  --> a&b

      // I believe there are no more reducible cases, and also that
      // the condition cannot appear in either the 'then' or 'else'
      // clauses, even in inverted form.  Let's test that assumption
      // first.
      LOC_ASSERT((condVar != thenVar), loc);
      LOC_ASSERT((condVar != elseVar), loc);
      LOC_ASSERT((condVar != opNot(thenVar)), loc);
      LOC_ASSERT((condVar != opNot(elseVar)), loc);

      // Now do all the 6 cases listed above
      if (thenVar == opNot(elseVar)) { // 1,2: a?~b:b --> a^b
        ret = binaryExpr(NUOp::eBiBitXor, condVar, elseVar, loc);
      }
      else if (thenVar == mOne) {     // 3: a?1:b  --> a|b
        ret = binaryExpr(NUOp::eBiBitOr, condVar, elseVar, loc);
      }
      else if (thenVar == mZero) {    // 4: a?0:b  --> (~a)&b
        if (!isInversion(condVar) && isInversion(elseVar)) {
          // demorgan: ~(a|~b)
          ret = binaryExpr(NUOp::eBiBitOr, condVar, opNot(elseVar), loc);
          if (ret != NULL) {
            ret = new NUUnaryOp(NUOp::eUnLogNot, ret, loc);
          }
        }
        else {
          // (~a)&b
          ret = binaryExpr(NUOp::eBiBitAnd, opNot(condVar), elseVar, loc);
        }
      }
      else if (elseVar == mOne) {     // 5: a?b:1  --> (~a)|b
        if (!isInversion(condVar) && isInversion(thenVar)) {
          // demorgan: ~(a&~b)
          ret = binaryExpr(NUOp::eBiBitAnd, condVar, opNot(thenVar), loc);
          if (ret != NULL) {
            ret = new NUUnaryOp(NUOp::eUnLogNot, ret, loc);
          }
        }
        else {
          // (~a)|b
          ret = binaryExpr(NUOp::eBiBitOr, opNot(condVar), thenVar, loc);
        }
      }
      else if (elseVar == mZero) {    // 6: a?b:0  --> a&b
        ret = binaryExpr(NUOp::eBiBitAnd, condVar, thenVar, loc);
      }
      else {
        // General case, need to make ?: or mux, hopefully mux.
        const NUExpr* condExpr = bddToExpr(condVar, loc);
        if (condExpr != NULL) {
          const NUExpr* thenExpr = bddToExpr(thenVar, loc);
          if (thenExpr != NULL) {
            const NUExpr* elseExpr = bddToExpr(elseVar, loc);
            if (elseExpr != NULL) {
              bool createMux = false;

              // We really would rather not generate ternary ops, if we can
              // avoid it.  But the alternative is to reference condExpr
              // twice, so let's cost that one out and see if it's cheap.
              // It would be nice if we could generate a temp for condExpr
              // but we are not working at the statement level here.
              if (mCreateMuxes) {
                NUCostContext cc;
                NUCost condCost, thenCost, elseCost;
                cc.calcExpr(condExpr, &condCost);
                cc.calcExpr(thenExpr, &thenCost);
                cc.calcExpr(elseExpr, &elseCost);
                if ((condCost.mInstructionsRun < 20) &&
                    (thenCost.mInstructionsRun < 10) &&
                    (elseCost.mInstructionsRun < 10))
                {
                  createMux = true;
                }
              }

              if (createMux) {
                // Build a MUX replicate the conditional
                const NUExpr* notCond = bddToExpr(opNot(condVar), loc);
                if (notCond != NULL) {
                  NUExpr* term1 = new NUBinaryOp(NUOp::eBiBitAnd, condExpr,
                                                 thenExpr, loc);
                  NUExpr* term2 = new NUBinaryOp(NUOp::eBiBitAnd, notCond,
                                                 elseExpr, loc);
                  ret = new NUBinaryOp(NUOp::eBiBitOr, term1, term2, loc);
                }
              }
              else {
                // The conditional is too expensive to replicate.  Build a ?:
                ret = new NUTernaryOp(NUOp::eTeCond,
                                      condExpr, thenExpr, elseExpr, loc);
              }
            }
          }
        }
      }
    } else {
      // Not reducing expressions; a straight translation of the BDD to a ?: form.
      const NUExpr* condExpr = bddToExpr(condVar, loc);
      if (condExpr != NULL) {
        const NUExpr* thenExpr = bddToExpr(thenVar, loc);
        if (thenExpr != NULL) {
          const NUExpr* elseExpr = bddToExpr(elseVar, loc);
          if (elseExpr != NULL) {
            ret = new NUTernaryOp(NUOp::eTeCond,
                                  condExpr, thenExpr, elseExpr, loc);
          }
        }
      }      
    }
  }
  return ret;
} // NUExpr* BDDContext::bddCondOpToExpr

const NUExpr* BDDContext::bddVarToExpr(const BDD& bdd,
                                       const SourceLocator& loc)
{
  NUExpr* ret = NULL;
  ReverseMap::iterator p = mReverseMap->find(bdd);
  if (p != mReverseMap->end()) {
    BDDIdent* id = p->second;
    SInt32 index;
    NUNet* net;
    NUNetElab* netElab;

    switch (id->getType()) {
    case BDDIdent::eBDDString:
      break;                    // ret will remain null
    case BDDIdent::eBDDNetElab:
      ret = new NUIdentRvalueElab(id->getNetElab(), loc);
      break;
    case BDDIdent::eBDDNetElabBit:
      netElab = id->getNetElabBit(&index);
      ret = new NUVarselRvalue(new NUIdentRvalueElab(netElab, loc),
                               ConstantRange(index, index), loc);
      break;
    case BDDIdent::eBDDNet:
      ret = new NUIdentRvalue(id->getNet(), loc);
      break;
    case BDDIdent::eBDDNetBit:
      net = id->getNetBit(&index);
      ret = new NUVarselRvalue(net, ConstantRange(index, index), loc);
      break;
    case BDDIdent::eBDDExpr:
      return id->getExpr();     // already in the factory
    } // switch
  }

  if (ret != NULL) {
    ret->resize(1);
    return mExprFactory->insert(ret);
  }
  return NULL;
} // NUExpr* BDDContext::bddVarToExpr

const NUExpr* BDDContext::bddToExpr(const BDD& bdd, const SourceLocator& loc) {
  NUExpr* expr = NULL;

  LOC_ASSERT((bdd.isValid()), loc);
  LOC_ASSERT((mExprFactory != NULL), loc);
  LOC_ASSERT((mBDDExprMap != NULL), loc);

  BDDExprMap::iterator q = mBDDExprMap->find(bdd);
  if (q != mBDDExprMap->end()) {
    return q->second;
  }

  switch (bdd_type(mManager, bdd.rep())) {
  case BDD_TYPE_NONTERMINAL: expr = bddCondOpToExpr(bdd, loc);         break;
  case BDD_TYPE_ZERO:        expr = NUConst::create(false, 0, 1, loc); break;
  case BDD_TYPE_ONE:         expr = NUConst::create(false, 1, 1, loc); break;
  case BDD_TYPE_POSVAR:      return bddVarToExpr(bdd, loc);            break;
  case BDD_TYPE_NEGVAR:      expr = bddInversionToExpr(bdd, loc);      break;
  case BDD_TYPE_OVERFLOW:    expr = NULL;                              break;
  case BDD_TYPE_CONSTANT:    expr = NULL;                              break;
  }
  if (expr != NULL) {
    expr->resize(1);
    expr->setSignedResult(false); // any expression reduced by bdd transform must now be unsigned
    const NUExpr* factoryExpr = mExprFactory->insert(expr);
    (*mBDDExprMap)[bdd] = factoryExpr;

    // Punt if the expression count gets too insane
    if (mBDDExprMap->size() > ((size_t) 4*mMaxCost)) {
      factoryExpr = NULL;
    }
    return factoryExpr;
  }

  return NULL;
} // const NUExpr* BDDContext::bddToExpr

#endif

UInt32 BDDContext::numEntries() const {
  BDD_ATT(return bdd_total_size(mManager));
  BDD_CUDD(INFO_ASSERT(0, "Attempt to get numEntries from CUDD"); return 0);
}

void BDDContext::clear() {
  if (mBDDExprMap != NULL)
    mBDDExprMap->clear();
  if (mExprBDDMap != NULL)
    mExprBDDMap->clear();
  if (mOwnFactory)
    mExprFactory->clear();

  mReverseMap->clear();
#if defined(CDB) || defined(BDD_PRINT_DETAIL)
  DbgStringMap::iterator p;
  for (p = mDbgStringMap->begin(); p != mDbgStringMap->end(); ++p)
    delete p->second;
  mDbgStringMap->clear();
#endif
  BDDIdents::iterator i;
  for (i = mBDDIdents->begin(); i != mBDDIdents->end(); ++i)
    delete *i;
  mBDDIdents->clear();
  mBDDMap.clear();

  mCostContext->clear();
}

void BDDContext::putCreateMuxes(bool on_off) {
  mCreateMuxes = on_off;
}

void BDDContext::dump(const char* filename) {
  UtString err;
  FILE* file = OSFOpen(filename, "w", &err);
  if (file == NULL) {
    UtIO::cout() << err << "\n";
    return;
  }

  UtString buf;
  for (ExprBDDMap::SortedLoop p = mExprBDDMap->loopSorted(); !p.atEnd(); ++p) {
    const NUExpr* expr = p.getKey();
    const BDD& bdd = p.getValue();

    buf.clear();
    expr->compose(&buf, NULL);
    fprintf(file, "%p  %s\n", bdd.mRep, buf.c_str());
  }

  fclose(file);
}
