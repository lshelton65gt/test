// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2006 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
 /*
 * \file
 * Interface to stable sorting algorithm, avoiding problems with qsort
 */

#include "util/CarbonPlatform.h"
#include "util/CarbonTypes.h"
#include "util/CarbonAssert.h"

#include <cstddef>
#include <algorithm>

extern "C" void bddsort (void *pointer, size_t nmem, size_t size);

void bddsort (void * pointer, size_t nmem, size_t size)
{
  INFO_ASSERT ( size == sizeof(UInt16), "BDD sort expects 16 bit indices.");
  UInt16 **first = static_cast<UInt16**>(pointer);
  UInt16 **last = first + nmem;
  
  std::stable_sort<UInt16**>(first, last);
}
