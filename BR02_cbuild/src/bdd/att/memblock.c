/* Memory block management routines */
#include <stdlib.h>
#include <string.h> /* for memcpy */
#include "memint.h"

/*
  Make the AT&T BDD package us the Carbon memory manager.  This may
  result in increased "CmpMem" numbers, because we will be including BDD
  memory that was previously ignored.  But it should result in reduced
  CmpVM numbers, because the Carbon memory manager is slightly more
  efficient than the built in malloc.

  I had to add carbonmem_realloc(), to support mem_resize_block, which
  is called in the BDD package from time to time.  My implementation is
  not very optimized, but I wanted to avoid complex testing issues.
*/
#define USE_CARBON_MEM_MANAGER 1
#if USE_CARBON_MEM_MANAGER 
#include "util/c_memmanager.h"
#endif


extern void exit(int);

/* Amount of memory allocated */

static SIZE_T block_allocation;


/* mem_copy(dest, src, size) copies a block of memory. */

void
mem_copy(pointer dest, const pointer src, SIZE_T size)
{
  MEM_COPY(dest, src, size);
}


/* mem_zero(ptr, size) zeros a block of memory. */

void
mem_zero(pointer ptr, SIZE_T size)
{
  MEM_ZERO(ptr, size);
}


/* mem_fatal(message) prints an error message and exits. */

void
mem_fatal(char *message)
{
  fprintf(stderr, "Memory management library: error: %s\n", message);
  exit(1);
}


SIZE_T
mem_allocation(void)

{
  /* This will always returns zero when we're using malloc and free, */
  /* but you can maybe change it depending on your system. */
  return (block_allocation);
}


char *
mem_version(void)
{
  return ("1.0");
}

/* This code used if we're using malloc and free. */

pointer
mem_get_block(SIZE_T size)
{
  pointer result;

  if (size <= 0)
    return ((pointer)0);
#if USE_CARBON_MEM_MANAGER 
  result=carbonmem_malloc(size);
#else
  result=malloc(size);
  if (!result)
    mem_fatal("mem_get_block: allocation failed");
#endif
  return (result);
}


void
mem_free_block(pointer p)
{
#if USE_CARBON_MEM_MANAGER 
  carbonmem_free(p);
#else
  if (!p)
    return;
  free(p);
#endif
}


pointer
mem_resize_block(pointer p, SIZE_T new_size)
{
#if USE_CARBON_MEM_MANAGER 
  return carbonmem_realloc(p, new_size);
#else
  if (!p)
    return (mem_get_block(new_size));
  if (new_size <= 0)
    {
      mem_free_block(p);
      return ((pointer)0);
    }
  return (realloc(p, new_size));
#endif
}
