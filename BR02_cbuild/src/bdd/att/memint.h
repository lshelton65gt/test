/* Memory management internal definitions */


#if !defined(_MEMINTH)
#define _MEMINTH


/* All user-visible stuff */

#include "memuser.h"


/* >>> Potentially system dependent configuration stuff */
/* See memuser.h as well. */


#define MEM_COPY(dest, src, size) (void)memcpy((dest), (src), (size))
#define MEM_ZERO(ptr, size) (void)memset((ptr), 0, (size))

#define ARGS(args) args

/* System independent stuff here. */

struct segment_
{
  pointer base_address;
  SIZE_T limit;
};

typedef struct segment_ *segment;


struct block_
{
  int used;
  int size_index;
  struct block_ *next;
  struct block_ *prev;
  segment seg;
};

typedef struct block_ *block;


#define HEADER_SIZE ((SIZE_T)ROUNDUP(sizeof(struct block_)))
#define MAX_SIZE_INDEX (8*sizeof(SIZE_T)-2)
#define MAX_SEG_SIZE ((SIZE_T)1 << MAX_SIZE_INDEX)
#define MAX_SIZE ((SIZE_T)(MAX_SEG_SIZE-HEADER_SIZE))
#define MIN_ALLOC_SIZE_INDEX 15

#define NICE_BLOCK_SIZE ((SIZE_T)4096-ROUNDUP(sizeof(struct block_)))


extern void mem_fatal ARGS((char *));


#undef ARGS

#endif
