// -*- C++ -*-

/******************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
/*!
 * \file
 * Prescan expressions to determine if they are likely to be reducible
 * via BDDs.  This is valuable to do to avoid BDD blow-up on expressions
 * like a32==b32 or |(a32 & b32).
 */

#include "bdd/BDDPrescan.h"

// BDDs will fail if the cost is too high.  Arbitrarily, let's say that
// if there are >64 variables referenced in an expression, then we will
// not attempt to compute a BDD for that expresson.  Also the BDDPrescan
// package itself will blow up merging all the mRefs through the expr
// hierarchy
#define MAX_NUM_NETS 64

BDDPrescan::ExprInfo::ExprInfo(NUNetRefFactory* nrf)
  : mInversions(0),
    mBitsRefedMoreThanOnce(false),
    mHasConstants(false),
    mTooManyNets(false),
    mRefs(nrf)
{
}

void BDDPrescan::ExprInfo::addInversion() {
  ++mInversions;
}

void BDDPrescan::ExprInfo::addNet(NUNet* net) {
  if (!mTooManyNets) {
    if (!mBitsRefedMoreThanOnce) {
      DynBitVecDesc oldBits;
      mBitsRefedMoreThanOnce = mRefs.findNet(net, &oldBits);
    }
    DynBitVectorFactory* bfactory = mRefs.getFactory()->getDynBitVectorFactory();
    DynBitVecDesc newBits(0, net->getBitSize(), bfactory);
    mRefs.insert(net, newBits);
    mTooManyNets = mRefs.size() > MAX_NUM_NETS;
  }
}

void BDDPrescan::ExprInfo::addNetRange(NUNet* net, const ConstantRange& range)
{
  if (!mTooManyNets) {
    DynBitVectorFactory* bfactory = mRefs.getFactory()->getDynBitVectorFactory();
    DynBitVecDesc newBits(range.getLsb(), range.getLength(), bfactory);

    if (!mBitsRefedMoreThanOnce) {
      DynBitVecDesc oldBits;
      if (mRefs.findNet(net, &oldBits)) {
        mBitsRefedMoreThanOnce = newBits.anyCommonBitsSet(oldBits,
                                                          net->getBitSize());
      }
    }
    mRefs.insert(net, newBits);
    mTooManyNets = mRefs.size() > MAX_NUM_NETS;
  }
}

void BDDPrescan::ExprInfo::addSubExpr(const ExprInfo* subInfo) {
  mTooManyNets |= subInfo->mTooManyNets;
  if (!mTooManyNets) {
    mInversions += subInfo->mInversions;
    mBitsRefedMoreThanOnce |= subInfo->mBitsRefedMoreThanOnce;
    mHasConstants |= subInfo->mHasConstants;

    if (!isBDDPromising()) {
      mBitsRefedMoreThanOnce =
        NUNetRefHashSet::set_has_intersection(mRefs, subInfo->mRefs);
      mRefs.insert(subInfo->mRefs);
      mTooManyNets = mRefs.size() > MAX_NUM_NETS;
    }
  }
}

bool BDDPrescan::ExprInfo::isBDDPromising() const {
  return !mTooManyNets &&
    ((mInversions > 1) || mBitsRefedMoreThanOnce || mHasConstants);
}


BDDPrescan::BDDPrescan(NUNetRefFactory* netRefFactory) 
  : mNetRefFactory(netRefFactory),
    mDepth(0),
    mCurrentBDDPrescan(NULL)
{
}
BDDPrescan::~BDDPrescan() {
  clear();
}

NUDesignCallback::Status BDDPrescan::operator()(Phase, NUBase*) {return eNormal;}

//! manage the NUExpr*->ExprInfo map as we descend into the expr tree
NUDesignCallback::Status BDDPrescan::pushPop(Phase phase, NUExpr* expr) {
  if (phase == ePre) {
    ExprInfo* info = mInfoMap[expr];
    if (info != NULL) {
      // We already computed the prescan information for this expression,
      // so no need to dive into sub-expressions.  This is required to
      // avoid n^2 behavior when Fold recurses into expressions, and
      // also to take advantage of expression-sharing when used on
      // a factory expression
      if (mCurrentBDDPrescan != NULL) {
        mCurrentBDDPrescan->addSubExpr(info);
      }
      return eSkip;
    }
    info = new ExprInfo(mNetRefFactory);
    mInfoMap[expr] = info;
    mInfoArray.push_back(info);
    mCurrentBDDPrescan = info;
    ++mDepth;
    NU_ASSERT(mInfoArray.size() == mDepth, expr);
  }
  else {
    NU_ASSERT(mInfoArray.size() == mDepth, expr);
    ExprInfo* info = mInfoArray.back();
    mInfoArray.pop_back();
    --mDepth;
    if (mDepth == 0) {
      mCurrentBDDPrescan = NULL;
    }
    else {
      mCurrentBDDPrescan = mInfoArray.back();
      mCurrentBDDPrescan->addSubExpr(info);
    }
  }
  return eNormal;
}

NUDesignCallback::Status BDDPrescan::operator()(Phase phase, NUExpr* expr) {
  if (pushPop(phase, expr) == eSkip) {
    return eSkip;
  }
  return eNormal;
}

NUDesignCallback::Status BDDPrescan::operator()(Phase phase, NUConst* expr) {
  if (pushPop(phase, expr) == eSkip) {
    return eSkip;
  }
  if (phase == ePre) {
    mCurrentBDDPrescan->putHasConstants(true);
  }
  return eNormal;
}

NUDesignCallback::Status BDDPrescan::operator()(Phase phase, NUBinaryOp* expr) {
  if (pushPop(phase, expr) == eSkip) {
    return eSkip;
  }
  if (phase == ePre) {
    switch (((NUUnaryOp*)expr)->getOp()) {
    case NUOp::eBiNeq:
      mCurrentBDDPrescan->addInversion();
      break;
    default:
      break;
    }
  }
  return eNormal;
}

NUDesignCallback::Status BDDPrescan::operator()(Phase phase, NUUnaryOp* expr) {
  if (pushPop(phase, expr) == eSkip) {
    return eSkip;
  }
  if (phase == ePre) {
    switch (((NUUnaryOp*)expr)->getOp()) {
    case NUOp::eUnLogNot:
    case NUOp::eUnBitNeg:
    case NUOp::eUnVhdlNot:
      mCurrentBDDPrescan->addInversion();
      break;
    default:
      break;
    }
  }
  return eNormal;
}

NUDesignCallback::Status BDDPrescan::operator()(Phase phase, NUIdentRvalue* expr) {
  if (pushPop(phase, expr) == eSkip) {
    return eSkip;
  }
  if (phase == ePre) {
//      NUNetElab* netElab = expr->getNetElab(NULL);
//      if (netElab == NULL)
    {
      mCurrentBDDPrescan->addNet(expr->getIdent());
    }
//      else {
//        info->addNetElab(netElab);
//      }
  }
  return eNormal;
}

NUDesignCallback::Status BDDPrescan::operator()(Phase phase,
                                                NUVarselRvalue* expr)
{
  if (pushPop(phase, expr) == eSkip) {
    return eSkip;
  }
  if (phase == ePre) {
    if (expr->isConstIndex() && expr->getIdentExpr()->isWholeIdentifier()) {
      const ConstantRange* range = expr->getRange();
      mCurrentBDDPrescan->addNetRange(expr->getIdent(), *range);
      // we are giving up, so call pushPop with ePost to make it look
      // like we are exiting cleanly (so depth is maintained properly)
      pushPop(ePost, expr);
      return eSkip;         // do not descend into the identifier
    }
  }
  return eNormal;
}

//! public accessor function to allow callers to determine an
//! expressions viability for BDD analysis
bool BDDPrescan::isBDDPromising(const NUExpr* expr) {
  NUDesignWalker walker(*this, false);
  walker.expr(const_cast<NUExpr*>(expr));
  ExprInfoMap::const_iterator p = mInfoMap.find(const_cast<NUExpr*>(expr));
  NU_ASSERT(p != mInfoMap.end(), expr);
  ExprInfo* info = p->second;
  return info->isBDDPromising();
}

void BDDPrescan::clear() {
  INFO_ASSERT(mDepth == 0, "BDDPrescan::clear sanity");
  mInfoMap.clearPointerValues();
}

void BDDPrescan::ExprInfo::print() {
  UtIO::cout () << "  BDDPrescan::exprInfo: mInversions: " << mInversions << ", mBitsRefedMoreThanOnce: " << mBitsRefedMoreThanOnce << ", mHasConstants: " << mHasConstants << ", mTooManyNets: " << mTooManyNets << ", mRefs.size(): " << mRefs.size() << "\n";
  return;
}

void BDDPrescan::print(const NUExpr* expr, bool withExpression) {
  ExprInfoMap::const_iterator p = mInfoMap.find(const_cast<NUExpr*>(expr));
  NU_ASSERT(p != mInfoMap.end(), expr);
  ExprInfo* info = p->second;
  UtString buf;
  expr->getLoc().compose(&buf);
  if ( withExpression ){
    buf << " ";
    expr->compose(&buf, NULL);
  }
  UtIO::cout() << buf << " ";
  info->print();
  return;
}
