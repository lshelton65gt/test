#ifndef __CARBONPROJECTTREENODE_H__
#define __CARBONPROJECTTREENODE_H__
#include "util/CarbonPlatform.h"

#include <QTreeWidget>
#include <QDragEnterEvent>

class CarbonProjectWidget;

class CarbonProjectTreeNode : public QTreeWidgetItem
{
public:
  CarbonProjectTreeNode(CarbonProjectWidget* parent) : QTreeWidgetItem((QTreeWidget*)parent)
  {
    mProjectWidget=parent;
  }

  CarbonProjectTreeNode(CarbonProjectWidget* tree, QTreeWidgetItem* parent) : QTreeWidgetItem(parent)
  {
    mProjectWidget=tree;
  }
  virtual ~CarbonProjectTreeNode() {}

  virtual void showContextMenu(const QPoint&) {}
  virtual void setOpenIcon() {}
  virtual void setClosedIcon() {}
  virtual void doubleClicked(int) {}
  virtual void singleClicked(int) {}
  virtual void keyPressEvent(QKeyEvent*) {}
  virtual bool dragBeginEvent(QMouseEvent*) { return false; }
  virtual bool canDropItem(CarbonProjectTreeNode*) { return false; }
  virtual bool dropItem(CarbonProjectTreeNode*) { return true; }

protected:
  CarbonProjectWidget* mProjectWidget;
};

#endif
