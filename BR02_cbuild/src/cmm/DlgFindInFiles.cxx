//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/OSWrapper.h"
#include "gui/CQt.h"
#include "DlgFindInFiles.h"
#include "cmm.h"
#include "DesignXML.h"
#include "CarbonComponent.h"
#include "CarbonMakerContext.h"  //new
#include "CarbonProjectWidget.h"  //new
#include "CarbonConsole.h"
#include "WorkspaceModelMaker.h"
#include "CarbonSourceGroups.h"
#include "CarbonHDLSourceFile.h"

#define WILDCARDS "Wildcards"


#define NUM_USER_DIRS "findInFiles/numDirectores"
#define USER_DIR_PREFIX "findInFiles/userDirectory"

#define NUM_USER_STRINGS "findInFiles/numStrings"
#define USER_STRING_PREFIX "findInFiles/userString"

#define LAST_LOOK_IN "findInFiles/lastLookIn"
#define LAST_STRING "findInFiles/lastString"
#define LAST_WHOLE_WORD "findInFiles/matchWholeWord"
#define LAST_MATCH_CASE "findInFiles/matchCase"
#define LAST_FILEFILTER "findInFiles/fileFilter"
#define LAST_USE "findInFiles/use"
#define LAST_USE_KIND "findInFiles/useKind"
#define LAST_CLEAR "findInFiles/lastClear"

DlgFindInFiles::DlgFindInFiles(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);

  ui.comboBoxLookIn->blockSignals(true);

  // Add special items here
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  if (pw && pw->project())
  {
    ui.comboBoxLookIn->addItem(SEARCH_PROJECT, (int)eSearchProject);
    ui.comboBoxLookIn->addItem(SEARCH_INCLUDES, (int)eSearchIncludes);
  }

  mDesignHierarchy = NULL;

  UtString cwd;
  OSGetCurrentDir(&cwd);

  QFileInfo fi(cwd.c_str());
  ui.comboBoxLookIn->addItem(fi.absolutePath(), (int)eSearchDirectory);

  // add user defined options here.
  int numDirectories = mSettings.value(NUM_USER_DIRS, 0).toInt();
  for (int i=0; i<numDirectories; i++)
  {
    QString keyName = QString("%1%2")
      .arg(USER_DIR_PREFIX)
      .arg(i);
    QString userDir = mSettings.value(keyName, "").toString();
    if (!userDir.isEmpty())
      ui.comboBoxLookIn->addItem(userDir, (int)eSearchDirectory);
  }

  int numStrings = mSettings.value(NUM_USER_STRINGS, 0).toInt();
  for (int i=0; i<numStrings; i++)
  {
    QString keyName = QString("%1%2").arg(USER_STRING_PREFIX).arg(i);
    QString userString = mSettings.value(keyName, "").toString();
    ui.comboBoxWhat->addItem(userString);
  }

  bool lastMatchWholeWord = mSettings.value(LAST_WHOLE_WORD, false).toBool();
  ui.checkBoxMatchWholeWord->setChecked(lastMatchWholeWord);
 

  bool lastClear = mSettings.value(LAST_CLEAR, true).toBool();
  ui.checkBoxClear->setChecked(lastClear);

  bool lastMatchCase = mSettings.value(LAST_MATCH_CASE, false).toBool();
  ui.checkBoxMatchCase->setChecked(lastMatchCase);

  bool lastUse = mSettings.value(LAST_USE, false).toBool();
  ui.checkBoxUse->setChecked(lastUse);

  QString lastUseKind = mSettings.value(LAST_USE_KIND, "").toString();
  if (!lastUseKind.isEmpty())
  {
    int index = ui.comboBoxUse->findText(lastUseKind);
    if (index != -1)
      ui.comboBoxUse->setCurrentIndex(index);
  }
 
  QString lastString = mSettings.value(LAST_STRING, "").toString();
  if (!lastString.isEmpty())
  {
    int index = ui.comboBoxWhat->findText(lastString);
    if (index != -1)
      ui.comboBoxWhat->setCurrentIndex(index);
  }
  
  if (!theApp->getSelectedText().isEmpty())
  {
    int index = ui.comboBoxWhat->findText(theApp->getSelectedText());
    if (index == -1)
    {
      ui.comboBoxWhat->addItem(theApp->getSelectedText());
      index = ui.comboBoxWhat->findText(theApp->getSelectedText());
    }
    ui.comboBoxWhat->setCurrentIndex(index);
  }

  QString lastLookIn = mSettings.value(LAST_LOOK_IN, "").toString();
  if (!lastLookIn.isEmpty())
  {
    int index = ui.comboBoxLookIn->findText(lastLookIn);
    if (index != -1)
      ui.comboBoxLookIn->setCurrentIndex(index);
  }

  QLineEdit* lineEdit = ui.comboBoxWhat->lineEdit();
  if (lineEdit)
    lineEdit->selectAll();


  QString lastFileFilter = mSettings.value(LAST_FILEFILTER, "").toString();
  if (!lastFileFilter.isEmpty())
  {
    int index = ui.comboBoxFileTypes->findText(lastFileFilter);
    if (index == -1)
      ui.comboBoxFileTypes->addItem(lastFileFilter);
    ui.comboBoxFileTypes->setCurrentIndex(ui.comboBoxFileTypes->findText(lastFileFilter));
  }

  if ( !( (ui.comboBoxLookIn->currentText() == SEARCH_PROJECT) || (ui.comboBoxLookIn->currentText() == SEARCH_INCLUDES) ) )
    ui.checkBoxIncludeSubfolders->setEnabled(true);

  ui.comboBoxLookIn->blockSignals(false);

  CQT_CONNECT(&mWorkerThread, matchFound(const QString&,int, const QString&), this, matchFound(const QString&,int, const QString&));
  CQT_CONNECT(&mWorkerThread, searchCompleted(int,int,int), this, searchCompleted(int,int,int));
  CQT_CONNECT(&mWorkerThread, updateStatus(const QString&), this, updateStatus(const QString&));
}

void DlgFindInFiles::on_comboBoxLookIn_currentIndexChanged(int)
{ 
}

void DlgFindInFiles::on_checkBoxUse_stateChanged(int newState)
{
  ui.comboBoxUse->setEnabled(newState == Qt::Checked);
}


void DlgFindInFiles::on_comboBoxLookIn_editTextChanged(const QString& newValue)
{
  ui.checkBoxIncludeSubfolders->setEnabled(! (newValue == SEARCH_PROJECT) || (newValue == SEARCH_INCLUDES) );
  ui.pushButtonFindAll->setEnabled(!newValue.isEmpty());
}

void DlgFindInFiles::on_toolButtonBrowse_clicked()
{
 QString dir = QFileDialog::getExistingDirectory(this, tr("Choose Directory"),
                                                     ".",
                                                     QFileDialog::ShowDirsOnly
                                                     | QFileDialog::DontResolveSymlinks);
 if (!dir.isEmpty())
 {
   ui.comboBoxLookIn->addItem(dir, eSearchDirectory);
   ui.comboBoxLookIn->setCurrentIndex(ui.comboBoxLookIn->findText(dir));
 }
}

void DlgFindInFiles::updateStatus(const QString& file)
{
  QString msg = QString("Searching %1...").arg(file);

  QStatusBar* statusBar = theApp->getContext()->getWorkspaceModelMaker()->getStatusBar();
  statusBar->showMessage(msg);
}

void DlgFindInFiles::searchCompleted(int numFiles, int numMatches, int totalFiles)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonConsole* console = pw->getConsole();
  UtString msg;
  msg << "Matching lines: " << numMatches << " Matching files: " << numFiles << " Total files searched: " << totalFiles;

  console->processOutput(CarbonConsole::Local, CarbonConsole::Check, msg.c_str());

  QStatusBar* statusBar = theApp->getContext()->getWorkspaceModelMaker()->getStatusBar();
  statusBar->clearMessage();
}

void DlgFindInFiles::searchStarted(QString key, QString location, bool subfolders)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonConsole* console = pw->getConsole();
  UtString msg;
  msg << "Find all \"" << key << "\", ";
  if (subfolders)
    msg << "Subfolders, ";
  msg << "\"" << location << "\"";

  console->processOutput(CarbonConsole::Local, CarbonConsole::Check, msg.c_str());
}


void DlgFindInFiles::matchFound(const QString& filePath, int lineNumber, const QString& line)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonConsole* console = pw->getConsole();
  UtString msg;
  msg << filePath << ":" << lineNumber << " " << line;

  console->processOutput(CarbonConsole::Local, CarbonConsole::Check, msg.c_str());
}


DlgFindInFiles::~DlgFindInFiles()
{
}

bool SearchThread::binaryText(const QString& line)
{
  for (int i=0; i<line.length(); i++)
  {
    QChar ch = line[i];
    if (!ch.isSpace() && ch.category() == QChar::Other_Control)
      return true;
  }
  return false;
}
int SearchThread::searchFile(const QString& filePath, const QString& stringToMatch, 
                             QRegExp::PatternSyntax pat, Qt::CaseSensitivity caseSense, bool matchWholeWord)
{
  int numMatches = 0;
  QFile file(filePath);
  if (file.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    QTextStream in(&file);
    QString line = in.readLine();
    int lineNumber = 1;
    emit updateStatus(filePath);
    while (!line.isNull())
    {
      QString searchString = stringToMatch;
      //search line for keyword here, output if found
      if (matchWholeWord)
      {
        pat = QRegExp::RegExp;
        searchString = "\\b" + stringToMatch + "\\b";
      }
      QRegExp reg(searchString, caseSense, pat);
      int pos = reg.indexIn(line);
      if (pos != -1 && !binaryText(line))
      {
        emit matchFound(filePath, lineNumber, line);
        numMatches++;
      }
      line = in.readLine();
      lineNumber++;
    }

    file.close();
  }
  return numMatches;
}

void SearchThread::search_files(const QStringList& fileList, const QStringList& fileKinds, int *numFiles, int *numMatches, int *totalFiles,
                                    QString stringToMatch,
                                    QRegExp::PatternSyntax pat, Qt::CaseSensitivity caseSense, bool matchWholeWord)
{
  foreach (QString fileName, fileList)
  {
    QFileInfo fi(fileName);
    QString extension;
    if (fi.completeSuffix().length() > 0)
      extension = "." + fi.completeSuffix();

    bool matchFound = false;
#if pfWINDOWS
    Qt::CaseSensitivity caseFileSense = Qt::CaseInsensitive;
#else
    Qt::CaseSensitivity caseFileSense = Qt::CaseSensitive;
#endif

    foreach (QString fileKind, fileKinds)
    {
      QRegExp rx(fileKind, caseFileSense, QRegExp::Wildcard);
      if (rx.exactMatch(extension))
      {
        matchFound = true;
        break;
      }
    }
    if (matchFound)
    {
      int matches = searchFile(fileName, stringToMatch, pat, caseSense, matchWholeWord);
      if (matches)
       *numFiles += 1;
      *numMatches += matches;
      *totalFiles += 1;
    }
  }
}

void SearchThread::search_directory(int *numFiles, int *numMatches, int *totalFiles,
                                    QString stringToMatch, QDir& dir, const QStringList& fileKinds,
                                    bool recurseFlag, QRegExp::PatternSyntax pat, Qt::CaseSensitivity caseSense, bool matchWholeWord)
{
  if (dir.exists())
  {
    dir.setFilter(QDir::Files);
    dir.setNameFilters(fileKinds);

    foreach(QFileInfo fileInfo, dir.entryInfoList())
    {
      *totalFiles += 1;
      int matches = searchFile(fileInfo.filePath(), stringToMatch, pat, caseSense, matchWholeWord);
      if (matches)
        *numFiles += 1;
      *numMatches += matches;
    }

    if (recurseFlag)
    {
      QDir subDir(dir.path());
      subDir.setFilter(QDir::Dirs);
      foreach (QFileInfo fileInfo, subDir.entryInfoList())
      {      
        if (fileInfo.fileName() != "." && fileInfo.fileName() != "..")
        {
          QDir rdir(fileInfo.filePath());
          search_directory(numFiles, numMatches, totalFiles, stringToMatch, rdir, fileKinds, recurseFlag, pat, caseSense, matchWholeWord);
        }
      }
    }
   }
}
void DlgFindInFiles::closeEvent(QCloseEvent* ev)
{
  ev->accept();
  theApp->resetFindInFilesDialog();
}

void DlgFindInFiles::saveStrings(const QString& searchString)
{
  if (ui.comboBoxWhat->findText(searchString) == -1)
    ui.comboBoxWhat->insertItem(0, searchString);

  int numStrings = 0;
  for (int i=0; i<ui.comboBoxWhat->count(); i++)
  {
    QString txt = ui.comboBoxWhat->itemText(i);
    QString keyName = QString("%1%2").arg(USER_STRING_PREFIX).arg(numStrings);
    mSettings.setValue(keyName, txt);
    numStrings++;
  }

  mSettings.setValue(NUM_USER_STRINGS, numStrings);
}


// Check the combo-box entries, if there is only one
// entry matching, then add a new entry for this dir
void DlgFindInFiles::saveDirectory(const QString& dirName)
{
  UtString cwd;
  OSGetCurrentDir(&cwd);
  QFileInfo fi(cwd.c_str());

  int numMatches = 0;
  for (int i=0; i<ui.comboBoxLookIn->count(); i++)
  {
    QString txt = ui.comboBoxLookIn->itemText(i);
    QVariant v = ui.comboBoxLookIn->itemData(i);
    SearchKind kind = static_cast<SearchKind>(v.toInt());
    if (kind == eSearchDirectory && txt == dirName)
      numMatches++;
  }

  if (numMatches <= 1)
  {
    if (dirName != fi.absolutePath())
      ui.comboBoxLookIn->addItem(dirName, (int)eSearchDirectory);
  }

  // Walk the combo-box and create entries 
  int numDirectories = 0;
  for (int i=0; i<ui.comboBoxLookIn->count(); i++)
  {
    QString txt = ui.comboBoxLookIn->itemText(i);
    QVariant v = ui.comboBoxLookIn->itemData(i);
    SearchKind kind = static_cast<SearchKind>(v.toInt());

    // Don't save the current directory entry
    if (kind == eSearchDirectory && txt != fi.absolutePath())
    {
      QString keyName = QString("%1%2").arg(USER_DIR_PREFIX).arg(numDirectories);
      mSettings.setValue(keyName, txt);
      numDirectories++;
    }
  }
  mSettings.setValue(NUM_USER_DIRS, numDirectories);
}

// search the -y and +incdir+ locations
void DlgFindInFiles::searchIncludes(const QString& stringToMatch, const QStringList& fileKinds)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  if (pw && pw->project())
  {
    QStringList dirs;
    CarbonProject* proj = pw->project();
    CarbonOptions* options = proj->getActive()->getOptions("VSPCompiler");
    CarbonPropertyValue* includeDirs = options->getValue("-y");
    if (includeDirs)
    {
      QString values = includeDirs->getValue();
      if (!values.isEmpty())
      {
        foreach (QString location, values.split(";"))
        {
          UtString path;
          path << location;
          QString filePath = proj->getWindowsEquivalentPath(path.c_str());
          dirs << filePath;
        }
      }
    }

    QString incDirs = options->getValue("+incdir+")->getValue();
    if (!incDirs.isEmpty())
    {
      foreach (QString location, incDirs.split(";"))
      {
        UtString path;
        path << location;
        QString filePath = proj->getWindowsEquivalentPath(path.c_str());
        dirs << filePath;
      }
    }

    if (dirs.count() > 0)
    {
      mWorkerThread.search(
          ui.comboBoxUse->currentText() == WILDCARDS ? QRegExp::Wildcard : QRegExp::RegExp,
          ui.checkBoxMatchCase->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive,
          ui.checkBoxIncludeSubfolders->isChecked(),
          dirs, 
          stringToMatch, fileKinds, 
          ui.checkBoxMatchWholeWord->isChecked());
    }
  }
}


void DlgFindInFiles::searchProject(const QString& stringToMatch, const QStringList& fileKinds)
{
  if (mDesignHierarchy)
  {
    delete mDesignHierarchy;
    mDesignHierarchy = NULL;
  }

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  if (pw && pw->project())
  {
    CarbonProject* proj = pw->project();
    const char* designHierarchyFile = proj->getDesignFilePath(".designHierarchy");
    QFileInfo fi(designHierarchyFile);
    if (!fi.exists())
    {
      QMessageBox::warning(this, 
        MODELSTUDIO_TITLE, "The design has not yet been compiled.\nThe set of files searched may be reduced.\nIn order to search the entire design, first compile it or use the explore hierarchy command.");
    }

    mDesignHierarchy = new XDesignHierarchy(designHierarchyFile);
    XmlErrorHandler* eh = mDesignHierarchy->getErrorHandler();      
    if (eh->hasErrors())
    {
      QString msg = QString("Error reading design hierarchy: %1\n%2")
        .arg(designHierarchyFile)
        .arg(eh->errorMessages().join("\n"));
      QMessageBox::critical(NULL, MODELSTUDIO_TITLE, msg);
      return;
    }

    QStringList fileList;
    XDesignFiles* designFiles = mDesignHierarchy->getDesignFiles();
    for (int i=0; i<designFiles->numFiles(); i++)
    {
      XDesignFile* file = designFiles->getAt(i);
      QString fileName = file->getName();

      UtString filename;
      filename << fileName;

      QString fname = proj->getWindowsEquivalentPath(filename.c_str());
      fileList.push_back(fname);
    }

    // Add component files here:
    CarbonComponents* comps = proj->getComponents();
    for (UInt32 i=0; i<comps->numComponents(); i++)
    {
      CarbonComponent* comp = comps->getComponent(i);
      comp->getGeneratedFiles(fileList);
    }

    // Add design files
    const char* iodbName = proj->getDesignFilePath(".symtab.db");
    QFileInfo fiIodb(iodbName);
    QString baseName = fiIodb.baseName();
    CarbonConfiguration* config = proj->getActive();
    if (config)
    {
      QString designFiles = QString("%1.*").arg(baseName);
      QDir designDir(config->getOutputDirectory());
      designDir.setFilter(QDir::Files);
      QStringList filters;
      filters << designFiles;
      designDir.setNameFilters(filters);

      foreach(QFileInfo fileInfo, designDir.entryInfoList())
      {
        if (!fileInfo.isDir())
        {
          // skip binary files.
          QString ext = fileInfo.completeSuffix().toLower();
          if (ext == "a" || ext == "lib" || ext == "so" || 
              ext == "dll" || ext == "io.db" || ext == "symtab.db" || ext == "drivers"
              )
              continue;

          qDebug() << "searching" << fileInfo.filePath();
          fileList.push_back(fileInfo.filePath());
        }
      }
    }

    // Add directives and command files as well
    QString directiveFiles = proj->getActive()->getOptions("VSPCompiler")->getValue("-directive")->getValue();
    if (!directiveFiles.isEmpty())
    {
      foreach (QString file, directiveFiles.split(";"))
      {
        UtString name;
        name << proj->resolveFile(file);;
        QString localizedPath = proj->getWindowsEquivalentPath(name.c_str());
        QFileInfo fi(localizedPath);
        QString absName = fi.absoluteFilePath();
        if (!fileList.contains(absName))
          fileList.push_back(absName);
      }
    }

    QString commandFiles = proj->getActive()->getOptions("VSPCompiler")->getValue("-f")->getValue();
    if (!commandFiles.isEmpty())
    {
      foreach (QString file, commandFiles.split(";"))
      {
        UtString name;
        name << proj->resolveFile(file);;
        QString localizedPath = proj->getWindowsEquivalentPath(name.c_str());
        QFileInfo fi(localizedPath);
        QString absName = fi.absoluteFilePath();
        if (!fileList.contains(absName))
          fileList.push_back(absName);
      }
    }

    for (int i=0; i<proj->getFiles()->numFiles(); ++i)
    {
      CarbonFile* file = proj->getFiles()->getFile(i);
      UtString fileName;
      fileName << file->getFilepath();
      UtString name;
      name << proj->getWindowsEquivalentPath(fileName.c_str());
      QFileInfo fi2(name.c_str());
      QString normalizedPath = fi2.absoluteFilePath();
      if (!fileList.contains(normalizedPath))
        fileList.push_back(normalizedPath);
    }
  
    // add the project listed files here as well
       // Files, and file-specifig switches next
    CarbonSourceGroups* sourceGroups = proj->getGroups();
    for (int i=0; i<sourceGroups->numGroups(); ++i)
    {
      CarbonSourceGroup* group = sourceGroups->getGroup(i);
      for (int j=0; j<group->numSources(); j++)
      {
        CarbonHDLSourceFile* srcFile = group->getSource(j);
        UtString srcPath;
        QFileInfo sfi(srcFile->getRelativePath());
        QString fileName = srcFile->getRelativePath();

        UtString resolvedName;
        resolvedName << proj->resolveFile(fileName); 
        UtString name;
        name << proj->getWindowsEquivalentPath(resolvedName.c_str());

        QFileInfo fi2(name.c_str());
        QString normalizedPath = fi2.absoluteFilePath();
        if (!fileList.contains(normalizedPath))
          fileList.push_back(normalizedPath);
      }
    }

    if (fileList.count() > 0)
    {
      mWorkerThread.searchFiles(fileList, fileKinds,
          ui.comboBoxUse->currentText() == WILDCARDS ? QRegExp::Wildcard : QRegExp::RegExp,
          ui.checkBoxMatchCase->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive,
          stringToMatch, 
          ui.checkBoxMatchWholeWord->isChecked());
 
    }
  }
}
void DlgFindInFiles::on_pushButtonFindAll_clicked()
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  QDockWidget* dw = theApp->getContext()->getWorkspaceModelMaker()->getRemoteConsoleDockWindow();
  // lower followed by raise causes it to "pop" forward
  dw->setVisible(true);
  dw->lower();
  dw->raise();

  CarbonConsole* console = theApp->getContext()->getCarbonProjectWidget()->getConsole();
  if (ui.checkBoxClear->isChecked())
    console->clear();

  QVariant v = ui.comboBoxLookIn->itemData(ui.comboBoxLookIn->currentIndex());
  SearchKind kind = static_cast<SearchKind>(v.toInt());

  QString searchLocation = ui.comboBoxLookIn->currentText();
  UtString loc;
  loc << searchLocation;

  QString stringToMatch = ui.comboBoxWhat->currentText();

  mSettings.setValue(LAST_STRING, stringToMatch);
  mSettings.setValue(LAST_LOOK_IN, searchLocation);
  mSettings.setValue(LAST_FILEFILTER, ui.comboBoxFileTypes->currentText());
  mSettings.setValue(LAST_USE, (bool)ui.checkBoxUse->isChecked());
  mSettings.setValue(LAST_USE_KIND, ui.comboBoxUse->currentText());
  mSettings.setValue(LAST_WHOLE_WORD, (bool)ui.checkBoxMatchWholeWord->isChecked());
  mSettings.setValue(LAST_MATCH_CASE, (bool)ui.checkBoxMatchCase->isChecked());
  mSettings.setValue(LAST_CLEAR, (bool)ui.checkBoxClear->isChecked());

  QStringList fileKinds = ui.comboBoxFileTypes->currentText().split(';');

  saveStrings(stringToMatch);

  switch (kind)
  {
  case eSearchIncludes:
    qDebug() << "search includes";
    searchStarted(stringToMatch, "RTL Include Directories", true);
    searchIncludes(stringToMatch, fileKinds);
    break;
  case eSearchProject:
    qDebug() << "search project";
    searchStarted(stringToMatch, "Entire Project", true);
    searchProject(stringToMatch, fileKinds);
    //QDir dir(); //directory of current project
    //search_directory(stringToMatch, dir, fileKinds);
    break;
  case eSearchDirectory:
    {
      searchStarted(stringToMatch, searchLocation, ui.checkBoxIncludeSubfolders->isChecked());
      saveDirectory(searchLocation);
      QStringList dirs;
      dirs << searchLocation;
      mWorkerThread.search(
          ui.comboBoxUse->currentText() == WILDCARDS ? QRegExp::Wildcard : QRegExp::RegExp,
          ui.checkBoxMatchCase->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive,
          ui.checkBoxIncludeSubfolders->isChecked(),
          dirs, 
          stringToMatch, fileKinds, 
          ui.checkBoxMatchWholeWord->isChecked());
    }
    break;
  }

  QApplication::restoreOverrideCursor();

  close();
}


//QFile file("/proc/modules");
//     if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
//         return;
//
//     QTextStream in(&file);
//     QString line = in.readLine();
//     while (!line.isNull()) {
//         process_line(line);
//         line = in.readLine();
//     }


void SearchThread::searchFiles(const QStringList& fileList, const QStringList& fileKinds, QRegExp::PatternSyntax patSyntax,
                               Qt::CaseSensitivity caseSense,  const QString& searchString, bool matchWholeWord)
{
  QMutexLocker locker(&mMutex);

  this->mSearchKind = eSearchFiles;
  this->mSearchFiles = fileList;
  this->mPatSyntax = patSyntax;
  this->mSearchString = searchString;
  this->mCaseSense = caseSense;
  this->mMatchWholeWord = matchWholeWord;
  this->mFileKinds = fileKinds;

  if (!isRunning()) 
  {
    start(LowPriority);
  }
  else
  {
    mRestart = true;
    mCondition.wakeOne();
   }
}



void SearchThread::search(QRegExp::PatternSyntax patSyntax, Qt::CaseSensitivity caseSense, bool recurseFlag, const QStringList& dirs, const QString& searchString, const QStringList& fileKinds, bool matchWholeWord)
{
  QMutexLocker locker(&mMutex);

  this->mSearchKind = eSearchDirectory;
  this->mPatSyntax = patSyntax;
  this->mRecurseFlag = recurseFlag;
  this->mSearchString = searchString;
  this->mDirs = dirs;
  this->mCaseSense = caseSense;
  this->mFileKinds = fileKinds;
  this->mMatchWholeWord = matchWholeWord;

  if (!isRunning()) 
  {
    start(LowPriority);
  }
  else
  {
    mRestart = true;
    mCondition.wakeOne();
   }
}

void SearchThread::run()
{
  forever
  {
    mMutex.lock();
    SearchKind searchKind = this->mSearchKind;
    QRegExp::PatternSyntax patSyntax = this->mPatSyntax;
    Qt::CaseSensitivity caseSense = this->mCaseSense;
    bool recurseFlag = this->mRecurseFlag;
    const QStringList& dirs = this->mDirs;
    QString searchString = this->mSearchString;
    const QStringList& fileKinds = this->mFileKinds;
    bool matchWholeWord = this->mMatchWholeWord;
    const QStringList& files = this->mSearchFiles;
    mMutex.unlock();

    int numFiles = 0;
    int numMatches = 0;
    int totalFiles = 0;

    if (searchKind == eSearchDirectory)
    {
      foreach (QString dir, dirs)
      {
        QDir qdir(dir);

        this->search_directory(&numFiles, &numMatches, &totalFiles, searchString, qdir, 
          fileKinds, recurseFlag, patSyntax, caseSense, matchWholeWord);
      }
    }
    else if (searchKind == eSearchFiles)
    { 
      this->search_files(files, fileKinds, &numFiles, &numMatches, &totalFiles, searchString, patSyntax, caseSense, matchWholeWord);
    }
    
    emit searchCompleted(numFiles, numMatches, totalFiles);

    mMutex.lock();
    if (!mRestart)
      mCondition.wait(&mMutex);
    
    mRestart = false;
    mMutex.unlock();
  }
}
