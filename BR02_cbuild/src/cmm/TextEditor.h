#ifndef __TextEditor_H
#define __TextEditor_H
#include "util/CarbonPlatform.h"

#include <QTextEdit>
#include <QMenuBar>
#include <QSyntaxHighlighter>

#include <Qsci/qsciscintilla.h>
#include <Qsci/qscilexervhdl.h>

#include "qscilexerverilog.h"

#include "util/UtString.h"
#include "Mdi.h"
#include "MdiTextEditor.h"
#include "DlgFindText.h"

class MDIDocumentTemplate;




class TextEditor : public QsciScintilla, public MDIWidget
{
  Q_OBJECT

public:
  TextEditor(MDIDocumentTemplate* doct=0, QWidget* parent=0);

  virtual void saveDocument() { save(); }
  void gotoLine(UInt32 line);
  void fixedWidth();
  void newFile();
  bool loadFile(const char* fileName);
  bool save();
  bool saveAs();
  bool saveFile(const char* fileName);
  const char* userFriendlyCurrentFile();
  const char* currentFile() { return mCurFile.c_str(); }
  void updateMenusAndToolbars();

  QMenuBar* menuBar();

protected:
  void closeEvent(QCloseEvent *event);
  const char* userFriendlyName()
  {
    return mCurFile.c_str();
  }

  private slots:
    void documentWasModified();
    void cut() {QsciScintilla::cut();}
    void copy() {QsciScintilla::copy();}
    void paste() {QsciScintilla::paste();}
    void fileSave();
    void find();
    void updateUndoStatus();
    void updateRedoStatus();
    void findDialogDestroyed();

private:
  QSyntaxHighlighter* mHighlighter;

private:
  bool maybeSave();
  void setCurrentFile(const char* newVal);
  const char* strippedName(const char* name);
  void verilogSyntax();
  void vhdlSyntax();

  UtString mCurFile;
  bool isUntitled;
  QMenuBar* mMenu;
  UtArray<UInt32> mLinePositionVec;

  int mMarker;
  DlgFindText* mDlgFind;

signals:
  void pasteAvailable(bool);
  void undoAvailable(bool);
  void redoAvailable(bool);

};

#endif
