#ifndef __MDISystemC_H__
#define __MDISystemC_H__

#include "util/CarbonPlatform.h"

#include "gui/CQt.h"

#include <QMenuBar>
#include <QProgressBar>
#include <QComboBox>

#include "util/UtString.h"
#include "Mdi.h"
#include "MdiProject.h"
#include "CarbonMakerContext.h"
class MDIWidget;

class MDISystemCTemplate : public MDIDocumentTemplate
{
public:
  MDISystemCTemplate(CarbonMakerContext* ctx);
 
  virtual MDIWidget* createNewDocument(QWidget* parent=0);
  virtual int createMenus();
  virtual int createToolbars();
  virtual void updateMenusAndToolbars(QWidget* widget);
  virtual MDIWidget* openDocument(QWidget* parent, const char* docName);

private:
  CarbonMakerContext* mContext;
};
#endif
