//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "ModelKitUserWizard.h"
#include "CarbonOptions.h"
#include "ModelKit.h"
#include "MaxsimComponent.h"
#include "SystemCComponent.h"
#include "CowareComponent.h"

MKUWPageFinished::MKUWPageFinished(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);
  mStartedPost = false;
  mCompilationEnded = false;
  mCompilationPhase1 = true;
  mCompilationPhase2 = false;
  mInitialized = false;

  mProjectWidget = theApp->getContext()->getCarbonProjectWidget();
  mProject = mProjectWidget->project();
  mProjectDir = mProject->getProjectDirectory();
  mOutputDir = mProject->getActive()->getOutputDirectory();
  mPlatform = mProject->getActivePlatform();

  setTitle("Compilation");
  setSubTitle("The Carbon Model Kit is being compiled and prepared for use.");
 
  ui.progressBar->setVisible(false);
  ui.label->setText("");


  CQT_CONNECT(mProjectWidget->getConsole(),
    stdoutChanged(CarbonConsole::CommandType, const QString&, const QString&),
    this,
    stdoutChanged(CarbonConsole::CommandType, const QString&, const QString&));
 
  CQT_CONNECT(mProjectWidget->getConsole(),
    stderrChanged(CarbonConsole::CommandType, const QString&, const QString&),
    this,
    stdoutChanged(CarbonConsole::CommandType, const QString&, const QString&));
}

void MKUWPageFinished::stdoutChanged(CarbonConsole::CommandType, const QString& str, const QString&)
{

  //#  1  Elapsed: 0:00:00 Complete:  0% 
  QRegExp statx("#\\s*([0-9]+)\\s+Elapsed: [0-9]+:[0-9]+:[0-9]+ Complete:\\s+([0-9]+)%");
  QRegExp staty(".*Comp:([0-9]+)\\% Mem:.*");
  if (statx.exactMatch(str))
  {
    int phase=0;
    UtString cap1;
    cap1 << statx.cap(1);
    UtIStringStream Phase(cap1);
    Phase >> UtIO::dec >> phase;

    int percent=0;
    UtString cap2;
    cap2 << statx.cap(2);
    UtIStringStream Percent(cap2);
    Percent >> UtIO::dec >> percent;

    if (phase == 1 && percent == 0)
      setProgressRange(0, 125);

    setProgress(percent);
    setStatusMessage("Compiling Model");
    return;
  }
  else if (staty.exactMatch(str))
  {
    int percent = staty.cap(1).toInt();
    setProgress(percent);
    setStatusMessage("Compiling Model");
    return;
  }
  
  QString s = str;

  if (s.startsWith("FINISHED MODEL COMPILATION"))
    setStatusMessage("Compiling Component(s)");

  ui.textBrowser->append(str);
}

MKUWPageFinished::~MKUWPageFinished()
{
}

bool MKUWPageFinished::validatePage()
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());

  if (theApp->getContext()->getCanCreateModelKits())
    wiz->finishRecording();

  return true;
}

void MKUWPageFinished::removeDirectory(const QString& dir)
{
  UtString uDir; uDir << dir;
  UtString reason;

  QDir qd(dir);
  if (qd.exists())
  {
    if (0 != OSDeleteRecursive(uDir.c_str(), &reason))
      qDebug() << "Failed to remove directory" << dir;  
    else
      qDebug() << "Removed directory" << dir;
  }
}

// remove all files (not directories) from directory
void MKUWPageFinished::removeAllFilesFromDirectory(const QString& directory)
{
  QDir dir(directory);

  foreach (QFileInfo fi, dir.entryInfoList())
  {
    if (fi.isFile())
    {
      qDebug() << "Removing file" << fi.filePath();
      QFile::remove(fi.filePath());
    }
  }
}

void MKUWPageFinished::saveRetainedFiles(ModelKit* kit, const QString& retainDir)
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  KitUserContext* userContext = wiz->getUserContext();

  foreach (RetainedFile* rf, kit->getRetainedFiles())
  {
    qDebug() << "saving retained file" << rf->mSrcName << rf->mDestDir;

    QString srcName = userContext->expandName(rf->mSrcName);
    QFileInfo fi(srcName);
    QString dstName;

    if (rf->mDestDir.isEmpty())
      dstName = retainDir + "/" + fi.fileName();
    else
      dstName = retainDir + "/" + rf->mDestDir + "/" + fi.fileName();

    QFileInfo dfi(dstName);
    QDir qd(dfi.absolutePath());
    qd.mkpath(dfi.absolutePath());
   
    QFileInfo srcFi(srcName);
    if (srcFi.exists())
    {
      if (dfi.exists())
        QFile::remove(dstName);

      if (QFile::copy(srcName, dstName))
        qDebug() << "Copied file" << srcName << "to" << dstName;
      else
        qDebug() << "Error: Unable to copy file" << srcName << "to" << dstName;
    }
    
    QFileInfo dest(dstName);
    if (!dest.exists())
    {
      qDebug() << "Error Copying file" << srcName << "to" << dstName;
    }
  }
}

void MKUWPageFinished::addKnownFiles(ModelKit* kit, const QString&)
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  KitManifest* manifest = wiz->getManifest();
  KitUserContext* userContext = wiz->getUserContext();
  QString outputDir = mOutputDir;

  // keep retained files
  for (quint32 i=0; i<manifest->numFiles(); i++)
  {
    KitFile* kf = manifest->getFile(i);
    if (kf->getRetained())
    {
      qDebug() << "Retaining file" << kf->getNormalizedName();
      kit->retainFile(userContext->expandName(kf->getNormalizedName()));
    }
  }

  for (quint32 i=0; i<manifest->numUserFiles(); i++)
  {
    KitFile* kf = manifest->getUserFile(i);
    if (kf->getRetained())
    {
      qDebug() << "Retaining user file" << kf->getNormalizedName();
      kit->retainFile(userContext->expandName(kf->getNormalizedName()));
    }
  }

  // Keep the .a and .h
  if (userContext->getModelTarget() == ContextEnums::Windows)
  {
    kit->retainFile(mProject->getDesignFilePath(".a"));
    kit->retainFile(mProject->getDesignFilePath(".lib"));
    kit->retainFile(mProject->getDesignFilePath(".io.db"));
    kit->retainFile(mProject->getDesignFilePath(".symtab.db"));
    kit->retainFile(mProject->getDesignFilePath(".gui.db"));
    kit->retainFile(mProject->getDesignFilePath(".h"));
  }

  // SocDesigner Component?
  MaxsimComponent* maxsim = mProjectWidget->getActiveMaxsimComponent();
  if (maxsim)
  {
    QStringList genFiles;
    // Save windows files
    if (userContext->getModelTarget() == ContextEnums::Windows)
      maxsim->getGeneratedFiles(genFiles);

    maxsim->getComponentFiles(genFiles);
     
    QString subDirName = "SoCDesigner";
    if (maxsim->isStandaloneComponent())
      subDirName = "Standalone";

    qDebug() << "Retaining SoC Designer Files" << genFiles << "in" << subDirName;

    foreach (QString file, genFiles)
      kit->retainFile(file, subDirName);
  }
}

bool MKUWPageFinished::binaryText(const QString& line)
{
  for (int i=0; i<line.length(); i++)
  {
    QChar ch = line[i];
    if (!ch.isSpace() && ch.category() == QChar::Other_Control)
      return true;
  }
  return false;
}


int MKUWPageFinished::searchFile(const QString& filePath, const QString& stringToMatch, 
                             QRegExp::PatternSyntax pat, Qt::CaseSensitivity caseSense, bool matchWholeWord)
{
  int numMatches = 0;
  QFile file(filePath);
  if (file.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    QTextStream in(&file);
    QString line = in.readLine();
    int lineNumber = 1;

    while (!line.isNull())
    {
      QString searchString = stringToMatch;
      //search line for keyword here, output if found
      if (matchWholeWord)
      {
        pat = QRegExp::RegExp;
        searchString = "\\b" + stringToMatch + "\\b";
      }
      QRegExp reg(searchString, caseSense, pat);
      int pos = reg.indexIn(line);
      if (pos != -1 && !binaryText(line))
      {
        numMatches++;
      }
      line = in.readLine();
      lineNumber++;
    }

    file.close();
  }
  return numMatches;
}


// The Kit is finished, it's time to cleanup all the junk
// also, time to verify that we didn't build with -noFullDB and observeSignal *.*
//
bool MKUWPageFinished::cleanupFiles()
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  ModelKit* kit = wiz->getModelKit();

  setProgress(125);

  setStatusMessage("Finished");

  if (mProjectWidget->project() == NULL)
    return true;

  // Epilogue script?
  if (!kit->getEpilogueScriptNormalized().isEmpty())
  {
    qDebug() << "running epilogue script";
    kit->runKitScript(wiz, wiz->getUserContext(), ModelKit::Epilogue);
  }

  theApp->setSaveProjectOnShutdown(false);

  bool deleteFiles = true;
  if ((theApp->getContext()->getCanCreateModelKits() && getenv("CARBON_MODELKIT_RETAINFILES")) ||
      theApp->checkArgument("-modelKitProfileGenerate"))
    deleteFiles = false;

  QString projectDir = mProjectDir;
  QString retainDir = projectDir + "/data";

  qDebug() << "Cleanup ModelKit Files" << projectDir;

  //
  // Verify after the design is built that it had -noFullDB and no observeSignal *.*
  // unless CARBON_MODELKIT_DEBUG was set, or building on the portal (MODEL_REQUEST_DIR) and (!CARBON_CBUILD_ARGS)
  //
  if ( (getenv("CARBON_MODELKIT_DEBUG") == NULL) && ((getenv("MODEL_REQUEST_DIR") != NULL) && (getenv("CARBON_CBUILD_ARGS") == NULL) ) )
  {
    QString buildDir = QString("%1/Linux/Default").arg(mProjectDir);
    qDebug() << "Looking for lib*.cmd in" << buildDir;
    QDir qd(buildDir);
    qd.setFilter(QDir::Files);
    QStringList filters;
    filters << "lib*.cmd";
    filters << "lib*.dir";
    qd.setNameFilters(filters);
    foreach(QFileInfo fileInfo, qd.entryInfoList())
    {
      QString ext = fileInfo.completeSuffix();
      if (ext == "cmd") 
      {
        qDebug() << "Verify -noFullDB present in file:" << fileInfo.absoluteFilePath();
        int numMatches = searchFile(fileInfo.absoluteFilePath(), "^-noFullDB", QRegExp::RegExp, Qt::CaseSensitive, false);
        int numGuiDBMatches = searchFile(fileInfo.absoluteFilePath(), "^-writeGuiDB", QRegExp::RegExp, Qt::CaseSensitive, false);
        qDebug() << "found" << numMatches << "of noFullDB";
        if (numMatches > 0 || numGuiDBMatches > 0)
        {
          qDebug() << "PASS: Model was built with -noFullDB count: " << numMatches << " -writeGuiDB count: " << numGuiDBMatches;
        }
        else
        {
          qDebug() << "FAILED: Model was NOT built with -noFullDB and not built with -writeGuiDB";
          return false;
        }
      }
      else if (ext == "dir")
      {
        qDebug() << "Verify observeSignal *.* NOT present in file:" << fileInfo.absoluteFilePath();
        int numMatches = searchFile(fileInfo.absoluteFilePath(), "^observeSignal \\*.\\*", QRegExp::RegExp, Qt::CaseSensitive, false);
        qDebug() << "found" << numMatches << "of observeSignal *.*";
        if (numMatches > 0)
        {
          qDebug() << "FAILED: Model was built with observeSignal *.*";
          return false;
        }
        else
        {
          qDebug() << "PASS: Model was not built with observeSignal *.*";          
        }
      }
    }
  }

  // save the known files for retention
  addKnownFiles(kit, retainDir);

  // Create the retained dir "data"
  QDir qd(retainDir);
  qd.mkpath(retainDir);

  // save all retained files specified by user scripts
  saveRetainedFiles(kit, retainDir);

  if (deleteFiles)
  {
    // blow away cloned arm_logical
    QString armLogDir = projectDir + "/" + "ARM_Logical";
    removeDirectory(armLogDir);
   
    QString monDir = projectDir + "/" + ".monitor";
    removeDirectory(monDir);

    QString platDir = projectDir + "/" + mProject->getActivePlatform();
    removeDirectory(platDir);

    // Now, remove all extra files in the project dir
    // leaving the sub-dirs
    removeAllFilesFromDirectory(projectDir);

    setStatusMessage("Completed");
  }
  return true;
}


bool MKUWPageFinished::isComplete() const
{
  return mCompilationEnded;
}

void MKUWPageFinished::makefileCompleted(CarbonConsole::CommandMode mode, CarbonConsole::CommandType)
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  KitManifest* manifest = wiz->getManifest();
  bool hasPostActions = manifest->getPostUserActions().count() > 0 ? true : false;
  
  // Force execution of postcompilation scripts for testing
  if (theApp->checkArgument("-modelKitTestScript"))
    hasPostActions = true;

  qDebug() << "make finished" << mode << mCompilationPhase1 << mCompilationPhase2;

  // After the local compile is done, we are done.
  if (mode == CarbonConsole::Local)
  {
    if (mCompilationPhase1 && !mCompilationPhase2 && hasPostActions)
    {
      if (!mStartedPost) // We might execute shell scripts which we don't consider
      {
        mStartedPost = true;
        UtString armProjDir;
        OSConstructFilePath(&armProjDir, mProject->getProjectDirectory(), "ARM_Logical");
        // Execute Post Compilation Script Actions
        if (executePostCompilationActions())
        {
          mCompilationPhase2 = true;
          mProject->signalPostCompiliationJavaScriptFinished();

          // Kick off the compile again
          QTimer* tempTimer = new QTimer(this);
          tempTimer->setSingleShot(true);
          tempTimer->setInterval(10);     // milliseconds
          connect(tempTimer, SIGNAL(timeout()), this, SLOT(finishCompile()));
          tempTimer->start();
        }
        else
          theApp->shutdown(1);
      }
    }
    else if ( (!hasPostActions && mCompilationPhase1) ||
              (hasPostActions && mCompilationPhase2) )
    {
      mCompilationEnded = true;

      if (cleanupFiles())
      {
        emit completeChanged();
        if (wiz->getAnswers())
        {
          qDebug() << "playback compilation finished, closing";
          qDebug() << "pending events" << qApp->hasPendingEvents();
          theApp->setExitCode(0);
          theApp->shutdown(0);
        }
      }
      else
      {
        qDebug() << "Error occurred during cleanup";
        qDebug() << "pending events" << qApp->hasPendingEvents();
        theApp->setExitCode(1);
        theApp->shutdown(1);
      }
    }
  }
}
bool MKUWPageFinished::executePostCompilationActions()
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  KitManifest* manifest = wiz->getManifest();
  KitUserContext* userContext = wiz->getUserContext();

  bool allValid = true;

  wiz->setWhen(ModelKitUserWizard::Postcompilation);

  foreach (KitUserAction* action, manifest->getPostUserActions())
  {
    qDebug() << "executing postcompilation script" << action->getName();

    UserActionUI* uaui = mActionMap[action];
    bool completed = action->execute(wiz, this, userContext);
    if (uaui)
      uaui->setComplete(completed);
    if (!completed)
    {
      allValid = false;
      return false;
      break;
    }
  }

  bool foundAnswers = false;
  QString answersFile = theApp->getArgumentPath("-modelKitTestAnswers", &foundAnswers);
  
  bool foundTestScript = false;
  QString testScriptFile = theApp->getArgumentPath("-modelKitTestScript", &foundTestScript);

  if (foundAnswers && foundTestScript)
  {
    qDebug() << "Running test script";
    ScriptingEngine* te = new ScriptingEngine();

    KitTest* kt = new KitTest();

    kt->setModelKitName(theApp->argumentValue("-modelKit"));
    
    if (kt->loadPortalAnswers(answersFile))
    {
      QScriptValueList sargs;
      sargs.append(te->newQObject(kt));

      qDebug() << "loading test script" << testScriptFile;
      te->runScript(testScriptFile);

      qDebug() << "calling testModel in script" << testScriptFile;
      QScriptValue sv = te->callScriptFunction("testModel", sargs);

      if (theApp->checkArgument("-modelKitTestOutputDir"))
      {
        QString destDir = theApp->argumentValue("-modelKitTestOutputDir");

        QDir dir(mOutputDir);
        dir.setFilter(QDir::Files);
        foreach (QFileInfo fi, dir.entryInfoList())
        {
          if (fi.isFile())
          {
            QString destName = QString("%1/%2").arg(destDir).arg(fi.fileName());

            if (!QFile::copy(fi.filePath(), destName))
              qDebug() << "ERROR: unable to copy" << fi.filePath() << "to" << destName;
            else
              qDebug() << "Test Retained" << fi.fileName();
          }
        }
      }

      if (sv.isBoolean() && !sv.toBoolean())
      {
        qDebug() << "Error: Test script failure";
        return false;
      }
    }
    else
    {
      qDebug() << "Error loading" << answersFile;
      return false;
    }
  }


  return true;
}

void MKUWPageFinished::processTerminated(CarbonConsole::CommandMode mode, CarbonConsole::CommandType ctype, int exitStatus)
{
  qDebug() << "wizardFinished" << mode << ctype << exitStatus;
  if (exitStatus == 0)
  {
    if (!theApp->isShuttingDown())
      makefileCompleted(mode, CarbonConsole::Compilation);
    else
      qDebug() << "user cancelled compilation";
  }
  else
  {
    QMessageBox::critical(NULL, MODELSTUDIO_TITLE, "Errors found during compilation, Contact Carbon Support");
    theApp->setExitCode(exitStatus);
    theApp->shutdown(exitStatus);
  }
}

void MKUWPageFinished::finishCompile()
{
  UtString armProjDir;
  OSConstructFilePath(&armProjDir, mProject->getProjectDirectory(), "ARM_Logical");
  mProjectWidget->compile(false, true, false, NULL, NULL, NULL, armProjDir.c_str());
}

void MKUWPageFinished::setStatusMessage(const QString& msg)
{
  ui.label->setText(msg);
}

void MKUWPageFinished::setProgress(int value)
{
  ui.progressBar->setVisible(true);
  ui.progressBar->setValue(value);
}

void MKUWPageFinished::setProgressRange(int low, int high)
{
  ui.progressBar->setVisible(true);
  ui.progressBar->setValue(0);
  ui.progressBar->setMinimum(low);
  ui.progressBar->setMaximum(high);
}



void MKUWPageFinished::initializePage()
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  KitManifest* manifest = wiz->getManifest();

  if (!mInitialized)
  {
    KitUserContext* userContext = wiz->getUserContext();
    CQT_CONNECT(userContext, updateStatusMessage(const QString&), this, setStatusMessage(const QString&));
    CQT_CONNECT(userContext, updateProgress(int), this, setProgress(int));
    CQT_CONNECT(userContext, updateProgressRange(int,int), this, setProgressRange(int,int));
    mInitialized = true;
  }
#if pfWINDOWS
  CQT_CONNECT(mProject, makefileCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, makefileCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType));
#else
  CQT_CONNECT(mProjectWidget->getConsole(), processTerminated(CarbonConsole::CommandMode, CarbonConsole::CommandType, int), this, processTerminated(CarbonConsole::CommandMode, CarbonConsole::CommandType, int));
#endif

  UtString armProjDir;
  OSConstructFilePath(&armProjDir, mProject->getProjectDirectory(), "ARM_Logical");

  bool hasPostActions = manifest->getPostUserActions().count() > 0 ? true : false;

  mCompilationPhase1 = true;

  if (hasPostActions)
    mProjectWidget->compile(false, true, false, "buildModel", NULL, NULL, armProjDir.c_str());
  else
    mProjectWidget->compile(false, true, false, NULL, NULL, NULL, armProjDir.c_str());

  qDebug() << "compilation started...";

  if (wiz->getAnswers())
  {
    wiz->next();
  }
}

