#ifndef __CARBONDATABASECONTEXT_H__
#define __CARBONDATABASECONTEXT_H__

#include "util/CarbonPlatform.h"

#include "gui/CQt.h"

#include "util/UtString.h"
#include "util/UtHashMap.h"
#include "util/AtomicCache.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/ArgProc.h"
#include "util/AtomicCache.h"
#include "util/OSWrapper.h"
#include "util/RandomValGen.h"
#include "util/SourceLocator.h"

#include "shell/CarbonAbstractRegister.h"
#include "shell/carbon_misc.h"

#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "shell/carbon_capi.h"

class RegTabWidget;
class CarbonBuild;
class CGraph;
struct CarbonCfg;

class CarbonDatabaseContext
{
public:
  CarbonDatabaseContext(CQtContext* cqt);
  CarbonDB* loadDatabase(CQtContext* cqt, const char* iodbName);
  void unloadDatabase();
  ~CarbonDatabaseContext();
  //CGraph* getCGraph() const { return mCGraph; }
  CarbonDB* getDB() const { return mDB; }
  CarbonCfg* getCfg() const { return mCfg; }

private:
  enum ParseResult {eIODB, eFullDB, eCcfg, eNoMatch};
  static ParseResult parseFileName(const char* filename, UtString* baseName);

private:
  CQtContext* mCQt;
  AtomicCache* mAtomicCache;
  SourceLocatorFactory mLocatorFactory;
  CarbonDB* mDB;
  CarbonCfg* mCfg;
  QString mLastDBName;
  //CGraph* mCGraph;
};


#endif
