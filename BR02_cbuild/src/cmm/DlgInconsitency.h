#ifndef DLGINCONSITENCY_H
#define DLGINCONSITENCY_H

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include <QtGui>
#include "ui_DlgInconsitency.h"

class DlgInconsitency : public QDialog
{
  Q_OBJECT

public:
  DlgInconsitency(QWidget *parent = 0, const QString& msg = "");
  ~DlgInconsitency();

private:
  Ui::DlgInconsitencyClass ui;

  private slots:
    void on_buttonBox_clicked(QAbstractButton*);
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
};

#endif // DLGINCONSITENCY_H
