//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include "ModelKit.h"

#include "DlgEditVersion.h"

DlgEditVersion::DlgEditVersion(QWidget *parent, ModelKit* kit, KitVersion* kv)
: QDialog(parent)
{
  mModelKit = kit;
  mVersion = kv;
  ui.setupUi(this);

  mManifestName = kv->getManifest()->getName();

  ui.lineEditVendorVersion->setText(kv->getVendorVersion());
  ui.lineEditDescription->setText(kv->getDescription());
  ui.checkBoxLocked->setChecked(kv->getLocked());

  ui.lineEditManifestName->setText(kv->getManifest()->getName());

  ui.lineEditManifestName->setDisabled(kv->getLocked());
  ui.lineEditVendorVersion->setDisabled(kv->getLocked());
  ui.lineEditDescription->setDisabled(kv->getLocked());
}

DlgEditVersion::~DlgEditVersion()
{
}


void DlgEditVersion::on_buttonBox_rejected()
{
  reject();
}

void DlgEditVersion::on_buttonBox_accepted()
{
  if (mManifestName != ui.lineEditManifestName->text())
  {
    KitManifest* mf = mModelKit->getManifests()->findManifest(ui.lineEditManifestName->text());
    if (mf)
    {
      QMessageBox::critical(NULL, "Error", "Manifest name already in use");
      return;
    }
  }
  accept();
}

void DlgEditVersion::on_checkBoxLocked_stateChanged(int state)
{
  bool locked = state == Qt::Checked;
  ui.lineEditVendorVersion->setDisabled(locked);
  ui.lineEditDescription->setDisabled(locked);
  ui.lineEditManifestName->setDisabled(locked);
}
