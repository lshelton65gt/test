#ifndef MKWPAGEINFO_H
#define MKWPAGEINFO_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>
#include "WizardPage.h"
#include <QWizardPage>
#include "ui_MKWPageInfo.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "KitManifest.h"

class MKWPageInfo : public WizardPage
{
  Q_OBJECT

public:
  MKWPageInfo(QWidget *parent = 0);
  ~MKWPageInfo();

private:
  void saveSettings();
  void restoreSettings();

protected:
  virtual bool validatePage();
  virtual void initializePage();

private slots:
  void on_pushButtonBrowse_clicked();

private:
  Ui::MKWPageInfoClass ui;
  QSettings mSettings;
};

#endif // MKWPAGEINFO_H
