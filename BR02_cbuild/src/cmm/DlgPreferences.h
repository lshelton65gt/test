#ifndef DLGPREFERENCES_H
#define DLGPREFERENCES_H
#include "util/CarbonPlatform.h"

#include <QtGui>
#include <Qsci/qsciscintilla.h>

#include <Qsci/qscilexercpp.h>
#include <Qsci/qscilexerhtml.h>
#include <Qsci/qscilexermakefile.h>
#include <Qsci/qscilexerjavascript.h>
#include "qscilexerverilog.h"

#include "ui_DlgPreferences.h"
#include "cmm.h"

#define PREF_TE_FONTSIZE "textEditor/fontSize"

#define PREF_GEN_REOPENLAST "general/ReopenLastProject"
#define PREF_GEN_AUTOLOADHIER "general/AutoLoadHierarchy"
#define PREF_GEN_AUTOLOADFILE "general/AutoLoadChangedFiles"

#define PREF_GEN_NUMLOGS "general/NumberOfLogs"

#define PREF_GEN_HOMEPAGE "general/HomePage"
#define PREF_GEN_HOMEPAGE_DEFAULT_VALUE "http://www.carbondesignsystems.com/ModelStudio/welcome.htm"

#define PREF_GEN_VARSCOPE "general/VariableScope"

#define PREF_VS_VERSION "general/VSVersion"


class DlgPreferences : public QDialog
{
  Q_OBJECT


public:
  enum GeneralPreference { GeneralTabSize, GeneralIndentSize, GeneralConvertTabs, GeneralVariableScope, GeneralVSVersion, GeneralHomePage, AutoLoadHierarchy, AutoLoadChangedFiles};
  enum ViewPreference { ViewWhitespace, ViewEndOfLine, 
                ViewIndentationGuide, ViewLineNumbers, ViewFoldMargin,
                ViewBraceMatching, ViewAutoIndent, ViewIntellisenseVariables};

  void writeSettings();
  void readSettings();

  QVariant getViewPreference(ViewPreference pref);
  QVariant getGeneralPreference(GeneralPreference pref);

  void setGeneralPreference(GeneralPreference pref, const QVariant& value);

public:
  DlgPreferences(QWidget *parent = 0);
  ~DlgPreferences();

private:
  void populateLanguages();
  void showStyle(int style);
  void showDefaultStyles();
  void setupLexers();
  void updateFont(const QString& font);
  void updateFont();
  void apply();

private slots:
  void on_listWidgetStyles_itemSelectionChanged();
  void on_buttonBox_clicked(QAbstractButton*);
  void on_spinBoxFontSize_valueChanged(int);
  void on_checkBoxItalic_stateChanged(int);
  void on_checkBoxBold_stateChanged(int);
  void on_fontComboBox_textChanged(const QString &);
  void on_listWidget_currentItemChanged(QListWidgetItem*,QListWidgetItem*);
  void on_buttonBox_accepted();
  void on_buttonBox_rejected();
  void on_pushButtonReset_clicked();
  void on_pushButtonBG_clicked();
  void on_pushButtonFG_clicked();
  void on_comboBoxLanguages_currentIndexChanged(int);

private:
  void initializeValues();
  void applyValues();
  void langStyleChanged(int);

private:
  Ui::DlgPreferencesClass ui;
  QSettings mSettings;

private:
  QsciLexer* mLexer;
  int mStyleNumber;
  QList<QsciLexer*> mLexers;

private:
  QVariant mTextEditorFontSize;
  QVariant mReopenLastProject;
  QVariant mAutoLoadHierarchy;
  QVariant mAutoLoadChangedFiles;
  QVariant mNumberOfLogs;
  QVariant mVSVersion;
  QVariant mHomePage;
  bool mInitialized;
};

#endif // DLGPREFERENCES_H
