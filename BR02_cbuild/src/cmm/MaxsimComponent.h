#ifndef __CARBONMAXSIMCOMPONENT_H__
#define __CARBONMAXSIMCOMPONENT_H__
#include "util/CarbonPlatform.h"
#include <QObject>

#include "carbon/carbon_dbapi.h"
#include "cfg/CarbonCfg.h"

#include "util/XmlParsing.h"
#include "CarbonComponent.h"
#include "util/UtString.h"
#include "RemoteConsole.h"
#include "CarbonConsole.h"

class ScrCcfg;
class CarbonOptions;
class CarbonProperties;
class CarbonProjectWidget;
class CarbonProperty;
class MaxsimTreeItem;
class CarbonConfigurations;
class CarbonConfiguration;
class MaxsimCcfgItem;
class MaxsimSourceItem;

class MaxsimComponent : public CarbonComponent, public CarbonCfgScriptExtension
{
  Q_OBJECT
  Q_CLASSINFO("ccfg", "ScrCcfg*");

  // Property: ccfg
  // The ccfg object associated with this component
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(ScrCcfg* ccfg READ getCcfg);

  virtual void registerTypes(QScriptEngine* engine);

public:
  MaxsimComponent(CarbonProject* proj);
  virtual ~MaxsimComponent();

  // Overrides
  static MaxsimComponent* deserialize(CarbonProject* proj, xmlNodePtr parent, UtXmlErrorHandler* eh);
  virtual bool serialize(xmlTextWriterPtr writer);
  virtual const char* getTargetName(CarbonConfiguration*,CarbonComponent::TargetType, CarbonComponent::TargetPlatform);
  virtual bool getTargetImpl(CarbonConfiguration*,QTextStream& stream, TargetType type);
  virtual CarbonComponentTreeItem* createTreeItem(CarbonProjectWidget*, QTreeWidgetItem*);
  virtual void generateMakefile(CarbonConfiguration*,const char* makefileName);
  void updateGUIFromCcfg(CarbonConfiguration* activeConfig, CarbonCfg* cfg);
  virtual const char* getOutputDirectory(const char* cfgName=NULL);
  virtual const char* getComponentDirectory() const;
  CarbonOptions* getOptions(const char* configName=NULL);
  CarbonConfigurations* getConfigurations() { return mConfigs; }
  void updateTreeIcons();
  MaxsimTreeItem* getRootItem() const { return mRootItem; }
  virtual bool checkComponent(CarbonConsole*);
  void icheckComponent();
  virtual void setEnabled(bool value);
  virtual bool isWindowsTarget(CarbonConfiguration*);
  bool importCcfg(QString filename, bool checkDB=true);
  const char* ccfgName();
  UInt32 numPackageFiles();
  QString getPackageFile(UInt32 index);
  virtual void setXtorDefFile(const char* filename);
  virtual const char* xtorDefFile();
  virtual int getGeneratedFiles(QStringList&);
  virtual int getComponentFiles(QStringList&);
  virtual int getSourceFiles(QStringList& fileList);
  void createDefaultComponent();
  void setComponentName(const QString& compName);
  bool isStandaloneComponent();

  QString getDbFilename();
  QString getDbFilename(const char* configName);

private:
  ScrCcfg* getCcfg();
  static void createDirectory(CarbonConfiguration*);
  void renameDirectory(const char* oldName, const char* newName);
  virtual void updateCcfg(CarbonConfiguration*, const char* iodbName);
  bool readCcfg(CarbonCfg* cfg, const char* filename);
  static void createConfigurations(MaxsimComponent* comp, xmlNodePtr parent);
  void updateTreeTextValues();
  UtString componentName();
  UtString cModelBaseName();
  void populateLoop(CarbonDB* carbonDB, CarbonCfgRTLPortType type, CarbonDBNodeIter* iter);
  CarbonCfgESLPortID addConnection(CarbonCfgRTLPortID rtlPort);
  CarbonCfgESLPortID addESLConnection(CarbonCfgRTLPortID rtlPort,
                                                    CarbonCfgESLPortType type);
  QStringList getCppFiles(CarbonCfg* cfg);
  QStringList getCppFiles();

signals:
  void xtorDefFileChanged();

private slots:
  void valueChanged(const CarbonProperty*, const char*);
  void configurationChanged(const char*);
  void compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void configurationRemoved(const char*);
  void configurationRenamed(const char*, const char*);
  void configurationCopy(const char*, const char*);
  void configurationAdded(const char* name);
  void compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void outputFilenameChanged(const CarbonProperty*, const char*);
  void projectLoaded(const char* fileName, CarbonProject*);
  void makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void xtorDefChanged(const CarbonProperty*, const char* newValue);
  void projectClosing(CarbonProject*);
  void postCompilationJavaScriptFinished();

private:
  //! Persist the ccfg so that Project.SoCDesigner.ccfg always returns the same one
  ScrCcfg* mCcfg;

  CarbonConfigurations* mConfigs;
  UtString mOutputDir;
  UtString mTargetName;
  MaxsimTreeItem* mRootItem;
  CarbonConfiguration* mActiveConfiguration;
  CarbonCfgID mCfg;
  CarbonDB* mDB;
  MaxsimCcfgItem* mCcfgItem;
  UtArray<MaxsimSourceItem*> mMaxsimSourceItems;
  MaxsimSourceItem* mMaxsimMkf;
  //! Is SystemC export requested for the active configuration?  This is set in generateMakefile()
  bool mActiveConfigSystemCExport;

  UtString mIODBName;
  UtString mCcfgName;
};


class MaxsimTreeItem : public CarbonComponentTreeItem
{
  Q_OBJECT

public:
  MaxsimTreeItem(CarbonProjectWidget* proj, MaxsimComponent* comp);
  MaxsimTreeItem(CarbonProjectWidget* proj, MaxsimComponent* comp, QTreeWidgetItem* parent);
  virtual void showContextMenu(const QPoint&);
  virtual void singleClicked(int);

private:
  void createActions();

public slots:
    void compileComponent();
    void deleteComponent();
    void cleanComponent();
    void icheckComponent();
    void recompileComponent();
    void deleteComponentBatch();

private:
  QAction* mActionClean;
  QAction* mActionCompile;
  QAction* mActionReCompile;
  QAction* mActionDelete;
  QAction* mActionCheck;
};

class MaxsimSourceItem : public CarbonProjectTreeNode
{
public:
  MaxsimSourceItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, MaxsimComponent* comp) : CarbonProjectTreeNode(proj,parent) 
  {
    setIcon(0, QIcon(":/cmm/Resources/file-generated-16x16.png"));
    mComp = comp;
  }
  virtual void doubleClicked(int);
  void setExists(bool value)
  {
    if (value)
      setIcon(0, QIcon(":/cmm/Resources/file-generated-16x16.png"));
    else
      setIcon(0, QIcon(":/cmm/Resources/file-16x16missing.png"));
  }

private:
  MaxsimComponent* mComp;
};

class MaxsimCcfgItem : public CarbonProjectTreeNode
{
public:
  MaxsimCcfgItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, MaxsimComponent* comp);
 
  virtual void doubleClicked(int);
  virtual void singleClicked(int);
  virtual void showContextMenu(const QPoint&);
  void setExists(bool value)
  {
    if (value)
      setIcon(0, QIcon(":/cmm/Resources/Wizard.png"));
    else
      setIcon(0, QIcon(":/cmm/Resources/file-16x16missing.png"));
  }

private:
  MaxsimComponent* mComp;
};

#endif
