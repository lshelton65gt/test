//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "util/UtEncryptDecrypt.h"
#include "CarbonMakerContext.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"
#include "KitManifest.h"
#include "CarbonFiles.h"
#include "CarbonComponent.h"
#include "ModelKitWizard.h"
#include "ShellConsole.h"
#include "ScriptingEngine.h"
#include "JavaScriptAPIs.h"
#include "ModelKitUserWizard.h"
#include <stdio.h>
#include <assert.h>

#include <QtXml>


static QString expandVariables(const QString& inputName)
{
  QString retValue;
  UtString result;
  UtString err;
  UtString inName; inName << inputName;
  OSExpandFilename(&result, inName.c_str(), &err);
  retValue = result.c_str();
  return retValue;
}

static QString envVarValue(const QString varName, bool* found)
{
  *found = false;
  QStringList environment = QProcess::systemEnvironment();
  foreach (QString evar, environment)
  {
    int equalsPos = evar.indexOf('=');
    QString evarName = evar.left(equalsPos);
    QString evarVal = evar.mid(equalsPos+1);
    if (evarName == varName)
    {
      *found = true;
      return evarVal;
      break;
    }
  }
 
  return "";
}


bool KitManifest::hasComponent(KitManifest::ComponentType ctype)
{
  QString compName;
  switch (ctype)
  {
  case SOCDesigner:   compName = "SocDesigner"; break;
  case CoWare:        compName = "CoWare"; break;
  case SystemC:       compName = "SystemC"; break;
  }

  foreach (QString name, getComponents())
  {
    if (name == compName)
      return true;
  }

  return false;
}


QString KitUserContext::expandName(const QString& inputStr)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QString resultStr = inputStr;
  int ix = -1;
  bool changed;

  do // repeat until no more ${var}s
  {
    changed = false;
    QString startingStr = resultStr;
    QRegExp braceExp("\\$\\{(\\S+)\\}");
    braceExp.setPatternSyntax(QRegExp::RegExp);
    braceExp.setMinimal(true);
    ix = braceExp.indexIn(resultStr);
    int i2 = braceExp.matchedLength();
    int nc = braceExp.numCaptures();
    if (ix != -1 && nc == 1)
    {
      QString left = resultStr.left(ix);
      QString right = resultStr.mid(ix+i2);
      QString var = braceExp.cap(1);
      QString bracedVarName = QString("${%1}").arg(var);
      QString varModifier;

      int colonPos = var.indexOf(':');
      if (colonPos != -1)
      {
        var = var.left(colonPos);
        varModifier = braceExp.cap(1).mid(colonPos+1);
        bracedVarName = QString("${%1}").arg(var);
      }

      bool matchFound = false;
      QString expandedVar;
      if (mVariableMap.contains(bracedVarName))
      {
        expandedVar = mVariableMap[bracedVarName];
        matchFound = true;
      }
      else
      {
        QString value = envVarValue(var, &matchFound);
        if (matchFound)
          expandedVar = value;
      }

      // Now, apply modifiers if we found something
      if (matchFound)
      {
        if (varModifier.isEmpty())
          resultStr = left + expandedVar + right;
        else
        {
          QFileInfo fi(expandedVar);
          // tail
          if (varModifier == "t")
            resultStr = left + fi.fileName() + right;
          else if (varModifier == "h")
            resultStr = left + fi.path() + right;
          else if (varModifier == "e")
            resultStr = left + fi.suffix() + right;
          else if (varModifier == "r")
          {
            QString root = QString("%1/%2").arg(fi.path()).arg(fi.baseName());
            resultStr = left + root + right;
          }
          else
            qDebug() << "Error: Unknown modifier" << varModifier;
        }
      }
    }

    if (startingStr != resultStr)
    {
      qDebug() << "expanded" << startingStr << "to" << resultStr;
      changed = true;
    }
  }
  while (changed);

 // CARBON_PROJECT
  resultStr = resultStr.replace("${CARBON_PROJECT}", proj->getProjectDirectory());
  
  // CARBON_OUTPUT
  resultStr = resultStr.replace("${CARBON_OUTPUT}", proj->getActive()->getOutputDirectory());

  qDebug() << "expanded" << inputStr << resultStr;

  return resultStr;
}

bool KitManifest::deserializeRTLFiles(const QDomElement& e, XmlErrorHandler*)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    n = n.nextSibling();

    if (!element.isNull() && !element.isComment())
    {
      if ("Signature" == element.tagName())
      {
        QString actualName = element.attribute("absolutePath");
        QString normalizedName = element.attribute("normalizedPath");
        QString fileAtts = element.attribute("fileAttributes");
        quint32 atts = fileAtts.toUInt(0, 16);
        if (mWizard)
          actualName = mWizard->expandName(normalizedName);
        QString hash = element.attribute("hash");

        KitFile* kf = new KitFile(actualName, normalizedName, false);
        kf->setHash(hash);
        kf->setPermissions(QFile::Permissions(atts));

        RTLFileOptions* rfu = mFileOptionsMap[normalizedName];                
        kf->setOptional(rfu->mOptional);
        kf->setCheckHash(rfu->mHashed);
        kf->setCopyFile(rfu->mCopied);
               
        mRTLFileList.append(kf);
      
        qDebug() << "signature" << normalizedName << "hash" << hash;
      }
    }
  }
  return true;
}

bool KitManifest::deserializeOptionalRTLFiles(const QDomElement& e, XmlErrorHandler*)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    n = n.nextSibling();

    if (!element.isNull() && !element.isComment())
    {
      if ("OptionalSignature" == element.tagName())
      {
        QString actualName = element.attribute("absolutePath");
        QString normalizedName = element.attribute("normalizedPath");
        QString fileAtts = element.attribute("fileAttributes");
        quint32 atts = fileAtts.toUInt(0, 16);

        bool checkHash = true;
        if (element.attribute("checkHash") == "false")
          checkHash = false;
        if (mWizard)
          actualName = mWizard->expandName(normalizedName);
        QString hash = element.attribute("hash");

        RTLFileOptions* rfu = mFileOptionsMap[normalizedName];  

        KitFile* kf = new KitFile(actualName, normalizedName, true);
        kf->setPermissions(QFile::Permissions(atts));
        kf->setCheckHash(checkHash);
        kf->setHash(hash);
        kf->setOptional(true);
        kf->setCopyFile(rfu->mCopied);
        rfu->mOptional = true;

        bool hasExisting = false;
        foreach(KitFile* ekf, mOptRTLFileList)
        {
          if (ekf->mNormalizedName == normalizedName)
          {
            hasExisting = true;
            ekf->setCheckHash(checkHash);
            ekf->setHash(hash);
            ekf->setOptional(true);
            ekf->setPermissions(QFile::Permissions(atts));
            break;
          }
        }

        if (!hasExisting)
          mOptRTLFileList.append(kf);

        qDebug() << "opt signature" << normalizedName << "hash" << hash;
      }
    }
  }
  return true;
}




bool KitManifest::deserializeProjectSettings(const QDomElement& e, XmlErrorHandler*)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    n = n.nextSibling();

    if (!element.isNull() && !element.isComment())
    {
      if ("Library" == element.tagName())
      {
        setLibraryName(element.text());
      }
      else if ("LicenseKey" == element.tagName())
      {
        setLicenseKey(element.text());
      }
    }
  }
  return true;
}

bool KitManifest::deserializeFileChecks(const QDomElement& e, XmlErrorHandler*)
{
  mFileOptionsMap.clear();

  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    n = n.nextSibling();

    if (!element.isNull() && !element.isComment())
    {
      if ("File" == element.tagName())
      {
        QString name = element.attribute("name");
        bool checked = element.attribute("checked") == "true" ? true : false;
        mFileCheckMap[name] = checked;

        RTLFileOptions* rfu = new RTLFileOptions();
        rfu->mHashed = true;
        rfu->mOptional = false;
        rfu->mCopied = checked;
        if (element.hasAttribute("hashed"))
          rfu->mHashed = element.attribute("hashed") == "true" ? true : false;
  
        if (element.hasAttribute("optional"))
          rfu->mOptional = element.attribute("optional") == "true" ? true : false;
       
        rfu->mFileName = name;

        mFileOptionsMap[name] = rfu;
      }
    }
  }
  return true;
}

bool KitManifest::deserializeCompilerSwitches(const QDomElement& e, XmlErrorHandler*)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    n = n.nextSibling();
    if (!element.isNull() && !element.isComment())
    {
      QString sw = element.text();
      if (sw.length() > 0)
      {
        qDebug() << "deserialized compiler switch" << sw;
        mCompilerSwitches.append(sw);
      }
    }
  }
 
  return true;
}

bool KitManifest::deserializeModelKitFeatures(const QDomElement& e, XmlErrorHandler*)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    n = n.nextSibling();
    if (!element.isNull() && !element.isComment())
    {
      QString feature = element.text();
      if (feature.length() > 0)
      {
        qDebug() << "deserialized model kit feature" << feature;
        mModelKitFeatures.insert(feature);
      }
    }
  }
 
  return true;
}

bool KitManifest::deserializeUserActions(const QDomElement& e, XmlErrorHandler* eh)
{
  QDomElement preElem = e.firstChildElement("PreCompile");
  if (!preElem.isNull())
  {
    QDomNode n = preElem.firstChild();
    while(!n.isNull())
    {
      QDomElement element = n.toElement(); // try to convert the node to an element.
      n = n.nextSibling();
      if (!element.isNull() && !element.isComment())
      {
        qDebug() << "kitManifest Pre Action" << element.tagName();
        KitUserAction* action = NULL;
        action = KitActionExecuteScript::deserialize(element, eh);
        if (action == NULL)
          action = KitActionInputFile::deserialize(element, eh);
        if (action == NULL)
          action = KitActionRunScript::deserialize(element, eh);
        if (action)
          addPreUserAction(action);
      }
    }
  }
  QDomElement postElem = e.firstChildElement("PostCompile");
  if (!postElem.isNull())
  {
    QDomNode n = postElem.firstChild();
    while(!n.isNull())
    {
      QDomElement element = n.toElement(); // try to convert the node to an element.
      n = n.nextSibling();
      if (!element.isNull() && !element.isComment())
      {
        qDebug() << "kitManifest Post Action" << element.tagName();
        KitUserAction* action = NULL;
        action = KitActionExecuteScript::deserialize(element, eh);
        if (action == NULL)
          action = KitActionInputFile::deserialize(element, eh);
        if (action == NULL)
          action = KitActionRunScript::deserialize(element, eh);
        if (action)
          addPostUserAction(action);
      }
    }
  }
  return true;
}


bool KitManifest::deserializeComponents(const QDomElement& e, XmlErrorHandler*)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    n = n.nextSibling();
    if (!element.isNull() && !element.isComment())
    {
      qDebug() << "creating kit component" << element.tagName();
      addComponent(element.tagName());
    }
  }
  return true;
}


bool KitManifest::deserializeFiles(const QDomElement& e, XmlErrorHandler*)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    n = n.nextSibling();

    if (!element.isNull() && !element.isComment())
    {
      if ("File" == element.tagName())
      {
        QString fileAtts = element.attribute("fileAttributes");
        quint32 atts = fileAtts.toUInt(0, 16);
        QString actualName = element.attribute("absolutePath");
        QString normalizedName = element.attribute("normalizedPath");
        if (mWizard)
          actualName = mWizard->expandName(normalizedName);

        qDebug() << "Deserialized Kit File"  << actualName << normalizedName;

        if (mWizard && !normalizedName.startsWith("$"))
        {
          qDebug() << "Re-normalizing";
          normalizedName = mWizard->normalizeName(normalizedName);
        }

        QString hash = element.attribute("hash");
        QString userFile = element.attribute("userfile");
        QString retained = element.attribute("retained");
        bool retainFile = false;
        if (!retained.isEmpty() && retained == "true")
          retainFile = true;

        KitFile* kf;
        if (userFile == "true")
        {
          kf = new KitFile(actualName, normalizedName, false, ZstreamZip::eFileTxt, true);
          kf->setPermissions(QFile::Permissions(atts));
          mUserFileList.append(kf);
        }
        else
        {
          kf = new KitFile(actualName, normalizedName, false);
          kf->setPermissions(QFile::Permissions(atts));
          mFileList.append(kf);
        }
        kf->setRetained(retainFile);
        kf->setHash(hash);
        qDebug() << "file" << normalizedName << "hash" << hash;
      }
    }
  }
  return true;
}

void KitManifest::serializeComponents(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QDomElement compsTag = doc.createElement("Components");
  parent.appendChild(compsTag);

  QList<CarbonComponent*> comps;
  proj->getComponents()->getActiveComponents(&comps);
  foreach (CarbonComponent* comp, comps)
  {
    if (comp->isModelKitComponent())
    {
      QString name = comp->getFriendlyName();
      QDomElement compTypeTag = doc.createElement(name);
      compsTag.appendChild(compTypeTag);
    }
  }
}

void KitManifest::serializeProjectSettings(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  const char* outputFilename = proj->getToolOptions("VSPCompiler")->getValue("-o")->getValue();
  setLibraryName(outputFilename);
  QDomElement projectTag = doc.createElement("Project");
  parent.appendChild(projectTag);

  QDomElement libTag = doc.createElement("Library");
  projectTag.appendChild(libTag);
  QDomText libText = doc.createTextNode(getLibraryName());
  libTag.appendChild(libText);

  QDomElement keyTag = doc.createElement("LicenseKey");
  projectTag.appendChild(keyTag);
  QDomText keyText = doc.createTextNode(getLicenseKey());
  keyTag.appendChild(keyText);
}


void KitManifest::serializeFileChecks(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  QDomElement subTag = doc.createElement("FileCheckboxes");
  parent.appendChild(subTag);

  foreach (QString fileName, mFileOptionsMap.keys())
  {
    RTLFileOptions* rfu = mFileOptionsMap[fileName];

    QDomElement fileTag = doc.createElement("File");
    subTag.appendChild(fileTag);
    fileTag.setAttribute("name", fileName);
     
    fileTag.setAttribute("checked", rfu->mCopied ? "true" : "false");
    fileTag.setAttribute("hashed", rfu->mHashed ? "true" : "false");
    fileTag.setAttribute("optional", rfu->mOptional ? "true" : "false");    
  }
}
void KitManifest::serializeOptionalRTLFiles(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QDomElement sigsTag = doc.createElement("OptionalSignatures");
  parent.appendChild(sigsTag);

  QStatusBar* sb = pw->context()->getMainWindow()->statusBar();

  foreach (KitFile* kf, mOptRTLFileList)
  {    
    if (!kf->getOptional())
      continue;

    QDomElement sigTag = doc.createElement("OptionalSignature");
    sigsTag.appendChild(sigTag);

    QString atts = QString("%1").arg((quint32)kf->getPermissions(), 8, 16, QChar('0')).trimmed();
    sigTag.setAttribute("fileAttributes", atts);
    sigTag.setAttribute("absolutePath", proj->getUnixEquivalentPath(kf->getActualName()));
    sigTag.setAttribute("normalizedPath", kf->getNormalizedName());
    sigTag.setAttribute("checkHash", kf->getCheckHash() ? "true" : "false");

    if (!getPreserveHashes())
    {
      QString rtlFileName = kf->getActualName();
      if (mWizard)
        rtlFileName = mWizard->expandName(kf->getNormalizedName());
      QString hashValue = proj->hashFile(rtlFileName);
      INFO_ASSERT(!hashValue.isEmpty(), "Invalid Hash");
      sigTag.setAttribute("hash", hashValue);
    }
    else
    {
      if (kf->getHash().isEmpty())
      {
        QString rtlFileName = kf->getActualName();
        if (mWizard)
          rtlFileName = mWizard->expandName(kf->getNormalizedName());

        kf->setHash(proj->hashFile(rtlFileName));
      }
      sigTag.setAttribute("hash", kf->getHash());
    }

    sb->showMessage(QString("Computed hash for %1 {%2}")
        .arg(kf->getActualName())
        .arg(sigTag.attribute("hash")));
  }
}

// Build and save all hashes for RTL files
void KitManifest::serializeRTLFiles(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QDomElement sigsTag = doc.createElement("Signatures");
  parent.appendChild(sigsTag);

  QStatusBar* sb = pw->context()->getMainWindow()->statusBar();
  foreach (KitFile* kf, mRTLFileList)
  {
    QDomElement sigTag = doc.createElement("Signature");
    sigsTag.appendChild(sigTag);
    QString atts = QString("%1").arg((quint32)kf->getPermissions(), 8, 16, QChar('0')).trimmed();
    sigTag.setAttribute("fileAttributes", atts);
    sigTag.setAttribute("absolutePath", proj->getUnixEquivalentPath(kf->getActualName()));
    sigTag.setAttribute("normalizedPath", kf->getNormalizedName());

    if (!getPreserveHashes())
    {
      QString rtlFileName = kf->getActualName();
      if (mWizard)
        rtlFileName = mWizard->expandName(kf->getNormalizedName());
      QString hashValue = proj->hashFile(rtlFileName);
      INFO_ASSERT(!hashValue.isEmpty(), "Invalid Hash");
      sigTag.setAttribute("hash", hashValue);
    }
    else
    {
      if (kf->getHash().isEmpty())
      {
        QString rtlFileName = kf->getActualName();
        if (mWizard)
          rtlFileName = mWizard->expandName(kf->getNormalizedName());

        kf->setHash(proj->hashFile(rtlFileName));
      }
      sigTag.setAttribute("hash", kf->getHash());
    }

    sb->showMessage(QString("Computed hash for %1 {%2}")
        .arg(kf->getActualName())
        .arg(sigTag.attribute("hash")));
  }
}

/*
void KitManifest::serializeOptionalRTLFiles(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QDomElement sigsTag = doc.createElement("OptionalSignatures");
  parent.appendChild(sigsTag);

  QStatusBar* sb = pw->context()->getMainWindow()->statusBar();
  foreach (KitFile* kf, mOptRTLFileList)
  {
    QDomElement sigTag = doc.createElement("OptionalSignature");
    sigsTag.appendChild(sigTag);

    QString atts = QString("%1").arg((quint32)kf->getPermissions(), 8, 16, QChar('0')).trimmed();
    sigTag.setAttribute("fileAttributes", atts);
    sigTag.setAttribute("absolutePath", proj->getUnixEquivalentPath(kf->getActualName()));
    sigTag.setAttribute("normalizedPath", kf->getNormalizedName());
    sigTag.setAttribute("checkHash", kf->getCheckHash() ? "true" : "false");

    if (!getPreserveHashes())
    {
      QString rtlFileName = kf->getActualName();
      if (mWizard)
        rtlFileName = mWizard->expandName(kf->getNormalizedName());
      QString hashValue = proj->hashFile(rtlFileName);
      INFO_ASSERT(!hashValue.isEmpty(), "Invalid Hash");
      sigTag.setAttribute("hash", hashValue);
    }
    else
    {
      if (kf->getHash().isEmpty())
      {
        QString rtlFileName = kf->getActualName();
        if (mWizard)
          rtlFileName = mWizard->expandName(kf->getNormalizedName());

        kf->setHash(proj->hashFile(rtlFileName));
      }
      sigTag.setAttribute("hash", kf->getHash());
    }

    sb->showMessage(QString("Computed hash for %1 {%2}")
        .arg(kf->getActualName())
        .arg(sigTag.attribute("hash")));
  }
}

*/

// Build and save all hashes for files
void KitManifest::serializeFiles(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QDomElement sigsTag = doc.createElement("Files");
  parent.appendChild(sigsTag);
  foreach (KitFile* kf, mFileList)
  {
    QDomElement sigTag = doc.createElement("File");
    sigsTag.appendChild(sigTag);

    QString atts = QString("%1").arg((quint32)kf->getPermissions(), 8, 16, QChar('0')).trimmed();
    sigTag.setAttribute("fileAttributes", atts);
    sigTag.setAttribute("retained", kf->getRetained() ? "true" : "false");
    sigTag.setAttribute("absolutePath", kf->getActualName());
    sigTag.setAttribute("normalizedPath", kf->getNormalizedName());
    if (!getPreserveHashes())
      sigTag.setAttribute("hash", proj->hashFile(kf->getActualName()));
    else
      sigTag.setAttribute("hash", kf->getHash());

    sigTag.setAttribute("userfile", "false");
  }

   foreach (KitFile* kf, mUserFileList)
  {
    QDomElement sigTag = doc.createElement("File");
    sigsTag.appendChild(sigTag);
    sigTag.setAttribute("absolutePath", kf->getActualName());
    QString atts = QString("%1").arg((quint32)kf->getPermissions(), 8, 16, QChar('0')).trimmed();
    sigTag.setAttribute("fileAttributes", atts);
    sigTag.setAttribute("retained", kf->getRetained() ? "true" : "false");
    if (mWizard && !kf->getNormalizedName().startsWith("$"))
    {
      QString currName = kf->getNormalizedName();
      QString name = mWizard->normalizeName(kf->getNormalizedName());
      kf->setNormalizedName(name);
      qDebug() << "re-normalizing" << currName << "to" << name;
    }

    sigTag.setAttribute("normalizedPath", kf->getNormalizedName());
    if (!getPreserveHashes())
      sigTag.setAttribute("hash", proj->hashFile(kf->getActualName()));
    else
      sigTag.setAttribute("hash", kf->getHash());
    sigTag.setAttribute("userfile", "true");

    qDebug() << "Serialized UserFile" << "normalized" << kf->getNormalizedName();
  }
}


void KitManifest::serializeCompilerSwitches(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  QDomElement baseTag = doc.createElement("CompilerSwitches");
  parent.appendChild(baseTag);

  foreach (QString sw, mCompilerSwitches)
  {
    if (sw.length() > 0)
    {
      QDomElement switchTag = doc.createElement("CompilerSwitch");
      baseTag.appendChild(switchTag);

      QDomText nodeText = doc.createTextNode(sw);
      switchTag.appendChild(nodeText);

      qDebug() << "serialized switch" << sw;
    }
  }
}

void KitManifest::serializeModelKitFeatures(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  QDomElement baseTag = doc.createElement("ModelKitFeatures");
  parent.appendChild(baseTag);

  foreach (QString feature, mModelKitFeatures)
  {
    if (feature.length() > 0)
    {
      QDomElement featureTag = doc.createElement("ModelKitFeature");
      baseTag.appendChild(featureTag);

      QDomText nodeText = doc.createTextNode(feature);
      featureTag.appendChild(nodeText);

      qDebug() << "serialized feature" << feature;
    }
  }
}

// Build and save all hashes for files
void KitManifest::serializeUserActions(QDomDocument& doc, QDomElement& parent, XmlErrorHandler* eh)
{
  QDomElement baseTag = doc.createElement("UserActions");
  parent.appendChild(baseTag);

  QDomElement preTag = doc.createElement("PreCompile");
  baseTag.appendChild(preTag);

  foreach (KitUserAction* ua, mPreUserActions)
  {
    ua->serialize(doc, preTag, eh);
  }

  QDomElement postTag = doc.createElement("PostCompile");
  baseTag.appendChild(postTag);

  foreach (KitUserAction* ua, mPostUserActions)
  {
    ua->serialize(doc, postTag, eh);
  }
}

void KitManifest::serialize(QDomDocument& doc, QDomElement& parent)
{
  QDomElement root = XmlHelper::addElement(doc, parent, "Manifest");

  qDebug() << "Serializing Manifest id" << getID() << "name" << getName() << "descr" << getDescription();
  
  root.setAttribute("version", MODELKIT_VERSION_NUMBER);
  root.setAttribute(MANIFEST_ID, mID);
  root.setAttribute(MANIFEST_COPY_FILES, mCopyFiles ? "true" : "false");
  root.setAttribute(MANIFEST_ASK_ROOT, mAskRoot ? "true" : "false");

  XmlHelper::addElement(doc, root, CARBON_VERSION, mSoftwareVersion);
  XmlHelper::addElement(doc, root, CARBON_RELEASE, mRelease);
  XmlHelper::addElement(doc, root, MANIFEST_DESCRIPTION, mDescription);
  XmlHelper::addElement(doc, root, MANIFEST_NAME, mName);

  XmlErrorHandler eh;

  // Save checkstates
  serializeFileChecks(doc, root, &eh);

  // Save project settings
  serializeProjectSettings(doc, root, &eh);

  // Save the signatures
  serializeRTLFiles(doc, root, &eh);

  // Save the optional signatures (we still do this for compatability)
  serializeOptionalRTLFiles(doc, root, &eh);

  // Save the embedded files
  serializeFiles(doc, root, &eh);

  // Save the components
  serializeComponents(doc, root, &eh);

  // Save the user Actions
  serializeUserActions(doc, root, &eh);

  // Save the compiler Switches
  serializeCompilerSwitches(doc, root, &eh);

  // Save the model kit features
  serializeModelKitFeatures(doc, root, &eh);

  qDebug() << "Finished Manifest id" << getID();

}

void KitManifest::serialize(const QString& )
{
  INFO_ASSERT(false, "Should not be calling this");
}

void KitManifest::deserialize(QDomElement& manifestElem)
{
  mErrorHandler.clear();
  mPreserveHashes = true;
  mFileList.clear();
  mUserFileList.clear();
  mComponents.clear();
  mPreUserActions.clear();
  mPostUserActions.clear();
  mOptRTLFileList.clear();
  mRTLFileList.clear();
  mCompilerSwitches.clear();
  mModelKitFeatures.clear();

  mVersion = manifestElem.attribute("version");

  if (!manifestElem.firstChildElement(MANIFEST_DESCRIPTION).text().isEmpty())
   mDescription = manifestElem.firstChildElement(MANIFEST_DESCRIPTION).text();
 
  if (!manifestElem.firstChildElement(MANIFEST_NAME).text().isEmpty())
    mName = manifestElem.firstChildElement(MANIFEST_NAME).text();

  if (!manifestElem.firstChildElement(CARBON_VERSION).text().isEmpty())
    mSoftwareVersion = manifestElem.firstChildElement(CARBON_VERSION).text();

  if (!manifestElem.firstChildElement(CARBON_RELEASE).text().isEmpty())
    mRelease = manifestElem.firstChildElement(CARBON_RELEASE).text();

  if (!manifestElem.attribute(MANIFEST_COPY_FILES).isEmpty())
    mCopyFiles = manifestElem.attribute(MANIFEST_COPY_FILES) == "false" ? false : true;

  if (!manifestElem.attribute(MANIFEST_ID).isEmpty())
    mID = manifestElem.attribute(MANIFEST_ID).toUInt();

  if (!manifestElem.attribute(MANIFEST_ASK_ROOT).isEmpty())
    mAskRoot = manifestElem.attribute(MANIFEST_ASK_ROOT) == "false" ? false : true;

  QDomNode n = manifestElem.firstChild();
  while(!n.isNull())
  {
    QDomElement e = n.toElement(); // try to convert the node to an element.
    n = n.nextSibling();

    if (!e.isNull() && !e.isComment())
    {
      if ("Signatures" == e.tagName())
        deserializeRTLFiles(e, &mErrorHandler);
      else if ("OptionalSignatures" == e.tagName())
        deserializeOptionalRTLFiles(e, &mErrorHandler);
      else if ("Files" == e.tagName())
        deserializeFiles(e, &mErrorHandler);
      else if ("Project" == e.tagName())
        deserializeProjectSettings(e, &mErrorHandler);
      else if ("Components" == e.tagName())
        deserializeComponents(e, &mErrorHandler);
      else if ("UserActions" == e.tagName())
        deserializeUserActions(e, &mErrorHandler);
      else if ("FileCheckboxes" == e.tagName())
        deserializeFileChecks(e, &mErrorHandler);
      else if ("CompilerSwitches" == e.tagName())
        deserializeCompilerSwitches(e, &mErrorHandler);
      else if ("ModelKitFeatures" == e.tagName())
        deserializeModelKitFeatures(e, &mErrorHandler);
    }
  }

  // Upward compatability stuff here.

  // walk the optionalfiles
  // if the checked="false" and the file is in the optional section, mark it as copied.
  foreach(KitFile* kf, mOptRTLFileList)
  {
    RTLFileOptions* rfu = mFileOptionsMap[kf->getNormalizedName()];
    rfu->mCopied = true;
    rfu->mHashed = kf->getCheckHash();
  }
  
  // Fix the hashed flag
  foreach(RTLFileOptions* rfu, mFileOptionsMap.values())
  {
    bool checked = rfu->mCopied;
    bool foundFile = false;
    foreach(KitFile* kf, mRTLFileList)
    {
      if (kf->getNormalizedName() == rfu->mFileName)
      {
        foundFile=true;
        break;
      }
    }

    if (!checked && !foundFile)
      rfu->mHashed = false;
    
  }
 
  

}


XmlErrorHandler* KitManifest::deserialize(const QString& xmlFile)
{
  mPreserveHashes = true;

  mFileList.clear();
  mUserFileList.clear();
  mComponents.clear();
  mPreUserActions.clear();
  mPostUserActions.clear();
  mRTLFileList.clear();

  QFile file(xmlFile);

  if (file.open(QFile::ReadOnly | QFile::Text))
  {
    QDomDocument doc;
    int errorLine;
    int errorCol;
    QString errStr;

    QFileInfo templateFi(xmlFile);

    if (doc.setContent(&file, true, &errStr, &errorLine, &errorCol))
    { 
      QDomElement docElem = doc.documentElement();
      deserialize(docElem);
    }
  }
  return &mErrorHandler;
}



void KitManifest::copyManifest(KitManifest* src)
{
  setDescription(src->getDescription());
  setName(src->getName());
  setLibraryName(src->getLibraryName());
  setVersion(src->getVersion());
  setLicenseKey(src->getLicenseKey());
  setCopyFiles(src->getCopyFiles());
  setAskRoot(src->getAskRoot());

  foreach(KitFile* kf, src->getRTLFiles())
  {
    KitFile* newKf = addRTLFile(kf->getActualName(), kf->getNormalizedName());
    newKf->setHash(kf->getHash());
  }

  foreach(KitFile* kf, src->getFiles())
  {
    KitFile* newKf = addFile(kf->getActualName(), kf->getNormalizedName(), kf->getFileType(), false);
    newKf->setHash(kf->getHash());
  }

  foreach(KitFile* kf, src->getUserFiles())
  {
    KitFile* newKf = addFile(kf->getActualName(), kf->getNormalizedName(), kf->getFileType(), true);
    newKf->setHash(kf->getHash());
  }

  foreach(KitUserAction* ua, src->getPreUserActions())
  {
    KitUserAction* na = ua->copyAction();
    addPreUserAction(na);
  }
  foreach(KitUserAction* ua, src->getPostUserActions())
  {
    KitUserAction* na = ua->copyAction();
    addPostUserAction(na);
  }

  QMap<QString,bool>& srcCheckMap = src->getFileCheckedMap();
  foreach(QString key, srcCheckMap.keys())
  {
    mFileCheckMap[key] = srcCheckMap[key];
  }
}

KitManifest* KitManifests::findManifest(quint32 id)
{
  foreach (KitManifest* mf, mManifests)
  {
    if (mf->getID() == id)
      return mf;
  }
  return NULL;
}


KitManifest* KitManifests::findManifest(const QString& manifestName)
{
  foreach (KitManifest* mf, mManifests)
  {
    if (mf->getName() == manifestName)
      return mf;
  }
  return NULL;
}



void KitManifest::prepareManifest(const QString& manifestRoot, KitManifest* mf)
{
  qDebug() << "preparing" << manifestRoot << mf->getID();
}

// Input File Action
void KitActionInputFile::serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler* errorHandler)
{
  QDomElement element = addElement(doc, parent, "InputFile");

  KitUserAction::serialize(doc, element, errorHandler);

  QString choice;
  switch (mChoose)
  {
  case KitActionInputFile::File: choice = "Filename"; break;
  case KitActionInputFile::Directory: choice = "Directory"; break;
  }

  addElement(doc, element, "Choose", choice);
  addElement(doc, element, "DefaultName", getDefaultFileName());
  addElement(doc, element, "DialogTitle", getDialogTitle());
  addElement(doc, element, "DefaultDirectory", getDefaultDirectory());
  addElement(doc, element, "FileTypes", getFileTypes().join(";"));
}
KitUserAction* KitActionInputFile::deserialize(const QDomElement& e, XmlErrorHandler* eh)
{
  if ("InputFile" == e.tagName())
  {
    KitActionInputFile* action = new KitActionInputFile();
    
    // base class options
    KitUserAction::deserialize(action, e, eh);

    QDomNode n = e.firstChild();
    while(!n.isNull())
    {
      QDomElement element = n.toElement(); // try to convert the node to an element.
      n = n.nextSibling();

      if (element.isNull() && element.isComment())
        continue;

      if ("DefaultName" == element.tagName())
        action->setDefaultFileName(element.text());
      else if ("Choose" == element.tagName())
      {
        QString choice = element.text();
        if (choice == "Filename")
          action->setChoice(KitActionInputFile::File);
        else if (choice == "Directory")
          action->setChoice(KitActionInputFile::Directory);
      }
      else if ("DialogTitle" == element.tagName())
        action->setDialogTitle(element.text());
      else if ("DefaultDirectory" == element.tagName())
        action->setDefaultDirectory(element.text());
      else if ("FileTypes" == element.tagName())
        action->setFileTypes(element.text().split(";"));
    }
    return action;
  }

  // Not this elements
  return NULL;
}

bool KitActionInputFile::execute(ModelKitUserWizard* wiz, QWidget* parent, KitUserContext* uc)
{
  QString defaultFile = uc->expandName(getDefaultFileName());

  QString fileFilter = getFileTypes().join(";");

  QString fileName;
  QString currentDir;

  if (wiz->getAnswers())
  {
    QDomElement e = wiz->getAnswers()->getAction(getID());
    if (e.isElement())
      fileName = expandVariables(e.firstChildElement("FileName").text()); 
    else
    {
      QString msg = QString("Expected InputFile answer for id: %1").arg(getID());
      theApp->abortApplication(msg, 99);
    }
  }
  else
  {
    if (getChoice() == KitActionInputFile::File)  
      fileName = QFileDialog::getOpenFileName(parent, getDialogTitle(), defaultFile, fileFilter);
    else if (getChoice() == KitActionInputFile::Directory)
      fileName = QFileDialog::getExistingDirectory(parent, getDialogTitle(), defaultFile);    
  }

  QDomElement re = wiz->recordAction(this);

  UtString currDir;
  OSGetCurrentDir(&currDir);
  XmlHelper::addElement(wiz->getDocRecord(), re, "CurrentDirectory", currDir.c_str());
  XmlHelper::addElement(wiz->getDocRecord(), re, "FileName", fileName);

  qDebug() << "Executing Action Input File" << defaultFile << fileFilter << fileName;

  if (!fileName.isEmpty())
  {
    QString varName = getVariableName();
    QString destName = uc->expandName(getDefaultDirectory());
    if (!varName.isEmpty())
      uc->setVariable(varName, fileName);

    QString destFileName = uc->expandName(destName);

    if (getChoice() == KitActionInputFile::File)
    {
      QFileInfo fi(destFileName);
      QDir dir = fi.dir();
      if (!dir.mkpath(fi.path()))
      {
        qDebug() << "error: unable to create directory" << fi.path();
        return false;
      }
      if (fi.exists())
      {
        if (!QFile::remove(destFileName))
        {
         qDebug() << "error: unable to remove file" << fileName;
        }
      }

      if (QFile::copy(fileName, destFileName))
      {
        qDebug() << "placing" << fileName << "into" << destFileName;
        return true;
      }
      else
      {
        QMessageBox::critical(NULL, MODELSTUDIO_TITLE,  
          QString("Error: Unable to copy file %1 to %2")
          .arg(fileName).arg(destFileName) );
        qDebug() << "error: unable to copy file" << fileName << "to" << destFileName;
      }

      return false;
    }
    else
      return true;
  }

  return false;
}

void KitUserAction::deserialize(KitUserAction* action, const QDomElement& e, XmlErrorHandler*)
{
  QString id = e.attribute("guid");
  if (id.isEmpty())
    action->setID(createGUID());
  else
    action->setID(id);

  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    n = n.nextSibling();

    if (element.isNull() && element.isComment())
      continue;
    if ("Description" == element.tagName())
      action->setDescription(element.text());
    else if ("VariableName" == element.tagName())
      action->setVariableName(element.text());
  }
}


void KitUserAction::serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  QString id = getID();
  if (id.isEmpty())
    setID(createGUID());
 
  parent.setAttribute("guid", getID());
  addElement(doc, parent, "Description", getDescription());
  addElement(doc, parent, "VariableName", getVariableName());
}

KitActionInputFile::KitActionInputFile() : KitUserAction(KitUserAction::eActionInputFile)
{
  qDebug() << "constructed KitActionInputFile";
  qDebug() << "kind" << getKind();
  mFileTypes << "Input File (* *.*)";
  mChoose = KitActionInputFile::File;
}

QString KitUserAction::getKindString() const
{
  switch (mKind)
  {
  case KitUserAction::eActionInputFile:
    return "Input File";
    break;
  case KitUserAction::eActionExecuteScript:
    return "Shell Script";
    break;
  case KitUserAction::eActionRunScript:
    return "Modelstudio Script";
    break;
  }

  return "Undefined";
}

// Execute Script
KitActionExecuteScript::KitActionExecuteScript() : KitUserAction(KitUserAction::eActionExecuteScript)
{
  mExitCode = -1;
  mValidExitCode = 0;
}

// Execute a shell script
bool KitActionExecuteScript::execute(ModelKitUserWizard*, QWidget*, KitUserContext* uc)
{
  QString scriptName = getScriptName();
  QString defaultDir = getDefaultDirectory();

  qDebug() << "execute script" << scriptName << "directory" << defaultDir << "args" << getScriptArguments();

  QStringList args;
  foreach (QString arg, getScriptArguments())
  {
    QString expandedArg = uc->expandName(arg);
    args.append(expandedArg);
  }

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  CarbonConsole::CommandMode mode = CarbonConsole::Local;
#if pfWINDOWS
  mode = CarbonConsole::Remote;
  if (!proj->windowsAuthenticate())
    return false;
#endif
  pw->makeConsoleVisible();

  QString program = proj->getUnixEquivalentPath(uc->expandName(scriptName));
  QString subDir = proj->getUnixEquivalentPath(uc->expandName(getDefaultDirectory()));

  QFile scriptFile(program);
  scriptFile.setPermissions(QFile::ReadOwner|QFile::WriteOwner|QFile::ExeOwner);

  QString fullProgram = QString("%1/%2").arg(subDir).arg(program);

  qDebug() << "executescript" << fullProgram << args << subDir;

  QApplication::setOverrideCursor(Qt::WaitCursor);
  int exitCode = pw->getConsole()->executeCommandAndWait(mode, CarbonConsole::Command, fullProgram, args, subDir);
  QApplication::restoreOverrideCursor();

  qDebug() << "script exited exitCode:" << exitCode << "valid code" << getValidExitCode();

  if (exitCode != getValidExitCode())
  {
    QMessageBox::critical(NULL, "Carbon Model Kit", 
      QString("Script %1 returned error status %2").arg(fullProgram).arg(exitCode));
  }

  return exitCode == getValidExitCode();

}

void KitActionExecuteScript::serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler* errorHandler)
{
  QDomElement element = addElement(doc, parent, "ExecuteScript");

  KitUserAction::serialize(doc, element, errorHandler);

  addElement(doc, element, "ScriptName", getScriptName());
  addElement(doc, element, "ScriptArguments", getScriptArguments().join(" "));
  addElement(doc, element, "ValidExitCode", QString(getValidExitCode()));
  addElement(doc, element, "DefaultDirectory", getDefaultDirectory());
}
KitUserAction* KitActionExecuteScript::deserialize(const QDomElement& e, XmlErrorHandler* eh)
{
  if ("ExecuteScript" == e.tagName())
  {
    KitActionExecuteScript* action = new KitActionExecuteScript();
    
    // base class options
    KitUserAction::deserialize(action, e, eh);

    QDomNode n = e.firstChild();
    while(!n.isNull())
    {
      QDomElement element = n.toElement(); // try to convert the node to an element.
      n = n.nextSibling();

      if (element.isNull() && element.isComment())
        continue;

      if ("ScriptName" == element.tagName())
        action->setScriptName(element.text());
      else if ("ScriptArguments" == element.tagName())
        action->setScriptArguments(element.text().split(" "));
      else if ("ValidExitCode" == element.tagName())
        action->setValidExitCode(element.text().toInt());
      else if ("DefaultDirectory" == element.tagName())
        action->setDefaultDirectory(element.text());
    }
    return action;
  }

  // Not this elements
  return NULL;
}

// UserScripts


void KitActionRunScript::scriptException(const QScriptValue &, int)
{
  qDebug() << "A Script Exception has occurred";

  mExceptionOccurred = true;
}

// Execute a modelstudio javascript
bool KitActionRunScript::execute(ModelKitUserWizard* wiz, QWidget*, KitUserContext* uc)
{
  bool retValue = true;
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  pw->makeConsoleVisible();

  mExceptionOccurred = false;
  mEngine = new ScriptingEngine();

  CQT_CONNECT(mEngine->getAgent(), scriptException(const QScriptValue &, int), this, scriptException(const QScriptValue &, int));

  mEngine->registerProperty("ModelKit", wiz->getModelKit());
  mEngine->registerProperty("Manifest", uc->getManifest());
  mEngine->registerProperty("Context", uc);

  bool debugger = getenv("CARBON_MODELKIT_DEBUGGER");
  if (debugger)
    mEngine->setEnableDebugger(true);

  QString scriptName = uc->expandName(getScriptName());
  QFileInfo fi(scriptName);
  bool fileExists = fi.exists();

  if (fileExists)
  {
    qDebug() << "executing useraction" << scriptName;
    if (fi.suffix().toLower() == "mjs")
    {
      QScriptValue rv = mEngine->runPlugin(scriptName);
      retValue = rv.toBoolean();
    }
    else
    {
      QScriptValue rv = mEngine->runScript(scriptName);
      if (rv.isBoolean())
        retValue = rv.toBoolean();
    }

    if (mExceptionOccurred)
      return false;
  }
  else
  {
    QMessageBox::critical(NULL, "Carbon ModelKit",
      QString("Missing script file: %1").arg(scriptName));
    return false;
  }

  return retValue;
}

KitActionRunScript::ScriptEventTime KitActionRunScript::stringToEventTime(const QString& str)
{
  if (str == "BeforeModelCompilation")
    return BeforeModelCompilation;
  else if (str == "AfterModelCompilation")
    return AfterModelCompilation;
  else
    return BeforeModelCompilation;
}

QString KitActionRunScript::getEventTimeString() const
{
  switch (mEventTime)
  {
  default:
  case BeforeModelCompilation:
    return "BeforeModelCompilation";
    break;
  case AfterModelCompilation:
    return "AfterModelCompilation";
    break;
  }
}

void KitActionRunScript::serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler* errorHandler)
{
  QDomElement element = addElement(doc, parent, "RunScript");

  KitUserAction::serialize(doc, element, errorHandler);

  addElement(doc, element, "ScriptName", getScriptName());
  addElement(doc, element, "EventTime", getEventTimeString());
}

// Static deserialize
KitUserAction* KitActionRunScript::deserialize(const QDomElement& e, XmlErrorHandler* eh)
{
  if ("RunScript" == e.tagName())
  {
    KitActionRunScript* action = new KitActionRunScript();
    
    // base class options
    KitUserAction::deserialize(action, e, eh);

    QDomNode n = e.firstChild();
    while(!n.isNull())
    {
      QDomElement element = n.toElement(); // try to convert the node to an element.
      n = n.nextSibling();

      if (element.isNull() && element.isComment())
        continue;

      if ("ScriptName" == element.tagName())
        action->setScriptName(element.text());
      else if ("EventTime" == element.tagName())
        action->setEventTime(stringToEventTime(element.text()));
    }
    return action;
  }
 return NULL;
}

QString KitManifest::encryptBuffer(const QString& buffer)
{
  UtEncryptDecrypt enc;
  UtString inputBuffer;
  inputBuffer << buffer;
  UtString result = enc.encryptBuffer(inputBuffer.c_str());
  return result.c_str();
}

QString KitManifest::decryptBuffer(const QString& buffer)
{
  bool status;
  UtEncryptDecrypt enc;
  UtString inputBuffer;
  inputBuffer << buffer;
  UtString result = enc.decryptBuffer(inputBuffer.c_str(), &status);
  return result.c_str();
}
bool KitManifest::decryptFile(const QString& inputFileName)
{
  UtString filename;
  filename << inputFileName;
  UtEncryptDecrypt enc;
  UtString errMsg;
  return enc.unprotectEFFile(filename.c_str(), &errMsg);
}
bool KitManifest::encryptFile(const QString& inputFileName)
{
  UtString filename;
  filename << inputFileName;
  UtEncryptDecrypt enc;
  UtString errMsg;
  return enc.protectEFFile(filename.c_str(), &errMsg);
}

void KitManifest::registerIntellisense(JavaScriptAPIs* apis)
{
  apis->loadAPIForVariable("Manifest", &KitManifest::staticMetaObject);
  apis->loadAPIForVariable("Context", &KitUserContext::staticMetaObject);
  apis->loadAPIForVariable("ContextEnums", &ContextEnums::staticMetaObject);
  apis->loadAPIForVariable("Test", &KitTest::staticMetaObject);
}

// Allows properties of these types to work through intellisense
void KitManifest::registerScriptTypes(QMap<QString, const QMetaObject*>& map)
{
  map["KitUserContext*"] = &KitUserContext::staticMetaObject;
  map["KitFile*"] = &KitFile::staticMetaObject;
  map["KitManifest*"] = &KitManifest::staticMetaObject;
  map["KitTest*"] = &KitTest::staticMetaObject;
}

void KitManifest::registerTypes(QScriptEngine* engine)
{
  if (engine)
  {    
    // Allows an object to return a property of these types
    qScriptRegisterQObjectMetaType<KitFile*>(engine);
    qScriptRegisterQObjectMetaType<KitUserContext*>(engine);
    qScriptRegisterQObjectMetaType<KitTest*>(engine);
  }
}


void KitManifest::removeAllItems()
{
  mOptRTLFileList.clear();
  mRTLFileList.clear();
  mFileCheckMap.clear();
  mFileList.clear();
  mUserFileList.clear();
  mPreUserActions.clear();
  mPostUserActions.clear();
  mFileOptionsMap.clear();
}

QString KitUserAction::createGUID()
{
  QString strGuid = QString("%1%2%3%4%5%6%7%8")
              .arg(qrand(), 2, 16, QChar('0'))
              .arg(qrand(), 2, 16, QChar('0'))
              .arg(qrand(), 2, 16, QChar('0'))
              .arg(qrand(), 2, 16, QChar('0'))
              .arg(qrand(), 2, 16, QChar('0'))
              .arg(qrand(), 2, 16, QChar('0'))
              .arg(qrand(), 2, 16, QChar('0'))
              .arg(qrand(), 2, 16, QChar('0'));

  qDebug() << "Created guid" << strGuid;

  return strGuid;
}


double KitUserContext::getDouble(const QString& questionId, const QString & title, const QString & label, double value, double minValue, double maxValue, int decimals, Qt::WindowFlags f)
{
  bool ok = true;
  double result;
  if (mWizard->getAnswers())
    result = mWizard->getAnswers()->getQuestion(questionId).toDouble();
  else
    result = QInputDialog::getDouble(mWizard, title, label, value, minValue, maxValue, decimals, &ok, f);
  
  if (ok)
    mWizard->recordQuestion(questionId, result);
  return result;
}

int KitUserContext::getInteger(const QString& questionId, const QString & title, const QString & label, int value, int minValue, int maxValue, int step, Qt::WindowFlags f)
{
  bool ok = true;
  int result;
  
  if (mWizard->getAnswers() && mWizard->getAnswers()->hasQuestion(questionId))
    result = mWizard->getAnswers()->getQuestion(questionId).toInt();
  else
    result = QInputDialog::getInteger(mWizard, title, label, value, minValue, maxValue, step, &ok, f);
  if (ok)
    mWizard->recordQuestion(questionId, result);
  return result;
}

QString KitUserContext::getItem(const QString& questionId, const QString & title, const QString & label, const QStringList& list, int current, bool editable, Qt::WindowFlags f)
{
  bool ok = true;
  QString result;

  if (mWizard->getAnswers() && mWizard->getAnswers()->hasQuestion(questionId))
    result = mWizard->getAnswers()->getQuestion(questionId).toString();
  else
    result = QInputDialog::getItem(mWizard, title, label, list, current, editable, &ok, f);
  
  if (ok)
    mWizard->recordQuestion(questionId, result);
  return result;
}

QString KitUserContext::getText(const QString& questionId, const QString & title, const QString & label, QLineEdit::EchoMode mode, const QString & text, Qt::WindowFlags f)
{
  bool ok = true;
  QString result;
  if (mWizard->getAnswers() && mWizard->getAnswers()->hasQuestion(questionId))
    result = mWizard->getAnswers()->getQuestion(questionId).toString();
  else
    result = QInputDialog::getText(mWizard, title, label, mode, text, &ok, f);
  
  if (ok)
    mWizard->recordQuestion(questionId, result);

  return result;
}


QString KitUserContext::getOpenFileName(const QString& questionId, const QString & caption, 
                                        const QString& initialDir, const QString& filter,
                                        const QString & selectedFilter)
{
  QString selFilter = selectedFilter;

  QString result;
  if (mWizard->getAnswers() && mWizard->getAnswers()->hasQuestion(questionId))
    result = expandName(mWizard->getAnswers()->getQuestion(questionId).toString());
  else
    result = QFileDialog::getOpenFileName(mWizard, caption, initialDir, filter, &selFilter);
  
  mWizard->recordFilename(questionId, result);
  
  return result;
}

QScriptValue KitUserContext::getOpenFileNames(const QString& questionId, const QString & caption, 
                                        const QString& initialDir, const QString& filter,
                                        const QString & selectedFilter)
{
  QString selFilter = selectedFilter;
  QStringList results;
  if (mWizard->getAnswers() && mWizard->getAnswers()->hasQuestion(questionId))
  {
    QStringList items = mWizard->getAnswers()->getQuestion(questionId).toStringList();
    foreach(QString fn, items)
      results.append(expandName(fn));
  }
  else
    results = QFileDialog::getOpenFileNames(mWizard, caption, initialDir, filter, &selFilter);

  mWizard->recordFilenames(questionId, results);
  return qScriptValueFromSequence(context()->engine(), results);
}

QString KitUserContext::getExistingDirectory(const QString& questionId, const QString& caption, const QString& dir)
{
  QString result;

  if (mWizard->getAnswers() && mWizard->getAnswers()->hasQuestion(questionId))
    result = mWizard->getAnswers()->getQuestion(questionId).toString();
  else
    result = QFileDialog::getExistingDirectory(mWizard, caption, dir);

  mWizard->recordFilename(questionId, result);
  return result;
}



KitUserContext::KitUserContext(QObject* parent,
                               ModelKitUserWizard* wiz, KitManifest* mf)
                               : QObject(parent)
{
  mWizard = wiz;
  mManifest = mf;
  mModelTarget = ContextEnums::Unix;
  mModelSymbols = ContextEnums::FullSymbols;
  mPlaybackMode = false;
}

void KitUserContext::registerTypes(QScriptEngine* engine)
{
  qScriptRegisterEnumMetaType<ContextEnums::ModelTarget>(engine); 
  qScriptRegisterEnumMetaType<ContextEnums::ModelSymbols>(engine); 

  // Static register for classes with Q_ENUMS
  QScriptValue ctxEnums = engine->newQMetaObject(&ContextEnums::staticMetaObject);
  engine->globalObject().setProperty("ContextEnums", ctxEnums);
}

void KitUserContext::registerScriptTypes(QMap<QString, const QMetaObject*>& map)
{
  map["ContextEnums"] = &ContextEnums::staticMetaObject;
}

void KitUserContext::registerIntellisense(JavaScriptAPIs* apis)
{
  apis->loadAPIForVariable("ContextEnums", &ContextEnums::staticMetaObject);
}


void KitTest::registerTypes(QScriptEngine* engine)
{
  qScriptRegisterEnumMetaType<TestEnums::TestExecutionPhase>(engine); 

  // Static register for classes with Q_ENUMS
  QScriptValue ctxEnums = engine->newQMetaObject(&TestEnums::staticMetaObject);
  engine->globalObject().setProperty("TestEnums", ctxEnums);

  qScriptRegisterQObjectMetaType<KitAnswer*>(engine);
  qScriptRegisterQObjectMetaType<KitAnswers*>(engine);
  qScriptRegisterQObjectMetaType<KitAnswerGroup*>(engine);
  qScriptRegisterQObjectMetaType<KitAnswerGroups*>(engine);
}

void KitTest::registerScriptTypes(QMap<QString, const QMetaObject*>& map)
{
  map["TestEnums"] = &TestEnums::staticMetaObject;

  map["KitAnswer*"] = &KitAnswer::staticMetaObject;
  map["KitAnswerGroup*"] = &KitAnswerGroup::staticMetaObject;

  map["KitAnswers*"] = &KitAnswers::staticMetaObject;
  map["KitAnswerGroups*"] = &KitAnswerGroups::staticMetaObject;
}

void KitTest::registerIntellisense(JavaScriptAPIs* apis)
{
  apis->loadAPIForVariable("TestEnums", &TestEnums::staticMetaObject);
}

// ctor
KitTest::KitTest(QObject* parent) : QObject(parent)
{
  mAnswers = new KitAnswers(this);
  mAnswerGroups = new KitAnswerGroups();
}


bool KitTest::loadPortalAnswers(const QString& answersFile)
{
  mAnswers->removeAll();
  mAnswerGroups->removeAll();

  QFile file(answersFile);
  if (file.open(QFile::ReadOnly | QFile::Text))
  {
    QDomDocument doc;
    int errorLine;
    int errorCol;
    QString errStr;
    if (doc.setContent(&file, true, &errStr, &errorLine, &errorCol))
    { 
      QDomElement rootElem = doc.documentElement();

      if (rootElem.tagName() == "Answers")
      {
        // //Answers/A
        for(QDomElement ae = rootElem.firstChildElement("A"); ae.isElement(); ae = ae.nextSiblingElement("A"))
        {
          QString id = ae.attribute("id");
          QString displayValue = ae.attribute("displayValue");
          QString value= ae.attribute("value");
          KitAnswer* ka = new KitAnswer();
          ka->setDisplayValue(displayValue);
          ka->setID(id);
          ka->setValue(value);
          mAnswers->addAnswer(ka);
        }

        for(QDomElement e = rootElem.firstChildElement("Group"); e.isElement(); e = e.nextSiblingElement("Group"))
        {
          QString groupId = e.attribute("id");
          QString index = e.attribute("index");
          KitAnswerGroup* kag = new KitAnswerGroup(this);
          kag->setID(groupId);
          kag->setIndex(index.toUInt());
          mAnswerGroups->addAnswerGroup(kag);

          for(QDomElement ae = e.firstChildElement("A"); ae.isElement(); ae = ae.nextSiblingElement("A"))
          {
            QString id = ae.attribute("id");
            QString displayValue = ae.attribute("displayValue");
            QString value= ae.attribute("value");
            KitAnswer* ka = new KitAnswer();
            ka->setDisplayValue(displayValue);
            ka->setID(id);
            ka->setValue(value);
            kag->getAnswers()->addAnswer(ka);
          }
        }

        return true;
      }     
    }
  }
  else
    qDebug() << "ERROR: unable to load: " << answersFile;

  return false;
}
