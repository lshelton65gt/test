/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/CarbonVersion.h"
#include "util/UtString.h"
#include "util/ArgProc.h"

#include <QWorkspace>
#include <QSignalMapper>
#include <QtGui>

#include "gui/CQt.h"
#include "CQtWizardContext.h"
#include "shell/ReplaySystem.h"
#include "shell/carbon_misc.h"

#include "CarbonModelsWidget.h"
#include "shell/ReplaySystem.h"
#include "SettingsEditor.h"
#include "CarbonInstanceOptions.h"
#include "SimulationHistoryWidget.h"
#include "OnDemandTraceWidget.h"
#include "CarbonRuntimeState.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonDatabaseContext.h"

void CarbonModelsWidget::closeEvent(QCloseEvent *ev)
{
  qDebug() << "Closing Widget";

  ev->accept(); 

  MDIWidget::closeEvent(this);
}


CarbonModelsWidget::CarbonModelsWidget(CarbonMakerContext *ctx, CarbonRuntimeState *runtimeState, QWidget *parent)
  :QTreeWidget(parent), mUpdating(false),
   mContext(ctx), mRuntimeState(runtimeState), mCurrentInstanceOptions(NULL)
{
  setAttribute(Qt::WA_DeleteOnClose);

  INFO_ASSERT(ctx->getCarbonModelsWidget() == NULL, "only one .css file opened at once");
  ctx->putCarbonModelsWidget(this);

  setObjectName(QString::fromUtf8("CarbonModelsList"));
  setEditTriggers(QAbstractItemView::AllEditTriggers);
  setAlternatingRowColors(true);
  setSelectionMode(QAbstractItemView::SingleSelection);
  setSelectionBehavior(QAbstractItemView::SelectRows);
  setRootIsDecorated(false);
  setColumnCount(colLAST);
  setIconSize(QSize(24,24));
  setItemDelegate(new CarbonModelDelegate(this, this));

  QStringList labels;
  labels << "Component"; // colNAME
  labels << "OnDemand Control";  // colONDEMAND
  labels << "Replay Control";    // colREPLAY

  setHeaderLabels(labels);

  header()->setResizeMode(colNAME, QHeaderView::ResizeToContents);
  header()->setResizeMode(colONDEMAND, QHeaderView::Interactive);
  header()->setResizeMode(colREPLAY, QHeaderView::Interactive);

  CQT_CONNECT(this, itemSelectionChanged(), this, itemSelectionChanged());
  CQT_CONNECT(this, itemClicked(QTreeWidgetItem*,int), this, itemClicked(QTreeWidgetItem*,int));

#if CCONTROL_ROOT_COMP
  CarbonInstanceOptions* root_opts = new CarbonInstanceOptions(mRuntimeState, NULL);
  mCarbonAllComponentsItem = new CarbonAllComponentsItem(this, root_opts);
#endif
}

CarbonModelsWidget::~CarbonModelsWidget()
{
}

bool CarbonModelsWidget::selectComponent(const char* compName)
{
  QTreeWidgetItem *itemToSelect = NULL;
  for (UInt32 i = 0, n = topLevelItemCount(); (i < n); ++i) {
    // get the name of the item
    QTreeWidgetItem* item = topLevelItem(i);
    UtString name;
    name << item->text(0);

    // Look for a match.  Ensure only the requested component is
    // selected.
    if (name == compName) {
      // We don't allow multiple selection right now, so don't select
      // the item yet.  If another component later in the list is
      // currently selected, this will cause an assertion in
      // itemSelectionChanged().  Instead, record this item and select
      // it after everything has been deselected.
      itemToSelect = item;
    }
    item->setSelected(false);
  }
    
  if (itemToSelect == NULL) {
    return false;
  }

  itemToSelect->setSelected(true);
  return true;
}

CarbonOptions* CarbonModelsWidget::getOptionsForComponent(const char* compName)
{
  CarbonOptions *opt = NULL;
  for (UInt32 i = 0, n = topLevelItemCount(); (opt == NULL) && (i < n); ++i) {
    // get the name of the item
    QTreeWidgetItem* item = topLevelItem(i);
    UtString name;
    name << item->text(0);

    // Look for a match
    if (name == compName) {
      CarbonModelInstanceItem *instanceItem = dynamic_cast<CarbonModelInstanceItem*>(item);
      opt = instanceItem->getOptions();
    }
  }

  return opt;
}

void CarbonModelsWidget::removeOnDemandExclusions(const QStringList& stateList, const QStringList& inputList)
{
  // For each component from this project, remove the list of state
  // exclusion and idle deposit signals.  First, we need to have a
  // project with a valid database.
  CarbonProjectWidget* projWidget = mContext->getCarbonProjectWidget();
  CarbonProject* project = NULL;
  if (projWidget != NULL) {
    project = projWidget->project();
  }
  if (project != NULL) {
    // Extract the project ID string from the .db file
    CarbonDatabaseContext* dbContext = project->getDbContext();
    if (dbContext != NULL) {
      UtString projectID;
      projectID << carbonDBGetIdString(dbContext->getDB());

      CarbonRuntimeState* rs = getRuntimeState();
      CarbonReplaySystem* carbonSystem = rs->getCarbonSystem();

      // Loop over the components, looking for matching ID
      for (UInt32 i = 0, n = topLevelItemCount(); i < n; ++i) {
        // get the name of the item
        QTreeWidgetItem* item = topLevelItem(i);
        UtString name;
        name << item->text(0);
        // Look up the component by name.
        CarbonSystemComponent *component = carbonSystem->findComponent(name.c_str());
        if ((component != NULL) && (projectID == component->getID())) {
          // Get the component options for this item
          CarbonModelInstanceItem *instanceItem = dynamic_cast<CarbonModelInstanceItem*>(item);
          CarbonOptions *options = instanceItem->getOptions();

          // Remove the state exclusions
          removeComponentOnDemandExclusions(options, ODM_PROP_EXCLUDED_STATE_SIGNALS, stateList);
          // Repeat for idle deposits
          removeComponentOnDemandExclusions(options, ODM_PROP_EXCLUDED_INPUT_SIGNALS, inputList);
        }
      }
    }
  }
}

void CarbonModelsWidget::removeComponentOnDemandExclusions(CarbonOptions* options, const char* propName, const QStringList& eraseList)
{
  CarbonPropertyValue *propValue = options->getValue(propName);
  if (propValue != NULL) {
    const QStringList &origList = propValue->getValues();
    // We need a modifiable copy of this list
    QStringList newList = origList;
    // For each entry in the property list, see if it's in the list of
    // signals to erase.  This is easier if we transform the list to a
    // set.
    QSet<QString> eraseSet = eraseList.toSet();
    bool anyErased = false;
    QStringList::iterator iter = newList.begin();
    while (iter != newList.end()) {
      const QString &val = *iter;
      if (eraseSet.contains(val)) {
        // Remove it from the list.  This returns the iterator for the next item.
        iter = newList.erase(iter);
        anyErased = true;
      } else {
        ++iter;   // Just move on
      }
    }
    if (anyErased) {
      // Update the property.  Note that we need to go through the
      // CarbonOptions object here, in order to trigger important
      // signals.
      options->putValues(propName, newList);
    }
  }
}

void CarbonModelsWidget::carbonSystemUpdated(CarbonRuntimeState & /*state */, CarbonReplaySystem &guiSystem, CarbonReplaySystem &simSystem)
{
  // JDM: I'm not sure I can reproduce this, but this is situation where infinite
  // recursion through signals & slots can occur, so block it undiplomatically
  if (mUpdating) {
    return;
  }
  mUpdating = true;

  // build a map from comp name to tree item
  typedef UtHashMap<UtString, QTreeWidgetItem*> CompItemMap;
  CompItemMap compItems;
  for (UInt32 i = 0, n = topLevelItemCount(); i < n; ++i) {
    // get the name of the item
    QTreeWidgetItem* item = topLevelItem(i);
    UtString name;
    name << item->text(0);

    // add map item
    compItems[name] = item;
  }

  // update the items for each component, creating new items for any
  // new components found
  for (CarbonReplaySystem::ComponentLoop p(simSystem.loopComponents()); !p.atEnd(); ++p) {
    CarbonSystemComponent* comp = p.getValue();
    const char* name = comp->getName();

    // lookup the item for this component
    QTreeWidgetItem *item = compItems[UtString(name)];
    CarbonModelInstanceItem *instance;
    if (item) {
      // we already have an item for this component; update its properties.
      instance = dynamic_cast<CarbonModelInstanceItem*>(item);
      INFO_ASSERT(instance, "Carbon Models list items must be of type CarbonModelInstanceItem");
      instance->putPropertyValues(guiSystem, simSystem, name);
    }
    else {
      // create a new tree item for this model instance
      instance = new CarbonModelInstanceItem(
#if CCONTROL_ROOT_COMP
        mCarbonAllComponentsItem,
#else
        this,
#endif
        guiSystem, simSystem, name);
      instance->putPropertyValues(guiSystem, simSystem, name);
      instance->setText(0, name);
    }
    
    // now that we've handled this component, delete it from the map
    compItems.erase(UtString(name));
  }

  // the only components left in the map that have items associated
  // with them must have been deleted from the running system.  So
  // delete the tree items associated with them.
  bool selectionChanged = false;
  for (CompItemMap::iterator iter = compItems.begin(); iter != compItems.end(); ++iter) {
    QTreeWidgetItem *item = iter->second;
    if (item) {
      if (item->isSelected()) {
        selectionChanged = true;
      }
      UtString name;
      name << item->text(0);
      remove(name.c_str());
    }
  }

  // if the selection changed, tell everyone about it
  if (selectionChanged) {
    emit itemSelectionChanged();
  }

  mUpdating = false;
}

void CarbonModelsWidget::itemSelectionChanged()
{
  UtStringArray newSelection;

  // update the properties editor with the properties for the selected items
  const QList<QTreeWidgetItem*> &itemList = selectedItems();
  INFO_ASSERT(itemList.size() <= 1, "multi-select not enabled");
  UtString name;

  mCurrentInstanceOptions = NULL;

  if (itemList.size() > 0) {
    QTreeWidgetItem* selected = *itemList.begin();
    CarbonModelInstanceItem* instance
      = dynamic_cast<CarbonModelInstanceItem*>(selected);
    mCurrentInstanceOptions = instance->getInstanceOptions();
    name << selected->text(0);
    newSelection.push_back(name);
  }
  mContext->getSettingsEditor()->setSettings(getCurrentOptions());

  // inform runtime state of the new selection
  mRuntimeState->putSelectedModels(newSelection);
} // void CarbonModelsWidget::itemSelectionChanged

void CarbonModelsWidget::itemClicked(QTreeWidgetItem*,int)
{
  itemSelectionChanged();
}

///////////////////////////////////////////////////////////////////////////////

CarbonModelInstanceItem::CarbonModelInstanceItem(CarbonModelsWidget* parent, CarbonReplaySystem& guiSystem, CarbonReplaySystem& simSystem, const char* compName)
  : QTreeWidgetItem(parent), mInstanceOptions(NULL), mGuiSystem(guiSystem), mSimSystem(simSystem), mComponentName(compName)
{
  mInstanceOptions = new CarbonInstanceOptions(parent->getRuntimeState(), compName);
  setFlags(flags() | Qt::ItemIsEditable);
  setFlags(flags() | Qt::ItemIsUserCheckable);
}

CarbonModelInstanceItem::~CarbonModelInstanceItem()
{
  delete mInstanceOptions;
}

QString CarbonModelInstanceItem::getReplayIcon(CarbonVHMMode mode)
{
  switch (mode)
  {
  default:
  case eCarbonRunNormal:    return ":/cmm/Resources/NormalButton.png"; break;
  case eCarbonRunRecord:    return ":/cmm/Resources/RecordButton.png"; break;
  case eCarbonRunPlayback:  return ":/cmm/Resources/PlayButton.png"; break;
  case eCarbonRunRecover:   return ":/cmm/Resources/RecoverButton.png"; break;
  }
}

QString CarbonModelInstanceItem::getOnDemandIcon(CarbonOnDemandMode mode)
{
  switch (mode)
  {
  default:
  case eCarbonOnDemandLooking:  return ":/cmm/Resources/OnDemandActive.png"; break;
  case eCarbonOnDemandBackoff:  return ":/cmm/Resources/OnDemandActive.png"; break;
  case eCarbonOnDemandIdle:     return ":/cmm/Resources/OnDemandIdle.png"; break;
  case eCarbonOnDemandStopped:  return ":/cmm/Resources/OnDemandStopped.png"; break;
  }
}

void CarbonModelInstanceItem::putPropertyValues(CarbonReplaySystem &guiSystem, CarbonReplaySystem &simSystem, const char *compName)
{
  CarbonSystemComponent* simComp = simSystem.findComponent(compName);
  INFO_ASSERT(simComp, "runtime component not found");

  mInstanceOptions->putPropertyValues(guiSystem, simSystem, compName);

  setIcon(CarbonModelsWidget::colREPLAY, QIcon(getReplayIcon(simComp->getReplayState())));
  setText(CarbonModelsWidget::colREPLAY, mInstanceOptions->getReplayRequestedState());
  
  setIcon(CarbonModelsWidget::colONDEMAND, QIcon(getOnDemandIcon(simComp->getOnDemandState())));
  setText(CarbonModelsWidget::colONDEMAND, mInstanceOptions->getOnDemandRequestedState());
}

// CarbonModelDelegate
CarbonModelDelegate::CarbonModelDelegate(QObject *parent, CarbonModelsWidget* models)
: QItemDelegate(parent), modelsWidget(models)
{
}

void CarbonModelDelegate::currentIndexChanged(int i)
{
  if (i != -1)
  {
    QComboBox *widget = qobject_cast<QComboBox *>(sender());
    commitData(widget);
    closeEditor(widget);
  }
}

QWidget *CarbonModelDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                           const QModelIndex &index) const
{
  UtString value;
  QTreeWidgetItem* itm = modelsWidget->topLevelItem(index.row());
  if (itm)
    value << itm->text(index.column());

  switch (index.column())
  {
  case CarbonModelsWidget::colONDEMAND:
    {
      if (modelsWidget->header()->sectionSize(CarbonModelsWidget::colONDEMAND) < 100)
        modelsWidget->header()->resizeSection(CarbonModelsWidget::colONDEMAND, 100);

      QComboBox *cbox = new QComboBox(parent);
      cbox->addItem(ODM_REQ_STATE_START);
      cbox->addItem(ODM_REQ_STATE_STOP);

      int ci = cbox->findText(value.c_str());
      if (ci != -1)
        cbox->setCurrentIndex(ci);

      CQT_CONNECT(cbox, currentIndexChanged(int), this, currentIndexChanged(int));

      return cbox;
      break;
    }
  case CarbonModelsWidget::colREPLAY:
    {
      if (modelsWidget->header()->sectionSize(CarbonModelsWidget::colREPLAY) < 120)
        modelsWidget->header()->resizeSection(CarbonModelsWidget::colREPLAY, 120);

      QComboBox *cbox = new QComboBox(parent);
      cbox->addItem(REP_REQ_STATE_RECORD);
      cbox->addItem(REP_REQ_STATE_PLAYBACK);
      cbox->addItem(REP_REQ_STATE_NORMAL);

      int ci = cbox->findText(value.c_str());
      if (ci != -1)
        cbox->setCurrentIndex(ci);

      CQT_CONNECT(cbox, currentIndexChanged(int), this, currentIndexChanged(int));

      return cbox;
      break;
    }
  default:
  case CarbonModelsWidget::colNAME:
    {
      break;
    }
  }

  return NULL;
}

void CarbonModelDelegate::commitAndCloseEditor()
{
  QLineEdit *editor = qobject_cast<QLineEdit *>(sender());
  emit commitData(editor);
  emit closeEditor(editor);
}

void CarbonModelDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit)
  {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }
}

void CarbonModelDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                         const QModelIndex &index) const
{
  QTreeWidgetItem* itm = modelsWidget->currentItem();
  switch (index.column())
  {
  case CarbonModelsWidget::colONDEMAND:
    {
      QComboBox* cbox = qobject_cast<QComboBox *>(editor);
      if (cbox != NULL)
      {
        UtString value;
        value << cbox->currentText();
        model->setData(index, value.c_str());

        CarbonModelInstanceItem* item = dynamic_cast<CarbonModelInstanceItem*>(itm);
        item->getInstanceOptions()->putOnDemandRequestedState(value.c_str());
      }
    }
    break;
  case CarbonModelsWidget::colREPLAY:
    {
      QComboBox* cbox = qobject_cast<QComboBox *>(editor);
      if (cbox != NULL)
      {
        UtString value;
        value << cbox->currentText();
        model->setData(index, value.c_str());

        CarbonModelInstanceItem* item = dynamic_cast<CarbonModelInstanceItem*>(itm);
        item->getInstanceOptions()->putReplayRequestedState(value.c_str());
      }
    }
    break;
  default:
    break;
  }
}

#if CCONTROL_ROOT_COMP
CarbonAllComponentsItem::CarbonAllComponentsItem(CarbonModelsWidget* cmw,
                                                 CarbonInstanceOptions* opts)
  : QTreeWidgetItem(cmw),
    mCarbonModelsWidget(cmw),
    mInstanceOptions(opts)
{
  setText(0, "All Components");
}
#endif
