//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"
#include "util/UtHashSet.h"

#include "util/CExprParse.h"

#include "CompWizardPortEditor.h"
#include "CompWizardTreeNodes.h"
#include "PortEditorCommands.h"
#include "ESLXtor.h"
#include "CcfgHelper.h"
#include "cmm.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "DlgAddMaxsimXtor.h"
#include "DlgSelectRTL.h"



// static initializer
ESLXtorParameterActions* ESLXtorInstanceParameterItem::mActions = NULL;

ESLXtorParameterActions::ESLXtorParameterActions(PortEditorTreeWidget* tree) : QObject(tree)
{
  mTree = tree;

  mConnectRTL = new QAction("Connect to RTL...", mTree);
  CQT_CONNECT(mConnectRTL, triggered(), this, actionConnectRTL());
}

QAction* ESLXtorParameterActions::getAction(Action kind)
{
  switch (kind)
  {
  case ConnectRTL:  return mConnectRTL; break;
  }
  return mConnectRTL;
}


// static initializers
ESLXtorPortActions* ESLXtorInstanceConnItem::mActions = NULL;

QAction* ESLXtorPortActions::getAction(Action kind)
{
  switch (kind)
  {
  case AddPortExpression: return mAddPortExpr; break;
  }
  return mAddPortExpr;
}

ESLXtorPortActions::ESLXtorPortActions(PortEditorTreeWidget* tree) : QObject(tree)
{
  mTree = tree;

  mAddPortExpr = new QAction("Add Port Expression", mTree);
  CQT_CONNECT(mAddPortExpr, triggered(), this, actionAddPortExpresssion());
}
void ESLXtorPortActions::actionAddPortExpresssion()
{
  qDebug() << "Add Port Expression";
  
  QList<PortEditorTreeItem*> items;
  mTree->fillSelectedItems(items, PortEditorTreeItem::XtorInstConn);

  if (items.count() > 0)
  {
    ESLXtorInstanceConnItem* item = dynamic_cast<ESLXtorInstanceConnItem*>(items.first());
    mTree->getUndoStack()->push(new ESLXtorInstanceConnItem::AddExpression(item, "0x0"));
  }
}

//
// Transactor Instance
//
UndoData* ESLXtorInstanceItem::deleteItem()
{
  ESLXtorInstanceItem::Delete* undoData = new ESLXtorInstanceItem::Delete();
  PortEditorTreeWidget* tree = dynamic_cast<PortEditorTreeWidget*>(treeWidget());

  undoData->mCfg = getCcfg();
  undoData->setTreeData(tree, this);
  undoData->setText(mXtorInst->getName());
  undoData->mXtorInstName = mXtorInst->getName();
  undoData->mXtorName = mXtorInst->getType()->getName();
  undoData->mXtorLibName = mXtorInst->getType()->getLibraryName();
  undoData->mXtorVariant = mXtorInst->getType()->getVariant();
  

  if (mXtorInst->getType()->useESLClockMaster())
  {
    if (mXtorInst->getESLClockMaster())
      undoData->mClockMaster = mXtorInst->getESLClockMaster();
  }
  else
  {
    if (mXtorInst->getClockMaster())
      undoData->mClockMaster = mXtorInst->getClockMaster()->getName();
  }

 
  // Save Parameters
  for (UInt32 i=0; i<mXtorInst->numParams(); i++)
  {
    // process both hidden and non-hidden parameters - We don't
    // believe the concept of hidden parameters will apply to CMS/RTL
    // use cases - it is currently only a model kit feature.
    CarbonCfgXtorParamInst* paramInst = mXtorInst->getParamInstance(i, true);
    ESLXtorInstanceItem::DeleteParam* undoParam = new ESLXtorInstanceItem::DeleteParam();

    undoParam->mCfg = getCcfg();
    undoParam->setText(paramInst->getParam()->getName());
    undoParam->mParamName = paramInst->getParam()->getName();
    undoParam->mParamValue = paramInst->getValue();
    undoParam->mXtorInstName = mXtorInst->getName();

    undoData->mParameters.append(undoParam);
  }

  // Save Connections
  for (UInt32 i=0; i<mXtorInst->getType()->numPorts(); i++)
  {
    CarbonCfgXtorConn* xtorConn = mXtorInst->getConnection(i);
    // We only care about connected ones
    if (xtorConn->getRTLPort())
    {
      ESLXtorInstanceItem::DeleteConn* undoConn = new ESLXtorInstanceItem::DeleteConn();
      undoConn->mCfg = getCcfg();
      undoConn->setText(xtorConn->getRTLPort()->getName());
      undoConn->mPortIndex = i;
      undoConn->setTreeData(tree);
      undoConn->mExpr = xtorConn->getExpr();
      undoConn->mXtorInstName = mXtorInst->getName();
      undoConn->mRTLPortName = xtorConn->getRTLPort()->getName();
      undoConn->mXtorPortName = xtorConn->getPort()->getName();
      undoData->mConnections[undoConn->mRTLPortName] = undoConn;
    }
  }
  return undoData;
}

void ESLXtorInstanceItem::DeleteConn::undo()
{
  qDebug() << "Undo Global Transactor Instance Delete" << text();

  // Find the ESL Port that we created earlier, and remove it
  QTreeWidgetItem* eslItem = getTree()->itemFromIndex(mESLPortIndex);
  getTree()->getEditor()->getPortsRoot()->removeChild(eslItem);

  UtString uXtorInstName; uXtorInstName << mXtorInstName;
  UtString uRTLPortName; uRTLPortName << mRTLPortName;
  UtString uESLPortName; uESLPortName << mESLPortName;

  CarbonCfgXtorInstance* xtorInst = mCfg->findXtorInstance(uXtorInstName.c_str());
  INFO_ASSERT(xtorInst, "Expecting Xtor Instance");
  
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  INFO_ASSERT(rtlPort, "Expecting RTL Port");

  CarbonCfgESLPort* eslPort = mCfg->findESLPort(uESLPortName.c_str());
  if (eslPort)
  {
    mCfg->disconnect(eslPort);   // Break the ESL port
  }

  CarbonCfgXtorConn* conn = carbonCfgConnectXtor(rtlPort, xtorInst, mPortIndex);

  UtString uExpr; uExpr << mExpr;
  conn->putExpr(uExpr.c_str());
}

void ESLXtorInstanceItem::DeleteConn::disconnect()
{
  qDebug() << "Disconnect Xtor ESL Port Connection" << text();

  UtString uRTLPortName; uRTLPortName << mRTLPortName;
  UtString uESLPortName; uESLPortName << mESLPortName;
  
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  INFO_ASSERT(rtlPort, "Expecting RTL Port");

  CarbonCfgESLPort* eslPort = mCfg->findESLPort(uESLPortName.c_str());
  if (eslPort)
  {
    mCfg->disconnect(eslPort);   // Break the ESL port
  }
}

void ESLXtorInstanceItem::DeleteConn::redo()
{
  qDebug() << "Delete Xtor Instance Connection" << text();

  UtString uXtorInstName; uXtorInstName << mXtorInstName;
  UtString uRTLPortName; uRTLPortName << mRTLPortName;
  
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  INFO_ASSERT(rtlPort, "Expecting RTL Port");

  ESLPortItem::makeESLPort(mCfg, rtlPort, getTree(), mESLPortName, mESLPortIndex);
}

void ESLXtorInstanceItem::DeleteParam::undo()
{
  qDebug() << "Undo Transactor Parameter Delete" << text();

  UtString uXtorInstName; uXtorInstName << mXtorInstName;
  UtString uParamName; uParamName << mParamName;
  UtString uParamValue; uParamValue << mParamValue;

  CarbonCfgXtorInstance* xtorInst = mCfg->findXtorInstance(uXtorInstName.c_str());
  INFO_ASSERT(xtorInst, "Expecting Xtor Instance");

  CarbonCfgXtorParamInst* paramInst = xtorInst->findParameterInst(uParamName);
  INFO_ASSERT(paramInst, "Expecting Param Instance");

  // Restore the value
  paramInst->putValue(uParamValue.c_str());
}

void ESLXtorInstanceItem::DeleteParam::redo()
{
  // Nothing to do here, this is normal
  qDebug() << "Delete Xtor Instance Parameter" << text();
}

void ESLXtorInstanceItem::Delete::undo()
{
  qDebug() << "Undo Transactor Instance Delete" << text();

  UtString uXtorInstName; uXtorInstName << mXtorInstName;
  UtString uXtorName; uXtorName << mXtorName;
  UtString uXtorLibName; uXtorLibName << mXtorLibName;
  UtString uXtorVariant; uXtorVariant << mXtorVariant;
  UtString uClockMaster; uClockMaster << mClockMaster;
  
  CarbonCfgXtor* xtor = mCfg->findXtor(uXtorName.c_str(), uXtorLibName.c_str(), uXtorVariant.c_str());
  INFO_ASSERT(xtor, "Expected to find Xtor");

   // Now disconnect each ESL port
  foreach(ESLXtorInstanceItem::DeleteConn* undoConn, mConnections)
    undoConn->disconnect();

  // Create the Xtor
  CarbonCfgXtorInstance* xtorInst = mCfg->addXtor(uXtorInstName.c_str(), xtor);
  
   // restore clock master
  if (!mClockMaster.isEmpty())
  {
    if (xtorInst->getType()->useESLClockMaster()) {
      if (uClockMaster == "Undefined") {
        xtorInst->putESLClockMaster("");
      } else {
        xtorInst->putESLClockMaster(uClockMaster.c_str());
      }
    } else
      xtorInst->putClockMaster(mCfg->findXtorInstance(uClockMaster.c_str()));
  }


  // Now put it's port values back
  foreach(ESLXtorInstanceItem::DeleteParam* undoParam, mParameters)
    undoParam->undo();

 // Now reconnect the RTL by undoing the ESL Ports
  foreach(ESLXtorInstanceItem::DeleteConn* undoConn, mConnections)
    undoConn->undo();

  // restore any clock masters that pointed to this
  if (xtorInst && !xtorInst->getType()->useESLClockMaster())
  {
    foreach (QString xtorInstName, mClockMasters)
    {
      QTreeWidgetItem* portsRoot = getTree()->getEditor()->getPortsRoot();
      ESLXtorInstanceItem* xtorInstItem = getTree()->findXtorInstance(portsRoot, xtorInstName);

      ESLXtorInstanceClockedByItem* clockedBy = xtorInstItem->getClockedBy();
      clockedBy->setClockMaster(xtorInst);      
    }
  }

  // Create the Item

  QTreeWidgetItem* portsRoot = getTree()->getEditor()->getPortsRoot();

  ESLXtorInstanceItem* newItem = new ESLXtorInstanceItem(portsRoot, xtorInst);

  getTree()->sortChildren(portsRoot);

  newItem->setSelected(true);
  getTree()->scrollToItem(newItem);

  getTree()->setModified(false);
}

void ESLXtorInstanceItem::Delete::redo()
{
  qDebug() << "Delete Xtor Instance" << text();

  UtString uXtorInstName; uXtorInstName << mXtorInstName;

  // Remember any Xtor's clocked by this instance we are deleting.
  mClockMasters.clear();

  QTreeWidgetItem* item = getItem();
  item->setSelected(false);

  CarbonCfgXtorInstance* xtorInst = mCfg->findXtorInstance(uXtorInstName.c_str());
  INFO_ASSERT(xtorInst, "Expecting Xtor Instance");

  // Check if this is a clock master, if so, reset the 
  // others to default
  if (xtorInst->getType()->isXtorClockMaster())
  {
    // Find all XtorInstances which referred to this clock master
    for (UInt32 i=0; i<mCfg->numXtorInstances(); i++)
    {
      CarbonCfgXtorInstance* xinst = mCfg->getXtorInstance(i);
      if (!xinst->getType()->useESLClockMaster() && xinst->getClockMaster() == xtorInst)
      {
        QTreeWidgetItem* portsRoot = getTree()->getEditor()->getPortsRoot();
        ESLXtorInstanceItem* xtorInstItem = getTree()->findXtorInstance(portsRoot, xinst->getName());
        INFO_ASSERT(xtorInstItem, "Expected to find xtor instance");
        mClockMasters.append(xinst->getName());
        ESLXtorInstanceClockedByItem* clockedBy = xtorInstItem->getClockedBy();
        clockedBy->setClockMaster(NULL);
      }
    }
  }

  QStringList rtlPortNames;
  for (UInt32 i=0; i<xtorInst->getType()->numPorts(); i++)
  {
    CarbonCfgXtorConn* xtorConn = xtorInst->getConnection(i);
    CarbonCfgRTLPort* rtlPort = xtorConn->getRTLPort();

    mCfg->disconnect(xtorConn);
    if (rtlPort)
    {
      rtlPortNames.append(rtlPort->getName());    
    }
  }

  getTree()->removeItem(item);

  // Blow it away so the ESL Port's can be re-created
  mCfg->removeXtorInstance(xtorInst);  

  foreach(QString rtlPortName, rtlPortNames)
  {  
    ESLXtorInstanceItem::DeleteConn* undoConn = mConnections[rtlPortName];
      INFO_ASSERT(undoConn, "Expecting Undo Connection");
      undoConn->redo();
  }

  // Broadcast the delete
  getTree()->getEditor()->signalDeleteXtorInstance(mXtorInstName);

  getTree()->setModified(true);
}


UndoData* ESLXtorInstanceConnItem::deleteItem()
{
  QTreeWidgetItem* parent = PortEditorTreeItem::parent();

  if (!parent->isSelected() && mXtorConn->getRTLPort())
  {
    ESLXtorInstanceConnItem::Delete* undoData = new ESLXtorInstanceConnItem::Delete();
    PortEditorTreeWidget* tree = dynamic_cast<PortEditorTreeWidget*>(treeWidget());

    undoData->setText(mXtorConn->getRTLPort()->getName());
    undoData->mPortIndex = mXtorConn->getPortIndex();
    undoData->mXtorInstName = mXtorConn->getInstance()->getName();
    undoData->mXtorPortName = mXtorConn->getPort()->getName();
    undoData->mRTLPortName = mXtorConn->getRTLPort()->getName();
    undoData->mExpr = mXtorConn->getExpr();
    undoData->mCfg = getCcfg();
    undoData->setTreeData(tree, this);

    return undoData;
  }
  return NULL; // Nothing to delete
}

void ESLXtorInstanceConnItem::Delete::undo()
{
  qDebug() << "Undo Transactor Instance Connection" << text();

  ESLXtorInstanceConnItem* item = dynamic_cast<ESLXtorInstanceConnItem*>(getItem());
  INFO_ASSERT(item, "Expecting items");

  // Connections are based upon Port Indexes
  CarbonCfgXtorConn* xtorConn = item->mXtorConn;
  UInt32 portIndex = mPortIndex;
  CarbonCfgXtorInstance* xtorInst = xtorConn->getInstance();
  INFO_ASSERT(xtorInst, "Expecting Xtor Inst");

  UtString uRTLPortName;
  uRTLPortName << mRTLPortName;

  // Re-find the RTL port
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  INFO_ASSERT(rtlPort, "Expecting RTL port");
 /* qDebug() << "Before RTL Port Connections" << rtlPort->numConnections();
  qDebug() << "RTL Port clkgen" << rtlPort->isClockGen();
  qDebug() << "RTL Port resetgen" << rtlPort->isResetGen();
  qDebug() << "RTL Port tied" << rtlPort->isTied();
  qDebug() << "RTL Port tied to param" << rtlPort->isTiedToParam();
  qDebug() << "RTL Port esl port" << rtlPort->isESLPort();
  qDebug() << "RTL Port xtor conn" << rtlPort->isConnectedToXtor();*/

  // Connect up the RTL port
  xtorConn = carbonCfgConnectXtor(rtlPort, xtorInst, portIndex);

  // Get the new connection
  item->mXtorConn = xtorConn;

  // Put back the expression
  UtString uExpr; uExpr << mExpr;
  xtorConn->putExpr(uExpr.c_str());
  item->setPortExpression(mExpr);

  // Update the tree display
  item->setText(colRTL, rtlPort->getName());
  item->setIcon(colNAME, item->icon(rtlPort->getType()));

  // Remove the ESL Port?
  if (!mESLPortIndex.isEmpty())
  {
    UtString uESLPortName; uESLPortName << mESLPortName;
    CarbonCfgESLPort* eslPort = mCfg->findESLPort(uESLPortName.c_str());
    INFO_ASSERT(eslPort, "Expecting ESL Port");
    mCfg->disconnect(eslPort);

    QTreeWidgetItem* eslItem = getTree()->itemFromIndex(mESLPortIndex);
    getTree()->getEditor()->getPortsRoot()->removeChild(eslItem);
  }

  item->setSelected(true);
  getTree()->scrollToItem(item);

  // Find and remove the ESL Port that was previously created
  getTree()->setModified(false);

  qDebug() << "After RTL Port Connections" << rtlPort->numConnections();
  qDebug() << "RTL Port clkgen" << rtlPort->isClockGen();
  qDebug() << "RTL Port resetgen" << rtlPort->isResetGen();
  qDebug() << "RTL Port tied" << rtlPort->isTied();
  qDebug() << "RTL Port tied to param" << rtlPort->isTiedToParam();
  qDebug() << "RTL Port esl port" << rtlPort->isESLPort();
  qDebug() << "RTL Port xtor conn" << rtlPort->isConnectedToXtor();
}

void ESLXtorInstanceConnItem::Delete::redo()
{
  qDebug() << "Delete Xtor Instance Connection" << text();

  ESLXtorInstanceConnItem* item = dynamic_cast<ESLXtorInstanceConnItem*>(getItem());

  INFO_ASSERT(item, "Expecting items");

  CarbonCfgXtorConn* xtorConn = item->mXtorConn;
  xtorConn->putExpr("");
  item->setPortExpression("");

  CarbonCfgRTLPort* rtlPort = xtorConn->getRTLPort();

  qDebug() << "Before RTL Port Connections" << rtlPort->numConnections();
  qDebug() << "RTL Port clkgen" << rtlPort->isClockGen();
  qDebug() << "RTL Port resetgen" << rtlPort->isResetGen();
  qDebug() << "RTL Port tied" << rtlPort->isTied();
  qDebug() << "RTL Port tied to param" << rtlPort->isTiedToParam();
  qDebug() << "RTL Port esl port" << rtlPort->isESLPort();
  qDebug() << "RTL Port xtor conn" << rtlPort->isConnectedToXtor();

  CarbonCfgXtorInstance* xtorInst = xtorConn->getInstance();

  UInt32 portIndex = mPortIndex;

  // Disconnect the RTL port to this connection
  rtlPort->disconnect(xtorConn);

  // Get the newly allocated XtorConn
  item->mXtorConn = xtorInst->getConnection(portIndex);

  INFO_ASSERT(item->mXtorConn->getRTLPort() == NULL, "Expecting no Connection");

  item->setText(colRTL, "");
  item->setIcon(colNAME, QIcon(":/cmm/Resources/disconnect.png"));

  // Now make this an ESL port again
  UtString uRTLPortName; uRTLPortName << mRTLPortName;

  CarbonDB* db = mCfg->getDB();
  const CarbonDBNode* node = carbonDBFindNode(db, uRTLPortName.c_str());
  INFO_ASSERT(node, "Expecting RTL Node");
  
  rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  INFO_ASSERT(rtlPort, "Expected RTL Port");
  
  // Clear selection on the current item
  item->setSelected(false);

  // An ESL Port might already exist, so don't create another one
  ESLPortItem::makeESLPort(mCfg, rtlPort, getTree(), mESLPortName, mESLPortIndex);
  
  getTree()->setModified(false);

  qDebug() << "After RTL Port Connections" << rtlPort->numConnections();
  qDebug() << "RTL Port clkgen" << rtlPort->isClockGen();
  qDebug() << "RTL Port resetgen" << rtlPort->isResetGen();
  qDebug() << "RTL Port tied" << rtlPort->isTied();
  qDebug() << "RTL Port tied to param" << rtlPort->isTiedToParam();
  qDebug() << "RTL Port esl port" << rtlPort->isESLPort();
  qDebug() << "RTL Port xtor conn" << rtlPort->isConnectedToXtor();
}


void ESLXtorInstanceConnExprItem::multiSelectContextMenu(QMenu&)
{
}

UndoData* ESLXtorInstanceConnExprItem::deleteItem()
{
  ESLXtorInstanceConnItem* parentItem = dynamic_cast<ESLXtorInstanceConnItem*>(PortEditorTreeItem::parent());
  if (!parentItem->isSelected())
  {
    ESLXtorInstanceConnExprItem::Delete* undoData = new ESLXtorInstanceConnExprItem::Delete();
    undoData->setTreeData(treeWidget(), this);
    undoData->mCfg = getCcfg();

    undoData->mXtorInstName = getXtorConn()->getInstance()->getName();
    if (getXtorConn()->getRTLPort())
      undoData->mRTLPortName = getXtorConn()->getRTLPort()->getName();

    undoData->mXtorPortName = getXtorConn()->getPort()->getName();
    undoData->mExpr = getXtorConn()->getExpr();
    undoData->mPortIndex = getXtorConn()->getPortIndex();
    undoData->setText(QString("Delete %1 Port Expression").arg(undoData->mXtorPortName));

    return undoData;
  }
  else
    return NULL; // Handled by the parent
}

void ESLXtorInstanceConnExprItem::Delete::undo()
{
  qDebug() << "Undo Delete Xtor Port Expression" << text();
  CcfgHelper ccfg(mCfg);
  CarbonCfgXtorInstance* xtorInst = ccfg.findXtorInstance(mXtorInstName);
  INFO_ASSERT(xtorInst, "Expecting Xtor Instance");

  CarbonCfgXtorConn* xtorConn = xtorInst->getConnection(mPortIndex);
  UtString uExpr; uExpr << mExpr;
  xtorConn->putExpr(uExpr.c_str());

  ESLXtorInstanceConnItem* connItem = dynamic_cast<ESLXtorInstanceConnItem*>(getParentItem());
  if (connItem)
  {
    connItem->setPortExpression(mExpr);
    if (connItem->getExprItem())
      connItem->getExprItem()->setSelected(true);
  }
  getTree()->setModified(false);
}

void  ESLXtorInstanceConnExprItem::Delete::redo()
{
  qDebug() << "Delete Xtor Port Expression" << text();
  CcfgHelper ccfg(mCfg);
  CarbonCfgXtorInstance* xtorInst = ccfg.findXtorInstance(mXtorInstName);
  INFO_ASSERT(xtorInst, "Expecting Xtor Instance");

  CarbonCfgXtorConn* xtorConn = xtorInst->getConnection(mPortIndex);
  xtorConn->putExpr("");
  
  QTreeWidgetItem* parentItem = getParentItem();

  ESLXtorInstanceConnItem* connItem = dynamic_cast<ESLXtorInstanceConnItem*>(parentItem);
  connItem->setPortExpression("");

  INFO_ASSERT(connItem, "Expecting item");

  getTree()->setModified(true);
}

ESLXtorInstanceConnExprItem::ESLXtorInstanceConnExprItem(ESLXtorInstanceConnItem* parent,
                                                 CarbonCfgXtorConn* xtorConn) 
  : PortEditorTreeItem(PortEditorTreeItem::XtorInstConnExpr, parent)
{
  mXtorConn = xtorConn;
  mXtorInstConnItem = parent;

  setItalicText(colNAME, "Port Expression");
  setPortExpression(xtorConn->getExpr());
  setFlags(flags() | Qt::ItemIsEditable);
}


QWidget* ESLXtorInstanceConnExprItem::createEditor(const PortEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLXtorInstanceConnExprItem::Columns col = ESLXtorInstanceConnExprItem::Columns(colIndex);

  switch (col)
  {
  case ESLXtorInstanceConnExprItem::colVALUE:
    {
      // Only a connected Xtor port can use a port expression that references
      // the field name.  Unconnected xtor ports can only be tied.
      QString xtorPortName;
      if (mXtorConn->getRTLPort())
        xtorPortName = mXtorConn->getPort()->getName();

      QLineEdit* lineEdit = new QLineEdit(parent);
      lineEdit->setValidator(new ESLPortExprValidator(lineEdit, xtorPortName));
      return lineEdit;
    }
    break;
  default:
    break;
  }

  return NULL;
}

void ESLXtorInstanceConnExprItem::setModelData(const PortEditorDelegate* , 
                                   QAbstractItemModel* model, const QModelIndex &modelIndex,
                                   QWidget* editor, int colIndex)
{
  ESLXtorInstanceConnExprItem::Columns col = ESLXtorInstanceConnExprItem::Columns(colIndex);
  switch (col)
  {
  case ESLXtorInstanceConnExprItem::colVALUE:
    {
      QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
      if (lineEdit)
      {
        const QValidator* validator = lineEdit->validator();
        int pos=0;
        QString txt = lineEdit->text();
        if (validator->validate(txt, pos) == QValidator::Acceptable)
        {
          QString oldValue = model->data(modelIndex).toString();
          QString newValue = lineEdit->text();
          model->setData(modelIndex, newValue);
          if (oldValue != newValue)
            getUndoStack()->push(new ESLXtorInstanceConnExprItem::CmdChangePortExpr(this, oldValue, newValue));
        }
      }
    }
    break;
  default:
    break;
  }
}

void ESLXtorInstanceConnItem::setConnection(CarbonCfgXtorConn* xtorConn)
{
  mXtorConn = xtorConn;
  CarbonCfgXtorPort* xtorPort = xtorConn->getPort();

  QString xtorPortName = xtorPort->getName();
  QString value = QString("%1").arg(xtorPort->getSize());
  setText(colNAME, xtorPortName.toUpper());
  setText(colSIZE, value);

  if (getTree()->getEditor()->getEditorMode() == CompWizardPortEditor::eModeCoWare || getTree()->getEditor()->getEditorMode() == CompWizardPortEditor::eModeSystemC)
    setText(colTYPE, xtorPort->getTypeDef());
  
  CarbonCfgRTLPort* rtlPort = xtorConn->getRTLPort();
  if (rtlPort)
  {
    setText(colRTL, rtlPort->getName());
    setIcon(colNAME, icon(rtlPort->getType()));
    QString value = QString("%1").arg(rtlPort->getWidth());
    setText(colSIZE, value);
  }
  else
  {
    setText(colRTL, "");
    setIcon(colNAME, QIcon(":/cmm/Resources/disconnect.png"));
  }

  setPortExpression(xtorConn->getExpr());
}

ESLXtorInstanceConnItem::ESLXtorInstanceConnItem(ESLXtorInstanceItem* parent,
                                                 CarbonCfgXtorConn* xtorConn) 
  : PortEditorTreeItem(PortEditorTreeItem::XtorInstConn, parent)
{
  mXtorConn = xtorConn;
  mXtorInstItem = parent;
  mPortExpr = NULL;

  setConnection(xtorConn);

  setExpanded(true);
}


void ESLXtorInstanceConnItem::setPortExpression(const QString& portExpr)
{
  if (portExpr.isEmpty())
  {
    if (mPortExpr)
      getTree()->removeItem(mPortExpr);
    mPortExpr = NULL;
  }
  else
  {
    if (mPortExpr == NULL)
      mPortExpr = new ESLXtorInstanceConnExprItem(this, mXtorConn);

    mPortExpr->setPortExpression(portExpr);
  }
}



UndoData* ESLXtorInstanceParameterRTLPortItem::deleteItem()
{
  QTreeWidgetItem* parentItem = PortEditorTreeItem::parent();
  if (!parentItem->isSelected())
  {
    ESLXtorInstanceParameterRTLPortItem::Delete* undoData = new ESLXtorInstanceParameterRTLPortItem::Delete();
    PortEditorTreeWidget* tree = dynamic_cast<PortEditorTreeWidget*>(treeWidget());

    undoData->setText(mRTLPort->getName());
    undoData->mXtorInstName = mXtorParamInst->getInstance()->getName();
    undoData->mXtorParamName = mXtorParamInst->getParam()->getName();
    undoData->mRTLPortName = mRTLPort->getName();
    undoData->mCfg = getCcfg();
    undoData->setTreeData(tree, this);

    return undoData;
  }
  return NULL;
}

void ESLXtorInstanceParameterRTLPortItem::Delete::redo()
{
  qDebug() << "Delete Xtor Instance Parameter Connection" << text();

  UtString uRTLPortName;
  uRTLPortName << mRTLPortName;

  ESLXtorInstanceParameterRTLPortItem* item = dynamic_cast<ESLXtorInstanceParameterRTLPortItem*>(getItem());
  INFO_ASSERT(item, "Expecting item");

  CarbonCfgXtorParamInst* xtorParamInst = item->mXtorParamInst;
  INFO_ASSERT(xtorParamInst, "Expecting Xtor Instance");

  CarbonCfgRTLPort* rtlPort = xtorParamInst->findRTLPort(uRTLPortName.c_str());
  INFO_ASSERT(rtlPort, "Expecting to find connection");

  // Break the connection from the RTL port to this parameter
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgTieParam* tie = conn->castTieParam();
    if (tie && tie->mXtorInstanceName.c_str() == mXtorInstName)
      mCfg->disconnect(tie);
  }

  xtorParamInst->removeRTLPort(rtlPort);

  // Re-create an ESL port for it
  if (rtlPort->numConnections() == 0)
    ESLPortItem::makeESLPort(mCfg, rtlPort, getTree(), mESLPortName, mESLPortIndex);

  QTreeWidgetItem* thisItem = getItem();
  QTreeWidgetItem* parentItem = getParentItem();

  INFO_ASSERT(parentItem && thisItem, "Expecting items");

  parentItem->removeChild(thisItem);
  getTree()->setModified(true);
}

void ESLXtorInstanceParameterRTLPortItem::Delete::undo()
{
  qDebug() << "Undo Delete XtorInst Param RTL Connection" << text();

  UtString uXtorInstName;
  uXtorInstName << mXtorInstName;

  CarbonCfgXtorInstance* xtorInst = mCfg->findXtorInstance(uXtorInstName.c_str());
  INFO_ASSERT(xtorInst, "Expecting Xtor Instance");

  UtString uXtorParamName;
  uXtorParamName << mXtorParamName;

  CarbonCfgXtorParamInst* xtorParamInst = xtorInst->findParameterInst(uXtorParamName.c_str());
  INFO_ASSERT(xtorParamInst, "Expecting Xtor Param Instance");

  UtString uRTLPortName;
  uRTLPortName << mRTLPortName;

  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  INFO_ASSERT(rtlPort, "Expecting RTL Port");

  // Remove the ESL port we had created before
  if (!mESLPortIndex.isEmpty())
  {
    PortEditorTreeItem* eslItem 
      = dynamic_cast<PortEditorTreeItem*>(getTree()->itemFromIndex(mESLPortIndex));
    INFO_ASSERT(eslItem, "Expected to find item");
    ESLPortItem::removeESLPort(eslItem, mESLPortName);
  }

  // Connect the parameter to the RTL
  xtorParamInst->addRTLPort(rtlPort);

  // Connect the RTL port to the parameter
  carbonCfgTieParam(rtlPort, uXtorInstName.c_str(), uXtorParamName.c_str());

  QTreeWidgetItem* parentItem = getParentItem();
  ESLXtorInstanceParameterRTLPortItem* newItem = 
    new ESLXtorInstanceParameterRTLPortItem(parentItem, xtorParamInst, rtlPort);


  PortEditorTreeWidget* tree = getTree();
  setTreeData(tree, newItem);

  getTree()->setModified(false);
}


void ESLXtorInstanceItem::actionBind()
{
  qDebug() << "XtorInstanceItem Show Bind Dialog";

  QString xtorName;

  CarbonCfgXtor* xtor = mXtorInst->getType();
  QString libraryName = xtor->getLibraryName();
  if (libraryName == CARBON_DEFAULT_XTOR_LIB)
    xtorName = xtor->getName();
  else {
    if (strlen(xtor->getVariant()) == 0) {
      xtorName = QString("%1/%2").arg(xtor->getLibraryName()).arg(xtor->getName());
    } else {
      xtorName = QString("%1/%2/%3").arg(xtor->getLibraryName()).arg(xtor->getName()).arg(xtor->getVariant());
    }
  }

  DlgAddMaxsimXtor dlg(getCcfg(), xtorName, mXtorInst);
  if (dlg.exec())
  {
    QList<ESLXtorInstanceItem::AddXtorMapping*> mappings;

    for (quint32 i=0; i<dlg.numPorts(); i++)
    {
      CarbonCfgRTLPort* rtlPort = NULL;
      CarbonCfgXtorPort* xtorPort = NULL;
      dlg.getMapping(i, &xtorPort, &rtlPort);
      if (rtlPort)
        qDebug() << "Map" << xtorPort->getName() << "to" << rtlPort->getName();
      else
        qDebug() << "Not Mapped" << xtorPort->getName();

      ESLXtorInstanceItem::AddXtorMapping* mapping = new ESLXtorInstanceItem::AddXtorMapping;
      mapping->mXtorPort = xtorPort;
      mapping->mRTLPort = rtlPort;
      mappings.append(mapping);
    }
    qDebug() << "Bindings changed";

    getUndoStack()->push(new ESLXtorInstanceItem::ChangeXtorCommand(this, mXtorInst, mappings));
  }
}

void ESLXtorInstanceItem::doubleClicked()
{
  mActionBind->trigger();
}


void ESLXtorInstanceItem::multiSelectContextMenu(QMenu& menu)
{
  menu.addAction(mActionBind);
}

void ESLXtorInstanceItem::addMenuItems(QMenu* menu)
{
  menu->addAction(mActionBind);
}

void ESLXtorInstanceConnItem::multiSelectContextMenu(QMenu& menu)
{
  mXtorInstItem->addMenuItems(&menu);

  QList<PortEditorTreeItem*> items;
  getTree()->fillSelectedItems(items, PortEditorTreeItem::XtorInstConn);

  QAction* actionAddExpr = getActions()->getAction(ESLXtorPortActions::AddPortExpression);

  QString expr = mXtorConn->getExpr();
  // Single selection for 
  if (items.count() == 1 && expr.isEmpty())
  {
    ESLXtorInstanceConnItem* item = dynamic_cast<ESLXtorInstanceConnItem*>(items.first());
    if (item->getXtorConn()->getRTLPort())
    {
      actionAddExpr->setStatusTip("Add a port expression to this Transactor Port");
      actionAddExpr->setEnabled(true);
    }
  }
  else
  {
    actionAddExpr->setStatusTip("Already exists, or more than one port is selected.");
    actionAddExpr->setEnabled(false);
  }

  menu.addAction(actionAddExpr);
}


// ESLXtorInstanceItem

QWidget* ESLXtorInstanceItem::createEditor(const PortEditorDelegate*,
                            QWidget* parent, int colIndex)
{
  ESLXtorInstanceItem::Columns col = ESLXtorInstanceItem::Columns(colIndex);

  if (col == ESLXtorInstanceItem::colNAME)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    lineEdit->setValidator(new XtorInstanceNameValidator(mXtorInst->getName(), lineEdit, getCcfg()));
    return lineEdit;
  }

  return NULL;
}

void ESLXtorInstanceItem::setModelData(const PortEditorDelegate*, 
                    QAbstractItemModel* model, const QModelIndex& modelIndex, 
                    QWidget* editor, int colIndex)
{
  ESLXtorInstanceItem::Columns col = ESLXtorInstanceItem::Columns(colIndex);
  if (col == ESLXtorInstanceItem::colNAME)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldName = model->data(modelIndex).toString();
      QString newName = lineEdit->text();
       // See if it's in use
      UtString n; n << newName;

      bool validName = XtorInstanceNameValidator::isValid(getCcfg(), newName);
      if (validName && oldName != newName)
      {
        model->setData(modelIndex, newName);
        getUndoStack()->push(new CmdRenameXtorInstance(this, mXtorInst, oldName, newName));     
      }
    }
  }
}

// Start Clocked By
QWidget* ESLXtorInstanceClockedByItem::createEditor(const PortEditorDelegate* delegate, QWidget* parent, int colIndex)
{
  ESLXtorInstanceClockedByItem::Columns col = ESLXtorInstanceClockedByItem::Columns(colIndex);
  if (col == ESLXtorInstanceClockedByItem::colVALUE)
  {
    CarbonCfg* cfg = getCcfg();
    QComboBox* cbox = new QComboBox(parent);

    QString name;

    if (mXtorInst->getType()->useESLClockMaster())
    {
      cbox->addItem("Undefined");

      // SystemC Clocks
      for (UInt32 i=0; i<cfg->numRTLPorts(); i++)
      {
        CarbonCfgRTLPort* rtlPort = cfg->getRTLPort(i);
        for (UInt32 c=0; c<rtlPort->numConnections(); c++)
        {
          CarbonCfgRTLConnection* rtlConn = rtlPort->getConnection(c);
          CarbonCfgSystemCClock* clockGen = rtlConn->castSystemCClock();
          if (clockGen)
          {
            CarbonDB* db = cfg->getDB();
            QString leafName = carbonDBNodeGetLeafName(db, carbonDBFindNode(db, rtlPort->getName()));
            cbox->addItem(leafName);
          }
        }
      }

      for (UInt32 i=0; i<cfg->numESLPorts(); i++)
      {
        CarbonCfgESLPort* eslPort = cfg->getESLPort(i);
        CarbonCfgRTLPort* rtlPort = eslPort->getRTLPort();
        if (eslPort->getPortType() == eCarbonCfgESLInput && rtlPort && rtlPort->getWidth() == 1)
          cbox->addItem(eslPort->getName());
      }     

      const char* clockMaster = mXtorInst->getESLClockMaster();
      if (clockMaster)
        name = clockMaster;
      else 
        name = "Undefined";

    }
    else
    {
      cbox->addItem("Component");
      for (UInt32 i=0; i<cfg->numXtorInstances(); i++)
      {
        CarbonCfgXtorInstance* xtorInst = cfg->getXtorInstance(i);
        if (xtorInst->getType()->isXtorClockMaster())
          cbox->addItem(xtorInst->getName());
      }

      CarbonCfgXtorInstance* clockMaster = mXtorInst->getClockMaster();
      if (clockMaster)
        name = clockMaster->getName();
      else 
        name = "Component";

    }

    cbox->setCurrentIndex(cbox->findText(name));

    CQT_CONNECT(cbox, currentIndexChanged(int), delegate, currentIndexChanged(int));

    return cbox;
  }

  return NULL;
}

void ESLXtorInstanceClockedByItem::setModelData(const PortEditorDelegate* , 
                                   QAbstractItemModel* model, const QModelIndex &modelIndex,
                                   QWidget* editor, int colIndex)
{
  ESLXtorInstanceClockedByItem::Columns col = ESLXtorInstanceClockedByItem::Columns(colIndex);

  if (col == ESLXtorInstanceClockedByItem::colVALUE)
  {
    QComboBox* cbox = qobject_cast<QComboBox*>(editor);
    if (cbox)
    {
      QString oldValue = model->data(modelIndex).toString();
      QString origOldValue = oldValue;
      if (oldValue == "Component")
        oldValue = "";
      QString newValue = cbox->currentText();
      model->setData(modelIndex, newValue);
      if (origOldValue != newValue)
        getUndoStack()->push(new ESLXtorInstanceClockedByItem::ChangeClockMasterCommand(this, oldValue, newValue));     
    }
  }
}

ESLXtorInstanceClockedByItem::ChangeClockMasterCommand::ChangeClockMasterCommand(ESLXtorInstanceClockedByItem* item, 
      const QString& oldValue, const QString& newValue, QUndoCommand* parent)
      : TreeUndoCommand(item, parent)
{
  mOldValue = oldValue;
  mNewValue = newValue;
  mCfg = item->getCcfg();
  mXtorInstName = item->getXtorInstance()->getName();

  setText(QString("Change Clock Master From %1 to %2")
    .arg(mOldValue)
    .arg(mNewValue));
}

void ESLXtorInstanceClockedByItem::ChangeClockMasterCommand::undo()
{
  CcfgHelper ccfg(mCfg);
  
  CarbonCfgXtorInstance* xtorInst = ccfg.findXtorInstance(mXtorInstName);
  CarbonCfgXtorInstance* clockMaster = NULL;
  UtString ov;

  if (mOldValue.isEmpty())
  {
   if (xtorInst->getType()->useESLClockMaster())
      xtorInst->putESLClockMaster("");  
    else
      xtorInst->putClockMaster(NULL);
  }
  else
  {
    clockMaster = ccfg.findXtorInstance(mOldValue);
    if (xtorInst->getType()->useESLClockMaster())
    {
      ov << mOldValue;
      if (ov == "Undefined") {
        xtorInst->putESLClockMaster("");
      } else {
        xtorInst->putESLClockMaster(ov.c_str());
      }
    }
    else
      xtorInst->putClockMaster(clockMaster);
  }
  
  ESLXtorInstanceClockedByItem* item = dynamic_cast<ESLXtorInstanceClockedByItem*>(getItem());

  if (xtorInst->getType()->useESLClockMaster())
    item->setESLClockMaster(ov.c_str());
  else
    item->setClockMaster(clockMaster);

  item->treeWidget()->scrollToItem(item);
  setModified(false);  
}

void ESLXtorInstanceClockedByItem::ChangeClockMasterCommand::redo()
{
  CcfgHelper ccfg(mCfg);
  
  CarbonCfgXtorInstance* xtorInst = ccfg.findXtorInstance(mXtorInstName);
  CarbonCfgXtorInstance* clockMaster = NULL;
  UtString nv; 

  if (mNewValue.isEmpty())
  {
    if (xtorInst->getType()->useESLClockMaster())
      xtorInst->putESLClockMaster("");
    else  
      xtorInst->putClockMaster(NULL);
    
  }
  else
  {
    if (xtorInst->getType()->useESLClockMaster())
    {
      nv << mNewValue;
      if (nv == "Undefined") {
        xtorInst->putESLClockMaster("");     
      } else {
        xtorInst->putESLClockMaster(nv.c_str());
      }
    }
    else
    {
      clockMaster = ccfg.findXtorInstance(mNewValue);
      xtorInst->putClockMaster(clockMaster);
    }
  }
 
  ESLXtorInstanceClockedByItem* item = dynamic_cast<ESLXtorInstanceClockedByItem*>(getItem());

  if (xtorInst->getType()->useESLClockMaster())
    item->setESLClockMaster(nv.c_str());
  else
    item->setClockMaster(clockMaster);

  item->treeWidget()->scrollToItem(item);
  setModified(true);  
}
// End Clocked By



// Start Reset By
QWidget* ESLXtorInstanceResetByItem::createEditor(const PortEditorDelegate* delegate, QWidget* parent, int colIndex)
{
  ESLXtorInstanceResetByItem::Columns col = ESLXtorInstanceResetByItem::Columns(colIndex);
  if (col == ESLXtorInstanceResetByItem::colVALUE)
  {
    CarbonCfg* cfg = getCcfg();
    QComboBox* cbox = new QComboBox(parent);

    QString name;

    if (mXtorInst->getType()->useESLClockMaster())
    {
      cbox->addItem("Undefined");
      for (UInt32 i=0; i<cfg->numESLPorts(); i++)
      {
        CarbonCfgESLPort* eslPort = cfg->getESLPort(i);
        CarbonCfgRTLPort* rtlPort = eslPort->getRTLPort();
        if (eslPort->getPortType() == eCarbonCfgESLInput && rtlPort && rtlPort->getWidth() == 1)
          cbox->addItem(eslPort->getName());
      }

      const char* resetMaster = mXtorInst->getESLResetMaster();
      if (resetMaster)
        name = resetMaster;
      else 
        name = "Undefined";
    }
   

    cbox->setCurrentIndex(cbox->findText(name));

    CQT_CONNECT(cbox, currentIndexChanged(int), delegate, currentIndexChanged(int));

    return cbox;
  }

  return NULL;
}

void ESLXtorInstanceResetByItem::setModelData(const PortEditorDelegate* , 
                                   QAbstractItemModel* model, const QModelIndex &modelIndex,
                                   QWidget* editor, int colIndex)
{
  ESLXtorInstanceResetByItem::Columns col = ESLXtorInstanceResetByItem::Columns(colIndex);

  if (col == ESLXtorInstanceResetByItem::colVALUE)
  {
    QComboBox* cbox = qobject_cast<QComboBox*>(editor);
    if (cbox)
    {
      QString oldValue = model->data(modelIndex).toString();
      QString origOldValue = oldValue;
      if (oldValue == "Component")
        oldValue = "";
      QString newValue = cbox->currentText();
      model->setData(modelIndex, newValue);
      if (origOldValue != newValue)
        getUndoStack()->push(new ESLXtorInstanceResetByItem::ChangeResetMasterCommand(this, oldValue, newValue));     
    }
  }
}

ESLXtorInstanceResetByItem::ChangeResetMasterCommand::ChangeResetMasterCommand(ESLXtorInstanceResetByItem* item, 
      const QString& oldValue, const QString& newValue, QUndoCommand* parent)
      : TreeUndoCommand(item, parent)
{
  mOldValue = oldValue;
  mNewValue = newValue;
  mCfg = item->getCcfg();
  mXtorInstName = item->getXtorInstance()->getName();

  setText(QString("Change Reset Master From %1 to %2")
    .arg(mOldValue)
    .arg(mNewValue));
}

void ESLXtorInstanceResetByItem::ChangeResetMasterCommand::undo()
{
  CcfgHelper ccfg(mCfg);
  
  CarbonCfgXtorInstance* xtorInst = ccfg.findXtorInstance(mXtorInstName);
  UtString ov;

  if (mOldValue.isEmpty())
  {
    xtorInst->putESLResetMaster("");  
  }
  else
  {
    ov << mOldValue;
    if (ov == "Undefined") {
      xtorInst->putESLResetMaster("");
    } else {
      xtorInst->putESLResetMaster(ov.c_str());
    }
  }
  
  ESLXtorInstanceResetByItem* item = dynamic_cast<ESLXtorInstanceResetByItem*>(getItem());

  item->setESLResetMaster(ov.c_str());

  item->treeWidget()->scrollToItem(item);
  setModified(false);  
}

void ESLXtorInstanceResetByItem::ChangeResetMasterCommand::redo()
{
  CcfgHelper ccfg(mCfg);
  
  CarbonCfgXtorInstance* xtorInst = ccfg.findXtorInstance(mXtorInstName);
  
  UtString nv; 

  if (mNewValue.isEmpty())
  {    
    xtorInst->putESLResetMaster(""); 
  }
  else
  {
    nv << mNewValue;
    if (nv == "Undefined") {
      xtorInst->putESLResetMaster("");
    } else {
      xtorInst->putESLResetMaster(nv.c_str());
    }
  }
 
  ESLXtorInstanceResetByItem* item = dynamic_cast<ESLXtorInstanceResetByItem*>(getItem());

  item->setESLResetMaster(nv.c_str());

  item->treeWidget()->scrollToItem(item);
  setModified(true);  
}

// End ResetBy


// Start Abstraction
QWidget* ESLXtorInstanceAbstractionItem::createEditor(const PortEditorDelegate* delegate, QWidget* parent, int colIndex)
{
  ESLXtorInstanceAbstractionItem::Columns col = ESLXtorInstanceAbstractionItem::Columns(colIndex);
  if (col == ESLXtorInstanceAbstractionItem::colVALUE)
  {
    
    QComboBox* cbox = new QComboBox(parent);

    QString name;

    if (mXtorInst->getType()->numAbstractions() > 0)
    {
      CarbonCfgXtor* xtor = mXtorInst->getType();
      name = mXtorInst->getAbstraction()->getName();

      for (UInt32 i=0; i<xtor->numAbstractions(); i++)
      {
        QString iname = xtor->getAbstraction(i)->getName();
        cbox->addItem(iname);
      }    
    }
   
    cbox->setCurrentIndex(cbox->findText(name));

    CQT_CONNECT(cbox, currentIndexChanged(int), delegate, currentIndexChanged(int));

    return cbox;
  }

  return NULL;
}

void ESLXtorInstanceAbstractionItem::setModelData(const PortEditorDelegate* , 
                                   QAbstractItemModel* model, const QModelIndex &modelIndex,
                                   QWidget* editor, int colIndex)
{
  ESLXtorInstanceAbstractionItem::Columns col = ESLXtorInstanceAbstractionItem::Columns(colIndex);

  if (col == ESLXtorInstanceAbstractionItem::colVALUE)
  {
    QComboBox* cbox = qobject_cast<QComboBox*>(editor);
    if (cbox)
    {
      QString oldValue = model->data(modelIndex).toString();
      QString origOldValue = oldValue;
     
      QString newValue = cbox->currentText();
      model->setData(modelIndex, newValue);
      if (origOldValue != newValue)
        getUndoStack()->push(new ESLXtorInstanceAbstractionItem::ChangeAbstractionCommand(this, oldValue, newValue));     
    }
  }
}

ESLXtorInstanceAbstractionItem::ChangeAbstractionCommand::ChangeAbstractionCommand(ESLXtorInstanceAbstractionItem* item, 
      const QString& oldValue, const QString& newValue, QUndoCommand* parent)
      : TreeUndoCommand(item, parent)
{
  mOldValue = oldValue;
  mNewValue = newValue;
  mCfg = item->getCcfg();
  mXtorInstName = item->getXtorInstance()->getName();

  setText(QString("Change Abstraction From %1 to %2")
    .arg(mOldValue)
    .arg(mNewValue));
}

void ESLXtorInstanceAbstractionItem::ChangeAbstractionCommand::undo()
{
  CcfgHelper ccfg(mCfg);
  
  CarbonCfgXtorInstance* xtorInst = ccfg.findXtorInstance(mXtorInstName);
  UtString ov;

  if (mOldValue.isEmpty())
  {
    xtorInst->putAbstraction("");  
  }
  else
  {
    ov << mOldValue;
    xtorInst->putAbstraction(ov.c_str());
  }
  
  ESLXtorInstanceAbstractionItem* item = dynamic_cast<ESLXtorInstanceAbstractionItem*>(getItem());

  item->setAbstraction(ov.c_str());

  item->treeWidget()->scrollToItem(item);
  setModified(false);  
}

void ESLXtorInstanceAbstractionItem::ChangeAbstractionCommand::redo()
{
  CcfgHelper ccfg(mCfg);
  
  CarbonCfgXtorInstance* xtorInst = ccfg.findXtorInstance(mXtorInstName);
  
  UtString nv; 

  if (mNewValue.isEmpty())
  {    
    xtorInst->putAbstraction(""); 
  }
  else
  {
    nv << mNewValue;
    xtorInst->putAbstraction(nv.c_str());     
  }
 
  ESLXtorInstanceAbstractionItem* item = dynamic_cast<ESLXtorInstanceAbstractionItem*>(getItem());

  item->setAbstraction(nv.c_str());

  item->treeWidget()->scrollToItem(item);
  setModified(true);  
}

// End Abstraction

ESLXtorInstanceItem::AddXtorCommand::AddXtorCommand(PortEditorTreeItem* parentItem, 
                    CarbonCfgXtor* xtor, QList<AddXtorMapping*>& mappings,
                    QUndoCommand* parent)
                    : TreeUndoCommand(parentItem, parent)
{
  foreach(AddXtorMapping* mapping, mappings)
  {
    XtorMapping* map = new XtorMapping;
    map->mXtorPortName = mapping->mXtorPort->getName();
    if (mapping->mRTLPort)
    {
      map->mRTLPortName = mapping->mRTLPort->getName();
      map->mWasESLPort = mapping->mRTLPort->isESLPort();
      map->mWasDisconnected = mapping->mRTLPort->isDisconnected();
    }
    mMappings.append(map);
  }

  mParentItem = parentItem;
  mCfg = parentItem->getCcfg();
  mXtorName = xtor->getName();
  mXtorLibName = xtor->getLibraryName();
  mXtorVariant = xtor->getVariant();
  UtString instName;
  mCfg->getUniqueESLName(&instName, xtor->getName());
  mXtorInstName = instName.c_str();

  setText(QString("Add Xtor %1 (%2)").arg(mXtorName).arg(mXtorInstName));
}



void ESLXtorInstanceItem::AddXtorCommand::undo()
{
  CcfgHelper ccfg(mCfg);
  CarbonCfgXtorInstance* xtorInst = ccfg.findXtorInstance(mXtorInstName);
  INFO_ASSERT(xtorInst, "Expected to find Xtor Instance");

  for (UInt32 i=0; i<xtorInst->getType()->numPorts(); i++)
  {
    CarbonCfgXtorConn* xtorConn = xtorInst->getConnection(i);
    ccfg.disconnect(xtorConn);
  }

  // Re-create any ESL Ports
  for(int i=0; i<mMappings.count(); i++)
  {
    XtorMapping* mapping = mMappings[i];
    CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(mapping->mRTLPortName);

    if (mapping->mWasESLPort)
    {
      foreach(CarbonCfgESLPort* eslPort, mapping->mESLPorts)
      {
        qDebug() << "Re-Creating ESLPort" << eslPort->getName();
        ESLPortItem* newItem = ESLPortItem::createESLPort(getTree(), NULL, rtlPort);
        QString portExpr = eslPort->getExpr();
        if (!portExpr.isEmpty())
        {
          newItem->setPortExpr(portExpr);
          newItem->setExpanded(true);
        }

        newItem->setPortMode(eslPort->getMode());
        newItem->setPortTypeDef(eslPort->getTypeDef());

        // Restore the original name if was renamed
        newItem->setPortName(eslPort->getName());
      }
    }
    else if (mapping->mWasDisconnected)
    {
      QTreeWidgetItem* disRoot = getTree()->getEditor()->getDisconnectsRoot();
      ESLDisconnectItem* disItem = new ESLDisconnectItem(disRoot, rtlPort);
      disItem->setSelected(true);
    }
  }
  
  QTreeWidgetItem* item = getItem();
  item->setSelected(false);
  getTree()->removeItem(item);

  mCfg->removeXtorInstance(xtorInst);  

  getTree()->setModified(false);
}


void ESLXtorInstanceItem::AddXtorCommand::redo()
{
  CcfgHelper ccfg(mCfg);

  CarbonCfgXtor* xtor = ccfg.findXtor(mXtorName, mXtorLibName, mXtorVariant);
  CarbonCfgXtorInstance* xtorInst = ccfg.addXtor(mXtorInstName, xtor);
  INFO_ASSERT(xtorInst, "Instance already exists");

  mXtorInstName = xtorInst->getName();

  qDebug() << "Added Xtor" << xtor->getName() << "as" << mXtorInstName;

  // Don't get flickerry
  getTree()->setUpdatesEnabled(false);

  // Now, hookup all the connections
  for(int i=0; i<mMappings.count(); i++)
  {
    XtorMapping* mapping = mMappings[i];
    mapping->mESLPorts.clear();

    CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(mapping->mRTLPortName);
    if (rtlPort)
    {
      qDebug() << "Connecting" << rtlPort->getName();
      qDebug() << "\t"
               << "tied" << rtlPort->isTied() 
               << "tieParam" << rtlPort->isTiedToParam()
               << "xtorConn" << rtlPort->isConnectedToXtor()
               << "eslPort" << rtlPort->isESLPort() 
               << "reset" << rtlPort->isResetGen()
               << "clock" << rtlPort->isClockGen();

      // Was it disconnected?
      if (rtlPort->isDisconnected())
      {
        QTreeWidgetItem* disRoot = getTree()->getEditor()->getDisconnectsRoot();
        ESLDisconnectItem* disItem = getTree()->findDisconnect(disRoot, rtlPort->getName());
        if (disItem)
          getTree()->removeItem(disItem);
      }
      else if (rtlPort->isESLPort())
      {
        for (UInt32 c=0; c<rtlPort->numConnections(); c++)
        {
          CarbonCfgRTLConnection* conn = rtlPort->getConnection(c);
          if (conn->castESLPort())
          {
            CarbonCfgESLPort* eslPort = conn->castESLPort();
            // Copy the ESL Port
            CarbonCfgRTLPort* rtlp = eslPort->getRTLPort();
            mapping->mESLPorts.append(new CarbonCfgESLPort(eslPort, rtlp));
            ESLPortItem* eslItem = getTree()->findESLPort(getTree()->getEditor()->getPortsRoot(), eslPort->getName());
            if (eslItem && eslPort)
            {
              qDebug() << "removing esl port" << eslPort->getName();
              ESLPortItem::removeESLPort(eslItem, eslPort->getName());
            }
          }
        }
      }

      UtString portName; portName << mapping->mXtorPortName;
      UInt32 portIndex = 0;
      bool foundPort = xtor->findPort(portName.c_str(), &portIndex);
      INFO_ASSERT(foundPort, "Expected to find port");

      CarbonCfgXtorConn* xtorConn = new CarbonCfgXtorConn(rtlPort, xtorInst, portIndex);
      rtlPort->connect(xtorConn);
    }
  }

  qDebug() << "added xtor instance" << xtorInst->getName();

  ESLXtorInstanceItem* newItem = new ESLXtorInstanceItem(mParentItem, xtorInst);
  newItem->setExpanded(false);
  getTree()->sortChildren(mParentItem);
  newItem->setSelected(true);
  newItem->setExpanded(true);
  getTree()->scrollToItem(newItem);
  getTree()->setModified(true);
  getTree()->setUpdatesEnabled(true);
  updateTreeIndex(newItem);
}


ESLXtorInstanceConnExprItem::CmdChangePortExpr::CmdChangePortExpr(ESLXtorInstanceConnExprItem* item, 
              const QString& oldValue, const QString& newValue, 
              QUndoCommand* parent)
              : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mXtorInstName = item->getXtorConn()->getInstance()->getName();

  if (item->getXtorConn()->getRTLPort())
    mRTLPortName = item->getXtorConn()->getRTLPort()->getName();

  mXtorPortName = item->getXtorConn()->getPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;
  mPortIndex = item->getXtorConn()->getPortIndex();

  setText(QString("Change Xtor Port Expression: %1 %2 to %3")
    .arg(mXtorPortName)
    .arg(oldValue)
    .arg(newValue));
}

void ESLXtorInstanceConnExprItem::CmdChangePortExpr::undo()
{
  CcfgHelper ccfg(mCfg);
  CarbonCfgXtorInstance* xtorInst = ccfg.findXtorInstance(mXtorInstName);
  INFO_ASSERT(xtorInst, "Expecting Xtor Instance");

  CarbonCfgXtorConn* xtorConn = xtorInst->getConnection(mPortIndex);
  UtString v; v << mOldValue;
  xtorConn->putExpr(v.c_str());
  
  ESLXtorInstanceConnExprItem* item = dynamic_cast<ESLXtorInstanceConnExprItem*>(getItem());
  
  item->setPortExpression(mOldValue);
  item->treeWidget()->scrollToItem(item);

  getTree()->setModified(false);
}

void ESLXtorInstanceConnExprItem::CmdChangePortExpr::redo()
{
  CcfgHelper ccfg(mCfg);
  CarbonCfgXtorInstance* xtorInst = ccfg.findXtorInstance(mXtorInstName);
  INFO_ASSERT(xtorInst, "Expecting Xtor Instance");

  CarbonCfgXtorConn* xtorConn = xtorInst->getConnection(mPortIndex);
  
  UtString v; v << mNewValue;
  xtorConn->putExpr(v.c_str());

  ESLXtorInstanceConnExprItem* item = dynamic_cast<ESLXtorInstanceConnExprItem*>(getItem());
  
  item->setPortExpression(mNewValue);
  item->treeWidget()->scrollToItem(item);

  getTree()->setModified(true);
}


ESLXtorInstanceConnItem::AddExpression::AddExpression(ESLXtorInstanceConnItem* item, 
                    const QString& expr, QUndoCommand* parent)
                    : TreeUndoCommand(item, parent)
{
  CarbonCfgXtorConn* xtorConn = item->getXtorConn();
  mCfg = item->getCcfg();
  mXtorInstName = xtorConn->getInstance()->getName();
  if (xtorConn->getRTLPort())
    mRTLPortName = xtorConn->getRTLPort()->getName();

  mXtorPortName = xtorConn->getPort()->getName();
  mExpr = expr;
  mInteractive = true;
  mPortIndex = xtorConn->getPortIndex();
  setText(QString("Add Xtor Port Expression %1").arg(mXtorPortName));
}

void ESLXtorInstanceConnItem::AddExpression::undo()
{
  ESLXtorInstanceConnItem* item = dynamic_cast<ESLXtorInstanceConnItem*>(getItem());
  item->getXtorConn()->putExpr("");
  item->setPortExpression("");
  item->setSelected(true);
  getTree()->scrollToItem(item);
  getTree()->setModified(false);
}

void ESLXtorInstanceConnItem::AddExpression::redo()
{
  ESLXtorInstanceConnItem* item = dynamic_cast<ESLXtorInstanceConnItem*>(getItem());
  UtString uExpr; uExpr << mExpr;
  item->getXtorConn()->putExpr(uExpr.c_str());
  item->setPortExpression(mExpr);

  if (item->getExprItem())
  {
    if (mInteractive)
      getTree()->setCurrentItem(item->getExprItem(), ESLXtorInstanceConnExprItem::colVALUE);
    else
      item->getExprItem()->setSelected(true);
  }

  item->setExpanded(true);

  mInteractive = false;

  getTree()->scrollToItem(item);
  getTree()->setModified(true);
}

// Change XTOR
ESLXtorInstanceItem::ChangeXtorCommand::ChangeXtorCommand(ESLXtorInstanceItem* item, 
                    CarbonCfgXtorInstance* xtorInst, QList<AddXtorMapping*>& mappings,
                    QUndoCommand* parent)
                    : TreeUndoCommand(item, parent)
{
  // Old Mappings (current mappings)
  for (UInt32 i=0; i<xtorInst->getType()->numPorts(); i++)
  {
    CarbonCfgXtorConn* conn = xtorInst->getConnection(i);
    XtorMapping* map = new XtorMapping;
    map->mXtorPortName = conn->getPort()->getName();
    if (conn->getRTLPort())
    {
      CarbonCfgRTLPort* rtlPort = conn->getRTLPort();
      map->mRTLPortName = rtlPort->getName();
      map->mWasESLPort = rtlPort->isESLPort();
      map->mWasDisconnected = rtlPort->isDisconnected();
    }
    mOldMappings[map->mXtorPortName] = map;
  }

  // New Mappings
  foreach(AddXtorMapping* mapping, mappings)
  {
    XtorMapping* map = new XtorMapping;
    map->mXtorPortName = mapping->mXtorPort->getName();
    if (mapping->mRTLPort)
    {
      map->mRTLPortName = mapping->mRTLPort->getName();
      map->mWasESLPort = mapping->mRTLPort->isESLPort();
      map->mWasDisconnected = mapping->mRTLPort->isDisconnected();
    }
    mNewMappings[map->mXtorPortName] = map;
  }

  mCfg = item->getCcfg();
  mXtorInstName = xtorInst->getName();

  setText(QString("Change XtorInstance %1 Bindings").arg(mXtorInstName));
}


CarbonCfgXtorConn* ESLXtorInstanceItem::ChangeXtorCommand::connectRTLPort(CarbonCfg* cfg, CarbonCfgXtorInstance* xtorInst, XtorMapping* mapping)
{
  CcfgHelper ccfg(cfg);

  // Remove any ESL or Disconnects that we are now connecting
  // to this Xtor port
  removeESLPort(mCfg, mapping);

  UtString portName; portName << mapping->mXtorPortName;
  UInt32 portIndex = 0;
  bool foundPort = xtorInst->getType()->findPort(portName.c_str(), &portIndex);
  INFO_ASSERT(foundPort, "Expected to find port");

  CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(mapping->mRTLPortName);
  INFO_ASSERT(rtlPort, "Expecting RTL Port");

  CarbonCfgXtorConn* xtorConn = new CarbonCfgXtorConn(rtlPort, xtorInst, portIndex);
  rtlPort->connect(xtorConn);

  // Now update the tree to show this new connection
  ESLXtorInstanceConnItem* connItem = getTree()->findXtorInstancePort(getItem(), portName.c_str());
  if (connItem)
  {
    connItem->setConnection(xtorConn);
    connItem->setSelected(true);
  }

  return xtorConn;
}

CarbonCfgXtorConn* ESLXtorInstanceItem::ChangeXtorCommand::disconnectRTLPort(CarbonCfg* cfg, CarbonCfgXtorInstance* xtorInst, XtorMapping* mapping)
{
  CcfgHelper ccfg(cfg);

  // Remove it as an ESL port or disconnect, if needed
  removeESLPort(mCfg, mapping);

  UtString portName; portName << mapping->mXtorPortName;
  UInt32 portIndex = 0;
  bool foundPort = xtorInst->getType()->findPort(portName.c_str(), &portIndex);
  INFO_ASSERT(foundPort, "Expected to find port");

  CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(mapping->mRTLPortName);
  INFO_ASSERT(rtlPort, "Expecting RTL Port");

  // Find the connection for this port
  CarbonCfgXtorConn* xtorConn = xtorInst->getConnection(portIndex);
  INFO_ASSERT(xtorConn->getRTLPort() == rtlPort, "Mismatching port");

  // Find the node for this connection before we break it
  ESLXtorInstanceConnItem* connItem = getTree()->findXtorInstancePort(getItem(), portName.c_str());
  
  // Break the connection
  mCfg->disconnect(xtorConn);

  // Get the newly allocated connection
  xtorConn = xtorInst->getConnection(portIndex);

  // Cause the tree item to show the newly broken connection
  if (connItem)
  {
    connItem->setConnection(xtorConn);
    connItem->setSelected(true);
  }

  return xtorConn;
}


// If this mapping was an ESL port, remove the ESL port, if it was a Disconnect
// remove the Disconnect.
void ESLXtorInstanceItem::ChangeXtorCommand::removeESLPort(CarbonCfg* cfg, XtorMapping* mapping)
{
  CcfgHelper ccfg(cfg);

  CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(mapping->mRTLPortName);
  // Remove the ESL Port?
  if (mapping->mWasESLPort)
  {
    QTreeWidgetItem* portsRoot = getTree()->getEditor()->getPortsRoot();
    for (UInt32 c=0; c<rtlPort->numConnections(); c++)
    {
      CarbonCfgRTLConnection* conn = rtlPort->getConnection(c);
      if (conn->castESLPort())
      {
        CarbonCfgESLPort* eslPort = conn->castESLPort();
        // Copy the ESL Port
        CarbonCfgRTLPort* rtlp = eslPort->getRTLPort();
        mapping->mESLPorts.append(new CarbonCfgESLPort(eslPort, rtlp));

        ESLPortItem* eslItem = getTree()->findESLPort(portsRoot, eslPort->getName());
        if (eslItem && eslPort)
        {
          qDebug() << "removing esl port" << eslPort->getName();
          // Remove it from the tree and data model
          ESLPortItem::removeESLPort(eslItem, eslPort->getName());
        }
      }
    }
  }
  else if (mapping->mWasDisconnected)
  {
    QTreeWidgetItem* disRoot = getTree()->getEditor()->getDisconnectsRoot();
    ESLDisconnectItem* disItem = getTree()->findDisconnect(disRoot, rtlPort->getName());
    if (disItem)
    {
      qDebug() << "removing disconnect" << rtlPort->getName();
      getTree()->removeItem(disItem);
    }
  }
}


void ESLXtorInstanceItem::ChangeXtorCommand::removeESLPorts(CarbonCfg* cfg, XtorMapping*, const QString& rtlPortName)
{
  CcfgHelper ccfg(cfg);

  CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(rtlPortName);
  // Remove the ESL Port?

  QTreeWidgetItem* portsRoot = getTree()->getEditor()->getPortsRoot();
  for (UInt32 c=0; c<rtlPort->numConnections(); c++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(c);
    if (conn->castESLPort())
    {
      CarbonCfgESLPort* eslPort = conn->castESLPort();
      ESLPortItem* eslItem = getTree()->findESLPort(portsRoot, eslPort->getName());
      if (eslItem && eslPort)
      {
        qDebug() << "removing esl port" << eslPort->getName();
        // Remove it from the tree and data model
        ESLPortItem::removeESLPort(eslItem, eslPort->getName());
      }
    }
  }
}


void ESLXtorInstanceItem::ChangeXtorCommand::reconstructMapping(XtorMapping* mapping)
{
  CcfgHelper ccfg(mCfg);

  CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(mapping->mRTLPortName);
  if (mapping->mWasESLPort)
  {
    foreach(CarbonCfgESLPort* eslPort, mapping->mESLPorts)
    {
      QString eslPortName = eslPort->getName();
      ESLPortItem* newItem = ESLPortItem::makeESLPort(mCfg, rtlPort, getTree(), eslPortName);
      INFO_ASSERT(newItem, "Expected to create ESL Port");
      newItem->setPortExpr(eslPort->getExpr());
    }
  }
  else if (mapping->mWasDisconnected)
  {
    QTreeWidgetItem* disconnectsRoot = getTree()->getEditor()->getDisconnectsRoot();
    ESLDisconnectItem* newItem = new ESLDisconnectItem(disconnectsRoot, rtlPort); 
    newItem->setSelected(true);
  }
}

void ESLXtorInstanceItem::ChangeXtorCommand::undo()
{
  CcfgHelper ccfg(mCfg);
  CarbonCfgXtorInstance* xtorInst = ccfg.findXtorInstance(mXtorInstName);
  INFO_ASSERT(xtorInst, "Expected to find Xtor Instance");

  foreach(XtorMapping* oldMapping, mOldMappings.values())
  {
    XtorMapping* newMapping = mNewMappings[oldMapping->mXtorPortName];

    INFO_ASSERT(oldMapping && newMapping, "Expecting matches");

    if (oldMapping->mRTLPortName != newMapping->mRTLPortName)
    {
      qDebug() << "Undo Mapping from" << newMapping->mRTLPortName << "to" << oldMapping->mRTLPortName;

      // 1 of 3 states, nothing -> binding, binding -> nothing, binding -> new binding
      if (newMapping->mRTLPortName.isEmpty() && !oldMapping->mRTLPortName.isEmpty())
      {
        connectRTLPort(mCfg, xtorInst, oldMapping);
        if (newMapping->mWasESLPort)
          removeESLPorts(mCfg, newMapping, oldMapping->mRTLPortName);
     
        reconstructMapping(newMapping);
      }
      else if (!newMapping->mRTLPortName.isEmpty() && oldMapping->mRTLPortName.isEmpty())
      {
        disconnectRTLPort(mCfg, xtorInst, newMapping);
        reconstructMapping(newMapping);
      }
      else // New mapping
      {
        disconnectRTLPort(mCfg, xtorInst, newMapping);
        reconstructMapping(newMapping);

        PortTreeIndex eslPortIndex;
        QString eslPortName;
        CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(newMapping->mRTLPortName);
        ESLPortItem* newItem = ESLPortItem::makeESLPort(mCfg, rtlPort, getTree(), eslPortName, eslPortIndex);
        if (newItem)
          newItem->setSelected(true);
        // Re-connect it back to the original RTL
        connectRTLPort(mCfg, xtorInst, oldMapping);
      }
    }
  }

  getTree()->setModified(false); 
}

void ESLXtorInstanceItem::ChangeXtorCommand::redo()
{
  CcfgHelper ccfg(mCfg);

  // Walk the new mappings, compare against the "old" to see if they 
  // are different, then apply changes
  INFO_ASSERT(mOldMappings.count() == mNewMappings.count(), "Expected equal maps");

  CarbonCfgXtorInstance* xtorInst = ccfg.findXtorInstance(mXtorInstName);
  INFO_ASSERT(xtorInst, "Expected Xtor Instance");

  foreach(XtorMapping* oldMapping, mOldMappings.values())
  {
    XtorMapping* newMapping = mNewMappings[oldMapping->mXtorPortName];

    INFO_ASSERT(oldMapping && newMapping, "Expecting matches");

    oldMapping->mESLPorts.clear();
    newMapping->mESLPorts.clear();

    if (oldMapping->mRTLPortName != newMapping->mRTLPortName)
    {
      qDebug() << "Changed Mapping from" << oldMapping->mRTLPortName << "to" << newMapping->mRTLPortName;

      // 1 of 3 states, nothing -> binding, binding -> nothing, binding -> new binding
      if (oldMapping->mRTLPortName.isEmpty() && !newMapping->mRTLPortName.isEmpty())
      {
        // we are changing from either a ESLPort(s) or a Disconnect to
        // connecting to this Xtor Port
        connectRTLPort(mCfg, xtorInst, newMapping);
      }
      else if (!oldMapping->mRTLPortName.isEmpty() && newMapping->mRTLPortName.isEmpty())
      {
        disconnectRTLPort(mCfg, xtorInst, oldMapping);
        CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(oldMapping->mRTLPortName);
        ESLPortItem* newItem = ESLPortItem::makeESLPort(mCfg, rtlPort, 
          getTree(), newMapping->mESLPortName, newMapping->mPortIndex);
        newMapping->mWasESLPort = newItem ? true : false;
      }
      else // New mapping
      {
        disconnectRTLPort(mCfg, xtorInst, oldMapping);
        CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(oldMapping->mRTLPortName);
        
        // Make it an ESL port if need be
        ESLPortItem* newItem = ESLPortItem::makeESLPort(mCfg, rtlPort, getTree(), 
          newMapping->mESLPortName, newMapping->mPortIndex);
        
        newMapping->mWasESLPort = newItem ? true : false;

        // Connect the Xtor to the new RTL
        connectRTLPort(mCfg, xtorInst, newMapping);
      }
    }
  }

  getTree()->setModified(true);
}

void ESLXtorInstanceParameterItem::multiSelectContextMenu(QMenu& menu)
{
  QAction* actionConnectRTL = getActions()->getAction(ESLXtorParameterActions::ConnectRTL);

  QList<PortEditorTreeItem*> items;
  getTree()->fillSelectedItems(items, PortEditorTreeItem::XtorInstParameter);
  
  if (items.count() == 1)
    menu.addAction(actionConnectRTL);
}

void ESLXtorParameterActions::actionConnectRTL()
{
  qDebug() << "Connect RTL";

  DlgSelectRTL dlg(mTree->getCcfg(), DlgSelectRTL::InputPorts, false, mTree);
  if (dlg.exec())
  {
    QList<PortEditorTreeItem*> items;
    mTree->fillSelectedItems(items, PortEditorTreeItem::XtorInstParameter);
    QStringList rtlPorts = dlg.getPorts();
    ESLXtorInstanceParameterItem* item = dynamic_cast<ESLXtorInstanceParameterItem*>(items.first());
    mTree->getUndoStack()->push(new ESLXtorInstanceParameterItem::ConnectRTL(item, rtlPorts));
  }
}

ESLXtorInstanceParameterItem::ConnectRTL::ConnectRTL(ESLXtorInstanceParameterItem* item,  
                                         const QStringList& rtlPorts, QUndoCommand* parent)
                                         : TreeUndoCommand(item, parent)
{
  CarbonCfgXtorParamInst* paramInst = item->getParamInst();
  mXtorInstanceName = paramInst->getInstance()->getName();
  mRTLPorts = rtlPorts;
  mCfg = item->getCcfg();

  setText(QString("Connect %1 RTL Port(s) to %2")
    .arg(rtlPorts.count())
    .arg(paramInst->getParam()->getName()));

}

void ESLXtorInstanceParameterItem::ConnectRTL::undo()
{
  CcfgHelper ccfg(mCfg);
  ESLXtorInstanceParameterItem* item = dynamic_cast<ESLXtorInstanceParameterItem*>(getItem());
  CarbonCfgXtorParamInst* paramInst = item->getParamInst();
  qDebug() << "Undo Connect Parameter" << paramInst->getParam()->getName() << "to" << mRTLPorts;

  // Disconnect the RTL
  foreach (QString rtlPortName, mRTLPorts)
  {
    CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(rtlPortName);
    paramInst->removeRTLPort(rtlPort);
    
   // Break the connection from the RTL port to this Parameter
    for (UInt32 i=0; i<rtlPort->numConnections(); i++)
    {
      CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
      CarbonCfgTieParam* tiep = conn->castTieParam();
      if (tiep && mXtorInstanceName == tiep->mXtorInstanceName.c_str())
        ccfg.disconnect(tiep);
    }

    foreach(QString eslPortName, mRemovedESLPorts[rtlPort->getName()])
    {
      qDebug() << "re-creating eslport" << eslPortName << "for" << rtlPort->getName();
      ESLPortItem::makeESLPort(mCfg, rtlPort, getTree(), eslPortName);
    }
  } 
  // Remove the Items
  foreach(PortTreeIndex index, mPortIndexes)
  {
    ESLXtorInstanceParameterRTLPortItem* oldItem = dynamic_cast<ESLXtorInstanceParameterRTLPortItem*>(getTree()->itemFromIndex(index));
    INFO_ASSERT(oldItem, "Expecting item");
    getTree()->removeItem(oldItem);
  }

  getTree()->scrollToItem(item);
  getTree()->setModified(false);
}

void ESLXtorInstanceParameterItem::ConnectRTL::redo()
{
  CcfgHelper ccfg(mCfg);
  ESLXtorInstanceParameterItem* item = dynamic_cast<ESLXtorInstanceParameterItem*>(getItem());
  CarbonCfgXtorParamInst* paramInst = item->getParamInst();
  qDebug() << "Connect Xtor Parameter" << paramInst->getParam()->getName() << "to" << mRTLPorts;

  mPortIndexes.clear();
  mRemovedESLPorts.clear();

  // Tie the RTL to the Parameter
  foreach (QString rtlPortName, mRTLPorts)
  {
    CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(rtlPortName);
    mRemovedESLPorts[rtlPort->getName()].clear();
    if (rtlPort->isESLPort())
    {
      // Remove all ESL ports associated with this RTL port
      for (UInt32 i=0; i<rtlPort->numConnections(); i++)
      {
        CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
        if (conn->castESLPort())
        {
          CarbonCfgESLPort* eslPort = conn->castESLPort();
          QString eslPortName = eslPort->getName();

          mRemovedESLPorts[rtlPort->getName()].append(eslPortName);
          // Find the ESL Port
          ESLPortItem* eslItem = getTree()->findESLPort(getTree()->getEditor()->getPortsRoot(), eslPortName);
          INFO_ASSERT(eslItem, "Expected ESL Port");
          ESLPortItem::removeESLPort(eslItem, eslPortName);
        }
      }
    }
    ccfg.disconnect(rtlPort);

    paramInst->addRTLPort(rtlPort);
    carbonCfgTieParam(rtlPort, paramInst->getInstance()->getName(), paramInst->getParam()->getName());

    ESLXtorInstanceParameterRTLPortItem* newItem = new ESLXtorInstanceParameterRTLPortItem(item, paramInst, rtlPort);
    newItem->setSelected(true);
    PortTreeIndex index = getTree()->indexFromItem(newItem);
    mPortIndexes.append(index);
  }

  getTree()->scrollToItem(item);
  getTree()->sortChildren(item);
  item->setExpanded(true);
  getTree()->setModified(true);
}
