//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "util/OSWrapper.h"

#include "DlgNewParam.h"
#include "Validators.h"

DlgNewParam::DlgNewParam(CarbonCfg* cfg, QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);
  mCfg = cfg;
  ui.comboBoxTypes->addItem("UInt32", (int)eCarbonCfgXtorUInt32);
  ui.comboBoxTypes->addItem("Bool", (int)eCarbonCfgXtorBool);
  ui.comboBoxTypes->addItem("Double", (int)eCarbonCfgXtorDouble);
  ui.comboBoxTypes->addItem("String", (int)eCarbonCfgXtorString);
  ui.comboBoxTypes->addItem("UInt64", (int)eCarbonCfgXtorUInt64);
  ui.radioButtonInitTime->setChecked(true);
  ui.lineEditParamName->setFocus();
  mValidator = new ParameterNameValidator(ui.lineEditParamName, mCfg);
  ui.lineEditParamName->setValidator(new ParameterNameValidator(ui.lineEditParamName, mCfg));

  QPushButton* okButton = ui.buttonBox->button(QDialogButtonBox::Ok);
  okButton->setEnabled(false);
}

DlgNewParam::~DlgNewParam()
{
}

void DlgNewParam::on_buttonBox_rejected()
{
  reject();
}

void DlgNewParam::on_buttonBox_accepted()
{
  if (ui.radioButtonInitTime->isChecked())
    mScope = eCarbonCfgXtorParamInit;
  else
    mScope = eCarbonCfgXtorParamRuntime;

  mName = ui.lineEditParamName->text();
  mValue = ui.widgetParamEditor->defaultText();

  if ( ui.widgetParamEditor->isEnumerated() ) {
    QStringList choices = ui.widgetParamEditor->choices();
    mEnumChoices = "|" + choices.join("|") + "|";
  } else {
    mEnumChoices = "";
  }
  
  QVariant vType = ui.comboBoxTypes->itemData(ui.comboBoxTypes->currentIndex());

  mType = (CarbonCfgParamDataType)vType.toInt();
  
  mDescription = ui.lineEdit->text();

  if (checkValues())
    accept();
}

bool DlgNewParam::checkValues()
{
  bool result = true;

  switch (mType)
  {
  case eCarbonCfgXtorBool:
    if (mValue == "true" || mValue == "false")
      return true;
    else
    {
      ui.widgetParamEditor->setFocus();
      QMessageBox::critical(NULL, "Illegal Value",
                            "Only 'true' or 'false' are allowed for boolean values");
      return false;
    }
    break;
  case eCarbonCfgXtorUInt32:
    {
      bool ok = false;
      mValue.toUInt(&ok, 0);  // use base 0 for automatic base detection
      if (!ok)
      {
        ui.widgetParamEditor->setFocus();
        QMessageBox::critical(NULL, "Illegal Value",
              "Illegal unsigned 32-bit integer value");
        return false;
      }
      break;
    }
  case eCarbonCfgXtorUInt64:
    {
      bool ok = false;
      mValue.toULongLong(&ok, 0);  // use base 0 for automatic base detection
      if (!ok)
      {
        ui.widgetParamEditor->setFocus();
        QMessageBox::critical(NULL, "Illegal Value",
              "Illegal unsigned 64-bit integer value");
        return false;
      }
      break;
    }
  case eCarbonCfgXtorDouble:
  {
    bool ok = false;
    mValue.toDouble(&ok);
    if (!ok)
    {
      ui.widgetParamEditor->setFocus();
      QMessageBox::critical(NULL, "Illegal Value",
                            "Illegal double value");
      return false;
    }
    break;
  }
  case eCarbonCfgXtorString:
    break; // everything is allowed for string values 
  }


  int len = mEnumChoices.length();
  if ( len ) {
    // check the individual enumerated values for valid values
    switch (mType) {
    case eCarbonCfgXtorBool:
    {
      QMessageBox::critical(NULL, "Invalid enum value string", "Enums cannot be defined for Boolean parameters" );
      return false;
    } 
    case eCarbonCfgXtorUInt32:
    case eCarbonCfgXtorUInt64:
    case eCarbonCfgXtorDouble:
    {
      // check each term
      bool checkInt = (mType == eCarbonCfgXtorUInt32); // true: checkInt, false: check double
      bool checkLong = (mType == eCarbonCfgXtorUInt64); // true: checkLong, false: check double
      QStringList terms = mEnumChoices.split("|"); // delimiter is always '|'
      for (int cur = 1; cur < terms.size()-1; cur++ ) { // ignore first and last, they are empty due to the  syntax for enum storage
        bool ok = false;
        if ( checkInt ) {
          terms.at(cur).toUInt(&ok,0); // use base 0 so that automatic base detection is done "0x"prefix->hex, "0"prefix->octal, otherwise decimal
        } if (checkLong) {
          terms.at(cur).toULongLong(&ok);
        } else {
          terms.at(cur).toDouble(&ok);
        }
        if (!ok)
        {
          ui.widgetParamEditor->setFocus();
          UtString msg;
          msg << "Illegal " << (checkInt ? "unsigned integer" : "double") << " value at position " << cur << " with value: '" << terms.at(cur) << "'";
          QMessageBox::critical(NULL, "Illegal Value", msg.c_str() );
          return false;
        } 
      }
    }
    case eCarbonCfgXtorString:
      break; // everything is allowed for string enums
    }
  }

  return result;
}

void DlgNewParam::on_lineEditParamName_textChanged(const QString& newName)
{
   QPushButton* okButton = ui.buttonBox->button(QDialogButtonBox::Ok);
   okButton->setEnabled(mValidator->isValid(mCfg, newName));
}
