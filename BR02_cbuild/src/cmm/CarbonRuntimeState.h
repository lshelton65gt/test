// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _CarbonRuntimeState_h_
#define _CarbonRuntimeState_h_

#include "util/CarbonPlatform.h"
#include "util/SourceLocator.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"

#include <QtGui/QMainWindow>
#include "shell/carbon_shelltypes.h"

#include "RemoteConsole.h"
#include "CarbonConsole.h"

class CarbonMakerContext;
struct CarbonReplaySystem;

class CarbonRuntimeState : public QObject
{
  Q_OBJECT

public:
  CarbonRuntimeState(CarbonMakerContext*);
  ~CarbonRuntimeState();

  bool loadCarbonSystem(const char *fname);
  void putSelectedModels(UtStringArray &selection);

  void replayVerbose(bool enabled);
  void replayDatabase(const char *db);
  void replayRecord();
  void replayPlayback();
  void replayStop();
  void replayCheckpointInterval(UInt32 interval);
  void replayRecoverPercentage(UInt32 percent);

  void onDemandStart();
  void onDemandStop();
  void onDemandTrace(bool enable);
  void onDemandMaxStates(UInt32 states);
  void onDemandBackoffStates(UInt32 states);
  void onDemandBackoffDecayPercent(UInt32 percent);
  void onDemandBackoffMaxDecay(UInt32 maxDecay);
  void onDemandBackoffStrategy(CarbonOnDemandBackoffStrategy strategy);
  void onDemandExcludedStateSignals(const char* compName, const char* sigs);
  void onDemandExcludedInputSignals(const char* compName, const char* sigs);

  bool isContinueSimEnabled() { return mContinueSimEnabled; }
  void continueSimulation();

  UInt32 numModelsSelected() { return mSelectedModels.size(); }
  const char* getSelectedModel(UInt32 i)
  {
    INFO_ASSERT(i < mSelectedModels.size(), "No such index");
    return mSelectedModels[i];
  }
  void replayModelCounts(UInt32 *normal, UInt32 *playback, UInt32 *record);
  void onDemandModelCounts(UInt32 *start, UInt32 *stop);

  const char *getOnDemandTraceFile(const char* compName);
  bool onDemandTraceFileExists(const char* compName);

  const char *getFileName() { return mFileName.c_str(); }

  CarbonReplaySystem* getPollCarbonSystem() {return mPollCarbonSystem;}
  CarbonReplaySystem* getCarbonSystem() {return mCarbonSystem;}

signals:
  void carbonSystemUpdated(CarbonRuntimeState &state, CarbonReplaySystem &guiSystem, CarbonReplaySystem &simSystem);

private slots:
  void pollSimFile();
  void simulationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType);

private:
  bool save();
  //  bool saveAs();
  bool saveFile();

  //! Parse a semicolon-delimited list into an array of strings
  static bool sParseDelimitedList(const char* str, UtStringArray* array);

  CarbonReplaySystem *mCarbonSystem;
  CarbonReplaySystem *mPollCarbonSystem;
  QTimer             *mPollTimer;
  bool                mCyclesSpecified;
  bool                mSimRequestActive;
  bool                mContinueSimEnabled;
  UtStringArray       mSelectedModels;
  UtString            mFileName;
  CarbonMakerContext* mCarbonMakerContext;
};



#endif // _CarbonRuntimeState_h_
