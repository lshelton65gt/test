//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "CowareWizardWidget.h"
#include "Mdi.h"
#include "MdiTextEditor.h"

#include "CodeGenerator.h"
#include "CarbonMakerContext.h"
#include "WorkspaceModelMaker.h"
#include "CowareComponent.h"
#include "SystemCComponent.h"
#include "CarbonProjectWidget.h"

#include "util/UtString.h"
#include "util/UtHashMap.h"
#include "util/AtomicCache.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/ArgProc.h"
#include "util/AtomicCache.h"
#include "util/OSWrapper.h"
#include "util/RandomValGen.h"
#include "shell/CarbonAbstractRegister.h"

#include "cfg/carbon_cfg.h"
#include "cfg/carbon_cfg_misc.h"
#include "cfg/CarbonCfg.h"

#include "gui/CQt.h"

#include "shell/carbon_misc.h"

#include "shell/carbon_capi.h"
#include "CarbonOptions.h"
#include "SettingsEditor.h"

#include "CompWizardPortEditor.h"
#include "CompWizardMemEditor.h"

#define tabPORTS 0
#define tabREGISTERS 1
#define tabMEMORIES 2
#define tabPROFILE 3

struct librarySorter {
  librarySorter() {}

  bool operator()(const char* s1, const char* s2) const
  {
    return (strcasecmp(s1, s2) < 0);
  }
};

CowareWizardWidget::CowareWizardWidget(CarbonMakerContext* ctx, MDIDocumentTemplate* t, QWidget *parent)
: QWidget(parent)
{
  ui.setupUi(this);
  setAttribute(Qt::WA_DeleteOnClose);

  initializeMDI(this, t);
  mCtx = ctx;

  mCQt = ctx->getQtContext();
  m_pDB = NULL;
  m_cfgID = NULL;
  mReg = NULL;
  mSelectedNode = NULL;
  mMemorySelectedNode = NULL;
  mXtorInst = NULL;

  mPortEditor = ui.widget;
  mMemEditor = ui.widget1;

  mPortEditor->setEditorMode(CompWizardPortEditor::eModeCoWare);
  mMemEditor->setEditorMode(CompWizardMemEditor::eModeCoWare);

  CQT_CONNECT(mPortEditor, widgetModified(bool), this, setWindowModified(bool));
  CQT_CONNECT(mMemEditor, widgetModified(bool), this, setWindowModified(bool));

 /* m_pPortView = new PortView(NULL);
  m_pPortView->putTieoffTable(ui.tieoffList);
  m_pPortView->putDisconnectTable(ui.disconnectList);
  m_pPortView->putPortTable(ui.tablePorts);*/

  setWindowTitle("CoWare Component[*]");
  mHasEnvironment = initialize();

  CQT_CONNECT(mCtx->getCarbonProjectWidget()->getConsole(), compilationStarted(CarbonConsole::CommandMode,CarbonConsole::CommandType), this, compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType));
  CQT_CONNECT(mCtx->getCarbonProjectWidget()->getConsole(), compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType));

  CQT_CONNECT(mCtx->getCarbonProjectWidget()->project(), projectLoaded(const char*, CarbonProject*), this, projectLoaded(const char*, CarbonProject*));

  CarbonOptions* options = mCtx->getCarbonProjectWidget()->project()->getToolOptions("VSPCompiler");
  options->registerPropertyChanged("-o", "outputFilenameChanged", this);

  mPortEditor->createToolbar(this);
  mMemEditor->createToolbar(this);

  ui.tabRegisters->setCurrentIndex(tabPORTS);

}

void CowareWizardWidget::projectLoaded(const char*, CarbonProject*)
{
}

void CowareWizardWidget::outputFilenameChanged(const CarbonProperty*, const char*)
{
  const char* iodbName = mCtx->getCarbonProjectWidget()->project()->getDesignFilePath(".symtab.db");
  CowareComponent* coware = mCtx->getCarbonProjectWidget()->getActiveCowareComponent();

  UtString relIODBName;
  relIODBName << mCtx->getCarbonProjectWidget()->makeRelativePath(coware->getOutputDirectory(), iodbName);

  if (m_cfgID)
    carbonCfgPutIODBFile(m_cfgID, relIODBName.c_str());

  m_dbName = iodbName;

  setWindowModified(true);
}

// We could have finished the "compilation" process, which did NOT result in a new
// database, so just re-enable.
void CowareWizardWidget::compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  setEnabled(true);
}
void CowareWizardWidget::compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
 setEnabled(false);
 saveDocument();
}
void CowareWizardWidget::xtorSelectionChanged(CarbonCfgXtorInstance* xinst)
{
  mXtorInst = xinst;
}
void CowareWizardWidget::xtorBindingSelectionChanged(CarbonCfgXtorConn*)
{
}

CowareWizardWidget::~CowareWizardWidget()
{
  CarbonProject *carbonProject = mCtx->getCarbonProjectWidget()->project();
  if (carbonProject != NULL) {
	CarbonOptions* options = carbonProject->getToolOptions("VSPCompiler");
	options->unregisterPropertyChanged("-o", this);
  }
}

void CowareWizardWidget::warning(QWidget* widget, const QString& msg)
{
  if (mCQt == NULL)
    return;

  bool do_message_box = !(mCQt->isPlaybackActive() || mCQt->isRecordActive());
  UtIO::cerr() << msg << UtIO::endl;
  UtIO::cerr().flush();
  if (do_message_box) {
    QMessageBox::warning(widget, tr(APP_NAME), msg);
  }
}

eCarbonMsgCBStatus CowareWizardWidget::sMsgCallback(CarbonClientData /*clientData*/,
                                         CarbonMsgSeverity severity,
                                         int number, const char* text,
                                         unsigned int)
{
  //CowareWizardWidget* mw = (CowareWizardWidget*) clientData;
  const char* severityStr = "";
  switch (severity) {
  case eCarbonMsgStatus:   severityStr = "Status"; break;
  case eCarbonMsgNote:     severityStr = "Note"; break;
  case eCarbonMsgWarning:  severityStr = "Warning"; break;
  case eCarbonMsgError:    severityStr = "Error"; break;
  case eCarbonMsgFatal:    severityStr = "Fatal"; break;
  case eCarbonMsgSuppress: severityStr = "Suppress"; break;
  case eCarbonMsgAlert:    severityStr = "Alert"; break;
  }

  UtString buf;
  buf << severityStr << " " << number << ": " << text;

  //mw->warning(mw, buf.c_str());
  return eCarbonMsgContinue;
}

void CowareWizardWidget::saveDocument()
{
  CowareComponent* coware = mCtx->getCarbonProjectWidget()->getActiveCowareComponent();
  INFO_ASSERT(coware, "Missing Component");

  mPortEditor->save();
  mMemEditor->save();

  QFileInfo dbfi(m_dbName.c_str());
  UtString baseName;
  baseName << dbfi.baseName() << ".ccfg";
  
  UtString ccfgName;
  OSConstructFilePath(&ccfgName, coware->getOutputDirectory(), baseName.c_str());
  m_ccfgName = ccfgName;

  QFileInfo fi(m_ccfgName.c_str());

  if (isWindowModified() || !fi.exists())
  {
    ChangeOutputDirectory temp(coware);
    updateTopModuleName();
    updateComponentName();

    UtString relLibName;
    const char* outputFilename = mCtx->getCarbonProjectWidget()->project()->getToolOptions("VSPCompiler")->getValue("-o")->getValue();
    QFileInfo ofi(outputFilename);
    UtString libExt;
    libExt << "." << ofi.completeSuffix();

    const char* libFilePath = mCtx->getCarbonProjectWidget()->project()->getDesignFilePath(libExt.c_str());
    relLibName << CarbonProjectWidget::makeRelativePath(coware->getOutputDirectory(), libFilePath);
    carbonCfgPutLibName(m_cfgID, relLibName.c_str());

    m_cfgID->write(m_ccfgName.c_str());

    UtString relIODBName;
    if (!fi.isRelative())
      relIODBName << mCtx->getCarbonProjectWidget()->makeRelativePath(coware->getOutputDirectory(), m_dbName.c_str());
    else
      relIODBName << m_dbName;

    coware->getOptions()->putValue("IODB File", relIODBName.c_str());
    coware->getOptions()->putValue("Output Directory",  coware->getOutputDirectory());

    setWindowModified(false);
    setWidgetModified(false);

    qDebug() << "Save CoWare";

    qDebug() << "Updating CoWare Tree Icons";
    coware->updateTreeIcons();
  }
}

bool CowareWizardWidget::loadFile(const QString& qFilename)
{
  bool status = false;

  UtString filename, basename;
  filename << qFilename;
  ParseResult parseResult = parseFileName(filename.c_str(), &basename);

  status = loadDesignFile(filename.c_str(), parseResult);
  if (status)
  {
    if (eCcfg == parseResult)
      m_ccfgName = filename;

    if (m_pDB != NULL)
    {
      mContext->putDB(m_pDB);
      mContext->putCfg(m_cfgID);
    }
  }

  return status;
}

void CowareWizardWidget::updateTopModuleName()
{
  CowareComponent* coware = mCtx->getCarbonProjectWidget()->getActiveCowareComponent();
  INFO_ASSERT(coware, "Missing Component");

  const char* topModule = coware->getOptions()->getValue("Top Module")->getValue();
  if (topModule && strlen(topModule) > 0)
    m_cfgID->putTopModuleName(topModule);
  else
    coware->getOptions()->putValue("Top Module", m_cfgID->getTopModuleName());
}

void CowareWizardWidget::updateComponentName()
{
  CowareComponent* coware = mCtx->getCarbonProjectWidget()->getActiveCowareComponent();
  INFO_ASSERT(coware, "Missing Component");

  const char* moduleName = coware->getOptions()->getValue("Module Name")->getValue();
  if (moduleName && strlen(moduleName) > 0)
    m_cfgID->putCompName(moduleName);
  else
    coware->getOptions()->putValue("Module Name", m_cfgID->getCompName());
}

const char* CowareWizardWidget::userFriendlyName()
{
  CowareComponent* coware = mCtx->getCarbonProjectWidget()->getActiveCowareComponent();
  INFO_ASSERT(coware, "Missing Component");
  INFO_ASSERT(m_dbName.length() > 0, "NO DB Name");

  QFileInfo fi(m_dbName.c_str());
  UtString baseName;
  baseName << fi.baseName() << ".ccfg";
  UtString ccfgName;
  m_ccfgName.clear();
  OSConstructFilePath(&ccfgName, coware->getOutputDirectory(), baseName.c_str());
  m_ccfgName << ccfgName;
  return m_ccfgName.c_str();
}

bool CowareWizardWidget::maybeSave()
{
  if (isWindowModified())
  {
    QMessageBox::StandardButton ret;
    ret = QMessageBox::warning(this, MODELSTUDIO_TITLE,
      tr("'%1' has been modified.\n"
      "Do you want to save your changes?")
      .arg(userFriendlyName()),
      QMessageBox::Save | QMessageBox::Discard
      | QMessageBox::Cancel);
    if (ret == QMessageBox::Save)
    {
      saveDocument();
      return true;
    }
    else if (ret == QMessageBox::Discard && isWidgetModified())
    {
      setWindowModified(false);
      setWidgetModified(false);
    }
    else if (ret == QMessageBox::Cancel)
      return false;
  }
  return true;
}
void CowareWizardWidget::closeEvent(QCloseEvent *event)
{
  if (maybeSave())
  {
    mPortEditor->closeEvent(event);
    mMemEditor->closeEvent(event);

    event->accept();

    qDebug() << "Closing CoWare Wizard";

    MDIWidget::closeEvent(this);

    if (m_cfgID)
      delete m_cfgID;
  }
  else
  {
    event->ignore();
  }
}

bool CowareWizardWidget::initialize()
{
  mMsgCallback = carbonAddMsgCB(NULL, sMsgCallback, this);

  m_pDB = NULL;

  // Initialize Carbon Cfg
  m_cfgID = carbonCfgModeCreate(CarbonCfg::ePlatarch);
  if (carbonCfgReadXtorLib(m_cfgID, eCarbonXtorsCoware) == eCarbonCfgFailure) 
  {
    mCQt->warning(this, carbonCfgGetErrmsg(m_cfgID));
    return false;
  }

  populateXtorLibComboBox();
  xtorLibraryChanged(0);

  mContext = new MakerCowareApplicationContext(this, m_pDB, m_cfgID, mCQt);
  ui.tableRegisters->putContext(mContext);
  ui.tableFields->putContext(mContext, mCtx->getCarbonProjectWidget());
  
  // Registers
  CQT_CONNECT(ui.tableFields, selectionChanged(CarbonCfgRegisterField*), this, fieldSelectionChanged(CarbonCfgRegisterField*));
  CQT_CONNECT(ui.tableRegisters, regSelectionChanged(CarbonCfgRegister*), this, registerSelectionChanged(CarbonCfgRegister*));
  CQT_CONNECT(ui.tabRegisters, currentChanged(int), this, tabChanged(int));

  // Register Widgets

  return true;
}

void CowareWizardWidget::memSelectionChanged(CarbonDBNode* node)
{
  mMemorySelectedNode = node;
}

void CowareWizardWidget::tabChanged(int currentTabNumber)
{
  ui.tableFields->FinishEdit();

  if (currentTabNumber != tabPORTS)
  {
    QDockWidget* dw1 = mCtx->getWorkspaceModelMaker()->getRemoteConsoleDockWindow();
    dw1->setVisible(false);
    QDockWidget* dw2 = mCtx->getWorkspaceModelMaker()->getErrorInfoDockWindow();
    dw2->setVisible(false);
    QDockWidget* dw3 = mCtx->getCarbonProjectWidget()->getHierarchyDockWidget();
    if (dw3)
      dw3->setVisible(true);
  }
  else
  {
    QDockWidget* dw1 = mCtx->getWorkspaceModelMaker()->getRemoteConsoleDockWindow();
    dw1->setVisible(false);
    QDockWidget* dw2 = mCtx->getWorkspaceModelMaker()->getErrorInfoDockWindow();
    dw2->setVisible(false);
    QDockWidget* dw3 = mCtx->getCarbonProjectWidget()->getHierarchyDockWidget();
    if (dw3)
      dw3->setVisible(false);
  }
}
void CowareWizardWidget::populateXtorLibComboBox()
{
}

void CowareWizardWidget::xtorLibraryChanged(int)
{
}

void CowareWizardWidget::populateXtorComboBox(const char*)
{
}

void CowareWizardWidget::selectionChanged(CarbonDBNode* node)
{
  mSelectedNode = node;
}

void CowareWizardWidget::fieldSelectionChanged(CarbonCfgRegisterField* /*field*/)
{
  bool fieldSelected = ui.tableFields->selectedItems().count() > 0;
  ui.btnBindConstant->setEnabled(fieldSelected);
  ui.btnDelete->setEnabled(fieldSelected);
  ui.btnDeleteField->setEnabled(fieldSelected);
}

void CowareWizardWidget::registerSelectionChanged(CarbonCfgRegister* reg)
{
  mReg = reg;
  ui.tableFields->Initialize(mReg);
  ui.btnAddField->setEnabled(mReg != NULL);
  ui.btnDelete->setEnabled(mReg != NULL);
}

bool CowareWizardWidget::loadDesignFile(const char* filename, ParseResult parseResult) 
{
  bool modifiedFlag = false;
  // temporarilly change to the Output directory because the iodb path name
  // was stored relative to the output dir.
  CowareComponent* coware = mCtx->getCarbonProjectWidget()->getActiveCowareComponent();
  INFO_ASSERT(coware, "Expecting Component");

  ChangeOutputDirectory temp(coware);

  QFileInfo fi(filename);
  bool ret = false;
  if ((parseResult == eCcfg && !fi.exists()) ||
      (parseResult == eFullDB) || (parseResult == eIODB))
  {
    m_dbName.clear();

    CowareComponent* coware = mCtx->getCarbonProjectWidget()->getActiveCowareComponent();
    UtString relPath;
    relPath << mCtx->getCarbonProjectWidget()->makeRelativePath(coware->getOutputDirectory(), filename);

    m_dbName << relPath.c_str();
    if (fi.exists())
      ret = loadDatabase(filename, true);
    else
    {
      m_dbName = mCtx->getCarbonProjectWidget()->project()->getDesignFilePath(".symtab.db");
      ret = loadDatabase(m_dbName.c_str(), true);
    }
    modifiedFlag = true;
  }
  else if (fi.exists() && parseResult == eCcfg)
  {
    ret = loadConfig(filename, &modifiedFlag);
  }  

  updateTopModuleName();
  updateComponentName();

  setWindowModified(modifiedFlag);
  setWidgetModified(modifiedFlag);

  return ret;
}

bool CowareWizardWidget::loadConfig(const QString &qfileName, bool* modifiedFlag)
{
  const char* iodbName = mCtx->getCarbonProjectWidget()->project()->getDesignFilePath(".symtab.db");
  QFileInfo fi(iodbName);
  if (!fi.exists())
    return false;

  UtString fileName;
  fileName << qfileName;
  carbonCfgClear(m_cfgID);
  CarbonCfgStatus cfgStatus = carbonCfgRead(m_cfgID, fileName.c_str());
  bool success = cfgStatus != eCarbonCfgFailure;

  // The ccfg reader has a change where it may modify some pins
  // because they used to not be hooked up right. So if it did, we
  // have to mark the project as modified.
  if (m_cfgID->isModified()) {
    *modifiedFlag = true;
  }

  if (cfgStatus == eCarbonCfgInconsistent)
  {
    *modifiedFlag = true;
    QDockWidget* dw = mCtx->getCarbonProjectWidget()->context()->getWorkspaceModelMaker()->getErrorInfoDockWindow();
    dw->lower();
    dw->raise();
  }

  if (success) 
  {
    // Read in the IODB file to populate the debug tree
    const char* iodbFile = carbonCfgGetIODBFile(m_cfgID);
    success = loadDatabase(iodbFile, false);
    if (success) 
    {
      m_dbName.clear();
      m_dbName << iodbFile;

      mPortEditor->setCcfg(m_cfgID);
      mMemEditor->setCcfg(m_cfgID);
      //m_pPortView->Update();
    }
  }
  else 
  {
    warning(this, tr("Cannot read file %1:\n%2.")
      .arg(qfileName)
      .arg(carbonCfgGetErrmsg(m_cfgID)));
  }

  return success;
}
CowareWizardWidget::ParseResult CowareWizardWidget::parseFileName(const char* filename,
                                            UtString* baseName)
{
  *baseName = filename;
  UInt32 sz = strlen(filename);

  ParseResult parseResult = eNoMatch;
  baseName->clear();

  if ((sz > 6) && (strcmp(filename + sz - 6, ".io.db") == 0)) {
    parseResult = eIODB;
    baseName->append(filename, sz - 6);
  }
  else if ((sz > 10) && (strcmp(filename + sz - 10, ".symtab.db") == 0)) {
    parseResult = eFullDB;
    baseName->append(filename, sz - 10);
  }
  else if ((sz > 5) && (strcmp(filename + sz - 5, ".ccfg") == 0)) {
    parseResult = eCcfg;
    baseName->append(filename, sz - 5);
  }
  return parseResult;
} 


bool CowareWizardWidget::loadDatabase(const char* iodbName, bool initPorts)
{   
  QFileInfo fi(iodbName);

  if (!fi.exists())
    return false;

  QApplication::setOverrideCursor(Qt::WaitCursor);

  CowareComponent* coware = mCtx->getCarbonProjectWidget()->getActiveCowareComponent();

  bool success = true;
  if (m_cfgID->getDB() == NULL)
  {
    CarbonDB* newDB = carbonDBOpenFile(iodbName);

    if (newDB == NULL) 
    {
      QApplication::restoreOverrideCursor();
      return false;
    }

    m_pDB = newDB;
    m_cfgID->putDB(m_pDB);  // Ownership transferred to CarbonCfg
  }
  else
    m_pDB = m_cfgID->getDB(); 

  mContext->putDB(m_pDB);
  mContext->putCfg(m_cfgID);

  UtString baseFile, errmsg;
  parseFileName(iodbName, &baseFile);

  if (initPorts) 
  {
    UtString libFile(baseFile);
#ifdef Q_OS_WIN
    libFile += ".dll";
#else
    libFile += ".so";
#endif
    carbonCfgPutLibName(m_cfgID, libFile.c_str());
    setWindowModified(true);
    coware->createDefaultComponent();

     QString ccfgFilePath = coware->ccfgName();
    
    qDebug() << carbonCfgGetErrmsg(m_cfgID);
    qDebug() << "Loading from" << ccfgFilePath;
    
    QFileInfo ccfgFi(ccfgFilePath);
    INFO_ASSERT(ccfgFi.exists(), "Expecting ccfg file");

    TempChangeDirectory cd(ccfgFi.absolutePath());
    UtString uCcfgFilePath;
    uCcfgFilePath << ccfgFilePath;

    CarbonCfgStatus cfgStatus = m_cfgID->read(uCcfgFilePath.c_str());
    if (cfgStatus != eCarbonCfgFailure)
    {
      qDebug() << "Read file" << ccfgFilePath;
    }
    else
    {
      qDebug() << "Error reading file" << ccfgFilePath;
      QApplication::restoreOverrideCursor();
      return false;
    }
  }

  ui.tableRegisters->Populate();
 
  if (initPorts)
  {
    mPortEditor->setCcfg(m_cfgID);
    mPortEditor->treeWidgetModified(true);
    mMemEditor->setCcfg(m_cfgID);
    mMemEditor->treeWidgetModified(true);
  }

  UtString relIODBName;
  relIODBName << mCtx->getCarbonProjectWidget()->makeRelativePath(coware->getOutputDirectory(), iodbName);

  carbonCfgPutIODBFile(m_cfgID, relIODBName.c_str());
  carbonCfgPutCcfgMode(m_cfgID, eCarbonCfgCOWARE);

  if (initPorts)
  {
    carbonCfgPutCompName(m_cfgID, carbonDBGetTopLevelModuleName(m_pDB));
    carbonCfgPutTopModuleName(m_cfgID, carbonDBGetTopLevelModuleName(m_pDB));
  }

  QApplication::restoreOverrideCursor();

  return success;
}

void CowareWizardWidget::xtorBindingsChanged()
{
}

// Action handlers

void CowareWizardWidget::on_btnBindConstant_clicked()
{
   ui.tableFields->BindConstant();
}

void CowareWizardWidget::on_btnDeleteField_clicked()
{
  ui.tableFields->DeleteField();
}

void CowareWizardWidget::on_btnAddField_clicked()
{
  ui.tableFields->AddField();
}

void CowareWizardWidget::on_btnAdd_clicked()
{
  ui.tableRegisters->AddRegister();
}

void CowareWizardWidget::on_btnDelete_clicked()
{
  ui.tableRegisters->DeleteRegister();
}


bool CowareWizardWidget::save()
{
  if (m_ccfgName.length() == 0)
    return saveAs();
  else
  {
    qDebug() << "CoWare Wizard Writing " << m_ccfgName.c_str();
    m_cfgID->write(m_ccfgName.c_str());
    return true;
  }
}

bool CowareWizardWidget::saveAs()
{
  UtString filename;
  
  bool ret = mCQt->popupFileDialog(this, false, // readFile
                                   "Create a Carbon Configuration",
                                   "Carbon Configuration (*.ccfg)",
                                   &filename);
  if (ret)
  {
    m_ccfgName.clear();
    m_ccfgName << filename.c_str();
    m_cfgID->write(m_ccfgName.c_str());
    return true;
  }
  else
    return false;
}

ChangeOutputDirectory::ChangeOutputDirectory(CowareComponent* coware)
{
  OSGetCurrentDir(&mCurrDir);
  UtString err;
  OSChdir(coware->getOutputDirectory(), &err);
}

ChangeOutputDirectory::ChangeOutputDirectory(SystemCComponent* systemc)
{
  OSGetCurrentDir(&mCurrDir);
  UtString err;
  OSChdir(systemc->getOutputDirectory(), &err);
}


ChangeOutputDirectory::~ChangeOutputDirectory()
{
  UtString err;
  OSChdir(mCurrDir.c_str(), &err);
}

void MakerCowareApplicationContext::setDocumentModified(bool value)
{
  mWizard->setWindowModified(value);
}
