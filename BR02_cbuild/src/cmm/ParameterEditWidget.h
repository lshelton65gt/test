#ifndef PARAMETEREDITWIDGET_H
#define PARAMETEREDITWIDGET_H

#include "util/CarbonPlatform.h"

#include <QtGui/QMainWindow>
#include "ui_ParameterEditWidget.h"

class ParameterEditWidget : public QWidget
{
    Q_OBJECT

public:

    ParameterEditWidget(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~ParameterEditWidget();

    void setIsEnumerated();
    bool isEnumerated();

    // use this to fill in the enumerated choices
    void setChoices(const QStringList& choices);
    QStringList choices();

    // use this to define the default choice, or to fill in the single value when !isEnumerated()
    void setDefaultText(const QString& value);
    QString defaultText();

private:
    Ui::ParameterEditWidgetClass ui;  
 
private slots:
    void on_checkBoxEnumValues_stateChanged(int);
 
};

#endif // PARAMETEREDITWIDGET_H
