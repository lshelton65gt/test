#ifndef DLGADDMAXSIMXTOR_H
#define DLGADDMAXSIMXTOR_H

#include "util/CarbonPlatform.h"

#include <QDialog>
#include "ui_DlgAddMaxsimXtor.h"
#include "gui/CQt.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/CarbonCfg.h"

class DlgAdvancedMatching;

class DlgAddMaxsimXtor : public QDialog
{
  Q_OBJECT

public:
  enum { colPORTNAME, colRTLNAME, colMATCHES };

  DlgAddMaxsimXtor(CarbonCfg* cfg,
              const QString& xtorName, CarbonCfgXtorInstance* xtorInst = 0, QWidget *parent = 0);
  ~DlgAddMaxsimXtor();

  CarbonCfgXtor* getXtor() { return mXtor; }
  quint32 numPorts() const { return ui.tableWidget->rowCount(); }
  void getMapping(quint32 index,  CarbonCfgXtorPort** xtorPort, 
                    CarbonCfgRTLPort** rtlPort);

private:
  CarbonCfgXtorLib* mXtorLib;
  QString mXtorName;
  CarbonCfgXtor* mXtor;
  CarbonCfgXtorInstance* mXtorInst;
  CarbonCfg* mCfg;
  DlgAdvancedMatching* mAdvancedMatch;

public:
  Ui::DlgAddMaxsimXtorClass ui;
  QMap<QString, CarbonCfgRTLPort*> mRTLPortMap;  // Case Sensitive
  QMap<QString, CarbonCfgRTLPort*> mRTLPortMapU; // Case Insensitive
  QMap<QString, QString> mRTLLeafName;

private:
  void apply();
  void reset();
  void populatePorts();
  void showPotentialMatches();
  QString getLeafName(const QString& rtlPortName);
  CarbonCfgRTLPort* findRTLPort(const QString& rtlPortName);
  CarbonCfgRTLPort* findRTLPortU(const QString& rtlPortName);

private slots:
    void on_buttonBox_clicked(QAbstractButton*);
	void on_pushButtonAdvanced_clicked();
    void on_lineEditSuffix_textChanged(const QString &);
    void on_lineEditPrefix_textChanged(const QString &);
    void on_comboBoxXtors_currentIndexChanged(QString);
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
};

#endif // DLGADDMAXSIMXTOR_H
