//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QtXml>
#include <QtGui>
#include <QDebug>

#include "Parameters.h"
#include "ParametersTreeDelegate.h"

void Parameters::addParam(Parameter* p)
{
  mParams.append(p);  

  // for the script to be able to access the object,
  // force the cast to QObject*
  QVariant v = qVariantFromValue((QObject*)p);

  // Collapse spaces
  QString name = p->getName().replace(" ", "");

  //qDebug() << "defining Parameters property: " << name;

  setProperty(name.toAscii(), v); // this_toascii_ok
}


Parameters::Parameters(QObject* parent) : QObject(parent)
{
}

QObject* Parameters::findParameter(const QString& name)
{
  foreach (Parameter* p, mParams)
  {
    if (name == p->getName())
      return p;
  }

  return NULL;
}

Parameter* Parameters::findParam(const QString& name)
{
  foreach (Parameter* p, mParams)
  {
    if (name == p->getName())
      return p;
  }

  return NULL;
}

void Parameter::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index)
{
  QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
  if (lineEdit)
  {
    model->setData(index, lineEdit->text());
    setValue(lineEdit->text());
  }
}

// Create an editor for this parameter, default is a TextWidget
QWidget* Parameter::createEditor(QTreeWidget*, QWidget* parent, const QString& currValue)
{
  QLineEdit* lineEdit = new QLineEdit(parent);

  QString checkExpr = getValidExpression();
  if (checkExpr.length() > 0)
  {
    QRegExp rx(checkExpr);
    QValidator *validator = new QRegExpValidator(rx, parent);
    lineEdit->setValidator(validator);
  }

  lineEdit->setText(currValue);
  lineEdit->setFocus();
  return lineEdit;
}

Parameter::ParameterType Parameter::fromString(const QString& val)
{
  QString v = val.toLower();
  if ("integer" == v)
    return Parameter::Integer;
  else if ("bool" == v)
    return Parameter::Bool;
  else if ("enum" == v)
    return Parameter::Enum;

  return Parameter::String;
}

bool Parameters::deserialize(const QDomElement& parent, XmlErrorHandler* eh)
{
  QDomNode n = parent.firstChild();

  while(!n.isNull())
  {
    QDomElement e = n.toElement(); // try to convert the node to an element.

    //qDebug() << e.tagName();

    if (!e.isNull())
    {
      if ("Parameters" == e.tagName())
      {
        if (!Parameter::deserialize(this, e, eh))
          return false;
      }

    }
    n = n.nextSibling();
  }
  return true;
}
Parameter* Parameter::copyParameterData(Parameter* p)
{
  p->setPrompt(getPrompt());
  p->setDescription(getDescription());
  p->setDefaultValue(getDefaultValue());
  p->setValue(getValue());
  p->setName(getName());
  p->setType(getType());
  return p;
}

Parameter* Parameter::copyParameter()
{
  Parameter* p = new Parameter();
  p->copyParameterData(this);
  return p;
}

bool Parameter::hasValueSet()
{
  bool retValue = true;
  if (mRequired)
    return mValue.isValid() && mValue.toString().length() > 0;

  return retValue;
}

bool Parameter::deserialize(Parameters* params, const QDomElement& parent, XmlErrorHandler* eh)
{
  QDomNode n = parent.firstChild();

  while(!n.isNull())
  {
    QDomElement e = n.toElement(); // try to convert the node to an element.

    if (!e.isNull())
    {
      if ("Parameter" == e.tagName())
      {
        Parameter* p = NULL;

        if (e.hasAttribute("type") && e.hasAttribute("name"))
        {
          QString typeString = e.attribute("type");
          QString name = e.attribute("name");
          switch (fromString(typeString))
          {
          case Integer:
            {
              IntegerParameter* ip = new IntegerParameter();
              p = ip;
              if (!ip->deserialize(e, eh))
                return false;
            }
            break;
          case Bool:
            {
              BoolParameter* bp = new BoolParameter();
              p = bp;
              if (!bp->deserialize(e, eh))
                return false;
            }
            break;
          case String:
            p = new Parameter();
            break;
          case Enum:
            {
              EnumParameter* ep = new EnumParameter();
              p = ep;
              if (!ep->deserialize(e, eh))
                return false;
            }
            break;
          }

          if (p)
          {
            p->setName(name);
            p->setType(fromString(typeString));

            if (e.hasAttribute("validExpression"))
            {
              QString expr = e.attribute("validExpression");
              p->setValidExpression(expr);
            }

            if (e.hasAttribute("required"))
            {
              QVariant rv = e.attribute("required");
              if (rv.canConvert(QVariant::Bool))
                p->setRequired(rv.toBool());
              else
              {
                eh->reportProblem(XmlErrorHandler::Error, e, "Invalid Parameter attribute value for 'required'");
                return false;
              }
            }

            QDomNode n = e.firstChild();
            while(!n.isNull())
            {
              QDomElement subElement = n.toElement(); // try to convert the node to an element.

              if (!subElement.isNull())
              {
                if ("Prompt" == subElement.tagName())
                  p->setPrompt(subElement.text());
                else if ("Description" == subElement.tagName())
                  p->setDescription(subElement.text());
                else if ("DefaultValue" == subElement.tagName())
                {
                  p->setDefaultValue(subElement.text());
                  p->setValue(subElement.text());
                }
              }
              n = n.nextSibling();
            }
            // Add it to the collection now
            params->addParam(p);
          }
        }
        else
        {
          eh->reportProblem(XmlErrorHandler::Error, e, "Parameter missing type and/or name attribute");
          return false;
        }
      }
    }
    n = n.nextSibling();
  }

  return true;
}

// Integer

QWidget* IntegerParameter::createEditor(QTreeWidget* tree, QWidget* parent, const QString& currValue)
{
  QSpinBox* sbox = new QSpinBox(parent);
  setTree(tree);

  QVariant v = getValue();
  qDebug() << "value" << v << "currValue" << currValue;

  if (mFrom != -1)
    sbox->setMinimum(mFrom);
  
  if (mTo != -1)
    sbox->setMaximum(mTo);
  
  sbox->setValue(v.toInt());

  return sbox;
}
void IntegerParameter::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index)
{
  QSpinBox* spinBox = qobject_cast<QSpinBox*>(editor);
  if (spinBox)
  {
    QVariant v = spinBox->value();
    model->setData(index, v.toString());
    setValue(v.toString());
  }
}

Parameter* IntegerParameter::copyParameter()
{
  IntegerParameter* p = new IntegerParameter();
  
  p->setFrom(getFrom());
  p->setTo(getTo());

  return copyParameterData(p);
}

bool IntegerParameter::hasValueSet()
{
  if (getValue().isValid() && getValue().canConvert(QVariant::Int))
    return true;
  else
    return false;
}

bool IntegerParameter::deserialize(const QDomElement& e, XmlErrorHandler*)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement subElement = n.toElement(); // try to convert the node to an element.

    if (!subElement.isNull())
    {
      if ("From" == subElement.tagName())
        setFrom(subElement.text().toInt());
      else if ("To" == subElement.tagName())
        setTo(subElement.text().toInt());
    }
    n = n.nextSibling();
  }
  return true;
}

// Enum
void EnumParameter::currentIndexChanged(int)
{
  QAbstractItemDelegate* d = getTree()->itemDelegate();
  ParametersTreeDelegate* pd = dynamic_cast<ParametersTreeDelegate*>(d);
  if (pd)
    pd->commit(qobject_cast<QWidget*>(sender()));
}

// Integer
Parameter* EnumParameter::copyParameter()
{
  EnumParameter* p = new EnumParameter();
  foreach (QString str, mChoices)
  {
    p->mChoices.append(str);
  }
  return copyParameterData(p);
}

QWidget* EnumParameter::createEditor(QTreeWidget* tree, QWidget* parent, const QString& currValue)
{
  QComboBox* cbox = new QComboBox(parent);
  setTree(tree);

  foreach (QString choice, mChoices)
    cbox->addItem(choice);

  cbox->setCurrentIndex(cbox->findText(currValue));

  if (!connect(cbox, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexChanged(int))))
    qDebug() << "Error: Unable to hookup slot: currentIndexChanged";

  return cbox;
}
void EnumParameter::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index)
{
  QComboBox* comboBox = qobject_cast<QComboBox*>(editor);
  if (comboBox)
  {
    model->setData(index, comboBox->currentText());
    setValue(comboBox->currentText());
  }
}

bool EnumParameter::deserialize(const QDomElement& e, XmlErrorHandler*)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement subElement = n.toElement(); // try to convert the node to an element.

    if (!subElement.isNull())
    {
      if ("Choices" == subElement.tagName())
      {
        QDomNode choicesChild = n.firstChild();
        while (!choicesChild.isNull())
        {
          QDomElement choiceElement = choicesChild.toElement();

          if ("Choice" == choiceElement.tagName())
            mChoices.append(choiceElement.text());
          choicesChild = choicesChild.nextSibling();
        }
      }
    }
    n = n.nextSibling();
  }
  return true;
}

// Bool

Parameter* BoolParameter::copyParameter()
{
  BoolParameter* p = new BoolParameter();
  return copyParameterData(p);
}

bool BoolParameter::hasValueSet()
{
  if (getValue().isValid() && getValue().canConvert(QVariant::Bool))
    return true;
  else
    return false;
}

bool BoolParameter::deserialize(const QDomElement& parent, XmlErrorHandler*)
{
  if (parent.hasAttribute("default"))
  {
    QVariant value = parent.attribute("default");
    setDefaultValue(value);
    setValue(value);
  }
  return true;
}

void BoolParameter::currentIndexChanged(int)
{
  QAbstractItemDelegate* d = getTree()->itemDelegate();
  ParametersTreeDelegate* pd = dynamic_cast<ParametersTreeDelegate*>(d);
  if (pd)
    pd->commit(qobject_cast<QWidget*>(sender()));
}

QWidget* BoolParameter::createEditor(QTreeWidget* tree, QWidget* parent, const QString& currValue)
{
  QComboBox* cbox = new QComboBox(parent);
  setTree(tree);

  cbox->addItem("true");
  cbox->addItem("false");

  cbox->setCurrentIndex(cbox->findText(currValue));

  if (!connect(cbox, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexChanged(int))))
    qDebug() << "Error: Unable to hookup slot: currentIndexChanged";

  return cbox;
}
void BoolParameter::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index)
{
  QComboBox* comboBox = qobject_cast<QComboBox*>(editor);
  if (comboBox)
  {
    model->setData(index, comboBox->currentText());

    QVariant cv = comboBox->currentText();
    setValue(cv.toBool());

    qDebug() << "thisBool: " << this << " value: " << comboBox->currentText();
  }
}
