#ifndef DLGPARAMETERS_H
#define DLGPARAMETERS_H
#include "util/CarbonVersion.h"
#include "gui/CQt.h"


#include <QDialog>
#include "ui_DlgParameters.h"

class Template;

class DlgParameters : public QDialog
{
  Q_OBJECT

public:
  DlgParameters(QWidget *parent = 0, Template* templ = 0);
  ~DlgParameters();

private:
  void populateDialog();

private slots:
  void on_buttonBox_accepted();
  void on_buttonBox_rejected();

private:
  Template* mTemplate;
  Ui::DlgParametersClass ui;
};

#endif // DLGPARAMETERS_H
