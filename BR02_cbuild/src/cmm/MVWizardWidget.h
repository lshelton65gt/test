#ifndef MVWIZARDWIDGET_H
#define MVWIZARDWIDGET_H

#include "util/CarbonPlatform.h"

#include <QWidget>
#include "util/UtString.h"
#include "Mdi.h"
#include "MdiProject.h"
#include "CarbonConsole.h"
#include "RemoteConsole.h"
#include "ui_MVWizardWidget.h"

class CarbonMakerContext;
class MVComponent;
class CQtContext;

class MVChangeOutputDirectory
{
public:
  MVChangeOutputDirectory(MVComponent* MV);
  ~MVChangeOutputDirectory();

private:
  UtString mCurrDir;
};

class MVWizardWidget : public QWidget, public MDIWidget
{
  Q_OBJECT

public:
  MVWizardWidget(CarbonMakerContext* ctx=0, MDIDocumentTemplate* doct=0, QWidget *parent = 0);
  ~MVWizardWidget();
  bool loadFile(const QString& qFilename);
  virtual const char* userFriendlyName();
  const char* userFriendlyCurrentFile();
  virtual void saveDocument();
  void updateMenusAndToolbars();

private: // methods
  void initialize();
  void closeEvent(QCloseEvent *event);
  bool maybeSave();
  void setCurrentFile(const char* fileName);
  const char* strippedName(const char* fullFileName);
  void populate();
  bool isPrimary(const CarbonDB* db, const CarbonDBNode* node);

private:
  CQtContext *mCQt;
  CarbonMakerContext* mCtx;
  UtString mCurFile;
  bool isUntitled;
  UtString mFriendlyName;

private slots:
  void compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void fileSave();
  void outputFilenameChanged(const CarbonProperty*, const char*);
  void projectLoaded(const char* fileName, CarbonProject*);
  void dataChanged();
  void on_pushButtonExpandAll_clicked();
  void on_pushButtonCollapseAll_clicked();
  void makefileCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType);

private:
  Ui::MVWizardWidgetClass ui;

};

#endif
