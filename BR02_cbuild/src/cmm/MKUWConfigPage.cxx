//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "util/UtEncryptDecrypt.h"
#include "ModelKitUserWizard.h"
#include "cmm.h"
#include "ModelKit.h"
#include "CarbonProjectWidget.h"
#include "CarbonMakerContext.h"
#include "CarbonDatabaseContext.h"
#include "CarbonOptions.h"


MKUWConfigPage::MKUWConfigPage(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);
  setTitle("Configuration Options");

  QRegExp rx("[A-Za-z_][A-Za-z0-9_]*");
  QRegExpValidator* validator = new QRegExpValidator(rx, this);
  ui.lineEditComponentName->setValidator(validator);
}

MKUWConfigPage::~MKUWConfigPage()
{
}

#define SETTING_MODEL_TARGET "mkuwPageConfig/ModelTarget"
#define SETTING_FULL_SYMBOLS "mkuwPageConfig/FullSymbols"
#define SETTING_COMP_NAME "mkuwPageConfig/ComponentName"

void MKUWConfigPage::saveSettings()
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());

  wiz->recordConfiguration("ModelTarget", ui.radioButtonUnix->isChecked() ? "Unix" : "Windows");
  wiz->recordConfiguration("Symbols", ui.radioButtonFull->isChecked() ? "FullSymbols" : "IOSymbols");
  wiz->recordConfiguration("ComponentName", ui.lineEditComponentName->text());

  mSettings.setValue(SETTING_MODEL_TARGET, ui.radioButtonUnix->isChecked());
  mSettings.setValue(SETTING_FULL_SYMBOLS, ui.radioButtonFull->isChecked());  

  wiz->setComponentName(ui.lineEditComponentName->text());
  wiz->setModelTarget(ui.radioButtonUnix->isChecked() ? ModelKitUserWizard::Unix : ModelKitUserWizard::Windows);
  wiz->setModelSymbols(ui.radioButtonFull->isChecked() ? ModelKitUserWizard::FullSymbols : ModelKitUserWizard::IOSymbols);
}

void MKUWConfigPage::restoreSettings()
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  QString componentName = wiz->getComponentName();

  ui.lineEditComponentName->setText(componentName);

  if (mSettings.value(SETTING_MODEL_TARGET, true).toBool())
    ui.radioButtonUnix->setChecked(true);
  else
    ui.radioButtonWindows->setChecked(true);

  if (mSettings.value(SETTING_FULL_SYMBOLS, true).toBool())
    ui.radioButtonFull->setChecked(true);
  else
    ui.radioButtonIO->setChecked(true);

  ModelKitAnswers* answers = wiz->getAnswers();
  if (answers)
  {
    switch (answers->getModelTarget())
    {
    case ModelKitAnswers::Unix:
      ui.radioButtonUnix->setChecked(true);
      break;
    case ModelKitAnswers::Windows:
      ui.radioButtonWindows->setChecked(true);
      break;
    }

    switch (answers->getModelSymbols())
    {
    case ModelKitAnswers::FullSymbols:
      ui.radioButtonFull->setChecked(true);
      break;
    case ModelKitAnswers::IOSymbols:
      ui.radioButtonIO->setChecked(true);
      break;
    }

    if (!answers->getComponentName().isEmpty())
      ui.lineEditComponentName->setText(answers->getComponentName());
  }
}

bool MKUWConfigPage::validatePage()
{
  saveSettings();

  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  KitUserContext* userContext = wiz->getUserContext();
  KitManifest* manifest = wiz->getManifest();

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  CarbonOptions* options = proj->getActive()->getOptions("VSPCompiler");

  // Figure out the cbuild library name. We used to just get this from
  // the manifest. But if the model kit turns on a new model kit
  // feature ComponentCbuildLibraryName, then use the name provided by the
  // user.
  QString componentCbuildLibraryName = "ComponentCbuildLibraryName";
  QString libraryName;
  if (manifest->findModelKitFeature(componentCbuildLibraryName)) {
    // We currently don't handle non-C identifiers too well, so
    // legislate this away by converting the name. We warn the user if
    // this happens.
    QString compName = wiz->getComponentName();
    QString validCompName;
    bool converted = false;
    for (int i = 0; i < compName.length(); ++i) {
      QChar ch = compName[i];
      if (((ch >= 'a') && (ch <= 'z')) || ((ch >= 'A') && (ch <= 'Z')) || ((ch >= '0') && (ch <= '9'))) {
        validCompName += ch;
      } else {
        validCompName += '_';
        converted = true;
      }
    }
    qDebug() << "Wizard componentName = " << validCompName << " used to name cbuild library.";
    libraryName = "lib" + validCompName;

    // If we converted the name, warn the user
    if (converted) {
      qDebug() << "Warning: component name converted to make it a legal C identifier to '"
               << validCompName
               << "'";
      wiz->setComponentName(validCompName);
    }

  } else {
    libraryName = manifest->getLibraryName();
  }
  QFileInfo fi(libraryName);

  switch (wiz->getModelTarget())
  {
  case ModelKitUserWizard::Unix:
    libraryName = fi.baseName() + ".a";
    break;
  case ModelKitUserWizard::Windows:
    libraryName = fi.baseName() + ".lib";
    break;
  }

  // Push the values into the context
  userContext->setModelTarget(ContextEnums::ModelTarget(wiz->getModelTarget()));
  userContext->setModelSymbols(ContextEnums::ModelSymbols(wiz->getModelSymbols()));
  userContext->setComponentName(wiz->getComponentName());
  userContext->setLibraryName(libraryName);
  manifest->setLibraryName(libraryName);

  qDebug() << "Target Library Name" << libraryName;

  UtString uLibraryName; uLibraryName << libraryName;
  options->putValue("-o", uLibraryName.c_str());
  pw->updateVHMName();

  // update compile.ef and explore.ef
  updateProtectedFiles();

  return true;
}

void MKUWConfigPage::updateProtectedFiles()
{
  qDebug() << "Updating protected files";

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  KitUserContext* userContext = wiz->getUserContext();
  ModelKit* kit = wiz->getModelKit();
  KitManifest* manifest = wiz->getManifest();

  QString compileFile = QString("%1/compile.ef").arg(proj->getActive()->getOutputDirectory());
  QString exploreFile = QString("%1/explore.ef").arg(proj->getActive()->getOutputDirectory());

  UtString uCompileFile; uCompileFile << compileFile;
  UtString uExploreFile; uExploreFile << exploreFile;
  UtEncryptDecrypt enc;
  bool status;
  UtString err;
  UtString efContents = enc.decryptFile(uCompileFile.c_str(), &err, &status);
  UtString exploreContents = enc.decryptFile(uExploreFile.c_str(), &err, &status);

  QString newEfContents = efContents.c_str();
  QString newExploreContents = exploreContents.c_str();

  newEfContents += QString("-o %1\n").arg(userContext->getLibraryName());
  newExploreContents += QString("-o %1\n").arg(userContext->getLibraryName());

  if (theApp->checkArgument("-attributeFile"))
  {
    QString attFile = theApp->argumentValue("-attributeFile");
    newEfContents += QString("-attributeFile %1\n").arg(attFile);
    newExploreContents += QString("-attributeFile %1\n").arg(attFile);
  }

  if (userContext->getModelSymbols() == ContextEnums::IOSymbols)
  {
    newEfContents += QString("-noFullDB\n");
    newExploreContents += QString("-noFullDB\n");  
  }

  // Key String?
  if (!kit->getLicenseKey().isEmpty())
  {
    qDebug() << "License Feature" << kit->getLicenseKey();
    newEfContents += QString("-keyString %1\n").arg(kit->getLicenseKey());
  }

  qDebug() << "adding compiler switches" << manifest->getCompilerSwitches().count();

  foreach(QString sw, manifest->getCompilerSwitches())
  {
    qDebug() << "inserting" << sw;
    newEfContents += QString("%1\n").arg(sw);
  }

  QFile fileCompileFile(compileFile);
  if (fileCompileFile.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    UtString content; content << newEfContents;
    UtString encryptedBuffer = enc.encryptBuffer(content.c_str());
    QTextStream out(&fileCompileFile);
    out << encryptedBuffer.c_str();
    fileCompileFile.close();
  }

  QFile fileExploreFile(exploreFile);
  if (fileExploreFile.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    UtString content; content << newExploreContents;
    UtString encryptedBuffer = enc.encryptBuffer(content.c_str());
    QTextStream out(&fileExploreFile);
    out << encryptedBuffer.c_str();
    fileExploreFile.close();
  }

  qDebug() << "compilationFile" << newEfContents;
  qDebug() << "exploreFile" << newExploreContents;

  setCommitPage(true);
}


void MKUWConfigPage::initializePage()
{
  restoreSettings();

  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  ModelKit* kit = wiz->getModelKit();

  QString title = QString("Model Kit %1 Revision: %2, Version: %3")
                  .arg(kit->getName())
                  .arg(kit->getRevision())
                  .arg(kit->getLatestVersion()->getVendorVersion());

  setTitle(title);
  setSubTitle(kit->getDescription());

  ModelKitAnswers* answers = wiz->getAnswers();

  // Preface script?
  if (!kit->getPrefaceScriptNormalized().isEmpty())
  {
    if (!kit->runKitScript(wiz, wiz->getUserContext(), ModelKit::Preface))
    {
      if (!answers)
      {
        QString msg = wiz->getUserContext()->getErrorMessages().join("\n");
        QMessageBox::critical(this, "Carbon Model Kit", msg);
      }
      else
        qDebug() << "Preface Script Error" << wiz->getUserContext()->getErrorMessages();


      wiz->reject();
    }

    if (answers)
      wiz->close();
  }

  if (answers)
  {
    wiz->next();
  }
}

int MKUWConfigPage::nextId() const
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  KitManifest* manifest = wiz->getManifest();

  if (manifest->getAskRoot())
    return ModelKitUserWizard::PageStart;

  if (manifest->getCopyFiles())
    return ModelKitUserWizard::PageClone;

  if (manifest->getPreUserActions().count() > 0)
    return ModelKitUserWizard::PageUserActions;

  return ModelKitUserWizard::PageFinished;
}


