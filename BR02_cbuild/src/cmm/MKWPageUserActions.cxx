//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "ModelKitWizard.h"
#include "CarbonMakerContext.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"
#include "CarbonComponent.h"
#include "KitManifest.h"
#include "WizDlgInputFile.h"
#include "WizDlgExecuteScript.h"
#include "WizDlgRunScript.h"
#include "DlgConfirmDelete.h"

MKWPageUserActions::MKWPageUserActions(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);

  setTitle("User Actions");
  setSubTitle("These actions will be executed on the end-users computer in the order specified.");

  QStringList actionNames;
  
  ui.comboBoxActions->addItem("Input File", (int)KitUserAction::eActionInputFile);
  ui.comboBoxActions->addItem("Shell Script", (int)KitUserAction::eActionExecuteScript);
  ui.comboBoxActions->addItem("Modelstudio Script", (int)KitUserAction::eActionRunScript);

  QStringList headers;
  headers << "Action" << "Variable" << "Description";

  mActiveTree = ui.treeWidgetPre;

  initializeTree(ui.treeWidgetPre, headers);
  initializeTree(ui.treeWidgetPost, headers);

  mWhen = PreAction;
}

void MKWPageUserActions::initializeTree(QTreeWidget* tree, const QStringList& headers)
{
  tree->setHeaderLabels(headers);
  tree->setColumnCount(headers.count());

  tree->header()->resizeSections(QHeaderView::ResizeToContents);
  
  for (int i=0; i<headers.count()-1; i++)
    tree->header()->setResizeMode(i, QHeaderView::Interactive);
  
  tree->header()->setResizeMode(headers.count()-1, QHeaderView::Stretch);
}



MKWPageUserActions::~MKWPageUserActions()
{
}

bool MKWPageUserActions::validatePage()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());

  wiz->removeUserActions();

  for (int i=0; i<ui.treeWidgetPre->topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = ui.treeWidgetPre->topLevelItem(i);
    QVariant qv = item->data(0, Qt::UserRole);
    KitUserAction* action = (KitUserAction*)qv.value<void*>(); 
    wiz->addPreUserAction(action);
  }


  for (int i=0; i<ui.treeWidgetPost->topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = ui.treeWidgetPost->topLevelItem(i);
    QVariant qv = item->data(0, Qt::UserRole);
    KitUserAction* action = (KitUserAction*)qv.value<void*>(); 
    wiz->addPostUserAction(action);
  }

  return true;
}

void MKWPageUserActions::initializePage()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());

  ui.treeWidgetPre->clear();
  ui.treeWidgetPost->clear();

  foreach (KitUserAction* action, wiz->getPreUserActions())
  {
    qDebug() << "restoring" << action->getName();
    QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidgetPre);
    showItemData(item, action);
  }

  foreach (KitUserAction* action, wiz->getPostUserActions())
  {
    qDebug() << "restoring" << action->getName();
    QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidgetPost);
    showItemData(item, action);
  }
  
  ui.treeWidgetPre->header()->resizeSections(QHeaderView::ResizeToContents);
  ui.treeWidgetPost->header()->resizeSections(QHeaderView::ResizeToContents);

  updateButtons();

  if (wiz->getRegenerate())
    wiz->next();
}

void MKWPageUserActions::showItemData(QTreeWidgetItem* item, KitUserAction* action)
{
  item->setText(0, action->getName());
  item->setText(1, action->getVariableName());
  item->setText(2, action->getDescription());

  QVariant qv = qVariantFromValue((void*)action);
  item->setData(0, Qt::UserRole, qv);

  activeTreeTab()->header()->resizeSections(QHeaderView::ResizeToContents);
}

void MKWPageUserActions::insertItem(KitUserAction* action)
{
  QTreeWidgetItem* item = new QTreeWidgetItem(activeTreeTab());
  showItemData(item, action);
}

void MKWPageUserActions::updateButtons()
{
  bool itemSelected = activeTreeTab()->selectedItems().count() > 0;

  ui.pushButtonDeleteAction->setEnabled(itemSelected);
  ui.pushButtonEdit->setEnabled(itemSelected);

  ui.pushButtonMoveUp->setEnabled(false);
  ui.pushButtonMoveDown->setEnabled(false);

  // We only allow single selection
  foreach (QTreeWidgetItem* item, activeTreeTab()->selectedItems())
  {
    int childIndex = activeTreeTab()->indexOfTopLevelItem(item);
    int nChildren = activeTreeTab()->topLevelItemCount();
    ui.pushButtonMoveUp->setEnabled(childIndex > 0);
    ui.pushButtonMoveDown->setEnabled(childIndex < nChildren-1);
    break;
  }
}

void MKWPageUserActions::on_pushButtonAddAction_clicked()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());
  KitUserAction::UserActionKind kind = static_cast<KitUserAction::UserActionKind>(ui.comboBoxActions->itemData(ui.comboBoxActions->currentIndex()).toInt());

  qDebug() << "add user action" << kind;

  switch (kind)
  {
  case KitUserAction::eActionInputFile:
    {
      WizDlgInputFile dlg(this, wiz);
      if (dlg.exec() == QDialog::Accepted)
      {
        KitUserAction* action = dlg.createAction();
        insertItem(action);
        if (mWhen == PreAction)
          wiz->addPreUserAction(action);
        else
          wiz->addPostUserAction(action);
      }
    }
    break;

  case KitUserAction::eActionExecuteScript:
    {
      WizDlgExecuteScript dlg(this, wiz);
      if (dlg.exec() == QDialog::Accepted)
      {
        KitUserAction* action = dlg.createAction();
        insertItem(action);
        if (mWhen == PreAction)
          wiz->addPreUserAction(action);
        else
          wiz->addPostUserAction(action);
      }
    }
    break;
    case KitUserAction::eActionRunScript:
    {
      WizDlgRunScript dlg(this, wiz);
      if (dlg.exec() == QDialog::Accepted)
      {
        KitUserAction* action = dlg.createAction();
        insertItem(action);
        if (mWhen == PreAction)
          wiz->addPreUserAction(action);
        else
          wiz->addPostUserAction(action);
      }
    }
    break;

  }
}


void MKWPageUserActions::on_treeWidgetPre_itemSelectionChanged()
{
  updateButtons();
}

void MKWPageUserActions::on_treeWidgetPost_itemSelectionChanged()
{
  updateButtons();
}

void MKWPageUserActions::on_pushButtonEdit_clicked()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());

  foreach (QTreeWidgetItem* item, activeTreeTab()->selectedItems())
  {
    QVariant qv = item->data(0, Qt::UserRole);
    KitUserAction* action = static_cast<KitUserAction*>(qv.value<void*>()); 
    KitUserAction::UserActionKind kind = action->getKind();
    
    qDebug() << "editing" << kind;

    switch (kind)
    {
    case KitUserAction::eActionInputFile:
      {
        KitActionInputFile* a = dynamic_cast<KitActionInputFile*>(action);
        WizDlgInputFile dlg(this, wiz, a);
        if (dlg.exec() == QDialog::Accepted)
          showItemData(item, action);
      }
      break;

    case KitUserAction::eActionExecuteScript:
      {
        KitActionExecuteScript* a = dynamic_cast<KitActionExecuteScript*>(action);
        WizDlgExecuteScript dlg(this, wiz, a);
        if (dlg.exec() == QDialog::Accepted)
          showItemData(item, action);
      }
      break;
    case KitUserAction::eActionRunScript:
      {
        KitActionRunScript* a = dynamic_cast<KitActionRunScript*>(action);
        WizDlgRunScript dlg(this, wiz,a);
        if (dlg.exec() == QDialog::Accepted)
          showItemData(item, action);
      }
      break;
    default:
      qDebug() << "Error Unknown kind" << kind;
      break;
    }
    break;
  }
}

void MKWPageUserActions::on_pushButtonDeleteAction_clicked()
{
  QString msg = "Are you sure you want to delete this item?";
  if (QMessageBox::question(this, MODELWIZARD_TITLE, msg, 
      QMessageBox::No|QMessageBox::Yes, QMessageBox::No) == QMessageBox::No)
    return;

  foreach (QTreeWidgetItem* item, activeTreeTab()->selectedItems())
  {
    activeTreeTab()->takeTopLevelItem(activeTreeTab()->indexOfTopLevelItem(item));
    break;
  }
  updateButtons();
}

void MKWPageUserActions::on_pushButtonMoveUp_clicked()
{
// We only allow single selection
  foreach (QTreeWidgetItem* item, activeTreeTab()->selectedItems())
  {
    int childIndex = activeTreeTab()->indexOfTopLevelItem(item);
    QTreeWidgetItem* movedItem = activeTreeTab()->takeTopLevelItem(childIndex);
    activeTreeTab()->insertTopLevelItem(childIndex-1, movedItem);
    activeTreeTab()->setCurrentItem(movedItem);
    break;
  }
  updateButtons();
}

void MKWPageUserActions::on_pushButtonMoveDown_clicked()
{
 // We only allow single selection
  foreach (QTreeWidgetItem* item, activeTreeTab()->selectedItems())
  {
    //bool expand = item->isExpanded();
    int childIndex = activeTreeTab()->indexOfTopLevelItem(item);
    QTreeWidgetItem* movedItem = activeTreeTab()->takeTopLevelItem(childIndex);
    activeTreeTab()->insertTopLevelItem(childIndex+1, movedItem);
    activeTreeTab()->setCurrentItem(movedItem);
    break;
  }
  updateButtons();
}

void MKWPageUserActions::on_treeWidgetPre_itemDoubleClicked(QTreeWidgetItem*,int)
{
  on_pushButtonEdit_clicked();
}

void MKWPageUserActions::on_treeWidgetPost_itemDoubleClicked(QTreeWidgetItem*,int)
{
  on_pushButtonEdit_clicked();
}

void MKWPageUserActions::on_tabWidget_currentChanged(int tabIndex)
{
  if (tabIndex == 0)
  {
    mActiveTree = ui.treeWidgetPre;
    mWhen = PreAction;
  }
  else
  {
    mActiveTree = ui.treeWidgetPost;
    mWhen = PostAction;
  }

  updateButtons();
}
