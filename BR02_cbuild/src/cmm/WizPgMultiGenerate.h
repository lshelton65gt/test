#ifndef WIZPGMULTIGENERATE_H
#define WIZPGMULTIGENERATE_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "WizardPage.h"
#include "MemoryWizard.h"
#include <QWizardPage>
#include "ui_WizPgMultiGenerate.h"
#include "CarbonConsole.h"

class WizPgMultiGenerate : public WizardPage
{
  Q_OBJECT

public:
  WizPgMultiGenerate(QWidget *parent = 0);
  ~WizPgMultiGenerate();

private:
  void updateButtons();
  void saveSettings();
  void restoreSettings();
  bool validateOptions();
  void addCompilerSwitch();

protected:
  virtual bool validatePage();
  virtual void initializePage();
  virtual bool isComplete() const;
  virtual int nextId() const { return MemoryWizard::PageMultiFinished; }

private:
  Ui::WizPgMultiGenerateClass ui;
  QSettings mSettings;
  bool mIsValid;
  bool mConnectedOutput;

private slots:
    void on_pushButtonBrowseDirname_clicked();
    void on_pushButtonBrowseFilename_clicked();
    void on_radioButtonMultiple_toggled(bool);
    void on_radioButtonSingle_toggled(bool);
    void on_lineEditSingleFilename_textChanged(const QString&);
    void outputChanged(CarbonConsole::CommandType ctype, const char* text, const char* workingDir);

};

#endif // WIZPGMULTIGENERATE_H
