//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QtGui>

#include "RegEditorTreeNodes.h"


void MemoryMapsItem::singleClicked(int)
{
  qDebug() << "memoryMaps";
  mProjectWidget->openSource("Memory Maps", -1, "MemoryMaps");
}

void MemoryMapsItem::doubleClicked(int v)
{
  singleClicked(v);
}

MemoryBankItem::MemoryBankItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, SpiritBank* bank) : CarbonProjectTreeNode(proj, parent)
{
  mBank = bank;
  setIcon(0, QIcon(":/cmm/Resources/bank.png"));

  QString name(bank->getName());
  if (name.isEmpty())
    name = "Bank";

  setText(0, name);

  foreach (SpiritAddressBlock* ab, bank->getAddressBlocks())
  {
    new AddressBlockItem(proj, this, ab);
  }

  foreach (SpiritBank* sb, bank->getBanks())
  {
    new MemoryBankItem(proj, this, sb);
  }
}

AddressBlockItem::AddressBlockItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, SpiritAddressBlock* ab) : CarbonProjectTreeNode(proj, parent)
{
  mAddressBlock = ab;
  setIcon(0, QIcon(":/cmm/Resources/addressBlock.png"));
  QString name = ab->getName();
  if (name.isEmpty())
    name = ab->getNameAttribute();

  setText(0, name);

  for (UInt32 i=0; i<ab->numRegisters(); i++)
  {
    SpiritRegister* reg = ab->getRegister(i);
    new RegisterItem(proj, this, reg);
  }
}

RegisterItem::RegisterItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, SpiritRegister* r) : CarbonProjectTreeNode(proj, parent)
{
  mRegister = r;
  setIcon(0, QIcon(":/cmm/Resources/register.png"));
  setText(0, r->getName());

  for (UInt32 i=0; i<r->numFields(); i++)
  {
    SpiritField* field = r->getField(i);
    new RegisterFieldItem(proj, this, r, field);
  }
}

RegisterFieldItem::RegisterFieldItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, SpiritRegister* r, SpiritField* f) : CarbonProjectTreeNode(proj, parent)
{
  mRegister = r;
  mField = f;
  setIcon(0, QIcon(":/cmm/Resources/field.png"));
  setText(0, f->getName());
}


void AddressBlockItem::singleClicked(int)
{
  UtString regPath;
  regPath << "regPath:" << mAddressBlock->getPath();

  mProjectWidget->openSource(regPath.c_str(), -1, "Registers");
}

void AddressBlockItem::doubleClicked(int v)
{
  singleClicked(v);
}

