//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/



/*#include "util/CarbonPlatform.h"

#include "XMLDesignHierarchy.h"
#include "cmm.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"

#include "gui/CQt.h"
#include <QFile>

QString line2;

XMLDesignHierarchy::XMLDesignHierarchy(QWidget *parent)
  : CDragDropTreeWidget(false, parent)
{
  ui.setupUi(this);
  clearAll();
  
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();

  mDesignHierarchy = NULL;
  mToplevelInterface = NULL;
  mInstance = NULL;

  mProjectWidget = pw;

  CarbonProject* proj = pw->project();
  if (proj)
  {
    const char* designHierarchyFile = proj->getDesignFilePath(".designHierarchy");
    mDesignHierarchy = new XDesignHierarchy(designHierarchyFile);
    XmlErrorHandler* eh = mDesignHierarchy->getErrorHandler();
    if (eh->hasErrors())
    {
      QString msg = QString("Error reading design hierarchy: %1\n%2")
        .arg(designHierarchyFile)
        .arg(eh->errorMessages().join("\n"));
    }
  }
  CQT_CONNECT(this, itemSelectionChanged(), this, itemSelectionChanged());
  CQT_CONNECT(this, itemDoubleClicked(QTreeWidgetItem*, int), this, itemDoubleClicked(QTreeWidgetItem*, int));
}

XMLDesignHierarchy::~XMLDesignHierarchy()
{

}

void XMLDesignHierarchy::itemDoubleClicked(QTreeWidgetItem* item, int /*col*//*)
{
  QVariant qv = item->data(0, Qt::UserRole);
  if (!qv.isNull())
  {
    CarbonDBNode* node = (CarbonDBNode*)qv.value<void*>();
    INFO_ASSERT(node, "Expecting carbondbnode");

    const char* nodeName = carbonDBNodeGetFullName(mDB, node);

    UtString module;
    UtString moduleLocator;

    ModuleLocator* locator = mModuleMap[nodeName];
    if (locator)
    {
      moduleLocator << locator->getLocator();
      module << locator->getModuleName();
    }

    if (module.length() > 0)
      emit nodeDoubleClicked(nodeName, module.c_str(), moduleLocator.c_str());
    else
      emit nodeDoubleClicked(nodeName, NULL, NULL);
  }
}

void XMLDesignHierarchy::selectModule(QTreeWidgetItem* parent, const char* moduleName, QStringList& moduleList)
{
  for (int i=0; i<parent->childCount(); i++)
  {
    QTreeWidgetItem* item = parent->child(i);
    QVariant qv = item->data(0, Qt::UserRole);
    if (!qv.isNull())
    {
      CarbonDBNode* node = (CarbonDBNode*)qv.value<void*>();
      UtString text;
      text << carbonDBNodeGetLeafName(mDB, node);

      if (0 == strcmp(moduleName, text.c_str()))
      {
        if (!moduleList.isEmpty())
        {
          UtString next;
          next << moduleList.takeFirst();
          selectModule(item, next.c_str(), moduleList);
        }
        else
        {
          item->setSelected(true);
          scrollToItem(item, QAbstractItemView::PositionAtCenter);
        }
      }
    }
  }
}

void XMLDesignHierarchy::expandModules(QStringList& moduleList)
{
  clearSelection();

  QString topModule = moduleList.takeFirst();
  UtString top;
  top << topModule;

  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);
    QVariant qv = item->data(0, Qt::UserRole);
    if (!qv.isNull())
    {
      CarbonDBNode* node = (CarbonDBNode*)qv.value<void*>();
      UtString text;
      text << carbonDBNodeGetLeafName(mDB, node);
      if (0 == strcmp(top.c_str(), text.c_str()))
      {
        if (moduleList.count() > 0)
        {
          UtString next;
          next << moduleList.takeFirst();
          selectModule(item, next.c_str(), moduleList);
          break;
        }
        else
        {
          item->setSelected(true);
        }
      }
    }
  }
}


void XMLDesignHierarchy::selectModule(const char* moduleName)
{
  clearSelection();
  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);
    QVariant qv = item->data(0, Qt::UserRole);
    if (!qv.isNull())
    {
      CarbonDBNode* node = (CarbonDBNode*)qv.value<void*>();
      UtString text;
      text << carbonDBNodeGetFullName(mDB, node);
      if (text == moduleName)
        item->setSelected(true);
    }
  }
}

void XMLDesignHierarchy::putContext(ApplicationContext* ctx)
{
  mCtx = ctx;
}

void XMLDesignHierarchy::Initialize(HierarchyMode mode)
{
  clearAll();

  QApplication::setOverrideCursor(Qt::WaitCursor);

  XTopLevelInterfaces* topInterfaces = mDesignHierarchy->getToplevelInterfaces();   
  
  for (int i=0; i<topInterfaces->numInterfaces(); i++)
  {
    XToplevelInterface* topi = topInterfaces->getInterface(i);
    QTreeWidgetItem* item = new QTreeWidgetItem/*(ui.treeWidget)*//*;
    item->setText(0, topi->getName());
    item->setExpanded(true);
    QVariant qv = qVariantFromValue((void*)topi);
    item->setData(0, Qt::UserRole, qv);
    foreach (XInstance* inst, topi->getInstances())
    {
      addInstance(item, topi, inst);
    }
  }

  header()->setResizeMode(0, QHeaderView::Interactive);
  header()->setStretchLastSection(true);

  header()->resizeSections(QHeaderView::ResizeToContents);

  QApplication::restoreOverrideCursor();
}

void XMLDesignHierarchy::addInstance(QTreeWidgetItem* parent, XToplevelInterface* topi, XInstance* inst)
{
  QVariant qv = qVariantFromValue((void*)topi);
  QVariant qv1 = qVariantFromValue((void*)inst);

  QTreeWidgetItem* item = new QTreeWidgetItem(parent);
  item->setText(0, inst->getID());
  item->setData(0, Qt::UserRole, qv);
  item->setData(1, Qt::UserRole, qv1);
  item->setExpanded(true);

  foreach (XInstance* childInst, inst->getInstances())
    addInstance(item, topi, childInst);
}

void XMLDesignHierarchy::clearAll()
{
  clear();
}

QTreeWidgetItem* XMLDesignHierarchy::addModule(const CarbonDBNode* node,
                                              QTreeWidgetItem* parentItem,
                                              ItemCount* totals)
{
  QTreeWidgetItem* item = NULL;
  if (parentItem == NULL) 
  {
    item = new QTreeWidgetItem(this);
  }
  else 
  {
    item = new QTreeWidgetItem(parentItem);
  }

  item->setText(0, carbonDBNodeGetLeafName(mDB, node));

  if (mModuleMap.count() != 0)
  {
    const char* fullName = carbonDBNodeGetFullName(mDB, node);
    ModuleLocator* locator = mModuleMap[fullName];
    if (locator)
    {
      UtString name;
      name << carbonDBNodeGetLeafName(mDB, node) << " (" << locator->getModuleName() << ")";
       item->setText(0, name.c_str());
    }
  }
 // Store the reference to the node in the tree node
  QVariant qv = qVariantFromValue((void*)node);
  item->setData(0, Qt::UserRole, qv);

  // Annotate the module item in the tree widget with the number
  // of visible and filtered nets and modules
  ItemCount myCount;

  CarbonDBNodeIter* iter = carbonDBLoopChildren(mDB, node);
  while ((node = carbonDBNodeIterNext(iter)) != NULL) {
    const char* leafName = carbonDBNodeGetLeafName(mDB, node);
    if (*leafName != '$') {
      // exclude generated symbols
      if (carbonDBIsBranch(mDB, node)) {
#if 1
        if (carbonDBIsArray(mDB, node) || carbonDBIsStruct(mDB, node))
          continue;
#endif
        // Annotate the module item in the tree widget with the number
        // of visible and filtered nets and modules
        ItemCount instanceCounts;
        addModule(node, item, &instanceCounts);
        instanceCounts.mInstances = 0;
        myCount += instanceCounts;
        ++myCount.mInstances;
      }
      else
        ++myCount.mNets;

      //else if (qualifyNet(node, &myCount.mFilteredNets)) {
      //  ++myCount.mNets;
      //}
    }
  }

  UtString buf;
  buf << myCount.mNets << " nets, " << myCount.mInstances << " instances";
  if (myCount.mFilteredNets != 0) {
    buf << ", " << myCount.mFilteredNets << " filtered nets";
  }    
 
  item->setText(1, buf.c_str());
  *totals += myCount;

  carbonDBFreeNodeIter(iter);

  return item;
} // QTreeWidgetItem* CarbonHBrowser::addModule

void XMLDesignHierarchy::buildSelectionList(QList<QTreeWidgetItem*>& list)
{
  // The list gives items for every column and row, we just want it row
  // oriented.

  list.clear();

  foreach (QTreeWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(0, Qt::UserRole);
    if (!qv.isNull())
    {
      CarbonDBNode* node = (CarbonDBNode*)qv.value<void*>();
      if (node != NULL)
      {
        list.append(item);
      }
    }
  }  
}

void XMLDesignHierarchy::itemSelectionChanged()
{
  QList<QTreeWidgetItem*> itemList;
  buildSelectionList(itemList);
  mDragText.clear();

  // Now, disconnect each item
  // Now walk the list of items to be deleted
  foreach (QTreeWidgetItem *item, itemList)
  {
    QVariant qv = item->data(0, Qt::UserRole);
    INFO_ASSERT(!qv.isNull(), "Null BrowserTreeWidget item");

    CarbonDBNode* node = (CarbonDBNode*)qv.value<void*>();
    if (!mDragText.empty())
      mDragText << "\n";

    mDragText << carbonDBNodeGetFullName(mDB, node);

    if (!signalsBlocked())
      emit nodeSelectionChanged(node);
  }
}


// this is only called by ModelStudio
void XMLDesignHierarchy::parseHierarchyFile(const char* hierFilePath)
{
  qDebug() << "parsing hierarchy file: " << hierFilePath;

  foreach(ModuleLocator* loc, mModuleMap)
    delete loc;

  mModuleMap.clear();

  QFile file(hierFilePath);
  if (file.open(QFile::ReadOnly | QFile::Text))
  {
    QTextStream in(&file);
    while (!in.atEnd()) 
    {
      QString headerLine = in.readLine();
      QStringList headerItems = headerLine.simplified().split(" ");
      if (headerItems.count() == 3 && 
        headerItems.at(0) == "Module" &&
        headerItems.at(1) == "Instance" && 
        headerItems.at(2) == "Location")
      {
        in.readLine(); // skip separator
        mHierLine = in.readLine();
        QStringList lineItems = mHierLine.simplified().split(" ");
        QString module = lineItems.at(0);
        mHierLine = in.readLine();
        parseHierarchy(module, 2, in);
      }
    }
    file.close();
  }
}


int countLeadingSpaces2(QString line2)
{
  int n=0;
  for (n=0; line2[n] == ' ' && n<line2.length(); n++)
    ;
  return n;
}

void XMLDesignHierarchy::parseHierarchy(QString parentPath, int spacesThisLevel, QTextStream& inStream)
{
  QString instance;

  while (!mHierLine.isNull())
  {
    QStringList lineItems = mHierLine.simplified().split(" ");
    QString module = lineItems.at(0);
    if (module == "<protected>")
      return;
    QString locator = lineItems.at(2);
  
    int numSpaces = countLeadingSpaces2(mHierLine);
      
    if (numSpaces == spacesThisLevel)
    {
      instance = lineItems.at(1);
      mModuleMap[parentPath + "." + instance] = new ModuleLocator(module, locator);
      mHierLine = inStream.readLine();
    }
    else if (numSpaces > spacesThisLevel)
      parseHierarchy(parentPath + "." + instance, 2+spacesThisLevel, inStream);
    else if (numSpaces < spacesThisLevel)
      return;
  }
}
*/
