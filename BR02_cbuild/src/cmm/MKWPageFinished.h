#ifndef MKWPAGEFINISHED_H
#define MKWPAGEFINISHED_H


#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>
#include "WizardPage.h"
#include "DesignXML.h"
#include <QWizardPage>
#include "ui_MKWPageFinished.h"

class ModelKit;
class KitManifest;

class MKWPageFinished : public WizardPage
{
  Q_OBJECT


public:
  MKWPageFinished(QWidget *parent = 0);
  ~MKWPageFinished();

protected:
  virtual void initializePage();
  virtual bool validatePage();

  void addStatus(const QString& msg);
  void addManifest(ModelKit* kit, KitManifest* manifest);
  void processCompilerSwitches(const QDomElement& parent, ModelKit* kit, KitManifest* manifest);
  void processEmbedded(const QDomElement& parent, ModelKit* kit, KitManifest* manifest);
  void processPreCompilationScripts(const QDomElement& parent, ModelKit* kit, KitManifest* manifest);
  void processPostCompilationScripts(const QDomElement& parent, ModelKit* kit, KitManifest* manifest);
  void processModelKitFeatures(const QDomElement& parent, ModelKit* kit, KitManifest* manifest);

private:
  QSettings mSettings;
  bool mTerminatePage;

private:
  Ui::MKWPageFinishedClass ui;
};

#endif // MKWPAGEFINISHED_H
