#ifndef WIZPGCONFIGURATIONS_H
#define WIZPGCONFIGURATIONS_H
#include "util/CarbonVersion.h"
#include "gui/CQt.h"


#include <QWizardPage>
#include "WizardPage.h"
#include "GfxModeSymbol.h"
#include "ui_WizPgConfigurations.h"

class GfxBlock;

class WizPgConfigurations : public WizardPage
{
  Q_OBJECT

public:
  WizPgConfigurations(QWidget *parent = 0);
  ~WizPgConfigurations();
  int nextId() const;

protected:
  virtual void initializePage();
  virtual bool serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*);

private slots:
  void blockSelected(GfxBlock*);

private:
  Ui::WizPgConfigurationsClass ui;
  GfxModeSymbol mSymbol;
  QGraphicsScene mScene;
};

#endif // WIZPGCONFIGURATIONS_H
