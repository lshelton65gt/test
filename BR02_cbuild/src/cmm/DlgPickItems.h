#ifndef DLGPICKITEMS_H
#define DLGPICKITEMS_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QDialog>
#include "ui_DlgPickItems.h"

class DlgPickItems : public QDialog
{
  Q_OBJECT

public:
  DlgPickItems(QWidget *parent = 0);
  void addItems(const QStringList& items);
  QString getValues() const { return mValues.join(" "); }

  ~DlgPickItems();

private:
  Ui::DlgPickItemsClass ui;

  QStringList mValues;

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
};

#endif // DLGPICKITEMS_H
