//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QDebug>

#include "gui/CQt.h"
#include "ListWidget.h"
#include "util/UtString.h"

ListWidget::ListWidget(QWidget* parent) : QListWidget(parent)
{
  setItemDelegate(new ListDelegate(parent, this));
}

void ListWidget::deleteItem()
{
  int row = currentRow();
  QListWidgetItem* item = takeItem(row);
  if (item)
    delete item;
  setCurrentRow(row);
}

void ListWidget::moveDown()
{
  foreach (QListWidgetItem* item, selectedItems())
  {
    int currentRow = row(item);
    QListWidgetItem* tookItem = takeItem(currentRow);
    insertItem(currentRow+1, tookItem);
    clearSelection();
    setCurrentRow(row(tookItem));
    break;
  }
}

void ListWidget::moveUp()
{
  foreach (QListWidgetItem* item, selectedItems())
  {
    int currentRow = row(item);
    QListWidgetItem* tookItem = takeItem(currentRow);
    insertItem(currentRow-1, tookItem);
    clearSelection();
    setCurrentRow(row(tookItem));
    break;
  }
}

void ListWidget::addNewItem()
{
  int currRow = currentRow();
  clearSelection();
  QListWidgetItem* newItem = new QListWidgetItem("");
  newItem->setFlags(newItem->flags() | Qt::ItemIsEditable);
  insertItem(currRow, newItem);
  newItem->setSelected(true);
  setCurrentRow(row(newItem));
  editItem(newItem);
}

void ListWidget::addNewItem(const char* value)
{
  int currRow = currentRow();
  clearSelection();
  QListWidgetItem* newItem = new QListWidgetItem(value);
  newItem->setFlags(newItem->flags() | Qt::ItemIsEditable);
  insertItem(currRow, newItem);
  newItem->setSelected(true);
  setCurrentRow(row(newItem));
}

// Editor Delegate
ListDelegate::ListDelegate(QObject *parent, ListWidget* t)
: QItemDelegate(parent), list(t)
{
}

QWidget *ListDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                    const QModelIndex & /*index*/) const
{
  QLineEdit* edit = new QLineEdit(parent);
  return edit;
}

void ListDelegate::commitAndCloseEditor()
{
  QLineEdit *editor = qobject_cast<QLineEdit *>(sender());
  if (editor)
  {
    emit commitData(editor);
    emit closeEditor(editor);
  }
}

void ListDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit)
  {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }
}

void ListDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                        const QModelIndex &index) const
{
  QListWidgetItem* item = list->item(index.row());
  QLineEdit* lineEdit = qobject_cast<QLineEdit *>(editor);

  UtString text;
  text << lineEdit->text();
  if (text.length() > 0)
  {
    model->setData(index, lineEdit->text());
    list->setCurrentRow(list->row(item));
  }
}

