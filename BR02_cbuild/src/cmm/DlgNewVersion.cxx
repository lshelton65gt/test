//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include "KitManifest.h"

#include "DlgNewVersion.h"

DlgNewVersion::DlgNewVersion(QWidget *parent, ModelKit* kit)
: QDialog(parent)
{
  mModelKit = kit;
  ui.setupUi(this);

  ui.lineEditID->setText(QString("%1").arg(1+kit->getVersions()->numVersions()));

  // Populate Manifests
  KitManifests* manifests = kit->getManifests();
  bool hasItems = false;
  for (UInt32 i=0; i<manifests->numManifests(); i++)
  {
    KitManifest* mf = manifests->getManifest(i);
    ui.comboBoxManifests->addItem(mf->getName());
    hasItems = true;
  }
  ui.radioButtonCopyManifest->setEnabled(hasItems);
  ui.lineEditVendorVersion->setFocus();
  
  ui.lineEditManifestName->setText(QString("Manifest %1").arg(ui.lineEditID->text()));

  QPushButton* okButton = ui.buttonBox->button(QDialogButtonBox::Ok);
  okButton->setEnabled(false);

  on_radioButtonCopyManifest_toggled(false);
}

DlgNewVersion::ManifestType DlgNewVersion::getManifestType()
{
  if (ui.radioButtonNew->isChecked())
    return NewManifest;
  else
    return CopyManifest;
}

DlgNewVersion::~DlgNewVersion()
{
}


bool DlgNewVersion::isValid()
{
  if (ui.lineEditDescription->text().trimmed().isEmpty())
  {
    QMessageBox::critical(this, "Error", "Description cannot be empty");
    ui.lineEditDescription->setFocus();
    return false;
  }

  QString vendorVersion = ui.lineEditVendorVersion->text().trimmed();

  if (vendorVersion.isEmpty())
  {
    QMessageBox::critical(this, "Error", "Vendor Version cannot be empty");
    ui.lineEditVendorVersion->setFocus();
    return false;
  }

  // already exists?
  KitVersions* versions = mModelKit->getVersions();
  for (UInt32 i=0; i<versions->numVersions(); i++)
  {
    KitVersion* kv = versions->getVersion(i);
    if (kv->getVendorVersion() == vendorVersion)
    {
      QMessageBox::critical(this, "Error", "Vendor Version names must be unique");
      ui.lineEditVendorVersion->setFocus();
      return false;
    }
  }

  if (!ui.radioButtonCopyManifest->isChecked())
  {
    QString manifestName = ui.lineEditManifestName->text().trimmed();
    // Validate manifest name is valid
    if (manifestName.isEmpty())
    {
      QMessageBox::critical(this, "Error", "Manifest name cannot be empty");
      ui.lineEditManifestName->setFocus();
      return false;
    }

    if (mModelKit->getManifests()->findManifest(manifestName))
    {
      QMessageBox::critical(this, "Error", "Manifest names must be unique");
      ui.lineEditManifestName->setFocus();
      return false;
    }
  }

  return true;
}

void DlgNewVersion::on_buttonBox_rejected()
{
  reject();
}

void DlgNewVersion::on_buttonBox_accepted()
{
  if (isValid())
    accept();
}

void DlgNewVersion::on_radioButtonCopyManifest_toggled(bool checked)
{
  ui.comboBoxManifests->setEnabled(checked);
  ui.lineEditManifestName->setEnabled(!checked);
}

quint32 DlgNewVersion::getVersionID()
{
  return ui.lineEditID->text().toUInt();
}

QString DlgNewVersion::getDescription()
{
  return ui.lineEditDescription->text();
}

QString DlgNewVersion::getVendorVersion()
{
  return ui.lineEditVendorVersion->text();
}

QString DlgNewVersion::getManifestName()
{
  if (ui.radioButtonCopyManifest->isChecked())
    return ui.comboBoxManifests->currentText();
  else
    return ui.lineEditManifestName->text();
}

void DlgNewVersion::on_lineEditVendorVersion_textChanged(QString txt)
{
  QPushButton* okButton = ui.buttonBox->button(QDialogButtonBox::Ok);
  okButton->setEnabled(!txt.trimmed().isEmpty());
}
