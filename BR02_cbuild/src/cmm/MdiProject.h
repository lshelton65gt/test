#ifndef __MDIPROJECT_H__
#define __MDIPROJECT_H__

#include "util/CarbonPlatform.h"
#include <QObject>
#include "gui/CQt.h"

#include <QMenuBar>
#include <QProgressBar>
#include <QComboBox>
#include <QSignalMapper>
#include <QMap>
#include <QList>

#include "util/UtString.h"
#include "Mdi.h"
#include "MdiProject.h"
#include "CarbonMakerContext.h"
#include "RemoteConsole.h"
#include "CarbonConsole.h"

class CarbonProperty;

class InvisibleProject : public QWidget, public MDIWidget
{
  Q_OBJECT

public:
  InvisibleProject(CarbonMakerContext* ctx, MDIDocumentTemplate* doct=0);
  virtual ~InvisibleProject();

  virtual void saveDocument() {}
  const char* userFriendlyName();

  void updateConfigurationsComboBox(QComboBox* cbox);
  void closeEvent(QCloseEvent *e);

private:
  CarbonMakerContext* mContext;
  QComboBox* mConfigComboBox;

public slots:
  void closeProject();
  void saveProject();
  void projectProperties();
  void projectCompile();
  void projectReCompile();
  void projectBatchBuild();
  void projectPackage();
  void projectBuildPrecompilationHierarchy();
  void projectClean();
  void projectCancelCompile();
  void projectCheck();
  void projectConfigurationManager();
  void projectCopyConfiguration();
  void projectRunScript();

  void projectWindow();
  void propWindow();
  void hierWindow();
  void toggleDockWindow();

  void actionAddSources();
  void actionAddLibrary();
  void actionCreateCoware();
  void actionCreateMaxsim();
  void actionCreateSystemC();
  void actionCreateMV();
  void actionSimulate();
  void actionImportCmd();
  void actionImportCcfg();
  void actionCreateModelWizard();
  void actionPackageOptions();

  void configurationRemoved(const char*);
  void configurationRenamed(const char*, const char*);
  void configurationCopy(const char*, const char*);
  void configurationAdded(const char* name);
  void configurationChanged(const char* name);

signals:
  void enableCoWare(bool);
  void enableSystemC(bool);
  void enableMaxsim(bool);
  void enableMV(bool);
  void enableAddLibrary(bool);
  void enableAddSources(bool);
  void enableSimulate(bool);

private slots:
  void currentConfigurationIndexChanged(int);
  void simulationScriptChanged(const CarbonProperty*, const char*);
  void simulationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void simulationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType);
};

class MDIProjectTemplate : public MDIDocumentTemplate
{
  Q_OBJECT

public:
  MDIProjectTemplate(CarbonMakerContext* ctx);
 
  virtual MDIWidget* createNewDocument(QWidget* parent=0);
  virtual int createMenus();
  virtual int createToolbars();
  virtual void updateMenusAndToolbars(QWidget* widget);
  virtual MDIWidget* openDocument(QWidget* parent, const char* docName);

  void registerDockWidget(QDockWidget* widget, const char* name, const char* shortCut=NULL);
  void compilationModeChanged(bool inProgress);
  void updateProjectMenus();
  InvisibleProject* getInvisibleProject() { return mInvisibleProject; }
  void putInvisibleProject(InvisibleProject* newVal) { mInvisibleProject=newVal; }

private slots:
  void dockVisibilityChanged(bool vis);
  void hierDockVisibilityChanged(bool vis);

private:
  QAction* mActionCloseProject;
  QAction* mActionSaveProject;
  QAction* mActionProjProperties;
  QAction* mActionProjConfig;
  QAction* mActionProjCopyConfig;
  QAction* mActionCompile;
  QAction* mActionPackage;
  QAction* mActionReCompile;
  QAction* mActionBatchBuild;
  QAction *mActionBuildPrecompilationHierarchy;
  QAction* mActionClean;
  QAction* mActionHierWindow;
  QAction* mActionCheck;
  QAction* mActionCancelCompile;
  QAction* mActionRunScript;

  QAction* mActionCreateCoware;
  QAction* mActionCreateMaxsim;
  QAction* mActionCreateSystemC;
  QAction* mActionPackageOptions;
  QAction* mActionAddSources;
  QAction* mActionAddLib;
  QAction* mActionCreateMV;
  QAction* mActionSimulate;
  QAction* mActionImportCmd;
  QAction* mActionImportCcfg;

  QAction* mActionModelWizard;

  QMenu* mViewMenu;

  QComboBox* mConfigurationsComboBox;
  CarbonMakerContext* mContext;
  bool mConnectedHierWindow;

  QMap<QWidget*, QAction*> mDockWidgetActionMap;
  QList<QAction*> mDockActions;
  static InvisibleProject* mInvisibleProject;

};
#endif
