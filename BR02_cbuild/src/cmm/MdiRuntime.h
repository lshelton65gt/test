// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __MdiRuntime_h__
#define __MdiRuntime_h__

#include "util/CarbonPlatform.h"
#include "Mdi.h"

class CarbonMakerContext;
class CarbonRuntimeState;
struct CarbonReplaySystem;

class MDIRuntimeTemplate : public MDIDocumentTemplate
{
  Q_OBJECT
public:
  MDIRuntimeTemplate(CarbonMakerContext *ctx);
 
  virtual MDIWidget* createNewDocument(QWidget* parent=0);
  virtual int createMenus();
  virtual int createToolbars();
  virtual void updateMenusAndToolbars(QWidget* widget);
  virtual MDIWidget* openDocument(QWidget* parent, const char* docName);
  QToolBar* getSimulationToolbar() { return mSimulationToolbar; }

public slots:
  void carbonSystemUpdated(CarbonRuntimeState &state, CarbonReplaySystem &guiSystem, CarbonReplaySystem &simSystem);

private:
  void updateReplayToolbar(CarbonRuntimeState &state, CarbonReplaySystem &guiSystem);
  void updateOnDemandToolbar(CarbonRuntimeState &state, CarbonReplaySystem &guiSystem);
  void updateSimulationToolbar(CarbonRuntimeState &state);
 
  CarbonMakerContext *mContext;

public:
  QAction *actionFileExit;
  QAction *actionHelpAbout;
  QAction *actionHelpDocumentation;
  QAction *actionHelpDocumentation2;
  QAction *actionViewCarbonModels;
  QAction *actionViewProperties;
  QAction *actionViewSimulationHistory;
  QAction *actionViewToolbarsReplay;
  QAction *actionViewToolbarsOnDemand;
  QAction *actionReplayRecord;
  QAction *actionReplayPlay;
  QAction *actionReplayNormal;
  QAction *actionOnDemandStart;
  QAction *actionOnDemandEnableTrace;
#define USE_ACTION_FOR_CONTINUE_SIM 1
#if USE_ACTION_FOR_CONTINUE_SIM
  QAction *actionContinueSimulation;
#endif
  QAction *actionReplayVerbose;
  QAction *actionOnDemandStop;
  QAction *actionOnDemandViewTrace;
  QAction *actionOnDemandTune;

  QWidget* mContinueSimButton;
  QColor mSaveSimContForeground;
  QColor mSaveSimContBackground;

  QToolBar* mSimulationToolbar;
}; // class MDIRuntimeTemplate : public MDIDocumentTemplate

#endif
