#ifndef __CARBONPROPERTY_H__
#define __CARBONPROPERTY_H__
#include "util/CarbonPlatform.h"

#include <QtGui>
#include "util/XmlParsing.h"

class PropertyEditor;
class UtXmlErrorHandler;
class CarbonDelegate;
class PropertyEditorDelegate;
class CarbonPropertyValue;
class CarbonOptions;

class CarbonProperty : public QObject
{
  Q_OBJECT

public:
  CARBONMEM_OVERRIDES

    enum SwitchKind {Unknown=0, Enum, Bool, String, Integer, InFile, OutFile, Stringlist, Filelist, DateStamp};
    enum ResetTo { eResetNone, eResetDefault, eResetGUIDefault };

  //! ctor
  CarbonProperty(const char* name, int nv, const char* descr, SwitchKind kind) : QObject(0),
    mDescription(descr),
    mName(name),
    mNumValues(nv),
    mHasDefaultValue(false),
    mPropertyEditor(NULL),
    mReadOnly(false),
    mDisplayOnly(false),
    mKind(kind)
  {
    mHasGuiDefaultValue = false;
    mHasTrueValue = false;
    mHasFalseValue = false;
    mRequired = false;
    mResetTo = eResetDefault;
  }

  CarbonProperty(const char* name, const char* descr, SwitchKind kind) : QObject(0),
    mDescription(descr),
    mName(name),
    mNumValues(0),
    mHasDefaultValue(false),
    mPropertyEditor(NULL),
    mReadOnly(false),
    mDisplayOnly(false),
    mKind(kind)
  {
    mHasGuiDefaultValue = false;
    mHasTrueValue = false;
    mHasFalseValue = false;
    mRequired = false;
    mResetTo = eResetDefault;
  }

  //! dtor
  virtual ~CarbonProperty() 
  {
    qDebug() << "Free property: " << mName.c_str(); 
  }

  virtual bool parseXML(xmlNodePtr parent, UtXmlErrorHandler* eh)=0;
  virtual bool writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* eh)=0;
  virtual bool readValueXML(xmlNodePtr parent, CarbonOptions* options, UtXmlErrorHandler* eh) const=0;

  virtual CarbonDelegate* createDelegate(QObject* parent, PropertyEditorDelegate* d, PropertyEditor* propEditor)=0;

  virtual const char* getName() const { return mName.c_str(); }
  virtual void putName(const char* newVal) { mName = newVal; }

  UInt32 getNumValues() const { return mNumValues; }
  void putNumValues(UInt32 newVal) { mNumValues=newVal; }

  bool isMultiValued() const {
    return (mKind == Stringlist || mKind == Filelist);
  }
  // Cannot be modified
  void putReadOnly(bool newVal) { mReadOnly=newVal; }
  bool getReadOnly() const { return mReadOnly; }

  void putResetTo(ResetTo newVal) { mResetTo=newVal; }
  ResetTo getResetTo() const { return mResetTo; }

  void putRequired(bool newVal) { mRequired=newVal; }
  bool getRequired() const { return mRequired; }

  // Not serialized
  void putDisplayOnly(bool newVal) { mDisplayOnly=newVal; }
  bool getDisplayOnly() const { return mDisplayOnly; }

  const char* getDescription() const { return mDescription.c_str(); }
  void putDescription(const char* newVal) { mDescription = newVal; }

  const char* getLicenseEnabled() const { return mLicenseEnabled.c_str(); }
  void putLicenseEnabled(const char* newVal) { mLicenseEnabled = newVal; }

  bool hasDefaultValue() const { return mHasDefaultValue; }
  virtual const char* getDefaultValue() const {return mDefaultValue.c_str(); }
  void putDefaultValue(const char* newVal) 
  {
    mHasDefaultValue = true;
    mDefaultValue = newVal;
  }

  QString getLegalPattern() const { return mLegalPattern; }
  void putLegalPattern(const QString& newVal) { mLegalPattern=newVal; }
  bool hasLegalPattern() const { return !mLegalPattern.isEmpty(); }

  void setTrueValue(const QString& newVal) { mHasTrueValue=true; mTrueValue=newVal; }
  QString getTrueValue() const { return mTrueValue; }
  bool hasTrueValue() const { return mHasTrueValue; }

  void setFalseValue(const QString& newVal) { mHasFalseValue=true; mFalseValue=newVal; }
  QString getFalseValue() const { return mFalseValue; }
  bool hasFalseValue() const { return mHasFalseValue; }

  bool hasGuiDefaultValue() const { return mHasGuiDefaultValue; }
  virtual const char* getGuiDefaultValue() const {return mGuiDefaultValue.c_str(); }
  void putGuiDefaultValue(const char* newVal) 
  {
    mHasGuiDefaultValue = true;
    mGuiDefaultValue = newVal;
  }

  SwitchKind getKind() const {return mKind; }

  void putExclusiveWith(const char* newVal) { mExclusiveWith=newVal; }
  const char* getExclusiveWith() const { return mExclusiveWith.c_str(); }

protected:
  UtString mDescription;
  UtString mLicenseEnabled;
  UtString mName;
  UtString mDefaultValue;
  UtString mGuiDefaultValue;
  UInt32 mNumValues;
  UtString mExclusiveWith;
  bool mHasDefaultValue;
  PropertyEditor* mPropertyEditor;
  bool mReadOnly;
  bool mDisplayOnly;
  bool mHasGuiDefaultValue;
  QString mTrueValue;
  QString mFalseValue;
  bool mHasTrueValue;
  bool mHasFalseValue;
  bool mRequired;
  ResetTo mResetTo;
  QString mLegalPattern;

private:
  SwitchKind mKind;
};

Q_DECLARE_METATYPE(CarbonProperty*);

#endif
