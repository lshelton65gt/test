#ifndef __CARBONPROPINFILE_H__
#define __CARBONPROPINFILE_H__

#include "util/CarbonPlatform.h"
#include <QObject>
#include "CarbonProperty.h"
#include "PropertyEditor.h"

class CarbonPropertyInfile : public CarbonProperty
{
public:
  CARBONMEM_OVERRIDES

    CarbonPropertyInfile(const char* name, int nv, const char* descr) : CarbonProperty(name,nv,descr,CarbonProperty::InFile){}

  virtual CarbonDelegate* createDelegate(QObject* parent,  PropertyEditorDelegate* d, PropertyEditor* propEd);
  virtual bool parseXML(xmlNodePtr parent, UtXmlErrorHandler* eh);
  virtual bool writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* eh);
  virtual bool readValueXML(xmlNodePtr parent, CarbonOptions* options, UtXmlErrorHandler* eh) const;
  const char* getFilter() const { return mFilter.c_str(); }
  void putFilter(const char* newVal) {mFilter=newVal;}

private:
  UtString mFilter;
};


class CarbonDelegateInfile : public CarbonDelegate
{
  Q_OBJECT
public:
  CarbonDelegateInfile(QObject *parent = 0, PropertyEditorDelegate* d=0, PropertyEditor* e=0);
  QWidget* createEditor(QWidget *parent, QTreeWidgetItem* item, CarbonProperty* prop, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const;

  private slots:
    void showInfileDialog();
    void commitAndCloseEditor();

};
#endif
