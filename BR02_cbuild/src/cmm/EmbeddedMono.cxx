#include <stdlib.h>
#include <exception>

#include "util/CarbonVersion.h"
#include "EmbeddedMono.h"

using std::exception;

#if pfWINDOWS
#include <ObjBase.h>
#endif

#include <mono/jit/jit.h>
#include <mono/metadata/threads.h>
#include <mono/metadata/metadata.h>
#include <mono/metadata/environment.h>
#include <mono/metadata/class.h>
#include <mono/metadata/assembly.h>
#include <mono/metadata/mono-config.h>
#include <mono/metadata/mono-debug.h>

#include <mono/utils/mono-error.h>
#include <mono/utils/mono-logger.h>
#include <mono/metadata/debug-helpers.h>

#include "CarbonProjectWidget.h"
#include "CarbonProject.h"

EmbeddedMono* EmbeddedMono::mInstance = NULL;
MonoDomain* EmbeddedMono::mDomain = NULL;
cmm* EmbeddedMono::mMainWindow = NULL;
QString EmbeddedMono::mLastError = "";

struct DialogData
{
  int x;
  int y;
  const char* str;
};



PINVOKEABLE void TestMethod()
{
  qDebug() << "****** TestMethod()"; 
}

PINVOKEABLE void ConsolePrint(const char* message)
{
  qDebug() << "Console print was called from Assembly";
  QString msg = message;
  EmbeddedMono* em = EmbeddedMono::Instance();
  em->CMSConsolePrint(msg);
}

MonoString* EmbeddedMono::NewString(const char* str)
{
  return mono_string_new(GetDomain(), str);
}

const char* EmbeddedMono::ToString(MonoString* s)
{
  return mono_string_to_utf8(s);
}

void EmbeddedMono::CMSConsolePrint(const QString& str)
{
  UtString ustr; ustr << str;
  getConsole()->processOutput(CarbonConsole::Local, CarbonConsole::Check, ustr.c_str());
}

CarbonConsole* EmbeddedMono::getConsole()
{
  CarbonProjectWidget* pw = mMainWindow->getContext()->getCarbonProjectWidget();
  CarbonConsole* console = pw->getConsole();
  return console;
}

char* EmbeddedMono::AllocString(const QString& str)
{
#ifdef Q_OS_WIN32
  QByteArray ba = str.toAscii(); // this_toascii_ok
  int len = ba.count();
  char* comem = (char*)CoTaskMemAlloc(len+1);
  strcpy_s(comem, len+1, (const char*)ba);
  return(comem);  
#else
  return strdup((const char*)str.toAscii()); // this_toascii_ok
#endif
}

MonoMethod* EmbeddedMono::findMethod(MonoClass* klass, const char* methodName)
{
  void* iter = NULL;
  MonoMethod* method = NULL;
  while ((method = mono_class_get_methods (klass, &iter))) 
  {
    if (strcmp (mono_method_get_name(method), methodName) == 0)
    {
      return method;    
    }
  }
  return NULL;
}

MonoObject* EmbeddedMono::InvokeMethod(MonoAssembly* assembly, const char* nameSpace, const char* className, const char* methodName, int numParams, void** params, MonoObject* instance, MonoObject** exception)
{
  MonoImage* image = mono_assembly_get_image(assembly);
  MonoClass* klass = mono_class_from_name(image, nameSpace, className);
  MonoMethod* method = mono_class_get_method_from_name(klass, methodName, numParams);

  if (method)
  {
    *exception = NULL;
    MonoObject* result = mono_runtime_invoke(method, instance, params, exception);
    if (*exception)
    {         
      MonoString* strEx = mono_object_to_string(*exception, NULL);
      mLastError = mono_string_to_utf8(strEx);      
      ConsolePrint(mono_string_to_utf8(strEx)); 
      if (mMainWindow)
        CMSConsolePrint(mLastError);     
    }
    return result;
  }
  else
  {
    mLastError = QString("Unable to find or load %1.%2::%3").arg(nameSpace).arg(className).arg(methodName);
    if (mMainWindow)
      CMSConsolePrint(mLastError);  
  }

  return NULL;
}

MonoObject* EmbeddedMono::ConstructObject(MonoAssembly* assembly, const char* nameSpace, const char* className)
{
  MonoImage* image = mono_assembly_get_image(assembly);
  MonoClass* klass = mono_class_from_name(image, nameSpace, className);
  MonoObject* instance = mono_object_new(mDomain, klass);
  mono_runtime_object_init(instance);
  return instance;
}

MonoAssembly* EmbeddedMono::LoadAssembly(const char* assemblyName)
{
  QString progPath = assemblyName;

  QFileInfo fi(progPath);
  if (!fi.exists())
  {
    QString pluginsHome = QString("%1/etc/assemblies").arg(getenv("CARBON_HOME"));
    QString pluginPath = QString("%1/%2").arg(pluginsHome).arg(assemblyName);

    QFileInfo fi2(pluginPath);
    if (fi2.exists())
      progPath = pluginPath;
  }

  UtString ppath; ppath << progPath;
  const char* programPath = ppath.c_str();

  MonoAssembly* assembly = mono_domain_assembly_open(mDomain, programPath);

  if (assembly)
  {    
    UtString msg;
    msg << "Loading Assembly" << programPath;
    ConsolePrint(msg.c_str());

    MonoImage* monoImage = mono_assembly_get_image(assembly);
    if (monoImage == NULL)
    {
      mLastError = QString("Unable to obtain image from Assembly: %1").arg(programPath);
      CMSConsolePrint(mLastError);
      return NULL;
    }
    return assembly;
  }
  else
  {
    mLastError = QString("Unable to find or load Assembly: %1").arg(programPath);
    CMSConsolePrint(mLastError);
    return NULL;
  }
}

int EmbeddedMono::RunMonoProgram(const char* programPath, const char* nameSpace, const char* className, const char* methodName)
{
  int retval = -1;

  MonoAssembly* assembly = mono_domain_assembly_open(mDomain, programPath);
  if (assembly)
  {   
    MonoImage* monoImage = mono_assembly_get_image(assembly);
    if (monoImage == NULL)
    {
      QString msg = QString("Unable to obtain image from Assembly: %1").arg(programPath);
      CMSConsolePrint(msg);
      return -1;
    }

    MonoClass* monoClass = mono_class_from_name(monoImage, nameSpace, className);
    if (monoClass == NULL)
    {
      QString msg = QString("Unable Obtain Class %1::%2 from Assembly: %3")
        .arg(nameSpace)
        .arg(className)
        .arg(programPath);

      CMSConsolePrint(msg);
      return -2;
    }

    void* iter = NULL;
    MonoMethod* m = NULL;
    MonoMethod* method = NULL;

    while ((m = mono_class_get_methods (monoClass, &iter))) 
    {
      if (strcmp (mono_method_get_name (m), methodName) == 0)
      {
        method = m;	
        break;
      }
    }

    if (method)
    {
      MonoObject* exc = NULL;

      //QString methodCallback = QString("%1::DoSomething").arg(className);
      //if (nameSpace)
      //  methodCallback = QString("%1.%2::DoSomething").arg(nameSpace).arg(className);

      //mono_add_internal_call(methodCallback.ascii(), DoSomething);

      mono_runtime_invoke(method, NULL, NULL, &exc);
      if (exc)
      {         
        MonoString* strEx = mono_object_to_string(exc, NULL);
        mLastError = mono_string_to_utf8(strEx);
        CMSConsolePrint(mLastError);
      }
      else
      {    
        // Get return value from program
        retval = mono_environment_exitcode_get ();      
      }
    }
    else // Unable to find method.
    {
      QString msg = QString("Unable Obtain Method %1::%2::%3 from Assembly: %4")
        .arg(nameSpace)
        .arg(className)
        .arg(methodName)
        .arg(programPath);

      mLastError = msg;

      CMSConsolePrint(mLastError);

      return -3;

    }
  }

  return retval;
}

EmbeddedMono* EmbeddedMono::Instance()
{
  return Instance(mMainWindow);
}

static MonoBreakPolicy
  cms_should_insert_breakpoint (MonoMethod *)
{
  printf("Setting Break Policy");
  return MONO_BREAK_POLICY_ON_DBG;
}

static const char* debuggerOptions = "MONO_DEBUG=keep-delegates,reverse-pinvoke-exceptions";

EmbeddedMono* EmbeddedMono::Instance(cmm* mainWindow)
{
  static UtString debugArgs;

  mMainWindow = mainWindow;
  bool enableDebugger = theApp->checkArgument("-monoDebugger");

  static const char* options[] = 
  {
    "--soft-breakpoints",
    "--debugger-agent=transport=dt_socket,address=127.0.0.1:10000,embedding=1"
  };

  if (mInstance == NULL)
  {
    UtString debugOptions;
    if (theApp->checkArgument("-monoDebugger"))
    {
      QString debuggerArg = theApp->argumentValue("-monoDebugger");
      if (!debuggerArg.isEmpty())
      {
        QString debugAgent = QString("--debugger-agent=transport=dt_socket,address=%1,embedding=1").arg(debuggerArg);
        debugArgs << debugAgent;
        options[1] = debugArgs.c_str();
      }
    }

    if (enableDebugger)
      putenv((char*)debuggerOptions);

    mInstance = new EmbeddedMono();

    const char* CARBON_HOME = getenv("CARBON_HOME");

#if defined(Q_OS_WIN32)
    QString monoLib = QString("%1/Win/mono/lib").arg(CARBON_HOME);
    QString monoEtc = QString("%1/Win/mono/etc").arg(CARBON_HOME);
#else
    QString monoLib = QString("%1/Linux/mono/lib").arg(CARBON_HOME);
    QString monoEtc = QString("%1/Linux/mono/etc").arg(CARBON_HOME);
#endif

    if (enableDebugger)
    {
      mono_set_signal_chaining (true);
      //      mono_jit_set_trace_options("all");
      mono_jit_parse_options(sizeof(options)/sizeof(char*), (char**)options);
    }


    //   mono_trace_set_mask_string ("info"); 
    // mono_trace_set_level_string ("asm"); 

    UtString mlib; mlib << monoLib;
    UtString elib; elib << monoEtc;

    mono_set_dirs(mlib.c_str(), elib.c_str());

    mono_config_parse(NULL);


    if (enableDebugger)
    {
      mono_debug_init(MONO_DEBUG_FORMAT_MONO);
      mono_set_break_policy(cms_should_insert_breakpoint);
    }

    mInstance->mDomain = mono_jit_init_version ("ModelStudio", "v4.0.20506");
  }

  mono_thread_attach(mDomain); 

  return mInstance;
}

cmm* EmbeddedMono::GetMainWindow()
{
  return mMainWindow;
}

EmbeddedMono::~EmbeddedMono(void)
{
  if (mDomain)
    mono_jit_cleanup (mDomain);
}
