#ifndef __CARBONWORKSPACE_H__
#define __CARBONWORKSPACE_H__

#include "util/CarbonPlatform.h"

#include <QObject>
#include <QMainWindow>

class CQtWizardContext;
class CarbonMakerContext;

class CarbonWorkspace : public QObject
{
  Q_OBJECT

public:
  CarbonWorkspace();
  virtual ~CarbonWorkspace();

  virtual void initialize(CarbonMakerContext* ctx);
  CarbonMakerContext* getContext() const { return mContext; }

private:
  CarbonMakerContext* mContext;
};

#endif
