//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "MdiMV.h"
#include <QtGui>
#include <QMenuBar>
#include <QToolBar>
#include "gui/CQt.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "SettingsEditor.h"
#include "MVWizardWidget.h"

MDIMVTemplate::MDIMVTemplate(CarbonMakerContext* ctx)
: MDIDocumentTemplate("MV", "New ModelValidation Component", "ModelValidation (*.mv.xml)") 
{
  mContext = ctx;  
}

// If the user switches windows with the keyboard or menu pick
// update the tab bar to reflect that this is the active window now.
void MDIMVTemplate::updateSelectedTab(QWidget* widget)
{
  QVariant qv = widget->property("CarbonTabWidget");
  QVariant qvPlaceholder = widget->property("CarbonTabPlaceholder");

  if (qv.isValid() && qvPlaceholder.isValid())
  {
    QTabWidget* tw = (QTabWidget*)qv.value<void*>();
    QWidget* w = (QWidget*)qvPlaceholder.value<void*>();

    int tabIndex = tw->indexOf(w);
    if (tabIndex != -1)
      tw->setCurrentIndex(tabIndex);
  }
}


void MDIMVTemplate::updateMenusAndToolbars(QWidget*)
{
}

int MDIMVTemplate::createToolbars()
{
  return 0;
}

int MDIMVTemplate::createMenus()
{
  return 0;
}

MDIWidget* MDIMVTemplate::createNewDocument(QWidget* /*parent*/)
{
  return NULL;
}

// We don't have a MDI document presence, only the docking window widget
// so we create the InvisibleProject here to handle menu & toolbar events
//
MDIWidget* MDIMVTemplate::openDocument(QWidget* parent, const char* docName)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  MVWizardWidget* widget = new MVWizardWidget(mContext, this, parent);

  bool status = widget->loadFile(docName);

  QApplication::restoreOverrideCursor();

  if (status)
    return widget;
  else
  {
    widget->close();
    return NULL;
  }
}
