#ifndef __PORTEDITORCOMMANDS_H_
#define __PORTEDITORCOMMANDS_H_

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/CarbonCfg.h"

#include <QtGui>

#include "CompWizardPortEditor.h"
#include "CompWizardTreeNodes.h"
#include "ESLPort.h"
#include "ESLXtor.h"
#include "ESLResetGen.h"
#include "ESLClockGen.h"

class CmdRenameXtorInstance : public TreeUndoCommand
{
public:
  CmdRenameXtorInstance(ESLXtorInstanceItem* item, 
    CarbonCfgXtorInstance* xtorInst, const QString& oldName, 
    const QString& newName, QUndoCommand* parent = 0);
  
  void undo();
  void redo();

private:
  CarbonCfg* mCfg;
  QString mOldName;
  QString mNewName;
  UtString mXtorInstName;
};

class CmdChangeESLPortMode : public TreeUndoCommand
{
  public:
  CmdChangeESLPortMode(ESLPortItem* portItem, CarbonCfg* cfg, 
    CarbonCfgESLPort* eslPort, CarbonCfgESLPortMode oldMode, 
    CarbonCfgESLPortMode newMode, QUndoCommand* parent = 0);
  
  void undo();
  void redo();

private:
  CarbonCfg* mCfg;
  CarbonCfgESLPortMode mOldMode;
  CarbonCfgESLPortMode mNewMode;
  UtString mESLPortName;
};

class CmdChangeESLPortTypedef : public TreeUndoCommand
{
public:
  CmdChangeESLPortTypedef(ESLPortItem* portItem, CarbonCfg* cfg, 
    CarbonCfgESLPort* eslPort, const QString& oldValue, 
    const QString& newValue, QUndoCommand* parent = 0);
  
  void undo();
  void redo();

private:
  CarbonCfg* mCfg;
  UtString mOldValue;
  UtString mNewValue;
  UtString mESLPortName;
};



class CmdRenameESLPort : public TreeUndoCommand
{
public:
  CmdRenameESLPort(ESLPortItem* portItem, CarbonCfg* cfg, 
    CarbonCfgESLPort* eslPort, const QString& oldName, 
    const QString& newName, QUndoCommand* parent = 0);
  
  void undo();
  void redo();

private:
  CarbonCfg* mCfg;
  QString mOldName;
  QString mNewName;
  UtString mESLPortName;
};

class CmdChangeESLPortExpr : public TreeUndoCommand
{
public:
  CmdChangeESLPortExpr( ESLPortExprItem* portExprItem, CarbonCfg* cfg, 
                        CarbonCfgESLPort* eslPort, 
                        const QString& oldValue, 
                        const QString& newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfg* mCfg;
  QString mOldValue;
  QString mNewValue;
  QString mESLPortName;
};

class CmdChangeESLTieValue : public TreeUndoCommand
{
public:
  CmdChangeESLTieValue(ESLTieValueItem* portExprItem, CarbonCfg* cfg, 
    CarbonCfgTie* tiePort, const DynBitVector* oldValue, 
    const DynBitVector* newValue, QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgTie* findTie(CarbonCfgRTLPort* rtlPort);

private:
  UtString mRTLPortName;
  CarbonCfg* mCfg;
  DynBitVector mOldValue;
  DynBitVector mNewValue;
};

class CmdChangeESLResetGenActive : public TreeUndoCommand
{
public:
 
  CmdChangeESLResetGenActive(ESLResetGenActiveItem* item,
                        quint64 oldValue, 
                        quint64 newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgResetGen* getResetGen();

private:
  quint64 mOldValue;
  quint64 mNewValue;
  UtString mRTLPortName;
  CarbonCfg* mCfg;
};

class CmdChangeESLResetGenInactive : public TreeUndoCommand
{
public:
  CmdChangeESLResetGenInactive(ESLResetGenInactiveItem* item,
                        quint64 oldValue, 
                        quint64 newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgResetGen* getResetGen();

private:
  quint64 mOldValue;
  quint64 mNewValue;
  UtString mRTLPortName;
  CarbonCfg* mCfg;
};

class CmdChangeESLResetGenCyclesBefore : public TreeUndoCommand
{
public:
  CmdChangeESLResetGenCyclesBefore(ESLResetGenCyclesBeforeItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgResetGen* getResetGen();

private:
  quint32 mOldValue;
  quint32 mNewValue;
  UtString mRTLPortName;
  CarbonCfg* mCfg;
};


class CmdChangeESLResetGenCyclesAfter : public TreeUndoCommand
{
public:
 
  CmdChangeESLResetGenCyclesAfter(ESLResetGenCyclesAfterItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgResetGen* getResetGen();

private:
  quint32 mOldValue;
  quint32 mNewValue;
  UtString mRTLPortName;
  CarbonCfg* mCfg;
};

class CmdChangeESLResetGenCyclesAsserted : public TreeUndoCommand
{
public:
 
  CmdChangeESLResetGenCyclesAsserted(ESLResetGenCyclesAssertedItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgResetGen* getResetGen();

private:
  quint32 mOldValue;
  quint32 mNewValue;
  UtString mRTLPortName;
  CarbonCfg* mCfg;
};

class CmdChangeESLResetFrequencyReset : public TreeUndoCommand
{
public:
 
  CmdChangeESLResetFrequencyReset(ESLResetGenFrequencyResetItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgResetGen* getResetGen();

private:
  quint32 mOldValue;
  quint32 mNewValue;
  UtString mRTLPortName;
  CarbonCfg* mCfg;
};

class CmdChangeESLResetFrequencyCycles : public TreeUndoCommand
{
public:
 
  CmdChangeESLResetFrequencyCycles(ESLResetGenFrequencyCyclesItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgResetGen* getResetGen();

private:
  quint32 mOldValue;
  quint32 mNewValue;
  UtString mRTLPortName;
  CarbonCfg* mCfg;
};

class CmdChangeESLClockGenInitialValue : public TreeUndoCommand
{
public:
 
  CmdChangeESLClockGenInitialValue(ESLClockGenInitialValueItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgClockGen* getClockGen();

private:
  UtString mRTLPortName;
  quint32 mOldValue;
  quint32 mNewValue;
  CarbonCfg* mCfg;
};


class CmdChangeSystemCClockGenInitialValue : public TreeUndoCommand
{
public:
 
  CmdChangeSystemCClockGenInitialValue(SystemCClockGenInitialValueItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgSystemCClock* getClockGen();

private:
  UtString mRTLPortName;
  quint32 mOldValue;
  quint32 mNewValue;
  CarbonCfg* mCfg;
};


class CmdChangeESLClockGenDelay : public TreeUndoCommand
{
public:
 
  CmdChangeESLClockGenDelay(ESLClockGenDelayItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgClockGen* getClockGen();

private:
  UtString mRTLPortName;
  quint32 mOldValue;
  quint32 mNewValue;
  CarbonCfg* mCfg;
};

class CmdChangeESLClockGenDutyCycle : public TreeUndoCommand
{
public:
 
  CmdChangeESLClockGenDutyCycle(ESLClockGenDutyCycleItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgClockGen* getClockGen();

private:
  UtString mRTLPortName;
  quint32 mOldValue;
  quint32 mNewValue;
  CarbonCfg* mCfg;
};

class CmdChangeSystemCClockGenDutyCycle : public TreeUndoCommand
{
public:
 
  CmdChangeSystemCClockGenDutyCycle(SystemCClockGenDutyCycleItem* item,
                        double oldValue, 
                        double newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgSystemCClock* getClockGen();

private:
  UtString mRTLPortName;
  double mOldValue;
  double mNewValue;
  CarbonCfg* mCfg;
};

// Period
class CmdChangeSystemCClockGenPeriod : public TreeUndoCommand
{
public:
 
  CmdChangeSystemCClockGenPeriod(SystemCClockGenPeriodItem* item,
                        double oldValue, 
                        double newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgSystemCClock* getClockGen();

private:
  UtString mRTLPortName;
  double mOldValue;
  double mNewValue;
  CarbonCfg* mCfg;
};


class CmdChangeSystemCClockGenPeriodTimeUnits : public TreeUndoCommand
{
public:
 
  CmdChangeSystemCClockGenPeriodTimeUnits(SystemCClockGenPeriodUnitsItem* item,
                        CarbonCfgSystemCTimeUnits oldValue, 
                        CarbonCfgSystemCTimeUnits newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgSystemCClock* getClockGen();

private:
  UtString mRTLPortName;
  CarbonCfgSystemCTimeUnits mOldValue;
  CarbonCfgSystemCTimeUnits mNewValue;
  CarbonCfg* mCfg;
};


// StartTime
class CmdChangeSystemCClockGenStartTime : public TreeUndoCommand
{
public:
 
  CmdChangeSystemCClockGenStartTime(SystemCClockGenStartTimeItem* item,
                        double oldValue, 
                        double newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgSystemCClock* getClockGen();

private:
  UtString mRTLPortName;
  double mOldValue;
  double mNewValue;
  CarbonCfg* mCfg;
};

class CmdChangeSystemCClockGenStartTimeUnits : public TreeUndoCommand
{
public:
 
  CmdChangeSystemCClockGenStartTimeUnits(SystemCClockGenStartTimeUnitsItem* item,
                        CarbonCfgSystemCTimeUnits oldValue, 
                        CarbonCfgSystemCTimeUnits newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgSystemCClock* getClockGen();

private:
  UtString mRTLPortName;
  CarbonCfgSystemCTimeUnits mOldValue;
  CarbonCfgSystemCTimeUnits mNewValue;
  CarbonCfg* mCfg;
};




class CmdChangeESLClockGenFrequencyCompCycles : public TreeUndoCommand
{
public:
 
  CmdChangeESLClockGenFrequencyCompCycles(ESLClockGenFrequencyCompCycles* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgClockGen* getClockGen();

private:
  UtString mRTLPortName;
  quint32 mOldValue;
  quint32 mNewValue;
  CarbonCfg* mCfg;
};

class CmdChangeESLClockGenFrequencyClockCycles : public TreeUndoCommand
{
public:
 
  CmdChangeESLClockGenFrequencyClockCycles(ESLClockGenFrequencyClockCycles* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgClockGen* getClockGen();

private:
  UtString mRTLPortName;
  quint32 mOldValue;
  quint32 mNewValue;
  CarbonCfg* mCfg;
};


// Common
class CmdChangeParamValueBool : public TreeUndoCommand
{
public:
 
  CmdChangeParamValueBool(ParameterValueItem* item,
                        bool oldValue, 
                        bool newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgXtorParamInst* getParamInst();

protected:
  ParameterValueItem::ParamType mParamType;

private:
  CarbonCfg* mCfg;
  UtString mParamInstName;
  UtString mParamName;
  bool mOldValue;
  bool mNewValue;
};

class CmdChangeParamValueUInt32 : public TreeUndoCommand
{
public:
 
  CmdChangeParamValueUInt32(ParameterValueItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgXtorParamInst* getParamInst();

private:
  CarbonCfg* mCfg;
  UtString mParamInstName;
  UtString mParamName;
  ParameterValueItem::ParamType mParamType;
  quint32 mOldValue;
  quint32 mNewValue;
};

class CmdChangeParamValueUInt64 : public TreeUndoCommand
{
public:
 
  CmdChangeParamValueUInt64(ParameterValueItem* item,
                        quint64 oldValue, 
                        quint64 newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgXtorParamInst* getParamInst();

private:
  CarbonCfg* mCfg;
  UtString mParamInstName;
  UtString mParamName;
  ParameterValueItem::ParamType mParamType;
  quint64 mOldValue;
  quint64 mNewValue;
};

class CmdChangeParamValueDouble : public TreeUndoCommand
{
public:
 
  CmdChangeParamValueDouble(ParameterValueItem* item,
                        double oldValue, 
                        double newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgXtorParamInst* getParamInst();

private:
  CarbonCfg* mCfg;
  UtString mParamInstName;
  UtString mParamName;
  ParameterValueItem::ParamType mParamType;
  double mOldValue;
  double mNewValue;
};

class CmdChangeParamValueString : public TreeUndoCommand
{
public:
 
  CmdChangeParamValueString(ParameterValueItem* item,
                        const QString& oldValue, 
                        const QString& newValue,
                        QUndoCommand* parent = 0);
  void undo();
  void redo();

private:
  CarbonCfgXtorParamInst* getParamInst();

private:
  CarbonCfg* mCfg;
  UtString mParamInstName;
  UtString mParamName;
  ParameterValueItem::ParamType mParamType;
  QString mOldValue;
  QString mNewValue;
};

class CmdDeleteItems : public QUndoCommand
{
public:
  CmdDeleteItems(QList<PortEditorTreeItem*> items, QUndoCommand* parent = 0);

  void undo();
  void redo();

  int numItems() { return mUndoDataList.count(); }

private:
  QList<UndoData*> mUndoDataList;
};

#endif
