// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/CarbonVersion.h"
#include "util/UtString.h"
#include "util/ArgProc.h"

#include <QWorkspace>
#include <QSignalMapper>
#include <QtGui>

#include "gui/CQt.h"
#include "CQtWizardContext.h"
#include "shell/ReplaySystem.h"
#include "shell/carbon_misc.h"

#include "MdiRuntime.h"
#include "RuntimeStateEditor.h"
#include "CarbonModelsWidget.h"
#include "shell/ReplaySystem.h"
#include "SettingsEditor.h"
#include "CarbonInstanceOptions.h"
#include "SimulationHistoryWidget.h"
#include "OnDemandTraceWidget.h"
#include "CarbonRuntimeState.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "OnDemandTuningWizard.h"
#include "CarbonFiles.h"


void RuntimeStateEditor::closeEvent(QCloseEvent* ev)
{
  ev->accept(); 
  MDIWidget::closeEvent(this); 
  // we are finished now
  mContext->putCarbonModelsWidget(NULL);
}

RuntimeStateEditor::RuntimeStateEditor(CarbonMakerContext *ctx, MDIRuntimeTemplate* tplate, QWidget *parent)
  :QWidget(parent), mContext(ctx), mOnDemandTuningWizard(NULL)
{
  mRuntimeState = new CarbonRuntimeState(ctx);

  setAttribute(Qt::WA_DeleteOnClose);

  initializeMDI(this, tplate);

  QVBoxLayout *vbox = new QVBoxLayout();
  vbox->setSpacing(6);
  vbox->setMargin(0);

  setLayout(vbox);

  mCarbonModels = new CarbonModelsWidget(ctx, mRuntimeState, this);

#define MULTI_SELECT 0
#if MULTI_SELECT
  QPushButton *selectAll;
  selectAll = new QPushButton(this);
  selectAll->setObjectName(QString::fromUtf8("selectAll"));
  QObject::connect(selectAll, SIGNAL(clicked()), mCarbonModels, SLOT(selectAll()));
  selectAll->setText(tr("select all"));
#endif

  QWidget *models = new QWidget(this);
  QVBoxLayout *modelsVBox = new QVBoxLayout(models);
  modelsVBox->setSpacing(6);
  modelsVBox->setMargin(0);

  models->setLayout(modelsVBox);
#if MULTI_SELECT
  modelsVBox->addWidget(selectAll);
#endif
  modelsVBox->addWidget(mCarbonModels);


  mSimulationHistory = new SimulationHistoryWidget(this);

  // This x axis stuff all belongs in SimulationHistoryWidget, but because that
  // is based on gui/ReplayPerfPlot (which I don't want to duplicate), we have
  // to do the layout here.  Eventually just copy ReplayPerfPlot and fix it.
  QComboBox *combo = new QComboBox(this);
  combo->setObjectName(QString::fromUtf8("xAxisCombo"));
  QFont font;
  font.setPointSize(12);
  font.setBold(true);
  font.setWeight(75);
  combo->setFont(font);
  combo->setFrame(false);
  combo->clear();
  combo->insertItems(0, QStringList()
                           << tr("CPU Seconds (User + Sys)")
                           << tr("Real Time (seconds)")
                           << tr("Cycles")
                           << tr("Aggregate Schedule Calls")
                           );
  mSimulationHistory->putXAxisCombo(combo);
  CQT_CONNECT(combo, activated(int), mSimulationHistory, changeXAxis(int));

  QWidget *graph = new QWidget(this);
  QVBoxLayout *graphVBox = new QVBoxLayout(graph);
  graphVBox->setSpacing(6);
  graphVBox->setMargin(0);

  graph->setLayout(graphVBox);
  graphVBox->addWidget(mSimulationHistory);
  graphVBox->addWidget(combo);

  QSplitter *hsplit = new QSplitter(Qt::Vertical, this);
  hsplit->addWidget(models);
  hsplit->addWidget(graph);

  vbox->addWidget(hsplit);

  // notify the widgets when we update the CarbonReplaySystem data
  CQT_CONNECT(mRuntimeState, carbonSystemUpdated(CarbonRuntimeState&,CarbonReplaySystem&,CarbonReplaySystem&),
              mCarbonModels, carbonSystemUpdated(CarbonRuntimeState&,CarbonReplaySystem&,CarbonReplaySystem&));
  CQT_CONNECT(mRuntimeState, carbonSystemUpdated(CarbonRuntimeState&,CarbonReplaySystem&,CarbonReplaySystem&),
              mSimulationHistory, carbonSystemUpdated(CarbonRuntimeState&,CarbonReplaySystem&,CarbonReplaySystem&));

  // notify the MDI template when the runtime state changes, so it can update the toolbars
  CQT_CONNECT(mRuntimeState, carbonSystemUpdated(CarbonRuntimeState&,CarbonReplaySystem&,CarbonReplaySystem&),
              tplate, carbonSystemUpdated(CarbonRuntimeState&,CarbonReplaySystem&,CarbonReplaySystem&));


  // hook up to the toolbar buttons
#if USE_ACTION_FOR_CONTINUE_SIM
  CQT_CONNECT(tplate->actionContinueSimulation, triggered(), this, on_actionContinueSimulation_triggered());
#else
  CQT_CONNECT(tplate->mContinueSimButton, clicked(), this,
              on_actionContinueSimulation_triggered());
#endif

  CQT_CONNECT(tplate->actionReplayPlay, triggered(), this, on_actionReplayPlay_triggered());
  CQT_CONNECT(tplate->actionReplayRecord, triggered(), this, on_actionReplayRecord_triggered());
  CQT_CONNECT(tplate->actionReplayNormal, triggered(), this, on_actionReplayNormal_triggered());
  CQT_CONNECT(tplate->actionReplayVerbose, triggered(bool), this, on_actionReplayVerbose_triggered(bool));
  CQT_CONNECT(tplate->actionOnDemandStart, triggered(), this, on_actionOnDemandStart_triggered());
  CQT_CONNECT(tplate->actionOnDemandStop, triggered(), this, on_actionOnDemandStop_triggered());
  CQT_CONNECT(tplate->actionOnDemandTune, triggered(bool), this, on_actionOnDemandTune_triggered());
  CQT_CONNECT(tplate->actionOnDemandEnableTrace, triggered(bool), this, on_actionOnDemandEnableTrace_triggered(bool));
  CQT_CONNECT(tplate->actionOnDemandViewTrace, triggered(), this, on_actionOnDemandViewTrace_triggered());
}

RuntimeStateEditor::~RuntimeStateEditor()
{
  delete mCarbonModels;
  delete mRuntimeState;
}

bool RuntimeStateEditor::open(const char *fname)
{
  UtString currDir;

  QFileInfo fi(fname);
  
  if (fi.absolutePath().length() == 0)
  {
    qDebug() << "changing to output directory";
    CarbonProject* proj = mContext->getCarbonProjectWidget()->project();
    if (proj)
    {
      CarbonConfiguration* cfg = proj->getActive();
      if (cfg)
        currDir = cfg->getOutputDirectory();
    }
  }
  else
  {
    qDebug() << "changing to css file directory";
    currDir << fi.absolutePath();
  }

  TempChangeDirectory t(currDir.c_str());

  bool stat = mRuntimeState->loadCarbonSystem(fname);

  if (stat) {
    // It's possible that the user has opened .cot files before
    // opening the .css file.  Go through all the components in the
    // system and look for a corresponding .cot file.
    CarbonReplaySystem *pollSystem = mRuntimeState->getPollCarbonSystem();
    const char* systemName = pollSystem->getSystemName();
    UInt32 numComponents = pollSystem->numComponents();
    for (UInt32 i = 0; i < numComponents; ++i) {
      // Determine the .cot file for this component
      CarbonSystemComponent *component = pollSystem->getComponent(i);
      const char *componentName = component->getName();
      UtString traceFile;
      traceFile << systemName << "." << componentName << CARBON_STUDIO_ONDEMAND_TRACE_EXT;
      OnDemandTuningWidget* tuningWidget = dynamic_cast<OnDemandTuningWidget*>(mContext->getCarbonProjectWidget()->getOpenFile(traceFile.c_str()));
      if (tuningWidget) {
        // Tell the tuning widget its component's name.  This also
        // causes the tuning widget to register for system updates.
        tuningWidget->putComponent(componentName);
      }
    }
  }

  return stat;
}

const char *RuntimeStateEditor::userFriendlyName()
{
  return mRuntimeState->getFileName();
}

void RuntimeStateEditor::on_actionContinueSimulation_triggered()
{
  mRuntimeState->continueSimulation();
}

void RuntimeStateEditor::on_actionReplayPlay_triggered()
{
  mRuntimeState->replayPlayback();
}

void RuntimeStateEditor::on_actionReplayRecord_triggered()
{
  mRuntimeState->replayRecord();
}

void RuntimeStateEditor::on_actionReplayNormal_triggered()
{
  mRuntimeState->replayStop();
}

void RuntimeStateEditor::on_actionReplayVerbose_triggered(bool verbose)
{
  mRuntimeState->replayVerbose(verbose);
}

void RuntimeStateEditor::on_actionOnDemandStart_triggered()
{
  mRuntimeState->onDemandStart();
}

void RuntimeStateEditor::on_actionOnDemandStop_triggered()
{
  mRuntimeState->onDemandStop();
}

void RuntimeStateEditor::on_actionOnDemandTune_triggered()
{
  if (mOnDemandTuningWizard == NULL) {
    mOnDemandTuningWizard = new OnDemandTuningWizard(mRuntimeState, this);
  }
  mOnDemandTuningWizard->show();
  mOnDemandTuningWizard->raise();
}

void RuntimeStateEditor::on_actionOnDemandEnableTrace_triggered(bool enable)
{
  mRuntimeState->onDemandTrace(enable);
}

void RuntimeStateEditor::on_actionOnDemandViewTrace_triggered()
{
  const char* systemName = mRuntimeState->getPollCarbonSystem()->getSystemName();

  UInt32 numModels = mRuntimeState->numModelsSelected();
  if (numModels > 0)
  {
    for (UInt32 i=0; i<numModels; ++i)
    {
      UtString traceFile;
      traceFile << systemName << "." << mRuntimeState->getSelectedModel(i) << CARBON_STUDIO_ONDEMAND_TRACE_EXT;
      OnDemandTuningWidget* tuningWidget = dynamic_cast<OnDemandTuningWidget*>(mContext->getCarbonProjectWidget()->openSource(traceFile.c_str()));
      if (tuningWidget)
        tuningWidget->putComponent( mRuntimeState->getSelectedModel(i) );
    }
  }
}

