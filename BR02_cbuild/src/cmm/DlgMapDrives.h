#ifndef DLGMAPDRIVES_H
#define DLGMAPDRIVES_H

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include <QDialog>
#include "ui_DlgMapDrives.h"
#include "CarbonProjectWidget.h"
#include "SSHConsole.h"
#include "ShellConsole.h"

class VerifyCommand
{
public:
  VerifyCommand(const QString& title)
  {
    mTitle = title;
    mExitStatus = -1;
    mResult = false;
    mFinishedVerify = false;
  }
  virtual ~VerifyCommand() {}
  virtual void commandFinished(int newVal)=0;
  virtual void processOutput(const QString&)=0;

  bool getResult() { return mResult; }
  QString getTitle() { return mTitle; }
  void putTitle(const QString& newVal) { mTitle = newVal; }
  QString getResultString() { return mResultString; }
  void putCommand(const QString& newVal) { mCommand = newVal; }
  void putArgs(const QStringList& args) { mArgs = args; }
  QString getCommand() { return mCommand; }
  QStringList& getArgs() { return mArgs; }
  bool getFinishedVerification() { return mFinishedVerify; }

protected:
  void putResult(bool newVal, const QString& newMsg)
  {
    mResult = newVal; 
    mResultString = newMsg;
  }

  void putFinishedVerification(bool newVal) { mFinishedVerify = newVal; }
  
private:
  bool mResult;
  bool mFinishedVerify;
  QString mCommand;
  QStringList mArgs;
  QString mResultString;
  QString mTitle;
  int mExitStatus;
};

class VerifyWriteability : public VerifyCommand
{
public:
  void putExpectedOutput(const QString& newVal) { mExpectedOutput = newVal; }
  VerifyWriteability() : VerifyCommand("Checking Drive Writeability") 
  {
    mDriveWriteable = false;
  }
  void putMappingInfo(const QString& driveLetter, const QString& unixPath)
  {
    mDriveLetter = driveLetter;
    mUnixPath = unixPath;
  }

protected:
  virtual void processOutput(const QString&);
  virtual void commandFinished(int);

private:
  bool mDriveWriteable;
  QString mExpectedOutput;
  QString mDriveLetter;
  QString mUnixPath;
};

class VerifyUnixDirectory : public VerifyCommand
{
public:
  VerifyUnixDirectory() : VerifyCommand("Checking Unix Directory") {}

protected:
  virtual void processOutput(const QString&);
  virtual void commandFinished(int);
};

class VerifyUnixDirectoryContents : public VerifyCommand
{
public:
  VerifyUnixDirectoryContents() : VerifyCommand("Checking Unix Directory Contents") {}
  void putMappingInfo(const QString& driveLetter, const QString& unixPath)
  {
    mDriveLetter = driveLetter;
    mUnixPath = unixPath;
  }

protected:
  virtual void processOutput(const QString&);
  virtual void commandFinished(int);

private:
  int mNumFiles;
  QStringList mFilenames;
  QString mDriveLetter;
  QString mUnixPath;
};

class VerifyUnixCommand : public VerifyCommand
{
public:
  VerifyUnixCommand() : VerifyCommand("") {}

protected:
  virtual void processOutput(const QString&);
  virtual void commandFinished(int);
};

class DlgMapDrives : public QDialog
{
  Q_OBJECT

public:
  DlgMapDrives(QWidget *parent = 0, CarbonProjectWidget* pw = 0, CarbonMappedDrives* mappings = 0);
  ~DlgMapDrives();
  static QString makeGUID();
  enum CommandState { Started, Finished };
  QString getUnixPath() { return mUnixPath; }
  QString getDriveLetter() { return mDriveLetter; }
  QString getUsername() { return mUsername; }
  QString getServer() { return mServer; }

private:
  Ui::DlgMapDrivesClass ui;

private slots:
  void on_pushButtonVerify_clicked();
  void serverInfoChanged();
  void on_comboBoxDrives_currentIndexChanged(int);
  void unixPathChanged();
  void on_buttonBox_rejected();
  void on_buttonBox_accepted();
  void sendCommand();

  // slots from SSHConsole
  void sessionLoginStatus(const QString&);
  void processOutput(const QString&);
  void commandCompleted(int);
  void sessionStateChanged(SSHConsole::LoginState);

private:
  void createVerifyDirectoryCommand();
  void createVerifyDirectoryContentsCommand(const QString& driveLetter);

  void enableButtons();

private:
  CarbonProjectWidget* mProjectWidget;
  SSHConsole mSSHConsole;
  CarbonMappedDrives* mMappedDrives;
  CarbonMappedDrives mLocalMap;
  bool driveIsWriteable(const char* driveLetter, QString& tempFile, QString& guid);
  CommandState mCommandState;
  QList<VerifyCommand*> mVerifyCommands;
  VerifyCommand* mCurrentCommand;
  bool mRestartSession;
  bool mVerified;

  QString mUnixPath;
  QString mDriveLetter;
  QString mUsername;
  QString mServer;
};

#endif // DLGMAPDRIVES_H
