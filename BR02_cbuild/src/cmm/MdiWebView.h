#ifndef __MDIWebView_H__
#define __MDIWebView_H__

#include "util/CarbonPlatform.h"

#include <QtGui>
#include <QtWebKit>
#include "util/UtString.h"
#include "Mdi.h"
#include "MdiWebView.h"


class MDIWebViewTemplate : public MDIDocumentTemplate
{
public:
  MDIWebViewTemplate();
 
  virtual MDIWidget* createNewDocument(QWidget* parent=0);
  virtual int createMenus();
  virtual int createToolbars();
  virtual void updateMenusAndToolbars(QWidget* widget);
  virtual MDIWidget* openDocument(QWidget* parent, const char* docName);
  virtual MDIWidget* createDocument(QWidget* parent, const char* docName);
  bool getYesToAll() const { return mYesToAll; }
  void putYesToAll(bool newVal) { mYesToAll=newVal; }

private:
  void updateSelectedTab(QWidget* widget);

private:
  QAction* mActionCut;
  QAction* mActionCopy;
  QAction* mActionPaste;
  QAction* mActionFind;
  QAction* mActionUndo;
  QAction* mActionRedo;
  QAction* mActionFileSave;
  QAction* mActionRunScript;
  QAction* mActionDebugScript;
  QAction* mActionToggleBreakpoint;
  QAction* mActionDeleteBreakpoints;

  bool mYesToAll;
};

class MDIWebBrowser : public MDIDocumentTemplate
{
public:
  MDIWebBrowser();
 
  virtual MDIWidget* createNewDocument(QWidget* parent=0);
  virtual MDIWidget* openDocument(QWidget* parent, const char* docName);

};

class WebBrowserWidget : public QWebView, public MDIWidget
{
 Q_OBJECT
public:
  WebBrowserWidget(MDIDocumentTemplate* doct=0, QWidget* parent=0);
  const char* userFriendlyName();

  void closeEvent(QCloseEvent *event);
private:
  UtString mDocName;
};

#endif
