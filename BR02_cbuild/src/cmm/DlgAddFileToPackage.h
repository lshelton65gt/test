// -*-C++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef DLGADDFILETOPACKAGE_H
#define DLGADDFILETOPACKAGE_H

#include <QDialog>
#include "ui_DlgAddFileToPackage.h"

class DlgAddFileToPackage : public QDialog
{
    Q_OBJECT

public:
    DlgAddFileToPackage(QWidget *parent = 0);
    ~DlgAddFileToPackage();
    QString fileName();
    QString filePath();

    enum Result {OK, Cancel};
private:
    Ui::DlgAddFileToPackageClass ui;
    QString mFileName;
    QString mFilePath;

private slots:
  void on_buttonBox_accepted();
  void on_buttonBox_rejected();
  void on_pushButtonBrowse_clicked();
};

#endif // DLGADDFILETOPACKAGE_H

