//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "gui/CQt.h"

#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/SourceLocator.h"
#include "util/UtVector.h"

#include "Directives.h"
#include "CarbonDirectives.h"

void Directive::serialize(xmlTextWriterPtr writer, CarbonDirectives* /*dirs*/, CarbonDirectiveGroup* /*group*/)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Directive");

  xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST mDirective.c_str());

  if (mValue.length() > 0)
    xmlTextWriterWriteAttribute(writer, BAD_CAST "Value", BAD_CAST mValue.c_str());


  xmlTextWriterEndElement(writer);

}

void Directive::deserialize(DirectiveType dirType, CarbonDirectives* dirs, CarbonDirectiveGroup* group, xmlNodePtr parent, UtXmlErrorHandler* /*eh*/)
{
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    const char* elementName = XmlParsing::elementName(child);
    if (elementName == NULL)
      continue;

    if (0 == strcmp(elementName, "Locator"))
    {
      UtString locator;
      XmlParsing::getProp(child, "Name", &locator);
  
      switch (dirType)
      {
      case eDirectiveNet:
        {
          NetDirectives* netDir = new NetDirectives(locator.c_str());
          group->getNetDirectives()->push_back(netDir);
          netDir->deserialize(child, dirs, group);
          break;
        }
      case eDirectiveModule:
        {
          ModuleDirectives* modDir = new ModuleDirectives(locator.c_str());
          group->getModuleDirectives()->push_back(modDir);
          modDir->deserialize(child, dirs, group);
         break;
        }
      }
    }
  }
}

void NetDirectives::copyDirectives(CarbonDirectiveGroup* newGroup)
{
  NetDirectives* netDir = new NetDirectives(mLocator.c_str());
  newGroup->addNetDirective(netDir);

  foreach (Directive* dir, mDirectives)
    netDir->addDirective(dir->copyDirective());
}

void NetDirectives::deserialize(xmlNodePtr parent, CarbonDirectives* dirs, CarbonDirectiveGroup* /*group*/)
{
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    const char* elementName = XmlParsing::elementName(child);
    if (elementName == NULL)
      continue;
  
    if (0 == strcmp(elementName, "Directive"))
    {
      UtString name;
      XmlParsing::getProp(child, "Name", &name);

      UtString value;
      XmlParsing::getProp(child, "Value", &value);

      DirectiveDefinition* def = dirs->findDefinition(name.c_str());

      UtString defError;
      defError << "Unable to locate directive definition for directive: " << name.c_str();
      INFO_ASSERT(def != NULL, defError.c_str());

      Directive* dir = new Directive(name.c_str(), value.c_str(), def);
      mDirectives.push_back(dir);
    }
  }
}

Directive* Directive::copyDirective()
{
  Directive* newDir = new Directive(mDirective.c_str(), mValue.c_str(), mDefinition);
  return newDir;
}

void ModuleDirectives::copyDirectives(CarbonDirectiveGroup* newGroup)
{
  ModuleDirectives* modDir = new ModuleDirectives(mLocator.c_str());
  newGroup->addModuleDirective(modDir);

  foreach (Directive* dir, mDirectives)
    modDir->addDirective(dir->copyDirective());
}

void ModuleDirectives::deserialize(xmlNodePtr parent, CarbonDirectives* dirs, CarbonDirectiveGroup* /*group*/)
{
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    const char* elementName = XmlParsing::elementName(child);
    if (elementName == NULL)
      continue;
    
    if (0 == strcmp(elementName, "Directive"))
    {
      UtString name;
      XmlParsing::getProp(child, "Name", &name);

      UtString dirValue;
      XmlParsing::getProp(child, "Value", &dirValue);

      DirectiveDefinition* def = dirs->findDefinition(name.c_str());

      UtString defError;
      defError << "Unable to locate directive definition for directive: " << name.c_str();
      INFO_ASSERT(def != NULL, defError.c_str());
      
      const char* value = NULL;
      if (dirValue.length() > 0)
        value = dirValue.c_str();

      Directive* dir = new Directive(name.c_str(), value, def);
      mDirectives.push_back(dir);
    }
  }
}


void NetDirectives::serialize(xmlTextWriterPtr writer, CarbonDirectives* dirs, CarbonDirectiveGroup* group)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Locator");

  xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST mLocator.c_str());

  foreach (Directive* dir, mDirectives)
    dir->serialize(writer, dirs, group);

  xmlTextWriterEndElement(writer);
}

void ModuleDirectives::serialize(xmlTextWriterPtr writer, CarbonDirectives* dirs, CarbonDirectiveGroup* group)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Locator");

  xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST mLocator.c_str());

  foreach (Directive* dir, mDirectives)
    dir->serialize(writer, dirs, group);

  xmlTextWriterEndElement(writer);
}
