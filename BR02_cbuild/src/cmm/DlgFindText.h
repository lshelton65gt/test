#ifndef DLGFINDTEXT_H
#define DLGFINDTEXT_H

#include "util/CarbonPlatform.h"
#include <QSettings>
#include <QDialog>
#include <QCompleter>

#include <Qsci/qsciscintilla.h>

#include "ui_DlgFindText.h"

class QTextEditor;

class DlgFindText : public QDialog
{
  Q_OBJECT

public:
  DlgFindText(QWidget *parent = 0);
  ~DlgFindText();

  void setEditor(QsciScintilla* editor);
  void setCurrentText(QString text);

private:
  Ui::DlgFindTextClass ui;
  QsciScintilla* mEditor;
  bool mFirst;
  QStringList mStrings;
  QCompleter* mCompleter;
  QString mLastText;

  private slots:
    void on_checkBoxRegexp_stateChanged(int);
    void on_checkBoxMatchCase_stateChanged(int);
    void on_checkBoxWholeWord_stateChanged(int);
    void on_radioButtonDown_toggled(bool);
    void on_radioButtonUp_toggled(bool);
    void on_pushButtonCancel_clicked();
    void on_pushButtonFind_clicked();
    void dialogClosed();

};

#endif // DLGFINDTEXT_H
