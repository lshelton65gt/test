#ifndef REMOTECONSOLE_H
#define REMOTECONSOLE_H
#include "util/CarbonPlatform.h"

#include <QWidget>
#include <QProcess>

#include "util/UtString.h"

#include "ui_RemoteConsole.h"

class CarbonProjectWidget;
class QTextCodec;
class CarbonProject;
class CarbonProperty;

class RemoteConsole : public QWidget
{
  Q_OBJECT

public:
  enum CommandType { Command, Compilation, Check, Clean};
  enum CommandMode { Local, Remote };
  enum CommandFlags { Program, Simulate };

  RemoteConsole(QWidget *parent = 0);
  ~RemoteConsole();
  void setProjectWidget(CarbonProjectWidget* pw);
  void executeCommand(CommandMode commandMode, CommandType commandType, QString program, QStringList args, QString workingDir, CommandFlags flags=Program);
  void processOutput(CommandType ctype, const QString& outputLine);
  void processBuffer(CommandType ctype, const char* buffer);
  void clear() { ui.textEdit->clear(); }
  void stopCompilation();
  void putUseProject(bool val) { mUseProject = val; }
  void setCredentials(const char* server, const char* user)
  {
    mAuthServer = server;
    mAuthUser = user;
  }
  bool hasActiveProcess()
  {
    // really only valid when in non-remote mode
    return (mProcess != NULL || mLocalProcess != NULL);
  }

  bool hasExtraProcess()
  {
    return (mLocalProcess != NULL);
  }

  void signalBeginCompilation() { emit compilationStarted( Local, Compilation ); }

private:
  Ui::RemoteConsoleClass ui;
  void launch( QString&, QStringList&, bool echo=true);
  void appendBytes(const QByteArray& bytes);
  bool loggedInAndReady();
  void login();
  void executeRemoteCommand(CommandType ctype, QString prog, QStringList args, QString workingDir, CommandFlags flags);
  void executeLocalCommand(CommandType ctype, QString prog, QStringList args, QString workingDir, CommandFlags flags);
  enum LoginState {Unknown=0, LoggingIn, Authorizing, FailedLogin, RemoteDirCheck, RemoteVersionCheck, LoggedIn, LoggedOut};
  void readStandardOutput(QProcess*);
  void readStandardError(QProcess*);
  void killProcessAndSubprocesses(int pid);
  void executeWindowsMake();

private slots:
  void processError(QProcess::ProcessError);
  void readStderr();
  void readStdout();
  void readStderrLocal();
  void readStdoutLocal();
  void started();
  void finished(int exit_code);
  void startedLocal();
  void finishedLocal(int exit_code);
  void projectLoaded(const char*, CarbonProject*);
  void remoteInfoChanged(const CarbonProperty* sw, const char* value);
  void on_toolButtonClearAll_clicked();
  void configurationChanged(const char* name);

signals:
  void stdoutChanged(RemoteConsole::CommandType ctype, const QString& text, const QString& workingDir);
  void stderrChanged(RemoteConsole::CommandType ctype, const QString& text, const QString& workingDir);
  void compilationStarted(RemoteConsole::CommandMode, RemoteConsole::CommandType);
  void compilationEnded(RemoteConsole::CommandMode, RemoteConsole::CommandType);
  void makefileFinished(RemoteConsole::CommandMode, RemoteConsole::CommandType);
  void modelCompilationFinished(RemoteConsole::CommandMode, RemoteConsole::CommandType);
  void simulationStarted(RemoteConsole::CommandMode, RemoteConsole::CommandType);
  void simulationEnded(RemoteConsole::CommandMode, RemoteConsole::CommandType);
  void exploreFinished(RemoteConsole::CommandMode, RemoteConsole::CommandType);

private:
  CarbonProjectWidget* mProjectWidget;
  QProcess* mProcess;
  QProcess* mLocalProcess;
  QTextCodec* mCodec;
  bool mLoggedIn;
  bool mCommandPending;
  bool mInitialized;
  LoginState mLoginState;
  UtString mPassword;
  QString mWorkingDir;
  QString mPendingProg;
  QStringList mPendingArgs;
  CommandType mCommandType;
  CommandMode mCommandMode;
  CommandFlags mCommandFlags;
  CommandFlags mPendingFlags;
  
  QString mProgram;
  QStringList mProgramArgs;

  QRegExp mStatusExp;
  bool mVersionMatched;
  bool mVersionCheckCompleted;
  bool mUseProject;

  UtString mAuthUser;
  UtString mAuthServer;
};


#endif // REMOTECONSOLE_H
