#ifndef __DESIGNXML_H__
#define __DESIGNXML_H__

#include "util/CarbonPlatform.h"

#include <QtGui> 
#include <QProcess>

#include "Scripting.h"

class XInstance;
class XInterfaces;
class XToplevelInterface;
class XParameters;
class JavaScriptAPIs;

// File: DesignHierarchy
//


// 
// Class: Parameter
//
class XParameter : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  static bool deserialize(XParameters* params, const QDomElement& e, XmlErrorHandler* eh);

  void dump();
 // Property: name
 // The String name of the Parameter
 //
 // Access:
 // *ReadOnly*
  Q_PROPERTY(QString name READ getName);
 // Property: type
 // The String type of the parameter
 //
 // Access:
 // *ReadOnly*
  Q_PROPERTY(QString type READ getType);
 // Property: value
 // The String value of the parameter
 //
 // Access:
 // *ReadOnly*
  Q_PROPERTY(QString value READ getValue);

  QString getName() { return mName; }
  void setName(const QString& newVal) { mName=newVal; }
  
  QString getType() { return mType; }
  void setType(const QString& newVal) { mType=newVal; }

  QString getValue() { return mValue; }
  void setValue(const QString& newVal ) { mValue=newVal; }

private:
  QString mName;
  QString mType;
  QString mValue;
};

//
// Class: Parameters
//
class XParameters : public QObject, protected QScriptable
{
  Q_OBJECT
  // Property: count
  // The Integer number of <Parameters>s in this collection.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(int count READ numParameters)
  // Method: Parameter getParameter(index)
  // Returns the *index*th item in the collection.
  //
  // Parameters: 
  //  name - Integer index of the Parameter to return (0-based)
  //
  // Returns:
  //   <Parameter> or null if not found.
public slots:
  int numParameters() const { return mParameters.count(); }

  XParameter* getParameter(quint32 index)
  {
    return mParameters[index];
  }

public:
  virtual ~XParameters();
  XParameters()
  {
    mInstance = NULL;
  }
  void dump();
  bool deserialize(XInstance* inst, const QDomElement& e, XmlErrorHandler* eh);
  void addParameter(XParameter* param) 
  {
    mParameters.push_back(param);
  }
  XParameter* getAt(int index)
  {
    return mParameters[index];
  }
private:
  XInstance* mInstance;
  QList<XParameter*> mParameters;
};


class XInstancePorts;
// Class: InstancePort
// 
class XInstancePort : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  void dump();

  // Property: actual
  // The String name of the actual port
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString actual READ getActual)
  QString getActual() { return mActual; }
  void setActual(const QString& newVal) { mActual=newVal; }

  // Property: formal
  // The String name of the formal port
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString formal READ getFormal)
  QString getFormal() { return mFormal; }
  void setFormal(const QString& newVal) { mFormal=newVal; }

  static bool deserialize(XInstancePorts* inst, const QDomElement& e, XmlErrorHandler* eh);

private:
  QString mActual;
  QString mFormal;
};

// Class: InstancePorts
// The collection of ports associated with this instance.
class XInstancePorts : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  virtual ~XInstancePorts();
  XInstancePorts()
  {
    mInstance = NULL;
  }
  void dump();
  void addPort(XInstancePort* port) 
  {
    mPorts.push_back(port);
  }
  // Property: count
  // The Integer number of <InstancePort>s in this collection.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(int count READ numPorts)
  // Method: InstancePort getPort(index)
  // Returns the *index*th item in the collection.
  //
  // Parameters: 
  //  name - Integer index of the port to return (0-based)
  //
  // Returns:
  //   <InstancePort> or null if not found.
  XInstancePort* getPort(int index)
  {
    return mPorts[index];
  }
  // Property: instance
  // The <Instance> that this port belongs to.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(XInstance* instance READ getInstance)
  XInstance* getInstance() { return mInstance; }

  bool deserialize(XInstance* inst, const QDomElement& e, XmlErrorHandler* eh);
  int numPorts() { return mPorts.count(); }
  XInstancePort* getAt(int index) { return mPorts[index]; }
  
private:
  XInstance* mInstance;
  QList<XInstancePort*> mPorts;
};

class XInterfacePorts;

// Class: InterfacePort
class XInterfacePort : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  enum InterfacePortDirection { Input, Output, InOut };

  void dump();
  // Property: name
  // The String name of this port.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString name READ getName)
  QString getName() { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

  // Property: type
  // The String type of this port.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString type READ getType)
  QString getType() { return mType; }
  void setType(const QString& newVal) { mType=newVal; }

  // Property: direction
  // The enumerated value *PortDirection* direction of this port
  // One of: InterfacePort.Input, InterfacePort.Output, InterfacePort.InOut
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(InterfacePortDirection direction READ getDirectionEnum)
  Q_ENUMS(InterfacePortDirection)
  InterfacePortDirection getDirectionEnum()
  {
    QString direction = mDirection.toLower();
    if (direction == "in")
      return Input;
    else if (direction == "out")
      return Output;
    else
      return InOut;
  } 
  QString getDirection() { return mDirection; }
  void setDirection(const QString& newVal) { mDirection=newVal; }
 
  // Property: width
  // The Integer width of this port in bits.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(int width READ getWidthInt)
  int getWidthInt() { return mWidth.toInt(); }
  QString getWidth() { return mWidth; }
  void setWidth(const QString& newVal) { mWidth=newVal; }

  static bool deserialize(XInterfacePorts* inst, const QDomElement& e, XmlErrorHandler* eh);

private:
  QString mName;
  QString mType;
  QString mDirection;
  QString mWidth;
};

class XInterfaceArchitectures;

// Class: InterfaceArchitecture
class XInterfaceArchitecture : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  void dump();

  // Property: name
  // The String name of this architecture
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString name READ getName)

  QString getName() { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

  // Property: library
  // The String library name that this architecture was compiled into.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString library READ getLibrary)

  QString getLibrary() { return mLibrary; }
  void setLibrary(const QString& newVal) { mLibrary=newVal; }
  
  // Property: start
  // The String locator of the form <filename>:<linenumber> of ths start of this architecture.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString start READ getStart)
  QString getStart() { return mStart; }
  void setStart(const QString& newVal) { mStart=newVal; }
 
  // Property: end
  // The String locator of the form <filename>:<linenumber> of ths end of this architecture.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString end READ getEnd)
  QString getEnd() { return mEnd; }
  void setEnd(const QString& newVal) { mEnd=newVal; }

  static bool deserialize(XInterfaceArchitectures* inst, const QDomElement& e, XmlErrorHandler* eh);

private:
  QString mName;
  QString mLibrary;
  QString mStart;
  QString mEnd;
};

class XInterface;

// Class: InterfacePorts
// The collection of <InterfacePort> objects associated with the interface.
class XInterfacePorts : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  XInterfacePorts()
  {
    mInterface = NULL;
  }
  virtual ~XInterfacePorts();

  void dump();
  void addPort(XInterfacePort* port) 
  {
    mPorts.push_back(port);
    mPortMap[port->getName()] = port;
  }
  // Property: count
  // The Integer number of <InterfacePort>s in this collection.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(int count READ numPorts);
  
  XInterfacePort* findPort(const QString& portName)
  {
    return mPortMap[portName];
  }
  
public slots:
  // Method: InterfacePort getPort(name)
  // Returns the *name*ed item in the collection.
  //
  // Parameters: 
  //  name - String name of the port to locate
  //
  // Returns:
  //   <InterfacePort> or null if not found.
  XInterfacePort* getPort(const QString& portName)
  {
    return findPort(portName);
  }
  // Method: InterfacePort getPort(index)
  // Returns the *index*th item in the collection.
  //
  // Parameters: 
  //  name - Integer index of the port to return (0-based)
  //
  // Returns:
  //   <InterfacePort> or null if not found.
  XInterfacePort* getPort(int index)
  {
    return mPorts[index];
  }


public:
  bool deserialize(XInterface* iface, const QDomElement& e, XmlErrorHandler* eh);
  int numPorts() { return mPorts.count(); }
  XInterfacePort* getAt(int i) { return mPorts[i]; }

private:
  XInterface* mInterface;
  QList<XInterfacePort*> mPorts;
  QMap<QString, XInterfacePort*> mPortMap;
};

// Class: InterfaceArchitectures
class XInterfaceArchitectures : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  virtual ~XInterfaceArchitectures();
  XInterfaceArchitectures()
  {
    mInterface = NULL;
  }
  void dump();
  void addArchitecture(XInterfaceArchitecture* architecture) 
  {
    mArchitectures.push_back(architecture);
    mArchitectureMap[architecture->getName()] = architecture;
  }
  Q_PROPERTY(int count READ numArchitectures)

  bool deserialize(XInterface* iface, const QDomElement& e, XmlErrorHandler* eh);
  int numArchitectures() { return mArchitectures.count(); }

  // Method: InterfaceArchitecture getArchitecture(index)
  // Returns the *index*th item in the collection.
  //
  // Parameters: 
  //  index - Integer index (0-based)
  //
  // Returns:
  //   <InterfaceArchitecture> or null if not found.
public slots:
  XInterfaceArchitecture* getArchitecture(int i) { return mArchitectures[i]; }

  // Method: InterfaceArchitecture find(archName)
  // Returns the archName named architecture if found.
  //
  // Parameters: 
  //  archName - String architecture to search for.
  //
  // Returns:
  //   <InterfaceArchitecture> or null if not found.
  XInterfaceArchitecture* find(const QString& archName)
  {
    return findArchitecture(archName);
  }

public:
  XInterfaceArchitecture* getAt(int i) { return mArchitectures[i]; }
  XInterfaceArchitecture* findArchitecture(const QString& archName)
  {
    return mArchitectureMap[archName];
  }
  // Method: InterfaceArchitecture getPort(name)
  // Returns the *name*ed item in the collection.
  //
  // Parameters: 
  //  name - String name of the port to locate
  //
  // Returns:
  //   <InterfacePort> or null if not found.
  Q_PROPERTY(XInterfaceArchitecture* architecture READ getArchitecture)
  XInterfaceArchitecture* getArchitecture() 
  { 
    if (!mArchitectures.isEmpty())
      return mArchitectures.first();
    else
      return NULL;
  }

private:
  XInterface* mInterface;
  QList<XInterfaceArchitecture*> mArchitectures;
  QMap<QString, XInterfaceArchitecture*> mArchitectureMap;
};

// Class: Interface
// Represents a VHDL Entity or a Verilog Module.
class XInterface : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  XInterface(XInterfaces* ifaces)
  {
    mInterfaces = ifaces;
  }
  void dump();
  // Property: library
  // The String name of the library that this interface was compiled into.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString library READ getLibrary)
  QString getLibrary() { return mLibrary; }
  void setLibrary(const QString& newVal) { mLibrary=newVal; }

  // Property: name
  // The String name of this interface.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString name READ getName)
  QString getName() { return mName; }
  void setName(const QString& newVal) { mName=newVal; }
  
  // Property: start
  // The String locator of the form <filename>:<linenumber> of the start
  // of this interface.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString start READ getStart)
  QString getStart() { return mStart; }
  void setStart(const QString& newVal) { mStart=newVal; }

  // Property: end
  // The String locator of the form <filename>:<linenumber> of the end
  // of this interface.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString end READ getEnd)
  QString getEnd() { return mEnd; }
  void setEnd(const QString& newVal) { mEnd=newVal; }

  // Property: language
  // The String language of this interface, either "vhdl" or "verilog"
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString language READ getLanguage)
  QString getLanguage() { return mLanguage; }
  void setLanguage(const QString& newVal) { mLanguage=newVal; }


  bool deserialize(const QDomElement& e, XmlErrorHandler* eh);

  Q_PROPERTY(XInterfacePorts* ports READ getPorts);
  XInterfacePorts* getPorts() { return &mPorts; }

  Q_PROPERTY(XInterfaceArchitectures* architectures READ getArchitectures);
  XInterfaceArchitectures* getArchitectures() { return &mArchitectures; }

  Q_PROPERTY(XInterfaceArchitecture* architecture READ getArchitecture);
  XInterfaceArchitecture* getArchitecture() { return mArchitectures.getArchitecture(); }

  XInterfaceArchitecture* findArchitecture(const QString& archName);

  Q_PROPERTY(XParameters* parameters READ getParameters);
  XParameters* getParameters() { return &mParameters; }

private:
  XInterfaces* mInterfaces;
  XInterfacePorts mPorts;
  XInterfaceArchitectures mArchitectures;
  XParameters mParameters;
  QString mLibrary;
  QString mName;
  QString mStart;
  QString mEnd;
  QString mLanguage;  
};

class XLibraries;
class XDesignFiles;

class XDefines;

class XDefine : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  bool deserialize(const QDomElement& e, XmlErrorHandler* eh);
  XDefine() {}
 
  // Property: name
  // The String name of this Define
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(QString name READ getName);
  QString getName() { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

  // Property: value
  // The String value of this Define
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(QString value READ getValue);
  QString getValue() { return mValue; }
  void setValue(const QString& newVal) { mValue=newVal; }

  // Property: locator
  // The String locator of this Define
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(QString locator READ getLocator);
  QString getLocator() { return mLocator; }
  void setLocator(const QString& newVal) { mLocator=newVal; }

private:
  QString mName;
  QString mValue;
  QString mLocator;
  XDefines* mDefines;
};

class XDefines : public QObject, protected QScriptable
{
  Q_OBJECT
  // Property: count
  // The Integer number of <Define>s in this collection.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(int count READ numDefines);
  XDefine* getAt(int index)
  {
    return mDefines[index];
  }

public slots:
  int numDefines() const { return mDefines.count(); }
  // Method: DesignFile getDefine(index)
  // Returns the *index*th item in the collection.
  //
  // Parameters: 
  //  index - Integer index from 0 to count-1
  //
  // Returns:
  //   <Define> or null if not found.
  XDefine* getDefine(int index)
  {
    return getAt(index);
  }

  // Method: Define getDefine(name)
  // Returns the *name*ed item in the collection.
  //
  // Parameters: 
  //  name - String name to match
  //
  // Returns:
  //   <Define> or null if not found.
  XDefine* getDefine(const QString& name)
  {
    return findDefine(name);
  }

public:
  virtual ~XDefines();

  void addDefine(XDefine* xdef)
  {
    mDefineMap[xdef->getName()] = xdef;
    mDefines.push_back(xdef);
  }

  XDefine* findDefine(const QString& name)
  {
    return mDefineMap[name];
  }

  QList<XDefine*>& getDefines() { return mDefines; }

private:
  QList<XDefine*> mDefines;
  QMap<QString, XDefine*> mDefineMap;

public:
  bool deserialize(const QDomElement& e, XmlErrorHandler* eh);
  void dump();
};


// Class: DesignFile
// 
// This class represents an RTL source file which was compiled into the design.
class XDesignFile : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  enum DesignFileKind { eKindUnknown=0, eKindEntity, eKindArchitecture, eKindPackageDeclaration, eKindPackageBody, 
                        eKindConfiguration, eKindModule, eKindVerilogLibrary, eKindIncludeFile };

  Q_ENUMS(DesignFileKind)
  XDesignFile(XDesignFiles* ifiles)
  {
    mFiles = ifiles;
    mFileKind = eKindUnknown;
  }
  
  virtual ~XDesignFile();

  void dump();
  // Property: name
  // The String fullpath name of this RTL source file
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(QString name READ getName);
  QString getName() { return mName; }
  void setName(const QString& newVal) { mName=newVal; }
  
  // Property: language
  // The String language of this source file.
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(QString language READ getLanguage);
  QString getLanguage() { return mLanguage; }
  void setLanguage(const QString& newVal) { mLanguage=newVal.toLower(); }

  Q_PROPERTY(XDefines* defines READ getDefines);

  // Property: kind
  // The enumerated value indicating the type of RTL source
  // One of: 
  //  DesignFile.eKindUnknown
  //  DesignFile.eKindEntity
  //  DesignFile.eKindArchitecture
  //  DesignFile.eKindPackageDeclaration
  //  DesignFile.eKindPackageBody
  //  DesignFile.eKindConfiguration
  //  DesignFile.eKindModule
  //  DesignFile.eKindVerilogLibrary
  //  DesignFile.eKindIncludeFile
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(DesignFileKind kind READ getKind);
  DesignFileKind getKind() { return mFileKind; }
  void setKind(DesignFileKind newVal) { mFileKind=newVal; }

  bool deserialize(const QDomElement& e, XmlErrorHandler* eh);

  DesignFileKind stringToKind(const QString& kind);

public slots:
  XDefines* getDefines() { return &mDefines; }

private:
  XDesignFiles* mFiles;
  QString mName;
  QString mLanguage;
  DesignFileKind mFileKind;
  XDefines mDefines;
};

//
// Class: Library
//
class XLibrary : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  XLibrary(XLibraries* ilibs)
  {
    mLibraries = ilibs;
  }
  void dump();
  // Property: name
  // The String name of this library.
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(QString name READ getName);
  QString getName() { return mName; }
  void setName(const QString& newVal) { mName=newVal; }
  
  // Property: defaultLibrary
  // Boolean flag indicates that this is the default library if true.
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(bool defaultLibrary READ getDefault);
  bool getDefault() { return mDefault; }
  void setDefault(bool newVal) { mDefault=newVal; }

  // Property: path
  // The String path of this library.
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(QString path READ getPath);
  QString getPath() { return mPath; }
  void setPath(const QString& newVal) { mPath=newVal; }

  bool deserialize(const QDomElement& e, XmlErrorHandler* eh);

private:
  XLibraries* mLibraries;
  bool mDefault;
  QString mName;
  QString mPath;

};

class XToplevelInterface;


//
// Class: Instances
// The collection of Instances
class XInstances : public QObject, protected QScriptable
{
  Q_OBJECT
 
  // Property: count
  // The Integer count of the number of instances in this collection.
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(int count READ getCount)

public:
  void addInstance(XInstance* item)
  {
    mItems.push_back(item);
  }
private:
  int getCount() { return mItems.count(); }

public slots:
  XInstance* getInstance(int i);

public:
  virtual ~XInstances();
  XInstances() {}

  QList<XInstance*>* getItems() { return &mItems; }

private:
  QList<XInstance*> mItems;
};

// Class: Instance
// Represents an instance of a Verilog Module or a VHDL Entity.
//
class XInstance : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  void dump();
  virtual ~XInstance();

  XInstance(XToplevelInterface* topi)
  {
    mInterface = NULL;
    mToplevelInterface = topi;
    mParentInst = NULL;
  }

  static bool deserialize(XToplevelInterface* iTop, XInstance* parentInst, const QDomElement& e, XmlErrorHandler* eh);

  bool isVlogGenerateLabel(void) {return getInterfaceType() == "vlog_generate"; }
  
  // Property: ID
  // The String identifier that represents this instance.
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(QString id READ getID)
  QString getID() { return mID; }
  void setID(const QString newVal) { mID=newVal; }

  // Property: locator
  // The String locator of the form <filename>:<linenumber> 
  // that identifies where this instance was instantiated.
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(QString locator READ getLocator)
  QString getLocator() { return mLocator; }
  void setLocator(const QString& newVal) { mLocator=newVal; }

  // Property: interfaceName
  // The String interface name that this is an instance of.
  //
  // Access: *ReadOnly* 
  // See Also:
  // *interface*
  Q_PROPERTY(QString interfaceName READ getInterfaceName)
  QString getInterfaceName() { return mInterfaceName; }
  void setInterfaceName(const QString& newVal) { mInterfaceName=newVal; }

  // Property: iface
  // The <Interface> object represented by this instance.
  //
  // Access: *ReadOnly* 
 // *iface*
  Q_PROPERTY(XInterface* iface READ getInterface)
  XInterface* getInterface() { return mInterface; }
  void setInterface(XInterface* newVal) { mInterface=newVal; }

  // Property: instances
  // The collection of children <Instance> objects underneath this instance.
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(XInstances* instances READ getChildInstances)
  XInstances* getChildInstances() { return &mChildren; }
 
  // Access: *ReadOnly* 
  // *iface type (process generate or instance)*
  Q_PROPERTY(QString ifaceType READ getInterfaceType)
  QString getInterfaceType() { return mInterfaceType; }
  void setInterfaceType(const QString& newVal) { mInterfaceType=newVal; }

  QList<XInstance*>* getChildren() { return mChildren.getItems(); }

  // Property: architectureName
  // The String Architecture name that this is an instance of.
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(QString architectureName READ getArchitectureName)
  QString getArchitectureName() { return mArchitectureName; }
  void setArchitectureName(const QString& newVal) { mArchitectureName=newVal; }

  // Property: ports
  // The colleciton of <InstancePorts> associated with this instance.
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(XInstancePorts* ports READ getPorts)
  XInstancePorts* getPorts() { return &mPorts; }

  // Property: interface
  // The <Interface> object represented by this instance.
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(XInstance* parent READ getParent)
  XInstance* getParent() { return mParentInst; }
  void setParent(XInstance* newVal) { mParentInst=newVal; }

  // Property: fullName
  // The String fully qualified name from the toplevel to this instance
  // seperated by '.'
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(QString fullName READ getFullName)
  QString getFullName();

  // Property: interface
  // The <Interface> object represented by this instance.
  //
  // Access: *ReadOnly* 
  Q_PROPERTY(XParameters* parameters READ getParameters)
  XParameters* getParameters() { return &mParameters; }

private:
  // Helpers for 'getFullName'
  QString getFullNameNormal();
  QString getFullNameLegacy();
  QString constructEscapedName(QStringList& genLabels);

private:
  QString mID;
  QString mLocator;
  QString mInterfaceName;
  QString mArchitectureName;
  QString mInterfaceType;
  XInterface* mInterface;
  XParameters mParameters;
  XInstancePorts mPorts;
  XInstance* mParentInst;
  XInstances mChildren;
  XToplevelInterface* mToplevelInterface;
};


class XInterfaces : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  virtual ~XInterfaces();

  bool deserialize(const QDomElement& e, XmlErrorHandler* eh);
  void dump();

  // Property: count
  // The String interface name that this is an instance of.
  //
  // Access: *ReadOnly* 
  int numInterfaces() const { return mInterfaces.count(); }
  XInterface* getAt(int index) { return mInterfaces[index]; }

  void addInterface(XInterface* iface)
  {
    mInterfaceMap[iface->getName()] = iface;
    mInterfaces.push_back(iface);
  }

  XInterface* findInterface(const QString& name)
  {
    return mInterfaceMap[name];
  }
private:
  QList<XInterface*> mInterfaces;
  QMap<QString, XInterface*> mInterfaceMap;
};

// Class: Libraries
class XLibraries : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  bool deserialize(const QDomElement& e, XmlErrorHandler* eh);
  void dump();
  // Property: count
  // The Integer number of <InstancePort>s in this collection.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(int count READ numLibraries)

  int numLibraries() { return mLibraries.count(); }


public slots:
  // Method: Library getLibrary(index)
  // Returns the *index*th item in the collection.
  //
  // Parameters: 
  //  index - Integer index from 0 to count-1
  //
  // Returns:
  //   <Library> or null if not found.
  XLibrary* getLibrary(int index)
  {
    return getAt(index);
  }
 // Method: Library getLibrary(name)
  // Returns the library matching *name* in the collection.
  //
  // Parameters: 
  //  name - String name of the library
  //
  // Returns:
  //   <Library> or null if not found.
  XLibrary* getLibrary(const QString& libName)
  {
    return findLibrary(libName);
  }

public:
  virtual ~XLibraries();

  XLibrary* getAt(int index)
  {
    return mLibraries[index];
  }

  void addLibrary(XLibrary* iLib)
  {
    mLibraryMap[iLib->getName()] = iLib;
    mLibraries.push_back(iLib);
  }

  XLibrary* findLibrary(const QString& name)
  {
    return mLibraryMap[name];
  }

  QList<XLibrary*>& getLibraries() { return mLibraries; }

private:
  QList<XLibrary*> mLibraries;
  QMap<QString, XLibrary*> mLibraryMap;
};

//
// Class: DesignFiles
// The collection of DesignFiles 
//
class XDesignFiles : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  bool deserialize(const QDomElement& e, XmlErrorHandler* eh);
  void dump();
  virtual ~XDesignFiles();

  // Property: count
  // The Integer number of <DesignFile>s in this collection.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(int count READ numFiles);
  int numFiles() { return mFiles.count(); }
  XDesignFile* getAt(int index)
  {
    return mFiles[index];
  }

public slots:
  // Method: DesignFile getFile(index)
  // Returns the *index*th item in the collection.
  //
  // Parameters: 
  //  index - Integer index from 0 to count-1
  //
  // Returns:
  //   <DesignFile> or null if not found.
  XDesignFile* getFile(int index)
  {
    return getAt(index);
  }

  // Method: DesignFile getFile(name)
  // Returns the *name*ed item in the collection.
  //
  // Parameters: 
  //  name - String name to match
  //
  // Returns:
  //   <DesignFile> or null if not found.
  XDesignFile* getFile(const QString& name)
  {
    return findFile(name);
  }

public:
  void addFile(XDesignFile* iFile)
  {
    mFileMap[iFile->getName()] = iFile;
    mFiles.push_back(iFile);
  }

  XDesignFile* findFile(const QString& name)
  {
    return mFileMap[name];
  }

  QList<XDesignFile*>& getFiles() { return mFiles; }

private:
  QList<XDesignFile*> mFiles;
  QMap<QString, XDesignFile*> mFileMap;
};




// Class: ToplevelInterface
// Contains the collection of instances used by this design root.
class XToplevelInterface : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  void addInstance(XInstance* inst)
  {
     mInstances.getItems()->push_back(inst);
  }
  // Property: name
  // The String name of this interface.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString name READ getName);
  QString getName() { return mName; }
  void setName(const QString& newVal) { mName=newVal; }
  
  // Property: locator
  // The String source locator to this interface of the form <absolutepath>filename:<linenumber>
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString locator READ getLocator);
  QString getLocator() { return mLocator; }
  void setLocator(const QString& newVal) { mLocator=newVal; }

  // Property: library
  // The String name of the library that this interface was compiled into.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString library READ getLibrary);
  QString getLibrary() { return mLibrary; }
  void setLibrary(const QString& newVal) { mLibrary=newVal; }

public slots:
  // Property: instances
  // The collection of <Instance> objects used by this design root.
  //
  // Access:
  // *ReadOnly*
  XInstances* getInstances() { return &mInstances; }
  XInstance* findInstance(const QString& instPath) { return findXInstance(instPath); }
public:
  QList<XInstance*>* getChildren() { return mInstances.getItems(); }

  void dump();
  void fixReferences(XInterfaces* ifaces);
  void fixInstance(XInterfaces* ifaces, XInstance* inst);
  XInstance* findXInstance(const QString& instPath);

private:
  XInstances mInstances;

  QString mName;
  QString mLocator;
  QString mLibrary;
};
//
// Class: TopLevelInterfaces
// The collection of top-level interfaces in this design.
//
class XTopLevelInterfaces : public QObject, protected QScriptable
{
  Q_OBJECT

public:
  bool deserialize(const QDomElement& e, XmlErrorHandler* eh);
  void dump();
  void finalize();
  XToplevelInterface* getAt(int index)
  {
    return mInterfaces[index];
  }
  // Property: count
  // The number of toplevel interfaces in this design.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(int count READ numInterfaces)

  int numInterfaces() { return mInterfaces.count(); }
public slots:
  // Method: Interface getInterface(index)
  // Returns the *index*th item in the collection.
  //
  // Parameters: 
  //  index - Integer index from 0 to count-1
  //
  // Returns:
  //   <Interface> or null if not found.
  XToplevelInterface* getInterface(int index)
  {
    return mInterfaces[index];
  }
  // Method: Interface getInterface(name)
  // Returns the <Interface> matching *name*
  //
  // Parameters: 
  //  name - String name of the interface (case-sensitive)
  //
  // Returns:
  //   <Interface> or null if not found.
public:
  XToplevelInterface* getInterface(const QString& name)
  {
    foreach (XToplevelInterface* iface, mInterfaces)
    {
      if (iface->getName() == name)
        return iface;
    }
    return NULL;
  }

private:
  QString mName;
  QString mLocator;
  QList<XToplevelInterface*> mInterfaces;
};

//
// Class: DesignHierarchy
// The collection of top-level interfaces in this design.
//
class XDesignHierarchy : public QObject, protected QScriptable
{
  Q_OBJECT

  // Property: top
  // The toplevel <Interface> for this design or null
  // if there is none.
  //
  // Access:
  // *ReadOnly*
  // See Also:
  // <Interfaces> interfaces   
  Q_PROPERTY(XToplevelInterface* top READ getTopLevelInterface);

  // Property: interfaces
  // The collection of <Interface>'s used by this design.
  //
  // Access:
  // *ReadOnly*
  // See Also:
  // <Interfaces> Collection   
  Q_PROPERTY(XTopLevelInterfaces* interfaces READ getInterfaces)

public slots:
  XTopLevelInterfaces* getInterfaces()
  {
    return &mTopLevelInterfaces;
  }

public:
  XDesignHierarchy(const QString& xmlHierarchyFile);
  virtual ~XDesignHierarchy();

  void dump();
  XmlErrorHandler* getErrorHandler() { return &mErrorHandler; }
  XTopLevelInterfaces* getToplevelInterfaces() { return &mTopLevelInterfaces; }
  XLibraries* getLibraries() { return &mLibraries; }
  XDesignFiles* getDesignFiles() { return &mDesignFiles; }
  XInterface* findInterface(const QString& ifaceName);
  XInterfaces* getAllInterfaces() { return &mInterfaces; }

  // Property: toplevelInterfaces
  // The collection of <ToplevelInterface>s associated with this design hierarchy.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(XTopLevelInterfaces* toplevelInterfaces READ getToplevelInterfaces)
  // Property: libraries
  // The collection of <Library>'s associated with this design hierarchy.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(XLibraries* libraries READ getLibraries)
   // Property: designFiles
  // The collection of <DesignFile>'s associated with this design hierarchy.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(XDesignFiles* designFiles READ getDesignFiles)

public slots:
  // Method: Interface getInterface(name)
  // Returns the <Interface> matching *name*
  //
  // Parameters: 
  //  name - String name of the interface (case-sensitive)
  //
  // Returns:
  //   <Interface> or null if not found.
  XInterface* find(const QString& ifaceName)
  {
    return findInterface(ifaceName);
  }

  XToplevelInterface* getTopLevelInterface()
  {
    if (mTopLevelInterfaces.numInterfaces() > 0)
      return mTopLevelInterfaces.getInterface(0);
    else
      return NULL;
  }

public:
  static void registerTypes(QScriptEngine* engine);
  static void registerScriptTypes(QMap<QString, const QMetaObject*>& map);
  static void registerIntellisense(JavaScriptAPIs* apis);

private:
  XTopLevelInterfaces mTopLevelInterfaces;
  XInterfaces mInterfaces;
  XLibraries mLibraries;
  XDesignFiles mDesignFiles;
  
private:
  bool parseFile(const QString& xmlFile, XmlErrorHandler* eh);
  XmlErrorHandler mErrorHandler;
};

Q_DECLARE_METATYPE(XDesignHierarchy*);
Q_DECLARE_METATYPE(XTopLevelInterfaces*)
Q_DECLARE_METATYPE(XToplevelInterface*)
Q_DECLARE_METATYPE(XDesignFiles*)
Q_DECLARE_METATYPE(XLibraries*)
Q_DECLARE_METATYPE(XInterfaces*)
Q_DECLARE_METATYPE(XInstance*)
Q_DECLARE_METATYPE(XInstances*)
Q_DECLARE_METATYPE(XLibrary*)
Q_DECLARE_METATYPE(XDesignFile*)
Q_DECLARE_METATYPE(XDefines*)
Q_DECLARE_METATYPE(XDefine*)
Q_DECLARE_METATYPE(XInterface*)
Q_DECLARE_METATYPE(XInterfaceArchitectures*)
Q_DECLARE_METATYPE(XInterfacePorts*)
Q_DECLARE_METATYPE(XInterfaceArchitecture*)
Q_DECLARE_METATYPE(XInterfacePort*)
Q_DECLARE_METATYPE(XInstancePorts*)
Q_DECLARE_METATYPE(XInstancePort*)
Q_DECLARE_METATYPE(XParameters*)
Q_DECLARE_METATYPE(XParameter*)

// Enums
Q_DECLARE_METATYPE(XInterfacePort::InterfacePortDirection)
Q_DECLARE_METATYPE(XDesignFile::DesignFileKind)

#endif
