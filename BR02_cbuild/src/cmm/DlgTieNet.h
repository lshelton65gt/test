#ifndef DLGTIENET_H
#define DLGTIENET_H

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include <QDialog>
#include "ui_DlgTieNet.h"

class DlgTieNet : public QDialog
{
  Q_OBJECT

public:
  DlgTieNet(QWidget *parent = 0);
  ~DlgTieNet();
  const char* getValue();
  void putValue(const char* value);

private:
  Ui::DlgTieNetClass ui;
  UtString mValue;

private slots:
  void on_buttonBox_accepted();
  void on_buttonBox_rejected();

};

#endif // DLGTIENET_H
