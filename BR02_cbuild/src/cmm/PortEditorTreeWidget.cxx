//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "PortEditorTreeWidget.h"
#include "CompWizardTreeNodes.h"
#include "CompWizardPortEditor.h"
#include "ESLPort.h"
#include "ESLXtor.h"

PortEditorTreeWidget::PortEditorTreeWidget(QWidget* parent)
  : QTreeWidget(parent)
{
  mEditor = NULL;
  mLastEditor = NULL;
  mDropSiteItem = NULL;
  mInternalDrag = false;

  CQT_CONNECT(this, itemExpanded(QTreeWidgetItem*), this, itemExpanded(QTreeWidgetItem*));
  CQT_CONNECT(this, itemCollapsed(QTreeWidgetItem*), this, itemCollapsed(QTreeWidgetItem*));
  CQT_CONNECT(this, itemDoubleClicked(QTreeWidgetItem*, int), this, itemDoubleClicked(QTreeWidgetItem*, int));
}

void PortEditorTreeWidget::itemDoubleClicked(QTreeWidgetItem* treeItem, int)
{
  PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(treeItem);
  if (item)
    item->doubleClicked();
}

ESLXtorInstanceConnItem* PortEditorTreeWidget::findXtorInstancePort(QTreeWidgetItem* parent, const QString& xtorPortName)
{
  for (int i=0; i<parent->childCount(); i++)
  {
    ESLXtorInstanceConnItem* xtorInstConnItem = dynamic_cast<ESLXtorInstanceConnItem*>(parent->child(i));
    if (xtorInstConnItem && xtorPortName == xtorInstConnItem->getXtorConn()->getPort()->getName())
        return xtorInstConnItem;
  }
  return NULL;
}


ESLXtorInstanceItem* PortEditorTreeWidget::findXtorInstance(QTreeWidgetItem* parent, const QString& xtorInstName)
{
  for (int i=0; i<parent->childCount(); i++)
  {
    ESLXtorInstanceItem* xtorInstItem = dynamic_cast<ESLXtorInstanceItem*>(parent->child(i));
    if (xtorInstItem && xtorInstName == xtorInstItem->getXtorInstance()->getName())
        return xtorInstItem;
  }
  return NULL;
}


ESLDisconnectItem* PortEditorTreeWidget::findDisconnect(QTreeWidgetItem* parent, const QString& rtlPortName)
{
  for (int i=0; i<parent->childCount(); i++)
  {
    ESLDisconnectItem* disItem = dynamic_cast<ESLDisconnectItem*>(parent->child(i));
    if (disItem && rtlPortName == disItem->getRTLPort()->getName())
        return disItem;
  }
  return NULL;
}


ESLPortItem* PortEditorTreeWidget::findESLPort(QTreeWidgetItem* parent, const QString& eslPortName)
{
  for (int i=0; i<parent->childCount(); i++)
  {
    ESLPortItem* eslPort = dynamic_cast<ESLPortItem*>(parent->child(i));
    if (eslPort && eslPortName == eslPort->getESLPort()->getName())
        return eslPort;
  }
  return NULL;
}

void PortEditorTreeWidget::closeTree()
{
  // Free static data
  ESLPortItem::resetData();
  ESLXtorInstanceParameterItem::resetData();
  ESLXtorInstanceConnItem::resetData();
  ESLParameterItem::resetData();
}


bool PortEditorTreeItem::operator<(const QTreeWidgetItem &o) const
{
  int column = treeWidget()->sortColumn();
  const PortEditorTreeItem& other = dynamic_cast<const PortEditorTreeItem&>(o);

  if (other.mNodeType == PortEditorTreeItem::XtorInstParams || other.mNodeType == PortEditorTreeItem::XtorInstClocksResets)
    return false;

  if (mNodeType == PortEditorTreeItem::XtorInstParams || mNodeType == PortEditorTreeItem::XtorInstClocksResets)
    return true;

  return text(column) < other.text(column);
}


PortEditorTreeItem* UndoData::getParentItem() 
{
  return dynamic_cast<PortEditorTreeItem*>(mTree->itemFromIndex(mParentIndex)); 
}

PortEditorTreeItem* UndoData::getItem()
{
  return dynamic_cast<PortEditorTreeItem*>(mTree->itemFromIndex(mItemIndex)); 
}

void PortEditorTreeWidget::expandChildren(QTreeWidgetItem* item)
{
  if (item)
  {
    expandItem(item);
    for (int i=0; i<item->childCount(); i++)
      expandChildren(item->child(i));
  }
}

void PortEditorTreeWidget::collapseChildren(QTreeWidgetItem* item)
{
  if (item)
  {
    collapseItem(item);
    for (int i=0; i<item->childCount(); i++)
      collapseChildren(item->child(i));
  }
}

void PortEditorTreeWidget::setExpanded(QTreeWidgetItem* item, bool expand)
{
  if (expand)
    expandItem(item);
  else
    collapseItem(item);
}
void PortEditorTreeWidget::sortChildren(QTreeWidgetItem* item)
{
  setUpdatesEnabled(false);
  bool expanded = item->isExpanded();
  collapseItem(item);
  item->sortChildren(0, Qt::AscendingOrder);
  if (expanded)
    expandItem(item);
  setUpdatesEnabled(true);
}


void PortEditorTreeWidget::closeEditor()
{
  if (mLastEditor)
  {
    PortEditorDelegate* de = static_cast<PortEditorDelegate*>(itemDelegate());
    de->commit(mLastEditor);
    mLastEditor = NULL;
  }
}

void PortEditorTreeWidget::itemExpanded(QTreeWidgetItem* treeItem)
{
  PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(treeItem);
  if (item && item->resizeOnExpand())
    header()->resizeSections(QHeaderView::ResizeToContents);
}

void PortEditorTreeWidget::itemCollapsed(QTreeWidgetItem* treeItem)
{
  PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(treeItem);
  if (item && item->resizeOnCollapse())
    header()->resizeSections(QHeaderView::ResizeToContents);
}

// Drag/Drop

// Note: Call base class methods first, otherwise, autoScroll won't work.
void PortEditorTreeWidget::dragEnterEvent(QDragEnterEvent * event)
{
  QTreeWidget::dragEnterEvent(event);

  qDebug() << "dragEnter" << event->mimeData()->text();

  animateDropSite(event->pos());

  event->acceptProposedAction();
}
void PortEditorTreeWidget::dragLeaveEvent(QDragLeaveEvent * event)
{
  QTreeWidget::dragLeaveEvent(event);

  qDebug() << "dragLeave";
  clearAnimation();
  mInternalDrag = false;
}
void PortEditorTreeWidget::dragMoveEvent(QDragMoveEvent * event)
{
  QTreeWidget::dragMoveEvent(event);

  qDebug() << "dragMove";

  animateDropSite(event->pos());

  QTreeWidgetItem* treeItem = itemAt(event->pos().x(), event->pos().y());
  if (treeItem)
    qDebug() << "Dragging over" << treeItem->text(0);

  if (treeItem)
  {
    PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(treeItem);
    if (mInternalDrag)
    {
      if (item->canDropHere(mSelectedItems, event))
        event->accept();
      else
        event->ignore();
    }
    else
    {
      if (item->canDropHere(event->mimeData()->text(), event))
        event->accept();
      else
        event->ignore();
    }
  }
  else
    event->ignore();
}
void PortEditorTreeWidget::dropEvent(QDropEvent * event)
{
  clearAnimation();
  qDebug() << "dropEvent";

  QTreeWidgetItem* treeItem = itemAt(event->pos().x(), event->pos().y());
  if (mInternalDrag && treeItem)
  {
    PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(treeItem);
    if (item)
      item->dropItems(mSelectedItems);
    event->acceptProposedAction();
  }
  else if (treeItem && event->mimeData()->text().length() > 0)
  {
    PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(treeItem);
    if (item)
      item->dropItem(event->mimeData()->text());
    event->acceptProposedAction();
  }
  else
    event->acceptProposedAction();

  mInternalDrag = false;
}
void PortEditorTreeWidget::mousePressEvent(QMouseEvent* event)
{
  QTreeWidget::mousePressEvent(event);

  QTreeWidgetItem* item = itemAt(event->x(), event->y());

  if (item && event->button() == Qt::LeftButton)
  {
    // Immediate drag
    if (!item->isSelected())
    {
      clearSelection();
      qDebug() << "Selecting item";
      //item->setSelected(true);
      setCurrentItem(item);
    }

    mDragStartPos = event->pos();
  }
}
void PortEditorTreeWidget::mouseReleaseEvent(QMouseEvent* event)
{
  qDebug() << "mouseRelease";
  mInternalDrag = false;
  QTreeWidget::mouseReleaseEvent(event);
}

void PortEditorTreeWidget::clearAnimation() 
{
  if (mDropSiteItem != NULL) 
  {
    // Max 10 columns
    for (int i = 0; i < columnCount() && i < DD_MAX_COLUMNS; ++i)
      mDropSiteItem->setBackground(i, mDropSaveBrush[i]);
    mDropSiteItem = NULL;
  }
}

void PortEditorTreeWidget::animateDropSite(const QPoint& pos) 
{
  QTreeWidgetItem* item = itemAt(pos.x(), pos.y());
  
  if (mDropSiteItem != item)
  {
    clearAnimation();
    mDropSiteItem = item;

    if (item != NULL)
    {
      QToolTip::showText(viewport()->mapToGlobal(pos), item->text(0), this);
      for (int i = 0; i < columnCount() && i < DD_MAX_COLUMNS; ++i) 
        mDropSaveBrush[i] = item->background(i);

      QColor dropHighlightColor = QColor(Qt::green);
      if (isItemSelected(item))
        dropHighlightColor = QColor(Qt::darkGreen);

      for (int i = 0; i < columnCount() && i < DD_MAX_COLUMNS; ++i) 
        item->setBackgroundColor(i, dropHighlightColor);
    }
  }
}


void PortEditorTreeWidget::mouseMoveEvent(QMouseEvent* event)
{
  if (!event->buttons() & Qt::LeftButton)
    return;

  if ((event->pos() - mDragStartPos).manhattanLength() < QApplication::startDragDistance())
    return;

  mSelectedItems.clear();

  // Begin drag
  if (selectedItems().count() > 0)
  {
    foreach(QTreeWidgetItem* treeItem, selectedItems())
    {
      PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(treeItem);
      if (item)
        mSelectedItems.append(item);
    }

    if (mSelectedItems.count() > 0)
    {
      mInternalDrag = true;
      qDebug() << "Begin Drag" << selectedItems().count() << "items";
      QDrag* drag = new QDrag(this);
      QMimeData* mimeData = new QMimeData;
      mimeData->setText("SelectedItems");
      drag->setMimeData(mimeData);
      drag->exec(Qt::MoveAction);
    }
  }

  if (!mInternalDrag)
    QTreeWidget::mouseMoveEvent(event);
}

quint32 PortEditorTreeWidget::numSelectedItems()
{
  quint32 numItems = 0;
  foreach (QTreeWidgetItem* treeItem, selectedItems())
  {
    PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(treeItem);
    if (item)
      numItems++;
  } 
  return numItems;
}

quint32 PortEditorTreeWidget::numSelectedItems(PortEditorTreeItem::NodeType nodeType)
{
  quint32 numItems = 0;
  foreach (QTreeWidgetItem* treeItem, selectedItems())
  {
    PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(treeItem);
    if (item && item->getNodeType() == nodeType)
      numItems++;
  } 
  return numItems;
}

quint32 PortEditorTreeWidget::fillSelectedItems(QList<PortEditorTreeItem*>& items, PortEditorTreeItem::NodeType nodeType)
{
  items.clear();
  foreach (QTreeWidgetItem* treeItem, selectedItems())
  {
    PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(treeItem);
    if (item && item->getNodeType() == nodeType)
      items.append(item);
  } 
  return items.count();
}

PortTreeIndex PortEditorTreeItem::getTreeIndex()
{
  PortEditorTreeWidget* tree = dynamic_cast<PortEditorTreeWidget*>(treeWidget());
  return tree->indexFromItem(this);
}

void PortEditorTreeItem::setModified(bool value)
{
  PortEditorTreeWidget* tree = dynamic_cast<PortEditorTreeWidget*>(treeWidget());
  tree->setModified(value);
}

CarbonCfg* PortEditorTreeItem::getCcfg() const
{
  PortEditorTreeWidget* tree = dynamic_cast<PortEditorTreeWidget*>(treeWidget());
  INFO_ASSERT(tree, "Expecting PortEditorTreeWidget");
  return tree->getCcfg();
}

PortEditorTreeWidget* PortEditorTreeItem::getTree() 
{
  return dynamic_cast<PortEditorTreeWidget*>(treeWidget());
}

QUndoStack* PortEditorTreeItem::getUndoStack() const
{
  PortEditorTreeWidget* tree = dynamic_cast<PortEditorTreeWidget*>(treeWidget());
  INFO_ASSERT(tree, "Expecting PortEditorTreeWidget");
  return tree->getUndoStack();
}

