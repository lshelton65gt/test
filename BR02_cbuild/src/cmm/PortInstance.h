#ifndef __PORTINSTANCE_H__
#define __PORTINSTANCE_H__

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "Scripting.h"

#include "Ports.h"

#include "BlockInstance.h"

// File: PortInstances, PortInstance
// The PortInstances collection and the PortInstance object
//
// Class: PortInstance
// Represents the instance of a <Port> within a <BlockInstance>
//
// See Also:
// <BlockInstance.portInstances>
//
class PortInstance : public QObject
{
  Q_OBJECT

  // Property: port
  // <Port> represented by this instance.
  Q_PROPERTY(QObject* port READ getPort)
  
  // Property: block
  // <Block> Underlying that this port was defined on.
  Q_PROPERTY(QObject* block READ getBlock)
  
  // Property: name
  // String Elaborated name of this port.  Any references to ^(instanceID) will have already
  // been expanded at this time.
  Q_PROPERTY(QString name READ getName)

  // Property: instanceID
  // String instance identifier for this port.
  Q_PROPERTY(QString instanceID READ getInstanceID)

  Q_PROPERTY(QString mappedPortName READ getMappedPortName)

public:
  PortInstance(Port* port, BlockInstance* blockInst, const QString& instanceID);

  Port* getPort() { return mPort; }
  BlockInstance* getBlockInstance() { return mBlockInstance; }
  Block* getBlock() { return mBlockInstance->getBlock(); }
  QString getInstanceID() const { return mInstanceID; }
  QString getName();
  QString getUnelaboratedName();
  QString getMappedPortName() { return mPortName; }
  void setMappedPortName(const QString& newVal) { mPortName=newVal; }

private:
  void elaboratePortName();

private:
  Port* mPort;
  BlockInstance* mBlockInstance;
  QString mInstanceID;
  QString mElaboratedName;
  QString mPortName;
};

// Class: PortInstances
// Collection Class of PortInstances
class PortInstances : public QObject
{
  Q_OBJECT

  // Property: count
  // 
  // Access:
  // *ReadOnly*
  //
  Q_PROPERTY(int count READ getCount)

public:
  PortInstances()
  {
  }
  
  int getCount() const { return mPortInstances.count(); }
  
  void addPortInstance(PortInstance* pi)
  {
    // for the script to be able to access the object,
    // force the cast to QObject*
    QVariant v = qVariantFromValue((QObject*)pi);
    QString name = pi->getPort()->getName().replace(" ", "");
    setProperty(name.toAscii(), v); // this_toascii_ok

    mPortInstances.append(pi);
  }

  PortInstance* getAt(int index)
  {
    return mPortInstances[index];
  }

  void clear() { mPortInstances.clear(); }

public slots:
// Method: PortInstance getPortInstance(name)
// Returns the <PortInstance> matching *name*
//
// Parameters: 
//  name - String name of the port
//
// Returns:
//   <PortInstance> or null if not found.
  QObject* getPortInstance(const QString& name)
  {
    foreach (PortInstance* portInst, mPortInstances)
    {
      if (portInst->getPort()->getName() == name)
        return portInst;
    }
    return NULL;
  }
// Method: PortInstance getPortInstance(index)
// Returns the <PortInstance> located at *index*
//
// Parameters: 
//  index - Integer from 0 to <count>-1
//
// Returns:
//   <PortInstance> or null if not found.

  QObject* getPortInstance(int index)
  {
    return mPortInstances[index];
  }

private:
  QList<PortInstance*> mPortInstances;
};

#endif
