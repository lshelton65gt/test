//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include "DlgSelectRTL.h"

DlgSelectRTL::DlgSelectRTL(CarbonCfg* cfg, SelectionType selType, bool singleSelection, QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);
  mCfg = cfg;
  mSingleSelection = singleSelection;

  if (mSingleSelection)
  {
    ui.labelChoose->setText("Choose a single RTL Port");   
    ui.treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
  }
  else
  {
    ui.labelChoose->setText("Choose RTL Port(s)");   
    ui.treeWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
  }

  ui.treeWidget->setSortingEnabled(false);

  for (UInt32 i=0; i<mCfg->numESLPorts(); i++)
  {
    CarbonCfgESLPort* eslPort = mCfg->getESLPort(i);
    CarbonCfgRTLPort* rtlPort = eslPort->getRTLPort();

    bool portsMatch = false;
    switch (selType)
    {
    case AllPorts: 
      portsMatch = true; break;
    case InputPorts: 
      portsMatch = rtlPort->getType() == eCarbonCfgRTLInput; 
      // We only allow inputs not already tied to parameters
      if (portsMatch && (rtlPort->isTiedToParam() || rtlPort->isConnectedToXtor()))
        portsMatch = false;
      break;
    case OutputPorts:
      portsMatch = rtlPort->getType() == eCarbonCfgRTLOutput; 
      break;
    }

    if (portsMatch)
    {
      QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidget);
      item->setText(0, rtlPort->getName());
    }
  }

  ui.treeWidget->sortByColumn(0, Qt::AscendingOrder);
  ui.treeWidget->setSortingEnabled(true);
  on_treeWidget_itemSelectionChanged();
}

DlgSelectRTL::~DlgSelectRTL()
{
}

void DlgSelectRTL::on_buttonBox_accepted()
{
  mSelectedPorts.clear();

  foreach (QTreeWidgetItem* item, ui.treeWidget->selectedItems())
    mSelectedPorts.append(item->text(0));
  accept();
}

void DlgSelectRTL::on_buttonBox_rejected()
{
  reject();
}

void DlgSelectRTL::on_treeWidget_itemSelectionChanged()
{
  QPushButton* okButton = ui.buttonBox->button(QDialogButtonBox::Ok);
  okButton->setEnabled(ui.treeWidget->selectedItems().count() > 0);
}

