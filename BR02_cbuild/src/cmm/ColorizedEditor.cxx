//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "ColorizedEditor.h"
#include "qscilexerverilog.h"
#include <Qsci/qscilexervhdl.h>

ColorizedEditor::ColorizedEditor(QWidget *parent)
: QsciScintilla(parent)
{
  mLanguage = Verilog;
  setMarginWidth(0, "1000");
  setMarginLineNumbers(0, true);
}


void ColorizedEditor::setLanguage(LanguageKind lang)
{ 
  mLanguage = lang;

  if (lang == Verilog)
  {
    QsciLexerVerilog* vlex = new QsciLexerVerilog();
    setLexer(vlex);
  }
  else if (lang == VHDL)
  {
    QsciLexerVHDL* vlex = new QsciLexerVHDL();
    setLexer(vlex);
  }
}
ColorizedEditor::~ColorizedEditor()
{
}
