//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "MemEditorTreeWidget.h"
#include "MemEditorTreeNodes.h"
#include "CompWizardMemEditor.h"
#include "MemEditorCommands.h"
#include "FileButtonBrowser.h"

BlocksRootActions::BlocksRootActions(MemEditorTreeWidget* tree) : QObject(tree)
{
  mTree = tree;

  mAddBlock = new QAction("Add Block", mTree);
  CQT_CONNECT(mAddBlock, triggered(), this, actionAddBlock());
}

void BlocksRootActions::actionAddBlock()
{
  QList<MemEditorTreeItem*> items;
  mTree->fillSelectedItems(items, MemEditorTreeItem::Blocks);

  foreach(MemEditorTreeItem* item, items)
  {
    BlocksRootItem* blocksItem = dynamic_cast<BlocksRootItem*>(item);
    
    mTree->getUndoStack()->push(new CmdAddESLMemoryBlock(blocksItem, blocksItem->getMemory()));

    break; // single selection
  }
}

QAction* BlocksRootActions::getAction(Action kind)
{
  switch (kind)
  {
  case AddBlock:          return mAddBlock; break;
  }
  return mAddBlock;
}



quint64 ConvertStringToUInt64(const QString& v)
{
  quint64 value;
  bool ok;

  // Try hex?
  if (v.toLower().startsWith("0x"))
    value = v.toULongLong(&ok, 16);
  else
    value = v.toULongLong(&ok, 10);

  if (!ok) // last ditch, try hex without the prefix
    value = v.toULongLong(&ok, 16);

  return value;
}

qint64 ConvertStringToSInt64(const QString& input)
{
  QString v = input.trimmed().toLower();

  bool ok = false;
  qint64 val = v.toLongLong(&ok);

  if(!ok) 
    val = v.toLongLong(&ok, 16);
  
  return val;
}

quint32 ConvertStringToUInt32(const QString& v)
{
  quint32 value;
  bool ok;

  // Try hex?
  if (v.toLower().startsWith("0x"))
    value = v.toUInt(&ok, 16);
  else
    value = v.toUInt(&ok, 10);

  if (!ok) // last ditch, try hex without the prefix
    value = v.toUInt(&ok, 16);

  return value;
}

QWidget* ESLMemory::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemory::Columns col = ESLMemory::Columns(colIndex);

  if (col == ESLMemory::colNAME)
  {
    QLineEdit* editor = new QLineEdit(parent);
    CExpressionValidator* validator = new ESLMemoryValidator(mMemory, mMemory->getName(), editor);
    editor->setValidator(validator);
    return editor;
  }
  else if (col == ESLMemory::colWIDTH)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    QIntValidator* validator = new QIntValidator(parent);
    validator->setBottom(1);
    validator->setTop(512);
    lineEdit->setValidator(validator);
    return lineEdit;
  }

  return NULL;
}

void ESLMemory::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemory::Columns col = ESLMemory::Columns(colIndex);
  if (col == ESLMemory::colNAME)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldName = model->data(modelIndex).toString();
      QString newName = lineEdit->text();
      if (oldName != newName)
      {
        model->setData(modelIndex, newName);
        getUndoStack()->push(new CmdRenameESLMemory(this, mMemory, oldName, newName));     
      }
    }
  }
  else if (col == ESLMemory::colWIDTH)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldValue = model->data(modelIndex).toString();
      QString newValue = lineEdit->text();
      if (oldValue != newValue)
      {
        model->setData(modelIndex, newValue);
        quint32 ov = ConvertStringToUInt32(oldValue);
        quint32 nv = ConvertStringToUInt32(newValue);
        getUndoStack()->push(new CmdChangeESLMemoryWidth(this, mMemory, ov, nv));     
      }
    }
  }
}


QWidget* ESLMemoryBigEndian::createEditor(const MemEditorDelegate* dg, QWidget* parent, int colIndex)
{
  ESLMemoryBigEndian::Columns col = ESLMemoryBigEndian::Columns(colIndex);

  if (col == ESLMemoryBigEndian::colVALUE)
  {
    QComboBox* cbox = new QComboBox(parent);
    cbox->addItem("true");
    cbox->addItem("false");
    if (mMemory->getBigEndian())
      cbox->setCurrentIndex(cbox->findText("true"));
    else
      cbox->setCurrentIndex(cbox->findText("false"));

    CQT_CONNECT(cbox, currentIndexChanged(int), dg, currentIndexChanged(int));

    return cbox;
  }

  return NULL;
}

void ESLMemoryBigEndian::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryBigEndian::Columns col = ESLMemoryBigEndian::Columns(colIndex);
  if (col == ESLMemoryBigEndian::colVALUE)
  {
    QComboBox* cbox = qobject_cast<QComboBox*>(editor);
    if (cbox)
    {
      QString textValue = cbox->currentText().toLower();
     
      bool oldValue = mMemory->getBigEndian();
      bool newValue;
      if (textValue == "true")
        newValue = true;
      else
        newValue = false;
     
      model->setData(modelIndex, cbox->currentText());
      
      if (oldValue != newValue)
        getUndoStack()->push(new CmdChangeESLMemoryBigEndian(this, mMemory, oldValue, newValue));     
    }
  }
}

void ESLMemory::Delete::undo()
{
  qDebug() << "Undo Delete Memory" << text();
 
  ESLMemory* pMem = new ESLMemory(getTree(), mMemory);

  if (mIsSubComponent)
  {
    mMemory->getParent()->insertMemoryUndo(mPos, mMemory);
    mSubComponent->insertChild(mChildIndex, pMem);
  }
  else
  {
    mCfg->insertMemoryUndo(mPos, mMemory);
    MemoriesRootItem* memsRoot = getTree()->getEditor()->getMemoriesRoot();
    memsRoot->insertChild(mTreePos, pMem);
  }

  getTree()->expandChildren(pMem);
  pMem->setSelected(true);
  getTree()->scrollToItem(pMem);
  getTree()->setModified(false);
}

bool ESLMemory::Delete::isSubComponent()
{
  qDebug() << "parent subcomps" << mMemory->getParent()->numSubComponents();

  bool topLevel = mMemory->getParent()->isTopLevel();
  
  return topLevel ? false : true;
}
void ESLMemory::Delete::redo()
{
  qDebug() << "Delete Memory" << text();

  mTreePos = -1;
  mIsSubComponent = isSubComponent();
  mChildIndex = -1;
  mSubComponent = NULL;

  QTreeWidgetItem* item = getItem();
  item->setSelected(false);
 
  if (mIsSubComponent)
  {
    mChildIndex = item->parent()->indexOfChild(item);
    QTreeWidgetItem* parentItem = item->parent();
    
    // Remove subcomponent memory
    mPos = mMemory->getParent()->removeMemoryUndo(mMemory);
    mSubComponent = dynamic_cast<ESLMemorySubComponent*>(parentItem);

    item->parent()->removeChild(item);   
  }
  else
  {
    mPos = mCfg->removeMemoryUndo(mMemory);
    MemoriesRootItem* memsRoot = getTree()->getEditor()->getMemoriesRoot();
    mTreePos = memsRoot->indexOfChild(item);
    memsRoot->removeChild(item);
  }

  getTree()->setModified(true);
}

MemUndoData* ESLMemory::deleteItem()
{
  QTreeWidgetItem* item = getItem();
  bool isDeletable = mMemory->GetEditFlags().testFlag(CcfgFlags::EditMemoryDeletions);

  if (isDeletable && item->isSelected())
  {
    ESLMemory::Delete* undoData = new ESLMemory::Delete();
    MemEditorTreeWidget* tree = dynamic_cast<MemEditorTreeWidget*>(treeWidget());

    undoData->setText(mMemory->getName());
    undoData->mMemory = mMemory;    
    undoData->mComponentName = mMemory->getParent()->getCompName();
    undoData->mCfg = getCcfg();
    undoData->setTreeData(tree, this);

    return undoData;
  }
  return NULL;
}


bool ESLMemoryMaxAddress::check(CarbonCfgMemory* mem, bool validate)
{
  if (getWizard() && !getWizard()->autoCheckEnabled())
    return false;

  MemoriesRootItem* memsRoot = getTree()->getEditor()->getMemoriesRoot();
  if (memsRoot)
  {
    ESLMemory* eslMem = findESLMemory(getTree(), mem);
    if (eslMem)
      return eslMem->check(validate);
  }
  return false;
}

void GeneralRootItem::populateGeneral(CarbonCfgMemory* mem)
{
  new ESLMemoryMaxAddress(getItem(), mem);
 // this is not yet implemeted in carmgr
 // new ESLMemoryBigEndian(getItem(), mem);
  setExpanded(true);
}

void MemoriesRootItem::populateMemories(CarbonCfg* cfg)
{
  qDebug() << "start populating memories....";

  mCfg = cfg;

  for (UInt32 i=0; i<mCfg->numMemories(); i++)
  {
    CarbonCfgMemory* mem = mCfg->getMemory(i);
    new ESLMemory(getItem(), mem);
  }

  for (UInt32 sc=0; sc<mCfg->numSubComponents(); sc++)
  {
    CarbonCfg* sub = mCfg->getSubComponent(sc);   
    qDebug() << "start populating sub-Component" << sub->getCompName();
    new ESLMemorySubComponent(getItem(), sub);
    qDebug() << "finished populating sub-Component" << sub->getCompName();
  }

  qDebug() << "finished populating memories....";

  setExpanded(true);
}

void BlocksRootItem::multiSelectContextMenu(QMenu& menu)
{
 // Add the action
  QAction* action = getActions()->getAction(BlocksRootActions::AddBlock);
  INFO_ASSERT(action, "Action should not be null");
  menu.addAction(action);

}

ESLMemoryBlock* BlocksRootItem::addBlock(CarbonCfgMemoryBlock* block)
{
  ESLMemoryBlock* item = new ESLMemoryBlock(getItem(), mMemory, block);
  setExpanded(true);
  return item;
}

bool BlocksRootItem::check(bool validate)
{
  if (getWizard() && !getWizard()->autoCheckEnabled())
    return false;

  bool errorsFound = false;
  if (getTree())
  {
    if (!mCheckInProgress)
    {
      mCheckInProgress = true;
      for (int ci=0; ci<childCount(); ci++)
      {
        QTreeWidgetItem* childItem = child(ci);
        MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(childItem);
        if (item->getNodeType() == MemEditorTreeItem::MemBlock)
        {
          ESLMemoryBlock* eslMemBlk = dynamic_cast<ESLMemoryBlock*>(item);
          if (eslMemBlk->check(validate))
            errorsFound = true;
        }
      }
      mCheckInProgress = false;
    }
  }
  return errorsFound;
}


ESLMemoryBlock* BlocksRootItem::findBlock(const QString& blockName)
{
  for (int ci=0; ci<childCount(); ci++)
  {
    QTreeWidgetItem* childItem = child(ci);
    MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(childItem);
    if (item->getNodeType() == MemEditorTreeItem::MemBlock)
    {
      ESLMemoryBlock* eslMemBlk = dynamic_cast<ESLMemoryBlock*>(item);
      if (eslMemBlk->getName() == blockName)
        return eslMemBlk;
    }
  }

  return NULL;
}

void BlocksRootItem::populateBlocks(CarbonCfgMemory* mem)
{
  for (UInt32 i=0; i<mem->numMemoryBlocks(); i++)
  {
    CarbonCfgMemoryBlock* block = mem->getMemoryBlock(i);
    qDebug() << "start adding block" << block->getName();
    new ESLMemoryBlock(getItem(), mem, block);
    qDebug() << "finished adding block" << block->getName();
  }
  //setExpanded(true);
}

void ESLMemoryBlockLocations::populateLocations(CarbonCfgMemoryBlock* block)
{
  for (UInt32 i=0; i<block->numLocs(); i++)
  {
    qDebug() << "adding block location" << i << "block" << block->getName();
    CarbonCfgMemoryLoc* loc = block->getLoc(i);
    if (loc->castRTL())
      new ESLMemoryBlockLocationRTL(getItem(), mMemory, mBlock, loc);
    else if (loc->castUser())
      new ESLMemoryBlockLocationUser(getItem(), mMemory, mBlock, loc);
    else if (loc->castPort())
      new ESLMemoryBlockLocationPort(getItem(), mMemory, mBlock, loc);
    qDebug() << "finished block location" << i << "block" << block->getName();
  }
}


QWidget* ESLMemoryBlock::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemoryBlock::Columns col = ESLMemoryBlock::Columns(colIndex);

  if (col == ESLMemoryBlock::colNAME)
  {
    QLineEdit* editor = new QLineEdit(parent);
    ESLMemoryBlockValidator* validator = new ESLMemoryBlockValidator(mMemory, mBlock->GetName(), editor);
    editor->setValidator(validator);
    return editor;
  }
 
  return NULL;
}

void ESLMemoryBlock::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryBlock::Columns col = ESLMemoryBlock::Columns(colIndex);
  if (col == ESLMemoryBlock::colNAME)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldName = model->data(modelIndex).toString();
      QString newName = lineEdit->text();
      if (oldName != newName)
      {
        model->setData(modelIndex, newName);
        getUndoStack()->push(new CmdChangeESLMemoryBlockName(this, mMemory, mBlock, oldName, newName));     
      }
    }
  }
}

QWidget* ESLMemoryBlockBase::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemoryBlockBase::Columns col = ESLMemoryBlockBase::Columns(colIndex);

  if (col == ESLMemoryBlockBase::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    HexDecValidator* validator = new HexDecValidator(this);
    lineEdit->setValidator(validator);
    return lineEdit;
  }
  return NULL;
}

void ESLMemoryBlockBase::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryBlockBase::Columns col = ESLMemoryBlockBase::Columns(colIndex);
  if (col == ESLMemoryBlockBase::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldString = model->data(modelIndex).toString();
      QString newString = lineEdit->text();
      if (oldString != newString)
      {
        quint64 oldValue = ConvertStringToUInt64(oldString);
        quint64 newValue = ConvertStringToUInt64(newString);
        model->setData(modelIndex, newString);
        getUndoStack()->push(new CmdChangeESLMemoryBlockBaseAddress(this, mMemory, mBlock, oldValue, newValue));     
      }
    }
  }
}

QWidget* ESLMemoryBlockSize::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemoryBlockSize::Columns col = ESLMemoryBlockSize::Columns(colIndex);

  if (col == ESLMemoryBlockSize::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    HexDecValidator* validator = new HexDecValidator(this);
    lineEdit->setValidator(validator);
    return lineEdit;
  }
 
  return NULL;
}

void ESLMemoryBlockSize::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryBlockSize::Columns col = ESLMemoryBlockSize::Columns(colIndex);
  if (col == ESLMemoryBlockSize::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldString = model->data(modelIndex).toString();
      QString newString = lineEdit->text();
      if (oldString != newString)
      {
        quint64 oldValue = ConvertStringToUInt64(oldString);
        quint64 newValue = ConvertStringToUInt64(newString);
        model->setData(modelIndex, newString);
        getUndoStack()->push(new CmdChangeESLMemoryBlockSize(this, mMemory, mBlock, oldValue, newValue));
      }
    }
  }
}

MemUndoData* ESLMemoryBlock::deleteItem()
{
  bool isDeletable = mMemory->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockDeletions);
  QTreeWidgetItem* item = getItem();
  if (isDeletable && item->isSelected())
  {
    ESLMemoryBlock::Delete* undoData = new ESLMemoryBlock::Delete();
    MemEditorTreeWidget* tree = dynamic_cast<MemEditorTreeWidget*>(treeWidget());

    undoData->setText(mBlock->getName());
    undoData->mMemory = mMemory;
    undoData->mBlock = mBlock;
    undoData->mCfg = getCcfg();
    undoData->setTreeData(tree, this);

    return undoData;
  }
  return NULL;
}

MemUndoData* ESLMemoryBlockLocationRTL::deleteItem()
{
  bool isDeletable = mMemory->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockLocationDeletions);
  QTreeWidgetItem* item = getItem();
  if (isDeletable && item->isSelected())
  {
    ESLMemoryBlockLocationRTL::Delete* undoData = new ESLMemoryBlockLocationRTL::Delete();
    MemEditorTreeWidget* tree = dynamic_cast<MemEditorTreeWidget*>(treeWidget());
    undoData->setText(mBlock->getName());
    undoData->mMemory = mMemory;
    undoData->mBlock = mBlock;
    undoData->mLoc = mLocRTL;
    undoData->mCfg = getCcfg();
    undoData->setTreeData(tree, this);

    return undoData;
  }

  return NULL;
}



QWidget* ESLMemoryBlockLocDisplayAttrStartWordOffset::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrStartWordOffset::Columns col = ESLMemoryBlockLocDisplayAttrStartWordOffset::Columns(colIndex);

  if (col == ESLMemoryBlockLocDisplayAttrStartWordOffset::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    HexDecValidator* validator = new HexDecValidator(this);
    lineEdit->setValidator(validator);
    return lineEdit;
  }
 
  return NULL;
}

void ESLMemoryBlockLocDisplayAttrStartWordOffset::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrStartWordOffset::Columns col = ESLMemoryBlockLocDisplayAttrStartWordOffset::Columns(colIndex);
  if (col == ESLMemoryBlockLocDisplayAttrStartWordOffset::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldString = model->data(modelIndex).toString();
      QString newString = lineEdit->text();
      if (oldString != newString)
      {
        quint32 oldValue = ConvertStringToUInt32(oldString);
        quint32 newValue = ConvertStringToUInt32(newString);
        model->setData(modelIndex, newString);
        getUndoStack()->push(new CmdChangeESLMemoryBlockLocDisplayAttrStartWordOffset(this, mMemory, mBlock, mLoc, oldValue, newValue));
      }
    }
  }
}

QWidget* ESLMemoryBlockLocDisplayAttrEndWordOffset::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrEndWordOffset::Columns col = ESLMemoryBlockLocDisplayAttrEndWordOffset::Columns(colIndex);

  if (col == ESLMemoryBlockLocDisplayAttrEndWordOffset::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    HexDecValidator* validator = new HexDecValidator(this);
    lineEdit->setValidator(validator);
    return lineEdit;
  }
 
  return NULL;
}

void ESLMemoryBlockLocDisplayAttrEndWordOffset::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrEndWordOffset::Columns col = ESLMemoryBlockLocDisplayAttrEndWordOffset::Columns(colIndex);
  if (col == ESLMemoryBlockLocDisplayAttrEndWordOffset::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldString = model->data(modelIndex).toString();
      QString newString = lineEdit->text();
      if (oldString != newString)
      {
        quint32 oldValue = ConvertStringToUInt32(oldString);
        quint32 newValue = ConvertStringToUInt32(newString);
        model->setData(modelIndex, newString);
        getUndoStack()->push(new CmdChangeESLMemoryBlockLocDisplayAttrEndWordOffset(this, mMemory, mBlock, mLoc, oldValue, newValue));
      }
    }
  }
}

QWidget* ESLMemoryBlockLocDisplayAttrLSB::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrLSB::Columns col = ESLMemoryBlockLocDisplayAttrLSB::Columns(colIndex);

  if (col == ESLMemoryBlockLocDisplayAttrLSB::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    HexDecValidator* validator = new HexDecValidator(this);
    lineEdit->setValidator(validator);
    return lineEdit;
  }
 
  return NULL;
}

void ESLMemoryBlockLocDisplayAttrLSB::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrLSB::Columns col = ESLMemoryBlockLocDisplayAttrLSB::Columns(colIndex);
  if (col == ESLMemoryBlockLocDisplayAttrLSB::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldString = model->data(modelIndex).toString();
      QString newString = lineEdit->text();
      if (oldString != newString)
      {
        quint32 oldValue = ConvertStringToUInt32(oldString);
        quint32 newValue = ConvertStringToUInt32(newString);
        model->setData(modelIndex, newString);
        getUndoStack()->push(new CmdChangeESLMemoryBlockLocDisplayAttrLSB(this, mMemory, mBlock, mLoc, oldValue, newValue));
      }
    }
  }
}

QWidget* ESLMemoryBlockLocDisplayAttrMSB::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrMSB::Columns col = ESLMemoryBlockLocDisplayAttrMSB::Columns(colIndex);

  if (col == ESLMemoryBlockLocDisplayAttrMSB::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    HexDecValidator* validator = new HexDecValidator(this);
    lineEdit->setValidator(validator);
    return lineEdit;
  }
 
  return NULL;
}

void ESLMemoryBlockLocDisplayAttrMSB::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrMSB::Columns col = ESLMemoryBlockLocDisplayAttrMSB::Columns(colIndex);
  if (col == ESLMemoryBlockLocDisplayAttrMSB::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldString = model->data(modelIndex).toString();
      QString newString = lineEdit->text();
      if (oldString != newString)
      {
        quint32 oldValue = ConvertStringToUInt32(oldString);
        quint32 newValue = ConvertStringToUInt32(newString);
        model->setData(modelIndex, newString);
        getUndoStack()->push(new CmdChangeESLMemoryBlockLocDisplayAttrMSB(this, mMemory, mBlock, mLoc, oldValue, newValue));
      }
    }
  }
}

// RTL Versions


QWidget* ESLMemoryBlockLocDisplayAttrStartWordOffsetRTL::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrStartWordOffsetRTL::Columns col = ESLMemoryBlockLocDisplayAttrStartWordOffsetRTL::Columns(colIndex);

  if (col == ESLMemoryBlockLocDisplayAttrStartWordOffsetRTL::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    HexDecValidator* validator = new HexDecValidator(this);
    lineEdit->setValidator(validator);
    return lineEdit;
  }
 
  return NULL;
}

void ESLMemoryBlockLocDisplayAttrStartWordOffsetRTL::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrStartWordOffsetRTL::Columns col = ESLMemoryBlockLocDisplayAttrStartWordOffsetRTL::Columns(colIndex);
  if (col == ESLMemoryBlockLocDisplayAttrStartWordOffsetRTL::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldString = model->data(modelIndex).toString();
      QString newString = lineEdit->text();
      if (oldString != newString)
      {
        quint32 oldValue = ConvertStringToUInt32(oldString);
        quint32 newValue = ConvertStringToUInt32(newString);
        model->setData(modelIndex, newString);
        getUndoStack()->push(new CmdChangeESLMemoryBlockLocDisplayAttrStartWordOffsetRTL(this, mMemory, mBlock, mLoc, oldValue, newValue));
      }
    }
  }
}

QWidget* ESLMemoryBlockLocDisplayAttrEndWordOffsetRTL::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrEndWordOffsetRTL::Columns col = ESLMemoryBlockLocDisplayAttrEndWordOffsetRTL::Columns(colIndex);

  if (col == ESLMemoryBlockLocDisplayAttrEndWordOffsetRTL::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    HexDecValidator* validator = new HexDecValidator(this);
    lineEdit->setValidator(validator);
    return lineEdit;
  }
 
  return NULL;
}

void ESLMemoryBlockLocDisplayAttrEndWordOffsetRTL::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrEndWordOffsetRTL::Columns col = ESLMemoryBlockLocDisplayAttrEndWordOffsetRTL::Columns(colIndex);
  if (col == ESLMemoryBlockLocDisplayAttrEndWordOffsetRTL::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldString = model->data(modelIndex).toString();
      QString newString = lineEdit->text();
      if (oldString != newString)
      {
        quint32 oldValue = ConvertStringToUInt32(oldString);
        quint32 newValue = ConvertStringToUInt32(newString);
        model->setData(modelIndex, newString);
        getUndoStack()->push(new CmdChangeESLMemoryBlockLocDisplayAttrEndWordOffsetRTL(this, mMemory, mBlock, mLoc, oldValue, newValue));
      }
    }
  }
}

QWidget* ESLMemoryBlockLocDisplayAttrLSBRTL::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrLSBRTL::Columns col = ESLMemoryBlockLocDisplayAttrLSBRTL::Columns(colIndex);

  if (col == ESLMemoryBlockLocDisplayAttrLSBRTL::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    HexDecValidator* validator = new HexDecValidator(this);
    lineEdit->setValidator(validator);
    return lineEdit;
  }
 
  return NULL;
}

void ESLMemoryBlockLocDisplayAttrLSBRTL::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrLSBRTL::Columns col = ESLMemoryBlockLocDisplayAttrLSBRTL::Columns(colIndex);
  if (col == ESLMemoryBlockLocDisplayAttrLSBRTL::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldString = model->data(modelIndex).toString();
      QString newString = lineEdit->text();
      if (oldString != newString)
      {
        quint32 oldValue = ConvertStringToUInt32(oldString);
        quint32 newValue = ConvertStringToUInt32(newString);
        model->setData(modelIndex, newString);
        getUndoStack()->push(new CmdChangeESLMemoryBlockLocDisplayAttrLSBRTL(this, mMemory, mBlock, mLoc, oldValue, newValue));
      }
    }
  }
}

QWidget* ESLMemoryBlockLocDisplayAttrMSBRTL::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrMSBRTL::Columns col = ESLMemoryBlockLocDisplayAttrMSBRTL::Columns(colIndex);

  if (col == ESLMemoryBlockLocDisplayAttrMSBRTL::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    HexDecValidator* validator = new HexDecValidator(this);
    lineEdit->setValidator(validator);
    return lineEdit;
  }
 
  return NULL;
}

void ESLMemoryBlockLocDisplayAttrMSBRTL::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryBlockLocDisplayAttrMSBRTL::Columns col = ESLMemoryBlockLocDisplayAttrMSBRTL::Columns(colIndex);
  if (col == ESLMemoryBlockLocDisplayAttrMSBRTL::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldString = model->data(modelIndex).toString();
      QString newString = lineEdit->text();
      if (oldString != newString)
      {
        quint32 oldValue = ConvertStringToUInt32(oldString);
        quint32 newValue = ConvertStringToUInt32(newString);
        model->setData(modelIndex, newString);
        getUndoStack()->push(new CmdChangeESLMemoryBlockLocDisplayAttrMSBRTL(this, mMemory, mBlock, mLoc, oldValue, newValue));
      }
    }
  }
}

// Memories

bool MemoryDropItem::canDropHere(const QString& nets, QDragMoveEvent* ev)
{
  CcfgHelper ccfg(getCcfg());

  QStringList netList = nets.split("\n");
  
  int goodNets = 0;
  foreach(QString net, netList)
  {
    qDebug() << "Dragging net" << net;
    UtString uNetName; uNetName << net;
    const CarbonDBNode* node = carbonDBFindNode(getCcfg()->getDB(), uNetName.c_str());
    bool isMemory = false;
    if (node && carbonDBIs2DArray(getTree()->getCcfg()->getDB(), node) && !carbonDBIsContainedByComposite(getTree()->getCcfg()->getDB(), node))
      isMemory = true;

    if (isMemory)
      goodNets++;
    else
      qDebug() << "Non-memories are not allowed:" << net;   

    break; // single net only
  }

  if (goodNets == netList.count())
    ev->setDropAction(Qt::CopyAction);
  else if (goodNets > 0)
    ev->setDropAction(Qt::MoveAction);
  else
  {
    QPoint pos = ev->pos();
    QString msg = QString("Net is not a memory which is not allowed.");
    QToolTip::showText(getTree()->viewport()->mapToGlobal(pos), msg, getTree());
    ev->setDropAction(Qt::IgnoreAction);
    return false;
  }

  return true;
}

bool MemoryDropItem::dropItem(const QString& memoryName)
{
  qDebug() << "dropped" << memoryName;
  getUndoStack()->push(new CmdAddESLMemory(getTree()->getEditor()->getMemoriesRoot(), getCcfg(), memoryName));     
  return true;
}




bool MemoriesRootItem::canDropHere(const QString& nets, QDragMoveEvent* ev)
{
  CcfgHelper ccfg(getCcfg());

  QStringList netList = nets.split("\n");
  
  int goodNets = 0;
  foreach(QString net, netList)
  {
    qDebug() << "Dragging net" << net;
    UtString uNetName; uNetName << net;
    const CarbonDBNode* node = carbonDBFindNode(getCcfg()->getDB(), uNetName.c_str());
    bool isMemory = false;
    if (node && carbonDBIs2DArray(getTree()->getCcfg()->getDB(), node) && !carbonDBIsContainedByComposite(getTree()->getCcfg()->getDB(), node))
      isMemory = true;

    if (isMemory)
      goodNets++;
    else
      qDebug() << "Non-memories are not allowed:" << net;   

    break; // single net only
  }

  if (goodNets == netList.count())
    ev->setDropAction(Qt::CopyAction);
  else if (goodNets > 0)
    ev->setDropAction(Qt::MoveAction);
  else
  {
    QPoint pos = ev->pos();
    QString msg = QString("Net is not a memory which is not allowed.");
    QToolTip::showText(getTree()->viewport()->mapToGlobal(pos), msg, getTree());
    ev->setDropAction(Qt::IgnoreAction);
    return false;
  }

  return true;
}

bool MemoriesRootItem::dropItem(const QString& memoryName)
{
  qDebug() << "dropped" << memoryName;
  getUndoStack()->push(new CmdAddESLMemory(this, getCcfg(), memoryName));     
  return true;
}

// Block RTL Path

QWidget* ESLMemoryBlockLocationRTL::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemoryBlockLocationRTL::Columns col = ESLMemoryBlockLocationRTL::Columns(colIndex);

  if (col == ESLMemoryBlockLocationRTL::colVALUE)
    return new QLineEdit(parent);
 
  return NULL;
}

bool ESLMemoryBlockLocationRTL::canDropHere(const QString& nets, QDragMoveEvent* ev)
{
  CcfgHelper ccfg(getCcfg());

  bool canAdd = mMemory->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockAdditions);
  if (!canAdd)
    return false;

  QStringList netList = nets.split("\n");
  
  int goodNets = 0;
  foreach(QString net, netList)
  {
    qDebug() << "Dragging net" << net;
    UtString uNetName; uNetName << net;
    const CarbonDBNode* node = carbonDBFindNode(getCcfg()->getDB(), uNetName.c_str());
    bool isMemory = false;
    if (node && carbonDBIs2DArray(getTree()->getCcfg()->getDB(), node) && !carbonDBIsContainedByComposite(getTree()->getCcfg()->getDB(), node))
      isMemory = true;

    if (isMemory)
      goodNets++;
    else
      qDebug() << "Non-memories are not allowed:" << net;    

    break; // single net only
  }

  if (goodNets == netList.count())
    ev->setDropAction(Qt::CopyAction);
  else if (goodNets > 0)
    ev->setDropAction(Qt::MoveAction);
  else
  {
    QPoint pos = ev->pos();
    QString msg = QString("Net is not a memory which is not allowed.");
    QToolTip::showText(getTree()->viewport()->mapToGlobal(pos), msg, getTree());
    ev->setDropAction(Qt::IgnoreAction);
    return false;
  }

  return true;
}

bool ESLMemoryBlockLocationRTL::dropItem(const QString& nets)
{
  CcfgHelper ccfg(getCcfg());

  QStringList netList = nets.split("\n");
  
  QString newName = netList.first().trimmed();

  QString oldName = mLocRTL->getPath();

  getUndoStack()->push(new CmdChangeESLMemoryBlockLocationRTL(this, mMemory, mBlock, mLoc->castRTL(), oldName, newName));     
 
  return true;
}


void ESLMemoryBlockLocationRTL::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryBlockLocationRTL::Columns col = ESLMemoryBlockLocationRTL::Columns(colIndex);
  if (col == ESLMemoryBlockLocationRTL::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldName = model->data(modelIndex).toString();
      QString newName = lineEdit->text();

      UtString uNetName; uNetName << newName;
      const CarbonDBNode* node = carbonDBFindNode(getTree()->getCcfg()->getDB(), uNetName.c_str());
      bool isMemory = false;
      if (node && carbonDBIs2DArray(getTree()->getCcfg()->getDB(), node) && !carbonDBIsContainedByComposite(getTree()->getCcfg()->getDB(), node))
        isMemory = true;

      if (isMemory && (oldName != newName))
      {
        model->setData(modelIndex, newName);
        getUndoStack()->push(new CmdChangeESLMemoryBlockLocationRTL(this, mMemory, mBlock, mLoc->castRTL(), oldName, newName));     
      }
      else if (!isMemory)
      {
        QString msg = QString("No such memory '%1' was found").arg(newName);
        QMessageBox::information(editor, "No such memory", msg , QMessageBox::Ok);
      }
    }
  }
}

ESLMemory* findParentMemory(MemEditorTreeItem* node)
{
  do
  {
    if (node->getNodeType() == MemEditorTreeItem::ESLMemoryNode)
      return dynamic_cast<ESLMemory*>(node);

    QTreeWidgetItem* parentItem = node->parent();

    node = dynamic_cast<MemEditorTreeItem*>(parentItem);
  }
  while(node != NULL);

  return NULL;
}

bool CheckBlockLocation(QTreeWidgetItem*, MemEditorTreeItem* thisNode, CarbonCfg* ccfg, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc, bool validate)
{
  bool errorsFound = false;

 // qDebug() << "Node Type:" << thisNode->getNodeType();

  if (thisNode->getTree())
  {
    qDebug() << "checking" << thisNode->text(0) << thisNode->text(1) << loc->messageState() << "validate" << validate;

    QString parentName;
    if (thisNode->parent() != NULL)
      parentName = thisNode->parent()->text(0);

    QString smsg = QString("Checking %1 %2 %3 %4 %5")
      .arg(parentName)
      .arg(mem->getName())
      .arg(block->getName())
      .arg(thisNode->text(0))
      .arg(thisNode->text(1));

    theApp->statusBar()->showMessage(smsg, 500);

    if (validate)
      block->check(mem->getParent(), mem->getName(), mem->getMaxAddrs(), ccfg->getDB());

    //ESLMemory* eslMem = findParentMemory(thisNode);

    //if (eslMem)
    //{
    //  BlocksRootItem* blocksRoot = eslMem->getBlocks();
    //  if (blocksRoot) // are we constructing the tree now?
    //  {
    //    ESLMemoryBlock* memBlock = blocksRoot->findBlock(block->getName());
    //    if (memBlock->check(mem, block))
    //      errorsFound = true;
    //  }
    //}

    if (loc->messageState())
    {
      QString msgs = loc->getMessage();
      thisNode->showErrors(0, 1, msgs, 0);
      errorsFound = true;
    }
    else
      thisNode->clearErrors(0, 1, 0);
  }

  return errorsFound;
}

bool CheckBlock(QTreeWidgetItem*, MemEditorTreeItem* thisNode, CarbonCfg* ccfg, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, bool validate)
{
  bool errorsFound = false;

  QApplication::setOverrideCursor(Qt::WaitCursor);
  //qDebug() << "Node Type:" << thisNode->getNodeType();

  if (thisNode->getTree())
  {
    if (validate)
      block->check(mem->getParent(), mem->getName(), mem->getMaxAddrs(), ccfg->getDB());
    
    errorsFound = block->messageState();

    qDebug() << "checking block" << thisNode->text(0) << thisNode->text(1) << block->messageState();

    ESLMemory* eslMem = findParentMemory(thisNode);
    if (eslMem)
    {
      BlocksRootItem* blocksRoot = eslMem->getBlocks();
      if (blocksRoot) // are we constructing the tree now?
      {
        ESLMemoryBlock* memBlock = blocksRoot->findBlock(block->getName());
        if (memBlock->check(mem, block, validate))
          errorsFound = true;
      }
    }
  }
  QApplication::restoreOverrideCursor();

  return errorsFound;
}



bool ESLMemoryBlockLocations::canDropHere(const QString& nets, QDragMoveEvent* ev)
{
  CcfgHelper ccfg(getCcfg());

  bool canAdd = mMemory->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockAdditions);
  if (!canAdd)
    return false;

  QStringList netList = nets.split("\n");
  
  int goodNets = 0;
  foreach(QString net, netList)
  {
    qDebug() << "Dragging net" << net;
    UtString uNetName; uNetName << net;
    const CarbonDBNode* node = carbonDBFindNode(getCcfg()->getDB(), uNetName.c_str());
    bool isMemory = false;
    if (node && carbonDBIs2DArray(getTree()->getCcfg()->getDB(), node) && !carbonDBIsContainedByComposite(getTree()->getCcfg()->getDB(), node))
      isMemory = true;

    if (isMemory)
      goodNets++;
    else
      qDebug() << "Non-memories are not allowed:" << net;    

    break; // single net only
  }

  if (goodNets == netList.count())
    ev->setDropAction(Qt::CopyAction);
  else if (goodNets > 0)
    ev->setDropAction(Qt::MoveAction);
  else
  {
    QPoint pos = ev->pos();
    QString msg = QString("Net is not a memory which is not allowed.");
    QToolTip::showText(getTree()->viewport()->mapToGlobal(pos), msg, getTree());
    ev->setDropAction(Qt::IgnoreAction);
    return false;
  }

  return true;
}

bool ESLMemoryBlockLocations::dropItem(const QString& nets)
{
  CcfgHelper ccfg(getCcfg());

  QStringList netList = nets.split("\n");
  
  QString memName = netList.first().trimmed();

  getUndoStack()->push(new CmdAddESLMemoryBlockLocationRTL(this, mMemory, mBlock, memName));     
 
  return true;
}

bool ESLMemoryBlockLocations::check(bool validate)
{
  if (getWizard() && !getWizard()->autoCheckEnabled())
    return false;

  bool errorsFound = false;

  if (!mCheckInProgress)
  {
    mCheckInProgress = true;

    if (validate)
    {
      for (int ci=0; ci<childCount(); ci++)
      {
        QTreeWidgetItem* childItem = child(ci);
        MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(childItem);
        if (item->getNodeType() == MemEditorTreeItem::MemBlockLocationRTL)
        {
          ESLMemoryBlockLocationRTL* rtlItem = dynamic_cast<ESLMemoryBlockLocationRTL*>(item);
          if (rtlItem->check(validate))
            errorsFound = true;
        }
      }
    }

    ESLMemory* mem = findESLMemory(getTree(), mMemory);
    if (mem)
      mem->showHasErrors(errorsFound);

    mCheckInProgress = false;
  }

  return errorsFound;
}

bool ESLMemoryBlockLocationRTL::check(bool validate)
{
  if (getWizard() && !getWizard()->autoCheckEnabled())
    return false;

  bool errorsFound = false;

  qDebug() << "checking RTL Location" << text(0) << text(1);

  if (mRTLAttr && mRTLAttr->check(validate))
    errorsFound = true;

  if (mDisplayAttr && mDisplayAttr->check(validate))
    errorsFound = true;

  return errorsFound;
}

ESLMemoryBlockLocationRTL* ESLMemoryBlockLocations::findRTLLocation(CarbonCfgMemoryLocRTL* loc)
{
  // walk children and look for RTL Location(s)
  for (int ci=0; ci<childCount(); ci++)
  {
    QTreeWidgetItem* childItem = child(ci);
    MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(childItem);
    if (item->getNodeType() == MemEditorTreeItem::MemBlockLocationRTL)
    {
      ESLMemoryBlockLocationRTL* rtlItem = dynamic_cast<ESLMemoryBlockLocationRTL*>(item);
      if (rtlItem->getLoc() == loc)
        return rtlItem;
    }
  }
  return NULL;
}

ESLMemoryBlockLocationRTL* ESLMemoryBlockLocations::addRTLLocation(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc)
{
  if (loc->castRTL())
  { 
    return new ESLMemoryBlockLocationRTL(getItem(), mem, block, loc);
  }

  return NULL;
}

QWidget* ESLMemoryMaxAddress::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemoryMaxAddress::Columns col = ESLMemoryMaxAddress::Columns(colIndex);

  if (col == ESLMemoryMaxAddress::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    HexDecValidator* validator = new HexDecValidator(this);
    lineEdit->setValidator(validator);
    return lineEdit;
  }
 
  return NULL;
}

void ESLMemoryMaxAddress::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryMaxAddress::Columns col = ESLMemoryMaxAddress::Columns(colIndex);
  if (col == ESLMemoryMaxAddress::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldString = model->data(modelIndex).toString();
      QString newString = lineEdit->text();
      if (oldString != newString)
      {
        quint32 oldValue = ConvertStringToUInt32(oldString);
        quint32 newValue = ConvertStringToUInt32(newString);
        model->setData(modelIndex, newString);
        getUndoStack()->push(new CmdChangeESLMemoryMaxAddress(this, mMemory, oldValue, newValue));
      }
    }
  }
}

QWidget* ESLMemoryDisassembly::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemoryDisassembly::Columns col = ESLMemoryDisassembly::Columns(colIndex);

  if (col == ESLMemoryDisassembly::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);   
    return lineEdit;
  }

  return NULL;
}

void ESLMemoryDisassembly::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryDisassembly::Columns col = ESLMemoryDisassembly::Columns(colIndex);
  
  if (col == ESLMemoryDisassembly::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldValue = model->data(modelIndex).toString();
      QString newValue = lineEdit->text();
      if (oldValue != newValue)
      {
        model->setData(modelIndex, newValue);
        getUndoStack()->push(new CmdChangeESLMemoryDisassembly(this, mMemory, oldValue, newValue));     
      }
    }
  }
}

QWidget* ESLMemoryDisassembler::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemoryDisassembler::Columns col = ESLMemoryDisassembler::Columns(colIndex);

  if (col == ESLMemoryDisassembler::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);   
    return lineEdit;
  }

  return NULL;
}

void ESLMemoryDisassembler::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemoryDisassembler::Columns col = ESLMemoryDisassembler::Columns(colIndex);
  
  if (col == ESLMemoryDisassembler::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldValue = model->data(modelIndex).toString();
      QString newValue = lineEdit->text();
      if (oldValue != newValue)
      {
        model->setData(modelIndex, newValue);
        getUndoStack()->push(new CmdChangeESLMemoryDisassembler(this, mComponent, oldValue, newValue));     
      }
    }
  }
}

bool MemoriesRootItem::check(bool validate)
{
  if (getWizard() && !getWizard()->autoCheckEnabled())
    return false;

  bool errorsFound = false;

  if (getTree())
  {
    for (int ci=0; ci<childCount(); ci++)
    {
      QTreeWidgetItem* childItem = child(ci);
      MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(childItem);
      if (item->getNodeType() == MemEditorTreeItem::ESLMemoryNode)
      {
        ESLMemory* eslMem = dynamic_cast<ESLMemory*>(item);
        if (eslMem->check(validate))
          errorsFound = true;
      }
      else if (item->getNodeType() == MemEditorTreeItem::MemorySubComponent)
      {
        ESLMemorySubComponent* subComp = dynamic_cast<ESLMemorySubComponent*>(item);
        if (subComp->check(validate))
          errorsFound = true;
      }
    }
  }
  return errorsFound;
}

QWidget* ESLMemorySubComponent::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLMemorySubComponent::Columns col = ESLMemorySubComponent::Columns(colIndex);

  if (col == ESLMemorySubComponent::colNAME)
  {
    QLineEdit* editor = new QLineEdit(parent);
    CExpressionValidator* validator = new ESLComponentValidator(getCcfg(), getSubComponent()->getCompName(), editor);
    editor->setValidator(validator);
    return editor;
  }
  return NULL;
}

void ESLMemorySubComponent::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLMemorySubComponent::Columns col = ESLMemorySubComponent::Columns(colIndex);
  if (col == ESLMemorySubComponent::colNAME)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldName = model->data(modelIndex).toString();
      QString newName = lineEdit->text();
      if (oldName != newName)
      {
        UtString nn;
        nn << newName;
        if (getCcfg()->findSubComponent(nn.c_str()) == NULL)
        {
          model->setData(modelIndex, newName);
          getUndoStack()->push(new CmdChangeESLMemorySubComponentName(this, mSubComponent, oldName, newName));     
        }
        else
        {
          QString errMessage = QString("SubComponent with name '%1' already exists, ignoring change.").arg(newName);
          QMessageBox::critical(NULL, MODELSTUDIO_TITLE, errMessage);
        }
      }
    }
  }
}

void ESLMemorySubComponent::deleteItem(QList<MemUndoData*>* undoItems)
{
  QTreeWidgetItem* item = getItem();
  
  bool isDeletable = mSubComponent->GetEditFlags().testFlag(CcfgFlags::EditComponentDeletions);

  if (item->isSelected() && isDeletable)
  {
    ESLMemorySubComponent::Delete* undoData = new ESLMemorySubComponent::Delete();
    MemEditorTreeWidget* tree = dynamic_cast<MemEditorTreeWidget*>(treeWidget());

    undoData->setText(mSubComponent->getCompName());
    undoData->mSubComponent = mSubComponent;
    undoData->mSubComponentName = mSubComponent->getCompName();
    undoData->mCfg = getCcfg();
    undoData->setTreeData(tree, this);
    
    undoItems->append(undoData);
  }
}

bool ESLMemorySubComponent::dropItems(const QList<MemEditorTreeItem*>& items)
{
  int numOperations = 0;

  UndoCommandFrame* frame = new UndoCommandFrame(getTree(), NULL);

  foreach(MemEditorTreeItem* item, items)
  {
    bool canAccept = false;

    // Moving a memory 
    if (item->getNodeType() == MemEditorTreeItem::ESLMemoryNode)
    {
      ESLMemory* memNode = dynamic_cast<ESLMemory*>(item);
      if (memNode->isSubComponent())
      {
        // make sure it's not from ourself
        CarbonCfgMemory* m = memNode->getMemory();
        if (findSubComponentMemory(m->getName()) == NULL)
        {
          numOperations++;
          canAccept = true;
          ESLMemorySubComponent* srcSubComponent = NULL;
          MemEditorTreeItem* memParent = dynamic_cast<MemEditorTreeItem*>(item->parent());
          srcSubComponent = dynamic_cast<ESLMemorySubComponent*>(memParent);
          INFO_ASSERT(srcSubComponent, "Expecting parent of ESLMemory to be a SubComponent");

          CmdMoveSubComponentMemoryToSubComponentMemory* cm 
              = new CmdMoveSubComponentMemoryToSubComponentMemory(srcSubComponent, memNode, this, memNode->getMemory(), 0);
          frame->addCommand(cm);
        }
        else
          qDebug() << "cannot drop onto itself";
      }
      else
      {
        numOperations++;
        CmdMoveMemoryToSubComponent* cm = new CmdMoveMemoryToSubComponent(memNode, this, memNode->getMemory(), 0);
        frame->addCommand(cm);
        canAccept = true;
      }
    }
  }

  if (numOperations > 0)
    getUndoStack()->push(frame);
  else
    delete frame;


  return true;
}


bool ESLMemorySubComponent::canDropHere(const QList<MemEditorTreeItem*>& items, QDragMoveEvent*)
{
  bool canAccept = false;
  foreach(MemEditorTreeItem* item, items)
  {
    // Moving a memory 
    if (item->getNodeType() == MemEditorTreeItem::ESLMemoryNode)
    {
      ESLMemory* memNode = dynamic_cast<ESLMemory*>(item);
      if (memNode->isSubComponent())
      {
        qDebug() << "src memory is subcomponent";
        // make sure it's not from ourself
        CarbonCfgMemory* m = memNode->getMemory();
        if (findSubComponentMemory(m->getName()) == NULL)
        {
          canAccept = true;
        }
        else
          qDebug() << "cannot drop onto itself";
      }
      else
      {
        qDebug() << "src memory is normal memory";
        canAccept = true;
      }
    }
  }
  return canAccept;
}

bool ESLMemorySubComponent::canDropHere(const QString& nets, QDragMoveEvent* ev)
{
  CcfgHelper ccfg(getCcfg());

  QStringList netList = nets.split("\n");
  
  // Is this is locked Component?
  bool canAdd = mSubComponent->GetEditFlags().testFlag(CcfgFlags::EditComponentAdditions);
  if (!canAdd)
    return false;

  int goodNets = 0;
  foreach(QString net, netList)
  {
    qDebug() << "Dragging net" << net;
    UtString uNetName; uNetName << net;
    const CarbonDBNode* node = carbonDBFindNode(getCcfg()->getDB(), uNetName.c_str());
    bool isMemory = false;
    if (node && carbonDBIs2DArray(getTree()->getCcfg()->getDB(), node) && !carbonDBIsContainedByComposite(getTree()->getCcfg()->getDB(), node))
      isMemory = true;

    if (isMemory)
      goodNets++;
    else
      qDebug() << "Non-memories are not allowed:" << net;   

    break; // single net only
  }

  if (goodNets == netList.count())
    ev->setDropAction(Qt::CopyAction);
  else if (goodNets > 0)
    ev->setDropAction(Qt::MoveAction);
  else
  {
    QPoint pos = ev->pos();
    QString msg = QString("Net is not a memory which is not allowed.");
    QToolTip::showText(getTree()->viewport()->mapToGlobal(pos), msg, getTree());
    ev->setDropAction(Qt::IgnoreAction);
    return false;
  }

  return true;
}

bool ESLMemorySubComponent::dropItem(const QString& memoryName)
{
  qDebug() << "dropped" << memoryName << "onto subcomponent" << getName();

  getUndoStack()->push(new CmdAddSubComponentESLMemory(this, getCcfg(), memoryName));     
  return true;
}

QWidget* ESLSystemAddressESLPortMapping::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLSystemAddressESLPortMapping::Columns col = ESLSystemAddressESLPortMapping::Columns(colIndex);

  if (col == ESLSystemAddressESLPortMapping::colBASEADDRESS)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    SignedInt64Validator* validator = new SignedInt64Validator(parent);   
    lineEdit->setValidator(validator);
    return lineEdit;
  }

  return NULL;
}

void ESLSystemAddressESLPortMapping::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLSystemAddressESLPortMapping::Columns col = ESLSystemAddressESLPortMapping::Columns(colIndex);
  if (col == ESLSystemAddressESLPortMapping::colBASEADDRESS)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {

      QString oldValue = model->data(modelIndex).toString();
      QString newValue = lineEdit->text();
      if (oldValue != newValue)
      {
        qint64 newAddr = ConvertStringToSInt64(newValue);
        qint64 oldAddr = ConvertStringToSInt64(oldValue);
        QString hexValue = QString("0x%1").arg(QString().setNum(newAddr, 16));
        model->setData(modelIndex, hexValue);
        getUndoStack()->push(new CmdChangeSystemAddressBaseAddress(this, getCcfg(), mMemory, oldAddr, newAddr));
      }
    }
  }
}
//
// Dialog based editor
//
QWidget* ESLMemorySystemAddressMapping::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  if (colIndex == ESLMemorySystemAddressMapping::colMAPPINGS)
  {
    int numDebugXtors = 0;
    for(quint32 i=0; i<getCcfg()->numXtorInstances(); i++)
    {
      CarbonCfgXtorInstance* xinst = getCcfg()->getXtorInstance(i);
      if (xinst->getType()->hasWriteDebug())
      {
        numDebugXtors++;
        break;
      }
    }

    // We must have at least one Debuggable Xtor
    if (numDebugXtors > 0)
    {
      DlgSystemAddressMapping dlg(parent, getCcfg(), mMemory);
      if (dlg.exec() == QDialog::Accepted && dlg.isModified())
      {
        QList<_AddressMapping> mappings;
        for (int i=0; i<dlg.numMappings(); i++)
        {
          const _AddressMapping& am = dlg.getMapping(i);
          mappings.append(am);
        }
        getUndoStack()->push(new CmdChangeSystemAddressMappings(this, getCcfg(), mMemory, mappings));
      }
    }
    else // No need for dialog, warn user
    {
     QMessageBox::warning(parent, APP_NAME, "No Transactor Ports supporting Debuggable Access\nYou must first add a Transactor port before mapping.");
    }
  }
  return NULL;
}

CarbonCfgMemInitType ESLMemorySystemAddressMapping::parseType(const QString& sType)
{
  if (sType == "None")
    return eCarbonCfgMemInitNone;
  else if (sType == "Program Preload")
    return eCarbonCfgMemInitProgPreload;
  else if (sType == "Readmem Initialization")
    return eCarbonCfgMemInitReadmem;

  return eCarbonCfgMemInitNone;
}


QWidget* ESLProgPreloadOffset::createEditor(const MemEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLProgPreloadOffset::Columns col = ESLProgPreloadOffset::Columns(colIndex);

  if (col == ESLProgPreloadOffset::colVALUE)
  {
    CHexSpinBox* sbox = new CHexSpinBox(parent);  
    sbox->setSingleStep(0x10000);
    sbox->setEnabled(true);
    DynBitVector baseAddrMax(64);
    baseAddrMax.set();
    sbox->setMinimum(0);
    sbox->setMaximum(baseAddrMax);
    DynBitVector bv(64, (mMemory->getBaseAddr() * mMemory->getMAU()));
    sbox->setValue(bv);
    return sbox;
  }
 
  return NULL;
}
void ESLProgPreloadOffset::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLProgPreloadOffset::Columns col = ESLProgPreloadOffset::Columns(colIndex);
  if (col == ESLProgPreloadOffset::colVALUE)
  {
    CHexSpinBox* sbox = qobject_cast<CHexSpinBox*>(editor);
    if (sbox)
    {
      DynBitVector value;
      if (sbox->getValue(&value))
      {
        UInt64 oldActualValue = mMemory->getBaseAddr();
        // The Base Addr is really a "minimum access unit address" so convert that from bytes before save
        UInt64 newActualValue = value.llvalue() / mMemory->getMAU();
          
        QString newValue = QString("0x%1").arg(newActualValue,0,16);
        QString oldValue = model->data(modelIndex).toString();
 
        if (oldActualValue != newActualValue)
        { 
          model->setData(modelIndex, newValue);
          getUndoStack()->push(new CmdChangeMemoryProgOffset(this, mMemory, oldActualValue, newActualValue));
        }
      }
    }
  }
}

QWidget* ESLReadmemKind::createEditor(const MemEditorDelegate* dg, QWidget* parent, int colIndex)
{
  ESLReadmemKind::Columns col = ESLReadmemKind::Columns(colIndex);

  if (col == ESLReadmemKind::colKIND)
  {
    QComboBox* combo = new QComboBox(parent);

    CarbonCfgReadmemType readmemType = mMemory->getReadmemType();

    combo->addItem("readmemh");
    combo->addItem("readmemb");
   
    switch(readmemType)
    {
    case eCarbonCfgReadmemh:
      combo->setCurrentIndex(combo->findText("readmemh"));
      break;
    case eCarbonCfgReadmemb:
      combo->setCurrentIndex(combo->findText("readmemb"));
      break;
    }

    CQT_CONNECT(combo, currentIndexChanged(int), dg, currentIndexChanged(int));
   
    return combo;
  }
 
  return NULL;
}

CarbonCfgReadmemType ESLReadmemKind::parseType(const QString& sType)
{
  if (sType == "readmemh")
    return eCarbonCfgReadmemh;
  else if (sType == "readmemb")
    return eCarbonCfgReadmemb;
  
  return eCarbonCfgReadmemh;
}

void ESLReadmemKind::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLReadmemKind::Columns col = ESLReadmemKind::Columns(colIndex);
  if (col == ESLReadmemKind::colKIND)
  {
    QComboBox* combo = qobject_cast<QComboBox*>(editor);
    if (combo)
    {
      QString oldValue = model->data(modelIndex).toString();
      QString newValue = combo->currentText();

      if (oldValue != newValue)
      {
        CarbonCfgReadmemType oldType = parseType(oldValue);
        CarbonCfgReadmemType newType = parseType(newValue);

        QString textValue = combo->currentText();     
        model->setData(modelIndex, combo->currentText());
        getUndoStack()->push(new CmdChangeReadmemKind(this, mMemory, oldType, newType));
      }
    }
  }
}

QWidget* ESLReadmemFile::createEditor(const MemEditorDelegate* dg, QWidget* parent, int colIndex)
{
  ESLReadmemFile::Columns col = ESLReadmemFile::Columns(colIndex);

  if (col == ESLReadmemFile::colVALUE)
  {
    FileButtonBrowser* lineEdit = new FileButtonBrowser(parent);

    lineEdit->setText(mMemory->getInitFile());

    CQT_CONNECT(lineEdit, valueChanged(), dg, valueChanged());

    return lineEdit;
  }
 
  return NULL;
}

void ESLReadmemFile::setModelData(const MemEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLReadmemFile::Columns col = ESLReadmemFile::Columns(colIndex);
  if (col == ESLReadmemFile::colVALUE)
  {
    FileButtonBrowser* lineEdit = qobject_cast<FileButtonBrowser*>(editor);
    if (lineEdit)
    {
      QString oldName = model->data(modelIndex).toString();
      QString newName = lineEdit->text();
      if (oldName != newName)
      {
        model->setData(modelIndex, newName);
        getUndoStack()->push(new CmdChangeReadmemFile(this, mMemory, oldName, newName));  
        getTree()->header()->resizeSections(QHeaderView::ResizeToContents);
      }
    }
  }
}


// Walk up from this node to find the memory
ESLMemory* findESLMemory(MemEditorTreeItem* node)
{
  do
  {
    if (node->getNodeType() == MemEditorTreeItem::ESLMemoryNode)
      return dynamic_cast<ESLMemory*>(node);

    QTreeWidgetItem* parentItem = node->parent();

    node = dynamic_cast<MemEditorTreeItem*>(parentItem);
  }
  while(node != NULL);

  return NULL;
}



// Walk down the tree looking for the matching ESLMemory
ESLMemory* findESLMemory(MemEditorTreeWidget* tree, CarbonCfgMemory* mem)
{
  CarbonCfg* cfg = mem->getParent();
  QString compName = cfg->getCompName();
  QString memName = mem->getName();

  // A memory could be child of memsRoot (directly)
  // or it could be a child of a subcomponent
  MemoriesRootItem* memsRoot = tree->getEditor()->getMemoriesRoot();
  for (int ci=0; ci<memsRoot->childCount(); ci++)
  {
    QTreeWidgetItem* childItem = memsRoot->child(ci);
    MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(childItem);
    if (item->getNodeType() == MemEditorTreeItem::ESLMemoryNode)
    {
      ESLMemory* eslMem = dynamic_cast<ESLMemory*>(item);
      if (eslMem && eslMem->getName() == mem->getName() && compName == eslMem->getMemory()->getParent()->getCompName())
        return eslMem;
    }
    else if (item->getNodeType() == MemEditorTreeItem::MemorySubComponent)
    {
      ESLMemorySubComponent* subComp = dynamic_cast<ESLMemorySubComponent*>(item);
      if (subComp && subComp->getName() == compName)
        return subComp->findSubComponentMemory(mem);
    }
  }

  return NULL;
}

// Walk down the tree looking for the matching ESLMemory
ESLMemory* findESLMemory(MemEditorTreeWidget* tree, const QString& compName, const QString& memName)
{
  // A memory could be child of memsRoot (directly)
  // or it could be a child of a subcomponent
  MemoriesRootItem* memsRoot = tree->getEditor()->getMemoriesRoot();
  for (int ci=0; ci<memsRoot->childCount(); ci++)
  {
    QTreeWidgetItem* childItem = memsRoot->child(ci);
    MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(childItem);
    if (item->getNodeType() == MemEditorTreeItem::ESLMemoryNode)
    {
      ESLMemory* eslMem = dynamic_cast<ESLMemory*>(item);
      if (eslMem && eslMem->getName() == memName && compName == eslMem->getMemory()->getParent()->getCompName())
        return eslMem;
    }
    else if (item->getNodeType() == MemEditorTreeItem::MemorySubComponent)
    {
      ESLMemorySubComponent* subComp = dynamic_cast<ESLMemorySubComponent*>(item);
      if (subComp && subComp->getName() == compName)
        return subComp->findSubComponentMemory(subComp->findSubComponentMemory(memName));
    }
  }

  return NULL;
}

CarbonCfgCompCustomCode* CustomCodesRootNode::findCompCustomCode(CarbonCfgCustomCodePosition pos, CarbonCfgCompCustomCodeSection sec)
{
  if (mComponent)
  {
    for(quint32 i=0; i<mComponent->numCustomCodes(); i++)
    {
      CarbonCfgCustomCode* cc = mComponent->getCustomCode(i);
      CarbonCfgCompCustomCode* code = cc->castComp();
      if (code && code->getPosition() == pos && code->getSection() == sec)
        return code;
    }
  }
  return NULL;
}

CarbonCfgMemoryCustomCode* CustomCodesRootNode::findMemoryCustomCode(CarbonCfgCustomCodePosition pos, CarbonCfgMemoryCustomCodeSection sec)
{
  if (mMemory)
  {
    for(quint32 i=0; i<mMemory->numCustomCodes(); i++)
    {
      CarbonCfgCustomCode* cc = mBlock->getCustomCode(i);
      CarbonCfgMemoryCustomCode* code = cc->castMemory();
      if (code && code->getPosition() == pos && code->getSection() == sec)
        return code;
    }
  }
  return NULL;
}

CarbonCfgMemoryBlockCustomCode* CustomCodesRootNode::findMemoryBlockCustomCode(CarbonCfgCustomCodePosition pos, CarbonCfgMemoryBlockCustomCodeSection sec)
{
  if (mBlock)
  {
    for(quint32 i=0; i<mBlock->numCustomCodes(); i++)
    {
      CarbonCfgCustomCode* cc = mBlock->getCustomCode(i);
      CarbonCfgMemoryBlockCustomCode* code = cc->castMemoryBlock();
      if (code && code->getPosition() == pos && code->getSection() == sec)
        return code;
    }
  }
  return NULL;
}

void CustomCodesRootNode::createMemoryBlockCustomCodeNodes(MemEditorTreeItem* parent, CarbonCfgMemoryBlockCustomCodeSection sect)
{
  setEditable(mMemory->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockCustomCodes));

  CarbonCfgMemoryBlockCustomCode* preCc = findMemoryBlockCustomCode(eCarbonCfgPre, sect);
  new MemoryBlockCustomCodeNode(parent, sect, mMemory, mBlock, preCc, CustomCodesRootNode::MemoryBlock, eCarbonCfgPre);

  CarbonCfgMemoryBlockCustomCode* postCc = findMemoryBlockCustomCode(eCarbonCfgPost, sect);
  new MemoryBlockCustomCodeNode(parent, sect, mMemory, mBlock, postCc, CustomCodesRootNode::MemoryBlock, eCarbonCfgPost);
}

MemEditorTreeItem* CustomCodesRootNode::createMemoryBlockCustomCodeNode(const QString& name, CarbonCfgMemoryBlockCustomCodeSection sect)
{
  MemEditorTreeItem* parent = new MemEditorTreeItem(MemEditorTreeItem::TextNode, getItem());
  parent->setText(0, name);
  createMemoryBlockCustomCodeNodes(parent, sect);
  return parent;
}

void CustomCodesRootNode::createCompCustomCodeNodes(MemEditorTreeItem* parent, CarbonCfgCompCustomCodeSection sect)
{
  setEditable(mComponent->GetEditFlags().testFlag(CcfgFlags::EditComponentCustomCodes));

  CarbonCfgCompCustomCode* preCc = findCompCustomCode(eCarbonCfgPre, sect);
  new ComponentCustomCodeNode(parent, sect, mComponent, preCc, CustomCodesRootNode::Component, eCarbonCfgPre);

  CarbonCfgCompCustomCode* postCc = findCompCustomCode(eCarbonCfgPost, sect);
  new ComponentCustomCodeNode(parent, sect, mComponent, postCc, CustomCodesRootNode::Component, eCarbonCfgPost);
}


void CustomCodesRootNode::createMemoryCustomCodeNodes(MemEditorTreeItem* parent, CarbonCfgMemoryCustomCodeSection sect)
{
  setEditable(mMemory->GetEditFlags().testFlag(CcfgFlags::EditMemoryCustomCodes));

  CarbonCfgMemoryCustomCode* preCc = findMemoryCustomCode(eCarbonCfgPre, sect);
  new MemoryCustomCodeNode(parent, sect, mMemory, preCc, CustomCodesRootNode::Memory, eCarbonCfgPre);

  CarbonCfgMemoryCustomCode* postCc = findMemoryCustomCode(eCarbonCfgPost, sect);
  new MemoryCustomCodeNode(parent, sect, mMemory, postCc, CustomCodesRootNode::Memory, eCarbonCfgPost);
}

MemEditorTreeItem* CustomCodesRootNode::createMemoryCustomCodeNode(const QString& name, CarbonCfgMemoryCustomCodeSection sect)
{
  MemEditorTreeItem* parent = new MemEditorTreeItem(MemEditorTreeItem::TextNode, getItem());
  parent->setText(0, name);
  createMemoryCustomCodeNodes(parent, sect);
  return parent;
}

MemEditorTreeItem* CustomCodesRootNode::createCompCustomCodeNode(const QString& name, CarbonCfgCompCustomCodeSection sect)
{
  MemEditorTreeItem* parent = new MemEditorTreeItem(MemEditorTreeItem::TextNode, getItem());
  parent->setText(0, name);
  createCompCustomCodeNodes(parent, sect);
  return parent;
}


// Custom Code sections may or may not be defined.
void CustomCodesRootNode::populateMemoryBlockCodes()
{
  createMemoryBlockCustomCodeNode("Class", eCarbonCfgMemoryBlockClass);
  createMemoryBlockCustomCodeNode("Constructor", eCarbonCfgMemoryBlockConstructor);
  createMemoryBlockCustomCodeNode("Destructor", eCarbonCfgMemoryBlockDestructor);
  createMemoryBlockCustomCodeNode("Read", eCarbonCfgMemoryBlockRead);
  createMemoryBlockCustomCodeNode("Write", eCarbonCfgMemoryBlockWrite);
}

void CustomCodesRootNode::populateMemoryCodes()
{

  createMemoryCustomCodeNode("Class", eCarbonCfgMemoryClass);
  createMemoryCustomCodeNode("Constructor", eCarbonCfgMemoryConstructor);
  createMemoryCustomCodeNode("Destructor", eCarbonCfgMemoryDestructor);
  createMemoryCustomCodeNode("Read", eCarbonCfgMemoryRead);
  createMemoryCustomCodeNode("Write", eCarbonCfgMemoryWrite);
}


void CustomCodesRootNode::populateComponentCodes()
{
  createCompCustomCodeNode("Cpp Include", eCarbonCfgCompCppInclude);
  createCompCustomCodeNode("H Include", eCarbonCfgCompHInclude);
  createCompCustomCodeNode("Constructor", eCarbonCfgCompConstructor);
  createCompCustomCodeNode("Destructor", eCarbonCfgCompDestructor);
  createCompCustomCodeNode("Communicate", eCarbonCfgCompCommunicate);

  createCompCustomCodeNode("Interconnect", eCarbonCfgCompInterconnect);
  createCompCustomCodeNode("Update", eCarbonCfgCompUpdate);
  createCompCustomCodeNode("Init", eCarbonCfgCompInit);
  createCompCustomCodeNode("Reset", eCarbonCfgCompReset);
  createCompCustomCodeNode("Terminate", eCarbonCfgCompTerminate);
  createCompCustomCodeNode("Set Parameter", eCarbonCfgCompSetParameter);
  createCompCustomCodeNode("Get Parameter", eCarbonCfgCompGetParameter);
  createCompCustomCodeNode("Get Property", eCarbonCfgCompGetProperty);
  createCompCustomCodeNode("Debug Transaction", eCarbonCfgCompDebugTransaction);
  createCompCustomCodeNode("Debug Access", eCarbonCfgCompDebugAccess);
  createCompCustomCodeNode("Processor Can Stop", eCarbonCfgCompProcCanStop);
  createCompCustomCodeNode("Stop At Debuggable Point", eCarbonCfgCompProcStopAtDebuggablePoint);
  createCompCustomCodeNode("Save Data", eCarbonCfgCompSaveData);
  createCompCustomCodeNode("Restore Data", eCarbonCfgCompRestoreData);

  createCompCustomCodeNode("Get Callback", eCarbonCfgCompGetCB);
  createCompCustomCodeNode("Save Stream Type", eCarbonCfgCompSaveStreamType);
  createCompCustomCodeNode("Save Data Arch", eCarbonCfgCompSaveDataArch);
  createCompCustomCodeNode("Restore Data Arch", eCarbonCfgCompRestoreDataArch);
  createCompCustomCodeNode("Save Data Binary", eCarbonCfgCompSaveDataBinary);
  createCompCustomCodeNode("Restore Data Binary", eCarbonCfgCompRestoreDataBinary);

}


QWidget* CustomCodeNode::createEditor(const MemEditorDelegate*, QWidget* parent, int)
{
  QString code;
  if (getCustomCode())
    code = getCustomCode()->getCode();

  DlgCodeEditor dlg(parent, code);
  if (dlg.exec() == QDialog::Accepted)
  {
    setCode(dlg.getCode());   
  }
  
  return NULL;
}


