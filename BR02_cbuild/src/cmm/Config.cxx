//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "Config.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QFont>
#include <QFontInfo>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QSettings>
#include <QList>

static CmmConfig *static_configuration = 0;

inline QString getVersionString()
{
  return QString::number( (QT_VERSION >> 16) & 0xff )
    + QLatin1String(".") + QString::number( (QT_VERSION >> 8) & 0xff );
}

CmmConfig::CmmConfig()
{
  if( !static_configuration ) {
    static_configuration = this;
  } else {
    qWarning( "Multiple configurations not allowed!" );
  }
}

CmmConfig *CmmConfig::loadConfig(const QString &profileFileName)
{
  CmmConfig *config = new CmmConfig();

  if (profileFileName.isEmpty()) { // no profile
    config->load();
    return config;
  }

  QFile file(profileFileName);
  if (!file.exists()) {
    return 0;
  }

  config->load();
  return config;
}

CmmConfig *CmmConfig::configuration()
{
  if (static_configuration == 0)
    static_configuration = new CmmConfig();

  return static_configuration;
}

void CmmConfig::load()
{
  const QString key = getVersionString() + QLatin1String("/");

  QSettings settings("Carbon Design Systems","ModelStudio");

  if (qApp->type() != QApplication::Tty)
    winGeometry = settings.value(key + QLatin1String("windowGeometry")).toByteArray();

  mainWinState = settings.value(key + QLatin1String("MainWindowState")).toByteArray();
}

void CmmConfig::save()
{
  saveSettings();
}

void CmmConfig::saveSettings()
{
  const QString key = getVersionString() + QLatin1String("/");

  QSettings settings("Carbon Design Systems","ModelStudio");

  if (qApp->type() != QApplication::Tty)
    settings.setValue(key + QLatin1String("windowGeometry"), winGeometry);

  settings.setValue( key + QLatin1String("MainWindowState"), mainWinState );
}

#ifdef ASSISTANT_DEBUG
static void dumpmap( const QMap<QString,QString> &m, const QString &header )
{
  qDebug( header );
  QMap<QString,QString>::ConstIterator it = m.begin();
  while (it != m.end()) {
    qDebug( "  " + it.key() + ":\t\t" + *it );
    ++it;
  }
}
#endif



