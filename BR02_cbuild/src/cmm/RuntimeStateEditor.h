// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _RuntimeStateEditor_h_
#define _RuntimeStateEditor_h_

#include "util/CarbonPlatform.h"
#include "Mdi.h"

#include <QtGui>

class CarbonMakerContext;
class CarbonRuntimeState;
class CarbonModelsWidget;
class SimulationHistoryWidget;
class OnDemandTraceWidget;
class MDIRuntimeTemplate;
class OnDemandTuningWizard;

class RuntimeStateEditor : public QWidget, public MDIWidget
{
  Q_OBJECT

public:
  RuntimeStateEditor(CarbonMakerContext *ctx, MDIRuntimeTemplate* t, QWidget *parent = 0);
  virtual ~RuntimeStateEditor();

  bool open(const char *fname);
  virtual const char* userFriendlyName();
  virtual void closeEvent(QCloseEvent *ev);

private slots:
  void on_actionContinueSimulation_triggered();
  void on_actionReplayPlay_triggered();
  void on_actionReplayRecord_triggered();
  void on_actionReplayNormal_triggered();
  void on_actionReplayVerbose_triggered(bool);
  void on_actionOnDemandStart_triggered();
  void on_actionOnDemandStop_triggered();
  void on_actionOnDemandTune_triggered();
  void on_actionOnDemandEnableTrace_triggered(bool);
  void on_actionOnDemandViewTrace_triggered();

signals:
  void componentSelected(bool);

private:
  CarbonRuntimeState      *mRuntimeState;
  CarbonMakerContext      *mContext;
  CarbonModelsWidget      *mCarbonModels;
  SimulationHistoryWidget *mSimulationHistory;
  OnDemandTraceWidget     *mOnDemandTrace;
  OnDemandTuningWizard    *mOnDemandTuningWizard;
};

#endif

