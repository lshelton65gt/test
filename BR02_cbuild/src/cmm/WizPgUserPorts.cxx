//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "WizPgUserPorts.h"
#include "MemoryWizard.h"
#include "WizardTemplate.h"
#include "WizardContext.h"
#include "Template.h"
#include "DlgAddWizardPort.h"
#include "CarbonProjectWidget.h"

WizPgUserPorts::WizPgUserPorts(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);
  setCommitPage(true);

  QStringList labels;
  labels << "Port"; // colNAME
  labels << "Width";
  labels << "Direction";    // colREPLAY
  labels << "Tie Value";

  ui.treeWidget->setHeaderLabels(labels);
  updateButtons();
}

WizPgUserPorts::~WizPgUserPorts()
{
}

bool WizPgUserPorts::serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  QDomElement tag = doc.createElement("WizardStep");
  tag.setAttribute("name", objectName());
  parent.appendChild(tag);

  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardTemplate* wizTempl = wiz->getTemplate();
  WizardContext* context = wizTempl->getContext();
  Template* templ = context->getTemplate();
  QString modeName = wiz->field("portMode").toString();
  Mode* mode = templ->findMode(modeName);
  Block* userBlock = mode->getBlocks()->findBlock("UserDefined");
  if (userBlock)
  {
    Ports* ports = userBlock->getPorts();
    for (int i=0; i<ports->getCount(); i++)
    {
      Port* port = ports->getAt(i);
      QDomElement portElement = addElement(doc, tag, "Port", port->getName());
      QString tieValue = port->getTieValue();
      
      portElement.setAttribute("width", port->getWidth());
      portElement.setAttribute("location", (int)port->getLocation());

      if (tieValue.length() > 0)
        portElement.setAttribute("tieValue", tieValue);
    }
  }

  return true;
}

void WizPgUserPorts::on_pushButtonAddPort_clicked()
{
  DlgAddWizardPort dlg;
  if (dlg.exec() == QDialog::Accepted)
  {
    MemoryWizard* wiz = (MemoryWizard*)wizard();
    WizardTemplate* wizTempl = wiz->getTemplate();
    WizardContext* context = wizTempl->getContext();
    Template* templ = context->getTemplate();
    QString modeName = wiz->field("portMode").toString();
    Mode* mode = templ->findMode(modeName);
    Block* userBlock = mode->getBlocks()->findBlock("UserDefined");
    if (userBlock == NULL)
    {
      mode->getBlocks()->addBlock("UserDefined", false);
      userBlock = mode->getBlocks()->findBlock("UserDefined");
    }
    INFO_ASSERT(userBlock, "Unable to locate User Block");
    Ports* ports = userBlock->getPorts();
    Ports::PortLocation loc = Ports::GraphicsLeft;
    if (dlg.getDirection() == DlgAddWizardPort::InputPort)
      loc = Ports::GraphicsLeft;
    else
      loc = Ports::GraphicsRight;

    Port* port = ports->findPort(dlg.getPortName());
    if (port == NULL)
    {
      addPort(ports, dlg.getPortName(), dlg.getTieValue(), dlg.getWidth(), loc);
      updateButtons();
    }
    else
    {
      QString msg = QString("Port named '%1' already exists, ignoring.").arg(dlg.getPortName());
      QMessageBox::critical(this, MODELWIZARD_TITLE, msg);
    }
  }
}

void WizPgUserPorts::initializePage()
{
  setTitle("User-Defined Ports");
  setSubTitle("Add any additional user-defined ports if necessary.");

  wizard()->setButtonText(QWizard::CommitButton, "Next");

  MemoryWizard* wiz = (MemoryWizard*)wizard();
  QString replayFile = wiz->getReplayFile();

  if (replayFile.length() > 0)
  {
    WizardTemplate* wizTempl = wiz->getTemplate();
    WizardContext* context = wizTempl->getContext();
    Template* templ = context->getTemplate();
    QString modeName = wiz->field("portMode").toString();
    Mode* mode = templ->findMode(modeName);

    QDomElement wizElem = wiz->findWizardStep(objectName());
    if (!wizElem.isNull())
    {
      bool performNext = true;
      QDomNode n = wizElem.firstChild();
      while(!n.isNull())
      {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) 
        {
          qDebug() << e.tagName();
          if (e.tagName() == "Port")
          {
            Block* userBlock = mode->getBlocks()->findBlock("UserDefined");
            if (userBlock == NULL)
            {
              mode->getBlocks()->addBlock("UserDefined", false);
              userBlock = mode->getBlocks()->findBlock("UserDefined");
            }
            Ports* ports = userBlock->getPorts();

            QString portName = e.text();
            QString width = e.attribute("width");
            QVariant vWidth(width);
            QString tieValue = e.attribute("tieValue");
            QString location = e.attribute("location");
            QVariant vLocation(location);
            Ports::PortLocation loc = static_cast<Ports::PortLocation>(vLocation.toInt());
            addPort(ports, portName, tieValue, vWidth.toInt(), loc);
          }
        }
        n = n.nextSibling();
      }
      if (performNext)
        wiz->postNext();
      else
        wiz->stopReplay();
    }
  }
}

void WizPgUserPorts::addPort(Ports* ports, const QString& portName, const QString& tieValue, int portWidth, Ports::PortLocation loc)
{
  Port* port = (Port*)ports->addPort(portName, loc, portName, false);
  port->setTieValue(tieValue);
  port->setWidth(portWidth);

  QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidget);

  item->setText(0, portName);

  QString wv;
  wv.setNum(portWidth);

  item->setText(1, wv);

  if (loc == Ports::GraphicsLeft)
    item->setText(2, "Input");
  else
    item->setText(2, "Output");

  item->setText(3, tieValue);
}

int WizPgUserPorts::nextId() const
{
  return MemoryWizard::PagePorts;
}

void WizPgUserPorts::on_treeWidget_itemSelectionChanged()
{
  updateButtons();
}

void WizPgUserPorts::updateButtons()
{
  ui.pushButtonDeletePort->setEnabled(ui.treeWidget->selectedItems().count() > 0);
}

void WizPgUserPorts::on_pushButtonDeletePort_clicked()
{
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardTemplate* wizTempl = wiz->getTemplate();
  WizardContext* context = wizTempl->getContext();
  Template* templ = context->getTemplate();
  QString modeName = wiz->field("portMode").toString();
  Mode* mode = templ->findMode(modeName);
  Block* userBlock = mode->getBlocks()->findBlock("UserDefined");
  Ports* ports = userBlock->getPorts();

  QTreeWidgetItemIterator it(ui.treeWidget);
  while (*it) 
  {
    QTreeWidgetItem* item = (*it);
    if (item->isSelected())
    {
      QString portName = item->text(0);
      Port* port = ports->findPort(portName);
      if (port)
        ports->removePort(port);
      qDebug() << "Delete" << portName;
      QTreeWidgetItem* parent = item->parent();
      if (parent == NULL)
        ui.treeWidget->takeTopLevelItem(ui.treeWidget->indexOfTopLevelItem(item));
      else
        parent->takeChild(parent->indexOfChild(item));
    }
    ++it;
  }
}

