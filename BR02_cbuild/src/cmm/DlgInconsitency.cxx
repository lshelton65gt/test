//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include "DlgInconsitency.h"

DlgInconsitency::DlgInconsitency(QWidget *parent, const QString& msg)
: QDialog(parent)
{
  ui.setupUi(this);
  ui.textBrowser->setText(msg);
}

DlgInconsitency::~DlgInconsitency()
{
}

void DlgInconsitency::on_buttonBox_rejected()
{
  reject();
}

void DlgInconsitency::on_buttonBox_accepted()
{
  accept();
}

void DlgInconsitency::on_buttonBox_clicked(QAbstractButton* btn)
{
  if (btn == ui.buttonBox->button(QDialogButtonBox::Yes))
    on_buttonBox_accepted();
  else if (btn == ui.buttonBox->button(QDialogButtonBox::No))
    on_buttonBox_rejected();
}
