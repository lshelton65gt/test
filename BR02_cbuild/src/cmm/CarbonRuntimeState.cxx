// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/CarbonVersion.h"
#include "util/UtString.h"
#include "CarbonRuntimeState.h"
#include "CarbonProjectWidget.h"
#include <QWorkspace>
#include <QSignalMapper>
#include <QtGui>

#include "gui/CQt.h"
#include "CQtWizardContext.h"
#include "shell/ReplaySystem.h"
#include "shell/carbon_misc.h"

#include "util/ArgProc.h"
#include "gui/CQt.h"
#include "QTextEditor.h"
#include "SettingsEditor.h"
#include "SettingsContainer.h"
#include "PropertyEditor.h"
#include "CarbonModelsWidget.h"
#include "SimulationHistoryWidget.h"
#include "OnDemandTraceWidget.h"
#include "CarbonMakerContext.h"
#include "WorkspaceModelMaker.h"
#include "CarbonProjectWidget.h"

CarbonRuntimeState::CarbonRuntimeState(CarbonMakerContext* cmc)
  : mCarbonSystem(0), 
    mCyclesSpecified(false),
    mSimRequestActive(false),
    mCarbonMakerContext(cmc)
{
  mCarbonSystem = new CarbonReplaySystem; // TODO: get rid of this. needed for now to make perf graph happy.

  mPollCarbonSystem = NULL;
  mPollTimer = new QTimer(this);
  connect(mPollTimer, SIGNAL(timeout()), this, SLOT(pollSimFile()));
  mPollTimer->setInterval(2000); // milliseconds

  mContinueSimEnabled = false;

  // Hookup to the console for simulationEnded
  CQT_CONNECT(mCarbonMakerContext->getCarbonProjectWidget()->getConsole(), 
    simulationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType), this,
    simulationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType));
}

void CarbonRuntimeState::simulationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  mContinueSimEnabled = false;
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
  mCarbonMakerContext->getCarbonProjectWidget()->project()->cleanupMonitor();
}

CarbonRuntimeState::~CarbonRuntimeState()
{
  delete mCarbonSystem;
  delete mPollCarbonSystem;
  delete mPollTimer;
}


void CarbonRuntimeState::putSelectedModels(UtStringArray &selection)
{
  mSelectedModels = selection;
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}


void CarbonRuntimeState::replayVerbose(bool verbose)
{
  mCarbonSystem->putVerbose(verbose);
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}


void CarbonRuntimeState::replayDatabase(const char *db)
{
  mCarbonSystem->putDatabaseDirectory(db);
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}

void CarbonRuntimeState::replayRecord()
{
  // loop over selected models and update the replay state for each one
  UInt32 numModels = mSelectedModels.size();
  for (UInt32 i = 0; i < numModels; i++) {
    const char *compName = mSelectedModels[i];
    mCarbonSystem->recordComponent(compName);
  }
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}

void CarbonRuntimeState::replayPlayback()
{
  // loop over selected models and update the replay state for each one
  UInt32 numModels = mSelectedModels.size();
  for (UInt32 i = 0; i < numModels; i++) {
    const char *compName = mSelectedModels[i];
    mCarbonSystem->playbackComponent(compName);
  }
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}

void CarbonRuntimeState::replayStop()
{
  // loop over selected models and update the replay state for each one
  UInt32 numModels = mSelectedModels.size();
  for (UInt32 i = 0; i < numModels; i++) {
    const char *compName = mSelectedModels[i];
    mCarbonSystem->replayStopComponent(compName);
  }
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}

void CarbonRuntimeState::replayCheckpointInterval(UInt32 interval)
{
  mCarbonSystem->putCheckpointInterval(mCarbonSystem->getRecoverPercentage(), interval);
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}

void CarbonRuntimeState::replayRecoverPercentage(UInt32 percent)
{
  mCarbonSystem->putCheckpointInterval(percent, mCarbonSystem->getCheckpointInterval());
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}

void CarbonRuntimeState::onDemandMaxStates(UInt32 states)
{
  UInt32 numModels = mSelectedModels.size();
  for (UInt32 i = 0; i < numModels; i++) {
    const char *compName = mSelectedModels[i];
    mCarbonSystem->putOnDemandMaxStates(compName, states);
  }
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}

void CarbonRuntimeState::onDemandBackoffStates(UInt32 states)
{
  UInt32 numModels = mSelectedModels.size();
  for (UInt32 i = 0; i < numModels; i++) {
    const char *compName = mSelectedModels[i];
    mCarbonSystem->putOnDemandBackoffStates(compName, states);
  }
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}

void CarbonRuntimeState::onDemandBackoffDecayPercent(UInt32 percent)
{
  UInt32 numModels = mSelectedModels.size();
  for (UInt32 i = 0; i < numModels; i++) {
    const char *compName = mSelectedModels[i];
    mCarbonSystem->putOnDemandBackoffDecayPercent(compName, percent);
  }
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}

void CarbonRuntimeState::onDemandBackoffMaxDecay(UInt32 maxDecay)
{
  UInt32 numModels = mSelectedModels.size();
  for (UInt32 i = 0; i < numModels; i++) {
    const char *compName = mSelectedModels[i];
    mCarbonSystem->putOnDemandBackoffMaxDecay(compName, maxDecay);
  }
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}

void CarbonRuntimeState::onDemandBackoffStrategy(CarbonOnDemandBackoffStrategy strategy)
{
  UInt32 numModels = mSelectedModels.size();
  for (UInt32 i = 0; i < numModels; i++) {
    const char *compName = mSelectedModels[i];
    mCarbonSystem->putOnDemandBackoffStrategy(compName, strategy);
  }
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}

void CarbonRuntimeState::onDemandExcludedStateSignals(const char* compName, const char* sigs)
{
  UtStringArray sigArray;
  if (sParseDelimitedList(sigs, &sigArray)) {
    // Clear the set of signals first
    mCarbonSystem->clearOnDemandExcluded(compName);
    // Add each signal from the list
    for (UInt32 j = 0; j < sigArray.size(); ++j) {
      const UtString &sigName = sigArray[j];
      mCarbonSystem->addOnDemandExcluded(compName, sigName.c_str());
    }
  }
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}

void CarbonRuntimeState::onDemandExcludedInputSignals(const char* compName, const char* sigs)
{
  UtStringArray sigArray;
  if (sParseDelimitedList(sigs, &sigArray)) {
    // Clear the set of signals first
    mCarbonSystem->clearOnDemandIdleDeposit(compName);
    // Add each signal from the list
    for (UInt32 j = 0; j < sigArray.size(); ++j) {
      const UtString &sigName = sigArray[j];
      mCarbonSystem->addOnDemandIdleDeposit(compName, sigName.c_str());
    }
  }
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}


void CarbonRuntimeState::onDemandStart()
{
  // loop over selected models and update the OnDemand state for each one
  UInt32 numModels = mSelectedModels.size();
  for (UInt32 i = 0; i < numModels; i++) {
    const char *compName = mSelectedModels[i];
    mCarbonSystem->onDemandStartComponent(compName);
  }
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}

void CarbonRuntimeState::onDemandTrace(bool enable)
{
  UInt32 numModels = mSelectedModels.size();
  for (UInt32 i = 0; i < numModels; i++) {
    const char *compName = mSelectedModels[i];
    mCarbonSystem->putOnDemandTrace(compName, enable);
  }
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}

void CarbonRuntimeState::onDemandStop()
{
  // loop over selected models and update the OnDemand state for each one
  UInt32 numModels = mSelectedModels.size();
  for (UInt32 i = 0; i < numModels; i++) {
    const char *compName = mSelectedModels[i];
    mCarbonSystem->onDemandStopComponent(compName);
  }
  save();
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}


bool CarbonRuntimeState::save()
{
  if (mCarbonSystem != NULL) {
    return saveFile();
  } 
  return false;
  /*
  else {
    return saveAs();
  }
  */

}
/*

bool CarbonRuntimeState::saveAs() {
  UtString filename;

  bool ret = mCQtContext->popupFileDialog(this, false, // readFile
                                   "Save a Carbon Replay System",
                                   "Carbon Replay Configuration (*.css)",
                                   &filename);
  if (ret) {
    setCurrentFile(filename.c_str());
    // remove the extension
    filename.resize(filename.size() - 4);
    mCarbonSystem->putSystemName(filename.c_str());
    ret = saveFile();
  }
  return ret;
}
*/

bool CarbonRuntimeState::saveFile()
{
  bool ret = false;

  ret = mCarbonSystem->writeCmdline();

  /*
  if (! mCarbonSystem->writeCmdline()) {
    mCarbonMakerContext->getQtContext()->warning(mCarbonMakerContext->getMainWindow(), tr("Cannot write command-file file:\n%1.")
                         .arg(mCarbonSystem->getErrmsg()));
  }
  else {
    setWindowModified(false);
    statusBar()->showMessage(tr("File saved"), 2000);
    ret = true;
  }
  */
  return ret;
} // bool CarbonRuntimeState::saveFile


bool CarbonRuntimeState::loadCarbonSystem(const char *filename)
{
  UInt32 fname_len = strlen(filename);
  QFileInfo fi(filename);
  if (!fi.exists())
  {
    UtString msg;
    msg << "Error: No such file: " << filename;
    QMessageBox::critical(NULL, MODELSTUDIO_TITLE, msg.c_str());
    return false;
  }

  if (fname_len < 5) {
    mCarbonMakerContext->getQtContext()->warning(mCarbonMakerContext->getMainWindow(), "loadFile called on filename with invalid extension");
    return false;
  }

  // Remove the extension provided, and then add the extensions we
  // are going to work with
  UtString base(filename, fname_len - 4), guiFile, simFile;
  simFile << base << CARBON_STUDIO_SIMULATION_EXT;
  guiFile << base << CARBON_STUDIO_USER_EXT;

  bool ret = false;
  CarbonReplaySystem* old_system = mCarbonSystem;

  mCarbonSystem = new CarbonReplaySystem(base.c_str());
  ret = mCarbonSystem->readSystem(false);

  // Now, read the GUI file to get the user's preferences, in case
  // they have not yet been reflected in the system.
  UtString result;
  if (ret) {
    // If there is a .csu file, read it.  It overrides the .css file
    if (OSStatFile(guiFile.c_str(), "r", &result) == 1) {
      if (mCarbonSystem->readCmdline(false) == eCarbonSystemCmdError) {
        ret = false;            // warning issued below
      }
    }
    else {
      // Otherwise, we should write a .csu file reflecting the last state
      // of the simulation
      ret = saveFile();
    }
  }

  if (ret) {
    if (old_system != NULL) {
      old_system->clearCControlActiveFile();
      delete old_system;
    }
    mCarbonSystem->writeCControlActiveFile();

    mPollCarbonSystem = new CarbonReplaySystem(base.c_str());

    /*
    setCurrentFile(guiFile.c_str());
    */

    // there is nothing useful in this file yet -- we haven't parsed it.
    // If you uncomment this line, it results in calling
    // SimulationHistoryWidget::carbonSystemUpdated, which puts the X axis in
    // CPU time mode, when I want the default to be determined after we
    // have initially parsed the file.  It will be parsed after the timer
    // expires, and the system can update then.
    emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);

    mPollTimer->start();
    pollSimFile();              // look immediately
  }
  else {
    
    mCarbonMakerContext->getQtContext()->warning(mCarbonMakerContext->getMainWindow(), mCarbonSystem->getErrmsg());
    
    delete mCarbonSystem;
    mCarbonSystem = old_system;

    if (ret)
      emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
  }

  if (ret) {
    mFileName = filename;
  }

  return ret;
}

// See if the .css file has changed.  If so, re-read it
// stolen from ReplayGUI.cxx
void CarbonRuntimeState::pollSimFile() 
{
  if ((mPollCarbonSystem != NULL) && mPollCarbonSystem->readSystem(true)) {
    UtString buf, time;
    OSTimeToString(mPollCarbonSystem->getSimReadTime(), &time);
    CarbonReplayEventType status = mPollCarbonSystem->getSimStatus();
    if (status == eReplayEventExit) {
      buf << "Simulation exited";
      // if any components are in record mode, auto-switch them to normal mode.
      for (CarbonReplaySystem::ComponentLoop p(mPollCarbonSystem->loopComponents()); !p.atEnd(); ++p) {
        CarbonSystemComponent* comp = p.getValue();
        const char* name = comp->getName();
        if(mPollCarbonSystem->isRecordRequested(name)) {
          mCarbonSystem->replayStopComponent(name);
        }
      }
    }
    else {
      buf << "Simhost " << mPollCarbonSystem->getSimHost()
          << ";   pid " << mPollCarbonSystem->getSimPID();

    }
    buf << ";   last update " << time;

    UInt32 numEvents = mPollCarbonSystem->numEvents();
    if (numEvents > 0) {
      CarbonReplaySystem::Event* event;
      event = mPollCarbonSystem->getEvent(numEvents - 1);

      if (mCyclesSpecified) {
        UInt32 cyclesPerSecond
          = UInt32(0.5 + (event->mCycles / event->mUser));
        buf << ";   speed " << cyclesPerSecond << "cps";
      }
      else {
        UInt32 cyclesPerSecond
          = UInt32(0.5 + (event->mTotalSchedCalls / event->mUser));
        buf << ";   accumulated aggregate speed " << cyclesPerSecond << "cps";
      }
    }

    QStatusBar* status_bar =
      mCarbonMakerContext->getWorkspaceModelMaker()->getStatusBar();
    if (status_bar != NULL) {
      status_bar->showMessage(buf.c_str());
    }

    emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);

    mCyclesSpecified = mPollCarbonSystem->isCycleCountSpecified();
  } // if

  if (!mSimRequestActive && mPollCarbonSystem->checkSimRequest()) {
    mSimRequestActive = true;

    QMainWindow* mw = mCarbonMakerContext->getMainWindow();
    mw->show();
    mw->raise();
    mContinueSimEnabled = true;
    // paintSimContButton();  TODO?
    emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
  }
} // void ReplayGUI::pollSimFile


void CarbonRuntimeState::continueSimulation() {
  mContinueSimEnabled = false;
  mPollCarbonSystem->writeSimGrant();
  mSimRequestActive = false;
  emit carbonSystemUpdated(*this, *mCarbonSystem, *mPollCarbonSystem);
}

void CarbonRuntimeState::replayModelCounts(UInt32 *normal, UInt32 *playback, UInt32 *record)
{
  *normal = 0;
  *playback = 0;
  *record = 0;

  UInt32 numModels = mSelectedModels.size();

  if (numModels > 0) {
    for (UInt32 i = 0; i < numModels; i++) {
      const char *compName = mSelectedModels[i];
      if (mCarbonSystem->isRecordRequested(compName)) {
        ++(*record);
      }
      else if (mCarbonSystem->isPlaybackRequested(compName)) {
        ++(*playback);
      }
      else {
        ++(*normal);
      }
    }
  }
}

void CarbonRuntimeState::onDemandModelCounts(UInt32 *start, UInt32 *stop)
{
  *start = 0;
  *stop = 0;

  UInt32 numModels = mSelectedModels.size();

  if (numModels > 0) {
    for (UInt32 i = 0; i < numModels; i++) {
      const char *compName = mSelectedModels[i];
      if (mCarbonSystem->isOnDemandStartRequested(compName)) {
        ++(*start);
      }
      else if (mCarbonSystem->isOnDemandStopRequested(compName)) {
        ++(*stop);
      }
    }
  }
}

const char *CarbonRuntimeState::getOnDemandTraceFile(const char* compName) 
{
  const char *result = NULL;
  if (mPollCarbonSystem) {
    result = mPollCarbonSystem->getOnDemandTraceFile(compName);
  }
  else if (mCarbonSystem) {
    result = mCarbonSystem->getOnDemandTraceFile(compName);
  }
  return result;
}

bool CarbonRuntimeState::onDemandTraceFileExists(const char* compName)
{
  bool traceExists = false;
  const char *traceFile = getOnDemandTraceFile(compName);
  if (traceFile) {
    UtString errmsg;
    OSStatEntry statEntry;
    OSStatFileEntry(traceFile, &statEntry, &errmsg);
    traceExists = statEntry.exists();
  }
  return traceExists;
}

bool CarbonRuntimeState::sParseDelimitedList(const char* str, UtStringArray* array)
{
  if ((str == NULL) || (array == NULL)) {
    return false;
  }

  StrToken parser(str, ";");
  while (!parser.atEnd()) {
    const char *token = *parser;
    array->push_back(token);
    ++parser;
  }
  return true;
}
