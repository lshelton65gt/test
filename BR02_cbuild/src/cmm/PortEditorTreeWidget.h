#ifndef __PORTEDITORTREEWIDGET_H__
#define __PORTEDITORTREEWIDGET_H__

#include "util/CarbonPlatform.h"
#include "util/CarbonAssert.h"
#include "cfg/CarbonCfg.h"

#include <QtGui>

class CarbonCfg;
class PortEditorDelegate;
class UndoData;
class PortEditorTreeWidget;
class ESLPortItem;
class ESLDisconnectItem;
class ESLXtorInstanceItem;
class ESLXtorInstanceConnItem;

class PortTreeIndex
{
public:
  PortTreeIndex()
  {
  }
  void addPath(const QString& item)
  {
    mPathItems.prepend(item);
  }

  bool isEmpty() { return mPathItems.count() == 0; }
  void clear() { mPathItems.clear(); }
  int numItems() const { return mPathItems.count(); }
  QString itemAt(int index) const { return mPathItems[index]; }

private:
  QStringList mPathItems;

};

class PortEditorTreeItem : public QTreeWidgetItem
{
public:
  enum NodeType { Ports,
                  GlobalParameters,
                  GlobalParameter,
                  GlobalParameterPort,
                  GlobalParameterValue,
                  GlobalParameterDefaultValue,
                  Resets,
                  Reset,
                  ResetInitialValue,
                  ResetCyclesBefore,
                  ResetCyclesAfter,
                  ResetCyclesAsserted,
                  ResetCompCycles,
                  ResetClockCycles,
                  ResetActive,
                  ResetInactive,
                  ResetFrequency,
                  Clocks,
                  Clock,
                  ClockFrequency,
                  ClockInitValue,
                  ClockDelay,
                  ClockDutyCycle,
                  ClockCompCycles,
                  ClockCycles,
                  Ties,
                  Tie,
                  TieValue,
                  Disconnects,
                  Disconnect,
                  ESLPort, 
                  ESLPortExpr,
                  ESLPortParam,
                  RTLPort, 
                  XtorInst,
                  XtorInstParams,
                  XtorInstParameter,
                  XtorInstClockedBy,
                  XtorInstParamRTLPort,
                  XtorInstConn,
                  XtorInstConnExpr,
                  XtorInstAbstraction,
                  ESLPortMode,
                  ESLPortType,
                  XtorInstClocksResets,
                  XtorInstResetBy,
                  SystemCClocks,
                  SystemCClock,
                  SystemCClockInitValue,
                  SystemCClockDutyCycle,
                  SystemCClockPeriod,
                  SystemCClockPeriodUnits,
                  SystemCClockStartTime,
                  SystemCClockStartTimeUnits,
                };

  PortEditorTreeItem(NodeType nodeType) : QTreeWidgetItem()
  {
    mNodeType = nodeType;
    init();
  }
  PortEditorTreeItem(NodeType nodeType, QTreeWidget* parent) : QTreeWidgetItem(parent)
  {
    mNodeType = nodeType;
    init();
  }
  PortEditorTreeItem(NodeType nodeType, QTreeWidgetItem* parent) : QTreeWidgetItem(parent)
  {
    mNodeType = nodeType;
    init();
  }

  PortTreeIndex getTreeIndex();


  bool operator<(const QTreeWidgetItem &other) const;

  virtual ~PortEditorTreeItem() {}

  virtual void doubleClicked() {}
  virtual bool resizeOnExpand() const { return false; }
  virtual bool resizeOnCollapse() const { return false; }
  virtual bool dropItem(const QString&) { return true; }
  virtual bool dropItems(const QList<PortEditorTreeItem*>&) { return true; }
  virtual bool canDropHere(const QString&, QDragMoveEvent*) { return false; }
  virtual bool canDropHere(const QList<PortEditorTreeItem*>&, QDragMoveEvent*) { return false; }
  virtual bool isDragSource() { return false; }
  // multiple item selection
  virtual void multiSelectContextMenu(QMenu&) { }
  virtual QWidget* createEditor(const PortEditorDelegate* /*delegate*/, 
    QWidget* /*parent*/, int /*columnIndex*/) { return NULL; }
  virtual void setModelData(const PortEditorDelegate* /*delegate*/, 
    QAbstractItemModel* /*model*/, const QModelIndex & /*modelIndex*/,
    QWidget* /*editor*/, int /*columnIndex*/) { }
  void setModified(bool value);
  virtual UndoData* deleteItem() { return NULL; }

  void setBoldText(int col, const QString& text)
  {
    QFont f = font(col);
    f.setBold(true);
    setFont(col, f);  
    setText(col, text);
  }

  void setNormalText(int col, const QString& text)
  {
    QFont f = font(col);
    f.setBold(false);
    setFont(col, f);  
    setText(col, text);
  }

  void setStrikeOutText(int col, const QString& text)
  {
    QFont f = font(col);
    f.setStrikeOut(true);
    setFont(col, f);  
    setText(col, text);
  }

  void setItalicText(int col, const QString& text)
  {
    QFont f = font(col);
    f.setItalic(true);
    setFont(col, f);  
    setText(col, text);
  }

  void highlightValue(int col, const QString& newVal, const QString& defaultVal)
  {
    if (newVal != defaultVal)
      setBoldText(col, newVal);
    else
      setNormalText(col, newVal);
  }

  CarbonCfg* getCcfg() const;
  QUndoStack* getUndoStack() const;
  PortEditorTreeWidget* getTree();

  QIcon icon(CarbonCfgRTLPortType portType) const
  {
    switch (portType)
    {
    case eCarbonCfgRTLInput:  return mIconInput;
    case eCarbonCfgRTLOutput: return mIconOutput;
    case eCarbonCfgRTLInout:  return mIconBidi;
    }
    return mIconInput;
  }

  QIcon icon(CarbonCfgESLPortType portType) const
  {
    switch (portType)
    {
    case eCarbonCfgESLInput:  return mIconInput;
    case eCarbonCfgESLOutput: return mIconOutput;
    case eCarbonCfgESLInout: return mIconBidi;
    case eCarbonCfgESLUndefined:
      INFO_ASSERT(portType != eCarbonCfgESLUndefined, "Found an undefined component signal port type");
      break;
    }
    return mIconInput;
  }

  NodeType getNodeType() const { return mNodeType; }


public:
  static void dumpRTLPort(CarbonCfgRTLPort*)
  {
   /* qDebug() << "RTL Port" << rtlPort->getName();
    qDebug() << "    RTL Port connections" << rtlPort->numConnections();
    qDebug() << "    RTL Port clkgen" << rtlPort->isClockGen();
    qDebug() << "    RTL Port resetgen" << rtlPort->isResetGen();
    qDebug() << "    RTL Port tied" << rtlPort->isTied();
    qDebug() << "    RTL Port tied to param" << rtlPort->isTiedToParam();
    qDebug() << "    RTL Port esl port" << rtlPort->isESLPort();
    qDebug() << "    RTL Port xtor conn" << rtlPort->isConnectedToXtor();*/
  }

private:
  void init();
  QIcon mIconInput;
  QIcon mIconOutput;
  QIcon mIconBidi;
  NodeType mNodeType;
};



class CompWizardPortEditor;

class PortEditorTreeWidget : public QTreeWidget
{
  Q_OBJECT

protected:
  virtual void dragEnterEvent(QDragEnterEvent * event);
  virtual void dragLeaveEvent(QDragLeaveEvent * event);
  virtual void dragMoveEvent(QDragMoveEvent * event);
  virtual void dropEvent(QDropEvent * event); 
  virtual void mousePressEvent(QMouseEvent* event);
  virtual void mouseReleaseEvent(QMouseEvent* event);
  virtual void mouseMoveEvent(QMouseEvent* event);


public:
  PortEditorTreeWidget(QWidget* parent=0);
  virtual ~PortEditorTreeWidget() {}
  
  void closeTree();

  ESLXtorInstanceConnItem* findXtorInstancePort(QTreeWidgetItem* parent, const QString& xtorPortName);
  ESLXtorInstanceItem* findXtorInstance(QTreeWidgetItem* parent, const QString& xtorInstName);
  ESLPortItem* findESLPort(QTreeWidgetItem* parent, const QString& eslPortName);
  ESLDisconnectItem* findDisconnect(QTreeWidgetItem* parent, const QString& rtlPortName);

  QUndoStack* getUndoStack() const { return mUndoStack; }
  void setUndoStack(QUndoStack* stack) { mUndoStack=stack; }

  CarbonCfg* getCcfg() const { return mCfg; }
  void setCcfg(CarbonCfg* cfg) { mCfg=cfg; }

  void setEditor(CompWizardPortEditor* editor) { mEditor=editor; }
  CompWizardPortEditor* getEditor() const { return mEditor; }
  
  void closeEditor();

  void expandChildren(QTreeWidgetItem* item);
  void collapseChildren(QTreeWidgetItem* item);
  void sortChildren(QTreeWidgetItem* item);
  void setExpanded(QTreeWidgetItem* item, bool expand);

  void setModified(bool value)
  {
    emit widgetModified(value);
  }

  void removeItem(QTreeWidgetItem* item)
  {
    QTreeWidgetItem* parent = item->parent();
    if (parent)
      parent->removeChild(item);
    else
      INFO_ASSERT(true, "Unexpected remove");
  }

  void removeItem(const PortTreeIndex& index)
  {
    QTreeWidgetItem* item = itemFromIndex(index);
    removeItem(item);
  }

  PortTreeIndex indexFromItem(QTreeWidgetItem *item, int column = 0) const
  {
    PortTreeIndex ti;
    do
    {
      ti.addPath(item->text(column));
      item = item->parent();
    }
    while (item);

    return ti;
  }

  QTreeWidgetItem *itemFromIndex(const PortTreeIndex &index) const
  {
    QTreeWidgetItem* item = NULL;
    for (int i=0; i<index.numItems(); i++)
    {
      QString itemName = index.itemAt(i);
      if (i == 0)
      {
        QList<QTreeWidgetItem*> items = findItems(itemName, Qt::MatchExactly);
        INFO_ASSERT(items.count() == 1, "Expecting single match");
        item = items[0];
      }
      else
      {
        for (int ci=0; ci<item->childCount(); ci++)
        {
          QTreeWidgetItem* childItem = item->child(ci);
          if (childItem->text(0) == itemName)
          {
            item = childItem;
            break;
          }
        }
      }
    }
    return item;
  }
  
  quint32 numSelectedItems();
  quint32 numSelectedItems(PortEditorTreeItem::NodeType nodeType);
  quint32 fillSelectedItems(QList<PortEditorTreeItem*>& items, PortEditorTreeItem::NodeType nodeType);

  void setEditorWidget(QWidget* w) { mLastEditor=w; }

private slots:
  void itemExpanded(QTreeWidgetItem*);
  void itemCollapsed(QTreeWidgetItem*);
  void itemDoubleClicked(QTreeWidgetItem*, int);

signals:
  void widgetModified(bool);

private: // methods
  void clearAnimation();
  void animateDropSite(const QPoint& pos);

private:
  QWidget* mLastEditor;
  QUndoStack* mUndoStack;
  CarbonCfg* mCfg;
  CompWizardPortEditor* mEditor;
  QPoint mDragStartPos;
#define DD_MAX_COLUMNS 10
  QTreeWidgetItem* mDropSiteItem;
  QBrush mDropSaveBrush[DD_MAX_COLUMNS];
  bool mInternalDrag;
  QList<PortEditorTreeItem*> mSelectedItems;
};

#endif
