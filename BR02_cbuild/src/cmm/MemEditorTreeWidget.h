#ifndef __MemEditorTreeWidget_H__
#define __MemEditorTreeWidget_H__

#include "util/CarbonPlatform.h"
#include "util/CarbonAssert.h"
#include "cfg/CarbonCfg.h"

#include <QtGui>

#include "cmm.h"
#include "CarbonMakerContext.h"
#include "CarbonProject.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"

class CarbonCfg;
class MemEditorDelegate;
class MemEditorTreeWidget;
class MemUndoData;
class CompWizardMemEditor;

class MemTreeIndex
{
public:
  MemTreeIndex()
  {
  }
  void addPath(const QString& item)
  {
    mPathItems.prepend(item);
  }

  bool isEmpty() { return mPathItems.count() == 0; }
  void clear() { mPathItems.clear(); }
  int numItems() const { return mPathItems.count(); }
  QString itemAt(int index) const { return mPathItems[index]; }

private:
  QStringList mPathItems;

};

class MemEditorTreeItem : public QTreeWidgetItem
{
public:
  enum NodeType { 
                  General,
                  Blocks,
                  Memories,
                  MemoryCustomCode,
                  CustomCodeNode,
                  CustomCodeSection,
                  MemoriesDropItem,
                  ESLMemoryNode, 
                  MemoryBigEndian, 
                  MemoryMaxAddress,
                  MemCustomCodes,
                  MemBlock,
                  MemBlockBase,
                  MemBlockSize,
                  MemBlockBank,
                  MemBlockLocations,
                  MemBlockCustomCodes,
                  MemBlockLocationRTL,
                  MemBlockLocationPort,
                  MemBlockLocationUser,
                  MemBlockLocationDisplayAttr,
                  MemBlockLocationDisplayAttrStartWordOffset,
                  MemBlockLocationDisplayAttrEndWordOffset,
                  MemBlockLocationDisplayAttrMSB,
                  MemBlockLocationDisplayAttrLSB,
                  MemBlockLocationDisplayAttrRTL,
                  MemBlockLocationDisplayAttrStartWordOffsetRTL,
                  MemBlockLocationDisplayAttrEndWordOffsetRTL,
                  MemBlockLocationDisplayAttrMSBRTL,
                  MemBlockLocationDisplayAttrLSBRTL,
                  Debugger,
                  Disassembly,
                  Disassembler,
                  MemorySubComponent,
                  MemInitializationRoot,
                  ProgPreloadESLPort,
                  ProgPreloadOffset,
                  ReadmemKind,
                  ReadmemFile,
                  TextNode,
                  CompCustomCodes,
                  SystemAddressESLPortMapping,
                };

  MemEditorTreeItem(NodeType nodeType) : QTreeWidgetItem()
  {
    mNodeType = nodeType;
    init();
  }
  MemEditorTreeItem(NodeType nodeType, QTreeWidget* parent) : QTreeWidgetItem(parent)
  {
    mNodeType = nodeType;
    init();
  }
  MemEditorTreeItem(NodeType nodeType, QTreeWidgetItem* parent) : QTreeWidgetItem(parent)
  {
    mNodeType = nodeType;
    init();
  }

  MemTreeIndex getTreeIndex();
  MemEditorTreeItem* getItem() { return this; }

  bool operator<(const QTreeWidgetItem &other) const;

  CompWizardMemEditor* getWizard();

  virtual ~MemEditorTreeItem() {}

  virtual void doubleClicked() {}
  virtual bool resizeOnExpand() const { return false; }
  virtual bool resizeOnCollapse() const { return false; }
  virtual bool dropItem(const QString&) { return true; }
  virtual bool dropItems(const QList<MemEditorTreeItem*>&) { return true; }
  virtual bool canDropHere(const QString&, QDragMoveEvent*) { return false; }
  virtual bool canDropHere(const QList<MemEditorTreeItem*>&, QDragMoveEvent*) { return false; }
  virtual bool isDragSource() { return false; }
  // multiple item selection
  virtual void multiSelectContextMenu(QMenu&) { }
  virtual QWidget* createEditor(const MemEditorDelegate* /*delegate*/, 
    QWidget* /*parent*/, int /*columnIndex*/) { return NULL; }
  virtual void setModelData(const MemEditorDelegate* /*delegate*/, 
    QAbstractItemModel* /*model*/, const QModelIndex & /*modelIndex*/,
    QWidget* /*editor*/, int /*columnIndex*/) { }
  void setModified(bool value);
  virtual MemUndoData* deleteItem() { return NULL; }
  virtual void deleteItem(QList<MemUndoData*>*) {}
  void setBoldText(int col, const QString& text)
  {
    QFont f = font(col);
    f.setBold(true);
    setFont(col, f);  
    setText(col, text);
  }
  void setColoredItalicText(int col, const QString& text, Qt::GlobalColor color);
  void setColoredItalicText(int col, const QString& text, const QColor& color);

  void setColoredBoldText(int col, const QString& text, Qt::GlobalColor color);
  void setColoredBoldText(int col, const QString& text, const QColor& color);

  void setColoredNormalText(int col, const QString& text, Qt::GlobalColor color);
  void setColoredNormalText(int col, const QString& text, const QColor& color);

  void setNormalText(int col, const QString& text);
  void showErrorText(int col);
  void showWarningText(int col);
  void showNormalText(int col);
  void setNormalItalicText(int col, const QString& text);

  void setEditable(bool value, bool affectParent = false)
  {
    if (value)
     setFlags(flags() | Qt::ItemIsEditable);
    else
    {
      setFlags(flags() & ~Qt::ItemIsEditable);
    
      QColor darkWhite = QColor(Qt::white).darker();

      setTextColor(0, darkWhite);
      setTextColor(1, darkWhite);

      if (affectParent)
      {
        parent()->setTextColor(0, darkWhite);
        parent()->setTextColor(1, darkWhite);
      }
    }
  }
  void setTreeWidget(MemEditorTreeWidget* tree)
  {
    mTreeWidget = tree;
  }
  void clearErrors(int startCol, int endCol, int errCol)
  {
    if (errCol != -1)
      setIcon(errCol, QIcon());

    for (int i=startCol; i<endCol; i++)
    {
      setToolTip(i, ""); 
      showNormalText(i);
    }
    
    QString helpGuide = text(2);
    if (!helpGuide.startsWith("("))
    {
      setText(2, "");
      setToolTip(2, "");
    }
    showNormalText(2);
  }

  void showErrors(int startCol, int endCol, const QString& inputErrMsg, int errCol)
  {
    QString e(inputErrMsg);
    QString errMsg = e.replace("\n","");

    if (errCol != -1)
      setIcon(errCol, QIcon(":/cmm/Resources/logError.png"));

    for (int i=startCol; i<endCol; i++)
    {
      setToolTip(i, errMsg);
      showErrorText(i);
    }
    
    CarbonConsole* console = theApp->getContext()->getCarbonProjectWidget()->getConsole();
    
    UtString errorMsg;
    errorMsg << "Error: 9999: " << errMsg;

    console->processOutput(CarbonConsole::Local, CarbonConsole::Check, errorMsg.c_str());

    setText(2, errMsg);
    setToolTip(2, errMsg);
    showErrorText(2);
  }

  void setStrikeOutText(int col, const QString& text)
  {
    QFont f = font(col);
    f.setStrikeOut(true);
    setFont(col, f);  
    setText(col, text);
  }

  void setMonoText(int col, const QString& text)
  { 
    QFont f = QFont("Courier",10);
    setFont(col, f);
    setText(col, text);
  }
  
  void setBoldItalicText(int col, const QString& text)
  {
    QFont f = font(col);
    f.setItalic(true);
    f.setBold(true);
    setFont(col, f);  
    setText(col, text);
    setTextAlignment(col, Qt::AlignTop);
  }

  void setItalicText(int col, const QString& text)
  {
    QFont f = font(col);
    f.setItalic(true);
    setFont(col, f);  
    setText(col, text);
    setTextAlignment(col, Qt::AlignTop);
  }

  void highlightValue(int col, const QString& newVal, const QString& defaultVal)
  {
    if (newVal != defaultVal)
      setBoldText(col, newVal);
    else
      setNormalText(col, newVal);
  }

  CarbonCfg* getCcfg() const;
  QUndoStack* getUndoStack() const;
  MemEditorTreeWidget* getTree();

  QIcon icon(CarbonCfgMemoryLoc* loc) const
  {
    switch (loc->getType())
    {
    case eCfgMemLocRTL:  return mIconLocRTL;
    case eCfgMemLocPort: return mIconLocPort;
    case eCfgMemLocUser:  return mIconLocUser;
    }
    return mIconLocRTL;
  }

  NodeType getNodeType() const { return mNodeType; }


public:
  static void dumpRTLPort(CarbonCfgRTLPort*)
  {
    //qDebug() << "RTL Port" << rtlPort->getName();
    //qDebug() << "    RTL Port connections" << rtlPort->numConnections();
    //qDebug() << "    RTL Port clkgen" << rtlPort->isClockGen();
    //qDebug() << "    RTL Port resetgen" << rtlPort->isResetGen();
    //qDebug() << "    RTL Port tied" << rtlPort->isTied();
    //qDebug() << "    RTL Port tied to param" << rtlPort->isTiedToParam();
    //qDebug() << "    RTL Port esl port" << rtlPort->isESLPort();
    //qDebug() << "    RTL Port xtor conn" << rtlPort->isConnectedToXtor();
  }

private:
  void init();
  QIcon mIconMemory;
  QIcon mIconBlock;
  QIcon mIconLocRTL;
  QIcon mIconLocPort;
  QIcon mIconLocUser;
  NodeType mNodeType;
  MemEditorTreeWidget* mTreeWidget;
};



class CompWizardMemEditor;

class MemEditorTreeWidget : public QTreeWidget
{
  Q_OBJECT

protected:
  virtual void dragEnterEvent(QDragEnterEvent * event);
  virtual void dragLeaveEvent(QDragLeaveEvent * event);
  virtual void dragMoveEvent(QDragMoveEvent * event);
  virtual void dropEvent(QDropEvent * event); 
  virtual void mousePressEvent(QMouseEvent* event);
  virtual void mouseReleaseEvent(QMouseEvent* event);
  virtual void mouseMoveEvent(QMouseEvent* event);
  virtual bool canDropHere(const QList<MemEditorTreeItem*>&, QDragMoveEvent*);
  virtual bool canDropHere(const QString&, QDragMoveEvent*);
  virtual void dropItem(const QString&);
  virtual void dropItem(const QList<MemEditorTreeItem*>&);

public:
  MemEditorTreeWidget(QWidget* parent=0);
  virtual ~MemEditorTreeWidget() {}
  
  void closeTree();

  static QColor getDefaultTextColor();

  QUndoStack* getUndoStack() const { return mUndoStack; }
  void setUndoStack(QUndoStack* stack) { mUndoStack=stack; }

  CarbonCfg* getCcfg() const { return mCfg; }
  void setCcfg(CarbonCfg* cfg) { mCfg=cfg; }

  void setEditor(CompWizardMemEditor* editor) { mEditor=editor; }
  CompWizardMemEditor* getEditor() const { return mEditor; }
  
  void closeEditor();
  void expandChildren(QTreeWidgetItem* item, QList<MemEditorTreeItem::NodeType>& nonExpandNodes);
  void expandChildren(QTreeWidgetItem* item);
  void collapseChildren(QTreeWidgetItem* item);
  void sortChildren(QTreeWidgetItem* item);
  void setExpanded(QTreeWidgetItem* item, bool expand);

  void setModified(bool value)
  {
    emit widgetModified(value);
  }

  void removeItem(QTreeWidgetItem* item)
  {
    QTreeWidgetItem* parent = item->parent();
    if (parent)
      parent->removeChild(item);
    else
      INFO_ASSERT(true, "Unexpected remove");
  }

  void removeItem(const MemTreeIndex& index)
  {
    QTreeWidgetItem* item = itemFromIndex(index);
    removeItem(item);
  }

  MemTreeIndex indexFromItem(QTreeWidgetItem *item, int column = 0) const
  {
    MemTreeIndex ti;
    do
    {
      ti.addPath(item->text(column));
      item = item->parent();
    }
    while (item);

    return ti;
  }

  QTreeWidgetItem *itemFromIndex(const MemTreeIndex &index) const
  {
    QTreeWidgetItem* item = NULL;
    for (int i=0; i<index.numItems(); i++)
    {
      QString itemName = index.itemAt(i);
      if (i == 0)
      {
        QList<QTreeWidgetItem*> items = findItems(itemName, Qt::MatchExactly);
        INFO_ASSERT(items.count() == 1, "Expecting single match");
        item = items[0];
      }
      else
      {
        for (int ci=0; ci<item->childCount(); ci++)
        {
          QTreeWidgetItem* childItem = item->child(ci);
          if (childItem->text(0) == itemName)
          {
            item = childItem;
            break;
          }
        }
      }
    }
    return item;
  }
  
  quint32 numSelectedItems();
  quint32 numSelectedItems(MemEditorTreeItem::NodeType nodeType);
  quint32 fillSelectedItems(QList<MemEditorTreeItem*>& items, MemEditorTreeItem::NodeType nodeType);
  quint32 findNodes(QList<MemEditorTreeItem*>& items, MemEditorTreeItem::NodeType nodeType);

  void setEditorWidget(QWidget* w) { mLastEditor=w; }

private slots:
  void itemExpanded(QTreeWidgetItem*);
  void itemCollapsed(QTreeWidgetItem*);
  void itemDoubleClicked(QTreeWidgetItem*, int);

signals:
  void widgetModified(bool);

private: // methods
  void clearAnimation();
  void animateDropSite(const QPoint& pos);
  void findNodeHelper(QList<MemEditorTreeItem*>& items, MemEditorTreeItem* parent, MemEditorTreeItem::NodeType nodeType);

private:
  QWidget* mLastEditor;
  QUndoStack* mUndoStack;
  CarbonCfg* mCfg;
  CompWizardMemEditor* mEditor;
  QPoint mDragStartPos;
  
#define DD_MAX_COLUMNS 10
  QTreeWidgetItem* mDropSiteItem;
  QBrush mDropSaveBrush[DD_MAX_COLUMNS];
  bool mInternalDrag;
  QList<MemEditorTreeItem*> mSelectedItems;
};


class MemTreeUndoCommand : public QUndoCommand
{
public:
  MemTreeUndoCommand(MemEditorTreeItem* item, QUndoCommand* parent);
  MemTreeUndoCommand(MemEditorTreeWidget* tree, QUndoCommand* parent);

  MemEditorTreeItem* getItem();
  MemEditorTreeWidget* getTree() { return mTree; }
  void updateTreeIndex(MemEditorTreeItem* item);
  void setModified(bool)
  {
    getTree()->setModified(true);
  }
private:
  MemEditorTreeWidget* mTree;
  MemTreeIndex mTreeIndex;
  bool mInitialModified;
};

// A frame of commands to undo
class UndoCommandFrame : public MemTreeUndoCommand
{
public:
  UndoCommandFrame(MemEditorTreeItem* item, QUndoCommand* parent);
  UndoCommandFrame(MemEditorTreeWidget* tree, QUndoCommand* parent);

  void addCommand(MemTreeUndoCommand* cmd)
  {
    mCommands.append(cmd);
  }

  void undo();
  void redo();

private:
  QList<MemTreeUndoCommand*> mCommands;

};

class MemUndoData
{
public:
  MemUndoData()
  {
    mTree = NULL;
  }
  virtual ~MemUndoData()
  {
  }

  virtual void undo()=0;
  virtual void redo()=0;

  void setTreeData(QTreeWidget* tree, QTreeWidgetItem* node = NULL)
  {
    mTree = dynamic_cast<MemEditorTreeWidget*>(tree);
    if (node)
    {
      if (node->parent())
        mParentIndex = mTree->indexFromItem(node->parent());
      mItemIndex = mTree->indexFromItem(node);
    }
  }

  void setText(const QString& newVal) { mText=newVal; }
  QString text() const { return mText; }

  MemEditorTreeWidget* getTree() { return mTree; }


  MemEditorTreeItem* getParentItem() 
  {
    return dynamic_cast<MemEditorTreeItem*>(mTree->itemFromIndex(mParentIndex)); 
  }

  MemEditorTreeItem* getItem()
  {
    return dynamic_cast<MemEditorTreeItem*>(mTree->itemFromIndex(mItemIndex)); 
  }


private:
  QString mText;
  MemEditorTreeWidget* mTree;
  MemTreeIndex mParentIndex;
  MemTreeIndex mItemIndex;
};

class MemCmdDeleteItems : public QUndoCommand
{
public:
  MemCmdDeleteItems(QList<MemEditorTreeItem*> items, QUndoCommand* parent = 0);

  void undo()
  {
    foreach (MemUndoData* undoData, mUndoDataList)
    {
      if (undoData)
        undoData->undo();  
    }
  }

  void redo()
  {
    foreach (MemUndoData* undoData, mUndoDataList)
    {
      if (undoData)
        undoData->redo();  
    }
  }

  int numItems() { return mUndoDataList.count(); }

private:
  QList<MemUndoData*> mUndoDataList;
};




#endif
