/****************************************************************************
**
** Copyright (C) 2005-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "util/CarbonPlatform.h"


#include <QtGui>
#include <QMenuBar>
#include <QToolBar>

#include "gui/CQt.h"
#include "../gui/VerilogSyntax.h"

#include "util/UtString.h"
#include "util/UtIStream.h"

#include "TextEditor.h"
#include "MdiTextEditor.h"


TextEditor::TextEditor(MDIDocumentTemplate* t, QWidget* parent) : QsciScintilla(parent)
{
  setAttribute(Qt::WA_DeleteOnClose);
  isUntitled = true;

  mCurFile.clear();

  initializeMDI(this, t);
  
  setWrapMode(QsciScintilla::WrapNone);
  fixedWidth();

  QPalette pal = palette();
  pal.setColor(QPalette::Active, QPalette::Highlight, Qt::blue);
  pal.setColor(QPalette::Active, QPalette::HighlightedText, Qt::yellow);
  pal.setColor(QPalette::Inactive, QPalette::Highlight, Qt::red);
  pal.setColor(QPalette::Inactive, QPalette::HighlightedText, Qt::green);
  setPalette(pal);

 // mMarker = markerDefine(QsciScintilla::RightArrow );

  QPixmap pm(":/cmm/Resources/GoToNextHS.png");
  mMarker = markerDefine(pm);

  mDlgFind = NULL;
}

void TextEditor::fileSave()
{
 save(); 
}

void TextEditor::newFile()
{
  static UInt32 sequenceNumber = 1;

  isUntitled = true;
  
  mCurFile.clear();
  mCurFile << "document" << sequenceNumber++ << ".txt";

  UtString title;
  title << mCurFile << "[*]";

  setWindowTitle(title.c_str());

  CQT_CONNECT(this, textChanged(), this, documentWasModified());

  verilogSyntax();
}

QMenuBar* TextEditor::menuBar()
{
  return mMenu;
}

bool TextEditor::loadFile(const char* fileName)
{
  QFile file(fileName);
  if (!file.open(QFile::ReadOnly | QFile::Text)) {
    QMessageBox::warning(this, tr("MDI"),
      tr("Cannot read file %1:\n%2.")
      .arg(fileName)
      .arg(file.errorString()));
    return false;
  }

  QTextStream in(&file);
  QApplication::setOverrideCursor(Qt::WaitCursor);
  setText(in.readAll());
  QApplication::restoreOverrideCursor();

  UtString fname;
  fname << fileName;
  if (strstr(fname.c_str(), ".vhd") != NULL) 
  {
    vhdlSyntax();
  }
  else if (strstr(fname.c_str(), ".v") != NULL)
  {
    verilogSyntax();
  }

  setCurrentFile(fileName);

  CQT_CONNECT(this, textChanged(), this, documentWasModified());

  UtString linesText;

  linesText << lines() << "9";

  setMarginWidth(0, linesText.c_str());
  setMarginLineNumbers(0, true);

  return true;
}

bool TextEditor::save()
{
  if (isUntitled) {
    return saveAs();
  } else {
    return saveFile(mCurFile.c_str());
  }
}

void TextEditor::updateMenusAndToolbars()
{
  emit undoAvailable(isUndoAvailable());
  emit redoAvailable(isRedoAvailable());
  emit pasteAvailable(true);
  emit copyAvailable(true); // hasSelectedText()
}


bool TextEditor::saveAs()
{
  QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), mCurFile.c_str());
  if (fileName.isEmpty())
    return false;

  UtString f;
  f << fileName;

  return saveFile(f.c_str());
}

bool TextEditor::saveFile(const char* fileName)
{
  QFile file(fileName);
  if (!file.open(QFile::WriteOnly | QFile::Text)) {
    QMessageBox::warning(this, tr("MDI"),
      tr("Cannot write file %1:\n%2.")
      .arg(fileName)
      .arg(file.errorString()));
    return false;
  }

  QTextStream out(&file);
  QApplication::setOverrideCursor(Qt::WaitCursor);
  out << text();
  QApplication::restoreOverrideCursor();

  setCurrentFile(fileName);
  return true;
}

void TextEditor::closeEvent(QCloseEvent *event)
{
  if (maybeSave())
  {
    event->accept();
    QVariant qv = this->property("CarbonTabWidget");
    QVariant qvPlaceholder = this->property("CarbonTabPlaceholder");

    if (qv.isValid() && qvPlaceholder.isValid())
    {
      QTabWidget* tw = (QTabWidget*)qv.value<void*>();
      QWidget* w = (QWidget*)qvPlaceholder.value<void*>();

      int tabIndex = tw->indexOf(w);
      if (tabIndex != -1)
        tw->removeTab(tabIndex);
    }
  }
  else
  {
    event->ignore();
  }
}

void TextEditor::documentWasModified()
{
  setWindowModified(isModified());

  updateMenusAndToolbars();
}

bool TextEditor::maybeSave()
{
  if (isModified()) {
    QMessageBox::StandardButton ret;
    ret = QMessageBox::warning(this, tr("MDI"),
      tr("'%1' has been modified.\n"
      "Do you want to save your changes?")
      .arg(userFriendlyCurrentFile()),
      QMessageBox::Save | QMessageBox::Discard
      | QMessageBox::Cancel);
    if (ret == QMessageBox::Save)
      return save();
    else if (ret == QMessageBox::Cancel)
      return false;
  }
  return true;
}

void TextEditor::setCurrentFile(const char* fileName)
{
  QString cf =  QFileInfo(fileName).absoluteFilePath();
  mCurFile.clear();
  mCurFile << cf;
  isUntitled = false;
  setModified(false);
  setWindowModified(false);
  UtString title;
  title << userFriendlyCurrentFile() << "[*]";

  setWindowTitle(title.c_str());
}

void TextEditor::find()
{
  if (mDlgFind == NULL)
  {
    mDlgFind = new DlgFindText(this);
    mDlgFind->setEditor(this);
    CQT_CONNECT(mDlgFind, rejected(), this, findDialogDestroyed());
    mDlgFind->show();
  }
  else
    mDlgFind->raise();
}

void TextEditor::findDialogDestroyed() 
{
  mDlgFind = NULL;
}

void TextEditor::updateUndoStatus()
{
  QAction* action = qobject_cast<QAction*>(sender());
  if (action)
  {
     action->setEnabled(isUndoAvailable());
  }
}

void TextEditor::updateRedoStatus()
{
  QAction* action = qobject_cast<QAction*>(sender());
  if (action)
  {
    action->setEnabled(isRedoAvailable());
  }
}

const char* TextEditor::userFriendlyCurrentFile()
{
  return strippedName(mCurFile.c_str());
}

const char* TextEditor::strippedName(const char* fullFileName)
{
  static UtString x;
  x.clear();
  x << QFileInfo(fullFileName).fileName();
  return x.c_str();
}


void TextEditor::verilogSyntax() 
{
  QFont f;
  f.setFamily("Courier");
  f.setFixedPitch(true);
#if defined(Q_OS_WIN32)
  f.setPointSize(10);
#else
  f.setPointSize(12);
#endif
  QsciLexerVerilog* vlex = new QsciLexerVerilog();
  vlex->setDefaultFont(f);
  setLexer(vlex);
}

void TextEditor::vhdlSyntax()
{
  QFont f;
  f.setFamily("Courier");
  f.setFixedPitch(true);
#if defined(Q_OS_WIN32)
  f.setPointSize(10);
#else
  f.setPointSize(12);
#endif

  QsciLexerVHDL* vlex = new QsciLexerVHDL();
  vlex->setDefaultFont(f);
  setLexer(vlex);  
}

void TextEditor::gotoLine(UInt32 line) 
{  
  int adjustedLine = line-1;

  int oldPolicy = SendScintilla(SCI_SETYCARETPOLICY, CARET_STRICT|CARET_EVEN);

  markerDeleteAll();

  markerAdd(adjustedLine, mMarker);
  ensureLineVisible(adjustedLine);
  setCursorPosition(adjustedLine,0);
  ensureCursorVisible();
  SendScintilla(SCI_SETYCARETPOLICY, oldPolicy);

  setFocus();
}

void TextEditor::fixedWidth() 
{
}
