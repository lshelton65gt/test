//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "ScrProject.h"
#include "cmm.h"
#include "CarbonOptions.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"
#include "DesignXML.h"
#include "CarbonFiles.h"
#include "MaxsimComponent.h"
#include "CowareComponent.h"
#include "MVComponent.h"
#include "SystemCComponent.h"
#include "CarbonComponent.h"
#include "CarbonComponents.h"
#include "JavaScriptAPIs.h"
#include "EmbeddedMono.h"

#if 1
#include "classGen/ScrProject_wrapper.cxx"
#endif

Q_DECLARE_METATYPE(ScrProject*);

extern "C"
{
  PINVOKEABLE char* CSharp_Test_ReturnString()
  {    
    QString sv = "test";
    return EmbeddedMono::AllocString(sv);     
  }

  PINVOKEABLE void CSharp_Test_ReturnString_out(char** str)
  {    
    const char* sv = "test";
    char* v = EmbeddedMono::AllocString(sv);
    *str = v;
  }


   PINVOKEABLE char* CSharp_Test_ReturnString1(const char* inputStr)
  {    
    QString sv = QString("Test cat %1").arg(inputStr);
    return EmbeddedMono::AllocString(sv);     
  }

}

MaxsimComponent* ScrProject::getMaxsimComp()
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  return pw->getActiveMaxsimComponent();
}

CowareComponent* ScrProject::getCowareComp()
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  return pw->getActiveCowareComponent();
}

SystemCComponent* ScrProject::getSystemCComp()
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  return pw->getActiveSystemCComponent();
}

void ScrProject::registerScriptTypes(QMap<QString, const QMetaObject*>& map)
{
  map["ProjectEnum"] = &ProjectEnum::staticMetaObject;
  map["ScrProject*"] = &ScrProject::staticMetaObject;
  map["XDesignHierarchy*"] = &XDesignHierarchy::staticMetaObject;
  map["MaxsimComponent*"] = &MaxsimComponent::staticMetaObject;
  map["CowareComponent*"] = &CowareComponent::staticMetaObject;
  map["SystemCComponent*"] = &SystemCComponent::staticMetaObject;
  map["CarbonComponent*"] = &CarbonComponent::staticMetaObject;
  map["CarbonOptions*"] = &CarbonOptions::staticMetaObject;
  map["CarbonPropertyValue*"] = &CarbonPropertyValue::staticMetaObject;
}

void ScrProject::registerIntellisense(JavaScriptAPIs* apis)
{
  apis->loadAPIForVariable("Project", &ScrProject::staticMetaObject);
  apis->loadAPIForVariable("Project::designHierarchy", &XDesignHierarchy::staticMetaObject);
  apis->loadAPIForVariable("Project::designHierarchy::top", &XToplevelInterface::staticMetaObject);

  apis->loadAPIForVariable("ProjectEnum", &ProjectEnum::staticMetaObject, true);

  apis->loadAPIForVariable("Project.options", &CarbonOptions::staticMetaObject, true);
  apis->loadAPIForVariable("Project.compilerOptions", &CarbonOptions::staticMetaObject, true);

}

void ScrProject::registerTypes(QScriptEngine* engine)
{
  // Register Types of properties returned
  qScriptRegisterQObjectMetaType<ScrProject*>(engine);
  qScriptRegisterQObjectMetaType<SystemCComponent*>(engine);
  qScriptRegisterQObjectMetaType<MaxsimComponent*>(engine);
  qScriptRegisterQObjectMetaType<CowareComponent*>(engine);
  qScriptRegisterQObjectMetaType<CarbonOptions*>(engine);
  qScriptRegisterQObjectMetaType<CarbonProperty*>(engine);

  // Register Enums

  // Q_DECLARE_METATYPE(ProjectEnum::ComponentType)
  qScriptRegisterEnumMetaType<ProjectEnum::ComponentType>(engine); 

  // Static register for classes with Q_ENUMS
  QScriptValue projEnums = engine->newQMetaObject(&ProjectEnum::staticMetaObject);
  engine->globalObject().setProperty("ProjectEnum", projEnums);

  QScriptValue componentStatic = engine->newQMetaObject(&CarbonComponent::staticMetaObject);
  engine->globalObject().setProperty("Component", componentStatic);
}

CarbonOptions* ScrProject::getOptions() const
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  return proj->getProjectOptions();
}

CarbonOptions* ScrProject::getCompilerOptions() const
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  return proj->getActive()->getOptions("VSPCompiler");
}

QString ScrProject::getHashValue(const QString& pathName) const
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  return proj->hashFile(pathName);
}

void ScrProject::addSourceFile(const QString& fileName, const QString& folderName, bool libraryFile)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  proj->addSourceFile(fileName, fileName, libraryFile, true, folderName);
}

QString ScrProject::getBaseName() const
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  const char* iodbName = proj->getDesignFilePath(".symtab.db");
  QFileInfo fi(iodbName);
  return fi.baseName();
}

QString ScrProject::getDesignFileName(const QString& ext) const 
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  UtString extension;
  extension << ext;
  const char* fileName = proj->getDesignFilePath(extension.c_str());

  return fileName;
}
QString ScrProject::projectDirectory() const
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QString projDir = proj->getProjectDirectory();
  return projDir;
}

QString ScrProject::outputDirectory() const
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QString outputDir = proj->getActive()->getOutputDirectory();
  return outputDir;
}

XDesignHierarchy* ScrProject::designHierarchy() const
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  if (proj)
  {
    XDesignHierarchy* dh = NULL;
    const char* designHierarchyFile = proj->getDesignFilePath(".designHierarchy");
    QFileInfo fi(designHierarchyFile);
    if (fi.exists())
    {
      dh = new XDesignHierarchy(designHierarchyFile);
      XmlErrorHandler* eh = dh->getErrorHandler();      
      if (eh->hasErrors())
      {
        QString msg = QString("Error reading design hierarchy: %1\n%2")
          .arg(designHierarchyFile)
          .arg(eh->errorMessages().join("\n"));
        QMessageBox::critical(NULL, MODELSTUDIO_TITLE, msg);
        return NULL;
      }
    }
    return dh;
  }
  else
    return NULL;
}

QObject* ScrProject::getComponent(quint32 index)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  if (proj)
  {
    if (index <= numComponents())
      return proj->getComponents()->getComponent(index);
  }

  return NULL;
}

bool ScrProject::deleteComponent(ProjectEnum::ComponentType compType)
{
  CarbonComponent::ComponentType type = CarbonComponent::ComponentType(compType);
  qDebug() << "delete component type" << type;
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  if (proj)
  {
    switch (type)
    {
    case CarbonComponent::SoCDesigner:
      if (pw->getActiveMaxsimComponent() != NULL)
      {
        MaxsimComponent* maxsimComp = pw->getActiveMaxsimComponent();
        MaxsimTreeItem* item = maxsimComp->getRootItem();
        item->deleteComponentBatch();
      }
      break;

    case CarbonComponent::CoWare:
      if (pw->getActiveCowareComponent() != NULL)
      {      
        CowareComponent* cowareComp = pw->getActiveCowareComponent();
        CowareTreeItem* item = cowareComp->getRootItem();
        item->deleteComponentBatch();
      }
      break;    

    case CarbonComponent::SystemC:
      if (pw->getActiveSystemCComponent() != NULL)
      {      
        SystemCComponent* systemCComp = pw->getActiveSystemCComponent();
        SystemCTreeItem* item = systemCComp->getRootItem();
        item->deleteComponentBatch();
      }
      break;
    default:
      qDebug() << "not deleting component type" << type;
      break;
    }
  }

  return false;
}
QObject* ScrProject::createComponent(ProjectEnum::ComponentType compType)
{
  CarbonComponent::ComponentType type = CarbonComponent::ComponentType(compType);
  
  qDebug() << "create component type" << type;

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  if (proj)
  {
    CarbonComponent* comp = NULL;
    switch (type)
    {
    case CarbonComponent::SoCDesigner:
      if (pw->getActiveMaxsimComponent() == NULL)
      {
        pw->createComponent(CarbonProjectWidget::eComponentMaxsim, NULL);
        MaxsimComponent* maxsimComp = pw->getActiveMaxsimComponent();
        if (maxsimComp)
          maxsimComp->createDefaultComponent();
        comp = maxsimComp;
      }
      else
        context()->throwError(QScriptContext::UnknownError, 
          QString("Soc Designer Component Already Exists"));
      break;
    case CarbonComponent::CoWare:
      if (pw->getActiveCowareComponent() == NULL)
      {
        pw->createComponent(CarbonProjectWidget::eComponentCoWare, NULL);
        CowareComponent* cowareComp = pw->getActiveCowareComponent();
        if (cowareComp)
          cowareComp->createDefaultComponent();
        comp = cowareComp;
      }
      else
        context()->throwError(QScriptContext::UnknownError, 
          QString("CoWare Component Already Exists"));

      break;
    case CarbonComponent::SystemC:
      if (pw->getActiveSystemCComponent() == NULL)
      {
        pw->createComponent(CarbonProjectWidget::eComponentSystemC, NULL);
        comp = pw->getActiveSystemCComponent();
      }
      else
        context()->throwError(QScriptContext::UnknownError, 
          QString("SystemC Component Already Exists"));
      break;
    case CarbonComponent::ModelValidation:
      if (pw->getActiveMVComponent() == NULL)
      {
        pw->createComponent(CarbonProjectWidget::eComponentModelValidation, NULL);
        comp = pw->getActiveMVComponent();
      }
      else
        context()->throwError(QScriptContext::UnknownError, 
          QString("ModelValidation Component Already Exists"));
      break;
    default:
      qDebug() << "not creating component type" << type;
      break;
    }

    if (comp)
      proj->getComponents()->populateComponentTree(pw, NULL);

    return comp;
  }
  else
    qDebug() << "error createComponent no project loaded";

  return NULL;
}

void ScrProject::save()
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  pw->saveProject();
}

quint32 ScrProject::numComponents() const
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  if (proj)
  {
    return proj->getComponents()->numComponents();
  }
  else
    return 0;
}

QString ScrProject::methodDescr(const QString& methodName, bool formatHtml) const
{
  qDebug() << "doc" << methodName << formatHtml;

  METHOD_DESCR("numComponents", "The number of components created in this Project");
  METHOD_DESCR("getBaseName", 
    "The base name of the compiled Carbon model, for example, if the design "
    "was libDesign.a this method would return the string \"Design\""
    );
  METHOD_DESCR_ALSO("createComponent", 
    "Creates a Component of type <i>type</i> and returns the newly created component",
    methodLink("numComponents,createComponent,Application::compile")+propertyLink("CoWare,SoCDesigner,SystemC")
    );
  METHOD_DESCR_ALSO("getComponent",
    "Returns the <i>index</i>th component in the 0 based "
    "collection of components", 
    methodLink("numComponents,createComponent,Application::compile")+propertyLink("CoWare,SoCDesigner,SystemC")
    );
  METHOD_DESCR("save", 
    "Saves the Carbon project file (.carbon) and generates the makefile(s)"
    );

  return QString(); 
}
  
QString ScrProject::propertyDescr(const QString& propName, bool formatHtml) const
{
  qDebug() << "doc" << propName << formatHtml;

  PROP_DESCR("CoWare", 
    "Returns CoWare Component for this Project, or null if none created.");
  PROP_DESCR("SoCDesigner", 
    "Returns SoCDesigner Component for this Project, or null if none created.");
  PROP_DESCR("SystemC", 
    "Returns SystemC Component for this Project, or null if none created.");

  return QString(); 
}
