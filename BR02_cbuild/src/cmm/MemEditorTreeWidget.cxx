//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "MemEditorTreeWidget.h"
#include "MemEditorTreeNodes.h"
#include "CompWizardMemEditor.h"
#include "CcfgHelper.h"
#include "MemEditorCommands.h"

MemEditorTreeWidget::MemEditorTreeWidget(QWidget* parent)
  : QTreeWidget(parent)
{
  mEditor = NULL;
  mLastEditor = NULL;
  mDropSiteItem = NULL;
  mInternalDrag = false;

  CQT_CONNECT(this, itemExpanded(QTreeWidgetItem*), this, itemExpanded(QTreeWidgetItem*));
  CQT_CONNECT(this, itemCollapsed(QTreeWidgetItem*), this, itemCollapsed(QTreeWidgetItem*));
  CQT_CONNECT(this, itemDoubleClicked(QTreeWidgetItem*, int), this, itemDoubleClicked(QTreeWidgetItem*, int));
}


void MemEditorTreeItem::setNormalText(int col, const QString& text)
{
  QFont f = font(col);
  f.setBold(false);
  setFont(col, f);  
  setText(col, text);
  setTextColor(col, getTree()->getDefaultTextColor());
}

void MemEditorTreeItem::setNormalItalicText(int col, const QString& text)
{
  QFont f = font(col);
  f.setBold(false);
  f.setItalic(true);
  setFont(col, f);  
  setText(col, text);
  setTextColor(col, getTree()->getDefaultTextColor());
}

void MemEditorTreeItem::setColoredItalicText(int col, const QString& text, Qt::GlobalColor color)
{
  QFont f = font(col);
  f.setBold(false);
  f.setItalic(true);
  setFont(col, f);  
  setText(col, text);
  setTextColor(col, QColor(color));
}

void MemEditorTreeItem::setColoredItalicText(int col, const QString& text, const QColor& color)
{
  QFont f = font(col);
  f.setBold(false);
  f.setItalic(true);
  setFont(col, f);  
  setText(col, text);
  setTextColor(col, color);
}

void MemEditorTreeItem::setColoredNormalText(int col, const QString& text, Qt::GlobalColor color)
{
  QFont f = font(col);
  f.setBold(false);
  f.setItalic(false);
  setFont(col, f);  
  setText(col, text);
  setTextColor(col, QColor(color));
}

void MemEditorTreeItem::setColoredNormalText(int col, const QString& text, const QColor& color)
{
  QFont f = font(col);
  f.setBold(false);
  f.setItalic(false);
  setFont(col, f);  
  setText(col, text);
  setTextColor(col, color);
}

void MemEditorTreeItem::setColoredBoldText(int col, const QString& text, Qt::GlobalColor color)
{
  QFont f = font(col);
  f.setBold(true);
  f.setItalic(false);
  setFont(col, f);  
  setText(col, text);
  setTextColor(col, QColor(color));
}

void MemEditorTreeItem::setColoredBoldText(int col, const QString& text, const QColor& color)
{
  QFont f = font(col);
  f.setBold(true);
  f.setItalic(false);
  setFont(col, f);  
  setText(col, text);
  setTextColor(col, color);
}


void MemEditorTreeItem::showErrorText(int col)
{
  setTextColor(col, QColor(Qt::red));
}

void MemEditorTreeItem::showWarningText(int col)
{
  setTextColor(col, QColor(Qt::magenta));
}

void MemEditorTreeItem::showNormalText(int col)
{
  setTextColor(col, getTree()->getDefaultTextColor());
}




QColor MemEditorTreeWidget::getDefaultTextColor()
{
  // Get the default text color of a tree widget item
  QTreeWidget tmpTree;
  tmpTree.setColumnCount(1);
  QTreeWidgetItem tmpItem(&tmpTree);
  return tmpItem.textColor(0);
}


bool MemEditorTreeWidget::canDropHere(const QString& nets, QDragMoveEvent* ev)
{
  CcfgHelper ccfg(getCcfg());

  QStringList netList = nets.split("\n");
  
  int goodNets = 0;
  foreach(QString net, netList)
  {
    UtString uNetName; uNetName << net;
    const CarbonDBNode* node = carbonDBFindNode(getCcfg()->getDB(), uNetName.c_str());
    bool isMemory = false;
    if (node && carbonDBIs2DArray(getCcfg()->getDB(), node) && !carbonDBIsContainedByComposite(getCcfg()->getDB(), node))
      isMemory = true;

    if (isMemory)
      goodNets++;
  }

 ///* if (goodNets > 0)
 // {
 //   QPoint pos = ev->pos();
 //   QString msg;
 //   if (goodNets == 1)
 //     msg = QString("Dropping here will create a new memory");
 //   else
 //     msg = QString("Dropping here will create a new memories");

 //   QToolTip::showText(viewport()->mapToGlobal(pos), msg, this);
 // }*/

  if (goodNets == netList.count())
    ev->setDropAction(Qt::CopyAction);
  else if (goodNets > 0)
    ev->setDropAction(Qt::MoveAction);
  else
  {
    ev->setDropAction(Qt::IgnoreAction);
    return false;
  }

  return true;
}


bool MemEditorTreeWidget::canDropHere(const QList<MemEditorTreeItem*>&, QDragMoveEvent*)
{
  qDebug() << "internal drag wanting to drop on empty....";
  return false;
}

void MemEditorTreeWidget::itemDoubleClicked(QTreeWidgetItem* treeItem, int)
{
  MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(treeItem);
  if (item)
    item->doubleClicked();
}

void MemEditorTreeWidget::closeTree()
{
  // Free static data
  
}


void MemEditorTreeItem::init()
{
  mTreeWidget = NULL;

  mIconMemory = QIcon(":/cmm/Resources/memoryMap.png");
  mIconBlock = QIcon(":/cmm/Resources/bank.png");

  mIconLocRTL = QIcon(":/cmm/Resources/field.png");
  mIconLocUser = QIcon(":/cmm/Resources/output.png");
  mIconLocPort = QIcon(":/cmm/Resources/input.png");
}

bool MemEditorTreeItem::operator<(const QTreeWidgetItem &o) const
{
  int column = treeWidget()->sortColumn();
  const MemEditorTreeItem& other = dynamic_cast<const MemEditorTreeItem&>(o);

  //if (other.mNodeType == MemEditorTreeItem::XtorInstParams)
  //  return false;

  //if (mNodeType == MemEditorTreeItem::XtorInstParams)
  //  return true;

  return text(column) < other.text(column);
}


void MemEditorTreeWidget::expandChildren(QTreeWidgetItem* item, QList<MemEditorTreeItem::NodeType>& nonExpandNodes)
{
  MemEditorTreeItem* treeItem = dynamic_cast<MemEditorTreeItem*>(item);
  if (treeItem)
  {
    bool expandIt = true;
    MemEditorTreeItem::NodeType nodeType = treeItem->getNodeType();
    foreach(MemEditorTreeItem::NodeType ntype, nonExpandNodes)
    {
      if (ntype == nodeType)
      {
        expandIt = false;
        break;
      }
    }

    if (expandIt)
    {
      expandItem(treeItem);

      for (int i=0; i<treeItem->childCount(); i++)
        expandChildren(treeItem->child(i), nonExpandNodes);
    }
  }
}


void MemEditorTreeWidget::expandChildren(QTreeWidgetItem* item)
{
  if (item)
  {
    expandItem(item);
    for (int i=0; i<item->childCount(); i++)
      expandChildren(item->child(i));
  }
}

void MemEditorTreeWidget::collapseChildren(QTreeWidgetItem* item)
{
  if (item)
  {
    collapseItem(item);
    for (int i=0; i<item->childCount(); i++)
      collapseChildren(item->child(i));
  }
}

void MemEditorTreeWidget::setExpanded(QTreeWidgetItem* item, bool expand)
{
  if (expand)
    expandItem(item);
  else
    collapseItem(item);
}
void MemEditorTreeWidget::sortChildren(QTreeWidgetItem* item)
{
  setUpdatesEnabled(false);
  bool expanded = item->isExpanded();
  collapseItem(item);
  item->sortChildren(0, Qt::AscendingOrder);
  if (expanded)
    expandItem(item);
  setUpdatesEnabled(true);
}


void MemEditorTreeWidget::closeEditor()
{
  if (mLastEditor)
  {
    MemEditorDelegate* de = static_cast<MemEditorDelegate*>(itemDelegate());
    de->commit(mLastEditor);
    mLastEditor = NULL;
  }
}

void MemEditorTreeWidget::itemExpanded(QTreeWidgetItem* treeItem)
{
  MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(treeItem);
  if (item && item->resizeOnExpand())
    header()->resizeSections(QHeaderView::ResizeToContents);
}

void MemEditorTreeWidget::itemCollapsed(QTreeWidgetItem* treeItem)
{
  MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(treeItem);
  if (item && item->resizeOnCollapse())
    header()->resizeSections(QHeaderView::ResizeToContents);
}

// Drag/Drop

// Note: Call base class methods first, otherwise, autoScroll won't work.
void MemEditorTreeWidget::dragEnterEvent(QDragEnterEvent * event)
{
  QTreeWidget::dragEnterEvent(event);

  animateDropSite(event->pos());

  event->acceptProposedAction();
}
void MemEditorTreeWidget::dragLeaveEvent(QDragLeaveEvent * event)
{
  QTreeWidget::dragLeaveEvent(event);

  clearAnimation();
  
  //mInternalDrag = false;

  qDebug() << "dragLeaveEvent";
}
void MemEditorTreeWidget::dragMoveEvent(QDragMoveEvent * event)
{
  QTreeWidget::dragMoveEvent(event);

  animateDropSite(event->pos());

  QTreeWidgetItem* treeItem = itemAt(event->pos().x(), event->pos().y());
  if (treeItem)
    qDebug() << "Dragging over" << treeItem->text(0) << "internal drag" << mInternalDrag;

  if (treeItem)
  {
    MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(treeItem);
    if (mInternalDrag)
    {
      if (item->canDropHere(mSelectedItems, event))
        event->accept();
      else
        event->ignore();
    }
    else
    {
      if (item->canDropHere(event->mimeData()->text(), event))
        event->accept();
      else
        event->ignore();
    }
  }
  else
  {
    if (mInternalDrag)
    {
      if (canDropHere(mSelectedItems, event))
        event->accept();
      else
        event->ignore();
    }  
    else
    {
      if (canDropHere(event->mimeData()->text(), event))
        event->accept();
      else
        event->ignore(); 
    }
  }
}
void MemEditorTreeWidget::dropEvent(QDropEvent * event)
{
  clearAnimation();
  qDebug() << "dropEvent";

  QTreeWidgetItem* treeItem = itemAt(event->pos().x(), event->pos().y());
  if (mInternalDrag && treeItem)
  {
    MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(treeItem);
    if (item)
      item->dropItems(mSelectedItems);
    event->acceptProposedAction();
  }
  else if (treeItem && event->mimeData()->text().length() > 0)
  {
    MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(treeItem);
    if (item)
      item->dropItem(event->mimeData()->text());
    event->acceptProposedAction();
  }
  else if (treeItem == NULL)
  {
    if (mInternalDrag)
      dropItem(mSelectedItems);
    else
      dropItem(event->mimeData()->text());
    event->acceptProposedAction();
  }

  mInternalDrag = false;
  qDebug() << "clearing internal drag";
}

void MemEditorTreeWidget::dropItem(const QList<MemEditorTreeItem*>&)
{
}

void MemEditorTreeWidget::dropItem(const QString& memoryName)
{
  qDebug() << "dropped" << memoryName;
  getUndoStack()->push(new CmdAddESLMemory(getEditor()->getMemoriesRoot(), getCcfg(), memoryName));     
}

void MemEditorTreeWidget::mousePressEvent(QMouseEvent* event)
{
  QTreeWidget::mousePressEvent(event);

  QTreeWidgetItem* item = itemAt(event->x(), event->y());

  if (item && event->button() == Qt::LeftButton)
  {
    // Immediate drag
    if (!item->isSelected())
    {
      clearSelection();
      qDebug() << "Selecting item";
      //item->setSelected(true);
      setCurrentItem(item);
    }

    mDragStartPos = event->pos();
  }
}
void MemEditorTreeWidget::mouseReleaseEvent(QMouseEvent* event)
{
  qDebug() << "mouseRelease, clearing internal drag";
  mInternalDrag = false;
  QTreeWidget::mouseReleaseEvent(event);
}

void MemEditorTreeWidget::clearAnimation() 
{
  if (mDropSiteItem != NULL) 
  {
    // Max 10 columns
    for (int i = 0; i < columnCount() && i < DD_MAX_COLUMNS; ++i)
      mDropSiteItem->setBackground(i, mDropSaveBrush[i]);
    mDropSiteItem = NULL;
  }
}

void MemEditorTreeWidget::animateDropSite(const QPoint& pos) 
{
  QTreeWidgetItem* item = itemAt(pos.x(), pos.y());
  
  if (mDropSiteItem != item)
  {
    clearAnimation();
    mDropSiteItem = item;

    if (item != NULL)
    {
      QToolTip::showText(viewport()->mapToGlobal(pos), item->text(0), this);
      for (int i = 0; i < columnCount() && i < DD_MAX_COLUMNS; ++i) 
        mDropSaveBrush[i] = item->background(i);

      QColor dropHighlightColor = QColor(Qt::green);
      if (isItemSelected(item))
        dropHighlightColor = QColor(Qt::darkGreen);

      for (int i = 0; i < columnCount() && i < DD_MAX_COLUMNS; ++i) 
        item->setBackgroundColor(i, dropHighlightColor);
    }
  }
}


void MemEditorTreeWidget::mouseMoveEvent(QMouseEvent* event)
{
  if (!event->buttons() & Qt::LeftButton)
    return;

  if ((event->pos() - mDragStartPos).manhattanLength() < QApplication::startDragDistance())
    return;

  mSelectedItems.clear();

  // Begin drag
  if (selectedItems().count() > 0)
  {
    foreach(QTreeWidgetItem* treeItem, selectedItems())
    {
      MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(treeItem);
      if (item && item->isDragSource())
        mSelectedItems.append(item);
    }

    if (mSelectedItems.count() > 0)
    {
      mInternalDrag = true;
      qDebug() << "Begin Drag" << selectedItems().count() << "items";
      QDrag* drag = new QDrag(this);
      QMimeData* mimeData = new QMimeData;
      mimeData->setText("SelectedItems");
      drag->setMimeData(mimeData);
      drag->exec(Qt::MoveAction);
    }
  }

  if (!mInternalDrag)
    QTreeWidget::mouseMoveEvent(event);
}

quint32 MemEditorTreeWidget::numSelectedItems()
{
  quint32 numItems = 0;
  foreach (QTreeWidgetItem* treeItem, selectedItems())
  {
    MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(treeItem);
    if (item)
      numItems++;
  } 
  return numItems;
}

quint32 MemEditorTreeWidget::numSelectedItems(MemEditorTreeItem::NodeType nodeType)
{
  quint32 numItems = 0;
  foreach (QTreeWidgetItem* treeItem, selectedItems())
  {
    MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(treeItem);
    if (item && item->getNodeType() == nodeType)
      numItems++;
  } 
  return numItems;
}

void MemEditorTreeWidget::findNodeHelper(QList<MemEditorTreeItem*>& items, MemEditorTreeItem* parent, MemEditorTreeItem::NodeType nodeType)
{
  if (parent->getNodeType() == nodeType)
    items.append(parent);

  for (int i=0; i<parent->childCount(); i++)
  {
    QTreeWidgetItem* treeItem = parent->child(i);
    MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(treeItem);
    if (item)
      findNodeHelper(items, item, nodeType);
  }
}

quint32 MemEditorTreeWidget::findNodes(QList<MemEditorTreeItem*>& items, MemEditorTreeItem::NodeType nodeType)
{
  items.clear();
  for(int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* treeItem = topLevelItem(i);
    MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(treeItem);
    if (item)
      findNodeHelper(items, item, nodeType);
  }

  return items.count();
}

quint32 MemEditorTreeWidget::fillSelectedItems(QList<MemEditorTreeItem*>& items, MemEditorTreeItem::NodeType nodeType)
{
  items.clear();
  foreach (QTreeWidgetItem* treeItem, selectedItems())
  {
    MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(treeItem);
    if (item && item->getNodeType() == nodeType)
      items.append(item);
  } 
  return items.count();
}

MemTreeIndex MemEditorTreeItem::getTreeIndex()
{
  MemEditorTreeWidget* tree = dynamic_cast<MemEditorTreeWidget*>(treeWidget());
  return tree->indexFromItem(this);
}

void MemEditorTreeItem::setModified(bool value)
{
  MemEditorTreeWidget* tree = dynamic_cast<MemEditorTreeWidget*>(treeWidget());
  tree->setModified(value);
}

CarbonCfg* MemEditorTreeItem::getCcfg() const
{
  MemEditorTreeWidget* tree = dynamic_cast<MemEditorTreeWidget*>(treeWidget());
  INFO_ASSERT(tree, "Expecting MemEditorTreeWidget");
  return tree->getCcfg();
}

CompWizardMemEditor* MemEditorTreeItem::getWizard()
{
  MemEditorTreeWidget* mtw = dynamic_cast<MemEditorTreeWidget*>(treeWidget());
  if (mtw)
    return mtw->getEditor();
  else
    return NULL;
}
MemEditorTreeWidget* MemEditorTreeItem::getTree() 
{
  if (treeWidget() == NULL)
    return mTreeWidget;
  else
    return dynamic_cast<MemEditorTreeWidget*>(treeWidget());
}

QUndoStack* MemEditorTreeItem::getUndoStack() const
{
  MemEditorTreeWidget* tree = dynamic_cast<MemEditorTreeWidget*>(treeWidget());
  INFO_ASSERT(tree, "Expecting MemEditorTreeWidget");
  return tree->getUndoStack();
}

MemCmdDeleteItems::MemCmdDeleteItems(QList<MemEditorTreeItem*> items, QUndoCommand* parent)
: QUndoCommand(parent)
{
  setText(QString("Delete %1 Item(s)").arg(items.count()));

  foreach(MemEditorTreeItem* item, items)
  {
    MemUndoData* undoData = item->deleteItem();
    if (undoData)
      mUndoDataList.append(undoData);

    // Handle a batch.
    item->deleteItem(&mUndoDataList);    
  }
}

