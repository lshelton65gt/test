//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "DlgAuthenticate.h"
#include "CarbonProjectWidget.h"
#include <QSettings>

inline QString getVersionString()
{
    return QString::number( (QT_VERSION >> 16) & 0xff )
        + QLatin1String(".") + QString::number( (QT_VERSION >> 8) & 0xff );
}

DlgAuthenticate::DlgAuthenticate(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);
  mOptions = NULL;

  //const QString key = getVersionString() + QLatin1String("/");

  //QSettings settings("Carbon Design Systems", "ModelStudio");

  //QString server = settings.value(key + QLatin1String("Remote Server")).toString();
  //if (ui.lineEditServerName->text().length() == 0)
  //  ui.lineEditServerName->setText(server);

  //QString username = settings.value(key + QLatin1String("Remote Username")).toString();
  //if (ui.lineEditUsername->text().length() == 0)
  //  ui.lineEditUsername->setText(username);

  //QString workdir = settings.value(key + QLatin1String("Remote Working Directory")).toString();
  //if (ui.lineEditRemoteDirectory->text().length() == 0)
  //  ui.lineEditRemoteDirectory->setText(workdir);

  //QString cmdprefix = settings.value(key + QLatin1String("Remote Command Prefix")).toString();
  //if (ui.lineEditCommandPrefix->text().length() == 0)
  //  ui.lineEditCommandPrefix->setText(cmdprefix);
}


void DlgAuthenticate::setOptions(CarbonOptions* options)
{
  mOptions = options;

  if (options)
  {
    CarbonPropertyValue* remoteServer = options->getValue("Remote Server");
    CarbonPropertyValue* remoteUsername = options->getValue("Remote Username");
    CarbonPropertyValue* remoteDirectory = options->getValue("Remote Working Directory");
    CarbonPropertyValue* remoteCommandPrefix = options->getValue("Remote Command Prefix");
    CarbonPropertyValue* remoteCarbonHome = options->getValue("Remote CARBON_HOME");

    QString remDir = remoteDirectory->getValue();
    if (remDir.isEmpty())
    {
      CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
      CarbonProject* proj = pw->project();
      remDir = proj->getRemoteWorkingDirectory();
    }

    ui.lineEditServerName->setText(remoteServer->getValue());
    ui.lineEditUsername->setText(remoteUsername->getValue());
    ui.lineEditRemoteDirectory->setText(remDir);
    ui.lineEditCommandPrefix->setText(remoteCommandPrefix->getValue());
    ui.lineEditRemoteCarbonHome->setText(remoteCarbonHome->getValue());
  }
 

  updateWarnings();
 }

DlgAuthenticate::~DlgAuthenticate()
{
}

bool DlgAuthenticate::checkField(QLabel* label, QLineEdit* control)
{
  bool isOK = true;

  if (control->text().length() == 0)
  {
    label->setVisible(true);
    isOK = false;
    control->setFocus();
  }
  else
    label->setVisible(false);

  return isOK;
}

bool DlgAuthenticate::updateWarnings()
{
  bool isOK = true;

  if (!checkField(ui.labelWarnUsername, ui.lineEditUsername))
    isOK = false;

  if (!checkField(ui.labelWarnServer, ui.lineEditServerName))
    isOK = false;

  if (!checkField(ui.labelWarnWorkingDir, ui.lineEditRemoteDirectory))
    isOK = false;

  ui.labelIndicateWarnings1->setVisible(!isOK);
  ui.labelIndicateWarnings2->setVisible(!isOK);

  return isOK;
}

void DlgAuthenticate::on_buttonBox_accepted()
{
  // Test for the minimum legal values
  QString remoteServer = ui.lineEditServerName->text();
  QString remoteUsername = ui.lineEditUsername->text();
  QString remoteWorkingDir = ui.lineEditRemoteDirectory->text();
  QString remoteCarbonHome = ui.lineEditRemoteCarbonHome->text();

  if (!updateWarnings())
    return;

  if (remoteUsername.startsWith("~"))
  {
    QMessageBox::critical(this, MODELSTUDIO_TITLE, "Usernames starting with a ~ are not supported.");
    return;
  }

  if (mOptions)
  {
    mOptions->putValue("Remote Server", ui.lineEditServerName->text());
    mOptions->putValue("Remote Username", ui.lineEditUsername->text());
    mOptions->putValue("Remote Working Directory", ui.lineEditRemoteDirectory->text());
    mOptions->putValue("Remote Command Prefix", ui.lineEditCommandPrefix->text());
    mOptions->putValue("Remote CARBON_HOME", ui.lineEditRemoteCarbonHome->text());
  }

  const QString key = getVersionString() + QLatin1String("/");

  QSettings settings("Carbon Design Systems","ModelStudio");
  settings.setValue( key + QLatin1String("Remote Server"), ui.lineEditServerName->text() );
  settings.setValue( key + QLatin1String("Remote Username"), ui.lineEditUsername->text() );
  settings.setValue( key + QLatin1String("Remote Working Directory"), ui.lineEditRemoteDirectory->text() );
  settings.setValue( key + QLatin1String("Remote Command Prefix"), ui.lineEditCommandPrefix->text() );
  
  accept();
}

void DlgAuthenticate::on_buttonBox_rejected()
{
  reject();
}
