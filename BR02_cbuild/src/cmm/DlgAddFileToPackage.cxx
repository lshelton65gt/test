/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#include "DlgAddFileToPackage.h"
#include <QFileDialog>
#include <QFileInfo>

DlgAddFileToPackage::DlgAddFileToPackage(QWidget *parent)
    : QDialog(parent)
{
  ui.setupUi(this);
}

DlgAddFileToPackage::~DlgAddFileToPackage()
{

}

QString DlgAddFileToPackage::fileName()
{
  return ui.FileName->text();
}

QString DlgAddFileToPackage::filePath()
{
  return ui.FilePath->text();
}

void DlgAddFileToPackage::on_pushButtonBrowse_clicked()
{
  QString fileName = QFileDialog::getOpenFileName(this, "Select file to add to package");
  ui.FilePath->setText(fileName);
  
  // Default the filename to the base name of the file that was browsed for.
  if(ui.FileName->text() == "") {
    QFileInfo fi(fileName);
    ui.FileName->setText(fi.fileName());
  }
}

void DlgAddFileToPackage::on_buttonBox_rejected()
{
  done(DlgAddFileToPackage::Cancel);
}

void DlgAddFileToPackage::on_buttonBox_accepted()
{
  done(DlgAddFileToPackage::OK);
}
