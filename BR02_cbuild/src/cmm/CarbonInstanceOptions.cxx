//! -*-C++-*-
/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "gui/CQt.h"
#include "CarbonRuntimeState.h"
#include "SettingsEditor.h"
#include "CarbonInstanceOptions.h"


CarbonInstanceOptions::CarbonInstanceOptions(CarbonRuntimeState *crs, const char* compName)
  : mRuntimeState(crs), mOptions(NULL), mProperties(NULL), mComponentName(compName), mNotify(false)
{
  mProperties = new CarbonProperties();
  mProperties->readPropertyDefinitions(":/cmm/Resources/CControlModelInstanceProperties.xml");
  mOptions = new CarbonOptions(NULL, mProperties, "Carbon Model");

  // turn notification on
  putNotify(true);
}

CarbonInstanceOptions::~CarbonInstanceOptions()
{
  delete mOptions;
  delete mProperties;
}

void CarbonInstanceOptions::putNotify(bool notify)
{
  if (mNotify != notify) {
    mNotify = notify;
    if (notify) {
      // notify the given widget when the properties are edited
      mOptions->registerPropertyChanged(REP_PROP_VERBOSE, "valueChangedReplayVerbose", this);
      mOptions->registerPropertyChanged(REP_PROP_DATABASE, "valueChangedReplayDatabase", this);
      mOptions->registerPropertyChanged(REP_PROP_REQ_STATE, "valueChangedReplayRequestedState", this);
      mOptions->registerPropertyChanged(REP_PROP_CHK_INTV, "valueChangedReplayCheckpointInterval", this);
      mOptions->registerPropertyChanged(REP_PROP_REC_PCNT, "valueChangedReplayRecoverPercentage", this);

      mOptions->registerPropertyChanged(ODM_PROP_MAX_STATES, "valueChangedOnDemandMaxStates", this);
      mOptions->registerPropertyChanged(ODM_PROP_BACKOFF_STATES, "valueChangedOnDemandBackoffStates", this);
      mOptions->registerPropertyChanged(ODM_PROP_BACKOFF_DECAY_PCT, "valueChangedOnDemandBackoffDecayPercent", this);
      mOptions->registerPropertyChanged(ODM_PROP_BACKOFF_MAX_DECAY, "valueChangedOnDemandBackoffMaxDecay", this);
      mOptions->registerPropertyChanged(ODM_PROP_BACKOFF_STRATEGY, "valueChangedOnDemandBackoffStrategy", this);
      mOptions->registerPropertyChanged(ODM_PROP_EXCLUDED_STATE_SIGNALS,
                                        "valueChangedOnDemandExcludedStateSignals",
                                        this);
      mOptions->registerPropertyChanged(ODM_PROP_EXCLUDED_INPUT_SIGNALS,
                                        "valueChangedOnDemandExcludedInputSignals",
                                        this);
      mOptions->registerPropertyChanged(ODM_PROP_REQ_STATE, "valueChangedOnDemandRequestedState", this);
      mOptions->registerPropertyChanged(ODM_PROP_TRACE, "valueChangedOnDemandTrace", this);
    }
    else {
      // do NOT notify the given widget when the properties are edited
      mOptions->unregisterPropertyChanged(REP_PROP_VERBOSE, this);
      mOptions->unregisterPropertyChanged(REP_PROP_DATABASE, this);
      mOptions->unregisterPropertyChanged(REP_PROP_REQ_STATE, this);
      mOptions->unregisterPropertyChanged(REP_PROP_CHK_INTV,  this);
      mOptions->unregisterPropertyChanged(REP_PROP_REC_PCNT,  this);

      mOptions->unregisterPropertyChanged(ODM_PROP_MAX_STATES, this);
      mOptions->unregisterPropertyChanged(ODM_PROP_BACKOFF_STATES, this);
      mOptions->unregisterPropertyChanged(ODM_PROP_BACKOFF_DECAY_PCT, this);
      mOptions->unregisterPropertyChanged(ODM_PROP_BACKOFF_MAX_DECAY, this);
      mOptions->unregisterPropertyChanged(ODM_PROP_BACKOFF_STRATEGY, this);
      mOptions->unregisterPropertyChanged(ODM_PROP_REQ_STATE, this);
      mOptions->unregisterPropertyChanged(ODM_PROP_TRACE, this);
      mOptions->unregisterPropertyChanged(ODM_PROP_EXCLUDED_STATE_SIGNALS,
                                          this);
      mOptions->unregisterPropertyChanged(ODM_PROP_EXCLUDED_INPUT_SIGNALS,
                                          this);
    }
  }
}


// set the property values based on the CarbonSystem state
void CarbonInstanceOptions::putPropertyValues(CarbonReplaySystem &guiSystem, CarbonReplaySystem &simSystem, const char *compName)
{
  CarbonSystemComponent* simComp = simSystem.findComponent(compName);
  INFO_ASSERT(simComp, "runtime component not found");

  // Note that "guiComp", which was in Jason's original code, is not
  // needed in the routine below.  Furthermore, if the GUI is up with
  // the results from a previous simulation, and the user adds a new
  // component without restarting the GUI, then guiComp will be null,
  // so we will assert.  What we really need to do here is to 
  // re-initialize the GUI components from the system components, so
  // this is TBD.
  //
  // To reproduce:
  //   1. edit to examples/ondemand/c/count.c
  //   2. change MULTI_COMP to 0
  //   3. make count.exe
  //   4. ./count.exe -carbonGUI
  //   5. continue simulation, leave model-studio/ccontrol running
  //   6. change MULTI_COMP to 1
  //   7. make count.exe
  //   8. ./count.exe -carbonGUI
  //
  //
  // (MK) Actually, guiComp is once again required, because some
  // component state (e.g. OnDemand exclusions) is not stored in the
  // .css file, only in the .csu file.  Other component state is in
  // both locations, but we want the user's requested value, if it's
  // available, to override the current state of the runtime
  // component.  If the GUI component is available, it will be used in
  // place of the sim component for these values.  If it's not
  // available (as in Josh's example above), it will default to the
  // sim component.
  CarbonSystemComponent* guiComp = guiSystem.findComponent(compName);
//   INFO_ASSERT(guiComp, "ccontrol component not found");

  // The GUI component, if available, else the sim component
  CarbonSystemComponent *preferredComp = simComp;
  if (guiComp != NULL) {
    preferredComp = guiComp;
  }

  const CarbonProperty *prop;
  const CarbonPropertyGroup *group;
  UtString value;

  // turn notification off; we already know these values are changing
  putNotify(false);
      
  // Replay properties
  group = mOptions->findGroup(REP_PROP_GROUP);
  INFO_ASSERT(group, REP_PROP_GROUP " properties group not found");

  // replay verbose mode
  prop = mOptions->findProperty(REP_PROP_VERBOSE);
  INFO_ASSERT(prop, REP_PROP_VERBOSE " property not found");
  mOptions->putValue(prop, guiSystem.isVerbose() ? "true" : "false");

  // replay current state
  prop = mOptions->findProperty(REP_PROP_CUR_STATE);
  INFO_ASSERT(prop, REP_PROP_CUR_STATE " property not found");
  mOptions->putValue(prop, simComp->getReplayState().getString());

  // replay requested state
  prop = mOptions->findProperty(REP_PROP_REQ_STATE);
  INFO_ASSERT(prop, REP_PROP_REQ_STATE " property not found");

  if (guiSystem.isRecordRequested(compName)) {
    mOptions->putValue(prop, REP_REQ_STATE_RECORD);
  }
  else if (guiSystem.isPlaybackRequested(compName)) {
    mOptions->putValue(prop, REP_REQ_STATE_PLAYBACK);
  }
  else {
    mOptions->putValue(prop, REP_REQ_STATE_NORMAL);
  }

  // Checkpoint interval
  prop = mOptions->findProperty(REP_PROP_CHK_INTV);
  INFO_ASSERT(prop, "Replay " REP_PROP_CHK_INTV " property not found");
  value.clear();
  value << guiSystem.getCheckpointInterval();  // TODO: isn't this a per-model value??
  mOptions->putValue(prop, value.c_str());

  prop = mOptions->findProperty("Number of Checkpoints");
  INFO_ASSERT(prop, "Replay Number of Checkpoints property not found");
  value.clear();
  value << simComp->getNumCheckpoints();
  mOptions->putValue(prop, value.c_str());

  prop = mOptions->findProperty("Number of Schedule Calls");
  INFO_ASSERT(prop, "Replay Number of Schedule Calls property not found");
  value.clear();
  value << simComp->getNumScheduleCalls();
  mOptions->putValue(prop, value.c_str());
    
  // recover percentage
  prop = mOptions->findProperty(REP_PROP_REC_PCNT);
  INFO_ASSERT(prop, "Replay " REP_PROP_REC_PCNT " property not found");
  value.clear();
  value << guiSystem.getRecoverPercentage(); // TODO: isn't this a per-model value??
  mOptions->putValue(prop, value.c_str());

  prop = mOptions->findProperty("Database");
  INFO_ASSERT(prop, "Replay Database property not found");
  mOptions->putValue(prop, guiSystem.getDatabaseDirectory());

  // OnDemand properties
  group = mOptions->findGroup("OnDemand");
  INFO_ASSERT(group, "OnDemand properties group not found");

  // OnDemand current state
  prop = mOptions->findProperty(ODM_PROP_CUR_STATE);
  INFO_ASSERT(prop, ODM_PROP_CUR_STATE " property not found");
  mOptions->putValue(prop, simComp->getOnDemandState().getString());

  // All the user-modifiable settings should use the .csu requested
  // value, if it's available.
  prop = mOptions->findProperty(ODM_PROP_MAX_STATES);
  INFO_ASSERT(prop, ODM_PROP_MAX_STATES " property not found.");
  value.clear();
  value << preferredComp->getOnDemandMaxStates();
  mOptions->putValue(prop, value.c_str());

  prop = mOptions->findProperty(ODM_PROP_BACKOFF_STATES);
  INFO_ASSERT(prop, ODM_PROP_BACKOFF_STATES " property not found.");
  value.clear();
  value << preferredComp->getOnDemandBackoffStates();
  mOptions->putValue(prop, value.c_str());

  prop = mOptions->findProperty(ODM_PROP_BACKOFF_DECAY_PCT);
  INFO_ASSERT(prop, ODM_PROP_BACKOFF_DECAY_PCT " property not found.");
  value.clear();
  value << preferredComp->getOnDemandBackoffDecayPercent();
  mOptions->putValue(prop, value.c_str());

  prop = mOptions->findProperty(ODM_PROP_BACKOFF_MAX_DECAY);
  INFO_ASSERT(prop, ODM_PROP_BACKOFF_MAX_DECAY " property not found.");
  value.clear();
  value << preferredComp->getOnDemandBackoffMaxDecay();
  mOptions->putValue(prop, value.c_str());

  prop = mOptions->findProperty(ODM_PROP_BACKOFF_STRATEGY);
  INFO_ASSERT(prop, ODM_PROP_BACKOFF_STRATEGY " property not found.");
  mOptions->putValue(prop, preferredComp->getOnDemandBackoffStrategy().getString());

  prop = mOptions->findProperty(ODM_PROP_EXCLUDED_STATE_SIGNALS);
  INFO_ASSERT(prop, ODM_PROP_EXCLUDED_STATE_SIGNALS " property not found.");
  // TODO - use QStringList
  value.clear();
  for (CarbonSystemComponent::SignalLoop l = preferredComp->loopOnDemandExcluded();
       !l.atEnd(); ++l) {
    const UtString &signalName = *l;
    // delimit with semicolon if there are other entries
    if (!value.empty()) {
      value << ";";
    }
    value << signalName;
  }
  mOptions->putValue(prop, value.c_str());

  prop = mOptions->findProperty(ODM_PROP_EXCLUDED_INPUT_SIGNALS);
  INFO_ASSERT(prop, ODM_PROP_EXCLUDED_INPUT_SIGNALS " property not found.");
  // TODO - use QStringList
  value.clear();
  for (CarbonSystemComponent::SignalLoop l = preferredComp->loopOnDemandIdleDeposit();
       !l.atEnd(); ++l) {
    const UtString &signalName = *l;
    // delimit with semicolon if there are other entries
    if (!value.empty()) {
      value << ";";
    }
    value << signalName;
  }
  mOptions->putValue(prop, value.c_str());

  prop = mOptions->findProperty(ODM_PROP_REQ_STATE);
  INFO_ASSERT(prop, ODM_PROP_REQ_STATE " property not found");
  if (guiSystem.isOnDemandStartRequested(compName)) {
    mOptions->putValue(prop, ODM_REQ_STATE_START);
  }
  else if (guiSystem.isOnDemandStopRequested(compName)) {
    mOptions->putValue(prop, ODM_REQ_STATE_STOP);
  }
  else {
    mOptions->putValue(prop, "");
  }

  prop = mOptions->findProperty(ODM_PROP_TRACE);
  INFO_ASSERT(prop, ODM_PROP_TRACE " property not found");
  mOptions->putValue(prop, preferredComp->getOnDemandTrace() ? "on" : "off");

  // This is not user-modifiable
  prop = mOptions->findProperty(ODM_PROP_TRACE_FILE);
  INFO_ASSERT(prop, ODM_PROP_TRACE_FILE " property not found");
  mOptions->putValue(prop, simComp->getOnDemandTraceFile());

  // turn notification back on
  putNotify(true);
}

// Merge all property values from the given property set into this one.
// Only merges properties from the given level.
// The overWrite flag causes the incoming options to overwrite the existing option values.
void CarbonInstanceOptions::merge(CarbonOptions *fromOptions, bool overWrite)
{  
  // turn notification off; we already know these values are changing
  putNotify(false);
      
  const CarbonProperties* props = mOptions->getProperties();
  for (UInt32 i=0; i<props->numGroups(); ++i)
  {
    CarbonPropertyGroup* group = props->getGroup(i);

    for (CarbonPropertyGroup::SwitchLoop p = group->loopSwitches(); !p.atEnd(); ++p)
    {
      CarbonProperty* prop = p.getValue();
      const char *propName = prop->getName();
      const CarbonProperty* fromProp = fromOptions->findProperty(propName);

      bool fromIsDefaulted, fromIsThisLevel;
      CarbonPropertyValue* fromPropValue = fromOptions->getValue(fromProp,&fromIsDefaulted,&fromIsThisLevel);

      bool isDefaulted, isThisLevel;
      CarbonPropertyValue* propValue = mOptions->getValue(prop,&isDefaulted,&isThisLevel);

      // we iterate over the CarbonProperty objects to makes sure we
      // visit all of the properties, but use CarbonOption::putValue()
      // set the values so that the Properties editor widget will
      // recieve value change notifications and keep the display
      // up-to-date
      if (overWrite) {
        // in overwrite mode, just use the given 'from' value
        mOptions->putValue(propName, fromPropValue->getValue());
      }
      else if (strcmp(propValue->getValue(), fromPropValue->getValue()) != 0) {
        // represent two different values with empty value: used for multiple selection.
        mOptions->putValue(propName, "");
      }
    }
  }

  // turn notification back on
  putNotify(true);
}


void CarbonInstanceOptions::valueChangedReplayVerbose(const CarbonProperty* /* prop */, const char* newValue)
{
  bool verbose = strcmp(newValue, "true") == 0;
  mRuntimeState->replayVerbose(verbose);
}

void CarbonInstanceOptions::valueChangedReplayDatabase(const CarbonProperty* /* prop */, const char* newValue)
{
  mRuntimeState->replayDatabase(newValue);
}

void CarbonInstanceOptions::valueChangedReplayRequestedState(const CarbonProperty* /* prop */, const char* newValue)
{
  if (strcmp(newValue, REP_REQ_STATE_RECORD) == 0) {
    mRuntimeState->replayRecord();
  }
  else if (strcmp(newValue, REP_REQ_STATE_PLAYBACK) == 0) {
    mRuntimeState->replayPlayback();
  }
  else if (strcmp(newValue, REP_REQ_STATE_NORMAL) == 0) { 
    mRuntimeState->replayStop();
  }
  else {
    INFO_ASSERT(false, "unknown replay requested state");
  }
}

void CarbonInstanceOptions::valueChangedReplayCheckpointInterval(const CarbonProperty* /* prop */, const char* newValue)
{
  UInt32 number;
  if (StringUtil::parseNumber(newValue, &number)) {
    mRuntimeState->replayCheckpointInterval(number);
  }
}

void CarbonInstanceOptions::valueChangedReplayRecoverPercentage(const CarbonProperty* /* prop */, const char* newValue)
{
  UInt32 number;
  if (StringUtil::parseNumber(newValue, &number)) {
    mRuntimeState->replayRecoverPercentage(number);
  }
}

void CarbonInstanceOptions::valueChangedOnDemandMaxStates(const CarbonProperty* /* prop */, const char* newValue)
{
  UInt32 number;
  if (StringUtil::parseNumber(newValue, &number)) {
    mRuntimeState->onDemandMaxStates(number);
  }
}

void CarbonInstanceOptions::valueChangedOnDemandBackoffStates(const CarbonProperty* /* prop */, const char* newValue)
{
  UInt32 number;
  if (StringUtil::parseNumber(newValue, &number)) {
    mRuntimeState->onDemandBackoffStates(number);
  }
}

void CarbonInstanceOptions::valueChangedOnDemandBackoffDecayPercent(const CarbonProperty* /* prop */, const char* newValue)
{
  UInt32 number;
  if (StringUtil::parseNumber(newValue, &number)) {
    mRuntimeState->onDemandBackoffDecayPercent(number);
  }
}

void CarbonInstanceOptions::valueChangedOnDemandBackoffMaxDecay(const CarbonProperty* /* prop */, const char* newValue)
{
  UInt32 number;
  if (StringUtil::parseNumber(newValue, &number)) {
    mRuntimeState->onDemandBackoffMaxDecay(number);
  }
}

void CarbonInstanceOptions::valueChangedOnDemandBackoffStrategy(const CarbonProperty* /* prop */, const char* newValue)
{
  if (strcmp(newValue, "constant") == 0) {
    mRuntimeState->onDemandBackoffStrategy(eCarbonOnDemandBackoffConstant);
  }
  else if (strcmp(newValue, "decay") == 0) {
    mRuntimeState->onDemandBackoffStrategy(eCarbonOnDemandBackoffDecay);
  }
  else {
    INFO_ASSERT(false, "unknown OnDemand backoff strategy");
  }
}

void CarbonInstanceOptions::valueChangedOnDemandExcludedStateSignals(const CarbonProperty* /* prop */, const char* newValue)
{
  mRuntimeState->onDemandExcludedStateSignals(mComponentName.c_str(), newValue);
}

void CarbonInstanceOptions::valueChangedOnDemandExcludedInputSignals(const CarbonProperty* /* prop */, const char* newValue)
{
  mRuntimeState->onDemandExcludedInputSignals(mComponentName.c_str(), newValue);
}

void CarbonInstanceOptions::valueChangedOnDemandRequestedState(const CarbonProperty* /* prop */, const char* newValue)
{
  if (strcmp(newValue, ODM_REQ_STATE_START) == 0) {
    mRuntimeState->onDemandStart();
  }
  else if (strcmp(newValue, ODM_REQ_STATE_STOP) == 0) {
    mRuntimeState->onDemandStop();
  }
  else {
    INFO_ASSERT(false, "unknown OnDemand requested state");
  }
}

void CarbonInstanceOptions::valueChangedOnDemandTrace(const CarbonProperty* /* prop */, const char* newValue)
{
  if (strcmp(newValue, "on") == 0) {
    mRuntimeState->onDemandTrace(true);
  }
  else if (strcmp(newValue, "off") == 0) {
    mRuntimeState->onDemandTrace(false);
  }
  else {
    INFO_ASSERT(false, "unknown OnDemand trace value");
  }
}

void CarbonInstanceOptions::putReplayRequestedState(const char* newVal)
{
  mOptions->putValue(REP_PROP_REQ_STATE, newVal);
}

void CarbonInstanceOptions::putOnDemandRequestedState(const char* newVal)
{
  mOptions->putValue(ODM_PROP_REQ_STATE, newVal);
}

void CarbonInstanceOptions::putReplayVerbose(bool newVal)
{
  mOptions->putValue(REP_PROP_VERBOSE, newVal ? "true" : "false");
}

bool CarbonInstanceOptions::getReplayVerbose()
{
  const char* value = mOptions->getValue(REP_PROP_VERBOSE)->getValue();
  return (value && 0 == strcmp(value, "true"));
}

const char* CarbonInstanceOptions::getOnDemandRequestedState()
{
  return mOptions->getValue(ODM_PROP_REQ_STATE)->getValue();
}

const char* CarbonInstanceOptions::getReplayRequestedState()
{
  return mOptions->getValue(REP_PROP_REQ_STATE)->getValue();
}
