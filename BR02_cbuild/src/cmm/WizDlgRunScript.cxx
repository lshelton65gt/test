//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "WizDlgRunScript.h"
#include "ModelKitWizard.h"

WizDlgRunScript::WizDlgRunScript(QWidget *parent, ModelKitWizard* wiz, KitActionRunScript* action)
: QDialog(parent)
{
  mWizard = wiz;
  mAction = action;

  ui.setupUi(this);
  
  if (action)
  {
   	ui.lineEditQuestionID->setText(action->getID());
    ui.lineEditDescription->setText(action->getDescription());
    ui.lineEditScriptName->setText(action->getScriptName());
  }
}

KitUserAction* WizDlgRunScript::createAction()
{
  KitActionRunScript* action = new KitActionRunScript();
  applyData(action);
  return action;
}
void WizDlgRunScript::applyData(KitActionRunScript* action)
{
  action->setID(ui.lineEditQuestionID->text());
  action->setScriptName(ui.lineEditScriptName->text());

  // Base Class
  action->setDescription(ui.lineEditDescription->text());
}

WizDlgRunScript::~WizDlgRunScript()
{
}

void WizDlgRunScript::on_pushButtonBrowseScript_clicked()
{
  QString fileName = QFileDialog::getOpenFileName(this, "ModelStudio Script", ".", "Script (*.js *.mjs);;Modelstudio Script (*.mjs)");
  if (!fileName.isEmpty())
  {
    QFileInfo fi(fileName);
    QString normalizedName = fi.fileName();
    if (mWizard)
    {
      normalizedName = mWizard->normalizeName(fi.filePath());
      normalizedName = normalizedName.replace("${ARM_ROOT}", "${CARBON_PROJECT_ARM}");
    }

    QFileInfo fi2(normalizedName);

    ui.lineEditScriptName->setText(fi2.fileName());
  }
}

void WizDlgRunScript::on_buttonBox_accepted()
{
  if (mAction)
    applyData(mAction);

  accept();
}

void WizDlgRunScript::on_buttonBox_rejected()
{
  reject();
}
