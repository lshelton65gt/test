#ifndef __CONFIGURATION_H_
#define __CONFIGURATION_H_

#include "Scripting.h"

class Template;
class Parameter;
class PortConfigurations;

#include "Modes.h"

// File: PortConfiguration
// The Ports collection and the Port object
//
// Class: PortConfiguration
//
class PortConfiguration : public QObject
{
  Q_OBJECT

  // Property: name
  // The name of the Port Configuration
  // 
  // Access:
  // *ReadOnly*
  //
  // See Also:
  // <Context>, <Template>, <Model> objects
  Q_PROPERTY(QString name READ getName)

  // Property: modes
  // The collection of <Mode> objects associated
  // with this PortConfiguration
  //
  Q_PROPERTY(QObject* modes READ getModes)

public:
  PortConfiguration()
  {
  }
  
  void setName(const QString& newVal) { mName=newVal; }
  QString getName() const { return mName; }

  static bool deserialize(Template* templ, PortConfigurations* configs, const QDomElement& e, XmlErrorHandler* eh);

  void addMode(Mode* mode);

public slots:
  Modes* getModes();
 
private:
  Modes mModes;
  QString mName;

};
#endif

