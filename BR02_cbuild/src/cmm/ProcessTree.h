#ifndef __PROCESSTREE_H__
#define __PROCESSTREE_H__

#include <QtGui>
#include "util/OSWrapper.h"

// Unix-only class which returns the children PIDs of a parent process
class ProcessTree 
{
public:
  ProcessTree(int parentPID);
  bool isParentAlive();
  const QList<int>& childProcesses();
  int findParentPID(int processID);

private:
  void findChildrenOf(int parentPID);

private:
  QList<int> mPidList;
  int mParentPID;
};

#endif
