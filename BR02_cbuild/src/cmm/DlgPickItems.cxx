//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "DlgPickItems.h"

DlgPickItems::DlgPickItems(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);
}

DlgPickItems::~DlgPickItems()
{

}

void DlgPickItems::addItems(const QStringList& items)
{
  foreach (QString varName, items)
  {
    QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidget);
    item->setText(0, varName);
    item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
    item->setCheckState(0, Qt::Unchecked);
  }
}


void DlgPickItems::on_buttonBox_rejected()
{
  reject();
}

void DlgPickItems::on_buttonBox_accepted()
{
  for (int i=0; i<ui.treeWidget->topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = ui.treeWidget->topLevelItem(i);
    Qt::CheckState state = item->checkState(0);

    if (Qt::Checked == state)
      mValues << item->text(0);
  }
  accept();
}
