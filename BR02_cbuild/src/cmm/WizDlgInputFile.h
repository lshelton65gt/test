#ifndef WIZDLGINPUTFILE_H
#define WIZDLGINPUTFILE_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include "KitManifest.h"
#include <QDialog>
#include "ui_WizDlgInputFile.h"

class ModelKitWizard;
class KitActionInputFile;

class WizDlgInputFile : public QDialog
{
  Q_OBJECT

public:
  WizDlgInputFile(QWidget *parent = 0, ModelKitWizard* wiz = 0, KitActionInputFile* action = 0);
  ~WizDlgInputFile();

  KitUserAction* createAction();

private:
  Ui::WizDlgInputFileClass ui;
  ModelKitWizard* mWizard;
  KitActionInputFile* mAction;

private:
  void showError(const QString& msg);
  void applyData(KitActionInputFile* action);

private slots:
    void on_buttonBox_rejected();
    void on_buttonBox_accepted();
    void on_pushButtonBrowseFile_clicked();
};

#endif // WIZDLGINPUTFILE_H
