// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _ON_DEMAND_TUNING_GRAPH_H_
#define _ON_DEMAND_TUNING_GRAPH_H_

#include "util/UtString.h"
#include "util/UtHashMap.h"
#include "util/UtArray.h"
#include "util/UtHashSet.h"
#include <QObject>
#include <qwt_plot.h>
#include <qwt_symbol.h>

class QwtPlotCurve;
class OTGBackground;
class OTGCurve;
class OnDemandTraceFileReader;
class CarbonMakerContext;

#define MIN_VIOLATIONS_TO_SHOW 5
#define FMIN_VIOLATIONS_TO_SHOW 5.0

class OnDemandTuningGraph : public QwtPlot {
  Q_OBJECT
public:
  CARBONMEM_OVERRIDES

  //! ctor
  OnDemandTuningGraph(QWidget *parent, CarbonMakerContext*);

  //! dtor
  ~OnDemandTuningGraph();

  //! update the plot based on the current state of the replay system
  void update();

  //! override the size hint so the user can squish the graph
  virtual QSize sizeHint() const;

  //! load a COT file.
  bool loadTraceFile(const char* fname, UtString* errmsg);

  class Node {
  public:
    CARBONMEM_OVERRIDES

    Node(const char* name) : mName(name) {}
    const char* getName() const {return mName.c_str();}
    UtString mName;             /* store the events? */
  };

  typedef UtArray<Node*> NodeArray;

  class Event {
  public:
    CARBONMEM_OVERRIDES

    Event(CarbonTime simTime,
          CarbonOnDemandDebugType type,
          CarbonOnDemandDebugAction action,
          UInt32 length,
          Node* node)
      : mSimTime(simTime), mType(type), mAction(action), mLength(length), mNode(node) {}
    ~Event() {}

    CarbonOnDemandDebugType getType() const {return mType;}
    CarbonTime getSimTime() const {return mSimTime;}
    UInt32 getLength() const { return mLength; }
    Node* getNode() const {return mNode;}

    void putNode(Node* node) { mNode = node; }

  private:
    CarbonTime mSimTime;
    CarbonOnDemandDebugType mType;
    CarbonOnDemandDebugAction mAction;
    UInt32 mLength;
    Node* mNode;
  };

  typedef UtArray<Event*> EventArray;

  //! A Quantum contains all the events covered by the cycle-depth window
  /*!
   *! By default, OnDemand looks for repeating patterns with a maximum
   *! depth of 16 cycles.  A Quantum represents all the violations
   *! that occur within that period of time.
   *!
   *! It would be nice if they user could move a slider to adjust this
   *! window to see if expanding it would eliminate a net-change
   *! violation.  But that is not possible with the OnDemand
   *! technology today, since we don't save the complete state-dump in
   *! the .cot file -- we only save the violation data.
   */
  class Quantum {
  public:
    CARBONMEM_OVERRIDES

    Quantum(CarbonTime time) : mFirstTime(time), mLastTime(time), mNumTimeSlots(1), mOpen(true) {}
    CarbonTime getLastTime() const {return mLastTime;}
    CarbonTime getFirstTime() const {return mFirstTime;}
    UInt32 numTimeSlots() const {return mNumTimeSlots;}

    void addEvent(Event* event) {
      INFO_ASSERT(mOpen, "event is closed");
      CarbonTime t = event->getSimTime();
      if (t != mLastTime) {
        INFO_ASSERT(t >= mLastTime, "time not monotonically increasing");
        mLastTime = t;
        ++mNumTimeSlots;
      }
      mNodeEventMap[event->getNode()].push_back(event);
    }

    UInt32 numNodeEvents() const {
      return mNodeEventMap.size();
    }

    CarbonTime averageTime() const {
      return (mFirstTime + mLastTime) / 2;
    }

    bool operator<(const Quantum& other) const {
      return mFirstTime < other.mFirstTime;
    }

    void getNodes(NodeArray* nodes) {
      for (LoopMap<NodeEventMap> p(mNodeEventMap); !p.atEnd(); ++p) {
        Node* node = p.getKey();
        nodes->push_back(node);
      }
    }

    bool getNodeViolations(Node* node, EventArray* events) const {
      NodeEventMap::const_iterator p = mNodeEventMap.find(node);
      if (p == mNodeEventMap.end()) {
        return false;
      }
      *events = p->second;
      return true;
    }

    bool hasSameViolations(Quantum* other);

    void putLastTime(CarbonTime t) {mLastTime = t;}

    void close(CarbonTime t)
    {
      INFO_ASSERT(mOpen, "event is closed");
      mLastTime = t;
      mOpen = false;
    }

  private:
    typedef UtHashMap<Node*,EventArray> NodeEventMap;
    NodeEventMap mNodeEventMap;
    CarbonTime mFirstTime;
    CarbonTime mLastTime;
    UInt32 mNumTimeSlots;
    bool mOpen;
  };

  typedef UtArray<Quantum*> QuantumArray;

  //! get a node for a path, creating one if it doesn't yet exist
  Node* getNode(const char* path);

  //! find an existing node for a path, returning NULL if doesn't exist
  Node* findNode(const char* path) const;

  void addEvent(Event* event);
  void newQuantum(CarbonTime t);
  void closeQuantum(CarbonTime t, bool deleteIfEmpty);

  virtual void mouseMoveEvent(QMouseEvent* e);
  virtual void mousePressEvent(QMouseEvent* e);

  Quantum* getQuantum(UInt32 index) const {return mQuantumArray[index];}

  SInt32 findQuantumIndex(QMouseEvent* e);

  void mergeSimilarQuanta();

  void putMagnification(double mag);

  UInt32 getMaxViolations() const {return mMaxViolations;}
  
  SInt32 getSelectionIndex() const {return mSelectIndex;}
  SInt32 numQuanta() const {return mQuantumArray.size();}
  void selectQuantum(SInt32 idx);

signals:
  void quantumSelect(UInt32 quantum_index);
  void quantumUnselect();
  void scaleChanged();

public slots:
  void readMoreText();

private:
  void clear(bool initQuantum);
  static void readTraceCB(void *userData,
                          CarbonUInt64 schedCalls,
                          CarbonTime simTime,
                          CarbonOnDemandDebugType type,
                          CarbonOnDemandDebugAction action,
                          UInt32 length,
                          const char *path);

  static void readModeChangeCB(void *userData,
                               CarbonUInt64 schedCalls,
                               CarbonTime simTime,
                               bool enterIdle);

  static void staleDataCB(void *userData);

  OTGCurve* addLegend(const char* name, QwtSymbol::Style style,
                      int x, int y, const QColor&);

  OTGCurve *mAnyActivity;
  OTGCurve *mNonIdleNet;
  OTGCurve *mNonIdleChange;
  OTGCurve *mDiverge;
  OTGCurve *mFail;

  double mXAxisMax;
  double mYAxisMax;
  
  OTGBackground* mBackground;
  typedef UtHashMap<UtString,Node*> StringNodeMap;
  StringNodeMap mNodes;
  EventArray mEvents;

  QuantumArray mQuantumArray;
  Quantum *mCurrQuantum;
  Quantum *mIdleQuantum;

  SInt32 mSelectIndex;
  SInt32 mHighlightIndex;

  OnDemandTraceFileReader* mReader;
  UtString mReadFilename;
  QTimer* mPollTimer;
  CarbonMakerContext* mCarbonMakerContext;

  double mXMax;
  double mYMax;

  double mUserYMultiplier;
  UInt32 mMaxViolations;
  CarbonTime mSimTime;
};

#endif
