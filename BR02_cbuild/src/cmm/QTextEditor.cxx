/****************************************************************************
**
** Copyright (C) 2005-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "util/CarbonPlatform.h"


#include <QtGui>
#include <QMenuBar>
#include <QToolBar>

#include "gui/CQt.h"
#include "../gui/VerilogSyntax.h"

#include "util/UtString.h"
#include "util/UtIStream.h"

#include "QTextEditor.h"
#include "MdiTextEditor.h"

#include <Qsci/qsciscintilla.h>
#include <Qsci/qsciapis.h>
#include <Qsci/qscicommandset.h>
#include <Qsci/qscicommand.h>
#include <Qsci/qscilexercpp.h>
#include <Qsci/qscilexerhtml.h>
#include <Qsci/qscilexermakefile.h>
#include <Qsci/qscilexerjavascript.h>

#include <Qsci/qscilexerbash.h>
#include <Qsci/qscilexerbatch.h>

#include "CarbonProjectWidget.h"
#include "DlgPreferences.h"
#include "cmm.h"
#include "ScriptingEngine.h"
#include "JavaScriptAPIs.h"

#define CARBON_WARCH_FILES

QLabel* QTextEditor::mTextEditLineColumnInfoLabel = NULL;


void QTextEditor::scriptPositionChanged(int lineNumber, int columnNumber)
{
  setStatusBarCursorPosInfo(lineNumber, columnNumber);
}

void QTextEditor::scriptException(const QScriptValue &, int lineNumber)
{
  if (lineNumber > 0)
    gotoLine((UInt32)lineNumber);
}

void QTextEditor::runScript()
{
  QFileInfo fi(currentFile());
  QString ext = fi.suffix().toLower();

  ScriptingEngine* engine = new ScriptingEngine();
  CQT_CONNECT(engine->getAgent(), scriptException(const QScriptValue&, int), this, scriptException(const QScriptValue&, int));

  if (ext == "js")
  {
    engine->runScript(fi.filePath());
  }
  else if (ext == "mjs")
  {
    engine->runPlugin(fi.filePath());
  }
}

void QTextEditor::debugScript()
{
  QFileInfo fi(currentFile());
  QString ext = fi.suffix().toLower();

  ScriptingEngine* engine = new ScriptingEngine();
  CQT_CONNECT(engine->getAgent(), scriptException(const QScriptValue&, int), this, scriptException(const QScriptValue&, int));
  CQT_CONNECT(engine->getAgent(), scriptPositionChanged(int,int), this, scriptPositionChanged(int, int));

  QList<int> bps;

  int bpLine = markerFindNext(0, 1 << mMarkerBP);
  while (bpLine > 0)
  {
    bps << bpLine;
    bpLine = markerFindNext(bpLine+1, 1 << mMarkerBP);
  }

  if (bps.count() > 0)
    engine->setBreakpoints(bps);

  engine->setEnableDebugger(true);

  if (ext == "js")
  {
    engine->runScript(fi.filePath());
  }
  else if (ext == "mjs")
  {
    engine->runPlugin(fi.filePath());
  }
}
void QTextEditor::shiftTab()
{
  qDebug() << "shiftTab";
}
void QTextEditor::widgetActivated()
{
  DlgFindText* dlg = theApp->getFindDlg();
  if (dlg)
  {
    dlg->setEditor(this);
  }
}

void QTextEditor::selectionChanged()
{
  theApp->setSelectedText(selectedText());
}

QTextEditor::QTextEditor(MDIDocumentTemplate* t, QWidget* parent) : QsciScintilla(parent)
{
  qDebug() << "Constructing text editor";

  mSaveInProgress = false;
  mFileChangedCount = 0;

  mWatcher = new QFileSystemWatcher(this);
  CQT_CONNECT(mWatcher, fileChanged(const QString&), this, fileChangedOnDisk(const QString&));

  setEolMode(QsciScintilla::EolUnix);
  convertEols(QsciScintilla::EolUnix);

  setAttribute(Qt::WA_DeleteOnClose);
  isUntitled = true;

  mCurFile.clear();
  mIsJavaScript = false;
  mBreakpointsModified = false;

  initializeMDI(this, t);
  
  mDefMargin0Width = marginWidth(0);
  mDefMargin1Width = marginWidth(1);
  mDefMargin2Width = marginWidth(2);


  //setWrapMode(QsciScintilla::WrapNone);
  fixedWidth();

#if 0
  QPalette pal = palette();
  pal.setColor(QPalette::Active, QPalette::Highlight, Qt::blue);
  pal.setColor(QPalette::Active, QPalette::HighlightedText, Qt::yellow);
  pal.setColor(QPalette::Inactive, QPalette::Highlight, Qt::red);
  pal.setColor(QPalette::Inactive, QPalette::HighlightedText, Qt::green);
  setPalette(pal);
#endif

// Add a column row indicator to the status bar.
  if (mTextEditLineColumnInfoLabel == NULL)
  {
    mTextEditLineColumnInfoLabel = new QLabel( tr("Ln %1, Col %2").arg(1).arg(1) );
    theApp->statusBar()->insertPermanentWidget(0, mTextEditLineColumnInfoLabel);
  }

  QPixmap pm(":/cmm/Resources/GoToNextHS.png");
  mMarker = markerDefine(pm);

  QPixmap pm2(":/cmm/Resources/replayRecord.png");
  mMarkerBP = markerDefine(pm2);

  setFolding(QsciScintilla::CircledTreeFoldStyle );

  new QShortcut(QKeySequence("Ctrl+G"), this, SLOT(gotoLineDialog()), 0, Qt::WidgetShortcut);

  CQT_CONNECT( this, cursorPositionChanged(int, int), this, setStatusBarCursorPosInfo(int, int));
  CQT_CONNECT(theApp, preferenceChanged(const QString&, const QVariant&), this, preferenceChanged(const QString&, const QVariant&));
  CQT_CONNECT(theApp, editorSettingsChanged(DlgPreferences*), this, editorSettingsChanged(DlgPreferences*));
  CQT_CONNECT(theApp, languageSettingsChanged(const QString&), this, languageSettingsChanged(const QString&));
  CQT_CONNECT(this, selectionChanged(), this, selectionChanged());

  DlgPreferences prefs;
  editorSettingsChanged(&prefs);

  //QsciCommandSet* cset = standardCommands();
  //QList<QsciCommand *> cmds = cset->commands();
  //foreach (QsciCommand* cmd, cmds)
  //{
  //  QKeySequence ks(cmd->key(), cmd->alternateKey());
  //  qDebug() << cmd->description() << cmd->key() << cmd->alternateKey() << ks;
  //}

  //Shift+Tab is broken in Qscintilla, this hack fixes it
  //and enables Shift -> Right Arrow and Shift -> Left Arrow to indent/unindent
  //
  int key1 = QsciScintillaBase::SCK_LEFT + (QsciScintillaBase::SCMOD_SHIFT << 16);
  SendScintilla(QsciScintillaBase::SCI_ASSIGNCMDKEY, key1, QsciScintillaBase::SCI_BACKTAB);
  
  int key2 = QsciScintillaBase::SCK_RIGHT + (QsciScintillaBase::SCMOD_SHIFT << 16);
  SendScintilla(QsciScintillaBase::SCI_ASSIGNCMDKEY, key2, QsciScintillaBase::SCI_TAB);
}

void QTextEditor::setStatusBarCursorPosInfo(int line, int col)
{
  mTextEditLineColumnInfoLabel->setText(QString("Ln: %1, Col: %2").arg(line+1).arg(col+1));
}

void QTextEditor::gotoLineDialog()
{
  int line;
  int index;
  getCursorPosition(&line, &index);
  int lineNumber = QInputDialog::getInteger(this, "Goto Line", "Line Number", line);
  if (lineNumber > 0)
  {
    UInt32 ln = (UInt32)lineNumber;
    gotoLine(ln);
  }
}

void QTextEditor::preferenceChanged(const QString& prefName, const QVariant& value)
{
  if (PREF_TE_FONTSIZE == prefName)
  {
    setFontSize(lexer(), value.toInt());
  }
}

void QTextEditor::fileSave()
{
 save(); 
}

void QTextEditor::newFile()
{
  static UInt32 sequenceNumber = 1;

  isUntitled = true;
  
  mCurFile.clear();
  mCurFile << "document" << sequenceNumber++ << ".txt";

  UtString title;
  title << mCurFile << "[*]";

  setWindowTitle(title.c_str());

  CQT_CONNECT(this, textChanged(), this, documentWasModified());
}

QMenuBar* QTextEditor::menuBar()
{
  return mMenu;
}

void QTextEditor::deleteBreakpoints()
{
  qDebug() << "Removing all breakpoints";
  markerDeleteAll(mMarkerBP);
  mBreakpointsModified = true;
}

bool QTextEditor::loadFile(const char* fileName)
{
  QFile file(fileName);
  if (!file.open(QFile::ReadOnly | QFile::Text)) {
    QMessageBox::warning(this, MODELSTUDIO_TITLE,
      tr("Cannot read file %1:\n%2.")
      .arg(fileName)
      .arg(file.errorString()));
    return false;
  }

  QTextStream in(&file);
  QApplication::setOverrideCursor(Qt::WaitCursor);
  setText(in.readAll());
  QApplication::restoreOverrideCursor();

  setCurrentFile(fileName);

  QString key = QString("breakPoints/%1").arg(mCurFile.c_str());
  QSettings settings;
  int size = settings.beginReadArray(key);

  QList<int> bps;
  for (int i = 0; i < size; ++i) 
  {
    settings.setArrayIndex(i);
    int bp = settings.value("bp").toInt();
    bps.append(bp);
  }
  settings.endArray();

  foreach (int bp, bps)
  {
    markerAdd(bp, mMarkerBP);
  }

  CQT_CONNECT(this, textChanged(), this, documentWasModified());

  UtString linesText;

  linesText << lines() << "9";

  setMarginWidth(0, linesText.c_str());
  setMarginLineNumbers(0, true);

  watchFile(fileName);
#if pfLINUX
  mFileChangedCount = -1;
#endif

  DlgPreferences prefs;
  editorSettingsChanged(&prefs);

  return true;
}


void QTextEditor::watchFile(const QString& fileName)
{
  mFileName = fileName;
  mFileChangedCount = 0;

  if (!mWatcher->files().contains(fileName))
  {
    qDebug() << "watch setup for" << fileName;
    mWatcher->addPath(fileName);
  }
  else
    qDebug() << "WATCH already setup for " << fileName;
}
void QTextEditor::setupLexer(const char* filename)
{
  QString name = filename;
  setupLexer(name);
}

void QTextEditor::toggleBreakpoint()
{
  int line;
  int index;
  getCursorPosition(&line, &index);

  mBreakpointsModified = true;

  unsigned int markers = markersAtLine(line);

  if (markers & (1 << mMarkerBP))
    markerDelete(line, mMarkerBP);
  else
    markerAdd(line, mMarkerBP);

  qDebug() << "toggle bp" << line << markers;
}

void QTextEditor::setupLexer(const QString& fileName)
{
  QFileInfo fi(fileName);
  QString ext = fi.suffix().toLower();

  if (ext.startsWith("vhd"))
    setLexer(new QsciLexerVHDL());
  else if (ext == "v" || ext == "vp" || ext == "vlog" || ext == "verilog")
    setLexer(new QsciLexerVLOG());
  else if (ext == "cxx" || ext == "cpp" || ext == "c" || ext == "h")
    setLexer(new QsciLexerCPP());
  else if (ext == "xml" || ext == "xsd" || ext == "xsl")
    setLexer(new QsciLexerXML());
  else if (ext == "html" || ext == "htm")
    setLexer(new QsciLexerHTML());
  else if (ext == "js" || ext == "mjs")
  {    
    mIsJavaScript = true;

    setLexer(new QsciLexerJavaScript());
    JavaScriptAPIs* apis = new JavaScriptAPIs(lexer());
    if (!apis->enableIntellisense(this))
      qDebug() << "Unable to load Intellisense";
  }

  else if (ext == "sh" || ext == "csh")
    setLexer(new QsciLexerBash());
  else if (ext == "bat" )
    setLexer(new QsciLexerBatch());
  else if (fi.fileName().toLower().startsWith("make") || ext == "mak")
    setLexer(new QsciLexerMakefile());

  QsciLexer* lex = lexer();
  if (lex)
  {
    QSettings settings;
    lex->readSettings(settings, TE_LANGUAGE_PREFIX);
  }
}

void QTextEditor::setNewFile(const char* fileName)
{
  QFileInfo fi(fileName);
  isUntitled = false;

  mCurFile = fileName;

  setupLexer(fileName);

  setMarginWidth(0, "1000");
  setMarginLineNumbers(0, true);

  setModified(true);

  if (!mFileName.isEmpty())
    unwatchFile(mFileName);

  watchFile(fileName);

  CQT_CONNECT(this, textChanged(), this, documentWasModified());

  UtString title;
  title << fi.fileName() << "[*]";
  setWindowTitle(title.c_str());
}

bool QTextEditor::save()
{
  bool status;

  if (isUntitled) {
    status = saveAs();
  } else {
    status = saveFile(mCurFile.c_str());
  }

  return status;
}

void QTextEditor::updateMenusAndToolbars()
{
  emit undoAvailable(isUndoAvailable());
  emit redoAvailable(isRedoAvailable());
  emit pasteAvailable(true);
  emit copyAvailable(true); // hasSelectedText()
  
  QFileInfo fi(currentFile());
  QString ext = fi.suffix().toLower();
  emit scriptLoaded(ext == "js" || ext == "mjs");
}


bool QTextEditor::saveAs()
{
  QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), mCurFile.c_str());
  if (fileName.isEmpty())
    return false;

  UtString f;
  f << fileName;

  return saveFile(f.c_str());
}

bool QTextEditor::saveFile(const char* fileName)
{
  QFile file(fileName);
  if (!file.open(QFile::WriteOnly | QFile::Text)) 
  {
    QMessageBox::warning(this, MODELSTUDIO_TITLE,
      tr("Cannot write file %1:\n%2.")
      .arg(fileName)
      .arg(file.errorString()));
    return false;
  }

  qDebug() << "saving file" << fileName;
  
  mSaveInProgress = true;

  QApplication::setOverrideCursor(Qt::WaitCursor);
  QTextStream out(&file);
  out << text();
  out.flush();
  file.close();
  QApplication::restoreOverrideCursor();

  setCurrentFile(fileName);
  
  mSaveInProgress = false;

  qDebug() << "finished saving file" << fileName;

  mFileChangedCount = 0;

  return true;
}

void QTextEditor::closeEvent(QCloseEvent *event)
{
  if (maybeSave())
  {
    if (mIsJavaScript && mBreakpointsModified)
    {
      QList<int> bps;
      int bpLine = markerFindNext(0, 1 << mMarkerBP);
      while (bpLine > 0)
      {
        bps << bpLine;
        bpLine = markerFindNext(bpLine+1, 1 << mMarkerBP);
      }
      QString key = QString("breakPoints/%1").arg(mCurFile.c_str());
      QSettings settings;
      settings.beginWriteArray(key);
      for (int i = 0; i < bps.size(); ++i) 
      {
        settings.setArrayIndex(i);
        settings.setValue("bp", bps[i]);
      }
      settings.endArray();
    }
    event->accept();
    MDIWidget::closeEvent(this);
  }
  else
  {
    event->ignore();
  }
}

void QTextEditor::refreshFile()
{
  QFile file(mCurFile.c_str());
  QFileInfo fi(mCurFile.c_str());

  //if (!fi.exists() && !isModified())
  //  close();

  if (!file.open(QFile::ReadOnly | QFile::Text)) {
    QMessageBox::warning(this, MODELSTUDIO_TITLE,
      tr("Cannot read file %1:\n%2 or file no longer exists.")
      .arg(mCurFile.c_str())
      .arg(file.errorString()));
    close();
    return;
  }

  QTextStream in(&file);
  QApplication::setOverrideCursor(Qt::WaitCursor);
  setText(in.readAll());
  QApplication::restoreOverrideCursor();
  setWindowModified(false);
  setModified(false);

}


void QTextEditor::unwatchFile(const QString& fileName)
{
  mFileChangedCount = 0;
  if (mWatcher->files().contains(fileName))
  {
    qDebug() << "removed watcher for" << fileName;
    mWatcher->removePath(fileName);
  }
  else
    qDebug() << "NOTHING to unwatchFile" << fileName; 
}

void QTextEditor::fileChangedOnDisk(const QString& fileName)
{
  if (mSaveInProgress)
  {
    qDebug() << "Save in progress, ignoring fileChangedOnDisk" << fileName;
    return;
  }

  bool fileHasChanged = true;

#if pfLINUX
  QString bufferContents = text();
  QFile xf(mFileName);
  if (xf.open(QFile::ReadOnly))
  {
    QTextStream ts(&xf);
    QString buffer = ts.readAll();
    xf.close();
    if (buffer == text())
      fileHasChanged = false;
  }
#endif
  qDebug() << mFileChangedCount << mFileName << mWatcher->files() << mWatcher->directories();

  if (fileHasChanged)
  {
    qDebug() << "fileChangedOnDisk" << fileName;
    mFileChangedCount++;

    MDITextEditorTemplate* docTemplate = static_cast<MDITextEditorTemplate*>(getDocumentTemplate());
    QFileInfo fi(fileName);
    if (fi.exists())
    {
      qDebug() << "notifying change";
        docTemplate->addModifiedEditor(this);
    }
  }
  else
    qDebug() << "File is not changed";
}

void QTextEditor::documentWasModified()
{
  setWindowModified(isModified());
  markerDeleteAll(mMarker);
  updateMenusAndToolbars();
}

void QTextEditor::saveDocument()
{
  QFileInfo fi(mCurFile.c_str());

  if (isModified() || !fi.exists())
    save();
}

bool QTextEditor::maybeSave()
{
  if (isModified() || isWindowModified()) {
    QMessageBox::StandardButton ret;
    ret = QMessageBox::warning(this, MODELSTUDIO_TITLE,
      tr("'%1' has been modified.\n"
      "Do you want to save your changes?")
      .arg(userFriendlyCurrentFile()),
      QMessageBox::Save | QMessageBox::Discard
      | QMessageBox::Cancel);
    if (ret == QMessageBox::Save)
      return save();
    else if (ret == QMessageBox::Cancel)
      return false;
  }
  return true;
}

void QTextEditor::setCurrentFile(const char* fileName)
{
  QString cf =  QFileInfo(fileName).absoluteFilePath();
  mCurFile.clear();
  mCurFile << cf;
  isUntitled = false;
  setModified(false);
  setWindowModified(false);
  UtString title;
  title << userFriendlyCurrentFile() << "[*]";
  setWindowTitle(title.c_str());
  setupLexer(fileName);
}

void QTextEditor::find()
{
  DlgFindText* dlg = theApp->getFindDlg();

  if (dlg == NULL)
  {
    dlg = new DlgFindText();
    theApp->setFindDlg(dlg);

    Qt::WindowFlags flags = dlg->windowFlags();

    dlg->setWindowFlags(flags | Qt::WindowStaysOnTopHint);
    dlg->setEditor(this);
    CQT_CONNECT(dlg, rejected(), this, findDialogDestroyed());
    dlg->setCurrentText(selectedText());
    dlg->show();
    dlg->raise();
  }
  else
  {
    dlg->setEditor(this);
    dlg->setCurrentText(selectedText());
    dlg->raise();
  }
}

void QTextEditor::findDialogDestroyed() 
{
  theApp->setFindDlg(NULL);
}

void QTextEditor::updateUndoStatus()
{
  QAction* action = qobject_cast<QAction*>(sender());
  if (action)
  {
     action->setEnabled(isUndoAvailable());
  }
}

void QTextEditor::updateRedoStatus()
{
  QAction* action = qobject_cast<QAction*>(sender());
  if (action)
  {
    action->setEnabled(isRedoAvailable());
  }
}

const char* QTextEditor::userFriendlyCurrentFile()
{
  return strippedName(mCurFile.c_str());
}

const char* QTextEditor::strippedName(const char* fullFileName)
{
  static UtString x;
  x.clear();
  x << QFileInfo(fullFileName).fileName();
  return x.c_str();
}

void QTextEditor::setFontSize(QsciLexer* lex, int carbonFontSize)
{
  QSettings settings;

  if (carbonFontSize == -1)
#if defined(Q_OS_WIN32)
    carbonFontSize = settings.value("textEditor/fontSize", 10).toInt();
#else
    carbonFontSize = settings.value("textEditor/fontSize", 16).toInt();
#endif

  QFont defFont = lex->defaultFont();
  defFont.setPointSize(carbonFontSize);
  //lex->setDefaultFont(defFont);
  //lex->setFont(defFont, STYLE_DEFAULT);
}

void QTextEditor::gotoLine(UInt32 inputLineNumber) 
{  
  int line = inputLineNumber;
  if (line <= 0)
    line = 1;

  int adjustedLine = line-1;

  int oldPolicy = SendScintilla(SCI_SETYCARETPOLICY, CARET_STRICT|CARET_EVEN);

  markerDeleteAll(mMarker);

  markerAdd(adjustedLine, mMarker);
  ensureLineVisible(adjustedLine);
  setCursorPosition(adjustedLine,0);
  ensureCursorVisible();
  SendScintilla(SCI_SETYCARETPOLICY, oldPolicy);

  setFocus();
}

void QTextEditor::fixedWidth() 
{
}

void QTextEditor::editorSettingsChanged(DlgPreferences* prefs)
{
  QSettings settings;

  QsciLexer* lex = lexer();
  if (lex)
  {
    lex->readSettings(settings, TE_LANGUAGE_PREFIX);
    QString lang = lex->language();
    if (lang.toLower() == "javascript")
    {
      JavaScriptAPIs* apis = static_cast<JavaScriptAPIs*>(lex->apis());
      bool enableVars = prefs->getViewPreference(DlgPreferences::ViewIntellisenseVariables).toBool();
      apis->enableIntellisenseVariables(enableVars);
    }
  }

  int tabSize = prefs->getGeneralPreference(DlgPreferences::GeneralTabSize).toInt();
  setTabWidth(tabSize);

  int indentSize = prefs->getGeneralPreference(DlgPreferences::GeneralIndentSize).toInt();
  setIndentationWidth(indentSize);

  setIndentationsUseTabs(prefs->getGeneralPreference(DlgPreferences::GeneralConvertTabs).toBool());

  setAutoIndent(prefs->getViewPreference(DlgPreferences::ViewAutoIndent).toBool());

  setIndentationGuides(prefs->getViewPreference(DlgPreferences::ViewIndentationGuide).toBool());

  bool braceMatch = prefs->getViewPreference(DlgPreferences::ViewBraceMatching).toBool();
  if (braceMatch)
    setBraceMatching(QsciScintilla::StrictBraceMatch);
  else
    setBraceMatching(QsciScintilla::NoBraceMatch);

  bool viewLineNumbers = prefs->getViewPreference(DlgPreferences::ViewLineNumbers).toBool();
  if (viewLineNumbers)
  {
    UtString linesText;
    linesText << lines() << "9";
    setMarginWidth(0, linesText.c_str());
    setMarginLineNumbers(0, true);
  }
  else
  {
   setMarginWidth(0, 0);
   setMarginLineNumbers(0, false);
  }

  bool viewFoldMargin = prefs->getViewPreference(DlgPreferences::ViewFoldMargin).toBool();
  if (viewFoldMargin)
    setMarginWidth(2, mDefMargin1Width);
  else
     setMarginWidth(2, 0);  

  setEolVisibility(prefs->getViewPreference(DlgPreferences::ViewEndOfLine).toBool());

  bool showWS = prefs->getViewPreference(DlgPreferences::ViewWhitespace).toBool();
  if (showWS)
    setWhitespaceVisibility(QsciScintilla::WsVisible);
  else
    setWhitespaceVisibility(QsciScintilla::WsInvisible);

  qDebug() << "Editor changed";
}

void QTextEditor::languageSettingsChanged(const QString& language)
{
  qDebug() << "Language Changed" << language;
}

