#ifndef WIZDLGEXECUTESCRIPT_H
#define WIZDLGEXECUTESCRIPT_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include "KitManifest.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"

#include <QDialog>
#include "ui_WizDlgExecuteScript.h"

class ModelKitWizard;

class WizDlgExecuteScript : public QDialog
{
  Q_OBJECT

public:
  WizDlgExecuteScript(QWidget *parent = 0, ModelKitWizard* wiz = 0, KitActionExecuteScript* action = 0);
  ~WizDlgExecuteScript();

  void applyData(KitActionExecuteScript* action);
  KitUserAction* createAction();

private:
  void showError(const QString& msg)
  {
    QMessageBox::critical(NULL, MODELSTUDIO_TITLE, msg);
  }

  bool checkInputs();

private:
  Ui::WizDlgExecuteScriptClass ui;
  ModelKitWizard* mWizard;
  KitActionExecuteScript* mAction;


private slots:
    void on_pushButtonAddArg_2_clicked();
    void on_pushButtonAddArg_clicked();
    void on_pushButtonBrowseScriptDir_clicked();
    void on_buttonBox_rejected();
    void on_buttonBox_accepted();
    void on_pushButtonBrowseScript_clicked();
};

#endif // WIZDLGEXECUTESCRIPT_H
