#ifndef DLGRUNSCRIPT_H
#define DLGRUNSCRIPT_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "ScriptingEngine.h"
#include <QtGui>
#include "ui_DlgRunScript.h"

class DlgRunScript : public QDialog
{
  Q_OBJECT

public:
  DlgRunScript(QWidget *parent = 0);
  ~DlgRunScript();

private:
  QSettings mSettings;

private:
  Ui::DlgRunScriptClass ui;

private:
  void saveSettings();
  void restoreSettings();
  ScriptingEngine mEngine;

private slots:
    void on_pushButtonClose_clicked();
    void on_pushButtonRun_clicked();
    void on_toolButtonBrowse_clicked();
};

#endif // DLGRUNSCRIPT_H
