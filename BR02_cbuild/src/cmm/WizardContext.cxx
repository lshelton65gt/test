//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "WizardContext.h"
#include "Response.h"
#include "DlgParameters.h"
#include "ElaboratedModel.h"
#include <QUiLoader>
#include <QtScript>
#include "cmm.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"
#include "DesignXML.h"


ElaboratedModel* WizardContext::getModel()
{
  return &mModel;
}

static QScriptValue expandFunction(QScriptContext *ctx, QScriptEngine *eng)
{
  int nArgs = ctx->argumentCount();
  if (nArgs > 1)
  {
    QScriptValue portScriptObj = ctx->argument(0);
    Port* port = dynamic_cast<Port*>(portScriptObj.toQObject());
    if (port)
    {
      QString expandedValue = port->getFormat();
      for (int i=1; i<nArgs; i += 2)
      {
        QString argName = QString("^(%1)").arg(ctx->argument(i).toString());
        QString argValue = ctx->argument(i+1).toString();

        expandedValue = expandedValue.replace(argName, argValue);        
      }    

      QString baseName = port->getBaseName();
      if (baseName.length() > 0)
      {      
        QRegExp exp("(\\$\\(.*\\))");
        int ix = exp.indexIn(expandedValue);
        int nc = exp.numCaptures();
        if (ix != -1 && nc == 1)
        {
          QString varname = exp.cap(1);
          expandedValue = expandedValue.replace(varname, baseName);
        }
      }

      return QScriptValue(eng, expandedValue);
    }
    else
    {
      QScriptValue caller = ctx->activationObject();

      qDebug() << portScriptObj.toString();
      qDebug() << caller.toString();

      ctx->throwError(QScriptContext::SyntaxError, "No such port: " + portScriptObj.toString());
    }
  }

  return eng->undefinedValue();
}

void WizardContext::addBuiltinFunctions()
{ 
  mEngine->setEnableDebugger(mEnableDebugger);

  // Add the base functions
  mEngine->addBuiltinFunctions();

  qDebug() << "WizardContext" << "addingBuiltinFunctions";

  QScriptValue global = mEngine->globalObject();

  // these are added because these classes expose Q_ENUM's
  QScriptValue staticPort = mEngine->newQMetaObject(&Port::staticMetaObject);
  global.setProperty("Port", staticPort);

  QScriptValue staticPorts = mEngine->newQMetaObject(&Ports::staticMetaObject);
  global.setProperty("Ports", staticPorts);

  QScriptValue staticDesignFile= mEngine->newQMetaObject(&XDesignFile::staticMetaObject);
  global.setProperty("DesignFile", staticDesignFile);

  QScriptValue staticIfacePort = mEngine->newQMetaObject(&XInterfacePort::staticMetaObject);
  global.setProperty("InterfacePort", staticIfacePort);

  QScriptValue expandFunc = mEngine->newFunction(expandFunction);
  if (!global.property(QLatin1String("expandPort")).isValid())
    global.setProperty(QLatin1String("expandPort"), expandFunc);

  // Import included files
  QFile fileCommon(":memwiz/Resources/common.js");
  fileCommon.open(QIODevice::ReadOnly);
  QScriptValue resultCommon = mEngine->evaluate(fileCommon.readAll());
  if (mEngine->hasUncaughtException())
  {
    QMessageBox::critical(NULL, "Script Error: common.js", 
      tr("Exception %1 on line %2")
      .arg(mEngine->uncaughtException().toString())
      .arg(mEngine->uncaughtExceptionLineNumber()));
  }
}
WizardContext::WizardContext(ScriptingEngine* engine)
{
  char* envEnableDebugger = getenv("CARBON_REMODEL_DEBUGGER");
  mEnableDebugger = envEnableDebugger != NULL ? true : false;
  mEngine = engine;
  addBuiltinFunctions();
}

QObject* WizardContext::getDesignHierarchy()
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  if (proj)
  {
    XDesignHierarchy* dh = NULL;
    const char* designHierarchyFile = proj->getDesignFilePath(".designHierarchy");
    QFileInfo fi(designHierarchyFile);
    if (fi.exists())
    {
      dh = new XDesignHierarchy(designHierarchyFile);
      XmlErrorHandler* eh = dh->getErrorHandler();      
      if (eh->hasErrors())
      {
        QString msg = QString("Error reading design hierarchy: %1\n%2")
          .arg(designHierarchyFile)
          .arg(eh->errorMessages().join("\n"));
        QMessageBox::critical(NULL, MODELSTUDIO_TITLE, msg);
        return NULL;
      }
    }
    return dh;
  }
  else
    return NULL;
}

QString WizardContext::generateCode()
{  
  Response response;
  QScriptValueList list;
  list.append(mEngine->newQObject(this));
  list.append(mEngine->newQObject(&response));

  QScriptValue pv = callScriptFunction("generateCode", list);
  qDebug() << pv.isNull() << pv.isBoolean() << pv.toString();

  if (!pv.isUndefined())
    QMessageBox::critical(NULL, "Script Error", pv.toString());

  return response.getResponse();
}


