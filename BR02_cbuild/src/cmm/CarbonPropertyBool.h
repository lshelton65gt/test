#ifndef __CARBONPROPBOOL_H__
#define __CARBONPROPBOOL_H__

#include "util/CarbonPlatform.h"
#include "CarbonProperty.h"
#include "PropertyEditor.h"

class CarbonPropertyBool : public CarbonProperty
{
public:
  CARBONMEM_OVERRIDES

    virtual CarbonDelegate* createDelegate(QObject* parent, PropertyEditorDelegate* d, PropertyEditor* propEd);
  virtual bool parseXML(xmlNodePtr parent, UtXmlErrorHandler* eh);
  virtual bool writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* eh);
  virtual bool readValueXML(xmlNodePtr parent, CarbonOptions* options, UtXmlErrorHandler* eh) const;
  CarbonPropertyBool(const char* name, int nv, const char* descr) : CarbonProperty(name,nv,descr,CarbonProperty::Bool){}
};


class CarbonDelegateBool : public CarbonDelegate
{
  Q_OBJECT
public:
  CarbonDelegateBool(QObject *parent = 0, PropertyEditorDelegate* d=0, PropertyEditor* e=0);
  QWidget* createEditor(QWidget *parent, QTreeWidgetItem* item, CarbonProperty* prop, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const;

  private slots:
    void currentIndexChanged(int index);
};

#endif
