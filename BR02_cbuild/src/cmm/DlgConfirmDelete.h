#ifndef DLGCONFIRMDELETE_H
#define DLGCONFIRMDELETE_H
#include "util/CarbonPlatform.h"

#include <QDialog>
#include "ui_DlgConfirmDelete.h"

class DlgConfirmDelete : public QDialog
{
    Q_OBJECT

public:
    DlgConfirmDelete(QWidget *parent = 0, const char* delMsg=0, const char* remMsg=0);
    ~DlgConfirmDelete();

    enum ConfirmResult { Cancel=0, Delete, Remove };

private:
    Ui::DlgConfirmDeleteClass ui;

private slots:
        void on_pushButtonCancel_clicked();
        void on_pushButtonDelete_clicked();
        void on_pushButtonRemove_clicked();
};

#endif // DLGCONFIRMDELETE_H
