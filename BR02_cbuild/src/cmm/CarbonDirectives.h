#ifndef __CARBONDIRECTIVES_H__
#define __CARBONDIRECTIVES_H__

#include "util/CarbonPlatform.h"

#include "util/UtString.h"

#include "util/XmlParsing.h"
#include "CarbonComponent.h"
#include "Directives.h"
#include "HierarchyDockWindow.h"

class CarbonOptions;
class CarbonProject;
class CarbonProperties;
class CarbonProjectWidget;
class CarbonConfigurations;
class CarbonConfiguration;
class CarbonProperty;
class CarbonDirectives;

class CarbonDirectiveGroup
{
public:
  CarbonDirectiveGroup(CarbonDirectives* dirs, CarbonProject* proj, const char* name, const char* platform);
  const char* getName() const { return mName.c_str(); }
  void putName(const char* newVal)
  {
    mName.clear(); 
    mName << newVal; 
  }

  void addNetDirective(NetDirectives* dir) { mNetDirectives.push_back(dir); }
  void addModuleDirective(ModuleDirectives* dir) { mModuleDirectives.push_back(dir); }
  const char* getPlatform() const { return mPlatform.c_str(); }
  bool serialize(CarbonDirectives*, xmlTextWriterPtr writer);
  static CarbonDirectiveGroup* deserialize(CarbonDirectives* dirs, xmlNodePtr parent, UtXmlErrorHandler* eh);
  void removeAllNetDirectives();
  void removeAllModuleDirectives();
  QList<NetDirectives*>* getNetDirectives() { return &mNetDirectives; }
  QList<ModuleDirectives*>* getModuleDirectives() { return &mModuleDirectives; }
  CarbonDirectives* getDirectives() const { return mDirectives; }
  NetDirectives* findNetDirectives(const char* name);
  ModuleDirectives* findModuleDirectives(const char* name);
  void copyDirectives(CarbonDirectiveGroup* copyFrom);

private:
  CarbonDirectives* mDirectives;
  UtString mName;
  UtString mPlatform;
  CarbonProject* mProject;
  QList<NetDirectives*> mNetDirectives;
  QList<ModuleDirectives*> mModuleDirectives;
};

class CarbonNetDirectivesTool;
class CarbonModuleDirectivesTool;

class CarbonDirectives : public QObject
{
  Q_OBJECT

public:
  CarbonDirectives(CarbonProject* proj);

  QList<CarbonDirectiveGroup*>* getConfigurations() { return &mConfigs; }

  CarbonDirectiveGroup* addDirectiveGroup(const char* name, const char* platform)
  {
    CarbonDirectiveGroup* dir = new CarbonDirectiveGroup(this, mProject, name, platform);
    mConfigs.push_back(dir);
    return dir;
  }

  CarbonProject* getProject() { return mProject; }

  CarbonDirectiveGroup* getActiveGroup() const { return mActiveGroup; }

  void deserialize(CarbonProject* proj, xmlNodePtr parent, UtXmlErrorHandler* eh);
  bool serialize(xmlTextWriterPtr writer);
  CarbonDirectiveGroup* findDirectiveGroup(const char* newConfig);
  CarbonDirectiveGroup* findDirectiveGroup(const char* name, const char* platform);
  DirectiveDefinition* findDefinition(const char* name) { return mDefinitionMap.value(name); }
  QList<DirectiveDefinition*>* getDefinitions() { return &mDirectorDefinitions; }

private:
  void readDefinitions();


public slots:
  void netSelectionChanged(const char*);

private slots:
  void configurationChanged(const char*);
  void configurationRenamed(const char*, const char*);
  void configurationCopy(const char*, const char*);
  void configurationRemoved(const char*);
  void configurationAdded(const char*);
  void moduleSelectionChanged(const char*);
  void projectLoaded(const char*, CarbonProject*);
  void projectClosing(CarbonProject*);
  void netItemSelectionChanged(const char*, const QVariant&);
  void moduleItemSelectionChanged(const char*, const QVariant&);
  void propertyModified(const CarbonProperty*, const char*, const char*);
  void modPropertyModified(const CarbonProperty*, const char*, const char*);

private:
  QList<CarbonDirectiveGroup*> mConfigs;
  CarbonDirectiveGroup* mActiveGroup;
  CarbonProject* mProject;
  QList<DirectiveDefinition*> mDirectorDefinitions;
  QMap<QString, DirectiveDefinition*> mDefinitionMap; // directiveName -> Defintion
  CarbonNetDirectivesTool* mNetToolOptions;
  CarbonModuleDirectivesTool* mModuleToolOptions;
};

#endif
