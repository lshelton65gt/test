#ifndef DLGAUTHENTICATE_H
#define DLGAUTHENTICATE_H

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include <QDialog>
#include "ui_DlgAuthenticate.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "CarbonOptions.h"


class DlgAuthenticate : public QDialog
{
  Q_OBJECT

public:
  DlgAuthenticate(QWidget *parent = 0);
  ~DlgAuthenticate();
  void setOptions(CarbonOptions* options);

  QString getUsername() { return ui.lineEditUsername->text(); }
  QString getServer() { return ui.lineEditServerName->text(); }

private:
  Ui::DlgAuthenticateClass ui;
  bool checkField(QLabel* label, QLineEdit* control);
  bool updateWarnings();

private slots:
  void on_buttonBox_rejected();
  void on_buttonBox_accepted();

private:
  CarbonOptions* mOptions;
};

#endif // DLGAUTHENTICATE_H
