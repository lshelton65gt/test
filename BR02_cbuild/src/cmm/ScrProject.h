#ifndef __SCRPROJ__
#define __SCRPROJ__

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>

#include "Scripting.h"
#include <QtScript/QScriptable>
#include <QtScript/QScriptClass>
#include <QtScript/QScriptString>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>

#include "CarbonComponent.h"

#include "CowareComponent.h"
#include "MaxsimComponent.h"
#include "SystemCComponent.h"

class JavaScriptAPIs;
class XDesignHierarchy;

class ProjectEnum : public QObject
{
  Q_OBJECT
  Q_CLASSINFO("EnumPrefix", "ProjectEnum");

public:
  enum ComponentType 
  {
    UnknownComponent=CarbonComponent::UnknownComponent,
    SoCDesigner=CarbonComponent::SoCDesigner,
    CoWare=CarbonComponent::CoWare,
    ModelValidation=CarbonComponent::ModelValidation, 
    SystemC=CarbonComponent::SystemC,
    Packaging=CarbonComponent::Packaging 
  };

  Q_ENUMS(ComponentType);
};

class ScrProject : public DocumentedQObject, public QScriptable
{
  Q_OBJECT

  Q_CLASSINFO("ClassName", "Project");
  Q_CLASSINFO("SoCDesigner", "MaxsimComponent*");
  Q_CLASSINFO("CoWare", "CowareComponent*");
  Q_CLASSINFO("SystemC", "SystemCComponent*");
  Q_CLASSINFO("designHierarchy", "XDesignHierarchy*");

public:
  Q_PROPERTY(XDesignHierarchy* designHierarchy READ designHierarchy);
  Q_PROPERTY(QString projectDirectory READ projectDirectory);
  Q_PROPERTY(QString outputDirectory READ outputDirectory);
  Q_PROPERTY(QString baseName READ getBaseName);
  Q_PROPERTY(quint32 numComponents READ numComponents);
  Q_PROPERTY(MaxsimComponent* SoCDesigner READ getMaxsimComp);
  Q_PROPERTY(CowareComponent* CoWare READ getCowareComp);
  Q_PROPERTY(SystemCComponent* SystemC READ getSystemCComp);
  Q_PROPERTY(CarbonOptions* options READ getOptions);
  Q_PROPERTY(CarbonOptions* compilerOptions READ getCompilerOptions);

private:
  MaxsimComponent* getMaxsimComp();
  CowareComponent* getCowareComp();
  SystemCComponent* getSystemCComp();

protected:
  virtual QString methodDescr(const QString& methodName, bool formatHtml) const;
  virtual QString propertyDescr(const QString& propName, bool formatHtml) const;

public slots:
  CarbonOptions* getOptions() const;
  CarbonOptions* getCompilerOptions() const;
  QString getHashValue(const QString& pathName) const;
  QString getDesignFileName(const QString& fileExtension) const;
  QString getBaseName() const;
  quint32 numComponents() const;

  void addSourceFile(const QString& fileName, const QString& folderName, bool libraryFile = false);
  void save();
  QObject* createComponent(ProjectEnum::ComponentType type);
  bool deleteComponent(ProjectEnum::ComponentType type);

  QObject* getComponent(quint32 index);

public:
  ScrProject(QObject* parent=0) : DocumentedQObject(parent) {}
  static void registerTypes(QScriptEngine* engine);
  static void registerIntellisense(JavaScriptAPIs* apis);
  static void registerScriptTypes(QMap<QString, const QMetaObject*>& map);

public slots:
  QString projectDirectory() const;
  QString outputDirectory() const;
  XDesignHierarchy* designHierarchy() const;
};

Q_DECLARE_METATYPE(CowareComponent*)
Q_DECLARE_METATYPE(MaxsimComponent*)
Q_DECLARE_METATYPE(SystemCComponent*)
Q_DECLARE_METATYPE(ProjectEnum::ComponentType)

#endif
