#ifndef MKWPAGEUSERACTIONS_H
#define MKWPAGEUSERACTIONS_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>
#include "WizardPage.h"

#include <QWizardPage>

#include "ui_MKWPageUserActions.h"

class KitUserAction;

class MKWPageUserActions : public WizardPage
{
  Q_OBJECT

public:
  MKWPageUserActions(QWidget *parent = 0);
  ~MKWPageUserActions();

  enum ActionWhen { PreAction, PostAction };

private:
  void insertItem(KitUserAction* action);
  void updateButtons();
  void showItemData(QTreeWidgetItem* item, KitUserAction* action);
  void initializeTree(QTreeWidget* tree, const QStringList& headers);
  QTreeWidget* activeTreeTab() { return mActiveTree; }

protected:
  virtual void initializePage();
  virtual bool validatePage();

private slots:
  void on_tabWidget_currentChanged(int);
  void on_treeWidgetPre_itemDoubleClicked(QTreeWidgetItem*,int);
  void on_treeWidgetPost_itemDoubleClicked(QTreeWidgetItem*,int);
  void on_pushButtonMoveDown_clicked();
  void on_pushButtonMoveUp_clicked();
  void on_pushButtonDeleteAction_clicked();
  void on_pushButtonEdit_clicked();
  void on_treeWidgetPre_itemSelectionChanged();
  void on_treeWidgetPost_itemSelectionChanged();
  void on_pushButtonAddAction_clicked();

private:
  Ui::MKWPageUserActionsClass ui;
  QTreeWidget* mActiveTree;
  ActionWhen mWhen;
};

#endif // MKWPAGEUSERACTIONS_H
