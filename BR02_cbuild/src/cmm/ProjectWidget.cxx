//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "ProjectWidget.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonProjectNodes.h"
#include "CarbonSourceGroups.h"
#include "CarbonHDLSourceFile.h"


ProjectWidget::ProjectWidget(QWidget *parent, CarbonMakerContext* ctx)
: QWidget(parent)
{
  mContext = ctx;
  ui.setupUi(this);
  connect(ui.treeWidget, SIGNAL(itemSelectionChanged()), this, SLOT(itemSelectionChanged()));
  itemSelectionChanged();
}

ProjectWidget::~ProjectWidget()
{
}

void ProjectWidget::setContext(CarbonMakerContext* ctx)
{
  ui.treeWidget->putContext(ctx);
}

void ProjectWidget::itemSelectionChanged()
{
  ui.pushButtonMoveUp->setEnabled(false);
  ui.pushButtonMoveDown->setEnabled(false);
  ui.pushButtonDelete->setEnabled(false);

  if (ui.treeWidget->selectedItems().count() > 0)
  {
    foreach (QTreeWidgetItem* item, ui.treeWidget->selectedItems())
    {
      QTreeWidgetItem* parent = item->parent();
      int childIndex = -1;
      int nChildren = 0;
      if (parent)
      {
        childIndex = parent->indexOfChild(item);
        nChildren = parent->childCount();
      }

      CarbonHDLFolderItem* folder = dynamic_cast<CarbonHDLFolderItem*>(item);      
      CarbonSourceItem* file = dynamic_cast<CarbonSourceItem*>(item);

      // We can move around HDLFolderItems, but not other folder types
      if (folder)
      {
        ui.pushButtonDelete->setEnabled(true);
        ui.pushButtonMoveUp->setEnabled(childIndex > 0);
        ui.pushButtonMoveDown->setEnabled(false);
        if (childIndex < nChildren-1)
        {
          // Check the next child, see if it is another folder
          CarbonHDLFolderItem* nextFolder = dynamic_cast<CarbonHDLFolderItem*>(parent->child(childIndex+1));
          ui.pushButtonMoveDown->setEnabled(nextFolder);
        }
      }
      else if (file) // Files can be moved within folders no problem
      {
        ui.pushButtonMoveUp->setEnabled(childIndex > 0);
        ui.pushButtonMoveDown->setEnabled(childIndex < nChildren-1);
        ui.pushButtonDelete->setEnabled(true);
      }
      break;
    }
  }
}


void ProjectWidget::on_pushButtonMoveUp_clicked()
{
  ui.treeWidget->moveUp();
}

void ProjectWidget::on_pushButtonMoveDown_clicked()
{
  ui.treeWidget->moveDown();
}

void ProjectWidget::on_pushButtonDelete_clicked()
{
  ui.treeWidget->deleteSource();
}

