//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QtGui>

#include "gui/CQt.h"

#include "util/XmlParsing.h"

#include "util/CarbonAssert.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"

#include "cmm.h"
#include "CarbonMakerContext.h"
#include "CarbonCompilerTool.h"
#include "CarbonPropertyString.h"
#include "CarbonProjectWidget.h"
#include "DlgPickDate.h"


bool CarbonPropertyString::parseXML(xmlNodePtr /*parent*/, UtXmlErrorHandler* /*eh*/)
{
  return true;
}

bool CarbonPropertyString::readValueXML(xmlNodePtr parent,  CarbonOptions* options, UtXmlErrorHandler* /*eh*/) const
{
  for (xmlNode *child = parent->children; child != NULL; child = child->next) 
  {
    const char* element = XmlParsing::elementName(child);
    if (element == NULL) // Skip non-interesting XML things
      continue;

    if (0 == strcmp(element, "Value"))
    {
      UtString value;
      XmlParsing::getContent(child, &value);
      options->putValue(this, value.c_str());
    }
  }
  return true;}

bool CarbonPropertyString::writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* /*eh*/)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Option");
  xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST getName());

  xmlTextWriterStartElement(writer, BAD_CAST "Value");
  xmlTextWriterWriteString(writer, BAD_CAST value->getValue());
  xmlTextWriterEndElement(writer);

  xmlTextWriterEndElement(writer);
  return true;
}
CarbonDelegate* CarbonPropertyString::createDelegate(QObject* parent,  PropertyEditorDelegate* delegate, PropertyEditor* propEditor)
{
  return new CarbonDelegateString(parent, delegate, propEditor);
}

// Delegate

// Editor Delegate
CarbonDelegateString::CarbonDelegateString(QObject *parent, PropertyEditorDelegate* delegate, PropertyEditor* editor)
: CarbonDelegate(parent,delegate,editor)
{
}

void CarbonDelegateString::commitAndCloseEditor()
{
  QLineEdit *widget = qobject_cast<QLineEdit *>(sender());
  if (widget)
    mDelegate->commit(widget);
}

QWidget* CarbonDelegateString::createEditor(QWidget *parent, QTreeWidgetItem* /*item*/, CarbonProperty* prop, const QModelIndex& /*index*/) const
{
  bool id,is;
  CarbonPropertyValue* sv = mEditor->getSettings()->getValue(prop,&id,&is);

  QLineEdit* lineEdit = new QLineEdit(parent);

  if (prop->hasLegalPattern())
  {
    QRegExp rx(prop->getLegalPattern());
    QRegExpValidator* validator = new QRegExpValidator(rx, parent);
    lineEdit->setValidator(validator);
  }


  lineEdit->setText(sv->getValue());
  connect(lineEdit, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
  return lineEdit;
}

void CarbonDelegateString::setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const
{
  bool id,is;

  if (mEditor->getSettings() == NULL)
    return;

  CarbonPropertyValue* sv = mEditor->getSettings()->getValue(prop,&id,&is);
  if (sv == NULL)
    return;

  UtString currentValue = sv->getValue();

  QLineEdit* lineEdit = qobject_cast<QLineEdit *>(editor);
  if (lineEdit != NULL)
  {
    UtString value;
    value << lineEdit->text();

    const QValidator* v = lineEdit->validator();
    int pos = 0;

    QString currText = lineEdit->text();
    if (v && v->validate(currText, pos) == QValidator::Invalid)
    {
      qDebug() << "Illegal value" << lineEdit->text();
      return;
    }

    if (prop->getRequired() && value.length() == 0)
      return;

    setValue(sv, item, model, index, currentValue.c_str(), value.c_str(), prop);
  }
}

// Date

bool CarbonPropertyDate::parseXML(xmlNodePtr /*parent*/, UtXmlErrorHandler* /*eh*/)
{
  return true;
}

bool CarbonPropertyDate::readValueXML(xmlNodePtr parent,  CarbonOptions* options, UtXmlErrorHandler* /*eh*/) const
{
  for (xmlNode *child = parent->children; child != NULL; child = child->next) 
  {
    const char* element = XmlParsing::elementName(child);
    if (element == NULL) // Skip non-interesting XML things
      continue;

    if (0 == strcmp(element, "Value"))
    {
      UtString value;
      XmlParsing::getContent(child, &value);
      options->putValue(this, value.c_str());
    }
  }
  return true;}

bool CarbonPropertyDate::writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* /*eh*/)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Option");
  xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST getName());

  xmlTextWriterStartElement(writer, BAD_CAST "Value");
  xmlTextWriterWriteString(writer, BAD_CAST value->getValue());
  xmlTextWriterEndElement(writer);

  xmlTextWriterEndElement(writer);
  return true;
}
CarbonDelegate* CarbonPropertyDate::createDelegate(QObject* parent,  PropertyEditorDelegate* delegate, PropertyEditor* propEditor)
{
  return new CarbonDelegateDate(parent, delegate, propEditor);
}

// Delegate

// Editor Delegate
CarbonDelegateDate::CarbonDelegateDate(QObject *parent, PropertyEditorDelegate* delegate, PropertyEditor* editor)
: CarbonDelegate(parent,delegate,editor)
{
}

void CarbonDelegateDate::commitAndCloseEditor()
{
  QLineEdit *widget = qobject_cast<QLineEdit *>(sender());
  if (widget)
    mDelegate->commit(widget);
}

QWidget* CarbonDelegateDate::createEditor(QWidget *parent, QTreeWidgetItem* item, CarbonProperty* prop, const QModelIndex& index) const
{
  bool id,is;
  CarbonPropertyValue* sv = mEditor->getSettings()->getValue(prop,&id,&is);

  DlgPickDate* dateEdit = new DlgPickDate(parent);

  dateEdit->getCalendar()->setVerticalHeaderFormat(QCalendarWidget::NoVerticalHeader);

  UtString currentValue;
  currentValue << sv->getValue();
  
  if (currentValue.length() > 0)
  {
    QDate date = QDate::fromString(sv->getValue(), "yyyy-MM-dd");
    dateEdit->getCalendar()->setSelectedDate(date);
  }

  CarbonMakerContext* ctx = theApp->getContext();
  QString licenseEnabled = prop->getLicenseEnabled();

  // Maximum date?
  if (ctx->getCanTimeBomb() && !licenseEnabled.isEmpty() && licenseEnabled.toLower() == "timebomb" && !ctx->getTimeBombDate().isNull())
    dateEdit->getCalendar()->setMaximumDate(ctx->getTimeBombDate());


  int result = dateEdit->exec();
  if (result == (int)QDialogButtonBox::Yes)
  {
    UtString newValue;
    newValue << dateEdit->getCalendar()->selectedDate().toString("yyyy-MM-dd");
    setValue(sv, item, NULL, index, currentValue.c_str(), newValue.c_str(), prop);
  }
  else if (result == (int)QDialogButtonBox::Reset)
  {
    UtString newValue;
    setValue(sv, item, NULL, index, currentValue.c_str(), newValue.c_str(), prop);
  }

  return 0;
}

void CarbonDelegateDate::setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const
{
  bool id,is;

  if (mEditor->getSettings() == NULL)
    return;

  CarbonPropertyValue* sv = mEditor->getSettings()->getValue(prop,&id,&is);
  if (sv == NULL)
    return;

  UtString currentValue = sv->getValue();

  QCalendarWidget* calEdit = qobject_cast<QCalendarWidget *>(editor);
  if (calEdit != NULL)
  {
    UtString value;
    value << calEdit->selectedDate().toString();
    setValue(sv, item, model, index, currentValue.c_str(), value.c_str(), prop);
  }
}


