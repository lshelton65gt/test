// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/CarbonPlatform.h"

#include <QtGui>
#include <QMenuBar>
#include <QToolBar>
#include "gui/CQt.h"
#include "Mdi.h"
#include "MdiOnDemandTrace.h"
#include "MdiRuntime.h"
#include "OnDemandTraceWidget.h"

MDIOnDemandTraceTemplate::MDIOnDemandTraceTemplate(CarbonMakerContext *ctx)
  : MDIDocumentTemplate("Carbon OnDemand Trace", "", "Carbon OnDemand Trace (*.cot)"),
    mContext(ctx)
{
}


void MDIOnDemandTraceTemplate::updateMenusAndToolbars(QWidget* widget)
{
  (void) widget;
}

int MDIOnDemandTraceTemplate::createToolbars()
{
  INFO_ASSERT(mRuntimeTemplate, "Expecting RuntimeTemplate");
  QToolBar* simToolBar = mRuntimeTemplate->getSimulationToolbar();
  INFO_ASSERT(simToolBar, "Expecting RuntimeTemplate to be created before OnDemandTraceTemplate");

  addFriendTemplateToolbar(mRuntimeTemplate, simToolBar);

  return numToolbars();
}

int MDIOnDemandTraceTemplate::createMenus()
{
  return numMenus();
}

MDIWidget* MDIOnDemandTraceTemplate::createNewDocument(QWidget* parent)
{
  (void) parent;
  return NULL;
}

MDIWidget* MDIOnDemandTraceTemplate::openDocument(QWidget* parent, const char* docName)
{
  OnDemandTraceWidget *editor = new OnDemandTraceWidget(this, parent,
                                                        mContext);
  editor->open(docName);
  return editor;
}
