//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "CarbonFiles.h"
#include "CarbonMakerContext.h"
#include "DlgConfirmDelete.h"
#include "SettingsEditor.h"
#include "ScriptingEngine.h"

CarbonFiles::CarbonFiles(CarbonProject* project)
{
  mProject = project;
}

CarbonFile::CarbonFile(CarbonFiles* parent)
{
  mParent = parent;
}

const char* CarbonFile::getFilepath()
{
  UtString fname;
  QFileInfo fi(getFilename());
  if (fi.isRelative())
  {
    OSConstructFilePath(&fname, getParent()->getProject()->getProjectDirectory(), getFilename());
    QFileInfo fi2(fname.c_str());
    fname.clear();
    fname << fi2.canonicalFilePath();
    if (fname.length() == 0)
      fname << fi2.absoluteFilePath();
  }
  else
    fname << fi.filePath();

  mFilePath = fname;
  return mFilePath.c_str();
}

bool CarbonFile::serialize(xmlTextWriterPtr writer)
{
 xmlTextWriterStartElement(writer, BAD_CAST "File");
 xmlTextWriterWriteAttribute(writer, BAD_CAST "RelativePath", BAD_CAST getFilename());
 xmlTextWriterEndElement(writer);
 return true; 
}


bool CarbonFile::deserialize(CarbonFile* file, xmlNodePtr parent, UtXmlErrorHandler*)
{
  UtString relativePath;
  XmlParsing::getProp(parent, "RelativePath", &relativePath);
  file->putFilename(relativePath.c_str());
  return true;
}

bool CarbonFiles::deserialize(xmlNodePtr parent, UtXmlErrorHandler* eh)
{
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "File"))
    {
      CarbonFile* file = new CarbonFile(this);
      file->deserialize(file, child, eh);
      addFile(file);
    }
  }
  return true;
}

bool CarbonFiles::serialize(xmlTextWriterPtr writer)
{
 xmlTextWriterStartElement(writer, BAD_CAST "Files");

  for (int i=0; i<mFiles.size(); ++i)
  {
    CarbonFile* file = mFiles[i];
    file->serialize(writer);
  }

  xmlTextWriterEndElement(writer);
  return true;
}

CarbonFileItem::CarbonFileItem(CarbonProjectWidget *proj, CarbonFile *file, QTreeWidgetItem *parent) : CarbonProjectTreeNode(proj, parent)
{
  actionDelete = new QAction(QIcon(":/cmm/Resources/DeleteHS.png"), "&Delete", this);
  actionDelete->setStatusTip("Delete or remove this file");
  CQT_CONNECT(actionDelete, triggered(), this, deleteFile());

  mProperties = new CarbonProperties();
  mProperties->readPropertyDefinitions(":/cmm/Resources/FileProperties.xml");
  mOptions = new CarbonOptions(NULL, mProperties, "File Properties");
  mFile = file;
  QFileInfo fi(file->getFilename());
  setText(0, fi.fileName());
  setToolTip(0, file->getFilepath());
  updateIcon();

  actionRunScript = NULL;

  QString ext = fi.suffix().toLower();

  if (ext == "js" || ext == "mjs")
  {
    actionRunScript = new QAction(QIcon(":/cmm/Resources/replayNormal.png"), "&Run Script", this);
    actionRunScript->setStatusTip("Execute this Script");
    CQT_CONNECT(actionRunScript, triggered(), this, runScript());
  }

  mOptions->putValue("FilePath", file->getFilename());
  mOptions->registerPropertyChanged("FilePath", "fileNameChanged", this);
}

void CarbonFileItem::runScript()
{
  QFileInfo fi(mFile->getFilename());
  QString ext = fi.suffix().toLower();

  ScriptingEngine* engine = new ScriptingEngine();
  if (ext == "js")
  {
    engine->runScript(fi.filePath());
  }
  else if (ext == "mjs")
  {
    engine->runPlugin(fi.filePath());
  }
}

CarbonFileItem::~CarbonFileItem()
{
  mOptions->unregisterPropertyChanged("FilePath", this);
  delete mOptions;
  delete mProperties;
}

void CarbonFileItem::fileNameChanged(const CarbonProperty*, const char* newValue)
{
  mFile->putFilename(newValue);
  QFileInfo fi(mFile->getFilename());
  setText(0, fi.fileName());
  updateIcon();
}
void CarbonFileItem::keyPressEvent(QKeyEvent* ev)
{
  if (ev->key() == Qt::Key_Delete)
    deleteFile();
}
void CarbonFileItem::singleClicked(int)
{
  QStringList items;
  QVariantList itemValues;

  mOptions->setItems("Project File Properties", items, itemValues);

  mProjectWidget->context()->getSettingsEditor()->setSettings(mOptions);
}

void CarbonFileItem::doubleClicked(int)
{
  mProjectWidget->openSource(mFile->getFilename(), -1, "TextEditor");
}

void CarbonFileItem::updateIcon()
{
  setIcon(0, QIcon(":/cmm/Resources/file-16x16.png"));
}

void CarbonFileItem::showContextMenu(const QPoint& point)
{
  QMenu menu;
  menu.addAction(actionDelete);
  if (actionRunScript)
    menu.addAction(actionRunScript);

  menu.exec(point);
}

void CarbonFileItem::deleteFile()
{
  QFileInfo fi(mFile->getFilename());

  UtString delMsg;
  delMsg << "'" << fi.fileName() << "'";

  DlgConfirmDelete dlg(treeWidget(), delMsg.c_str(), delMsg.c_str());
  DlgConfirmDelete::ConfirmResult result = (DlgConfirmDelete::ConfirmResult)dlg.exec();
  switch (result)
  {
  case DlgConfirmDelete::Delete:
    {
      QFile f(mFile->getFilepath());
      f.remove();
      QTreeWidgetItem* parent = QTreeWidgetItem::parent();
      parent->removeChild(this);
      mFile->getParent()->removeFile(mFile);
    break;
    }
  case DlgConfirmDelete::Remove:
    {
      QTreeWidgetItem* parent = QTreeWidgetItem::parent();
      parent->removeChild(this);
      mFile->getParent()->removeFile(mFile);
      break;
    }
  case DlgConfirmDelete::Cancel:
    break;
  }
}

CarbonFilesFolder::CarbonFilesFolder(CarbonFiles* files, CarbonProjectWidget* proj, QTreeWidgetItem* parent) : CarbonProjectTreeNode(proj,parent) 
{
  mFiles = files;
  setIcon(0, QIcon(":/cmm/Resources/folder-open-16x16.png"));
  setText(0, "Files");
  actionAddFiles = new QAction(QIcon(":/cmm/Resources/addfile.png"), "&Add File(s)", this);
  actionAddFiles->setStatusTip("Add file(s) to the project");
  CQT_CONNECT(actionAddFiles, triggered(), this, addFiles());
}

void CarbonFilesFolder::showContextMenu(const QPoint& point)
{
  QMenu menu;
  menu.addAction(actionAddFiles);
  menu.exec(point);
}

void CarbonFilesFolder::addFile(const char* fileName)
{
  TempChangeDirectory tmpDir(mProjectWidget->project()->getProjectDirectory());
  UtString relFilePath;
  relFilePath << CarbonProjectWidget::makeRelativePath(mProjectWidget->project()->getProjectDirectory(), fileName);

  CarbonFile* file = new CarbonFile(mFiles);
  file->putFilename(relFilePath.c_str());
  mFiles->addFile(file);
  CarbonFileItem* fi = new CarbonFileItem(mProjectWidget, file, this);
  fi->setToolTip(0, fileName);
}

void CarbonFilesFolder::addFiles()
{
  QStringList files = QFileDialog::getOpenFileNames(mProjectWidget, "Select one or more files",
                           mProjectWidget->project()->getProjectDirectory(),
                           "All Files (*)");  
  foreach (QString fileName, files)
  {
    UtString filePath;
    filePath << fileName;
    addFile(filePath.c_str());
  }
}

void CarbonFilesFolder::updateIcons()
{
  for (int i=0; i<childCount(); i++)
  {
    QTreeWidgetItem* item = child(i);
    CarbonFileItem* fileItem = dynamic_cast<CarbonFileItem*>(item);
    if (fileItem)
      fileItem->updateIcon();
  }
}
