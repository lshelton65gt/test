//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "util/OSWrapper.h"

#include "DlgDriveMappings.h"
#include "DlgMapDrives.h"
#include "SSHConsoleHelper.h"

DlgDriveMappings::DlgDriveMappings(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);

  populate();
}

DlgDriveMappings::~DlgDriveMappings()
{
}

void DlgDriveMappings::on_buttonBox_rejected()
{
  reject();
}

void DlgDriveMappings::on_buttonBox_accepted()
{
  mMappedDrives.saveMappings();
  accept();
}

void DlgDriveMappings::on_pushButtonAdd_clicked()
{
  QStringList driveLetters, remoteNames;
  SSHConsoleHelper::getNetworkDrives(driveLetters, remoteNames);
  int numNetDrives = driveLetters.count();
  int numMappedDrives = mMappedDrives.numMappings();

  if (numMappedDrives >= numNetDrives)
  {
    UtString msg;
    msg << "All available network drives have already been mapped.";
    QMessageBox::warning(this, MODELSTUDIO_TITLE, msg.c_str());  
  }
  else
  {
    DlgMapDrives dlg(this, NULL, &mMappedDrives);
    if (dlg.exec() == QDialog::Accepted)
    {
      qDebug() << "Drive Mapped: " << dlg.getDriveLetter() << " to: " << dlg.getUnixPath();
      mMappedDrives.addMapping(dlg.getDriveLetter(), dlg.getUnixPath());
      populate();
    }
  }
}

void DlgDriveMappings::populate()
{
  ui.listMappings->clear();
 
  for (int i=0; i<mMappedDrives.numMappings(); i++)
  {
    CarbonMappedDrive* drive = mMappedDrives.getMapping(i);
    UtString mapping;
    mapping << drive->mDriveLetter << " = " << drive->mUnixPath;
    ui.listMappings->addItem(mapping.c_str());
  }
}

void DlgDriveMappings::on_pushButtonRemove_clicked()
{
  foreach (QListWidgetItem* item, ui.listMappings->selectedItems())
  {
    QString rowText = item->text();
    QString driveLetter = rowText.left(2);
    mMappedDrives.removeMapping(driveLetter);
  }
  populate();
}



