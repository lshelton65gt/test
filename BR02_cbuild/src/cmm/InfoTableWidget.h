#ifndef INFOTABLEWIDGET_H
#define INFOTABLEWIDGET_H

#include "util/CarbonPlatform.h"
#include <QtGui>
#include <QTableWidget>
#include <QRegExp>
#include <QProgressBar>
#include "ui_InfoTableWidget.h"
#include "RemoteConsole.h"
#include "CarbonConsole.h"
#include "util/XmlParsing.h"
#include "util/CarbonAssert.h"

class InfoObject;
class CarbonMakerContext;
class UtXmlErrorHandler;
class CarbonProject;
class MessageExpression;
class CarbonProperty;

#include "CMSCommon.h"


class InfoTableWidget : public QTableWidget
{
  Q_OBJECT

  enum Columns { Category, DefaultOrder, Description, File, Line, MessageNumber };

public:
  enum Severity { Information, Warning, Error, Filtered };

  InfoTableWidget(QWidget *parent = 0);
  ~InfoTableWidget();
  void connectConsole(QPushButton* err, QPushButton* wrn, QPushButton* info, CarbonMakerContext* ctx, CarbonConsole* console, QProgressBar* pb, QStatusBar* sb);
  void toggleDisplay(bool value, Severity sev);
  void displayOutput(CarbonConsole::CommandType ctype, const QString& text, const QString& workingDir);
  void autoFit();
  QList<ErrorMessage*> getErrorMessages() { return mErrorMessages; }
  void removeMessage(ErrorMessage* em);
  void clearAll();
  int  getErrors() const {return mErrors;}
  int  getWarnings() const {return mWarnings;}
  int  getInfos() const {return mInfos;}
private:
  QIcon& severityIcon(Severity sev);
  QIcon& severityIcon(MessageExpression::Severity sev);

  void populateItem(ErrorMessage* em, bool counted=true);

  void openSource(ErrorMessage* em);
  bool readMessageDefinitions(const char* xmlFile);
  bool parseXML(xmlNodePtr parent, UtXmlErrorHandler* eh);
  void createActions();
  void readCarbonHelp();
  void showContextHelp();
  virtual void keyPressEvent(QKeyEvent* ev);
  bool isFiltered(ErrorMessage* em);

private slots:
  void stderrChanged(CarbonConsole::CommandType, const QString& text, const QString& workingDir);
  void stdoutChanged(CarbonConsole::CommandType, const QString& text, const QString& workingDir);
  void itemDoubleClicked(QTableWidgetItem*);
  void compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void showContextMenu(const QPoint &pos);
  void menuVisit();
  void menuCrossProbe();
  void menuClearAll();
  void selectionChanged();
  void timeLineFinished();
  void menuFilterMessage();
  void projectLoaded(const char*, CarbonProject*);
  void filteredMessagesChanged(const CarbonProperty* prop, const char*);
  void makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void menuFilterInstance();

  friend class InfoObject;

signals:
  void showExtendedHelp(const char*);

private:
  int mErrors;
  int mWarnings;
  int mInfos;
  int mFiltered;
  QIcon mIconErr;
  QIcon mIconWarning;
  QIcon mIconInfo;
  CarbonMakerContext* mContext;
  Severity mSeverity;
  QProgressBar* mProgressBar;
  QStatusBar* mStatusBar;
  Ui::InfoTableWidgetClass ui;

  QPushButton* mButtonErrors;
  QPushButton* mButtonWarnings;
  QPushButton* mButtonInfos;

  QAction* mActionVisit;
  QAction* mActionShowHierarchy;
  QAction* mActionClearAll;
  QAction* mActionFilterMsg;
  QAction* mActionFilterInst;

  QList<MessageExpression*> mExpressions;
  QList<ErrorMessage*> mErrorMessages;
  bool mShowErrors;
  bool mShowWarnings;
  bool mShowInfos;
  bool mShowFiltered;

  CarbonConsole::CommandMode mCompileMode;
  CarbonConsole::CommandType mCommandType;

  QMap<UInt32, QString> mMessageMap;

  QTimeLine* mTimeLine;
  QString mProgressFormat;

  UtString mCurrentWorkingDir;
};


#endif // INFOTABLEWIDGET_H
