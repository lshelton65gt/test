//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "ModelKitWizard.h"
#include "CarbonMakerContext.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "CarbonFiles.h"

QString ModelKitWizard::normalizeName(const QString& fileName)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  if (fileName.contains('$'))
    return fileName;

  // ARM Root always ends with '/', remove it in this case
  QString armRootKit = getARMRoot();
  QString rootKit = proj->getUnixEquivalentPath(armRootKit);
  if (rootKit.endsWith("/"))
    rootKit = rootKit.left(rootKit.length()-1);

  QString projRoot = proj->getUnixEquivalentPath(proj->getProjectDirectory());

  UtString tmp;
  tmp << fileName;
  UtString unixName;
  unixName << proj->getUnixEquivalentPath(tmp.c_str());

  QString inputName = unixName.c_str();

  QString name;
  if (!rootKit.isEmpty())
    name = inputName.replace(rootKit, ENV_ARM_ROOT);
  else
    name = inputName;

  name = name.replace(projRoot, ENV_PROJ_ROOT);

  if (name != fileName)
    qDebug() << "translated" << fileName << "to" << name;
  else
    qDebug() << "unchanged normalized file" << fileName << projRoot << rootKit;
  
  return name;
}

QString ModelKitWizard::expandName(const QString& fileName)
{
  QString inputName = fileName;
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QString armRootKit = getARMRoot();

  QString rootKit = proj->getUnixEquivalentPath(armRootKit);
  if (rootKit.endsWith("/"))
    rootKit = rootKit.left(rootKit.length()-1);

  QString projRoot = proj->getUnixEquivalentPath(proj->getProjectDirectory());

  QString name = inputName.replace(ENV_PROJ_ROOT, projRoot);
  if (!rootKit.isEmpty())
    name = name.replace(ENV_ARM_ROOT, rootKit);

  UtString expandedName;
  UtString err;
  UtString uName; uName << name;

  if (OSExpandFilename(&expandedName, uName.c_str(), &err))
    name = expandedName.c_str();

  return proj->getWindowsEquivalentPath(name);
}

QString ModelKitWizard::normalizeProjectName(const QString& fileName)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QString armRootKit = getARMRoot();
  QString rootKit = proj->getUnixEquivalentPath(armRootKit);
  if (rootKit.endsWith("/"))
    rootKit = rootKit.left(rootKit.length()-1);

  QString projRoot = proj->getUnixEquivalentPath(proj->getProjectDirectory());

  UtString tmp;
  tmp << fileName;
  UtString unixName;
  unixName << proj->getUnixEquivalentPath(tmp.c_str());

  QString inputName = unixName.c_str();

  QString name = inputName.replace(projRoot, ENV_PROJ_ROOT);
  if (!rootKit.isEmpty())
    name = name.replace(rootKit, ENV_ARM_ROOT);

  if (name != fileName)
    qDebug() << "translated" << fileName << "to" << name;
  
  return name;
}

// given a folder, collect up all files in it
// and add them to fileList
void ModelKitWizard::getFilesForFolder(QStringList& fileList, const QString& folderName, const QStringList& fileKinds, bool recurseFlag)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  TempChangeDirectory cd(folderName);

  QDir dir(folderName);
  if (dir.exists())
  {
    dir.setFilter(QDir::Files);
    dir.setNameFilters(fileKinds);

    foreach(QFileInfo fileInfo, dir.entryInfoList())
    {
      QString filePath = proj->getUnixEquivalentPath(fileInfo.absoluteFilePath());
      fileList.append(filePath);
    }

    if (recurseFlag)
    {
      QDir subDir(folderName);
      subDir.setFilter(QDir::Dirs);
      foreach (QFileInfo fileInfo, subDir.entryInfoList())
      {      
        if (fileInfo.fileName() != "." && fileInfo.fileName() != "..")
          getFilesForFolder(fileList, fileInfo.absoluteFilePath(), fileKinds, recurseFlag);
      }
    }
  }
}

ModelKitWizard::ModelKitWizard(QWidget *parent)
: QWizard(parent)
{
  ui.setupUi(this);

  mActiveManifest = NULL;
  mModelKit = new ModelKit();
  mCopyFiles = true;
  mAskRoot = true;
  mRegenerate = theApp->checkArgument("-modelKitRegen");

  if (mRegenerate)
    theApp->setShutdownOnDialog(true);

#ifndef Q_WS_MAC
  setWizardStyle(ModernStyle);
#endif

  qDebug() << "launched ModelKit Wizard";

  setPage(ModelKitWizard::PageVersions, new MKWPageVersions(this));
  setPage(ModelKitWizard::PageInfo, new MKWPageInfo(this));
  setPage(ModelKitWizard::PageFiles, new MKWPageFiles(this));
  setPage(ModelKitWizard::PageUserActions, new MKWPageUserActions(this));
  setPage(ModelKitWizard::PageFinished, new MKWPageFinished(this));

  setStartId(ModelKitWizard::PageVersions);

  setWindowTitle(tr("ModelKit Wizard"));

}

bool ModelKitWizard::fileExists(const QString& file)
{
  int tries = 5;
  bool ok = true;
  do
  {
    QFileInfo fi(file);
    fi.setCaching(false); // Don't cache results
    ok = fi.exists();

    if (!ok) { // HACK: Sleep for 2 seconds, wait for NFS to catch up.
      OSSleep(2);
      QTime time = QTime::currentTime();
      QString timeString = time.toString();
      qDebug() << "Sleeping for 2 seconds wrt file " << fi.filePath() << " Current time: " << timeString ;

      // we can put this in if we need to:
      // QString program = "ls";
      // QStringList arguments;
      // arguments << "-lt" << fi.filePath() << "> lsOutput.txt";

      //QProcess *myProcess = new QProcess(this);
      //myProcess->start(program, arguments);

      // OSSleep(2);
    }
  } while (!ok && (tries-- > 0));
  return ok;
}

bool ModelKitWizard::canOpenWizard(QString& errMsg)
{
  errMsg.clear();
  QTextStream stream(&errMsg);

  // Check for Project
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  if (proj == NULL)
    stream << "No project is opened, use the -project switch" << "\n";
  else
  {
    if (!fileExists(proj->getDesignFilePath(".dir")))
      stream << "Missing directives file (lib<Design>.dir) named" << proj->getDesignFilePath(".dir") << "\n";
  
    if (!fileExists(proj->getDesignFilePath(".ncmd")))
      stream << "Missing ncmd file (lib<Design>.ncmd) named" << proj->getDesignFilePath(".ncmd") << "\n";
  }
  
  qDebug() << "ERROR: " << errMsg;

  if (errMsg.isEmpty())
    return true;
  else
    return false;
}

ModelKitWizard::~ModelKitWizard()
{

}
