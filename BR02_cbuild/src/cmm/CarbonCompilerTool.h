#ifndef __CARBONCOMPILERTOOL_H__
#define __CARBONCOMPILERTOOL_H__

#include "util/CarbonPlatform.h"

#include <QtGui>

#include "util/XmlParsing.h"

#include "util/CarbonAssert.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"
#include "util/UtList.h"

#include "CarbonOptions.h"

class CarbonCompilerTool : public CarbonTool
{
public:
  CARBONMEM_OVERRIDES
    CarbonCompilerTool();

};

#endif
