#ifndef __ELABMODEL_H
#define __ELABMODEL_H
#include "util/CarbonVersion.h"
#include "gui/CQt.h"


#include "Scripting.h"
#include "Mode.h"
#include "BlockInstance.h"
#include "PortInstance.h"

class Template;
class WizardContext;

// File: Model
// The Elaborated Model object which represents all the users selections.
//
// See Also:
//   <Context>
//
// Description:
//   The model object.
//   The elaborated model is generated after all template parameters
//   have been defined.

class ElaboratedModel : public QObject, protected QScriptable
{
  Q_OBJECT

// Property: mode
// The <Mode> object that this model is using based upon
// the user selection.
//
// See Also:
//   <Mode>
  Q_PROPERTY(QObject* mode READ getMode)
// Property: blockInstances
// A collection of <BlockInstance> objects
//
// See Also:
// <BlockInstance>
  Q_PROPERTY(QObject* blockInstances READ getBlockInstances)
  
// Property: portInstances
// A collection of <PortInstance> objects
  Q_PROPERTY(QObject* portInstances READ getPortInstances)
  
// Property: uniquePortInstances
// A collection of uniquely named <PortInstance> objects.  This is useful
// when a block that is tied to a parameter is expanded out and the block
// shares a common pin, such as output enable or chip select.  This can
// be used to generate the module declaration since duplicates are
// filtered out.
  Q_PROPERTY(QObject* uniquePortInstances READ getUniquePorts)

public:
  ElaboratedModel()
  {
    mMode = NULL;
  }

  // Elaborate the mode from the template
  bool elaborate(WizardContext* context, Mode* mode);
 
  Mode* getMode();
  BlockInstances* getBlockInstances() { return &mBlockInstances; }
  PortInstances* getPortInstances() { return &mPortInstances; }
  PortInstances* getUniquePorts() { return &mUniquePortInstances; }

protected:
  bool preElaborate(WizardContext* context, Mode* mode);

public slots:
  QObject* getBlockInstances(const QString& blockName);
  QObject* getBlockInstance(const QString& blockName, const QString& instanceID);
  QObject* getPortInstances(const QString& blockName, const QString& instanceID);

private:
  BlockInstances mBlockInstances;
  PortInstances mPortInstances;
  PortInstances mUniquePortInstances;

  Mode* mMode;
};

#endif
