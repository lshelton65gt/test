//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "DlgPickDate.h"

DlgPickDate::DlgPickDate(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);
}

DlgPickDate::~DlgPickDate()
{

}


void DlgPickDate::on_buttonBox_rejected()
{
  reject();
  setResult((int)QDialogButtonBox::Cancel);
}

void DlgPickDate::on_buttonBox_accepted()
{
  accept();
  setResult((int)QDialogButtonBox::Yes);
}

void DlgPickDate::on_buttonBox_reset()
{
  accept();
  setResult((int)QDialogButtonBox::Reset);
}

void DlgPickDate::on_buttonBox_clicked(QAbstractButton* btn)
{
  if (btn == ui.buttonBox->button(QDialogButtonBox::Reset))
    on_buttonBox_reset();
}

