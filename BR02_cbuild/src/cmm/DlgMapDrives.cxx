//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "util/OSWrapper.h"

#include "DlgMapDrives.h"
#include <QtGui>
#include <QDebug>
#include "CarbonProject.h"
#include "SSHConsoleHelper.h"

#if pfWINDOWS
#include <windows.h>
#include <stdio.h>
#endif


void VerifyWriteability::processOutput(const QString& line)
{
  if (line == mExpectedOutput)
    mDriveWriteable = true;
}

void VerifyWriteability::commandFinished(int exitCode)
{
  if (exitCode == 0 && mDriveWriteable)
  {
    putResult(true, "Mapping is Valid");
    putFinishedVerification(true);
  }
  else
  {
    UtString err;
    err << "Error: Drive " << mDriveLetter << " is not equivalent to " << mUnixPath;
    putResult(false, err.c_str());
  }
}

void VerifyUnixDirectoryContents::processOutput(const QString& line)
{
  QRegExp fileEx("\\(([0-9]+)\\): (.*)");
  
  if (fileEx.exactMatch(line))
  {
    QString fileName = fileEx.cap(2);
    mFilenames.push_back(fileName);
  }
  else
    qDebug() << "Error: VerifyUnixDirectoryContents No match";
}
void VerifyUnixDirectoryContents::commandFinished(int)
{
  qDebug() << "Finished";
  UtString msg;
  msg << "Verified " << mDriveLetter << " matches " << mUnixPath;
  putResult(true, msg.c_str());

  foreach (QString unixFileName, mFilenames)
  {
    QFileInfo ufi(unixFileName);
    QString dosFilePath;
    dosFilePath = mDriveLetter + "/" + ufi.fileName();
    QFileInfo dfi(dosFilePath);

    bool fileExists = dfi.isFile() && dfi.exists();
    bool dirExists = dfi.isDir() && dfi.dir().exists();
    bool exists = fileExists || dirExists;

    if (!exists)
    {
      UtString err;
      err << "Error: Drive " << mDriveLetter << " contents are not equivalent to " << mUnixPath;
      putResult(false, err.c_str());
      qDebug() << dosFilePath;
      break;
    }
  }
}


void VerifyUnixDirectory::processOutput(const QString& line)
{
  QRegExp verGoodCmd("CARBON DIRECTORY EXISTS: (.*)");
  QRegExp verBadCmd("CARBON DIRECTORY NOT FOUND: (.*)");

  qDebug() << "VERIFYUNIXDIR: " << line;

  if (verGoodCmd.exactMatch(line))
  {
    UtString msg;
    msg << verGoodCmd.cap(1) << " is a valid Unix directory.";
    putResult(true, msg.c_str());
  }
  else if (verBadCmd.exactMatch(line))
  {
    UtString msg;
    msg << "Error: " << verGoodCmd.cap(1) << " is NOT a valid Unix directory";
    putResult(false, msg.c_str());
  }
}
void VerifyUnixDirectory::commandFinished(int exitCode)
{
  bool currResult = getResult();
  if (exitCode == 0 && currResult)
    putResult(true, getResultString());
  else
    putResult(false, getResultString());
}

void VerifyUnixCommand::commandFinished(int exitCode)
{
  if (exitCode != 0)
  {
    UtString err;
    err << "ERROR: Unix Command: " << getCommand() << " failed with exitStatus=" << exitCode;
    qDebug() << "ERROR: Unix Command failed:";
    qDebug() << getCommand();
    qDebug() << getArgs();
    putResult(false, err.c_str());
  }
  else
  {
    qDebug() << "unix command: " << getCommand() << getArgs() << " finished with exitstatus=0";
    putResult(true, "");
  }
}

void VerifyUnixCommand::processOutput(const QString& line)
{
  qDebug() << "VUC: " << line;
}


QString DlgMapDrives::makeGUID()
{
  return CarbonProject::createGUID();
}


void DlgMapDrives::on_buttonBox_accepted()
{
  mUnixPath = ui.lineEditUnixPath->text();
  mDriveLetter = ui.comboBoxDrives->currentText().left(2);
  accept();
}

void DlgMapDrives::on_buttonBox_rejected()
{
  reject();
}

void DlgMapDrives::enableButtons()
{
  QPushButton* okButton = ui.buttonBox->button(QDialogButtonBox::Ok);

  bool validServerInfo = ui.lineEditServer->text().length() > 0 && ui.lineEditUsername->text().length() > 0;
  bool validUnixPath = ui.lineEditUnixPath->text().length() > 0;

  bool enable = validServerInfo && validUnixPath;

  ui.pushButtonVerify->setEnabled(enable);

  okButton->setEnabled(enable && mVerified);
}

void DlgMapDrives::unixPathChanged()
{
  mVerified = false;
  enableButtons();
}

void DlgMapDrives::serverInfoChanged()
{
  mRestartSession = true;
  mVerified = false;
  enableButtons();
}

void DlgMapDrives::on_comboBoxDrives_currentIndexChanged(int)
{
  unixPathChanged();
}


DlgMapDrives::DlgMapDrives(QWidget *parent, CarbonProjectWidget* pw, CarbonMappedDrives* mappedDrives)
: QDialog(parent)
{
  mMappedDrives = mappedDrives;
  if (mMappedDrives == NULL)
    mMappedDrives = &mLocalMap;

  mProjectWidget = pw;
  mCurrentCommand = NULL;
  mRestartSession = false;
  mVerified = false;

  ui.setupUi(this);

  QSettings settings("Carbon", "Drive Mappings");
  QString user = settings.value("userName").toString();
  QString server = settings.value("serverName").toString();

  ui.lineEditUsername->setText(user);
  ui.lineEditServer->setText(server);

  CQT_CONNECT(ui.lineEditServer, textChanged(const QString&), this, serverInfoChanged());
  CQT_CONNECT(ui.lineEditUsername, textChanged(const QString&), this, serverInfoChanged());
  
  CQT_CONNECT(ui.lineEditUnixPath, textChanged(const QString&), this, unixPathChanged());

  CQT_CONNECT(&mSSHConsole, processReadyForCommands(), this, sendCommand());
  CQT_CONNECT(&mSSHConsole, sessionLoginStatus(const QString&), this, sessionLoginStatus(const QString&));
  CQT_CONNECT(&mSSHConsole, processOutput(const QString&), this, processOutput(const QString&));
  CQT_CONNECT(&mSSHConsole, commandCompleted(int), this, commandCompleted(int));
  CQT_CONNECT(&mSSHConsole, sessionStateChanged(SSHConsole::LoginState), this, sessionStateChanged(SSHConsole::LoginState));

  QStringList drivesList, infoList;

  SSHConsoleHelper::getNetworkDrives(drivesList, infoList);
  for (int i=0; i<drivesList.count(); i++)
  {
    QString driveLetter = drivesList[i];
    QString driveInfo = infoList[i];

    UtString driveName;
    driveName << driveLetter << " (" << driveInfo << ")";

    if (!mMappedDrives->isDriveLetterMapped(driveLetter))
      ui.comboBoxDrives->addItem(driveName.c_str());
  }

  enableButtons();
}

bool DlgMapDrives::driveIsWriteable(const char* driveLetter, QString& tempFile, QString& guid)
{
  guid = makeGUID();
  QString fileName = QString(driveLetter) + "/.carbon." + guid;

  QFile file(fileName);
  if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    return false;
  else
  {
    QTextStream out(&file);
    out << guid << "\n";
    tempFile = ".carbon." + guid;
    file.close();
    return true;
  }
}

void DlgMapDrives::on_pushButtonVerify_clicked()
{
  mVerified = false;
  ui.textEdit->clear();

  ui.pushButtonVerify->setEnabled(false);

  if (mRestartSession && mSSHConsole.isSessionConnected())
    mSSHConsole.abortSession();

  mRestartSession = false;
  mVerifyCommands.clear();

  QSettings settings("Carbon", "Drive Mappings");
  settings.setValue("userName", ui.lineEditUsername->text());
  settings.setValue("serverName", ui.lineEditServer->text());

  mUsername = ui.lineEditUsername->text();
  mServer = ui.lineEditServer->text();

  if (mUsername.startsWith("~"))
  {
    QMessageBox::critical(this, MODELSTUDIO_TITLE, "Usernames starting with a ~ are not supported.");
    return;
  }

  // First check, verify that we can write files to the mapped drive letter:
  UtString driveLetter;
  driveLetter << ui.comboBoxDrives->currentText().left(2);

  UtString unixPath;
  unixPath << ui.lineEditUnixPath->text();

  QStringList args;

  QString tempFile;
  QString guid;

  if (driveIsWriteable(driveLetter.c_str(), tempFile, guid))
  {
    UtString unixCmd;

    unixCmd << "cat '" << unixPath.c_str() << "/" << tempFile << "'" << "\n";
    VerifyWriteability* cmd = new VerifyWriteability();
    cmd->putTitle("");
    cmd->putExpectedOutput(guid);
    cmd->putMappingInfo(driveLetter.c_str(), unixPath.c_str());
    cmd->putCommand(unixCmd.c_str());
    mVerifyCommands.push_back(cmd);
  }

  createVerifyDirectoryCommand();
  createVerifyDirectoryContentsCommand(driveLetter.c_str());

  if (!mSSHConsole.isSessionConnected())
    mSSHConsole.startSession(NULL, ui.lineEditServer->text(), ui.lineEditUsername->text());
  else
    sendCommand();
}

void DlgMapDrives::sessionStateChanged(SSHConsole::LoginState state)
{
  switch (state)
  {
  default:
    break;
  case SSHConsole::Started:
    ui.textEdit->append("Started Process");
    break;
  case SSHConsole::LoggingIn:
    ui.textEdit->append("Logging in");
    break;
  case SSHConsole::Authorizing:
  case SSHConsole::RetryPassword:
    ui.textEdit->append("Authorizing");
    break;
  case SSHConsole::LoggedIn:
    ui.textEdit->append("Logged in");
    break;
  case SSHConsole::LoggedOut:
    ui.textEdit->append("Logged out");
    break;
  case SSHConsole::Terminated:
    ui.textEdit->append("Process Terminated");
    break;
  case SSHConsole::ShellStarted:
    ui.textEdit->append("Shell Ready");
    break;
  case SSHConsole::FailedLogin:
    ui.textEdit->append("Failed to log in");
    break;
  }
}

DlgMapDrives::~DlgMapDrives()
{
  mSSHConsole.abortSession();
}

void DlgMapDrives::commandCompleted(int exitStatus)
{
  if(mCurrentCommand) {
    mCurrentCommand->commandFinished(exitStatus);
    QString resultString = mCurrentCommand->getResultString();

    if (mCurrentCommand->getResult())
    {
      if (resultString.length() > 0)
        ui.textEdit->append(resultString);

      if (!mCurrentCommand->getFinishedVerification() && mVerifyCommands.count() > 0)
        sendCommand();
      else
      {
        ui.textEdit->append("Finished");
        mVerified = true;
        enableButtons();
      } 
    }
    else
    {
      mVerified = false;
      ui.textEdit->append(resultString);
      ui.textEdit->append("Finished with Errors");
    }
  }
}

void DlgMapDrives::sessionLoginStatus(const QString& msg)
{
  ui.textEdit->append(msg);
}

void DlgMapDrives::processOutput(const QString& line)
{
  // Ignore non-relevant output
  if (mCurrentCommand)
    mCurrentCommand->processOutput(line);
}

void DlgMapDrives::sendCommand()
{
  mCommandState = Started;

  if (mVerifyCommands.count() > 0)
  {
    mCurrentCommand = mVerifyCommands.takeFirst();
    if (mCurrentCommand->getTitle().length() > 0)
      ui.textEdit->append(mCurrentCommand->getTitle());
    QStringList args;
    CarbonProject* project = NULL;
    if (mProjectWidget)
      project = mProjectWidget->project();
    mSSHConsole.executeCommand(project, "", args, mCurrentCommand->getCommand(), mCurrentCommand->getArgs(), true);
  }
  else
    mCurrentCommand = NULL;
}

// Build up a shell script which gets the first (up to) 5 files
// in a directory to sanity check they are the same dir.
void DlgMapDrives::createVerifyDirectoryContentsCommand(const QString& driveLetter)
{
  UtString shellCmd;

  QStringList args;
  args << ui.lineEditUnixPath->text();

  shellCmd << "cat > ~/.carbon-files << CARBONEOF" << "\n";
  shellCmd << "#!/bin/sh" << "\n";
  shellCmd << "filecount=0" << "\n";
  shellCmd << "for file in \\$1/*" << "\n";
  shellCmd << "do" << "\n";
  // If file or directory and not a symlink
  shellCmd << "  if [ \\( -f \\$file -o -d \\$file \\) -a ! -L \\$file ]" << "\n";
  shellCmd << "  then" << "\n";
  shellCmd << "    echo \"(\\$filecount): \\$file\"" << "\n";
  shellCmd << "    let \"filecount += 1\"" << "\n";
  shellCmd << "    if [ \\$filecount -eq 5 ]" << "\n";
  shellCmd << "    then" << "\n";
  shellCmd << "      break" << "\n";
  shellCmd << "    fi" << "\n";
  shellCmd << "  fi" << "\n";
  shellCmd << "done" << "\n";
  shellCmd << "CARBONEOF" << "\n";
  shellCmd << "chmod +x ~/.carbon-files" << "\n";

  VerifyUnixCommand* cmd1 = new VerifyUnixCommand();
  cmd1->putCommand(shellCmd.c_str());
  mVerifyCommands.push_back(cmd1);

  VerifyUnixDirectoryContents* cmd2 = new VerifyUnixDirectoryContents();
  cmd2->putTitle("Checking Unix Directory Contents: " + ui.lineEditUnixPath->text());
  cmd2->putCommand("~/.carbon-files");
  cmd2->putArgs(args);
  cmd2->putMappingInfo(driveLetter, ui.lineEditUnixPath->text());
  mVerifyCommands.push_back(cmd2);

}
void DlgMapDrives::createVerifyDirectoryCommand()
{
  UtString shellCmd;

  shellCmd << "cat > ~/.carbon-check << CARBONEOF" << "\n";
  shellCmd << "#!/bin/sh" << "\n";
  shellCmd << "if [ -d \"\\$1\" ]; then \\echo \"CARBON DIRECTORY EXISTS: \\$1\"; else \\echo \"CARBON DIRECTORY NOT FOUND: \\$1\"; fi" << "\n";
  shellCmd << "CARBONEOF" << "\n";
  shellCmd << "chmod +x ~/.carbon-check" << "\n";
  
  VerifyUnixCommand* cmd1 = new VerifyUnixCommand();
  cmd1->putCommand(shellCmd.c_str());
  mVerifyCommands.push_back(cmd1);
 
  QStringList args;
  args << ui.lineEditUnixPath->text();

  VerifyUnixDirectory* cmd2 = new VerifyUnixDirectory();
  cmd2->putTitle("Checking Unix Directory: " + ui.lineEditUnixPath->text());
  cmd2->putCommand("~/.carbon-check");
  cmd2->putArgs(args);

  mVerifyCommands.push_back(cmd2);
}


