//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"
#include "util/UtHashSet.h"

#include "util/CExprParse.h"

#include "Validators.h"
#include "cmm.h"
#include "CcfgHelper.h"


void ESLPortExprValidator::fixup(QString& input) const
{
  qDebug() << "fixup" << input;
}

QValidator::State ESLPortExprValidator::validate(QString& input, int& pos) const
{
  qDebug() << "validate" << input << pos;

  UtString expr;
  expr << input;
  UtStringSet idents;
  if (carbonCExprValidate(expr.c_str(), &idents))
  {
    bool badIdents = false;
    for (UtStringSet::SortedLoop p = idents.loopSorted(); !p.atEnd(); ++p) 
    {
      const UtString& ident = *p;
      if (mIdentifier != ident.c_str())
      {
        qDebug() << "bad identifier" << ident.c_str();
        badIdents = true;
        break;
      }
    }
    if (badIdents)
    {
      QPalette palette = mLineEdit->palette();
      palette.setColor(QPalette::Text, Qt::red);
      mLineEdit->setPalette(palette);
      qDebug() << "Intermediate";
      return QValidator::Intermediate;
    }
    else
    {
      QPalette palette = mLineEdit->palette();
      palette.setColor(QPalette::Text, mLineEdit->foregroundRole());
      mLineEdit->setPalette(palette);
      qDebug() << "Acceptable";  
      return QValidator::Acceptable;
    }
  }

  QPalette palette = mLineEdit->palette();
  palette.setColor(QPalette::Text, Qt::red);
  mLineEdit->setPalette(palette);

  qDebug() << "Intermediate";
  return QValidator::Intermediate;
}


bool HexInputValidator64::isValid(const QString& input)
{
  bool ok;
  quint64 value;
  // Try hex?
  if (input.toLower().startsWith("0x"))
    value = input.toULongLong(&ok, 16);
  else
    value = input.toULongLong(&ok, 10);

  if (!ok) // last ditch, try hex without the prefix
    value = input.toULongLong(&ok, 16);

  qDebug() << "converted" << input << "to" << value;

  return ok;
}

quint64 HexInputValidator64::convert(const QString& input)
{
  bool ok;
  quint64 value = 0;

  // Try hex?
  if (input.toLower().startsWith("0x"))
    value = input.toULongLong(&ok, 16);
  else
    value = input.toULongLong(&ok, 10);

  if (!ok) // last ditch, try hex without the prefix
    value = input.toULongLong(&ok, 16);

  return value;
}
HexInputValidator64::HexInputValidator64(QLineEdit* parent) : QValidator(parent)
{
  mLineEdit = parent;
}

void HexInputValidator64::fixup(QString&) const
{
}

QValidator::State HexInputValidator64::validate(QString& input, int&) const
{
  QValidator::State state;

  if (isValid(input))
    state = QValidator::Acceptable;
  else if (input.toLower() == "0x")
    state = QValidator::Intermediate;
  else
    state = QValidator::Invalid;

  if (state != QValidator::Acceptable)
  {
    QPalette palette = mLineEdit->palette();
    palette.setColor(QPalette::Text, Qt::red);
    mLineEdit->setPalette(palette);
  }
  else
  {
    QPalette palette = mLineEdit->palette();
    palette.setColor(QPalette::Text, mLineEdit->foregroundRole());
    mLineEdit->setPalette(palette);
  }

  return state;
}

// 32
bool HexInputValidator32::isValid(const QString& input)
{
  bool ok;
  quint32 value;
  // Try hex?
  if (input.toLower().startsWith("0x"))
    value = input.toULongLong(&ok, 16);
  else
    value = input.toULongLong(&ok, 10);

  if (!ok) // last ditch, try hex without the prefix
    value = input.toULongLong(&ok, 16);

  qDebug() << "converted" << input << "to" << value;

  return ok;
}

quint32 HexInputValidator32::convert(const QString& input)
{
  bool ok;
  quint32 value = 0;

  // Try hex?
  if (input.toLower().startsWith("0x"))
    value = input.toULongLong(&ok, 16);
  else
    value = input.toULongLong(&ok, 10);

  if (!ok) // last ditch, try hex without the prefix
    value = input.toULongLong(&ok, 16);

  return value;
}
HexInputValidator32::HexInputValidator32(QLineEdit* parent) : QValidator(parent)
{
  mLineEdit = parent;
}

void HexInputValidator32::fixup(QString&) const
{
}

QValidator::State HexInputValidator32::validate(QString& input, int&) const
{
  QValidator::State state;

  if (isValid(input))
    state = QValidator::Acceptable;
  else if (input.toLower() == "0x")
    state = QValidator::Intermediate;
  else
    state = QValidator::Invalid;

  if (state != QValidator::Acceptable)
  {
    QPalette palette = mLineEdit->palette();
    palette.setColor(QPalette::Text, Qt::red);
    mLineEdit->setPalette(palette);
  }
  else
  {
    QPalette palette = mLineEdit->palette();
    palette.setColor(QPalette::Text, mLineEdit->foregroundRole());
    mLineEdit->setPalette(palette);
  }

  return state;
}

CExpressionValidator::CExpressionValidator(const QString& value, QLineEdit* parent) : QValidator(parent)
{
  mValue = value;
  mLineEdit = parent;
}

void CExpressionValidator::fixup(QString&) const
{
}

QValidator::State CExpressionValidator::validate(QString& input, int&) const
{
  UtString expr;
  expr << input;
  UtStringSet idents;
  
  UtString value;
  value << mValue;
  idents.insert(value.c_str());

  if (!carbonCExprValidate(expr.c_str(), &idents))
  {
    bool badIdents = false;
    QString tooltip;
    for (UtStringSet::SortedLoop p = idents.loopSorted(); !p.atEnd(); ++p) 
    {
      const UtString& ident = *p;
      if (mValue != ident.c_str() && ident != "_")
        badIdents = true;
      else
      {
        if (tooltip.isEmpty())
          tooltip = ident.c_str();
        else
          tooltip += QString(", %1").arg(ident.c_str());
      }
    }
    if (badIdents)
    {
      QPalette palette = mLineEdit->palette();
      palette.setColor(QPalette::Text, Qt::red);
      mLineEdit->setToolTip(tooltip);
      mLineEdit->setPalette(palette);
      return QValidator::Intermediate;
    }
    else
    {
      mLineEdit->setToolTip("");
      QPalette palette = mLineEdit->palette();
      palette.setColor(QPalette::Text, mLineEdit->foregroundRole());
      mLineEdit->setPalette(palette);
      return QValidator::Acceptable;
    }
  }
  else
    return QValidator::Acceptable;

  //QPalette palette = mLineEdit->palette();
  //palette.setColor(QPalette::Text, Qt::red);
  //mLineEdit->setPalette(palette);
}

bool CExpressionValidator::isValid(const QString& input)
{
  if (input.isEmpty())
    return false;

  return true;
}


XtorInstanceNameValidator::XtorInstanceNameValidator(const QString& startName, QLineEdit* parent, CarbonCfg* cfg) : QValidator(parent)
{
  mName = startName;
  mLineEdit = parent;
  mCfg = cfg;
}

void XtorInstanceNameValidator::fixup(QString&) const
{
}

QValidator::State XtorInstanceNameValidator::validate(QString& input, int&) const
{
  State state = QValidator::Intermediate;

  if (input == mName || isValid(mCfg, input))
    state = QValidator::Acceptable;

  if (state != QValidator::Acceptable)
  {
    QPalette palette = mLineEdit->palette();
    palette.setColor(QPalette::Text, Qt::red);
    mLineEdit->setPalette(palette);
  }
  else
  {
    QPalette palette = mLineEdit->palette();
    palette.setColor(QPalette::Text, mLineEdit->foregroundRole());
    mLineEdit->setPalette(palette);
  }

  return state;
}

bool XtorInstanceNameValidator::isValid(CarbonCfg* cfg, const QString& input)
{
  UtString v; v << input;
  if (cfg->findXtorInstance(v.c_str()))
    return false;
  else
    return true;
}

// Param
ParameterNameValidator::ParameterNameValidator(QLineEdit* parent, CarbonCfg* cfg) : QValidator(parent)
{
  mLineEdit = parent;
  mCfg = cfg;
}

void ParameterNameValidator::fixup(QString&) const
{
}

QValidator::State ParameterNameValidator::validate(QString& input, int&) const
{
  State state = QValidator::Intermediate;

  if (isValid(mCfg, input))
    state = QValidator::Acceptable;

  if (state != QValidator::Acceptable)
  {
    QPalette palette = mLineEdit->palette();
    palette.setColor(QPalette::Text, Qt::red);
    mLineEdit->setPalette(palette);
  }
  else
  {
    QPalette palette = mLineEdit->palette();
    palette.setColor(QPalette::Text, mLineEdit->foregroundRole());
    mLineEdit->setPalette(palette);
  }

  return state;
}

bool ParameterNameValidator::isValid(CarbonCfg* cfg, const QString& input)
{
  CcfgHelper ccfg(cfg);
  if (input.isEmpty() || ccfg.findParameter(input))
    return false;
  else
    return true;
}


