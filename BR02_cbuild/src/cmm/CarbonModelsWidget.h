// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _CarbonModelsWidget_h_
#define _CarbonModelsWidget_h_

#include "util/CarbonPlatform.h"
#include "gui/CDragDropTreeWidget.h"
#include "shell/ReplaySystem.h"
#include "CarbonOptions.h"
#include "CarbonInstanceOptions.h"
#include "Mdi.h"

#include <QtGui>

struct CarbonReplaySystem;
class SettingsEditor;
class CarbonInstanceOptions;
class CQtWizardContext;
class CarbonMakerContext;
class SimulationHistoryWidget;
class OnDemandTraceWidget;

#define CCONTROL_ROOT_COMP 0
#if CCONTROL_ROOT_COMP
class CarbonAllComponentsItem;
#endif

class CarbonModelsWidget : public QTreeWidget, public MDIWidget
{
  Q_OBJECT

public:
  enum Columns {colNAME=0, colONDEMAND, colREPLAY, colLAST };

  CarbonModelsWidget(CarbonMakerContext *ctx, CarbonRuntimeState *state, QWidget *parent = 0);
  virtual ~CarbonModelsWidget();
  virtual void closeEvent(QCloseEvent *ev);

  CarbonRuntimeState *getRuntimeState() { return mRuntimeState; }

  CarbonInstanceOptions* getCurrentInstanceOptions() {
    return mCurrentInstanceOptions;
  }
  CarbonOptions* getCurrentOptions() {
    return mCurrentInstanceOptions ? mCurrentInstanceOptions->getOptions() : 0;
  }

  bool selectComponent(const char* compName);
  CarbonOptions* getOptionsForComponent(const char* compName);

  void removeOnDemandExclusions(const QStringList& stateList, const QStringList& inputList);
  /*
  bool loadCarbonSystem(const char *fname);
  //  void putSelectedModels(UtStringArray &selection);

  void replayVerbose(bool enabled);
  void replayDatabase(const char *db);
  void replayRecord();
  void replayPlayback();
  void replayStop();
  void replayCheckpointInterval(UInt32 interval);
  void replayRecoverPercentage(UInt32 percent);

  void onDemandStart();
  void onDemandStop();
  void onDemandTrace(bool enable);
  void onDemandMaxStates(UInt32 states);
  void onDemandBackoffStates(UInt32 states);
  void onDemandBackoffDecayPercent(UInt32 percent);
  void onDemandBackoffMaxDecay(UInt32 maxDecay);
  void onDemandBackoffStrategy(CarbonOnDemandBackoffStrategy strategy);

  void updateReplayToolbar();
  void updateOnDemandToolbar();
  void showOnDemandTrace(bool show);
  */


  /*
signals:
  void carbonSystemUpdated(CarbonReplaySystem &guiSystem, CarbonReplaySystem &simSystem);
  */

public slots:
  void carbonSystemUpdated(CarbonRuntimeState &state, CarbonReplaySystem &guiSystem, CarbonReplaySystem &simSystem);


private slots:
  virtual void itemSelectionChanged();
  void itemClicked(QTreeWidgetItem*,int);

private:
  // set the window title to reflect the current file
  void setCurrentFile(const char *fname);

  bool save();
  bool saveAs();
  bool saveFile();

  void removeComponentOnDemandExclusions(CarbonOptions* options, const char* propName, const QStringList& eraseList);

  CQtWizardContext   *mCQtContext;
  QTimer             *mPollTimer;
  SimulationHistoryWidget *mSimulationHistory;
  OnDemandTraceWidget *mOnDemandTrace;
  bool                mCyclesSpecified;
  bool                mSimRequestActive;
  bool                mUpdating;
  QWidget            *mContinueSim;
  UtStringArray       mSelectedModels;

  friend class CarbonModelDelegate;

private:
  CarbonMakerContext     *mContext;
  CarbonRuntimeState     *mRuntimeState;  
  CarbonInstanceOptions  *mCurrentInstanceOptions;
#if CCONTROL_ROOT_COMP
  CarbonAllComponentsItem* mCarbonAllComponentsItem;
#endif
}; // class CarbonModelsWidget : public QTreeWidget, public MDIWidget

#if CCONTROL_ROOT_COMP
class CarbonAllComponentsItem : public QTreeWidgetItem {
public:
  CarbonAllComponentsItem(CarbonModelsWidget*, CarbonInstanceOptions*);

  CarbonInstanceOptions* getInstanceOptions() {return mInstanceOptions;}

private:
  CarbonModelsWidget* mCarbonModelsWidget;
  CarbonInstanceOptions  *mInstanceOptions;
};
#endif

class CarbonModelDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    CarbonModelDelegate(QObject *parent = 0, CarbonModelsWidget* models =0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
        const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
        const QModelIndex &index) const;

private slots:
  void currentIndexChanged(int);
  void commitAndCloseEditor();

private:
  CarbonModelsWidget* modelsWidget;
};

class CarbonModelInstanceItem: public QTreeWidgetItem
{
public:

  CarbonModelInstanceItem(CarbonModelsWidget* parent, CarbonReplaySystem& guiSystem, CarbonReplaySystem& simSystem, const char* compName);
  virtual ~CarbonModelInstanceItem();

  void putPropertyValues(CarbonReplaySystem &guiSystem, CarbonReplaySystem &simSystem, const char *compName);
  CarbonOptions *getOptions() { return mInstanceOptions->getOptions(); }
  CarbonInstanceOptions *getInstanceOptions() { return mInstanceOptions; }

  QString getReplayIcon(CarbonVHMMode mode);
  QString getOnDemandIcon(CarbonOnDemandMode mode);

private:
  CarbonInstanceOptions *mInstanceOptions;
  CarbonReplaySystem& mGuiSystem;
  CarbonReplaySystem& mSimSystem;
  UtString mComponentName;
};

#endif

