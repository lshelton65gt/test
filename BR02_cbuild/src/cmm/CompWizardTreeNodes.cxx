//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"
#include "util/UtHashSet.h"

#include "util/CExprParse.h"

#include "CompWizardPortEditor.h"
#include "CompWizardTreeNodes.h"
#include "PortEditorCommands.h"
#include "Validators.h"
#include "cmm.h"
#include "CcfgHelper.h"
#include "DlgSelectRTL.h"
#include "ESLPort.h"

// static initializer
ESLParameterActions* ESLParameterItem::mActions = NULL;

ESLParameterActions::ESLParameterActions(PortEditorTreeWidget* tree) : QObject(tree)
{
  mTree = tree;

  mConnectRTL = new QAction("Connect to RTL...", mTree);
  CQT_CONNECT(mConnectRTL, triggered(), this, actionConnectRTL());
}

QAction* ESLParameterActions::getAction(Action kind)
{
  switch (kind)
  {
  case ConnectRTL:  return mConnectRTL; break;
  }
  return mConnectRTL;
}


ESLParameterItem::ConnectRTL::ConnectRTL(ESLParameterItem* item,  
                                         const QStringList& rtlPorts, QUndoCommand* parent)
                                         : TreeUndoCommand(item, parent)
{
  CarbonCfgXtorParamInst* paramInst = item->getParamInst();
  mRTLPorts = rtlPorts;
  mCfg = item->getCcfg();

  setText(QString("Connect %1 RTL Port(s) to %2")
    .arg(rtlPorts.count())
    .arg(paramInst->getParam()->getName()));

}

void ESLParameterItem::ConnectRTL::undo()
{
  CcfgHelper ccfg(mCfg);
  ESLParameterItem* item = dynamic_cast<ESLParameterItem*>(getItem());
  CarbonCfgXtorParamInst* paramInst = item->getParamInst();
  qDebug() << "Undo Connect Global Parameter" << paramInst->getParam()->getName() << "to" << mRTLPorts;

  // Disconnect the RTL
  foreach (QString rtlPortName, mRTLPorts)
  {
    CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(rtlPortName);
    paramInst->removeRTLPort(rtlPort);
    
   // Break the connection from the RTL port to this Parameter
    for (UInt32 i=0; i<rtlPort->numConnections(); i++)
    {
      CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
      CarbonCfgTieParam* tiep = conn->castTieParam();
      if (tiep && tiep->mParam == paramInst->getParam()->getName())
        ccfg.disconnect(tiep);
    }

    // Re-create the ESL Ports
    foreach(QString eslPortName, mRemovedESLPorts[rtlPort->getName()])
    {
      qDebug() << "re-creating eslport" << eslPortName << "for" << rtlPort->getName();
      ESLPortItem::makeESLPort(mCfg, rtlPort, getTree(), eslPortName);
    }

  } 
  // Remove the Items
  foreach(PortTreeIndex index, mPortIndexes)
  {
    ESLParameterPortItem* oldItem = dynamic_cast<ESLParameterPortItem*>(getTree()->itemFromIndex(index));
    INFO_ASSERT(oldItem, "Expecting item");
    getTree()->removeItem(oldItem);
  }

  
  getTree()->scrollToItem(item);
  getTree()->setModified(false);
}

void ESLParameterItem::ConnectRTL::redo()
{
  CcfgHelper ccfg(mCfg);
  ESLParameterItem* item = dynamic_cast<ESLParameterItem*>(getItem());
  CarbonCfgXtorParamInst* paramInst = item->getParamInst();
  qDebug() << "Connect Global Parameter" << paramInst->getParam()->getName() << "to" << mRTLPorts;

  mPortIndexes.clear();
  mRemovedESLPorts.clear();

  // Tie the RTL to the Parameter
  foreach (QString rtlPortName, mRTLPorts)
  {
    CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(rtlPortName);

    mRemovedESLPorts[rtlPort->getName()].clear();
    if (rtlPort->isESLPort())
    {
      // Remove all ESL ports associated with this RTL port
      for (UInt32 i=0; i<rtlPort->numConnections(); i++)
      {
        CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
        if (conn->castESLPort())
        {
          CarbonCfgESLPort* eslPort = conn->castESLPort();
          QString eslPortName = eslPort->getName();

          mRemovedESLPorts[rtlPort->getName()].append(eslPortName);
          // Find the ESL Port
          ESLPortItem* eslItem = getTree()->findESLPort(getTree()->getEditor()->getPortsRoot(), eslPortName);
          INFO_ASSERT(eslItem, "Expected ESL Port");
          ESLPortItem::removeESLPort(eslItem, eslPortName);
        }
      }
    }
    ccfg.disconnect(rtlPort);

    paramInst->addRTLPort(rtlPort);
    carbonCfgTieParam(rtlPort, "", paramInst->getParam()->getName());

    ESLParameterPortItem* newItem = new ESLParameterPortItem(item, paramInst, rtlPort);
    newItem->setSelected(true);
    PortTreeIndex index = getTree()->indexFromItem(newItem);
    mPortIndexes.append(index);
  }

  getTree()->scrollToItem(item);
  getTree()->sortChildren(item);
  item->setExpanded(true);
  getTree()->setModified(true);
}


void ESLParameterActions::actionConnectRTL()
{
  qDebug() << "Connect RTL";

  DlgSelectRTL dlg(mTree->getCcfg(), DlgSelectRTL::InputPorts, false, mTree);
  if (dlg.exec())
  {
    QList<PortEditorTreeItem*> items;
    mTree->fillSelectedItems(items, PortEditorTreeItem::GlobalParameter);
    QStringList rtlPorts = dlg.getPorts();
    ESLParameterItem* item = dynamic_cast<ESLParameterItem*>(items.first());
    mTree->getUndoStack()->push(new ESLParameterItem::ConnectRTL(item, rtlPorts));
  }
}

// Create a new tie and connect it
// to the RTL port
ESLTieItem* ESLTieItem::createTie(PortEditorTreeWidget* tree, UndoData* undoData, 
                                  CarbonCfgRTLPort* rtlPort, UInt32 tieValue)
{
  // Make a tie for this rtlPort
  CarbonCfgRTLConnection* tieConn = carbonCfgTie(rtlPort, &tieValue, 1); 
  QTreeWidgetItem* tiesRoot = tree->getEditor()->getTiesRoot();
  ESLTieItem* newItem = new ESLTieItem(tiesRoot, tieConn->castTie());
  newItem->setSelected(true);
  undoData->setTreeData(tree, newItem);  
  tiesRoot->setExpanded(true);
  tree->scrollToItem(newItem);

  return newItem;
}


CarbonCfgRTLPort* ESLTieItem::removeTie(PortEditorTreeItem *item, const QString& rtlPortName)
{
  CcfgHelper ccfg(item->getCcfg());
  PortEditorTreeWidget* tree = item->getTree();
  tree->removeItem(item);

  CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(rtlPortName);
 // Break the connection from the RTL port to this tie
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgTie* tie = conn->castTie();
    if (tie)
      ccfg.disconnect(tie);
  }

  return rtlPort;
}


// Global Parameters
void ESLParameterItem::multiSelectContextMenu(QMenu& menu)
{
  QAction* actionConnectRTL = getActions()->getAction(ESLParameterActions::ConnectRTL);

  QList<PortEditorTreeItem*> items;
  getTree()->fillSelectedItems(items, PortEditorTreeItem::GlobalParameter);
  
  if (items.count() == 1)
    menu.addAction(actionConnectRTL);
}

UndoData* ESLParameterItem::deleteItem()
{
  ESLParameterItem::Delete* undoData = new ESLParameterItem::Delete();
  PortEditorTreeWidget* tree = dynamic_cast<PortEditorTreeWidget*>(treeWidget());

  undoData->mCfg = getCcfg();
  undoData->setTreeData(tree, this);

  CarbonCfgXtorParam* param = mParamInst->getParam();

  undoData->setText(param->getName());
  undoData->mParamName = param->getName();
  undoData->mParamValue = mParamInst->getValue();
  undoData->mParamSize = param->getSize();
  undoData->mParamType = param->getType();
  undoData->mParamFlag = param->getFlag();
  undoData->mParamDescription = param->getDescription();
  undoData->mParamEnumChoices = param->getEnumChoices();
  undoData->mDefaultValue = param->getDefaultValue();
  undoData->mExpanded = isExpanded();

// find all the ESL ports connected to this parameter instance
  for (UInt32 i=0; i<getCcfg()->numESLPorts(); i++)
  {
    CarbonCfgESLPort* eslPort = getCcfg()->getESLPort(i);
    if (eslPort->getParamInstance() == mParamInst)
    {
      ESLPortItem* eslPortItem = 
          getTree()->findESLPort(tree->getEditor()->getPortsRoot(), eslPort->getName());
      INFO_ASSERT(eslPortItem, "Expected to find item");
      undoData->mESLPorts.append(tree->indexFromItem(eslPortItem));
    }
  }
 
  // Save the connections
  for (UInt32 i=0; i<mParamInst->numRTLPorts(); i++)
  {
    ESLParameterItem::DeleteConn* undoConn = new ESLParameterItem::DeleteConn();
    CarbonCfgRTLPort* rtlPort = mParamInst->getRTLPort(i);
    undoConn->mCfg = getCcfg();
    undoConn->setTreeData(tree, this);
    undoConn->setText(rtlPort->getName());
    undoConn->mRTLPortName = rtlPort->getName();
    undoConn->mParamName = undoData->mParamName;
    undoData->mConnections.append(undoConn);
  }

  return undoData;
}

void ESLParameterItem::Delete::redo()
{
  qDebug() << "Delete Global Parameter" << text();
  CcfgHelper ccfg(mCfg);

  // Locate parameter
  CarbonCfgXtorParamInst* paramInst = ccfg.findParameter(mParamName);
  INFO_ASSERT(paramInst, "Expecting to find parameter");
  
  // Now delegate to each connection
  foreach(DeleteConn* undoConn, mConnections)
    undoConn->redo();

  foreach(PortTreeIndex index, mESLPorts)
  {
    ESLPortItem* eslItem = dynamic_cast<ESLPortItem*>(getTree()->itemFromIndex(index));
    INFO_ASSERT(eslItem, "Expected Item");
    CarbonCfgESLPort* eslPort = eslItem->getESLPort();
    eslPort->putParamInstance(NULL);
    eslItem->setParam(eslPort);
  }

  // Now remove the Parameter
  mCfg->removeParam(paramInst);

  getTree()->removeItem(getItem());
  
  getTree()->setModified(true);
}

void ESLParameterItem::Delete::undo()
{
  qDebug() << "Undo Global Parameter Delete" << text();

  getTree()->setUpdatesEnabled(false);

  // First, create the parameter
  UtString uParamName; uParamName << mParamName;
  UtString uParamValue; uParamValue << mParamValue;
  UtString uParamDescr; uParamDescr << mParamDescription;
  UtString uParamEnumChoices; uParamEnumChoices << mParamEnumChoices;
  CarbonCfgXtorParamInst* paramInst = mCfg->addParam(uParamName.c_str(),
         mParamType, uParamValue.c_str(), mParamFlag, uParamDescr.c_str(), uParamEnumChoices.c_str());

  // Now delegate to each connection
  foreach(DeleteConn* undoConn, mConnections)
    undoConn->undo();

  QTreeWidgetItem* paramsRoot = getTree()->getEditor()->getParamsRoot();

  foreach(PortTreeIndex index, mESLPorts)
  {
    ESLPortItem* eslItem = dynamic_cast<ESLPortItem*>(getTree()->itemFromIndex(index));
    INFO_ASSERT(eslItem, "Expected Item");
    CarbonCfgESLPort* eslPort = eslItem->getESLPort();
    eslPort->putParamInstance(paramInst);
    eslItem->setParam(eslPort);
  }

  ESLParameterItem* newItem = new ESLParameterItem(paramsRoot, paramInst);
  setTreeData(getTree(), newItem);

  // Re-Sort the items
  getTree()->sortChildren(paramsRoot);

  // Restore expansion
  getTree()->setExpanded(newItem, mExpanded);
 
  // select and ensure visibible
  newItem->setSelected(true);
  getTree()->scrollToItem(newItem);

  getTree()->setUpdatesEnabled(true);
  getTree()->setModified(false);
}

void ESLParameterItem::DeleteConn::redo()
{
  qDebug() << "Delete Global Parameter Connection" << text();

  // Find the RTL Port
  UtString uRTLPortName; uRTLPortName << mRTLPortName;

  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgTieParam* tie = conn->castTieParam();
    if (tie && mParamName == tie->mParam.c_str())
      mCfg->disconnect(tie);
  }

  // If the RTL port is now disconnected, make it an ESL port again
  if (rtlPort->isDisconnected())
    ESLPortItem::makeESLPort(mCfg, rtlPort, getTree(), mESLPortName, mESLPortIndex);
}

void ESLParameterItem::DeleteConn::undo()
{
  qDebug() << "Undo Global Parameter Connection Delete" << text();

 // Find the RTL Port
  UtString uRTLPortName; uRTLPortName << mRTLPortName;

  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());

  // If we had made it an ESL port, remove it first
  if (!mESLPortIndex.isEmpty())
  {
    PortEditorTreeItem* eslItem 
      = dynamic_cast<PortEditorTreeItem*>(getTree()->itemFromIndex(mESLPortIndex));
    INFO_ASSERT(eslItem, "Expected to find item");
    ESLPortItem::removeESLPort(eslItem, mESLPortName);
  }


  CarbonCfgXtorParamInst* paramInst = NULL;
  for (UInt32 i=0; i<mCfg->numParams(); i++)
  {
    CarbonCfgXtorParamInst* pi = mCfg->getParam(i);
    if (mParamName == pi->getParam()->getName())
    {
      paramInst = pi;
      break;
    }
  }
  INFO_ASSERT(paramInst, "Expecting to find parameter");  
  
  // Connect the Parameter to the RTL Port and
  paramInst->addRTLPort(rtlPort);

  UtString uParamName; uParamName << mParamName;
  // Connect the RTL port to the parameter
  carbonCfgTieParam(rtlPort, "", uParamName.c_str());
}


// ESLTieValueItem

QWidget* ESLTieValueItem::createEditor(const PortEditorDelegate* , QWidget* parent, int colIndex)
{
  ESLTieValueItem::Columns col = ESLTieValueItem::Columns(colIndex);
  if (col == ESLTieValueItem::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    lineEdit->setValidator(new HexInputValidator64(lineEdit));
    return lineEdit;
  }

  return NULL;
}

void ESLTieValueItem::setModelData(const PortEditorDelegate* , 
                                   QAbstractItemModel* model, const QModelIndex &modelIndex,
                                   QWidget* editor, int colIndex)
{
  ESLTieValueItem::Columns col = ESLTieValueItem::Columns(colIndex);

  if (col == ESLTieValueItem::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldValue = model->data(modelIndex).toString();
      QString newValue = lineEdit->text();
      // Verify this is a number before allowing
      bool ok;
      quint64 value;
      // Try hex?
      if (newValue.toLower().startsWith("0x"))
        value = newValue.toULongLong(&ok, 16);
      else
        value = newValue.toULongLong(&ok, 10);

      if (!ok) // last ditch, try hex without the prefix
        value = newValue.toULongLong(&ok, 16);

      DynBitVector newBv(mTie->getRTLPort()->getWidth());
      if (ok)
      {
        QString hexValue = QString("%1").arg(value,0,16);
        UtString buf;
        buf << hexValue;
        UtIStringStream ss(buf);
        if (ss >> UtIO::hex >> newBv) 
          ok = true;
        else
          ok = false;
      }

      if (ok)
      {
        QString hexValue = QString("0x%1").arg(value,0,16);

        const DynBitVector* oldBv = &mTie->mValue;

        model->setData(modelIndex, hexValue);
        getUndoStack()->push(new CmdChangeESLTieValue(this, getCcfg(), mTie, oldBv, &newBv));     
      }
    }
  }
}

UndoData* ESLDisconnectItem::deleteItem()
{
  ESLDisconnectItem::Delete* undoData = new ESLDisconnectItem::Delete();
  
  undoData->mCfg = getCcfg();
  undoData->setText(mRTLPort->getName());
  undoData->mRTLPortName = mRTLPort->getName();
  undoData->setTreeData(treeWidget(), this);
  
  return undoData;
}

void ESLDisconnectItem::Delete::undo()
{
  qDebug() << "Undo Disconnect Delete" << text();

  UtString uRTLPortName;
  uRTLPortName << mRTLPortName;

  UtString uESLPortName;
  uESLPortName << mESLPortName;

  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  INFO_ASSERT(rtlPort, "Expecting RTL Port");

  CarbonCfgESLPort* eslPort = mCfg->findESLPort(uESLPortName.c_str());
  INFO_ASSERT(eslPort, "Expecting ESL Port");

  mCfg->disconnect(eslPort);

  QTreeWidgetItem* disItem = getTree()->itemFromIndex(mESLPortIndex);
  QTreeWidgetItem* parent = disItem->parent();
  parent->removeChild(disItem);

  PortEditorTreeWidget* tree = getTree();
  CompWizardPortEditor* editor = tree->getEditor();

  ESLDisconnectItem* newItem = new ESLDisconnectItem(editor->getDisconnectsRoot(), rtlPort);
  newItem->setSelected(true);
  getTree()->scrollToItem(newItem);
  setTreeData(getTree(), newItem);

  getTree()->setModified(false);
}

void ESLDisconnectItem::Delete::redo()
{
  qDebug() << "Delete Disconnect" << text();

  UtString uRTLPortName;
  uRTLPortName << mRTLPortName;

  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());

  // Remove this disconnect
  getTree()->removeItem(getItem());

  // Turn it into an ESL Port
  ESLPortItem* newESLItem = ESLPortItem::makeESLPort(mCfg, rtlPort, getTree(), mESLPortName, mESLPortIndex);  
  newESLItem->setSelected(true);
  getTree()->scrollToItem(newESLItem);
  getTree()->setModified(true);
}

UndoData* ESLTieItem::deleteItem()
{
  ESLTieItem::Delete* undoData = new ESLTieItem::Delete();
  
  undoData->setText(mTie->getRTLPort()->getName());
  undoData->mCfg = getCcfg();
  undoData->mRTLPortName = mTie->getRTLPort()->getName();
  undoData->mTieValue = mTie->mValue;
  undoData->setTreeData(treeWidget(), this);
  
  return undoData;
}

void ESLTieItem::Delete::undo()
{
  qDebug() << "Undo Tie Delete" << text();

  // Destroy the ESL Port
  UtString uESLPortName; uESLPortName << mESLPortName;
  CarbonCfgESLPort* eslPort = mCfg->findESLPort(uESLPortName.c_str());
  INFO_ASSERT(eslPort, "Expecting ESL Port");
  mCfg->disconnect(eslPort);

  QTreeWidgetItem* eslItem = getTree()->itemFromIndex(mESLPortIndex);
  INFO_ASSERT(eslItem, "Expecting ESL Port Item");
  getTree()->removeItem(eslItem);

  // Re-build the Tie
  UtString uRTLPortName; uRTLPortName << mRTLPortName;
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  CarbonCfgTie* tie = new CarbonCfgTie(rtlPort, mTieValue);
  rtlPort->connect(tie); 

  QTreeWidgetItem* tiesRoot = getTree()->getEditor()->getTiesRoot();
  ESLTieItem* newItem = new ESLTieItem(tiesRoot, tie);
  newItem->setSelected(true);
  setTreeData(getTree(), newItem);  
  getTree()->scrollToItem(newItem);

  getTree()->setModified(false);
}

void ESLTieItem::Delete::redo()
{
  qDebug() << "Delete Tie" << text();

  UtString uRTLPortName;
  uRTLPortName << mRTLPortName;

  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());

  // Break the connection from the RTL port to this tie
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgTie* tie = conn->castTie();
    if (tie)
      mCfg->disconnect(tie);
  }

  getTree()->removeItem(getItem());

 // Turn it into an ESL Port
  ESLPortItem* newESLItem = ESLPortItem::makeESLPort(mCfg, rtlPort, getTree(), mESLPortName, mESLPortIndex);  
  getTree()->scrollToItem(newESLItem);
  
  getTree()->setModified(true);
}



// ESLParameterPortItem (Component Parameters) tied to RTL Ports

UndoData* ESLParameterPortItem::deleteItem()
{
  // If the parent is selected, this will get
  // deleted anyhow.
  QTreeWidgetItem* parentItem = PortEditorTreeItem::parent();
  if (!parentItem->isSelected())
  {
    ESLParameterPortItem::Delete* undoData = new ESLParameterPortItem::Delete();
    undoData->setText(mRTLPort->getName());
    undoData->mCfg = getCcfg();
    undoData->mParamName = mParamInst->getParam()->getName();
    undoData->mRTLPortName = mRTLPort->getName();
    undoData->setTreeData(treeWidget(), this);
    
    return undoData;
  }

  return NULL;
}

void ESLParameterPortItem::Delete::undo()
{
  qDebug() << "Undo Parameter Port Item Delete" << text();

  UtString uRTLPortName;
  uRTLPortName << mRTLPortName;

  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());

 // Remove the ESL port we had created before
  if (!mESLPortIndex.isEmpty())
  {
    PortEditorTreeItem* eslItem 
      = dynamic_cast<PortEditorTreeItem*>(getTree()->itemFromIndex(mESLPortIndex));
    INFO_ASSERT(eslItem, "Expected to find item");
    ESLPortItem::removeESLPort(eslItem, mESLPortName);
  }

  // Locate parameter
  CarbonCfgXtorParamInst* paramInst = NULL;
  for (UInt32 i=0; i<mCfg->numParams(); i++)
  {
    CarbonCfgXtorParamInst* pi = mCfg->getParam(i);
    if (mParamName == pi->getParam()->getName())
    {
      paramInst = pi;
      break;
    }
  }
  INFO_ASSERT(paramInst, "Expecting to find parameter");

  // Create the tie back to this parameter
  UtString uParamName; uParamName << mParamName;
  CarbonCfgTieParam* tie = new CarbonCfgTieParam(rtlPort, "", uParamName.c_str());
  rtlPort->connect(tie);

  QTreeWidgetItem* parentItem = getParentItem();
  INFO_ASSERT(parentItem, "Expecting to find Parent");

  ESLParameterPortItem* newItem = new ESLParameterPortItem(parentItem, paramInst, rtlPort);
  setTreeData(getTree(), newItem);
  newItem->setSelected(true);
  getTree()->scrollToItem(newItem);

  parentItem->setExpanded(true);

  getTree()->setModified(false);
}

void ESLParameterPortItem::Delete::redo()
{
  qDebug() << "Delete Parameter Port Item" << text();

  UtString uRTLPortName;
  uRTLPortName << mRTLPortName;

  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());

  // Find the tie to this parameter
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgTieParam* tie = conn->castTieParam();
    if (tie && tie->mParam.c_str() == mParamName)
      mCfg->disconnect(tie);
  }

  mESLPortIndex.clear();
  mESLPortName.clear();

  // re-create ESL Port?
  if (rtlPort->numConnections() == 0)
  {
    qDebug() << "Creating an ESL Port";
    ESLPortItem::makeESLPort(mCfg, rtlPort, getTree(), mESLPortName, mESLPortIndex);
  }

  // Remove the node
  QTreeWidgetItem* item = getItem();
  QTreeWidgetItem* parentItem = getParentItem();

  INFO_ASSERT(parentItem && item, "Expecting items");

  parentItem->removeChild(item);

  getTree()->setModified(true);
}


// ESLPortExprItem
void ESLPortExprItem::Delete::undo()
{  
  qDebug() << "Undo Port Expression Delete" << text();

  CcfgHelper ccfg(mCfg);

  UtString uPortExpr;
  uPortExpr << mPortExpr;

  CarbonCfgESLPort* eslPort = ccfg.findESLPort(mESLPortName);
  eslPort->putExpr(uPortExpr.c_str());

  ESLPortItem* portItem = dynamic_cast<ESLPortItem*>(getParentItem());
  QTreeWidgetItem* newItem = portItem->setPortExpr(mPortExpr);
  newItem->setSelected(true);
  getTree()->scrollToItem(newItem);

  portItem->setExpanded(true);

  getTree()->setModified(false);
}

void ESLPortExprItem::Delete::redo()
{
  qDebug() << "Delete Port Expression" << text();
  CcfgHelper ccfg(mCfg);

  CarbonCfgESLPort* eslPort = ccfg.findESLPort(mESLPortName);
  if (eslPort)
    eslPort->putExpr("");

  ESLPortItem* portItem = dynamic_cast<ESLPortItem*>(getParentItem());
  portItem->setPortExpr("");

  getTree()->setModified(true);
}


UndoData* ESLPortExprItem::deleteItem()
{
  ESLPortItem* parentItem = dynamic_cast<ESLPortItem*>(PortEditorTreeItem::parent());
  if (!parentItem->isSelected())
  {
    ESLPortExprItem::Delete* undoData = new ESLPortExprItem::Delete();
    undoData->setText(mESLPort->getName());
    undoData->mCfg = getCcfg();
    undoData->mESLPortName = mESLPort->getName();
    undoData->mPortExpr = mESLPort->getExpr();
    undoData->setTreeData(treeWidget(), this);
    
    return undoData;
  }
  else
    return NULL; // Handled by the parent
}


QWidget* ESLPortExprItem::createEditor(const PortEditorDelegate*, QWidget* parent, int colIndex)
{
  ESLPortExprItem::Columns col = ESLPortExprItem::Columns(colIndex);

  switch (col)
  {
  case ESLPortExprItem::colVALUE:
    {
      QLineEdit* lineEdit = new QLineEdit(parent);
      lineEdit->setValidator(new ESLPortExprValidator(lineEdit, mESLPort->getName()));
      return lineEdit;
    }
    break;
  default:
    break;
  }

  return NULL;
}

void ESLPortExprItem::setModelData(const PortEditorDelegate* , 
                                   QAbstractItemModel* model, const QModelIndex &modelIndex,
                                   QWidget* editor, int colIndex)
{
  ESLPortExprItem::Columns col = ESLPortExprItem::Columns(colIndex);
  switch (col)
  {
  case ESLPortExprItem::colVALUE:
    {
      QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
      if (lineEdit)
      {
        QString oldValue = model->data(modelIndex).toString();
        QString newValue = lineEdit->text();
        model->setData(modelIndex, newValue);
        if (oldValue != newValue)
          getUndoStack()->push(new CmdChangeESLPortExpr(this, getCcfg(), mESLPort, oldValue, newValue));     
      }
    }
    break;
  default:
    break;
  }
}

// Common

void ParameterValueItem::updateESLReferences(const QString&)
{
  if (mParamType == ParameterValueItem::eTypeGLOBAL_PARAM)
  {
    CarbonCfgXtorParamInst* xtorParamInst = getParamInst();
    for (quint32 i=0; i<getCcfg()->numESLPorts(); i++)
    {
      CarbonCfgESLPort* eslPort = getCcfg()->getESLPort(i);
      if (eslPort->getParamInstance() == xtorParamInst)
      {
        ESLPortItem* eslPortItem = getTree()->findESLPort(getTree()->getEditor()->getPortsRoot(), 
                  eslPort->getName());
        if (eslPortItem)
          eslPortItem->setParam(eslPort);
      }
    }
  }
}

QWidget* ParameterValueItem::createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int colIndex)
{
  ParameterValueItem::Columns col = ParameterValueItem::Columns(colIndex);
  if (col == ParameterValueItem::colVALUE)
  {
    CarbonCfgXtorParamInst* xtorParamInst = getParamInst();
    QString value = xtorParamInst->getValue();
    QString defaultValue = xtorParamInst->getDefaultValue();

    QString currValue = value;
    if (currValue.isEmpty())
      currValue = defaultValue;
 
    if (xtorParamInst->getParam()->numEnumChoices() == 0) {
      switch(xtorParamInst->getParam()->getType())
      {
      case eCarbonCfgXtorBool:
        {
          QComboBox* cbox = new QComboBox(parent);
          cbox->addItem("true");
          cbox->addItem("false");
          if (currValue.toLower() == "true")
            cbox->setCurrentIndex(cbox->findText("true"));
          else
            cbox->setCurrentIndex(cbox->findText("false"));

          CQT_CONNECT(cbox, currentIndexChanged(int), delegate, currentIndexChanged(int));
          return cbox;
        }
        break;
      case eCarbonCfgXtorUInt32:
        {
          QLineEdit* lineEdit = new QLineEdit(parent);
          QIntValidator* validator = new QIntValidator(lineEdit);

          // Check if this has a min and max
          UtString minStr(xtorParamInst->getParam()->min());
          UtString maxStr(xtorParamInst->getParam()->max());
          int min;
          if (!minStr.empty()) {
            minStr >> min;
          } else {
            // unsigned in has a 0 bottom
            min = 0;
          }
          validator->setBottom(min);
          if (!maxStr.empty()) {
            int max;
            maxStr >> max;
            validator->setTop(max);
          }

          lineEdit->setText(currValue);
          lineEdit->setValidator(validator);
          return lineEdit;
        }
        break;
      case eCarbonCfgXtorUInt64:
        {
          // Note that I could not find a 64-bit validator. So for now
          // this is using a 32-bit one. We should investigate fixing
          // this.
          QLineEdit* lineEdit = new QLineEdit(parent);
          QIntValidator* validator = new QIntValidator(lineEdit);

          // Check if this has a min and max.
          UtString minStr(xtorParamInst->getParam()->min());
          UtString maxStr(xtorParamInst->getParam()->max());
          int min;
          if (!minStr.empty()) {
            minStr >> min;
          } else {
            // unsigned in has a 0 bottom
            min = 0;
          }
          validator->setBottom(min);
          if (!maxStr.empty()) {
            int max;
            maxStr >> max;
            validator->setTop(max);
          }

          lineEdit->setText(currValue);
          lineEdit->setValidator(validator);
          return lineEdit;
        }
        break;
      case eCarbonCfgXtorDouble:
        {
          QLineEdit* lineEdit = new QLineEdit(parent);
          QDoubleValidator* validator = new QDoubleValidator(lineEdit);

          // Check if this has a min and max
          UtString minStr(xtorParamInst->getParam()->min());
          UtString maxStr(xtorParamInst->getParam()->max());
          double min;
          if (!minStr.empty()) {
            minStr >> min;
            validator->setBottom(min);
          }
          if (!maxStr.empty()) {
            double max;
            maxStr >> max;
            validator->setTop(max);
          }

          lineEdit->setText(currValue);
          lineEdit->setValidator(validator);
          return lineEdit;
        }
        break;
      case eCarbonCfgXtorString:
        {
          QLineEdit* lineEdit = new QLineEdit(parent);
          lineEdit->setText(currValue);
          return lineEdit;
        }
        break;
      }
    }
    else // Enumerated
    {       
      QComboBox* cbox = new QComboBox(parent);
      for (UInt32 i = 0; i < xtorParamInst->numEnumChoices(); ++i) {
        QString enumItem = xtorParamInst->getEnumChoice(i);
        cbox->addItem(enumItem);

        if (currValue == enumItem)
          cbox->setCurrentIndex(cbox->findText(currValue));       
      }

      CQT_CONNECT(cbox, currentIndexChanged(int), delegate, currentIndexChanged(int));
      return cbox;
       
    }
  }
  return NULL;
}

void ParameterValueItem::setModelData(const PortEditorDelegate*, 
        QAbstractItemModel* model, const QModelIndex& modelIndex, 
        QWidget* editor, int colIndex)
{
  ParameterValueItem::Columns col = ParameterValueItem::Columns(colIndex);

  if (col == ParameterValueItem::colVALUE)
  {
    CarbonCfgXtorParamInst* xtorParamInst = getParamInst();
    QString value = xtorParamInst->getValue();
    if (mParamType == eTypeGLOBAL_PARAM)
      value = xtorParamInst->getDefaultValue();

    QString defaultValue = xtorParamInst->getDefaultValue();
    QString oldTextValue = value;
    if (oldTextValue.isEmpty())
      oldTextValue = defaultValue;
    
    QString enumChoices = xtorParamInst->getParam()->getEnumChoices();

    if (enumChoices.isEmpty())
    {
      switch(xtorParamInst->getParam()->getType())
      {
      case eCarbonCfgXtorBool:
        {
          QComboBox* cbox = qobject_cast<QComboBox*>(editor);
          if (cbox)
          {
            bool oldValue = oldTextValue.toLower() == "true" ? true : false;
            bool newValue = cbox->currentText().toLower() == "true" ? true : false;

            model->setData(modelIndex, newValue);

            getUndoStack()->push(new CmdChangeParamValueBool(this, oldValue, newValue));     
          }
        }
        break;
      case eCarbonCfgXtorUInt32:
        {
          QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
          if (lineEdit)
          {
            QString newText = lineEdit->text();
            if (newText != oldTextValue)
            {
              quint32 oldValue = oldTextValue.toUInt();
              quint32 newValue = lineEdit->text().toUInt();
              model->setData(modelIndex, newValue);
              getUndoStack()->push(new CmdChangeParamValueUInt32(this, oldValue, newValue));     
            }
          }
        }
        break;
      case eCarbonCfgXtorUInt64:
        {
          QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
          if (lineEdit)
          {
            QString newText = lineEdit->text();
            if (newText != oldTextValue)
            {
              quint64 oldValue = oldTextValue.toULongLong();
              quint64 newValue = lineEdit->text().toULongLong();
              model->setData(modelIndex, newValue);
              getUndoStack()->push(new CmdChangeParamValueUInt64(this, oldValue, newValue));     
            }
          }
        }
        break;
      case eCarbonCfgXtorDouble:
        {
          QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
          if (lineEdit)
          {
            QString newText = lineEdit->text();
            if (newText != oldTextValue)
            {
              double oldValue = oldTextValue.toDouble();
              double newValue = lineEdit->text().toDouble();
              model->setData(modelIndex, newValue);
              getUndoStack()->push(new CmdChangeParamValueDouble(this, oldValue, newValue));     
            }
          }
        }
        break;
      case eCarbonCfgXtorString:
        {
          QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
          if (lineEdit)
          {
            QString newText = lineEdit->text();
            if (newText != oldTextValue)
            {
              QString oldValue = oldTextValue;
              QString newValue = lineEdit->text();
              model->setData(modelIndex, newValue);
              getUndoStack()->push(new CmdChangeParamValueString(this, oldValue, newValue));     
            }
          }
        }
        break;
      }
    }
    else // Enumerated
    {
      QComboBox* cbox = qobject_cast<QComboBox*>(editor);
      if (cbox)
      {
        QString oldValue = oldTextValue;
        QString newValue = cbox->currentText();

        model->setData(modelIndex, newValue);

        getUndoStack()->push(new CmdChangeParamValueString(this, oldValue, newValue));     
      }
    }
  }
}

ESLParameterItem::AddParameter::AddParameter(ParametersRootItem* item, 
      CarbonCfgParamDataType type, const QString& name, const QString& value,
      const QString& description, const QString& enumChoices, CarbonCfgParamFlag scope, QUndoCommand* parent)
      : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mType = type;
  mName = name;
  mValue = value;
  mDescription = description;
  mEnumChoices = enumChoices;
  mScope = scope;

  setText(QString("Add Parameter %1").arg(mName));
}

void ESLParameterItem::AddParameter::undo()
{
  CcfgHelper ccfg(mCfg);
  qDebug() << "Undo Add New Parameter" << mName << mType << mValue << mDescription << mEnumChoices << mScope;
  CarbonCfgXtorParamInst* paramInst = ccfg.findParameter(mName);
  INFO_ASSERT(paramInst, "Expected parameter");
  
  PortEditorTreeItem* item = getItem();
  getTree()->removeItem(item);

  mCfg->removeParam(paramInst);

  getTree()->setModified(false);
}

void ESLParameterItem::AddParameter::redo()
{
  CcfgHelper ccfg(mCfg);

  qDebug() << "Add New Parameter" << mName << mType << mValue << mDescription << mEnumChoices << mScope;

  CarbonCfgXtorParamInst* paramInst = ccfg.addParam(mType, mName, mValue, mDescription, mScope, mEnumChoices);
  INFO_ASSERT(paramInst, "Expected paraminst");

  QTreeWidgetItem* paramsRoot = getTree()->getEditor()->getParamsRoot();
  ESLParameterItem* newItem = new ESLParameterItem(paramsRoot, paramInst);
  updateTreeIndex(newItem);
  
  newItem->setExpanded(true);
  newItem->setSelected(true);
  getTree()->scrollToItem(newItem);

  getTree()->setModified(true);
}

ParametersRootItem::ParametersRootItem(QTreeWidget* parent)
    : PortEditorTreeItem(PortEditorTreeItem::GlobalParameters, parent)
{
  setBoldText(0, "Component Parameters");

  mActionAddParam = new QAction(QIcon(":/cmm/Resources/NewCardHS.png"), tr("Add Parameter"), this);
  mActionAddParam->setToolTip("Add Parameter");
  mActionAddParam->setStatusTip("Add a new Parameter.");
  CQT_CONNECT(mActionAddParam, triggered(), getTree()->getEditor(), actionAddParameter());
}

void ParametersRootItem::multiSelectContextMenu(QMenu& menu)
{
  menu.addAction(mActionAddParam);
}

bool DisconnectsRootItem::canDropHere(const QList<PortEditorTreeItem*>& items, QDragMoveEvent*)
{
  foreach(PortEditorTreeItem* item, items)
  {
    if (item->getNodeType() == PortEditorTreeItem::ESLPort)
    {
      return true;
    }
  }

  return false;
}

bool DisconnectsRootItem::dropItems(const QList<PortEditorTreeItem*>& items)
{
  QList<PortEditorTreeItem*> eslPorts;
  foreach(PortEditorTreeItem* item, items)
  {
    if (item->getNodeType() == PortEditorTreeItem::ESLPort)
    {
      eslPorts.append(dynamic_cast<ESLPortItem*>(item));
    }
  }

  qDebug() << "Disconnect" << eslPorts.count() << "esl ports";

  getTree()->getUndoStack()->push(new CmdDeleteItems(eslPorts));

  return true;
}
bool TiesRootItem::dropItems(const QList<PortEditorTreeItem*>& items)
{
  QList<PortEditorTreeItem*> eslPorts;
  foreach(PortEditorTreeItem* item, items)
  {
    if (item->getNodeType() == PortEditorTreeItem::ESLPort)
    {
      eslPorts.append(dynamic_cast<ESLPortItem*>(item));
    }
  }

  qDebug() << "Tie" << eslPorts.count() << "esl ports";

  QAction* action = ESLPortItem::getActions(getTree())->getAction(ESLPortActions::TieToOne);
  if (action)
     action->trigger();
 
  return true;
}
bool TiesRootItem::canDropHere(const QList<PortEditorTreeItem*>& items, QDragMoveEvent*)
{
  foreach(PortEditorTreeItem* item, items)
  {
    if (item->getNodeType() == PortEditorTreeItem::ESLPort)
    {
      return true;
    }
  }
  return false;
}

// Resets
bool ResetsRootItem::dropItems(const QList<PortEditorTreeItem*>& items)
{
  QList<PortEditorTreeItem*> eslPorts;
  foreach(PortEditorTreeItem* item, items)
  {
    if (item->getNodeType() == PortEditorTreeItem::ESLPort)
    {
      eslPorts.append(dynamic_cast<ESLPortItem*>(item));
    }
  }

  qDebug() << "Resets" << eslPorts.count() << "esl ports";

  QAction* action = ESLPortItem::getActions(getTree())->getAction(ESLPortActions::ResetGen);
  if (action)
     action->trigger();
 
  return true;
}
bool ResetsRootItem::canDropHere(const QList<PortEditorTreeItem*>& items, QDragMoveEvent*)
{
  foreach(PortEditorTreeItem* item, items)
  {
    if (item->getNodeType() == PortEditorTreeItem::ESLPort)
    {
      return true;
    }
  }
  return false;
}

// CLocks
bool ClocksRootItem::dropItems(const QList<PortEditorTreeItem*>& items)
{
  QList<PortEditorTreeItem*> eslPorts;
  foreach(PortEditorTreeItem* item, items)
  {
    if (item->getNodeType() == PortEditorTreeItem::ESLPort)
    {
      eslPorts.append(dynamic_cast<ESLPortItem*>(item));
    }
  }

  qDebug() << "ClockGen" << eslPorts.count() << "esl ports";

  QAction* action = ESLPortItem::getActions(getTree())->getAction(ESLPortActions::ClockGen);
  if (action)
     action->trigger();
 
  return true;
}
bool ClocksRootItem::canDropHere(const QList<PortEditorTreeItem*>& items, QDragMoveEvent*)
{
  foreach(PortEditorTreeItem* item, items)
  {
    if (item->getNodeType() == PortEditorTreeItem::ESLPort)
    {
      return true;
    }
  }
  return false;
}

// We are draggint from the Hierarchy Window?
bool PortsRootItem::canDropHere(const QString& nets, QDragMoveEvent* ev)
{
  CcfgHelper ccfg(getCcfg());
  qDebug() << ev->possibleActions() << ev->proposedAction();

  QStringList netList = nets.split("\n");
  
  int goodNets = 0;
  foreach(QString net, netList)
  {
    qDebug() << "Dragging net" << net;
    if (ccfg.findRTLPort(net))
    {
      qDebug() << "already exists" << net;
    }
    else // Verify net type
    { 
      UtString uNetName; uNetName << net;
      const CarbonDBNode* node = carbonDBFindNode(getCcfg()->getDB(), uNetName.c_str());
      if (node && carbonDBCanBeCarbonNet(getCcfg()->getDB(), node))
      {
        bool depositable = carbonDBIsDepositable(getCcfg()->getDB(), node);
        bool observable = carbonDBIsObservable(getCcfg()->getDB(), node);

        if (depositable || observable)
        {
          qDebug() << "create new rtl/esl port" << net;
          goodNets++;
        }
      }
      else
        qDebug() << "Composites are not allowed:" << net;
    }
  }

  if (goodNets == netList.count())
    ev->setDropAction(Qt::CopyAction);
  else if (goodNets > 0)
    ev->setDropAction(Qt::MoveAction);
  else
  {
    QPoint pos = ev->pos();
    QString msg = QString("Port already exists, is a composite, or not visible which is not allowed.");
    QToolTip::showText(getTree()->viewport()->mapToGlobal(pos), msg, getTree());
    ev->setDropAction(Qt::IgnoreAction);
    return false;
  }

  return true;
}

bool PortsRootItem::dropItem(const QString& nets)
{
  CcfgHelper ccfg(getCcfg());

  QStringList netList = nets.split("\n");
  
  QString rtlPortName = netList.first().trimmed();

  if (!ccfg.findRTLPort(rtlPortName))
  {
    UtString uNetName; uNetName << rtlPortName;
    const CarbonDBNode* node = carbonDBFindNode(getCcfg()->getDB(), uNetName.c_str());
    if (node && carbonDBCanBeCarbonNet(getCcfg()->getDB(), node))
      getUndoStack()->push(new ESLPortItem::CreatePort(this, rtlPortName));
  }
 
  return true;
}

// CLocks
bool SystemCClocksRootItem::dropItems(const QList<PortEditorTreeItem*>& items)
{
  QList<PortEditorTreeItem*> eslPorts;
  foreach(PortEditorTreeItem* item, items)
  {
    if (item->getNodeType() == PortEditorTreeItem::ESLPort)
    {
      eslPorts.append(dynamic_cast<ESLPortItem*>(item));
    }
  }

  qDebug() << "ClockGen" << eslPorts.count() << "esl ports";

  QAction* action = ESLPortItem::getActions(getTree())->getAction(ESLPortActions::ClockGen);
  if (action)
     action->trigger();
 
  return true;
}
bool SystemCClocksRootItem::canDropHere(const QList<PortEditorTreeItem*>& items, QDragMoveEvent*)
{
  foreach(PortEditorTreeItem* item, items)
  {
    if (item->getNodeType() == PortEditorTreeItem::ESLPort)
    {
      return true;
    }
  }
  return false;
}










