#ifndef __CARBONCOMPONENTS_H__
#define __CARBONCOMPONENTS_H__
#include "util/CarbonPlatform.h"

#include <QList>
#include <QTreeWidget>

#include "util/XmlParsing.h"
#include "util/UtString.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtHashMap.h"
#include "util/UtStringArray.h"
#include "util/OSWrapper.h"

class CarbonProject;
class UtXmlErrorHandler;
class CarbonComponent;
class CarbonProjectWidget;

class CarbonComponents
{
public:
  CarbonComponents(CarbonProject* proj);
  virtual ~CarbonComponents();
  CarbonComponent* addComponent(CarbonComponent* comp);
  CarbonComponent* addComponent(const char* name);
  CarbonComponent* findComponent(const char* name);
  CarbonComponent* getComponent(UInt32 index) { return mComponents[index]; }
  UInt32 numComponents() { return mComponents.size(); }
  int getActiveComponents(QList<CarbonComponent*>* components);
  void populateComponentTree(CarbonProjectWidget* tree, QTreeWidgetItem* parent);
  void deleteComponent(CarbonComponent* comp);
  bool deserialize(xmlNodePtr parent, UtXmlErrorHandler* eh);
  bool serialize(xmlTextWriterPtr writer);

  typedef UtArray<CarbonComponent*> ComponentVec;

private:
  void clearNodes(QTreeWidget* tree, QTreeWidgetItem* parent);

private:
  ComponentVec mComponents;
  CarbonProject* mProject;
};

#endif





