#ifndef __WIZARDTEMPLATE_H_
#define __WIZARDTEMPLATE_H_
#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "Scripting.h"
#include "WizardContext.h"

class WizardTemplate
{
public:
  WizardTemplate(QWidget* parent) : mContext(&mEngine)
  {
    mParent = parent;
  }
  bool readTemplate(const QString& templFile);

  QString getTemplatePath() const { return mTemplatePath; }
  
  WizardContext* getContext() { return &mContext; }
  ScriptingEngine* getEngine() { return &mEngine; }


private:
  ScriptingEngine mEngine;
  WizardContext mContext;
  QString mTemplatePath;
  QWidget* mParent;

};

#endif
