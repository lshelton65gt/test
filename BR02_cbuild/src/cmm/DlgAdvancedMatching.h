#ifndef DLGADVANCEDMATCHING_H
#define DLGADVANCEDMATCHING_H

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include <QDialog>
#include "ui_DlgAdvancedMatching.h"
  
class DlgAddMaxsimXtor;

class DlgAdvancedMatching : public QDialog
{
	Q_OBJECT

    enum MatchAction { Prefix, Suffix, Remove };

public:
	DlgAdvancedMatching(DlgAddMaxsimXtor* parentDlg, QWidget *parent = 0);
	~DlgAdvancedMatching();
    QString ComputeMatch(const QString& input);
    QString patternText()
    {
      return ui.comboBoxRegex->currentText();
    }

private:
	Ui::DlgAdvancedMatchingClass ui;
    DlgAddMaxsimXtor* mParentDlg;    

private slots:   
    void on_buttonBox_accepted();
    void on_comboBoxPorts_textChanged(const QString &);
    void on_comboBoxRegex_textChanged(const QString &);  
    void currentIndexChanged(int);

private slots:
	void on_buttonBox_rejected();
	void on_pushButtonApply_clicked();			
};

#endif // DLGADVANCEDMATCHING_H
