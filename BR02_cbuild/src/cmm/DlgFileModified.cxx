//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "DlgFileModified.h"

DlgFileModified::DlgFileModified(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);
}

DlgFileModified::~DlgFileModified()
{
}

void DlgFileModified::on_buttonBox_clicked(QAbstractButton* button)
{
  mReturnValue = QDialogButtonBox::Yes;

  if (button == ui.buttonBox->button(QDialogButtonBox::Yes))
    mReturnValue = QDialogButtonBox::Yes;
  else if (button == ui.buttonBox->button(QDialogButtonBox::YesToAll))
    mReturnValue = QDialogButtonBox::YesToAll;
  else if (button == ui.buttonBox->button(QDialogButtonBox::No))
    mReturnValue = QDialogButtonBox::No;
  else if (button == ui.buttonBox->button(QDialogButtonBox::NoToAll))
    mReturnValue = QDialogButtonBox::NoToAll;

  accept();
}

