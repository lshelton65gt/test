#ifndef MKUWPAGEFINISHED_H
#define MKUWPAGEFINISHED_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>
#include "WizardPage.h"
#include <QWizardPage>
#include "ui_MKUWPageFinished.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"

class KitUserAction;
class UserActionUI;
class ModelKit;
class CarbonProjectWidget;
class CarbonProject;

class MKUWPageFinished : public WizardPage
{
  Q_OBJECT

public:
  MKUWPageFinished(QWidget *parent = 0);
  ~MKUWPageFinished();
  bool cleanupFiles();
  int searchFile(const QString& filePath, const QString& stringToMatch, 
                 QRegExp::PatternSyntax pat, Qt::CaseSensitivity caseSense, bool matchWholeWord);
  bool binaryText(const QString& line);

protected:
  virtual bool validatePage();
  virtual void initializePage();
  virtual bool isComplete() const;


private:
  bool executePostCompilationActions();
  void saveRetainedFiles(ModelKit* kit, const QString& retainDir);
  void removeDirectory(const QString& dir);
  void removeAllFilesFromDirectory(const QString& directory);
  void addKnownFiles(ModelKit* kit, const QString& retainDir);

private slots:
  void makefileCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void processTerminated(CarbonConsole::CommandMode, CarbonConsole::CommandType, int);
  void finishCompile();
  void stdoutChanged(CarbonConsole::CommandType, const QString&, const QString&);
  void setStatusMessage(const QString& msg);
  void setProgress(int value);
  void setProgressRange(int low, int high);

private:
  bool mCompilationEnded;
  Ui::MKUWPageFinishedClass ui;
  bool mCompilationPhase1;
  bool mCompilationPhase2;
  bool mStartedPost;
  int mStepNumber;
  QMap<KitUserAction*, UserActionUI*> mActionMap;
  bool mInitialized;
  CarbonProjectWidget* mProjectWidget;
  CarbonProject* mProject;
  QString mProjectDir;
  QString mOutputDir;
  QString mPlatform;
};

#endif // MKUWPAGEFINISHED_H
