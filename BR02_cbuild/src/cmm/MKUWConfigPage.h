#ifndef MKUWCONFIGPAGE_H
#define MKUWCONFIGPAGE_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>
#include "WizardPage.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "ui_MKUWConfigPage.h"

class MKUWConfigPage : public WizardPage
{
  Q_OBJECT

public:
  MKUWConfigPage(QWidget *parent = 0);
  ~MKUWConfigPage();

protected:
  virtual bool validatePage();
  virtual void initializePage();
  virtual int nextId() const;

private:
  void saveSettings();
  void restoreSettings();
  void updateProtectedFiles();

private:
  QSettings mSettings;
  Ui::MKUWConfigPageClass ui;
};

#endif // MKUWCONFIGPAGE_H
