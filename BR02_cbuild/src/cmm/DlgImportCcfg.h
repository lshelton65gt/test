#ifndef DLGIMPORTCCFG_H
#define DLGIMPORTCCFG_H

#include "util/CarbonPlatform.h"

#include <QDialog>
#include "ui_DlgImportCcfg.h"

class CarbonProjectWidget;

class DlgImportCcfg : public QDialog
{
  Q_OBJECT

public:
  enum CcfgType {eCcfgCoWare, eCcfgArm};

public:
  DlgImportCcfg(QWidget *parent = 0, CarbonProjectWidget* pw = 0);
  ~DlgImportCcfg();  
  QString getFilename() { return mFilename; }
  CcfgType getType() { return mType; }

private:
  Ui::DlgImportCcfgClass ui;
  
private:
  bool legalValues();

private slots:
  void on_toolButtonBrowse_clicked();
  void on_buttonBox_accepted();
  void on_buttonBox_rejected();

private:
  CarbonProjectWidget* mProjectWidget;
  QString mFilename;
  CcfgType mType;
};

#endif // DLGIMPORTCCFG_H
