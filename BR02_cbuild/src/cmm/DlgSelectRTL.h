#ifndef DLGSELECTRTL_H
#define DLGSELECTRTL_H

#include "util/CarbonPlatform.h"

#include <QtGui>

#include "ui_DlgSelectRTL.h"
#include "gui/CQt.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/CarbonCfg.h"

class DlgSelectRTL : public QDialog
{
  Q_OBJECT

public:
  enum SelectionType { AllPorts, InputPorts, OutputPorts };

  DlgSelectRTL(CarbonCfg* cfg = 0, SelectionType = InputPorts, bool singleSelection = true, QWidget *parent = 0);
  ~DlgSelectRTL();

  const QStringList& getPorts() { return mSelectedPorts; }

private:
  CarbonCfg* mCfg;
  bool mSingleSelection;
  Ui::DlgSelectRTLClass ui;
  QStringList mSelectedPorts;

private slots:
    void on_treeWidget_itemSelectionChanged();
    void on_buttonBox_rejected();
    void on_buttonBox_accepted();
};

#endif // DLGSELECTRTL_H
