//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "BlockInstance.h"
#include "ElaboratedModel.h"

Ports* BlockInstance::getPorts()
{
  return &mPorts;
}
Parameters* BlockInstance::getParameters()
{
  return &mParameters;
}

QObject* BlockInstance::getPortInstances()
{
  return mModel->getPortInstances(mBlock->getName(), mInstanceID);
}

BlockInstance::BlockInstance(ElaboratedModel* model, Block* block, QString instanceID)
{
  mModel = model;
  mBlock = block;
  mInstanceID = instanceID;
  copyData();
  mInstanceName = QString("%1 %2").arg(block->getName()).arg(instanceID);
}

void BlockInstance::copyData()
{
  Parameters* params = mBlock->getParameters();
  Ports* ports = mBlock->getPorts();

  // copy parameters
  for (int i=0; i<params->getCount(); i++)
  {
    Parameter* param = params->getAt(i);
    if (param)
      mParameters.addParam(param->copyParameter());
  }

  // copy ports
  for (int i=0; i<ports->getCount(); i++)
  {
    Port* port = ports->getAt(i);
    if (port)
      mPorts.addPort(port->copyPort());
  }
}
