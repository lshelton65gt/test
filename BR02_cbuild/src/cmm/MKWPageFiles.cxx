//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "ModelKitWizard.h"
#include "CarbonMakerContext.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"
#include "CarbonComponent.h"
#include "KitManifest.h"
#include "gui/CQt.h"

#define colPATH 0
#define colKIND 1
#define colCOPY 2
#define colHASHED 3
#define colOPTIONAL 4


void MKWPageFiles::on_pushButtonAddFiles_clicked()
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QStringList files = QFileDialog::getOpenFileNames(pw, "Select one or more files",
                           proj->getProjectDirectory(),
                           "All Files (*)");  
  foreach (QString fileName, files)
  {
    addEmbeddedFile(fileName, mAdditionalFiles, true);
  }

  mAdditionalFiles->setExpanded(true);
}


MKWPageFiles::MKWPageFiles(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);
  mDesignHierarchy = NULL;
  mInitialized = false;
  mAdditionalFiles = NULL;

  ui.pushButtonDelete->setEnabled(false);

  setTitle("ModelKit File Manager");
  setSubTitle("Files are either hashed or embedded.  Hashed means that it has a signature and embedded means shipped with the kit.");

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QStringList colNames;
  colNames << "File Path" << "Type" << "Copy" << "Hashed" << "Optional";
 
  ui.treeWidget->setHeaderLabels(colNames);
  ui.treeWidget->setColumnCount(colNames.count());

  if (proj && proj->getActive())
  {
    const char* designHierarchyFile = proj->getDesignFilePath(".designHierarchy");
    mDesignHierarchy = new XDesignHierarchy(designHierarchyFile);
    XmlErrorHandler* eh = mDesignHierarchy->getErrorHandler();      
    if (eh->hasErrors())
    {
      QString msg = QString("Error reading design hierarchy: %1\n%2")
        .arg(designHierarchyFile)
        .arg(eh->errorMessages().join("\n"));
      UtString errMsg;
      errMsg << msg;
      pw->getConsole()->processOutput(CarbonConsole::Local, CarbonConsole::Command, errMsg.c_str());
    }
  }

  

  ui.treeWidget->setItemDelegate(new FileManagerDelegate(this, ui.treeWidget, this));
  ui.treeWidget->setEditTriggers(QAbstractItemView::AllEditTriggers);

  CQT_CONNECT(ui.treeWidget, itemSelectionChanged(), this, itemSelectionChanged());
  CQT_CONNECT(ui.treeWidget, itemActivated(QTreeWidgetItem*,int), this, itemActivated(QTreeWidgetItem*,int));

}

void MKWPageFiles::itemSelectionChanged()
{
}

void MKWPageFiles::itemActivated(QTreeWidgetItem* item, int col)
{
  qDebug() << item->text(0) << col;
}

void MKWPageFiles::cleanupPage()
{
  mInitialized = false;
}
void MKWPageFiles::initializePage()
{
  if (!mInitialized)
  {
    if (mDesignHierarchy)
      populate(); 
    mInitialized = true;
  }

  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());
  if (wiz->getRegenerate())
    wiz->next();
}

bool MKWPageFiles::validatePage()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());

  QList<OptionalHashedFile*>& optionalHashedFileList = wiz->getOptionalHashedFileList();
  QStringList& hashedFileList = wiz->getHashedFileList();
  QStringList& fileList = wiz->getFileList();
  QStringList& userFileList = wiz->getUserFileList();
  QMap<QString, bool>& checkedFileMap = wiz->getCheckedFileMap();
  QMap<QString, RTLFileOptions*>& fileOptionsMap = wiz->getFileOptionsMap();

  checkedFileMap.clear();
  hashedFileList.clear();
  optionalHashedFileList.clear();
  fileOptionsMap.clear();

  fileList.clear();
  userFileList.clear();

  for (int i=0; i<ui.treeWidget->topLevelItemCount(); i++)
  {
    QTreeWidgetItem* rootItem = ui.treeWidget->topLevelItem(i);
    for (int j=0; j<rootItem->childCount(); j++)
    {
      QTreeWidgetItem* item = rootItem->child(j);
      QVariant qv = item->data(colPATH, Qt::UserRole);
      QString fileName = qv.toString();

      QCheckBox* cbCopyFile = qobject_cast<QCheckBox*>(ui.treeWidget->itemWidget(item, colCOPY));
      QCheckBox* cbHashFile = qobject_cast<QCheckBox*>(ui.treeWidget->itemWidget(item, colHASHED));
      QCheckBox* cbOptional = qobject_cast<QCheckBox*>(ui.treeWidget->itemWidget(item, colOPTIONAL));
     

      // only RTL files have checkboxes
      if (cbCopyFile)
      {
        RTLFileOptions* rfu = new RTLFileOptions();
        rfu->mCopied = cbCopyFile->isChecked();
        rfu->mHashed = cbHashFile->isChecked();
        rfu->mOptional = cbOptional->isChecked();
        rfu->mFileName = item->text(colPATH);

        fileOptionsMap[rfu->mFileName] = rfu;
        
        checkedFileMap[item->text(colPATH)] = cbCopyFile->isChecked();
      }

      QVariant vkind = item->data(colKIND, Qt::UserRole);
      FileKind kind = (FileKind)vkind.toInt();

      if (cbOptional && cbOptional->isChecked())
      {
        OptionalHashedFile* hf = new OptionalHashedFile;
        hf->mFileName = fileName;
        hf->mChecked = true;
        optionalHashedFileList << hf;
      }

      if (cbHashFile)
        hashedFileList << fileName;
      
      if (kind == MKWPageFiles::EmbeddedFile)
        fileList << fileName;
      else if (kind == MKWPageFiles::EmbeddedUserFile)
        userFileList << fileName;
    }
  }
  return true;
}

// Add all the Referenced (parsed) RTL files
// to be in the list of files to be hash checked.
void MKWPageFiles::addRTLFiles()
{
  ModelKitWizard* wiz = (ModelKitWizard*)wizard();

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QTreeWidgetItem* rtlItems = new QTreeWidgetItem(ui.treeWidget);
  rtlItems->setText(0, "RTL Files");
  rtlItems->setFlags(rtlItems->flags() & ~Qt::ItemIsUserCheckable);
  rtlItems->setData(colPATH, Qt::UserRole, QVariant());
  rtlItems->setData(colKIND, Qt::UserRole, QVariant());


  QStringList armFiles;
  QStringList fileKinds;
  fileKinds << "*";

  QString armFolder = wiz->getARMRoot();

  QMap<QString,bool>& checkedMap = wiz->getCheckedFileMap();
  QMap<QString,RTLFileOptions*> optionsMap = wiz->getFileOptionsMap();

  // Get all Files
  wiz->getFilesForFolder(armFiles, armFolder, fileKinds);

  bool isSpecialization = theApp->checkArgument("-modelKitRegen") && theApp->checkArgument("-modelKitManifest");

  // Add in all the RTL referenced files first.
  foreach (QString file, armFiles)
  {
    QString fileName = wiz->normalizeName(file);

    if (isSpecialization && !checkedMap.contains(fileName))
    {
      qDebug() << "Skipping new file" << fileName << isSpecialization;
      continue;
    }
  


    FileManagerTreeItem* item = new FileManagerTreeItem(FileManagerTreeItem::RTLSourceFile, rtlItems);
    item->setText(colPATH, fileName);
   

    QCheckBox* cbHashed = new QCheckBox();
    cbHashed->setChecked(true);    
    ui.treeWidget->setItemWidget(item, colHASHED, cbHashed);

    QCheckBox* cbCopy = new QCheckBox();
    cbCopy->setChecked(true);
    ui.treeWidget->setItemWidget(item, colCOPY, cbCopy);

    QCheckBox* cbOptional = new QCheckBox();
    cbOptional->setChecked(false);
    ui.treeWidget->setItemWidget(item, colOPTIONAL, cbOptional);


    QString actualName = proj->getWindowsEquivalentPath(file);
    QVariant qv(actualName);
    item->setData(colPATH, Qt::UserRole, qv);
    item->setData(colKIND, Qt::UserRole, (int)RTLFileReference);
    item->setFlags(item->flags() | Qt::ItemIsEditable);
    
    item->setFlags(item->flags() & ~Qt::ItemIsUserCheckable);

    Qt::CheckState checkFlag = Qt::Checked;
    if (checkedMap.contains(fileName))
      checkFlag = checkedMap[fileName] ? Qt::Checked : Qt::Unchecked;
   

   // item->setCheckState(colPATH, checkFlag);
    
    cbCopy->setCheckState(checkFlag);

    if (optionsMap.contains(fileName))
    {
      RTLFileOptions* rfu = optionsMap[fileName];

      cbCopy->setCheckState(rfu->mCopied ? Qt::Checked : Qt::Unchecked);
      cbOptional->setCheckState(rfu->mOptional ? Qt::Checked : Qt::Unchecked);
      cbHashed->setCheckState(rfu->mHashed ? Qt::Checked : Qt::Unchecked);
    }

    //cb->setChecked(true);
  }
  rtlItems->setExpanded(true);
}


// Add all the input and generated component files
// which will be embedded in the .modelKit file
//
void MKWPageFiles::embedProjectFiles()
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  QTreeWidgetItem* rootItem = new QTreeWidgetItem(ui.treeWidget);
  rootItem->setText(0, "Project Files");
  rootItem->setFlags(rootItem->flags() & ~Qt::ItemIsUserCheckable);

  QFileInfo fi(proj->getDesignFilePath(".dir"));
  QString newName = QString("%1/cbuild.dir").arg(fi.path());
  
  UtString newname;
  newname << newName;
  UtString err;
  if (!OSCopyFile(proj->getDesignFilePath(".dir"), newname.c_str(), &err))
  {
    QMessageBox::critical(NULL, MODELSTUDIO_TITLE, "Fatal error copying directives file");
    return;
  }

  // Add directives
  addEmbeddedFile(newname.c_str(), rootItem);

  rootItem->setExpanded(true);
}

void MKWPageFiles::addEmbeddedFile(const QString& rawFileName, QTreeWidgetItem* parentItem, bool userFile)
{
  ModelKitWizard* wiz = (ModelKitWizard*)wizard();
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QString fileName = wiz->normalizeProjectName(rawFileName);
  QTreeWidgetItem* item = new QTreeWidgetItem(parentItem);
  item->setText(colPATH, fileName);

  UtString name;
  name << rawFileName;
  UtString actualName;
  actualName << proj->getWindowsEquivalentPath(name.c_str());
  QVariant qv(actualName.c_str());
  item->setData(colPATH, Qt::UserRole, qv);
  item->setData(colKIND, Qt::UserRole, userFile ? (int)EmbeddedUserFile : (int)EmbeddedFile);
  item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
  item->setCheckState(0, Qt::Checked);
  item->setText(colKIND, "embed");
}


// Add all the input and generated component files
// which will be embedded in the .modelKit file
//
void MKWPageFiles::embedComponentFiles()
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QList<CarbonComponent*> comps;
  proj->getComponents()->getActiveComponents(&comps);
  foreach (CarbonComponent* comp, comps)
  {
    QStringList files;
    int nFiles = comp->getSourceFiles(files);
    if (nFiles > 0)
    {
      QTreeWidgetItem* rootItem = new QTreeWidgetItem(ui.treeWidget);
      UtString compName;
      compName << comp->getFriendlyName() << " Files";
      rootItem->setFlags(rootItem->flags() & ~Qt::ItemIsUserCheckable);
      rootItem->setText(0, compName.c_str());
      foreach (QString rawFileName, files)
        addEmbeddedFile(rawFileName, rootItem);
      rootItem->setExpanded(true);
    }
  }
}

void MKWPageFiles::addUserFiles()
{
  ModelKitWizard* wiz = (ModelKitWizard*)wizard();
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  foreach (QString rawFileName, wiz->getUserFileList())
  {
    QString fileName = wiz->normalizeProjectName(rawFileName);
    QTreeWidgetItem* item = new QTreeWidgetItem(mAdditionalFiles);
    item->setText(colPATH, fileName);

    UtString name;
    name << rawFileName;
    UtString actualName;
    actualName << proj->getWindowsEquivalentPath(name.c_str());

    qDebug() << "rawname" << rawFileName << "normalized" << fileName << "actual" << actualName.c_str();

    QVariant qv(actualName.c_str());
    item->setData(colPATH, Qt::UserRole, qv);
    item->setData(colKIND, Qt::UserRole, (int)EmbeddedUserFile);
    item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
    item->setCheckState(0, Qt::Checked);
    item->setText(colKIND, "embed");
  }
  mAdditionalFiles->setExpanded(true);
}


// build the list of design files here
void MKWPageFiles::populate()
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  ModelKitWizard* wiz = (ModelKitWizard*)wizard();

  ui.treeWidget->clear();

  // Add the Project files
  embedProjectFiles();

  // now, collect the component files
  embedComponentFiles();

  if (!wiz->getARMRoot().isEmpty())
  {
    addRTLFiles();  
  }

  // add the Additional Files Node
  mAdditionalFiles = new QTreeWidgetItem(ui.treeWidget);
  mAdditionalFiles->setText(0, "Additional Files");
  mAdditionalFiles->setFlags(mAdditionalFiles->flags() & ~Qt::ItemIsUserCheckable);

  addUserFiles();

  ui.treeWidget->header()->setResizeMode(0, QHeaderView::Interactive);
  ui.treeWidget->header()->setResizeMode(colKIND, QHeaderView::ResizeToContents);
  ui.treeWidget->header()->setResizeMode(colCOPY, QHeaderView::ResizeToContents);
  ui.treeWidget->header()->setResizeMode(colHASHED, QHeaderView::ResizeToContents);
  ui.treeWidget->header()->setResizeMode(colOPTIONAL, QHeaderView::ResizeToContents);

  ui.treeWidget->header()->setResizeMode(1, QHeaderView::ResizeToContents);
  ui.treeWidget->header()->resizeSections(QHeaderView::ResizeToContents);
  ui.treeWidget->header()->setResizeMode(0, QHeaderView::Interactive);

  QApplication::restoreOverrideCursor();
}

MKWPageFiles::~MKWPageFiles()
{
}


void MKWPageFiles::on_toolButtonCheckAll_clicked()
{
  checkItems(NULL, Qt::Checked);
}

void MKWPageFiles::on_toolButtonUncheckAll_clicked()
{
  checkItems(NULL, Qt::Unchecked);
}

void MKWPageFiles::checkItems(QTreeWidgetItem* item, Qt::CheckState checkState)
{
  if (item == NULL)
  {
    for(int i=0; i<ui.treeWidget->topLevelItemCount(); i++)
    {
      QTreeWidgetItem* item = ui.treeWidget->topLevelItem(i);
      checkItems(item, checkState);
    }
  }
  else
  {
    if (item->flags() & Qt::ItemIsUserCheckable)
      item->setCheckState(0, checkState);

    QCheckBox* cbCopy = qobject_cast<QCheckBox*>(ui.treeWidget->itemWidget(item, colCOPY));
    if (cbCopy)
      cbCopy->setCheckState(checkState);

    for (int i=0; i<item->childCount(); i++)
    {
      QTreeWidgetItem* childItem = item->child(i);
      checkItems(childItem, checkState);
    }
  }
}


void MKWPageFiles::on_pushButtonDelete_clicked()
{
  QTreeWidgetItem* item = ui.treeWidget->selectedItems().first();
  QTreeWidgetItem* parent = item->parent();
  int childIndex = parent->indexOfChild(item);
  parent->takeChild(childIndex);
}

void MKWPageFiles::on_treeWidget_itemSelectionChanged()
{
  ui.pushButtonDelete->setEnabled(false);

  if (ui.treeWidget->selectedItems().count() > 0)
  {
    QTreeWidgetItem* item = ui.treeWidget->selectedItems().first();
    if (item)
      ui.pushButtonDelete->setEnabled(item->parent() != NULL);   
  }
}

QWidget* FileManagerTreeItem::createEditor(const FileManagerDelegate* /*delegate*/, 
    QWidget* parent, int columnIndex)
{
  if (columnIndex == colKIND)
  {
    QCheckBox* cb = new QCheckBox(parent);  
    cb->setChecked(getHashed());
    return cb;
  }
  else
    return NULL;
}


void FileManagerTreeItem::setModelData(const FileManagerDelegate* /*delegate*/, 
    QAbstractItemModel* model, const QModelIndex & modelIndex,
    QWidget* editor, int columnIndex)
{

  if (columnIndex == colKIND)
  {
    QCheckBox* cb = qobject_cast<QCheckBox*>(editor);
    if (cb)
    {
      bool oldChecked = getHashed();
      bool newChecked = cb->isChecked();
      
      if (oldChecked != newChecked)
      {
        QString value = "Hashed";
        if (!newChecked)
          value = "Not Hashed";
        setHashed(newChecked);
        model->setData(modelIndex, value);
      }
    }
  }
}


// Editor Delegate
FileManagerDelegate::FileManagerDelegate(QObject *parent, QTreeWidget* tw, MKWPageFiles* pe)
: QItemDelegate(parent), mTree(tw), mPageFiles(pe)
{
}

 

void FileManagerDelegate::currentIndexChanged(int)
{
  emit commitData(qobject_cast<QWidget*>(sender()));
  emit closeEditor(qobject_cast<QWidget*>(sender()));
}

QWidget *FileManagerDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                              const QModelIndex &index) const
{
  int col = index.column();

  QTreeWidgetItem* treeItem = mTree->currentItem();

  FileManagerTreeItem* item = dynamic_cast<FileManagerTreeItem*>(treeItem);
  if (item == NULL)
    return NULL;
  
  QWidget* w = item->createEditor(this, parent, col);
  
 // mTree->setEditorWidget(w);

  return w;
}


void FileManagerDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit)
  {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }

  QComboBox* cbox = qobject_cast<QComboBox*>(editor);
  if (cbox)
  {
    QString txt = index.model()->data(index, Qt::EditRole).toString();
    int ci = cbox->findText(txt);
    if (ci != -1)
        cbox->setCurrentIndex(ci);
    else // Unknown, add it on the fly and choose it.
    {
      cbox->addItem(txt);
      cbox->setCurrentIndex(cbox->findText(txt));
    }
    qDebug() << "set value" << txt;   
  }
}
void FileManagerDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                          const QModelIndex &index) const
{
  int col = index.column();

  QTreeWidgetItem* treeItem = mTree->currentItem();

  FileManagerTreeItem* item = dynamic_cast<FileManagerTreeItem*>(treeItem);
  if (item == NULL)
    return;

  item->setModelData(this, model, index, editor, col);

  //mTree->setEditorWidget(NULL);
}


