#ifndef TABMODULEDIRECTIVES_H
#define TABMODULEDIRECTIVES_H

#include "util/CarbonPlatform.h"


#include <QTreeWidget>
#include <QList>
#include <QItemDelegate>

#include "Directives.h"
class DirectivesEditor;
class CarbonDirectiveGroup;

class TabModuleDirectives : public QTreeWidget
{
  Q_OBJECT

public:
  enum Column {colNAME=0, colOTHER};

  TabModuleDirectives(QWidget *parent);
  ~TabModuleDirectives();
  virtual void dragEnterEvent(QDragEnterEvent *event);
  virtual void dropEvent(QDropEvent *event);
  virtual void dragMoveEvent(QDragMoveEvent* event);
  void setDirectivesEditor(DirectivesEditor* editor) { mDirectivesEditor=editor; }
  void deleteDirectives();
  void populate(CarbonDirectiveGroup* group);
  void saveDocument();
  void setModified(bool value);
  bool selectItem(const char* net);
  void setDirective(const QStringList& netNames, const char* dirName, bool setIt);
  void newDirective();
  const QList<ModuleDirectives*> getDirectives();
  void addOrChange(const char* net, const char* dirName, const char* value);

private:
  void buildSelectionList(QList<QTreeWidgetItem*>& list);
  bool maybeSave();
  QTreeWidgetItem* addNewRow(const char* loc, bool exclusiveSelect=true);
  void addDirectiveRow(ModuleDirectives* dir);
  int findDirective(QTreeWidgetItem* item, const char* dirName);
  void addDirective(QTreeWidgetItem* item, const char* dirName);
  QTreeWidgetItem* findNet(const char* netName);

private:
  CarbonDirectiveGroup* mGroup;
  QList<ModuleDirectives*> mDirectives;
  DirectivesEditor* mDirectivesEditor;

  friend class TabModuleDirectivesDelegate;

};


class DlgSubsModule;

class TabModuleDirectivesDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    TabModuleDirectivesDelegate(QObject *parent = 0, TabModuleDirectives* table=0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
        const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
        const QModelIndex &index) const;

private slots:
    void currentIndexChanged(int);
    void commitAndCloseEditor();

private:
  TabModuleDirectives* tableWidget;
  DlgSubsModule* mDlgSubsModule;

};

#endif // TABMODULEDIRECTIVES_H
