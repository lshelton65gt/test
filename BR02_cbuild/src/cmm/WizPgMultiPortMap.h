#ifndef WIZPGMULTIPORTMAP_H
#define WIZPGMULTIPORTMAP_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "WizardPage.h"
#include "MemoryWizard.h"
#include <QWizardPage>
#include "ui_WizPgMultiPortMap.h"

class WizPgMultiPortMap : public WizardPage
{
  Q_OBJECT

public:
  enum PortMapColumns { colNAME=0, colFUNCTIONFANCY, colPORTINDEX };

  WizPgMultiPortMap(QWidget *parent = 0);
  ~WizPgMultiPortMap();

protected:
  virtual void initializePage();
  virtual bool validatePage();
  virtual bool isComplete() const;
  virtual int nextId() const;

  void populate(const QString& memgenFile);
  void readPreviousMappings(const QString& memgenFile);

private:
  QSettings mSettings;
  bool mIsValid;
  bool mHasParameters;
  Ui::WizPgMultiPortMapClass ui;
  QMap<QString, MWPortMap*> mPreviousMappings;

private slots:
    void on_checkBoxRemember_stateChanged(int);
};

#endif // WIZPGMULTIPORTMAP_H
