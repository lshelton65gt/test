#ifndef __CARBONFILES_H__
#define __CARBONFILES_H__

#include "util/CarbonPlatform.h"
#include <QObject>
#include "util/UtArray.h"
#include "util/UtString.h"
#include "util/XmlParsing.h"
#include "CarbonProjectWidget.h"
#include "CarbonProject.h"
#include "CarbonProjectTreeNode.h"
#include "CarbonOptions.h"

class CarbonFiles;

class CarbonFile : public QObject
{
  Q_OBJECT

public:
  CarbonFile(CarbonFiles* parent);
  CarbonFiles* getParent() const { return mParent; }
  bool serialize(xmlTextWriterPtr writer);
  static bool deserialize(CarbonFile* file, xmlNodePtr parent, UtXmlErrorHandler* eh);
  void putFilename(const char* fileName)
  {
    mRelativePath = fileName;
  }
  const char* getFilename() const { return mRelativePath.c_str(); }
  const char* getFilepath();

private:
  CarbonFiles* mParent;
  UtString mRelativePath;
  UtString mFilePath;
};

class CarbonFiles : public QObject
{
  Q_OBJECT

public:
  CarbonFiles(CarbonProject* project);
  bool serialize(xmlTextWriterPtr writer);
  bool deserialize(xmlNodePtr parent, UtXmlErrorHandler* eh);

  void addFile(CarbonFile* file)
  {
    mFiles.push_back(file);
  }
  void removeFile(CarbonFile* file)
  {
    mFiles.removeAll(file);
  }
  int numFiles() { return mFiles.count(); }
  CarbonFile* getFile(int index) { return mFiles.at(index); }
  CarbonProject* getProject() const { return mProject; }

private:
  CarbonProject* mProject;
  QList<CarbonFile*> mFiles;
};

class CarbonFileItem : public QObject, public CarbonProjectTreeNode
{
  Q_OBJECT

public:
  CarbonFileItem(CarbonProjectWidget* proj, CarbonFile* file, QTreeWidgetItem* parent);
  virtual ~CarbonFileItem();

  void updateIcon();

  virtual void singleClicked(int);
  virtual void doubleClicked(int);
  virtual void keyPressEvent(QKeyEvent*);
  virtual void showContextMenu(const QPoint& point);
 
  CarbonFile* getFile() { return mFile; }

public slots:
  void deleteFile();
  void runScript();
 
private slots:
  void fileNameChanged(const CarbonProperty*, const char*);

private:
  CarbonOptions* mOptions;
  CarbonProperties* mProperties;
  QAction* actionDelete;
  QAction* actionRunScript;
  CarbonFile* mFile;
};

class CarbonFilesFolder : public QObject, public CarbonProjectTreeNode
{
  Q_OBJECT

public:
  CarbonFilesFolder(CarbonFiles* files, CarbonProjectWidget* proj, QTreeWidgetItem* parent);
  virtual void showContextMenu(const QPoint&);
  void addFile(const char* fileName);
  void updateIcons();

public slots:
  void addFiles();

private:
  QAction* actionAddFiles;
  CarbonFiles* mFiles;

};

class TempChangeDirectory
{
public:
  TempChangeDirectory(const char* newDir)
  {
    OSGetCurrentDir(&mCurrDir);
    OSChdir(newDir, &mErr);
  }
  TempChangeDirectory(const QString& newDir)
  {
    UtString newdir;
    newdir << newDir;
    OSGetCurrentDir(&mCurrDir);
    OSChdir(newdir.c_str(), &mErr);
  }
  ~TempChangeDirectory()
  {
    OSChdir(mCurrDir.c_str(), &mErr);
  }

private:
  UtString mErr;
  UtString mCurrDir;
};

#endif
