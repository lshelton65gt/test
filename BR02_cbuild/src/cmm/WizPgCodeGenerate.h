#ifndef WIZPGCODEGENERATE_H
#define WIZPGCODEGENERATE_H
#include "util/CarbonVersion.h"
#include "gui/CQt.h"


#include <QWizardPage>
#include "WizardPage.h"
#include "ui_WizPgCodeGenerate.h"

class WizPgCodeGenerate : public WizardPage
{
  Q_OBJECT

public:
  WizPgCodeGenerate(QWidget *parent = 0);
  ~WizPgCodeGenerate();
  virtual void initializePage();

private:
  Ui::WizPgCodeGenerateClass ui;
};

#endif // WIZPGCODEGENERATE_H
