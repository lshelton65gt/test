//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "MemoryMapsWidget.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "SpiritXML.h"

#define TAB_ADDRESSBLOCK 0
#define TAB_BANK 1

#define TAG_MEMORYMAP 0
#define TAG_ADDRESSBLOCK 1
#define TAG_BANK 2
#define TAG_REGISTER 3

const char* MemoryMapsWidget::userFriendlyName()
{
  mFriendlyName.clear();

  OSConstructFilePath(&mFriendlyName,mCtx->getCarbonProjectWidget()->project()->getProjectDirectory(), "Memory Maps");
  return mFriendlyName.c_str();
}


const char* MemoryMapsWidget::strippedName(const char* fullFileName)
{
  static UtString x;
  x.clear();
  x << QFileInfo(fullFileName).fileName();
  return x.c_str();
}

const char* MemoryMapsWidget::userFriendlyCurrentFile()
{
  return "Memory Maps";
//  return strippedName(mCurFile.c_str());
}

MemoryMapsWidget::MemoryMapsWidget(CarbonMakerContext* ctx, MDIDocumentTemplate* t, QWidget *parent) : QWidget(parent)
{
  setAttribute(Qt::WA_DeleteOnClose);

  ui.setupUi(this);

  initializeMDI(this, t);
  mCtx = ctx;

  setAttribute(Qt::WA_DeleteOnClose);

  setWindowTitle("Memory Maps[*]");
}

bool MemoryMapsWidget::maybeSave()
{
  if (isWindowModified()) {
    QMessageBox::StandardButton ret;
    ret = QMessageBox::warning(this, MODELSTUDIO_TITLE,
      tr("'%1' has been modified.\n"
      "Do you want to save your changes?")
      .arg(userFriendlyCurrentFile()),
      QMessageBox::Save | QMessageBox::Discard
      | QMessageBox::Cancel);
    if (ret == QMessageBox::Save)
    {
      saveDocument();
      return true;
    }
    else if (ret == QMessageBox::Cancel)
      return false;
  }
  return true;
}

MemoryMapsWidget::~MemoryMapsWidget()
{
}

void MemoryMapsWidget::closeEvent(QCloseEvent *event)
{
  if (maybeSave())
  {
    event->accept();
    MDIWidget::closeEvent(this);
  }
  else
  {
    event->ignore();
  }
}

bool MemoryMapsWidget::loadFile(const QString& /*fileName*/)
{
  mCurFile.clear();
  
  populate();

  return true;
}

void MemoryMapsWidget::on_treeWidget_itemSelectionChanged()
{
  foreach (QTreeWidgetItem *item, ui.treeWidget->selectedItems())
  {
    QVariant qv = item->data(TAG_MEMORYMAP, Qt::UserRole);
    if (!qv.isNull())
    {
      SpiritMemoryMap* map = (SpiritMemoryMap*)qv.value<void*>();
      showMapUI(map);
      break;
    }
  } 
}

void MemoryMapsWidget::showMapUI(SpiritMemoryMap* map)
{
  if (map)
  {
    SpiritAddressBlock* ab = map->getAddressBlock();
    SpiritBank* bank = map->getBank();
    if (ab)
    {
      ui.tabWidget->setCurrentIndex(TAB_ADDRESSBLOCK);
    }
    else if (bank)
    {
      ui.tabWidget->setCurrentIndex(TAB_BANK);
    }
  }
}

void MemoryMapsWidget::populate()
{
  ui.treeWidget->clear();

  CarbonProject* proj = mCtx->getCarbonProjectWidget()->project();
  if (proj)
  {
    SpiritXML* spirit = proj->getSpiritXML();

    SpiritMemoryMaps* memMaps = spirit->getMemoryMaps();
    for (UInt32 i=0; i<memMaps->numItems(); i++)
    {
      SpiritMemoryMap* map = memMaps->getItem(i);
      QVariant qv = qVariantFromValue((void*)map);

      QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidget);
      item->setData(TAG_MEMORYMAP, Qt::UserRole, qv);

      if (map->getAddressBlock())
        addAddressBlock(NULL, map);
      else if (map->getBank())
        addBank(NULL, map);
    }
  }
}

void MemoryMapsWidget::addBank(QTreeWidgetItem* parent, SpiritMemoryMap* map)
{
  QTreeWidgetItem* item = NULL;
  if (parent)
    item = new QTreeWidgetItem(parent);
  else
    item = new QTreeWidgetItem(ui.treeWidget);

  SpiritBank* bank = map->getBank();
  item->setText(0, bank->getName());
  item->setText(1, "Bank");

  // Save data pointer
  QVariant qv = qVariantFromValue((void*)bank);
  item->setData(TAG_BANK, Qt::UserRole, qv);
}

void MemoryMapsWidget::addAddressBlock(QTreeWidgetItem* parent, SpiritMemoryMap* map)
{
  QTreeWidgetItem* item = NULL;
  if (parent)
    item = new QTreeWidgetItem(parent);
  else
    item = new QTreeWidgetItem(ui.treeWidget);

  SpiritAddressBlock* ab = map->getAddressBlock();
  item->setText(0, ab->getResolvedName());
  item->setText(1, "Address Block");

  // Save data pointer
  QVariant qv = qVariantFromValue((void*)ab);
  item->setData(TAG_ADDRESSBLOCK, Qt::UserRole, qv);


  // Add the registers (if any)
  addRegisters(item, ab);
}

void MemoryMapsWidget::addRegisters(QTreeWidgetItem* parent, SpiritAddressBlock* ab)
{
  for (UInt32 i=0; i<ab->numRegisters(); i++)
  {
    SpiritRegister* reg = ab->getRegister(i);

    QTreeWidgetItem* item = NULL;
    if (parent)
      item = new QTreeWidgetItem(parent);
    else
      item = new QTreeWidgetItem(ui.treeWidget);

    // Save data pointer
    QVariant qv = qVariantFromValue((void*)ab);
    item->setData(TAG_REGISTER, Qt::UserRole, qv);

    item->setText(1, reg->getName());

    //SpiritAddressBlock* ab = map->getAddressBlock();
    //item->setText(0, ab->getResolvedName());
    //item->setText(1, "Address Block")
  }
}


void MemoryMapsWidget::saveDocument()
{
  if (isWindowModified())
  {
    setWindowModified(false);
    setWidgetModified(false);
  }
  else
    qDebug() << "nothing to save in MemoryMaps";
}
