// -*-C++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef __MDIPACKAGETOOL_H__
#define __MDIPACKAGETOOL_H__

#include "util/CarbonPlatform.h"

#include "gui/CQt.h"

#include <QMenuBar>
#include <QProgressBar>
#include <QComboBox>

#include "util/UtString.h"
#include "Mdi.h"
#include "MdiProject.h"
#include "CarbonMakerContext.h"
class MDIWidget;

class MDIPackageToolTemplate : public MDIDocumentTemplate
{
  Q_OBJECT

public:
  MDIPackageToolTemplate(CarbonMakerContext* ctx);
 
  virtual MDIWidget* createNewDocument(QWidget* parent=0);
  virtual int createMenus();
  virtual int createToolbars();
  virtual void updateMenusAndToolbars(QWidget* widget);
  virtual MDIWidget* openDocument(QWidget* parent, const char* docName);

private:
  QAction* mActionDelete;
  QAction* mActionNewParam;

private:
  CarbonMakerContext* mContext;
};
#endif
