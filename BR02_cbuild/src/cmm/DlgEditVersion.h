#ifndef DLGEDITVERSION_H
#define DLGEDITVERSION_H


#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include "ModelKit.h"
#include <QDialog>
#include "ui_DlgEditVersion.h"

class DlgEditVersion : public QDialog
{
  Q_OBJECT

public:
  DlgEditVersion(QWidget *parent = 0, ModelKit* kit = 0, KitVersion* kv = 0);
  ~DlgEditVersion();

  QString getVendorVersion() { return ui.lineEditVendorVersion->text(); }
  bool getLocked() { return ui.checkBoxLocked->isChecked(); }
  QString getDescription() { return ui.lineEditDescription->text(); }
  QString getManifestName() { return ui.lineEditManifestName->text(); }

private:
  Ui::DlgEditVersionClass ui;

  private slots:
    void on_checkBoxLocked_stateChanged(int);
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

private:
  KitVersion* mVersion;
  ModelKit* mModelKit;
  QString mManifestName;
};

#endif // DLGEDITVERSION_H
