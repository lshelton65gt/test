#ifndef __CarbonProjectWidget_H__
#define __CarbonProjectWidget_H__
#include "util/CarbonPlatform.h"

#include <QtGui>

#ifndef MODELSTUDIO_TITLE
#define MODELSTUDIO_TITLE "Model Studio"
#endif

#define MODELWIZARD_TITLE "Remodeling Wizard"
#define MODELWIZARD_GENERATED "Wizard Generated"

#include "gui/CDragDropTreeWidget.h"
#include "CarbonProjectTreeNode.h"
#include "CarbonProject.h"
#include "ApplicationContext.h"
#include "Directives.h"

class CarbonProject;
class CarbonProjectWidget;
class CarbonRootItem;
class CarbonHDLFolderItem;
class CarbonMakerContext;
class CarbonModelItem;
class CarbonHDLSourceFile;
class CarbonSourceGroup;
class CarbonSourceItem;
class RemoteConsole;
class CarbonConsole;
class CarbonGenFilesFolder;
class CarbonProperty;
class CarbonRTLFolder;
class CarbonDirectivesFolder;
class HierarchyDockWindow;
class CowareComponent;
class MaxsimComponent;
class SystemCComponent;
class MVComponent;
class PackageComponent;
class DirectivesEditor;
class PackageToolWizardWidget;
class CarbonDirectives;
class MDIWidget;
class CarbonDatabaseFolder;
class CarbonFilesFolder;
class MemoryMapsItem;

struct CarbonMappedDrive
{
  UtString mDriveLetter;
  UtString mUnixPath;
};

class CarbonCursor
{
public:
  CarbonCursor(Qt::CursorShape shape=Qt::WaitCursor)
  {
    QApplication::setOverrideCursor(shape);
  }
  ~CarbonCursor()
  {
    QApplication::restoreOverrideCursor();
  }
};

// Class which holds the mapped drives
class CarbonMappedDrives
{
public:
  CarbonMappedDrives();
  ~CarbonMappedDrives()
  {
    foreach (CarbonMappedDrive* drive, mMappedDrives)
    {
      delete drive;
    }
  }

  void addMapping(const QString& driveLetter, const QString& unixPath)
  {
    INFO_ASSERT(driveLetter.length() == 2, "Drive letter must be two characters only");
    INFO_ASSERT(unixPath.length() > 0, "Unix path must not be empty");

    CarbonMappedDrive* drive = new CarbonMappedDrive();
    drive->mDriveLetter << driveLetter;
    drive->mUnixPath  << unixPath;
    mMappedDrives.push_back(drive);
  }

  void removeMapping(const QString& driveLetter)
  {
    for (int i=0; i<mMappedDrives.count(); i++)
    {
      CarbonMappedDrive* drive = mMappedDrives[i];
      if (driveLetter == drive->mDriveLetter.c_str())
      {
        mMappedDrives.removeAt(i);
        break;
      }
    }
  }

  bool isDriveLetterMapped(const QString& driveLetter)
  {
    INFO_ASSERT(driveLetter.length() == 2, "Drive letter must be two characters only");
    
    foreach (CarbonMappedDrive* drive, mMappedDrives)
    {
      if (driveLetter == drive->mDriveLetter.c_str())
        return true;
    }
    return false;
  }
  void saveMappings();
  int numMappings() { return mMappedDrives.count(); }
  
  CarbonMappedDrive* findMapping(const QString& driveLetter)
  {
    INFO_ASSERT(driveLetter.length() == 2, "Drive letter must be two characters only");
    foreach (CarbonMappedDrive* drive, mMappedDrives)
    {
      if (driveLetter == drive->mDriveLetter.c_str())
        return drive;
    }
    return NULL;
  }
  
  CarbonMappedDrive* getMapping(int index)
  {
    INFO_ASSERT(index < mMappedDrives.count(), "Invalid index");
    return mMappedDrives[index];
  }

private:
  QList<CarbonMappedDrive*> mMappedDrives;
};

class CarbonProjectWidget : public CDragDropTreeWidget
{
  Q_OBJECT

public:
  CarbonProjectWidget(QWidget* parent=0, CarbonMakerContext* ctx=0);
  virtual ~CarbonProjectWidget();

  enum ComponentType { eComponentCoWare, eComponentMaxsim, eComponentSystemC, eComponentModelValidation };
  enum BatchBuildType { eBatchBuild, eBatchRebuild, eBatchClean };

  const char* userFriendlyName()
  {
    return "Project Name Here";
  }
  void deleteSource();
  void setConsole(CarbonConsole* console);
  void newProject();
  bool loadProject(const char* fileName);
  QMenu* getContextMenuRoot();
  CarbonMakerContext* context() { return mContext; }
  CarbonProject* project() { return mProject; }
  void putProject(CarbonProject* newVal) { mProject = newVal; }
  DirectivesEditor* directivesEditor() { return mDirectivesEditor; }
  HierarchyDockWindow* getHierarchyWindow() const { return mHierarchyWindow; }
  void putDirectivesEditor(DirectivesEditor* dirEd) { mDirectivesEditor=dirEd; }
  CarbonFilesFolder* getFilesFolder() { return mFilesFolder; }
  bool isModified() { return mModified; }
  void putModified(bool newVal);
  bool saveProject();
  void closeProject();
  void properties();
  static QString makeRelativePath(const QString& fromPath, const QString& toPath);
  void putConfiguration(const char* name, bool retainContext = true);
  void putPlatform(const char* name);
  void fileProperties(CarbonSourceItem* item);
  void folderProperties(CarbonHDLFolderItem* folderItem);
  void compilerProperties();
  DirectivesEditor* compilerDirectives();
  void compile(bool cleanFlag, bool compilationFlag, bool packageFlag,const char* compileTarget=NULL, const char* cleanTarget=NULL, QStringList* args=NULL, const char* armKitRoot=NULL);
  void clean();
  bool check();
  QString getDbFilename();
  void removeSource(QTreeWidgetItem* item, CarbonHDLSourceFile* src);
  CarbonConsole* getConsole() { return mConsole; }
  MDIWidget* openSourceFile(const QString& filename, int lineNumber=-1, const char* docType=NULL);
  MDIWidget* openSource(const char* filename, int lineNumber=-1, const char* docType=NULL);
  MDIWidget* getOpenFile(const char* filename);

  static CarbonProject::HDLType hdlFileType(const char* file);
  void putContext(CarbonMakerContext* newVal) { mContext=newVal; }
  void moveUp();
  void moveDown();
  void moveSourceItem(CarbonHDLFolderItem* toFolder, CarbonSourceItem* srcItem);
  void putDockWidget(QDockWidget* newVal) { mDockWidget=newVal; }
  SystemCComponent* getActiveSystemCComponent();
  CowareComponent* getActiveCowareComponent();
  MaxsimComponent* getActiveMaxsimComponent();
  MVComponent* getActiveMVComponent();
  void deleteComponent(CarbonComponent* comp);
  static void removeDirectoryAndFiles(const char*);
  QDockWidget* getHierarchyDockWidget() const { return mHierarchyDockWidget; }
  CarbonSourceItem* findSource(CarbonHDLSourceFile* src);
  void stopCompilation();
  void updateContextMenuActions();
  CarbonRTLFolder* getRTLFolder() { return mRTL; }
  CarbonHDLFolderItem* findGroup(const char* groupName);
  void createComponent(ComponentType compType, const char* ccfgFile, bool checkDB=true);
  bool isRemoteCompilation();
  QString getCurrentSelection();
  void setCurrentSelection(const QString & value);
  void reopenWizards();
  bool isCompilationInProgress() {  return mCompilationInProgress; }
  void makeConsoleVisible();
  CarbonHDLFolderItem* findHDLFolderItem(CarbonSourceGroup* group);
  bool actionCopyConfiguration();
  CarbonProjectTreeNode* addRTLSource(const QString& folderName, const QString& file);
  CarbonComponent* createPackageComp();
  PackageToolWizardWidget* openPackageOptions();
  void updateVHMName();
  void closeWizards();

protected:
    //! Override mouse events in order to initiate drags
  virtual void mousePressEvent(QMouseEvent* e);
  virtual void mouseReleaseEvent(QMouseEvent* e);
  virtual void mouseMoveEvent(QMouseEvent* e);

  //! Override events in order to handle drops
  virtual void dragEnterEvent(QDragEnterEvent*);
  virtual void dragMoveEvent(QDragMoveEvent*);
  virtual void dropEvent(QDropEvent*);
  virtual void dragLeaveEvent(QDragLeaveEvent* event);
  virtual bool dragBeginEvent(QMouseEvent*);

  virtual void closeEvent(QCloseEvent* ev);

private slots:
  void showContextMenu(const QPoint &pos);
  void itemCollapsed(QTreeWidgetItem* item);
  void itemExpanded(QTreeWidgetItem* item);
  void itemDoubleClicked(QTreeWidgetItem* item, int column);
  void itemClicked(QTreeWidgetItem* item, int column);
  void compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void configurationChanged(const char*);
  void modelCompilationCompleted(CarbonConsole::CommandMode mode, CarbonConsole::CommandType type);
  void makefileCompleted(CarbonConsole::CommandMode mode, CarbonConsole::CommandType type);
  void exploreFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void itemSelectionChanged();
  void setWidgetFocus();
  void applySignalDirective(DesignDirective, const char*, bool);
  void restartCompile();

public slots:
  void navigateSource(const char*, int);
  void actionConfigurationManager();
  void actionProperties();
  void actionProjectProperties();
  void actionFileProperties();
  void outputFilenameChanged(const CarbonProperty*, const char*);
  void directiveFilesChanged(const CarbonProperty*, const char*);
  void actionAddSources();
  void actionAddLibrary();
  void actionCreateCoware();
  void actionCreateMaxsim();
  void actionCreateSystemC();
  void actionCreateMV();
  void actionImportCommandFile();
  void actionImportCcfgFile();
  void actionBatchBuild(const QStringList& configList, BatchBuildType type);
  void actionBuildPrecompiledHierarchy();
  void actionModelWizard();
  void actionPackageOptions();

signals:
  void projectClosing(CarbonProject*);
  void projectLoaded(const char*, CarbonProject*);
  void canCreateCoWare(bool value);
  void canCreateMaxsim(bool value);
  void canCreateSystemC(bool value);
  void canCreateMV(bool value);
  void canAddLibrary(bool value);
  void canAddSources(bool value);
  void projectModified(bool value);
  void directivesEditorOpened();

private:
  void populateMemoryMaps();
  void addCarbonFiles(CarbonFilesFolder* parent);
  void updateWidgets();
  void updateHierarchy(HierarchyMode mode, const char* iodbName);
  void populate();
  void createActions();
  void updateDirectiveNodes(CarbonDirectivesFolder*, const char*);
  void addFiles(CarbonSourceGroup* group, CarbonHDLFolderItem* parent);
  void addGroupsAndFiles(CarbonRTLFolder* parent);
  void setContextMenuNode(CarbonProjectTreeNode* node) { mContextMenuNode=node; }
  virtual void keyPressEvent ( QKeyEvent * event ); 
  void addGeneratedFiles(CarbonGenFilesFolder* parent);
  void populateVHM(CarbonGenFilesFolder* parent, const char* outputDir, const char* vhmName);
  void updatePropertiesDisplay();
  void readCompilerOutput();
  void readOutputFile(const char* fileName);
  void reopenWizard(CarbonComponent* comp, const char* compDocumentType, const char* wizardFile);
  bool composeFilename(const char* filename, UtString* composedName);
  QTreeWidgetItem* findChild(QTreeWidgetItem* parent, const QString& childName);
  void importCowareCcfg(QString filename);
  void importMaxsimCcfg(QString filename);
  void checkComponentIntegrities();

private:
  CarbonMakerContext* mContext;
  CarbonProject* mProject;
  CarbonRootItem* mRoot;
  CarbonHDLFolderItem* mHDLSources;
  CarbonModelItem* mVHM;
  MemoryMapsItem* mMemoryMaps;
  CarbonRTLFolder* mRTL;
  CarbonDirectivesFolder* mDirectives;
  CarbonDatabaseFolder* mDbFolder;
  CarbonFilesFolder* mFilesFolder;
  CarbonGenFilesFolder* mGenFolder;
  CarbonProjectTreeNode* mContextMenuNode;
  CarbonConsole* mConsole;
  CarbonProjectTreeNode* mDragItem;
  QDockWidget* mDockWidget;
  HierarchyDockWindow* mHierarchyWindow;
  DirectivesEditor* mDirectivesEditor;
  QDockWidget* mHierarchyDockWidget;
  bool mCompilationInProgress;

private: // actions
  QAction* mActionCreateCoware;
  QAction* mActionCreateMaxsim;
  QAction* mActionCreateSystemC;
  QAction* mActionAddSources;
  QAction* mActionAddLib;
  QAction* mActionProperties;
  QAction* mActionCreateMV;

  bool mModified; 
  bool mLoadingProject;
};

#endif
