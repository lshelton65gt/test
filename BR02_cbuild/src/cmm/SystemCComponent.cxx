//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include <QtGui>
#include <QDebug>
#include "gui/CQt.h"
#include "CarbonProjectWidget.h"
#include "CarbonComponents.h"
#include "SystemCComponent.h"
#include "CarbonOptions.h"
#include "InfoTableWidget.h"
#include "ErrorInfoWidget.h"
#include "DlgConfirmDelete.h"
#include "Mdi.h"
#include "MdiSystemC.h"
#include "SystemCWizardWidget.h"
#include "SettingsEditor.h"
#include "WorkspaceModelMaker.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "shell/carbon_misc.h"
#include "shell/carbon_capi.h"
#include "shell/CarbonDatabasePriv.h"
#include "iodb/IODBRuntime.h"
#include "util/DynBitVector.h"
#include "cfg/carbon_cfg.h"
#include "cfg/carbon_cfg_misc.h"
#include "cfg/CarbonCfg.h"

#include "ModelStudioCommon.h"
#include "CarbonDatabaseContext.h"
#include "CarbonMakerContext.h"
#include "cmm.h"
#include "ScrCcfg.h"

#define MODEL_64x64 "64-bit compile, 64-bit model"

class SystemCTool : public CarbonTool
{
public:
  CARBONMEM_OVERRIDES
  SystemCTool();

};

SystemCTool::SystemCTool() : CarbonTool(SYSTEMC_COMPONENT_NAME, ":cmm/Resources/SystemCTool.xml")
{
  mProperties.readPropertyDefinitions(":/cmm/Resources/SystemCComponentOptions.xml");
  mDefaultOptions = new CarbonOptions(NULL, getProperties(), "SystemCDefaults");
  mOptions = new CarbonOptions(mDefaultOptions, getProperties(), SYSTEMC_COMPONENT_NAME);
}

CarbonOptions* SystemCComponent::getOptions(const char* configName) 
{
  CarbonOptions* options = mActiveConfiguration->getOptions(SYSTEMC_COMPONENT_NAME);
  if (configName != NULL)
    options = getConfigurations()->findConfiguration(configName)->getOptions(SYSTEMC_COMPONENT_NAME);
  else
    configName = mActiveConfiguration->getName();

  QString dbname = getProject()->getConfigDatabaseFilePath(configName);
  UtString iodbName;
  iodbName << dbname;

  const char* outDir = getOutputDirectory(configName);

  UtString relIODBName;
  relIODBName << CarbonProjectWidget::makeRelativePath(outDir, iodbName.c_str());

  options->putValue("IODB File", relIODBName.c_str());
  options->putValue("Output Directory", outDir);

  return options;
}

void SystemCComponent::setComponentName(const QString& compName)
{
  qDebug() << "updating SystemCComponent component name" << compName;
}

int SystemCComponent::getSourceFiles(QStringList& fileList)
{
  int currSize = fileList.count();
  fileList << getWizardFilename();
  getGeneratedFiles(fileList);
  return fileList.count()-currSize;
}


ScrCcfg* SystemCComponent::getCcfg()
{
  QString iodbName = getProject()->getDesignFilePath(".symtab.db");
  QFileInfo fi(iodbName);
  QString ccfgDir = QString("%1/%2")
    .arg(getProject()->getActive()->getOutputDirectory())
    .arg(SYSTEMC_COMPONENT_SUBDIR);

  UtString ccfgFilePath;
  ccfgFilePath << ccfgDir << "/" << fi.baseName() << ".ccfg";

  ScrCcfg* ccfg = new ScrCcfg(this);
  ccfg->setFilePath(ccfgFilePath.c_str());
  CcfgEnum::CarbonCfgStatus status = ccfg->read(CcfgEnum::SystemC, ccfgFilePath.c_str());

  if (status != CcfgEnum::Failure)
    return ccfg;
  else
    return NULL;
}

int SystemCComponent::getGeneratedFiles(QStringList& fileList)
{
  const char* iodbName = getProject()->getDesignFilePath(".symtab.db");
  QFileInfo fi(iodbName);
  
  fileList.push_back(QString("%1/%2.systemc.h").arg(getOutputDirectory()).arg(fi.baseName()));
  fileList.push_back(QString("%1/%2.systemc.cpp").arg(getOutputDirectory()).arg(fi.baseName()));

  return 2; 
}

const char* SystemCComponent::getOutputDirectory(const char* cfgName)
{
  mOutputDir.clear();

  CarbonProject* proj = getProject();

  if (cfgName == NULL && proj && proj->getActive())
    cfgName = getProject()->getActive()->getName();

  if (mActiveConfiguration == NULL && mConfigs)
    mActiveConfiguration = mConfigs->findConfiguration(cfgName);

  if (mActiveConfiguration)
  {
    const char* platform = mActiveConfiguration->getPlatform();

    OSConstructFilePath(&mOutputDir, getProject()->getProjectDirectory(), platform);
    OSConstructFilePath(&mOutputDir, mOutputDir.c_str(), cfgName);
    OSConstructFilePath(&mOutputDir, mOutputDir.c_str(), SYSTEMC_COMPONENT_SUBDIR);
  }
  else
    qDebug() << "Error: NO active configuration";

  return mOutputDir.c_str(); 
}

const char* SystemCComponent::getComponentDirectory() const {
  return SYSTEMC_COMPONENT_SUBDIR;
}

UInt32 SystemCComponent::numPackageFiles() {
  return mPackageFiles.count();
}

QString SystemCComponent::getPackageFile(UInt32 index) {
  INFO_ASSERT(index < numPackageFiles(), "Index out of range.");
  return mPackageFiles[index];
}

void SystemCComponent::setEnabled(bool enable)
{
  if (mRootItem)
    mRootItem->setDisabled(!enable);
}

void SystemCComponent::icheckComponent()
{
  bool status = checkComponent(getProject()->getProjectWidget()->getConsole());

  if (status)
  {
    QDockWidget* dw = getProject()->getProjectWidget()->context()->getWorkspaceModelMaker()->getErrorInfoDockWindow();
    // lower followed by raise causes it to "pop" forward
    dw->lower();
    dw->raise();
    getProject()->getProjectWidget()->context()->getMainWindow()->statusBar()->showMessage(tr("Problems found"), 2000);
  }
  else
    QMessageBox::information(NULL, MODELSTUDIO_TITLE,
      tr("No Errors or Warnings were found during pre-compile check(s)."), QMessageBox::Ok);
}

// Check Registers and Memories for 32 bit widths.
bool SystemCComponent::checkRegisters(CarbonConsole* console)
{
  bool errorsFound = false;

  CarbonConfiguration* activeConfig = mActiveConfiguration;

  const char* outDir = getOutputDirectory(activeConfig->getName());
  const char* iodbName = getProject()->getConfigDesignFilePath(activeConfig->getName(), ".symtab.db");

  TempChangeDirectory cd(outDir);

  QFileInfo fi(iodbName);
  if (!fi.exists())
    return false;

  UtString relIODBName;
  relIODBName << CarbonProjectWidget::makeRelativePath(outDir, iodbName);

  UtString ccfgPath;
  UtString ccfgName;
  ccfgName << fi.baseName() << ".ccfg";

  OSConstructFilePath(&ccfgName, outDir, ccfgName.c_str());

  CarbonCfgID cfg = carbonCfgCreate();

  CarbonDB* newDB = carbonDBOpenFile(iodbName);
  cfg->putDB(newDB);

  CarbonCfgStatus xtorStatus = carbonCfgReadXtorLib(cfg, eCarbonXtorsSystemC);
  if (xtorStatus != eCarbonCfgSuccess)
   console->processOutput(CarbonConsole::Local, CarbonConsole::Check, carbonCfgGetErrmsg(cfg));
   

  // read any user transactor defs
  const char* xtorDefs = getProject()->getProjectWidget()->context()->getQtContext()->getXtorDefFile();
  if (xtorDefs)
    carbonCfgReadXtorDefinitions(cfg, xtorDefs);

  QFileInfo fiCfg(ccfgName.c_str());
  if (fiCfg.exists())
  {
    CarbonCfgStatus cfgStatus = carbonCfgRead(cfg, ccfgName.c_str());
    if (cfgStatus == eCarbonCfgSuccess)
    {
      // Check Registers   
      for (UInt32 ri=0; ri<carbonCfgNumRegisters(cfg); ri++)
      {
        CarbonCfgRegisterID reg = carbonCfgGetRegister(cfg, ri);
        UInt32 regWidth = carbonCfgRegisterGetWidth(reg);
        if (regWidth != 32 && regWidth != 64)
        {
          errorsFound = true;
          UtString msg;
          msg << "Error 1: SystemC Registers must be 32 or 64 bits only: ";
          msg << carbonCfgRegisterGetName(reg) << " is " << regWidth << " bit(s)";
          console->processOutput(CarbonConsole::Local, CarbonConsole::Check, msg.c_str());
        }
      }
      
      // Check Memories
      for (UInt32 mi=0; mi<carbonCfgNumMemories(cfg); mi++)
      {
        CarbonCfgMemoryID mem = carbonCfgGetMemory(cfg, mi);
        UInt32 memWidth = carbonCfgMemoryGetWidth(mem);
        if (memWidth != 32 && memWidth != 64)
        {
          errorsFound = true;
          UtString msg;
          msg << "Error 1: SystemC Memories must be 32 or 64 bits only: ";
          msg << carbonCfgMemoryGetName(mem) << " is " << memWidth << " bit(s)";
          console->processOutput(CarbonConsole::Local, CarbonConsole::Check, msg.c_str());
        }
      }
    }
  }

  carbonCfgDestroy(cfg);

  return errorsFound;
}
bool SystemCComponent::checkComponent(CarbonConsole* console)
{
  bool errorFlag = false;
  qDebug() << "Checking SystemC Component";

  CarbonOptions* options = getProject()->getToolOptions("VSPCompiler");
  QString modelCompilation = options->getValue("Compiler Control")->getValue();
  if (modelCompilation == MODEL_64x64)
  {

    console->processOutput(CarbonConsole::Local, CarbonConsole::Check, 
                            "64 Bit Target Architectures are not Supported for SystemC Components");
    return false;
  }
 

  const char* iodbName = getProject()->getDesignFilePath(".symtab.db");
  QFileInfo fi(iodbName);  
  if (!fi.exists())
    return false;

  // Check SystemC Env
  if (getenv("SYSTEMCHOME") == NULL && getenv("SYSTEMCHOMEIPDIR") == NULL)
  {
    UtString msg;
    msg << "Error 2: EnvCheck: SYSTEMCHOME or SYSTEMCHOMEIPDIR environment variable must be defined";
    console->processOutput(CarbonConsole::Local, CarbonConsole::Check, msg.c_str());
    errorFlag = true;
  }

  if (checkRegisters(console))
    errorFlag = true;

  // Check for "Top Module"
  const char* topModule = getOptions()->getValue("Top Module")->getValue();
  if (topModule == NULL || (topModule && strlen(topModule) == 0))
  {
    UtString msg;
    msg << "Error 3: SystemC: The 'Top Module' property is not defined for the component";
    console->processOutput(CarbonConsole::Local, CarbonConsole::Check, msg.c_str());
    errorFlag = true;
  }
  

  return errorFlag;
}

SystemCComponent::SystemCComponent(CarbonProject* proj) :CarbonComponent(proj, "SystemCComponent")
{
  setFriendlyName("SystemC");
  setModelKitComponent(true);

  mConfigs = new CarbonConfigurations(proj);
  mRootItem = NULL;
  mActiveConfiguration = NULL;
  mCcfgItem = NULL;
  mHeaderItem = NULL;
  mCppItem = NULL;

  mCfg = 0;
  mDB = NULL;

  mIODBName = "libdesign.symtab.db";

  CarbonConfigurations* configs = proj->getConfigurations();
  for (UInt32 i=0; i<configs->numConfigurations(); i++)
  {
    CarbonConfiguration* projConfig = configs->getConfiguration(i);
    const char* configName = projConfig->getName();

    CarbonConfiguration* config = mConfigs->addConfig("Linux", configName);

    if (mActiveConfiguration == NULL)
      mActiveConfiguration = mConfigs->findConfiguration(proj->getActiveConfiguration());

    config->addTool(new SystemCTool()); 
    createDirectory(config);
  }

  CQT_CONNECT(proj, configurationChanged(const char*), this, configurationChanged(const char*));
  CQT_CONNECT(proj, configurationRenamed(const char*, const char*), this, configurationRenamed(const char*, const char*));
  CQT_CONNECT(proj, configurationCopy(const char*, const char*), this, configurationCopy(const char*, const char*));
  CQT_CONNECT(proj, configurationRemoved(const char*), this, configurationRemoved(const char*));
  CQT_CONNECT(proj, configurationAdded(const char*), this, configurationAdded(const char*));
  CQT_CONNECT(proj, projectLoaded(const char*,CarbonProject*), this, projectLoaded(const char*,CarbonProject*));

  CQT_CONNECT(getProject()->getProjectWidget(), projectClosing(CarbonProject*), this, projectClosing(CarbonProject*));

  CQT_CONNECT(proj->getProjectWidget()->getConsole(), compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType));
  CQT_CONNECT(proj->getProjectWidget()->getConsole(), compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType));
  CQT_CONNECT(proj->getProjectWidget()->getConsole(), makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType));

 // Only register during creates, not deserialization
  if (proj->getActive())
  {
    CarbonOptions* options = proj->getToolOptions("VSPCompiler");
    options->registerPropertyChanged("-o", "outputFilenameChanged", this);
  }
}

void SystemCComponent::projectClosing(CarbonProject* proj)
{
  disconnect(proj, SIGNAL(configurationChanged(const char*)), this, SLOT(configurationChanged(const char*)));

  disconnect(proj, SIGNAL(configurationRenamed(const char*, const char*)), this, SLOT(configurationRenamed(const char*, const char*)));
  disconnect(proj, SIGNAL(configurationCopy(const char*, const char*)), this,  SLOT(configurationCopy(const char*, const char*)));
  disconnect(proj, SIGNAL(configurationRemoved(const char*)), this,  SLOT(configurationRemoved(const char*)));
  disconnect(proj, SIGNAL(configurationAdded(const char*)), this,  SLOT(configurationAdded(const char*)));
  disconnect(proj, SIGNAL(projectLoaded(const char*,CarbonProject*)), this,  SLOT(projectLoaded(const char*,CarbonProject*)));

  disconnect(proj->getProjectWidget(), SIGNAL(projectClosing(CarbonProject*)), this,  SLOT(projectClosing(CarbonProject*)));

  disconnect(proj->getProjectWidget()->getConsole(), SIGNAL(compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)), this,  SLOT(compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)));
  disconnect(proj->getProjectWidget()->getConsole(), SIGNAL(compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType)), this,  SLOT(compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType)));
  disconnect(proj->getProjectWidget()->getConsole(), SIGNAL(makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType)), this, SLOT( makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType)));

}

// if we change the design name, we need to copy over the
// filename, then fix up the database reference to the new name
void SystemCComponent::outputFilenameChanged(const CarbonProperty*, const char* newValue)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  QFileInfo fiNew(newValue);
  QFileInfo fiOld(mIODBName.c_str());

  UtString newCcfgName;
  newCcfgName << fiNew.baseName() << ".ccfg";

  UtString oldCcfgName;
  oldCcfgName << fiOld.baseName() << ".ccfg";

  UtString newName;
  OSConstructFilePath(&newName, getOutputDirectory(), newCcfgName.c_str());

  UtString oldName;
  OSConstructFilePath(&oldName, getOutputDirectory(), oldCcfgName.c_str());

  copyAndRenameCcfg(getOutputDirectory(), oldName.c_str(), newName.c_str());

  updateTreeIcons();

  QApplication::restoreOverrideCursor();

}

void SystemCComponent::configurationAdded(const char* name)
{
  qDebug() << "Adding SystemCComponent Configuration: " << name;
  CarbonConfiguration* cfg = getConfigurations()->findConfiguration(name);
  if (cfg == NULL)
  {
    cfg = mConfigs->addConfig(mActiveConfiguration->getPlatform(), name);
    cfg->addTool(new SystemCTool());
    createDirectory(cfg);
  }
}

void SystemCComponent::configurationRenamed(const char* oldName, const char* newName)
{
  CarbonConfiguration* oldConfig = mConfigs->findConfiguration(oldName);
  INFO_ASSERT(oldConfig, "Expecting Configuration");
  oldConfig->putName(newName);
  renameDirectory(oldName, newName);
  updateTreeIcons();
}

bool SystemCComponent::importCcfg(QString srcCcfgFile, bool checkDB)
{
  bool status = true;

  UtString srcFile;
  srcFile << srcCcfgFile;
  
  QFileInfo srcFileInfo(srcFile.c_str());

  UtString absPath;
  absPath << srcFileInfo.absolutePath();

  UtString absFilePath;
  absFilePath << srcFileInfo.absoluteFilePath();

  // change dir to the source file
  TempChangeDirectory temp1(absPath.c_str());

  CarbonCfgID cfg = carbonCfgCreate();
  CarbonCfgStatus xtorStatus = carbonCfgReadXtorLib(cfg, eCarbonXtorsSystemC);
  if (xtorStatus != eCarbonCfgSuccess)
  {
    CarbonConsole* console = getProject()->getProjectWidget()->getConsole();
    console->processOutput(CarbonConsole::Local, CarbonConsole::Check, carbonCfgGetErrmsg(cfg));
  }

  // read any user transactor defs
  const char* xtorDefs = getProject()->getProjectWidget()->context()->getQtContext()->getXtorDefFile();
  if (xtorDefs)
    carbonCfgReadXtorDefinitions(cfg, xtorDefs);

  CarbonCfgStatus cfgStatus;
  
  if (checkDB)
    cfgStatus = cfg->read(absFilePath.c_str());  
  else
    cfgStatus = cfg->readNoCheck(absFilePath.c_str());  

  if (cfgStatus != eCarbonCfgFailure)
  {
    // copy the filename (already validated for existence)
    // to the current active configuration
    const char* iodbName = getProject()->getDesignFilePath(".symtab.db");
    QFileInfo fi(iodbName);  
    UtString ccfgName;

    ccfgName << fi.baseName() << ".ccfg";

    UtString newName;
    OSConstructFilePath(&newName, getOutputDirectory(), ccfgName.c_str());

    // change dir to the source file
    TempChangeDirectory temp2(getOutputDirectory());

    UtString relIODBName;
    relIODBName << CarbonProjectWidget::makeRelativePath(getOutputDirectory(), iodbName);
   
    carbonCfgPutIODBFile(cfg, relIODBName.c_str());

    CarbonConfiguration* activeConfig = mActiveConfiguration;

    UtString relLibName;
    const char* outputFilename = getProject()->getToolOptions(activeConfig->getName(), "VSPCompiler")->getValue("-o")->getValue();
    QFileInfo ofi(outputFilename);
    UtString libExt;
    libExt << "." << ofi.completeSuffix();

    const char* libFilePath = getProject()->getConfigDesignFilePath(activeConfig->getName(), libExt.c_str());
    relLibName << CarbonProjectWidget::makeRelativePath(getOutputDirectory(), libFilePath);
    carbonCfgPutLibName(cfg, relLibName.c_str());

    const char* topModule = getOptions(activeConfig->getName())->getValue("Top Module")->getValue();
    if (topModule == NULL || (topModule && strlen(topModule) == 0))
    {
      CarbonDatabaseContext* dbContext = getProject()->getDbContext();
      if (dbContext != NULL && dbContext->getDB() != NULL)
      {
        topModule = carbonDBGetTopLevelModuleName(dbContext->getDB());
        getOptions(activeConfig->getName())->putValue("Top Module", topModule);
      }
      else // get the value from the existing one
        getOptions(activeConfig->getName())->putValue("Top Module", carbonCfgGetTopModuleName(cfg));
    }

    getOptions(activeConfig->getName())->putValue("Module Name", carbonCfgGetCompName(cfg));

    carbonCfgWrite(cfg, newName.c_str());
  }
  else
    status = false;

  updateTreeIcons();
  
  if (!status)
  {
    UtString msg;
    msg << "The import has failed, please check the existance of the .symtab.db file\n";
    msg << "that is referenced by this .ccfg file.\n";
    msg << cfg->getErrmsg();
    QMessageBox::critical(getProject()->getProjectWidget(), MODELSTUDIO_TITLE, msg.c_str());
  }

  carbonCfgDestroy(cfg);

  return status;
}


void SystemCComponent::configurationCopy(const char* oldCfgName, const char* newCfgName)
{
  QFileInfo oldfi(getProject()->getConfigDesignFilePath(oldCfgName, ".symtab.db"));  
  QFileInfo newfi(getProject()->getConfigDesignFilePath(newCfgName, ".symtab.db"));  

  UtString oldCcfgName;
  oldCcfgName << oldfi.baseName() << ".ccfg";

  UtString newCcfgName;
  newCcfgName << newfi.baseName() << ".ccfg";

  UtString newName;
  OSConstructFilePath(&newName, getOutputDirectory(newCfgName), newCcfgName.c_str());

  UtString oldName;
  OSConstructFilePath(&oldName, getOutputDirectory(oldCfgName), oldCcfgName.c_str());
  
  UtString errInfo;
  OSCopyFile(oldName.c_str(), newName.c_str(), &errInfo);

  CarbonOptions* oldOptions = getOptions(oldCfgName);
  CarbonOptions* newOptions = getOptions(newCfgName);

  oldOptions->setBlockSignals(true);
  newOptions->setBlockSignals(true);

  for (CarbonOptions::OptionsLoop p = oldOptions->loopOptions(); !p.atEnd(); ++p)
  {
    CarbonPropertyValue* sv = p.getValue();
    const char* value = sv->getValue();
    const char* propName = sv->getProperty()->getName();
    qDebug() << "copying from " << oldCfgName << " " << propName << " = " << value << " to " << newCfgName;
    newOptions->putValue(propName, sv->getProperty()->getDefaultValue());
    newOptions->putValue(propName, value);
  }

  oldOptions->setBlockSignals(false);
  newOptions->setBlockSignals(false);

  updateTreeIcons();
}

void SystemCComponent::configurationRemoved(const char* name)
{
  qDebug() << "Removing SystemCComponent Configuration: " << name;
  mConfigs->removeConfiguration(name);
}

void SystemCComponent::configurationChanged(const char* newConfig)
{
  qDebug() << "SystemC Component Config changed to: " << newConfig;

  CarbonOptions* options = getOptions(newConfig);

  qDebug() << "top module: " << options->getValue("Top Module")->getValue();

  mActiveConfiguration = mConfigs->findConfiguration(newConfig);
  INFO_ASSERT(mActiveConfiguration, "Unable to locate configuration");
  createDirectory(mActiveConfiguration);
  updateTreeIcons();
}

void SystemCComponent::renameDirectory(const char* oldName, const char* newName)
{
  UtString oldDir;
  UtString newDir;

  OSConstructFilePath(&oldDir, getProject()->getProjectDirectory(), mActiveConfiguration->getPlatform());
  OSConstructFilePath(&oldDir, oldDir.c_str(), oldName);
  OSConstructFilePath(&oldDir, oldDir.c_str(), SYSTEMC_COMPONENT_SUBDIR);

  OSConstructFilePath(&newDir, getProject()->getProjectDirectory(), mActiveConfiguration->getPlatform());
  OSConstructFilePath(&newDir, newDir.c_str(), newName);
  OSConstructFilePath(&newDir, newDir.c_str(), SYSTEMC_COMPONENT_SUBDIR);

  QDir dir;
  if (!dir.rename(oldDir.c_str(), newDir.c_str()))
    qDebug() << "failed to rename directory!";
}

// Create the directory for the configuration
void SystemCComponent::createDirectory(CarbonConfiguration* config)
{
  UtString outDir;

  OSConstructFilePath(&outDir, config->getProject()->getProjectDirectory(), config->getPlatform());
  OSConstructFilePath(&outDir, outDir.c_str(), config->getName());
  OSConstructFilePath(&outDir, outDir.c_str(), SYSTEMC_COMPONENT_SUBDIR);

  QDir q(outDir.c_str());
  if (!q.exists())
  {
    qDebug() << "Creating directory: " << outDir.c_str();
    q.mkpath(outDir.c_str());
  }
}

SystemCComponent::~SystemCComponent()
{
  CarbonOptions* options = getProject()->getToolOptions("VSPCompiler");
  options->unregisterPropertyChanged("-o", this);
}

void SystemCComponent::createConfigurations(SystemCComponent* comp, xmlNodePtr parent)
{
  // first make sure the configurations exist for later deserialization
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "Configurations"))
    {
      for (xmlNodePtr configChild = child->children; configChild != NULL; configChild = configChild->next) 
      {
        if (XmlParsing::isElement(configChild, "Configuration"))
        {
          UtString configName;
          XmlParsing::getProp(configChild, "Name", &configName);
          QString cname = configName.c_str();
          QStringList configParts = cname.split('|');

          QString qname = configParts[0];
          QString qplat = configParts[1];

          UtString name;
          name << qname;
          UtString platform;
          platform << qplat;

          CarbonConfiguration* cfg = comp->getConfigurations()->findConfiguration(name.c_str());
          if (cfg == NULL)
          {
            comp->getConfigurations()->addConfig(name.c_str(), platform.c_str());
            cfg->addTool(new SystemCTool());
            createDirectory(cfg);
          }
        }
      }
    }
  }
}

SystemCComponent* SystemCComponent::deserialize(CarbonProject* proj, xmlNodePtr parent, UtXmlErrorHandler* eh)
{
  SystemCComponent* comp = NULL;

  // Is this a SystemCComponent?
  if (XmlParsing::isElement(parent, "SystemCComponent"))
  {
    comp = new SystemCComponent(proj);
  
    createConfigurations(comp, parent);

    for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
    {
      if (XmlParsing::isElement(child, "Configurations"))
        comp->getConfigurations()->deserialize(child, eh);
    }
  }

  return comp;
}

bool SystemCComponent::serialize(xmlTextWriterPtr writer)
{
  xmlTextWriterStartElement(writer, BAD_CAST "SystemCComponent");

  mConfigs->serialize(writer);

  xmlTextWriterEndElement(writer);
  
  return true;
}

void SystemCComponent::projectLoaded(const char*, CarbonProject* proj)
{
  CarbonOptions* options = proj->getToolOptions("VSPCompiler");
  options->registerPropertyChanged("-o", "outputFilenameChanged", this);
}

CarbonComponentTreeItem* SystemCComponent::createTreeItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent)
{
  SystemCTreeItem* item = NULL;

  if (parent)
    item = new SystemCTreeItem(proj, this, parent);
  else
  {
    item = new SystemCTreeItem(proj, this);
    proj->addTopLevelItem(item);
  }
  item->setText(0, SYSTEMC_COMPONENT_DESCRIPTION);

  mCcfgItem = new SystemCCcfgItem(proj, item, this);
  mHeaderItem = new SystemCSourceItem(proj, item, this);
  mCppItem = new SystemCSourceItem(proj, item, this);
 

  updateTreeTextValues();

  item->setExpanded(true);

  mRootItem = item;

  updateTreeIcons();

  return item;
}

void SystemCComponent::compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  setEnabled(false);
}

void SystemCComponent::makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
}

void SystemCComponent::compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  setEnabled(true);
  updateTreeIcons();
}

const char* SystemCComponent::getTargetName(CarbonConfiguration*,TargetType type, TargetPlatform)
{
  static UtString targetName;
  targetName.clear();

  switch (type)
  {
  case CarbonComponent::Build: targetName << SYSTEMC_COMPONENT_TARGET; break;
  case CarbonComponent::Package: break;
  case CarbonComponent::Clean: targetName << SYSTEMC_COMPONENT_TARGET_CLEAN; break;
  }

  return targetName.c_str();
}

void SystemCComponent::updateTreeTextValues()
{
  if (!mActiveConfiguration)
    return;

  const char* iodbName = getProject()->getDesignFilePath(".symtab.db");
  QFileInfo fi(iodbName);  
  UtString SystemCName;
  bool bCanCompile = canCompile(mActiveConfiguration);

  SystemCName << fi.baseName() << ".ccfg";
  if (mCcfgItem)
  {
    mCcfgItem->setText(0, SystemCName.c_str());
    mCcfgItem->setExists(bCanCompile);
  }

  //mRootItem->setDisabled(!bCanCompile);

  mPackageFiles.clear();

  SystemCName.clear();
  SystemCName << fi.baseName() << ".systemc.h";
  if (mHeaderItem)
    mHeaderItem->setText(0, SystemCName.c_str());
  mPackageFiles << SystemCName.c_str();

  SystemCName.clear();
  SystemCName << fi.baseName() << ".systemc.cpp";
  if (mCppItem)
    mCppItem->setText(0, SystemCName.c_str());
  mPackageFiles << SystemCName.c_str();
 

  CarbonDatabaseContext* dbContext = getProject()->getDbContext();
  UtString interfaceName;
  if (dbContext != NULL && dbContext->getDB() != NULL)
    interfaceName << carbonDBGetInterfaceName(dbContext->getDB());
  else
  {
    UtString tempName;
    tempName << fi.baseName();
    interfaceName = tempName.substr(3);
  }
}
// Refresh the icons for SystemCSourceItems
void SystemCComponent::updateTreeIcons()
{
  const char* iodbName = getProject()->getDesignFilePath(".symtab.db");
  QFileInfo fi(iodbName);  
  mIODBName = iodbName;

  updateTreeTextValues();

  // Update each node
  if (mRootItem)
  {
    const char* od = getOutputDirectory();
    for (int i=0; i<mRootItem->childCount(); i++)
    {
      QTreeWidgetItem* item = mRootItem->child(i);
      SystemCSourceItem* srcItem = dynamic_cast<SystemCSourceItem*>(item);
      if (srcItem)
      {
        UtString fpath;
        UtString name;
        name << srcItem->text(0);
        OSConstructFilePath(&fpath, od, name.c_str());
        QFile file(fpath.c_str());
        srcItem->setExists(file.exists());
      }
    }
  }
}

void SystemCComponent::generateMakefile(CarbonConfiguration* cfg, const char* /*makefilePath*/)
{
  const char* outDir = getOutputDirectory(cfg->getName());
  UtString configName;
  OSConstructFilePath(&configName, outDir, "configuration");
  QString buffer;
  QTextStream stream(&buffer);
  stream.setCodec("UTF-8");

  stream << getOptions(cfg->getName())->getValue("Top Module")->getValue() << endl;
  const char* iodbName = getProject()->getConfigDesignFilePath(cfg->getName(), ".symtab.db");
  QFileInfo fi(iodbName);

  UtString relIODBName;
  relIODBName << CarbonProjectWidget::makeRelativePath(outDir, iodbName);
  stream << relIODBName.c_str() << endl;

  stream << getProject()->getToolOptions(cfg->getName(), "VSPCompiler")->getValue("-o")->getValue() << endl;

  if (getProject()->writeFileIfChanged(configName.c_str(), &stream))
    updateCcfg(cfg, iodbName);
}

void SystemCComponent::updateCcfg(CarbonConfiguration* activeConfig, const char* iodbName)
{
  const char* outDir = getOutputDirectory(activeConfig->getName());

  TempChangeDirectory cd(outDir);

  QFileInfo fi(iodbName);
  if (!fi.exists())
    return;

  qDebug() << "Writing SystemC .ccfg file";

  UtString relIODBName;
  relIODBName << CarbonProjectWidget::makeRelativePath(outDir, iodbName);

  UtString ccfgPath;
  UtString ccfgName;
  ccfgName << fi.baseName() << ".ccfg";

  OSConstructFilePath(&ccfgName, outDir, ccfgName.c_str());

  CarbonCfgID cfg = carbonCfgCreate();

  CarbonDB* newDB = carbonDBOpenFile(iodbName);
  cfg->putDB(newDB);

  CarbonCfgStatus xtorStatus = carbonCfgReadXtorLib(cfg, eCarbonXtorsSystemC);
  if (xtorStatus != eCarbonCfgSuccess)
  {
    CarbonConsole* console = getProject()->getProjectWidget()->getConsole();
    console->processOutput(CarbonConsole::Local, CarbonConsole::Check, carbonCfgGetErrmsg(cfg));
  }

  const char* topModule = getOptions(activeConfig->getName())->getValue("Top Module")->getValue();
  if (topModule == NULL || (topModule && strlen(topModule) == 0))
  {
    CarbonDatabaseContext* dbContext = getProject()->getDbContext();
    if (dbContext != NULL && dbContext->getDB() != NULL)
    {
      topModule = carbonDBGetTopLevelModuleName(dbContext->getDB());
      getOptions(activeConfig->getName())->putValue("Top Module", topModule);
    }
  }
  
  // update the library reference (relative)
  UtString relLibName;
  const char* outputFilename = getProject()->getToolOptions(activeConfig->getName(), "VSPCompiler")->getValue("-o")->getValue();
  QFileInfo ofi(outputFilename);
  UtString libExt;
  libExt << "." << ofi.completeSuffix();

  const char* libFilePath = getProject()->getConfigDesignFilePath(activeConfig->getName(), libExt.c_str());
  relLibName << CarbonProjectWidget::makeRelativePath(outDir, libFilePath);
  carbonCfgPutLibName(cfg, relLibName.c_str());

  carbonCfgPutTopModuleName(cfg, topModule);
  carbonCfgPutCcfgMode(cfg, eCarbonCfgSYSTEMC);

  // read any user transactor defs
  const char* xtorDefs = getProject()->getProjectWidget()->context()->getQtContext()->getXtorDefFile();
  if (xtorDefs)
    carbonCfgReadXtorDefinitions(cfg, xtorDefs);

  carbonCfgRead(cfg, ccfgName.c_str());  
  carbonCfgPutIODBFile(cfg, relIODBName.c_str());

  const char* compName = getOptions()->getValue("Module Name")->getValue();
  if (compName == NULL || (compName && strlen(compName) == 0))
    carbonCfgPutCompName(cfg, topModule);
  else 
    carbonCfgPutCompName(cfg, compName);

  carbonCfgWrite(cfg, ccfgName.c_str());
  carbonCfgDestroy(cfg);
}

bool SystemCComponent::getTargetImpl(CarbonConfiguration* cfg, QTextStream& stream, TargetType type)
{
  UtString generatedOutputName;
  const char* outDir = getOutputDirectory(cfg->getName());

  const char* iodbName = getProject()->getConfigDesignFilePath(cfg->getName(), ".symtab.db");
  QFileInfo fi(iodbName);

  UtString SystemCName;
  SystemCName << fi.baseName();

  generatedOutputName << SYSTEMC_COMPONENT_SUBDIR << "/" << SystemCName.c_str() << ".systemc.cpp" << " " << SYSTEMC_COMPONENT_SUBDIR << "/" << SystemCName.c_str() << ".systemc.h";

  switch (type)
  {
  case CarbonComponent::Build:
    {
      UtString ccfgPath;
      UtString ccfgName;
      ccfgName << fi.baseName() << ".ccfg";

      stream << getTargetName(cfg, CarbonComponent::Build) << ": " << generatedOutputName.c_str() << endl << endl;
      const char* topModule = getOptions(cfg->getName())->getValue("Module Name")->getValue();
       
      if (topModule == NULL || (topModule && strlen(topModule) == 0)) 
        topModule = getOptions(cfg->getName())->getValue("Top Module")->getValue();

      OSConstructFilePath(&ccfgName, outDir, ccfgName.c_str());
      QFileInfo ccfgfi(ccfgName.c_str());
      
      if (ccfgfi.exists())
      {
        stream << generatedOutputName.c_str() << ": " << "$(VHMNAME)" << " " << SYSTEMC_COMPONENT_SUBDIR << "/" << ccfgfi.fileName() << " " << SYSTEMC_COMPONENT_SUBDIR << "/configuration" << endl;
        stream << "\t" << echoBegin() << "Start " << SYSTEMC_COMPONENT_NAME << " Component Generation" << echoEndl();
        stream << "\t" << echoBegin() << "CarbonWorkingDir: " << SYSTEMC_COMPONENT_NAME << echoEndl();
        stream << "\t" << "cd " << SYSTEMC_COMPONENT_NAME << " && \"$(CARBON_HOME)/bin/$(CARBON_BATCH)\" systemCWrapper -ccfg " << ccfgfi.fileName() << endl;
        stream << "\t" << echoBegin() << "Finished " << SYSTEMC_COMPONENT_NAME << " Component Generation" << echoEndl();
      }
      else
      {
        UtString SystemCName;
        SystemCName << fi.fileName();

        stream << generatedOutputName.c_str() << ": " << SystemCName.c_str() << " " << SYSTEMC_COMPONENT_SUBDIR << "/configuration" << endl;
        stream << "\t" << echoBegin() << "Start " << SYSTEMC_COMPONENT_NAME << " Component Generation" << echoEndl();
        stream << "\t" << echoBegin() << "CarbonWorkingDir: " << SYSTEMC_COMPONENT_NAME << echoEndl();
        stream << "\t" << "cd " << SYSTEMC_COMPONENT_NAME << " && \"$(CARBON_HOME)/bin/$(CARBON_BATCH)\" systemCWrapper -moduleName " << topModule << " ../" << SystemCName.c_str() << endl;
        stream << "\t" << echoBegin() << "Finished " << SYSTEMC_COMPONENT_NAME << " Component Generation" << echoEndl();
      }
    }
    break;
  case CarbonComponent::Package: break;
  case CarbonComponent::Clean:
    {
      stream << ".PHONY: " << getTargetName(cfg, CarbonComponent::Clean) << endl;
      stream << getTargetName(cfg, CarbonComponent::Clean) << ":" << endl;
      stream << "\t" << "-cd " << SYSTEMC_COMPONENT_NAME << " && /bin/touch configuration" << endl;
      stream << "\t" << echoBegin() << "Cleaned " << SYSTEMC_COMPONENT_NAME << " Component" << echoEndl();
    }
    break;
  }

  return true;
}

void SystemCSourceItem::doubleClicked(int)
{
  UtString fileName;
  fileName << text(0);

  const char* od = mComp->getOutputDirectory();
  UtString fpath;
  OSConstructFilePath(&fpath, od, fileName.c_str());

  QFileInfo fi(fpath.c_str());
  if (fi.exists())
    mProjectWidget->openSource(fpath.c_str());
  else
  {
    UtString msg;
    msg << "The design must be compiled before you can open the Wizard";
    QMessageBox::critical(mProjectWidget, MODELSTUDIO_TITLE, msg.c_str());
  }
}

void SystemCCcfgItem::doubleClicked(int)
{
  UtString fileName;
  fileName << text(0);

  const char* od = mComp->getOutputDirectory();
  UtString fpath;
  OSConstructFilePath(&fpath, od, fileName.c_str());

  MDIWidget* widget = mProjectWidget->openSource(fpath.c_str(), -1, SYSTEMC_COMPONENT_NAME);
  if (widget == NULL)
  {
    UtString msg;
    msg << "Unable to open the " << SYSTEMC_COMPONENT_NAME << " Wizard." << "\n";
    msg << "In order to open the Wizard: " << "\n";
    msg << "1) The design must be compiled for the active configuration." << "\n";
    msg << "2) The component must exist for this configuration" << "\n";
    msg << "Note: You can copy components from other configurations by using the Configuration Manager";
    QMessageBox::warning(mProjectWidget, MODELSTUDIO_TITLE, msg.c_str(), QMessageBox::Ok);  
  }
}


void SystemCCcfgItem::singleClicked(int)
{
  QStringList items;
  QVariantList itemValues;

  mComp->getOptions()->setItems("SystemC Component Properties", items, itemValues);
  mProjectWidget->context()->getSettingsEditor()->setSettings(mComp->getOptions());
}

void SystemCCcfgItem::showContextMenu(const QPoint& pos)
{
  SystemCTreeItem* treeItem = dynamic_cast<SystemCTreeItem*>(parent());
  if (treeItem)
    treeItem->showContextMenu(pos);
}

void SystemCTreeItem::singleClicked(int)
{
  SystemCComponent* comp = static_cast<SystemCComponent*>(mComponent);
  QStringList items;
  QVariantList itemValues;

  comp->getOptions()->setItems("SystemC Component Properties", items, itemValues);
  mProjectWidget->context()->getSettingsEditor()->setSettings(comp->getOptions());
}

void SystemCTreeItem::showContextMenu(const QPoint& pos)
{
  QMenu menu;
  menu.addAction(mActionCompile);
  menu.addAction(mActionCheck);
  menu.addAction(mActionClean);
  menu.addAction(mActionDelete);

  menu.setEnabled(!mProjectWidget->isCompilationInProgress());

  menu.exec(pos);
}

SystemCCcfgItem::SystemCCcfgItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, SystemCComponent* comp) : CarbonProjectTreeNode(proj,parent) 
{
  setIcon(0, QIcon(":/cmm/Resources/Wizard.png"));
  mComp = comp;
}

void SystemCTreeItem::createActions()
{
  mActionCompile = new QAction(mProjectWidget);
  mActionCompile->setText("Compile");
  mActionCompile->setObjectName(QString::fromUtf8("cpwCreateSystemCComponent"));
  mActionCompile->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/SystemC.png")));
  mActionCompile->setStatusTip("Compile this component");
  CQT_CONNECT(mActionCompile, triggered(), this, compileComponent());

  mActionCheck = new QAction(mProjectWidget);
  mActionCheck->setText("Check");
  mActionCheck->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/check.png")));
  mActionCheck->setStatusTip("Check this component for errors");
  CQT_CONNECT(mActionCheck, triggered(), this, icheckComponent());

  mActionClean = new QAction(mProjectWidget);
  mActionClean->setText("Clean");
  mActionClean->setObjectName(QString::fromUtf8("cpwCreateSystemCComponent"));
  mActionClean->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/clearall.png")));
  mActionClean->setStatusTip("Clean files generated for this component");
  CQT_CONNECT(mActionClean, triggered(), this, cleanComponent());

  mActionDelete = new QAction(mProjectWidget);
  mActionDelete->setText("Delete");
  mActionDelete->setObjectName(QString::fromUtf8("cpwCreateSystemCComponent"));
  mActionDelete->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/DeleteHS.png")));
  mActionDelete->setStatusTip("Delete this component");
  CQT_CONNECT(mActionDelete, triggered(), this, deleteComponent());

}

SystemCTreeItem::SystemCTreeItem(CarbonProjectWidget* proj, SystemCComponent* comp) : CarbonComponentTreeItem(proj, 0, comp)
{
  setIcon(0, QIcon(":/cmm/Resources/SystemC.png"));
  createActions();
}

SystemCTreeItem::SystemCTreeItem(CarbonProjectWidget* proj, SystemCComponent* comp, QTreeWidgetItem* parent) : CarbonComponentTreeItem(proj, parent, comp)
{
  setIcon(0, QIcon(":/cmm/Resources/SystemC.png"));
  createActions();
}



// We need to delete the whole item
void SystemCTreeItem::deleteComponent()
{
  SystemCComponent* SystemC = static_cast<SystemCComponent*>(mComponent);
  INFO_ASSERT(SystemC, "Bad component");
  const char* outputDir = SystemC->getOutputDirectory();
  CarbonComponentTreeItem::deleteComponent(outputDir, CarbonComponentTreeItem::SystemC);
}

void SystemCTreeItem::deleteComponentBatch()
{
  SystemCComponent* SystemC = static_cast<SystemCComponent*>(mComponent);
  INFO_ASSERT(SystemC, "Bad component");
  const char* outputDir = SystemC->getOutputDirectory();
  CarbonComponentTreeItem::deleteComponent(outputDir, CarbonComponentTreeItem::SystemC, false, DlgConfirmDelete::Delete);
}


void SystemCTreeItem::cleanComponent()
{
  UtString target; // getTargetName() uses static buffer, so save copy of result
  target << mComponent->getTargetName(mProjectWidget->project()->getActive(), CarbonComponent::Clean);
  mProjectWidget->compile(true, false, false, NULL, target.c_str());
 // ((SystemCComponent*)mComponent)->updateTreeIcons();
}
void SystemCTreeItem::compileComponent()
{
  UtString target; // getTargetName() uses static buffer, so save copy of result
  target << mComponent->getTargetName(mProjectWidget->project()->getActive(), CarbonComponent::Build);
  mProjectWidget->compile(false, true, false, target.c_str());
 // ((SystemCComponent*)mComponent)->updateTreeIcons();
}

void SystemCTreeItem::icheckComponent()
{
 ((SystemCComponent*)mComponent)->icheckComponent();
}

const char* SystemCComponent::computeSystemCType(CarbonCfgRTLPort* rtlPort)
{
  int nConnections = rtlPort->numConnections();
  INFO_ASSERT(nConnections == 1, "Port has more than one connection");
  CarbonCfgRTLConnection* conn = rtlPort->getConnection(0);
  CarbonCfgESLPort* eslPort = conn->castESLPort();

  if (eslPort != NULL)
  {
    return mCfg->computeSystemCType(rtlPort);
  }
  return NULL;
}




void SystemCComponent::computeMode(CarbonDB* db, const CarbonDBNode* node, CarbonCfgESLPort* eslPort)
{
  if (carbonDBIsAsyncPosReset(db, node) || carbonDBIsAsyncNegReset(db, node))
  {
    eslPort->putMode(eCarbonCfgESLPortReset);
  }
  else if (carbonDBIsClk(db, node) || carbonDBIsClkTree(db, node))
  {
    eslPort->putMode(eCarbonCfgESLPortClock);
  }
  else
  {
    eslPort->putMode(eCarbonCfgESLPortControl);
  }
}

void SystemCComponent::createDefaultComponent()
{
  QString iodbName = getProject()->getDatabaseFilePath();
  QFileInfo fi(iodbName);
  QString ccfgDir = QString("%1/%2")
    .arg(getProject()->getActive()->getOutputDirectory())
    .arg(SYSTEMC_COMPONENT_SUBDIR);

  UtString ccfgFilePath;
  ccfgFilePath << ccfgDir << "/" << fi.baseName() << ".ccfg";

  CarbonMakerContext* cmContext = theApp->getContext();

    // Build the leaf widgets that hold the content for the configuration
  mCfg = carbonCfgModeCreate(CarbonCfg::eSystemC);
  CQtContext* cqt = cmContext->getQtContext();

  if (carbonCfgReadXtorLib(mCfg, eCarbonXtorsSystemC) == eCarbonCfgFailure)
  {
    CarbonConsole* console = getProject()->getProjectWidget()->getConsole();
    console->processOutput(CarbonConsole::Local, CarbonConsole::Check, carbonCfgGetErrmsg(mCfg));
  }

  // read any user transactor defs
  if (cqt->getXtorDefFile() != NULL)
  {
    if (carbonCfgReadXtorDefinitions(mCfg, cqt->getXtorDefFile()) == eCarbonCfgFailure)
      cqt->warning(cmContext->getMainWindow(), carbonCfgGetErrmsg(mCfg));
  }
  UtString iodbname;
  iodbname << iodbName;

  mDB = carbonDBOpenFile(iodbname.c_str());
  if (mDB)
  {
    mCfg->putDB(mDB);

    populateLoop(mDB, eCarbonCfgRTLInput,
                 carbonDBLoopPrimaryInputs(mDB));
    populateLoop(mDB, eCarbonCfgRTLInout,
                 carbonDBLoopPrimaryBidis(mDB));
    populateLoop(mDB, eCarbonCfgRTLOutput,
                 carbonDBLoopPrimaryOutputs(mDB));
    populateLoop(mDB, eCarbonCfgRTLInput,
                 carbonDBLoopScDepositable(mDB));
    populateLoop(mDB, eCarbonCfgRTLOutput,
                 carbonDBLoopScObservable(mDB));

    UtString relIODBName;
    relIODBName << "../" << fi.fileName();

    carbonCfgPutIODBFile(mCfg, relIODBName.c_str());

    UtString relLibName;
    const char* outputFilename = getProject()->getToolOptions("VSPCompiler")->getValue("-o")->getValue();
    QFileInfo ofi(outputFilename);
    UtString libExt;
    libExt << "." << ofi.completeSuffix();

    const char* libFilePath = getProject()->getDesignFilePath(libExt.c_str());
    relLibName << CarbonProjectWidget::makeRelativePath(ccfgDir, libFilePath);
    carbonCfgPutLibName(mCfg, relLibName.c_str());

    carbonCfgPutCompName(mCfg, carbonDBGetTopLevelModuleName(mDB));
    carbonCfgPutTopModuleName(mCfg, carbonDBGetTopLevelModuleName(mDB));

 // Turn Tie'd ports into Ties
    for(UInt32 i=0; i<mCfg->numESLPorts(); i++)
    {
      CarbonCfgESLPort* eslPort = mCfg->getESLPort(i);
      CarbonCfgRTLPort* rtlPort = eslPort->getRTLPort();
      CarbonCfgESLPortMode mode = eslPort->getMode();
      
      if (rtlPort)
      {
        if (mode == eCarbonCfgESLPortUndefined)
        {
          const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPort->getName());
          computeMode(mDB, node, eslPort);
        }

        eslPort->putTypeDef(computeSystemCType(rtlPort));

        const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPort->getName());
        if (carbonDBIsTied(mDB, node))
        {
          const IODBRuntime* iodb = CarbonDatabasePriv::getIODB(mDB);
          const STSymbolTableNode* stNode = CarbonDatabasePriv::getSymTabNode(node);
          const DynBitVector* bv = iodb->getTieValue(stNode);
          UtString valStr;
          bv->format(&valStr, eCarbonHex);
          qDebug() << "RTL Port" << rtlPort->getName() << "tied" << valStr.c_str();
          mCfg->disconnect(eslPort);
          UInt32 tieValue = bv->value();
          carbonCfgTie(rtlPort, &tieValue, 1); 
        }
      }
    }

    if (carbonCfgWrite(mCfg, ccfgFilePath.c_str()) == eCarbonCfgFailure)
    {
      cqt->warning(cmContext->getMainWindow(), tr("Cannot write file %1:\n%2.")
                    .arg(ccfgFilePath.c_str())
                    .arg(carbonCfgGetErrmsg(mCfg)));
    }
  }
}

const char* SystemCComponent::ccfgName()
{
  QFileInfo fi(mIODBName.c_str());
  UtString baseName;
  baseName << fi.baseName() << ".ccfg";
  UtString ccfgName;
  mCcfgName.clear();
  OSConstructFilePath(&ccfgName, getOutputDirectory(), baseName.c_str());
  mCcfgName << ccfgName;
  return mCcfgName.c_str();
}

void SystemCComponent::populateLoop(CarbonDB* carbonDB,
                                 CarbonCfgRTLPortType type,
                                 CarbonDBNodeIter* iter)
{
  const CarbonDBNode* node;
  while ((node = carbonDBNodeIterNext(iter)) != NULL)
  {
    int width = carbonDBGetWidth(carbonDB, node);
    const char* name = carbonDBNodeGetFullName(carbonDB, node);
    CarbonCfgRTLPortID port = carbonCfgAddRTLPort(mCfg, name, width, type);
    if (port != NULL)
    {
      (void) addConnection(port);
    }
  }
  carbonDBFreeNodeIter(iter);
}

CarbonCfgESLPortID SystemCComponent::addConnection(CarbonCfgRTLPortID rtlPort)
{
  CarbonCfgESLPortType portType = CarbonCfg::convertRTLPortType(rtlPort);
  return addESLConnection(rtlPort, portType);
}

CarbonCfgESLPortID SystemCComponent::addESLConnection(CarbonCfgRTLPortID rtlPort,
                                                    CarbonCfgESLPortType type)
{
  // Get a leaf name for the RTL port, use that as a starting point for
  // the ESL name
  const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPort->getName());
  UtString buf;
  if (node == NULL) {
    mCfg->getUniqueESLName(&buf, rtlPort->getName());
  }
  else {
    mCfg->getUniqueESLName(&buf, carbonDBNodeGetLeafName(mDB, node));
  }

  CarbonCfgESLPortID eslPort = carbonCfgAddESLPort(mCfg, rtlPort,
                                                   buf.c_str(), type);

  INFO_ASSERT(eslPort, "Failed to create ESL port");
  return eslPort;
}
