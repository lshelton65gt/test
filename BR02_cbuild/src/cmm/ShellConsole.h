#ifndef __SHELLCONSOLE_H__
#define __SHELLCONSOLE_H__

#include "util/CarbonPlatform.h"
#include "util/UtHashMap.h"

#include <QtGui>
#include <QProcess>

class CarbonProject;

class ShellConsole : public QObject
{
  Q_OBJECT

public:
  enum LoginState {Unknown=0, Started, Terminated};

  ShellConsole(QWidget* parent = 0);
  virtual ~ShellConsole();

  void abortSession();
  int executeProgram(CarbonProject* proj, const QString& workingDir, QStringList& envVars, const QString& program, const QStringList& arguments);
  int executeProgramWait(CarbonProject* proj, const QString& workingDir, QStringList& envVars, const QString& program, const QStringList& arguments);
  int getPid();

private slots:
  // slots from QProcess
  void procStarted();
  void procFinished(int exitCode);
  void procError(QProcess::ProcessError err);
  void procReadStderr();
  void procReadStdout();

signals:
  void processReadyForCommands();
  void processOutput(const QString&);
  void processTerminated(int);
  void sessionStateChanged(ShellConsole::LoginState);
  void sessionLoginStatus(const QString&);
  void commandCompleted(int);

private: // methods
  bool useProject() { return mProject != NULL; }
  bool processLine(const QString& line);
  bool processOutputLine(const QString& line);
  void buildCommandLine(QTextStream& cmdLine);
  void addEnvVar(QStringList& env, const QString& name, const QString& value);
  void addCarbonEnvironmentVariables(const QStringList& extraVars);
  void writeCommand(const QString& cmd);
  void writeHiddenCommand(const QString& cmd);
  void setState(LoginState newState);
  LoginState getState() { return mLoginState; }

private:
  //! Map to keep setenv strings static
  typedef UtHashMap<UtString, UtString*> EnvVars;
  EnvVars mEnvVars;

  QString mProcessWorkingDirectory;
  QString mPassword;
  bool mUsePassword;
  LoginState mLoginState;
  QString mProgram;
  QStringList mArgs;
  CarbonProject* mProject;
  QProcess* mProcess;
  int mExitCode;
  QString mUsername;
  QString mServer;
  QWidget* mParent;
  QString mCommandProgram;
  QStringList mCommandArgs;
  QString mWorkingDir;
  bool mAborting;
};

#endif
