#ifndef __ESLRESETGEN__
#define __ESLRESETGEN__

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "gui/CExprValidator.h"
#include "util/DynBitVector.h"
#include "CompWizardTreeNodes.h"
#include "PortEditorTreeWidget.h"
#include "cfg/CarbonCfg.h"
#include "cfg/carbon_cfg.h"
#include <QtGui>


class ESLResetGenActiveItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colVALUE };

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  CarbonCfgResetGen* getResetGen() const { return mResetGen; }

  void setValue(quint64 newValue)
  {
    QString value = QString("0x%1").arg(QString().setNum(newValue, 16));
    setText(colVALUE, value);
  }

  ESLResetGenActiveItem(QTreeWidgetItem* parent, CarbonCfgResetGen* resetGen)
    : PortEditorTreeItem(PortEditorTreeItem::ResetActive, parent)
  {
    mResetGen = resetGen;

    setItalicText(colNAME, "Active Value");
    setValue(resetGen->mActiveValue);
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgResetGen* mResetGen;
};

class ESLResetGenInactiveItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colVALUE };

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  void setValue(quint64 newValue)
  {
    QString value = QString("0x%1").arg(QString().setNum(newValue, 16));
    setText(colVALUE, value);
  }

  ESLResetGenInactiveItem(QTreeWidgetItem* parent, CarbonCfgResetGen* resetGen)
    : PortEditorTreeItem(PortEditorTreeItem::ResetInactive, parent)
  {
    mResetGen = resetGen;

    setItalicText(colNAME, "Inactive Value");
    setValue(resetGen->mInactiveValue);
    setFlags(flags() | Qt::ItemIsEditable);
  }

  CarbonCfgResetGen* getResetGen() const { return mResetGen; }

private:
  CarbonCfgResetGen* mResetGen;
};

class ESLResetGenCyclesBeforeItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colVALUE };

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  CarbonCfgResetGen* getResetGen() const { return mResetGen; }

  void setValue(quint32 newValue)
  {
    QString value = QString("%1").arg(newValue);
    setText(colVALUE, value);
  }

  ESLResetGenCyclesBeforeItem(QTreeWidgetItem* parent, CarbonCfgResetGen* resetGen)
    : PortEditorTreeItem(PortEditorTreeItem::ResetCyclesBefore, parent)
  {
    mResetGen = resetGen;

    setItalicText(colNAME, "Cycles Before");
    setValue(resetGen->mCyclesBefore);
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgResetGen* mResetGen;
};

class ESLResetGenCyclesAfterItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colVALUE };

  CarbonCfgResetGen* getResetGen() const { return mResetGen; }

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  void setValue(quint32 newValue)
  {
    QString value =  QString("%1").arg(newValue);
    setText(colVALUE, value);
  }

  ESLResetGenCyclesAfterItem(QTreeWidgetItem* parent, CarbonCfgResetGen* resetGen)
    : PortEditorTreeItem(PortEditorTreeItem::ResetCyclesAfter, parent)
  {
    mResetGen = resetGen;

    setItalicText(colNAME, "Cycles After");
    setValue(resetGen->mCyclesAfter);
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgResetGen* mResetGen;
};

class ESLResetGenCyclesAssertedItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colVALUE };

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  CarbonCfgResetGen* getResetGen() const { return mResetGen; }

  void setValue(quint32 newValue)
  {
    QString value = QString("%1").arg(newValue);
    setText(colVALUE, value);
  }

  ESLResetGenCyclesAssertedItem(QTreeWidgetItem* parent, CarbonCfgResetGen* resetGen)
    : PortEditorTreeItem(PortEditorTreeItem::ResetCyclesAsserted, parent)
  {
    mResetGen = resetGen;

    setItalicText(colNAME, "Cycles Asserted");
    setValue(resetGen->mCyclesAsserted);
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgResetGen* mResetGen;
};

class ESLResetGenFrequencyResetItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colVALUE };

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  CarbonCfgResetGen* getResetGen() const { return mResetGen; }

  void setValue(quint32 newValue)
  {
    QString value = QString("%1").arg(newValue);
    setText(colVALUE, value);
  }

  ESLResetGenFrequencyResetItem(QTreeWidgetItem* parent, CarbonCfgResetGen* resetGen)
    : PortEditorTreeItem(PortEditorTreeItem::ResetClockCycles, parent)
  {
    mResetGen = resetGen;

    setItalicText(colNAME, "Reset Cycles");
    setValue(resetGen->mFrequency.getClockCycles());
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgResetGen* mResetGen;
};

class ESLResetGenFrequencyCyclesItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colVALUE };

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  CarbonCfgResetGen* getResetGen() const { return mResetGen; }

  void setValue(quint32 newValue)
  {
    QString value = QString("%1").arg(newValue);
    setText(colVALUE, value);
  }

  ESLResetGenFrequencyCyclesItem(QTreeWidgetItem* parent, CarbonCfgResetGen* resetGen)
    : PortEditorTreeItem(PortEditorTreeItem::ResetCompCycles, parent)
  {
    mResetGen = resetGen;

    setItalicText(colNAME, "Component Cycles");
    setValue(resetGen->mFrequency.getCompCycles());
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgResetGen* mResetGen;
};

class ESLResetGenFrequencyItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0} ;

  ESLResetGenFrequencyItem(QTreeWidgetItem* parent, CarbonCfgResetGen* resetGen)
    : PortEditorTreeItem(PortEditorTreeItem::ResetFrequency, parent)
  {
    mResetGen = resetGen;

    setBoldText(colNAME, "Frequency");

    new ESLResetGenFrequencyResetItem(this, resetGen);
    new ESLResetGenFrequencyCyclesItem(this, resetGen);

    setExpanded(true);
  }

private:
  CarbonCfgResetGen* mResetGen;
};


class ESLResetGenItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colUNUSED, colSIZE};

  virtual UndoData* deleteItem();

  ESLResetGenItem(QTreeWidgetItem* parent, CarbonCfgResetGen* resetGen)
    : PortEditorTreeItem(PortEditorTreeItem::Reset, parent)
  {
    mResetGen = resetGen;

    CarbonCfgRTLPort* rtlPort = resetGen->getRTLPort();

    setText(colNAME, rtlPort->getName());
    setIcon(colNAME, QIcon(":/cmm/Resources/RepeatHS.png"));
    QString value = QString("%1").arg(rtlPort->getWidth());
    setText(colSIZE, value);

    new ESLResetGenActiveItem(this, resetGen);
    new ESLResetGenInactiveItem(this, resetGen);

    new ESLResetGenCyclesBeforeItem(this, resetGen);
    new ESLResetGenCyclesAfterItem(this, resetGen);
    new ESLResetGenCyclesAssertedItem(this, resetGen);

    new ESLResetGenFrequencyItem(this, resetGen);

    setExpanded(true);
  }

  static CarbonCfgRTLPort* removeResetGen(PortEditorTreeItem* item, const QString& rtlPortName);
  static ESLResetGenItem* createResetGen(PortEditorTreeWidget* tree,
                        UndoData* undoData, CarbonCfgRTLPort* rtlPort,
                        UInt64 ActiveValue, UInt64 InactiveValue,
                        UInt32 ClockCycles, UInt32 CompCycles,
                        UInt32 CyclesBefore, UInt32 CyclesAsserted,
                        UInt32 CyclesAfter);

private: 
 class Delete : public UndoData
  {
  public:
    CarbonCfg* mCfg;
    QString mRTLPortName;
    QString mESLPortName;
    PortTreeIndex mESLPortIndex;
    UInt64 mActiveValue;
    UInt64 mInactiveValue;
    UInt32 mClockCycles, mCompCycles;
    UInt32 mCyclesBefore, mCyclesAsserted, mCyclesAfter;

  protected:
    void undo();
    void redo();
  };

private:
  CarbonCfgResetGen* mResetGen;
};

#endif
