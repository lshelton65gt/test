//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QDebug>
#include <QProgressBar>

#include "ErrorInfoWidget.h"
#include "gui/CQt.h"

ErrorInfoWidget::ErrorInfoWidget(QWidget *parent, CarbonMakerContext* ctx, CarbonConsole* console, QProgressBar* pb, QStatusBar* sb)
: QWidget(parent)
{
  ui.setupUi(this);
 
  ui.widgetInfo->connectConsole(ui.pushButtonErrors, ui.pushButtonWarnings, ui.pushButtonMessages, ctx, console, pb, sb);

  ui.textBrowser->setMinimumHeight(0);
  ui.textBrowser->viewport()->setBackgroundRole(QPalette::Button);

  ui.splitter->setSizes(QList<int>() << 200 << 40);

  CQT_CONNECT(ui.widgetInfo, showExtendedHelp(const char*), this, showExtendedHelp(const char*));
}

void ErrorInfoWidget::showExtendedHelp(const char* help)
{
  ui.textBrowser->setText(help);
}

ErrorInfoWidget::~ErrorInfoWidget()
{
}

void ErrorInfoWidget::on_pushButtonFiltered_toggled(bool value)
{
  ui.widgetInfo->toggleDisplay(value, InfoTableWidget::Filtered);
}

void ErrorInfoWidget::on_pushButtonErrors_toggled(bool value)
{
  ui.widgetInfo->toggleDisplay(value, InfoTableWidget::Error);
}

void ErrorInfoWidget::on_pushButtonWarnings_toggled(bool value)
{
  ui.widgetInfo->toggleDisplay(value, InfoTableWidget::Warning);
}

void ErrorInfoWidget::on_pushButtonMessages_toggled(bool value)
{
  ui.widgetInfo->toggleDisplay(value, InfoTableWidget::Information);
}
