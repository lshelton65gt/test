#ifndef __Attribute_H_
#define __Attribute_H_
#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "Scripting.h"

class Template;
class Attributes;
class Port;

class Attribute : public QObject
{
  Q_OBJECT

  Q_ENUMS(AttributeType)
  Q_PROPERTY(QString name READ getName)
  Q_PROPERTY(QVariant value READ getValue)

public:
  enum AttributeType { Undefined, Integer, String, Bool, Enum };
  static AttributeType fromString(const QString& val);

  static bool deserialize(Template* templ, Attributes* atts, const QDomElement& e, XmlErrorHandler* eh);

  void setName(const QString& newVal) { mName=newVal; }
  QString getName() const { return mName; }

  AttributeType getType() const { return mType; }
  void setType(AttributeType newVal) { mType=newVal; }

  void setValue(const QVariant& newVal) { mValue=newVal; }
  QVariant getValue() const { return mValue; }

  Attribute()
  {
  }

private:
  Q_DISABLE_COPY(Attribute)

private:
  QString mName;
  QVariant mValue;
  AttributeType mType;

};

Q_DECLARE_METATYPE(Attribute*);

#endif
