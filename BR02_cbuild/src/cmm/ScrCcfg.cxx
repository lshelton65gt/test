//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2013 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "DesignXML.h"
#include "ScrCcfg.h"
#include "CarbonFiles.h"
#include "JavaScriptAPIs.h"
#include "cmm.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "MaxsimComponent.h"
#include "SystemCComponent.h"
#include "CowareComponent.h"
#include "EmbeddedMono.h"

//
// Include generated Wrappers
//
#if 1
#include "classGen/ScrCcfg_wrapper.cxx"
#include "classGen/ProtoCarbonCfgCadiCustomCode_wrapper.cxx"
#include "classGen/ProtoCarbonCfgCadi_wrapper.cxx"
#include "classGen/ProtoCarbonCfgClockGen_wrapper.cxx"
#include "classGen/ProtoCarbonCfgCompCustomCode_wrapper.cxx"
#include "classGen/ProtoCarbonCfgCustomCode_wrapper.cxx"
#include "classGen/ProtoCarbonCfgELFLoader_wrapper.cxx"
#include "classGen/ProtoCarbonCfgESLPort_wrapper.cxx"
#include "classGen/ProtoCarbonCfgGroup_wrapper.cxx"
#include "classGen/ProtoCarbonCfgMemoryBlockCustomCode_wrapper.cxx"
#include "classGen/ProtoCarbonCfgMemoryBlock_wrapper.cxx"
#include "classGen/ProtoCarbonCfgMemoryCustomCode_wrapper.cxx"
#include "classGen/ProtoCarbonCfgMemoryLocPort_wrapper.cxx"
#include "classGen/ProtoCarbonCfgMemoryLocRTL_wrapper.cxx"
#include "classGen/ProtoCarbonCfgMemoryLocUser_wrapper.cxx"
#include "classGen/ProtoCarbonCfgMemoryLoc_wrapper.cxx"
#include "classGen/ProtoCarbonCfgMemory_wrapper.cxx"
#include "classGen/ProtoCarbonCfgProcInfo_wrapper.cxx"
#include "classGen/ProtoCarbonCfgRegCustomCode_wrapper.cxx"
#include "classGen/ProtoCarbonCfgRegisterField_wrapper.cxx"
#include "classGen/ProtoCarbonCfgRegisterLocArray_wrapper.cxx"
#include "classGen/ProtoCarbonCfgRegisterLocConstant_wrapper.cxx"
#include "classGen/ProtoCarbonCfgRegisterLocReg_wrapper.cxx"
#include "classGen/ProtoCarbonCfgRegisterLocRTL_wrapper.cxx"
#include "classGen/ProtoCarbonCfgRegisterLocUser_wrapper.cxx"
#include "classGen/ProtoCarbonCfgRegisterLoc_wrapper.cxx"
#include "classGen/ProtoCarbonCfgRegister_wrapper.cxx"
#include "classGen/ProtoCarbonCfgResetGen_wrapper.cxx"
#include "classGen/ProtoCarbonCfgRTLConnection_wrapper.cxx"
#include "classGen/ProtoCarbonCfgRTLPort_wrapper.cxx"
#include "classGen/ProtoCarbonCfgTemplate_wrapper.cxx"
#include "classGen/ProtoCarbonCfgTieParam_wrapper.cxx"
#include "classGen/ProtoCarbonCfgTie_wrapper.cxx"
#include "classGen/ProtoCarbonCfgXtorConn_wrapper.cxx"
#include "classGen/ProtoCarbonCfgXtorInstance_wrapper.cxx"
#include "classGen/ProtoCarbonCfgXtorLib_wrapper.cxx"
#include "classGen/ProtoCarbonCfgXtorParamInst_wrapper.cxx"
#include "classGen/ProtoCarbonCfgXtorParam_wrapper.cxx"
#include "classGen/ProtoCarbonCfgXtorPort_wrapper.cxx"
#include "classGen/ProtoCarbonCfgXtor_wrapper.cxx"
#endif

Q_SCRIPT_DECLARE_QMETAOBJECT(ScrCcfg, QObject*)

static QScriptValue constructCarbonCfgMemoryLocRTL(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 3)
  {
    QString rtlPath = qscriptvalue_cast<QString>(ctx->argument(0));
    quint32 bitWidth =  qscriptvalue_cast<quint32>(ctx->argument(1));
    quint32 endWord =  qscriptvalue_cast<quint32>(ctx->argument(2));

    UtString rtlpath;
    rtlpath << rtlPath;

    CarbonCfgMemoryLocRTL* memLocRTL = new CarbonCfgMemoryLocRTL(rtlpath.c_str(), bitWidth, endWord);
    return eng->toScriptValue(memLocRTL);
  }
  else
    ctx->throwError("CarbonCfgMemoryLocRTL() constructor requires 3 arguments");

  return QScriptValue();
}

static QScriptValue constructCarbonCfgMemoryLocPort(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 1)
  {
    QString portName = qscriptvalue_cast<QString>(ctx->argument(0));
    
    UtString portname;
    portname << portName;

    CarbonCfgMemoryLocPort* memLocPort = new CarbonCfgMemoryLocPort(portname.c_str());
    return eng->toScriptValue(memLocPort);
  }
  else
    ctx->throwError("CarbonCfgMemoryLocPort() constructor requires 1 arguments");

  return QScriptValue();
}


static QScriptValue constructCarbonCfgXtorConn(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 3)
  {
    CarbonCfgRTLPort* rtlPort = qscriptvalue_cast<CarbonCfgRTLPort*>(ctx->argument(0));
    CarbonCfgXtorInstance* xtorInst = qscriptvalue_cast<CarbonCfgXtorInstance*>(ctx->argument(1));
    quint32 index = qscriptvalue_cast<quint32>(ctx->argument(2));

    CarbonCfgXtorConn* xtorConn = new CarbonCfgXtorConn(rtlPort, xtorInst, index);
    rtlPort->connect(xtorConn);
    return eng->toScriptValue(xtorConn);
  }
  else
    ctx->throwError("CarbonCfgXtorConn() constructor requires 3 arguments");

  return QScriptValue();
}
 
static QScriptValue constructCarbonCfgResetGen(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 8)
  {
    CarbonCfgRTLPort* rtlPort = qscriptvalue_cast<CarbonCfgRTLPort*>(ctx->argument(0));
    quint64 activeValue = qscriptvalue_cast<quint64>(ctx->argument(1));
    quint64 inactiveValue = qscriptvalue_cast<quint64>(ctx->argument(2));
    quint32 clockCycles = qscriptvalue_cast<quint32>(ctx->argument(3));
    quint32 compCycles = qscriptvalue_cast<quint32>(ctx->argument(4));
    quint32 cyclesBefore = qscriptvalue_cast<quint32>(ctx->argument(5));
    quint32 cyclesAsserted = qscriptvalue_cast<quint32>(ctx->argument(6));
    quint32 cyclesAfter = qscriptvalue_cast<quint32>(ctx->argument(7));

    CarbonCfgResetGen* resetGen = new CarbonCfgResetGen(rtlPort, activeValue,
      inactiveValue, clockCycles, compCycles, cyclesBefore, cyclesAsserted, cyclesAfter);

    return eng->toScriptValue(resetGen);
 
  }
  else
    ctx->throwError("CarbonCfgResetGen() constructor requires 8 arguments");

  return QScriptValue();
}

static QScriptValue constructCarbonCfgClockGen(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 6)
  {
    CarbonCfgRTLPort* rtlPort = qscriptvalue_cast<CarbonCfgRTLPort*>(ctx->argument(0));
    quint32 init = qscriptvalue_cast<quint32>(ctx->argument(1));
    quint32 delay = qscriptvalue_cast<quint32>(ctx->argument(2));
    quint32 clockCycles = qscriptvalue_cast<quint32>(ctx->argument(3));
    quint32 compCycles = qscriptvalue_cast<quint32>(ctx->argument(4));
    quint32 duty = qscriptvalue_cast<quint32>(ctx->argument(5));

    CarbonCfgClockGen* clkGen = new CarbonCfgClockGen(rtlPort, init, delay, clockCycles, compCycles, duty);
    return eng->toScriptValue(clkGen);
  }
  else
    ctx->throwError("CarbonCfgClockGen() constructor requires 6 arguments");

  return QScriptValue();
}

static QScriptValue constructCarbonCfgTie(QScriptContext* ctx, QScriptEngine* eng)
{
  if (ctx->argumentCount() >= 2) {
    // The rtl port is the first argument
    CarbonCfgRTLPort* rtlPort = qscriptvalue_cast<CarbonCfgRTLPort*>(ctx->argument(0));    

    // The tie value is an array of uint32's passed as separate arguments
    DynBitVector tieVal(ctx->argumentCount()-1);
    UInt32* vals = tieVal.getUIntArray();
    for (int i = 1; i < ctx->argumentCount(); ++i) {
      vals[i-1] = qscriptvalue_cast<quint32>(ctx->argument(i));
    }

    // Construct the new object
    CarbonCfgTie* tie = new CarbonCfgTie(rtlPort, tieVal);
    return eng->toScriptValue(tie);

  } else {
    ctx->throwError("CarbonCfgTie() constructor takes at least 2 arguments");
    return QScriptValue();
  }
}


static QScriptValue
constructCarbonCfgTieParam(QScriptContext* ctx, QScriptEngine* eng)
{
  if (ctx->argumentCount() == 3) {
    // The rtl port is the first argument
    CarbonCfgRTLPort* rtlPort = qscriptvalue_cast<CarbonCfgRTLPort*>(ctx->argument(0));    

    // The second argument is the xtor inst name
    QString xtorName = qscriptvalue_cast<QString>(ctx->argument(1));
    UtString xName;
    xName << xtorName;

    // The third argument is the transactor param name
    QString paramName = qscriptvalue_cast<QString>(ctx->argument(2));
    UtString pName;
    pName << paramName;

    // Construct the new object
    CarbonCfgTieParam* tieParam = new CarbonCfgTieParam(rtlPort, xName.c_str(),
                                                        pName.c_str());
    return eng->toScriptValue(tieParam);

  } else {
    ctx->throwError("CarbonCfgTie() constructor takes at least 2 arguments");
    return QScriptValue();
  }
}

static QScriptValue constructCarbonCfgRegisterField(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 3)
  {
    quint32 high = qscriptvalue_cast<quint32>(ctx->argument(0));
    quint32 low = qscriptvalue_cast<quint32>(ctx->argument(1));
    int intAccesss = qscriptvalue_cast<int>(ctx->argument(2));
    CarbonCfgRegAccessType access = static_cast<CarbonCfgRegAccessType>(intAccesss);

    CarbonCfgRegisterField* field = new CarbonCfgRegisterField(high, low, access);
    return eng->toScriptValue(field);
  }
  else
    ctx->throwError("CarbonCfgRegisterField() constructor requires 3 arguments");

  return QScriptValue();
}

static QScriptValue constructCarbonCfgRegisterLocConstant(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 1)
  {
    quint64 value = qscriptvalue_cast<quint64>(ctx->argument(0));
    CarbonCfgRegisterLocConstant* loc = new CarbonCfgRegisterLocConstant(value);
    return eng->toScriptValue(loc);
  }
  else
    ctx->throwError("CarbonCfgRegisterLocConstant() constructor requires 1 argument");

  return QScriptValue();
}

static QScriptValue constructCarbonCfgRegisterLocUser(QScriptContext*, QScriptEngine *eng)
{
  CarbonCfgRegisterLocUser* loc = new CarbonCfgRegisterLocUser();
  return eng->toScriptValue(loc);
}

static QScriptValue constructCarbonCfgRegisterLocReg(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 1)
  {
    QString path = qscriptvalue_cast<QString>(ctx->argument(0));
    UtString rtlPath;
    rtlPath << path;
    CarbonCfgRegisterLocReg* loc = new CarbonCfgRegisterLocReg(rtlPath.c_str());
    return eng->toScriptValue(loc);
  }
  else if (ctx->argumentCount() == 3)
  {
    QString path = qscriptvalue_cast<QString>(ctx->argument(0));
    UtString rtlPath;
    rtlPath << path;

    quint32 left = qscriptvalue_cast<quint32>(ctx->argument(1));
    quint32 right = qscriptvalue_cast<quint32>(ctx->argument(2));
    CarbonCfgRegisterLocReg* loc = new CarbonCfgRegisterLocReg(rtlPath.c_str(), left, right);
    return eng->toScriptValue(loc);
 }
  else
    ctx->throwError("CarbonCfgRegisterLocReg() constructor requires 1 or 3 arguments");

  return QScriptValue();
}

static QScriptValue constructCarbonCfgRegisterLocArray(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 2)
  {
    QString path = qscriptvalue_cast<QString>(ctx->argument(0));
    UtString rtlPath;
    rtlPath << path;
    quint32 index = qscriptvalue_cast<quint32>(ctx->argument(1));

    CarbonCfgRegisterLocArray* loc = new CarbonCfgRegisterLocArray(rtlPath.c_str(), index);
    return eng->toScriptValue(loc);
  }
  else if (ctx->argumentCount() == 4)
  {
    QString path = qscriptvalue_cast<QString>(ctx->argument(0));
    UtString rtlPath;
    rtlPath << path;
    quint32 index = qscriptvalue_cast<quint32>(ctx->argument(1));

    quint32 left = qscriptvalue_cast<quint32>(ctx->argument(2));
    quint32 right = qscriptvalue_cast<quint32>(ctx->argument(3));
    CarbonCfgRegisterLocArray* loc = new CarbonCfgRegisterLocArray(rtlPath.c_str(), index, left, right);
    return eng->toScriptValue(loc);
 }
  else
    ctx->throwError("CarbonCfgRegisterLocReg() constructor requires 2 or 4 arguments");

  return QScriptValue(); 
}

void ScrCcfg::registerIntellisense(JavaScriptAPIs* apis)
{
  apis->loadAPIForVariable("Ccfg", &ScrCcfg::staticMetaObject);

  // Allows script typer to use CcfgEnum.<EnumName>
  apis->loadAPIForVariable("CcfgEnum", &CcfgEnum::staticMetaObject, true);
}

// Allows properties of these types to work through intellisense
void ScrCcfg::registerScriptTypes(QMap<QString, const QMetaObject*>& map)
{
  map["CcfgEnum"] = &CcfgEnum::staticMetaObject;
  map["ScrCcfg*"] = &ScrCcfg::staticMetaObject;
  map["CarbonCfgESLPort*"] = &ProtoCarbonCfgESLPort::staticMetaObject;
  map["CarbonCfgXtorInstance*"] = &ProtoCarbonCfgXtorInstance::staticMetaObject;
  map["CarbonCfgRTLPort*"] = &ProtoCarbonCfgRTLPort::staticMetaObject;
  map["CarbonCfgRTLConnection*"] = &ProtoCarbonCfgRTLConnection::staticMetaObject;
  map["CarbonCfgTieParam*"] = &ProtoCarbonCfgTieParam::staticMetaObject;
  map["CarbonCfgTie*"] = &ProtoCarbonCfgTie::staticMetaObject;
  map["CarbonCfgResetGen*"] = &ProtoCarbonCfgResetGen::staticMetaObject;
  map["CarbonCfgClockGen*"] = &ProtoCarbonCfgClockGen::staticMetaObject;
  map["CarbonCfgXtorConn*"] = &ProtoCarbonCfgXtorConn::staticMetaObject;
  map["CarbonCfgXtor*"] = &ProtoCarbonCfgXtor::staticMetaObject;
  map["CarbonCfgXtorLib*"] = &ProtoCarbonCfgXtorLib::staticMetaObject;
  map["CarbonCfgXtorPort*"] = &ProtoCarbonCfgXtorPort::staticMetaObject;
  map["CarbonCfgXtorParam*"] = &ProtoCarbonCfgXtorParam::staticMetaObject;
  map["CarbonCfgXtorParamInst*"] = &ProtoCarbonCfgXtorParamInst::staticMetaObject;
  map["CarbonCfgRegister*"] = &ProtoCarbonCfgRegister::staticMetaObject;
  map["CarbonCfgELFLoader*"] = &ProtoCarbonCfgELFLoader::staticMetaObject;
  map["CarbonCfgProcInfo*"] = &ProtoCarbonCfgProcInfo::staticMetaObject;
  map["CarbonCfgMemoryLoc*"] = &ProtoCarbonCfgMemoryLoc::staticMetaObject;
  map["CarbonCfgMemoryLocRTL*"] = &ProtoCarbonCfgMemoryLocRTL::staticMetaObject;
  map["CarbonCfgMemoryLocPort*"] = &ProtoCarbonCfgMemoryLocPort::staticMetaObject;
  map["CarbonCfgMemoryLocUser*"] = &ProtoCarbonCfgMemoryLocUser::staticMetaObject;
  map["CarbonCfgMemory*"] = &ProtoCarbonCfgMemory::staticMetaObject;
  map["CarbonCfgMemoryBlock*"] = &ProtoCarbonCfgMemoryBlock::staticMetaObject;
  map["CarbonCfgGroup*"] = &ProtoCarbonCfgGroup::staticMetaObject;
  map["CarbonCfgRegisterField*"] = &ProtoCarbonCfgRegisterField::staticMetaObject;
  map["CarbonCfgRegisterLoc*"] = &ProtoCarbonCfgRegisterLoc::staticMetaObject;
  map["CarbonCfgRegisterLocConstant*"] = &ProtoCarbonCfgRegisterLocConstant::staticMetaObject;
  map["CarbonCfgRegisterLocUser*"] = &ProtoCarbonCfgRegisterLocUser::staticMetaObject;
  map["CarbonCfgRegisterLocRTL*"] = &ProtoCarbonCfgRegisterLocRTL::staticMetaObject;
  map["CarbonCfgRegisterLocReg*"] = &ProtoCarbonCfgRegisterLocReg::staticMetaObject;
  map["CarbonCfgRegisterLocArray*"] = &ProtoCarbonCfgRegisterLocArray::staticMetaObject;
  map["CarbonCfgCustomCode*"] = &ProtoCarbonCfgCustomCode::staticMetaObject;
  map["CarbonCfgCompCustomCode*"] = &ProtoCarbonCfgCompCustomCode::staticMetaObject;
  map["CarbonCfgCadiCustomCode*"] = &ProtoCarbonCfgCadiCustomCode::staticMetaObject;
  map["CarbonCfgRegCustomCode*"] = &ProtoCarbonCfgRegCustomCode::staticMetaObject;
  map["CarbonCfgMemoryBlockCustomCode*"] = &ProtoCarbonCfgMemoryBlockCustomCode::staticMetaObject;
  map["CarbonCfgMemoryCustomCode*"] = &ProtoCarbonCfgMemoryCustomCode::staticMetaObject;
  map["CarbonCfgTemplate*"] = &ProtoCarbonCfgTemplate::staticMetaObject;
  map["CarbonCfgCadi*"] = &ProtoCarbonCfgCadi::staticMetaObject;
}


void ScrCcfg::registerTypes(QScriptEngine* engine)
{
  if (engine)
  {
    QScriptValue global = engine->globalObject();
    
    // Allows an object to return a property of type ScrCcfg
    qScriptRegisterQObjectMetaType<ScrCcfg*>(engine);

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgElementGenerationType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgElementGenerationType>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRegisterLoc);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgRegisterLocKind>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRegAccessType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgRegAccessType>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRadix);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgRadix>(engine); 
 
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRTLPortType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgRTLPortType>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRTLConnectionType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgRTLConnectionType>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgStatus);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgStatus>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgMode);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgMode>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgParamDataType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgParamDataType>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgParamFlag);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgParamFlag>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgESLPortType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgESLPortType>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgESLPortMode);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgESLPortMode>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgMemInitType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgMemInitType>(engine); 
    
    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgReadmemType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgReadmemType>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgMemLocType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgMemLocType>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgWaveType);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgWaveType>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgCustomCodePosition);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgCustomCodePosition>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgCompCustomCodeSection);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgCompCustomCodeSection>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgCadiCustomCodeSection);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgCadiCustomCodeSection>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgRegCustomCodeSection);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgRegCustomCodeSection>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgMemoryBlockCustomCodeSection);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgMemoryBlockCustomCodeSection>(engine); 

    //Q_DECLARE_METATYPE(CcfgEnum::CarbonCfgMemoryCustomCodeSection);
    qScriptRegisterEnumMetaType<CcfgEnum::CarbonCfgMemoryCustomCodeSection>(engine); 

    qScriptRegisterEnumMetaType<CcfgFlags::CarbonCfgMemoryEditFlags>(engine);
    qScriptRegisterEnumMetaType<CcfgFlags::CarbonCfgComponentEditFlags>(engine);

    // Static register for classes with Q_ENUMS
    QScriptValue ccfgEnum = engine->newQMetaObject(&CcfgEnum::staticMetaObject);
    engine->globalObject().setProperty("CcfgEnum", ccfgEnum);

   // Static register for classes with Q_FLAGS
    QScriptValue ccfgFlags = engine->newQMetaObject(&CcfgFlags::staticMetaObject);
    engine->globalObject().setProperty("CcfgFlags", ccfgFlags);

    // Constructors
    QScriptValue ccfgClass = engine->scriptValueFromQMetaObject<ScrCcfg>();
    global.setProperty("CarbonCfg", ccfgClass);

    QScriptValue resetGenCtor = engine->newFunction(constructCarbonCfgResetGen);
    engine->globalObject().setProperty("CarbonCfgResetGen", resetGenCtor);
 
    QScriptValue clkGenCtor = engine->newFunction(constructCarbonCfgClockGen);
    engine->globalObject().setProperty("CarbonCfgClockGen", clkGenCtor);

    QScriptValue tieCtor = engine->newFunction(constructCarbonCfgTie);
    engine->globalObject().setProperty("CarbonCfgTie", tieCtor);

    QScriptValue tieParamCtor = engine->newFunction(constructCarbonCfgTieParam);
    engine->globalObject().setProperty("CarbonCfgTieParam", tieParamCtor);

    QScriptValue memLocPortctor = engine->newFunction(constructCarbonCfgMemoryLocPort);
    engine->globalObject().setProperty("CarbonCfgMemoryLocPort", memLocPortctor);

    QScriptValue memLocRTLctor = engine->newFunction(constructCarbonCfgMemoryLocRTL);
    engine->globalObject().setProperty("CarbonCfgMemoryLocRTL", memLocRTLctor);

    QScriptValue xtorConnCtor = engine->newFunction(constructCarbonCfgXtorConn);
    engine->globalObject().setProperty("CarbonCfgXtorConn", xtorConnCtor);

    QScriptValue regFieldCtor = engine->newFunction(constructCarbonCfgRegisterField);
    engine->globalObject().setProperty("CarbonCfgRegisterField", regFieldCtor);

    QScriptValue regLocConstantCtor = engine->newFunction(constructCarbonCfgRegisterLocConstant);
    engine->globalObject().setProperty("CarbonCfgRegisterLocConstant", regLocConstantCtor);

    QScriptValue regLocUserCtor = engine->newFunction(constructCarbonCfgRegisterLocUser);
    engine->globalObject().setProperty("CarbonCfgRegisterLocUser", regLocUserCtor);

    QScriptValue regLocRegCtor = engine->newFunction(constructCarbonCfgRegisterLocReg);
    engine->globalObject().setProperty("CarbonCfgRegisterLocReg", regLocRegCtor);

    QScriptValue regLocArrayCtor = engine->newFunction(constructCarbonCfgRegisterLocArray);
    engine->globalObject().setProperty("CarbonCfgRegisterLocArray", regLocArrayCtor);

    // Prototypes
    ProtoCarbonCfgRegister* regProto = new ProtoCarbonCfgRegister();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgRegister*>(),
      engine->newQObject(regProto));

    ProtoCarbonCfgCustomCode* customCodeProto = new ProtoCarbonCfgCustomCode();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgCustomCode*>(),
                                engine->newQObject(customCodeProto));

    ProtoCarbonCfgCompCustomCode* compCustomCodeProto = new ProtoCarbonCfgCompCustomCode();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgCompCustomCode*>(),
                                engine->newQObject(compCustomCodeProto));

    ProtoCarbonCfgCadiCustomCode* cadiCustomCodeProto = new ProtoCarbonCfgCadiCustomCode();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgCadiCustomCode*>(),
                                engine->newQObject(cadiCustomCodeProto));

    ProtoCarbonCfgRegCustomCode* regCustomCodeProto = new ProtoCarbonCfgRegCustomCode();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgRegCustomCode*>(),
                                engine->newQObject(regCustomCodeProto));

    ProtoCarbonCfgMemoryBlockCustomCode* memoryBlockCustomCodeProto = new ProtoCarbonCfgMemoryBlockCustomCode();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgMemoryBlockCustomCode*>(),
                                engine->newQObject(memoryBlockCustomCodeProto));

    ProtoCarbonCfgMemoryCustomCode* memoryCustomCodeProto = new ProtoCarbonCfgMemoryCustomCode();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgMemoryCustomCode*>(),
                                engine->newQObject(memoryCustomCodeProto));

    ProtoCarbonCfgMemory* memProto = new ProtoCarbonCfgMemory();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgMemory*>(),
      engine->newQObject(memProto));

    ProtoCarbonCfgMemoryLoc* memLocProto = new ProtoCarbonCfgMemoryLoc();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgMemoryLoc*>(),
      engine->newQObject(memLocProto));

    ProtoCarbonCfgMemoryLocRTL* memLocRTLProto = new ProtoCarbonCfgMemoryLocRTL();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgMemoryLocRTL*>(),
      engine->newQObject(memLocRTLProto));

    ProtoCarbonCfgMemoryLocPort* memLocPortProto = new ProtoCarbonCfgMemoryLocPort();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgMemoryLocPort*>(),
      engine->newQObject(memLocPortProto));

    ProtoCarbonCfgMemoryLocUser* memLocUserProto = new ProtoCarbonCfgMemoryLocUser();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgMemoryLocUser*>(),
      engine->newQObject(memLocUserProto));

    ProtoCarbonCfgMemoryBlock* memBlockProto = new ProtoCarbonCfgMemoryBlock();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgMemoryBlock*>(),
      engine->newQObject(memBlockProto));

    ProtoCarbonCfgRegisterLoc* regLocProto = new ProtoCarbonCfgRegisterLoc();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgRegisterLoc*>(),
      engine->newQObject(regLocProto));

    ProtoCarbonCfgRegisterLocConstant* regLocConstProto = new ProtoCarbonCfgRegisterLocConstant();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgRegisterLocConstant*>(),
      engine->newQObject(regLocConstProto));

    ProtoCarbonCfgRegisterLocReg* regLocRegProto = new ProtoCarbonCfgRegisterLocReg();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgRegisterLocReg*>(),
      engine->newQObject(regLocRegProto));

    ProtoCarbonCfgRegisterLocArray* regLocArrayProto = new ProtoCarbonCfgRegisterLocArray();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgRegisterLocArray*>(),
      engine->newQObject(regLocArrayProto));

    ProtoCarbonCfgRegisterField* regFieldProto = new ProtoCarbonCfgRegisterField();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgRegisterField*>(),
      engine->newQObject(regFieldProto));

    ProtoCarbonCfgGroup* regGroup = new ProtoCarbonCfgGroup();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgGroup*>(),
      engine->newQObject(regGroup));

    ProtoCarbonCfgELFLoader* elfLoader = new ProtoCarbonCfgELFLoader();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgELFLoader*>(),
      engine->newQObject(elfLoader));

    ProtoCarbonCfgProcInfo* procInfo = new ProtoCarbonCfgProcInfo();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgProcInfo*>(),
      engine->newQObject(procInfo));

    ProtoCarbonCfgXtorInstance* xproto = new ProtoCarbonCfgXtorInstance();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgXtorInstance*>(),
      engine->newQObject(xproto));

    ProtoCarbonCfgESLPort* eslProto = new ProtoCarbonCfgESLPort();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgESLPort*>(),
      engine->newQObject(eslProto));

    ProtoCarbonCfgRTLPort* rtlProto = new ProtoCarbonCfgRTLPort();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgRTLPort*>(),
      engine->newQObject(rtlProto));

    ProtoCarbonCfgRTLConnection* rtlConn = new ProtoCarbonCfgRTLConnection();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgRTLConnection*>(),
      engine->newQObject(rtlConn));

    ProtoCarbonCfgResetGen* resetProto = new ProtoCarbonCfgResetGen();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgResetGen*>(),
      engine->newQObject(resetProto));

    ProtoCarbonCfgClockGen* clockProto = new ProtoCarbonCfgClockGen();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgClockGen*>(),
      engine->newQObject(clockProto));

    ProtoCarbonCfgTie* tieProto = new ProtoCarbonCfgTie();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgTie*>(),
      engine->newQObject(tieProto));

    ProtoCarbonCfgTieParam* tieParamProto = new ProtoCarbonCfgTieParam();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgTieParam*>(),
      engine->newQObject(tieParamProto));

    ProtoCarbonCfgXtorConn* xtorConnProto = new ProtoCarbonCfgXtorConn();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgXtorConn*>(),
      engine->newQObject(xtorConnProto));

    ProtoCarbonCfgXtor* xtorProto = new ProtoCarbonCfgXtor();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgXtor*>(),
      engine->newQObject(xtorProto));

    ProtoCarbonCfgXtorPort* xtorPortProto = new ProtoCarbonCfgXtorPort();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgXtorPort*>(),
      engine->newQObject(xtorPortProto));

    ProtoCarbonCfgXtorLib* xtorLibProto = new ProtoCarbonCfgXtorLib();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgXtorLib*>(),
      engine->newQObject(xtorLibProto));

    ProtoCarbonCfgXtorParam* xtorParmProto = new ProtoCarbonCfgXtorParam();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgXtorParam*>(),
      engine->newQObject(xtorParmProto));

    ProtoCarbonCfgXtorParamInst* xtorParmInstProto = new ProtoCarbonCfgXtorParamInst();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgXtorParamInst*>(),
      engine->newQObject(xtorParmInstProto));

    ProtoCarbonCfgTemplate* templateProto = new ProtoCarbonCfgTemplate();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgTemplate*>(),
      engine->newQObject(templateProto));

    ProtoCarbonCfgCadi* cadiProto = new ProtoCarbonCfgCadi();
    engine->setDefaultPrototype(qMetaTypeId<CarbonCfgCadi*>(),
      engine->newQObject(cadiProto));



   

  }
}

ScrCcfg::ScrCcfg(QObject* parent, CarbonCfg* cfg) : QObject(parent), mCfg(cfg)
{
  qDebug() << "Constructing ScrCcfg";

  // If the caller didn't have a prebuilt cfg, build one for them
  if (mCfg == NULL) {
    mCfg = new CarbonCfgTop();
  }
}

CcfgEnum::CarbonCfgStatus ScrCcfg::read(CcfgEnum::CarbonCfgMode mode, const QString& filename)
{
  QFileInfo fi(filename);
  TempChangeDirectory cd(fi.absolutePath());

  mCfg->putCcfgMode(static_cast<CarbonCfgMode>(mode));

  qDebug() << "reading" << fi.fileName() << "from" << fi.absolutePath();

  mFilePath = filename;

  UtString name;
  name << filename;
  CarbonCfgStatus status = mCfg->read(name.c_str());

  if ((getXtorLib() == NULL) || getXtorLib()->empty())
    mCfg->readXtorLib(eCarbonXtorsMaxsim);

  return static_cast<CcfgEnum::CarbonCfgStatus>(status);
}

CcfgEnum::CarbonCfgStatus ScrCcfg::readNoCheck(CcfgEnum::CarbonCfgMode mode, const QString& filename)
{
  QFileInfo fi(filename);
  TempChangeDirectory cd(fi.absolutePath());

  mCfg->putCcfgMode(static_cast<CarbonCfgMode>(mode));

  qDebug() << "reading" << fi.fileName() << "from" << fi.absolutePath();

  UtString name;
  name << filename;

  mFilePath = filename;

  CarbonCfgStatus status = mCfg->readNoCheck(name.c_str());

  qDebug() << "readnoCheckStatus" << status << "message" << mCfg->getErrmsg();

  if ((getXtorLib() == NULL) || getXtorLib()->empty())
    mCfg->readXtorLib(eCarbonXtorsMaxsim);

  return static_cast<CcfgEnum::CarbonCfgStatus>(status);
}

CcfgEnum::CarbonCfgStatus ScrCcfg::write(const QString& filename)
{
  UtString name;
  name << filename;
  CarbonCfgStatus status = mCfg->write(name.c_str());
  return static_cast<CcfgEnum::CarbonCfgStatus>(status);
}

CcfgEnum::CarbonCfgStatus ScrCcfg::write()
{
  if (!mFilePath.isEmpty())
  {
    QFileInfo fi(mFilePath);
    TempChangeDirectory cd(fi.absolutePath());
    QString filename = mFilePath;
    UtString name;
    name << filename;

    qDebug() << "writing ccfg to" << mFilePath;

    CarbonCfgStatus status = mCfg->write(name.c_str());
    return static_cast<CcfgEnum::CarbonCfgStatus>(status);
  }
  else
  {
    QScriptContext* ctx = context();
    if (ctx)
      ctx->throwError("No file was previously read, use write(filename) instead");
    return CcfgEnum::Failure;
  }
}

CarbonCfgTemplate* ScrCcfg::getTemplate()
{
  return mCfg->getTemplate();
}

void ScrCcfg::setCompName(const QString& newVal)
{
  UtString nv; nv << newVal; 
  mCfg->putCompName(nv.c_str());

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  if (pw && pw->project())
  {
    MaxsimComponent* maxsim = pw->getActiveMaxsimComponent();
    CowareComponent* coware = pw->getActiveCowareComponent();
    SystemCComponent* systemc = pw->getActiveSystemCComponent();

    switch (mCfg->getCcfgMode())
    {
    case eCarbonCfgARM:
      if (maxsim)
        maxsim->setComponentName(newVal);
      break;
    case eCarbonCfgCOWARE:
      if (coware)
        coware->setComponentName(newVal);
      break;
    case eCarbonCfgSYSTEMC:
      if (systemc)
        systemc->setComponentName(newVal);
      break;

    default:
      break;
    }
  }
}

void ScrCcfg::setPCTraceAccessor(const QString& newVal)
{
  UtString nv;
  nv << newVal;
  mCfg->putPCTraceAccessor(nv.c_str());
}

QScriptValue ScrCcfg::clone(QVariant obj)
{
  CarbonCfgMemory* mem = qvariant_cast<CarbonCfgMemory*>(obj);
  if (mem)
    return engine()->toScriptValue(cloneMemory(mem));
 
  CarbonCfgCompCustomCode* ccode = qvariant_cast<CarbonCfgCompCustomCode*>(obj);
  if (ccode)
    return engine()->toScriptValue(cloneCustomCode(ccode));

  CarbonCfgCadi* cadi = qvariant_cast<CarbonCfgCadi*>(obj);
  if (cadi)
    cloneCadi(cadi);

  CarbonCfgProcInfo* proci = qvariant_cast<CarbonCfgProcInfo*>(obj);
  if (proci)
    cloneProcInfo(proci);

  return QScriptValue();
}

