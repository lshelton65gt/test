//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "Template.h"
#include "Attributes.h"
#include "Attribute.h"
#include "Ports.h"

Attribute::AttributeType Attribute::fromString(const QString& val)
{
  if ("integer" == val)
    return Attribute::Integer;
  else if ("bool" == val)
    return Attribute::Bool;
  else if ("enum" == val)
    return Attribute::Enum;
  else if ("string" == val)
    return Attribute::String;

 return Attribute::Undefined;
}

bool Attribute::deserialize(Template*, Attributes* atts, const QDomElement& e, XmlErrorHandler* eh)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
 
    if ("Attribute" == element.tagName())
    {
      if (element.hasAttribute("name") && element.hasAttribute("type"))
      {
        AttributeType type = fromString(element.attribute("type"));
        if (type != Attribute::Undefined)
        {
          Attribute* att = new Attribute();
          att->setType(type);
          att->setName(element.attribute("name"));
          atts->addAttribute(att);
        }
        else
        {
          eh->reportProblem(XmlErrorHandler::Error, element, QString("Attribute has illegal type attribute: %1").arg(element.attribute("type")) );
          return false;
        }
      }
      else
      {
        eh->reportProblem(XmlErrorHandler::Error, element, "Attribute is missing one or more 'name', 'type' attributes" );
        return false;
      }
    }
    else if (!element.isText())
    {
      eh->reportProblem(XmlErrorHandler::Error, element, QString("Unknown Attributes Element: %1\n").arg(element.tagName()) );
      return false;
    }
    n = n.nextSibling();
  }
  return true;
}

