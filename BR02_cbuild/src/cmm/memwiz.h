#ifndef MEMWIZ_H
#define MEMWIZ_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QtGui>
#include <QtXml>
#include <QtScript>

#include "ui_memwiz.h"

class Template;

class memwiz : public QMainWindow
{
  Q_OBJECT

public:
  memwiz(QWidget *parent = 0, Qt::WFlags flags = 0);
  ~memwiz();

  QTextBrowser* textBrowser() const { return ui.textBrowser; }

private:
  Ui::memwizClass ui;

private slots:
  void on_pushButtonWizard_clicked();

};

extern memwiz* theApp;

#endif // MEMWIZ_H
