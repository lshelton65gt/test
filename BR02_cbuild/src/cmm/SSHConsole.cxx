//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"

#include "gui/CQt.h"
#include "util/UtString.h"

#include "CarbonProjectWidget.h"
#include "SSHConsole.h"
#include "SSHConsoleHelper.h"

#define PLINK_LOGIN_SUCCESS1 "Access granted"
#define PLINK_LOGIN_SUCCESS2 "Authentication successful"
#define PLINK_STORE_KEY "Store key in cache? (y/n)"
#define PLINK_CONT_OK "Continue with connection? (y/n)"
#define PLINK_INVALID_PASSWORD "Access denied"
#define PLINK_SERVER_REJECTED "Connection abandoned."
#define PLINK_START_SESSION_RX "^Looking up host \"(.*)\""
#define PLINK_LOGGED_IN_RX "^Using username \"(.*)\"\\."
#define PLINK_SHELL_STARTED_RX "^Started a shell/command"
#define PLINK_SHELL_STARTED "CARBON SHELL STARTED"
#define PLINK_CARBON_EXIT "CARBON EXIT STATUS"
#define PLINK_CARBON_EXIT_HIDDEN "CARBON HIDDEN EXIT STATUS"
#define PLINK_CARBON_EXIT_RX ".*CARBON EXIT STATUS: (.*)$"
#define PLINK_CARBON_EXIT_HIDDEN_RX ".*CARBON HIDDEN EXIT STATUS: (.*)$"
#define PLINK_BAD_CONN "Unable to open connection:"
#define PLINK_BAD_HOST "Host does not exist"
#define CARBON_VERSION_CHECK_COMPLETED "CARBON VERSION CHECK COMPLETED"
#define CARBON_FILE_EXISTS_COMPLETED_RX "CARBON FILE EXISTS: (.*)"
#define CARBON_FILE_NOT_EXISTS_COMPLETED_RX "CARBON FILE NOT FOUND: (.*)"

#define CARBON_PLINK_ARGS "CARBON_PLINK_ARGS"

SSHConsole::SSHConsole(QWidget* parent)
{
  mMagicFileName = "";
  mUsePassword = false;
  mProject = NULL;
  mProcess = new QProcess(this);

  mLoginState = SSHConsole::Unknown;
  mParent = parent;
  mExitCode = -1;
  mShutdown = false;

  CQT_CONNECT(mProcess, started(), this, procStarted());
  CQT_CONNECT(mProcess, finished(int), this, procFinished(int));
  CQT_CONNECT(mProcess, error(QProcess::ProcessError), this, procError(QProcess::ProcessError));
  CQT_CONNECT(mProcess, readyReadStandardError(), this, procReadStderr());
  CQT_CONNECT(mProcess, readyReadStandardOutput(), this, procReadStdout());
}

// Initiate the plink session with server, username, password
// will be prompted for only if necessary, and one has not been
// provided via putPassword
// will emit sessionReady(pid) when it is connected and logged in
void SSHConsole::startSession(CarbonProject* proj, const QString& serverName, const QString& userName)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  INFO_ASSERT(mProcess->state() != QProcess::Running, "Process already running");
  
  mProject = proj;
  mUsername = userName;
  mServer = serverName;

  UtString err;
  UtString plinkProgram;


  UtString startMsg;
  startMsg << "Initiating SSH connection: " << userName << "@" << serverName;
  processOutputLine(startMsg.c_str());

#if pfWINDOWS
  QString plinkProg = QString("%1/Win/bin/plink.exe").arg(getenv("CARBON_HOME"));
  plinkProgram << plinkProg;
#else
  OSExpandFilename(&plinkProgram,"$CARBON_HOME/bin/plink",&err);
#endif

  // Check for plink existence
  QFileInfo fi(plinkProgram.c_str());
  if (!fi.exists())
  {
    UtString errMsg;
    errMsg << "Error: unable to locate program: " << plinkProgram.c_str();
    processOutputLine(errMsg.c_str());
    emit commandCompleted(-1);
    procFinished(-1);
  }

  mProgram.clear();

#if pfWINDOWS
  mProgram = QString("\"%1\"").arg(plinkProgram.c_str());
#else
  mProgram = plinkProgram.c_str();
#endif


  UtString userInfo;
  userInfo << userName << "@" << serverName;

  // Arguments passed to plink
  mArgs.clear();

  mArgs << "-v" << userInfo.c_str();

  // User can supply extra arguments to plink by using the
  // env. var CARBON_PLINK_ARGS
  const char* plinkArgs = getenv(CARBON_PLINK_ARGS);
  if (plinkArgs)
  {
    QString extra = plinkArgs;
    QStringList extraArgs = extra.split(" ");
    foreach (QString arg, extraArgs)
      mArgs << arg;
  }

  qDebug() << mProgram << mArgs;

  // Fire up plink
  mProcess->start( mProgram, mArgs );

 QApplication::restoreOverrideCursor();

}
// Read from Standard Output
void SSHConsole::procReadStdout()
{
  if (mProcess == NULL)
    return;

  QString textRead = QString::fromLocal8Bit(mProcess->readAllStandardOutput());
  QStringList linesRead = textRead.split('\n');
  foreach (QString line, linesRead)
  {
    QString asciiLine = line.trimmed();
    UtString theLine;
    theLine << asciiLine;
    qDebug() << "stdout: " << theLine.c_str();
    if (!processLine(asciiLine))
      break;
  }
}

// Execute a remote command asynchronously
void SSHConsole::executeCommand(CarbonProject* proj, const QString& workingDir, QStringList& envVars, const QString& program, const QStringList& args, bool hiddenCmd)
{
  INFO_ASSERT(mLoginState == SSHConsole::LoggedIn, "Expecting user to be logged in");
  
  mProject = proj;
  mCommandProgram = program;
  mCommandArgs = args; 
  mProgramRunning = true;

  QString cmdLine;
  QTextStream cmdStream(&cmdLine);

  if (useProject())
    buildCommandLine(workingDir, cmdStream);

  // Add additional env. vars
  foreach (QString var, envVars)
  {
    UtString exportCmd;
    exportCmd << "export " << var;
    writeHiddenCommand(exportCmd.c_str());
  }

  cmdStream << mCommandProgram << " ";
  foreach (QString arg, mCommandArgs)
  {
    // Do we need to quote the arguments?
    if (arg.contains(' ') && !arg.startsWith("'"))
      cmdStream << " '" << arg << "'";
    else
      cmdStream << " " << arg;
  }

  UtString theCmd;
  theCmd << cmdLine;
  if (hiddenCmd)
    writeHiddenCommand(theCmd.c_str()); 
  else
    writeCommand(theCmd.c_str());
  
  // Grab the exit status (hidden)
  UtString cmdExitStatus;
  cmdExitStatus << "/bin/echo " << PLINK_CARBON_EXIT << ": $?";
  writeHiddenCommand(cmdExitStatus.c_str());
}

void SSHConsole::addEnvVar(const QString& name, const QString& value)
{
  if (value.length() > 0)
  {
    UtString exportCmd;
    exportCmd << "export " << name << "=" << value;
    writeHiddenCommand(exportCmd.c_str());
  }
}

// We are using a Project, so setup env. vars and command line
void SSHConsole::buildCommandLine(const QString& workingDir, QTextStream& cmdLine)
{
  INFO_ASSERT(useProject(), "Project must be defined");
  
  QString remoteDir = mProject->getRemoteWorkingDirectory();

  // Stuff in the Carbon env. vars
  addCarbonEnvironmentVariables();

  QFileInfo fi(workingDir);

  cmdLine << "cd ~/\n";
  if (remoteDir.length() > 0)
  {
    if (fi.isRelative())
      cmdLine << "cd " << remoteDir << "/" << workingDir << "\n";
    else
      cmdLine << "cd " << workingDir << "\n";
  }

  QString remotePrefix = mProject->getProjectOptions()->getValue("Remote Command Prefix")->getValue();
  if (remotePrefix.length() > 0)
    cmdLine << remotePrefix << " ";
}

void SSHConsole::addCarbonEnvironmentVariables()
{
  QString remoteCarbonHome = mProject->getProjectOptions()->getValue("Remote CARBON_HOME")->getValue();

  if (remoteCarbonHome.length() > 0)
    addEnvVar("CARBON_HOME", remoteCarbonHome);

  // CARBON_CONFIGURATION
  UtString confName;
  confName << mProject->getActivePlatform() << "/" << mProject->getActiveConfiguration();
  addEnvVar("CARBON_CONFIGURATION", confName.c_str());

  // CARBON_MODEL
  UtString carbonModel;
  carbonModel << mProject->getActive()->getOptions("VSPCompiler")->getValue("-o")->getValue();
  addEnvVar("CARBON_MODEL", carbonModel.c_str());

  // CARBON_OUTPUT
  UtString carbonOutput;
  carbonOutput << mProject->getRemoteWorkingDirectory() << "/" << confName;
  addEnvVar("CARBON_OUTPUT", carbonOutput.c_str());

  // CARBON_PROJECT
  addEnvVar("CARBON_PROJECT", mProject->getRemoteWorkingDirectory());

  // CARBON_PROJECT_RELATIVE
  addEnvVar("CARBON_PROJECT_RELATIVE", "../..");
}

// Read from Standard Error
void SSHConsole::procReadStderr()
{
  if (mProcess == NULL)
    return;

  QString textRead = QString::fromLocal8Bit(mProcess->readAllStandardError());

  QStringList linesRead = textRead.split('\n');
  foreach (QString line, linesRead)
  {
    QString asciiLine = line.trimmed();
    UtString theLine;
    theLine << asciiLine;
    qDebug() << "stderr: " << theLine.c_str();
    if (!processLine(asciiLine, true))
      break;
  }
}

void SSHConsole::scanSessionInitialize(const QString& line, bool isStandardError)
{
  QRegExp sStart(PLINK_START_SESSION_RX);
  if (sStart.exactMatch(line))
  {
    emit sessionLoginStatus(line);
    return;
  }

  QRegExp sBadConn(PLINK_BAD_CONN);
  if (sBadConn.exactMatch(line))
  {
    emit sessionLoginStatus(line);
    return;
  }

  QRegExp sBadHost(PLINK_BAD_HOST);
  if (sBadHost.exactMatch(line))
  {
    emit sessionLoginStatus(line);
    return;
  }

  // If user supplies arguments, be more verbose with errors
  if (isStandardError && getenv(CARBON_PLINK_ARGS))
  {
    emit sessionLoginStatus(line);
  }
}


void SSHConsole::writeHiddenCommand(const QString& cmd)
{
  qDebug() << "sshhid: " << cmd;
  UtString theCmd;
  theCmd << cmd;

  mProcess->write(theCmd.c_str());
  mProcess->write("\n");
}

void SSHConsole::writeCommand(const QString& cmd)
{
  qDebug() << "sshcmd: " << cmd;
 
  // Echo commands only during user shown checks or afer login
  if ( getState() == SSHConsole::LoggedIn 
    || getState() == SSHConsole::RemoteVersionCheck
    || getState() == SSHConsole::RemoteDirCheck )
  {
    UtString theCmd;
    theCmd << cmd.trimmed();
    processOutputLine(theCmd.c_str());
  }

  UtString theCmd;
  theCmd << cmd;

  mProcess->write(theCmd.c_str());
  mProcess->write("\n");
}

void SSHConsole::startShell()
{
  // Start Bourne Shell
  writeCommand("/bin/sh");
  // Hide the prompt
  writeCommand("PS1="); 
  writeCommand("PS2="); 
  // Don't echo commands
  writeCommand("stty -echo");
  UtString startCmd;
  startCmd << "/bin/echo " << PLINK_SHELL_STARTED ;
  writeCommand(startCmd.c_str());
  setState(SSHConsole::ShellStarting);
}

void SSHConsole::setState(LoginState newState)
{
  bool broadCastChange = false;
  if (newState != mLoginState)
    broadCastChange = true;

  mLoginState = newState;

  if (broadCastChange)
    emit sessionStateChanged(newState);
}
// if we are not yet logged in, then special handling
bool SSHConsole::processLine(const QString& line, bool isStandardError)
{
  scanSessionInitialize(line, isStandardError);

  if (getState() == SSHConsole::LoggedIn)
    return processOutputLine(line);
  else if (mProcess)  // Handle the login state machine
  {
    switch (getState())
    {
    default:
      qDebug() << "No handler for state: " << getState();
      break;
    case SSHConsole::RemoteDirCheck:
      {
        // if we are here, we already know
        // we are using the project
        INFO_ASSERT(useProject(), "Expecting to use Project");

        QRegExp expSuccess(CARBON_FILE_EXISTS_COMPLETED_RX);
        QRegExp expFailure(CARBON_FILE_NOT_EXISTS_COMPLETED_RX);

        bool dircheckValid = false;

        if (expSuccess.exactMatch(line))
        {
          QFileInfo fi(mMagicFileName);
          if (fi.exists())
          {
            QFile mf(mMagicFileName);
            mf.remove();
            qDebug() << "removed magic file: " << mMagicFileName;
            mMagicFileName = "";
          }
          dircheckValid = true;
        }

        // Failure
        if (expFailure.exactMatch(line))
        {
          processOutputLine("Error: The Project's Remote Working Directory:");
          processOutputLine("  '" + mProject->getRemoteWorkingDirectory() + "'");
          processOutputLine("does not match the Windows Project Directory:");
          processOutputLine("  '" + QString(mProject->getProjectDirectory()) + "'");
          processOutputLine("Please verify and correct the following parameters:");
          processOutputLine("Remote Server: " + QString(mProject->getProjectOptions()->getValue("Remote Server")->getValue()));
          processOutputLine("Remote Username: " + QString(mProject->getProjectOptions()->getValue("Remote Username")->getValue()));
          processOutputLine("Remote Working Directory: " + mProject->getRemoteWorkingDirectory());

          emit commandCompleted(-1);
          procFinished(-1);
          abortSession();
          return false;
        }

        // When validated, send this:
        if (dircheckValid)
        {
          setState(SSHConsole::RemoteVersionCheck);
          sendVersionCheckCommand();
        }
      }
      break;
    case SSHConsole::FailedLogin:
      break;
    case SSHConsole::RemoteVersionCheck:
      {
        processOutputLine(line);
        qDebug() << "Vercheck: " << line;
        if (line == CARBON_VERSION_CHECK_COMPLETED)
        {
          if (mVersionCheckLine == CARBON_RELEASE_ID)
          {
            setState(SSHConsole::LoggedIn);
            emit processReadyForCommands();
          }
          else
          {            
            UtString errorString;
            errorString << "Error: Incorrect Remote Carbon Software, expecting version: " << CARBON_RELEASE_ID << "\n";
            errorString << "Received: " << mVersionCheckLine << "\n";
            errorString << "Please install or configure Remote CARBON_HOME to the matching version." << "\n";
            QString errMsg = errorString.c_str();
            processOutputLine(errMsg);
            abortSession();
            return false;
          }
        }
        if (line.length() > 0)
          mVersionCheckLine = line;
      }
      break;
    case SSHConsole::ShellStarting:
      {
        if (line == PLINK_SHELL_STARTED)
        {
          if (useProject())
          {
            setState(SSHConsole::RemoteDirCheck);
            checkWorkingDirectory();
          }
          else
          {
            setState(SSHConsole::LoggedIn);
            emit processReadyForCommands();
          }
        }
      }
      break;
    case SSHConsole::Started:
      setState(LoggingIn); // Fall through here now to LoggingIn (on purpose)
    case SSHConsole::LoggingIn:
      {
        UtString prompt;
        prompt << mUsername << "@" << mServer << "*'s password:";
        QRegExp promptExp(prompt.c_str());
        QRegExp prompt1Exp("Password:");

        if (promptExp.exactMatch(line) || prompt1Exp.exactMatch(line))
        {
          QString password;
          if (mUsePassword)
            password = mPassword;
          else // Prompt for the password
          {
            bool ok;
            
            UtString ask;
            ask << mUsername << "@" << mServer << "'s Password:";

            password = QInputDialog::getText(mParent,
                MODELSTUDIO_TITLE, ask.c_str(), QLineEdit::Password, "", &ok);
            if (!ok)
            {
              setState(SSHConsole::FailedLogin);
              abortSession();
              return false;
            }  
          }
          UtString clearPassword;
          clearPassword << password;
#if pfWINDOWS
          clearPassword << "\r\n";
#else
          clearPassword << "\n";
#endif
          mProcess->write(clearPassword.c_str());

          setState(SSHConsole::Authorizing);
        }
        else if (line == PLINK_LOGIN_SUCCESS1 || line == PLINK_LOGIN_SUCCESS2)
          startShell();
        else if (line == PLINK_STORE_KEY)
          mProcess->write("n\n");
        else if (line == PLINK_CONT_OK)
          mProcess->write("y\n");
      }
      break;
    case SSHConsole::RetryPassword:
        setState(SSHConsole::Authorizing);// Skip the second "Access Denied"
      break;
    case SSHConsole::Authorizing:
      {
        // yahoo
        if (line == PLINK_LOGIN_SUCCESS1 || line == PLINK_LOGIN_SUCCESS2)
        {
          startShell();
        } // SSH Key first time
        else if (line == PLINK_STORE_KEY)
          mProcess->write("n\n");
        else if (line == PLINK_CONT_OK)
          mProcess->write("y\n");
        // Access Denied
        else if (line == PLINK_INVALID_PASSWORD || line == PLINK_SERVER_REJECTED)
        {
          bool ok;
          UtString prompt;
          prompt << "Invalid password, please re-enter or cancel to abort\nPassword:";
          QString password = QInputDialog::getText(mParent,
              MODELSTUDIO_TITLE, prompt.c_str(), QLineEdit::Password, "", &ok);
          if (ok)
          {
            setState(SSHConsole::RetryPassword);
            UtString thePassword;
            thePassword << password;
#if pfWINDOWS
            thePassword << "\r\n";
#else
            thePassword << "\n";
#endif
            mProcess->write(thePassword.c_str());
          }
          else
          {
            setState(SSHConsole::FailedLogin);
            abortSession();
            return false;
          }
        }
      }
      break;
    }
  }

  return true;
}

// This is called only after we are logged in
bool SSHConsole::processOutputLine(const QString& line)
{
  QRegExp exitRx(PLINK_CARBON_EXIT_RX);
  if (exitRx.exactMatch(line))
  {
    QString exitValue = exitRx.cap(1);
    emit commandCompleted(exitValue.toInt());
    mProgramRunning = false;
    return true;
  }

  QRegExp exitHiddenRx(PLINK_CARBON_EXIT_HIDDEN_RX);
  if (exitHiddenRx.exactMatch(line))
  {
    QString exitValue = exitHiddenRx.cap(1);
    int exitStatus = exitValue.toInt();
    if (exitStatus != 0) // We have finished with an error
    {
      emit commandCompleted(exitValue.toInt());
      mProgramRunning = false;

      // If the user gives us the wrong CARBON_HOME
      // or anything else environmental is wrong
      // abort the session because the VersionCheck bailed.
      if (getState() == SSHConsole::RemoteVersionCheck)
        abortSession();

      return true;
    }
    else // Don't display these, just check them
      return true;
  }

  if (line.length() > 0)
    emit processOutput(line);
  
  return true;
}

// Handle errors on startup
void SSHConsole::procError(QProcess::ProcessError err)
{
  const char* emsg = "Unknown error";
  switch (err) 
  {
  case QProcess::FailedToStart: emsg = "FailedToStart"; break;
  case QProcess::Crashed: emsg = "Terminated"; break;
  case QProcess::Timedout: emsg = "Timedout"; break;
  case QProcess::WriteError: emsg = "WriteError"; break;
  case QProcess::ReadError: emsg = "ReadError"; break;
  case QProcess::UnknownError: emsg = "UnknownError"; break;
  }

  UtString errMsg;
  errMsg << "Unable to launch: " << mProgram << "\n" << emsg;

  if (err != QProcess::Crashed && mExitCode != -1)
    QMessageBox::critical(NULL, MODELSTUDIO_TITLE, errMsg.c_str());
}

// Process has started, signal that to the caller
void SSHConsole::procStarted()
{
  qDebug() << "SSH Process started PID=" << mProcess->pid();
  setState(Started);
}

void SSHConsole::procFinished(int exitCode)
{
  if (mExitCode == -1)
    mExitCode = exitCode;

  qDebug() << "SSH Process Exited ExitCode=" << mExitCode;

  setState(Terminated);

  emit processTerminated(exitCode);
}

SSHConsole::~SSHConsole()
{  
  abortSession();
}

// Validate that the Remote Working Directory matches
// the project directory.
void SSHConsole::checkWorkingDirectory()
{
  INFO_ASSERT(mProject, "Expecting a project");
  mMagicFileName = "";

  // To check the working directory.
  // Write write the project guid to a file called
  // .carbon.{project-guid} in the project directory
  // from windows. then we check it's existence
  // from unix
  UtString magicFileName;
  UtString projectGUID;
  projectGUID << mProject->getGUID();

  UtString fileName;
  fileName << ".carbon." << projectGUID;
  OSConstructFilePath(&magicFileName, mProject->getProjectDirectory(), fileName.c_str());

  QFile magicFile(magicFileName.c_str());
  if (!magicFile.open(QFile::WriteOnly | QFile::Text))
  {
    UtString errMsg;
    errMsg << "Unable to verify working directory.  Cannot create file: " << magicFileName.c_str();
    processOutput(errMsg.c_str());
    abortSession();
    return;
  }

  mMagicFileName = magicFileName.c_str();

  qDebug() << "Creating magic file: " << magicFileName.c_str();

  UtString remoteWorkingDir;
  remoteWorkingDir << mProject->getRemoteWorkingDirectory();

  UtString unixFileName;
  OSConstructFilePath(&unixFileName, remoteWorkingDir.c_str(), fileName.c_str());

  // Write the guid into the file just to have something
  magicFile.write(projectGUID.c_str());
  magicFile.close();

  QString checkFileScript = "~/.carbon-check-file";

  UtString scriptCommand;
  scriptCommand << SSHConsoleHelper::shellCreateTestFileScript(checkFileScript);
  scriptCommand << checkFileScript << " '" << unixFileName.c_str() << "'" << "\n";

  writeHiddenCommand(scriptCommand.c_str());

  UtString cmdExitStatus;
  cmdExitStatus << "/bin/echo " << PLINK_CARBON_EXIT_HIDDEN << ": $?";
  writeHiddenCommand(cmdExitStatus.c_str());
}

// Validate that the identical version of 
// Carbon software is being run on the remote system.
void SSHConsole::sendVersionCheckCommand()
{
  INFO_ASSERT(mProject, "Expecting a project");

  QString cmdLine;
  QTextStream cmdStream(&cmdLine);

  QString workingDir;
  buildCommandLine(workingDir, cmdStream);

  CarbonConfiguration* activeConfig = mProject->getActive();

  UtString makefileDir;
  mProject->getRemoteWorkingDirectory();

  UtString projectDir;
  projectDir << mProject->getRemoteWorkingDirectory();

  OSConstructFilePath(&makefileDir, projectDir.c_str(), activeConfig->getPlatform());
  OSConstructFilePath(&makefileDir, makefileDir.c_str(), activeConfig->getName());

  QStringList args;
  args << "-C" << makefileDir.c_str() << "-f" << "Makefile.carbon" << "CarbonVersionCheck";

  cmdStream << "make";

  foreach (QString arg, args)
  {
    // Do we need to quote the arguments?
    if (arg.contains(' ') && !arg.startsWith("'"))
      cmdStream << " '" << arg << "'";
    else
      cmdStream << " " << arg;
  }

  UtString theCmd;
  theCmd << cmdLine;

  writeCommand(theCmd.c_str());

  UtString cmdExitStatus;
  cmdExitStatus << "/bin/echo " << PLINK_CARBON_EXIT_HIDDEN << ": $?";
  writeHiddenCommand(cmdExitStatus.c_str());
}
