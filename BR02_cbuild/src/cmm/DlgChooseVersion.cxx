// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "DlgChooseVersion.h"

DlgChooseVersion::DlgChooseVersion(QWidget *parent, ModelKit* kit)
: QDialog(parent)
{
  mModelKit = kit;
  mVersion = NULL;
  ui.setupUi(this);
  
  // Latest at the top
  for(int i=mModelKit->getVersions()->numVersions()-1; i>=0; i--)
  {
    KitVersion* kv = mModelKit->getVersions()->getVersion((quint32)i);
    QVariant qv = qVariantFromValue((void*)kv);
    ui.comboBoxVersions->addItem(kv->getVendorVersion(), qv);
  }
  ui.comboBoxVersions->setCurrentIndex(0);
}

DlgChooseVersion::~DlgChooseVersion()
{
}


void DlgChooseVersion::on_buttonBox_accepted()
{
  QVariant v = ui.comboBoxVersions->itemData(ui.comboBoxVersions->currentIndex());
  mVersion = (KitVersion*)v.value<void*>();
  accept();
}


void DlgChooseVersion::on_buttonBox_rejected()
{
  reject();
}


void DlgChooseVersion::on_comboBoxVersions_currentIndexChanged(int index)
{
  ui.labelDescr->setText("");
  QVariant v = ui.comboBoxVersions->itemData(index);
  KitVersion* kv = (KitVersion*)v.value<void*>();
  if (kv)
  {
    ui.labelDescr->setText(kv->getDescription());
  }
}
