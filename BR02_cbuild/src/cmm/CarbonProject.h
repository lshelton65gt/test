#ifndef __CARBONPROJECT_H__
#define __CARBONPROJECT_H__

#define ENVVAR_REFERENCE "$"

#include "util/CarbonPlatform.h"

#include <QObject>
#include <QTextStream>
#include <QFileSystemWatcher>

#include "util/UtString.h"
#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtHashMap.h"
#include "util/UtStringArray.h"
#include "util/OSWrapper.h"

#include "RemoteConsole.h"
#include "CarbonConsole.h"

#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>

#include "CarbonComponents.h"
#include "CarbonOptions.h"
#include "DlgNewProject.h"

class CarbonSourceGroups;
class CarbonCompilerTool;
class CarbonTool;
class CarbonProject;
class UtXmlErrorHandler;
class CarbonPropertyValue;
class CarbonProjectWidget;
class CarbonDatabaseContext;
class CarbonDirectives;
class CarbonFiles;
class CarbonSourceGroup;
class SpiritXML;

struct Switch
{
  QString mName;
  QString mValue;
};

class CarbonConfiguration
{
  public:
  CARBONMEM_OVERRIDES

  CarbonConfiguration(CarbonProject* proj, const char* name, const char* platform, bool useCompilerSwitches = true);
  ~CarbonConfiguration();

  UInt32 numTools() const {return mTools.size();}
  CarbonTool* getTool(UInt32 i) const {
    INFO_ASSERT(i < mTools.size(), "ToolVec out of range");
    return mTools[i];
  }

  void addTool(CarbonTool* tool);
  void putPlatform(const char* newVal) { mPlatform=newVal;}
  const char* getPlatform() { return mPlatform.c_str(); }
  CarbonProject* getProject() { return mProject; }

  const char* getOutputDirectory() const { return mOutputDir.c_str(); }
  void putName(const char* newVal);

  const char* getName() const { return mName.c_str(); }
  bool serialize(xmlTextWriterPtr writer);
  bool deserialize(xmlNodePtr parent, UtXmlErrorHandler* eh);

  CarbonOptions* getOptions(const char* toolName);

  typedef UtArray<CarbonTool*> ToolVec;

private:
  UtString mName;
  UtString mPlatform;
  ToolVec mTools;
  UtString mOutputDir;
  CarbonProject* mProject;
};


class CarbonConfigurations
{
  public:
  CARBONMEM_OVERRIDES

  CarbonConfigurations(CarbonProject* proj);
  ~CarbonConfigurations();
  CarbonConfiguration* addConfig(const char* platform, const char* name, bool useCompilerSwitches = true);

  CarbonConfiguration* findConfiguration(const char* name);
  UInt32 numConfigurations() { return mConfigs.size(); }
  CarbonConfiguration* getConfiguration(UInt32 index) { return mConfigs[index]; }
  bool serialize(xmlTextWriterPtr writer);
  bool deserialize(xmlNodePtr node, UtXmlErrorHandler* errHandler);
  void removeConfiguration(const char* name);
  typedef UtArray<CarbonConfiguration*> ConfigVec;

private:
  ConfigVec mConfigs;
  CarbonProject* mProject;
};

// QObject because we use Signals / Slots for notifications, thats all
class CarbonProject : public QObject
{
  Q_OBJECT

public:
  CARBONMEM_OVERRIDES

  QString resolveFile(const QString& fileName);


  enum CarbonProjectType { HDL=0, DatabaseOnly, ModelKitProject};
  enum MakefileType { eCbuildNormal, eCbuildScan } ;

  void setWriteErrorsOccurred(bool value, const char* msg);
  bool writeErrorsOccurred() { return mWriteErrorsOccurred; }

  CarbonProject(QObject* parent=0, CarbonProjectWidget* pw=NULL);
  virtual ~CarbonProject();

  enum HDLType {Verilog=1, VHDL=2, CPP=3, Header=4 };
  enum BatchType { NotBatch, BatchBuild, BatchClean, BatchRebuild };
  enum CompilationPhase { CompilationPhase1, CompilationPhase2, CompilationPhaseFinished };

  void putProjectType(CarbonProjectType v) { mProjectType=v; }
  CarbonProjectType getProjectType() { return mProjectType; }

  QString getGUID() { return mGUID; }

  void putActiveConfiguration(const char* configName);
  
  const char* getActiveConfiguration() const;
  const char* getActivePlatform();
  const char* getActivePlatformReal();
  void connectConsole();
  static QString fixVarReferences(const QString& inStr);

  CarbonConfiguration* getActive() const { return mActiveConfiguration; }
  CarbonDatabaseContext* getDbContext() const;
  bool loadProject(const char* filename);
  const char* createNewProject(const char* projDir, const char* projName);
  const char* createNewProjectFromExisting(QWidget* parent, const char* projDir, const char* projName);
  const char* createNewProjectFromDatabase(QWidget* parent, const char* projDir, const char* projName, const char* dbFile);
  const char* createNewProjectFromCcfg(QWidget*, const char* projectLocation, const char* projectName, const char* ccfgFile, DlgNewProject::ProjectKind kind);
  const char* createNewProjectFromModelKit(QWidget*, const char* projectLocation, const char* projectName, const char* modelKitFile);

  QString getDatabaseFilePath();
  QString getConfigDatabaseFilePath(const QString& configName);
  
  void setEnvironmentVariables();

  const char* shortName() 
  { 
    return mShortName.c_str();
  }
  const char* projectFile() { return mProjectFile.c_str(); }
  const char* getProjectDirectory() {return mProjectDir.c_str(); }
  const char* getVHMName();
  const char* getDesignFilePath(const char* extension);
  const char* getConfigDesignFilePath(const char* configName, const char* extension);
  void batch(CarbonProjectWidget* pw, const QStringList& configList, BatchType btype);

  CarbonProjectWidget* getProjectWidget() { return mProjectWidget; }
  CarbonConfigurations* getConfigurations() { return mConfigs; }
  CarbonSourceGroups* getGroups() { return mSourceGroups; }
  CarbonFiles* getFiles() { return mFiles; }

  CarbonComponents* getComponents() { return mComponents; }
  CarbonDirectives* getDirectives() { return mDirectives; } 

  UInt32 numPlatforms() { return mPlatforms.size(); }
  const char* getPlatform(UInt32 index) { return mPlatforms[index]; }

  void addComponent(CarbonComponent* comp);
  void deleteComponent(CarbonComponent* comp);

  bool save();

  CarbonOptions* getToolOptions(const char* toolName) { return mActiveConfiguration->getOptions(toolName); }
  CarbonOptions* getToolOptions(const char* cfgName, const char* toolName);
  CarbonOptions* getProjectOptions();
  
  QString pathRelativeToProject(const QString& inputPath);
  static void sXmlStructuredErrorHandler(void *arg, xmlErrorPtr error);

  // compile active configuration
  void compile(CarbonProjectWidget* pw, bool clean=false, bool compilation=false, bool package=false, const char* compileTarget=NULL, const char* cleanTarget=NULL, QStringList* makefileArgs=NULL, const char* armKitRoot=NULL, CompilationPhase phase=CompilationPhase1);
  bool check(CarbonProjectWidget*);
  void writeRemodelMakefile(CarbonConfiguration* activeConfig, const QString& scandir, const QStringList& filters, bool recurseFlag=false);
  bool writeFileIfChanged(const char* fileName, QTextStream* stream);
  void renameConfiguration(const char* oldName, const char* newName);
  void removeConfiguration(const char* name);
  void addConfiguration(const char* name);
  void copyConfiguration(const char* oldName, const char* newName);
  void checkPidFile(const char* name);
  void actionImportCommandFile();
  void actionImportCcfgFile();
  const char* generateLibraryCommandFile(CarbonConfiguration* activeConfig, CarbonSourceGroup* group, int libIndex, const char* outputDir, const char* cmdFileName);
  const char* generateMakeFile(CarbonConfiguration* activeConfig, CarbonConsole::CommandMode mode, const char* outputDir, const char* cmdFileName, const char* dirFileName);
  void cleanupMonitor();
  QString getRemoteWorkingDirectory();
  void generatePrecompiledHierarchy();
  QString remoteCommandName(const QString& cmdName);
  static QString createGUID();
  bool checkProject();
  QString getWindowsEquivalentPath(const char* unixPath);
  QString getWindowsEquivalentPath(const QString& unixPath);
  bool isMappedUnixPath(const char* unixPath);
  QString getUnixEquivalentPath(const QString& windowsPath);
  QString getUnixEquivalentPath(const char* windowsPath);
  bool checkFilePath(const char* fileDir, const char* filePath);
  SpiritXML* getSpiritXML() { return mSpiritXML; }
  void setSpiritXML(SpiritXML* newVal);
  QString hashFile(const QString& fileName);
  bool encryptCommandFile();
  bool windowsAuthenticate();
  int executeMakeCommand(CarbonConfiguration* activeConfig, CarbonConsole::CommandType rtype, const char* targetName, QStringList* makeArgs=NULL, BatchType=NotBatch, bool wait=false);
  void addSourceFile(const QString& cmdFilePath, const QString& fileName, bool libraryFile, bool createGUIElements, const QString& folderName = QString());
  void setARMKitRoot(const QString& newVal) { mArmKitRoot=newVal; }

private:
  void writeFileDependenciesVar(QTextStream& stream, CarbonConfiguration* activeConfig, const QString& varName, const char* switchName);
  void createNonDefaultSwitches();
  void writeCarbonVariables(CarbonConfiguration*,QTextStream& stream, bool windowsTarget);
  void writePlatforms(xmlTextWriterPtr writer);
  bool readFile(const char* projFile);
  void parseProjectOptions(xmlNodePtr parent);
  void parseOption(xmlNodePtr parent);
  void processCommandFile(const char* cmdFile, bool createGUIElements = false);
  void generateDirectivesFile(CarbonConfiguration* cfg, const char* dirFile);
  const char* formatSwitch(CarbonPropertyValue* propValue, bool checkChanged=true);
  const char* formatBoolSwitch(CarbonPropertyValue* propValue, bool checkChanged);
  const char* formatStringSwitch(CarbonPropertyValue* propValue, bool checkChanged);
  const char* formatStringlistSwitch(CarbonPropertyValue* propValue, bool checkChanged);
  const char* formatFileSwitch(CarbonPropertyValue* propValue, bool checkChanged);
  const char* formatEnumSwitch(CarbonPropertyValue* propValue, bool checkChanged);
  const char* formatIntegerSwitch(CarbonPropertyValue* propValue, bool checkChanged);
  const char* formatFilelistSwitch(CarbonPropertyValue* propValue, bool checkChanged);
  void writeCompilerSwitches(CarbonConfiguration* activeConfig, QTextStream& stream, MakefileType mtype=eCbuildNormal);
  void writeFileMacros(CarbonConfiguration* activeConfig, QTextStream& stream, QTextStream* groupStream);
  void writeFileSpecificSwitches(CarbonConfiguration* activeConfig, QTextStream& stream, CarbonSourceGroup* group, bool referencesOnly, bool blockVlogLib);
  bool copyDesignFiles(const char* designDir, const char* designBaseName, const char* destinationDir);
  QString copyAndRenameCcfg(const char* ccfgFile, const char* ccfgDir, const char* designFilesDir);
  void setDefaultLibraryName(const char* configName=NULL);
  void copyCompilerSettings(const char* oldCfgName, const char* newCfgName);
  bool deserializePreferences(xmlNodePtr parent, UtXmlErrorHandler* eh);
  bool serializePreferences(xmlTextWriterPtr writer);
  void writeMakefile(CarbonConfiguration*);
  void writeToplevelMakefile();
  void writeTopMakefileEntry(QTextStream& stream, const char* start, const char* finished, const char* target);
  bool checkCommandFilePaths(const char* cmdFile);
  void applySwitch(const char* switchName, const char* switchValue);
  void writeSourceGroupSwitches(CarbonConfiguration* activeConfig, QTextStream& stream, CarbonSourceGroup* group, QStringList& groupSwitches);
  void writeScanFiles(QTextStream& commandStream, const QString& scanDir, const QStringList& fileKinds, bool recurseFlag);
  void showWindows(bool showOrHide);
  bool encryptFile(const QString& inputFileName);

private slots:
  void compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void modelCompilationFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void carbonDirectoryChanged(const QString&);
  void propertiesModified();
  void exploreFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void commandCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType, int);
  void finishCompile();
  void processTerminated(CarbonConsole::CommandMode, CarbonConsole::CommandType, int);

signals:
    void configurationChanged(const char*);
    void projectLoaded(const char*, CarbonProject*);
    void configurationRenamed(const char*, const char*);
    void configurationCopy(const char*, const char*);
    void configurationRemoved(const char*);
    void configurationAdded(const char*);
    void makefileCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
    void modelCompilationCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
    void componentAdded(CarbonComponent*);
    void componentDeleted(CarbonComponent*);
    void postCompilationJavaScriptFinished();

public:
    QString postCompilationScript();
    bool hasPostCompilationScript();
    void signalPostCompiliationJavaScriptFinished()
    {
      emit postCompilationJavaScriptFinished();
    }

private:
  bool mWriteErrorsOccurred;
  QStringList mWriteErrors;

  QString mGUID;
  bool mConnectedConsole;
  bool mConnectedCommand;
  UtStringArray mPlatforms;
  QFileSystemWatcher* mProjectWatcher;
  CarbonProjectWidget* mProjectWidget;
  CarbonConfigurations* mConfigs;
  CarbonSourceGroups* mSourceGroups;
  CarbonFiles* mFiles;
  CarbonComponents* mComponents;
  CarbonDirectives* mDirectives;

  CarbonProjectType mProjectType;

  UtString mProjectFile;
  UtString mProjectDir;
  UtString mShortName;

  CarbonConfiguration* mActiveConfiguration;
  CarbonOptions* mOptions;
  CarbonDatabaseContext* mDbContext;

  UtString mLastLibraryName;

  CarbonProperties mFileProperties;
  CarbonOptions* mFileOptions;

  UtString mDefaultConfig;

  bool mFirstConfiguration;

  QList<Switch*> mImportSwitches;
  SpiritXML* mSpiritXML;

  QMutex mMutexEncryptFile;
  QWaitCondition mWaitEncryptFile;

  QString mArmKitRoot;

  bool mModelCompilationStarted;
  bool mCarbonModelWasCompiled;
  CompilationPhase mCompilationPhase;

  UtString mEnvCarbonProj;
  UtString mEnvCarbonProjRel;
};

class CarbonProperties;

class CarbonProjectOptions : public QObject
{
  Q_OBJECT
public:
  CARBONMEM_OVERRIDES
  CarbonProjectOptions(CarbonProjectWidget* pw);

  CarbonProperties* getProperties() { return mProperties; }
  CarbonOptions* getOptions() { return mOptions; }
  virtual ~CarbonProjectOptions();

private slots:
  void propertiesModified();

private:
  CarbonProperties* mProperties;
  CarbonOptions* mOptions;
  CarbonProjectWidget* mProjectWidget;
};

#endif
