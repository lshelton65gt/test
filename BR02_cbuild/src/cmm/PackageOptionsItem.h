// -*-C++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef _PackageOptionsItem_h_
#define _PackageOptionsItem_h_

#include "CarbonComponent.h"

class PackageComponent;

class PackageOptionsItem : public CarbonComponentTreeItem
{
public:
  PackageOptionsItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, PackageComponent* comp);
  void showContextMenu(const QPoint& point);
  void singleClicked(int);
  void doubleClicked(int);

private:
  void openWidget();
};

#endif
