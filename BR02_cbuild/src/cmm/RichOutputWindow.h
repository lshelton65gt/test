#ifndef RICHOUTPUTWINDOW_H
#define RICHOUTPUTWINDOW_H
#include "util/CarbonPlatform.h"

#include <QTextBrowser>
#include "ui_RichOutputWindow.h"

class RichOutputWindow : public QTextBrowser
{
    Q_OBJECT

public:
    RichOutputWindow(QWidget *parent = 0);
    ~RichOutputWindow();

    // Add a line of output
    int writeLine(const char* line);

private:
    Ui::RichOutputWindowClass ui;

private slots:
   void on_anchorClicked(QUrl);

};

#endif // RICHOUTPUTWINDOW_H
