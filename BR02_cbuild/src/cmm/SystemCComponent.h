#ifndef __CARBONSystemCComponent_H__
#define __CARBONSystemCComponent_H__

#include "util/XmlParsing.h"

#include "carbon/carbon_dbapi.h"
#include "cfg/CarbonCfg.h"

#include "CarbonComponent.h"
#include "util/UtString.h"
#include "RemoteConsole.h"
#include "CarbonConsole.h"

class CarbonOptions;
class CarbonProperties;
class CarbonProjectWidget;
class CarbonConfigurations;
class CarbonConfiguration;
class SystemCTreeItem;
class SystemCCcfgItem;
class SystemCSourceItem;
class ScrCcfg;

class SystemCComponent : public CarbonComponent
{
  Q_OBJECT
  Q_CLASSINFO("ccfg", "ScrCcfg*");

  // Property: ccfg
  // The ccfg object associated with this component
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(ScrCcfg* ccfg READ getCcfg);

public:
  SystemCComponent(CarbonProject* proj);
  virtual ~SystemCComponent();
  const char* ccfgName();

  // Overrides
  static SystemCComponent* deserialize(CarbonProject* proj, xmlNodePtr parent, UtXmlErrorHandler* eh);
  virtual bool serialize(xmlTextWriterPtr writer);
  virtual const char* getTargetName(CarbonConfiguration*,CarbonComponent::TargetType type, CarbonComponent::TargetPlatform=CarbonComponent::Unix);
  virtual bool getTargetImpl(CarbonConfiguration*,QTextStream& stream, TargetType type);
  virtual void generateMakefile(CarbonConfiguration*,const char* makefileName);
  virtual CarbonComponentTreeItem* createTreeItem(CarbonProjectWidget*, QTreeWidgetItem*);
  virtual const char* getOutputDirectory(const char* configName=NULL);
  virtual const char* getComponentDirectory() const;
  CarbonOptions* getOptions(const char* configName=NULL);
  CarbonConfigurations* getConfigurations() { return mConfigs; }
  void updateTreeIcons();
  SystemCTreeItem* getRootItem() const { return mRootItem; }
  virtual bool checkComponent(CarbonConsole*);
  void icheckComponent();
  virtual void setEnabled(bool value);
  bool importCcfg(QString filename, bool checkDB=true);
  UInt32 numPackageFiles();
  QString getPackageFile(UInt32 index);
  virtual int getGeneratedFiles(QStringList&);
  virtual int getSourceFiles(QStringList& fileList);
  void createDefaultComponent();
  void setComponentName(const QString& compName);

private:
  ScrCcfg* getCcfg();
  static void createDirectory(CarbonConfiguration*);
  void renameDirectory(const char* oldName, const char* newName);
  static void createConfigurations(SystemCComponent* comp, xmlNodePtr parent);
  virtual void updateCcfg(CarbonConfiguration*, const char* iodbName);
  void updateTreeTextValues();
  bool checkRegisters(CarbonConsole* console);
  void populateLoop(CarbonDB* carbonDB, CarbonCfgRTLPortType type, CarbonDBNodeIter* iter);
  CarbonCfgESLPortID addConnection(CarbonCfgRTLPortID rtlPort);
  CarbonCfgESLPortID addESLConnection(CarbonCfgRTLPortID rtlPort,
                                                    CarbonCfgESLPortType type);
  void computeMode(CarbonDB* db, const CarbonDBNode* node, CarbonCfgESLPort* eslPort);
  const char* computeSystemCType(CarbonCfgRTLPort* rtlPort);

private slots:
  void configurationChanged(const char*);
  void configurationRemoved(const char*);
  void configurationRenamed(const char*, const char*);
  void configurationCopy(const char*, const char*);
  void compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void configurationAdded(const char* name);
  void compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void outputFilenameChanged(const CarbonProperty*, const char*);
  void projectLoaded(const char* fileName, CarbonProject*);
  void projectClosing(CarbonProject* proj);

signals:
  void xtorDefFileChanged();

private:
  CarbonConfigurations* mConfigs;
  UtString mOutputDir;
  UtString mTargetName;
  UtString mCcfgName;
  CarbonConfiguration* mActiveConfiguration;
  SystemCTreeItem* mRootItem;

  SystemCCcfgItem* mCcfgItem;
  SystemCSourceItem* mHeaderItem;
  SystemCSourceItem* mCppItem;
 
  QStringList mPackageFiles;
  UtString mIODBName;
  CarbonCfgID mCfg;
  CarbonDB* mDB;
};


class SystemCTreeItem : public CarbonComponentTreeItem
{
  Q_OBJECT

public:
  SystemCTreeItem(CarbonProjectWidget* proj, SystemCComponent* comp);
  SystemCTreeItem(CarbonProjectWidget* proj, SystemCComponent* comp, QTreeWidgetItem* parent);

  virtual void showContextMenu(const QPoint&);
  virtual void singleClicked(int);

private:
  void createActions();

 public slots:
    void compileComponent();
    void deleteComponent();
    void cleanComponent();
    void icheckComponent();
    void deleteComponentBatch();

private:
  QAction* mActionClean;
  QAction* mActionCompile;
  QAction* mActionDelete;
  QAction* mActionCheck;
};

class SystemCSourceItem : public CarbonProjectTreeNode
{
public:
  SystemCSourceItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, SystemCComponent* comp) : CarbonProjectTreeNode(proj,parent) 
  {
    setIcon(0, QIcon(":/cmm/Resources/file-generated-16x16.png"));
    mComp = comp;
  }
  virtual void doubleClicked(int);
  void setExists(bool value)
  {
    if (value)
      setIcon(0, QIcon(":/cmm/Resources/file-generated-16x16.png"));
    else
      setIcon(0, QIcon(":/cmm/Resources/file-16x16missing.png"));
  }
private:
  SystemCComponent* mComp;
};

class SystemCCcfgItem : public CarbonProjectTreeNode
{
public:
  SystemCCcfgItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, SystemCComponent* comp);
  virtual void doubleClicked(int);
  virtual void showContextMenu(const QPoint&);
  virtual void singleClicked(int);
  void setExists(bool value)
  {
    if (value)
      setIcon(0, QIcon(":/cmm/Resources/Wizard.png"));
    else
      setIcon(0, QIcon(":/cmm/Resources/file-16x16missing.png"));
  }

private:
  SystemCComponent* mComp;
};

#endif
