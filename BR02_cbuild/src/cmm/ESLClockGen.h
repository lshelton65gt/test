#ifndef __ESLCLOCKGEN__
#define __ESLCLOCKGEN__

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "gui/CExprValidator.h"
#include "util/DynBitVector.h"
#include "CompWizardTreeNodes.h"
#include "PortEditorTreeWidget.h"
#include "cfg/CarbonCfg.h"
#include "cfg/carbon_cfg.h"
#include <QtGui>


class SystemCClocksRootItem : public PortEditorTreeItem
{
public:
  virtual bool dropItems(const QList<PortEditorTreeItem*>&);
  virtual bool canDropHere(const QList<PortEditorTreeItem*>&, QDragMoveEvent*);

  SystemCClocksRootItem(QTreeWidget* parent)
    : PortEditorTreeItem(PortEditorTreeItem::SystemCClocks, parent)
  {
    setBoldText(0, "Clock Generators");
  }
};


class SystemCClockGenInitialValueItem : public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE };

  CarbonCfgSystemCClock* getClockGen() const { return mClockGen; }

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  void setValue(quint32 newValue)
  {
    if (newValue)
      setText(colVALUE, "High");
    else
      setText(colVALUE, "Low");
  }

  SystemCClockGenInitialValueItem(QTreeWidgetItem* parent, CarbonCfgSystemCClock* clockGen)
    : PortEditorTreeItem(PortEditorTreeItem::SystemCClockInitValue, parent)
  {
    mClockGen = clockGen;

    setItalicText(colNAME, "Initial Value");
    setValue(clockGen->getInitialValue());
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgSystemCClock* mClockGen;
};


class SystemCClockGenDutyCycleItem : public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE };

  CarbonCfgSystemCClock* getClockGen() const { return mClockGen; }

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  void setValue(double newValue)
  {
    QString value = QString("%1").arg(newValue);
    setText(colVALUE, value);
  }

  SystemCClockGenDutyCycleItem(QTreeWidgetItem* parent, CarbonCfgSystemCClock* clockGen)
    : PortEditorTreeItem(PortEditorTreeItem::SystemCClockDutyCycle, parent)
  {
    mClockGen = clockGen;

    setItalicText(colNAME, "Duty Cycle");
    setValue(clockGen->getDutyCycle());
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgSystemCClock* mClockGen;
};

class SystemCClockGenPeriodItem : public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE };

  CarbonCfgSystemCClock* getClockGen() const { return mClockGen; }

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  void setValue(double newValue)
  {
    QString value = QString("%1").arg(newValue);
    setText(colVALUE, value);
  }

  SystemCClockGenPeriodItem(QTreeWidgetItem* parent, CarbonCfgSystemCClock* clockGen)
    : PortEditorTreeItem(PortEditorTreeItem::SystemCClockPeriod, parent)
  {
    mClockGen = clockGen;

    setItalicText(colNAME, "Period");
    setValue(clockGen->getPeriod());
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgSystemCClock* mClockGen;
};


class SystemCClockGenStartTimeItem : public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE };

  CarbonCfgSystemCClock* getClockGen() const { return mClockGen; }

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  void setValue(double newValue)
  {
    QString value = QString("%1").arg(newValue);
    setText(colVALUE, value);
  }

  SystemCClockGenStartTimeItem(QTreeWidgetItem* parent, CarbonCfgSystemCClock* clockGen)
    : PortEditorTreeItem(PortEditorTreeItem::SystemCClockStartTime, parent)
  {
    mClockGen = clockGen;

    setItalicText(colNAME, "Start Time");
    setValue(clockGen->getStartTime());
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgSystemCClock* mClockGen;
};


class SystemCClockGenStartTimeUnitsItem : public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE };

  CarbonCfgSystemCClock* getClockGen() const { return mClockGen; }

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  void setValue(CarbonCfgSystemCTimeUnits newValue)
  {
    QString value = gCarbonCfgSystemCTimeUnits[(int)newValue];
    setText(colVALUE, value);    
  }

  SystemCClockGenStartTimeUnitsItem(QTreeWidgetItem* parent, CarbonCfgSystemCClock* clockGen)
    : PortEditorTreeItem(PortEditorTreeItem::SystemCClockStartTimeUnits, parent)
  {
    mClockGen = clockGen;

    setItalicText(colNAME, "Start Time Units");
    setValue(clockGen->getStartTimeUnits());
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgSystemCClock* mClockGen;
};


class SystemCClockGenPeriodUnitsItem : public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE };

  CarbonCfgSystemCClock* getClockGen() const { return mClockGen; }

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  void setValue(CarbonCfgSystemCTimeUnits newValue)
  {
    QString value = gCarbonCfgSystemCTimeUnits[(int)newValue];
    setText(colVALUE, value);    
  }

  SystemCClockGenPeriodUnitsItem(QTreeWidgetItem* parent, CarbonCfgSystemCClock* clockGen)
    : PortEditorTreeItem(PortEditorTreeItem::SystemCClockPeriodUnits, parent)
  {
    mClockGen = clockGen;

    setItalicText(colNAME, "Period Time Units");
    setValue(clockGen->getPeriodUnits());
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgSystemCClock* mClockGen;
};

// SystemC Clock Generators
class SystemCClockGenItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colUNUSED, colSIZE};

  CarbonCfgSystemCClock* getClockGen() const { return mClockGen; }

  virtual UndoData* deleteItem();

  SystemCClockGenItem(QTreeWidgetItem* parent, CarbonCfgSystemCClock* clockGen) 
    : PortEditorTreeItem(PortEditorTreeItem::SystemCClock, parent)
  {
    mClockGen = clockGen;

    CarbonCfgRTLPort* rtlPort = clockGen->getRTLPort();

    QString value = QString("%1").arg(rtlPort->getWidth());

    setText(colNAME, rtlPort->getName());
    setText(colSIZE, value);
    setIcon(colNAME, QIcon(":/cmm/Resources/clock.png"));

    new SystemCClockGenInitialValueItem(this, clockGen);
    new SystemCClockGenStartTimeItem(this, clockGen);
    new SystemCClockGenStartTimeUnitsItem(this, clockGen);
    new SystemCClockGenDutyCycleItem(this, clockGen);
    new SystemCClockGenPeriodItem(this, clockGen);
    new SystemCClockGenPeriodUnitsItem(this, clockGen);

    setExpanded(true);
  }
  
  static CarbonCfgRTLPort* removeSystemCClockGen(PortEditorTreeItem* item, const QString& rtlPortName);
  static SystemCClockGenItem* createSystemCClockGen(PortEditorTreeWidget* tree,
                        UndoData* undoData, CarbonCfgRTLPort* rtlPort,
                        UInt32 initialValue, double period, CarbonCfgSystemCTimeUnits periodUnits,
                        double dutyCycle,
                        double startTime, CarbonCfgSystemCTimeUnits startTimeUnits);
private: 
 class Delete : public UndoData
  {
  public:
    CarbonCfg* mCfg;
    QString mRTLPortName;
    QString mESLPortName;
    PortTreeIndex mESLPortIndex;
    UInt32 mInitialValue;
    double mPeriod;
    CarbonCfgSystemCTimeUnits mPeriodUnits;
    double mDutyCycle;
    double mStartTime;
    CarbonCfgSystemCTimeUnits mStartTimeUnits;

  protected:
    void undo();
    void redo();
  };

private:
  CarbonCfgSystemCClock* mClockGen;

};

// Clock Generators
class ESLClockGenInitialValueItem : public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE };

  CarbonCfgClockGen* getClockGen() const { return mClockGen; }

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  void setValue(quint32 newValue)
  {
    if (newValue)
      setText(colVALUE, "High");
    else
      setText(colVALUE, "Low");
  }

  ESLClockGenInitialValueItem(QTreeWidgetItem* parent, CarbonCfgClockGen* clockGen)
    : PortEditorTreeItem(PortEditorTreeItem::ClockInitValue, parent)
  {
    mClockGen = clockGen;

    setItalicText(colNAME, "Initial Value");
    setValue(clockGen->mInitialValue);
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgClockGen* mClockGen;
};

class ESLClockGenDelayItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colVALUE };

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  void setValue(quint32 newValue)
  {
    QString value = QString("%1%").arg(newValue);
    setText(colVALUE, value);
  }

  CarbonCfgClockGen* getClockGen() const { return mClockGen; }

  ESLClockGenDelayItem(QTreeWidgetItem* parent, CarbonCfgClockGen* clockGen)
    : PortEditorTreeItem(PortEditorTreeItem::ClockDelay, parent)
  {
    mClockGen = clockGen;

    setItalicText(colNAME, "Delay");
    setValue(clockGen->mDelay);
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgClockGen* mClockGen;
};

class ESLClockGenDutyCycleItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colVALUE };

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  void setValue(quint32 newValue)
  {
    QString value = QString("%1%").arg(newValue);
    setText(colVALUE, value);
  }

  CarbonCfgClockGen* getClockGen() const { return mClockGen; }

  ESLClockGenDutyCycleItem(QTreeWidgetItem* parent, CarbonCfgClockGen* clockGen)
    : PortEditorTreeItem(PortEditorTreeItem::ClockDutyCycle, parent)
  {
    mClockGen = clockGen;
    setItalicText(colNAME, "Duty Cycle");
    setValue(clockGen->mDutyCycle);
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgClockGen* mClockGen;
};

class ESLClockGenFrequencyClockCycles : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colVALUE };

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  CarbonCfgClockGen* getClockGen() const { return mClockGen; }

  void setValue(quint32 newValue)
  {
    QString value = QString("%1").arg(newValue);
    setText(colVALUE, value);
  }

  ESLClockGenFrequencyClockCycles(QTreeWidgetItem* parent, CarbonCfgClockGen* clockGen)
    : PortEditorTreeItem(PortEditorTreeItem::ClockCycles, parent)
  {
    mClockGen = clockGen;
    setItalicText(colNAME, "Clock Cycles");
    setValue(clockGen->mFrequency.getClockCycles());
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgClockGen* mClockGen;
};

class ESLClockGenFrequencyCompCycles : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colVALUE };

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  void setValue(quint32 newValue)
  {
    QString value = QString("%1").arg(newValue);
    setText(colVALUE, value);
  }

  CarbonCfgClockGen* getClockGen() const { return mClockGen; }

  ESLClockGenFrequencyCompCycles(QTreeWidgetItem* parent, CarbonCfgClockGen* clockGen) 
    : PortEditorTreeItem(PortEditorTreeItem::ClockCompCycles, parent)
  {
    mClockGen = clockGen;
    setItalicText(colNAME, "Component Cycles");
    setValue(clockGen->mFrequency.getCompCycles());
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgClockGen* mClockGen;
};

class ESLClockGenFrequencyItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colVALUE };

  CarbonCfgClockGen* getClockGen() const { return mClockGen; }

  ESLClockGenFrequencyItem(QTreeWidgetItem* parent, CarbonCfgClockGen* clockGen)\
    : PortEditorTreeItem(PortEditorTreeItem::ClockFrequency, parent)
  {
    mClockGen = clockGen;

    setBoldText(colNAME, "Frequency");

    new ESLClockGenFrequencyClockCycles(this, clockGen);
    new ESLClockGenFrequencyCompCycles(this, clockGen);

    setExpanded(true);
  }

private:
  CarbonCfgClockGen* mClockGen;
};


class ESLClockGenItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colUNUSED, colSIZE};

  CarbonCfgClockGen* getClockGen() const { return mClockGen; }

  virtual UndoData* deleteItem();

  ESLClockGenItem(QTreeWidgetItem* parent, CarbonCfgClockGen* clockGen) 
    : PortEditorTreeItem(PortEditorTreeItem::Clock, parent)
  {
    mClockGen = clockGen;

    CarbonCfgRTLPort* rtlPort = clockGen->getRTLPort();

    QString value = QString("%1").arg(rtlPort->getWidth());

    setText(colNAME, rtlPort->getName());
    setText(colSIZE, value);
    setIcon(colNAME, QIcon(":/cmm/Resources/clock.png"));

    new ESLClockGenInitialValueItem(this, clockGen);
    new ESLClockGenDelayItem(this, clockGen);
    new ESLClockGenDutyCycleItem(this, clockGen);
    new ESLClockGenFrequencyItem(this, clockGen);

    setExpanded(true);
  }

  static CarbonCfgRTLPort* removeClockGen(PortEditorTreeItem* item, const QString& rtlPortName);
  static ESLClockGenItem* createClockGen(PortEditorTreeWidget* tree,
                        UndoData* undoData, CarbonCfgRTLPort* rtlPort,
                        UInt32 initialValue, UInt32 delay, UInt32 dutyCycle,
                        UInt32 clockCycles, UInt32 compCycles);
private: 
 class Delete : public UndoData
  {
  public:
    CarbonCfg* mCfg;
    QString mRTLPortName;
    QString mESLPortName;
    PortTreeIndex mESLPortIndex;
    UInt32 mInitialValue;
    UInt32 mDelay;
    UInt32 mClockCycles;
    UInt32 mCompCycles;
    UInt32 mDutyCycle; 

  protected:
    void undo();
    void redo();
  };

private:
  CarbonCfgClockGen* mClockGen;
};

#endif
