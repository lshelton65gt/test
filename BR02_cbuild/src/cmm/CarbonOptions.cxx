//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtShellTok.h"

#include "CarbonOptions.h"
#include "JavaScriptAPIs.h"
#include "EmbeddedMono.h"

#if 1
#include "classGen/CarbonOptions_wrapper.cxx"
#endif

CarbonPropertyGroup* CarbonOptions::findGroup(const QString& groupName)
{
  UtString name; name << groupName;
  return (CarbonPropertyGroup*)findGroup(name.c_str());
}

CarbonProperty* CarbonOptions::lookupProperty(const QString& propertyName)
{
  UtString name; name << propertyName;
  return (CarbonProperty*)findProperty(name.c_str());
}

quint32 CarbonOptions::numSwitches()
{
  return mSwitchValues.size();
}

void CarbonOptions::setSwitchValue(const QString& switchName, const QString& value)
{
  UtString name; name << switchName;
  putValue(name.c_str(), value);
}

CarbonPropertyValue* CarbonOptions::getSwitchValue(quint32 index)
{
  quint32 i=0;
  for (CarbonOptions::OptionsLoop p = loopOptions(); !p.atEnd(); ++p)
  {
    if (i == index)
    {
      const CarbonPropertyValue* sw = p.getValue();
      return (CarbonPropertyValue*)sw;
    }
    i++;
  }

  return NULL;
}

CarbonPropertyValue* CarbonOptions::getSwitchValue(const QString& switchName)
{
  UtString name; name << switchName;
  CarbonPropertyValue* v = getValue(name.c_str());
  return v;
}

const char* CarbonPropertyValue::multiValueString(const QStringList& items)
{
  static UtString result;
  result.clear();

  foreach (QString item, items)
  {
    UtString value;
    value << item;
    UtString tok;
    const char* token = UtShellTok::quote(value.c_str(), &tok, false, ";");
    if (result.length() > 0)
      result << ";";
    result << token;
  }
  return result.c_str();
}


// items are a list of unquoted items, so quote them if need be
void CarbonPropertyValue::putValues(const QStringList& items)
{
  mValues.clear();

  foreach (QString item, items)
  {
    UtString value;
    value << item;
    UtString tok;
    const char* token = UtShellTok::quote(value.c_str(), &tok, false, ";");
    mValues.append(token);
  }
  UtString newValue;
  foreach (QString item, mValues)
  {
    if (newValue.length() > 0)
      newValue << ";";
    newValue << item;
  }
  updateValue(newValue.c_str());
}

void CarbonPropertyValue::putValue(const char* newVal)
{
  updateValue(newVal);
  mValues.clear();

  if (mMultiVal)
  {
    for (UtShellTok tok(newVal, false, ";"); !tok.atEnd(); ++tok)
    {
      const char* value = *tok;
      mValues.append(value);
    }
  }
  else
    mValues.append(newVal);
}

void CarbonPropertyValue::updateValue(const char* newVal)
{
  if (newVal)
  {
    mValue=newVal;
    mValueSet=true;
  }
  else
  {
    mValue.clear();
    mValueSet=false;
  }
}

bool CarbonPropertyValue::getBoolValue() const
{
  if (mValue == "true") {
    return true;
  }
  return false;
}

void CarbonPropertyValue::putBoolValue(bool val)
{
  putValue(val ? "true" : "false");
}

// Carbon Settings
CarbonOptions::CarbonOptions(CarbonOptions* parent, CarbonProperties* properties, const char* name) :
mParent(parent), mProperties(properties)
{
  mBlockSignals = false;
  mName = name;

  if (mParent == NULL) // This is the root, populate all switches 
  {
    INFO_ASSERT(properties != NULL, "Root must not have null Switches");
    for (UInt32 gn=0; gn<properties->numGroups(); ++gn)
    {
      CarbonPropertyGroup* group = properties->getGroup(gn);
      for (CarbonPropertyGroup::SwitchLoop p = group->loopSwitches(); !p.atEnd(); ++p)
      {
        const CarbonProperty* sw = p.getValue();
        addSwitch(sw);
      }
    }
  }
  else
    setParentOptions(parent);
}

void CarbonOptions::onSelectedItemChanged(const char* /*item*/, const QVariant& /*data*/)
{
}

void CarbonOptions::setSelectedItem(const char* item, const QVariant& data)
{
  // for subclassers
  onSelectedItemChanged(item, data);
  
  // for the signal typers
  emit selectionChanged(item, data);
}


void CarbonOptions::setParentOptions(CarbonOptions* parent)
{
  mParent=parent;
  for (UInt32 gn=0; gn<mProperties->numGroups(); ++gn)
  {
    CarbonPropertyGroup* group = mProperties->getGroup(gn);
    for (CarbonPropertyGroup::SwitchLoop p = group->loopSwitches(); !p.atEnd(); ++p)
    {
      const CarbonProperty* sw = p.getValue();
      // Add the switch, but only if the parent doesn't
      // already have it (in other words, just the additions)
      if (!parent->findProperty(sw->getName()))
        addSwitch(sw);
    }
  }
}

void CarbonOptions::setItems(const char* item, const QStringList& itemList, const QVariantList& itemDataList)
{
  INFO_ASSERT(itemList.count() == itemDataList.count(), "Mismatching items");

  if (item)
    mItem = item;

  foreach (QString str, itemList)
    mItems.append(str);

  foreach (QVariant var, itemDataList)
    mItemDataList.append(var);
}

CarbonOptions::~CarbonOptions()
{
  for (CarbonOptions::OptionsLoop p = loopOptions(); !p.atEnd(); ++p)
    delete p.getValue();

  // Free up the map
  QList<NotifyEntryList*> values = mNotifyChangedMap.values();
  foreach (NotifyEntryList* list, values)
  {
    for (int i=0; i<list->count(); i++)
    {
      NotifyClientEntry* entry = list->at(i);
      delete entry;
    }
    delete list;
  }

  mNotifyChangedMap.clear();
}

// This is the core of setting a value
void CarbonOptions::putValue(const CarbonProperty* sw, const char* value)
{
  INFO_ASSERT(sw, "Missing CarbonProperty");
  bool newProperty = false;
  QString propName = sw->getName();

  CarbonPropertyValue* sv = mSwitchValues[sw->getName()];
  if (sv == NULL)
  {
    newProperty = true; 
    sv = new CarbonPropertyValue(this, sw);
  }

  UtString oldValue;
  oldValue << sv->getValue();

  // Is the value different?
  if (newProperty || (0 != strcmp(oldValue.c_str(), value) || sw->hasGuiDefaultValue()))
  {
    if (!mBlockSignals)
      emit propertiesModified();

    sv->putValue(value);
    mSwitchValues[sw->getName()] = sv;

    if (!mBlockSignals)
    {
      emit propertyModified(sw, oldValue.c_str(), value);

      if (mNotifyChangedMap.contains(propName))
      {
        NotifyEntryList* list = mNotifyChangedMap[propName];
        for (int i=0; i<list->count(); i++)
        { 
          NotifyClientEntry* entry = list->at(i);
          bool result = QMetaObject::invokeMethod(entry->getObject(), entry->getSlotName(),
            Qt::DirectConnection, Q_ARG(const CarbonProperty*, sw), Q_ARG(const char*, value));
          if (!result)
          {
            UtString err;
            err << "Failed to invoke method: " << entry->getSlotName();
          
            INFO_ASSERT(result, err.c_str());
          }
        }
      }
    }

    UtString exclusiveWith;
    exclusiveWith << sw->getExclusiveWith();
    if (exclusiveWith.length() > 0)
    {
      const CarbonProperty* exProp = findProperty(exclusiveWith.c_str());
      if (exProp)
      {
        if (sw->getKind() == CarbonProperty::Bool)
        {
          if (0 == strcmp(value, "true"))
            putValue(exProp, "false");
          else
            putValue(exProp, "true");
        }
      }
    }
  }
}

void CarbonOptions::putValues(const CarbonProperty* sw, const QStringList& valueList)
{
  INFO_ASSERT(sw, "Missing CarbonProperty");

  QString propName = sw->getName();

  CarbonPropertyValue* sv = mSwitchValues[sw->getName()];
  if (sv == NULL)
    sv = new CarbonPropertyValue(this, sw);

  // Save the old value in both string and list form.
  UtString oldValueStr;
  oldValueStr << sv->getValue();
  const QStringList &oldValueList = sv->getValues();

  // Is the value different?
  if (!mBlockSignals && (oldValueList != valueList))
  {
    emit propertiesModified();

    sv->putValues(valueList);
    mSwitchValues[sw->getName()] = sv;

    // Now that the value has been updated, we can easily get it as a string
    UtString valueStr;
    valueStr << sv->getValue();

    emit propertyModified(sw, oldValueStr.c_str(), valueStr.c_str());

    if (mNotifyChangedMap.contains(propName))
    {
      NotifyEntryList* list = mNotifyChangedMap[propName];
      for (int i=0; i<list->count(); i++)
      { 
        NotifyClientEntry* entry = list->at(i);
        bool result = QMetaObject::invokeMethod(entry->getObject(), entry->getSlotName(),
          Qt::DirectConnection, Q_ARG(const CarbonProperty*, sw), Q_ARG(const char*, valueStr.c_str()));
        if (!result)
        {
          UtString err;
          err << "Failed to invoke method: " << entry->getSlotName();
        
          INFO_ASSERT(result, err.c_str());
        }
      }
    }
  }
}

// for every call to this, there must be a corresponding 
// call to unregisterPropertyChanged, don't forget to do that.
void CarbonOptions::registerPropertyChanged(const char* propertyName, const char* slotName, QObject* obj)
{
  NotifyClientEntry* entry = new NotifyClientEntry(obj, slotName);
  if (mNotifyChangedMap.contains(propertyName))
  {
    NotifyEntryList* list = mNotifyChangedMap[propertyName];
    INFO_ASSERT(list, "Invalid map");
    list->push_back(new NotifyClientEntry(obj, slotName));
  }
  else
  {
    NotifyEntryList* list = new NotifyEntryList();
    list->push_back(entry);
    mNotifyChangedMap[propertyName] = list;
  }
}
void CarbonOptions::unregisterPropertyChanged(const char* propertyName, QObject* obj)
{
  if (mNotifyChangedMap.contains(propertyName)) {
    NotifyEntryList* list = mNotifyChangedMap[propertyName];
    QList<NotifyClientEntry*> entryList;
    INFO_ASSERT(list, "Invalid map");
    for (int i=0; i<list->count(); i++)
    {
      NotifyClientEntry* entry = list->at(i);
      if (entry->getObject() == obj)
        entryList.push_back(entry);
    }
    // Remove all entries for this object
    foreach(NotifyClientEntry* entry, entryList)
      list->removeAll(entry);

    if (list->count() == 0)
      mNotifyChangedMap.remove(propertyName);
  }
}

const CarbonProperties* CarbonOptions::getProperties() const
{
  return mProperties;
}

CarbonPropertyValue* CarbonOptions::lookupValue(const CarbonProperty* sw) const
{
  SwitchMap::const_iterator q = mSwitchValues.find(sw->getName());
  if (q != mSwitchValues.end()) 
  {
    CarbonPropertyValue* sv = q->second;
    return sv;
  }
  return NULL;
}

// Write out any settings at this level
bool CarbonOptions::serialize(xmlTextWriterPtr writer)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Tool");
  xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST getName());

  UtXmlErrorHandler eh;

  const CarbonProperties* props = getProperties();
  for (UInt32 i=0; i<props->numGroups(); ++i)
  {
    CarbonPropertyGroup* group = props->getGroup(i);

    for (CarbonPropertyGroup::SwitchLoop p = group->loopSwitches(); !p.atEnd(); ++p)
    {
      CarbonProperty* prop = p.getValue();
      bool isDefaulted, isThisLevel;
      CarbonPropertyValue* propValue = getValue(prop,&isDefaulted,&isThisLevel);
      if (isThisLevel && !prop->getReadOnly())
        prop->writeValueXML(writer, propValue, &eh);
    }
  }
  xmlTextWriterEndElement(writer);

  return true;
}


const CarbonPropertyGroup* CarbonOptions::findGroup(const char* name)
{
  return mProperties->findGroup(name);
}

const char* CarbonOptions::getParentValue(const char* propName) const
{
  const CarbonOptions* parentOptions = getParent();
  if (parentOptions && parentOptions->findProperty(propName))
    return parentOptions->getValue(propName)->getValue();
  else
    return NULL;
}

const CarbonProperty* CarbonOptions::findProperty(CarbonPropertyGroup *group, const char* name) const
{
  return mProperties->findProperty(group, name);
}

const CarbonProperty* CarbonOptions::findProperty(const char* name) const
{
  return mProperties->findProperty(name);
}

const CarbonProperty* CarbonOptions::locateProperty(const char* name)
{
  return mProperties->locateProperty(name);
}

// Helper; Find the value by property name
CarbonPropertyValue* CarbonOptions::getValue(const char* propName, bool* isDefaulted, bool* isThisLevel) const
{
  const CarbonProperty* prop = mProperties->findProperty(propName);
  if (prop == NULL)
  {
    UtString err;
    err << "Error: Unable to find property: " << propName;
    INFO_ASSERT(prop != NULL, err.c_str());
  }
  return getValue(prop,isDefaulted,isThisLevel);
}


// This is the real meat here;
// We want the latest value, so walk backward to the beginning and get the last value
CarbonPropertyValue* CarbonOptions::getValue(const CarbonProperty* sw, bool* isDefaulted, bool* isThisLevel) const
{
  QList<const CarbonOptions*> mParents;

  const CarbonOptions* parent = getParent();
  mParents.push_front(this);
  while (parent != NULL)
  {
    mParents.push_front(parent);
    parent=parent->getParent();
  }

  CarbonPropertyValue* value = NULL;

  if (isThisLevel)
    *isThisLevel = false;

  // Walk forward and find the "latest" value
  foreach (const CarbonOptions* parentSettings, mParents)
  {
    if (isDefaulted) 
      *isDefaulted = false;

    CarbonPropertyValue* newVal = parentSettings->lookupValue(sw);
    if (newVal)
    {
      // Notify that this value (if returned) is the default value
      if (parentSettings->getParent() == NULL && isDefaulted)
        *isDefaulted = true;

      // Is it defined at this level ?
      if (isThisLevel)
        *isThisLevel = parentSettings == this ? true : false;

      value = newVal;
    }
  }

  // The last value we found is the winner
  return value;
}

void CarbonOptions::removeValue(const char* propName)
{
  const CarbonProperty* prop = findProperty(propName);
  const char* exclusiveWith = prop->getExclusiveWith();

  if (exclusiveWith && strlen(exclusiveWith) > 0)
    mSwitchValues.erase(exclusiveWith);

  mSwitchValues.erase(propName);
}


void CarbonOptions::removeProperty(const CarbonProperty* prop)
{
  const char *propName = prop->getName();
  
  const char* exclusiveWith = prop->getExclusiveWith();

  if (exclusiveWith && strlen(exclusiveWith) > 0)
  {
    const CarbonProperty* exProp = findProperty(exclusiveWith);
    mSwitchValues.erase(exclusiveWith);
    mProperties->removeProperty(exProp);
  }

  mSwitchValues.erase(propName);
  mProperties->removeProperty(prop);

}

void CarbonOptions::removeGroup(const char* groupName)
{
  CarbonPropertyGroup* group = mProperties->findGroup(groupName);
  
  if (group)
  {
    for (CarbonPropertyGroup::SwitchLoop p = group->loopSwitches(); !p.atEnd(); ++p)
    {
      const CarbonProperty* sw = p.getValue();
      mSwitchValues.erase(sw->getName());
    }
  }

  mProperties->removeGroup(groupName);
}

void CarbonOptions::removeProperty(const char* propName)
{
  const CarbonProperty* prop = findProperty(propName);
  if (prop)
    removeProperty(prop);
}


void CarbonOptions::registerScriptTypes(QMap<QString, const QMetaObject*>& map)
{
  map["CarbonOptions*"] = &CarbonOptions::staticMetaObject;
  map["CarbonPropertyValue*"] = &CarbonPropertyValue::staticMetaObject;
}

// Register intellisense
void CarbonOptions::registerIntellisense(JavaScriptAPIs*)
{
}

void CarbonOptions::registerTypes(QScriptEngine* engine)
{
  // Makes these types known to the scripting engine
  qScriptRegisterQObjectMetaType<CarbonOptions*>(engine);
  qScriptRegisterQObjectMetaType<CarbonPropertyValue*>(engine);
}
