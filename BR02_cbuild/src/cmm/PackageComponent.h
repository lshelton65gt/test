// -*-C++-*-
/******************************************************************************
 Copyright (c) 2008-2010 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef __CARBONPACKAGECOMPONENT_H__
#define __CARBONPACKAGECOMPONENT_H__

#include "CarbonComponent.h"
#include "util/XmlParsing.h"
#include "util/UtHashMap.h"
#include "util/UtDLList.h"
#include "PackageTreeItem.h"

class PackageComponent : public CarbonComponent {
  Q_OBJECT

public:
  typedef UtHashMap<UtString, PackageTreeItem*> TreeMap;

  PackageComponent(CarbonProject* proj);
  virtual ~PackageComponent();
  void setDefaultValues();

  // Overrides
  static PackageComponent* deserialize(CarbonProject* proj, xmlNodePtr parent, UtXmlErrorHandler* eh);
  bool serialize(xmlTextWriterPtr writer);
  void deserializePackage(xmlNodePtr parent, UtXmlErrorHandler* eh);
  bool canCompile(CarbonConfiguration*) { return true; }
  CarbonComponentTreeItem* createTreeItem(CarbonProjectWidget* tree, QTreeWidgetItem* parent);

  CarbonConfigurations* getConfigurations() { return mConfigs; }
  
  // Create component directory
  void createDirectory(CarbonConfiguration* config);

  // Access Methods
  PackageTreeItem* getRootItem();
  bool containsComp(const char* compName);

  // Component Options
  QString InstallerName();
  const char* getProductName();
  void setProductName(const char* value);
  const char* getShortName();
  void setShortName(const char* value);
  const char* getVersion();
  void setVersion(const char* value);
  
  // Handle File Tree
  bool addComponentToPackage(const char* compName);
  bool removeComponentFromPackage(const char* compName);
  bool addFileToPackage(PackageTreeItem* node, const char* fileName);
  bool removeFileFromPackage(PackageTreeItem* node, const char* fileName);

  // Makefile Generation
  const char* getTargetName(CarbonConfiguration*,TargetType type, TargetPlatform);
  bool getTargetImpl(CarbonConfiguration* cfg, QTextStream& stream, TargetType type);

  // Configuration Management
public slots:
  void configurationChanged(const char* conf);
  void configurationRemoved(const char* conf);
  void configurationRenamed(const char*, const char*);
  void configurationCopy(const char*, const char*);

private:
  // Configuration methods
  CarbonOptions* getOptions(const char* configName = NULL) const;
  const char* getOption(const char* optionName);
  void setOption(const char* optionName, const char* value);
 
  // Parse configuration's from .carbon file
  void createConfigurations(xmlNodePtr parent);

  // Adds a file to the package
  bool addFile(const UtString &packageLoc, const UtString &source, const char* compName, PackageTreeItem* dirItem);
  
  // Conveniance methods for makefile generation
  UtString getAllSourceFiles(PackageTreeItem* root);
  void setIjPlatform(QTextStream& stream);
  void PackageCommand(QTextStream& stream);

  // Project Parameters
  UtString mProjName;
  UtString mCurrentConfig;

  CarbonConfigurations* mConfigs;

  // Package Parameters
  TreeMap mRootItems;
  UtHashMap<UtString, UInt32> mSelComps;
};

#endif

