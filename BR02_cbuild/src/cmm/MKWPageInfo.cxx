//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "ModelKitWizard.h"
#include "MKWPageInfo.h"
#include "KitManifest.h"

// Settings Keys
#define KIT_NAME "modelKits/kitName"
#define ARM_KIT_ROOT "modelKits/armKitRoot"
#define SAVED_MANIFEST_NAME "modelKits/manifestName"

bool MKWPageInfo::validatePage()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());

  QString dir = ui.lineEditKitDir->text();
  if (dir.endsWith("/"))
    dir = dir.left(dir.length()-1);

  ui.lineEditKitDir->setText(dir);

  wiz->setARMRoot(dir);

  saveSettings();
  return true;
}

void MKWPageInfo::saveSettings()
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  QString baseName;
  if (proj)
    baseName = QString(proj->getProjectDirectory()) + "/";
  
  mSettings.setValue(baseName + ARM_KIT_ROOT, ui.lineEditKitDir->text());  
}


void MKWPageInfo::restoreSettings()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  QString baseName;
  if (proj)
    baseName = QString(proj->getProjectDirectory()) + "/";

  ui.lineEditKitDir->setText(mSettings.value(baseName + ARM_KIT_ROOT,"").toString());

  if (theApp->checkArgument("-armRoot"))
  {
    QString armRoot = theApp->argumentValue("-armRoot");
    ui.lineEditKitDir->setText(armRoot);
  }

  if (wiz->getRegenerate())
    wiz->next();
}

MKWPageInfo::MKWPageInfo(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);

  setTitle("Start Page");
  setSubTitle("Specify the Model Kit Details here");
}

void MKWPageInfo::initializePage()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());

  restoreSettings();

  if (wiz->getRegenerate())
    wiz->next();

}
void MKWPageInfo::on_pushButtonBrowse_clicked()
{
  QString dir = QFileDialog::getExistingDirectory(this, tr("ARM Kit Distribution Root Directory"),
    ui.lineEditKitDir->text(),
    QFileDialog::ShowDirsOnly
    | QFileDialog::DontResolveSymlinks);
  if (!dir.isEmpty())
  {
#if pfWINDOWS
    CarbonMappedDrives mappedDrives;
    QString winPath(dir);
  // Is this a dos path? (<x>:)
  if (winPath.length() >= 2 && winPath[1] == ':')
  {
    QString driveLetter = winPath.left(2).toUpper();
    if (!mappedDrives.isDriveLetterMapped(driveLetter))
    {
      QMessageBox::critical(this, MODELSTUDIO_TITLE, "Drive is not mapped, aborting.");
      return;
    }
  }
#endif

    if (dir.endsWith("/"))
      dir = dir.left(dir.length()-1);

    ui.lineEditKitDir->setText(dir);
   
    CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
    CarbonProject* proj = pw->project();
    UtString rootDir;
    rootDir << dir;

    qDebug() << "unix path" << proj->getUnixEquivalentPath(rootDir.c_str());
  }
}

MKWPageInfo::~MKWPageInfo()
{
}
