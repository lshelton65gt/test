#ifndef DLGNEWPROJECT_H
#define DLGNEWPROJECT_H
#include "util/CarbonPlatform.h"

#include <QDialog>
#include "ui_DlgNewProject.h"

#include "util/UtString.h"

class CarbonProjectWidget;

class DlgNewProject : public QDialog
{
  Q_OBJECT

public:
  DlgNewProject(QWidget *parent = 0, CarbonProjectWidget* pw = 0);
  ~DlgNewProject();

  enum ProjectKind { EmptyProject, FromExistingCommand, FromExistingDatabase, FromCcfgCoware, FromCcfgMaxsim, FromModelKit};
  enum ProjectCompilation { Local, Remote };

  const char* getProjectDirectory() { return mProjectDirectory.c_str(); }
  const char* getProjectName() { return mProjectName.c_str(); }
  
  const char* getRemoteWorkingDirectory() { return mRemoteWorkingDir.c_str(); }
  const char* getRemoteUsername() { return mUsername.c_str(); }
  const char* getRemoteServer() { return mServer.c_str(); }
  bool isRemoteCompilation() { return mCompilation == Remote; }

  ProjectKind getProjectKind() { return mKind; }
  ProjectCompilation getProjectCompilation() { return mCompilation; }

private:
  void createIcons();
  void updateDefaultName(const char*);
  void enableControls();
  void populateMappedDrives();

private:
  Ui::DlgNewProjectClass ui;

private slots:
    void on_listWidgetProjectKind_itemDoubleClicked(QListWidgetItem*);
  void on_buttonBox_accepted();
  void on_lineEditProjectName_textChanged(QString);
  void on_pushButtonBrowse_clicked();
  void on_buttonBox_rejected();
  void projectTypeChanged();
  void on_pushButtonMapDrives_clicked();
  void on_checkBoxRemoteCompile_clicked();
  void mappedDriveChanged();

private:
  CarbonProjectWidget* mProjectWidget;
  
  UtString mProjectDirectory;
  UtString mProjectName;
  
  UtString mServer;
  UtString mUsername;
  UtString mRemoteWorkingDir;

  ProjectKind mKind;
  ProjectCompilation mCompilation;

  QListWidgetItem* mKindEmpty;
  QListWidgetItem* mKindFromExistingCmd;
  QListWidgetItem* mKindFromExistingDB;
  QListWidgetItem* mKindFromExistingCfgCoWare;
  QListWidgetItem* mKindFromExistingCfgMaxsim;
  QListWidgetItem* mKindFromModelKit;

};

#endif // DLGNEWPROJECT_H
