//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "Modes.h"
#include "Mode.h"
#include "Template.h"

QObject* Modes::getMode(int index)
{
  return mModes[index];
}

bool Modes::deserialize(Template* templ, const QDomElement& e, XmlErrorHandler* eh)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    if (!element.isNull())
    {
      if ("Modes" == element.tagName())
      {
        if (!Mode::deserialize(templ, this, element, eh))
          return false;
      }
    }
    n = n.nextSibling();
  }

  return this;}


