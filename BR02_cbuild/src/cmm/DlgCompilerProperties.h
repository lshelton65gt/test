#ifndef DLGCOMPILERPROPERTIES_H
#define DLGCOMPILERPROPERTIES_H
#include "util/CarbonPlatform.h"

#include <QDialog>
#include "gui/CQt.h"

#include "ui_DlgCompilerProperties.h"

#include "CarbonProperties.h"
#include "CarbonProjectWidget.h"

class CarbonMakerContext;

class DlgCompilerProperties : public QDialog
{
  Q_OBJECT

public:
  DlgCompilerProperties(QWidget *parent = 0);
  ~DlgCompilerProperties();
  void putContext(CarbonMakerContext* ctx);

private:
  Ui::DlgCompilerPropertiesClass ui;

private:
  CarbonMakerContext* mContext;

private slots:
        void on_buttonBox_accepted();
        void on_buttonBox_rejected();
        void on_comboBoxConfigs_currentIndexChanged(int);
};

#endif // DLGCOMPILERPROPERTIES_H
