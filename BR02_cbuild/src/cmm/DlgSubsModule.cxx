//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "DlgSubsModule.h"

DlgSubsModule::DlgSubsModule(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);
  mResult = eSubsCancelled;
}

DlgSubsModule::~DlgSubsModule()
{
}

void DlgSubsModule::on_pushButtonOK_clicked()
{
  mResult = eSubsAccepted;
  accept();
}
void DlgSubsModule::on_pushButtonCancel_clicked()
{
  mResult = eSubsCancelled;
  reject();
}

void DlgSubsModule::on_pushButtonDelete_clicked()
{
  mResult = eSubsDelete;
  reject();
}

void DlgSubsModule::setExpression(const QString& expr)
{
  mExpression = expr;
  QStringList tokens = expr.split(':');
  if (tokens.count() >= 4)
  {
    ui.lineEditReplace->setText(tokens[0]);
    ui.lineEditReplaceMap->setText(tokens[1]);
    ui.lineEditWith->setText(tokens[2]);
    ui.lineEditWithMap->setText(tokens[3]);
  }
 
}

QString DlgSubsModule::getExpression()
{
  QString value;

  value = QString("%1:%2:%3:%4")
    .arg(ui.lineEditReplace->text())
    .arg(ui.lineEditReplaceMap->text())
    .arg(ui.lineEditWith->text())
    .arg(ui.lineEditWithMap->text());

  return value;
}




