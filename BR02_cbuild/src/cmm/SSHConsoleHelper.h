#ifndef __SSHCONSOLEHELPER_H__
#define __SSHCONSOLEHELPER_H__

#include "util/CarbonPlatform.h"

#include <QtGui>
#include <QProcess>

class SSHConsoleHelper
{
public:
  static QString shellCreateTestFileScript(const QString& scriptName);
  static QString shellCreateTestDirectoryScript(const QString& scriptName);
  static int getNetworkDrives(QStringList& driveListLocalName, QStringList& driveListRemoteName);
  // Can be slow, because it has to enumerate the drives every time
  static bool isNetworkDrive(const QChar inputDriveLetter);
  // Works off the cached list instead
  static bool isNetworkDrive(const QChar inputDriveLetter, const QStringList& cachedDriveLetters);

};

#endif
