//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "FileButtonBrowser.h"

FileButtonBrowser::FileButtonBrowser(QWidget *parent)
: QWidget(parent)
{
  ui.setupUi(this);

  mBrowseTitle = "Choose File...";
  mBrowseFilter = "All Files (* *.*)";
}

FileButtonBrowser::~FileButtonBrowser()
{
}

void FileButtonBrowser::on_pushButtonBrowse_clicked()
{
  QString fileName = QFileDialog::getOpenFileName(this, mBrowseTitle, "", mBrowseFilter);
  if (!fileName.isEmpty())
  {
    ui.lineEditFileName->setText(fileName);
    emit valueChanged();
  }
}
