PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLoc_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLoc* obj = (ProtoCarbonCfgRegisterLoc*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CarbonCfgRegisterLocConstant* CSharp_ProtoCarbonCfgRegisterLoc_castConstant_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLoc* obj = (ProtoCarbonCfgRegisterLoc*)(thisObj);
    return obj->castConstant();
}
PINVOKEABLE CarbonCfgRegisterLocRTL* CSharp_ProtoCarbonCfgRegisterLoc_castRTL_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLoc* obj = (ProtoCarbonCfgRegisterLoc*)(thisObj);
    return obj->castRTL();
}
PINVOKEABLE CarbonCfgRegisterLocReg* CSharp_ProtoCarbonCfgRegisterLoc_castReg_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLoc* obj = (ProtoCarbonCfgRegisterLoc*)(thisObj);
    return obj->castReg();
}
PINVOKEABLE CarbonCfgRegisterLocArray* CSharp_ProtoCarbonCfgRegisterLoc_castArray_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLoc* obj = (ProtoCarbonCfgRegisterLoc*)(thisObj);
    return obj->castArray();
}
PINVOKEABLE CarbonCfgRegisterLocUser* CSharp_ProtoCarbonCfgRegisterLoc_castUser_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLoc* obj = (ProtoCarbonCfgRegisterLoc*)(thisObj);
    return obj->castUser();
}
PINVOKEABLE CcfgEnum::CarbonCfgRegisterLocKind CSharp_ProtoCarbonCfgRegisterLoc_getType_0_35(void* thisObj)
{
    ProtoCarbonCfgRegisterLoc* obj = (ProtoCarbonCfgRegisterLoc*)(thisObj);
    return obj->getType();
}
