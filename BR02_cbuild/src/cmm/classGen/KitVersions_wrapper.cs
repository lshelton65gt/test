using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class Versions : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal Versions(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(Versions obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~Versions()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_KitVersions_numVersions_0_3")]
    private extern static uint CSharp_KitVersions_numVersions_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitVersions_getVersion_1_19")]
    private extern static IntPtr CSharp_KitVersions_getVersion_1_19(HandleRef thisObj, uint index);
    #endregion DllImports
    #region Properties
    public uint NumVersions
    {
      get
      {
        return numVersions();
      }
    }
    #endregion Properties
    #region Methods
    public uint numVersions()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Versions object");
      return CSharp_KitVersions_numVersions_0_3(objCPtr);
    }

    #endregion Methods

  }

}
