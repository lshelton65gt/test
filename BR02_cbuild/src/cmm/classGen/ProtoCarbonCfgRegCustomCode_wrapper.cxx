PINVOKEABLE void CSharp_ProtoCarbonCfgRegCustomCode_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgRegCustomCode* obj = (ProtoCarbonCfgRegCustomCode*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CcfgEnum::CarbonCfgCustomCodePosition CSharp_ProtoCarbonCfgRegCustomCode_getPosition_0_7(void* thisObj)
{
    ProtoCarbonCfgRegCustomCode* obj = (ProtoCarbonCfgRegCustomCode*)(thisObj);
    return obj->getPosition();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegCustomCode_setPosition_1_3(void* thisObj, CcfgEnum::CarbonCfgCustomCodePosition position)
{
    ProtoCarbonCfgRegCustomCode* obj = (ProtoCarbonCfgRegCustomCode*)(thisObj);
    obj->setPosition(position);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgRegCustomCode_getCode_0_3(void* thisObj)
{
    ProtoCarbonCfgRegCustomCode* obj = (ProtoCarbonCfgRegCustomCode*)(thisObj);
    return EmbeddedMono::AllocString(obj->getCode());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegCustomCode_setCode_1_1(void* thisObj, const char* code)
{
    ProtoCarbonCfgRegCustomCode* obj = (ProtoCarbonCfgRegCustomCode*)(thisObj);
    obj->setCode(code);
}
PINVOKEABLE CcfgEnum::CarbonCfgRegCustomCodeSection CSharp_ProtoCarbonCfgRegCustomCode_getSection_0_7(void* thisObj)
{
    ProtoCarbonCfgRegCustomCode* obj = (ProtoCarbonCfgRegCustomCode*)(thisObj);
    return obj->getSection();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegCustomCode_setSection_1_3(void* thisObj, CcfgEnum::CarbonCfgRegCustomCodeSection section)
{
    ProtoCarbonCfgRegCustomCode* obj = (ProtoCarbonCfgRegCustomCode*)(thisObj);
    obj->setSection(section);
}
PINVOKEABLE ProtoCarbonCfgCustomCode* CSharp_ProtoCarbonCfgRegCustomCode_UpCast(ProtoCarbonCfgRegCustomCode* obj)
{
  return (ProtoCarbonCfgCustomCode*)(obj);
}
