using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class phaseScopeTypeEnum
  {
    public enum phaseScopeType
    {
      GLOBAL = 0,
      LOCAL = 1
    }
  }
}
