using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgESLPort : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgESLPort(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgESLPort obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgESLPort()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgESLPort_name_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgESLPort_name_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgESLPort_expr_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgESLPort_expr_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgESLPort_getPortType_0_35")]
    private extern static int CSharp_ProtoCarbonCfgESLPort_getPortType_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgESLPort_getParameterInstance_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgESLPort_getParameterInstance_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgESLPort_setParameterInstance_1_3")]
    private extern static void CSharp_ProtoCarbonCfgESLPort_setParameterInstance_1_3(HandleRef thisObj, IntPtr inst);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgESLPort_putExpr_1_1")]
    private extern static void CSharp_ProtoCarbonCfgESLPort_putExpr_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgESLPort_typeDef_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgESLPort_typeDef_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgESLPort_rtlPort_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgESLPort_rtlPort_0_7(HandleRef thisObj);
    #endregion DllImports
    #region Properties
    public string Name
    {
      get
      {
        return name();
      }
    }
    public string Expr
    {
      set
      {
        putExpr(value);
      }
      get
      {
        return expr();
      }
    }
    public string TypeDef
    {
      get
      {
        return typeDef();
      }
    }
    public CarbonCfgRTLPort RtlPort
    {
      get
      {
        return rtlPort();
      }
    }
    public CcfgEnum.CarbonCfgESLPortType PortType
    {
      get
      {
        return getPortType();
      }
    }
    public CarbonCfgXtorParamInst ParameterInstance
    {
      set
      {
        setParameterInstance(value);
      }
      get
      {
        return getParameterInstance();
      }
    }
    #endregion Properties
    #region Methods
    public string name()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgESLPort object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgESLPort_name_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string expr()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgESLPort object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgESLPort_expr_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public CcfgEnum.CarbonCfgESLPortType getPortType()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgESLPort object");
      return (CcfgEnum.CarbonCfgESLPortType)CSharp_ProtoCarbonCfgESLPort_getPortType_0_35(objCPtr);
    }

    public CarbonCfgXtorParamInst getParameterInstance()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgESLPort object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgESLPort_getParameterInstance_0_3(objCPtr);
      CarbonCfgXtorParamInst ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorParamInst(cPtr, false);
      return ret;
    }

    public void setParameterInstance(CarbonCfgXtorParamInst inst)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgESLPort object");
      CSharp_ProtoCarbonCfgESLPort_setParameterInstance_1_3(objCPtr, CarbonCfgXtorParamInst.getCPtr(inst).Handle);
    }

    public void putExpr(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgESLPort object");
      CSharp_ProtoCarbonCfgESLPort_putExpr_1_1(objCPtr, newVal);
    }

    public string typeDef()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgESLPort object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgESLPort_typeDef_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public CarbonCfgRTLPort rtlPort()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgESLPort object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgESLPort_rtlPort_0_7(objCPtr);
      CarbonCfgRTLPort ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRTLPort(cPtr, false);
      return ret;
    }

    #endregion Methods

  }

}
