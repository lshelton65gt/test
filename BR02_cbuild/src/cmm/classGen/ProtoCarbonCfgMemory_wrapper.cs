using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgMemory : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgMemory(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgMemory obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgMemory()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_GetEditFlags_0_3")]
    private extern static int CSharp_ProtoCarbonCfgMemory_GetEditFlags_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_SetEditFlags_1_1")]
    private extern static void CSharp_ProtoCarbonCfgMemory_SetEditFlags_1_1(HandleRef thisObj, int newValue);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_numMemoryBlocks_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgMemory_numMemoryBlocks_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_getName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemory_getName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_getInitFile_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemory_getInitFile_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_setInitFile_1_1")]
    private extern static void CSharp_ProtoCarbonCfgMemory_setInitFile_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_getComment_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemory_getComment_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_setComment_1_1")]
    private extern static void CSharp_ProtoCarbonCfgMemory_setComment_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_getWidth_0_7")]
    private extern static uint CSharp_ProtoCarbonCfgMemory_getWidth_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_getReadmemType_0_35")]
    private extern static int CSharp_ProtoCarbonCfgMemory_getReadmemType_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_setReadmemType_1_21")]
    private extern static void CSharp_ProtoCarbonCfgMemory_setReadmemType_1_21(HandleRef thisObj, int newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_getInitType_0_35")]
    private extern static int CSharp_ProtoCarbonCfgMemory_getInitType_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_setInitType_1_53")]
    private extern static void CSharp_ProtoCarbonCfgMemory_setInitType_1_53(HandleRef thisObj, int newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_getSystemAddressESLPortName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemory_getSystemAddressESLPortName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_setProgPreloadEslPort_1_1")]
    private extern static void CSharp_ProtoCarbonCfgMemory_setProgPreloadEslPort_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_getDisassemblyName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemory_getDisassemblyName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_setDisassemblyName_1_1")]
    private extern static void CSharp_ProtoCarbonCfgMemory_setDisassemblyName_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_getDisplayAtZero_0_3")]
    private extern static bool CSharp_ProtoCarbonCfgMemory_getDisplayAtZero_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_setDisplayAtZero_1_3")]
    private extern static void CSharp_ProtoCarbonCfgMemory_setDisplayAtZero_1_3(HandleRef thisObj, bool newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_getMAU_0_35")]
    private extern static uint CSharp_ProtoCarbonCfgMemory_getMAU_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_setMAU_1_19")]
    private extern static void CSharp_ProtoCarbonCfgMemory_setMAU_1_19(HandleRef thisObj, uint newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_getBigEndian_0_7")]
    private extern static bool CSharp_ProtoCarbonCfgMemory_getBigEndian_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_setBigEndian_1_3")]
    private extern static void CSharp_ProtoCarbonCfgMemory_setBigEndian_1_3(HandleRef thisObj, bool newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_getProgramMemory_0_3")]
    private extern static bool CSharp_ProtoCarbonCfgMemory_getProgramMemory_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_setProgramMemory_1_3")]
    private extern static void CSharp_ProtoCarbonCfgMemory_setProgramMemory_1_3(HandleRef thisObj, bool newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_addMemoryBlock_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemory_addMemoryBlock_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_removeMemoryBlock_1_3")]
    private extern static void CSharp_ProtoCarbonCfgMemory_removeMemoryBlock_1_3(HandleRef thisObj, IntPtr block);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_getMemoryBlock_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemory_getMemoryBlock_1_19(HandleRef thisObj, uint index);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_numCustomCodes_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgMemory_numCustomCodes_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_addCustomCode_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemory_addCustomCode_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_getCustomCode_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemory_getCustomCode_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemory_removeCustomCode_1_3")]
    private extern static void CSharp_ProtoCarbonCfgMemory_removeCustomCode_1_3(HandleRef thisObj, IntPtr customCode);
    #endregion DllImports
    #region Properties
    public uint NumMemoryBlocks
    {
      get
      {
        return numMemoryBlocks();
      }
    }
    public string Name
    {
      get
      {
        return getName();
      }
    }
    public string InitFile
    {
      set
      {
        setInitFile(value);
      }
      get
      {
        return getInitFile();
      }
    }
    public string Comment
    {
      set
      {
        setComment(value);
      }
      get
      {
        return getComment();
      }
    }
    public uint Width
    {
      get
      {
        return getWidth();
      }
    }
    public CcfgEnum.CarbonCfgReadmemType ReadmemType
    {
      set
      {
        setReadmemType(value);
      }
      get
      {
        return getReadmemType();
      }
    }
    public CcfgEnum.CarbonCfgMemInitType InitType
    {
      set
      {
        setInitType(value);
      }
      get
      {
        return getInitType();
      }
    }
    public string ProgPreloadEslPort
    {
      set
      {
        setProgPreloadEslPort(value);
      }
    }
    public string SystemAddressESLPortName
    {
      get
      {
        return getSystemAddressESLPortName();
      }
    }
    public bool DisplayAtZero
    {
      set
      {
        setDisplayAtZero(value);
      }
      get
      {
        return getDisplayAtZero();
      }
    }
    public uint MAU
    {
      set
      {
        setMAU(value);
      }
      get
      {
        return getMAU();
      }
    }
    public bool BigEndian
    {
      set
      {
        setBigEndian(value);
      }
      get
      {
        return getBigEndian();
      }
    }
    public bool ProgramMemory
    {
      set
      {
        setProgramMemory(value);
      }
      get
      {
        return getProgramMemory();
      }
    }
    public uint NumCustomCodes
    {
      get
      {
        return numCustomCodes();
      }
    }
    #endregion Properties
    #region Methods
    public CcfgFlags.CarbonCfgMemoryEditFlags GetEditFlags()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      return (CcfgFlags.CarbonCfgMemoryEditFlags)CSharp_ProtoCarbonCfgMemory_GetEditFlags_0_3(objCPtr);
    }

    public void SetEditFlags(CcfgFlags.CarbonCfgMemoryEditFlags newValue)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      CSharp_ProtoCarbonCfgMemory_SetEditFlags_1_1(objCPtr, (int)newValue);
    }

    public uint numMemoryBlocks()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      return CSharp_ProtoCarbonCfgMemory_numMemoryBlocks_0_3(objCPtr);
    }

    public string getName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgMemory_getName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getInitFile()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgMemory_getInitFile_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setInitFile(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      CSharp_ProtoCarbonCfgMemory_setInitFile_1_1(objCPtr, newVal);
    }

    public string getComment()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgMemory_getComment_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setComment(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      CSharp_ProtoCarbonCfgMemory_setComment_1_1(objCPtr, newVal);
    }

    public uint getWidth()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      return CSharp_ProtoCarbonCfgMemory_getWidth_0_7(objCPtr);
    }

    public CcfgEnum.CarbonCfgReadmemType getReadmemType()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      return (CcfgEnum.CarbonCfgReadmemType)CSharp_ProtoCarbonCfgMemory_getReadmemType_0_35(objCPtr);
    }

    public void setReadmemType(CcfgEnum.CarbonCfgReadmemType newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      CSharp_ProtoCarbonCfgMemory_setReadmemType_1_21(objCPtr, (int)newVal);
    }

    public CcfgEnum.CarbonCfgMemInitType getInitType()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      return (CcfgEnum.CarbonCfgMemInitType)CSharp_ProtoCarbonCfgMemory_getInitType_0_35(objCPtr);
    }

    public void setInitType(CcfgEnum.CarbonCfgMemInitType newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      CSharp_ProtoCarbonCfgMemory_setInitType_1_53(objCPtr, (int)newVal);
    }

    public string getSystemAddressESLPortName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgMemory_getSystemAddressESLPortName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setProgPreloadEslPort(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      CSharp_ProtoCarbonCfgMemory_setProgPreloadEslPort_1_1(objCPtr, newVal);
    }

    public string getDisassemblyName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgMemory_getDisassemblyName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setDisassemblyName(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      CSharp_ProtoCarbonCfgMemory_setDisassemblyName_1_1(objCPtr, newVal);
    }

    public bool getDisplayAtZero()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      return CSharp_ProtoCarbonCfgMemory_getDisplayAtZero_0_3(objCPtr);
    }

    public void setDisplayAtZero(bool newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      CSharp_ProtoCarbonCfgMemory_setDisplayAtZero_1_3(objCPtr, newVal);
    }

    public uint getMAU()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      return CSharp_ProtoCarbonCfgMemory_getMAU_0_35(objCPtr);
    }

    public void setMAU(uint newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      CSharp_ProtoCarbonCfgMemory_setMAU_1_19(objCPtr, newVal);
    }

    public bool getBigEndian()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      return CSharp_ProtoCarbonCfgMemory_getBigEndian_0_7(objCPtr);
    }

    public void setBigEndian(bool newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      CSharp_ProtoCarbonCfgMemory_setBigEndian_1_3(objCPtr, newVal);
    }

    public bool getProgramMemory()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      return CSharp_ProtoCarbonCfgMemory_getProgramMemory_0_3(objCPtr);
    }

    public void setProgramMemory(bool newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      CSharp_ProtoCarbonCfgMemory_setProgramMemory_1_3(objCPtr, newVal);
    }

    public CarbonCfgMemoryBlock addMemoryBlock()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgMemory_addMemoryBlock_0_3(objCPtr);
      CarbonCfgMemoryBlock ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemoryBlock(cPtr, false);
      return ret;
    }

    public void removeMemoryBlock(CarbonCfgMemoryBlock block)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      CSharp_ProtoCarbonCfgMemory_removeMemoryBlock_1_3(objCPtr, CarbonCfgMemoryBlock.getCPtr(block).Handle);
    }

    public CarbonCfgMemoryBlock getMemoryBlock(uint index)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgMemory_getMemoryBlock_1_19(objCPtr, index);
      CarbonCfgMemoryBlock ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemoryBlock(cPtr, false);
      return ret;
    }

    public uint numCustomCodes()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      return CSharp_ProtoCarbonCfgMemory_numCustomCodes_0_3(objCPtr);
    }

    public CarbonCfgMemoryCustomCode addCustomCode()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgMemory_addCustomCode_0_3(objCPtr);
      CarbonCfgMemoryCustomCode ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemoryCustomCode(cPtr, false);
      return ret;
    }

    public CarbonCfgMemoryCustomCode getCustomCode(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgMemory_getCustomCode_1_19(objCPtr, i);
      CarbonCfgMemoryCustomCode ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemoryCustomCode(cPtr, false);
      return ret;
    }

    public void removeCustomCode(CarbonCfgMemoryCustomCode customCode)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemory object");
      CSharp_ProtoCarbonCfgMemory_removeCustomCode_1_3(objCPtr, CarbonCfgMemoryCustomCode.getCPtr(customCode).Handle);
    }

    #endregion Methods

  }

}
