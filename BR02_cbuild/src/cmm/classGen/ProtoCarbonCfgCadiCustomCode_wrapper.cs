using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgCadiCustomCode : CarbonCfgCustomCode, IDisposable
  {
    private HandleRef objCPtr;
    internal CarbonCfgCadiCustomCode(IntPtr cPtr, bool cMemoryOwn) : base(CSharp_ProtoCarbonCfgCadiCustomCode_UpCast(cPtr), cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgCadiCustomCode obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgCadiCustomCode()
    {
      Dispose();
    }
    public override void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCadiCustomCode_getSection_0_7")]
    private extern static int CSharp_ProtoCarbonCfgCadiCustomCode_getSection_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCadiCustomCode_setSection_1_3")]
    private extern static void CSharp_ProtoCarbonCfgCadiCustomCode_setSection_1_3(HandleRef thisObj, int section);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCadiCustomCode_UpCast")]
    private static extern IntPtr CSharp_ProtoCarbonCfgCadiCustomCode_UpCast(IntPtr obj);
    #endregion DllImports
    #region Properties
    public CcfgEnum.CarbonCfgCadiCustomCodeSection Section
    {
      set
      {
        setSection(value);
      }
      get
      {
        return getSection();
      }
    }
    #endregion Properties
    #region Methods
    public CcfgEnum.CarbonCfgCadiCustomCodeSection getSection()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgCadiCustomCode object");
      return (CcfgEnum.CarbonCfgCadiCustomCodeSection)CSharp_ProtoCarbonCfgCadiCustomCode_getSection_0_7(objCPtr);
    }

    public void setSection(CcfgEnum.CarbonCfgCadiCustomCodeSection section)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgCadiCustomCode object");
      CSharp_ProtoCarbonCfgCadiCustomCode_setSection_1_3(objCPtr, (int)section);
    }

    #endregion Methods

  }

}
