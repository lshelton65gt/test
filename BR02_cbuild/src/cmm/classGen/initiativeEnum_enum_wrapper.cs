using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class initiativeEnum
  {
    public enum initiative
    {
      REQUIRES = 0,
      PROVIDES = 1,
      BOTH = 2,
      PHANTOM = 3
    }
  }
}
