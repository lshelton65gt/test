using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgCompCustomCode : CarbonCfgCustomCode, IDisposable
  {
    private HandleRef objCPtr;
    internal CarbonCfgCompCustomCode(IntPtr cPtr, bool cMemoryOwn) : base(CSharp_ProtoCarbonCfgCompCustomCode_UpCast(cPtr), cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgCompCustomCode obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgCompCustomCode()
    {
      Dispose();
    }
    public override void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCompCustomCode_getSection_0_7")]
    private extern static int CSharp_ProtoCarbonCfgCompCustomCode_getSection_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCompCustomCode_setSection_1_3")]
    private extern static void CSharp_ProtoCarbonCfgCompCustomCode_setSection_1_3(HandleRef thisObj, int section);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCompCustomCode_UpCast")]
    private static extern IntPtr CSharp_ProtoCarbonCfgCompCustomCode_UpCast(IntPtr obj);
    #endregion DllImports
    #region Properties
    public CcfgEnum.CarbonCfgCompCustomCodeSection Section
    {
      set
      {
        setSection(value);
      }
      get
      {
        return getSection();
      }
    }
    #endregion Properties
    #region Methods
    public CcfgEnum.CarbonCfgCompCustomCodeSection getSection()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgCompCustomCode object");
      return (CcfgEnum.CarbonCfgCompCustomCodeSection)CSharp_ProtoCarbonCfgCompCustomCode_getSection_0_7(objCPtr);
    }

    public void setSection(CcfgEnum.CarbonCfgCompCustomCodeSection section)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgCompCustomCode object");
      CSharp_ProtoCarbonCfgCompCustomCode_setSection_1_3(objCPtr, (int)section);
    }

    #endregion Methods

  }

}
