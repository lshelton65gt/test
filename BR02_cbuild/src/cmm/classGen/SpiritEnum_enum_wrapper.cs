using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class SpiritEnum
  {
    public enum SpiritFormat
    {
      UnsetFormat = 0,
      BitString = 1,
      Boolean = 2,
      Float = 3,
      Long = 4,
      String = 5
    }
    public enum SpiritResolve
    {
      UnsetResolve = 0,
      Immediate = 1,
      User = 2,
      Dependent = 3,
      Generated = 4
    }
    public enum SpiritAccess
    {
      UnsetAccess = 0,
      ReadWrite = 1,
      ReadOnly = 2,
      WriteOnly = 3
    }
    public enum SpiritUsage
    {
      UnsetUsage = 0,
      Memory = 1,
      Register = 2,
      Reserved = 3
    }
    public enum SpiritEndianness
    {
      UnsetEndian = 0,
      LittleEndian = 1,
      BigEndian = 2
    }
    public enum SpiritLocation
    {
      UnsetLocation = 0,
      LocationRegister = 1,
      LocationArray = 2,
      LocationConstant = 3
    }
    public enum SpiritAlignment
    {
      Parallel = 0,
      Serial = 1
    }
    public enum SpiritDirection
    {
      Input = 0,
      Output = 1,
      Bidirect = 2
    }
  }
}
