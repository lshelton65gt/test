PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocConstant_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocConstant* obj = (ProtoCarbonCfgRegisterLocConstant*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CarbonCfgRegisterLocConstant* CSharp_ProtoCarbonCfgRegisterLocConstant_castConstant_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocConstant* obj = (ProtoCarbonCfgRegisterLocConstant*)(thisObj);
    return obj->castConstant();
}
PINVOKEABLE CarbonCfgRegisterLocRTL* CSharp_ProtoCarbonCfgRegisterLocConstant_castRTL_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocConstant* obj = (ProtoCarbonCfgRegisterLocConstant*)(thisObj);
    return obj->castRTL();
}
PINVOKEABLE CarbonCfgRegisterLocReg* CSharp_ProtoCarbonCfgRegisterLocConstant_castReg_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLocConstant* obj = (ProtoCarbonCfgRegisterLocConstant*)(thisObj);
    return obj->castReg();
}
PINVOKEABLE CarbonCfgRegisterLocArray* CSharp_ProtoCarbonCfgRegisterLocConstant_castArray_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLocConstant* obj = (ProtoCarbonCfgRegisterLocConstant*)(thisObj);
    return obj->castArray();
}
PINVOKEABLE CarbonCfgRegisterLocUser* CSharp_ProtoCarbonCfgRegisterLocConstant_castUser_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocConstant* obj = (ProtoCarbonCfgRegisterLocConstant*)(thisObj);
    return obj->castUser();
}
PINVOKEABLE CcfgEnum::CarbonCfgRegisterLocKind CSharp_ProtoCarbonCfgRegisterLocConstant_getType_0_35(void* thisObj)
{
    ProtoCarbonCfgRegisterLocConstant* obj = (ProtoCarbonCfgRegisterLocConstant*)(thisObj);
    return obj->getType();
}
PINVOKEABLE unsigned long long CSharp_ProtoCarbonCfgRegisterLocConstant_getValue_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLocConstant* obj = (ProtoCarbonCfgRegisterLocConstant*)(thisObj);
    return obj->getValue();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocConstant_setValue_1_7(void* thisObj, unsigned long long newVal)
{
    ProtoCarbonCfgRegisterLocConstant* obj = (ProtoCarbonCfgRegisterLocConstant*)(thisObj);
    obj->setValue(newVal);
}
PINVOKEABLE ProtoCarbonCfgRegisterLoc* CSharp_ProtoCarbonCfgRegisterLocConstant_UpCast(ProtoCarbonCfgRegisterLocConstant* obj)
{
  return (ProtoCarbonCfgRegisterLoc*)(obj);
}
