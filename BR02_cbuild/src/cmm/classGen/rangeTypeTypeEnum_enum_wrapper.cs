using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class rangeTypeTypeEnum
  {
    public enum rangeTypeType
    {
      FLOAT = 0,
      INT = 1,
      UNSIGNEDINT = 2,
      LONG = 3,
      UNSIGNEDLONG = 4
    }
  }
}
