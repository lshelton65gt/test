PINVOKEABLE void CSharp_ProtoCarbonCfgESLPort_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgESLPort* obj = (ProtoCarbonCfgESLPort*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgESLPort_name_0_3(void* thisObj)
{
    ProtoCarbonCfgESLPort* obj = (ProtoCarbonCfgESLPort*)(thisObj);
    return EmbeddedMono::AllocString(obj->name());
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgESLPort_expr_0_7(void* thisObj)
{
    ProtoCarbonCfgESLPort* obj = (ProtoCarbonCfgESLPort*)(thisObj);
    return EmbeddedMono::AllocString(obj->expr());
}
PINVOKEABLE CcfgEnum::CarbonCfgESLPortType CSharp_ProtoCarbonCfgESLPort_getPortType_0_35(void* thisObj)
{
    ProtoCarbonCfgESLPort* obj = (ProtoCarbonCfgESLPort*)(thisObj);
    return obj->getPortType();
}
PINVOKEABLE CarbonCfgXtorParamInst* CSharp_ProtoCarbonCfgESLPort_getParameterInstance_0_3(void* thisObj)
{
    ProtoCarbonCfgESLPort* obj = (ProtoCarbonCfgESLPort*)(thisObj);
    return obj->getParameterInstance();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgESLPort_setParameterInstance_1_3(void* thisObj, CarbonCfgXtorParamInst* inst)
{
    ProtoCarbonCfgESLPort* obj = (ProtoCarbonCfgESLPort*)(thisObj);
    obj->setParameterInstance(inst);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgESLPort_putExpr_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgESLPort* obj = (ProtoCarbonCfgESLPort*)(thisObj);
    obj->putExpr(newVal);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgESLPort_typeDef_0_7(void* thisObj)
{
    ProtoCarbonCfgESLPort* obj = (ProtoCarbonCfgESLPort*)(thisObj);
    return EmbeddedMono::AllocString(obj->typeDef());
}
PINVOKEABLE CarbonCfgRTLPort* CSharp_ProtoCarbonCfgESLPort_rtlPort_0_7(void* thisObj)
{
    ProtoCarbonCfgESLPort* obj = (ProtoCarbonCfgESLPort*)(thisObj);
    return obj->rtlPort();
}
