using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgRegisterLocReg : CarbonCfgRegisterLocRTL, IDisposable
  {
    private HandleRef objCPtr;
    internal CarbonCfgRegisterLocReg(IntPtr cPtr, bool cMemoryOwn) : base(CSharp_ProtoCarbonCfgRegisterLocReg_UpCast(cPtr), cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgRegisterLocReg obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgRegisterLocReg()
    {
      Dispose();
    }
    public override void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocReg_UpCast")]
    private static extern IntPtr CSharp_ProtoCarbonCfgRegisterLocReg_UpCast(IntPtr obj);
    #endregion DllImports
    #region Properties
    #endregion Properties
    #region Methods
    #endregion Methods

  }

}
