PINVOKEABLE void CSharp_ScrCarbonDB_deleteLater_0_7(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE char* CSharp_ScrCarbonDB_getDatabaseFileName_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return EmbeddedMono::AllocString(obj->getDatabaseFileName());
}
PINVOKEABLE bool CSharp_ScrCarbonDB_hasDatabase_0_35(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->hasDatabase();
}
PINVOKEABLE CarbonDBNode* CSharp_ScrCarbonDB_findNode_1_1(void* thisObj, const char* path)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->findNode(path);
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopPrimaryPorts_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopPrimaryPorts();
}
PINVOKEABLE char* CSharp_ScrCarbonDB_nodeGetFullName_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return EmbeddedMono::AllocString(obj->nodeGetFullName(node));
}
PINVOKEABLE char* CSharp_ScrCarbonDB_nodeGetLeafName_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return EmbeddedMono::AllocString(obj->nodeGetLeafName(node));
}
PINVOKEABLE CarbonDBNode* CSharp_ScrCarbonDB_nodeIterNext_1_3(void* thisObj, CarbonDBNodeIter* iter)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIterNext(iter);
}
PINVOKEABLE void CSharp_ScrCarbonDB_freeNodeIter_1_3(void* thisObj, CarbonDBNodeIter* iter)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    obj->freeNodeIter(iter);
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopChildren_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopChildren(node);
}
PINVOKEABLE CarbonDBNode* CSharp_ScrCarbonDB_nodeGetParent_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeGetParent(node);
}
PINVOKEABLE CarbonDBNode* CSharp_ScrCarbonDB_findChild_2_1(void* thisObj, CarbonDBNode* parent, const char* childName)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->findChild(parent, childName);
}
PINVOKEABLE char* CSharp_ScrCarbonDB_getVersion_0_7(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return EmbeddedMono::AllocString(obj->getVersion());
}
PINVOKEABLE char* CSharp_ScrCarbonDB_getSoftwareVersion_0_7(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return EmbeddedMono::AllocString(obj->getSoftwareVersion());
}
PINVOKEABLE char* CSharp_ScrCarbonDB_getIdString_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return EmbeddedMono::AllocString(obj->getIdString());
}
PINVOKEABLE char* CSharp_ScrCarbonDB_getTopLevelModuleName_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return EmbeddedMono::AllocString(obj->getTopLevelModuleName());
}
PINVOKEABLE char* CSharp_ScrCarbonDB_getInterfaceName_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return EmbeddedMono::AllocString(obj->getInterfaceName());
}
PINVOKEABLE char* CSharp_ScrCarbonDB_getSystemCModuleName_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return EmbeddedMono::AllocString(obj->getSystemCModuleName());
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopPrimaryInputs_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopPrimaryInputs();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopPrimaryOutputs_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopPrimaryOutputs();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopPrimaryBidis_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopPrimaryBidis();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopPrimaryClks_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopPrimaryClks();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopClkTree_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopClkTree();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopDepositable_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopDepositable();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopObservable_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopObservable();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopScDepositable_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopScDepositable();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopScObservable_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopScObservable();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopLoopAsyncs_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopLoopAsyncs();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopAsyncOutputs_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopAsyncOutputs();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopAsyncDeposits_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopAsyncDeposits();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopAsyncPosResets_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopAsyncPosResets();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopAsyncNegResets_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopAsyncNegResets();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopPosedgeTriggers_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopPosedgeTriggers();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopNegedgeTriggers_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopNegedgeTriggers();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopBothEdgeTriggers_0_3(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopBothEdgeTriggers();
}
PINVOKEABLE CarbonDBNodeIter* CSharp_ScrCarbonDB_loopPosedgeTrigger_0_7(void* thisObj)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->loopPosedgeTrigger();
}
PINVOKEABLE char* CSharp_ScrCarbonDB_nodeComponentName_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return EmbeddedMono::AllocString(obj->nodeComponentName(node));
}
PINVOKEABLE char* CSharp_ScrCarbonDB_nodeSourceLanguage_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return EmbeddedMono::AllocString(obj->nodeSourceLanguage(node));
}
PINVOKEABLE char* CSharp_ScrCarbonDB_nodeGetSourceFile_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return EmbeddedMono::AllocString(obj->nodeGetSourceFile(node));
}
PINVOKEABLE int CSharp_ScrCarbonDB_nodeGetWidth_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeGetWidth(node);
}
PINVOKEABLE int CSharp_ScrCarbonDB_nodeGetMSB_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeGetMSB(node);
}
PINVOKEABLE int CSharp_ScrCarbonDB_nodeGetLSB_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeGetLSB(node);
}
PINVOKEABLE int CSharp_ScrCarbonDB_nodeGetBitSize_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeGetBitSize(node);
}
PINVOKEABLE int CSharp_ScrCarbonDB_nodeGetSourceLine_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeGetSourceLine(node);
}
PINVOKEABLE int CSharp_ScrCarbonDB_nodeGetArrayLeftBound_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeGetArrayLeftBound(node);
}
PINVOKEABLE int CSharp_ScrCarbonDB_nodeGetArrayRightBound_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeGetArrayRightBound(node);
}
PINVOKEABLE int CSharp_ScrCarbonDB_nodeGetArrayDims_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeGetArrayDims(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsContainedByComposite_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsContainedByComposite(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsStruct_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsStruct(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsArray_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsArray(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsEnum_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsEnum(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeCanBeCarbonNet_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeCanBeCarbonNet(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsScalar_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsScalar(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsTristate_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsTristate(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsVector_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsVector(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsConstant_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsConstant(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIs2DArray_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIs2DArray(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsClkTree_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsClkTree(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsAsync_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsAsync(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsAsyncOutput_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsAsyncOutput(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsAsyncDeposit_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsAsyncDeposit(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsAsyncPosReset_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsAsyncPosReset(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsAsyncNegReset_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsAsyncNegReset(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsForcible_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsForcible(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsDepositable_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsDepositable(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsScDepositable_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsScDepositable(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsObservable_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsObservable(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsVisible_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsVisible(node);
}
PINVOKEABLE bool CSharp_ScrCarbonDB_nodeIsTied_1_3(void* thisObj, CarbonDBNode* node)
{
    ScrCarbonDB* obj = (ScrCarbonDB*)(thisObj);
    return obj->nodeIsTied(node);
}
