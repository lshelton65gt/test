using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class componentPortDirectionTypeEnum
  {
    public enum componentPortDirectionType
    {
      IN = 0,
      OUT = 1,
      INOUT = 2,
      PHANTOM = 3
    }
  }
}
