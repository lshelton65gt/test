PINVOKEABLE void CSharp_ScrProject_deleteLater_0_7(void* thisObj)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CarbonOptions* CSharp_ScrProject_getOptions_0_3(void* thisObj)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    return obj->getOptions();
}
PINVOKEABLE CarbonOptions* CSharp_ScrProject_getCompilerOptions_0_3(void* thisObj)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    return obj->getCompilerOptions();
}
PINVOKEABLE char* CSharp_ScrProject_getHashValue_1_1(void* thisObj, const char* pathName)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    return EmbeddedMono::AllocString(obj->getHashValue(pathName));
}
PINVOKEABLE char* CSharp_ScrProject_getDesignFileName_1_1(void* thisObj, const char* fileExtension)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    return EmbeddedMono::AllocString(obj->getDesignFileName(fileExtension));
}
PINVOKEABLE char* CSharp_ScrProject_getBaseName_0_3(void* thisObj)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    return EmbeddedMono::AllocString(obj->getBaseName());
}
PINVOKEABLE unsigned int CSharp_ScrProject_numComponents_0_3(void* thisObj)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    return obj->numComponents();
}
PINVOKEABLE void CSharp_ScrProject_addSourceFile_3_3(void* thisObj, const char* fileName, const char* folderName, bool libraryFile)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    obj->addSourceFile(fileName, folderName, libraryFile);
}
PINVOKEABLE void CSharp_ScrProject_addSourceFile_2_1(void* thisObj, const char* fileName, const char* folderName)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    obj->addSourceFile(fileName, folderName);
}
PINVOKEABLE void CSharp_ScrProject_save_0_3(void* thisObj)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    obj->save();
}
PINVOKEABLE QObject* CSharp_ScrProject_createComponent_1_53(void* thisObj, ProjectEnum::ComponentType type)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    return obj->createComponent(type);
}
PINVOKEABLE bool CSharp_ScrProject_deleteComponent_1_53(void* thisObj, ProjectEnum::ComponentType type)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    return obj->deleteComponent(type);
}
PINVOKEABLE QObject* CSharp_ScrProject_getComponent_1_19(void* thisObj, unsigned int index)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    return obj->getComponent(index);
}
PINVOKEABLE char* CSharp_ScrProject_projectDirectory_0_3(void* thisObj)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    return EmbeddedMono::AllocString(obj->projectDirectory());
}
PINVOKEABLE char* CSharp_ScrProject_outputDirectory_0_3(void* thisObj)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    return EmbeddedMono::AllocString(obj->outputDirectory());
}
PINVOKEABLE XDesignHierarchy* CSharp_ScrProject_designHierarchy_0_3(void* thisObj)
{
    ScrProject* obj = (ScrProject*)(thisObj);
    return obj->designHierarchy();
}
