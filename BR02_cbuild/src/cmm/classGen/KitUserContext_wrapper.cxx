PINVOKEABLE void CSharp_KitUserContext_deleteLater_0_7(void* thisObj)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE ContextEnums::ModelTarget CSharp_KitUserContext_getModelTarget_0_7(void* thisObj)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return obj->getModelTarget();
}
PINVOKEABLE ContextEnums::ModelSymbols CSharp_KitUserContext_getModelSymbols_0_3(void* thisObj)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return obj->getModelSymbols();
}
PINVOKEABLE char* CSharp_KitUserContext_getComponentName_0_3(void* thisObj)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return EmbeddedMono::AllocString(obj->getComponentName());
}
PINVOKEABLE char* CSharp_KitUserContext_getLibraryName_0_3(void* thisObj)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return EmbeddedMono::AllocString(obj->getLibraryName());
}
PINVOKEABLE void CSharp_KitUserContext_setStatusMessage_1_1(void* thisObj, const char* msg)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    obj->setStatusMessage(msg);
}
PINVOKEABLE void CSharp_KitUserContext_setProgress_1_7(void* thisObj, int value)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    obj->setProgress(value);
}
PINVOKEABLE void CSharp_KitUserContext_setProgressRange_2_7(void* thisObj, int low, int high)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    obj->setProgressRange(low, high);
}
PINVOKEABLE double CSharp_KitUserContext_getDouble_7_7(void* thisObj, const char* questionId, const char* title, const char* label, double value, double minValue, double maxValue, int decimals)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return obj->getDouble(questionId, title, label, value, minValue, maxValue, decimals);
}
PINVOKEABLE double CSharp_KitUserContext_getDouble_6_5(void* thisObj, const char* questionId, const char* title, const char* label, double value, double minValue, double maxValue)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return obj->getDouble(questionId, title, label, value, minValue, maxValue);
}
PINVOKEABLE double CSharp_KitUserContext_getDouble_5_5(void* thisObj, const char* questionId, const char* title, const char* label, double value, double minValue)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return obj->getDouble(questionId, title, label, value, minValue);
}
PINVOKEABLE double CSharp_KitUserContext_getDouble_4_5(void* thisObj, const char* questionId, const char* title, const char* label, double value)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return obj->getDouble(questionId, title, label, value);
}
PINVOKEABLE double CSharp_KitUserContext_getDouble_3_1(void* thisObj, const char* questionId, const char* title, const char* label)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return obj->getDouble(questionId, title, label);
}
PINVOKEABLE int CSharp_KitUserContext_getInteger_7_7(void* thisObj, const char* questionId, const char* title, const char* label, int value, int minValue, int maxValue, int step)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return obj->getInteger(questionId, title, label, value, minValue, maxValue, step);
}
PINVOKEABLE int CSharp_KitUserContext_getInteger_6_7(void* thisObj, const char* questionId, const char* title, const char* label, int value, int minValue, int maxValue)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return obj->getInteger(questionId, title, label, value, minValue, maxValue);
}
PINVOKEABLE int CSharp_KitUserContext_getInteger_5_7(void* thisObj, const char* questionId, const char* title, const char* label, int value, int minValue)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return obj->getInteger(questionId, title, label, value, minValue);
}
PINVOKEABLE int CSharp_KitUserContext_getInteger_4_7(void* thisObj, const char* questionId, const char* title, const char* label, int value)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return obj->getInteger(questionId, title, label, value);
}
PINVOKEABLE int CSharp_KitUserContext_getInteger_3_1(void* thisObj, const char* questionId, const char* title, const char* label)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return obj->getInteger(questionId, title, label);
}
PINVOKEABLE char* CSharp_KitUserContext_getText_3_1(void* thisObj, const char* questionId, const char* title, const char* label)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return EmbeddedMono::AllocString(obj->getText(questionId, title, label));
}
PINVOKEABLE char* CSharp_KitUserContext_getOpenFileName_5_1(void* thisObj, const char* questionId, const char* caption, const char* initialDir, const char* filter, const char* selectedFilter)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return EmbeddedMono::AllocString(obj->getOpenFileName(questionId, caption, initialDir, filter, selectedFilter));
}
PINVOKEABLE QScriptValue CSharp_KitUserContext_getOpenFileNames_5_1(void* thisObj, const char* questionId, const char* caption, const char* initialDir, const char* filter, const char* selectedFilter)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return obj->getOpenFileNames(questionId, caption, initialDir, filter, selectedFilter);
}
PINVOKEABLE char* CSharp_KitUserContext_getExistingDirectory_3_1(void* thisObj, const char* questionId, const char* caption, const char* dir)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return EmbeddedMono::AllocString(obj->getExistingDirectory(questionId, caption, dir));
}
PINVOKEABLE char* CSharp_KitUserContext_expandName_1_1(void* thisObj, const char* inputName)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return EmbeddedMono::AllocString(obj->expandName(inputName));
}
PINVOKEABLE void CSharp_KitUserContext_setVariable_2_1(void* thisObj, const char* varName, const char* value)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    obj->setVariable(varName, value);
}
PINVOKEABLE char* CSharp_KitUserContext_lookupVariable_1_1(void* thisObj, const char* varName)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return EmbeddedMono::AllocString(obj->lookupVariable(varName));
}
PINVOKEABLE KitManifest* CSharp_KitUserContext_getManifest_0_39(void* thisObj)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return obj->getManifest();
}
PINVOKEABLE bool CSharp_KitUserContext_getPlaybackMode_0_3(void* thisObj)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    return obj->getPlaybackMode();
}
PINVOKEABLE void CSharp_KitUserContext_reportError_1_1(void* thisObj, const char* errorMessage)
{
    KitUserContext* obj = (KitUserContext*)(thisObj);
    obj->reportError(errorMessage);
}
