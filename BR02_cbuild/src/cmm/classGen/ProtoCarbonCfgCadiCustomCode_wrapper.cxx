PINVOKEABLE void CSharp_ProtoCarbonCfgCadiCustomCode_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgCadiCustomCode* obj = (ProtoCarbonCfgCadiCustomCode*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CcfgEnum::CarbonCfgCustomCodePosition CSharp_ProtoCarbonCfgCadiCustomCode_getPosition_0_7(void* thisObj)
{
    ProtoCarbonCfgCadiCustomCode* obj = (ProtoCarbonCfgCadiCustomCode*)(thisObj);
    return obj->getPosition();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgCadiCustomCode_setPosition_1_3(void* thisObj, CcfgEnum::CarbonCfgCustomCodePosition position)
{
    ProtoCarbonCfgCadiCustomCode* obj = (ProtoCarbonCfgCadiCustomCode*)(thisObj);
    obj->setPosition(position);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgCadiCustomCode_getCode_0_3(void* thisObj)
{
    ProtoCarbonCfgCadiCustomCode* obj = (ProtoCarbonCfgCadiCustomCode*)(thisObj);
    return EmbeddedMono::AllocString(obj->getCode());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgCadiCustomCode_setCode_1_1(void* thisObj, const char* code)
{
    ProtoCarbonCfgCadiCustomCode* obj = (ProtoCarbonCfgCadiCustomCode*)(thisObj);
    obj->setCode(code);
}
PINVOKEABLE CcfgEnum::CarbonCfgCadiCustomCodeSection CSharp_ProtoCarbonCfgCadiCustomCode_getSection_0_7(void* thisObj)
{
    ProtoCarbonCfgCadiCustomCode* obj = (ProtoCarbonCfgCadiCustomCode*)(thisObj);
    return obj->getSection();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgCadiCustomCode_setSection_1_3(void* thisObj, CcfgEnum::CarbonCfgCadiCustomCodeSection section)
{
    ProtoCarbonCfgCadiCustomCode* obj = (ProtoCarbonCfgCadiCustomCode*)(thisObj);
    obj->setSection(section);
}
PINVOKEABLE ProtoCarbonCfgCustomCode* CSharp_ProtoCarbonCfgCadiCustomCode_UpCast(ProtoCarbonCfgCadiCustomCode* obj)
{
  return (ProtoCarbonCfgCustomCode*)(obj);
}
