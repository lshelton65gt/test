PINVOKEABLE void CSharp_ProtoCarbonCfgProcInfo_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgProcInfo_getIsProcessor_0_7(void* thisObj)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    return obj->getIsProcessor();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgProcInfo_setIsProcessor_1_3(void* thisObj, bool newVal)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    obj->setIsProcessor(newVal);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgProcInfo_getDebuggerName_0_3(void* thisObj)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    return EmbeddedMono::AllocString(obj->getDebuggerName());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgProcInfo_setDebuggerName_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    obj->setDebuggerName(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgProcInfo_getPipeStages_0_3(void* thisObj)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    return obj->getPipeStages();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgProcInfo_setPipeStages_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    obj->setPipeStages(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgProcInfo_getHwThreads_0_3(void* thisObj)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    return obj->getHwThreads();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgProcInfo_setHwThreads_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    obj->setHwThreads(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgProcInfo_getNumProcessorOptions_0_3(void* thisObj)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    return obj->getNumProcessorOptions();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgProcInfo_getProcessorOption_1_19(void* thisObj, unsigned int i)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    return EmbeddedMono::AllocString(obj->getProcessorOption(i));
}
PINVOKEABLE void CSharp_ProtoCarbonCfgProcInfo_addProcessorOption_1_1(void* thisObj, const char* newOption)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    obj->addProcessorOption(newOption);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgProcInfo_getPCRegGroupName_0_3(void* thisObj)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    return EmbeddedMono::AllocString(obj->getPCRegGroupName());
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgProcInfo_getPCRegName_0_3(void* thisObj)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    return EmbeddedMono::AllocString(obj->getPCRegName());
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgProcInfo_getExtendedFeaturesRegGroupName_0_3(void* thisObj)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    return EmbeddedMono::AllocString(obj->getExtendedFeaturesRegGroupName());
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgProcInfo_getExtendedFeaturesRegName_0_3(void* thisObj)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    return EmbeddedMono::AllocString(obj->getExtendedFeaturesRegName());
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgProcInfo_getTargetName_0_3(void* thisObj)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    return EmbeddedMono::AllocString(obj->getTargetName());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgProcInfo_setPCRegName_2_1(void* thisObj, const char* newGroupName, const char* newRegName)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    obj->setPCRegName(newGroupName, newRegName);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgProcInfo_setExtendedFeaturesRegName_2_1(void* thisObj, const char* newGroupName, const char* newRegName)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    obj->setExtendedFeaturesRegName(newGroupName, newRegName);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgProcInfo_setTargetName_1_1(void* thisObj, const char* newTargetName)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    obj->setTargetName(newTargetName);
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgProcInfo_getDebuggablePoint_0_7(void* thisObj)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    return obj->getDebuggablePoint();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgProcInfo_setDebuggablePoint_1_3(void* thisObj, bool newVal)
{
    ProtoCarbonCfgProcInfo* obj = (ProtoCarbonCfgProcInfo*)(thisObj);
    obj->setDebuggablePoint(newVal);
}
