using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgTemplate : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgTemplate(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgTemplate obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgTemplate()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgTemplate_getNumVariables_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgTemplate_getNumVariables_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgTemplate_getVariable_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgTemplate_getVariable_1_19(HandleRef thisObj, uint index);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgTemplate_getValue_1_1")]
    private extern static IntPtr CSharp_ProtoCarbonCfgTemplate_getValue_1_1(HandleRef thisObj, string variable);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgTemplate_getErrMsg_0_35")]
    private extern static IntPtr CSharp_ProtoCarbonCfgTemplate_getErrMsg_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgTemplate_addVariable_2_1")]
    private extern static bool CSharp_ProtoCarbonCfgTemplate_addVariable_2_1(HandleRef thisObj, string variable, string value);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgTemplate_clear_0_39")]
    private extern static void CSharp_ProtoCarbonCfgTemplate_clear_0_39(HandleRef thisObj);
    #endregion DllImports
    #region Properties
    public uint NumVariables
    {
      get
      {
        return getNumVariables();
      }
    }
    #endregion Properties
    #region Methods
    public uint getNumVariables()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgTemplate object");
      return CSharp_ProtoCarbonCfgTemplate_getNumVariables_0_3(objCPtr);
    }

    public string getVariable(uint index)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgTemplate object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgTemplate_getVariable_1_19(objCPtr, index);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getValue(string variable)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgTemplate object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgTemplate_getValue_1_1(objCPtr, variable);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getErrMsg()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgTemplate object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgTemplate_getErrMsg_0_35(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public bool addVariable(string variable, string value)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgTemplate object");
      return CSharp_ProtoCarbonCfgTemplate_addVariable_2_1(objCPtr, variable, value);
    }

    public void clear()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgTemplate object");
      CSharp_ProtoCarbonCfgTemplate_clear_0_39(objCPtr);
    }

    #endregion Methods

  }

}
