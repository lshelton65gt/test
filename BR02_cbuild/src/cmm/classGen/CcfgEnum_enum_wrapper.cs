using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CcfgEnum
  {
    public enum CarbonCfgElementGenerationType
    {
      User = 0,
      Generated = 1
    }
    public enum CarbonCfgWaveType
    {
      WaveNone = 0,
      WaveVCD = 1,
      WaveFSDB = 2
    }
    public enum CarbonCfgMemLocType
    {
      MemLocRTL = 0,
      MemLocPort = 1,
      MemLocUser = 2
    }
    public enum CarbonCfgReadmemType
    {
      Readmemh = 0,
      Readmemb = 1
    }
    public enum CarbonCfgMemInitType
    {
      MemInitNone = 0,
      MemInitProgPreload = 1,
      MemInitReadmem = 2
    }
    public enum CarbonCfgRegisterLocKind
    {
      RegLocConstant = 0,
      RegLocRegister = 1,
      RegLocArray = 2,
      RegLocUser = 3
    }
    public enum CarbonCfgRegAccessType
    {
      RW = 0,
      RO = 1,
      WO = 2
    }
    public enum CarbonCfgRadix
    {
      BooleanActiveHigh = 0,
      BooleanActiveLow = 1,
      Hex = 2,
      SignedInt = 3,
      UnsignedInt = 4,
      Float = 5,
      String = 6
    }
    public enum CarbonCfgRTLPortType
    {
      Input = 0,
      Output = 1,
      InOut = 2
    }
    public enum CarbonCfgRTLConnectionType
    {
      ClockGen = 0,
      ResetGen = 1,
      Tie = 2,
      TieParam = 3,
      ESLPort = 4,
      XtorConn = 5
    }
    public enum CarbonCfgStatus
    {
      Success = 0,
      Failure = 1,
      Inconsistent = 2
    }
    public enum CarbonCfgMode
    {
      Undefined = 0,
      Arm = 1,
      CoWare = 2,
      SystemC = 3
    }
    public enum CarbonCfgParamDataType
    {
      XtorBool = 0,
      XtorUInt32 = 1,
      XtorDouble = 2,
      XtorString = 3,
      XtorUInt64 = 4
    }
    public enum CarbonCfgParamFlag
    {
      Runtime = 0,
      Compile = 1,
      Both = 2,
      Init = 3
    }
    public enum CarbonCfgESLPortType
    {
      ESLUndefined = 0,
      ESLInput = 1,
      ESLOutput = 2,
      ESLInout = 3
    }
    public enum CarbonCfgESLPortMode
    {
      ESLPortUndefined = 0,
      ESLPortControl = 1,
      ESLPortClock = 2,
      ESLPortReset = 3
    }
    public enum CarbonCfgCustomCodePosition
    {
      Pre = 0,
      Post = 1
    }
    public enum CarbonCfgCompCustomCodeSection
    {
      CompCppInclude = 0,
      CompHInclude = 1,
      CompClass = 2,
      CompConstructor = 3,
      CompDestructor = 4,
      CompCommunicate = 5,
      CompInterconnect = 6,
      CompUpdate = 7,
      CompInit = 8,
      CompReset = 9,
      CompTerminate = 10,
      CompSetParameter = 11,
      CompGetParameter = 12,
      CompGetProperty = 13,
      CompDebugTransaction = 14,
      CompDebugAccess = 15,
      ProcStopAtDebuggablePoint = 17,
      ProcCanStop = 16,
      CompSaveData = 18,
      CompRestoreData = 19,
      CompGetCB = 20,
      CompSaveStreamType = 21,
      CompSaveDataArch = 22,
      CompRestoreDataArch = 23,
      CompSaveDataBinary = 24,
      CompRestoreDataBinary = 25,
      CompLoadFile = 26,
      CompStopAtDebuggablePoint = 27,
      CompCanStop = 28,
      CompRegisterDebugAccessCB = 29,
      CompRetrieveDebugAccessIF = 30,
      CompDebugMemRead = 31,
      CompDebugMemWrite = 32
    }
    public enum CarbonCfgRegCustomCodeSection
    {
      RegClass = 0,
      RegConstructor = 1,
      RegDestructor = 2,
      RegRead = 3,
      RegWrite = 4,
      RegReset = 5
    }
    public enum CarbonCfgCadiCustomCodeSection
    {
      CadiCppInclude = 0,
      CadiHInclude = 1,
      CadiClass = 2,
      CadiConstructor = 3,
      CadiDestructor = 4,
      CadiCADIRegGetGroups = 5,
      CadiCADIRegGetMap = 6,
      CadiCADIRegRead = 7,
      CadiCADIRegWrite = 8,
      CadiCADIGetPC = 9,
      CadiCADIGetDisassembler = 10,
      CadiCADIXfaceBypass = 11,
      CadiCADIMemGetSpaces = 12,
      CadiCADIMemGetBlocks = 13,
      CadiCADIMemWrite = 14,
      CadiCADIMemRead = 15,
      CadiCADIGetCommitedPCs = 16
    }
    public enum CarbonCfgMemoryBlockCustomCodeSection
    {
      MemoryBlockClass = 0,
      MemoryBlockConstructor = 1,
      MemoryBlockDestructor = 2,
      MemoryBlockRead = 3,
      MemoryBlockWrite = 4
    }
    public enum CarbonCfgMemoryCustomCodeSection
    {
      MemoryClass = 0,
      MemoryConstructor = 1,
      MemoryDestructor = 2,
      MemoryRead = 3,
      MemoryWrite = 4
    }
  }
}
