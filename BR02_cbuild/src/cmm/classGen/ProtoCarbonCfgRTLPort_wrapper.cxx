PINVOKEABLE void CSharp_ProtoCarbonCfgRTLPort_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CarbonCfgRTLPort* CSharp_ProtoCarbonCfgRTLPort_pObj_0_7(void* thisObj)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    return obj->pObj();
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgRTLPort_isDisconnected_0_7(void* thisObj)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    return obj->isDisconnected();
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgRTLPort_isConnected_0_7(void* thisObj)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    return obj->isConnected();
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgRTLPort_isResetGen_0_7(void* thisObj)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    return obj->isResetGen();
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgRTLPort_isClockGen_0_7(void* thisObj)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    return obj->isClockGen();
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgRTLPort_isTied_0_7(void* thisObj)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    return obj->isTied();
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgRTLPort_isTiedToParam_0_3(void* thisObj)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    return obj->isTiedToParam();
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgRTLPort_isConnectedToXtor_0_7(void* thisObj)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    return obj->isConnectedToXtor();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRTLPort_connectClockGen_1_3(void* thisObj, CarbonCfgClockGen* conn)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    obj->connectClockGen(conn);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRTLPort_connectResetGen_1_3(void* thisObj, CarbonCfgResetGen* conn)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    obj->connectResetGen(conn);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRTLPort_connectTie_1_3(void* thisObj, CarbonCfgTie* conn)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    obj->connectTie(conn);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRTLPort_connectTieParam_1_3(void* thisObj, CarbonCfgTieParam* conn)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    obj->connectTieParam(conn);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRTLPort_connectESLPort_1_3(void* thisObj, CarbonCfgESLPort* conn)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    obj->connectESLPort(conn);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRTLPort_connectXtorConn_1_3(void* thisObj, CarbonCfgXtorConn* conn)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    obj->connectXtorConn(conn);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRTLPort_connectRTLConn_1_3(void* thisObj, CarbonCfgRTLConnection* conn)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    obj->connectRTLConn(conn);
}
PINVOKEABLE CcfgEnum::CarbonCfgRTLPortType CSharp_ProtoCarbonCfgRTLPort_portType_0_35(void* thisObj)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    return obj->portType();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgRTLPort_name_0_3(void* thisObj)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    return EmbeddedMono::AllocString(obj->name());
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRTLPort_width_0_7(void* thisObj)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    return obj->width();
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRTLPort_numConnections_0_3(void* thisObj)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    return obj->numConnections();
}
PINVOKEABLE CarbonCfgRTLConnection* CSharp_ProtoCarbonCfgRTLPort_getConnection_1_19(void* thisObj, unsigned int i)
{
    ProtoCarbonCfgRTLPort* obj = (ProtoCarbonCfgRTLPort*)(thisObj);
    return obj->getConnection(i);
}
