using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgMemoryBlock : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgMemoryBlock(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgMemoryBlock obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgMemoryBlock()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_getName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemoryBlock_getName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_setName_1_1")]
    private extern static void CSharp_ProtoCarbonCfgMemoryBlock_setName_1_1(HandleRef thisObj, string _name);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_getBase_0_35")]
    private extern static ulong CSharp_ProtoCarbonCfgMemoryBlock_getBase_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_setBase_1_7")]
    private extern static void CSharp_ProtoCarbonCfgMemoryBlock_setBase_1_7(HandleRef thisObj, ulong newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_getSize_0_3")]
    private extern static ulong CSharp_ProtoCarbonCfgMemoryBlock_getSize_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_setSize_1_7")]
    private extern static void CSharp_ProtoCarbonCfgMemoryBlock_setSize_1_7(HandleRef thisObj, ulong newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_getWidth_0_7")]
    private extern static uint CSharp_ProtoCarbonCfgMemoryBlock_getWidth_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_addLocRTL_2_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemoryBlock_addLocRTL_2_19(HandleRef thisObj, string path, uint width);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_addLocRTL_1_1")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemoryBlock_addLocRTL_1_1(HandleRef thisObj, string path);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_addLocRTL_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemoryBlock_addLocRTL_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_addLocPort_1_1")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemoryBlock_addLocPort_1_1(HandleRef thisObj, string port_name);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_addLocUser_1_1")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemoryBlock_addLocUser_1_1(HandleRef thisObj, string _name);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_removeLocMemoryLoc_1_3")]
    private extern static void CSharp_ProtoCarbonCfgMemoryBlock_removeLocMemoryLoc_1_3(HandleRef thisObj, IntPtr loc);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_removeLocPort_1_3")]
    private extern static void CSharp_ProtoCarbonCfgMemoryBlock_removeLocPort_1_3(HandleRef thisObj, IntPtr loc);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_removeLocRTL_1_3")]
    private extern static void CSharp_ProtoCarbonCfgMemoryBlock_removeLocRTL_1_3(HandleRef thisObj, IntPtr loc);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_removeLocUser_1_3")]
    private extern static void CSharp_ProtoCarbonCfgMemoryBlock_removeLocUser_1_3(HandleRef thisObj, IntPtr loc);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_numLocs_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgMemoryBlock_numLocs_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_getLoc_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemoryBlock_getLoc_1_19(HandleRef thisObj, uint index);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_numCustomCodes_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgMemoryBlock_numCustomCodes_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_addCustomCode_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemoryBlock_addCustomCode_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_getCustomCode_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemoryBlock_getCustomCode_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryBlock_removeCustomCode_1_3")]
    private extern static void CSharp_ProtoCarbonCfgMemoryBlock_removeCustomCode_1_3(HandleRef thisObj, IntPtr customCode);
    #endregion DllImports
    #region Properties
    public string Name
    {
      set
      {
        setName(value);
      }
      get
      {
        return getName();
      }
    }
    public ulong Base
    {
      set
      {
        setBase(value);
      }
      get
      {
        return getBase();
      }
    }
    public ulong Size
    {
      set
      {
        setSize(value);
      }
      get
      {
        return getSize();
      }
    }
    public uint Width
    {
      get
      {
        return getWidth();
      }
    }
    public uint NumLocs
    {
      get
      {
        return numLocs();
      }
    }
    public uint NumCustomCodes
    {
      get
      {
        return numCustomCodes();
      }
    }
    #endregion Properties
    #region Methods
    public string getName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgMemoryBlock_getName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setName(string _name)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      CSharp_ProtoCarbonCfgMemoryBlock_setName_1_1(objCPtr, _name);
    }

    public ulong getBase()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      return CSharp_ProtoCarbonCfgMemoryBlock_getBase_0_35(objCPtr);
    }

    public void setBase(ulong newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      CSharp_ProtoCarbonCfgMemoryBlock_setBase_1_7(objCPtr, newVal);
    }

    public ulong getSize()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      return CSharp_ProtoCarbonCfgMemoryBlock_getSize_0_3(objCPtr);
    }

    public void setSize(ulong newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      CSharp_ProtoCarbonCfgMemoryBlock_setSize_1_7(objCPtr, newVal);
    }

    public uint getWidth()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      return CSharp_ProtoCarbonCfgMemoryBlock_getWidth_0_7(objCPtr);
    }

    public CarbonCfgMemoryLocRTL addLocRTL(string path, uint width)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgMemoryBlock_addLocRTL_2_19(objCPtr, path, width);
      CarbonCfgMemoryLocRTL ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemoryLocRTL(cPtr, false);
      return ret;
    }

    public CarbonCfgMemoryLocRTL addLocRTL(string path)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgMemoryBlock_addLocRTL_1_1(objCPtr, path);
      CarbonCfgMemoryLocRTL ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemoryLocRTL(cPtr, false);
      return ret;
    }

    public CarbonCfgMemoryLocRTL addLocRTL()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgMemoryBlock_addLocRTL_0_7(objCPtr);
      CarbonCfgMemoryLocRTL ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemoryLocRTL(cPtr, false);
      return ret;
    }

    public CarbonCfgMemoryLocPort addLocPort(string port_name)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgMemoryBlock_addLocPort_1_1(objCPtr, port_name);
      CarbonCfgMemoryLocPort ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemoryLocPort(cPtr, false);
      return ret;
    }

    public CarbonCfgMemoryLocUser addLocUser(string _name)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgMemoryBlock_addLocUser_1_1(objCPtr, _name);
      CarbonCfgMemoryLocUser ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemoryLocUser(cPtr, false);
      return ret;
    }

    public void removeLocMemoryLoc(CarbonCfgMemoryLoc loc)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      CSharp_ProtoCarbonCfgMemoryBlock_removeLocMemoryLoc_1_3(objCPtr, CarbonCfgMemoryLoc.getCPtr(loc).Handle);
    }

    public void removeLocPort(CarbonCfgMemoryLocPort loc)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      CSharp_ProtoCarbonCfgMemoryBlock_removeLocPort_1_3(objCPtr, CarbonCfgMemoryLocPort.getCPtr(loc).Handle);
    }

    public void removeLocRTL(CarbonCfgMemoryLocRTL loc)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      CSharp_ProtoCarbonCfgMemoryBlock_removeLocRTL_1_3(objCPtr, CarbonCfgMemoryLocRTL.getCPtr(loc).Handle);
    }

    public void removeLocUser(CarbonCfgMemoryLocUser loc)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      CSharp_ProtoCarbonCfgMemoryBlock_removeLocUser_1_3(objCPtr, CarbonCfgMemoryLocUser.getCPtr(loc).Handle);
    }

    public uint numLocs()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      return CSharp_ProtoCarbonCfgMemoryBlock_numLocs_0_3(objCPtr);
    }

    public CarbonCfgMemoryLoc getLoc(uint index)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgMemoryBlock_getLoc_1_19(objCPtr, index);
      CarbonCfgMemoryLoc ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemoryLoc(cPtr, false);
      return ret;
    }

    public uint numCustomCodes()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      return CSharp_ProtoCarbonCfgMemoryBlock_numCustomCodes_0_3(objCPtr);
    }

    public CarbonCfgMemoryBlockCustomCode addCustomCode()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgMemoryBlock_addCustomCode_0_3(objCPtr);
      CarbonCfgMemoryBlockCustomCode ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemoryBlockCustomCode(cPtr, false);
      return ret;
    }

    public CarbonCfgMemoryBlockCustomCode getCustomCode(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgMemoryBlock_getCustomCode_1_19(objCPtr, i);
      CarbonCfgMemoryBlockCustomCode ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemoryBlockCustomCode(cPtr, false);
      return ret;
    }

    public void removeCustomCode(CarbonCfgMemoryBlockCustomCode customCode)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryBlock object");
      CSharp_ProtoCarbonCfgMemoryBlock_removeCustomCode_1_3(objCPtr, CarbonCfgMemoryBlockCustomCode.getCPtr(customCode).Handle);
    }

    #endregion Methods

  }

}
