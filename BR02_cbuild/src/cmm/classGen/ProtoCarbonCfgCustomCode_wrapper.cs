using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgCustomCode : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgCustomCode(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgCustomCode obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgCustomCode()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCustomCode_getPosition_0_7")]
    private extern static int CSharp_ProtoCarbonCfgCustomCode_getPosition_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCustomCode_setPosition_1_3")]
    private extern static void CSharp_ProtoCarbonCfgCustomCode_setPosition_1_3(HandleRef thisObj, int position);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCustomCode_getCode_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgCustomCode_getCode_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCustomCode_setCode_1_1")]
    private extern static void CSharp_ProtoCarbonCfgCustomCode_setCode_1_1(HandleRef thisObj, string code);
    #endregion DllImports
    #region Properties
    public CcfgEnum.CarbonCfgCustomCodePosition Position
    {
      set
      {
        setPosition(value);
      }
      get
      {
        return getPosition();
      }
    }
    public string Code
    {
      set
      {
        setCode(value);
      }
      get
      {
        return getCode();
      }
    }
    #endregion Properties
    #region Methods
    public CcfgEnum.CarbonCfgCustomCodePosition getPosition()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgCustomCode object");
      return (CcfgEnum.CarbonCfgCustomCodePosition)CSharp_ProtoCarbonCfgCustomCode_getPosition_0_7(objCPtr);
    }

    public void setPosition(CcfgEnum.CarbonCfgCustomCodePosition position)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgCustomCode object");
      CSharp_ProtoCarbonCfgCustomCode_setPosition_1_3(objCPtr, (int)position);
    }

    public string getCode()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgCustomCode object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgCustomCode_getCode_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setCode(string code)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgCustomCode object");
      CSharp_ProtoCarbonCfgCustomCode_setCode_1_1(objCPtr, code);
    }

    #endregion Methods

  }

}
