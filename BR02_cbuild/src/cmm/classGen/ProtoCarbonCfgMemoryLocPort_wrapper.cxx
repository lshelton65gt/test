PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocPort_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CcfgEnum::CarbonCfgMemLocType CSharp_ProtoCarbonCfgMemoryLocPort_getType_0_35(void* thisObj)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    return obj->getType();
}
PINVOKEABLE unsigned long long CSharp_ProtoCarbonCfgMemoryLocPort_getDisplayStartWordOffset_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    return obj->getDisplayStartWordOffset();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocPort_setDisplayStartWordOffset_1_7(void* thisObj, unsigned long long newVal)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    obj->setDisplayStartWordOffset(newVal);
}
PINVOKEABLE unsigned long long CSharp_ProtoCarbonCfgMemoryLocPort_getDisplayEndWordOffset_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    return obj->getDisplayEndWordOffset();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocPort_setDisplayEndWordOffset_1_7(void* thisObj, unsigned long long newVal)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    obj->setDisplayEndWordOffset(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemoryLocPort_getDisplayMsb_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    return obj->getDisplayMsb();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocPort_setDisplayMsb_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    obj->setDisplayMsb(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemoryLocPort_getDisplayLsb_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    return obj->getDisplayLsb();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocPort_setDisplayLsb_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    obj->setDisplayLsb(newVal);
}
PINVOKEABLE CarbonCfgMemoryLocRTL* CSharp_ProtoCarbonCfgMemoryLocPort_castRTL_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    return obj->castRTL();
}
PINVOKEABLE CarbonCfgMemoryLocPort* CSharp_ProtoCarbonCfgMemoryLocPort_castPort_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    return obj->castPort();
}
PINVOKEABLE CarbonCfgMemoryLocUser* CSharp_ProtoCarbonCfgMemoryLocPort_castUser_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    return obj->castUser();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgMemoryLocPort_getPortName_0_3(void* thisObj)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    return EmbeddedMono::AllocString(obj->getPortName());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocPort_setPortName_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    obj->setPortName(newVal);
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgMemoryLocPort_getFixedAddress_0_35(void* thisObj)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    return obj->getFixedAddress();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocPort_setFixedAddress_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgMemoryLocPort* obj = (ProtoCarbonCfgMemoryLocPort*)(thisObj);
    obj->setFixedAddress(newVal);
}
PINVOKEABLE ProtoCarbonCfgMemoryLoc* CSharp_ProtoCarbonCfgMemoryLocPort_UpCast(ProtoCarbonCfgMemoryLocPort* obj)
{
  return (ProtoCarbonCfgMemoryLoc*)(obj);
}
