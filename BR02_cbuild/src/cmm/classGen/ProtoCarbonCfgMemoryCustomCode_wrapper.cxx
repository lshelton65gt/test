PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryCustomCode_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryCustomCode* obj = (ProtoCarbonCfgMemoryCustomCode*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CcfgEnum::CarbonCfgCustomCodePosition CSharp_ProtoCarbonCfgMemoryCustomCode_getPosition_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryCustomCode* obj = (ProtoCarbonCfgMemoryCustomCode*)(thisObj);
    return obj->getPosition();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryCustomCode_setPosition_1_3(void* thisObj, CcfgEnum::CarbonCfgCustomCodePosition position)
{
    ProtoCarbonCfgMemoryCustomCode* obj = (ProtoCarbonCfgMemoryCustomCode*)(thisObj);
    obj->setPosition(position);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgMemoryCustomCode_getCode_0_3(void* thisObj)
{
    ProtoCarbonCfgMemoryCustomCode* obj = (ProtoCarbonCfgMemoryCustomCode*)(thisObj);
    return EmbeddedMono::AllocString(obj->getCode());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryCustomCode_setCode_1_1(void* thisObj, const char* code)
{
    ProtoCarbonCfgMemoryCustomCode* obj = (ProtoCarbonCfgMemoryCustomCode*)(thisObj);
    obj->setCode(code);
}
PINVOKEABLE CcfgEnum::CarbonCfgMemoryCustomCodeSection CSharp_ProtoCarbonCfgMemoryCustomCode_getSection_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryCustomCode* obj = (ProtoCarbonCfgMemoryCustomCode*)(thisObj);
    return obj->getSection();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryCustomCode_setSection_1_3(void* thisObj, CcfgEnum::CarbonCfgMemoryCustomCodeSection section)
{
    ProtoCarbonCfgMemoryCustomCode* obj = (ProtoCarbonCfgMemoryCustomCode*)(thisObj);
    obj->setSection(section);
}
PINVOKEABLE ProtoCarbonCfgCustomCode* CSharp_ProtoCarbonCfgMemoryCustomCode_UpCast(ProtoCarbonCfgMemoryCustomCode* obj)
{
  return (ProtoCarbonCfgCustomCode*)(obj);
}
