PINVOKEABLE void CSharp_ProtoCarbonCfgMemory_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CcfgFlags::CarbonCfgMemoryEditFlags CSharp_ProtoCarbonCfgMemory_GetEditFlags_0_3(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return obj->GetEditFlags();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemory_SetEditFlags_1_1(void* thisObj, CcfgFlags::CarbonCfgMemoryEditFlags newValue)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    obj->SetEditFlags(newValue);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemory_numMemoryBlocks_0_3(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return obj->numMemoryBlocks();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgMemory_getName_0_3(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return EmbeddedMono::AllocString(obj->getName());
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgMemory_getInitFile_0_3(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return EmbeddedMono::AllocString(obj->getInitFile());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemory_setInitFile_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    obj->setInitFile(newVal);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgMemory_getComment_0_7(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return EmbeddedMono::AllocString(obj->getComment());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemory_setComment_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    obj->setComment(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemory_getWidth_0_7(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return obj->getWidth();
}
PINVOKEABLE CcfgEnum::CarbonCfgReadmemType CSharp_ProtoCarbonCfgMemory_getReadmemType_0_35(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return obj->getReadmemType();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemory_setReadmemType_1_21(void* thisObj, CcfgEnum::CarbonCfgReadmemType newVal)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    obj->setReadmemType(newVal);
}
PINVOKEABLE CcfgEnum::CarbonCfgMemInitType CSharp_ProtoCarbonCfgMemory_getInitType_0_35(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return obj->getInitType();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemory_setInitType_1_53(void* thisObj, CcfgEnum::CarbonCfgMemInitType newVal)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    obj->setInitType(newVal);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgMemory_getSystemAddressESLPortName_0_3(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return EmbeddedMono::AllocString(obj->getSystemAddressESLPortName());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemory_setProgPreloadEslPort_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    obj->setProgPreloadEslPort(newVal);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgMemory_getDisassemblyName_0_3(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return EmbeddedMono::AllocString(obj->getDisassemblyName());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemory_setDisassemblyName_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    obj->setDisassemblyName(newVal);
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgMemory_getDisplayAtZero_0_3(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return obj->getDisplayAtZero();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemory_setDisplayAtZero_1_3(void* thisObj, bool newVal)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    obj->setDisplayAtZero(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemory_getMAU_0_35(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return obj->getMAU();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemory_setMAU_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    obj->setMAU(newVal);
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgMemory_getBigEndian_0_7(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return obj->getBigEndian();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemory_setBigEndian_1_3(void* thisObj, bool newVal)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    obj->setBigEndian(newVal);
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgMemory_getProgramMemory_0_3(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return obj->getProgramMemory();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemory_setProgramMemory_1_3(void* thisObj, bool newVal)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    obj->setProgramMemory(newVal);
}
PINVOKEABLE CarbonCfgMemoryBlock* CSharp_ProtoCarbonCfgMemory_addMemoryBlock_0_3(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return obj->addMemoryBlock();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemory_removeMemoryBlock_1_3(void* thisObj, CarbonCfgMemoryBlock* block)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    obj->removeMemoryBlock(block);
}
PINVOKEABLE CarbonCfgMemoryBlock* CSharp_ProtoCarbonCfgMemory_getMemoryBlock_1_19(void* thisObj, unsigned int index)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return obj->getMemoryBlock(index);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemory_numCustomCodes_0_3(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return obj->numCustomCodes();
}
PINVOKEABLE CarbonCfgMemoryCustomCode* CSharp_ProtoCarbonCfgMemory_addCustomCode_0_3(void* thisObj)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return obj->addCustomCode();
}
PINVOKEABLE CarbonCfgMemoryCustomCode* CSharp_ProtoCarbonCfgMemory_getCustomCode_1_19(void* thisObj, unsigned int i)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    return obj->getCustomCode(i);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemory_removeCustomCode_1_3(void* thisObj, CarbonCfgMemoryCustomCode* customCode)
{
    ProtoCarbonCfgMemory* obj = (ProtoCarbonCfgMemory*)(thisObj);
    obj->removeCustomCode(customCode);
}
