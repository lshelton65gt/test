using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgRegisterLocConstant : CarbonCfgRegisterLoc, IDisposable
  {
    private HandleRef objCPtr;
    internal CarbonCfgRegisterLocConstant(IntPtr cPtr, bool cMemoryOwn) : base(CSharp_ProtoCarbonCfgRegisterLocConstant_UpCast(cPtr), cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgRegisterLocConstant obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgRegisterLocConstant()
    {
      Dispose();
    }
    public override void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocConstant_getValue_0_3")]
    private extern static ulong CSharp_ProtoCarbonCfgRegisterLocConstant_getValue_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocConstant_setValue_1_7")]
    private extern static void CSharp_ProtoCarbonCfgRegisterLocConstant_setValue_1_7(HandleRef thisObj, ulong newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocConstant_UpCast")]
    private static extern IntPtr CSharp_ProtoCarbonCfgRegisterLocConstant_UpCast(IntPtr obj);
    #endregion DllImports
    #region Properties
    public ulong Value
    {
      set
      {
        setValue(value);
      }
      get
      {
        return getValue();
      }
    }
    #endregion Properties
    #region Methods
    public ulong getValue()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocConstant object");
      return CSharp_ProtoCarbonCfgRegisterLocConstant_getValue_0_3(objCPtr);
    }

    public void setValue(ulong newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocConstant object");
      CSharp_ProtoCarbonCfgRegisterLocConstant_setValue_1_7(objCPtr, newVal);
    }

    #endregion Methods

  }

}
