using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class Ccfg : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal Ccfg(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(Ccfg obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~Ccfg()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_GetEditFlags_0_3")]
    private extern static int CSharp_ScrCcfg_GetEditFlags_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_SetEditFlags_1_1")]
    private extern static void CSharp_ScrCcfg_SetEditFlags_1_1(HandleRef thisObj, int newValue);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getFilePath_0_7")]
    private extern static IntPtr CSharp_ScrCcfg_getFilePath_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setFilePath_1_1")]
    private extern static void CSharp_ScrCcfg_setFilePath_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getDescription_0_7")]
    private extern static IntPtr CSharp_ScrCcfg_getDescription_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setDescription_1_1")]
    private extern static void CSharp_ScrCcfg_setDescription_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getCxxFlags_0_3")]
    private extern static IntPtr CSharp_ScrCcfg_getCxxFlags_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setCxxFlags_1_1")]
    private extern static void CSharp_ScrCcfg_setCxxFlags_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getLinkFlags_0_3")]
    private extern static IntPtr CSharp_ScrCcfg_getLinkFlags_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setLinkFlags_1_1")]
    private extern static void CSharp_ScrCcfg_setLinkFlags_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getSourceFiles_0_3")]
    private extern static IntPtr CSharp_ScrCcfg_getSourceFiles_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setSourceFiles_1_1")]
    private extern static void CSharp_ScrCcfg_setSourceFiles_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getIncludeFiles_0_3")]
    private extern static IntPtr CSharp_ScrCcfg_getIncludeFiles_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setIncludeFiles_1_1")]
    private extern static void CSharp_ScrCcfg_setIncludeFiles_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getLibName_0_3")]
    private extern static IntPtr CSharp_ScrCcfg_getLibName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setLibName_1_1")]
    private extern static void CSharp_ScrCcfg_setLibName_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getCompName_0_3")]
    private extern static IntPtr CSharp_ScrCcfg_getCompName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setCompName_1_1")]
    private extern static void CSharp_ScrCcfg_setCompName_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getCompDisplayName_0_3")]
    private extern static IntPtr CSharp_ScrCcfg_getCompDisplayName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setCompDisplayName_1_1")]
    private extern static void CSharp_ScrCcfg_setCompDisplayName_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getTopModuleName_0_3")]
    private extern static IntPtr CSharp_ScrCcfg_getTopModuleName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setTopModuleName_1_1")]
    private extern static void CSharp_ScrCcfg_setTopModuleName_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getWaveFile_0_3")]
    private extern static IntPtr CSharp_ScrCcfg_getWaveFile_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setWaveFile_1_1")]
    private extern static void CSharp_ScrCcfg_setWaveFile_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getWaveType_0_35")]
    private extern static int CSharp_ScrCcfg_getWaveType_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setWaveType_1_21")]
    private extern static void CSharp_ScrCcfg_setWaveType_1_21(HandleRef thisObj, int wt);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getType_0_35")]
    private extern static IntPtr CSharp_ScrCcfg_getType_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setType_1_1")]
    private extern static void CSharp_ScrCcfg_setType_1_1(HandleRef thisObj, string name);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getVersion_0_7")]
    private extern static IntPtr CSharp_ScrCcfg_getVersion_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setVersion_1_1")]
    private extern static void CSharp_ScrCcfg_setVersion_1_1(HandleRef thisObj, string name);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getDocFile_0_3")]
    private extern static IntPtr CSharp_ScrCcfg_getDocFile_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setDocFile_1_1")]
    private extern static void CSharp_ScrCcfg_setDocFile_1_1(HandleRef thisObj, string name);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getLoadfileExtension_0_7")]
    private extern static IntPtr CSharp_ScrCcfg_getLoadfileExtension_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setLoadfileExtension_1_1")]
    private extern static void CSharp_ScrCcfg_setLoadfileExtension_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getUseStaticScheduling_0_3")]
    private extern static bool CSharp_ScrCcfg_getUseStaticScheduling_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setUseStaticScheduling_1_19")]
    private extern static void CSharp_ScrCcfg_setUseStaticScheduling_1_19(HandleRef thisObj, int val);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getLegacyMemories_0_3")]
    private extern static int CSharp_ScrCcfg_getLegacyMemories_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setLegacyMemories_1_19")]
    private extern static void CSharp_ScrCcfg_setLegacyMemories_1_19(HandleRef thisObj, int val);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_isStandAloneComp_0_7")]
    private extern static int CSharp_ScrCcfg_isStandAloneComp_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setIsStandAloneComp_1_19")]
    private extern static void CSharp_ScrCcfg_setIsStandAloneComp_1_19(HandleRef thisObj, int val);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_isSubComponentFullName_0_3")]
    private extern static int CSharp_ScrCcfg_isSubComponentFullName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setSubComponentFullName_1_19")]
    private extern static void CSharp_ScrCcfg_setSubComponentFullName_1_19(HandleRef thisObj, int val);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getNumSubComponents_0_3")]
    private extern static uint CSharp_ScrCcfg_getNumSubComponents_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getNumXtorInstances_0_3")]
    private extern static uint CSharp_ScrCcfg_getNumXtorInstances_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getNumESLPorts_0_3")]
    private extern static uint CSharp_ScrCcfg_getNumESLPorts_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getNumRTLPorts_0_3")]
    private extern static uint CSharp_ScrCcfg_getNumRTLPorts_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getNumParams_0_3")]
    private extern static uint CSharp_ScrCcfg_getNumParams_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getNumRegisters_0_35")]
    private extern static uint CSharp_ScrCcfg_getNumRegisters_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getNumGroups_0_3")]
    private extern static uint CSharp_ScrCcfg_getNumGroups_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getNumMemories_0_3")]
    private extern static uint CSharp_ScrCcfg_getNumMemories_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getNumCustomCodes_0_3")]
    private extern static uint CSharp_ScrCcfg_getNumCustomCodes_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_read_2_1")]
    private extern static int CSharp_ScrCcfg_read_2_1(HandleRef thisObj, int mode, string fileName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_readNoCheck_2_1")]
    private extern static int CSharp_ScrCcfg_readNoCheck_2_1(HandleRef thisObj, int mode, string fileName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_write_1_1")]
    private extern static int CSharp_ScrCcfg_write_1_1(HandleRef thisObj, string fileName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_write_0_3")]
    private extern static int CSharp_ScrCcfg_write_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getELFLoader_0_7")]
    private extern static IntPtr CSharp_ScrCcfg_getELFLoader_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getProcInfo_0_3")]
    private extern static IntPtr CSharp_ScrCcfg_getProcInfo_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_addSubComp_1_1")]
    private extern static IntPtr CSharp_ScrCcfg_addSubComp_1_1(HandleRef thisObj, string componentName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_removeSubComp_1_3")]
    private extern static void CSharp_ScrCcfg_removeSubComp_1_3(HandleRef thisObj, IntPtr comp);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_removeSubComps_0_3")]
    private extern static void CSharp_ScrCcfg_removeSubComps_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_removeRegisters_0_35")]
    private extern static void CSharp_ScrCcfg_removeRegisters_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_removeMemories_0_3")]
    private extern static void CSharp_ScrCcfg_removeMemories_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_removeCustomCodes_0_3")]
    private extern static void CSharp_ScrCcfg_removeCustomCodes_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_removeXtorInstances_0_3")]
    private extern static void CSharp_ScrCcfg_removeXtorInstances_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_removeParameters_0_35")]
    private extern static void CSharp_ScrCcfg_removeParameters_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_removeGroups_0_3")]
    private extern static void CSharp_ScrCcfg_removeGroups_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getSubComponent_1_19")]
    private extern static IntPtr CSharp_ScrCcfg_getSubComponent_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getUniqueRegName_1_1")]
    private extern static IntPtr CSharp_ScrCcfg_getUniqueRegName_1_1(HandleRef thisObj, string baseName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getUniqueGroupName_1_1")]
    private extern static IntPtr CSharp_ScrCcfg_getUniqueGroupName_1_1(HandleRef thisObj, string baseName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getUniqueESLName_1_1")]
    private extern static IntPtr CSharp_ScrCcfg_getUniqueESLName_1_1(HandleRef thisObj, string baseName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_numGroups_0_3")]
    private extern static uint CSharp_ScrCcfg_numGroups_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getGroup_1_19")]
    private extern static IntPtr CSharp_ScrCcfg_getGroup_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_findGroup_1_1")]
    private extern static IntPtr CSharp_ScrCcfg_findGroup_1_1(HandleRef thisObj, string groupName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_addGroup_1_1")]
    private extern static IntPtr CSharp_ScrCcfg_addGroup_1_1(HandleRef thisObj, string groupName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getXtorLib_0_39")]
    private extern static IntPtr CSharp_ScrCcfg_getXtorLib_0_39(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_findESLPort_1_1")]
    private extern static IntPtr CSharp_ScrCcfg_findESLPort_1_1(HandleRef thisObj, string portName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_findRTLPort_1_1")]
    private extern static IntPtr CSharp_ScrCcfg_findRTLPort_1_1(HandleRef thisObj, string portName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_addRegister_7_3")]
    private extern static IntPtr CSharp_ScrCcfg_addRegister_7_3(HandleRef thisObj, string debugName, IntPtr group, uint width, bool bigEndian, int radixVal, string commentStr, bool pcReg);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_addRegister_6_1")]
    private extern static IntPtr CSharp_ScrCcfg_addRegister_6_1(HandleRef thisObj, string debugName, IntPtr group, uint width, bool bigEndian, int radixVal, string commentStr);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_addCustomCode_0_3")]
    private extern static IntPtr CSharp_ScrCcfg_addCustomCode_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getCustomCode_1_19")]
    private extern static IntPtr CSharp_ScrCcfg_getCustomCode_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_removeCustomCode_1_3")]
    private extern static void CSharp_ScrCcfg_removeCustomCode_1_3(HandleRef thisObj, IntPtr customCode);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_addESLPort_5_5")]
    private extern static IntPtr CSharp_ScrCcfg_addESLPort_5_5(HandleRef thisObj, IntPtr rtlPort, string name, int ptype, string typeDef, int pmode);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_addParam_6_1")]
    private extern static IntPtr CSharp_ScrCcfg_addParam_6_1(HandleRef thisObj, string Name, int type, string Value, int scope, string Description, string EnumChoices);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_addParam_5_1")]
    private extern static IntPtr CSharp_ScrCcfg_addParam_5_1(HandleRef thisObj, string Name, int type, string Value, int scope, string Description);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_addXtor_2_3")]
    private extern static IntPtr CSharp_ScrCcfg_addXtor_2_3(HandleRef thisObj, string instName, IntPtr type);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getRegister_1_19")]
    private extern static IntPtr CSharp_ScrCcfg_getRegister_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getXtorInstance_1_19")]
    private extern static IntPtr CSharp_ScrCcfg_getXtorInstance_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getESLPort_1_19")]
    private extern static IntPtr CSharp_ScrCcfg_getESLPort_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getRTLPort_1_19")]
    private extern static IntPtr CSharp_ScrCcfg_getRTLPort_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getParam_1_19")]
    private extern static IntPtr CSharp_ScrCcfg_getParam_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_findXtor_1_1")]
    private extern static IntPtr CSharp_ScrCcfg_findXtor_1_1(HandleRef thisObj, string xtorName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_findXtorInstance_1_1")]
    private extern static IntPtr CSharp_ScrCcfg_findXtorInstance_1_1(HandleRef thisObj, string xtorName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_removeXtorInstance_1_3")]
    private extern static void CSharp_ScrCcfg_removeXtorInstance_1_3(HandleRef thisObj, IntPtr xtorInst);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_findRegister_1_1")]
    private extern static IntPtr CSharp_ScrCcfg_findRegister_1_1(HandleRef thisObj, string regName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_removeRegister_1_3")]
    private extern static void CSharp_ScrCcfg_removeRegister_1_3(HandleRef thisObj, IntPtr reg);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_removeMemory_1_3")]
    private extern static void CSharp_ScrCcfg_removeMemory_1_3(HandleRef thisObj, IntPtr mem);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_disconnect_1_3")]
    private extern static void CSharp_ScrCcfg_disconnect_1_3(HandleRef thisObj, IntPtr conn);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_addMemory_3_19")]
    private extern static IntPtr CSharp_ScrCcfg_addMemory_3_19(HandleRef thisObj, string name, uint width, uint maxAddr);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_numMemories_0_3")]
    private extern static uint CSharp_ScrCcfg_numMemories_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getMemory_1_19")]
    private extern static IntPtr CSharp_ScrCcfg_getMemory_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getCadi_0_3")]
    private extern static IntPtr CSharp_ScrCcfg_getCadi_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_cloneCustomCode_1_3")]
    private extern static IntPtr CSharp_ScrCcfg_cloneCustomCode_1_3(HandleRef thisObj, IntPtr cc);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_cloneMemory_1_3")]
    private extern static IntPtr CSharp_ScrCcfg_cloneMemory_1_3(HandleRef thisObj, IntPtr cc);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_cloneCadi_1_3")]
    private extern static void CSharp_ScrCcfg_cloneCadi_1_3(HandleRef thisObj, IntPtr cadi);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_cloneProcInfo_1_3")]
    private extern static void CSharp_ScrCcfg_cloneProcInfo_1_3(HandleRef thisObj, IntPtr procInfo);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_clone_2_3")]
    private extern static IntPtr CSharp_ScrCcfg_clone_2_3(HandleRef thisObj, IntPtr group, IntPtr cc);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setRequiresMkLibrary_1_3")]
    private extern static void CSharp_ScrCcfg_setRequiresMkLibrary_1_3(HandleRef thisObj, bool req);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getRequiresMkLibrary_0_3")]
    private extern static bool CSharp_ScrCcfg_getRequiresMkLibrary_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_setUseVersionedMkLibrary_1_3")]
    private extern static void CSharp_ScrCcfg_setUseVersionedMkLibrary_1_3(HandleRef thisObj, bool req);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCcfg_getUseVersionedMkLibrary_0_3")]
    private extern static bool CSharp_ScrCcfg_getUseVersionedMkLibrary_0_3(HandleRef thisObj);
    #endregion DllImports
    #region Properties
    public string FilePath
    {
      set
      {
        setFilePath(value);
      }
      get
      {
        return getFilePath();
      }
    }
    public uint NumSubComponents
    {
      get
      {
        return getNumSubComponents();
      }
    }
    public uint NumXtorInstances
    {
      get
      {
        return getNumXtorInstances();
      }
    }
    public uint NumESLPorts
    {
      get
      {
        return getNumESLPorts();
      }
    }
    public uint NumRTLPorts
    {
      get
      {
        return getNumRTLPorts();
      }
    }
    public uint NumParams
    {
      get
      {
        return getNumParams();
      }
    }
    public uint NumRegisters
    {
      get
      {
        return getNumRegisters();
      }
    }
    public uint NumGroups
    {
      get
      {
        return getNumGroups();
      }
    }
    public uint NumMemories
    {
      get
      {
        return getNumMemories();
      }
    }
    public uint NumCustomCodes
    {
      get
      {
        return getNumCustomCodes();
      }
    }
    public CarbonCfgXtorLib XtorLib
    {
      get
      {
        return getXtorLib();
      }
    }
    public string Description
    {
      set
      {
        setDescription(value);
      }
      get
      {
        return getDescription();
      }
    }
    public string CxxFlags
    {
      set
      {
        setCxxFlags(value);
      }
      get
      {
        return getCxxFlags();
      }
    }
    public string LinkFlags
    {
      set
      {
        setLinkFlags(value);
      }
      get
      {
        return getLinkFlags();
      }
    }
    public string SourceFiles
    {
      set
      {
        setSourceFiles(value);
      }
      get
      {
        return getSourceFiles();
      }
    }
    public string IncludeFiles
    {
      set
      {
        setIncludeFiles(value);
      }
      get
      {
        return getIncludeFiles();
      }
    }
    public string LibName
    {
      set
      {
        setLibName(value);
      }
      get
      {
        return getLibName();
      }
    }
    public string CompName
    {
      set
      {
        setCompName(value);
      }
      get
      {
        return getCompName();
      }
    }
    public string CompDisplayName
    {
      set
      {
        setCompDisplayName(value);
      }
      get
      {
        return getCompDisplayName();
      }
    }
    public string TopModuleName
    {
      set
      {
        setTopModuleName(value);
      }
      get
      {
        return getTopModuleName();
      }
    }
    public string WaveFile
    {
      set
      {
        setWaveFile(value);
      }
      get
      {
        return getWaveFile();
      }
    }
    public CcfgEnum.CarbonCfgWaveType WaveType
    {
      set
      {
        setWaveType(value);
      }
      get
      {
        return getWaveType();
      }
    }
    public CarbonCfgELFLoader ELFLoader
    {
      get
      {
        return getELFLoader();
      }
    }
    public CarbonCfgProcInfo ProcInfo
    {
      get
      {
        return getProcInfo();
      }
    }
    public string Type
    {
      set
      {
        setType(value);
      }
      get
      {
        return getType();
      }
    }
    public string Version
    {
      set
      {
        setVersion(value);
      }
      get
      {
        return getVersion();
      }
    }
    public CarbonCfgCadi Cadi
    {
      get
      {
        return getCadi();
      }
    }
    public bool RequiresMkLibrary
    {
      set
      {
        setRequiresMkLibrary(value);
      }
      get
      {
        return getRequiresMkLibrary();
      }
    }
    public bool UseVersionedMkLibrary
    {
      set
      {
        setUseVersionedMkLibrary(value);
      }
      get
      {
        return getUseVersionedMkLibrary();
      }
    }
    #endregion Properties
    #region Methods
    public CcfgFlags.CarbonCfgComponentEditFlags GetEditFlags()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return (CcfgFlags.CarbonCfgComponentEditFlags)CSharp_ScrCcfg_GetEditFlags_0_3(objCPtr);
    }

    public void SetEditFlags(CcfgFlags.CarbonCfgComponentEditFlags newValue)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_SetEditFlags_1_1(objCPtr, (int)newValue);
    }

    public string getFilePath()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getFilePath_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setFilePath(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setFilePath_1_1(objCPtr, newVal);
    }

    public string getDescription()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getDescription_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setDescription(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setDescription_1_1(objCPtr, newVal);
    }

    public string getCxxFlags()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getCxxFlags_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setCxxFlags(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setCxxFlags_1_1(objCPtr, newVal);
    }

    public string getLinkFlags()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getLinkFlags_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setLinkFlags(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setLinkFlags_1_1(objCPtr, newVal);
    }

    public string getSourceFiles()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getSourceFiles_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setSourceFiles(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setSourceFiles_1_1(objCPtr, newVal);
    }

    public string getIncludeFiles()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getIncludeFiles_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setIncludeFiles(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setIncludeFiles_1_1(objCPtr, newVal);
    }

    public string getLibName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getLibName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setLibName(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setLibName_1_1(objCPtr, newVal);
    }

    public string getCompName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getCompName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setCompName(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setCompName_1_1(objCPtr, newVal);
    }

    public string getCompDisplayName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getCompDisplayName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setCompDisplayName(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setCompDisplayName_1_1(objCPtr, newVal);
    }

    public string getTopModuleName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getTopModuleName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setTopModuleName(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setTopModuleName_1_1(objCPtr, newVal);
    }

    public string getWaveFile()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getWaveFile_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setWaveFile(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setWaveFile_1_1(objCPtr, newVal);
    }

    public CcfgEnum.CarbonCfgWaveType getWaveType()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return (CcfgEnum.CarbonCfgWaveType)CSharp_ScrCcfg_getWaveType_0_35(objCPtr);
    }

    public void setWaveType(CcfgEnum.CarbonCfgWaveType wt)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setWaveType_1_21(objCPtr, (int)wt);
    }

    public string getType()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getType_0_35(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setType(string name)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setType_1_1(objCPtr, name);
    }

    public string getVersion()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getVersion_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setVersion(string name)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setVersion_1_1(objCPtr, name);
    }

    public string getDocFile()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getDocFile_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setDocFile(string name)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setDocFile_1_1(objCPtr, name);
    }

    public string getLoadfileExtension()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getLoadfileExtension_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setLoadfileExtension(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setLoadfileExtension_1_1(objCPtr, newVal);
    }

    public bool getUseStaticScheduling()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_getUseStaticScheduling_0_3(objCPtr);
    }

    public void setUseStaticScheduling(int val)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setUseStaticScheduling_1_19(objCPtr, val);
    }

    public int getLegacyMemories()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_getLegacyMemories_0_3(objCPtr);
    }

    public void setLegacyMemories(int val)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setLegacyMemories_1_19(objCPtr, val);
    }

    public int isStandAloneComp()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_isStandAloneComp_0_7(objCPtr);
    }

    public void setIsStandAloneComp(int val)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setIsStandAloneComp_1_19(objCPtr, val);
    }

    public int isSubComponentFullName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_isSubComponentFullName_0_3(objCPtr);
    }

    public void setSubComponentFullName(int val)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setSubComponentFullName_1_19(objCPtr, val);
    }

    public uint getNumSubComponents()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_getNumSubComponents_0_3(objCPtr);
    }

    public uint getNumXtorInstances()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_getNumXtorInstances_0_3(objCPtr);
    }

    public uint getNumESLPorts()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_getNumESLPorts_0_3(objCPtr);
    }

    public uint getNumRTLPorts()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_getNumRTLPorts_0_3(objCPtr);
    }

    public uint getNumParams()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_getNumParams_0_3(objCPtr);
    }

    public uint getNumRegisters()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_getNumRegisters_0_35(objCPtr);
    }

    public uint getNumGroups()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_getNumGroups_0_3(objCPtr);
    }

    public uint getNumMemories()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_getNumMemories_0_3(objCPtr);
    }

    public uint getNumCustomCodes()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_getNumCustomCodes_0_3(objCPtr);
    }

    public CcfgEnum.CarbonCfgStatus read(CcfgEnum.CarbonCfgMode mode, string fileName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return (CcfgEnum.CarbonCfgStatus)CSharp_ScrCcfg_read_2_1(objCPtr, (int)mode, fileName);
    }

    public CcfgEnum.CarbonCfgStatus readNoCheck(CcfgEnum.CarbonCfgMode mode, string fileName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return (CcfgEnum.CarbonCfgStatus)CSharp_ScrCcfg_readNoCheck_2_1(objCPtr, (int)mode, fileName);
    }

    public CcfgEnum.CarbonCfgStatus write(string fileName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return (CcfgEnum.CarbonCfgStatus)CSharp_ScrCcfg_write_1_1(objCPtr, fileName);
    }

    public CcfgEnum.CarbonCfgStatus write()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return (CcfgEnum.CarbonCfgStatus)CSharp_ScrCcfg_write_0_3(objCPtr);
    }

    public CarbonCfgELFLoader getELFLoader()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_getELFLoader_0_7(objCPtr);
      CarbonCfgELFLoader ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgELFLoader(cPtr, false);
      return ret;
    }

    public CarbonCfgProcInfo getProcInfo()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_getProcInfo_0_3(objCPtr);
      CarbonCfgProcInfo ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgProcInfo(cPtr, false);
      return ret;
    }

    public Ccfg addSubComp(string componentName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_addSubComp_1_1(objCPtr, componentName);
      Ccfg ret = (cPtr == IntPtr.Zero) ? null : new Ccfg(cPtr, false);
      return ret;
    }

    public void removeSubComp(Ccfg comp)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_removeSubComp_1_3(objCPtr, Ccfg.getCPtr(comp).Handle);
    }

    public void removeSubComps()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_removeSubComps_0_3(objCPtr);
    }

    public void removeRegisters()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_removeRegisters_0_35(objCPtr);
    }

    public void removeMemories()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_removeMemories_0_3(objCPtr);
    }

    public void removeCustomCodes()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_removeCustomCodes_0_3(objCPtr);
    }

    public void removeXtorInstances()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_removeXtorInstances_0_3(objCPtr);
    }

    public void removeParameters()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_removeParameters_0_35(objCPtr);
    }

    public void removeGroups()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_removeGroups_0_3(objCPtr);
    }

    public Ccfg getSubComponent(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_getSubComponent_1_19(objCPtr, i);
      Ccfg ret = (cPtr == IntPtr.Zero) ? null : new Ccfg(cPtr, false);
      return ret;
    }

    public string getUniqueRegName(string baseName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getUniqueRegName_1_1(objCPtr, baseName);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getUniqueGroupName(string baseName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getUniqueGroupName_1_1(objCPtr, baseName);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getUniqueESLName(string baseName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr ipSr = CSharp_ScrCcfg_getUniqueESLName_1_1(objCPtr, baseName);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public uint numGroups()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_numGroups_0_3(objCPtr);
    }

    public CarbonCfgGroup getGroup(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_getGroup_1_19(objCPtr, i);
      CarbonCfgGroup ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgGroup(cPtr, false);
      return ret;
    }

    public CarbonCfgGroup findGroup(string groupName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_findGroup_1_1(objCPtr, groupName);
      CarbonCfgGroup ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgGroup(cPtr, false);
      return ret;
    }

    public CarbonCfgGroup addGroup(string groupName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_addGroup_1_1(objCPtr, groupName);
      CarbonCfgGroup ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgGroup(cPtr, false);
      return ret;
    }

    public CarbonCfgXtorLib getXtorLib()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_getXtorLib_0_39(objCPtr);
      CarbonCfgXtorLib ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorLib(cPtr, false);
      return ret;
    }

    public CarbonCfgESLPort findESLPort(string portName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_findESLPort_1_1(objCPtr, portName);
      CarbonCfgESLPort ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgESLPort(cPtr, false);
      return ret;
    }

    public CarbonCfgRTLPort findRTLPort(string portName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_findRTLPort_1_1(objCPtr, portName);
      CarbonCfgRTLPort ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRTLPort(cPtr, false);
      return ret;
    }

    public CarbonCfgRegister addRegister(string debugName, CarbonCfgGroup group, uint width, bool bigEndian, CcfgEnum.CarbonCfgRadix radixVal, string commentStr, bool pcReg)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_addRegister_7_3(objCPtr, debugName, CarbonCfgGroup.getCPtr(group).Handle, width, bigEndian, (int)radixVal, commentStr, pcReg);
      CarbonCfgRegister ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegister(cPtr, false);
      return ret;
    }

    public CarbonCfgRegister addRegister(string debugName, CarbonCfgGroup group, uint width, bool bigEndian, CcfgEnum.CarbonCfgRadix radixVal, string commentStr)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_addRegister_6_1(objCPtr, debugName, CarbonCfgGroup.getCPtr(group).Handle, width, bigEndian, (int)radixVal, commentStr);
      CarbonCfgRegister ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegister(cPtr, false);
      return ret;
    }

    public CarbonCfgCompCustomCode addCustomCode()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_addCustomCode_0_3(objCPtr);
      CarbonCfgCompCustomCode ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgCompCustomCode(cPtr, false);
      return ret;
    }

    public CarbonCfgCompCustomCode getCustomCode(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_getCustomCode_1_19(objCPtr, i);
      CarbonCfgCompCustomCode ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgCompCustomCode(cPtr, false);
      return ret;
    }

    public void removeCustomCode(CarbonCfgCompCustomCode customCode)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_removeCustomCode_1_3(objCPtr, CarbonCfgCompCustomCode.getCPtr(customCode).Handle);
    }

    public CarbonCfgESLPort addESLPort(CarbonCfgRTLPort rtlPort, string name, CcfgEnum.CarbonCfgESLPortType ptype, string typeDef, CcfgEnum.CarbonCfgESLPortMode pmode)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_addESLPort_5_5(objCPtr, CarbonCfgRTLPort.getCPtr(rtlPort).Handle, name, (int)ptype, typeDef, (int)pmode);
      CarbonCfgESLPort ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgESLPort(cPtr, false);
      return ret;
    }

    public CarbonCfgXtorParamInst addParam(string Name, CcfgEnum.CarbonCfgParamDataType type, string Value, CcfgEnum.CarbonCfgParamFlag scope, string Description, string EnumChoices)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_addParam_6_1(objCPtr, Name, (int)type, Value, (int)scope, Description, EnumChoices);
      CarbonCfgXtorParamInst ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorParamInst(cPtr, false);
      return ret;
    }

    public CarbonCfgXtorParamInst addParam(string Name, CcfgEnum.CarbonCfgParamDataType type, string Value, CcfgEnum.CarbonCfgParamFlag scope, string Description)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_addParam_5_1(objCPtr, Name, (int)type, Value, (int)scope, Description);
      CarbonCfgXtorParamInst ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorParamInst(cPtr, false);
      return ret;
    }

    public CarbonCfgXtorInstance addXtor(string instName, CarbonCfgXtor type)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_addXtor_2_3(objCPtr, instName, CarbonCfgXtor.getCPtr(type).Handle);
      CarbonCfgXtorInstance ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorInstance(cPtr, false);
      return ret;
    }

    public CarbonCfgRegister getRegister(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_getRegister_1_19(objCPtr, i);
      CarbonCfgRegister ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegister(cPtr, false);
      return ret;
    }

    public CarbonCfgXtorInstance getXtorInstance(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_getXtorInstance_1_19(objCPtr, i);
      CarbonCfgXtorInstance ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorInstance(cPtr, false);
      return ret;
    }

    public CarbonCfgESLPort getESLPort(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_getESLPort_1_19(objCPtr, i);
      CarbonCfgESLPort ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgESLPort(cPtr, false);
      return ret;
    }

    public CarbonCfgRTLPort getRTLPort(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_getRTLPort_1_19(objCPtr, i);
      CarbonCfgRTLPort ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRTLPort(cPtr, false);
      return ret;
    }

    public CarbonCfgXtorParamInst getParam(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_getParam_1_19(objCPtr, i);
      CarbonCfgXtorParamInst ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorParamInst(cPtr, false);
      return ret;
    }

    public CarbonCfgXtor findXtor(string xtorName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_findXtor_1_1(objCPtr, xtorName);
      CarbonCfgXtor ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtor(cPtr, false);
      return ret;
    }

    public CarbonCfgXtorInstance findXtorInstance(string xtorName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_findXtorInstance_1_1(objCPtr, xtorName);
      CarbonCfgXtorInstance ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorInstance(cPtr, false);
      return ret;
    }

    public void removeXtorInstance(CarbonCfgXtorInstance xtorInst)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_removeXtorInstance_1_3(objCPtr, CarbonCfgXtorInstance.getCPtr(xtorInst).Handle);
    }

    public CarbonCfgRegister findRegister(string regName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_findRegister_1_1(objCPtr, regName);
      CarbonCfgRegister ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegister(cPtr, false);
      return ret;
    }

    public void removeRegister(CarbonCfgRegister reg)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_removeRegister_1_3(objCPtr, CarbonCfgRegister.getCPtr(reg).Handle);
    }

    public void removeMemory(CarbonCfgMemory mem)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_removeMemory_1_3(objCPtr, CarbonCfgMemory.getCPtr(mem).Handle);
    }

    public void disconnect(CarbonCfgRTLConnection conn)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_disconnect_1_3(objCPtr, CarbonCfgRTLConnection.getCPtr(conn).Handle);
    }

    public CarbonCfgMemory addMemory(string name, uint width, uint maxAddr)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_addMemory_3_19(objCPtr, name, width, maxAddr);
      CarbonCfgMemory ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemory(cPtr, false);
      return ret;
    }

    public uint numMemories()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_numMemories_0_3(objCPtr);
    }

    public CarbonCfgMemory getMemory(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_getMemory_1_19(objCPtr, i);
      CarbonCfgMemory ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemory(cPtr, false);
      return ret;
    }

    public CarbonCfgCadi getCadi()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_getCadi_0_3(objCPtr);
      CarbonCfgCadi ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgCadi(cPtr, false);
      return ret;
    }

    public CarbonCfgCompCustomCode cloneCustomCode(CarbonCfgCompCustomCode cc)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_cloneCustomCode_1_3(objCPtr, CarbonCfgCompCustomCode.getCPtr(cc).Handle);
      CarbonCfgCompCustomCode ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgCompCustomCode(cPtr, false);
      return ret;
    }

    public CarbonCfgMemory cloneMemory(CarbonCfgMemory cc)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_cloneMemory_1_3(objCPtr, CarbonCfgMemory.getCPtr(cc).Handle);
      CarbonCfgMemory ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemory(cPtr, false);
      return ret;
    }

    public void cloneCadi(CarbonCfgCadi cadi)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_cloneCadi_1_3(objCPtr, CarbonCfgCadi.getCPtr(cadi).Handle);
    }

    public void cloneProcInfo(CarbonCfgProcInfo procInfo)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_cloneProcInfo_1_3(objCPtr, CarbonCfgProcInfo.getCPtr(procInfo).Handle);
    }

    public CarbonCfgRegister clone(CarbonCfgGroup group, CarbonCfgRegister cc)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      IntPtr cPtr = CSharp_ScrCcfg_clone_2_3(objCPtr, CarbonCfgGroup.getCPtr(group).Handle, CarbonCfgRegister.getCPtr(cc).Handle);
      CarbonCfgRegister ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegister(cPtr, false);
      return ret;
    }

    public void setRequiresMkLibrary(bool req)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setRequiresMkLibrary_1_3(objCPtr, req);
    }

    public bool getRequiresMkLibrary()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_getRequiresMkLibrary_0_3(objCPtr);
    }

    public void setUseVersionedMkLibrary(bool req)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      CSharp_ScrCcfg_setUseVersionedMkLibrary_1_3(objCPtr, req);
    }

    public bool getUseVersionedMkLibrary()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Ccfg object");
      return CSharp_ScrCcfg_getUseVersionedMkLibrary_0_3(objCPtr);
    }

    #endregion Methods

  }

}
