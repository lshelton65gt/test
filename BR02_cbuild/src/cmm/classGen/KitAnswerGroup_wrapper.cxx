PINVOKEABLE void CSharp_KitAnswerGroup_deleteLater_0_7(void* thisObj)
{
    KitAnswerGroup* obj = (KitAnswerGroup*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE char* CSharp_KitAnswerGroup_getID_0_7(void* thisObj)
{
    KitAnswerGroup* obj = (KitAnswerGroup*)(thisObj);
    return EmbeddedMono::AllocString(obj->getID());
}
PINVOKEABLE void CSharp_KitAnswerGroup_setID_1_1(void* thisObj, const char* newVal)
{
    KitAnswerGroup* obj = (KitAnswerGroup*)(thisObj);
    obj->setID(newVal);
}
PINVOKEABLE unsigned int CSharp_KitAnswerGroup_getIndex_0_7(void* thisObj)
{
    KitAnswerGroup* obj = (KitAnswerGroup*)(thisObj);
    return obj->getIndex();
}
PINVOKEABLE void CSharp_KitAnswerGroup_setIndex_1_19(void* thisObj, unsigned int newVal)
{
    KitAnswerGroup* obj = (KitAnswerGroup*)(thisObj);
    obj->setIndex(newVal);
}
PINVOKEABLE KitAnswers* CSharp_KitAnswerGroup_getAnswers_0_3(void* thisObj)
{
    KitAnswerGroup* obj = (KitAnswerGroup*)(thisObj);
    return obj->getAnswers();
}
