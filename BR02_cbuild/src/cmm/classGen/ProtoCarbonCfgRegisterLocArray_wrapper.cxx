PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocArray_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CarbonCfgRegisterLocConstant* CSharp_ProtoCarbonCfgRegisterLocArray_castConstant_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    return obj->castConstant();
}
PINVOKEABLE CarbonCfgRegisterLocRTL* CSharp_ProtoCarbonCfgRegisterLocArray_castRTL_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    return obj->castRTL();
}
PINVOKEABLE CarbonCfgRegisterLocReg* CSharp_ProtoCarbonCfgRegisterLocArray_castReg_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    return obj->castReg();
}
PINVOKEABLE CarbonCfgRegisterLocArray* CSharp_ProtoCarbonCfgRegisterLocArray_castArray_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    return obj->castArray();
}
PINVOKEABLE CarbonCfgRegisterLocUser* CSharp_ProtoCarbonCfgRegisterLocArray_castUser_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    return obj->castUser();
}
PINVOKEABLE CcfgEnum::CarbonCfgRegisterLocKind CSharp_ProtoCarbonCfgRegisterLocArray_getType_0_35(void* thisObj)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    return obj->getType();
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgRegisterLocArray_getHasRange_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    return obj->getHasRange();
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgRegisterLocArray_hasPath_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    return obj->hasPath();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgRegisterLocArray_getPath_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    return EmbeddedMono::AllocString(obj->getPath());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocArray_setPath_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    obj->setPath(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterLocArray_getLeft_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    return obj->getLeft();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocArray_setLeft_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    obj->setLeft(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterLocArray_getRight_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    return obj->getRight();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocArray_setRight_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    obj->setRight(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterLocArray_getLSB_0_39(void* thisObj)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    return obj->getLSB();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocArray_setLSB_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    obj->setLSB(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterLocArray_getMSB_0_39(void* thisObj)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    return obj->getMSB();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocArray_setMSB_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    obj->setMSB(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterLocArray_getIndex_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    return obj->getIndex();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocArray_setIndex_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgRegisterLocArray* obj = (ProtoCarbonCfgRegisterLocArray*)(thisObj);
    obj->setIndex(newVal);
}
PINVOKEABLE ProtoCarbonCfgRegisterLocRTL* CSharp_ProtoCarbonCfgRegisterLocArray_UpCast(ProtoCarbonCfgRegisterLocArray* obj)
{
  return (ProtoCarbonCfgRegisterLocRTL*)(obj);
}
