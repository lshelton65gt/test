using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgRegisterField : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgRegisterField(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgRegisterField obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgRegisterField()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_getWidth_0_7")]
    private extern static uint CSharp_ProtoCarbonCfgRegisterField_getWidth_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_getHigh_0_7")]
    private extern static uint CSharp_ProtoCarbonCfgRegisterField_getHigh_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_setHigh_1_19")]
    private extern static void CSharp_ProtoCarbonCfgRegisterField_setHigh_1_19(HandleRef thisObj, uint nv);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_getLow_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgRegisterField_getLow_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_setLow_1_19")]
    private extern static void CSharp_ProtoCarbonCfgRegisterField_setLow_1_19(HandleRef thisObj, uint nv);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_getName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegisterField_getName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_setName_1_1")]
    private extern static void CSharp_ProtoCarbonCfgRegisterField_setName_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_getLoc_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegisterField_getLoc_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_setLocReg_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRegisterField_setLocReg_1_3(HandleRef thisObj, IntPtr newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_setLocConstant_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRegisterField_setLocConstant_1_3(HandleRef thisObj, IntPtr newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_setLocArray_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRegisterField_setLocArray_1_3(HandleRef thisObj, IntPtr newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_setLocUser_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRegisterField_setLocUser_1_3(HandleRef thisObj, IntPtr newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_getRegLoc_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegisterField_getRegLoc_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_getConstantLoc_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegisterField_getConstantLoc_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_getArrayLoc_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegisterField_getArrayLoc_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_getUserLoc_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegisterField_getUserLoc_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_getAccess_0_3")]
    private extern static int CSharp_ProtoCarbonCfgRegisterField_getAccess_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterField_setAccess_1_21")]
    private extern static void CSharp_ProtoCarbonCfgRegisterField_setAccess_1_21(HandleRef thisObj, int newVal);
    #endregion DllImports
    #region Properties
    public string Name
    {
      set
      {
        setName(value);
      }
      get
      {
        return getName();
      }
    }
    public uint Width
    {
      get
      {
        return getWidth();
      }
    }
    public uint High
    {
      set
      {
        setHigh(value);
      }
      get
      {
        return getHigh();
      }
    }
    public uint Low
    {
      set
      {
        setLow(value);
      }
      get
      {
        return getLow();
      }
    }
    public CcfgEnum.CarbonCfgRegAccessType Access
    {
      set
      {
        setAccess(value);
      }
      get
      {
        return getAccess();
      }
    }
    public CarbonCfgRegisterLocReg RegLoc
    {
      get
      {
        return getRegLoc();
      }
    }
    public CarbonCfgRegisterLocConstant ConstantLoc
    {
      get
      {
        return getConstantLoc();
      }
    }
    public CarbonCfgRegisterLocArray ArrayLoc
    {
      get
      {
        return getArrayLoc();
      }
    }
    public CarbonCfgRegisterLocUser UserLoc
    {
      get
      {
        return getUserLoc();
      }
    }
    #endregion Properties
    #region Methods
    public uint getWidth()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      return CSharp_ProtoCarbonCfgRegisterField_getWidth_0_7(objCPtr);
    }

    public uint getHigh()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      return CSharp_ProtoCarbonCfgRegisterField_getHigh_0_7(objCPtr);
    }

    public void setHigh(uint nv)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      CSharp_ProtoCarbonCfgRegisterField_setHigh_1_19(objCPtr, nv);
    }

    public uint getLow()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      return CSharp_ProtoCarbonCfgRegisterField_getLow_0_3(objCPtr);
    }

    public void setLow(uint nv)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      CSharp_ProtoCarbonCfgRegisterField_setLow_1_19(objCPtr, nv);
    }

    public string getName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgRegisterField_getName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setName(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      CSharp_ProtoCarbonCfgRegisterField_setName_1_1(objCPtr, newVal);
    }

    public CarbonCfgRegisterLoc getLoc()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRegisterField_getLoc_0_3(objCPtr);
      CarbonCfgRegisterLoc ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegisterLoc(cPtr, false);
      return ret;
    }

    public void setLocReg(CarbonCfgRegisterLocReg newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      CSharp_ProtoCarbonCfgRegisterField_setLocReg_1_3(objCPtr, CarbonCfgRegisterLocReg.getCPtr(newVal).Handle);
    }

    public void setLocConstant(CarbonCfgRegisterLocConstant newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      CSharp_ProtoCarbonCfgRegisterField_setLocConstant_1_3(objCPtr, CarbonCfgRegisterLocConstant.getCPtr(newVal).Handle);
    }

    public void setLocArray(CarbonCfgRegisterLocArray newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      CSharp_ProtoCarbonCfgRegisterField_setLocArray_1_3(objCPtr, CarbonCfgRegisterLocArray.getCPtr(newVal).Handle);
    }

    public void setLocUser(CarbonCfgRegisterLocUser newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      CSharp_ProtoCarbonCfgRegisterField_setLocUser_1_3(objCPtr, CarbonCfgRegisterLocUser.getCPtr(newVal).Handle);
    }

    public CarbonCfgRegisterLocReg getRegLoc()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRegisterField_getRegLoc_0_3(objCPtr);
      CarbonCfgRegisterLocReg ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegisterLocReg(cPtr, false);
      return ret;
    }

    public CarbonCfgRegisterLocConstant getConstantLoc()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRegisterField_getConstantLoc_0_3(objCPtr);
      CarbonCfgRegisterLocConstant ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegisterLocConstant(cPtr, false);
      return ret;
    }

    public CarbonCfgRegisterLocArray getArrayLoc()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRegisterField_getArrayLoc_0_3(objCPtr);
      CarbonCfgRegisterLocArray ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegisterLocArray(cPtr, false);
      return ret;
    }

    public CarbonCfgRegisterLocUser getUserLoc()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRegisterField_getUserLoc_0_3(objCPtr);
      CarbonCfgRegisterLocUser ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegisterLocUser(cPtr, false);
      return ret;
    }

    public CcfgEnum.CarbonCfgRegAccessType getAccess()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      return (CcfgEnum.CarbonCfgRegAccessType)CSharp_ProtoCarbonCfgRegisterField_getAccess_0_3(objCPtr);
    }

    public void setAccess(CcfgEnum.CarbonCfgRegAccessType newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterField object");
      CSharp_ProtoCarbonCfgRegisterField_setAccess_1_21(objCPtr, (int)newVal);
    }

    #endregion Methods

  }

}
