using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgRegisterLoc : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgRegisterLoc(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgRegisterLoc obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgRegisterLoc()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLoc_castConstant_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegisterLoc_castConstant_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLoc_castRTL_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegisterLoc_castRTL_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLoc_castReg_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegisterLoc_castReg_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLoc_castArray_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegisterLoc_castArray_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLoc_castUser_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegisterLoc_castUser_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLoc_getType_0_35")]
    private extern static int CSharp_ProtoCarbonCfgRegisterLoc_getType_0_35(HandleRef thisObj);
    #endregion DllImports
    #region Properties
    public CarbonCfgRegisterLocConstant CastConstant
    {
      get
      {
        return castConstant();
      }
    }
    public CarbonCfgRegisterLocRTL CastRTL
    {
      get
      {
        return castRTL();
      }
    }
    public CarbonCfgRegisterLocReg CastReg
    {
      get
      {
        return castReg();
      }
    }
    public CarbonCfgRegisterLocArray CastArray
    {
      get
      {
        return castArray();
      }
    }
    public CarbonCfgRegisterLocUser CastUser
    {
      get
      {
        return castUser();
      }
    }
    public CcfgEnum.CarbonCfgRegisterLocKind Type
    {
      get
      {
        return getType();
      }
    }
    #endregion Properties
    #region Methods
    public CarbonCfgRegisterLocConstant castConstant()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLoc object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRegisterLoc_castConstant_0_7(objCPtr);
      CarbonCfgRegisterLocConstant ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegisterLocConstant(cPtr, false);
      return ret;
    }

    public CarbonCfgRegisterLocRTL castRTL()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLoc object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRegisterLoc_castRTL_0_7(objCPtr);
      CarbonCfgRegisterLocRTL ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegisterLocRTL(cPtr, false);
      return ret;
    }

    public CarbonCfgRegisterLocReg castReg()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLoc object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRegisterLoc_castReg_0_3(objCPtr);
      CarbonCfgRegisterLocReg ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegisterLocReg(cPtr, false);
      return ret;
    }

    public CarbonCfgRegisterLocArray castArray()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLoc object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRegisterLoc_castArray_0_3(objCPtr);
      CarbonCfgRegisterLocArray ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegisterLocArray(cPtr, false);
      return ret;
    }

    public CarbonCfgRegisterLocUser castUser()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLoc object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRegisterLoc_castUser_0_7(objCPtr);
      CarbonCfgRegisterLocUser ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegisterLocUser(cPtr, false);
      return ret;
    }

    public CcfgEnum.CarbonCfgRegisterLocKind getType()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLoc object");
      return (CcfgEnum.CarbonCfgRegisterLocKind)CSharp_ProtoCarbonCfgRegisterLoc_getType_0_35(objCPtr);
    }

    #endregion Methods

  }

}
