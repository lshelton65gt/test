PINVOKEABLE void CSharp_ProtoCarbonCfgXtorLib_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgXtorLib* obj = (ProtoCarbonCfgXtorLib*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgXtorLib_numXtors_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorLib* obj = (ProtoCarbonCfgXtorLib*)(thisObj);
    return obj->numXtors();
}
PINVOKEABLE CarbonCfgXtor* CSharp_ProtoCarbonCfgXtorLib_getXtor_1_19(void* thisObj, unsigned int i)
{
    ProtoCarbonCfgXtorLib* obj = (ProtoCarbonCfgXtorLib*)(thisObj);
    return obj->getXtor(i);
}
PINVOKEABLE CarbonCfgXtor* CSharp_ProtoCarbonCfgXtorLib_findXtor_1_1(void* thisObj, const char* xtorName)
{
    ProtoCarbonCfgXtorLib* obj = (ProtoCarbonCfgXtorLib*)(thisObj);
    return obj->findXtor(xtorName);
}
