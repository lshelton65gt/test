PINVOKEABLE void CSharp_ProtoCarbonCfgRTLConnection_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgRTLConnection* obj = (ProtoCarbonCfgRTLConnection*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CcfgEnum::CarbonCfgRTLConnectionType CSharp_ProtoCarbonCfgRTLConnection_getType_0_35(void* thisObj)
{
    ProtoCarbonCfgRTLConnection* obj = (ProtoCarbonCfgRTLConnection*)(thisObj);
    return obj->getType();
}
PINVOKEABLE CarbonCfgRTLPort* CSharp_ProtoCarbonCfgRTLConnection_getRTLPort_0_7(void* thisObj)
{
    ProtoCarbonCfgRTLConnection* obj = (ProtoCarbonCfgRTLConnection*)(thisObj);
    return obj->getRTLPort();
}
PINVOKEABLE CarbonCfgClockGen* CSharp_ProtoCarbonCfgRTLConnection_getClockGen_0_7(void* thisObj)
{
    ProtoCarbonCfgRTLConnection* obj = (ProtoCarbonCfgRTLConnection*)(thisObj);
    return obj->getClockGen();
}
PINVOKEABLE CarbonCfgResetGen* CSharp_ProtoCarbonCfgRTLConnection_getResetGen_0_7(void* thisObj)
{
    ProtoCarbonCfgRTLConnection* obj = (ProtoCarbonCfgRTLConnection*)(thisObj);
    return obj->getResetGen();
}
PINVOKEABLE CarbonCfgTie* CSharp_ProtoCarbonCfgRTLConnection_getTie_0_35(void* thisObj)
{
    ProtoCarbonCfgRTLConnection* obj = (ProtoCarbonCfgRTLConnection*)(thisObj);
    return obj->getTie();
}
PINVOKEABLE CarbonCfgTieParam* CSharp_ProtoCarbonCfgRTLConnection_getTieParam_0_3(void* thisObj)
{
    ProtoCarbonCfgRTLConnection* obj = (ProtoCarbonCfgRTLConnection*)(thisObj);
    return obj->getTieParam();
}
PINVOKEABLE CarbonCfgXtorConn* CSharp_ProtoCarbonCfgRTLConnection_getXtorConn_0_7(void* thisObj)
{
    ProtoCarbonCfgRTLConnection* obj = (ProtoCarbonCfgRTLConnection*)(thisObj);
    return obj->getXtorConn();
}
PINVOKEABLE CarbonCfgESLPort* CSharp_ProtoCarbonCfgRTLConnection_getESLPort_0_7(void* thisObj)
{
    ProtoCarbonCfgRTLConnection* obj = (ProtoCarbonCfgRTLConnection*)(thisObj);
    return obj->getESLPort();
}
