PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocUser_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocUser* obj = (ProtoCarbonCfgRegisterLocUser*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CarbonCfgRegisterLocConstant* CSharp_ProtoCarbonCfgRegisterLocUser_castConstant_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocUser* obj = (ProtoCarbonCfgRegisterLocUser*)(thisObj);
    return obj->castConstant();
}
PINVOKEABLE CarbonCfgRegisterLocRTL* CSharp_ProtoCarbonCfgRegisterLocUser_castRTL_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocUser* obj = (ProtoCarbonCfgRegisterLocUser*)(thisObj);
    return obj->castRTL();
}
PINVOKEABLE CarbonCfgRegisterLocReg* CSharp_ProtoCarbonCfgRegisterLocUser_castReg_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLocUser* obj = (ProtoCarbonCfgRegisterLocUser*)(thisObj);
    return obj->castReg();
}
PINVOKEABLE CarbonCfgRegisterLocArray* CSharp_ProtoCarbonCfgRegisterLocUser_castArray_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLocUser* obj = (ProtoCarbonCfgRegisterLocUser*)(thisObj);
    return obj->castArray();
}
PINVOKEABLE CarbonCfgRegisterLocUser* CSharp_ProtoCarbonCfgRegisterLocUser_castUser_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocUser* obj = (ProtoCarbonCfgRegisterLocUser*)(thisObj);
    return obj->castUser();
}
PINVOKEABLE CcfgEnum::CarbonCfgRegisterLocKind CSharp_ProtoCarbonCfgRegisterLocUser_getType_0_35(void* thisObj)
{
    ProtoCarbonCfgRegisterLocUser* obj = (ProtoCarbonCfgRegisterLocUser*)(thisObj);
    return obj->getType();
}
PINVOKEABLE ProtoCarbonCfgRegisterLoc* CSharp_ProtoCarbonCfgRegisterLocUser_UpCast(ProtoCarbonCfgRegisterLocUser* obj)
{
  return (ProtoCarbonCfgRegisterLoc*)(obj);
}
