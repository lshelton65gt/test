PINVOKEABLE void CSharp_KitVersions_deleteLater_0_7(void* thisObj)
{
    KitVersions* obj = (KitVersions*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE unsigned int CSharp_KitVersions_numVersions_0_3(void* thisObj)
{
    KitVersions* obj = (KitVersions*)(thisObj);
    return obj->numVersions();
}
PINVOKEABLE KitVersion* CSharp_KitVersions_getVersion_1_19(void* thisObj, unsigned int index)
{
    KitVersions* obj = (KitVersions*)(thisObj);
    return obj->getVersion(index);
}
