PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocReg_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CarbonCfgRegisterLocConstant* CSharp_ProtoCarbonCfgRegisterLocReg_castConstant_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    return obj->castConstant();
}
PINVOKEABLE CarbonCfgRegisterLocRTL* CSharp_ProtoCarbonCfgRegisterLocReg_castRTL_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    return obj->castRTL();
}
PINVOKEABLE CarbonCfgRegisterLocReg* CSharp_ProtoCarbonCfgRegisterLocReg_castReg_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    return obj->castReg();
}
PINVOKEABLE CarbonCfgRegisterLocArray* CSharp_ProtoCarbonCfgRegisterLocReg_castArray_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    return obj->castArray();
}
PINVOKEABLE CarbonCfgRegisterLocUser* CSharp_ProtoCarbonCfgRegisterLocReg_castUser_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    return obj->castUser();
}
PINVOKEABLE CcfgEnum::CarbonCfgRegisterLocKind CSharp_ProtoCarbonCfgRegisterLocReg_getType_0_35(void* thisObj)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    return obj->getType();
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgRegisterLocReg_getHasRange_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    return obj->getHasRange();
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgRegisterLocReg_hasPath_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    return obj->hasPath();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgRegisterLocReg_getPath_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getPath());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocReg_setPath_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    obj->setPath(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterLocReg_getLeft_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    return obj->getLeft();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocReg_setLeft_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    obj->setLeft(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterLocReg_getRight_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    return obj->getRight();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocReg_setRight_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    obj->setRight(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterLocReg_getLSB_0_39(void* thisObj)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    return obj->getLSB();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocReg_setLSB_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    obj->setLSB(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterLocReg_getMSB_0_39(void* thisObj)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    return obj->getMSB();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocReg_setMSB_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgRegisterLocReg* obj = (ProtoCarbonCfgRegisterLocReg*)(thisObj);
    obj->setMSB(newVal);
}
PINVOKEABLE ProtoCarbonCfgRegisterLocRTL* CSharp_ProtoCarbonCfgRegisterLocReg_UpCast(ProtoCarbonCfgRegisterLocReg* obj)
{
  return (ProtoCarbonCfgRegisterLocRTL*)(obj);
}
