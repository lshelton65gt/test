using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class accessTypeEnum
  {
    public enum accessType
    {
      READONLY = 0,
      WRITEONLY = 1,
      READWRITE = 2
    }
  }
}
