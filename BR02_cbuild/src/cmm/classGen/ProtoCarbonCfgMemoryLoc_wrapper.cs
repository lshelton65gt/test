using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgMemoryLoc : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgMemoryLoc(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgMemoryLoc obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgMemoryLoc()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLoc_getType_0_35")]
    private extern static int CSharp_ProtoCarbonCfgMemoryLoc_getType_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLoc_getDisplayStartWordOffset_0_7")]
    private extern static ulong CSharp_ProtoCarbonCfgMemoryLoc_getDisplayStartWordOffset_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLoc_setDisplayStartWordOffset_1_7")]
    private extern static void CSharp_ProtoCarbonCfgMemoryLoc_setDisplayStartWordOffset_1_7(HandleRef thisObj, ulong newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLoc_getDisplayEndWordOffset_0_7")]
    private extern static ulong CSharp_ProtoCarbonCfgMemoryLoc_getDisplayEndWordOffset_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLoc_setDisplayEndWordOffset_1_7")]
    private extern static void CSharp_ProtoCarbonCfgMemoryLoc_setDisplayEndWordOffset_1_7(HandleRef thisObj, ulong newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLoc_getDisplayMsb_0_7")]
    private extern static uint CSharp_ProtoCarbonCfgMemoryLoc_getDisplayMsb_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLoc_setDisplayMsb_1_19")]
    private extern static void CSharp_ProtoCarbonCfgMemoryLoc_setDisplayMsb_1_19(HandleRef thisObj, uint newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLoc_getDisplayLsb_0_7")]
    private extern static uint CSharp_ProtoCarbonCfgMemoryLoc_getDisplayLsb_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLoc_setDisplayLsb_1_19")]
    private extern static void CSharp_ProtoCarbonCfgMemoryLoc_setDisplayLsb_1_19(HandleRef thisObj, uint newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLoc_castRTL_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemoryLoc_castRTL_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLoc_castPort_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemoryLoc_castPort_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLoc_castUser_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemoryLoc_castUser_0_7(HandleRef thisObj);
    #endregion DllImports
    #region Properties
    public CcfgEnum.CarbonCfgMemLocType Type
    {
      get
      {
        return getType();
      }
    }
    public ulong DisplayStartWordOffset
    {
      set
      {
        setDisplayStartWordOffset(value);
      }
      get
      {
        return getDisplayStartWordOffset();
      }
    }
    public ulong DisplayEndWordOffset
    {
      set
      {
        setDisplayEndWordOffset(value);
      }
      get
      {
        return getDisplayEndWordOffset();
      }
    }
    public CarbonCfgMemoryLocRTL CastRTL
    {
      get
      {
        return castRTL();
      }
    }
    public CarbonCfgMemoryLocPort CastPort
    {
      get
      {
        return castPort();
      }
    }
    public CarbonCfgMemoryLocUser CastUser
    {
      get
      {
        return castUser();
      }
    }
    #endregion Properties
    #region Methods
    public CcfgEnum.CarbonCfgMemLocType getType()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLoc object");
      return (CcfgEnum.CarbonCfgMemLocType)CSharp_ProtoCarbonCfgMemoryLoc_getType_0_35(objCPtr);
    }

    public ulong getDisplayStartWordOffset()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLoc object");
      return CSharp_ProtoCarbonCfgMemoryLoc_getDisplayStartWordOffset_0_7(objCPtr);
    }

    public void setDisplayStartWordOffset(ulong newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLoc object");
      CSharp_ProtoCarbonCfgMemoryLoc_setDisplayStartWordOffset_1_7(objCPtr, newVal);
    }

    public ulong getDisplayEndWordOffset()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLoc object");
      return CSharp_ProtoCarbonCfgMemoryLoc_getDisplayEndWordOffset_0_7(objCPtr);
    }

    public void setDisplayEndWordOffset(ulong newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLoc object");
      CSharp_ProtoCarbonCfgMemoryLoc_setDisplayEndWordOffset_1_7(objCPtr, newVal);
    }

    public uint getDisplayMsb()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLoc object");
      return CSharp_ProtoCarbonCfgMemoryLoc_getDisplayMsb_0_7(objCPtr);
    }

    public void setDisplayMsb(uint newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLoc object");
      CSharp_ProtoCarbonCfgMemoryLoc_setDisplayMsb_1_19(objCPtr, newVal);
    }

    public uint getDisplayLsb()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLoc object");
      return CSharp_ProtoCarbonCfgMemoryLoc_getDisplayLsb_0_7(objCPtr);
    }

    public void setDisplayLsb(uint newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLoc object");
      CSharp_ProtoCarbonCfgMemoryLoc_setDisplayLsb_1_19(objCPtr, newVal);
    }

    public CarbonCfgMemoryLocRTL castRTL()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLoc object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgMemoryLoc_castRTL_0_7(objCPtr);
      CarbonCfgMemoryLocRTL ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemoryLocRTL(cPtr, false);
      return ret;
    }

    public CarbonCfgMemoryLocPort castPort()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLoc object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgMemoryLoc_castPort_0_7(objCPtr);
      CarbonCfgMemoryLocPort ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemoryLocPort(cPtr, false);
      return ret;
    }

    public CarbonCfgMemoryLocUser castUser()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLoc object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgMemoryLoc_castUser_0_7(objCPtr);
      CarbonCfgMemoryLocUser ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgMemoryLocUser(cPtr, false);
      return ret;
    }

    #endregion Methods

  }

}
