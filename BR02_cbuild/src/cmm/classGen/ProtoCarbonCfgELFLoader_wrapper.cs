using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgELFLoader : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgELFLoader(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgELFLoader obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgELFLoader()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgELFLoader_addSection_3_1")]
    private extern static void CSharp_ProtoCarbonCfgELFLoader_addSection_3_1(HandleRef thisObj, string name, uint space, string access);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgELFLoader_numSections_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgELFLoader_numSections_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgELFLoader_getSectionName_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgELFLoader_getSectionName_1_19(HandleRef thisObj, uint index);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgELFLoader_getSectionSpace_1_19")]
    private extern static uint CSharp_ProtoCarbonCfgELFLoader_getSectionSpace_1_19(HandleRef thisObj, uint index);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgELFLoader_getSectionAccess_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgELFLoader_getSectionAccess_1_19(HandleRef thisObj, uint index);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgELFLoader_removeSections_0_3")]
    private extern static void CSharp_ProtoCarbonCfgELFLoader_removeSections_0_3(HandleRef thisObj);
    #endregion DllImports
    #region Properties
    public uint NumSections
    {
      get
      {
        return numSections();
      }
    }
    #endregion Properties
    #region Methods
    public void addSection(string name, uint space, string access)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgELFLoader object");
      CSharp_ProtoCarbonCfgELFLoader_addSection_3_1(objCPtr, name, space, access);
    }

    public uint numSections()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgELFLoader object");
      return CSharp_ProtoCarbonCfgELFLoader_numSections_0_3(objCPtr);
    }

    public string getSectionName(uint index)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgELFLoader object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgELFLoader_getSectionName_1_19(objCPtr, index);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public uint getSectionSpace(uint index)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgELFLoader object");
      return CSharp_ProtoCarbonCfgELFLoader_getSectionSpace_1_19(objCPtr, index);
    }

    public string getSectionAccess(uint index)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgELFLoader object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgELFLoader_getSectionAccess_1_19(objCPtr, index);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void removeSections()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgELFLoader object");
      CSharp_ProtoCarbonCfgELFLoader_removeSections_0_3(objCPtr);
    }

    #endregion Methods

  }

}
