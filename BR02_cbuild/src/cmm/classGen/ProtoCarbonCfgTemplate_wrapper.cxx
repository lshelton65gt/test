PINVOKEABLE void CSharp_ProtoCarbonCfgTemplate_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgTemplate* obj = (ProtoCarbonCfgTemplate*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgTemplate_getNumVariables_0_3(void* thisObj)
{
    ProtoCarbonCfgTemplate* obj = (ProtoCarbonCfgTemplate*)(thisObj);
    return obj->getNumVariables();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgTemplate_getVariable_1_19(void* thisObj, unsigned int index)
{
    ProtoCarbonCfgTemplate* obj = (ProtoCarbonCfgTemplate*)(thisObj);
    return EmbeddedMono::AllocString(obj->getVariable(index));
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgTemplate_getValue_1_1(void* thisObj, const char* variable)
{
    ProtoCarbonCfgTemplate* obj = (ProtoCarbonCfgTemplate*)(thisObj);
    return EmbeddedMono::AllocString(obj->getValue(variable));
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgTemplate_getErrMsg_0_35(void* thisObj)
{
    ProtoCarbonCfgTemplate* obj = (ProtoCarbonCfgTemplate*)(thisObj);
    return EmbeddedMono::AllocString(obj->getErrMsg());
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgTemplate_addVariable_2_1(void* thisObj, const char* variable, const char* value)
{
    ProtoCarbonCfgTemplate* obj = (ProtoCarbonCfgTemplate*)(thisObj);
    return obj->addVariable(variable, value);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgTemplate_clear_0_39(void* thisObj)
{
    ProtoCarbonCfgTemplate* obj = (ProtoCarbonCfgTemplate*)(thisObj);
    obj->clear();
}
