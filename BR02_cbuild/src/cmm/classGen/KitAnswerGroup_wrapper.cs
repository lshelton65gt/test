using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class KitAnswerGroup : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal KitAnswerGroup(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(KitAnswerGroup obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~KitAnswerGroup()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_KitAnswerGroup_getID_0_7")]
    private extern static IntPtr CSharp_KitAnswerGroup_getID_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitAnswerGroup_setID_1_1")]
    private extern static void CSharp_KitAnswerGroup_setID_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_KitAnswerGroup_getIndex_0_7")]
    private extern static uint CSharp_KitAnswerGroup_getIndex_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitAnswerGroup_setIndex_1_19")]
    private extern static void CSharp_KitAnswerGroup_setIndex_1_19(HandleRef thisObj, uint newVal);
    [DllImport("__Internal", EntryPoint="CSharp_KitAnswerGroup_getAnswers_0_3")]
    private extern static IntPtr CSharp_KitAnswerGroup_getAnswers_0_3(HandleRef thisObj);
    #endregion DllImports
    #region Properties
    public uint Index
    {
      get
      {
        return getIndex();
      }
    }
    #endregion Properties
    #region Methods
    public string getID()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL KitAnswerGroup object");
      IntPtr ipSr = CSharp_KitAnswerGroup_getID_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setID(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL KitAnswerGroup object");
      CSharp_KitAnswerGroup_setID_1_1(objCPtr, newVal);
    }

    public uint getIndex()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL KitAnswerGroup object");
      return CSharp_KitAnswerGroup_getIndex_0_7(objCPtr);
    }

    public void setIndex(uint newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL KitAnswerGroup object");
      CSharp_KitAnswerGroup_setIndex_1_19(objCPtr, newVal);
    }

    #endregion Methods

  }

}
