PINVOKEABLE void CSharp_ScrCcfg_deleteLater_0_7(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CcfgFlags::CarbonCfgComponentEditFlags CSharp_ScrCcfg_GetEditFlags_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->GetEditFlags();
}
PINVOKEABLE void CSharp_ScrCcfg_SetEditFlags_1_1(void* thisObj, CcfgFlags::CarbonCfgComponentEditFlags newValue)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->SetEditFlags(newValue);
}
PINVOKEABLE char* CSharp_ScrCcfg_getFilePath_0_7(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getFilePath());
}
PINVOKEABLE void CSharp_ScrCcfg_setFilePath_1_1(void* thisObj, const char* newVal)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setFilePath(newVal);
}
PINVOKEABLE char* CSharp_ScrCcfg_getDescription_0_7(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getDescription());
}
PINVOKEABLE void CSharp_ScrCcfg_setDescription_1_1(void* thisObj, const char* newVal)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setDescription(newVal);
}
PINVOKEABLE char* CSharp_ScrCcfg_getCxxFlags_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getCxxFlags());
}
PINVOKEABLE void CSharp_ScrCcfg_setCxxFlags_1_1(void* thisObj, const char* newVal)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setCxxFlags(newVal);
}
PINVOKEABLE char* CSharp_ScrCcfg_getLinkFlags_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getLinkFlags());
}
PINVOKEABLE void CSharp_ScrCcfg_setLinkFlags_1_1(void* thisObj, const char* newVal)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setLinkFlags(newVal);
}
PINVOKEABLE char* CSharp_ScrCcfg_getSourceFiles_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getSourceFiles());
}
PINVOKEABLE void CSharp_ScrCcfg_setSourceFiles_1_1(void* thisObj, const char* newVal)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setSourceFiles(newVal);
}
PINVOKEABLE char* CSharp_ScrCcfg_getIncludeFiles_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getIncludeFiles());
}
PINVOKEABLE void CSharp_ScrCcfg_setIncludeFiles_1_1(void* thisObj, const char* newVal)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setIncludeFiles(newVal);
}
PINVOKEABLE char* CSharp_ScrCcfg_getLibName_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getLibName());
}
PINVOKEABLE void CSharp_ScrCcfg_setLibName_1_1(void* thisObj, const char* newVal)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setLibName(newVal);
}
PINVOKEABLE char* CSharp_ScrCcfg_getCompName_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getCompName());
}
PINVOKEABLE void CSharp_ScrCcfg_setCompName_1_1(void* thisObj, const char* newVal)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setCompName(newVal);
}
PINVOKEABLE char* CSharp_ScrCcfg_getCompDisplayName_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getCompDisplayName());
}
PINVOKEABLE void CSharp_ScrCcfg_setCompDisplayName_1_1(void* thisObj, const char* newVal)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setCompDisplayName(newVal);
}
PINVOKEABLE char* CSharp_ScrCcfg_getTopModuleName_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getTopModuleName());
}
PINVOKEABLE void CSharp_ScrCcfg_setTopModuleName_1_1(void* thisObj, const char* newVal)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setTopModuleName(newVal);
}
PINVOKEABLE char* CSharp_ScrCcfg_getWaveFile_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getWaveFile());
}
PINVOKEABLE void CSharp_ScrCcfg_setWaveFile_1_1(void* thisObj, const char* newVal)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setWaveFile(newVal);
}
PINVOKEABLE CcfgEnum::CarbonCfgWaveType CSharp_ScrCcfg_getWaveType_0_35(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getWaveType();
}
PINVOKEABLE void CSharp_ScrCcfg_setWaveType_1_21(void* thisObj, CcfgEnum::CarbonCfgWaveType wt)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setWaveType(wt);
}
PINVOKEABLE char* CSharp_ScrCcfg_getType_0_35(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getType());
}
PINVOKEABLE void CSharp_ScrCcfg_setType_1_1(void* thisObj, const char* name)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setType(name);
}
PINVOKEABLE char* CSharp_ScrCcfg_getVersion_0_7(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getVersion());
}
PINVOKEABLE void CSharp_ScrCcfg_setVersion_1_1(void* thisObj, const char* name)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setVersion(name);
}
PINVOKEABLE char* CSharp_ScrCcfg_getDocFile_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getDocFile());
}
PINVOKEABLE void CSharp_ScrCcfg_setDocFile_1_1(void* thisObj, const char* name)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setDocFile(name);
}
PINVOKEABLE char* CSharp_ScrCcfg_getLoadfileExtension_0_7(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getLoadfileExtension());
}
PINVOKEABLE void CSharp_ScrCcfg_setLoadfileExtension_1_1(void* thisObj, const char* newVal)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setLoadfileExtension(newVal);
}
PINVOKEABLE bool CSharp_ScrCcfg_getUseStaticScheduling_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getUseStaticScheduling();
}
PINVOKEABLE void CSharp_ScrCcfg_setUseStaticScheduling_1_19(void* thisObj, int val)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setUseStaticScheduling(val);
}
PINVOKEABLE int CSharp_ScrCcfg_getLegacyMemories_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getLegacyMemories();
}
PINVOKEABLE void CSharp_ScrCcfg_setLegacyMemories_1_19(void* thisObj, int val)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setLegacyMemories(val);
}
PINVOKEABLE int CSharp_ScrCcfg_isStandAloneComp_0_7(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->isStandAloneComp();
}
PINVOKEABLE void CSharp_ScrCcfg_setIsStandAloneComp_1_19(void* thisObj, int val)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setIsStandAloneComp(val);
}
PINVOKEABLE int CSharp_ScrCcfg_isSubComponentFullName_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->isSubComponentFullName();
}
PINVOKEABLE void CSharp_ScrCcfg_setSubComponentFullName_1_19(void* thisObj, int val)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setSubComponentFullName(val);
}
PINVOKEABLE unsigned int CSharp_ScrCcfg_getNumSubComponents_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getNumSubComponents();
}
PINVOKEABLE unsigned int CSharp_ScrCcfg_getNumXtorInstances_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getNumXtorInstances();
}
PINVOKEABLE unsigned int CSharp_ScrCcfg_getNumESLPorts_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getNumESLPorts();
}
PINVOKEABLE unsigned int CSharp_ScrCcfg_getNumRTLPorts_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getNumRTLPorts();
}
PINVOKEABLE unsigned int CSharp_ScrCcfg_getNumParams_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getNumParams();
}
PINVOKEABLE unsigned int CSharp_ScrCcfg_getNumRegisters_0_35(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getNumRegisters();
}
PINVOKEABLE unsigned int CSharp_ScrCcfg_getNumGroups_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getNumGroups();
}
PINVOKEABLE unsigned int CSharp_ScrCcfg_getNumMemories_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getNumMemories();
}
PINVOKEABLE unsigned int CSharp_ScrCcfg_getNumCustomCodes_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getNumCustomCodes();
}
PINVOKEABLE CcfgEnum::CarbonCfgStatus CSharp_ScrCcfg_read_2_1(void* thisObj, CcfgEnum::CarbonCfgMode mode, const char* fileName)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->read(mode, fileName);
}
PINVOKEABLE CcfgEnum::CarbonCfgStatus CSharp_ScrCcfg_readNoCheck_2_1(void* thisObj, CcfgEnum::CarbonCfgMode mode, const char* fileName)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->readNoCheck(mode, fileName);
}
PINVOKEABLE CcfgEnum::CarbonCfgStatus CSharp_ScrCcfg_write_1_1(void* thisObj, const char* fileName)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->write(fileName);
}
PINVOKEABLE CcfgEnum::CarbonCfgStatus CSharp_ScrCcfg_write_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->write();
}
PINVOKEABLE CarbonCfgELFLoader* CSharp_ScrCcfg_getELFLoader_0_7(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getELFLoader();
}
PINVOKEABLE CarbonCfgProcInfo* CSharp_ScrCcfg_getProcInfo_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getProcInfo();
}
PINVOKEABLE ScrCcfg* CSharp_ScrCcfg_addSubComp_1_1(void* thisObj, const char* componentName)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->addSubComp(componentName);
}
PINVOKEABLE void CSharp_ScrCcfg_removeSubComp_1_3(void* thisObj, ScrCcfg* comp)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->removeSubComp(comp);
}
PINVOKEABLE void CSharp_ScrCcfg_removeSubComps_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->removeSubComps();
}
PINVOKEABLE void CSharp_ScrCcfg_removeRegisters_0_35(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->removeRegisters();
}
PINVOKEABLE void CSharp_ScrCcfg_removeMemories_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->removeMemories();
}
PINVOKEABLE void CSharp_ScrCcfg_removeCustomCodes_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->removeCustomCodes();
}
PINVOKEABLE void CSharp_ScrCcfg_removeXtorInstances_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->removeXtorInstances();
}
PINVOKEABLE void CSharp_ScrCcfg_removeParameters_0_35(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->removeParameters();
}
PINVOKEABLE void CSharp_ScrCcfg_removeGroups_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->removeGroups();
}
PINVOKEABLE ScrCcfg* CSharp_ScrCcfg_getSubComponent_1_19(void* thisObj, unsigned int i)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getSubComponent(i);
}
PINVOKEABLE char* CSharp_ScrCcfg_getUniqueRegName_1_1(void* thisObj, const char* baseName)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getUniqueRegName(baseName));
}
PINVOKEABLE char* CSharp_ScrCcfg_getUniqueGroupName_1_1(void* thisObj, const char* baseName)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getUniqueGroupName(baseName));
}
PINVOKEABLE char* CSharp_ScrCcfg_getUniqueESLName_1_1(void* thisObj, const char* baseName)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return EmbeddedMono::AllocString(obj->getUniqueESLName(baseName));
}
PINVOKEABLE unsigned int CSharp_ScrCcfg_numGroups_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->numGroups();
}
PINVOKEABLE CarbonCfgGroup* CSharp_ScrCcfg_getGroup_1_19(void* thisObj, unsigned int i)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getGroup(i);
}
PINVOKEABLE CarbonCfgGroup* CSharp_ScrCcfg_findGroup_1_1(void* thisObj, const char* groupName)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->findGroup(groupName);
}
PINVOKEABLE CarbonCfgGroup* CSharp_ScrCcfg_addGroup_1_1(void* thisObj, const char* groupName)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->addGroup(groupName);
}
PINVOKEABLE CarbonCfgXtorLib* CSharp_ScrCcfg_getXtorLib_0_39(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getXtorLib();
}
PINVOKEABLE CarbonCfgESLPort* CSharp_ScrCcfg_findESLPort_1_1(void* thisObj, const char* portName)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->findESLPort(portName);
}
PINVOKEABLE CarbonCfgRTLPort* CSharp_ScrCcfg_findRTLPort_1_1(void* thisObj, const char* portName)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->findRTLPort(portName);
}
PINVOKEABLE CarbonCfgRegister* CSharp_ScrCcfg_addRegister_7_3(void* thisObj, const char* debugName, CarbonCfgGroup* group, unsigned int width, bool bigEndian, CcfgEnum::CarbonCfgRadix radixVal, const char* commentStr, bool pcReg)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->addRegister(debugName, group, width, bigEndian, radixVal, commentStr, pcReg);
}
PINVOKEABLE CarbonCfgRegister* CSharp_ScrCcfg_addRegister_6_1(void* thisObj, const char* debugName, CarbonCfgGroup* group, unsigned int width, bool bigEndian, CcfgEnum::CarbonCfgRadix radixVal, const char* commentStr)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->addRegister(debugName, group, width, bigEndian, radixVal, commentStr);
}
PINVOKEABLE CarbonCfgCompCustomCode* CSharp_ScrCcfg_addCustomCode_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->addCustomCode();
}
PINVOKEABLE CarbonCfgCompCustomCode* CSharp_ScrCcfg_getCustomCode_1_19(void* thisObj, unsigned int i)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getCustomCode(i);
}
PINVOKEABLE void CSharp_ScrCcfg_removeCustomCode_1_3(void* thisObj, CarbonCfgCompCustomCode* customCode)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->removeCustomCode(customCode);
}
PINVOKEABLE CarbonCfgESLPort* CSharp_ScrCcfg_addESLPort_5_5(void* thisObj, CarbonCfgRTLPort* rtlPort, const char* name, CcfgEnum::CarbonCfgESLPortType ptype, const char* typeDef, CcfgEnum::CarbonCfgESLPortMode pmode)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->addESLPort(rtlPort, name, ptype, typeDef, pmode);
}
PINVOKEABLE CarbonCfgXtorParamInst* CSharp_ScrCcfg_addParam_6_1(void* thisObj, const char* Name, CcfgEnum::CarbonCfgParamDataType type, const char* Value, CcfgEnum::CarbonCfgParamFlag scope, const char* Description, const char* EnumChoices)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->addParam(Name, type, Value, scope, Description, EnumChoices);
}
PINVOKEABLE CarbonCfgXtorParamInst* CSharp_ScrCcfg_addParam_5_1(void* thisObj, const char* Name, CcfgEnum::CarbonCfgParamDataType type, const char* Value, CcfgEnum::CarbonCfgParamFlag scope, const char* Description)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->addParam(Name, type, Value, scope, Description);
}
PINVOKEABLE CarbonCfgXtorInstance* CSharp_ScrCcfg_addXtor_2_3(void* thisObj, const char* instName, CarbonCfgXtor* type)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->addXtor(instName, type);
}
PINVOKEABLE CarbonCfgRegister* CSharp_ScrCcfg_getRegister_1_19(void* thisObj, unsigned int i)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getRegister(i);
}
PINVOKEABLE CarbonCfgXtorInstance* CSharp_ScrCcfg_getXtorInstance_1_19(void* thisObj, unsigned int i)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getXtorInstance(i);
}
PINVOKEABLE CarbonCfgESLPort* CSharp_ScrCcfg_getESLPort_1_19(void* thisObj, unsigned int i)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getESLPort(i);
}
PINVOKEABLE CarbonCfgRTLPort* CSharp_ScrCcfg_getRTLPort_1_19(void* thisObj, unsigned int i)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getRTLPort(i);
}
PINVOKEABLE CarbonCfgXtorParamInst* CSharp_ScrCcfg_getParam_1_19(void* thisObj, unsigned int i)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getParam(i);
}
PINVOKEABLE CarbonCfgXtor* CSharp_ScrCcfg_findXtor_1_1(void* thisObj, const char* xtorName)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->findXtor(xtorName);
}
PINVOKEABLE CarbonCfgXtorInstance* CSharp_ScrCcfg_findXtorInstance_1_1(void* thisObj, const char* xtorName)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->findXtorInstance(xtorName);
}
PINVOKEABLE void CSharp_ScrCcfg_removeXtorInstance_1_3(void* thisObj, CarbonCfgXtorInstance* xtorInst)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->removeXtorInstance(xtorInst);
}
PINVOKEABLE CarbonCfgRegister* CSharp_ScrCcfg_findRegister_1_1(void* thisObj, const char* regName)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->findRegister(regName);
}
PINVOKEABLE void CSharp_ScrCcfg_removeRegister_1_3(void* thisObj, CarbonCfgRegister* reg)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->removeRegister(reg);
}
PINVOKEABLE void CSharp_ScrCcfg_removeMemory_1_3(void* thisObj, CarbonCfgMemory* mem)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->removeMemory(mem);
}
PINVOKEABLE void CSharp_ScrCcfg_disconnect_1_3(void* thisObj, CarbonCfgRTLConnection* conn)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->disconnect(conn);
}
PINVOKEABLE CarbonCfgMemory* CSharp_ScrCcfg_addMemory_3_19(void* thisObj, const char* name, unsigned int width, unsigned int maxAddr)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->addMemory(name, width, maxAddr);
}
PINVOKEABLE unsigned int CSharp_ScrCcfg_numMemories_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->numMemories();
}
PINVOKEABLE CarbonCfgMemory* CSharp_ScrCcfg_getMemory_1_19(void* thisObj, unsigned int i)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getMemory(i);
}
PINVOKEABLE CarbonCfgCadi* CSharp_ScrCcfg_getCadi_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getCadi();
}
PINVOKEABLE CarbonCfgCompCustomCode* CSharp_ScrCcfg_cloneCustomCode_1_3(void* thisObj, CarbonCfgCompCustomCode* cc)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->cloneCustomCode(cc);
}
PINVOKEABLE CarbonCfgMemory* CSharp_ScrCcfg_cloneMemory_1_3(void* thisObj, CarbonCfgMemory* cc)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->cloneMemory(cc);
}
PINVOKEABLE void CSharp_ScrCcfg_cloneCadi_1_3(void* thisObj, CarbonCfgCadi* cadi)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->cloneCadi(cadi);
}
PINVOKEABLE void CSharp_ScrCcfg_cloneProcInfo_1_3(void* thisObj, CarbonCfgProcInfo* procInfo)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->cloneProcInfo(procInfo);
}
PINVOKEABLE CarbonCfgRegister* CSharp_ScrCcfg_clone_2_3(void* thisObj, CarbonCfgGroup* group, CarbonCfgRegister* cc)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->clone(group, cc);
}
PINVOKEABLE void CSharp_ScrCcfg_setRequiresMkLibrary_1_3(void* thisObj, bool req)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setRequiresMkLibrary(req);
}
PINVOKEABLE bool CSharp_ScrCcfg_getRequiresMkLibrary_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getRequiresMkLibrary();
}
PINVOKEABLE void CSharp_ScrCcfg_setUseVersionedMkLibrary_1_3(void* thisObj, bool req)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    obj->setUseVersionedMkLibrary(req);
}
PINVOKEABLE bool CSharp_ScrCcfg_getUseVersionedMkLibrary_0_3(void* thisObj)
{
    ScrCcfg* obj = (ScrCcfg*)(thisObj);
    return obj->getUseVersionedMkLibrary();
}
