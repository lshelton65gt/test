using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgXtorInstance : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgXtorInstance(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgXtorInstance obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgXtorInstance()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorInstance_getClockMaster_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorInstance_getClockMaster_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorInstance_setClockMaster_1_3")]
    private extern static void CSharp_ProtoCarbonCfgXtorInstance_setClockMaster_1_3(HandleRef thisObj, IntPtr clockMaster);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorInstance_findParameter_1_1")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorInstance_findParameter_1_1(HandleRef thisObj, string paramName);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorInstance_hideParameter_1_1")]
    private extern static bool CSharp_ProtoCarbonCfgXtorInstance_hideParameter_1_1(HandleRef thisObj, string paramName);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorInstance_addConnection_2_3")]
    private extern static void CSharp_ProtoCarbonCfgXtorInstance_addConnection_2_3(HandleRef thisObj, uint i, IntPtr conn);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorInstance_getConnection_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorInstance_getConnection_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorInstance_name_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorInstance_name_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorInstance_numConnections_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgXtorInstance_numConnections_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorInstance_numParams_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgXtorInstance_numParams_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorInstance_setUseDebugAccess_1_3")]
    private extern static void CSharp_ProtoCarbonCfgXtorInstance_setUseDebugAccess_1_3(HandleRef thisObj, bool val);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorInstance_getXtor_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorInstance_getXtor_0_7(HandleRef thisObj);
    #endregion DllImports
    #region Properties
    public string Name
    {
      get
      {
        return name();
      }
    }
    public uint NumConnections
    {
      get
      {
        return numConnections();
      }
    }
    public uint NumParams
    {
      get
      {
        return numParams();
      }
    }
    public CarbonCfgXtorInstance ClockMaster
    {
      set
      {
        setClockMaster(value);
      }
      get
      {
        return getClockMaster();
      }
    }
    public CarbonCfgXtor Xtor
    {
      get
      {
        return getXtor();
      }
    }
    #endregion Properties
    #region Methods
    public CarbonCfgXtorInstance getClockMaster()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorInstance object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgXtorInstance_getClockMaster_0_7(objCPtr);
      CarbonCfgXtorInstance ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorInstance(cPtr, false);
      return ret;
    }

    public void setClockMaster(CarbonCfgXtorInstance clockMaster)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorInstance object");
      CSharp_ProtoCarbonCfgXtorInstance_setClockMaster_1_3(objCPtr, CarbonCfgXtorInstance.getCPtr(clockMaster).Handle);
    }

    public CarbonCfgXtorParamInst findParameter(string paramName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorInstance object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgXtorInstance_findParameter_1_1(objCPtr, paramName);
      CarbonCfgXtorParamInst ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorParamInst(cPtr, false);
      return ret;
    }

    public bool hideParameter(string paramName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorInstance object");
      return CSharp_ProtoCarbonCfgXtorInstance_hideParameter_1_1(objCPtr, paramName);
    }

    public void addConnection(uint i, CarbonCfgXtorConn conn)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorInstance object");
      CSharp_ProtoCarbonCfgXtorInstance_addConnection_2_3(objCPtr, i, CarbonCfgXtorConn.getCPtr(conn).Handle);
    }

    public CarbonCfgXtorConn getConnection(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorInstance object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgXtorInstance_getConnection_1_19(objCPtr, i);
      CarbonCfgXtorConn ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorConn(cPtr, false);
      return ret;
    }

    public string name()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorInstance object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgXtorInstance_name_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public uint numConnections()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorInstance object");
      return CSharp_ProtoCarbonCfgXtorInstance_numConnections_0_3(objCPtr);
    }

    public uint numParams()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorInstance object");
      return CSharp_ProtoCarbonCfgXtorInstance_numParams_0_3(objCPtr);
    }

    public void setUseDebugAccess(bool val)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorInstance object");
      CSharp_ProtoCarbonCfgXtorInstance_setUseDebugAccess_1_3(objCPtr, val);
    }

    public CarbonCfgXtor getXtor()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorInstance object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgXtorInstance_getXtor_0_7(objCPtr);
      CarbonCfgXtor ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtor(cPtr, false);
      return ret;
    }

    #endregion Methods

  }

}
