PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterField_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterField_getWidth_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    return obj->getWidth();
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterField_getHigh_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    return obj->getHigh();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterField_setHigh_1_19(void* thisObj, unsigned int nv)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    obj->setHigh(nv);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterField_getLow_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    return obj->getLow();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterField_setLow_1_19(void* thisObj, unsigned int nv)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    obj->setLow(nv);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgRegisterField_getName_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    return EmbeddedMono::AllocString(obj->getName());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterField_setName_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    obj->setName(newVal);
}
PINVOKEABLE CarbonCfgRegisterLoc* CSharp_ProtoCarbonCfgRegisterField_getLoc_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    return obj->getLoc();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterField_setLocReg_1_3(void* thisObj, CarbonCfgRegisterLocReg* newVal)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    obj->setLocReg(newVal);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterField_setLocConstant_1_3(void* thisObj, CarbonCfgRegisterLocConstant* newVal)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    obj->setLocConstant(newVal);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterField_setLocArray_1_3(void* thisObj, CarbonCfgRegisterLocArray* newVal)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    obj->setLocArray(newVal);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterField_setLocUser_1_3(void* thisObj, CarbonCfgRegisterLocUser* newVal)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    obj->setLocUser(newVal);
}
PINVOKEABLE CarbonCfgRegisterLocReg* CSharp_ProtoCarbonCfgRegisterField_getRegLoc_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    return obj->getRegLoc();
}
PINVOKEABLE CarbonCfgRegisterLocConstant* CSharp_ProtoCarbonCfgRegisterField_getConstantLoc_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    return obj->getConstantLoc();
}
PINVOKEABLE CarbonCfgRegisterLocArray* CSharp_ProtoCarbonCfgRegisterField_getArrayLoc_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    return obj->getArrayLoc();
}
PINVOKEABLE CarbonCfgRegisterLocUser* CSharp_ProtoCarbonCfgRegisterField_getUserLoc_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    return obj->getUserLoc();
}
PINVOKEABLE CcfgEnum::CarbonCfgRegAccessType CSharp_ProtoCarbonCfgRegisterField_getAccess_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    return obj->getAccess();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterField_setAccess_1_21(void* thisObj, CcfgEnum::CarbonCfgRegAccessType newVal)
{
    ProtoCarbonCfgRegisterField* obj = (ProtoCarbonCfgRegisterField*)(thisObj);
    obj->setAccess(newVal);
}
