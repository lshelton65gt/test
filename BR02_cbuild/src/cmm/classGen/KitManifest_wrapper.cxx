PINVOKEABLE void CSharp_KitManifest_deleteLater_0_7(void* thisObj)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE char* CSharp_KitManifest_getRelease_0_3(void* thisObj)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return EmbeddedMono::AllocString(obj->getRelease());
}
PINVOKEABLE char* CSharp_KitManifest_getSoftwareVersion_0_7(void* thisObj)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return EmbeddedMono::AllocString(obj->getSoftwareVersion());
}
PINVOKEABLE unsigned int CSharp_KitManifest_getID_0_7(void* thisObj)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return obj->getID();
}
PINVOKEABLE unsigned int CSharp_KitManifest_numRTLFiles_0_3(void* thisObj)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return obj->numRTLFiles();
}
PINVOKEABLE unsigned int CSharp_KitManifest_numFiles_0_3(void* thisObj)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return obj->numFiles();
}
PINVOKEABLE unsigned int CSharp_KitManifest_numUserFiles_0_3(void* thisObj)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return obj->numUserFiles();
}
PINVOKEABLE unsigned int CSharp_KitManifest_numCompilerSwitches_0_3(void* thisObj)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return obj->numCompilerSwitches();
}
PINVOKEABLE unsigned int CSharp_KitManifest_numModelKitFeatures_0_3(void* thisObj)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return obj->numModelKitFeatures();
}
PINVOKEABLE bool CSharp_KitManifest_decryptFile_1_1(void* thisObj, const char* inputFileName)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return obj->decryptFile(inputFileName);
}
PINVOKEABLE bool CSharp_KitManifest_encryptFile_1_1(void* thisObj, const char* inputFileName)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return obj->encryptFile(inputFileName);
}
PINVOKEABLE char* CSharp_KitManifest_encryptBuffer_1_1(void* thisObj, const char* buffer)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return EmbeddedMono::AllocString(obj->encryptBuffer(buffer));
}
PINVOKEABLE char* CSharp_KitManifest_decryptBuffer_1_1(void* thisObj, const char* buffer)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return EmbeddedMono::AllocString(obj->decryptBuffer(buffer));
}
PINVOKEABLE char* CSharp_KitManifest_getCompilerSwitch_1_19(void* thisObj, unsigned int index)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return EmbeddedMono::AllocString(obj->getCompilerSwitch(index));
}
PINVOKEABLE KitFile* CSharp_KitManifest_getUserFile_1_19(void* thisObj, unsigned int index)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return obj->getUserFile(index);
}
PINVOKEABLE KitFile* CSharp_KitManifest_getRTLFile_1_19(void* thisObj, unsigned int index)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return obj->getRTLFile(index);
}
PINVOKEABLE KitFile* CSharp_KitManifest_getFile_1_19(void* thisObj, unsigned int index)
{
    KitManifest* obj = (KitManifest*)(thisObj);
    return obj->getFile(index);
}
