using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgRegister : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgRegister(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgRegister obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgRegister()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_addField_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRegister_addField_1_3(HandleRef thisObj, IntPtr field);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_removeField_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRegister_removeField_1_3(HandleRef thisObj, IntPtr field);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_getRadix_0_7")]
    private extern static int CSharp_ProtoCarbonCfgRegister_getRadix_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_setRadix_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRegister_setRadix_1_3(HandleRef thisObj, int newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_getField_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegister_getField_1_19(HandleRef thisObj, uint index);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_numFields_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgRegister_numFields_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_getGroupName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegister_getGroupName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_getPort_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegister_getPort_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_setPort_1_1")]
    private extern static void CSharp_ProtoCarbonCfgRegister_setPort_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_getBigEndian_0_7")]
    private extern static bool CSharp_ProtoCarbonCfgRegister_getBigEndian_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_setBigEndian_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRegister_setBigEndian_1_3(HandleRef thisObj, bool nv);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_getOffset_0_7")]
    private extern static uint CSharp_ProtoCarbonCfgRegister_getOffset_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_setOffset_1_19")]
    private extern static void CSharp_ProtoCarbonCfgRegister_setOffset_1_19(HandleRef thisObj, uint nv);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_getComment_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegister_getComment_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_setComment_1_1")]
    private extern static void CSharp_ProtoCarbonCfgRegister_setComment_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_getWidth_0_7")]
    private extern static uint CSharp_ProtoCarbonCfgRegister_getWidth_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_setWidth_1_19")]
    private extern static void CSharp_ProtoCarbonCfgRegister_setWidth_1_19(HandleRef thisObj, uint nv);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_getName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegister_getName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_numCustomCodes_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgRegister_numCustomCodes_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_addCustomCode_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegister_addCustomCode_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_getCustomCode_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegister_getCustomCode_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegister_removeCustomCode_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRegister_removeCustomCode_1_3(HandleRef thisObj, IntPtr customCode);
    #endregion DllImports
    #region Properties
    public string Name
    {
      get
      {
        return getName();
      }
    }
    public uint Width
    {
      set
      {
        setWidth(value);
      }
      get
      {
        return getWidth();
      }
    }
    public string Comment
    {
      set
      {
        setComment(value);
      }
      get
      {
        return getComment();
      }
    }
    public uint Offset
    {
      set
      {
        setOffset(value);
      }
      get
      {
        return getOffset();
      }
    }
    public bool BigEndian
    {
      set
      {
        setBigEndian(value);
      }
      get
      {
        return getBigEndian();
      }
    }
    public string Port
    {
      set
      {
        setPort(value);
      }
      get
      {
        return getPort();
      }
    }
    public uint NumFields
    {
      get
      {
        return numFields();
      }
    }
    public string GroupName
    {
      get
      {
        return getGroupName();
      }
    }
    public CcfgEnum.CarbonCfgRadix Radix
    {
      set
      {
        setRadix(value);
      }
      get
      {
        return getRadix();
      }
    }
    public uint NumCustomCodes
    {
      get
      {
        return numCustomCodes();
      }
    }
    #endregion Properties
    #region Methods
    public void addField(CarbonCfgRegisterField field)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      CSharp_ProtoCarbonCfgRegister_addField_1_3(objCPtr, CarbonCfgRegisterField.getCPtr(field).Handle);
    }

    public void removeField(CarbonCfgRegisterField field)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      CSharp_ProtoCarbonCfgRegister_removeField_1_3(objCPtr, CarbonCfgRegisterField.getCPtr(field).Handle);
    }

    public CcfgEnum.CarbonCfgRadix getRadix()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      return (CcfgEnum.CarbonCfgRadix)CSharp_ProtoCarbonCfgRegister_getRadix_0_7(objCPtr);
    }

    public void setRadix(CcfgEnum.CarbonCfgRadix newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      CSharp_ProtoCarbonCfgRegister_setRadix_1_3(objCPtr, (int)newVal);
    }

    public CarbonCfgRegisterField getField(uint index)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRegister_getField_1_19(objCPtr, index);
      CarbonCfgRegisterField ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegisterField(cPtr, false);
      return ret;
    }

    public uint numFields()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      return CSharp_ProtoCarbonCfgRegister_numFields_0_3(objCPtr);
    }

    public string getGroupName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgRegister_getGroupName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getPort()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgRegister_getPort_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setPort(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      CSharp_ProtoCarbonCfgRegister_setPort_1_1(objCPtr, newVal);
    }

    public bool getBigEndian()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      return CSharp_ProtoCarbonCfgRegister_getBigEndian_0_7(objCPtr);
    }

    public void setBigEndian(bool nv)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      CSharp_ProtoCarbonCfgRegister_setBigEndian_1_3(objCPtr, nv);
    }

    public uint getOffset()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      return CSharp_ProtoCarbonCfgRegister_getOffset_0_7(objCPtr);
    }

    public void setOffset(uint nv)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      CSharp_ProtoCarbonCfgRegister_setOffset_1_19(objCPtr, nv);
    }

    public string getComment()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgRegister_getComment_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setComment(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      CSharp_ProtoCarbonCfgRegister_setComment_1_1(objCPtr, newVal);
    }

    public uint getWidth()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      return CSharp_ProtoCarbonCfgRegister_getWidth_0_7(objCPtr);
    }

    public void setWidth(uint nv)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      CSharp_ProtoCarbonCfgRegister_setWidth_1_19(objCPtr, nv);
    }

    public string getName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgRegister_getName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public uint numCustomCodes()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      return CSharp_ProtoCarbonCfgRegister_numCustomCodes_0_3(objCPtr);
    }

    public CarbonCfgRegCustomCode addCustomCode()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRegister_addCustomCode_0_3(objCPtr);
      CarbonCfgRegCustomCode ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegCustomCode(cPtr, false);
      return ret;
    }

    public CarbonCfgRegCustomCode getCustomCode(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRegister_getCustomCode_1_19(objCPtr, i);
      CarbonCfgRegCustomCode ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRegCustomCode(cPtr, false);
      return ret;
    }

    public void removeCustomCode(CarbonCfgRegCustomCode customCode)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegister object");
      CSharp_ProtoCarbonCfgRegister_removeCustomCode_1_3(objCPtr, CarbonCfgRegCustomCode.getCPtr(customCode).Handle);
    }

    #endregion Methods

  }

}
