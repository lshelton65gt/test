using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgMemoryCustomCode : CarbonCfgCustomCode, IDisposable
  {
    private HandleRef objCPtr;
    internal CarbonCfgMemoryCustomCode(IntPtr cPtr, bool cMemoryOwn) : base(CSharp_ProtoCarbonCfgMemoryCustomCode_UpCast(cPtr), cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgMemoryCustomCode obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgMemoryCustomCode()
    {
      Dispose();
    }
    public override void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryCustomCode_getSection_0_7")]
    private extern static int CSharp_ProtoCarbonCfgMemoryCustomCode_getSection_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryCustomCode_setSection_1_3")]
    private extern static void CSharp_ProtoCarbonCfgMemoryCustomCode_setSection_1_3(HandleRef thisObj, int section);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryCustomCode_UpCast")]
    private static extern IntPtr CSharp_ProtoCarbonCfgMemoryCustomCode_UpCast(IntPtr obj);
    #endregion DllImports
    #region Properties
    public CcfgEnum.CarbonCfgMemoryCustomCodeSection Section
    {
      set
      {
        setSection(value);
      }
      get
      {
        return getSection();
      }
    }
    #endregion Properties
    #region Methods
    public CcfgEnum.CarbonCfgMemoryCustomCodeSection getSection()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryCustomCode object");
      return (CcfgEnum.CarbonCfgMemoryCustomCodeSection)CSharp_ProtoCarbonCfgMemoryCustomCode_getSection_0_7(objCPtr);
    }

    public void setSection(CcfgEnum.CarbonCfgMemoryCustomCodeSection section)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryCustomCode object");
      CSharp_ProtoCarbonCfgMemoryCustomCode_setSection_1_3(objCPtr, (int)section);
    }

    #endregion Methods

  }

}
