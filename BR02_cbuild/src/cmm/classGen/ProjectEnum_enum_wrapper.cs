using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class ProjectEnum
  {
    public enum ComponentType
    {
      UnknownComponent = 0,
      SoCDesigner = 1,
      CoWare = 2,
      ModelValidation = 3,
      SystemC = 4,
      Packaging = 5
    }
  }
}
