using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgProcInfo : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgProcInfo(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgProcInfo obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgProcInfo()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_getIsProcessor_0_7")]
    private extern static bool CSharp_ProtoCarbonCfgProcInfo_getIsProcessor_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_setIsProcessor_1_3")]
    private extern static void CSharp_ProtoCarbonCfgProcInfo_setIsProcessor_1_3(HandleRef thisObj, bool newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_getDebuggerName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgProcInfo_getDebuggerName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_setDebuggerName_1_1")]
    private extern static void CSharp_ProtoCarbonCfgProcInfo_setDebuggerName_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_getPipeStages_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgProcInfo_getPipeStages_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_setPipeStages_1_19")]
    private extern static void CSharp_ProtoCarbonCfgProcInfo_setPipeStages_1_19(HandleRef thisObj, uint newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_getHwThreads_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgProcInfo_getHwThreads_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_setHwThreads_1_19")]
    private extern static void CSharp_ProtoCarbonCfgProcInfo_setHwThreads_1_19(HandleRef thisObj, uint newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_getNumProcessorOptions_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgProcInfo_getNumProcessorOptions_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_getProcessorOption_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgProcInfo_getProcessorOption_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_addProcessorOption_1_1")]
    private extern static void CSharp_ProtoCarbonCfgProcInfo_addProcessorOption_1_1(HandleRef thisObj, string newOption);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_getPCRegGroupName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgProcInfo_getPCRegGroupName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_getPCRegName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgProcInfo_getPCRegName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_getExtendedFeaturesRegGroupName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgProcInfo_getExtendedFeaturesRegGroupName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_getExtendedFeaturesRegName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgProcInfo_getExtendedFeaturesRegName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_getTargetName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgProcInfo_getTargetName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_setPCRegName_2_1")]
    private extern static void CSharp_ProtoCarbonCfgProcInfo_setPCRegName_2_1(HandleRef thisObj, string newGroupName, string newRegName);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_setExtendedFeaturesRegName_2_1")]
    private extern static void CSharp_ProtoCarbonCfgProcInfo_setExtendedFeaturesRegName_2_1(HandleRef thisObj, string newGroupName, string newRegName);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_setTargetName_1_1")]
    private extern static void CSharp_ProtoCarbonCfgProcInfo_setTargetName_1_1(HandleRef thisObj, string newTargetName);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_getDebuggablePoint_0_7")]
    private extern static bool CSharp_ProtoCarbonCfgProcInfo_getDebuggablePoint_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgProcInfo_setDebuggablePoint_1_3")]
    private extern static void CSharp_ProtoCarbonCfgProcInfo_setDebuggablePoint_1_3(HandleRef thisObj, bool newVal);
    #endregion DllImports
    #region Properties
    public string DebuggerName
    {
      set
      {
        setDebuggerName(value);
      }
      get
      {
        return getDebuggerName();
      }
    }
    public uint PipeStages
    {
      set
      {
        setPipeStages(value);
      }
      get
      {
        return getPipeStages();
      }
    }
    public uint HwThreads
    {
      set
      {
        setHwThreads(value);
      }
      get
      {
        return getHwThreads();
      }
    }
    public uint NumProcessorOptions
    {
      get
      {
        return getNumProcessorOptions();
      }
    }
    public string ExtendedFeaturesRegGroupName
    {
      get
      {
        return getExtendedFeaturesRegGroupName();
      }
    }
    public string ExtendedFeaturesRegName
    {
      get
      {
        return getExtendedFeaturesRegName();
      }
    }
    public string TargetName
    {
      set
      {
        setTargetName(value);
      }
      get
      {
        return getTargetName();
      }
    }
    public bool DebuggablePoint
    {
      set
      {
        setDebuggablePoint(value);
      }
      get
      {
        return getDebuggablePoint();
      }
    }
    #endregion Properties
    #region Methods
    public bool getIsProcessor()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      return CSharp_ProtoCarbonCfgProcInfo_getIsProcessor_0_7(objCPtr);
    }

    public void setIsProcessor(bool newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      CSharp_ProtoCarbonCfgProcInfo_setIsProcessor_1_3(objCPtr, newVal);
    }

    public string getDebuggerName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgProcInfo_getDebuggerName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setDebuggerName(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      CSharp_ProtoCarbonCfgProcInfo_setDebuggerName_1_1(objCPtr, newVal);
    }

    public uint getPipeStages()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      return CSharp_ProtoCarbonCfgProcInfo_getPipeStages_0_3(objCPtr);
    }

    public void setPipeStages(uint newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      CSharp_ProtoCarbonCfgProcInfo_setPipeStages_1_19(objCPtr, newVal);
    }

    public uint getHwThreads()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      return CSharp_ProtoCarbonCfgProcInfo_getHwThreads_0_3(objCPtr);
    }

    public void setHwThreads(uint newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      CSharp_ProtoCarbonCfgProcInfo_setHwThreads_1_19(objCPtr, newVal);
    }

    public uint getNumProcessorOptions()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      return CSharp_ProtoCarbonCfgProcInfo_getNumProcessorOptions_0_3(objCPtr);
    }

    public string getProcessorOption(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgProcInfo_getProcessorOption_1_19(objCPtr, i);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void addProcessorOption(string newOption)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      CSharp_ProtoCarbonCfgProcInfo_addProcessorOption_1_1(objCPtr, newOption);
    }

    public string getPCRegGroupName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgProcInfo_getPCRegGroupName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getPCRegName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgProcInfo_getPCRegName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getExtendedFeaturesRegGroupName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgProcInfo_getExtendedFeaturesRegGroupName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getExtendedFeaturesRegName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgProcInfo_getExtendedFeaturesRegName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getTargetName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgProcInfo_getTargetName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setPCRegName(string newGroupName, string newRegName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      CSharp_ProtoCarbonCfgProcInfo_setPCRegName_2_1(objCPtr, newGroupName, newRegName);
    }

    public void setExtendedFeaturesRegName(string newGroupName, string newRegName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      CSharp_ProtoCarbonCfgProcInfo_setExtendedFeaturesRegName_2_1(objCPtr, newGroupName, newRegName);
    }

    public void setTargetName(string newTargetName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      CSharp_ProtoCarbonCfgProcInfo_setTargetName_1_1(objCPtr, newTargetName);
    }

    public bool getDebuggablePoint()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      return CSharp_ProtoCarbonCfgProcInfo_getDebuggablePoint_0_7(objCPtr);
    }

    public void setDebuggablePoint(bool newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgProcInfo object");
      CSharp_ProtoCarbonCfgProcInfo_setDebuggablePoint_1_3(objCPtr, newVal);
    }

    #endregion Methods

  }

}
