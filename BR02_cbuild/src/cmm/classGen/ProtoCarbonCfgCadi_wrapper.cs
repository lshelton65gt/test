using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgCadi : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgCadi(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgCadi obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgCadi()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCadi_getDisassemblerName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgCadi_getDisassemblerName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCadi_setDisassemblerName_1_1")]
    private extern static void CSharp_ProtoCarbonCfgCadi_setDisassemblerName_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCadi_numCustomCodes_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgCadi_numCustomCodes_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCadi_addCustomCode_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgCadi_addCustomCode_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCadi_getCustomCode_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgCadi_getCustomCode_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgCadi_removeCustomCode_1_3")]
    private extern static void CSharp_ProtoCarbonCfgCadi_removeCustomCode_1_3(HandleRef thisObj, IntPtr customCode);
    #endregion DllImports
    #region Properties
    public string DisassemblerName
    {
      set
      {
        setDisassemblerName(value);
      }
      get
      {
        return getDisassemblerName();
      }
    }
    public uint NumCustomCodes
    {
      get
      {
        return numCustomCodes();
      }
    }
    #endregion Properties
    #region Methods
    public string getDisassemblerName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgCadi object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgCadi_getDisassemblerName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setDisassemblerName(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgCadi object");
      CSharp_ProtoCarbonCfgCadi_setDisassemblerName_1_1(objCPtr, newVal);
    }

    public uint numCustomCodes()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgCadi object");
      return CSharp_ProtoCarbonCfgCadi_numCustomCodes_0_3(objCPtr);
    }

    public CarbonCfgCadiCustomCode addCustomCode()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgCadi object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgCadi_addCustomCode_0_3(objCPtr);
      CarbonCfgCadiCustomCode ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgCadiCustomCode(cPtr, false);
      return ret;
    }

    public CarbonCfgCadiCustomCode getCustomCode(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgCadi object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgCadi_getCustomCode_1_19(objCPtr, i);
      CarbonCfgCadiCustomCode ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgCadiCustomCode(cPtr, false);
      return ret;
    }

    public void removeCustomCode(CarbonCfgCadiCustomCode customCode)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgCadi object");
      CSharp_ProtoCarbonCfgCadi_removeCustomCode_1_3(objCPtr, CarbonCfgCadiCustomCode.getCPtr(customCode).Handle);
    }

    #endregion Methods

  }

}
