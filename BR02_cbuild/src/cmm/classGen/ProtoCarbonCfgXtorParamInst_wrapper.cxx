PINVOKEABLE void CSharp_ProtoCarbonCfgXtorParamInst_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgXtorParamInst* obj = (ProtoCarbonCfgXtorParamInst*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CarbonCfgRTLPort* CSharp_ProtoCarbonCfgXtorParamInst_getRTLPort_1_19(void* thisObj, unsigned int i)
{
    ProtoCarbonCfgXtorParamInst* obj = (ProtoCarbonCfgXtorParamInst*)(thisObj);
    return obj->getRTLPort(i);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgXtorParamInst_numRTLPorts_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorParamInst* obj = (ProtoCarbonCfgXtorParamInst*)(thisObj);
    return obj->numRTLPorts();
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgXtorParamInst_getNumEnumChoices_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorParamInst* obj = (ProtoCarbonCfgXtorParamInst*)(thisObj);
    return obj->getNumEnumChoices();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgXtorParamInst_getDefaultValue_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorParamInst* obj = (ProtoCarbonCfgXtorParamInst*)(thisObj);
    return EmbeddedMono::AllocString(obj->getDefaultValue());
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgXtorParamInst_getValue_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorParamInst* obj = (ProtoCarbonCfgXtorParamInst*)(thisObj);
    return EmbeddedMono::AllocString(obj->getValue());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgXtorParamInst_putValue_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgXtorParamInst* obj = (ProtoCarbonCfgXtorParamInst*)(thisObj);
    obj->putValue(newVal);
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgXtorParamInst_removeEnumChoice_1_1(void* thisObj, const char* choice)
{
    ProtoCarbonCfgXtorParamInst* obj = (ProtoCarbonCfgXtorParamInst*)(thisObj);
    return obj->removeEnumChoice(choice);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgXtorParamInst_getEnumChoice_1_19(void* thisObj, unsigned int i)
{
    ProtoCarbonCfgXtorParamInst* obj = (ProtoCarbonCfgXtorParamInst*)(thisObj);
    return EmbeddedMono::AllocString(obj->getEnumChoice(i));
}
PINVOKEABLE CarbonCfgXtorInstance* CSharp_ProtoCarbonCfgXtorParamInst_getInstance_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorParamInst* obj = (ProtoCarbonCfgXtorParamInst*)(thisObj);
    return obj->getInstance();
}
PINVOKEABLE CarbonCfgXtorParam* CSharp_ProtoCarbonCfgXtorParamInst_getParam_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorParamInst* obj = (ProtoCarbonCfgXtorParamInst*)(thisObj);
    return obj->getParam();
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgXtorParamInst_getHidden_0_7(void* thisObj)
{
    ProtoCarbonCfgXtorParamInst* obj = (ProtoCarbonCfgXtorParamInst*)(thisObj);
    return obj->getHidden();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgXtorParamInst_putHidden_1_3(void* thisObj, bool hidden)
{
    ProtoCarbonCfgXtorParamInst* obj = (ProtoCarbonCfgXtorParamInst*)(thisObj);
    obj->putHidden(hidden);
}
