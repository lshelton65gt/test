PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryBlockCustomCode_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryBlockCustomCode* obj = (ProtoCarbonCfgMemoryBlockCustomCode*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CcfgEnum::CarbonCfgCustomCodePosition CSharp_ProtoCarbonCfgMemoryBlockCustomCode_getPosition_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryBlockCustomCode* obj = (ProtoCarbonCfgMemoryBlockCustomCode*)(thisObj);
    return obj->getPosition();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryBlockCustomCode_setPosition_1_3(void* thisObj, CcfgEnum::CarbonCfgCustomCodePosition position)
{
    ProtoCarbonCfgMemoryBlockCustomCode* obj = (ProtoCarbonCfgMemoryBlockCustomCode*)(thisObj);
    obj->setPosition(position);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgMemoryBlockCustomCode_getCode_0_3(void* thisObj)
{
    ProtoCarbonCfgMemoryBlockCustomCode* obj = (ProtoCarbonCfgMemoryBlockCustomCode*)(thisObj);
    return EmbeddedMono::AllocString(obj->getCode());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryBlockCustomCode_setCode_1_1(void* thisObj, const char* code)
{
    ProtoCarbonCfgMemoryBlockCustomCode* obj = (ProtoCarbonCfgMemoryBlockCustomCode*)(thisObj);
    obj->setCode(code);
}
PINVOKEABLE CcfgEnum::CarbonCfgMemoryBlockCustomCodeSection CSharp_ProtoCarbonCfgMemoryBlockCustomCode_getSection_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryBlockCustomCode* obj = (ProtoCarbonCfgMemoryBlockCustomCode*)(thisObj);
    return obj->getSection();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryBlockCustomCode_setSection_1_3(void* thisObj, CcfgEnum::CarbonCfgMemoryBlockCustomCodeSection section)
{
    ProtoCarbonCfgMemoryBlockCustomCode* obj = (ProtoCarbonCfgMemoryBlockCustomCode*)(thisObj);
    obj->setSection(section);
}
PINVOKEABLE ProtoCarbonCfgCustomCode* CSharp_ProtoCarbonCfgMemoryBlockCustomCode_UpCast(ProtoCarbonCfgMemoryBlockCustomCode* obj)
{
  return (ProtoCarbonCfgCustomCode*)(obj);
}
