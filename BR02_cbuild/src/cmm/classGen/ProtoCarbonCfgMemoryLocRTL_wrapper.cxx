PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocRTL_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CcfgEnum::CarbonCfgMemLocType CSharp_ProtoCarbonCfgMemoryLocRTL_getType_0_35(void* thisObj)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    return obj->getType();
}
PINVOKEABLE unsigned long long CSharp_ProtoCarbonCfgMemoryLocRTL_getDisplayStartWordOffset_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    return obj->getDisplayStartWordOffset();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocRTL_setDisplayStartWordOffset_1_7(void* thisObj, unsigned long long newVal)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    obj->setDisplayStartWordOffset(newVal);
}
PINVOKEABLE unsigned long long CSharp_ProtoCarbonCfgMemoryLocRTL_getDisplayEndWordOffset_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    return obj->getDisplayEndWordOffset();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocRTL_setDisplayEndWordOffset_1_7(void* thisObj, unsigned long long newVal)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    obj->setDisplayEndWordOffset(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemoryLocRTL_getDisplayMsb_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    return obj->getDisplayMsb();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocRTL_setDisplayMsb_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    obj->setDisplayMsb(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemoryLocRTL_getDisplayLsb_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    return obj->getDisplayLsb();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocRTL_setDisplayLsb_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    obj->setDisplayLsb(newVal);
}
PINVOKEABLE CarbonCfgMemoryLocRTL* CSharp_ProtoCarbonCfgMemoryLocRTL_castRTL_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    return obj->castRTL();
}
PINVOKEABLE CarbonCfgMemoryLocPort* CSharp_ProtoCarbonCfgMemoryLocRTL_castPort_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    return obj->castPort();
}
PINVOKEABLE CarbonCfgMemoryLocUser* CSharp_ProtoCarbonCfgMemoryLocRTL_castUser_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    return obj->castUser();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgMemoryLocRTL_getPath_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    return EmbeddedMono::AllocString(obj->getPath());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocRTL_setPath_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    obj->setPath(newVal);
}
PINVOKEABLE unsigned long long CSharp_ProtoCarbonCfgMemoryLocRTL_getStartWordOffset_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    return obj->getStartWordOffset();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocRTL_setStartWordOffset_1_7(void* thisObj, unsigned long long newVal)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    obj->setStartWordOffset(newVal);
}
PINVOKEABLE unsigned long long CSharp_ProtoCarbonCfgMemoryLocRTL_getEndWordOffset_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    return obj->getEndWordOffset();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocRTL_setEndWordOffset_1_7(void* thisObj, unsigned long long newVal)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    obj->setEndWordOffset(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemoryLocRTL_getMsb_0_39(void* thisObj)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    return obj->getMsb();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocRTL_setMsb_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    obj->setMsb(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemoryLocRTL_getLsb_0_39(void* thisObj)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    return obj->getLsb();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLocRTL_setLsb_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    obj->setLsb(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemoryLocRTL_getBitWidth_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLocRTL* obj = (ProtoCarbonCfgMemoryLocRTL*)(thisObj);
    return obj->getBitWidth();
}
PINVOKEABLE ProtoCarbonCfgMemoryLoc* CSharp_ProtoCarbonCfgMemoryLocRTL_UpCast(ProtoCarbonCfgMemoryLocRTL* obj)
{
  return (ProtoCarbonCfgMemoryLoc*)(obj);
}
