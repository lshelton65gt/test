using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgMemoryLocUser : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgMemoryLocUser(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgMemoryLocUser obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgMemoryLocUser()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocUser_getName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemoryLocUser_getName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocUser_setName_1_1")]
    private extern static void CSharp_ProtoCarbonCfgMemoryLocUser_setName_1_1(HandleRef thisObj, string _name);
    #endregion DllImports
    #region Properties
    public string Name
    {
      set
      {
        setName(value);
      }
      get
      {
        return getName();
      }
    }
    #endregion Properties
    #region Methods
    public string getName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocUser object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgMemoryLocUser_getName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setName(string _name)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocUser object");
      CSharp_ProtoCarbonCfgMemoryLocUser_setName_1_1(objCPtr, _name);
    }

    #endregion Methods

  }

}
