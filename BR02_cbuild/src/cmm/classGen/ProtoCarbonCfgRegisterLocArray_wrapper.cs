using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgRegisterLocArray : CarbonCfgRegisterLocRTL, IDisposable
  {
    private HandleRef objCPtr;
    internal CarbonCfgRegisterLocArray(IntPtr cPtr, bool cMemoryOwn) : base(CSharp_ProtoCarbonCfgRegisterLocArray_UpCast(cPtr), cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgRegisterLocArray obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgRegisterLocArray()
    {
      Dispose();
    }
    public override void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocArray_getIndex_0_7")]
    private extern static uint CSharp_ProtoCarbonCfgRegisterLocArray_getIndex_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocArray_setIndex_1_19")]
    private extern static void CSharp_ProtoCarbonCfgRegisterLocArray_setIndex_1_19(HandleRef thisObj, uint newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocArray_UpCast")]
    private static extern IntPtr CSharp_ProtoCarbonCfgRegisterLocArray_UpCast(IntPtr obj);
    #endregion DllImports
    #region Properties
    public uint Index
    {
      set
      {
        setIndex(value);
      }
      get
      {
        return getIndex();
      }
    }
    #endregion Properties
    #region Methods
    public uint getIndex()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocArray object");
      return CSharp_ProtoCarbonCfgRegisterLocArray_getIndex_0_7(objCPtr);
    }

    public void setIndex(uint newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocArray object");
      CSharp_ProtoCarbonCfgRegisterLocArray_setIndex_1_19(objCPtr, newVal);
    }

    #endregion Methods

  }

}
