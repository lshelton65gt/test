using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgMemoryLocRTL : CarbonCfgMemoryLoc, IDisposable
  {
    private HandleRef objCPtr;
    internal CarbonCfgMemoryLocRTL(IntPtr cPtr, bool cMemoryOwn) : base(CSharp_ProtoCarbonCfgMemoryLocRTL_UpCast(cPtr), cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgMemoryLocRTL obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgMemoryLocRTL()
    {
      Dispose();
    }
    public override void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocRTL_getPath_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemoryLocRTL_getPath_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocRTL_setPath_1_1")]
    private extern static void CSharp_ProtoCarbonCfgMemoryLocRTL_setPath_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocRTL_getStartWordOffset_0_7")]
    private extern static ulong CSharp_ProtoCarbonCfgMemoryLocRTL_getStartWordOffset_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocRTL_setStartWordOffset_1_7")]
    private extern static void CSharp_ProtoCarbonCfgMemoryLocRTL_setStartWordOffset_1_7(HandleRef thisObj, ulong newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocRTL_getEndWordOffset_0_7")]
    private extern static ulong CSharp_ProtoCarbonCfgMemoryLocRTL_getEndWordOffset_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocRTL_setEndWordOffset_1_7")]
    private extern static void CSharp_ProtoCarbonCfgMemoryLocRTL_setEndWordOffset_1_7(HandleRef thisObj, ulong newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocRTL_getMsb_0_39")]
    private extern static uint CSharp_ProtoCarbonCfgMemoryLocRTL_getMsb_0_39(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocRTL_setMsb_1_19")]
    private extern static void CSharp_ProtoCarbonCfgMemoryLocRTL_setMsb_1_19(HandleRef thisObj, uint newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocRTL_getLsb_0_39")]
    private extern static uint CSharp_ProtoCarbonCfgMemoryLocRTL_getLsb_0_39(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocRTL_setLsb_1_19")]
    private extern static void CSharp_ProtoCarbonCfgMemoryLocRTL_setLsb_1_19(HandleRef thisObj, uint newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocRTL_getBitWidth_0_7")]
    private extern static uint CSharp_ProtoCarbonCfgMemoryLocRTL_getBitWidth_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocRTL_UpCast")]
    private static extern IntPtr CSharp_ProtoCarbonCfgMemoryLocRTL_UpCast(IntPtr obj);
    #endregion DllImports
    #region Properties
    public string Path
    {
      set
      {
        setPath(value);
      }
      get
      {
        return getPath();
      }
    }
    public ulong StartWordOffset
    {
      set
      {
        setStartWordOffset(value);
      }
      get
      {
        return getStartWordOffset();
      }
    }
    public ulong EndWordOffset
    {
      set
      {
        setEndWordOffset(value);
      }
      get
      {
        return getEndWordOffset();
      }
    }
    public uint BitWidth
    {
      get
      {
        return getBitWidth();
      }
    }
    #endregion Properties
    #region Methods
    public string getPath()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocRTL object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgMemoryLocRTL_getPath_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setPath(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocRTL object");
      CSharp_ProtoCarbonCfgMemoryLocRTL_setPath_1_1(objCPtr, newVal);
    }

    public ulong getStartWordOffset()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocRTL object");
      return CSharp_ProtoCarbonCfgMemoryLocRTL_getStartWordOffset_0_7(objCPtr);
    }

    public void setStartWordOffset(ulong newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocRTL object");
      CSharp_ProtoCarbonCfgMemoryLocRTL_setStartWordOffset_1_7(objCPtr, newVal);
    }

    public ulong getEndWordOffset()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocRTL object");
      return CSharp_ProtoCarbonCfgMemoryLocRTL_getEndWordOffset_0_7(objCPtr);
    }

    public void setEndWordOffset(ulong newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocRTL object");
      CSharp_ProtoCarbonCfgMemoryLocRTL_setEndWordOffset_1_7(objCPtr, newVal);
    }

    public uint getMsb()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocRTL object");
      return CSharp_ProtoCarbonCfgMemoryLocRTL_getMsb_0_39(objCPtr);
    }

    public void setMsb(uint newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocRTL object");
      CSharp_ProtoCarbonCfgMemoryLocRTL_setMsb_1_19(objCPtr, newVal);
    }

    public uint getLsb()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocRTL object");
      return CSharp_ProtoCarbonCfgMemoryLocRTL_getLsb_0_39(objCPtr);
    }

    public void setLsb(uint newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocRTL object");
      CSharp_ProtoCarbonCfgMemoryLocRTL_setLsb_1_19(objCPtr, newVal);
    }

    public uint getBitWidth()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocRTL object");
      return CSharp_ProtoCarbonCfgMemoryLocRTL_getBitWidth_0_7(objCPtr);
    }

    #endregion Methods

  }

}
