using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class Application : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal Application(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(Application obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~Application()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_getIsUnix_0_7")]
    private extern static bool CSharp_ScrApplicationProto_getIsUnix_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_getWorkingDirectory_0_3")]
    private extern static IntPtr CSharp_ScrApplicationProto_getWorkingDirectory_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_setWorkingDirectory_1_1")]
    private extern static void CSharp_ScrApplicationProto_setWorkingDirectory_1_1(HandleRef thisObj, string dir);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_checkArgument_1_1")]
    private extern static bool CSharp_ScrApplicationProto_checkArgument_1_1(HandleRef thisObj, string argName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_argumentValue_1_1")]
    private extern static IntPtr CSharp_ScrApplicationProto_argumentValue_1_1(HandleRef thisObj, string argName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_shutdown_1_7")]
    private extern static void CSharp_ScrApplicationProto_shutdown_1_7(HandleRef thisObj, int exitCode);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_shutdown_0_7")]
    private extern static void CSharp_ScrApplicationProto_shutdown_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_createProject_2_1")]
    private extern static IntPtr CSharp_ScrApplicationProto_createProject_2_1(HandleRef thisObj, string projectName, string projectDirectory);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_getProject_0_39")]
    private extern static IntPtr CSharp_ScrApplicationProto_getProject_0_39(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_getCurrentProject_0_39")]
    private extern static IntPtr CSharp_ScrApplicationProto_getCurrentProject_0_39(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_getRelease_0_3")]
    private extern static IntPtr CSharp_ScrApplicationProto_getRelease_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_getSoftwareVersion_0_7")]
    private extern static IntPtr CSharp_ScrApplicationProto_getSoftwareVersion_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_getConsole_0_3")]
    private extern static IntPtr CSharp_ScrApplicationProto_getConsole_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_compareSoftwareVersionSegments_2_1")]
    private extern static int CSharp_ScrApplicationProto_compareSoftwareVersionSegments_2_1(HandleRef thisObj, string leftQ, string rightQ);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_setenv_2_1")]
    private extern static void CSharp_ScrApplicationProto_setenv_2_1(HandleRef thisObj, string name, string value);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_setenv_1_1")]
    private extern static void CSharp_ScrApplicationProto_setenv_1_1(HandleRef thisObj, string var);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_getenv_1_1")]
    private extern static IntPtr CSharp_ScrApplicationProto_getenv_1_1(HandleRef thisObj, string varName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_recompile_0_3")]
    private extern static void CSharp_ScrApplicationProto_recompile_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_compile_0_3")]
    private extern static void CSharp_ScrApplicationProto_compile_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_compileNoWait_0_7")]
    private extern static void CSharp_ScrApplicationProto_compileNoWait_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_compileTarget_1_1")]
    private extern static int CSharp_ScrApplicationProto_compileTarget_1_1(HandleRef thisObj, string targetName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_checkoutRuntimeLicense_1_1")]
    private extern static bool CSharp_ScrApplicationProto_checkoutRuntimeLicense_1_1(HandleRef thisObj, string licFeature);
    [DllImport("__Internal", EntryPoint="CSharp_ScrApplicationProto_checkoutSimulationRuntime_1_1")]
    private extern static bool CSharp_ScrApplicationProto_checkoutSimulationRuntime_1_1(HandleRef thisObj, string licFeature);
    #endregion DllImports
    #region Properties
    public string Release
    {
      get
      {
        return getRelease();
      }
    }
    public string SoftwareVersion
    {
      get
      {
        return getSoftwareVersion();
      }
    }
    public Project Project
    {
      get
      {
        return getProject();
      }
    }
    public Project CurrentProject
    {
      get
      {
        return getCurrentProject();
      }
    }
    public bool IsUnix
    {
      get
      {
        return getIsUnix();
      }
    }
    public string WorkingDirectory
    {
      set
      {
        setWorkingDirectory(value);
      }
      get
      {
        return getWorkingDirectory();
      }
    }
    #endregion Properties
    #region Methods
    public bool getIsUnix()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      return CSharp_ScrApplicationProto_getIsUnix_0_7(objCPtr);
    }

    public string getWorkingDirectory()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      IntPtr ipSr = CSharp_ScrApplicationProto_getWorkingDirectory_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setWorkingDirectory(string dir)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      CSharp_ScrApplicationProto_setWorkingDirectory_1_1(objCPtr, dir);
    }

    public bool checkArgument(string argName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      return CSharp_ScrApplicationProto_checkArgument_1_1(objCPtr, argName);
    }

    public string argumentValue(string argName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      IntPtr ipSr = CSharp_ScrApplicationProto_argumentValue_1_1(objCPtr, argName);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void shutdown(int exitCode)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      CSharp_ScrApplicationProto_shutdown_1_7(objCPtr, exitCode);
    }

    public void shutdown()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      CSharp_ScrApplicationProto_shutdown_0_7(objCPtr);
    }

    public Project createProject(string projectName, string projectDirectory)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      IntPtr cPtr = CSharp_ScrApplicationProto_createProject_2_1(objCPtr, projectName, projectDirectory);
      Project ret = (cPtr == IntPtr.Zero) ? null : new Project(cPtr, false);
      return ret;
    }

    public Project getProject()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      IntPtr cPtr = CSharp_ScrApplicationProto_getProject_0_39(objCPtr);
      Project ret = (cPtr == IntPtr.Zero) ? null : new Project(cPtr, false);
      return ret;
    }

    public Project getCurrentProject()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      IntPtr cPtr = CSharp_ScrApplicationProto_getCurrentProject_0_39(objCPtr);
      Project ret = (cPtr == IntPtr.Zero) ? null : new Project(cPtr, false);
      return ret;
    }

    public string getRelease()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      IntPtr ipSr = CSharp_ScrApplicationProto_getRelease_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getSoftwareVersion()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      IntPtr ipSr = CSharp_ScrApplicationProto_getSoftwareVersion_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public int compareSoftwareVersionSegments(string leftQ, string rightQ)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      return CSharp_ScrApplicationProto_compareSoftwareVersionSegments_2_1(objCPtr, leftQ, rightQ);
    }

    public void setenv(string name, string value)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      CSharp_ScrApplicationProto_setenv_2_1(objCPtr, name, value);
    }

    public void setenv(string var)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      CSharp_ScrApplicationProto_setenv_1_1(objCPtr, var);
    }

    public string getenv(string varName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      IntPtr ipSr = CSharp_ScrApplicationProto_getenv_1_1(objCPtr, varName);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void recompile()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      CSharp_ScrApplicationProto_recompile_0_3(objCPtr);
    }

    public void compile()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      CSharp_ScrApplicationProto_compile_0_3(objCPtr);
    }

    public void compileNoWait()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      CSharp_ScrApplicationProto_compileNoWait_0_7(objCPtr);
    }

    public int compileTarget(string targetName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      return CSharp_ScrApplicationProto_compileTarget_1_1(objCPtr, targetName);
    }

    public bool checkoutRuntimeLicense(string licFeature)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      return CSharp_ScrApplicationProto_checkoutRuntimeLicense_1_1(objCPtr, licFeature);
    }

    public bool checkoutSimulationRuntime(string licFeature)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Application object");
      return CSharp_ScrApplicationProto_checkoutSimulationRuntime_1_1(objCPtr, licFeature);
    }

    #endregion Methods

  }

}
