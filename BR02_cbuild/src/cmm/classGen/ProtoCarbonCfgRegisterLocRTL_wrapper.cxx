PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocRTL_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CarbonCfgRegisterLocConstant* CSharp_ProtoCarbonCfgRegisterLocRTL_castConstant_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    return obj->castConstant();
}
PINVOKEABLE CarbonCfgRegisterLocRTL* CSharp_ProtoCarbonCfgRegisterLocRTL_castRTL_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    return obj->castRTL();
}
PINVOKEABLE CarbonCfgRegisterLocReg* CSharp_ProtoCarbonCfgRegisterLocRTL_castReg_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    return obj->castReg();
}
PINVOKEABLE CarbonCfgRegisterLocArray* CSharp_ProtoCarbonCfgRegisterLocRTL_castArray_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    return obj->castArray();
}
PINVOKEABLE CarbonCfgRegisterLocUser* CSharp_ProtoCarbonCfgRegisterLocRTL_castUser_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    return obj->castUser();
}
PINVOKEABLE CcfgEnum::CarbonCfgRegisterLocKind CSharp_ProtoCarbonCfgRegisterLocRTL_getType_0_35(void* thisObj)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    return obj->getType();
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgRegisterLocRTL_getHasRange_0_3(void* thisObj)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    return obj->getHasRange();
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgRegisterLocRTL_hasPath_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    return obj->hasPath();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgRegisterLocRTL_getPath_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    return EmbeddedMono::AllocString(obj->getPath());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocRTL_setPath_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    obj->setPath(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterLocRTL_getLeft_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    return obj->getLeft();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocRTL_setLeft_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    obj->setLeft(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterLocRTL_getRight_0_7(void* thisObj)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    return obj->getRight();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocRTL_setRight_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    obj->setRight(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterLocRTL_getLSB_0_39(void* thisObj)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    return obj->getLSB();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocRTL_setLSB_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    obj->setLSB(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegisterLocRTL_getMSB_0_39(void* thisObj)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    return obj->getMSB();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegisterLocRTL_setMSB_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgRegisterLocRTL* obj = (ProtoCarbonCfgRegisterLocRTL*)(thisObj);
    obj->setMSB(newVal);
}
PINVOKEABLE ProtoCarbonCfgRegisterLoc* CSharp_ProtoCarbonCfgRegisterLocRTL_UpCast(ProtoCarbonCfgRegisterLocRTL* obj)
{
  return (ProtoCarbonCfgRegisterLoc*)(obj);
}
