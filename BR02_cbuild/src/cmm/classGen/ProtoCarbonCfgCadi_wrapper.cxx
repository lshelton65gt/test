PINVOKEABLE void CSharp_ProtoCarbonCfgCadi_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgCadi* obj = (ProtoCarbonCfgCadi*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgCadi_getDisassemblerName_0_3(void* thisObj)
{
    ProtoCarbonCfgCadi* obj = (ProtoCarbonCfgCadi*)(thisObj);
    return EmbeddedMono::AllocString(obj->getDisassemblerName());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgCadi_setDisassemblerName_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgCadi* obj = (ProtoCarbonCfgCadi*)(thisObj);
    obj->setDisassemblerName(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgCadi_numCustomCodes_0_3(void* thisObj)
{
    ProtoCarbonCfgCadi* obj = (ProtoCarbonCfgCadi*)(thisObj);
    return obj->numCustomCodes();
}
PINVOKEABLE CarbonCfgCadiCustomCode* CSharp_ProtoCarbonCfgCadi_addCustomCode_0_3(void* thisObj)
{
    ProtoCarbonCfgCadi* obj = (ProtoCarbonCfgCadi*)(thisObj);
    return obj->addCustomCode();
}
PINVOKEABLE CarbonCfgCadiCustomCode* CSharp_ProtoCarbonCfgCadi_getCustomCode_1_19(void* thisObj, unsigned int i)
{
    ProtoCarbonCfgCadi* obj = (ProtoCarbonCfgCadi*)(thisObj);
    return obj->getCustomCode(i);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgCadi_removeCustomCode_1_3(void* thisObj, CarbonCfgCadiCustomCode* customCode)
{
    ProtoCarbonCfgCadi* obj = (ProtoCarbonCfgCadi*)(thisObj);
    obj->removeCustomCode(customCode);
}
