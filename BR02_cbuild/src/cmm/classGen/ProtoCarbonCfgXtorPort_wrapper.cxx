PINVOKEABLE void CSharp_ProtoCarbonCfgXtorPort_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgXtorPort* obj = (ProtoCarbonCfgXtorPort*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CcfgEnum::CarbonCfgRTLPortType CSharp_ProtoCarbonCfgXtorPort_getType_0_35(void* thisObj)
{
    ProtoCarbonCfgXtorPort* obj = (ProtoCarbonCfgXtorPort*)(thisObj);
    return obj->getType();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgXtorPort_name_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorPort* obj = (ProtoCarbonCfgXtorPort*)(thisObj);
    return EmbeddedMono::AllocString(obj->name());
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgXtorPort_size_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorPort* obj = (ProtoCarbonCfgXtorPort*)(thisObj);
    return obj->size();
}
