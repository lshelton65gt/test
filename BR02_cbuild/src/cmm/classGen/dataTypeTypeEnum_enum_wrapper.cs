using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class dataTypeTypeEnum
  {
    public enum dataTypeType
    {
      INT = 0,
      UNSIGNEDINT = 1,
      LONG = 2,
      UNSIGNEDLONG = 3,
      FLOAT = 4,
      DOUBLE = 5,
      CHAR = 6,
      VOID = 7
    }
  }
}
