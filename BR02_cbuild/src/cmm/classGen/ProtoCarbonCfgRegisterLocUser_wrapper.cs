using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgRegisterLocUser : CarbonCfgRegisterLoc, IDisposable
  {
    private HandleRef objCPtr;
    internal CarbonCfgRegisterLocUser(IntPtr cPtr, bool cMemoryOwn) : base(CSharp_ProtoCarbonCfgRegisterLocUser_UpCast(cPtr), cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgRegisterLocUser obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgRegisterLocUser()
    {
      Dispose();
    }
    public override void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocUser_UpCast")]
    private static extern IntPtr CSharp_ProtoCarbonCfgRegisterLocUser_UpCast(IntPtr obj);
    #endregion DllImports
    #region Properties
    #endregion Properties
    #region Methods
    #endregion Methods

  }

}
