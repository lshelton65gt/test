using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgMemoryLocPort : CarbonCfgMemoryLoc, IDisposable
  {
    private HandleRef objCPtr;
    internal CarbonCfgMemoryLocPort(IntPtr cPtr, bool cMemoryOwn) : base(CSharp_ProtoCarbonCfgMemoryLocPort_UpCast(cPtr), cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgMemoryLocPort obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgMemoryLocPort()
    {
      Dispose();
    }
    public override void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocPort_getPortName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgMemoryLocPort_getPortName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocPort_setPortName_1_1")]
    private extern static void CSharp_ProtoCarbonCfgMemoryLocPort_setPortName_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocPort_getFixedAddress_0_35")]
    private extern static bool CSharp_ProtoCarbonCfgMemoryLocPort_getFixedAddress_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocPort_setFixedAddress_1_19")]
    private extern static void CSharp_ProtoCarbonCfgMemoryLocPort_setFixedAddress_1_19(HandleRef thisObj, uint newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgMemoryLocPort_UpCast")]
    private static extern IntPtr CSharp_ProtoCarbonCfgMemoryLocPort_UpCast(IntPtr obj);
    #endregion DllImports
    #region Properties
    public bool FixedAddress
    {
      get
      {
        return getFixedAddress();
      }
    }
    #endregion Properties
    #region Methods
    public string getPortName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocPort object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgMemoryLocPort_getPortName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setPortName(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocPort object");
      CSharp_ProtoCarbonCfgMemoryLocPort_setPortName_1_1(objCPtr, newVal);
    }

    public bool getFixedAddress()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocPort object");
      return CSharp_ProtoCarbonCfgMemoryLocPort_getFixedAddress_0_35(objCPtr);
    }

    public void setFixedAddress(uint newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgMemoryLocPort object");
      CSharp_ProtoCarbonCfgMemoryLocPort_setFixedAddress_1_19(objCPtr, newVal);
    }

    #endregion Methods

  }

}
