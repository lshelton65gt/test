using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class resolveTypeEnum
  {
    public enum resolveType
    {
      IMMEDIATE = 0,
      USER = 1,
      DEPENDENT = 2,
      GENERATED = 3
    }
  }
}
