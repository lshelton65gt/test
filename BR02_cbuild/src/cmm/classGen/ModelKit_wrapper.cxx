PINVOKEABLE void CSharp_ModelKit_deleteLater_0_7(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE void CSharp_ModelKit_retainFile_1_1(void* thisObj, const char* fileName)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    obj->retainFile(fileName);
}
PINVOKEABLE void CSharp_ModelKit_retainFile_2_1(void* thisObj, const char* fileName, const char* subdirName)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    obj->retainFile(fileName, subdirName);
}
PINVOKEABLE char* CSharp_ModelKit_getPrefaceScriptActual_0_7(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return EmbeddedMono::AllocString(obj->getPrefaceScriptActual());
}
PINVOKEABLE char* CSharp_ModelKit_getEpilogueScriptActual_0_7(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return EmbeddedMono::AllocString(obj->getEpilogueScriptActual());
}
PINVOKEABLE char* CSharp_ModelKit_getPrefaceScriptNormalized_0_7(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return EmbeddedMono::AllocString(obj->getPrefaceScriptNormalized());
}
PINVOKEABLE char* CSharp_ModelKit_getEpilogueScriptNormalized_0_7(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return EmbeddedMono::AllocString(obj->getEpilogueScriptNormalized());
}
PINVOKEABLE char* CSharp_ModelKit_getRevision_0_7(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return EmbeddedMono::AllocString(obj->getRevision());
}
PINVOKEABLE char* CSharp_ModelKit_getComponentName_0_3(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return EmbeddedMono::AllocString(obj->getComponentName());
}
PINVOKEABLE void CSharp_ModelKit_setComponentName_1_1(void* thisObj, const char* newVal)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    obj->setComponentName(newVal);
}
PINVOKEABLE char* CSharp_ModelKit_getRelease_0_3(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return EmbeddedMono::AllocString(obj->getRelease());
}
PINVOKEABLE char* CSharp_ModelKit_getSoftwareVersion_0_7(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return EmbeddedMono::AllocString(obj->getSoftwareVersion());
}
PINVOKEABLE void CSharp_ModelKit_setCopyFiles_1_3(void* thisObj, bool newVal)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    obj->setCopyFiles(newVal);
}
PINVOKEABLE bool CSharp_ModelKit_getCopyFiles_0_3(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return obj->getCopyFiles();
}
PINVOKEABLE void CSharp_ModelKit_setAskRoot_1_3(void* thisObj, bool newVal)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    obj->setAskRoot(newVal);
}
PINVOKEABLE bool CSharp_ModelKit_getAskRoot_0_7(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return obj->getAskRoot();
}
PINVOKEABLE char* CSharp_ModelKit_getName_0_3(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return EmbeddedMono::AllocString(obj->getName());
}
PINVOKEABLE void CSharp_ModelKit_setName_1_1(void* thisObj, const char* newVal)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    obj->setName(newVal);
}
PINVOKEABLE char* CSharp_ModelKit_getDescription_0_7(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return EmbeddedMono::AllocString(obj->getDescription());
}
PINVOKEABLE void CSharp_ModelKit_setDescription_1_1(void* thisObj, const char* newVal)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    obj->setDescription(newVal);
}
PINVOKEABLE char* CSharp_ModelKit_getLicenseKey_0_3(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return EmbeddedMono::AllocString(obj->getLicenseKey());
}
PINVOKEABLE void CSharp_ModelKit_setLicenseKey_1_1(void* thisObj, const char* newVal)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    obj->setLicenseKey(newVal);
}
PINVOKEABLE KitVersions* CSharp_ModelKit_getVersions_0_3(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return obj->getVersions();
}
PINVOKEABLE KitManifests* CSharp_ModelKit_getManifests_0_3(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return obj->getManifests();
}
PINVOKEABLE KitVersion* CSharp_ModelKit_getLatestVersion_0_7(void* thisObj)
{
    ModelKit* obj = (ModelKit*)(thisObj);
    return obj->getLatestVersion();
}
