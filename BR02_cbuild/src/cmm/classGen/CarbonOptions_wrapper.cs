using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class Switches : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal Switches(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(Switches obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~Switches()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_CarbonOptions_lookupProperty_1_1")]
    private extern static IntPtr CSharp_CarbonOptions_lookupProperty_1_1(HandleRef thisObj, string propertyName);
    [DllImport("__Internal", EntryPoint="CSharp_CarbonOptions_numSwitches_0_3")]
    private extern static uint CSharp_CarbonOptions_numSwitches_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_CarbonOptions_setSwitchValue_2_1")]
    private extern static void CSharp_CarbonOptions_setSwitchValue_2_1(HandleRef thisObj, string switchName, string value);
    [DllImport("__Internal", EntryPoint="CSharp_CarbonOptions_getSwitchValue_1_19")]
    private extern static IntPtr CSharp_CarbonOptions_getSwitchValue_1_19(HandleRef thisObj, uint index);
    [DllImport("__Internal", EntryPoint="CSharp_CarbonOptions_getSwitchValue_1_1")]
    private extern static IntPtr CSharp_CarbonOptions_getSwitchValue_1_1(HandleRef thisObj, string propName);
    #endregion DllImports
    #region Properties
    #endregion Properties
    #region Methods
    public uint numSwitches()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Switches object");
      return CSharp_CarbonOptions_numSwitches_0_3(objCPtr);
    }

    public void setSwitchValue(string switchName, string value)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Switches object");
      CSharp_CarbonOptions_setSwitchValue_2_1(objCPtr, switchName, value);
    }

    #endregion Methods

  }

}
