using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgXtorPort : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgXtorPort(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgXtorPort obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgXtorPort()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorPort_getType_0_35")]
    private extern static int CSharp_ProtoCarbonCfgXtorPort_getType_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorPort_name_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorPort_name_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorPort_size_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgXtorPort_size_0_3(HandleRef thisObj);
    #endregion DllImports
    #region Properties
    public string Name
    {
      get
      {
        return name();
      }
    }
    public uint Size
    {
      get
      {
        return size();
      }
    }
    public CcfgEnum.CarbonCfgRTLPortType Type
    {
      get
      {
        return getType();
      }
    }
    #endregion Properties
    #region Methods
    public CcfgEnum.CarbonCfgRTLPortType getType()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorPort object");
      return (CcfgEnum.CarbonCfgRTLPortType)CSharp_ProtoCarbonCfgXtorPort_getType_0_35(objCPtr);
    }

    public string name()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorPort object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgXtorPort_name_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public uint size()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorPort object");
      return CSharp_ProtoCarbonCfgXtorPort_size_0_3(objCPtr);
    }

    #endregion Methods

  }

}
