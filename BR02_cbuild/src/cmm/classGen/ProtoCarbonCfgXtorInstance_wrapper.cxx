PINVOKEABLE void CSharp_ProtoCarbonCfgXtorInstance_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgXtorInstance* obj = (ProtoCarbonCfgXtorInstance*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CarbonCfgXtorInstance* CSharp_ProtoCarbonCfgXtorInstance_getClockMaster_0_7(void* thisObj)
{
    ProtoCarbonCfgXtorInstance* obj = (ProtoCarbonCfgXtorInstance*)(thisObj);
    return obj->getClockMaster();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgXtorInstance_setClockMaster_1_3(void* thisObj, CarbonCfgXtorInstance* clockMaster)
{
    ProtoCarbonCfgXtorInstance* obj = (ProtoCarbonCfgXtorInstance*)(thisObj);
    obj->setClockMaster(clockMaster);
}
PINVOKEABLE CarbonCfgXtorParamInst* CSharp_ProtoCarbonCfgXtorInstance_findParameter_1_1(void* thisObj, const char* paramName)
{
    ProtoCarbonCfgXtorInstance* obj = (ProtoCarbonCfgXtorInstance*)(thisObj);
    return obj->findParameter(paramName);
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgXtorInstance_hideParameter_1_1(void* thisObj, const char* paramName)
{
    ProtoCarbonCfgXtorInstance* obj = (ProtoCarbonCfgXtorInstance*)(thisObj);
    return obj->hideParameter(paramName);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgXtorInstance_addConnection_2_3(void* thisObj, unsigned int i, CarbonCfgXtorConn* conn)
{
    ProtoCarbonCfgXtorInstance* obj = (ProtoCarbonCfgXtorInstance*)(thisObj);
    obj->addConnection(i, conn);
}
PINVOKEABLE CarbonCfgXtorConn* CSharp_ProtoCarbonCfgXtorInstance_getConnection_1_19(void* thisObj, unsigned int i)
{
    ProtoCarbonCfgXtorInstance* obj = (ProtoCarbonCfgXtorInstance*)(thisObj);
    return obj->getConnection(i);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgXtorInstance_name_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorInstance* obj = (ProtoCarbonCfgXtorInstance*)(thisObj);
    return EmbeddedMono::AllocString(obj->name());
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgXtorInstance_numConnections_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorInstance* obj = (ProtoCarbonCfgXtorInstance*)(thisObj);
    return obj->numConnections();
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgXtorInstance_numParams_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorInstance* obj = (ProtoCarbonCfgXtorInstance*)(thisObj);
    return obj->numParams();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgXtorInstance_setUseDebugAccess_1_3(void* thisObj, bool val)
{
    ProtoCarbonCfgXtorInstance* obj = (ProtoCarbonCfgXtorInstance*)(thisObj);
    obj->setUseDebugAccess(val);
}
PINVOKEABLE CarbonCfgXtor* CSharp_ProtoCarbonCfgXtorInstance_getXtor_0_7(void* thisObj)
{
    ProtoCarbonCfgXtorInstance* obj = (ProtoCarbonCfgXtorInstance*)(thisObj);
    return obj->getXtor();
}
