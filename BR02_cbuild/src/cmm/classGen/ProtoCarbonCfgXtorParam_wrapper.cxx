PINVOKEABLE void CSharp_ProtoCarbonCfgXtorParam_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgXtorParam* obj = (ProtoCarbonCfgXtorParam*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgXtorParam_getName_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorParam* obj = (ProtoCarbonCfgXtorParam*)(thisObj);
    return EmbeddedMono::AllocString(obj->getName());
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgXtorParam_getSize_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorParam* obj = (ProtoCarbonCfgXtorParam*)(thisObj);
    return obj->getSize();
}
PINVOKEABLE CcfgEnum::CarbonCfgParamFlag CSharp_ProtoCarbonCfgXtorParam_getFlag_0_35(void* thisObj)
{
    ProtoCarbonCfgXtorParam* obj = (ProtoCarbonCfgXtorParam*)(thisObj);
    return obj->getFlag();
}
PINVOKEABLE CcfgEnum::CarbonCfgParamDataType CSharp_ProtoCarbonCfgXtorParam_getType_0_35(void* thisObj)
{
    ProtoCarbonCfgXtorParam* obj = (ProtoCarbonCfgXtorParam*)(thisObj);
    return obj->getType();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgXtorParam_getDescription_0_7(void* thisObj)
{
    ProtoCarbonCfgXtorParam* obj = (ProtoCarbonCfgXtorParam*)(thisObj);
    return EmbeddedMono::AllocString(obj->getDescription());
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgXtorParam_getEnumChoices_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorParam* obj = (ProtoCarbonCfgXtorParam*)(thisObj);
    return EmbeddedMono::AllocString(obj->getEnumChoices());
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgXtorParam_getDefaultValue_0_3(void* thisObj)
{
    ProtoCarbonCfgXtorParam* obj = (ProtoCarbonCfgXtorParam*)(thisObj);
    return EmbeddedMono::AllocString(obj->getDefaultValue());
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgXtorParam_inRange_1_1(void* thisObj, const char* value)
{
    ProtoCarbonCfgXtorParam* obj = (ProtoCarbonCfgXtorParam*)(thisObj);
    return obj->inRange(value);
}
