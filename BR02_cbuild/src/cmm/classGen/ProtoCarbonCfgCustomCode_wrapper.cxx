PINVOKEABLE void CSharp_ProtoCarbonCfgCustomCode_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgCustomCode* obj = (ProtoCarbonCfgCustomCode*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CcfgEnum::CarbonCfgCustomCodePosition CSharp_ProtoCarbonCfgCustomCode_getPosition_0_7(void* thisObj)
{
    ProtoCarbonCfgCustomCode* obj = (ProtoCarbonCfgCustomCode*)(thisObj);
    return obj->getPosition();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgCustomCode_setPosition_1_3(void* thisObj, CcfgEnum::CarbonCfgCustomCodePosition position)
{
    ProtoCarbonCfgCustomCode* obj = (ProtoCarbonCfgCustomCode*)(thisObj);
    obj->setPosition(position);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgCustomCode_getCode_0_3(void* thisObj)
{
    ProtoCarbonCfgCustomCode* obj = (ProtoCarbonCfgCustomCode*)(thisObj);
    return EmbeddedMono::AllocString(obj->getCode());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgCustomCode_setCode_1_1(void* thisObj, const char* code)
{
    ProtoCarbonCfgCustomCode* obj = (ProtoCarbonCfgCustomCode*)(thisObj);
    obj->setCode(code);
}
