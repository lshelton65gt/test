using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class KitAnswer : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal KitAnswer(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(KitAnswer obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~KitAnswer()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_KitAnswer_getID_0_7")]
    private extern static IntPtr CSharp_KitAnswer_getID_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitAnswer_setID_1_1")]
    private extern static void CSharp_KitAnswer_setID_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_KitAnswer_getDisplayValue_0_3")]
    private extern static IntPtr CSharp_KitAnswer_getDisplayValue_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitAnswer_setDisplayValue_1_1")]
    private extern static void CSharp_KitAnswer_setDisplayValue_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_KitAnswer_getValue_0_3")]
    private extern static IntPtr CSharp_KitAnswer_getValue_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitAnswer_setValue_1_1")]
    private extern static void CSharp_KitAnswer_setValue_1_1(HandleRef thisObj, string newVal);
    #endregion DllImports
    #region Properties
    public string DisplayValue
    {
      get
      {
        return getDisplayValue();
      }
    }
    public string Value
    {
      get
      {
        return getValue();
      }
    }
    #endregion Properties
    #region Methods
    public string getID()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL KitAnswer object");
      IntPtr ipSr = CSharp_KitAnswer_getID_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setID(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL KitAnswer object");
      CSharp_KitAnswer_setID_1_1(objCPtr, newVal);
    }

    public string getDisplayValue()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL KitAnswer object");
      IntPtr ipSr = CSharp_KitAnswer_getDisplayValue_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setDisplayValue(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL KitAnswer object");
      CSharp_KitAnswer_setDisplayValue_1_1(objCPtr, newVal);
    }

    public string getValue()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL KitAnswer object");
      IntPtr ipSr = CSharp_KitAnswer_getValue_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setValue(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL KitAnswer object");
      CSharp_KitAnswer_setValue_1_1(objCPtr, newVal);
    }

    #endregion Methods

  }

}
