PINVOKEABLE void CSharp_ScrApplicationProto_deleteLater_0_7(void* thisObj)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE bool CSharp_ScrApplicationProto_getIsUnix_0_7(void* thisObj)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return obj->getIsUnix();
}
PINVOKEABLE char* CSharp_ScrApplicationProto_getWorkingDirectory_0_3(void* thisObj)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return EmbeddedMono::AllocString(obj->getWorkingDirectory());
}
PINVOKEABLE void CSharp_ScrApplicationProto_setWorkingDirectory_1_1(void* thisObj, const char* dir)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    obj->setWorkingDirectory(dir);
}
PINVOKEABLE bool CSharp_ScrApplicationProto_checkArgument_1_1(void* thisObj, const char* argName)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return obj->checkArgument(argName);
}
PINVOKEABLE char* CSharp_ScrApplicationProto_argumentValue_1_1(void* thisObj, const char* argName)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return EmbeddedMono::AllocString(obj->argumentValue(argName));
}
PINVOKEABLE void CSharp_ScrApplicationProto_shutdown_1_7(void* thisObj, int exitCode)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    obj->shutdown(exitCode);
}
PINVOKEABLE void CSharp_ScrApplicationProto_shutdown_0_7(void* thisObj)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    obj->shutdown();
}
PINVOKEABLE ScrProject* CSharp_ScrApplicationProto_createProject_2_1(void* thisObj, const char* projectName, const char* projectDirectory)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return obj->createProject(projectName, projectDirectory);
}
PINVOKEABLE ScrProject* CSharp_ScrApplicationProto_getProject_0_39(void* thisObj)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return obj->getProject();
}
PINVOKEABLE ScrProject* CSharp_ScrApplicationProto_getCurrentProject_0_39(void* thisObj)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return obj->getCurrentProject();
}
PINVOKEABLE char* CSharp_ScrApplicationProto_getRelease_0_3(void* thisObj)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return EmbeddedMono::AllocString(obj->getRelease());
}
PINVOKEABLE char* CSharp_ScrApplicationProto_getSoftwareVersion_0_7(void* thisObj)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return EmbeddedMono::AllocString(obj->getSoftwareVersion());
}
PINVOKEABLE CarbonConsole* CSharp_ScrApplicationProto_getConsole_0_3(void* thisObj)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return obj->getConsole();
}
PINVOKEABLE QMenuBar* CSharp_ScrApplicationProto_getMenuBar_0_7(void* thisObj)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return obj->getMenuBar();
}
PINVOKEABLE QStatusBar* CSharp_ScrApplicationProto_getStatusBar_0_7(void* thisObj)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return obj->getStatusBar();
}
PINVOKEABLE int CSharp_ScrApplicationProto_compareSoftwareVersionSegments_2_1(void* thisObj, const char* leftQ, const char* rightQ)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return obj->compareSoftwareVersionSegments(leftQ, rightQ);
}
PINVOKEABLE void CSharp_ScrApplicationProto_setenv_2_1(void* thisObj, const char* name, const char* value)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    obj->setenv(name, value);
}
PINVOKEABLE void CSharp_ScrApplicationProto_setenv_1_1(void* thisObj, const char* var)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    obj->setenv(var);
}
PINVOKEABLE char* CSharp_ScrApplicationProto_getenv_1_1(void* thisObj, const char* varName)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return EmbeddedMono::AllocString(obj->getenv(varName));
}
PINVOKEABLE void CSharp_ScrApplicationProto_recompile_0_3(void* thisObj)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    obj->recompile();
}
PINVOKEABLE void CSharp_ScrApplicationProto_compile_0_3(void* thisObj)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    obj->compile();
}
PINVOKEABLE void CSharp_ScrApplicationProto_compileNoWait_0_7(void* thisObj)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    obj->compileNoWait();
}
PINVOKEABLE int CSharp_ScrApplicationProto_compileTarget_1_1(void* thisObj, const char* targetName)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return obj->compileTarget(targetName);
}
PINVOKEABLE bool CSharp_ScrApplicationProto_checkoutRuntimeLicense_1_1(void* thisObj, const char* licFeature)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return obj->checkoutRuntimeLicense(licFeature);
}
PINVOKEABLE bool CSharp_ScrApplicationProto_checkoutSimulationRuntime_1_1(void* thisObj, const char* licFeature)
{
    ScrApplicationProto* obj = (ScrApplicationProto*)(thisObj);
    return obj->checkoutSimulationRuntime(licFeature);
}
