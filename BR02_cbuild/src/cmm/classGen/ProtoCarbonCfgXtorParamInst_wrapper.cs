using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgXtorParamInst : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgXtorParamInst(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgXtorParamInst obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgXtorParamInst()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParamInst_getRTLPort_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorParamInst_getRTLPort_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParamInst_numRTLPorts_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgXtorParamInst_numRTLPorts_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParamInst_getNumEnumChoices_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgXtorParamInst_getNumEnumChoices_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParamInst_getDefaultValue_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorParamInst_getDefaultValue_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParamInst_getValue_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorParamInst_getValue_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParamInst_putValue_1_1")]
    private extern static void CSharp_ProtoCarbonCfgXtorParamInst_putValue_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParamInst_removeEnumChoice_1_1")]
    private extern static bool CSharp_ProtoCarbonCfgXtorParamInst_removeEnumChoice_1_1(HandleRef thisObj, string choice);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParamInst_getEnumChoice_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorParamInst_getEnumChoice_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParamInst_getInstance_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorParamInst_getInstance_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParamInst_getParam_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorParamInst_getParam_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParamInst_getHidden_0_7")]
    private extern static bool CSharp_ProtoCarbonCfgXtorParamInst_getHidden_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParamInst_putHidden_1_3")]
    private extern static void CSharp_ProtoCarbonCfgXtorParamInst_putHidden_1_3(HandleRef thisObj, bool hidden);
    #endregion DllImports
    #region Properties
    public string Value
    {
      set
      {
        putValue(value);
      }
      get
      {
        return getValue();
      }
    }
    public string DefaultValue
    {
      get
      {
        return getDefaultValue();
      }
    }
    public uint NumRTLPorts
    {
      get
      {
        return numRTLPorts();
      }
    }
    public uint NumEnumChoices
    {
      get
      {
        return getNumEnumChoices();
      }
    }
    public CarbonCfgXtorInstance Instance
    {
      get
      {
        return getInstance();
      }
    }
    public CarbonCfgXtorParam Param
    {
      get
      {
        return getParam();
      }
    }
    public bool Hidden
    {
      set
      {
        putHidden(value);
      }
      get
      {
        return getHidden();
      }
    }
    #endregion Properties
    #region Methods
    public CarbonCfgRTLPort getRTLPort(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParamInst object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgXtorParamInst_getRTLPort_1_19(objCPtr, i);
      CarbonCfgRTLPort ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRTLPort(cPtr, false);
      return ret;
    }

    public uint numRTLPorts()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParamInst object");
      return CSharp_ProtoCarbonCfgXtorParamInst_numRTLPorts_0_3(objCPtr);
    }

    public uint getNumEnumChoices()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParamInst object");
      return CSharp_ProtoCarbonCfgXtorParamInst_getNumEnumChoices_0_3(objCPtr);
    }

    public string getDefaultValue()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParamInst object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgXtorParamInst_getDefaultValue_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getValue()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParamInst object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgXtorParamInst_getValue_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void putValue(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParamInst object");
      CSharp_ProtoCarbonCfgXtorParamInst_putValue_1_1(objCPtr, newVal);
    }

    public bool removeEnumChoice(string choice)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParamInst object");
      return CSharp_ProtoCarbonCfgXtorParamInst_removeEnumChoice_1_1(objCPtr, choice);
    }

    public string getEnumChoice(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParamInst object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgXtorParamInst_getEnumChoice_1_19(objCPtr, i);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public CarbonCfgXtorInstance getInstance()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParamInst object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgXtorParamInst_getInstance_0_3(objCPtr);
      CarbonCfgXtorInstance ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorInstance(cPtr, false);
      return ret;
    }

    public CarbonCfgXtorParam getParam()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParamInst object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgXtorParamInst_getParam_0_3(objCPtr);
      CarbonCfgXtorParam ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorParam(cPtr, false);
      return ret;
    }

    public bool getHidden()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParamInst object");
      return CSharp_ProtoCarbonCfgXtorParamInst_getHidden_0_7(objCPtr);
    }

    public void putHidden(bool hidden)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParamInst object");
      CSharp_ProtoCarbonCfgXtorParamInst_putHidden_1_3(objCPtr, hidden);
    }

    #endregion Methods

  }

}
