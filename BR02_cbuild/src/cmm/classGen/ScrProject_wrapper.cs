using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class Project : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal Project(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(Project obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~Project()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ScrProject_getOptions_0_3")]
    private extern static IntPtr CSharp_ScrProject_getOptions_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrProject_getCompilerOptions_0_3")]
    private extern static IntPtr CSharp_ScrProject_getCompilerOptions_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrProject_getHashValue_1_1")]
    private extern static IntPtr CSharp_ScrProject_getHashValue_1_1(HandleRef thisObj, string pathName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrProject_getDesignFileName_1_1")]
    private extern static IntPtr CSharp_ScrProject_getDesignFileName_1_1(HandleRef thisObj, string fileExtension);
    [DllImport("__Internal", EntryPoint="CSharp_ScrProject_getBaseName_0_3")]
    private extern static IntPtr CSharp_ScrProject_getBaseName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrProject_numComponents_0_3")]
    private extern static uint CSharp_ScrProject_numComponents_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrProject_addSourceFile_3_3")]
    private extern static void CSharp_ScrProject_addSourceFile_3_3(HandleRef thisObj, string fileName, string folderName, bool libraryFile);
    [DllImport("__Internal", EntryPoint="CSharp_ScrProject_addSourceFile_2_1")]
    private extern static void CSharp_ScrProject_addSourceFile_2_1(HandleRef thisObj, string fileName, string folderName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrProject_save_0_3")]
    private extern static void CSharp_ScrProject_save_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrProject_deleteComponent_1_53")]
    private extern static bool CSharp_ScrProject_deleteComponent_1_53(HandleRef thisObj, int type);
    [DllImport("__Internal", EntryPoint="CSharp_ScrProject_projectDirectory_0_3")]
    private extern static IntPtr CSharp_ScrProject_projectDirectory_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrProject_outputDirectory_0_3")]
    private extern static IntPtr CSharp_ScrProject_outputDirectory_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrProject_designHierarchy_0_3")]
    private extern static IntPtr CSharp_ScrProject_designHierarchy_0_3(HandleRef thisObj);
    #endregion DllImports
    #region Properties
    public string ProjectDirectory
    {
      get
      {
        return projectDirectory();
      }
    }
    public string OutputDirectory
    {
      get
      {
        return outputDirectory();
      }
    }
    public string BaseName
    {
      get
      {
        return getBaseName();
      }
    }
    public uint NumComponents
    {
      get
      {
        return numComponents();
      }
    }
    public Switches Options
    {
      get
      {
        return getOptions();
      }
    }
    public Switches CompilerOptions
    {
      get
      {
        return getCompilerOptions();
      }
    }
    #endregion Properties
    #region Methods
    public Switches getOptions()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Project object");
      IntPtr cPtr = CSharp_ScrProject_getOptions_0_3(objCPtr);
      Switches ret = (cPtr == IntPtr.Zero) ? null : new Switches(cPtr, false);
      return ret;
    }

    public Switches getCompilerOptions()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Project object");
      IntPtr cPtr = CSharp_ScrProject_getCompilerOptions_0_3(objCPtr);
      Switches ret = (cPtr == IntPtr.Zero) ? null : new Switches(cPtr, false);
      return ret;
    }

    public string getHashValue(string pathName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Project object");
      IntPtr ipSr = CSharp_ScrProject_getHashValue_1_1(objCPtr, pathName);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getDesignFileName(string fileExtension)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Project object");
      IntPtr ipSr = CSharp_ScrProject_getDesignFileName_1_1(objCPtr, fileExtension);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getBaseName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Project object");
      IntPtr ipSr = CSharp_ScrProject_getBaseName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public uint numComponents()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Project object");
      return CSharp_ScrProject_numComponents_0_3(objCPtr);
    }

    public void addSourceFile(string fileName, string folderName, bool libraryFile)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Project object");
      CSharp_ScrProject_addSourceFile_3_3(objCPtr, fileName, folderName, libraryFile);
    }

    public void addSourceFile(string fileName, string folderName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Project object");
      CSharp_ScrProject_addSourceFile_2_1(objCPtr, fileName, folderName);
    }

    public void save()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Project object");
      CSharp_ScrProject_save_0_3(objCPtr);
    }

    public bool deleteComponent(ProjectEnum.ComponentType type)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Project object");
      return CSharp_ScrProject_deleteComponent_1_53(objCPtr, (int)type);
    }

    public string projectDirectory()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Project object");
      IntPtr ipSr = CSharp_ScrProject_projectDirectory_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string outputDirectory()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Project object");
      IntPtr ipSr = CSharp_ScrProject_outputDirectory_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    #endregion Methods

  }

}
