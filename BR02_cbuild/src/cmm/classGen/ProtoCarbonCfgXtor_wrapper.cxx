PINVOKEABLE void CSharp_ProtoCarbonCfgXtor_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgXtor* obj = (ProtoCarbonCfgXtor*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CarbonCfgXtorPort* CSharp_ProtoCarbonCfgXtor_getPort_1_19(void* thisObj, unsigned int i)
{
    ProtoCarbonCfgXtor* obj = (ProtoCarbonCfgXtor*)(thisObj);
    return obj->getPort(i);
}
PINVOKEABLE CarbonCfgXtorPort* CSharp_ProtoCarbonCfgXtor_findPort_1_1(void* thisObj, const char* portName)
{
    ProtoCarbonCfgXtor* obj = (ProtoCarbonCfgXtor*)(thisObj);
    return obj->findPort(portName);
}
PINVOKEABLE CarbonCfgXtorParam* CSharp_ProtoCarbonCfgXtor_findParam_1_1(void* thisObj, const char* paramName)
{
    ProtoCarbonCfgXtor* obj = (ProtoCarbonCfgXtor*)(thisObj);
    return obj->findParam(paramName);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgXtor_name_0_3(void* thisObj)
{
    ProtoCarbonCfgXtor* obj = (ProtoCarbonCfgXtor*)(thisObj);
    return EmbeddedMono::AllocString(obj->name());
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgXtor_numPorts_0_3(void* thisObj)
{
    ProtoCarbonCfgXtor* obj = (ProtoCarbonCfgXtor*)(thisObj);
    return obj->numPorts();
}
