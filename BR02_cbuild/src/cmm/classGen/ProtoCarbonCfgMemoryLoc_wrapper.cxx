PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLoc_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLoc* obj = (ProtoCarbonCfgMemoryLoc*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CcfgEnum::CarbonCfgMemLocType CSharp_ProtoCarbonCfgMemoryLoc_getType_0_35(void* thisObj)
{
    ProtoCarbonCfgMemoryLoc* obj = (ProtoCarbonCfgMemoryLoc*)(thisObj);
    return obj->getType();
}
PINVOKEABLE unsigned long long CSharp_ProtoCarbonCfgMemoryLoc_getDisplayStartWordOffset_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLoc* obj = (ProtoCarbonCfgMemoryLoc*)(thisObj);
    return obj->getDisplayStartWordOffset();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLoc_setDisplayStartWordOffset_1_7(void* thisObj, unsigned long long newVal)
{
    ProtoCarbonCfgMemoryLoc* obj = (ProtoCarbonCfgMemoryLoc*)(thisObj);
    obj->setDisplayStartWordOffset(newVal);
}
PINVOKEABLE unsigned long long CSharp_ProtoCarbonCfgMemoryLoc_getDisplayEndWordOffset_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLoc* obj = (ProtoCarbonCfgMemoryLoc*)(thisObj);
    return obj->getDisplayEndWordOffset();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLoc_setDisplayEndWordOffset_1_7(void* thisObj, unsigned long long newVal)
{
    ProtoCarbonCfgMemoryLoc* obj = (ProtoCarbonCfgMemoryLoc*)(thisObj);
    obj->setDisplayEndWordOffset(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemoryLoc_getDisplayMsb_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLoc* obj = (ProtoCarbonCfgMemoryLoc*)(thisObj);
    return obj->getDisplayMsb();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLoc_setDisplayMsb_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgMemoryLoc* obj = (ProtoCarbonCfgMemoryLoc*)(thisObj);
    obj->setDisplayMsb(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemoryLoc_getDisplayLsb_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLoc* obj = (ProtoCarbonCfgMemoryLoc*)(thisObj);
    return obj->getDisplayLsb();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryLoc_setDisplayLsb_1_19(void* thisObj, unsigned int newVal)
{
    ProtoCarbonCfgMemoryLoc* obj = (ProtoCarbonCfgMemoryLoc*)(thisObj);
    obj->setDisplayLsb(newVal);
}
PINVOKEABLE CarbonCfgMemoryLocRTL* CSharp_ProtoCarbonCfgMemoryLoc_castRTL_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLoc* obj = (ProtoCarbonCfgMemoryLoc*)(thisObj);
    return obj->castRTL();
}
PINVOKEABLE CarbonCfgMemoryLocPort* CSharp_ProtoCarbonCfgMemoryLoc_castPort_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLoc* obj = (ProtoCarbonCfgMemoryLoc*)(thisObj);
    return obj->castPort();
}
PINVOKEABLE CarbonCfgMemoryLocUser* CSharp_ProtoCarbonCfgMemoryLoc_castUser_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryLoc* obj = (ProtoCarbonCfgMemoryLoc*)(thisObj);
    return obj->castUser();
}
