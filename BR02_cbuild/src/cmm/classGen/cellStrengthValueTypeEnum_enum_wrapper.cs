using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class cellStrengthValueTypeEnum
  {
    public enum cellStrengthValueType
    {
      LOW = 0,
      MEDIAN = 1,
      HIGH = 2
    }
  }
}
