PINVOKEABLE void CSharp_CarbonOptions_deleteLater_0_7(void* thisObj)
{
    CarbonOptions* obj = (CarbonOptions*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CarbonProperty* CSharp_CarbonOptions_lookupProperty_1_1(void* thisObj, const char* propertyName)
{
    CarbonOptions* obj = (CarbonOptions*)(thisObj);
    return obj->lookupProperty(propertyName);
}
PINVOKEABLE unsigned int CSharp_CarbonOptions_numSwitches_0_3(void* thisObj)
{
    CarbonOptions* obj = (CarbonOptions*)(thisObj);
    return obj->numSwitches();
}
PINVOKEABLE void CSharp_CarbonOptions_setSwitchValue_2_1(void* thisObj, const char* switchName, const char* value)
{
    CarbonOptions* obj = (CarbonOptions*)(thisObj);
    obj->setSwitchValue(switchName, value);
}
PINVOKEABLE CarbonPropertyValue* CSharp_CarbonOptions_getSwitchValue_1_19(void* thisObj, unsigned int index)
{
    CarbonOptions* obj = (CarbonOptions*)(thisObj);
    return obj->getSwitchValue(index);
}
PINVOKEABLE CarbonPropertyValue* CSharp_CarbonOptions_getSwitchValue_1_1(void* thisObj, const char* propName)
{
    CarbonOptions* obj = (CarbonOptions*)(thisObj);
    return obj->getSwitchValue(propName);
}
