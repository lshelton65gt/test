PINVOKEABLE void CSharp_ProtoCarbonCfgGroup_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgGroup* obj = (ProtoCarbonCfgGroup*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgGroup_getName_0_3(void* thisObj)
{
    ProtoCarbonCfgGroup* obj = (ProtoCarbonCfgGroup*)(thisObj);
    return EmbeddedMono::AllocString(obj->getName());
}
