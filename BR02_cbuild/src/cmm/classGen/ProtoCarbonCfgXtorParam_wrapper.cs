using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgXtorParam : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgXtorParam(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgXtorParam obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgXtorParam()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParam_getName_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorParam_getName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParam_getSize_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgXtorParam_getSize_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParam_getFlag_0_35")]
    private extern static int CSharp_ProtoCarbonCfgXtorParam_getFlag_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParam_getType_0_35")]
    private extern static int CSharp_ProtoCarbonCfgXtorParam_getType_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParam_getDescription_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorParam_getDescription_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParam_getEnumChoices_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorParam_getEnumChoices_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParam_getDefaultValue_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorParam_getDefaultValue_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorParam_inRange_1_1")]
    private extern static bool CSharp_ProtoCarbonCfgXtorParam_inRange_1_1(HandleRef thisObj, string value);
    #endregion DllImports
    #region Properties
    public string Name
    {
      get
      {
        return getName();
      }
    }
    public uint Size
    {
      get
      {
        return getSize();
      }
    }
    public CcfgEnum.CarbonCfgParamFlag Flag
    {
      get
      {
        return getFlag();
      }
    }
    public CcfgEnum.CarbonCfgParamDataType Type
    {
      get
      {
        return getType();
      }
    }
    public string Description
    {
      get
      {
        return getDescription();
      }
    }
    public string EnumChoices
    {
      get
      {
        return getEnumChoices();
      }
    }
    public string DefaultValue
    {
      get
      {
        return getDefaultValue();
      }
    }
    #endregion Properties
    #region Methods
    public string getName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParam object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgXtorParam_getName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public uint getSize()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParam object");
      return CSharp_ProtoCarbonCfgXtorParam_getSize_0_3(objCPtr);
    }

    public CcfgEnum.CarbonCfgParamFlag getFlag()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParam object");
      return (CcfgEnum.CarbonCfgParamFlag)CSharp_ProtoCarbonCfgXtorParam_getFlag_0_35(objCPtr);
    }

    public CcfgEnum.CarbonCfgParamDataType getType()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParam object");
      return (CcfgEnum.CarbonCfgParamDataType)CSharp_ProtoCarbonCfgXtorParam_getType_0_35(objCPtr);
    }

    public string getDescription()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParam object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgXtorParam_getDescription_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getEnumChoices()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParam object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgXtorParam_getEnumChoices_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getDefaultValue()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParam object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgXtorParam_getDefaultValue_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public bool inRange(string value)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorParam object");
      return CSharp_ProtoCarbonCfgXtorParam_inRange_1_1(objCPtr, value);
    }

    #endregion Methods

  }

}
