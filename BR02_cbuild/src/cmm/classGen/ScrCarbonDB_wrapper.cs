using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonDB : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonDB(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonDB obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonDB()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_getDatabaseFileName_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_getDatabaseFileName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_hasDatabase_0_35")]
    private extern static bool CSharp_ScrCarbonDB_hasDatabase_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_findNode_1_1")]
    private extern static IntPtr CSharp_ScrCarbonDB_findNode_1_1(HandleRef thisObj, string path);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopPrimaryPorts_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopPrimaryPorts_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeGetFullName_1_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_nodeGetFullName_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeGetLeafName_1_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_nodeGetLeafName_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIterNext_1_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_nodeIterNext_1_3(HandleRef thisObj, IntPtr iter);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_freeNodeIter_1_3")]
    private extern static void CSharp_ScrCarbonDB_freeNodeIter_1_3(HandleRef thisObj, IntPtr iter);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopChildren_1_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopChildren_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeGetParent_1_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_nodeGetParent_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_findChild_2_1")]
    private extern static IntPtr CSharp_ScrCarbonDB_findChild_2_1(HandleRef thisObj, IntPtr parent, string childName);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_getVersion_0_7")]
    private extern static IntPtr CSharp_ScrCarbonDB_getVersion_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_getSoftwareVersion_0_7")]
    private extern static IntPtr CSharp_ScrCarbonDB_getSoftwareVersion_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_getIdString_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_getIdString_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_getTopLevelModuleName_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_getTopLevelModuleName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_getInterfaceName_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_getInterfaceName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_getSystemCModuleName_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_getSystemCModuleName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopPrimaryInputs_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopPrimaryInputs_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopPrimaryOutputs_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopPrimaryOutputs_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopPrimaryBidis_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopPrimaryBidis_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopPrimaryClks_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopPrimaryClks_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopClkTree_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopClkTree_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopDepositable_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopDepositable_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopObservable_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopObservable_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopScDepositable_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopScDepositable_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopScObservable_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopScObservable_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopLoopAsyncs_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopLoopAsyncs_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopAsyncOutputs_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopAsyncOutputs_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopAsyncDeposits_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopAsyncDeposits_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopAsyncPosResets_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopAsyncPosResets_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopAsyncNegResets_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopAsyncNegResets_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopPosedgeTriggers_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopPosedgeTriggers_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopNegedgeTriggers_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopNegedgeTriggers_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopBothEdgeTriggers_0_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopBothEdgeTriggers_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_loopPosedgeTrigger_0_7")]
    private extern static IntPtr CSharp_ScrCarbonDB_loopPosedgeTrigger_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeComponentName_1_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_nodeComponentName_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeSourceLanguage_1_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_nodeSourceLanguage_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeGetSourceFile_1_3")]
    private extern static IntPtr CSharp_ScrCarbonDB_nodeGetSourceFile_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeGetWidth_1_3")]
    private extern static int CSharp_ScrCarbonDB_nodeGetWidth_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeGetMSB_1_3")]
    private extern static int CSharp_ScrCarbonDB_nodeGetMSB_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeGetLSB_1_3")]
    private extern static int CSharp_ScrCarbonDB_nodeGetLSB_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeGetBitSize_1_3")]
    private extern static int CSharp_ScrCarbonDB_nodeGetBitSize_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeGetSourceLine_1_3")]
    private extern static int CSharp_ScrCarbonDB_nodeGetSourceLine_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeGetArrayLeftBound_1_3")]
    private extern static int CSharp_ScrCarbonDB_nodeGetArrayLeftBound_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeGetArrayRightBound_1_3")]
    private extern static int CSharp_ScrCarbonDB_nodeGetArrayRightBound_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeGetArrayDims_1_3")]
    private extern static int CSharp_ScrCarbonDB_nodeGetArrayDims_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsContainedByComposite_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsContainedByComposite_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsStruct_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsStruct_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsArray_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsArray_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsEnum_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsEnum_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeCanBeCarbonNet_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeCanBeCarbonNet_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsScalar_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsScalar_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsTristate_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsTristate_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsVector_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsVector_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsConstant_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsConstant_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIs2DArray_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIs2DArray_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsClkTree_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsClkTree_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsAsync_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsAsync_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsAsyncOutput_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsAsyncOutput_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsAsyncDeposit_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsAsyncDeposit_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsAsyncPosReset_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsAsyncPosReset_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsAsyncNegReset_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsAsyncNegReset_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsForcible_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsForcible_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsDepositable_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsDepositable_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsScDepositable_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsScDepositable_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsObservable_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsObservable_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsVisible_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsVisible_1_3(HandleRef thisObj, IntPtr node);
    [DllImport("__Internal", EntryPoint="CSharp_ScrCarbonDB_nodeIsTied_1_3")]
    private extern static bool CSharp_ScrCarbonDB_nodeIsTied_1_3(HandleRef thisObj, IntPtr node);
    #endregion DllImports
    #region Properties
    public bool HasDatabase
    {
      get
      {
        return hasDatabase();
      }
    }
    public string DatabaseFileName
    {
      get
      {
        return getDatabaseFileName();
      }
    }
    #endregion Properties
    #region Methods
    public string getDatabaseFileName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      IntPtr ipSr = CSharp_ScrCarbonDB_getDatabaseFileName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public bool hasDatabase()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_hasDatabase_0_35(objCPtr);
    }

    public string nodeGetFullName(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      IntPtr ipSr = CSharp_ScrCarbonDB_nodeGetFullName_1_3(objCPtr, node);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string nodeGetLeafName(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      IntPtr ipSr = CSharp_ScrCarbonDB_nodeGetLeafName_1_3(objCPtr, node);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void freeNodeIter(IntPtr iter)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      CSharp_ScrCarbonDB_freeNodeIter_1_3(objCPtr, iter);
    }

    public string getVersion()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      IntPtr ipSr = CSharp_ScrCarbonDB_getVersion_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getSoftwareVersion()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      IntPtr ipSr = CSharp_ScrCarbonDB_getSoftwareVersion_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getIdString()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      IntPtr ipSr = CSharp_ScrCarbonDB_getIdString_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getTopLevelModuleName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      IntPtr ipSr = CSharp_ScrCarbonDB_getTopLevelModuleName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getInterfaceName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      IntPtr ipSr = CSharp_ScrCarbonDB_getInterfaceName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getSystemCModuleName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      IntPtr ipSr = CSharp_ScrCarbonDB_getSystemCModuleName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string nodeComponentName(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      IntPtr ipSr = CSharp_ScrCarbonDB_nodeComponentName_1_3(objCPtr, node);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string nodeSourceLanguage(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      IntPtr ipSr = CSharp_ScrCarbonDB_nodeSourceLanguage_1_3(objCPtr, node);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string nodeGetSourceFile(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      IntPtr ipSr = CSharp_ScrCarbonDB_nodeGetSourceFile_1_3(objCPtr, node);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public int nodeGetWidth(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeGetWidth_1_3(objCPtr, node);
    }

    public int nodeGetMSB(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeGetMSB_1_3(objCPtr, node);
    }

    public int nodeGetLSB(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeGetLSB_1_3(objCPtr, node);
    }

    public int nodeGetBitSize(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeGetBitSize_1_3(objCPtr, node);
    }

    public int nodeGetSourceLine(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeGetSourceLine_1_3(objCPtr, node);
    }

    public int nodeGetArrayLeftBound(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeGetArrayLeftBound_1_3(objCPtr, node);
    }

    public int nodeGetArrayRightBound(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeGetArrayRightBound_1_3(objCPtr, node);
    }

    public int nodeGetArrayDims(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeGetArrayDims_1_3(objCPtr, node);
    }

    public bool nodeIsContainedByComposite(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsContainedByComposite_1_3(objCPtr, node);
    }

    public bool nodeIsStruct(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsStruct_1_3(objCPtr, node);
    }

    public bool nodeIsArray(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsArray_1_3(objCPtr, node);
    }

    public bool nodeIsEnum(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsEnum_1_3(objCPtr, node);
    }

    public bool nodeCanBeCarbonNet(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeCanBeCarbonNet_1_3(objCPtr, node);
    }

    public bool nodeIsScalar(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsScalar_1_3(objCPtr, node);
    }

    public bool nodeIsTristate(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsTristate_1_3(objCPtr, node);
    }

    public bool nodeIsVector(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsVector_1_3(objCPtr, node);
    }

    public bool nodeIsConstant(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsConstant_1_3(objCPtr, node);
    }

    public bool nodeIs2DArray(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIs2DArray_1_3(objCPtr, node);
    }

    public bool nodeIsClkTree(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsClkTree_1_3(objCPtr, node);
    }

    public bool nodeIsAsync(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsAsync_1_3(objCPtr, node);
    }

    public bool nodeIsAsyncOutput(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsAsyncOutput_1_3(objCPtr, node);
    }

    public bool nodeIsAsyncDeposit(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsAsyncDeposit_1_3(objCPtr, node);
    }

    public bool nodeIsAsyncPosReset(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsAsyncPosReset_1_3(objCPtr, node);
    }

    public bool nodeIsAsyncNegReset(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsAsyncNegReset_1_3(objCPtr, node);
    }

    public bool nodeIsForcible(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsForcible_1_3(objCPtr, node);
    }

    public bool nodeIsDepositable(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsDepositable_1_3(objCPtr, node);
    }

    public bool nodeIsScDepositable(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsScDepositable_1_3(objCPtr, node);
    }

    public bool nodeIsObservable(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsObservable_1_3(objCPtr, node);
    }

    public bool nodeIsVisible(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsVisible_1_3(objCPtr, node);
    }

    public bool nodeIsTied(IntPtr node)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonDB object");
      return CSharp_ScrCarbonDB_nodeIsTied_1_3(objCPtr, node);
    }

    #endregion Methods

  }

}
