using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgXtor : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgXtor(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgXtor obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgXtor()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtor_getPort_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtor_getPort_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtor_findPort_1_1")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtor_findPort_1_1(HandleRef thisObj, string portName);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtor_findParam_1_1")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtor_findParam_1_1(HandleRef thisObj, string paramName);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtor_name_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtor_name_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtor_numPorts_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgXtor_numPorts_0_3(HandleRef thisObj);
    #endregion DllImports
    #region Properties
    public string Name
    {
      get
      {
        return name();
      }
    }
    public uint NumPorts
    {
      get
      {
        return numPorts();
      }
    }
    #endregion Properties
    #region Methods
    public CarbonCfgXtorPort getPort(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtor object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgXtor_getPort_1_19(objCPtr, i);
      CarbonCfgXtorPort ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorPort(cPtr, false);
      return ret;
    }

    public CarbonCfgXtorPort findPort(string portName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtor object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgXtor_findPort_1_1(objCPtr, portName);
      CarbonCfgXtorPort ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorPort(cPtr, false);
      return ret;
    }

    public CarbonCfgXtorParam findParam(string paramName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtor object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgXtor_findParam_1_1(objCPtr, paramName);
      CarbonCfgXtorParam ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorParam(cPtr, false);
      return ret;
    }

    public string name()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtor object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgXtor_name_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public uint numPorts()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtor object");
      return CSharp_ProtoCarbonCfgXtor_numPorts_0_3(objCPtr);
    }

    #endregion Methods

  }

}
