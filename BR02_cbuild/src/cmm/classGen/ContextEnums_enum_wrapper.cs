using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class ContextEnums
  {
    public enum ModelTarget
    {
      Unix = 0,
      Windows = 1
    }
    public enum ModelSymbols
    {
      FullSymbols = 0,
      IOSymbols = 1
    }
  }
}
