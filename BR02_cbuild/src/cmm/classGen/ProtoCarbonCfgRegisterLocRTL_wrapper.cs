using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgRegisterLocRTL : CarbonCfgRegisterLoc, IDisposable
  {
    private HandleRef objCPtr;
    internal CarbonCfgRegisterLocRTL(IntPtr cPtr, bool cMemoryOwn) : base(CSharp_ProtoCarbonCfgRegisterLocRTL_UpCast(cPtr), cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgRegisterLocRTL obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgRegisterLocRTL()
    {
      Dispose();
    }
    public override void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocRTL_getHasRange_0_3")]
    private extern static bool CSharp_ProtoCarbonCfgRegisterLocRTL_getHasRange_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocRTL_hasPath_0_7")]
    private extern static bool CSharp_ProtoCarbonCfgRegisterLocRTL_hasPath_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocRTL_getPath_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRegisterLocRTL_getPath_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocRTL_setPath_1_1")]
    private extern static void CSharp_ProtoCarbonCfgRegisterLocRTL_setPath_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocRTL_getLeft_0_7")]
    private extern static uint CSharp_ProtoCarbonCfgRegisterLocRTL_getLeft_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocRTL_setLeft_1_19")]
    private extern static void CSharp_ProtoCarbonCfgRegisterLocRTL_setLeft_1_19(HandleRef thisObj, uint newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocRTL_getRight_0_7")]
    private extern static uint CSharp_ProtoCarbonCfgRegisterLocRTL_getRight_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocRTL_setRight_1_19")]
    private extern static void CSharp_ProtoCarbonCfgRegisterLocRTL_setRight_1_19(HandleRef thisObj, uint newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocRTL_getLSB_0_39")]
    private extern static uint CSharp_ProtoCarbonCfgRegisterLocRTL_getLSB_0_39(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocRTL_setLSB_1_19")]
    private extern static void CSharp_ProtoCarbonCfgRegisterLocRTL_setLSB_1_19(HandleRef thisObj, uint newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocRTL_getMSB_0_39")]
    private extern static uint CSharp_ProtoCarbonCfgRegisterLocRTL_getMSB_0_39(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocRTL_setMSB_1_19")]
    private extern static void CSharp_ProtoCarbonCfgRegisterLocRTL_setMSB_1_19(HandleRef thisObj, uint newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRegisterLocRTL_UpCast")]
    private static extern IntPtr CSharp_ProtoCarbonCfgRegisterLocRTL_UpCast(IntPtr obj);
    #endregion DllImports
    #region Properties
    public bool HasPath
    {
      get
      {
        return hasPath();
      }
    }
    public bool HasRange
    {
      get
      {
        return getHasRange();
      }
    }
    public string Path
    {
      set
      {
        setPath(value);
      }
      get
      {
        return getPath();
      }
    }
    public uint Left
    {
      set
      {
        setLeft(value);
      }
      get
      {
        return getLeft();
      }
    }
    public uint Right
    {
      set
      {
        setRight(value);
      }
      get
      {
        return getRight();
      }
    }
    #endregion Properties
    #region Methods
    public bool getHasRange()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocRTL object");
      return CSharp_ProtoCarbonCfgRegisterLocRTL_getHasRange_0_3(objCPtr);
    }

    public bool hasPath()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocRTL object");
      return CSharp_ProtoCarbonCfgRegisterLocRTL_hasPath_0_7(objCPtr);
    }

    public string getPath()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocRTL object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgRegisterLocRTL_getPath_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setPath(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocRTL object");
      CSharp_ProtoCarbonCfgRegisterLocRTL_setPath_1_1(objCPtr, newVal);
    }

    public uint getLeft()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocRTL object");
      return CSharp_ProtoCarbonCfgRegisterLocRTL_getLeft_0_7(objCPtr);
    }

    public void setLeft(uint newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocRTL object");
      CSharp_ProtoCarbonCfgRegisterLocRTL_setLeft_1_19(objCPtr, newVal);
    }

    public uint getRight()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocRTL object");
      return CSharp_ProtoCarbonCfgRegisterLocRTL_getRight_0_7(objCPtr);
    }

    public void setRight(uint newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocRTL object");
      CSharp_ProtoCarbonCfgRegisterLocRTL_setRight_1_19(objCPtr, newVal);
    }

    public uint getLSB()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocRTL object");
      return CSharp_ProtoCarbonCfgRegisterLocRTL_getLSB_0_39(objCPtr);
    }

    public void setLSB(uint newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocRTL object");
      CSharp_ProtoCarbonCfgRegisterLocRTL_setLSB_1_19(objCPtr, newVal);
    }

    public uint getMSB()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocRTL object");
      return CSharp_ProtoCarbonCfgRegisterLocRTL_getMSB_0_39(objCPtr);
    }

    public void setMSB(uint newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRegisterLocRTL object");
      CSharp_ProtoCarbonCfgRegisterLocRTL_setMSB_1_19(objCPtr, newVal);
    }

    #endregion Methods

  }

}
