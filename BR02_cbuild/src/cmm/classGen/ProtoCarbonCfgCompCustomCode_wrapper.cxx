PINVOKEABLE void CSharp_ProtoCarbonCfgCompCustomCode_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgCompCustomCode* obj = (ProtoCarbonCfgCompCustomCode*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE CcfgEnum::CarbonCfgCustomCodePosition CSharp_ProtoCarbonCfgCompCustomCode_getPosition_0_7(void* thisObj)
{
    ProtoCarbonCfgCompCustomCode* obj = (ProtoCarbonCfgCompCustomCode*)(thisObj);
    return obj->getPosition();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgCompCustomCode_setPosition_1_3(void* thisObj, CcfgEnum::CarbonCfgCustomCodePosition position)
{
    ProtoCarbonCfgCompCustomCode* obj = (ProtoCarbonCfgCompCustomCode*)(thisObj);
    obj->setPosition(position);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgCompCustomCode_getCode_0_3(void* thisObj)
{
    ProtoCarbonCfgCompCustomCode* obj = (ProtoCarbonCfgCompCustomCode*)(thisObj);
    return EmbeddedMono::AllocString(obj->getCode());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgCompCustomCode_setCode_1_1(void* thisObj, const char* code)
{
    ProtoCarbonCfgCompCustomCode* obj = (ProtoCarbonCfgCompCustomCode*)(thisObj);
    obj->setCode(code);
}
PINVOKEABLE CcfgEnum::CarbonCfgCompCustomCodeSection CSharp_ProtoCarbonCfgCompCustomCode_getSection_0_7(void* thisObj)
{
    ProtoCarbonCfgCompCustomCode* obj = (ProtoCarbonCfgCompCustomCode*)(thisObj);
    return obj->getSection();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgCompCustomCode_setSection_1_3(void* thisObj, CcfgEnum::CarbonCfgCompCustomCodeSection section)
{
    ProtoCarbonCfgCompCustomCode* obj = (ProtoCarbonCfgCompCustomCode*)(thisObj);
    obj->setSection(section);
}
PINVOKEABLE ProtoCarbonCfgCustomCode* CSharp_ProtoCarbonCfgCompCustomCode_UpCast(ProtoCarbonCfgCompCustomCode* obj)
{
  return (ProtoCarbonCfgCustomCode*)(obj);
}
