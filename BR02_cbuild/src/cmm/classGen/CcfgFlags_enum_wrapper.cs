using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CcfgFlags
  {
    public enum CarbonCfgMemoryEditFlags
    {
      EditMemoryAll = -1,
      EditMemoryNone = 0,
      EditMemoryName = 1,
      EditMemoryWidth = 2,
      EditMemorySystemAddressMapping = 4,
      EditMemorySystemAddressESLPortName = 8,
      EditMemorySystemAddressESLPortBaseAddress = 16,
      EditMemoryInitializationReadmemFormat = 32,
      EditMemoryInitializationReadmemFile = 64,
      EditMemoryBlockName = 128,
      EditMemoryMaxAddress = 256,
      EditMemoryDeletions = 512,
      EditMemoryAdditions = 1024,
      EditMemoryBlockBaseAddress = 2048,
      EditMemoryBlockSize = 4096,
      EditMemoryBlockLocationAdditions = 8192,
      EditMemoryBlockLocationDeletions = 16384,
      EditMemoryBlockLocationRTLPath = 32768,
      EditMemoryBlockLocationDisplayStartWordOffset = 65536,
      EditMemoryBlockLocationDisplayEndWordOffset = 131072,
      EditMemoryBlockLocationDisplayStartLSB = 262144,
      EditMemoryBlockLocationDisplayStartMSB = 524288,
      EditMemoryBlockLocationRTLStartWordOffset = 1048576,
      EditMemoryBlockLocationRTLEndWordOffset = 2097152,
      EditMemoryBlockLocationRTLStartLSB = 4194304,
      EditMemoryBlockLocationRTLStartMSB = 8388608,
      EditMemoryBlockAdditions = 16777216,
      EditMemoryBlockDeletions = 33554432,
      EditMemoryCustomCodes = 67108864,
      EditMemoryBlockCustomCodes = 134217728
    }
    public enum CarbonCfgComponentEditFlags
    {
      EditComponentAll = -1,
      EditComponentNone = 0,
      EditComponentName = 1,
      EditComponentAdditions = 2,
      EditComponentDeletions = 4,
      EditComponentDisassembler = 8,
      EditComponentCustomCodes = 16
    }
  }
}
