using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgRTLPort : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgRTLPort(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgRTLPort obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgRTLPort()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_pObj_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRTLPort_pObj_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_isDisconnected_0_7")]
    private extern static bool CSharp_ProtoCarbonCfgRTLPort_isDisconnected_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_isConnected_0_7")]
    private extern static bool CSharp_ProtoCarbonCfgRTLPort_isConnected_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_isResetGen_0_7")]
    private extern static bool CSharp_ProtoCarbonCfgRTLPort_isResetGen_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_isClockGen_0_7")]
    private extern static bool CSharp_ProtoCarbonCfgRTLPort_isClockGen_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_isTied_0_7")]
    private extern static bool CSharp_ProtoCarbonCfgRTLPort_isTied_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_isTiedToParam_0_3")]
    private extern static bool CSharp_ProtoCarbonCfgRTLPort_isTiedToParam_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_isConnectedToXtor_0_7")]
    private extern static bool CSharp_ProtoCarbonCfgRTLPort_isConnectedToXtor_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_connectClockGen_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRTLPort_connectClockGen_1_3(HandleRef thisObj, IntPtr conn);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_connectResetGen_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRTLPort_connectResetGen_1_3(HandleRef thisObj, IntPtr conn);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_connectTie_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRTLPort_connectTie_1_3(HandleRef thisObj, IntPtr conn);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_connectTieParam_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRTLPort_connectTieParam_1_3(HandleRef thisObj, IntPtr conn);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_connectESLPort_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRTLPort_connectESLPort_1_3(HandleRef thisObj, IntPtr conn);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_connectXtorConn_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRTLPort_connectXtorConn_1_3(HandleRef thisObj, IntPtr conn);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_connectRTLConn_1_3")]
    private extern static void CSharp_ProtoCarbonCfgRTLPort_connectRTLConn_1_3(HandleRef thisObj, IntPtr conn);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_portType_0_35")]
    private extern static int CSharp_ProtoCarbonCfgRTLPort_portType_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_name_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRTLPort_name_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_width_0_7")]
    private extern static uint CSharp_ProtoCarbonCfgRTLPort_width_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_numConnections_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgRTLPort_numConnections_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLPort_getConnection_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRTLPort_getConnection_1_19(HandleRef thisObj, uint i);
    #endregion DllImports
    #region Properties
    public string Name
    {
      get
      {
        return name();
      }
    }
    public uint Width
    {
      get
      {
        return width();
      }
    }
    public CcfgEnum.CarbonCfgRTLPortType PortType
    {
      get
      {
        return portType();
      }
    }
    public uint NumConnections
    {
      get
      {
        return numConnections();
      }
    }
    #endregion Properties
    #region Methods
    public CarbonCfgRTLPort pObj()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRTLPort_pObj_0_7(objCPtr);
      CarbonCfgRTLPort ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRTLPort(cPtr, false);
      return ret;
    }

    public bool isDisconnected()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      return CSharp_ProtoCarbonCfgRTLPort_isDisconnected_0_7(objCPtr);
    }

    public bool isConnected()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      return CSharp_ProtoCarbonCfgRTLPort_isConnected_0_7(objCPtr);
    }

    public bool isResetGen()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      return CSharp_ProtoCarbonCfgRTLPort_isResetGen_0_7(objCPtr);
    }

    public bool isClockGen()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      return CSharp_ProtoCarbonCfgRTLPort_isClockGen_0_7(objCPtr);
    }

    public bool isTied()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      return CSharp_ProtoCarbonCfgRTLPort_isTied_0_7(objCPtr);
    }

    public bool isTiedToParam()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      return CSharp_ProtoCarbonCfgRTLPort_isTiedToParam_0_3(objCPtr);
    }

    public bool isConnectedToXtor()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      return CSharp_ProtoCarbonCfgRTLPort_isConnectedToXtor_0_7(objCPtr);
    }

    public void connectClockGen(CarbonCfgClockGen conn)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      CSharp_ProtoCarbonCfgRTLPort_connectClockGen_1_3(objCPtr, CarbonCfgClockGen.getCPtr(conn).Handle);
    }

    public void connectResetGen(CarbonCfgResetGen conn)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      CSharp_ProtoCarbonCfgRTLPort_connectResetGen_1_3(objCPtr, CarbonCfgResetGen.getCPtr(conn).Handle);
    }

    public void connectTie(CarbonCfgTie conn)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      CSharp_ProtoCarbonCfgRTLPort_connectTie_1_3(objCPtr, CarbonCfgTie.getCPtr(conn).Handle);
    }

    public void connectTieParam(CarbonCfgTieParam conn)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      CSharp_ProtoCarbonCfgRTLPort_connectTieParam_1_3(objCPtr, CarbonCfgTieParam.getCPtr(conn).Handle);
    }

    public void connectESLPort(CarbonCfgESLPort conn)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      CSharp_ProtoCarbonCfgRTLPort_connectESLPort_1_3(objCPtr, CarbonCfgESLPort.getCPtr(conn).Handle);
    }

    public void connectXtorConn(CarbonCfgXtorConn conn)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      CSharp_ProtoCarbonCfgRTLPort_connectXtorConn_1_3(objCPtr, CarbonCfgXtorConn.getCPtr(conn).Handle);
    }

    public void connectRTLConn(CarbonCfgRTLConnection conn)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      CSharp_ProtoCarbonCfgRTLPort_connectRTLConn_1_3(objCPtr, CarbonCfgRTLConnection.getCPtr(conn).Handle);
    }

    public CcfgEnum.CarbonCfgRTLPortType portType()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      return (CcfgEnum.CarbonCfgRTLPortType)CSharp_ProtoCarbonCfgRTLPort_portType_0_35(objCPtr);
    }

    public string name()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      IntPtr ipSr = CSharp_ProtoCarbonCfgRTLPort_name_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public uint width()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      return CSharp_ProtoCarbonCfgRTLPort_width_0_7(objCPtr);
    }

    public uint numConnections()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      return CSharp_ProtoCarbonCfgRTLPort_numConnections_0_3(objCPtr);
    }

    public CarbonCfgRTLConnection getConnection(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLPort object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRTLPort_getConnection_1_19(objCPtr, i);
      CarbonCfgRTLConnection ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRTLConnection(cPtr, false);
      return ret;
    }

    #endregion Methods

  }

}
