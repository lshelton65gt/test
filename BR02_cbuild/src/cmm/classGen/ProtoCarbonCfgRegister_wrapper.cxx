PINVOKEABLE void CSharp_ProtoCarbonCfgRegister_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegister_addField_1_3(void* thisObj, CarbonCfgRegisterField* field)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    obj->addField(field);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegister_removeField_1_3(void* thisObj, CarbonCfgRegisterField* field)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    obj->removeField(field);
}
PINVOKEABLE CcfgEnum::CarbonCfgRadix CSharp_ProtoCarbonCfgRegister_getRadix_0_7(void* thisObj)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    return obj->getRadix();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegister_setRadix_1_3(void* thisObj, CcfgEnum::CarbonCfgRadix newVal)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    obj->setRadix(newVal);
}
PINVOKEABLE CarbonCfgRegisterField* CSharp_ProtoCarbonCfgRegister_getField_1_19(void* thisObj, unsigned int index)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    return obj->getField(index);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegister_numFields_0_3(void* thisObj)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    return obj->numFields();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgRegister_getGroupName_0_3(void* thisObj)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    return EmbeddedMono::AllocString(obj->getGroupName());
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgRegister_getPort_0_7(void* thisObj)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    return EmbeddedMono::AllocString(obj->getPort());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegister_setPort_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    obj->setPort(newVal);
}
PINVOKEABLE bool CSharp_ProtoCarbonCfgRegister_getBigEndian_0_7(void* thisObj)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    return obj->getBigEndian();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegister_setBigEndian_1_3(void* thisObj, bool nv)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    obj->setBigEndian(nv);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegister_getOffset_0_7(void* thisObj)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    return obj->getOffset();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegister_setOffset_1_19(void* thisObj, unsigned int nv)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    obj->setOffset(nv);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgRegister_getComment_0_7(void* thisObj)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    return EmbeddedMono::AllocString(obj->getComment());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegister_setComment_1_1(void* thisObj, const char* newVal)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    obj->setComment(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegister_getWidth_0_7(void* thisObj)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    return obj->getWidth();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegister_setWidth_1_19(void* thisObj, unsigned int nv)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    obj->setWidth(nv);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgRegister_getName_0_3(void* thisObj)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    return EmbeddedMono::AllocString(obj->getName());
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgRegister_numCustomCodes_0_3(void* thisObj)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    return obj->numCustomCodes();
}
PINVOKEABLE CarbonCfgRegCustomCode* CSharp_ProtoCarbonCfgRegister_addCustomCode_0_3(void* thisObj)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    return obj->addCustomCode();
}
PINVOKEABLE CarbonCfgRegCustomCode* CSharp_ProtoCarbonCfgRegister_getCustomCode_1_19(void* thisObj, unsigned int i)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    return obj->getCustomCode(i);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgRegister_removeCustomCode_1_3(void* thisObj, CarbonCfgRegCustomCode* customCode)
{
    ProtoCarbonCfgRegister* obj = (ProtoCarbonCfgRegister*)(thisObj);
    obj->removeCustomCode(customCode);
}
