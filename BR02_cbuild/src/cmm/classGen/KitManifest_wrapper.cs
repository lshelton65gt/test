using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class Manifest : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal Manifest(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(Manifest obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~Manifest()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_getRelease_0_3")]
    private extern static IntPtr CSharp_KitManifest_getRelease_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_getSoftwareVersion_0_7")]
    private extern static IntPtr CSharp_KitManifest_getSoftwareVersion_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_getID_0_7")]
    private extern static uint CSharp_KitManifest_getID_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_numRTLFiles_0_3")]
    private extern static uint CSharp_KitManifest_numRTLFiles_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_numFiles_0_3")]
    private extern static uint CSharp_KitManifest_numFiles_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_numUserFiles_0_3")]
    private extern static uint CSharp_KitManifest_numUserFiles_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_numCompilerSwitches_0_3")]
    private extern static uint CSharp_KitManifest_numCompilerSwitches_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_numModelKitFeatures_0_3")]
    private extern static uint CSharp_KitManifest_numModelKitFeatures_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_decryptFile_1_1")]
    private extern static bool CSharp_KitManifest_decryptFile_1_1(HandleRef thisObj, string inputFileName);
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_encryptFile_1_1")]
    private extern static bool CSharp_KitManifest_encryptFile_1_1(HandleRef thisObj, string inputFileName);
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_encryptBuffer_1_1")]
    private extern static IntPtr CSharp_KitManifest_encryptBuffer_1_1(HandleRef thisObj, string buffer);
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_decryptBuffer_1_1")]
    private extern static IntPtr CSharp_KitManifest_decryptBuffer_1_1(HandleRef thisObj, string buffer);
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_getCompilerSwitch_1_19")]
    private extern static IntPtr CSharp_KitManifest_getCompilerSwitch_1_19(HandleRef thisObj, uint index);
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_getUserFile_1_19")]
    private extern static IntPtr CSharp_KitManifest_getUserFile_1_19(HandleRef thisObj, uint index);
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_getRTLFile_1_19")]
    private extern static IntPtr CSharp_KitManifest_getRTLFile_1_19(HandleRef thisObj, uint index);
    [DllImport("__Internal", EntryPoint="CSharp_KitManifest_getFile_1_19")]
    private extern static IntPtr CSharp_KitManifest_getFile_1_19(HandleRef thisObj, uint index);
    #endregion DllImports
    #region Properties
    public string Release
    {
      get
      {
        return getRelease();
      }
    }
    public string SoftwareVersion
    {
      get
      {
        return getSoftwareVersion();
      }
    }
    public uint NumRTLFiles
    {
      get
      {
        return numRTLFiles();
      }
    }
    public uint NumFiles
    {
      get
      {
        return numFiles();
      }
    }
    public uint NumUserFiles
    {
      get
      {
        return numUserFiles();
      }
    }
    public uint NumCompilerSwitches
    {
      get
      {
        return numCompilerSwitches();
      }
    }
    public uint NumModelKitFeatures
    {
      get
      {
        return numModelKitFeatures();
      }
    }
    #endregion Properties
    #region Methods
    public string getRelease()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Manifest object");
      IntPtr ipSr = CSharp_KitManifest_getRelease_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getSoftwareVersion()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Manifest object");
      IntPtr ipSr = CSharp_KitManifest_getSoftwareVersion_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public uint getID()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Manifest object");
      return CSharp_KitManifest_getID_0_7(objCPtr);
    }

    public uint numRTLFiles()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Manifest object");
      return CSharp_KitManifest_numRTLFiles_0_3(objCPtr);
    }

    public uint numFiles()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Manifest object");
      return CSharp_KitManifest_numFiles_0_3(objCPtr);
    }

    public uint numUserFiles()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Manifest object");
      return CSharp_KitManifest_numUserFiles_0_3(objCPtr);
    }

    public uint numCompilerSwitches()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Manifest object");
      return CSharp_KitManifest_numCompilerSwitches_0_3(objCPtr);
    }

    public uint numModelKitFeatures()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Manifest object");
      return CSharp_KitManifest_numModelKitFeatures_0_3(objCPtr);
    }

    public bool decryptFile(string inputFileName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Manifest object");
      return CSharp_KitManifest_decryptFile_1_1(objCPtr, inputFileName);
    }

    public bool encryptFile(string inputFileName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Manifest object");
      return CSharp_KitManifest_encryptFile_1_1(objCPtr, inputFileName);
    }

    public string encryptBuffer(string buffer)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Manifest object");
      IntPtr ipSr = CSharp_KitManifest_encryptBuffer_1_1(objCPtr, buffer);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string decryptBuffer(string buffer)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Manifest object");
      IntPtr ipSr = CSharp_KitManifest_decryptBuffer_1_1(objCPtr, buffer);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getCompilerSwitch(uint index)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Manifest object");
      IntPtr ipSr = CSharp_KitManifest_getCompilerSwitch_1_19(objCPtr, index);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    #endregion Methods

  }

}
