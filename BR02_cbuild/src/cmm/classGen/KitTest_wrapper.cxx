PINVOKEABLE void CSharp_KitTest_deleteLater_0_7(void* thisObj)
{
    KitTest* obj = (KitTest*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE char* CSharp_KitTest_getModelKitName_0_3(void* thisObj)
{
    KitTest* obj = (KitTest*)(thisObj);
    return EmbeddedMono::AllocString(obj->getModelKitName());
}
PINVOKEABLE void CSharp_KitTest_setModelKitName_1_1(void* thisObj, const char* kitName)
{
    KitTest* obj = (KitTest*)(thisObj);
    obj->setModelKitName(kitName);
}
PINVOKEABLE KitAnswers* CSharp_KitTest_getAnswers_0_3(void* thisObj)
{
    KitTest* obj = (KitTest*)(thisObj);
    return obj->getAnswers();
}
PINVOKEABLE KitAnswerGroups* CSharp_KitTest_getAnswerGroups_0_3(void* thisObj)
{
    KitTest* obj = (KitTest*)(thisObj);
    return obj->getAnswerGroups();
}
