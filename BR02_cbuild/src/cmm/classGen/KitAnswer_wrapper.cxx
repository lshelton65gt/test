PINVOKEABLE void CSharp_KitAnswer_deleteLater_0_7(void* thisObj)
{
    KitAnswer* obj = (KitAnswer*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE char* CSharp_KitAnswer_getID_0_7(void* thisObj)
{
    KitAnswer* obj = (KitAnswer*)(thisObj);
    return EmbeddedMono::AllocString(obj->getID());
}
PINVOKEABLE void CSharp_KitAnswer_setID_1_1(void* thisObj, const char* newVal)
{
    KitAnswer* obj = (KitAnswer*)(thisObj);
    obj->setID(newVal);
}
PINVOKEABLE char* CSharp_KitAnswer_getDisplayValue_0_3(void* thisObj)
{
    KitAnswer* obj = (KitAnswer*)(thisObj);
    return EmbeddedMono::AllocString(obj->getDisplayValue());
}
PINVOKEABLE void CSharp_KitAnswer_setDisplayValue_1_1(void* thisObj, const char* newVal)
{
    KitAnswer* obj = (KitAnswer*)(thisObj);
    obj->setDisplayValue(newVal);
}
PINVOKEABLE char* CSharp_KitAnswer_getValue_0_3(void* thisObj)
{
    KitAnswer* obj = (KitAnswer*)(thisObj);
    return EmbeddedMono::AllocString(obj->getValue());
}
PINVOKEABLE void CSharp_KitAnswer_setValue_1_1(void* thisObj, const char* newVal)
{
    KitAnswer* obj = (KitAnswer*)(thisObj);
    obj->setValue(newVal);
}
