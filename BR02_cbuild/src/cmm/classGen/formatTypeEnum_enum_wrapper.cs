using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class formatTypeEnum
  {
    public enum formatType
    {
      BITSTRING = 0,
      BOOL = 1,
      FLOAT = 2,
      LONG = 3,
      STRING = 4
    }
  }
}
