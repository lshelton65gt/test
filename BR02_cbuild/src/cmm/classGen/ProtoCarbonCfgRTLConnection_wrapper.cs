using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgRTLConnection : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgRTLConnection(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgRTLConnection obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgRTLConnection()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLConnection_getType_0_35")]
    private extern static int CSharp_ProtoCarbonCfgRTLConnection_getType_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLConnection_getRTLPort_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRTLConnection_getRTLPort_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLConnection_getClockGen_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRTLConnection_getClockGen_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLConnection_getResetGen_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRTLConnection_getResetGen_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLConnection_getTie_0_35")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRTLConnection_getTie_0_35(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLConnection_getTieParam_0_3")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRTLConnection_getTieParam_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLConnection_getXtorConn_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRTLConnection_getXtorConn_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgRTLConnection_getESLPort_0_7")]
    private extern static IntPtr CSharp_ProtoCarbonCfgRTLConnection_getESLPort_0_7(HandleRef thisObj);
    #endregion DllImports
    #region Properties
    public CcfgEnum.CarbonCfgRTLConnectionType Type
    {
      get
      {
        return getType();
      }
    }
    public CarbonCfgClockGen ClockGen
    {
      get
      {
        return getClockGen();
      }
    }
    public CarbonCfgResetGen ResetGen
    {
      get
      {
        return getResetGen();
      }
    }
    public CarbonCfgTie Tie
    {
      get
      {
        return getTie();
      }
    }
    public CarbonCfgTieParam TieParam
    {
      get
      {
        return getTieParam();
      }
    }
    public CarbonCfgXtorConn XtorConn
    {
      get
      {
        return getXtorConn();
      }
    }
    #endregion Properties
    #region Methods
    public CcfgEnum.CarbonCfgRTLConnectionType getType()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLConnection object");
      return (CcfgEnum.CarbonCfgRTLConnectionType)CSharp_ProtoCarbonCfgRTLConnection_getType_0_35(objCPtr);
    }

    public CarbonCfgRTLPort getRTLPort()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLConnection object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRTLConnection_getRTLPort_0_7(objCPtr);
      CarbonCfgRTLPort ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgRTLPort(cPtr, false);
      return ret;
    }

    public CarbonCfgClockGen getClockGen()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLConnection object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRTLConnection_getClockGen_0_7(objCPtr);
      CarbonCfgClockGen ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgClockGen(cPtr, false);
      return ret;
    }

    public CarbonCfgResetGen getResetGen()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLConnection object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRTLConnection_getResetGen_0_7(objCPtr);
      CarbonCfgResetGen ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgResetGen(cPtr, false);
      return ret;
    }

    public CarbonCfgTie getTie()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLConnection object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRTLConnection_getTie_0_35(objCPtr);
      CarbonCfgTie ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgTie(cPtr, false);
      return ret;
    }

    public CarbonCfgTieParam getTieParam()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLConnection object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRTLConnection_getTieParam_0_3(objCPtr);
      CarbonCfgTieParam ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgTieParam(cPtr, false);
      return ret;
    }

    public CarbonCfgXtorConn getXtorConn()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLConnection object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRTLConnection_getXtorConn_0_7(objCPtr);
      CarbonCfgXtorConn ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtorConn(cPtr, false);
      return ret;
    }

    public CarbonCfgESLPort getESLPort()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgRTLConnection object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgRTLConnection_getESLPort_0_7(objCPtr);
      CarbonCfgESLPort ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgESLPort(cPtr, false);
      return ret;
    }

    #endregion Methods

  }

}
