PINVOKEABLE void CSharp_ProtoCarbonCfgELFLoader_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgELFLoader* obj = (ProtoCarbonCfgELFLoader*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgELFLoader_addSection_3_1(void* thisObj, const char* name, unsigned int space, const char* access)
{
    ProtoCarbonCfgELFLoader* obj = (ProtoCarbonCfgELFLoader*)(thisObj);
    obj->addSection(name, space, access);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgELFLoader_numSections_0_3(void* thisObj)
{
    ProtoCarbonCfgELFLoader* obj = (ProtoCarbonCfgELFLoader*)(thisObj);
    return obj->numSections();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgELFLoader_getSectionName_1_19(void* thisObj, unsigned int index)
{
    ProtoCarbonCfgELFLoader* obj = (ProtoCarbonCfgELFLoader*)(thisObj);
    return EmbeddedMono::AllocString(obj->getSectionName(index));
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgELFLoader_getSectionSpace_1_19(void* thisObj, unsigned int index)
{
    ProtoCarbonCfgELFLoader* obj = (ProtoCarbonCfgELFLoader*)(thisObj);
    return obj->getSectionSpace(index);
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgELFLoader_getSectionAccess_1_19(void* thisObj, unsigned int index)
{
    ProtoCarbonCfgELFLoader* obj = (ProtoCarbonCfgELFLoader*)(thisObj);
    return EmbeddedMono::AllocString(obj->getSectionAccess(index));
}
PINVOKEABLE void CSharp_ProtoCarbonCfgELFLoader_removeSections_0_3(void* thisObj)
{
    ProtoCarbonCfgELFLoader* obj = (ProtoCarbonCfgELFLoader*)(thisObj);
    obj->removeSections();
}
