using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class busInterfaceTypeMonitorInterfaceModeEnum
  {
    public enum busInterfaceTypeMonitorInterfaceMode
    {
      MASTER = 0,
      SLAVE = 1,
      SYSTEM = 2,
      MIRROREDMASTER = 3,
      MIRROREDSLAVE = 4,
      MIRROREDSYSTEM = 5
    }
  }
}
