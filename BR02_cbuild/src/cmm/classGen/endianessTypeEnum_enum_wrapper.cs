using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class endianessTypeEnum
  {
    public enum endianessType
    {
      BIG = 0,
      LITTLE = 1
    }
  }
}
