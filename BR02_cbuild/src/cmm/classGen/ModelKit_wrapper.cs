using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class ModelKit : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal ModelKit(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(ModelKit obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~ModelKit()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_retainFile_1_1")]
    private extern static void CSharp_ModelKit_retainFile_1_1(HandleRef thisObj, string fileName);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_retainFile_2_1")]
    private extern static void CSharp_ModelKit_retainFile_2_1(HandleRef thisObj, string fileName, string subdirName);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getPrefaceScriptActual_0_7")]
    private extern static IntPtr CSharp_ModelKit_getPrefaceScriptActual_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getEpilogueScriptActual_0_7")]
    private extern static IntPtr CSharp_ModelKit_getEpilogueScriptActual_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getPrefaceScriptNormalized_0_7")]
    private extern static IntPtr CSharp_ModelKit_getPrefaceScriptNormalized_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getEpilogueScriptNormalized_0_7")]
    private extern static IntPtr CSharp_ModelKit_getEpilogueScriptNormalized_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getRevision_0_7")]
    private extern static IntPtr CSharp_ModelKit_getRevision_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getComponentName_0_3")]
    private extern static IntPtr CSharp_ModelKit_getComponentName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_setComponentName_1_1")]
    private extern static void CSharp_ModelKit_setComponentName_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getRelease_0_3")]
    private extern static IntPtr CSharp_ModelKit_getRelease_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getSoftwareVersion_0_7")]
    private extern static IntPtr CSharp_ModelKit_getSoftwareVersion_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_setCopyFiles_1_3")]
    private extern static void CSharp_ModelKit_setCopyFiles_1_3(HandleRef thisObj, bool newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getCopyFiles_0_3")]
    private extern static bool CSharp_ModelKit_getCopyFiles_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_setAskRoot_1_3")]
    private extern static void CSharp_ModelKit_setAskRoot_1_3(HandleRef thisObj, bool newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getAskRoot_0_7")]
    private extern static bool CSharp_ModelKit_getAskRoot_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getName_0_3")]
    private extern static IntPtr CSharp_ModelKit_getName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_setName_1_1")]
    private extern static void CSharp_ModelKit_setName_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getDescription_0_7")]
    private extern static IntPtr CSharp_ModelKit_getDescription_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_setDescription_1_1")]
    private extern static void CSharp_ModelKit_setDescription_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getLicenseKey_0_3")]
    private extern static IntPtr CSharp_ModelKit_getLicenseKey_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_setLicenseKey_1_1")]
    private extern static void CSharp_ModelKit_setLicenseKey_1_1(HandleRef thisObj, string newVal);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getVersions_0_3")]
    private extern static IntPtr CSharp_ModelKit_getVersions_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getManifests_0_3")]
    private extern static IntPtr CSharp_ModelKit_getManifests_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ModelKit_getLatestVersion_0_7")]
    private extern static IntPtr CSharp_ModelKit_getLatestVersion_0_7(HandleRef thisObj);
    #endregion DllImports
    #region Properties
    public bool CopyFiles
    {
      get
      {
        return getCopyFiles();
      }
    }
    public bool AskRoot
    {
      get
      {
        return getAskRoot();
      }
    }
    public string Name
    {
      get
      {
        return getName();
      }
    }
    public string Description
    {
      get
      {
        return getDescription();
      }
    }
    public string Release
    {
      get
      {
        return getRelease();
      }
    }
    public string SoftwareVersion
    {
      get
      {
        return getSoftwareVersion();
      }
    }
    public string ComponentName
    {
      set
      {
        setComponentName(value);
      }
      get
      {
        return getComponentName();
      }
    }
    public string Revision
    {
      get
      {
        return getRevision();
      }
    }
    #endregion Properties
    #region Methods
    public void retainFile(string fileName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      CSharp_ModelKit_retainFile_1_1(objCPtr, fileName);
    }

    public void retainFile(string fileName, string subdirName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      CSharp_ModelKit_retainFile_2_1(objCPtr, fileName, subdirName);
    }

    public string getPrefaceScriptActual()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      IntPtr ipSr = CSharp_ModelKit_getPrefaceScriptActual_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getEpilogueScriptActual()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      IntPtr ipSr = CSharp_ModelKit_getEpilogueScriptActual_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getPrefaceScriptNormalized()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      IntPtr ipSr = CSharp_ModelKit_getPrefaceScriptNormalized_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getEpilogueScriptNormalized()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      IntPtr ipSr = CSharp_ModelKit_getEpilogueScriptNormalized_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getRevision()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      IntPtr ipSr = CSharp_ModelKit_getRevision_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getComponentName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      IntPtr ipSr = CSharp_ModelKit_getComponentName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setComponentName(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      CSharp_ModelKit_setComponentName_1_1(objCPtr, newVal);
    }

    public string getRelease()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      IntPtr ipSr = CSharp_ModelKit_getRelease_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getSoftwareVersion()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      IntPtr ipSr = CSharp_ModelKit_getSoftwareVersion_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setCopyFiles(bool newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      CSharp_ModelKit_setCopyFiles_1_3(objCPtr, newVal);
    }

    public bool getCopyFiles()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      return CSharp_ModelKit_getCopyFiles_0_3(objCPtr);
    }

    public void setAskRoot(bool newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      CSharp_ModelKit_setAskRoot_1_3(objCPtr, newVal);
    }

    public bool getAskRoot()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      return CSharp_ModelKit_getAskRoot_0_7(objCPtr);
    }

    public string getName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      IntPtr ipSr = CSharp_ModelKit_getName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setName(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      CSharp_ModelKit_setName_1_1(objCPtr, newVal);
    }

    public string getDescription()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      IntPtr ipSr = CSharp_ModelKit_getDescription_0_7(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setDescription(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      CSharp_ModelKit_setDescription_1_1(objCPtr, newVal);
    }

    public string getLicenseKey()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      IntPtr ipSr = CSharp_ModelKit_getLicenseKey_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setLicenseKey(string newVal)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      CSharp_ModelKit_setLicenseKey_1_1(objCPtr, newVal);
    }

    public Versions getVersions()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL ModelKit object");
      IntPtr cPtr = CSharp_ModelKit_getVersions_0_3(objCPtr);
      Versions ret = (cPtr == IntPtr.Zero) ? null : new Versions(cPtr, false);
      return ret;
    }

    #endregion Methods

  }

}
