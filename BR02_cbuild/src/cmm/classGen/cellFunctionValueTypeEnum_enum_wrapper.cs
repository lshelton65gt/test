using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class cellFunctionValueTypeEnum
  {
    public enum cellFunctionValueType
    {
      NAND2 = 0,
      BUF = 1,
      INV = 2,
      MUX21 = 3,
      DFF = 4,
      LATCH = 5,
      XOR2 = 6
    }
  }
}
