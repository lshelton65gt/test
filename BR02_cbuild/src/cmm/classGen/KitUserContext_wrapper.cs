using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class Context : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal Context(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(Context obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~Context()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getModelTarget_0_7")]
    private extern static int CSharp_KitUserContext_getModelTarget_0_7(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getModelSymbols_0_3")]
    private extern static int CSharp_KitUserContext_getModelSymbols_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getComponentName_0_3")]
    private extern static IntPtr CSharp_KitUserContext_getComponentName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getLibraryName_0_3")]
    private extern static IntPtr CSharp_KitUserContext_getLibraryName_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_setStatusMessage_1_1")]
    private extern static void CSharp_KitUserContext_setStatusMessage_1_1(HandleRef thisObj, string msg);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_setProgress_1_7")]
    private extern static void CSharp_KitUserContext_setProgress_1_7(HandleRef thisObj, int value);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_setProgressRange_2_7")]
    private extern static void CSharp_KitUserContext_setProgressRange_2_7(HandleRef thisObj, int low, int high);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getDouble_7_7")]
    private extern static double CSharp_KitUserContext_getDouble_7_7(HandleRef thisObj, string questionId, string title, string label, double value, double minValue, double maxValue, int decimals);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getDouble_6_5")]
    private extern static double CSharp_KitUserContext_getDouble_6_5(HandleRef thisObj, string questionId, string title, string label, double value, double minValue, double maxValue);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getDouble_5_5")]
    private extern static double CSharp_KitUserContext_getDouble_5_5(HandleRef thisObj, string questionId, string title, string label, double value, double minValue);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getDouble_4_5")]
    private extern static double CSharp_KitUserContext_getDouble_4_5(HandleRef thisObj, string questionId, string title, string label, double value);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getDouble_3_1")]
    private extern static double CSharp_KitUserContext_getDouble_3_1(HandleRef thisObj, string questionId, string title, string label);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getInteger_7_7")]
    private extern static int CSharp_KitUserContext_getInteger_7_7(HandleRef thisObj, string questionId, string title, string label, int value, int minValue, int maxValue, int step);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getInteger_6_7")]
    private extern static int CSharp_KitUserContext_getInteger_6_7(HandleRef thisObj, string questionId, string title, string label, int value, int minValue, int maxValue);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getInteger_5_7")]
    private extern static int CSharp_KitUserContext_getInteger_5_7(HandleRef thisObj, string questionId, string title, string label, int value, int minValue);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getInteger_4_7")]
    private extern static int CSharp_KitUserContext_getInteger_4_7(HandleRef thisObj, string questionId, string title, string label, int value);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getInteger_3_1")]
    private extern static int CSharp_KitUserContext_getInteger_3_1(HandleRef thisObj, string questionId, string title, string label);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getText_3_1")]
    private extern static IntPtr CSharp_KitUserContext_getText_3_1(HandleRef thisObj, string questionId, string title, string label);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getOpenFileName_5_1")]
    private extern static IntPtr CSharp_KitUserContext_getOpenFileName_5_1(HandleRef thisObj, string questionId, string caption, string initialDir, string filter, string selectedFilter);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getExistingDirectory_3_1")]
    private extern static IntPtr CSharp_KitUserContext_getExistingDirectory_3_1(HandleRef thisObj, string questionId, string caption, string dir);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_expandName_1_1")]
    private extern static IntPtr CSharp_KitUserContext_expandName_1_1(HandleRef thisObj, string inputName);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_setVariable_2_1")]
    private extern static void CSharp_KitUserContext_setVariable_2_1(HandleRef thisObj, string varName, string value);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_lookupVariable_1_1")]
    private extern static IntPtr CSharp_KitUserContext_lookupVariable_1_1(HandleRef thisObj, string varName);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getManifest_0_39")]
    private extern static IntPtr CSharp_KitUserContext_getManifest_0_39(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_getPlaybackMode_0_3")]
    private extern static bool CSharp_KitUserContext_getPlaybackMode_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_KitUserContext_reportError_1_1")]
    private extern static void CSharp_KitUserContext_reportError_1_1(HandleRef thisObj, string errorMessage);
    #endregion DllImports
    #region Properties
    public string ComponentName
    {
      get
      {
        return getComponentName();
      }
    }
    public string LibraryName
    {
      get
      {
        return getLibraryName();
      }
    }
    public ContextEnums.ModelTarget ModelTarget
    {
      get
      {
        return getModelTarget();
      }
    }
    #endregion Properties
    #region Methods
    public ContextEnums.ModelTarget getModelTarget()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      return (ContextEnums.ModelTarget)CSharp_KitUserContext_getModelTarget_0_7(objCPtr);
    }

    public ContextEnums.ModelSymbols getModelSymbols()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      return (ContextEnums.ModelSymbols)CSharp_KitUserContext_getModelSymbols_0_3(objCPtr);
    }

    public string getComponentName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      IntPtr ipSr = CSharp_KitUserContext_getComponentName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getLibraryName()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      IntPtr ipSr = CSharp_KitUserContext_getLibraryName_0_3(objCPtr);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setStatusMessage(string msg)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      CSharp_KitUserContext_setStatusMessage_1_1(objCPtr, msg);
    }

    public void setProgress(int value)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      CSharp_KitUserContext_setProgress_1_7(objCPtr, value);
    }

    public void setProgressRange(int low, int high)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      CSharp_KitUserContext_setProgressRange_2_7(objCPtr, low, high);
    }

    public double getDouble(string questionId, string title, string label, double value, double minValue, double maxValue, int decimals)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      return CSharp_KitUserContext_getDouble_7_7(objCPtr, questionId, title, label, value, minValue, maxValue, decimals);
    }

    public double getDouble(string questionId, string title, string label, double value, double minValue, double maxValue)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      return CSharp_KitUserContext_getDouble_6_5(objCPtr, questionId, title, label, value, minValue, maxValue);
    }

    public double getDouble(string questionId, string title, string label, double value, double minValue)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      return CSharp_KitUserContext_getDouble_5_5(objCPtr, questionId, title, label, value, minValue);
    }

    public double getDouble(string questionId, string title, string label, double value)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      return CSharp_KitUserContext_getDouble_4_5(objCPtr, questionId, title, label, value);
    }

    public double getDouble(string questionId, string title, string label)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      return CSharp_KitUserContext_getDouble_3_1(objCPtr, questionId, title, label);
    }

    public int getInteger(string questionId, string title, string label, int value, int minValue, int maxValue, int step)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      return CSharp_KitUserContext_getInteger_7_7(objCPtr, questionId, title, label, value, minValue, maxValue, step);
    }

    public int getInteger(string questionId, string title, string label, int value, int minValue, int maxValue)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      return CSharp_KitUserContext_getInteger_6_7(objCPtr, questionId, title, label, value, minValue, maxValue);
    }

    public int getInteger(string questionId, string title, string label, int value, int minValue)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      return CSharp_KitUserContext_getInteger_5_7(objCPtr, questionId, title, label, value, minValue);
    }

    public int getInteger(string questionId, string title, string label, int value)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      return CSharp_KitUserContext_getInteger_4_7(objCPtr, questionId, title, label, value);
    }

    public int getInteger(string questionId, string title, string label)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      return CSharp_KitUserContext_getInteger_3_1(objCPtr, questionId, title, label);
    }

    public string getText(string questionId, string title, string label)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      IntPtr ipSr = CSharp_KitUserContext_getText_3_1(objCPtr, questionId, title, label);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getOpenFileName(string questionId, string caption, string initialDir, string filter, string selectedFilter)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      IntPtr ipSr = CSharp_KitUserContext_getOpenFileName_5_1(objCPtr, questionId, caption, initialDir, filter, selectedFilter);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string getExistingDirectory(string questionId, string caption, string dir)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      IntPtr ipSr = CSharp_KitUserContext_getExistingDirectory_3_1(objCPtr, questionId, caption, dir);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public string expandName(string inputName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      IntPtr ipSr = CSharp_KitUserContext_expandName_1_1(objCPtr, inputName);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public void setVariable(string varName, string value)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      CSharp_KitUserContext_setVariable_2_1(objCPtr, varName, value);
    }

    public string lookupVariable(string varName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      IntPtr ipSr = CSharp_KitUserContext_lookupVariable_1_1(objCPtr, varName);
      string result = Marshal.PtrToStringAnsi(ipSr);
      Marshal.FreeCoTaskMem(ipSr);
      return result;
    }

    public Manifest getManifest()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      IntPtr cPtr = CSharp_KitUserContext_getManifest_0_39(objCPtr);
      Manifest ret = (cPtr == IntPtr.Zero) ? null : new Manifest(cPtr, false);
      return ret;
    }

    public bool getPlaybackMode()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      return CSharp_KitUserContext_getPlaybackMode_0_3(objCPtr);
    }

    public void reportError(string errorMessage)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL Context object");
      CSharp_KitUserContext_reportError_1_1(objCPtr, errorMessage);
    }

    #endregion Methods

  }

}
