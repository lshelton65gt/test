PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryBlock_deleteLater_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    obj->deleteLater();
}
PINVOKEABLE char* CSharp_ProtoCarbonCfgMemoryBlock_getName_0_3(void* thisObj)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    return EmbeddedMono::AllocString(obj->getName());
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryBlock_setName_1_1(void* thisObj, const char* _name)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    obj->setName(_name);
}
PINVOKEABLE unsigned long long CSharp_ProtoCarbonCfgMemoryBlock_getBase_0_35(void* thisObj)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    return obj->getBase();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryBlock_setBase_1_7(void* thisObj, unsigned long long newVal)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    obj->setBase(newVal);
}
PINVOKEABLE unsigned long long CSharp_ProtoCarbonCfgMemoryBlock_getSize_0_3(void* thisObj)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    return obj->getSize();
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryBlock_setSize_1_7(void* thisObj, unsigned long long newVal)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    obj->setSize(newVal);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemoryBlock_getWidth_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    return obj->getWidth();
}
PINVOKEABLE CarbonCfgMemoryLocRTL* CSharp_ProtoCarbonCfgMemoryBlock_addLocRTL_2_19(void* thisObj, const char* path, unsigned int width)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    return obj->addLocRTL(path, width);
}
PINVOKEABLE CarbonCfgMemoryLocRTL* CSharp_ProtoCarbonCfgMemoryBlock_addLocRTL_1_1(void* thisObj, const char* path)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    return obj->addLocRTL(path);
}
PINVOKEABLE CarbonCfgMemoryLocRTL* CSharp_ProtoCarbonCfgMemoryBlock_addLocRTL_0_7(void* thisObj)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    return obj->addLocRTL();
}
PINVOKEABLE CarbonCfgMemoryLocPort* CSharp_ProtoCarbonCfgMemoryBlock_addLocPort_1_1(void* thisObj, const char* port_name)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    return obj->addLocPort(port_name);
}
PINVOKEABLE CarbonCfgMemoryLocUser* CSharp_ProtoCarbonCfgMemoryBlock_addLocUser_1_1(void* thisObj, const char* _name)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    return obj->addLocUser(_name);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryBlock_removeLocMemoryLoc_1_3(void* thisObj, CarbonCfgMemoryLoc* loc)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    obj->removeLocMemoryLoc(loc);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryBlock_removeLocPort_1_3(void* thisObj, CarbonCfgMemoryLocPort* loc)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    obj->removeLocPort(loc);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryBlock_removeLocRTL_1_3(void* thisObj, CarbonCfgMemoryLocRTL* loc)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    obj->removeLocRTL(loc);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryBlock_removeLocUser_1_3(void* thisObj, CarbonCfgMemoryLocUser* loc)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    obj->removeLocUser(loc);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemoryBlock_numLocs_0_3(void* thisObj)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    return obj->numLocs();
}
PINVOKEABLE CarbonCfgMemoryLoc* CSharp_ProtoCarbonCfgMemoryBlock_getLoc_1_19(void* thisObj, unsigned int index)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    return obj->getLoc(index);
}
PINVOKEABLE unsigned int CSharp_ProtoCarbonCfgMemoryBlock_numCustomCodes_0_3(void* thisObj)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    return obj->numCustomCodes();
}
PINVOKEABLE CarbonCfgMemoryBlockCustomCode* CSharp_ProtoCarbonCfgMemoryBlock_addCustomCode_0_3(void* thisObj)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    return obj->addCustomCode();
}
PINVOKEABLE CarbonCfgMemoryBlockCustomCode* CSharp_ProtoCarbonCfgMemoryBlock_getCustomCode_1_19(void* thisObj, unsigned int i)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    return obj->getCustomCode(i);
}
PINVOKEABLE void CSharp_ProtoCarbonCfgMemoryBlock_removeCustomCode_1_3(void* thisObj, CarbonCfgMemoryBlockCustomCode* customCode)
{
    ProtoCarbonCfgMemoryBlock* obj = (ProtoCarbonCfgMemoryBlock*)(thisObj);
    obj->removeCustomCode(customCode);
}
