using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class CarbonCfgXtorLib : IDisposable
  {
    private HandleRef objCPtr;
    protected bool cMemOwn;
    public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }
    internal CarbonCfgXtorLib(IntPtr cPtr, bool cMemoryOwn)
    {
      cMemOwn = cMemoryOwn;
      objCPtr = new HandleRef(this, cPtr);
    }
    public static HandleRef getCPtr(CarbonCfgXtorLib obj)
    {
       return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;
    }
    ~CarbonCfgXtorLib()
    {
      Dispose();
    }
    public virtual void Dispose()
    {
      lock(this)
      {
        if (objCPtr.Handle != IntPtr.Zero)
        {
          if (cMemOwn)
          {
            cMemOwn = false;
            // TBD: examplePINVOKE.delete_Shape(objCPtr);
          }
          objCPtr = new HandleRef(null, IntPtr.Zero);
        }
        GC.SuppressFinalize(this);
      }
    }

    #region DllImports
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorLib_numXtors_0_3")]
    private extern static uint CSharp_ProtoCarbonCfgXtorLib_numXtors_0_3(HandleRef thisObj);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorLib_getXtor_1_19")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorLib_getXtor_1_19(HandleRef thisObj, uint i);
    [DllImport("__Internal", EntryPoint="CSharp_ProtoCarbonCfgXtorLib_findXtor_1_1")]
    private extern static IntPtr CSharp_ProtoCarbonCfgXtorLib_findXtor_1_1(HandleRef thisObj, string xtorName);
    #endregion DllImports
    #region Properties
    public uint NumXtors
    {
      get
      {
        return numXtors();
      }
    }
    #endregion Properties
    #region Methods
    public uint numXtors()
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorLib object");
      return CSharp_ProtoCarbonCfgXtorLib_numXtors_0_3(objCPtr);
    }

    public CarbonCfgXtor getXtor(uint i)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorLib object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgXtorLib_getXtor_1_19(objCPtr, i);
      CarbonCfgXtor ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtor(cPtr, false);
      return ret;
    }

    public CarbonCfgXtor findXtor(string xtorName)
    {
      if (objCPtr.Handle == IntPtr.Zero) throw new Exception("Attempt to dereference NULL CarbonCfgXtorLib object");
      IntPtr cPtr = CSharp_ProtoCarbonCfgXtorLib_findXtor_1_1(objCPtr, xtorName);
      CarbonCfgXtor ret = (cPtr == IntPtr.Zero) ? null : new CarbonCfgXtor(cPtr, false);
      return ret;
    }

    #endregion Methods

  }

}
