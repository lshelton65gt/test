using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class bankAlignmentTypeEnum
  {
    public enum bankAlignmentType
    {
      SERIAL = 0,
      PARALLEL = 1
    }
  }
}
