#ifndef __SCRIPTINGENGINE__
#define __SCRIPTINGENGINE__

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>
#include <QtScriptTools>
#include <QUiLoader>
#include <QScriptEngineAgent>
#include <QScriptEngineDebugger>

class ProtoUIWidget : public QObject, public QScriptable
{
  Q_OBJECT

public:
  ProtoUIWidget(QObject *parent = 0) : QObject(parent) {}
  QWidget* pObj() const { return qscriptvalue_cast<QWidget*>(thisObject()); }

public slots:
  QWidget* findChild(const QString& name)
  {
    return pObj()->findChild<QWidget*>(name);
  }
};

class ScriptAgent : public QObject, public QScriptEngineAgent
{
  Q_OBJECT

public:
  ScriptAgent(QScriptEngine* engine=0, QObject* parent=0) : QObject(parent), QScriptEngineAgent(engine)
  {
    mLineNumber = -1;
    mColumnNumber = -1;
  }

private:
  virtual void scriptUnload(qint64 id)
  {
    qDebug() << "scriptUnload" << id;
    mScriptFiles.remove(id);
    emit scriptFinished();
  }
  virtual void scriptLoad(qint64 id, const QString &, const QString &fileName, int baseLineNumber)
  {
    qDebug() << "loaded" << id << fileName << baseLineNumber;
    mScriptFiles[id] = fileName;
    mFileName = fileName;
  }

  virtual void exceptionCatch(qint64 scriptId, const QScriptValue &exception)
  {
   qDebug() << "exceptionCatch" << scriptId << exception.toString();
  }
  virtual void positionChange(qint64 id, int lineNumber, int columnNumber)
  {
    mLineNumber = lineNumber;
    mColumnNumber = columnNumber;
    mScriptLines[id] = lineNumber;
    emit scriptPositionChanged(lineNumber, columnNumber);
  }

  virtual void exceptionThrow(qint64 scriptId, const QScriptValue &exception, bool hasHandler);

signals:
  void scriptPositionChanged(int lineNumber, int columnNumber);
  void scriptException(const QScriptValue &exception, int lineNumber);
  void scriptFinished();

private:
  QString mFileName;
  int mLineNumber;
  int mColumnNumber;
  QMap<qint64, QString> mScriptFiles;
  QMap<qint64, int> mScriptLines;
};

class ScriptingEngine :  public QScriptEngine
{
  Q_OBJECT

public:
  ScriptingEngine();
  ~ScriptingEngine();
  QScriptValue run(const QString& scriptFileName);
  QScriptValue runPlugin(const QString& scriptFileName);
  QScriptValue runScript(const QString& scriptFileName);
  QWidget* runScriptUI(const QString& scriptFileName, const QString& uiFileName, const QString& ctorName, QWidget* parent = 0);
  void setEnableDebugger(bool newVal=true, bool stopAtFirstLine=true);
  QScriptValue callScriptFunction(const QString& methodName, const QScriptValueList args);
  QScriptValue callOptionalScriptFunction(const QString& methodName, const QScriptValueList args);
  void addBuiltinFunctions();
  QWidget* loadUI(const QString& uiFile, QWidget* parent = 0);
  void registerProperty(const QString& propertyName, QObject* obj);
  void setBreakpoints(const QList<int>& bps) { mBreakPoints = bps;  }
  ScriptAgent* getAgent() { return mAgent; }

private slots:
  void scriptFinished();
  void executionHalted();
  void selected(const QModelIndex& index);
  void evaluationSuspended();
  void evaluationResumed();

private:
  void createEnumerationWrappers(QScriptValue value);

private:
  bool mDebugger;
  bool mDebugStopAtFirstLine;
  QScriptEngineDebugger mEmbeddedDebugger;
  QUiLoader mLoader;
  ScriptAgent* mAgent;
  QList<int> mBreakPoints;
};

#endif
