//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QtGui>

#include "util/XmlParsing.h"


#include "util/CarbonAssert.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"


#include "gui/CQt.h"
#include "CarbonProject.h"
#include "CarbonCompilerTool.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "DlgStringList.h"
#include "PropertyEditor.h"

#include "CarbonPropertyBool.h"
#define PROP_CARBON_EDIT "CarbonEditBox"
#define PROP_CARBON_FILTER "CarbonFileFilter"
#define PROP_CARBON_FILE "CarbonEditFileName"
#define PROP_CARBON_ITEM "CarbonFileWidget"
#define PROP_CARBON_SWITCHVALUE "CarbonPropertyValue"
#define ASSOCIATED_EDITOR "AssociatedEditor"



bool CarbonPropertyBool::parseXML(xmlNodePtr /*parent*/, UtXmlErrorHandler* /*eh*/)
{
  return true;
}

bool CarbonPropertyBool::readValueXML(xmlNodePtr parent, CarbonOptions* options, UtXmlErrorHandler* /*eh*/) const
{
  for (xmlNode *child = parent->children; child != NULL; child = child->next) 
  {
    const char* element = XmlParsing::elementName(child);
    if (element == NULL) // Skip non-interesting XML things
      continue;

    if (0 == strcmp(element, "Value"))
    {
      UtString value;
      XmlParsing::getContent(child, &value);
      options->putValue(this, value.c_str());
    }
  }
  return true;
}

bool CarbonPropertyBool::writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* /*eh*/)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Option");
  xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST getName());

  xmlTextWriterStartElement(writer, BAD_CAST "Value");
  xmlTextWriterWriteString(writer, BAD_CAST value->getValue());
  xmlTextWriterEndElement(writer);

  xmlTextWriterEndElement(writer);
  return true;
}

CarbonDelegate* CarbonPropertyBool::createDelegate(QObject* parent,  PropertyEditorDelegate* delegate, PropertyEditor* editor)
{
  return new CarbonDelegateBool(parent, delegate, editor);
}

// Delegate

// Editor Delegate
CarbonDelegateBool::CarbonDelegateBool(QObject *parent, PropertyEditorDelegate* delegate, PropertyEditor* editor)
: CarbonDelegate(parent,delegate,editor)
{
}

QWidget* CarbonDelegateBool::createEditor(QWidget *parent, QTreeWidgetItem* /*item*/, CarbonProperty* prop, const QModelIndex& /*index*/) const
{
    QComboBox* cbox = new QComboBox(parent);
    cbox->addItem("true");
    cbox->addItem("false");
    bool id,is;

    CarbonPropertyValue* sv = mEditor->getSettings()->getValue(prop,&id,&is);
    UtString value = sv->getValue();

    value.lowercase();
    if (value == "true")
      cbox->setCurrentIndex(cbox->findText("true"));
    else
      cbox->setCurrentIndex(cbox->findText("false"));

    connect(cbox, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexChanged(int)));

    return cbox;
}

void CarbonDelegateBool::currentIndexChanged(int i)
{
  if (i != -1)
  {
    QComboBox *widget = qobject_cast<QComboBox *>(sender());
    if (widget)
      mDelegate->commit(widget);
  }
}
void CarbonDelegateBool::setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const
{
  bool id,is;
  
  if (mEditor->getSettings() == NULL)
    return;

  CarbonPropertyValue* sv = mEditor->getSettings()->getValue(prop,&id,&is);
  if (sv == NULL)
    return;

  UtString currentValue = sv->getValue();

  QComboBox* cbox = qobject_cast<QComboBox *>(editor);
  if (cbox != NULL)
  {
    UtString value;
    value << cbox->currentText();

    QVariant qv = cbox->itemData(cbox->currentIndex());
    if (qv.isValid())
    {
      value.clear();
      value << qv.toString();
    }

    setValue(sv, item, model, index, currentValue.c_str(), value.c_str(), prop);
  }
}

