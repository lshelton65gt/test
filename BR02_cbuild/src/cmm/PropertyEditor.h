#ifndef __PROPERTYEDITOR_H__
#define __PROPERTYEDITOR_H__

#include "util/CarbonPlatform.h"

#include <QtGui>

class CarbonMakerContext;
class CarbonOptions;
class CarbonProperty;
class PropertyEditorDelegate;
class CarbonPropertyValue;
class PropertyTreeWidgetItem;

class PropertyEditor : public QTreeWidget
{
  Q_OBJECT

  enum Columns {colNAME=0, colVALUE};

public:
  enum SortType {Categorized, Alphabetical};
  PropertyEditor(QWidget *parent = 0);
  ~PropertyEditor();
  bool populate(CarbonOptions* settings, SortType sortType=Categorized);
  void putContext(CarbonMakerContext* ctx) { mContext=ctx; }

  CarbonMakerContext* getContext() { return mContext; }
  CarbonOptions* getSettings() { return mSettings; }

  friend class CarbonDelegate;
  friend class PropertyEditorDelegate;

  void sortAlphabetical();
  void sortCategorized();
  void resetSwitch();
  void resetAllSwitches();

  void putEditWidget(QWidget* editWidget) { mEditWidget=editWidget; }
  void commitEdit();
  void resetSwitchValue(CarbonProperty* sw, CarbonPropertyValue* sv);

signals:
  void resetButtonStateChanged(bool resetAll, bool resetSwitch);

private slots:
  void itemClicked(QTreeWidgetItem*,int);
  void itemSelectionChanged();

private:
  CarbonMakerContext* mContext;
  CarbonOptions* mSettings;
  PropertyEditorDelegate* mDelegate;
  bool mShutdown;
  QWidget* mEditWidget;
  SortType mLastSortType;
  QTreeWidgetItem* mSelectedItem;
};

// Abstract class
class CarbonDelegate : public QObject
{
 Q_OBJECT

public:
  CarbonDelegate(QObject *parent = 0, PropertyEditorDelegate* d=0, PropertyEditor* e=0) : QObject(parent)
  {
    mEditor=e;
    mDelegate=d;
  }
  virtual QWidget* createEditor(QWidget *parent, QTreeWidgetItem* item, CarbonProperty* prop, const QModelIndex &index) const=0;
  virtual void setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const=0;
  virtual void setEditorData(QWidget* /*editor*/, QTreeWidgetItem* /*item*/, CarbonProperty* /*prop*/, const QModelIndex &/*index*/) const {}

protected:
  virtual void setValue(CarbonPropertyValue* sv, QTreeWidgetItem* item, QAbstractItemModel *model, const QModelIndex &index, const char* currentValue, const char* newValue, const CarbonProperty* sw) const;

protected:
  PropertyEditor* mEditor;
  PropertyEditorDelegate* mDelegate;
};

// The single delegate which then delegates to a CarbonDelegate for
// each type
class PropertyEditorDelegate : public QItemDelegate
{
  Q_OBJECT

public:
    PropertyEditorDelegate(QObject *parent = 0, PropertyEditor* e=0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
        const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
        const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;

    void commit(QWidget* w)
    {
      commitData(w);
      closeEditor(w);
    }

private:
  void setValue(QTreeWidgetItem* item, QAbstractItemModel *model, const QModelIndex &index, const char* currentValue, const char* newValue, const CarbonProperty* sw) const;

private:
  PropertyEditor* mEditor;
};

class CarbonPropertyContext : public QObject
{
  Q_OBJECT

public:
  CarbonPropertyContext(CarbonOptions* options, CarbonProperty* prop, CarbonPropertyValue* propValue, CarbonDelegate* propDelegate, PropertyTreeWidgetItem* treeItem);
  virtual ~CarbonPropertyContext();

  CarbonProperty* getProperty() { return mProperty; }
  CarbonPropertyValue* getPropValue() { return mPropertyValue; }
  CarbonDelegate* getDelegate() { return mDelegate; }
  PropertyTreeWidgetItem* getTreeItem() { return mTreeItem; }

private slots:
  void propChanged(const CarbonProperty*, const char*);
  void destroyed(QObject* obj);

private:
  CarbonOptions* mOptions;
  CarbonProperty* mProperty;
  CarbonPropertyValue* mPropertyValue;
  CarbonDelegate* mDelegate;
  PropertyTreeWidgetItem* mTreeItem;
};

#endif
