//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "CarbonSourceGroups.h"
#include "CarbonProject.h"
#include "CarbonHDLSourceFile.h"
#include "CarbonOptions.h"
#include "CarbonProperties.h"
#include "CarbonCompilerTool.h"
#include "CarbonProjectNodes.h"
#include <QFileInfo>

CarbonSourceGroupTool::CarbonSourceGroupTool() : CarbonTool("RTL Properties", ":cmm/Resources/RTLFolderProperties.xml")
{
  mProperties.readPropertyDefinitions(":/cmm/Resources/RTLFolderProperties.xml");
  mDefaultOptions = new CarbonOptions(NULL, getProperties(), "RTLPropertiesDefaults");
  mOptions = new CarbonOptions(mDefaultOptions, getProperties(), "RTL Properties");
}


// Source Groups
CarbonSourceGroups::CarbonSourceGroups(CarbonProject* proj)
{
  mProject=proj;

  mProperties.readPropertyDefinitions(":/cmm/Resources/VSPCompilerFileProperties.xml");
  mDefaultOptions = new CarbonOptions(NULL, getProperties(), "VSPCompilerFileDefaults");

  CQT_CONNECT(proj, configurationRenamed(const char*, const char*), this, configurationRenamed(const char*, const char*));
  CQT_CONNECT(proj, configurationCopy(const char*, const char*), this, configurationCopy(const char*, const char*));
  CQT_CONNECT(proj, configurationRemoved(const char*), this, configurationRemoved(const char*));
  CQT_CONNECT(proj, configurationAdded(const char*), this, configurationAdded(const char*));
}

void CarbonSourceGroups::configurationAdded( const char* newName)
{
  for (int i=0; i<mGroups.count(); ++i)
  {
    CarbonSourceGroup* grp = mGroups[i];

    for (int j=0; j<grp->numSources(); j++)
    {
      CarbonHDLSourceFile* src = grp->getSource(j);
      src->addConfiguration(newName);
    }
  }
}

void CarbonSourceGroups::configurationCopy(const char*, const char*)
{
// Nothing to implement since the sources were copied during the "configurationAdded" event
// prior to this event.
}

void CarbonSourceGroups::configurationRenamed(const char* oldName, const char* newName)
{
  for (int i=0; i<mGroups.count(); ++i)
  {
    CarbonSourceGroup* grp = mGroups[i];

    for (int j=0; j<grp->numSources(); j++)
    {
      CarbonHDLSourceFile* src = grp->getSource(j);
      CarbonHDLSourceConfiguration* srcConfig = src->getConfiguration(oldName);
      srcConfig->putName(newName);
    }
  }
}

void CarbonSourceGroups::configurationRemoved(const char* name)
{
  for (int i=0; i<mGroups.count(); ++i)
  {
    CarbonSourceGroup* grp = mGroups[i];

    for (int j=0; j<grp->numSources(); j++)
    {
      CarbonHDLSourceFile* src = grp->getSource(j);
      src->removeConfiguration(name);      
    }
  }
}

CarbonSourceGroups::~CarbonSourceGroups()
{
  delete mDefaultOptions;
}

void CarbonSourceGroups::moveUp(CarbonSourceGroup* group)
{
  for (int i=0; i<mGroups.count(); ++i)
  {
    CarbonSourceGroup* grp = mGroups[i];
    if (grp == group)
    {
      mGroups.removeAt(i);
      mGroups.insert(i-1, grp);
      break;
    }
  }
  updateIndexes();
}

void CarbonSourceGroups::remove(CarbonSourceGroup* group)
{
 for (int i=0; i<mGroups.count(); ++i)
  {
    CarbonSourceGroup* grp = mGroups[i];
    if (grp == group)
    {
      mGroups.removeAt(i);
      delete grp;
      break;
    }
  }
  updateIndexes();
}

void CarbonSourceGroups::updateIndexes()
{
  for (int i=0; i<mGroups.count(); ++i)
  {
    CarbonSourceGroup* group = mGroups[i];
    group->putIndex(i);
  }
}

void CarbonSourceGroups::moveDown(CarbonSourceGroup* group)
{
  for (int i=0; i<mGroups.count(); ++i)
  {
    CarbonSourceGroup* grp = mGroups[i];
    if (grp == group)
    {
      mGroups.removeAt(i);
      mGroups.insert(i+1, grp);
      break;
    }
  }

  updateIndexes();
}


bool CarbonSourceGroups::serialize(xmlTextWriterPtr writer)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Sources");

  for (int i=0; i<mGroups.size(); ++i)
  {
    CarbonSourceGroup* group = mGroups[i];
    group->serialize(writer);
  }

  xmlTextWriterEndElement(writer);
  return true;
}

bool CarbonSourceGroups::deserialize(xmlNodePtr parent, UtXmlErrorHandler* eh)
{
  CarbonSourceGroup::deserialize(this, parent, eh);

  return true;
}

void CarbonSourceGroup::addConfiguration(const char* name, const char* platform)
{
  CarbonConfiguration* config = mConfigs->addConfig(platform, name, false);
  config->addTool(new CarbonSourceGroupTool()); 

  // Register for changes
  config->getOptions("RTL Properties")->registerPropertyChanged("Folder Name", "folderNameChanged", this);
}

void CarbonSourceGroup::folderNameChanged(const CarbonProperty*, const char* value)
{
  putName(value);

  // update the tree node to reflect new name
  CarbonHDLFolderItem* treeItem = mGroups->getProject()->getProjectWidget()->findHDLFolderItem(this);
  if (treeItem)
    treeItem->setText(0, value);
}

bool CarbonSourceGroup::isVhdlLibrary()
{
  QString vhdlLib = getVhdlLibrary();
  return vhdlLib.length() > 0; 
}

bool CarbonSourceGroup::isVerilogLibrary()
{
  QString vlogLib = getVerilogLibrary();
  return vlogLib.length() > 0; 
}

bool CarbonSourceGroup::isLibrary()
{
  QString vhdlLib = getVhdlLibrary();
  QString vlogLib = getVerilogLibrary();

  return (vhdlLib.length() > 0 || vlogLib.length() > 0); 
} 

// All configurations get the same name
void CarbonSourceGroup::putVhdlLibrary(const char* newVal)
{
  if (mConfigs->numConfigurations() > 0)
  {
    for (UInt32 i=0; i<mConfigs->numConfigurations(); i++)
    {
      CarbonConfiguration* config = mConfigs->getConfiguration(i);
      CarbonOptions* options = getOptions(config->getName());
      options->putValue("-vhdlLib", newVal);
    }
  }
  else
    mPendingVHDLLib = newVal;
}

const char* CarbonSourceGroup::getVhdlLibrary()
{
  CarbonOptions* options = getOptions();
  CarbonPropertyValue* propValue = options->getValue("-vhdlLib");
  if (propValue->isDefaultValue(propValue->getValue()))
    return "";
  else
    return options->getValue("-vhdlLib")->getValue();
}

// All configurations get the same name
void CarbonSourceGroup::putVerilogLibrary(const char* newVal)
{
  if (mConfigs->numConfigurations() > 0)
  {
    for (UInt32 i=0; i<mConfigs->numConfigurations(); i++)
    {
      CarbonConfiguration* config = mConfigs->getConfiguration(i);
      CarbonOptions* options = getOptions(config->getName());
      options->putValue("-vlogLib", newVal);
    }
  }
  else
    mPendingVerilogLib = newVal;
}

const char* CarbonSourceGroup::getVerilogLibrary()
{
  CarbonOptions* options = getOptions();
  CarbonPropertyValue* propValue = options->getValue("-vlogLib");
  if (propValue->isDefaultValue(propValue->getValue()))
    return "";
  else
    return options->getValue("-vlogLib")->getValue();
}


void CarbonSourceGroup::addDefaultConfigurations()
{
  CarbonProject* proj = mGroups->getProject();

  if (mConfigs)
  {
    for (UInt32 i=0; i<mConfigs->numConfigurations(); i++)
    {
      CarbonConfiguration* config = mConfigs->getConfiguration(i);
      config->getOptions("RTL Properties")->unregisterPropertyChanged("Folder Name", this);        
    }
  }

  mActiveConfiguration = NULL;
  mConfigs = new CarbonConfigurations(proj);

  CarbonConfigurations* projConfigs = proj->getConfigurations();

  for (UInt32 i=0; i<projConfigs->numConfigurations(); i++)
  {
    CarbonConfiguration* projConfig = projConfigs->getConfiguration(i);
    const char* configName = projConfig->getName();

    addConfiguration(configName, "Linux");
  }

  if (mActiveConfiguration == NULL)
    mActiveConfiguration = mConfigs->findConfiguration(proj->getActiveConfiguration());
}

// Source Group
CarbonSourceGroup::CarbonSourceGroup(CarbonSourceGroups* parent)
{
  mGroups=parent;
  mIndex=-1;
  mConfigs = NULL;

  CarbonProject* proj = parent->getProject();
  INFO_ASSERT(proj, "Expecting a project");

  addDefaultConfigurations();

  CQT_CONNECT(proj, configurationChanged(const char*), this, configurationChanged(const char*));
  CQT_CONNECT(proj, configurationRenamed(const char*, const char*), this, configurationRenamed(const char*, const char*));
  CQT_CONNECT(proj, configurationCopy(const char*, const char*), this, configurationCopy(const char*, const char*));
  CQT_CONNECT(proj, configurationRemoved(const char*), this, configurationRemoved(const char*));
  CQT_CONNECT(proj, configurationAdded(const char*), this, configurationAdded(const char*));
}

// A Library name can be of the form: WORK:{path} or simply WORK
// but here, we only want the part to the right of the colon or an empty string
// if there is nothing to the right
const char* CarbonSourceGroup::getLibraryPath()
{
  mLibraryPath.clear();
  QString rawLibName;

  QString vhdlLib = getVhdlLibrary();
  QString vlogLib = getVerilogLibrary();

  if (vhdlLib.length() > 0)
    rawLibName = vhdlLib;
  else if (vlogLib.length() > 0)
    rawLibName = vlogLib;
  
  int colonPos = rawLibName.indexOf(":");
  
  QString libPath;
  if (colonPos != -1)
    libPath = rawLibName.mid(colonPos+1);
  
  mLibraryPath << libPath;

  return mLibraryPath.c_str();
}

// A Library name can be of the form: WORK:{path} or simply WORK
// but here, we only want the part to the left of the colon, or simply WORK
const char* CarbonSourceGroup::getLibraryName()
{
  mLibraryName.clear();
  if (isLibrary())
  {
    QString vhdlLib = getVhdlLibrary();
    QString vlogLib = getVerilogLibrary();

    QString rawLibName;
    if (vhdlLib.length() > 0)
     rawLibName = vhdlLib;
    else if (vlogLib.length() > 0)
     rawLibName = vlogLib;
  
    int colonPos = rawLibName.indexOf(":");

    QString libName;
    if (colonPos != -1)
      libName = rawLibName.left(colonPos);
    else
      libName = rawLibName;

    mLibraryName << libName;
  }
  return mLibraryName.c_str();
}

const char* CarbonSourceGroup::getCommandFilename(CarbonConfiguration* cfg)
{
  mCommandName.clear();
  QFileInfo libDesignInfo(mGroups->getProject()->getConfigDesignFilePath(cfg->getName(), ".symtab.db"));
  
  mCommandName << "library." << libDesignInfo.baseName() << "." << getLibraryName() << ".cmd";;

  return mCommandName.c_str();
}

// A Library name can be of the form: WORK:{path} or simply WORK
// so the target name is simply lib<design>.WORK or, it will be
// {path}/lib<design>.WORK

const char* CarbonSourceGroup::getTargetName(CarbonConfiguration* activeConfig)
{
  mTargetName.clear();
  mTargetName << getTargetDirectory(activeConfig) << "/group" << getIndex() << ".lib";
  return mTargetName.c_str();
}

const char* CarbonSourceGroup::getTargetDirectory(CarbonConfiguration* activeConfig)
{
  mTargetDir.clear();
  
  QFileInfo libDesignInfo(mGroups->getProject()->getConfigDesignFilePath(activeConfig->getName(), ".symtab.db"));
  
  UtString libDesign;
  libDesign << libDesignInfo.baseName() << "." << getLibraryName();

  UtString libPath;
  libPath << getLibraryPath();

  if (libPath.length() > 0)
    mTargetDir << libPath.c_str() << "/" << libDesign.c_str();
  else
    mTargetDir << libDesign.c_str();

  return mTargetDir.c_str();
}


void CarbonSourceGroup::moveUp(CarbonHDLSourceFile* srcFile)
{
  for (int i=0; i<mSources.count(); ++i)
  {
    CarbonHDLSourceFile* src = mSources[i];
    if (src == srcFile)
    {
      mSources.removeAt(i);
      mSources.insert(i-1, src);
      break;
    } 
  }
}
void CarbonSourceGroup::moveDown(CarbonHDLSourceFile* srcFile)
{
  for (int i=0; i<mSources.count(); ++i)
  {
    CarbonHDLSourceFile* src = mSources[i];
    if (src == srcFile)
    {
      mSources.removeAt(i);
      mSources.insert(i+1, src);
      break;
    }
  }
}

void CarbonSourceGroup::remove(CarbonHDLSourceFile* srcFile, bool deleteItem)
{
  for (int i=0; i<mSources.size(); ++i)
  {
    CarbonHDLSourceFile* src = mSources[i];
    if (src == srcFile)
    {
      mSources.removeAt(i);
      if (deleteItem)
        delete srcFile;
      break;
    }
  }
}

bool CarbonSourceGroup::addSource(CarbonHDLSourceFile* newSource)
{
  for (int i=0; i<mSources.size(); ++i)
  {
    CarbonHDLSourceFile* src = mSources[i];
    if (0 == strcmp(src->getAbsolutePath(), newSource->getAbsolutePath()))
    {
      delete newSource;
      return false;
    }
  }

  // Remember our new parent here in case we 
  // move a source file between RTL libraries
  newSource->putGroup(this);

  mSources.push_back(newSource);

  return true;
}

bool CarbonSourceGroup::serialize(xmlTextWriterPtr writer)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Group");
  xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST getName());

  // Write out group switches
  mConfigs->serialize(writer);

  for (int i=0; i<mSources.size(); ++i)
  {
    CarbonHDLSourceFile* src = mSources[i];
    src->serialize(writer);
  }

  xmlTextWriterEndElement(writer);
  return true;
}

void CarbonSourceGroup::putActiveConfig(const char* name)
{
  mActiveConfiguration = NULL;
  for (UInt32 i=0; i<mConfigs->numConfigurations(); i++)
  {
    CarbonConfiguration* config = mConfigs->getConfiguration(i);
    if (0 == strcmp(config->getName(), name))
    {
      mActiveConfiguration = config;
      break;
    }
  }
}

bool CarbonSourceGroup::deserialize(CarbonSourceGroups* groups, xmlNodePtr parent, UtXmlErrorHandler* eh)
{
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "Group"))
    {
      UtString name;
      XmlParsing::getProp(child, "Name", &name);

      UtString vhdlLib;
      XmlParsing::getProp(child, "VHDLLibrary", &vhdlLib);
      
      UtString verilogLib;
      XmlParsing::getProp(child, "VerilogLibrary", &verilogLib);
      
      CarbonSourceGroup* group = new CarbonSourceGroup(groups);

      group->putName(name.c_str());
      if (vhdlLib.length() > 0)
        group->putVhdlLibrary(vhdlLib.c_str());
      
      if (verilogLib.length() > 0)
        group->putVerilogLibrary(verilogLib.c_str());

      groups->addGroup(group);
      
      // Grab the configurations
      for (xmlNodePtr subChild = child->children; subChild != NULL; subChild = subChild->next) 
      {
        if (XmlParsing::isElement(subChild, "Configurations")) 
        {
          // We need to make sure that all the configs we are about to 
          // serialize already exist.
          CarbonConfigurations* configs = group->getConfigurations();
          for (xmlNodePtr cfgChild = subChild->children; cfgChild != NULL; cfgChild = cfgChild->next)
          {
            if (XmlParsing::isElement(cfgChild, "Configuration"))
            {
              UtString configName;
              XmlParsing::getProp(cfgChild, "Name", &configName);
              QString cname = configName.c_str();
              QStringList configParts = cname.split('|');

              UtString config;
              UtString platform;
              config << configParts[0];
              platform << configParts[1];

              group->addConfiguration(config.c_str(), platform.c_str());
            }
          }
          configs->deserialize(subChild, eh);
        }
      }
    
      if (child->children)
        CarbonHDLSourceFile::deserialize(group, child->children, eh);
    }
  }

  return true;
}


CarbonHDLSourceFile* CarbonSourceGroup::findFile(const char* fileName)
{
  for (int i=0; i<mSources.count(); ++i)
  {
    CarbonHDLSourceFile* src = mSources[i];
    if (0 == strcmp(src->getRelativePath(), fileName))
      return src;
  }
  return NULL;
}


CarbonOptions* CarbonSourceGroup::getOptions(const char* configName) 
{
  CarbonOptions* options = NULL;
  if (mActiveConfiguration)
    options = mActiveConfiguration->getOptions("RTL Properties");

  if (configName != NULL) {
    CarbonConfiguration* config = getConfigurations()->findConfiguration(configName);
    if(config) options = config->getOptions("RTL Properties");
  }

  if (options)
    options->putValue("Folder Name", getName());

  return options;
}
void CarbonSourceGroup::configurationAdded(const char* name)
{
  qDebug() << "Adding CarbonSourceGroup Configuration: " << name;
  CarbonConfiguration* cfg = getConfigurations()->findConfiguration(name);
  if (cfg == NULL)
    addConfiguration(name, mActiveConfiguration->getPlatform());
}

void CarbonSourceGroup::configurationRenamed(const char* oldName, const char* newName)
{
  CarbonConfiguration* oldConfig = mConfigs->findConfiguration(oldName);
  INFO_ASSERT(oldConfig, "Expecting Configuration");
  oldConfig->putName(newName);
}

void CarbonSourceGroup::configurationCopy(const char* oldCfgName, const char* newCfgName)
{
  CarbonOptions* oldOptions = getOptions(oldCfgName);
  CarbonOptions* newOptions = getOptions(newCfgName);

  oldOptions->setBlockSignals(true);
  newOptions->setBlockSignals(true);

  for (CarbonOptions::OptionsLoop p = oldOptions->loopOptions(); !p.atEnd(); ++p)
  {
    CarbonPropertyValue* sv = p.getValue();
    const char* value = sv->getValue();
    const char* propName = sv->getProperty()->getName();
    newOptions->putValue(propName, sv->getProperty()->getDefaultValue());
    newOptions->putValue(propName, value);
  }

  oldOptions->setBlockSignals(false);
  newOptions->setBlockSignals(false);
}

void CarbonSourceGroup::configurationRemoved(const char* name)
{
  qDebug() << "Removing CarbonSourceGroup Configuration: " << name;
  mConfigs->removeConfiguration(name);
}

void CarbonSourceGroup::configurationChanged(const char* newConfig)
{
  qDebug() << "CarbonSourceGroup Component Config changed to: " << newConfig;

  // This could be empty at the time we constructed
  // so ensure we are in sync with the project at this time.
  if (mConfigs->numConfigurations() == 0)
    addDefaultConfigurations();

  for (UInt32 i=0; i<mConfigs->numConfigurations(); i++)
  {
    mActiveConfiguration = mConfigs->getConfiguration(i);
    if (mPendingVHDLLib.length() > 0)
      putVhdlLibrary(mPendingVHDLLib.c_str());
    
    if (mPendingVerilogLib.length() > 0)
      putVerilogLibrary(mPendingVerilogLib.c_str());
  }

  mPendingVHDLLib.clear();
  mPendingVerilogLib.clear();

  mActiveConfiguration = mConfigs->findConfiguration(newConfig);

  INFO_ASSERT(mActiveConfiguration, "Unable to locate configuration");
}
