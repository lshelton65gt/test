//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"

#include "gui/CQt.h"
#include "util/UtString.h"

#include "SSHConsoleHelper.h"
#include "CarbonProjectWidget.h"

#if pfWINDOWS
#include <windows.h>
#include <stdio.h>
#endif

QString SSHConsoleHelper::shellCreateTestFileScript(const QString& scriptName)
{
  UtString shellCmd;

  shellCmd << "cat > " << scriptName << " << CARBONSCRIPTEOF" << "\n";
  shellCmd << "#!/bin/sh" << "\n";

  shellCmd << "if [ -f \"\\$1\" ]" << "\n";
  shellCmd << "then" << "\n";
  shellCmd << "  \\echo CARBON FILE EXISTS: \\$1" << "\n";
  shellCmd << "  exit 0" << "\n";
  shellCmd << "else" << "\n";
  shellCmd << "  \\echo CARBON FILE NOT FOUND: \\$1" << "\n";
  shellCmd << "  exit 1" << "\n";
  shellCmd << "fi" << "\n";
  shellCmd << "CARBONSCRIPTEOF" << "\n";

  shellCmd << "chmod +x " << scriptName << "\n";

  return shellCmd.c_str();
}

QString SSHConsoleHelper::shellCreateTestDirectoryScript(const QString& scriptName)
{
  UtString shellCmd;

  shellCmd << "cat > " << scriptName << " << CARBONSCRIPTEOF" << "\n";
  shellCmd << "#!/bin/sh" << "\n";

  shellCmd << "if [ -d \"\\$1\" ]" << "\n";
  shellCmd << "then" << "\n";
  shellCmd << "  \\echo CARBON DIRECTORY EXISTS: \\$1" << "\n";
  shellCmd << "  exit 0" << "\n";
  shellCmd << "else" << "\n";
  shellCmd << "  \\echo CARBON DIRECTORY NOT FOUND: \\$1" << "\n";
  shellCmd << "  exit 1" << "\n";
  shellCmd << "fi" << "\n";
  shellCmd << "CARBONSCRIPTEOF" << "\n";

  shellCmd << "chmod +x " << scriptName << "\n";

  return shellCmd.c_str();
}

bool SSHConsoleHelper::isNetworkDrive(const QChar inputDriveLetter)
{
  QString driveLetter = QString(inputDriveLetter) + ":";

  QStringList driveLetters;
  QStringList driveRemotes; // we don't care about this here.
  getNetworkDrives(driveLetters, driveRemotes);

  foreach (QString dl, driveLetters)
  {
    if (dl == driveLetter)
      return true;
  }
  return false;
}

bool SSHConsoleHelper::isNetworkDrive(const QChar inputDriveLetter, const QStringList& cachedDriveLetters)
{
  QString driveLetter = QString(inputDriveLetter) + ":";

  foreach (QString dl, cachedDriveLetters)
  {
    if (dl == driveLetter)
      return true;
  }
  return false;
}

// Returns two parallel lists, drive letter and drive info (name)
int SSHConsoleHelper::getNetworkDrives(QStringList& driveListLocalName, QStringList& driveListRemoteName)
{
  // Clear out parallel lists
  driveListLocalName.clear();
  driveListRemoteName.clear();

  int numDrives = 0;

#if pfWINDOWS
  DWORD dwResult;
  HANDLE hEnum;
  DWORD cbBuffer = 16384;
  DWORD cEntries = 0xFFFFFFFF;
  LPNETRESOURCE lpnrDrv;
  DWORD i;

  dwResult = WNetOpenEnum( RESOURCE_CONNECTED,
    RESOURCETYPE_ANY,
    0,
    NULL,
    &hEnum );
  

  if (dwResult != NO_ERROR)
  {
    UtString msg;
    msg << "Unable to enumerate network drives";
    QMessageBox::critical(NULL, MODELSTUDIO_TITLE, msg.c_str());  
    return 0;
  }

  do
  {
    lpnrDrv = (LPNETRESOURCE) GlobalAlloc( GPTR, cbBuffer );

    dwResult = WNetEnumResource( hEnum, &cEntries, lpnrDrv, &cbBuffer);

    if (dwResult == NO_ERROR)
    {
      for( i = 0; i < cEntries; i++ )
      {
        if( lpnrDrv[i].lpLocalName != NULL )
        {
          driveListLocalName.append(lpnrDrv[i].lpLocalName);
          driveListRemoteName.append(lpnrDrv[i].lpRemoteName);
          numDrives++;
        }
      }
    }
    else if( dwResult != ERROR_NO_MORE_ITEMS )
    {
      UtString msg;
      msg << "Unable to enumerate network drives";
      QMessageBox::critical(NULL, MODELSTUDIO_TITLE, msg.c_str());  
      GlobalFree( (HGLOBAL) lpnrDrv );
      break;
    }
    GlobalFree( (HGLOBAL) lpnrDrv );
  }
  while( dwResult != ERROR_NO_MORE_ITEMS );

  WNetCloseEnum(hEnum);
#endif	

  return numDrives;
}
