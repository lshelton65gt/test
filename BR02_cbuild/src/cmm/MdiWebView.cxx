//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QtGui>
#include <QMenuBar>
#include <QToolBar>
#include <QTextBrowser>
#include "gui/CQt.h"
#include "QTextEditor.h"
#include "MdiWebView.h"


MDIWebViewTemplate::MDIWebViewTemplate()
  : MDIDocumentTemplate("TextEditor", "New Text File", "Verilog File (*.v);;VHDL File (*.vhd*);;C++ File (*.cpp *.cxx);;C++ Header File (*.h);;Text Files (*.txt);;All Files (*.*)") 
{
  mYesToAll = false;

  mActionCut = new QAction(QIcon(":/cmm/Resources/cut.png"), tr("Cu&t"), this);
  mActionCut->setShortcut(tr("Ctrl+X"));
  mActionCut->setStatusTip(tr("Cut the current selection's contents to the "
    "clipboard"));

  mActionCopy = new QAction(QIcon(":/cmm/Resources/copy.png"), tr("Co&py"), this);
  mActionCopy->setShortcut(tr("Ctrl+C"));
  mActionCopy->setStatusTip(tr("Copy the current selection's contents to the "
    "clipboard"));

  mActionPaste = new QAction(QIcon(":/cmm/Resources/paste.png"), tr("Pa&ste"), this);
  mActionPaste->setShortcut(tr("Ctrl+V"));
  mActionPaste->setStatusTip(tr("Paste the current selection's contents from the "
    "clipboard"));

  mActionFind = new QAction(QIcon(":/cmm/Resources/FindHS.png"), tr("Find"), this);
  mActionFind->setShortcut(tr("Ctrl+F"));
  mActionFind->setStatusTip(tr("Search for text"));

  mActionUndo = new QAction(QIcon(":/cmm/Resources/undo.png"), tr("U&ndo"), this);
  mActionUndo->setShortcut(tr("Ctrl+Z"));
  mActionUndo->setStatusTip(tr("Undo the last operation"));

  mActionRedo = new QAction(QIcon(":/cmm/Resources/redo.png"), tr("Re&do"), this);
  mActionRedo->setShortcut(tr("Ctrl+Y"));
  mActionRedo->setStatusTip(tr("Redo the last operation"));

  mActionFileSave = new QAction(QIcon(":/cmm/Resources/save.png"), tr("Sa&ve"), this);
  mActionFileSave->setShortcut(tr("Ctrl+S"));
  mActionFileSave->setStatusTip(tr("Save the document"));

  mActionRunScript = new QAction(QIcon(":/cmm/Resources/onDemandStart.png"), tr("&Run Script"), this);
  mActionRunScript->setShortcut(tr("Ctrl+F5"));
  mActionRunScript->setStatusTip(tr("Run this script"));
  
  mActionDebugScript = new QAction(QIcon(":/cmm/Resources/replayPlayback.png"), tr("Debu&g Script"), this);
  mActionDebugScript->setShortcut(tr("F5"));
  mActionDebugScript->setStatusTip(tr("Debug this script"));
  
  mActionToggleBreakpoint = new QAction(QIcon(":/cmm/Resources/replayRecord.png"), tr("To&ggle Breakpoint"), this);
  mActionToggleBreakpoint->setShortcut(tr("F9"));
  mActionToggleBreakpoint->setStatusTip(tr("Toggle a breakpoint"));

  mActionDeleteBreakpoints = new QAction(tr("Delete all Breakpoints"), this);
  mActionDeleteBreakpoints->setShortcut(tr("Ctrl+Shift+F9"));
  mActionDeleteBreakpoints->setStatusTip(tr("Delete all breakpoints"));
}

void MDIWebViewTemplate::updateMenusAndToolbars(QWidget*)
{

}

// If the user switches windows with the keyboard or menu pick
// update the tab bar to reflect that this is the active window now.
void MDIWebViewTemplate::updateSelectedTab(QWidget* widget)
{
  QVariant qv = widget->property("CarbonTabWidget");
  QVariant qvPlaceholder = widget->property("CarbonTabPlaceholder");

  if (qv.isValid() && qvPlaceholder.isValid())
  {
    QTabWidget* tw = (QTabWidget*)qv.value<void*>();
    QWidget* w = (QWidget*)qvPlaceholder.value<void*>();

    int tabIndex = tw->indexOf(w);
    if (tabIndex != -1)
      tw->setCurrentIndex(tabIndex);
  }
}

int MDIWebViewTemplate::createToolbars()
{
  QToolBar* toolbar = new QToolBar("Text Editor");
  toolbar->setObjectName("TextEditorToolbar1");

  toolbar->addAction(mActionFileSave);
  toolbar->addAction(mActionUndo);
  toolbar->addAction(mActionRedo);
  toolbar->addAction(mActionCut);
  toolbar->addAction(mActionCopy);
  toolbar->addAction(mActionPaste);
  toolbar->addAction(mActionFind);

  toolbar->addAction(mActionRunScript);
  toolbar->addAction(mActionDebugScript);
  toolbar->addAction(mActionToggleBreakpoint);

  registerAction(mActionRunScript, SLOT(runScript()));
  registerAction(mActionToggleBreakpoint, SLOT(toggleBreakpoint()));
  registerAction(mActionDebugScript, SLOT(debugScript()));
  registerAction(mActionDeleteBreakpoints, SLOT(deleteBreakpoints()));

  registerUpdateAction(mActionRunScript, SIGNAL(scriptLoaded(bool)), SLOT(setEnabled(bool)));
  registerUpdateAction(mActionRunScript, SIGNAL(scriptLoaded(bool)), SLOT(setVisible(bool)));

  registerUpdateAction(mActionDebugScript, SIGNAL(scriptLoaded(bool)), SLOT(setEnabled(bool)));
  registerUpdateAction(mActionDebugScript, SIGNAL(scriptLoaded(bool)), SLOT(setVisible(bool)));

  registerUpdateAction(mActionToggleBreakpoint, SIGNAL(scriptLoaded(bool)), SLOT(setEnabled(bool)));
  registerUpdateAction(mActionToggleBreakpoint, SIGNAL(scriptLoaded(bool)), SLOT(setVisible(bool)));

  registerUpdateAction(mActionDeleteBreakpoints, SIGNAL(scriptLoaded(bool)), SLOT(setEnabled(bool)));
  registerUpdateAction(mActionDeleteBreakpoints, SIGNAL(scriptLoaded(bool)), SLOT(setVisible(bool)));


  addToolbar(toolbar);
  return numToolbars();
}

int MDIWebViewTemplate::createMenus()
{
  QMenu* fileMenu = new QMenu("&File");
  fileMenu->setObjectName("TextEditorFileMenu");
  fileMenu->addAction(mActionFileSave);
  registerAction(mActionFileSave, SLOT(fileSave()));

  addMenu(fileMenu);

  QMenu* menu = new QMenu("&Edit");
  menu->setObjectName("TextEditorEditMenu");

  menu->addAction(mActionUndo);
  registerAction(mActionUndo, SLOT(undo()));
  registerUpdateAction(mActionUndo, SIGNAL(undoAvailable(bool)), SLOT(setEnabled(bool)));

  menu->addAction(mActionRedo);
  registerAction(mActionRedo, SLOT(redo()));
  registerUpdateAction(mActionRedo, SIGNAL(redoAvailable(bool)), SLOT(setEnabled(bool)));

  menu->addAction(mActionCut);
  registerAction(mActionCut, SLOT(cut()));

  menu->addAction(mActionCopy);
  registerAction(mActionCopy, SLOT(copy()));
  registerUpdateAction(mActionCopy, SIGNAL(copyAvailable(bool)), SLOT(setEnabled(bool)));

  menu->addAction(mActionPaste);
  registerAction(mActionPaste, SLOT(paste()));
  registerUpdateAction(mActionPaste, SIGNAL(pasteAvailable(bool)), SLOT(setEnabled(bool)));

  menu->addAction(mActionFind);
  registerAction(mActionFind, SLOT(find()));

  menu->addSeparator();
  menu->addAction(mActionRunScript);
  menu->addAction(mActionDebugScript);
  menu->addAction(mActionToggleBreakpoint);
  menu->addAction(mActionDeleteBreakpoints);

  addMenu(menu);

  return numMenus();
}

MDIWidget* MDIWebViewTemplate::createNewDocument(QWidget*)
{
  return NULL;
}

MDIWidget* MDIWebViewTemplate::openDocument(QWidget* parent, const char* docName)
{
  QFileInfo fi(docName);
  if (fi.exists())
  {
    WebBrowserWidget* te = new WebBrowserWidget(this, parent);
    return te;
  }
  else
  {
    UtString msg;
    msg << "Unable to open file: " << docName;
    QMessageBox::warning(parent, "File not found", msg.c_str());
    return NULL;
  }
}

MDIWidget* MDIWebViewTemplate::createDocument(QWidget* parent, const char* )
{

  WebBrowserWidget* te = new WebBrowserWidget(this, parent);
  return te;
}

WebBrowserWidget::WebBrowserWidget(MDIDocumentTemplate* t, QWidget* parent) : QWebView(parent)
{
  initializeMDI(this, t);
}

void WebBrowserWidget::closeEvent(QCloseEvent *event)
{
  event->accept();
  MDIWidget::closeEvent(this);
}

const char* WebBrowserWidget::userFriendlyName()
{
  return "Friendly Name";
}

MDIWebBrowser::MDIWebBrowser()
  : MDIDocumentTemplate("WEBBrowser", "Html File", "") 
{
}

MDIWidget* MDIWebBrowser::createNewDocument(QWidget* parent)
{
  WebBrowserWidget* tb = new WebBrowserWidget(this, parent);
  return tb;
}

MDIWidget* MDIWebBrowser::openDocument(QWidget* parent, const char* docName)
{
  WebBrowserWidget* browser = new WebBrowserWidget(this, parent);
  QUrl url(docName);
  browser->load(url);
  return browser;
}
