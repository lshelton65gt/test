<Properties
  name="VSPCompiler"
  >
  <section name="Basic Options">
     <option name="-o" required="true" reset="none" legalPattern="lib.+\.(a|lib|so|dll)">
      <numValues>1</numValues>
      <description><![CDATA[Use this option to specify the name of all output files for the Carbon Model. You must specify an extension for object files of either '.a', which generates a traditional archive, '.so', which generates a linked shared library, '.lib', which generates a static Windows library, or '.dll' which generates a Windows DLL. The file argument can be a full path or a relative file name.]]></description>
      <outfile nameOnly="true">
        <filter>Design Archive (*.a);;Shared Library (*.so);;Windows Library (*.lib);;Windows DLL (*.dll)</filter>
        <default></default>
      </outfile>
    </option>

    <option name="-timeBomb" licenseEnabled="timebomb">
      <numValues>1</numValues>
      <description>A model constructed with a timeBomb license will expire on this date that is specified.</description>
      <datestamp>
        <default></default>
      </datestamp>
    </option>
    
    <option name="Compiler Control">
      <numValues>1</numValues>
      <description>Use this option to choose compiler control</description>
      <enum>
        <default>32-bit compile, 32-bit model</default>
        <option value="32-bit compile, 32-bit model">32-bit compile, 32-bit model</option>
        <option value="64-bit compile, 32-bit model">64-bit compile, 32-bit model</option>
        <option value="64-bit compile, 64-bit model">64-bit compile, 64-bit model</option>
      </enum>
    </option>
    <option name="-O">
      <numValues>1</numValues>
      <description><![CDATA[Use this option to control the design optimization level. The level can be an integer between 0 and 3, with 0 implying no optimization, or 's', which optimizes for space.]]></description>
      <enum>
        <default>2</default>
        <option value="0">0 - No Optimization</option>
        <option value="1">1 - Basic Optimization</option>
        <option value="2">2 - Advanced Optimization</option>
        <option value="3">3 - Advanced, Agressive Optimization</option>
        <option value="s">s - Minimize Code Size</option>
      </enum>
    </option>
  </section>
  <section name="General Compile Control">
    <option name="-topLevelParam">
      <numValues>0</numValues>
      <description>
        <![CDATA[This option can be used to specify all the parameters and generics in the top level design unit. Any parameters and generics not specified using this option will retain their default values. For parameters, the case of the parameter name has to match. For generics this restriction does not apply, since VHDL is case insensitive.]]></description>
      <stringlist repeat="OptionNameValue">
        <default></default>
      </stringlist>
    </option>
    <option name="-annotateCode">
      <numValues>0</numValues>
      <description><![CDATA[Annotate the generated C++ for C++ fault diagnosis]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-clockGlitchDetect" falseValue="-noClockGlitchDetect" trueValue="-clockGlitchDetect">
      <numValues>0</numValues>
      <description><![CDATA[Enable support for clock glitch detection in the model.  This still needs to be enabled at model creation time.]]></description>
      <bool>
        <default>true</default>
      </bool>
    </option>
    <option name="-j">
      <numValues>1</numValues>
      <description><![CDATA[Use this option to limit parallel make sub-jobs, i.e., the number of object compilations running in parallel. Set this to 1 for serial runs. You may set this option to any positive non-zero value you want; however, compile performance degradation may occur if it is set too high.]]></description>
      <integer>
        <range minimum="0" maximum="64"/>
        <default>4</default>
      </integer>
    </option>
    <option name="-L">
      <numValues>1</numValues>
      <description><![CDATA[Specifies a library to be searched when resolving a design unit.  Multiple instances of this switch are allowed.]]></description>
      <string>
        <default></default>
      </string>
    </option>
    <option name="-Lf">
      <numValues>1</numValues>
      <description><![CDATA[Specifies a library to be searched first when resolving a design unit.  Multiple instances of this switch are allowed.]]></description>
      <string>
        <default></default>
      </string>
    </option>
    <option name="-licq">
      <numValues>0</numValues>
      <description><![CDATA[Wait for a license rather than exiting, if none is available]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-profileGenerate">
      <numValues>0</numValues>
      <description><![CDATA[Compile with instrumentation for profile based optimization.  After compiling: (1) Link using gcc's -fprofile-generate option, (2) run the model, (3) recompile using Carbon compiler's -profileUse option, (4) relink.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-profileUse">
      <numValues>0</numValues>
      <description><![CDATA[Recompile using feedback from profile directed optimization.  Use this after compiling with -profileGenerate and linking with gcc's -fprofile-generate option and running the model.  This option reuses much of the previous Carbon compiler compilation, so HDL changes and many Carbon compiler options are ignored.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-attributeFile" ReadOnly="true">
      <numValues>0</numValues>
      <description><![CDATA[Carbon Attributes file to be passed to the compiler]]></description>
      <infile>
        <filter>Attributes File (*.attributes);;All Files (*.*)</filter>
      </infile>
    </option>
    <option name="-synth" falseValue="-noSynth" trueValue="-synth">
      <numValues>0</numValues>
      <description><![CDATA[Compile design with synthesis subset restrictions. For VHDL designs this switch processes the full synthesis subset check. For Verilog designs the synthesis check is limited to the sensitivity list of always blocks.]]></description>
      <bool>
        <default>true</default>
      </bool>
    </option>
    <option name="-compileLibOnly">
      <numValues>0</numValues>
      <description><![CDATA[Compile HDL into specified libraries. The Carbon Model will not be generated.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="Additional Options">
      <numValues>1</numValues>
      <description><![CDATA[Additional options to be passed to the compiler]]></description>
      <stringlist repeat="ValuesOnlyOnePerLine">
        <default></default>
      </stringlist>
    </option>    
  </section>
  <section name="Input File Control">
    <option name="-directive">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to include a directives file on the command line. Note that directives specified in files are additive. The syntax of a directives file is line-oriented. Each directive and its value(s) must be specified on its own line. The file argument can be a full path or a relative file name. There is no restriction on file naming; however, it is standard to use the suffixes '.dct' or '.dir' for directives files.]]></description>
      <filelist type="InputFile" filter="Directives (*.dct *.dir*);;All Files (*.*)">
      </filelist>
    </option>
    <option name="-f">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to specify a file name from which the Carbon compiler will read command-line options. Note that options specified in files are additive. The syntax of the file is simply a space-separated list of options and arguments; each option does not need to be specified on a new line. The file argument can be a full path or a relative file name. There is no restriction on file naming. Shell comments, specified with #, and C++-style comments, // and /*...*/, are allowed.]]></description>
      <filelist type="InputFile" filter="Command Files (*.cmd *.f);;All Files (*.*)">
      </filelist>
    </option>
    <option name="-showParseMessages">
      <numValues>0</numValues>
      <description><![CDATA[Print informational messages during HDL design file parsing]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
  </section>
  <section name="Module Control">
    <option name="-checkpoint">
      <numValues>0</numValues>
      <description><![CDATA[Enable generation of checkpoint save/restore support in the Carbon Model. When enabled, Carbon API functions may be used to save and restore the state of the model during run time.]]></description>
      <bool>
        <default>true</default>
      </bool>
    </option>
    <option name="-flattenParentThreshold">
      <numValues>1</numValues>
      <description><![CDATA[Avoid flattening any more children into modules of this size, unless the child moduls are tiny (see -flattenTinyThreshold).]]></description>
      <integer>
        <default>10000</default>
      </integer>
    </option>
    <option name="-flattenThreshold">
      <numValues>1</numValues>
      <description><![CDATA[Maximum module size for flattening.]]></description>
      <integer>
        <default>25</default>
      </integer>
    </option>
    <option name="-flattenTinyThreshold">
      <numValues>1</numValues>
      <description><![CDATA[multiple instances allowed. Flatten modules of this size or smaller, even if their parents are large.]]></description>
      <integer>
        <default>10</default>
      </integer>
    </option>
    <option name="-inlineSingleTaskCalls">
      <numValues>0</numValues>
      <description><![CDATA[Inline tasks/functions with a single call.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-inlineTasks">
      <numValues>0</numValues>
      <description><![CDATA[Inline tasks/functions. This switch may improve performance if there are not a large number of tasks. A large number of tasks will cause backend code bloat, which could degrade performance. The use of this switch will disable task parameter usage checks such as an uninitialized output reads. Checks are also disabled for each inlined task specified in a directive.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-multi-thread">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to generate a thread-safe model. Multi-threading is supported in the Carbon API, with some exceptions. So, you may want to enable this option if you intend the model to be used in a multi-threaded environment.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-noFlatten">
      <numValues>0</numValues>
      <description><![CDATA[Disable the flattening of modules based on cost.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-tristate">
      <numValues>1</numValues>
      <description><![CDATA[Use this option to set how internal tristate data values will appear in waveforms and net examinations. Valid modes: 1, 0, x, z (case-insensitive)]]></description>
      <enum>
        <option value="1">1</option>
        <option value="0">0</option>
        <option value="x">x</option>
        <option value="z">z</option>
        <option></option>
        <default>x</default>
      </enum>
    </option>
  </section>
  <section name="Net Control">
    <option name="-bufferedMemoryThreshold">
      <numValues>1</numValues>
      <description><![CDATA[Sets a new threshold in bits for choosing to buffer memories to fix scheduling conflicts. Use this to allow larger memories to get buffered.]]></description>
      <integer>
        <default>16384</default>
      </integer>
    </option>
    <option name="-checkOOB">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to report out-of-bounds bit and memory accesses at runtime.  The Carbon compiler will emit runtime checking code.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-doNetVec" trueValue="-doNetVec" falseValue="-noNetVec">
      <numValues>0</numValues>
      <description><![CDATA[Enable net vectorisation]]></description>
      <bool>
        <default>true</default>
      </bool>
    </option>
    <option name="-g">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to disable optimizations that reduce debug visibility to design nets.  Note that -g allows optimizations to run that don't compromise debug visibility.  Also note that not all nets in the design are preserved by -g.  Dead nets (nets that do not reach primary outputs or observed signals) are not livened by -g.  Dead nets can be livened by the observeSignal directive, which allows wildcards.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-memoryCapacity">
      <numValues>1</numValues>
      <description><![CDATA[Specify the total amount of runtime memory allocated for modelling memories. Memories which do not fit into this threshold will be modelled using a space-efficient representation. A value of 0 will produce only sparse memories, while a value of -1 will produce no sparse memories.]]></description>
      <integer>
        <default>4194304</default>
      </integer>
    </option>
    <option name="-netVecMinCluster">
      <numValues>0</numValues>
      <description><![CDATA[Minimum size of a vector created during net vectorisation]]></description>
      <integer>
      </integer>
    </option>
    <option name="-netVecPrimary">
      <numValues>0</numValues>
      <description><![CDATA[Vectorise primary ports]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-netVecThroughPrimary" trueValue="-netVecThroughPrimary" falseValue="-noNetVecThroughPrimary">
      <numValues>0</numValues>
      <description><![CDATA[Propagate vectorisation through primary ports but do not replace scalar primary ports with vector ports]]></description>
      <bool>
        <default>true</default>
      </bool>
    </option>
    <option name="-no-OOB">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to ignore out-of-bounds bit references, i.e., the Carbon compiler will not check for such references. As a result, the compile time will be faster, the generated code will be smaller and the resulting design should run faster. If you specify this option, the model will exhibit unpredictable behavior during run time if there are out-of-bounds bit or memory references.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-noCoercePorts">
      <numValues>0</numValues>
      <description><![CDATA[By default ports are redeclared based on the read/write characteristics of the design. This overrides that functionality. Turning off the feature disables the ability to process complex bidirectional ports.]]></description>
    </option>
    <option name="-sanitizeCheck">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to check for dirty writes. After every write to a net, it will check for non-zero bits outside the declared size of the net.  This helps to detect out-of-bound reads/writes permitted by the -no-OOB flag.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-waveformDumpSizeLimit">
      <numValues>1</numValues>
      <description><![CDATA[multiple instances allowed. Specify the maximum size (in bits) of design elements that should be dumped to waveform files.  This is an alternative to the carbonDumpSizeLimit() API function, although the API function has precedence.]]></description>
      <integer>
        <default>1024</default>
      </integer>
    </option>
  </section>
  <section name="Output Control">
    <option name="-q">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to enable quiet mode and suppress banner output.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
     <option name="-noCModelWrapper">
      <numValues>0</numValues>
      <description><![CDATA[Disable generation of C-Model wrapper.  Use this option to prevent generation of code and directives that make the design callable as a C-Model from other Carbon models.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-noFullDB">
      <numValues>0</numValues>
      <description><![CDATA[Suppress debug database generation.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-profile">
      <numValues>0</numValues>
      <description><![CDATA[Enable block profiling.  Compiling creates lib<design>.prof, simulating creates carbon_profile.dat.  Run "carbon profile" to see results.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-stats">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to print time and memory statistics for the compile to stdout.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-reportNetVec">
      <numValues>1</numValues>
      <description><![CDATA[Create a net vectorization report.]]></description>
      <string>
        <default></default>
      </string>
    </option>
    <option name="-version">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to obtain the version of the Carbon compiler. The compiler will exit after printing the version.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-w">
      <numValues>0</numValues>
      <description><![CDATA[Specify this option to suppress all warnings generated by the Carbon compiler.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
  </section>
  <section name="VHDL Options">
    <option name="-87" exclusiveWith="-93" trueValue="-87" falseValue="">
      <numValues>0</numValues>
      <description><![CDATA[Use VHDL-87 syntax for all following VHDL files until a "-93" is encountered.  If not specified, all files are parsed with VHDL-93 syntax rules.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-93" exclusiveWith="-87" trueValue="-93" falseValue="">
      <numValues>0</numValues>
      <description><![CDATA[Use VHDL-93 syntax for all following VHDL files until a "-87" is encountered.  If not specified, all files are parsed with VHDL-93 syntax rules.]]></description>
      <bool>
        <default>true</default>
      </bool>
    </option>
    <option name="-vhdlCase">
      <numValues>1</numValues>
      <description><![CDATA[Specifies treatment of VHDL identifiers.  Legal values are lower, upper, or preserve.]]></description>
      <enum>
        <default>lower</default>
        <option>lower</option>
        <option>upper</option>
        <option>preserve</option>
      </enum>
    </option>
    <option name="-vhdlEnableFileIO">
      <numValues>0</numValues>
      <description><![CDATA[By default the Carbon compiler ignores the vhdl file-IO subprograms: file_open, read, write, readline, file_close etc. This option enables their inclusion in the generated model.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-vhdlExt">
      <numValues>0</numValues>
      <description><![CDATA[Specifies colon-separated list of extensions used to identify vhdl files. The '.' in the extension is optional. The list of extensions augments the .vhd and .vhdl extensions. The extensions are case-insensitive.]]></description>
      <string>
        <default></default>
      </string>
    </option>
    <option name="-vhdlLib">
      <numValues>1</numValues>
      <description><![CDATA[Specify a VHDL library.  Multiple instances of this switch are allowed.  The last specified library is the WORK library for the design.  VHDL source files will be compiled into the last preceding library specified on the command line.  VHDL files preceding all occurrences of -vhdlLib will be compiled into the default library.]]></description>
      <string>
        <default></default>
      </string>
    </option>
    <option name="-vhdlNoWriteLib">
      <numValues>0</numValues>
      <description><![CDATA[Do not store any parsed VHDL design units on disk, and do not create any new disk libraries.  Any preexisting user libraries will remain unmodified.  System libraries will remain unaffected.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-vhdlTop">
      <numValues>0</numValues>
      <description><![CDATA[Top entity name in VHDL flow.]]></description>
      <string>
        <default></default>
      </string>
    </option>
    <option name="-vhdl_synth_prefix">
      <numValues>1</numValues>
      <description><![CDATA[Specify a prefix for synthesis specific compiler directives.  Multiple instances of this switch are allowed.]]></description>
      <stringlist>
      </stringlist>
    </option>
  </section>
  <section name="Verilog Options">
    <option name="+define+">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to specify Verilog macros to be used during compilation. Enter the variables with optional values, linked with plus signs. Syntax: +define+<var1>+<var2>+ ... +<varN>=<value>. The equals sign (=) effectively terminates the string. That is, anything after the equals sign will be treated as part of the value of the variable with which it is associated. If the value is not given the variable is defined so that `ifdef constructs will work. Later defines override earlier defines on the command line.]]></description>
      <stringlist repeat="OptionNameValueCompressed">
        <default></default>
      </stringlist>
    </option>
    <option name="+incdir+">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to specify the directories that the Carbon compiler should search for include files. Enter the list of relative or absolute paths, linked with plus signs (+). The paths will be searched in the order specified.]]></description>
      <filelist repeat="OptionNameValueCompressed" type="InputDirectories" filter="Directories (*)">
      </filelist>
    </option>
    <option name="+libext+">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to specify extensions used on the files you want to reference in a library directory (see the -y option). There is no default value for these extensions.  If multiple +libext+ are specified, only the last is used.   Multiple extensions may be specified in a single libext argument when separated by plus signs (+) (e.g. +libext++.v+.vlog+ defines three possible extensions: the empty string, '.v', and '.vlog') . ]]></description>
      <stringlist repeat="OptionNameValueCompressed">
        <default></default>
      </stringlist>
    </option>
    <option name="+maxdelay">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to select the maximum delay for all min:typ:max expressions, and evaluate static expressions using the maximum values for all min:typ:max expressions.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="+mindelay">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to select the minimum delay for all min:typ:max expressions, and evaluate static expressions using the minimum values for all min:typ:max expressions.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="+protect[.ext]">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to generate protected source versions of all given Verilog input files.  New files are generated for each input, with all source between `protect and `endprotect compiler directives in encoded form.  By default, the output file is named with a .vp extension.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="+typdelay">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to select the typical delay for all min:typ:max expressions, and evaluate static expressions using the typical values for all min:typ:max expressions.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-2001">
      <numValues>0</numValues>
      <description><![CDATA[This option enables Verilog-2001 compilation mode. All files encountered during the compilation will be treated as Verilog 2001.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-allow">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to allow multiple definitions of a wire in the Carbon Model.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-duplicate">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to mark the file as a high priority file.  This will cause the file to be compiled first.  If a normal source file contains a module defined in the high priority file, that module will be ignored in the normal file.  It is an error if multiple high priority files define the same module.]]></description>
      <string>
        <default></default>
      </string>
    </option>
    <option name="-enableOutputSysTasks">
      <numValues>0</numValues>
      <description><![CDATA[By default the Carbon compiler ignores the system tasks: $display, $write, $fdisplay, $fwrite. This option enables their inclusion in the generated model. See also the module specific directive 'enableOutputSysTasks']]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-keepfirst">
      <numValues>0</numValues>
      <description><![CDATA[This this option to allow multiple definitions of modules, and give first occurrences of modules precedence. The Carbon compiler will maintain the first module definition it reads, even if it encounters another of the same name later in the compilation process. If you do not specify either this option or -override, the compiler will terminate and issue an error.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-noPortDeclarationExtensions">
      <numValues>0</numValues>
      <description><![CDATA[This option disables the ability to use, in a single module, a mixture of the two styles of module port declarations: a simple list of port names and a list-of-ports(-2001).  Although this extension is enabled by default it is not part of the Verilog language standard.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-no_translate">
      <numValues>0</numValues>
      <description><![CDATA[Ignore translate_on/translate_off pragmas, while still adhering to other pragmas such as full_case/parallel_case.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-override">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to allow multiple definitions of modules, and give later defined modules precedence. The Carbon compiler will use the last module definition it reads during the compilation process when it has encountered multiple modules with the same name. If you do not specify either this option or -keepfirst, the compiler will terminate and issue an error.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-synth_prefix">
      <numValues>1</numValues>
      <description><![CDATA[Specify the prefix for compiler pragmas.  Multiple instances of this switch are allowed, each one adds to the list of recognized pragma prefixes.]]></description>
      <stringlist repeat="OptionNameValue">
        <default><![CDATA[carbon]]></default>
      </stringlist>
    </option>
    <option name="-topModuleListDumpFile">
      <numValues>0</numValues>
      <description><![CDATA[Specifies the file into which Carbon compiler will place the names of the top level modules of the Verilog design.]]></description>
      <string>
        <default></default>
      </string>
    </option>
    <option name="-u">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to make the Carbon compiler case insensitive to Verilog input. All Verilog constructs will compile properly regardless of case; however, all names will be converted to upper case.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-vlogLib">
      <numValues>1</numValues>
      <description><![CDATA[Specify a Verilog library. With -compileLibOnly, only one library can be specified. All verilog files are compiled into that library. Without -compileLibOnly, multiple libraries can be specified. The libraries are used read-only. The last library specified becomes the WORK library for design.]]></description>
      <string>
        <default></default>
      </string>
    </option>
    <option name="-vlogTop">
      <numValues>1</numValues>
      <description><![CDATA[This option allows you to specify which module in the design hierarchy is the top-most module. The Carbon compiler will analyze only the specified module and its descendents. If you do not specify this option, all modules from the top down in the given design will be compiled.]]></description>
      <string>
        <default></default>
      </string>
    </option>
    <option name="-warnForSysTask">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to generate an Alert message if an undefined system task or function is found in the design.]]></description>
      <bool>
        <default>false</default>
      </bool>
    </option>
    <option name="-y">
      <numValues>0</numValues>
      <description><![CDATA[Use this option to specify a library directory. The Carbon compiler will scan the directory for module definitions that have not been resolved in the specified design or library files. Enter the directory path after the option. The file names within the specified directory must match the module names that are being searched (using one of the file extensions defined by the +libext option).]]></description>
      <filelist repeat="OptionNameValue" type="InputDirectories" filter="Directories (*)">
      </filelist>
    </option>
  </section>
</Properties>
