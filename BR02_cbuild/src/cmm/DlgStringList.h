#ifndef DLGSTRINGLIST_H
#define DLGSTRINGLIST_H

#include "util/CarbonPlatform.h"

#include "ListWidget.h"
#include <QListWidget>
#include <QDialog>
#include "ui_DlgStringList.h"
#include "util/UtStringArray.h"
#include "CarbonPropertyFilelist.h"

class CarbonProject;

class DlgStringList : public QDialog
{
  Q_OBJECT

public:
  DlgStringList(QWidget *parent = 0, UtStringArray* inputItems=0, bool isFileList=false, CarbonProject* proj=NULL, const char* componentName=NULL);
  ~DlgStringList();
  UtStringArray* getItemArray() {return &mItems; }
  void setFilter(const char* v) { mFilter = v; }
  void setFileKind(CarbonPropertyFilelist::FileType v) { mFileKind=v; }
  CarbonPropertyFilelist::FileType getFileKind() const { return mFileKind; }
  const char* getFilter() const { return mFilter.c_str(); }

  QListWidget* getListWidget() { return ui.listWidget; }

private:
  Ui::DlgStringListClass ui;

private slots:
  void on_buttonBox_rejected();
  void on_buttonBox_accepted();
  void on_pushButtonDeleteItem_clicked();
  void on_pushButtonNewItem_clicked();
  void on_pushButtonMoveUp_clicked();
  void on_pushButtonMoveDown_clicked();
  void selectionChanged();

  friend class DlgStringListDelegate;

private:
  bool mFileList;
  UtStringArray mItems;
  UtString mFilter;
  UtString mComponentDir;
  CarbonPropertyFilelist::FileType mFileKind;
  CarbonProject* mProject;
};

// The single delegate which then delegates to a CarbonDelegate for
// each type
class DlgStringListDelegate : public QItemDelegate
{
  Q_OBJECT

public:
    DlgStringListDelegate(QObject *parent = 0, DlgStringList* sl=0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
        const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
        const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;

    void commit(QWidget* w)
    {
      commitData(w);
      closeEditor(w);
    }

private slots:
  void commitAndCloseEditor();
  void showFilePromptDialog();

private:
  DlgStringList* sl;
};


#endif // DLGSTRINGLIST_H
