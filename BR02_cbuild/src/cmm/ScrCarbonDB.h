#ifndef __SCRCARBONDB_H__
#define __SCRCARBONDB_H__

#include "util/CarbonPlatform.h"
#include "cfg/CarbonCfg.h"

#include <QtGui> 
#include <QProcess>

#include "Scripting.h"

class JavaScriptAPIs;

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "carbon/carbon_dbapi.h"

class CarbonDatabaseContext;




// File: CarbonDB
//
// The CarbonDB object represents the compiled Carbon Design
// 
// If there is no project opened or the design has not
// yet beein compiled then this property will be null.
//
// Example:
//
//(start code)
//var primaryPorts = CarbonDB.loopPrimaryPorts();
//
//var node = CarbonDB.nodeIterNext(primaryPorts);
//while (node != null)
//{
//  debug(CarbonDB.nodeGetFullName(node) + " " + CarbonDB.nodeGetLeafName(node));
//  node = CarbonDB.nodeIterNext(primaryPorts);
//}
//
//CarbonDB.freeNodeIter(primaryPorts);
//
//(end)
//


//
// Class: CarbonDB
//
// Example:
//
//(start code)
//var primaryPorts = CarbonDB.loopPrimaryPorts();
//
//var node = CarbonDB.nodeIterNext(primaryPorts);
//while (node != null)
//{
//  debug(CarbonDB.nodeGetFullName(node) + " " + CarbonDB.nodeGetLeafName(node));
//  node = CarbonDB.nodeIterNext(primaryPorts);
//}
//
//CarbonDB.freeNodeIter(primaryPorts);
//
//(end)
//
class ScrCarbonDB : public QObject, public QScriptable
{
  Q_OBJECT
 
  Q_CLASSINFO("ClassName", "CarbonDB");

  Q_PROPERTY(bool hasDatabase READ hasDatabase);
  Q_PROPERTY(QString databaseFileName READ getDatabaseFileName);

public:
  ScrCarbonDB(QObject *parent = 0);
  CarbonDB* getDB() { return mDB; }
  static void registerTypes(QScriptEngine*);
  static void registerScriptTypes(QMap<QString, const QMetaObject*>& map);
  static void registerIntellisense(JavaScriptAPIs* apis);

public slots:
  QString getDatabaseFileName() const { return mDBFileName; }
  bool hasDatabase() const { return mDB != NULL; }

// Method: findNode
//   Search for and returns the <CarbonDBNode> maching path
//
// Parameters: 
//  path - The fullpath String of the node being located.
//   
// Returns:
//   <CarbonDBNode>
//
  CarbonDBNode* findNode(const QString& path)
  {
    UtString name;
    name << path;
    return (CarbonDBNode*)carbonDBFindNode(mDB, name.c_str());
  }

// Method: loopPrimaryPorts
// Returns the <CarbonDBNodeIter> for all the
// primary ports for the design.
//
// Notes:
//   The returned <CarbonDBNodeIter> must be freed by calling
//   <CarbonDB.freeNodeIter>
//
// Returns:
//   <CarbonDBNodeIter>
//
  CarbonDBNodeIter* loopPrimaryPorts() { return carbonDBLoopPrimaryPorts(mDB); }
// Method: nodeGetFullName
//
// Parameters: 
//  node - A <CarbonDBNode> object.
//
// Returns the full path of the node passed in.
//
// Returns:
//   String full path name
//
// See Also:
//  <nodeGetLeafName>
//
  QString nodeGetFullName(CarbonDBNode* node) { return carbonDBNodeGetFullName(mDB, node); }
// Method: nodeGetLeafName
//
// Parameters: 
//  node - A <CarbonDBNode> object.
//
// Returns the leaf name of the node passed in
//
// Returns:
//   String leaf name
//
// See Also:
//  <nodeGetFullName>
//
  QString nodeGetLeafName(CarbonDBNode* node) { return carbonDBNodeGetLeafName(mDB, node); }

// Method: nodeIterNext
//  Returns the next node in the iteration object or null if at the end.
//
// Parameters: 
//  iter - A <CarbonDBNodeIter> iteration object.
//
// Returns:
//   <CarbonDBNode>
//
  CarbonDBNode* nodeIterNext(CarbonDBNodeIter* iter) { return (CarbonDBNode*)carbonDBNodeIterNext(iter); }

// Method: freeNodeIter
//   Frees an iteration object.
//
// Parameters: 
//  iter - A <CarbonDBNodeIter> object.
//
//
  void freeNodeIter(CarbonDBNodeIter* iter) { carbonDBFreeNodeIter(iter); }

// Method: loopChildren
//   Iterate the children of the iter object passed in.
//
// Parameters: 
//  node - A <CarbonDBNode> object.
//
// Notes:
//   The returned <CarbonDBNodeIter> must be freed by calling
//   <CarbonDB.freeNodeIter>
//
// Returns:
//   <CarbonDBNodeIter>
//
  CarbonDBNodeIter* loopChildren(CarbonDBNode* node) { return carbonDBLoopChildren(mDB, node); }
  
// Method: nodeGetParent
//   Returns the parent <CarbonDBNode> for the node passed in.
//
// Parameters: 
//  node - A <CarbonDBNode> object.
//   
// Returns:
//   <CarbonDBNode>
//
  CarbonDBNode* nodeGetParent(CarbonDBNode* node) { return (CarbonDBNode*)carbonDBNodeGetParent(mDB, node); }

// Method: findChild
//   Search for and returns the <CarbonDBNode> maching childName
//
// Parameters: 
//  parent - A <CarbonDBNode> object.
//  childName - The String of the child node being located.
//   
// Returns:
//   <CarbonDBNode>
//
  CarbonDBNode* findChild(CarbonDBNode* parent, const QString& childName)
  {
    UtString value;
    value << childName;
    return (CarbonDBNode*)carbonDBFindChild(mDB, parent, value.c_str());
  }

// Method: getParserNumber
//   Returns identity of parser used to parse RTL (Interra, Verific)
//
// Parameters: 
//   none
//   
// Returns:
//   0 => Interra Parser 
//   1 => Verific Parser
//
  int getParserNumber() 
  {
    int parserNumber = 0;
    CarbonUInt32 val = 0;
    if (eCarbon_OK == carbonDBGetIntAttribute(mDB, "parserNumber", &val))
      parserNumber = val;
    return parserNumber;
  }
  
  QString getVersion() { return carbonDBGetVersion(); }
  QString getSoftwareVersion() { return carbonDBGetSoftwareVersion(mDB); }
  QString getIdString() { return carbonDBGetIdString(mDB); }
  QString getTopLevelModuleName() { return carbonDBGetTopLevelModuleName(mDB); }
  QString getInterfaceName() { return carbonDBGetInterfaceName(mDB); }
  QString getSystemCModuleName() { return carbonDBGetSystemCModuleName(mDB); }

  CarbonDBNodeIter* loopPrimaryInputs() { return carbonDBLoopPrimaryInputs(mDB); }
  CarbonDBNodeIter* loopPrimaryOutputs() { return carbonDBLoopPrimaryOutputs(mDB); }
  CarbonDBNodeIter* loopPrimaryBidis() { return carbonDBLoopPrimaryBidis(mDB); }
  CarbonDBNodeIter* loopPrimaryClks() { return carbonDBLoopPrimaryClks(mDB); }
  CarbonDBNodeIter* loopClkTree() { return carbonDBLoopClkTree(mDB); }
  CarbonDBNodeIter* loopDepositable() { return carbonDBLoopDepositable(mDB); }
  CarbonDBNodeIter* loopObservable() { return carbonDBLoopObservable(mDB); }
  CarbonDBNodeIter* loopScDepositable() { return carbonDBLoopScDepositable(mDB); }
  CarbonDBNodeIter* loopScObservable() { return carbonDBLoopScObservable(mDB); }
  CarbonDBNodeIter* loopLoopAsyncs() { return carbonDBLoopAsyncs(mDB); }
  CarbonDBNodeIter* loopAsyncOutputs() { return carbonDBLoopAsyncOutputs(mDB); }
  CarbonDBNodeIter* loopAsyncDeposits() { return carbonDBLoopAsyncDeposits(mDB); }
  CarbonDBNodeIter* loopAsyncPosResets() { return carbonDBLoopAsyncPosResets(mDB); }
  CarbonDBNodeIter* loopAsyncNegResets() { return carbonDBLoopAsyncNegResets(mDB); }
  CarbonDBNodeIter* loopPosedgeTriggers() { return carbonDBLoopPosedgeTriggers(mDB); }
  CarbonDBNodeIter* loopNegedgeTriggers() { return carbonDBLoopNegedgeTriggers(mDB); }
  CarbonDBNodeIter* loopBothEdgeTriggers() { return carbonDBLoopBothEdgeTriggers(mDB); }
  CarbonDBNodeIter* loopPosedgeTrigger() { return carbonDBLoopPosedgeTriggers(mDB); }

  QString nodeComponentName(CarbonDBNode* node) { return carbonDBComponentName(mDB, node); }
  QString nodeSourceLanguage(CarbonDBNode* node) { return carbonDBSourceLanguage(mDB, node); }
  QString nodeGetSourceFile(CarbonDBNode* node) { return carbonDBGetSourceFile(mDB, node); }

  int nodeGetWidth(CarbonDBNode* node) { return carbonDBGetWidth(mDB, node); }
  int nodeGetMSB(CarbonDBNode* node) { return carbonDBGetMSB(mDB, node); }
  int nodeGetLSB(CarbonDBNode* node) { return carbonDBGetLSB(mDB, node); }
  int nodeGetBitSize(CarbonDBNode* node) { return carbonDBGetBitSize(mDB, node); }
  int nodeGetSourceLine(CarbonDBNode* node) { return carbonDBGetSourceLine(mDB, node); }
  int nodeGetArrayLeftBound(CarbonDBNode* node) { return carbonDBGetArrayLeftBound(mDB, node); }
  int nodeGetArrayRightBound(CarbonDBNode* node) { return carbonDBGetArrayRightBound(mDB, node); }
  int nodeGetArrayDims(CarbonDBNode* node) { return carbonDBGetArrayDims(mDB, node); }
  
  bool nodeIsContainedByComposite(CarbonDBNode* node) { return carbonDBIsContainedByComposite(mDB, node); }
  bool nodeIsStruct(CarbonDBNode* node) { return carbonDBIsStruct(mDB, node); }
  bool nodeIsArray(CarbonDBNode* node) { return carbonDBIsArray(mDB, node); }
  bool nodeIsEnum(CarbonDBNode* node) { return carbonDBIsEnum(mDB, node); }
  bool nodeCanBeCarbonNet(CarbonDBNode* node) { return carbonDBCanBeCarbonNet(mDB, node); }
  bool nodeIsScalar(CarbonDBNode* node) { return carbonDBIsScalar(mDB, node); }
  bool nodeIsTristate(CarbonDBNode* node) { return carbonDBIsTristate(mDB, node); }
  bool nodeIsVector(CarbonDBNode* node) { return carbonDBIsVector(mDB, node); }
  bool nodeIsConstant(CarbonDBNode* node) { return carbonDBIsConstant(mDB, node); }
  bool nodeIs2DArray(CarbonDBNode* node) { return carbonDBIs2DArray(mDB, node); }
  bool nodeIsClkTree(CarbonDBNode* node) { return carbonDBIsClkTree(mDB, node); }
  bool nodeIsAsync(CarbonDBNode* node) { return carbonDBIsAsync(mDB, node); }
  bool nodeIsAsyncOutput(CarbonDBNode* node) { return carbonDBIsAsyncOutput(mDB, node); }
  bool nodeIsAsyncDeposit(CarbonDBNode* node) { return carbonDBIsAsyncDeposit(mDB, node); }
  bool nodeIsAsyncPosReset(CarbonDBNode* node) { return carbonDBIsAsyncPosReset(mDB, node); }
  bool nodeIsAsyncNegReset(CarbonDBNode* node) { return carbonDBIsAsyncNegReset(mDB, node); }
  
  bool nodeIsForcible(CarbonDBNode* node) { return carbonDBIsForcible(mDB, node); }
  bool nodeIsDepositable(CarbonDBNode* node) { return carbonDBIsDepositable(mDB, node); }

  bool nodeIsScDepositable(CarbonDBNode* node) { return carbonDBIsScDepositable(mDB, node); }

  bool nodeIsObservable(CarbonDBNode* node) { return carbonDBIsObservable(mDB, node); }
  bool nodeIsVisible(CarbonDBNode* node) { return carbonDBIsVisible(mDB, node); }
 
  bool nodeIsTied(CarbonDBNode* node) { return carbonDBIsTied(mDB, node); }

private:
  QString mDBFileName;
  CarbonDB* mDB;
  CarbonDatabaseContext* mDBContext;
};

#endif
