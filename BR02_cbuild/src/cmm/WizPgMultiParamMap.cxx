//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "WizPgMultiParamMap.h"
#include "MemoryWizard.h"
#include "cmm.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"
#include "DesignXML.h"

#define UNUSED_ITEM "<Unused>"

WizPgMultiParamMap::WizPgMultiParamMap(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);
  setTitle("Parameter Mapping");
  setSubTitle("Map all module parameters to their equivalent function.");
  
  QStringList labels;
  labels << "Parameter";
  labels << "Function";

  ui.tableWidget->setColumnCount(labels.count());
  ui.tableWidget->setHorizontalHeaderLabels(labels);
}

WizPgMultiParamMap::~WizPgMultiParamMap()
{
  mIsValid = true;
}

void WizPgMultiParamMap::initializePage()
{
  populate();
}

void WizPgMultiParamMap::readPreviousMappings()
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  QString configDir = proj->getActive()->getOutputDirectory();
  QString desName = proj->getDesignFilePath(".designHierarchy");
  QFileInfo fi(desName);
  QString xmlFile = QString("%1/%2.memGen").arg(configDir).arg(fi.baseName());

  mPreviousMappings.clear();

  QFile file(xmlFile);

  if (file.open(QFile::ReadOnly | QFile::Text))
  {
    QDomDocument doc;
    int errorLine;
    int errorCol;
    QString errStr;

    QFileInfo templateFi(xmlFile);

    if (doc.setContent(&file, true, &errStr, &errorLine, &errorCol))
    { 
      QDomElement docElem = doc.documentElement();
      QDomNode n = docElem.firstChild();
      while(!n.isNull())
      {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        n = n.nextSibling();
        if (!e.isNull() && !e.isComment())
        {
          if ("ModuleParameters" == e.tagName())
          {
            QDomNode n = e.firstChild();
            while(!n.isNull())
            {
              QDomElement element = n.toElement(); // try to convert the node to an element.
              n = n.nextSibling();
              if (!element.isNull() && !element.isComment())
              {
                if ("Parameter" == element.tagName())
                {
                  MWParamMap* pm = new MWParamMap(
                    element.attribute("name"),
                    element.attribute("parameterFunction"));

                  mPreviousMappings[element.attribute("name")] = pm;
                }
              }
            }
            break;
          }
        }
      }
    }
  }
}

void WizPgMultiParamMap::populate()
{
  MemoryWizard* wiz = static_cast<MemoryWizard*>(wizard());
  MWPortConfigurations* pf = wiz->getPortConfigurations();
  
  if (ui.checkBoxRemember->isChecked())
    readPreviousMappings();

  ui.tableWidget->clearContents();
  ui.tableWidget->setRowCount(pf->getModuleParameters().count());

  int row = 0;

  foreach (MWModuleParam* modParm, pf->getModuleParameters())
  {
    // Name
    QTableWidgetItem* nameItem = new QTableWidgetItem();
    nameItem->setText(modParm->getName());
    Qt::ItemFlags flags = nameItem->flags();
    nameItem->setFlags(flags & ~Qt::ItemIsEditable);

    QVariant qv = qVariantFromValue((void*)modParm);
    nameItem->setData(Qt::UserRole, qv);

    ui.tableWidget->setItem(row, WizPgMultiParamMap::colNAME, nameItem);
    
    // Function ComboBox
    QComboBox* cbox = new QComboBox(ui.tableWidget);
    cbox->insertItem(0, UNUSED_ITEM, "");
    int comboIndex=1;
    foreach (MWModuleParamFunction* parmFunc, pf->getModuleParameterFunctions())
    {
      cbox->insertItem(comboIndex++, parmFunc->getName(), parmFunc->getParameter());
    }
    // Default to Unused
    cbox->setCurrentIndex(0);

    if (ui.checkBoxRemember->isChecked())
    {
      MWParamMap* pm = mPreviousMappings[modParm->getName()];
      if (pm)
      {
        int index = cbox->findData(pm->getParamFunction());
        if (index != -1)
        {
          cbox->setCurrentIndex(index);
        }
      }
    }

    cbox->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLength);
    ui.tableWidget->setCellWidget(row, WizPgMultiParamMap::colFUNCTION, cbox);

    row++;
  }
}
bool WizPgMultiParamMap::validatePage()
{
  MemoryWizard* wiz = static_cast<MemoryWizard*>(wizard());

  wiz->removeAllParamMaps();   
  
  for (int i=0; i<ui.tableWidget->rowCount(); i++)
  {
    QTableWidgetItem* nameItem = ui.tableWidget->item(i, WizPgMultiParamMap::colNAME);
    QString paramName = nameItem->text();

    QComboBox* funcCombo = qobject_cast<QComboBox*>(ui.tableWidget->cellWidget(i, WizPgMultiParamMap::colFUNCTION));
    QString paramFunction = funcCombo->currentText();

    if (UNUSED_ITEM == paramFunction)
      wiz->addParamMapping(paramName, "");
    else
    {
      QString paramFunc = funcCombo->itemData(funcCombo->currentIndex()).toString();
      wiz->addParamMapping(paramName, paramFunc);
    }
  }

  return true;
}

void WizPgMultiParamMap::on_checkBoxRemember_stateChanged(int)
{
  qDebug() << "remember setting changed" << ui.checkBoxRemember->isChecked();
  if (!ui.checkBoxRemember->isChecked())
  {
    for (int i=0; i<ui.tableWidget->rowCount(); i++)
    {
      QComboBox* funcCombo = qobject_cast<QComboBox*>(ui.tableWidget->cellWidget(i, WizPgMultiParamMap::colFUNCTION));
      funcCombo->setCurrentIndex(funcCombo->findText(UNUSED_ITEM));
    }
  }
}
