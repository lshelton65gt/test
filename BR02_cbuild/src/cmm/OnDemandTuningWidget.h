// -*-C++-*-

#include "gui/CQt.h"
#include "OnDemandTuningGraph.h"
#include "util/UtBiMap.h"
#include "util/XmlParsing.h"

class CarbonMakerContext;
class CarbonRuntimeState;
struct CarbonReplaySystem;
class CarbonOptions;

class OnDemandTuningWidget : public QWidget {
  Q_OBJECT
public:
  CARBONMEM_OVERRIDES

  OnDemandTuningWidget(QWidget* parent, CarbonMakerContext* ctx);
  ~OnDemandTuningWidget();

  void setViolators(OnDemandTuningGraph::Quantum* quantum);
  void putComponent(const char* compName);

  bool loadTraceFile(const char* fname, UtString* errmsg);

  //! Here are the various ways we exclude active nodes from violating onDemand
  enum Excuse {
    eOnDemandIdleDeposit = 0x1,
    eOnDemandExcluded    = 0x2,
    eOnDemandBoth        = 0x3
  };


  void findExcusedNodes(UtStringArray* nodes) const;

  Excuse findExcuse(const char* node);

  bool addExcuse(const char* violator, Excuse excuse);

public slots:
  void quantumSelect(UInt32 quantum_index);
  void quantumUnselect();
  void selectViolator();
  void selectExcused();
  void dropViolator(const char* violator, QTreeWidgetItem*);
  void addExcuse();
  void deleteExcuse();
  void applyToComponent();
  void makePermanentInModel();
  void scaleChanged();
  void sliderMoved(int value);
  void selectLeft();
  void selectRight();
  void carbonSystemUpdated(CarbonRuntimeState &state, CarbonReplaySystem &guiSystem, CarbonReplaySystem &simSystem);

signals:
  void changeExcused();

private:
  OnDemandTuningGraph* mGraph;
  CDragDropTreeWidget* mViolators;
  CDragDropTreeWidget* mExcused;

  void collectValues(UtString* excludedStates, UtString* excludedInputs, QStringList* statesList = NULL, QStringList* inputsList = NULL);
  bool isComponentFromProject();
  void registerForSystemUpdates();
  void updateMakePermanentButton();
  void saveAttributes(CarbonOptions* options);
  void writeAttribute(CarbonOptions* options, xmlTextWriterPtr writer, const char* attName, const char* optionName);

  typedef UtBiMap<QTreeWidgetItem*,OnDemandTuningGraph::Node*> ItemNodeMap;
  ItemNodeMap mItemNodeMap;
  OnDemandTuningGraph::Quantum* mQuantum;

  typedef UtHashMap<UtString,Excuse> NodeExcusesMap;
  NodeExcusesMap mNodeExcusesMap;

  void populateExcusesMap();
  void populateExcusedWidget();

  CarbonMakerContext* mCarbonMakerContext;

  QPushButton* mAddExcuse;
  QPushButton* mDeleteExcuse;
  QPushButton* mApplyToComponent;
  QPushButton* mMakePermanentInModel;
  QPushButton* mSelectLeft;
  QPushButton* mSelectRight;
  QSlider* mVZoom;

  bool mChangedSinceApply;
  bool mChangedSinceMakePermanent;

  UtString mComponent; // The Component that this trace is for.
  bool mUpdatesRegistered;
};
