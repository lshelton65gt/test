//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include <QtGui>

#include "DlgImportCcfg.h"
#include "CarbonProjectWidget.h"
#include "CarbonMakerContext.h"

DlgImportCcfg::DlgImportCcfg(QWidget *parent, CarbonProjectWidget* pw)
: QDialog(parent)
{
  mProjectWidget = pw;
  ui.setupUi(this);

  ui.radioButtonCoWare->setDisabled(!mProjectWidget->context()->getCanCreateCoware());
  ui.radioButtonCoWare->setChecked(mProjectWidget->context()->getCanCreateCoware());

  ui.radioButtonSocDesigner->setDisabled(!mProjectWidget->context()->getCanCreateMaxsim());
  ui.radioButtonSocDesigner->setChecked(mProjectWidget->context()->getCanCreateMaxsim());
}

DlgImportCcfg::~DlgImportCcfg()
{
}

void DlgImportCcfg::on_buttonBox_accepted()
{
  if (legalValues())
  {
    mFilename = ui.lineEditFileName->text();
    if (ui.radioButtonCoWare->isChecked())
      mType = eCcfgCoWare;
    else if (ui.radioButtonSocDesigner->isChecked())
      mType = eCcfgArm;

    mFilename = ui.lineEditFileName->text();

    accept();
  }
}

void DlgImportCcfg::on_buttonBox_rejected()
{
  reject();
}


void DlgImportCcfg::on_toolButtonBrowse_clicked()
{
  QString fileName = QFileDialog::getOpenFileName(mProjectWidget, tr("Carbon Wizard File"), "", tr("Wizard File (*.ccfg)"));
  if (fileName.length() > 0)
    ui.lineEditFileName->setText(fileName);
}

bool DlgImportCcfg::legalValues()
{
  UtString errMsg;
  bool status = true;
  bool somethingChecked = ui.radioButtonCoWare->isChecked() || ui.radioButtonSocDesigner->isChecked();

  if (!somethingChecked)
  {
    errMsg << "You must choose either Soc Designer or CoWare to proceed.\n";
    status = false;
  }

  QString filename = ui.lineEditFileName->text().trimmed();
  if (filename.length() == 0)
  {
    errMsg << "The filename to import is not specified.\n";
    status = false;
  }
  else
  {
    QFileInfo fi(ui.lineEditFileName->text());
    if (!fi.exists())
    {
      errMsg << "The filename '" << fi.fileName() << "' is not valid or cannot be accessed.\n";
      status = false;
    }
  }
  
  if (!status)
    QMessageBox::critical(this, MODELSTUDIO_TITLE, errMsg.c_str());
  
  return status;
}

