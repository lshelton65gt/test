//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include <QtGui>
#include "DlgCopyConfiguration.h"
#include "CarbonProjectWidget.h"

DlgCopyConfiguration::DlgCopyConfiguration(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);
  mProject = NULL;
}

DlgCopyConfiguration::~DlgCopyConfiguration()
{
}

void DlgCopyConfiguration::on_cancelButton_clicked()
{
  reject();
}

void DlgCopyConfiguration::setProject(CarbonProjectWidget* proj)
{
  mProject = proj;

  ui.lineEditName->selectAll();

  CarbonConfigurations* configs = mProject->project()->getConfigurations();

  ui.comboBoxConfigs->addItem("<Empty>");

  for (UInt32 i=0; i<configs->numConfigurations(); i++)
  {
    CarbonConfiguration* config = configs->getConfiguration(i);
    ui.comboBoxConfigs->addItem(config->getName());
  }
}

void DlgCopyConfiguration::on_okButton_clicked()
{
  QString value = ui.lineEditName->text().trimmed();

  // const char * foo = value.toStdString().c_str();
  QRegExp regexp("([A-Za-z_0-9]+)"); 

  bool legitimateValue = false;
  if (regexp.exactMatch(value) && regexp.cap(0) == value)
	legitimateValue = true;

  if (!legitimateValue)
  {
    UtString msg;
	msg << "A Configuration name may only contain alphanumeric characters and may not contain embedded spaces.";
    QMessageBox::warning(mProject, MODELSTUDIO_TITLE, msg.c_str());
    setResult(QDialog::Rejected);
    return;
  }

  mConfigName.clear();
  mConfigName << ui.lineEditName->text().trimmed();

  CarbonConfigurations* configs = mProject->project()->getConfigurations();
  if (configs->findConfiguration(mConfigName.c_str()) != NULL)
  {
    mConfigName.clear();
    UtString msg;
    msg << "A Configuration named <" << ui.lineEditName->text() << "> already exists, ignoring";
    QMessageBox::warning(mProject, MODELSTUDIO_TITLE, msg.c_str());
    setResult(QDialog::Rejected);
    return;
  }

  if (0 == strcmp(mConfigName.c_str(), "<New>"))
    mConfigName.clear();

  mCopyFrom.clear();
  mCopyFrom << ui.comboBoxConfigs->currentText();
  if (0 == strcmp(mCopyFrom.c_str(), "<Empty>"))
    mCopyFrom.clear();
}


