#ifndef DLGSYSTEMADDRESSMAPPING_H
#define DLGSYSTEMADDRESSMAPPING_H

#include <QtGui>

#include "ui_DlgSystemAddressMapping.h"

class CarbonCfgMemory;
struct CarbonCfg;
struct _AddressMapping;



class DlgSystemAddressMapping : public QDialog
{
  Q_OBJECT

 enum Columns { colPORT_NAME = 0, colBASE_ADDRESS };

public:
  DlgSystemAddressMapping(QWidget *parent = 0, CarbonCfg* ccfg = 0, CarbonCfgMemory* mem = 0);
  ~DlgSystemAddressMapping();

  bool isModified();

  friend class AddressMappingDelegate;

  int numMappings() { return ui.treeWidget->topLevelItemCount(); }

  const _AddressMapping& getMapping(int index);

private:
  Ui::DlgSystemAddressMappingClass ui;
  CarbonCfgMemory* mMemory;
  CarbonCfg* mCcfg;
  bool mIsModified;

  QMap<QString, _AddressMapping*> mPortMap;

private:
  void populate();

private slots:
  void on_buttonBox_accepted();
  void on_buttonBox_rejected();
};

class AddressMappingDelegate : public QItemDelegate
{
  Q_OBJECT


public:
  AddressMappingDelegate(QObject *parent = 0, QTreeWidget* t=0, CarbonCfg* cfg=0);
  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &, const QModelIndex &index) const;
  void setEditorData(QWidget *editor, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

private slots:
  void commitAndCloseEditor();

private:
  CarbonCfg* mCfg; 
  QTreeWidget* mTree;
};

#endif // DLGSYSTEMADDRESSMAPPING_H
