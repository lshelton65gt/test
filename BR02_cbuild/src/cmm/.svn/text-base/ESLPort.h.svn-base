#ifndef __ESLPORT_H_
#define __ESLPORT_H_

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "gui/CExprValidator.h"
#include "util/DynBitVector.h"
#include "CompWizardTreeNodes.h"
#include "PortEditorTreeWidget.h"
#include "cfg/CarbonCfg.h"
#include "cfg/carbon_cfg.h"
#include <QtGui>


class ESLPortExprItem: public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE } ;

  void setPortExpression(const QString& newVal)
  {
    setText(colVALUE, newVal);
  }
  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);
  virtual UndoData* deleteItem();

  ESLPortExprItem(QTreeWidgetItem* parent, CarbonCfgESLPort* eslPort)
    : PortEditorTreeItem(PortEditorTreeItem::ESLPortExpr, parent)
  {
    mESLPort = eslPort;
    //mValidator = new CExprValidator(NULL, "validator", true);
    setItalicText(colNAME, "Port Expression");
    setPortExpression(eslPort->getExpr());
    setFlags(flags() | Qt::ItemIsEditable);
  }

  class Delete : public UndoData
  {
  public:
    CarbonCfg* mCfg;
    QString mESLPortName;
    QString mPortExpr;

  protected:
    void undo();
    void redo();
  };


private:
  CarbonCfgESLPort* mESLPort;
  CExprValidator* mValidator;
};

class ESLPortItem;

// Handle multiple ESL port actions
class ESLPortActions : public QObject
{
  Q_OBJECT

public:
  enum Action { TieToOne, TieToZero, ResetGen, ClockGen, SystemCClockGen, AddPortExpression, ChangeInput, ChangeOutput }; 

  ESLPortActions(PortEditorTreeWidget* tree);

  QAction* getAction(Action);

private slots:
  void actionTieToOne();
  void actionTieToZero();
  void actionResetGen();
  void actionClockGen();
  void actionSystemCClockGen();
  void actionAddExpression();
  void actionChangeInput();
  void actionChangeOutput();

private:
  void actionTie(Action);
  void selectedItems(QList<ESLPortItem*>& tieItems);

  PortEditorTreeWidget* mTree;
  QAction* mTieToOne;
  QAction* mTieToZero;
  QAction* mResetGen;
  QAction* mClockGen;
  QAction* mSystemCClockGen;
  QAction* mAddExpr;
  QAction* mChangeInput;
  QAction* mChangeOutput;
};

// Shows the Parameter for an ESL Port
class ESLPortParameter : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colPARAMETER };

  CarbonCfgESLPort* getESLPort() { return mESLPort; }

  void setParamInstance(CarbonCfgXtorParamInst* inst)
  {
    setIcon(colNAME, QIcon(":/cmm/Resources/parameter.png"));

    if (inst->getInstance() == NULL)
      setItalicText(colNAME, "Parameter");
    else
      setItalicText(colNAME, inst->getInstance()->getName());
 
    setNormalText(colPARAMETER, "");

    if (inst)
    {
      QString value = inst->getDefaultValue();
      QString txt = QString("%1 (%2)").arg(inst->getParam()->getName()).arg(value);
      setNormalText(colPARAMETER, txt);
    }
  }

  ESLPortParameter(QTreeWidgetItem* parent, CarbonCfgESLPort* eslPort)
    : PortEditorTreeItem(PortEditorTreeItem::ESLPortParam, parent)
  {
    mESLPort = eslPort;

    setParamInstance(mESLPort->getParamInstance());
  }

  virtual UndoData* deleteItem();

  class Delete : public UndoData
  {
  public:
    CarbonCfg* mCfg;
    QString mESLPortName;
    PortTreeIndex mESLPortIndex;
    QString mParameterName;

  protected:
    void undo();
    void redo();
  };
private:
  CarbonCfgESLPort* mESLPort;
};

// An ESL Port
class ESLPortItem : public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colRTL, colSIZE, colRTL_MODE, colSCTYPE} ;

  static void resetData()
  {
    mActions = NULL;
  }

  // Editable fields
  void setPortName(const QString& newVal)
  {
    setText(colNAME, newVal);
  }

  void setPortTypeDef(const QString& newVal)
  {
    if (getTree()->getEditor()->getEditorMode() == CompWizardPortEditor::eModeCoWare || getTree()->getEditor()->getEditorMode() == CompWizardPortEditor::eModeSystemC)
    {
      mEslPortTypeDef.clear();
      mEslPortTypeDef << newVal;
      mESLPort->putTypeDef(mEslPortTypeDef.c_str());
      setText(colSCTYPE, newVal);
    }
  }

  void setPortMode(CarbonCfgESLPortMode eslPortMode)
  {
    if (getTree()->getEditor()->getEditorMode() == CompWizardPortEditor::eModeCoWare || getTree()->getEditor()->getEditorMode() == CompWizardPortEditor::eModeSystemC)
    {
      mESLPort->putMode(eslPortMode);
      switch(eslPortMode)
      {
        case eCarbonCfgESLPortUndefined:  setText(colRTL_MODE, "Undefined"); break;
        case eCarbonCfgESLPortControl:    setText(colRTL_MODE, "Control"); break;
        case eCarbonCfgESLPortClock:      setText(colRTL_MODE, "Clock"); break;
        case eCarbonCfgESLPortReset:      setText(colRTL_MODE, "Reset"); break;
      }
    }
  }

  void setPortType(CarbonCfgESLPortType eslPortType)
  {
    mESLPort->putPortType(eslPortType);
    setIcon(colNAME, icon(eslPortType));
  }

  CarbonCfgESLPortMode fromModeString(const QString& m);

  QTreeWidgetItem* setPortExpr(const QString& newVal)
  {
    UtString v; v << newVal;
    mESLPort->putExpr(v.c_str());

    if (newVal.isEmpty())
    {
      if (mEslPortExpr)
        getTree()->removeItem(mEslPortExpr);
      mEslPortExpr = NULL;
    }
    else
    {
      if (mEslPortExpr == NULL)
        mEslPortExpr = new ESLPortExprItem(this, mESLPort);

      mEslPortExpr->setPortExpression(newVal);
    }

    return mEslPortExpr;
  } 

  // Create a new item
  static ESLPortItem* createESLPort(PortEditorTreeWidget* tree,
                        UndoData* undoData, CarbonCfgRTLPort* rtlPort);
  
  static ESLPortItem* createESLPort(PortEditorTreeWidget* tree,
                        UndoData* undoData, const QString& rtlPortName);

  static CarbonCfgRTLPort* removeESLPort(PortEditorTreeItem* item, const QString& eslPortName);

  virtual void multiSelectContextMenu(QMenu&);
  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  virtual UndoData* deleteItem();
  static ESLPortItem* makeESLPort(CarbonCfg* cfg, 
                                    CarbonCfgRTLPort* rtlPort, 
                                    PortEditorTreeWidget* tree,
                                    const QString& eslPortName);

  static ESLPortItem* makeESLPort(CarbonCfg* cfg, 
                                    CarbonCfgRTLPort* rtlPort, 
                                    PortEditorTreeWidget* tree,
                                    QString& eslPortName,
                                    PortTreeIndex& eslPortIndex);

  CarbonCfgESLPort* getESLPort() { return mESLPort; }
  ESLPortExprItem* getExprItem() { return mEslPortExpr; }

  friend class ESLPortActions;

  ESLPortParameter* setParam(CarbonCfgESLPort* eslPort)
  {
    if (mParam)
      getTree()->removeItem(mParam);
    
    if (eslPort->getParamInstance())
      mParam = new ESLPortParameter(this, eslPort);

    if (mParam)
      setExpanded(true);

    return mParam;
  }

  ESLPortItem(QTreeWidgetItem* parent, CarbonCfgESLPort* eslPort)
    : PortEditorTreeItem(PortEditorTreeItem::ESLPort, parent)
  {
    mESLPort = eslPort;
    mParam = NULL;

    setPortName(mESLPort->getName());
    setFlags(flags() | Qt::ItemIsEditable);

    CarbonCfgRTLPort* rtlPort = mESLPort->getRTLPort();
    mEslPortExpr = NULL;

    if (rtlPort)
    {
      setText(colRTL, rtlPort->getName());

      QString value = QString("%1").arg(rtlPort->getWidth());
      setText(colSIZE, value);

      setPortExpr(mESLPort->getExpr());
    
      setExpanded(true);
    }

    setPortType(mESLPort->getPortType());
    setPortMode(mESLPort->getMode());
    setPortTypeDef(mESLPort->getTypeDef());

    setParam(mESLPort);
  }

  static void computeModeAndType(PortEditorTreeWidget* tree, CarbonDB* db, const CarbonDBNode* node, CarbonCfgESLPort* eslPort, CarbonCfgRTLPort* rtlPort)
  {
    if (tree->getEditor()->getEditorMode() == CompWizardPortEditor::eModeCoWare || tree->getEditor()->getEditorMode() == CompWizardPortEditor::eModeSystemC)
    {
      computeMode(db, node, eslPort);
      eslPort->putTypeDef(computeSystemCType(rtlPort));
    }
  }

  static const char* computeSystemCType(CarbonCfgRTLPort* rtlPort)
  {
    int nConnections = rtlPort->numConnections();
    INFO_ASSERT(nConnections == 1, "Port has more than one connection");
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(0);
    CarbonCfgESLPort* eslPort = conn->castESLPort();
    int width = rtlPort->getWidth();

    if (eslPort != NULL)
    {
      if (rtlPort->getType() == eCarbonCfgRTLInout)
      {
        if (width == 1)
          return "sc_logic";
        else
          return "sc_lv";
      }
      else // 2 State
      {
        if (width == 1)
          return "bool";
        else if (width < 65)
          return "sc_uint";
        else
          return "sc_biguint";
      }
    }
    return NULL;
  }


  static void computeMode(CarbonDB* db, const CarbonDBNode* node, CarbonCfgESLPort* eslPort)
  {
    if (carbonDBIsAsyncPosReset(db, node) || carbonDBIsAsyncNegReset(db, node))
    {
      eslPort->putMode(eCarbonCfgESLPortReset);
    }
    else if (carbonDBIsClk(db, node) || carbonDBIsClkTree(db, node))
    {
      eslPort->putMode(eCarbonCfgESLPortClock);
    }
    else
    {
      eslPort->putMode(eCarbonCfgESLPortControl);
    }
  }

  
  class CreatePort : public TreeUndoCommand
  {
  public:
    CreatePort(PortEditorTreeItem* portsItem, const QString& rtlPortName, QUndoCommand* parent = 0);
    
    CarbonCfg* mCfg;
    QString mRTLPortName;
    QString mESLPortName;
    PortTreeIndex mESLPortIndex;

  protected:
    void undo();
    void redo();
  };

  class AddExpression : public TreeUndoCommand
  {
  public:
    AddExpression(ESLPortItem* item, const QString& expr, QUndoCommand* parent = 0);

    CarbonCfg* mCfg;
    bool mInteractive;
    QString mESLPortName;
    QString mExpr;
  
  protected:
    void undo();
    void redo();
  };

  class ConnectParameter : public TreeUndoCommand
  {
  public:
    ConnectParameter(ESLPortItem* item, const QString& paramName, QUndoCommand* parent = 0);

    CarbonCfg* mCfg;
    QString mESLPortName;
    QString mNewParamName;
    QString mOldParamName;
  
  protected:
    void undo();
    void redo();
  };

  class DisconnectParameter : public TreeUndoCommand
  {
  public:
    DisconnectParameter(ESLPortItem* item, const QString& paramName, QUndoCommand* parent = 0);

    CarbonCfg* mCfg;
    QString mESLPortName;
    QString mParameterName;
  
  protected:
    void undo();
    void redo();
  };


  class ChangeDirection : public UndoData
  {
  public:
    ChangeDirection(QTreeWidget* tree, QTreeWidgetItem* item) { setTreeData(tree, item); }
    
    CarbonCfg* mCfg;
    PortTreeIndex mESLPortIndex;
    CarbonCfgESLPortType mOldDirection;
    CarbonCfgESLPortType mNewDirection;

  protected:
    void undo();
    void redo();
  };

  class ChangeDirectionCommand : public MultiUndoCommand
  {
  public:
    ChangeDirectionCommand(QList<ESLPortItem*> items,
      CarbonCfgESLPortType newType, QUndoCommand* parent = 0);
 };
 
  class Tie : public UndoData
  {
  public:
    Tie(QTreeWidget* tree, QTreeWidgetItem* item)
    {
      setTreeData(tree, item);
    }
    CarbonCfg* mCfg;
    QString mESLPortName;
    QString mRTLPortName;
    QString mESLPortExpr;
    QString mTypeDef;
    CarbonCfgESLPortMode mPortMode;

    UInt32 mTieValue;
    PortTreeIndex mESLTieIndex;

    void undo();
    void redo();
  };

  class Delete : public UndoData
  {
  public:
    QString mRTLPortName;
    QString mESLPortName;
    QString mPortExpr;
    QString mParameterName;
    QString mParameterInstance;
    PortTreeIndex mESLPortIndex;
    PortTreeIndex mESLDisconnectIndex;
    CarbonCfgESLPortType mPortType;
    CarbonCfgESLPortMode mPortMode;
    QString mTypeDef;
    CarbonCfg* mCfg;
    QStringList mClockMasters;
    QStringList mClockMasterXtors;

  protected:
    void undo();
    void redo();
  };

  class ResetGen : public UndoData
  {
  public:
    ResetGen(QTreeWidget* tree, QTreeWidgetItem* item) { setTreeData(tree, item); }
    CarbonCfg* mCfg;
    QString mRTLPortName;
    QString mESLPortName;
    QString mESLPortExpr;
    QString mTypeDef;
    CarbonCfgESLPortMode mPortMode;
    PortTreeIndex mESLPortIndex;
    PortTreeIndex mResetGenIndex;
    UInt64 mActiveValue;
    UInt64 mInactiveValue;
    UInt32 mClockCycles;
    UInt32 mCompCycles;
    UInt32 mCyclesBefore;
    UInt32 mCyclesAsserted;
    UInt32 mCyclesAfter;

  protected:
    void undo();
    void redo();
  };

  class ResetGenCommand : public MultiUndoCommand
  {
  public:
    ResetGenCommand(QList<ESLPortItem*> items, 
      UInt64 activeValue, UInt64 inactiveValue,
      UInt32 clockCycles, UInt32 compCycles,
      UInt32 cyclesBefore, UInt32 cyclesAsserted, UInt32 cyclesAfter,
      QUndoCommand* parent = 0);
  };

  class ClockGen : public UndoData
  {
  public:
    ClockGen(QTreeWidget* tree, QTreeWidgetItem* item) { setTreeData(tree, item); }
    CarbonCfg* mCfg;
    QString mRTLPortName;
    QString mESLPortName;
    QString mESLPortExpr;
    PortTreeIndex mESLPortIndex;
    PortTreeIndex mClockGenIndex;
    QString mTypeDef;
    CarbonCfgESLPortMode mPortMode;
    UInt32 mInitialValue;
    UInt32 mDelay;
    UInt32 mDutyCycle;
    UInt32 mClockCycles;
    UInt32 mCompCycles;

  protected:
    void undo();
    void redo();
  };

  class ClockGenCommand : public MultiUndoCommand
  {
  public:
    ClockGenCommand(QList<ESLPortItem*> items, 
       UInt32 initialValue, UInt32 delay, UInt32 dutyCycle,
       UInt32 clockCycles, UInt32 compCycles,
      QUndoCommand* parent = 0);
  };

  class SystemCClockGen : public UndoData
  {
  public:
    SystemCClockGen(QTreeWidget* tree, QTreeWidgetItem* item) { setTreeData(tree, item); }
    CarbonCfg* mCfg;
    QString mRTLPortName;
    QString mESLPortName;
    QString mESLPortExpr;
    PortTreeIndex mESLPortIndex;
    PortTreeIndex mClockGenIndex;
    QString mTypeDef;
    CarbonCfgESLPortMode mPortMode;
    double mPeriod;
    CarbonCfgSystemCTimeUnits mPeriodUnits;
    double mDutyCycle;
    double mStartTime; 
    CarbonCfgSystemCTimeUnits mStartTimeUnits;
    UInt32 mInitialValue;

  protected:
    void undo();
    void redo();
  };


  class SystemCClockGenCommand : public MultiUndoCommand
  {
  public:
    SystemCClockGenCommand(QList<ESLPortItem*> items, 
      double period, CarbonCfgSystemCTimeUnits periodUnits,
      double dutyCycle,
      double startTime, CarbonCfgSystemCTimeUnits startTimeUnits,
      UInt32 initialValue, 
      QUndoCommand* parent = 0);
  };


  class TieCommand : public MultiUndoCommand
  {
  public:
    TieCommand(QList<ESLPortItem*> items, UInt32 value, QUndoCommand* parent = 0);
  };

  ESLPortActions* getActions() 
  {
    if (mActions == NULL)
      mActions = new ESLPortActions(getTree());

    return mActions;
  }

  static ESLPortActions* getActions(PortEditorTreeWidget* tree) 
  {
    if (mActions == NULL)
      mActions = new ESLPortActions(tree);

    return mActions;
  }
private slots:
  void actionConnectParameter();

private:
  static ESLPortActions* mActions;
  CarbonCfgESLPort* mESLPort;
  ESLPortExprItem* mEslPortExpr;
  ESLPortParameter* mParam;
  UtString mEslPortTypeDef;
  CarbonCfgESLPortMode mPortMode;
  QString mTypeDef;

};


#endif
