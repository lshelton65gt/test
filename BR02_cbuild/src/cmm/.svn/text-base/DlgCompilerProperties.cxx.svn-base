// -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "DlgCompilerProperties.h"
#include "CarbonMakerContext.h"
#include "CarbonOptions.h"

DlgCompilerProperties::DlgCompilerProperties(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);
}

DlgCompilerProperties::~DlgCompilerProperties()
{

}

void DlgCompilerProperties::putContext(CarbonMakerContext* ctx)
{
  mContext = ctx;

  ui.widgetSettings->putContext(mContext);
  ui.comboBoxConfigs->clear();

  CarbonProject* proj = mContext->getCarbonProjectWidget()->project();
  CarbonConfigurations* configs = proj->getConfigurations();
  const char* activeConfigName = proj->getActiveConfiguration();

  for (UInt32 i=0; i<configs->numConfigurations(); ++i)
  {
    UtString name;
    CarbonConfiguration* config = configs->getConfiguration(i);
    if (0 == strcmp(config->getName(), activeConfigName))
      name << "Active(" << config->getName() << ")";
    else
      name = config->getName();

    ui.comboBoxConfigs->addItem(name.c_str(), config->getName());
  }

  ui.comboBoxConfigs->setCurrentIndex(ui.comboBoxConfigs->findData(activeConfigName));

  QStringList items;
  QVariantList itemValues;

  mContext->getCarbonProjectWidget()->project()->getToolOptions("VSPCompiler")->setItems("Compiler Properties", items, itemValues);

  ui.widgetSettings->setSettings(mContext->getCarbonProjectWidget()->project()->getToolOptions("VSPCompiler"));
}


void DlgCompilerProperties::on_comboBoxConfigs_currentIndexChanged(int newIndex)
{
  QVariant v = ui.comboBoxConfigs->itemData(newIndex);
  UtString configName;
  configName << v.toString();

  CarbonProject* proj = mContext->getCarbonProjectWidget()->project();
  CarbonConfigurations* configs = proj->getConfigurations();
  for (UInt32 i=0; i<configs->numConfigurations(); ++i)
  {
    CarbonConfiguration* config = configs->getConfiguration(i);
    if (0 == strcmp(config->getName(), configName.c_str()))
    {
      QStringList items;
      QVariantList itemValues;

      config->getOptions("VSPCompiler")->setItems("Compiler Properties", items, itemValues);
      ui.widgetSettings->setSettings(config->getOptions("VSPCompiler"));
      break;
    }
  }
}

void DlgCompilerProperties::on_buttonBox_rejected()
{
  reject();
}

void DlgCompilerProperties::on_buttonBox_accepted()
{
  accept();
}
