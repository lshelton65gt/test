#ifndef MVNETTREE_H
#define MVNETTREE_H

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include "util/UtString.h"
#include "util/OSWrapper.h"

#include "CarbonMakerContext.h"
#include "CarbonDatabaseContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonProject.h"

#include <QTreeWidget>
#include <QXmlStreamWriter>

#include "ui_MVNetTree.h"

class MVNetTree : public QTreeWidget
{
  Q_OBJECT

public:
  MVNetTree(QWidget *parent = 0);
  ~MVNetTree();
  void populate(CarbonProject* proj, const QString& xmlFileName);
  bool writeXmlFile(QXmlStreamWriter& sw);
  void expandAll();
  void collapseAll();
  void clearAll();

private:
  enum StateFlags { UnknownChecked, AllChecked, NoneChecked, SomeChecked };

  enum PortFlags { FolderItem=0x1, PrimaryPort=0x2, NormalPort=0x4 };

  bool isPrimaryPort(const CarbonDB* db, const CarbonDBNode* node);
  QTreeWidgetItem* createHierarchicalPath(CarbonDB* db, const CarbonDBNode* node, Qt::CheckState initialState, Qt::CheckState initialFolderState);
  QString appendFlag(const QString& flags, const QString& value);
  void countCheckboxes(QTreeWidgetItem* parent, int* numItems, int* numChecked, int *numUnchecked);
  void saveValues(QXmlStreamWriter& sw, QTreeWidgetItem* parent, bool writeUnchecked);
  Qt::CheckState readXmlFile(const QString& xmlFileName, bool initialStateInfo = false);
  void changeItemAndChildren(QTreeWidgetItem* item, Qt::CheckState newState);
  bool hasFlags(QTreeWidgetItem*, PortFlags);
  void setFlags(QTreeWidgetItem* item, PortFlags flags);
  bool allSiblings(QTreeWidgetItem* item, Qt::CheckState state);
  StateFlags updateFolderCheckboxes(QTreeWidgetItem* item);
  void updateParentCheckboxes(QTreeWidgetItem* item);
  bool allChildrenChecked(QTreeWidgetItem* item);
  void expandChildren(QTreeWidgetItem* item);
  void collapseChildren(QTreeWidgetItem* item);

private slots:
  void itemChanged(QTreeWidgetItem*, int);

signals:
  void dataChanged();

private:
  Ui::MVNetTreeClass ui;
  QMap<QString, QTreeWidgetItem*> mModuleMap;
  CarbonDB* mDB;
};

#endif // MVNETTREE_H
