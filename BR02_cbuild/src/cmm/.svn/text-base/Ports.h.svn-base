#ifndef __PORTS_H__
#define __PORTS_H__

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include "Scripting.h"

class Ports;
class Mode;
class Template;
class Block;

// File: Ports, Port
// The Ports collection and the Port object
//
// Class: Port
//
class Port : public QObject, protected QScriptable
{
  Q_OBJECT  
  // Property: name
  // The name of this port as described in the *Port* or *OptionalPort*
  // tags.
  //
  // This name will also be used to create an automatic property with the same
  // name (with all embedded spaces stripped) and may be accessed from
  // the <Ports> collection as a property name directly.
  //
  // Access:
  // *ReadOnly*
  //
  Q_PROPERTY(QString name READ getName)

  // Property: format
  // String format string that represents this ports underlying model name
  // may be of the form $(name) and contain optional ^(instanceID)
  // references which will get replaced after elaboration.
  Q_PROPERTY(QString format READ getFormat WRITE setFormat)
  Q_PROPERTY(QString onCondition READ getOnCondition WRITE setOnCondition)
  
  // Property: description
  // String verbose description describing this ports function.
  Q_PROPERTY(QString description READ getDescription WRITE setDescription)

  // Property: tieValue
  // String tieValue for tied-off user-defined output ports
  Q_PROPERTY(QString tieValue READ getTieValue)

  // Property: width
  // Integer width for user-defined ports.  Returns -1 if not defined.
  Q_PROPERTY(int width READ getWidth WRITE setWidth)

  // Property: enabled
  // Boolean indicates whether or not this port was enabled after elaboration
  // and all <OptionalPort>s have been resolved.   
  //
  // Default Value:
  //   true
  Q_PROPERTY(bool enabled READ isEnabled)
  

  // Property: location
  // Enumerated value indicates graphical port location  
  //
  // Default Value:
  //   Port.Left
  Q_PROPERTY(PortLocation location READ getLocation WRITE setLocation)
  Q_ENUMS(PortLocation)

  // Property: direction
  // Enumerated value indicates port direction
  // One of: Port.Input, Port.Output, Port.InOut
  //
  // Default Value:
  //   Port.Input
  Q_PROPERTY(PortDirection direction READ getDirection WRITE setDirection)
  Q_ENUMS(PortDirection)

public:
  enum PortLocation { GraphicsLeft, GraphicsRight, GraphicsTop, GraphicsBottom };
  enum PortDirection { Input, Output, InOut };

  static PortDirection directionFromString(const QString&);
  static PortLocation locationFromString(const QString&);

  virtual Port* copyPort();
  Port(bool userDefined = false);

// overridable
  virtual bool isEnabled() { return true; }

  bool isUserDefined() const { return mUserDefined; }

  int getWidth() const { return mWidth; }
  void setWidth(int newVal) { mWidth=newVal; }

  QString getTieValue() const { return mTieValue; }
  void setTieValue(const QString& newVal) { mTieValue=newVal; }

  QString getDescription() const { return mDescription; }
  void setDescription(const QString& newVal) { mDescription=newVal; }

  QString getName() const { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

  QString getFormat() const { return mFormat; }
  void setFormat(const QString& newVal) { mFormat=newVal; }

  QString getOnCondition() const { return mOnCondition; }
  void setOnCondition(const QString& newVal);

  void setBaseName(const QString& newVal) { mBaseName=newVal; }
  QString getBaseName() const { return mBaseName; }

  PortLocation getLocation() const { return mLocation; }
  void setLocation(PortLocation newVal) { mLocation=newVal; }

  PortDirection getDirection() const { return mDirection; }
  void setDirection(PortDirection newVal) { mDirection=newVal; }

  static bool deserialize(Mode* mode, Template* templ, Block* block, Ports* port, const QDomElement& e, XmlErrorHandler* eh);
  static bool parseBaseAttributes(Port* p, const QDomElement& element, XmlErrorHandler* eh);

protected:
  Port* copyPortData(Port* p);

private:
  void expandPort(QScriptContext *context, QScriptEngine *env);

public slots:
  bool hasOnCondition() { return mOnCondition.length() > 0; }
  bool evaluateOnCondition(QScriptValue v);

private:
  bool mUserDefined;
  QString mName;
  QString mFormat;
  QString mOnCondition;
  QString mDescription;
  QString mBaseName;
  QString mTieValue;
  int mWidth;
  PortLocation mLocation;
  PortDirection mDirection;
};

// Class: Ports
//
class Ports : public QObject, protected QScriptable
{
  Q_OBJECT

  // Property: count
  //
  // Access:
  // *ReadOnly*
  //  
  Q_PROPERTY(int count READ getCount)
  Q_ENUMS(PortLocation)

public:
  Ports() {}
  enum PortLocation { GraphicsLeft, GraphicsRight, GraphicsTop, GraphicsBottom };

  bool deserialize(Mode* mode, Template* templ, Block* block, const QDomElement& e, XmlErrorHandler* eh);
  void addPort(Port* p);
  int getCount() const { return mPorts.count(); }
  void removeUserDefined();
  void removePort(Port* port)
  {
    mPorts.removeAt(mPorts.indexOf(port));
  }

  Port* getAt(int i)
  {
    return mPorts[i];
  }
  Port* findPort(const QString& name)
  {
    foreach (Port* port, mPorts)
    {
      if (port->getName() == name)
        return port;
    }
    return NULL;
  }

public slots:
// Method: Port addPort(name, location, format)
//
// Add a new Port to the collection.  This may be done only at 
// initialization or during the *definePorts* script callback.
//
// Parameters: 
//  name - String name for this new port
//  location - <PortLocation> indicating the graphics position of this port
//  format - String format (see <Port> format for details)
//
// Returns:
// 
//   <Port> newly created port
//
  QObject* addPort(const QString& portName, PortLocation location, const QString& format, bool userPort=true);

// Method: Port getPort(name)
// Locate and return the port matching *name*
// 
// Parameters: 
//  name - String name for this new port
//
// Returns:
// 
//   <Port> object or null if not found
//
  QObject* getPort(const QString& name)
  {
    foreach (Port* port, mPorts)
    {
      if (port->getName() == name)
        return port;
    }
    return NULL;
  }
// Method: Port getPort(index)
// Returns the Port indexed by *index* within the 0-based collection.
// 
// Parameters: 
//  index - Integer from 0 to *count*-1
//
// Returns:
//   <Port> object or null if not found
//
  QObject* getPort(int i)
  {
    return mPorts[i];
  }

private:
  QList<Port*> mPorts;

};

#include "OptionalPort.h"

#endif
