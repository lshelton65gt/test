//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "DlgCodeEditor.h"


DlgCodeEditor::DlgCodeEditor(QWidget *parent, const QString& code)
: QDialog(parent)
{
  ui.setupUi(this);

  ui.editor->setEolMode(QsciScintilla::EolUnix);
  ui.editor->convertEols(QsciScintilla::EolUnix);


  QsciCommandSet* cset = ui.editor->standardCommands();
  QList<QsciCommand *> cmds = cset->commands();
  foreach (QsciCommand* cmd, cmds)
  {
    QKeySequence ks(cmd->key(), cmd->alternateKey());
    qDebug() << cmd->description() << cmd->key() << cmd->alternateKey() << ks;
  }

  //Shift+Tab is broken in Qscintilla, this hack fixes it
  //and enables Shift -> Right Arrow and Shift -> Left Arrow to indent/unindent
  //
  int key1 = QsciScintillaBase::SCK_LEFT + (QsciScintillaBase::SCMOD_SHIFT << 16);
  ui.editor->SendScintilla(QsciScintillaBase::SCI_ASSIGNCMDKEY, key1, QsciScintillaBase::SCI_BACKTAB);
  
  int key2 = QsciScintillaBase::SCK_RIGHT + (QsciScintillaBase::SCMOD_SHIFT << 16);
  ui.editor->SendScintilla(QsciScintillaBase::SCI_ASSIGNCMDKEY, key2, QsciScintillaBase::SCI_TAB);

  ui.editor->setLexer(new QsciLexerCPP());
  
  ui.editor->setText(code);
}

DlgCodeEditor::~DlgCodeEditor()
{
}

void DlgCodeEditor::on_buttonBox_rejected()
{
  reject();
}

void DlgCodeEditor::on_buttonBox_accepted()
{
  accept();
}
