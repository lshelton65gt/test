#include "util/CarbonPlatform.h"
#include <QtGui>
#include <QSettings>

#include "DlgAdvancedMatching.h"
#include "DlgAddMaxsimXtor.h"

#include "carbon/carbon_dbapi.h"
#include "cfg/CarbonCfg.h"
#include "CcfgHelper.h"

#define CARBON_DESIGN "Carbon Design Systems"
#define REGEX_SETTINGS "Regular Expression Matching"

DlgAdvancedMatching::DlgAdvancedMatching(DlgAddMaxsimXtor* parentDlg, QWidget *parent)
: QDialog(parent)
{
  mParentDlg = parentDlg;

  ui.setupUi(this);
  ui.comboBoxPorts->setEnabled(true);
  ui.pushButtonApply->setEnabled(true);

  ui.comboBoxPorts->blockSignals(true);
  foreach(QString name, mParentDlg->mRTLPortMap.keys())
  {
    ui.comboBoxPorts->addItem(name);
  }
  QSortFilterProxyModel* proxy = new QSortFilterProxyModel(ui.comboBoxPorts); 
  proxy->setSourceModel(ui.comboBoxPorts->model());                            // <-- 
  ui.comboBoxPorts->model()->setParent(proxy);                                 // <--
  ui.comboBoxPorts->setModel(proxy);                                           // <-- 
  ui.comboBoxPorts->model()->sort(0); // "A","B","C"
  ui.comboBoxPorts->blockSignals(false);

  ui.comboBoxRegex->blockSignals(true);
  QSettings settings(CARBON_DESIGN, REGEX_SETTINGS);

 
  int numItems = settings.value("regex_items", 0).toInt();
  for (int i=0; i<numItems; i++)
  {
    QString key = QString("regex_item_%1").arg(i);
    QString value = settings.value(key, "").toString();
    if (!value.isEmpty())
      ui.comboBoxRegex->addItem(value);   
  }

  QString current = settings.value("regex_current", "").toString();
  ui.comboBoxRegex->setEditText(current);


  ui.comboBoxRegex->blockSignals(false);
}

DlgAdvancedMatching::~DlgAdvancedMatching()
{
}

QString DlgAdvancedMatching::ComputeMatch(const QString& input)
{
  QString testString = input;
  QString pattern = ui.comboBoxRegex->currentText();

  QRegExp re(pattern);
  if (re.isValid())
  {
    int pos = re.indexIn(testString);
    if (pos == -1)
    {    
      ui.labelStatus->setText("No match");
      return "";
    }
    else
      ui.labelStatus->setText("");


    int count = re.captureCount();
    if (count >= 1)
    {       
      for (int row=0; row<count; row++)
      {        
        QTableWidgetItem* c = new QTableWidgetItem(re.cap(row+1));
        ui.tableWidget->setItem(row, 1, c);         
      }     
    }
  }

  QStringList prefixes;
  QStringList suffixes;
  int rows = ui.tableWidget->rowCount();

  for (int row=0; row<rows; row++)
  {
    QTableWidgetItem* item = ui.tableWidget->item(row, 1);
    QString token = item->text();
    QComboBox* cbox = (QComboBox*)ui.tableWidget->cellWidget(row, 2);
    if (cbox)
    {
      QVariant qv = cbox->itemData(cbox->currentIndex());
      int v = qv.toInt();
      MatchAction ma = (MatchAction)v;

      switch(ma)
      {
      case Prefix:
        prefixes << token;
        break;
      case Suffix:
        suffixes << token;
        break;
      case Remove:
        break;
      }   
    }
  }

  QString prefix = prefixes.join("");
  QString suffix = suffixes.join("");

  QString result = QString("%1%2").arg(prefix).arg(suffix);

  return result;
}


void DlgAdvancedMatching::currentIndexChanged(int)
{
  ui.lineEditResultString->setText(ComputeMatch(ui.comboBoxPorts->currentText()));
}

void DlgAdvancedMatching::on_pushButtonApply_clicked()
{
  QString pattern = ui.comboBoxRegex->currentText();
  QString testString = ui.comboBoxPorts->currentText();

  ui.tableWidget->clearContents();
  ui.tableWidget->setRowCount(0);

  QRegExp re(pattern);
  if (re.isValid())
  {
    ui.pushButtonApply->setEnabled(false);
    //ui.comboBoxPorts->setEnabled(true);
    int pos = re.indexIn(testString);    
    int count = re.captureCount();
    if (count >= 1)
    {
      if (pos == -1)
        ui.labelStatus->setText("No match");
      else
        ui.labelStatus->setText("");

      ui.labelStatus->setText("");
      ui.tableWidget->setRowCount(count);
      for (int row=0; row<count; row++)
      {
        QString colname = QString("%1").arg(row);
        QTableWidgetItem *newItem = new QTableWidgetItem(colname);
        ui.tableWidget->setItem(row, 0, newItem);
        QTableWidgetItem* c = new QTableWidgetItem(re.cap(row+1));
        ui.tableWidget->setItem(row, 1, c);
        QString cn = re.cap(row+1);

        QComboBox* cbox = new QComboBox(ui.tableWidget);

        cbox->addItem("Prefix", (int)Prefix);
        cbox->addItem("Suffix", (int)Suffix);
        cbox->addItem("Remove", (int)Remove);

        ui.tableWidget->setCellWidget(row, 2, cbox);
        CQT_CONNECT(cbox, currentIndexChanged(int), this, currentIndexChanged(int));
      }
      currentIndexChanged(0);   
    }
    else
      ui.labelStatus->setText("No match");
  }
  else
  {
    //ui.comboBoxPorts->setEnabled(false);
    QMessageBox::warning(this, "Regular Expression Error", re.errorString());
  }
}

void DlgAdvancedMatching::on_buttonBox_rejected()
{
  reject();
}

void DlgAdvancedMatching::on_comboBoxRegex_textChanged(const QString &)
{
  ui.pushButtonApply->setEnabled(true);
}


void DlgAdvancedMatching::on_comboBoxPorts_textChanged(const QString& text)
{
  ui.lineEditResultString->setText(ComputeMatch(text));
}

void DlgAdvancedMatching::on_buttonBox_accepted()
{
  ui.comboBoxRegex->blockSignals(true);
  QSettings settings(CARBON_DESIGN, REGEX_SETTINGS);

  QString current = ui.comboBoxRegex->currentText();

  settings.setValue("regex_current", current);

  bool foundItem = false;
  for (int j=0; j<ui.comboBoxRegex->count(); j++)
  {
    QString v = ui.comboBoxRegex->itemText(j);
    if (v == current)
    {
      foundItem = true;
      break;
    }
  }

  if (!foundItem)
    ui.comboBoxRegex->addItem(current);

  int i = 0;
  for (i=0; i<ui.comboBoxRegex->count(); i++)
  {
    QString value = ui.comboBoxRegex->itemText(i);
    QString key = QString("regex_item_%1").arg(i);
    settings.setValue(key, value);
  }

  settings.setValue("regex_items", i);
  ui.comboBoxRegex->blockSignals(false);

  accept();
}


