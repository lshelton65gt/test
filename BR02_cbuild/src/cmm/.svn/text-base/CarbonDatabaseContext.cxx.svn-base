//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "CarbonDatabaseContext.h"
#include <QDebug>

// things from gui
#include "GenProp.h"
#include "ApplicationContext.h"

#include "iodb/CGraph.h"
#include "../shell/CarbonDatabase.h"
#include "cfg/CarbonCfg.h"

CarbonDatabaseContext::~CarbonDatabaseContext()
{
  unloadDatabase();
}
CarbonDatabaseContext::CarbonDatabaseContext(CQtContext* cqt)
{
  mCQt = cqt;
  mCfg = carbonCfgCreate();
  mAtomicCache = NULL;
  mDB = NULL;
  //mCGraph = NULL;

}

void CarbonDatabaseContext::unloadDatabase()
{
 
}

// Load up a Carbon Database
CarbonDB* CarbonDatabaseContext::loadDatabase(CQtContext* cqt, const char* iodbName)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);
 
  mCQt = cqt;
  
  if (mAtomicCache)
    delete mAtomicCache;

  mAtomicCache = new AtomicCache;

  // clear old DB
  mDB = 0;
  CarbonDB *oldDB = mCfg->getDB();
  if (oldDB) {
    qDebug() << "freeing db" << oldDB;
    mCfg->putDB(0);
    carbonDBFree(oldDB);
  }

  // load new DB
  CarbonDatabaseStandalone* newDB = new CarbonDatabaseStandalone;
  if (!newDB->openDBFile(iodbName, &mLocatorFactory))
  {
    QApplication::restoreOverrideCursor();
    return false;
  }

  qDebug() << "database" << newDB;

  mDB = newDB;
  if (mCfg)
    mCfg->putDB(mDB);  // Ownership transferred to CarbonCfg

  UtString baseFile, errmsg;
  parseFileName(iodbName, &baseFile);

  errmsg.clear();
  UtString libFile(baseFile);
#ifdef Q_OS_WIN
    libFile += ".lib";
#else
    libFile += ".a";
#endif

  if (mCfg)
  {
    carbonCfgPutLibName(mCfg, libFile.c_str());
    carbonCfgPutIODBFile(mCfg, iodbName);
    carbonCfgPutCompName(mCfg, carbonDBGetTopLevelModuleName(mDB));
    carbonCfgPutTopModuleName(mCfg, carbonDBGetTopLevelModuleName(mDB));
  }

  QApplication::restoreOverrideCursor();
  
  return mDB;
} 

CarbonDatabaseContext::ParseResult CarbonDatabaseContext::parseFileName(const char* filename,
                                                  UtString* baseName)
{
  *baseName = filename;
  UInt32 sz = strlen(filename);

  ParseResult parseResult = eNoMatch;
  baseName->clear();

  if ((sz > 6) && (strcmp(filename + sz - 6, ".io.db") == 0)) {
    parseResult = eIODB;
    baseName->append(filename, sz - 6);
  }
  if ((sz > 7) && (strcmp(filename + sz - 7, ".gui.db") == 0)) {
    parseResult = eIODB;
    baseName->append(filename, sz - 7);
  }
  else if ((sz > 10) && (strcmp(filename + sz - 10, ".symtab.db") == 0)) {
    parseResult = eFullDB;
    baseName->append(filename, sz - 10);
  }
  else if ((sz > 5) && (strcmp(filename + sz - 5, ".ccfg") == 0)) {
    parseResult = eCcfg;
    baseName->append(filename, sz - 5);
  }
  return parseResult;
}

