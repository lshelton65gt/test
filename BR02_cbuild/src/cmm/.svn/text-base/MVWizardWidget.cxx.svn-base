//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "MVWizardWidget.h"
#include "Mdi.h"
#include "MdiTextEditor.h"

#include "CodeGenerator.h"
#include "CarbonMakerContext.h"
#include "MVComponent.h"
#include "CarbonProjectWidget.h"
#include "CarbonDatabaseContext.h"

#include "gui/CQt.h"

#include "util/UtString.h"
#include "util/UtHashMap.h"
#include "util/AtomicCache.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/ArgProc.h"
#include "util/AtomicCache.h"
#include "util/OSWrapper.h"
#include "util/RandomValGen.h"
#include "shell/CarbonAbstractRegister.h"
#include "cfg/carbon_cfg.h"
#include "cfg/carbon_cfg_misc.h"
#include "cfg/CarbonCfg.h"

#include "shell/carbon_misc.h"
#include "carbon/carbon_capi.h"
#include "CarbonOptions.h"
#include "car2cow.h"


const char* MVWizardWidget::userFriendlyName()
{
  mFriendlyName.clear();
  OSConstructFilePath(&mFriendlyName,mCtx->getCarbonProjectWidget()->project()->getProjectDirectory(), "Shadow Hierarchy");
  return mFriendlyName.c_str();
}


void MVWizardWidget::on_pushButtonExpandAll_clicked()
{
  ui.treeWidget->expandAll();
}

void MVWizardWidget::on_pushButtonCollapseAll_clicked()
{
  ui.treeWidget->collapseAll();
}


MVWizardWidget::MVWizardWidget(CarbonMakerContext* ctx, MDIDocumentTemplate* t, QWidget *parent) : QWidget(parent)
{
  ui.setupUi(this);

  initializeMDI(this, t);
  mCtx = ctx;

  isUntitled = true;

  setAttribute(Qt::WA_DeleteOnClose);

  setWindowTitle("MV Component[*]");
  initialize();

  CQT_CONNECT(mCtx->getCarbonProjectWidget()->getConsole(), compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType));
  CQT_CONNECT(mCtx->getCarbonProjectWidget()->getConsole(), compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType));
  
  CQT_CONNECT(mCtx->getCarbonProjectWidget()->project(), projectLoaded(const char*, CarbonProject*), this, projectLoaded(const char*, CarbonProject*));
  CQT_CONNECT(mCtx->getCarbonProjectWidget()->project(), makefileCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, makefileCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType));

  CQT_CONNECT(ui.treeWidget, dataChanged(), this, dataChanged());

  CarbonOptions* options = mCtx->getCarbonProjectWidget()->project()->getToolOptions("VSPCompiler");
  options->registerPropertyChanged("-o", "outputFilenameChanged", this);
}


void MVWizardWidget::dataChanged()
{
  setWindowModified(true);
  setWidgetModified(true);
}

void MVWizardWidget::compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  saveDocument();
  setEnabled(false);

}

void MVWizardWidget::makefileCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  populate();
}

void MVWizardWidget::compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  setEnabled(true);
}

MVWizardWidget::~MVWizardWidget()
{
}

void MVWizardWidget::fileSave()
{
  saveDocument();
}

const char* MVWizardWidget::strippedName(const char* fullFileName)
{
  static UtString x;
  x.clear();
  x << QFileInfo(fullFileName).fileName();
  return x.c_str();
}

const char* MVWizardWidget::userFriendlyCurrentFile()
{
  return strippedName(mCurFile.c_str());
}

void MVWizardWidget::setCurrentFile(const char* fileName)
{
  QString cf =  QFileInfo(fileName).absoluteFilePath();
  mCurFile.clear();
  mCurFile << cf;
  isUntitled = false;
  setWindowModified(false);
  setWidgetModified(false);
  UtString title;
  title << userFriendlyCurrentFile() << "[*]";

  setWindowTitle(title.c_str());
}

void MVWizardWidget::saveDocument()
{
  if (isWindowModified())
  {
    MVComponent* mvComp = mCtx->getCarbonProjectWidget()->getActiveMVComponent();

    QString xmlFileName = mvComp->getXMLFilename();
    QFile xmlFile(xmlFileName);

    if (xmlFile.open(QIODevice::WriteOnly))
    {
      QXmlStreamWriter xw(&xmlFile);
      ui.treeWidget->writeXmlFile(xw);
      xmlFile.close();
    }

    QString fileName = mCurFile.c_str();
    setWindowModified(false);
    setWidgetModified(false);

    qDebug() << "Save MV: " << xmlFileName;
  }
  else
    qDebug() << "nothing to save in MV document";
}

void MVWizardWidget::updateMenusAndToolbars()
{
}

bool MVWizardWidget::loadFile(const QString& /*fileName*/)
{
  mCurFile.clear();

  CarbonDatabaseContext* dbContext = mCtx->getCarbonProjectWidget()->project()->getDbContext();
  if (dbContext && dbContext->getDB())
  {
    MVComponent* MV = mCtx->getCarbonProjectWidget()->getActiveMVComponent();
    if (MV)
      populate();
    else
      return false;
  }
  
  return true;
}

bool MVWizardWidget::isPrimary(const CarbonDB* db, const CarbonDBNode* node)
{
  return carbonDBIsPrimaryInput(db, node) ||
    carbonDBIsPrimaryOutput(db, node) ||
    carbonDBIsPrimaryBidi(db, node);
}
void MVWizardWidget::populate()
{
  MVComponent* mvComp = mCtx->getCarbonProjectWidget()->getActiveMVComponent();
  if (mvComp)
  {
    QString xmlFileName = mvComp->getXMLFilename();

    ui.treeWidget->populate(mCtx->getCarbonProjectWidget()->project(), xmlFileName);
  }
  else
    ui.treeWidget->clearAll();
}
void MVWizardWidget::projectLoaded(const char*, CarbonProject*)
{
}

void MVWizardWidget::outputFilenameChanged(const CarbonProperty*, const char*)
{
  setWindowModified(true);
  setWidgetModified(true);
}


bool MVWizardWidget::maybeSave()
{
  if (isWindowModified())
  {
    QMessageBox::StandardButton ret;
    ret = QMessageBox::warning(this, MODELSTUDIO_TITLE,
      tr("'%1' has been modified.\n"
      "Do you want to save your changes?")
      .arg(userFriendlyName()),
      QMessageBox::Save | QMessageBox::Discard
      | QMessageBox::Cancel);
    if (ret == QMessageBox::Save)
    {
      saveDocument();
      return true;
    }
    else if (ret == QMessageBox::Discard && isWidgetModified())
    {
      setWindowModified(false);
      setWidgetModified(false);
    }
    else if (ret == QMessageBox::Cancel)
      return false;
  }
  return true;
}
void MVWizardWidget::closeEvent(QCloseEvent *event)
{
  if (maybeSave())
  {
    qDebug() << "Closing MV Wizard";
    MDIWidget::closeEvent(this);
    event->accept();
  }
  else
  {
    event->ignore();
  }
}

void MVWizardWidget::initialize()
{
 
}
