#ifndef __SSHCONSOLE_H__
#define __SSHCONSOLE_H__

#include "util/CarbonPlatform.h"

#include <QtGui>
#include <QProcess>

class CarbonProject;

class SSHConsole : public QObject
{
  Q_OBJECT

public:

  enum LoginState {Unknown=0, Started, LoggingIn, Authorizing, RetryPassword, FailedLogin, 
          ShellStarting, ShellStarted, RemoteDirCheck, RemoteVersionCheck, LoggedIn, LoggedOut, Terminated};

  SSHConsole(QWidget* parent = 0);
  virtual ~SSHConsole();
  
  void setShuttingDown() { mShutdown = true; }
  void executeCommand(CarbonProject* proj, const QString& workingDir, QStringList& envVars, const QString& command, const QStringList& args, bool hidden=false);
  void putProcessWorkingDirectory(const QString& newVal) { mProcessWorkingDirectory = newVal; }
  void putPassword(const QString& newVal)
  {
    mUsePassword = true;  
    mPassword = newVal; 
  }
  void clearPassword()
  {
    mUsePassword = false;
    mPassword = "";
  }
  void startSession(CarbonProject* proj, const QString& serverName, const QString& userName);
  void logoutSession()
  {
    if (isSessionConnected())
    {
      writeCommand("logout");
      setState(LoggedOut);
    }
  }
  void abortSession()
  {
    if (!mShutdown)
    {
      mProgramRunning = false;
      mExitCode = 1;
      if (mProcess->state() == QProcess::Running)
        mProcess->terminate();
      // Kill must be used instead of terminate on windows
      // because plink doesn't have a window, it's a console app and terminate()
      // does nothing
      mProcess->kill();
    }
  }

  bool isProgramRunning()
  {
    return isSessionConnected() && mProgramRunning;
  }

  bool isSessionConnected()
  {
    return mProcess && mLoginState == LoggedIn;
  }

private slots:
  void procStarted();
  void procFinished(int exitCode);
  void procError(QProcess::ProcessError err);
  void procReadStderr();
  void procReadStdout();

signals:
  void processReadyForCommands();
  void processOutput(const QString&);
  void processTerminated(int);
  void sessionStateChanged(SSHConsole::LoginState);
  void sessionLoginStatus(const QString&);
  void commandCompleted(int);

private:
  bool useProject() { return mProject != NULL; }
  bool processLine(const QString& line, bool isStandardError = false);
  bool processOutputLine(const QString& line);
  void scanSessionInitialize(const QString& line, bool isStandardError);
  void buildCommandLine(const QString& workingDir, QTextStream& cmdLine);
  void addEnvVar(const QString& name, const QString& value);
  void addCarbonEnvironmentVariables();
  void startShell();
  void writeCommand(const QString& cmd);
  void writeHiddenCommand(const QString& cmd);
  void setState(LoginState newState);
  void sendVersionCheckCommand();
  void checkWorkingDirectory();

  LoginState getState() { return mLoginState; }

private:
  QString mProcessWorkingDirectory;
  QString mPassword;
  bool mUsePassword;
  LoginState mLoginState;
  QString mProgram;
  QStringList mArgs;
  CarbonProject* mProject;
  QProcess* mProcess;
  int mExitCode;
  QString mUsername;
  QString mServer;
  QWidget* mParent;
  QString mCommandProgram;
  QStringList mCommandArgs;
  QString mVersionCheckLine;
  bool mProgramRunning;
  bool mShutdown;
  QString mMagicFileName;
};


#endif
