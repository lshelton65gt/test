//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"

#include "gui/CQt.h"
#include "util/UtString.h"

#include "CarbonProjectWidget.h"
#include "ShellConsole.h"

#if pfWINDOWS
#pragma warning( disable : 4996) 
#endif

ShellConsole::ShellConsole(QWidget* parent)
{
  mUsePassword = false;
  mProject = NULL;
  mParent = parent;
  mExitCode = -1;
  mProcess = NULL;
  mAborting = false;
}

ShellConsole::~ShellConsole()
{  
  QProcess* proc = mProcess;
 
  abortSession();

  if (proc)
    delete proc;
}

// Return the PID, but only if it is running
int ShellConsole::getPid()
{
  if (mProcess && mProcess->state() == QProcess::Running)
    return (int)mProcess->pid();
  else
    return 0;
}
// Handle errors on startup
void ShellConsole::procError(QProcess::ProcessError err)
{
  const char* emsg = "Unknown error";
  switch (err) 
  {
  case QProcess::FailedToStart: emsg = "FailedToStart"; break;
  case QProcess::Crashed: emsg = "Terminated"; break;
  case QProcess::Timedout: emsg = "Timedout"; break;
  case QProcess::WriteError: emsg = "WriteError"; break;
  case QProcess::ReadError: emsg = "ReadError"; break;
  case QProcess::UnknownError: emsg = "UnknownError"; break;
  }

  UtString errMsg;
  errMsg << "Unable to launch: " << mProgram << "\n" << emsg;

  if (err != QProcess::Crashed) {
    QMessageBox::critical(NULL, MODELSTUDIO_TITLE, errMsg.c_str());
    emit processTerminated(mExitCode);
  }
}

// Process has started, signal that to the caller
void ShellConsole::procStarted()
{
  qDebug() << "ShellConsole Process started PID=" << mProcess->pid();
  setState(Started);
}

void ShellConsole::abortSession()
{
  mAborting = true;

  if (mProcess)
  {
    mProcess->kill();
    mProcess->waitForFinished();
  }
}

void ShellConsole::procFinished(int exitCode)
{
  if (mExitCode == -1)
    mExitCode = exitCode;

  emit commandCompleted(exitCode);

  qDebug() << "ShellConsole Process Exited ExitCode=" << mExitCode;

  setState(Terminated);

  emit processTerminated(exitCode);

  mProcess = NULL;
}

void ShellConsole::setState(LoginState newState)
{
  bool broadCastChange = false;
  if (newState != mLoginState)
    broadCastChange = true;

  mLoginState = newState;

  if (broadCastChange)
    emit sessionStateChanged(newState);
}

// Read from Standard Output
void ShellConsole::procReadStdout()
{
  if (mProcess == NULL)
    return;

  QString textRead = QString::fromLocal8Bit(mProcess->readAllStandardOutput());

  QStringList linesRead = textRead.split('\n');
  foreach (QString line, linesRead)
  {
    QString asciiLine = line.trimmed();
    UtString theLine;
    theLine << asciiLine;
    qDebug() << "stdout: " << theLine.c_str();
    if (!processLine(asciiLine))
      break;
  }
}

// Read from Standard Error
void ShellConsole::procReadStderr()
{
  if (mProcess == NULL)
    return;

  QString textRead = QString::fromLocal8Bit(mProcess->readAllStandardError());
  QStringList linesRead = textRead.split('\n');
  foreach (QString line, linesRead)
  {
    QString asciiLine = line.trimmed();
    UtString theLine;
    theLine << asciiLine;
    qDebug() << "stderr: " << theLine.c_str();
    if (!processLine(asciiLine))
      break;
  }
}


// if we are not yet logged in, then special handling
bool ShellConsole::processLine(const QString& line)
{
  return processOutputLine(line);
}

// This is called only after we are logged in
bool ShellConsole::processOutputLine(const QString& line)
{
  if (mAborting)
    return false;

  if (line.length() > 0)
    emit processOutput(line);
  
  return true;
}

void ShellConsole::addEnvVar(QStringList& env, const QString& name, const QString& value)
{
  if (value.length() > 0)
  {
    // Create the strings for the permanent map
    UtString varName;
    varName << name;
    UtString* var = new UtString;
    (*var) << name << "=" << value;

    // Set the environment
    qDebug() << "shcon: " << var->c_str();
    putenv((char*)var->c_str());
    env.append(var->c_str());

    // Update the map. If it existed before, delete the old string
    EnvVars::iterator pos = mEnvVars.find(varName);
    if (pos != mEnvVars.end()) {
      delete pos->second;
      pos->second = var;
    } else {
      mEnvVars.insert(EnvVars::value_type(varName, var));
    }
  }
}

void ShellConsole::addCarbonEnvironmentVariables(const QStringList& extraVars)
{
  QStringList env = QProcess::systemEnvironment();

  // CARBON_CONFIGURATION
  UtString confName;
  confName << mProject->getActivePlatform() << "/" << mProject->getActiveConfiguration();
  addEnvVar(env, "CARBON_CONFIGURATION", confName.c_str());

  // CARBON_MODEL
  UtString carbonModel;
  carbonModel << mProject->getActive()->getOptions("VSPCompiler")->getValue("-o")->getValue();
  addEnvVar(env, "CARBON_MODEL", carbonModel.c_str());

  // CARBON_OUTPUT
  UtString carbonOutput;
  carbonOutput << mProject->getProjectDirectory() << "/" << confName;
  addEnvVar(env, "CARBON_OUTPUT", carbonOutput.c_str());

  // CARBON_PROJECT
  addEnvVar(env, "CARBON_PROJECT", mProject->getProjectDirectory());

  foreach (QString var, extraVars)
  {
    qDebug() << var;
    env.append(var);
  }

  mProcess->setEnvironment(env);
}

int ShellConsole::executeProgramWait(CarbonProject* proj, const QString& workingDir, QStringList& envVars, const QString& program, const QStringList& arguments)
{
  mProcess = new QProcess(this);

  if (workingDir.length() > 0)
    mProcess->setWorkingDirectory(workingDir);

  CQT_CONNECT(mProcess, started(), this, procStarted());
  CQT_CONNECT(mProcess, finished(int), this, procFinished(int));
  CQT_CONNECT(mProcess, error(QProcess::ProcessError), this, procError(QProcess::ProcessError));
  CQT_CONNECT(mProcess, readyReadStandardError(), this, procReadStderr());
  CQT_CONNECT(mProcess, readyReadStandardOutput(), this, procReadStdout());

  mProject = proj;
  UtString err;
  UtString shellProgram;

  if (useProject())
    addCarbonEnvironmentVariables(envVars);

  mArgs.clear();

  UtString expandedProg;
  UtString inputProg;
  inputProg << program;

  UtString errEx;
  OSExpandFilename(&expandedProg, inputProg.c_str(), &errEx);

  mProgram = expandedProg.c_str();
  mArgs << arguments;

  UtString cmdLine;
  cmdLine << expandedProg;
  foreach (QString arg, arguments)
    cmdLine << " " << arg;

#if pfWINDOWS
  QString commandLine = cmdLine.c_str();
  processLine(commandLine);
#endif

  qDebug() << "qprocesss" << mProgram << mArgs;

  mProcess->start( mProgram, mArgs );

  mProcess->waitForFinished(-1);

  return mExitCode;
}
// Initiate the shell session 
int ShellConsole::executeProgram(CarbonProject* proj, const QString& workingDir, QStringList& envVars, const QString& program, const QStringList& arguments)
{
  mProcess = new QProcess(this);

  if (workingDir.length() > 0)
    mProcess->setWorkingDirectory(workingDir);

  CQT_CONNECT(mProcess, started(), this, procStarted());
  CQT_CONNECT(mProcess, finished(int), this, procFinished(int));
  CQT_CONNECT(mProcess, error(QProcess::ProcessError), this, procError(QProcess::ProcessError));
  CQT_CONNECT(mProcess, readyReadStandardError(), this, procReadStderr());
  CQT_CONNECT(mProcess, readyReadStandardOutput(), this, procReadStdout());

  mProject = proj;
  UtString err;
  UtString shellProgram;

  if (useProject())
    addCarbonEnvironmentVariables(envVars);

  mArgs.clear();

  UtString expandedProg;
  UtString inputProg;
  inputProg << program;

  UtString errEx;
  OSExpandFilename(&expandedProg, inputProg.c_str(), &errEx);

  qDebug() << "before" << expandedProg.c_str();

#if pfWINDOWS
  QString eprog = expandedProg.c_str();
  eprog = eprog.replace("\\","/");
  expandedProg.clear();
  expandedProg << eprog;
#endif

  qDebug() << "after" << expandedProg.c_str();

  mProgram = expandedProg.c_str();
  mArgs << arguments;

  UtString cmdLine;
  cmdLine << expandedProg;
  foreach (QString arg, arguments)
    cmdLine << " " << arg;

#if pfWINDOWS
  QString commandLine = cmdLine.c_str();
  processLine(commandLine);
#endif

  qDebug() << "qprocesss" << mProgram << mArgs;
  qDebug() << "workingdir" << mProcess->workingDirectory();

  mProcess->start( mProgram, mArgs );

  return 0;
}
