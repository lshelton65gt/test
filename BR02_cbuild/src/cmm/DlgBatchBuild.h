#ifndef DLGBATCHBUILD_H
#define DLGBATCHBUILD_H

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include <QDialog>
#include "ui_DlgBatchBuild.h"
class CarbonProjectWidget;

class DlgBatchBuild : public QDialog
{
  Q_OBJECT

public:
  DlgBatchBuild(QWidget *parent = 0);
  ~DlgBatchBuild();
  void setProject(CarbonProjectWidget* proj);

private slots:
  void on_selectAll_clicked();
  void on_deselectAll_clicked();
  void on_buildButton_clicked();
  void on_rebuildButton_clicked();
  void on_cleanButton_clicked();

private:
  void checkButtons(Qt::CheckState newState);
  void buildCheckedList(QStringList& checkedList);

private:
  Ui::DlgBatchBuildClass ui;
  CarbonProjectWidget* mProject;
};

#endif // DLGBATCHBUILD_H
