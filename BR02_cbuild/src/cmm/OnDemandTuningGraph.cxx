//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


// This widget was derived from from Qwt's 'cpuplot' example.

#include "gui/CQt.h"
#include <qapplication.h>
#include <qlayout.h>
#include <qlabel.h>
#include <qpainter.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_curve.h>
#include <qwt_scale_draw.h>
#include <qwt_scale_widget.h>
#include <qwt_scale_engine.h>
#include <qwt_legend.h>
#include <qwt_legend_item.h>
#include <qwt_symbol.h>
#include <QTimer>
#include <QMouseEvent>
#include <QStatusBar>

#include "OnDemandTuningGraph.h"
#include "util/UtDoublePointArray.h"
#include "util/UtIOStream.h"
#include "shell/OnDemandTraceFileReader.h"
#include "CarbonMakerContext.h"
#include "gui/CQt.h"
#include "WorkspaceModelMaker.h"

// Ideally we should have a quasi-logarithmic scale like I describe in bug7894.
// It looks like this can be implemented in Qwt by overriding QwtLog10ScaleEngine
// and related classes.  But I don't think I'll be able to get that done for this
// release.
// 
// As an alternative, I'm going to try having buttons to help adjust the scale.
// These are in OnDemandTuningWidget.cxx

#define INITIAL_MAX_CPS 1000
#define INITIAL_SCHED_CALLS 1000
#define MIN_HEIGHT 50
#define INIT_HEIGHT 260
  
//! class to represent the background coloring on a replay performance plot
class OTGBackground: public QwtPlotItem {
public:
  OTGBackground() {
    setZ(0.0);
    unhighlight();
    unselect();
  }
  
  virtual int rtti() const {
    return QwtPlotItem::Rtti_PlotUserItem;
  }
  
  void unhighlight() {
    mHighlightXmin = -1;
    mHighlightXmax = -1;
  }

  void highlight(double xmin, double xmax) {
    mHighlightXmin = xmin;
    mHighlightXmax = xmax;
  }

  void select(double xmin, double xmax) {
    mSelectXmin = xmin;
    mSelectXmax = xmax;
  }

  void unselect() {
    mSelectXmin = -1;
    mSelectXmax = -1;
  }

  virtual void draw(QPainter *painter,
                    const QwtScaleMap &xMap, const QwtScaleMap &,
                    const QRect &rect) const
  {
    QColor c(Qt::white);
    QRect r = rect;

    // Draw all white first.  Then draw the selection & highlight over it
    painter->fillRect(rect, Qt::white);

    // Now draw idle & backoff regions
    for (UInt32 i = 0; i < mIdleRegions.size(); ++i) {
      r.setLeft(xMap.transform(mIdleRegions.getX(i)));
      r.setRight(xMap.transform(mIdleRegions.getY(i)));
      painter->fillRect(r, Qt::magenta);
    }
    for (UInt32 i = 0; i < mBackoffRegions.size(); ++i) {
      r.setLeft(xMap.transform(mBackoffRegions.getX(i)));
      r.setRight(xMap.transform(mBackoffRegions.getY(i)));
      painter->fillRect(r, QColor("orange"));
    }

    if (mHighlightXmax >= 0) {
      r.setLeft(xMap.transform(mHighlightXmin));
      r.setRight(xMap.transform(mHighlightXmax));
      painter->fillRect(r, Qt::lightGray);
    }

    if (mSelectXmax >= 0) {
      r.setLeft(xMap.transform(mSelectXmin));
      r.setRight(xMap.transform(mSelectXmax));
      painter->fillRect(r, QColor("darkslategray"));
    }
  }

  void clearIdleRegions() {
    mIdleRegions.clear();
  }

  void addIdleRegion(double first, double last) {
    mIdleRegions.push_back(first, last);
  }

  void clearBackoffRegions() {
    mBackoffRegions.clear();
  }

  void addBackoffRegion(double first, double last) {
    mBackoffRegions.push_back(first, last);
  }

private:
  UtDoublePointArray mIdleRegions;
  UtDoublePointArray mBackoffRegions;

  double mHighlightXmin;
  double mHighlightXmax;
  double mSelectXmin;
  double mSelectXmax;
}; // class OTGBackground: public QwtPlotItem

//! OTGCurve class
/*!
 *! This class maintains two arrays of doubles in the format required
 *! by QwtPlotCurve::setRawData.  We can't use UtArray for this
 *! because it has not (yet) implemented value semantics supporting
 *! data that's more than 4 bytes.  We can't use UtVector for this
 *! because std::vector has linking issues on the cross-compiled
 *! Windows for us.  So we maintain these arrays with raw alloc/free
 *! calls and explicit capacity.
 *!
 *! The best solution to clean this up is probably to give UtArray
 *! value-semantics, following the code in UtHashSet.  But for now
 *! we must need to implement this properly.
 *!
 */
class OTGCurve: public QwtPlotCurve {
public:
  //! ctor
  OTGCurve(const QString& title) :
    QwtPlotCurve(title)
  {
  }
  
  //! dtor
  ~OTGCurve() {
  }

  //! set the color of this curve
  void setColor(const QColor &color)
  {
    QColor c = color;
    c.setAlpha(128);
    
    setPen(c);
    setBrush(c);
  }

  void push_back(CarbonTime time, UInt32 num_violations) {
    mPoints.push_back((double) time, (double) num_violations);
  }

  //! clear the curve
  void clear() {
    mPoints.clear();
  }

  //! remove a point from this curve
  void pop_back() {
    mPoints.pop_back();
  }
 
  bool empty() const {return mPoints.empty();}

  double lastX() {return mPoints.lastX();}
  double lastY() {return mPoints.lastY();}

  //! graphically render the point-array buffers, copying them into the base class curve
  void finalize() {
    setRawData(mPoints.getXData(), mPoints.getYData(), mPoints.size());
  }

  UtDoublePointArray mPoints;
};

OTGCurve* OnDemandTuningGraph::addLegend(const char* name, QwtSymbol::Style style,
                                    int x, int y, const QColor& color)
{
  QwtSymbol block;
  block.setStyle(style);
  block.setSize(QSize(x, y));
  block.setBrush(QBrush(color));
  block.setPen(color);

  OTGCurve* curve = new OTGCurve(name);
  curve->setSymbol(block);
  curve->setColor(color);
  curve->attach(this);
  curve->setStyle(QwtPlotCurve::Lines);

  return curve;
}

OnDemandTuningGraph::OnDemandTuningGraph(QWidget *parent,
                                         CarbonMakerContext* cmc):
  QwtPlot(parent),
  mReader(NULL),
  mCarbonMakerContext(cmc),
  mUserYMultiplier(1.0),
  mMaxViolations(0),
  mSimTime(0)
{
  clear(true);
  setAutoReplot(false);
  
  plotLayout()->setAlignCanvasToScales(true);
  
  QwtLegend* right_legend = new QwtLegend;
  insertLegend(right_legend, QwtPlot::RightLegend);
  
  setAxisScale(QwtPlot::xBottom, 0, INITIAL_SCHED_CALLS);
  mXAxisMax = INITIAL_SCHED_CALLS;
  setAxisLabelAlignment(QwtPlot::xBottom, Qt::AlignLeft | Qt::AlignBottom);
  //setAxisScaleEngine(QwtPlot::yLeft, new QwtLog10ScaleEngine);

  /*
    In situations, when there is a label at the most right position of the
    scale, additional space is needed to display the overlapping part
    of the label would be taken by reducing the width of scale and canvas.
    To avoid this "jumping canvas" effect, we add a permanent margin.
    We don't need to do the same for the left border, because there
    is enough space for the overlapping label below the left scale.
  */
  
  QwtScaleWidget *scaleWidget = axisWidget(QwtPlot::xBottom);
  const int fmh = QFontMetrics(scaleWidget->font()).height();
  scaleWidget->setMinBorderDist(0, fmh / 2);
  
  setAxisScale(QwtPlot::yLeft, 0, INITIAL_MAX_CPS);
  setAxisTitle(QwtPlot::yLeft, "Active Signals");
  mYAxisMax = INITIAL_MAX_CPS;
  setAxisTitle(QwtPlot::xBottom, "Simulation Time");
  

  mBackground = new OTGBackground;
  mBackground->attach(this);
  
  mAnyActivity = addLegend("Violation", QwtSymbol::NoSymbol,  12, 12,
                           QColor(Qt::blue));
  (void) addLegend("Idle", QwtSymbol::Rect,  12, 12, QColor(Qt::magenta));
  (void) addLegend("Backoff", QwtSymbol::Rect,  12, 12, QColor("orange"));
/*
  mNonIdleNet = addLegend("Active Net", QwtSymbol::NoSymbol,  12, 12,
                          QColor(Qt::red));
  mNonIdleChange = addLegend("Active Change", QwtSymbol::NoSymbol,  12, 12,
                             QColor(Qt::green));
  mDiverge = addLegend("Diverge", QwtSymbol::NoSymbol,  12, 12,
                       QColor(Qt::magenta));
  mFail = addLegend("Fail", QwtSymbol::NoSymbol,  12, 12,
                    QColor(Qt::yellow));
*/

  setMinimumHeight(MIN_HEIGHT);
  setMouseTracking(true);
  canvas()->setMouseTracking(true);

  mPollTimer = new QTimer(this);
  mPollTimer->setInterval(2000); // milliseconds

  CQT_CONNECT(mPollTimer, timeout(), this, readMoreText());
} // OnDemandTuningGraph::OnDemandTuningGraph

OnDemandTuningGraph::~OnDemandTuningGraph() {
  mPollTimer->stop();
  // delete mPollTimer?
  if (mReader != NULL) {
    delete mReader;
  }
  clear(false);
}

void OnDemandTuningGraph::clear(bool initQuantum) {
  mCurrQuantum = NULL;
  mIdleQuantum = NULL;
  mSelectIndex = -1;
  mHighlightIndex = -1;
  mXMax = 0;
  mYMax = 0;
  mNodes.clearPointerValues();
  mEvents.clearPointers();
  mQuantumArray.clearPointers();

  if (initQuantum) {
    newQuantum(0);
  }

  emit quantumUnselect();
}


OnDemandTuningGraph::Node* OnDemandTuningGraph::getNode(const char* path) {
  Node* node = mNodes[path];
  if (node == NULL) {
    node = new Node(path);
    mNodes[path] = node;
  }
  return node;
}

OnDemandTuningGraph::Node* OnDemandTuningGraph::findNode(const char* path)
  const
{
  StringNodeMap::const_iterator p = mNodes.find(path);
  if (p == mNodes.end()) {
    return NULL;
  }
  return p->second;
}

void OnDemandTuningGraph::readTraceCB(void *userData,
                                      CarbonUInt64 schedCalls,
                                      CarbonTime simTime,
                                      CarbonOnDemandDebugType type,
                                      CarbonOnDemandDebugAction action,
                                      UInt32 length,
                                      const char *path)
{
  // Schedule calls aren't used yet
  (void) schedCalls;
  
  OnDemandTuningGraph* graph = (OnDemandTuningGraph*) userData;
  Node* node = graph->getNode(path);
  graph->addEvent(new Event(simTime, type, action, length, node));
}

void OnDemandTuningGraph::mergeSimilarQuanta() {
  // At this point we have nominally completed this quantum.
  // To simplify the graphics and make it easier to select,
  // merge with previous quanta if the set of signal violations
  // is identical
  UInt32 sz = mQuantumArray.size();
  if (sz > 1) {
    Quantum* quantum = mQuantumArray[sz - 1];
    Quantum* prev = mQuantumArray[sz - 2];
    if (prev->hasSameViolations(quantum)) {
      prev->putLastTime(quantum->getLastTime());
      delete quantum;
      mQuantumArray.resize(sz - 1);
    }

    // On the other hand, if the previous quantum has no events (meaning
    // the design is idle), and there is a gap in time between that one
    // and this one, then we extend the idle quanta to include the gap.
    else if ((prev->numNodeEvents() == 0) &&
             (quantum->getFirstTime() - prev->getLastTime() > 1))
    {
      prev->putLastTime(quantum->getFirstTime() - 1);
    }
  }
}

void OnDemandTuningGraph::readModeChangeCB(void *userData,
                                           CarbonUInt64 schedCalls,
                                           CarbonTime simTime,
                                           bool enterIdle)
{
  (void) schedCalls;

  OnDemandTuningGraph* graph = (OnDemandTuningGraph*) userData;
  if (enterIdle) {
    // Never delete empty idle quanta.  We want them to show up in the
    // graph as zero-event periods.
    graph->closeQuantum(simTime, false);
  } else {
    graph->newQuantum(simTime);
  }
}

void OnDemandTuningGraph::staleDataCB(void *userData) {
  OnDemandTuningGraph* graph = (OnDemandTuningGraph*) userData;
  graph->clear(true);
}

void OnDemandTuningGraph::addEvent(Event* event) {
  CarbonTime sim_time = event->getSimTime();
  
  // cwavetestbench -n 2 can cause simulation time to go backwards.
  // Handle this by clearing out all existing events if we see this
  // in the cot file.
  if (mSimTime > sim_time) {
    // Before we clear the graph, we need to save the name of the node
    // associated with this event.  The nodes are allocated by a
    // factory class, and they'll get deleted when the graph is
    // cleared.
    Node* node = event->getNode();
    UtString nodeName;
    nodeName << node->getName();

    // Now it's safe to clear
    clear(true);

    // Reallocate the node from the factory
    node = getNode(nodeName.c_str());
    event->putNode(node);
  }
  mSimTime = sim_time;

#if 0

  Quantum* quantum = NULL;
  UInt32 sz = mQuantumArray.size();
  if (sz > 0) {
    quantum = mQuantumArray[sz - 1];
    if (time != quantum->getLastTime()) {
      if (quantum->numTimeSlots() == mCycleDepth) {
        mergeSimilarQuanta();
        quantum = NULL;
      }
    }
  }

  if (quantum == NULL) {
    quantum = new Quantum(time);
    mQuantumArray.push_back(quantum);
  }
  quantum->addEvent(event);
#endif

  if (mCurrQuantum != NULL) {
    mCurrQuantum->addEvent(event);
  } else {
    // If we're idle, there's no quantum open because we don't want to
    // display anything in the current timeframe.  However, an event
    // has happened that will cause us to exit the idle state, so we
    // want to record it.  We'll save these in a special non-idle
    // quantum, which will be used when the ensuing mode change occurs
    if (mIdleQuantum == NULL) {
      mIdleQuantum = new Quantum(sim_time);
    }
    mIdleQuantum->addEvent(event);
  }

/*
  //mAnyActivity->push_back(timed, 
  switch (event->getType()) {
  case eCarbonOnDemandDebugNonIdleNet:
  case eCarbonOnDemandDebugNonIdleChange:
  case eCarbonOnDemandDebugRestore:
  case eCarbonOnDemandDebugDiverge:
  case eCarbonOnDemandDebugFail:
    break;
  }
*/
  mEvents.push_back(event);
}

bool OnDemandTuningGraph::Quantum::hasSameViolations(Quantum* other) {
  if (other->mNodeEventMap.size() != mNodeEventMap.size()) {
    return false;
  }

  // walk through all my nodes, and make sure 'other' has the same
  // nodes and the same type events in those nodes
  for (LoopMap<NodeEventMap> p(mNodeEventMap); !p.atEnd(); ++p) {
    Node* node = p.getKey();

    NodeEventMap::iterator q = other->mNodeEventMap.find(node);
    if (q == other->mNodeEventMap.end()) {
      return false;
    }

    const EventArray& a1 = p.getValue();
    const EventArray& a2 = q->second;
    UInt32 sz = a1.size();
    if (sz != a2.size()) {
      return false;             // different numbers of events
    }

    for (UInt32 i = 0; i < sz; ++i) {
      if (a1[i]->getType() != a2[i]->getType()) {
        return false;
      }
    }
  }
  return true;
} // bool OnDemandTuningGraph::Quantum::hasSameViolations

void OnDemandTuningGraph::newQuantum(CarbonTime t)
{
  // Close the previous one.  It wasn't for an idle state, so delete
  // it if it's empty.  This allows quantum merging to be more
  // effective.
  //
  // For example, if idle detection continually fails due to a
  // non-repeating signal, you will have alternating quanta types: one
  // for (looking, backoff) with the events for each signal, and one
  // for (backoff, looking) with no events.  By deleting these empty
  // events, the repeated failed pattern detections will be merged
  // into a single quantum.
  closeQuantum(t, true);
  mergeSimilarQuanta();         // merge last two quant if identical

  // If we were idle and were building a temporary quantum, use that
  // one
  if (mIdleQuantum != NULL) {
    mCurrQuantum = mIdleQuantum;
    mIdleQuantum = NULL;
  } else {
    mCurrQuantum = new Quantum(t);
  }
  mQuantumArray.push_back(mCurrQuantum);
}

void OnDemandTuningGraph::closeQuantum(CarbonTime t, bool deleteIfEmpty)
{
  if (mCurrQuantum != NULL) {
    mCurrQuantum->close(t);
    // If requested, and there are no events, delete it
    if (deleteIfEmpty && (mCurrQuantum->numNodeEvents() == 0)) {
      delete mCurrQuantum;
      mQuantumArray.pop_back();
    }
    mCurrQuantum = NULL;
  }
}


bool OnDemandTuningGraph::loadTraceFile(const char* fname, UtString* /*errmsg*/) {
  if (mReader != NULL) {
    delete mReader;
  }
  else {
    mPollTimer->start();
  }
  mReader = new OnDemandTraceFileReader(fname, readTraceCB, readModeChangeCB,
                                        staleDataCB, this);
  mReadFilename = fname;
  mAnyActivity->clear();
  readMoreText();              // look immediately
  return true;
}

void OnDemandTuningGraph::readMoreText() {
  if (! mReader->checkFileForUpdate()) {
    return;
  }

  if (! mReader->read()) {
    // If there was an error, just put it in the status bar for 2 seconds.
    const char* err = mReader->getErrmsg();
    QStatusBar* status_bar
      = mCarbonMakerContext->getWorkspaceModelMaker()->getStatusBar();
    status_bar->showMessage(err, 2000);
    return;
  }
  
  mergeSimilarQuanta();         // merge last two quant if identical

  // At this point we have quantized all the results into buckets for
  // mCycleDepth samples.  Present them like that.
  CarbonTime last_time = 0;
  UInt32 max_events = 0;
  mAnyActivity->clear();
  mAnyActivity->push_back(0, 0);
  mBackground->clearIdleRegions();
  mBackground->clearBackoffRegions();

  for (UInt32 i = 0; i < mQuantumArray.size(); ++i) {
    Quantum* quantum = mQuantumArray[i];

    CarbonTime first_time = quantum->getFirstTime();

    // back-off time is represented by an absense a quantum.  So any
    // gaps between the previous quantum and this one are back-off time.
    if (first_time > (last_time + 1)) {
      mBackground->addBackoffRegion(last_time + 1, first_time - 1);
    }

    mAnyActivity->push_back(first_time, 0);
    // For now just count the total number of events on each node in
    // the quantum
    UInt32 num_events = quantum->numNodeEvents();
    last_time = quantum->getLastTime();
    mAnyActivity->push_back(first_time, num_events);
    mAnyActivity->push_back(last_time, num_events);
    max_events = std::max(num_events, max_events);

    mAnyActivity->push_back(last_time, 0);

    // idle-time is represented by a Magenta (see ReplayPerfPlot.cxx's
    // handling of case eOnDemandEventIdle.  Idle-time is represented
    // in the COT file as a quantum with 0 events.  So paint that with
    // a purple background
    if (num_events == 0) {
      mBackground->addIdleRegion(first_time, last_time);
    }
  }  
  //mAnyActivity->push_back(last_time, 0);

  mAnyActivity->finalize();
/*
  mNonIdleNet->finalize();
  mNonIdleChange->finalize();
  mDiverge->finalize();
  mFail->finalize();
*/

  if (mMaxViolations != max_events) {
    mMaxViolations = max_events;
    emit scaleChanged();
  }
  
  // When we are finished, leave a little room at the top because if
  // the top line is at the top border then it's hard to see.
  if (mReader->isEndOfData()) {
    mYMax = 1.2 * max_events;
    setAxisScale(QwtPlot::yLeft, 0, std::max(FMIN_VIOLATIONS_TO_SHOW, mYMax * mUserYMultiplier));
    mXMax = last_time;
    setAxisScale(QwtPlot::xBottom, 0, mXMax);
  }
  else {
    if (max_events > mYMax) {
      // No extra headroom needed for # events, as that is likely to be stable
      mYMax = 1.2 * max_events; // no extra 
      setAxisScale(QwtPlot::yLeft, 0, std::max(FMIN_VIOLATIONS_TO_SHOW, mYMax * mUserYMultiplier));
    }
    if (last_time > mXMax) {
      // 50% headroom for time axis as time marches along indefinitely --
      // we don't know when the simulation will end.
      mXMax = 1.5 * last_time;
      setAxisScale(QwtPlot::xBottom, 0, mXMax);
    }
  }

  replot();
} // void OnDemandTuningGraph::readMoreText

void OnDemandTuningGraph::putMagnification(double mag) {
  mUserYMultiplier = mag;
  setAxisScale(QwtPlot::yLeft, 0, std::max(FMIN_VIOLATIONS_TO_SHOW, mYMax * mUserYMultiplier));
  replot();
}

QSize OnDemandTuningGraph::sizeHint() const {
  QSize plotSizeHint = QwtPlot::sizeHint();
  plotSizeHint.setHeight(INIT_HEIGHT);
  return plotSizeHint;
}

struct ODFindQuantum {
  ODFindQuantum(double dtime) : mTime((CarbonTime) dtime) {}

  bool operator<(const OnDemandTuningGraph::Quantum* q) const {
    CarbonTime qtime = q->getFirstTime();
    return mTime < qtime;
  }

  CarbonTime mTime;
};

bool operator<(const OnDemandTuningGraph::Quantum* q, const ODFindQuantum& f) {
  return q->getFirstTime() < f.mTime;
}

//bool operator<(const OnDemandTuningGraph::Quantum* q, CarbonTime ctime) {
//  return q->getFirstTime() < ctime);
//}

SInt32 OnDemandTuningGraph::findQuantumIndex(QMouseEvent* e) {
  // We do our drawing with respect to canvas().  But we can only install the
  // event handler in the QwtPlot widget because that's what we have the
  // opportunity to override via this virtual method.   So let's translate
  // this coordinate into the canvas, which effectively subtracts off the
  // space for the y-axis label & decoration.
  QPoint canvas_pos = canvas()->mapFrom(this, e->pos());

  QwtScaleMap x_scale_map = canvasMap(QwtPlot::xBottom);
  QwtScaleMap y_scale_map = canvasMap(QwtPlot::yLeft);

  double x_val = x_scale_map.invTransform(canvas_pos.x());
  double y_val = y_scale_map.invTransform(canvas_pos.y());

  // Return nothing selected if the cursor is out of bounds.
  double x_min = x_scale_map.s1();
  double x_max = x_scale_map.s2();
  double y_min = y_scale_map.s1();
  double y_max = y_scale_map.s2();

  if ((x_val < x_min) || (x_val > x_max) ||
      (y_val < y_min) || (y_val > y_max)) {
    return -1;
  }

  // Find the quantum that contains this x_val
  std::pair<QuantumArray::const_iterator,QuantumArray::const_iterator> p =
    std::equal_range(mQuantumArray.begin(), mQuantumArray.end(), ODFindQuantum(x_val));
//  std::pair<QuantumArray::iterator,QuantumArray::iterator> p =
//    std::equal_range(mQuantumArray.begin(), mQuantumArray.end(), (CarbonTime) x_val);

  UInt32 i1 = p.first - mQuantumArray.begin();
  UInt32 i2 = p.second - mQuantumArray.begin();

  UInt32 arraySize = mQuantumArray.size();
  if ((i1 == i2) && (arraySize != 0) && (i1 <= arraySize)) {
    // equal_range actually gives us the position *after* the one we want, unless
    // we are in pixel 0.  So decrement.
    if (i1 > 0) {
      --i1;
    }

    // Avoid selecting an idle quantum -- there is no value to users because there is
    // nothing to display.  Note that we will not have two consecutive idle quanta.
    // If we did they would be merged into one anyway by mergeSimilarQuanta().  Note
    // that this allows selection of the left-most idle region.
    Quantum* left_quantum = mQuantumArray[i1];
    if ((left_quantum->numNodeEvents() == 0) && (i1 > 0)) {
      --i1;
      left_quantum = mQuantumArray[i1];
    }

    // This binary search approach selects the quantum to the left if the user
    // moves the mouse between quanta.  It would be preferable to select the
    // closest one.  When the user moves the mouse to the right edge of no-man's
    // land, then it should select the right quanta if there is one.
    if ((left_quantum->getLastTime() < x_val) && ((i1 + 1) < arraySize)) {
      double distance_from_left = x_val - left_quantum->getLastTime();
      UInt32 right_idx = i1 + 1;
      Quantum* right_quantum = mQuantumArray[right_idx];

      // Don't snap to an idle region, instead snap to the next region
      if ((right_quantum->numNodeEvents() == 0) && ((right_idx + 1) < arraySize)) {
        ++right_idx;
        right_quantum = mQuantumArray[right_idx];
      }

      double distance_from_right = right_quantum->getFirstTime() - x_val;
      if (distance_from_right < distance_from_left) {
        // select the right quantum instead
        i1 = right_idx;
      }
    }

    return (SInt32) i1;
  }
  return -1;
} // SInt32 OnDemandTuningGraph::findQuantumIndex

void OnDemandTuningGraph::mouseMoveEvent(QMouseEvent* e) {
  SInt32 idx = findQuantumIndex(e);

  // Found an index?  Highlight the quantum
  if (idx != -1) {
    if (mHighlightIndex != idx) {
      mHighlightIndex = idx;
      Quantum* quantum = mQuantumArray[idx];
      mBackground->highlight(quantum->getFirstTime(), quantum->getLastTime());
      replot();
    }
  }
  else {
    if (mHighlightIndex != -1) {
      mBackground->unhighlight();
      mHighlightIndex = -1;
      replot();
    }
  }

  // Forward the event
  QwtPlot::mouseMoveEvent(e);
} // void OnDemandTuningGraph::mouseMoveEvent

void OnDemandTuningGraph::mousePressEvent(QMouseEvent* e) {
  SInt32 idx = findQuantumIndex(e);

  // Found an index?  Select it and emit a signal for tuning widget to update
  if (idx != -1) {
    selectQuantum(idx);
  }
  else {
    if (mSelectIndex != -1) {
      mBackground->unselect();
      mSelectIndex = -1;
      emit quantumUnselect();
      replot();
    }
  }

  // Forward the event
  QwtPlot::mouseMoveEvent(e);
} // void OnDemandTuningGraph::mousePressEvent

void OnDemandTuningGraph::selectQuantum(SInt32 idx) {
  if ((idx >= 0) && (idx < ((SInt32) mQuantumArray.size())) && (idx != mSelectIndex)) {
    mSelectIndex = idx;
    Quantum* quantum = mQuantumArray[idx];
    mBackground->select(quantum->getFirstTime(), quantum->getLastTime());
    emit quantumSelect(idx);
    replot();
  }
}
