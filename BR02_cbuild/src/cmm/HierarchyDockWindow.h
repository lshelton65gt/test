#ifndef HIERARCHYDOCKWINDOW_H
#define HIERARCHYDOCKWINDOW_H

#include "util/CarbonPlatform.h"

#include <QWidget>
#include "ui_HierarchyDockWindow.h"
#include "CarbonDatabaseContext.h"
#include "ApplicationContext.h"

class CarbonMakerContext;
class XToplevelInterface;
class XInstance;


class AppContext : public ApplicationContext
{
public:
  AppContext() : ApplicationContext(NULL,NULL,NULL)
  {
    mModified = false;
  }
  virtual void setDocumentModified(bool flag)
  {
    mModified = flag;
  }
private:
  bool mModified;
};

class HierarchyDockWindow : public QWidget
{
  Q_OBJECT

public:
  HierarchyDockWindow(QWidget *parent = 0);
  ~HierarchyDockWindow();
  void setContext(HierarchyMode mode, CarbonMakerContext* ctx, CarbonDatabaseContext* dbContext);
  void clear();
  void selectNet(const char* netName);
  void selectModule(const char* modName);
  bool browseNet(const char* netName);
  bool browseableNet(const char* netName);

private:
  const char* buildModuleList(const CarbonDBNode* node, QStringList& moduleList);

signals:
  void netSelectionChanged(const char* netPath);
  void moduleSelectionChanged(const char* modPath);
  void navigateSource(const char* src, int lineNumber);
  void applySignalDirective(DesignDirective, const char*, bool);

private slots:
    void on_checkBoxLoadDesign_stateChanged(int);
 void selectionChanged(CarbonDBNode* node);
 void moduleSelectionChanged(CarbonDBNode* node,XToplevelInterface*,XInstance*);
 void moduleDoubleClicked(const char* nodeName, const char* module, const char* locator);
 void navigateRequest(const char* src, int line);
 void broadcastDirective(DesignDirective, const char* text, bool value);
 void on_checkBoxEO_stateChanged(int);
 void on_checkBoxED_stateChanged(int);
 void on_lineEditFilter_editingFinished();

private:
  Ui::HierarchyDockWindowClass ui;
  AppContext mAppCtx;
  CarbonMakerContext* mContext;
  bool mExplicitObserves;
  bool mExplicitDeposits;
  QString mFilterString;
};

#endif // HIERARCHYDOCKWINDOW_H
