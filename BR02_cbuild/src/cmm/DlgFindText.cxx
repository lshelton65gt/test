//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "gui/CQt.h"
#include "DlgFindText.h"
#include "QTextEditor.h"
#include <QDebug>

#include "util/UtString.h"

DlgFindText::DlgFindText(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);
  mEditor = NULL;
  QSettings settings("Carbon Design Systems","EditStrings");

  mStrings = settings.value("SearchStrings").toStringList();

  mCompleter = new QCompleter(mStrings, this);
  ui.comboBoxText->setCompleter(mCompleter);

  for (int i=0; i<mStrings.count(); i++)
   ui.comboBoxText->addItem(mStrings[i]);

  CQT_CONNECT(this, rejected(), this, dialogClosed());
}

void DlgFindText::dialogClosed()
{
  QSettings settings("Carbon Design Systems","EditStrings");
 
  settings.setValue("SearchStrings", mStrings);
}

void DlgFindText::setCurrentText(QString text)
{
  ui.comboBoxText->setEditText(text);
  QLineEdit* lineEdit = ui.comboBoxText->lineEdit();
  lineEdit->setSelection(0, text.length());
}

void DlgFindText::setEditor(QsciScintilla* editor)
{
  mEditor = editor;
  mFirst = false;
}

DlgFindText::~DlgFindText()
{
}

void DlgFindText::on_pushButtonFind_clicked()
{
  QString currText = ui.comboBoxText->currentText();

  if (currText != mLastText)
    mFirst = false;

  if (!mStrings.contains(currText))
  {
    ui.comboBoxText->insertItem(0, currText);
    mStrings.push_front(currText);
  }
  else
  {
    int index = mStrings.indexOf(currText);
    if (index != 0)
    {
      mStrings.removeAt(index);
      mStrings.push_front(currText);
      ui.comboBoxText->clear();
      ui.comboBoxText->addItems(mStrings);
    }
  }

  bool foundsomething = false;
  if (mEditor)
  {
    UtString text;
    text << currText;

    bool direction = ui.radioButtonDown->isChecked();

    if (!mFirst)
    {
      foundsomething = mEditor->findFirst(text.c_str(), 
        ui.checkBoxRegexp->isChecked(),
        ui.checkBoxMatchCase->isChecked(),
        ui.checkBoxWholeWord->isChecked(),
        true,
        direction);
      mFirst = true;
    }
    else
      foundsomething = mEditor->findNext();
  }

  if (foundsomething)
    mLastText = currText;
  else
    QApplication::beep();
}

void DlgFindText::on_pushButtonCancel_clicked()
{
  reject();
}

void DlgFindText::on_radioButtonUp_toggled(bool)
{
  mFirst = false;
}

void DlgFindText::on_radioButtonDown_toggled(bool)
{
  mFirst = false;
}

void DlgFindText::on_checkBoxWholeWord_stateChanged(int)
{
  mFirst = false;
}

void DlgFindText::on_checkBoxMatchCase_stateChanged(int)
{
  mFirst = false;
}

void DlgFindText::on_checkBoxRegexp_stateChanged(int)
{
  mFirst = false;
}
