//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include "util/UtEncryptDecrypt.h"

#include <QUiLoader>
#include <QtScript>

#include "ScriptingEngine.h"
#include "ScrCcfg.h"
#include "cmm.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "JavaScriptAPIs.h"
#include "CarbonConsole.h"
#include "DesignXML.h"
#include "ScrProject.h"
#include "ScrApplication.h"
#include "ScrCarbonDB.h"
#include "ScrPlugin.h"
#include "SpiritXML.h"
#include "Spirit14.h"
#include "KitManifest.h"
#include "ModelKit.h"
#include "CarbonOptions.h"
#include "DlgMessageText.h"
#include <QtScriptTools>
#include <QScriptEngineDebugger>

#include "scriptedit.h"
#include "textedit.h"

// Builtin functions
static QScriptValue alertFunction(QScriptContext *ctx, QScriptEngine *eng);
static QScriptValue debugFunction(QScriptContext *ctx, QScriptEngine *eng);
static QScriptValue loaduiFunction(QScriptContext *ctx, QScriptEngine *eng);
static QScriptValue includeFunction(QScriptContext *ctx, QScriptEngine *eng);
static QScriptValue enumToStringFunction(QScriptContext *ctx, QScriptEngine *eng);
static QScriptValue stringToEnumFunction(QScriptContext *ctx, QScriptEngine *eng);

void ScriptingEngine::evaluationSuspended()
{
  qDebug() << "Script Debugger Shown (Suspended)";
}

void ScriptingEngine::evaluationResumed()
{
  qDebug() << "Script Debugger Resumed";
}

void ScriptingEngine::setEnableDebugger(bool enable, bool stopAtFirstLine)
{
  bool allowDebugger = true;
  
  // -modelKit disables this (we want the unexpected modal dialog error)
  if (theApp->checkArgument("-modelKit"))
    allowDebugger = false;

  if (theApp->checkArgument("-allowDebugger"))
    allowDebugger = true;

  if (allowDebugger)
  {
    mDebugger = enable;
    mDebugStopAtFirstLine = stopAtFirstLine;

    if (enable)
    {
      mEmbeddedDebugger.attachTo(this);

      CQT_CONNECT(&mEmbeddedDebugger, evaluationSuspended(), this, evaluationSuspended());
      CQT_CONNECT(&mEmbeddedDebugger, evaluationResumed(), this, evaluationResumed());

      QMainWindow* debugWindow = mEmbeddedDebugger.standardWindow();

      debugWindow->resize(1024, 640);

      if (stopAtFirstLine)
      {
        mEmbeddedDebugger.action(QScriptEngineDebugger::InterruptAction)->trigger();
        debugWindow->show();
      }
    }
    else
    {
      mEmbeddedDebugger.detach();
    }
  }
}

void ScriptingEngine::registerProperty(const QString& propertyName, QObject* obj)
{
  QScriptValue global = globalObject();
  global.setProperty(propertyName, newQObject(obj));
}

class EnumWrapper : public QScriptClass
{
public:
  EnumWrapper(QScriptEngine* engine, const QMetaObject* mo, QMetaEnum me) : QScriptClass(engine)
  {
    mMetaObject = mo;
    mMetaEnum = me;

    //qDebug() << "Creating enum wrapper for" << me.name();

    QString enumName = me.name();
    engine->globalObject().setProperty(enumName, engine->newObject(this));
  }

  virtual ~EnumWrapper()
  {
  }

  QueryFlags queryProperty(const QScriptValue &,
    const QScriptString &name,
    QueryFlags , uint *)
  {
    QString ename = name.toString();
    UtString v; v << ename;
    int value = mMetaEnum.keyToValue(v.c_str());

    if (value >= 0)
      return QScriptClass::HandlesReadAccess;
    else
    {
      QString errMsg = QString("Unknown enumeration %1 %2 %3")
        .arg(mMetaEnum.scope())
        .arg(mMetaEnum.name())
        .arg(ename);
      engine()->currentContext()->throwError(QScriptContext::TypeError, errMsg);
      return 0;
    }
  }


  QScriptValue property(const QScriptValue &,
    const QScriptString &name, uint)
  {
    QString ename = name.toString();
    UtString v; v << ename;   
    int value = mMetaEnum.keyToValue(v.c_str());
    return QScriptValue(engine(), value);
  }

private:
  QMetaEnum mMetaEnum;
  const QMetaObject* mMetaObject;
};



void ScriptingEngine::addBuiltinFunctions()
{
  QScriptValue global = globalObject();

  ProtoUIWidget* uiWidgetProto = new ProtoUIWidget();
  setDefaultPrototype(qMetaTypeId<QWidget*>(), newQObject(uiWidgetProto));

  KitUserContext::registerTypes(this);
  KitTest::registerTypes(this);
  CarbonOptions::registerTypes(this);
  ModelKit::registerTypes(this);
  KitManifest::registerTypes(this);
  ScrPlugin::registerTypes(this);

  // In order to test the embedded model scripts (used by libcfg)
  // inside Modelstudio use this switch or env. variable, otherwise
  // the scripting engine doesn't register those types (and you can't mix both)
  if (theApp->checkArgument("-debugEmbeddedModels") || getenv("CARBON_SCRIPTING_DEBUG_USE_NATIVE_CCFG") != NULL)
  {
    // You cannot register both CarbonCfg::registerTypes 
    // and ScrCcfg::registerTypes (it's one or the other)
    // You will get collisions when mixing objects returned
    CarbonCfg::registerTypes(this);
    CfgScriptingCarbonDB::registerTypes(this);
  }
  else // the Default
    ScrCcfg::registerTypes(this);

  ScrProject::registerTypes(this);
  ScrApplicationProto::registerTypes(this);
  ScrCarbonDB::registerTypes(this);
  SpiritXML::registerTypes(this);
  SpiritXML_1_4::registerTypes(this);
  CarbonConsole::registerTypes(this);
  XDesignHierarchy::registerTypes(this);

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  // Construct Application Object
  ScrApplication* scriptApp = new ScrApplication(this);
  global.setProperty("Application", newObject(scriptApp));

  if (proj)
  {
    // Construct Project Object (if there is one)
    ScrProject* scriptProject = new ScrProject();
    global.setProperty("Project", newQObject(scriptProject));

    // Construct CarbonDB object
    ScrCarbonDB* scriptDB = new ScrCarbonDB();
    if (scriptDB->getDB())
      global.setProperty("CarbonDB", newQObject(scriptDB));
  }

  QScriptValue alertFunc = newFunction(alertFunction);
  if (!global.property(QLatin1String("alert")).isValid())
    global.setProperty(QLatin1String("alert"), alertFunc);

  QScriptValue debugFunc = newFunction(debugFunction);
  if (!global.property(QLatin1String("debug")).isValid())
    global.setProperty(QLatin1String("debug"), debugFunc);

  if (!global.property(QLatin1String("echo")).isValid())
    global.setProperty(QLatin1String("echo"), debugFunc);

  QScriptValue loaduiFunc = newFunction(loaduiFunction);
  if (!global.property(QLatin1String("loadui")).isValid())
    global.setProperty(QLatin1String("loadui"), loaduiFunc);

  QScriptValue includeFunc = newFunction(includeFunction);
  if (!global.property(QLatin1String("include")).isValid())
    global.setProperty(QLatin1String("include"), includeFunc);

  QScriptValue expandFunc = newFunction(ExpandFormatStringFunction);
  if (!global.property(QLatin1String("ExpandFormatString")).isValid())
    global.setProperty(QLatin1String("ExpandFormatString"), expandFunc);

  QScriptValue enumToStringFunc = newFunction(enumToStringFunction);
  if (!global.property(QLatin1String("enumToString")).isValid())
    global.setProperty(QLatin1String("enumToString"), enumToStringFunc);

  QScriptValue stringToEnumFunc = newFunction(stringToEnumFunction);
  if (!global.property(QLatin1String("stringToEnum")).isValid())
    global.setProperty(QLatin1String("stringToEnum"), stringToEnumFunc);

  createEnumerationWrappers(global);
}

//
// Walk the global object and create wrappers for all enums
// 
void ScriptingEngine::createEnumerationWrappers(QScriptValue value)
{
  if (value.isValid())
  {
    QScriptValueIterator it(value);
    while (it.hasNext()) 
    {
      it.next();
      QScriptValue sv = it.value();
      if (sv.isValid() && sv.isQMetaObject())
      {
        const QMetaObject* mo = sv.toQMetaObject();
        for (int i=0; i<mo->enumeratorCount(); i++)
        {
          const QMetaEnum eo = mo->enumerator(i);       
          new EnumWrapper(this, mo, eo);
        }
      }
    }
  }
}

QWidget* ScriptingEngine::loadUI(const QString& uiFileName, QWidget* parent)
{
  QFile uiFile(uiFileName);

  if (uiFile.open(QIODevice::ReadOnly))
  {
    QFileInfo fi(uiFileName);
    mLoader.setWorkingDirectory(fi.path());
    QWidget *ui = mLoader.load(&uiFile, parent);
    uiFile.close();
    return ui;
  }
  else
    return NULL;
}

QWidget* ScriptingEngine::runScriptUI(const QString& scriptFileName, const QString& uiFileName, const QString& constructorName, QWidget* parent)
{
  if (!mDebugger)
    setEnableDebugger(true, false);

  QWidget* uiWidget = NULL;
  QFile fileCommon(":memwiz/Resources/common.js");
  fileCommon.open(QIODevice::ReadOnly);

  QFile fileScript(scriptFileName);
  fileScript.open(QIODevice::ReadOnly);

  QString program = fileScript.readAll() + "\n\n" + fileCommon.readAll();

  if (!mDebugger)
    QApplication::setOverrideCursor(Qt::BusyCursor);

  evaluate(program, scriptFileName);

  if (!mDebugger)
    QApplication::restoreOverrideCursor();

  QFile uiFile(uiFileName);

  if (uiFile.open(QIODevice::ReadOnly))
  {
    QFileInfo fi(uiFileName);
    mLoader.setWorkingDirectory(fi.path());
    QWidget *ui = mLoader.load(&uiFile, parent);
    if (ui)
    {
      uiWidget = ui;

      uiFile.close();

      QScriptValue ctor = evaluate(constructorName, scriptFileName);
      QScriptValue scriptUi = newQObject(ui, QScriptEngine::ScriptOwnership);
      QScriptValue calc = ctor.construct(QScriptValueList() << scriptUi);

      ui->show();

      if (hasUncaughtException())
      {
        QMessageBox::critical(NULL, 
          QString("Script Error: %1").arg(scriptFileName),
          QString("Exception %1 on line %2")
          .arg(uncaughtException().toString())
          .arg(uncaughtExceptionLineNumber()));
      }
    }
    else
    {
      QMessageBox::critical(NULL, "UI Error", 
        QString("Unable to load UI file: %1").arg(uiFileName),
        QString("Script Error: %1").arg(scriptFileName));
    }
  }
  else
  {
    QMessageBox::critical(NULL, "UI Error", 
      QString("Unable to open UI file: %1").arg(uiFileName),
      QString("Script Error: %1").arg(scriptFileName));
  }

  return uiWidget;
}

QScriptValue  ScriptingEngine::run(const QString& scriptFileName)
{
  QFileInfo fi(scriptFileName);

  if (fi.exists())
  {
    if (fi.suffix().toLower() == "mjs")
      return runPlugin(scriptFileName);
    else
      return runScript(scriptFileName);
  }
  else
    qDebug() << "Error: No such script file" << scriptFileName;

  return QScriptValue();
}

QScriptValue ScriptingEngine::runScript(const QString& scriptName)
{
  if (!mDebugger)
    setEnableDebugger(true, false);

  // Import included files
  QFile fileCommon(":memwiz/Resources/common.js");
  fileCommon.open(QIODevice::ReadOnly);

  QString program;

  QString scriptFileName = scriptName;
  QFileInfo fi(scriptName);
  QString suffix = fi.suffix().toLower();

  QFile fileScript(scriptFileName);
  fileScript.open(QIODevice::ReadOnly);
  program = fileScript.readAll() + "\n\n" + fileCommon.readAll();

  if (!mDebugger)
    QApplication::setOverrideCursor(Qt::BusyCursor);

  QScriptValue resultCommon = evaluate(program, scriptFileName);

  if (!mDebugger)
    QApplication::restoreOverrideCursor();

  if (hasUncaughtException())
  {
    qDebug() << "ScriptError:" << scriptFileName << QString("Exception %1 on line %2").arg(uncaughtException().toString()).arg(uncaughtExceptionLineNumber());

    QMessageBox::critical(NULL, 
      QString("Script Error: %1").arg(scriptFileName),
      QString("Exception %1 on line %2")
      .arg(uncaughtException().toString())
      .arg(uncaughtExceptionLineNumber()));
  }

  return resultCommon;
}


QScriptValue ScriptingEngine::runPlugin(const QString& scriptFileName)
{
  QScriptValue retValue;

  // Import included files
  QFile fileCommon(":memwiz/Resources/common.js");
  fileCommon.open(QIODevice::ReadOnly);

  QFileInfo fileScriptUI(scriptFileName);
  QFile fileScript(scriptFileName);
  fileScript.open(QIODevice::ReadOnly);

  QString program = fileScript.readAll() + "\n\n" + fileCommon.readAll();


  QScriptValue pluginObj = evaluate(program, scriptFileName);
  qDebug() << "result" << pluginObj.toString();

  if (pluginObj.isObject())
  {
    QScriptValueList args;
    QScriptValue func = pluginObj.property("initialize");
    if (func.isFunction())
    {
      ScrPlugin* plugin = new ScrPlugin();
      args << newQObject(plugin);

      qDebug() << "calling initialize method";
      QScriptValue retValue = func.call(globalObject(), args);
      qDebug() << "initialize returned" << retValue.toString();
      if (!hasUncaughtException())
      {
        bool abort = false;
        if (plugin->getCtor().length() == 0)
        {
          QMessageBox::critical(NULL, "Plugin Error", "Plugin did not supply a constructor property");
          abort = true;
        }

        QString constructorName = plugin->getCtor();

        QString uiFileName = plugin->getUIFile();
        if (uiFileName.length() > 0)
        {
          QFileInfo fi(uiFileName);
          qDebug() << "uifile" << fi.path() << fi.fileName() << fi.filePath();
          if (fi.path() == ".")
            uiFileName = QString("%1/%2").arg(fileScriptUI.path()).arg(fi.fileName());

          QFile uiFile(uiFileName);
          if (uiFile.open(QIODevice::ReadOnly))
          {
            QFileInfo uiFileInfo(uiFileName);
            mLoader.setWorkingDirectory(uiFileInfo.path());

            QWidget* parent = 0;
            QDockWidget* dockWidget = 0;

            if (plugin->getFlags() == ScrPlugin::PluginWindowDocked)
            {
              QString title = uiFileInfo.baseName();
              if (!plugin->getTitle().isEmpty())
                title = plugin->getTitle();
              dockWidget = new QDockWidget(title, theApp);
              dockWidget->setObjectName(title);
            }

            QDialog* dlg = NULL;
            if (plugin->getDialog())
            {
              qDebug() << "plugin is a dialog";
              dlg = new QDialog(parent);
              parent = dlg;
            }
            else
              qDebug() << "plugin is NOT a dialog";

            QWidget *ui = mLoader.load(&uiFile, parent);
            if (ui)
            {
              uiFile.close();
              QScriptValue ctor = evaluate(constructorName, scriptFileName);
              QScriptValue scriptUi = newQObject(ui, QScriptEngine::ScriptOwnership);

              QScriptValueList ctorArgs;
              if (plugin->getDialog())
              {
                ctorArgs << newQObject(parent);
                ctorArgs << newQObject(dlg);
              }

              ctorArgs << scriptUi;

              QScriptValue pluginCtor = ctor.construct(ctorArgs);
              qDebug() << "constructed" << pluginCtor.toString();

              if (dockWidget)
              {
                qDebug() << "docking plugin" << plugin->getArea();
                dockWidget->setWidget(ui);
                theApp->addDockWidget(plugin->getArea(), dockWidget);
              }
              if (plugin->getDialog())
              {
                int result = dlg->exec();
                if (result == QDialog::Accepted)
                  retValue = QScriptValue(this, true);
                else
                  retValue = QScriptValue(this, false);
                qDebug() << "plugin returned" << retValue.toString();
              }
              else
              {
                ui->show();
                retValue = QScriptValue(this, true);
              }
            }
            else
            {
              QMessageBox::critical(NULL, "Plugin Error", 
                QString("UI Loader failed to load: %1").arg(uiFileName));
            }
          }
          else
          {
            QMessageBox::critical(NULL, "Plugin Error", 
              QString("UI Loader failed to open: %1").arg(uiFileName));
          }
        }
        else
        {
          retValue = evaluate(constructorName, scriptFileName);
          qDebug() << "constructed formless" << retValue.toString();
        }
      }
    }
    else
    {
      QMessageBox::critical(NULL, "Plugin Error", 
        QString("Plugin is missing the initialize() method"));

    }
  }

  if (hasUncaughtException())
  {
    QMessageBox::critical(NULL, 
      QString("Script Error: %1").arg(scriptFileName),
      QString("Exception %1 on line %2")
      .arg(uncaughtException().toString())
      .arg(uncaughtExceptionLineNumber()));
  }

  return retValue;
}

void ScriptingEngine::scriptFinished()
{
  qDebug() << "Script Finished";
  if (mDebugger)
    mEmbeddedDebugger.detach();
}

void ScriptingEngine::selected(const QModelIndex& index)
{
  qDebug() << "index" << index;

  //QScriptDebugger* dbg = mEmbeddedDebugger.getDebugger();
  //QScriptDebuggerCodeWidget* cw = static_cast<QScriptDebuggerCodeWidget*>(dbg->codeWidget());
  //QScriptDebuggerScriptsWidget* sw = static_cast<QScriptDebuggerScriptsWidget*>(dbg->scriptsWidget());
  //ScriptEdit* te = cw->currentEditor();

  //if (te && mBreakPoints.count() > 0)
  //{
  //  qDebug() << "breakpoints" << te->breakpoints();
  //  te->setBreakpoints(mBreakPoints);
  //}
}

void ScriptingEngine::executionHalted()
{
  qDebug() << "Halted";

  //QScriptDebugger* dbg = mEmbeddedDebugger.getDebugger();
  //QScriptDebuggerCodeWidget* cw = static_cast<QScriptDebuggerCodeWidget*>(dbg->codeWidget());
  //QScriptDebuggerScriptsWidget* sw = static_cast<QScriptDebuggerScriptsWidget*>(dbg->scriptsWidget());

  //if (sw)
  //{
  //  qDebug() << "Scripts Widget";
  //  CQT_CONNECT(sw,  selected(const QModelIndex &), this, selected(const QModelIndex &));
  //
  //  ScriptEdit* te = cw->currentEditor();
  //  if (te && mBreakPoints.count() > 0)
  //  {
  //   qDebug() << "breakpoints" << te->breakpoints();
  //   te->setBreakpoints(mBreakPoints);
  //  }
  //}
}

ScriptingEngine::ScriptingEngine()
{
  mDebugger = false;
  mDebugStopAtFirstLine = false;

  addBuiltinFunctions();
  mAgent = new ScriptAgent(this);
  setAgent(mAgent);
  CQT_CONNECT(mAgent, scriptFinished(), this, scriptFinished());

  importExtension("qt.core");
  importExtension("qt.gui");
  importExtension("qt.xml");
  importExtension("qt.network");
  importExtension("qt.uitools");
}

ScriptingEngine::~ScriptingEngine()
{
}

static QScriptValue enumToStringFunction(QScriptContext *ctx, QScriptEngine *eng)
{
  QScriptValue value;

  if (ctx->argumentCount() == 2)
  {
    // Enum name must ProjEnum::EnumName
    QString enumFullName = ctx->argument(0).toString();
    int enumValue = ctx->argument(1).toUInt32();
    int colons = enumFullName.indexOf("::");

    if (enumFullName.contains("::"))
    {
      QString className = enumFullName.left(colons);
      QString enumName = enumFullName.mid(colons+2);

      const QMetaObject* mo = JavaScriptAPIs::lookupClass(className);
      if (mo)
      {
        UtString n;
        n << enumName;
        int enumIndex = mo->indexOfEnumerator(n.c_str());
        if (enumIndex != -1)
        {
          QString memName = mo->enumerator(enumIndex).key(enumValue);
          return QScriptValue(eng, 
            QString("%1.%2").arg(enumName).arg(memName));
        }
        else
          ctx->throwError(QScriptContext::SyntaxError, 
          QString("enumToString Unknown enum member name: %1 (%2)").arg(enumName).arg(enumFullName));
      }
      else
        ctx->throwError(QScriptContext::SyntaxError, 
        QString("enumToString Unknown class name: %1 (%2)").arg(className).arg(enumFullName));
    }
    else
      ctx->throwError(QScriptContext::SyntaxError,
      QString("enumToString expects ClassName::EnumName, not (%1)").arg(enumFullName));
  }
  else
  {
    ctx->throwError(QScriptContext::SyntaxError, "enumToString requires 2 arguments");
  }
  return value;
}

static QScriptValue stringToEnumFunction(QScriptContext *ctx, QScriptEngine *eng)
{
  QScriptValue value;

  if (ctx->argumentCount() == 2)
  {
    // Enum name must ProjEnum::EnumName
    QString enumFullName = ctx->argument(0).toString();
    QString enumValue = ctx->argument(1).toString();
    if (enumValue.contains("."))
      enumValue = enumValue.mid(enumValue.indexOf('.')+1);

    int colons = enumFullName.indexOf("::");

    if (enumFullName.contains("::"))
    {
      QString className = enumFullName.left(colons);
      QString enumName = enumFullName.mid(colons+2);

      const QMetaObject* mo = JavaScriptAPIs::lookupClass(className);
      if (mo)
      {
        UtString n;
        n << enumName;
        int enumIndex = mo->indexOfEnumerator(n.c_str());
        if (enumIndex != -1)
        {
          UtString kv;
          kv << enumValue;
          int enumVal = mo->enumerator(enumIndex).keyToValue(kv.c_str());
          return QScriptValue(eng, enumVal);
        }
        else
          ctx->throwError(QScriptContext::SyntaxError, 
          QString("stringToEnum Unknown enum member name: %1 (%2)").arg(enumName).arg(enumFullName));
      }
      else
        ctx->throwError(QScriptContext::SyntaxError, 
        QString("stringToEnum Unknown class name: %1 (%2)").arg(className).arg(enumFullName));
    }
    else
      ctx->throwError(QScriptContext::SyntaxError,
      QString("stringToEnum expects ClassName::EnumName, not (%1)").arg(enumFullName));
  }
  else
  {
    ctx->throwError(QScriptContext::SyntaxError, "stringToEnum requires 2 arguments");
  }
  return value;
}


static QScriptValue includeFunction(QScriptContext *ctx, QScriptEngine *eng)
{
  QScriptValue value;

  if (ctx->argumentCount() > 0)
  {
    for (int i=0; i<ctx->argumentCount(); i++)
    {
      QString includeFile = ctx->argument(i).toString();
      UtString expandedFileName, err;
      UtString uIncludeFile; uIncludeFile << includeFile;
      if (!OSExpandFilename(&expandedFileName, uIncludeFile.c_str(), &err)) {
        QString msg = err.c_str();
        QString errMsg = QString("Unable to expand include file: %1, err: %2").arg(includeFile, msg);
        ctx->throwError(QScriptContext::URIError, errMsg);
      }

      QString expandedName = expandedFileName.c_str();
      QFileInfo fi(expandedName);

      qDebug() << "Script is including" << includeFile << "expanded" << expandedName;

      QFile file( expandedName );
      if ( file.open( QIODevice::ReadOnly | QIODevice::Text))
      {
        QString fileContents = file.readAll();

        ctx->setActivationObject(ctx->parentContext()->activationObject());
        ctx->setThisObject(ctx->parentContext()->thisObject());
        value = eng->evaluate( fileContents, expandedName );
      }
      else
      {
        QString errMsg = QString("Unable to read include file: %1, Error: %2").arg(expandedName).arg(file.error());
        ctx->throwError(QScriptContext::URIError, errMsg);
      }
    }
  }

  return value;
}

static QScriptValue alertFunction(QScriptContext *ctx, QScriptEngine *eng)
{
  // if numargs = 1
  QString title = MODELSTUDIO_TITLE;
  QString message;

  int nArgs = ctx->argumentCount();

  if (nArgs == 1)
    message = ctx->argument(0).toString();
  else if (nArgs == 2)
  {
    title = ctx->argument(0).toString();
    message = ctx->argument(1).toString();
  }

  qDebug() << "alert" << title << message;

  DlgMessageText dlg(NULL);
  dlg.setMessage(message);
  dlg.setTitle(title);
  dlg.exec();

  return eng->undefinedValue();
}

static QScriptValue debugFunction(QScriptContext *ctx, QScriptEngine *eng)
{
  QString outputLine;
  for (int i=0; i<ctx->argumentCount(); i++)
  {
    QScriptValue arg = ctx->argument(i);
    if (!outputLine.isEmpty())
      outputLine += " ";
    outputLine += arg.toString();
  }

  qDebug() << qPrintable(outputLine);

  UtString s;
  s << outputLine;

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  pw->getConsole()->processOutput(CarbonConsole::Local, CarbonConsole::Compilation, s.c_str());

  return eng->undefinedValue();
}

static QScriptValue loaduiFunction(QScriptContext *ctx, QScriptEngine *eng)
{
  QScriptValue uiFileName = ctx->argument(0);
  qDebug() << "debug: " << uiFileName.toString();

  QUiLoader loader;
  QFile uiFile(uiFileName.toString());

  if (uiFile.open(QIODevice::ReadOnly))
  {
    QFileInfo fi(uiFileName.toString());
    loader.setWorkingDirectory(fi.path());
    QWidget *ui = loader.load(&uiFile);
    uiFile.close();
    if (ui)
    {
      QScriptValue scriptUi = eng->newQObject(ui);
      ui->show();
      return scriptUi;
    }
    else
    {
      qDebug() << "error failed to load ui file" << uiFileName.toString();
      QMessageBox::critical(NULL, "UI Error", QString("Unable to load UI file: %1").arg(uiFileName.toString()));
    }
  }
  else
  {
    qDebug() << "error failed to open ui file" << uiFileName.toString();
    QMessageBox::critical(NULL, "UI Error", QString("Unable to open UI file: %1").arg(uiFileName.toString()));
  }
  return QScriptValue();
}

QScriptValue ScriptingEngine::callOptionalScriptFunction(const QString& methodName, const QScriptValueList args)
{
  QScriptValue retValue(this, true);

  QScriptValue func = globalObject().property(methodName);
  if (func.isFunction())
  {
    qDebug() << "Calling Optional ScriptMethod: " << methodName;
    retValue = func.call(globalObject(), args);
    qDebug() << "Returned From Optional ScriptMethod: " << methodName << " returnValue: " << retValue.toString();

    if (hasUncaughtException())
    {
      QMessageBox::critical(NULL, "Script Error", 
        tr("Exception %1 on line %2")
        .arg(uncaughtException().toString())
        .arg(uncaughtExceptionLineNumber()));
    }
  }

  return retValue;
}

QScriptValue ScriptingEngine::callScriptFunction(const QString& methodName, const QScriptValueList args)
{
  QScriptValue retValue(this, false);

  QScriptValue func = globalObject().property(methodName);
  if (func.isFunction())
  {
    qDebug() << "Calling ScriptMethod: " << methodName;
    retValue = func.call(globalObject(), args);
    qDebug() << "Returned From ScriptMethod: " << methodName << " returnValue: " << retValue.toString();

    if (hasUncaughtException())
    {
      QMessageBox::critical(NULL, "Script Error", 
        tr("Exception %1 on line %2")
        .arg(uncaughtException().toString())
        .arg(uncaughtExceptionLineNumber()));
    }
  }
  else
  {
    QMessageBox::warning(NULL, "Bad Method", tr("No such method: %1").arg(methodName));
  }
  return retValue;
}


void ScriptAgent::exceptionThrow(qint64 scriptId, const QScriptValue &exception, bool hasHandler)
{
  qDebug() << "exceptionThrow" << "file: " << mFileName << scriptId << exception.toString() << hasHandler << engine()->hasUncaughtException();
  if (engine()->hasUncaughtException())
  {
    QString bt = engine()->uncaughtExceptionBacktrace().join("\n");
    QString ex = exception.toString();
    int ln = engine()->uncaughtExceptionLineNumber();
    if (ln != -1)
      mLineNumber = ln;
    QString errMsg = QString("Filename: %1, line %2\n%3")
      .arg(mScriptFiles[scriptId])
      .arg(mLineNumber)
      .arg(ex);
    qDebug() << "scriptingError" << errMsg << ex << bt;
    if (!hasHandler)
    {
      if ((theApp->checkArgument("-batch") || theApp->checkArgument("-modelKit")))
      {
        qDebug() << "forcing shutdown with exitStatus=1 due to script exception";
        theApp->shutdown(1);
      }

      QMessageBox::information(NULL, "Script Error", errMsg);
      emit scriptException(exception, engine()->uncaughtExceptionLineNumber());
      engine()->clearExceptions();
    }
  }
  else
  {
    if (!hasHandler)
    {
      emit scriptException(exception, mLineNumber);
      engine()->clearExceptions();
    }
  }
}
