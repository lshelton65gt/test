//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include "RegisterEditorWidget.h"
#include "CarbonProject.h"
#include "CarbonProjectWidget.h"
#include "CarbonMakerContext.h"
#include "CarbonDatabaseContext.h"
#include "FieldsGraphic.h"

#include "SpiritXML.h"

void RegisterEditorWidget::closeEvent(QCloseEvent *event)
{
  if (maybeSave())
  {
    event->accept();
    MDIWidget::closeEvent(this);
  }
  else
  {
    event->ignore();
  }
}

bool RegisterEditorWidget::maybeSave()
{
  if (isWindowModified()) {
    QMessageBox::StandardButton ret;
    ret = QMessageBox::warning(this, MODELSTUDIO_TITLE,
      tr("'%1' has been modified.\n"
      "Do you want to save your changes?")
      .arg(userFriendlyCurrentFile()),
      QMessageBox::Save | QMessageBox::Discard
      | QMessageBox::Cancel);
    if (ret == QMessageBox::Save)
    {
      saveDocument();
      return true;
    }
    else if (ret == QMessageBox::Cancel)
      return false;
  }
  return true;
}
RegisterEditorWidget::RegisterEditorWidget(CarbonMakerContext* ctx, MDIDocumentTemplate* t, QWidget *parent) : QWidget(parent)
{
  setAttribute(Qt::WA_DeleteOnClose);

  ui.setupUi(this);

  mSpiritXML = NULL;
  initializeMDI(this, t);
  mCtx = ctx;

  setWindowTitle("Registers[*]");

  setupRegisterTree();
  setupFieldsTree();

}

RegisterEditorWidget::~RegisterEditorWidget()
{
  if (mSpiritXML)
    delete mSpiritXML;
  mSpiritXML = NULL;
}


void RegisterEditorWidget::setupRegisterTree()
{
  QRegisterTreeWidget* tree = ui.treeWidgetRegisters;

  tree->setContext(mCtx, this);
  tree->setEditTriggers(QAbstractItemView::AllEditTriggers);
  tree->setSelectionMode(QAbstractItemView::SingleSelection);
  tree->setSelectionBehavior(QAbstractItemView::SelectRows);
  tree->setRootIsDecorated(false);
  tree->setColumnCount(colRegLAST);
  
  //tree->setItemDelegate(new CarbonModelDelegate(this, this));

  QStringList labels;
  labels << "Offset";
  labels << "Name"; 
  labels << "Width"; 
  labels << "Description";
  labels << "Radix";

  tree->setHeaderLabels(labels);
}

void RegisterEditorWidget::on_treeWidgetFields_itemSelectionChanged()
{
  updateFieldButtons();

  foreach (QTreeWidgetItem *item, ui.treeWidgetFields->selectedItems())
  {
    QVariant qv = item->data(FLD_TAG_FIELD, Qt::UserRole);
    if (!qv.isNull())
    {
      SpiritField* field = (SpiritField*)qv.value<void*>();
      ui.graphicsView->highlightField(field);
    }
    break;
  }
}


void RegisterEditorWidget::on_treeWidgetRegisters_itemSelectionChanged()
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  ui.treeWidgetFields->setRegister(NULL);
  ui.treeWidgetFields->clear();

  foreach (QTreeWidgetItem *item, ui.treeWidgetRegisters->selectedItems())
  {
    QVariant qv = item->data(REG_TAG_REGISTER, Qt::UserRole);
    if (!qv.isNull())
    {
      SpiritRegister* reg = (SpiritRegister*)qv.value<void*>();
      ui.treeWidgetFields->setRegister(reg);
      showRegisterFields(reg);
      drawRegister(reg);
      break;
    }
  } 

  QApplication::restoreOverrideCursor();
}


void RegisterEditorWidget::createField(SpiritField* field)
{
  QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidgetFields);
  item->setFlags(item->flags() | Qt::ItemIsEditable);
  item->setText(colFieldNAME, field->getName());
  SpiritBitOffset* offset = field->getBitOffset();
  QVariant bitoffset(offset->getBitOffset());
  item->setText(colFieldBITOFFSET, bitoffset.toString());
  item->setText(colFieldNOTES, field->getDescription());
  QVariant vwidth(field->getBitWidth());
  item->setText(colFieldBITS, vwidth.toString());
  item->setText(colFieldACCESS, accessToString(field->getAccess()));
  item->setText(colFieldRTL, field->getRTLPath());
    
  if (field->getLocation() == SpiritEnum::LocationArray)
  {
    UtString memIndex;
    memIndex << field->getMemoryIndex();
    item->setText(colFieldMEMORYINDEX, memIndex.c_str());
  }

  UtString partSelect;
  if (field->getHasRange())
  {
    if (field->getPartSelectLeft() != field->getPartSelectRight())
      partSelect << "[" << field->getPartSelectLeft() << ":" << field->getPartSelectRight() << "]";
    else
      partSelect << "[" << field->getPartSelectLeft() << "]";
    item->setText(colFieldPARTSELECT, partSelect.c_str());
  }
   // Data tags
  QVariant qv = qVariantFromValue((void*)field);
  item->setData(FLD_TAG_FIELD, Qt::UserRole, qv);

  QVariant qv1 = qVariantFromValue((void*)field->getRegister());
  item->setData(FLD_TAG_REGISTER, Qt::UserRole, qv1);
}

void RegisterEditorWidget::showRegisterFields(SpiritRegister* reg)
{
  QRegisterFieldsTreeWidget* tree = ui.treeWidgetFields;
  tree->clear();

  //SpiritAddressBlock* ab = reg->getAddressBlock();

  CarbonDB* db = tree->getDB();

  for (UInt32 i=0; i<reg->numFields(); i++)
  {
    SpiritField* field = reg->getField(i);
    if (!field->getRTLPath().isEmpty())
    {
      UtString rtl;
      rtl << field->getRTLPath();
      field->setCarbonNode(carbonDBFindNode(db, rtl.c_str()));
    }
    createField(field);
  }

  resizeFields();
}

void RegisterEditorWidget::resizeFields()
{
  QRegisterFieldsTreeWidget* tree = ui.treeWidgetFields;

  QString errMsg;
  tree->updateFieldColors(errMsg);

  if (!errMsg.isEmpty())
    tree->showStatus(errMsg);
  else
    tree->clearStatus();

  tree->sortItems(colFieldBITOFFSET, Qt::AscendingOrder);

  tree->header()->setResizeMode(0, QHeaderView::Interactive);
  tree->header()->setResizeMode(0, QHeaderView::Stretch);
  tree->header()->resizeSections(QHeaderView::ResizeToContents);
  tree->header()->setResizeMode(0, QHeaderView::Interactive);
}

void RegisterEditorWidget::setupFieldsTree()
{
  QRegisterFieldsTreeWidget* tree = ui.treeWidgetFields;
  
  tree->setItemDelegate(new RegisterFieldsDelegate(this, tree, this));

  tree->setTextBrowser(ui.textBrowser);

  tree->setContext(mCtx, this);
  tree->setEditTriggers(QAbstractItemView::AllEditTriggers);
  tree->setSelectionMode(QAbstractItemView::SingleSelection);
  tree->setSelectionBehavior(QAbstractItemView::SelectRows);
  tree->setRootIsDecorated(false);
  tree->setColumnCount(colFieldLAST);
  tree->setAcceptDrops(true);
  tree->setDragDropMode(QAbstractItemView::DropOnly);
  
  //tree->setItemDelegate(new CarbonModelDelegate(this, this));

  QStringList labels;
  labels << "Name"; 
  labels << "Bit Offset";
  labels << "Bits"; 
  labels << "Access";
  labels << "RTL";
  labels << "PartSelect";
  labels << "Memory Index";
  labels << "Notes";

  tree->setHeaderLabels(labels);
}

void RegisterEditorWidget::populate(SpiritAddressBlock* ab)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  QTreeWidget* tree = ui.treeWidgetRegisters;
  tree->clear();
  ui.treeWidgetFields->clear();

  if (ab)
  {
    qDebug() << ab->numRegisters();
    for (UInt32 i=0; i<ab->numRegisters(); i++)
    {
      SpiritRegister* reg = ab->getRegister(i);
      QTreeWidgetItem* item = new QTreeWidgetItem(tree);

      UtString hexOffset;
      UtOStringStream ss(&hexOffset);
      ss << "0x" << UtIO::hex << reg->getAddressOffset();
      item->setText(colRegOFFSET, hexOffset.c_str());
      item->setText(colRegNAME, reg->getName());
      item->setText(colRegDESCRIPTION, reg->getDescription());
      QVariant vwidth(reg->getSize());
      item->setText(colRegWIDTH, vwidth.toString());

      // Data tags
      QVariant qv = qVariantFromValue((void*)reg);
      item->setData(REG_TAG_REGISTER, Qt::UserRole, qv);
    }
  }

  tree->sortItems(colRegOFFSET, Qt::AscendingOrder);

  tree->header()->setResizeMode(0, QHeaderView::Interactive);
  tree->header()->setResizeMode(0, QHeaderView::Stretch);
  tree->header()->resizeSections(QHeaderView::ResizeToContents);
  tree->header()->setResizeMode(0, QHeaderView::Interactive);

  QApplication::restoreOverrideCursor();
}

void RegisterEditorWidget::updateFieldButtons()
{
  QTreeWidget* tree = ui.treeWidgetFields;
  int selectedFields = tree->selectedItems().count();
  ui.btnDeleteField->setEnabled(selectedFields > 0);
  ui.btnBindConstant->setEnabled(selectedFields > 0);
}

void RegisterEditorWidget::drawRegister(SpiritRegister* reg)
{
  ui.graphicsView->drawRegister(reg);
}

void RegisterEditorWidget::updateRegisterButtons()
{
}

void RegisterEditorWidget::on_btnAddField_clicked()
{
}

void RegisterEditorWidget::on_btnDeleteField_clicked()
{
  ui.treeWidgetFields->deleteSelectedField();
  updateFieldButtons();
}

void RegisterEditorWidget::on_btnBindConstant_clicked()
{
}

void RegisterEditorWidget::on_btnAdd_clicked()
{
}

void RegisterEditorWidget::on_btnDelete_clicked()
{
}

bool RegisterEditorWidget::loadFile(const QString& addressBlockPath)
{
  mCurFile.clear(); 
  mCurFile << addressBlockPath;

  CarbonProject* proj = mCtx->getCarbonProjectWidget()->project();
  // Now, re-read for SpiritXML

  if (maybeSave())
  {
    mSpiritXML = new SpiritXML();
    mSpiritXML->parseSpirtXML(proj->projectFile(), true);

    SpiritAddressBlock* ab = mSpiritXML->findAddressBlock(addressBlockPath);

    populate(ab);
  
    return true;
  }
  else
    return false;
}

void RegisterEditorWidget::saveDocument()
{
  if (isWindowModified())
  { 
    QString selectedRegName;
    foreach (QTreeWidgetItem* item, ui.treeWidgetRegisters->selectedItems())
    {
      selectedRegName = item->text(colRegNAME);
      break;
    }

    // first clear selections (commits edits)
    ui.treeWidgetRegisters->setCurrentItem(NULL);
    ui.treeWidgetFields->setCurrentItem(NULL);

   // Transfer ownership to the Project and create a new one.
    CarbonProject* proj = mCtx->getCarbonProjectWidget()->project();
    proj->setSpiritXML(mSpiritXML);
    proj->save();

    // Rebuild a new version from the persisted one.
    mSpiritXML = new SpiritXML();
    mSpiritXML->parseSpirtXML(proj->projectFile(), true);
    QString addressBlockPath = mCurFile.c_str();
    SpiritAddressBlock* ab = mSpiritXML->findAddressBlock(addressBlockPath);
    
    populate(ab);

    // Re-select regiser
    if (!selectedRegName.isEmpty())
    {
      for (int i=0; i<ui.treeWidgetRegisters->topLevelItemCount(); i++)
      {
        QTreeWidgetItem* item = ui.treeWidgetRegisters->topLevelItem(i);
        QString name = item->text(colRegNAME);
        if (name == selectedRegName)
        {
          ui.treeWidgetRegisters->setCurrentItem(item);
          break;
        }
      }
    }
    setWindowModified(false);
    setWidgetModified(false);
  }
  else
    qDebug() << "nothing to save in RegisterEditorWidget";
}
const char* RegisterEditorWidget::userFriendlyName()
{
  return mCurFile.c_str();
}


const char* RegisterEditorWidget::strippedName(const char* fullFileName)
{
  static UtString x;
  x.clear();
  x << QFileInfo(fullFileName).fileName();
  return x.c_str();
}

const char* RegisterEditorWidget::userFriendlyCurrentFile()
{
  return "Registers";
}
