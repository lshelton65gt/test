//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "CarbonProjectNodes.h"
#include "CarbonMakerContext.h"
#include "Mdi.h"
#include "DlgConfirmDelete.h"
#include "CarbonProperties.h"
#include "CarbonOptions.h"
#include "CarbonSourceGroups.h"
#include "DlgConfirmDelete.h"
#include "CarbonHDLSourceFile.h"
#include "gui/CQt.h"
#include "util/OSWrapper.h"


bool CarbonHDLFolderItem::canDropItem(CarbonProjectTreeNode* node)
{
  CarbonSourceItem* srcItem = dynamic_cast<CarbonSourceItem*>(node);
  return (srcItem != NULL);
}

bool CarbonHDLFolderItem::dropItem(CarbonProjectTreeNode* node)
{
  CarbonSourceItem* srcItem = dynamic_cast<CarbonSourceItem*>(node);
  INFO_ASSERT(srcItem, "Expecting a Source Item");
  mProjectWidget->moveSourceItem(this, srcItem);
  return true;
}

bool CarbonRTLFolder::canDropItem(CarbonProjectTreeNode*)
{
  return false;
}

void CarbonSourceItem::setData(const char* title, const char* file)
{
  setText(0, title);
  mFilePath = file;
}

bool CarbonSourceItem::canDropItem(CarbonProjectTreeNode*)
{
  return false;
}

void CarbonSourceItem::singleClicked(int)
{
  mProjectWidget->fileProperties(this);
}

bool CarbonSourceItem::dragBeginEvent(QMouseEvent*)
{
  return true;
}

void CarbonSourceItem::librarySource()
{
  bool newValue = !mSource->isLibraryFile();
  mSource->putLibraryFile(newValue);
  if (newValue)
    setIcon(0, QIcon(":/cmm/Resources/libraryFile.png"));
  else
    setIcon(0, QIcon(":/cmm/Resources/file-16x16.png"));
}

void CarbonSourceItem::deleteSource()
{
  QFileInfo fi(mFilePath.c_str());

  UtString delMsg;
  delMsg << "'" << fi.fileName() << "'";

  DlgConfirmDelete dlg(treeWidget(), delMsg.c_str(), delMsg.c_str());
  DlgConfirmDelete::ConfirmResult result = (DlgConfirmDelete::ConfirmResult)dlg.exec();
  switch (result)
  {
  case DlgConfirmDelete::Delete:
    {
      QFile f(mFilePath.c_str());
      f.remove();
      mProjectWidget->removeSource(this, mSource);
      qDebug() << "Delete this source: " << mFilePath.c_str();
    break;
    }
  case DlgConfirmDelete::Remove:
    {
      mProjectWidget->removeSource(this, mSource);
      qDebug() << "Remove this source: " << mFilePath.c_str();
      break;
    }
  case DlgConfirmDelete::Cancel:
    break;
  }
}

void CarbonSourceItem::keyPressEvent(QKeyEvent* ev)
{
  if (ev->key() == Qt::Key_Delete)
    deleteSource();
}

void CarbonSourceItem::doubleClicked(int)
{
  mProjectWidget->openSource(mFilePath.c_str());
}

void CarbonHDLFolderItem::deleteFolder()
{
  mGroup->getGroups()->remove(mGroup);
  int childIndex = QTreeWidgetItem::parent()->indexOfChild(this);
  QTreeWidgetItem* removedItem = QTreeWidgetItem::parent()->takeChild(childIndex);
  delete removedItem;
}

void CarbonHDLFolderItem::showContextMenu(const QPoint& point)
{
  QMenu menu;
  if (mGroup->isLibrary())
  {
    menu.addAction(actionCompile);
    menu.addAction(actionClean);     
  }
  menu.addAction(actionDelete);
  menu.addSeparator();
  menu.addActions(mProjectWidget->getContextMenuRoot()->actions());

  menu.setEnabled(!mProjectWidget->isCompilationInProgress());
  
  menu.exec(point);
}

CarbonHDLFolderItem::CarbonHDLFolderItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, CarbonSourceGroup* group) : CarbonProjectTreeNode(proj, parent)
{
  mGroup=group;
  setIcon(0, QIcon(":/cmm/Resources/folder-open-16x16.png"));

  actionDelete = new QAction(QIcon(":/cmm/Resources/DeleteHS.png"), "&Remove Library", this);
  actionDelete->setStatusTip("Remove this compilation folder");
  CQT_CONNECT(actionDelete, triggered(), this, deleteFolder());

  actionCompile = new QAction(QIcon(":/cmm/Resources/compile.png"), "&Compile Library", this);
  actionCompile->setStatusTip("Compile this library and any dependent libraries");
  CQT_CONNECT(actionCompile, triggered(), this, compileLibrary());

  actionClean = new QAction(QIcon(":/cmm/Resources/compile.png"), "&Clean Library", this);
  actionClean->setStatusTip("Clean this library");
  CQT_CONNECT(actionClean, triggered(), this, cleanLibrary());
}

void CarbonHDLFolderItem::cleanLibrary()
{
  CarbonConfiguration* cfg = mProjectWidget->project()->getActive();
  qDebug() << "Clean: " << mGroup->getTargetName(cfg);

  UtString argValue;
  argValue << "CLEANLIB_NAME='" << mGroup->getTargetDirectory(cfg) << "'";

  QStringList args;
  args << argValue.c_str();

  mProjectWidget->compile(true, false, false, NULL, "cleanlib", &args);
}

void CarbonHDLFolderItem::compileLibrary()
{
  CarbonConfiguration* cfg = mProjectWidget->project()->getActive();

  qDebug() << "Compile: " << mGroup->getTargetName(cfg);
  mProjectWidget->compile(false, true, mGroup->getTargetName(cfg));
}

CarbonHDLFolderItem::~CarbonHDLFolderItem()
{
}

void CarbonHDLFolderItem::singleClicked(int)
{
  mProjectWidget->folderProperties(this);
}

CarbonOptions* CarbonHDLFolderItem::getOptions()
{
  return mGroup->getOptions();
}

