#ifndef COLORIZEDEDITOR_H
#define COLORIZEDEDITOR_H
#include "util/CarbonVersion.h"
#include "gui/CQt.h"


#include <Qsci/qsciscintilla.h>
#include <Qsci/qscilexervhdl.h>

class ColorizedEditor : public QsciScintilla
{
  Q_OBJECT

  Q_PROPERTY(QString text READ getText)

public:
  enum LanguageKind { Verilog, VHDL };

  QString getText() { return text(); }

  ColorizedEditor(QWidget *parent);
  ~ColorizedEditor();

  void setLanguage(LanguageKind language);

private:
  LanguageKind mLanguage;
};

#endif // COLORIZEDEDITOR_H
