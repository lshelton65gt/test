#ifndef __DlgCopyConfiguration_H
#define __DlgCopyConfiguration_H
#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include <QDialog>
#include "ui_DlgCopyConfiguration.h"

class CarbonProjectWidget;

class DlgCopyConfiguration : public QDialog
{
  Q_OBJECT

public:
  DlgCopyConfiguration(QWidget *parent = 0);
  ~DlgCopyConfiguration();
  void setProject(CarbonProjectWidget* proj);

  const char* getConfigurationName() const { return mConfigName.c_str(); }
  const char* getCopyFromName() const { return mCopyFrom.c_str(); }

private slots:
  void on_okButton_clicked();
  void on_cancelButton_clicked();

private:
  Ui::DlgCopyConfigurationClass ui;
  CarbonProjectWidget* mProject;

  UtString mConfigName;
  UtString mCopyFrom;
};

#endif // DlgCopyConfiguration_H
