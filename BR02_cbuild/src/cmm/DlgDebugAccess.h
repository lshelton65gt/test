#ifndef DLGDEBUGACCESS_H
#define DLGDEBUGACCESS_H

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "util/OSWrapper.h"

#include <QDialog>
#include "ui_DlgDebugAccess.h"

#include "cfg/CarbonCfg.h"

//
// DlgDebugAccess dlg();
// dlg.setProtocolInstance(items);
// if (dlg.exec() == QDialog::Accepted)
// {
//    QString value = dlg.protocolInstance();
// }
class DlgDebugAccess : public QDialog
{
  Q_OBJECT

public:
  DlgDebugAccess(QWidget *parent = 0);
  ~DlgDebugAccess();
  void setProtocolInstances(const QStringList& items);

  QString protocolInstance();

private:
  Ui::DlgDebugAccessClass ui;

private slots:
    void on_buttonBox_rejected();
    void on_buttonBox_accepted();
};

#endif // DLGDEBUGACCESS_H
