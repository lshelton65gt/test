// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _CarbonTraceWidget_h_
#define _CarbonTraceWidget_h_

#include "util/CarbonPlatform.h"
#include "gui/CDragDropTreeWidget.h"
#include "shell/ReplaySystem.h"
#include "shell/OnDemandTraceFileReader.h"
#include "Mdi.h"
#include "OnDemandTuningWidget.h"

#include <QtGui>

class OnDemandTraceWidget : public OnDemandTuningWidget, public MDIWidget
{
  Q_OBJECT

public:
  OnDemandTraceWidget(MDIDocumentTemplate *tplate, QWidget *parent,
                      CarbonMakerContext* ctx);
  virtual ~OnDemandTraceWidget();

  bool open(const char *fname);
  virtual void closeEvent(QCloseEvent *ev) { ev->accept(); MDIWidget::closeEvent(this); }
  virtual const char* userFriendlyName() { return mTraceFileName.c_str(); }

private slots:
  void readTraceFile();

private:
  static void reader(void *userData,
                     CarbonUInt64 schedCalls,
                     CarbonTime simTime,
                     CarbonOnDemandDebugType type,
                     CarbonOnDemandDebugAction action,
                     const char *path);

  void resetTraceReader(const char *name);

  OnDemandTraceFileReader  *mTraceReader;
  UtString                  mTraceFileName;
  SInt64                    mTraceFileSize;
  QTimer                   *mTraceTimer;
  CarbonMakerContext*       mCarbonMakerContext;
};

#endif

