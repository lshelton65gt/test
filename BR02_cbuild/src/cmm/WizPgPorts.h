#ifndef WizPgPorts_H
#define WizPgPorts_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QWizardPage>
#include "WizardPage.h"
#include "ui_WizPgPorts.h"

class WizardContext;
class Mode;
class PortInstance;

class WizPgPorts : public WizardPage
{
  Q_OBJECT

public:

  enum { colPORT, colUSERPORT, colLAST };

  WizPgPorts(QWidget *parent = 0);
  ~WizPgPorts();
  virtual void initializePage();
  virtual bool validatePage();
  virtual void cleanupPage();

  int nextId() const;
  bool isComplete () const;
  void setVisible(bool visible);
  QString mappedName(const QString& portInstanceName);

private slots:
  void reconfigureButtonClicked();

private:
  virtual bool serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*);

private:
  WizardContext* mContext;
  Mode* mMode;

private slots:
  void on_pushButtonChoose_clicked();
  void on_pushButtonMap_clicked();
  void on_pushButtonRemove_clicked();
  void on_treeWidgetModelPorts_itemSelectionChanged();
  void on_treeWidgetModulePorts_itemSelectionChanged();
  void dropModulePort(const char* name, QTreeWidgetItem* item);
  void dropModelPort(const char* name, QTreeWidgetItem* item);

private:
  void populate();
  void updateButtons();
  void addItem(PortInstance* portInst);

private:
  Ui::WizPgPortsClass ui;
  bool mIsCompleted;
  QMap<QString,QString> mPortMap;
  UtString mModelDragData;
  UtString mModuleDragData;

};

#endif // WizPgPorts_H
