//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "CarbonProjectWidget.h"
#include "CarbonProject.h"
#include "CarbonComponents.h"
#include "CarbonComponent.h"
#include "CowareComponent.h"
#include "MaxsimComponent.h"
#include "SystemCComponent.h"
#include "PackageComponent.h"
#include "MVComponent.h"

CarbonComponents::~CarbonComponents()
{
  for (UInt32 i=0; i<mComponents.size(); ++i)
  {
    CarbonComponent* comp = mComponents[i];
    delete comp;
  }
}

CarbonComponents::CarbonComponents(CarbonProject* proj)
{
  mProject = proj;
}

void CarbonComponents::clearNodes(QTreeWidget* tree, QTreeWidgetItem* parent)
{
  if (parent)
  {
    // First wipe out any children present
    foreach (QTreeWidgetItem* item, parent->takeChildren())
      delete item;
  }
  else // Rooted Items, only delete ComponentTreeNodes
  {
    QList<QTreeWidgetItem*> deleteItems;
    for (int i=0; i<tree->topLevelItemCount(); i++)
    {
      QTreeWidgetItem* item = tree->topLevelItem(i);
      CarbonComponentTreeItem* compTreeItem = dynamic_cast<CarbonComponentTreeItem*>(item);
      if (compTreeItem)
        deleteItems.push_back(item);
    }
    foreach (QTreeWidgetItem* item, deleteItems)
    {
      int index = tree->indexOfTopLevelItem(item);
      tree->takeTopLevelItem(index);
      delete item;
    }
  }
}

// Fill out the component tree from the active components
// underneath parent, if parent is NULL then the items are rooted
// at the top level
void CarbonComponents::populateComponentTree(CarbonProjectWidget* tree, QTreeWidgetItem* parent)
{
  // first blow away any prior nodes in the tree
  clearNodes(tree, parent);

  // build up the list of active componets that need to be 
  // put into the tree
  QList<CarbonComponent*> comps;
  getActiveComponents(&comps);

  foreach (CarbonComponent* comp, comps)
    comp->createTreeItem(tree, parent);
}

// Active components are ones that match the config name and platform
int CarbonComponents::getActiveComponents(QList<CarbonComponent*>* components)
{
  components->clear();

  for (UInt32 i=0; i<mComponents.size(); ++i)
  {
    CarbonComponent* comp = mComponents[i];
    components->push_back(comp);
  }

  return components->count();
}


bool CarbonComponents::deserialize(xmlNodePtr parent, UtXmlErrorHandler* eh)
{
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    const char* elementName = XmlParsing::elementName(child);
    if (elementName == NULL)
      continue;

    CowareComponent* coware = CowareComponent::deserialize(mProject, child, eh);
    if (coware)
    {
      addComponent(coware);
      continue;
    }

    MaxsimComponent* maxsim = MaxsimComponent::deserialize(mProject, child, eh);
    if (maxsim)
    {
      addComponent(maxsim);
      continue;
    }

    SystemCComponent* systemc = SystemCComponent::deserialize(mProject, child, eh);
    if (systemc)
    {
      addComponent(systemc);
      continue;
    }

    MVComponent* mv = MVComponent::deserialize(mProject, child, eh);
    if (mv)
    {
      addComponent(mv);
      continue;
    }
    PackageComponent* package = PackageComponent::deserialize(mProject, child, eh);
    if (package)
    {
      addComponent(package);
      continue;
    }

  }

  return true;
}

bool CarbonComponents::serialize(xmlTextWriterPtr writer)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Components");

  for (UInt32 i=0; i<mComponents.size(); ++i)
  {
    CarbonComponent* comp = mComponents[i];
    comp->serialize(writer);
  }
  xmlTextWriterEndElement(writer);

  return true;
}

CarbonComponent* CarbonComponents::addComponent(const char* name)
{
  CarbonComponent* comp = new CarbonComponent(mProject, name);
  mComponents.push_back(comp);
  return comp;
}

void CarbonComponents::deleteComponent(CarbonComponent* compToDelete)
{
  mComponents.remove(compToDelete);
}

CarbonComponent* CarbonComponents::addComponent(CarbonComponent* comp)
{
  mComponents.push_back(comp);
  return comp;
}

CarbonComponent* CarbonComponents::findComponent(const char* name)
{
  for (UInt32 i=0; i<mComponents.size(); ++i)
  {
    CarbonComponent* comp = mComponents[i];
    if (0 == strcmp(comp->getName(), name))
      return comp;
  }
  return NULL;
}
