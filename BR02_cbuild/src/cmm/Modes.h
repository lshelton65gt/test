#ifndef __CONFIGURATIONOPTIONS_H__
#define __CONFIGURATIONOPTIONS_H__

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "Scripting.h"

class PortConfiguration;
class Mode;
class Template;

class Modes : public QObject
{
  Q_OBJECT

  Q_PROPERTY(int count READ getCount)

public:
  int getCount() const  { return mModes.count(); }
  Modes() {}

  void addMode(Mode* mode)
  {
    mModes.append(mode);
  }

  Mode* getAt(int index)
  {
    return mModes[index];
  }

  bool deserialize(Template* templ, const QDomElement& e, XmlErrorHandler* eh);

public slots:
  QObject* getMode(int index);

private:
  QList<Mode*> mModes;

};

#endif
