//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "MdiMemoryMaps.h"

#include <QtGui>
#include <QMenuBar>
#include <QToolBar>
#include "gui/CQt.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "MemoryMapsWidget.h"

MDIMemoryMapsTemplate::MDIMemoryMapsTemplate(CarbonMakerContext* ctx)
: MDIDocumentTemplate("MemoryMaps", "Memory Maps", "Memory Maps(*.mmaps)") 
{
  mContext = ctx;  
}

// If the user switches windows with the keyboard or menu pick
// update the tab bar to reflect that this is the active window now.
void MDIMemoryMapsTemplate::updateSelectedTab(QWidget* widget)
{
  QVariant qv = widget->property("CarbonTabWidget");
  QVariant qvPlaceholder = widget->property("CarbonTabPlaceholder");

  if (qv.isValid() && qvPlaceholder.isValid())
  {
    QTabWidget* tw = (QTabWidget*)qv.value<void*>();
    QWidget* w = (QWidget*)qvPlaceholder.value<void*>();

    int tabIndex = tw->indexOf(w);
    if (tabIndex != -1)
      tw->setCurrentIndex(tabIndex);
  }
}


void MDIMemoryMapsTemplate::updateMenusAndToolbars(QWidget* widget)
{
  updateSelectedTab(widget);
}

int MDIMemoryMapsTemplate::createToolbars()
{
  return 0;
}

int MDIMemoryMapsTemplate::createMenus()
{
  return 0;
}

MDIWidget* MDIMemoryMapsTemplate::createNewDocument(QWidget* /*parent*/)
{
  return NULL;
}

// We don't have a MDI document presence, only the docking window widget
// so we create the InvisibleProject here to handle menu & toolbar events
//
MDIWidget* MDIMemoryMapsTemplate::openDocument(QWidget* parent, const char* docName)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  MemoryMapsWidget* widget = new MemoryMapsWidget(mContext, this, parent);

  bool status = widget->loadFile(docName);

  QApplication::restoreOverrideCursor();

  if (status)
    return widget;
  else
  {
    widget->close();
    return NULL;
  }
}
