#ifndef __MEMORYMAPS1_H__
#define __MEMORYMAPS1_H__

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "util/UtString.h"
#include "Mdi.h"
#include "MdiProject.h"
#include "CarbonMakerContext.h"

class MDIWidget;

class MDIRegisterEditorTemplate : public MDIDocumentTemplate
{
public:
  MDIRegisterEditorTemplate(CarbonMakerContext* ctx);
 
  virtual MDIWidget* createNewDocument(QWidget* parent=0);
  virtual int createMenus();
  virtual int createToolbars();
  virtual void updateMenusAndToolbars(QWidget* widget);
  virtual MDIWidget* openDocument(QWidget* parent, const char* docName);

private:
  void updateSelectedTab(QWidget* widget);

private:
  CarbonMakerContext* mContext;
};
#endif
