//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "RichOutputWindow.h"
#include <QDebug>

RichOutputWindow::RichOutputWindow(QWidget *parent)
: QTextBrowser(parent)
{
  ui.setupUi(this);

  QString html;

  html = "<html><body><h1>Hello</h1>The HTML body goes here, along with <a href=\"http://www.yahoo.com\">anchors</a></body</html>";

  setHtml(html);

  connect(this, SIGNAL(anchorClicked(QUrl)), this, SLOT(on_anchorClicked(QUrl)));
}
RichOutputWindow::~RichOutputWindow()
{

}

int RichOutputWindow::writeLine(const char*  /*line*/)
{
  return 0;
}

void RichOutputWindow::on_anchorClicked(QUrl url)
{
  QUrl x;
  setSource(x);
  qDebug() << url.toString();
}

