//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "DlgAddWizardPort.h"

DlgAddWizardPort::DlgAddWizardPort(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);
  ui.radioButtonInput->setChecked(true);
  ui.lineEditWidth->setText("1");
  on_radioButtonInput_clicked();
}

DlgAddWizardPort::~DlgAddWizardPort()
{
}

void DlgAddWizardPort::on_radioButtonInput_clicked()
{
  ui.lineEditTie->setEnabled(false);
  ui.labelTie->setEnabled(false);
}

void DlgAddWizardPort::on_radioButtonOutput_clicked()
{
  ui.lineEditTie->setEnabled(true);
  ui.labelTie->setEnabled(true);
}

void DlgAddWizardPort::on_buttonBox_accepted()
{
  mTieValue = ui.lineEditTie->text();
  mPortName = ui.lineEditPortName->text();
  if (ui.radioButtonInput->isChecked())
    mDirection = InputPort;
  else
    mDirection = OutputPort;
  QVariant width(ui.lineEditWidth->text());
  mWidth = width.toInt();
  if (mPortName.length() > 0)
    accept();
}

void DlgAddWizardPort::on_buttonBox_rejected()
{
  reject();
}


