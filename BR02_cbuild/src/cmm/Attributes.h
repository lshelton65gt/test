#ifndef __Attributes_H_
#define __Attributes_H_

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "Scripting.h"

class Attribute;
class Template;

class Attributes : public QObject
{
  Q_OBJECT

  Q_PROPERTY(int count READ getCount)

public:
  int getCount() const  { return mAttributes.count(); }
  Attributes() {}

  Attribute* get(int index)
  {
    return mAttributes[index];
  }
  void addAttribute(Attribute* pa);
  bool deserialize(Template* templ, const QDomElement& e, XmlErrorHandler* eh);

private:

public slots:
  QObject* getAttribute(int index);

private:
  QList<Attribute*> mAttributes;

};

#endif

