#ifndef __PluginWidget__h
#define __PluginWidget__h

#include "util/CarbonPlatform.h"

#include <QtGui>

#include "Mdi.h"
#include "MdiPlugin.h"
#include "CarbonMakerContext.h"
#include "ScriptingEngine.h"

class PluginWidget : public QWidget, public MDIWidget
{
  Q_OBJECT

public:
  PluginWidget(CarbonMakerContext* ctx=0, MDIDocumentTemplate* doct=0, QWidget *parent = 0);
  ~PluginWidget();

  bool loadFile(const QString& pluginName);

private:
  CarbonMakerContext* mCtx;
  ScriptingEngine mEngine;
  QString mScriptFilePath;
  QWidget* mParent;
  QSplitter* mSplitter;
};

#endif
