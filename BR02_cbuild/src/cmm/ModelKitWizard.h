#ifndef MODELKITWIZARD_H
#define MODELKITWIZARD_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QWizard>
#include "ui_ModelKitWizard.h"
#include "Scripting.h"
#include "MKWPageVersions.h"
#include "MKWPageInfo.h"
#include "MKWPageFiles.h"
#include "MKWPageUserActions.h"
#include "MKWPageFinished.h"
#include "CarbonMakerContext.h"
#include "cmm.h"

#define ENV_ARM_ROOT "${ARM_ROOT}"
#define ENV_PROJ_ROOT "${CARBON_PROJECT}"

class KitUserAction;

struct OptionalHashedFile
{
  QString mFileName;
  bool mChecked;
};

struct RTLFileOptions
{
  bool mHashed;
  bool mCopied;
  bool mOptional;
  QString mFileName;

  RTLFileOptions()
  {
    mHashed = true;
    mCopied = true;
    mOptional = false;
  }
};


class ModelKitWizard : public QWizard
{
  Q_OBJECT

public:

  enum { PageVersions, PageInfo, PageFiles, PageUserActions, PageLicensing, PageScripts, PageDatasheet, PageFinished } ;
  enum UserActionWhen { PreCompilation, PostCompilation };

  ModelKitWizard(QWidget *parent = 0);
  ~ModelKitWizard();
  
  bool canOpenWizard(QString& msg);

  QString expandName(const QString& inputName);

  QList<OptionalHashedFile*>& getOptionalHashedFileList() { return mOptHashedFileList; }
  QMap<QString, RTLFileOptions*>& getFileOptionsMap() { return mFileOptionsMap; }
  QStringList& getHashedFileList() { return mHashedFileList; }
  QStringList& getFileList() { return mFileList; }
  QStringList& getUserFileList() { return mUserFileList; }
  void addCheckFile(const QString& fileName, bool value)
  {
    mCheckedFileMap[fileName] = value;
  }
  QMap<QString, bool>& getCheckedFileMap() { return mCheckedFileMap; }
  QString normalizeName(const QString& fileName);
  QString normalizeProjectName(const QString& fileName);
  
  bool getRegenerate() const { return mRegenerate; }

  void setARMRoot(const QString& newVal)
  {
    if (newVal.isEmpty())
      mARMRoot=newVal;
    else
    {
      QString temp(newVal);
      QString av = temp.replace("\\", "/");
      if (!av.endsWith("/"))
        av += "/";
      mARMRoot = av;
    }
  }
  QString getARMRoot()
  {
    QString root = mARMRoot.replace("\\", "/");
    if (!root.isEmpty() && !root.endsWith("/"))
      root += "/";
    return root;
  }

  ModelKit* newModelKit()
  {
    if (mModelKit)
      delete mModelKit;
    mModelKit = new ModelKit();
    return mModelKit;
  }

  void getFilesForFolder(QStringList& fileList, const QString& folderName, const QStringList& fileKinds, bool recurse=true);

  void removeUserActions()
  {
    mPreUserActions.clear();
    mPostUserActions.clear();
  }
  void addPreUserAction(KitUserAction* action) { mPreUserActions.append(action); }
  void addPostUserAction(KitUserAction* action) { mPostUserActions.append(action); }
  const QList<KitUserAction*>& getPreUserActions() { return mPreUserActions; }
  const QList<KitUserAction*>& getPostUserActions() { return mPostUserActions; }
  void setManifestFileName(const QString& newVal) { mManifestFileName=newVal; }
  QString getManifestFileName() const { return mManifestFileName; }

  void setCopyFiles(bool newVal) { mCopyFiles=newVal; }
  bool getCopyFiles() const { return mCopyFiles; }

  void setAskRoot(bool newVal) { mAskRoot=newVal; }
  bool getAskRoot() const { return mAskRoot; }

  ModelKit* getModelKit() { return mModelKit; }
  KitManifest* getActiveManifest() { return mActiveManifest; }
  void setActiveManifest(KitManifest* mf) { mActiveManifest=mf; }
  bool fileExists(const QString& file);

  void closeWizard(int code)
  {
    done(code);
  }

private:
  QStringList mHashedFileList;
  QList<OptionalHashedFile*> mOptHashedFileList;
  QStringList mFileList;
  QStringList mUserFileList;
  QList<KitUserAction*> mPreUserActions;
  QList<KitUserAction*> mPostUserActions;
  QMap<QString, bool> mCheckedFileMap;
  QMap<QString,RTLFileOptions*> mFileOptionsMap;
  QString mManifestFileName;
  Ui::ModelKitWizardClass ui;
  bool mCopyFiles;
  bool mAskRoot;
  bool mRegenerate;
  QString mARMRoot;
  ModelKit* mModelKit;
  KitManifest* mActiveManifest;
};

#endif // MODELKITWIZARD_H
