//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "util/UtHashMap.h"
#include "util/UtHashSet.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"

#include "shell/ShellNetMacroDefine.h"
#include "util/ConstantRange.h"
#include "hdl/HdlVerilogPath.h"
#include "hdl/HdlId.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtStringUtil.h"


#include "RegisterTreeWidgets.h"
#include "RegisterEditorWidget.h"
#include "CarbonProject.h"
#include "CarbonConsole.h"
#include "CarbonProjectWidget.h"
#include "CarbonMakerContext.h"
#include "CarbonDatabaseContext.h"
#include "WorkspaceModelMaker.h"
#include "SpiritXML.h"


void QRegisterTreeWidget::dragEnterEvent(QDragEnterEvent *event)
{
  if (event->mimeData()->hasFormat("text/plain"))
    event->acceptProposedAction();
}
void QRegisterTreeWidget::dropEvent(QDropEvent *event)
{
  event->acceptProposedAction();
}
void QRegisterTreeWidget::dragMoveEvent(QDragMoveEvent* event)
{
  bool ignore = true;
  const QMimeData* mimeData = event->mimeData();
  if (mimeData->hasText())
  {
    event->acceptProposedAction();
    ignore = false;
  }
  if (ignore)
    event->ignore();
}


void QRegisterFieldsTreeWidget::dragLeaveEvent(QDragLeaveEvent*)
{
  clearAnimation();
  clearStatus();
}

void QRegisterFieldsTreeWidget::dragEnterEvent(QDragEnterEvent *event)
{
  bool accept = false;
  QString errMsg;

  if (event->mimeData()->hasFormat("text/plain") && mRegister)
  {
    animateDropSite(event->pos());

    QString rtlPath = event->mimeData()->text();
    UtString rtlpath;
    rtlpath << rtlPath;

    CarbonDB* db = getDB();

    const CarbonDBNode* node = carbonDBFindNode(db, rtlpath.c_str());
    bool isMemory = carbonDBIs2DArray(db, node) && !carbonDBIsContainedByComposite(db, node);
    bool canBeNet = carbonDBCanBeCarbonNet(db, node);

    if (isMemory || canBeNet)
      accept = true;
    else
      errMsg = "Composites are not allowed as register fields.";
  }

  if (accept)
    event->acceptProposedAction();
  else
  {
    event->ignore();
    if (!errMsg.isEmpty())
      QMessageBox::critical(NULL, MODELSTUDIO_TITLE, errMsg);
  }
}
void QRegisterFieldsTreeWidget::dropEvent(QDropEvent *event)
{
  bool accept = false;
  clearStatus();

  clearAnimation();

  QString errMsg;

  if (mRegister)
  {
    QString rtlPath = event->mimeData()->text();
    UtString rtlpath;
    rtlpath << rtlPath;

    qDebug() << "registerField drop" << rtlPath;

    CarbonDB* db = getDB();
    const CarbonDBNode* node = carbonDBFindNode(db, rtlpath.c_str());
    bool isMemory = carbonDBIs2DArray(db, node) && !carbonDBIsContainedByComposite(db, node);
    bool canBeNet = carbonDBCanBeCarbonNet(db, node);
    int unusedBits = mRegister->numUnusedBits();

    if (isMemory || canBeNet)
    {
      QTreeWidgetItem* item = itemAt(event->pos());

      // Dropped on an existing field?
      if (item)
      {
        QVariant qv = item->data(FLD_TAG_FIELD, Qt::UserRole);
        INFO_ASSERT(!qv.isNull(), "Missing Field Tag");
        SpiritField* field = (SpiritField*)qv.value<void*>();
        bool processBinding = true;
        if (!field->getRTLPath().isEmpty())
        {
          if (QMessageBox::warning(this, MODELSTUDIO_TITLE, "Replace existing value?", QMessageBox::Ok|QMessageBox::Cancel) == QMessageBox::Cancel)
            processBinding = false;
        }

        if (processBinding)
        {
          accept = true;
          bindField(item, field, node);
        }
      }
      else // Create a new field
      {      
        if (unusedBits <= 0)
        {
          errMsg = QString("The register is fully populated at %1 bits").arg(mRegister->getSize());
          accept = false;
        }
        else // create a field on the fly
        {
          if (createNewField(node))
            accept = true;
        }
      }
    }
  }

  if (accept)
    event->acceptProposedAction();
  else
  {
    event->ignore();
    if (!errMsg.isEmpty())
      QMessageBox::critical(NULL, MODELSTUDIO_TITLE, errMsg); 
  }
}
bool QRegisterFieldsTreeWidget::createNewField(const CarbonDBNode* node)
{
  bool status = false;

  CarbonDB* db = getDB();
  int nodeBitSize = carbonDBGetWidth(db, node);
  int unusedBits = mRegister->numUnusedBits();
  bool isVector = carbonDBIsVector(db, node);
  // We can create a field if:
  // the node is less or equal than unusedBits
  // or if the node is greater and it can be partselected
  if ( (nodeBitSize <= unusedBits) || ( (nodeBitSize > unusedBits) && isVector) )
  {
    // Default to full size
    int fieldSize = unusedBits;
    if (nodeBitSize <= unusedBits)
      fieldSize = nodeBitSize;

    QString fieldName = mRegister->nextFieldName();
    int nextBitOffset = mRegister->nextBitOffset();

    SpiritField* field = new SpiritField();
    field->setBitWidth(fieldSize);
    field->setRTLPath(carbonDBNodeGetFullName(db, node));
    field->setRegister(mRegister);
    field->setName(fieldName);
    field->setCarbonNode(node);
    field->setAccess(SpiritEnum::ReadWrite);
    SpiritBitOffset* bitOffset = new SpiritBitOffset();
    bitOffset->setBitOffset(nextBitOffset);
    field->setBitOffset(bitOffset);

    if (isVector)
    {
      int lsb = carbonDBGetLSB(db, node);
      field->setHasRange(true);
      field->setPartSelectLeft(lsb+fieldSize-1);
      field->setPartSelectRight(lsb);
    }
    mRegister->addField(field);

    mRegEditor->createField(field);
    mRegEditor->resizeFields();

    qDebug() << "create field";
    status = true;
  }
  else
  {
    showStatus("Unable to create field");
  }

  return status;
}

bool QRegisterFieldsTreeWidget::updateFieldColor(QTreeWidgetItem* item, QString& errMsg)
{
  bool badField = false;
  CarbonDB* db = getDB();

  QVariant qv = item->data(FLD_TAG_FIELD, Qt::UserRole);
  SpiritField* field = (SpiritField*)qv.value<void*>();

  badField = field->checkForProblems(db, errMsg);
 
  // Now, colorize
  if (badField)
  {
    for (int c=0; c<columnCount(); c++)
      item->setBackgroundColor(c, QColor(255,182,193));
  }
  else
  {
    for (int c=0; c<columnCount(); c++)
      item->setBackgroundColor(c, mDefaultBackgroundColor);
  }
  
  return badField;
}


// Go through each field, verify its contents
bool QRegisterFieldsTreeWidget::updateFieldColors(QString& errMsg)
{
  bool badFields = false;
  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);
    if (!updateFieldColor(item, errMsg))
      badFields = true;
  }
  return badFields;
}

void QRegisterFieldsTreeWidget::bindField(QTreeWidgetItem* item, SpiritField* field, const CarbonDBNode* node)
{
  CarbonDB* db = getDB();

  SpiritRegister* reg = field->getRegister();
  SpiritAddressBlock* ab = reg->getAddressBlock();

  const char* rtlFullName = carbonDBNodeGetFullName(db, node);

  int lsb = carbonDBGetLSB(db, node);
  int msb = carbonDBGetMSB(db, node);

  int left; 
  int right;

  // Is the Register Big or little endian?
  if (ab->getEndian() == SpiritEnum::LittleEndian)
  {
    // Is the Carbon Net big or little endian?
    if (msb > lsb)
    {
      left = lsb+field->getBitWidth()-1;
      right = lsb;
    }
    else
    {
      left = msb;
      right = msb+field->getBitWidth()-1;
    }
  }
  else // Big Endian
  {
    // Is the Carbon Net big or little endian?
    if (msb > lsb)
    {
      left = msb;
      right = msb+field->getBitWidth()-1;
    }
    else
    {
      left = lsb+field->getBitWidth()-1;
      right = lsb;
    }
  }

  bool isVector = carbonDBIsVector(db, node);
  //int nodeBitSize = carbonDBGetWidth(db, node);

  if (carbonDBIsScalar(db, node) || isVector)
  {
    field->setPartSelectLeft((UInt32)left);
    field->setPartSelectRight((UInt32)right);
    field->setHasRange(false);
    field->setLocation(SpiritEnum::LocationRegister);
    field->setCarbonNode(node);

    field->setRTLPath(rtlFullName);
    item->setText(RegisterEditorWidget::colFieldRTL, rtlFullName);

    if (isVector)
    {
      field->setHasRange(true);
      UtString partSelect;
      if (left != right)
        partSelect << "[" << left << ":" << right << "]";
      else
        partSelect << "[" << left << "]";
      item->setText(RegisterEditorWidget::colFieldPARTSELECT, partSelect.c_str());
    }
    else
      item->setText(RegisterEditorWidget::colFieldPARTSELECT, "");

    item->setText(RegisterEditorWidget::colFieldMEMORYINDEX, "");

    mRegEditor->setModified(true);
  }
  else if (carbonDBIs2DArray(db, node))
  {
    field->setPartSelectLeft((UInt32)left);
    field->setPartSelectRight((UInt32)right);
    field->setLocation(SpiritEnum::LocationArray);
    field->setHasRange(true);
    field->setMemoryIndex(carbonDBGetArrayLeftBound(db, node));
    field->setCarbonNode(node);

    UtString leftBound;
    leftBound << carbonDBGetArrayLeftBound(db, node);
    item->setText(RegisterEditorWidget::colFieldMEMORYINDEX, leftBound.c_str());

    field->setRTLPath(rtlFullName);
    item->setText(RegisterEditorWidget::colFieldRTL, rtlFullName);

    UtString partSelect;
    if (left != right)
      partSelect << "[" << left << ":" << right << "]";
    else
      partSelect << "[" << left << "]";

    item->setText(RegisterEditorWidget::colFieldPARTSELECT, partSelect.c_str());

    mRegEditor->setModified(true);
  }

  QString msg;
  updateFieldColor(item, msg);

  if (!msg.isEmpty())
    showStatus(msg);
  else
    clearStatus();

  resizeColumns();
}

void QRegisterFieldsTreeWidget::resizeColumns()
{
  header()->setResizeMode(0, QHeaderView::Interactive);
  header()->setResizeMode(0, QHeaderView::Stretch);
  header()->resizeSections(QHeaderView::ResizeToContents);
  header()->setResizeMode(0, QHeaderView::Interactive);
}

void QRegisterFieldsTreeWidget::clearStatus()
{
  mTextBrowser->clear();
}

void QRegisterFieldsTreeWidget::showStatus(const QString& value)
{
  if (mTextBrowser)
    mTextBrowser->setText(value);
}

CarbonDB* QRegisterFieldsTreeWidget::getDB()
{
  CarbonProjectWidget* projWidget = mCtx->getCarbonProjectWidget();
  CarbonProject* project = projWidget->project();
  CarbonDatabaseContext* dbContext = project->getDbContext();
  CarbonDB* db = dbContext->getDB();
  return db;
}

void QRegisterFieldsTreeWidget::dragMoveEvent(QDragMoveEvent* event)
{
  bool ignore = false;

  const QMimeData* mimeData = event->mimeData();
  if (mRegister && mimeData->hasText())
  {
    // Creating a new field? Is there room at the inn?
    QTreeWidgetItem* item = itemAt(event->pos());
    int unusedBits = mRegister->numUnusedBits();
    if (item == NULL && unusedBits == 0)
    {
      ignore = true;
      showStatus("Register is already fully populated");
    } 
    else if (item) // dropping into a field
    {
      UtString rtlpath;
      rtlpath << mimeData->text();

      QVariant qv = item->data(FLD_TAG_FIELD, Qt::UserRole);
      INFO_ASSERT(!qv.isNull(), "Missing Field Tag");
      SpiritField* field = (SpiritField*)qv.value<void*>();

      int fieldWidth = field->getBitWidth();
      CarbonDB* db = getDB();

      const CarbonDBNode* node = carbonDBFindNode(db, rtlpath.c_str());

      //bool isVector = carbonDBIsVector(db, node);
      bool isScalar = carbonDBIsScalar(db, node);
      //bool isMemory = carbonDBIs2DArray(db, node);

      int nodeBitSize = carbonDBGetWidth(db, node);
      if (nodeBitSize < fieldWidth)
      {
        ignore = true;
        showStatus("RTL item is too small for this bit field");
      }
      else if ((nodeBitSize > fieldWidth) && isScalar)
      {
        ignore = true;
        showStatus("Part selects on scalars are not allowed.");
      }
    }

    animateDropSite(event->pos());
  }

  if (ignore)
    event->ignore();
  else
  {
    clearStatus();
    event->acceptProposedAction();
  }
}

void QRegisterFieldsTreeWidget::animateDropSite(const QPoint& pos)
{
  QTreeWidgetItem* item = itemAt(pos.x(), pos.y());
  if (mDropSiteItem != item)
  {
    clearAnimation();
    mDropSiteItem = item;
    if (item != NULL) 
    {
      mDropItemSelected = isItemSelected(item);
      mDropColumnCount = std::min(TDD_MAX_COLUMNS, columnCount());
      for (UInt32 i = 0; i < mDropColumnCount; ++i)
        mDropSaveBrush[i] = item->background(i);
      QColor dropHighlightColor = QColor(Qt::green);
      if (mDropItemSelected) 
      {
        setItemSelected(item, false);
        dropHighlightColor = QColor(Qt::darkGreen);
      }
      for (UInt32 i = 0; i < mDropColumnCount; ++i)
        item->setBackgroundColor(i, dropHighlightColor);
    }
  }
}

void QRegisterFieldsTreeWidget::deleteSelectedField()
{
  foreach (QTreeWidgetItem* item, selectedItems())
  {
    QVariant qv = item->data(FLD_TAG_FIELD, Qt::UserRole);
    INFO_ASSERT(!qv.isNull(), "Missing Field Tag");
    SpiritField* field = (SpiritField*)qv.value<void*>();
    takeTopLevelItem(indexOfTopLevelItem(item));

    mRegister->removeField(field);
    
    break;
  }
}

void QRegisterFieldsTreeWidget::clearAnimation()
{
  if (mDropSiteItem != NULL) 
  {
    for (UInt32 i = 0; i < mDropColumnCount; ++i)
      mDropSiteItem->setBackground(i, mDropSaveBrush[i]);
    if (mDropItemSelected)
      setItemSelected(mDropSiteItem, true);
    mDropSiteItem = NULL;
  }
}

// Editor Delegate
RegisterFieldsDelegate::RegisterFieldsDelegate(QObject *parent, QRegisterFieldsTreeWidget* t, RegisterEditorWidget* w)
: QItemDelegate(parent), tree(t), widget(w)
{
}

void RegisterFieldsDelegate::currentIndexChanged(int)
{
  emit commitData(qobject_cast<QWidget*>(sender()));
  emit closeEditor(qobject_cast<QWidget*>(sender()));

  //QWidget* widget = tree->mEditItem;
  //if (widget)
  //{
  //  commitData(widget);
  //  closeEditor(widget);
  //}
}

QWidget *RegisterFieldsDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                              const QModelIndex &index) const
{
  QTreeWidgetItem* item = tree->currentItem();
  if (item == NULL)
    return NULL;

  QVariant qv = item->data(FLD_TAG_FIELD, Qt::UserRole);
  INFO_ASSERT(!qv.isNull(), "Missing Field Tag");
  SpiritField* field = (SpiritField*)qv.value<void*>();
  SpiritRegister* reg = field->getRegister();

  switch (index.column())
  {
  case RegisterEditorWidget::colFieldNAME:
    {
      QLineEdit *editor = new QLineEdit(parent);
      connect(editor, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
      return editor;
    }
    break;
  case RegisterEditorWidget::colFieldPARTSELECT:
    {
      QString currValue = item->text(RegisterEditorWidget::colFieldPARTSELECT);
      if (field->getHasRange() || !currValue.isEmpty())
      {
        QLineEdit *editor = new QLineEdit(parent);
        connect(editor, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
        return editor;
      }
    }
    break;
  case RegisterEditorWidget::colFieldBITOFFSET:
    {
      QSpinBox* sbox = new QSpinBox(parent);
      QVariant v = field->getBitOffset()->getBitOffset();

      sbox->setMinimum(0);
      sbox->setMaximum(reg->getSize()-1);

      sbox->setValue(v.toInt());

      connect(sbox, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
      return sbox;  
    }
    break;
  case RegisterEditorWidget::colFieldBITS:
    {
      QSpinBox* sbox = new QSpinBox(parent);
      QVariant v = field->getBitWidth();

      sbox->setMinimum(1);
      sbox->setMaximum(reg->getSize());

      sbox->setValue(v.toInt());

      connect(sbox, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
      return sbox;  
    }
    break;
  case RegisterEditorWidget::colFieldMEMORYINDEX:
    {
      if (field->getLocation() == SpiritEnum::LocationArray)
      {
        CarbonDB* db = tree->getDB();
        UtString rtlPath;
        rtlPath << field->getRTLPath();

        const CarbonDBNode* node = carbonDBFindNode(db, rtlPath.c_str());

        SInt32 left = carbonDBGet2DArrayLeftAddr(db, node);
        SInt32 right = carbonDBGet2DArrayRightAddr(db, node);

        QSpinBox* sbox = new QSpinBox(parent);
        QVariant v = field->getMemoryIndex();

        sbox->setMinimum(left);
        sbox->setMaximum(right);

        sbox->setValue(v.toInt());

        connect(sbox, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));

        return sbox;  
      }
    }
    break;
  case RegisterEditorWidget::colFieldACCESS:
    {
      QComboBox* cbox = new QComboBox(parent);

      cbox->addItem(accessToString(SpiritEnum::ReadWrite), (int)SpiritEnum::ReadWrite);
      cbox->addItem(accessToString(SpiritEnum::ReadOnly), (int)SpiritEnum::ReadOnly);
      cbox->addItem(accessToString(SpiritEnum::WriteOnly), (int)SpiritEnum::WriteOnly);

      cbox->setCurrentIndex(cbox->findData((int)field->getAccess()));

      if (!connect(cbox, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexChanged(int))))
        qDebug() << "Error: Unable to hookup slot: currentIndexChanged";
      return cbox;
    }
    break;
  default:
    break;
  }

  return NULL;
}

void RegisterFieldsDelegate::commitAndCloseEditor()
{
  QWidget *editor = qobject_cast<QWidget *>(sender());
  if (editor)
  {
    emit commitData(qobject_cast<QWidget*>(sender()));
    emit closeEditor(qobject_cast<QWidget*>(sender()));
  }
}

void RegisterFieldsDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit)
  {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }
}
void RegisterFieldsDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                          const QModelIndex &index) const
{
  QTreeWidgetItem* item = tree->currentItem();
  if (item == NULL)
    item = tree->topLevelItem(index.row());

  if (item == NULL)
    return;

  QVariant qv = item->data(FLD_TAG_FIELD, Qt::UserRole);
  INFO_ASSERT(!qv.isNull(), "Missing Field Tag");
  SpiritField* field = (SpiritField*)qv.value<void*>();
  SpiritRegister* reg = field->getRegister();

  UtString editText;
  switch (index.column())
  {
  case RegisterEditorWidget::colFieldNAME:
    {
      QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
      if (edit) 
      { 
        if (edit->text().contains(' '))
        {
          tree->showStatus("Field names must not contain spaces.");
          return;
        }
        SpiritField* fld = reg->findField(edit->text());
        if (fld == NULL || fld == field)
        {
          editText << edit->text();
          if (edit->text() != field->getName())
          {
            field->setName(edit->text());
            widget->setModified(true);
            model->setData(index, editText.c_str());
          }
        }
        else
        {
          tree->showStatus("Duplicate field names are not allowed.");
          return;
        }
      }
    }
    break;
  case RegisterEditorWidget::colFieldPARTSELECT:
    {
      QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
      if (edit) 
      {
        QString data = edit->text();
        editText << edit->text();

        HdlVerilogPath hpath;

        UtString newVal;

        if (data.isEmpty() || (data[0] != '[')) 
        {
          newVal << "[" << data << "]";
        }
        else
        {
          newVal << data;
        }

        const char* vector = newVal.c_str();
        HdlId hdlid;
        if (HdlHierPath::eLegal == hpath.parseVectorInfo(&vector, NULL, &hdlid))
        {
          UInt32 rangeSize = hdlid.getWidth();
          if (rangeSize == field->getBitWidth())
          {
            widget->setModified(true);
            model->setData(index, newVal.c_str());
            switch (hdlid.getType())
            {
            case HdlId::eVectBitRange:
              field->setPartSelectRight(hdlid.getVectLSB());
              field->setPartSelectLeft(hdlid.getVectMSB());
              break;

            case HdlId::eVectBit:
              field->setPartSelectRight(hdlid.getVectIndex());
              field->setPartSelectLeft(hdlid.getVectIndex());
              break;

            default:
            case HdlId::eScalar:
              field->setPartSelectRight(hdlid.getVectLSB());
              field->setPartSelectLeft(hdlid.getVectLSB());
              break;
            }
            tree->clearStatus();
          }
          else
          {
            tree->showStatus("Partselect bit width does not match field size, ignoring");
            return;
          }
        }
      }
    }
    break;
  case RegisterEditorWidget::colFieldBITOFFSET:
    {
      QSpinBox* spinBox = qobject_cast<QSpinBox*>(editor);
      if (spinBox)
      {
        QVariant v = spinBox->value();
        model->setData(index, v.toString());
        field->getBitOffset()->setBitOffset(v.toUInt());
        widget->setModified(true);
      }      
    }
    break;
  case RegisterEditorWidget::colFieldBITS:
    {
      QSpinBox* spinBox = qobject_cast<QSpinBox*>(editor);
      if (spinBox)
      {
        QVariant v = spinBox->value();
        model->setData(index, v.toString());
        field->setBitWidth(v.toUInt());
        widget->setModified(true);
      }      
    }
    break;
 case RegisterEditorWidget::colFieldMEMORYINDEX:
    {
      QSpinBox* spinBox = qobject_cast<QSpinBox*>(editor);
      if (spinBox)
      {
        QVariant v = spinBox->value();
        model->setData(index, v.toString());
        field->setMemoryIndex(v.toUInt());
        widget->setModified(true);
      }      
    }
    break;
  case RegisterEditorWidget::colFieldACCESS:
    {
      QComboBox* comboBox = qobject_cast<QComboBox*>(editor);
      if (comboBox)
      {
        model->setData(index, comboBox->currentText());
        field->setAccess(stringToAccess(comboBox->currentText()));
        widget->setModified(true);
      }
    }
    break;
  default:
    break;
  }

  QString msg;
  tree->updateFieldColors(msg);

  if (!msg.isEmpty())
    tree->showStatus(msg);
  else
    tree->clearStatus();
}
