#ifndef DLGNEWVERSION_H
#define DLGNEWVERSION_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include "ModelKit.h"
#include <QDialog>
#include "ui_DlgNewVersion.h"

class DlgNewVersion : public QDialog
{
  Q_OBJECT

public:
  enum ManifestType { NewManifest, CopyManifest };

  DlgNewVersion(QWidget *parent = 0, ModelKit* mkit = 0);
  ~DlgNewVersion();

  ManifestType getManifestType();
  quint32 getVersionID();
  QString getDescription();
  QString getVendorVersion();
  QString getManifestName();

  bool isValid();

private:
  Ui::DlgNewVersionClass ui;
  ManifestType mManifestType;
  ModelKit* mModelKit;


private slots:
    void on_lineEditVendorVersion_textChanged(QString);
    void on_radioButtonCopyManifest_toggled(bool);
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
};

#endif // DLGNEWVERSION_H
