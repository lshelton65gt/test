#ifndef __CARBONPROPERTIES_H__
#define __CARBONPROPERTIES_H__

#include "util/CarbonPlatform.h"

#include "CarbonProperty.h"
#include "CarbonPropertyBool.h"
#include "CarbonPropertyEnum.h"
#include "CarbonPropertyInfile.h"
#include "CarbonPropertyInteger.h"
#include "CarbonPropertyOutfile.h"
#include "CarbonPropertyString.h"
#include "CarbonPropertyStringlist.h"
#include "CarbonPropertyPassword.h"

class CarbonProperties;

// A named group of settings
class CarbonPropertyGroup
{
public:
  CARBONMEM_OVERRIDES

  CarbonPropertyGroup(CarbonProperties* parent)
  {
    mSwitches=parent;
  }

  const char* getName() { return mName.c_str(); }
  void putGroup(const char* newVal) {mName = newVal; }
  CarbonPropertyGroup(const CarbonPropertyGroup& src) : mName(src.mName) {}
  ~CarbonPropertyGroup() {}

  typedef UtHashMap<UtString,CarbonProperty*> SwitchMap;
  typedef SwitchMap::SortedLoop SwitchLoop;

  SwitchLoop loopSwitches() 
  {
    return mSwitchMap.loopSorted();
  }

  CarbonPropertyGroup& operator=(const CarbonPropertyGroup& src)
  {
    if (&src != this) {
      mName = src.mName;
    }
    return *this;
  }

  void addProperty(CarbonProperty* sw)
  {
    mSwitchMap[sw->getName()] = sw;
  }

  void removeProperty(const char* propName)
  {
    mSwitchMap.erase(propName);
  }

private:
  UtString mName;
  CarbonProperties* mSwitches;
  SwitchMap mSwitchMap;
};


// This class holds a collection of groups
class CarbonProperties : public QObject
{
  Q_OBJECT

public:
  CARBONMEM_OVERRIDES
    bool readPropertyDefinitions(const char* xmlFile);

  CarbonProperties();

  UInt32 numGroups() const { return mPropertyGroups.size(); }

  //! get the nth group
  CarbonPropertyGroup* getGroup(UInt32 index) const
  {
    INFO_ASSERT((int)index < mPropertyGroups.size(), "group out of range");
    return mPropertyGroups[(int)index];
  }
  CarbonPropertyGroup* findGroup(const char* name);
  CarbonPropertyGroup* addGroup(const char* name);
  const CarbonProperty* locateProperty(const char* name) const;
  CarbonProperty* getProperty(const char* name) const;
  const CarbonProperty* findProperty(const char* name) const;
  const CarbonProperty* findProperty(CarbonPropertyGroup* group, const char* name) const;
  void addProperty(CarbonPropertyGroup* group, CarbonProperty* prop);
  bool removeProperty(const CarbonProperty* prop);
  bool removeGroup(const char* groupName);

private:
  bool read(xmlNodePtr parent, UtXmlErrorHandler* errors);
  bool parseSection(xmlNodePtr parent, UtXmlErrorHandler* errors);
  bool parseOption(xmlNodePtr parent, CarbonPropertyGroup* group, UtXmlErrorHandler* errors);

private:
  QList<CarbonPropertyGroup*> mPropertyGroups;
  QList<UtString> mStrArray;
};

#endif
