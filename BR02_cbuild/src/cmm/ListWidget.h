#ifndef __LISTWIDGET_H__
#define __LISTWIDGET_H__

#include "util/CarbonPlatform.h"

#include <QtGui>
#include <QListWidget>
#include <QItemDelegate>

class ListWidget : public QListWidget
{
  Q_OBJECT

public:
  ListWidget(QWidget* parent=0);

  friend class ListDelegate;

  void addNewItem();
  void addNewItem(const char* value);
  void deleteItem();
  void moveDown();
  void moveUp();
};


class ListDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    ListDelegate(QObject *parent = 0, ListWidget* t=0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
        const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
        const QModelIndex &index) const;
 
private slots:
    void commitAndCloseEditor();

private:
  ListWidget* list;
};


#endif
