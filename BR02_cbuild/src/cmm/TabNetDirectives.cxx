//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QtGui>
#include "gui/CQt.h"

#include "shell/CarbonDatabasePriv.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/carbon_cfg.h"
#include "cfg/CarbonCfg.h"

#include "TabNetDirectives.h"
#include "DirectivesEditor.h"
#include "CarbonDirectives.h"
#include "DlgTieNet.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonDatabaseContext.h"


SpecialDirective specialDirs[] =
{ 
  {"observeSignal", TabNetDirectives::colOBSERVE}, 
  {"depositSignal", TabNetDirectives::colDEPOSIT},
  {"forceSignal", TabNetDirectives::colFORCE}
};

#define numSpecialDirs (sizeof(specialDirs) / sizeof(SpecialDirective))

SpecialDirective* TabNetDirectives::getSpecialDirective(const char* dirName)
{
  for (int i=0; i<(int)numSpecialDirs; i++)
  {
    UtString s1;
    UtString s2;
    s1 << specialDirs[i].mName;
    s2 << dirName;
    if (s1 == s2)
      return &specialDirs[i];
  } 
  return NULL;
}
/* new ram */
bool TabNetDirectives::getSpecialDirectiveValue(const char *netName, const char *whichDirective, bool *isAlreadyPresent) {
  QTreeWidgetItem* item = findNet(netName);
  *isAlreadyPresent = false;
  if (item)
  {
    SpecialDirective* spDir = getSpecialDirective(whichDirective);
    if (spDir)
    {
      QVariant qv = itemWidget(item, spDir->mColumn)->property("checkBox");
      if (!qv.isNull())
      {
        *isAlreadyPresent = true;
        QCheckBox* cb = (QCheckBox*)qv.value<void*>();
        return (cb->isChecked());
      }
    }
  }
  return false;
}
void TabNetDirectives::newDirective()
{
  QTreeWidgetItem* itm = new QTreeWidgetItem(this);
  itm->setFlags(itm->flags() | Qt::ItemIsEditable);

  setCurrentItem(itm);
  addTopLevelItem(itm);

  // first 5 are special directives, so check for them
  createCheckBox(itm, NULL, TabNetDirectives::colOBSERVE, "observeSignal");
  createCheckBox(itm, NULL, TabNetDirectives::colDEPOSIT, "depositSignal");
  createCheckBox(itm, NULL, TabNetDirectives::colFORCE, "forceSignal");

  setModified(true);
}

TabNetDirectives::TabNetDirectives(QWidget *parent)
: QTreeWidget(parent)
{
  mApplyNewTieValue = false;

  mGroup = NULL;
  setWindowTitle("NetDirectives[*]");
  setSortingEnabled(false);

  QStringList colNames;

  colNames << "Net";
  colNames << "Observe";
  colNames << "Deposit";
  colNames << "Force";
  colNames << "Other 1";
  colNames << "Other 2";
  colNames << "Other 3";
  colNames << "Other 4";
  colNames << "Other 5";

  setHeaderLabels(colNames);

  setEditTriggers(QAbstractItemView::AllEditTriggers);
  setSelectionMode(QAbstractItemView::ExtendedSelection);
  setItemDelegate(new TabNetDirectivesDelegate(this, this));
}


TabNetDirectives::~TabNetDirectives()
{
}

void TabNetDirectives::dragEnterEvent(QDragEnterEvent *event)
{
  if (event->mimeData()->hasFormat("text/plain"))
    event->acceptProposedAction();
}

void TabNetDirectives::buildSelectionList(QList<QTreeWidgetItem*>& list)
{
  // The list gives items for every column and row, we just want it row
  // oriented.
  list.clear();

  foreach (QTreeWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(0, Qt::UserRole);
    if (!qv.isNull())
    {
      list.append(item);
    }
  }  
}

void TabNetDirectives::populate(CarbonDirectiveGroup* group)
{
  mGroup = group;

  blockSignals(true);

  clear();

  if (group)
  {
    foreach (NetDirectives* dir, *mGroup->getNetDirectives())
      addDirectiveRow(dir);
  }

  header()->setResizeMode(TabNetDirectives::colNAME, QHeaderView::Interactive);
  header()->setResizeMode(TabNetDirectives::colOBSERVE, QHeaderView::ResizeToContents);
  header()->setResizeMode(TabNetDirectives::colDEPOSIT, QHeaderView::ResizeToContents);
  header()->setResizeMode(TabNetDirectives::colFORCE, QHeaderView::ResizeToContents);
  header()->setResizeMode(TabNetDirectives::colOTHER, QHeaderView::Interactive);

  header()->resizeSections(QHeaderView::ResizeToContents);

  setModified(false);  

  blockSignals(false);
}

void TabNetDirectives::deleteDirectives()
{
  foreach (QTreeWidgetItem* item, selectedItems())
  {
    for (int col=0; col<columnCount(); col++)
    {
      removeItemWidget(item, col);
    }
    QTreeWidgetItem* parent = item->parent();
    if (parent != NULL) 
    {
      int childIndex = parent->indexOfChild(item);
      parent->takeChild(childIndex);
    }
    else
    {
      int childIndex = indexOfTopLevelItem(item);
      takeTopLevelItem(childIndex);
    }
  }
  blockSignals(true);
  foreach (QTreeWidgetItem* item, selectedItems()) {
    setItemSelected(item, false);
  }
  blockSignals(false);

  setModified(true);
}


void TabNetDirectives::changeCheckboxQuiet(QCheckBox* cb, int newState)
{
  if (cb)
  {
    // save current state
    bool blockSignals = cb->signalsBlocked();
    cb->blockSignals(true);
    cb->setCheckState((Qt::CheckState)newState);
    // restore the previous state
    cb->blockSignals(blockSignals);
  }
}


void TabNetDirectives::applyNewValue(QTreeWidgetItem* item, QTreeWidgetItem*, int newState, int col)
{
  if (item)
  {
    QVariant qvCurrent = itemWidget(item, col)->property("checkBox");
    QCheckBox* cbCurrent = (QCheckBox*)qvCurrent.value<void*>();

    switch (col)
    {
    default:
      break;

    case colDEPOSIT:
      {
        changeCheckboxQuiet(cbCurrent, newState);
      }
      break;

    case colOBSERVE:
      {
        changeCheckboxQuiet(cbCurrent, newState);
      }
      break;
    }
  }
}

void TabNetDirectives::stateChanged(int newState)
{
  QCheckBox* cbox = qobject_cast<QCheckBox*>(sender());
  QVariant qvColumn = cbox->property("columnIndex");
  QVariant qvItem = cbox->property("columnItem");
  QTreeWidgetItem* item = NULL;
  if (!qvItem.isNull())
    item = (QTreeWidgetItem*)qvItem.value<void*>();

  bool wasSelectedItem = item->isSelected();

  blockSignals(true);
  // now, if multiple items are selected, change the state of the
  // other items in this column to match this one.
  if (item && qvColumn.isValid())
    applyNewValue(item, item, newState, qvColumn.toInt()); 

  // We changed a value on a selected checkbox, propagate
  // the change to all the other selected nets
  if (wasSelectedItem)
  {
    foreach (QTreeWidgetItem* otherItem, selectedItems())
    {
      QWidget* w = itemWidget(otherItem, qvColumn.toInt());
      if (w && otherItem != item)
      {
        QVariant qv = w->property("checkBox");
        if (!qv.isNull())
          applyNewValue(otherItem, item, newState, qvColumn.toInt());
      }
    }
  }

  blockSignals(false);

  // Reflect the new edits, then signal we've changed values
  refreshValues();

  mDirectivesEditor->signalDirectivesChanged(eDirectiveNet);

  mDirectivesEditor->setWindowModified(true);

  setWindowModified(true);
}

// Find a directive for a given item
int TabNetDirectives::findDirective(QTreeWidgetItem* item, const char* dirName)
{
  // check Other Rows
  for (int col=colOTHER; col<columnCount(); col++)
  {
    UtString name;
    name << item->text(col);
    if (name.length() > 0 && name == dirName)
      return col;
  }
  return -1;
}

void TabNetDirectives::addDirective(QTreeWidgetItem* item, const char* dirName)
{
  for (int col=colOTHER; col<columnCount(); col++)
  {
    UtString name;
    name << item->text(col);
    if (name.length() == 0)
    {
      item->setText(col, dirName);
      setModified(true);
      break;
    }
  }
}

void TabNetDirectives::setDirective(const QStringList& netNames, const char* dirName, bool setIt)
{
  clearSelection();

  foreach (QString str, netNames)
  {
    UtString net;
    net << str;
    const char* netName = net.c_str();

    qDebug() << "Set directive on net: " << netName << " directive: " << dirName << " value: " << setIt;

    if (setIt) // we are setting the directive
    { 
      QTreeWidgetItem* item = addNewRow(netName, false);
      INFO_ASSERT(item, "Expecting item");
      item->setSelected(true);

      SpecialDirective* spDir = getSpecialDirective(dirName);
      if (spDir)
      {
        QVariant qv = itemWidget(item, spDir->mColumn)->property("checkBox");
        if (!qv.isNull())
        {
          QCheckBox* cb = (QCheckBox*)qv.value<void*>();
          cb->setChecked(true);  
          setModified(true);
        }
      }
      else // Find directive for this item
      {
        int col = findDirective(item, dirName);
        if (col == -1)
          addDirective(item, dirName);
      }
    }
    else
    {
      QTreeWidgetItem* item = findNet(netName);
      if (item)
      {
        item->setSelected(true);
        SpecialDirective* spDir = getSpecialDirective(dirName);
        if (spDir)
        {
          QVariant qv = itemWidget(item, spDir->mColumn)->property("checkBox");
          if (!qv.isNull())
          {
            QCheckBox* cb = (QCheckBox*)qv.value<void*>();
            cb->setChecked(false);  
            setModified(true);
          }
        }
        else
        {
          int col = findDirective(item, dirName);
          if (col != -1)
          {
            item->setText(col, "");
            setModified(true);
          }
        }
      }
    }
  }
}

bool TabNetDirectives::selectItems(const char* text)
{
  bool selectedSomething = false;

  clearSelection();
  QString nets(text);

  foreach (QString net, nets.split('\n'))
  {
    foreach (QTreeWidgetItem* item, findItems(net, Qt::MatchExactly))
    {
      selectedSomething = true;
      item->setSelected(true);
    }
  }

  return selectedSomething;
}

bool TabNetDirectives::selectItem(const char* text)
{
  bool selectedSomething = false;

  clearSelection();

  foreach (QTreeWidgetItem* item, findItems(text, Qt::MatchExactly))
  {
    selectedSomething = true;
    item->setSelected(true);
  }

  return selectedSomething;
}


void TabNetDirectives::refreshValues()
{
  if (mGroup == NULL)
    return;

  // First clear the list, then rebuild it
  mGroup->removeAllNetDirectives();

  for (int row=0; row<topLevelItemCount(); row++)
  {
    QTreeWidgetItem* itm = topLevelItem(row);
    UtString name;
    name << itm->text(TabNetDirectives::colNAME);

    NetDirectives* dir = new NetDirectives(name.c_str());
    mGroup->addNetDirective(dir);

    if (name.length() > 0) // Only save non-empty rows
    {
      // Observe
      QVariant qvObserve = itemWidget(itm, TabNetDirectives::colOBSERVE)->property("checkBox");
      if (!qvObserve.isNull())
      {
        DirectiveDefinition* def = mGroup->getDirectives()->findDefinition("observeSignal");
        QCheckBox* cb = (QCheckBox*)qvObserve.value<void*>();

        if (cb && cb->isChecked())
          dir->addDirective(new Directive("observeSignal", NULL, def));
      }

      // Deposit
      QVariant qvDeposit = itemWidget(itm, TabNetDirectives::colDEPOSIT)->property("checkBox");
      if (!qvDeposit.isNull())
      {
        DirectiveDefinition* def = mGroup->getDirectives()->findDefinition("depositSignal");
        QCheckBox* cb = (QCheckBox*)qvDeposit.value<void*>();
        if (cb && cb->isChecked())
          dir->addDirective(new Directive("depositSignal", NULL, def));
      }

      // Force
      QVariant qvForce = itemWidget(itm, TabNetDirectives::colFORCE)->property("checkBox");
      if (!qvForce.isNull())
      {
        DirectiveDefinition* def = mGroup->getDirectives()->findDefinition("forceSignal");
        QCheckBox* cb = (QCheckBox*)qvForce.value<void*>();
        if (cb && cb->isChecked())
          dir->addDirective(new Directive("forceSignal", NULL, def));
      }

      // check Other Rows
      for (int col=colOTHER; col<columnCount(); col++)
      {
        if (itm)
        {
          UtString dirName;
          dirName << itm->text(col);
          if (dirName.length() > 0)
          {
            UtString directiveValue;
            QString dname = dirName.c_str();
            if (dname.startsWith("tieNet"))
            {
              directiveValue << dname.mid(strlen("tieNet:"));  
              dirName.clear();
              dirName << "tieNet";
            }

            DirectiveDefinition* def = mGroup->getDirectives()->findDefinition(dirName.c_str());
            INFO_ASSERT(def, "Unknown directive name");
            dir->addDirective(new Directive(dirName.c_str(), directiveValue.c_str(), def));
          }
        }
      }
    }
  }
}


void TabNetDirectives::saveDocument()
{
  if (mGroup && isWindowModified())
  {
    refreshValues();
    setWindowModified(false);   
  }
}

void TabNetDirectives::createCheckBox(QTreeWidgetItem* item, NetDirectives* dir, Column col, const char* directiveName)
{
  QHBoxLayout * layout = new QHBoxLayout;
  QCheckBox* cb = new QCheckBox;

  cb->setProperty("columnIndex", col);
  cb->setProperty("columnItem", qVariantFromValue((void*)item));

  UtString cbName;
  cbName << "cb" << directiveName << indexOfTopLevelItem(item);
  cb->setObjectName(cbName.c_str());

  if (dir)
    cb->setChecked(dir->findDirective(directiveName));

  CQT_CONNECT(cb, stateChanged(int), this, stateChanged(int));

  layout->addWidget(cb);
  layout->setSpacing(2);
  layout->setMargin(0);
  QWidget* widget = CQt::makeWidget(layout);
  layout->setAlignment(cb, Qt::AlignCenter);
  setItemWidget(item, col, widget);
  widget->setProperty("checkBox", qVariantFromValue((void*)cb));
}

void TabNetDirectives::addDirectiveRow(NetDirectives* netDir)
{
  bool updates = updatesEnabled();
  setUpdatesEnabled(false);

  QTreeWidgetItem* itm = new QTreeWidgetItem(this);
  itm->setFlags(itm->flags() | Qt::ItemIsEditable);
  itm->setText(TabNetDirectives::colNAME, netDir->mLocator.c_str());

  setCurrentItem(itm);
  addTopLevelItem(itm);

  // first 5 are special directives, so check for them
  createCheckBox(itm, netDir, TabNetDirectives::colOBSERVE, "observeSignal");
  createCheckBox(itm, netDir, TabNetDirectives::colDEPOSIT, "depositSignal");
  createCheckBox(itm, netDir, TabNetDirectives::colFORCE, "forceSignal");

  // now check the non-builtin directives
  foreach (Directive* dir, netDir->mDirectives)
  {
    if (!dir->mDefinition->mBuiltin)
    {
      // find an empty column
      for (int col=TabNetDirectives::colOTHER; col<columnCount(); col++)
      {
        UtString value;
        value << itm->text(col);
        if (value.length() == 0)
        {
          if (dir->mDirective == "tieNet")
          {
            UtString tmp;
            tmp << "tieNet:" << dir->mValue;

            itm->setText(col, tmp.c_str());
          }
          else
            itm->setText(col, dir->mDirective.c_str());
          break;
        }
      }
    }
  }
  setUpdatesEnabled(updates);
}

void TabNetDirectives::setModified(bool value)
{
  setWindowModified(value);
  mDirectivesEditor->setWindowModified(value);
}

QTreeWidgetItem* TabNetDirectives::findNet(const char* netName)
{
  for (int row=0; row<topLevelItemCount(); row++)
  {
    QTreeWidgetItem* itm = topLevelItem(row);
    UtString name;
    name << itm->text(TabNetDirectives::colNAME);
    if (name == netName)
      return itm;
  }
  return NULL;
}


QTreeWidgetItem* TabNetDirectives::addNewRow(const char* loc, bool exclusiveSelect, QTreeWidgetItem* clockItem)
{
  // Already Exists!
  foreach (QTreeWidgetItem* item, findItems(loc, Qt::MatchExactly))
  {
    if (exclusiveSelect)
      setCurrentItem(item);
    return item;
  }

  bool updates = updatesEnabled();
  setUpdatesEnabled(false);

  QTreeWidgetItem* item = NULL;

  if (clockItem)
    item = new QTreeWidgetItem(clockItem);
  else
    item = new QTreeWidgetItem(this);

  item->setText(TabNetDirectives::colNAME, loc);
  item->setFlags(item->flags() | Qt::ItemIsEditable);

  if (exclusiveSelect)
    setCurrentItem(item);

  if (!clockItem)
  {
    addTopLevelItem(item);

    createCheckBox(item, NULL, TabNetDirectives::colOBSERVE, "observeSignal");
    createCheckBox(item, NULL, TabNetDirectives::colDEPOSIT, "depositSignal");
    createCheckBox(item, NULL, TabNetDirectives::colFORCE, "forceSignal");
  }
  else
    expandItem(clockItem);

  header()->setResizeMode(TabNetDirectives::colNAME, QHeaderView::ResizeToContents);
  header()->setResizeMode(TabNetDirectives::colOBSERVE, QHeaderView::ResizeToContents);
  header()->setResizeMode(TabNetDirectives::colDEPOSIT, QHeaderView::ResizeToContents);
  header()->setResizeMode(TabNetDirectives::colFORCE, QHeaderView::ResizeToContents);
  header()->setResizeMode(TabNetDirectives::colOTHER, QHeaderView::Interactive);

  setModified(true);

  setUpdatesEnabled(updates);

  return item;
}

void TabNetDirectives::dropEvent(QDropEvent *event)
{
  event->acceptProposedAction();

  QPoint pos = event->pos();
  QTreeWidgetItem* item = itemAt(pos.x(), pos.y());
  QTreeWidgetItem* clockItem = NULL;

  if (item)
  {
    for (int col=TabNetDirectives::colOTHER; col<columnCount(); col++)
    {
      UtString dirName;
      dirName << item->text(col);
      if (dirName == "collapseClock")
        clockItem = item;
    }
  }

  const QMimeData* mimeData = event->mimeData();
  if (mimeData->hasText())
  {
    QStringList locList = mimeData->text().split("\n");

    CarbonProjectWidget* pw = mDirectivesEditor->getContext()->getCarbonProjectWidget();
    CarbonProject* proj = pw->project();
    CarbonDatabaseContext* dbContext = proj->getDbContext();
    UtString errMsg;

    clearSelection();
    for (int i = 0; i< locList.size(); ++i)
    {
      UtString value;
      value << locList.at(i);

      const CarbonDBNode* nodeName = carbonDBFindNode(dbContext->getDB(), value.c_str());
      if (!carbonDBIsContainedByComposite(dbContext->getDB(), nodeName))
      {
        QTreeWidgetItem* newItem = addNewRow(value.c_str(), false, clockItem);
        newItem->setSelected(true);
      }
      else
        errMsg << "\t" << value << "\n";
    }

    if (errMsg.length() > 0)
    {
      UtString msg;
      msg << "Composites are not allowed to set directives:" << "\n\n";
      msg << errMsg;
      QMessageBox::warning(this, MODELSTUDIO_TITLE, msg.c_str());
    }

    setFocus();
  }

  header()->resizeSections(QHeaderView::ResizeToContents);
}

void TabNetDirectives::dragMoveEvent(QDragMoveEvent* event)
{
  bool ignore = true;
  const QMimeData* mimeData = event->mimeData();
  if (mimeData->hasText())
  {
    event->acceptProposedAction();
    ignore = false;
  }
  if (ignore)
    event->ignore();
}

// Delegate

// Editor Delegate
TabNetDirectivesDelegate::TabNetDirectivesDelegate(QObject *parent, TabNetDirectives* table)
: QItemDelegate(parent), tableWidget(table)
{
  mDlgTieNet = NULL;
}

void TabNetDirectivesDelegate::currentIndexChanged(int i)
{
  if (i != -1)
  {
    QComboBox *widget = qobject_cast<QComboBox *>(sender());

    // collapseClock
    if (widget->currentText() == "collapseClock")
    {
      QMessageBox::information(widget, tr("collapseClock Directive"),
        tr("You can drag nets to this entry to define the clocks"), QMessageBox::Ok);
    }

    if (widget->currentText().startsWith("tieNet"))
    {
      mDlgTieNet = new DlgTieNet(tableWidget);
      if (mDlgTieNet->exec() == QDialog::Accepted)
      {
        UtString value;
        value << "tieNet:" << mDlgTieNet->getValue();
        tableWidget->setApplyNewTieValue(true, mDlgTieNet->getValue());
      }
    }

    commitData(widget);
    closeEditor(widget);

  }
}


QWidget *TabNetDirectivesDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                                const QModelIndex &index) const
{
  bool bTieNet = false;
  tableWidget->setApplyNewTieValue(false, "");

  if (index.column() == TabNetDirectives::colNAME)
  {
    QLineEdit *editor = new QLineEdit(parent);
    connect(editor, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
    return editor;
  }

  if (index.column() >= TabNetDirectives::colOTHER)
  {
    QComboBox *cbox = new QComboBox(parent);
    UtString value;

    QTreeWidgetItem* itm = tableWidget->topLevelItem(index.row());
    if (itm)
      value << itm->text(index.column());

    QString currValue = value.c_str();
    if (currValue.startsWith("tieNet:"))
    {
      bTieNet = true;
      DlgTieNet dlg(tableWidget);
      UtString modValue;
      modValue << currValue.mid(strlen("tieNet:"));
      dlg.putValue(modValue.c_str());
      value.clear();
      if (dlg.exec() == QDialog::Accepted)
      {
        if (strlen(dlg.getValue()) > 0)
        {
          value << "tieNet:" << dlg.getValue();
          tableWidget->setApplyNewTieValue(true, dlg.getValue());
        }
        else
        {
          value << "";
          bTieNet = false;
        }
      }
      else
      {
        value << "tieNet:" << modValue.c_str();
        tableWidget->setApplyNewTieValue(true, modValue.c_str());
      }

    }

    foreach (DirectiveDefinition* def, *tableWidget->mGroup->getDirectives()->getDefinitions())
    {
      // Skip legacy directives
      if (0 == strcmp(def->mName.c_str(), "mvObserveSignal") || 0 == strcmp(def->mName.c_str(), "mvDepositSignal"))
        continue;

      if (!def->mBuiltin && (def->mMode == eDirInstance || def->mMode == eDirBoth))
        cbox->addItem(def->mName.c_str());
    }

    cbox->model()->sort(0);

    cbox->insertItem(0, "<Nothing>");

    if (itm)
    {
      if (bTieNet)
      {
        value.clear();
        value << "tieNet";
      }

      int ci = cbox->findText(value.c_str());
      if (ci != -1)
        cbox->setCurrentIndex(ci);
      else
        cbox->setCurrentIndex(cbox->findText("<Nothing>"));
    }
    else
      cbox->setCurrentIndex(cbox->findText("<Nothing>"));


    CQT_CONNECT(cbox, currentIndexChanged(int), this, currentIndexChanged(int));

    return cbox;

  }

  return NULL;
}

void TabNetDirectivesDelegate::commitAndCloseEditor()
{
  QLineEdit *editor = qobject_cast<QLineEdit *>(sender());
  emit commitData(editor);
  emit closeEditor(editor);
}

void TabNetDirectivesDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit)
  {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }
}

void TabNetDirectivesDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                            const QModelIndex &index) const
{
  QTreeWidgetItem* item = tableWidget->currentItem();
  if (tableWidget->signalsBlocked())
    return;

  UtString editText;

  if (index.column() == TabNetDirectives::colNAME)
  {
    QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
    if (edit)
    {
      UtString currText;
      currText << item->text(TabNetDirectives::colNAME);
      editText << edit->text();

      if (0 != strcmp(currText.c_str(), editText.c_str()))
      {
        model->setData(index, edit->text());
        tableWidget->setModified(true);
        tableWidget->refreshValues();
        tableWidget->mDirectivesEditor->signalDirectivesChanged(eDirectiveNet);
        tableWidget->mDirectivesEditor->signalNetRenamed(eDirectiveNet, currText.c_str(), editText.c_str());
      }
    }
  }

  if (index.column() >= TabNetDirectives::colOTHER)
  {
    QComboBox* cbox = qobject_cast<QComboBox *>(editor);
    if (cbox != NULL)
    {
      UtString value;
      value << cbox->currentText();

      if (cbox->currentText().startsWith("tieNet"))
      {
        value.clear();
        if (tableWidget->getApplyNewTieValue())
          value << "tieNet:" << tableWidget->getTieValue();
      }

      if (value == "<Nothing>")
        model->setData(index, "");
      else
        model->setData(index, value.c_str());

      tableWidget->setModified(true);
      tableWidget->mDirectivesEditor->signalDirectivesChanged(eDirectiveNet);
      tableWidget->refreshValues();
    }
  }
}
