//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "DesignXML.h"
#include "JavaScriptAPIs.h"
#include "CarbonProjectWidget.h"
#include "cmm.h"


void XDesignHierarchy::registerTypes(QScriptEngine* engine)
{
  if (engine)
  {
    QScriptValue global = engine->globalObject();
    
    // Allows an object to return a property of type ScrCcfg
    qScriptRegisterQObjectMetaType<XDesignHierarchy*>(engine);
    qScriptRegisterQObjectMetaType<XTopLevelInterfaces*>(engine);
    qScriptRegisterQObjectMetaType<XToplevelInterface*>(engine);
    qScriptRegisterQObjectMetaType<XDesignFiles*>(engine);
    qScriptRegisterQObjectMetaType<XLibraries*>(engine);
    qScriptRegisterQObjectMetaType<XInterfaces*>(engine);
    qScriptRegisterQObjectMetaType<XInstance*>(engine);
    qScriptRegisterQObjectMetaType<XInstances*>(engine);
    qScriptRegisterQObjectMetaType<XLibrary*>(engine);
    qScriptRegisterQObjectMetaType<XDesignFile*>(engine);
    qScriptRegisterQObjectMetaType<XDefines*>(engine);
    qScriptRegisterQObjectMetaType<XDefine*>(engine);
    qScriptRegisterQObjectMetaType<XInterface*>(engine);
    qScriptRegisterQObjectMetaType<XInterfaceArchitectures*>(engine);
    qScriptRegisterQObjectMetaType<XInterfacePorts*>(engine);
    qScriptRegisterQObjectMetaType<XInterfaceArchitecture*>(engine);
    qScriptRegisterQObjectMetaType<XInterfacePort*>(engine);
    qScriptRegisterQObjectMetaType<XInstancePorts*>(engine);
    qScriptRegisterQObjectMetaType<XInstancePort*>(engine);
    qScriptRegisterQObjectMetaType<XParameters*>(engine);
    qScriptRegisterQObjectMetaType<XParameter*>(engine);

    //Q_DECLARE_METATYPE(XInterfacePort::InterfacePortDirection)
    qScriptRegisterEnumMetaType<XInterfacePort::InterfacePortDirection>(engine); 
    //Q_DECLARE_METATYPE(XDesignFile::DesignFileKind)
    qScriptRegisterEnumMetaType<XDesignFile::DesignFileKind>(engine); 

    // Static register for classes with Q_ENUMS
    QScriptValue xipEnums = engine->newQMetaObject(&XInterfacePort::staticMetaObject);
    engine->globalObject().setProperty("XInterfacePort", xipEnums);

    QScriptValue xdfEnums = engine->newQMetaObject(&XDesignFile::staticMetaObject);
    engine->globalObject().setProperty("XDesignFile", xdfEnums);
  }
}

void XDesignHierarchy::registerScriptTypes(QMap<QString, const QMetaObject*>& map)
{
  map["XDesignHierarchy*"] = &XDesignHierarchy::staticMetaObject;
  map["XDesignHierarchy"] = &XDesignHierarchy::staticMetaObject;
  map["XTopLevelInterfaces*"] = &XTopLevelInterfaces::staticMetaObject;
  map["XToplevelInterface*"] = &XToplevelInterface::staticMetaObject;
  map["XDesignFiles*"] = &XDesignFiles::staticMetaObject;
  map["XLibraries*"] = &XLibraries::staticMetaObject;
  map["XInterfaces*"] = &XInterfaces::staticMetaObject;
  map["XInstance*"] = &XInstance::staticMetaObject;
  map["XInstances*"] = &XInstances::staticMetaObject;
  map["XLibrary*"] = &XLibrary::staticMetaObject;
  map["XDesignFile*"] = &XDesignFile::staticMetaObject;
  map["XDefines*"] = &XDefines::staticMetaObject;
  map["XDefine*"] = &XDefine::staticMetaObject;
  map["XInterface*"] = &XInterface::staticMetaObject;
  map["XInterfaceArchitectures*"] = &XInterfaceArchitectures::staticMetaObject;
  map["XInterfacePorts*"] = &XInterfacePorts::staticMetaObject;
  map["XInterfaceArchitecture*"] = &XInterfaceArchitecture::staticMetaObject;
  map["XInterfacePort*"] = &XInterfacePort::staticMetaObject;
  map["XInstancePorts*"] = &XInstancePorts::staticMetaObject;
  map["XInstancePort*"] = &XInstancePort::staticMetaObject;
  map["XParameters*"] = &XParameters::staticMetaObject;
  map["XParameter*"] = &XParameter::staticMetaObject;
}

void XDesignHierarchy::registerIntellisense(JavaScriptAPIs* apis)
{
  // Allows script typer to use XInterfacePort.<EnumName>
  apis->loadAPIForVariable("XInterfacePort", &XInterfacePort::staticMetaObject, true);
  
  // Allows script typer to use XDesignFile.<EnumName>
  apis->loadAPIForVariable("XDesignFile", &XDesignFile::staticMetaObject, true);

}

XInterfaceArchitectures::~XInterfaceArchitectures()
{
  while (!mArchitectures.isEmpty())
    delete mArchitectures.takeFirst();
}

XDefines::~XDefines()
{
  while (!mDefines.isEmpty())
    delete mDefines.takeFirst();
}

XInterfacePorts::~XInterfacePorts()
{
  while (!mPorts.isEmpty())
    delete mPorts.takeFirst();
}

XParameters::~XParameters()
{
  while (!mParameters.isEmpty())
    delete mParameters.takeFirst();
}

XInstancePorts::~XInstancePorts()
{
  while (!mPorts.isEmpty())
    delete mPorts.takeFirst();
}

XInstance::~XInstance()
{
}

XInstances::~XInstances()
{
  while (!mItems.isEmpty())
    delete mItems.takeFirst();
}

XDesignFile::~XDesignFile()
{
}

XDesignFiles::~XDesignFiles()
{
  while (!mFiles.isEmpty())
    delete mFiles.takeFirst();
}

XInterfaces::~XInterfaces()
{
  while (!mInterfaces.isEmpty())
    delete mInterfaces.takeFirst();
}

XDesignHierarchy::~XDesignHierarchy()
{
  qDebug() << "Deleting" << this;
}

XLibraries::~XLibraries()
{
  while (!mLibraries.isEmpty())
    delete mLibraries.takeFirst();
}

XDesignHierarchy::XDesignHierarchy(const QString &xmlSrcFile)
{
  qDebug() << "Constructing" << this << xmlSrcFile;
  parseFile(xmlSrcFile, NULL);
}

XInterface* XDesignHierarchy::findInterface(const QString& ifaceName)
{
  XInterface* iface = mInterfaces.findInterface(ifaceName);
  return iface;
}


XInstance* XToplevelInterface::findXInstance(const QString& instPath)
{
  QStringList pathLets = instPath.split(".");
  if (pathLets.count() > 0)
  {
    QString topName = pathLets[0];
    if (topName == getName())
    {
      XInstances* instances = &mInstances;
      QList<XInstance*>* instList = instances->getItems();
      XInstance* theInst = NULL;
      for (int i=1; i<pathLets.count(); i++)
      {
        if (instList == NULL)
          break;

        QString instName = pathLets[i];
        foreach (XInstance* inst, *instList)
        {
          if (inst->getFullName() == instPath)
            return inst;

          if (inst->getID() == instName)
          {
            theInst = inst;
            instList = inst->getChildren();
            break;
          }
        }
      }
    }
  }

  // No match
  return NULL;
}


bool XDesignHierarchy::parseFile(const QString& xmlFile,  XmlErrorHandler* eh)
{
  bool successFlag = true;

  if (eh == NULL)
    eh = &mErrorHandler;

  QFile file(xmlFile);

  if (file.open(QFile::ReadOnly | QFile::Text))
  {
    QDomDocument doc;
    int errorLine;
    int errorCol;
    QString errStr;

    QFileInfo templateFi(xmlFile);

    if (doc.setContent(&file, true, &errStr, &errorLine, &errorCol))
    { 
      QDomElement docElem = doc.documentElement();
      QDomNode n = docElem.firstChild();
      while(!n.isNull())
      {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        
        if (!e.isNull() && !e.isComment())
        {
          if ("ToplevelInterface" == e.tagName())
          {
            if (!mTopLevelInterfaces.deserialize(e, eh))
              return false;
          }
          else if ("Interfaces" == e.tagName())
          {
            if (!mInterfaces.deserialize(e, eh))
              return false;
          }
          else if ("Libraries" == e.tagName())
          {
            if (!mLibraries.deserialize(e, eh))
              return false;
          }
          else if ("Files" == e.tagName())
          {
            if (!mDesignFiles.deserialize(e, eh))
              return false;
          }

        }
        n = n.nextSibling();
      }

      // Now, fixup all interface references from the
      // instances
      for (int i=0; i<mTopLevelInterfaces.numInterfaces(); i++)
      {
        XToplevelInterface* topi = mTopLevelInterfaces.getAt(i);
        topi->fixReferences(&mInterfaces);
      }
    }
  }
  else
    qDebug() << "Error: no such file: " << xmlFile;

  return successFlag;
}

void XToplevelInterface::fixReferences(XInterfaces* ifaces)
{
  QList<XInstance*>* items = mInstances.getItems();

  foreach (XInstance* inst, *items)
    fixInstance(ifaces, inst);
}

void XToplevelInterface::fixInstance(XInterfaces* ifaces, XInstance* inst)
{
  QString ifaceName = inst->getInterfaceName();
  inst->setInterface(ifaces->findInterface(ifaceName));

  QList<XInstance*>* instances = inst->getChildren();
  foreach (XInstance* childInst, *instances)
    fixInstance(ifaces, childInst);
}

bool XInterfaces::deserialize(const QDomElement& e, XmlErrorHandler* eh)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    if (!element.isNull())
    {
      if ("Interface" == element.tagName())
      {
        XInterface* iface = new XInterface(this);
        if (!iface->deserialize(element, eh))
          return false;        
        addInterface(iface);
      }
    }
    n = n.nextSibling();
  }
  return true;
}

bool XLibraries::deserialize(const QDomElement& e, XmlErrorHandler* eh)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    if (!element.isNull())
    {
      if ("Library" == element.tagName())
      {
        XLibrary* iLib = new XLibrary(this);
        if (!iLib->deserialize(element, eh))
          return false;        
        addLibrary(iLib);
      }
    }
    n = n.nextSibling();
  }
  return true;
}

bool XDefine::deserialize(const QDomElement& e, XmlErrorHandler*)
{
  setName(e.attribute("name"));
  setValue(e.attribute("value"));
  setLocator(e.attribute("start"));

  return true;
}
bool XDefines::deserialize(const QDomElement& e, XmlErrorHandler* eh)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    if (!element.isNull())
    {
      if ("Define" == element.tagName())
      {
        XDefine* xdef = new XDefine();
        if (!xdef->deserialize(element, eh))
          return false;        
        addDefine(xdef);
      }
    }
    n = n.nextSibling();
  }
  return true;
}



bool XDesignFiles::deserialize(const QDomElement& e, XmlErrorHandler* eh)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    if (!element.isNull())
    {
      if ("File" == element.tagName())
      {
        XDesignFile* iFile = new XDesignFile(this);
        if (!iFile->deserialize(element, eh))
          return false;        
        addFile(iFile);
      }
    }
    n = n.nextSibling();
  }
  return true;
}

XInstance* XInstances::getInstance(int i)
{
  return mItems[i];
}


XInterfaceArchitecture* XInterface::findArchitecture(const QString& archName)
{
  return mArchitectures.findArchitecture(archName);
}


bool XInterface::deserialize(const QDomElement& e, XmlErrorHandler* eh)
{
  setLibrary(e.attribute("library"));
  setName(e.attribute("name"));
  setStart(e.attribute("start"));
  setEnd(e.attribute("end"));
  setLanguage(e.attribute("language"));

  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    if (!element.isNull() && !element.isComment())
    {
      if ("Parameters" == element.tagName())
      {
        if (!mParameters.deserialize(NULL, element, eh))
          return false;        
      }
      else if ("Ports" == element.tagName())
      {
        if (!mPorts.deserialize(this, element, eh)) 
          return false;        
      }
      else if ("Architectures" == element.tagName())
      {
        if (!mArchitectures.deserialize(this, element, eh)) 
          return false;        
      }
    }
    n = n.nextSibling();
  }
  return true;
}

bool XTopLevelInterfaces::deserialize(const QDomElement& e, XmlErrorHandler* eh)
{
  XToplevelInterface* xtop = new XToplevelInterface();
  mInterfaces.push_back(xtop);

  xtop->setLocator(e.attribute("locator"));
  xtop->setLibrary(e.attribute("library"));
  xtop->setName(e.attribute("name"));

  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    if (!element.isNull())
    {
      if ("Instance" == element.tagName())
      {
        if (!XInstance::deserialize(xtop, NULL, element, eh))
          return false;        
      }
    }
    n = n.nextSibling();
  }
  return true;
}

// this extra level of call nesting can be removed once support for enableLegacyGenerateHierarchyNames is removed
QString XInstance::getFullName()
{
  bool usingLegacyNames = false; // set this to the default value for -enableLegacyGenerateHierarchyNames/-disableLegacyGenerateHierarchyNames pair
  {
    // remove this section when -disableLegacyGenerateHierarchyNames is removed,
    // This scheme only looks for the *LegacyGeneateHierarchyNames switches in the "Additional Options" section
    // (they might appear in a -f file and they would be missed by this check)
    CarbonPropertyValue* additionalOptions = theApp->getContext()->getCarbonProjectWidget()->project()->getToolOptions("VSPCompiler")->getValue("Additional Options");
    QStringList optionList = additionalOptions->getValues();

    for (int i = 0; i < optionList.size(); i++)
    {
      if (optionList[i] == "-enableLegacyGenerateHierarchyNames") {
        usingLegacyNames = true;
      } else if (optionList[i] == "-disableLegacyGenerateHierarchyNames") {
        usingLegacyNames = false; // must look through all switches since multiple are allowed and last wins
      }
    }
  }
  

  if ( usingLegacyNames ) {
    return getFullNameLegacy();
  } else {
    return getFullNameNormal();
  }
}

// the normal version of getFullName()
QString XInstance::getFullNameNormal()
{
  QString name;
  QStringList names;
  names.push_front(mID);
  XInstance* parent = getParent();  

  while (parent != NULL)
  {
    names.push_front(parent->getID());
    parent = parent->getParent();
  }

  // The top level interface name is the root
  names.push_front(mToplevelInterface->getName());

  name = names.join(".");

  return name;
}

// This implementation of 'getFullName' (getFullNameLegacy())
//  constructs the full path name
// of instances nested underneath verilog generate
// labels using rules that match what is stored in
// the Carbon database. Unfortunately, these
// rules don't exactly match the Verilog standard.
//
// Specifically, instances underneath generate
// labels are given an instance name equal to
// the generate label(s) separated by "_",
// prefixed to the instance name.
//
// For example:
//
// module top (...);
// parameter A = 0;
// generate
//   begin: gentop
//   if (A == 0)
//      begin: gena
//          modname inst1 (...);  // top.gentop.gena.inst1
//      end // gena
//   end // gentop
// endgenerate
//
// endmodule // top
//
// The instance: "top.gentop.gena.inst1" will have the 
// following name in the Carbon database:
//
// top.gentop_gena_inst1
//
// As a complication, we have to deal with Verilog escaped
// names. When prefixing an instance name with generates,
// care has to be taken to handle generate labels
// that are escaped (e.g. \user-gen-label) or indexed
// generate labels that are not escaped in the design
// hierarchy file, but that NEED to be escaped when they
// are prefixed onto an instance label. E.G.
//
//   top.\genloop[0]_genblk1_inst1
//
// For more discussion, see Bug 14915
//
// Note that there is code in VerilogPopulate::getInstanceMangledName() 
// (in Populate.cxx) that implements a similar algorithm, but operates on
// the carbon database. (it is only used when mUseLegacyGenerateHierarchyNames is true)

// Helper for 'getFullNameLegacy'. Takes an array of LEGAL user
// supplied labels, or automatically generated labels of
// the form genblkN, or genloop[N] (created by the Carbon
// compiler for 'generates' as required by the Verilog standard).
// It then joins these into a properly escaped Carbon
// name (in the form shown above).

QString XInstance::constructEscapedName(QStringList& genLabels)
{
  
  QString esc = "\\";
  QString left = "[";
  QString right = "]";
  
  bool addEscapePrefix = false;
  
  for (int i = 0; i < genLabels.size(); i++)
  {
    // If any of the labels are escaped, then the final result
    // has to be escaped; and escapes internal to the label removed.
    if (genLabels[i].indexOf(esc) == 0)
    {
      addEscapePrefix = true;
      genLabels[i].remove(0, 1);
      // Should be whitespace at the end. 
      // Note: .designHierarchy file only seems to terminate escapes with
      // spaces, even if the original RTL had newline, or tab.
      int last = genLabels[i].size() - 1;
      const QChar lastchar = genLabels[i].at(last);
      if (lastchar.isSpace())
        genLabels[i].remove(last, 1);
    }

    // If there are any generate loops, then the final result
    // must be escaped.
    if ( (genLabels[i].indexOf(left) >= 0) || (genLabels[i].indexOf(right) >= 0) )
    {
      addEscapePrefix = true;
    }
  }
  
  QString path = genLabels.join("_");
  if (addEscapePrefix)
  {
    path.push_front("\\");
    path.push_back(" ");
  }
  
  return path;
}

// Return the full path name, following an old set of cbuild Verilog generate label rules
QString XInstance::getFullNameLegacy()
{
  // Final path name
  QString path;
  QStringList names;

  // Temporary generate label path 
  QString genPath;
  QStringList genLabels;

  // True if we're in process of concatenating generate labels as we
  // walk up the hierarchy.
  bool constructingTemp = false;

  // Walk up the hierarchy
  XInstance* current = this;
  while (current != NULL)
  {
    XInstance* parent = current->getParent();
    bool parentIsGenLabel = (parent && parent->isVlogGenerateLabel());
    bool currentIsGenLabel = (current && current->isVlogGenerateLabel());
    if (!constructingTemp)
    {
      // If this is an instance with a generate label above, then we need to concatenate
      if (!currentIsGenLabel && parentIsGenLabel)
      {
        constructingTemp = true;
        genPath.clear();
        genLabels.push_front(current->getID());
      }
      // Otherwise build normal '.' separated names
      else
        names.push_front(current->getID());
    }
    // We're in the process of concatenating generate labels. We're done when we hit 
    // an instance that isn't a generate label. The top instance will never be a generate
    // label, so we're guaranteed to stop there.
    else
    {
      if (currentIsGenLabel)
        genLabels.push_front(current->getID());
      else // Done
      {
        names.push_front(constructEscapedName(genLabels));
        if (parentIsGenLabel)
        {
          // Start a new genPath construction
          genLabels.clear();
          genLabels.push_front(current->getID());
        }
        else
        {
          names.push_front(current->getID());
          constructingTemp  = false;
        }
      }
    }

    current = parent;
  }

  // Finish up if we were in the process of concatenating generate label names
  if (constructingTemp)
    names.push_front(constructEscapedName(genLabels));
  
  // The top level interface name is the root
  names.push_front(mToplevelInterface->getName());

  path = names.join(".");

  return path;
}

bool XInstance::deserialize(XToplevelInterface* iTop, XInstance* parentInst, const QDomElement& e, XmlErrorHandler* eh)
{
  XInstance* inst = new XInstance(iTop);

  inst->setParent(parentInst);

  if (parentInst == NULL)
    iTop->addInstance(inst);
  else
    parentInst->mChildren.addInstance(inst);

  inst->setID(e.attribute("id"));
  inst->setLocator(e.attribute("locator"));
  inst->setInterfaceName(e.attribute("interface"));
  inst->setInterface(NULL);
  inst->setArchitectureName(e.attribute("architecture"));
  inst->setInterfaceType(e.attribute("type"));
 
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    if (!element.isNull() && !element.isComment())
    {
      if ("Parameters" == element.tagName())
      {
        if (!inst->mParameters.deserialize(inst, element, eh))
          return false;        
      }
      else if ("Ports" == element.tagName())
      {
        if (!inst->mPorts.deserialize(inst, element, eh))
          return false;        
      }
      else if ("Instance" == element.tagName())
      {
        XInstance::deserialize(iTop, inst, element, eh);
      }
    }
    n = n.nextSibling();
  }
  return true;
}

bool XParameters::deserialize(XInstance* inst, const QDomElement& e, XmlErrorHandler* eh)
{
  mInstance = inst;

  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    if (!element.isNull() && !element.isComment())
    {
      if ("Parameter" == element.tagName())
      {
        if (!XParameter::deserialize(this, element, eh))
          return false;        
      }
    }
    n = n.nextSibling();
  }

  return true;
}



bool XParameter::deserialize(XParameters* params, const QDomElement& e, XmlErrorHandler*)
{
  XParameter* param = new XParameter();
  params->addParameter(param);

  param->setName(e.attribute("name"));
  param->setType(e.attribute("type"));
  param->setValue(e.attribute("value"));

  return true;
}


bool XInstancePort::deserialize(XInstancePorts* ports, const QDomElement& e, XmlErrorHandler*)
{
  XInstancePort* port = new XInstancePort();
  ports->addPort(port);

  port->setActual(e.attribute("actual"));
  port->setFormal(e.attribute("formal"));

  return true;
}


bool XInstancePorts::deserialize(XInstance* inst, const QDomElement& e, XmlErrorHandler* eh)
{
  mInstance = inst;

  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    if (!element.isNull() && !element.isComment())
    {
      if ("Port" == element.tagName())
      {
        if (!XInstancePort::deserialize(this, element, eh))
          return false;        
      }
    }
    n = n.nextSibling();
  }
  return true;
}

//
bool XInterfacePort::deserialize(XInterfacePorts* ports, const QDomElement& e, XmlErrorHandler*)
{
  XInterfacePort* port = new XInterfacePort();

  port->setName(e.attribute("name"));
  port->setType(e.attribute("type"));
  port->setDirection(e.attribute("direction"));
  port->setWidth(e.attribute("width"));

  ports->addPort(port);

  return true;
}

bool XInterfaceArchitecture::deserialize(XInterfaceArchitectures* architectures, const QDomElement& e, XmlErrorHandler*)
{
  XInterfaceArchitecture* architecture = new XInterfaceArchitecture();

  architecture->setName(e.attribute("name"));
  architecture->setLibrary(e.attribute("lib"));
  architecture->setStart(e.attribute("start"));
  architecture->setEnd(e.attribute("end"));

  architectures->addArchitecture(architecture);

  return true;
}


bool XInterfacePorts::deserialize(XInterface* iface, const QDomElement& e, XmlErrorHandler* eh)
{
  mInterface = iface;

  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    if (!element.isNull() && !element.isComment())
    {
      if ("Port" == element.tagName())
      {
        if (!XInterfacePort::deserialize(this, element, eh))
          return false;        
      }
    }
    n = n.nextSibling();
  }
  return true;
}

bool XInterfaceArchitectures::deserialize(XInterface* iface, const QDomElement& e, XmlErrorHandler* eh)
{
  mInterface = iface;

  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    if (!element.isNull() && !element.isComment())
    {
      if ("Architecture" == element.tagName())
      {
        if (!XInterfaceArchitecture::deserialize(this, element, eh))
          return false;        
      }
    }
    n = n.nextSibling();
  }
  return true;
}

bool XLibrary::deserialize(const QDomElement& e, XmlErrorHandler*)
{
  setDefault(e.attribute("default") == "true");
  setName(e.attribute("name"));
  setPath(e.attribute("path"));

  return true;
}

XDesignFile::DesignFileKind XDesignFile::stringToKind(const QString& k)
{
  QString kind = k.toLower();

  if ("entity" == kind)
    return eKindEntity;
  else if ("architecture" == kind)
    return eKindArchitecture;
  else if ("package declaration" == kind)
    return eKindPackageDeclaration;
  else if ("package body" == kind)
    return eKindPackageBody;
  else if ("configuration" == kind)
    return eKindConfiguration;
  else if ("module" == kind)
    return eKindModule;
  else if ("verilog library" == kind)
    return eKindVerilogLibrary;
  else if ("included file" == kind)
    return eKindIncludeFile;
  else if ("unknown" == kind)
    return eKindUnknown;

  return eKindUnknown;
}

bool XDesignFile::deserialize(const QDomElement& e, XmlErrorHandler* eh)
{
  setName(e.attribute("name"));
  setLanguage(e.attribute("language"));
  if (!e.attribute("type").isEmpty())
    setKind(stringToKind(e.attribute("type")));

  QDomElement defines = e.firstChildElement("Defines");
  if (!defines.isNull())
  {
    QDomNode n = defines.firstChild();
    while(!n.isNull())
    {
      QDomElement e = n.toElement(); // try to convert the node to an element.
      n = n.nextSibling();

      if (!e.isNull() && !e.isComment())
      {
        if ("Define" == e.tagName())
        {
          XDefine* xdef = new XDefine();
          xdef->deserialize(e, eh);
          mDefines.addDefine(xdef);
        }
      }
    }    
  }
  return true;
}



// Dump
void XDesignHierarchy::dump()
{ 
  mTopLevelInterfaces.dump();
  mInterfaces.dump();
}

void XTopLevelInterfaces::dump()
{
  foreach (XToplevelInterface* itop, mInterfaces)
  {
    itop->dump();
  }
}

void XInstance::dump()
{
  qDebug() << "instance" << getInterfaceName() << getID() << getLocator() << getArchitectureName();

  mParameters.dump();
  mPorts.dump();
}


void XToplevelInterface::dump()
{
  QList<XInstance*>* instances = mInstances.getItems();
  foreach (XInstance* inst, *instances)
  {
    inst->dump();
  }
}


void XInterfaces::dump()
{
  foreach (XInterface* iface, mInterfaces)
  {
    iface->dump();
  }
}

void XLibraries::dump()
{
  foreach (XLibrary* iLib, mLibraries)
  {
    iLib->dump();
  }
}

void XInterface::dump()
{
  qDebug() << "interface" << getLibrary() << getName() << getStart() << getEnd() << getLanguage();

  mPorts.dump();
  mParameters.dump();
  mArchitectures.dump();
}

void XLibrary::dump()
{
  qDebug() << "Library" << getDefault() << getName() << getPath();
}

void XParameters::dump()
{
  foreach (XParameter* param, mParameters)
  {
    param->dump();
  }
}

void XParameter::dump()
{
  qDebug() << "parameter" << getName() << getType() << getValue();
}

void XInstancePorts::dump()
{
  foreach (XInstancePort* port, mPorts)
  {
    port->dump();
  }
}

void XInstancePort::dump()
{
  qDebug() << "port" << getActual() << getFormal();
}

void XInterfacePorts::dump()
{
  foreach (XInterfacePort* port, mPorts)
  {
    port->dump();
  }
}

void XInterfaceArchitectures::dump()
{
  foreach (XInterfaceArchitecture* architecture, mArchitectures)
  {
    architecture->dump();
  }
}

void XInterfacePort::dump()
{
  qDebug() << "iport" << getName() << getType() << getDirection() << getWidth();
}

void XInterfaceArchitecture::dump()
{
  qDebug() << "iport" << getName() << getLibrary() << getStart() << getEnd();
}


void XDesignFile::dump()
{
  qDebug() << "designFile" << getName() << "language" << getLanguage();
}

