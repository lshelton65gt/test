//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/XmlParsing.h"

#include "gui/CQt.h"
#include "util/CarbonAssert.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"

#include "cmm.h"
#include "CarbonMakerContext.h"
#include "CarbonProperties.h"
#include "CarbonCompilerTool.h"
#include "CarbonPropertyFilelist.h"


static bool isPropertyEnabled(CarbonMakerContext* ctx, const QString& licenseEnabled)
{
  if (licenseEnabled.length() > 0)
  {
    if (licenseEnabled.toLower() == "timebomb")
    {
      if (ctx->getCanTimeBomb() && !ctx->getTimeBombDate().isNull())
        return true;
      else
        return false;
    } else if (licenseEnabled.toLower() == "socdsystemcexport") {
      return ctx->getCanGenSoCDSystemCExport();
    }
    // future: check against feature(s) here when we need this
    return false;
  }
  return true;
}

CarbonPropertyGroup* CarbonProperties::addGroup(const char* name)
{
  CarbonPropertyGroup* newGroup = new CarbonPropertyGroup(this);
  newGroup->putGroup(name);
  mPropertyGroups.insert(mPropertyGroups.begin(), newGroup);
  return newGroup;
}

void CarbonProperties::addProperty(CarbonPropertyGroup* group, CarbonProperty* prop)
{
  group->addProperty(prop); 
}

bool CarbonProperties::parseOption(xmlNodePtr parent, CarbonPropertyGroup* group, UtXmlErrorHandler* eh)
{
  CarbonMakerContext* ctx = theApp->getContext(); 
  bool requiredValue = false;
  CarbonProperty::ResetTo resetTo = CarbonProperty::eResetDefault;

  UtString name;
  XmlParsing::getProp(parent, "name", &name);

  UtString legalPattern;
  if (XmlParsing::hasProp(parent, "legalPattern"))
    XmlParsing::getProp(parent, "legalPattern", &legalPattern);

  UtString licenseEnabled;
  if (XmlParsing::hasProp(parent, "licenseEnabled"))
    XmlParsing::getProp(parent, "licenseEnabled", &licenseEnabled);

  if (XmlParsing::hasProp(parent, "reset"))
  {
    UtString resetAtt;
    XmlParsing::getProp(parent, "reset", &resetAtt);
    if (0 == strcmp(resetAtt.c_str(), "default"))
      resetTo = CarbonProperty::eResetDefault;
    else if (0 == strcmp(resetAtt.c_str(), "guidefault"))
      resetTo = CarbonProperty::eResetGUIDefault;
  }

  if (XmlParsing::hasProp(parent, "required"))
  {
    UtString requiredAtt;
    XmlParsing::getProp(parent, "required", &requiredAtt);
    if (0 == strcmp(requiredAtt.c_str(), "true"))
      requiredValue = true;
  }

  UtString exclusiveWith;
  XmlParsing::getProp(parent, "exclusiveWith", &exclusiveWith);

  bool readOnly=false;
  UtString readonly;
  XmlParsing::getProp(parent, "ReadOnly", &readonly);

  bool displayOnly=false;
  UtString displayonly;
  XmlParsing::getProp(parent, "DisplayOnly", &displayonly);

  if (0 == strcmp(readonly.c_str(), "true"))
    readOnly = true;

  if (0 == strcmp(displayonly.c_str(), "true"))
    displayOnly = true;

  UtString description;
  int numValues=0;

  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    UtString content;
    XmlParsing::getContent(child, &content);
    
	UtString guiDefaultValue;
	bool hasguiDefault = false;
	UtString defaultValue;

    bool hasDefault = false;

    for (xmlNodePtr defchild = child->children; defchild != NULL; defchild = defchild->next)
    {
      if (XmlParsing::isElement(defchild, "default"))
      {
        hasDefault = true;
        XmlParsing::getContent(defchild, &defaultValue);
      }
	  else if (XmlParsing::isElement(defchild, "guidefault"))
	  {
        hasguiDefault = true;
        XmlParsing::getContent(defchild, &guiDefaultValue);
	  }
    }

    if (XmlParsing::isElement(child, "numValues"))
    {
      UtIStringStream is(content);
      is >> numValues;
    }
    else if (XmlParsing::isElement(child, "description"))
    {
      description = content;
    }
    else if (XmlParsing::isElement(child, "enum"))
    {
      CarbonPropertyEnum* sw = new CarbonPropertyEnum(name.c_str(), numValues, description.c_str());
      sw->putLicenseEnabled(licenseEnabled.c_str());
      sw->putReadOnly(readOnly);
      sw->putDisplayOnly(displayOnly);
      sw->putExclusiveWith(exclusiveWith.c_str());
      if (hasDefault)
        sw->putDefaultValue(defaultValue.c_str());
      sw->putResetTo(resetTo);
      sw->parseXML(child, eh);
      description.clear(); numValues=0;

      if (isPropertyEnabled(ctx, QString(licenseEnabled.c_str())))
        group->addProperty(sw); 
    }
    else if (XmlParsing::isElement(child, "bool"))
    {
      CarbonPropertyBool* sw = new CarbonPropertyBool(name.c_str(), numValues, description.c_str());
      sw->putReadOnly(readOnly);
      sw->putResetTo(resetTo);
      sw->putLicenseEnabled(licenseEnabled.c_str());

      bool hasTrueValue = XmlParsing::hasProp(parent, "trueValue");
      UtString trueValue;
      XmlParsing::getProp(parent, "trueValue", &trueValue);

      bool hasFalseValue = XmlParsing::hasProp(parent, "falseValue");
      UtString falseValue;
      XmlParsing::getProp(parent, "falseValue", &falseValue);

      if (hasTrueValue)
        sw->setTrueValue(trueValue.c_str());

      if (hasFalseValue)
        sw->setFalseValue(falseValue.c_str());

      sw->putDisplayOnly(displayOnly);
      sw->putExclusiveWith(exclusiveWith.c_str());
      if (hasDefault)
        sw->putDefaultValue(defaultValue.c_str());

      if (hasguiDefault)
        sw->putGuiDefaultValue(guiDefaultValue.c_str());

      sw->parseXML(child, eh);
      description.clear(); numValues=0;
      if (isPropertyEnabled(ctx, QString(licenseEnabled.c_str())))
        group->addProperty(sw); 
   }
    else if (XmlParsing::isElement(child, "integer"))
    {
      CarbonPropertyInteger* sw = new CarbonPropertyInteger(name.c_str(), numValues, description.c_str());
      sw->putLicenseEnabled(licenseEnabled.c_str());
      sw->putReadOnly(readOnly);
      sw->putDisplayOnly(displayOnly);
      sw->putExclusiveWith(exclusiveWith.c_str());
      sw->putRequired(requiredValue);
      sw->putResetTo(resetTo);

      if (hasDefault)
        sw->putDefaultValue(defaultValue.c_str());
      
      sw->parseXML(child, eh);
      description.clear(); numValues=0;
      if (isPropertyEnabled(ctx, QString(licenseEnabled.c_str())))
        group->addProperty(sw); 
    }
    else if (XmlParsing::isElement(child, "infile"))
    {
      CarbonPropertyInfile* sw = new CarbonPropertyInfile(name.c_str(), numValues, description.c_str());
      sw->putLicenseEnabled(licenseEnabled.c_str());
      sw->putReadOnly(readOnly);
      sw->putLegalPattern(legalPattern.c_str());
      sw->putResetTo(resetTo);
      sw->putDisplayOnly(displayOnly);
      sw->putExclusiveWith(exclusiveWith.c_str());
      if (hasDefault)
        sw->putDefaultValue(defaultValue.c_str());
      sw->putRequired(requiredValue);
      sw->parseXML(child, eh);
      description.clear(); numValues=0;
      if (isPropertyEnabled(ctx, QString(licenseEnabled.c_str())))
        group->addProperty(sw); 
    }
    else if (XmlParsing::isElement(child, "filelist"))
    {
      CarbonPropertyFilelist* sw = new CarbonPropertyFilelist(name.c_str(), numValues, description.c_str());
      sw->putLicenseEnabled(licenseEnabled.c_str());
      sw->putReadOnly(readOnly);
      sw->putLegalPattern(legalPattern.c_str());
      sw->putResetTo(resetTo);
      sw->putDisplayOnly(displayOnly);
      sw->putRequired(requiredValue);
      sw->putExclusiveWith(exclusiveWith.c_str());
      if (hasDefault)
        sw->putDefaultValue(defaultValue.c_str());

      sw->parseXML(child, eh);
      description.clear(); numValues=0;
      if (isPropertyEnabled(ctx, QString(licenseEnabled.c_str())))
        group->addProperty(sw); 
    }
    else if (XmlParsing::isElement(child, "outfile"))
    {
      CarbonPropertyOutfile* sw = new CarbonPropertyOutfile(name.c_str(), numValues, description.c_str());

      bool nameOnly = XmlParsing::hasProp(child, "nameOnly");
      sw->putLicenseEnabled(licenseEnabled.c_str());
      sw->putReadOnly(readOnly);
      sw->setNameOnly(nameOnly);
      sw->putLegalPattern(legalPattern.c_str());
      sw->putResetTo(resetTo);
      sw->putDisplayOnly(displayOnly);
      sw->putRequired(requiredValue);
      sw->putExclusiveWith(exclusiveWith.c_str());
      if (hasDefault)
        sw->putDefaultValue(defaultValue.c_str());

      sw->parseXML(child, eh);
      description.clear(); numValues=0;
      if (isPropertyEnabled(ctx, QString(licenseEnabled.c_str())))
        group->addProperty(sw); 
    }
    else if (XmlParsing::isElement(child, "string"))
    {
      CarbonPropertyString* sw = new CarbonPropertyString(name.c_str(), numValues, description.c_str());
      sw->putLicenseEnabled(licenseEnabled.c_str());
      sw->putReadOnly(readOnly);
      sw->putLegalPattern(legalPattern.c_str());
      sw->putResetTo(resetTo);
      sw->putDisplayOnly(displayOnly);
      sw->putRequired(requiredValue);
      sw->putExclusiveWith(exclusiveWith.c_str());
      if (hasDefault)
        sw->putDefaultValue(defaultValue.c_str());

      sw->parseXML(child, eh);
      description.clear(); numValues=0;
      if (isPropertyEnabled(ctx, QString(licenseEnabled.c_str())))
        group->addProperty(sw); 
    }
    else if (XmlParsing::isElement(child, "password"))
    {
      CarbonPropertyPassword* sw = new CarbonPropertyPassword(name.c_str(), numValues, description.c_str());
      sw->putLicenseEnabled(licenseEnabled.c_str());
      sw->putReadOnly(readOnly);
      sw->putLegalPattern(legalPattern.c_str());
      sw->putResetTo(resetTo);
      sw->putDisplayOnly(displayOnly);
      sw->putRequired(requiredValue);
      sw->putExclusiveWith(exclusiveWith.c_str());
      if (hasDefault)
        sw->putDefaultValue(defaultValue.c_str());

      sw->parseXML(child, eh);
      description.clear(); numValues=0;
      if (isPropertyEnabled(ctx, QString(licenseEnabled.c_str())))
        group->addProperty(sw); 
    }
    else if (XmlParsing::isElement(child, "datestamp"))
    {
      CarbonPropertyDate* sw = new CarbonPropertyDate(name.c_str(), numValues, description.c_str());
      sw->putLicenseEnabled(licenseEnabled.c_str());
      sw->putReadOnly(readOnly);
      sw->putLegalPattern(legalPattern.c_str());
      sw->putResetTo(resetTo);
      sw->putDisplayOnly(displayOnly);
      sw->putRequired(requiredValue);
      sw->putExclusiveWith(exclusiveWith.c_str());
      if (hasDefault)
        sw->putDefaultValue(defaultValue.c_str());

      sw->parseXML(child, eh);
      description.clear(); numValues=0;
      if (isPropertyEnabled(ctx, QString(licenseEnabled.c_str())))
        group->addProperty(sw); 
    }

    else if (XmlParsing::isElement(child, "stringlist"))
    {
      CarbonPropertyStringlist* sw = new CarbonPropertyStringlist(name.c_str(), numValues, description.c_str());
      sw->putLicenseEnabled(licenseEnabled.c_str());
      sw->putReadOnly(readOnly);
      sw->putLegalPattern(legalPattern.c_str());
      sw->putResetTo(resetTo);
      sw->putRequired(requiredValue);
      sw->putDisplayOnly(displayOnly);
      sw->putExclusiveWith(exclusiveWith.c_str());

      if (hasDefault)
        sw->putDefaultValue(defaultValue.c_str());

      sw->parseXML(child, eh);
      description.clear(); numValues=0;
      if (isPropertyEnabled(ctx, QString(licenseEnabled.c_str())))
        group->addProperty(sw); 
    }
    else if (XmlParsing::isTextNode(child) || XmlParsing::isCommentNode(child))
    {
      ;
    }
    else
    {
      UtString err;
      err << "Error: Unknown option: " << XmlParsing::elementName(child);
      eh->reportError(err.c_str());
      INFO_ASSERT(false, err.c_str());
    }
  }

  return true;
}

CarbonProperties::CarbonProperties()
{
}

CarbonPropertyGroup* CarbonProperties::findGroup(const char* groupName)
{
  for (UInt32 i=0; i<numGroups(); ++i)
  {
    CarbonPropertyGroup* group = getGroup(i);
    if (0 == strcmp(group->getName(), groupName))
      return group;
  }
  return NULL;
}

const CarbonProperty* CarbonProperties::findProperty(const char* name) const
{
  for (UInt32 i=0; i<numGroups(); ++i) {
    CarbonPropertyGroup* group = getGroup(i);
    const CarbonProperty* sw = findProperty(group, name);
    if (sw != NULL) {
      return sw;
    }
  }
  return NULL;
}

// Remove a group and all its switches
bool CarbonProperties::removeGroup(const char* groupName)
{
  QList<const CarbonProperty*> removeList;

  for (UInt32 i=0; i<numGroups(); ++i)
  {
    CarbonPropertyGroup* group = getGroup(i);
    if (0 == strcmp(group->getName(), groupName))
    {
      // We cant delete them during walking, so build a list, then delete them
      for (CarbonPropertyGroup::SwitchLoop p = group->loopSwitches(); !p.atEnd(); ++p)
      {
        const CarbonProperty* sw = p.getValue();
        removeList.append(sw);
      }
      foreach (const CarbonProperty* prop, removeList)
        removeProperty(prop);

      qDebug() << "count: " << mPropertyGroups.count();
      mPropertyGroups.removeAll(group);
      qDebug() << "count: " << mPropertyGroups.count();
      break;
    }
  }
  return true;
}

bool CarbonProperties::removeProperty(const CarbonProperty* prop)
{
  for (UInt32 i=0; i<numGroups(); ++i)
  {
    CarbonPropertyGroup* group = getGroup(i);
    group->removeProperty(prop->getName());
  }

  return true;
}


CarbonProperty* CarbonProperties::getProperty(const char* name) const
{
  for (UInt32 i=0; i<numGroups(); ++i)
  {
    CarbonPropertyGroup* group = getGroup(i);
    for (CarbonPropertyGroup::SwitchLoop p = group->loopSwitches(); !p.atEnd(); ++p)
    {
      CarbonProperty* sw = p.getValue();
      if (0 == strcmp(sw->getName(), name))
        return sw;
    }
  }
  return NULL;
}


const CarbonProperty* CarbonProperties::findProperty(CarbonPropertyGroup* group, const char* name) const
{
  for (CarbonPropertyGroup::SwitchLoop p = group->loopSwitches(); !p.atEnd(); ++p) {
    CarbonProperty* sw = p.getValue();
    if (0 == strcmp(sw->getName(), name)) {
      return sw;
    }
  }
  return NULL;
}

// This is slightly different from findProperty in that the name passed in
// could be like +define+CARBON (because it was read from a .cmd file)
// so we need to do special handling for these types
const CarbonProperty* CarbonProperties::locateProperty(const char* name) const
{
  UtString modName;

  QRegExp plusSwitch("\\+(.*)\\+(.*)");
  if (plusSwitch.exactMatch(name))
    modName << "+" << plusSwitch.cap(1) << "+";
  else
    modName << name;

  const char* modifiedName = modName.c_str();

  for (UInt32 i=0; i<numGroups(); ++i)
  {
    CarbonPropertyGroup* group = getGroup(i);
    for (CarbonPropertyGroup::SwitchLoop p = group->loopSwitches(); !p.atEnd(); ++p)
    {
      CarbonProperty* sw = p.getValue();
      if (0 == strcmp(sw->getName(), modifiedName))
        return sw;
    }
  }
  return NULL;
}

bool CarbonProperties::parseSection(xmlNodePtr parent, UtXmlErrorHandler* eh)
{
  UtString name;
  XmlParsing::getProp(parent, "name", &name);
  CarbonPropertyGroup* newGroup = new CarbonPropertyGroup(this);
  newGroup->putGroup(name.c_str());
  mPropertyGroups.push_back(newGroup);

  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "option"))
      parseOption(child, newGroup, eh);
  }

  return true;
}

bool CarbonProperties::read(xmlNodePtr parent, UtXmlErrorHandler* eh)
{
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "section"))
      parseSection(child, eh);
  }

  return true;
}

bool CarbonProperties::readPropertyDefinitions(const char* xmlFile)
{
  bool status = false;

  QFile xf(xmlFile);
  if (xf.open(QFile::ReadOnly))
  {
    QTextStream ts(&xf);
    QString buffer = ts.readAll();
    UtString sbuffer;
    sbuffer << buffer;

    xmlDocPtr doc = xmlReadMemory(sbuffer.c_str(), buffer.length(), xmlFile, NULL, 0);
    if (doc == NULL) 
    {
      UtString message;
      message << "Failed to parse file " <<  xmlFile;
      qDebug() << message.c_str();
    }
    else
    {
      UtXmlErrorHandler eh;
      xmlNode *rootNode = xmlDocGetRootElement(doc);
      INFO_ASSERT(xmlStrcmp(rootNode->name, BAD_CAST "Properties") == 0, "Expecting Properties");

      status = read(rootNode, &eh);
      if (eh.getError())
      {
        //reportError(eh.errorText());
        status = false;
      }

      xmlFreeDoc(doc);
    }
  }
  return status;
}

void CarbonDelegate::setValue(CarbonPropertyValue*, QTreeWidgetItem* item, QAbstractItemModel *model, const QModelIndex &index, const char* currentValue, const char* newValue, const CarbonProperty* sw) const
{
  if (0 != strcmp(currentValue, newValue))
  {
    CarbonOptions* settings = mEditor->getSettings();
    bool isDefaulted, isThisLevel;
    settings->getValue(sw->getName(),&isDefaulted,&isThisLevel);

    QFont f = item->font(PropertyEditor::colVALUE);
    bool bolded = false;
    bool internalProperty = sw->getReadOnly() || sw->getDisplayOnly();
    if ( !internalProperty && isThisLevel)
      bolded = true;

    f.setBold(bolded);
    item->setFont(PropertyEditor::colVALUE, f);
    
    if (mEditor)
      mEditor->getSettings()->putValue(sw, newValue);
    
    if (model)
      model->setData(index, newValue);
  }
}

