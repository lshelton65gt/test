//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "ModelKitWizard.h"
#include "CarbonMakerContext.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"
#include "CarbonComponent.h"
#include "KitManifest.h"

#define LAST_KIT "versions/lastKit"
#define LAST_CHOICE "versions/lastChoice"

MKWPageFinished::MKWPageFinished(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);

  setTitle("Kit Generation");
  setSubTitle("Summary");
  mTerminatePage = false;
}

MKWPageFinished::~MKWPageFinished()
{
}
void MKWPageFinished::addStatus(const QString& msg)
{
  qDebug() << msg;
  ui.textEdit->append(msg);
}

void MKWPageFinished::processEmbedded(const QDomElement& parent, ModelKit*, KitManifest* manifest)
{
  QDomElement e = parent.firstChildElement("Embed").toElement();
  while (!e.isNull())
  {
    QString source = e.attribute("source");
    QFileInfo fi(source);
    if (fi.isRelative())
      source = theApp->getWorkingDirectory() + "/" + source;

    QString destination = e.attribute("destination");

    if (!manifest->hasFile(destination))
    {
      addStatus(QString("Embedding File %1...(%2)")
        .arg(destination)
        .arg(source)
        );

      KitFile* kf = manifest->addFile(source, destination, ZstreamZip::eFileTxt, true);
      bool retainFile = true;
      QString retain = e.attribute("retained");
      if (!retain.isEmpty())
      {
        retainFile = retain == "true" ? true : false;
        kf->setRetained(retainFile);
      }
    }

    e = e.nextSiblingElement("Embed");
  }
}


void MKWPageFinished::processCompilerSwitches(const QDomElement& parent, ModelKit*, KitManifest* manifest)
{
  QDomElement e = parent.firstChildElement("CompilerSwitch").toElement();
  while (!e.isNull())
  {
    QString name = e.attribute("name");
    QString value = e.attribute("value");

    addStatus(QString("Adding Switch %1 %2)")
        .arg(name)
        .arg(value)
        );

    manifest->addCompilerSwitch(name, value);
      
    e = e.nextSiblingElement("CompilerSwitch");
  }
}

void MKWPageFinished::processModelKitFeatures(const QDomElement& parent, ModelKit*, KitManifest* manifest)
{
  QDomElement e = parent.firstChildElement("ModelKitFeature").toElement();
  while (!e.isNull())
  {
    QString name = e.attribute("name");

    addStatus(QString("Adding Model Kit Feature %1")
        .arg(name)
        );

    manifest->addModelKitFeature(name);
      
    e = e.nextSiblingElement("ModelKitFeature");
  }
}
void MKWPageFinished::processPreCompilationScripts(const QDomElement& parent, ModelKit*, KitManifest* manifest)
{
  QDomElement e = parent.firstChildElement("PreCompilationScript").toElement();
  while (!e.isNull())
  {
    QString source = e.attribute("source");
    QFileInfo fi(source);
    if (fi.isRelative())
      source = theApp->getWorkingDirectory() + "/" + source;

    QString destination = e.attribute("destination");

    QString prependValue = e.attribute("prepend").toLower();
    bool prependFlag = (!prependValue.isEmpty() && (prependValue == "true")) ? true : false;

    if (!manifest->hasFile(destination))
    {
      KitActionRunScript* rs = new KitActionRunScript();
      rs->setEventTime(KitActionRunScript::BeforeModelCompilation);
      rs->setScriptName(destination);

      manifest->addFile(source, destination, ZstreamZip::eFileTxt, true, prependFlag);
      addStatus(QString("Embedding Pre Compilation Script File %1...(%2) prepend=%3")
        .arg(destination)
        .arg(source)
        .arg(prependFlag)
        );

      manifest->addPreUserAction(rs, prependFlag);
    }

    e = e.nextSiblingElement("PreCompilationScript");
  }
}

void MKWPageFinished::processPostCompilationScripts(const QDomElement& parent, ModelKit*, KitManifest* manifest)
{
  QDomElement e = parent.firstChildElement("PostCompilationScript").toElement();
  while (!e.isNull())
  {
    QString source = e.attribute("source");
    QFileInfo fi(source);
    if (fi.isRelative())
      source = theApp->getWorkingDirectory() + "/" + source;

    QString destination = e.attribute("destination");

    QString prependValue = e.attribute("prepend").toLower();
    bool prependFlag = (!prependValue.isEmpty() && (prependValue == "true")) ? true : false;


    if (!manifest->hasFile(destination))
    {
      manifest->addFile(source, destination, ZstreamZip::eFileTxt, true, prependFlag);
      addStatus(QString("Embedding Post Compilation Script File %1...(%2) prepend=%3")
         .arg(destination)
         .arg(source)
         .arg(prependFlag)
         );

      KitActionRunScript* rs = new KitActionRunScript();
      rs->setEventTime(KitActionRunScript::AfterModelCompilation);
      rs->setScriptName(destination);

      manifest->addPostUserAction(rs, prependFlag);
    }

    e = e.nextSiblingElement("PostCompilationScript");
  }
}



void MKWPageFinished::addManifest(ModelKit* kit, KitManifest* manifest)
{
  QString userManifest = theApp->argumentValue("-modelKitManifest");
  QFileInfo fi(userManifest);
  if (fi.isRelative())
    userManifest = theApp->getWorkingDirectory() + "/" + userManifest;
  
  QFile file(userManifest);

  if (file.open(QFile::ReadOnly | QFile::Text))
  {
    QDomDocument doc;
    int errorLine;
    int errorCol;
    QString errStr;

    QFileInfo templateFi(userManifest);

    if (doc.setContent(&file, true, &errStr, &errorLine, &errorCol))
    { 
      QDomElement rootElem = doc.documentElement();
      if (rootElem.tagName() == "ModelKitManifest")
      {
        processCompilerSwitches(rootElem, kit, manifest);
        processEmbedded(rootElem, kit, manifest);
        processPostCompilationScripts(rootElem, kit, manifest);
        processPreCompilationScripts(rootElem, kit, manifest);
        processModelKitFeatures(rootElem, kit, manifest);
      }
      else
        qDebug() << "Error: Illegal manifest file" << userManifest;
    }
  }
  else
  {
    QString msg = QString("Error: no such manifest file: %1").arg(userManifest);
    QMessageBox::critical(NULL, "Kit Error", msg);
  }
}

void MKWPageFinished::initializePage()
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  ui.textEdit->clear();

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  ModelKitWizard* wiz = (ModelKitWizard*)wizard();
  ModelKit* kit = wiz->getModelKit();
  KitManifest* mf = wiz->getActiveManifest();

  UtString kitName;
  kitName << kit->getName() << ".modelKit";
  UtString outName;
  OSConstructFilePath(&outName, proj->getProjectDirectory(), "modelKit");
  OSConstructFilePath(&outName, outName.c_str(), kitName.c_str());

  addStatus(QString("Generating %1...").arg(outName.c_str()));
  
  mf->removeAllItems();

  // for each file
  foreach (QString fileName, wiz->getFileList())
  {
    QString actualFile = wiz->expandName(fileName);
    QString normalizedName = wiz->normalizeProjectName(fileName);
    mf->addFile(actualFile, normalizedName);
    addStatus(QString("Embedding %1...(%2)")
      .arg(normalizedName)
      .arg(actualFile)
      );
  }

  foreach (QString fileName, wiz->getUserFileList())
  {
    QString actualFile = wiz->expandName(fileName);
    QString normalizedName = wiz->normalizeProjectName(fileName);
    mf->addFile(actualFile, normalizedName, ZstreamZip::eFileTxt, true);
    addStatus(QString("Embedding User File %1...(%2)")
      .arg(normalizedName)
      .arg(actualFile)
      );
  }

  QMap<QString,RTLFileOptions*> optionsMap = wiz->getFileOptionsMap();
  QMap<QString,bool> wizFileMap =  wiz->getCheckedFileMap();

  foreach(QString fileName, optionsMap.keys())
  {
    RTLFileOptions* rfu = optionsMap[fileName];
    qDebug() << rfu->mFileName;
    mf->addFileOption(fileName, rfu);
  }

  
  foreach (QString fileName, wizFileMap.keys())
    mf->addFileCheck(fileName, wizFileMap[fileName]);

  // for each RTL file
  foreach (QString fileName, wiz->getHashedFileList())
  {
    QString actualFile = fileName;
    QString normalizedName = wiz->normalizeName(fileName);
    RTLFileOptions* rfu = optionsMap[normalizedName];
    KitFile* kf = mf->addRTLFile(actualFile, normalizedName);
    kf->setOptional(rfu->mOptional);
    kf->setCheckHash(rfu->mHashed);    
  }
  
  // Now add the optional files
  foreach(QString fileName, optionsMap.keys())
  {
    RTLFileOptions* rfu = optionsMap[fileName];
    if (rfu->mOptional)
    {
      QString actualFile = fileName;
      QString normalizedName = wiz->normalizeName(fileName);
      RTLFileOptions* rfu = optionsMap[normalizedName];
      KitFile* kf = mf->addOptionalRTLFile(actualFile, normalizedName);
      kf->setOptional(rfu->mOptional);
      kf->setCheckHash(rfu->mHashed);    
    }
  }


  // for each optional RTL file
 /* foreach (OptionalHashedFile* hf, wiz->getOptionalHashedFileList())
  {
    QString actualFile = hf->mFileName;
    QString normalizedName = wiz->normalizeName(hf->mFileName);
    mf->addOptionalRTLFile(actualFile, normalizedName, hf->mChecked);
  }*/


  foreach(KitUserAction* action, wiz->getPreUserActions())
  {
    mf->addPreUserAction(action);
  }

  foreach(KitUserAction* action, wiz->getPostUserActions())
  {
    mf->addPostUserAction(action);
  }

  qDebug() << "creating modelkit" << kitName.c_str();

  QString fileName(outName.c_str());

  addStatus("Generating kit...");

  if (theApp->checkArgument("-modelKitManifest"))
    addManifest(kit, mf);

  if (!kit->writeKitFile(wiz, fileName, mf))
  {
    qDebug() << "abort";
  }

  QString baseName;
  if (proj)
    baseName = QString(proj->getProjectDirectory()) + "/";
  
  mSettings.setValue(baseName + LAST_KIT, fileName);
  mSettings.setValue(baseName + LAST_CHOICE, true);

  addStatus("Finished Modelkit Generation");

  if (wiz->getRegenerate())
    wiz->next();

  QApplication::restoreOverrideCursor();
}

bool MKWPageFinished::validatePage()
{
  ModelKitWizard* wiz = (ModelKitWizard*)wizard();

  if (wiz->getRegenerate())
  {
    if (!mTerminatePage)
    {
      setFinalPage(true);
      wiz->close();
      theApp->shutdown(0);
      mTerminatePage = true;
    }
  }

  return true;
}

