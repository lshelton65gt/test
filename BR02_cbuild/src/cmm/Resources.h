#ifndef __RESOURCES_H_
#define __RESOURCES_H_

#include "util/CarbonVersion.h"
#include "gui/CQt.h"


#include "Scripting.h"

class Template;
class Resources;

// File: Resources, Resource
// The Resources collection and the Resource object
//
// Class: Resource
//
class Resource : public QObject, protected QScriptable
{
  Q_OBJECT  

  // Property: id
  // 
  // String resource identifier (unique)
  Q_PROPERTY(QString id READ getID)
  // Property: text
  // String value for this resource, may contain multiple lines
  Q_PROPERTY(QString text READ getText)

public:
  QString getID() const { return mID; }
  void setID(const QString& newVal) { mID=newVal; }

  QString getText() const { return mText; }
  void setText(const QString& newVal) { mText=newVal; }

  static bool deserialize(Resources* ress, const QDomElement& e, XmlErrorHandler* eh);

private:
  QString mID;
  QString mText;
};

// Class: Resources
//
// A collection of <Resource> objects
class Resources : public QObject, protected QScriptable
{
  Q_OBJECT

    // Property: count
  // The number of <Resource> objects in the collection
  Q_PROPERTY(int count READ getCount)

public:
  int getCount() const { return mResources.count(); }
  bool deserialize(Template* templ, const QDomElement& e, XmlErrorHandler* eh);
  void addResource(Resource* res);

public slots:
// Method: Resource getResource(index)
// Returns the Nth resource in the collection
//
// Parameters: 
//  index - An index from 0 to <count>-1
//
//   An index from 0 to N
//
// See Also:
// <Resource>
  QObject* getResource(int index)
  {
    return mResources[index];
  }
// Method: Resource getResource(name)
// Returns the named resource from the collection
//
// Parameters: 
//  name - String identifier of resource to locate
//
// Returns:
// <Resource> matching *name* or null if not found.
//
// See Also:
// <Resource>
  QObject* getResource(const QString& id)
  {
    foreach (Resource* res, mResources)
    {
      if (res->getID() == id)
        return res;
    }
    return NULL;
  }
private:
  QList<Resource*> mResources;

};
#endif
