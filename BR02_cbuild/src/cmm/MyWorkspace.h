#ifndef MYWORKSPACE_H
#define MYWORKSPACE_H

#include "util/CarbonPlatform.h"

#include <QWorkspace>

class MyWorkspace : public QWorkspace
{
	Q_OBJECT

public:
    MyWorkspace(QWidget *parent);
    ~MyWorkspace();

private:
    
};

#endif // MYWORKSPACE_H
