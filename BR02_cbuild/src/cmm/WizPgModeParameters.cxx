//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "MemoryWizard.h"
#include "WizPgModeParameters.h"
#include "WizardTemplate.h"
#include "Template.h"
#include "Modes.h"
#include "Mode.h"

WizPgModeParameters::WizPgModeParameters(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);
}

WizPgModeParameters::~WizPgModeParameters()
{
}

bool WizPgModeParameters::serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardContext* context = wiz->getTemplate()->getContext();
  Template* templ = context->getTemplate();

  QString modeName = wiz->field("portMode").toString();
  Mode* mode = templ->findMode(modeName);

  QDomElement tag = doc.createElement("WizardStep");
  tag.setAttribute("name", objectName());
  parent.appendChild(tag);

  addElement(doc, tag, "PortMode", modeName);

  Parameters* params = mode->getParameters();

  for (int i=0; i<params->getCount(); i++)
  {
    Parameter* p = params->getAt(i);
    QVariant v = p->getValue();
    QDomElement param = addElement(doc, tag, "Parameter", v.toString());
    param.setAttribute("name", p->getName());
  }
  return true;
}

void WizPgModeParameters::initializePage()
{
  setTitle("Additional Options");
  setSubTitle("Configure the additional options for this model.");

  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardContext* context = wiz->getTemplate()->getContext();
  Template* templ = context->getTemplate();

  QString modeName = wiz->field("portMode").toString();

  QString replayFile = wiz->getReplayFile();
  if (replayFile.length() > 0)
  {
    QString modeName = wiz->field("portMode").toString();
    Mode* mode = templ->findMode(modeName);
    Parameters* params = mode->getParameters();

    QDomElement wizElem = wiz->findWizardStep(objectName());
    if (!wizElem.isNull())
    {
      bool performNext = false;
      QDomNode n = wizElem.firstChild();
      while(!n.isNull())
      {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) 
        {
          qDebug() << e.tagName();
          if (e.tagName() == "Parameter")
          {
            QString paramName = e.attribute("name");
            QString paramValue = e.text();
            Parameter* param = params->findParam(paramName);
            if (param)
            {
              param->setValue(paramValue);
              performNext = true;
            }
            else
              performNext = false;
          }
        }
        n = n.nextSibling();
      }
      if (performNext)
        wiz->postNext();
    }
  }

  ui.labelParameters->setText(QString("%1 Parameters").arg(modeName));
  Mode* mode = templ->findMode(modeName);

  ui.treeWidget->populate(mode->getParameters());
}

int WizPgModeParameters::nextId() const
{
  return MemoryWizard::PageUserPorts;
}
