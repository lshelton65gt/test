#ifndef MKUWPAGECLONE_H
#define MKUWPAGECLONE_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>
#include "WizardPage.h"
#include <QWizardPage>
#include "ui_MKUWPageClone.h"

class KitFile;
class RTLFileOptions;

class MKUWPageClone : public WizardPage
{
  Q_OBJECT

public:
  MKUWPageClone(QWidget *parent = 0);
  ~MKUWPageClone();

protected:
  virtual void initializePage();
  virtual bool validatePage();
  virtual bool isComplete() const;
  virtual int nextId() const;

private:
  bool cloneFiles();
  bool copyKitFile(KitFile* kitFile);

private:
  Ui::MKUWPageCloneClass ui;
  bool mIsValid;
};

#endif // MKUWPAGECLONE_H
