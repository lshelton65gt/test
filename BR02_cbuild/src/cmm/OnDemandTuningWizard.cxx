//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#include "CarbonRuntimeState.h"
#include "OnDemandTuningWizard.h"


///////////////////////////////////////////////////////////////////////////////
class SelectIdleTimePage : public OnDemandWizardPage
{
public:
  SelectIdleTimePage(WizardState *state, QWidget * parent = 0)
    : OnDemandWizardPage(state, parent) {

    setTitle("Select Idle Time");
    setSubTitle(" ");

    QLabel *label = new QLabel(
    "Please select a time at which you expect the model to enter an idle state.\n\n"
    );
    label->setWordWrap(true);

    // TODO: get range from data file
    int min = 0;
    int max = 99;

    QSlider *slider = new QSlider(Qt::Horizontal, this);
    slider->setTickPosition(QSlider::TicksBelow);
    slider->setMinimum(min);
    slider->setMaximum(max);
    QSpinBox *spinBox = new QSpinBox;
    spinBox->setRange(min, max);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(label);
    layout->addWidget(slider);
    layout->addWidget(spinBox);
    setLayout(layout);

    connect(slider, SIGNAL(valueChanged(int)), spinBox, SLOT(setValue(int)));
    connect(spinBox, SIGNAL(valueChanged(int)), slider, SLOT(setValue(int)));
  }
};

///////////////////////////////////////////////////////////////////////////////
class SignalNotIdlePage : public OnDemandWizardPage
{
public:
  SignalNotIdlePage(WizardState *state, QWidget * parent = 0)
    : OnDemandWizardPage(state, parent) {

    setTitle("Signal Not Idle");
    setSubTitle("This signal does not appear to reach an idle (repeating) state.");

    SignalDisplay *signal = new SignalDisplay("some.net", this);

    QLabel *label = new QLabel(
    "Please indicate whether or not the value of this signal is critical to "
    "the correctness of your simulation.\n\n"
    "For example, counters may be incremented on a regular basis, but it may be "
    "reasonable to stop updating a counter while the model is in an idle state.\n\n"
    );
    label->setWordWrap(true);

    QRadioButton *mustBeCorrect = new QRadioButton("Signal value must always be correct", this);
    QRadioButton *canBeIgnored = new QRadioButton("Signal may be ignored when searching for idle states", this);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(signal);
    layout->addWidget(label);
    layout->addWidget(mustBeCorrect);
    layout->addWidget(canBeIgnored);
    setLayout(layout);
  }
};

///////////////////////////////////////////////////////////////////////////////
class InputValueChangePage : public OnDemandWizardPage
{
public:
  InputValueChangePage(WizardState *state, QWidget * parent = 0)
    : OnDemandWizardPage(state, parent) {

    setTitle("Unexpected Input Value Change");
    setSubTitle("The value of this signal has been changed by a call to the Carbon C API.");

    SignalDisplay *signal = new SignalDisplay("some.net", this);

    QLabel *label = new QLabel(
    "OnDemand has stopped looking for an idle state because, by default, "
    "only clock inputs are expected to change value while the model is idle.\n\n"
    "At the cost of some performance, the system may be configured to expect other signals to change.\n\n"
    "Does a write to this input signal mean that the model is not in an idle state?\n\n"
    );
    label->setWordWrap(true);

    QRadioButton *neverChanges = new QRadioButton("This signal NEVER changes value while the model is idle", this);
    QRadioButton *mayChange = new QRadioButton("This signal MAY change while the model is idle", this);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(signal);
    layout->addWidget(label);
    layout->addWidget(neverChanges);
    layout->addWidget(mayChange);
    setLayout(layout);
  }
};

///////////////////////////////////////////////////////////////////////////////
class MemoryAccessPage : public OnDemandWizardPage
{
public:
  MemoryAccessPage(WizardState *state, QWidget * parent = 0)
    : OnDemandWizardPage(state, parent) {

    setTitle("Memory Access");
    setSubTitle("This memory was accessed while OnDemand was attempting to discover or maintain an idle state.");

    SignalDisplay *signal = new SignalDisplay("some.mem", this);

    QLabel *label = new QLabel(
    "If this memory access is part of an idle state, you can account for "
    "this by increasing the OnDemand Memory Size Limit.\n\n"
    "NOTE: This configuration change is made using a Carbon compiler directive. "
    "The model must be recompiled for this change to affect simulation performance.\n\n"
    );
    label->setWordWrap(true);

    int memSizeLimit = 10;  // TODO: get current configuration limit

    QSpinBox *spinBox = new QSpinBox;
    spinBox->setValue(memSizeLimit);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(signal);
    layout->addWidget(label);
    layout->addWidget(spinBox);
    setLayout(layout);
  }
};

///////////////////////////////////////////////////////////////////////////////
class AdjustMaxStatesPage : public OnDemandWizardPage
{
public:
  AdjustMaxStatesPage(WizardState *state, QWidget * parent = 0)
    : OnDemandWizardPage(state, parent) {

    setTitle("Adjust Maximum States");
    setSubTitle(" ");

    QLabel *label = new QLabel(

    "In order to avoid impacting performance, OnDemand limits the number of model "
    "states that it examines while searching for a repeating (idle) pattern.\n\n"
    "Setting this number too high will cause more time to be spent attempting "
    "to discover idle states when the model is not idle.\n\n"
    "Setting this number too low will prevent OnDemand from identifying idle "
    "patterns that are longer than the limit.\n\n"
    "If you believe that the signals identified as changing value will repeat "
    "if given enough time, increase this limit to allow the repeating pattern to "
    "be identified.\n\n"
    );
    label->setWordWrap(true);

    int maxStates = 10;  // TODO: get current configuration limit

    QSpinBox *spinBox = new QSpinBox;
    spinBox->setValue(maxStates);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(label);
    layout->addWidget(spinBox);
    setLayout(layout);
  }
};


///////////////////////////////////////////////////////////////////////////////
OnDemandTuningWizard::OnDemandTuningWizard(CarbonRuntimeState *runtimeState, QWidget * parent, Qt::WindowFlags flags)
  : QWizard(parent, flags)
{
  mState = new WizardState(runtimeState);

  setWindowTitle(tr("Carbon OnDemand Tuning Wizard"));
  setPixmap(QWizard::LogoPixmap, QPixmap(":/cmm/Resources/carbon.png"));

  addPage(new IntroPage(mState, this));
  addPage(new SelectIdleTimePage(mState, this));
  addPage(new SignalNotIdlePage(mState, this));
  addPage(new InputValueChangePage(mState, this));
  addPage(new MemoryAccessPage(mState, this));
  addPage(new AdjustMaxStatesPage(mState, this));
}

OnDemandTuningWizard::~OnDemandTuningWizard()
{
  delete mState;
}


