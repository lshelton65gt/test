#ifndef __KITMANIFEST_H__
#define __KITMANIFEST_H__

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "util/Zstream.h"
#include "util/ZstreamZip.h"
#include "util/OSWrapper.h"
#include "util/UtStringArray.h"
#include "util/UtIStream.h"

#include "CarbonConsole.h"
#include "ScriptingEngine.h"

#include <QtGui>
#include <QtXml>
#include "Scripting.h"
#include "KitVersions.h"

extern const char* gCarbonVersion();

struct RTLFileOptions;
//
// File: Manifest
//
#define MODELKIT_VERSION_NUMBER "1.0"
#define CARBON_RELEASE "release"
#define CARBON_VERSION "softwareVersion"
#define MODELKIT_ID "id"
#define MODELKIT_DESCRIPTION "description"

#define MANIFEST_ID "id"
#define MANIFEST_NAME "name"
#define MANIFEST_DESCRIPTION "description"
#define MANIFEST_COPY_FILES "copyFiles"
#define MANIFEST_ASK_ROOT "askRoot"
#define VERSION_LOCKED "true"

class KitManifest;
class ModelKit;
class ModelKitUserWizard;

class KitVersion : public QObject, public QScriptable
{
  Q_OBJECT

  Q_PROPERTY(KitManifest* manifest READ getManifest);
  Q_PROPERTY(QString vendorVersion READ getVendorVersion);
  Q_PROPERTY(QString description READ getDescription);
  Q_PROPERTY(quint32 id READ getID);
  Q_PROPERTY(bool locked READ getLocked);

public:
  void serialize(QDomDocument& doc, QDomElement& parent);

  KitVersion(KitManifest* manifest, const QString& description, quint32 id)
  {
    mManifest = manifest;
    mDescription = description;
    mID = id;
    mLocked = false;
  }

  void setDescription(const QString& newVal) { mDescription=newVal; }
  void setVendorVersion(const QString& newVal) { mVendorVersion=newVal; }
  void setLocked(bool newVal) { mLocked=newVal; }

public slots:
  KitManifest* getManifest() { return mManifest; }
  QString getVendorVersion() const { return mVendorVersion; }
  QString getDescription() const { return mDescription; }
  quint32 getID() const { return mID; }
  bool getLocked() const { return mLocked; }

private:
  KitManifest* mManifest;
  QString mVendorVersion;
  QString mDescription;
  quint32 mID;
  bool mLocked;
};


class KitManifests : public QObject, public QScriptable
{
  Q_OBJECT

  Q_CLASSINFO("ClassName", "Manifests");

  Q_PROPERTY(quint32 numManifests READ numManifests)

public:
  KitManifests();
  
  KitManifest* findManifest(const QString& manifestName);
  KitManifest* findManifest(quint32 manifestID);
  quint32 nextID();

  void deserialize(ModelKit* kit, QDomElement& parent);
  void serialize(QDomDocument& doc, QDomElement& parent);
  void addManifest(KitManifest* mf) { mManifests.append(mf); }
  void removeAll()
  {
    mManifests.clear();
  }
  void deleteManifest(KitManifest* m)
  {
    mManifests.removeOne(m);
  }

public slots:
  quint32 numManifests() const { return mManifests.count(); }
  KitManifest* getManifest(quint32 index) { return mManifests[index]; }

private:
  QList<KitManifest*> mManifests;
};


class KitVersions : public QObject, public QScriptable
{
  Q_OBJECT

  Q_CLASSINFO("ClassName", "Versions");

  Q_PROPERTY(quint32 numVersions READ numVersions)
 
public:
  KitVersions();

  KitVersion* getLatestVersion();

  KitVersion* addVersion(KitManifest* manifest, const QString& vendorVersion, const QString& descr)
  {
    KitVersion* v = new KitVersion(manifest, descr, mVersions.count()+1);
    v->setVendorVersion(vendorVersion);
    v->setDescription(descr);
    mVersions.append(v);
    return v;
  }

  void deleteVersion(KitVersion* v)
  {
    if (v == getLatestVersion())
      mVersions.removeOne(v);
    else
      INFO_ASSERT(false, "Can only delete the latest version");
  }

  void deserialize(ModelKit* kit, QDomElement& parent);
  void serialize(QDomDocument& doc, QDomElement& parent);
  void removeAll()
  {
    mVersions.clear();
  }
public slots:
  quint32 numVersions() const { return mVersions.count(); }
  KitVersion* getVersion(quint32 index) { return mVersions[index]; }

private:
  QList<KitVersion*> mVersions;
};


// <Manifest>
//  <RTLFiles>
//   <File name="$(FOOBAR)/Vic.v" hash="aabbccdd"/>
//  </RTLFiles>
// </Manifest>

//
// Class: KitFile
//
// A file included in the manifest for a model kit.
//
class KitFile : public QObject, public QScriptable
{
  Q_OBJECT

  Q_PROPERTY(QString actualName READ getActualName);
  Q_PROPERTY(QString normalizedName READ getNormalizedName);
  Q_PROPERTY(QString hash READ getHash);
  Q_PROPERTY(bool retainedFile READ getRetained);

public:
  KitFile() {}

  KitFile(const QString& actualName, const QString& normalizedName,
    bool optional, ZstreamZip::FileType fileType=ZstreamZip::eFileTxt, bool userFile=false)
  {
    UtString errMsg;
    errMsg << "Expected normalized name for " << actualName << " got" << normalizedName;
    INFO_ASSERT(normalizedName.startsWith("$"), errMsg.c_str());
    mFileType = fileType;
    mOptional = optional;
    mCheckHash = true;
    mActualName = actualName;
    mNormalizedName = normalizedName;
    mUserFile = userFile;
    QFileInfo fi(actualName);
    mPermissions = fi.permissions();
    mRetained = false;
    mCopyFile = true;
  }

  bool getCheckHash() const { return mCheckHash; }
  void setCheckHash(bool newVal) { mCheckHash=newVal; }

  bool getRetained() const { return mRetained; }
  void setRetained(bool newVal) { mRetained=newVal; }

  bool getOptional() const { return mOptional; }
  void setOptional(bool newVal) { mOptional=newVal; }

  bool getCopyFile() const { return mCopyFile; }
  void setCopyFile(bool newVal) { mCopyFile=newVal; }

  void setHash(const QString& newVal) { mHash=newVal; }
  QString getHash() const { return mHash; }

  void setActualName(const QString& newVal) { mActualName=newVal; }
  QString getActualName() const { return mActualName; }

  void setNormalizedName(const QString& newVal) { mNormalizedName=newVal; }
  QString getNormalizedName() const { return mNormalizedName; }

  QFile::Permissions getPermissions() const { return mPermissions; }
  void setPermissions(QFile::Permissions perms) { mPermissions=perms; }

  ZstreamZip::FileType getFileType() const { return mFileType; }

  QString mHash;
  QString mActualName;
  QString mNormalizedName;
  ZstreamZip::FileType mFileType;
  bool mUserFile;
  bool mOptional;
  bool mCheckHash;
  bool mRetained;
  bool mCopyFile;
  QFile::Permissions mPermissions;
};

class KitManifest;
class ModelKitUserWizard;

class TestEnums : public QObject
{
  Q_OBJECT

  Q_CLASSINFO("EnumPrefix", "TestEnums");

public:
  // Preface = Before anything is done
  // PreCompilation = Before Cbuild
  // PostComplation = After Cbuild
  // Epiloge = After Component Generation
  enum TestExecutionPhase { Preface, PreCompilation, PostCompilation, Epilogue };
  Q_ENUMS(ModelTarget);
};

Q_DECLARE_METATYPE(TestEnums::TestExecutionPhase);

class ContextEnums : public QObject
{
  Q_OBJECT

  Q_CLASSINFO("EnumPrefix", "ContextEnums");

public:
  enum ModelTarget { Unix, Windows };
  Q_ENUMS(ModelTarget);

  enum ModelSymbols { FullSymbols, IOSymbols };
  Q_ENUMS(ModelSymbols);
};



class KitAnswer : public QObject, public QScriptable
{
  Q_OBJECT

  Q_CLASSINFO("ClassName", "KitAnswer");

  Q_PROPERTY(QString id READ getID);
  Q_PROPERTY(QString displayValue READ getDisplayValue);
  Q_PROPERTY(QString value READ getValue);

public slots:
  QString getID() const { return mID; }
  void setID(const QString& newVal) { mID=newVal; }

  QString getDisplayValue() const { return mDisplayValue; }
  void setDisplayValue(const QString& newVal) { mDisplayValue=newVal; }

  QString getValue() const { return mValue; }
  void setValue(const QString& newVal) { mValue=newVal; }

private:
  QString mID;
  QString mDisplayValue;
  QString mValue;
};


// Collection of Answers
class KitAnswers : public QObject, public QScriptable
{
  Q_OBJECT

  Q_CLASSINFO("ClassInfo", "KitAnswers");

  Q_PROPERTY(quint32 numItems READ numItems);
  Q_PROPERTY(bool passed READ getPassed WRITE setPassed);


public:
  KitAnswers(QObject* parent=0) : QObject(parent)
  {
    mPassed = false;
  }

  void addAnswer(KitAnswer* a)
  {
    mAnswerMap[a->getID()] = a;
    mItems.append(a);
  }

  void removeAll() { mItems.clear(); }

public slots:
  bool getPassed() { return mPassed; }
  void setPassed(bool newVal) { mPassed=newVal; }

  quint32 numItems() { return mItems.count(); }
  KitAnswer* getAt(quint32 i) { return mItems[i]; }
  KitAnswer* findAnswer(const QString& id)
  {
    if (mAnswerMap.contains(id))
      return mAnswerMap[id];
    else
      return NULL;
  }

private:
  QList<KitAnswer*> mItems;
  QMap<QString, KitAnswer*> mAnswerMap;
  bool mPassed;

};

class KitAnswerGroup : public QObject, public QScriptable
{
  Q_OBJECT

  Q_CLASSINFO("ClassName", "KitAnswerGroup");
  Q_PROPERTY(QString id READ getID);
  Q_PROPERTY(quint32 index READ getIndex);
  Q_PROPERTY(KitAnswers* answers READ getAnswers);

public:
  KitAnswerGroup(QObject* parent=0) : QObject(parent)
  {
    mAnswers = new KitAnswers(this);
  }

public slots:
  QString getID() const { return mID; }
  void setID(const QString& newVal) { mID=newVal; }

  quint32 getIndex() const { return mIndex; }
  void setIndex(quint32 newVal) { mIndex=newVal; }

  KitAnswers* getAnswers() const { return mAnswers; }

private:
  KitAnswers* mAnswers;
  quint32 mIndex;
  QString mID;
};




// Collection of Answers
class KitAnswerGroups : public QObject, public QScriptable
{
  Q_OBJECT

  Q_CLASSINFO("ClassInfo", "KitAnswerGroups");

  Q_PROPERTY(quint32 numItems READ numItems);

public:
  void addAnswerGroup(KitAnswerGroup* g)
  {
    mItems.append(g);
  }
  void removeAll() { mItems.clear(); }

public slots:
  quint32 numItems() { return mItems.count(); }
  KitAnswerGroup* getAt(quint32 i) { return mItems[i]; }

private:
  QList<KitAnswerGroup*> mItems;
};


class KitTest : public QObject, public QScriptable
{
  Q_OBJECT

  Q_CLASSINFO("ClassName", "KitTest");

  Q_PROPERTY(QString modelKitName READ getModelKitName);
  Q_PROPERTY(KitAnswers* answers READ getAnswers);
  Q_PROPERTY(KitAnswerGroups* answerGroups READ getAnswerGroups);
 
public:
  KitTest(QObject* parent = 0);

public:
  static void registerTypes(QScriptEngine* engine);
  static void registerScriptTypes(QMap<QString, const QMetaObject*>& map);
  static void registerIntellisense(JavaScriptAPIs* apis);
  bool loadPortalAnswers(const QString& answerFile);

public slots:
  QString getModelKitName() const { return mModelKitName; }
  void setModelKitName(const QString& kitName) { mModelKitName=kitName; }

  KitAnswers* getAnswers() { return mAnswers; }
  KitAnswerGroups* getAnswerGroups(){ return mAnswerGroups; }
 
private:
  QString mModelKitName;
  KitAnswers* mAnswers;
  KitAnswerGroups* mAnswerGroups;
};


class KitUserContext : public QObject, public QScriptable
{
  Q_OBJECT

public:
  // Make sure these enums match ModelKitUserWizard definitions

  Q_CLASSINFO("ClassName", "Context");

  Q_PROPERTY(QString componentName READ getComponentName);
  Q_PROPERTY(QString libraryName READ getLibraryName);
  Q_PROPERTY(ContextEnums::ModelTarget modelTarget READ getModelTarget);
  Q_PROPERTY(ContextEnums::ModelSymbols symbols READ getModelSymbols);
  

  KitUserContext(QObject* parent = 0, ModelKitUserWizard* wiz = 0, KitManifest* mf = 0);
  void setPlaybackMode(bool newVal) { mPlaybackMode=newVal; }

  void setComponentName(const QString& newVal) { mComponentName=newVal; }
  void setLibraryName(const QString& newVal) { mLibraryName=newVal; }
  void setModelSymbols(ContextEnums::ModelSymbols newVal) { mModelSymbols=newVal; }
  void setModelTarget(ContextEnums::ModelTarget newVal) { mModelTarget=newVal; }

  static void registerTypes(QScriptEngine* engine);
  static void registerScriptTypes(QMap<QString, const QMetaObject*>& map);
  static void registerIntellisense(JavaScriptAPIs* apis);
  QStringList& getErrorMessages() { return mErrorStrings; }

signals:
  void updateStatusMessage(const QString& msg);
  void updateProgress(int value);
  void updateProgressRange(int low, int high);

public slots:
  ContextEnums::ModelTarget getModelTarget() const { return mModelTarget; }
  ContextEnums::ModelSymbols getModelSymbols() const { return mModelSymbols; }

  QString getComponentName() const { return mComponentName; }
  QString getLibraryName() const { return mLibraryName; }

  void setStatusMessage(const QString& msg)
  {
    emit updateStatusMessage(msg);
  }
  void setProgress(int value)
  {
    emit updateProgress(value);
  }
  void setProgressRange(int low, int high)
  {
    emit updateProgressRange(low, high);
  }
  double getDouble(const QString& questionId, const QString & title, const QString & label, double value = 0, double minValue = -2147483647, double maxValue = 2147483647, int decimals = 1, Qt::WindowFlags f = 0 );
  int getInteger(const QString& questionId, const QString & title, const QString & label, int value = 0, int minValue = -2147483647, int maxValue = 2147483647, int step = 1, Qt::WindowFlags f = 0);
  QString getItem(const QString& questionId, const QString & title, const QString & label, const QStringList& list, int current = 0, bool editable = true, Qt::WindowFlags f = 0);
  QString getText(const QString& questionId, const QString & title, const QString & label, QLineEdit::EchoMode mode = QLineEdit::Normal, const QString & text = QString(), Qt::WindowFlags f = 0);
  QString getOpenFileName(const QString& questionId, const QString & caption, const QString& initialDir, const QString& filter, const QString & selectedFilter);
  QScriptValue getOpenFileNames(const QString& questionId, const QString & caption, const QString& initialDir, const QString& filter, const QString & selectedFilter);
  QString getExistingDirectory(const QString& questionId, const QString& caption, const QString& dir);

  QString expandName(const QString& inputName);
  
  void setVariable(const QString& varName, const QString& value)
  {
    mVariableMap[varName] = value;
  }
  QString lookupVariable(const QString& varName) 
  {
    return mVariableMap[varName];
  }
  KitManifest* getManifest() const { return mManifest; }
  bool getPlaybackMode() const { return mPlaybackMode; }

  void reportError(const QString& errorMessage)
  {
    mErrorStrings.append(errorMessage);
  }

private:
  bool mPlaybackMode;
  KitManifest* mManifest;
  ModelKitUserWizard* mWizard;
  QMap<QString, QString> mVariableMap;
  QString mComponentName;
  QString mLibraryName;
  ContextEnums::ModelSymbols mModelSymbols;
  ContextEnums::ModelTarget mModelTarget;
  QStringList mErrorStrings;
};

Q_DECLARE_METATYPE(ContextEnums::ModelSymbols);
Q_DECLARE_METATYPE(ContextEnums::ModelTarget);

// Abstract Base Class for user actions
class KitUserAction
{
public:

  enum UserActionKind { eActionInputFile, eActionExecuteScript, eActionRunScript };

  KitUserAction(UserActionKind kind)
  {
    mKind = kind;
  }
  virtual ~KitUserAction()
  {
  }

 
  void copy(KitUserAction* src)
  {
    mKind = src->getKind();
    setDescription(src->getDescription());
    setVariableName(src->getVariableName());
  }

  static QString createGUID();
  QString getKindString() const;

  UserActionKind getKind() const { return mKind; }

  QString getName() const { return getKindString(); }
  
  QString getDescription() const { return mDescription; }
  void setDescription(const QString& newVal) { mDescription=newVal; }

  QString getVariableName() const { return mVariable; }
  void setVariableName(const QString& newVal) { mVariable=newVal; }


  QString getID() const { return mID; }
  void setID(const QString& newVal) { mID=newVal; }

  // Required
public:
  virtual void serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler* errorHandler);
  virtual bool execute(ModelKitUserWizard* wiz, QWidget* parent, KitUserContext* context)=0;
  virtual KitUserAction* copyAction()=0;

protected:
  static void deserialize(KitUserAction* action, const QDomElement& e, XmlErrorHandler*);

  QDomElement addElementWithAtts(QDomDocument& doc, QDomElement& parent, const QString& elementName, const QString& attName, const QString& attValue)
  {
    QDomElement element = doc.createElement(elementName);
    parent.appendChild(element);
    if (!attName.isEmpty())
      element.setAttribute(attName, attValue);
    return element;
  }

  QDomElement addElement(QDomDocument& doc, QDomElement& parent, const QString& elementName)
  {
    QDomElement element = doc.createElement(elementName);
    parent.appendChild(element);
    return element;
  }

  QDomElement addElement(QDomDocument& doc, QDomElement& parent, const QString& elementName, const QString& value)
  {
    QDomElement element = doc.createElement(elementName);
    parent.appendChild(element);

    QDomText text = doc.createTextNode(value);
    element.appendChild(text);

    return element;
  }

private:
  UserActionKind mKind;
  QString mVariable;    // Variable: $ConfigurationFile
  QString mDescription; // Description: In order to proceed we need your .conf file from your design.
  QString mID;
};

// Execute a modelstudio script action
class KitActionRunScript : public QObject, public KitUserAction
{
  Q_OBJECT

private slots:
  void scriptException(const QScriptValue &, int);

public:
  KitActionRunScript() : KitUserAction(KitUserAction::eActionRunScript)
  {
    mExceptionOccurred = false;  
    mEngine=NULL;
  }

  enum ScriptEventTime { BeforeModelCompilation, AfterModelCompilation };

  // Static deserialize
  static KitUserAction* deserialize(const QDomElement& e, XmlErrorHandler*);

  // Required 
  virtual void serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler* errorHandler);
  virtual bool execute(ModelKitUserWizard* wiz, QWidget* parent, KitUserContext* context);

  virtual KitUserAction* copyAction()
  {
    KitActionRunScript* rs = new KitActionRunScript();

    rs->copy(this);
    rs->setScriptName(getScriptName());
    rs->setEventTime(getEventTime());

    return rs;
  }

  QString getScriptName() const { return mScriptName; }
  void setScriptName(const QString& newVal ) { mScriptName=newVal; }

  static ScriptEventTime stringToEventTime(const QString& str);
  ScriptEventTime getEventTime() const { return mEventTime; }
  QString getEventTimeString() const;
  void setEventTime(ScriptEventTime newVal) { mEventTime=newVal; }

private:
  QString mScriptName;
  ScriptEventTime mEventTime;
  ScriptingEngine* mEngine;
  bool mExceptionOccurred;
};

class KitActionExecuteScript : public KitUserAction
{
public:
  KitActionExecuteScript();

  // Static deserialize
  static KitUserAction* deserialize(const QDomElement& e, XmlErrorHandler*);

  // Required 
  virtual void serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler* errorHandler);
  virtual bool execute(ModelKitUserWizard* wiz, QWidget* parent, KitUserContext* context);

  QString getScriptName() const { return mScriptName; }
  void setScriptName(const QString& newVal ) { mScriptName=newVal; }

  QString getDefaultDirectory() const { return mDefaultDirectory; }
  void setDefaultDirectory(const QString& newVal ) { mDefaultDirectory=newVal; }

  int getValidExitCode() const { return mValidExitCode; }
  void setValidExitCode(int newVal) { mValidExitCode=newVal; }

  const QStringList& getScriptArguments() const { return mScriptArguments; }
  void setScriptArguments(const QStringList& newVal ) { mScriptArguments=newVal; }

  int getExitCode() const { return mExitCode; }

  virtual KitUserAction* copyAction()
  {
    KitActionExecuteScript* rs = new KitActionExecuteScript();
    rs->copy(this);
    rs->setScriptName(getScriptName());
    rs->setDefaultDirectory(getDefaultDirectory());
    rs->setValidExitCode(getValidExitCode());
    rs->setScriptArguments(getScriptArguments());

    return rs;
  }
private:
  int mValidExitCode;
  int mExitCode;
  QString mScriptName;
  QString mDefaultDirectory;
  QStringList mScriptArguments;
};

class KitActionInputFile : public KitUserAction
{
public:
  KitActionInputFile();

  enum Choose { File, Directory };

  // Static deserialize
  static KitUserAction* deserialize(const QDomElement& e, XmlErrorHandler*);

  // Required 
  virtual void serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler* errorHandler);
  virtual bool execute(ModelKitUserWizard* wiz, QWidget* parent, KitUserContext* context);

  QString getDefaultFileName() const { return mDefaultFileName; }
  void setDefaultFileName(const QString& newVal ) { mDefaultFileName=newVal; }

  QString getDialogTitle() const { return mDialogTitle; }
  void setDialogTitle(const QString& newVal ) { mDialogTitle=newVal; }

  QString getDefaultDirectory() const { return mDefaultDirectory; }
  void setDefaultDirectory(const QString& newVal ) { mDefaultDirectory=newVal; }

  const QStringList& getFileTypes() const { return mFileTypes; }
  void setFileTypes(const QStringList& newVal ) { mFileTypes=newVal; }
  
  Choose getChoice() const { return mChoose; }
  void setChoice(Choose newVal) { mChoose=newVal; }

  virtual KitUserAction* copyAction()
  {
    KitActionInputFile* rs = new KitActionInputFile();
    rs->copy(this);
    rs->setDefaultFileName(getDefaultFileName());
    rs->setDialogTitle(getDialogTitle());
    rs->setDefaultDirectory(getDefaultDirectory());
    rs->setFileTypes(getFileTypes());
    rs->setChoice(getChoice());
    return rs;
  }
private:
  QString mDialogTitle;
  QString mDefaultFileName;
  QString mDefaultDirectory;
  QStringList mFileTypes;
  Choose mChoose;
};

class ModelKitWizard;
class JavaScriptAPIs;
//
// Class: Manifest
//
// The manifest of a Model Kit
//
class KitManifest : public QObject, QScriptable
{
  Q_OBJECT

  Q_CLASSINFO("ClassName", "Manifest");

  Q_PROPERTY(QString name READ getName)
  Q_PROPERTY(QString release READ getRelease)
  Q_PROPERTY(QString softwareVersion READ getSoftwareVersion)
  Q_PROPERTY(QString libraryName READ getLibraryName);
  Q_PROPERTY(QString version READ getVersion);
  Q_PROPERTY(QString licenseKey READ getLicenseKey);
  Q_PROPERTY(quint32 numRTLFiles READ numRTLFiles);
  Q_PROPERTY(quint32 numFiles READ numFiles);
  Q_PROPERTY(quint32 numUserFiles READ numUserFiles);
  Q_PROPERTY(quint32 id READ getID)
  Q_PROPERTY(quint32 numCompilerSwitches READ numCompilerSwitches);
  Q_PROPERTY(quint32 numModelKitFeatures READ numModelKitFeatures);

public:
  enum ComponentType { SOCDesigner, CoWare, SystemC };

  static void registerTypes(QScriptEngine* engine);
  static void registerScriptTypes(QMap<QString, const QMetaObject*>& map);
  static void registerIntellisense(JavaScriptAPIs* apis);

  // Copy all embedded files into manifestRoot/{id} for this manifest
  static void prepareManifest(const QString& manifestRoot, KitManifest* mf);

  void copyManifest(KitManifest* srcManifest);

  bool hasComponent(ComponentType ctype);
  bool hasFile(const QString& file)
  {
    for (quint32 i=0; i<numFiles(); i++)
    {
      KitFile* kf = getFile(i);
      if (kf->getNormalizedName() == file)
        return true;
    }
    for (quint32 i=0; i<numUserFiles(); i++)
    {
      KitFile* kf = getUserFile(i);
      if (kf->getNormalizedName() == file)
        return true;
    }
    return false;
   }
  KitManifest() 
  {
    mID = 1;
    mCopyFiles = true;
    mAskRoot = true;
    mWizard = NULL;
    mVersion = MODELKIT_VERSION_NUMBER;
    mRelease = CARBON_RELEASE_ID;
    mSoftwareVersion = gCarbonVersion();
    mName = "Default";
    mEditable = false;
    mPreserveHashes = false;
  }

  void removeAllItems();

  void setWizard(ModelKitWizard* wiz) { mWizard=wiz; }

  void serialize(const QString& fileName);
  void serialize(QDomDocument& doc, QDomElement& parent);
  XmlErrorHandler* deserialize(const QString& fileName);
  void deserialize(QDomElement& parent);

  void setDescription(const QString& newVal) { mDescription=newVal; }
  QString getDescription() const { return mDescription; }

  void setName(const QString& newVal) { mName=newVal; }
  QString getName() const { return mName; }

  void setLibraryName(const QString& newVal) { mLibName=newVal; }
  QString getLibraryName() const { return mLibName; }

  // Version
  void setVersion(const QString& newVal) { mVersion=newVal; }
  QString getVersion() const { return mVersion; }
 
  // LicenseKey
  void setLicenseKey(const QString& newVal) { mLicenseKey=newVal; }
  QString getLicenseKey() const { return mLicenseKey; }

  void setCopyFiles(bool newVal) { mCopyFiles=newVal; }
  bool getCopyFiles() const { return mCopyFiles; }

  void setAskRoot(bool newVal) { mAskRoot=newVal; }
  bool getAskRoot() const { return mAskRoot; }

  void setID(quint32 id) { mID=id; }

  // RTLFiles
  KitFile* addRTLFile(const QString& actualName, const QString& normalizedName)
  {
    qDebug() << "addRTL File" << "adding kit file" << actualName << normalizedName;
    QFileInfo fi(actualName);
    KitFile* kf = new KitFile(actualName, normalizedName, false);
    mRTLFileList.append(kf);
    return kf;
  }

   // RTLFiles
  KitFile* addOptionalRTLFile(const QString& actualName, const QString& normalizedName)
  {
    qDebug() << "addRTL File" << "adding kit file" << actualName << normalizedName;
    QFileInfo fi(actualName);
    KitFile* kf = new KitFile(actualName, normalizedName, true);
    mOptRTLFileList.append(kf);
    return kf;
  }


  const QList<KitFile*>& getRTLFiles() { return mRTLFileList; }
  const QList<KitFile*>& getOptionalRTLFiles() { return mOptRTLFileList; }
 
  const QList<KitUserAction*>& getPreUserActions() { return mPreUserActions; }
  const QList<KitUserAction*>& getPostUserActions() { return mPostUserActions; }

  QMap<QString,RTLFileOptions*>& getFileOptionsMap() { return mFileOptionsMap; }

  QMap<QString,bool>& getFileCheckedMap() { return mFileCheckMap; }
  void addPreUserAction(KitUserAction* action, bool prepend=false) {
    if (prepend)
      mPreUserActions.prepend(action);
    else
      mPreUserActions.append(action);
  }
  void addPostUserAction(KitUserAction* action, bool prepend=false) {
    if (prepend)
      mPostUserActions.prepend(action);
    else
      mPostUserActions.append(action);
  }
  void addFileOption(const QString& fileName, RTLFileOptions* options)
  {
    mFileOptionsMap[fileName] = options;
  }
  void addFileCheck(const QString& fileName, bool value)
  {
    mFileCheckMap[fileName] = value;
  }
  void addCompilerSwitch(const QString& name, const QString& value)
  {
    qDebug() << "addCompilerSwitch" << name << value;
    QString s = QString("%1 %2").arg(name).arg(value);
    mCompilerSwitches.append(s);
  }
  void addModelKitFeature(const QString& name)
  {
    qDebug() << "addModelKitFeature" << name;
    mModelKitFeatures.insert(name);
  }

  KitFile* addFile(const QString& actualName, const QString& normalizedName, ZstreamZip::FileType fileType=ZstreamZip::eFileTxt, bool userFile = false, bool prepend=false)
  {
    qDebug() << "addFile (userFile=" << (userFile? "T" : "F") << ") (prepend=" << (prepend ? "T" : "F" ) << ")" << "adding kit file" << actualName << normalizedName;
    KitFile* kf = new KitFile(actualName, normalizedName, false, fileType, userFile);
    if (userFile){
      if (prepend) 
    mUserFileList.prepend(kf);
      else
    mUserFileList.append(kf);
    } else {
      if (prepend)
    mFileList.prepend(kf);
      else
    mFileList.append(kf);
    }

    return kf;
  }
  const QStringList& getCompilerSwitches() { return mCompilerSwitches; }
  const QList<QString> getModelKitFeatures() { return mModelKitFeatures.toList(); }
  bool findModelKitFeature(const QString feature) { return mModelKitFeatures.contains(feature); }
  const QList<KitFile*>& getFiles() { return mFileList; }
  const QList<KitFile*>& getUserFiles() { return mUserFileList; }
  void addComponent(const QString& compName) { mComponents.append(compName); }
  const QStringList& getComponents() { return mComponents; }
  void setPreserveHashes(bool newVal) { mPreserveHashes=newVal; }
  bool getPreserveHashes() const { return mPreserveHashes; }

  void setEditable(bool newVal) { mEditable=newVal; }
  bool isEditable() const { return mEditable; }

public slots:
  QString getRelease() const { return mRelease; }
  QString getSoftwareVersion() const { return mSoftwareVersion; }
  quint32 getID() const { return mID; }
  quint32 numRTLFiles() const { return mRTLFileList.count(); }
  quint32 numFiles() const { return mFileList.count(); }
  quint32 numUserFiles() const { return mUserFileList.count(); }
  quint32 numCompilerSwitches() const { return mCompilerSwitches.count(); }
  quint32 numModelKitFeatures() const { return mModelKitFeatures.count(); }

  bool decryptFile(const QString& inputFileName);
  bool encryptFile(const QString& inputFileName);
  QString encryptBuffer(const QString& buffer);
  QString decryptBuffer(const QString& buffer);

  QString getCompilerSwitch(quint32 index) {return mCompilerSwitches[index]; }
  KitFile* getUserFile(quint32 index) { return mUserFileList[index]; }
  KitFile* getRTLFile(quint32 index) { return mRTLFileList[index]; }
  KitFile* getFile(quint32 index) { return mFileList[index]; }

private:
  void serializeComponents(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*);
  void serializeProjectSettings(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*);
  void serializeRTLFiles(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*);
  void serializeFiles(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*);
  void serializeUserActions(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*);
  void serializeFileChecks(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*);
  void serializeCompilerSwitches(QDomDocument& doc, QDomElement& parent, XmlErrorHandler* eh);
  void serializeModelKitFeatures(QDomDocument& doc, QDomElement& parent, XmlErrorHandler* eh);
  void serializeOptionalRTLFiles(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*);

  bool deserializeProjectSettings(const QDomElement& e, XmlErrorHandler*);
  bool deserializeFiles(const QDomElement& e, XmlErrorHandler*);
  bool deserializeRTLFiles(const QDomElement& e, XmlErrorHandler*);
  bool deserializeOptionalRTLFiles(const QDomElement& e, XmlErrorHandler*);
  bool deserializeComponents(const QDomElement& e, XmlErrorHandler*);
  bool deserializeUserActions(const QDomElement& e, XmlErrorHandler*);
  bool deserializeFileChecks(const QDomElement& e, XmlErrorHandler*);
  bool deserializeCompilerSwitches(const QDomElement& e, XmlErrorHandler* eh);
  bool deserializeModelKitFeatures(const QDomElement& e, XmlErrorHandler* eh);

private:
  QString mDescription;
  QString mLicenseKey;
  QString mLibName;
  QString mVersion;
  QStringList mCompilerSwitches;
  QSet<QString> mModelKitFeatures;
  QList<KitFile*> mRTLFileList;
  QList<KitFile*> mOptRTLFileList;
  QList<KitFile*> mFileList;
  QList<KitFile*> mUserFileList;
  QList<KitUserAction*> mPreUserActions;
  QList<KitUserAction*> mPostUserActions;
  QMap<QString, bool> mFileCheckMap;
  QMap<QString,RTLFileOptions*> mFileOptionsMap;
  XmlErrorHandler mErrorHandler;
  QStringList mComponents;
  bool mCopyFiles;
  bool mAskRoot;
  ModelKitWizard* mWizard;
  QString mRelease;
  QString mSoftwareVersion;
  quint32 mID;
  QString mName;
  bool mEditable;
  bool mPreserveHashes;
};


Q_DECLARE_METATYPE(KitFile*);
Q_DECLARE_METATYPE(KitUserContext*);
Q_DECLARE_METATYPE(KitTest*);
Q_DECLARE_METATYPE(KitAnswer*);
Q_DECLARE_METATYPE(KitAnswerGroup*);
Q_DECLARE_METATYPE(KitAnswers*);
Q_DECLARE_METATYPE(KitAnswerGroups*);


#endif
