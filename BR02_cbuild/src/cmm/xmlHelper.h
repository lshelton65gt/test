#ifndef __XMLHELPER__
#define __XMLHELPER__

class SpiritXMLHelper
{
public:
  static QDomElement addElementWithAtts(QDomDocument& doc, QDomElement& parent, const QString& elementName, const QString& attName, const QString& attValue)
  {
    QDomElement element = doc.createElement(elementName);
    element.setPrefix("spirit:");
    parent.appendChild(element);
    if (!attName.isEmpty())
      element.setAttribute(attName, attValue);
    return element;
  }

  static QDomElement addElement(QDomDocument& doc, QDomElement& parent, const QString& elementName)
  {
    QDomElement element = doc.createElement(elementName);
    element.setPrefix("spirit:");
    parent.appendChild(element);
    return element;
  }

  static QDomElement addElement(QDomDocument& doc, QDomElement& parent, const QString& elementName, const QString& value)
  {
    if (!value.isEmpty())
    {
      QDomElement element = doc.createElement(elementName);
      element.setPrefix("spirit:");
      parent.appendChild(element);

      QDomText text = doc.createTextNode(value);
      element.appendChild(text);

      return element;
    }

    return QDomElement();
  }
};

#endif
