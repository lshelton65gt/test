#ifndef __MODELKIT_H__
#define __MODELKIT_H__

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "KitManifest.h"

#define MODELKIT_XML "modelKit.xml"
#define MANIFEST_FILENAME "modelKitManifest.xml"

extern const char* gCarbonVersion();

class ModelKitUserWizard;

struct RetainedFile
{
  QString mSrcName;
  QString mDestDir;
};

// The class that represents a ModelKit (i.e. the .modelKit file)
class ModelKit : public DocumentedQObject
{
  Q_OBJECT

  Q_CLASSINFO("ClassName", "ModelKit");

  Q_PROPERTY(bool copyFiles READ getCopyFiles);
  Q_PROPERTY(bool askRoot READ getAskRoot);
  Q_PROPERTY(QString name READ getName);
  Q_PROPERTY(QString description READ getDescription);
  Q_PROPERTY(KitVersion* version READ getLatestVersion);
  Q_PROPERTY(QString release READ getRelease);
  Q_PROPERTY(QString softwareVersion READ getSoftwareVersion);
  Q_PROPERTY(QString componentName READ getComponentName WRITE setComponentName);
  Q_PROPERTY(QString revision READ getRevision);

public:
  enum KitScript { Preface, Epilogue };
  bool runKitScript(ModelKitUserWizard* wiz, KitUserContext* userContext, KitScript script);

  static void registerTypes(QScriptEngine* engine);
  static void registerScriptTypes(QMap<QString, const QMetaObject*>& map);
  static void registerIntellisense(JavaScriptAPIs* apis);

  void setPrefaceScript(const QString& normalizedName, const QString& actualName) 
  {
    mPrefaceScriptNormalizedName=normalizedName; 
    mPrefaceScriptActualName=actualName;
  }
  void setEpilogueScript(const QString& normalizedName, const QString& actualName) 
  {
    mEpilogueScriptNormalizedName=normalizedName; 
    mEpilogueScriptActualName=actualName;
  }

  QList<RetainedFile*>& getRetainedFiles() { return mRetainedFiles; }

  ModelKit() : DocumentedQObject(NULL)
  {
    mCopyFiles = true;
    mAskRoot = true;
    mSoftwareVersion = MODELKIT_VERSION_NUMBER;
    mRelease = CARBON_RELEASE_ID;
    mRevision = "1.0";
  }

  void setRevision(const QString& newVal) { mRevision=newVal; }

  bool writeKitFile(ModelKitWizard* wiz, const QString& name, KitManifest* manifest);
  KitManifest* openKitFile(const QString& name, const QString& armRoot);
  void setArmRoot(const QString& newVal) { mArmRoot=newVal; }
  KitManifest* createManifest(const QString& name, const QString& description);
  KitVersion* createVersion(KitManifest* manifest, const QString& vendorVersion, const QString& descr);
  KitManifest* findManifest(const QString& manifestName);
  KitManifest* copyManifest(const QString& manifestName);
  void deleteVersion(KitVersion* v)
  {
    mManifests.deleteManifest(v->getManifest());
    mVersions.deleteVersion(v); 
  }
 
  void reopenModelKit(const QString& name);
  void extractManifest(ZistreamZip& arRead, const QString& kitRoot, KitManifest* manifest, bool endUser = false);
  void serialize(const QString& modelKitName);
  void deserialize(const QString& modelKitXml);

public slots:
  void retainFile(const QString& fileName)
  {
    qDebug() << "user requested to retain" << fileName << "for modelKit" << this;
    RetainedFile* rf = new RetainedFile;
    rf->mSrcName = fileName;
    mRetainedFiles.append(rf);
  }

  void retainFile(const QString& fileName, const QString& subdirName)
  {
    qDebug() << "user requested to retain" << fileName << "in" << subdirName << "for modelkit" << this;
    RetainedFile* rf = new RetainedFile;
    rf->mSrcName = fileName;
    rf->mDestDir = subdirName;
    mRetainedFiles.append(rf);
  }

  QString getPrefaceScriptActual() const { return mPrefaceScriptActualName; }
  QString getEpilogueScriptActual() const { return mEpilogueScriptActualName; }

  QString getPrefaceScriptNormalized() const { return mPrefaceScriptNormalizedName; }
  QString getEpilogueScriptNormalized() const { return mEpilogueScriptNormalizedName; }

  QString getRevision() const { return mRevision; }

  QString getComponentName() const { return mComponentName; }
  void setComponentName(const QString& newVal) { mComponentName=newVal; }

  QString getRelease() const { return mRelease; }
  QString getSoftwareVersion() const { return mSoftwareVersion; }

  void setCopyFiles(bool newVal) { mCopyFiles=newVal; }
  bool getCopyFiles() const { return mCopyFiles; }

  void setAskRoot(bool newVal) { mAskRoot=newVal; }
  bool getAskRoot() const { return mAskRoot; }

  QString getName() const { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

  QString getDescription() const { return mDescription; }
  void setDescription(const QString& newVal) { mDescription=newVal; }

  QString getLicenseKey() const { return mLicenseKey; }
  void setLicenseKey(const QString& newVal) { mLicenseKey=newVal; }

  KitVersions* getVersions() { return &mVersions; }
  KitManifests* getManifests() { return &mManifests; }

  KitVersion* getLatestVersion();

public:
  bool extractFile(ZistreamZip& arRead, const QString& absolutePathName,
    const QString& normalizedName, QFile::Permissions perms);
  void setupEnvironment(const QString& armRoot);
  QString createEncryptedCommandFile(ModelKitWizard* wiz, const char* cmdFile);
  bool writeCommandFile(ModelKitWizard* wiz, QTextStream& outstream, const QString& cmdFile);

protected:
  virtual QString methodDescr(const QString& methodName, bool formatHtml) const;
  virtual QString methodDescription(const QString& methodName) const;
  virtual QString propertyDescr(const QString& propName, bool formatHtml) const;

private:
  QString mRevision;
  QString mName;
  QString mDescription;
  QString mLicenseKey;
  QString mXMLVersion;
  QString mSoftwareVersion;
  QString mRelease;
  QString mComponentName;
  QString mPrefaceScriptNormalizedName;
  QString mPrefaceScriptActualName;
  QString mEpilogueScriptNormalizedName;
  QString mEpilogueScriptActualName;
  KitManifest mManifest;

  bool mCopyFiles;
  bool mAskRoot;

  KitVersions mVersions;
  KitManifests mManifests;

  UtString mEnvArm;
  QString mArmRoot;

  QList<RetainedFile*> mRetainedFiles;
};

Q_DECLARE_METATYPE(KitManifests*);
Q_DECLARE_METATYPE(KitVersions*);
Q_DECLARE_METATYPE(KitVersion*);

#endif
