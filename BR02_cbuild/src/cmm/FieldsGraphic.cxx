//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "FieldsGraphic.h"
#include "SpiritXML.h"
#include <QtGui>

#define FIELD_HEIGHT 60
#define FIELD_SPACING 30
#define BRISTLE_HEIGHT 10

#define TEXT_HEIGHT(txt) (int)(txt->boundingRect().height())
#define TEXT_WIDTH(txt) (int)(txt->boundingRect().width())


FieldsGraphic::FieldsGraphic(QWidget *parent)
: QGraphicsView(parent)
{
}

FieldsGraphic::~FieldsGraphic()
{

}

// Draw the bit label (0-nn)
void FieldsGraphic::drawBitNumber(int bitNumber)
{
  int cellWidth = width() / 16;
  int cellNumber = bitNumber / 16;

  int x = 0;
  int y = cellNumber * (FIELD_HEIGHT + FIELD_SPACING);

  QGraphicsTextItem* txt = new QGraphicsTextItem();
  txt->setPlainText(QString("%1").arg(bitNumber));
  
  int tw = TEXT_WIDTH(txt);
  int th = TEXT_HEIGHT(txt);
  
  txt->setZValue(3);

  int endBit = 32;
  if ((cellNumber % 2) == 0) // even
    endBit = 16;

  int tx = x + cellWidth*(endBit-bitNumber);
  int ty = y - th;

  tx -= (cellWidth/2);
  tx -= (tw/2);

  txt->setPos(tx, ty);
  scene()->addItem(txt);
}

//
// Draw Vertical bar, and bitNumber to right of it
// and bitNumber+1 to the left of it.

// bitNumber = 0 based (0-31 or 0-63)
void FieldsGraphic::drawBitDivider(int bitNumber)
{
  int cellWidth = width() / 16;
  int cellNumber = bitNumber / 16;

  int x = 0;
  int y = cellNumber * (FIELD_HEIGHT + FIELD_SPACING);
  
  int endBit = 32;
  if ((cellNumber % 2) == 0) // even
    endBit = 16;

  drawBitNumber(bitNumber);
  
  if (endBit-1 - bitNumber > 0)
    drawBitNumber(bitNumber+1);

  int cx = x + cellWidth*(endBit-bitNumber);
  int cy = y;

  QGraphicsLineItem* line = new QGraphicsLineItem();
  line->setLine(cx - cellWidth, cy + FIELD_HEIGHT, cx - cellWidth, cy-BRISTLE_HEIGHT);

  line->setZValue(2);
  scene()->addItem(line);

  
}

void FieldsGraphic::drawRegister(SpiritRegister* reg)
{

  mHighlightRects.clear();

  QGraphicsScene* scene = new QGraphicsScene(this);
  setScene(scene);
  resetTransform();

  int vw = width();

  UInt32 nBlocks = reg->getSize() / 16;

  int x = 0;
  int y = 0;

  for (UInt32 i=0; i<nBlocks; i++)
  {
    QGraphicsRectItem* blockRect = new QGraphicsRectItem();

    int width = vw;
    int height = FIELD_HEIGHT;

    blockRect->setPen(QPen(Qt::black));
    blockRect->setBrush(QBrush(Qt::green));
    blockRect->setZValue(0);
    blockRect->setRect(x, y, width, height);


    scene->addItem(blockRect);

    QGraphicsTextItem* rightText = new QGraphicsTextItem();
    rightText->setPlainText(QVariant(i*16).toString());
    rightText->setZValue(2);
    int tx = x+width;
    int ty = y;
    rightText->setPos(tx, ty);
    scene->addItem(rightText);

    y += FIELD_HEIGHT + FIELD_SPACING;
  }

  for (UInt32 i=0; i<reg->numFields(); i++)
  {
    SpiritField* field = reg->getField(i);
    drawBitField(field);

  }
  qDebug() << "drawing reg" << reg;
  scale(0.80, 0.80);
  show();
}

// Draw the field label (multiple times if needed)
void FieldsGraphic::drawLabel(SpiritField* field)
{
  int startBit = (int)field->getBitOffset()->getBitOffset();
  int endBit = startBit + (int)field->getBitWidth()-1;
  int cellWidth = width() / 16;

  int numCells = field->getBitWidth() / 16;
  if (numCells == 0)
    numCells = 1;
  else
    numCells += 1;
  
  // For each cell the label appears
  for (int cell=0; cell<numCells; cell++)
  {
    int cellStartBit = cell*16;
    int cellEndBit = cellStartBit + 15;
    int rangeLeft = 0;
    int rangeRight = 0;
    if (startBit >= cellStartBit)
      rangeRight = startBit;
    else
      rangeRight = cellStartBit;

    if (endBit < cellEndBit)
      rangeLeft = endBit;
    else
      rangeLeft = cellEndBit;


    int rightBit = rangeRight - cell*16;
    int leftBit = rangeLeft - cell*16;

    qDebug() << rangeLeft << rangeRight << leftBit << rightBit;

    QGraphicsTextItem* txt = new QGraphicsTextItem();   
    txt->setPlainText(field->getName());
   
    int tw = TEXT_WIDTH(txt);
    int th = TEXT_HEIGHT(txt);

    int x = (cellWidth/2)-(tw/2) + (15-leftBit)*cellWidth  + ((leftBit-rightBit)/2)*cellWidth;
    int y = (FIELD_HEIGHT/2) - (th/2) + cell * (FIELD_HEIGHT + FIELD_SPACING);

    txt->setZValue(5);
    txt->setPos(x, y);

    scene()->addItem(txt);
  }
}
void FieldsGraphic::highlightField(SpiritField* field)
{
  foreach(QGraphicsRectItem* rect, mHighlightRects)
  {
    scene()->removeItem(rect);
  }

  mHighlightRects.clear();

  highlight(field);
}

void FieldsGraphic::highlight(SpiritField* field)
{
  int startBit = (int)field->getBitOffset()->getBitOffset();
  int endBit = startBit + (int)field->getBitWidth()-1;
  int cellWidth = width() / 16;

  int numCells = field->getBitWidth() / 16;
  if (numCells == 0)
    numCells = 1;
  else
    numCells += 1;
  
  // For each cell the label appears
  for (int cell=0; cell<numCells; cell++)
  {
    int cellStartBit = cell*16;
    int cellEndBit = cellStartBit + 15;
    int rangeLeft = 0;
    int rangeRight = 0;
    if (startBit >= cellStartBit)
      rangeRight = startBit;
    else
      rangeRight = cellStartBit;

    if (endBit < cellEndBit)
      rangeLeft = endBit;
    else
      rangeLeft = cellEndBit;


    int rightBit = rangeRight - cell*16;
    int leftBit = rangeLeft - cell*16;

    qDebug() << rangeLeft << rangeRight << leftBit << rightBit;

    QGraphicsRectItem* rect = new QGraphicsRectItem();   
    
    rect->setPen(QPen(Qt::black));
    rect->setBrush(QBrush(Qt::lightGray));
    rect->setZValue(1);

    mHighlightRects.push_back(rect);
    int x = (15-leftBit)*cellWidth;
    int y = cell * (FIELD_HEIGHT + FIELD_SPACING);

    int width = (1+leftBit-rightBit) * cellWidth;
    int height = FIELD_HEIGHT;

    rect->setRect(x, y, width, height);

    scene()->addItem(rect);
  }
}
void FieldsGraphic::drawBitField(SpiritField* field)
{
  int startBit = (int)field->getBitOffset()->getBitOffset();
  int endBit = startBit + (int)field->getBitWidth()-1;

  drawBitNumber(startBit);  
  if (startBit > 0)
    drawBitDivider(startBit-1);

  drawBitDivider(endBit);
  drawLabel(field);
}


