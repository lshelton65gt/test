/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#include "util/CarbonPlatform.h"
#include "MdiPackageTool.h"
#include <QtGui>
#include <QMenuBar>
#include <QToolBar>
#include "gui/CQt.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "SettingsEditor.h"
#include "PackageToolWizardWidget.h"
#include "ModelStudioCommon.h"

MDIPackageToolTemplate::MDIPackageToolTemplate(CarbonMakerContext* ctx)
: MDIDocumentTemplate(PACKAGE_COMPONENT_NAME, "Package Options", "") 
{
  mContext = ctx; 
}

void MDIPackageToolTemplate::updateMenusAndToolbars(QWidget* /*widget*/)
{
}

int MDIPackageToolTemplate::createToolbars()
{
  QToolBar* toolbar = new QToolBar("Package Toolbar");
  toolbar->setObjectName("PackageTool Toolbar1");

  //toolbar->addAction(mActionDelete);
  //toolbar->addAction(mActionNewParam);

  addToolbar(toolbar);
  return numToolbars();
}

int MDIPackageToolTemplate::createMenus()
{
  QMenu* fileMenu = new QMenu("&Edit");
  fileMenu->setObjectName("PackageToolEditMenu");
  //fileMenu->addAction(mActionDelete);

  //registerAction(mActionDelete, SLOT(actionDelete()));
  //registerAction(mActionNewParam, SLOT(actionNewParameter()));

  addMenu(fileMenu);

  return numMenus();
}

MDIWidget* MDIPackageToolTemplate::createNewDocument(QWidget* /*parent*/)
{
  return NULL;
}

// We don't have a MDI document presence, only the docking window widget
// so we create the InvisibleProject here to handle menu & toolbar events
//
MDIWidget* MDIPackageToolTemplate::openDocument(QWidget* parent, const char* docName)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  PackageToolWizardWidget* widget = new PackageToolWizardWidget(mContext, this, parent);

  bool status = widget->loadFile(docName);

  QApplication::restoreOverrideCursor();

  if (status)
    return widget;
  else
  {
    widget->close();
    return NULL;
  }
}
