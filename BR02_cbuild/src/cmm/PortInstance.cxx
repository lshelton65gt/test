//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "PortInstance.h"
#include "Block.h"

PortInstance::PortInstance(Port* port, BlockInstance* blockInst, const QString& instanceID)
{
  mPort = port;
  mBlockInstance = blockInst;
  mInstanceID = instanceID;
}

void PortInstance::elaboratePortName()
{
  QString argName = "^(instanceID)";
  QString argValue = mInstanceID;
  mElaboratedName = mPort->getFormat().replace(argName, argValue);

  QString baseName = mPort->getBaseName();
  if (baseName.length() > 0)
  {      
    QRegExp exp("(\\$\\(.*\\))");
    int ix = exp.indexIn(mElaboratedName);
    int nc = exp.numCaptures();
    if (ix != -1 && nc == 1)
    {
      QString varname = exp.cap(1);
      mElaboratedName = mElaboratedName.replace(varname, baseName);
    }
  }
}

QString PortInstance::getName()
{
  elaboratePortName();

  return mElaboratedName; 
}

QString PortInstance::getUnelaboratedName()
{
  QString argName = "^(instanceID)";
  QString argValue = mInstanceID;
  QString name = mPort->getFormat().replace(argName, argValue);
  return name;
}





