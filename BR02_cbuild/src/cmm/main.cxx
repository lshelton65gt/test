//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QtGui/QApplication>
#include <QtGui>
#include "cmm.h"
#include "gui/CQt.h"
#include "CQtWizardContext.h"
#include "util/CarbonVersion.h"

#include "util/CarbonHome.h"
#include "util/OSWrapper.h"

extern bool gDisableCarbonMemSystem;
extern bool gCarbonMemSystemIsInitialized();
extern const char* gCarbonVersion();

int main(int argc, char *argv[])
{    

  Q_INIT_RESOURCE(cmm);

  if (gCarbonMemSystemIsInitialized())
  {
    printf("CarbonMemory system has been initialized, aborting.\n");
    return 1;
  }
  // QT has a nasty FileOpen dialog bug which causes problems interacting
  // with ModelStudio and other Carbon Apps, this disables it.
  gDisableCarbonMemSystem = true;

#if pfWINDOWS

  // Try to set CARBON_HOME and PATH environment variables based on
  // location of the executable file we're running.  On Unix we do
  // this with the script in $CARBON_HOME/bin instead, but starting a
  // batch file on Windows opens an ugly console window, so for
  // Windows we do this.

  // Convert argv[0], that the user typed, to an absolute path.
  char* absPath = _fullpath(NULL, argv[0], 0);

  // Get path (without filename) into path.
  UtString path, file;
  OSParseFileName(absPath, &path, &file);
  free(absPath);                // Allocated by _fullpath()
  absPath = NULL;

  // In the normal case where we're running from
  // CARBON_HOME/bin/modelstudio, CARBON_HOME is two dirctories up.
  path += "/../..";
  absPath = _fullpath(NULL, path.c_str(), 0);
  
  // Try setting it.  If it doesn't look like a CARBON_HOME directory,
  // SetCarbonHome() returns false.
  if (!SetCarbonHome(absPath)) {
    // That didn't work.  Maybe we're in the development environment,
    // running from somewhere like
    // /w/lr/larry/sandbox/home/obj/Win.product/cmm/modelstudio.  In
    // that case we just tried obj.  If this directory is named obj,
    // try its parent.
    OSParseFileName(absPath, &path, &file);
    free(absPath);              // Allocated by _fullpath()
    absPath = NULL;
    if (file == "obj")
      SetCarbonHome(path.c_str());
    // That may or may not have worked.  If not, we leave CARBON_HOME
    // and PATH as they were when we were called.
  }
    
#endif // pfWINDOWS

  CQtWizardContext a(argc, argv);

  bool batchMode = cmm::checkArgument("-build");
  if (cmm::checkArgument("-batch"))
    batchMode = true;
  else if (cmm::checkArgument("-modelKit"))
    batchMode = true;

  QSplashScreen* splash = NULL;
  if (!batchMode && !cmm::checkArgument("-modelKit") && !cmm::checkArgument("-batch"))
  {
    QPixmap pixmap(":/cmm/Resources/Model_Studio_Splash.png");
    splash = new QSplashScreen(pixmap);
    splash->showMessage(CARBON_RELEASE_ID, Qt::AlignRight, QColor("white"));
    splash->show();
  }

  a.init(argc, argv);
  a.putType(CQtWizardContext::eMult);
  a.processEvents();

  cmm w(&a);
  
  bool debugWindows = cmm::checkArgument("-debugWindows");
  qDebug() << "debugWindows" << debugWindows << "batchMode" << batchMode << "exitCode" << w.getExitCode();

  if (debugWindows || (w.getExitCode() == 0 && !batchMode))
  {
    qDebug() << "showing cms main window";
    w.show();
  }
  else
    qDebug() << "not showing main window";

  if (!debugWindows && w.getExitCode() != 0)
  {
    qDebug() << "exiting" << w.getExitCode();
    return w.getExitCode();
  }

  if (splash)
    splash->finish(&w);

  w.processArguments();

  w.maybeShowWizard();

  a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));

  int result = a.exec();

  return result;
}
