//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#include "util/CarbonVersion.h"
#include "ModelKitWizard.h"
#include "CarbonMakerContext.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"
#include "CarbonComponent.h"
#include "KitManifest.h"
#include "WizDlgInputFile.h"
#include "WizDlgExecuteScript.h"
#include "WizDlgRunScript.h"
#include "DlgConfirmDelete.h"
#include "DlgNewVersion.h"
#include "DlgEditVersion.h"

#define LAST_KIT "versions/lastKit"
#define LAST_CHOICE "versions/lastChoice"

MKWPageVersions::MKWPageVersions(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);

  mIsValid = true;

  setTitle("Versions");
  setSubTitle("Specify the Model Kit Version");
}

void MKWPageVersions::initializePage()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());

  on_radioButtonStartKit_toggled(false);
  on_radioButtonNewKit_toggled(true);
  restoreSettings();
  updateCheckboxes();
  updateVersionButtons();

  if (wiz->getRegenerate())
    wiz->next();
}

bool MKWPageVersions::validatePage()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());
  ModelKit* kit = wiz->getModelKit();

  // Set the active manifest
  updateVersionButtons();

  // This is set only if we want to use it
  wiz->setARMRoot("");

  kit->setName(ui.lineEditKitName->text());
  kit->setRevision(ui.lineEditRevision->text());
  kit->setDescription(ui.lineEditDescription->text());
  kit->setLicenseKey(ui.lineEditLicenseKey->text());
  kit->setCopyFiles(ui.checkBoxClone->isChecked());
  kit->setAskRoot(ui.checkBoxPromptARM->isChecked());

  QString preScript = ui.lineEditPrefaceScript->text();
  kit->setPrefaceScript(preScript, wiz->expandName(preScript));

  QString epiScript = ui.lineEditEpilogueScript->text();
  kit->setEpilogueScript(epiScript, wiz->expandName(epiScript));

  KitManifest* manifest = wiz->getActiveManifest(); 

  qDebug() << "Creating Kit" << kit->getName() << "using Manifest" << manifest->getName();

  // Inform the restore about this context
  manifest->setWizard(wiz);

  manifest->setCopyFiles(kit->getCopyFiles());
  manifest->setAskRoot(kit->getAskRoot());

  wiz->setCopyFiles(kit->getCopyFiles());
  wiz->setAskRoot(kit->getAskRoot());

  wiz->removeUserActions();
  wiz->getUserFileList().clear();
  wiz->getOptionalHashedFileList().clear();
  wiz->getFileOptionsMap().clear();

  QMap<QString,bool>& wizFileMap = wiz->getCheckedFileMap();
  wizFileMap.clear();

  QMap<QString,bool>& manifestMap = manifest->getFileCheckedMap();
  foreach (QString fileName, manifestMap.keys())
    wiz->addCheckFile(fileName, manifestMap[fileName]);

  QMap<QString, RTLFileOptions*> optionsMap = manifest->getFileOptionsMap();
  foreach (QString fileName, optionsMap.keys())
  {
    RTLFileOptions* rfu = optionsMap[fileName];
    wiz->getFileOptionsMap()[fileName] = rfu;
  }

 /* foreach (KitFile* kf, manifest->getOptionalRTLFiles())
  {
    OptionalHashedFile* hf = new OptionalHashedFile();
    hf->mFileName = kf->getActualName();
    hf->mChecked = kf->getCheckHash();
    wiz->getOptionalHashedFileList().append(hf);
  }*/

  foreach (KitFile* kf, manifest->getUserFiles())
  {
    QString actualName = wiz->expandName(kf->getNormalizedName());    
    wiz->getUserFileList().append(actualName);
  }

  foreach (KitUserAction* action, manifest->getPreUserActions())
    wiz->addPreUserAction(action);

  foreach (KitUserAction* action, manifest->getPostUserActions())
    wiz->addPostUserAction(action);

  saveSettings();

  return true;
}

void MKWPageVersions::saveSettings()
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  QString baseName;
  if (proj)
    baseName = QString(proj->getProjectDirectory()) + "/";
  
  mSettings.setValue(baseName + LAST_CHOICE, ui.radioButtonStartKit->isChecked());
  mSettings.setValue(baseName + LAST_KIT, ui.lineEditKitFile->text());
}


void MKWPageVersions::restoreSettings()
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  QString baseName;
  if (proj)
    baseName = QString(proj->getProjectDirectory()) + "/";

  if (theApp->checkArgument("-modelKit"))
  {
    QString modelKitFile = theApp->argumentValue("-modelKit");
    qDebug() << "modelkitfilearg" << modelKitFile;
    QFileInfo fi(modelKitFile);
    if (fi.isRelative())
    {
      modelKitFile = theApp->getWorkingDirectory() + "/" + modelKitFile;
      qDebug() << "adjusted modelkitfilearg" << modelKitFile;
    }

    ui.lineEditKitFile->setText(modelKitFile);
    ui.radioButtonStartKit->setChecked(true);
    on_radioButtonStartKit_toggled(true);
  }
  else
  {
    ui.lineEditKitFile->setText(mSettings.value(baseName + LAST_KIT, "").toString());
    ui.radioButtonStartKit->setChecked(mSettings.value(baseName + LAST_CHOICE, false).toBool());
  }

  qDebug() << "modelkitfile" << ui.lineEditKitFile->text();
}


int MKWPageVersions::nextId() const
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());
  ModelKit* kit = wiz->getModelKit();

  if (kit->getAskRoot())
    return ModelKitWizard::PageInfo;
  else
    return ModelKitWizard::PageFiles;
}


MKWPageVersions::~MKWPageVersions()
{
}

void MKWPageVersions::on_pushButtonNew_clicked()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());
  DlgNewVersion dlg(this, wiz->getModelKit());
  if (dlg.exec())
  {
    ModelKit* kit = wiz->getModelKit();
    switch (dlg.getManifestType())
    {
    case DlgNewVersion::NewManifest:
      {
        KitManifest* mf = kit->createManifest(dlg.getManifestName(), dlg.getDescription());
        wiz->setActiveManifest(mf);
      }
      break;
    case DlgNewVersion::CopyManifest:
      {
        KitManifest* mf = kit->copyManifest(dlg.getManifestName());
        wiz->setActiveManifest(mf);
      }
      break;
    }

    kit->createVersion(wiz->getActiveManifest(), dlg.getVendorVersion(), dlg.getDescription());

    populate(kit);
  }
}

void MKWPageVersions::setItemData(QTreeWidgetItem* item, KitVersion* ver)
{
  QVariant qv = qVariantFromValue((void*)ver);
  item->setData(0, Qt::UserRole, qv);

  item->setText(0, QString("%1").arg(ver->getID()));
  item->setText(1, ver->getManifest()->getName());
  item->setText(2, ver->getDescription());
  item->setText(3, ver->getVendorVersion());

  if (ver->getLocked())
    item->setIcon(0, QIcon(":/cmm/Resources/lock.png"));
  else
    item->setIcon(0, QIcon(":/cmm/Resources/lock_open.png"));
}
void MKWPageVersions::populate(ModelKit* kit)
{
  ui.treeWidget->clear();
  KitVersions* versions = kit->getVersions();

  for(UInt32 i=0; i<versions->numVersions(); i++)
  {
    KitVersion* ver = versions->getVersion(i);
    QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidget);
    setItemData(item, ver);
    if (i == versions->numVersions()-1)
      ui.treeWidget->setCurrentItem(item);
  }

  ui.lineEditKitName->setText(kit->getName());
  ui.lineEditRevision->setText(kit->getRevision());
  ui.lineEditDescription->setText(kit->getDescription());
  ui.lineEditLicenseKey->setText(kit->getLicenseKey());
  ui.lineEditPrefaceScript->setText(kit->getPrefaceScriptNormalized());
  ui.lineEditEpilogueScript->setText(kit->getEpilogueScriptNormalized());
  ui.checkBoxClone->setChecked(kit->getCopyFiles());
  ui.checkBoxPromptARM->setChecked(kit->getAskRoot());

  autoFit();
}


void MKWPageVersions::autoFit()
{
  QStringList headers;
  headers << "ID" << "Manifest" << "Description" << "Vendor Version";

  ui.treeWidget->setColumnCount(headers.count());

  ui.treeWidget->header()->resizeSections(QHeaderView::ResizeToContents);
  
  for (int i=0; i<headers.count()-1; i++)
    ui.treeWidget->header()->setResizeMode(i, QHeaderView::Interactive);
  
  ui.treeWidget->header()->setResizeMode(headers.count()-1, QHeaderView::Stretch);

  updateCheckboxes();
}

void MKWPageVersions::on_toolButton_clicked()
{
  QString modelKitName = QFileDialog::getOpenFileName(this, 
    tr("Carbon ModelKit"), "", tr("Model Kit (*.modelKit)"));
  if (!modelKitName.isEmpty())
  {
    ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());
    ui.lineEditKitFile->setText(modelKitName);
    ModelKit* mkit = wiz->newModelKit();
    mkit->reopenModelKit(modelKitName);
    populate(mkit);
  }
}

void MKWPageVersions::on_treeWidget_itemSelectionChanged()
{
  updateVersionButtons();
}

void MKWPageVersions::on_radioButtonNewKit_toggled(bool checked)
{
  if (checked)
  {
    ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());
    ModelKit* mkit = wiz->newModelKit();

    KitManifest* manifest = mkit->createManifest("Default", "<Manifest Description>");
    mkit->createVersion(manifest, "rel0", "<Description Goes Here>");
    wiz->setActiveManifest(manifest);
   
    CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
    CarbonProject* proj = pw->project();

    QFileInfo projInfo(proj->projectFile());

    mkit->setName(projInfo.baseName());
    mkit->setDescription("Kit Description>");

    mkit->setAskRoot(true);
    mkit->setCopyFiles(true);

    ui.checkBoxPromptARM->setChecked(true);
    ui.checkBoxClone->setChecked(true);

    populate(mkit);
  }
}

void MKWPageVersions::on_radioButtonStartKit_toggled(bool checked)
{
  ui.lineEditKitFile->setEnabled(checked);

  if (checked)
  {
    ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());

    if (!ui.lineEditKitFile->text().trimmed().isEmpty())
    {
      ModelKit* mkit = wiz->newModelKit();

      QFileInfo fi(ui.lineEditKitFile->text().trimmed());
      if (fi.exists())
      {
        mkit->reopenModelKit(ui.lineEditKitFile->text().trimmed());
        populate(mkit);

        if (theApp->checkArgument("-modelKitRevision"))
          ui.lineEditRevision->setText(theApp->argumentValue("-modelKitRevision"));
      }
      else
      {
        QString msg = QString("No such modelkit file: %1").arg(ui.lineEditKitFile->text().trimmed());
        QMessageBox::critical(NULL, MODELSTUDIO_TITLE, msg);
      }
    }
  }
}


void MKWPageVersions::updateCheckboxes()
{
 ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());
 ModelKit* mkit = wiz->getModelKit();

 int numVersions = mkit->getVersions()->numVersions();
 ui.checkBoxPromptARM->setEnabled(numVersions <= 1);
 ui.checkBoxClone->setEnabled(numVersions <= 1);

 if (numVersions <= 1)
 {
   ui.checkBoxClone->setEnabled(ui.checkBoxPromptARM->isChecked());
   if (!ui.checkBoxPromptARM->isChecked())
     ui.checkBoxClone->setChecked(false);
 }
}

void MKWPageVersions::on_checkBoxPromptARM_stateChanged(int)
{
  updateCheckboxes();
}

void MKWPageVersions::on_checkBoxClone_stateChanged(int)
{
  updateCheckboxes();
}

void MKWPageVersions::updateVersionButtons()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());

  ui.pushButtonDeleteVersion->setEnabled(false);
  ui.pushButtonEditVersion->setEnabled(false);

  if (ui.treeWidget->selectedItems().count() > 0)
  {
    QTreeWidgetItem* item = ui.treeWidget->selectedItems().first();
    QVariant v = item->data(0, Qt::UserRole);
    KitVersion* ver = (KitVersion*)v.value<void*>();

    wiz->setActiveManifest(ver->getManifest());
   
    int numVersions = ui.treeWidget->topLevelItemCount();
    int itemIndex = ui.treeWidget->indexOfTopLevelItem(item);

    if (numVersions > 1 && !ver->getLocked() && itemIndex == numVersions-1)
      ui.pushButtonDeleteVersion->setEnabled(true);
   
    ui.pushButtonEditVersion->setEnabled(true);
  }
}


void MKWPageVersions::on_pushButtonEditVersion_clicked()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());

  if (ui.treeWidget->selectedItems().count() > 0)
  {
    QTreeWidgetItem* item = ui.treeWidget->selectedItems().first();
    QVariant v = item->data(0, Qt::UserRole);
    KitVersion* ver = (KitVersion*)v.value<void*>();
    // If this is the latest version, then it's considered "New"
    qDebug() << "Kit" << ver->getID();

    DlgEditVersion dlg(this, wiz->getModelKit(), ver);
    if (dlg.exec())
    {
      ver->getManifest()->setName(dlg.getManifestName());
      ver->setDescription(dlg.getDescription());
      ver->setLocked(dlg.getLocked());
      ver->setVendorVersion(dlg.getVendorVersion());
      setItemData(item, ver);
      autoFit();
    }
  }
}

void MKWPageVersions::on_pushButtonDeleteVersion_clicked()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());
  if (ui.treeWidget->selectedItems().count() > 0)
  {
    QTreeWidgetItem* item = ui.treeWidget->selectedItems().first();
    QVariant v = item->data(0, Qt::UserRole);
    KitVersion* ver = (KitVersion*)v.value<void*>();

    if (wiz->getModelKit()->getLatestVersion() == ver)
    {
      wiz->getModelKit()->deleteVersion(ver);
      populate(wiz->getModelKit());
    }
  }
}

void MKWPageVersions::on_treeWidget_itemDoubleClicked(QTreeWidgetItem*,int)
{
  on_pushButtonEditVersion_clicked();
}


void MKWPageVersions::on_toolButtonPrefaceScript_clicked()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());

  QString fileName = QFileDialog::getOpenFileName(this, "Preface Script",
    ui.lineEditPrefaceScript->text(),"Script files (*.js)");

  QString normalized = wiz->normalizeName(fileName);
  ui.lineEditPrefaceScript->setText(normalized);
}


void MKWPageVersions::on_toolButtonEpilogueScript_clicked()
{
  ModelKitWizard* wiz = static_cast<ModelKitWizard*>(wizard());

  QString fileName = QFileDialog::getOpenFileName(this, "Epilogue Script",
    ui.lineEditEpilogueScript->text(),"Script files (*.js)");

  QString normalized = wiz->normalizeName(fileName);
  ui.lineEditEpilogueScript->setText(normalized);

}
