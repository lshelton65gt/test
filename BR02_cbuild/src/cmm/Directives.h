#ifndef __CMMDIRECTIVES_H___
#define __CMMDIRECTIVES_H___

#include "util/CarbonPlatform.h"

#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/SourceLocator.h"
#include "util/UtVector.h"

#include <QList>

#include "util/XmlParsing.h"

//typedef UtVector<UtStringArray> TokenVector;
//! Enum for controlling how directives work.
/*! The directives can be module based only (module.net), instance
*  based only (top.A.B.net), or both. This enum defines what mode a
*  directive uses.
*/
enum DirectiveMode
{
  eDirModule,         //!< process directive nets as module.net
  eDirInstance,       //!< process directive nets as instance names
  eDirBoth            //!< process directive nets as both module/instance
};

// Note if you change or add any of these, fix the dirToString function
enum DesignDirective 
{
  eNone, eFastClock, eObserve, eForce, eDeposit, eIgnoreOutput, eCollapseClocks,
  eInputDir, eCModuleBegin, eCPortBegin, eCTiming, eCFanin, eCPortEnd,
  eCModuleEnd, eClockSpeed, eExpose, eCFunctionBegin, eCFunctionEnd,
  eCTaskBegin, eCTaskEnd, eCArg, eTieNet, eCNullPortBegin, eCNullPortEnd,
  eCHasSideEffect, eCCallOnPlayback, eSubstituteModule, eSubstituteEntity, eOutputsTiming, 
  eInputsTiming, eBlast, eVectorMatch, 
  eWrapperPort2State, eWrapperPort4State,
  eScDeposit, eScObserve, eDelayedClock, eDepositFrequent, 
  eDepositInfrequent, eSlowClock, eFastReset, eAsyncReset, eTraceSignal,
  eOnDemandEligible, eOnDemandExcluded, eMvDeposit, eMvObserve
};

class CarbonDirectives;
class CarbonDirectiveGroup;


enum DirectiveEditor
{
  eDirEditorNone,
  eDirEditorNetList
};

struct DirectiveDefinition
{
  DirectiveMode mMode;
  UtString mName;
  DirectiveEditor mEditor;
  bool mBuiltin;

  DirectiveDefinition()
  {
    mBuiltin=false;
  }

//  TokenVector mTokenValues;
//  void addTokenValue(const UtStringArray& tok) {mTokenValues.push_back(tok);}

};

enum DirectiveType
{
  eDirectiveNet,
  eDirectiveModule
};

struct Directive 
{
  UtString mDirective;
//  TokenVector mTokens;
  DirectiveDefinition* mDefinition;

  // used to hold auxiliary parse token, such as the frequency from
  // clockSpeed
  UtString mValue;

  Directive(const char* dir, const char* val, DirectiveDefinition* def)
    : mDirective(dir)
  {
    mDefinition = def;
    if (val != NULL)
      mValue = val;
  }

  Directive* copyDirective();
  void serialize(xmlTextWriterPtr writer, CarbonDirectives* dirs, CarbonDirectiveGroup* group);
  static void deserialize(DirectiveType type, CarbonDirectives* dirs, CarbonDirectiveGroup* group, xmlNodePtr parent, UtXmlErrorHandler* eh);
//  void addToken(const UtStringArray& tok) {mTokens.push_back(tok);}

private:
  Directive(const Directive&); // forbid
  Directive& operator=(const Directive&); // forbid
};


struct NetDirectives
{
  UtString mLocator;

  NetDirectives(const char* netLocator) : mLocator(netLocator)
  {
  }
  void addDirective(Directive* dir) { mDirectives.push_back(dir); }
  Directive* findDirective(const char* name) 
  {
    foreach (Directive* dir, mDirectives)
    {
      if (dir->mDirective == name)
        return dir;
    }
    return NULL;
  }
  void copyDirectives(CarbonDirectiveGroup*);
  void deserialize(xmlNodePtr parent, CarbonDirectives* dirs, CarbonDirectiveGroup* group);
  void serialize(xmlTextWriterPtr writer, CarbonDirectives* dirs, CarbonDirectiveGroup* group);
  QList<Directive*> mDirectives;
};


struct ModuleDirectives
{
  UtString mLocator;

  ModuleDirectives(const char* netLocator) : mLocator(netLocator)
  {
  }
  Directive* findDirective(const char* name) 
  {
    foreach (Directive* dir, mDirectives)
    {
      if (dir->mDirective == name)
        return dir;
    }
    return NULL;
  }
  void copyDirectives(CarbonDirectiveGroup*);
  void deserialize(xmlNodePtr parent, CarbonDirectives* dirs, CarbonDirectiveGroup* group);
  void serialize(xmlTextWriterPtr writer, CarbonDirectives* dirs, CarbonDirectiveGroup* group);
  void addDirective(Directive* dir) { mDirectives.push_back(dir); }
  QList<Directive*> mDirectives;
};

#endif
