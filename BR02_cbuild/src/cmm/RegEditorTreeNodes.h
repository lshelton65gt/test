#ifndef __REGEDITORTREENODES__
#define __REGEDITORTREENODES__

#include "util/CarbonPlatform.h"
#include "CarbonProjectTreeNode.h"
#include "CarbonProject.h"
#include "CarbonProjectWidget.h"
#include "CarbonOptions.h"
#include "SettingsEditor.h"
#include "CarbonMakerContext.h"
#include "SpiritXML.h"

// The Root Node for all Memory Maps
class MemoryMapsItem : public QObject, public CarbonProjectTreeNode
{
    Q_OBJECT

public:
  MemoryMapsItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, SpiritXML* sxml) : CarbonProjectTreeNode(proj, parent)
  {
    mSpiritXML = sxml;
    mMemoryMaps = sxml->getMemoryMaps();
    setIcon(0, QIcon(":/cmm/Resources/project.png"));
    setText(0, "Memory Maps");
  
    actionAddMemoryMap = new QAction(QIcon(":/cmm/Resources/DeleteHS.png"), "&Add Memory Map...", this);
    actionAddMemoryMap->setStatusTip("Add a new memory map region");
    CQT_CONNECT(actionAddMemoryMap, triggered(), this, addMemoryMap());
  }


  virtual void showContextMenu(const QPoint& point)
  {
    QMenu menu;
    menu.addAction(actionAddMemoryMap);
    menu.exec(point);

  }

  virtual void singleClicked(int);
  virtual void doubleClicked(int);

private slots:
  void addMemoryMap()
  {
    qDebug() << "add memory map";
  }

private:
  SpiritMemoryMaps* mMemoryMaps;
  SpiritXML* mSpiritXML;
  QAction* actionAddMemoryMap;
};


class MemoryMapItem: public QObject, public CarbonProjectTreeNode
{
    Q_OBJECT

public:
  MemoryMapItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, SpiritMemoryMap* mmap) : CarbonProjectTreeNode(proj, parent)
  {
    mMemoryMap = mmap;
    setIcon(0, QIcon(":/cmm/Resources/memoryMap.png"));
    setText(0, mmap->getName());
  }

  virtual void showContextMenu(const QPoint&)
  {
  }

  virtual void singleClicked(int)
  {
  }

  virtual void doubleClicked(int)
  {
  }
private:
  SpiritMemoryMap* mMemoryMap;
};

class MemoryBankItem : public QObject, public CarbonProjectTreeNode
{
    Q_OBJECT

public:
  MemoryBankItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, SpiritBank* bank);

private:
  SpiritBank* mBank;
};

class AddressBlockItem : public QObject, public CarbonProjectTreeNode
{
    Q_OBJECT

public:
  AddressBlockItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, SpiritAddressBlock* ab);
  
  virtual void singleClicked(int);
  virtual void doubleClicked(int);

private:
  SpiritAddressBlock* mAddressBlock;
};

class RegisterItem : public QObject, public CarbonProjectTreeNode
{
    Q_OBJECT

public:
  RegisterItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, SpiritRegister* reg);
  

private:
  SpiritRegister* mRegister;
};

class RegisterFieldItem : public QObject, public CarbonProjectTreeNode
{
    Q_OBJECT

public:
  RegisterFieldItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, SpiritRegister* reg, SpiritField* field);
  

private:
  SpiritRegister* mRegister;
  SpiritField* mField;
};


#endif
