//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "MemoryWizard.h"
#include "WizPgMultiPortMap.h"
#include "cmm.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"
#include "DesignXML.h"

#define CHOOSE_ITEM "<Choose...>"
#define COMMON_ITEM "(Common)"

int WizPgMultiPortMap::nextId() const
{
  if (mHasParameters)
    return MemoryWizard::PageMultiParamMap; 
  else
    return MemoryWizard::PageMultiGenerate;
}

WizPgMultiPortMap::WizPgMultiPortMap(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);

  mIsValid = true;
  mHasParameters = false;

  setTitle("Port Function Mapping");
  setSubTitle("Map all unique ports to their associated function.");

  QStringList labels;
  labels << "Port Name";
  labels << "Function";
  labels << "Port Number";

  ui.tableWidget->setColumnCount(labels.count());
  ui.tableWidget->setHorizontalHeaderLabels(labels);
 }

WizPgMultiPortMap::~WizPgMultiPortMap()
{
}

#define SETTING_REMEMBER "wizPgMultiPortMap/rememberMappings"

void WizPgMultiPortMap::initializePage()
{
  MemoryWizard* wiz = static_cast<MemoryWizard*>(wizard());
  
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  QString configDir = proj->getActive()->getOutputDirectory();
  QString desName = proj->getDesignFilePath(".designHierarchy");
  QFileInfo fi(desName);
  QString outputName = QString("%1/%2.modules").arg(configDir).arg(fi.baseName());
  wiz->serializeModulesFile(outputName);

  wiz->setOption("moduleListFile", outputName);

  qDebug() << "generated" << outputName;

  QString mappingFileName = QString("%1/%2.portConfigurations").arg(configDir).arg(fi.baseName());
  QString memGenFilename = QString("%1/%2.memGen").arg(configDir).arg(fi.baseName());

  CarbonConsole::CommandMode mode = CarbonConsole::Local;

  pw->makeConsoleVisible();

#if pfWINDOWS
  QString program = "${CARBON_HOME}/bin/carbon.bat";
  QString argOutputName = proj->getWindowsEquivalentPath(outputName);
  QString argDesignHierarchy = proj->getWindowsEquivalentPath(wiz->getOption("designHierarchyFile").toString());
  QString argMappingFile = proj->getWindowsEquivalentPath(mappingFileName);
#else
  QString program = "$(CARBON_HOME)/bin/carbon";
  QString argOutputName = proj->getUnixEquivalentPath(outputName);
  QString argDesignHierarchy = proj->getUnixEquivalentPath(wiz->getOption("designHierarchyFile").toString());
  QString argMappingFile = proj->getUnixEquivalentPath(mappingFileName);
#endif

  QStringList args;
  args << "remodeler";
  args << "--analyze";
  args << "--template";
  args << wiz->getOption("template").toString();
  args << "--inputModules";
  args << argOutputName;
  args << "--designHierarchy";
  args << argDesignHierarchy;
  args << "--output";
  args << argMappingFile;

  UtString subDir;
  subDir << proj->getActivePlatform() << "/" << proj->getActiveConfiguration();

  qDebug() << program << args;

  QApplication::setOverrideCursor(Qt::WaitCursor);
  int exitCode = pw->getConsole()->executeCommandAndWait(mode, CarbonConsole::Command, program, args, subDir.c_str());
  QApplication::restoreOverrideCursor();

  qDebug() <<  "carbon memgen -analyze, exitCode" << exitCode;

  mHasParameters = false;

  if (exitCode == 0)
  {
    MWPortConfigurations* pf = wiz->getPortConfigurations();
    XmlErrorHandler* eh = pf->parseFile(mappingFileName);
    if (eh->hasErrors())
    {
      QMessageBox::critical(this, "Unexpected Error", eh->errorMessages().join("\n"));
      mIsValid = false;
    }
    else
    {
      mIsValid = true;
      mHasParameters = pf->getModuleParameters().count() > 0 ? true : false;
    }
  }
  else
    mIsValid = false;

  emit completeChanged();

  qDebug() << "exitCode" << exitCode;

  if (mIsValid)
    populate(memGenFilename);
}

bool WizPgMultiPortMap::isComplete() const
{
  return mIsValid;
}

void WizPgMultiPortMap::populate(const QString& memGenFilename)
{
  MemoryWizard* wiz = static_cast<MemoryWizard*>(wizard());
  MWPortConfigurations* pf = wiz->getPortConfigurations();
  
  if (ui.checkBoxRemember->isChecked())
    readPreviousMappings(memGenFilename);

  ui.tableWidget->clearContents();

  ui.tableWidget->setRowCount(pf->getPorts().count());

  int row = 0;
  int minLength = strlen(CHOOSE_ITEM);

  foreach (MWPort* port, pf->getPorts())
  {
    // Name
    QTableWidgetItem* nameItem = new QTableWidgetItem();
    nameItem->setText(port->getName());
    Qt::ItemFlags flags = nameItem->flags();
    nameItem->setFlags(flags & ~Qt::ItemIsEditable);
    QVariant qv = qVariantFromValue((void*)port);
    nameItem->setData(Qt::UserRole, qv);

    ui.tableWidget->setItem(row, WizPgMultiPortMap::colNAME, nameItem);

    // Function Combo
    QString defFuncName = port->getDefaultFunctionName();
    MWFunctionClass* funcClass = pf->getFunctionClass(port->getFunctionClass());
    QComboBox* functionCombo = new QComboBox(ui.tableWidget);

    functionCombo->addItem(CHOOSE_ITEM);
    for (int i=0; i<funcClass->numFunctions(); i++)
    {
      MWFunction* func = funcClass->getFunc(i);
      functionCombo->addItem(func->getDisplayName());
      if (func->getDisplayName().length() > minLength)
        minLength = func->getDisplayName().length();
    }

    functionCombo->setMinimumContentsLength(minLength);

    if (!defFuncName.isEmpty())
    {
      MWFunction* func = funcClass->findFunction(defFuncName);
      int index = functionCombo->findText(func->getDisplayName());
      functionCombo->setCurrentIndex(index);
    }
    else
    {
      bool itemSet = false;
      MWPortMap* pm = mPreviousMappings[port->getName()];
      if (ui.checkBoxRemember->isChecked() && pm != NULL)
      {
        QString prevClassName = pm->getFunctionClass();
        QString prevFuncName = pm->getPortFunction();

        MWFunctionClass* prevClass = pf->getFunctionClass(prevClassName);
        if (prevClass)
        {
          MWFunction* prevFunc = prevClass->findFunction(prevFuncName);
          if (prevFunc)
          {
            int index = functionCombo->findText(prevFunc->getDisplayName());
            if (index != -1)
            {
              functionCombo->setCurrentIndex(index);
              itemSet = true;
            }
          }
        }
      }
      if (!itemSet)
        functionCombo->setCurrentIndex(functionCombo->findText(CHOOSE_ITEM));
    }

    functionCombo->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLength);

    ui.tableWidget->setCellWidget(row, WizPgMultiPortMap::colFUNCTIONFANCY, functionCombo);

    QComboBox* portCombo = new QComboBox(ui.tableWidget);
    portCombo->addItem(COMMON_ITEM);
    int portIndex = port->getPortIndex();
    for (int i=0; i<255; i++)
    {
      portCombo->addItem(QString("%1").arg(i));
      if (i == portIndex) // first item is (ALL)
        portCombo->setCurrentIndex(i+1);
    }

    ui.tableWidget->setCellWidget(row, WizPgMultiPortMap::colPORTINDEX, portCombo);


    row++;
  }
 
  ui.tableWidget->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);

  ui.tableWidget->horizontalHeader()->setResizeMode(WizPgMultiPortMap::colNAME, QHeaderView::Interactive);
  ui.tableWidget->horizontalHeader()->setResizeMode(WizPgMultiPortMap::colFUNCTIONFANCY, QHeaderView::Interactive);
  ui.tableWidget->horizontalHeader()->setResizeMode(WizPgMultiPortMap::colPORTINDEX, QHeaderView::Interactive);

  int colWidth = ui.tableWidget->columnWidth(WizPgMultiPortMap::colFUNCTIONFANCY) + minLength;
  ui.tableWidget->setColumnWidth(WizPgMultiPortMap::colFUNCTIONFANCY, colWidth+50);

  ui.tableWidget->sortItems(0);
}

bool WizPgMultiPortMap::validatePage()
{
  MemoryWizard* wiz = static_cast<MemoryWizard*>(wizard());

  wiz->removeAllPortMaps();   

  MWPortConfigurations* pcs = wiz->getPortConfigurations();

  for (int i=0; i<ui.tableWidget->rowCount(); i++)
  {
    QTableWidgetItem* nameItem = ui.tableWidget->item(i, WizPgMultiPortMap::colNAME);
    QString portName = nameItem->text();

    QComboBox* funcCombo = qobject_cast<QComboBox*>(ui.tableWidget->cellWidget(i, WizPgMultiPortMap::colFUNCTIONFANCY));
    QString portFunction = funcCombo->currentText();
    if (CHOOSE_ITEM == portFunction)
    {
      QMessageBox::critical(NULL, MODELWIZARD_TITLE, "All ports must be mapped to functions before proceeding");
      return false;
    }
    else
    {
      QVariant qv = nameItem->data(Qt::UserRole);
      MWPort* port = (MWPort*)qv.value<void*>(); 
      QComboBox* cbox = qobject_cast<QComboBox*>(ui.tableWidget->cellWidget(i, WizPgMultiPortMap::colPORTINDEX));
      QString funcClassName = port->getFunctionClass();
      MWFunctionClass* funcClass = pcs->getFunctionClass(funcClassName);
      for (int fi=0; fi<funcClass->numFunctions(); fi++)
      {
        int portIndex = -1;
        if (cbox->currentText() != COMMON_ITEM)
          portIndex = cbox->currentText().toInt();

        MWFunction* func = funcClass->getFunc(fi);
        if (portFunction == func->getDisplayName())
        {
          wiz->addPortMapping(portName, func->getName(), portIndex, funcClassName);
          break;
        }
      }
    }
  }
  
  bool isValid = wiz->numPortMaps() > 0;

  return isValid;
}

void WizPgMultiPortMap::readPreviousMappings(const QString& xmlFile)
{
  mPreviousMappings.clear();

  QFile file(xmlFile);

  if (file.open(QFile::ReadOnly | QFile::Text))
  {
    QDomDocument doc;
    int errorLine;
    int errorCol;
    QString errStr;

    QFileInfo templateFi(xmlFile);

    if (doc.setContent(&file, true, &errStr, &errorLine, &errorCol))
    { 
      QDomElement docElem = doc.documentElement();
      QDomNode n = docElem.firstChild();
      while(!n.isNull())
      {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        n = n.nextSibling();
        if (!e.isNull() && !e.isComment())
        {
          qDebug() << e.tagName();
          if ("Ports" == e.tagName())
          {
            QDomNode n = e.firstChild();
            while(!n.isNull())
            {
              QDomElement element = n.toElement(); // try to convert the node to an element.
              n = n.nextSibling();
              if (!element.isNull() && !element.isComment())
              {
                if ("Port" == element.tagName())
                {
                  MWPortMap* pm = new MWPortMap(
                    element.attribute("name"),
                    element.attribute("function"),
                    element.attribute("portNumber").toInt(),
                    element.attribute("functionClass"));
                  mPreviousMappings[element.attribute("name")] = pm;
                }
              }
            }
            break;
          }
        }
      }
    }
  }
}

void WizPgMultiPortMap::on_checkBoxRemember_stateChanged(int)
{
  qDebug() << "remember setting changed" << ui.checkBoxRemember->isChecked();
  if (!ui.checkBoxRemember->isChecked())
  {
    for (int i=0; i<ui.tableWidget->rowCount(); i++)
    {
      QComboBox* funcCombo = qobject_cast<QComboBox*>(ui.tableWidget->cellWidget(i, WizPgMultiPortMap::colFUNCTIONFANCY));
      funcCombo->setCurrentIndex(funcCombo->findText(CHOOSE_ITEM));
    }
  }
}
