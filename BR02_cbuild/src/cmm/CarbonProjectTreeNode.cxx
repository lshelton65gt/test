//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "CarbonProjectTreeNode.h"
#include "CarbonProjectNodes.h"
#include "CarbonHDLSourceFile.h"
#include "CarbonProject.h"
#include "CarbonOptions.h"
#include "CarbonMakerContext.h"
#include "SettingsEditor.h"
#include "DlgConfirmDelete.h" 
#include "MemoryWizard.h"
#include "CarbonFiles.h"
#include "cmm.h" 
#include "CarbonMakerContext.h" 
#include "CarbonProjectWidget.h" 

CarbonDirectivesItem::~CarbonDirectivesItem()
{
  delete mDefaultOptions;
  delete mOptions;
}

CarbonDirectivesItem::CarbonDirectivesItem(CarbonProjectWidget* proj, const char* filePath, QTreeWidgetItem* parent, const char* relPath) : CarbonProjectTreeNode(proj, parent)
{
  mFilePath = filePath;
  mRelPath = relPath;

  QFileInfo fi(mFilePath.c_str());

  QString name = fi.fileName();

  setText(0, name);
  setIcon(0, QIcon(":/cmm/Resources/file-16x16.png"));
  setToolTip(0, mFilePath.c_str());

  mProperties.readPropertyDefinitions(":/cmm/Resources/CarbonDirectivesSource.xml");
  mDefaultOptions = new CarbonOptions(NULL, &mProperties, "CarbonDirectives");
  mOptions = new CarbonOptions(mDefaultOptions, &mProperties, "CarbonDirectives");

  actionDelete = new QAction(QIcon(":/cmm/Resources/DeleteHS.png"), "&Delete", this);
  actionDelete->setStatusTip("Delete this compilation folder");
  connect(actionDelete, SIGNAL(triggered()), this, SLOT(deleteFile()));
}

void CarbonDirectivesItem::deleteFile()
{
  QFileInfo fi(mFilePath.c_str());

  UtString delMsg;
  delMsg << "'" << fi.fileName() << "'";

  DlgConfirmDelete dlg(treeWidget(), delMsg.c_str(), delMsg.c_str());
  DlgConfirmDelete::ConfirmResult result = (DlgConfirmDelete::ConfirmResult)dlg.exec();
  switch (result)
  {
  // Fall through to the Remove after the delete, on purpose
  case DlgConfirmDelete::Delete:
    {
      QFile f(mFilePath.c_str());
      f.remove();
    }
  case DlgConfirmDelete::Remove:
    {
      const QStringList dirValues = mProjectWidget->project()->getToolOptions("VSPCompiler")->getValue("-directive")->getValues();
      QStringList newValues;
      foreach (QString v, dirValues)
      {
        UtString value;
        value << v;
        if (0 != strcmp(value.c_str(), mRelPath.c_str()))
          newValues.append(value.c_str());
      }
    
      mProjectWidget->context()->getSettingsEditor()->setSettings(NULL);
      
      // After the next line, this object will be deleted
      // because there is a registered handler for changes to the -directive options
      CarbonPropertyValue* propVal = mProjectWidget->project()->getToolOptions("VSPCompiler")->getValue("-directive");
      mProjectWidget->project()->getToolOptions("VSPCompiler")->putValue("-directive", propVal->multiValueString(newValues));
      break;
    }
  case DlgConfirmDelete::Cancel:
    break;
  }
}

void CarbonDirectivesItem::singleClicked(int)
{
  QStringList items;
  QVariantList itemValues;

  mOptions->setItems("Directives File Properties", items, itemValues);

  mOptions->putValue("File Path", mFilePath.c_str());

  mProjectWidget->context()->getSettingsEditor()->setSettings(mOptions);
}

void CarbonDirectivesItem::doubleClicked(int)
{
  mProjectWidget->openSource(mFilePath.c_str(), -1, "TextEditor");
}

void CarbonDirectivesItem::showContextMenu(const QPoint& point)
{
  QMenu menu;
  menu.addAction(actionDelete);
  menu.exec(point);
}

CarbonRemodelItem::CarbonRemodelItem(CarbonProjectWidget* proj, const char* filePath, QTreeWidgetItem* parent, const char* relFilePath) : CarbonProjectTreeNode(proj, parent) 
{
  setIcon(0, QIcon(":/cmm/Resources/ModelWizard.png"));
  QFileInfo fi(filePath);
  setText(0, fi.fileName());
  mFilePath = filePath;
  mRelPath = relFilePath;
}

void CarbonRemodelItem::doubleClicked(int)
{
  qDebug() << "open" << mFilePath.c_str();

  MemoryWizard* memwiz = new MemoryWizard(mProjectWidget);
  memwiz->replayFile(mFilePath.c_str());
  memwiz->exec();
}

void CarbonSourceItem::findFile()
{
  CarbonProject* proj = mProjectWidget->project();

  QString rePath = mSource->getRelativePath();

  UtString absPath;
  QFileInfo fi(rePath);
  if (!rePath.contains(ENVVAR_REFERENCE) && fi.isRelative())
    absPath << fi.absoluteFilePath();
  else
    absPath << CarbonProject::fixVarReferences(rePath);

  QString localPath = proj->getWindowsEquivalentPath(absPath.c_str());
  QString fileName = QFileDialog::getOpenFileName(NULL, "Choose RTL File", localPath, "RTL File (*.v *.vhd*)");
  
  if (!fileName.isEmpty())
  {
    UtString filename;
    filename << fileName;
    TempChangeDirectory cd(proj->getProjectDirectory());
    UtString relSrcPath;
    relSrcPath << CarbonProjectWidget::makeRelativePath(proj->getProjectDirectory(), filename.c_str());
    qDebug() << "findFile" << rePath << relSrcPath.c_str();

    QString normalizedPath = theApp->normalizePath(relSrcPath.c_str());
    UtString normPath; normPath << normalizedPath;

    mSource->setRelativePath(normPath.c_str());
    QFileInfo fi(fileName);
    setText(0, fi.fileName());
    updateIcon();
    singleClicked(0);
  }
}

CarbonSourceItem::CarbonSourceItem(CarbonProjectWidget* proj, CarbonHDLSourceFile* src) : CarbonProjectTreeNode(proj) 
{
  mSource=src;

  actionDelete = new QAction(QIcon(":/cmm/Resources/DeleteHS.png"), "&Delete", this);
  actionDelete->setStatusTip("Delete this compilation folder");
  connect(actionDelete, SIGNAL(triggered()), this, SLOT(deleteSource()));

  actionLibraryFile = new QAction("&Library File", this);
  actionLibraryFile->setStatusTip("Library Source");
  actionLibraryFile->setCheckable(true);
  connect(actionLibraryFile, SIGNAL(triggered()), this, SLOT(librarySource()));
  
  actionFindFile = new QAction("&Find File...", this);
  actionFindFile->setStatusTip("Change the filepath that this file is pointing to.");
  connect(actionFindFile, SIGNAL(triggered()), this, SLOT(findFile()));

  if (src->isLibraryFile())
    setIcon(0, QIcon(":/cmm/Resources/libraryFile.png"));
  else
    setIcon(0, QIcon(":/cmm/Resources/file-16x16.png"));
 }

void CarbonSourceItem::updateIcon()
{
  if (mSource->isLibraryFile())
    setIcon(0, QIcon(":/cmm/Resources/libraryFile.png"));
  else
    setIcon(0, QIcon(":/cmm/Resources/file-16x16.png"));
}

CarbonSourceItem::CarbonSourceItem(CarbonProjectWidget* proj, CarbonHDLSourceFile* src, QTreeWidgetItem* parent) : CarbonProjectTreeNode(proj,parent)
{
  mSource=src;
  actionDelete = new QAction(QIcon(":/cmm/Resources/DeleteHS.png"), "&Delete", this);
  actionDelete->setStatusTip("Delete this compilation folder");
  connect(actionDelete, SIGNAL(triggered()), this, SLOT(deleteSource()));

  actionLibraryFile = new QAction("&Library File", this);
  actionLibraryFile->setStatusTip("Library Source");
  actionLibraryFile->setCheckable(true);
  connect(actionLibraryFile, SIGNAL(triggered()), this, SLOT(librarySource()));

  actionFindFile = new QAction("&Find File...", this);
  actionFindFile->setStatusTip("Change the filepath that this file is pointing to.");
  connect(actionFindFile, SIGNAL(triggered()), this, SLOT(findFile()));

  if (src->isLibraryFile())
    setIcon(0, QIcon(":/cmm/Resources/libraryFile.png"));
  else
    setIcon(0, QIcon(":/cmm/Resources/file-16x16.png"));
}

void CarbonSourceItem::showContextMenu(const QPoint& point)
{
  QMenu menu;
  menu.addAction(actionDelete);
  actionLibraryFile->setChecked(mSource->isLibraryFile());
  menu.addAction(actionLibraryFile);
  menu.addAction(actionFindFile);

  menu.setEnabled(!mProjectWidget->isCompilationInProgress());

  menu.exec(point);
}

CarbonDatabaseFolder::CarbonDatabaseFolder(CarbonProjectWidget* proj, QTreeWidgetItem* parent, const char* filePath)
  : CarbonProjectTreeNode(proj,parent) 
{
  setIcon(0, QIcon(":/cmm/Resources/database.png"));
  mFilePath = filePath;
  mProperties = new CarbonProperties();
  mProperties->readPropertyDefinitions(":/cmm/Resources/FileProperties.xml");

  CarbonProperty* prop = mProperties->getProperty("FilePath");
  if (prop)
  {
    prop->putRequired(true);
    prop->putResetTo(CarbonProperty::eResetNone);
  }

  mOptions = new CarbonOptions(NULL, mProperties, "File Properties");

  CQT_CONNECT(proj->project(), configurationChanged(const char*), this, configurationChanged(const char*));
  
  CarbonOptions* options = proj->project()->getToolOptions("VSPCompiler");

  mOptions->putValue("FilePath", mFilePath.c_str());

  options->registerPropertyChanged("-o", "outputFilenameChanged", this);
}

void CarbonDatabaseFolder::configurationChanged(const char*)
{
  QString iodbName = mProjectWidget->project()->getDatabaseFilePath();
  mFilePath.clear();
  mFilePath << iodbName;
  QFileInfo fi(iodbName);
  setText(0, fi.fileName());
}

void CarbonDatabaseFolder::outputFilenameChanged(const CarbonProperty*, const char*)
{
  QString iodbName = mProjectWidget->project()->getDatabaseFilePath();
  mFilePath.clear();
  mFilePath << iodbName;
  QFileInfo fi(iodbName);
  setText(0, fi.fileName());
}

void CarbonDatabaseFolder::showContextMenu(const QPoint& )
{
}

void CarbonDatabaseFolder::singleClicked(int)
{
  QStringList items;
  QVariantList itemValues;

  mOptions->setItems("Database File Properties", items, itemValues);

  mProjectWidget->context()->getSettingsEditor()->setSettings(mOptions);
}

bool CarbonDatabaseFolder::canDropItem(CarbonProjectTreeNode*)
{
  return false;
}

