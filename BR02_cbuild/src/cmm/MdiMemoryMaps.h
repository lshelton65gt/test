#ifndef __MEMORYMAPS_H__
#define __MEMORYMAPS_H__

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "util/UtString.h"
#include "Mdi.h"
#include "MdiProject.h"
#include "CarbonMakerContext.h"

class MDIWidget;

class MDIMemoryMapsTemplate : public MDIDocumentTemplate
{
public:
  MDIMemoryMapsTemplate(CarbonMakerContext* ctx);
 
  virtual MDIWidget* createNewDocument(QWidget* parent=0);
  virtual int createMenus();
  virtual int createToolbars();
  virtual void updateMenusAndToolbars(QWidget* widget);
  virtual MDIWidget* openDocument(QWidget* parent, const char* docName);

private:
  void updateSelectedTab(QWidget* widget);

private:
  CarbonMakerContext* mContext;
};
#endif
