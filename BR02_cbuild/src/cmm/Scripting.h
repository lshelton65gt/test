#ifndef __SCRIPTING_H_
#define __SCRIPTING_H_

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include "cfg/CarbonCfg.h"

#include <QtGui>
#include <QtXml>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptable>
#include <QtScript/QScriptValue>


#define METHOD_DESCR(mn, descr) \
  if (mn == methodName) { return QString(descr); }

#define METHOD_DESCR_ALSO(mn, descr, also) \
  if (formatHtml && mn == methodName) { return QString("%1<p>See Also %2</p>").arg(descr).arg(also); }

#define PROP_DESCR(pn, descr) \
  if (pn == propName) { return QString(descr); }

#define PROP_DESCR_ALSO(pn, descr, also) \
  if (formatHtml && pn == propName) { return QString("%1<p>See Also %2</p>").arg(descr).arg(also); }

class XmlErrorHandler
{
public:
  enum XmlErrorType { Warning, Error };

  XmlErrorHandler()
  {
    reset();
  }

  void reset()
  {
    clear();
  }

  void clear() 
  {
    mErrors = 0;
    mWarnings = 0;
    mMessages.clear();

  }

  void reportProblem(XmlErrorType errType, const QDomElement& e, const QString& msg);
  bool hasErrors() const { return mErrors > 0; }
  bool numWarnings() const { return mWarnings; }
  bool numErrors() const { return mErrors; }
  const QStringList& errorMessages() const { return mMessages; }

private:
  int mWarnings;
  int mErrors;
  QStringList mMessages;
};

class DocumentedQObject : public QObject
{
  Q_OBJECT
  
public:
  DocumentedQObject(QObject* parent=0) : QObject(parent)
  {
  }
  virtual ~DocumentedQObject()
  {
  }

  // Describe method
  virtual QString methodDescr(const QString& /*methodName*/, bool /*formatHtml*/ = true) const { return QString(); }
  virtual QString methodDescription(const QString&) const { return QString(); }
  
  // Describe property
  virtual QString propertyDescr(const QString& /*propName*/, bool /*formatHtml*/ = true) const { return QString(); }

  QString methodLink(const QString& methodName) const;
  QString propertyLink(const QString& propertyLink) const;
  QString enumLink(const QString& enumLink) const;
  QString signalLink(const QString& signalLink) const;

private:
  QString makeLink(const QString& link, const QString& linkType) const;
};

#endif

