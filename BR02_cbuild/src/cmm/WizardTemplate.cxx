//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include "WizardTemplate.h"
#include "cmm.h"
#include "CarbonMakerContext.h"
#include "WorkspaceModelMaker.h"
#include "ErrorInfoWidget.h"
#include "InfoTableWidget.h"

bool WizardTemplate::readTemplate(const QString& templFile)
  {
    XmlErrorHandler eh;
  
    bool status = mContext.getTemplate()->read(&mEngine, templFile, &eh);
    if (status)
    {
      mTemplatePath = templFile;
      QScriptValue global = mEngine.globalObject();
      global.setProperty("template", mEngine.newQObject(mContext.getTemplate()));
      global.setProperty("context", mEngine.newQObject(&mContext));
    }
    else
    {
      QString msg = QString("Error reading template: %1\n%2")
         .arg(templFile)
         .arg(eh.errorMessages().join("\n"));

      QMessageBox::critical(NULL, "Template Error", msg );  

      CarbonMakerContext* appCtx = theApp->getContext();
      QDockWidget* dw = appCtx->getWorkspaceModelMaker()->getErrorInfoDockWindow();
      ErrorInfoWidget* errInfo = dynamic_cast<ErrorInfoWidget*>(dw->widget());
      if (errInfo)
      {
        InfoTableWidget* infoWidget = errInfo->getInfoWidget();
        UtString text;
        text << msg;
        infoWidget->displayOutput(CarbonConsole::Command, text.c_str(), ".");
      }
    }

    return status;
  }
