//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtShellTok.h"

#include <QtGui>

#include "util/XmlParsing.h"

#include "gui/CQt.h"

#include "util/CarbonAssert.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"
#include "util/UtList.h"

#include "CarbonCompilerTool.h"
#include "CarbonPropertyFilelist.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "DlgStringList.h"
#include "CarbonProjectWidget.h"

#define PROP_CARBON_EDIT "CarbonEditBox"
#define PROP_CARBON_FILTER "CarbonFileFilter"
#define PROP_CARBON_FILE "CarbonEditFileName"
#define PROP_CARBON_ITEM "CarbonFileWidget"
#define PROP_CARBON_SWITCHVALUE "CarbonPropertyValue"
#define ASSOCIATED_EDITOR "AssociatedEditor"


// FileList

bool CarbonPropertyFilelist::parseXML(xmlNodePtr parent, UtXmlErrorHandler* /*eh*/)
{
  UtString fileList;
  XmlParsing::getProp(parent, "type", &fileList);
  if (fileList == "InputFile")
    mFileType = InputFile;
  else if (fileList == "OutputFile")
    mFileType = OutputFile;
  else if (fileList == "InputDirectories")
    mFileType = InputDirectories;

  UtString compDir;
  XmlParsing::getProp(parent, "componentDir", &compDir);
  if (compDir.length() > 0)
    mComponentDir = compDir;

  UtString fileFilter;
  XmlParsing::getProp(parent, "filter", &fileFilter);
  mFilter = fileFilter;

  UtString repeatKind;
  XmlParsing::getProp(parent, "repeat", &repeatKind);

  if (repeatKind.length() > 0)
  {
    if (0 == strcmp("OptionNameValue", repeatKind.c_str()))
      mKind = CarbonPropertyFilelist::OptionNameValue;
    else if (0 == strcmp("OptionNameValueCompressed", repeatKind.c_str()))
      mKind = CarbonPropertyFilelist::OptionNameValueCompressed;
    else if (0 == strcmp("ValuesOnly", repeatKind.c_str()))
      mKind = CarbonPropertyFilelist::ValuesOnly;
    else if (0 == strcmp("ValuesOnlyOnePerLine", repeatKind.c_str()))
      mKind = CarbonPropertyFilelist::ValuesOnlyOnePerLine;
  }

  return true;
}

bool CarbonPropertyFilelist::readValueXML(xmlNodePtr parent,  CarbonOptions* options, UtXmlErrorHandler* /*eh*/) const
{
  for (xmlNode *child = parent->children; child != NULL; child = child->next) 
  {
    const char* element = XmlParsing::elementName(child);
    if (element == NULL) // Skip non-interesting XML things
      continue;

    if (0 == strcmp(element, "Value"))
    {
      UtString value;
      XmlParsing::getContent(child, &value);
      options->putValue(this, value.c_str());
    }
  }
  return true;
}

bool CarbonPropertyFilelist::writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* /*eh*/)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Option");
  xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST getName());

  xmlTextWriterStartElement(writer, BAD_CAST "Value");
  xmlTextWriterWriteString(writer, BAD_CAST value->getValue());
  xmlTextWriterEndElement(writer);

  xmlTextWriterEndElement(writer);
  return true;
}

CarbonDelegate* CarbonPropertyFilelist::createDelegate(QObject* parent,  PropertyEditorDelegate* delegate, PropertyEditor* propEditor)
{
  return new CarbonDelegateFilelist(parent, delegate, propEditor);
}

// Delegate

// Editor Delegate
CarbonDelegateFilelist::CarbonDelegateFilelist(QObject *parent, PropertyEditorDelegate* delegate, PropertyEditor* editor)
: CarbonDelegate(parent,delegate,editor)
{
}
void CarbonDelegateFilelist::commitAndCloseEditor()
{
  QLineEdit *widget = qobject_cast<QLineEdit *>(sender());
  if (widget)
    mDelegate->commit(widget);
}

QWidget* CarbonDelegateFilelist::createEditor(QWidget *parent, QTreeWidgetItem* item, CarbonProperty* prop, const QModelIndex& /*index*/) const
{
  bool id,is;
  CarbonPropertyValue* sv = mEditor->getSettings()->getValue(prop,&id,&is);
  QWidget* widget = NULL;
  UtString value = sv->getValue();

  QWidget* w = new QWidget(parent);
  QHBoxLayout* layout = new QHBoxLayout();
  QPushButton* btn = new QPushButton("...");

  connect(btn, SIGNAL(clicked(bool)), this, SLOT(showFilelistDialog()));

  btn->setMaximumWidth(20);
  QLineEdit* edit = new QLineEdit(widget);
  edit->setText(value.c_str());
  edit->setObjectName(ASSOCIATED_EDITOR);

  QVariant qv = qVariantFromValue((void*)edit);
  btn->setProperty(PROP_CARBON_EDIT, qv);

  QVariant qvw = qVariantFromValue((void*)item);
  btn->setProperty(PROP_CARBON_ITEM, qvw);

  QVariant qvs = qVariantFromValue((void*)sv);
  btn->setProperty(PROP_CARBON_SWITCHVALUE, qvs);

  layout->addWidget(edit);
  layout->addWidget(btn);

  layout->setMargin(0);
  layout->setSpacing(0);

  w->setAutoFillBackground(true);
  w->setLayout(layout);

  return w;
}

void CarbonDelegateFilelist::showFilelistDialog()
{
  QPushButton *pb = qobject_cast<QPushButton *>(sender());
  QVariant qsv = pb->property(PROP_CARBON_SWITCHVALUE);
  if (qsv.isValid())
  {
    CarbonPropertyValue* sv = (CarbonPropertyValue*)qsv.value<void*>();
    QVariant qv = pb->property(PROP_CARBON_EDIT);

    const QStringList flist = sv->getValues();
    UtStringArray itemArray;

    for (int j = 0; j < flist.size(); ++j)
    {
      QString filter = flist.at(j);
      UtString f;
      f << filter;
      itemArray.push_back(f);
    }

    CarbonProject* proj = NULL;
    if (mEditor->getContext()->getCarbonProjectWidget())
      proj = mEditor->getContext()->getCarbonProjectWidget()->project();


    const CarbonPropertyFilelist* fileProp = dynamic_cast<const CarbonPropertyFilelist*>(sv->getProperty());
    INFO_ASSERT(fileProp, "Expecting CarbonPropertyFlelist");

    DlgStringList dlg(mEditor, &itemArray, true, proj, fileProp->getComponentDir());
    dlg.setFileKind(fileProp->getFileType());
    dlg.setFilter(fileProp->getFilter());
    dlg.setWindowTitle(sv->getProperty()->getName());

    if (dlg.exec())
    {
      UtString newValues;
      UtStringArray* newItems = dlg.getItemArray();
      UInt32 nItems = newItems->size();

      for (UInt32 i=0; i<nItems; ++i)
      {
        const char* value = (*newItems)[i];
        UtString val;
        val << value;

        const char* v = UtShellTok::quote(value, &val, false, ";");
        newValues << v;
        if (i < nItems-1)
          newValues << ";";
      }

      QVariant qv = pb->property(PROP_CARBON_EDIT);
      if (qv.isValid())
      {
        QLineEdit* edit = (QLineEdit*)qv.value<void*>();
        edit->setText(newValues.c_str());

        // This code causes the newly entered text to be committed
        // closeEditor() and commitData() didn't work
        QVariant qvi = pb->property(PROP_CARBON_ITEM);
        if (qvi.isValid())
        {
          QTreeWidgetItem* item = (QTreeWidgetItem*)qvi.value<void*>();
          mEditor->setCurrentItem(item, 0);
        }
      }
    }
  }
}

void CarbonDelegateFilelist::setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const
{
  bool id,is;

  if (mEditor->getSettings() == NULL)
    return;

  CarbonPropertyValue* sv = mEditor->getSettings()->getValue(prop,&id,&is);
   if (sv == NULL)
      return;

 
  UtString currentValue;
  currentValue << sv->getValue();
 

  QLineEdit* lineEdit = editor->findChild<QLineEdit*>(ASSOCIATED_EDITOR);
  if (lineEdit != NULL)
  {
    UtString value;
    value << lineEdit->text();

    QRegExp re(prop->getLegalPattern());
    if (prop->hasLegalPattern() && !re.exactMatch(value.c_str()))
    {
      QString msg = QString("The switch '%1' value of '%2' does not match the legal pattern: %3, ignoring.")
        .arg(prop->getName())
        .arg(value.c_str())
        .arg(prop->getLegalPattern());
      QMessageBox::warning(NULL, MODELSTUDIO_TITLE, msg);
      return;
    }

    if (prop->getRequired() && value.length() == 0)
    {
      QString msg = QString("The switch '%1' value must not be blank").arg(prop->getName());
      QMessageBox::warning(NULL, MODELSTUDIO_TITLE, msg);
      return;
    }

    setValue(sv, item, model, index, currentValue.c_str(), value.c_str(), prop);
  }
}
