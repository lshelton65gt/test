#ifndef CMM_H
#define CMM_H

#include "util/CarbonPlatform.h"
#include "util/CarbonAbort.h"
#include "util/CarbonAssert.h"
#include "util/SourceLocator.h"

#include <QtGui/QMainWindow>
#include <QFileSystemWatcher>

#include <QProgressBar>
#include "ui_cmm.h"

#include "Mdi.h"

#include "CarbonMakerContext.h"

#include <Qsci/qsciscintilla.h>
#include <Qsci/qscilexer.h>
#include <Qsci/qsciapis.h>
#include <Qsci/qscilexerjavascript.h>

#ifndef MODELSTUDIO_TITLE
#define MODELSTUDIO_TITLE "Model Studio"
#endif

#define TE_LANGUAGE_PREFIX "Language"

class QWebView;

class QTextEditor;
class MDIWindow;
class QWorkspace;
class QSignalMapper;
class ArgProc;
class MDITextEditorTemplate;
class MDIWidget;
class MDIProjectTemplate;
class CarbonProject;
class MDICowareTemplate;
class MDIMaxsimTemplate;
class MDISystemCTemplate;
class CQtWizardContext;
class WorkspaceModelMaker;
class CarbonWorkspace;
class MDIDirectivesTemplate;
class MDIRuntimeTemplate;
class MDIOnDemandTraceTemplate;
class MDIMVTemplate;
class MDIPackageToolTemplate;
class MDITextBrowser;
class MDIMemoryMapsTemplate;
class MDIRegisterEditorTemplate;
class MDIPluginTemplate;
class DlgFindInFiles;
class DlgFindText;
class DlgPreferences;
class JavaScriptAPIs;
class MDIWebBrowser;

class ModelStudioAbort : public CarbonAbortOverride
{
public:
  //! dtor
  virtual ~ModelStudioAbort() {}

  //! User must override this to change the way printing occurs
  virtual void printHandler(const char*);

  //! User must override this to change the way the application exits
  virtual void abortHandler();
};

class cmm : public QMainWindow
{
  Q_OBJECT

public:
  QString getArgumentPath(const QString& arg, bool* foundValue);
  QString normalizePath(const QString& absolutePath);
  QString expandPath(const QString& normalizedPath);
  void removeAllEnvironmentVariables();
  void addEnvVar(const QString& varName);
  void removeVariable(const QString& varName);
  quint32 numEnvVariables() { return mEnvVars.count(); }
  QString getEnvVariable(quint32 index);
  QString lookupVariable(const QString& var);
  bool variablesScope() { return mVariablesProjectScope; }
  void setVariablesScope(bool projectScope) { mVariablesProjectScope=projectScope; }

  void setVSVersion(const QString& txt) { mVSVersion=txt; }
  QString getVSVersion() const { return mVSVersion; }

  void setHomepage(const QString& txt) { mHomePage; }
  QString getHomePage() const { return mHomePage; }

  void setSelectedText(const QString& txt) { mSelectionText=txt; }

  QString getSelectedText() const { return mSelectionText; }

  cmm(CQtWizardContext* cqt=0, QWidget *parent = 0, Qt::WFlags flags = 0);
  ~cmm();
  void shutdown(int exitCode);
  void setSaveProjectOnShutdown(bool newVal) { mSaveProjOnShutdown=newVal; }
  void maybeShowWizard();
  void registerDockWidget(QDockWidget* widget, const char* name, const char* shortCut=NULL);
  void openRecent(const char* fileName);
  bool isSquishMode() { return mSquishMode; }
  void compilationModeChanged(bool inProgress);
  CarbonMakerContext* getContext() { return mContext; }
  bool eventFilter(QObject* obj, QEvent* ev);
  bool isShuttingDown() { return mBeginShutdown; }
  void setShutdownOnDialog(bool newVal)
  {
    if (newVal)
      mShutdownCount++;
    else
      mShutdownCount=0;
    mShutdownOnDialog=newVal;
  }
  void abortApplication(const QString& msg, int exitCode=1);
  void createEmptyProject(const QString& projDir, const QString& projName);
  void signalPreferenceChanged(const QString& prefName, const QVariant& value)
  {
    emit preferenceChanged(prefName, value);
  }
  int getExitCode() const { return mExitCode; }
  void setExitCode(int exitCode) { mExitCode=exitCode; }

  void resetFindInFilesDialog() 
  {
    mFindDialog = NULL;
  }
  void processArguments();
  static bool checkArgument(const QString& arg);
  static bool checkArgument(const char* arg);
  static QString argumentValue(const QString& arg);

  bool isBatchMode() const;
  DlgFindText* getFindDlg() { return mDlgFind; }
  void setFindDlg(DlgFindText* dlg) { mDlgFind=dlg; }
  void signalEditorSettingsChanged(DlgPreferences* prefs)
  {
    emit editorSettingsChanged(prefs);
  }
  QDockWidget* findDockWidget(QString name);
  QString getWorkingDirectory() const { return mWorkingDirectory; }
  const QMap<QString,QString>& getEnvironment() { return mEnvironment; }
  void setEnvironment(const QString& name, const QString& value);

protected:
  virtual void showEvent(QShowEvent* ev);

private:
  Ui::cmmClass ui;
  QString mSelectionText;
  void loadBuiltinPage();
  QTextEditor *findTextEditor(const QString &fileName);
  void writeSettings();
  void readSettings();
  MDIWidget *activeMdiWindow();
  void createMenus();
  void createActions();
  void updateRecentFileActions();
  void updateRecentFile(const char* fileName, bool removeFlag=false);
  void openProjectFile(const char* projectFile);
  void setProjectRemoteParams(const char* remoteWorkingDir, const char* remoteUsername, 
      const char* remoteServer, const char* remoteCarbonHome, const char* remotePrefix);
  void createModelKitProject(const QString& modelKitDir,
    const QString& projName, const QString& fileName);

  QStringList mSwitchesWithArgs;
  QStringList mColonSwitches;
  QStringList mSwitches;

  QMap<QString,QString> mColonValues;
  QMap<QString,QString> mEnvVars;
  QMap<QString,QString> mEnvironment;

private:
  QWorkspace *workspace;
  QSignalMapper *windowMapper;
  bool mShutdownBegun;
  bool mSquishMode;
  bool mShutdownOnDialog;
  bool mSaveProjOnShutdown;
  bool mDebugWindows;
  bool mBeginShutdown;
  bool mWelcomePageLoaded;
  bool mVariablesProjectScope;
  QString mVSVersion;
  QString mHomePage;

protected:
  void closeEvent(QCloseEvent* ev);

  QMenu* modelKitMenu;
  QMenu* windowMenu;
  MDITextEditorTemplate* mTemplateTextEditor;
  MDIDocumentManager* mDocManager;
  MDIProjectTemplate* mTemplateProject;
  MDICowareTemplate* mTemplateCoware;
  MDIMaxsimTemplate* mTemplateMaxsim;
  MDISystemCTemplate* mTemplateSystemC;
  MDIDirectivesTemplate* mTemplateDirectives;
  MDIRuntimeTemplate *mTemplateRuntime;
  MDIOnDemandTraceTemplate *mTemplateOnDemandTrace;
  MDIMVTemplate* mTemplateModelValidation;
  MDIPackageToolTemplate* mTemplatePackageTool;
  MDITextBrowser* mTemplateHtmlBrowser;
  MDIWebBrowser* mTemplateWebBrowser;
  MDIMemoryMapsTemplate* mTemplateMemoryMaps;
  MDIRegisterEditorTemplate* mTemplateRegisters;
  MDIPluginTemplate* mTemplatePlugin;

private slots:
  QTextEditor *createTextEditor();
  void updateMenus();
  void updateWindowMenu();
  void about();
  void windowActivated(QWidget*);
  void on_actionFileOpen_triggered();
  void on_actionNewProject_triggered();
  void on_actionOpenProject_triggered();
  void tabSelected(int index);
  void on_actionExit_triggered();
  void closeTab();
  void on_actionNewFile_triggered();
  void dockVisibilityChanged(bool vis);
  void toggleDockWindow();
  void openRecentFile();
  void showDocs();
  void showDocs2();
  void projectModified(bool value);
  void actionResetWindows();
  void on_actionDriveMappings_triggered();
  void on_actionPreferences_triggered();
  void on_actionFind_in_Files_triggered();
  void showKitWizard();
  void dumpModelKit();
  void apiPreparationFinished();
  void onShutdown();
  void loadFinished(bool);
  void loadProgress(int progress);
  void loadStarted();
  void statusBarMessage(const QString& text);
  void urlChanged( const QUrl & url );


signals:
  void preferenceChanged(const QString& prefName, const QVariant& value);
  void editorSettingsChanged(DlgPreferences*);
  void languageSettingsChanged(const QString& language);

private:
  QWebView* mWebView;
  int mExitCode;
  QMenu *fileMenu;
  QMenu *editMenu;
  QMenu *helpMenu;
  QToolBar *fileToolBar;
  QToolBar *editToolBar;
  QAction *exitAct;
  QAction *closeAct;
  QAction *closeAllAct;
  QAction *tileAct;
  QAction *cascadeAct;
  QAction *nextAct;
  QAction *previousAct;
  QAction *separatorAct;
  QAction *aboutAct;
  QAction *docAct;
  QAction *docAct2;
  QAction* createKitAct;

  QProgressBar* mProgressBar;
  QFileSystemWatcher* mCssWatcher;
  CarbonMakerContext* mContext;
  QAction *windowResetAct;
  
  enum {MaxRecentFiles = 10};
  QAction* recentFileActs[MaxRecentFiles];

  CQtWizardContext* mCQt;
  WorkspaceModelMaker* mWorkspaceModelMaker;
  CarbonWorkspace* mWorkspace;

  SourceLocatorFactory mFactory;
  bool showWizard;

  QMap<QDockWidget*, QAction*> mDockWidgetActionMap;
  QList<QAction*> mDockActions;
  DlgFindInFiles* mFindDialog;
  DlgFindText* mDlgFind;
  JavaScriptAPIs* mAPIs;

  ModelStudioAbort mAbortHandler;

  QString mWorkingDirectory;
  int mShutdownCount;
};

extern cmm* theApp;


#endif // CMM_H
