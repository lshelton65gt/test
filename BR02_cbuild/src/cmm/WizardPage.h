#ifndef __WIZARDPAGE___
#define __WIZARDPAGE___

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>
#include <QtXml>
#include "Scripting.h"

class WizardPage : public QWizardPage
{
  Q_OBJECT

public:
  WizardPage(QWidget *parent = 0) : QWizardPage(parent)
  {
  }

public:
  virtual bool deserialize(const QDomElement&, XmlErrorHandler*)
  {
    return true;
  }
  virtual bool serialize(QDomDocument&, QDomElement&, XmlErrorHandler*)
  {
    return true;
  }

protected:
  QDomElement addElement(QDomDocument& doc, QDomElement& parent, const QString& elementName, const QString& value)
  {
    QDomElement e = doc.createElement(elementName);  
    QDomText t = doc.createTextNode(value);
    e.appendChild(t);
    parent.appendChild(e);
    return e;
  }
};

#endif
