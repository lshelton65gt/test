#ifndef __CARBONSOURCEGROUPS_H__
#define __CARBONSOURCEGROUPS_H__
#include "util/CarbonPlatform.h"

#include "util/UtArray.h"
#include "util/UtString.h"
#include "util/XmlParsing.h"

class CarbonProject;
class CarbonSourceGroups;
class CarbonHDLSourceFile;
class UtXmlErrorHandler;
class CarbonOptions;
class CarbonConfiguration;
class CarbonConfigurations;

#include "CarbonProperties.h"
#include "CarbonOptions.h"


class CarbonSourceGroupTool : public CarbonTool
{
public:
  CARBONMEM_OVERRIDES
    CarbonSourceGroupTool();

};


class CarbonSourceGroup : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString name READ getName WRITE setName)
  Q_PROPERTY(QString libraryName READ getLibraryName)
  Q_PROPERTY(QString libraryPath READ getLibraryPath)
  
private:
  void setName(const QString& name)
  {
    UtString n;
    n << name;
    putName(n.c_str());
  }

public slots:
  bool isVhdlLibrary();
  bool isVerilogLibrary();
  bool isLibrary();

public:
  CarbonSourceGroup(CarbonSourceGroups* parent);

  void putName(const char* newVal) { mName=newVal; }
  const char* getName() const { return mName.c_str(); }
  int getIndex() { return mIndex; }
  void putIndex(int newVal) { mIndex=newVal; }

  void putVhdlLibrary(const char* newVal);
  const char* getVhdlLibrary();

  void putVerilogLibrary(const char* newVal);
  const char* getVerilogLibrary();
  
  const char* getLibraryName();
  const char* getTargetName(CarbonConfiguration*);
  const char* getCommandFilename(CarbonConfiguration*);
  const char* getLibraryPath();
  const char* getTargetDirectory(CarbonConfiguration* activeConfig);
  void putActiveConfig(const char* name);

  CarbonConfigurations* getConfigurations() { return mConfigs; }
  CarbonConfiguration* getActive() { return mActiveConfiguration; }
  CarbonOptions* getOptions(const char* configName=NULL);

  enum GroupType {
    Verilog, VHDL
  };

  void remove(CarbonHDLSourceFile* src, bool deleteItem=true);
  void moveUp(CarbonHDLSourceFile* src);
  void moveDown(CarbonHDLSourceFile* src);

  void putType(GroupType newVal) { mType=newVal; }
  GroupType getType() const { return mType; }

  bool addSource(CarbonHDLSourceFile* src);

  bool serialize(xmlTextWriterPtr writer);
  static bool deserialize(CarbonSourceGroups* groups, xmlNodePtr parent, UtXmlErrorHandler* eh);

  int numSources() const {return mSources.count();}
  CarbonHDLSourceFile* getSource(int i) const 
  {
    INFO_ASSERT(i < mSources.count(), "Index out of range");
    return mSources[i];
  }

  CarbonSourceGroups* getGroups() { return mGroups; }
  CarbonHDLSourceFile* findFile(const char* fileName);


private:
  void addDefaultConfigurations();
  void addConfiguration(const char* name, const char* platform);

private slots:
  void configurationAdded(const char* name);
  void configurationRenamed(const char*, const char*);
  void configurationChanged(const char*);
  void configurationRemoved(const char*);
  void configurationCopy(const char*, const char*);
  void folderNameChanged(const CarbonProperty*, const char* value);

private:
  CarbonSourceGroups* mGroups;
  QList<CarbonHDLSourceFile*> mSources;
  UtString mName;
  GroupType mType;
  UtString mTargetName;
  UtString mLibraryName;
  UtString mCommandName;
  UtString mLibraryPath;
  UtString mTargetDir;
  int mIndex;

  UtString mPendingVHDLLib;
  UtString mPendingVerilogLib;
  
  CarbonConfiguration* mActiveConfiguration;
  CarbonConfigurations* mConfigs;

};

class CarbonSourceGroups : public QObject
{
  Q_OBJECT

public:
  CarbonSourceGroups(CarbonProject* project);
  virtual ~CarbonSourceGroups();

  typedef UtArray<CarbonSourceGroup*> GroupVec;

  bool serialize(xmlTextWriterPtr writer);
  bool deserialize(xmlNodePtr parent, UtXmlErrorHandler* eh);

public slots:
  void addGroup(CarbonSourceGroup* group) 
  {
    mGroups.push_back(group);
    updateIndexes();
  }

  int numGroups() const {return mGroups.count();}
  CarbonSourceGroup* getGroup(int i) const 
  {
    INFO_ASSERT(i < mGroups.count(), "Index out of range");
    return mGroups[i];
  }

  CarbonSourceGroup* findGroupByLibraryName(const char* libName)
  {
    for (int i=0; i<mGroups.count(); i++)
    {
      CarbonSourceGroup* group = getGroup(i);
      if (group->getVhdlLibrary() && 0 == strcmp(group->getVhdlLibrary(), libName))
        return group;
      if (group->getVerilogLibrary() && 0 == strcmp(group->getVerilogLibrary(), libName))
        return group;
    }
    return NULL;
  }

  CarbonOptions* getDefaultOptions() const { return mDefaultOptions; }
  CarbonProperties* getProperties() { return &mProperties; }

  CarbonProject* getProject() const { return mProject; }
  void moveUp(CarbonSourceGroup* group);
  void moveDown(CarbonSourceGroup* group);
  void remove(CarbonSourceGroup* group);

private slots:
  void configurationRenamed(const char*, const char*);
  void configurationCopy(const char*, const char*);
  void configurationRemoved(const char*);
  void configurationAdded(const char*);

private:
  void updateIndexes();

private:
  QList<CarbonSourceGroup*> mGroups;
  CarbonProject* mProject;

  CarbonProperties mProperties;
  CarbonOptions* mDefaultOptions;
};


#endif
