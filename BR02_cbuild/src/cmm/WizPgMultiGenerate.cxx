//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "MemoryWizard.h"
#include "WizPgMultiGenerate.h"
#include "cmm.h"
#include "WorkspaceModelMaker.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"
#include "CarbonOptions.h"
#include "CarbonFiles.h"

WizPgMultiGenerate::WizPgMultiGenerate(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);

  mIsValid = true;
  mConnectedOutput = false;

  setTitle("Generate Options");
  setSubTitle("Choose options to control the generation of the remodeled Module(s).");
}

WizPgMultiGenerate::~WizPgMultiGenerate()
{
}

bool WizPgMultiGenerate::isComplete() const
{
  return mIsValid;
}

// Add a -y but only if it isn't already there in the front
void WizPgMultiGenerate::addCompilerSwitch()
{
  MemoryWizard* wiz = static_cast<MemoryWizard*>(wizard());

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  CarbonOptions* options = proj->getToolOptions("VSPCompiler");
  QString modulesDir = ui.lineEditDirectoryName->text();

  TempChangeDirectory projDir(proj->getProjectDirectory());
  
  UtString relpath;
  relpath << CarbonProjectWidget::makeRelativePath(proj->getProjectDirectory(), modulesDir);
  QString relPath(relpath.c_str());

  bool foundSwitch = false;
  QString dashYValue(options->getValue("-y")->getValue());
  foreach(QString value, dashYValue.split(";"))
  {
    if (value == relPath)
    {
      foundSwitch = true;
      break;
    }
  }

  if (!foundSwitch)
  {
    qDebug() << "adding -y for" << relPath;
    if (dashYValue.isEmpty())
      wiz->setOption("-y", relPath);
    else
      wiz->setOption("-y", QString(relPath + ";" + dashYValue));
  }
  else
    wiz->setOption("-y", "");
}

bool WizPgMultiGenerate::validatePage()
{
  if (validateOptions())
    saveSettings();

  if (mIsValid)
  {
    // Add the switch
    if (ui.radioButtonMultiple->isChecked() && ui.checkBoxAddSwitch->isChecked())
      addCompilerSwitch(); 

    if (!mConnectedOutput)
    {
      CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();

      CQT_CONNECT(pw->getConsole(),
        stdoutChanged(CarbonConsole::CommandType, const QString&, const QString&), 
        this,
        outputChanged(CarbonConsole::CommandType, const QString&, const QString&));

      CQT_CONNECT(pw->getConsole(),
        stderrChanged(CarbonConsole::CommandType, const QString&, const QString&), 
        this,
        outputChanged(CarbonConsole::CommandType, const QString&, const QString&));
      
      mConnectedOutput = true;
    }
  }

  return mIsValid;
}

void WizPgMultiGenerate::outputChanged(CarbonConsole::CommandType, const char* text, const char*)
{
  ui.textBrowser->append(text);
}


bool WizPgMultiGenerate::validateOptions()
{
  mIsValid = true;

  if (ui.radioButtonSingle->isChecked())
  {
    if (ui.lineEditSingleFilename->text().isEmpty())
    {
      QMessageBox::critical(NULL, MODELSTUDIO_TITLE, "Filename must not be empty");
      ui.lineEditSingleFilename->setFocus();
      mIsValid = false;
    }
  }
  else
  {
    bool dirExists = false;
    if (!ui.lineEditDirectoryName->text().isEmpty())
    {
      QDir dir(ui.lineEditDirectoryName->text());
      dirExists = dir.exists();
    }

    if (!dirExists || ui.lineEditDirectoryName->text().isEmpty())
    {
      QMessageBox::critical(NULL, MODELSTUDIO_TITLE, "Directory name must not be empty and must exist");
      ui.lineEditDirectoryName->setFocus();
      mIsValid = false;
    }

    UtString unixdir;
    unixdir << ui.lineEditDirectoryName->text();
    CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
    CarbonProject* proj = pw->project();

    if (mIsValid && !proj->isMappedUnixPath(unixdir.c_str()))
    {
      QMessageBox::critical(NULL, MODELSTUDIO_TITLE, "Directory must reside on a mapped drive.");
      ui.lineEditDirectoryName->setFocus();
      mIsValid = false;
    }
  }

  emit completeChanged();

  return mIsValid;
}

void WizPgMultiGenerate::initializePage()
{
  ui.textBrowser->clear();
  restoreSettings();
  updateButtons();
}

void WizPgMultiGenerate::updateButtons()
{
  bool singleFile = ui.radioButtonSingle->isChecked();

  ui.lineEditSingleFilename->setEnabled(singleFile);
  ui.pushButtonBrowseFilename->setEnabled(singleFile);

  ui.lineEditDirectoryName->setEnabled(!singleFile);
  ui.pushButtonBrowseDirname->setEnabled(!singleFile);
  ui.checkBoxAddSwitch->setEnabled(!singleFile);
}

void WizPgMultiGenerate::on_radioButtonSingle_toggled(bool)
{
  updateButtons();
}

void WizPgMultiGenerate::on_radioButtonMultiple_toggled(bool)
{
  updateButtons();
}

void WizPgMultiGenerate::on_pushButtonBrowseFilename_clicked()
{

  QString currValue = ui.lineEditSingleFilename->text();
  if (currValue.isEmpty())
  {
    CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
    CarbonProject* proj = pw->project();
    currValue = proj->getProjectDirectory();
  }

  QString fileName = QFileDialog::getSaveFileName(this, tr("Output File"),
                                currValue,
                                tr("Verilog File (*.v)"));
  if (!fileName.isEmpty())
  {
    ui.lineEditSingleFilename->setText(fileName);
    validateOptions();
  }
}

void WizPgMultiGenerate::on_pushButtonBrowseDirname_clicked()
{
  QString currValue = ui.lineEditDirectoryName->text();
  if (currValue.isEmpty())
  {
    CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
    CarbonProject* proj = pw->project();
    currValue = proj->getProjectDirectory();
  }

  QString directory = QFileDialog::getExistingDirectory(this,
                       tr("Select Output Directory"),
                       currValue,
                       QFileDialog::DontResolveSymlinks | QFileDialog::ShowDirsOnly);
  if (!directory.isEmpty())
  {
    ui.lineEditDirectoryName->setText(directory);
    validateOptions();
  }
}

#define SETTING_SINGLE_FILE "wizPgMultiGenerate/singleFile"
#define SETTING_SINGLE_FILENAME "wizPgMultiGenerate/singleFileName"
#define SETTING_MULTI_DIRNAME "wizPgMultiGenerate/directoryName"
#define SETTING_PREFIX "wizPgMultiGenerate/prefix"
#define SETTING_SUFFIX "wizPgMultiGenerate/suffix"

void WizPgMultiGenerate::on_lineEditSingleFilename_textChanged(const QString&)
{
  mIsValid = true;
  emit completeChanged();
}

void WizPgMultiGenerate::saveSettings()
{
  mSettings.setValue(SETTING_SINGLE_FILE, ui.radioButtonSingle->isChecked());
  mSettings.setValue(SETTING_SINGLE_FILENAME, ui.lineEditSingleFilename->text());
  mSettings.setValue(SETTING_MULTI_DIRNAME, ui.lineEditDirectoryName->text());
  mSettings.setValue(SETTING_PREFIX, ui.lineEditPrefix->text());
  mSettings.setValue(SETTING_SUFFIX, ui.lineEditSuffix->text());

  MemoryWizard* wiz = static_cast<MemoryWizard*>(wizard());

  if (ui.radioButtonSingle->isChecked())
    wiz->setOption("generate", "singleFile");
  else
    wiz->setOption("generate", "directory");

  wiz->setOption("directory", ui.lineEditDirectoryName->text());
  wiz->setOption("modulefile", ui.lineEditSingleFilename->text());
  wiz->setOption("prefix", ui.lineEditPrefix->text());
  wiz->setOption("suffix", ui.lineEditSuffix->text());
}


void WizPgMultiGenerate::restoreSettings()
{
  bool singleFile = mSettings.value(SETTING_SINGLE_FILE, true).toBool();
  ui.radioButtonSingle->setChecked(singleFile);
  ui.radioButtonMultiple->setChecked(!singleFile);

  ui.lineEditPrefix->setText(mSettings.value(SETTING_PREFIX, "").toString());
  ui.lineEditSuffix->setText(mSettings.value(SETTING_SUFFIX, "").toString());

  ui.lineEditDirectoryName->setText(mSettings.value(SETTING_MULTI_DIRNAME, "").toString());
  ui.lineEditSingleFilename->setText(mSettings.value(SETTING_SINGLE_FILENAME, "").toString());
}
