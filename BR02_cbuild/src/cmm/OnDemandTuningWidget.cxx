//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "gui/CQt.h"
#include "gui/CDragDropTreeWidget.h"
#include "OnDemandTuningWidget.h"
#include "CarbonMakerContext.h"
#include "CarbonOptions.h"
#include "CarbonInstanceOptions.h"
#include "CarbonModelsWidget.h"
#include "CarbonProjectWidget.h"
#include "DirectivesEditor.h"
#include "CarbonDatabaseContext.h"
#include "CarbonRuntimeState.h"
#include "TabNetDirectives.h"
#include <QGroupBox>
#include <QSlider>

#define XML_ENCODING "ISO-8859-1"

#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"

#define ZOOM_SLIDER_MAX 1000 // arbitrary

OnDemandTuningWidget::OnDemandTuningWidget(QWidget* parent, CarbonMakerContext* ctx)
  : QWidget(parent),
    mCarbonMakerContext(ctx)
{
  mGraph = new OnDemandTuningGraph(this, ctx);
  mViolators = new CDragDropTreeWidget(false, this);
  mExcused = new CDragDropTreeWidget(true, this);
  mAddExcuse = new QPushButton("Add Excuse");
  mDeleteExcuse = new QPushButton("Delete Excuse");
  mApplyToComponent = new QPushButton("Apply To Component");
  mMakePermanentInModel = new QPushButton("Make Permanent In Model");
  mSelectLeft = new QPushButton(QIcon(":/cmm/Resources/GoRtlHS.png"), "");
  mSelectRight = new QPushButton(QIcon(":/cmm/Resources/GoLtrHS.png"), "");

  mVZoom = new QSlider(Qt::Vertical);
  mVZoom->setMinimum(0);
  mVZoom->setMaximum(ZOOM_SLIDER_MAX);
  mVZoom->setPageStep(ZOOM_SLIDER_MAX / 10);
  mVZoom->setValue(0);

  mViolators->setColumnCount(2);
  mViolators->setAlternatingRowColors(true);
  CQT_CONNECT(mViolators, selectionChanged(), this, selectViolator());
  QStringList headers;
  headers << "RTL Name";
  headers << "Violation";
  mViolators->setHeaderLabels(headers);

  QWidget* violators_box = new QGroupBox("Selected OnDemand Violations");
  violators_box->setLayout(CQt::hbox(mViolators));

  headers.clear();
  headers << "RTL Name";
  headers << "Excused Violations";
  mExcused->setHeaderLabels(headers);
  CQT_CONNECT(mExcused, selectionChanged(), this, selectExcused());
  CQT_CONNECT(mExcused, dropReceived(const char*,QTreeWidgetItem*),
              this, dropViolator(const char*,QTreeWidgetItem*));

  QWidget* excused_box = new QGroupBox("Excused Violations");
  excused_box->setLayout(CQt::hbox(mExcused));

  QLayout* layout = CQt::vbox();
  layout->setSpacing(2); layout->setMargin(0);
  QLayout* slider_graph = CQt::hbox();
  slider_graph->setSpacing(2); slider_graph->setMargin(0);
  QLayout* bottom = CQt::hbox();
  bottom->setSpacing(2); bottom->setMargin(0);
  QLayout* buttons = CQt::hbox();
  buttons->setSpacing(2); buttons->setMargin(0);
  *bottom << violators_box << excused_box;
  *buttons << mSelectLeft << mSelectRight
           << mAddExcuse << mDeleteExcuse
           << mApplyToComponent << mMakePermanentInModel;
  *slider_graph << mVZoom << mGraph;
  *layout << slider_graph << bottom << buttons;

  CQT_CONNECT(mGraph, quantumSelect(UInt32), this, quantumSelect(UInt32));
  CQT_CONNECT(mGraph, quantumUnselect(), this, quantumUnselect());
  CQT_CONNECT(mAddExcuse, clicked(), this, addExcuse());
  CQT_CONNECT(mDeleteExcuse, clicked(), this, deleteExcuse());
  CQT_CONNECT(mApplyToComponent, clicked(), this, applyToComponent());
  CQT_CONNECT(mMakePermanentInModel, clicked(), this, makePermanentInModel());
  CQT_CONNECT(mSelectLeft, clicked(), this, selectLeft());
  CQT_CONNECT(mSelectRight, clicked(), this, selectRight());
  CQT_CONNECT(mVZoom, valueChanged(int), this, sliderMoved(int));
  CQT_CONNECT(mGraph, scaleChanged(), this, scaleChanged());

  setLayout(layout);
  mQuantum = NULL;
  mChangedSinceApply = false;
  mChangedSinceMakePermanent = false;
  mAddExcuse->setEnabled(false);
  mDeleteExcuse->setEnabled(false);
  mApplyToComponent->setEnabled(false);
  mMakePermanentInModel->setEnabled(false);
  mMakePermanentInModel->setToolTip("No Project loaded or this component does not belong to the current project.");

  mUpdatesRegistered = false;
  registerForSystemUpdates();
}

OnDemandTuningWidget::~OnDemandTuningWidget() {
}

void OnDemandTuningWidget::scaleChanged() {
  // Qt seems to have some strange bug with enabling/disabling this QSlider
  // widget.  If I disable it then it causes weird MDI behavior.  This also
  // seems to occur if I show or hide that widget from this context.  I
  // don't know why.  So for now I'm just going to leave that widget visible
  // and movable at all times.  Annoying, I know.
#define QT_MDI_SLIDER_BUG 1
#if !QT_MDI_SLIDER_BUG
  bool ena = mGraph->getMaxViolations() > MIN_VIOLATIONS_TO_SHOW;

# define TRY_SHOW_HIDE 1
# if TRY_SHOW_HIDE 1
  // here's the code that tries to show/hide the zoom-slider which disturbs MDI
  if (ena) {
    mVZoom->show();
  }
  else {
    mVZoom->hide();
  }
# else
  // here's the code that enables/disables the zoom-slider which also disturbs
  // mdi
  mVZoom->setEnabled(ena);
# endif
#endif
} // void OnDemandTuningWidget::scaleChanged

void OnDemandTuningWidget::selectLeft() {
  mGraph->selectQuantum(mGraph->getSelectionIndex() - 1);
}  

void OnDemandTuningWidget::selectRight() {
  mGraph->selectQuantum(mGraph->getSelectionIndex() + 1);
}  

void OnDemandTuningWidget::sliderMoved(int value) {
  // a value of 0 means we want to see the entire spectrum of violations.
  // a value of ZOOM_SLIDER_MAX means we want the top of the graph to
  // show about 5 violations.
  double ymax_at_0 = std::max(mGraph->getMaxViolations(), UInt32(MIN_VIOLATIONS_TO_SHOW));
  double ymax_at_1000 = MIN_VIOLATIONS_TO_SHOW;
  double ratio = (double) value / (double) ZOOM_SLIDER_MAX;
  double ymax = ymax_at_1000 * ratio + ymax_at_0 * (1.0 - ratio);
  mGraph->putMagnification(ymax / ymax_at_0);
}

struct ODCompareNodes {
  bool operator()(const OnDemandTuningGraph::Node* a,
                  const OnDemandTuningGraph::Node* b)
    const
  {
    return strcmp(a->getName(), b->getName()) < 0;
  }
};

void OnDemandTuningWidget::putComponent(const char* compName)
{
  mComponent=compName; 
  updateMakePermanentButton();

  // We know there is a valid component in the system, so populate its excuses from
  // its properties
  populateExcusesMap();
  // Also ensure we get updated when the system changes.  It's
  // possible that the models widget didn't exist when we were
  // constructed, in which case this won't have been done yet.
  registerForSystemUpdates();
}

void OnDemandTuningWidget::setViolators(OnDemandTuningGraph::Quantum* quantum)
{
  quantumUnselect();
  mItemNodeMap.clear();
  mQuantum = quantum;
  if (quantum != NULL) {
    OnDemandTuningGraph::NodeArray nodes;
    mQuantum->getNodes(&nodes);
    std::sort(nodes.begin(), nodes.end(), ODCompareNodes());
    for (UInt32 i = 0; i < nodes.size(); ++i) {
      OnDemandTuningGraph::Node* node = nodes[i];
      QTreeWidgetItem* item = new QTreeWidgetItem(mViolators);
      mItemNodeMap.map(item, node);
      item->setText(0, node->getName());

      // If this item has already been excused, then disable it
      if (mNodeExcusesMap.find(node->getName()) != mNodeExcusesMap.end()) {
        item->setFlags(0);
      }
      // figure out what the violations for this net are within the quantum
      OnDemandTuningGraph::EventArray events;
      mQuantum->getNodeViolations(node, &events);

      UtString itemText, itemTip;
      UInt32 event_mask = 0;
      for (UInt32 j = 0; j < events.size(); ++j) {
        OnDemandTuningGraph::Event* event = events[j];
        UInt32 mask = 1 << event->getType();
        if ((event_mask & mask) == 0) {
          if (event_mask != 0) {
            // We've already seen an event, so add punctuation to the
            // existing strings.
            itemText << "; ";
            itemTip << "; ";
          }
          event_mask |= mask;
          UInt32 length = event->getLength();
          switch (event->getType()) {
          case eCarbonOnDemandDebugNonIdleNet:
            itemText << "ND";
            itemTip << "This signal is not allowed to be deposited while idle";
            break;
          case eCarbonOnDemandDebugNonIdleChange:
            itemText << "NI";
            itemTip << "This signal was accessed in a way that is not allowed while idle";
            break;
          case eCarbonOnDemandDebugRestore:
            itemText << "R";
            itemTip << "This signal was accessed in a way that required the model state to be restored";
            break;
          case eCarbonOnDemandDebugDiverge:
            itemText << "D";
            itemTip << "The stimulus applied to this signal diverged from the idle pattern";
            break;
          case eCarbonOnDemandDebugFail:
            if (length == 0) {
              itemText << "NR";
              itemTip << "The values of this signal did not repeat";
            } else {
              itemText << "PR (" << length << ")";
              itemTip << "The values of this signal repeated partially, with the period shown";
            }
            break;
          }
        }
      } // for
      mViolators->resizeColumnToContents(0);
      if (!itemText.empty()) {
        item->setText(1, itemText.c_str());
      }
      if (!itemTip.empty()) {
        // Add the tooltip to both columns
        item->setToolTip(0, itemTip.c_str());
        item->setToolTip(1, itemTip.c_str());
      }
    } // for
  } // if
} // void OnDemandTuningWidget::setViolators

bool OnDemandTuningWidget::loadTraceFile(const char* fname, UtString* errmsg) {
  return mGraph->loadTraceFile(fname, errmsg);
}

void OnDemandTuningWidget::quantumSelect(UInt32 quantum_index) {
  mQuantum = mGraph->getQuantum(quantum_index);
  setViolators(mQuantum);
  mSelectLeft->setEnabled(quantum_index > 0);
  mSelectRight->setEnabled(((SInt32) quantum_index) < mGraph->numQuanta() - 1);
} // void OnDemandTuningWidget::quantumSelect

void OnDemandTuningWidget::quantumUnselect() {
  mViolators->clear();
  mItemNodeMap.clear();
  mSelectLeft->setEnabled(false);
  mSelectRight->setEnabled(false);
}

void OnDemandTuningWidget::selectViolator() {
  QTreeItemList sel = mViolators->selectedItems();
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* item = *p;
    OnDemandTuningGraph::Node* node = mItemNodeMap.find(item);
    if (mNodeExcusesMap.find(node->getName()) != mNodeExcusesMap.end()) {
      mViolators->putDragData(NULL);
    }
    else {
      mViolators->putDragData(node->getName());
    }
  }
  mAddExcuse->setEnabled(! sel.empty());
}

void OnDemandTuningWidget::selectExcused() {
  QTreeItemList sel = mExcused->selectedItems();
  mDeleteExcuse->setEnabled(!sel.empty());
}

void OnDemandTuningWidget::addExcuse() {
  QTreeItemList sel = mViolators->selectedItems();
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* item = *p;
    OnDemandTuningGraph::Node* node = mItemNodeMap.find(item);
    dropViolator(node->getName(), item);
  }
}

void OnDemandTuningWidget::dropViolator(const char* violator,
                                        QTreeWidgetItem*)
{
  // When we drop a violator onto the 'excused' widget, we are saying
  // than any onDemand violations contributed by the violator in
  // this Quantum are 'excusable', and should be masked off by directives

  // First of all, look up the Node for the violator
  OnDemandTuningGraph::Node* node = mGraph->findNode(violator);
  OnDemandTuningGraph::EventArray events;
  if ((node == NULL) || (mQuantum == NULL) ||
      !mQuantum->getNodeViolations(node, &events))
  {
    CQtContext* cqt = mCarbonMakerContext->getQtContext();
    cqt->warning(this, "No such node");
  }
  else {
    bool changed = false;
    for (UInt32 i = 0; i < events.size(); ++i) {
      OnDemandTuningGraph::Event* event = events[i];
      switch (event->getType()) {
      case eCarbonOnDemandDebugNonIdleChange:
        // there is no valid excluse, except possibly increasing the
        // value of -onDemandMemoryStateLimit
        break;
      case eCarbonOnDemandDebugNonIdleNet:
        changed |= addExcuse(violator, eOnDemandIdleDeposit);
        break;
      case eCarbonOnDemandDebugRestore:
        break;
      case eCarbonOnDemandDebugDiverge:
        break;
      case eCarbonOnDemandDebugFail:
        changed |= addExcuse(violator, eOnDemandExcluded);
        break;
      }
    }
    if (changed) {
      populateExcusedWidget();
      mChangedSinceApply = true;
      mChangedSinceMakePermanent = true;
      if (mCarbonMakerContext->getCarbonModelsWidget() != NULL) {
        mApplyToComponent->setEnabled(true);
      }
      updateMakePermanentButton();

      // When a violator item has been successfully excused, then it should
      // be disabled in the violators list.
      if (mItemNodeMap.test(node)) {
        QTreeWidgetItem* violator_item = mItemNodeMap.find(node);
        violator_item->setFlags(0);
      }
    }
  }
} // void OnDemandTuningWidget::dropViolator

bool OnDemandTuningWidget::addExcuse(const char* violator, Excuse excuse) {
  NodeExcusesMap::iterator p = mNodeExcusesMap.find(violator);
  if (p == mNodeExcusesMap.end()) {
    mNodeExcusesMap[violator] = excuse;
    return true;
  }
  Excuse& excuses = p->second;
  if ((excuses & excuse) == 0) {
    excuses = Excuse(excuses | excuses);
    return true;
  }
  return false;
}

void OnDemandTuningWidget::deleteExcuse() {
  QTreeItemList sel = mExcused->selectedItems();
  for (QTreeItemList::iterator p = sel.begin(), e = sel.end(); p != e; ++p) {
    QTreeWidgetItem* item = *p;
    int idx = mExcused->indexOfTopLevelItem(item);
    if (idx >= 0) {
      UtString node_name;
      node_name << item->text(0);
      NodeExcusesMap::iterator p = mNodeExcusesMap.find(node_name);
      if (p != mNodeExcusesMap.end()) {
        mNodeExcusesMap.erase(p);
      }
      mExcused->takeTopLevelItem(idx);
      delete item;
      mChangedSinceMakePermanent = true;
      mChangedSinceApply = true;
      if (mCarbonMakerContext->getCarbonModelsWidget() != NULL) {
        mApplyToComponent->setEnabled(true);
      }
      updateMakePermanentButton();

      // back in the violators list, enable the item in the violators
      // list, if present.  Note that the item might not be in the violators
      // list if it's from a different quantum than the one selected, so
      // test first.
      OnDemandTuningGraph::Node* node = mGraph->findNode(node_name.c_str());
      if ((node != NULL) && mItemNodeMap.test(node)) {
        QTreeWidgetItem* violator_item = mItemNodeMap.find(node);
        violator_item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
      }
    }
  }
} // void OnDemandTuningWidget::deleteExcuse

void OnDemandTuningWidget::collectValues(UtString* excludedStates, UtString* excludedInputs, QStringList* statesList, QStringList* inputsList)
{
  if (excludedStates)
    excludedStates->clear();
  
  if (excludedInputs)
    excludedInputs->clear();

  if (inputsList)
    inputsList->clear();
  if (statesList)
    statesList->clear();

  UtString excluded_state, excluded_inputs;

  for (NodeExcusesMap::SortedLoop p(mNodeExcusesMap.loopSorted());
       !p.atEnd(); ++p)
  {
    const UtString& node_name = p.getKey();
    Excuse excuse = p.getValue();
    if (excuse != eOnDemandIdleDeposit) {
      if (!excluded_state.empty()) {
        excluded_state << ";";
      }
      excluded_state << node_name;
      if (statesList)
        statesList->append(node_name.c_str());
    }
    if (excuse != eOnDemandExcluded) {
      if (!excluded_inputs.empty()) {
        excluded_inputs << ";";
      }
      excluded_inputs << node_name;
      if (inputsList)
        inputsList->append(node_name.c_str());
    }
  }
  if (excludedStates)
    *excludedStates = excluded_state;

  if (excludedInputs)
    *excludedInputs = excluded_inputs;
}
void OnDemandTuningWidget::applyToComponent() {
  mApplyToComponent->setEnabled(false);

  CarbonModelsWidget* cmw = mCarbonMakerContext->getCarbonModelsWidget();
  if (cmw != NULL) {
    // Select the component first.  It's possible that the selection
    // has been changed since the tuning widget was opened.
    bool selected = cmw->selectComponent(mComponent.c_str());
    CarbonOptions* options = cmw->getCurrentOptions();
    if (selected && (options != NULL)) {
      UtString excluded_state, excluded_inputs;
      collectValues(&excluded_state, &excluded_inputs);
      // TBD: CALL NEW API PROVIDED BY MARKK
      options->putValue(ODM_PROP_EXCLUDED_STATE_SIGNALS, excluded_state.c_str());
      options->putValue(ODM_PROP_EXCLUDED_INPUT_SIGNALS, excluded_inputs.c_str());
    } // if
    // Set this last.  Changing the component selection above triggers
    // a system update of all components, and we don't want our
    // changes to be overwritten.
    mChangedSinceApply = false;
  } // if
  emit changeExcused();
} // void OnDemandTuningWidget::applyToComponent

bool OnDemandTuningWidget::isComponentFromProject()
{
  bool belongsToProject = false;
  // Do we have a project? We could have been launched w/o a project begin loaded/open
  CarbonProjectWidget* projWidget = mCarbonMakerContext->getCarbonProjectWidget();
  CarbonProject* project = NULL;
  if (projWidget != NULL) {
    project = projWidget->project();
  }
  if (project != NULL)
  {
    CarbonDatabaseContext* dbContext = project->getDbContext();
    CarbonModelsWidget* cmw = mCarbonMakerContext->getCarbonModelsWidget();
    // Ensure we have a valid database.  We also need a models widget.
    // If the user opens the .cot file directly, there won't be one.
    // Currently, the only way to propagate exclusions from the tuning
    // widget back to the project is through the options contained in
    // the models widget.
    if ((cmw != NULL) && dbContext->getDB())
    {
      UtString idString;
      idString << carbonDBGetIdString(dbContext->getDB());

      CarbonRuntimeState* rs = cmw->getRuntimeState();
      CarbonReplaySystem* carbonSystem = rs->getCarbonSystem();
      qDebug() << "DB ID String: " << idString.c_str();
      CarbonSystemComponent* thisTraceComponent = carbonSystem->findComponent(mComponent.c_str());
      INFO_ASSERT(thisTraceComponent, "Expecting to find component for this trace");
      belongsToProject = (0 == strcmp(thisTraceComponent->getID(), idString.c_str())) ? true : false;
    }
  }
  return belongsToProject;
}

void OnDemandTuningWidget::registerForSystemUpdates() {
  // We need to know when component settings have been changed, so
  // that we can update the excused list.  Ensure we haven't already
  // registered, because we don't want to be called twice.
  if (!mUpdatesRegistered) {
    CarbonModelsWidget *cmw = mCarbonMakerContext->getCarbonModelsWidget();
    if (cmw != NULL) {
      CQT_CONNECT(cmw->getRuntimeState(), carbonSystemUpdated(CarbonRuntimeState&,CarbonReplaySystem&,CarbonReplaySystem&),
                  this, carbonSystemUpdated(CarbonRuntimeState&,CarbonReplaySystem&,CarbonReplaySystem&));
      mUpdatesRegistered = true;
    }
  }
}

void OnDemandTuningWidget::updateMakePermanentButton()
{
  bool enableButton = isComponentFromProject();
  mMakePermanentInModel->setEnabled(enableButton);
  if (enableButton)
    mMakePermanentInModel->setToolTip("Permanently save changes as Directives");
  else
    mMakePermanentInModel->setToolTip("No Project loaded or this component does not belong to the current project.");
}

void OnDemandTuningWidget::makePermanentInModel() {
  // This can only be done with the appropriate directive.  TBD.
  mChangedSinceMakePermanent = false;
  mMakePermanentInModel->setEnabled(false);
  QStringList stateList;
  QStringList inputList;

  collectValues(NULL, NULL, &stateList, &inputList);

  CarbonProjectWidget* projWidget = mCarbonMakerContext->getCarbonProjectWidget();
  if (projWidget) // we opened with a project
  {
    if (isComponentFromProject())
    {
      DirectivesEditor* directivesEditor = projWidget->compilerDirectives();
      INFO_ASSERT(directivesEditor, "Expecting a Directives Editor Widget");

      TabNetDirectives* netDirectives = directivesEditor->getNetDirectivesTab();
      INFO_ASSERT(netDirectives, "Expecting Net Directives Tab");

      if (stateList.count() > 0)
        netDirectives->setDirective(stateList, "onDemandExcluded", true);
   
      if (inputList.count() > 0)
        netDirectives->setDirective(inputList, "onDemandIdleDeposit", true);

      // Remove the runtime exclusions, since they're no longer
      // needed.  This is delegated to the models widget because this
      // should apply to all components that came from this project.
      CarbonModelsWidget *cmw = mCarbonMakerContext->getCarbonModelsWidget();
      if (cmw != NULL) {
        cmw->removeOnDemandExclusions(stateList, inputList);
        CarbonOptions* options = cmw->getOptionsForComponent(mComponent.c_str());
        INFO_ASSERT(options, "Expecting CarbonOptions for Component");

        saveAttributes(options);
      }
    }
  }
}

 

// Persist the transient attributes to the .attributes file
void OnDemandTuningWidget::writeAttribute(CarbonOptions* options, xmlTextWriterPtr writer, const char* optionName, const char* attName)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Attribute");
  xmlTextWriterWriteAttribute(writer, BAD_CAST "name", BAD_CAST attName);
  CarbonPropertyValue* propVal = options->getValue(optionName);
  INFO_ASSERT(propVal, "Expecting to find property");

  switch (propVal->getProperty()->getKind())
  {
  default:
  case CarbonProperty::String:
    xmlTextWriterWriteAttribute(writer, BAD_CAST "type", BAD_CAST "string");   
    break;
  case CarbonProperty::Integer:
    xmlTextWriterWriteAttribute(writer, BAD_CAST "type", BAD_CAST "integer");   
    break;
  }
  xmlTextWriterWriteString(writer, BAD_CAST propVal->getValue());
  xmlTextWriterEndElement(writer);

}

void OnDemandTuningWidget::saveAttributes(CarbonOptions* options)
{
  CarbonProjectWidget* projWidget = mCarbonMakerContext->getCarbonProjectWidget();

  qDebug() << "Saving attributes for this project";

  xmlDocPtr doc=NULL;
  xmlTextWriterPtr writer = xmlNewTextWriterDoc(&doc, 0);
  xmlTextWriterSetIndent(writer, 1);

  xmlTextWriterStartElement(writer, BAD_CAST "DatabaseAttributes");
  xmlTextWriterWriteAttribute(writer, BAD_CAST "version", BAD_CAST "1");
  
  writeAttribute(options, writer, ODM_PROP_MAX_STATES, ODM_ATTR_MAX_STATES);
  writeAttribute(options, writer, ODM_PROP_BACKOFF_STRATEGY, ODM_ATTR_BACKOFF_STRATEGY);
  writeAttribute(options, writer, ODM_PROP_BACKOFF_STATES, ODM_ATTR_BACKOFF_STATES);
  writeAttribute(options, writer, ODM_PROP_BACKOFF_DECAY_PCT, ODM_ATTR_BACKOFF_DECAY_PCT);
  writeAttribute(options, writer, ODM_PROP_BACKOFF_MAX_DECAY, ODM_ATTR_BACKOFF_MAX_DECAY);

  xmlTextWriterEndElement(writer);
  xmlTextWriterEndDocument(writer);
  xmlFreeTextWriter(writer);

  UtString outputName;
  UtString attFileName;
  attFileName << projWidget->project()->getActiveConfiguration() << ".attributes";

  CarbonConfiguration* cfg = projWidget->project()->getActive();
  OSConstructFilePath(&outputName, cfg->getOutputDirectory(), attFileName.c_str());

  QFileInfo fi(outputName.c_str());

  xmlSaveFileEnc(outputName.c_str(), doc, XML_ENCODING);
  
  CarbonOptions* compilerOptions = projWidget->project()->getActive()->getOptions("VSPCompiler");
  UtString filePath;
  filePath << "../" << fi.fileName();
  compilerOptions->putValue("-attributeFile", filePath.c_str());

  xmlFreeDoc(doc);
}


void OnDemandTuningWidget::carbonSystemUpdated(CarbonRuntimeState &/* state */, CarbonReplaySystem &/* guiSystem */, CarbonReplaySystem &/* simSystem */)
{
  // Update the excuses map with the current values from the component properties, but only if we haven't made any changes locally!
  if (!mChangedSinceApply) {
    populateExcusesMap();
  }
}

void OnDemandTuningWidget::populateExcusesMap()
{
  // Repopulate our node->excuses map with the current set of
  // exclusions from the component options
  mNodeExcusesMap.clear();
  CarbonModelsWidget* cmw = mCarbonMakerContext->getCarbonModelsWidget();
  // Our component might not be selected, so look up the options by name
  CarbonOptions* options = cmw->getOptionsForComponent(mComponent.c_str());
  if (options != NULL) {
    CarbonPropertyValue *propValue;

    propValue = options->getValue(ODM_PROP_EXCLUDED_STATE_SIGNALS);
    if (propValue != NULL) {
      const QStringList &valList = propValue->getValues();
      foreach (QString val, valList) {
        UtString value;
        value << val;
        addExcuse(value.c_str(), eOnDemandExcluded);
      }
    }

    propValue = options->getValue(ODM_PROP_EXCLUDED_INPUT_SIGNALS);
    if (propValue != NULL) {
      const QStringList &valList = propValue->getValues();
      foreach (QString val, valList) {
        UtString value;
        value << val;
        addExcuse(value.c_str(), eOnDemandIdleDeposit);
      }
    }
  }

  // Update the widget
  populateExcusedWidget();
}

void OnDemandTuningWidget::populateExcusedWidget()
{
  mExcused->clear();
  for (NodeExcusesMap::SortedLoop p(mNodeExcusesMap.loopSorted());
       !p.atEnd(); ++p)
  {
    const UtString& violator = p.getKey();
    Excuse excuse = p.getValue();
    QTreeWidgetItem* item = new QTreeWidgetItem(mExcused);
    item->setText(0, violator.c_str());
    switch (excuse) {
    case eOnDemandIdleDeposit: item->setText(1, "IdleDeposit");     break;
    case eOnDemandExcluded:    item->setText(1, "Excluded");        break;
    case eOnDemandBoth: item->setText(1, "Excluded & IdleDeposit"); break;
    }
  }
  mExcused->resizeColumnToContents(0);
}
