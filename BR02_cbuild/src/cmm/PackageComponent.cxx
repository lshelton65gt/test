/******************************************************************************
 Copyright (c) 2008-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#include "PackageComponent.h"
#include "util/UtDLList.h"
#include "ModelStudioCommon.h"
#include "PackageOptionsItem.h"
#include "CarbonMakerContext.h"

extern const char* gCarbonVersion();

class PackageTool : public CarbonTool
{
public:
  CARBONMEM_OVERRIDES
  PackageTool();

};

PackageTool::PackageTool() : CarbonTool("PackageOptions", ":cmm/Resources/PackageTool.xml")
{
  mProperties.readPropertyDefinitions(":/cmm/Resources/PackageComponentOptions.xml");
  mDefaultOptions = new CarbonOptions(NULL, getProperties(), "PackageDefaults");
  mOptions = new CarbonOptions(mDefaultOptions, getProperties(), "PackageOptions");
}


PackageComponent::PackageComponent(CarbonProject *proj) :
  CarbonComponent(proj, PACKAGE_COMPONENT_NAME)
{
 mConfigs        = new CarbonConfigurations(proj);
 mProjName       = proj->shortName();
 mCurrentConfig  = proj->getActiveConfiguration();
 
 // Setup only a Default configuration, since packaging options are not
 // dependent on configuration.
 CarbonConfigurations* configs = proj->getConfigurations();
 for (UInt32 i=0; i<configs->numConfigurations(); i++)
 {
   CarbonConfiguration* projConfig = configs->getConfiguration(i);
   const char* configName = projConfig->getName();

   CarbonConfiguration* config = mConfigs->addConfig("Linux", configName);

   config->addTool(new PackageTool()); 
   createDirectory(config);

   mRootItems[configName] = new PackageTreeRoot("");
 }

  CQT_CONNECT(proj, configurationChanged(const char*), this, configurationChanged(const char*));
  CQT_CONNECT(proj, configurationRenamed(const char*, const char*), this, configurationRenamed(const char*, const char*));
  CQT_CONNECT(proj, configurationCopy(const char*, const char*), this, configurationCopy(const char*, const char*));
  CQT_CONNECT(proj, configurationRemoved(const char*), this, configurationRemoved(const char*));
}

PackageComponent::~PackageComponent()
{
  // Delete tree
  for(TreeMap::iterator iter = mRootItems.begin(); iter != mRootItems.end(); ++iter) {
    PackageTreeRoot* root = static_cast<PackageTreeRoot*>(iter->second);
    root->removeAllChildren();
    delete root;
  }

  // Delete configurations
  delete mConfigs;
}

// Set reasonable defaults for packaging.  This allows packaging without 
// needing to first fill out the Package Options form.
void PackageComponent::setDefaultValues()
{
  // Set default values for Package Options.
  size_t pos = mProjName.find_first_of('.');
  // Lop off ".carbon".
  UtString prodName(mProjName, 0, pos);
  setProductName(prodName.c_str());
  QString shortName(getProductName());
  shortName.replace(" ", ""); // Remove spaces
  setShortName(qPrintable(shortName));
  setVersion("1");
  addComponentToPackage("StandAlone");
}

PackageComponent* PackageComponent::deserialize(CarbonProject *proj, xmlNodePtr parent, UtXmlErrorHandler *eh)
{
  PackageComponent* comp = NULL;

  // Is this a PackageComponent?
  if (XmlParsing::isElement(parent, "PackageComponent"))
  {
    comp = new PackageComponent(proj);   
    comp->createConfigurations(parent);

    for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
    {
      if (XmlParsing::isElement(child, "Configurations"))
        comp->getConfigurations()->deserialize(child, eh);
      else if (XmlParsing::isElement(child, "PackageContent"))
        comp->deserializePackage(child, eh);
    }
  }

  return comp;

}

void PackageComponent::deserializePackage(xmlNodePtr parent, UtXmlErrorHandler *eh)
{
  CarbonConfiguration* config = getProject()->getConfigurations()->getConfiguration(0);
  PackageTreeItem* rootItem = mRootItems[config->getName()];
  if (rootItem)
  {
    for (xmlNodePtr child = parent->children; child != NULL; child = child->next) {
      PackageTreeItem* item = PackageTreeItem::deserialize(child, eh, rootItem);
      if(item) rootItem->addChild(item);
    }
  }
}

CarbonComponentTreeItem* PackageComponent::createTreeItem(CarbonProjectWidget* tree, QTreeWidgetItem* parent)
{
  if (tree->context()->getCanCreatePackage()) {
    PackageOptionsItem* item = new PackageOptionsItem(tree, parent, this);
    if (!parent)
    {
      // If parent is null, this item should be placed as a top level of the tree,
      // so add as top level item.
      tree->addTopLevelItem(item);
    }
    return item;
  }
  else
    return NULL;
}

void PackageComponent::createConfigurations(xmlNodePtr parent)
{
  // first make sure the configurations exist for later deserialization
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "Configurations"))
    {
      for (xmlNodePtr configChild = child->children; configChild != NULL; configChild = configChild->next) 
      {
        if (XmlParsing::isElement(configChild, "Configuration"))
        {
          UtString configName;
          XmlParsing::getProp(configChild, "Name", &configName);

          // Seperate Config name from platform
          size_t divider = configName.find_first_of('|');
          UtString name(configName, 0, divider);
          configName.erase(0, divider+1);
          UtString platform = configName;

          CarbonConfiguration* cfg = getConfigurations()->findConfiguration(name.c_str());
          if (cfg == NULL)
          {
            cfg = getConfigurations()->addConfig(platform.c_str(), name.c_str());
            cfg->addTool(new PackageTool());
          }
        }
      }
    }
  }
}

bool PackageComponent::serialize(xmlTextWriterPtr writer)
{
  xmlTextWriterStartElement(writer, BAD_CAST "PackageComponent");
 
  mConfigs->serialize(writer);
  xmlTextWriterStartElement(writer, BAD_CAST "PackageContent");

  CarbonConfiguration* config = getProject()->getConfigurations()->getConfiguration(0);

  mRootItems[config->getName()]->serialize(writer);
  xmlTextWriterEndElement(writer); // PackageContent
  xmlTextWriterEndElement(writer); // PackageComponent

  return true;
}

// Create the directory for the configuration
void PackageComponent::createDirectory(CarbonConfiguration* config)
{
  UtString outDir;

  OSConstructFilePath(&outDir, config->getProject()->getProjectDirectory(), config->getPlatform());
  OSConstructFilePath(&outDir, outDir.c_str(), config->getName());
  OSConstructFilePath(&outDir, outDir.c_str(), MAXSIM_COMPONENT_SUBDIR);

  QDir q(outDir.c_str());
  if (!q.exists())
  {
    qDebug() << "Creating directory: " << outDir.c_str();
    q.mkpath(outDir.c_str());
  }
}

CarbonOptions* PackageComponent::getOptions(const char* configName) const
{ 
  // Default to active config
  if(configName == NULL) configName = mCurrentConfig.c_str();
  
  // Find configuration
  CarbonConfiguration* conf = mConfigs->findConfiguration(configName);
  CarbonOptions* options = NULL;
  if(conf)
    options = conf->getOptions("PackageOptions");

  return options;
}

const char* PackageComponent::getOption(const char* option)
{
  CarbonOptions* options = getOptions();
  INFO_ASSERT(options, "Empty package options.");
  if(options)
    return getOptions()->getValue(option)->getValue();

  // This should never happen
  else return "";
}

void PackageComponent::setOption(const char* option, const char* value)
{
  CarbonOptions* options = getOptions();
  INFO_ASSERT(options, "Empty package options.");
  if(options)
    options->putValue(option, value);
}

const char* PackageComponent::getProductName()
{
  return getOption("Product Name");
}

void PackageComponent::setProductName(const char* value)
{
  setOption("Product Name", value);
}

const char* PackageComponent::getShortName()
{
  return getOption("Short Name");
}

void PackageComponent::setShortName(const char* value)
{
  setOption("Short Name", value);
}

const char* PackageComponent::getVersion()
{
  return getOption("Version");
}

void PackageComponent::setVersion(const char* value)
{
  setOption("Version", value);
}


bool PackageComponent::containsComp(const char* compName)
{
  UtString optionName;
  optionName << compName << " checked";

  CarbonOptions* options = getOptions();
  CarbonPropertyValue* propVal = NULL;
  if (options->findProperty(optionName.c_str()))
    propVal = options->getValue(optionName.c_str());
  if (propVal)
    return strcmp(propVal->getValue(), "true") == 0;
  
  return false;
}

PackageTreeItem* PackageComponent::getRootItem()
{
  return mRootItems[mCurrentConfig];
}

bool PackageComponent::addComponentToPackage(const char* compName)
{
  CarbonConfiguration* config = getProject()->getConfigurations()->getConfiguration(0);
  PackageTreeItem* rootItem = mRootItems[config->getName()];
  bool success = false;
  
  // Don't add if, component is already added
  if(containsComp(compName)) return true;

  // Ouput Makefile Macro
  UtString outDir("./");

  // Standalone is not an actual component so need to treat that specially.
  if(strcmp(compName, "StandAlone") == 0) {
    UtString vhmName(getProject()->getVHMName());
    addFile(vhmName.c_str(), UtString(outDir + vhmName), "StandAlone", rootItem);

    size_t pos = vhmName.find_last_of('.');
    UtString vhmBase(vhmName, 0, pos);
    UtString modelHeader(vhmBase + ".h");
    addFile(modelHeader.c_str(), UtString(outDir + modelHeader), "StandAlone", rootItem);

    // We now embed the iodb, so we don't automatically add .db files.
  }
  else {
    // Find component and query about files that it wants packaged
    CarbonComponent* comp = getProject()->getComponents()->findComponent(compName);
    if(comp) {
      for(UInt32 i = 0; i < comp->numPackageFiles(); ++i){
        QString fileName = comp->getPackageFile(i);
        UtString packageFile;
        packageFile << comp->getComponentDirectory() << "/" << fileName;
        UtString srcFile;
        srcFile << outDir << comp->getComponentDirectory() << "/" << fileName;
        addFile(packageFile, srcFile, compName, rootItem);
      }
      success = true;
    }
  }
  
  // Mark component as added
  CarbonOptions* options = getOptions();
  if(options) {
    UtString optionName;
    optionName << compName << " checked";
    options->putValue(optionName.c_str(), "true");
  }
  return success;
}

bool PackageComponent::removeComponentFromPackage(const char* compName)
{
  CarbonConfiguration* config = getProject()->getConfigurations()->getConfiguration(0);
  PackageTreeItem* rootItem = mRootItems[config->getName()];

  // Go through the tree and remove all nodes that matches the component name
  // If there are user files in component specific directories, they will
  // be removed too.  
  bool success = rootItem->removeNamedChildren(compName);

  // Mark component unchecked
  CarbonOptions* options = getOptions();
  if(options && success) {
    UtString optionName;
    optionName << compName << " checked";
    options->putValue(optionName.c_str(), "false");
  }

  return success;
}

bool PackageComponent::addFile(const UtString &packageLoc, const UtString &source, const char* compName, PackageTreeItem* dirItem)
{
  bool success = true;

  // First we need to parse the package location to seperate out the directories
  size_t lastDirPos = 0;
  UtString locName(packageLoc);
  if(dirItem) {
    do {
      size_t dirPos = locName.find_first_of('/');
      if(dirPos != UtString::npos) {
        UtString dir;
        dir.assign(locName, 0, dirPos);
        locName.erase(0, dirPos+1);
        qDebug() << "PackageComponent::addDir:" << dir.c_str();

        // Go through the children to see if this directory exists, otherwise add
        bool foundDir = false;
        for(PackageTreeItem* item = dirItem->firstChild(); item != NULL; item = dirItem->nextChild()) {
          if(strcmp(item->name(), dir.c_str()) == 0) {
            foundDir = true;
            dirItem = static_cast<PackageTreeDir*>(item);
            break;
          }
        }

        if(!foundDir) {
          // If the directory didn't exist, create a new one
          PackageTreeDir* tmpDir = new PackageTreeDir(dir.c_str(), compName, dirItem);
          dirItem->addChild(tmpDir);
          dirItem = tmpDir;
        }
      }
      lastDirPos = dirPos;
    } while (lastDirPos != UtString::npos);


    // What should be left now is the filename, add it to the directory
    PackageTreeFile* file = new PackageTreeFile(locName.c_str(), source.c_str(), compName, dirItem);

    dirItem->addChild(file);
  }
  else success = false;

  return success;
}

void PackageComponent::configurationChanged(const char* config)
{
  // If the configuration doesn't 
  if(mCurrentConfig != config) {
    if(mRootItems.find(config) != mRootItems.end()) {
      mCurrentConfig = config;
      qDebug() << "PackageComponent: Config changed to " << config << ".";
      createDirectory(getProject()->getConfigurations()->findConfiguration(config));
    }
  }
}

void PackageComponent::configurationRemoved(const char* config)
{
  if(mRootItems.find(config) != mRootItems.end()) {
    PackageTreeRoot* root = static_cast<PackageTreeRoot*>(mRootItems[config]);
    root->removeAllChildren();
    mConfigs->removeConfiguration(config);

    // If the configuration removed is the active configuration, we need to clear it
    if(mCurrentConfig == config) {
      mCurrentConfig = "";
    }
  }
  qDebug() << "PackageComponent: Config " << config << "removed.";
}

void PackageComponent::configurationRenamed(const char* fromConfig, const char* toConfig)
{ 
  PackageTreeItem* fromItem = mRootItems[fromConfig];
  mRootItems.erase(fromConfig);
  mRootItems[toConfig] = fromItem;
  qDebug() << "PackageComponent: Config renamed.";
}

void PackageComponent::configurationCopy(const char*, const char*)
{
  qDebug() << "PackageComponent: Config copied.";
}

// --------------------------------------------------------------------------
// Makefile Generation Methods
// --------------------------------------------------------------------------
const char* PackageComponent::getTargetName(CarbonConfiguration*,TargetType type, TargetPlatform platform)
{
  static UtString targetName;
  targetName.clear();
  
  UtString dirSeparator = (platform == CarbonComponent::Windows) ? "\\" : "/";
 
  switch (type)
  {
  case CarbonComponent::Build: break;
  case CarbonComponent::Package: targetName << PACKAGE_COMPONENT_SUBDIR << dirSeparator << InstallerName(); break;
  case CarbonComponent::Clean: targetName << PACKAGE_COMPONENT_TARGET_CLEAN; break;
  }

  return targetName.c_str();
}

// Generate code to set CARBON_IJ_PLATFORM Make variable containing
// platform name in IJ-speak to stream.
void
PackageComponent::setIjPlatform(QTextStream& stream)
{
  stream <<
    "# Table for mapping Carbon OS names to InstallJammer OS names"               "\n"
    "CARBON_IJ_OSMAP_Windows := Windows"                                          "\n"
    "CARBON_IJ_OSMAP_Linux := Linux-x86"                                          "\n"
    ""                                                                            "\n"
    "CARBON_MPI := $(CARBON_HOME)/lib/installers/IPInstall.mpi"                   "\n"
    ""                                                                            "\n"
    "ifeq ($(CARBON_MODEL_PLATFORM),Unix)"                                        "\n"
    "  CARBON_OS_SPECIFIC := $(shell uname)"                                      "\n"
    "else"                                                                        "\n"
    "  CARBON_OS_SPECIFIC := Windows"                                             "\n"
    "endif"                                                                       "\n"
    ""                                                                            "\n"
    "CARBON_IJ_PLATFORM := $(CARBON_IJ_OSMAP_$(CARBON_OS_SPECIFIC))"              "\n\n";
}

// Generate the command for the Make rule that builds the installer to
// stream.  This is always run on Unix.
void
PackageComponent::PackageCommand(QTextStream& stream)
{
  stream <<
    "ifeq ($(CARBON_OS),Unix)"                                                    "\n"
    "	$(CARBON_CP) \"$(CARBON_MPI)\" Package"                                   "\n"
    "	\"$(CARBON_HOME)/installjammer/installjammer$(CARBON_EXE_EXT)\""          "\\\n"
    "		--platform $(CARBON_IJ_PLATFORM)"                                 "\\\n"
    "		-DAppName \"$(PackageProductName)\""                              "\\\n"
    "		-DShortAppName \"$(PackageShortName)\""                           "\\\n"
    "		-DBuildDir package"                                               "\\\n"
    "		-DInstallerName \"$(PackageFileName)\""                           "\\\n"
    "		-DVersion \"$(PackageVersion)\""                                  "\\\n"
    "		-DIs64Bit $(Package64Bit)"                                        "\\\n"
    "		-DRuntimeVersionRequired " << CARBON_RELEASE_ID <<                "\\\n" <<
    "		--output-dir ../.."                                               "\\\n"
    "		--build-for-release"                                              "\\\n"
    "		Package/IPInstall.mpi"                                            "\n"
    "endif"                                                                       "\n";
}

// Return filename of installer for this product/platform/configuration.
QString
PackageComponent::InstallerName()
{
  QStringList result;

  // Example: "MyProject-Linux-Default-Install"
  // Return empty string if no product name.
  if (strlen(getProductName()) > 0)
    result << getProductName() << "-"
           << getProject()->getActivePlatformReal() << "-"
           << mCurrentConfig.c_str() << "-Install"
#if pfWINDOWS                   // Should be if targeting Windows
           << ".exe"
#endif
      ;
  return result.join("");
}

bool PackageComponent::getTargetImpl(CarbonConfiguration* cfg, QTextStream& stream, TargetType type)
{
  UtString packageRootDir;
  packageRootDir << PACKAGE_COMPONENT_SUBDIR << "/package/";

  bool success = false;
  switch (type)
  {
  case CarbonComponent::Build:
    break;
  case CarbonComponent::Package:
    {
      stream << "\n#\n# Package Component\n#" << endl;
      CarbonConfiguration* config = getProject()->getConfigurations()->getConfiguration(0);

      // Generate code to set InstallJammer platform name for passing to --platform option.
      setIjPlatform(stream);

      // Setup Package Makefile Variables
      stream << "PackageFileName=" << InstallerName() << endl;
      stream << "PackageProductName=" << getProductName() << endl;
      stream << "PackageShortName=" << getShortName() << endl;
      stream << "PackageVersion=" << getVersion() << endl;
      stream << "PackageComponentSources=" << getAllSourceFiles(mRootItems[config->getName()]).c_str() << endl; 
      stream << "Package64Bit = $(if $(findstring 64,$(CARBON_TARGET_ARCH)),1,0)" << endl;
      stream << endl;
      
      // Installer Target
      // We need the PackageClean prerequisite because InstallJammer
      // will package everything left in the package directory.  If we
      // don't clean it out, we may package components that have been
      // turned off in Package Options in the GUI.  And we need to do
      // it only on Unix, because forward slashes cause the associated
      // build actions to fail on Windows.
      stream << getTargetName(cfg, type, CarbonComponent::Unix) << ": $(CARBON_PACKAGECLEAN) " << PACKAGE_COMPONENT_SUBDIR << "/packageCreated" << endl;
      PackageCommand(stream);
      stream << endl;

      // Package Directory Target (only on linux)
      stream << PACKAGE_COMPONENT_SUBDIR << "/packageCreated: $(PackageComponentSources)" << endl;
      stream << "ifeq ($(CARBON_OS),Unix)" << endl;
      stream << "\t$(CARBON_MKDIR) " << packageRootDir.c_str() << endl; 
      PackageTreeItem::PkgItemList dirList = createPackageDirList(mRootItems[config->getName()]);
      for(PackageTreeItem::PkgItemList::iterator iter = dirList.begin(); iter != dirList.end(); ++iter) {
        stream << "\t$(CARBON_MKDIR) " << packageRootDir.c_str() << (*iter)->packagePath().c_str() << endl;
      }
      PackageTreeItem::PkgItemList fileList = createPackageFileList(mRootItems[config->getName()]);
      for(PackageTreeItem::PkgItemList::iterator iter = fileList.begin(); iter != fileList.end(); ++iter) {
        stream << "\t$(CARBON_CP) " << (*iter)->filePath() << " " << packageRootDir.c_str() << (*iter)->packagePath().c_str() << endl;
      }
      stream << "\ttouch $@" << endl;
      stream << "else" << endl;
      stream << "\t@echo CARBON COMPONENT PACKAGING NOT SUPPORTED ON WINDOWS" << endl;
      stream << "endif" << endl;
      success = true;
    }
    break;
  case CarbonComponent::Clean:
    {
      stream << ".PHONY: " << getTargetName(cfg, CarbonComponent::Clean, CarbonComponent::Unix) << endl;
      stream << getTargetName(cfg, CarbonComponent::Clean, CarbonComponent::Unix) << ":" << endl;
      stream << "\t" << echoBegin() << "CarbonWorkingDir: " << PACKAGE_COMPONENT_NAME << "" << echoEndl();
      stream << "\t" << "-cd " << PACKAGE_COMPONENT_SUBDIR << " && $(CARBON_RM) package " << InstallerName() << " packageCreated" << endl;
      stream << "\t" << echoBegin() << "Cleaned " << PACKAGE_COMPONENT_NAME << " Component" << echoEndl();
      success = true;
    }
    break;
  }
  return success;
}

// Convenience methods for makefile generation
UtString PackageComponent::getAllSourceFiles(PackageTreeItem* root)
{
  PackageTreeItem::PkgItemList itemList = createPackageFileList(root);
  UtString fileList;
  for(PackageTreeItem::PkgItemList::iterator iter = itemList.begin(); iter != itemList.end(); ++iter)
    fileList << (*iter)->filePath() << " ";

  return fileList;
}
