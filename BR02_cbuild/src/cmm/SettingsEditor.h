#ifndef SETTINGSEDITOR_H
#define SETTINGSEDITOR_H
#include "util/CarbonPlatform.h"

#include <QObject>
#include <QWidget>
#include <QTreeWidget>
#include <QItemDelegate>

#include "ui_SettingsEditor.h"
#include "SettingsContainer.h"

class CarbonOptions;
class CarbonMakerContext;

class SettingsEditor : public QWidget
{
  Q_OBJECT

public:
  SettingsEditor(QWidget *parent = 0);
  ~SettingsEditor();

  void setSettings(CarbonOptions* settings);
  void setSettings(CarbonOptions* settings, PropertyEditor::SortType type);
  void putContext(CarbonMakerContext* ctx);
  void registerActiveConfigurationChange(QComboBox* src);
  CarbonOptions* getSettings() { return mSettings; }
  void commitEdit();

private slots:
  void on_toolButtonAlphabetical_clicked();
  void on_toolButtonCategorized_clicked();
  void on_toolButtonReset_clicked();
  void on_toolButtonResetAll_clicked();
  void itemActivated(QTreeWidgetItem* itm, int col);
  void selectionChanged();
  void configurationChanged();
  void itemSelectionChanged(int index);
  void updateResetButtons(bool resetAll, bool resetSwitch);

private:
  CarbonMakerContext* mContext;
  Ui::SettingsEditorClass ui;
  CarbonOptions* mSettings;
};

#endif // SETTINGSEDITOR_H
