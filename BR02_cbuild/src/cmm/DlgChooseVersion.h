#ifndef DLGCHOOSEVERSION_H
#define DLGCHOOSEVERSION_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include "ModelKit.h"

#include <QDialog>
#include "ui_DlgChooseVersion.h"

class DlgChooseVersion : public QDialog
{
  Q_OBJECT

public:
  DlgChooseVersion(QWidget *parent = 0, ModelKit* kit = 0);
  ~DlgChooseVersion();

  KitVersion* getVersion() { return mVersion; }

private:
  ModelKit* mModelKit;
  KitVersion* mVersion;
  Ui::DlgChooseVersionClass ui;

private slots:

    void on_comboBoxVersions_currentIndexChanged(int);
    void on_buttonBox_rejected();
    void on_buttonBox_accepted();
};

#endif // DLGCHOOSEVERSION_H
