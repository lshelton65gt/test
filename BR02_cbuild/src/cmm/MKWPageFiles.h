#ifndef MKWPAGEFILES_H
#define MKWPAGEFILES_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>
#include "WizardPage.h"
#include "DesignXML.h"
#include <QWizardPage>
#include "ui_MKWPageFiles.h"


class MKWPageFiles;

class FileManagerDelegate : public QItemDelegate
{
  Q_OBJECT

public:
  FileManagerDelegate(QObject* parent=0, QTreeWidget* tw=0, MKWPageFiles* t=0);
  QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem&, const QModelIndex&) const;
  void setEditorData(QWidget* editor, const QModelIndex& index) const;
  void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;
  void commit(QWidget* w)
  {
    commitData(w);
    closeEditor(w);
  }

public slots:
  void currentIndexChanged(int);

private:
  QTreeWidget* mTree;
  MKWPageFiles* mPageFiles;
};


class FileManagerTreeItem : public QTreeWidgetItem
{
public:
  enum NodeType { 
                  RTLSourceFile,                  
                };

  FileManagerTreeItem(NodeType nodeType) : QTreeWidgetItem()
  {
    mNodeType = nodeType;
    init();
  }
  FileManagerTreeItem(NodeType nodeType, QTreeWidget* parent) : QTreeWidgetItem(parent)
  {
    mNodeType = nodeType;
    init();
  }
  FileManagerTreeItem(NodeType nodeType, QTreeWidgetItem* parent) : QTreeWidgetItem(parent)
  {
    mNodeType = nodeType;
    init();
  }

  virtual ~FileManagerTreeItem() {}

  virtual QWidget* createEditor(const FileManagerDelegate* /*delegate*/, 
    QWidget* /*parent*/, int /*columnIndex*/);
  virtual void setModelData(const FileManagerDelegate* /*delegate*/, 
    QAbstractItemModel* /*model*/, const QModelIndex & /*modelIndex*/,
    QWidget* /*editor*/, int /*columnIndex*/);
  void setModified(bool value);
 
  NodeType getNodeType() const { return mNodeType; }

  void init()
  {
    mIsHashed = true;
  }

  bool getHashed() { return mIsHashed; }
  void setHashed(bool newValue) { mIsHashed=newValue; }

public:
 
private:  
  NodeType mNodeType;
  bool mIsHashed;
};



class MKWPageFiles : public WizardPage
{
  Q_OBJECT

  enum FileKind { RTLFileReference, OptionalRTLFileReference, EmbeddedFile, EmbeddedUserFile };

public:
  MKWPageFiles(QWidget *parent = 0);
  ~MKWPageFiles();

private:
  XDesignHierarchy* mDesignHierarchy;

protected:
  virtual void initializePage();
  virtual bool validatePage();
  virtual void cleanupPage();

private:
  void embedComponentFiles();
  void embedProjectFiles();
  void addRTLFiles();
  void addEmbeddedFile(const QString& rawFileName, QTreeWidgetItem* parentItem, bool userFile=false);
  void addUserFiles();
  void checkItems(QTreeWidgetItem* parent, Qt::CheckState checkState);

private slots:
  void itemSelectionChanged();
  void itemActivated(QTreeWidgetItem*,int);
    void on_treeWidget_itemSelectionChanged();
    void on_pushButtonDelete_clicked();   
    void on_toolButtonUncheckAll_clicked();
    void on_toolButtonCheckAll_clicked();
  void on_pushButtonAddFiles_clicked();

private:
  void populate();

private:
  Ui::MKWPageFilesClass ui;
  QTreeWidgetItem* mAdditionalFiles;
  

  bool mInitialized;
};

#endif // MKWPAGEFILES_H
