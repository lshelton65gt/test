# We want built-in wchar support, and no unicode
s^TreatWChar_tAsBuiltInType="false"^TreatWChar_tAsBuiltInType="true"^g
s^UNICODE^NO_UNICODE^g
# Because Qt was installed into y:\windows\qt-4.4.3 at the time it was
# built, that's permanently stored internally, and is used by default
# to build the locations of headers, libraries, etc.  While in most
# places it's possible to override this by setting variables in the
# .pro file, there are some library references that I can't seem to
# fix.  Luckily, all the y: references seem to be duplicates of other
# library references that use the correct drive letter.  Since that
# drive letter is never y:, we can safely remove the y: references.
s^ e:\\Qt\\4.7.1\\lib\\QtXmld4.lib e:\\Qt\\4.7.1\\lib\\QtGuid4.lib e:\\Qt\\4.7.1\\lib\\QtCored4.lib^^g
s^ e:\\Qt\\4.7.1\\lib\\QtXml4.lib e:\\Qt\\4.7.1\\lib\\QtGui4.lib e:\\Qt\\4.7.1\\lib\\QtCore4.lib^^g
# Add missing comments that allow Incredibuild to parallelize moc calls
s^CommandLine="\(.*moc\.exe\)^CommandLine="Rem Incredibuild_AllowRemote \&#x0d;\&#x0a; \1^g
