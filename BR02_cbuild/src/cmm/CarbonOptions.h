#ifndef __CARBONOPTIONS_H__
#define __CARBONOPTIONS_H__
#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include <QtGui>

#include "util/XmlParsing.h"

#include "util/CarbonAssert.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"
#include "util/UtList.h"

#include "CarbonTool.h"
#include "Scripting.h"

class JavaScriptAPIs;

#include "CarbonProperties.h"

class CarbonConfiguration;

class CarbonSetting
{
public:
  CARBONMEM_OVERRIDES

    CarbonSetting() 
  {
    mSwitch = NULL;
    mHasValue = false;
    mValue.clear();
  }

  void setSwitch(CarbonProperty* newVal) {mSwitch=newVal;}
  CarbonProperty* getSwitch() {return mSwitch; }
  void setValue(const char* newVal)
  {
    mHasValue = newVal != NULL ? true : false;
    if (newVal)
      mValue = newVal;
    else
      mValue.clear();
  }

private:
  CarbonProperty* mSwitch;
  bool mHasValue;
  UtString mValue;

};

// This holds a value for a switch, the value is always
// represented by a string
class CarbonPropertyValue : public QObject
{
  Q_OBJECT

public:
  CARBONMEM_OVERRIDES

    CarbonPropertyValue(CarbonOptions* options, const CarbonProperty* sw) : QObject(0)
  {
    mSwitch=sw;
    mValue.clear();
    mValueSet=false;
    mMultiVal=sw->isMultiValued();   
    if (sw->hasDefaultValue())
      mValue = sw->getDefaultValue();
    if (sw->hasGuiDefaultValue())
      mValue = sw->getGuiDefaultValue();
    mOptions = options;
  }

  const char* multiValueString(const QStringList& items);

  const CarbonProperty* getProperty() const {return mSwitch; }

  bool hasDefaultValue()
  {
    return mSwitch->hasDefaultValue();
  }
  bool isDefaultValue(const char* val)
  {
    if (mSwitch->hasDefaultValue())
    {
      return (0 == strcmp(val, mSwitch->getDefaultValue())) ? true : false;
    }
    else
      return false;
  }
  void putValue(const char* newVal);

  // Takes in a list of items, un-quoted and stores them
  // and updates mValue
  void putValues(const QStringList& items);
  // encodedQuoteString can be of the form:
  // a;b or "a";"b" or "a;b";"c"
  // or 'a';'b'

  const char* getValue() {return mValue.c_str(); }
  const QStringList& getValues() { return mValues; }

  bool getBoolValue() const;
  void putBoolValue(bool val);
 
  bool hasValueSet() { return mValueSet; }
  void setMultiValue(bool newVal) { mMultiVal = newVal; }
  void updateValue(const char* val);

  CarbonOptions* getOptions() const { return mOptions; }
private:

private:
  bool mValueSet;
  bool mMultiVal;
  const CarbonProperty* mSwitch;
  UtString mValue;
  QStringList mValues;
  CarbonOptions* mOptions;
};

// This class represents the settings for a given object, its parentage
// represents the chain of values back to the NULL parent (root)
// 
class CarbonOptions : public QObject
{
  Q_OBJECT

  Q_CLASSINFO("ClassName", "Switches");

public:
  CarbonOptions(CarbonOptions* parent, CarbonProperties* properties, const char* name);
  virtual ~CarbonOptions();

  void setSelectedItem(const char* item, const QVariant& data);
  void setItems(const char* item, const QStringList& itemList, const QVariantList& itemDataList);
  void setBlockSignals(bool value) { mBlockSignals=value; }
  void removeProperty(const CarbonProperty* prop);
  void removeProperty(const char* propName);
  void removeGroup(const char* groupName);
  void setParentOptions(CarbonOptions* parent);

  const char* getParentValue(const char* propName) const;

protected:
  virtual void onSelectedItemChanged(const char* item, const QVariant& data);

public:
  void addSwitch(const CarbonProperty* sw)
  {
    if (mSwitchValues[sw->getName()] == NULL) 
    {
      CarbonPropertyValue* sv = new CarbonPropertyValue(this, sw);
      mSwitchValues[sw->getName()] = sv;
    }
  }
  void removeValue(const char* propName);
  void putValue(const char* propName, const QString& value)
  {
    UtString v;
    v << value;
    putValue(propName, v.c_str());
  }
  void putValue(const CarbonProperty* sw, const char* value);
  void putValue(const char* propName, const char* value)
  {
    putValue(findProperty(propName), value);
  }
  void putValues(const CarbonProperty* sw, const QStringList& valueList);
  void putValues(const char* propName, const QStringList& valueList)
  {
    putValues(findProperty(propName), valueList);
  }
  UInt32 numOptionsSet() { return mSwitchValues.size(); }

  struct PropertyNotify
  {
    QObject* mObject;
    UtString mSlotName;
  };
  
  static void registerTypes(QScriptEngine* engine);
  static void registerScriptTypes(QMap<QString, const QMetaObject*>& map);
  static void registerIntellisense(JavaScriptAPIs* apis);
  CarbonPropertyGroup* findGroup(const QString& groupName);

public slots:
  CarbonProperty* lookupProperty(const QString& propertyName);
  quint32 numSwitches();
  void setSwitchValue(const QString& switchName, const QString& value);
  CarbonPropertyValue* getSwitchValue(quint32 index);
  CarbonPropertyValue* getSwitchValue(const QString& propName);

public:
  const CarbonPropertyGroup* findGroup(const char* name);
  const CarbonProperty* findProperty(const char* name) const;
  const CarbonProperty* findProperty(CarbonPropertyGroup *group, const char* name) const;
  const CarbonProperty* locateProperty(const char* name);
  CarbonPropertyValue* getValue(const char* propName, bool* isDefaulted=NULL, bool* isThisLevel=NULL) const;
  CarbonPropertyValue* getValue(const CarbonProperty* sw, bool* isDefaulted, bool* isThisLevel) const;
  CarbonPropertyValue* lookupValue(const CarbonProperty* sw) const;
  const CarbonOptions* getParent() const  { return mParent; }
  const CarbonProperties* getProperties() const;
  const char* getName() { return mName.c_str(); }
  typedef UtHashMap<UtString,CarbonPropertyValue*> SwitchMap;
  bool serialize(xmlTextWriterPtr writer);
  CarbonProperties* getEditProperties() { return mProperties; }
  const QStringList& getItems() { return mItems; }
  const QVariantList& getItemsData() { return mItemDataList; }
  const char* getItem() { return mItem.c_str(); }

  class NotifyClientEntry
  {
  public:
    NotifyClientEntry(QObject* obj, const char* slotName)
    {
      mObject=obj;
      mSlotName=slotName;
    }
    ~NotifyClientEntry() 
    {
    }
    const char* getSlotName() { return mSlotName.c_str(); }
    QObject* getObject() { return mObject; }

  private:
    QObject* mObject;
    UtString mSlotName;
  };

  typedef QList<NotifyClientEntry*> NotifyEntryList;

  void registerPropertyChanged(const char* propertyName, const char* slotName, QObject* obj);
  void unregisterPropertyChanged(const char* propertyName, QObject* obj);

  typedef SwitchMap::SortedLoop OptionsLoop;

  OptionsLoop loopOptions() 
  {
    return mSwitchValues.loopSorted();
  }

signals:
  void propertiesModified();
  void selectionChanged(const char* item,  const QVariant& itemData);
  void propertyModified(const CarbonProperty* prop, const char* oldValue, const char* newValue);

private:
  SwitchMap mSwitchValues;
  bool mBlockSignals;

  QVariantList mItemDataList;
  QStringList mItems;
  UtString mItem;

  QMap<QString, NotifyEntryList*> mNotifyChangedMap;
  const CarbonOptions* mParent;
  CarbonProperties* mProperties;
  UtString mName;
};

Q_DECLARE_METATYPE(CarbonOptions*);
Q_DECLARE_METATYPE(CarbonPropertyValue*);

#endif
