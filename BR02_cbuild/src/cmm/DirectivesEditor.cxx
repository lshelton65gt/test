//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "SystemCWizardWidget.h"
#include "Mdi.h"
#include "MdiDirectives.h"

#include "CodeGenerator.h"
#include "DirectivesEditor.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"

#include "util/UtString.h"
#include "util/UtHashMap.h"
#include "gui/CQt.h"

#include "CarbonDirectives.h"
#include "Directives.h"

TabNetDirectives* DirectivesEditor::getNetDirectivesTab()
{
  ui.tabWidget->setCurrentIndex(tabNETS);
  return ui.tableNetDirectives;
}

TabModuleDirectives* DirectivesEditor::getModuleDirectivesTab()
{
  ui.tabWidget->setCurrentIndex(tabMODULES);
  return ui.tableModuleDirectives;
}

DirectivesEditor::DirectivesEditor(CarbonMakerContext* ctx, MDIDocumentTemplate* t, QWidget *parent)
: QWidget(parent)
{
  ui.setupUi(this);

  initializeMDI(this, t);
  mCtx = ctx;

  ui.tableNetDirectives->setDirectivesEditor(this);
  ui.tableModuleDirectives->setDirectivesEditor(this);

  CQT_CONNECT(ui.tableNetDirectives, itemSelectionChanged(), this, itemSelectionChanged()); 
  CQT_CONNECT(ui.tableModuleDirectives, itemSelectionChanged(), this, itemSelectionChanged());

  CQT_CONNECT(ui.tabWidget, currentChanged(int), this, itemSelectionChanged());

  setWindowTitle("Directives[*]");

  CQT_CONNECT(ctx->getCarbonProjectWidget()->project(), configurationChanged(const char*), this, configurationChanged(const char*));
  CQT_CONNECT(ctx->getCarbonProjectWidget()->project(), configurationRemoved(const char*), this, configurationRemoved(const char*));

  CQT_CONNECT(ctx->getCarbonProjectWidget(), projectClosing(CarbonProject*), this, projectClosing(CarbonProject*));

  CQT_CONNECT(mCtx->getCarbonProjectWidget()->getConsole(), compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType));
  CQT_CONNECT(mCtx->getCarbonProjectWidget()->getConsole(), compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType));

  CQT_CONNECT(mCtx->getCarbonProjectWidget()->getHierarchyWindow(), applySignalDirective(DesignDirective,const char*,bool), this, applySignalDirective(DesignDirective,const char*, bool));

  mActiveGroup = mCtx->getCarbonProjectWidget()->project()->getDirectives()->getActiveGroup();

  populate();
}

void DirectivesEditor::applySignalDirective(DesignDirective directive, const char* text, bool value)
{
  modifyDirective(directive, text, value);
}

void DirectivesEditor::modifyDirective(DesignDirective directive, const char* text, bool value)
{
   QStringList items;
   items.append(text);
   switch(directive) {
     case eObserve:
       this->getNetDirectivesTab()->setDirective(items, "observeSignal", value); 
       break;
     case eForce:
       this->getNetDirectivesTab()->setDirective(items, "forceSignal", value); 
       break;
     case eDeposit:
       this->getNetDirectivesTab()->setDirective(items, "depositSignal", value); 
       break;
     default:
       break;
   }  
   
   if (mActiveGroup)
    mActiveGroup->getDirectives()->netSelectionChanged(text);
}

void DirectivesEditor::compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  setEnabled(true);
}

void DirectivesEditor::compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
 setEnabled(false);
 saveDocument();
}

void DirectivesEditor::projectClosing(CarbonProject* proj)
{
  if (proj)
  {
    ui.tableNetDirectives->populate(NULL);
    ui.tableModuleDirectives->populate(NULL);
  }
}
void DirectivesEditor::configurationRemoved(const char* /*name*/)
{
  saveDocument();
  CarbonProject* proj = mCtx->getCarbonProjectWidget()->project();
  mActiveGroup = proj->getDirectives()->findDirectiveGroup(proj->getActiveConfiguration());
  INFO_ASSERT(mActiveGroup, "Unable to locate Directive Group");
  populate();
}

void DirectivesEditor::configurationChanged(const char* newConfig)
{
  qDebug() << "Directives Editor Config changed to: " << newConfig;

  saveDocument();

  mActiveGroup = mCtx->getCarbonProjectWidget()->project()->getDirectives()->findDirectiveGroup(newConfig);
  INFO_ASSERT(mActiveGroup, "Unable to locate Directive Group");
  populate();
}

void DirectivesEditor::populate()
{
  ui.tableNetDirectives->populate(mActiveGroup);
  ui.tableModuleDirectives->populate(mActiveGroup);
}

const char* DirectivesEditor::userFriendlyName()
{
  static UtString filePath;
  OSConstructFilePath(&filePath, mCtx->getCarbonProjectWidget()->project()->getProjectDirectory(), "Directives");
  return filePath.c_str();
}

void DirectivesEditor::saveDocument()
{
  if (isWindowModified())
  {
    qDebug() << "Saving Directives";
    ui.tableNetDirectives->saveDocument();
    ui.tableModuleDirectives->saveDocument();
    mCtx->getCarbonProjectWidget()->saveProject();
  }

  setWindowModified(false);
}

void DirectivesEditor::closeEvent(QCloseEvent *event)
{
 if (maybeSave())
 {
    mCtx->getCarbonProjectWidget()->putDirectivesEditor(NULL);
    event->accept();
    MDIWidget::closeEvent(this);
  }
  else
  {
    event->ignore();
  }
}

bool DirectivesEditor::maybeSave()
{
  if (isWindowModified())
  {
    QMessageBox::StandardButton ret;
    ret = QMessageBox::warning(this, MODELSTUDIO_TITLE,
      tr("%1 have been modified.\n"
      "Do you want to save your changes?")
      .arg("Compiler Directives"),
      QMessageBox::Save | QMessageBox::Discard
      | QMessageBox::Cancel);
    if (ret == QMessageBox::Save)
    {
      saveDocument();
      return true;
    }
    else if (ret == QMessageBox::Cancel)
      return false;
  }
  return true;
}

DirectivesEditor::~DirectivesEditor()
{

}

void DirectivesEditor::itemSelectionChanged()
{
  switch (ui.tabWidget->currentIndex())
  {
  case tabNETS:
    {
    QTreeWidget* tree = ui.tableNetDirectives;
    emit somethingSelected(tree->selectedItems().count() > 0 ? true : false);
    return;
    break;
    }
  case tabMODULES:
    QTreeWidget* tree = ui.tableModuleDirectives;
    emit somethingSelected(tree->selectedItems().count() > 0 ? true : false);
    return;
    break;
  }

  emit somethingSelected(false);
}

void DirectivesEditor::newDirective()
{
  switch (ui.tabWidget->currentIndex())
  {
  case tabNETS:
    ui.tableNetDirectives->newDirective();
    break;
  case tabMODULES:
    ui.tableModuleDirectives->newDirective();
    break;
  }
}

void DirectivesEditor::deleteDirective()
{
  switch (ui.tabWidget->currentIndex())
  {
  case tabNETS:
    ui.tableNetDirectives->deleteDirectives();
    break;
  case tabMODULES:
    ui.tableModuleDirectives->deleteDirectives();
    break;
  }
}

void DirectivesEditor::setWindowModified(bool newVal)
{
  QWidget::setWindowModified(newVal);
}
