#ifndef TABNETDIRECTIVES_H
#define TABNETDIRECTIVES_H

#include "util/CarbonPlatform.h"

#include <QtGui>
#include <QTreeWidget>
#include <QList>
#include <QItemDelegate>
#include <QtXml>

#include "Directives.h"
class DirectivesEditor;
class CarbonDirectiveGroup;

struct SpecialDirective
{
public:
  QString mName;
  int mColumn;
};

class TabNetDirectives : public QTreeWidget
{
  Q_OBJECT

public:
  enum Column {colNAME=0, colOBSERVE, colDEPOSIT, colFORCE, colOTHER};

  TabNetDirectives(QWidget *parent);
  ~TabNetDirectives();
  virtual void dragEnterEvent(QDragEnterEvent *event);
  virtual void dropEvent(QDropEvent *event);
  virtual void dragMoveEvent(QDragMoveEvent* event);
  void setDirectivesEditor(DirectivesEditor* editor) { mDirectivesEditor=editor; }
  void deleteDirectives();
  void populate(CarbonDirectiveGroup* group);
  void saveDocument();
  void setModified(bool value);
  bool selectItem(const char* net);
  bool selectItems(const char* text);
  void setDirective(const QStringList& netNames, const char* dirName, bool setIt);
  bool getSpecialDirectiveValue(const char *netName, const char *specialDirective, bool *wasPresent);
  void setApplyNewTieValue(bool value, const char* tieValue)
  { 
    mApplyNewTieValue = value;
    mTieValue.clear();
    mTieValue << tieValue;
  }
  bool getApplyNewTieValue() const { return mApplyNewTieValue; }
  const char* getTieValue() const { return mTieValue.c_str(); }
  void newDirective();

  const QList<NetDirectives*> getDirectives();

private:
  void buildSelectionList(QList<QTreeWidgetItem*>& list);
  bool maybeSave();
  QTreeWidgetItem* addNewRow(const char* loc, bool exclusiveSelect=true, QTreeWidgetItem* clockItem=NULL);
  void createCheckBox(QTreeWidgetItem* item, NetDirectives* dir, Column col, const char* directiveName);
  void addDirectiveRow(NetDirectives* dir);
  SpecialDirective* getSpecialDirective(const char* name);
  int findDirective(QTreeWidgetItem* item, const char* dirName);
  void addDirective(QTreeWidgetItem* item, const char* dirName);
  QTreeWidgetItem* findNet(const char* netName);
  void refreshValues();
  void parseXml(const QDomElement& parent);
  void applyNewValue(QTreeWidgetItem* item, QTreeWidgetItem* origItem, int newState, int col);
  void changeCheckboxQuiet(QCheckBox* cb, int newState);

private:
  bool mApplyNewTieValue;
  UtString mTieValue;
  CarbonDirectiveGroup* mGroup;
  QList<NetDirectives*> mDirectives;
  DirectivesEditor* mDirectivesEditor;

private slots:
  void stateChanged(int);

  friend class TabNetDirectivesDelegate;

};

class DlgTieNet;

class TabNetDirectivesDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    TabNetDirectivesDelegate(QObject *parent = 0, TabNetDirectives* table=0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
        const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
        const QModelIndex &index) const;

private slots:
    void currentIndexChanged(int);
    void commitAndCloseEditor();

private:
  TabNetDirectives* tableWidget;
  DlgTieNet* mDlgTieNet;
  bool mApplyNewTieValue;
};

#endif // TABNETDIRECTIVES_H
