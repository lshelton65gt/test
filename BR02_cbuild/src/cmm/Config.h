#ifndef __CMMCONFIG_H__
#define __CMMCONFIG_H__
#include "util/CarbonPlatform.h"

#include <QString>
#include <QStringList>
#include <QPixmap>
#include <QMap>

class CmmConfig
{
public:

  CmmConfig();

  void load();
  void save();

  QByteArray windowGeometry() const { return winGeometry; }
  void setWindowGeometry( const QByteArray &geometry ) { winGeometry = geometry; }

  QByteArray mainWindowState() const { return mainWinState; }
  void setMainWindowState( const QByteArray &state ) { mainWinState = state; }

  static CmmConfig *configuration();
  static CmmConfig *loadConfig(const QString &profileFileName);

private:
  CmmConfig( const CmmConfig &c );
  CmmConfig& operator=( const CmmConfig &c );

  void saveSettings();

private:
  QString home;
  QStringList src;
  QByteArray mainWinState;
  QByteArray winGeometry;
  qreal pointFntSize;
};

#endif // CONFIG_H
