//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtShellTok.h"

#include "InfoTableWidget.h"
#include "RemoteConsole.h"
#include "CarbonConsole.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "Mdi.h"
#include "QTextEditor.h"
#include "HierarchyDockWindow.h"
#include "DlgStringList.h"
#include "CarbonOptions.h"

#include "DirectivesEditor.h"
#include "CarbonDirectives.h"
#include "Directives.h"

#include "gui/CQt.h"

#include "util/OSWrapper.h"
#include "util/UtString.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"
#include "util/CbuildMsgContext.h"


void InfoTableWidget::timeLineFinished()
{
  mTimeLine->toggleDirection();
  mTimeLine->start();
}

InfoTableWidget::InfoTableWidget(QWidget *parent)
: QTableWidget(parent)
{
  mContext = NULL;
  mErrors = 0;
  mInfos = 0;
  mWarnings = 0;
  mFiltered = 0;
  mButtonErrors = NULL;
  mButtonWarnings = NULL;
  mButtonInfos = NULL;
  mShowErrors = true;
  mShowWarnings = true;
  mShowInfos = true;
  mShowFiltered = true;
  mTimeLine = NULL;
  mStatusBar = NULL;

  ui.setupUi(this);


  horizontalHeader()->setResizeMode(InfoTableWidget::Category, QHeaderView::Interactive);
  horizontalHeader()->setResizeMode(InfoTableWidget::DefaultOrder, QHeaderView::Interactive);
  horizontalHeader()->setResizeMode(InfoTableWidget::Description, QHeaderView::Stretch);
  horizontalHeader()->setResizeMode(InfoTableWidget::File, QHeaderView::Interactive);
  horizontalHeader()->setResizeMode(InfoTableWidget::Line, QHeaderView::Interactive);
  horizontalHeader()->setResizeMode(InfoTableWidget::MessageNumber, QHeaderView::Interactive);

  // nuke the first column
  verticalHeader()->close();

  horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);

  mIconErr.addFile(":/cmm/Resources/logError.png");
  mIconWarning.addFile(":/cmm/Resources/logWarning.png");
  mIconInfo.addFile(":/cmm/Resources/logInfo.png");

  setEditTriggers(QAbstractItemView::NoEditTriggers);

  mSeverity = Information;

  readMessageDefinitions(":/cmm/Resources/CompilerMessages.xml");

  createActions();

  CQT_CONNECT(this, customContextMenuRequested(QPoint), this, showContextMenu(QPoint));
  CQT_CONNECT(this, itemSelectionChanged(), this, selectionChanged());

  readCarbonHelp();

  horizontalHeaderItem(Category)->setToolTip("Category");
  horizontalHeaderItem(DefaultOrder)->setToolTip("Default Order");
}

void InfoTableWidget::selectionChanged()
{
  foreach (QTableWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(Qt::UserRole);
    if (!qv.isNull())
    {
      ErrorMessage* errMsg = (ErrorMessage*)qv.value<void*>();
      int msgNumber = errMsg->getMessageNumber();
      if (msgNumber != -1 && msgNumber != 0)
      {
        UtString helpText;
        helpText << mMessageMap[msgNumber];
        emit showExtendedHelp(helpText.c_str());
        break;
      }
    }
  }  
}

// read the carbon help file for extended help
// $CARBON_HOME/userdoc/help/carbon.help

void InfoTableWidget::readCarbonHelp()
{
  const char* releaseLocation = "$CARBON_HOME/userdoc/help/carbon.help";

#if pfLINUX || pfLINUX64
  const char* altLocation = "$CARBON_HOME/tree3/obj/Linux.product/util/carbon.help";
  const char* sandboxLocation = "$CARBON_HOME/obj/Linux.product/util/carbon.help";
#endif

#if pfWINDOWS
  const char* altLocation = "$CARBON_HOME/tree3/obj/Win.product/util/carbon.help";
  const char* sandboxLocation = "$CARBON_HOME/obj/Win.product/util/carbon.help";
#endif

  UtString dummy1, dummy2, dummy3, helpLoc1, helpLoc2, helpLoc3, helpFileName;
  
  OSExpandFilename(&helpLoc1, releaseLocation, &dummy1);
  OSExpandFilename(&helpLoc2, altLocation, &dummy2);
  OSExpandFilename(&helpLoc3, sandboxLocation, &dummy3);

  if (QFile::exists(helpLoc1.c_str()))
    helpFileName = helpLoc1;
  else if (QFile::exists(helpLoc2.c_str()))
    helpFileName = helpLoc2;
  else if (QFile::exists(helpLoc3.c_str()))
    helpFileName = helpLoc3;

  if (helpFileName.length() > 0)
  {
    QFile helpFile(helpFileName.c_str());
  
    if (helpFile.open(QFile::ReadOnly | QFile::Text)) 
    {
      qDebug() << "Reading: " << helpFileName.c_str();
      
      QTextStream in(&helpFile);
      UtString buffer;
      QRegExp messageStart("\\[Help Message # ([0-9]+)\\]$");

      int msgNum = -1;
      while (!in.atEnd()) 
      {
        QString line = in.readLine();
        if (messageStart.exactMatch(line))
        {
          if (msgNum != -1)
          {
            QString value = buffer.c_str();
            mMessageMap[msgNum] = value;
            buffer.clear();
          }
          UtString tmpValue;
          tmpValue << messageStart.cap(1);
          msgNum = atoi( tmpValue.c_str() );
        }
        else
          buffer << line << "\n";
      }

      helpFile.close();
      qDebug() << "Finished Reading: " << helpFileName.c_str();
    }
  }
}
void InfoTableWidget::createActions()
{
  mActionVisit = new QAction(QIcon(":/cmm/Resources/goto.png"), tr("Visit"), this);
  connect(mActionVisit, SIGNAL(triggered()), this, SLOT(menuVisit()));

  mActionShowHierarchy = new QAction(QIcon(":/cmm/Resources/OrgChartHS.png"), tr("Open in Design Hierarchy"), this);
  connect(mActionShowHierarchy, SIGNAL(triggered()), this, SLOT(menuCrossProbe())); 

  mActionClearAll = new QAction(QIcon(":/cmm/Resources/clearall.png"), tr("Clear All"), this);
  connect(mActionClearAll, SIGNAL(triggered()), this, SLOT(menuClearAll())); 

  mActionFilterMsg = new QAction(QIcon(":/cmm/Resources/Filter2HS.png"), tr("Filter Message"), this);
  connect(mActionFilterMsg, SIGNAL(triggered()), this, SLOT(menuFilterMessage())); 

  mActionFilterInst = new QAction(QIcon(":/cmm/Resources/Filter2HS.png"), tr("Filter Instance"), this);
  connect(mActionFilterInst, SIGNAL(triggered()), this, SLOT(menuFilterInstance())); 

}

void InfoTableWidget::menuFilterMessage()
{
  foreach (QTableWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(Qt::UserRole);
    if (!qv.isNull())
    {
      ErrorMessage* em = (ErrorMessage*)qv.value<void*>();
      int msgNum = em->getMessageNumber();
      if (msgNum != -1 && msgNum != 0 && !isFiltered(em))
      {
        QString sMsgNumber;
        sMsgNumber.setNum(msgNum);

        CarbonOptions* options = mContext->getCarbonProjectWidget()->project()->getProjectOptions();
        CarbonPropertyValue* filterValue = options->getValue("Filtered Messages");
        QString filters = filterValue->getValue();
        if (filters.length() > 0)
          filters = filters + ";" + sMsgNumber;
        else
          filters = sMsgNumber;
        options->putValue("Filtered Messages", filters);
        break;
      }
    }
  }
}
void InfoTableWidget::menuFilterInstance()
{
  foreach (QTableWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(Qt::UserRole);
    if (!qv.isNull())
    {
      ErrorMessage* em = (ErrorMessage*)qv.value<void*>();
      const char* src = em->getSource();
      if (src && strlen(src) > 0 && !isFiltered(em))
      {
        UtString msgLocator;
        msgLocator << src << ":" << em->getLineNumber();
        QString msgInstance = msgLocator.c_str();
        UtString out;
        const char* quotedMsgInstance = UtShellTok::quote(msgLocator.c_str(), &out, false, ";");
        CarbonOptions* options = mContext->getCarbonProjectWidget()->project()->getProjectOptions();
        CarbonPropertyValue* filterValue = options->getValue("Filtered Instances");
        QString filters = filterValue->getValue();
        if (filters.length() > 0)
          filters = filters + ";" + QString(quotedMsgInstance);
        else
          filters = quotedMsgInstance;
        options->putValue("Filtered Instances", filters);
        break;
      }
    }
  }
}

void InfoTableWidget::menuClearAll()
{
  clearAll();
}

void InfoTableWidget::menuVisit()
{
  itemDoubleClicked(NULL);
}

void InfoTableWidget::menuCrossProbe()
{
  foreach (QTableWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(Qt::UserRole);
    if (!qv.isNull())
    {
      ErrorMessage* em = (ErrorMessage*)qv.value<void*>();
      const char* ref = em->getReference();
      if (ref != NULL && strlen(ref) > 0)
      {
        HierarchyDockWindow* hierWindow = mContext->getCarbonProjectWidget()->getHierarchyWindow();
        if (hierWindow)
        {
          if (hierWindow->browseNet(ref))
          {
            QDockWidget* dw = mContext->getCarbonProjectWidget()->getHierarchyDockWidget();
            
            if (!dw->isVisible())
              dw->setVisible(true);

            // the next two lines are necessary, even though
            // it seems unnecessary, these two lines cause the tab to "pop" to the top
            // without first calling lower() it doesn't do it every time
            dw->lower();
            dw->raise();
          }
        }
      }
    }
    break;
  }
}

void InfoTableWidget::showContextMenu(const QPoint &pos)
{
  QMenu menu(this);
  menu.addAction(mActionVisit);

  foreach (QTableWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(Qt::UserRole);
    if (!qv.isNull())
    {
      ErrorMessage* em = (ErrorMessage*)qv.value<void*>();
      const char* ref = em->getReference();

      if (em->getMessageNumber() != 0 && em->getMessageNumber() != -1)
        menu.addAction(mActionFilterMsg);

      if (em->getSource() != 0 && strlen(em->getSource()) > 0)
        menu.addAction(mActionFilterInst);

      HierarchyDockWindow* hierWindow = mContext->getCarbonProjectWidget()->getHierarchyWindow();
      if (ref != NULL && strlen(ref) > 0 && hierWindow && hierWindow->browseableNet(ref))
      {
        menu.addAction(mActionShowHierarchy);
        menu.addSeparator();
      }
    }
    break;
  }

  menu.addAction(mActionClearAll);
  menu.exec(viewport()->mapToGlobal(pos));
}
void InfoTableWidget::connectConsole(QPushButton* btnErr, QPushButton* btnWarn, QPushButton* btnInfo, CarbonMakerContext* ctx, CarbonConsole* console, QProgressBar* pb, QStatusBar* sb)
{
  mContext = ctx;
  mProgressBar = pb;
  mButtonErrors = btnErr;
  mButtonWarnings = btnWarn;
  mButtonInfos = btnInfo;
  mStatusBar = sb;

  CQT_CONNECT(console, stdoutChanged(CarbonConsole::CommandType, const QString&, const QString&), this, stdoutChanged(CarbonConsole::CommandType, const QString&, const QString&));
  CQT_CONNECT(console, stderrChanged(CarbonConsole::CommandType, const QString&, const QString&), this, stderrChanged(CarbonConsole::CommandType, const QString&, const QString&));
  CQT_CONNECT(console, makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType));
  CQT_CONNECT(console, compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType));
  CQT_CONNECT(console, compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType));

  CQT_CONNECT(this, itemDoubleClicked(QTableWidgetItem*), this, itemDoubleClicked(QTableWidgetItem*));

  mTimeLine = new QTimeLine(5000, this);
  mTimeLine->setFrameRange(0, 100);

  CQT_CONNECT(mTimeLine, finished(), this, timeLineFinished());
  CQT_CONNECT(mTimeLine, frameChanged(int), mProgressBar, setValue(int));

  mProgressFormat = mProgressBar->format();

  INFO_ASSERT(mContext->getCarbonProjectWidget(), "Expecting Project Widget");
  CQT_CONNECT(mContext->getCarbonProjectWidget(), projectLoaded(const char*, CarbonProject*), this, projectLoaded(const char*, CarbonProject*));
}

void InfoTableWidget::filteredMessagesChanged(const CarbonProperty*, const char*)
{
  toggleDisplay(true, InfoTableWidget::Filtered);
}

// we need to know if the user has changed the filtered messages 
void InfoTableWidget::projectLoaded(const char* /*name*/, CarbonProject* proj)
{
  CarbonOptions* options = proj->getProjectOptions();
  options->registerPropertyChanged("Filtered Messages", "filteredMessagesChanged", this);
  options->registerPropertyChanged("Filtered Instances", "filteredMessagesChanged", this);
}

void InfoTableWidget::itemDoubleClicked(QTableWidgetItem*)
{
  foreach (QTableWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(Qt::UserRole);
    if (!qv.isNull())
    {
      ErrorMessage* em = (ErrorMessage*)qv.value<void*>();
      if (em && strlen(em->getSource()) > 0)
      {
        openSource(em);
        break;
      }
    }
  }
}


InfoTableWidget::~InfoTableWidget()
{
  while (mExpressions.count() > 0)
    delete mExpressions.takeFirst();
}

void InfoTableWidget::clearAll()
{
  while (mErrorMessages.count() > 0)
    delete mErrorMessages.takeFirst();

  setRowCount(0);
  mErrors = 0;
  mInfos = 0;
  mWarnings = 0;
  mFiltered = 0;

  if (mButtonErrors)
    mButtonErrors->setText("0 Errors");
  if (mButtonWarnings)
    mButtonWarnings->setText("0 Messages");
  if (mButtonInfos)
    mButtonInfos->setText("0 Warnings");
}

void InfoTableWidget::compilationStarted(CarbonConsole::CommandMode mode, CarbonConsole::CommandType ctype)
{
  mCompileMode = mode;
  mCommandType = ctype;
#if pfWINDOWS
  if (mode == CarbonConsole::Remote)
    clearAll();
#else
  if (mode == CarbonConsole::Local)
    clearAll();
#endif
}

void InfoTableWidget::makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  mTimeLine->stop();
  mProgressBar->setValue(0);
  mProgressBar->setFormat(mProgressFormat);
  autoFit();
}

void InfoTableWidget::compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  UtString errors, infos, warnings;

  errors << mErrors << " Errors";
  infos << mInfos << " Messages";
  warnings << mWarnings << " Warnings";

  if (mButtonErrors)
    mButtonErrors->setText(errors.c_str());

  if (mButtonInfos)
    mButtonInfos->setText(infos.c_str());

  if (mButtonWarnings)
    mButtonWarnings->setText(warnings.c_str());

  mProgressBar->setValue(0);
  autoFit();
}

//
// horizontalHeader()->setResizeMode(InfoTableWidget::Category, QHeaderView::Interactive);
//  horizontalHeader()->setResizeMode(InfoTableWidget::DefaultOrder, QHeaderView::Interactive);
//  horizontalHeader()->setResizeMode(InfoTableWidget::Description, QHeaderView::Interactive);
//  horizontalHeader()->setResizeMode(InfoTableWidget::File, QHeaderView::Interactive);
//  horizontalHeader()->setResizeMode(InfoTableWidget::Line, QHeaderView::Interactive);
//  horizontalHeader()->setResizeMode(InfoTableWidget::MessageNumber, QHeaderView::Interactive);

void InfoTableWidget::autoFit()
{

  resizeColumnToContents(InfoTableWidget::Category);
  resizeColumnToContents(InfoTableWidget::DefaultOrder);
  resizeColumnToContents(InfoTableWidget::Description);
  resizeColumnToContents(InfoTableWidget::File);
  resizeColumnToContents(InfoTableWidget::Line);
  resizeColumnToContents(InfoTableWidget::MessageNumber);

  //for (int c=0; c<columnCount(); c++)
  //{
  //  QHeaderView::ResizeMode mode = horizontalHeader()->resizeMode(c);
  //  if (mode == QHeaderView::ResizeToContents)
  //  {
  //    resizeColumnToContents(c);
  //  }
  //}
}

bool InfoTableWidget::isFiltered(ErrorMessage* em)
{
  // check message numbers
  CarbonProject* proj = mContext->getCarbonProjectWidget()->project();
  if (proj == NULL)
    return false;

  CarbonOptions* options = proj->getProjectOptions();
  CarbonPropertyValue* filterValue = options->getValue("Filtered Messages");
  const QStringList filters = filterValue->getValues();
  foreach (QString filter, filters)
  {
    UtString sFilter;
    sFilter << filter;
    int msgNum = atoi( sFilter.c_str() );
    if (filter.length() > 0 && em->getMessageNumber() == msgNum)
      return true;
  }

  // Check instances
  CarbonPropertyValue* filterInstValue = options->getValue("Filtered Instances");
  const QStringList filterInsts = filterInstValue->getValues();
  foreach (QString filter, filterInsts)
  {
    if (filter.length() == 0)
      continue;

    QRegExp locator("(.*):([0-9]+)");
    if (locator.exactMatch(filter))
    {
      UtString filterSrc, filterLineNum;
      filterSrc << locator.cap(1);
      filterLineNum << locator.cap(2);
      int filterNum = atoi(filterLineNum.c_str());

      if (0 == strcmp(em->getSource(), filterSrc.c_str()) && filterNum == em->getLineNumber())
        return true;
    }
  }

  return false;
}

void InfoTableWidget::toggleDisplay(bool showIt, Severity sev)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  setRowCount(0);

  switch (sev)
  {
  case Error:       mShowErrors = showIt; break;
  case Warning:     mShowWarnings = showIt; break;
  case Filtered:    mShowFiltered = showIt; break;
  default:
  case Information: mShowInfos = showIt; break;
  }

  foreach (ErrorMessage* em, mErrorMessages)
  {
    MessageExpression::Severity errorSev = em->getSeverity();
    bool skipIt = false;
    switch (errorSev)
    {
    default:
    case MessageExpression::Information:  skipIt = !mShowInfos; break;
    case MessageExpression::Warning:      skipIt = !mShowWarnings; break;
    case MessageExpression::Error:        skipIt = !mShowErrors; break;
    }

    if (skipIt)
      continue;

    populateItem(em, false);
  }

  autoFit();

  QApplication::restoreOverrideCursor();
}

void InfoTableWidget::stderrChanged(CarbonConsole::CommandType ctype, const QString& text, const QString& workingDir)
{
  stdoutChanged(ctype, text, workingDir);
}

QIcon& InfoTableWidget::severityIcon(MessageExpression::Severity sev)
{
  switch (sev)
  {
  case MessageExpression::Unknown: return mIconInfo; break;
  case MessageExpression::Information: return mIconInfo; break;
  case MessageExpression::Warning: return mIconWarning; break;
  case MessageExpression::Error: return mIconErr; break;
  }

  return mIconInfo;
}

QIcon& InfoTableWidget::severityIcon(Severity sev)
{
  switch (sev)
  {
  default:
  case InfoTableWidget::Information: return mIconInfo; break;
  case InfoTableWidget::Warning: return mIconWarning; break;
  case InfoTableWidget::Error: return mIconErr; break;
  }

  return mIconInfo;
}

void InfoTableWidget::populateItem(ErrorMessage* em, bool counted)
{
  bool filtered = isFiltered(em);

  MessageExpression::Severity sev = em->getSeverity();

  if (counted)
  {
    switch (sev)
    {
    case MessageExpression::Warning: mWarnings++; break;
    case MessageExpression::Information: mInfos++; break;
    case MessageExpression::Error: mErrors++; break;
    default:
      return;
      break;
    }

    if (mShowFiltered && filtered)
      mFiltered++;
  }
  UtString errors, infos, warnings;

  errors << mErrors << " Errors";
  infos << mInfos << " Messages";
  warnings << mWarnings << " Warnings";

  if (mButtonErrors)
    mButtonErrors->setText(errors.c_str());

  if (mButtonInfos)
    mButtonInfos->setText(infos.c_str());

  if (mButtonWarnings)
    mButtonWarnings->setText(warnings.c_str());

  // don't show it, it's filtered
  if (mShowFiltered && filtered)
    return;

  int row = rowCount();
  setRowCount(row+1);
  setRowHeight(row, 18);

  // Relate this item to it's ErrorMessage object
  QTableWidgetItem* item = new QTableWidgetItem();
  QVariant qv = qVariantFromValue((void*)em);
  item->setData(Qt::UserRole, qv);
  item->setIcon(severityIcon(sev));
  setItem(row, InfoTableWidget::Category, item);

  UtString rowText;
  rowText << row+1;
  QTableWidgetItem* orderItem = new QTableWidgetItem();
  orderItem->setText(rowText.c_str());
  orderItem->setToolTip(rowText.c_str());
  setItem(row, InfoTableWidget::DefaultOrder, orderItem);

  QTableWidgetItem* descrItem = new QTableWidgetItem();
  descrItem->setText(em->getMessage());
  setItem(row, InfoTableWidget::Description, descrItem);
  descrItem->setToolTip(em->getRawMessage());

  if (strlen(em->getSource()) > 0)
  {
    QFileInfo fi(em->getSource());
    QTableWidgetItem* fileItem = new QTableWidgetItem();
    fileItem->setText(fi.fileName());
    fileItem->setToolTip(fi.fileName());
    setItem(row, InfoTableWidget::File, fileItem);
    fileItem->setToolTip(em->getSource());
  }
  else if (strlen(em->getReference()) > 0)
  {
    QTableWidgetItem* refItem = new QTableWidgetItem();
    refItem->setText(em->getReference());
    refItem->setToolTip(em->getReference());
    setItem(row, InfoTableWidget::File, refItem);
  }

  if (em->getLineNumber() != -1)
  {
    UtString lineNum;
    lineNum << em->getLineNumber();

    QTableWidgetItem* lineNumItem = new QTableWidgetItem();
    lineNumItem->setTextAlignment(Qt::AlignHCenter);
    lineNumItem->setText(lineNum.c_str());
    setItem(row, InfoTableWidget::Line, lineNumItem);
  }

  if (em->getMessageNumber() != -1 && em->getMessageNumber() != 0)
  {
    UtString lineNum;
    lineNum << em->getMessageNumber();

    QTableWidgetItem* item = new QTableWidgetItem();
    item->setTextAlignment(Qt::AlignHCenter);
    item->setText(lineNum.c_str());
    setItem(row, InfoTableWidget::MessageNumber, item);
  }

}

void InfoTableWidget::showContextHelp()
{
}

void InfoTableWidget::keyPressEvent(QKeyEvent* ev)
{
  if (ev->key() == Qt::Key_F1 || ev->key() == Qt::Key_Help)
    showContextHelp();
}

void InfoTableWidget::displayOutput(CarbonConsole::CommandType ctype, const QString& text, const QString& workingDir)
{
  stdoutChanged(ctype, text, workingDir);
}

void InfoTableWidget::stdoutChanged(CarbonConsole::CommandType ctype, const QString& text, const QString& workingDir)
{
  // 0 warnings, 0 errors, and 0 alerts detected.

  //../../../Project1/VicAhbifReg.v:390 Net Vic_carbon.uVicAhbifReg.NxtVICIntEnable: Cycle node 1/1.
  QString str = text;
  QStringList list;

  qDebug() << text;

  QRegExp enterDir("CarbonWorkingDir: (.*)");
  QRegExp enterDir2("Entering directory `(.*)'");
  if (enterDir2.exactMatch(text))
  {
    qDebug() << "Make changing dirs: " << enterDir2.cap(1);
  }

  if (enterDir.exactMatch(text))
  {
    qDebug() << "Entering Directory: " << enterDir.cap(1);
    mCurrentWorkingDir.clear();    
    mCurrentWorkingDir << enterDir.cap(1);
  }

  QRegExp finishedall("^CARBON FINISHED COMPILATION");
  if (finishedall.exactMatch(str))
    mStatusBar->showMessage("Finished", 1000);

  QRegExp startlong("^Start (.*) Generation");
  if (startlong.exactMatch(str))
  {
    mProgressBar->setFormat("");
    mProgressBar->setValue(0);
    mTimeLine->start();
    mStatusBar->showMessage(str, 30000);
  }

  QRegExp finishlong("^Finished (.*) Generation");
  if (finishlong.exactMatch(str))
  {
    mTimeLine->stop();
    mProgressBar->setValue(0);
    mProgressBar->setFormat(mProgressFormat);
    mStatusBar->showMessage(str);
  }

  //#  1  Elapsed: 0:00:00 Complete:  0% 
  QRegExp statx("#\\s*([0-9]+)\\s+Elapsed: [0-9]+:[0-9]+:[0-9]+ Complete:\\s+([0-9]+)%");
  if (statx.exactMatch(str))
  {
    int phase=0;
    UtString cap1;
    cap1 << statx.cap(1);
    UtIStringStream Phase(cap1);
    Phase >> UtIO::dec >> phase;

    int percent=0;
    UtString cap2;
    cap2 << statx.cap(2);
    UtIStringStream Percent(cap2);
    Percent >> UtIO::dec >> percent;

    if (phase == 1 && percent == 0)
      mProgressBar->setRange(0,101); // 101 because of the back-end compilation

    mProgressBar->setValue(percent);
  }
  QRegExp staty(".*Comp:([0-9]+)\\% Mem:.*");
  if (staty.exactMatch(str))
  {
    int percent = staty.cap(1).toInt();
    mProgressBar->setValue(percent);
  }

  if (ctype == CarbonConsole::Compilation)
  {
    //0 warnings, 0 errors, and 0 alerts detected.
    QRegExp sumexp("([0-9]+) warnings, ([0-9]+) errors, and ([0-9]+) alerts detected.");
    if (sumexp.exactMatch(str))
    {
      qDebug() << sumexp.cap(1) << " warnings";
      qDebug() << sumexp.cap(2) << " errors";
      qDebug() << sumexp.cap(3) << " alerts";

      UtString numErrors;
      numErrors << sumexp.cap(2);

      // If we ended with errors, then signal we are done (for remote mode only)
      if (numErrors != "0" && mCompileMode == CarbonConsole::Remote)
        compilationEnded(CarbonConsole::Remote, ctype);
    }
    if (mCompileMode == CarbonConsole::Remote)
    {
      QRegExp exp("Note 111: Successfully created (.*)");
      if (exp.exactMatch(str))
        compilationEnded(CarbonConsole::Remote, ctype);
    }
  }

  for (int i=0; i<mExpressions.count(); i++)
  {
    MessageExpression* exp = mExpressions[i];
    UtString txt; txt << text;
    if (exp->process(txt.c_str()))
    {
      ErrorMessage* em = new ErrorMessage(text, exp);
      UtString wdir; wdir << workingDir;
      em->putWorkingDir(wdir.c_str());
      em->putAlternate(mCurrentWorkingDir.c_str());
      mErrorMessages.push_back(em);
      populateItem(em);
      break;
    }
  }
}


void InfoTableWidget::removeMessage(ErrorMessage* em)
{
  mErrorMessages.removeAt(mErrorMessages.indexOf(em));
}


void InfoTableWidget::openSource(ErrorMessage* em)
{
  const char* src = em->getSource();
  int lineNumber = em->getLineNumber();
  bool navigateFile = true;
 
  UtString filePath;
  CarbonProject* proj = mContext->getCarbonProjectWidget()->project();

  // special handling for Directives editor
  UtString directivesFile;
  directivesFile << proj->getActiveConfiguration() << ".directives";

  if (0 == strcmp(directivesFile.c_str(), src))
  {
    // Opens editor if needed
    DirectivesEditor* dirEditor = mContext->getCarbonProjectWidget()->compilerDirectives();
    if (dirEditor)
    {
      TabNetDirectives* netDirs = dirEditor->getNetDirectivesTab();
      TabModuleDirectives* moduleDirs = dirEditor->getModuleDirectivesTab();

      QRegExp signalExp("No match for signal (\\w+)\\.");
      if (signalExp.exactMatch(em->getMessage()))
      {
        UtString itemName;
        itemName << signalExp.cap(1);

        if (netDirs->selectItem(itemName.c_str()))
        {
          dirEditor->getNetDirectivesTab();
          navigateFile = false;
        }
        else if (moduleDirs->selectItem(itemName.c_str()))
        {
          dirEditor->getModuleDirectivesTab();
          navigateFile = false;
        }
      }
    }
  }
  
  // Still navigate?
  if (navigateFile)
  { 
    QFileInfo fi(src);
    if (fi.isRelative())
    {
      if (strlen(em->getWorkingDir()) > 0)
      {
        UtString f;
        OSConstructFilePath(&f, em->getWorkingDir(), src);
        QFileInfo nfi(f.c_str());
        filePath << nfi.canonicalFilePath();
      }

      if (filePath.length() == 0)
      {
        UtString f;
        OSConstructFilePath(&f, proj->getActive()->getOutputDirectory(), src);
        QFileInfo nfi(f.c_str());
        filePath << nfi.canonicalFilePath();
      }

      if (filePath.length() == 0)
      {
        UtString f;
        OSConstructFilePath(&f, proj->getActive()->getOutputDirectory(), em->getAlternate());
        OSConstructFilePath(&f, f.c_str(), src);
        QFileInfo nfi(f.c_str());
        filePath << nfi.canonicalFilePath();
      }
    }
    else
      filePath << src;

    mContext->getCarbonProjectWidget()->openSource(filePath.c_str(), lineNumber, "TextEditor");
  }
}

bool InfoTableWidget::readMessageDefinitions(const char* xmlFile)
{
  bool status = false;

  QFile xf(xmlFile);
  if (xf.open(QFile::ReadOnly))
  {
    QTextStream ts(&xf);
    QString buffer = ts.readAll();
    UtString buf;
    buf << buffer;

    xmlDocPtr doc = xmlReadMemory(buf.c_str(), buffer.length(), xmlFile, NULL, 0);
    if (doc == NULL) 
    {
      UtString message;
      message << "Failed to parse file " <<  xmlFile;
      //reportError(message.c_str());
    }
    else
    {
      UtXmlErrorHandler eh;
      xmlNode *rootNode = xmlDocGetRootElement(doc);
      INFO_ASSERT(xmlStrcmp(rootNode->name, BAD_CAST "Messages") == 0, "Expecting Messages");

      status = parseXML(rootNode, &eh);
      if (eh.getError())
      {
        //reportError(eh.errorText());
        status = false;
      }

      xmlFreeDoc(doc);
    }
  }
  return status;
}

bool InfoTableWidget::parseXML(xmlNodePtr parent, UtXmlErrorHandler* /*eh*/)
{
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "Message"))
    {
      UtString filter;
      XmlParsing::getContent(child, &filter);
      MessageExpression* exp = MessageExpression::parseXML(child);
      if (exp)
        mExpressions.push_back(exp);
    }
  }

  return true;
}

