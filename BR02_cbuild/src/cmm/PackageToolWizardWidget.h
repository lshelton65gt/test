// -*-C++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#ifndef PACKAGETOOLWIZARDWIDGET_H
#define PACKAGETOOLWIZARDWIDGET_H

#include "util/CarbonPlatform.h"

#include <QWidget>
#include <QString>

#include "util/UtString.h"
#include "CarbonComponent.h"
#include "Mdi.h"
#include "MdiProject.h"
#include "CarbonConsole.h"
#include "RemoteConsole.h"
#include "ui_PackageWizardWidget.h"

class CarbonMakerContext;
class PackageToolComponent;
class CQtContext;
class PackageComponent;
class PackageTreeItem;
class PackageToolWizardWidget;

class PackageToolChangeOutputDirectory
{
public:
  PackageToolChangeOutputDirectory(PackageToolComponent* PackageTool);
  ~PackageToolChangeOutputDirectory();

private:
  UtString mCurrDir;
};

class PackageQTreeWidgetItem : public QObject, public QTreeWidgetItem
{
  Q_OBJECT

public:
  PackageQTreeWidgetItem(PackageTreeItem* item, QTreeWidget* widget, PackageToolWizardWidget* parent);
  void showContextMenu(const QPoint& pos);
  void updateTreeItem(int col);

private:
  PackageTreeItem* mItem;
  QTreeWidget* mWidget;
  PackageToolWizardWidget* mParent;
  QAction* mActionDelete;
  QAction* mActionRename;
  QAction* mActionAddFile;
  QAction* mActionAddDir;

private slots:
  void addFile();
  void addDirectory();
  void renameMe();
  void deleteMe();
};

class PackageFileQTreeWidgetItem : public QTreeWidgetItem {
public:
  PackageFileQTreeWidgetItem(const char* name, const char* filepath, QTreeWidget* widget) : QTreeWidgetItem()
  {
      setText(0, name);
      setText(1, filepath);

      mActionDelete = new QAction(widget);
      mActionDelete->setText("Delete");

  }
  void showContextMenu(const QPoint& pos)
  {
    QMenu menu;
    menu.addAction(mActionDelete);
    menu.setEnabled(true);
    menu.exec(pos);  
  }

private:
  QAction* mActionDelete;

};

class PackageToolWizardWidget : public QWidget, public MDIWidget
{
  Q_OBJECT

public:
  PackageToolWizardWidget(CarbonMakerContext* ctx=0, MDIDocumentTemplate* doct=0, QWidget *parent = 0);
  ~PackageToolWizardWidget();
  bool loadFile(const QString& qFilename);
  virtual const char* userFriendlyName();
  virtual void saveDocument();
  void updateMenusAndToolbars();
  void setComp(PackageComponent* comp);
  void populateTree();
  void populateTreeChildren(QTreeWidgetItem* widgetParent, PackageTreeItem* pktParent);
  CarbonProject* getProject();
  bool validateForPackaging();  // Returns true if we're ready to package


private: // methods
  void initialize();
  void closeEvent(QCloseEvent *event);
  void addRemoveComponent(const char* component, Qt::CheckState state);

private:
  //CQtContext *mCQt;
  CarbonMakerContext* mCtx;
  UtString mFriendlyName;
  PackageComponent* mComp;

  // Instantiate a PackageQTreeWidgetItem used to hold the Root PackageTreeItem
  // and to display and perform context menu action on the top level of the tree
  // This item is actually never instantiated in the QTreeWidget
  PackageQTreeWidgetItem* mRootQItem;

private slots:
  void on_lineEdit_Version_textChanged();
  void on_lineEdit_ShortName_textChanged();
  void on_lineEdit_ProductName_textChanged();
  void on_checkBoxSystemC_stateChanged(int);
  void on_checkBoxCoWare_stateChanged(int);
  void on_checkBoxSoCDesigner_stateChanged(int);
  void on_checkBoxStandAlone_stateChanged(int);
  void computeStandAloneState();
  void fileSave();
  void projectLoaded(const char* fileName, CarbonProject*);
  void dataChanged();
  void configurationChanged(const char*);
  void compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void configurationRemoved(const char*);
  void configurationRenamed(const char*, const char*);
  void configurationCopy(const char*, const char*);
  void configurationAdded(const char* name);
  void showTreeContextMenu(QPoint pos);
  void treeItemChanged(QTreeWidgetItem*, int);
  void componentAdded(CarbonComponent*);
  void componentDeleted(CarbonComponent*);
  void outputFilenameChanged(const CarbonProperty*, const char*);


private:
  Ui::PacketWizardWidget ui;

};

#endif
