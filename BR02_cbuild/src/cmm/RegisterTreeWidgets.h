#ifndef __REGISTERTREEWIDGETS_H__
#define __REGISTERTREEWIDGETS_H__

#include "util/CarbonPlatform.h"

#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"

#include <QtGui>

class SpiritRegister;
class SpiritField;
class CarbonMakerContext;
class RegisterEditorWidget;

class QRegisterTreeWidget : public QTreeWidget
{
  Q_OBJECT

public:
  QRegisterTreeWidget(QWidget* parent=0) : QTreeWidget(parent)
  {
  }
  void setContext(CarbonMakerContext* ctx, RegisterEditorWidget* regEditor)
  {
    mCtx=ctx; 
    mRegEditor=regEditor;
  }

protected:
  virtual void dragEnterEvent(QDragEnterEvent *event);
  virtual void dropEvent(QDropEvent *event);
  virtual void dragMoveEvent(QDragMoveEvent* event);

private:
  CarbonMakerContext* mCtx;
  RegisterEditorWidget* mRegEditor;
};

class QRegisterFieldsTreeWidget : public QTreeWidget
{
  Q_OBJECT

public:
  QRegisterFieldsTreeWidget(QWidget* parent=0) : QTreeWidget(parent)
  {
    mRegister = NULL;
    mTextBrowser = NULL;
    mDropSiteItem = NULL;
    mDropColumnCount = 0;
    mDropItemSelected = false;
    mDefaultBackgroundColor = palette().brush(QPalette::Base).color();
  }

  CarbonDB* getDB();

  void setTextBrowser(QTextBrowser* tb) { mTextBrowser=tb; }
  void setContext(CarbonMakerContext* ctx, RegisterEditorWidget* regEditor)
  {
    mCtx=ctx;
    mRegEditor=regEditor;
  }
  void showStatus(const QString& value);
  void clearStatus();

  bool updateFieldColors(QString& errMsg);
  bool updateFieldColor(QTreeWidgetItem*, QString& errMsg);

  void setDefaultBackgroundColor(const QColor& color) { mDefaultBackgroundColor=color; }

  friend class RegisterFieldsDelegate;

  void deleteSelectedField();

private:
  void animateDropSite(const QPoint& pos);
  void clearAnimation();
  void bindField(QTreeWidgetItem* item, SpiritField* field, const CarbonDBNode* node);
  void resizeColumns();
  bool createNewField(const CarbonDBNode* node);

private:
  QTreeWidgetItem* mDropSiteItem;
# define TDD_MAX_COLUMNS 20 // UtArray is not defined on objects like QColor
  QBrush mDropSaveBrush[TDD_MAX_COLUMNS];
  UInt32 mDropColumnCount;
  bool mDropItemSelected;
  QTextBrowser* mTextBrowser;

public:
  SpiritRegister* getRegister() { return mRegister; }
  void setRegister(SpiritRegister* newVal) { mRegister = newVal; }

private:
  SpiritRegister* mRegister;
  CarbonMakerContext* mCtx;
  RegisterEditorWidget* mRegEditor;
  QColor mDefaultBackgroundColor;

protected:
  virtual void dragEnterEvent(QDragEnterEvent *event);
  virtual void dropEvent(QDropEvent *event);
  virtual void dragMoveEvent(QDragMoveEvent* event);
  virtual void dragLeaveEvent(QDragLeaveEvent* event);

};

class RegisterFieldsDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    RegisterFieldsDelegate(QObject *parent = 0, QRegisterFieldsTreeWidget* t=0, RegisterEditorWidget* w=0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
        const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
        const QModelIndex &index) const;
 
private slots:
    void commitAndCloseEditor();
    void currentIndexChanged(int index);

private:
  QRegisterFieldsTreeWidget* tree;
  RegisterEditorWidget* widget;
};
#endif
