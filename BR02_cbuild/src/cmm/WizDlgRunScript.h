#ifndef WIZDLGRUNSCRIPT_H
#define WIZDLGRUNSCRIPT_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include "KitManifest.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"

#include <QDialog>
#include "ui_WizDlgRunScript.h"

class WizDlgRunScript : public QDialog
{
  Q_OBJECT

public:
  WizDlgRunScript(QWidget *parent = 0, ModelKitWizard* wiz = 0, KitActionRunScript* action = 0);
  ~WizDlgRunScript();

  void applyData(KitActionRunScript* action);
  KitUserAction* createAction();

private:
  Ui::WizDlgRunScriptClass ui;
  ModelKitWizard* mWizard;
  KitActionRunScript* mAction;


private slots:
    void on_buttonBox_rejected();
    void on_buttonBox_accepted();
    void on_pushButtonBrowseScript_clicked();
};

#endif // WIZDLGRUNSCRIPT_H
