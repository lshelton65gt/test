#ifndef __MDIMAXSIM_H__
#define __MDIMAXSIM_H__

#include "util/CarbonPlatform.h"

#include "gui/CQt.h"

#include <QMenuBar>
#include <QProgressBar>
#include <QComboBox>

#include "util/UtString.h"
#include "Mdi.h"
#include "MdiProject.h"
#include "CarbonMakerContext.h"
class MDIWidget;

class MDIMaxsimTemplate : public MDIDocumentTemplate
{
  Q_OBJECT

public:
  enum ToolBars { Basic=0 };

  MDIMaxsimTemplate(CarbonMakerContext* ctx);
 
  virtual MDIWidget* createNewDocument(QWidget* parent=0);
  virtual int createMenus();
  virtual int createToolbars();
  virtual void updateMenusAndToolbars(QWidget* widget);
  virtual MDIWidget* openDocument(QWidget* parent, const char* docName);

private:
  QAction* mActionDelete;

private:
  CarbonMakerContext* mContext;
};
#endif
