#ifndef __CARBONTOOL_H__
#define __CARBONTOOL_H__
#include "util/CarbonPlatform.h"

#include "util/XmlParsing.h"
#include "CarbonProperties.h"

class UtXmlErrorHandler;
class CarbonProperties;
class CarbonOptions;

// Base class for all CarbonTools
class CarbonTool
{
public:
  CarbonTool(const char* toolName, const char* toolFile);
  const char* getName() { return mToolName.c_str(); }
  void putName(const char* newVal) { mToolName=newVal; }
  CarbonProperties* getProperties() { return &mProperties; }
  CarbonOptions* getOptions() { return mOptions; }
  CarbonOptions* getDefaultOptions() { return mDefaultOptions; }

  void putProgram(const char* newVal) { mToolProgram=newVal; }
  const char* getProgram() { return mToolProgram.c_str(); }

  bool serialize(xmlTextWriterPtr writer);
  bool deserialize(xmlNodePtr parent, UtXmlErrorHandler* eh);

protected:
  CarbonProperties mProperties;
  CarbonOptions* mDefaultOptions;
  CarbonOptions* mOptions;

private:
  UtString mToolName;
  UtString mToolProgram;
};

#endif
