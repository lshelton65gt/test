//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "WizardContext.h"
#include "ElaboratedModel.h"
#include "Template.h"
#include "Mode.h"
#include "BlockInstance.h"
#include "PortInstance.h"

Mode* ElaboratedModel::getMode()
{
  return mMode;
}

// Build a collection of blocks with this name
QObject* ElaboratedModel::getBlockInstances(const QString& blockName)
{
  BlockInstances* blocks = new BlockInstances();

  for (int i=0; i<mBlockInstances.getCount(); i++)
  {
    BlockInstance* blockInst = mBlockInstances.getAt(i);
    if (blockInst && blockInst->getBlock()->getName() == blockName)
      blocks->addBlockInstance(blockInst);
  }

  return blocks;
}

QObject* ElaboratedModel::getBlockInstance(const QString& blockName, const QString& instanceID)
{
  for (int i=0; i<mBlockInstances.getCount(); i++)
  {
    BlockInstance* blockInst = mBlockInstances.getAt(i);
    if (blockInst && blockInst->getBlock()->getName() == blockName && blockInst->getInstanceID() == instanceID)
      return blockInst;
  }
  return NULL;
}

QObject* ElaboratedModel::getPortInstances(const QString& blockName, const QString& instanceID)
{
  PortInstances* ports = new PortInstances();
  for (int i=0; i<mPortInstances.getCount(); i++)
  {
    PortInstance* portInst = mPortInstances.getAt(i);
    if (portInst->getBlock()->getName() == blockName && portInst->getBlockInstance()->getInstanceID() == instanceID)
    {
      ports->addPortInstance(portInst);
    }
  }

  return ports;
}


// Allow the user to define script-created objects here.
bool ElaboratedModel::preElaborate(WizardContext* context, Mode*)
{
  // First, wipe out any user script-defined ports
  // from the template, since definePorts will re-build them
  mMode->preElaborate();

  QScriptValueList args;
  args.append(context->getScriptEngine()->newQObject(context));
  QScriptValue value = context->callOptionalScriptFunction("definePorts", args);

  if (value.isBoolean())
    return value.toBoolean();
  else
    return false;
}

bool ElaboratedModel::elaborate(WizardContext* context, Mode* mode)
{
  mMode = mode;

  if (!preElaborate(context, mode))
    return false;

  // Erase these
  mBlockInstances.clear();
  mPortInstances.clear();
  mUniquePortInstances.clear();

  // Elaborate the block
  Blocks* blocks = mode->getBlocks();
  for (int i=0; i<blocks->getCount(); i++)
  {
    Block* block = blocks->getAt(i);

    Parameter* param = block->getParameter();
    if (param) // Then we have 0 to param-1 instances
    {
      for (int pi=0; pi<param->getIntValue(); pi++)
      {
        BlockInstance* blockInst = new BlockInstance(this, block, QString("%1").arg(pi));
        mBlockInstances.addBlockInstance(blockInst);
      }
    }
    else // 1 Instance
    {
      BlockInstance* blockInst = new BlockInstance(this, block); 
      mBlockInstances.addBlockInstance(blockInst);
    }
  }

  QMap<QString, PortInstance*> mapUnique;

  // Elaborate the Ports
  for (int i=0; i<mBlockInstances.getCount(); i++)
  {
    BlockInstance* blockInst = mBlockInstances.getAt(i);
    Ports* ports = blockInst->getPorts();
    if (ports)
    {
      for (int pi=0; pi<ports->getCount(); pi++)
      {
        Port* port = ports->getAt(pi);
        if (port->isEnabled())
        {
          PortInstance* portInst = new PortInstance(port, blockInst, blockInst->getInstanceID());
          mPortInstances.addPortInstance(portInst);
          
          QString expandedName = portInst->getName();
          mapUnique[expandedName] = portInst;
        }
      }
    }
  }

  // now build a uniquePorts list from the map
  QMapIterator<QString, PortInstance *> it(mapUnique);
  while (it.hasNext())
  {
     it.next();
     PortInstance* portInst = it.value();
     if (portInst->getPort()->isEnabled())
      mUniquePortInstances.addPortInstance(portInst);
     else
       qDebug() << "notEnabled" << portInst->getName();
  }

  return true;
}
