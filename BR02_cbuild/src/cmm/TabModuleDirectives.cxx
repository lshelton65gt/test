//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QtGui>

#include "gui/CQt.h"

#include "TabModuleDirectives.h"
#include "DirectivesEditor.h"
#include "CarbonDirectives.h"
#include "DlgSubsModule.h"

TabModuleDirectives::TabModuleDirectives(QWidget *parent)
: QTreeWidget(parent)
{
  mGroup = NULL;
  setWindowTitle("ModuleDirectives[*]");
  setSortingEnabled(false);
  
  setEditTriggers(QAbstractItemView::AllEditTriggers);
  setSelectionMode(QAbstractItemView::ExtendedSelection);
  setItemDelegate(new TabModuleDirectivesDelegate(this, this));
}

void TabModuleDirectives::newDirective()
{
  QTreeWidgetItem* itm = new QTreeWidgetItem(this);
  itm->setFlags(itm->flags() | Qt::ItemIsEditable);

  setCurrentItem(itm);
  addTopLevelItem(itm);
  setModified(true);
}

TabModuleDirectives::~TabModuleDirectives()
{
}

void TabModuleDirectives::dragEnterEvent(QDragEnterEvent *event)
{
  if (event->mimeData()->hasFormat("text/plain"))
    event->acceptProposedAction();
}

void TabModuleDirectives::buildSelectionList(QList<QTreeWidgetItem*>& list)
{
  // The list gives items for every column and row, we just want it row
  // oriented.
  list.clear();

  foreach (QTreeWidgetItem *item, selectedItems())
  {
    QVariant qv = item->data(0, Qt::UserRole);
    if (!qv.isNull())
    {
      list.append(item);
    }
  }  
}

void TabModuleDirectives::populate(CarbonDirectiveGroup* group)
{
  mGroup = group;
  blockSignals(true);

  clear();

  if (group)
  {
    foreach (ModuleDirectives* dir, *mGroup->getModuleDirectives())
      addDirectiveRow(dir);
  }

  header()->resizeSections(QHeaderView::ResizeToContents);

  header()->setResizeMode(TabModuleDirectives::colNAME, QHeaderView::Interactive);

  for (int col=colOTHER; col<columnCount(); col++)
    header()->setResizeMode(col, QHeaderView::Interactive);

  setModified(false);  
  blockSignals(false);
}

void TabModuleDirectives::deleteDirectives()
{
  foreach (QTreeWidgetItem* item, selectedItems())
  {
    for (int col=0; col<columnCount(); col++)
    {
      removeItemWidget(item, col);
    }
    QTreeWidgetItem* parent = item->parent();
    if (parent != NULL) 
    {
      int childIndex = parent->indexOfChild(item);
      parent->takeChild(childIndex);
    }
    else
    {
      int childIndex = indexOfTopLevelItem(item);
      takeTopLevelItem(childIndex);
    }
  }
    
  setModified(true);
}



// Find a directive for a given item
int TabModuleDirectives::findDirective(QTreeWidgetItem* item, const char* dirName)
{
  // check Other Rows
  for (int col=colOTHER; col<columnCount(); col++)
  {
    UtString name;
    name << item->text(col);
    if (item->text(col).startsWith("substituteModule:"))
      name = "substituteModule";
    
    if (item->text(col).startsWith("substituteEntity:"))
      name = "substituteEntity";

    if (name.length() > 0 && name == dirName)
      return col;
  }
  return -1;
}

void TabModuleDirectives::addDirective(QTreeWidgetItem* item, const char* dirName)
{
  for (int col=colOTHER; col<columnCount(); col++)
  {
    UtString name;
    name << item->text(col);
    if (name.length() == 0)
    {
      item->setText(col, dirName);
      setModified(true);
      break;
    }
  }
}

void TabModuleDirectives::setDirective(const QStringList& netNames, const char* dirName, bool setIt)
{
  clearSelection();

  foreach (QString str, netNames)
  {
    UtString net;
    net << str;
    const char* netName = net.c_str();

    if (setIt) // we are setting the directive
    { 
      QTreeWidgetItem* item = addNewRow(netName, false);
      INFO_ASSERT(item, "Expecting item");
      item->setSelected(true);

      int col = findDirective(item, dirName);
      if (col == -1)
        addDirective(item, dirName);
    }
    else
    {
      QTreeWidgetItem* item = findNet(netName);
      if (item)
      {
        item->setSelected(true);
        int col = findDirective(item, dirName);
        if (col != -1)
        {
          item->setText(col, "");
          setModified(true);
        }
      }
    }
  }
}

bool TabModuleDirectives::selectItem(const char* text)
{
  bool selectedSomething = false;

  clearSelection();
  foreach (QTreeWidgetItem* item, findItems(text, Qt::MatchExactly))
  {
    item->setSelected(true);
    selectedSomething = true;
  }

  return selectedSomething;
}

void TabModuleDirectives::saveDocument()
{
  if (mGroup && isWindowModified())
  {
    // First clear the list, then rebuild it
    mGroup->removeAllModuleDirectives();

    for (int row=0; row<topLevelItemCount(); row++)
    {
      QTreeWidgetItem* itm = topLevelItem(row);
      UtString name;
      name << itm->text(TabModuleDirectives::colNAME);

      ModuleDirectives* dir = new ModuleDirectives(name.c_str());
      mGroup->addModuleDirective(dir);

      if (name.length() > 0) // Only save non-empty rows
      {
        // check Other Rows
        for (int col=colOTHER; col<columnCount(); col++)
        {
          if (itm)
          {
            UtString dirName;
            dirName << itm->text(col);
            QString directiveName = itm->text(col);
            if (dirName.length() > 0)
            {
              const char* value = NULL;
              UtString realValue;
              if (directiveName.startsWith("substituteModule:"))
              {
                dirName = "substituteModule";
                QString valueExp = directiveName.mid(strlen("substituteModule:"));
                realValue << valueExp;
                value = realValue.c_str();
              }
              if (directiveName.startsWith("substituteEntity:"))
              {
                dirName = "substituteEntity";
                QString valueExp = directiveName.mid(strlen("substituteEntity:"));
                realValue << valueExp;
                value = realValue.c_str();
              }

              DirectiveDefinition* def = mGroup->getDirectives()->findDefinition(dirName.c_str());
              INFO_ASSERT(def, "Unknown directive name");
              dir->addDirective(new Directive(dirName.c_str(), value, def));
            }
          }
        }
      }
    }
    setWindowModified(false);   
  }
}

void TabModuleDirectives::addDirectiveRow(ModuleDirectives* netDir)
{
  bool updates = updatesEnabled();
  setUpdatesEnabled(false);

  QTreeWidgetItem* itm = new QTreeWidgetItem(this);
  itm->setFlags(itm->flags() | Qt::ItemIsEditable);
  itm->setText(TabModuleDirectives::colNAME, netDir->mLocator.c_str());

  setCurrentItem(itm);
  addTopLevelItem(itm);

  // now check the non-builtin directives
  foreach (Directive* dir, netDir->mDirectives)
  {
    if (!dir->mDefinition->mBuiltin)
    {
      // find an empty column
      for (int col=TabModuleDirectives::colOTHER; col<columnCount(); col++)
      {
        UtString value;
        value << itm->text(col);
        if (value.length() == 0)
        {
          if (dir->mValue.length() > 0)
          {
            QString value = QString("%1:%2").arg(dir->mDirective.c_str()).arg(dir->mValue.c_str());          
            itm->setText(col, value);
          }
          else
            itm->setText(col, dir->mDirective.c_str());
          break;
        }
      }
    }
  }
  setUpdatesEnabled(updates);
}

void TabModuleDirectives::setModified(bool value)
{
  setWindowModified(value);
  mDirectivesEditor->setWindowModified(value);
}

QTreeWidgetItem* TabModuleDirectives::findNet(const char* netName)
{
  for (int row=0; row<topLevelItemCount(); row++)
  {
    QTreeWidgetItem* itm = topLevelItem(row);
    UtString name;
    name << itm->text(TabModuleDirectives::colNAME);
    if (name == netName)
      return itm;
  }
  return NULL;
}


QTreeWidgetItem* TabModuleDirectives::addNewRow(const char* loc, bool exclusiveSelect)
{
  // Already Exists!
  foreach (QTreeWidgetItem* item, findItems(loc, Qt::MatchExactly))
  {
    if (exclusiveSelect)
      setCurrentItem(item);
    return item;
  }

  bool updates = updatesEnabled();
  setUpdatesEnabled(false);

  QTreeWidgetItem* item = NULL;

  item = new QTreeWidgetItem(this);

  item->setText(TabModuleDirectives::colNAME, loc);
  item->setFlags(item->flags() | Qt::ItemIsEditable);

  if (exclusiveSelect)
    setCurrentItem(item);

  addTopLevelItem(item);

  header()->setResizeMode(TabModuleDirectives::colNAME, QHeaderView::ResizeToContents);
  header()->setResizeMode(TabModuleDirectives::colOTHER, QHeaderView::Interactive);

  setModified(true);

  setUpdatesEnabled(updates);

  return item;
}
void TabModuleDirectives::addOrChange(const char* moduleName, const char* dirName, const char* value)
{
  QTreeWidgetItem* moduleItem = addNewRow(moduleName);
  int index = findDirective(moduleItem, dirName);
  if (index == -1)
  {
    UtString v;
    if (value == NULL || strlen(value) == 0)
      v = dirName;
    else
      v << dirName << ":" << value;
    addDirective(moduleItem, v.c_str());
  }
  else
  {
    qDebug() << "update value";
  }
}

void TabModuleDirectives::dropEvent(QDropEvent *event)
{
  event->acceptProposedAction();

  QPoint pos = event->pos();
/*  QTreeWidgetItem* item = itemAt(pos.x(), pos.y()); */

  const QMimeData* mimeData = event->mimeData();
  if (mimeData->hasText())
  {
    QStringList locList = mimeData->text().split("\n");

    clearSelection();
    for (int i = 0; i< locList.size(); ++i)
    {
      UtString value;
      value << locList.at(i);
      QTreeWidgetItem* newItem = addNewRow(value.c_str(), false);
      newItem->setSelected(true);
    }
    setFocus();
  }
}

void TabModuleDirectives::dragMoveEvent(QDragMoveEvent* event)
{
  bool ignore = true;
  const QMimeData* mimeData = event->mimeData();
  if (mimeData->hasText())
  {
    event->acceptProposedAction();
    ignore = false;
  }
  if (ignore)
    event->ignore();
}

// Delegate

// Editor Delegate
TabModuleDirectivesDelegate::TabModuleDirectivesDelegate(QObject *parent, TabModuleDirectives* table)
: QItemDelegate(parent), tableWidget(table)
{
}


void TabModuleDirectivesDelegate::currentIndexChanged(int i)
{
  if (i != -1)
  {
    QComboBox *widget = qobject_cast<QComboBox *>(sender());    

    if (widget->currentText().startsWith("substituteModule"))
    {
      mDlgSubsModule = new DlgSubsModule(tableWidget);
      if (mDlgSubsModule->exec() == QDialog::Accepted)
      {
        UtString value;
        value << "substituteModule:" << mDlgSubsModule->getExpression();
        int index = widget->currentIndex();
        widget->setItemData(index, value.c_str());
      }
    }

    if (widget->currentText().startsWith("substituteEntity"))
    {
      mDlgSubsModule = new DlgSubsModule(tableWidget);
      if (mDlgSubsModule->exec() == QDialog::Accepted)
      {
        UtString value;
        value << "substituteEntity:" << mDlgSubsModule->getExpression();
        int index = widget->currentIndex();
        widget->setItemData(index, value.c_str());
      }
    }


    commitData(widget);
    closeEditor(widget);
  }
}

QWidget *TabModuleDirectivesDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                             const QModelIndex &index) const
{
  if (index.column() == TabModuleDirectives::colNAME)
  {
    QLineEdit *editor = new QLineEdit(parent);
    connect(editor, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
    return editor;
  }

  if (index.column() >= TabModuleDirectives::colOTHER)
  {
    QComboBox *cbox = new QComboBox(parent);
    UtString value;

    QTreeWidgetItem* itm = tableWidget->topLevelItem(index.row());
    if (itm)
      value << itm->text(index.column());

    QString v = value.c_str();

    foreach (DirectiveDefinition* def, *tableWidget->mGroup->getDirectives()->getDefinitions())
    {
      if (!def->mBuiltin && (def->mMode == eDirModule || def->mMode == eDirBoth))
      {
        QString name = def->mName.c_str();
        cbox->addItem(def->mName.c_str());
        if (name == "substituteModule" && v.startsWith("substituteModule:"))
        {
          value = "substituteModule";
          int ci = cbox->findText(value.c_str());
          cbox->setItemData(ci, v.mid(strlen("substituteModule:")));
        }        
        if (name == "substituteEntity" && v.startsWith("substituteEntity:"))
        {
          value = "substituteEntity";
          int ci = cbox->findText(value.c_str());
          cbox->setItemData(ci, v.mid(strlen("substituteEntity:")));
        }        
      }
    }

    cbox->model()->sort(0);
    cbox->insertItem(0, "<Nothing>");

    if (itm)
    {
      int ci = cbox->findText(value.c_str());
      if (ci != -1)
      {
        cbox->setCurrentIndex(ci);
        if (v.startsWith("substituteModule:"))
        {
          QString valueExp = v.mid(strlen("substituteModule:"));
          QStringList tokens = valueExp.split(':');
          if (tokens.count() > 1)
          {
            DlgSubsModule dlg(parent);
            dlg.setExpression(valueExp);

            int dlgResult = dlg.exec();
            if (dlgResult == QDialog::Accepted && dlg.getResult() == DlgSubsModule::eSubsAccepted)
            {
              UtString value;
              value << "substituteModule:" << dlg.getExpression();
              cbox->setItemData(ci, value.c_str());
            }
            else if (dlg.getResult() == DlgSubsModule::eSubsDelete)
              cbox->setCurrentIndex(cbox->findText("<Nothing>"));
            else if (dlg.getResult() == DlgSubsModule::eSubsCancelled)
              return NULL;
          }
        }
        if (v.startsWith("substituteEntity:"))
        {
          QString valueExp = v.mid(strlen("substituteEntity:"));
          QStringList tokens = valueExp.split(':');
          if (tokens.count() > 1)
          {
            DlgSubsModule dlg(parent);
            dlg.setExpression(valueExp);

            int dlgResult = dlg.exec();
            if (dlgResult == QDialog::Accepted && dlg.getResult() == DlgSubsModule::eSubsAccepted)
            {
              UtString value;
              value << "substituteEntity:" << dlg.getExpression();
              cbox->setItemData(ci, value.c_str());
            }
            else if (dlg.getResult() == DlgSubsModule::eSubsDelete)
              cbox->setCurrentIndex(cbox->findText("<Nothing>"));
            else if (dlg.getResult() == DlgSubsModule::eSubsCancelled)
              return NULL;
          }
        }
      }
      else
        cbox->setCurrentIndex(cbox->findText("<Nothing>"));
    }
    else
      cbox->setCurrentIndex(cbox->findText("<Nothing>"));
    
    CQT_CONNECT(cbox, currentIndexChanged(int), this, currentIndexChanged(int));

    return cbox;
 
  }

  return NULL;
}

void TabModuleDirectivesDelegate::commitAndCloseEditor()
{
  QLineEdit *editor = qobject_cast<QLineEdit *>(sender());
  emit commitData(editor);
  emit closeEditor(editor);
}

void TabModuleDirectivesDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit)
  {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }
}

void TabModuleDirectivesDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                         const QModelIndex &index) const
{
  QTreeWidgetItem* item = tableWidget->currentItem();
  if (item == NULL || tableWidget->signalsBlocked())
    return;

  UtString editText;

  if (index.column() == TabModuleDirectives::colNAME)
  {
    QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
    if (edit)
    {
      editText << edit->text();
      UtString currText;
      currText << item->text(TabModuleDirectives::colNAME);
      model->setData(index, edit->text());

      if (currText != editText)
      {
        tableWidget->setModified(true);
        tableWidget->mDirectivesEditor->signalDirectivesChanged(eDirectiveModule);
      }
    }
  }

  if (index.column() >= TabModuleDirectives::colOTHER)
  {
    QComboBox* cbox = qobject_cast<QComboBox *>(editor);
    if (cbox != NULL)
    {
      UtString value;
      QString dirName = cbox->currentText();
      if (0 == dirName.compare("substituteModule") || 0 == dirName.compare("substituteEntity"))
      {
        QVariant vs = cbox->itemData(cbox->currentIndex());
        value << vs.toString();
      }
      else
        value << cbox->currentText();
      if (value == "<Nothing>")
        model->setData(index, "");
      else
        model->setData(index, value.c_str());

      tableWidget->setModified(true);
      tableWidget->mDirectivesEditor->signalDirectivesChanged(eDirectiveModule);
    }
  }
}
