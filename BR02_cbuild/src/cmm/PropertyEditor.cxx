//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "gui/CQt.h"
#include "PropertyEditor.h"
#include "CarbonProject.h"
#include "CarbonCompilerTool.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "DlgStringList.h"


#define PROP_CARBON_EDIT "CarbonEditBox"
#define PROP_CARBON_FILTER "CarbonFileFilter"
#define PROP_CARBON_FILE "CarbonEditFileName"
#define PROP_CARBON_ITEM "CarbonFileWidget"
#define PROP_CARBON_SWITCHVALUE "CarbonPropertyValue"
#define ASSOCIATED_EDITOR "AssociatedEditor"
#define PROP_CARBON_DELEGATE "CarbonPropertyDelegate"

class PropertyTreeWidgetItem : public QTreeWidgetItem
{
public:
  void putPropertyContext(CarbonPropertyContext* ctx) { mPropertyContext=ctx; }
  CarbonPropertyContext* getPropertyContext() { return mPropertyContext; }

  PropertyTreeWidgetItem(QTreeWidgetItem* parent, CarbonPropertyContext* propContext) : QTreeWidgetItem(parent) 
  {
    mPropertyContext=propContext;
  }

  virtual ~PropertyTreeWidgetItem()
  {
   if (mPropertyContext)
     delete mPropertyContext;
   mPropertyContext=NULL;
  }

private:
  CarbonPropertyContext* mPropertyContext;
};

PropertyEditor::PropertyEditor(QWidget *parent)
: QTreeWidget(parent)
{
  mContext = NULL;
  mSettings = NULL;
  mShutdown = false;
  mEditWidget = NULL;
  mLastSortType = PropertyEditor::Categorized;
  mSelectedItem = NULL;

  headerItem()->setText(colNAME, "Name");
  headerItem()->setText(colVALUE, "Value");

  setEditTriggers(QAbstractItemView::AllEditTriggers);

  header()->setResizeMode(colNAME, QHeaderView::Interactive);
  header()->setResizeMode(colVALUE, QHeaderView::Interactive);
 
  mDelegate = new PropertyEditorDelegate(this,this);
  setItemDelegate(mDelegate);

  CQT_CONNECT(this, itemClicked(QTreeWidgetItem*,int), this, itemClicked(QTreeWidgetItem*,int));
  CQT_CONNECT(this, itemSelectionChanged(), this, itemSelectionChanged());

}
void PropertyEditor::itemSelectionChanged()
{
  mSelectedItem = NULL;

  foreach (QTreeWidgetItem* item, selectedItems())
  {
    QVariant qv = item->data(PropertyEditor::colNAME, Qt::UserRole);
    if (!qv.isNull())
    {
      mSelectedItem = item;
      QFont f = item->font(1);
      emit resetButtonStateChanged(true, f.bold());
      break; // single selection
    }
  }

  qDebug() << "selection changed";
}

// If a user clicks on the first column, 
void PropertyEditor::itemClicked(QTreeWidgetItem* item, int col)
{
  if (item && item->flags() & Qt::ItemIsEditable && col == 0)
    editItem(item, colVALUE);
}

PropertyEditor::~PropertyEditor()
{
  mShutdown = true;
}

void PropertyEditor::sortAlphabetical()
{
  if (mSettings)
    populate(mSettings, PropertyEditor::Alphabetical);
}

void PropertyEditor::resetSwitch()
{
  if (mSelectedItem)
  {
    QVariant qv = mSelectedItem->data(PropertyEditor::colNAME, Qt::UserRole);
    if (!qv.isNull())
    {
      CarbonPropertyContext* propContext = (CarbonPropertyContext*)qv.value<void*>();
      mSelectedItem = NULL;
      resetSwitchValue(propContext->getProperty(), propContext->getPropValue());
      bool oldValue = blockSignals(true);
      bool someBold = populate(mSettings, mLastSortType);
      blockSignals(oldValue);
      emit resetButtonStateChanged(someBold, false);
    }
  }
}

void PropertyEditor::resetSwitchValue(CarbonProperty* sw, CarbonPropertyValue* sv)
{
  // skip the -o switch we don't want to mess with that one.
  if (0 == strcmp(sw->getName(), "-o"))
    return;

  if (sw->getResetTo() == CarbonProperty::eResetGUIDefault)
    mSettings->putValue(sw, sw->getGuiDefaultValue());
  else if (sw->getResetTo() == CarbonProperty::eResetNone)
  {
    qDebug() << "reset blocked for property" << sv->getProperty()->getName();
  }
  else if (sw->getResetTo() == CarbonProperty::eResetDefault)
  {
    CarbonOptions* switchOptions = sv->getOptions();
    const char* pv = NULL;
    if (switchOptions)
      pv = switchOptions->getParentValue(sv->getProperty()->getName());
    
    // Some properties are only defined at this level
    // If it is, reset it to the default, otherwise it will
    // inherit the parent's value.
    if (pv)
      sv->getOptions()->removeValue(sw->getName());
    else
      mSettings->putValue(sw, sw->getDefaultValue());
  }
}

void PropertyEditor::resetAllSwitches()
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  if (mSettings)
  {
    const CarbonProperties* sws = mSettings->getProperties();
    for (UInt32 gn=0; gn<sws->numGroups(); ++gn)
    {
      CarbonPropertyGroup* group = sws->getGroup(gn);    
      for (CarbonPropertyGroup::SwitchLoop p = group->loopSwitches(); !p.atEnd(); ++p)
      {
        CarbonProperty* sw = p.getValue();
        bool isDefaulted, isThisLevel;
        CarbonPropertyValue* sv = mSettings->getValue(sw,&isDefaulted,&isThisLevel);
        UtString value;
        value << sv->getValue();
        if (isThisLevel && !sw->getDisplayOnly() && !sw->getReadOnly() && value.length() > 0)
          resetSwitchValue(sw, sv);
      }
    }
  }

  bool oldValue = blockSignals(true);
  bool someBold = populate(mSettings, mLastSortType);
  blockSignals(oldValue);

  QApplication::restoreOverrideCursor();

  mEditWidget = NULL;

  emit resetButtonStateChanged(someBold, false);
}

void PropertyEditor::sortCategorized()
{
  if (mSettings)
    populate(mSettings, PropertyEditor::Categorized);
}

void PropertyEditor::commitEdit()
{
  if (mEditWidget)
    mDelegate->commit(mEditWidget);
  mEditWidget = NULL;
}
bool PropertyEditor::populate(CarbonOptions* settings, SortType sortType)
{
  mSettings = settings;
  mLastSortType = sortType;
  mEditWidget = NULL;

  clear();

  // Nothing to show
  if (mSettings == NULL)
    return false;

  bool someBold = false;
  const CarbonProperties* sws = settings->getProperties();

  for (UInt32 gn=0; gn<sws->numGroups(); ++gn)
  {
    CarbonPropertyGroup* group = sws->getGroup(gn);
    QTreeWidgetItem* itmGroup = NULL;

    if (sortType == PropertyEditor::Categorized)
    {
      itmGroup = new QTreeWidgetItem();

      itmGroup->setText(colNAME, group->getName());
      QColor buttonColor = palette().brush(QPalette::Button).color();

      itmGroup->setBackgroundColor(colNAME, buttonColor);
      itmGroup->setBackgroundColor(colVALUE, buttonColor);

      addTopLevelItem(itmGroup);
    }

    for (CarbonPropertyGroup::SwitchLoop p = group->loopSwitches(); !p.atEnd(); ++p)
    {
      CarbonProperty* sw = p.getValue();

      bool tmp1,tmp2;
      CarbonPropertyValue* svtest = settings->getValue(sw,&tmp1,&tmp2);
      if (svtest == NULL)
      {
        qDebug() << "no switch named" << sw->getName();
        continue;
      }

      PropertyTreeWidgetItem* itm = new PropertyTreeWidgetItem(itmGroup, NULL);

      UtString displayName;
      if (sw->getReadOnly())
        displayName << "(" << sw->getName() << ")";
      else
        displayName = sw->getName();

      itm->setText(colNAME, displayName.c_str());

      if (sortType == PropertyEditor::Alphabetical)
        addTopLevelItem(itm);

      bool isDefaulted, isThisLevel;
      CarbonPropertyValue* sv = settings->getValue(sw->getName(),&isDefaulted,&isThisLevel);

      itm->setText(colVALUE, sv->getValue());
      itm->setToolTip(colVALUE, sv->getValue());

      CarbonDelegate* delegate = NULL;
      if (!sw->getReadOnly())
      {
        delegate = sw->createDelegate(this, mDelegate, this);
        if (delegate)
          itm->setFlags(itm->flags() | Qt::ItemIsEditable);
      }

      CarbonPropertyContext* pc = new CarbonPropertyContext(settings, sw, sv, delegate, itm);

      QVariant qvContext = qVariantFromValue((void*)pc);
      itm->setData(colVALUE, Qt::UserRole, qvContext);

      itm->putPropertyContext(pc);

      // This property might not have a parent value
      // so we must be able to handle that.
      bool differentFromParent = false;
      UtString parentValue;
      CarbonPropertyValue* pv = NULL;
      if (settings->getParent() && settings->getParent()->findProperty(sw->getName()))
        pv = settings->getParent()->getValue(sw->getName());

      // it has a parent value
      if (pv)
      {
        parentValue << pv->getValue();
        if (0 != strcmp(parentValue.c_str(), sv->getValue()))
          differentFromParent = true;
      }
      
      bool internalProperty = sw->getReadOnly() || sw->getDisplayOnly();
      if ( !internalProperty && (isThisLevel || differentFromParent))
      {
        QFont f = itm->font(1);
        f.setBold(true);
        someBold = true;
        itm->setFont(colVALUE, f);
      }

      QVariant qv = qVariantFromValue((void*)pc);
      itm->setData(colNAME, Qt::UserRole, qv);
    }

    if (sortType == PropertyEditor::Alphabetical)
    {
      setRootIsDecorated(false);    
      sortItems(0, Qt::AscendingOrder);
    }
    else if (sortType == PropertyEditor::Categorized && itmGroup)
    {
      itmGroup->setExpanded(true);
      setRootIsDecorated(true); 
      sortItems(0, Qt::AscendingOrder);
      expandAll();
    }
    resizeColumnToContents(0);
  }

  emit resetButtonStateChanged(someBold, false);

  return someBold;
}

CarbonPropertyContext::CarbonPropertyContext(CarbonOptions* options, CarbonProperty* prop, CarbonPropertyValue* propValue, CarbonDelegate* propDelegate, PropertyTreeWidgetItem* treeItem) :
  mOptions(options),
  mProperty(prop),
  mPropertyValue(propValue),
  mDelegate(propDelegate),
  mTreeItem(treeItem)
{
  // Register for property changes, so we can keep the tree
  // updated if someone calls putValue() on this item
  mOptions->registerPropertyChanged(prop->getName(), "propChanged", this);
  connect(mOptions, SIGNAL(destroyed(QObject*)), this, SLOT(destroyed(QObject*)));
}

CarbonPropertyContext::~CarbonPropertyContext()
{
  // Tree item is being destroyed, unregister for changes
  if (mOptions)
    mOptions->unregisterPropertyChanged(mProperty->getName(), this);
}

void CarbonPropertyContext::destroyed(QObject* /*obj*/)
{
  // Tree item is being destroyed, unregister for changes
  if (mOptions)
    mOptions->unregisterPropertyChanged(mProperty->getName(), this);

  // mOptions will be invalid later on in the destructor 
  // because we are shutting down, so guard against accessing it.
  mOptions = NULL;
}

void CarbonPropertyContext::propChanged(const CarbonProperty* /*prop*/, const char* newValue)
{
  //qDebug() << "refreshing tree: " << prop->getName() << " : " << newValue;
  mTreeItem->setText(1, newValue);
}

PropertyEditorDelegate::PropertyEditorDelegate(QObject *parent, PropertyEditor* editor)
: QItemDelegate(parent), mEditor(editor)
{
  mEditor->putEditWidget(NULL);
}

QWidget* PropertyEditorDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem& /*option*/, const QModelIndex &index) const
{
  QTreeWidgetItem* item = mEditor->itemFromIndex(index);

  if (index.column() == PropertyEditor::colVALUE)
  {
    QVariant qv = item->data(PropertyEditor::colNAME, Qt::UserRole);
    CarbonPropertyContext* propContext = (CarbonPropertyContext*)qv.value<void*>();
    if (propContext)
    {
      QWidget* editWidget = propContext->getDelegate()->createEditor(parent, item, propContext->getProperty(), index);
      mEditor->putEditWidget(editWidget);
      return editWidget;
    }
    else
      return NULL;
  }
  else
    return NULL;
 }

void PropertyEditorDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
  if (!mEditor->signalsBlocked())
  {
    QTreeWidgetItem* item = mEditor->itemFromIndex(index);
    QVariant qv = item->data(PropertyEditor::colNAME, Qt::UserRole);
    CarbonPropertyContext* propContext = (CarbonPropertyContext*)qv.value<void*>();
    
    propContext->getDelegate()->setModelData(editor,item,propContext->getProperty(),model,index);
  }
  mEditor->putEditWidget(NULL);
}

void PropertyEditorDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QTreeWidgetItem* item = mEditor->itemFromIndex(index);
  QVariant qv = item->data(PropertyEditor::colNAME, Qt::UserRole);
  CarbonPropertyContext* propContext = (CarbonPropertyContext*)qv.value<void*>();
  
  propContext->getDelegate()->setEditorData(editor, item, propContext->getProperty(), index);
}

void PropertyEditorDelegate::setValue(QTreeWidgetItem* item, QAbstractItemModel *model, const QModelIndex &index, const char* currentValue, const char* newValue, const CarbonProperty* sw) const
{
  if (0 != strcmp(currentValue, newValue))
  {
    QFont f = item->font(PropertyEditor::colVALUE);
    f.setBold(true);
    item->setFont(PropertyEditor::colVALUE, f);
    item->setToolTip(PropertyEditor::colVALUE, newValue);
    mEditor->getSettings()->putValue(sw, newValue);
    model->setData(index, newValue);
  }
}

