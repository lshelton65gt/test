//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "ParameterEditWidget.h"

#include <QtGui>


ParameterEditWidget::ParameterEditWidget(QWidget *parent, Qt::WFlags flags)
: QWidget(parent, flags)
{
  ui.setupUi(this);
  setVisible(true);
  ui.enumChoiceWidget->setItems(QStringList(),0);

  on_checkBoxEnumValues_stateChanged(0);
}

ParameterEditWidget::~ParameterEditWidget()
{

}
void ParameterEditWidget::on_checkBoxEnumValues_stateChanged(int)
{
  bool is_enum = ui.checkBoxEnumValues->isChecked();

  if (is_enum && ui.enterDefaultLineEdit->isModified() ) {
    ui.enumChoiceWidget->setItems(QStringList(ui.enterDefaultLineEdit->text()),0);
  } else if (!is_enum) {
    ui.enumChoiceWidget->setDefaultText(QString()); // unsets the default
  }

  ui.enumChoiceWidget->setVisible(is_enum);
  ui.enterDefaultLineEdit->setVisible(!is_enum);
}




void ParameterEditWidget::setIsEnumerated()
{
  ui.checkBoxEnumValues->setChecked(true);
  on_checkBoxEnumValues_stateChanged(0);
}

bool ParameterEditWidget::isEnumerated()
{
  return ui.checkBoxEnumValues->isChecked();
}


void ParameterEditWidget::setChoices(const QStringList& choices)
{
  bool is_enum = ui.checkBoxEnumValues->isChecked();
  if ( is_enum ) {
    ui.enumChoiceWidget->clearFocus();
    ui.enumChoiceWidget->setItems(choices,0);
  }
}

QStringList ParameterEditWidget::choices()
{
  bool is_enum = ui.checkBoxEnumValues->isChecked();
  if ( is_enum ) {
    return ui.enumChoiceWidget->getItems();
  } else {
    return QStringList(ui.enterDefaultLineEdit->text());
  }
}

void ParameterEditWidget::setDefaultText(const QString& value)
{
  bool is_enum = ui.checkBoxEnumValues->isChecked();
  if ( is_enum ) {
    ui.enumChoiceWidget->setDefaultText(value);
  } else {
    ui.enterDefaultLineEdit->setText(value);
  }
}

QString ParameterEditWidget::defaultText()
{
  bool is_enum = ui.checkBoxEnumValues->isChecked();
  if ( is_enum ) {
    return ui.enumChoiceWidget->defaultText();
  } else {
    return ui.enterDefaultLineEdit->text();
  }
}
