#ifndef __MEMEDITORTREENODES_H__
#define __MEMEDITORTREENODES_H__

#include "util/CarbonPlatform.h"
#include "util/CarbonAssert.h"
#include "cfg/CarbonCfg.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/UtStringArray.h"
#include "util/UtStringUtil.h"
#include "util/DynBitVector.h"
#include "gui/CHexSpinBox.h"

#include "MemEditorTreeWidget.h"
#include "CompWizardMemEditor.h"
#include "CarbonConsole.h"
#include "DlgCodeEditor.h"
#include "DlgSystemAddressMapping.h"

class ESLMemory;
class CarbonConsole;

ESLMemory* findESLMemory(MemEditorTreeItem* node);
ESLMemory* findESLMemory(MemEditorTreeWidget* tree, CarbonCfgMemory* mem);
ESLMemory* findESLMemory(MemEditorTreeWidget* tree, const QString& compName, const QString& memName);

bool CheckBlockLocation(QTreeWidgetItem* parentItem, MemEditorTreeItem* thisNode, CarbonCfg* ccfg, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc, bool validate);
bool CheckBlock(QTreeWidgetItem* parentItem, MemEditorTreeItem* thisNode, CarbonCfg* ccfg, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, bool validate);

quint64 ConvertStringToUInt64(const QString& v);
qint64 ConvertStringToSInt64(const QString& v);
quint32 ConvertStringToUInt32(const QString& v);


struct _AddressMapping
{
  bool mPortMapped;
  QString mPortName;
  qint64 mBaseAddress;
  bool mModified;

  void setMapped(bool v) { mPortMapped = v; }
  void setModified(bool v) { mModified = v; }
  void setBaseAddress(qint64 v) { mBaseAddress = v; }

  _AddressMapping(const _AddressMapping& a)
  {
    mPortMapped = a.mPortMapped;
    mPortName = a.mPortName;
    mBaseAddress = a.mBaseAddress;
    mModified = a.mModified;
  }

  _AddressMapping()
  {
    mPortMapped = false;
    mModified = false;
    mBaseAddress = 0;
  }

  _AddressMapping(const QString& name)
  {
    mPortName = name;
    mPortMapped = false;
    mBaseAddress = 0;
    mModified = false;
  }

  ~_AddressMapping()
  {
    qDebug() << "~_AddressMapping" << mPortName;
  }
};

class SignedInt64Validator : public QValidator 
{
 public: 
  SignedInt64Validator(QObject* parent)
    : QValidator(parent),
      mRegExp("[-]*([0-9]+|0x[0-9,a-f,A-F]+)"),
      mRegExpValidator(mRegExp, parent)
  {
  }
 
  //! Perform string validation
  QValidator::State validate(QString & rawInput, int & pos) const
  {
    QString input = rawInput.trimmed().toLower();

    // First use the RegExp validator to check it it's legal characters
    QValidator::State state = mRegExpValidator.validate(input, pos);

    // If its a legal value, check the range
    if(state == QValidator::Acceptable) 
    {
      // First check if value is decimal
      bool ok = false;
      qint64 val = input.toLongLong(&ok);
      
      if(!ok) 
      {
        bool isHexNegative = input.startsWith("-0x");
        bool isHexPositive = input.startsWith("0x");

        if (isHexNegative || isHexPositive)
        {
          int startPos = input.length()-2;
          if (isHexNegative)
            startPos = input.length()-3;

          QString hexValue = input.right(startPos);          
          val = hexValue.toLongLong(&ok, 16);
        }
      }   

      // If both conversions failed..
      if (!ok)
        state = QValidator::Invalid;
    }

    return state;
  }

private:
  QRegExp            mRegExp;
  QRegExpValidator   mRegExpValidator; 
};

class HexDecValidator : public QValidator {
 public:
  //! Constructor
  /*
    \param parent Parent Widget
    \param min Minimum value allowed, -1 means no minimum.
    \param min Maximum value allowed, -1 means no maximum.
  */
  HexDecValidator(QObject* parent, unsigned long long min = 0, unsigned long long max = 0xffffffffffffffffLL)
    : QValidator(parent),
      mRegExp("[0-9]+|0x[0-9,a-f,A-F]+"),
      mRegExpValidator(mRegExp, parent),
      mMin(min),
      mMax(max)
  {
  }
  
  //! Set Valid Range
  /*
    \param min Minimum value allowed, -1 means no minimum.
    \param min Maximum value allowed, -1 means no maximum.
  */
  void setRange(unsigned long long min, unsigned long long max)
  {
    mMin = min;
    mMax = max;
  }

  //! Perform string validation
  QValidator::State validate(QString & input, int & pos) const
  {
    // First use the RegExp validator to check it it's legal characters
    QValidator::State state = mRegExpValidator.validate(input, pos);

    // If its a legal value, check the range
    if(state == QValidator::Acceptable) {
      // First check if value is decimal
      bool ok = false;
      unsigned long long val = input.toULongLong(&ok);
      
      // If ok is true, the value must have been decimal, else it must be hexadecimal
      // It already passed the Dec or Hex validator, so there are no other posibilities
      if(!ok) {
        val = input.right(input.length()-2).toULongLong(&ok, 16);
      }
      
      // now check the values
      if(val < mMin || val > mMax)
        state = QValidator::Invalid;
    }

    return state;
  }

private:
  QRegExp            mRegExp;
  QRegExpValidator   mRegExpValidator;
  unsigned long long mMin;
  unsigned long long mMax;
};

class MemoryDropItem : public QObject, public MemEditorTreeItem
{
  Q_OBJECT
  // Overrides
  virtual bool canDropHere(const QString&, QDragMoveEvent*);
  virtual bool dropItem(const QString&);

public:
  MemoryDropItem(QTreeWidget* parent, CarbonCfg* cfg) : MemEditorTreeItem(MemEditorTreeItem::MemoriesDropItem, parent)
  {
    mCfg = cfg;
    setItalicText(0, "Drop new memories here");
  }

private:
  CarbonCfg* mCfg;
};

class ESLMemoryBlock;


class BlocksRootActions : public QObject
{
  Q_OBJECT

public:
  enum Action { AddBlock }; 

  BlocksRootActions(MemEditorTreeWidget* tree);

  QAction* getAction(Action);

private slots:
  void actionAddBlock();
 
private:
  MemEditorTreeWidget* mTree;
  QAction* mAddBlock;
};

class CustomCodeSectionNode : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0} ;
  CustomCodeSectionNode(QTreeWidgetItem* parent, CarbonCfgCustomCode* code)
    : MemEditorTreeItem(MemEditorTreeItem::CustomCodeSection, parent)
  {
    mCustomCode = code;

    switch (code->getPosition())
    {
    case eCarbonCfgPre:   setBoldText(colNAME, "Pre"); break;
    case eCarbonCfgPost:  setBoldText(colNAME, "Post"); break;
    }    
  }
private:
  CarbonCfgCustomCode* mCustomCode;
};



class CustomCodesRootNode : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0} ;
  enum CustomCodeKind { Memory, MemoryBlock, Cadi, Component, SubComponent };
  enum Action { AddSection }; 

  // ctor - for A Memory Block
  CustomCodesRootNode(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockCustomCodes, parent)
  {
    init();
    mKind = MemoryBlock;
    mMemory = mem;
    mBlock = block;
    populateMemoryBlockCodes();
  }
 
  CustomCodesRootNode(QTreeWidgetItem* parent, CarbonCfgMemory* mem)
    : MemEditorTreeItem(MemEditorTreeItem::MemCustomCodes, parent)
  {
    init();
    mMemory = mem;
    mKind = Memory;
    populateMemoryCodes();
  }

  CustomCodesRootNode(QTreeWidgetItem* parent, CarbonCfg* comp)
    : MemEditorTreeItem(MemEditorTreeItem::CompCustomCodes, parent)
  {
    init();
    mComponent = comp;
    populateComponentCodes();
  }



  CustomCodesRootNode(QTreeWidgetItem* parent, CustomCodeKind kind)
    : MemEditorTreeItem(MemEditorTreeItem::MemCustomCodes, parent)
  {
    init();
    mKind = kind;
  }
 
  void init()
  {
    mComponent = NULL;
    mMemory = NULL;
    mBlock = NULL;

    setBoldText(colNAME, "Custom Code Sections");
    setExpanded(false);
  }

private:
  CustomCodeKind mKind;
  MemEditorTreeWidget* mTree;

private:
  void populateMemoryCodes();
  void populateMemoryBlockCodes();
  void populateComponentCodes();

  CarbonCfgMemoryCustomCode* findMemoryCustomCode(CarbonCfgCustomCodePosition pos, CarbonCfgMemoryCustomCodeSection sec);
  CarbonCfgMemoryBlockCustomCode* findMemoryBlockCustomCode(CarbonCfgCustomCodePosition pos, CarbonCfgMemoryBlockCustomCodeSection sec);
  void createMemoryBlockCustomCodeNodes(MemEditorTreeItem* parent, CarbonCfgMemoryBlockCustomCodeSection sect);
  MemEditorTreeItem* createMemoryBlockCustomCodeNode(const QString& name, CarbonCfgMemoryBlockCustomCodeSection sect);

  void createMemoryCustomCodeNodes(MemEditorTreeItem* parent, CarbonCfgMemoryCustomCodeSection sect);
  MemEditorTreeItem* createMemoryCustomCodeNode(const QString& name, CarbonCfgMemoryCustomCodeSection sect);

  CarbonCfgCompCustomCode* findCompCustomCode(CarbonCfgCustomCodePosition pos, CarbonCfgCompCustomCodeSection sec);
  void createCompCustomCodeNodes(MemEditorTreeItem* parent, CarbonCfgCompCustomCodeSection sect);
  MemEditorTreeItem* createCompCustomCodeNode(const QString& name, CarbonCfgCompCustomCodeSection sect);


private:
  CarbonCfg* mComponent;
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
};

class CustomCodeNode : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colWIDTH, colCOMMENT} ;

  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);

  virtual void setCode(const QString& code) = 0;

  CustomCodeNode(QTreeWidgetItem* parent, CarbonCfgCustomCode* cc, CustomCodesRootNode::CustomCodeKind kind, CarbonCfgCustomCodePosition pos)
    : MemEditorTreeItem(MemEditorTreeItem::CustomCodeNode, parent)
  {
    setCustomCode(cc);

    mPosition = pos;
    mKind = kind;
    
    setFlags(flags() | Qt::ItemIsEditable);
  }
  void setCodeText(const QString& code)
  {
    int colWidth = getTree()->columnWidth(1);
    QString elidedText = getTree()->fontMetrics().elidedText(code, Qt::ElideMiddle, colWidth);
    setMonoText(1, elidedText);
  }

  void setTitle(const QString& title)
  {
    setItalicText(0, title);
  }

  CarbonCfgCustomCodePosition getPosition() { return mPosition; }
  CarbonCfgCustomCode* getCustomCode() { return mCustomCode; }

  void setCustomCode(CarbonCfgCustomCode* newVal)
  {
    mCustomCode = newVal;
    if (mCustomCode)
    {
      setToolTip(1, mCustomCode->getCode());
      setColoredBoldText(1, "<Click to Edit...>", Qt::darkGreen);
    }
    else
    {
      setToolTip(1, "[No Code Defined]");
      setColoredItalicText(1, "<Click to Edit...>", getTree()->getDefaultTextColor());
    }
  }
  
private:
  CustomCodesRootNode::CustomCodeKind mKind;
  CarbonCfgCustomCodePosition mPosition;
  CarbonCfgCustomCode* mCustomCode;
};



class BlocksRootItem : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

  virtual void multiSelectContextMenu(QMenu&);

public:
  BlocksRootItem(QTreeWidgetItem* parent, CarbonCfgMemory* mem) : MemEditorTreeItem(MemEditorTreeItem::Blocks, parent)
  {
    mCheckInProgress = false;
    mMemory = mem;
    setBoldText(0, "Blocks");
    populateBlocks(mem);
    mActions = new BlocksRootActions(getTree());
  }

  bool check(bool validate);

  ESLMemoryBlock* addBlock(CarbonCfgMemoryBlock* block);
  ESLMemoryBlock* findBlock(const QString& blockName);
   
  friend class BlocksRootActions;

  BlocksRootActions* getActions() 
  {
    return mActions;
  }

  CarbonCfgMemory* getMemory() { return mMemory; }

private:
  void populateBlocks(CarbonCfgMemory* mem);

private:
  BlocksRootActions* mActions;
  CarbonCfgMemory* mMemory;
  bool mCheckInProgress;
};


class GeneralRootItem : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  GeneralRootItem(QTreeWidgetItem* parent, CarbonCfgMemory* mem) : MemEditorTreeItem(MemEditorTreeItem::General, parent)
  {
    mMemory = mem;
    setBoldText(0, "General");
    populateGeneral(mem);
    setExpanded(true);
  }

private:
  void populateGeneral(CarbonCfgMemory* mem);

private:
  CarbonCfgMemory* mMemory;

};

class ESLProgPreloadOffset : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  enum Columns { colNAME = 0, colVALUE, colSIZE} ;

   ESLProgPreloadOffset(QTreeWidgetItem* parent, CarbonCfgMemory* mem)
    : MemEditorTreeItem(MemEditorTreeItem::ProgPreloadOffset, parent)
  {
    mMemory = mem;

    setItalicText(colNAME, "Offset");
    setItalicText(colSIZE, "(bytes)");

    showValue();

    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemorySystemAddressESLPortBaseAddress), true);
   }

  void setValue(UInt64 newValue)
  {
    mMemory->putBaseAddr(newValue);
    showValue();
  }
  void showValue()
  {
    UtString buf;
    UtOStringStream os(&buf);
    os << UtIO::hex << mMemory->getBaseAddr() * mMemory->getMAU();

    QString hex = QString("0x%1").arg(buf.c_str());

    setText(colVALUE, hex);
  }

private:
  CarbonCfgMemory* mMemory;
};

// Port Name & Port Base Address
class ESLSystemAddressESLPortMapping : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colPORT = 0, colBASEADDRESS, colSIZE} ;

  bool check(bool /*validate*/)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    bool hasErrors = true;
    bool foundPort = false;
    clearErrors(0, 1, 1);

    CarbonCfg* cfg = getCcfg();
    for(quint32 i=0; i<cfg->numXtorInstances(); i++)
    {
      CarbonCfgXtorInstance* inst = cfg->getXtorInstance(i);
      if (mPortName == inst->getName())
      {
        foundPort = true;
        if (inst->getType()->hasWriteDebug())
          hasErrors = false;
        else
        {
          UtString msg;
          msg << "No Transactor Instance named '" << mPortName << "' found";
          mMemory->reportWarning(cfg, msg);
          showErrors(0, 1, msg.c_str(), 0);
        }
      }
    }

    return hasErrors;
  }

  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);
 
   ESLSystemAddressESLPortMapping(QTreeWidgetItem* parent, quint32 portIndex, CarbonCfgMemory* mem)
    : MemEditorTreeItem(MemEditorTreeItem::SystemAddressESLPortMapping, parent)
  {
    mMemory = mem;

    qint64 baseAddress = mem->getSystemAddressESLPortBaseAddress(portIndex);
    QString portName = mem->getSystemAddressESLPortName(portIndex);

    mPortName = portName;

    QString hexValue = QString("0x%1").arg(QString().setNum(baseAddress, 16));

    setText(colPORT, portName);
    setText(colBASEADDRESS, hexValue);
    setItalicText(colSIZE, "(bytes)");

    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemorySystemAddressESLPortBaseAddress), true);

    QString decValue = QString("Decimal: %1").arg(baseAddress);
    setToolTip(colBASEADDRESS, decValue);
  }

  QString getPortName() { return mPortName; }

  void changePortName(const QString& oldName, const QString& newName)
  {
    for(quint32 i=0; i<mMemory->numSystemAddressESLPorts(); i++)
    {
      QString portName = mMemory->getSystemAddressESLPortName(i);
      if (portName == oldName)
      {
        UtString name; name << newName;
        mMemory->putSystemAddressESLPortName(i, name.c_str());
        break;
      }
    }
    
    mPortName = newName;
    setText(colPORT, newName);
  }

  void setValue(qint64 value)
  {
    for(quint32 i=0; i<mMemory->numSystemAddressESLPorts(); i++)
    {
      QString portName = mMemory->getSystemAddressESLPortName(i);
      if (portName == mPortName)
      {
        mMemory->putSystemAddressESLPortBaseAddress(i, value);
        break;
      }
    }

    QString decValue = QString("Decimal: %1").arg(value);
    setToolTip(colBASEADDRESS, decValue);

    QString hexValue = QString("0x%1").arg(QString().setNum(value, 16));
    setText(colBASEADDRESS, hexValue);
  }

private:
  QString mPortName;
  CarbonCfgMemory* mMemory;
};



class ESLReadmemKind : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colKIND} ;

  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);


  ESLReadmemKind(QTreeWidgetItem* parent, CarbonCfgMemory* mem)
    : MemEditorTreeItem(MemEditorTreeItem::ReadmemKind, parent)
  {
    mMemory = mem;

    setItalicText(colNAME, "Format");
    setText(colKIND, mMemory->getReadmemTypeStr());
    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryInitializationReadmemFormat), true);
}

  void setKind(CarbonCfgReadmemType newType)
  {
    mMemory->putReadmemType(newType);
    setText(colKIND, mMemory->getReadmemTypeStr());
  }

private:
  CarbonCfgReadmemType parseType(const QString& sType);

private:
  CarbonCfgMemory* mMemory;
};

class ESLReadmemFile : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;

  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  ESLReadmemFile(QTreeWidgetItem* parent, CarbonCfgMemory* mem)
    : MemEditorTreeItem(MemEditorTreeItem::ReadmemFile, parent)
  {
    mMemory = mem;

    setItalicText(colNAME, "File");
    setText(colVALUE, mMemory->getInitFile());
    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryInitializationReadmemFile));
  }

  void setFile(const QString& newFile)
  {
    UtString nv; nv << newFile;
    mMemory->putInitFile(nv.c_str());
    setText(colVALUE, mMemory->getInitFile());
  }
  
  bool check(bool /*validate*/)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    clearErrors(0, 1, 0);
    if (mMemory->numMemoryBlocks() > 1)
    {  
      UtString msg;
      msg << "Readmem supports a single block";
      showErrors(0, 1, msg.c_str(), 0);
      mMemory->reportWarning(mMemory->getParent(), msg);
      return true;    
    }
    return false;
  }


private:
  CarbonCfgMemory* mMemory;
};


class ESLMemorySystemAddressMapping : public QObject, public MemEditorTreeItem
{
 Q_OBJECT

public:
  enum Columns { colNAME = 0, colMAPPINGS} ;

  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  
  bool check(bool validate)
  {  
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    bool hasErrors = false;
    for (int ci=0; ci<childCount(); ci++)
    {
      QTreeWidgetItem* childItem = child(ci);
      MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(childItem);
      if (item->getNodeType() == MemEditorTreeItem::SystemAddressESLPortMapping)
      {
        ESLSystemAddressESLPortMapping* pm = dynamic_cast<ESLSystemAddressESLPortMapping*>(item);
        if (pm->check(validate))
          hasErrors = true;
      }
    }
    return hasErrors;
  }

  ESLMemorySystemAddressMapping(QTreeWidgetItem* parent, CarbonCfgMemory* mem)
    : MemEditorTreeItem(MemEditorTreeItem::MemInitializationRoot, parent)
  {  
    if (mem->GetEditFlags().testFlag(CcfgFlags::EditMemorySystemAddressMapping))
     setFlags(flags() | Qt::ItemIsEditable);
   
    mMemory = mem;
    setBoldText(0, "System Address Mapping");  

    setItalicText(colMAPPINGS, "(click here to add/edit/delete)");

    populate();  
  }

  void setInitType(CarbonCfgMemInitType newType)
  {
    mMemory->putMemInitType(newType);
  }

 
  void populate()
  {
     // First wipe out any children present
    foreach (QTreeWidgetItem* item, takeChildren())
      delete item;

    for(quint32 i=0; i<mMemory->NumSystemAddressESLPorts(); i++)
    {
      new ESLSystemAddressESLPortMapping(this, i, mMemory);
    }
  }

private:
  CarbonCfgMemInitType parseType(const QString& sType);


private:
  CarbonCfgMemory* mMemory;
  
};


class ESLMemoryMaxAddress : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE, colSIZE} ;

  // Overrides
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  bool check(CarbonCfgMemory* mem, bool validate);

  ESLMemoryMaxAddress(QTreeWidgetItem* parent, CarbonCfgMemory* mem)
    : MemEditorTreeItem(MemEditorTreeItem::MemoryMaxAddress, parent)
  {
    mMemory = mem;

    setItalicText(colNAME, "Max Address");
    setItalicText(colSIZE, "(words)");

    setValue(mem->getMaxAddrs());
    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryMaxAddress), true);
 }

  void setValue(UInt32 value)
  {
    QString hexValue = QString("0x%1").arg(value,0,16);
    mMemory->putMaxAddrs(value);
    setText(colVALUE, hexValue);
    QString decValue = QString("Decimal: %1").arg(value);
    setToolTip(colVALUE, decValue);
  }


private:
  CarbonCfgMemory* mMemory;
};

class ESLMemoryDisassembler : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;

  // Overrides
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);
  

  ESLMemoryDisassembler(QTreeWidgetItem* parent, CarbonCfg* comp)
    : MemEditorTreeItem(MemEditorTreeItem::Disassembler, parent)
  {
    bool isEditable = true;
    
    if (comp)
      isEditable = comp->GetEditFlags().testFlag(CcfgFlags::EditComponentDisassembler);
    else
      isEditable = getCcfg()->GetEditFlags().testFlag(CcfgFlags::EditComponentDisassembler);

    mComponent = comp;

    setItalicText(colNAME, "Disassembler");

    if (mComponent)
    {     
      QString name = mComponent->getCadi()->getDisassemblerName();
      setName(name);
    }
    else
    {
      CarbonCfg* cfg = getCcfg();
      QString name = cfg->getCadi()->getDisassemblerName();
      setName(name);
    }  
    setEditable(isEditable);
  }

  void setName(const QString& value)
  {    
    UtString v;
    v << value;

    setText(colVALUE, value);

    if (mComponent)
    {
      mComponent->getCadi()->putDisassemblerName(v.c_str());
    }
    else
    {
      CarbonCfg* cfg = getCcfg();
      cfg->getCadi()->putDisassemblerName(v.c_str());
    }
  }

private:
  CarbonCfg* mComponent;
};

class ESLMemoryDisassembly : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;

  // Overrides
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);


  ESLMemoryDisassembly(QTreeWidgetItem* parent, CarbonCfgMemory* mem)
    : MemEditorTreeItem(MemEditorTreeItem::Disassembly, parent)
  {
    setFlags(flags() | Qt::ItemIsEditable);

    mMemory = mem;

    setItalicText(colNAME, "Disassembly");
    setName(mMemory->getMemoryDisassemblyName());
  }

  void setName(const QString& value)
  {    
    UtString v;
    v << value;
    mMemory->putMemoryDisassemblyName(v.c_str());
  }

private:
  CarbonCfgMemory* mMemory;
};


class ESLMemoryBigEndian : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;

  // overrides
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  ESLMemoryBigEndian(QTreeWidgetItem* parent, CarbonCfgMemory* mem)
    : MemEditorTreeItem(MemEditorTreeItem::MemoryBigEndian, parent)
  {
    setFlags(flags() | Qt::ItemIsEditable);
    setItalicText(colNAME, "Big Endian");
    mMemory = mem;
    setValue(mMemory->getBigEndian());
  }
  void setValue(bool value)
  {
    mMemory->putBigEndian(value);
    if (value)
      setText(colVALUE, "true");
    else
      setText(colVALUE, "false");
  }

private:
  CarbonCfgMemory* mMemory;
};


class BlocksRootItem;
class ESLMemorySubComponent;

class ESLMemory : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colWIDTH, colCOMMENT} ;
  virtual bool isDragSource() { return true; }

  // overrides
  virtual MemUndoData* deleteItem();
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  bool isSubComponent()
  {
    return !mMemory->getParent()->isTopLevel();
  }

  void showHasErrors(bool value)
  {
    QTreeWidgetItem* parent = MemEditorTreeItem::parent();

    // the text color and tooltip is prioritized, error -> warning -> normal 
    if (value)
    {
      if (parent)
      {
        parent->setTextColor(0, QColor(Qt::red));
        parent->setToolTip(0, "This memory contains errors, scroll down to view them");
      }
      showErrorText(0);
      setToolTip(0, "This memory contains errors, scroll down to view them");
    }
    else if ( ! mWidthIsByteMultiple )
    {
      if (parent) {
	parent->setTextColor(0, QColor(Qt::magenta));
	parent->setToolTip(0, "This memory contains warnings, scroll down to view them");
      }
      showWarningText(0);
      setToolTip(0, "This memory contains warnings, scroll down to view them");
    }
    else
    {
      if (parent)
      {
        parent->setTextColor(0, getTree()->getDefaultTextColor());
        parent->setToolTip(0, "");
      }
      showNormalText(0);
      setToolTip(0, "");
    }

    if ( ! mWidthIsByteMultiple ) {
      setIcon(colWIDTH, QIcon(":/cmm/Resources/logWarning.png"));
      setToolTip(colWIDTH, "For compatibility with SoCDesigner the memory width must be an integer multiple of 8.  Increase this value.");
    } else {
      setIcon(colWIDTH, QIcon());
      setToolTip(colWIDTH, "");      
    }
  }

  bool check(bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    bool errorsFound = false;

    if (getTree())
    {
      if (!mCheckInProgress)
      {
        mCheckInProgress = true;

        if (mInitRootItem && mInitRootItem->check(validate))
          errorsFound = true;
        if (mBlocksRootItem)
          if (mBlocksRootItem->check(validate))
            errorsFound = true;
        mCheckInProgress = false;

      }
    }
    showHasErrors(errorsFound);

    return errorsFound;
  }

  // Constructor for delete undo
  ESLMemory(CarbonCfgMemory* mem)
    : MemEditorTreeItem(MemEditorTreeItem::ESLMemoryNode)
  {
    init(mem);
  }

   ESLMemory(MemEditorTreeWidget* tree, CarbonCfgMemory* mem)
    : MemEditorTreeItem(MemEditorTreeItem::ESLMemoryNode)
  {
    setTreeWidget(tree);
    init(mem);
  }


  ESLMemory(QTreeWidgetItem* parent, CarbonCfgMemory* mem)
    : MemEditorTreeItem(MemEditorTreeItem::ESLMemoryNode, parent)
  {
    init(mem);

    QList<MemEditorTreeItem::NodeType> nonExpandNodes;
    nonExpandNodes.append(MemEditorTreeItem::MemBlockCustomCodes);
    nonExpandNodes.append(MemEditorTreeItem::MemCustomCodes);
    nonExpandNodes.append(MemEditorTreeItem::CompCustomCodes);

    getTree()->expandChildren(this, nonExpandNodes);
  }

  QString getName()
  {
    return mMemory->getName();
  }
  void setName(const QString& name)
  {
    setBoldText(colNAME, name);
    setIcon(colNAME, QIcon(":/cmm/Resources/memoryMap.png"));
  }

  BlocksRootItem* getBlocks() { return mBlocksRootItem; }

  void init(CarbonCfgMemory* mem)
  {
    mCheckInProgress = false;
    mWidthIsByteMultiple = false;
    mBlocksRootItem = NULL;
    mGeneralRootItem = NULL;
    mInitRootItem = NULL;

    setIcon(colNAME, QIcon(":/cmm/Resources/memoryMap.png"));

    mMemory = mem;
    setBoldText(colNAME, mem->getName());

    setWidth(mem->getWidth());

    setItalicText(colCOMMENT, "(word size in bits)");

    mGeneralRootItem = new GeneralRootItem(getItem(), mem);

    if (getTree()->getEditor()->getEditorMode() == CompWizardMemEditor::eModeSocDesigner)
    {
      mInitRootItem = new ESLMemorySystemAddressMapping(this, mem);   
    }

    mCodes = new CustomCodesRootNode(getItem(), mem);

    mBlocksRootItem = new BlocksRootItem(getItem(), mem);
    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryName));
  }

  CarbonCfgMemory* getMemory() 
  {
    return mMemory;
  }
  void setWidth(UInt32 v)
  {
    mWidthIsByteMultiple = ((v % 8) == 0);

    UtString width;
    UtOStringStream os1(&width);
    os1 << UtIO::dec << v;
    setText(colWIDTH, width.c_str());
  }
private:
  // Delete operation
  class Delete : public MemUndoData
  {
  public:
    CarbonCfg* mCfg;
    CarbonCfgMemory* mMemory;
    int mPos;
    QString mComponentName;
    bool mIsSubComponent;
    int mTreePos;
    int mChildIndex; // valid for sub-components only
    bool isSubComponent();
    ESLMemorySubComponent* mSubComponent;
    
  protected:
    void undo();
    void redo();
  };

private:
  GeneralRootItem* mGeneralRootItem;
  BlocksRootItem* mBlocksRootItem;
  ESLMemorySystemAddressMapping* mInitRootItem;
  CarbonCfgMemory* mMemory; 
  CustomCodesRootNode* mCodes;
  bool mCheckInProgress;
  bool mWidthIsByteMultiple;
};



class MemoriesRootItem : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

  // Overrides
  virtual bool canDropHere(const QString&, QDragMoveEvent*);
  virtual bool dropItem(const QString&);


public:
  bool check(CarbonConsole*, bool validate = true)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    bool hasErrors = check(validate);

    if (hasErrors)
    {
      showErrorText(0);
      setToolTip(0, "One or more memories contains problems, scroll down to view.");
    }
    else
    {
      showNormalText(0);
      setToolTip(0, "");
    }

    return hasErrors;
  }
  int firstMemoryChildPosition()
  {
    if (getTree()->getEditor()->getEditorMode() == CompWizardMemEditor::eModeSocDesigner)
      return 1; // After Disassembler node
    else
      return 0; // Right under "Memories..."
  }
  bool check(bool validate);
  

public:
  ESLMemory* findMemory(const QString& memName)
  {
   // walk children and look for Memories
    for (int ci=0; ci<childCount(); ci++)
    {
      QTreeWidgetItem* childItem = child(ci);
      MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(childItem);
      if (item->getNodeType() == MemEditorTreeItem::ESLMemoryNode)
      {
        ESLMemory* eslMem = dynamic_cast<ESLMemory*>(item);
        if (eslMem->getName() == memName)
          return eslMem;
      }    
    }
    return NULL;
  }

  MemoriesRootItem(QTreeWidget* parent, CarbonCfg* cfg) : MemEditorTreeItem(MemEditorTreeItem::Memories, parent)
  {
    mCfg = cfg;
    setBoldText(0, "Memories");

    mRootDisassembler = NULL;

    if (getTree()->getEditor()->getEditorMode() == CompWizardMemEditor::eModeSocDesigner)
    {
      mRootDisassembler = new ESLMemoryDisassembler(this, NULL);
    }
  }

public:
  void populateMemories(CarbonCfg* cfg);

private:
  CarbonCfg* mCfg;
  ESLMemoryDisassembler* mRootDisassembler;
};

class ESLMemoryBlockLocationRTL;

class ComponentCustomCodeNode : public CustomCodeNode
{
  Q_OBJECT

public:
  ComponentCustomCodeNode(QTreeWidgetItem* parent, CarbonCfgCompCustomCodeSection sect, CarbonCfg* comp, CarbonCfgCompCustomCode* cc, 
    CustomCodesRootNode::CustomCodeKind kind, CarbonCfgCustomCodePosition pos)
    : CustomCodeNode(parent, cc, kind, pos)
  {
    mSection = sect;
    mComponent = comp;
    mCustomCode = cc;

    switch(pos)
    {
    case eCarbonCfgPre: setItalicText(0, "Pre"); break;
    case eCarbonCfgPost: setItalicText(0, "Post"); break;
    }

    bool editAble = comp->GetEditFlags().testFlag(CcfgFlags::EditComponentCustomCodes);
    setEditable(editAble, true);
    
    if (!editAble)
    {
      MemEditorTreeItem* parentItem = dynamic_cast<MemEditorTreeItem*>(parent);
      parentItem->setEditable(editAble);
    }
  }

  virtual void setCode(const QString& code)
  {
    QString trimmedCode = code.trimmed();

    CarbonCfgCustomCode* cc = getCustomCode();
    if (cc)
    {
      if (code != cc->getCode() || trimmedCode.isEmpty())
      {
        if (!trimmedCode.isEmpty())
          cc->SetCode(code);
        else
        {
          mComponent->removeCustomCode((CarbonCfgCompCustomCode*)cc);
          setCustomCode(NULL);
        }
        setModified(true);
      }
    }
    else if (!trimmedCode.isEmpty())
    {
      CarbonCfgCompCustomCode* mcc = mComponent->addCustomCode();

      CarbonCfgCompCustomCodeSectionIO sectIO = mSection;
      mcc->putSectionString(sectIO.getString()); 
     
      mcc->SetCode(code);
      setCustomCode(mcc);
      setModified(true);
    }
  }

private:
  CarbonCfg* mComponent;
  CarbonCfgCompCustomCode* mCustomCode;
  CarbonCfgCompCustomCodeSection mSection;

};

class MemoryCustomCodeNode : public CustomCodeNode
{
  Q_OBJECT

public:
  MemoryCustomCodeNode(QTreeWidgetItem* parent, CarbonCfgMemoryCustomCodeSection sect, CarbonCfgMemory* mem, CarbonCfgMemoryCustomCode* cc, 
    CustomCodesRootNode::CustomCodeKind kind, CarbonCfgCustomCodePosition pos)
    : CustomCodeNode(parent, cc, kind, pos)
  {
    mSection = sect;
    mMemory = mem;
    mCustomCode = cc;

    switch(pos)
    {
    case eCarbonCfgPre: setItalicText(0, "Pre"); break;
    case eCarbonCfgPost: setItalicText(0, "Post"); break;
    }

    bool editAble = mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryCustomCodes);
    setEditable(editAble, true);
    
    if (!editAble)
    {
      MemEditorTreeItem* parentItem = dynamic_cast<MemEditorTreeItem*>(parent);
      parentItem->setEditable(editAble);
    }
  }

  virtual void setCode(const QString& code)
  {
    QString trimmedCode = code.trimmed();

    CarbonCfgCustomCode* cc = getCustomCode();
    if (cc)
    {
      if (code != cc->getCode() || trimmedCode.isEmpty())
      {
        if (!trimmedCode.isEmpty())
          cc->SetCode(code);
        else
        {
          mMemory->removeCustomCode(cc);
          setCustomCode(NULL);
        }
        setModified(true);
      }
    }
    else if (!trimmedCode.isEmpty())
    {
      CarbonCfgMemoryCustomCode* mcc = mMemory->addCustomCode();
      CarbonCfgMemoryCustomCodeSectionIO sectIO = mSection;
      mcc->putSectionString(sectIO.getString()); 
      mcc->SetCode(code);
      setCustomCode(mcc);
      setModified(true);
    }
  }

private:
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryCustomCode* mCustomCode;
  CarbonCfgMemoryCustomCodeSection mSection;

};

class MemoryBlockCustomCodeNode : public CustomCodeNode
{
  Q_OBJECT

public:
  MemoryBlockCustomCodeNode(QTreeWidgetItem* parent, CarbonCfgMemoryBlockCustomCodeSection sect, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, 
    CarbonCfgMemoryBlockCustomCode* cc, CustomCodesRootNode::CustomCodeKind kind, CarbonCfgCustomCodePosition pos)
    : CustomCodeNode(parent, cc, kind, pos)
  {
    mSection = sect;
    mMemory = mem;
    mBlock = block;

    switch(pos)
    {
    case eCarbonCfgPre: setItalicText(0, "Pre"); break;
    case eCarbonCfgPost: setItalicText(0, "Post"); break;
    }

    bool editAble = mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockCustomCodes);
    setEditable(editAble, true);
    
    if (!editAble)
    {
      MemEditorTreeItem* parentItem = dynamic_cast<MemEditorTreeItem*>(parent);
      parentItem->setEditable(editAble);
    }
  }

  virtual void setCode(const QString& code)
  {
    CarbonCfgCustomCode* cc = getCustomCode();
    if (cc)
    {
      if (code != cc->getCode())
      {
        cc->SetCode(code);
        setModified(true);
      }
    }
    else
    {
      CarbonCfgMemoryBlockCustomCode* mcc = mBlock->addCustomCode();
      CarbonCfgMemoryBlockCustomCodeSectionIO sectIO = mSection;
      mcc->putSectionString(sectIO.getString()); 
      mcc->SetCode(code);
      setCustomCode(mcc);
      setModified(true);
    } 
  }

private:
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  CarbonCfgMemoryBlockCustomCodeSection mSection;
};

class ESLMemoryBlockLocations : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colWIDTH, colCOMMENT} ;

  // Overrides
  virtual bool canDropHere(const QString&, QDragMoveEvent*);
  virtual bool dropItem(const QString&);


  ESLMemoryBlockLocations(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockLocations, parent)
  {
    mCheckInProgress = false;
    mMemory = mem;
    mBlock = block;
    setBoldText(colNAME, "Locations");
    populateLocations(block);
  }

  bool check(bool validate);

  ESLMemoryBlockLocationRTL* addRTLLocation(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc);
  ESLMemoryBlockLocationRTL* findRTLLocation(CarbonCfgMemoryLocRTL* loc);
  
private:
  void populateLocations(CarbonCfgMemoryBlock* block);

private:
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  bool mCheckInProgress;
};

// RTL Versions
class ESLMemoryBlockLocDisplayAttrStartWordOffsetRTL : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;

  // Overrides
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  bool check(bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return check(mMemory, mBlock, mLoc, validate);
    else
      return false;
  }
  bool check(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc, bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return CheckBlockLocation(getItem()->parent(), this, getCcfg(), mem, block, loc, validate);
    else
      return false;
  }

  ESLMemoryBlockLocDisplayAttrStartWordOffsetRTL(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLocRTL* loc)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockLocationDisplayAttrStartWordOffsetRTL, parent)
  {  
    mMemory = mem;
    mBlock = block;
    mLoc = loc;

    setItalicText(colNAME, "Start Word Offset");
    setValue(mLoc->getRTLStartWordOffset());
    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockLocationRTLStartWordOffset), true);
  }

  void setValue(UInt32 value)
  {
    mLoc->putRTLStartWordOffset(value);
    QString hexValue = QString("0x%1").arg(value,0,16);
    setText(colVALUE, hexValue);
    QString decValue = QString("Decimal: %1").arg(value);
    setToolTip(colVALUE, decValue);
  }


private:
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  CarbonCfgMemoryLocRTL* mLoc;
};

class ESLMemoryBlockLocDisplayAttrEndWordOffsetRTL : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;

  // Overrides
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  bool check(bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return check(mMemory, mBlock, mLoc, validate);
    else
      return false;
  }
  bool check(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc, bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return CheckBlockLocation(getItem()->parent(), this, getCcfg(), mem, block, loc, validate);
    else
      return false;
  }

  ESLMemoryBlockLocDisplayAttrEndWordOffsetRTL(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLocRTL* loc)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockLocationDisplayAttrEndWordOffsetRTL, parent)
  {
    mMemory = mem;
    mBlock = block;
    mLoc = loc;

    setItalicText(colNAME, "End Word Offset");
    setValue(loc->getRTLEndWordOffset());
    setEditable( mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockLocationRTLEndWordOffset), true);   
  }

  void setValue(UInt64 value)
  {
    mLoc->putRTLEndWordOffset(value);
    QString hexValue = QString("0x%1").arg(value,0,16);
    setText(colVALUE, hexValue);
    QString decValue = QString("Decimal: %1").arg(value);
    setToolTip(colVALUE, decValue);
  }

private:
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  CarbonCfgMemoryLocRTL* mLoc;
};

class ESLMemoryBlockLocDisplayAttrLSBRTL : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;

  // Overrides
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);


  ESLMemoryBlockLocDisplayAttrLSBRTL(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLocRTL* loc)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockLocationDisplayAttrLSBRTL, parent)
  {

    mMemory = mem;
    mBlock = block;
    mLoc = loc;

    setItalicText(colNAME, "LSB");

    setValue(mLoc->getRTLLsb());
    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockLocationRTLStartLSB), true);  
 }

  bool check(bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return check(mMemory, mBlock, mLoc, validate);
    else
      return false;
  }

  bool check(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc, bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return CheckBlockLocation(getItem()->parent(), this, getCcfg(), mem, block, loc, validate);
    else
      return false;
  }

  void setValue(UInt32 value)
  {
    mLoc->putRTLLsb(value);
    QString v = QString("%1").arg(value);
    setText(colVALUE, v);
  }

private:
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  CarbonCfgMemoryLocRTL* mLoc;
};

class ESLMemoryBlockLocDisplayAttrMSBRTL : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;

  // Overrides
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  bool check(bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return check(mMemory, mBlock, mLoc, validate);
    else
      return false;
  }

  bool check(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc, bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return CheckBlockLocation(getItem()->parent(), this, getCcfg(), mem, block, loc, validate);
    else
      return false;
  }

  ESLMemoryBlockLocDisplayAttrMSBRTL(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLocRTL* loc)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockLocationDisplayAttrMSBRTL, parent)
  {
    mMemory = mem;
    mBlock = block;
    mLoc = loc;

    setItalicText(colNAME, "MSB");

    setValue(mLoc->getRTLMsb());
    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockLocationRTLStartMSB), true);
 }

  void setValue(UInt32 value)
  {
    mLoc->putRTLMsb(value);
    QString v = QString("%1").arg(value);
    setText(colVALUE, v);
  }


private:
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  CarbonCfgMemoryLocRTL* mLoc;
};


// Base Versions
class ESLMemoryBlockLocDisplayAttrStartWordOffset : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE, colINFO} ;

  // Overrides
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  bool check(bool validate)  
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return check(mMemory, mBlock, mLoc, validate);
    else
      return false;
  }

  bool check(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc, bool validate) 
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return CheckBlockLocation(getItem()->parent(), this, getCcfg(), mem, block, loc, validate);
    else
      return false;
  }

  ESLMemoryBlockLocDisplayAttrStartWordOffset(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockLocationDisplayAttrStartWordOffset, parent)
  {
    mMemory = mem;
    mBlock = block;
    mLoc = loc;

    setItalicText(colNAME, "Start Word Offset");
    setValue(mLoc->getDisplayStartWordOffset());
    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockLocationDisplayStartWordOffset), true);
 }

  void setValue(UInt64 value)
  {
    mLoc->putDisplayStartWordOffset(value);
    QString hexValue = QString("0x%1").arg(value,0,16);
    setText(colVALUE, hexValue);
    QString decValue = QString("Decimal: %1").arg(value);
    setToolTip(colVALUE, decValue);
    QString hexByteAddress = QString("(byte address: 0x%1)").arg(((((mMemory->getWidth()+7)/8)*value)+mMemory->getBaseAddr()),0,16);
    setToolTip(colVALUE, hexByteAddress);
  }


private:
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  CarbonCfgMemoryLoc* mLoc;
};

class ESLMemoryBlockLocDisplayAttrEndWordOffset : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;

  // Overrides
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  bool check(bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return check(mMemory, mBlock, mLoc, validate);
    else
      return false;
  }
  bool check(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc, bool validate) 
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return CheckBlockLocation(getItem()->parent(), this, getCcfg(), mem, block, loc, validate);
    else
      return false;
  }

  ESLMemoryBlockLocDisplayAttrEndWordOffset(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockLocationDisplayAttrEndWordOffset, parent)
  {  
    mMemory = mem;
    mBlock = block;
    mLoc = loc;

    setItalicText(colNAME, "End Word Offset");
    setValue(loc->getDisplayEndWordOffset());
    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockLocationDisplayEndWordOffset), true);
}

  void setValue(UInt64 value)
  {
    mLoc->putDisplayEndWordOffset(value);
    QString hexValue = QString("0x%1").arg(value,0,16);
    setText(colVALUE, hexValue);
    QString decValue = QString("Decimal: %1").arg(value);
    setToolTip(colVALUE, decValue);
    QString hexByteAddress = QString("(byte address: 0x%1)").arg(((((mMemory->getWidth()+7)/8)*value)+mMemory->getBaseAddr()),0,16);
    setToolTip(colVALUE, hexByteAddress);
  }

private:
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  CarbonCfgMemoryLoc* mLoc;
};

class ESLMemoryBlockLocDisplayAttrLSB : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;

  // Overrides
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  bool check(bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return check(mMemory, mBlock, mLoc, validate);
    else
      return false;
  }

  bool check(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc, bool validate) 
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return CheckBlockLocation(getItem()->parent(), this, getCcfg(), mem, block, loc, validate);
    else
      return false;
  }

  ESLMemoryBlockLocDisplayAttrLSB(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockLocationDisplayAttrLSB, parent)
  { 
    mMemory = mem;
    mBlock = block;
    mLoc = loc;

    setItalicText(colNAME, "LSB");

    setValue(mLoc->getDisplayLsb());
    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockLocationDisplayStartLSB), true);
  }
  
  void setValue(UInt32 value)
  {
    mLoc->putDisplayLsb(value);
    QString v = QString("%1").arg(value);
    setText(colVALUE, v);
  }

private:
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  CarbonCfgMemoryLoc* mLoc;
};

class ESLMemoryBlockLocDisplayAttrMSB : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;

  // Overrides
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  bool check(bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return check(mMemory, mBlock, mLoc, validate);
    else
      return false;
  }

  bool check(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc, bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return CheckBlockLocation(getItem()->parent(), this, getCcfg(), mem, block, loc, validate);
    else
      return false;
  }

  ESLMemoryBlockLocDisplayAttrMSB(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockLocationDisplayAttrMSB, parent)
  {
    mMemory = mem;
    mBlock = block;
    mLoc = loc;

    setItalicText(colNAME, "MSB");

    setValue(mLoc->getDisplayMsb());
    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockLocationDisplayStartMSB), true);
  }

  void setValue(UInt32 value)
  {
    mLoc->putDisplayMsb(value);
    QString v = QString("%1").arg(value);
    setText(colVALUE, v);
  }


private:
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  CarbonCfgMemoryLoc* mLoc;
};


class ESLMemoryBlockLocationDisplayAttr : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;


  bool check(bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    bool errorsFound = false;
    if (getTree())
    {
      if (!mCheckInProgress)
      {
        mCheckInProgress = true;
        if (mStartWordOffset && mStartWordOffset->check(validate))
          errorsFound = true;
        if (mEndWordOffset && mEndWordOffset->check(validate))
          errorsFound = true;
        if (mLsb && mLsb->check(validate))
          errorsFound = true;
        if (mMsb && mMsb->check(validate))
          errorsFound = true;
        mCheckInProgress = false;
      }
    }
    return errorsFound;
  }

  ESLMemoryBlockLocationDisplayAttr(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockLocationDisplayAttr, parent)
  {
    mCheckInProgress = false;
    mStartWordOffset = NULL;
    mEndWordOffset = NULL;
    mLsb = NULL;
    mMsb = NULL;

    mMemory = mem;
    mBlock = block;
    mLoc = loc;

    setBoldText(colNAME, "Display");

    mStartWordOffset = new ESLMemoryBlockLocDisplayAttrStartWordOffset(getItem(), mem, block, loc);
    mEndWordOffset = new ESLMemoryBlockLocDisplayAttrEndWordOffset(getItem(), mem, block, loc);
    mLsb = new ESLMemoryBlockLocDisplayAttrLSB(getItem(), mem, block, loc);
    mMsb = new ESLMemoryBlockLocDisplayAttrMSB(getItem(), mem, block, loc);
  
    //setExpanded(true);
  }

private:
  ESLMemoryBlockLocDisplayAttrStartWordOffset* mStartWordOffset;
  ESLMemoryBlockLocDisplayAttrEndWordOffset* mEndWordOffset;
  ESLMemoryBlockLocDisplayAttrLSB* mLsb;
  ESLMemoryBlockLocDisplayAttrMSB* mMsb;

  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  CarbonCfgMemoryLoc* mLoc;
  bool mCheckInProgress;
};

class ESLMemoryBlockLocationDisplayAttrRTL : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;

  bool check(bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    bool errorsFound = false;

    if (getTree())
    {
      if (!mCheckInProgress)
      {
        mCheckInProgress = true;
        if (mStartWordOffset && mStartWordOffset->check(validate))
          errorsFound = true;

        if (mEndWordOffset && mEndWordOffset->check(validate))
          errorsFound = true;

        if (mLsb && mLsb->check(validate))
          errorsFound = true;

        if (mMsb && mMsb->check(validate))
          errorsFound = true;

        mCheckInProgress = false;
      }
    }
    return errorsFound;
  }

  ESLMemoryBlockLocationDisplayAttrRTL(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLocRTL* loc)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockLocationDisplayAttrRTL, parent)
  {
    mCheckInProgress = false;
    mStartWordOffset = NULL;
    mEndWordOffset = NULL;
    mLsb = NULL;
    mMsb = NULL;

    mMemory = mem;
    mBlock = block;
    mLoc = loc;

    setBoldText(colNAME, "RTL");

    mStartWordOffset = new ESLMemoryBlockLocDisplayAttrStartWordOffsetRTL(getItem(), mem, block, loc);
    mEndWordOffset = new ESLMemoryBlockLocDisplayAttrEndWordOffsetRTL(getItem(), mem, block, loc);
    mLsb = new ESLMemoryBlockLocDisplayAttrLSBRTL(getItem(), mem, block, loc);
    mMsb = new ESLMemoryBlockLocDisplayAttrMSBRTL(getItem(), mem, block, loc);
  
    //setExpanded(true);
  }

  void setStartWordOffset(UInt32 v)
  {
    mStartWordOffset->setValue(v);
  }

  void setEndWordOffset(UInt32 v)
  {
    mEndWordOffset->setValue(v);
  }

  void setMsb(UInt32 v)
  {
    mMsb->setValue(v);
  }

  void setLsb(UInt32 v)
  {
    mLsb->setValue(v);
  }

private:
  ESLMemoryBlockLocDisplayAttrStartWordOffsetRTL* mStartWordOffset;
  ESLMemoryBlockLocDisplayAttrEndWordOffsetRTL* mEndWordOffset;
  ESLMemoryBlockLocDisplayAttrLSBRTL* mLsb;
  ESLMemoryBlockLocDisplayAttrMSBRTL* mMsb;

  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  CarbonCfgMemoryLocRTL* mLoc;
  bool mCheckInProgress;
};



class ESLMemoryBlockLocationRTL : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;

  // Overrides
  virtual MemUndoData* deleteItem();
  virtual bool canDropHere(const QString&, QDragMoveEvent*);
  virtual bool dropItem(const QString&);
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);
  bool check(bool validate);

  ESLMemoryBlockLocationRTL(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockLocationRTL)
  {
    init(mem, block, loc);
  }

  ESLMemoryBlockLocationRTL(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockLocationRTL, parent)
  {
    init(mem, block, loc);
  }

  void init(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc)
  { 
    mDisplayAttr = NULL;
    mRTLAttr = NULL;

    mMemory = mem;
    mBlock = block;
    mLoc = loc;
    mLocRTL = loc->castRTL();

    setIcon(colNAME, QIcon(":/cmm/Resources/field.png"));
    setText(colNAME, "RTL Path");

    setPath(mLocRTL->getPath());

    mDisplayAttr = new ESLMemoryBlockLocationDisplayAttr(getItem(), mem, block, mLocRTL);
    mRTLAttr = new ESLMemoryBlockLocationDisplayAttrRTL(getItem(), mem, block, mLocRTL);  
    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockLocationRTLPath), true);
  }

  void setPath(const QString& path)
  {
    setText(colVALUE, path);
  }

  ESLMemoryBlockLocationDisplayAttrRTL* rtlAttr() { return mRTLAttr; }
  CarbonCfgMemoryLocRTL* getLoc() { return mLocRTL; }

private:
 // Delete operation
  class Delete : public MemUndoData
  {
  public:
    CarbonCfg* mCfg;
    CarbonCfgMemory* mMemory;
    CarbonCfgMemoryBlock* mBlock;
    CarbonCfgMemoryLocRTL* mLoc;
    int mPos; // position in ccfg structure
    int mChildPos; // position in the gui

  protected:
    void undo()
    {
      qDebug() << "Undo Delete Block RTL Loc" << text();

      mBlock->insertLocUndo(mChildPos, mLoc);

      ESLMemoryBlockLocationRTL* pMem = new ESLMemoryBlockLocationRTL(mMemory, mBlock, mLoc);

      QTreeWidgetItem* parent = getParentItem();
      parent->insertChild(mChildPos, pMem);
       
      getTree()->expandChildren(pMem);
      pMem->setSelected(true);
      getTree()->scrollToItem(pMem);
      getTree()->setModified(false);
    }
    void redo()
    {
      qDebug() << "Delete Block RTL Loc" << text();

      QTreeWidgetItem* parent = getParentItem();
      QTreeWidgetItem* item = getItem();

      mChildPos = parent->indexOfChild(item);

      item->setSelected(false);
      mPos = mBlock->removeLocUndo(mLoc);
      parent->removeChild(item);
      getTree()->setModified(true);
    }
  };

  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  CarbonCfgMemoryLoc* mLoc;
  CarbonCfgMemoryLocRTL* mLocRTL;
  ESLMemoryBlockLocationDisplayAttrRTL* mRTLAttr;
  ESLMemoryBlockLocationDisplayAttr* mDisplayAttr;
};

class ESLMemoryBlockLocationPort : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;


  ESLMemoryBlockLocationPort(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockLocationPort, parent)
  {
    mMemory = mem;
    mBlock = block;
    mLoc = loc;
    mLocPort = loc->castPort();

  }

private:
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  CarbonCfgMemoryLoc* mLoc;
  CarbonCfgMemoryLocPort* mLocPort;

};

class ESLMemoryBlockLocationUser: public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;


  ESLMemoryBlockLocationUser(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockLocationUser, parent)
  {
    mMemory = mem;
    mBlock = block;
    mLoc = loc;
    mLocUser = loc->castUser();
  }

private:
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  CarbonCfgMemoryLoc* mLoc;
  CarbonCfgMemoryLocUser* mLocUser;
};

class ESLMemoryBlockBase : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE, colSIZE} ;
  
  // Overrides
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  bool check(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return CheckBlock(getItem()->parent(), this, getCcfg(), mem, block, validate);
    else
      return false;
  }

  ESLMemoryBlockBase(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockBase, parent)
  {
    mBlock = block;
    mMemory = mem;
    setItalicText(colNAME, "Base Address");
    setItalicText(colSIZE, "(bytes)");

    setValue(block->getBase());
    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockBaseAddress), true);
  }

  void setValue(UInt64 value)
  {
    QString hexValue = QString("0x%1").arg(value,0,16);
    mBlock->putBase(value);
    setText(colVALUE, hexValue);
    QString decValue = QString("Decimal: %1").arg(value);
    setToolTip(colVALUE, decValue);
   }


private:
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
};

class ESLMemoryBlockSize : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE, colSIZE} ;

  // Overrides
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  bool check(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return CheckBlock(getItem()->parent(), this, getCcfg(), mem, block, validate);
    else
      return false;
  }

  ESLMemoryBlockSize(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlockSize, parent)
  {   
    mMemory = mem;
    mBlock = block;
    setItalicText(colNAME, "Size");
    setItalicText(colSIZE, "(bytes)");

    setValue(block->getSize());
    UtString width;
    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockSize), true);
  }

   void setValue(UInt64 value)
  {
    QString hexValue = QString("0x%1").arg(value,0,16);
    mBlock->putSize(value);
    setText(colVALUE, hexValue);
    QString decValue = QString("Decimal: %1").arg(value);
    setToolTip(colVALUE, decValue);

  }


private:
  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
};


class ESLMemoryBlock : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE} ;

  // overrides
  virtual MemUndoData* deleteItem();

  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  bool check(bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    if (getTree())
      return check(mMemory, mBlock, validate);
    else
      return false;
  }
  bool check(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    bool errorsFound = false;

    if (getTree())
    {
      if (validate)
        block->check(getCcfg(), mem->getName(), mem->getMaxAddrs(), getCcfg()->getDB());

      if (block->messageState())
      {
        QString msgs = block->getMessage();
        showErrors(colNAME, colVALUE, msgs, colNAME);
        errorsFound = true;
      }
      else
        clearErrors(colNAME, colVALUE, colNAME);

      if (mLocations)
      {
        if (mLocations->check(validate))
          errorsFound = true;
      }

      ESLMemory* mem = findESLMemory(getTree(), mMemory);
      if (mem)
        mem->showHasErrors(errorsFound);
    }

    return errorsFound;
  }

  ESLMemoryBlock(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block)
     : MemEditorTreeItem(MemEditorTreeItem::MemBlock)
  {
    init(mem, block);
  }

  ESLMemoryBlock(QTreeWidgetItem* parent, CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block)
    : MemEditorTreeItem(MemEditorTreeItem::MemBlock, parent)
  {
    init(mem, block);
  }

  void init(CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block)
  {
    mLocations = NULL;   

    setIcon(colNAME, QIcon(":/cmm/Resources/bank.png"));

    mMemory = mem;
    mBlock = block;

    setName(block->getName());

    new ESLMemoryBlockBase(getItem(), mem,  block);
    mSize = new ESLMemoryBlockSize(getItem(), mem, block);

    mCodes = new CustomCodesRootNode(getItem(), mem, block);
    mLocations = new ESLMemoryBlockLocations(getItem(), mem, block);

    //setExpanded(true);

    mCodes->setExpanded(false);

    setEditable(mem->GetEditFlags().testFlag(CcfgFlags::EditMemoryBlockName), true);
  }
  
  ESLMemoryBlockSize* getSize() { return mSize; }

  void setName(const QString& v)
  {
    setText(colNAME, v);
  }

  CarbonCfgMemory* getMemory() { return mMemory; }
  CarbonCfgMemoryBlock* getBlock() { return mBlock; }
  QString getName() { return mBlock->getName(); }

private:
 // Delete operation
  class Delete : public MemUndoData
  {
  public:
    CarbonCfg* mCfg;
    CarbonCfgMemory* mMemory;
    CarbonCfgMemoryBlock* mBlock;
    int mPos; // position in ccfg structure
    int mChildPos; // position in the gui

  protected:
    void undo()
    {
       qDebug() << "Undo Delete Memory Block" << text();

       mMemory->insertMemoryBlockUndo(mPos, mBlock);

       ESLMemoryBlock* pMem = new ESLMemoryBlock(mMemory, mBlock);

       QTreeWidgetItem* parent = getParentItem();
       parent->insertChild(mChildPos, pMem);
       
       getTree()->expandChildren(pMem);
       pMem->setSelected(true);
       getTree()->scrollToItem(pMem);
       getTree()->setModified(false);
      }
    void redo()
    {
      qDebug() << "Delete Memory Block" << text();

      QTreeWidgetItem* parent = getParentItem();
      QTreeWidgetItem* item = getItem();

      mChildPos = parent->indexOfChild(item);

      item->setSelected(false);
      mPos = mMemory->removeMemoryBlockUndo(mBlock);
      parent->removeChild(item);
      getTree()->setModified(true);
    }
  };

  CarbonCfgMemory* mMemory;
  CarbonCfgMemoryBlock* mBlock;
  ESLMemoryBlockLocations* mLocations;
  CustomCodesRootNode* mCodes;
  ESLMemoryBlockSize* mSize;
};

class ESLMemorySubComponent : public QObject, public MemEditorTreeItem
{
  Q_OBJECT

public:
  // Drags from the Hierarchy Window
  virtual bool canDropHere(const QString&, QDragMoveEvent*);
  virtual bool dropItem(const QString&);

  // Drags within the Memory Tree
  virtual bool canDropHere(const QList<MemEditorTreeItem*>&, QDragMoveEvent*);
  virtual bool dropItems(const QList<MemEditorTreeItem*>&);

  virtual void deleteItem(QList<MemUndoData*>* undoItems);
  virtual QWidget* createEditor(const MemEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const MemEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  enum Columns { colNAME = 0, colVALUE} ;
 
  ESLMemorySubComponent(QTreeWidgetItem* parent, CarbonCfg* subComp) : MemEditorTreeItem(MemEditorTreeItem::MemorySubComponent, parent)
  {
    mSubComponent = subComp;
    mCodes = NULL;

    QString subComponentName = subComp->getCompName();

    setName(subComponentName);

    setIcon(colNAME, QIcon(":/cmm/Resources/bank.png"));
    setItalicText(1, "(drop memories here)");
   

    mDisassembler = NULL;

    if (getTree()->getEditor()->getEditorMode() == CompWizardMemEditor::eModeSocDesigner)
    {
      mDisassembler = new ESLMemoryDisassembler(this, subComp);
      mCodes = new CustomCodesRootNode(getItem(), subComp);
    }

    for (UInt32 i=0; i<subComp->numMemories(); i++)
    {
      CarbonCfgMemory* mem = subComp->getMemory(i);
      new ESLMemory(this, mem);
    }

    QList<MemEditorTreeItem::NodeType> nonExpandNodes;
    nonExpandNodes.append(MemEditorTreeItem::MemBlockCustomCodes);
    nonExpandNodes.append(MemEditorTreeItem::MemCustomCodes);
    nonExpandNodes.append(MemEditorTreeItem::CompCustomCodes);

    getTree()->expandChildren(this, nonExpandNodes);   

    setEditable(subComp->GetEditFlags().testFlag(CcfgFlags::EditComponentName));
  }

  bool check(bool validate)
  {
    if (getWizard() && !getWizard()->autoCheckEnabled())
      return false;

    bool errorsFound = false;
    for (int i=0; i<childCount(); i++)
    {
      QTreeWidgetItem* childItem = child(i);
      MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(childItem);
      if (item->getNodeType() == MemEditorTreeItem::ESLMemoryNode)
      {
        ESLMemory* memNode = dynamic_cast<ESLMemory*>(item);
        if (memNode->check(validate))
          errorsFound = true;
      }
    }
    return errorsFound;
  }

  ESLMemory* addMemory(CarbonCfgMemory* mem)
  {
    return new ESLMemory(this, mem);
  }

  ESLMemory* findSubComponentMemory(CarbonCfgMemory* mem)
  {
    for (int i=0; i<childCount(); i++)
    {
      QTreeWidgetItem* childItem = child(i);
      MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(childItem);
      if (item->getNodeType() == MemEditorTreeItem::ESLMemoryNode)
      {
        ESLMemory* memNode = dynamic_cast<ESLMemory*>(item);
        if (memNode->getMemory() == mem)
          return memNode;
      }
    }

    return NULL;
  }

  int removeSubComponentMemory(CarbonCfgMemory* mem)
  {
    int childIndex = -1;

    for (int i=0; i<childCount(); i++)
    {
      QTreeWidgetItem* childItem = child(i);
      MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(childItem);
      if (item->getNodeType() == MemEditorTreeItem::ESLMemoryNode)
      {
        ESLMemory* memNode = dynamic_cast<ESLMemory*>(item);
        if (memNode->getMemory() == mem)
        {
          childIndex = i;
          removeChild(memNode);
          break;
        }
      }
    }

    return childIndex;
  }


  CarbonCfgMemory* findSubComponentMemory(const QString& memName)
  {
    for (UInt32 i=0; i<mSubComponent->numMemories(); i++)
    {
      CarbonCfgMemory* mem = mSubComponent->getMemory(i);
      if (memName == mem->getName())
        return mem;
    }
    return NULL;
  }

  QString getName()
  {
    return mSubComponent->getCompName();
  }

  CarbonCfg* getSubComponent()
  {
    return mSubComponent;
  }


  void setName(const QString& name)
  {
    setBoldText(0, name);
    UtString n;
    n << name;
    mSubComponent->putCompName(n.c_str());
  }

  // Delete operation
  class Delete : public MemUndoData
  {
  public:
    CarbonCfg* mCfg;
    CarbonCfg* mSubComponent;
    QString mSubComponentName;

    int mPos; // position in ccfg structure
    int mChildPos; // position in the gui

  protected:
    void undo()
    {
      qDebug() << "Undo Delete SubComponent" << text();

      UtString compName;
      compName << mSubComponentName;

      mCfg->restoreSubCompRedo(mSubComponent);

      QTreeWidgetItem* parent = getParentItem();

      ESLMemorySubComponent* pComp = new ESLMemorySubComponent(parent, mSubComponent);

      QList<MemEditorTreeItem::NodeType> nonExpandNodes;
      nonExpandNodes.append(MemEditorTreeItem::MemBlockCustomCodes);
      nonExpandNodes.append(MemEditorTreeItem::MemCustomCodes);
      nonExpandNodes.append(MemEditorTreeItem::CompCustomCodes);

      getTree()->expandChildren(pComp, nonExpandNodes);   

      pComp->setSelected(true);
      getTree()->scrollToItem(pComp);
      getTree()->setModified(false);
    }

    void redo()
    {
      qDebug() << "Delete SubComponent" << text();

      QTreeWidgetItem* parent = getParentItem();
      QTreeWidgetItem* item = getItem();

      mChildPos = parent->indexOfChild(item);

      item->setSelected(false);

      mCfg->removeSubCompUndo(mSubComponent);

      parent->removeChild(item);

      getTree()->setModified(true);
    }
  };

private:

private:
  CarbonCfg* mSubComponent;
  CustomCodesRootNode* mCodes;
  ESLMemoryDisassembler* mDisassembler;

};


#endif

