//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "DlgTieNet.h"

DlgTieNet::DlgTieNet(QWidget *parent)
    : QDialog(parent)
{
  ui.setupUi(this);
  ui.textBrowser->viewport()->setBackgroundRole(QPalette::Button);
  ui.textBrowser->viewport()->setForegroundRole(QPalette::ButtonText);
}

DlgTieNet::~DlgTieNet()
{
}

void DlgTieNet::putValue(const char* value)
{
  ui.lineEdit->setText(value);
}
void DlgTieNet::on_buttonBox_accepted()
{
  mValue << ui.lineEdit->text();
  accept();
}

void DlgTieNet::on_buttonBox_rejected()
{
  reject();
}

const char* DlgTieNet::getValue()
{
  return mValue.c_str();
}

