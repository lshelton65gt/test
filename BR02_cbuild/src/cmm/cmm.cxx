//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2014 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "cmm.h"

#include <QWorkspace>
#include <QSignalMapper>
#include <QtGui>

#include "CQtWizardContext.h"
#include "util/ArgProc.h"
#include "util/CarbonVersion.h"
#include "util/UtShellTok.h"
#include "gui/CQt.h"
#include "Mdi.h"
#include "MdiTextEditor.h"
#include "MdiProject.h"
#include "MdiCoware.h"
#include "MdiMaxsim.h"
#include "MdiSystemC.h"
#include "MdiDirectives.h"
#include "MdiRuntime.h"
#include "MdiOnDemandTrace.h"
#include "MdiMV.h"
#include "MdiMemoryMaps.h"
#include "MdiRegisterEditor.h"
#include "MdiPackageTool.h"
#include "MdiPlugin.h"
#include "MdiWebView.h"
#include "QTextEditor.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonProject.h"
#include "RichOutputWindow.h"
#include "SettingsEditor.h"
#include "SettingsContainer.h"
#include "Config.h"
#include "DlgNewProject.h"
#include "DlgPreferences.h"
#include "PropertyEditor.h"
#include "RemoteConsole.h"
#include "CarbonConsole.h"
#include "ErrorInfoWidget.h"
#include "ProjectWidget.h"
#include "WorkspaceModelMaker.h"
#include "CarbonFiles.h"
#include "ProcessTree.h"
#include "DlgDriveMappings.h"
#include "DlgPreferences.h"
#include "SpiritXML.h"
#include "DlgFindInFiles.h"
#include "ModelKitWizard.h"

#include "CarbonConsole.h"
#include "ScrCcfg.h"
#include "ScrProject.h"
#include "ScrCarbonDB.h"
#include "ScrApplication.h"
#include "DesignXML.h"

#include "ScriptingEngine.h"
#include "JavaScriptAPIs.h"

#include "EmbeddedMono.h"

#if pfLINUX || pfLINUX64
#include <signal.h>
#include <unistd.h>

struct sigaction oldHandler;

int gAppExitCode = 0;
bool gAppIsShuttingDown = false;

void myHandler(int sig, siginfo_t* siginfo, void* context)
{
  qDebug() << "Signal Handler: sig:" << sig << "context" << context << "code:" << siginfo->si_code;  
  if (siginfo->si_code == SEGV_MAPERR)
  {
    qDebug() << "Address not mapped to object";
  }
  else if (siginfo->si_code == SEGV_ACCERR)
  {
    qDebug() << "Invalid permissions for mapped object";
  }
 
  qDebug() << "Segfault was handled and app exitCode:" << gAppExitCode << "app shutdown:" << gAppIsShuttingDown;
  exit(gAppExitCode);
}

#endif

QFile* cmmLogFile = NULL;
QString sLogFilePath;
cmm* theApp = NULL;
extern const char* gCarbonVersion();

UInt32 replaceSubstring(UtString* str, const char* oldStr, const char* newStr) {
  if (strcmp(oldStr,newStr) == 0)
    return 0;

  size_t pos;
  UInt32 num = 0;
  while ((pos = str->find(oldStr)) != UtString::npos) {
    str->replace(pos, strlen(oldStr), newStr);
    ++num;
  }
  return num;
}

bool cmm::isBatchMode() const
{
  return checkArgument("-build");
}

bool replaceWithVar(QString& absPath, const QString& varName, const QString& varPath)
{
  if (absPath.contains(varPath))
  {
    QString argName = QString("${%1}").arg(varName);
    absPath.replace(varPath, argName);
    return true;
  }
  else
    return false;
}

// Take in an absolute path and
// try to convert it to env. variable references



void cmm::setEnvironment(const QString& name, const QString& value)
{
  if (value.isEmpty())
    mEnvironment.remove(name);
  else
    mEnvironment[name] = value;
}


QString cmm::normalizePath(const QString& absolutePath)
{
  QString result = absolutePath;

  // Don't double convert
  if (result.contains('$'))
    return absolutePath;

  // Longer paths first
  QString vCarbonOutput = mEnvironment["CARBON_OUTPUT"];
  QString vCarbonProject = mEnvironment["CARBON_PROJECT"];

  if (!vCarbonOutput.isEmpty() && replaceWithVar(result, "CARBON_OUTPUT", vCarbonOutput))
    return result;
  
  if (!vCarbonProject.isEmpty() && replaceWithVar(result, "CARBON_PROJECT", vCarbonProject))
    return result;



  for (quint32 i=0; i<numEnvVariables(); i++)
  {
    QString varName = getEnvVariable(i);
    
    if (mEnvironment.contains(varName))
    {
      if (replaceWithVar(result, varName, mEnvironment[varName]))
        return result;
    }
  }

  return result;
}

QString cmm::expandPath(const QString& normalizedPath)
{
  QString inputPath = normalizedPath;
  QString result = normalizedPath;

  // Longer paths first
  QString vCarbonOutput = mEnvironment["CARBON_OUTPUT"];
  if (!vCarbonOutput.isEmpty())
  {
    QString varName = QString("${%1}").arg("CARBON_OUTPUT");
    if (normalizedPath.contains(varName))
    {
      result = inputPath.replace(varName, vCarbonOutput);
      return result;
    }
  }


  QString vCarbonProject =  mEnvironment["CARBON_PROJECT"];
  if (!vCarbonProject.isEmpty())
  {
    QString varName = QString("${%1}").arg("CARBON_PROJECT");
    if (normalizedPath.contains(varName))
    {
      result = inputPath.replace(varName, vCarbonProject);
      return result;
    }
  }

  // process all variables
  bool matchFound = false;
  for (quint32 i=0; i<numEnvVariables(); i++)
  {
    QString envVar = getEnvVariable(i);
    if (!envVar.isEmpty() && mEnvironment.contains(envVar))
    {
      matchFound = true;
      QString varName = QString("${%1}").arg(envVar);
      QString varValue = mEnvironment[envVar];
      inputPath = inputPath.replace(varName, varValue);
    }
  }

  UtString expandedPath;
  UtString inpath; inpath << inputPath;
  UtString err;
  if (OSExpandFilename(&expandedPath, inpath.c_str(), &err))
    return expandedPath.c_str();
  else if (matchFound)
    return inputPath;
  else
    return normalizedPath;
}

void cmm::removeVariable(const QString& varName)
{
  mEnvVars.remove(varName);
}

void cmm::removeAllEnvironmentVariables()
{
  qDebug() << "removing all vars";
  mEnvVars.clear();
  qDebug() << "map has" << numEnvVariables();
}

QString cmm::lookupVariable(const QString& var)
{
  if (mEnvVars.contains(var))
    return mEnvVars[var];
  else
    return "";
}

QString cmm::getEnvVariable(quint32 index)
{
  return mEnvVars.keys()[index];  
}

void cmm::addEnvVar(const QString& varName)
{
  qDebug() << "adding variable" << varName;
  mEnvVars[varName] = varName;
}

void cmmRenameAndTrimLogs(const char* homePath)
{
  QSettings defSettings;
  QDir qd(homePath);
  qd.mkpath(homePath);

  qd.setFilter(QDir::Files);

  QStringList filters;
  filters << "ModelStudio.log.*";
  qd.setNameFilters(filters);

  QMap<int, QString> logMap;

  foreach(QFileInfo fileInfo, qd.entryInfoList())
  {
    int num = fileInfo.suffix().toInt();
    logMap[num]= fileInfo.absoluteFilePath();
  }
  
  QVariant vNumLogs = defSettings.value(PREF_GEN_NUMLOGS, 5);
  int logSize = vNumLogs.toInt();

  // Reverse the list
  QList<int> descendingKeys;
  foreach (int logNumber, logMap.keys())
    descendingKeys.push_front(logNumber);
 
  int newLogNumber = 1;
  if (descendingKeys.count() > 0)
    newLogNumber = descendingKeys[0] + 1;

  // Rename Modelstudio.log to this hightest number +1;
  UtString fileName;
  fileName << "ModelStudio.log";
  UtString defaultPath;
  OSConstructFilePath(&defaultPath, homePath, fileName.c_str());
  QFileInfo currentLog(defaultPath.c_str());
  if (currentLog.exists())
  {
    QFile lf(defaultPath.c_str());
    QString newName = QString("%1/ModelStudio.log.%2")
      .arg(homePath)
      .arg(newLogNumber);
    lf.rename(newName);
  }   
  int keepIndex = 0;
  foreach (int logNumber, descendingKeys)
  {
    if (keepIndex >= (logSize-1))
    {
      QFile delLog(logMap[logNumber]);
      delLog.remove();
    }
    keepIndex++;
  }
}
   
void cmmMessageOutput(QtMsgType type, const char *msg)
{
  QSettings defSettings;
  QVariant vNumLogs = defSettings.value(PREF_GEN_NUMLOGS, 5);

  if (cmmLogFile == NULL)
  {
    UtString homePath;
    homePath << (QDir::homePath()) << "/.carbon";

    cmmRenameAndTrimLogs(homePath.c_str());

    UtString defaultPath;
    UtString fileName;
    fileName << "ModelStudio.log";
    OSConstructFilePath(&defaultPath, homePath.c_str(), fileName.c_str());

    QFileInfo fi(defaultPath.c_str());

    UtString logName;
    logName << fi.absoluteFilePath();

    sLogFilePath = logName.c_str();

    cmmLogFile = new QFile(fi.absoluteFilePath());
    cmmLogFile->open(QFile::WriteOnly|QFile::Unbuffered);
  }

  switch (type)
  {
     case QtDebugMsg:
       cmmLogFile->write(msg);
       cmmLogFile->write("\n");
       break;
     case QtWarningMsg:
       fprintf(stderr, "ModelStudio: Warning: %s\n", msg);
       break;
     case QtCriticalMsg:
       fprintf(stderr, "ModelStudio: Critical: %s\n", msg);
       break;
     case QtFatalMsg:
       fprintf(stderr, "ModelStudio: Fatal: %s\n", msg);
       break;
  }
}

void cmm::shutdown(int exitCode)
{
  mExitCode = exitCode;
  QTimer::singleShot(100, this, SLOT(onShutdown()));
}

void cmm::onShutdown()
{
  if (!mDebugWindows)
  {
    if (!mShutdownBegun)
    {
#if pfLINUX || pfLINUX64
      gAppIsShuttingDown = true;
      gAppExitCode = mExitCode;
#endif
      mShutdownBegun = true;
      qDebug() << "onShutdown exitCode new" << mExitCode;
#if pfLINUX || pfLINUX64
      if (checkArgument("-modelKitRegen"))
      {
        qDebug() << "onShutdown phase 1 batch exitCode using kill()" << mExitCode;
        kill(getpid(), mExitCode);
        qDebug() << "onShutdown phase 1 batch exitCode using kill() should never get here" << mExitCode;
//        qApp->exit(mExitCode);
      }
#endif
      qApp->processEvents();
      qDebug() << "onShutdown finished events" << mExitCode;
      qApp->exit(mExitCode);
      qDebug() << "onShutdown after exit" << mExitCode;

#if pfLINUX || pfLINUX64
      kill(getpid(), mExitCode); // kill thyself      
#endif
    }
    else
    {
      qDebug() << "onShutdown shutting down exitCode" << mExitCode;
#if pfLINUX || pfLINUX64
      if (checkArgument("-modelKitRegen"))
      {
        qDebug() << "onShutdown phase 2 batch exitCode using kill()" << mExitCode;
        kill(getpid(), mExitCode); // kill thyself
        qDebug() << "onShutdown phase 2 batch exitCode using kill() should never get here" << mExitCode;
//        exit(mExitCode);
      }
#endif
      qApp->closeAllWindows();
      qApp->processEvents();
      exit(mExitCode);
    }
  }
  else
    qDebug() << "Shutdown aborted";
}


void cmm::showEvent(QShowEvent* ev)
{
  qDebug() << "Shown" << ev;
}


void cmm::closeTab()
{
  int index = ui.tabWidget->currentIndex();

  QWidget* tw = ui.tabWidget->widget(index);
  QVariant qv = tw->property("CarbonTab");
  if (qv.isValid())
  {
    QWidget* w = (QWidget*)qv.value<void*>();

    w->close();
  }
}

void cmm::openRecent(const char* fileName)
{
  UtString projFile;
  QFileInfo fi(fileName);
  projFile << fileName;
  if (fi.exists())
  {
    qDebug() << "open file: " << projFile.c_str();
    if (mContext->getCarbonProjectWidget()->project())
      mContext->getCarbonProjectWidget()->closeProject();
    mContext->getCarbonProjectWidget()->loadProject(projFile.c_str());
    if (!mTemplateProject->openDocument(this, projFile.c_str()))
      mContext->getCarbonProjectWidget()->closeProject();
    updateRecentFile(projFile.c_str());
  }
  else
    qDebug() << "Unable to open recent file, not found: " << fileName;
}

void cmm::openRecentFile()
{
  QAction *action = qobject_cast<QAction *>(sender());
  if (action)
  {
    UtString projFile;
    QVariant v = action->data();
    QString fileName = v.toString();
    
    QFileInfo fi(fileName);

    if (fi.exists())
    {
      projFile << fileName;
      openRecent(projFile.c_str());
    }
    else
    {
      UtString projFile;
      UtString msg;
      msg << "Project file does not exist: " << fi.fileName();
      projFile << fileName;
      QMessageBox::critical(NULL, MODELSTUDIO_TITLE, msg.c_str());
      updateRecentFile(projFile.c_str(), true);
    }
  }
}

QString strippedName(const QString &fullFileName)
{
  return QFileInfo(fullFileName).fileName();
} 
void cmm::updateRecentFileActions()
{
   QSettings settings("ModelStudio", "Recent Projects");
   QStringList files = settings.value("recentFileList").toStringList();

   int numRecentFiles = qMin(files.size(), (int)MaxRecentFiles);

   for (int i = 0; i < numRecentFiles; ++i) {
       QString text = tr("&%1 %2").arg(i + 1).arg(strippedName(files[i]));
       recentFileActs[i]->setText(text);
       recentFileActs[i]->setData(files[i]);
       recentFileActs[i]->setVisible(true);
       recentFileActs[i]->setStatusTip(files[i]);
   }
   for (int j = numRecentFiles; j < MaxRecentFiles; ++j)
       recentFileActs[j]->setVisible(false);

   //separatorAct->setVisible(numRecentFiles > 0);
}

void cmm::updateRecentFile(const char* fileName, bool removeFile)
{
  if (!mSquishMode)
  {
    QSettings settings("ModelStudio", "Recent Projects");
    QStringList files = settings.value("recentFileList").toStringList();
    files.removeAll(fileName);
    
    if (!removeFile)
      files.prepend(fileName);

    while (files.size() > MaxRecentFiles)
      files.removeLast();

    settings.setValue("recentFileList", files);

    foreach (QWidget *widget, QApplication::topLevelWidgets())
    {
      cmm *pThis = qobject_cast<cmm *>(widget);
      if (pThis)
        pThis->updateRecentFileActions();
    }
  }
}

void cmm::projectModified(bool value)
{
  setWindowModified(value);
}

bool cmm::checkArgument(const QString& argument)
{
  if (theApp && theApp->mColonValues.contains(argument))
     return true;

  int nArgs = qApp->arguments().count();
  for (int i=1; i<nArgs; i++)
  {
    QString arg = qApp->arguments()[i];
    if (arg == argument)
      return true;
  }
  return false;
}

QString cmm::argumentValue(const QString& argument)
{
  QString value;
  int nArgs = qApp->arguments().count();

  if (theApp && theApp->mColonValues.contains(argument))
  {
    return theApp->mColonValues[argument];
  }

  for (int i=1; i<nArgs; i++)
  {
    QString arg = qApp->arguments()[i];
    if (arg == argument)
    {
      if (i+1 < nArgs)
      {
        value = qApp->arguments()[i+1];
        return value;
      }
      break;
    }
  }

  return value;
}


bool cmm::checkArgument(const char* argument)
{
  if (theApp && theApp->mColonValues.contains(argument))
     return true;

  int nArgs = qApp->arguments().count();
  for (int i=1; i<nArgs; i++)
  {
    QString arg = qApp->arguments()[i];
    if (arg == argument)
    {
      if (theApp == NULL)
        return true;

      if (!theApp->mSwitchesWithArgs.contains(arg))
        return true;
      else return (i+1 < nArgs);
    }
  }
  return false;
}

void cmm::loadBuiltinPage()
{
  UtString welcomeName;
  welcomeName = "qrc:/cmm/Resources/welcome.htm";
  if (mContext->getCanCreateCoware() && !mContext->getCanCreateMaxsim())
    welcomeName = "qrc:/cmm/Resources/welcome-coware.htm";
  else if (!mContext->getCanCreateCoware() && mContext->getCanCreateMaxsim())
    welcomeName = "qrc:/cmm/Resources/welcome-arm.htm";
    mContext->getCarbonProjectWidget()->openSource(welcomeName.c_str(), -1, "HTMLBrowser");
}

void cmm::loadFinished(bool result)
{
  if (!result && !mWelcomePageLoaded)
  {
    ui.widgetWorkspace->closeActiveWindow();
    loadBuiltinPage();
  }

  if (mWebView)
    qDebug() << "Page loaded" << mWebView->url();

  mWelcomePageLoaded = true;
  mProgressBar->setValue(0);
}


void cmm::urlChanged( const QUrl & url )
{
  qDebug() << "url Changed" << url;
}


void cmm::loadProgress(int progress)
{
  mProgressBar->setValue(progress);
}

void cmm::loadStarted()
{
  mProgressBar->setValue(0);
}

void cmm::statusBarMessage(const QString& msg)
{
  ui.statusBar->showMessage(msg);
}


cmm::cmm(CQtWizardContext* cqt, QWidget *parent, Qt::WFlags flags)
: QMainWindow(parent, flags)
{
  EmbeddedMono::Instance(this);

  theApp = this;
  mShutdownBegun = false;
  mBeginShutdown = false;
  mFindDialog = NULL;
  mDlgFind = NULL;
  mExitCode = 0;
  mShutdownOnDialog = false;
  mSaveProjOnShutdown = true;
  mDebugWindows = false;
  mShutdownCount = 0;
  mWelcomePageLoaded = false;
  mVariablesProjectScope = true;
  mVSVersion = tr("");
  mHomePage = tr("");

  mWebView = NULL;

  mWorkingDirectory = QDir::current().absolutePath();

  mSwitchesWithArgs << "-modelKitAnswers";
  mSwitchesWithArgs << "-modelKit";
  mSwitchesWithArgs << "-modelKitRevision";
  mSwitchesWithArgs << "-modelKitManifest";
  mSwitchesWithArgs << "-modelKitTestAnswers";
  mSwitchesWithArgs << "-modelKitTestScript";
  mSwitchesWithArgs << "-modelKitTestDir";
  mSwitchesWithArgs << "-projectName";
  mSwitchesWithArgs << "-project";
  mSwitchesWithArgs << "-projectDir";
  mSwitchesWithArgs << "-armRoot";
  mSwitchesWithArgs << "-script";
  mSwitchesWithArgs << "-dbTest";
  mSwitchesWithArgs << "-monoDebugger";
  mSwitchesWithArgs << "-monoLoad";


  mColonSwitches << "-modelKitTestOutputDir";
  mColonSwitches << "-attributeFile";

  mSwitches << "-api";
  mSwitches << "-classGen";
  mSwitches << "-modelKitRegen";
  mSwitches << "-batch";
  mSwitches << "-compile";
  mSwitches << "-scriptDebugger";
  mSwitches << "-debugWindows";
  mSwitches << "-modelKitDump";
  mSwitches << "-modelKitSCUseComponentName";
  mSwitches << "-modelKitProfileGenerate";
  mSwitches << "-modelKitProfileUse"; 
  mSwitches << "-modelKitSystemCExport"; 
  mSwitches << "-noLog";
  mSwitches << "-debugEmbeddedModels";
  mSwitches << "-allowDebugger";

  QString id = qApp->sessionId().replace("-", "").replace("{","").replace("}","");

  quint32 randSeed = id.right(8).toUInt(0, 16);

  qsrand(randSeed);

  foreach(QString envPair, QProcess::systemEnvironment())
  {
    int equals = envPair.indexOf('=');
    if (equals != -1)
    {
      QString varName = envPair.left(equals);
      QString varValue = envPair.mid(equals+1);
      mEnvironment[varName] = varValue;
    }
  }

/*
  try 
  {
    throw 20;
  }
  catch (int e)
  {
    qDebug() << "Caught" << e;
  }
*/

  QCoreApplication::setOrganizationName("Carbon Design Systems");
  QCoreApplication::setOrganizationDomain("carbondesignsystems.com");
  QCoreApplication::setApplicationName("Model Studio");

#ifndef _DEBUG
  if (!checkArgument("-modelKit") && !checkArgument("-batch"))
  {
    if (!checkArgument("-noLog"))
      qInstallMsgHandler(cmmMessageOutput);
  }
#endif
  showWizard = false;

  QString carbonArch;
  const char* envCarbonArch = getenv("CARBON_ARCH");
  if (envCarbonArch)
    carbonArch = envCarbonArch;
  else
  {
  // setup path to plugins
#if pfLINUX || pfLINUX64
  carbonArch = "Linux";
#elif pfWINDOWS
  carbonArch = "Win";
#endif
  }

  QString pluginsDir = QString("${CARBON_HOME}/%1/lib/plugins").arg(carbonArch);
  UtString uPlugins; uPlugins << pluginsDir;

  UtString expandedName;
  UtString err;
  if (OSExpandFilename(&expandedName, uPlugins.c_str(), &err))
  {
    QString newPath = expandedName.c_str();
    QCoreApplication::addLibraryPath(newPath);
    qDebug() << "Plugins path(s)" << QCoreApplication::libraryPaths();
  }
  else
    qDebug() << "Warning: Unable to expand plugins" << err.c_str();

// Windows handled differently, not statically linked.
#if pfWINDOWS
  QString pluginsDir1 = QString("${CARBON_HOME}/%1/bin/plugins").arg(carbonArch);
  UtString uPlugins1; uPlugins1 << pluginsDir1;

  UtString expandedName1;
  UtString err1;
  if (OSExpandFilename(&expandedName1, uPlugins1.c_str(), &err1))
  {
    QString newPath = expandedName1.c_str();
    QCoreApplication::addLibraryPath(newPath);
    qDebug() << "Plugins path(s)" << QCoreApplication::libraryPaths();
  }
  else
    qDebug() << "Warning: Unable to expand plugins" << err.c_str();
#endif

  mDebugWindows = checkArgument("-debugWindows");

  mSquishMode = checkArgument("-squish");

  ui.setupUi(this);


  DlgPreferences prefs;
  mVariablesProjectScope = prefs.getGeneralPreference(DlgPreferences::GeneralVariableScope).toBool();
  mVSVersion = prefs.getGeneralPreference(DlgPreferences::GeneralVSVersion).toString();
  mHomePage = prefs.getGeneralPreference(DlgPreferences::GeneralHomePage).toString();

  qDebug() << "cms started in" << mWorkingDirectory;

  ui.centralWidget->installEventFilter(this);

  QTabWidget* tw=ui.tabWidget;

  const QFontMetrics fm = tw->fontMetrics();
  tw->setMaximumHeight(fm.height() + 2);
  QToolButton *closeTabButton = new QToolButton(this);
  QPalette pal = palette();
  pal.setColor(QPalette::Active, QPalette::Button, pal.color(QPalette::Active, QPalette::Window));
  pal.setColor(QPalette::Disabled, QPalette::Button, pal.color(QPalette::Disabled, QPalette::Window));
  pal.setColor(QPalette::Inactive, QPalette::Button, pal.color(QPalette::Inactive, QPalette::Window));

  closeTabButton->setAutoRaise(true);
  closeTabButton->setIcon(QIcon(":/cmm/Resources/closebutton.png"));
  QSize size(9,8);
  closeTabButton->setIconSize(size);
  closeTabButton->setPalette(pal);
  ui.tabWidget->setCornerWidget(closeTabButton, Qt::TopRightCorner);

  connect(closeTabButton, SIGNAL(clicked()), this, SLOT(closeTab()));
  
  mCQt = cqt;

  mCQt->setIgnoreRegistration(true);

  mDocManager = new MDIDocumentManager(this);

  mDocManager->putWorkspace(ui.widgetWorkspace);

  mContext = new CarbonMakerContext();
  
  mContext->putModelStudio(this);
  mContext->putQtContext(mCQt);
  mContext->putDocumentManager(mDocManager);
  mContext->putWorkspace(ui.widgetWorkspace);
  mContext->putMainWindow(this);
  mContext->putSourceTabWidget(ui.tabWidget);

  ui.tabWidget->removeTab(0);

  mProgressBar = new QProgressBar(this);
  mProgressBar->setMaximumWidth(150);
  mProgressBar->setMaximumHeight(15);
  mProgressBar->setRange(0,100);
  mProgressBar->setValue(0);


  // Carbon ModelMaker profile
  mWorkspaceModelMaker = new WorkspaceModelMaker(mProgressBar, ui.statusBar);

  // Default workspace
  mWorkspace = mWorkspaceModelMaker;

  mContext->putWorkspaceModelMaker(mWorkspaceModelMaker);

  mTemplateProject = new MDIProjectTemplate(mContext);
  mDocManager->addDocTemplate(mTemplateProject);

  mTemplateMaxsim = new MDIMaxsimTemplate(mContext);
  mDocManager->addDocTemplate(mTemplateMaxsim);

  mTemplateCoware = new MDICowareTemplate(mContext);
  mDocManager->addDocTemplate(mTemplateCoware);

  mTemplateSystemC = new MDISystemCTemplate(mContext);
  mDocManager->addDocTemplate(mTemplateSystemC);

  mTemplateDirectives = new MDIDirectivesTemplate(mContext);
  mDocManager->addDocTemplate(mTemplateDirectives);

  mTemplateRegisters = new MDIRegisterEditorTemplate(mContext);
  mDocManager->addDocTemplate(mTemplateRegisters);

  mTemplateMemoryMaps = new MDIMemoryMapsTemplate(mContext);
  mDocManager->addDocTemplate(mTemplateMemoryMaps);

  mTemplateModelValidation = new MDIMVTemplate(mContext);
  mDocManager->addDocTemplate(mTemplateModelValidation);

  mTemplatePackageTool = new MDIPackageToolTemplate(mContext);
  mDocManager->addDocTemplate(mTemplatePackageTool);

  mTemplateRuntime = new MDIRuntimeTemplate(mContext);
  mDocManager->addDocTemplate(mTemplateRuntime);

  mTemplateOnDemandTrace = new MDIOnDemandTraceTemplate(mContext);
  mTemplateOnDemandTrace->putRuntimeTemplate(mTemplateRuntime);
  mDocManager->addDocTemplate(mTemplateOnDemandTrace);

  mTemplateHtmlBrowser = new MDITextBrowser();
  mDocManager->addDocTemplate(mTemplateHtmlBrowser);

  mTemplateWebBrowser = new MDIWebBrowser();
  mDocManager->addDocTemplate(mTemplateWebBrowser);

  mTemplatePlugin = new MDIPluginTemplate(mContext);
  mDocManager->addDocTemplate(mTemplatePlugin);

  // Make sure this one is last, because it's filter is *.* to catch all files
  mTemplateTextEditor = new MDITextEditorTemplate();
  mDocManager->addDocTemplate(mTemplateTextEditor);

  // Init the workspace
  mWorkspace->initialize(mContext);

  mDocManager->initializeTemplates();

  connect(ui.tabWidget, SIGNAL(currentChanged(int)), this, SLOT(tabSelected(int)));
  connect(ui.widgetWorkspace, SIGNAL(windowActivated(QWidget*)), this, SLOT(windowActivated(QWidget*))); 
  connect(ui.widgetWorkspace, SIGNAL(windowActivated(QWidget *)), this, SLOT(updateMenus()));

  windowMapper = new QSignalMapper(this);
  connect(windowMapper, SIGNAL(mapped(QWidget*)), ui.widgetWorkspace, SLOT(setActiveWindow(QWidget*)));

  setWindowTitle(tr("Carbon Model Studio[*]"));

  ui.statusBar->addPermanentWidget(mProgressBar);

  createActions();

  createMenus();

  updateMenus();

  if (checkArgument("-batch"))
    setShutdownOnDialog(true);

  int nArgs = qApp->arguments().count();
  

  if (mContext->getCarbonProjectWidget())
  {
    if (!checkArgument("-modelKit") && !checkArgument("-batch"))
    {
      UtString homePage;
      if ( "Default" == mHomePage) {
        homePage << PREF_GEN_HOMEPAGE_DEFAULT_VALUE;
      } else if ( "Local" == mHomePage ) {
        homePage = "";          // an empty string means load the internal home page
      } else {
        homePage << mHomePage;  // this allows a string to be stored in the config file and it will be used as a URL
      }
      MDIWidget* mdiWidget = mTemplateWebBrowser->openDocument(this, homePage.c_str());
      
      mWebView = static_cast<QWebView*>(mdiWidget->getWidget());

      CQT_CONNECT(mWebView, loadStarted(), this, loadStarted());
      CQT_CONNECT(mWebView, loadFinished(bool), this, loadFinished(bool));
      CQT_CONNECT(mWebView, urlChanged( const QUrl & ), this, urlChanged( const QUrl & ));
      CQT_CONNECT(mWebView, loadProgress(int), this, loadProgress(int));
      CQT_CONNECT(mWebView, statusBarMessage(const QString&), this, statusBarMessage(const QString&));

      mContext->getWorkspace()->addWindow(mdiWidget->getWidget());
        // Did you forget to call initializeMDI in the widget constructor?
      mdiWidget->getWidget()->showMaximized();
      // a newly opened document cannot be modified
      mdiWidget->getWidget()->setWindowModified(mdiWidget->isWidgetModified());

      QWidget* tw = new QWidget();
      QVariant qv = qVariantFromValue((void*)mdiWidget->getWidget());

      tw->setProperty("CarbonTab", qv);

      QString tabName = "Carbon Website";
      QString tabTooltip = "http://www.carbondesignsystems.com";
      // This is wrong for tabs like Directives and PackageTool.

      int index = mContext->getSourceTabWidget()->addTab(tw, tabName);
      mContext->getSourceTabWidget()->setTabToolTip(index, tabTooltip);
      mContext->getSourceTabWidget()->setCurrentIndex(index);

      QVariant ti(index);
      mdiWidget->getWidget()->setProperty("CarbonTabPlaceholder", 
        qVariantFromValue((void*)tw));

      mdiWidget->getWidget()->setProperty("CarbonTabWidget", 
        qVariantFromValue((void*)mContext->getSourceTabWidget()));
    }

    CQT_CONNECT(mContext->getCarbonProjectWidget(), projectModified(bool), this, projectModified(bool));
  }

  bool openedRecent = false;
  if (nArgs > 1)
  {
    for (int i=1; i<nArgs; i++)
    {
      QString arg = qApp->arguments()[i];
      qDebug() << "processing arg" << arg;

      if (arg == "-project" && i<nArgs-1)
      {
        QString projFileArg = qApp->arguments()[i+1];
        i++;
        QFileInfo fi(projFileArg);
        if (fi.exists())
        {
          UtString projFile;
          // This will convert dos slashes to unix slashes
          projFile << fi.absoluteFilePath();
          mContext->getCarbonProjectWidget()->loadProject(projFile.c_str());
          mTemplateProject->openDocument(this, projFile.c_str());
          openedRecent = true;
          updateRecentFile(projFile.c_str());
        }
        else
        {
          UtString msg;
          UtString projFile;
          projFile << fi.absoluteFilePath();

          msg << "Project file does not exist: " << fi.fileName();
          QMessageBox::warning(this, MODELSTUDIO_TITLE, msg.c_str());
          updateRecentFile(projFile.c_str(), true);
        }
      }
      else 
      {
        if (mSwitches.contains(arg))
          continue;

        if (mSwitchesWithArgs.contains(arg))
        {
          qDebug() << "skipping value for" << arg;
          i++;
          continue;
        }

        foreach(QString cs, mColonSwitches)
        {
          if (arg.contains(cs))
          {
            int colonPos = arg.indexOf(':');
            if (colonPos != -1)
            {
              QString s = arg.left(colonPos);
              QString v = arg.mid(colonPos+1);
              qDebug() << "arg mapping" << s << " -> " << v;
              mColonValues[s] = v;
            }
          }          
        }

        if (mContext->getCarbonProjectWidget() && arg.left(1) != "-") 
        {
          UtString fname;
          QFileInfo fi(arg);
          fname << fi.absoluteFilePath(); // convert dos slashes
          if (fi.exists())
          {
            if (fi.completeSuffix() == "carbon")
            {
              mContext->getCarbonProjectWidget()->loadProject(fname.c_str());
              mTemplateProject->openDocument(this, fname.c_str());
              updateRecentFile(fname.c_str());
              openedRecent = true;
            }
            else
            {
              qDebug() << "openSource" << fname.c_str();
              mContext->getCarbonProjectWidget()->openSource(fname.c_str());        
            }
          }
          else
          {
            UtString msg;
            UtString projFile;
            projFile << fi.absoluteFilePath();
            msg << "File does not exist: " << fi.fileName();
            QMessageBox::warning(this, MODELSTUDIO_TITLE, msg.c_str());
            updateRecentFile(projFile.c_str(), true);
          }
        }
      }
    }
  }  
 
  qDebug() << "Finished processing args";

  if (!checkArgument("-batch") && !checkArgument("-modelKit") && !openedRecent && !mSquishMode)   
  {
    QSettings settings("ModelStudio", "Recent Projects");
    QStringList files = settings.value("recentFileList").toStringList();
    // special handling for non-project specific options here
    QSettings carbSettings("Carbon Design Systems", MODELSTUDIO_TITLE);
    QVariant vHadProject = carbSettings.value("hadProjectOpen");

    QSettings defSettings;
    QVariant vLast = defSettings.value(PREF_GEN_REOPENLAST, true);

    if (vLast.isValid())
    {
      bool openLast = vLast.toBool();
      if (vHadProject.isValid() && !vHadProject.toBool())
        openLast = false;

      if (openLast && files.count() > 0)
      {
        QString lastProj = files[0];
        UtString projName;
        projName << lastProj;
        openRecent(projName.c_str());
      }
    }

    if (files.count() == 0)
      showWizard = true;
  }

  QSettings settings("Carbon Design Systems","ModelStudio");

  const QString key = QString::number( (QT_VERSION >> 16) & 0xff )+ QLatin1String(".") + QString::number( (QT_VERSION >> 8) & 0xff ) + QLatin1String("/");
  QString ver = QString("%1/%2").arg(CARBON_RELEASE_ID).arg(gCarbonVersion());
  QString finalKey = QString("%1/%2").arg(key).arg(ver);
  QByteArray defaultState = settings.value(finalKey + QLatin1String("defaultWindowGeometry")).toByteArray();
  if (defaultState.isEmpty())
  {
    showMaximized();
    QByteArray winGeometry = saveGeometry();
    QByteArray mainWinState = saveState();
    settings.setValue(finalKey + QLatin1String("defaultWindowGeometry"), winGeometry);
    settings.setValue( finalKey + QLatin1String("defaultMainWindowState"), mainWinState );
  }

  // If we opened a project set the project name in the Title Bar
  if (mSquishMode)
  {
    showMaximized();
    QApplication::setActiveWindow(this);
  }

  if (!checkArgument("-batch") && !mSquishMode)
    readSettings();
  
 // Purposely create a script engine here, to pre-load 
  // the scripting dlls/.so's 
  ScriptingEngine engine;
  qDebug() << "Imported Extensions" << engine.importedExtensions();

  if (checkArgument("-modelKitTestOutputDir"))
  {
    QString toDir = argumentValue("-modelKitTestOutputDir");
    
    qDebug() << "-modelKitTestOutputDir" << toDir;

    QDir q(toDir);
    if (!q.exists())
    {
      qDebug() << "Creating Test Output Directory" << toDir;
      q.mkpath(toDir);
    }
  }

  // mWorkingDirectory (is where we started)
  if (checkArgument("-modelKitTestAnswers"))
  {
    mShutdownOnDialog = true;
    bool validArg = false;
    getArgumentPath("-modelKitTestAnswers", &validArg);
    if (!validArg)
    {
      qDebug() << "Error: No -modelKitTestAnswers file" << argumentValue("-modelKitTestAnswers");
      mExitCode = 1;
    }
  }

  if (checkArgument("-modelKitTestScript"))
  {
    mShutdownOnDialog = true;
    bool validArg = false;
    getArgumentPath("-modelKitTestScript", &validArg);
    if (!validArg)
    {
      qDebug() << "Error: No -modelKitTestScript file" << argumentValue("-modelKitTestScript");
      mExitCode = 1;
    }
  }


//
// Sometimes we get a crash during a modelkit regen on exit, handle that here upon exit
// with a catch of the SEGFAULT, but only for batch or modelkitregen otherwise
// crash (normally)
//
#if pfLINUX || pfLINUX64
  if (checkArgument("-batch") || checkArgument("-modelKitRegen"))
  {
    struct sigaction sigAct;
    int status = 0;
    sigAct.sa_handler = 0;
    sigAct.sa_sigaction = myHandler;
    sigfillset(&sigAct.sa_mask);
    sigAct.sa_flags = SA_SIGINFO;
    status = sigaction(SIGSEGV, &sigAct, &oldHandler);
    if (status != 0)
      qDebug() << "ERROR: Unable to install SIGSEGV handler";
  }
#endif

  if (mShutdownOnDialog && mExitCode != 0)
    return;

  if (JavaScriptAPIs::loadAPIs())
  {
    shutdown(0);
    return;
  }
}

QString cmm::getArgumentPath(const QString& arg, bool* foundValue)
{
  *foundValue = false;

  if (checkArgument(arg))
  {
    QString av = argumentValue(arg);
    QString path1 = av;
    QString path2 = QString("%1/%2").arg(mWorkingDirectory).arg(av);
    QFileInfo fi1(path1);
    QFileInfo fi2(path2);

    if (fi1.exists())
    {
      *foundValue = true;
      return path1;
    }
    else if (fi2.exists())
    {
      *foundValue = false;
      return path2;
    }
  }

  return "";
}

void cmm::apiPreparationFinished()
{
  if (mAPIs->savePrepared())
    qDebug() <<"Finished API Preparation";
  else
    qDebug() <<"Error Finished API Preparation";
}

void cmm::tabSelected(int index)
{
  qDebug() << "tab selected" << index;
  QWidget* tw = ui.tabWidget->widget(index);
  if (tw) 
  {
    QVariant qv = tw->property("CarbonTab");
    INFO_ASSERT(qv.isValid(), "Widget must define a MDI_DOCUMENT property");
    QWidget* w = (QWidget*)qv.value<void*>();
    ui.widgetWorkspace->setActiveWindow(w);
  }
}

void cmm::windowActivated(QWidget* win)
{
  if (win)
  {
    qDebug() << "window activated" << win->windowTitle();
    if (win->windowTitle().isEmpty())
      ui.tabWidget->setCurrentIndex(0);
  }

  mDocManager->windowActivated(win);

  MDIWidget*w = activeMdiWindow();
  if (w)
    w->widgetActivated();


}

void cmm::processArguments()
{
  int nArgs = qApp->arguments().count();
  for (int i=1; i<nArgs; i++)
  {
    QString arg = qApp->arguments()[i];
    if (arg == "-build")
    {
      hide();
      mContext->getCarbonProjectWidget()->compile(false, true, false);
    }
  }

  if (checkArgument("-dbTest"))
  {
    QFileInfo fi(argumentValue("-dbTest"));

    QString carbonDbFile = fi.absoluteFilePath();    
    QString testDir = fi.absolutePath();

    mContext->getCarbonProjectWidget()->closeProject();
  
    CarbonProject proj(NULL, mContext->getCarbonProjectWidget());
    proj.putProjectType(CarbonProject::DatabaseOnly);
    

    UtString projDir;
    projDir << testDir;

    UtString dbFile;
    dbFile << carbonDbFile;

    UtString newProjectfile;
    newProjectfile << proj.createNewProjectFromDatabase(this, projDir.c_str(), "testDb", dbFile.c_str());      
    
    openProjectFile(newProjectfile.c_str());

    QString answersFile = QString("%1/%2").arg(testDir).arg("request.answers");
    QString testScriptFile = QString("%1/%2").arg(testDir).arg("testModel.js");

    qDebug() << "Running test script";
    ScriptingEngine* te = new ScriptingEngine();

    KitTest* kt = new KitTest();

    kt->setModelKitName(theApp->argumentValue("-modelKit"));
    
    if (kt->loadPortalAnswers(answersFile))
    {
      QScriptValueList sargs;
      sargs.append(te->newQObject(kt));

      if (checkArgument("-scriptDebugger"))
        te->setEnableDebugger(true);

      qDebug() << "loading test script" << testScriptFile;
      te->runScript(testScriptFile);

      qDebug() << "calling testModel in script" << testScriptFile;
      QScriptValue sv = te->callScriptFunction("testModel", sargs);

      shutdown(sv.toBoolean() ? 0 : 1);
    }
    else
    {
      qDebug() << "ERROR: Failed to load:" << answersFile;
      shutdown(1);
    }
    
    shutdown(0);
  }

  if (checkArgument("-modelKit") && checkArgument("-projectDir") && checkArgument("-projectName"))
  {
    QString projDir = argumentValue("-projectDir");
    QString projName = argumentValue("-projectName");
    QString modelKitFile = argumentValue("-modelKit");

    QFileInfo pdfi(projDir);
    if (pdfi.isRelative())
      projDir = theApp->getWorkingDirectory() + "/" + projDir;
    
    QFileInfo mkfi(modelKitFile);
    if (mkfi.isRelative())
      modelKitFile = theApp->getWorkingDirectory() + "/" + modelKitFile;

    if (!checkArgument("-modelKitDump"))
    {
      qDebug() << "Creating kit" << projName << "dir" << projDir << "mkit" << modelKitFile;
      createModelKitProject(projDir, projName, modelKitFile);
    }
    else
    {
      dumpModelKit();
      shutdown(0);
    }
  }
  else if (checkArgument("-modelKit") && checkArgument("-modelKitRegen"))
  {
    qDebug() << "showing modelkit regen wizard";
    showKitWizard();

    QMainWindow* mw = theApp->getContext()->getMainWindow();
    if (!theApp->checkArgument("-debugWindows"))
      mw->show();

    qDebug() << "shutting down application";
    shutdown(0);
  }
  else if (checkArgument("-script"))
  {
    QString scriptFilename = argumentValue("-script");
    ScriptingEngine engine;

    if (checkArgument("-scriptDebugger"))
      engine.setEnableDebugger(true);

    engine.run(scriptFilename);
  }

  if (checkArgument("-monoLoad"))
  {
    QString value = argumentValue("-monoLoad");
    if (!value.isEmpty() && value.count(':') == 3)
    {
      QStringList parts = value.split(":");
      QString assembly = parts[0];
      QString ns = parts[1];
      QString klass = parts[2];
      QString method = parts[3];

      EmbeddedMono* mono = EmbeddedMono::Instance(this);

      // -monoLoad foo.dll:Namespace:Class:Method
    
      UtString arg1; arg1 << assembly;
      UtString arg2; arg2 << ns;
      UtString arg3; arg3 << klass;
      UtString arg4; arg4 << method;
      
      qDebug() << "Attempting to load" << assembly << ns << klass << method;

      MonoAssembly* assy = mono->LoadAssembly(arg1.c_str());
      if (assy)
      {
        MonoObject* obj = mono->ConstructObject(assy, arg2.c_str(), arg3.c_str());    
        if (obj)
        {
          void* mparams = NULL;
          MonoObject* exc = NULL;
          mono->InvokeMethod(assy, arg2.c_str(), arg3.c_str(), arg4.c_str(), 0, &mparams, obj, &exc); 
        }
        else
          qDebug() << "Unable to construct object" << ns << klass << method;
      }
      else
        qDebug() << "Unable to locate assembly" << assembly;
    }
    else
    {
      qDebug() << "Invalid arguments to -monoLoad";
    }
  }

}

void cmm::maybeShowWizard()
{

  if (showWizard)
  {
    showMaximized();
    on_actionNewProject_triggered();
  }
  showWizard = false;

  mContext->getCarbonProjectWidget()->setFocus();
}

bool cmm::eventFilter(QObject* obj, QEvent* ev)
{
  bool retValue = QMainWindow::eventFilter(obj, ev);
  if (ev->type() == QEvent::WindowBlocked)
  {
    if (mShutdownOnDialog)
    {
      mShutdownCount--;
      if (mShutdownCount > 0)
      {
        qDebug() << "object" << obj << "event" << ev;
        obj->dumpObjectInfo();   
        qDebug() << "Abort: Error, a Modal Dialog was blocked, abnormal exit";
        shutdown(2);
      }
    }
  }
  return retValue;
}

void cmm::closeEvent(QCloseEvent* event)
{
  bool hasProjectOpen = false;
  
  mBeginShutdown = true;

  qDebug() << "start closeEvent";

  ui.widgetWorkspace->closeAllWindows();

  hasProjectOpen = mContext->getCarbonProjectWidget()->project() ? true : false;
  CarbonConsole* console = mContext->getCarbonProjectWidget()->getConsole();
  if (console)
    console->closeEvent(event);

  MDIWidget* w = activeMdiWindow();
  if (w) 
  {
    MDIWidget* w = activeMdiWindow();
    qDebug() << "not closing, active mdi window" << w->userFriendlyName();
    event->ignore();
  }
  else if (event->isAccepted())
  {
    bool saveFile = true;
    bool closeProject = true;

    qDebug() << "checking for modified project";

    if (mContext->getCarbonProjectWidget()->isModified())
    {
      QMessageBox::StandardButton ret;
      ret = QMessageBox::warning(this, MODELSTUDIO_TITLE,
        "The Project has unsaved changes.\nDo you want to save your changes before closing?",
        QMessageBox::Save | QMessageBox::Discard
        | QMessageBox::Cancel);
      if (ret == QMessageBox::Save)
      {

        qDebug() << "saving project";
        saveFile = true;
        closeProject = true;
        console->setShuttingDown();
      }
      else if (ret == QMessageBox::Cancel)
      {
        qDebug() << "cancelled saving project";
        closeProject = false;
        saveFile = false;
      }
    }

    if (saveFile)
    {
      if (mSaveProjOnShutdown)
        mContext->getCarbonProjectWidget()->saveProject();

      writeSettings();
      if (cmmLogFile)
      {
        qDebug() << "shutting down normally";
        cmmLogFile->close();
      }
    }   

    if (closeProject)
    {
      qDebug() << "accepted close event";     
      event->accept();
    }
    else
    {
      qDebug() << "blocking close event";     
      event->ignore();
    }
  }

  if (event->isAccepted())
  {
    QSettings settings("Carbon Design Systems", MODELSTUDIO_TITLE);
    settings.setValue("hadProjectOpen", hasProjectOpen);
    qDebug() << "closing was accepted";
  }
  else
  {
    qDebug() << "closing was blocked";
  }
}

void cmm::compilationModeChanged(bool inProgress)
{
  ui.menuNew->setEnabled(!inProgress);
  ui.menuOpenRecentProject->setEnabled(!inProgress);
  ui.actionOpenProject->setEnabled(!inProgress);
}

cmm::~cmm()
{
  //delete mContext;
}

void cmm::on_actionExit_triggered()
{
  qApp->closeAllWindows();
}

inline QString getCmmVersionString()
{
    return QString::number( (QT_VERSION >> 16) & 0xff )
        + QLatin1String(".") + QString::number( (QT_VERSION >> 8) & 0xff );
}

void cmm::setProjectRemoteParams(const char* remoteWorkingDir, const char* remoteUsername, const char* remoteServer, const char* remoteCarbonHome, const char* remotePrefix)
{
  mContext->getCarbonProjectWidget()->project()->getProjectOptions()->putValue("Remote Working Directory", remoteWorkingDir);
  mContext->getCarbonProjectWidget()->project()->getProjectOptions()->putValue("Remote Username", remoteUsername);
  mContext->getCarbonProjectWidget()->project()->getProjectOptions()->putValue("Remote Server", remoteServer);
  mContext->getCarbonProjectWidget()->project()->getProjectOptions()->putValue("Remote CARBON_HOME", remoteCarbonHome);
  mContext->getCarbonProjectWidget()->project()->getProjectOptions()->putValue("Remote Command Prefix", remotePrefix);
}

void cmm::createEmptyProject(const QString& projDir, const QString& projName)
{
  mContext->getCarbonProjectWidget()->closeProject();
  CarbonProject proj(NULL, mContext->getCarbonProjectWidget());
  UtString uProjDir; uProjDir << projDir;
  UtString uProjName; uProjName << projName;
  UtString projFile(proj.createNewProject(uProjDir.c_str(), uProjName.c_str()));
  mContext->getCarbonProjectWidget()->loadProject(projFile.c_str());
  mTemplateProject->openDocument(this, projFile.c_str());
}

void cmm::on_actionNewProject_triggered()
{
  DlgNewProject dlg(this, mContext->getCarbonProjectWidget());
  if (dlg.exec() == QDialog::Accepted)
  {
    qDebug() << "new project dir: " << dlg.getProjectDirectory();

    QDir q;
    q.mkpath(dlg.getProjectDirectory());

    UtString remotePrefix;
    UtString remoteCarbonHome;

    CarbonProject* currentProject = mContext->getCarbonProjectWidget()->project();
    if (currentProject)
    {
      remotePrefix << currentProject->getProjectOptions()->getValue("Remote Command Prefix")->getValue();
      remoteCarbonHome << currentProject->getProjectOptions()->getValue("Remote CARBON_HOME")->getValue();
    }
    else
    {
      const QString key = getCmmVersionString() + QLatin1String("/");
      QSettings settings("Carbon Design Systems", "ModelStudio");

      QString cmdPrefix = settings.value("Remote Command Prefix").toString();
      remotePrefix << cmdPrefix;

      QString carbHome = settings.value("Remote CARBON_HOME").toString();
      remoteCarbonHome << carbHome;
    }

    switch (dlg.getProjectKind())
    {
    default:
    case DlgNewProject::EmptyProject:
      {
        mContext->getCarbonProjectWidget()->closeProject();
        CarbonProject proj(NULL, mContext->getCarbonProjectWidget());
        UtString projFile(proj.createNewProject(dlg.getProjectDirectory(), dlg.getProjectName()));
        mContext->getCarbonProjectWidget()->loadProject(projFile.c_str());
#if pfWINDOWS
        if (dlg.isRemoteCompilation())
          setProjectRemoteParams(dlg.getRemoteWorkingDirectory(), 
            dlg.getRemoteUsername(), dlg.getRemoteServer(), remoteCarbonHome.c_str(), remotePrefix.c_str());
#endif
        mTemplateProject->openDocument(this, projFile.c_str());
        updateRecentFile(projFile.c_str());
      }
      break;
    case DlgNewProject::FromExistingCommand:
      {
        mContext->getCarbonProjectWidget()->closeProject();
        CarbonProject proj(NULL, mContext->getCarbonProjectWidget());
        UtString projFile(proj.createNewProjectFromExisting(this, dlg.getProjectDirectory(), dlg.getProjectName()));
        mContext->getCarbonProjectWidget()->loadProject(projFile.c_str());
#if pfWINDOWS
        if (dlg.isRemoteCompilation())
          setProjectRemoteParams(dlg.getRemoteWorkingDirectory(), 
            dlg.getRemoteUsername(), dlg.getRemoteServer(), remoteCarbonHome.c_str(), remotePrefix.c_str());
#endif
        mTemplateProject->openDocument(this, projFile.c_str());
        updateRecentFile(projFile.c_str());
      }
      break;
    case DlgNewProject::FromCcfgCoware: // treated the same at this point
    case DlgNewProject::FromCcfgMaxsim:
      {
        mContext->getCarbonProjectWidget()->closeProject();
        UtString filter;
        if (dlg.getProjectKind() == DlgNewProject::FromCcfgCoware)
          filter << "Carbon CoWare Wizard File (*.ccfg)";
        else 
          filter << "Carbon SocDesigner Wizard File (*.ccfg)";

        QString fileName = QFileDialog::getOpenFileName(this, tr("Select Carbon Wizard File"), "", filter.c_str());
        if (fileName.length() > 0)
        {
          UtString ccfgFile;
          ccfgFile << fileName;
          QFileInfo fiCcfg(ccfgFile.c_str());
          CarbonProject proj(NULL, mContext->getCarbonProjectWidget());
          proj.putProjectType(CarbonProject::DatabaseOnly);
          const char* newProjFile = proj.createNewProjectFromCcfg(this, dlg.getProjectDirectory(), dlg.getProjectName(), ccfgFile.c_str(), dlg.getProjectKind());
          if (newProjFile)
          {
            UtString projFile(newProjFile);
            mContext->getCarbonProjectWidget()->loadProject(projFile.c_str());
#if pfWINDOWS
            if (dlg.isRemoteCompilation())
              setProjectRemoteParams(dlg.getRemoteWorkingDirectory(), 
                dlg.getRemoteUsername(), dlg.getRemoteServer(), remoteCarbonHome.c_str(), remotePrefix.c_str());
#endif
            mTemplateProject->openDocument(this, projFile.c_str());
            updateRecentFile(projFile.c_str());
          }
          else
          {
             QMessageBox::critical(this, MODELSTUDIO_TITLE, "Unable to create project");
          }
        }
      }
      break;
   case DlgNewProject::FromExistingDatabase:
      {
        mContext->getCarbonProjectWidget()->closeProject();

        QString filter = "Carbon Database (*.symtab.db *.io.db)";
        QString fileName = QFileDialog::getOpenFileName(this, tr("Select Carbon Wizard File"), "", filter);
        if (fileName.length() > 0)
        {
          UtString dbFile;
          dbFile << fileName;
          QFileInfo fiDb(dbFile.c_str());
          
          if (fiDb.exists())
          {
            CarbonProject proj(NULL, mContext->getCarbonProjectWidget());
            proj.putProjectType(CarbonProject::DatabaseOnly);
            UtString projFile(proj.createNewProjectFromDatabase(this, dlg.getProjectDirectory(), dlg.getProjectName(), dbFile.c_str()));

            if (projFile.length() > 0)
            {
              mContext->getCarbonProjectWidget()->loadProject(projFile.c_str());
#if pfWINDOWS
              if (dlg.isRemoteCompilation())
                setProjectRemoteParams(dlg.getRemoteWorkingDirectory(), 
                  dlg.getRemoteUsername(), dlg.getRemoteServer(), remoteCarbonHome.c_str(), remotePrefix.c_str());
#endif
              mTemplateProject->openDocument(this, projFile.c_str());
              updateRecentFile(projFile.c_str());
            }
          }
          else
          {
            UtString errMsg;
            errMsg << "Error, unable to create project the following do not exist:\n";
            if (!fiDb.exists())
              errMsg << "Database file: " << fiDb.filePath() << "\n";
            QMessageBox::critical(this, MODELSTUDIO_TITLE, errMsg.c_str());
          }
        }
      }
      break;

   case DlgNewProject::FromModelKit:
     {
       mContext->getCarbonProjectWidget()->closeProject();
       QString filter = "Carbon Model Kit (*.modelKit)";
       QString fileName = QFileDialog::getOpenFileName(this, tr("Select Carbon Model Kit"), "", filter);
       if (fileName.length() > 0)
         createModelKitProject(dlg.getProjectDirectory(), dlg.getProjectName(), fileName);
     }
     break;
    }
  }
}

void cmm::createModelKitProject(const QString& modelKitDir, const QString& projName, const QString& fileName)
{
  mContext->getCarbonProjectWidget()->closeProject();
  if (fileName.length() > 0)
  {
    UtString mkFile;
    mkFile << fileName;
    QFileInfo fiMK(mkFile.c_str());
    if (fiMK.exists())
    {
      CarbonProject proj(NULL, mContext->getCarbonProjectWidget());
      proj.putProjectType(CarbonProject::ModelKitProject);
      UtString uKitDir; uKitDir << modelKitDir;
      UtString uProjName; uProjName << projName;
      proj.createNewProjectFromModelKit(this, uKitDir.c_str(), uProjName.c_str(), mkFile.c_str());
      
      bool debugger = getenv("CARBON_MODELKIT_DEBUGGER");
      if (!debugger)
        shutdown(mExitCode);
    }
    else
    {
      UtString errMsg;
      errMsg << "Error, unable to create project the following do not exist:\n";
      if (!fiMK.exists())
        errMsg << "Modelkit file: " << fiMK.filePath() << "\n";
      QMessageBox::critical(this, MODELSTUDIO_TITLE, errMsg.c_str());
    }
  }
}

void cmm::on_actionOpenProject_triggered()
{ 
  qDebug() << "open proj......";
  QFileDialog fdlg;
  fdlg.setFilter(QDir::Hidden);
  QString fileName = fdlg.getOpenFileName(this,
                                 tr("Open Project"),
                                 ".",
                                 tr("Carbon Project (*.carbon)"));
  if (fileName.length() > 0)
  {
    UtString projFile;
    QFileInfo fi(fileName);
    projFile << fi.absoluteFilePath();

    if (fi.exists())
      openProjectFile(projFile.c_str());
    else
    {
      UtString msg;
      QMessageBox::warning(this, MODELSTUDIO_TITLE, projFile.c_str());
    }
  }
}

void cmm::on_actionNewFile_triggered()
{
  UtString defaultDir;
  CarbonProjectWidget* projWidget = mContext->getCarbonProjectWidget();
  CarbonProject* proj = NULL;
  if (projWidget)
    proj = projWidget->project();

  if (proj)
    defaultDir << proj->getProjectDirectory();
  else
    defaultDir << ".";

  QFileDialog dialog(this, "New File");
  dialog.setFileMode(QFileDialog::AnyFile);
  dialog.setFilter("New file (*.*)");
  dialog.setDirectory(defaultDir.c_str());
  QStringList fileNames;  
  if (dialog.exec())
    fileNames = dialog.selectedFiles();

  if (fileNames.count() > 0)
  {
    UtString filePath;
    filePath << fileNames[0];

    MDIWidget* mdiWidget = mTemplateTextEditor->createDocument(mContext->getMainWindow(), filePath.c_str());
    if (mdiWidget)
    {
      mContext->getWorkspace()->addWindow(mdiWidget->getWidget());
      // Did you forget to call initializeMDI in the widget constructor?
      mdiWidget->getWidget()->showMaximized();

      QFileInfo f(filePath.c_str());   
      QWidget* tw = new QWidget();
      QVariant qv = qVariantFromValue((void*)mdiWidget->getWidget());

      tw->setProperty("CarbonTab", qv);
      int index = mContext->getSourceTabWidget()->addTab(tw, f.fileName());
      mContext->getSourceTabWidget()->setTabToolTip(index, f.absoluteFilePath());
      mContext->getSourceTabWidget()->setCurrentIndex(index);

      QVariant ti(index);
      mdiWidget->getWidget()->setProperty("CarbonTabPlaceholder", 
        qVariantFromValue((void*)tw));

      mdiWidget->getWidget()->setProperty("CarbonTabWidget", 
        qVariantFromValue((void*)mContext->getSourceTabWidget()));

      if (proj)
      {
        CarbonFilesFolder* files = projWidget->getFilesFolder();
        if (files)
          files->addFile(filePath.c_str());
      }
    }
  }
}

void cmm::openProjectFile(const char* projectFile)
{
  if (mContext->getCarbonProjectWidget()->project())
    mContext->getCarbonProjectWidget()->closeProject();
  
  mContext->getCarbonProjectWidget()->loadProject(projectFile);
  if (!mTemplateProject->openDocument(this, projectFile))
    mContext->getCarbonProjectWidget()->closeProject();
  
  updateRecentFile(projectFile);
}

void cmm::on_actionFileOpen_triggered()
{
  CarbonProject* proj = mContext->getCarbonProjectWidget()->project();
  QString initialDir = ".";

  if (proj)
    initialDir = proj->getProjectDirectory();

  QStringList files = QFileDialog::getOpenFileNames(this, "Select one or more files",
    initialDir,
    "All Files (*)");  
  foreach (QString fileName, files)
  {
    UtString filePath;
    filePath << fileName;
    QFileInfo fi(fileName);
    QString suffix = fi.suffix().toLower();
    const char* fileType = "TextEditor";
    if ("css" == suffix || "csu" == suffix) // || "mjs" == suffix)
      fileType = NULL;
    mContext->getCarbonProjectWidget()->openSource(filePath.c_str(), 0, fileType);
  }
}

QDockWidget* cmm::findDockWidget(QString name)
{
 QMapIterator<QDockWidget*, QAction*> i(mDockWidgetActionMap);
 while (i.hasNext())
 {
   i.next();
   QDockWidget* w = i.key();
   QAction* a = i.value();
   if (a->text() == name)
     return w;
 }
 return NULL;
}

void cmm::actionResetWindows()
{
  QSettings settings("Carbon Design Systems","ModelStudio");
  const QString key = QString::number( (QT_VERSION >> 16) & 0xff )+ QLatin1String(".") + QString::number( (QT_VERSION >> 8) & 0xff ) + QLatin1String("/");
  QString ver = QString("%1/%2").arg(CARBON_RELEASE_ID).arg(gCarbonVersion());
  QString finalKey = QString("%1/%2").arg(key).arg(ver);
  QByteArray winGeometry = settings.value(finalKey + QLatin1String("defaultWindowGeometry")).toByteArray();
  QByteArray mainWinState = settings.value(finalKey + QLatin1String("defaultMainWindowState")).toByteArray();
  restoreGeometry(winGeometry);
  restoreState(mainWinState);
  InvisibleProject* ip = mTemplateProject->getInvisibleProject();
  if (ip)
    mDocManager->windowActivated(ip);
}

void cmm::on_actionFind_in_Files_triggered()
{
  if (mFindDialog == NULL)
    mFindDialog = new DlgFindInFiles();

  mFindDialog->setWindowFlags(mFindDialog->windowFlags() | Qt::WindowStaysOnTopHint);
  mFindDialog->show();
  mFindDialog->raise();
}


void cmm::on_actionPreferences_triggered()
{
  DlgPreferences dlg(this);
  dlg.exec();
}

void cmm::on_actionDriveMappings_triggered()
{
  DlgDriveMappings dlg(this);
  dlg.exec();
}

void cmm::createActions()
{
  exitAct = new QAction(tr("E&xit"), this);
  exitAct->setShortcut(tr("Ctrl+Q"));
  exitAct->setStatusTip(tr("Exit the application"));
  connect(exitAct, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));

  closeAct = new QAction(tr("Cl&ose"), this);
  closeAct->setShortcut(tr("Ctrl+F4"));
  closeAct->setStatusTip(tr("Close the active window"));
  connect(closeAct, SIGNAL(triggered()),
    ui.widgetWorkspace, SLOT(closeActiveWindow()));

  closeAllAct = new QAction(tr("Close &All"), this);
  closeAllAct->setStatusTip(tr("Close all the windows"));
  connect(closeAllAct, SIGNAL(triggered()),
    ui.widgetWorkspace, SLOT(closeAllWindows()));

  tileAct = new QAction(tr("&Tile"), this);
  tileAct->setStatusTip(tr("Tile the windows"));
  connect(tileAct, SIGNAL(triggered()), ui.widgetWorkspace, SLOT(tile()));

  cascadeAct = new QAction(tr("&Cascade"), this);
  cascadeAct->setStatusTip(tr("Cascade the windows"));
  connect(cascadeAct, SIGNAL(triggered()), ui.widgetWorkspace, SLOT(cascade()));

  windowResetAct = new QAction(tr("Re&set Windows"), this);
  windowResetAct->setStatusTip(tr("Reset the window to the original positions"));
  connect(windowResetAct, SIGNAL(triggered()),
    this, SLOT(actionResetWindows()));

  nextAct = new QAction(tr("Ne&xt"), this);
  nextAct->setStatusTip(tr("Move the focus to the next window"));
  connect(nextAct, SIGNAL(triggered()),
    ui.widgetWorkspace, SLOT(activateNextWindow()));
  //nextAct->setShortcut(tr("Ctrl+Tab"));

  previousAct = new QAction(tr("Pre&vious"), this);
  previousAct->setStatusTip(tr("Move the focus to the previous "
    "window"));
  previousAct->setShortcut(tr("Ctrl+Shift+Tab"));

  connect(previousAct, SIGNAL(triggered()),
    ui.widgetWorkspace, SLOT(activatePreviousWindow()));

  separatorAct = new QAction(this);
  separatorAct->setSeparator(true);

  docAct = new QAction(tr("&Help CMS User Manual..."), this);
  docAct->setShortcut(tr("F1"));
  docAct->setStatusTip(tr("Carbon ModelStudio User Manual"));
  connect(docAct, SIGNAL(triggered()), this, SLOT(showDocs()));

  docAct2 = new QAction(tr("&Carbon Documentation"), this);
  docAct2->setStatusTip(tr("Carbon ModelStudio Documentation"));
  connect(docAct2, SIGNAL(triggered()), this, SLOT(showDocs2()));

  aboutAct = new QAction(tr("&About"), this);
  aboutAct->setStatusTip(tr("Show the application's About box"));
  connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

  for (int i = 0; i < MaxRecentFiles; ++i) 
  {
    recentFileActs[i] = new QAction(this);
    recentFileActs[i]->setVisible(false);
    connect(recentFileActs[i], SIGNAL(triggered()), this, SLOT(openRecentFile()));
  }

  createKitAct = new QAction(tr("Create..."), this);
  createKitAct->setStatusTip(tr("Invoke the Model Kit Wizard"));
  connect(createKitAct, SIGNAL(triggered()), this, SLOT(showKitWizard()));

  docAct = new QAction(tr("&Help CMS User Manual..."), this);
  docAct->setShortcut(tr("F1"));
  docAct->setStatusTip(tr("Carbon ModelStudio User Manual"));
  connect(docAct, SIGNAL(triggered()), this, SLOT(showDocs()));

}

void cmm::dumpModelKit()
{
  ModelKit mkit;

  QString modelKitFile = argumentValue("-modelKit");
  QFileInfo fi(modelKitFile);
  if (fi.isRelative())
    modelKitFile = getWorkingDirectory() + "/" + modelKitFile;

  QString projectName = argumentValue("-projectName");
  QString projectDir = argumentValue("-projectDir");
  QString armRoot = argumentValue("-armRoot");

  createEmptyProject(projectDir, projectName);

  // step 1 (setup env)
  mkit.setupEnvironment(armRoot);
 // Read it back in and verify
  UtString uModelKitFileName; uModelKitFileName << modelKitFile;
  ZISTREAMZIP(arRead, uModelKitFileName.c_str());

  if (mkit.extractFile(arRead, MODELKIT_XML, MODELKIT_XML, 0))
  {
    mkit.deserialize(MODELKIT_XML); 
    KitVersion* v = mkit.getLatestVersion();
    mkit.extractManifest(arRead, projectDir, v->getManifest(), true);
  }
}

void cmm::showKitWizard()
{
  CarbonProject* proj = mContext->getCarbonProjectWidget()->project();
  if (proj)
  {
    ModelKitWizard* mkw = new ModelKitWizard(this);
    QString msg;
    if (mkw->canOpenWizard(msg))
    {
      mkw->exec();
      delete mkw;
    }
    else
      QMessageBox::critical(NULL, MODELSTUDIO_TITLE, msg);
  }
  else
    QMessageBox::critical(NULL, MODELSTUDIO_TITLE, "No project is currently opened");
}

void cmm::showDocs()
{
  UtString helpFile;
  // OSConstructFilePath(&helpFile, getenv("CARBON_HOME"), "help/cms.htm");
  OSConstructFilePath(&helpFile, getenv("CARBON_HOME"), "userdoc/pdf/Carbon_Model_Studio_Manual.pdf");

  QUrl url;
  url.setScheme("file");
  url.setPath(helpFile.c_str());

  if (!QDesktopServices::openUrl(url))
  {
    UtString msg;
    msg << "Unable to launch reader for help located at: " << helpFile.c_str();
    QMessageBox::warning(this, MODELSTUDIO_TITLE, msg.c_str());
  }
}

void cmm::showDocs2()
{
  UtString helpFile;
  OSConstructFilePath(&helpFile, getenv("CARBON_HOME"), "userdoc/index.html");
  //OSConstructFilePath(&helpFile, getenv("CARBON_HOME"), "userdoc/pdf/Carbon_Model_Studio_Manual.pdf");

  QUrl url;
  url.setScheme("file");
  url.setPath(helpFile.c_str());

  if (!QDesktopServices::openUrl(url))
  {
    UtString msg;
    msg << "Unable to launch reader for carbon documentation located at: " << helpFile.c_str();
    QMessageBox::warning(this, MODELSTUDIO_TITLE, msg.c_str());
  }
}

void cmm::createMenus()
{
  // Remove this menu pick for Unix
#if !pfWINDOWS
  QMenu* fileMenu = ui.menuFile;
  fileMenu->removeAction(ui.actionDriveMappings);
#endif

  QMenu* actionMenu = ui.menuOpenRecentProject;

  for (int i=0; i<MaxRecentFiles; i++)
    actionMenu->addAction(recentFileActs[i]);

  updateRecentFileActions();

  if (mContext->getCanCreateModelKits())
  {
    modelKitMenu = menuBar()->addMenu(tr("&Model Kits"));
    modelKitMenu->addAction(createKitAct);
  }

  windowMenu = menuBar()->addMenu(tr("&Window"));
  updateWindowMenu();
  connect(windowMenu, SIGNAL(aboutToShow()), this, SLOT(updateWindowMenu()));

  menuBar()->addSeparator();

  helpMenu = menuBar()->addMenu(tr("&Help"));
  helpMenu->addAction(docAct);
  helpMenu->addAction(docAct2);
  helpMenu->addAction(aboutAct);
  QMenu* viewMenu = ui.menu_View;
  // View Menu
  foreach (QAction* action, mDockActions)
    viewMenu->addAction(action);
}


void cmm::dockVisibilityChanged(bool)
{
  QDockWidget* dock = qobject_cast<QDockWidget*>(sender());
  if (dock)
  {
    QAction* action = mDockWidgetActionMap[dock];
    if (action)
      action->setChecked(dock->isVisible());
  }
}

void cmm::toggleDockWindow()
{
  QAction* action = qobject_cast<QAction*>(sender());

  QVariant qv = action->property("CarbonDockWidget");
  QDockWidget* dockWidget = (QDockWidget*)qv.value<void*>();
  
  bool hidflag = dockWidget->isHidden();
  bool visflag = dockWidget->isVisible();

  qDebug() << action->text() << " hidden: " << hidflag << " visflag: " << visflag;

  dockWidget->setVisible(!visflag);
}

void cmm::registerDockWidget(QDockWidget* widget, const char* name, const char* shortCut)
{
  QAction* action = new QAction(name, this);
  if (shortCut)
    action->setShortcut(QKeySequence(shortCut));

  action->setCheckable(true);
  action->setChecked(true);
  action->setProperty("CarbonDockWidget", qVariantFromValue((void*)widget));

  mDockWidgetActionMap[widget] = action;
  mDockActions.push_back(action);

  CQT_CONNECT(widget, visibilityChanged(bool), this, dockVisibilityChanged(bool));
  CQT_CONNECT(action, triggered(), this, toggleDockWindow());
}

void cmm::updateMenus()
{
  bool hasMdiChild = (activeMdiWindow() != 0);
  closeAct->setEnabled(hasMdiChild);
  closeAllAct->setEnabled(hasMdiChild);
  tileAct->setEnabled(hasMdiChild);
  cascadeAct->setEnabled(hasMdiChild);
  nextAct->setEnabled(hasMdiChild);
  previousAct->setEnabled(hasMdiChild);
  separatorAct->setVisible(hasMdiChild);

}
void cmm::readSettings()
{
  // squish tests require default settings to work
  if (!mSquishMode)
  {
    const QString key = QString::number( (QT_VERSION >> 16) & 0xff )+ QLatin1String(".") + QString::number( (QT_VERSION >> 8) & 0xff ) + QLatin1String("/");
    QSettings settings;
    QString ver = QString("%1/%2").arg(CARBON_RELEASE_ID).arg(gCarbonVersion());
    restoreState(settings.value(QString("State/%1/%2").arg(key).arg(ver)).toByteArray());
    restoreGeometry(settings.value(QString("Geometry/%1/%2").arg(key).arg(ver)).toByteArray());
  }
}

void cmm::writeSettings()
{
  if (!mSquishMode)
  {
    QSettings settings;
    const QString key = QString::number( (QT_VERSION >> 16) & 0xff )+ QLatin1String(".") + QString::number( (QT_VERSION >> 8) & 0xff ) + QLatin1String("/");
    QString ver = QString("%1/%2").arg(CARBON_RELEASE_ID).arg(gCarbonVersion());   
    settings.setValue(QString("State/%1/%2").arg(key).arg(ver), saveState());
    settings.setValue(QString("Geometry/%1/%2").arg(key).arg(ver), saveGeometry());
  }
}
void cmm::updateWindowMenu()
{
  windowMenu->clear();
  windowMenu->addAction(closeAct);
  windowMenu->addAction(closeAllAct);
  windowMenu->addSeparator();
  windowMenu->addAction(tileAct);
  windowMenu->addAction(cascadeAct);
  windowMenu->addSeparator();
  windowMenu->addAction(nextAct);
  windowMenu->addAction(previousAct);
  windowMenu->addAction(windowResetAct);
  windowMenu->addAction(separatorAct);

  QList<QWidget *> windows = ui.widgetWorkspace->windowList();
  separatorAct->setVisible(!windows.isEmpty());

  for (int i = 0; i < windows.size(); ++i) 
  {
    MDIWidget* child = dynamic_cast<MDIWidget*>(windows.at(i));

    if (child == NULL)
      continue;

    QString text;
    if (i < 9) {
      text = tr("&%1 %2").arg(i + 1)
        .arg(child->userFriendlyName());
    } else {
      text = tr("%1 %2").arg(i + 1)
        .arg(child->userFriendlyName());
    }

    QAction *action  = windowMenu->addAction(text);
    action->setCheckable(true);
    action ->setChecked(child == activeMdiWindow());
    connect(action, SIGNAL(triggered()), windowMapper, SLOT(map()));
    windowMapper->setMapping(action, child->getWidget());
  }

}

void cmm::abortApplication(const QString& msg, int exitCode)
{
  qDebug() << "ABORT:" << msg;
  shutdown(exitCode);
}

void cmm::about()
{
#if 0

  EmbeddedMono* mono = EmbeddedMono::Instance();
  MonoAssembly* assy = mono->LoadAssembly("TestKit.dll");
  MonoObject* obj = mono->ConstructObject(assy, "TestKit", "ModelKit");    
  void* mparams = NULL;
  
  MonoObject* exc = NULL;
  mono->InvokeMethod(assy, "TestKit", "ModelKit", "TestMethod", 0, &mparams, obj, &exc); 
  if (exc)
  {         
    MonoString* strEx = mono_object_to_string(exc, NULL);
    QString error = mono_string_to_utf8(strEx);
    mono->CMSConsolePrint(error);  
  }



  EmbeddedMono* mono = EmbeddedMono::Instance();
  MonoAssembly* assy = mono->LoadAssembly("CarbonCloudServices.dll");
  MonoObject* obj = mono->ConstructObject(assy, "CarbonCloudServices", "ServiceHelper");
         
  void* mparams = NULL;
  
  MonoObject* exc;
  MonoObject* mresult = mono->InvokeMethod(assy, "CarbonCloudServices", "ServiceHelper", "TestInterop", 0, &mparams, obj, &exc); 
  if (mresult)
  {
    MonoString* strReturn = (MonoString *)mresult;   
    const char* svalue = mono->ToString(strReturn);
    qDebug(svalue);
  }   
#endif

  UtString msg;
  msg << "<b>Carbon Model Studio</b> ";
  msg << "creates Carbon Models from RTL and facilitates the creation of components";
  msg << " including SoC Designer, CoWare and SystemC";
  msg << "<p>Version " CARBON_RELEASE_ID;
  msg << "<p>Build " << gCarbonVersion();
  msg << "<p>Log File: " << sLogFilePath;
  msg << "<br/><br/>PuTTY is copyright 1997-2007 Simon Tatham.<br/>";
  msg << "Portions copyright Robert de Bath, Joris van Rantwijk, Delian Delchev, Andreas Schultz, Jeroen Massar, Wez Furlong, Nicolas Barry, Justin Bradford, Ben Harris, Malcolm Smith, Ahmad Khalifa, Markus Kuhn, and CORE SDI S.A.<br/>";

  QMessageBox::about(this, tr("About Carbon Model Studio"),msg.c_str());
}


QTextEditor *cmm::createTextEditor()
{
  QTextEditor* te = (QTextEditor*)mTemplateTextEditor->createNewDocument(this);

  ui.widgetWorkspace->addWindow(te);
  te->getWidget()->showMaximized();

  return te;
}

MDIWidget *cmm::activeMdiWindow()
{
  MDIWidget* w = dynamic_cast<MDIWidget*>(ui.widgetWorkspace->activeWindow());
  return w;
}

QTextEditor *cmm::findTextEditor(const QString &fileName)
{
  QString canonicalFilePath;
  QFileInfo fi(fileName);
  if (fi.isRelative())
    canonicalFilePath = fi.canonicalFilePath();
  else
    canonicalFilePath = fi.absoluteFilePath();

  foreach (QWidget *window, ui.widgetWorkspace->windowList()) 
  {
    QTextEditor *textEditor = qobject_cast<QTextEditor*>(window);
    if (textEditor->currentFile() == canonicalFilePath)
      return textEditor;
  }
  return 0;
}

void ModelStudioAbort::printHandler(const char* abortMsg)
{
  qDebug() << "**** ABORT *****";
  qDebug() << abortMsg;

  if (!theApp->isBatchMode())
  {
    QString errMsg = QString("%1\nPlease send the log file to support (%2)")
      .arg(abortMsg)
      .arg(sLogFilePath);
    QMessageBox::critical(NULL, MODELSTUDIO_TITLE, errMsg);
  }
}

void ModelStudioAbort::abortHandler()
{
  qDebug() << "*** ABORT EXIT ***";
  ::abort();
}

// the following hack is used by eric to build modelmaker under windows without mkcds it is only defined in
// a private windows build, please don't remove it.
#ifdef WINDOWS_HACK

#include <util/DynBitVector.h>

void __cdecl DynBitVector::anytoany(unsigned int const *,unsigned int *,unsigned int,unsigned int,unsigned int)
{
}

#endif

