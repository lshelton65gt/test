//! -*-C++-*-
#ifndef __SCRCCFG__
#define __SCRCCFG__

//
// Wrapper around CarbonCfg and iodb
//
#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>
#include "cfg/CarbonCfg.h"
#include <QtScript/QScriptable>
#include <QtScript/QScriptClass>
#include <QtScript/QScriptString>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>

#include "Scripting.h"
 
class JavaScriptAPIs;


class ProtoCarbonCfgTemplate : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgTemplate");

  // Property: numVariables
  // The number of variables stored in this template object
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(quint32 numVariables READ getNumVariables)

public:
  ProtoCarbonCfgTemplate(QObject *parent = NULL) : QObject(parent) {}
  CarbonCfgTemplate* pObj() const { return qscriptvalue_cast<CarbonCfgTemplate*>(thisObject()); }

public slots:
  quint32 getNumVariables() { return pObj()->getNumVariables(); }

  QString getVariable(quint32 index)
  {
    return pObj()->getVariable(index);
  }

  QString getValue(const QString& variable)
  {
    UtString nv;
    nv << variable;
    return pObj()->getValue(nv.c_str());
  }
  QString getErrMsg() { return pObj()->getErrMsg(); }

  bool addVariable(const QString& variable, const QString& value)
  {
    UtString var, val;
    var << variable;
    val << value;
    return pObj()->addVariable(var.c_str(), val.c_str());
  }

  void clear() { pObj()->clear(); }

}; // class ProtoCarbonCfgTemplate : public QObject

class ProtoCarbonCfgRegisterLoc : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgRegisterLoc");

public:
  Q_PROPERTY(CarbonCfgRegisterLocConstant* castConstant READ castConstant)
  Q_PROPERTY(CarbonCfgRegisterLocRTL* castRTL READ castRTL)
  Q_PROPERTY(CarbonCfgRegisterLocReg* castReg READ castReg)
  Q_PROPERTY(CarbonCfgRegisterLocArray* castArray READ castArray)
  Q_PROPERTY(CarbonCfgRegisterLocUser* castUser READ castUser)
  Q_PROPERTY(CcfgEnum::CarbonCfgRegisterLocKind type READ getType)

  ProtoCarbonCfgRegisterLoc(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgRegisterLoc* pObj() const { 
    CarbonCfgRegisterLoc* me = qscriptvalue_cast<CarbonCfgRegisterLoc*>(thisObject());
    if (me)
      return me;

    QScriptValue sv = thisObject();
    
    if (sv.isVariant())
    {
      QString className = sv.toVariant().typeName();
      if ("CarbonCfgRegisterLocConstant*" == className) {
        qDebug() << "cast CarbonCfgRegisterLocConstant";
        return qscriptvalue_cast<CarbonCfgRegisterLocConstant*>(thisObject());
      }

      if ("CarbonCfgRegisterLocRTL*" == className) {
        qDebug() << "cast CarbonCfgRegisterLocRTL";
        return qscriptvalue_cast<CarbonCfgRegisterLocRTL*>(thisObject());
      }

      if ("CarbonCfgRegisterLocReg*" == className) {
        qDebug() << "cast CarbonCfgRegisterLocReg";
        return qscriptvalue_cast<CarbonCfgRegisterLocReg*>(thisObject());
      }
      
      if ("CarbonCfgRegisterLocArray*" == className) {
        qDebug() << "cast CarbonCfgRegisterLocArray";
        return qscriptvalue_cast<CarbonCfgRegisterLocArray*>(thisObject());
      }

      if ("CarbonCfgRegisterLocUser*" == className) {
        qDebug() << "cast CarbonCfgRegisterLocUser";
        return qscriptvalue_cast<CarbonCfgRegisterLocUser*>(thisObject());
      }
      QString err = QString("No Class Conversion handler for CarbonCfgRegisterLoc::%1").arg(className);
      UtString e; e << err;
      INFO_ASSERT(false, e.c_str());
      }

    return 0;
  }

public slots:
  CarbonCfgRegisterLocConstant* castConstant() const { return pObj()->castConstant(); }
  CarbonCfgRegisterLocRTL* castRTL() const { return pObj()->castRTL(); }
  CarbonCfgRegisterLocReg* castReg() const { return pObj()->castReg(); }
  CarbonCfgRegisterLocArray* castArray() const { return pObj()->castArray(); }
  CarbonCfgRegisterLocUser* castUser() const { return pObj()->castUser(); }
  CcfgEnum::CarbonCfgRegisterLocKind getType() const { return CcfgEnum::CarbonCfgRegisterLocKind(pObj()->getType()); }
};

class ProtoCarbonCfgRegisterLocRTL : public ProtoCarbonCfgRegisterLoc
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgRegisterLocRTL");

  Q_PROPERTY(bool hasPath READ hasPath)
  Q_PROPERTY(bool hasRange READ getHasRange)
  Q_PROPERTY(QString path READ getPath WRITE setPath)
  Q_PROPERTY(quint32 left READ getLeft WRITE setLeft)
  Q_PROPERTY(quint32 right READ getRight WRITE setRight)
  Q_PROPERTY(quint32 lsb READ getLSB WRITE setLSB)
  Q_PROPERTY(quint32 msb READ getMSB WRITE setMSB)
 
public:
  ProtoCarbonCfgRegisterLocRTL(QObject *parent = 0) : ProtoCarbonCfgRegisterLoc(parent) {}
  CarbonCfgRegisterLocRTL* pObj() const {

    CarbonCfgRegisterLocRTL* me = qscriptvalue_cast<CarbonCfgRegisterLocRTL*>(thisObject());
    if (me)
      return me;

    QScriptValue sv = thisObject();
    
    if (sv.isVariant())
    {
      QString className = sv.toVariant().typeName();
    
      if ("CarbonCfgRegisterLocReg*" == className)
        return qscriptvalue_cast<CarbonCfgRegisterLocReg*>(thisObject());

      if ("CarbonCfgRegisterLocArray*" == className)
        return qscriptvalue_cast<CarbonCfgRegisterLocArray*>(thisObject());

      QString err = QString("No Class Conversion handler for CarbonCfgRegisterLoc::%1").arg(className);
      UtString e; e << err;
      INFO_ASSERT(false, e.c_str());
    }
    
    return 0;
  }

public slots:
  bool getHasRange() const { return pObj()->getHasRange(); }
  bool hasPath() const { return pObj()->hasPath(); }
  QString getPath() const { return pObj()->getPath(); }
  void setPath(const QString& newVal)
  {
    UtString value;
    value << newVal;
    pObj()->putPath(value.c_str());
  }
  quint32 getLeft() const { return pObj()->getLeft(); }
  void setLeft(quint32 newVal) { pObj()->putLeft(newVal); }

  quint32 getRight() const { return pObj()->getRight(); }
  void setRight(quint32 newVal) { pObj()->putRight(newVal); }

  quint32 getLSB() const { return pObj()->getLSB(); }
  void setLSB(quint32 newVal) { pObj()->putLSB(newVal); }

  quint32 getMSB() const { return pObj()->getMSB(); }
  void setMSB(quint32 newVal) { pObj()->putMSB(newVal); }
};


class ProtoCarbonCfgRegisterLocReg : public ProtoCarbonCfgRegisterLocRTL
{
  Q_OBJECT
  
  Q_CLASSINFO("ClassName", "CarbonCfgRegisterLocReg");

public:
  ProtoCarbonCfgRegisterLocReg(QObject *parent = 0) : ProtoCarbonCfgRegisterLocRTL(parent) {}
  CarbonCfgRegisterLocReg* pObj() const {
    CarbonCfgRegisterLocReg* me = qscriptvalue_cast<CarbonCfgRegisterLocReg*>(thisObject());
   if (me == NULL)
      qDebug() << "*****ERROR: Invalid cast";

   return qscriptvalue_cast<CarbonCfgRegisterLocReg*>(thisObject());
  }
};

class ProtoCarbonCfgRegisterLocConstant : public ProtoCarbonCfgRegisterLoc
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgRegisterLocConstant");

  Q_PROPERTY(quint64 value READ getValue WRITE setValue)

public:
  ProtoCarbonCfgRegisterLocConstant(QObject *parent = 0) : ProtoCarbonCfgRegisterLoc(parent) {}
  CarbonCfgRegisterLocConstant* pObj() const {
    return qscriptvalue_cast<CarbonCfgRegisterLocConstant*>(thisObject());
  }

public slots:
  quint64 getValue() const { return pObj()->getValue(); }
  void setValue(quint64 newVal)
  {
    pObj()->putValue(newVal);
  }
};


class ProtoCarbonCfgRegisterLocUser : public ProtoCarbonCfgRegisterLoc
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgRegisterLocUser");

public:
  ProtoCarbonCfgRegisterLocUser(QObject *parent = 0) : ProtoCarbonCfgRegisterLoc(parent) {}
  CarbonCfgRegisterLocUser* pObj() const { return qscriptvalue_cast<CarbonCfgRegisterLocUser*>(thisObject()); }

public slots:
};

class ProtoCarbonCfgRegisterLocArray : public ProtoCarbonCfgRegisterLocRTL
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgRegisterLocArray");

  Q_PROPERTY(quint32 index READ getIndex WRITE setIndex)

public:
  ProtoCarbonCfgRegisterLocArray(QObject *parent = 0) : ProtoCarbonCfgRegisterLocRTL(parent) {}
  CarbonCfgRegisterLocArray* pObj() const { return qscriptvalue_cast<CarbonCfgRegisterLocArray*>(thisObject()); }

public slots:
  quint32 getIndex() const { return pObj()->getIndex(); }
  void setIndex(quint32 newVal) { pObj()->putIndex(newVal); }
};

class ProtoCarbonCfgGroup : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgGroup");

  Q_PROPERTY(QString name READ getName)

public:
  ProtoCarbonCfgGroup(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgGroup* pObj() const { return qscriptvalue_cast<CarbonCfgGroup*>(thisObject()); }

public slots:
  QString getName() const { return pObj()->getName(); }
};


class ProtoCarbonCfgRegisterField : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgRegisterField");

  Q_PROPERTY(QString name READ getName WRITE setName)
  Q_PROPERTY(quint32 width READ getWidth)
  Q_PROPERTY(quint32 high READ getHigh WRITE setHigh)
  Q_PROPERTY(quint32 low READ getLow WRITE setLow)
  Q_PROPERTY(CcfgEnum::CarbonCfgRegAccessType access READ getAccess WRITE setAccess)
  Q_PROPERTY(CarbonCfgRegisterLoc* location READ getLoc)
  Q_PROPERTY(CarbonCfgRegisterLocReg* regLoc READ getRegLoc)
  Q_PROPERTY(CarbonCfgRegisterLocConstant* constantLoc READ getConstantLoc)
  Q_PROPERTY(CarbonCfgRegisterLocArray* arrayLoc READ getArrayLoc)
  Q_PROPERTY(CarbonCfgRegisterLocUser* userLoc READ getUserLoc)

public:
  ProtoCarbonCfgRegisterField(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgRegisterField* pObj() const { return qscriptvalue_cast<CarbonCfgRegisterField*>(thisObject()); }
 
  static CcfgEnum::CarbonCfgRegAccessType toAccessType(const QString& typeName)
  {
    if (typeName == "RW")
      return CcfgEnum::RW;
    else if (typeName == "RO")
      return CcfgEnum::RO;
    else if (typeName == "WO")
      return CcfgEnum::WO;
    return CcfgEnum::RW;
  }

public slots:
  quint32 getWidth() const { return pObj()->getWidth(); }
 
  quint32 getHigh() const { return pObj()->getHigh(); }
  void setHigh(quint32 nv) { pObj()->putHigh(nv); }

  quint32 getLow() const { return pObj()->getLow(); }
  void setLow(quint32 nv) { pObj()->putLow(nv); }

  QString getName() const { return pObj()->getName(); }
  void setName(const QString& newVal)
  {
    UtString value;
    value << newVal;
    pObj()->putName(value.c_str());
  }
  CarbonCfgRegisterLoc* getLoc() const { return pObj()->getLoc(); }

  void setLoc(QVariant loc)
  {
    CarbonCfgRegisterLocReg* lr = qvariant_cast<CarbonCfgRegisterLocReg*>(loc);
    CarbonCfgRegisterLocConstant* lc = qvariant_cast<CarbonCfgRegisterLocConstant*>(loc);
    CarbonCfgRegisterLocArray* la = qvariant_cast<CarbonCfgRegisterLocArray*>(loc);
    CarbonCfgRegisterLocUser* lu = qvariant_cast<CarbonCfgRegisterLocUser*>(loc);

    if (lr)
      setLocReg(lr);
    else if (lc)
      setLocConstant(lc);
    else if (la)
      setLocArray(la);
    else if (lu)
      setLocUser(lu);
    else
    {
      qDebug() << "ERROR: Unable to cast loc: " << loc << "to CarbonCfgRegisterLocReg, Constant, Array or User";
      INFO_ASSERT(false, "ERROR: Unable to cast loc");
    }
  }

  void setLocReg(CarbonCfgRegisterLocReg* newVal) { pObj()->putLoc(newVal); }
  void setLocConstant(CarbonCfgRegisterLocConstant* newVal) { pObj()->putLoc(newVal); }
  void setLocArray(CarbonCfgRegisterLocArray* newVal) { pObj()->putLoc(newVal); }
  void setLocUser(CarbonCfgRegisterLocUser* newVal) { pObj()->putLoc(newVal); }
  
  CarbonCfgRegisterLocReg* getRegLoc() { return pObj()->getLoc()->castReg(); }
  CarbonCfgRegisterLocConstant* getConstantLoc() { return pObj()->getLoc()->castConstant(); }
  CarbonCfgRegisterLocArray* getArrayLoc() { return pObj()->getLoc()->castArray(); }
  CarbonCfgRegisterLocUser* getUserLoc() { return pObj()->getLoc()->castUser(); }
  
  CcfgEnum::CarbonCfgRegAccessType getAccess() const
  {
    CarbonCfgRegAccessTypeIO accType = pObj()->getAccess();
    return static_cast<CcfgEnum::CarbonCfgRegAccessType>((int)accType);
  }
  void setAccess(CcfgEnum::CarbonCfgRegAccessType newVal) 
  { 
    pObj()->putAccess(static_cast<CarbonCfgRegAccessType>(newVal));
  }
};

class ProtoCarbonCfgCustomCode : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgCustomCode");

  Q_PROPERTY(CcfgEnum::CarbonCfgCustomCodePosition position READ getPosition WRITE setPosition);
  Q_PROPERTY(QString code READ getCode WRITE setCode);

public:
  ProtoCarbonCfgCustomCode(QObject* parent = 0) : QObject(parent) {}

  // We need a way to cast to the base class CarbonCfgCustomCode from
  // an object that has been registered using the derived Proto
  // class. To do this we have to try to cast to each possible derived
  // class. There may be a better way to do this, but for now this is
  // it. So if you add a new custom code derived class, add the cast
  // function in the if/then/else
  CarbonCfgCustomCode* pObj() const
  {
    CarbonCfgCustomCode* p;
    p = qscriptvalue_cast<CarbonCfgCompCustomCode*>(thisObject());
    if (p == NULL) {
      p = qscriptvalue_cast<CarbonCfgRegCustomCode*>(thisObject());
    }
    if (p == NULL) {
      p = qscriptvalue_cast<CarbonCfgMemoryBlockCustomCode*>(thisObject());
    }
    if (p == NULL) {
      p = qscriptvalue_cast<CarbonCfgMemoryCustomCode*>(thisObject());
    }
    if (p == NULL) {
      p = qscriptvalue_cast<CarbonCfgCadiCustomCode*>(thisObject());
    }
    INFO_ASSERT(p, "Missing object cast function for a derivation of CarbonCfgCustomCode");
    return p;
  }

public slots:
  CcfgEnum::CarbonCfgCustomCodePosition getPosition() const
  {
    return CcfgEnum::CarbonCfgCustomCodePosition(pObj()->getPosition());
  }
  void setPosition(CcfgEnum::CarbonCfgCustomCodePosition position)
  {
    pObj()->putPosition(CarbonCfgCustomCodePosition(position));
  }
  QString getCode() const { return pObj()->getCode(); }
  void setCode(const QString& code)
  {
    UtString newCode;
    newCode << code;
    pObj()->putCode(newCode.c_str());
  }
}; // class ProtoCarbonCfgCustomCode : public QObject, public QScriptable

class ProtoCarbonCfgCompCustomCode : public ProtoCarbonCfgCustomCode
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgCompCustomCode");

  Q_PROPERTY(CcfgEnum::CarbonCfgCompCustomCodeSection section READ getSection WRITE setSection);

public:
  ProtoCarbonCfgCompCustomCode(QObject* parent = 0) : ProtoCarbonCfgCustomCode(parent) {}
  CarbonCfgCompCustomCode* pObj() const { return qscriptvalue_cast<CarbonCfgCompCustomCode*>(thisObject()); }

public slots:
  CcfgEnum::CarbonCfgCompCustomCodeSection getSection() const
  {
    return CcfgEnum::CarbonCfgCompCustomCodeSection(pObj()->getSection());
  }
  void setSection(CcfgEnum::CarbonCfgCompCustomCodeSection section)
  {
    pObj()->putSection(CarbonCfgCompCustomCodeSection(section));
  }
}; // class ProtoCarbonCfgCompCustomCode : public ProtoCarbonCfgCustomCode

class ProtoCarbonCfgCadiCustomCode : public ProtoCarbonCfgCustomCode
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgCadiCustomCode");

  Q_PROPERTY(CcfgEnum::CarbonCfgCadiCustomCodeSection section READ getSection WRITE setSection);

public:
  ProtoCarbonCfgCadiCustomCode(QObject* parent = 0) : ProtoCarbonCfgCustomCode(parent) {}
  CarbonCfgCadiCustomCode* pObj() const { return qscriptvalue_cast<CarbonCfgCadiCustomCode*>(thisObject()); }

public slots:
  CcfgEnum::CarbonCfgCadiCustomCodeSection getSection() const
  {
    return CcfgEnum::CarbonCfgCadiCustomCodeSection(pObj()->getSection());
  }
  void setSection(CcfgEnum::CarbonCfgCadiCustomCodeSection section)
  {
    pObj()->putSection(CarbonCfgCadiCustomCodeSection(section));
  }
}; // class ProtoCarbonCfgCadiCustomCode : public ProtoCarbonCfgCustomCode

class ProtoCarbonCfgRegCustomCode : public ProtoCarbonCfgCustomCode
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgRegCustomCode");

  Q_PROPERTY(CcfgEnum::CarbonCfgRegCustomCodeSection section READ getSection WRITE setSection);

public:
  ProtoCarbonCfgRegCustomCode(QObject* parent = 0) : ProtoCarbonCfgCustomCode(parent) {}
  CarbonCfgRegCustomCode* pObj() const { return qscriptvalue_cast<CarbonCfgRegCustomCode*>(thisObject()); }

public slots:
  CcfgEnum::CarbonCfgRegCustomCodeSection getSection() const
  {
    return CcfgEnum::CarbonCfgRegCustomCodeSection(pObj()->getSection());
  }
  void setSection(CcfgEnum::CarbonCfgRegCustomCodeSection section)
  {
    pObj()->putSection(CarbonCfgRegCustomCodeSection(section));
  }
}; // class ProtoCarbonCfgRegCustomCode : public ProtoCarbonCfgCustomCode

class ProtoCarbonCfgMemoryBlockCustomCode : public ProtoCarbonCfgCustomCode
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgMemoryBlockCustomCode");

  Q_PROPERTY(CcfgEnum::CarbonCfgMemoryBlockCustomCodeSection section READ getSection WRITE setSection);

public:
  ProtoCarbonCfgMemoryBlockCustomCode(QObject* parent = 0) : ProtoCarbonCfgCustomCode(parent) {}
  CarbonCfgMemoryBlockCustomCode* pObj() const { return qscriptvalue_cast<CarbonCfgMemoryBlockCustomCode*>(thisObject()); }

public slots:
  CcfgEnum::CarbonCfgMemoryBlockCustomCodeSection getSection() const
  {
    return CcfgEnum::CarbonCfgMemoryBlockCustomCodeSection(pObj()->getSection());
  }
  void setSection(CcfgEnum::CarbonCfgMemoryBlockCustomCodeSection section)
  {
    pObj()->putSection(CarbonCfgMemoryBlockCustomCodeSection(section));
  }
}; // class ProtoCarbonCfgMemoryBlockCustomCode : public ProtoCarbonCfgCustomCode

class ProtoCarbonCfgMemoryCustomCode : public ProtoCarbonCfgCustomCode
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgMemoryCustomCode");

  Q_PROPERTY(CcfgEnum::CarbonCfgMemoryCustomCodeSection section READ getSection WRITE setSection);

public:
  ProtoCarbonCfgMemoryCustomCode(QObject* parent = 0) : ProtoCarbonCfgCustomCode(parent) {}
  CarbonCfgMemoryCustomCode* pObj() const { return qscriptvalue_cast<CarbonCfgMemoryCustomCode*>(thisObject()); }

public slots:
  CcfgEnum::CarbonCfgMemoryCustomCodeSection getSection() const
  {
    return CcfgEnum::CarbonCfgMemoryCustomCodeSection(pObj()->getSection());
  }
  void setSection(CcfgEnum::CarbonCfgMemoryCustomCodeSection section)
  {
    pObj()->putSection(CarbonCfgMemoryCustomCodeSection(section));
  }
}; // class ProtoCarbonCfgMemoryCustomCode : public ProtoCarbonCfgCustomCode

class CarbonCfgMemoryLocRTL;
class CarbonCfgMemoryLocPort;
class CarbonCfgMemoryLocUser;

class ProtoCarbonCfgMemoryLoc : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgMemoryLoc");

  Q_PROPERTY(CcfgEnum::CarbonCfgMemLocType type READ getType);
  Q_PROPERTY(quint64 displayStartWordOffset READ getDisplayStartWordOffset WRITE setDisplayStartWordOffset);
  Q_PROPERTY(quint64 displayEndWordOffset READ getDisplayEndWordOffset WRITE setDisplayEndWordOffset);
  Q_PROPERTY(quint32 displayMSB READ getDisplayMsb WRITE setDisplayMsb);
  Q_PROPERTY(quint32 displayLSB READ getDisplayLsb WRITE setDisplayLsb);
  Q_PROPERTY(CarbonCfgMemoryLocRTL* castRTL READ castRTL);
  Q_PROPERTY(CarbonCfgMemoryLocPort* castPort READ castPort);
  Q_PROPERTY(CarbonCfgMemoryLocUser* castUser READ castUser);

public:
  ProtoCarbonCfgMemoryLoc(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgMemoryLoc* pObj() const {
    
    CarbonCfgMemoryLoc* me = qscriptvalue_cast<CarbonCfgMemoryLoc*>(thisObject());
    if (me)
      return me;

    QScriptValue sv = thisObject();
    
    if (sv.isVariant())
    {
      QString className = sv.toVariant().typeName();
      if ("CarbonCfgMemoryLocPort*" == className)
        return qscriptvalue_cast<CarbonCfgMemoryLocPort*>(thisObject());
      
      if ("CarbonCfgMemoryLocRTL*" == className)
        return qscriptvalue_cast<CarbonCfgMemoryLocRTL*>(thisObject());
   
      QString err = QString("No Class Conversion handler for CarbonCfgMemoryLoc::%1").arg(className);
      UtString e; e << err;
      INFO_ASSERT(false, e.c_str());
    }

    return 0;
  }

public slots:
  CcfgEnum::CarbonCfgMemLocType getType() const { return CcfgEnum::CarbonCfgMemLocType(pObj()->getType()); }

  quint64 getDisplayStartWordOffset() const { return pObj()->getDisplayStartWordOffset(); }
  void setDisplayStartWordOffset(quint64 newVal) { pObj()->putDisplayStartWordOffset(newVal); }
 
  quint64 getDisplayEndWordOffset() const { return pObj()->getDisplayEndWordOffset(); }
  void setDisplayEndWordOffset(quint64 newVal) { pObj()->putDisplayEndWordOffset(newVal); }

  quint32 getDisplayMsb() const { return pObj()->getDisplayMsb(); }
  void setDisplayMsb(quint32 newVal) { pObj()->putDisplayMsb(newVal); }

  quint32 getDisplayLsb() const { return pObj()->getDisplayLsb(); }
  void setDisplayLsb(quint32 newVal) { pObj()->putDisplayLsb(newVal); }

  CarbonCfgMemoryLocRTL* castRTL() const { return pObj()->castRTL(); }
  CarbonCfgMemoryLocPort* castPort() const { return pObj()->castPort(); }
  CarbonCfgMemoryLocUser* castUser() const { return pObj()->castUser(); }
};


class ProtoCarbonCfgMemoryLocRTL : public ProtoCarbonCfgMemoryLoc
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgMemoryLocRTL");

  Q_PROPERTY(QString path READ getPath WRITE setPath);
  Q_PROPERTY(quint64 startWordOffset READ getStartWordOffset WRITE setStartWordOffset);
  Q_PROPERTY(quint64 endWordOffset READ getEndWordOffset WRITE setEndWordOffset);
  Q_PROPERTY(quint32 MSB READ getMsb WRITE setMsb);
  Q_PROPERTY(quint32 LSB READ getLsb WRITE setLsb);
  Q_PROPERTY(quint32 bitWidth READ getBitWidth);
  Q_PROPERTY(quint64 displayStartWordOffset READ getDisplayStartWordOffset WRITE setDisplayStartWordOffset);
  Q_PROPERTY(quint64 displayEndWordOffset READ getDisplayEndWordOffset WRITE setDisplayEndWordOffset);
  Q_PROPERTY(quint32 displayMSB READ getDisplayMsb WRITE setDisplayMsb);
  Q_PROPERTY(quint32 displayLSB READ getDisplayLsb WRITE setDisplayLsb);


public:
  ProtoCarbonCfgMemoryLocRTL(QObject *parent = 0) : ProtoCarbonCfgMemoryLoc(parent) {}
  CarbonCfgMemoryLocRTL* pObj() const {
    return qscriptvalue_cast<CarbonCfgMemoryLocRTL*>(thisObject());
  }

public slots:
  quint64 getDisplayStartWordOffset() const { return pObj()->getDisplayStartWordOffset(); }
  void setDisplayStartWordOffset(quint64 newVal) { pObj()->putDisplayStartWordOffset(newVal); }
 
  quint64 getDisplayEndWordOffset() const { return pObj()->getDisplayEndWordOffset(); }
  void setDisplayEndWordOffset(quint64 newVal) { pObj()->putDisplayEndWordOffset(newVal); }

  quint32 getDisplayMsb() const { return pObj()->getDisplayMsb(); }
  void setDisplayMsb(quint32 newVal) { pObj()->putDisplayMsb(newVal); }

  quint32 getDisplayLsb() const { return pObj()->getDisplayLsb(); }
  void setDisplayLsb(quint32 newVal) { pObj()->putDisplayLsb(newVal); }

  QString getPath() const {  return pObj()->getPath(); }
  void setPath(const QString& newVal) { UtString nv; nv << newVal; pObj()->putPath(nv.c_str()); }

  quint64 getStartWordOffset() const { return pObj()->getRTLStartWordOffset(); }
  void setStartWordOffset(quint64 newVal) { pObj()->putRTLStartWordOffset(newVal); }
 
  quint64 getEndWordOffset() const { return pObj()->getRTLEndWordOffset(); }
  void setEndWordOffset(quint64 newVal) { pObj()->putRTLEndWordOffset(newVal); }

  quint32 getMsb() const { return pObj()->getRTLMsb(); }
  void setMsb(quint32 newVal) { pObj()->putRTLMsb(newVal); }

  quint32 getLsb() const { return pObj()->getRTLLsb(); }
  void setLsb(quint32 newVal) { pObj()->putRTLLsb(newVal); }

  quint32 getBitWidth() const { return pObj()->getBitWidth(); }
};

class ProtoCarbonCfgMemoryLocPort : public ProtoCarbonCfgMemoryLoc
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgMemoryLocPort");

  Q_PROPERTY(QString name READ getPortName WRITE setPortName);
  Q_PROPERTY(bool fixedAddress READ getFixedAddress WRITE setFixedAddress);
  Q_PROPERTY(quint64 displayStartWordOffset READ getDisplayStartWordOffset WRITE setDisplayStartWordOffset);
  Q_PROPERTY(quint64 displayEndWordOffset READ getDisplayEndWordOffset WRITE setDisplayEndWordOffset);
  Q_PROPERTY(quint32 displayMSB READ getDisplayMsb WRITE setDisplayMsb);
  Q_PROPERTY(quint32 displayLSB READ getDisplayLsb WRITE setDisplayLsb);


public:
  ProtoCarbonCfgMemoryLocPort(QObject *parent = 0) : ProtoCarbonCfgMemoryLoc(parent) {}
  CarbonCfgMemoryLocPort* pObj() const { return qscriptvalue_cast<CarbonCfgMemoryLocPort*>(thisObject()); }

public slots:
  QString getPortName() const { return pObj()->getPortName(); }
  void setPortName(const QString& newVal) { UtString nv; nv << newVal; pObj()->putPortName(nv.c_str()); }

  bool getFixedAddress() const { return pObj()->hasFixedAddress(); }
  void setFixedAddress(quint32 newVal) { pObj()->putFixedAddress(newVal); }

  quint64 getDisplayStartWordOffset() const { return pObj()->getDisplayStartWordOffset(); }
  void setDisplayStartWordOffset(quint64 newVal) { pObj()->putDisplayStartWordOffset(newVal); }
 
  quint64 getDisplayEndWordOffset() const { return pObj()->getDisplayEndWordOffset(); }
  void setDisplayEndWordOffset(quint64 newVal) { pObj()->putDisplayEndWordOffset(newVal); }

  quint32 getDisplayMsb() const { return pObj()->getDisplayMsb(); }
  void setDisplayMsb(quint32 newVal) { pObj()->putDisplayMsb(newVal); }

  quint32 getDisplayLsb() const { return pObj()->getDisplayLsb(); }
  void setDisplayLsb(quint32 newVal) { pObj()->putDisplayLsb(newVal); }
};

class ProtoCarbonCfgMemoryLocUser : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgMemoryLocUser");

  Q_PROPERTY(QString name READ getName WRITE setName);

public:
  ProtoCarbonCfgMemoryLocUser(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgMemoryLocUser* pObj() const { return qscriptvalue_cast<CarbonCfgMemoryLocUser*>(thisObject()); }

public slots:
  QString getName() const { return pObj()->getName(); }
  void setName(const QString& _name)
  {
    UtString name;
    name << _name;
    pObj()->putName(name.c_str());
  }


};

class ProtoCarbonCfgMemoryBlock : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgMemoryBlock");
  
  Q_PROPERTY(QString name READ getName WRITE setName);
  Q_PROPERTY(quint64 base READ getBase WRITE setBase);
  Q_PROPERTY(quint64 size READ getSize WRITE setSize);
  Q_PROPERTY(quint32 width READ getWidth);
  Q_PROPERTY(quint32 numLocs READ numLocs);
  Q_PROPERTY(quint32 numCustomCodes READ numCustomCodes)

public:
  ProtoCarbonCfgMemoryBlock(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgMemoryBlock* pObj() const { return qscriptvalue_cast<CarbonCfgMemoryBlock*>(thisObject()); }

public slots:
  QString getName() const { return pObj()->getName(); }
  void setName(const QString& _name)
  {
    UtString name;
    name << _name;
    pObj()->putName(name.c_str());
  }
 
  quint64 getBase() const { return pObj()->getBase(); }
  void setBase(quint64 newVal) { pObj()->putBase(newVal); }

  quint64 getSize() const { return pObj()->getSize(); }
  void setSize(quint64 newVal) { pObj()->putSize(newVal); }

  quint32 getWidth() const { return pObj()->getWidth(); }

  CarbonCfgMemoryLocRTL* addLocRTL(const QString path = QString(), quint32 width = 0)
  {
    UtString p;
    p << path;
    return pObj()->addLocRTL(p.c_str(), width);
  }
  CarbonCfgMemoryLocPort* addLocPort(const QString& port_name)
  {
    UtString pname;
    pname << port_name;
    return pObj()->addLocPort(pname.c_str());
  }
  CarbonCfgMemoryLocUser* addLocUser(const QString& _name)
  {
    UtString name;
    name << _name;
    return pObj()->addLocUser(name.c_str());
  }
  
  void removeLoc(QVariant loc)
  {
    CarbonCfgMemoryLoc* ml = qvariant_cast<CarbonCfgMemoryLoc*>(loc);
    if (ml)
      removeLocMemoryLoc(ml);

    CarbonCfgMemoryLocPort* mlp = qvariant_cast<CarbonCfgMemoryLocPort*>(loc);
    if (mlp)
      removeLocPort(mlp);

    CarbonCfgMemoryLocRTL* mlr = qvariant_cast<CarbonCfgMemoryLocRTL*>(loc);
    if (mlr)
      removeLocRTL(mlr);

    CarbonCfgMemoryLocUser* mlu = qvariant_cast<CarbonCfgMemoryLocUser*>(loc);
    if (mlu)
      removeLocUser(mlu);
  }


  void removeLocMemoryLoc(CarbonCfgMemoryLoc* loc) { pObj()->removeLoc(loc); }
  void removeLocPort(CarbonCfgMemoryLocPort* loc) { pObj()->removeLoc(loc); }
  void removeLocRTL(CarbonCfgMemoryLocRTL* loc) { pObj()->removeLoc(loc); }
  void removeLocUser(CarbonCfgMemoryLocUser* loc) { pObj()->removeLoc(loc); }
 
  quint32 numLocs() const { return pObj()->numLocs(); }
  
  CarbonCfgMemoryLoc* getLoc(quint32 index) { return pObj()->getLoc(index); }

  quint32 numCustomCodes() const { return pObj()->numCustomCodes(); }
  CarbonCfgMemoryBlockCustomCode* addCustomCode()
  {
    return static_cast<CarbonCfgMemoryBlockCustomCode*>(pObj()->addCustomCode());
  }
  CarbonCfgMemoryBlockCustomCode* getCustomCode(quint32 i) const
  {
    return static_cast<CarbonCfgMemoryBlockCustomCode*>(pObj()->getCustomCode(i));
  }
  void removeCustomCode(CarbonCfgMemoryBlockCustomCode* customCode)
  {
    pObj()->removeCustomCode(customCode);
  }

};


class ProtoCarbonCfgProcInfo : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgProcInfo");
  Q_PROPERTY(bool processor READ getIsProcessor WRITE setIsProcessor);
  Q_PROPERTY(QString debuggerName READ getDebuggerName WRITE setDebuggerName);
  Q_PROPERTY(quint32 pipeStages READ getPipeStages WRITE setPipeStages);
  Q_PROPERTY(quint32 hwThreads READ getHwThreads WRITE setHwThreads);
  Q_PROPERTY(quint32 numProcessorOptions READ getNumProcessorOptions);
  Q_PROPERTY(QString pcRegGroupName READ getPCRegGroupName);
  Q_PROPERTY(QString pcRegName READ getPCRegName);
  Q_PROPERTY(QString extendedFeaturesRegGroupName READ getExtendedFeaturesRegGroupName);
  Q_PROPERTY(QString extendedFeaturesRegName READ getExtendedFeaturesRegName);
  Q_PROPERTY(QString targetName READ getTargetName WRITE setTargetName);
  Q_PROPERTY(bool debuggablePoint READ getDebuggablePoint WRITE setDebuggablePoint);

public:
  ProtoCarbonCfgProcInfo(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgProcInfo* pObj() const { return qscriptvalue_cast<CarbonCfgProcInfo*>(thisObject()); }

public slots:
  bool getIsProcessor() const { return pObj()->isProcessor(); }
  void setIsProcessor(bool newVal) { pObj()->putIsProcessor(newVal); }

  QString getDebuggerName() const { return pObj()->getDebuggerName(); }
  void setDebuggerName(const QString& newVal) { UtString nv; nv << newVal; pObj()->putDebuggerName(nv.c_str()); }

  quint32 getPipeStages() const { return pObj()->getPipeStages(); }
  void setPipeStages(quint32 newVal) { pObj()->putPipeStages(newVal); }

  quint32 getHwThreads() const { return pObj()->getHwThreads(); }
  void setHwThreads(quint32 newVal) { pObj()->putHwThreads(newVal); }

  quint32 getNumProcessorOptions(void) const
  {
    return pObj()->getNumProcessorOptions();
  }

  QString getProcessorOption(quint32 i) const
  {
    return pObj()->getProcessorOption(i);
  }

  void addProcessorOption(QString newOption)
  {
    UtString option;
    option << newOption;
    pObj()->addProcessorOption(option.c_str());
  }

  QString getPCRegGroupName(void) const
  {
    return pObj()->getPCRegGroupName();
  }

  QString getPCRegName(void) const
  {
    return pObj()->getPCRegName();
  }

  QString getExtendedFeaturesRegGroupName(void) const
  {
    return pObj()->getExtendedFeaturesRegGroupName();
  }

  QString getExtendedFeaturesRegName(void) const
  {
    return pObj()->getExtendedFeaturesRegName();
  }

  QString getTargetName(void) const
  {
    return pObj()->getTargetName();
  }

  void setPCRegName(QString newGroupName, QString newRegName)
  {
    UtString groupName, regName;
    groupName << newGroupName;
    regName << newRegName;
    pObj()->putPCRegName(groupName.c_str(), regName.c_str());
  }

  void setExtendedFeaturesRegName(QString newGroupName, QString newRegName)
  {
    UtString groupName, regName;
    groupName << newGroupName;
    regName << newRegName;
    pObj()->putExtendedFeaturesRegName(groupName.c_str(), regName.c_str());
  }

  void setTargetName(QString newTargetName)
  {
    UtString targetName;
    targetName << newTargetName;
    pObj()->putTargetName(targetName.c_str());
  }

  bool getDebuggablePoint() const { return pObj()->getDebuggablePoint(); }
  void setDebuggablePoint(bool newVal) { pObj()->putDebuggablePoint(newVal); }
};

class ProtoCarbonCfgELFLoader : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgELFLoader");

  Q_PROPERTY(quint32 numSections READ numSections);

public:
  ProtoCarbonCfgELFLoader(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgELFLoader* pObj() const { return qscriptvalue_cast<CarbonCfgELFLoader*>(thisObject()); }

public slots:
  void addSection(const QString& name, quint32 space, const QString& access)
  {
    UtString uName;
    uName << name;
    UtString uAccess;
    uAccess << access;
    pObj()->addSection(uName.c_str(), space, uAccess.c_str());
  }
  quint32 numSections() const { return pObj()->numSections(); }
  QString getSectionName(quint32 index) { return pObj()->getSectionName(index); }
  quint32 getSectionSpace(quint32 index) { return pObj()->getSectionSpace(index); }
  QString getSectionAccess(quint32 index) { return pObj()->getSectionAccess(index); }
  void removeSections(void) { pObj()->removeSections(); }

};

class ProtoCarbonCfgMemory : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgMemory");

  Q_PROPERTY(quint32 numMemoryBlocks READ numMemoryBlocks);
  Q_PROPERTY(QString name READ getName);
  Q_PROPERTY(QString initFile READ getInitFile WRITE setInitFile);
  Q_PROPERTY(QString comment READ getComment WRITE setComment);
  Q_PROPERTY(quint32 width READ getWidth);
  Q_PROPERTY(CcfgEnum::CarbonCfgReadmemType readmemType READ getReadmemType WRITE setReadmemType);
  Q_PROPERTY(CcfgEnum::CarbonCfgMemInitType initType READ getInitType WRITE setInitType);
  // ** Deprecated ** 
  Q_PROPERTY(QString progPreloadEslPort READ getSystemAddressESLPortName WRITE setProgPreloadEslPort);
  // Replaces progPreloadEslPort
  Q_PROPERTY(QString systemAddressESLPortName READ getSystemAddressESLPortName WRITE setProgPreloadEslPort);
  Q_PROPERTY(bool displayAtZero READ getDisplayAtZero WRITE setDisplayAtZero);
  Q_PROPERTY(quint32 MAU READ getMAU WRITE setMAU);
  Q_PROPERTY(bool bigEndian READ getBigEndian WRITE setBigEndian);
  Q_PROPERTY(bool programMemory READ getProgramMemory WRITE setProgramMemory);
  Q_PROPERTY(quint32 numCustomCodes READ numCustomCodes)
  Q_PROPERTY(CcfgFlags::CarbonCfgMemoryEditFlags EditFlags READ GetEditFlags WRITE SetEditFlags);

public:
  ProtoCarbonCfgMemory(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgMemory* pObj() const { return qscriptvalue_cast<CarbonCfgMemory*>(thisObject()); }

public slots:
  CcfgFlags::CarbonCfgMemoryEditFlags GetEditFlags() const { return pObj()->GetEditFlags(); }
  void SetEditFlags(CcfgFlags::CarbonCfgMemoryEditFlags newValue) { pObj()->SetEditFlags(newValue); }

  quint32 numMemoryBlocks() const { return pObj()->numMemoryBlocks(); }

  QString getName() const { return pObj()->getName(); }
  
  QString getInitFile() const { return pObj()->getInitFile(); }
  void setInitFile(const QString& newVal) { UtString nv; nv << newVal; pObj()->putInitFile(nv.c_str()); }

  QString getComment() const { return pObj()->getComment(); }
  void setComment(const QString& newVal) { UtString nv; nv << newVal; pObj()->putComment(nv.c_str()); }

  quint32 getWidth() const { return pObj()->getWidth(); }

  CcfgEnum::CarbonCfgReadmemType getReadmemType() const { return CcfgEnum::CarbonCfgReadmemType(pObj()->getReadmemType()); }
  void setReadmemType(CcfgEnum::CarbonCfgReadmemType newVal) { pObj()->putReadmemType(CarbonCfgReadmemType(newVal)); }

  CcfgEnum::CarbonCfgMemInitType getInitType() const { return CcfgEnum::CarbonCfgMemInitType(pObj()->getMemInitType()); }
  void setInitType(CcfgEnum::CarbonCfgMemInitType newVal) { pObj()->putMemInitType(CarbonCfgMemInitType(newVal)); }

  QString getSystemAddressESLPortName() const { return pObj()->getSystemAddressESLPortName(); }
  void setProgPreloadEslPort(const QString& newVal) { UtString nv; nv << newVal; pObj()->putSystemAddressESLPortName(nv.c_str()); }

  QString getDisassemblyName() const { return pObj()->getMemoryDisassemblyName(); }
  void setDisassemblyName(const QString& newVal) { UtString nv; nv << newVal; pObj()->putMemoryDisassemblyName(nv.c_str()); }

  bool getDisplayAtZero() const { return pObj()->getDisplayAtZero(); }
  void setDisplayAtZero(bool newVal) { pObj()->putDisplayAtZero(newVal); }

  quint32 getMAU() const { return pObj()->getMAU(); }
  void setMAU(quint32 newVal) { pObj()->putMAU(newVal); }

  bool getBigEndian() const { return pObj()->getBigEndian(); }
  void setBigEndian(bool newVal) { pObj()->putBigEndian(newVal); }

  bool getProgramMemory() const { return pObj()->getProgramMemory(); }
  void setProgramMemory(bool newVal) { pObj()->putProgramMemory(newVal); }

  CarbonCfgMemoryBlock* addMemoryBlock() { return pObj()->addMemoryBlock(); }
  void removeMemoryBlock(CarbonCfgMemoryBlock* block) { pObj()->removeMemoryBlock(block); }
  CarbonCfgMemoryBlock* getMemoryBlock(quint32 index) { return pObj()->getMemoryBlock(index); }

  quint32 numCustomCodes() const { return pObj()->numCustomCodes(); }
  CarbonCfgMemoryCustomCode* addCustomCode()
  {
    return static_cast<CarbonCfgMemoryCustomCode*>(pObj()->addCustomCode());
  }
  CarbonCfgMemoryCustomCode* getCustomCode(quint32 i) const
  {
    return static_cast<CarbonCfgMemoryCustomCode*>(pObj()->getCustomCode(i));
  }
  void removeCustomCode(CarbonCfgMemoryCustomCode* customCode)
  {
    pObj()->removeCustomCode(customCode);
  }
};

class ProtoCarbonCfgRegister : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgRegister");

  Q_PROPERTY(QString name READ getName)
  Q_PROPERTY(quint32 width READ getWidth WRITE setWidth)
  Q_PROPERTY(QString comment READ getComment WRITE setComment)
  Q_PROPERTY(quint32 offset READ getOffset WRITE setOffset)
  Q_PROPERTY(bool bigEndian READ getBigEndian WRITE setBigEndian)
  Q_PROPERTY(QString port READ getPort WRITE setPort)
  Q_PROPERTY(quint32 numFields READ numFields)
  Q_PROPERTY(QString groupName READ getGroupName)
  Q_PROPERTY(CcfgEnum::CarbonCfgRadix radix READ getRadix WRITE setRadix)
  Q_PROPERTY(quint32 numCustomCodes READ numCustomCodes)

public:
  ProtoCarbonCfgRegister(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgRegister* pObj() const { return qscriptvalue_cast<CarbonCfgRegister*>(thisObject()); }

public slots:
  void addField(CarbonCfgRegisterField* field) { pObj()->addField(field); }
  void removeField(CarbonCfgRegisterField* field) { pObj()->removeField(field); }
  CcfgEnum::CarbonCfgRadix getRadix() const 
  {
    CarbonCfgRadixIO radix = pObj()->getRadix();
    return static_cast<CcfgEnum::CarbonCfgRadix>((int)radix);
  }
  void setRadix(CcfgEnum::CarbonCfgRadix newVal) { pObj()->putRadix(static_cast<CarbonCfgRadix>(newVal)); }

  CarbonCfgRegisterField* getField(quint32 index) { return pObj()->getField(index); }

  quint32 numFields() const { return pObj()->numFields(); }
  QString getGroupName() const { return pObj()->getGroupName(); }

  QString getPort() const { return pObj()->getPort(); }
  void setPort(const QString& newVal)
  {
    UtString value;
    value << newVal;
    pObj()->putPort(value.c_str()); 
  }

  bool getBigEndian() const { return pObj()->getBigEndian(); }
  void setBigEndian(bool nv) { pObj()->putBigEndian(nv); }

  quint32 getOffset() const { return pObj()->getOffset(); }
  void setOffset(quint32 nv) { pObj()->putOffset(nv); }

  QString getComment() const { return pObj()->getComment(); }
  void setComment(const QString& newVal)
  {
    UtString value;
    value << newVal;
    pObj()->putComment(value.c_str()); 
  }

  quint32 getWidth() const { return pObj()->getWidth(); }
  void setWidth(quint32 nv) { pObj()->putWidth(nv); }

  QString getName() const { return pObj()->getName(); }

  quint32 numCustomCodes() const { return pObj()->numCustomCodes(); }
  CarbonCfgRegCustomCode* addCustomCode()
  {
    return pObj()->addCustomCode();
  }
  CarbonCfgRegCustomCode* getCustomCode(quint32 i) const
  {
    return pObj()->getCustomCode(i)->castReg();
  }
  void removeCustomCode(CarbonCfgRegCustomCode* customCode)
  {
    pObj()->removeCustomCode(customCode);
  }
};

class ProtoCarbonCfgXtorInstance : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgXtorInstance");
    
  Q_PROPERTY(QString name READ name);
  Q_PROPERTY(quint32 numConnections READ numConnections);
  Q_PROPERTY(quint32 numParams READ numParams);
  Q_PROPERTY(CarbonCfgXtorInstance* clockMaster READ getClockMaster WRITE setClockMaster);
  Q_PROPERTY(CarbonCfgXtor* xtor READ getXtor);

public:
  ProtoCarbonCfgXtorInstance(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgXtorInstance* pObj() const { return qscriptvalue_cast<CarbonCfgXtorInstance*>(thisObject()); }

public slots:
  CarbonCfgXtorInstance* getClockMaster() const { return pObj()->getClockMaster(); }
  void setClockMaster(CarbonCfgXtorInstance* clockMaster) { pObj()->putClockMaster(clockMaster); }

  CarbonCfgXtorParamInst* findParameter(const QString& paramName) 
  {
    UtString paramname;
    paramname << paramName;
    return pObj()->findParameterInst(paramname.c_str());
  }
  bool hideParameter(const QString& paramName) 
  {
    UtString paramname;
    paramname << paramName;
    return pObj()->hideParameterInst(paramname.c_str());
  }

  void addConnection(quint32 i, CarbonCfgXtorConn* conn) { pObj()->putConnection(i, conn); }
  CarbonCfgXtorConn* getConnection(quint32 i) { return pObj()->getConnection(i); }
  QString name() const { return pObj()->getName(); }
  quint32 numConnections() const { return pObj()->getType()->numPorts(); }
  quint32 numParams() const { return pObj()->numParams(); }
  void setUseDebugAccess(bool val) { pObj()->putUseDebugAccess(val); }
  CarbonCfgXtor* getXtor() const { return pObj()->getType(); }
};

class ProtoCarbonCfgXtorParam : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgXtorParam");

  Q_PROPERTY(QString name READ getName);
  Q_PROPERTY(quint32 size READ getSize);
  Q_PROPERTY(CcfgEnum::CarbonCfgParamFlag flag READ getFlag);
  Q_PROPERTY(CcfgEnum::CarbonCfgParamDataType type READ getType);
  Q_PROPERTY(QString description READ getDescription);
  Q_PROPERTY(QString enumChoices READ getEnumChoices);
  Q_PROPERTY(QString defaultValue READ getDefaultValue);
 
public:
  ProtoCarbonCfgXtorParam(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgXtorParam* pObj() const { return qscriptvalue_cast<CarbonCfgXtorParam*>(thisObject()); }

public slots:
  QString getName() const { return pObj()->getName(); }
  quint32 getSize() const { return pObj()->getSize(); }
  CcfgEnum::CarbonCfgParamFlag getFlag() const { return CcfgEnum::CarbonCfgParamFlag(pObj()->getFlag()); }
  CcfgEnum::CarbonCfgParamDataType getType() const { return CcfgEnum::CarbonCfgParamDataType(pObj()->getType()); }
  QString getDescription() const { return pObj()->getDescription(); }
  QString getEnumChoices() const { return pObj()->getEnumChoices(); }
  QString getDefaultValue() const { return pObj()->getDefaultValue(); }
  bool inRange(const QString& value)
  {
    UtString cValue;
    cValue << value;
    return pObj()->inRange(cValue.c_str());
  }
};

class ProtoCarbonCfgXtorParamInst : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgXtorParamInst");

  Q_PROPERTY(QString value READ getValue WRITE putValue)
  Q_PROPERTY(QString defaultValue READ getDefaultValue)
  Q_PROPERTY(quint32 numRTLPorts READ numRTLPorts)
  Q_PROPERTY(quint32 numEnumChoices READ getNumEnumChoices)
  Q_PROPERTY(CarbonCfgXtorInstance* instance READ getInstance)
  Q_PROPERTY(CarbonCfgXtorParam* param READ getParam)
  Q_PROPERTY(bool hidden READ getHidden WRITE putHidden)

public:
  ProtoCarbonCfgXtorParamInst(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgXtorParamInst* pObj() const { return qscriptvalue_cast<CarbonCfgXtorParamInst*>(thisObject()); }

public slots:
  CarbonCfgRTLPort* getRTLPort(quint32 i) { return pObj()->getRTLPort(i); }
  quint32 numRTLPorts() const { return pObj()->numRTLPorts(); }
  quint32 getNumEnumChoices() { return pObj()->numEnumChoices();}
  QString getDefaultValue() const { return pObj()->getDefaultValue(); }
  QString getValue() const { return pObj()->getValue(); }
  void putValue(const QString& newVal)
  {
    UtString value;
    value << newVal;
    pObj()->putValue(value.c_str());
  }
  bool removeEnumChoice(const QString& choice)
  {
    UtString value;
    value << choice;
    return pObj()->removeEnumChoice(value.c_str());
  }
  QString getEnumChoice(quint32 i) { return pObj()->getEnumChoice(i);}

  CarbonCfgXtorInstance* getInstance() const { return pObj()->getInstance(); }
  CarbonCfgXtorParam* getParam() const { return pObj()->getParam(); }

  bool getHidden() const { return pObj()->hidden(); }
  void putHidden(bool hidden) { pObj()->putHidden(hidden); }
};

class ProtoCarbonCfgXtorLib : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgXtorLib");


  Q_PROPERTY(quint32 numXtors READ numXtors)

public:
  ProtoCarbonCfgXtorLib(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgXtorLib* pObj() const { return qscriptvalue_cast<CarbonCfgXtorLib*>(thisObject()); }

public slots:
  quint32 numXtors() const { return pObj()->numXtors(); }
  CarbonCfgXtor* getXtor(quint32 i) { return pObj()->getXtor(i); }
  CarbonCfgXtor* findXtor(const QString& xtorName)
  { 
    UtString name;
    name << xtorName;
    return pObj()->findXtor(name.c_str());
  }
};

class ProtoCarbonCfgXtorPort : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgXtorPort");

  Q_PROPERTY(QString name READ name)
  Q_PROPERTY(quint32 size READ size)
  Q_PROPERTY(CcfgEnum::CarbonCfgRTLPortType type READ getType)

public:

  ProtoCarbonCfgXtorPort(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgXtorPort* pObj() const { return qscriptvalue_cast<CarbonCfgXtorPort*>(thisObject()); }

public slots:
  CcfgEnum::CarbonCfgRTLPortType getType() const 
  {
    return static_cast<CcfgEnum::CarbonCfgRTLPortType>(pObj()->getType()); 
  }
  QString name() const { return pObj()->getName(); }
  quint32 size() const { return pObj()->getSize(); }

};

class ProtoCarbonCfgXtor : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgXtor");
 
  Q_PROPERTY(QString name READ name)
  Q_PROPERTY(quint32 numPorts READ numPorts)

public:
  ProtoCarbonCfgXtor(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgXtor* pObj() const { return qscriptvalue_cast<CarbonCfgXtor*>(thisObject()); }

public slots:
  CarbonCfgXtorPort* getPort(quint32 i) { return pObj()->getPort(i); }
  CarbonCfgXtorPort* findPort(const QString& portName)
  { 
    UtString name;
    name << portName;
    return pObj()->findPort(name.c_str());
  }
  CarbonCfgXtorParam* findParam(const QString& paramName)
  { 
    UtString name;
    name << paramName;
    return pObj()->findParam(name.c_str());
  }
  QString name() const { return pObj()->getName(); }
  quint32 numPorts() const { return pObj()->numPorts(); }
};

class ProtoCarbonCfgESLPort : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgESLPort");

  Q_PROPERTY(QString name READ name)
  Q_PROPERTY(QString expr READ expr WRITE putExpr)
  Q_PROPERTY(QString typeDef READ typeDef)
  Q_PROPERTY(CarbonCfgRTLPort* rtlPort READ rtlPort)
  Q_PROPERTY(CcfgEnum::CarbonCfgESLPortType portType READ getPortType)
  Q_PROPERTY(CarbonCfgXtorParamInst* parameterInstance READ getParameterInstance WRITE setParameterInstance)

public:
  ProtoCarbonCfgESLPort(QObject *parent = 0) : QObject(parent) {}

  CarbonCfgESLPort* pObj() const { return qscriptvalue_cast<CarbonCfgESLPort*>(thisObject()); }

public slots:
  QString name() const { return pObj()->getName(); }
  QString expr() const { return pObj()->getExpr(); }
  CcfgEnum::CarbonCfgESLPortType getPortType() const { return CcfgEnum::CarbonCfgESLPortType(pObj()->getPortType()); }

  CarbonCfgXtorParamInst* getParameterInstance() const { return pObj()->getParamInstance(); }
  void setParameterInstance(CarbonCfgXtorParamInst* inst) { pObj()->putParamInstance(inst); }

  void putExpr(const QString& newVal)
  {
    UtString value;
    value << newVal;
    pObj()->putExpr(value.c_str());
  }
  QString typeDef() const { return pObj()->getTypeDef(); }
  CarbonCfgRTLPort* rtlPort() const { return pObj()->getRTLPort(); }
};

class ProtoCarbonCfgResetGen : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgResetGen");

public:
  ProtoCarbonCfgResetGen(QObject *parent = 0) : QObject(parent) {}
};

class ProtoCarbonCfgClockGen : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgClockGen");

public:
  ProtoCarbonCfgClockGen(QObject *parent = 0) : QObject(parent) {}
};

class ProtoCarbonCfgTie : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgTie");

public:
  ProtoCarbonCfgTie(QObject *parent = 0) : QObject(parent) {}
};

class ProtoCarbonCfgTieParam : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgTieParam");

  Q_PROPERTY(QString param READ getParam WRITE setParam)
  Q_PROPERTY(QString xtorInstanceName READ getXtorInstName WRITE setXtorInstName)
  Q_PROPERTY(QString fullName READ getFullName WRITE setFullName)

public:
  ProtoCarbonCfgTieParam(QObject *parent = 0) : QObject(parent) {}

private:
  CarbonCfgTieParam* pObj() const { return qscriptvalue_cast<CarbonCfgTieParam*>(thisObject()); }

  QString getParam() const { return pObj()->mParam.c_str(); }
  void setParam(const QString& newVal)
  {
    pObj()->mParam.clear();
    pObj()->mParam << newVal;
  }

  QString getXtorInstName() const { return pObj()->mXtorInstanceName.c_str(); }
  void setXtorInstName(const QString& newVal)
  {
    pObj()->mXtorInstanceName.clear();
    pObj()->mXtorInstanceName << newVal;
  }

  QString getFullName() const { return pObj()->mFullParamName.c_str(); }
  void setFullName(const QString& newVal)
  {
    pObj()->mFullParamName.clear();
    pObj()->mFullParamName << newVal;
  }
};

class ProtoCarbonCfgXtorConn : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgXtorConn");

  Q_PROPERTY(CarbonCfgRTLPort* rtlPort READ getRTLPort);
  Q_PROPERTY(QString expr READ getExpr WRITE putExpr);

public:
  ProtoCarbonCfgXtorConn(QObject *parent = 0) : QObject(parent) {}
  CarbonCfgXtorConn* pObj() const { return qscriptvalue_cast<CarbonCfgXtorConn*>(thisObject()); }
  CarbonCfgRTLPort* getRTLPort() const { return pObj()->getRTLPort(); }
  QString getExpr() const { return pObj()->getExpr(); }
  void putExpr(const QString& newVal)
  {
    UtString value;
    value << newVal;
    pObj()->putExpr(value.c_str());
  }
};

class ProtoCarbonCfgRTLConnection : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgRTLConnection");

public:
  Q_PROPERTY(CcfgEnum::CarbonCfgRTLConnectionType type READ getType)
  Q_PROPERTY(CarbonCfgRTLPort* rtlPort READ getRTLPort)
  Q_PROPERTY(CarbonCfgClockGen* clockGen READ getClockGen)
  Q_PROPERTY(CarbonCfgResetGen* resetGen READ getResetGen)
  Q_PROPERTY(CarbonCfgTie* tie READ getTie)
  Q_PROPERTY(CarbonCfgTieParam* tieParam READ getTieParam)
  Q_PROPERTY(CarbonCfgXtorConn* xtorConn READ getXtorConn)

  ProtoCarbonCfgRTLConnection(QObject *parent = 0) : QObject(parent) {}

private:
  CarbonCfgRTLConnection* pObj() const { return qscriptvalue_cast<CarbonCfgRTLConnection*>(thisObject()); }

public slots:
  CcfgEnum::CarbonCfgRTLConnectionType getType() const
  {
    return static_cast<CcfgEnum::CarbonCfgRTLConnectionType>(pObj()->getType());
  }
  CarbonCfgRTLPort* getRTLPort() const { return pObj()->getRTLPort(); }
  CarbonCfgClockGen* getClockGen() const { return pObj()->castClockGen(); }
  CarbonCfgResetGen* getResetGen() const { return pObj()->castResetGen(); }
  CarbonCfgTie* getTie() const { return pObj()->castTie(); }
  CarbonCfgTieParam* getTieParam() const { return pObj()->castTieParam(); }
  CarbonCfgXtorConn* getXtorConn() const { return pObj()->castXtorConn(); }
  CarbonCfgESLPort* getESLPort() const { return pObj()->castESLPort(); }
};

class ProtoCarbonCfgRTLPort : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgRTLPort");

public:

  Q_PROPERTY(QString name READ name)
  Q_PROPERTY(quint32 width READ width)
  Q_PROPERTY(CcfgEnum::CarbonCfgRTLPortType portType READ portType)
  Q_PROPERTY(quint32 numConnections READ numConnections)

public:
  ProtoCarbonCfgRTLPort(QObject *parent = 0) : QObject(parent) {}

public slots:
  CarbonCfgRTLPort* pObj() const { return qscriptvalue_cast<CarbonCfgRTLPort*>(thisObject()); }
  
  bool isDisconnected() const { return pObj()->isDisconnected(); }
  bool isConnected() const { return pObj()->isConnected(); }
  bool isResetGen() const { return pObj()->isResetGen(); }
  bool isClockGen() const { return pObj()->isClockGen(); }
  bool isTied() const { return pObj()->isTied(); }
  bool isTiedToParam() const { return pObj()->isTiedToParam(); }
  bool isConnectedToXtor() const { return pObj()->isConnectedToXtor(); }

  void connect(QVariant c)
  {
    CarbonCfgClockGen* cg = qvariant_cast<CarbonCfgClockGen*>(c);
    if (cg)
      connectClockGen(cg);

    CarbonCfgResetGen* crg = qvariant_cast<CarbonCfgResetGen*>(c);
    if (crg)
      connectResetGen(crg);

    CarbonCfgTie* ctie = qvariant_cast<CarbonCfgTie*>(c);
    if (ctie)
      connectTie(ctie);

    CarbonCfgTieParam* ctiep = qvariant_cast<CarbonCfgTieParam*>(c);
    if (ctiep)
      connectTieParam(ctiep);

    CarbonCfgESLPort* ceslp = qvariant_cast<CarbonCfgESLPort*>(c);
    if (ceslp)
      connectESLPort(ceslp);

    CarbonCfgXtorConn* cxtor = qvariant_cast<CarbonCfgXtorConn*>(c);
    if (cxtor)
      connectXtorConn(cxtor);

    CarbonCfgRTLConnection* crtl = qvariant_cast<CarbonCfgRTLConnection*>(c);
    if (crtl)
      connectRTLConn(crtl);
  }
  void connectClockGen(CarbonCfgClockGen* conn) { pObj()->connect(conn); }
  void connectResetGen(CarbonCfgResetGen* conn) { pObj()->connect(conn); }
  void connectTie(CarbonCfgTie* conn) { pObj()->connect(conn); }
  void connectTieParam(CarbonCfgTieParam* conn) { pObj()->connect(conn); }
  void connectESLPort(CarbonCfgESLPort* conn) { pObj()->connect(conn); }
  void connectXtorConn(CarbonCfgXtorConn* conn) { pObj()->connect(conn); }
  void connectRTLConn(CarbonCfgRTLConnection* conn) { pObj()->connect(conn); }

  // Note that disconnect got removed because it can break the data model.
  // Please use disconnect through ScrCcfg.

  CcfgEnum::CarbonCfgRTLPortType portType() const 
  {
    return static_cast<CcfgEnum::CarbonCfgRTLPortType>(pObj()->getType());
  }
  QString name() const { return pObj()->getName(); }
  quint32 width() const { return pObj()->getWidth(); }
  quint32 numConnections() const { return pObj()->numConnections(); }
  CarbonCfgRTLConnection* getConnection(quint32 i) const { return pObj()->getConnection(i); }
};

class ProtoCarbonCfgCadi : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "CarbonCfgCadi");

  Q_PROPERTY(QString disassemblerName READ getDisassemblerName WRITE setDisassemblerName);
  Q_PROPERTY(quint32 numCustomCodes READ numCustomCodes)

public:
  ProtoCarbonCfgCadi(QObject* parent = NULL) : QObject(parent) {}
  CarbonCfgCadi* pObj() const { return qscriptvalue_cast<CarbonCfgCadi*>(thisObject()); }

public slots:
  QString getDisassemblerName() const
  { 
    return pObj()->getDisassemblerName();
  }
  void setDisassemblerName(const QString& newVal)
  {
    UtString nv; nv << newVal; pObj()->putDisassemblerName(nv.c_str());
  }

  quint32 numCustomCodes() const { return pObj()->numCustomCodes(); }
  CarbonCfgCadiCustomCode* addCustomCode()
  {
    return static_cast<CarbonCfgCadiCustomCode*>(pObj()->addCustomCode());
  }
  CarbonCfgCadiCustomCode* getCustomCode(quint32 i) const
  {
    return static_cast<CarbonCfgCadiCustomCode*>(pObj()->getCustomCode(i));
  }
  void removeCustomCode(CarbonCfgCadiCustomCode* customCode)
  {
    pObj()->removeCustomCode(customCode);
  }
}; // class ProtoCarbonCfgCadi : public QObject, public QScriptable

class ScrCcfg : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "Ccfg");
  Q_CLASSINFO("Template", "CarbonCfgTemplate*");
  Q_CLASSINFO("ProcInfo", "CarbonCfgProcInfo*");

  Q_PROPERTY(QString filePath READ getFilePath WRITE setFilePath)
  Q_PROPERTY(quint32 numSubComponents READ getNumSubComponents)
  Q_PROPERTY(quint32 numXtorInstances READ getNumXtorInstances)
  Q_PROPERTY(quint32 numESLPorts READ getNumESLPorts)
  Q_PROPERTY(quint32 numRTLPorts READ getNumRTLPorts)
  Q_PROPERTY(quint32 numParams READ getNumParams)
  Q_PROPERTY(quint32 numRegisters READ getNumRegisters)
  Q_PROPERTY(quint32 numGroups READ getNumGroups)
  Q_PROPERTY(quint32 numMemories READ getNumMemories)
  Q_PROPERTY(quint32 numCustomCodes READ getNumCustomCodes);
  Q_PROPERTY(CarbonCfgXtorLib* xtorLib READ getXtorLib)
  Q_PROPERTY(QString description READ getDescription WRITE setDescription);
  Q_PROPERTY(QString cxxFlags READ getCxxFlags WRITE setCxxFlags);
  Q_PROPERTY(QString linkFlags READ getLinkFlags WRITE setLinkFlags);
  Q_PROPERTY(QString sourceFiles READ getSourceFiles WRITE setSourceFiles);
  Q_PROPERTY(QString includeFiles READ getIncludeFiles WRITE setIncludeFiles);
  Q_PROPERTY(QString libName READ getLibName WRITE setLibName);
  Q_PROPERTY(QString compName READ getCompName WRITE setCompName);
  Q_PROPERTY(QString compDisplayName READ getCompDisplayName WRITE setCompDisplayName);
  Q_PROPERTY(QString topModuleName READ getTopModuleName WRITE setTopModuleName);
  Q_PROPERTY(QString waveFile READ getWaveFile WRITE setWaveFile);
  Q_PROPERTY(CcfgEnum::CarbonCfgWaveType waveType READ getWaveType WRITE setWaveType);
  Q_PROPERTY(CarbonCfgELFLoader* ELFLoader READ getELFLoader);
  Q_PROPERTY(CarbonCfgProcInfo* procInfo READ getProcInfo);
  Q_PROPERTY(CarbonCfgTemplate* Template READ getTemplate);
  Q_PROPERTY(CarbonCfgProcInfo* ProcInfo READ getProcInfo);
  Q_PROPERTY(QString type READ getType WRITE setType);
  Q_PROPERTY(QString version READ getVersion WRITE setVersion);
  Q_PROPERTY(CarbonCfgCadi* Cadi READ getCadi);
  Q_PROPERTY(bool requiresMkLibrary READ getRequiresMkLibrary WRITE setRequiresMkLibrary);
  Q_PROPERTY(bool useVersionedMkLibrary READ getUseVersionedMkLibrary WRITE setUseVersionedMkLibrary);
  Q_PROPERTY(bool subComponentFullName READ isSubComponentFullName WRITE setSubComponentFullName);
  Q_PROPERTY(QString pcTraceAccessor READ getPCTraceAccessor WRITE setPCTraceAccessor);
  Q_PROPERTY(CcfgFlags::CarbonCfgComponentEditFlags EditFlags READ GetEditFlags WRITE SetEditFlags);

public:
  static void registerTypes(QScriptEngine* engine);
  static void registerScriptTypes(QMap<QString, const QMetaObject*>& map);
  static void registerIntellisense(JavaScriptAPIs* apis);

public:
  ScrCcfg(QObject* parent=0, CarbonCfg* cfg=0);
  virtual ~ScrCcfg()
  {
  }

private:
  CarbonCfg* mCfg;
  QString mFilePath;
  CarbonCfgTemplate* getTemplate();

public slots:
  CcfgFlags::CarbonCfgComponentEditFlags GetEditFlags() const { return mCfg->GetEditFlags(); }
  void SetEditFlags(CcfgFlags::CarbonCfgComponentEditFlags newValue) { mCfg->SetEditFlags(newValue); }

  QString getFilePath() const { return mFilePath; }
  void setFilePath(const QString& newVal) { mFilePath=newVal; }

  QString getDescription() const { return mCfg->getDescription(); }
  void setDescription(const QString& newVal) { UtString nv; nv << newVal; mCfg->putDescription(nv.c_str()); }

  QString getCxxFlags() const { return mCfg->getCxxFlags(); }
  void setCxxFlags(const QString& newVal) { UtString nv; nv << newVal; mCfg->putCxxFlags(nv.c_str()); }

  QString getLinkFlags() const { return mCfg->getLinkFlags(); }
  void setLinkFlags(const QString& newVal) { UtString nv; nv << newVal; mCfg->putLinkFlags(nv.c_str()); }

  QString getSourceFiles() const { return mCfg->getSourceFiles(); }
  void setSourceFiles(const QString& newVal) { UtString nv; nv << newVal; mCfg->putSourceFiles(nv.c_str()); }

  QString getIncludeFiles() const { return mCfg->getIncludeFiles(); }
  void setIncludeFiles(const QString& newVal) { UtString nv; nv << newVal; mCfg->putIncludeFiles(nv.c_str()); }

  QString getLibName() const { return mCfg->getLibName(); }
  void setLibName(const QString& newVal) { UtString nv; nv << newVal; mCfg->putLibName(nv.c_str()); }

  QString getCompName() const { return mCfg->getCompName(); }
  void setCompName(const QString& newVal);

  QString getCompDisplayName() const { return mCfg->getCompDisplayName(); }
  void setCompDisplayName(const QString& newVal) { UtString nv; nv << newVal; mCfg->putCompDisplayName(nv.c_str()); }

  QString getTopModuleName() const { return mCfg->getTopModuleName(); }
  void setTopModuleName(const QString& newVal) { UtString nv; nv << newVal; mCfg->putTopModuleName(nv.c_str()); }

  QString getWaveFile() const { return mCfg->getWaveFile(); }
  void setWaveFile(const QString& newVal) { UtString nv; nv << newVal; mCfg->putWaveFile(nv.c_str()); }

  CcfgEnum::CarbonCfgWaveType getWaveType() const { return CcfgEnum::CarbonCfgWaveType(mCfg->getWaveType()); }
  void setWaveType(CcfgEnum::CarbonCfgWaveType wt) { mCfg->putWaveType(CarbonCfgWaveType(wt)); }

  QString getType(void) const
  {
    return mCfg->getCompType();
  }

  void setType(const QString& name)
  {
    UtString n;
    n << name;
    mCfg->putCompType(n.c_str());
  }

  QString getVersion(void) const
  {
    return mCfg->getCompVersion();
  }

  void setVersion(const QString& name)
  {
    UtString n;
    n << name;
    mCfg->putCompVersion(n.c_str());
  }

  QString getDocFile(void) const
  {
    return mCfg->getCompDocFile();
  }

  void setDocFile(const QString& name)
  {
    UtString n;
    n << name;
    mCfg->putCompDocFile(n.c_str());
  }

  QString getLoadfileExtension() const { return mCfg->getLoadfileExtension(); }
  void setLoadfileExtension(const QString& newVal)
  {
    UtString nv;
    nv << newVal;
    mCfg->putLoadfileExtension(nv.c_str());
  }

  bool getUseStaticScheduling() const { return mCfg->getUseStaticScheduling(); }
  void setUseStaticScheduling(qint32 val) { mCfg->putUseStaticScheduling(val); }

  int getLegacyMemories() const { return mCfg->useLegacyMemories(); }
  void setLegacyMemories(qint32 val) { mCfg->putLegacyMemories(val); }

  int isStandAloneComp() const { return mCfg->isStandAloneComp(); }
  void setIsStandAloneComp(qint32 val) { mCfg->putIsStandAloneComp(val); }

  int isSubComponentFullName() const { return mCfg->isSubComponentFullName(); }
  void setSubComponentFullName(qint32 val) { mCfg->putSubComponentFullName(val); }

  quint32 getNumSubComponents() const { return mCfg->numSubComponents(); }
  quint32 getNumXtorInstances() const { return mCfg->numXtorInstances(); }
  quint32 getNumESLPorts() const { return mCfg->numESLPorts(); }
  quint32 getNumRTLPorts() const { return mCfg->numRTLPorts(); }
  quint32 getNumParams() const { return mCfg->numParams(); }
  quint32 getNumRegisters() const { return mCfg->numRegisters(); }
  quint32 getNumGroups() const { return mCfg->numGroups(); }
  quint32 getNumMemories() const { return mCfg->numMemories(); }
  quint32 getNumCustomCodes() const { return mCfg->numCustomCodes(); }

  CcfgEnum::CarbonCfgStatus read(CcfgEnum::CarbonCfgMode mode, const QString& fileName);
  CcfgEnum::CarbonCfgStatus readNoCheck(CcfgEnum::CarbonCfgMode mode, const QString& fileName);
  CcfgEnum::CarbonCfgStatus write(const QString& fileName);
  CcfgEnum::CarbonCfgStatus write();

  CarbonCfgELFLoader* getELFLoader() { return mCfg->getELFLoader(); }
  CarbonCfgProcInfo* getProcInfo() { return mCfg->getProcInfo(); }

  ScrCcfg* addSubComp(const QString& componentName)
  {
    UtString name;
    name << componentName;
    return new ScrCcfg(this, mCfg->addSubComp(name.c_str()));
  }

  void removeSubComp(ScrCcfg* comp)
  {
    mCfg->removeSubCompRecursive(comp->mCfg);
    delete comp;
  }

  void removeSubComps()
  {
    mCfg->removeSubCompsRecursive();
  }

  void removeRegisters()
  {
    mCfg->removeRegistersRecursive();
  }

  void removeMemories()
  {
    mCfg->removeMemoriesRecursive();
  }

  void removeCustomCodes()
  {
    mCfg->removeCustomCodes();
  }

  void removeXtorInstances()
  {
    mCfg->removeXtorInstancesRecursive();
  }

  void removeParameters()
  {
    mCfg->removeParameters();
  }

  void removeGroups()
  {
    mCfg->removeGroupsRecursive();
  }

  ScrCcfg* getSubComponent(quint32 i)
  {
    return new ScrCcfg(this, mCfg->getSubComponent(i));
  }

  QString getUniqueRegName(const QString& baseName)
  {
    UtString newName;
    UtString baseNameStr;
    baseNameStr << baseName;
    mCfg->getUniqueRegName(&newName, baseNameStr.c_str());
    return newName.c_str();
  }
  QString getUniqueGroupName(const QString& baseName)
  {
    UtString newName;
    UtString baseNameStr;
    baseNameStr << baseName;
    mCfg->getUniqueGroupName(&newName, baseNameStr.c_str());
    return newName.c_str();
  }

  QString getUniqueESLName(const QString& baseName)
  {
    UtString newName;
    UtString baseNameStr;
    baseNameStr << baseName;
    mCfg->getUniqueESLName(&newName, baseNameStr.c_str());
    return newName.c_str();
  }

  quint32 numGroups() const { return mCfg->numGroups(); }
  CarbonCfgGroup* getGroup(quint32 i) { return mCfg->getGroup(i); }

  CarbonCfgGroup* findGroup(const QString& groupName)
  {
    for (UInt32 i=0; i<mCfg->numGroups(); i++)
    {
      CarbonCfgGroup* group = mCfg->getGroup(i);
      QString gname = group->getName();
      if (gname == groupName)
        return group;
    }
    return NULL;
  }

  CarbonCfgGroup* addGroup(const QString& groupName)
  {
    UtString name;
    name << groupName;
    return mCfg->addGroup(name.c_str());
  }

  CarbonCfgXtorLib* getXtorLib() { return mCfg->findXtorLib(CARBON_DEFAULT_XTOR_LIB); }

  CarbonCfgESLPort* findESLPort(const QString& portName)
  {
    UtString name;
    name << portName;
    return mCfg->findESLPort(name.c_str()); 
  }
  CarbonCfgRTLPort* findRTLPort(const QString& portName)
  {
    UtString name;
    name << portName;
    return mCfg->findRTLPort(name.c_str()); 
  }
  CarbonCfgRegister* addRegister(const QString& debugName, CarbonCfgGroup* group, 
    quint32 width, bool bigEndian, CcfgEnum::CarbonCfgRadix radixVal, const QString& commentStr, bool pcReg = false)
  {
    CarbonCfgRadix radix = static_cast<CarbonCfgRadix>(radixVal);
    UtString debugname;
    debugname << debugName;
    UtString comment;
    comment << commentStr;
    return mCfg->addRegister(debugname.c_str(), group, width, bigEndian, radix, comment.c_str(), pcReg);
  }

  CarbonCfgCompCustomCode* addCustomCode()
  {
    return mCfg->addCustomCode();
  }
  CarbonCfgCompCustomCode* getCustomCode(quint32 i) const
  {
    return mCfg->getCustomCode(i);
  }
  void removeCustomCode(CarbonCfgCompCustomCode* customCode)
  {
    mCfg->removeCustomCode(customCode);
  }
  CarbonCfgESLPort* addESLPort(CarbonCfgRTLPort* rtlPort, const QString& name,
                               CcfgEnum::CarbonCfgESLPortType ptype,
                               const QString& typeDef,
                               CcfgEnum::CarbonCfgESLPortMode pmode)
    {
      UtString portName;
      portName << name;
      UtString portType;
      portType << typeDef;
      return mCfg->addESLPort(rtlPort, portName.c_str(),
                                   CarbonCfgESLPortType(ptype), portType.c_str(),
                                   CarbonCfgESLPortMode(pmode));
    }


  CarbonCfgXtorParamInst* addParam(const QString& Name, CcfgEnum::CarbonCfgParamDataType type,
    const QString& Value, CcfgEnum::CarbonCfgParamFlag scope, const QString& Description, const QString& EnumChoices = QString())
  {
    UtString name;
    name << Name;
    UtString value;
    value << Value;
    UtString description;
    description << Description;
    UtString enumChoices;
    enumChoices << EnumChoices;
    
    return mCfg->addParam(name.c_str(), CarbonCfgParamDataType(type), 
                          value.c_str(), CarbonCfgParamFlag(scope),
                          description.c_str(), enumChoices.c_str());
  }

  CarbonCfgXtorInstance* addXtor(const QString& instName, CarbonCfgXtor* type)
  {
    UtString name;
    name << instName;
    return mCfg->addXtor(name.c_str(), type);
  }

  CarbonCfgRegister* getRegister(quint32 i) const { return mCfg->getRegister(i); }
  CarbonCfgXtorInstance* getXtorInstance(quint32 i) const { return mCfg->getXtorInstance(i); }
  CarbonCfgESLPort* getESLPort(quint32 i) const { return mCfg->getESLPort(i); }
  CarbonCfgRTLPort* getRTLPort(quint32 i) const { return mCfg->getRTLPort(i); }
  CarbonCfgXtorParamInst* getParam(quint32 i) const { return mCfg->getParam(i); }
  CarbonCfgXtor* findXtor(const QString& xtorName)
  {
    UtString name;
    name << xtorName;
    return mCfg->findXtor(name.c_str(), CARBON_DEFAULT_XTOR_LIB);
  }
  CarbonCfgXtorInstance* findXtorInstance(const QString& xtorName)
  {
    UtString name;
    name << xtorName;
    return mCfg->findXtorInstance(name.c_str());
  }
  void removeXtorInstance(CarbonCfgXtorInstance* xtorInst)
  {
    mCfg->removeXtorInstanceRecursive(xtorInst);
  }

  CarbonCfgRegister* findRegister(const QString& regName)
  {
    for (UInt32 i=0; i<mCfg->numRegisters(); i++)
    {
      CarbonCfgRegister* reg = mCfg->getRegister(i);
      if (reg->getName() == regName)
        return reg;
    }
    return NULL;
  }

  void removeRegister(CarbonCfgRegister* reg)
  {
    mCfg->removeRegisterRecursive(reg);
  }

  void removeMemory(CarbonCfgMemory* mem)
  {
    mCfg->removeMemoryRecursive(mem);
  }

  void disconnect(CarbonCfgRTLConnection* conn)
  {
    mCfg->disconnect(conn);
  }

  CarbonCfgMemory* addMemory(const QString& name, quint32 width, quint32 maxAddr)
  {
    UtString uName;
    uName << name;
    return mCfg->addMemory(uName.c_str(), width, maxAddr);
  }

  quint32 numMemories() const { return mCfg->numMemories(); }
  CarbonCfgMemory* getMemory(quint32 i)
  {
    return mCfg->getMemory(i);
  }

  CarbonCfgCadi* getCadi() { return mCfg->getCadi(); }

  QScriptValue clone(QVariant co);

  CarbonCfgCompCustomCode* cloneCustomCode(CarbonCfgCompCustomCode* cc)
  {
    CarbonCfgCompCustomCode* newCc = mCfg->clone(cc);
    if (mCfg->hasMessages()) {
      UtIO::cout() << mCfg->getErrmsg();
      mCfg->clearMessages();
    }
    return newCc;
  }

  CarbonCfgMemory* cloneMemory(CarbonCfgMemory* cc)
  {
    CarbonCfgMemory* newMem = mCfg->clone(cc);
    if (mCfg->hasMessages()) {
      UtIO::cout() << mCfg->getErrmsg();
      mCfg->clearMessages();
    }
    return newMem;
  }

  void cloneCadi(CarbonCfgCadi* cadi)
  {
    mCfg->clone(cadi);
    if (mCfg->hasMessages()) {
      UtIO::cout() << mCfg->getErrmsg();
      mCfg->clearMessages();
    }
  }

  void cloneProcInfo(CarbonCfgProcInfo* procInfo)
  {
    mCfg->clone(procInfo);
    if (mCfg->hasMessages()) {
      UtIO::cout() << mCfg->getErrmsg();
      mCfg->clearMessages();
    }
  }


  CarbonCfgRegister* clone(CarbonCfgGroup* group, CarbonCfgRegister* cc)
  {
    CarbonCfgRegister* newReg = mCfg->clone(group, cc);
    if (mCfg->hasMessages()) {
      UtIO::cout() << mCfg->getErrmsg();
      mCfg->clearMessages();
    }
    return newReg;
  }

 
  void setRequiresMkLibrary(bool req)
  {
    mCfg->putRequiresMkLibrary(req);
  }

  bool getRequiresMkLibrary(void)
  {
    return mCfg->getRequiresMkLibrary();
  }

  void setUseVersionedMkLibrary(bool req)
  {
    mCfg->putUseVersionedMkLibrary(req);
  }

  bool getUseVersionedMkLibrary(void)
  {
    return mCfg->getUseVersionedMkLibrary();
  }

  QString getPCTraceAccessor() const { return mCfg->getPCTraceAccessor(); }
  void setPCTraceAccessor(const QString& newVal);
};

Q_DECLARE_METATYPE(ScrCcfg*);


#endif
