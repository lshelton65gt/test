//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "WorkspaceModelMaker.h"
#include "cmm.h"
#include <QtGui>

#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonProject.h"
#include "RichOutputWindow.h"
#include "SettingsEditor.h"
#include "SettingsContainer.h"
#include "Config.h"
#include "DlgNewProject.h"
#include "PropertyEditor.h"
#include "RemoteConsole.h"
#include "CarbonConsole.h"
#include "ErrorInfoWidget.h"
#include "ProjectWidget.h"
#include "MdiProject.h"


WorkspaceModelMaker::WorkspaceModelMaker(QProgressBar* pbar, QStatusBar* sbar)
{
  mProgressBar = pbar;
  mStatusBar = sbar;
  mProjectDockWindow = NULL;
  mPropertiesDockWindow = NULL;
  mRemoteConsoleDockWindow = NULL;
  mErrorInfoDockWindow = NULL;
}

WorkspaceModelMaker::~WorkspaceModelMaker()
{
}

void WorkspaceModelMaker::initialize(CarbonMakerContext* ctx)
{
  CarbonWorkspace::initialize(ctx);

  QDockWidget *dock3 = new QDockWidget(tr("Project Explorer"), ctx->getMainWindow());
  dock3->setObjectName("Project Explorer");

  mProjectDockWindow = dock3;

  ProjectWidget* pw = new ProjectWidget(ctx->getMainWindow(), ctx);
  pw->setContext(ctx);

  ctx->putCarbonProjectWidget(pw->getCarbonProjectWidget());
  QDockWidget *dock1 = new QDockWidget(tr("Console"), ctx->getMainWindow());

  pw->getCarbonProjectWidget()->putDockWidget(dock1);

  connect(ctx->getCarbonProjectWidget(), SIGNAL(projectClosing(CarbonProject*)), this, SLOT(projectClosing(CarbonProject*)));
  
  dock3->setWidget(pw);
  ctx->getMainWindow()->addDockWidget(Qt::LeftDockWidgetArea, dock3);

  CarbonConsole* remoteConsole = new CarbonConsole(ctx->getMainWindow());
  remoteConsole->setProjectWidget(ctx->getCarbonProjectWidget());

  dock1->setObjectName("Console");

  dock1->setWidget(remoteConsole);
  ctx->getMainWindow()->addDockWidget(Qt::BottomDockWidgetArea, dock1);

  mRemoteConsoleDockWindow = dock1;

  ctx->getCarbonProjectWidget()->setConsole(remoteConsole);

  QDockWidget *dock2 = new QDockWidget(tr("Error List"), ctx->getMainWindow());
  dock2->setObjectName("Error List");

  dock2->setWidget(new ErrorInfoWidget(ctx->getMainWindow(), ctx, remoteConsole, mProgressBar, mStatusBar));
  mErrorInfoDockWindow = dock2;

  ctx->getMainWindow()->addDockWidget(Qt::BottomDockWidgetArea, dock2);
  ctx->getMainWindow()->tabifyDockWidget(dock1,dock2);

  SettingsEditor* se = new SettingsEditor(ctx->getMainWindow());
  ctx->putSettingsEditor(se);
  se->putContext(ctx);

  QDockWidget *dock5 = new QDockWidget(tr("Properties"), ctx->getMainWindow());
  dock5->setObjectName("Properties");
  dock5->setWidget(se);
  ctx->getMainWindow()->addDockWidget(Qt::RightDockWidgetArea, dock5);


  mPropertiesDockWindow = dock5;
  
  cmm* cmm = ctx->getModelStudio();
  if (cmm)
  {
    cmm->registerDockWidget(dock1, "&Console", "Ctrl+T,Ctrl+C");
    cmm->registerDockWidget(dock2, "&Error List", "Ctrl+T,Ctrl+E");
    cmm->registerDockWidget(dock3, "Project E&xplorer", "Ctrl+T,Ctrl+X");
    cmm->registerDockWidget(dock5, "&Properties", "Ctrl+T,Ctrl+P");
  }
}

void WorkspaceModelMaker::projectClosing(CarbonProject*)
{
  getContext()->getWorkspace()->closeAllWindows();
}
