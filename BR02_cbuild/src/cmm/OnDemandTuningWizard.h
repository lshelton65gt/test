#ifndef __OnDemandTuningWizard_h__
#define __OnDemandTuningWizard_h__

#include "CarbonRuntimeState.h"
#include <QtGui>
#include <QWizard>

class CarbonRuntimeState;
class WizardState;


///////////////////////////////////////////////////////////////////////////////
class WizardState
{
public:
  WizardState(CarbonRuntimeState *state)
    : mRuntimeState(state) {

    mCreateTraceFile = false;
  }

  bool mCreateTraceFile;
  CarbonRuntimeState *mRuntimeState;
};


///////////////////////////////////////////////////////////////////////////////
class SignalDisplay : public QWidget
{
public:
  SignalDisplay(const char *signalName, QWidget * parent = 0)
    :QWidget(parent) {
    
    // TODO: eventually, this should be a display of all net aliases, along with
    //       buttons to view wource, view waveforms, etc.
    QLabel *label = new QLabel(signalName);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(label);
    setLayout(layout);    
  }
};

///////////////////////////////////////////////////////////////////////////////
class OnDemandWizardPage :  public QWizardPage
{
public:
  OnDemandWizardPage(WizardState *state, QWidget * parent = 0)
    : QWizardPage(parent), mState(state) {
  }

protected:
  WizardState *mState;
};

///////////////////////////////////////////////////////////////////////////////
class IntroPage : public OnDemandWizardPage
{
  Q_OBJECT
public:
  IntroPage(WizardState *state, QWidget * parent = 0)
    : OnDemandWizardPage(state, parent) {

    setTitle("Welcome to the OnDemand Tuning Wizard");
    setSubTitle(" ");

    QLabel *label = new QLabel(
    "This wizard will help you optimize the performance "
    "of the Carbon OnDemand system.\n\n"
    "OnDemand works by detecting when a model is in an idle (repeating) state, "
    "during which it can respond to the rest of the system by playing back "
    "previously-computed output values.\n\n"
    "In order to optimize performance, we must first trace the operation of "
    "the OnDemand system in order to determine when it is unable to detect idle states."
    );
    label->setWordWrap(true);

    mRadioExistingTrace = new QRadioButton("Use existing trace file", this);
    mRadioCreateTrace = new QRadioButton("Create a new trace file", this);

    mRadioExistingTrace->setChecked(!mState->mCreateTraceFile);
    mRadioCreateTrace->setChecked(mState->mCreateTraceFile);

    // TODO: widgets for selecting existing and/or new trace file name

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(label);
    layout->addWidget(mRadioExistingTrace);
    layout->addWidget(mRadioCreateTrace);
    setLayout(layout);

    connect(mRadioCreateTrace, SIGNAL(clicked(bool)), this, SLOT(createTraceSelected(bool)));
    connect(mRadioExistingTrace, SIGNAL(clicked(bool)), this, SLOT(existingTraceSelected(bool)));
  }

  virtual bool isComplete() {
    return mRadioExistingTrace->isChecked() || mRadioCreateTrace->isChecked();
  }

private slots:
  void createTraceSelected(bool selected) {
    mState->mCreateTraceFile = selected;
    emit completeChanged();
  }

  void existingTraceSelected(bool selected) {
    mState->mCreateTraceFile = !selected;
    emit completeChanged();
  }

private:
  QRadioButton *mRadioExistingTrace;
  QRadioButton *mRadioCreateTrace;
};


///////////////////////////////////////////////////////////////////////////////
class OnDemandTuningWizard : public QWizard
{
 public:
  OnDemandTuningWizard(CarbonRuntimeState *state, QWidget * parent = 0, Qt::WindowFlags flags = 0);
  ~OnDemandTuningWizard();

 private:
  WizardState *mState;
};

#endif
