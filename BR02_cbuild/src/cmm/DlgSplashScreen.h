#ifndef DLGSPLASHSCREEN_H
#define DLGSPLASHSCREEN_H

#include <QDialog>
#include "ui_DlgSplashScreen.h"

class DlgSplashScreen : public QDialog
{
    Q_OBJECT

public:
    DlgSplashScreen(QWidget *parent = 0);
    ~DlgSplashScreen();

private:
    Ui::DlgSplashScreenClass ui;
};

#endif // DLGSPLASHSCREEN_H
