#ifndef __CARBONPROPENUM_H__
#define __CARBONPROPENUM_H__

#include "util/CarbonPlatform.h"
#include <QObject>
#include "CarbonProperty.h"

#include "util/CarbonAssert.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"

#include "PropertyEditor.h"

class CarbonPropertyEnum : public CarbonProperty
{
public:
  CARBONMEM_OVERRIDES

    CarbonPropertyEnum(const char* name, int nv, const char* descr) : CarbonProperty(name,nv,descr,CarbonProperty::Enum){}

  class EnumChoice
  {
  public:
    EnumChoice(const char* choice, const char* value)
    {
      mChoice=choice;
      mValue=value;
    }

    const char* getChoice() { return mChoice.c_str(); }
    const char* getValue() { return mValue.c_str(); }

  private:
    UtString mChoice;
    UtString mValue;
  };

  // Pure Overrides
  virtual bool parseXML(xmlNodePtr parent, UtXmlErrorHandler* eh);
  virtual bool writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* eh);
  virtual bool readValueXML(xmlNodePtr parent, CarbonOptions* options, UtXmlErrorHandler* eh) const;

  virtual CarbonDelegate* createDelegate(QObject* parent,  PropertyEditorDelegate* d, PropertyEditor* propEd);

  UInt32 numChoices() const { return mChoices.size(); }
  EnumChoice* getChoice(UInt32 i) const
  {
    INFO_ASSERT((int)i < mChoices.size(), "Index out of range");
    return mChoices[(int)i];
  }

  void addChoice(const char* choice, const char* value)
  {
    EnumChoice* ec = new EnumChoice(choice,value);
    mChoices.push_back(ec);
  }

private:

  QList<EnumChoice*> mChoices;
};




class CarbonDelegateEnum : public CarbonDelegate
{
  Q_OBJECT
public:
  CarbonDelegateEnum(QObject *parent = 0, PropertyEditorDelegate* d=0, PropertyEditor* e=0);
  virtual QWidget* createEditor(QWidget *parent, QTreeWidgetItem* item, 
    CarbonProperty* prop, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const;

  private slots:
    void currentIndexChanged(int index);
};

#endif
