//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"
#include "util/UtHashSet.h"

#include "util/CExprParse.h"

#include "CompWizardPortEditor.h"
#include "CompWizardTreeNodes.h"
#include "PortEditorCommands.h"
#include "ESLClockGen.h"
#include "CcfgHelper.h"
#include "cmm.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"


// SystemC Clock

UndoData* SystemCClockGenItem::deleteItem()
{
   SystemCClockGenItem::Delete* undoData = new SystemCClockGenItem::Delete();
 
   undoData->mCfg = getCcfg();
   undoData->setText(mClockGen->getRTLPort()->getName());
   undoData->setTreeData(treeWidget(), this);
   undoData->mRTLPortName = mClockGen->getRTLPort()->getName();

   undoData->mInitialValue = mClockGen->getInitialValue();
   undoData->mPeriod = mClockGen->getPeriod();
   undoData->mPeriodUnits = mClockGen->getPeriodUnits();
   undoData->mDutyCycle = mClockGen->getDutyCycle();
   undoData->mStartTime = mClockGen->getStartTime();
   undoData->mStartTimeUnits = mClockGen->getStartTimeUnits();

   return undoData;
}

void SystemCClockGenItem::Delete::undo()
{
  qDebug() << "Undo SystemC ClockGen Delete" << text();

  UtString uESLPortName; uESLPortName << mESLPortName;
  CarbonCfgESLPort* eslPort = mCfg->findESLPort(uESLPortName.c_str());
  INFO_ASSERT(eslPort, "Expecting ESL Port");
  mCfg->disconnect(eslPort);

  QTreeWidgetItem* eslItem = getTree()->itemFromIndex(mESLPortIndex);
  INFO_ASSERT(eslItem, "Expecting ESL Port Item");
  getTree()->removeItem(eslItem);

  UtString uRTLPortName; uRTLPortName << mRTLPortName;
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  INFO_ASSERT(rtlPort, "Expecting RTL Port");

  // Create the resetgen and tie it back to this port
  CarbonCfgSystemCClock* clockGen = new CarbonCfgSystemCClock(rtlPort);

  clockGen->putDutyCycle(mDutyCycle);
  clockGen->putInitialValue(mInitialValue);
  clockGen->putPeriod(mPeriod, mPeriodUnits);
  clockGen->putStartTime(mStartTime, mStartTimeUnits);

  rtlPort->connect(clockGen);
  
  QTreeWidgetItem* clocksRoot = getTree()->getEditor()->getSystemCClocksRoot();
  SystemCClockGenItem* newItem = new SystemCClockGenItem(clocksRoot, clockGen);

  setTreeData(getTree(), newItem);
  clocksRoot->setExpanded(true);

  newItem->setSelected(true);
  getTree()->scrollToItem(newItem);
  getTree()->setModified(false);
}

void SystemCClockGenItem::Delete::redo()
{
  qDebug() << "Delete ClockGen" << text();

  UtString uRTLPortName; uRTLPortName << mRTLPortName;

  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  INFO_ASSERT(rtlPort, "Expecting RTL Port");

// Locate Clock
  CarbonCfgSystemCClock* clockGen = NULL;
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgSystemCClock* clock = conn->castSystemCClock();
    if (clock && uRTLPortName == clock->getRTLPort()->getName())
    {
      clockGen = clock;
      break;
    }
  }
  INFO_ASSERT(clockGen, "Expecting to find clock gen");

  // Disconnect the Clock Gen
  mCfg->disconnect(clockGen);

  // Remove the node
  getTree()->removeItem(getItem());

  // Turn the RTL Port into an ESL Port
  ESLPortItem* newItem = ESLPortItem::makeESLPort(mCfg, rtlPort, getTree(), mESLPortName, mESLPortIndex);
  getTree()->scrollToItem(newItem);

  getTree()->setModified(true);
}


CarbonCfgRTLPort* SystemCClockGenItem::removeSystemCClockGen(PortEditorTreeItem* item, const QString& rtlPortName)
{
  CcfgHelper ccfg(item->getCcfg());
  PortEditorTreeWidget* tree = item->getTree();
  tree->removeItem(item);
  CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(rtlPortName);
  ccfg.disconnect(rtlPort);
  return rtlPort;
}

SystemCClockGenItem* SystemCClockGenItem::createSystemCClockGen(PortEditorTreeWidget* tree,
                        UndoData* undoData, CarbonCfgRTLPort* rtlPort,
                        UInt32 initialValue, double period, CarbonCfgSystemCTimeUnits periodUnits, double dutyCycle,
                        double startTime, CarbonCfgSystemCTimeUnits startTimeUnits)
{
  CcfgHelper ccfg(tree->getCcfg());

  CompWizardPortEditor* editor = tree->getEditor();

  // disconnect if needed
  ccfg.disconnect(rtlPort);

   // Create the ClockGen and tie it back to this port
  CarbonCfgSystemCClock* clockGen = new CarbonCfgSystemCClock(rtlPort);
  
  clockGen->putDutyCycle(dutyCycle);
  clockGen->putInitialValue(initialValue);
  clockGen->putPeriod(period, periodUnits);
  clockGen->putStartTime(startTime, startTimeUnits);
 

  // connect it to the RTL port
  rtlPort->connect(clockGen);

  // Create the Tree Item
  QTreeWidgetItem* rootItem = editor->getSystemCClocksRoot();
  SystemCClockGenItem* newItem = new SystemCClockGenItem(rootItem, clockGen);
  undoData->setTreeData(tree, newItem);  
  tree->sortChildren(rootItem);
  tree->expandChildren(rootItem);
  newItem->setSelected(true);
  tree->scrollToItem(newItem);
  return newItem;
}



// ClockGen
UndoData* ESLClockGenItem::deleteItem()
{
   ESLClockGenItem::Delete* undoData = new ESLClockGenItem::Delete();
 
   undoData->mCfg = getCcfg();
   undoData->setText(mClockGen->getRTLPort()->getName());
   undoData->setTreeData(treeWidget(), this);
   undoData->mRTLPortName = mClockGen->getRTLPort()->getName();

   undoData->mInitialValue = mClockGen->mInitialValue;
   undoData->mDelay = mClockGen->mDelay;
   undoData->mClockCycles = mClockGen->mFrequency.getClockCycles();
   undoData->mCompCycles = mClockGen->mFrequency.getCompCycles();
   undoData->mDutyCycle = mClockGen->mDutyCycle;

   return undoData;
}

void ESLClockGenItem::Delete::undo()
{
  qDebug() << "Undo ClockGen Delete" << text();

  UtString uESLPortName; uESLPortName << mESLPortName;
  CarbonCfgESLPort* eslPort = mCfg->findESLPort(uESLPortName.c_str());
  INFO_ASSERT(eslPort, "Expecting ESL Port");
  mCfg->disconnect(eslPort);

  QTreeWidgetItem* eslItem = getTree()->itemFromIndex(mESLPortIndex);
  INFO_ASSERT(eslItem, "Expecting ESL Port Item");
  getTree()->removeItem(eslItem);

  UtString uRTLPortName; uRTLPortName << mRTLPortName;
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  INFO_ASSERT(rtlPort, "Expecting RTL Port");

  // Create the resetgen and tie it back to this port
  CarbonCfgClockGen* clockGen = new CarbonCfgClockGen(rtlPort,
          mInitialValue, mDelay, mClockCycles, mCompCycles, mDutyCycle); 

  rtlPort->connect(clockGen);
  
  QTreeWidgetItem* clocksRoot = getTree()->getEditor()->getClocksRoot();
  ESLClockGenItem* newItem = new ESLClockGenItem(clocksRoot, clockGen);

  setTreeData(getTree(), newItem);
  clocksRoot->setExpanded(true);

  newItem->setSelected(true);
  getTree()->scrollToItem(newItem);
  getTree()->setModified(false);
}

void ESLClockGenItem::Delete::redo()
{
  qDebug() << "Delete ClockGen" << text();

  UtString uRTLPortName; uRTLPortName << mRTLPortName;

  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  INFO_ASSERT(rtlPort, "Expecting RTL Port");

// Locate Clock
  CarbonCfgClockGen* clockGen = NULL;
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgClockGen* clock = conn->castClockGen();
    if (clock && uRTLPortName == clock->getRTLPort()->getName())
    {
      clockGen = clock;
      break;
    }
  }
  INFO_ASSERT(clockGen, "Expecting to find clock gen");

  // Disconnect the Clock Gen
  mCfg->disconnect(clockGen);

  // Remove the node
  getTree()->removeItem(getItem());

  // Turn the RTL Port into an ESL Port
  ESLPortItem* newItem = ESLPortItem::makeESLPort(mCfg, rtlPort, getTree(), mESLPortName, mESLPortIndex);
  getTree()->scrollToItem(newItem);

  getTree()->setModified(true);
}

// Clock Generators

QWidget* ESLClockGenInitialValueItem::createEditor(const PortEditorDelegate* delegate,
                                                   QWidget* parent, int colIndex)
{
  if (colIndex == ESLClockGenInitialValueItem::colVALUE)
  {
    QComboBox* cbox = new QComboBox(parent);
    cbox->addItem("Low");
    cbox->addItem("High");
    if (getClockGen()->mInitialValue)
      cbox->setCurrentIndex(cbox->findText("High"));
    else
      cbox->setCurrentIndex(cbox->findText("Low"));

    CQT_CONNECT(cbox, currentIndexChanged(int), delegate, currentIndexChanged(int));

    return cbox;
  }

  return NULL;
}

void ESLClockGenInitialValueItem::setModelData(const PortEditorDelegate*, 
                                               QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                               QWidget* editor, int colIndex)
{
  ESLClockGenInitialValueItem::Columns col = ESLClockGenInitialValueItem::Columns(colIndex);

  if (col == ESLClockGenInitialValueItem::colVALUE)
  {
    QComboBox* cbox = qobject_cast<QComboBox*>(editor);
    QString textValue = cbox->currentText().toLower();
   
    quint32 oldValue = getClockGen()->mInitialValue;
    quint32 newValue;
    if (textValue == "low")
      newValue = 0;
    else
      newValue = 1;
   
    model->setData(modelIndex, cbox->currentText());

    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeESLClockGenInitialValue(this, oldValue, newValue));     
  }
}

QWidget* ESLClockGenDelayItem::createEditor(const PortEditorDelegate*,
                                            QWidget* parent, int colIndex)
{
  if (colIndex == ESLClockGenDelayItem::colVALUE)
  {
    QSpinBox* spinBox = new QSpinBox(parent);
    spinBox->setSuffix("%");
    spinBox->setRange(0,100);
    spinBox->setValue(getClockGen()->mDelay);
    return spinBox;
  }

  return NULL;
}

void ESLClockGenDelayItem::setModelData(const PortEditorDelegate*, 
                                        QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                        QWidget* editor, int colIndex)
{
  ESLClockGenDelayItem::Columns col = ESLClockGenDelayItem::Columns(colIndex);

  if (col == ESLClockGenDelayItem::colVALUE)
  {
    QSpinBox* spinBox = qobject_cast<QSpinBox*>(editor);
    quint32 oldValue = getClockGen()->mDelay;
    quint32 newValue = (quint32)(spinBox->value());
    model->setData(modelIndex, newValue);
    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeESLClockGenDelay(this, oldValue, newValue));     
  }
}

QWidget* ESLClockGenDutyCycleItem::createEditor(const PortEditorDelegate*,
                                                QWidget* parent, int colIndex)
{
  if (colIndex == ESLClockGenDutyCycleItem::colVALUE)
  {
    QSpinBox* spinBox = new QSpinBox(parent);
    spinBox->setSuffix("%");
    spinBox->setRange(0,100);
    spinBox->setValue(getClockGen()->mDutyCycle);
    return spinBox;
  }

  return NULL;
}

void ESLClockGenDutyCycleItem::setModelData(const PortEditorDelegate*, 
                                            QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                            QWidget* editor, int colIndex)
{
  ESLClockGenDutyCycleItem::Columns col = ESLClockGenDutyCycleItem::Columns(colIndex);

  if (col == ESLClockGenDutyCycleItem::colVALUE)
  {
    QSpinBox* spinBox = qobject_cast<QSpinBox*>(editor);

    quint32 oldValue = getClockGen()->mDutyCycle;
    quint32 newValue = (quint32)(spinBox->value());
   
    model->setData(modelIndex, newValue);
    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeESLClockGenDutyCycle(this, oldValue, newValue));     
  }
}

QWidget* ESLClockGenFrequencyClockCycles::createEditor(const PortEditorDelegate*,
                                                       QWidget* parent, int colIndex)
{
  ESLClockGenFrequencyClockCycles::Columns col = ESLClockGenFrequencyClockCycles::Columns(colIndex);

  if (col == ESLClockGenFrequencyClockCycles::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    QIntValidator* validator = new QIntValidator(parent);
    validator->setBottom(0);
    lineEdit->setValidator(validator);
    return lineEdit;
  }

  return NULL;
}

void ESLClockGenFrequencyClockCycles::setModelData(const PortEditorDelegate*, 
                                                   QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                                   QWidget* editor, int colIndex)
{
  ESLClockGenFrequencyClockCycles::Columns col = ESLClockGenFrequencyClockCycles::Columns(colIndex);

  if (col == ESLClockGenFrequencyClockCycles::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    quint32 oldValue = getClockGen()->mFrequency.getClockCycles();
    quint32 newValue = lineEdit->text().toUInt();
    model->setData(modelIndex, newValue);
    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeESLClockGenFrequencyClockCycles(this, oldValue, newValue));     
  }
}

QWidget* ESLClockGenFrequencyCompCycles::createEditor(const PortEditorDelegate*,
                                                      QWidget* parent, int colIndex)
{
  ESLClockGenFrequencyCompCycles::Columns col = ESLClockGenFrequencyCompCycles::Columns(colIndex);

  if (col == ESLClockGenFrequencyCompCycles::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    QIntValidator* validator = new QIntValidator(parent);
    validator->setBottom(0);
    lineEdit->setValidator(validator);
    return lineEdit;
  }

  return NULL;
}

void ESLClockGenFrequencyCompCycles::setModelData(const PortEditorDelegate*, 
                                                  QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                                  QWidget* editor, int colIndex)
{
  ESLClockGenFrequencyCompCycles::Columns col = ESLClockGenFrequencyCompCycles::Columns(colIndex);
  
  if (col == ESLClockGenFrequencyCompCycles::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    quint32 oldValue = getClockGen()->mFrequency.getCompCycles();
    quint32 newValue = lineEdit->text().toUInt();
    model->setData(modelIndex, newValue);
    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeESLClockGenFrequencyCompCycles(this, oldValue, newValue));     
  }
}


CarbonCfgRTLPort* ESLClockGenItem::removeClockGen(PortEditorTreeItem* item, const QString& rtlPortName)
{
  CcfgHelper ccfg(item->getCcfg());
  PortEditorTreeWidget* tree = item->getTree();
  tree->removeItem(item);
  CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(rtlPortName);
  ccfg.disconnect(rtlPort);
  return rtlPort;
}

ESLClockGenItem* ESLClockGenItem::createClockGen(PortEditorTreeWidget* tree,
                        UndoData* undoData, CarbonCfgRTLPort* rtlPort,
                        UInt32 initialValue, UInt32 delay, UInt32 dutyCycle,
                        UInt32 clockCycles, UInt32 compCycles)
{
  CcfgHelper ccfg(tree->getCcfg());

  CompWizardPortEditor* editor = tree->getEditor();

  // disconnect if needed
  ccfg.disconnect(rtlPort);

  // Create the ClockGen and tie it back to this port
  CarbonCfgClockGen* clockGen = new CarbonCfgClockGen(rtlPort, 
                  initialValue, delay, clockCycles, compCycles, dutyCycle);

  // connect it to the RTL port
  rtlPort->connect(clockGen);

  // Create the Tree Item
  QTreeWidgetItem* rootItem = editor->getClocksRoot();
  ESLClockGenItem* newItem = new ESLClockGenItem(rootItem, clockGen);
  undoData->setTreeData(tree, newItem);  
  tree->sortChildren(rootItem);
  tree->expandChildren(rootItem);
  newItem->setSelected(true);
  tree->scrollToItem(newItem);
  return newItem;
}

// SystemC

QWidget* SystemCClockGenInitialValueItem::createEditor(const PortEditorDelegate* delegate,
                                                   QWidget* parent, int colIndex)
{
  if (colIndex == SystemCClockGenInitialValueItem::colVALUE)
  {
    QComboBox* cbox = new QComboBox(parent);
    cbox->addItem("Low");
    cbox->addItem("High");
    if (getClockGen()->getInitialValue())
      cbox->setCurrentIndex(cbox->findText("High"));
    else
      cbox->setCurrentIndex(cbox->findText("Low"));

    CQT_CONNECT(cbox, currentIndexChanged(int), delegate, currentIndexChanged(int));

    return cbox;
  }

  return NULL;
}

void SystemCClockGenInitialValueItem::setModelData(const PortEditorDelegate*, 
                                               QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                               QWidget* editor, int colIndex)
{
  SystemCClockGenInitialValueItem::Columns col = SystemCClockGenInitialValueItem::Columns(colIndex);

  if (col == SystemCClockGenInitialValueItem::colVALUE)
  {
    QComboBox* cbox = qobject_cast<QComboBox*>(editor);
    QString textValue = cbox->currentText().toLower();
   
    quint32 oldValue = getClockGen()->getInitialValue();
    quint32 newValue;
    if (textValue == "low")
      newValue = 0;
    else
      newValue = 1;
   
    model->setData(modelIndex, cbox->currentText());

    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeSystemCClockGenInitialValue(this, oldValue, newValue));     
  }
}


// Duty Cycle
QWidget* SystemCClockGenDutyCycleItem::createEditor(const PortEditorDelegate*,
                                                QWidget* parent, int colIndex)
{
  if (colIndex == SystemCClockGenDutyCycleItem::colVALUE)
  {
    QLineEdit* le = new QLineEdit(parent);  
    QDoubleValidator* dv = new QDoubleValidator(parent);
    QString v = QString("%1").arg(getClockGen()->getDutyCycle());
    le->setValidator(dv);
    le->setText(v);   
    return le;
  }

  return NULL;
}

void SystemCClockGenDutyCycleItem::setModelData(const PortEditorDelegate*, 
                                            QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                            QWidget* editor, int colIndex)
{
  SystemCClockGenDutyCycleItem::Columns col = SystemCClockGenDutyCycleItem::Columns(colIndex);

  if (col == SystemCClockGenDutyCycleItem::colVALUE)
  {
    QLineEdit* le = qobject_cast<QLineEdit*>(editor);

    double oldValue = getClockGen()->getDutyCycle();
    double newValue = le->text().toDouble();
   
    model->setData(modelIndex, newValue);
    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeSystemCClockGenDutyCycle(this, oldValue, newValue));     
  }
}

// Period

QWidget* SystemCClockGenPeriodItem::createEditor(const PortEditorDelegate*,
                                                QWidget* parent, int colIndex)
{
  if (colIndex == SystemCClockGenPeriodItem::colVALUE)
  {
    QLineEdit* le = new QLineEdit(parent);  
    QDoubleValidator* dv = new QDoubleValidator(parent);
    QString v = QString("%1").arg(getClockGen()->getPeriod());
    le->setValidator(dv);
    le->setText(v);   
    return le;
  }

  return NULL;
}

void SystemCClockGenPeriodItem::setModelData(const PortEditorDelegate*, 
                                            QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                            QWidget* editor, int colIndex)
{
  SystemCClockGenPeriodItem::Columns col = SystemCClockGenPeriodItem::Columns(colIndex);

  if (col == SystemCClockGenPeriodItem::colVALUE)
  {
    QLineEdit* le = qobject_cast<QLineEdit*>(editor);

    double oldValue = getClockGen()->getPeriod();
    double newValue = le->text().toDouble();
   
    model->setData(modelIndex, newValue);
    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeSystemCClockGenPeriod(this, oldValue, newValue));     
  }
}

// StartTime

QWidget* SystemCClockGenStartTimeItem::createEditor(const PortEditorDelegate*,
                                                QWidget* parent, int colIndex)
{
  if (colIndex == SystemCClockGenStartTimeItem::colVALUE)
  {
    QLineEdit* le = new QLineEdit(parent);  
    QDoubleValidator* dv = new QDoubleValidator(parent);
    QString v = QString("%1").arg(getClockGen()->getStartTime());
    le->setValidator(dv);
    le->setText(v);   
    return le;
  }

  return NULL;
}

void SystemCClockGenStartTimeItem::setModelData(const PortEditorDelegate*, 
                                            QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                            QWidget* editor, int colIndex)
{
  SystemCClockGenStartTimeItem::Columns col = SystemCClockGenStartTimeItem::Columns(colIndex);

  if (col == SystemCClockGenStartTimeItem::colVALUE)
  {
     QLineEdit* le = qobject_cast<QLineEdit*>(editor);

    double oldValue = getClockGen()->getStartTime();
    double newValue = le->text().toDouble();
   
    model->setData(modelIndex, newValue);
    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeSystemCClockGenStartTime(this, oldValue, newValue));     
  }
}



// Period Units
QWidget* SystemCClockGenPeriodUnitsItem::createEditor(const PortEditorDelegate* delegate,
                                                   QWidget* parent, int colIndex)
{
  if (colIndex == SystemCClockGenPeriodUnitsItem::colVALUE)
  {
    QComboBox* cbox = new QComboBox(parent);

    int i = 0;
    for (const char** p = gCarbonCfgSystemCTimeUnits; *p != NULL; ++p) 
    {
      QString val = gCarbonCfgSystemCTimeUnits[i];
      cbox->addItem(val);

      int cv = (int)getClockGen()->getStartTimeUnits();
      if (cv == i)
        cbox->setCurrentIndex(cbox->findText(val));
      i++;
    }

 
    CQT_CONNECT(cbox, currentIndexChanged(int), delegate, currentIndexChanged(int));

    return cbox;
  }

  return NULL;
}

void SystemCClockGenPeriodUnitsItem::setModelData(const PortEditorDelegate*, 
                                               QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                               QWidget* editor, int colIndex)
{
  SystemCClockGenPeriodUnitsItem::Columns col = SystemCClockGenPeriodUnitsItem::Columns(colIndex);

  if (col == SystemCClockGenPeriodUnitsItem::colVALUE)
  {
    QComboBox* cbox = qobject_cast<QComboBox*>(editor);
   
    QString textValue = cbox->currentText().toLower();
   
    CarbonCfgSystemCTimeUnits oldValue = getClockGen()->getPeriodUnits();
    CarbonCfgSystemCTimeUnits newValue = (CarbonCfgSystemCTimeUnits)cbox->currentIndex();   
   
    model->setData(modelIndex, cbox->currentText());

    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeSystemCClockGenPeriodTimeUnits(this, oldValue, newValue));     
  }
}

// Start Time Units
QWidget* SystemCClockGenStartTimeUnitsItem::createEditor(const PortEditorDelegate* delegate,
                                                   QWidget* parent, int colIndex)
{
  if (colIndex == SystemCClockGenStartTimeUnitsItem::colVALUE)
  {
    QComboBox* cbox = new QComboBox(parent);

    int i = 0;
    for (const char** p = gCarbonCfgSystemCTimeUnits; *p != NULL; ++p) 
    {
      QString val = gCarbonCfgSystemCTimeUnits[i];
      cbox->addItem(val);

      int cv = (int)getClockGen()->getStartTimeUnits();
      if (cv == i)
        cbox->setCurrentIndex(cbox->findText(val));
      i++;
    }

 
    CQT_CONNECT(cbox, currentIndexChanged(int), delegate, currentIndexChanged(int));

    return cbox;
  }

  return NULL;
}

void SystemCClockGenStartTimeUnitsItem::setModelData(const PortEditorDelegate*, 
                                               QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                               QWidget* editor, int colIndex)
{
  SystemCClockGenStartTimeUnitsItem::Columns col = SystemCClockGenStartTimeUnitsItem::Columns(colIndex);

  if (col == SystemCClockGenStartTimeUnitsItem::colVALUE)
  {
    QComboBox* cbox = qobject_cast<QComboBox*>(editor);
   
    QString textValue = cbox->currentText().toLower();
   
    CarbonCfgSystemCTimeUnits oldValue = getClockGen()->getStartTimeUnits();
    CarbonCfgSystemCTimeUnits newValue = (CarbonCfgSystemCTimeUnits)cbox->currentIndex();   
   
    model->setData(modelIndex, cbox->currentText());

    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeSystemCClockGenStartTimeUnits(this, oldValue, newValue));     
  }
}
