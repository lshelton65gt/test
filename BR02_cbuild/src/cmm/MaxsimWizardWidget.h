#ifndef MAXSIMWIZARDWIDGET_H
#define MAXSIMWIZARDWIDGET_H
#include "util/CarbonPlatform.h"

#include <QWidget>
#include "util/UtString.h"
#include "Mdi.h"
#include "MdiTextEditor.h"
#include "PortView.h"
#include "RegisterTableWidget.h"
#include "DialogAdvancedBinding.h"
#include "CowareApplicationContext.h"
#include "RemoteConsole.h"
#include "CarbonConsole.h"

class CodeGenerator;
class CarbonMakerContext;
class MaxsimComponent;

class CompWizardPortEditor;

class QTextEdit;
class CarbonClkPanel;
class RegTabWidget;
class CarbonBuild;
class CarbonHBrowser;
class CompWizardMemEditor;
class CarbonProfileEdit;
class TextEdit;
class CarbonGenProp;

#include "MaxSimApplicationContext.h"

class MaxsimChangeOutputDirectory
{
public:
  MaxsimChangeOutputDirectory(MaxsimComponent* maxsim);
  ~MaxsimChangeOutputDirectory();

private:
  UtString mCurrDir;
};

class MaxsimWizardWidget;

class MakerMaxsimApplicationContext : public MaxSimApplicationContext
{
public:
  MakerMaxsimApplicationContext(MaxsimWizardWidget* wiz, CarbonDB *db, CarbonCfgID cfg, CQtContext *cqt) : MaxSimApplicationContext(db,cfg,cqt)
  {
    mWizard = wiz;
  }
  virtual void setDocumentModified(bool value);

private:
  MaxsimWizardWidget* mWizard;
};



class MaxsimWizardWidget : public QWidget, public MDIWidget, public CarbonCfgScriptExtension
{
  Q_OBJECT

public:
  enum ParseResult {eIODB, eFullDB, eCcfg, eNoMatch};
  MaxsimWizardWidget(CarbonMakerContext* ctx=0, MDIDocumentTemplate* doct=0, QWidget *parent = 0);
  ~MaxsimWizardWidget();
  bool loadFile(const char* filename);

  virtual void registerTypes(QScriptEngine* engine);
  virtual void saveDocument();
  virtual const char* userFriendlyName();
  bool check(CarbonConsole* console);

private:
  void buildLayout();
  ParseResult parseFileName(const char* filename,UtString* baseName);
  bool loadDatabase(const char* iodbName, bool initPorts);
  bool loadConfig(const QString &fileName, bool* modifiedFlag);
  bool loadDatabase(const QString& iodbFileName, bool initPorts);
  void closeEvent(QCloseEvent *event);
  bool maybeSave();
  void updateTopModuleName();
  void readXtorDefs();
  bool loadFile(const QString& filename);
  QString getDbFilename();

private:
  static eCarbonMsgCBStatus sMsgCallback(CarbonClientData, CarbonMsgSeverity,
                                         int number, const char* text,
                                         unsigned int len);

  CarbonMsgCBDataID* mMsgCallback;
  RegTabWidget* mRegTabWidget;
  CarbonBuild* mBuild;
  UtString mCurFile;
  UtString mMakefile;
  UtString mProjName;
  
  TextEdit* mErrorTextEditor;

  CQtContext* mCQt;
  AtomicCache* mAtomicCache;
  SourceLocatorFactory mLocatorFactory;
  CompWizardMemEditor* mMemEdit;
  CarbonGenProp* mGenProp;
  //CarbonPortEdit* mPortEdit;
  CarbonProfileEdit* mProfileEdit;
  CompWizardPortEditor* mPortEditor;
  QAction *mBrowseAct;
  CarbonMakerContext* mCtx;
  CarbonDB* mDB;
  CarbonCfgID mCfg;
  UInt32 mCurrentTabIndex;
  UInt32 mNumBrowsers;
 
  UtString m_ccfgName;
  UtString m_dbName;
  MakerMaxsimApplicationContext* mContext;

private slots:
  void newFile();
  void open();
  bool saveAs();
  void about();
  void help();
  void documentWasModified();
  void textEditDestroyed(QObject*);
  void compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void outputFilenameChanged(const CarbonProperty*, const char*);
  void projectLoaded(const char* fileName, CarbonProject*);
  void xtorDefFileChanged();

signals:
  void portsTabSelected(bool);

public slots:
  bool save();
  void tabSwitch(int);
  void createBrowser();
  void actionDelete();
  void actionNewParameter();

};

#endif
