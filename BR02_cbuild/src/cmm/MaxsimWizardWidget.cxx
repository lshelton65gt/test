//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "gui/CDragDropTreeWidget.h"

#include "MaxsimWizardWidget.h"
#include "MaxSimApplicationContext.h"
#include "Mdi.h"
#include "MdiTextEditor.h"

#include "CodeGenerator.h"
#include "CarbonMakerContext.h"
#include "WorkspaceModelMaker.h"

#include "MaxsimComponent.h"
#include "CarbonProjectWidget.h"
#include "RegTabWidget.h"
#include "ProfileEdit.h"
#include "PortEdit.h"
#include "ProfileEdit.h"
#include "textedit.h"
#include "CarbonOptions.h"

#include "iodb/CGraph.h"

#include "util/CarbonVersion.h"
#include "util/UtString.h"
#include "util/UtHashMap.h"
#include "util/AtomicCache.h"
#include "util/UtIOStream.h"
#include "util/UtIStream.h"
#include "util/ArgProc.h"
#include "util/AtomicCache.h"
#include "util/OSWrapper.h"
#include "util/RandomValGen.h"
#include "shell/CarbonAbstractRegister.h"

#include "cfg/CarbonCfg.h"
#include "cfg/carbon_cfg.h"
#include "cfg/carbon_cfg_misc.h"

#include "shell/carbon_misc.h"
#include "shell/carbon_capi.h"

#include "cmm.h"
#include "ScrCcfg.h"
#include "ScrProject.h"
#include "ScrApplication.h"

#define APP_NAME "Carbon Component Wizard"

#include "CompWizardPortEditor.h"
#include "CompWizardMemEditor.h"

#define tabPORTS 0
#define tabREGISTERS 1
#define tabMEMORIES 2
#define tabPROFILE 3

eCarbonMsgCBStatus MaxsimWizardWidget::sMsgCallback(CarbonClientData clientData,
                                             CarbonMsgSeverity severity,
                                             int number, const char* text,
                                             unsigned int)
{
  MaxsimWizardWidget* mw = (MaxsimWizardWidget*) clientData;
  const char* severityStr = "";
  switch (severity) {
  case eCarbonMsgStatus:   severityStr = "Status"; break;
  case eCarbonMsgNote:     severityStr = "Note"; break;
  case eCarbonMsgWarning:  severityStr = "Warning"; break;
  case eCarbonMsgError:    severityStr = "Error"; break;
  case eCarbonMsgFatal:    severityStr = "Fatal"; break;
  case eCarbonMsgSuppress: severityStr = "Suppress"; break;
  case eCarbonMsgAlert:    severityStr = "Alert"; break;
  }

  UtString buf;
  buf << severityStr << " " << number << ": " << text;

  mw->mCQt->warning(mw, buf.c_str());
  return eCarbonMsgContinue;
}

void MaxsimWizardWidget::xtorDefFileChanged()
{
  // Since the definition file changed,
  // we need to re-read the transactor definitions.
  readXtorDefs();

  // Add them to the GUI
  //mPortEdit->addTransactorsToComboBox();
}

void MaxsimWizardWidget::tabSwitch(int index)
{
  mCurrentTabIndex = index;

  emit portsTabSelected(index == tabPORTS);

  if (mCurrentTabIndex != tabPORTS)
  {
    QDockWidget* dw1 = mCtx->getWorkspaceModelMaker()->getRemoteConsoleDockWindow();
    dw1->setVisible(false);
    QDockWidget* dw2 = mCtx->getWorkspaceModelMaker()->getErrorInfoDockWindow();
    dw2->setVisible(false);
    QDockWidget* dw3 =  mCtx->getCarbonProjectWidget()->getHierarchyDockWidget();
    if (dw3)
      dw3->setVisible(true);
    if (mCurrentTabIndex == tabREGISTERS) {
      mRegTabWidget->populateBaseAddressComboBox();
    }
  }
  else
  {
    QDockWidget* dw1 = mCtx->getWorkspaceModelMaker()->getRemoteConsoleDockWindow();
    dw1->setVisible(false);
    QDockWidget* dw2 = mCtx->getWorkspaceModelMaker()->getErrorInfoDockWindow();
    dw2->setVisible(false);
    QDockWidget* dw3 = mCtx->getCarbonProjectWidget()->getHierarchyDockWidget();
    if (dw3)
      dw3->setVisible(false);
  }
}

void MaxsimWizardWidget::actionNewParameter()
{
  qDebug() << "new param";
  //mPortEdit->newParameter();
}

void MaxsimWizardWidget::actionDelete()
{
  setWindowModified(true);
  switch (mCurrentTabIndex) {
  case 0: // port setup
    mPortEditor->deleteCurrent();
    break;
  case 1: // registers
    mRegTabWidget->deleteRegister();
    break;
  case 2: // memories
    mMemEdit->deleteCurrent();
    break;
  case 3: // profile
    mProfileEdit->deleteCurrent();
    break;
  default:
    break;
  }
}

bool MaxsimWizardWidget::maybeSave()
{
  if (isWindowModified())
  {
    QMessageBox::StandardButton ret;
    ret = QMessageBox::warning(this, MODELSTUDIO_TITLE,
      tr("'%1' has been modified.\n"
      "Do you want to save your changes?")
      .arg(userFriendlyName()),
      QMessageBox::Save | QMessageBox::Discard
      | QMessageBox::Cancel);
    if (ret == QMessageBox::Save)
    {
      saveDocument();
      return true;
    }
    else if (ret == QMessageBox::Discard && isWidgetModified())
    {
      setWindowModified(false);
      setWidgetModified(false);
    }
    else if (ret == QMessageBox::Cancel)
      return false;
  }
  return true;
}

void MaxsimWizardWidget::closeEvent(QCloseEvent *event)
{
 if (maybeSave())
  {
    mPortEditor->closeEvent(event);
    mMemEdit->closeEvent(event);

    qDebug() << "Closing SOCDesigner Wizard";
    QVariant qv = this->property("CarbonTabWidget");
    QVariant qvPlaceholder = this->property("CarbonTabPlaceholder");

    if (qv.isValid() && qvPlaceholder.isValid())
    {
      QTabWidget* tw = (QTabWidget*)qv.value<void*>();
      QWidget* w = (QWidget*)qvPlaceholder.value<void*>();

      int tabIndex = tw->indexOf(w);
      if (tabIndex != -1)
        tw->removeTab(tabIndex);
    }

    if (mCfg)
      delete mCfg;

    event->accept();
  }
  else
  {
    event->ignore();
  }
}

void MakerMaxsimApplicationContext::setDocumentModified(bool value)
{
  mWizard->setWindowModified(value);
}

bool MaxsimWizardWidget::check(CarbonConsole* console)
{
  return mMemEdit->check(console);
}

MaxsimWizardWidget::MaxsimWizardWidget(CarbonMakerContext* ctx, MDIDocumentTemplate* t, QWidget *parent)
: QWidget(parent)
{
  mCtx = ctx;
  setAttribute(Qt::WA_DeleteOnClose);

  initializeMDI(this, t);

  mCQt = ctx->getQtContext();

  mCQt->putMainWindow(this);

  mMsgCallback = carbonAddMsgCB(NULL, sMsgCallback, this);


  mBuild = NULL;
  mNumBrowsers = 0;
  mAtomicCache = new AtomicCache;
  mDB = NULL;
 // mCGraph = NULL;
  mCurrentTabIndex = 0;
  mBrowseAct = NULL;

  mErrorTextEditor = NULL;
  
  // Get Maxsim Component
  MaxsimComponent* maxsim = mCtx->getCarbonProjectWidget()->getActiveMaxsimComponent();

  // Check for commandline xtorDef
  if (maxsim && mCQt->getXtorDefFile() != NULL) {
    maxsim->setXtorDefFile(mCQt->getXtorDefFile());
  }

  // Build the leaf widgets that hold the content for the configuration
  mCfg = carbonCfgCreate();
  mCfg->addScriptExtension(this);

  mCfg->putLegacyMemories(0);
  
  // Read Transactor Library
  if (carbonCfgReadXtorLib(mCfg, eCarbonXtorsMaxsim) == eCarbonCfgFailure) {
    mCQt->warning(this, carbonCfgGetErrmsg(mCfg));
  }
  else qDebug() << carbonCfgGetErrmsg(mCfg);

  
  // Read Transactor Definitions
  readXtorDefs();

  mContext = new MakerMaxsimApplicationContext(this, mDB, mCfg, mCQt);

  mPortEditor = new CompWizardPortEditor(this);
  CQT_CONNECT(mPortEditor, widgetModified(bool), this, setWindowModified(bool));

  // Registers window
  mRegTabWidget = new RegTabWidget(ctx->getMainWindow(), mCQt, mCfg, RegTabWidget::ModelMaker, mCtx->getCarbonProjectWidget());

  mMemEdit = new CompWizardMemEditor(this);
  mMemEdit->setEditorMode(CompWizardMemEditor::eModeSocDesigner);
  
  CQT_CONNECT(mMemEdit, widgetModified(bool), this, setWindowModified(bool));

  // Listen to the PortEditor for renames
  CQT_CONNECT(mPortEditor, renameXtorInstance(const QString&, const QString&), mMemEdit, renameXtorInstance(const QString&, const QString&));

  mProfileEdit = new CarbonProfileEdit(mCQt, mCfg, mAtomicCache, NULL, ctx->getMainWindow()->statusBar());
  
  buildLayout();

  mRegTabWidget->getContext()->putModelStudio(this);

  CQT_CONNECT(mCtx->getCarbonProjectWidget()->getConsole(), compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType));
  CQT_CONNECT(mCtx->getCarbonProjectWidget()->getConsole(), compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType));
 
  CQT_CONNECT(mCtx->getCarbonProjectWidget()->project(), projectLoaded(const char*, CarbonProject*), this, projectLoaded(const char*, CarbonProject*));

  CarbonOptions* options = mCtx->getCarbonProjectWidget()->project()->getToolOptions("VSPCompiler");
  options->registerPropertyChanged("-o", "outputFilenameChanged", this);

  setWindowTitle("SOC Designer Component[*]");

  if (maxsim)
  {
    CQT_CONNECT(maxsim, xtorDefFileChanged(), this, xtorDefFileChanged());
  }

  mMemEdit->createToolbar(this);
  mPortEditor->createToolbar(this);

  tabSwitch(tabPORTS);
}

void MaxsimWizardWidget::projectLoaded(const char*, CarbonProject*)
{
  qDebug() << "ProjectLoaded";
}

void MaxsimWizardWidget::readXtorDefs()
{
  MaxsimComponent* maxsim = mCtx->getCarbonProjectWidget()->getActiveMaxsimComponent();
  if(maxsim) {
    // read any user transactor defs
    if (maxsim->xtorDefFile())
      if (carbonCfgReadXtorDefinitions(mCfg, maxsim->xtorDefFile()) == eCarbonCfgFailure) {
        mCQt->warning(this, carbonCfgGetErrmsg(mCfg));
      }
  }
}

void MaxsimWizardWidget::outputFilenameChanged(const CarbonProperty*, const char*)
{
  QString iodbName = getDbFilename();

  MaxsimComponent* maxsim = mCtx->getCarbonProjectWidget()->getActiveMaxsimComponent();

  UtString relIODBName;
  relIODBName << mCtx->getCarbonProjectWidget()->makeRelativePath(maxsim->getOutputDirectory(), iodbName);

  if (mCfg)
    carbonCfgPutIODBFile(mCfg, relIODBName.c_str());

  m_dbName.clear();
  m_dbName << iodbName;

  setWindowModified(true);
}

// We could have finished the "compilation" process, which did NOT result in a new
// database, so just re-enable.
void MaxsimWizardWidget::compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  setEnabled(true);
}

void MaxsimWizardWidget::compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
 setEnabled(false);  
 saveDocument();
}

MaxsimWizardWidget::ParseResult MaxsimWizardWidget::parseFileName(const char* filename,
                                                  UtString* baseName)
{
  *baseName = filename;
  UInt32 sz = strlen(filename);

  ParseResult parseResult = eNoMatch;
  baseName->clear();

  if ((sz > 6) && (strcmp(filename + sz - 6, ".io.db") == 0)) {
    parseResult = eIODB;
    baseName->append(filename, sz - 6);
  }
  else if ((sz > 10) && (strcmp(filename + sz - 10, ".symtab.db") == 0)) {
    parseResult = eFullDB;
    baseName->append(filename, sz - 10);
  }
  else if ((sz > 5) && (strcmp(filename + sz - 5, ".ccfg") == 0)) {
    parseResult = eCcfg;
    baseName->append(filename, sz - 5);
  }
  return parseResult;
} 

bool MaxsimWizardWidget::loadFile(const QString& filename)
{
  UtString fn;
  fn << filename;
  return loadFile(fn.c_str());
}

bool MaxsimWizardWidget::loadFile(const char* filename)
{
  MaxsimComponent* maxsim = mCtx->getCarbonProjectWidget()->getActiveMaxsimComponent();
  INFO_ASSERT(maxsim, "Missing Component");
  bool modifiedFlag = false;

  MaxsimChangeOutputDirectory temp(maxsim);

  UtString basename;
  ParseResult parseResult = parseFileName(filename, &basename);

  QFileInfo fi(filename);

  bool ret = false;
  if ((parseResult == eCcfg && !fi.exists()) || (parseResult == eFullDB) || (parseResult == eIODB)) {
    m_dbName.clear();
    m_dbName << filename;
    if (fi.exists())
      ret = loadDatabase(filename, true);
    else
      ret = loadDatabase(getDbFilename(), true);
    modifiedFlag = true;


  }
  else if (fi.exists() && parseResult == eCcfg)
  {
    ret = loadConfig(filename, &modifiedFlag);
  }

  updateTopModuleName();

  
  setWidgetModified(modifiedFlag);
  
  CarbonProject* project = mCtx->getCarbonProjectWidget()->project();
  mProjName << project->shortName();

  return ret;
}

QString MaxsimWizardWidget::getDbFilename()
{
  QString symDB = mCtx->getCarbonProjectWidget()->project()->getDesignFilePath(".symtab.db");
  QString ioDB = mCtx->getCarbonProjectWidget()->project()->getDesignFilePath(".io.db");

  QFileInfo fi1(symDB);
  QFileInfo fi2(ioDB);

  if (fi1.exists())
	return symDB;
  else if (fi2.exists())
	return ioDB;
  else
	return symDB;
}


void MaxsimWizardWidget::buildLayout()
{
  CTabWidget* tab = mCQt->tabWidget("main_tab", 0, this);

  tab->setTabShape(QTabWidget::Triangular);
  tab->setTabPosition(QTabWidget::South);

#if pfWINDOWS
  QFont f("Helvetica", 10, QFont::Normal);
#else
  QFont f("Helvetica", 14, QFont::Normal);
#endif
  tab->getTabBar()->setFont(f);

  tab->addTab(mPortEditor, "Ports");
  tab->addTab(mRegTabWidget, "Registers");
  tab->addTab(mMemEdit, "Memories");
  tab->addTab(mProfileEdit->buildLayout(), "Profile");

  CQT_CONNECT(tab, currentChanged(int), this, tabSwitch(int));

  QLayout* layout = new QGridLayout(this);

  layout->setSpacing(1);
  layout->setMargin(0);

  //layout->addItem(new QSpacerItem(0, 1));
  layout->addWidget(tab);
} 

const char* MaxsimWizardWidget::userFriendlyName()
{
  MaxsimComponent* comp = mCtx->getCarbonProjectWidget()->getActiveMaxsimComponent();
  INFO_ASSERT(comp, "Missing Component");
  INFO_ASSERT(m_dbName.length() > 0, "NO DB Name");

  QFileInfo fi(m_dbName.c_str());
  UtString baseName;
  baseName << fi.baseName() << ".ccfg";
  UtString ccfgName;
  m_ccfgName.clear();
  OSConstructFilePath(&ccfgName, comp->getOutputDirectory(), baseName.c_str());
  m_ccfgName << ccfgName;
  return m_ccfgName.c_str();
}

void MaxsimWizardWidget::saveDocument()
{
  MaxsimComponent* maxsim = mCtx->getCarbonProjectWidget()->getActiveMaxsimComponent();
  INFO_ASSERT(maxsim, "Missing Component");
   
  QFileInfo dbfi(m_dbName.c_str());
  UtString baseName;
  baseName << dbfi.baseName() << ".ccfg";
  
  UtString ccfgName;
  OSConstructFilePath(&ccfgName, maxsim->getOutputDirectory(), baseName.c_str());
  m_ccfgName = ccfgName;

  mPortEditor->save();
  mMemEdit->save();

  QFileInfo fi(m_ccfgName.c_str());

  if (isWindowModified() || !fi.exists())
  {
    updateTopModuleName();

    carbonCfgPutCompName(mCfg, maxsim->getOptions()->getValue("Component Name")->getValue());
    carbonCfgPutCxxFlags(mCfg, maxsim->getOptions()->getValue("C++ Compile Flags")->getValue());
    carbonCfgPutLinkFlags(mCfg, maxsim->getOptions()->getValue("Linker Flags")->getValue());
  
    carbonCfgPutSourceFiles(mCfg, maxsim->getOptions()->getValue("Source Files")->getValue());
    carbonCfgPutIncludeFiles(mCfg, maxsim->getOptions()->getValue("Include Files")->getValue());
    carbonCfgPutUseStaticScheduling(mCfg, maxsim->getOptions()->getValue("Use Static Scheduling")->getBoolValue());

    const char* waveforms = maxsim->getOptions()->getValue("Waveforms")->getValue();
    if (0 == strcmp(waveforms, "FSDB"))
      carbonCfgPutWaveType(mCfg, eCarbonCfgFSDB);
    else if (0 == strcmp(waveforms, "VCD"))
      carbonCfgPutWaveType(mCfg, eCarbonCfgVCD);
    else
      carbonCfgPutWaveType(mCfg, eCarbonCfgNone);


    carbonCfgPutWaveFilename(mCfg, maxsim->getOptions()->getValue("Waveform File")->getValue());

    QFileInfo fi(m_dbName.c_str());
    MaxsimChangeOutputDirectory temp(maxsim);

    UtString relLibName;
    const char* outputFilename = mCtx->getCarbonProjectWidget()->project()->getToolOptions("VSPCompiler")->getValue("-o")->getValue();
    QFileInfo ofi(outputFilename);
    UtString libExt;
    libExt << "." << ofi.completeSuffix();

    const char* libFilePath = mCtx->getCarbonProjectWidget()->project()->getDesignFilePath(libExt.c_str());
    relLibName << CarbonProjectWidget::makeRelativePath(maxsim->getOutputDirectory(), libFilePath);
    carbonCfgPutLibName(mCfg, relLibName.c_str());

    UtString relIODBName;
    if (!fi.isRelative())
      relIODBName << mCtx->getCarbonProjectWidget()->makeRelativePath(maxsim->getOutputDirectory(), m_dbName.c_str());
    else
      relIODBName << m_dbName;

    maxsim->getOptions()->putValue("IODB File", relIODBName.c_str());
    maxsim->getOptions()->putValue("Output Directory", maxsim->getOutputDirectory());

    carbonCfgPutIODBFile(mCfg, relIODBName.c_str());

    if (carbonCfgWrite(mCfg, m_ccfgName.c_str()) == eCarbonCfgFailure) {
      mCQt->warning(this, tr("Cannot write file %1:\n%2.")
                    .arg(m_ccfgName.c_str())
                    .arg(carbonCfgGetErrmsg(mCfg)));
    }
    else
      mCtx->getMainWindow()->statusBar()->showMessage(tr("File saved"), 2000);
    

    setWindowModified(false);
    setWidgetModified(false);

    qDebug() << "Saved Maxsim";

    qDebug() << "Updating Maxsim Tree Icons";
    maxsim->updateTreeIcons();
  }
}


void MaxsimWizardWidget::updateTopModuleName()
{
  MaxsimComponent* maxsim = mCtx->getCarbonProjectWidget()->getActiveMaxsimComponent();
  INFO_ASSERT(maxsim, "Missing Component");

  const char* compName = maxsim->getOptions()->getValue("Component Name")->getValue();
  if (compName && strlen(compName) > 0)
    mCfg->putCompName(compName);
  else
    maxsim->getOptions()->putValue("Component Name", mCfg->getCompName());
}

MaxsimWizardWidget::~MaxsimWizardWidget()
{
  CarbonProject *carbonProject = mCtx->getCarbonProjectWidget()->project();
  if (carbonProject != NULL) {
	CarbonOptions* options = carbonProject->getToolOptions("VSPCompiler");
	options->unregisterPropertyChanged("-o", this);
  }
}

void MaxsimWizardWidget::newFile()
{
}
void MaxsimWizardWidget::open()
{
}
bool MaxsimWizardWidget::saveAs()
{
  return true;
}
void MaxsimWizardWidget::about()
{
}
void MaxsimWizardWidget::help()
{
}
void MaxsimWizardWidget::documentWasModified()
{
}
void MaxsimWizardWidget::textEditDestroyed(QObject*)
{
}

bool MaxsimWizardWidget::save()
{
  return true;
}

void MaxsimWizardWidget::createBrowser()
{
}

bool MaxsimWizardWidget::loadDatabase(const QString& iodbFileName, bool initPorts)
{
  UtString iodbName;
  iodbName << iodbFileName;
  return loadDatabase(iodbName.c_str(), initPorts);
} 
bool MaxsimWizardWidget::loadDatabase(const char* iodbName, bool initPorts)
{
  QFileInfo fi(iodbName);

  if (!fi.exists())
    return false;

  QApplication::setOverrideCursor(Qt::WaitCursor);

  m_dbName = iodbName;

  // clear old DB
  mDB = 0;
  CarbonDB *oldDB = mCfg->getDB();
  if (oldDB) {
    mCfg->putDB(0);
    carbonDBFree(oldDB);
  }

  // load new DB
  CarbonDB* newDB = carbonDBOpenFile(iodbName);

  if (newDB == NULL) {
    QApplication::restoreOverrideCursor();
    return false;
  }
  mDB = newDB;
  mCfg->putDB(mDB);  // Ownership transferred to CarbonCfg

  if (mBrowseAct)
    mBrowseAct->setEnabled(true);
  
  // Only initialize data from the database if we are starting
  // from one.  If we came from a .ccfg file then the ports are already
  // initialized, and we should probably validate that they are
  // consistent

  UtString baseFile, errmsg;
  parseFileName(iodbName, &baseFile);

  errmsg.clear();

  MaxsimComponent* maxsim = mCtx->getCarbonProjectWidget()->getActiveMaxsimComponent();

  if (initPorts)
  {
    UtString libFile(baseFile);

    if (maxsim->isWindowsTarget(NULL))
      libFile += ".lib";
    else
      libFile += ".a";

    carbonCfgPutLibName(mCfg, libFile.c_str());
    setWindowModified(true);

    // Create the initial .ccfg file
    maxsim->createDefaultComponent();

    // Read the Xtor Libs
    mCfg->readXtorLib(eCarbonXtorsMaxsim);
    const char* xtorDefs = maxsim->xtorDefFile();
    if (xtorDefs)
      carbonCfgReadXtorDefinitions(mCfg, xtorDefs);
    QString ccfgFilePath = maxsim->ccfgName();
    
    qDebug() << carbonCfgGetErrmsg(mCfg);
    qDebug() << "Loading from" << ccfgFilePath;
    
    QFileInfo ccfgFi(ccfgFilePath);
    INFO_ASSERT(ccfgFi.exists(), "Expecting ccfg file");

    TempChangeDirectory cd(ccfgFi.absolutePath());
    UtString uCcfgFilePath;
    uCcfgFilePath << ccfgFilePath;

    CarbonCfgStatus cfgStatus = mCfg->read(uCcfgFilePath.c_str());
    if (cfgStatus != eCarbonCfgFailure)
    {
      qDebug() << "Read file" << ccfgFilePath;
    }
    else
    {
      qDebug() << "Error reading file" << ccfgFilePath;
      return false;
    }
 
    mPortEditor->setCcfg(mCfg);
    mPortEditor->treeWidgetModified(true);  
    
    mMemEdit->setCcfg(mCfg);
    mMemEdit->treeWidgetModified(true);
  
    mRegTabWidget->clear();   
    mProfileEdit->clear();
  }

  mRegTabWidget->populate(mDB);
  mProfileEdit->populate(mDB);

  UtString relIODBName;
  relIODBName << mCtx->getCarbonProjectWidget()->makeRelativePath(maxsim->getOutputDirectory(), iodbName);

  carbonCfgPutIODBFile(mCfg, relIODBName.c_str());

  if (initPorts) {
    carbonCfgPutCompName(mCfg, carbonDBGetTopLevelModuleName(mDB));
    carbonCfgPutTopModuleName(mCfg, carbonDBGetTopLevelModuleName(mDB));
  }

  carbonCfgPutCcfgMode(mCfg, eCarbonCfgARM);

  mCtx->getMainWindow()->statusBar()->showMessage(tr("Database loaded"), 2000);
  QApplication::restoreOverrideCursor();
  return true;
} 

bool MaxsimWizardWidget::loadConfig(const QString &fileName, bool* modifiedFlag)
{
  QString iodbName = getDbFilename();
  QFileInfo fi(iodbName);
  if (!fi.exists()) {
    QString msg = QString("Error: .db file not found: %1.  (Only full db is supported)\n").arg(iodbName);
    qDebug() << msg;
    mCtx->getCarbonProjectWidget()->getConsole()->appendLine(msg);
    mCtx->getCarbonProjectWidget()->makeConsoleVisible();
    return false;
  }

  QApplication::setOverrideCursor(Qt::WaitCursor);
  carbonCfgClear(mCfg);

  mCfg->addScriptExtension(this);

  UtString fname;
  fname << fileName;

  if (QFile::exists(fileName))
  {
    qDebug() << "Reading: " << fileName;

    CarbonCfgStatus cfgStatus = carbonCfgRead(mCfg, fname.c_str());
    bool success = cfgStatus != eCarbonCfgFailure;
    QApplication::restoreOverrideCursor();
    
    if (mCfg->getDB() == NULL)
    {
      QString msg = QString("Error: %1").arg(mCfg->getErrmsg());    
      qDebug() << msg;
      mCtx->getCarbonProjectWidget()->getConsole()->appendLine(msg);
      mCtx->getCarbonProjectWidget()->makeConsoleVisible();
      return false;
    }

    if (success) {
      // Read in the IODB file to populate the debug tree
      const char* iodbFile = carbonCfgGetIODBFile(mCfg);
      success = loadDatabase(iodbFile, false);
      if (success) 
      {
      //  setCurrentFile(fileName);
        m_dbName.clear();
        m_dbName << iodbFile;
        mPortEditor->setCcfg(mCfg);
        mMemEdit->setCcfg(mCfg);
        //mPortEdit->updateWidgets();       
        mProfileEdit->read();
      }
    }
    else {
      mCQt->warning(this, tr("Cannot read file %1:\n%2.")
                    .arg(fileName)
                    .arg(carbonCfgGetErrmsg(mCfg)));
    }
    
    // Database is out of synch, warn the user
    if (cfgStatus == eCarbonCfgInconsistent) 
    {
      *modifiedFlag = true;
      QDockWidget* dw = mCtx->getCarbonProjectWidget()->context()->getWorkspaceModelMaker()->getErrorInfoDockWindow();
      dw->lower();
      dw->raise();
    }

    return success;
  }
  else {
    QString msg = QString("Error: ccfg file not found: %1.\n").arg(fileName);
    qDebug() << msg;
    mCtx->getCarbonProjectWidget()->getConsole()->appendLine(msg);
    mCtx->getCarbonProjectWidget()->makeConsoleVisible();
    return false;
  }
} 

MaxsimChangeOutputDirectory::MaxsimChangeOutputDirectory(MaxsimComponent* maxsim)
{
  OSGetCurrentDir(&mCurrDir);
  UtString err;
  OSChdir(maxsim->getOutputDirectory(), &err);
}

MaxsimChangeOutputDirectory::~MaxsimChangeOutputDirectory()
{
  UtString err;
  OSChdir(mCurrDir.c_str(), &err);
}

void MaxsimWizardWidget::registerTypes(QScriptEngine* engine)
{
  QScriptValue global = engine->globalObject();

   // Construct Application Object
  ScrApplication* scriptApp = new ScrApplication(engine);
  global.setProperty("Application", engine->newObject(scriptApp));

  if (mCtx->getCarbonProjectWidget())
  {
    // Construct Project Object (if there is one)
    ScrProject* scriptProject = new ScrProject();
    global.setProperty("Project", engine->newQObject(scriptProject));
  }
}

