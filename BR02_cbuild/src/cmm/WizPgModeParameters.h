#ifndef WIZPGMODEPARAMETERS_H
#define WIZPGMODEPARAMETERS_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"


#include <QWizardPage>
#include "WizardPage.h"
#include "ui_WizPgModeParameters.h"

class WizardTemplate;

class WizPgModeParameters : public WizardPage
{
  Q_OBJECT

public:
  WizPgModeParameters(QWidget *parent);
  ~WizPgModeParameters();

protected:
  virtual void initializePage();
  int nextId() const;
  virtual bool serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*);

private:
  Ui::WizPgModeParametersClass ui;

};

#endif // WIZPGMODEPARAMETERS_H
