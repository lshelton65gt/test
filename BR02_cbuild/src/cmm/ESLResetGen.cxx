//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"
#include "util/UtHashSet.h"

#include "util/CExprParse.h"

#include "CompWizardPortEditor.h"
#include "CompWizardTreeNodes.h"
#include "PortEditorCommands.h"
#include "ESLResetGen.h"
#include "CcfgHelper.h"
#include "cmm.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"

// ResetGen

UndoData* ESLResetGenItem::deleteItem()
{
   ESLResetGenItem::Delete* undoData = new ESLResetGenItem::Delete();
 
   undoData->mCfg = getCcfg();
   undoData->setTreeData(treeWidget(), this);
   undoData->mRTLPortName = mResetGen->getRTLPort()->getName();
   undoData->setText(mResetGen->getRTLPort()->getName());

   undoData->mActiveValue = mResetGen->mActiveValue;
   undoData->mInactiveValue = mResetGen->mInactiveValue;
   undoData->mClockCycles = mResetGen->mFrequency.getClockCycles();
   undoData->mCompCycles = mResetGen->mFrequency.getCompCycles();
   undoData->mCyclesBefore = mResetGen->mCyclesBefore;
   undoData->mCyclesAsserted = mResetGen->mCyclesAsserted;
   undoData->mCyclesAfter = mResetGen->mCyclesAfter;

   return undoData;
}

void ESLResetGenItem::Delete::undo()
{
  qDebug() << "Undo ResetGen Delete" << text();

  UtString uESLPortName; uESLPortName << mESLPortName;
  CarbonCfgESLPort* eslPort = mCfg->findESLPort(uESLPortName.c_str());
  INFO_ASSERT(eslPort, "Expecting ESL Port");
  mCfg->disconnect(eslPort);

  QTreeWidgetItem* eslItem = getTree()->itemFromIndex(mESLPortIndex);
  INFO_ASSERT(eslItem, "Expecting ESL Port Item");
  getTree()->removeItem(eslItem);

  UtString uRTLPortName; uRTLPortName << mRTLPortName;
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  INFO_ASSERT(rtlPort, "Expecting RTL Port");

  // Create the resetgen and tie it back to this port
  CarbonCfgResetGen* resetGen = new CarbonCfgResetGen(rtlPort, 
                mActiveValue, mInactiveValue, mClockCycles,
                mCompCycles, mCyclesBefore, mCyclesAsserted, mCyclesAfter);

  rtlPort->connect(resetGen);
  
  QTreeWidgetItem* resetsRoot = getTree()->getEditor()->getResetsRoot();
  ESLResetGenItem* newItem = new ESLResetGenItem(resetsRoot, resetGen);

  setTreeData(getTree(), newItem);
  resetsRoot->setExpanded(true);

  newItem->setSelected(true);
  getTree()->scrollToItem(newItem);
  getTree()->setModified(false);
}

void ESLResetGenItem::Delete::redo()
{
  qDebug() << "Delete ResetGen" << text();

  UtString uRTLPortName; uRTLPortName << mRTLPortName;

  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  INFO_ASSERT(rtlPort, "Expecting RTL Port");

// Locate ResetGen
  CarbonCfgResetGen* resetGen = NULL;
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgResetGen* reset = conn->castResetGen();
    if (reset && uRTLPortName == reset->getRTLPort()->getName())
    {
      resetGen = reset;
      break;
    }
  }
  INFO_ASSERT(resetGen, "Expecting to find reset gen");

  // Disconnect the Reset Gen
  mCfg->disconnect(resetGen);

  // Clear selection on the current item
  getItem()->setSelected(false);

  // Remove the node
  getTree()->removeItem(getItem());

  // Turn the RTL Port into an ESL Port
  ESLPortItem* newItem = ESLPortItem::makeESLPort(mCfg, rtlPort, getTree(), mESLPortName, mESLPortIndex);
  getTree()->scrollToItem(newItem);

  getTree()->setModified(true);
}

// Editors

QWidget* ESLResetGenActiveItem::createEditor(const PortEditorDelegate*,
                                             QWidget* parent, int colIndex)
{
  ESLResetGenActiveItem::Columns col = ESLResetGenActiveItem::Columns(colIndex);

  if (col == ESLResetGenActiveItem::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    HexInputValidator64* validator = new HexInputValidator64(lineEdit);
    lineEdit->setValidator(validator);
    return lineEdit;
  }
  return NULL;
}

void ESLResetGenActiveItem::setModelData(const PortEditorDelegate*, 
                                         QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                         QWidget* editor, int colIndex)
{
  ESLResetGenActiveItem::Columns col = ESLResetGenActiveItem::Columns(colIndex);

  if (col == ESLResetGenActiveItem::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (HexInputValidator64::isValid(lineEdit->text()))
    {
      quint64 oldValue = getResetGen()->mActiveValue;
      quint64 newValue = HexInputValidator64::convert(lineEdit->text());
      model->setData(modelIndex, newValue);
  
      if (oldValue != newValue)
        getUndoStack()->push(new CmdChangeESLResetGenActive(this, oldValue, newValue));     
    }
  }
}

QWidget* ESLResetGenInactiveItem::createEditor(const PortEditorDelegate*,
                                               QWidget* parent, int colIndex)
{
  ESLResetGenInactiveItem::Columns col = ESLResetGenInactiveItem::Columns(colIndex);

  if (col == ESLResetGenInactiveItem::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    QIntValidator* validator = new QIntValidator(parent);
    validator->setBottom(0);
    lineEdit->setValidator(validator);
    return lineEdit;
  }

  return NULL;
}

void ESLResetGenInactiveItem::setModelData(const PortEditorDelegate*, 
                                           QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                           QWidget* editor, int colIndex)
{
  ESLResetGenInactiveItem::Columns col = ESLResetGenInactiveItem::Columns(colIndex);

  if (col == ESLResetGenInactiveItem::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (HexInputValidator64::isValid(lineEdit->text()))
    {
      quint64 oldValue = getResetGen()->mInactiveValue;
      quint64 newValue = HexInputValidator64::convert(lineEdit->text());
      model->setData(modelIndex, newValue);
      if (oldValue != newValue)
        getUndoStack()->push(new CmdChangeESLResetGenInactive(this, oldValue, newValue));     
    }
  }
}

QWidget* ESLResetGenCyclesBeforeItem::createEditor(const PortEditorDelegate*,
                                                   QWidget* parent, int colIndex)
{
  ESLResetGenCyclesBeforeItem::Columns col = ESLResetGenCyclesBeforeItem::Columns(colIndex);

  if (col == ESLResetGenCyclesBeforeItem::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    QIntValidator* validator = new QIntValidator(parent);
    validator->setBottom(0);
    lineEdit->setValidator(validator);
    return lineEdit;
  }

  return NULL;
}

void ESLResetGenCyclesBeforeItem::setModelData(const PortEditorDelegate*, 
                                               QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                               QWidget* editor, int colIndex)
{
  ESLResetGenCyclesBeforeItem::Columns col = ESLResetGenCyclesBeforeItem::Columns(colIndex);

  if (col == ESLResetGenCyclesBeforeItem::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    quint32 oldValue = getResetGen()->mCyclesBefore;
    quint32 newValue = lineEdit->text().toUInt();
    model->setData(modelIndex, newValue);
    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeESLResetGenCyclesBefore(this, oldValue, newValue));     
  }
}

QWidget* ESLResetGenCyclesAfterItem::createEditor(const PortEditorDelegate*,
                                                  QWidget* parent, int colIndex)
{
  ESLResetGenCyclesAfterItem::Columns col = ESLResetGenCyclesAfterItem::Columns(colIndex);

  if (col == ESLResetGenCyclesAfterItem::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    QIntValidator* validator = new QIntValidator(parent);
    validator->setBottom(0);
    lineEdit->setValidator(validator);
    return lineEdit;
  }

  return NULL;
}

void ESLResetGenCyclesAfterItem::setModelData(const PortEditorDelegate*, 
                                              QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                              QWidget* editor, int colIndex)
{
  ESLResetGenCyclesAfterItem::Columns col = ESLResetGenCyclesAfterItem::Columns(colIndex);

  if (col == ESLResetGenCyclesAfterItem::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    quint32 oldValue = getResetGen()->mCyclesAfter;
    quint32 newValue = lineEdit->text().toUInt();
    model->setData(modelIndex, newValue);
    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeESLResetGenCyclesAfter(this, oldValue, newValue));     
  }
}

QWidget* ESLResetGenCyclesAssertedItem::createEditor(const PortEditorDelegate*,
                                                     QWidget* parent, int colIndex)
{
  ESLResetGenCyclesAssertedItem::Columns col = ESLResetGenCyclesAssertedItem::Columns(colIndex);

  if (col == ESLResetGenCyclesAssertedItem::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    QIntValidator* validator = new QIntValidator(parent);
    validator->setBottom(0);
    lineEdit->setValidator(validator);
    return lineEdit;
  }

  return NULL;
}

void ESLResetGenCyclesAssertedItem::setModelData(const PortEditorDelegate*, 
                                                 QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                                 QWidget* editor, int colIndex)
{
  ESLResetGenCyclesAssertedItem::Columns col = ESLResetGenCyclesAssertedItem::Columns(colIndex);

  if (col == ESLResetGenCyclesAssertedItem::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    quint32 oldValue = getResetGen()->mCyclesAsserted;
    quint32 newValue = lineEdit->text().toUInt();
    model->setData(modelIndex, newValue);
    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeESLResetGenCyclesAsserted(this, oldValue, newValue));     
  }
}

QWidget* ESLResetGenFrequencyResetItem::createEditor(const PortEditorDelegate*,
                                                     QWidget* parent, int colIndex)
{
  ESLResetGenFrequencyResetItem::Columns col = ESLResetGenFrequencyResetItem::Columns(colIndex);

  if (col == ESLResetGenFrequencyResetItem::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    QIntValidator* validator = new QIntValidator(parent);
    validator->setBottom(0);
    lineEdit->setValidator(validator);
    return lineEdit;
  }

  return NULL;
}

void ESLResetGenFrequencyResetItem::setModelData(const PortEditorDelegate*, 
                                                 QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                                 QWidget* editor, int colIndex)
{
  ESLResetGenFrequencyResetItem::Columns col = ESLResetGenFrequencyResetItem::Columns(colIndex);

  if (col == ESLResetGenFrequencyResetItem::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    quint32 oldValue = getResetGen()->mFrequency.getClockCycles();
    quint32 newValue = lineEdit->text().toUInt();
    model->setData(modelIndex, newValue);
    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeESLResetFrequencyReset(this, oldValue, newValue));     
  }
}

QWidget* ESLResetGenFrequencyCyclesItem::createEditor(const PortEditorDelegate*,
                                                      QWidget* parent, int colIndex)
{
  ESLResetGenFrequencyCyclesItem::Columns col = ESLResetGenFrequencyCyclesItem::Columns(colIndex);

  if (col == ESLResetGenFrequencyCyclesItem::colVALUE)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    QIntValidator* validator = new QIntValidator(parent);
    validator->setBottom(0);
    lineEdit->setValidator(validator);
    return lineEdit;
  }

  return NULL;
}

void ESLResetGenFrequencyCyclesItem::setModelData(const PortEditorDelegate*, 
                                                  QAbstractItemModel* model, const QModelIndex& modelIndex, 
                                                  QWidget* editor, int colIndex)
{
  ESLResetGenFrequencyCyclesItem::Columns col = ESLResetGenFrequencyCyclesItem::Columns(colIndex);

  if (col == ESLResetGenFrequencyCyclesItem::colVALUE)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    quint32 oldValue = getResetGen()->mFrequency.getCompCycles();
    quint32 newValue = lineEdit->text().toUInt();
    model->setData(modelIndex, newValue);
    if (oldValue != newValue)
      getUndoStack()->push(new CmdChangeESLResetFrequencyCycles(this, oldValue, newValue));     
  }
}


CarbonCfgRTLPort* ESLResetGenItem::removeResetGen(PortEditorTreeItem* item, const QString& rtlPortName)
{
  CcfgHelper ccfg(item->getCcfg());
  PortEditorTreeWidget* tree = item->getTree();
  tree->removeItem(item);
  CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(rtlPortName);
  ccfg.disconnect(rtlPort);
  return rtlPort;
}

ESLResetGenItem* ESLResetGenItem::createResetGen(PortEditorTreeWidget* tree,
                        UndoData* undoData, CarbonCfgRTLPort* rtlPort,
                        UInt64 activeValue, UInt64 inactiveValue,
                        UInt32 clockCycles, UInt32 compCycles,
                        UInt32 cyclesBefore, UInt32 cyclesAsserted,
                        UInt32 cyclesAfter)
{
  CcfgHelper ccfg(tree->getCcfg());

  CompWizardPortEditor* editor = tree->getEditor();

  // disconnect if needed
  ccfg.disconnect(rtlPort);

  // Create the resetgen and tie it back to this port
  CarbonCfgResetGen* resetGen = new CarbonCfgResetGen(rtlPort, 
                activeValue, inactiveValue, clockCycles,
                compCycles, cyclesBefore, cyclesAsserted, cyclesAfter);

  // connect it to the RTL port
  rtlPort->connect(resetGen);

  // Create the Tree Item
  QTreeWidgetItem* rootItem = editor->getResetsRoot();
  ESLResetGenItem* newItem = new ESLResetGenItem(rootItem, resetGen);
  undoData->setTreeData(tree, newItem);  
  tree->sortChildren(rootItem);
  tree->expandChildren(rootItem);
  newItem->setSelected(true);
  tree->scrollToItem(newItem);
  return newItem;
}

