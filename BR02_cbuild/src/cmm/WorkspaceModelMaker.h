#ifndef __WORKSPACEMODELMAKER_H__
#define __WORKSPACEMODELMAKER_H__

#include "util/CarbonPlatform.h"

#include <QObject>
#include <QProgressBar>
#include <QStatusBar>
#include <QDockWidget>

#include "CarbonWorkspace.h"

class CarbonProject;

class WorkspaceModelMaker : public CarbonWorkspace
{
  Q_OBJECT

public:
  WorkspaceModelMaker(QProgressBar*, QStatusBar*);
  virtual ~WorkspaceModelMaker();
  QDockWidget* getProjectDockWindow() const { return mProjectDockWindow; }
  QDockWidget* getPropertiesDockWindow() const { return mPropertiesDockWindow; }
  QDockWidget* getRemoteConsoleDockWindow() const { return mRemoteConsoleDockWindow; }
  QDockWidget* getErrorInfoDockWindow() const { return mErrorInfoDockWindow; }
  QStatusBar* getStatusBar() const { return mStatusBar; }

protected:
  virtual void initialize(CarbonMakerContext* context);

private slots:
  void projectClosing(CarbonProject*);

private:
  QProgressBar* mProgressBar;
  QStatusBar* mStatusBar;
  QDockWidget* mProjectDockWindow;
  QDockWidget* mPropertiesDockWindow;
  QDockWidget* mRemoteConsoleDockWindow;
  QDockWidget* mErrorInfoDockWindow;
};

#endif

