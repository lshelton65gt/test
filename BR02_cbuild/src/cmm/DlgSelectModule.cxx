//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "DlgSelectModule.h" 

DlgSelectModule::DlgSelectModule(QWidget *parent, CarbonProjectWidget* pw)
: QDialog(parent)
{
  ui.setupUi(this);

  mDesignHierarchy = NULL;
  mToplevelInterface = NULL;
  mInstance = NULL;

  mProjectWidget = pw;

  CarbonProject* proj = pw->project();
  if (proj)
  {
    const char* designHierarchyFile = proj->getDesignFilePath(".designHierarchy");
    mDesignHierarchy = new XDesignHierarchy(designHierarchyFile);
    XmlErrorHandler* eh = mDesignHierarchy->getErrorHandler();
    if (eh->hasErrors())
    {
      QString msg = QString("Error reading design hierarchy: %1\n%2")
        .arg(designHierarchyFile)
        .arg(eh->errorMessages().join("\n"));
    }
    else
      populate();
  }
  updateButtons();
}

void DlgSelectModule::on_buttonBox1_accepted()
{
  mInstance = NULL;

  foreach (QTreeWidgetItem* item, ui.treeWidget->selectedItems())
  {
    QVariant qv = item->data(1, Qt::UserRole);
    QVariant qv1 = item->data(0, Qt::UserRole);

    mInstance = (XInstance*)qv.value<void*>();
    mToplevelInterface = (XToplevelInterface*)qv1.value<void*>();

    break;
  }

  accept();
}
void DlgSelectModule::on_buttonBox1_rejected()
{
  reject();
} 
void DlgSelectModule::on_treeWidget_itemSelectionChanged()
{
  updateButtons();
}
void DlgSelectModule::updateButtons()
{
  QPushButton* okButton = ui.buttonBox1->button(QDialogButtonBox::Ok);
  okButton->setEnabled(ui.treeWidget->selectedItems().count() > 0);
}
void DlgSelectModule::populate()
{
  ui.treeWidget->clear();

  XTopLevelInterfaces* topInterfaces = mDesignHierarchy->getToplevelInterfaces();   for (int i=0; i<topInterfaces->numInterfaces(); i++)
  {
    XToplevelInterface* topi = topInterfaces->getAt(i);
    QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidget);
    item->setText(0, topi->getName());
    item->setExpanded(true);
    QVariant qv = qVariantFromValue((void*)topi);
    item->setData(0, Qt::UserRole, qv);
    QList<XInstance*>* instances = topi->getChildren();
    foreach (XInstance* inst, *instances)
    {
      addInstance(item, topi, inst);
    }
  }
} 
void DlgSelectModule::addInstance(QTreeWidgetItem* parent, XToplevelInterface* topi, XInstance* inst)
{
  QVariant qv = qVariantFromValue((void*)topi);
  QVariant qv1 = qVariantFromValue((void*)inst);

  QTreeWidgetItem* item = new QTreeWidgetItem(parent);
  item->setText(0, inst->getID());
  item->setData(0, Qt::UserRole, qv);
  item->setData(1, Qt::UserRole, qv1);
  item->setExpanded(true);

  QList<XInstance*>* instances = inst->getChildren();
  foreach (XInstance* childInst, *instances)
    addInstance(item, topi, childInst);
}

DlgSelectModule::~DlgSelectModule()
{
  if (mDesignHierarchy)
    delete mDesignHierarchy;
}

