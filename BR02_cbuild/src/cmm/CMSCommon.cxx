//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"

#include "gui/CQt.h"
#include "util/OSWrapper.h"
#include "util/UtString.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"

#include "CMSCommon.h"

static UInt32 replaceSubstring(UtString* str, const char* oldStr, const char* newStr);

// If we have a format, use it, otherwise return the message
const char* MessageExpression::getFormattedMessage()
{
  if (mFormat.length() == 0)
    return getCapturedValue(MessageCapture::Message);
  else
  {
    static UtString msg;
    msg.clear();

    msg << mFormat;

    for (UInt32 i=0; i<numCaptures(); i++)
    {
      UtString capName;
      capName << "$(" << i+1 << ")";
      MessageCapture* cap = getCapture(i);
      replaceSubstring(&msg, capName.c_str(), cap->getCapture());
    }
   
    return msg.c_str();
  }
}

// MessageExpressions
MessageExpression* MessageExpression::parseXML(xmlNodePtr parent)
{
  MessageExpression* exp = new MessageExpression();

  UtString type;
  XmlParsing::getProp(parent, "type", &type);
  INFO_ASSERT(type.length() > 0, "Missing type property");

  exp->mType = convertMessage(type.c_str());

  UtString nav;
  XmlParsing::getProp(parent, "navigate", &nav);
  INFO_ASSERT(type.length() > 0, "Missing navigate property");

  exp->mNavigation = convertNavigation(nav.c_str());

  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    const char* element = XmlParsing::elementName(child);
    if (element == NULL) // Skip non-interesting XML things
      continue;

    if (0 == strcmp(element, "Expression"))
    {
      UtString expr;
      XmlParsing::getProp(child, "RegExp", &expr);
      INFO_ASSERT(expr.length() > 0, "Missing RegExp property");
      exp->mRegExp.setPattern(expr.c_str());

      for (xmlNodePtr expChild = child->children; expChild != NULL; expChild = expChild->next)
      {
        element = XmlParsing::elementName(expChild);
        if (element == NULL)
          continue;

        if (0 == strcmp(element, "MessageFormat"))
        {
          UtString format;
          XmlParsing::getContent(expChild, &format);
          exp->putFormat(format.c_str());
        }
        else if (0 == strcmp(element, "Capture"))
          exp->mCaptures.push_back(MessageCapture::parseXML(expChild));
      }
    }
  }

  return exp;
}

const char* MessageExpression::getCapturedValue(MessageCapture::Capture captureType)
{
  for (int i=0; i<mCaptures.count(); i++)
  {
    MessageCapture* cap = mCaptures[i];
    if (cap->getType() == captureType)
      return cap->getCapture();
  }
  return "";
}

int MessageExpression::getCapturedValueInt(MessageCapture::Capture captureType)
{
  int result=0;
  const char* value = getCapturedValue(captureType);
  UtIStringStream IntValue(value);
  IntValue >> UtIO::dec >> result;
  return result;
}

MessageExpression::Severity MessageExpression::getSeverity()
{
  for (int i=0; i<mCaptures.count(); i++)
  {
    MessageCapture* cap = mCaptures[i];
    if (cap->getType() == MessageCapture::Severity)
      return convertSeverity(cap->getCapture(), false);
  }
  return MessageExpression::Unknown;
}

MessageExpression::Severity MessageExpression::convertSeverity(const char* sev, bool validate)
{
  if (0 == strcmp(sev, "Error") || 0 == strcmp(sev, "Alert") || 0 == strcmp(sev, "Fatal") || 0 == strcmp(sev, "error") || 0 == strcmp(sev, "fatal error"))
    return MessageExpression::Error;

  if (0 == strcmp(sev, "Warning") || 0 == strcmp(sev, "warning"))
    return MessageExpression::Warning;

  if (0 == strcmp(sev, "Information") || 0 == strcmp(sev, "Note") || 0 == strcmp(sev, "Status"))
    return MessageExpression::Information;

  if (validate)
  {
    UtString err;
    err << "Unknown severity: " << sev;
    INFO_ASSERT(false, err.c_str());
  }

  return MessageExpression::Unknown;
}

MessageExpression::MessageType MessageExpression::convertMessage(const char* sev)
{
  if (0 == strcmp(sev, "MakefileError"))
    return MessageExpression::MakefileError;

  if (0 == strcmp(sev, "SourceHyperlink"))
    return MessageExpression::SourceHyperlink;

  if (0 == strcmp(sev, "Generic"))
    return MessageExpression::Generic;

  if (0 == strcmp(sev, "CompileError"))
    return MessageExpression::CompileError;

  UtString err;
  err << "Unknown Message Type: " << sev;
  INFO_ASSERT(false, err.c_str());

  return MessageExpression::Generic;
}

MessageExpression::Navigation MessageExpression::convertNavigation(const char* sev)
{
  if (0 == strcmp(sev, "None"))
    return MessageExpression::None;

  if (0 == strcmp(sev, "SourceLine"))
    return MessageExpression::SourceLine;


  UtString err;
  err << "Unknown Navigation Type: " << sev;
  INFO_ASSERT(false, err.c_str());

  return MessageExpression::None;
}

bool MessageExpression::process(const char* line)
{
  if (mRegExp.exactMatch(line))
  {
    INFO_ASSERT((int)numCaptures() <= mRegExp.numCaptures(), "Mismatching number of captures, check regexp");

    for (int i=0; i<mCaptures.count(); i++)
    {
      MessageCapture* cap = mCaptures[i];
      UtString capture;
      capture << mRegExp.cap(cap->getCaptureIndex());
      cap->putCapture(capture.c_str());
    }
    return true;
  }

  return false;
}

const char* MessageExpression::getNavigationLocator()
{
  mNavigationLocator.clear();

  return mNavigationLocator.c_str();
}

ErrorMessage::ErrorMessage(const QString& msg, MessageExpression* exp)
{
  mRawMsg << msg;
  mLineNumber = -1;
  mMessage = exp->getFormattedMessage();
  mExp = exp;
  mReference = exp->getCapturedValue(MessageCapture::Reference);

  switch (mExp->getNavigation())
  {
  case MessageExpression::None:
    break;
  case MessageExpression::Source:
    break;
  case MessageExpression::SourceLine:
    {
      mSource = exp->getCapturedValue(MessageCapture::Source);
      mLineNumber = exp->getCapturedValueInt(MessageCapture::LineNumber);
    }
    break;
  }
  mMessageNumber = exp->getCapturedValueInt(MessageCapture::ErrorNumber);
}

MessageExpression::Severity ErrorMessage::getSeverity() const
{
  mExp->process(mRawMsg.c_str());
  return mExp->getSeverity();
}

// MessageCapture

MessageCapture* MessageCapture::parseXML(xmlNodePtr parent)
{
  MessageCapture* cap = new MessageCapture();

  UtString item;
  XmlParsing::getProp(parent, "item", &item);
  INFO_ASSERT(item.length() > 0, "Missing item property");

  UtIStringStream Item(item);
  Item >> UtIO::dec >> cap->mCaptureIndex;

  UtString type;
  XmlParsing::getProp(parent, "type", &type);
  INFO_ASSERT(type.length() > 0, "Missing type property");

  cap->mType = convertCapture(type.c_str());

  return cap;
}

MessageCapture::Capture MessageCapture::convertCapture(const char* sev)
{
  if (0 == strcmp(sev, "Source"))
    return MessageCapture::Source;

  if (0 == strcmp(sev, "LineNumber"))
    return MessageCapture::LineNumber;

  if (0 == strcmp(sev, "Severity"))
    return MessageCapture::Severity;

  if (0 == strcmp(sev, "Message"))
    return MessageCapture::Message;

  if (0 == strcmp(sev, "ErrorNumber"))
    return MessageCapture::ErrorNumber;

  if (0 == strcmp(sev, "Reference"))
    return MessageCapture::Reference;

  UtString err;
  err << "Unknown Capure Type: " << sev;
  INFO_ASSERT(false, err.c_str());

  return MessageCapture::Message;
}

static UInt32 replaceSubstring(UtString* str, const char* oldStr, const char* newStr) {
  size_t pos;
  UInt32 num = 0;
  while ((pos = str->find(oldStr)) != UtString::npos) {
    str->replace(pos, strlen(oldStr), newStr);
    ++num;
  }
  return num;
}
