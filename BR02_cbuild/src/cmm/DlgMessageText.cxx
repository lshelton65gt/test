//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>

#include "DlgMessageText.h"

DlgMessageText::DlgMessageText(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);

  QMessageBox mb;
  ui.textBrowser->setFontPointSize(mb.fontInfo().pointSizeF());
}

DlgMessageText::~DlgMessageText()
{
}

void DlgMessageText::setTitle(const QString& title)
{
  setWindowTitle(title);
}

void DlgMessageText::setMessage(const QString& msg)
{
  ui.textBrowser->setText(msg);
}

void DlgMessageText::on_pushButtonOK_clicked()
{
  accept();
}
