//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "PluginWidget.h"
#include "cmm.h"

PluginWidget::PluginWidget(CarbonMakerContext* ctx, MDIDocumentTemplate* t, QWidget *parent)
: QWidget(parent)
{
  mCtx = ctx;
  mParent = parent;
  //setAttribute(Qt::WA_DeleteOnClose);
  initializeMDI(this, t);
}

PluginWidget::~PluginWidget()
{
}


bool PluginWidget::loadFile(const QString& pluginName)
{
  mScriptFilePath = pluginName;

  // tbd
  //QDockWidget* dw = new QDockWidget("Title", theApp);
  //QWidget* uiWidget = mEngine.runScriptUI("calculator.mjs", "calculator.ui", "Calculator", dw);
  //dw->setWidget(uiWidget);
  //theApp->addDockWidget(Qt::BottomDockWidgetArea, dw);

  /* mSplitter->addWidget(uiWidget);
  mSplitter->addWidget(new QTextBrowser(this));
    mSplitter->setStretchFactor(0, 0);
    mSplitter->setStretchFactor(1, 1);
*/
  //mSplitter->show();

 // mSplitter->insertWidget(1, uiWidget);

  //QScriptValueList args;
  //QScriptValue uiFileValue = mEngine.callOptionalScriptFunction("cmsUIFilename", args);

  //if (uiFileValue.isValid())
  //{
  //  QString uiFileName = uiFileValue.toString();
  //  QWidget* formWidget = mEngine.loadUI(uiFileName, mParent);
  //  if (formWidget)
  //    formWidget->show();
  //}

  return false;
}


