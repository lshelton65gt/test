#ifndef __CARBONCOMPONENT_H__
#define __CARBONCOMPONENT_H__

#include "util/CarbonPlatform.h"

#include "util/XmlParsing.h"
#include <QObject>
#include <QTreeWidget>
#include <QTextStream>
#include "CarbonProjectTreeNode.h"
#include "CarbonFiles.h"
#include "DlgConfirmDelete.h"

class CarbonProject;
class UtXmlErrorHandler;
class CarbonComponent;
class CarbonProjectWidget;
class RemoteConsole;
class CarbonConsole;
class CarbonConfiguration;

//
// File: CarbonComponent
//
// A Carbon component
//
class CarbonComponentTreeItem : public QObject, public CarbonProjectTreeNode
{
  Q_OBJECT

public:
  enum ComponentType { Maxsim, CoWare, ModelValidation, SystemC };

  CarbonComponentTreeItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, CarbonComponent* comp);
  virtual ~CarbonComponentTreeItem();

public:
  void deleteComponent(const char* outputDir, ComponentType compType, bool interactive = true, DlgConfirmDelete::ConfirmResult confirmResult = DlgConfirmDelete::Cancel);

protected:
  CarbonComponent* mComponent;
};

//
// Class: CarbonComponent
//
class CarbonComponent : public QObject
{
  Q_OBJECT

public:
  // Enum: ComponentType
  //
  // Enum Values: [UnknownComponent|SoCDesigner|CoWare|ModelValidation|SystemC|Packaging]
  enum ComponentType { UnknownComponent, SoCDesigner, CoWare, ModelValidation, SystemC, Packaging };

  enum TargetType { Build, Clean, Package};
  enum TargetPlatform { Windows, Unix };

// Property: type
// The enumerated type of this component
// one of:
//
//   Component.UnknownComponent
//   Component.SocDesigner
//   Component.CoWare
//   Component.ModelValidation
//   Component.SystemC
//   Component.Packaging
//
// Access:
// *ReadOnly*
  Q_PROPERTY(ComponentType type READ getType);
  // Property: name
  // The string name for this component.
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString name READ getNameStr);
  Q_ENUMS(ComponentType);

  ComponentType getType() const;
  QString getNameStr() { return getName(); }
  CarbonComponent(CarbonProject* proj, const char* name);
  const char* getName() const { return mName.c_str(); }
  void putName(const char* newVal) { mName=newVal; }
  QString getFriendlyName() const { return mFriendlyName; }
  void setFriendlyName(const QString& newVal) { mFriendlyName=newVal; }

  // Overridable
  virtual bool serialize(xmlTextWriterPtr writer);

  virtual const char* getTargetName(CarbonConfiguration*, TargetType, TargetPlatform=Unix) { return ""; }
  virtual bool getTargetImpl(CarbonConfiguration*, QTextStream&, TargetType) { return false; }
  // override to implement a specific check
  virtual bool checkComponent(CarbonConsole*) { return true; }
  virtual bool canCompile(CarbonConfiguration* cfg);

  // override to generate additional supporting files
  virtual void generateMakefile(CarbonConfiguration*, const char* /*makefileName*/) { }

  virtual CarbonComponentTreeItem* createTreeItem(CarbonProjectWidget* tree, QTreeWidgetItem* parent);
  virtual ~CarbonComponent();

  virtual void setXtorDefFile(const char*)
  {
  }

  virtual const char* xtorDefFile()
  {
    return NULL;
  }

  // override for additional post-compilation commands
  virtual void makefileFinished(TargetType) { }

  virtual void setEnabled(bool) {}
  virtual void updateCcfg(CarbonConfiguration*, const char*) {};

  // override to get the output directory
  virtual const char* getOutputDirectory(const char* cfgName=NULL);
  virtual const char* getComponentDirectory() const;
  
  // override for specific name
  virtual const char* getWizardFilename();

  // overide to specify "packageable" files, the files will be added to the distribution
  // package when this component is checked in the Package Options.
  virtual UInt32 numPackageFiles() { return 0;}
  virtual QString getPackageFile(UInt32) { return ""; }

  virtual bool isWindowsTarget(CarbonConfiguration*) { return false; }

  virtual int getGeneratedFiles(QStringList&) { return 0; }
  virtual int getComponentFiles(QStringList&) { return 0; }
  virtual int getSourceFiles(QStringList&) { return 0; }

  void copyAndRenameCcfg(const char* outputDir, const char* oldCcfgName, const char* newCcfgName);

  static const char* echoBegin() { return "$(call CARBON_ECHO,"; }
  static const char* echoEndl() { return ")\n"; }
  bool checkForCcfgErrors(QStringList& errorList);

  bool isModelKitComponent() const { return mModelKitComponent; }
  void setModelKitComponent(bool newVal) { mModelKitComponent=newVal; }

protected:
  CarbonProject* getProject() { return mProject; }
  const CarbonProject* getProject() const { return mProject; }

private:
  UtString mName;
  CarbonProject* mProject;
  QString mFriendlyName;
  bool mModelKitComponent;
};

#endif
