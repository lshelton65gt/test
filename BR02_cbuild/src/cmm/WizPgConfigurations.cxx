//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "WizPgConfigurations.h"
#include "MemoryWizard.h"
#include "WizardTemplate.h"
#include "Template.h"
#include "Mode.h"
#include "Block.h"
#include "BlockInstance.h"
#include "GfxBlock.h"

WizPgConfigurations::WizPgConfigurations(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);
  ui.graphicsView->setScene(&mScene);

  if (!connect(&mSymbol, SIGNAL(blockSelected(GfxBlock*)), this, SLOT(blockSelected(GfxBlock*))))
    qDebug() << "not such slot";
}

void WizPgConfigurations::blockSelected(GfxBlock* gfxBlock)
{
  BlockInstance* blockInstance = gfxBlock->getBlockInstance();

  ui.labelParameters->setText(QString("Parameters for: %1").arg(gfxBlock->getName()));
  ui.treeWidgetBlockParams->populate(blockInstance->getParameters());
  ui.treeWidgetPortTree->populate(blockInstance->getPorts());
}

void WizPgConfigurations::initializePage()
{
  setTitle("Graphical Representation");
  setSubTitle("The following interface(s) will be generated for this model.");

  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardTemplate* templ = wiz->getTemplate();
  WizardContext* context = templ->getContext();

  QString modeName = wiz->field("portMode").toString();

  Mode* mode = templ->getContext()->getTemplate()->findMode(modeName);
  if (mode)
  {
    // Draw the symbol
    mSymbol.draw(&mScene, context->getModel());
  }
  else
    qDebug() << "Internal Error: Mode should be defined";

  QString replayFile = wiz->getReplayFile();
  if (replayFile.length() > 0)
    wiz->postNext();
}

WizPgConfigurations::~WizPgConfigurations()
{

}

bool WizPgConfigurations::serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  QDomElement tag = doc.createElement("WizardStep");
  tag.setAttribute("name", objectName());
  parent.appendChild(tag);

  return true;
}

int WizPgConfigurations::nextId() const
{
  return MemoryWizard::PageCodeGenerate;
}
