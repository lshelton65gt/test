//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "Modes.h"
#include "Mode.h"
#include "Template.h"

#include "Blocks.h"
#include "Ports.h"
#include "Parameters.h"

#include <QDebug>

Ports* Mode::getPorts()
{
  return &mPorts;
}

Blocks* Mode::getBlocks()
{
  return &mBlocks;
}

Parameters* Mode::getParameters()
{
  return &mParameters;
}

// Remove any user defined ports
void Mode::preElaborate()
{
  mBlocks.removeUserDefined();
  mPorts.removeUserDefined();
}

//
// Read ALL Modes and add them to config
//
bool Mode::deserialize(Template* templ, Modes* modes, const QDomElement& parent, XmlErrorHandler* eh)
{
  QDomNode n = parent.firstChild();
  while(!n.isNull())
  {
    QDomElement e = n.toElement(); // try to convert the node to an element.
 
    if ("Mode" == e.tagName())
    {
      Mode* mode = new Mode();
      mode->setName(e.attribute("name"));
      modes->addMode(mode);

      if (!mode->mParameters.deserialize(e, eh))
        return false;

      if (!mode->mBlocks.deserialize(mode, templ, e, eh))
        return false;

      if (!mode->mPorts.deserialize(mode, templ, NULL, e, eh))
        return false;
    }
    n = n.nextSibling();
  }
  return true;
}
