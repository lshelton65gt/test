//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "MdiPlugin.h"
#include "PluginWidget.h"

MDIPluginTemplate::MDIPluginTemplate(CarbonMakerContext* ctx)
  : MDIDocumentTemplate("ScriptPlugin", "Modelstdio Script", "CMS Script (*.mjs)") 
{
  mContext = ctx; 
}

void MDIPluginTemplate::updateMenusAndToolbars(QWidget* /*widget*/)
{
}

int MDIPluginTemplate::createToolbars()
{
  return 0;
  //QToolBar* toolbar = new QToolBar("SoC Designer Toolbar");
  //toolbar->setObjectName("MaxSim Toolbar1");

  //toolbar->addAction(mActionDelete);
  //toolbar->addAction(mActionNewParam);

  //addToolbar(toolbar);
  //return numToolbars();
}

int MDIPluginTemplate::createMenus()
{
  return 0;
}

MDIWidget* MDIPluginTemplate::createNewDocument(QWidget* /*parent*/)
{
  return NULL;
}

// We don't have a MDI document presence, only the docking window widget
// so we create the InvisibleProject here to handle menu & toolbar events
//
MDIWidget* MDIPluginTemplate::openDocument(QWidget* parent, const char* docName)
{
  qDebug() << "Loading CMS Plugin" << docName;

  QApplication::setOverrideCursor(Qt::WaitCursor);
  PluginWidget* widget = new PluginWidget(mContext, this, parent);
  bool status = widget->loadFile(docName);
  QApplication::restoreOverrideCursor();

  if (status)
    return widget;
  else
  {
    widget->close();
    return NULL;
  }
  return NULL;
}
