//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "EnumChoiceWidget.h"

#include <QtGui>

EnumChoiceWidget::EnumChoiceWidget(QWidget *parent)
: QWidget(parent)
{
  ui.setupUi(this);
  // start with an empty table, caller should use setItems(list) to prime the table
  ui.tableWidget->setRowCount(0);
  ui.tableWidget->setColumnCount(1);

  ui.tableWidget->horizontalHeader()->setVisible(false);
  ui.tableWidget->verticalHeader()->setVisible(false);

  mItems = QStringList();
  mDefaultItem = NULL;

}

EnumChoiceWidget::~EnumChoiceWidget()
{
mDefaultItem = NULL;
}
void EnumChoiceWidget::on_Add_Item_clicked()
{
  ui.pushButtonUP->setEnabled(false);
  ui.pushButtonDOWN->setEnabled(false);

  int numRows = ui.tableWidget->rowCount();
  int currRow = -1;
  if ( numRows ){
    currRow = numRows-1;
    QTableWidgetItem* curr = ui.tableWidget->currentItem();
    if (curr ){
      currRow = curr->row();
    }
  }
  currRow++;  // insert following the selected item
  ui.tableWidget->insertRow(currRow);

  QTableWidgetItem* item = new QTableWidgetItem("New item");

  ui.tableWidget->setItem(currRow, 0, item); 
  ui.tableWidget->scrollToItem(item);
  ui.tableWidget->setCurrentItem(item);  
  ui.tableWidget->editItem(item);

  if (numRows == 0)
    setDefaultItem(item);
}

void EnumChoiceWidget::on_pushButtonUP_clicked()
{
  moveCurrentItem(-1);
}

void EnumChoiceWidget::on_pushButtonDOWN_clicked()
{
  moveCurrentItem(1);
}

void EnumChoiceWidget::moveCurrentItem(int delta)
{
  bool sorting = ui.tableWidget->isSortingEnabled();

  if (sorting)
    ui.tableWidget->setSortingEnabled(false);

  QTableWidgetItem* curr = ui.tableWidget->currentItem();
  int currRow = curr->row();

  QTableWidgetItem* srcItem = ui.tableWidget->takeItem(currRow, 0); 
  QTableWidgetItem* dstItem = ui.tableWidget->takeItem(currRow+delta, 0);

  ui.tableWidget->setItem(currRow, 0, dstItem);

  ui.tableWidget->setItem(currRow+delta, 0, srcItem);

  ui.tableWidget->setCurrentItem(srcItem);

  if (sorting)
    ui.tableWidget->setSortingEnabled(true);
}


void EnumChoiceWidget::setItems(const QStringList &items, int defaultIndex){
  int i = 0;

  int numRows = items.size();
  if (numRows ) {
    ui.tableWidget->setRowCount(numRows);
    foreach(QString stringitem, items)
    {
      QTableWidgetItem* item = new QTableWidgetItem(stringitem);
      ui.tableWidget->setItem(i, 0, item);
      if ( defaultIndex == i ) {
        setDefaultItem(item);
      }
      i++;
    }
    if ( mDefaultItem ){
      ui.tableWidget->setCurrentItem(mDefaultItem);
    } else {
      // no default so auto select the first item
      QTableWidgetItem* curr = ui.tableWidget->item(0,0);
      ui.tableWidget->setCurrentItem(curr);
    }
  }
}
void EnumChoiceWidget::setDefaultText(const QString& default_text){
  mDefaultItem = NULL;
  int numRows = ui.tableWidget->rowCount();
  for ( int i = 0; i < numRows; i++) {
    QTableWidgetItem* item = ui.tableWidget->item(i,0);
    if ( item->text() == default_text ){
      setDefaultItem(item);
    }
  }
}

QString EnumChoiceWidget::defaultText()
{
  return mDefaultItem->text();
}

void EnumChoiceWidget::setCurrentItem(const QString& current_text){
  int numRows = ui.tableWidget->rowCount();
  for ( int i = 0; i < numRows; i++) {
    QTableWidgetItem* item = ui.tableWidget->item(i,0);
    if ( item->text() == current_text ){
      ui.tableWidget->setCurrentItem(NULL); // set to null then to the requested item to force a currentItemChanged event
      ui.tableWidget->setCurrentItem(item);
    }
  }
}


QStringList EnumChoiceWidget::getItems(){
    int numRows = ui.tableWidget->rowCount();
    mItems.clear();
    for ( int i = 0; i < numRows; i++) 
  {
    QTableWidgetItem* item = ui.tableWidget->item(i,0);
    mItems.append(item->text());
  }
  return mItems;
}

void EnumChoiceWidget::on_tableWidget_currentItemChanged(QTableWidgetItem* current, QTableWidgetItem*)
{
  ui.pushButtonSetDefault->setEnabled(current != NULL);
  ui.pushButtonUP->setEnabled(false);
  ui.pushButtonDOWN->setEnabled(false);
  ui.Delete_Item->setEnabled(false);

  if (current)
  {
    ui.Delete_Item->setEnabled(true);
  
   
    int currRow = current->row();
    if (currRow > 0)
      ui.pushButtonUP->setEnabled(true);

    if (currRow < ui.tableWidget->rowCount()-1)
      ui.pushButtonDOWN->setEnabled(true);
  }
}



void EnumChoiceWidget::on_Delete_Item_clicked()
{
  QString msg = QString("Are you sure you want to delete this item?");
  if (QMessageBox::question(this, "Item Delete", msg, QMessageBox::Cancel|QMessageBox::Yes, QMessageBox::Cancel) == QMessageBox::Yes){



    ui.pushButtonUP->setEnabled(false);
    ui.pushButtonDOWN->setEnabled(false);

    int numRows = ui.tableWidget->rowCount();
    if (numRows ) {

      QTableWidgetItem* curr = ui.tableWidget->currentItem();
      if (curr ){
        if (curr == mDefaultItem ) 
          mDefaultItem = NULL;

        int currRow = curr->row();
        ui.tableWidget->removeRow(currRow);
        // the tableWidget moves the current selection to an item adjacent to the item selected for deletion, 
        // but apparently the move happens before the deletion, we need to move the current item again 
        // to force the generation of the currentItemChanged signal which is used for setting the up/down buttons
        curr = ui.tableWidget->currentItem();
        if ( curr ) {
          ui.tableWidget->setCurrentItem(0);
          ui.tableWidget->setCurrentItem(curr);
        }
      }
    }
  }
}






void EnumChoiceWidget::setDefaultItem(QTableWidgetItem* item)
{
  if (mDefaultItem)
  {
    QFont f = mDefaultItem->font();
    f.setBold(false);
    mDefaultItem->setFont(f);
  }
  
  mDefaultItem = item;

  if (mDefaultItem)
  {
    QFont f = mDefaultItem->font();
    f.setBold(true);
    mDefaultItem->setFont(f);
  }
}

void EnumChoiceWidget::on_pushButtonSetDefault_clicked()
{
  QTableWidgetItem* item = ui.tableWidget->currentItem();
  if (item)
  {
    setDefaultItem(item);
  }
}
