//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#include "SimulationHistoryWidget.h"

SimulationHistoryWidget::SimulationHistoryWidget(QWidget *parent)
  : ReplayPerfPlot(new CarbonReplaySystem(), parent)
{
}

SimulationHistoryWidget::~SimulationHistoryWidget()
{
}

void SimulationHistoryWidget::carbonSystemUpdated(CarbonRuntimeState &/*state*/, CarbonReplaySystem &guiSystem, CarbonReplaySystem &simSystem)
{
  (void) guiSystem;

  // save current item text
  QString currentText = mXAxisCombo->currentText();

  // set up the x axis widget.  not sure why this is not done in ReplayPerfPlot
  mXAxisCombo->clear();
  mXAxisCombo->addItem("CPU Seconds (User + Sys)");
  mXAxisValues.clear();
  mXAxisValues.push_back(ReplayPerfPlot::eCPUTime);
  mXAxisCombo->addItem("Real Time (seconds)");
  mXAxisValues.push_back(ReplayPerfPlot::eRealTime);

  if (simSystem.isCycleCountSpecified()) {
    mXAxisCombo->addItem("Cycles");
    mXAxisValues.push_back(ReplayPerfPlot::eCycles);
  }
  if (simSystem.isSimTimeSpecified()) {
    UtString buf;
    buf << "Sim Time (" << simSystem.getSimTimeUnits() << ")";
    mXAxisCombo->addItem(buf.c_str());
    mXAxisValues.push_back(ReplayPerfPlot::eSimTime);
  }

  // Don't let the user focus on an x-axis metric that does not exist in this
  // simulation.  E.g. don't look at cycles in (say) System C where there is not
  // a clear definition of cycles.  Prefer (in that case) simulatio time if
  // available
  if (! simSystem.isCycleCountSpecified() &&
      (getXAxis() == ReplayPerfPlot::eCycles))
  {
    if (simSystem.isSimTimeSpecified()) {
      putXAxis(ReplayPerfPlot::eSimTime);
    }
    else {
      putXAxis(ReplayPerfPlot::eCPUTime); // cpu time is next best metric
    }
  }
  else if (! simSystem.isSimTimeSpecified() &&
           (getXAxis() == ReplayPerfPlot::eSimTime))
  {
    // In Maxsim the opposite is probably the case.  We know about cycles but
    // not sim time
    if (simSystem.isCycleCountSpecified()) {
      putXAxis(ReplayPerfPlot::eCycles);
    }
    else {
      putXAxis(ReplayPerfPlot::eCPUTime); // cpu time is next best metric
    }
  }

  mXAxisCombo->addItem("Aggregate Schedule Calls");
  mXAxisValues.push_back(ReplayPerfPlot::eSchedCalls);

  // select the previously-selected axis if possible
  for (int i = 0, n = mXAxisCombo->count(); i < n; i++) {
    if (currentText == mXAxisCombo->itemText(i)) {
      mXAxisCombo->setCurrentIndex(i);
      break;
    }
  }

  // call inherited methods to update the plot
  putReplaySystem(&simSystem);
  update();
}

void SimulationHistoryWidget::changeXAxis(int i) {
  putXAxis(mXAxisValues[i]);
}
