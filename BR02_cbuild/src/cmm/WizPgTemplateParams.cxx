//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "MemoryWizard.h"
#include "WizPgTemplateParams.h"
#include "WizardTemplate.h"
#include "Template.h"
#include "PortConfigurations.h"
#include "PortConfiguration.h"
#include "Modes.h"
#include "Mode.h"

WizPgTemplateParams::WizPgTemplateParams(QWidget *parent)
    : WizardPage(parent)
{
  ui.setupUi(this);

  registerField("portMode*", ui.comboBoxMode, "currentText");
  registerField("moduleName*", ui.lineEditModuleName);

  if (!connect(ui.treeWidget, SIGNAL(valuesChanged()), this, SLOT(valuesChanged())))
  {
    qDebug() << "ERROR: No such slot valuesChanged";
  }
}

WizPgTemplateParams::~WizPgTemplateParams()
{
}

bool WizPgTemplateParams::validatePage()
{
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardTemplate* wizTempl = wiz->getTemplate();
  wizTempl->getContext()->setEnableDebugger(true);
  Template* templ = wizTempl->getContext()->getTemplate();
  templ->setModuleName(wizard()->field("moduleName").toString());
  return QWizardPage::validatePage();
}

bool WizPgTemplateParams::serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardTemplate* templ = wiz->getTemplate();
  Parameters* params = templ->getContext()->getTemplate()->getParameters();

  QDomElement tag = doc.createElement("WizardStep");
  tag.setAttribute("name", objectName());
  parent.appendChild(tag);

  addElement(doc, tag, "PortMode", wizard()->field("portMode").toString());
  addElement(doc, tag, "ModuleName", wizard()->field("moduleName").toString());
  
  for (int i=0; i<params->getCount(); i++)
  {
    Parameter* p = params->getAt(i);
    QVariant v = p->getValue();
    QDomElement param = addElement(doc, tag, "Parameter", v.toString());
    param.setAttribute("name", p->getName());
  }

  return true;
}

bool WizPgTemplateParams::hasAllRequiredParameters() const
{
  bool retValue = true;
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardTemplate* wizTempl = wiz->getTemplate();
  Parameters* params = wizTempl->getContext()->getTemplate()->getParameters();
  for (int i=0; i<params->getCount(); i++)
  {
    Parameter* p = params->getAt(i);

    qDebug() << "paramCheck" << p->getName() << p->getType();

    QString validExpr = p->getValidExpression();

    if (p->getRequired() && !p->hasValueSet())
      return false;

    // Test against expression
    if (validExpr.length() > 0)
    {
      QRegExp rx(validExpr);
      QString value = p->getValue().toString();
      if (rx.indexIn(value) == -1)
        return false;
    }
  }

  if (ui.lineEditModuleName->text().length() == 0)
  {
    return false;
  }
  else if (ui.lineEditModuleName->text().contains(' '))
  {
    return false;
  }

  return retValue;
}

// isCompleted is called during initialize and 
// also when hitting 'back'
bool WizPgTemplateParams::isComplete() const
{
  return hasAllRequiredParameters();
}


// A parameter has been edited
void WizPgTemplateParams::valuesChanged()
{
  emit completeChanged();
}

void WizPgTemplateParams::initializePage()
{
  setTitle("Template Specific Options");
  setSubTitle("Configure this templates options, and select a module name for this model.");

  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardTemplate* templ = wiz->getTemplate();

  populateModes(templ);

  Parameters* params = templ->getContext()->getTemplate()->getParameters();

  QString replayFile = wiz->getReplayFile();
  if (replayFile.length() > 0)
  {
    QDomElement wizElem = wiz->findWizardStep(objectName());
    if (!wizElem.isNull())
    {
      bool performNext = false;
      QDomNode n = wizElem.firstChild();
      while(!n.isNull())
      {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) 
        {
          qDebug() << e.tagName();
          if (e.tagName() == "PortMode")
          {
            QString portMode = e.text();
            int index = ui.comboBoxMode->findText(portMode);
            ui.comboBoxMode->setCurrentIndex(index);
          }
          if (e.tagName() == "ModuleName")
          {
            QString modName = e.text();
            ui.lineEditModuleName->setText(modName);
          }
          else if (e.tagName() == "Parameter")
          {
            QString paramName = e.attribute("name");
            QString paramValue = e.text();
            Parameter* param = params->findParam(paramName);
            if (param)
            {
              param->setValue(paramValue);
              performNext = true;
            }
            else
              performNext = false;
          }
        }
        n = n.nextSibling();
      }
      if (performNext)
        wiz->postNext();
    }
  }

   ui.treeWidget->populate(templ->getContext()->getTemplate()->getParameters());
}

void WizPgTemplateParams::populateModes(WizardTemplate* wt)
{
  Template* templ = wt->getContext()->getTemplate();
  ui.comboBoxMode->clear();

  PortConfigurations* portConfigs = templ->getPortConfigs();
  for (int i=0; i<portConfigs->getCount(); i++)
  {
    PortConfiguration* portConfig = portConfigs->getAt(i);
    Modes* modes = portConfig->getModes();
    for (int m=0; m<modes->getCount(); m++)
    {
      Mode* mode = modes->getAt(m);
      ui.comboBoxMode->addItem(mode->getName());
    }
  }
}

int WizPgTemplateParams::nextId() const
{
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardContext* context = wiz->getTemplate()->getContext();
  Template* templ = context->getTemplate();

  QString modeName = wiz->field("portMode").toString();
  Mode* mode = templ->findMode(modeName);

  if (mode && mode->getParameters()->getCount() > 0)
    return MemoryWizard::PageModeParameters;
  else if (templ->getPortConfigs()->getCount() > 0)
    return MemoryWizard::PageUserPorts;
  else
    return MemoryWizard::PageUserPorts;
}
