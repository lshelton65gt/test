// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef __MdiOnDemandTrace_h__
#define __MdiOnDemandTrace_h__

#include "util/CarbonPlatform.h"
#include "Mdi.h"

class CarbonMakerContext;
class MDIRuntimeTemplate;

class MDIOnDemandTraceTemplate : public MDIDocumentTemplate
{
public:
  MDIOnDemandTraceTemplate(CarbonMakerContext *ctx);
 
  virtual MDIWidget* createNewDocument(QWidget* parent=0);
  virtual int createMenus();
  virtual int createToolbars();
  virtual void updateMenusAndToolbars(QWidget* widget);
  virtual MDIWidget* openDocument(QWidget* parent, const char* docName);

  void putRuntimeTemplate(MDIRuntimeTemplate* rt)
  {
    mRuntimeTemplate = rt;
  }
private:
  CarbonMakerContext *mContext;
  MDIRuntimeTemplate* mRuntimeTemplate;
};

#endif
