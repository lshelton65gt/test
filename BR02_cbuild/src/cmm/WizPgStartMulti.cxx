//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "MemoryWizard.h"
#include "WizPgStartMulti.h"
#include "cmm.h"
#include "WorkspaceModelMaker.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"
#include "DesignXML.h"

  //CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  //CarbonProject* proj = pw->project();


WizPgStartMulti::WizPgStartMulti(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);
  
  mIsValid = true;

  setTitle("Module Selection");
  setSubTitle("The list of Modules to be remodeled can either come from the design associated with this project, or by finding files in the specified directory.  In order to proceed choose a search method and click Apply");

  QStringList labels;
  labels << "Module"; // colNAME
  labels << "Locator";

  mDesignHierarchy = NULL;

  ui.treeWidget->setHeaderLabels(labels);
  ui.treeWidget->setRootIsDecorated(false);

  ui.treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
  ui.treeWidget->setSelectionBehavior(QAbstractItemView::SelectRows);

  ui.treeWidget->header()->setResizeMode(0, QHeaderView::Interactive);
  ui.treeWidget->header()->setResizeMode(1, QHeaderView::Interactive);
}

bool WizPgStartMulti::isComplete() const
{
  return mIsValid;
}

void WizPgStartMulti::on_radioButtonProject_clicked()
{
  ui.treeWidget->clear();
  updateButtons();
}
void WizPgStartMulti::on_radioButtonSearchDir_clicked()
{
  ui.treeWidget->clear();
  updateButtons();
}

void WizPgStartMulti::updateButtons()
{
  ui.lineEditDir->setEnabled(ui.radioButtonSearchDir->isChecked());
  ui.pushButtonBrowse->setEnabled(ui.radioButtonSearchDir->isChecked());
  ui.checkBoxRecurse->setEnabled(ui.radioButtonSearchDir->isChecked());

  ui.lineEditSingleFile->setEnabled(ui.radioButtonSingleFile->isChecked());
  ui.pushButtonBrowseFile->setEnabled(ui.radioButtonSingleFile->isChecked());

  ui.comboBoxFileKinds->setEnabled(ui.radioButtonSearchDir->isChecked());

  ui.pushButtonCheckAll->setEnabled(ui.treeWidget->topLevelItemCount() > 0);
  ui.pushButtonUncheckAll->setEnabled(ui.treeWidget->topLevelItemCount() > 0);

  validatePage();
}

WizPgStartMulti::~WizPgStartMulti()
{
}

void WizPgStartMulti::applyFilter()
{
  QApplication::setOverrideCursor(Qt::WaitCursor);
  
  bool oldValue = ui.treeWidget->blockSignals(true);

  ui.treeWidget->clear();

  if (mDesignHierarchy)
  {
    XInterfaces* ifaces = mDesignHierarchy->getAllInterfaces();
    for (int i=0; i<ifaces->numInterfaces(); i++)
    {
      XInterface* iface = ifaces->getAt(i);
      QString name = iface->getName();

      qDebug() << name;

      QRegExp exp(ui.lineEditMatching->text(),
        ui.checkBoxCaseSensitive->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive,
        ui.radioButtonWildcard->isChecked() ? QRegExp::Wildcard : QRegExp::RegExp);
  
      // Inclusion filter?
      if (ui.radioButtonWildcard->isChecked() ? exp.exactMatch(name) : exp.indexIn(name) != -1)
      {
        // Exclusion filter?
        if (!ui.lineEditExcluding->text().isEmpty())
        {
          QRegExp exp2(ui.lineEditExcluding->text(), 
            ui.checkBoxCaseSensitiveExcl->isChecked() ? Qt::CaseSensitive : Qt::CaseInsensitive,
              ui.radioButtonWildcardExcl->isChecked() ? QRegExp::Wildcard : QRegExp::RegExp);
          if (ui.radioButtonWildcardExcl->isChecked() ? exp2.exactMatch(name) : exp2.indexIn(name) != -1)
            continue;
        }

        QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidget);
        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
        item->setText(0, iface->getName());
        item->setText(1, iface->getStart());
        item->setCheckState(0, Qt::Checked);
      }
    }
  }

  ui.treeWidget->sortItems(0, Qt::AscendingOrder);

  ui.treeWidget->header()->resizeSections(QHeaderView::ResizeToContents);

  validatePage();

  ui.treeWidget->blockSignals(oldValue);

  updateButtons();

  QApplication::restoreOverrideCursor();
}

#define SETTING_SEARCH_PROJECT "wizardPageStartMulti/searchProject"
#define SETTING_SEARCH_FILE "wizardPageStartMulti/searchFile"
#define SETTING_SEARCH_DIR "wizardPageStartMulti/searchDirectory"
#define SETTING_LAST_TEMPLATE "wizardPageStartMulti/lastTemplate"
#define SETTING_LAST_DIR "wizardPageStartMulti/lastDir"
#define SETTING_LAST_FILE "wizardPageStartMulti/lastFile"
#define SETTING_MATCHING "wizardPageStarMulti/matching"
#define SETTING_MATCHING_WILDCARD "wizardPageStarMulti/matchWildcard"
#define SETTING_MATCHING_CASE "wizardPageStarMulti/matchCase"
#define SETTING_EXCLUDING "wizardPageStarMulti/excluding"
#define SETTING_EXCLUDING_WILDCARD "wizardPageStarMulti/excludeWildcard"
#define SETTING_EXCLUDING_CASE "wizardPageStarMulti/excludeCase"
#define SETTING_TEMPLATE "wizardPageStartMulti/templateName"

void WizPgStartMulti::restoreSettings()
{
  bool searchProj = mSettings.value(SETTING_SEARCH_PROJECT, true).toBool();
  bool searchFile = mSettings.value(SETTING_SEARCH_FILE, false).toBool();
  bool searchDir = mSettings.value(SETTING_SEARCH_DIR, false).toBool();

  ui.radioButtonProject->setChecked(searchProj);
  ui.radioButtonSingleFile->setChecked(searchFile);
  ui.radioButtonSearchDir->setChecked(searchDir);

  ui.lineEditDir->setText(mSettings.value(SETTING_LAST_DIR, "").toString());

  bool matchWildcard = mSettings.value(SETTING_MATCHING_WILDCARD, true).toBool();
  ui.radioButtonWildcard->setChecked(matchWildcard);
  ui.radioButtonRegEx->setChecked(!matchWildcard);
  ui.checkBoxCaseSensitive->setChecked(mSettings.value(SETTING_MATCHING_CASE, true).toBool());
  ui.lineEditMatching->setText(mSettings.value(SETTING_MATCHING, "*").toString());
  
  bool excludeWildcard = mSettings.value(SETTING_EXCLUDING_WILDCARD, true).toBool();
  ui.radioButtonWildcardExcl->setChecked(excludeWildcard);
  ui.radioButtonRegExExcl->setChecked(!excludeWildcard);
  ui.checkBoxCaseSensitiveExcl->setChecked(mSettings.value(SETTING_EXCLUDING_CASE, true).toBool());
  ui.lineEditExcluding->setText(mSettings.value(SETTING_EXCLUDING, "").toString());

  ui.lineEditSingleFile->setText(mSettings.value(SETTING_LAST_FILE, "").toString());

  int lastIndex = ui.comboBoxTemplates->findText(mSettings.value(SETTING_LAST_TEMPLATE, "").toString());
  if (lastIndex != -1)
    ui.comboBoxTemplates->setCurrentIndex(lastIndex);
}

void WizPgStartMulti::saveSettings()
{
  mSettings.setValue(SETTING_LAST_TEMPLATE, ui.comboBoxTemplates->currentText());

  mSettings.setValue(SETTING_SEARCH_PROJECT, ui.radioButtonProject->isChecked());
  mSettings.setValue(SETTING_SEARCH_FILE, ui.radioButtonSingleFile->isChecked());
  mSettings.setValue(SETTING_SEARCH_DIR, ui.radioButtonSearchDir->isChecked());

  mSettings.setValue(SETTING_LAST_DIR, ui.lineEditDir->text());
  mSettings.setValue(SETTING_LAST_FILE, ui.lineEditSingleFile->text());

  mSettings.setValue(SETTING_MATCHING, ui.lineEditMatching->text());
  mSettings.setValue(SETTING_MATCHING_WILDCARD, ui.radioButtonWildcard->isChecked());
  mSettings.setValue(SETTING_MATCHING_CASE, ui.checkBoxCaseSensitive->isChecked());
 
  mSettings.setValue(SETTING_EXCLUDING, ui.lineEditExcluding->text());
  mSettings.setValue(SETTING_EXCLUDING_WILDCARD, ui.radioButtonWildcardExcl->isChecked());
  mSettings.setValue(SETTING_EXCLUDING_CASE, ui.checkBoxCaseSensitiveExcl->isChecked());
}

// Hard coded for now until Elliot gives us a method
void WizPgStartMulti::loadTemplates()
{
  ui.comboBoxTemplates->addItem("Carbon Synchronous Memories");
  int i = ui.comboBoxTemplates->findText("Carbon Synchronous Memories");
  ui.comboBoxTemplates->setItemData(i, "CarbonSyncMem");
  ui.comboBoxTemplates->setCurrentIndex(i);
}

void WizPgStartMulti::initializePage()
{
  loadTemplates();

  restoreSettings();
 
  if (ui.radioButtonProject->isChecked())
    on_pushButtonApply_clicked();

  updateButtons();
}

bool WizPgStartMulti::validatePage()
{
  MemoryWizard* wiz = static_cast<MemoryWizard*>(wizard());

  mIsValid = ui.treeWidget->topLevelItemCount() > 0;

  wiz->removeAllModules();

  if (mIsValid)
  {
    mIsValid = false;
    for (int i=0; i<ui.treeWidget->topLevelItemCount(); i++)
    {
      QTreeWidgetItem* item = ui.treeWidget->topLevelItem(i);
      if (item->checkState(0) == Qt::Checked)
      {
        mIsValid = true;
        ModuleItem* mi = new ModuleItem(item->text(0), item->text(1));
        wiz->addModuleItem(mi);
      }
    }
  }
  
  emit completeChanged();

  return mIsValid;
}

bool WizPgStartMulti::checkDirExistence(const QString& dirPath)
{
  UtString dirname;
  dirname << dirPath;

#if pfWINDOWS
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  if (!proj->isMappedUnixPath(dirname.c_str()))
  {
    QMessageBox::critical(NULL, "Unmapped Location",
      QString("The Directory '%1' is not on a mapped drive, update your drive mappings first").arg(dirPath));
    return false;
  }
#else
  QDir dir(dirPath);
  if (!dir.exists())
  {
    QMessageBox::critical(NULL, "Invalid Directory",
      QString("The Directory '%1' is invalid or does not exist.").arg(dirPath));
  }
  return dir.exists();
#endif

  return true;
}
void WizPgStartMulti::on_pushButtonApply_clicked()
{
  MemoryWizard* wiz = static_cast<MemoryWizard*>(wizard());
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  int ti = ui.comboBoxTemplates->currentIndex();
  QString templateName = ui.comboBoxTemplates->itemData(ti).toString();

  wiz->setOption("template", templateName);

  saveSettings();

  if (ui.radioButtonProject->isChecked())
  {
    const char* designHierarchyFile = proj->getDesignFilePath(".designHierarchy");
    readDesignHierarchy(designHierarchyFile);
  }
  else // invoke cbuild and get the files
  {
    if (mDesignHierarchy)
      delete mDesignHierarchy;

    mDesignHierarchy = NULL;
    UtString dirpath;
    dirpath << ui.lineEditDir->text();

    if (ui.radioButtonSearchDir->isChecked())
    {
      if (checkDirExistence(proj->getUnixEquivalentPath(dirpath.c_str())))
        invokeCbuildScan();
    }
    else if (ui.radioButtonSingleFile->isChecked())
    {
      if (!ui.lineEditSingleFile->text().isEmpty())
        invokeCbuildScan();
      else
      {
        QMessageBox::critical(NULL, MODELSTUDIO_TITLE, "No filename specified");
        ui.lineEditSingleFile->setFocus();
        return;
      }
    }
  }

  applyFilter();
}

void WizPgStartMulti::invokeCbuildScan()
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QStringList kinds;
  bool recurseFlag = false;
  QString searchLocation;
  if (ui.radioButtonSearchDir->isChecked())
  {
    foreach (QString filter, ui.comboBoxFileKinds->currentText().split(";"))
      kinds << filter;
    recurseFlag = ui.checkBoxRecurse->isChecked();
    searchLocation = ui.lineEditDir->text();
  }
  else if (ui.radioButtonSingleFile->isChecked())
  {
    recurseFlag = false;
    QString filePath = ui.lineEditSingleFile->text();
    QFileInfo fi(filePath);
    searchLocation = fi.path();
    kinds << fi.fileName();
  }

  proj->writeRemodelMakefile(proj->getActive(), searchLocation, kinds, recurseFlag);

  CarbonConsole::CommandMode mode = CarbonConsole::Local;
#if pfWINDOWS
  mode = CarbonConsole::Remote;
#endif

  pw->makeConsoleVisible();

#if pfWINDOWS
  QString program = proj->remoteCommandName("cbuild");
#else
  QString program = "$(CARBON_HOME)/bin/cbuild";
#endif

  QStringList args;
  args << "-f";
  args << "Remodel.f.cmd";

  qDebug() << "invoking cbuild -scan";
  UtString subDir;
  subDir << proj->getActivePlatform() << "/" << proj->getActiveConfiguration();

  int exitCode = pw->getConsole()->executeCommandAndWait(mode, CarbonConsole::Command, program, args, subDir.c_str());
  
  qDebug() <<  "cbuild -scan finished, exitCode" << exitCode;

  QApplication::restoreOverrideCursor();

  if (exitCode == 0)
  {
    UtString dhfile;
    OSConstructFilePath(&dhfile, proj->getActive()->getOutputDirectory(), "libRemodelScanner.designHierarchy");
    QFileInfo fi(dhfile.c_str());
    if (fi.exists())
    {
      const char* designHierarchyFile = dhfile.c_str();
      readDesignHierarchy(designHierarchyFile);
    }
    else
      QMessageBox::critical(NULL, MODELSTUDIO_TITLE, "Unable to locate libRemodelScanner.designHierarchy");
  }
  else
  {
    QMessageBox::critical(NULL, MODELSTUDIO_TITLE, "The compilation finished with errors\n.See the Console output for errors, correct then try again.");
    wizard()->close();
  }
}

void WizPgStartMulti::on_pushButtonBrowse_clicked()
{
  QString directory = QFileDialog::getExistingDirectory(this,
                       tr("Select Directory Containing RTL"),
                       ui.lineEditDir->text(),
                       QFileDialog::DontResolveSymlinks | QFileDialog::ShowDirsOnly);

  if (!checkDirExistence(directory))
  {
    ui.lineEditDir->setText("");
    return;
  }

  ui.lineEditDir->setText(directory);
}

void WizPgStartMulti::readDesignHierarchy(const QString& fileName)
{
  if (mDesignHierarchy)
    delete mDesignHierarchy;

  qDebug() << "reading" << fileName;

  mDesignHierarchy = NULL;
  
  MemoryWizard* wiz = static_cast<MemoryWizard*>(wizard());
  if (wiz)
    wiz->setOption("designHierarchyFile", "");

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  if (proj)
  {
    QFileInfo fi(fileName);
    if (fi.exists())
    {
      if (wiz)
        wiz->setOption("designHierarchyFile", fileName);
      qDebug() << "parsing" << fileName;
      mDesignHierarchy = new XDesignHierarchy(fileName);
      XmlErrorHandler* eh = mDesignHierarchy->getErrorHandler();      
      if (eh->hasErrors())
      {
        QString msg = QString("Unexpected Error reading design hierarchy: %1\n%2")
          .arg(fileName)
          .arg(eh->errorMessages().join("\n"));
        QMessageBox::critical(NULL, MODELSTUDIO_TITLE, msg);
      }
    }
  }
}

void WizPgStartMulti::on_pushButtonUncheckAll_clicked()
{
  bool oldvalue = ui.treeWidget->blockSignals(true);
  for (int i=0; i<ui.treeWidget->topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = ui.treeWidget->topLevelItem(i);
    item->setCheckState(0, Qt::Unchecked);
  }
  ui.treeWidget->blockSignals(oldvalue);

  mIsValid = false;

  emit completeChanged();
}

void WizPgStartMulti::on_pushButtonCheckAll_clicked()
{
  bool oldvalue = ui.treeWidget->blockSignals(true);
  for (int i=0; i<ui.treeWidget->topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = ui.treeWidget->topLevelItem(i);
    item->setCheckState(0, Qt::Checked);
    mIsValid = true;
  }
  ui.treeWidget->blockSignals(oldvalue);

  emit completeChanged();
}


void WizPgStartMulti::on_pushButtonBrowseFile_clicked()
{
  QString startLocation = ".";
  if (ui.lineEditSingleFile->text().length() > 0)
    startLocation = ui.lineEditSingleFile->text();
  QString fileName = QFileDialog::getOpenFileName(this, "Choose RTL File", startLocation, tr("RTL File (*.v *.vhd*)"));
  if (!fileName.isEmpty())
    ui.lineEditSingleFile->setText(fileName);
}

void WizPgStartMulti::on_radioButtonSingleFile_toggled(bool)
{
  updateButtons();
}


void WizPgStartMulti::on_treeWidget_itemChanged(QTreeWidgetItem* item,int)
{
  if (item->checkState(0) == Qt::Checked)
    mIsValid = true;

  emit completeChanged();
}


void WizPgStartMulti::on_lineEditMatching_textChanged(const QString &)
{
  applyFilter();
}

void WizPgStartMulti::on_lineEditExcluding_textChanged(const QString &)
{
  applyFilter();
}

void WizPgStartMulti::on_radioButtonWildcard_toggled(bool)
{
  applyFilter();
}

void WizPgStartMulti::on_radioButtonRegEx_toggled(bool)
{
  applyFilter();
}

void WizPgStartMulti::on_checkBoxCaseSensitive_stateChanged(int)
{
  applyFilter();
}

void WizPgStartMulti::on_radioButtonRegExExcl_toggled(bool)
{
  applyFilter();
}

void WizPgStartMulti::on_checkBoxCaseSensitiveExcl_stateChanged(int)
{
  applyFilter();
}
