#ifndef __WIZARDTREENOES_H_
#define __WIZARDTREENOES_H_

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "gui/CExprValidator.h"
#include "util/DynBitVector.h"
#include "PortEditorTreeWidget.h"
#include "cfg/CarbonCfg.h"
#include "cfg/carbon_cfg.h"
#include "CompWizardPortEditor.h"
#include <QtGui>

class PortEditorDelegate;
class ESLXtorInstanceItem;
class PortEditorTreeItem;


class ESLParameterActions : public QObject
{
  Q_OBJECT

public:
  enum Action { ConnectRTL }; 

  ESLParameterActions(PortEditorTreeWidget* tree);

  QAction* getAction(Action);

private slots:
  void actionConnectRTL();

private:
  PortEditorTreeWidget* mTree;
  QAction* mConnectRTL;
};


class UndoData
{
public:
  UndoData()
  {
    mTree = NULL;
  }
  virtual ~UndoData()
  {
  }

  virtual void undo()=0;
  virtual void redo()=0;

  void setTreeData(QTreeWidget* tree, QTreeWidgetItem* node = NULL)
  {
    mTree = dynamic_cast<PortEditorTreeWidget*>(tree);
    if (node)
    {
      mParentIndex = mTree->indexFromItem(node->parent());
      mItemIndex = mTree->indexFromItem(node);
    }
  }

  void setText(const QString& newVal) { mText=newVal; }
  QString text() const { return mText; }

  PortEditorTreeWidget* getTree() { return mTree; }
  PortEditorTreeItem* getParentItem();
  PortEditorTreeItem* getItem();

private:
  QString mText;
  PortEditorTreeWidget* mTree;
  PortTreeIndex mParentIndex;
  PortTreeIndex mItemIndex;
};


class TreeUndoCommand : public QUndoCommand
{
public:
  TreeUndoCommand(PortEditorTreeItem* item, QUndoCommand* parent);

  PortEditorTreeItem* getItem();
  PortEditorTreeWidget* getTree() { return mTree; }
  void updateTreeIndex(PortEditorTreeItem* item);
  void setModified(bool)
  {
    getTree()->setModified(true);
  }
private:
  PortEditorTreeWidget* mTree;
  PortTreeIndex mTreeIndex;
  bool mInitialModified;
};


class MultiUndoCommand : public QUndoCommand
{
public:
  MultiUndoCommand(QUndoCommand* parent=0) : QUndoCommand(parent)
  {
  }

protected:
  virtual void undo()
  {
    foreach (UndoData* item, mUndoList)
    {
      if (item)
        item->undo();
    }
  }
  virtual void redo()
  {
    foreach (UndoData* item, mUndoList)
    {
      if (item)
        item->redo();
    }
  }
  void addItem(UndoData* item) { mUndoList.append(item); }

private:
  QList<UndoData*> mUndoList;
};



class PortsRootItem : public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:

  virtual bool canDropHere(const QString&, QDragMoveEvent*);
  virtual bool dropItem(const QString&);
  virtual void multiSelectContextMenu(QMenu& menu)
  {
    menu.addAction(mActionAddXtor);
  }

  PortsRootItem(QTreeWidget* parent) : PortEditorTreeItem(PortEditorTreeItem::Ports, parent)
  {
    setBoldText(0, "Component Ports");

    mActionAddXtor = new QAction(QIcon(":/cmm/Resources/addxtor.png"), tr("Add Transaction Port..."), this);
    mActionAddXtor->setToolTip("Add Transaction Port");
    mActionAddXtor->setStatusTip("Add a new Transaction Port");
    CQT_CONNECT(mActionAddXtor, triggered(), getTree()->getEditor(), actionAddXtor());

  }

private:
  QAction* mActionAddXtor;

};

class ParametersRootItem : public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:
  virtual void multiSelectContextMenu(QMenu& menu);
  ParametersRootItem(QTreeWidget* parent);

private:
  QAction* mActionAddParam;
};

class TiesRootItem : public PortEditorTreeItem
{
public:
  virtual bool dropItems(const QList<PortEditorTreeItem*>&);
  virtual bool canDropHere(const QList<PortEditorTreeItem*>&, QDragMoveEvent*);

  TiesRootItem(QTreeWidget* parent) 
    : PortEditorTreeItem(PortEditorTreeItem::Ties, parent)
  {
    setBoldText(0, "RTL Ties");
  }
};

class DisconnectsRootItem : public PortEditorTreeItem
{
public:
  virtual bool dropItems(const QList<PortEditorTreeItem*>&);
  virtual bool canDropHere(const QList<PortEditorTreeItem*>&, QDragMoveEvent*);

  DisconnectsRootItem(QTreeWidget* parent) 
    : PortEditorTreeItem(PortEditorTreeItem::Disconnects, parent)
  {
    setBoldText(0, "RTL Disconnects");
  }
};

class ResetsRootItem : public PortEditorTreeItem
{
public:
  virtual bool dropItems(const QList<PortEditorTreeItem*>&);
  virtual bool canDropHere(const QList<PortEditorTreeItem*>&, QDragMoveEvent*);

  ResetsRootItem(QTreeWidget* parent)
    : PortEditorTreeItem(PortEditorTreeItem::Resets, parent)
  {
    setBoldText(0, "Reset Generators");
  }
};

class ClocksRootItem : public PortEditorTreeItem
{
public:
  virtual bool dropItems(const QList<PortEditorTreeItem*>&);
  virtual bool canDropHere(const QList<PortEditorTreeItem*>&, QDragMoveEvent*);

  ClocksRootItem(QTreeWidget* parent)
    : PortEditorTreeItem(PortEditorTreeItem::Clocks, parent)
  {
    setBoldText(0, "Clock Generators");
  }
};

// Component Parameters tied to RTL Ports
class ESLParameterPortItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colTYPE, colSIZE } ;

  virtual UndoData* deleteItem();

  ESLParameterPortItem(QTreeWidgetItem* parent, CarbonCfgXtorParamInst* paramInst, CarbonCfgRTLPort* rtlPort) 
    : PortEditorTreeItem(PortEditorTreeItem::GlobalParameterPort, parent)
  {
    mParamInst = paramInst;
    mRTLPort = rtlPort;

    setText(colNAME, rtlPort->getName());
  }

private:
  class Delete : public UndoData
  {
  public:
    CarbonCfg* mCfg;
    QString mParamName;
    QString mRTLPortName;
    PortTreeIndex mESLPortIndex;
    QString mESLPortName;

  protected:
    void undo();
    void redo();
  };


private:
  CarbonCfgXtorParamInst* mParamInst;
  CarbonCfgRTLPort* mRTLPort;
};

class ParameterValueItem : public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:
  enum ParamType { eTypeUNKNOWN, eTypeXTOR_PARAM, eTypeGLOBAL_PARAM } ;

  enum Columns { colNAME = 0, colVALUE} ;
  ParameterValueItem(ParamType paramType, QTreeWidgetItem* parent)
    : PortEditorTreeItem(PortEditorTreeItem::GlobalParameterValue, parent)
  {
    mParamType = paramType;
  }

  void updateESLReferences(const QString& value);

  void setValue(const QString& value, bool select = false)
  {
    highlightValue(colVALUE, value, getDefaultValue());
    updateESLReferences(value);
    if (select)
    {
      treeWidget()->scrollToItem(this);
      treeWidget()->setCurrentItem(this);
    }
  }
  
  void setValue(bool newVal, bool select = false)
  {
    highlightValue(colVALUE, newVal ? "true" : "false", getDefaultValue());
    updateESLReferences(newVal ? "true" : "false");
    if (select)
    {
      treeWidget()->scrollToItem(this);
      treeWidget()->setCurrentItem(this);
    }
  }

  void setValue(quint32 newVal, bool select = false)
  {
    QString value = QString("%1").arg(newVal);
    updateESLReferences(value);
    highlightValue(colVALUE, value, getDefaultValue());
    if (select)
    {
      treeWidget()->scrollToItem(this);
      treeWidget()->setCurrentItem(this);
    }
  }

  void setValue(quint64 newVal, bool select = false)
  {
    QString value = QString("%1").arg(newVal);
    updateESLReferences(value);
    highlightValue(colVALUE, value, getDefaultValue());
    if (select)
    {
      treeWidget()->scrollToItem(this);
      treeWidget()->setCurrentItem(this);
    }
  }

  void setValue(double newVal, bool select = false)
  {
    QString value = QString("%1").arg(newVal);
    updateESLReferences(value);
    highlightValue(colVALUE, value, getDefaultValue());
    if (select)
    {
      treeWidget()->scrollToItem(this);
      treeWidget()->setCurrentItem(this);
    }
  }

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);
  virtual CarbonCfgXtorParamInst* getParamInst() const { INFO_ASSERT(true, "Override"); return NULL; }
  virtual QString getDefaultValue() const { return QString(); }
  virtual QString getEnumChoices() const { return QString(); }

  ParamType getType() const { return mParamType; }

private:
  ParamType mParamType;

};

class ESLParameterValueItem : public ParameterValueItem
{
public:
  enum Columns { colNAME = 0, colVALUE} ;

  virtual QString getDefaultValue() const { return mParamInst->getDefaultValue(); }
  virtual CarbonCfgXtorParamInst* getParamInst() const { return mParamInst; }

  ESLParameterValueItem(QTreeWidgetItem* parent, CarbonCfgXtorParamInst* paramInst)
    : ParameterValueItem(ParameterValueItem::eTypeGLOBAL_PARAM, parent)
  {
    mParamInst = paramInst;
    setFlags(flags() | Qt::ItemIsEditable);
    setItalicText(colNAME, "Value");  
    QString value = mParamInst->getValue();
    if (value.isEmpty())
      value = mParamInst->getDefaultValue();
    setValue(value);
  }


private:
  CarbonCfgXtorParamInst* mParamInst;
};

// Global Parameters
class ESLParameterItem : public PortEditorTreeItem
{
public:
  // here we define which column we want the parameter information to appear
  enum Columns { colNAME = 0, colTYPE, colDESCR, colENUMCHOICES } ;

  virtual UndoData* deleteItem();
  virtual void multiSelectContextMenu(QMenu&);

  static void resetData()
  {
    mActions = NULL;
  }
  
  CarbonCfgXtorParamInst* getParamInst() { return mParamInst; }

  ESLParameterActions* getActions() 
  {
    if (mActions == NULL)
      mActions = new ESLParameterActions(getTree());

    return mActions;
  }

  ESLParameterItem(QTreeWidgetItem* parent, CarbonCfgXtorParamInst* paramInst)
    : PortEditorTreeItem(PortEditorTreeItem::GlobalParameter, parent)
  {
    mParamInst = paramInst;
    CarbonCfgXtorParam* xtorParam = mParamInst->getParam();

    CarbonCfgParamFlagIO flag(xtorParam->getFlag());
    CarbonCfgParamDataTypeIO dataType(xtorParam->getType());


    INFO_ASSERT(xtorParam, "Expecting XtorParam");

    setText(colNAME, xtorParam->getName());
    setIcon(colNAME, QIcon(":/cmm/Resources/parameter.png"));

    QString kind;
    switch (xtorParam->getFlag())
    {
    case eCarbonCfgXtorParamRuntime:  kind = "Run-time"; break;
    case eCarbonCfgXtorParamCompile:  kind = "Compile-time"; break;
    case eCarbonCfgXtorParamBoth:     kind = "Both"; break;
    case eCarbonCfgXtorParamInit:     kind = "Init-time"; break;
    }

    QString typeString = QString("%1 (%2)")
      .arg(dataType.getString())
      .arg(kind);

    setText(colTYPE, typeString);
    setText(colDESCR, xtorParam->getDescription());
    setText(colENUMCHOICES, xtorParam->getEnumChoices());

    mValueItem = new ESLParameterValueItem(this, paramInst);

    // Global Parameters
    for (UInt32 i=0; i<mParamInst->numRTLPorts(); i++)
    {
      CarbonCfgRTLPort* rtlPort = mParamInst->getRTLPort(i);
      new ESLParameterPortItem(this, paramInst, rtlPort);
    }

    setExpanded(true);
  }

public:
  class ConnectRTL : public TreeUndoCommand
  {
  public:
    ConnectRTL(ESLParameterItem* item, const QStringList& rtlPorts, QUndoCommand* parent = 0);

    CarbonCfg* mCfg;
    QStringList mRTLPorts;
    QList<PortTreeIndex> mPortIndexes;
    QMap<QString, QStringList> mRemovedESLPorts;

  protected:
    void undo();
    void redo();
  };

  class AddParameter : public TreeUndoCommand
  {
  public:
    AddParameter(ParametersRootItem* item, 
      CarbonCfgParamDataType type, const QString& name, const QString& value,
      const QString& description, const QString& enumChoices, CarbonCfgParamFlag scope, QUndoCommand* parent = 0);

    CarbonCfg* mCfg;
    CarbonCfgParamDataType mType;
    QString mName;
    QString mValue;
    QString mDescription;
    QString mEnumChoices;
    CarbonCfgParamFlag mScope;

  protected:
    void undo();
    void redo();
  };


private:
  class DeleteConn : public UndoData
  {
  public:
    CarbonCfg* mCfg;
    QString mParamName;
    QString mRTLPortName;
    QString mESLPortName;
    PortTreeIndex mESLPortIndex;

    void undo();
    void redo();
  };

  class Delete : public UndoData
  {
  public:
    CarbonCfg* mCfg;
    QString mParamName;
    QString mParamValue;
    QList<DeleteConn*> mConnections;
    int mParamSize;
    CarbonCfgParamDataType mParamType;
    CarbonCfgParamFlag mParamFlag;
    QString mParamDescription;
    QString mParamEnumChoices;
    QString mDefaultValue;
    bool mExpanded;
    QList<PortTreeIndex> mESLPorts;

  protected:
    void undo();
    void redo();
  };
private:
  static ESLParameterActions* mActions;
  CarbonCfgXtorParamInst* mParamInst;
  ESLParameterValueItem* mValueItem;
};


class ESLTieValueItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colVALUE };

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  void setTieValue(const QString& newVal)
  {
    setText(colVALUE, newVal);
  }

  ESLTieValueItem(QTreeWidgetItem* parent, CarbonCfgTie* tie) 
    : PortEditorTreeItem(PortEditorTreeItem::TieValue, parent)
  {
    mTie = tie;
    const DynBitVector* bv = &mTie->mValue;
    UtString valStr;
    bv->format(&valStr, eCarbonHex);
    setItalicText(colNAME, "Tied Value");   
    QString value = QString("0x%1").arg(valStr.c_str());
    setTieValue(value);
    setFlags(flags() | Qt::ItemIsEditable);
  }

private:
  CarbonCfgTie* mTie;
};

class ESLTieItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colRTL, colSIZE} ;

  virtual UndoData* deleteItem();

  ESLTieItem(QTreeWidgetItem* parent, CarbonCfgTie* tie)
    : PortEditorTreeItem(PortEditorTreeItem::Tie, parent)
  {
    mTie = tie;
    CarbonCfgRTLPort* rtlPort = mTie->getRTLPort();

    setIcon(colNAME, QIcon(":/cmm/Resources/PushpinHS.png"));

    if (rtlPort)
    {
      QString value = QString("%1").arg(rtlPort->getWidth());
      setText(colNAME, rtlPort->getName());
      setText(colSIZE, value);

      mTieValue = new ESLTieValueItem(this, tie);

      setExpanded(true);
    }
  }

  // Remove a tie
  static CarbonCfgRTLPort* removeTie(PortEditorTreeItem* item, const QString& eslPortName);

  // Create a new item
  static ESLTieItem* createTie(PortEditorTreeWidget* tree,
        UndoData* undoData, CarbonCfgRTLPort* rtlPort, UInt32 tieValue);


private:
  class Delete : public UndoData
  {
  public:
    QString mRTLPortName;
    QString mESLPortName;
    PortTreeIndex mESLPortIndex;
    CarbonCfg* mCfg;
    DynBitVector mTieValue;

  protected:
    void undo();
    void redo();
  };

private:
  ESLTieValueItem* mTieValue;
  CarbonCfgTie* mTie;
};

class ESLDisconnectItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colSIZE, colTYPE} ;

  virtual UndoData* deleteItem();

  ESLDisconnectItem(QTreeWidgetItem* parent, CarbonCfgRTLPort* rtlPort) 
    : PortEditorTreeItem(PortEditorTreeItem::Disconnect, parent)
  {
    mRTLPort = rtlPort;
    if (rtlPort)
    {
      setText(colNAME, rtlPort->getName());
      setIcon(colNAME, QIcon(":/cmm/Resources/unconnected.png"));

      CarbonCfgRTLPortTypeIO dir(rtlPort->getType());
      QString value = QString("%1").arg(rtlPort->getWidth());
      setText(colSIZE, value);
      setText(colTYPE, dir.getString());
      setIcon(colSIZE, icon(rtlPort->getType()));
    }
  }

  CarbonCfgRTLPort* getRTLPort() const { return mRTLPort; }

private:
  class Delete : public UndoData
  {
  public:
    QString mRTLPortName;
    QString mESLPortName;
    CarbonCfg* mCfg;
    PortTreeIndex mESLPortIndex;
    PortEditorTreeWidget* mTree;

  protected:
    void undo();
    void redo();
  };

private:
  CarbonCfgRTLPort* mRTLPort;
};



#endif
