#ifndef __CMSCOMMON_H__
#define __CMSCOMMON_H__

#include "util/CarbonPlatform.h"
#include "util/XmlParsing.h"
#include "util/CarbonAssert.h"
#include <QtGui>

class MessageCapture
{
public:
  enum Capture { Unknown, Source, LineNumber, Reference, Severity, Message, ErrorNumber };

  MessageCapture() 
  {
    mType = Unknown;
  }
  static MessageCapture* parseXML(xmlNodePtr parent);

  Capture getType() { return mType; }
  const char* getCapture() { return mCapture.c_str(); }
  void putCapture(const char* newVal) { mCapture=newVal; }
  UInt32 getCaptureIndex() { return mCaptureIndex; }

private:
  static Capture convertCapture(const char* sev);

private:
  Capture mType;
  UtString mCapture;
  UInt32 mCaptureIndex;
};

class MessageExpression
{
public:
  MessageExpression()
  {
    mSeverity = Unknown;
    mNumCaptures = 0;
  }
  virtual ~MessageExpression()
  {
    while (mCaptures.count() > 0)
      delete mCaptures.takeFirst();
  }

  enum Severity { Unknown, Information, Warning, Error };
  enum MessageType { Generic, CompileError, MakefileError, SourceHyperlink };
  enum Navigation { None, Source, SourceLine };

public:
  bool process(const char* line);
  static MessageExpression* parseXML(xmlNodePtr parent);
  const char* getCapturedValue(MessageCapture::Capture captureType);
  int getCapturedValueInt(MessageCapture::Capture captureType);
  const char* getFormattedMessage();
  void putFormat(const char* newVal) { mFormat=newVal; }

  Navigation getNavigation() { return mNavigation; }
  MessageType getType() { return mType; }
  Severity getSeverity();
  MessageCapture* getCapture(int index)
  {
    INFO_ASSERT(index < mCaptures.count(), "Index out of range");
    return mCaptures[index];
  }
  const char* getNavigationLocator();
  UInt32 numCaptures() { return mCaptures.count(); }

private:
  static Severity convertSeverity(const char* sev, bool validate=true);
  static MessageType convertMessage(const char* sev);
  static Navigation convertNavigation(const char* sev);

private:
  Severity mSeverity;
  MessageType mType;
  Navigation mNavigation;
  UInt32 mNumCaptures;
  QList<MessageCapture*> mCaptures;
  UtString mNavigationLocator;
  QRegExp mRegExp;
  UtString mFormat;
};

// The Matched message
class ErrorMessage
{
public:
  ErrorMessage(const QString& msg, MessageExpression* exp);

  virtual ~ErrorMessage()
  {
  }

  MessageExpression::Severity getSeverity() const;
  const char* getMessage() const { return mMessage.c_str(); }
  const char* getRawMessage() const { return mRawMsg.c_str(); }
  int getLineNumber() const { return mLineNumber; }
  const char* getSource() const { return mSource.c_str(); }
  const char* getReference() const { return mReference.c_str(); }
  int getMessageNumber() const { return mMessageNumber; }
  const char* getWorkingDir() const { return mWorkingDir.c_str(); }
  const char* getAlternate() const { return mAlternate.c_str(); }

  void putWorkingDir(const char* newValue) { mWorkingDir = newValue; }
  void putAlternate(const char* newValue) { mAlternate = newValue; }

private:
  UtString mMessage;
  MessageExpression* mExp;
  int mLineNumber;
  UtString mSource;
  UtString mReference;
  UtString mRawMsg;
  int mMessageNumber;
  UtString mWorkingDir;
  UtString mAlternate;
};

#endif
