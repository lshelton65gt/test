#ifndef __QTextEditor_H
#define __QTextEditor_H
#include "util/CarbonPlatform.h"

#include <QTextEdit>
#include <QMenuBar>
#include <QSyntaxHighlighter>
#include <QFileSystemWatcher>

#include <Qsci/qsciscintilla.h>
#include <Qsci/qscilexervhdl.h>

#include "qscilexerverilog.h"

#include "Scripting.h"

#include "util/UtString.h"
#include "Mdi.h"
#include "MdiTextEditor.h"
#include "DlgFindText.h"

class DlgPreferences;
class MDIDocumentTemplate;

class QTextEditor : public QsciScintilla, public MDIWidget
{
  Q_OBJECT

public:
  QTextEditor(MDIDocumentTemplate* doct=0, QWidget* parent=0);

  virtual void saveDocument();
  virtual void widgetActivated();
  void gotoLine(UInt32 line);
  void fixedWidth();
  void newFile();
  bool loadFile(const char* fileName);
  bool save();
  bool saveAs();
  bool saveFile(const char* fileName);
  const char* userFriendlyCurrentFile();
  const char* currentFile() { return mCurFile.c_str(); }
  void updateMenusAndToolbars();
  bool getYesToAll() { return mYesToAll; }
  void setYesToAll(bool newVal) { mYesToAll=newVal; }
  void refreshFile();
  void setCurrentFile(const char* newVal);
  void setModified(bool value) 
  {
    setWindowModified(value);
    QsciScintilla::setModified(value);
  }
  QMenuBar* menuBar();
  void setNewFile(const char* fileName);

protected:
  void closeEvent(QCloseEvent *event);
  const char* userFriendlyName()
  {
    return mCurFile.c_str();
  }

private slots:
  void documentWasModified();
  void cut() {QsciScintilla::cut();}
  void copy() {QsciScintilla::copy();}
  void paste() {QsciScintilla::paste();}
  void fileSave();
  void find();
  void updateUndoStatus();
  void updateRedoStatus();
  void findDialogDestroyed();
  void fileChangedOnDisk(const QString&);
  void preferenceChanged(const QString& prefName, const QVariant& value);
  void gotoLineDialog();
  void editorSettingsChanged(DlgPreferences* prefs);
  void languageSettingsChanged(const QString& language);
  void shiftTab();
  void runScript();
  void debugScript();
  void toggleBreakpoint();
  void deleteBreakpoints();
  void setStatusBarCursorPosInfo(int line, int col);
  void scriptPositionChanged(int lineNumber, int columnNumber);
  void scriptException(const QScriptValue &exception, int lineNumber);
  void selectionChanged();

private:
  QSyntaxHighlighter* mHighlighter;
  QFileSystemWatcher* mWatcher;

private:
  int mFileChangedCount;
  bool maybeSave();
  void setFontSize(QsciLexer* lex, int size=-1);
  const char* strippedName(const char* name);

  void setupLexer(const QString& fileName);
  void setupLexer(const char* filename);
  void watchFile(const QString& fileName);
  void unwatchFile(const QString& fileName);

  UtString mCurFile;
  bool isUntitled;
  QMenuBar* mMenu;
  UtArray<UInt32> mLinePositionVec;

  int mMarker;
  int mMarkerBP;
  static DlgFindText* mDlgFind;
  bool mYesToAll;

  QString mFileName;
  bool mSaveInProgress;

  // defaults
  int mDefMargin0Width;
  int mDefMargin1Width;
  int mDefMargin2Width;

  bool mIsJavaScript;
  bool mBreakpointsModified;
  static QLabel* mTextEditLineColumnInfoLabel;

signals:
  void pasteAvailable(bool);
  void undoAvailable(bool);
  void redoAvailable(bool);
  void scriptLoaded(bool);

};

#endif
