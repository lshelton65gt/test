/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
*******************************************************************************/
#include "util/CarbonPlatform.h"
#include <QString>
#include "PackageToolWizardWidget.h"
#include "DlgAddFileToPackage.h"
#include "PackageOptionsItem.h"
#include "PackageComponent.h"
#include "ModelStudioCommon.h"
#include "CarbonProjectWidget.h"

PackageQTreeWidgetItem::PackageQTreeWidgetItem(PackageTreeItem* item,
                                               QTreeWidget* widget,
                                               PackageToolWizardWidget* parent) : QTreeWidgetItem(),
  mItem(item),
  mWidget(widget),
  mParent(parent),
  mActionDelete(0),
  mActionRename(0),
  mActionAddFile(0),
  mActionAddDir(0)
{
  setText(0, item->name());
  if (item->isFile()) {
    setText(1, item->filePath());
    setToolTip(1, item->filePath());
  }
  // Setup allowed actions

  // User files/directories or component files can be renamed but not component directories.
  mActionRename = new QAction(widget);
  mActionRename->setText("Rename");
  if(item->isUser() || item->isFile()) {
    mActionRename->setEnabled(true);
    mActionRename->setStatusTip("Rename the selected file or directory.");
  }
  else {
    mActionRename->setStatusTip("Can not rename component directories.");
    mActionRename->setEnabled(false);
  }
  CQT_CONNECT(mActionRename, triggered(), this, renameMe());

  // Only user added files or directories can be deleted
  mActionDelete = new QAction(widget);
  mActionDelete->setText("Delete");
  if(item->isUser()) {
    mActionDelete->setEnabled(true);
    mActionDelete->setStatusTip("Delete the selected file or directory.");
  }
  else {
    mActionDelete->setStatusTip("Can not delete component files or directories.");
    mActionDelete->setEnabled(false);
  }
  CQT_CONNECT(mActionDelete, triggered(), this, deleteMe());

  if(item->isDir() || item->isRoot()) {
    mActionAddFile = new QAction(widget);
    mActionAddFile->setText("Add File");
    mActionAddFile->setStatusTip("Add a file to the package at this position.");
    connect(mActionAddFile, SIGNAL(triggered()), this, SLOT(addFile()));

    mActionAddDir = new QAction(widget);
    mActionAddDir->setText("Add Directory");
    mActionAddDir->setStatusTip("Add a directory to the package at this position.");
    CQT_CONNECT(mActionAddDir, triggered(), this, addDirectory());
  }

  // Directory items can be edited
  if(item->isDir())
    setFlags(flags() | Qt::ItemIsEditable);

  // Set Item Icon
  if(item->isDir())
    setIcon(0, QIcon(":/cmm/Resources/folder-open-16x16.png"));
  else if(item->isFile())
    setIcon(0, QIcon(":/cmm/Resources/file-16x16.png"));
}

void PackageQTreeWidgetItem::showContextMenu(const QPoint& pos)
{
  QMenu menu;
  if(mItem->isDir() || mItem->isRoot()) {
    menu.addAction(mActionAddFile);
    menu.addAction(mActionAddDir);
  }
  
  if(mItem->isDir() || mItem->isFile()) {
    menu.addAction(mActionRename);
    menu.addAction(mActionDelete);
  }

  menu.setEnabled(true);
  menu.exec(pos);
}

void PackageQTreeWidgetItem::updateTreeItem(int col)
{
  // Update underlying PackageTreeItem if the widget item changed
  if(col == 0) {
    UtString name;
    name << text(col);
    mItem->setName(name.c_str());
  }
}

void PackageQTreeWidgetItem::renameMe()
{
  // Let user edit me!
  mWidget->editItem(this, 0);
}

void PackageQTreeWidgetItem::deleteMe()
{
  // First Delete Package item
  mItem->parent()->removeChild(mItem);

  // Clear mItem, so it will not be accessed by misstake
  mItem = 0;

  // If parent is NULL, this is a top level item and we must'
  // be removed as such
  QTreeWidgetItem* qTreeParent = QTreeWidgetItem::parent();
  if (qTreeParent)
    qTreeParent->removeChild(this);
  else {
    int index = mWidget->indexOfTopLevelItem(this);
    mWidget->takeTopLevelItem(index);
  }
  // This is unusual, but since we're not executing any more
  // code after the delete, I hope it should be ok.
  delete this;
}

void PackageQTreeWidgetItem::addFile()
{
  // Execute "Add File" dialog.
  DlgAddFileToPackage dlg(mParent);
  DlgAddFileToPackage::Result result 
    = (DlgAddFileToPackage::Result)dlg.exec();
  
  if(result == DlgAddFileToPackage::OK) {   
    // Make file relative to the configuration directory
    UtString filePath;
    filePath << CarbonProjectWidget::makeRelativePath(mParent->getProject()->getActive()->getOutputDirectory(), dlg.filePath());

    UtString fileName;
    fileName << dlg.fileName();

    // First Create a new PackageTreeItem
    PackageTreeFile* newItem = new PackageTreeFile(fileName.c_str(), filePath.c_str(), "User", mItem);
    mItem->addChild(newItem);

    // Then add it to the QTreeWidget
    PackageQTreeWidgetItem* widgetItem = new PackageQTreeWidgetItem(newItem, mWidget, mParent);
    if(mItem->isRoot())
      mWidget->addTopLevelItem(widgetItem);
    else
      addChild(widgetItem);
  
    qDebug() << "PackageQTreeWidget::addFile(): " << fileName.c_str() << "Path: " << filePath.c_str();

    // Expand directory after file is added so the user can see the result
    mWidget->expandItem(this);
  }
}

void PackageQTreeWidgetItem::addDirectory()
{
  // First Add the directory, then let the user rename it.
  PackageTreeDir* newItem = new PackageTreeDir("New Directory", "User", mItem);
  mItem->addChild(newItem);

  // Add it to the QTreeWidget
  PackageQTreeWidgetItem* widgetItem = new PackageQTreeWidgetItem(newItem, mWidget, mParent);
  if(mItem->isRoot())
    mWidget->addTopLevelItem(widgetItem);
  else
    addChild(widgetItem);
  
  // Now let the user edit the directory name
  widgetItem->renameMe();

  qDebug() << "PackageQTreeWidget::addDirectory()";
}

PackageToolWizardWidget::PackageToolWizardWidget(CarbonMakerContext* ctx, MDIDocumentTemplate* t, QWidget *parent) : 
  QWidget(parent),
  mCtx(ctx),
  mFriendlyName("Package Options"),
  mComp(NULL),
  mRootQItem(NULL) 
{
  ui.setupUi(this);
  ui.treeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
  initializeMDI(this, t);
  setAttribute(Qt::WA_DeleteOnClose);

  setWindowTitle(mFriendlyName.c_str());
  initialize();

  CarbonProject* proj = mCtx->getCarbonProjectWidget()->project();
  CQT_CONNECT(mCtx->getCarbonProjectWidget()->project(), projectLoaded(const char*, CarbonProject*), this, projectLoaded(const char*, CarbonProject*));
  CQT_CONNECT(proj, configurationChanged(const char*), this, configurationChanged(const char*));
  CQT_CONNECT(proj, configurationRenamed(const char*, const char*), this, configurationRenamed(const char*, const char*));
  CQT_CONNECT(proj, configurationCopy(const char*, const char*), this, configurationCopy(const char*, const char*));
  CQT_CONNECT(proj, configurationRemoved(const char*), this, configurationRemoved(const char*));
  CQT_CONNECT(proj, configurationAdded(const char*), this, configurationAdded(const char*));
  CQT_CONNECT(proj, projectLoaded(const char*,CarbonProject*), this, projectLoaded(const char*,CarbonProject*));
  CQT_CONNECT(proj, componentAdded(CarbonComponent*), this, componentAdded(CarbonComponent*));
  CQT_CONNECT(proj, componentDeleted(CarbonComponent*), this, componentDeleted(CarbonComponent*));
  CQT_CONNECT(ui.treeWidget, customContextMenuRequested(QPoint), this, showTreeContextMenu(QPoint));
  CQT_CONNECT(ui.treeWidget, itemChanged(QTreeWidgetItem*,int), this, treeItemChanged(QTreeWidgetItem*,int));

  // Only register during creates, not deserialization
  if (proj->getActive())
  {
    CarbonOptions* options = proj->getToolOptions("VSPCompiler");
    options->registerPropertyChanged("-o", "outputFilenameChanged", this);
  }
}

const char* PackageToolWizardWidget::userFriendlyName()
{
  return mFriendlyName.c_str();
}

void PackageToolWizardWidget::dataChanged()
{
  setWindowModified(true);
  setWidgetModified(true);
}

PackageToolWizardWidget::~PackageToolWizardWidget()
{
  CarbonProject* proj = getProject();
  if (proj) {
    CarbonOptions* options = proj->getToolOptions("VSPCompiler");
    if (options)
      options->unregisterPropertyChanged("-o", this);
  }

  delete mRootQItem;
}

void PackageToolWizardWidget::fileSave()
{
  //saveDocument();
}

bool PackageToolWizardWidget::loadFile(const QString &/*filename*/)
{
 return true;
}

void PackageToolWizardWidget::saveDocument()
{
  // Package info is save in the .carbon file, so there is never
  // any "component data" to be saved
}

void PackageToolWizardWidget::updateMenusAndToolbars()
{
}

void PackageToolWizardWidget::setComp(PackageComponent* comp)
{ 
  mComp = comp;
  // Does this test make sense?  Otherwise wouldn't we leak when called multiple times.
  if (mRootQItem == NULL)
    mRootQItem = new PackageQTreeWidgetItem(mComp->getRootItem(), ui.treeWidget, this);
}

void PackageToolWizardWidget::projectLoaded(const char*, CarbonProject*)
{
}

void PackageToolWizardWidget::closeEvent(QCloseEvent *event)
{
  qDebug() << "Closing PackageTool Wizard";
  MDIWidget::closeEvent(this);
  event->accept();
}

void PackageToolWizardWidget::configurationChanged(const char* config)
{
  if(mComp) mComp->configurationChanged(config);
  populateTree();
}
void PackageToolWizardWidget::compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  qDebug() << "Compilation ended.";
}
void PackageToolWizardWidget::configurationRemoved(const char* config)
{
  if(mComp) mComp->configurationRemoved(config);
  populateTree();
}
void PackageToolWizardWidget::configurationRenamed(const char* toConfig, const char*fromConfig)
{
  if(mComp) mComp->configurationRenamed(toConfig, fromConfig);
  populateTree();
}
void PackageToolWizardWidget::configurationCopy(const char* toConfig, const char*fromConfig)
{
  if(mComp) mComp->configurationCopy(toConfig, fromConfig);
  populateTree();
}
void PackageToolWizardWidget::configurationAdded(const char* config)
{
  qDebug() << "PT: Config " << config << " added.";
}

void PackageToolWizardWidget::initialize()
{
  // Enable only the components that exists in the project, so first disable all components
  ui.checkBoxCoWare->setEnabled(false);
  ui.checkBoxSoCDesigner->setEnabled(false);
  ui.checkBoxSystemC->setEnabled(false);

  // Then indicate that existing components have been added
  CarbonProject* proj = mCtx->getCarbonProjectWidget()->project();
  for (UInt32 i = 0; i < proj->getComponents()->numComponents(); ++i) {
    CarbonComponent* comp = proj->getComponents()->getComponent(i);
    if (comp) componentAdded(comp);
  }
}

CarbonProject* PackageToolWizardWidget::getProject()
{
  return mCtx->getCarbonProjectWidget()->project();
}

void PackageToolWizardWidget::populateTree()
{
  // Update filename
  ui.labelInstallerFileName->setText(mComp->InstallerName());
  ui.labelInstallerFileName->setToolTip(mComp->InstallerName());
  ui.lineEdit_ProductName->setText(mComp->getProductName());
  ui.lineEdit_ShortName->setText(mComp->getShortName());
  ui.lineEdit_Version->setText(mComp->getVersion());
  

  // Update Checkboxes
  ui.checkBoxStandAlone->setChecked(mComp->containsComp("StandAlone"));
  ui.checkBoxSoCDesigner->setChecked(mComp->containsComp("MaxsimComponent"));
  ui.checkBoxCoWare->setChecked(mComp->containsComp("CowareComponent"));
  ui.checkBoxSystemC->setChecked(mComp->containsComp("SystemCComponent"));

  // Clear tree first
  ui.treeWidget->clear();
 
  // Populate top items
  PackageTreeItem* rootItem = mComp->getRootItem();
  for(PackageTreeItem* item = rootItem->firstChild(); item != NULL; item = rootItem->nextChild()) {
    PackageQTreeWidgetItem* top = new PackageQTreeWidgetItem(item, ui.treeWidget, this);
    ui.treeWidget->addTopLevelItem(top);
    
    // Populate the children
    populateTreeChildren(top, item);
  }

  // Expand everything
  ui.treeWidget->expandAll();

  ui.treeWidget->header()->resizeSections(QHeaderView::ResizeToContents);
}

void PackageToolWizardWidget::populateTreeChildren(QTreeWidgetItem* widgetParent, PackageTreeItem* pktParent)
{
  for(PackageTreeItem* item = pktParent->firstChild(); item != NULL; item = pktParent->nextChild()) {
    QTreeWidgetItem* child = new PackageQTreeWidgetItem(item, ui.treeWidget, this);
    widgetParent->addChild(child);

    // If this item is a directory we need to populate it's children
    if(item->isDir())
      populateTreeChildren(child, item);
  }
}

void PackageToolWizardWidget::addRemoveComponent(const char* component, Qt::CheckState state)
{
  if(state == Qt::Checked)
    mComp->addComponentToPackage(component);
  else
    mComp->removeComponentFromPackage(component);

  // Re-populate the tree after changes
  populateTree();
}

bool PackageToolWizardWidget::validateForPackaging()
{
  if (!ui.checkBoxStandAlone->isChecked() &&
      !ui.checkBoxSoCDesigner->isChecked() &&
      !ui.checkBoxCoWare->isChecked() &&
      !ui.checkBoxSystemC->isChecked()) {
    QMessageBox::warning(this, MODELSTUDIO_TITLE, "You must choose at least one package component");
    ui.checkBoxStandAlone->setFocus();
    return false;
  }

  if (ui.lineEdit_ProductName->text().length() == 0) {
    QMessageBox::warning(this, MODELSTUDIO_TITLE, "You must enter a Product Name");
    ui.lineEdit_ProductName->setFocus();
    return false;
  }

  if (ui.lineEdit_ShortName->text().length() == 0) {
    QMessageBox::warning(this, MODELSTUDIO_TITLE, "You must enter a Short Name");
    ui.lineEdit_ShortName->setFocus();
    return false;
  }

  if (ui.lineEdit_Version->text().length() == 0) {
    QMessageBox::warning(this, MODELSTUDIO_TITLE, "You must enter a Version");
    ui.lineEdit_Version->setFocus();
    return false;
  }
  return true;  // Validated
}

// Implement logic causing StandAlone to be a prerequisite of SoCDesigner or
// CoWare components.
void PackageToolWizardWidget::computeStandAloneState()
{
  // If SoCDesigner or CoWare is checked, check StandAlone and disable it.
  if (ui.checkBoxCoWare->checkState() == Qt::Checked ||
      ui.checkBoxSystemC->checkState() == Qt::Checked) {
    ui.checkBoxStandAlone->setCheckState(Qt::Checked);
    ui.checkBoxStandAlone->setEnabled(false);
  }
  // If neither is checked, enable it (and leave checked state alone).
  else
    ui.checkBoxStandAlone->setEnabled(true);
}

void PackageToolWizardWidget::on_checkBoxStandAlone_stateChanged(int state)
{
  addRemoveComponent("StandAlone", Qt::CheckState(state));
}

void PackageToolWizardWidget::on_checkBoxSoCDesigner_stateChanged(int state)
{
  addRemoveComponent("MaxsimComponent", Qt::CheckState(state));
}

void PackageToolWizardWidget::on_checkBoxCoWare_stateChanged(int state)
{
  addRemoveComponent("CowareComponent", Qt::CheckState(state));
  computeStandAloneState();
}

void PackageToolWizardWidget::on_checkBoxSystemC_stateChanged(int state)
{
  addRemoveComponent("SystemCComponent", Qt::CheckState(state));
  computeStandAloneState();
}

void PackageToolWizardWidget::componentAdded(CarbonComponent* comp)
{
  if (comp) {
    if (strcmp(comp->getName(), "MaxsimComponent") == 0)
      ui.checkBoxSoCDesigner->setEnabled(true);
    else if (strcmp(comp->getName(), "CowareComponent") == 0)
      ui.checkBoxCoWare->setEnabled(true);
    else if (strcmp(comp->getName(), "SystemCComponent") == 0)
      ui.checkBoxSystemC->setEnabled(true);
  }
}

void PackageToolWizardWidget::componentDeleted(CarbonComponent* comp)
{
  if (comp) {
    if (strcmp(comp->getName(), "MaxsimComponent") == 0) {
      ui.checkBoxSoCDesigner->setChecked(false);
      ui.checkBoxSoCDesigner->setEnabled(false);
    }
    else if (strcmp(comp->getName(), "CowareComponent") == 0) {
      ui.checkBoxCoWare->setChecked(false);
      ui.checkBoxCoWare->setEnabled(false);
    }
    else if (strcmp(comp->getName(), "SystemCComponent") == 0) {
      ui.checkBoxSystemC->setChecked(false);
      ui.checkBoxSystemC->setEnabled(false);
    }
  }
}

// When cbuild -o option changes, we need to get the new names for
// files to package.  Do this by removing and adding the components.
void PackageToolWizardWidget::outputFilenameChanged(const CarbonProperty*, const char* /*newValue*/)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  QStringList components;
  components << "StandAlone" << "MaxsimComponent" << "CowareComponent" << "SystemCComponent";
  QStringListIterator it(components);
  while (it.hasNext()) {
    QString compCopy = it.next();
    if (mComp->containsComp(qPrintable(compCopy))) {
      mComp->removeComponentFromPackage(qPrintable(compCopy));
      mComp->addComponentToPackage(qPrintable(compCopy));
    }
  }

  populateTree();

  QApplication::restoreOverrideCursor();
}

void PackageToolWizardWidget::showTreeContextMenu(QPoint pos)
{
  PackageQTreeWidgetItem* item = dynamic_cast<PackageQTreeWidgetItem*>(ui.treeWidget->itemAt(pos));
  QPoint point = ui.treeWidget->viewport()->mapToGlobal(pos);
  if (item)
  {
    item->showContextMenu(point);
  }
  else {
    // If not a PackageQTreeWidgetItem, it must be top level.
    // Use a PackageQTreeWidgetItem object as a convenient way perform the top level add
    mRootQItem->showContextMenu(point);
  }
}

void PackageToolWizardWidget::treeItemChanged(QTreeWidgetItem* item, int col)
{
  PackageQTreeWidgetItem* packItem = dynamic_cast<PackageQTreeWidgetItem*>(item);
  if(packItem) {
    packItem->updateTreeItem(col);
  }
}

// Using textChanged rather than editingFinished, because clicking a
// toolbar doesn't cause editingFinished to fire.
void PackageToolWizardWidget::on_lineEdit_ProductName_textChanged()
{
  UtString text;
  text << ui.lineEdit_ProductName->text();
  mComp->setProductName(text.c_str());
  // Update the installer name widget immediately so the installer name
  // contains the product name as it's being typed.
  ui.labelInstallerFileName->setText(mComp->InstallerName());
  ui.labelInstallerFileName->setToolTip(mComp->InstallerName());
}

void PackageToolWizardWidget::on_lineEdit_ShortName_textChanged()
{
  UtString text;
  text << ui.lineEdit_ShortName->text();
  mComp->setShortName(text.c_str());
}

void PackageToolWizardWidget::on_lineEdit_Version_textChanged()
{
  UtString text;
  text << ui.lineEdit_Version->text();
  mComp->setVersion(text.c_str());
}

PackageOptionsItem::PackageOptionsItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, PackageComponent* comp) : 
  CarbonComponentTreeItem(proj,parent,comp)
{
  setIcon(0, QIcon(":/cmm/Resources/package.png"));
  setText(0, "Package Options");
}
 
void PackageOptionsItem::showContextMenu(const QPoint& point)
{
  mProjectWidget->getContextMenuRoot()->exec(point);   
}

void PackageOptionsItem::singleClicked(int)
{
  openWidget();
}

void PackageOptionsItem::doubleClicked(int)
{
  openWidget();
}

void PackageOptionsItem::openWidget()
{
  // Package Options are stored in the .carbon file, so the package component
  // doesn't have a file associated with it. Just give NULL as filename
  MDIWidget* widget = mProjectWidget->openSource(NULL, -1, PACKAGE_COMPONENT_NAME);

  PackageToolWizardWidget* packageWidget = dynamic_cast<PackageToolWizardWidget*>(widget);
  INFO_ASSERT(packageWidget, "Expecting Package Options Editor");

  // Let the widget know about the component.
  if (packageWidget) {
    packageWidget->setComp(dynamic_cast<PackageComponent*>(mComponent));
    packageWidget->populateTree();
  }
}
