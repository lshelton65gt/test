//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "WizPgFinished.h"
#include "MemoryWizard.h"
#include "Template.h"
#include "WizardTemplate.h"
#include "cmm.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonProjectNodes.h"
#include "DirectivesEditor.h"
#include "ElaboratedModel.h"

WizPgFinished::WizPgFinished(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);

  registerField("fileName*", ui.lineEditFilename);

  updateControls();
}

WizPgFinished::~WizPgFinished()
{
}

void WizPgFinished::updateControls()
{
  ui.lineEditReplaceModule->setEnabled(ui.checkBoxSubstituteModule->isChecked());
  ui.lineEditWithModule->setEnabled(ui.checkBoxSubstituteModule->isChecked());
  
}

void WizPgFinished::on_checkBoxSubstituteModule_stateChanged(int)
{
  updateControls();
}

bool WizPgFinished::serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  QDomElement tag = doc.createElement("WizardStep");
  tag.setAttribute("name", objectName());
  parent.appendChild(tag);

  addElement(doc, tag, "ReplaceModule", ui.lineEditReplaceModule->text());
  addElement(doc, tag, "WithModule", ui.lineEditWithModule->text());
  addElement(doc, tag, "SubstituteModule", ui.checkBoxSubstituteModule->isChecked() ? "true" : "false");
  addElement(doc, tag, "OutputFile", ui.lineEditFilename->text());

  return true;
}

void WizPgFinished::initializePage()
{
  setTitle("Finished");
  setSubTitle("The model is now ready for use.");

  QVariant value = wizard()->field("moduleName");
  if (!value.isNull())
    ui.lineEditWithModule->setText(wizard()->field("moduleName").toString());

  QVariant replaceValue = wizard()->field("replaceModuleName");
  if (!replaceValue.isNull())
    ui.lineEditReplaceModule->setText(wizard()->field("replaceModuleName").toString());

  ui.lineEditFilename->setText(mFileName);

  MemoryWizard* wiz = (MemoryWizard*)wizard();
  QString replayFile = wiz->getReplayFile();
  if (replayFile.length() > 0)
  {
    QDomElement wizElem = wiz->findWizardStep(objectName());
    if (!wizElem.isNull())
    {
      QDomNode n = wizElem.firstChild();
      while(!n.isNull())
      {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) 
        {
          if (e.tagName() == "ReplaceModule")
          {
            QString moduleName = e.text();
            ui.lineEditReplaceModule->setText(moduleName);
          }
          else if (e.tagName() == "OutputFile")
          {
            mFileName = e.text();
            wizard()->setField("fileName", mFileName);
            ui.lineEditFilename->setText(mFileName);
          }
          else if (e.tagName() == "WithModule")
          {
            QString moduleName = e.text();
            ui.lineEditWithModule->setText(moduleName);
          }
          else if (e.tagName() == "SubstituteModule")
          {
            QString value = e.text();
            if (0 == value.toLower().compare("true"))
              ui.checkBoxSubstituteModule->setChecked(true);
            else
              ui.checkBoxSubstituteModule->setChecked(false);
          }
        }
        n = n.nextSibling();
      }
    }
    wiz->stopReplay();
    wiz->postRestart();
  }
}

bool WizPgFinished::checkArguments()
{
  QString msg;

  if (ui.lineEditFilename->text().length() == 0)
    msg += QString("Filename must not be empty\n");
  else
  {
    QFileInfo fi(ui.lineEditFilename->text());
    QString ext = fi.completeSuffix().toLower();
    if (ext.length() == 0)
      msg += QString("Filename is missing an extension such as .v, .vhd or .vhdl");
  }
 

  if (msg.length() > 0)
  {
    QMessageBox::critical(this, MODELWIZARD_TITLE, msg);
    return false;
  }
  else
    return true;
}

void WizPgFinished::serializePages(const QString& outputName)
{
  QDomDocument doc("RemodelWizard");
  QDomElement root = doc.createElement("RemodelWizard");
  doc.appendChild(root);
  XmlErrorHandler eh;

  foreach (int pageIndex, wizard()->visitedPages())
  {
    WizardPage* page = dynamic_cast<WizardPage*>(wizard()->page(pageIndex));
    if (page)
    {
      page->serialize(doc, root, &eh);
    }
  }

  QFile file(outputName);
  if (file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
     QTextStream out(&file);
     out << doc.toString(2);
  }
  qDebug() << doc.toString(2);
}

bool WizPgFinished::validatePage()
{
  if (!checkArguments())
    return false;

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();

  bool addToProject = ui.checkBoxAddToProject->isChecked();
  QString fileName = ui.lineEditFilename->text();
  QFileInfo fi(fileName);

  qDebug() << fi.filePath() << fi.fileName();
  qDebug() << fi.absoluteFilePath() << fi.absolutePath();

  UtString fname;
  fname << fi.fileName();

  UtString filePath;
  if (0 == fi.filePath().compare(fi.fileName()))
    OSConstructFilePath(&filePath, pw->project()->getProjectDirectory(), fname.c_str());
  else
    filePath << fi.filePath();

  QFileInfo fi2(filePath.c_str());

  QString msg = QString("A file '%1' already exists, do you want to overwrite it?").arg(filePath.c_str());

  if (fi2.exists() && 
    QMessageBox::question(this, MODELWIZARD_TITLE, msg, 
      QMessageBox::No|QMessageBox::Yes, QMessageBox::No) == QMessageBox::No)
      return false;
  

  QString wizardFileName = QString("%1/%2.rmxml").arg(fi2.absolutePath()).arg(fi2.baseName());

  serializePages(wizardFileName);

  QString code = field("generatedCode").toString();
  QFile file(filePath.c_str());
  if (file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    QTextStream out(&file);
    out << code << "\n";
    file.close();
  }

  if (addToProject)
  {
    UtString wizName;
    wizName << wizardFileName;

    CarbonProjectTreeNode* parent = pw->addRTLSource(MODELWIZARD_GENERATED, filePath.c_str());
    if (parent)
    {
      CarbonRemodelItem* item = new CarbonRemodelItem(pw, wizName.c_str(), parent, wizName.c_str());
      if (item)
        item->setExpanded(true);
    }
    qDebug() << code;
  }

  if (ui.checkBoxSubstituteModule->isChecked())
  {
    DirectivesEditor* dirEditor = pw->directivesEditor();
    if (dirEditor == NULL)
      dirEditor = pw->compilerDirectives();

    if (dirEditor)
    {
      TabModuleDirectives* modDirs = dirEditor->getModuleDirectivesTab();
      
      UtString modName;
      modName << ui.lineEditReplaceModule->text();
     
      UtString origModuleName;
      origModuleName << ui.lineEditWithModule->text();

      UtString portNames;
      portNames << "portsBegin";

      MemoryWizard* wiz = (MemoryWizard*)wizard();
      WizardTemplate* templ = wiz->getTemplate();
      WizardContext* context = templ->getContext();
      ElaboratedModel* model = context->getModel();
      PortInstances* instances = model->getUniquePorts();

      UtString origNames;
      for (int i=0; i<instances->getCount(); i++)
      {
        PortInstance* portInst = instances->getAt(i);
        portNames << " " << portInst->getName();
        origNames << " " << portInst->getPort()->getBaseName();
      }

      portNames << " portsEnd";

      QString dirString = QString("%3:%4:%1:%2")
        .arg(origModuleName.c_str())
        .arg(origNames.c_str())
        .arg(modName.c_str())
        .arg(portNames.c_str());
 
      UtString value;
      value << dirString;

      modDirs->addOrChange(modName.c_str(), "substituteModule", value.c_str());
    }
  }

  return true;
}
