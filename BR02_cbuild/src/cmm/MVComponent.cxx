//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include <QDebug>
#include "gui/CQt.h"
#include "CarbonProjectWidget.h"
#include "CarbonComponents.h"
#include "MVComponent.h"
#include "CarbonOptions.h"
#include "DlgConfirmDelete.h"
#include "Mdi.h"
#include "MdiMaxsim.h"
#include "MVWizardWidget.h"
#include "SettingsEditor.h"
#include "CarbonDatabaseContext.h"
#include "WorkspaceModelMaker.h"

class MVTool : public CarbonTool
{
public:
  CARBONMEM_OVERRIDES
  MVTool();

};

MVTool::MVTool() : CarbonTool("MV", ":cmm/Resources/MVTool.xml")
{
  mProperties.readPropertyDefinitions(":/cmm/Resources/MVComponentOptions.xml");
  mDefaultOptions = new CarbonOptions(NULL, getProperties(), "MVDefaults");
  mOptions = new CarbonOptions(mDefaultOptions, getProperties(), "MV");
}

CarbonOptions* MVComponent::getOptions(const char* configName) 
{
  CarbonOptions* options = mActiveConfiguration->getOptions("MV");
  if (configName != NULL)
    options = getConfigurations()->findConfiguration(configName)->getOptions("MV");

  return options;
}

const char* MVComponent::getWizardFilename()
{
  return "Shadow Hierarchy";
}

const char* MVComponent::getOutputDirectory(const char* cfgName)
{
  mOutputDir.clear();

  CarbonProject* proj = getProject();

  if (cfgName == NULL && proj && proj->getActive())
    cfgName = getProject()->getActive()->getName();

  if (mActiveConfiguration == NULL && mConfigs)
    mActiveConfiguration = mConfigs->findConfiguration(cfgName);

  if (mActiveConfiguration)
  {
    const char* platform = mActiveConfiguration->getPlatform();

    OSConstructFilePath(&mOutputDir, getProject()->getProjectDirectory(), platform);
    OSConstructFilePath(&mOutputDir, mOutputDir.c_str(), cfgName);
    OSConstructFilePath(&mOutputDir, mOutputDir.c_str(), "MV");
  }
  else
    qDebug() << "Error: NO active configuration";

  return mOutputDir.c_str(); 
}

void MVComponent::icheckComponent()
{
  bool status = checkComponent(getProject()->getProjectWidget()->getConsole());

  if (status)
  {
    QDockWidget* dw = getProject()->getProjectWidget()->context()->getWorkspaceModelMaker()->getErrorInfoDockWindow();
    // lower followed by raise causes it to "pop" forward
    dw->lower();
    dw->raise();
    getProject()->getProjectWidget()->context()->getMainWindow()->statusBar()->showMessage(tr("Problems found"), 2000);
  }
  else
    QMessageBox::information(NULL, MODELSTUDIO_TITLE,
      tr("No Errors or Warnings were found during pre-compile check(s)."), QMessageBox::Ok);
}

int MVComponent::getGeneratedFiles(QStringList& fileList)
{
  const char* iodbName = getProject()->getDesignFilePath(".symtab.db");
  QFileInfo fi(iodbName);
  
  fileList.push_back(QString("%1/carbon_%2%3").arg(getOutputDirectory()).arg(fi.baseName()).arg(getLanguageExtension()));
  fileList.push_back(QString("%1/carbon_%2.c").arg(getOutputDirectory()).arg(fi.baseName()));

  return 2;

}

bool MVComponent::checkComponent(CarbonConsole* console)
{
  bool errorFlag = false;
  qDebug() << "Checking Model Validation Component";

  const char* simulatorName = getOptions()->getValue("Simulator")->getValue();

  // Check VCS Env
  if (simulatorName && 0 == strcmp(simulatorName, "VCS") && NULL == getenv("VCS_HOME"))
  {
    UtString msg;
    msg << "Error 3: EnvCheck: VCS_HOME environment variable not defined";
    console->processOutput(CarbonConsole::Local, CarbonConsole::Check, msg.c_str());
    errorFlag = true;
  }

  if (simulatorName && 0 == strcmp(simulatorName, "ModelSim") && NULL == getenv("MTI_HOME"))
  {
    UtString msg;
    msg << "Error 3: EnvCheck: ModelSim requires MTI_HOME environment variable";
    console->processOutput(CarbonConsole::Local, CarbonConsole::Check, msg.c_str());
    errorFlag = true;
  }

  if (simulatorName && 0 == strcmp(simulatorName, "NCSim") && NULL == getenv("SIM_HOME"))
  {
    UtString msg;
    msg << "Error 3: EnvCheck: NCSim requires the SIM_HOME environment variable";
    console->processOutput(CarbonConsole::Local, CarbonConsole::Check, msg.c_str());
    errorFlag = true;
  }

  return errorFlag;
}

MVComponent::MVComponent(CarbonProject* proj) :CarbonComponent(proj, "MVComponent")
{
  setFriendlyName("ModelValidation");
  mProject = proj;
  mConfigs = new CarbonConfigurations(proj);
  mRootItem = NULL;
  mActiveConfiguration = NULL;
 
  mXmlItem = NULL;
  mRtlFile = NULL;
  mCFile = NULL;

  CarbonConfigurations* configs = proj->getConfigurations();
  for (UInt32 i=0; i<configs->numConfigurations(); i++)
  {
    CarbonConfiguration* projConfig = configs->getConfiguration(i);
    const char* configName = projConfig->getName();

    CarbonConfiguration* config = mConfigs->addConfig("Linux", configName);

    if (mActiveConfiguration == NULL)
      mActiveConfiguration = mConfigs->findConfiguration(proj->getActiveConfiguration());

    config->addTool(new MVTool()); 
    createDirectory(config);
  }

  updateTopModuleName();

  CQT_CONNECT(proj, configurationChanged(const char*), this, configurationChanged(const char*));
  CQT_CONNECT(proj, configurationRenamed(const char*, const char*), this, configurationRenamed(const char*, const char*));
  CQT_CONNECT(proj, configurationCopy(const char*, const char*), this, configurationCopy(const char*, const char*));
  CQT_CONNECT(proj, configurationRemoved(const char*), this, configurationRemoved(const char*));
  CQT_CONNECT(proj, configurationAdded(const char*), this, configurationAdded(const char*));
  CQT_CONNECT(proj, projectLoaded(const char*,CarbonProject*), this, projectLoaded(const char*,CarbonProject*));

  CQT_CONNECT(getProject()->getProjectWidget(), projectClosing(CarbonProject*), this, projectClosing(CarbonProject*));

  CQT_CONNECT(getProject()->getProjectWidget()->getConsole(), compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType));
  CQT_CONNECT(getProject()->getProjectWidget()->getConsole(), compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType));

 // Only register during creates, not deserialization
  if (proj->getActive())
  {
    CarbonOptions* options = proj->getToolOptions("VSPCompiler");
    options->registerPropertyChanged("-o", "outputFilenameChanged", this);
  }
}

void MVComponent::projectClosing(CarbonProject* proj)
{
  disconnect(proj, SIGNAL(configurationChanged(const char*)), this, SLOT(configurationChanged(const char*)));

  disconnect(proj, SIGNAL(configurationRenamed(const char*, const char*)), this, SLOT(configurationRenamed(const char*, const char*)));
  disconnect(proj, SIGNAL(configurationCopy(const char*, const char*)), this,  SLOT(configurationCopy(const char*, const char*)));
  disconnect(proj, SIGNAL(configurationRemoved(const char*)), this,  SLOT(configurationRemoved(const char*)));
  disconnect(proj, SIGNAL(configurationAdded(const char*)), this,  SLOT(configurationAdded(const char*)));
  disconnect(proj, SIGNAL(projectLoaded(const char*,CarbonProject*)), this,  SLOT(projectLoaded(const char*,CarbonProject*)));

  disconnect(proj->getProjectWidget(), SIGNAL(projectClosing(CarbonProject*)), this,  SLOT(projectClosing(CarbonProject*)));

  disconnect(proj->getProjectWidget()->getConsole(), SIGNAL(compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)), this,  SLOT(compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)));
  disconnect(proj->getProjectWidget()->getConsole(), SIGNAL(compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType)), this,  SLOT(compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType)));
}

void MVComponent::outputFilenameChanged(const CarbonProperty*, const char*)
{
  updateTreeIcons();
}
void MVComponent::projectLoaded(const char*, CarbonProject* proj)
{
  CarbonOptions* options = proj->getToolOptions("VSPCompiler");
  options->registerPropertyChanged("-o", "outputFilenameChanged", this);
}

void MVComponent::configurationAdded(const char* name)
{
  qDebug() << "Adding MVComponent Configuration: " << name;
  CarbonConfiguration* cfg = getConfigurations()->findConfiguration(name);
  if (cfg == NULL)
  {
    cfg = mConfigs->addConfig(mActiveConfiguration->getPlatform(), name);
    cfg->addTool(new MVTool());
    createDirectory(cfg);
  }
}

void MVComponent::configurationRenamed(const char* oldName, const char* newName)
{
  CarbonConfiguration* oldConfig = mConfigs->findConfiguration(oldName);
  INFO_ASSERT(oldConfig, "Expecting Configuration");
  oldConfig->putName(newName);
  renameDirectory(oldName, newName);
  updateTreeIcons();
}

void MVComponent::configurationCopy(const char* oldCfgName, const char* newCfgName)
{
  CarbonOptions* oldOptions = getOptions(oldCfgName);
  CarbonOptions* newOptions = getOptions(newCfgName);

  INFO_ASSERT(oldOptions, "Expecting old options");
  INFO_ASSERT(newOptions, "Expecting new options");

  oldOptions->setBlockSignals(true);
  newOptions->setBlockSignals(true);

  for (CarbonOptions::OptionsLoop p = oldOptions->loopOptions(); !p.atEnd(); ++p)
  {
    CarbonPropertyValue* sv = p.getValue();
    const char* value = sv->getValue();
    const char* propName = sv->getProperty()->getName();
    newOptions->putValue(propName, sv->getProperty()->getDefaultValue());
    newOptions->putValue(propName, value);
  }

  oldOptions->setBlockSignals(false);
  newOptions->setBlockSignals(false);

  updateTreeIcons();
}

void MVComponent::configurationRemoved(const char* name)
{
  qDebug() << "Removing MVComponent Configuration: " << name;
  mConfigs->removeConfiguration(name);
}

void MVComponent::configurationChanged(const char* newConfig)
{
  qDebug() << "MV Component Config changed to: " << newConfig;

  mActiveConfiguration = mConfigs->findConfiguration(newConfig);
  INFO_ASSERT(mActiveConfiguration, "Unable to locate configuration");
  createDirectory(mActiveConfiguration);
  updateTreeIcons();
}


void MVComponent::renameDirectory(const char* oldName, const char* newName)
{
  UtString oldDir;
  UtString newDir;

  OSConstructFilePath(&oldDir, getProject()->getProjectDirectory(), mActiveConfiguration->getPlatform());
  OSConstructFilePath(&oldDir, oldDir.c_str(), oldName);
  OSConstructFilePath(&oldDir, oldDir.c_str(), "MV");

  OSConstructFilePath(&newDir, getProject()->getProjectDirectory(), mActiveConfiguration->getPlatform());
  OSConstructFilePath(&newDir, newDir.c_str(), newName);
  OSConstructFilePath(&newDir, newDir.c_str(), "MV");

  QDir dir;
  if (!dir.rename(oldDir.c_str(), newDir.c_str()))
    qDebug() << "failed to rename directory!";
}


void MVComponent::createConfigurations(MVComponent* comp, xmlNodePtr parent)
{
  // first make sure the configurations exist for later deserialization
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "Configurations"))
    {
      for (xmlNodePtr configChild = child->children; configChild != NULL; configChild = configChild->next) 
      {
        if (XmlParsing::isElement(configChild, "Configuration"))
        {
          UtString configName;
          XmlParsing::getProp(configChild, "Name", &configName);
          QString cname = configName.c_str();
          QStringList configParts = cname.split('|');

          QString qname = configParts[0];
          QString qplat = configParts[1];

          UtString name;
          name << qname;
          UtString platform;
          platform << qplat;

          CarbonConfiguration* cfg = comp->getConfigurations()->findConfiguration(name.c_str());
          if (cfg == NULL)
          {
            comp->getConfigurations()->addConfig(platform.c_str(), name.c_str());
            cfg->addTool(new MVTool());
            createDirectory(cfg);
          }
        }
      }
    }
  }
}
// Create the directory for the configuration
void MVComponent::createDirectory(CarbonConfiguration* config)
{
  UtString outDir;

  OSConstructFilePath(&outDir, config->getProject()->getProjectDirectory(), config->getPlatform());
  OSConstructFilePath(&outDir, outDir.c_str(), config->getName());
  OSConstructFilePath(&outDir, outDir.c_str(), "MV");

  QDir q(outDir.c_str());
  if (!q.exists())
  {
    qDebug() << "Creating directory: " << outDir.c_str();
    q.mkpath(outDir.c_str());
  }
}

MVComponent::~MVComponent()
{
}

MVComponent* MVComponent::deserialize(CarbonProject* proj, xmlNodePtr parent, UtXmlErrorHandler* eh)
{
  MVComponent* comp = NULL;

  // Is this a MVComponent?
  if (XmlParsing::isElement(parent, "MVComponent"))
  {
    comp = new MVComponent(proj);
    
    createConfigurations(comp, parent);

    for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
    {
      if (XmlParsing::isElement(child, "Configurations"))
        comp->getConfigurations()->deserialize(child, eh);
    }
  }

  return comp;
}

QString MVComponent::getXMLFilename(const char* configName)
{
  UtString srcFile;
  const char* name;
  if (configName == NULL)
    name = mProject->getActiveConfiguration();
  else
    name = configName;

  UtString fileName;
  fileName << name << ".mv.xml";

  OSConstructFilePath(&srcFile, getOutputDirectory(configName), fileName.c_str());
  return srcFile.c_str();
}

bool MVComponent::serialize(xmlTextWriterPtr writer)
{
  xmlTextWriterStartElement(writer, BAD_CAST "MVComponent");

  mConfigs->serialize(writer);

  xmlTextWriterEndElement(writer);
  
  return true;
}

CarbonComponentTreeItem* MVComponent::createTreeItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent)
{
  MVTreeItem* item = NULL;

  if (parent)
    item = new MVTreeItem(proj, this, parent);
  else
  {
    item = new MVTreeItem(proj, this);
    proj->addTopLevelItem(item);
  }
  item->setText(0, "Model Validation Component");

  updateTopModuleName();

  mXmlItem = new MVXmlItem(proj, item, this);
  mRtlFile = new MVSourceItem(proj, item, this);
  mCFile = new MVSourceItem(proj, item, this);

  item->setExpanded(true);

  mRootItem = item;

  updateTreeIcons();

  return item;
}

void MVComponent::updateTreeTextValues()
{
  UtString MVName;
  const char* topModule = getOptions()->getValue("Top Module")->getValue();

  if (mXmlItem)
    mXmlItem->setText(0, "Shadow Hierarchy");

  MVName.clear();
  MVName << "carbon_" << topModule << getLanguageExtension();
  if (mRtlFile)
    mRtlFile->setText(0, MVName.c_str());

  MVName.clear();
  MVName << "carbon_" << topModule << ".c";
  if (mCFile)
    mCFile->setText(0, MVName.c_str());
}

const char* MVComponent::getLanguageExtension()
{
  CarbonDatabaseContext* dbContext = getProject()->getDbContext();
  if (dbContext && dbContext->getDB())
  {
    CarbonDBNodeIter* iter = carbonDBLoopDesignRoots(dbContext->getDB());
    const CarbonDBNode* node;

    const char* value = ".v";
    while ((node = carbonDBNodeIterNext(iter)) != NULL)
    {
      const char* language = carbonDBSourceLanguage(dbContext->getDB(), node);
      if (0 == strcmp(language, "Verilog"))
      {
        value = ".v";
        break;
      }
      else if (0 == strcmp(language, "VHDL"))
      {
        value = ".vhdl";
        break;
      }
    }
    carbonDBFreeNodeIter(iter);

    return value;
  }
  // if all else fails, default to Verilog
  return ".v";
}

void MVComponent::compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  setEnabled(false);
}

void MVComponent::compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  updateTreeIcons();
  setEnabled(true);
}

const char* MVComponent::getTargetName(CarbonConfiguration*,TargetType type, TargetPlatform)
{
  static UtString targetName;
  targetName.clear();

  switch (type)
  {
  case CarbonComponent::Build: targetName << "MVComponent"; break;
  case CarbonComponent::Clean: targetName << "MVComponentClean"; break;
  case CarbonComponent::Package: break;
  }

  return targetName.c_str();
}
// Refresh the icons for MVSourceItems
void MVComponent::updateTreeIcons()
{
  updateTreeTextValues();

  if (mRootItem)
  {
    const char* od = getOutputDirectory();
    for (int i=0; i<mRootItem->childCount(); i++)
    {
      QTreeWidgetItem* item = mRootItem->child(i);
      MVSourceItem* srcItem = dynamic_cast<MVSourceItem*>(item);
      if (srcItem)
      {
        UtString fpath;
        UtString name;
        name << srcItem->text(0);
        OSConstructFilePath(&fpath, od, name.c_str());
        QFile file(fpath.c_str());
        srcItem->setExists(file.exists());
      }
    }
  }
}

void MVComponent::generateMakefile(CarbonConfiguration* cfg,const char* /*makefileName*/)
{
  UtString configName;
  OSConstructFilePath(&configName, getOutputDirectory(cfg->getName()), "configuration");
  
  QString buffer;
  QTextStream stream(&buffer);
  stream.setCodec("UTF-8");

  stream << getOptions(cfg->getName())->getValue("Top Module")->getValue() << endl;
  stream << getOptions(cfg->getName())->getValue("Simulator")->getValue() << endl;
  stream << getOptions(cfg->getName())->getValue("Compile")->getValue() << endl;
  stream << getOptions(cfg->getName())->getValue("Input Flow Mode")->getValue() << endl;
  stream << getOptions(cfg->getName())->getValue("Preserve Names")->getValue() << endl;

  stream << getProject()->getToolOptions(cfg->getName(), "VSPCompiler")->getValue("-o")->getValue() << endl;

  getProject()->writeFileIfChanged(configName.c_str(), &stream);
}

const char* MVComponent::getDocumentPath()
{
  UtString fileName;
  fileName << mProject->getActiveConfiguration() << ".mv.xml";

  mDocPath.clear();
  OSConstructFilePath(&mDocPath, getOutputDirectory(), fileName.c_str());

  return mDocPath.c_str();
}

void MVComponent::updateTopModuleName()
{
  CarbonDatabaseContext* dbContext = getProject()->getDbContext();
  if (dbContext && dbContext->getDB())
  {
    const char* topModule = carbonDBGetTopLevelModuleName(dbContext->getDB());
    getOptions()->putValue("Top Module", topModule);
  }
}

bool MVComponent::getTargetImpl(CarbonConfiguration* cfg, QTextStream& stream, TargetType type)
{
  const char* iodbName = getProject()->getConfigDesignFilePath(cfg->getName(), ".symtab.db");
  QFileInfo fi(iodbName);

  UtString MVName;
  MVName << fi.baseName();

  updateTopModuleName();

  const char* topModule = getOptions(cfg->getName())->getValue("Top Module")->getValue();

  UtString generatedOutputsName;
  generatedOutputsName <<  "MV/" << "carbon_" << topModule << getLanguageExtension();
  generatedOutputsName << " MV/" << "carbon_" << topModule << ".c";
  generatedOutputsName << " MV/" << "Makefile.carbon." << topModule;

  switch (type)
  {
  case CarbonComponent::Build:
    {
      const char* iodbName = getProject()->getConfigDesignFilePath(cfg->getName(), ".symtab.db");
      UtString relIODBName;
      relIODBName << CarbonProjectWidget::makeRelativePath(getOutputDirectory(cfg->getName()), iodbName);

      const char* topModule = getOptions(cfg->getName())->getValue("Top Module")->getValue();

      stream << getTargetName(cfg, CarbonComponent::Build) << ": " << generatedOutputsName.c_str() << endl << endl;

      const char* simulator = getOptions(cfg->getName())->getValue("Simulator")->getValue();

      UtString MVName;
      MVName << fi.fileName();

      bool executeMakefile = false;
      const char* autoCompile = getOptions(cfg->getName())->getValue("Compile")->getValue();
      if (autoCompile && 0 == strcmp(autoCompile, "true"))
        executeMakefile = true;

      bool hasMVXml = false;

      QString xmlFileName = QString("%1/%2.mv.xml").arg(getOutputDirectory(cfg->getName())).arg(cfg->getName());
      QFileInfo xfi(xmlFileName);
      hasMVXml = xfi.exists();
      UtString xmlName;
      xmlName << xmlFileName;
     
      UtString relXMLName;
      relXMLName << CarbonProjectWidget::makeRelativePath(getOutputDirectory(cfg->getName()), xmlFileName);

      UtString makefileName;
      makefileName << "Makefile.carbon." << topModule;

      stream << generatedOutputsName.c_str() << ": " << MVName.c_str() << " MV/configuration";
      if (hasMVXml)
        stream << " MV/" << cfg->getName() << ".mv.xml";
      stream << endl;

      stream << "\t" << echoBegin() << "Start MV Component Generation" << echoEndl();
      stream << "\t" << echoBegin() << "CarbonWorkingDir: MV" << echoEndl();

      stream << "\t" << "cd MV && $(CARBON_HOME)/bin/$(CARBON_BATCH) MVGenerate -s " << simulator << " " << relIODBName.c_str();
      if (hasMVXml)
      {
        UtString equivPath;
        equivPath << getProject()->getUnixEquivalentPath(relXMLName.c_str());
        stream << " -d " << equivPath.c_str();
      }

      const char* inputFlowMode = getOptions(cfg->getName())->getValue("Input Flow Mode")->getValue();
      if (0 == strcmp(inputFlowMode, "true"))
        stream << " -i ";

      const char* preserveNames = getOptions(cfg->getName())->getValue("Preserve Names")->getValue();
      if (0 == strcmp(preserveNames, "true"))
        stream << " -p ";

      const char* memorySize = getOptions(cfg->getName())->getValue("Memory Size")->getValue();
      const char* defaultMemorySize = getOptions(cfg->getName())->getValue("Memory Size")->getProperty()->getDefaultValue();
      if (0 != strcmp(memorySize, defaultMemorySize))
        stream << " -M " << memorySize;
        
      stream << endl;

      if (executeMakefile)
        stream << "\t" << "cd MV && $(CARBON_HOME)/bin/$(CARBON_BATCH) make VHM_DIR=.. -f " << makefileName.c_str() << endl;
      stream << "\t" << echoBegin() << "Finished MV Component Generation" << echoEndl();
    }
    break;
  case CarbonComponent::Package: break;
  case CarbonComponent::Clean:
    {
      UtString cleanFiles;
      cleanFiles << "carbon_" << topModule << getLanguageExtension();
      cleanFiles << " " << "carbon_" << topModule << ".c";
      cleanFiles << " " << "Makefile.carbon." << topModule;

      stream << ".PHONY: " << getTargetName(cfg, CarbonComponent::Clean) << endl;
      stream << getTargetName(cfg, CarbonComponent::Clean) << ":" << endl;
      stream << "\t" << echoBegin() << "CarbonWorkingDir: MV" << echoEndl();
      stream << "\t" << "cd MV && $(CARBON_RM) " << cleanFiles.c_str() << endl;
    }
    break;
  }

  return true;
}

void MVSourceItem::doubleClicked(int)
{
  UtString fileName;
  fileName << text(0);

  const char* od = mComp->getOutputDirectory();
  UtString fpath;
  OSConstructFilePath(&fpath, od, fileName.c_str());

  mProjectWidget->openSource(fpath.c_str());

}

void MVXmlItem::doubleClicked(int)
{
  UtString fileName;
  fileName << text(0);
  mProjectWidget->openSource(fileName.c_str(), -1, "MV");
}


void MVXmlItem::singleClicked(int)
{
  QStringList items;
  QVariantList itemValues;
  mComp->getOptions()->setItems("Model Validation Component Properties", items, itemValues);
  mProjectWidget->context()->getSettingsEditor()->setSettings(mComp->getOptions());
}

void MVXmlItem::showContextMenu(const QPoint& pos)
{
  MVTreeItem* treeItem = dynamic_cast<MVTreeItem*>(parent());
  if (treeItem)
    treeItem->showContextMenu(pos);
}

void MVTreeItem::singleClicked(int)
{
  MVComponent* comp = static_cast<MVComponent*>(mComponent);
  QStringList items;
  QVariantList itemValues;
  comp->getOptions()->setItems("Model Validation Component Properties", items, itemValues);
  mProjectWidget->context()->getSettingsEditor()->setSettings(comp->getOptions());
}

void MVTreeItem::showContextMenu(const QPoint& pos)
{
  QMenu menu;
  menu.addAction(mActionCompile);
  menu.addAction(mActionCheck);
  menu.addAction(mActionClean);
  menu.addAction(mActionDelete);
  menu.exec(pos);
}

MVXmlItem::MVXmlItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, MVComponent* comp) : CarbonProjectTreeNode(proj,parent) 
{
  setIcon(0, QIcon(":/cmm/Resources/Wizard.png"));
  mComp = comp;
}

void MVTreeItem::createActions()
{
  mActionCompile = new QAction(mProjectWidget);
  mActionCompile->setText("Compile");
  mActionCompile->setObjectName(QString::fromUtf8("cpwCreateMVComponent"));
  mActionCompile->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/TaskHS.png")));
  mActionCompile->setStatusTip("Compile this component");
  CQT_CONNECT(mActionCompile, triggered(), this, compileComponent());

  mActionCheck = new QAction(mProjectWidget);
  mActionCheck->setText("Check");
  mActionCheck->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/check.png")));
  mActionCheck->setStatusTip("Check this component for errors");
  CQT_CONNECT(mActionCheck, triggered(), this, icheckComponent());

  mActionClean = new QAction(mProjectWidget);
  mActionClean->setText("Clean");
  mActionClean->setObjectName(QString::fromUtf8("cpwCreateMVComponent"));
  mActionClean->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/clearall.png")));
  mActionClean->setStatusTip("Clean files generated for this component");
  CQT_CONNECT(mActionClean, triggered(), this, cleanComponent());

  mActionDelete = new QAction(mProjectWidget);
  mActionDelete->setText("Delete");
  mActionDelete->setObjectName(QString::fromUtf8("cpwCreateMVComponent"));
  mActionDelete->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/DeleteHS.png")));
  mActionDelete->setStatusTip("Delete this component");
  CQT_CONNECT(mActionDelete, triggered(), this, deleteComponent());

}

MVTreeItem::MVTreeItem(CarbonProjectWidget* proj, MVComponent* comp) : CarbonComponentTreeItem(proj, 0, comp)
{
  setIcon(0, QIcon(":/cmm/Resources/TaskHS.png"));
  createActions();
}

MVTreeItem::MVTreeItem(CarbonProjectWidget* proj, MVComponent* comp, QTreeWidgetItem* parent) : CarbonComponentTreeItem(proj, parent, comp)
{
  setIcon(0, QIcon(":/cmm/Resources/TaskHS.png"));
  createActions();
}

// We need to delete the whole item
void MVTreeItem::deleteComponent()
{
  MVComponent* MV = static_cast<MVComponent*>(mComponent);
  INFO_ASSERT(MV, "Bad component");
  const char* outputDir = MV->getOutputDirectory();
  CarbonComponentTreeItem::deleteComponent(outputDir, CarbonComponentTreeItem::ModelValidation);
}

void MVTreeItem::cleanComponent()
{
  UtString target; // getTargetName() uses static buffer, so save copy of result
  target << mComponent->getTargetName(mProjectWidget->project()->getActive(), CarbonComponent::Clean);
  mProjectWidget->compile(true, false, false, NULL, target.c_str());
 // ((MVComponent*)mComponent)->updateTreeIcons();
}
void MVTreeItem::compileComponent()
{
  UtString target; // getTargetName() uses static buffer, so save copy of result
  target << mComponent->getTargetName(mProjectWidget->project()->getActive(), CarbonComponent::Build);
  mProjectWidget->compile(false, true, false, target.c_str());
 // ((MVComponent*)mComponent)->updateTreeIcons();
}

void MVTreeItem::icheckComponent()
{
 ((MVComponent*)mComponent)->icheckComponent();
}

