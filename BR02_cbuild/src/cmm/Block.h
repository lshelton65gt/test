#ifndef __BLOCK_H__
#define __BLOCK_H__

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "Scripting.h"
#include "Parameters.h"
#include "Ports.h"

class Blocks;
class Template;
class Mode;

// File: Block
//
// The Block represents a logical collection of <Port> and <Parameter> objects
// associated with this block.
class Block : public QObject, protected QScriptable
{
  Q_OBJECT

  // Property: name
  // The String name of the block
  //
  // Access:
  // *ReadOnly*
  Q_PROPERTY(QString name READ getName)
  
  // Property: parameters
  // The collection of <Parameter>s associated with this block
  //
  // See Also:
  // <Parameters> Collection 
  Q_PROPERTY(QObject* parameters READ getParameters)
  
  // Property: ports
  // The collection of <Port>s associated with this block.
  // 
  // See Also:
  // <Ports> Collection
  Q_PROPERTY(QObject* ports READ getPorts)

  // Property: parameter
  // If this block is associated with a parameter then this will 
  // be the <Parameter> or null if not associated with any parameter.
  Q_PROPERTY(QObject* parameter READ getParameter)

public:
  void setName(const QString& newVal) { mName=newVal; }
  QString getName() const { return mName; }
  static bool deserialize(Mode* mode, Template* templ, Blocks* blocks, const QDomElement& e, XmlErrorHandler* eh);
  Parameter* getParameter() { return mBlockParameter; }
  Ports* getPorts();
  Parameters* getParameters();
  bool isUserDefined() const { return mUserDefined; }

public:
  Block(bool userDefinedBlock = false)
  {
    mBlockParameter = NULL;
    mUserDefined = userDefinedBlock;
  }


private:
  QString mName;
  bool mUserDefined;
  
  // The Parameter that this block is tied to
  Parameter* mBlockParameter;

  // Parameters for this block
  Parameters mParameters;

  // Ports for this block
  Ports mPorts;
};

#endif
