//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "ScrCarbonDB.h"

#include "cmm.h"  
#include "CarbonMakerContext.h" 
#include "CarbonProjectWidget.h" 
#include "CarbonDatabaseContext.h"
#include "JavaScriptAPIs.h"

Q_DECLARE_METATYPE(ScrCarbonDB*);
Q_SCRIPT_DECLARE_QMETAOBJECT(ScrCarbonDB, QObject*)

ScrCarbonDB::ScrCarbonDB(QObject* parent) : QObject(parent)
{
  mDB = NULL;
  mDBContext = new CarbonDatabaseContext(theApp->getContext()->getQtContext());

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  INFO_ASSERT(pw, "Expecting ProjectWidget");

  CarbonProject* proj = pw->project();
  if (proj && proj->getActive())
  {
    // getDatabaseFilePath tests for existence
    QString dbFileName = proj->getDatabaseFilePath();
    mDBFileName = dbFileName;
    QFileInfo fi(dbFileName);
    if (!dbFileName.isEmpty() && fi.exists())
    {
      UtString dbName; dbName << dbFileName;
      qDebug() << "CarbonDB Loading" << dbFileName;
      mDBContext->loadDatabase(theApp->getContext()->getQtContext(), dbName.c_str());
      mDB = mDBContext->getDB();
    } 
  }

  if (mDB)
    qDebug() << "Carbon DB Loaded Database";
  else
    qDebug() << "Warning: No Carbon DB Was located";
}


// Allows properties of these types to work through intellisense
void ScrCarbonDB::registerScriptTypes(QMap<QString, const QMetaObject*>& map)
{
  map["CarbonDB"] = &ScrCarbonDB::staticMetaObject;
  map["CarbonDB*"] = &ScrCarbonDB::staticMetaObject;
}

void ScrCarbonDB::registerIntellisense(JavaScriptAPIs* apis)
{
  apis->loadAPIForVariable("CarbonDB", &ScrCarbonDB::staticMetaObject);
}

static QScriptValue constructCarbonDB(QScriptContext *ctx, QScriptEngine *eng)
{
  qDebug() << "Constructing new CarbonDB";

  ScrCarbonDB* db = new ScrCarbonDB();
  if (db->hasDatabase())
    return eng->toScriptValue(db);
  else
    return ctx->throwError(QScriptContext::ReferenceError, "No Carbon Database (.io.db, .symtab.db or .gui.db) was found");
}

void ScrCarbonDB::registerTypes(QScriptEngine* engine)
{
  // Constructors
  QScriptValue dbGenCtor = engine->newFunction(constructCarbonDB);
  engine->globalObject().setProperty("CarbonDatabase", dbGenCtor);

  qScriptRegisterQObjectMetaType<ScrCarbonDB*>(engine);

  ProtoCarbonDBNodeIter* dbNodeIterProto = new ProtoCarbonDBNodeIter();
  engine->setDefaultPrototype(qMetaTypeId<CarbonDBNodeIter*>(),
    engine->newQObject(dbNodeIterProto));

  ProtoCarbonDBNode* dbNodeProto = new ProtoCarbonDBNode();
  engine->setDefaultPrototype(qMetaTypeId<CarbonDBNode*>(),
    engine->newQObject(dbNodeProto));
}

