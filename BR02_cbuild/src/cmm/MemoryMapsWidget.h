#ifndef MEMORYMAPSWIDGET_H
#define MEMORYMAPSWIDGET_H

#include "util/CarbonPlatform.h"

#include <QWidget>
#include "ui_MemoryMapsWidget.h"
#include "util/UtString.h"
#include "Mdi.h"
#include "MdiProject.h"

class SpiritMemoryMap;
class SpiritAddressBlock;
class CarbonMakerContext;

class MemoryMapsWidget : public QWidget, public MDIWidget
{
  Q_OBJECT

public:
  MemoryMapsWidget(CarbonMakerContext* ctx=0, MDIDocumentTemplate* doct=0, QWidget *parent = 0);
  ~MemoryMapsWidget();
  bool loadFile(const QString& qFilename);
  virtual const char* userFriendlyName();
  const char* userFriendlyCurrentFile();
  virtual void saveDocument();

protected:
  void closeEvent(QCloseEvent *event);
  bool maybeSave();

private:
  const char* strippedName(const char* fullFileName);
  void populate();
  void showMapUI(SpiritMemoryMap* map);
  void addBank(QTreeWidgetItem* parent, SpiritMemoryMap* map);
  void addAddressBlock(QTreeWidgetItem* parent, SpiritMemoryMap* map);
  void addRegisters(QTreeWidgetItem* parent, SpiritAddressBlock* ab);

private slots:
  void on_treeWidget_itemSelectionChanged();

private:
  CarbonMakerContext* mCtx;
  UtString mFriendlyName;
  UtString mCurFile;

private:
  Ui::MemoryMapsWidgetClass ui;
};

#endif // MEMORYMAPSWIDGET_H
