//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include "Resources.h"
#include "Template.h"

bool Resources::deserialize(Template*, const QDomElement& parent, XmlErrorHandler* eh)
{
  QDomNode n = parent.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    if ("Resources" == element.tagName())
    {
      if (!Resource::deserialize(this, element, eh))
        return false;
    }
    n = n.nextSibling();
  }
  return true;
}

bool Resource::deserialize(Resources* ress,  const QDomElement& parent, XmlErrorHandler* eh)
{
  QDomNode n = parent.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    qDebug() << element.tagName();

    if ("Resource" == element.tagName())
    {
      Resource* r = NULL;
      if (element.hasAttribute("id"))
      {
        r = new Resource();
        r->setID(element.attribute("id"));
        r->setText(element.text());
        ress->addResource(r);
      }
      else
      {
        eh->reportProblem(XmlErrorHandler::Error, element, "Resource missing 'id' attribute");
        return false;
      }
    }
    n = n.nextSibling();
  }
  return true;
}

void Resources::addResource(Resource* res)
{
  QVariant v = qVariantFromValue((QObject*)res);
  QString name = res->getID().replace(" ", "");
  setProperty(name.toAscii(), v); // this_toascii_ok
  mResources.append(res);
}

