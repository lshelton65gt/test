//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "WizDlgExecuteScript.h"
#include "ModelKitWizard.h"
#include "DlgPickItems.h"
#include "cmm.h"
#include "CarbonFiles.h"

WizDlgExecuteScript::WizDlgExecuteScript(QWidget *parent, ModelKitWizard* wiz, KitActionExecuteScript* action)
: QDialog(parent)
{
  mWizard = wiz;
  mAction = action;

  ui.setupUi(this);

  ui.pushButtonAddArg->setEnabled(mWizard != NULL);

  if (action)
  {
 	ui.lineEditQuestionID->setText(action->getID());
    ui.lineEditDescription->setText(action->getDescription());
    ui.lineEditScriptDirectory->setText(action->getDefaultDirectory());
    ui.lineEditScriptName->setText(action->getScriptName());
    ui.lineEditArguments->setText(action->getScriptArguments().join(" "));

    //ui.lineEditDialogTitle->setText(action->getDialogTitle());
    //ui.lineEditFileDestDir->setText(action->getDefaultDirectory());
    //ui.lineEditFilename->setText(action->getDefaultFileName());
    //ui.lineEditVariableName->setText(action->getVariableName());
    //ui.lineEditFileTypes->setText(action->getFileTypes().join(";"));
  }
}

KitUserAction* WizDlgExecuteScript::createAction()
{
  KitActionExecuteScript* action = new KitActionExecuteScript();
  applyData(action);
  return action;
}

void WizDlgExecuteScript::applyData(KitActionExecuteScript* action)
{
  action->setID(ui.lineEditQuestionID->text());
  action->setScriptName(ui.lineEditScriptName->text());
  action->setDefaultDirectory(ui.lineEditScriptDirectory->text());
  QString cmdLine = ui.lineEditArguments->text().trimmed();
  action->setScriptArguments(cmdLine.split(" "));

  // Base Class
  action->setDescription(ui.lineEditDescription->text());
}

WizDlgExecuteScript::~WizDlgExecuteScript()
{
}


void WizDlgExecuteScript::on_pushButtonBrowseScript_clicked()
{
  QString fileName = QFileDialog::getOpenFileName(this, "Script File", ".", "Filename (* *.*)");
  if (!fileName.isEmpty())
  {
    QFileInfo fi(fileName);
    QString normalizedName = fi.fileName();
    if (mWizard)
    {
      normalizedName = mWizard->normalizeName(fi.filePath());
      normalizedName = normalizedName.replace("${ARM_ROOT}", "${CARBON_PROJECT_ARM}");
    }

    QFileInfo fi2(normalizedName);

    ui.lineEditScriptName->setText(fi2.fileName());
    ui.lineEditScriptDirectory->setText(fi2.path());
  }
}

bool WizDlgExecuteScript::checkInputs()
{
  QString scriptName = ui.lineEditScriptName->text();

  if (scriptName.isEmpty())
  {
    showError("Script name must not be empty");
    ui.lineEditScriptName->setFocus();
    return false;
  }

  QString scriptDir = ui.lineEditScriptDirectory->text();

  if (scriptName.isEmpty() || !scriptDir.startsWith("${"))
  {
    showError("Script directory must not be empty and must start with ${var}");
    ui.lineEditScriptDirectory->setFocus();
    return false;
  }
  return true;
}

void WizDlgExecuteScript::on_buttonBox_accepted()
{
  if (checkInputs())
  {
    if (mAction)
      applyData(mAction);
    accept();
  }
}

void WizDlgExecuteScript::on_buttonBox_rejected()
{
  reject();
}

void WizDlgExecuteScript::on_pushButtonBrowseScriptDir_clicked()
{

}

void WizDlgExecuteScript::on_pushButtonAddArg_clicked()
{
  if (mWizard)
  {
    QStringList items;
    foreach (KitUserAction* action, mWizard->getPreUserActions())
    {
      QString var = action->getVariableName();
      if (!var.isEmpty())
        items.append(var);
    }
    DlgPickItems dlg(this);
    dlg.addItems(items);
    if (dlg.exec() == QDialog::Accepted)
    {
      QString values = dlg.getValues();
      if (!values.isEmpty())
        ui.lineEditArguments->setText(ui.lineEditArguments->text() + " " + dlg.getValues());
    }
  }
}

void WizDlgExecuteScript::on_pushButtonAddArg_2_clicked()
{
  if (mWizard)
  {
    QStringList items;
    foreach (KitUserAction* action, mWizard->getPreUserActions())
    {
      QString var = action->getVariableName();
      if (!var.isEmpty())
        items.append(var);
    }
    DlgPickItems dlg(this);
    dlg.addItems(items);
    if (dlg.exec() == QDialog::Accepted)
    {
      QString values = dlg.getValues();
      if (!values.isEmpty())
        ui.lineEditScriptName->setText(dlg.getValues());
    }
  }
}
