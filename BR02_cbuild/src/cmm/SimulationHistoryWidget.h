// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _SimulationHistoryWidget_h_
#define _SimulationHistoryWidget_h_

#include "util/CarbonPlatform.h"
#include "../gui/ReplayPerfPlot.h"
#include "util/UtArray.h"

#include <QtGui>

struct CarbonReplaySystem;
class CarbonRuntimeState;

class SimulationHistoryWidget : public ReplayPerfPlot
{
  Q_OBJECT

public:
  SimulationHistoryWidget(QWidget *parent);
  virtual ~SimulationHistoryWidget();

  void putXAxisCombo(QComboBox* combo) { mXAxisCombo = combo; }

public slots:
  void carbonSystemUpdated(CarbonRuntimeState &state, CarbonReplaySystem &guiSystem, CarbonReplaySystem &simSystem);
  void changeXAxis(int);

private:
  QComboBox *mXAxisCombo; 
  UtArray<ReplayPerfPlot::XAxis> mXAxisValues; // maps combo box item to ReplayPerfPlot::XAxis
};

#endif

