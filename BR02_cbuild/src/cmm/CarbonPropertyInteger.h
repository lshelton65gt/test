#ifndef __CARBONPROPINTEGER_H__
#define __CARBONPROPINTEGER_H__
#include "util/CarbonPlatform.h"
#include <QObject>

#include "CarbonProperty.h"
#include "PropertyEditor.h"

class CarbonPropertyInteger : public CarbonProperty
{
public:
  CARBONMEM_OVERRIDES

  CarbonPropertyInteger(const char* name, int nv, const char* descr) : CarbonProperty(name,nv,descr,CarbonProperty::Integer)
  {
    // Only valid if range is valid
    mMin = -1;
    mMax = -1;
    mRange = false;
  }

  virtual CarbonDelegate* createDelegate(QObject* parent,  PropertyEditorDelegate* d, PropertyEditor* propEd);
  virtual bool parseXML(xmlNodePtr parent, UtXmlErrorHandler* eh);
  virtual bool writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* eh);
  virtual bool readValueXML(xmlNodePtr parent, CarbonOptions* options, UtXmlErrorHandler* eh) const;

  bool hasRange() const { return mRange; }
  int getMinimum() const { return mMin; }
  void putMinimum(int newVal)
  {
    mMin=newVal;
    mRange=true;
  }

  int getMaximum() const { return mMax; }
  void putMaximum(int newVal)
  {
    mMax=newVal;
    mRange=true;
  }

private:
  int mMin;
  int mMax;
  bool mRange;
};

class CarbonDelegateInteger : public CarbonDelegate
{
  Q_OBJECT
public:
  CarbonDelegateInteger(QObject *parent = 0, PropertyEditorDelegate* d=0, PropertyEditor* e=0);
  QWidget* createEditor(QWidget *parent, QTreeWidgetItem* item, CarbonProperty* prop, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const;

private slots:
  void commitAndCloseEditor();

};
#endif
