#ifndef __CARBONPROPSTRING_H__
#define __CARBONPROPSTRING_H__

#include "util/CarbonPlatform.h"
#include <QObject>
#include "CarbonProperty.h"
#include "PropertyEditor.h"

class CarbonPropertyString : public CarbonProperty
{
public:
  CARBONMEM_OVERRIDES

  virtual CarbonDelegate* createDelegate(QObject* parent,  PropertyEditorDelegate* d, PropertyEditor* propEd);
  virtual bool parseXML(xmlNodePtr parent, UtXmlErrorHandler* eh);
  virtual bool writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* eh);
  virtual bool readValueXML(xmlNodePtr parent, CarbonOptions* options, UtXmlErrorHandler* eh) const;
  CarbonPropertyString(const char* name, int nv, const char* descr) : CarbonProperty(name,nv,descr,CarbonProperty::String)
  {
    mEditor=NULL;
  }

private:
  PropertyEditor* mEditor;
};

class CarbonDelegateString : public CarbonDelegate
{
  Q_OBJECT
public:
  CarbonDelegateString(QObject *parent = 0, PropertyEditorDelegate* d=0, PropertyEditor* e=0);
  QWidget* createEditor(QWidget *parent, QTreeWidgetItem* item, CarbonProperty* prop, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const;

  private slots:
    void commitAndCloseEditor();

};

// A DateStamp property

class CarbonPropertyDate : public CarbonProperty
{
public:
  CARBONMEM_OVERRIDES

  virtual CarbonDelegate* createDelegate(QObject* parent,  PropertyEditorDelegate* d, PropertyEditor* propEd);
  virtual bool parseXML(xmlNodePtr parent, UtXmlErrorHandler* eh);
  virtual bool writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* eh);
  virtual bool readValueXML(xmlNodePtr parent, CarbonOptions* options, UtXmlErrorHandler* eh) const;
  CarbonPropertyDate(const char* name, int nv, const char* descr) : CarbonProperty(name,nv,descr,CarbonProperty::DateStamp)
  {
    mEditor=NULL;
  }

private:
  PropertyEditor* mEditor;
};

class CarbonDelegateDate : public CarbonDelegate
{
  Q_OBJECT
public:
  CarbonDelegateDate(QObject *parent = 0, PropertyEditorDelegate* d=0, PropertyEditor* e=0);
  QWidget* createEditor(QWidget *parent, QTreeWidgetItem* item, CarbonProperty* prop, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const;

  private slots:
    void commitAndCloseEditor();

};
#endif
