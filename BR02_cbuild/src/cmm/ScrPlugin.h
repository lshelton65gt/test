#ifndef __SCRPLUGIN_H__
#define __SCRPLUGIN_H__

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QtGui>

#include <QtScript/QScriptable>
#include <QtScript/QScriptClass>
#include <QtScript/QScriptString>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>


class ScrPlugin : public QObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("EnumPrefix", "Plugin");

public:
  enum ScrPluginFlags { PluginWindowTop, PluginWindowDocked, PluginWindowMDI };

  Q_PROPERTY(QString title READ getTitle WRITE setTitle)
  Q_PROPERTY(ScrPluginFlags flags READ getFlags WRITE setFlags)
  Q_PROPERTY(QString uiFile READ getUIFile WRITE setUIFile)
  Q_PROPERTY(QString constructor READ getCtor WRITE setCtor)
  Q_PROPERTY(int dockArea READ getAreaInt WRITE setArea)
  Q_PROPERTY(bool dialog READ getDialog WRITE setDialog)

  Q_ENUMS(ScrPluginFlags);

public slots:

 
  void setFlags(ScrPluginFlags newVal) { mFlags=newVal; }
  ScrPluginFlags getFlags() const { return mFlags; }

  void setUIFile(const QString& newVal) { mUIFile=newVal; }
  QString getUIFile() const { return mUIFile; }

  void setCtor(const QString& newVal) { mCtor=newVal; }
  QString getCtor() const { return mCtor; }

  void setDialog(bool newVal) { mIsDialog=newVal; }
  bool getDialog() const { return mIsDialog; }

  void setTitle(const QString& newVal) { mTitle=newVal; }
  QString getTitle() const { return mTitle; }

  void setArea(int newVal) { mArea=static_cast<Qt::DockWidgetArea>(newVal); }
  int getAreaInt() const { return static_cast<int>(mArea); }
  Qt::DockWidgetArea getArea() const { return mArea; }

public:
  ScrPlugin(QObject* parent=0) : QObject(parent)
  {
    mUIFile = PluginWindowTop;
    mArea = Qt::BottomDockWidgetArea;
    mIsDialog = false;
  }
  static void registerTypes(QScriptEngine* engine);

private:
  Qt::DockWidgetArea mArea;
  ScrPluginFlags mFlags;
  QString mUIFile;
  QString mCtor;
  QString mTitle;
  bool mIsDialog;
};

#endif
