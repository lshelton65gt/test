#ifndef COWAREWIZARDWIDGET_H
#define COWAREWIZARDWIDGET_H
#include "util/CarbonPlatform.h"

#include <QWidget>
#include "ui_CowareWizardWidget.h"
#include "util/UtString.h"
#include "Mdi.h"
#include "MdiTextEditor.h"
#include "PortView.h"
#include "RegisterTableWidget.h"
#include "DialogAdvancedBinding.h"
#include "CowareApplicationContext.h"
#include "RemoteConsole.h"
#include "CarbonConsole.h"

class CompWizardPortEditor;
class CompWizardMemEditor;
class CodeGenerator;
class CarbonMakerContext;
class CowareComponent;
class SystemCComponent;

class ChangeOutputDirectory
{
public:
  ChangeOutputDirectory(CowareComponent* coware);
  ChangeOutputDirectory(SystemCComponent* systemc);
  ~ChangeOutputDirectory();

private:
  UtString mCurrDir;
};

class CowareWizardWidget;

class MakerCowareApplicationContext : public CowareApplicationContext
{
public:
  MakerCowareApplicationContext(CowareWizardWidget* wiz, CarbonDB *db, CarbonCfgID cfg, CQtContext *cqt) : CowareApplicationContext(db,cfg,cqt)
  {
    mWizard = wiz;
  }
  virtual void setDocumentModified(bool value);

private:
  CowareWizardWidget* mWizard;
};

class CowareWizardWidget : public QWidget, public MDIWidget
{
  Q_OBJECT

  enum ParseResult {eIODB, eFullDB, eCcfg, eNoMatch};
  enum CowizTabs {tabPORTS=0, tabXTORS, tabREGISTERS, tabMEMORIES, tabBUILD};  

public:
  CowareWizardWidget(CarbonMakerContext* ctx=0, MDIDocumentTemplate* doct=0, QWidget *parent = 0);
  ~CowareWizardWidget();
  bool loadFile(const QString& qFilename);
  virtual const char* userFriendlyName();
  virtual void saveDocument();

  CowareApplicationContext* getCtx() {return mContext;}
  CarbonDB* getDB() { return m_pDB; }
  CarbonCfgID getCcfgID() { return m_cfgID; }
  UtString getCcfgFilename() {return m_ccfgName; }
  bool hasCowareEnvironment() { return mHasEnvironment; }

private: // methods
  static eCarbonMsgCBStatus sMsgCallback(CarbonClientData, CarbonMsgSeverity,
    int number, const char* text,
    unsigned int len);
  bool initialize();
  void populateXtorLibComboBox();
  void populateXtorComboBox(const char* libname);
  ParseResult parseFileName(const char* filename, UtString* baseName);
  bool loadDesignFile(const char* filename, ParseResult parseResult);
  bool loadDatabase(const char* iodbName, bool initPorts);
  bool loadConfig(const QString &fileName, bool* modifiedFlag);
  void warning(QWidget* widget, const QString& msg);
  bool save();
  bool saveAs();
  void closeEvent(QCloseEvent *event);
  bool maybeSave();
  void updateTopModuleName();
  void updateComponentName();

private:
  bool mHasEnvironment;
  PortView* m_pPortView;
  CompWizardPortEditor* mPortEditor;
  CompWizardMemEditor* mMemEditor;
  CarbonAbstractRegisterID m_pReg;
  UtString m_dbName;
  CarbonDB* m_pDB;
  CarbonCfgID m_cfgID;
  CQtContext *mCQt;
  CarbonMsgCBDataID* mMsgCallback;
  UtString m_ccfgName;
  AtomicCache* mAtomicCache;
  SourceLocatorFactory mLocatorFactory;
  CarbonCfgXtorInstance* mXtorInst;
  MakerCowareApplicationContext* mContext;
  CodeGenerator* mCodegen;
  CarbonDBNode* mSelectedNode;
  CarbonDBNode* mMemorySelectedNode;
  CarbonCfgRegister* mReg;
  QAction* openAct;
  QAction* saveAct;
  CarbonMakerContext* mCtx;

private:
  Ui::CowareWizardWidgetClass ui;

private slots:
  void xtorSelectionChanged(CarbonCfgXtorInstance* xinst);
  void xtorBindingSelectionChanged(CarbonCfgXtorConn* xconn);
  void xtorBindingsChanged();
  void xtorLibraryChanged(int index);
  void selectionChanged(CarbonDBNode* node);
  void fieldSelectionChanged(CarbonCfgRegisterField* field);
  void registerSelectionChanged(CarbonCfgRegister* reg);
  void tabChanged(int index);
  void memSelectionChanged(CarbonDBNode* node);

  void on_btnAdd_clicked();
  void on_btnDelete_clicked();
  void on_btnDeleteField_clicked();
  void on_btnBindConstant_clicked();
  void on_btnAddField_clicked();

  void compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void outputFilenameChanged(const CarbonProperty*, const char*);
  void projectLoaded(const char* fileName, CarbonProject*);

};

#endif // COWAREWIZARDWIDGET_H
