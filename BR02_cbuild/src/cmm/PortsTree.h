#ifndef PORTSTREE_H
#define PORTSTREE_H
#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include <QTreeWidget>
#include <QTreeWidgetItem>

class Port;
class WizardContext;
class BlockInstance;
class PortInstance;

class PortsTree : public QTreeWidget
{
  Q_OBJECT

  enum { colBLOCKINST, colPORT, colUSERPORT, colDESCRIPTION, colLAST };
    
public:
  PortsTree(QWidget *parent);
  ~PortsTree();

  void populate(WizardContext* templ);
  void commitData();

private slots:
  void itemChanged(QTreeWidgetItem*,int);

private:
  void addItem(PortInstance* portInst);
  WizardContext* mContext;

};

#endif // PORTSTREE_H
