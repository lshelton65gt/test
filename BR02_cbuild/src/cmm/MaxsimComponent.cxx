//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include <QtGui>
#include <QDebug>
#include <QDir>
#include "gui/CQt.h"
#include "CarbonProjectWidget.h"
#include "CarbonProject.h"
#include "CarbonComponents.h"
#include "MaxsimComponent.h"
#include "CarbonOptions.h"
#include "DlgConfirmDelete.h"
#include "DlgPreferences.h"
#include "Mdi.h"
#include "MdiMaxsim.h"
#include "MaxsimWizardWidget.h"
#include "SettingsEditor.h"
#include "WorkspaceModelMaker.h"
#include "RemoteConsole.h"
#include "CarbonConsole.h"
#include "ErrorInfoWidget.h"
#include "cfg/CarbonCfg.h"
#include "cfg/carbon_cfg.h"
#include "cfg/carbon_cfg_misc.h"

#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "shell/carbon_misc.h"
#include "shell/carbon_capi.h"
#include "shell/CarbonDatabasePriv.h"
#include "iodb/IODBRuntime.h"
#include "util/DynBitVector.h"

#include "ModelStudioCommon.h"
#include "CarbonDatabaseContext.h"
#include "CarbonMakerContext.h"
#include "cmm.h"
#include "ScrCcfg.h"
#include "CcfgHelper.h"

#include "ScrProject.h"
#include "ScrApplication.h"
#include "ScrCarbonDB.h"

#define MODEL_64x64 "64-bit compile, 64-bit model"


class MaxsimTool : public CarbonTool
{
public:
  CARBONMEM_OVERRIDES
  MaxsimTool();

};

MaxsimTool::MaxsimTool() : CarbonTool("SOCDesigner", ":cmm/Resources/MaxsimTool.xml")
{
  mProperties.readPropertyDefinitions(":/cmm/Resources/MaxsimComponentOptions.xml");
  mDefaultOptions = new CarbonOptions(NULL, getProperties(), "MaxsimDefaults");
  mOptions = new CarbonOptions(mDefaultOptions, getProperties(), "SOCDesigner");
}

void MaxsimComponent::setComponentName(const QString& compName)
{
  UtString uCompName; uCompName << compName;
  if (mActiveConfiguration)
  {
    qDebug() << "updating SoC Designer component name" << compName;
    CarbonOptions* options = getOptions();
    options->putValue("Component Name", uCompName.c_str());
  }
  else
    qDebug() << "No Maxsim Active Configuration to update component name";
}

QString MaxsimComponent::getDbFilename(const char* configName)
{
  QString symDB = getProject()->getConfigDesignFilePath(configName, ".symtab.db");
  QString ioDB = getProject()->getConfigDesignFilePath(configName, ".io.db");
  QFileInfo fi1(symDB);
  QFileInfo fi2(ioDB);

  if (fi1.exists())
    return symDB;
  else if (fi2.exists())
    return ioDB;
  else
    return symDB; 
 }

QString MaxsimComponent::getDbFilename()
{
  QString symDB = getProject()->getDesignFilePath(".symtab.db");
  QString ioDB = getProject()->getDesignFilePath(".io.db");

  QFileInfo fi1(symDB);
  QFileInfo fi2(ioDB);

  if (fi1.exists())
    return symDB;
  else if (fi2.exists())
    return ioDB;
  else
    return symDB; 
}
ScrCcfg* MaxsimComponent::getCcfg()
{
  // Return it if we have already created it
  if (mCcfg != NULL) {
    return mCcfg;
  }

  QString iodbName = getDbFilename();
  QFileInfo fi(iodbName);
  QString ccfgDir = QString("%1/%2")
    .arg(getProject()->getActive()->getOutputDirectory())
    .arg(MAXSIM_COMPONENT_SUBDIR);

  UtString ccfgFilePath;
  ccfgFilePath << ccfgDir << "/" << fi.baseName() << ".ccfg";


  mCcfg = new ScrCcfg(NULL);
  mCcfg->setFilePath(ccfgFilePath.c_str());

  CcfgEnum::CarbonCfgStatus status = mCcfg->readNoCheck(CcfgEnum::Arm, ccfgFilePath.c_str());

  if (status != CcfgEnum::Failure)
    return mCcfg;
  else
    return NULL;
}

// We are asking for the options here. so update any dynamic values
// before returning the set.
CarbonOptions* MaxsimComponent::getOptions(const char* configName) 
{ 
  CarbonOptions* options = mActiveConfiguration->getOptions("SOCDesigner");
  if (configName != NULL)
    options = getConfigurations()->findConfiguration(configName)->getOptions("SOCDesigner");
  else
    configName = mActiveConfiguration->getName();

  QString iodbName = getDbFilename(configName);
  const char* outDir = getOutputDirectory(configName);

  UtString relIODBName;
  relIODBName << CarbonProjectWidget::makeRelativePath(outDir, iodbName);

  options->putValue("IODB File", relIODBName.c_str());
  options->putValue("Output Directory", outDir);

  return options;
}

void MaxsimComponent::icheckComponent()
{
  // Clear all errors before check
  QDockWidget* dw = getProject()->getProjectWidget()->context()->getWorkspaceModelMaker()->getErrorInfoDockWindow();
  ErrorInfoWidget* eiw = static_cast<ErrorInfoWidget*>(dw->widget());
  eiw->getInfoWidget()->clearAll();

  // Check Component
  bool status = checkComponent(getProject()->getProjectWidget()->getConsole());

  if (status)
  {
    int errors = eiw->getInfoWidget()->getErrors();
    int warnings = eiw->getInfoWidget()->getWarnings();

    // lower followed by raise causes it to "pop" forward
    dw->lower();
    dw->raise();
    getProject()->getProjectWidget()->context()->getMainWindow()->statusBar()->showMessage(tr("Problems found"), 2000);
    QMessageBox::information(NULL, MODELSTUDIO_TITLE,
                             tr("%1 Errors and %2 Warnings were found during pre-compile check(s).")
                             .arg(errors)
                             .arg(warnings), QMessageBox::Ok);
  }
  else
  {
    printf("Dialog now!");
    QMessageBox::information(NULL, MODELSTUDIO_TITLE,
      tr("No Errors or Warnings were found during pre-compile check(s)."), QMessageBox::Ok);
  }
}

int MaxsimComponent::getSourceFiles(QStringList& fileList)
{
  int currSize = fileList.count();
  
  // Add .ccfg file to list
  fileList << getWizardFilename();

  // Add C++ files to list
  foreach(QString file, getCppFiles())
    fileList.push_back(QString ("%1/%2").arg(getOutputDirectory()).arg(file));

  return fileList.count()-currSize;
}

QStringList MaxsimComponent::getCppFiles(CarbonCfg* cfg)
{
  CarbonPropertyValue* topModuleProp = getOptions()->getValue("Component Name", NULL, NULL);
  UtString topModule;
  topModule << topModuleProp->getValue();

  // Set file prefix
  UtString prefix("mx");
  if (cfg) {
    if (cfg->isStandAloneComp()) 
      prefix = "sa";
  }

  QStringList fileList;
  fileList.push_back(QString("%2.%1.h").arg(topModule.c_str()).arg(prefix.c_str()));
  fileList.push_back(QString("%2.%1.cpp").arg(topModule.c_str()).arg(prefix.c_str()));

  if (cfg)
  {

    // Check for CADI source files
    if(cfg->numMemories() > 0 || cfg->numRegisters() > 0) {
      fileList.push_back(QString("%2.%1_CADI.h").arg(cfg->getCompName()).arg(prefix.c_str()));
      fileList.push_back(QString("%2.%1_CADI.cpp").arg(cfg->getCompName()).arg(prefix.c_str()));
    }

    // Loop through sub components and look for CADI source files
    for (UInt32 i = 0; i < cfg->numSubComponents(); ++i) {
      CarbonCfg* sub = cfg->getSubComponent(i);
      if(sub->numMemories() > 0 || sub->numRegisters() > 0) {
        fileList.push_back(QString("%2.%1_CADI.h").arg(sub->getCompName()).arg(prefix.c_str()));
        fileList.push_back(QString("%2.%1_CADI.cpp").arg(sub->getCompName()).arg(prefix.c_str()));
      }
    }
  }

  return fileList;
}

QStringList MaxsimComponent::getCppFiles()
{
  // If mCfg is setup use that otherwise try to read the ccfg file to get the information
  CarbonCfg* cfg = mCfg;
  if (!cfg) {
    cfg = new CarbonCfgTop();
    // If read failed delete cfg again
    if (readCcfg(cfg, ccfgName()) == eCarbonCfgSuccess) {
      delete cfg;
      cfg = NULL;
    }
  }
  
  QStringList fileList = getCppFiles(cfg);

  // If mCfg is null, we must have new'ed a ccfg object so clean up after ourselfs.
  if (mCfg == NULL)
    delete cfg;
  
  return fileList;
}

// Files generated after compiling the component
int MaxsimComponent::getComponentFiles(QStringList& fileList)
{
  int startSize = fileList.count();

  for (UInt32 i=0; i<numPackageFiles(); i++)
  {
    QString file = QString("%1/%2").arg(getOutputDirectory()).arg(getPackageFile(i));
    fileList.push_back(file);
  }

  return fileList.count() - startSize;
}

// Files generated by CarMgr
int MaxsimComponent::getGeneratedFiles(QStringList& fileList)
{  
  int startSize = fileList.count();
  INFO_ASSERT(mActiveConfiguration, "Expecting config");

  bool windowsTarget = isWindowsTarget(mActiveConfiguration);

  CarbonPropertyValue* topModuleProp = getOptions()->getValue("Component Name", NULL, NULL);
  QString componentName = topModuleProp->getValue();

  if (componentName.isEmpty())
  {
    QString iodbName = getDbFilename();
    QFileInfo fi(iodbName);
    QString ccfgDir = QString("%1/%2")
      .arg(getProject()->getActive()->getOutputDirectory())
      .arg(MAXSIM_COMPONENT_SUBDIR);

    UtString ccfgFilePath;
    ccfgFilePath << ccfgDir << "/" << fi.baseName() << ".ccfg";

    ScrCcfg ccfg;
    ccfg.readNoCheck(CcfgEnum::Arm, ccfgFilePath.c_str());
    componentName = ccfg.getCompName();
  }


  if (windowsTarget)
  {
    QString directory = getOutputDirectory();
    QDir dir(directory);
    dir.setFilter(QDir::Files);
    // dir.setNameFilters(fileKinds);
    foreach (QFileInfo fi, dir.entryInfoList())
    {
      if (fi.isFile())
      {
        QString ext = fi.suffix().toLower();
        if ((ext == "cpp") || (ext == "h")) {
          qDebug() << fi.filePath();
          fileList.push_back(fi.filePath());
        }
      }
    }
  }
  else
  {
    foreach(QString file, getCppFiles())
      fileList.push_back(QString("%1/%2").arg(getOutputDirectory()).arg(file));
  }

  QString iodbName = getDbFilename();
  QFileInfo fi(iodbName);  
  QString baseName = fi.baseName();

  CarbonCfgTop cfg;
  readCcfg(&cfg, ccfgName());

  QString prefix("mx");
  if (cfg.isStandAloneComp()) prefix = "sa";

  if (windowsTarget)
    fileList.push_back(QString("%1/Makefile.%2.%3.windows").arg(getOutputDirectory()).arg(baseName).arg(prefix));
  else
    fileList.push_back(QString("%1/Makefile.%2.%3").arg(getOutputDirectory()).arg(baseName).arg(prefix));

  return fileList.count() - startSize;
}

bool MaxsimComponent::checkComponent(CarbonConsole* console)
{
  bool errorFlag = false;

  CarbonOptions* options = getProject()->getToolOptions("VSPCompiler");
  QString modelCompilation = options->getValue("Compiler Control")->getValue();
  if (modelCompilation == MODEL_64x64)
  {

    console->processOutput(CarbonConsole::Local, CarbonConsole::Check, 
                            "64 Bit Target Architectures are not Supported for SoC Designer Components");
    return false;
  }
  

  QString iodbName = getDbFilename();
  QFileInfo fi(iodbName);  
  if (!fi.exists())
    return false;

  qDebug() << "Checking SOCDesigner Component";
  
  // Create Config object
  CarbonCfgID cfg = carbonCfgCreate();
  cfg->addScriptExtension(this);

  // This must be done before reading the user transaction definitions
  carbonCfgReadXtorLib(cfg, eCarbonXtorsMaxsim);
  qDebug() << carbonCfgGetErrmsg(cfg);

  // read any user transactor defs
  if (xtorDefFile()) {
    TempChangeDirectory cd(getProject()->getProjectDirectory());
    CarbonCfgStatus cfgStatus = carbonCfgReadXtorDefinitions(cfg, xtorDefFile());
    if(cfgStatus != eCarbonCfgSuccess) {
      errorFlag = true;
      console->processOutput(CarbonConsole::Local, CarbonConsole::Check, carbonCfgGetErrmsg(cfg));
    }
  }
  
  // Need to change directory to the SoCDesigner directory so we can find the .db file
  // while reading the .ccfg file.
  // This must be done after reading the xtor definitions above because if the xtor 
  // definition file is a relative path, it can't be found after changing directory.
  const char* socDesignerDir = getOutputDirectory(mActiveConfiguration->getName());
  TempChangeDirectory cd(socDesignerDir);

  // Read the ccfg file and check integrity
  CarbonCfgStatus cfgStatus = carbonCfgRead(cfg, ccfgName());
  errorFlag |= (cfgStatus != eCarbonCfgSuccess) || carbonCfgGetNumWarnings(cfg);
  
  // To be able to proccess the errors and warnings, we need to emit one line at a time to the console
  UtString msg = carbonCfgGetErrmsg(cfg);
  
  QStringList items = QString(msg.c_str()).split('\n');
  foreach (QString line, items)
  {
    UtString tmp;
    tmp << line;
    console->processOutput(CarbonConsole::Local, CarbonConsole::Check, tmp.c_str());
  }

  return errorFlag;
}

const char* MaxsimComponent::ccfgName()
{
  QFileInfo fi(mIODBName.c_str());
  UtString baseName;
  baseName << fi.baseName() << ".ccfg";
  UtString ccfgName;
  mCcfgName.clear();
  OSConstructFilePath(&ccfgName, getOutputDirectory(), baseName.c_str());
  mCcfgName << ccfgName;
  return mCcfgName.c_str();
}

const char* MaxsimComponent::getOutputDirectory(const char* cfgName)
{
  mOutputDir.clear();

  CarbonProject* proj = getProject();

  if (cfgName == NULL && proj && proj->getActive())
    cfgName = getProject()->getActive()->getName();

  if (mActiveConfiguration == NULL && mConfigs)
    mActiveConfiguration = mConfigs->findConfiguration(cfgName);

  if (mActiveConfiguration)
  {
    const char* platform = mActiveConfiguration->getPlatform();

    OSConstructFilePath(&mOutputDir, getProject()->getProjectDirectory(), platform);
    OSConstructFilePath(&mOutputDir, mOutputDir.c_str(), cfgName);
    OSConstructFilePath(&mOutputDir, mOutputDir.c_str(), MAXSIM_COMPONENT_SUBDIR);
  }
  else
    qDebug() << "Error: NO active configuration";

  return mOutputDir.c_str(); 
}

const char* MaxsimComponent::getComponentDirectory() const {
  return MAXSIM_COMPONENT_SUBDIR;
}

UInt32 MaxsimComponent::numPackageFiles() {
  // Exporting to SystemC adds an additional wrapper file
  return mActiveConfigSystemCExport ? 5 : 4;
}

QString MaxsimComponent::getPackageFile(UInt32 index) {
  INFO_ASSERT(index < numPackageFiles(), "Index out of range.");

  // This can probably be done more elegantly, but I wanted to
  // generate the filenames dynamically, in case the project name changes for
  // some reason.
  UtString sharedExt, statExt, confExt;
  if (isWindowsTarget(mActiveConfiguration)) {
    sharedExt = ".dll";
    statExt = ".lib";
    confExt = ".windows.conf";
  }
  else {
    sharedExt = ".so";
    statExt = ".a";
    confExt = ".conf";
  }

  // If set, the systemC name should use the module name
  bool scUseComponentName = theApp->checkArgument("-modelKitSCUseComponentName");
  UtString scName;
  if (scUseComponentName) {
    scName << "lib" << componentName();
  } else {
    scName << cModelBaseName();
  }

  UtString pkgName;
  CarbonCfgTop cfg;
  readCcfg(&cfg, ccfgName());
  if (cfg.isStandAloneComp()) {
    switch(index) {
    case 0: pkgName << "lib" << componentName() << ".sa" << statExt; break;
    case 1: pkgName << "sa." << componentName() << ".h"; break;
    case 2: pkgName << scName << ".systemc.h"; break;
    case 3: pkgName << scName << ".systemc.cpp"; break;
    }
  }
  else {
    // Use the display name for the .xpm file if it exists, otherwise
    // use the component name.
    UtString xpmName = cfg.getCompDisplayName();
    if (xpmName.empty()) {
      xpmName = componentName();
    }
    switch(index) {
    case 0: pkgName << "lib" << componentName() << ".mx" << sharedExt; break;
    case 1: pkgName << "lib" << componentName() << ".mx_DBG" << sharedExt; break;
    case 2: pkgName << "maxlib." << cModelBaseName() << confExt; break;
    case 3: pkgName << xpmName << ".xpm"; break;
      // Wrapper file for SystemC export
    case 4: pkgName << "mx.export." << componentName() << ".h"; break;
    }
  }

  return pkgName.c_str();
}

MaxsimComponent::MaxsimComponent(CarbonProject* proj) : CarbonComponent(proj, "MaxsimComponent"),
                                                        mMaxsimSourceItems()
{
  setFriendlyName("SocDesigner");
  setModelKitComponent(true);

  mCcfg = NULL;
  mConfigs = new CarbonConfigurations(proj);
  mRootItem = NULL;
  mActiveConfiguration = NULL;
  mCcfgItem = NULL;
  mMaxsimMkf = NULL; 
  mCfg = 0;
  mDB = NULL;
  mActiveConfigSystemCExport = false;

  mIODBName = "libdesign.symtab.db";

  CarbonConfigurations* configs = proj->getConfigurations();
  for (UInt32 i=0; i<configs->numConfigurations(); i++)
  {
    CarbonConfiguration* projConfig = configs->getConfiguration(i);
    const char* configName = projConfig->getName();

    CarbonConfiguration* config = mConfigs->addConfig("Linux", configName);

    if (mActiveConfiguration == NULL)
      mActiveConfiguration = mConfigs->findConfiguration(proj->getActiveConfiguration());

    config->addTool(new MaxsimTool()); 
    createDirectory(config);
  }

  CQT_CONNECT(proj, configurationChanged(const char*), this, configurationChanged(const char*));
  CQT_CONNECT(proj, configurationRenamed(const char*, const char*), this, configurationRenamed(const char*, const char*));
  CQT_CONNECT(proj, configurationCopy(const char*, const char*), this, configurationCopy(const char*, const char*));
  CQT_CONNECT(proj, configurationRemoved(const char*), this, configurationRemoved(const char*));
  CQT_CONNECT(proj, configurationAdded(const char*), this, configurationAdded(const char*));
  CQT_CONNECT(proj, projectLoaded(const char*,CarbonProject*), this, projectLoaded(const char*,CarbonProject*));
  CQT_CONNECT(proj, postCompilationJavaScriptFinished(), this, postCompilationJavaScriptFinished());

  CQT_CONNECT(getProject()->getProjectWidget(), projectClosing(CarbonProject*), this, projectClosing(CarbonProject*));

  CQT_CONNECT(getProject()->getProjectWidget()->getConsole(), compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType));
  CQT_CONNECT(getProject()->getProjectWidget()->getConsole(), compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType));
  CQT_CONNECT(getProject()->getProjectWidget()->getConsole(), makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType));

  // Only register during creates, not deserialization
  if (proj->getActive())
  {
    CarbonOptions* options = proj->getToolOptions("VSPCompiler");
    options->registerPropertyChanged("-o", "outputFilenameChanged", this);
    getOptions()->registerPropertyChanged("Transactor Definition File", "xtorDefChanged", this);
  }
}

void MaxsimComponent::setXtorDefFile(const char* filename)
{
  CarbonOptions* options = getOptions(mActiveConfiguration->getName());
  if(options)
    options->putValue("Transactor Definition File", filename);
}

const char* MaxsimComponent::xtorDefFile()
{
  const char* filename = NULL;
  CarbonOptions* options = getOptions(mActiveConfiguration->getName());
  if(options) {
    filename = options->getValue("Transactor Definition File")->getValue();
    // return null for empty string
    if(strcmp(filename,"") == 0)
      filename = NULL;
  }
  return filename;
}

void MaxsimComponent::projectClosing(CarbonProject* proj)
{
  disconnect(proj, SIGNAL(configurationChanged(const char*)), this, SLOT(configurationChanged(const char*)));

  disconnect(proj, SIGNAL(configurationRenamed(const char*, const char*)), this, SLOT(configurationRenamed(const char*, const char*)));
  disconnect(proj, SIGNAL(configurationCopy(const char*, const char*)), this,  SLOT(configurationCopy(const char*, const char*)));
  disconnect(proj, SIGNAL(configurationRemoved(const char*)), this,  SLOT(configurationRemoved(const char*)));
  disconnect(proj, SIGNAL(configurationAdded(const char*)), this,  SLOT(configurationAdded(const char*)));
  disconnect(proj, SIGNAL(projectLoaded(const char*,CarbonProject*)), this,  SLOT(projectLoaded(const char*,CarbonProject*)));

  disconnect(proj->getProjectWidget(), SIGNAL(projectClosing(CarbonProject*)), this,  SLOT(projectClosing(CarbonProject*)));

  disconnect(proj->getProjectWidget()->getConsole(), SIGNAL(compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)), this,  SLOT(compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)));
  disconnect(proj->getProjectWidget()->getConsole(), SIGNAL(compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType)), this,  SLOT(compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType)));
  disconnect(proj->getProjectWidget()->getConsole(), SIGNAL(makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType)), this, SLOT( makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType)));

}

void MaxsimComponent::outputFilenameChanged(const CarbonProperty*, const char* newValue)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  QFileInfo fiNew(newValue);
  QFileInfo fiOld(mIODBName.c_str());

  UtString newCcfgName;
  newCcfgName << fiNew.baseName() << ".ccfg";

  UtString oldCcfgName;
  oldCcfgName << fiOld.baseName() << ".ccfg";

  UtString newName;
  OSConstructFilePath(&newName, getOutputDirectory(), newCcfgName.c_str());

  UtString oldName;
  OSConstructFilePath(&oldName, getOutputDirectory(), oldCcfgName.c_str());

  copyAndRenameCcfg(getOutputDirectory(), oldName.c_str(), newName.c_str());

  updateTreeIcons();
  
  QApplication::restoreOverrideCursor();
}

void MaxsimComponent::projectLoaded(const char*, CarbonProject* proj)
{
  CarbonOptions* options = proj->getToolOptions("VSPCompiler");
  options->registerPropertyChanged("-o", "outputFilenameChanged", this);
  getOptions()->registerPropertyChanged("Transactor Definition File", "xtorDefChanged", this);
}
void MaxsimComponent::configurationAdded(const char* name)
{
  qDebug() << "Adding MaxsimComponent Configuration: " << name;
  CarbonConfiguration* cfg = getConfigurations()->findConfiguration(name);
  if (cfg == NULL)
  {
    cfg = mConfigs->addConfig(mActiveConfiguration->getPlatform(), name);
    cfg->addTool(new MaxsimTool());
    createDirectory(cfg);
  }
}

void MaxsimComponent::configurationRenamed(const char* oldName, const char* newName)
{
  CarbonConfiguration* oldConfig = mConfigs->findConfiguration(oldName);
  INFO_ASSERT(oldConfig, "Expecting Configuration");
  oldConfig->putName(newName);
  renameDirectory(oldName, newName);
  updateTreeIcons();
}


bool MaxsimComponent::importCcfg(QString srcCcfgFile, bool checkDB)
{
  bool status = true;

  UtString srcFile;
  srcFile << srcCcfgFile;

  QFileInfo srcFileInfo(srcFile.c_str());

  UtString absPath;
  absPath << srcFileInfo.absolutePath();

  UtString absFilePath;
  absFilePath << srcFileInfo.absoluteFilePath();

  // change dir to the source file
  TempChangeDirectory temp1(absPath.c_str());

  CarbonCfgID cfg = carbonCfgCreate();
  cfg->addScriptExtension(this);

  carbonCfgReadXtorLib(cfg, eCarbonXtorsMaxsim);
  qDebug() << carbonCfgGetErrmsg(cfg);

  // read any user transactor defs
  if (xtorDefFile())
  {
    TempChangeDirectory cd(getProject()->getProjectDirectory());
    CarbonCfgStatus cfgStatus = carbonCfgReadXtorDefinitions(cfg, xtorDefFile());
    if(cfgStatus != eCarbonCfgSuccess) {
      CarbonConsole* console = getProject()->getProjectWidget()->getConsole();
      console->processOutput(CarbonConsole::Local, CarbonConsole::Check, carbonCfgGetErrmsg(cfg));
    }
  }

  CarbonCfgStatus cfgStatus;
  if (checkDB)
    cfgStatus = cfg->read(absFilePath.c_str());  
  else
    cfgStatus = cfg->readNoCheck(absFilePath.c_str());  

  if (cfgStatus != eCarbonCfgFailure)
  {
    // copy the filename (already validated for existence)
    // to the current active configuration
    QString iodbName = getDbFilename();
    QFileInfo fi(iodbName);  
    UtString ccfgName;

    ccfgName << fi.baseName() << ".ccfg";

    UtString newName;
    OSConstructFilePath(&newName, getOutputDirectory(), ccfgName.c_str());

    // change dir to the source file
    TempChangeDirectory temp2(getOutputDirectory());

    UtString relIODBName;
    relIODBName << CarbonProjectWidget::makeRelativePath(getOutputDirectory(), iodbName);
   
    carbonCfgPutIODBFile(cfg, relIODBName.c_str());

    CarbonConfiguration* activeConfig = mActiveConfiguration;

    UtString relLibName;
    const char* outputFilename = getProject()->getToolOptions(activeConfig->getName(), "VSPCompiler")->getValue("-o")->getValue();
    QFileInfo ofi(outputFilename);
    UtString libExt;
    libExt << "." << ofi.completeSuffix();

    const char* libFilePath = getProject()->getConfigDesignFilePath(activeConfig->getName(), libExt.c_str());
    relLibName << CarbonProjectWidget::makeRelativePath(getOutputDirectory(), libFilePath);
    carbonCfgPutLibName(cfg, relLibName.c_str());

    // get the interesting values from the ccfg and reflect them here.
    updateGUIFromCcfg(activeConfig, cfg);

    carbonCfgWrite(cfg, newName.c_str());
  }
  else
    status = false;

  updateTreeIcons();

  carbonCfgDestroy(cfg);

  return status;
}

void MaxsimComponent::configurationCopy(const char* oldCfgName, const char* newCfgName)
{
  QFileInfo oldfi(getDbFilename(oldCfgName)); 
  QFileInfo newfi(getDbFilename(newCfgName));

  UtString oldCcfgName;
  oldCcfgName << oldfi.baseName() << ".ccfg";

  UtString newCcfgName;
  newCcfgName << newfi.baseName() << ".ccfg";

  UtString newName;
  OSConstructFilePath(&newName, getOutputDirectory(newCfgName), newCcfgName.c_str());

  UtString oldName;
  OSConstructFilePath(&oldName, getOutputDirectory(oldCfgName), oldCcfgName.c_str());
  
  UtString errInfo;
  OSCopyFile(oldName.c_str(), newName.c_str(), &errInfo);

  CarbonOptions* oldOptions = getOptions(oldCfgName);
  CarbonOptions* newOptions = getOptions(newCfgName);

  oldOptions->setBlockSignals(true);
  newOptions->setBlockSignals(true);

  for (CarbonOptions::OptionsLoop p = oldOptions->loopOptions(); !p.atEnd(); ++p)
  {
    CarbonPropertyValue* sv = p.getValue();
    const char* value = sv->getValue();
    const char* propName = sv->getProperty()->getName();
    newOptions->putValue(propName, sv->getProperty()->getDefaultValue());
    newOptions->putValue(propName, value);
  }

  oldOptions->setBlockSignals(false);
  newOptions->setBlockSignals(false);

  updateTreeIcons();
}

void MaxsimComponent::configurationRemoved(const char* name)
{
  qDebug() << "Removing MaxsimComponent Configuration: " << name;
  mConfigs->removeConfiguration(name);
}

void MaxsimComponent::renameDirectory(const char* oldName, const char* newName)
{
  UtString oldDir;
  UtString newDir;

  OSConstructFilePath(&oldDir, getProject()->getProjectDirectory(), mActiveConfiguration->getPlatform());
  OSConstructFilePath(&oldDir, oldDir.c_str(), oldName);
  OSConstructFilePath(&oldDir, oldDir.c_str(), MAXSIM_COMPONENT_SUBDIR);

  OSConstructFilePath(&newDir, getProject()->getProjectDirectory(), mActiveConfiguration->getPlatform());
  OSConstructFilePath(&newDir, newDir.c_str(), newName);
  OSConstructFilePath(&newDir, newDir.c_str(), MAXSIM_COMPONENT_SUBDIR);

  QDir dir;
  if (!dir.rename(oldDir.c_str(), newDir.c_str()))
    qDebug() << "failed to rename directory!";

}

void MaxsimComponent::createConfigurations(MaxsimComponent* comp, xmlNodePtr parent)
{
  // first make sure the configurations exist for later deserialization
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "Configurations"))
    {
      for (xmlNodePtr configChild = child->children; configChild != NULL; configChild = configChild->next) 
      {
        if (XmlParsing::isElement(configChild, "Configuration"))
        {
          UtString configName;
          XmlParsing::getProp(configChild, "Name", &configName);
          QString cname = configName.c_str();
          QStringList configParts = cname.split('|');

          QString qname = configParts[0];
          QString qplat = configParts[1];

          UtString name;
          name << qname;
          UtString platform;
          platform << qplat;

          CarbonConfiguration* cfg = comp->getConfigurations()->findConfiguration(name.c_str());
          if (cfg == NULL)
          {
            cfg = comp->getConfigurations()->addConfig(platform.c_str(), name.c_str());
            cfg->addTool(new MaxsimTool());
            createDirectory(cfg);
          }
        }
      }
    }
  }
}

// Create the directory for the configuration
void MaxsimComponent::createDirectory(CarbonConfiguration* config)
{
  UtString outDir;

  OSConstructFilePath(&outDir, config->getProject()->getProjectDirectory(), config->getPlatform());
  OSConstructFilePath(&outDir, outDir.c_str(), config->getName());
  OSConstructFilePath(&outDir, outDir.c_str(), MAXSIM_COMPONENT_SUBDIR);

  QDir q(outDir.c_str());
  if (!q.exists())
  {
    qDebug() << "Creating directory: " << outDir.c_str();
    q.mkpath(outDir.c_str());
  }
}

void MaxsimComponent::xtorDefChanged(const CarbonProperty*, const char* newValue)
{
  qDebug() << "new defs: " << newValue;
  QFile q(newValue);
  if (!q.exists())
    return;
  
  setXtorDefFile(newValue);

  emit xtorDefFileChanged();
}

void MaxsimComponent::configurationChanged(const char* newConfig)
{
  qDebug() << "MaxsimComponent Config changed to: " << newConfig;

  mActiveConfiguration = mConfigs->findConfiguration(newConfig);
  INFO_ASSERT(mActiveConfiguration, "Unable to locate configuration");
  createDirectory(mActiveConfiguration);
  updateTreeIcons();

  CarbonOptions* options = getOptions(newConfig);
  options->registerPropertyChanged("Transactor Definition File", "xtorDefChanged", this);
}

MaxsimComponent::~MaxsimComponent()
{
  CarbonProject* proj = getProject();
  if(proj) {
    CarbonOptions* options = proj->getToolOptions("VSPCompiler");
    if(options)
      options->unregisterPropertyChanged("-o", this);
  }
  CarbonOptions* options = getOptions();
  if(options)
    options->unregisterPropertyChanged("Transactor Definition File", this);
}

MaxsimComponent* MaxsimComponent::deserialize(CarbonProject* proj, xmlNodePtr parent, UtXmlErrorHandler* eh)
{
  MaxsimComponent* comp = NULL;

  // Is this a MaxsimComponent?
  if (XmlParsing::isElement(parent, "MaxsimComponent"))
  {
    comp = new MaxsimComponent(proj);
   
    createConfigurations(comp, parent);

    for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
    {
      if (XmlParsing::isElement(child, "Configurations"))
        comp->getConfigurations()->deserialize(child, eh);
    }
  }

  return comp;
}

bool MaxsimComponent::serialize(xmlTextWriterPtr writer)
{
  xmlTextWriterStartElement(writer, BAD_CAST "MaxsimComponent");
 
  mConfigs->serialize(writer);

  xmlTextWriterEndElement(writer);
  
  return true;
}

void MaxsimComponent::valueChanged(const CarbonProperty* prop, const char* newValue)
{
  if (mRootItem && (0 == strcmp(prop->getName(), "Component Name")))
  {
    for (int i=0; i<mRootItem->childCount(); i++)
    {
      MaxsimSourceItem* item = dynamic_cast<MaxsimSourceItem*>(mRootItem->child(i));
      if (item)
      {
        QString fname = item->text(0);
        if (fname.contains(".cpp") || fname.contains(".h"))
        {
          QFileInfo fi(fname);
          UtString newName;
          newName << fi.baseName() << "." << newValue << "." << fi.suffix();
          item->setText(0, newName.c_str());
        }
      }
    }
  }
  updateTreeIcons();
}

void MaxsimComponent::setEnabled(bool value)
{
  if (mRootItem)
    mRootItem->setDisabled(!value);
}

CarbonComponentTreeItem* MaxsimComponent::createTreeItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent)
{
  // First clear old source items
  mMaxsimSourceItems.clear();

  MaxsimTreeItem* item = NULL;

  getOptions()->registerPropertyChanged("Component Name", "valueChanged", this);

  if (parent)
    item = new MaxsimTreeItem(proj, this, parent);
  else
  {
    item = new MaxsimTreeItem(proj, this);
    proj->addTopLevelItem(item);
  }
  item->setText(0, "SoC Designer Component");

  mCcfgItem = new MaxsimCcfgItem(proj, item, this);
  mMaxsimMkf = new MaxsimSourceItem(proj, item, this);

  item->setExpanded(true);

  mRootItem = item;

  updateTreeIcons();

  return item;
}

void MaxsimComponent::createDefaultComponent()
{
  QString iodbName = getProject()->getDatabaseFilePath();
  QFileInfo fi(iodbName);
  QString ccfgDir = QString("%1/%2")
    .arg(getProject()->getActive()->getOutputDirectory())
    .arg(MAXSIM_COMPONENT_SUBDIR);

  UtString ccfgFilePath;
  ccfgFilePath << ccfgDir << "/" << fi.baseName() << ".ccfg";

  CarbonMakerContext* cmContext = theApp->getContext();

    // Build the leaf widgets that hold the content for the configuration
  mCfg = carbonCfgCreate();
  mCfg->addScriptExtension(this);

  mCfg->putLegacyMemories(0); // New ccfg files do not use legacy

  CQtContext* cqt = cmContext->getQtContext();

  if (carbonCfgReadXtorLib(mCfg, eCarbonXtorsMaxsim) == eCarbonCfgFailure)
    cqt->warning(cmContext->getMainWindow(), carbonCfgGetErrmsg(mCfg));
  else
    qDebug() << carbonCfgGetErrmsg(mCfg);

  if (xtorDefFile())
  {
    TempChangeDirectory cd(getProject()->getProjectDirectory());
    CarbonCfgStatus cfgStatus = carbonCfgReadXtorDefinitions(mCfg, xtorDefFile());
    if(cfgStatus != eCarbonCfgSuccess) {
      CarbonConsole* console = getProject()->getProjectWidget()->getConsole();
      console->processOutput(CarbonConsole::Local, CarbonConsole::Check, carbonCfgGetErrmsg(mCfg));
    }
  }

  // read any user transactor defs
  if (cqt->getXtorDefFile() != NULL)
  {
    if (carbonCfgReadXtorDefinitions(mCfg, cqt->getXtorDefFile()) == eCarbonCfgFailure)
      cqt->warning(cmContext->getMainWindow(), carbonCfgGetErrmsg(mCfg));
  }
  UtString iodbname;
  iodbname << iodbName;

  mDB = carbonDBOpenFile(iodbname.c_str());
  if (mDB)
  {
    mCfg->putDB(mDB);

    populateLoop(mDB, eCarbonCfgRTLInput,
                 carbonDBLoopPrimaryInputs(mDB));
    populateLoop(mDB, eCarbonCfgRTLInout,
                 carbonDBLoopPrimaryBidis(mDB));
    populateLoop(mDB, eCarbonCfgRTLOutput,
                 carbonDBLoopPrimaryOutputs(mDB));
    populateLoop(mDB, eCarbonCfgRTLInput,
                 carbonDBLoopScDepositable(mDB));
    populateLoop(mDB, eCarbonCfgRTLOutput,
                 carbonDBLoopScObservable(mDB));

    UtString relIODBName;
    relIODBName << "../" << fi.fileName();

    carbonCfgPutIODBFile(mCfg, relIODBName.c_str());

    UtString relLibName;
    const char* outputFilename = getProject()->getToolOptions("VSPCompiler")->getValue("-o")->getValue();
    QFileInfo ofi(outputFilename);
    UtString libExt;
    libExt << "." << ofi.completeSuffix();

    const char* libFilePath = getProject()->getDesignFilePath(libExt.c_str());
    relLibName << CarbonProjectWidget::makeRelativePath(ccfgDir, libFilePath);
    carbonCfgPutLibName(mCfg, relLibName.c_str());

    carbonCfgPutCompName(mCfg, carbonDBGetTopLevelModuleName(mDB));
    carbonCfgPutTopModuleName(mCfg, carbonDBGetTopLevelModuleName(mDB));

    // Turn Tie'd ports into Ties
    for(UInt32 i=0; i<mCfg->numESLPorts(); i++)
    {
      CarbonCfgESLPort* eslPort = mCfg->getESLPort(i);
      CarbonCfgRTLPort* rtlPort = eslPort->getRTLPort();
      if (rtlPort)
      {
        const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPort->getName());
        if (carbonDBIsTied(mDB, node))
        {
          const IODBRuntime* iodb = CarbonDatabasePriv::getIODB(mDB);
          const STSymbolTableNode* stNode = CarbonDatabasePriv::getSymTabNode(node);
          const DynBitVector* bv = iodb->getTieValue(stNode);
          UtString valStr;
          bv->format(&valStr, eCarbonHex);
          qDebug() << "RTL Port" << rtlPort->getName() << "tied" << valStr.c_str();
          mCfg->disconnect(eslPort);
          UInt32 tieValue = bv->value();
          carbonCfgTie(rtlPort, &tieValue, 1); 
        }
      }
    }

    if (carbonCfgWrite(mCfg, ccfgFilePath.c_str()) == eCarbonCfgFailure)
    {
      cqt->warning(cmContext->getMainWindow(), tr("Cannot write file %1:\n%2.")
                    .arg(ccfgFilePath.c_str())
                    .arg(carbonCfgGetErrmsg(mCfg)));
    }
  }
}

void MaxsimComponent::populateLoop(CarbonDB* carbonDB,
                                 CarbonCfgRTLPortType type,
                                 CarbonDBNodeIter* iter)
{
  const CarbonDBNode* node;
  while ((node = carbonDBNodeIterNext(iter)) != NULL)
  {
    int width = carbonDBGetWidth(carbonDB, node);
    const char* name = carbonDBNodeGetFullName(carbonDB, node);
    CarbonCfgRTLPortID port = carbonCfgAddRTLPort(mCfg, name, width, type);
    if (port != NULL)
    {
      (void) addConnection(port);
    }
  }
  carbonDBFreeNodeIter(iter);
}

CarbonCfgESLPortID MaxsimComponent::addConnection(CarbonCfgRTLPortID rtlPort)
{
  CarbonCfgESLPortType portType = CarbonCfg::convertRTLPortType(rtlPort);
  return addESLConnection(rtlPort, portType);
}

CarbonCfgESLPortID MaxsimComponent::addESLConnection(CarbonCfgRTLPortID rtlPort,
                                                    CarbonCfgESLPortType type)
{
  // Get a leaf name for the RTL port, use that as a starting point for
  // the ESL name
  const CarbonDBNode* node = carbonDBFindNode(mDB, rtlPort->getName());
  UtString buf;
  if (node == NULL) {
    mCfg->getUniqueESLName(&buf, rtlPort->getName());
  }
  else {
    mCfg->getUniqueESLName(&buf, carbonDBNodeGetLeafName(mDB, node));
  }

  CarbonCfgESLPortID eslPort = carbonCfgAddESLPort(mCfg, rtlPort,
                                                   buf.c_str(), type);

  INFO_ASSERT(eslPort, "Failed to create ESL port");
  return eslPort;
}

void MaxsimComponent::updateTreeTextValues()
{
  if (mRootItem == NULL) return;

  QString iodbName = getDbFilename();
  QFileInfo fi(iodbName);
  UtString MaxsimName;

  bool bCanCompile = canCompile(mActiveConfiguration);

  MaxsimName << fi.baseName() << ".ccfg";

  if (mCcfgItem)
  {
    mCcfgItem->setExists(bCanCompile);
    mCcfgItem->setText(0, MaxsimName.c_str());
  }

  const char* componentName = getOptions()->getValue("Component Name")->getValue();
  if (componentName == NULL || (componentName && strlen(componentName) == 0))
  {
    CarbonDatabaseContext* dbContext = getProject()->getDbContext();
    if (dbContext != NULL && dbContext->getDB() != NULL)
    {
      componentName = carbonDBGetTopLevelModuleName(dbContext->getDB());
      getOptions()->putValue("Component Name", componentName);
    }
  }

  // Get List of source files
  CarbonCfgTop cfg;
  readCcfg(&cfg, ccfgName());

  QStringList srcFileList = getCppFiles(&cfg);

  // Temporarily remove Makefile item from the list while we adjust the size
  mRootItem->removeChild(mMaxsimMkf);

  // Remove extra items from the list, if number of tree items is too large
  while ((int)mMaxsimSourceItems.size() > srcFileList.size()) {
    mRootItem->removeChild(mMaxsimSourceItems.back());
    delete mMaxsimSourceItems.back();
    mMaxsimSourceItems.pop_back();
  }

  // Add extra items to the list, if number of tree items is too small
  while ((int)mMaxsimSourceItems.size() < srcFileList.size()) {
    MaxsimSourceItem* cppItem = new MaxsimSourceItem(getProject()->getProjectWidget(), mRootItem, this);
    mMaxsimSourceItems.push_back(cppItem);
  }

  // Add the makefile item back
  mRootItem->addChild(mMaxsimMkf);

  // At this point the tree should match the number of source items, so just update the text
  for(UInt32 i = 0; i < mMaxsimSourceItems.size(); ++i) {
    mMaxsimSourceItems[i]->setText(0, srcFileList[i]);
  }

  // Update Makfile name
  UtString prefix(".mx");
  if (cfg.isStandAloneComp()) prefix = ".sa";
  MaxsimName.clear();
  if (isWindowsTarget(mActiveConfiguration))
    MaxsimName << "Makefile." << fi.baseName() << prefix << ".windows";
  else
    MaxsimName << "Makefile." << fi.baseName() << prefix;

  if (mMaxsimMkf)
    mMaxsimMkf->setText(0, MaxsimName.c_str());
}

UtString MaxsimComponent::componentName()
{
  CarbonOptions* options = getOptions();
  UtString compName(options->getValue("Component Name")->getValue());
  return compName;
}

UtString MaxsimComponent::cModelBaseName()
{
  CarbonConfiguration* cfg = getProject()->getActive();
  const char* fileName = getProject()->getToolOptions(cfg->getName(), "VSPCompiler")->getValue("-o")->getValue();
  QFileInfo fi(fileName);
  UtString cModelBase;
  cModelBase << fi.baseName();
  return cModelBase;
}

void MaxsimComponent::compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  setEnabled(false);
}

void MaxsimComponent::makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
}


void MaxsimComponent::compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  updateTreeIcons();
  setEnabled(true);
}

// Refresh the icons for MaxsimSourceItems
void MaxsimComponent::updateTreeIcons()
{
  QString iodbName = getDbFilename();
  QFileInfo fi(iodbName);

  mIODBName.clear();
  mIODBName << iodbName;

  updateTreeTextValues();

  if (mRootItem)
  {
    const char* od = getOutputDirectory();
    for (int i=0; i<mRootItem->childCount(); i++)
    {
      QTreeWidgetItem* item = mRootItem->child(i);
      MaxsimSourceItem* srcItem = dynamic_cast<MaxsimSourceItem*>(item);
      if (srcItem)
      {
        UtString fpath;
        UtString name;
        name << srcItem->text(0);
        OSConstructFilePath(&fpath, od, name.c_str());
        QFile file(fpath.c_str());
        srcItem->setExists(file.exists());
      }
    }
  }
}

bool MaxsimComponent::isWindowsTarget(CarbonConfiguration* cfg)
{
  if (cfg == NULL)
    cfg = mActiveConfiguration;

  INFO_ASSERT(cfg, "Expecting active configuration");

  const char* outputFilename = getProject()->getToolOptions(cfg->getName(), "VSPCompiler")->getValue("-o")->getValue();
  QFileInfo ofi(outputFilename);

  QString outputType = ofi.completeSuffix().toLower();

  if (outputType == "lib" || outputType == "dll")
    return true;
  else
    return false;
}

const char* MaxsimComponent::getTargetName(CarbonConfiguration*,TargetType type, TargetPlatform)
{
  static UtString targetName;
  targetName.clear();

  switch (type)
  {
  case CarbonComponent::Build: targetName << MAXSIM_COMPONENT_TARGET; break;
  case CarbonComponent::Package: break;
  case CarbonComponent::Clean: targetName << MAXSIM_COMPONENT_TARGET_CLEAN; break;
  }

  return targetName.c_str();
}

void MaxsimComponent::updateCcfg(CarbonConfiguration* activeConfig, const char* iodbName)
{
  qDebug() << "Writing Maxsim .ccfg file";
  const char* outDir = getOutputDirectory(activeConfig->getName());

  TempChangeDirectory cd(outDir);

  QFileInfo fi(iodbName);
  if (!fi.exists())
    return;

  UtString relIODBName;
  relIODBName << CarbonProjectWidget::makeRelativePath(outDir, iodbName);

  UtString ccfgPath;
  UtString ccfgName;
  ccfgName << fi.baseName() << ".ccfg";

  OSConstructFilePath(&ccfgName, outDir, ccfgName.c_str());

  CarbonCfgID cfg = carbonCfgCreate();
  cfg->addScriptExtension(this);

  CarbonDB* newDB = carbonDBOpenFile(iodbName);
  cfg->putDB(newDB);

  carbonCfgReadXtorLib(cfg, eCarbonXtorsMaxsim);
  qDebug() << carbonCfgGetErrmsg(cfg);

  // read any user transactor defs
  if (xtorDefFile())
  {
    TempChangeDirectory cd(getProject()->getProjectDirectory());
    CarbonCfgStatus cfgStatus = carbonCfgReadXtorDefinitions(cfg, xtorDefFile());
    if(cfgStatus != eCarbonCfgSuccess) {
      CarbonConsole* console = getProject()->getProjectWidget()->getConsole();
      console->processOutput(CarbonConsole::Local, CarbonConsole::Check, carbonCfgGetErrmsg(cfg));
    }
  }

  // update the library reference (relative)
  UtString relLibName;
  const char* outputFilename = getProject()->getToolOptions(activeConfig->getName(), "VSPCompiler")->getValue("-o")->getValue();
  QFileInfo ofi(outputFilename);
  UtString libExt;
  libExt << "." << ofi.completeSuffix();

  const char* libFilePath = getProject()->getConfigDesignFilePath(activeConfig->getName(), libExt.c_str());
  relLibName << CarbonProjectWidget::makeRelativePath(outDir, libFilePath);
  carbonCfgPutLibName(cfg, relLibName.c_str());

  // update the iodb reference to a relative path
  carbonCfgPutIODBFile(cfg, relIODBName.c_str());
  carbonCfgRead(cfg, ccfgName.c_str());  
  carbonCfgPutCcfgMode(cfg, eCarbonCfgARM);

  const char* componentName = getOptions(activeConfig->getName())->getValue("Component Name")->getValue();
  if (componentName == NULL || (componentName && strlen(componentName) == 0))
  {
    CarbonDatabaseContext* dbContext = getProject()->getDbContext();
    if (dbContext != NULL && dbContext->getDB() != NULL)
    {
      componentName = carbonDBGetTopLevelModuleName(dbContext->getDB());
      getOptions(activeConfig->getName())->putValue("Component Name", componentName);
    }
  }

  carbonCfgPutCompName(cfg, getOptions(activeConfig->getName())->getValue("Component Name")->getValue());
  carbonCfgPutCxxFlags(cfg, getOptions(activeConfig->getName())->getValue("C++ Compile Flags")->getValue());
  carbonCfgPutLinkFlags(cfg, getOptions(activeConfig->getName())->getValue("Linker Flags")->getValue());
  
  carbonCfgPutSourceFiles(cfg, getOptions(activeConfig->getName())->getValue("Source Files")->getValue());
  carbonCfgPutIncludeFiles(cfg, getOptions(activeConfig->getName())->getValue("Include Files")->getValue());
  carbonCfgPutLoadfileExtension(cfg, getOptions(activeConfig->getName())->getValue("Loadfile Extension")->getValue());
  carbonCfgPutUseStaticScheduling(cfg, getOptions(activeConfig->getName())->getValue("Use Static Scheduling")->getBoolValue());

  carbonCfgPutType(cfg, getOptions(activeConfig->getName())->getValue("Component Type")->getValue());

  const char* waveforms = getOptions(activeConfig->getName())->getValue("Waveforms")->getValue();
  if (0 == strcmp(waveforms, "FSDB"))
    carbonCfgPutWaveType(cfg, eCarbonCfgFSDB);
  else if (0 == strcmp(waveforms, "VCD"))
    carbonCfgPutWaveType(cfg, eCarbonCfgVCD);
  else
    carbonCfgPutWaveType(cfg, eCarbonCfgNone);

  carbonCfgPutWaveFilename(cfg, getOptions(activeConfig->getName())->getValue("Waveform File")->getValue());
  carbonCfgWrite(cfg, ccfgName.c_str());
  carbonCfgDestroy(cfg);
}

// Read CCFG file without complaining,
// Returns true if successful.
// The resulting CarbonCfg object will be placed in cfg
bool MaxsimComponent::readCcfg(CarbonCfg* cfg, const char* filename)
{
  // Check that we have a valid cfg object
  if (!cfg) return false;

  // Check if the file exists, if not don't bother doing anything else.
  if (!QFile::exists(filename)) return false;

  // Read transactor libraries, don't coninue if this fails, since we don't
  // know what adverse affect that could hade when using the cfg object.
  if (cfg->readXtorLib(eCarbonXtorsMaxsim) == eCarbonCfgFailure) return false;
  
  const char* userXtorDef = xtorDefFile();
  if (userXtorDef != NULL && strlen(userXtorDef) > 0)
  {
    TempChangeDirectory cd(getProject()->getProjectDirectory());
    CarbonCfgXtorLib* xtorLib = cfg->findXtorLib(CARBON_DEFAULT_XTOR_LIB);
    UtString errmsg;
    bool status = xtorLib->readXtorDefinitions(userXtorDef, &errmsg);
    if(!status) {
      CarbonConsole* console = getProject()->getProjectWidget()->getConsole();
      console->processOutput(CarbonConsole::Local, CarbonConsole::Check, carbonCfgGetErrmsg(cfg));
    }
  }

  // read any user transactor defs
  CQtContext* cqt = theApp->getContext()->getQtContext();
  if (cqt->getXtorDefFile() != NULL)
  {
    CarbonCfgXtorLib* xtorLib = cfg->findXtorLib(CARBON_DEFAULT_XTOR_LIB);
    if (xtorLib) {
      UtString errmsg;
      xtorLib->readXtorDefinitions(cqt->getXtorDefFile(), &errmsg);
    }
  }

  // Now read ccfg file and return status, without checking the db
  return (cfg->readNoCheck(filename) == eCarbonCfgSuccess); 
  
}

void MaxsimComponent::postCompilationJavaScriptFinished()
{
  CarbonCfgTop cfg;
  readCcfg(&cfg, ccfgName());
  updateGUIFromCcfg(mActiveConfiguration, &cfg);
}

void MaxsimComponent::updateGUIFromCcfg(CarbonConfiguration* activeConfig, CarbonCfg* cfg)
{
  CarbonOptions* options = getOptions(activeConfig->getName());
  options->putValue("Component Name", carbonCfgGetCompName(cfg));
  options->putValue("Component Type", carbonCfgGetType(cfg));
  options->putValue("C++ Compile Flags", carbonCfgGetCxxFlags(cfg));
  options->putValue("Linker Flags", carbonCfgGetLinkFlags(cfg));
  options->putValue("Source Files", carbonCfgGetSourceFiles(cfg));
  options->putValue("Include Files", carbonCfgGetIncludeFiles(cfg));
  options->putValue("Loadfile Extension", carbonCfgGetLoadfileExtension(cfg));
  options->putValue("Use Static Scheduling", carbonCfgGetUseStaticScheduling(cfg) ? "true" : "false");

  CarbonCfgWaveType waveType = carbonCfgGetWaveType(cfg);
  switch (waveType)
  {
    case eCarbonCfgNone:
      options->putValue("Waveforms", "None");
      break;
    case eCarbonCfgVCD:
      options->putValue("Waveforms", "VCD");
      break;
    case eCarbonCfgFSDB:
      options->putValue("Waveforms", "FSDB");
      break;
  }
  const char* waveFile = carbonCfgGetWaveFilename(cfg);
  if (waveFile)
    options->putValue("Waveform File", waveFile);
}

void MaxsimComponent::generateMakefile(CarbonConfiguration* activeConfig,const char* /*makefileName*/)
{
  const char* outDir = getOutputDirectory(activeConfig->getName());

  UtString configName;
  OSConstructFilePath(&configName, outDir, "configuration");
  QString buffer;
  QTextStream stream(&buffer);
  stream.setCodec("UTF-8");

  stream << getOptions(activeConfig->getName())->getValue("Component Name")->getValue() << endl;
  stream << getOptions(activeConfig->getName())->getValue("Component Type")->getValue() << endl;
  stream << getOptions(activeConfig->getName())->getValue("C++ Compile Flags")->getValue() << endl;
  stream << getOptions(activeConfig->getName())->getValue("Linker Flags")->getValue() << endl;
  stream << getOptions(activeConfig->getName())->getValue("Source Files")->getValue() << endl;
  stream << getOptions(activeConfig->getName())->getValue("Include Files")->getValue() << endl;

  // Export to SystemC option is only present with the appropriate
  // license.  It can also be forced in the model kit flow with a
  // command line switch.  Save the value for later use.
  mActiveConfigSystemCExport = false;
  if (theApp->checkArgument("-modelKitSystemCExport")) {
    mActiveConfigSystemCExport = true;
  } else {
    CarbonOptions* opts = getOptions(activeConfig->getName());
    const CarbonProperty* exportProp = opts->findProperty("Export to SystemC");
    if (exportProp != NULL) {
      mActiveConfigSystemCExport = opts->getValue(exportProp, NULL, NULL)->getBoolValue();
    }
  }
  stream << (mActiveConfigSystemCExport ? "true" : "false");

  stream << getOptions(activeConfig->getName())->getValue("Loadfile Extension")->getValue() << endl;
  stream << getOptions(activeConfig->getName())->getValue("Use Static Scheduling")->getValue() << endl;
  stream << getOptions(activeConfig->getName())->getValue("Waveforms")->getValue() << endl;
  stream << getOptions(activeConfig->getName())->getValue("Waveform File")->getValue() << endl;
  stream << getProject()->getToolOptions(activeConfig->getName(), "VSPCompiler")->getValue("-o")->getValue() << endl;

  QString iodbName = getDbFilename(activeConfig->getName());
  QFileInfo fi(iodbName);

  UtString relIODBName;
  relIODBName << CarbonProjectWidget::makeRelativePath(outDir, iodbName);
  stream << relIODBName.c_str() << endl;
  
  // Record Changes to standalone
  CarbonCfgTop cfg;
  readCcfg(&cfg, ccfgName());
  stream << cfg.isStandAloneComp();

  UtString idbName;
  idbName << iodbName;

  if (getProject()->writeFileIfChanged(configName.c_str(), &stream))
    updateCcfg(activeConfig, idbName.c_str());
}

bool MaxsimComponent::getTargetImpl(CarbonConfiguration* cfg, QTextStream& stream, TargetType type)
{  
  const char* outDir = getOutputDirectory(cfg->getName());
  CarbonOptions* compilerOptions = getProject()->getToolOptions("VSPCompiler");
  const char* outputFilename = compilerOptions->getValue("-o")->getValue();
  QFileInfo ofi(outputFilename);

  QString outputType = ofi.completeSuffix().toLower();

  UtString sharedExtension;
  bool windowsTarget = false;
  bool onWindows = false;

#if pfWINDOWS
  onWindows = true;
#else
  onWindows = false;
#endif

  DlgPreferences prefs;
  QString prefVSVersion = prefs.getGeneralPreference(DlgPreferences::GeneralVSVersion).toString();


  if (outputType == "lib" || outputType == "dll")
  {
    sharedExtension = ".dll";
    windowsTarget = true;
  }
  else
    sharedExtension = ".so";

  QString iodbName = getDbFilename(cfg->getName());
  QFileInfo fi(iodbName);

  CarbonPropertyValue* topModuleProp = getOptions(cfg->getName())->getValue("Component Name", NULL, NULL);
  UtString topModule;
  topModule << topModuleProp->getValue();

  UtString modelDir;
  modelDir << outDir;

  UtString ccfgPath;
  UtString ccfgName;
  ccfgName << fi.baseName() << ".ccfg";

  OSConstructFilePath(&ccfgName, outDir, ccfgName.c_str());
  QFileInfo ccfgfi(ccfgName.c_str());

  // Read ccfg file to check some properties
  bool compileStandAlone = false;
  CarbonCfgTop ccfg;
  if (readCcfg(&ccfg, ccfgName.c_str())) {
    compileStandAlone = ccfg.isStandAloneComp();
  }
  
  // Get a list of generated source files
  UtString maxsimFiles;
  foreach(QString file, getCppFiles())
    maxsimFiles << MAXSIM_COMPONENT_SUBDIR << "/" << file << " ";

  // first, write out the Unix Target
  UtString MaxsimName;
  UtString unixMakefileName;
  UtString windowsMakefileName;
  if (compileStandAlone) {
    MaxsimName << "lib" << topModule << ".sa";
    unixMakefileName << "Makefile." << fi.baseName() << ".sa";
    windowsMakefileName << "Makefile." << fi.baseName() << ".sa.windows";
  }
  else {
    MaxsimName << "lib" << topModule << ".mx";
    unixMakefileName << "Makefile." << fi.baseName() << ".mx";
    windowsMakefileName << "Makefile." << fi.baseName() << ".mx.windows";
  }

  // Add any optional carmgr switches based on various state
  UtString carmgrSwitches;
  if (theApp->checkArgument("-modelKitSCUseComponentName")) {
    carmgrSwitches << "-scUseComponentName ";
  }

  const char* projProfGen = compilerOptions->getValue("-profileGenerate")->getValue();
  const char* projProfUse = compilerOptions->getValue("-profileUse")->getValue();
  if (theApp->checkArgument("-modelKitProfileGenerate") || (strcmp(projProfGen, "true") == 0)) {
    carmgrSwitches << "-profileGenerate ";
  } else if (theApp->checkArgument("-modelKitProfileUse") || (strcmp(projProfUse, "true") == 0)) {
    carmgrSwitches << "-profileUse ";
  }

  switch (type)
  {
  case CarbonComponent::Build:
    {
      stream << "ifeq ($(CARBON_MODEL_PLATFORM),Unix)" << endl;
      stream << "SoCComponentLibrary=" << MAXSIM_COMPONENT_SUBDIR << "/" << MaxsimName.c_str() << ".so" << endl;
      stream << "SoCComponentMakefileBase=" << unixMakefileName.c_str() << endl;
      stream << "SoCComponentMakefile=" << MAXSIM_COMPONENT_SUBDIR << "/$(SoCComponentMakefileBase)" << endl;
      stream << "else" << endl;
      stream << "SoCComponentLibrary=" << MAXSIM_COMPONENT_SUBDIR << "/" << MaxsimName.c_str() << ".dll" << endl;
      stream << "SoCComponentMakefileBase=" << windowsMakefileName.c_str() << endl;
      stream << "SoCComponentMakefile=" << MAXSIM_COMPONENT_SUBDIR << "/$(SoCComponentMakefileBase)" << endl;
      stream << "endif" << endl;

      stream << endl;
      stream << "SoCComponentSources=" << maxsimFiles.c_str() << " $(SoCComponentMakefile)" << endl;
      stream << "SoCSystemCExportFiles=" << MAXSIM_COMPONENT_SUBDIR << "/mx.export." << topModule.c_str() << ".cpp " << MAXSIM_COMPONENT_SUBDIR << "/mx.export." << topModule.c_str() << ".h" << endl;
      stream << endl;

      stream << "SoCDesignerComponent: $(VHMNAME) $(SoCComponentSources) $(SoCComponentLibrary)";
      if (mActiveConfigSystemCExport) {
        stream << " $(SoCSystemCExportFiles)";
      }
      stream << " SoCDesigner/configuration" << endl;
      stream << "$(SoCComponentSources): $(VHMNAME) " << MAXSIM_COMPONENT_SUBDIR << "/" << ccfgfi.fileName() << " SoCDesigner/configuration" << endl;
      
      if (getProject()->getProjectType() != CarbonProject::ModelKitProject)
        stream << "ifeq ($(CARBON_MODEL_PLATFORM),$(CARBON_OS))" << endl;

      stream << "\t" << "cd " << MAXSIM_COMPONENT_SUBDIR << " && \"$(CARBON_HOME)/bin/$(CARBON_BATCH)\" carmgr " << carmgrSwitches.c_str() << ccfgfi.fileName();
      
      CarbonOptions* options = getOptions(cfg->getName());
      UtString xtorDefsValue = options->getValue("Transactor Definition File")->getValue();

      UtString relXtorDefsValue;
      relXtorDefsValue << CarbonProjectWidget::makeRelativePath(outDir, xtorDefsValue.c_str());

      if (xtorDefsValue.length() > 0)
        stream << " -xtorDef " << getProject()->getUnixEquivalentPath(relXtorDefsValue.c_str());
      
      stream << endl;

     if (getProject()->getProjectType() != CarbonProject::ModelKitProject)
       stream << "endif" << endl;

      stream << "$(SoCComponentLibrary): $(SoCComponentSources) SoCDesigner/configuration" << endl;
      stream << "	$(call CARBON_ECHO,Start SoCDesigner Component Generation)" << endl;
      stream << "	$(call CARBON_ECHO,CarbonWorkingDir: SoCDesigner)" << endl;
      stream << "ifeq ($(CARBON_OS),Unix)" << endl;
      stream << "ifeq ($(CARBON_MODEL_PLATFORM),Unix)" << endl;
      stream << "	cd " << MAXSIM_COMPONENT_SUBDIR << " && $(CARBON_MAKE) VHM_DIR=.. -f " << unixMakefileName.c_str() << endl;
      stream << "endif" << endl;
      stream << "endif" << endl;
      stream << "ifeq ($(CARBON_OS),Windows)" << endl;
      stream << "ifeq ($(CARBON_MODEL_PLATFORM),Windows)" << endl;
      char* nmakeOverRide = getenv("CARBON_NMAKE_OVERRIDE");
      if( nmakeOverRide != NULL )
        stream << "	cd " << MAXSIM_COMPONENT_SUBDIR << " && \"$(CARBON_HOME)/Win/bin/nmake.exe\" MAKEFLAGS= -f " << windowsMakefileName.c_str() << endl;	  
      else  stream << "	cd " << MAXSIM_COMPONENT_SUBDIR << " && \"%" << prefVSVersion << "%VSVARS32.BAT\" && \"nmake.exe\" MAKEFLAGS= -f " << windowsMakefileName.c_str() << endl;	  

      stream << "endif" << endl;
      stream << "endif" << endl;
      stream << "	$(call CARBON_ECHO,Finished SoCDesigner Component Generation)" << endl;

      stream << endl;
      stream << "$(SoCSystemCExportFiles): $(VHMNAME) " << MAXSIM_COMPONENT_SUBDIR << "/" << ccfgfi.fileName() << " SoCDesigner/configuration" << endl;
      stream << "\t" << "cd " << MAXSIM_COMPONENT_SUBDIR << " && \"$(CARBON_HOME)/bin/socdexport$(CARBON_SCRIPT_EXT)\" " << ccfgfi.fileName() << endl;
      stream << "	$(call CARBON_ECHO,Finished SoCDesigner SystemC Export Generation)" << endl;
     }
    break;
  case CarbonComponent::Package: break;
  case CarbonComponent::Clean:
    {
      stream << ".PHONY: " << getTargetName(cfg, CarbonComponent::Clean, CarbonComponent::Unix) << endl;
      stream << getTargetName(cfg, CarbonComponent::Clean, CarbonComponent::Unix) << ":" << endl;
      stream << "\t" << echoBegin() << "CarbonWorkingDir: " << MAXSIM_COMPONENT_NAME << "" << echoEndl();
      stream << "\t" << "-cd " << MAXSIM_COMPONENT_SUBDIR << " && $(CARBON_RM) $(SoCComponentMakefileBase)" << endl;
      stream << "\t" << "-cd " << MAXSIM_COMPONENT_SUBDIR << " && echo \"x\" > configuration" << endl;      
      stream << "\t" << echoBegin() << "Cleaned " << MAXSIM_COMPONENT_NAME << " Component" << echoEndl();
    }
    break;
  }

  return mTargetName.c_str();
}

bool MaxsimComponent::isStandaloneComponent()
{
  ScrCcfg* ccfg = getCcfg();
  if (ccfg)
    return ccfg->isStandAloneComp() > 0 ? true : false;
  else
    return false;
}

void MaxsimSourceItem::doubleClicked(int)
{
  UtString fileName;
  fileName << text(0);

  const char* od = mComp->getOutputDirectory();
  UtString fpath;
  OSConstructFilePath(&fpath, od, fileName.c_str());

  QFileInfo fi(fpath.c_str());
  if (fi.exists())
    mProjectWidget->openSource(fpath.c_str());
  else
  {
    UtString msg;
    msg << "The design must be compiled before you can open the Wizard";
    QMessageBox::critical(mProjectWidget, MODELSTUDIO_TITLE, msg.c_str());
  }
}

void MaxsimTreeItem::singleClicked(int)
{
  MaxsimComponent* comp = static_cast<MaxsimComponent*>(mComponent);
  QStringList items;
  QVariantList itemValues;

  comp->getOptions()->setItems("SOC Designer Component Properties", items, itemValues);

  mProjectWidget->context()->getSettingsEditor()->setSettings(comp->getOptions());
}

void MaxsimCcfgItem::doubleClicked(int)
{
  UtString fileName;
  fileName << text(0);

  const char* od = mComp->getOutputDirectory();
  UtString fpath;
  OSConstructFilePath(&fpath, od, fileName.c_str());

  MDIWidget* widget = mProjectWidget->openSource(fpath.c_str(), -1, MAXSIM_COMPONENT_NAME);
  if (widget == NULL)
  {
    UtString msg;
    msg << "Unable to open the " << MAXSIM_COMPONENT_NAME << " Wizard." << "\n";
    msg << "In order to open the Wizard: " << "\n";
    msg << "1) The design must be compiled for the active configuration." << "\n";
    msg << "2) The component must exist for this configuration" << "\n";
    msg << "Note: You can copy components from other configurations by using the Configuration Manager";
    QMessageBox::warning(mProjectWidget, MODELSTUDIO_TITLE, msg.c_str(), QMessageBox::Ok);  
  }

}


void MaxsimCcfgItem::singleClicked(int)
{
  QStringList items;
  QVariantList itemValues;

  mComp->getOptions()->setItems("SOC Designer Component Properties", items, itemValues);

  mProjectWidget->context()->getSettingsEditor()->setSettings(mComp->getOptions());
}

void MaxsimCcfgItem::showContextMenu(const QPoint& pos)
{
  MaxsimTreeItem* treeItem = dynamic_cast<MaxsimTreeItem*>(parent());
  if (treeItem)
    treeItem->showContextMenu(pos);
}


void MaxsimTreeItem::showContextMenu(const QPoint& pos)
{
  QMenu menu;
  menu.addAction(mActionCompile);
  menu.addAction(mActionReCompile);
  menu.addAction(mActionCheck);
  menu.addAction(mActionClean);
  menu.addAction(mActionDelete);

  menu.setEnabled(!mProjectWidget->isCompilationInProgress());

  menu.exec(pos);
}

MaxsimCcfgItem::MaxsimCcfgItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, MaxsimComponent* comp) : CarbonProjectTreeNode(proj,parent) 
{
  setIcon(0, QIcon(":/cmm/Resources/Wizard.png"));
  mComp = comp;

}

void MaxsimTreeItem::createActions()
{
  mActionCompile = new QAction(mProjectWidget);
  mActionCompile->setText("Compile");
  mActionCompile->setObjectName(QString::fromUtf8("cpwCompileMaxsimComponent"));
  mActionCompile->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/maxsim.png")));
  mActionCompile->setStatusTip("Compile this component");
  CQT_CONNECT(mActionCompile, triggered(), this, compileComponent());

  mActionReCompile = new QAction(mProjectWidget);
  mActionReCompile->setText("Re Compile");
  mActionReCompile->setObjectName(QString::fromUtf8("cpwReCompileMaxsimComponent"));
  mActionReCompile->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/maxsim.png")));
  mActionReCompile->setStatusTip("ReCompile this component");
  CQT_CONNECT(mActionReCompile, triggered(), this, recompileComponent());

  mActionCheck = new QAction(mProjectWidget);
  mActionCheck->setText("Check");
  mActionCheck->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/check.png")));
  mActionCheck->setStatusTip("Check this component for errors");
  CQT_CONNECT(mActionCheck, triggered(), this, icheckComponent());

  mActionClean = new QAction(mProjectWidget);
  mActionClean->setText("Clean");
  mActionClean->setObjectName(QString::fromUtf8("cpwCleanMaxsimComponent"));
  mActionClean->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/clearall.png")));
  mActionClean->setStatusTip("Clean files generated for this component");
  CQT_CONNECT(mActionClean, triggered(), this, cleanComponent());

  mActionDelete = new QAction(mProjectWidget);
  mActionDelete->setText("Delete");
  mActionDelete->setObjectName(QString::fromUtf8("cpwDeleteMaxsimComponent"));
  mActionDelete->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/DeleteHS.png")));
  mActionDelete->setStatusTip("Delete this component");
  CQT_CONNECT(mActionDelete, triggered(), this, deleteComponent());

}

MaxsimTreeItem::MaxsimTreeItem(CarbonProjectWidget* proj, MaxsimComponent* comp) : CarbonComponentTreeItem(proj, 0, comp)
{
  setIcon(0, QIcon(":/cmm/Resources/maxsim.png"));
  createActions();
}

MaxsimTreeItem::MaxsimTreeItem(CarbonProjectWidget* proj, MaxsimComponent* comp, QTreeWidgetItem* parent) : CarbonComponentTreeItem(proj, parent, comp)
{
  setIcon(0, QIcon(":/cmm/Resources/maxsim.png"));
  createActions();
}

// We need to delete the whole item
void MaxsimTreeItem::deleteComponent()
{
  MaxsimComponent* maxsim = static_cast<MaxsimComponent*>(mComponent);
  INFO_ASSERT(maxsim, "Bad component");
  const char* outputDir = maxsim->getOutputDirectory();
  CarbonComponentTreeItem::deleteComponent(outputDir, CarbonComponentTreeItem::Maxsim);
}

void MaxsimTreeItem::deleteComponentBatch()
{
  MaxsimComponent* maxsim = static_cast<MaxsimComponent*>(mComponent);
  INFO_ASSERT(maxsim, "Bad component");
  const char* outputDir = maxsim->getOutputDirectory();
  CarbonComponentTreeItem::deleteComponent(outputDir, CarbonComponentTreeItem::Maxsim, false, DlgConfirmDelete::Delete);
}

void MaxsimTreeItem::cleanComponent()
{
  UtString target; // getTargetName() uses static buffer, so save copy of result

  CarbonComponent::TargetPlatform platType = CarbonComponent::Unix;
  // if we are on Windows and NOT remote, then windows
#if pfWINDOWS 
  MaxsimComponent* maxsimComponent = dynamic_cast<MaxsimComponent*>(mComponent);
  if (mProjectWidget->isRemoteCompilation() && !maxsimComponent->isWindowsTarget(NULL))
    platType = CarbonComponent::Unix;
  else
    platType = CarbonComponent::Windows;
#else
  platType = CarbonComponent::Unix;
#endif

  target << mComponent->getTargetName(mProjectWidget->project()->getActive(), CarbonComponent::Clean, platType);

  mProjectWidget->compile(true, false, false, NULL, target.c_str());
}

void MaxsimTreeItem::recompileComponent()
{

  CarbonComponent::TargetPlatform platType = CarbonComponent::Unix;
  // if we are on Windows and NOT remote, then windows
#if pfWINDOWS 
  MaxsimComponent* maxsimComponent = dynamic_cast<MaxsimComponent*>(mComponent);
  if (mProjectWidget->isRemoteCompilation() && !maxsimComponent->isWindowsTarget(NULL))
    platType = CarbonComponent::Unix;
  else
    platType = CarbonComponent::Windows;
#else
  platType = CarbonComponent::Unix;
#endif

  UtString cleanTarget;
  cleanTarget << "SoCDesignerComponentClean";

  UtString target; // getTargetName() uses static buffer, so save copy of result
  target << "SoCDesignerComponent";

  mProjectWidget->compile(true, true, false, target.c_str(), cleanTarget.c_str());
}

void MaxsimTreeItem::compileComponent()
{
  UtString target; // getTargetName() uses static buffer, so save copy of result

  CarbonComponent::TargetPlatform platType = CarbonComponent::Unix;
  // if we are on Windows and NOT remote, then windows
#if pfWINDOWS 
  MaxsimComponent* maxsimComponent = dynamic_cast<MaxsimComponent*>(mComponent);
  if (mProjectWidget->isRemoteCompilation() && !maxsimComponent->isWindowsTarget(NULL))
    platType = CarbonComponent::Unix;
  else
    platType = CarbonComponent::Windows;
#else
  platType = CarbonComponent::Unix;
#endif
  target << mComponent->getTargetName(mProjectWidget->project()->getActive(), CarbonComponent::Build, platType);
  mProjectWidget->compile(false, true, false, target.c_str());
}

void MaxsimTreeItem::icheckComponent()
{
 ((MaxsimComponent*)mComponent)->icheckComponent();
}

void MaxsimComponent::registerTypes(QScriptEngine* engine)
{
  QScriptValue global = engine->globalObject();

   // Construct Application Object
  ScrApplication* scriptApp = new ScrApplication(engine);
  global.setProperty("Application", engine->newObject(scriptApp));

  if (getProject())
  {
    // Construct Project Object (if there is one)
    ScrProject* scriptProject = new ScrProject();
    global.setProperty("Project", engine->newQObject(scriptProject));
  }
}



