//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "ScrPlugin.h"

Q_SCRIPT_DECLARE_QMETAOBJECT(ScrPlugin, QObject*)

void ScrPlugin::registerTypes(QScriptEngine* engine)
{
  if (engine)
  {
    QScriptValue global = engine->globalObject();
    QScriptValue pluginStatic = engine->newQMetaObject(&ScrPlugin::staticMetaObject);
    global.setProperty("Plugin", pluginStatic);
  }
}


