//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "DlgSystemAddressMapping.h"
#include "cfg/CarbonCfg.h"
#include "MemEditorTreeNodes.h"

DlgSystemAddressMapping::DlgSystemAddressMapping(QWidget *parent, CarbonCfg* ccfg, CarbonCfgMemory* mem)
: QDialog(parent)
{
  ui.setupUi(this);

  mCcfg = ccfg;
  mMemory = mem;
  mIsModified = false;

  ui.treeWidget->setItemDelegate(new AddressMappingDelegate(this, ui.treeWidget, mCcfg));

  populate();
}

bool DlgSystemAddressMapping::isModified()
{
  bool _isModified = false;

  for(int i=0; i<ui.treeWidget->topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = ui.treeWidget->topLevelItem(i);
    QVariant v = item->data(0, Qt::UserRole);
    _AddressMapping* am =  (_AddressMapping*)v.value<void*>(); 
    
    bool isChecked = item->checkState(colPORT_NAME) == Qt::Checked;

    if (am->mModified || (am->mPortMapped != isChecked))
    {
      _isModified = true;
      break;
    }
  }

  return _isModified;
}

const _AddressMapping& DlgSystemAddressMapping::getMapping(int index)
{
  QTreeWidgetItem* item = ui.treeWidget->topLevelItem(index);
  QVariant v = item->data(0, Qt::UserRole);
  _AddressMapping* am =  (_AddressMapping*)v.value<void*>(); 
    
  bool isChecked = item->checkState(colPORT_NAME) == Qt::Checked;
  if (am->mPortMapped != isChecked)
    am->mModified = true;

  am->mPortMapped = isChecked;

  return *am;
}


void DlgSystemAddressMapping::populate()
{
  QMap<QString, quint32> portMap;

  for(quint32 i=0; i<mMemory->NumSystemAddressESLPorts(); i++)
  {
    QString name = mMemory->getSystemAddressESLPortName(i);
    portMap[name] = i;
  }
 
  for(quint32 i=0; i<mCcfg->numXtorInstances(); i++)
  {
    CarbonCfgXtorInstance* xinst = mCcfg->getXtorInstance(i);
    if (xinst->getType()->hasWriteDebug())
    {
      QString name = xinst->getName();
      QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidget);

      _AddressMapping* am = new _AddressMapping(name);

      item->setFlags(item->flags() | Qt::ItemIsUserCheckable | Qt::ItemIsEditable);
      item->setText(colPORT_NAME, name);
      item->setText(colBASE_ADDRESS, "0x0");
      item->setCheckState(colPORT_NAME, Qt::Unchecked);

      if (portMap.contains(name))
      {
        quint32 index = portMap[name];
        qint64 value = mMemory->getSystemAddressESLPortBaseAddress(index);
        am->mPortMapped = true;
        am->mBaseAddress = value;
        QString hexValue = QString("0x%1").arg(value,0,16);
        item->setText(colBASE_ADDRESS, hexValue);
        item->setCheckState(colPORT_NAME, Qt::Checked);
      }

      item->setData(0, Qt::UserRole, qVariantFromValue((void*)am));
    }
  }

  ui.treeWidget->header()->resizeSections(QHeaderView::ResizeToContents);
}

DlgSystemAddressMapping::~DlgSystemAddressMapping()
{
}


void DlgSystemAddressMapping::on_buttonBox_rejected()
{
  reject();
}

void DlgSystemAddressMapping::on_buttonBox_accepted()
{
  accept();
}

AddressMappingDelegate::AddressMappingDelegate(QObject*, QTreeWidget* tree, CarbonCfg* cfg)
{
  mTree = tree;
  mCfg = cfg;
}


QWidget *AddressMappingDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                              const QModelIndex &index) const
{
  if (index.column() == DlgSystemAddressMapping::colBASE_ADDRESS)
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    SignedInt64Validator* validator = new SignedInt64Validator(lineEdit);
    lineEdit->setValidator(validator);
    return lineEdit;
  }

  return NULL;
}

void AddressMappingDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit)
  {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }
}

void AddressMappingDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& modelIndex) const
{
  if (modelIndex.column() == DlgSystemAddressMapping::colBASE_ADDRESS)
  {
    QTreeWidgetItem* item = mTree->currentItem();
    QVariant v = item->data(0, Qt::UserRole);
    _AddressMapping* am =  (_AddressMapping*)v.value<void*>(); 

    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldValue = model->data(modelIndex).toString();
      QString newValue = lineEdit->text();
      if (oldValue != newValue)
      {
        qint64 baseAddress = ConvertStringToSInt64(newValue);
        QString hexValue = QString("0x%1").arg(QString().setNum(baseAddress, 16));

        item->setCheckState(DlgSystemAddressMapping::colPORT_NAME, Qt::Checked);

        model->setData(modelIndex, hexValue);

        am->setMapped(true);
        am->setModified(true);
        am->setBaseAddress(baseAddress);
      }
    }
  }
}

void AddressMappingDelegate::commitAndCloseEditor()
{
  QLineEdit *editor = qobject_cast<QLineEdit *>(sender());
  if (editor)
  {
    emit commitData(editor);
    emit closeEditor(editor);
  }
}

