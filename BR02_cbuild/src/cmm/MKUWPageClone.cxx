//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "ModelKitUserWizard.h"
#include "CarbonFiles.h"
#include "ModelKitWizard.h"

MKUWPageClone::MKUWPageClone(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);

  setTitle("Preparing for Compilation");
  setSubTitle("The project is being prepared for compilation.");

  mIsValid = true;
}

int MKUWPageClone::nextId() const
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  KitManifest* manifest = wiz->getManifest();

  if (manifest->getPreUserActions().count() == 0)
    return ModelKitUserWizard::PageFinished;
  else
    return ModelKitUserWizard::PageUserActions;
}
void MKUWPageClone::initializePage()
{
  // We could have started on this page, so hide it now.
  QMainWindow* mw = theApp->getContext()->getMainWindow();
  if (!theApp->checkArgument("-debugWindows"))
    mw->hide();
 
  ui.textBrowser->clear();
  QApplication::setOverrideCursor(Qt::WaitCursor);
  mIsValid = cloneFiles();
    
  QApplication::restoreOverrideCursor();

  emit completeChanged();

  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  if (wiz->getAnswers())
    wiz->next();
}

bool MKUWPageClone::isComplete() const
{
  return mIsValid;
}

bool MKUWPageClone::copyKitFile(KitFile* kitFile)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  QString armKitRoot = wizard()->field("armKitRoot").toString();
  QString kitPath = proj->getWindowsEquivalentPath(armKitRoot);

  UtString armProjDir;
  OSConstructFilePath(&armProjDir, proj->getProjectDirectory(), "ARM_Logical");

  QString armLogicalDir = armProjDir.c_str();
  QDir pdir(armLogicalDir);

  UtString normalizedName;
  normalizedName << kitFile->getNormalizedName();

  UtString expandedName;
  UtString err;

  if (OSExpandFilename(&expandedName, normalizedName.c_str(), &err))
  {
    QString localName =  proj->getWindowsEquivalentPath(expandedName.c_str());
    QString relLocalName = localName.mid(kitPath.length());

    QString newName = QString("%1/%2").arg(armLogicalDir).arg(relLocalName);
    
    qDebug() << "cloning" << localName << "to" << newName;

    QFileInfo destName(newName);
    QDir dir = destName.dir();
    if (!dir.mkpath(destName.absolutePath()))
    {
      ui.textBrowser->append(QString("Error: Unable to create directory: %1").arg(destName.absolutePath()));
      return false;
    }

    if (destName.exists())
    {
      if (!QFile::remove(newName))
      {
        ui.textBrowser->append(QString("Error: Unable to remove file: %1").arg(newName));
        return false;
      }
    }
    if (QFile::copy(localName, newName))
    {
      ui.textBrowser->append(QString("Copied %1 to %2").arg(localName).arg(newName));
    }
    else
    {
      if (!kitFile->getOptional())
      {
        ui.textBrowser->append(QString("Error: Unable to copy %1 to %2").arg(localName).arg(newName));
        return false;
      }
      else
        qDebug() << "optional file" << localName << "not found";
    }   
  }
  else
  {
    qDebug() << "error expand failed for" << normalizedName.c_str();
    return false;
  }

  return true;
}

bool MKUWPageClone::cloneFiles()
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  QString armKitRoot = wizard()->field("armKitRoot").toString();
  QString kitPath = proj->getWindowsEquivalentPath(armKitRoot);
  
  UtString envArm;
  envArm << "ARM_ROOT=" << wizard()->field("armKitRoot").toString();
  putenv((char*)envArm.c_str());

  proj->setARMKitRoot(wizard()->field("armKitRoot").toString());

  UtString armProjDir;
  OSConstructFilePath(&armProjDir, proj->getProjectDirectory(), "ARM_Logical");

  TempChangeDirectory cd(armProjDir.c_str());

  QString armLogicalDir = armProjDir.c_str();
  QDir pdir(armLogicalDir);

  KitManifest* manifest = wiz->getManifest();

  qDebug() << "creating" << armLogicalDir << "copyFiles" << manifest->getCopyFiles() << manifest->getAskRoot();

  if (! pdir.mkpath(armLogicalDir))
  {
    qDebug() << "error" << "failed to create" << armLogicalDir;
    return false;
  }

  if ( manifest->getCopyFiles())
  {
    // Copy the RTL files
    foreach (KitFile* kitFile, manifest->getRTLFiles())
    {
      RTLFileOptions* rfu = manifest->getFileOptionsMap()[kitFile->getNormalizedName()];
      if (rfu->mCopied)
      {
        kitFile->setOptional(rfu->mOptional);
        kitFile->setCheckHash(rfu->mHashed);
        copyKitFile(kitFile);
      }
    }
    // Copy the Optional Files
    foreach (KitFile* kitFile, manifest->getOptionalRTLFiles())
    {
      kitFile->setOptional(true);
      copyKitFile(kitFile);
    }
  } else
  {
    ui.textBrowser->append(QString("No files needed to by copied."));
  }
  return true;
}

bool MKUWPageClone::validatePage()
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  UtString armProjDir;
  OSConstructFilePath(&armProjDir, proj->getProjectDirectory(), "ARM_Logical");

  proj->setARMKitRoot(armProjDir.c_str());

  emit completeChanged();
  return mIsValid;
}

MKUWPageClone::~MKUWPageClone()
{
}
