#ifndef __Mode_H_
#define __Mode_H_

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "Scripting.h"
#include "Ports.h"
#include "Blocks.h"
#include "Parameters.h"

class PortConfiguration;
class Modes;
class Template;

class Blocks;
class Parameters;
class Ports;

// File: Mode
// Class: Mode
class Mode : public QObject
{
  Q_OBJECT

  // Property: name
  //
  // Returns: String name of this block
  //
  Q_PROPERTY(QString name READ getName)
  
  // Property: blocks
  // Collection of blocks associated with this mode.
  //
  // Returns:
  // <Blocks> collection of <Block> objects
  //
  Q_PROPERTY(QObject* blocks READ getBlocks)
  
  // Property: ports
  // Collection of ports associated with this mode.
  //
  // Returns:
  // <Ports> collection of <Port> objects
  //
  Q_PROPERTY(QObject* ports READ getPorts)
  
  // Property: parameters
  // Collection of parameters associated with this mode.
  //
  // Returns:
  // <Parameters> collection of <Parameter> objects
  //
  Q_PROPERTY(QObject* parameters READ getParameters)

public:
  void setName(const QString& newVal) { mName=newVal; }
  QString getName() const { return mName; }
  void preElaborate();
  static bool deserialize(Template* templ, Modes* modes, const QDomElement& e, XmlErrorHandler* eh);

public:
  Blocks* getBlocks();
  Ports* getPorts();
  Parameters* getParameters();

private:
  QString mName;
  Blocks mBlocks;
  Ports mPorts;
  Parameters mParameters;
};

#endif
