//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "MemoryWizard.h"

#include "WizPgMultiFinished.h"
#include "cmm.h"
#include "WorkspaceModelMaker.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"
#include "DesignXML.h"

  //CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  //CarbonProject* proj = pw->project();

WizPgMultiFinished::WizPgMultiFinished(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);
  setTitle("Finished");
  setSubTitle("Summary results");
  mIsValid = true;
  mConnectedOutput = false;
}

void WizPgMultiFinished::append(const QString& str)
{
  ui.textBrowser->append(str);
}


WizPgMultiFinished::~WizPgMultiFinished()
{
}

bool WizPgMultiFinished::isComplete() const
{
  return mIsValid;
}

bool WizPgMultiFinished::validatePage()
{
  emit completeChanged();

  return mIsValid;
}

void WizPgMultiFinished::initializePage()
{
  MemoryWizard* wiz = static_cast<MemoryWizard*>(wizard());

  qDebug() << "generate" << wiz->getOption("generate").toString();
  qDebug() << "directory" << wiz->getOption("directory").toString();
  qDebug() << "modulefile" << wiz->getOption("modulefile").toString();
  qDebug() << "prefix" << wiz->getOption("prefix").toString();
  qDebug() << "suffix" << wiz->getOption("suffix").toString();
  qDebug() << "-y" << wiz->getOption("-y").toString();
  
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  CarbonOptions* options = proj->getToolOptions("VSPCompiler");
 
  if (!mConnectedOutput)
  {
    CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();

    CQT_CONNECT(pw->getConsole(),
      stdoutChanged(CarbonConsole::CommandType, const QString&, const QString&), 
      this,
      outputChanged(CarbonConsole::CommandType, const QString&, const QString&));

    CQT_CONNECT(pw->getConsole(),
      stderrChanged(CarbonConsole::CommandType, const QString&, const QString&), 
      this,
      outputChanged(CarbonConsole::CommandType, const QString&, const QString&));
    
    mConnectedOutput = true;
  }

  QString gen = wiz->getOption("generate").toString();
  QString templateName = wiz->getOption("template").toString();

  ui.textBrowser->clear();
  ui.textBrowserGenerate->clear();

  append(QString("Using Template: %1").arg(templateName));

  bool singleFile = false;
  if ("singleFile" == gen)
  {
    singleFile = true;
    append(QString("Generating Source File: %1").arg(wiz->getOption("modulefile").toString()));
  }
  else
  {
    singleFile = false;
    append(QString("Generating Modules in Directory: %1").arg(wiz->getOption("directory").toString()));
  }

  QString dashY = wiz->getOption("-y").toString();
  if (!dashY.isEmpty())
  {
    options->putValue("-y", dashY);
    append(QString("Adding -y %1").arg(dashY));
  }

  QString prefix = wiz->getOption("prefix").toString();
  QString suffix = wiz->getOption("suffix").toString();

  if (!prefix.isEmpty())
    append(QString("Using Module Prefix: %1").arg(prefix));

  if (!suffix.isEmpty())
    append(QString("Using Module Suffix: %1").arg(suffix));
//
//<MemoryGenerator version="1">
//  <Options>
//    <Option name="templateName" value="template"/>
//    <Option name="moduleNamePrefix" value="carbon_"/>
//    <Option name="singleFile" value="foo.v"/>
//    <!-- dir will be created if not existing
//    // if outputdir is a relative path, it is relative to the file.xml
//    //-->
//    <Output name="outputDirectory" value="/usr/files"/>
//  </Options>
//  
//  <Ports>
//    <Port name="portNameA" function="FuncNameA"/>
//    <Port name="portNameB" function="FuncNameB"/>
//  </Ports>
//</MemoryGenerator>
  QString memGenXml = generateMemoryGenFile();

  CarbonConsole::CommandMode mode = CarbonConsole::Local;
#if pfWINDOWS
  QString program = "${CARBON_HOME}/bin/carbon.bat";
  QString argGenerate = proj->getWindowsEquivalentPath(memGenXml);
  QString argInputModules = proj->getWindowsEquivalentPath(wiz->getOption("moduleListFile").toString());
  QString argDesignHierarchy = proj->getWindowsEquivalentPath(wiz->getOption("designHierarchyFile").toString());
#else
  QString program = "${CARBON_HOME}/bin/carbon";
  QString argGenerate = proj->getUnixEquivalentPath(memGenXml);
  QString argInputModules = proj->getUnixEquivalentPath(wiz->getOption("moduleListFile").toString());
  QString argDesignHierarchy = proj->getUnixEquivalentPath(wiz->getOption("designHierarchyFile").toString());
#endif

  pw->makeConsoleVisible();

  QStringList args;
  args << "remodeler";
  args << "--generate" << argGenerate;
  args << "--inputModules" << argInputModules;
  args << "--designHierarchy" << argDesignHierarchy;

  UtString subDir;
  subDir << proj->getActivePlatform() << "/" << proj->getActiveConfiguration();
  
  qDebug() << program << args;

  QApplication::setOverrideCursor(Qt::WaitCursor);
  int exitCode = pw->getConsole()->executeCommandAndWait(mode, CarbonConsole::Command, program, args, subDir.c_str());
  QApplication::restoreOverrideCursor();

  qDebug() <<  "carbon memgen --generate finished, exitCode" << exitCode;
  if (exitCode == 0)
  {
    mIsValid = true;
  }
  else
  {
    QMessageBox::critical(NULL, MODELSTUDIO_TITLE, "The memory generator completed with errors");
    mIsValid = false;
  }

  emit completeChanged();
}
void WizPgMultiFinished::outputChanged(CarbonConsole::CommandType, const char* text, const char*)
{
  ui.textBrowserGenerate->append(text);
}
QString WizPgMultiFinished::generateMemoryGenFile()
{
  QString outputName;

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  MemoryWizard* wiz = static_cast<MemoryWizard*>(wizard());

  const char* desHier = proj->getDesignFilePath(".designHierarchy");
  QFileInfo fi(desHier);
  UtString filename;
  filename << fi.baseName() << ".memGen";

  UtString outname;
  OSConstructFilePath(&outname, proj->getActive()->getOutputDirectory(), filename.c_str());
  outputName = outname.c_str();

  QDomDocument doc;
  QDomElement rootTag = doc.createElement("MemoryGenerator");
  rootTag.setAttribute("version", "1");
  doc.appendChild(rootTag);
  XmlErrorHandler eh;

  QDomElement optionsTag = doc.createElement("Options");
  rootTag.appendChild(optionsTag);

  if (wiz->getOption("generate") == "singleFile")
    addOption(doc, optionsTag, "singleFile",  wiz->getOption("modulefile").toString());
  else
    addOption(doc, optionsTag, "outputDirectory",  wiz->getOption("directory").toString());

  addOption(doc, optionsTag, "templateName", wiz->getOption("template").toString());
 
  QString prefix = wiz->getOption("prefix").toString();
  if (!prefix.isEmpty())
    addOption(doc, optionsTag, "moduleNamePrefix", prefix);

  QString suffix = wiz->getOption("suffix").toString();
  if (!suffix.isEmpty())
    addOption(doc, optionsTag, "moduleNameSuffix", suffix);

  QDomElement portsTag = doc.createElement("Ports");
  rootTag.appendChild(portsTag);

  for (int i=0; i<wiz->numPortMaps(); i++)
  {
    MWPortMap* pm = wiz->getPortMap(i);
    QDomElement portTag = doc.createElement("Port");
    portsTag.appendChild(portTag);
    if (pm->getPortNumber() == -1)
      portTag.setAttribute("portNumber", "");
    else
      portTag.setAttribute("portNumber", pm->getPortNumber());
    portTag.setAttribute("name", pm->getPortName());
    portTag.setAttribute("function", pm->getPortFunction());
    portTag.setAttribute("functionClass", pm->getFunctionClass());
  }

  QDomElement modParamsTag = doc.createElement("ModuleParameters");
  rootTag.appendChild(modParamsTag);
  for (int i=0; i<wiz->numParamMaps(); i++)
  {
    MWParamMap* pm = wiz->getParamMap(i);
    QDomElement paramTag = doc.createElement("Parameter");
    modParamsTag.appendChild(paramTag);
    if (pm->getParamFunction().isEmpty())
      paramTag.setAttribute("parameterFunction", "");
    else
      paramTag.setAttribute("parameterFunction", pm->getParamFunction());

    paramTag.setAttribute("name", pm->getParamName());
  }


  QFile file(outputName);
  if (file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    QTextStream out(&file);
    out << doc.toString(2);
    file.close();
  }

  return outputName;
}

void WizPgMultiFinished::addOption(QDomDocument& doc, QDomElement& parent, const QString& name, const QString& value)
{
  QDomElement optionTag = doc.createElement("Option");
  parent.appendChild(optionTag);
  optionTag.setAttribute("value", value);
  optionTag.setAttribute("name", name);
}
