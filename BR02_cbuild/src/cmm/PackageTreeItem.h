// -*-C++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include "util/UtDLList.h"
#include "util/XmlParsing.h"

// Class for holding individual package items
class PackageTreeItem {
public:
  typedef UtDLList<PackageTreeItem*> PkgItemList;

  PackageTreeItem(const char* name, const char* filepath, const char* compName, PackageTreeItem* parent);

  virtual ~PackageTreeItem()
  { 
  }

  // Query item type
  virtual bool isDir()const=0;
  virtual bool isFile()const=0;
  virtual bool isRoot()const=0;
  virtual bool isUser() const;

  // Item Properties
  const char* name()         const { return mName.c_str();}
  UtString packagePath()     const;
  const char* filePath()     const { return mFilePath.c_str(); };
  const UtString& compName() const { return mCompName; }
  PackageTreeItem* parent()        { return mParent;}

  void setName(const char* name) { mName = name; }
  
  // File IO methods
  bool serialize(xmlTextWriterPtr writer);
  static PackageTreeItem* deserialize(xmlNodePtr parent, UtXmlErrorHandler* eh, PackageTreeItem* treeParent);

  // Iteration Methods
  PackageTreeItem* firstChild();
  PackageTreeItem* nextChild();

  // Content control methods
  bool addChild(PackageTreeItem* child);
  bool removeChild(PackageTreeItem* child);
  bool removeAllChildren();
  bool removeNamedChildren(const UtString& name);

private:
  UtString mName;
  UtString mFilePath;
  UtString mCompName; // Component the node belongs to
  PackageTreeItem* mParent;
  UtDLList<PackageTreeItem*>  mChildList;
  UtDLList<PackageTreeItem*>::iterator mChildIter;
};

// Package Root Node
class PackageTreeRoot : public PackageTreeItem {
public:
  PackageTreeRoot(const char* name) : PackageTreeItem(name, "", "Root", NULL) {
  }
  bool isDir() const { return false;}
  bool isFile() const { return false;}
  bool isRoot() const { return true;}
};

// Package Directory Node
class PackageTreeDir : public PackageTreeItem {
public:
  PackageTreeDir(const char* name, const char* compName, PackageTreeItem* parent) :
    PackageTreeItem(name, "", compName, parent)
  {
  }
  bool isDir() const {return true;}
  bool isFile() const {return false;}
  bool isRoot() const {return false;}
};

// Package File Node
class PackageTreeFile : public PackageTreeItem {
public:
  PackageTreeFile(const char* name, const char* path, const char* compName, PackageTreeItem* parent) :
    PackageTreeItem(name, path, compName, parent)
  {
  }
  bool isDir() const {return false;}
  bool isFile() const {return true;}
  bool isRoot() const {return false;}
};

//! Utility function to create a list of all directory items in the tree
/*!
  \param root Start item for traversing the list
  \return List of directory items in the tree
  */
PackageTreeItem::PkgItemList createPackageDirList(PackageTreeItem* root);

//! Utility function to create a list of all file items in the tree
/*!
  \param root Start item for traversing the list
  \return List of file items in the tree
  */
PackageTreeItem::PkgItemList createPackageFileList(PackageTreeItem* root);

