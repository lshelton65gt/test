//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtShellTok.h"
#include "shell/ReplaySystem.h"
#include "util/CarbonVersion.h"
#include "util/RandomValGen.h"
#include "util/UtEncryptDecrypt.h"
#include "cfg/carbon_cfg.h"
#include "util/OSWrapper.h"

#include <QFile>
#include <QDebug>
#include <QCryptographicHash>

#include "gui/CQt.h"

#include "CarbonProjectWidget.h"
#include "CarbonProject.h"
#include "CarbonCompilerTool.h"
#include "CarbonSourceGroups.h"
#include "CarbonHDLSourceFile.h"
#include "CarbonProperty.h"
#include "CarbonProperties.h"
#include "CarbonOptions.h"
#include "CarbonSourceGroups.h"
#include "CarbonHDLSourceFile.h"
#include "RemoteConsole.h"
#include "CarbonConsole.h"
#include "CarbonDatabaseContext.h"
#include "CarbonComponent.h"
#include "CarbonDirectives.h"
#include "CarbonMakerContext.h"
#include "WorkspaceModelMaker.h"
#include "CarbonPropertyFilelist.h"
#include "Mdi.h"
#include "CarbonFiles.h"
#include "CarbonProjectNodes.h"
#include "ErrorInfoWidget.h"

#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "shell/carbon_misc.h"
#include "shell/carbon_capi.h"

#include "ModelStudioCommon.h"
#include "SSHConsoleHelper.h"
#include "DlgAuthenticate.h"
#include "ModelKit.h"
#include "ModelKitUserWizard.h"

#include "ScriptingEngine.h"

#include "SpiritXML.h"

#include "MaxsimWizardWidget.h"
#include "CowareWizardWidget.h"
#include "SystemCWizardWidget.h"

#define XML_ENCODING "ISO-8859-1"
#define XML_SCHEMA_VERSION "1.0"

#include <time.h>

static const char* ignoreSwitches[] =
{
  "-phaseStats"
};

static const char* ignoreOutputSwitches[] =
{
  "Compiler Control",
  "-vhdlLib",
  "-vlogLib"
};

// these are switches that may contain relative paths
// that require fixing when importing a project
static const char* adjustPathSwitches[] = 
{
  "+incdir+",
  "-y",
  "-v",
  "-directive"
};

// these must match VSPCompilerProperties.xml file
#define MODEL_32x32 "32-bit compile, 32-bit model"
#define MODEL_64x32 "64-bit compile, 32-bit model"
#define MODEL_64x64 "64-bit compile, 64-bit model"

extern UInt32 replaceSubstring(UtString* str, const char* oldStr, const char* newStr);

class ProjectChangeOutputDirectory
{
public:
  ProjectChangeOutputDirectory(const char* newDir)
  {
    OSGetCurrentDir(&mCurrDir);
    OSChdir(newDir, &mErr);
  }
  ~ProjectChangeOutputDirectory()
  {
    OSChdir(mCurrDir.c_str(), &mErr);
  }

private:
  UtString mErr;
  UtString mCurrDir;
};


void CarbonProject::setSpiritXML(SpiritXML* newVal)
{
  if (newVal && newVal != mSpiritXML)
    delete mSpiritXML;
  mSpiritXML=newVal;
}

QString CarbonProject::hashFile(const QString& fp)
{
  QString hashResult;
  QString filePath = getWindowsEquivalentPath(fp);
  QFile file(filePath);
  if (file.open(QIODevice::ReadOnly))
  {
    QTextStream ts(&file);
    QString fileContents = ts.readAll();
    QCryptographicHash hash(QCryptographicHash::Md5);
    hash.addData(fileContents.toUtf8());
    hashResult = hash.result().toHex();
    qDebug() << "hashed" << filePath << "value" << hashResult;
    file.close();
  }

  return hashResult;
}

CarbonOptions* CarbonProject::getToolOptions(const char* cfgName, const char* toolName)
{
  CarbonConfiguration* cfg = getConfigurations()->findConfiguration(cfgName);
  INFO_ASSERT(cfg, "Expecting configuration");

  return cfg->getOptions(toolName);
}

void CarbonProject::propertiesModified()
{
  mProjectWidget->putModified(true);
}

void CarbonProjectOptions::propertiesModified()
{
  mProjectWidget->putModified(true);
}

CarbonProject::CarbonProject(QObject* parent, CarbonProjectWidget* pw) : QObject(parent)
{
  mWriteErrorsOccurred = false;
  mSpiritXML = NULL;
  mProjectWidget = pw;
  mProjectWatcher = NULL;
  mFirstConfiguration = true;
  mActiveConfiguration = NULL;

  mModelCompilationStarted = false;
  mCarbonModelWasCompiled = false;

  QString guid = createGUID();

  mProjectType = HDL;
  mFileProperties.readPropertyDefinitions(":/cmm/Resources/VSPCompilerFileProperties.xml");
  mFileOptions = new CarbonOptions(NULL, &mFileProperties, "VSPCompilerFileDefaults");

  mFileOptions->setBlockSignals(true);
  // set any gui defaults here.
  for (UInt32 i=0; i<mFileProperties.numGroups(); i++)
  {
    CarbonPropertyGroup* group = mFileProperties.getGroup(i);

    for (CarbonPropertyGroup::SwitchLoop p = group->loopSwitches(); !p.atEnd(); ++p)
    {
      CarbonProperty* prop = p.getValue();
      if (prop->hasGuiDefaultValue())
      {
        const char *propName = prop->getName();
        mFileOptions->getValue(propName)->putValue(prop->getGuiDefaultValue());
      }
    }
  }
  mFileOptions->setBlockSignals(false);


  CQT_CONNECT(mFileOptions, propertiesModified(), this, propertiesModified());

  mConfigs = new CarbonConfigurations(this);

  mFiles = new CarbonFiles(this);

  if (mProjectType == HDL)
    mSourceGroups = new CarbonSourceGroups(this);

  mComponents = new CarbonComponents(this);
  mDbContext = new CarbonDatabaseContext(pw->context()->getQtContext());

  if (mProjectType == HDL)
    mDirectives = new CarbonDirectives(this);


  CarbonProjectOptions* projOptions = new CarbonProjectOptions(mProjectWidget);
  mOptions = new CarbonOptions(projOptions->getOptions(), projOptions->getProperties(), "ProjectOptions");
  CQT_CONNECT(mOptions, propertiesModified(), this, propertiesModified());

  mProjectFile.clear();
  mProjectDir.clear();

  // The currently supported platforms
  mPlatforms.push_back("Linux");
  mPlatforms.push_back("Solaris");

  mConnectedConsole = false;
}


void CarbonProject::connectConsole()
{
  if (mProjectWidget && !mConnectedConsole)
  {
    CQT_CONNECT(mProjectWidget->getConsole(), compilationEnded(CarbonConsole::CommandMode,CarbonConsole::CommandType), this, compilationEnded(CarbonConsole::CommandMode,CarbonConsole::CommandType));
    CQT_CONNECT(mProjectWidget->getConsole(), compilationStarted(CarbonConsole::CommandMode,CarbonConsole::CommandType), this, compilationStarted(CarbonConsole::CommandMode,CarbonConsole::CommandType));
    CQT_CONNECT(mProjectWidget->getConsole(), modelCompilationFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, modelCompilationFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType));
    CQT_CONNECT(mProjectWidget->getConsole(), makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType));
    CQT_CONNECT(mProjectWidget->getConsole(), exploreFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, exploreFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType));
    CQT_CONNECT(mProjectWidget->getConsole(), processTerminated(CarbonConsole::CommandMode, CarbonConsole::CommandType, int), this, processTerminated(CarbonConsole::CommandMode, CarbonConsole::CommandType, int));
    CQT_CONNECT(mProjectWidget->getConsole(), commandCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType, int), this, commandCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType, int));
    mConnectedConsole=true;
  }
}

void CarbonProject::processTerminated(CarbonConsole::CommandMode mode, CarbonConsole::CommandType ctype, int exitStatus)
{
#if pfWINDOWS
  qDebug() << "windows" << "carbonProject" << mode << ctype << exitStatus;
#else
  qDebug() << "unix" << "carbonProject" << mode << ctype << exitStatus;
#endif
}

void CarbonProject::actionImportCcfgFile()
{
}

void CarbonProject::actionImportCommandFile()
{
  QString fileName = QFileDialog::getOpenFileName(mProjectWidget,
    "Select Carbon Command File",getProjectDirectory(),"Carbon Command File (*.cmd *.f)");
  if (fileName.length() > 0)
  {
    UtString cmdFile;
    cmdFile << fileName;
    processCommandFile(cmdFile.c_str(), true);
  }
}


const char* CarbonProject::createNewProject(const char* projectLocation, const char* projectName)
{
  UtString homePath(projectLocation);
  UtString projName(projectName);

  if (theApp->variablesScope())
    theApp->removeAllEnvironmentVariables();

  projName << ".carbon";
  mProjectFile.clear();

  mProjectDir.clear();
  mProjectDir << projectLocation;

  mConfigs = new CarbonConfigurations(this);

  mActiveConfiguration = mConfigs->addConfig("Linux", "Default");

  if (mProjectType == HDL)
  {
    CarbonSourceGroup* group = new CarbonSourceGroup(mSourceGroups);
    group->putName("RTL Model");
    group->putVhdlLibrary("");
    group->putVerilogLibrary("");
    mSourceGroups->addGroup(group);
  }

  OSConstructFilePath(&mProjectFile,  homePath.c_str(), projName.c_str());

  save();

  return mProjectFile.c_str();
}


const char* CarbonProject::createNewProjectFromExisting(QWidget* parent, const char* projectLocation, const char* projectName)
{
  const char* newProjectFile = createNewProject(projectLocation, projectName);

  QString fileName = QFileDialog::getOpenFileName(parent,
    "Select Carbon Command File",projectLocation,"Carbon Command File (*.cmd *.f)");

  if (fileName.length() > 0)
  {
    UtString cmdFile;
    cmdFile << fileName;

    processCommandFile(cmdFile.c_str(), false);
    save();
  }

  return newProjectFile;
}


const char* CarbonProject::createNewProjectFromDatabase(QWidget*, const char* projectLocation, const char* projectName, const char* dbFile)
{
  const char* newProjectFile = createNewProject(projectLocation, projectName);

  mProjectType = DatabaseOnly;

  QFileInfo databaseFile(dbFile);

  UtString name;
  name << databaseFile.fileName();

  CarbonOptions* options = getActive()->getOptions("VSPCompiler");
  options->putValue("-o", name.c_str());

  UtString srcDir;
  srcDir << databaseFile.absolutePath();

  QFileInfo dbfi(dbFile);
  UtString ext;
  ext << dbfi.completeSuffix();

  const char *iodbName = getDesignFilePath(".symtab.db");
  QFileInfo destInfo(iodbName);

  UtString destDir;
  destDir << destInfo.absolutePath();

  UtString baseName;
  baseName << databaseFile.baseName();

  QDir dir;
  dir.mkpath(destDir.c_str());

  // When importing a design, the base name must match 
  UtString newBaseName;
  newBaseName << baseName;

  qDebug() << "srcDir" << srcDir.c_str() << "destDir" << destDir.c_str();

  bool sameDir = srcDir == destDir;

  if (sameDir || copyDesignFiles(srcDir.c_str(), baseName.c_str(), destDir.c_str()))
  {
    UtString alibName;
    alibName << getDesignFilePath(".a");

    UtString libname;
    libname << getDesignFilePath(".lib");

    QFileInfo aFi(alibName.c_str());
    QFileInfo libFi(libname.c_str());

    if (aFi.exists())
    {
      UtString libfile;
      libfile << aFi.fileName();
      options->putValue("-o", libfile.c_str());
    }
    else if (libFi.exists())
    {
      UtString libfile;
      libfile << libFi.fileName();
      options->putValue("-o", libfile.c_str());
    }

    save();
    return newProjectFile;
  }
  else
    return "";
}

// Copy all files from designDir matching designBaseName.*
// to destinationDir and change the output names to destBaseName.*

bool CarbonProject::copyDesignFiles(const char* designDir, const char* designBaseName, const char* destinationDir)
{
  QStatusBar* statusBar = getProjectWidget()->context()->getMainWindow()->statusBar();
  bool status = true;
  bool designLib = false;
  QDir srcDir(designDir);
  UtString designWildcard;
  designWildcard << designBaseName << ".*";
  QStringList nameFilters;
  nameFilters << designWildcard.c_str();

  // Each source file matching the wildcard
  QFileInfoList srcDesignFileInfos = srcDir.entryInfoList(nameFilters);
  foreach (QFileInfo srcFi, srcDesignFileInfos)
  {
    UtString srcFile;
    UtString destFile;
    srcFile << srcFi.absoluteFilePath();

    if (srcFi.isDir())
      continue;

    destFile << destinationDir << "/" << srcFi.baseName();
    if (srcFi.completeSuffix().length() > 0)
      destFile << "." << srcFi.completeSuffix();
    
    QString fileSuffix = srcFi.completeSuffix().toLower();

    if (fileSuffix == "a" || fileSuffix == "lib" || fileSuffix == "so" || fileSuffix == "dll")
      designLib = true;

    UtString err;
    if (!OSCopyFile(srcFile.c_str(), destFile.c_str(), &err))
    {
      qDebug() << "Failed to copy: " << err.c_str();
      status = false;
    }
    else
    {
      QString copyMsg = QString("Copied %1 to %2").arg(srcFile.c_str()).arg(destFile.c_str());
      statusBar->showMessage(copyMsg, 2000);  
      qDebug() << copyMsg;
    }
  }

  if (!designLib)
  {
    QMessageBox::critical(NULL, MODELSTUDIO_TITLE, "Unable to locate a .a or .lib file that goes with this design. Aborting");
    status = false;
  }

  return status;
}

// ccfgFile is the source file
// inside the ccfgFile is a reference to the database files
// we need to copy the database files and the ccfg file
QString CarbonProject::copyAndRenameCcfg(const char* ccfgFile, const char* ccfgDestDir, const char* designFilesDir)
{
  QString newDesignArchive;

  QFileInfo fi(ccfgFile);

  UtString ccfgDir;
  ccfgDir << fi.absolutePath();

 // Temp change to the ccfg dir so we can load the .ccfg file
  ProjectChangeOutputDirectory tmp(ccfgDir.c_str());

  CarbonCfgID cfg = carbonCfgCreate();

  // read any user transactor defs
  const char* xtorDefs = getProjectWidget()->context()->getQtContext()->getXtorDefFile();
  if (xtorDefs)
    carbonCfgReadXtorDefinitions(cfg, xtorDefs);

  CarbonCfgStatus cfgStatus = carbonCfgRead(cfg, ccfgFile);  

  // full absolute iodb filepath
  UtString iodbFilePath;
  
  // design base name e.g. "libdesign" 
  UtString designBaseName;
  
  if (cfgStatus == eCarbonCfgSuccess)
  {
    const char* iodbFile = carbonCfgGetIODBFile(cfg);
    const char* libraryFile = carbonCfgGetLibName(cfg);

    QFileInfo fiIodb(iodbFile);
    if (fiIodb.isRelative())
      OSConstructFilePath(&iodbFilePath, ccfgDir.c_str(), iodbFile);
    else
      iodbFilePath << iodbFile;

    QFileInfo fiIodbAbs(iodbFilePath.c_str());
    if (fiIodbAbs.exists())
    {
      UtString designDir;
      designDir << fiIodbAbs.absolutePath();

      designBaseName << fiIodbAbs.baseName();

      copyDesignFiles(designDir.c_str(), designBaseName.c_str(), designFilesDir);

      // Update the IODB reference
      UtString newIodb;
      newIodb << "../" << designBaseName << "." << fiIodb.completeSuffix();
      carbonCfgPutIODBFile(cfg, newIodb.c_str());

      // Update the library reference
      QFileInfo fiLib(libraryFile);
      UtString newLibName;
      newLibName << "../" << designBaseName << "." << fiLib.completeSuffix();
      carbonCfgPutLibName(cfg, newLibName.c_str());
      
      newDesignArchive = newLibName.c_str();

      // Destination new ccfg name
      UtString newCcfgName;
      newCcfgName << ccfgDestDir << "/" << designBaseName << ".ccfg";
      carbonCfgWrite(cfg, newCcfgName.c_str());
      carbonCfgDestroy(cfg);
    }
  }

  return newDesignArchive;
}

const char* CarbonProject::createNewProjectFromModelKit(QWidget*, const char* projectLocation, const char* projectName, const char* modelKitFile)
{
  QFileInfo fi(modelKitFile);
  mProjectType = ModelKitProject;

  const char* newProjectFile = createNewProject(projectLocation, projectName);

  UtString destDir;
  OSConstructFilePath(&destDir, projectLocation, "Linux");
  OSConstructFilePath(&destDir, destDir.c_str(), getActiveConfiguration());

  QDir qd;
  qd.mkpath(destDir.c_str());

  qDebug() << projectLocation << projectName << modelKitFile;

  save();

  mProjectWidget->loadProject(newProjectFile);
  CarbonProject* proj = mProjectWidget->project();
  mProjectWidget->putProject(proj);
  proj->mDbContext = new CarbonDatabaseContext(mProjectWidget->context()->getQtContext());

  CarbonOptions* options = proj->getActive()->getOptions("VSPCompiler");

  ModelKit* modelKit = new ModelKit();
  KitManifest* manifest = modelKit->openKitFile(modelKitFile, "");

  if (!modelKit->getLicenseKey().isEmpty())
  {
    UtString reason;
    qDebug() << "Checking license" << modelKit->getLicenseKey();
    if (!mProjectWidget->context()->checkoutRuntimeCustomer(modelKit->getLicenseKey(), &reason))
    {
      qDebug() << "No license for" << modelKit->getLicenseKey() << "reason" << reason.c_str();
      QString msg = QString("NO license for feature %1 was found\n%2").arg(modelKit->getLicenseKey()).arg(reason.c_str());
      QMessageBox::critical(NULL, MODELSTUDIO_TITLE, msg);
      theApp->shutdown(9);
      return "";
    }
  }

  if (manifest)
  {
    UtString libname;
    libname << manifest->getLibraryName();
    QFileInfo fi(libname.c_str());

    options->putValue("-o", libname.c_str());
    mProjectWidget->updateVHMName();

    foreach (QString compName, manifest->getComponents())
    {
      if (compName == "SocDesigner")
      {
        UtString ccfgName;
        ccfgName << proj->getActive()->getOutputDirectory() << "/" << MAXSIM_COMPONENT_SUBDIR << "/" << fi.baseName() << ".ccfg";
        mProjectWidget->createComponent(CarbonProjectWidget::eComponentMaxsim, ccfgName.c_str(), false);
      }
      else if (compName == "CoWare")
      {
        UtString ccfgName;
        ccfgName << proj->getActive()->getOutputDirectory() << "/" << COWARE_COMPONENT_SUBDIR << "/" << fi.baseName() << ".ccfg";
        mProjectWidget->createComponent(CarbonProjectWidget::eComponentCoWare, ccfgName.c_str(), false);
      }
      else if (compName == "SystemC")
      {
        mProjectWidget->createComponent(CarbonProjectWidget::eComponentSystemC, NULL);
      }
    }
    proj->save();
  }
  else
  {
    theApp->close();
  }

  mProjectWidget->closeProject();
  mProjectWidget->loadProject(newProjectFile);

  if (manifest)
  {
    qDebug() << "Starting Model Kit User Wizard";
    ModelKitUserWizard* wiz = new ModelKitUserWizard(NULL, modelKit, manifest);
    bool debugger = getenv("CARBON_MODELKIT_DEBUGGER");
    if (debugger)
      wiz->show();
    else
    {
      wiz->exec();
      theApp->close();
    }
  }
  else
    theApp->close();

  qDebug() << "Finished Model Kit User Wizard";
  qDebug() << "Shutting down exitStatus: " << theApp->getExitCode();

  return "";
}

// Create a project from an existing .ccfg file
// first, create the project dir
// then copy all the design files over
// fixup the reference to the .io.db or .symtab.db in the ccfg to the new
// project output directory, such as Linux/Default
// then write the .ccfg file into the new component dir "CoWare" or "Maxsim"
//
const char* CarbonProject::createNewProjectFromCcfg(QWidget*, const char* projectLocation, const char* projectName, const char* ccfgFile, DlgNewProject::ProjectKind kind)
{
  QFileInfo fi(ccfgFile);
  mProjectType = DatabaseOnly;

  UtString destBaseName;

  const char* newProjectFile = createNewProject(projectLocation, projectName);

  UtString destDir;
  OSConstructFilePath(&destDir, projectLocation, "Linux");
  OSConstructFilePath(&destDir, destDir.c_str(), getActiveConfiguration());

  QDir qd;
  qd.mkpath(destDir.c_str());
  
  // We now have a new Project and default configuration

  UtString newCcfgName;
  UtString platformDir;

  if (kind == DlgNewProject::FromCcfgCoware)
    platformDir << destDir << "/" << COWARE_COMPONENT_SUBDIR;
  else if (kind == DlgNewProject::FromCcfgMaxsim)
    platformDir << destDir << "/" << MAXSIM_COMPONENT_SUBDIR;

  qd.mkpath(platformDir.c_str());

  QString newArchiveName = copyAndRenameCcfg(ccfgFile, platformDir.c_str(), destDir.c_str());
  if (newArchiveName.length() > 0)
  {
    QFileInfo fi(newArchiveName);
    
    UtString designArchive;
    designArchive << fi.fileName();
 
    CarbonOptions* options = getActive()->getOptions("VSPCompiler");
    options->putValue("-o", designArchive.c_str());   

    save();

    mProjectWidget->loadProject(newProjectFile);

    options = getActive()->getOptions("VSPCompiler");
    options->putValue("-o", designArchive.c_str());   

    newCcfgName << platformDir << "/" << fi.baseName() << ".ccfg";

    // Now, create the component
    if (kind == DlgNewProject::FromCcfgCoware)
      mProjectWidget->createComponent(CarbonProjectWidget::eComponentCoWare, newCcfgName.c_str());
    else if (kind == DlgNewProject::FromCcfgMaxsim)
      mProjectWidget->createComponent(CarbonProjectWidget::eComponentMaxsim, newCcfgName.c_str());

    mProjectWidget->project()->save();
  }

  return newProjectFile;
}


QString CarbonProject::resolveFile(const QString& fileName)
{
  QFileInfo sfi(fileName);
  UtString srcPath;
  UtString filename;
  filename << fileName;

  if (!fileName.contains(ENVVAR_REFERENCE) && sfi.isRelative())
    OSConstructFilePath(&srcPath, getProjectDirectory(), filename.c_str());
  else
    srcPath << fixVarReferences(fileName);

  UtString unixPath;
  unixPath << getUnixEquivalentPath(srcPath.c_str());

  return unixPath.c_str();
}


void CarbonProject::addSourceFile(const QString& cmdFilePath, const QString& fileName, bool libraryFile, bool createGUIElements, const QString& folderName )
{
  CarbonSourceGroups* groups = getGroups();
  UtString file;
  file << fileName;

  QFileInfo fi(cmdFilePath);

  QFileInfo nfi(file.c_str());
  bool skipFileAdd = false;

  UtString filePath;
  if (!fileName.contains(ENVVAR_REFERENCE) && nfi.isRelative())
  {
    UtString absPath;
    absPath << fi.absolutePath();
    OSConstructFilePath(&filePath, absPath.c_str(), file.c_str());
  }
  else
    filePath << fixVarReferences(fileName);

  UtString relFilePath;
  relFilePath << getUnixEquivalentPath(filePath.c_str());

  if (groups->numGroups() > 0)
  {
    CarbonSourceGroup* group = NULL;
    if (folderName.isEmpty())
      group = getGroups()->findGroupByLibraryName(mLastLibraryName.c_str());
    else
    {
      UtString libname;
      libname << folderName;
      group = getGroups()->findGroupByLibraryName(libname.c_str());
    }

    if (group == NULL) // default 
      group = groups->getGroup(0); // first group

    INFO_ASSERT(group, "Expecting a group");

    // check to see if we already have this file added, if so, skip it
    CarbonHDLSourceFile* srcFile = group->findFile(relFilePath.c_str());
    if (srcFile)
    {
      qDebug() << "Skipping file import: " << relFilePath.c_str() << " already addded";
      skipFileAdd = true;
    }

    CarbonProject::HDLType hdlType = CarbonProjectWidget::hdlFileType(file.c_str());

    CarbonHDLFolderItem* parentItem = NULL;
    if (createGUIElements)
      parentItem = mProjectWidget->findGroup(group->getName());

    UtString title;
    title << nfi.fileName();

    if (!skipFileAdd)
    {
      qDebug() << " importing src: " << fileName;
      switch (hdlType)
      {
      case CarbonProject::VHDL:  
        {
          qDebug() << " process VHDL file: " << relFilePath.c_str();
          CarbonVHDLSource* src = new CarbonVHDLSource(group, relFilePath.c_str());
          srcFile = src;
          src->putLibraryFile(libraryFile);
          group->addSource(src);
          if (createGUIElements)
          {
            CarbonVhdlItem* item = new CarbonVhdlItem(mProjectWidget, src, parentItem);
            item->setData(title.c_str(), relFilePath.c_str());
          }

          break;
        }
      case CarbonProject::Verilog:
        {
          qDebug() << " process Verilog file: " << relFilePath.c_str();
          CarbonVerilogSource* src = new CarbonVerilogSource(group, relFilePath.c_str());
          srcFile = src;
          src->putLibraryFile(libraryFile);
          group->addSource(src);

          if (createGUIElements)
          {
            CarbonVerilogItem* item = new CarbonVerilogItem(mProjectWidget, src, parentItem);
            item->setData(title.c_str(), relFilePath.c_str());
          }
          break;
        }
      case CarbonProject::CPP:
        {
          qDebug() << " process CPP file: " << relFilePath.c_str();
          CarbonCPPSource* src = new CarbonCPPSource(group, relFilePath.c_str());
          srcFile = src;
          src->putLibraryFile(libraryFile);
          group->addSource(src);
          if (createGUIElements)
          {
            CarbonCPPItem* item = new CarbonCPPItem(mProjectWidget, src, parentItem);
            item->setData(title.c_str(), relFilePath.c_str());
          }

          break;
        }
      case CarbonProject::Header:
        {
          qDebug() << " process H file: " << relFilePath.c_str();
          CarbonHSource* src = new CarbonHSource(group, relFilePath.c_str());
          srcFile = src;
          src->putLibraryFile(libraryFile);
          group->addSource(src);
          if (createGUIElements)
          {
            CarbonHItem* item = new CarbonHItem(mProjectWidget, src, parentItem);
            item->setData(title.c_str(), relFilePath.c_str());
          }
          break;
        }
      }
    }
  }
  else
    qDebug() << "NO groups!";
}
QString CarbonProject::getUnixEquivalentPath(const QString& windowsPath)
{
  UtString tmp;
  tmp << windowsPath;
  return getUnixEquivalentPath(tmp.c_str());
}

QString CarbonProject::getUnixEquivalentPath(const char* windowsPath)
{
  QString value = windowsPath;
#if pfWINDOWS
  CarbonMappedDrives mappedDrives;
  QString winPath(windowsPath);
  // Is this a dos path? (<x>:)
  if (winPath.length() >= 2 && winPath[1] == ':')
  {
    QString driveLetter = winPath.left(2).toUpper();
    for (int i=0; i<mappedDrives.numMappings(); i++)
    {
      CarbonMappedDrive* drive = mappedDrives.getMapping(i);
      if (driveLetter == drive->mDriveLetter.c_str())
      {
        UtString resolvedPath;
        resolvedPath = drive->mUnixPath << winPath.mid(2);
        value = resolvedPath.c_str();
        break;
      }
    }
  }
#endif
  return value.replace("\\","/");
}

QString CarbonProject::getWindowsEquivalentPath(const QString& unixPath)
{
  UtString tmp;
  tmp << unixPath;
  return getWindowsEquivalentPath(tmp.c_str());
}

// Returns windows equivalent path for a unixpath
// for example /home/cds/eric/foo.v would resolve to z:/foo.v if
// z: was mapped to /home/cds/eric
// returns empty string if path cannot be resolved
QString CarbonProject::getWindowsEquivalentPath(const char* unixPath)
{
  QString value = unixPath;
  QString valuePath = unixPath;

#if pfWINDOWS
  value = "";

  // if we pass in a DOS path, leave it alone, return it unchanged
  if (valuePath.contains(ENVVAR_REFERENCE) || (strlen(unixPath) >= 2 && unixPath[1] == ':'))
    return CarbonProject::fixVarReferences(unixPath);

  CarbonMappedDrives mappedDrives;
  for (int i=0; i<mappedDrives.numMappings(); i++)
  {
    CarbonMappedDrive* drive = mappedDrives.getMapping(i);
    QString mappedUnixPath = drive->mUnixPath.c_str();
    if (valuePath.startsWith(mappedUnixPath))
    {
      QString pathSuffix = valuePath.mid(mappedUnixPath.length());
      UtString resolvedPath;

      if (pathSuffix.length() > 0)
        resolvedPath = drive->mDriveLetter << pathSuffix;
      else 
        resolvedPath = drive->mDriveLetter << "/";

      value = resolvedPath.c_str();
      break;
    }
  }

#endif

  return value;
}

// Tests unixpath against the drivemappings to determine
// if the unix path is accessible from Windows
// returns true if it can, false otherwise, always true on unix
bool CarbonProject::isMappedUnixPath(const char* unixPath)
{
  bool onMappedPath = true;
  QString valuePath = unixPath;

#if pfWINDOWS
  CarbonMappedDrives mappedDrives;
  onMappedPath = false;

  for (int i=0; i<mappedDrives.numMappings(); i++)
  {
    CarbonMappedDrive* drive = mappedDrives.getMapping(i);
    QString mappedUnixPath = drive->mUnixPath.c_str();
    QString driveLetterPath = valuePath.left(2).toUpper();

    if (valuePath.startsWith(mappedUnixPath) || driveLetterPath.startsWith(drive->mDriveLetter.c_str()))
    {
      onMappedPath = true;
      break;
    }
  }
#endif

  return onMappedPath;
}

bool CarbonProject::checkFilePath(const char* fileDir, const char* filePath)
{
  bool fileExists = true;

  QString qFilePath = filePath;

  // If the value contains $(vars), assume its correct
  if (qFilePath.contains(ENVVAR_REFERENCE))
    return true;
  
  QFileInfo fileInfo(filePath);

  if (!fileInfo.isRelative())
  {
    bool onMappedPath = true;
#if pfWINDOWS
    onMappedPath = isMappedUnixPath(filePath);
    QFileInfo fi(getWindowsEquivalentPath(filePath));
#else
    QFileInfo fi(filePath);
#endif
    if (!onMappedPath || !fi.exists())
      fileExists = false;
  }
  else // Its a relative path, first, make it an absolute path
  {
    UtString absFilePath;
    OSConstructFilePath(&absFilePath, fileDir, filePath);
    bool onMappedPath = true;
#if pfWINDOWS
    onMappedPath = isMappedUnixPath(absFilePath.c_str());
    QFileInfo fi(getWindowsEquivalentPath(absFilePath.c_str()));
#else
    QFileInfo fi(filePath);
#endif

    if (!onMappedPath || !fi.exists())
      fileExists = false;
  }

  return fileExists;
}
// If we are importing on windows, scan the file for
// any absolute paths and test to see if we have mapped drives
bool CarbonProject::checkCommandFilePaths(const char* cmdFile)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  QString cmdFileName = cmdFile;

  bool retValue = true;
#if pfWINDOWS
  UtString errorsString;
  bool errorsFound = false;
  CarbonConsole* console = mProjectWidget->getConsole();

  if (console)
  {
    console->clear();
    mProjectWidget->makeConsoleVisible();
  }

  CarbonMappedDrives mappedDrives;

  QFileInfo fiCmdFile(cmdFile);
  UtString cmdFileDir;
  cmdFileDir << fiCmdFile.absolutePath();
  TempChangeDirectory td1(cmdFileDir.c_str());

  QFile file(cmdFile);
  // File existence was already checked
  file.open(QIODevice::ReadOnly | QIODevice::Text);
  QTextStream in(&file);
  while (!in.atEnd()) 
  {
    QString line = in.readLine();
    // skip comments
    if (line.left(2) == "//")
      continue;

    qDebug() << "cmd file line: " << line;

    QStringList tokens = line.split(" ");
    INFO_ASSERT(tokens.count() > 0, "Bad Command Line");

    UtString args;
    for (int i=1; i<tokens.count(); i++)
    {
      if (args.length() > 0)
        args << " ";
      args << tokens[i];
    }

    // is is a switch? or a file?
    if (tokens[0].left(1) == "-" || tokens[0].left(1) == "+")
    {
      UtString switchName;
      UtString switchValue;

      QRegExp plusSwitch("(\\+[^\\+]+\\+)(.*)");
      if (plusSwitch.exactMatch(tokens[0]))
      {
        switchName << plusSwitch.cap(1);
        switchValue << plusSwitch.cap(2);
      }
      else
      {
        switchValue << args;
        switchName << tokens[0];
      }

      for (int is=0; is<(int)(sizeof(adjustPathSwitches)/sizeof(const char*)); is++)
      {
        if (switchName == adjustPathSwitches[is])
        {
          qDebug() << "Switchname: " << switchName.c_str() << " value: " << switchValue.c_str();

          if (!checkFilePath(cmdFileDir.c_str(), switchValue.c_str()))
          {
            errorsFound = true;
            UtString msg;
            msg << "Error: Unable to resolve path: " << switchValue.c_str();
            if (console)
              console->processOutput(CarbonConsole::Local, CarbonConsole::Check, msg.c_str());            
          }
          break;
        }
      }
    }
    else // it's a filename
    {
      QString fileName = tokens[0];

      QFileInfo fi(fileName);
      UtString filePath;
      filePath << fileName;

      if (!checkFilePath(cmdFileDir.c_str(), filePath.c_str()))
      {
        errorsFound = true;
        UtString msg;
        msg << "Error: Unable to resolve path: " << filePath.c_str();
        if (console)
          console->processOutput(CarbonConsole::Local, CarbonConsole::Check, msg.c_str());            
      }
    }
  }

  QApplication::restoreOverrideCursor();

  if (errorsFound)
  {
    retValue = false;

    errorsString << "The command file contains file references which do not reside" << "\n";
    errorsString << "on any of your currently mapped drives, please map these paths first and try again.\n";
    errorsString << "The console window contains a lisf of all non-resolved file references.";

    QMessageBox::warning(getProjectWidget(), MODELSTUDIO_TITLE, errorsString.c_str());
  }
#else
  QApplication::restoreOverrideCursor();
#endif

  return retValue;
}

void CarbonProject::applySwitch(const char* switchName, const char* switchValue)
{
  // First, check for file-specific switch, if it is apply it there, otherwise
  // its an overall switch
  CarbonOptions* toolOptions = getToolOptions("VSPCompiler");
  const CarbonProperty* option = toolOptions->locateProperty(switchName);
  qDebug() << "Apply: " << switchName << " = " << switchValue;
  
  toolOptions->putValue(option, switchValue);
  Switch* sw = new Switch();
  sw->mName = switchName;
  sw->mValue = switchValue;
  mImportSwitches.append(sw);
}

// Read in the Command File and process it (line by line)
void CarbonProject::processCommandFile(const char* cmdFile, bool createGUIElements)
{
  mImportSwitches.clear(); // First, wipe out the accumulated list of switches

  // First, reset any file specific options to their default value
  for (CarbonOptions::OptionsLoop p = mFileOptions->loopOptions(); !p.atEnd(); ++p)
  {
    CarbonPropertyValue* sv = p.getValue();
    const CarbonProperty* prop = sv->getProperty();
    sv->putValue(prop->getDefaultValue());
  }

  QFile file(cmdFile);
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    qDebug() << "Unable to read command file: " << cmdFile;
    return;
  }


  // Verify all files references by this command file
  // may are accessible
  if (!checkCommandFilePaths(cmdFile))
    return;

  QApplication::setOverrideCursor(Qt::WaitCursor);

  QFileInfo fi(cmdFile);

  CarbonOptions* toolOptions = getToolOptions("VSPCompiler");

  QTextStream in(&file);
  while (!in.atEnd()) 
  {
    QString line = in.readLine().trimmed();

    // skip blanks
    if (line.length() == 0)
      continue;

    // skip comments
    if (line.left(2) == "//" || line.left(1) == "#")
      continue;

    qDebug() << "processing: " << line;

    QStringList tokens = line.split(" ");

    UtString args;
    for (int i=1; i<tokens.count(); i++)
    {
      if (args.length() > 0)
        args << " ";
      args << tokens[i];
    }
    // is is a switch? or a file?
    if (tokens[0].left(1) == "-" || tokens[0].left(1) == "+")
    {
      UtString switchName;
      UtString switchValue;
      switchName << tokens[0];
      if (switchName == "-incdir")
        switchName = "+incdir+";

      // some switches we want to ignore, so skip those here.

      bool ignoreIt = false;
      for (int is=0; !ignoreIt && is<(int)(sizeof(ignoreSwitches)/sizeof(const char*)); is++)
      {
        if (switchName == ignoreSwitches[is])
        {
          qDebug() << "Ignoring switch: " << switchName.c_str();
          ignoreIt = true;
          break;
        }
      }

      if (ignoreIt)
        continue;


      // handle vhdl/verilog libraries here
      if (switchName == "-vhdlLib" || switchName == "-vlogLib")
      {
        mLastLibraryName.clear();
        mLastLibraryName << tokens[1];

        CarbonSourceGroup* group = getGroups()->findGroupByLibraryName(mLastLibraryName.c_str());
        if (group == NULL)
        {
          group = new CarbonSourceGroup(getGroups());
          if (switchName == "-vhdlLib")
            group->putVhdlLibrary(mLastLibraryName.c_str());
          else
            group->putVerilogLibrary(mLastLibraryName.c_str());

          group->putName(mLastLibraryName.c_str());
          getGroups()->addGroup(group);

          if (createGUIElements)
          {
            CarbonHDLFolderItem* folder = new CarbonHDLFolderItem(mProjectWidget, mProjectWidget->getRTLFolder(), group);
            folder->setText(0, group->getName());
            folder->setExpanded(true);
          }

          // Now Apply any specific switches here.
          foreach (Switch* sw, mImportSwitches)
          {
            UtString swName;
            swName << sw->mName;
            const CarbonProperty* fileProp = mFileOptions->findProperty(swName.c_str());
            if (fileProp)
            {
              UtString swValue;
              swValue << sw->mValue;

              CarbonOptions* groupOptions = group->getOptions();
              const CarbonProperty* libProp = groupOptions->findProperty(swName.c_str());
              if (libProp)
                groupOptions->putValue(libProp, swValue.c_str());
            }
          }
        }
        continue;
      }

      // -v indicates a "special" kind of source file, handle that here
      if (switchName == "-v") // Library File
      {
        addSourceFile(cmdFile, args.c_str(), true, createGUIElements);
        continue;
      }

      // everything between the plusses +define+a+b+c 
      // yields cap(1) = define
      //        cap(2) = a+b+c
      //
      QRegExp plusSwitch("(\\+[^\\+]+\\+)(.*)");
      if (plusSwitch.exactMatch(tokens[0]))
      {
        switchName.clear();
        switchName << plusSwitch.cap(1);
        switchValue << plusSwitch.cap(2);
      }
      else
        switchValue = args;

      QString qSwitchValue = switchValue.c_str();
      bool isEnvPath = qSwitchValue.contains(ENVVAR_REFERENCE);

      // Use locateProperty for special handling of +name+ operators
      UtString lastSwitchValue;
      const CarbonProperty* option = toolOptions->locateProperty(switchName.c_str());
      if (option)
      {
        // an Input file
        if (option->getKind() == CarbonProperty::InFile)
        {
          UtString file = switchValue;

          UtString filePath;
          QFileInfo nfi(file.c_str());
          if (!isEnvPath && nfi.isRelative())
          {
            UtString absPath;
            absPath << fi.absolutePath();
            OSConstructFilePath(&filePath, absPath.c_str(), file.c_str());
          }
          else
            filePath << fixVarReferences(file.c_str());

          UtString relFilePath;
          relFilePath << getUnixEquivalentPath(filePath.c_str());

          applySwitch(switchName.c_str(), relFilePath.c_str());
        }
        else if (option->getKind() == CarbonProperty::Filelist)
        {
          qDebug() << "filelist, prop: " << switchName.c_str() << " switchvalue: " << switchValue.c_str();

          UtString currValue;
          CarbonPropertyValue* v = toolOptions->getValue(option, NULL, NULL);
          currValue << v->getValue();
          if (currValue.length() > 0)
            currValue << ";";

          // if the path is relative, it is relative
          // to the command file, so first make it absolute to the command
          // file, then relative to the project directory.
          QFileInfo nfi(switchValue.c_str());
          UtString tmpFile;
          if (!isEnvPath && nfi.isRelative())
          {
            UtString absPath;
            absPath << fi.absolutePath();
            OSConstructFilePath(&tmpFile, absPath.c_str(), switchValue.c_str());
          }
          else
            tmpFile << fixVarReferences(switchValue.c_str());

          UtString unixPath;
          unixPath << getUnixEquivalentPath(tmpFile.c_str());


          UtString out;
          const char* quotedVal = UtShellTok::quote(unixPath.c_str(), &out, false, ";");
          currValue << quotedVal;

          applySwitch(switchName.c_str(), currValue.c_str());
        }
        else if (option->getKind() == CarbonProperty::Stringlist)
        {
          // fixPaths;
          bool fixPaths = false;
          for (int is=0; !fixPaths && is<(int)(sizeof(adjustPathSwitches)/sizeof(const char*)); is++)
          {
            if (switchName == adjustPathSwitches[is])
              fixPaths = true;
          }

          if (fixPaths)
          {
            UtString file = switchValue;
            UtString filePath;
            QFileInfo nfi(file.c_str());
            if (!isEnvPath && nfi.isRelative())
            {
              UtString absPath;
              absPath << fi.absolutePath();
              OSConstructFilePath(&filePath, absPath.c_str(), file.c_str());
            }
            else
              filePath << fixVarReferences(file.c_str());

            UtString relFilePath;
            relFilePath << getUnixEquivalentPath(filePath.c_str());

            qDebug() << "converting " << switchName.c_str() << " from: " << switchValue.c_str() << " to: " << relFilePath.c_str();
            switchValue = relFilePath;
          }

          UtString currValue;
          CarbonPropertyValue* v = toolOptions->getValue(option, NULL, NULL);
          currValue << v->getValue();
          if (currValue.length() > 0)
            currValue << ";";
          UtString out;
          const char* quotedVal = UtShellTok::quote(switchValue.c_str(), &out, false, ";");
          currValue << quotedVal;

          applySwitch(switchName.c_str(), currValue.c_str());
        }
        else if (option->getKind() == CarbonProperty::Bool)
        {
          applySwitch(switchName.c_str(), "true");
        }
        else
        {
          applySwitch(switchName.c_str(), switchValue.c_str());
        }
      }
      else // unknown switch goes in "Additional Options"
      {
        option = toolOptions->locateProperty("Additional Options");
        if (option)
        {
          UtString currValue;
          CarbonPropertyValue* v = toolOptions->getValue(option, NULL, NULL);
          currValue << v->getValue();

          if (currValue.length() > 0)
            currValue << ";";

          currValue << switchName;

          if (switchValue.length() > 0)
            currValue << " " << switchValue;

          toolOptions->putValue(option, currValue.c_str());
        }
        else
        {
          qDebug() << "ERROR: Unable to locate switch: " << switchName.c_str();
          INFO_ASSERT(false, "Invalid Switch");
        }
      }
    }
    else // it's a file argument
    {
      QString srcFile = tokens[0];
      addSourceFile(cmdFile, tokens[0], false, createGUIElements);
    }

    if (tokens.count() > 1)
      qDebug() << "  args: " << args.c_str();
  }

  QApplication::restoreOverrideCursor();
}
CarbonDatabaseContext* CarbonProject::getDbContext() const 
{
  return mDbContext;
}
CarbonProject::~CarbonProject()
{
  delete mComponents;
  delete mOptions;
  delete mConfigs;
  delete mDbContext;
  delete mFileOptions;
}

const char* CarbonProject::getVHMName()
{
  static UtString vhmName;
  vhmName.clear();

  CarbonPropertyValue* outSwitch = getActive()->getOptions("VSPCompiler")->getValue("-o");
  const char* outputTarget = outSwitch->getValue();
  QFileInfo fi(outputTarget);
  vhmName << fi.fileName();
  return vhmName.c_str();
}

// Returns full path to design file for active cofniguration
const char* CarbonProject::getConfigDesignFilePath(const char* configName, const char* extension)
{
  static UtString designFileName;
  designFileName.clear();

  CarbonConfiguration* config = getConfigurations()->findConfiguration(configName);

  CarbonPropertyValue* outSwitch = config->getOptions("VSPCompiler")->getValue("-o");
  const char* outputTarget = outSwitch->getValue();
  QFileInfo fi(outputTarget);
  UtString vhmName;
  vhmName << fi.fileName();

  UtString designfile;
  designfile << fi.baseName() << extension;

  OSConstructFilePath(&designFileName, config->getOutputDirectory(), designfile.c_str());

  return designFileName.c_str();
}

// Return .io.db, .symtab.db or .gui.db
QString CarbonProject::getDatabaseFilePath()
{
  QString iodbName = getDesignFilePath(".io.db");
  QString guidbName = getDesignFilePath(".gui.db");
  QString symtabDb = getDesignFilePath(".symtab.db");

  QFileInfo sfi(symtabDb);
  if (sfi.exists())
    return symtabDb;

  QFileInfo ifi(iodbName);
  if (ifi.exists())
    return iodbName;

  QFileInfo gfi(guidbName);
  if (gfi.exists())
    return guidbName;

  return symtabDb;
}

QString CarbonProject::getConfigDatabaseFilePath(const QString& configName)
{
  UtString uConfigName; uConfigName << configName;
 
  QString iodbName = getConfigDesignFilePath(uConfigName.c_str(), ".io.db");
  QString guidbName = getConfigDesignFilePath(uConfigName.c_str(), ".gui.db");
  QString symtabDb = getConfigDesignFilePath(uConfigName.c_str(), ".symtab.db");

  QFileInfo sfi(symtabDb);
  if (sfi.exists())
    return symtabDb;

  QFileInfo ifi(iodbName);
  if (ifi.exists())
    return iodbName;

  QFileInfo gfi(guidbName);
  if (gfi.exists())
    return guidbName;

  return "";
}


// Returns full path to design file for active cofniguration
const char* CarbonProject::getDesignFilePath(const char* extension)
{
  static UtString designFileName;
  designFileName.clear();

  CarbonConfiguration* cfg = getActive();
  if (cfg)
  {
    CarbonPropertyValue* outSwitch = getActive()->getOptions("VSPCompiler")->getValue("-o");
    const char* outputTarget = outSwitch->getValue();
    QFileInfo fi(outputTarget);
    UtString vhmName;
    vhmName << fi.fileName();

    UtString designfile;
    designfile << fi.baseName() << extension;

    OSConstructFilePath(&designFileName, getActive()->getOutputDirectory(), designfile.c_str());

    return designFileName.c_str();
  }
  else
    return "";
}

void CarbonProject::checkPidFile(const char* pidFile)
{
  qDebug() << "checking pid file: " << pidFile;

  CarbonConsole::CommandMode mode = CarbonConsole::Local;
  if (mProjectWidget->isRemoteCompilation())
    mode = CarbonConsole::Remote;

  bool parsedFile = false;
  do
  {
    QFile xf(pidFile);
    if (xf.open(QFile::ReadOnly))
    {
      QTextStream ts(&xf);
      QString buffer = ts.readAll();
      UtString sbuffer;
      sbuffer << buffer;

      xmlDocPtr doc = xmlReadMemory(sbuffer.c_str(), buffer.length(), pidFile, NULL, 0);
      if (doc == NULL) 
      {
        UtString message;
        message << "Failed to parse file " <<  pidFile;
        qDebug() << message.c_str();
        OSSleep(1);
      }
      else
      {
        xmlNode *rootNode = xmlDocGetRootElement(doc);
        if (0 == xmlStrcmp(rootNode->name, BAD_CAST "Start"))
        {
          UtString projectDir;
          UtString systemName;
          UtString simPID;
          UtString msPID;
          UtString cssName;
          UtString simDir;

          for (xmlNodePtr child = rootNode->children; child != NULL; child = child->next) 
          {
            const char* elementName = XmlParsing::elementName(child);
            if (elementName == NULL)
              continue;
            if (0 == strcmp(elementName, "ProjectDir"))
              XmlParsing::getContent(child, &projectDir);
            else if (0 == strcmp(elementName, "SystemName"))
              XmlParsing::getContent(child, &systemName);
            else if (0 == strcmp(elementName, "SimulationPID"))
              XmlParsing::getContent(child, &simPID);
            else if (0 == strcmp(elementName, "ModelStudioPID"))
              XmlParsing::getContent(child, &msPID);
            else if (0 == strcmp(elementName, "CurrentDir"))
              XmlParsing::getContent(child, &simDir);
            else if (0 == strcmp(elementName, "Css"))
              XmlParsing::getContent(child, &cssName);
          }

          qDebug() << "CSSName: " << cssName.c_str();

          UtString cssFileName;
          if (mode == CarbonConsole::Remote)
          {
            cssFileName << cssName.c_str();
            const char* localProjectDir = getProjectDirectory();
            UtString rd;
            rd << getRemoteWorkingDirectory();
            const char* remoteDir = rd.c_str();
            replaceSubstring(&cssFileName, remoteDir, localProjectDir);
            replaceSubstring(&simDir, remoteDir, localProjectDir);
          }
          else
            cssFileName << cssName.c_str();

          qDebug() << "ProjectDir: " << projectDir.c_str();
          qDebug() << "Css: " << cssName.c_str();
          qDebug() << "SimDir: " << simDir.c_str();

          UtString primaryLocation;
          OSConstructFilePath(&primaryLocation, simDir.c_str(), cssName.c_str());

          // Second location, <project>
          UtString altLocation;
          OSConstructFilePath(&altLocation, projectDir.c_str(), cssName.c_str());

          // Third location, <project>/Linux/Default
          UtString altLocation2;
          OSConstructFilePath(&altLocation2, projectDir.c_str(), getActiveConfiguration());
          OSConstructFilePath(&altLocation2, primaryLocation.c_str(), cssName.c_str());

          QFileInfo fiAbsolute(cssName.c_str());
          QFileInfo fiPrimary(primaryLocation.c_str());
          QFileInfo fiAlt(altLocation.c_str());
          QFileInfo fiAlt2(altLocation2.c_str());

          if (fiAbsolute.exists())
          {
            qDebug() << "opening absolute css location: " << cssName.c_str();
            mProjectWidget->openSource(cssName.c_str());
          }
          else if (fiPrimary.exists())
          {
            qDebug() << "opening primary css location: " << primaryLocation.c_str();
            mProjectWidget->openSource(primaryLocation.c_str());
          }
          else if (fiAlt.exists())
          {
            qDebug() << "opening alternate css location: " << altLocation.c_str();
            mProjectWidget->openSource(altLocation.c_str());
          }
          else if (fiAlt2.exists())
          {
            qDebug() << "opening alternate css location: " << altLocation2.c_str();
            mProjectWidget->openSource(altLocation2.c_str());
          }
          else
          {
            UtString msg;
            msg << "Error: Unable to locate the css file in any of the following locations:\n";
            msg << primaryLocation << "\n";
            msg << altLocation << "\n";
            msg << altLocation2 << "\n";

            QMessageBox::warning(mProjectWidget->context()->getMainWindow(), MODELSTUDIO_TITLE, msg.c_str());
          }

          qDebug() << "Parsed PID File: " << pidFile;

          QFile pf(pidFile);
          if (pf.exists())
          {
            pf.remove();
            qDebug() << "  Removed PID file: " << pidFile;
          }
          parsedFile = true;
        }
      }
    }
  }
  while (!parsedFile);
}

// Interrupt when .carbon directory changes
void CarbonProject::carbonDirectoryChanged(const QString& fn)
{
  qDebug() << "CarbonDir Changed: " << fn;

  UtString carbonDir;
  carbonDir << fn;

  UtString pidFile;
  pidFile << "ms.carbon." << OSGetPid();

  UtString thisPidFile;
  OSConstructFilePath(&thisPidFile, carbonDir.c_str(), pidFile.c_str());

  QFileInfo fi(thisPidFile.c_str());
  if (fi.exists())
    checkPidFile(thisPidFile.c_str());
}

void CarbonProject::cleanupMonitor()
{
  UtString monitorDir;
  OSConstructFilePath(&monitorDir, mProjectDir.c_str(), ".monitor");

  UtString pidFile;
  pidFile << "ms.carbon." << OSGetPid();

  UtString thisPidFile;
  OSConstructFilePath(&thisPidFile, monitorDir.c_str(), pidFile.c_str());

  QFile pf(thisPidFile.c_str());
  if (pf.exists())
  {
    pf.remove();
    qDebug() << "Removing old pid file: " << thisPidFile.c_str();
  }
}

// If we have any switches with a GUI default
// which is different than the cbuild default
// we create a switch here, but only if the switch
// wasn't already set by the project.
void CarbonProject::createNonDefaultSwitches()  
{
  // foreach Configuration
  for (UInt32 i=0; i<mConfigs->numConfigurations(); i++)
  {
    CarbonConfiguration* config = mConfigs->getConfiguration(i);
    CarbonOptions* options = config->getOptions("VSPCompiler");

    const CarbonProperties* props = options->getProperties();
    
    // For all properties
    for (UInt32 g=0; g<props->numGroups(); g++)
    {
      CarbonPropertyGroup* group = props->getGroup(g);
      for (CarbonPropertyGroup::SwitchLoop p = group->loopSwitches(); !p.atEnd(); ++p)
      {
        CarbonProperty* prop = p.getValue();
        // Has a GUI Default
        if (prop->hasGuiDefaultValue())
        {
          // Check for existing switch
          CarbonPropertyValue* value = options->lookupValue(prop);
          if (value == NULL)
            options->putValue(prop->getName(), prop->getGuiDefaultValue());
        }
      }      
    }
  }  
}

bool CarbonProject::loadProject(const char* fileName)
{
  bool result = false;
  mProjectFile = fileName;
  mProjectDir.clear();
  mShortName.clear();
  mModelCompilationStarted = false;
  mCarbonModelWasCompiled = false;

  QFileInfo fi(fileName);

  mProjectDir << fi.dir().absolutePath();
  mShortName << fi.fileName();

  // if true, it's per project
  if (theApp->variablesScope())
    theApp->removeAllEnvironmentVariables();

  setEnvironmentVariables();

  UtString monitorDir;


  // QT FileSystemWatcher is not reliable on Network Filesystems, so on windows
  // default to a local filesystem.
#if pfWINDOWS
  monitorDir = "C:/.carbon/.monitor";
#else
  OSConstructFilePath(&monitorDir, mProjectDir.c_str(), ".monitor");
#endif

  QDir qd;
  qd.mkpath(monitorDir.c_str());

  if (fi.exists())
    result = readFile(fileName);
  else
  {
    UtString msg;

    msg << "No such file: " << fileName;
    QMessageBox::warning(mProjectWidget, MODELSTUDIO_TITLE, msg.c_str());
    result = false;
  }

  if (result)
  {
    setEnvironmentVariables();
    createNonDefaultSwitches();  
    emit projectLoaded(fileName, this);

    checkProject();
  }

  if (mProjectWatcher)
    delete mProjectWatcher;

  mProjectWatcher = new QFileSystemWatcher(this);
  mProjectWatcher->addPath(monitorDir.c_str()); // Directory

  CQT_CONNECT(mProjectWatcher, directoryChanged(const QString&), this, carbonDirectoryChanged(const QString&));

  // Set this to local for non-Windows
#ifndef Q_OS_WIN
  getProjectOptions()->putValue("Simulation Script Execution", "local");
#endif

  setDefaultLibraryName();
  
  // Cause the Package component to be created, so Package Options appears in the tree.
  getProjectWidget()->createPackageComp();

  // Use the values provided by user as starting point, if not, default to sane choice
  // getenv will return NULL which is a QString empty
  QString hostArch = getenv("CARBON_HOST_ARCH");
  QString targetArch = getenv("CARBON_TARGET_ARCH");
  if (hostArch.isEmpty())
    hostArch = "Linux";
  if (targetArch.isEmpty())
    targetArch = "Linux";

  CarbonOptions* options = getToolOptions("VSPCompiler");
  CarbonPropertyValue* compileControl = options->getValue("Compiler Control");
  if (compileControl)
  {
    if (hostArch == "Linux" && targetArch == "Linux")
      compileControl->putValue(MODEL_32x32);
    else if (hostArch == "Linux64" && targetArch == "Linux")
      compileControl->putValue(MODEL_64x32);
    else if (hostArch == "Linux64" && targetArch == "Linux64")
      compileControl->putValue(MODEL_64x64);
  }
 

  return result;
}

void CarbonProject::setEnvironmentVariables()
{
  static UtString envCarbonOutput;
  static UtString envCarbonConfig;

  envCarbonOutput.clear();
  envCarbonConfig.clear();

  mEnvCarbonProj.clear();
  mEnvCarbonProj << "CARBON_PROJECT=" << mProjectDir;

  theApp->setEnvironment("CARBON_PROJECT", mProjectDir.c_str());

  putenv((char*)mEnvCarbonProj.c_str());

  mEnvCarbonProjRel.clear();
  mEnvCarbonProjRel << "CARBON_PROJECT_RELATIVE=../..";
  putenv((char*)mEnvCarbonProjRel.c_str());

  CarbonConfiguration* activeConfig = getActive();
  if (activeConfig)
  {
    QString outputVal = activeConfig->getOutputDirectory();
    QString configVal = QString("%1/%2").arg(getActivePlatform()).arg(activeConfig->getName());

    envCarbonOutput << "CARBON_OUTPUT=" << outputVal;
    envCarbonConfig << "CARBON_CONFIGURATION=" << configVal;

    theApp->setEnvironment("CARBON_OUTPUT", outputVal);
    theApp->setEnvironment("CARBON_CONFIGURATION", configVal);

    putenv((char*)envCarbonOutput.c_str());
    putenv((char*)envCarbonConfig.c_str());
  }
  else // remove them
  {
    theApp->setEnvironment("CARBON_OUTPUT","");
    theApp->setEnvironment("CARBON_CONFIGURATION","");
    putenv((char*)"CARBON_OUTPUT=");
    putenv((char*)"CARBON_CONFIGURATION=");
  }
}

void CarbonProject::setDefaultLibraryName(const char* configName)
{
  // Set the default design name to lib<Project>.[lib|a] only if the value is 
  // currently set to the default name.
  CarbonOptions* options = getToolOptions("VSPCompiler");
  if (configName != NULL)
  {
    CarbonConfiguration* config = getConfigurations()->findConfiguration(configName);
    options = config->getOptions("VSPCompiler");
  }

  // has the value been set yet?
  CarbonPropertyValue* outnameValue = options->getValue("-o");
  if (0 == strlen(outnameValue->getValue()))
  {
    QFileInfo projfi(shortName());
    QString shortName = projfi.baseName();
    shortName.replace(" ", "_");
    UtString newDefault;
#if pfWINDOWS
    newDefault << "lib" << shortName << ".lib";
#else
    newDefault << "lib" << shortName << ".a";
#endif
    options->putValue("-o", newDefault.c_str());
    outnameValue->putValue(newDefault.c_str());
  }
}
void CarbonProject::parseOption(xmlNodePtr parent)
{
  UtString optionName;
  XmlParsing::getProp(parent, "Name", &optionName);

  const CarbonProperty* prop = mOptions->findProperty(optionName.c_str());
  if (prop)
    prop->readValueXML(parent, mOptions, NULL);
}

void CarbonProject::parseProjectOptions(xmlNodePtr parent)
{
  for (xmlNode *child = parent->children; child != NULL; child = child->next) 
  {
    const char* element = XmlParsing::elementName(child);
    if (element == NULL) // Skip non-interesting XML things
      continue;

    if (0 == strcmp(element, "Option"))
      parseOption(child);
    else
    {
      UtString err;
      err << "Unknown element: " << element;
      INFO_ASSERT(false, err.c_str());
    }
  }
}


bool CarbonProject::deserializePreferences(xmlNodePtr parent, UtXmlErrorHandler*)
{
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "DefaultConfiguration"))
    {
      UtString name;
      XmlParsing::getProp(child, "Name", &name);
      mDefaultConfig.clear();
      mDefaultConfig << name;
    }
  }

  return true;
}

bool CarbonProject::serializePreferences(xmlTextWriterPtr writer)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Preferences");

  xmlTextWriterStartElement(writer, BAD_CAST "DefaultConfiguration");
  xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST getActiveConfiguration());
  xmlTextWriterEndElement(writer);

  xmlTextWriterEndElement(writer);

  return true;
}


bool CarbonProject::readFile(const char* projFile)
{
  xmlDocPtr doc = xmlReadFile(projFile, NULL, 0);
  UtXmlErrorHandler eh;
  bool containsSpirit = false;

  xmlNode *parent = xmlDocGetRootElement(doc);
  if (parent == NULL || xmlStrcmp(parent->name, BAD_CAST "CarbonProjectFile") != 0)
    return false;

  UtString projType;
  XmlParsing::getProp(parent, "type", &projType);
  if (projType == "DatabaseOnly")
    mProjectType = DatabaseOnly;
  else if (projType == "ModelKit")
    mProjectType = ModelKitProject;

  // All projects will have a persistent guid
  // which will remain constant, if we read an old project file
  // assign it a guid on the fly.
  UtString projGUID;
  XmlParsing::getProp(parent, "guid", &projGUID);
  if (projGUID.length() == 0)
    mGUID = createGUID();
  else
    mGUID = projGUID.c_str();

  if (theApp->variablesScope())
    theApp->removeAllEnvironmentVariables();

  // We need the environment variables first to resolve values
  for (xmlNode *child = parent->children; child != NULL; child = child->next) 
  {
    const char* element = XmlParsing::elementName(child);
    if (element == NULL) // Skip non-interesting XML things
      continue;
    
    if (0 == strcmp(element, "EnvironmentVariables"))
    {
      for (xmlNode* varChild = child->children; varChild != NULL; varChild = varChild->next)
      {
        const char* varElement = XmlParsing::elementName(varChild);
        if (varElement == NULL)
          continue;

        if (0 == strcmp(varElement, "Variable"))
        {
          UtString envVar;
          XmlParsing::getContent(varChild, &envVar);
          theApp->addEnvVar(envVar.c_str());
        }
      }
      break; // Finished with Env Vars now
    }
  }

  // Walk the file in order
  for (xmlNode *child = parent->children; child != NULL; child = child->next) 
  {
    xmlNsPtr ns = child->ns;
    const char* element = XmlParsing::elementName(child);
    if (element == NULL) // Skip non-interesting XML things
      continue;

    else if (0 == strcmp(element, "Tool"))
      parseProjectOptions(child);

    else if (0 == strcmp(element, "Sources"))
      mSourceGroups->deserialize(child, &eh);

    else if (0 == strcmp(element, "Configurations"))
    {
      mConfigs->deserialize(child, &eh);
      if (mDirectives)
        delete mDirectives;
      mDirectives = new CarbonDirectives(this);
    }

    else if (0 == strcmp(element, "Files"))
      mFiles->deserialize(child, &eh);

    else if (0 == strcmp(element, "Components"))
      mComponents->deserialize(child, &eh);

    else if (0 == strcmp(element, "CompilerDirectives"))
      mDirectives->deserialize(this, child, &eh);

    else if (0 == strcmp(element, "Platforms"))
    {
      qDebug() << "Platforms";
    }

    else if (0 == strcmp(element, "Preferences"))
      deserializePreferences(child, &eh);

    else if (ns && 0 == strcmp((const char*)ns->prefix, "spirit") && 0 == strcmp(element, "component"))
    {
      containsSpirit = true;
      qDebug() << "project contains sxml";
    }
    else if (0 == strcmp(element, "EnvironmentVariables"))
    {
    }
    else
    {
      UtString err;
      err << "ignoring element: " << element;
      qDebug() << err.c_str();
    }
  }

  if (mDefaultConfig.length() == 0)
    mActiveConfiguration = mConfigs->getConfiguration(0);
  else
    mActiveConfiguration = mConfigs->findConfiguration(mDefaultConfig.c_str());

  // Now, fix up the parents of the source groups;
  for (int i=0; i<mSourceGroups->numGroups(); i++)
  {
    CarbonSourceGroup* group = mSourceGroups->getGroup(i);
    CarbonConfigurations* configs = group->getConfigurations();
    for (UInt32 j=0; j<configs->numConfigurations(); j++)
    {
      CarbonConfiguration* config = configs->getConfiguration(j);
      CarbonOptions* options = config->getOptions("RTL Properties");
      CarbonOptions* toolOptions = getToolOptions(config->getName(), "VSPCompiler");
      options->setParentOptions(toolOptions);
    }
  } 

  xmlFreeDoc(doc);
  xmlCleanupParser();

  mSpiritXML = new SpiritXML();
  // Now, re-read for SpiritXML
  if (containsSpirit)
    mSpiritXML->parseSpirtXML(projFile, true);

  return true;
}

void CarbonProject::writePlatforms(xmlTextWriterPtr writer)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Platforms");
  for (UtStringArray::iterator pi = mPlatforms.begin(); pi != mPlatforms.end(); ++pi)
  {
    UtString platform = *pi;
    xmlTextWriterStartElement(writer, BAD_CAST "Platform");
    xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST platform.c_str());
    xmlTextWriterEndElement(writer);
  }
  xmlTextWriterEndElement(writer);
}

bool CarbonProject::save()
{
  qDebug() << "Saving file";
 
  // clear the error saver
  setWriteErrorsOccurred(false, NULL);

  QApplication::setOverrideCursor(Qt::WaitCursor);

  xmlDocPtr doc=NULL;
  xmlTextWriterPtr writer = xmlNewTextWriterDoc(&doc, 0);
  xmlTextWriterSetIndent(writer, 1);

  UtString guid;
  guid << getGUID();

  xmlTextWriterStartElement(writer, BAD_CAST "CarbonProjectFile");
  xmlTextWriterWriteAttribute(writer, BAD_CAST "version", BAD_CAST XML_SCHEMA_VERSION);
  xmlTextWriterWriteAttribute(writer, BAD_CAST "guid", BAD_CAST guid.c_str());

  switch (mProjectType)
  {
  default:
  case HDL:
    xmlTextWriterWriteAttribute(writer, BAD_CAST "type", BAD_CAST "HDL");
    break;
  case ModelKitProject:
    xmlTextWriterWriteAttribute(writer, BAD_CAST "type", BAD_CAST "ModelKit");
    break;
  case DatabaseOnly:
    xmlTextWriterWriteAttribute(writer, BAD_CAST "type", BAD_CAST "DatabaseOnly");
    break;
  }

  xmlTextWriterStartElement(writer, BAD_CAST "EnvironmentVariables");
  // write env. vars

  qDebug() << "Writing" << theApp->numEnvVariables() << "variables";
  if (theApp->numEnvVariables() > 1)
    qDebug() << "Found some";

  for (quint32 i=0; i<theApp->numEnvVariables(); i++)
  {
    QString var = theApp->getEnvVariable(i);
    xmlTextWriterStartElement(writer, BAD_CAST "Variable");
    xmlTextWriterWriteString(writer, BAD_CAST qPrintable(var));
    xmlTextWriterEndElement(writer);
  }
  xmlTextWriterEndElement(writer);

  serializePreferences(writer);

  mSourceGroups->serialize(writer);

  mFiles->serialize(writer);

  writePlatforms(writer);

  mConfigs->serialize(writer);

  mOptions->serialize(writer);

  mComponents->serialize(writer);

  mDirectives->serialize(writer);

  if (mSpiritXML)
  {
    XmlErrorHandler eh;
    mSpiritXML->serialize(writer, &eh);
  }

  xmlTextWriterEndElement(writer);
  xmlTextWriterEndDocument(writer);
  xmlFreeTextWriter(writer);

  xmlSetStructuredErrorFunc(this, sXmlStructuredErrorHandler);
  
  int saveCode = xmlSaveFileEnc(mProjectFile.c_str(), doc, XML_ENCODING); 

  qDebug() << saveCode;

  xmlFreeDoc(doc);

  CarbonConfigurations* configs = getConfigurations();

  writeToplevelMakefile();

  for (UInt32 i=0; i<configs->numConfigurations(); i++)
  {
    CarbonConfiguration* cfg = configs->getConfiguration(i);
    writeMakefile(cfg);
  }

  if (!mWriteErrorsOccurred)
    mProjectWidget->putModified(false);
  else
  {
    QString title = QString("Error Saving File: %1").arg(mProjectFile.c_str());
    QMessageBox::critical(NULL, title, mWriteErrors.join("\n"));
  }
 
  QApplication::restoreOverrideCursor();

  xmlSetStructuredErrorFunc(this, NULL);

  return !mWriteErrorsOccurred;
}

///////////////////////////////////////////////////////////////////////////////
void CarbonProject::sXmlStructuredErrorHandler(void *ptr, xmlErrorPtr error)
{ 
  CarbonProject* proj = (CarbonProject*)ptr;
  const char *fileName = error->file;
  const char *msg = error->message;
  if (fileName == NULL)
    fileName = proj->projectFile();
 
  UtString message;
  message << ((fileName == NULL) ? "<unknown>" : fileName);
  message << ": " << msg;

  proj->setWriteErrorsOccurred(true, message.c_str());
}

void CarbonProject::setWriteErrorsOccurred(bool value, const char* msg)
{
  mWriteErrorsOccurred = value;

  if (value)
  {
    QString errorMessage = msg;
    if (!mWriteErrors.contains(msg))
      mWriteErrors.append(msg);
  }
  else
    mWriteErrors.clear();   
}



void CarbonProject::writeTopMakefileEntry(QTextStream& stream, const char* start, const char* finished, const char* target)
{
  CarbonConfigurations* configs = getConfigurations();
  // Write out default "clean" target
  stream << target << ":" << endl;
  stream << "\t" << "@echo " << start << endl;
  for (UInt32 i=0; i<configs->numConfigurations(); i++)
  {
    CarbonConfiguration* cfg = configs->getConfiguration(i);
    stream << "ifeq ($(" << cfg->getName() << "),1)" << endl;
    stream << "\t" << "$(MAKE) -C " << cfg->getName() << " CARBON_VERBOSE=0 $(NMOVERRIDE) -f Makefile.carbon " << target << endl;
    stream << "endif" << endl;
  }
  stream << "\t" << "@echo " << finished << endl;
}

// 
// Write out the Batch Makefile
//
void CarbonProject::writeToplevelMakefile()
{
  QString makefileBuffer;
  QTextStream stream(&makefileBuffer);
  stream.setCodec("UTF-8");

  CarbonConfigurations* configs = getConfigurations();

  UtString makefileName;
  OSConstructFilePath(&makefileName, getProjectDirectory(), getActive()->getPlatform() );
  OSConstructFilePath(&makefileName, makefileName.c_str(), "Makefile.carbon");

  stream << "# Generated Makefile by Carbon Model Studio *DO NOT EDIT, CHANGES WILL BE LOST *" << endl;
  stream << "# Carbon Model Studio Version: " << CARBON_RELEASE_ID << endl << endl;

  // Write out Makefile variables for each Configuration, default to 1
  for (UInt32 i=0; i<configs->numConfigurations(); i++)
  {
    CarbonConfiguration* cfg = configs->getConfiguration(i);
    stream << cfg->getName() << "=1" << endl;
  }

  // This crap is needed because gmake sets MAKEFLAGS which causes nmake to
  // see an invalid switch "/w" because MAKEFLAGS is set during make's execution
  // which gets passed down to nmake which barfs.
  stream << "ifeq ($(OS),Windows_NT)" << "\n";
  stream << "  NMOVERRIDE=MAKEFLAGS=" << "\n";
  stream << "endif" << "\n";

  stream << endl;

  writeTopMakefileEntry(stream, "CARBON BEGIN COMPILATION", "CARBON FINISHED COMPILATION", "build");
  writeTopMakefileEntry(stream, "CARBON BEGIN CLEAN", "CARBON FINISHED CLEAN", "clean");
  writeTopMakefileEntry(stream, "CARBON BEGIN COMPILATION", "CARBON FINISHED COMPILATION", "rebuild");

  writeFileIfChanged(makefileName.c_str(), &stream);
}

void CarbonProject::renameConfiguration(const char* oldName, const char* newName)
{
  UtString oldDir;
  OSConstructFilePath(&oldDir, getProjectDirectory(), getActive()->getPlatform());
  OSConstructFilePath(&oldDir, oldDir.c_str(), oldName);

  UtString newDir;
  OSConstructFilePath(&newDir, getProjectDirectory(), getActive()->getPlatform());
  OSConstructFilePath(&newDir, newDir.c_str(), newName);

  QDir dir;
  dir.rename(oldDir.c_str(), newDir.c_str());

  CarbonConfiguration* cfg = getConfigurations()->findConfiguration(oldName);
  if (cfg)
    cfg->putName(newName);

  emit configurationRenamed(oldName, newName);
}


void CarbonProject::removeConfiguration(const char* name)
{
  getConfigurations()->removeConfiguration(name);
  emit configurationRemoved(name);
}

void CarbonProject::copyConfiguration(const char* oldName, const char* newName)
{
  qDebug() << "Copy Configuration " << oldName << " to " << newName;

  CarbonConfiguration* newConfig = getConfigurations()->findConfiguration(newName);
  if (newConfig == NULL)
    addConfiguration(newName);

  newConfig = getConfigurations()->findConfiguration(newName);
  INFO_ASSERT(newConfig != NULL, "Expecting new configuration");

  copyCompilerSettings(oldName, newName);

  emit configurationCopy(oldName, newName);

  mProjectWidget->reopenWizards();

  qDebug() << "Finished copying configuration";
}

// Copy all settings (non-defaulted) from old to new
void CarbonProject::copyCompilerSettings(const char* oldCfgName, const char* newCfgName)
{
  CarbonConfiguration* oldConfig = getConfigurations()->findConfiguration(oldCfgName);
  CarbonConfiguration* newConfig = getConfigurations()->findConfiguration(newCfgName);

  INFO_ASSERT(oldConfig, "Expecting old config");
  INFO_ASSERT(newConfig, "Expecting new config");

  CarbonOptions* oldOptions = oldConfig->getOptions("VSPCompiler");
  CarbonOptions* newOptions = newConfig->getOptions("VSPCompiler");

  oldOptions->setBlockSignals(true);
  newOptions->setBlockSignals(true);

  for (CarbonOptions::OptionsLoop p = oldOptions->loopOptions(); !p.atEnd(); ++p)
  {
    CarbonPropertyValue* sv = p.getValue();
    const char* value = sv->getValue();
    const char* propName = sv->getProperty()->getName();
    newOptions->putValue(propName, sv->getProperty()->getDefaultValue());
    newOptions->putValue(propName, value);
  }

  oldOptions->setBlockSignals(false);
  newOptions->setBlockSignals(false);
}

void CarbonProject::addConfiguration(const char* name)
{
  UtString newDir;
  OSConstructFilePath(&newDir, getProjectDirectory(), getActive()->getPlatform());
  OSConstructFilePath(&newDir, newDir.c_str(), name);
  QDir dir;
  dir.mkpath(newDir.c_str());

  CarbonConfiguration* newConfig = getConfigurations()->findConfiguration(name);
  if (newConfig == NULL) // Only if it is new
  {
    getConfigurations()->addConfig(getActive()->getPlatform(), name);
    setDefaultLibraryName(name);
    emit configurationAdded(name);
  }
}

void CarbonProject::putActiveConfiguration(const char* configName)
{
  const char* currName = getActiveConfiguration();

  mActiveConfiguration = mConfigs->findConfiguration(configName);

  setDefaultLibraryName();

  INFO_ASSERT(mActiveConfiguration, "No such configuration");

  if (mFirstConfiguration || (0 != strcmp(currName, configName)))
    emit configurationChanged(configName);

  mFirstConfiguration = false;
}

const char* CarbonProject::getActivePlatform()
{
  return "Linux";
}

// Return platform name, including Windows if we were built on Windows.
const char* CarbonProject::getActivePlatformReal()
{
#if pfWINDOWS
  return "Windows";
#elif pfLINUX || pfLINUX64
  return "Linux";
#endif
}

const char* CarbonProject::getActiveConfiguration() const
{
  if (mActiveConfiguration)
    return mActiveConfiguration->getName();
  else
    return "Default";
}


// Static return value, use caution
const char* CarbonProject::formatSwitch(CarbonPropertyValue* propValue, bool checkChanged)
{
  const CarbonProperty* prop = propValue->getProperty();
  const char* result = "";

  switch (prop->getKind())
  {
  case CarbonProperty::Bool:
    result = formatBoolSwitch(propValue, checkChanged); 
    break;
  case CarbonProperty::Stringlist: 
    result = formatStringlistSwitch(propValue, checkChanged);
    break;
  case CarbonProperty::Enum:
    result = formatEnumSwitch(propValue, checkChanged);
    break;
  case CarbonProperty::InFile:
  case CarbonProperty::OutFile:
    result = formatFileSwitch(propValue, checkChanged); 
    break;
  case CarbonProperty::Integer:
    return formatIntegerSwitch(propValue, checkChanged);
    break;
  case CarbonProperty::Filelist:
    result = formatFilelistSwitch(propValue, checkChanged);
    break;
  case CarbonProperty::String:
  default:
    result = formatStringSwitch(propValue, checkChanged);
    break;
  }

  //if (result && strlen(result) > 0)
  //  qDebug() << "switch" << result;

  return result;
}

const char* CarbonProject::formatStringSwitch(CarbonPropertyValue* propValue, bool checkChanged)
{
  static UtString line;
  line.clear();

  bool plusSwitch = false;

  QRegExp plussw("\\+(.*)\\+(.*)");
  if (plussw.exactMatch(propValue->getProperty()->getName()))
    plusSwitch = true;

  const char* parentValue = propValue->getOptions()->getParentValue(propValue->getProperty()->getName());
  if (!checkChanged || (parentValue && 0 != strcmp(parentValue, propValue->getValue())))
  {
    if (plusSwitch)
      line << propValue->getProperty()->getName() << propValue->getValue();
    else
      line << propValue->getProperty()->getName() << " " << propValue->getValue();
  }

  return line.c_str();
}

const char* CarbonProject::formatStringlistSwitch(CarbonPropertyValue* propValue, bool)
{
  static UtString line;
  line.clear();

  const CarbonPropertyStringlist* propString = dynamic_cast<const CarbonPropertyStringlist*>(propValue->getProperty());

  const QStringList valueList = propValue->getValues();

  for (int i = 0; i< valueList.size(); ++i)
  {
    UtString value;
    value << valueList.at(i);

    if (line.length() > 0)
      line << "\n"; 

    switch (propString->getKind())
    {
    default:
    case CarbonPropertyStringlist::OptionNameValue:
      line << propValue->getProperty()->getName() << " " << value.c_str();
      break;
    case CarbonPropertyStringlist::OptionNameValueCompressed:
      line << propValue->getProperty()->getName() << value.c_str();
      break;
    case CarbonPropertyStringlist::ValuesOnly:
      line << value.c_str();
      break;
    case CarbonPropertyStringlist::ValuesOnlyOnePerLine:
      line << value.c_str();
      break;
    }
  }

  return line.c_str();
}


const char* CarbonProject::formatFilelistSwitch(CarbonPropertyValue* propValue, bool)
{
  static UtString line;
  line.clear();

  const CarbonPropertyFilelist* propString = dynamic_cast<const CarbonPropertyFilelist*>(propValue->getProperty());

  QStringList valueList = propValue->getValues();

  for (int i = 0; i< valueList.size(); ++i)
  {
    UtString tmpValue;
    tmpValue << valueList.at(i);

    QFileInfo fi(tmpValue.c_str());

    UtString tmpFile;
    QString name = tmpValue.c_str();

    if (!name.contains(ENVVAR_REFERENCE) && fi.isRelative())
    {
      OSConstructFilePath(&tmpFile, getProjectDirectory(), tmpValue.c_str());
    }
    else
      tmpFile << fixVarReferences(tmpValue.c_str());

    if (line.length() > 0)
      line << "\n"; 

    UtString value;
    value << getUnixEquivalentPath(tmpFile.c_str());

    switch (propString->getRepeatKind())
    {
    default:
    case CarbonPropertyFilelist::OptionNameValue:
      line << propValue->getProperty()->getName() << " " << value.c_str();
      break;
    case CarbonPropertyFilelist::OptionNameValueCompressed:
      line << propValue->getProperty()->getName() << value.c_str();
      break;
    case CarbonPropertyFilelist::ValuesOnly:
      line << value.c_str();
      break;
    case CarbonPropertyFilelist::ValuesOnlyOnePerLine:
      line << value.c_str();
      break;
    }
  }

  return line.c_str();
}

const char* CarbonProject::formatFileSwitch(CarbonPropertyValue* propValue, bool)
{
  static UtString line;
  line.clear();

  CarbonConfiguration* activeConfig = getActive();

  UtString fixedPath;
  QFileInfo sfi(propValue->getValue());

  QString valuePath = propValue->getValue();

  if (!valuePath.contains(ENVVAR_REFERENCE) && sfi.isRelative())
    fixedPath << CarbonProjectWidget::makeRelativePath(activeConfig->getOutputDirectory(), propValue->getValue());
  else
    fixedPath << fixVarReferences(valuePath);

  UtString absPath;
  absPath << getUnixEquivalentPath(fixedPath.c_str());

  line << propValue->getProperty()->getName() << " " << absPath.c_str();

  return line.c_str();
}
const char* CarbonProject::formatEnumSwitch(CarbonPropertyValue* propValue, bool checkChanged)
{
  return formatIntegerSwitch(propValue, checkChanged);
}
const char* CarbonProject::formatIntegerSwitch(CarbonPropertyValue* propValue, bool checkChanged)
{
  static UtString line;
  line.clear();

  const char* parentValue = propValue->getOptions()->getParentValue(propValue->getProperty()->getName());
  if (!checkChanged || (parentValue && 0 != strcmp(parentValue, propValue->getValue())))
    line << propValue->getProperty()->getName() << " " << propValue->getValue();

  return line.c_str();
}

// Static return value, use caution
const char* CarbonProject::formatBoolSwitch(CarbonPropertyValue* propValue, bool checkChanged)
{
  static UtString line;
  line.clear();
  const char* value = propValue->getValue();

  const CarbonProperty* prop = propValue->getProperty();

  const char* parentValue = propValue->getOptions()->getParentValue(prop->getName());
  bool outputTheValue = false;
  bool valuesEqual = false;
  if (parentValue && 0 == strcmp(value, parentValue))
    valuesEqual = true;

  if (!checkChanged || ((parentValue && !valuesEqual) || (prop->hasGuiDefaultValue() && !propValue->isDefaultValue(value))) )
    outputTheValue = true;
  
  if (outputTheValue)
  {
    // True
    if (0 == strcmp(value, "true"))
    {       
      if (prop->hasTrueValue())
      {
        if (!prop->getTrueValue().isEmpty())
          line << prop->getTrueValue();
      }
      else
        line << propValue->getProperty()->getName();     
    }
    else // False
    {
      if (prop->hasFalseValue())
      {
        if (!prop->getFalseValue().isEmpty())      
          line << prop->getFalseValue();
      }
      else // add a -no infront of the name
      {
        QString switchValue;
        QString switchName = propValue->getProperty()->getName();
        if (switchName.startsWith('-'))
        {
          switchName = switchName.mid(1);
          QString firstLetter = switchName.left(1);
          switchValue = QString("-no%1%2")
            .arg(firstLetter.toUpper())
            .arg(switchName.mid(1));
        }
        else
          switchValue = switchName;

        line << switchValue;
      }
    }
  }
  
  return line.c_str();
}

// Create the Directives from the CarbonDirectives editor
void CarbonProject::generateDirectivesFile(CarbonConfiguration* activeConfig, const char* dirFile)
{
  QString buffer;
  QTextStream stream(&buffer);

  //CarbonDirectiveGroup* group = mDirectives->getActiveGroup();
  CarbonDirectiveGroup* group = mDirectives->findDirectiveGroup(activeConfig->getName());

  if (group)
  {
    // Write the Net Directives
    foreach (NetDirectives* netDir, *group->getNetDirectives())
    {
      // 
      foreach (Directive* dir, netDir->mDirectives)
      {
        if (dir->mDirective == "tieNet")
          stream << dir->mDirective.c_str() << " " << dir->mValue.c_str() << " " << netDir->mLocator.c_str() << endl;
        else
          stream << dir->mDirective.c_str() << " " << netDir->mLocator.c_str() << endl;
      }
    }

    // Write the Module Directives
    foreach (ModuleDirectives* modDir, *group->getModuleDirectives())
    {
      // 
      foreach (Directive* dir, modDir->mDirectives)
      {
        if (dir->mDirective == "substituteModule" || dir->mDirective == "substituteEntity")
        {
          QString value = dir->mValue.c_str();
          QStringList tokens = value.split(':');
          stream << dir->mDirective.c_str();
          for (int i=0; i<tokens.count(); i++)
          {
            stream << " " << tokens[i];
          }
          stream << endl;
        }

        else
          stream << dir->mDirective.c_str() << " " << modDir->mLocator.c_str() << endl;
      }
    }

  }

  // write directives file if changed or doesnt exist
  writeFileIfChanged(dirFile, &stream);

}

bool CarbonProject::check(CarbonProjectWidget* pw)
{
  // Clear all errors and warnings before check.
  QDockWidget* dw = pw->context()->getWorkspaceModelMaker()->getErrorInfoDockWindow();
  ErrorInfoWidget* eiw = static_cast<ErrorInfoWidget*>(dw->widget());
  eiw->getInfoWidget()->clearAll();
  bool checkFailure = false;

  checkFailure = checkProject();

  if (checkFailure)
  {
    QList<CarbonComponent*> comps;
    mComponents->getActiveComponents(&comps);
    foreach (CarbonComponent* comp, comps)
    {
      if (comp->checkComponent(pw->getConsole()))
        checkFailure = true;
    }
  }


  QWorkspace* ws = pw->context()->getWorkspace();
  foreach (QWidget* window, ws->windowList()) 
  {
    MDIWidget* mdiWidget = dynamic_cast<MDIWidget*>(window);
    if (mdiWidget)
    {
      MaxsimWizardWidget* mw = dynamic_cast<MaxsimWizardWidget*>(mdiWidget);
      SystemCWizardWidget* sw = dynamic_cast<SystemCWizardWidget*>(mdiWidget);
      CowareWizardWidget* cw = dynamic_cast<CowareWizardWidget*>(mdiWidget);
      
      if (mw && mw->check(pw->getConsole()))
        checkFailure = true;       

      if (sw)
        qDebug() << "SystemC Wizard";

      if (cw)
        qDebug() << "Coware Wizard";
    
    }
  }


  return checkFailure;
}

QString CarbonProject::postCompilationScript()
{
  CarbonOptions* projOptions = getProjectOptions();
  const char* msScript = projOptions->getValue("Post Compilation ModelStudio Script")->getValue();
  return msScript;
}

bool CarbonProject::hasPostCompilationScript()
{
  CarbonOptions* projOptions = getProjectOptions();
  const char* msScript = projOptions->getValue("Post Compilation ModelStudio Script")->getValue();
  return (msScript && strlen(msScript) > 0);
}


void CarbonProject::makefileFinished(CarbonConsole::CommandMode mode, CarbonConsole::CommandType type)
{
  qDebug() << "emitting makefileCompleted" << mode << type;
  emit makefileCompleted(mode,type);
  if (mCarbonModelWasCompiled && mode == CarbonConsole::Local )
    mProjectWidget->reopenWizards();
}

void CarbonProject::modelCompilationFinished(CarbonConsole::CommandMode mode, CarbonConsole::CommandType type)
{
  qDebug() << "Carbon Model compilation done" << mode << type;
  mCarbonModelWasCompiled = true;
}

void CarbonProject::finishCompile()
{
  qDebug() << "starting compilation for components";
  compile(mProjectWidget, false, true, false, NULL, NULL, NULL, NULL, CompilationPhase2);
}

void CarbonProject::writeScanFiles(QTextStream& commandStream, const QString& scanDir, const QStringList& fileKinds, bool recurseFlag)
{
  TempChangeDirectory cd(scanDir);
  QDir dir(scanDir);
  if (dir.exists())
  {
    dir.setFilter(QDir::Files);
    dir.setNameFilters(fileKinds);

    foreach(QFileInfo fileInfo, dir.entryInfoList())
    {
      UtString filepath;
      filepath << fileInfo.absoluteFilePath();
      QString filePath = getUnixEquivalentPath(filepath.c_str());
      commandStream << filePath << "\n";
    }

    if (recurseFlag)
    {
      QDir subDir(scanDir);
      subDir.setFilter(QDir::Dirs);
      foreach (QFileInfo fileInfo, subDir.entryInfoList())
      {      
        if (fileInfo.fileName() != "." && fileInfo.fileName() != "..")
          writeScanFiles(commandStream, fileInfo.absoluteFilePath(), fileKinds, recurseFlag);
      }
    }
  }
}


void CarbonProject::writeRemodelMakefile(CarbonConfiguration* activeConfig, const QString& scanDir, const QStringList& filters, bool recurseFlag)
{
  UtString msg;
  msg << "Writing " << activeConfig->getName() << " Makefile.remodel";
  getProjectWidget()->context()->getMainWindow()->statusBar()->showMessage(msg.c_str(), 2000);  
  if (mProjectWidget->project())
  {
    // Change to the output directory for relative/absolute paths to work
    ProjectChangeOutputDirectory temp(getProjectDirectory());
//    CarbonConsole::CommandMode mode = CarbonConsole::Local;
//#if pfWINDOWS
//    mode = CarbonConsole::Remote;
//#endif
    UtString makefileName;
    OSConstructFilePath(&makefileName, activeConfig->getOutputDirectory(), "Makefile.remodel");
    QString makefileBuffer;
    QTextStream stream(&makefileBuffer);
    stream.setCodec("UTF-8");

    QString commandBuffer;
    QTextStream commandStream(&commandBuffer);
    commandStream.setCodec("UTF-8");

    UtString remodelCommandFile;
    OSConstructFilePath(&remodelCommandFile, activeConfig->getOutputDirectory(), "Remodel.f.cmd");

    writeCompilerSwitches(activeConfig, commandStream, eCbuildScan);

    commandStream << "-o libRemodelScanner.a" << "\n";
    commandStream << "-scan" << "\n";

    writeScanFiles(commandStream, scanDir, filters, recurseFlag);

    writeFileIfChanged(remodelCommandFile.c_str(), &commandStream);

    writeFileIfChanged(makefileName.c_str(), &stream);
  }

}

void CarbonProject::writeMakefile(CarbonConfiguration* activeConfig)
{
  UtString msg;
  msg << "Writing " << activeConfig->getName() << " Makefile.carbon";
  getProjectWidget()->context()->getMainWindow()->statusBar()->showMessage(msg.c_str(), 2000);  

  if (mProjectWidget->project())
  {
    // Change to the output directory for relative/absolute paths to work
    ProjectChangeOutputDirectory temp(getProjectDirectory());

    CarbonConsole::CommandMode mode = CarbonConsole::Local;
    if (mProjectWidget->isRemoteCompilation())
      mode = CarbonConsole::Remote;

    CarbonSourceGroups* sourceGroups = getGroups();

    UtString cmdFileName;
    cmdFileName << activeConfig->getName() << ".f.cmd";

    UtString cmdFile;
    OSConstructFilePath(&cmdFile, activeConfig->getOutputDirectory(), cmdFileName.c_str());

    UtString dirFileName;
    dirFileName << activeConfig->getName() << ".directives";

    UtString dirFile;
    OSConstructFilePath(&dirFile, activeConfig->getOutputDirectory(), dirFileName.c_str());

    UtString relOutputDir;
    relOutputDir << CarbonProjectWidget::makeRelativePath(getProjectDirectory(), activeConfig->getOutputDirectory());

    QString buffer;
    QTextStream stream(&buffer);

    // stream is the command file 
    UtString relFile;
    relFile << CarbonProjectWidget::makeRelativePath(activeConfig->getOutputDirectory(), dirFile.c_str());

    stream << "-directive " << relFile.c_str() << endl;

    // Write out the compiler switches
    writeCompilerSwitches(activeConfig, stream);

    // Change to the output directory for relative/absolute paths to work
    ProjectChangeOutputDirectory newDir1(getProjectDirectory());

    // Files, and file-specifig switches next
    for (int i=0; i<sourceGroups->numGroups(); ++i)
    {
      CarbonSourceGroup* group = sourceGroups->getGroup(i);
      writeFileSpecificSwitches(activeConfig, stream, group, group->isLibrary(), false);
    } 

    // write command file if changed
    writeFileIfChanged(cmdFile.c_str(), &stream);

    generateDirectivesFile(activeConfig, dirFile.c_str());

    generateMakeFile(activeConfig, mode, activeConfig->getOutputDirectory(), cmdFileName.c_str(), dirFileName.c_str());
  }
}


void CarbonProject::batch(CarbonProjectWidget*, const QStringList& configList, BatchType btype)
{
  CarbonConfiguration* activeConfig = getActive();

  // Remote/Local
  CarbonConsole::CommandMode mode = CarbonConsole::Local;
  if (mProjectWidget->isRemoteCompilation())
    mode = CarbonConsole::Remote;

  UtString projectDir;
  if (mode == CarbonConsole::Remote)
    projectDir << getRemoteWorkingDirectory();
  else
    projectDir << getProjectDirectory();

  // The arguments passed to make
  QStringList args;

  UtString configOptions;
  CarbonConfigurations* configs = getConfigurations();
  for (UInt32 i=0; i<configs->numConfigurations(); i++)
  {
    UtString option;
    CarbonConfiguration* config = configs->getConfiguration(i);
    if (configList.contains(config->getName()))
      option << config->getName() << "=1";
    else
      option << config->getName() << "=0";
    args << option.c_str();
  }

  UtString makefileDir;
  OSConstructFilePath(&makefileDir, projectDir.c_str(), activeConfig->getPlatform());

  args << "-C" << makefileDir.c_str() << "-f" << "Makefile.carbon";

  qDebug() << args;

  CarbonConsole::CommandType remoteType = CarbonConsole::Compilation;
  if (btype == BatchClean)
    remoteType = CarbonConsole::Clean;

  switch (btype)
  {
  default:
  case BatchBuild:
    args << "build";
    break;
  case BatchClean:
    args << "clean";
    break;
  case BatchRebuild:
    args << "rebuild";
    break;
  }

  executeMakeCommand(activeConfig, remoteType, NULL, &args, btype);
}

void CarbonProject::exploreFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  // nothing done here for now.

  showWindows(true);
}

int CarbonProject::executeMakeCommand(CarbonConfiguration* activeConfig, CarbonConsole::CommandType rcmdType, const char* targetName, QStringList* makefileArgs, BatchType btype, bool waitFlag)
{
  CarbonProjectWidget* pw = getProjectWidget();

  qDebug() << "execute make command" << waitFlag;

  const char* modelName = activeConfig->getOptions("VSPCompiler")->getValue("-o")->getValue();
  if (modelName == NULL || (modelName && strlen(modelName) == 0))
  {
    QMessageBox::warning(pw, MODELSTUDIO_TITLE, "You cannot compile the design without specifying an output name see the '-o'option");
    return -1;
  }

  CarbonMakerContext* ctx = pw->context();
  if (ctx)
    ctx->getDocumentManager()->saveAllDocuments(ctx->getWorkspace());

  // Save project, writes out makefiles
  pw->saveProject();

  // Execute the explore target
  CarbonConsole::CommandMode mode = CarbonConsole::Local;
  if (pw->isRemoteCompilation())
    mode = CarbonConsole::Remote;

  // clear out the console before compiling
  pw->getConsole()->clear();

  UtString command;
  QStringList args;

  if (mode == CarbonConsole::Remote)
    args << "CARBON_REMOTE=1";

  // Insert Makefile Args
  if (makefileArgs)
  {
    foreach (QString marg, *makefileArgs)
      args << marg;
  }

  // Was there a target passed in?
  if (targetName)
    args << targetName;

  // Change to the output directory for relative/absolute paths to work
  ProjectChangeOutputDirectory newDir(getProjectDirectory());

  connectConsole();

  if (mode == CarbonConsole::Remote)
    command << remoteCommandName("make");
  else
  {
    UtString err;
#ifdef Q_OS_WIN
    OSExpandFilename(&command,"$CARBON_HOME/Win/bin/make.exe",&err);
#else
    OSExpandFilename(&command,"$CARBON_HOME/bin/make",&err);
#endif
  }

  UtString relOutputDir;
  relOutputDir << CarbonProjectWidget::makeRelativePath(getProjectDirectory(), activeConfig->getOutputDirectory());

  UtString chdir;
#if pfWINDOWS
  if (mode == CarbonConsole::Remote)
    chdir << relOutputDir.c_str();
  else
    chdir << activeConfig->getOutputDirectory();
#else
  chdir << activeConfig->getOutputDirectory();
#endif

  if (mArmKitRoot.length() > 0)
  {
    static UtString envArm;
    envArm << "ARM_KIT_ROOT=" << getUnixEquivalentPath(mArmKitRoot);
    args << envArm.c_str();
  }

  qDebug() << "arguments: " << args;
  int exitCode = 0;
  if (waitFlag)
  {
    pw->getConsole()->executeCommandAndWait(mode, rcmdType, command.c_str(), args,  chdir.c_str(), CarbonConsole::Program, btype != NotBatch);
  }
  else
    pw->getConsole()->executeCommand(mode, rcmdType, command.c_str(), args,  chdir.c_str(), CarbonConsole::Program, btype != NotBatch);
  
  return exitCode;
}

void CarbonProject::generatePrecompiledHierarchy()
{
  QStringList args;

  args << "-f" << "Makefile.carbon";

  executeMakeCommand(getActive(), CarbonConsole::Compilation, "explore", &args);
}
bool CarbonProject::windowsAuthenticate()
{
#if pfWINDOWS
  CarbonOptions* options = getProjectOptions();
  bool forceShowDialog = false;

  // HDL projects Must be compiled remotely on windows
  if (mProjectWidget->project()->getProjectType() == CarbonProject::HDL)
  {
    QString remoteServer = options->getValue("Remote Server")->getValue();
    QString remoteUsername = options->getValue("Remote Server")->getValue();
    QString remoteWorkingDir = mProjectWidget->project()->getRemoteWorkingDirectory();

    // If these are blank we must have them
    if (remoteServer.length() == 0 || remoteUsername.length() == 0 || remoteWorkingDir == 0)
      forceShowDialog = true;
  }

  bool shellAlreadyConnected = mProjectWidget->getConsole()->isRemoteShellConnected();
#if pfWINDOWS
  if (getProjectType() != HDL)
  {
    forceShowDialog = false;
    shellAlreadyConnected = true;
  }
#endif
  DlgAuthenticate dlg(mProjectWidget);
  if (forceShowDialog || !shellAlreadyConnected)
  {
    bool validFlag;
    dlg.setOptions(options);
    if (dlg.exec() == QDialog::Accepted)
      validFlag = true;  
    else
      validFlag = false;
    return validFlag;
  }
#endif
  return true;
}


void CarbonProject::commandCompleted(CarbonConsole::CommandMode mode, CarbonConsole::CommandType ctype, int exitCode)
{
  qDebug() << "command completed" << mode << ctype << exitCode << mCompilationPhase << mModelCompilationStarted << mCarbonModelWasCompiled;

  // If we are not finishing a compile...
  if (ctype != CarbonConsole::Compilation && ctype != CarbonConsole::Command)
  {
#if pfWINDOWS
    if (mode == CarbonConsole::Remote)
    {
      qDebug() << "carbonProject initiating windows make";
      mProjectWidget->getConsole()->executeWindowsMake();
    }
#endif
    // We are finished, it's a single phase compile
    return;
  }

  // This is only handling Compilations, not cleans or commands
  if (ctype != CarbonConsole::Compilation)
    return;

  qDebug() << "compilation command completed with " << exitCode << mModelCompilationStarted;
  
  if (mModelCompilationStarted && mCarbonModelWasCompiled && mCompilationPhase == CompilationPhase1)
  {
    if (hasPostCompilationScript())
    {
      foreach (QString scriptFileName, postCompilationScript().split(';'))
      {
        qDebug() << "running modelstudio script" << scriptFileName;
        UtString expandedName;
        UtString err;
        UtString scriptName;
        scriptName << scriptFileName;

        if (OSExpandFilename(&expandedName, scriptName.c_str(), &err))
        {
          ScriptingEngine* engine = new ScriptingEngine();
          QString modelstudioScript = expandedName.c_str();
          QFileInfo fi(modelstudioScript);
          if (fi.exists())
          {
            qDebug() << "executing ms script" << modelstudioScript;
            engine->runScript(modelstudioScript);
            qDebug() << "finished ms script" << modelstudioScript;
          }
          else
          {
            QMessageBox::critical(NULL, MODELSTUDIO_TITLE, 
              QString("Unable to execute ModelStudio Script: %1").arg(modelstudioScript));
            break;
          }
        }
        else
        {
          QMessageBox::critical(NULL, MODELSTUDIO_TITLE, 
            QString("Error executing ModelStudio Script: %1\nError: %2").arg(scriptName.c_str()).arg(err.c_str()));
          break;
        }
      }
    }
   }
  else
  {
    qDebug() << "Carbon Model Was Not Recompiled";
  }

  if (mModelCompilationStarted && mCompilationPhase == CompilationPhase1 && hasPostCompilationScript())
  {
    qDebug() << "Need to compile the components now";
    mCompilationPhase = CompilationPhase2;
    QTimer* tempTimer = new QTimer(this);
    tempTimer->setSingleShot(true);
    tempTimer->setInterval(10);     // milliseconds
    connect(tempTimer, SIGNAL(timeout()), this, SLOT(finishCompile()));
    tempTimer->start();
  }
  else if (!hasPostCompilationScript() || (mCompilationPhase == CompilationPhase2))
  {
#if pfWINDOWS
    if (mode == CarbonConsole::Remote && ctype != CarbonConsole::Command)
    {
      mCompilationPhase = CompilationPhaseFinished;
      qDebug() << "carbonProject initiating windows make";
      mProjectWidget->getConsole()->executeWindowsMake();
    }
#endif
  }
}

void CarbonProject::showWindows(bool showOrHide)
{
  QDockWidget* dw1 = mProjectWidget->context()->getWorkspaceModelMaker()->getErrorInfoDockWindow();    
  // lower followed by raise causes it to "pop" forward
  if (showOrHide)
  {
    dw1->lower();
    dw1->raise();
  }
  dw1->setVisible(showOrHide);

  QDockWidget* dw = mProjectWidget->context()->getWorkspaceModelMaker()->getRemoteConsoleDockWindow();    
  // lower followed by raise causes it to "pop" forward
  if (showOrHide)
  {
    dw->lower();
    dw->raise();
  }
  dw->setVisible(showOrHide);

}

bool CarbonProject::encryptFile(const QString& inputFileName)
{
  UtString inputName;
  inputName << QString("%1/%2")
    .arg(getActive()->getOutputDirectory())
    .arg(inputFileName);

  UtString errMsg;
  UtEncryptDecrypt enc;

  return enc.protectEFFile(inputName.c_str(), &errMsg);
}

bool CarbonProject::encryptCommandFile()
{
  // Check the project for any problems here
  if (!checkProject())
    return false;
  
  CarbonProjectWidget* pw = getProjectWidget();
  CarbonMakerContext* ctx = getProjectWidget()->context();
  CarbonConfiguration* activeConfig = getActive();
  const char* modelName = activeConfig->getOptions("VSPCompiler")->getValue("-o")->getValue();
  if (modelName == NULL || (modelName && strlen(modelName) == 0))
  {
    QMessageBox::warning(pw, MODELSTUDIO_TITLE, "You cannot compile the design without specifying an output name see the '-o'option");
    return false;
  }

  if (ctx)
    ctx->getDocumentManager()->saveAllDocuments(ctx->getWorkspace());

  // Commit the project changes if any
  // this will also write out the makefile(s)
  getProjectWidget()->saveProject();
  
  // Change to the output directory for relative/absolute paths to work
  ProjectChangeOutputDirectory newDir(getProjectDirectory());

  bool createdFile = encryptFile("compile.tef");
 
  if (!createdFile)
    qDebug() << "Error: Unable to create file compile.tef";
  else
  {
    createdFile = encryptFile("explore.tef");
    if (!createdFile)
      qDebug() << "Error: Unable to create file explore.tef";
  }

  return createdFile;
}
void CarbonProject::compile(CarbonProjectWidget* pw, bool cleanFlag, bool compilation, bool package, const char* compileTarget, const char* cleanTarget, QStringList* makefileArgs, const char* armKitRoot, CompilationPhase phase)
{
  setEnvironmentVariables();

  qDebug() << "carbonProject::compile" << cleanFlag << compilation << compileTarget << cleanTarget << makefileArgs << armKitRoot << phase;

  mCompilationPhase = phase;
  mCarbonModelWasCompiled = false;
  if (phase != CompilationPhase1)
    mModelCompilationStarted = false;

  // Check the project for any problems here
  if (!checkProject())
    return;

  if (!windowsAuthenticate())
    return;

  CarbonMakerContext* ctx = getProjectWidget()->context();

  CarbonConfiguration* activeConfig = getActive();
  const char* modelName = activeConfig->getOptions("VSPCompiler")->getValue("-o")->getValue();
  if (modelName == NULL || (modelName && strlen(modelName) == 0))
  {
    QMessageBox::warning(pw, MODELSTUDIO_TITLE, "You cannot compile the design without specifying an output name see the '-o'option");
    return;
  }

  if (ctx)
    ctx->getDocumentManager()->saveAllDocuments(ctx->getWorkspace());

  // Commit the project changes if any
  // this will also write out the makefile(s)
  getProjectWidget()->saveProject();

  CarbonConsole::CommandMode mode = CarbonConsole::Local;
  if (mProjectWidget->isRemoteCompilation())
    mode = CarbonConsole::Remote;

  // Change to the output directory for relative/absolute paths to work
  ProjectChangeOutputDirectory newDir(getProjectDirectory());

  // We will compile the active configuration
  if (compilation)
    qDebug() << "Compiling " << getActiveConfiguration() << " : " << compileTarget;

  if (cleanFlag)
    qDebug() << "Cleaning " << getActiveConfiguration() << " : " << cleanTarget;

  if (package)
    qDebug() << "Packaging " << getActiveConfiguration();

  // build up the list of active components that need to be checked
  if (!cleanFlag && compilation)
  {
    if (check(pw))
    {
      showWindows(true);
      // If there were errors during the check, ask the user if he wants to continue
      QDockWidget* dw = mProjectWidget->context()->getWorkspaceModelMaker()->getErrorInfoDockWindow();    
      ErrorInfoWidget* eiw = static_cast<ErrorInfoWidget*>(dw->widget());
      int errors = eiw->getInfoWidget()->getErrors();
      if(errors > 0) {
        int ret = QMessageBox::warning(pw, MODELSTUDIO_TITLE,
                                       tr("Design checker errors detected\n\n"
                                          "Do you want to continue compilation?"),
                                       QMessageBox::Yes ,
                                       QMessageBox::No | QMessageBox::Default
          );
        if (ret == QMessageBox::No)
          return;
      }
    }
  }

  // clear out the console before compiling
  if (phase == CarbonProject::CompilationPhase1)
    pw->getConsole()->clear();

  // This disables the gui until the makefile completes
  pw->getConsole()->signalBeginCompilation();

  connectConsole();

  UtString relOutputDir;
  relOutputDir << CarbonProjectWidget::makeRelativePath(getProjectDirectory(), activeConfig->getOutputDirectory());

  UtString chdir;
#if pfWINDOWS
  if (mode == CarbonConsole::Remote)
    chdir << relOutputDir.c_str();
  else
    chdir << activeConfig->getOutputDirectory();
#else
  chdir << activeConfig->getOutputDirectory();
#endif

  CarbonConsole::CommandType rcmdType = CarbonConsole::Compilation;
  if (compilation && cleanFlag)
    rcmdType = CarbonConsole::ReBuild;
  else if (cleanFlag && !compilation)
    rcmdType = CarbonConsole::Clean;
  else if (package)
    rcmdType = CarbonConsole::Package;

  UtString command;
  QStringList args;

  if (mode == CarbonConsole::Remote)
    args << "CARBON_REMOTE=1";

  if (mArmKitRoot.length() > 0 || armKitRoot)
  {
    UtString envArm;
    if (armKitRoot)
    {
      envArm << "ARM_KIT_ROOT=" << getUnixEquivalentPath(armKitRoot);
      mArmKitRoot = armKitRoot;
    }
    else
      envArm << "ARM_KIT_ROOT=" << getUnixEquivalentPath(mArmKitRoot);

    args << envArm.c_str();
  }

  args << "-f" << "Makefile.carbon";

  // Insert Makefile Args
  if (makefileArgs)
  {
    foreach (QString marg, *makefileArgs)
      args << marg;
  }

  if (mode == CarbonConsole::Remote)
    command << remoteCommandName("make");
  else
  {
    UtString err;
#ifdef Q_OS_WIN
    command << "$CARBON_HOME/Win/bin/make.exe";
    //OSExpandFilename(&command,"$CARBON_HOME/Win/bin/make.exe",&err);
#else
    command << "$CARBON_HOME/bin/make";
    //OSExpandFilename(&command,"$CARBON_HOME/bin/make",&err);
#endif
  }

  // Write out the makefile now
  writeMakefile(activeConfig);

  if (cleanFlag && !compilation)
  {
    if (cleanTarget)
      args << "StartClean" << cleanTarget << "FinishedClean";
    else
      args << "clean";
  }
  else if (compilation && !cleanFlag)
  {
    bool hasScript = hasPostCompilationScript();
 
    if (compileTarget)
      args << "Start" << compileTarget << "Finished";
    else
    {
      if (phase == CarbonProject::CompilationPhase1)
        mModelCompilationStarted = true;

      if (hasScript && phase == CarbonProject::CompilationPhase1)
        args << "buildModel";
      else
        args << "build"; 
    }
  }
  else if (compilation && cleanFlag)
  {
    bool hasScript = hasPostCompilationScript();

    rcmdType = CarbonConsole::Compilation;
    if (phase == CarbonProject::CompilationPhase1)
      mModelCompilationStarted = true;

    if (cleanTarget)
      args << "StartClean" << cleanTarget << "FinishedClean";
    else
      args << "clean";

    if (compileTarget)
      args << "Start" << compileTarget << "Finished";
    else
    {
      if (hasScript && phase == CarbonProject::CompilationPhase1)
        args << "buildModel";
      else
        args << "build"; 
    }
  }

  else if (package)
    args << "package";

  if (cleanFlag || compilation || package)
    pw->getConsole()->executeCommand(mode, rcmdType, command.c_str(), args, chdir.c_str());
}

CarbonOptions* CarbonProject::getProjectOptions()
{
  mOptions->putValue("Project Directory", getProjectDirectory());
  if (mActiveConfiguration) 
    mOptions->putValue("Target Directory", mActiveConfiguration->getOutputDirectory());

  return mOptions;
}

// Write out the list of files for this group, preceded by any file-specific switches
// for each file.
void CarbonProject::writeFileSpecificSwitches(CarbonConfiguration* activeConfig, QTextStream& stream, CarbonSourceGroup* group, bool referencesOnly, bool blockVlogLib)
{
  if (!referencesOnly) // Skip this for just references, otherwise file-specific switches and filename
  {
    bool mixedLibrary = false;

    QStringList groupSwitches;
    // write out the library switches now.
    writeSourceGroupSwitches(activeConfig, stream, group, groupSwitches);

    for (int j=0; j<group->numSources(); j++)
    {
      QStringList fileSwitchList;
      QMap<QString,QString> fileSwitchValues;
      CarbonHDLSourceFile* srcFile = group->getSource(j);
      UtString srcPath;
      QFileInfo sfi(srcFile->getRelativePath());
      QString fileName = srcFile->getRelativePath();

      if (!fileName.contains(ENVVAR_REFERENCE) && sfi.isRelative())
        OSConstructFilePath(&srcPath, getProjectDirectory(), srcFile->getRelativePath());
      else
        srcPath << fixVarReferences(srcFile->getCanonicalPath());

      UtString unixPath;
      unixPath << getUnixEquivalentPath(srcPath.c_str());

      UtString relFile;
      relFile = unixPath;

      // Any file specific options?
      const char* configName = activeConfig->getName();
      CarbonHDLSourceConfiguration* fileConfig = srcFile->getConfiguration(configName);
      for (CarbonOptions::OptionsLoop p = fileConfig->getOptions()->loopOptions(); !p.atEnd(); ++p)
      {
        CarbonPropertyValue* sv = p.getValue();
        if (sv)
        {
          const CarbonProperty* prop = sv->getProperty();
          bool ignoreIt = false;
          for (int is=0; is<(int)(sizeof(ignoreOutputSwitches)/sizeof(const char*)); is++)
          {
            if (0 == strcmp(prop->getName(), ignoreOutputSwitches[is]))
            {
              ignoreIt = true;
              break;
            }
          }

          if (ignoreIt)
            continue;

          if (!prop->getReadOnly() && !prop->getDisplayOnly())
          {
            const char* swValue = formatSwitch(sv);
            if (swValue && strlen(swValue) > 0)
            {
              fileSwitchList << prop->getName();
              fileSwitchValues[prop->getName()] = swValue;
              stream << swValue << endl;  
            }
          }
        }
      }

      // library files are special
      if (srcFile->isLibraryFile())
        stream << "-v " << relFile.c_str() << endl;
      else 
      {
        HDLType hdlType = CarbonProjectWidget::hdlFileType(relFile.c_str());

        // hdl file which is going into a library of the same type
        if ((hdlType == Verilog && group->isVerilogLibrary()) || (hdlType == VHDL && group->isVhdlLibrary()) )
          stream << relFile.c_str() << endl;
        else
        {
          bool changedLib = false;
          // change the lib reference if we are compiling into a library
          if (hdlType == Verilog && group->isVhdlLibrary())
          {
            // Only do this once! 
            if (!mixedLibrary && !blockVlogLib)
            {
              stream << "-vlogLib " << group->getLibraryName() << endl;
              changedLib = true;
            }
          }
          else if (group->isVerilogLibrary())
          {
            stream << "-vhdlLib " << group->getLibraryName() << endl;
            changedLib = true;
          }
          
          // now put out the file itself
          stream << relFile.c_str() << endl;
          
          if (changedLib)
          {
            // put back the library reference if we were compiling into a library
            if (hdlType == Verilog && group->isVhdlLibrary())
              stream << "-vhdlLib " << group->getLibraryName() << endl;
            else if (group->isVerilogLibrary() && !blockVlogLib)
              stream << "-vlogLib " << group->getLibraryName() << endl;
             mixedLibrary = true;
          }
        }
      }
      
      // The file switches have been put out
      const CarbonOptions* parentFileOptions = fileConfig->getOptions()->getParent();
      foreach (QString switchName, fileSwitchList)
      {
        UtString sname;
        sname << switchName;
        CarbonPropertyValue* parentValue = parentFileOptions->getValue(sname.c_str());
        const char* swResetValue = formatSwitch(parentValue, false);
        qDebug() << "reset" << switchName << "from" << fileSwitchValues[switchName] << "to" << swResetValue;
        if (swResetValue && strlen(swResetValue) > 0)
          stream << swResetValue << endl;
      }
    }
  // put back the sourceGroup switches
    const CarbonOptions* parentGroupOptions = group->getOptions(activeConfig->getName())->getParent();
    foreach (QString switchName, groupSwitches)
    {
      UtString sname;
      sname << switchName;
      CarbonPropertyValue* parentValue = parentGroupOptions->getValue(sname.c_str());
      const char* swResetValue = formatSwitch(parentValue, false);
      qDebug() << "reset" << switchName << "to" << swResetValue;
      if (swResetValue && strlen(swResetValue) > 0)
        stream << swResetValue << endl;
    }
  }
  else
  {
    bool sentVlogLib = false;

    if (group->isLibrary())
    {
      if (group->isVhdlLibrary())
        stream << "-vhdlLib " << group->getVhdlLibrary() << endl;
      else if (group->isVerilogLibrary() && !sentVlogLib && !blockVlogLib)
      {
        sentVlogLib = true;
        stream << "-vlogLib " << group->getVerilogLibrary() << endl;
      }
    }
 
    // Check for mixed-language references
    for (int j=0; j<group->numSources(); j++)
    {
      CarbonHDLSourceFile* srcFile = group->getSource(j);
      HDLType hdlType = CarbonProjectWidget::hdlFileType(srcFile->getRelativePath());
      if ((hdlType == Verilog && group->isVhdlLibrary()) || (hdlType == VHDL && group->isVerilogLibrary()))
      {
        if (hdlType == Verilog && group->isVhdlLibrary() && !sentVlogLib && !blockVlogLib)
        {
          stream << "-vlogLib " << group->getLibraryName() << endl;
          sentVlogLib = true;
        }
        else if (group->isVerilogLibrary())
          stream << "-vhdlLib " << group->getLibraryName() << endl;
        break;
      }
    }
  }
}


// Write out all the compiler switches to the stream
// for the active configuration
void CarbonProject::writeCompilerSwitches(CarbonConfiguration* activeConfig, QTextStream& stream, MakefileType mtype)
{
  for (CarbonOptions::OptionsLoop p = activeConfig->getOptions("VSPCompiler")->loopOptions(); !p.atEnd(); ++p)
  {
    CarbonPropertyValue* sv = p.getValue();
    if (sv)
    {
      const CarbonProperty* prop = sv->getProperty();
      if (!prop->getReadOnly() && !prop->getDisplayOnly())
      {
        bool ignoreIt = false;
        for (int is=0; is<(int)(sizeof(ignoreOutputSwitches)/sizeof(const char*)); is++)
        {
          if (0 == strcmp(prop->getName(), ignoreOutputSwitches[is]))
          {
            ignoreIt = true;
            break;
          }
        }

        if (ignoreIt)
          continue;

        // skip these for scan projects
        if (mtype == CarbonProject::eCbuildScan)
        {
          if (0 == strcmp(sv->getProperty()->getName(), "-vlogTop") ||
              0 == strcmp(sv->getProperty()->getName(), "-vhdlTop") ||
              0 == strcmp(sv->getProperty()->getName(), "-o"))
              continue;
        }

        if (mtype == CarbonProject::eCbuildScan)
          qDebug() << "cbuildScan" << sv->getProperty()->getName();

        const char* swValue = formatSwitch(sv);
        if (swValue && strlen(swValue) > 0)
          stream << swValue << endl;
      }
    }
  }
}

QString CarbonProject::getRemoteWorkingDirectory()
{
  QString remDir = getProjectOptions()->getValue("Remote Working Directory")->getValue();
  if (remDir.isEmpty())
    remDir = getUnixEquivalentPath(getProjectDirectory());
  return remDir;
}

void CarbonProject::writeCarbonVariables(CarbonConfiguration* cfg, QTextStream& stream, bool windowsTarget)
{
  stream << "CARBON_VERBOSE=1" << endl;
  stream << "CARBON_REMOTE=0" << endl;

  if (windowsTarget)
    stream << "CARBON_MODEL_PLATFORM := Windows" << endl << endl;
  else
    stream << "CARBON_MODEL_PLATFORM := Unix" << endl << endl;

  stream << endl;

  stream << "ifeq ($(CARBON_VERBOSE),1)" << endl;
  stream << "  CARBON_ECHOFLAG=1" << endl;
  stream << "endif" << endl;

  stream << endl;

  stream << "define CARBON_ECHO" << endl;
  stream << "  $(if $(CARBON_ECHOFLAG),@echo $1)" << endl;
  stream << "endef" << endl;

  stream << endl;

  stream << "ifeq ($(OS),Windows_NT)" << endl;
  stream << "  CARBON_OS := Windows" << endl;
  stream << "  CARBON_MAKE := nmake" << endl;
  stream << "  CARBON_START=StartWindows" << endl;
  stream << "  CARBON_FINISHED=FinishedWindows Completed" << endl;
  stream << "  CARBON_RM := cmd.exe /c del /q" << endl;
  stream << "  CARBON_CP := cmd.exe /c copy /y" << endl;
  stream << "  CARBON_MKDIR := cmd.exe /c mkdir" << endl;
  // Windows touch: copy /b filename +,,
  stream << "  CARBON_TOUCH := cmd.exe /c copy /b" << endl;
  stream << "  CARBON_TOUCH_POST := +,," << endl;
  stream << "  CARBON_WINDOWS_EXT := .windows" << endl;
  stream << "  CARBON_EXE_EXT := .exe" << endl;
  stream << "  CARBON_SCRIPT_EXT := .bat" << endl;
  stream << "  CARBON_SHARED_EXT := .dll" << endl;
  stream << "  CARBON_BATCH := carbon.bat" << endl;
  stream << "  CARBON_CMD_SEPARATOR := &&" << endl;
  stream << "  CARBON_PACKAGECLEAN :=" << endl;
  stream << "else" << endl;
  stream << "  CARBON_OS := Unix" << endl;
  stream << "  CARBON_MAKE := make" << endl;
  stream << "  CARBON_START=Start" << endl;
  stream << "ifeq ($(CARBON_REMOTE),0)" << endl;
  stream << "  CARBON_FINISHED=Finished Completed" << endl;
  stream << "else" << endl;
  stream << "  CARBON_FINISHED=Finished" << endl;
  stream << "endif" << endl;
  stream << "  CARBON_RM := rm -rf" << endl;
  stream << "  CARBON_CP := cp -u" << endl;
  stream << "  CARBON_TOUCH := touch" << endl;
  stream << "  CARBON_TOUCH_POST :=" << endl;
  stream << "  CARBON_MKDIR := mkdir -p" << endl;
  stream << "  CARBON_WINDOWS_EXT :=" << endl;
  stream << "  CARBON_EXE_EXT :=" << endl;
  stream << "  CARBON_SCRIPT_EXT :=" << endl;
  stream << "  CARBON_SHARED_EXT := .so" << endl;
  stream << "  CARBON_BATCH := carbon" << endl;
  stream << "  CARBON_CMD_SEPARATOR := ;" << endl;
  stream << "  CARBON_PACKAGECLEAN := PackageClean" << endl;
  stream << "endif" << endl << endl;

  QString platformString = "Linux";

  UtString projDir;
#if pfWINDOWS
  QString rdir = getRemoteWorkingDirectory();
  if (rdir.length() > 0)
    projDir << rdir;
  else
    projDir << getProjectDirectory();
#else
  projDir << getProjectDirectory();
#endif

  // Always put this into the Makefile CTA and CHA
  QString modelCompilation = cfg->getOptions("VSPCompiler")->getValue("Compiler Control")->getValue();

  QString hostArch = "Linux";
  QString targetArch = "Linux";

  if (modelCompilation == MODEL_32x32)
  {
    hostArch = "Linux";
    targetArch = "Linux";
  }
  else if (modelCompilation == MODEL_64x32)
  {
    hostArch = "Linux64";
    targetArch = "Linux";
  }
  else if (modelCompilation == MODEL_64x64)
  {
    hostArch = "Linux64";
    targetArch = "Linux64";
  }

  stream << "# These variables control the Model compilation" << "\n";
  stream << "CARBON_HOST_ARCH" << "=" << hostArch << "\n";
  stream << "export CARBON_HOST_ARCH" << "\n";
  stream << "CARBON_TARGET_ARCH" << "=" << targetArch << "\n";
  stream << "export CARBON_TARGET_ARCH" << "\n";
  

  stream << "# These variables are provided as a convenience for downstream tools." << "\n";
  stream << "CARBON_PROJECT" << "=" << projDir.c_str() << "\n";
  stream << "export CARBON_PROJECT" << "\n";

  stream << "CARBON_PROJECT_RELATIVE=../.." << "\n";
  stream << "export CARBON_PROJECT_RELATIVE" << "\n";

#if pfWINDOWS
  stream << "CARBON_PROJECT_WINDOWS" << "=" << getProjectDirectory() << "\n";
  stream << "export CARBON_PROJECT_WINDOWS" << "\n";
#endif

  stream << "CARBON_CONFIGURATION" << "=" << platformString << "/" << cfg->getName() << "\n";
  stream << "export CARBON_CONFIGURATION" << "\n";
  stream << "CARBON_OUTPUT" << "=" << "$(CARBON_PROJECT)/$(CARBON_CONFIGURATION)" << "\n";
  stream << "export CARBON_OUTPUT" << "\n";
  stream << "CARBON_MODEL=" << cfg->getOptions("VSPCompiler")->getValue("-o")->getValue() << "\n";
  stream << "export CARBON_MODEL" << "\n";

  stream << "\n";
}

// Static method, takes in strings
// $foo/bar and expands them to ${foo}/bar
// otherwise, it leave it alone and returns the origianl string
// $(foo)/bar and returns ${foo}/bar

QString CarbonProject::fixVarReferences(const QString& inStr)
{
  if (inStr.contains(ENVVAR_REFERENCE))
  {
    QString temp = inStr;
    int ix = -1;
    do // Repeat this until there are no more $vars 
    {
      QRegExp exp1("(\\$\\w+)");
      exp1.setPatternSyntax(QRegExp::RegExp);
      ix = exp1.indexIn(temp);
      int i2 = exp1.matchedLength();
      int nc = exp1.numCaptures();
      if (ix != -1 && nc == 1)
      {
        QString left = temp.left(ix);
        QString rest = temp.mid(ix+i2);
        temp = left + "${" + exp1.cap(1).mid(1) + "}" + rest;
      }
    }
    while (ix != -1);

    ix = -1;
    do // repeat until no more $(var)s
    {
      QRegExp exp1("\\$\\((\\w+)\\)");
      exp1.setPatternSyntax(QRegExp::RegExp);
      ix = exp1.indexIn(temp);
      int i2 = exp1.matchedLength();
      int nc = exp1.numCaptures();
      if (ix != -1 && nc == 1)
      {
        QString left = temp.left(ix);
        QString rest = temp.mid(ix+i2);
        temp = left + "${" + exp1.cap(1) + "}" + rest;
      }
    }
    while (ix != -1);

    return temp;
  }
  
  return inStr;
}
void CarbonProject::writeFileMacros(CarbonConfiguration*, QTextStream& stream, QTextStream* groupStream)
{
#if pfWINDOWS
  QStringList driveLetters;
  QStringList notUsedList;

  SSHConsoleHelper::getNetworkDrives(driveLetters, notUsedList);
#endif

  // Build the sources used to create the VHM, ignoring libraries for now
  // we will create seperate targets for libraries.
  TempChangeDirectory temp1(getProjectDirectory());

  // Guard all file macros with non-Windows
  stream << "ifneq ($(CARBON_OS),Windows)" << endl;

  for (int i=0; i<getGroups()->numGroups(); ++i)
  {
    CarbonSourceGroup* group = getGroups()->getGroup(i);
    int groupIndex = group->getIndex();

    int nSources = group->numSources();
    bool isLibrary = group->isLibrary();

    if (isLibrary)
      stream << "RTL_LIB_GROUP_" << groupIndex << " = \\" << endl;
    else
      stream << "RTL_GROUP_" << groupIndex << " = \\" << endl;

    if (!isLibrary && groupStream)
      *groupStream << "$(RTL_GROUP_" << groupIndex << ") ";

    for (int j=0; j<nSources; j++)
    {
      CarbonHDLSourceFile* srcFile = group->getSource(j);
      QString srcFilePath = srcFile->getRelativePath();
      UtString srcPath;
      QFileInfo sfi(srcFilePath);
      if (!srcFilePath.contains(ENVVAR_REFERENCE) && sfi.isRelative())
        OSConstructFilePath(&srcPath, getProjectDirectory(), srcFile->getRelativePath());
      else
        srcPath << fixVarReferences(srcFile->getCanonicalPath());

      UtString unixPath;
      unixPath << getUnixEquivalentPath(srcPath.c_str());

      stream << "\t" << unixPath.c_str();

      if (j < nSources-1)
        stream << " \\" << endl;
    }

    stream << endl << endl;
  } 

  // Close guard
  stream << endl << "endif" << endl << endl;

}

void CarbonProject::writeSourceGroupSwitches(CarbonConfiguration* activeConfig, QTextStream& stream, CarbonSourceGroup* group, QStringList& groupSwitches)
{
  groupSwitches.clear();

  CarbonOptions* options = group->getOptions(activeConfig->getName());

  for (CarbonOptions::OptionsLoop p = options->loopOptions(); !p.atEnd(); ++p)
  {
    CarbonPropertyValue* sv = p.getValue();
    if (sv)
    {
      const CarbonProperty* prop = sv->getProperty();
      if (!prop->getReadOnly() && !prop->getDisplayOnly())
      {
        bool ignoreIt = false;
        for (int is=0; is<(int)(sizeof(ignoreOutputSwitches)/sizeof(const char*)); is++)
        {
          if (0 == strcmp(prop->getName(), ignoreOutputSwitches[is]))
          {
            ignoreIt = true;
            break;
          }
        }

        if (ignoreIt)
          continue;

        const char* swValue = formatSwitch(sv);
        if (swValue && strlen(swValue) > 0)
        {
          groupSwitches << prop->getName();
          stream << swValue << endl;
        }
      }
    }
  }
}

// Build up a libdesign.<logicalName>.cmd file
const char* CarbonProject::generateLibraryCommandFile(CarbonConfiguration* activeConfig, CarbonSourceGroup* group, int libIndex, const char* outputDir, const char* cmdFileName)
{
  static UtString outputFileName;

  outputFileName.clear();

  OSConstructFilePath(&outputFileName, outputDir, cmdFileName);

  qDebug() << "Generating Library Command File: " << outputFileName.c_str();

  QString cmdfileBuffer;
  QTextStream stream(&cmdfileBuffer);
  stream.setCodec("UTF-8");

  stream << "-compileLibOnly" << endl;

  // Spit out the compiler switches
  writeCompilerSwitches(activeConfig, stream);
  
  bool blockVlogLib = false;

  // we need to write out references to all the previous libraries here
  for (int i=0; i<=libIndex; i++)
  {
    CarbonSourceGroup* libGroup = getGroups()->getGroup(i);
    if (libGroup->isVhdlLibrary())
      stream << "-vhdlLib " << libGroup->getVhdlLibrary() << endl;
    else if (libGroup->isVerilogLibrary() && !blockVlogLib)
    {
      stream << "-vlogLib " << libGroup->getVerilogLibrary() << endl;
      blockVlogLib = true;
    }
  }
  
  QStringList groupSwitches;
  // write out the library switches now.
  writeSourceGroupSwitches(activeConfig, stream, group, groupSwitches);

  // Write files and file-specific switches
  writeFileSpecificSwitches(activeConfig, stream, group, false, blockVlogLib);

  // put back the sourceGroup switches
  const CarbonOptions* parentGroupOptions = group->getOptions(activeConfig->getName())->getParent();
  foreach (QString switchName, groupSwitches)
  {
    UtString sname;
    sname << switchName;
    CarbonPropertyValue* parentValue = parentGroupOptions->getValue(sname.c_str());
    const char* swResetValue = formatSwitch(parentValue, false);
    qDebug() << "reset" << switchName << "to" << swResetValue;
    if (swResetValue && strlen(swResetValue) > 0)
      stream << swResetValue << endl;
  }

  // write the file only if different
  writeFileIfChanged(outputFileName.c_str(), &stream);


  return outputFileName.c_str();
}

void CarbonProject::writeFileDependenciesVar(QTextStream& stream, CarbonConfiguration* activeConfig, const QString& varName, const char* switchName)
{
  QString files = activeConfig->getOptions("VSPCompiler")->getValue(switchName)->getValue();
  stream << varName << "=" << endl;
  stream << "ifeq ($(CARBON_OS),Unix)" << endl;

  if (!files.isEmpty())
  {
    stream << varName << "=";
    foreach (QString file, files.split(";"))
      stream << " " << resolveFile(file);
    stream << endl;
  }
  stream << "endif" << endl;
}

//
// Build up a makefile into the stream
const char* CarbonProject::generateMakeFile(CarbonConfiguration* activeConfig, CarbonConsole::CommandMode, const char* outputDir, const char* cmdFileName, const char* dirFileName)
{
  static UtString target;
  target.clear();
  const char* postCompileScript = getProjectOptions()->getValue("Post Compilation Script")->getValue();
  const char* postCompileScriptArgs = getProjectOptions()->getValue("Post Compilation Script Arguments")->getValue();

  const char* epilogueScript = getProjectOptions()->getValue("Epilogue Script")->getValue();
  const char* epilogueScriptArgs = getProjectOptions()->getValue("Epilogue Script Arguments")->getValue();

  // always local for Linux, otherwise, user can define the mode
  CarbonConsole::CommandMode epilogueMode = CarbonConsole::Local;

  // Windows users can change it to remote, otherwise default to Local
#if pfWINDOWS
  const char* epilogueScriptMode = getProjectOptions()->getValue("Epilogue Script Execution")->getValue();
  if (0 == strcmp(epilogueScriptMode, "remote"))
    epilogueMode = CarbonConsole::Remote;
#endif

  QString makefileBuffer;
  QTextStream stream(&makefileBuffer);
  stream.setCodec("UTF-8");

  stream << "# Generated Makefile by Carbon ModelStudio *DO NOT EDIT, CHANGES WILL BE LOST *" << endl;
  stream << "# Carbon ModelStudio Version: " << CARBON_RELEASE_ID << endl << endl;

  stream << "ifeq ($(CARBON_HOME),)" << endl;
  stream << "$(error CARBON_HOME is not defined, please define Remote CARBON_HOME or in your login script.)" << endl;
  stream << "endif" << endl << endl;

  CarbonSourceGroups* sourceGroups = getGroups();

  // Build up the primary target:
  CarbonPropertyValue* outSwitch = activeConfig->getOptions("VSPCompiler")->getValue("-o");
  const char* outputTarget = outSwitch->getValue();
  QFileInfo fioutput(outputTarget);

  QString outputType = fioutput.completeSuffix().toLower();
  bool windowsTarget = false;
  if (outputType == "lib" || outputType == "dll")
    windowsTarget = true;
  else
    windowsTarget = false;

  target << outputTarget;
  // List out the source files

  QString groupList;
  QTextStream groupStream(&groupList);

  writeCarbonVariables(activeConfig, stream, windowsTarget);

  // the model name
  stream << "VHMNAME=" << outputTarget << endl << endl;
  

  if (mProjectType == ModelKitProject)
  {
    stream << "export ARM_ROOT=$(ARM_KIT_ROOT)" << endl << endl;
    stream << "MODELKITNAME=" << outputTarget << endl << endl;
  }


  // Build the sources used to create the VHM, 
  writeFileMacros(activeConfig, stream, &groupStream);

  // build up the list of active componets that need to be 
  // put into the makefile
  QList<CarbonComponent*> comps;
  mComponents->getActiveComponents(&comps);

  stream << endl << endl;

  stream << "all: ; @echo Must specify a target; exit 1" << endl << endl;

  stream << ".PHONY: rebuild" << endl;
  stream << "rebuild: clean build" << endl;
  stream << endl;

  stream << ".PHONY: build" << endl;
  stream << "build: $(CARBON_START) buildinternal $(CARBON_FINISHED)" << endl << endl;

  stream << ".PHONY: buildinternal" << endl;
  stream << "buildinternal:";

  // buildinternal: $(VHMNAME) SoCDesignerComponent CoWareComponent
  if (mProjectType == HDL)
    stream << " " << "$(VHMNAME)";

  if (mProjectType == ModelKitProject)
    stream << " " << "$(MODELKITNAME)";

  foreach(CarbonComponent* comp, comps)
  {
    if (comp->canCompile(activeConfig))
      stream << " " << comp->getTargetName(activeConfig, CarbonComponent::Build, CarbonComponent::Unix);
  }
  stream << endl << endl;

  stream << ".PHONY: package" << endl;
  stream << "package: $(CARBON_START) buildinternal";

  // package: $(CARBON_START) buildinternal SoCDesignerComponent CoWareComponent $(CARBON_FINISHED)
  foreach(CarbonComponent* comp, comps)
  {
    stream << " " << comp->getTargetName(activeConfig, CarbonComponent::Package, CarbonComponent::Unix);
  }
  stream << " $(CARBON_FINISHED)" << endl << endl << endl;

  stream << ".PHONY: buildModel" << endl;
  stream << "buildModel: $(CARBON_START)";

  if (theApp->checkArgument("-modelKitProfileUse")) {
    qDebug() << "-modelKitProfileUse found: adding cleanlib to buildModel target.";
    stream << " " << "cleanlib";
  }
  if (mProjectType == HDL)
    stream << " " << "$(VHMNAME)";

  if (mProjectType == ModelKitProject)
    stream << " " << "$(MODELKITNAME)";

  stream << " $(CARBON_FINISHED)" << endl << endl;

  stream << endl;

  stream << ".PHONY: CarbonVersionCheck" << endl;
  stream << "CarbonVersionCheck:" << endl;
  stream << "\t" << "$(CARBON_HOME)/bin/cbuild -version" << endl;
  stream << "\t" << "@echo CARBON VERSION CHECK COMPLETED" << endl << endl;

  stream << ".PHONY: CarbonFileCheck" << endl;
  stream << "CarbonFileCheck:" << endl;
  stream << "\t" << "$(CARBON_HOME)/bin/check_file $(CARBON_CHECK_FILENAME)" << endl;

  stream << ".PHONY: StartClean" << endl;
  stream << "StartClean:" << endl;
  stream << "\t" << CarbonComponent::echoBegin() << "CARBON BEGIN CLEAN" << CarbonComponent::echoEndl() << endl;

  stream << ".PHONY: FinishedClean" << endl;
  stream << "FinishedClean:" << endl;
  stream << "\t" << CarbonComponent::echoBegin() << "CARBON FINISHED CLEAN" << CarbonComponent::echoEndl() << endl;

  stream << ".PHONY: StartExplore" << endl;
  stream << "StartExplore:" << endl;
  stream << "\t" << CarbonComponent::echoBegin() << "CARBON BEGIN EXPLORE" << CarbonComponent::echoEndl() << endl;

  stream << ".PHONY: FinishedExplore" << endl;
  stream << "FinishedExplore:" << endl;
  stream << "\t" << CarbonComponent::echoBegin() << "CARBON FINISHED EXPLORE" << CarbonComponent::echoEndl() << endl;

  stream << ".PHONY: StartSimulate" << endl;
  stream << "StartSimulate:" << endl;
  stream << "\t" << CarbonComponent::echoBegin() << "CARBON BEGIN SIMULATION" << CarbonComponent::echoEndl() << endl;

  stream << ".PHONY: FinishedSimulate" << endl;
  stream << "FinishedSimulate:" << endl;
  stream << "\t" << CarbonComponent::echoBegin() << "CARBON FINISHED SIMULATION" << CarbonComponent::echoEndl() << endl;

  stream << ".PHONY: StartWindows" << endl;
  stream << "StartWindows:" << endl;
  stream << "\t" << CarbonComponent::echoBegin() << "CARBON BEGIN WINDOWS COMPILATION" << CarbonComponent::echoEndl() << endl;

  stream << ".PHONY: FinishedWindows" << endl;
  stream << "FinishedWindows: " << endl;
  stream << "\t" << CarbonComponent::echoBegin() << "CARBON FINISHED WINDOWS COMPILATION" << CarbonComponent::echoEndl();

  stream << ".PHONY: Start" << endl;
  stream << "Start:" << endl;
  stream << "\t" << CarbonComponent::echoBegin() << "CARBON BEGIN COMPILATION" << CarbonComponent::echoEndl() << endl;

  stream << ".PHONY: Finished" << endl;
  stream << "Finished: " << endl;
  stream << "\t" << CarbonComponent::echoBegin() << "CARBON FINISHED COMPILATION" << CarbonComponent::echoEndl();

  if (postCompileScript && strlen(postCompileScript) > 0)
    stream << "\t" << "@ if [ -e carbon.compilation ]; then " << postCompileScript << " " << postCompileScriptArgs << "; $(CARBON_RM) carbon.compilation; fi" << endl;

  // After we hit "Finished" and we are on Windows, and the mode is remote, do it now.
  if (epilogueScript && strlen(epilogueScript) > 0 && epilogueMode == CarbonConsole::Remote)
    stream << "\t" << epilogueScript << " " << epilogueScriptArgs << endl;  

  stream << ".PHONY: Completed" << endl;
  stream << "Completed: " << endl;

  // If the mode is local and we have an epilogue script, do it now
  if (epilogueScript && strlen(epilogueScript) > 0 && epilogueMode == CarbonConsole::Local)
    stream << "\t" << epilogueScript << " " << epilogueScriptArgs << endl;  

  stream << "\t" << CarbonComponent::echoBegin() << "CARBON MAKEFILE COMPLETED" << CarbonComponent::echoEndl();

  stream << endl;
  QStringList libraryList;

  if (mProjectType == HDL)
  {
    // Generate Library Targets
    for (int i=0; i<sourceGroups->numGroups(); ++i)
    {
      CarbonSourceGroup* group = sourceGroups->getGroup(i);
      int groupIndex = group->getIndex();

      if (group->isLibrary()) // Only "Libraries" here
      {
        UtString targetName = group->getTargetName(activeConfig);

        UtString libCmdFileName;
        libCmdFileName << group->getCommandFilename(activeConfig);

        generateLibraryCommandFile(activeConfig, group, groupIndex, outputDir, libCmdFileName.c_str());

        stream << targetName.c_str() << ": " << libCmdFileName.c_str() << " ";
        foreach (QString libDir, libraryList)
          stream << libDir << " ";

        stream << "$(RTL_LIB_GROUP_" << groupIndex << ")";
        stream << endl;
        stream << "\t" << "$(CARBON_HOME)/bin/cbuild -f $<" << endl;
        stream << "\t" << "touch $@" << endl << endl;
        libraryList.append(targetName.c_str());
      }
    }

  // write dependencies to external files (directives and/or command files)
    writeFileDependenciesVar(stream, activeConfig, "CARBON_DIRECTIVE_FILES", "-directive");
    writeFileDependenciesVar(stream, activeConfig, "CARBON_COMMAND_FILES", "-f");

    // write out the main VHM target here
    stream << "$(VHMNAME)" << ": ";
    foreach (QString libDir, libraryList)
      stream << libDir << " ";
    stream << groupList << " " << cmdFileName << " " << dirFileName << " $(CARBON_DIRECTIVE_FILES) $(CARBON_COMMAND_FILES)" << endl;

    stream << "ifeq ($(CARBON_OS),Unix)" << endl;
    stream << "\t" << "$(CARBON_HOME)/bin/cbuild -f " << cmdFileName << " $(GUI_DB_FLAGS)" << endl;
    if (postCompileScript && strlen(postCompileScript) > 0)
      stream << "\t" << "touch carbon.compilation" << endl;
    stream << "\t" << CarbonComponent::echoBegin() << "FINISHED MODEL COMPILATION" << CarbonComponent::echoEndl() << endl;
    stream << "endif" << endl;

    stream << ".PHONY: explore" << endl;
    stream << "explore: GUI_DB_FLAGS=-writeGuiDB" << endl;
    stream << "explore: StartExplore " << fioutput.baseName() << ".gui.db FinishedExplore Completed" << endl << endl;

    // gui db target
    stream << fioutput.baseName() << ".gui.db: ";
    foreach (QString libDir, libraryList)
      stream << libDir << " ";

    stream << " $(RTL_GROUP_0) " << activeConfig->getName() << ".f.cmd " << activeConfig->getName() << ".directives" << " $(CARBON_DIRECTIVE_FILES) $(CARBON_COMMAND_FILES)" << endl;
    stream << "ifeq ($(CARBON_OS),Unix)" << endl;
    stream << "\t" << "$(CARBON_HOME)/bin/cbuild -f " << activeConfig->getName() << ".f.cmd $(GUI_DB_FLAGS)" << endl;
    stream << "endif" << endl;
  }

  if (mProjectType == ModelKitProject)
  {
    stream << "$(MODELKITNAME)" << ":" << endl;
    stream << "ifeq ($(CARBON_OS),Unix)" << endl;
    stream << "\t" << "$(CARBON_HOME)/bin/cbuild -ef compile.ef";
    if (theApp->checkArgument("-modelKitProfileGenerate")) {
      qDebug() << "-modelKitProfileGenerate found: adding -profileGenerate compile switch";
      stream << " -profileGenerate";
    } else if (theApp->checkArgument("-modelKitProfileUse")) {
      qDebug() << "-modelKitProfileUse found: adding -profileUse compile switch";
      stream << " -profileUse";
    }
    stream << endl;
    if (postCompileScript && strlen(postCompileScript) > 0)
      stream << "\t" << "touch carbon.compilation" << endl;
    stream << "\t" << CarbonComponent::echoBegin() << "FINISHED MODEL COMPILATION" << CarbonComponent::echoEndl() << endl;
    stream << "endif" << endl;

    stream << "modelkit_explore" << ":" << endl;
    stream << "ifeq ($(CARBON_OS),Unix)" << endl;
    stream << "\t" << "$(CARBON_HOME)/bin/cbuild -ef explore.ef" << endl;
    stream << "\t" << CarbonComponent::echoBegin() << "FINISHED MODEL COMPILATION" << CarbonComponent::echoEndl() << endl;
    stream << "endif" << endl;
  }

  UtString makefileName;
  OSConstructFilePath(&makefileName, outputDir, "Makefile.carbon");

  // allow components to create supporting files
  foreach(CarbonComponent* comp, comps)
  {
    if (comp->canCompile(activeConfig))
      comp->generateMakefile(activeConfig, makefileName.c_str());
    else
      qDebug() << "Cannot compile: " << comp->getName() << " " << activeConfig->getName() << " " << activeConfig->getPlatform();
  }

  // write out the component targets
  foreach(CarbonComponent* comp, comps)
  {
    if (comp->canCompile(activeConfig))
    {
      stream << endl << endl;
      comp->getTargetImpl(activeConfig, stream, CarbonComponent::Build);
    }
  }

  // write out the component package targets
  foreach(CarbonComponent* comp, comps)
  {
    comp->getTargetImpl(activeConfig, stream, CarbonComponent::Package);
  }

  // write out the component clean targets
  foreach(CarbonComponent* comp, comps)
  {
    if (comp->canCompile(activeConfig))
    {
      stream << endl << endl;
      comp->getTargetImpl(activeConfig, stream, CarbonComponent::Clean);
    }
  }
  // Unix Clean
  stream << endl << endl;
  stream << ".PHONY: clean" << endl;
  stream << endl << "clean: StartClean";
  foreach(CarbonComponent* comp, comps)
  {
    if (comp->canCompile(activeConfig))
      stream << " " << comp->getTargetName(activeConfig, CarbonComponent::Clean);
  }

  stream << " FinishedClean" << endl;

  if (mProjectType == HDL)
  {
    stream << "\t" << "$(CARBON_RM) $(basename $(VHMNAME)).*" << endl;
    for (int i=0; i<sourceGroups->numGroups(); ++i)
    {
      CarbonSourceGroup* group = sourceGroups->getGroup(i);
      if (group->isLibrary()) // Only "Libraries" here
      {
        UtString targetDir = group->getTargetDirectory(activeConfig);
        stream << "\t" << "$(CARBON_RM) " << targetDir.c_str() << endl;
      }
    }
  }

  stream << endl << endl;

  stream << endl << ".PHONY: cleanlib" << endl;
  stream << endl << "cleanlib:" << endl;
  stream << "\t" << "$(CARBON_RM) ";
  if (mProjectType == ModelKitProject) {
    stream << "$(MODELKITNAME)";
  } else {
    stream << "$(CLEANLIB_NAME)";
  }
  stream << endl;

  stream << endl << ".PHONY: simulate" << endl;
  stream << endl << "simulate:" << endl;

  UtString programToRun;
  const char* simulationScript = getProjectOptions()->getValue("Simulation Script")->getValue();
  if (simulationScript && strlen(simulationScript) > 0)
  {
    UtString simulationArgs = getProjectOptions()->getValue("Simulation Script Arguments")->getValue();

    CarbonConsole::CommandMode mode = CarbonConsole::Local;
    const char* scriptLoc = getProjectOptions()->getValue("Simulation Script Execution")->getValue();
    if (scriptLoc && 0 == strcmp(scriptLoc, "remote"))
      mode = CarbonConsole::Remote;
    else
      mode = CarbonConsole::Local;

    stream << "\t" << simulationScript;
    if (simulationArgs.length() > 0)
      stream << " " << simulationArgs.c_str();
    stream << endl;
  }
  else
    stream << "\t" << "@echo ****WARNING: No simulation script has been set" << endl;

  writeFileIfChanged(makefileName.c_str(), &stream);

  return target.c_str();
}

// Write out the command file which gets passed to the compiler
// only if there isn't one or if the contents would be different 
// from the existing one because this ends up being a dependency in
// the makefile
bool CarbonProject::writeFileIfChanged(const char* fileName, QTextStream* stream)
{
  bool writeFile = true;
  QFileInfo fi(fileName);
  QString streamBuffer = stream->readAll();

  if (fi.exists()) // Already exists, check to see if the new one would be different
  {
    QFile currentFile(fileName);
    currentFile.open(QFile::ReadOnly);
    QTextStream currStream(&currentFile);
    QString currBuffer = currStream.readAll();
    if (currBuffer == streamBuffer)
      writeFile = false;
    currentFile.close();
  }

  // do we need to write the command file?
  if (writeFile)
  {
    QFile file(fileName);
    file.open(QFile::WriteOnly | QFile::Unbuffered);
    UtString buffer;
    buffer << streamBuffer;
    file.write(buffer.c_str());
    file.close();
  }

  return writeFile;
}

void CarbonProject::addComponent(CarbonComponent* comp)
{
  mComponents->addComponent(comp);
  mComponents->populateComponentTree(mProjectWidget, NULL);
  emit componentAdded(comp);
}

void CarbonProject::deleteComponent(CarbonComponent* comp)
{
  emit componentDeleted(comp);
  mComponents->deleteComponent(comp);
}

void CarbonProject::compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  QApplication::restoreOverrideCursor();
  qDebug() << "Compilation Completed";
}

void CarbonProject::compilationStarted(CarbonConsole::CommandMode , CarbonConsole::CommandType )
{
  QApplication::setOverrideCursor(Qt::WaitCursor);
  qDebug() << "Compilation Starting";
}

CarbonProjectOptions::~CarbonProjectOptions()
{
  delete mProperties;
  delete mOptions;
}

CarbonProjectOptions::CarbonProjectOptions(CarbonProjectWidget* pw)
{
  mProjectWidget = pw;
  mProperties = new CarbonProperties();
  mProperties->readPropertyDefinitions(":/cmm/Resources/ProjectOptions.xml");

  mOptions = new CarbonOptions(NULL, mProperties, "ProjectOptions");
}

QString CarbonProject::remoteCommandName(const QString& cmdName)
{
  QString remoteName = cmdName;

#if pfWINDOWS
  UtString command;
  CarbonPropertyValue* remoteHome = getProjectOptions()->getValue("Remote CARBON_HOME");
  UtString remoteCarbonHome;
  remoteCarbonHome << remoteHome->getValue();

  if (remoteCarbonHome.length() > 0)
    command << remoteCarbonHome << "/bin/" << cmdName;
  else
    command << cmdName;

  remoteName = command.c_str();
#endif
  return remoteName;
}

#if pfWINDOWS
#include <windows.h> // needed for CoCreateGuid below
#include <stdio.h>
#endif

QString CarbonProject::createGUID()
{
  QString strGuid;

#if pfWINDOWS
  CoInitialize( 0 );
  GUID guid;
  CoCreateGuid(&guid);

  strGuid = strGuid.sprintf("%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X",
    guid.Data1, guid.Data4, guid.Data3,
    guid.Data4[ 0 ], guid.Data4[ 1 ],
    guid.Data4[ 2 ], guid.Data4[ 3 ],
    guid.Data4[ 4 ], guid.Data4[ 5 ],
    guid.Data4[ 6 ], guid.Data4[ 7 ] );

  CoUninitialize();
#else
  strGuid = qApp->sessionId();
#endif

  return strGuid;
}

bool CarbonProject::checkProject()
{
  bool validFlag = true;
  // Special handling for Windows only

#if pfWINDOWS
  // Verify that the Project Directory resides on a non-local drive
  QFileInfo projPath(getProjectDirectory());
  if (getProjectType() == HDL && !SSHConsoleHelper::isNetworkDrive(projPath.absolutePath()[0]))
  {
    QString dl = projPath.absolutePath()[0];
    UtString msg;
    msg << "The Project Directory: " << getProjectDirectory() << " must reside on a Network Mapped Drive\n";
    msg << "No Windows mapping was found for drive letter (" << dl << ":)\n";
    QMessageBox::critical(mProjectWidget, MODELSTUDIO_TITLE, msg.c_str());
    validFlag = false;
  }
#endif

  // Validate all RTL file path(s)
  CarbonConsole* console = mProjectWidget->getConsole();

  for (int i=0; i<getGroups()->numGroups(); ++i)
  {
    CarbonSourceGroup* group = getGroups()->getGroup(i);
    for (int j=0; j<group->numSources(); j++)
    {
      CarbonHDLSourceFile* srcFile = group->getSource(j);
      UtString srcPath;
      QFileInfo sfi(srcFile->getRelativePath());
      QString fileName = srcFile->getRelativePath();

      if (!fileName.contains(ENVVAR_REFERENCE) && sfi.isRelative())
        OSConstructFilePath(&srcPath, getProjectDirectory(), srcFile->getRelativePath());
      else
        srcPath << fixVarReferences(srcFile->getCanonicalPath());

      QString actualPath = getWindowsEquivalentPath(theApp->expandPath(srcPath.c_str()));
      QFileInfo sourceFile(actualPath);

      if (!sourceFile.exists())
      {
        QString errMsg = QString("Error 982: Unable to locate Source File: %1").arg(actualPath);
        UtString msg; msg << errMsg;
        console->processOutput(CarbonConsole::Local, CarbonConsole::Check, msg.c_str());
        validFlag = false;
      }
    }
  }

  return validFlag;
}

QString CarbonProject::pathRelativeToProject(const QString& inputPath)
{
  return inputPath;
}
