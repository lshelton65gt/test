// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#ifndef _CarbonInstanceOptions_h_
#define _CarbonInstanceOptions_h_

#include "util/CarbonPlatform.h"
#include "shell/ReplaySystem.h"
#include "CarbonOptions.h"

#include <QtGui>

#define REP_PROP_GROUP "Replay"
#define REP_PROP_CUR_STATE "Replay Current State"

#define REP_PROP_VERBOSE "Verbose"

#define REP_PROP_DATABASE "Database"

#define REP_PROP_REQ_STATE "Replay Requested State"
#define REP_REQ_STATE_RECORD   "record"
#define REP_REQ_STATE_PLAYBACK "playback"
#define REP_REQ_STATE_NORMAL   "normal"

#define REP_PROP_CHK_INTV "Checkpoint Interval"
#define REP_PROP_REC_PCNT "Recover Percentage"

#define ODM_PROP_CUR_STATE "OnDemand Current State"
#define ODM_PROP_MAX_STATES        "Maximum States"
#define ODM_PROP_BACKOFF_STATES    "Backoff Schedule Count"
#define ODM_PROP_BACKOFF_DECAY_PCT "Backoff Decay Percentage"
#define ODM_PROP_BACKOFF_MAX_DECAY "Backoff Maximum Decay"
#define ODM_PROP_BACKOFF_STRATEGY  "Backoff Strategy"
#define ODM_PROP_EXCLUDED_STATE_SIGNALS    "Excluded State Signals"
#define ODM_PROP_EXCLUDED_INPUT_SIGNALS    "Excluded Input Signals"

#define ODM_ATTR_MAX_STATES  "OnDemandMaxStates"
#define ODM_ATTR_BACKOFF_STRATEGY "OnDemandBackoffStrategy"
#define ODM_ATTR_BACKOFF_STATES "OnDemandBackoffCount"
#define ODM_ATTR_BACKOFF_DECAY_PCT "OnDemandBackoffDecayPercentage"
#define ODM_ATTR_BACKOFF_MAX_DECAY "OnDemandBackoffMaxDecay"

#define ODM_PROP_REQ_STATE "OnDemand Requested State"
#define ODM_REQ_STATE_START "start"
#define ODM_REQ_STATE_STOP  "stop"

#define ODM_PROP_TRACE "Trace"
#define ODM_PROP_TRACE_FILE "Trace File"

class CarbonRuntimeState;

class CarbonInstanceOptions: public QObject
{
  Q_OBJECT
public:
  CarbonInstanceOptions(CarbonRuntimeState *crs, const char* compName);
  ~CarbonInstanceOptions();

  CarbonOptions *getOptions() { return mOptions; }
  void putPropertyValues(CarbonReplaySystem &guiSystem, CarbonReplaySystem &simSystem, const char *compName);

  void merge(CarbonOptions *fromOptions, bool overWrite = false);
  const char* getReplayRequestedState();
  const char* getOnDemandRequestedState();
  bool getReplayVerbose();

  void putReplayRequestedState(const char* newVal);
  void putOnDemandRequestedState(const char* newVal);
  void putReplayVerbose(bool newVal);

private slots:
  void valueChangedReplayVerbose(const CarbonProperty* prop, const char* newValue);
  void valueChangedReplayDatabase(const CarbonProperty* prop, const char* newValue);
  void valueChangedReplayRequestedState(const CarbonProperty* prop, const char* newValue);
  void valueChangedReplayCheckpointInterval(const CarbonProperty* prop, const char* newValue);
  void valueChangedReplayRecoverPercentage(const CarbonProperty* prop, const char* newValue);

  void valueChangedOnDemandMaxStates(const CarbonProperty* prop, const char* newValue);
  void valueChangedOnDemandBackoffStates(const CarbonProperty* prop, const char* newValue);
  void valueChangedOnDemandBackoffDecayPercent(const CarbonProperty* prop, const char* newValue);
  void valueChangedOnDemandBackoffMaxDecay(const CarbonProperty* prop, const char* newValue);
  void valueChangedOnDemandBackoffStrategy(const CarbonProperty* prop, const char* newValue);
  void valueChangedOnDemandExcludedStateSignals(const CarbonProperty* prop, const char* newValue);
  void valueChangedOnDemandExcludedInputSignals(const CarbonProperty* prop, const char* newValue);
  void valueChangedOnDemandRequestedState(const CarbonProperty* prop, const char* newValue);
  void valueChangedOnDemandTrace(const CarbonProperty* prop, const char* newValue);

private:
  // turn notification of value changes on/off
  void putNotify(bool notify);

  CarbonRuntimeState    *mRuntimeState;
  CarbonOptions         *mOptions;
  CarbonProperties      *mProperties;
  UtString               mComponentName;
  bool                   mNotify;
};


#endif

