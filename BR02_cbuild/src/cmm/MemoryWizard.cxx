//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "MemoryWizard.h"

#include "WizPgStart.h"
#include "WizPgTemplates.h"
#include "WizPgTemplateParams.h"
#include "WizPgConfigurations.h"
#include "WizPgFinished.h"
#include "WizPgUserPorts.h"
#include "WizPgPorts.h"
#include "WizPgCodeGenerate.h"
#include "WizPgModeParameters.h"
#include "WizPgStartMulti.h"
#include "WizPgMultiPortMap.h"
#include "WizPgMultiGenerate.h"
#include "WizPgMultiFinished.h"
#include "WizPgMultiParamMap.h"

MemoryWizard::MemoryWizard(QWidget *parent)
: QWizard(parent)
{
  ui.setupUi(this);

#ifndef Q_WS_MAC
  setWizardStyle(ModernStyle);
#endif
  
  setPage(MemoryWizard::PageStart, new WizPgStart(this));
  setPage(MemoryWizard::PageTemplates, new WizPgTemplates(this));
  setPage(MemoryWizard::PageParameters, new WizPgTemplateParams(this));
  setPage(MemoryWizard::PageModeParameters, new WizPgModeParameters(this));
  setPage(MemoryWizard::PageUserPorts, new WizPgUserPorts(this));
  setPage(MemoryWizard::PagePorts, new WizPgPorts(this));
  setPage(MemoryWizard::PageConfigurations, new WizPgConfigurations(this));
  setPage(MemoryWizard::PageCodeGenerate, new WizPgCodeGenerate(this));
  setPage(MemoryWizard::PageFinished, new WizPgFinished(this));

  // Multi wizard flow

  setPage(MemoryWizard::PageStartMulti, new WizPgStartMulti(this));
  setPage(MemoryWizard::PageMultiPortMap, new WizPgMultiPortMap(this));
  setPage(MemoryWizard::PageMultiParamMap, new WizPgMultiParamMap(this));
  setPage(MemoryWizard::PageMultiGenerate, new WizPgMultiGenerate(this));
  setPage(MemoryWizard::PageMultiFinished, new WizPgMultiFinished(this));

  if (!mReplayFile.isEmpty())
    setStartId(MemoryWizard::PageTemplates); 
  else
    setStartId(MemoryWizard::PageStart);

  setWindowTitle(tr("Remodeling Wizard"));
}

void MemoryWizard::replayFile(const QString &replayFile)
{
  mReplayFile = replayFile;
  setStartId(MemoryWizard::PageTemplates); 
}

QDomElement MemoryWizard::findWizardStep(const QString stepName)
{
  QFile file(mReplayFile);
  if (file.open(QFile::ReadOnly | QFile::Text))
  {
    QDomDocument doc;
    int errorLine;
    int errorCol;
    QString errStr;
    if (doc.setContent(&file, true, &errStr, &errorLine, &errorCol))
    { 
      QDomElement docElem = doc.documentElement();
      QDomNode n = docElem.firstChild();
      while(!n.isNull())
      {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) 
        {
          if (e.tagName() == "WizardStep")
          {
            if (e.attribute("name") == stepName)
              return e;
          }
        }
        n = n.nextSibling();
      }
    }
  }
  return QDomElement();
}

void MemoryWizard::postNext()
{
  QTimer::singleShot(10, this, SLOT(next()));
}

void MemoryWizard::postRestart()
{
  QTimer::singleShot(10, this, SLOT(firstPage()));
}

void MemoryWizard::firstPage()
{
  hide();

  while (0 != currentPage()->objectName().compare("WizPgTemplateParamsClass"))
  {
    back();
    if (0 == currentPage()->objectName().compare("WizPgTemplatesClass"))
      break;
  }

  qDebug() << currentPage()->objectName();

  show();

}


bool MemoryWizard::serializeModulesFile(const QString& outputName)
{
  QDomDocument doc;
  QDomElement modulesTag = doc.createElement("Modules");
  modulesTag.setAttribute("version", "1");
  doc.appendChild(modulesTag);
  XmlErrorHandler eh;

  for (int i=0; i<numModules(); i++)
  {
    ModuleItem* mi = getModule(i);
    QDomElement moduleTag = doc.createElement("Module");
    moduleTag.setAttribute("name", mi->getName());
    moduleTag.setAttribute("locator", mi->getLocator());
    modulesTag.appendChild(moduleTag);
  }

  QFile file(outputName);
  if (file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    QTextStream out(&file);
    out << doc.toString(2);
  }

  return true;
}

XmlErrorHandler* MWPortConfigurations::parseFile(const QString &xmlFile)
{
  mErrorHandler.clear();
  mPorts.clear();
  mFuncClasses.clear();
  mModuleParamFunctions.clear();
  mModuleParams.clear();

  QFile file(xmlFile);

  if (file.open(QFile::ReadOnly | QFile::Text))
  {
    QDomDocument doc;
    int errorLine;
    int errorCol;
    QString errStr;

    QFileInfo templateFi(xmlFile);

    if (doc.setContent(&file, true, &errStr, &errorLine, &errorCol))
    { 
      QDomElement docElem = doc.documentElement();
      mVersion = docElem.attribute("version").toInt();

      QDomNode n = docElem.firstChild();
      while(!n.isNull())
      {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        n = n.nextSibling();
        if (!e.isNull() && !e.isComment())
        {
          if ("Ports" == e.tagName())
          {
            deserializePorts(e, &mErrorHandler);
          }
          else if ("Functions" == e.tagName())
          {
            deserializeFunctions(e, &mErrorHandler);
          }
          else if ("ModuleParameters" == e.tagName())
          {
            deserializeModuleParameters(e, &mErrorHandler);
          }
        }
      }
    }
  }
  else
  {
    QDomElement e;
    mErrorHandler.reportProblem(XmlErrorHandler::Error, e, QString("no such file: %1").arg(xmlFile));
  }

  return &mErrorHandler;
}  

bool MWPort::deserialize(const QDomElement& e, XmlErrorHandler*)
{
  setName(e.attribute("name"));
  setFunctionClass(e.attribute("functionClass"));
  setDefaultFunctionName(e.attribute("defaultFunction"));
  if (e.hasAttribute("portNumber"))
    setPortIndex(e.attribute("portNumber").toInt());
  else
    setPortIndex(0);
  return true;
}

bool MWPortConfigurations::deserializePorts(const QDomElement& e, XmlErrorHandler* eh)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    n = n.nextSibling();

    if (!element.isNull() && !element.isComment())
    {
      if ("Port" == element.tagName())
      {
        MWPort* port = new MWPort();
        port->deserialize(element, eh);
        mPorts.append(port);
      }
    }
  }
  return true;
}

bool MWFunction::deserialize(const QDomElement& e, XmlErrorHandler*)
{
  setName(e.attribute("name"));
  setDisplayName(e.attribute("displayName"));
  setDescription(e.attribute("description"));
  return true;
}

bool MWFunctionClass::deserialize(const QDomElement& e, XmlErrorHandler* eh)
{
  setName(e.attribute("name"));
  qDebug() << "functionclass" << getName();

  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    n = n.nextSibling();

    if (!element.isNull() && !element.isComment())
    {
      if ("Function" == element.tagName())
      {
        MWFunction* ffunc = new MWFunction(this);
        ffunc->deserialize(element, eh);
        mFuncs.append(ffunc);
      }
    }
  }
  return true;
}

bool MWPortConfigurations::deserializeFunctions(const QDomElement& e, XmlErrorHandler* eh)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    n = n.nextSibling();

    if (!element.isNull() && !element.isComment())
    {
      if ("FunctionClass" == element.tagName())
      {
        MWFunctionClass* fclass = new MWFunctionClass();
        fclass->deserialize(element, eh);
        mFuncClasses.append(fclass);
      }
    }
  }
  return true;
}

bool MWPortConfigurations::deserializeModuleParameters(const QDomElement& e, XmlErrorHandler* eh)
{
  QDomElement eFuncs = e.firstChildElement("Functions");
  if (!eFuncs.isNull())
  {
    deserializeModuleParameterFunctions(eFuncs, eh);
  }
  QDomElement eParams = e.firstChildElement("Parameters");
  if (!eParams.isNull())
  {
    deserializeModuleParametersParams(eParams, eh);
  }

  return true;
}

bool MWPortConfigurations::deserializeModuleParameterFunctions(const QDomElement& e, XmlErrorHandler* eh)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    n = n.nextSibling();

    if (!element.isNull() && !element.isComment())
    {
      if ("Function" == element.tagName())
      {
        MWModuleParamFunction* obj = new MWModuleParamFunction();
        obj->deserialize(element, eh);
        mModuleParamFunctions.append(obj);
      }
    }
  }
  return true;
}

bool MWPortConfigurations::deserializeModuleParametersParams(const QDomElement& e, XmlErrorHandler* eh)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    n = n.nextSibling();

    if (!element.isNull() && !element.isComment())
    {
      if ("Parameter" == element.tagName())
      {
        MWModuleParam* obj = new MWModuleParam();
        obj->deserialize(element, eh);
        mModuleParams.append(obj);
      }
    }
  }
  return true;
}

bool MWModuleParam::deserialize(const QDomElement& e, XmlErrorHandler*)
{
  setName(e.attribute("name"));
  return true;
}

bool MWModuleParamFunction::deserialize(const QDomElement& e, XmlErrorHandler*)
{
  setName(e.attribute("name"));
  setParameter(e.attribute("parameter"));
  return true;
}



