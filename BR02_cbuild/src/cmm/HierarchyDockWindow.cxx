//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QDebug>

#include "HierarchyDockWindow.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonProjectNodes.h"
#include "CarbonSourceGroups.h"
#include "CarbonHDLSourceFile.h"
#include "cmm.h"
#include "DlgPreferences.h"

HierarchyDockWindow::HierarchyDockWindow(QWidget *parent)
: QWidget(parent)
{
  ui.setupUi(this);
  mContext = NULL;

  mExplicitObserves = false;
  mExplicitDeposits = false;

  DlgPreferences prefs;
  bool defaultLoadHierarchy = prefs.getGeneralPreference(DlgPreferences::AutoLoadHierarchy).toBool();

  ui.checkBoxLoadDesign->setChecked(defaultLoadHierarchy);

  CQT_CONNECT(ui.widgetTable, navigateSource(const char*, int), this, navigateRequest(const char*, int));
  CQT_CONNECT(ui.widgetTable, applySignalDirective(DesignDirective,const char*,bool), this, broadcastDirective(DesignDirective,const char*, bool));
}


void HierarchyDockWindow::navigateRequest(const char* src, int line)
{
  emit navigateSource(src, line);
}

void HierarchyDockWindow::broadcastDirective(DesignDirective designDirective, const char* text, bool value)
{
  emit applySignalDirective(designDirective, text, value);
}

HierarchyDockWindow::~HierarchyDockWindow()
{

}

void HierarchyDockWindow::clear()
{
  ui.widgetTable->clearAll();
  ui.widgetTree->clearAll();
}

const char* HierarchyDockWindow::buildModuleList(const CarbonDBNode* node, QStringList& moduleList)
{
  if (node != NULL)
  {
    const CarbonDBNode* parent = carbonDBNodeGetParent(mAppCtx.getDB(), node);

    if (carbonDBCanBeCarbonNet(mAppCtx.getDB(), node))
    {
      buildModuleList(parent, moduleList);
      const char* leafName = carbonDBNodeGetLeafName(mAppCtx.getDB(), node);
      return leafName;
    }
    else 
    {
      buildModuleList(parent, moduleList);
      moduleList.push_back(carbonDBNodeGetLeafName(mAppCtx.getDB(), node));
    }
  } 
  return NULL;
}

// Expand Module and Net trees to the appropriate place
// for the given net.

// ../../VicAhbifReg.v:390 Net Vic_carbon.uVicAhbifReg.NxtVICIntEnable: Cycle node 1/1. 
bool HierarchyDockWindow::browseableNet(const char* netName)
{
  return carbonDBFindNode(mAppCtx.getDB(), netName) ? true : false;
}

bool HierarchyDockWindow::browseNet(const char* netName)
{
  const CarbonDBNode* node = carbonDBFindNode(mAppCtx.getDB(), netName);
  if (node)
  {
    QStringList moduleList;

    UtString fullName;
    fullName << carbonDBNodeGetFullName(mAppCtx.getDB(), node);

    const char* net = buildModuleList(node, moduleList);

    if (net)
    {
      ui.widgetTree->expandModules(moduleList);
      ui.widgetTable->selectNet(fullName.c_str());
    }
    raise();
    return true;
  }
  else
  {
    qDebug() << "Unable to locate net: " << netName;
    return false;
  }
}
 
void HierarchyDockWindow::selectNet(const char* netName)
{
  ui.widgetTable->selectNet(netName);
}

void HierarchyDockWindow::selectModule(const char* modName)
{
  ui.widgetTree->selectModule(modName);
}

void HierarchyDockWindow::selectionChanged(CarbonDBNode* /*node*/)
{  
  ui.widgetTable->putDragData(ui.widgetTable->getDragText());
  emit netSelectionChanged(ui.widgetTable->getDragText());
}

void HierarchyDockWindow::moduleSelectionChanged(CarbonDBNode*,XToplevelInterface*,XInstance*)
{
  ui.widgetTree->putDragData(ui.widgetTree->getDragText());
  emit moduleSelectionChanged(ui.widgetTree->getDragText());
}

void HierarchyDockWindow::moduleDoubleClicked(const char* nodeName, const char* module, const char* locator)
{
  if (locator)
    qDebug() << "dbl: " << nodeName << " module: " << module << " locator: " << locator;

  UtString filePath;
  CarbonProject* proj = mContext->getCarbonProjectWidget()->project();

  QRegExp sourceLocator("(.*):([0-9]+)");
  if (sourceLocator.exactMatch(locator))
  {
    QString src = sourceLocator.cap(1);
    QString sLineNumber = sourceLocator.cap(2);

    QFileInfo fi(src);
    if (fi.isRelative())
    {
      UtString srcFile;
      srcFile << src;
      UtString f;
      OSConstructFilePath(&f, proj->getActive()->getOutputDirectory(), srcFile.c_str());
      QFileInfo nfi(f.c_str());
      filePath << nfi.canonicalFilePath();
    }
    else
      filePath << src;

    mContext->getCarbonProjectWidget()->openSource(filePath.c_str(), sLineNumber.toInt(), "TextEditor");
  }
}

void HierarchyDockWindow::on_checkBoxEO_stateChanged(int state)
{
  mExplicitObserves = (state == Qt::Checked) ? true : false;
  ui.widgetTable->applyFilters(ui.lineEditFilter->text(), mExplicitObserves, mExplicitDeposits);
}

void HierarchyDockWindow::on_checkBoxED_stateChanged(int state)
{
  mExplicitDeposits = (state == Qt::Checked) ? true : false;
  ui.widgetTable->applyFilters(ui.lineEditFilter->text(), mExplicitObserves, mExplicitDeposits);
}

void HierarchyDockWindow::on_lineEditFilter_editingFinished()
{
  ui.widgetTable->applyFilters(ui.lineEditFilter->text(), mExplicitObserves, mExplicitDeposits);
}


void HierarchyDockWindow::setContext(HierarchyMode mode, CarbonMakerContext* ctx, CarbonDatabaseContext* dbContext)
{

  mContext = ctx;

  QApplication::setOverrideCursor(Qt::WaitCursor);
  if (dbContext->getDB())
  {
    mAppCtx.putDB(dbContext->getDB());
    mAppCtx.putCfg(dbContext->getCfg());
    mAppCtx.putCQt(ctx->getQtContext());

    bool loadDesign = ui.checkBoxLoadDesign->isChecked();
    if (theApp->checkArgument("-modelKit"))
      loadDesign = false;

    ui.widgetTree->putContext(&mAppCtx, loadDesign);
    ui.widgetTree->Initialize(mode);
    ui.widgetTree->setDragImmediate(true);

    ui.widgetTable->setDragImmediate(true);
    ui.widgetTable->putContext(&mAppCtx, ctx);
    ui.widgetTable->Initialize(mode);
    ui.widgetTable->Connect(ui.widgetTree);

    ui.widgetTree->setSelectionMode(QAbstractItemView::ExtendedSelection);
    ui.widgetTable->setSelectionMode(QAbstractItemView::ExtendedSelection);
    
    CQT_CONNECT(ui.widgetTable, selectionChanged(CarbonDBNode*), this, selectionChanged(CarbonDBNode*));
    CQT_CONNECT(ui.widgetTree, nodeSelectionChanged(CarbonDBNode*,XToplevelInterface*,XInstance*), this, moduleSelectionChanged(CarbonDBNode*,XToplevelInterface*,XInstance*));
    CQT_CONNECT(ui.widgetTree, nodeDoubleClicked(const char*,const char*,const char*), this, moduleDoubleClicked(const char*,const char*,const char*));

  }
  QApplication::restoreOverrideCursor();
}


void HierarchyDockWindow::on_checkBoxLoadDesign_stateChanged(int)
{
  if (ui.checkBoxLoadDesign->checkState() == Qt::Checked)
    ui.widgetTree->putContext(&mAppCtx, true);
  else
  {
    ui.widgetTree->putContext(&mAppCtx, false);
    emit netSelectionChanged("");
  }
}

