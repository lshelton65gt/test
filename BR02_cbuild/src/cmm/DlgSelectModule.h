#ifndef DLGSELECTMODULE_H
#define DLGSELECTMODULE_H


#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include <QDialog>

#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "DesignXML.h"

#include "ui_DlgSelectModule.h"

class DlgSelectModule : public QDialog
{
  Q_OBJECT

public:
  DlgSelectModule(QWidget *parent = 0, CarbonProjectWidget* pw = 0);
  ~DlgSelectModule();

  XInstance* getInstance() { return mInstance; }
  XToplevelInterface* getToplevelInterface() { return mToplevelInterface; }
  XDesignHierarchy* getHierarchy() { return mDesignHierarchy; }

private:
  void populate();
  void addInstance(QTreeWidgetItem* parent, XToplevelInterface* topi, XInstance* inst);
  void updateButtons();

private slots:
 void on_treeWidget_itemSelectionChanged();
 void on_buttonBox1_accepted();
 void on_buttonBox1_rejected();

private:
  XDesignHierarchy* mDesignHierarchy;
  XInstance* mInstance;
  XToplevelInterface* mToplevelInterface;
  CarbonProjectWidget* mProjectWidget;
  Ui::DlgSelectModuleClass ui;
};

#endif // DLGSELECTMODULE_H

