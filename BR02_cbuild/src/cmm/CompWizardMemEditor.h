#ifndef COMPWIZARDMEMEDITOR_H
#define COMPWIZARDMEMEDITOR_H

#include "util/CarbonPlatform.h"

#include <QtGui>

#include "ui_CompWizardMemEditor.h"

#include "gui/CQt.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/CarbonCfg.h"

#include "MemEditorTreeWidget.h"
#include "CarbonConsole.h"

class CarbonComponent;
class CarbonProjectWidget;
class CarbonProject;

class PortsRootItem;
class TransactionPortsRootItem;
class ParametersRootItem;
class ResetsRootItem;
class ClocksRootItem;
class TiesRootItem;
class DisconnectsRootItem;
class CompWizardMemEditor;
class MemEditorTreeWidget;
class MemoriesRootItem;
class MemoryDropItem;

class MemEditorDelegate : public QItemDelegate
{
  Q_OBJECT

public:
  MemEditorDelegate(QObject* parent=0, MemEditorTreeWidget* tw=0, CompWizardMemEditor* t=0);
  QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem&, const QModelIndex&) const;
  void setEditorData(QWidget* editor, const QModelIndex& index) const;
  void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;
  void commit(QWidget* w)
  {
    commitData(w);
    closeEditor(w);
  }

public slots:
  void currentIndexChanged(int);
  void valueChanged();

private:
  MemEditorTreeWidget* mTree;
  CompWizardMemEditor* mMemEditor;
};

class CompWizardMemEditor : public QWidget
{
  Q_OBJECT

public:
  CompWizardMemEditor(QWidget *parent = 0);
  ~CompWizardMemEditor();

  enum EditorMode {eModeSocDesigner, eModeCoWare, eModeSystemC };

  void setEditorMode(EditorMode mode);  
  EditorMode getEditorMode() { return mEditorMode; }

  void setCcfg(CarbonCfg* cfg);
  bool populate(CarbonDB* carbonDB, bool initPorts = false);
  void save();
  bool check(CarbonConsole*);
  bool autoCheckEnabled() 
  {
    return (ui.checkBoxAutoCheck->checkState() == Qt::Checked);
  }
  CarbonProject* getProject();
  CarbonProjectWidget* getProjectWidget(); 
  CarbonComponent* getComponent();

  MemoriesRootItem* getMemoriesRoot() { return mMemoriesRoot; }
  MemoryDropItem* getMemoryDropItem() { return mMemoryDropItem; }

  virtual void closeEvent(QCloseEvent* ev);

  friend class MemEditorDelegate;

  void createToolbar(QWidget* wiz);
  void createActions();
  void createMenus();
  void connectWidgets();
  void deleteCurrent();

private:
  void populateFromCcfg();
  void populateMemories();
  bool nodeIsDescendant(const QString& moduleName, const CarbonDBNode* node);
  UInt32 addAllMemoriesHelper(bool dry_run); 
  UInt32 findMemories(const CarbonDBNode* node, bool dry_run);
  QTreeWidgetItem* addMemoryNode(const CarbonDBNode* node); 
  QMap<QString,CarbonCfgMemory*> mMemoryNameMap;

public slots:
  void treeWidgetModified(bool value);
  void renameXtorInstance(const QString& oldName, const QString& newName);
  void deleteXtorInstance(const QString& name);

signals:
  void widgetModified(bool);


private slots:
  void modelCompilationFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void customContextMenuRequested(const QPoint& pos);
  void undo();
  void redo();
  void treeWidgetSelectionChanged();
  void canRedoChanged(bool enabled);
  void canUndoChanged(bool enabled);
  void actionAddMemory();
  void actionDelete();
  void actionAddSubComponent();

private:
  bool mUndoEnabled;
  bool mRedoEnabled;
  QUndoStack* mUndoStack;
  QUndoView* mUndoView;
  CarbonDB* mDB;
  QString mCcfgFilePath;
  CarbonCfg* mCfg;
  EditorMode mEditorMode;
  MemoriesRootItem* mMemoriesRoot;
  MemoryDropItem* mMemoryDropItem;
  QToolBar* mToolbar;

  // Actions
  QAction* mActionRedo;
  QAction* mActionUndo;
  QAction* mActionDelete;
  QAction* mActionAddMemory;
  QAction* mActionAddSubComponent;

private:
  Ui::CompWizardMemEditorClass ui;
};

#endif // COMPWIZARDMEMEDITOR_H
