//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "HierarchyWidget.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonProjectNodes.h"
#include "CarbonSourceGroups.h"
#include "CarbonHDLSourceFile.h"

#include "util/UtString.h"
#include "gui/CQt.h"

#include "util/RandomValGen.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "shell/carbon_misc.h"
#include "shell/carbon_capi.h"

HierarchyWidget::HierarchyWidget(QWidget *parent)
: QTreeWidget(parent)
{
  mContext = NULL;
}

HierarchyWidget::~HierarchyWidget()
{

}
 
void HierarchyWidget::populate()
{
  CarbonProject* proj = mContext->getCarbonProjectWidget()->project();
  const char* hfile = proj->getDesignFilePath(".hierarchy");
 
  QFileInfo fi(hfile);
  if (fi.exists())
  {
    const char* iodbName = proj->getDesignFilePath(".symtab.db");
    INFO_ASSERT(iodbName, "no db");
    CarbonDB* newDB = carbonDBOpenFile(iodbName);
    INFO_ASSERT(newDB, "no db load");
  }
}

void HierarchyWidget::putContext(CarbonMakerContext* ctx)
{
  mContext = ctx;
  populate();
}


