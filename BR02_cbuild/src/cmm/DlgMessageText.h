#ifndef DLGMESSAGETEXT_H
#define DLGMESSAGETEXT_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"


#include <QDialog>
#include "ui_DlgMessageText.h"

class DlgMessageText : public QDialog
{
  Q_OBJECT

public:
  void setTitle(const QString& title);
  void setMessage(const QString& msg);

  DlgMessageText(QWidget *parent = 0);
  ~DlgMessageText();

private:
  Ui::DlgMessageTextClass ui;

private slots:
    void on_pushButtonOK_clicked();
};

#endif // DLGMESSAGETEXT_H
