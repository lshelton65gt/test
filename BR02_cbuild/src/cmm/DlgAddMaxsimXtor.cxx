//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "DlgAddMaxsimXtor.h"
#include "DlgAdvancedMatching.h"
#include "carbon/carbon_dbapi.h"
#include "cfg/CarbonCfg.h"
#include "CcfgHelper.h"
#include <QtGui>


#define PROP_XTOR_PORT "CarbonCfgXtorPort*"

static bool typesMatch(CarbonCfgRTLPort* rtlPort, CarbonCfgXtorPort* xtorPort)
{
  bool typesMatch = false;
  if (rtlPort)
  {
    switch (rtlPort->getType())
    {
    case eCarbonCfgRTLInput:  
      typesMatch = xtorPort->getType() == eCarbonCfgRTLOutput; 
      break;
    case eCarbonCfgRTLOutput: 
      typesMatch = xtorPort->getType() == eCarbonCfgRTLInput; 
      break;
    case eCarbonCfgRTLInout:
      typesMatch = true;
      break;
    }
  }
  return typesMatch;
}

class XtorPortCombo : public QComboBox
{
public:
  XtorPortCombo(QTableWidget* tbl, QTableWidgetItem* matchItem, CarbonCfg* cfg, 
    CarbonCfgXtorPort* xtorPort, QWidget* parent=0) : QComboBox(parent)
  {
    mTableWidget = tbl;
    mMatchItem = matchItem;
    mCfg = cfg;
    QVariant vXtorPort = qVariantFromValue((void*)xtorPort);
    setProperty(PROP_XTOR_PORT, vXtorPort);
  }

  CarbonCfgXtorPort* getXtorPort() const
  {
    QVariant vXtorPort = property(PROP_XTOR_PORT);
    CarbonCfgXtorPort* xtorPort = (CarbonCfgXtorPort*)vXtorPort.value<void*>(); 
    return xtorPort;
  }

protected:
  virtual void showPopup()
  {
    QString currValue = currentText();
    // Clear all items
    clear();

    addItem("");

    CarbonCfgXtorPort* xtorPort = getXtorPort();

    // Find all RTL Ports which match type
    for (UInt32 i=0; i<mCfg->numRTLPorts(); i++)
    {
      CarbonCfgRTLPort* rtlPort = mCfg->getRTLPort(i);      
      if (
        typesMatch(rtlPort, xtorPort) 
        && !rtlPort->isTied() 
        && !rtlPort->isResetGen() 
        && !rtlPort->isClockGen()
        && !rtlPort->isTiedToParam()
        && !rtlPort->isConnectedToXtor()
        )
        addItem(rtlPort->getName());
    }

    QString potentialMatch = mMatchItem->text();
    
    if (!currValue.isEmpty())
      setCurrentIndex(findText(currValue, Qt::MatchExactly));
    else if (!potentialMatch.isEmpty())
      setCurrentIndex(findText(potentialMatch, Qt::MatchExactly));


    QComboBox::showPopup();
  }

private:
  QTableWidgetItem* mMatchItem;
  static QTableWidget* mTableWidget;
  static CarbonCfg* mCfg;
};

CarbonCfg* XtorPortCombo::mCfg = NULL;
QTableWidget* XtorPortCombo::mTableWidget = NULL;

DlgAddMaxsimXtor::DlgAddMaxsimXtor(CarbonCfg* cfg,
                                   const QString& xtorName, CarbonCfgXtorInstance* xtorInst, QWidget *parent)
: QDialog(parent)
{
  mAdvancedMatch = NULL;
  mCfg = cfg;
  mXtorInst = xtorInst;
  mXtorName = xtorName;
  ui.setupUi(this);

  QStringList items;
  for (CarbonCfg::XtorLibs::SortedLoop l = mCfg->loopXtorLibs(); !l.atEnd(); ++l) {
    const UtString& libName = l.getKey();
    CarbonCfgXtorLib* xlib = l.getValue();
    for (UInt32 i = 0, n = xlib->numXtors(); i < n; ++i) {
      CarbonCfgXtor* xtor = xlib->getXtor(i);
      QString thisXtorName;
      if (libName == CARBON_DEFAULT_XTOR_LIB) {
        thisXtorName = xtor->getName();
      } else {
        if (strlen(xtor->getVariant()) == 0) {
          thisXtorName = QString("%1/%2").arg(libName.c_str()).arg(xtor->getName());
        } else {
          thisXtorName = QString("%1/%2/%3").arg(libName.c_str()).arg(xtor->getName()).arg(xtor->getVariant());
        }
      }
      items.append(thisXtorName);

      if (thisXtorName == xtorName) {
        mXtor = xtor;
      }
    }
  }
  items.sort();

  // Build a map of names for speed
  for (UInt32 i=0; i<mCfg->numRTLPorts(); i++)
  {
    CarbonCfgRTLPort* rtlPort = mCfg->getRTLPort(i);
    QString rtlPortName = rtlPort->getName();
    mRTLPortMap[rtlPortName] = rtlPort;
    mRTLPortMapU[rtlPortName.toUpper()] = rtlPort;
    const CarbonDBNode* node = carbonDBFindNode(mCfg->getDB(), rtlPort->getName());
    if (node)
      mRTLLeafName[rtlPort->getName()] = carbonDBNodeGetLeafName(mCfg->getDB(), node);
  }

  ui.comboBoxXtors->addItems(items);
  ui.comboBoxXtors->setCurrentIndex(ui.comboBoxXtors->findText(xtorName));

  if (mXtorInst)
  {
    ui.comboBoxXtors->setEnabled(false);
    setWindowTitle("Edit Transactor Connections");
  }
}

DlgAddMaxsimXtor::~DlgAddMaxsimXtor()
{
}

QString DlgAddMaxsimXtor::getLeafName(const QString& rtlPortName)
{
  return mRTLLeafName[rtlPortName];
}

CarbonCfgRTLPort* DlgAddMaxsimXtor::findRTLPort(const QString& rtlPortName)
{
  return mRTLPortMap[rtlPortName];
}

CarbonCfgRTLPort* DlgAddMaxsimXtor::findRTLPortU(const QString& rtlPortName)
{
  return mRTLPortMapU[rtlPortName.toUpper()];
}


void DlgAddMaxsimXtor::on_buttonBox_rejected()
{
  reject();
}

void DlgAddMaxsimXtor::on_buttonBox_accepted()
{
  apply();
  accept();
}

void DlgAddMaxsimXtor::on_comboBoxXtors_currentIndexChanged(QString xtorNameParam)
{
  QString xtorName;
  QString libName;
  QString variant;

  QStringList parts = xtorNameParam.split("/");
  if (parts.size() > 2)
  {
    libName = parts[0];
    xtorName = parts[1];
    variant = parts[2];
  }
  else if (parts.size() > 1)
  {
    libName = parts[0];
    xtorName = parts[1];
  }
  else
  {
    libName = CARBON_DEFAULT_XTOR_LIB;
    xtorName = xtorNameParam;
  }

  mXtorName = xtorName;
  UtString n, l, v;
  l << libName;
  n << xtorName;
  v << variant;
  mXtor = mCfg->findXtor(n.c_str(), l.c_str(), v.c_str());
  populatePorts();
}

// Fill out the ports from the Xtor Names
void DlgAddMaxsimXtor::populatePorts()
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  CcfgHelper ccfg(mCfg);

  ui.tableWidget->setSortingEnabled(false);
  ui.tableWidget->clearContents();
  ui.tableWidget->setRowCount(0);

  if (mXtor)
  {
    for (UInt32 i=0; i<mXtor->numPorts(); i++)
    {
      CarbonCfgXtorPort* xtorPort = mXtor->getPort(i);
      int row = ui.tableWidget->rowCount();
      ui.tableWidget->setRowCount(row+1);

      QTableWidgetItem* matchItem = new QTableWidgetItem();
      ui.tableWidget->setItem(row, colMATCHES, matchItem);

      QTableWidgetItem* itmXtorName = new QTableWidgetItem(xtorPort->getName());
      ui.tableWidget->setItem(row, colPORTNAME, itmXtorName);
      XtorPortCombo* cbox = new XtorPortCombo(ui.tableWidget, matchItem, mCfg, xtorPort, ui.tableWidget);

      // Editing?
      if (mXtorInst)
      {
        CarbonCfgXtorConn* xtorConn = ccfg.findXtorPort(mXtorInst, xtorPort->getName());
        if (xtorConn && xtorConn->getRTLPort())
        {
          QString rtlName = xtorConn->getRTLPort()->getName();
          int index = cbox->findText(rtlName);
          if (index == -1)
            cbox->addItem(rtlName);
          cbox->setCurrentIndex(cbox->findText(rtlName));
        }
      }
      ui.tableWidget->setCellWidget(row, colRTLNAME, cbox);
    }
  }

  int cw = ui.tableWidget->columnWidth(colPORTNAME);

  ui.tableWidget->setColumnWidth(colRTLNAME, cw * 3);
  ui.tableWidget->setColumnWidth(colMATCHES, cw * 3);

  ui.tableWidget->horizontalHeader()->setResizeMode(colPORTNAME, QHeaderView::Interactive);
  ui.tableWidget->horizontalHeader()->setResizeMode(colRTLNAME, QHeaderView::Interactive);

  showPotentialMatches();

  ui.tableWidget->horizontalHeader()->setResizeMode(colMATCHES, QHeaderView::ResizeToContents);
  ui.tableWidget->horizontalHeader()->setResizeMode(colMATCHES, QHeaderView::Stretch);

  ui.tableWidget->setSortingEnabled(true);

  ui.tableWidget->sortItems(colPORTNAME);

  QApplication::restoreOverrideCursor();
}


void DlgAddMaxsimXtor::on_lineEditPrefix_textChanged(const QString &)
{
  showPotentialMatches();
}

void DlgAddMaxsimXtor::on_lineEditSuffix_textChanged(const QString &)
{
  showPotentialMatches();
}

void DlgAddMaxsimXtor::showPotentialMatches()
{
  bool useAdvancedMatch = mAdvancedMatch != NULL;
  if (useAdvancedMatch)
    ui.lineEditAdvanced->setText(mAdvancedMatch->patternText());
  else
    ui.lineEditAdvanced->setText("");

  QString prefix = ui.lineEditPrefix->text();
  QString suffix = ui.lineEditSuffix->text();

  qDebug() << "Potential Matches using prefix" << prefix << "suffix" << suffix;

  QApplication::setOverrideCursor(Qt::WaitCursor);
  
  QString topLevelModule = mCfg->getTopModuleName();

  QColor normalColor;

  for (int row=0; row<ui.tableWidget->rowCount(); row++)
  {
    if (row == 0) // Initialize color
      normalColor = ui.tableWidget->item(0,0)->textColor();

    XtorPortCombo* xtorCombo = dynamic_cast<XtorPortCombo*>(ui.tableWidget->cellWidget(row, colRTLNAME));
    if (xtorCombo == NULL)
      continue;

    CarbonCfgXtorPort* xtorPort = xtorCombo->getXtorPort();

    QTableWidgetItem* matchItem = ui.tableWidget->item(row, colMATCHES);

    QString currBinding = xtorCombo->currentText();

    if (!currBinding.isEmpty())
    {
      matchItem->setText("");
      continue;
    }
  
    QString xtorPortName = xtorPort->getName();
    QString computedXtorPortName = xtorPort->getName();

    CarbonCfgRTLPort* rtlPort = NULL;

    QString rtlName;
    if (useAdvancedMatch) // Scan through RTL names, and then match to Xtor port name
    {
      foreach(QString name, mRTLPortMap.keys())
      {
         // Abcs.xxxx_w_valid
         QString translatedName = mAdvancedMatch->ComputeMatch(name);        
          if (xtorPortName.toLower() == translatedName.toLower())
          {
            rtlPort = mRTLPortMap[name];
            break;
          }
        }     
    }
    else // Old style prefix/suffix turning the Xtor port name into an RTL name
    {
      rtlName = QString("%1.%2%3%4")
        .arg(topLevelModule)
        .arg(prefix)
        .arg(xtorPort->getName())
        .arg(suffix);
    
      rtlPort = findRTLPortU(rtlName);
      if (rtlPort == NULL) // Check to see if a "Starts with" match
      {
        foreach(QString name, mRTLPortMap.keys())
        {        
          if (name.toLower().contains(rtlName.toLower()))
          {
            rtlPort = mRTLPortMap[name];
            break;
          }
        }
      }
    }
  

    bool typesOK = typesMatch(rtlPort, xtorPort);

    bool isConnectable = true;
    if (rtlPort && rtlPort->isTied())
      isConnectable = false;

    if (rtlPort && typesOK && isConnectable)
    {
      matchItem->setText(rtlPort->getName());
      matchItem->setTextColor(normalColor);
    }
    else
    {
      QString reason;
      if (rtlPort == NULL)
        reason = "No such RTL Port";
      else if (!typesOK)
        reason = "Incompatible port directions";
      else if (!isConnectable)
        reason = "Port is tied";

      QString txt = QString("<%1> : %2")
                    .arg(rtlName)
                    .arg(reason);

      matchItem->setText(txt);
      matchItem->setTextColor(Qt::red);
    }
  }


  QApplication::restoreOverrideCursor();
}

void DlgAddMaxsimXtor::apply()
{
  qDebug() << "Apply Connections";
  for (int row=0; row<ui.tableWidget->rowCount(); row++)
  {
    XtorPortCombo* xtorCombo = dynamic_cast<XtorPortCombo*>(ui.tableWidget->cellWidget(row, colRTLNAME));
    if (xtorCombo == NULL)
      continue;

    QTableWidgetItem* matchItem = ui.tableWidget->item(row, colMATCHES);
    QString matchText = matchItem->text();

    // Respect the checkbox or if it's empty
    bool okToApply = !ui.checkBoxApplyEmpty->isChecked() || xtorCombo->currentText().isEmpty();

    if (okToApply && !matchText.isEmpty() && !matchText.startsWith("<"))
    {
      int index = xtorCombo->findText(matchText, Qt::MatchStartsWith);
      if (index == -1)
        xtorCombo->addItem(matchText);
      xtorCombo->setCurrentIndex(xtorCombo->findText(matchText));
    }
  }
}

void DlgAddMaxsimXtor::getMapping(quint32 index, CarbonCfgXtorPort** xtorPort, 
                                  CarbonCfgRTLPort** rtlPort)
{
  *rtlPort = NULL; // May not be mapped
  XtorPortCombo* xtorCombo = dynamic_cast<XtorPortCombo*>(ui.tableWidget->cellWidget(index, colRTLNAME));
  *xtorPort = xtorCombo->getXtorPort();
  *rtlPort = findRTLPort(xtorCombo->currentText());
}


void DlgAddMaxsimXtor::on_pushButtonAdvanced_clicked()
{
  if (mAdvancedMatch == NULL)
    mAdvancedMatch = new DlgAdvancedMatching(this);

  if (mAdvancedMatch->exec() == QDialog::Accepted)
  {
	showPotentialMatches();
  }
  else
  {
    ui.lineEditAdvanced->setText("");
    mAdvancedMatch = NULL;
  }
}

void DlgAddMaxsimXtor::reset()
{
  qDebug() << "Reset Connections";
  for (int row=0; row<ui.tableWidget->rowCount(); row++)
  {
    XtorPortCombo* xtorCombo = dynamic_cast<XtorPortCombo*>(ui.tableWidget->cellWidget(row, colRTLNAME));
    if (xtorCombo == NULL)
      continue;
    xtorCombo->setCurrentIndex(xtorCombo->findText(""));
  }
}

void DlgAddMaxsimXtor::on_buttonBox_clicked(QAbstractButton* button)
{
  if (button == ui.buttonBox->button(QDialogButtonBox::Apply))
    apply();
  else if (button == ui.buttonBox->button(QDialogButtonBox::Reset))
    reset();
}
