#ifndef DIRECTIVESEDITOR_H
#define DIRECTIVESEDITOR_H

#include "util/CarbonPlatform.h"

#include <QWidget>
#include <QObject>
#include "util/UtString.h"
#include "Mdi.h"
#include "util/OSWrapper.h"
#include "RemoteConsole.h"
#include "CarbonConsole.h"
#include "ui_DirectivesEditor.h"

class CarbonMakerContext;
class MDIDocumentTemplate;
class CarbonDirectiveGroup;
class TabNetDirectives;
class TabModuleDirectives;
class CarbonProject;

#include "CarbonTool.h"
#include "Directives.h"
#include "CarbonDirectives.h"
#include "CarbonProjectWidget.h"
#include "CarbonMakerContext.h"
#include "CarbonComponents.h"
#include "CowareComponent.h"
#include "CarbonOptions.h"
#include "SettingsEditor.h"
#include "TabNetDirectives.h"
#include "TabModuleDirectives.h"


class CarbonNetDirectivesTool : public QObject, public CarbonTool
{
  Q_OBJECT

public:
  CARBONMEM_OVERRIDES
  CarbonNetDirectivesTool(QObject* parent=0, CarbonDirectives* dirs=0, const char* nets=0);
  enum StateValue {Undefined=0, Set};

private:
  void processNets(const char* nets);

private slots:
  void directivesChanged(DirectiveType type);
  void netRenamed(DirectiveType, const char* oldName, const char* newName);

private:
  CarbonDirectives* mDirectives;
  UtString mNets;
};

class CarbonModuleDirectivesTool : public QObject, public CarbonTool
{
  Q_OBJECT

public:
  CARBONMEM_OVERRIDES
  CarbonModuleDirectivesTool(QObject* parent=0, CarbonDirectives* dirs=0, const char* nets=0);
  enum StateValue {Undefined=0, Set};

private:
  void processModules(const char* modules);

private slots:
  void directivesChanged(DirectiveType type);

private:
  CarbonDirectives* mDirectives;
  UtString mModules;
};


class DirectivesEditor : public QWidget, public MDIWidget
{
  Q_OBJECT

    enum Tabs { tabNETS=0, tabMODULES };

public:
  DirectivesEditor(CarbonMakerContext* ctx, MDIDocumentTemplate* t, QWidget *parent = 0);
  ~DirectivesEditor();
  virtual const char* userFriendlyName();
  virtual void saveDocument();
  virtual void setWindowModified(bool newVal);
  void setConfiguration(const char* name, const char* platform);
  TabNetDirectives* getNetDirectivesTab();
  TabModuleDirectives* getModuleDirectivesTab();
  void signalDirectivesChanged(DirectiveType type)
  {
    emit directivesChanged(type);
  }
  void modifyDirective(DesignDirective, const char*, bool value);
  CarbonMakerContext* getContext() { return mCtx; }

  void signalNetRenamed(DirectiveType type, const char* oldName, const char* newName)
  {
    emit netRenamed(type, oldName, newName);
  }

protected:
  virtual void closeEvent(QCloseEvent *event);

private:
  bool maybeSave();
  void populate();

public slots:
  void deleteDirective();
  void itemSelectionChanged();
  void save() { saveDocument(); }
  void newDirective();

private slots:
  void configurationChanged(const char*);
  void configurationRemoved(const char*);
  void projectClosing(CarbonProject*);
  void compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void applySignalDirective(DesignDirective, const char*, bool);

signals:
  void somethingSelected(bool);
  void directivesChanged(DirectiveType);
  void netRenamed(DirectiveType type, const char*, const char*);

private:
  Ui::DirectivesEditorClass ui;

  CarbonDirectiveGroup* mActiveGroup;
  CarbonMakerContext* mCtx;
};

#endif // DIRECTIVESEDITOR_H
