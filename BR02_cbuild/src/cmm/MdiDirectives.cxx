//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "MdiDirectives.h"
#include <QtGui>
#include <QMenuBar>
#include <QToolBar>
#include "gui/CQt.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "SettingsEditor.h"
#include "DirectivesEditor.h"

MDIDirectivesTemplate::MDIDirectivesTemplate(CarbonMakerContext* ctx)
: MDIDocumentTemplate("Directives", "Directives Component", "Carbon Directives (*.directives)") 
{
  mContext = ctx;  

  mActionDelete = new QAction(QIcon(":/cmm/Resources/DeleteHS.png"), tr("Delete"), this);
  mActionDelete->setStatusTip(tr("Delete Directive"));
  registerAction(mActionDelete, SLOT(deleteDirective()));
  registerUpdateAction(mActionDelete, SIGNAL(somethingSelected(bool)), SLOT(setEnabled(bool)));

  mActionNew = new QAction(QIcon(":/cmm/Resources/NewCardHS.png"), tr("New Directive"), this);
  mActionNew->setStatusTip(tr("New Directive"));
  registerAction(mActionNew, SLOT(newDirective()));

  mSaveAct = new QAction(QIcon(":/cmm/Resources/save.png"), tr("Sa&ve"), this);
  mSaveAct->setShortcut(tr("Ctrl+S"));
  mSaveAct->setStatusTip(tr("Save the directives to disk"));
  registerAction(mSaveAct, SLOT(save()));

}

void MDIDirectivesTemplate::updateMenusAndToolbars(QWidget* widget)
{
  DirectivesEditor* directivesEditor = dynamic_cast<DirectivesEditor*>(widget);
  directivesEditor->itemSelectionChanged();
}

int MDIDirectivesTemplate::createToolbars()
{
  QToolBar* toolbar = new QToolBar("Directives");
  toolbar->setObjectName("DirectivesToolbar");

  toolbar->addAction(mSaveAct);
  toolbar->addAction(mActionDelete);
  toolbar->addAction(mActionNew);

  addToolbar(toolbar);
  return numToolbars();}

int MDIDirectivesTemplate::createMenus()
{
  QMenu* fileMenu = new QMenu("&File");
  fileMenu->setObjectName("DirectivesEditorFileMenu");
  fileMenu->addAction(mSaveAct);

  addMenu(fileMenu);

  return numMenus();
}


MDIWidget* MDIDirectivesTemplate::createNewDocument(QWidget* /*parent*/)
{
  return NULL;
}

// We don't have a MDI document presence, only the docking window widget
// so we create the InvisibleProject here to handle menu & toolbar events
//
MDIWidget* MDIDirectivesTemplate::openDocument(QWidget* parent, const char* /*docName*/)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  DirectivesEditor* widget = new DirectivesEditor(mContext, this, parent);
  mContext->getCarbonProjectWidget()->putDirectivesEditor(widget);

  QApplication::restoreOverrideCursor();

  return widget;
}
