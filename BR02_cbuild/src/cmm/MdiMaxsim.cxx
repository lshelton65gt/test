//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "MdiMaxsim.h"
#include <QtGui>
#include <QMenuBar>
#include <QToolBar>
#include "gui/CQt.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "SettingsEditor.h"
#include "MaxsimWizardWidget.h"
#include "ModelStudioCommon.h"

MDIMaxsimTemplate::MDIMaxsimTemplate(CarbonMakerContext* ctx)
: MDIDocumentTemplate(MAXSIM_COMPONENT_NAME, "New Maxsim Component", "") 
{
  mContext = ctx; 

  mActionDelete = new QAction(QIcon(":/cmm/Resources/DeleteHS.png"), tr("Dele&te"), this);
  mActionDelete->setShortcut(tr("Ctrl+X"));
  mActionDelete->setStatusTip(tr("Delete the selected item"));
}

void MDIMaxsimTemplate::updateMenusAndToolbars(QWidget* /*widget*/)
{
}

int MDIMaxsimTemplate::createToolbars()
{
  QToolBar* toolbar = new QToolBar("SoC Designer Toolbar");
  toolbar->setObjectName("MaxSim Toolbar1");

  toolbar->addAction(mActionDelete);


  addToolbar(toolbar);
  return numToolbars();
}

int MDIMaxsimTemplate::createMenus()
{
  QMenu* fileMenu = new QMenu("&Edit");
  fileMenu->setObjectName("MaxsimEditMenu");
  fileMenu->addAction(mActionDelete);

  registerAction(mActionDelete, SLOT(actionDelete()));

  addMenu(fileMenu);

  return numMenus();
}

MDIWidget* MDIMaxsimTemplate::createNewDocument(QWidget* /*parent*/)
{
  return NULL;
}

// We don't have a MDI document presence, only the docking window widget
// so we create the InvisibleProject here to handle menu & toolbar events
//
MDIWidget* MDIMaxsimTemplate::openDocument(QWidget* parent, const char* docName)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  MaxsimWizardWidget* widget = new MaxsimWizardWidget(mContext, this, parent);

  bool status = widget->loadFile(docName);

  QApplication::restoreOverrideCursor();

  if (status)
    return widget;
  else
  {
    widget->close();
    return NULL;
  }
}
