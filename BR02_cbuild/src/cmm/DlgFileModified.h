#ifndef DLGFILEMODIFIED_H
#define DLGFILEMODIFIED_H

#include <QtGui>
#include "ui_DlgFileModified.h"

class DlgFileModified : public QDialog
{
  Q_OBJECT

public:
  DlgFileModified(QWidget *parent = 0);
  ~DlgFileModified();
  void setFileName(const QString& fileName)
  {
    ui.labelFileName->setText(fileName);
  }
  QDialogButtonBox::StandardButton buttonType()
  {
    return mReturnValue;
  }

  bool rememberPreference()
  {
    return ui.checkBoxRemember->isChecked();
  }
private:
  QDialogButtonBox::StandardButton mReturnValue;

  Ui::DlgFileModifiedClass ui;

private slots:
    void on_buttonBox_clicked(QAbstractButton*);
};

#endif // DLGFILEMODIFIED_H
