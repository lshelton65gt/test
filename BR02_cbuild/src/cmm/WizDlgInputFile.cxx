//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include "WizDlgInputFile.h"
#include "ModelKitWizard.h"

WizDlgInputFile::WizDlgInputFile(QWidget *parent, ModelKitWizard* wiz, KitActionInputFile* action)
: QDialog(parent)
{
  mWizard = wiz;
  mAction = action;

  ui.setupUi(this);

  if (action)
  {
	ui.lineEditQuestionID->setText(action->getID());
    ui.lineEditDescription->setText(action->getDescription());
    ui.lineEditDialogTitle->setText(action->getDialogTitle());
    ui.lineEditFileDestDir->setText(action->getDefaultDirectory());
    ui.lineEditFilename->setText(action->getDefaultFileName());
    ui.lineEditVariableName->setText(action->getVariableName());
    ui.lineEditFileTypes->setText(action->getFileTypes().join(";"));
    if (action->getChoice() == KitActionInputFile::File)
      ui.radioButtonPickFiles->setChecked(true);
    else
      ui.radioButtonPickDirectory->setChecked(true);
  }
}

WizDlgInputFile::~WizDlgInputFile()
{
}

void WizDlgInputFile::applyData(KitActionInputFile* action)
{
  action->setID(ui.lineEditQuestionID->text());
  action->setDialogTitle(ui.lineEditDialogTitle->text());
  action->setDefaultDirectory(ui.lineEditFileDestDir->text());
  action->setDefaultFileName(ui.lineEditFilename->text());
  action->setFileTypes(ui.lineEditFileTypes->text().split(";"));

  if (ui.radioButtonPickFiles->isChecked())
    action->setChoice(KitActionInputFile::File);
  else
    action->setChoice(KitActionInputFile::Directory);
  
  // Base Class
  action->setVariableName(ui.lineEditVariableName->text());
  action->setDescription(ui.lineEditDescription->text());

  qDebug() << action->getVariableName();
  qDebug() << action->getDescription();
}

KitUserAction* WizDlgInputFile::createAction()
{
  KitActionInputFile* action = new KitActionInputFile();
  applyData(action);
  return action;
}

void WizDlgInputFile::on_pushButtonBrowseFile_clicked()
{
  QString fileName = QFileDialog::getOpenFileName(this, "User Supplied File", ".", "Filename (* *.*)");
  if (!fileName.isEmpty())
  {
    QFileInfo fi(fileName);
    ui.lineEditFilename->setText(fi.fileName());
  }
}

void WizDlgInputFile::showError(const QString& msg)
{
  QMessageBox::critical(NULL, MODELSTUDIO_TITLE, msg);
}

void WizDlgInputFile::on_buttonBox_accepted()
{
  QString filename = ui.lineEditFilename->text();
  if (filename.isEmpty())
  {
    showError("Filename must not be empty.");
    ui.lineEditFilename->setFocus();
    return;
  }

  if (mAction)
    applyData(mAction);

  accept();
}

void WizDlgInputFile::on_buttonBox_rejected()
{
  reject();
}
