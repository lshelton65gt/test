#ifndef WIZPGMULTIPARAMMAP_H
#define WIZPGMULTIPARAMMAP_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "WizardPage.h"
#include "MemoryWizard.h"

#include <QWizardPage>
#include "ui_WizPgMultiParamMap.h"

class WizPgMultiParamMap : public WizardPage
{
  Q_OBJECT
  
  enum ParamMapColumns { colNAME=0, colFUNCTION};

public:
  WizPgMultiParamMap(QWidget *parent = 0);
  ~WizPgMultiParamMap();

protected:
  virtual void initializePage();
  virtual bool validatePage();
  virtual bool isComplete() const { return mIsValid; }
  virtual int nextId() const { return MemoryWizard::PageMultiGenerate; }

private:
  QSettings mSettings;
  bool mIsValid;

private:
  void readPreviousMappings();
  void populate();
  QMap<QString, MWParamMap*> mPreviousMappings;

private slots:
    void on_checkBoxRemember_stateChanged(int);

private:
  Ui::WizPgMultiParamMapClass ui;
};

#endif // WIZPGMULTIPARAMMAP_H
