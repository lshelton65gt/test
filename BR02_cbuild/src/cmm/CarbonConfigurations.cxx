//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "CarbonProject.h"
#include "CarbonCompilerTool.h"
#include "gui/CQt.h"
#include "util/OSWrapper.h"

#define XML_ENCODING "ISO-8859-1"
#define XML_SCHEMA_VERSION "1.0"


void CarbonConfigurations::removeConfiguration(const char* name)
{
  CarbonConfiguration* config = findConfiguration(name);
  if (config)
    mConfigs.remove(config);
}

CarbonConfigurations::CarbonConfigurations(CarbonProject* proj)
{
  mProject=proj;
}

CarbonConfigurations::~CarbonConfigurations()
{
  for(UInt32 i = 0; i < mConfigs.size(); ++i)
    delete mConfigs[i];
}

CarbonConfiguration* CarbonConfigurations::addConfig(const char* platform, const char* name, bool useCompilerSwitches)
{
  CarbonConfiguration* cfg = new CarbonConfiguration(mProject, name, platform, useCompilerSwitches);
  mConfigs.push_back(cfg);
  
  return cfg;
}

CarbonConfiguration* CarbonConfigurations::findConfiguration(const char* name)
{
  for (UInt32 i=0; i<mConfigs.size(); ++i)
  {
    CarbonConfiguration* config = mConfigs[i];
    if (0 == strcmp(config->getName(), name))
      return config;
  }
  return NULL;
}


bool CarbonConfigurations::deserialize(xmlNodePtr parent, UtXmlErrorHandler* eh)
{
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "Configuration"))
    {
      UtString configName;
      XmlParsing::getProp(child, "Name", &configName);
      QString cname = configName.c_str();
      QStringList configParts = cname.split('|');

      QString qname = configParts[0];
      QString qplat = configParts[1];

      UtString name;
      name << qname;
      UtString platform;
      platform << qplat;

      CarbonConfiguration* existingConfig = findConfiguration(name.c_str());
      if (existingConfig)
        existingConfig->deserialize(child, eh);
      else
      {
        CarbonConfiguration* cfg = addConfig(platform.c_str(), name.c_str());
        cfg->deserialize(child, eh);
      }
    }
  }
  return true;
}

bool CarbonConfigurations::serialize(xmlTextWriterPtr writer)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Configurations");

  for (UInt32 i=0; i<mConfigs.size(); ++i)
  {
    CarbonConfiguration* config = mConfigs[i];
    config->serialize(writer);
  }
  xmlTextWriterEndElement(writer);

  return true;
}

// Configurations


void CarbonConfiguration::putName(const char* newVal)
{
  mName=newVal;

  mOutputDir.clear();

  OSConstructFilePath(&mOutputDir, mProject->getProjectDirectory(), mProject->getActive()->getPlatform());
  OSConstructFilePath(&mOutputDir, mOutputDir.c_str(), newVal);

  QDir q;
  q.mkpath(mOutputDir.c_str());
}


CarbonConfiguration::CarbonConfiguration(CarbonProject* project, const char* name, const char* platform, bool useCompilerSwitches)
{
  mProject = project;

  mName = name;
  mPlatform = platform;

  if (useCompilerSwitches)
  {
    CarbonCompilerTool* compiler = new CarbonCompilerTool();
    mTools.push_back(compiler);
  }

  mOutputDir.clear();

  OSConstructFilePath(&mOutputDir, mProject->getProjectDirectory(), platform);
  OSConstructFilePath(&mOutputDir, mOutputDir.c_str(), name);

  QDir q;
  q.mkpath(mOutputDir.c_str());
}

CarbonConfiguration::~CarbonConfiguration()
{
  for(UInt32 i = 0; i < mTools.size(); ++i)
  {
    delete mTools[i];
  }
}

bool CarbonConfiguration::serialize(xmlTextWriterPtr writer)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Configuration");
  UtString configName;
  configName << getName() << "|" << getPlatform();

  xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST configName.c_str());

  for (UInt32 i=0; i<mTools.size(); ++i)
  {
    CarbonTool* tool = getTool(i);
    tool->serialize(writer);
  }

  xmlTextWriterEndElement(writer);

  return true;
}

bool CarbonConfiguration::deserialize(xmlNodePtr parent, UtXmlErrorHandler* eh)
{
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "Tool"))
    {
      bool foundTool = false;
      UtString toolName;
      XmlParsing::getProp(child, "Name", &toolName);
      for (UInt32 i=0; i<mTools.size(); ++i)
      {
        CarbonTool* tool = getTool(i);
        if (0 == strcmp(tool->getName(), toolName.c_str()))
        {
          tool->deserialize(child, eh);
          foundTool = true;
          break;
        }
      }
      INFO_ASSERT(foundTool, "Unable to find tool");
    }
  }
  return true;
}

CarbonOptions* CarbonConfiguration::getOptions(const char* toolName)
{
  for (UInt32 i=0; i<mTools.size(); ++i)
  {
    CarbonTool* tool = getTool(i);
    if (0 == strcmp(tool->getName(), toolName))
      return tool->getOptions();
  }

  INFO_ASSERT(false, "Expected to find named tool");
  return NULL;
}

void CarbonConfiguration::addTool(CarbonTool* tool)
{
  mTools.push_back(tool);
}


