#ifndef __ESLXTOR_H_
#define __ESLXTOR_H_

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "gui/CExprValidator.h"
#include "util/DynBitVector.h"
#include "CompWizardTreeNodes.h"
#include "PortEditorTreeWidget.h"
#include "cfg/CarbonCfg.h"
#include "cfg/carbon_cfg.h"
#include "PortEditorCommands.h"

#include <QtGui>

  // Handle multiple Xtor port actions
class ESLXtorPortActions : public QObject
{
  Q_OBJECT

public:
  enum Action { AddPortExpression }; 

  ESLXtorPortActions(PortEditorTreeWidget* tree);

  QAction* getAction(Action);

private slots:
  void actionAddPortExpresssion();
 
private:
  PortEditorTreeWidget* mTree;
  QAction* mAddPortExpr;
};

class ESLXtorParameterActions : public QObject
{
  Q_OBJECT

public:
  enum Action { ConnectRTL }; 

  ESLXtorParameterActions(PortEditorTreeWidget* tree);

  QAction* getAction(Action);

private slots:
  void actionConnectRTL();

private:
  PortEditorTreeWidget* mTree;
  QAction* mConnectRTL;
};

class ESLXtorInstanceParameterRTLPortItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colSIZE } ;

  virtual UndoData* deleteItem();

  CarbonCfgXtorParamInst* getParamInst() { return mXtorParamInst; }
  CarbonCfgRTLPort* getRTLPort() { return mRTLPort; }

  ESLXtorInstanceParameterRTLPortItem(QTreeWidgetItem* parent, 
    CarbonCfgXtorParamInst* xtorParamInst, CarbonCfgRTLPort* rtlPort)
    : PortEditorTreeItem(PortEditorTreeItem::XtorInstParamRTLPort, parent)
  {
    mXtorParamInst = xtorParamInst;
    mRTLPort = rtlPort;

    setText(colNAME, mRTLPort->getName());
    QString value = QString("%1").arg(xtorParamInst->getParam()->getSize());
    setText(colSIZE, value);

    setExpanded(true);
  }

protected:
  class Delete : public UndoData
  {
  public:
    CarbonCfg* mCfg;
    UInt32 mPortIndex;
    QString mXtorInstName;
    QString mXtorParamName;
    QString mRTLPortName;
    PortTreeIndex mESLPortIndex;
    QString mESLPortName;

  protected:
    void undo();
    void redo();
  };


private:
  CarbonCfgXtorParamInst* mXtorParamInst;
  CarbonCfgRTLPort* mRTLPort;
};

class ESLXtorInstanceParameterValueItem : public ParameterValueItem
{
public:
  enum Columns { colNAME = 0, colVALUE, colSIZE, colENUMCHOICES};

  virtual QString getDefaultValue() const { return mXtorParamInst->getDefaultValue(); }
  virtual CarbonCfgXtorParamInst* getParamInst() const { return mXtorParamInst; }

  ESLXtorInstanceParameterValueItem(QTreeWidgetItem* parent, CarbonCfgXtorParamInst* xtorParamInst)
    : ParameterValueItem(ParameterValueItem::eTypeXTOR_PARAM, parent)
  {
    mXtorParamInst = xtorParamInst;
    setItalicText(colNAME, "Value");
    QString value = xtorParamInst->getValue();

    if (xtorParamInst->getParam())
    {
      if (value.isEmpty())
        value = xtorParamInst->getDefaultValue();

      setValue(value);
      setItalicText(colSIZE, xtorParamInst->getParam()->getDescription());
      setText(colENUMCHOICES, xtorParamInst->getParam()->getEnumChoices()); // is this the correct column?
    }
    setFlags(flags() | Qt::ItemIsEditable);
  }
private: // Members
  CarbonCfgXtorParamInst* mXtorParamInst;
};

class ESLXtorInstanceParameterItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colTYPE } ;

  virtual void multiSelectContextMenu(QMenu&);

  CarbonCfgXtorParamInst* getParamInst() { return mXtorParamInst; }

  ESLXtorInstanceParameterItem(QTreeWidgetItem* parent, CarbonCfgXtorParamInst* xtorParamInst)
    : PortEditorTreeItem(PortEditorTreeItem::XtorInstParameter, parent)
  {
    mXtorParamInst = xtorParamInst;

    CarbonCfgXtorParam* xtorParam = mXtorParamInst->getParam();
    if (xtorParam)
    {
      CarbonCfgParamDataTypeIO paramType(xtorParam->getType());
      CarbonCfgParamFlagIO flag(xtorParam->getFlag());

      QString value = QString("(%1) %2")
        .arg(flag.getString())
        .arg(paramType.getString());

      setText(colTYPE, value);
      setText(colNAME, xtorParam->getName());
    }

    mParamValueItem = new ESLXtorInstanceParameterValueItem(this, xtorParamInst);

    for (UInt32 i=0; i<xtorParamInst->numRTLPorts(); i++)
    {
      CarbonCfgRTLPort* rtlPort = xtorParamInst->getRTLPort(i);
      new ESLXtorInstanceParameterRTLPortItem(this, xtorParamInst, rtlPort);
    }
  }

  static void resetData()
  {
    mActions = NULL;
  }

  ESLXtorParameterActions* getActions() 
  {
    if (mActions == NULL)
      mActions = new ESLXtorParameterActions(getTree());

    return mActions;
  }

  class ConnectRTL : public TreeUndoCommand
  {
  public:
    ConnectRTL(ESLXtorInstanceParameterItem* item, const QStringList& rtlPorts, QUndoCommand* parent = 0);

    CarbonCfg* mCfg;
    QStringList mRTLPorts;
    QString mXtorInstanceName;
    QList<PortTreeIndex> mPortIndexes;
    QMap<QString, QStringList> mRemovedESLPorts;

  protected:
    void undo();
    void redo();
  };
private:
  static ESLXtorParameterActions* mActions;

  ESLXtorInstanceParameterValueItem* mParamValueItem;
  CarbonCfgXtorParamInst* mXtorParamInst;
};

class ESLXtorInstanceResetByItem : public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE } ;

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  CarbonCfgXtorInstance* getXtorInstance() const { return mXtorInst; }

  void setESLResetMaster(const char* resetMaster)
  {
    if (resetMaster == NULL || strlen(resetMaster) == 0 || strcmp(resetMaster, "Undefined") == 0)
      setNormalText(colVALUE, "Undefined");
    else
      setBoldText(colVALUE, resetMaster);
  }

  ESLXtorInstanceResetByItem(QTreeWidgetItem* parent, CarbonCfgXtorInstance* xtorInst) 
    : PortEditorTreeItem(PortEditorTreeItem::XtorInstResetBy, parent)
  {
    mXtorInst = xtorInst;
    setItalicText(colNAME, "Reset By (active low)");

   
    const char* resetMaster = xtorInst->getESLResetMaster();
    setESLResetMaster(resetMaster);
 
  
    setFlags(flags() | Qt::ItemIsEditable);
  }

  class ChangeResetMasterCommand : public TreeUndoCommand
  {
  public:
    ChangeResetMasterCommand(ESLXtorInstanceResetByItem* item, 
      const QString& oldValue, const QString& newValue, QUndoCommand* parent = 0);

  protected:
    void undo();
    void redo();

  private:
    QString mXtorInstName;
    QString mOldValue;
    QString mNewValue;
    CarbonCfg* mCfg;
  };

private:
  CarbonCfgXtorInstance* mXtorInst;
};


// Abstraction
class ESLXtorInstanceAbstractionItem : public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE } ;

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  CarbonCfgXtorInstance* getXtorInstance() const { return mXtorInst; }

  void setAbstraction(const char* abstraction)  
  {
    if (abstraction == NULL || strlen(abstraction) == 0)
      setNormalText(colVALUE, "Undefined");
    else
      setBoldText(colVALUE, abstraction);
  }

  ESLXtorInstanceAbstractionItem(QTreeWidgetItem* parent, CarbonCfgXtorInstance* xtorInst) 
    : PortEditorTreeItem(PortEditorTreeItem::XtorInstAbstraction, parent)
  {
    mXtorInst = xtorInst;
    setItalicText(colNAME, "Abstraction");

    CarbonCfgXtorAbstraction* abstraction = xtorInst->getAbstraction();
    setAbstraction(abstraction->getName());
  
    setFlags(flags() | Qt::ItemIsEditable);
  }

  class ChangeAbstractionCommand : public TreeUndoCommand
  {
  public:
    ChangeAbstractionCommand(ESLXtorInstanceAbstractionItem* item, 
      const QString& oldValue, const QString& newValue, QUndoCommand* parent = 0);

  protected:
    void undo();
    void redo();

  private:
    QString mXtorInstName;
    QString mOldValue;
    QString mNewValue;
    CarbonCfg* mCfg;
  };

private:
  CarbonCfgXtorInstance* mXtorInst;
};




// Clocked By
class ESLXtorInstanceClockedByItem : public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colVALUE } ;

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);

  CarbonCfgXtorInstance* getXtorInstance() const { return mXtorInst; }


  void setClockMaster(CarbonCfgXtorInstance* clockMaster)
  {
    if (clockMaster == NULL)
      setNormalText(colVALUE, "Component");
    else
      setBoldText(colVALUE, clockMaster->getName());
  }

  void setESLClockMaster(const char* clockMaster)
  {
    if (clockMaster == NULL || strlen(clockMaster) == 0 || strcmp(clockMaster, "Undefined") == 0)
      setNormalText(colVALUE, "Undefined");
    else
      setBoldText(colVALUE, clockMaster);
  }

  ESLXtorInstanceClockedByItem(QTreeWidgetItem* parent, CarbonCfgXtorInstance* xtorInst) 
    : PortEditorTreeItem(PortEditorTreeItem::XtorInstClockedBy, parent)
  {
    mXtorInst = xtorInst;
    setItalicText(colNAME, "Clocked By");

   if (mXtorInst->getType()->useESLClockMaster())
   {
      const char* clockMaster = xtorInst->getESLClockMaster();
      setESLClockMaster(clockMaster);
   }
   else
   {
      CarbonCfgXtorInstance* clockMaster = xtorInst->getClockMaster();
      setClockMaster(clockMaster);
   }
  
    setFlags(flags() | Qt::ItemIsEditable);
  }

  class ChangeClockMasterCommand : public TreeUndoCommand
  {
  public:
    ChangeClockMasterCommand(ESLXtorInstanceClockedByItem* item, 
      const QString& oldValue, const QString& newValue, QUndoCommand* parent = 0);

  protected:
    void undo();
    void redo();

  private:
    QString mXtorInstName;
    QString mOldValue;
    QString mNewValue;
    CarbonCfg* mCfg;
  };

private:
  CarbonCfgXtorInstance* mXtorInst;
};



class ESLXtorInstanceParametersItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0 } ;
  virtual bool resizeOnExpand() const { return true; }
  virtual bool resizeOnCollapse() const { return true; }

  ESLXtorInstanceParametersItem(QTreeWidgetItem* parent, CarbonCfgXtorInstance* xtorInst) 
    : PortEditorTreeItem(PortEditorTreeItem::XtorInstParams, parent)
  {
    mXtorInst = xtorInst;
    setBoldText(colNAME, "Parameters");

    for (UInt32 i=0; i<mXtorInst->numParams(); i++)
    {
      // handle both hidden and non-hidden parameters - We don't
      // believe the concept of hidden parameters will apply to CMS/RTL
      // use cases - it is currently only a model kit feature.
      CarbonCfgXtorParamInst* paramInst = mXtorInst->getParamInstance(i, true);
      new ESLXtorInstanceParameterItem(this, paramInst);
    }
  }

private:
  CarbonCfgXtorInstance* mXtorInst;
};

class ESLXtorInstanceClocksResetsItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0 } ;
  virtual bool resizeOnExpand() const { return true; }
  virtual bool resizeOnCollapse() const { return true; }

  ESLXtorInstanceClocksResetsItem(QTreeWidgetItem* parent, CarbonCfgXtorInstance* xtorInst) 
    : PortEditorTreeItem(PortEditorTreeItem::XtorInstClocksResets, parent)
  {
    mXtorInst = xtorInst;
    setBoldText(colNAME, "Clocks / Resets");

    mResetBy = NULL;
    mAbstraction = NULL;

    mClockedBy = new ESLXtorInstanceClockedByItem(this, xtorInst);

    if (mXtorInst->getType()->useESLResetMaster())
      mResetBy = new ESLXtorInstanceResetByItem(this, xtorInst);

    if (mXtorInst->getType()->numAbstractions() > 0)
      mAbstraction = new ESLXtorInstanceAbstractionItem(this, xtorInst);
  }

  ESLXtorInstanceClockedByItem* getClockedBy() const { return mClockedBy; }
  ESLXtorInstanceResetByItem* getResetBy() const { return mResetBy; }
  ESLXtorInstanceAbstractionItem* getAbstraction() const { return mAbstraction; }

private:
  CarbonCfgXtorInstance* mXtorInst;
  ESLXtorInstanceClockedByItem* mClockedBy;
  ESLXtorInstanceResetByItem* mResetBy;
  ESLXtorInstanceAbstractionItem* mAbstraction;
};


class ESLXtorInstanceConnItem;

class ESLXtorInstanceConnExprItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colVALUE} ;

  ESLXtorInstanceConnExprItem(ESLXtorInstanceConnItem* parent, CarbonCfgXtorConn* xtorConn);
  
  // Overrides
  // Add Expression Context menu
  virtual void multiSelectContextMenu(QMenu&);
  virtual UndoData* deleteItem();
  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);
  
  // Methods
  void setPortExpression(const QString& newVal)
  {
    setText(colVALUE, newVal);
  }

  CarbonCfgXtorConn* getXtorConn() const { return mXtorConn; }

protected:
  class Delete : public UndoData
  {
  public:
    CarbonCfg* mCfg;
    QString mRTLPortName;
    QString mXtorInstName;
    QString mXtorPortName;
    QString mExpr;
    UInt32 mPortIndex;

  protected:
    void undo();
    void redo(); 
  };

  class CmdChangePortExpr : public TreeUndoCommand
  {
  public:
    CmdChangePortExpr(ESLXtorInstanceConnExprItem* item, 
      const QString& oldValue, const QString& newValue, QUndoCommand* parent = 0);

  protected:
    void undo();
    void redo();

  private:
    QString mRTLPortName;
    QString mXtorInstName;
    QString mXtorPortName;
    QString mOldValue;
    QString mNewValue;
    CarbonCfg* mCfg;
    UInt32 mPortIndex;
  };

private:
  ESLXtorInstanceConnItem* mXtorInstConnItem;
  CarbonCfgXtorConn* mXtorConn;
};

// Transactor connection to an RTL Value
class ESLXtorInstanceConnItem : public PortEditorTreeItem
{
public:
  enum Columns { colNAME = 0, colRTL, colSIZE, colMODE, colTYPE } ;
  static void resetData()
  {
    mActions = NULL;
  }
  void setConnection(CarbonCfgXtorConn* xtorConn);

  ESLXtorInstanceConnItem(ESLXtorInstanceItem* parent, CarbonCfgXtorConn* xtorConn);
  
  virtual void multiSelectContextMenu(QMenu&);
  virtual UndoData* deleteItem();
  void setPortExpression(const QString& expr);
  ESLXtorInstanceConnExprItem* getExprItem() { return mPortExpr; }
  CarbonCfgXtorConn* getXtorConn() const { return mXtorConn; }
  
  ESLXtorPortActions* getActions() 
  {
    if (mActions == NULL)
      mActions = new ESLXtorPortActions(getTree());

    return mActions;
  }

protected:
  class Delete : public UndoData
  {
  public:
    CarbonCfg* mCfg;
    UInt32 mPortIndex;
    QString mXtorInstName;
    QString mXtorPortName;
    QString mRTLPortName;
    QString mESLPortName;
    QString mExpr;
    PortTreeIndex mESLPortIndex;

  protected:
    void undo();
    void redo();
  };

  class AddExpression : public TreeUndoCommand
  {
  public:
    AddExpression(ESLXtorInstanceConnItem* item, const QString& expr, QUndoCommand* parent = 0);

    CarbonCfg* mCfg;
    UInt32 mPortIndex;
    QString mXtorInstName;
    QString mXtorPortName;
    QString mRTLPortName;
    QString mESLPortName;
    QString mExpr;
    bool mInteractive;

  protected:
    void undo();
    void redo();
  };
 
  friend class ESLXtorPortActions;

private:
  ESLXtorInstanceItem* mXtorInstItem;
  CarbonCfgXtorConn* mXtorConn;
  ESLXtorInstanceConnExprItem* mPortExpr;
  static ESLXtorPortActions* mActions;

};

// A transactor instance
class ESLXtorInstanceItem : public QObject, public PortEditorTreeItem
{
  Q_OBJECT

public:
  enum Columns { colNAME = 0, colTYPE } ;

  virtual bool resizeOnExpand() const { return true; }
  virtual bool resizeOnCollapse() const { return true; }
  
  virtual UndoData* deleteItem();

  virtual QWidget* createEditor(const PortEditorDelegate* delegate,
    QWidget* parent, int col);
  virtual void setModelData(const PortEditorDelegate* delegate, 
    QAbstractItemModel* model, const QModelIndex& modelIndex, 
    QWidget* editor, int colIndex);
  virtual void multiSelectContextMenu(QMenu& menu);
  virtual void doubleClicked();

  void addMenuItems(QMenu*);

  void setInstanceName(const QString& value)
  {
    setText(colNAME, value);
    setIcon(colNAME, QIcon(":/cmm/Resources/directives.png"));
    CarbonCfgXtor* xtor = mXtorInst->getType();
    if (xtor->isXtorClockMaster())
      setIcon(colNAME, QIcon(":/cmm/Resources/clock.png"));
  }

  CarbonCfgXtorInstance* getXtorInstance() const { return mXtorInst; }

  ESLXtorInstanceItem(QTreeWidgetItem* parent, CarbonCfgXtorInstance* xtorInst)
    : PortEditorTreeItem(PortEditorTreeItem::XtorInst, parent)
  {
    init(treeWidget());

    CQT_CONNECT(mActionBind, triggered(), this, actionBind());

    mXtorInst = xtorInst;
    mParametersRoot = NULL;
    mClocksResetRoot = new ESLXtorInstanceClocksResetsItem(this, xtorInst);
    mClocksResetRoot->setExpanded(false);
    getTree()->sortChildren(mClocksResetRoot);
    mClocksResetRoot->setExpanded(true);

    setInstanceName(xtorInst->getName());

    // For CoWare protocols can have variants (Master/Slave). Display
    // that so the user knows which variant was selected.
    CarbonCfgXtor* xtor = xtorInst->getType();
    QString xtorName;
    if (strlen(xtor->getVariant()) == 0) {
      xtorName = xtor->getName();
    } else {
      xtorName = QString("%1/%2").arg(xtor->getName()).arg(xtor->getVariant());
    }
    setText(colTYPE, xtorName);

    setFlags(flags() | Qt::ItemIsEditable);

    if (mXtorInst->numParams() > 0)
    {
      mParametersRoot = new ESLXtorInstanceParametersItem(this, xtorInst);
      
      mParametersRoot->setExpanded(false);
      getTree()->sortChildren(mParametersRoot);
      mParametersRoot->setExpanded(false);
    }

    // Now show the signals;
    for (UInt32 i=0; i<xtorInst->getType()->numPorts(); i++)
    {
      CarbonCfgXtorConn* xtorConn = xtorInst->getConnection(i);
      new ESLXtorInstanceConnItem(this, xtorConn);
    }

    getTree()->sortChildren(this);

    setExpanded(true);
  }

  ESLXtorInstanceClockedByItem* getClockedBy() const 
  {
    return mClocksResetRoot->getClockedBy();
  }

  ESLXtorInstanceResetByItem* getResetBy() const 
  {
    return mClocksResetRoot->getResetBy();
  }


 
public slots:
  void actionBind();

public:
  struct AddXtorMapping
  {
    CarbonCfgXtorPort* mXtorPort;
    CarbonCfgRTLPort* mRTLPort;
  };

  struct XtorMapping
  {
    XtorMapping()
    {
      mWasESLPort = false;
      mWasDisconnected = false;
    }
    QString mXtorPortName;
    QString mRTLPortName;
    QString mESLPortName;
    bool mWasESLPort;
    bool mWasDisconnected;
    PortTreeIndex mPortIndex;
    QList<CarbonCfgESLPort*> mESLPorts;
  };

public: 
 class AddXtorCommand : public TreeUndoCommand
  {
  public:
    AddXtorCommand(PortEditorTreeItem* parentItem, 
                    CarbonCfgXtor* xtor, QList<AddXtorMapping*>& mappings,
       QUndoCommand* parent = 0);

  protected:
    void undo();
    void redo();

  private:
    QString mXtorInstName;
    QString mXtorName;
    QString mXtorLibName;
    QString mXtorVariant;
    QList<XtorMapping*> mMappings;
    PortEditorTreeItem* mParentItem;
    CarbonCfg* mCfg;
  };

 class ChangeXtorCommand : public TreeUndoCommand
  {
  public:
    ChangeXtorCommand(ESLXtorInstanceItem* item, 
                    CarbonCfgXtorInstance* xtorInst, QList<AddXtorMapping*>& mappings,
       QUndoCommand* parent = 0);

  protected:
    void undo();
    void redo();
    CarbonCfgXtorConn* connectRTLPort(CarbonCfg* cfg, CarbonCfgXtorInstance* xtorInst, XtorMapping* mapping);
    CarbonCfgXtorConn* disconnectRTLPort(CarbonCfg* cfg, CarbonCfgXtorInstance* xtorInst, XtorMapping* mapping);
    void reconstructMapping(XtorMapping* mapping);

    void removeESLPort(CarbonCfg* cfg, XtorMapping* mapping);
    void removeESLPorts(CarbonCfg* cfg, XtorMapping* mapping, const QString& rtlPortName);

  private:
    QString mXtorInstName;
    QMap<QString, XtorMapping*> mNewMappings;
    QMap<QString, XtorMapping*> mOldMappings;
    CarbonCfg* mCfg;
  };

private:
  class DeleteConn : public UndoData
  {
  public:
    CarbonCfg* mCfg;
    QString mRTLPortName;
    QString mXtorPortName;
    QString mXtorInstName;
    QString mESLPortName;
    QString mExpr;
    UInt32 mPortIndex;
    PortTreeIndex mESLPortIndex;

    void undo();
    void redo();
    void disconnect();
  };

  class DeleteParam : public UndoData
  {
  public:
    CarbonCfg* mCfg;
    QString mParamName;
    QString mParamValue;
    QString mXtorInstName;
    void undo();
    void redo();
  };

  class Delete : public UndoData
  {
  public:
    CarbonCfg* mCfg;
    QString mXtorName;
    QString mXtorLibName;
    QString mXtorVariant;
    QString mXtorInstName;
    QString mClockMaster;
    QList<DeleteParam*> mParameters;
    QMap<QString, DeleteConn*> mConnections;
    QStringList mClockMasters;

  protected:
    void undo();
    void redo();
  };

private:
  void init(QObject* obj)
  {
    mActionBind = new QAction("Edit Connections...", obj);
  }

private:
  QAction* mActionBind;

private:
  ESLXtorInstanceParametersItem* mParametersRoot;
  ESLXtorInstanceClocksResetsItem* mClocksResetRoot;
  CarbonCfgXtorInstance* mXtorInst;
 
};

#endif
