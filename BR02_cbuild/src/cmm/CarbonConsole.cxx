//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#include "util/CarbonVersion.h"
#include "util/UtShellTok.h"
#include "util/CarbonVersion.h"
#include "shell/ReplaySystem.h"
#include "gui/CQt.h"

#include "util/UtString.h"

#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"

#include "RemoteConsole.h"
#include "CarbonProject.h"
#include "CarbonProjectWidget.h"
#include "CarbonOptions.h"
#include "DlgAuthenticate.h"
#include "WorkspaceModelMaker.h"
#include "CarbonMakerContext.h"
#include "ProcessTree.h"
#include "CMSCommon.h"
#include "CarbonFiles.h"
#include "JavaScriptAPIs.h"
#include "cmm.h"
#include "MaxsimComponent.h"

Q_DECLARE_METATYPE(CarbonConsole*)

void CarbonConsole::registerTypes(QScriptEngine* engine)
{
  // Register this type so objects can return this
  qScriptRegisterQObjectMetaType<CarbonConsole*>(engine);

  //Q_DECLARE_METATYPE(CarbonConsole::CommandType);
  qScriptRegisterEnumMetaType<CarbonConsole::CommandType>(engine); 
  //Q_DECLARE_METATYPE(CarbonConsole::CommandMode);
  qScriptRegisterEnumMetaType<CarbonConsole::CommandMode>(engine); 
  //Q_DECLARE_METATYPE(CarbonConsole::CommandFlags);
  qScriptRegisterEnumMetaType<CarbonConsole::CommandFlags>(engine); 

  QScriptValue consoleEnums = engine->newQMetaObject(&CarbonConsole::staticMetaObject);
  engine->globalObject().setProperty("CarbonConsole", consoleEnums);
}

void CarbonConsole::registerScriptTypes(QMap<QString, const QMetaObject*>& map)
{
  map["CarbonConsole"] = &CarbonConsole::staticMetaObject;
  map["CarbonConsole*"] = &CarbonConsole::staticMetaObject;
}

void CarbonConsole::registerIntellisense(JavaScriptAPIs* apis)
{
  apis->loadAPIForVariable("CarbonConsole", &CarbonConsole::staticMetaObject, true);
}

CarbonConsole::CarbonConsole(QWidget *parent)
: QWidget(parent)
{
  ui.setupUi(this);
  mExitCode = -1;
  mUseProject = true;
  mProjectWidget=NULL;
  mLocalCommandType = Compilation;
  mRemoteCommandType = Compilation;
  mStatusExp.setPattern("#\\s*([0-9]+)\\s+Elapsed: [0-9]+:[0-9]+:[0-9]+ Complete:\\s+([0-9]+)%");
  mInitialized = false;
  mVersionMatched = false;
  mVersionCheckCompleted = false;
  mLocalCommandFlags = Program;
  mRemoteCommandFlags = Program;
  mRemoteBatchMode = false;
  mLocalBatchMode = false;
  mCarbonModelWasCompiled = false;

  ui.textEdit->setReadOnly(true);

  // Hookup the Remote Console
  CQT_CONNECT(&mSSHConsole, processReadyForCommands(), this, remoteSendCommand());
  CQT_CONNECT(&mSSHConsole, sessionLoginStatus(const QString&), this, remoteSessionLoginStatus(const QString&));
  CQT_CONNECT(&mSSHConsole, processOutput(const QString&), this, remoteProcessOutput(const QString&));
  CQT_CONNECT(&mSSHConsole, commandCompleted(int), this, remoteCommandCompleted(int));
  CQT_CONNECT(&mSSHConsole, sessionStateChanged(SSHConsole::LoginState), this, remoteSessionStateChanged(SSHConsole::LoginState));
  CQT_CONNECT(&mSSHConsole, processTerminated(int), this, remoteProcessTerminated(int));

  // Hookup the Local Console
  CQT_CONNECT(&mShellConsole, processReadyForCommands(), this, localSendCommand());
  CQT_CONNECT(&mShellConsole, sessionLoginStatus(const QString&), this, localSessionLoginStatus(const QString&));
  CQT_CONNECT(&mShellConsole, processOutput(const QString&), this, localProcessOutput(const QString&));
  CQT_CONNECT(&mShellConsole, commandCompleted(int), this, localCommandCompleted(int));
  CQT_CONNECT(&mShellConsole, processTerminated(int), this, localProcessTerminated(int));

  CQT_CONNECT(ui.textEdit, mouseDoubleClicked(QMouseEvent*), this, mouseTextDoubleClicked(QMouseEvent*));

  CQT_CONNECT(this, commandCompleted(CarbonConsole::CommandMode,CarbonConsole::CommandType,int), this, commandWaitCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType, int));

  // Read the message filters
  readMessageDefinitions(":/cmm/Resources/CompilerMessages.xml");
}

void CarbonConsole::commandWaitCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType, int exitCode)
{
  mExitCode = exitCode;
  mCommandFinished = true;
}
void CarbonConsole::mouseTextDoubleClicked(QMouseEvent*)
{
  QTextCursor cursor = ui.textEdit->textCursor();
  QTextBlock b = cursor.block();

  for (int i=0; i<mExpressions.count(); i++)
  {
    UtString text;
    text << b.text();

    MessageExpression* exp = mExpressions[i];
    if (exp->process(text.c_str()))
    {
      ErrorMessage em(text.c_str(), exp);
      const char* src = em.getSource();
      int lineNumber = em.getLineNumber();
      if (src)
      {
        cursor.select(QTextCursor::LineUnderCursor);
        ui.textEdit->setTextCursor(cursor);
        UtString filePath;
        filePath << mProjectWidget->project()->getWindowsEquivalentPath(src);

        qDebug() << "navigate: " << src << " line: " << lineNumber << "path" << filePath.c_str();

        QFileInfo fi(filePath.c_str());
        if (fi.exists())
          mProjectWidget->openSource(filePath.c_str(), lineNumber, "TextEditor");
        else // Check Sub-dirs
        {
          MaxsimComponent* comp = mProjectWidget->getActiveMaxsimComponent();
          if (comp)
          {
            QString compDir = comp->getOutputDirectory();
            QString socdPath = QString("%1/%2").arg(compDir).arg(filePath.c_str());
            qDebug() << "testing" << socdPath;
            QFileInfo fi2(socdPath);
            if (fi2.exists() && fi2.isFile())
              mProjectWidget->openSourceFile(socdPath, lineNumber, "TextEditor");
          }
        }
      }
    }
  }
}

CarbonConsole::~CarbonConsole()
{
}

bool CarbonConsole::isRemoteShellConnected()
{
  return mSSHConsole.isSessionConnected();
}

void CarbonConsole::setShuttingDown()
{
  mShutdown = true;
  mSSHConsole.setShuttingDown();
}


int CarbonConsole::executeLocalWait(QString prog, QStringList args, QString workingDir)
{
  mShutdown = false;
  mCarbonModelWasCompiled = false;

  qDebug() << "ExecuteLocalCmd: " << prog << args << workingDir;

  // Set to the same value
  mLocalBatchMode = false;
  mRemoteBatchMode = false;
  // clear the output before a compilation
  QDockWidget* dw = mProjectWidget->context()->getWorkspaceModelMaker()->getRemoteConsoleDockWindow();
  dw->lower();
  dw->raise();
// Connect to the project, if needed
  connectProject();

  mLocalCommandType = Command;
  mLocalWorkingDir = workingDir;
  mLocalCommandFlags = Program;
  mLocalProgram = prog;
  mLocalProgramArgs = args;
  mLocalBatchMode = false;

  QStringList envVars;

  addSimulationVariables(mLocalCommandFlags, envVars);

  return mShellConsole.executeProgramWait(mProjectWidget->project(), mLocalWorkingDir, envVars, mLocalProgram, mLocalProgramArgs);
}


// Single entry point, dispatches to local or remote
// depending on mode
void CarbonConsole::executeCommand(CommandMode mode, CommandType ctype, QString prog, QStringList args, QString workingDir, CommandFlags flags, bool batchMode)
{
  mShutdown = false;
  
  mCarbonModelWasCompiled = false;

  qDebug() << "ExecuteCmd: " << prog << " " << args;
  qDebug() << "ExecuteCmd: " << workingDir;
  qDebug() << "ExecuteCmd: " << mode << " " << ctype << " " << flags << " " << batchMode;

  // Set to the same value
  mLocalBatchMode = batchMode;
  mRemoteBatchMode = batchMode;

  // clear the output before a compilation
  if (ctype == CarbonConsole::Compilation)
  {
    QDockWidget* dw = mProjectWidget->context()->getWorkspaceModelMaker()->getRemoteConsoleDockWindow();
    dw->lower();
    dw->raise();
  }

  // Connect to the project, if needed
  connectProject();

  if (mode == Local)
    executeLocalCommand(ctype, prog, args, workingDir, flags, batchMode);
  else
    executeRemoteCommand(ctype, prog, args, workingDir, flags, batchMode);
}

// Single entry point, dispatches to local or remote
// depending on mode
int CarbonConsole::executeCommandAndWait(CommandMode mode, CommandType ctype, QString prog, QStringList args, QString workingDir, CommandFlags flags, bool batchMode)
{
  mShutdown = false;
  mCarbonModelWasCompiled = false;
  mCommandFinished = false;
  mExitCode = -1;

  qDebug() << "ExecuteCmd: " << prog << " " << args;
  qDebug() << "ExecuteCmd: " << workingDir;
  qDebug() << "ExecuteCmd: " << mode << " " << ctype << " " << flags << " " << batchMode;

  // Set to the same value
  mLocalBatchMode = batchMode;
  mRemoteBatchMode = batchMode;

  // clear the output before a compilation
  if (ctype == CarbonConsole::Compilation)
  {
    QDockWidget* dw = mProjectWidget->context()->getWorkspaceModelMaker()->getRemoteConsoleDockWindow();
    dw->lower();
    dw->raise();
  }

  // Connect to the project, if needed
  connectProject();

  if (mode == Local)
    executeLocalCommand(ctype, prog, args, workingDir, flags, batchMode);
  else
    executeRemoteCommand(ctype, prog, args, workingDir, flags, batchMode);

  while (!mCommandFinished)
  {
    OSSleep(1);
    qApp->processEvents();
    qDebug() <<  "waiting for command to finish";
  }

  return mExitCode;
}


// Remote handlers
void CarbonConsole::remoteSessionLoginStatus(const QString& line)
{
  processOutput(Remote, mRemoteCommandType, line);
}
void CarbonConsole::remoteProcessOutput(const QString& line)
{
  processOutput(Remote, mRemoteCommandType, line);
}

void CarbonConsole::processBuffer(CommandType ctype, const char* buffer)
{
  QStringList lines = QString(buffer).split('\n');
  foreach (QString line, lines)
  {
    UtString outputLine;
    outputLine << line;
    processOutput(Local, ctype, outputLine.c_str());
  }
}

void CarbonConsole::localProcessTerminated(int exitCode)
{
  QApplication::restoreOverrideCursor();

  qDebug() << "local exit code" << exitCode;
  mExitCode = exitCode;
  mCommandFinished = true;

  if (!mShutdown)
  {
    if (mLocalCommandType == Compilation || mLocalCommandType == Clean)
    {
      emit compilationEnded(CarbonConsole::Local, mRemoteCommandType);
      emit makefileFinished(CarbonConsole::Local, mLocalCommandType);
    }
    
    if (mLocalCommandFlags == Simulate)
      emit simulationEnded(CarbonConsole::Local, mLocalCommandType);
  }

  emit processTerminated(CarbonConsole::Local, mLocalCommandType, exitCode);
}

void CarbonConsole::remoteProcessTerminated(int exitCode)
{
  qDebug() << "Exitcode: " << exitCode;

  mExitCode = exitCode;
  mCommandFinished = true;

  QApplication::restoreOverrideCursor();

  if (!mShutdown)
  {
    if (mRemoteCommandType == Compilation || mRemoteCommandType == Clean)
    {
      emit compilationEnded(CarbonConsole::Local, mRemoteCommandType);
      emit makefileFinished(CarbonConsole::Local, mRemoteCommandType);
    }

    if (mRemoteCommandFlags == Simulate)
      emit simulationEnded(CarbonConsole::Local, mRemoteCommandType);
  }

  emit processTerminated(CarbonConsole::Remote, mRemoteCommandType, exitCode);
}

void CarbonConsole::remoteCommandCompleted(int exitStatus)
{
  qDebug() << "REMOTE COMMAND FINISHED: " << exitStatus;
  if (exitStatus == 0)
  {
    // We don't want to compile windows until we have first executed any
    // post compilation scripts
    //if (mRemoteCommandType != Command && !mCarbonModelWasCompiled)
    //  executeWindowsMake();
  }
  else
  {
    UtString err;
    err << "Error: Makefile finished with exit status = " << exitStatus;
    appendLine(err.c_str());

    if (mRemoteCommandType == Compilation || mRemoteCommandType == Clean)
      emit makefileFinished(CarbonConsole::Local, mRemoteCommandType);
    
    if (mRemoteCommandFlags == Simulate)
      emit simulationEnded(CarbonConsole::Local, mRemoteCommandType);
  }

  emit commandCompleted(CarbonConsole::Remote, mRemoteCommandType, exitStatus);

  QApplication::restoreOverrideCursor();
}
void CarbonConsole::remoteSessionStateChanged(SSHConsole::LoginState state)
{
 switch (state)
  {
  default:
   break;
  case SSHConsole::LoggingIn:
    appendLine("Logging in...");
    break;
  case SSHConsole::RemoteVersionCheck:
    appendLine("Checking Carbon Version...");
    break;
  case SSHConsole::RemoteDirCheck:
    appendLine("Verifying Working Directory...");
    break;
  case SSHConsole::Authorizing:
  case SSHConsole::RetryPassword:
    appendLine("Authorizing");
    break;
  case SSHConsole::LoggedIn:
    appendLine("Logged in");
    break;
  case SSHConsole::LoggedOut:
    appendLine("Logged out");
    break;
  case SSHConsole::Terminated:
    appendLine("Process Terminated");
    emit makefileFinished(CarbonConsole::Local, mLocalCommandType);
    break;
  case SSHConsole::ShellStarted:
    appendLine("Shell Ready");
    break;
  case SSHConsole::FailedLogin:
    appendLine("Failed to log in");
    break;
  }
}
void CarbonConsole::remoteSendCommand()
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  QStringList envVars;
  addSimulationVariables(mRemoteCommandFlags, envVars);

  UtString cmdLine;
  cmdLine << mRemoteProgram;
  foreach (QString arg, mRemoteProgramArgs)
    cmdLine << " " << arg;

  appendLine(cmdLine.c_str());

  mSSHConsole.executeCommand(mProjectWidget->project(), mRemoteWorkingDir, envVars, mRemoteProgram, mRemoteProgramArgs, true);
}
void CarbonConsole::executeRemoteCommand(CommandType ctype, QString prog, QStringList args, QString workingDir, CommandFlags flags, bool batchMode)
{
  mRemoteCommandType = ctype;
  mRemoteWorkingDir = workingDir;
  mRemoteCommandFlags = flags;
  mRemoteProgram = prog;
  mRemoteProgramArgs = args;
  mRemoteBatchMode = batchMode;
  mCarbonModelWasCompiled = false;

  CarbonProject* proj = mProjectWidget->project();
  if (proj)
  {
    CarbonOptions* options = proj->getProjectOptions();

    CarbonPropertyValue* remoteServer = options->getValue("Remote Server");
    CarbonPropertyValue* remoteUsername = options->getValue("Remote Username");

    if (!mSSHConsole.isSessionConnected())
    {
      UtString startMsg;
      startMsg << "Initiating SSH connection: " << remoteUsername->getValue() << "@" << remoteServer->getValue();
      mProjectWidget->context()->getMainWindow()->statusBar()->showMessage(startMsg.c_str(), 2000);  

      mSSHConsole.startSession(proj, remoteServer->getValue(), remoteUsername->getValue());
    }
    else
      remoteSendCommand();
  }
  else
    INFO_ASSERT(false, "Remote not allowed without project");
}

void CarbonConsole::addSimulationVariables(CommandFlags flags, QStringList& envVars)
{
  if (flags == CarbonConsole::Simulate)
  {
    UtString carbonPid;
    carbonPid << CARBON_MODELSTUDIO_PID << "=" << OSGetPid();
    envVars.append(carbonPid.c_str());

    const char* simUI = mProjectWidget->project()->getProjectOptions()->getValue("Simulation User Interface")->getValue();
    if (simUI && 0 == strcmp(simUI, "true"))
    {
      UtString carbonSim;
      carbonSim << CARBON_MODELSTUDIO_SIM << "=true";
      envVars.append(carbonSim.c_str());
    }
  }
}

void CarbonConsole::executeLocalCommand(CommandType ctype, QString prog, QStringList args, QString workingDir, CommandFlags flags, bool batchMode)
{
  mLocalCommandType = ctype;
  mLocalWorkingDir = workingDir;
  mLocalCommandFlags = flags;

#if pfWINDOWS
  if (prog.contains(' ') || prog.contains('$') && !prog.startsWith('\"'))
	  prog = "\"" + prog + "\"";
#endif

  mLocalProgram = prog;
  mLocalProgramArgs = args;
  mLocalBatchMode = batchMode;
  mCarbonModelWasCompiled = false;

  QStringList envVars;

  addSimulationVariables(mLocalCommandFlags, envVars);

  localSendCommand();
}

void CarbonConsole::connectProject()
{
  if (!mInitialized)
  {
    CarbonProject* proj = mProjectWidget->project();
    if (proj)
    {
      CarbonOptions* options = proj->getProjectOptions();

      options->registerPropertyChanged("Remote Server", "remoteInfoChanged", this);
      options->registerPropertyChanged("Remote Username", "remoteInfoChanged", this);
      options->registerPropertyChanged("Remote Working Directory", "remoteInfoChanged", this);
      options->registerPropertyChanged("Remote Command Prefix", "remoteInfoChanged", this);

    }
    CQT_CONNECT(mProjectWidget, projectLoaded(const char*, CarbonProject*), this, projectLoaded(const char*, CarbonProject*));
    mInitialized = true;
  }
}

void CarbonConsole::localSessionLoginStatus(const QString&)
{
}
void CarbonConsole::localProcessOutput(const QString& line)
{
  processOutput(Local, mLocalCommandType, line);
}
void CarbonConsole::localCommandCompleted(int exitStatus)
{
  QApplication::restoreOverrideCursor();
  
  qDebug() << "LOCAL COMMAND FINISHED";

  if (exitStatus != 0)
  {
    UtString err;
    err << "Error: Makefile finished with exit status = " << exitStatus;
    appendLine(err.c_str());
    if (mSSHConsole.isSessionConnected())
      mSSHConsole.abortSession();

    if (mLocalCommandType == Compilation || mLocalCommandType == Clean)
      emit makefileFinished(CarbonConsole::Local, mLocalCommandType);
    
    if (mLocalCommandFlags == Simulate)
      emit simulationEnded(CarbonConsole::Local, mLocalCommandType);
  }

  emit commandCompleted(CarbonConsole::Local, mLocalCommandType, exitStatus);

}
void CarbonConsole::localSessionStateChanged(SSHConsole::LoginState)
{
}

void CarbonConsole::localSendCommand()
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  QStringList envVars;
  addSimulationVariables(mLocalCommandFlags, envVars);

  UtString cmdLine;
  cmdLine << mLocalProgram;
  foreach (QString arg, mLocalProgramArgs)
    cmdLine << " " << arg;

  // Unix doesn't echo the command, so we do here.
#if !pfWINDOWS
  appendLine(cmdLine.c_str());
#endif

  mShellConsole.executeProgram(mProjectWidget->project(), mLocalWorkingDir, envVars, mLocalProgram, mLocalProgramArgs);
}

void CarbonConsole::processOutput(CommandMode mode, CommandType ctype, const QString& line)
{
  UtString outputLine; outputLine << line;

  bool signalError = false;
  UtString errorString;

  QString wdir;
  if (mode == Remote)
    wdir = mRemoteWorkingDir;
  else
    wdir = mLocalWorkingDir;

  emit stdoutChanged(ctype, line, wdir);
  emit stdoutChanged(ctype, line);

  if (theApp->isBatchMode())
  {
    printf("%s\n", outputLine.c_str());
  }

  QRegExp finishedall("^CARBON MAKEFILE COMPLETED");
  QRegExp finishError("^make(.exe)?: \\*\\*\\* \\[(.*)\\] Error [0-9]+");

  if (finishedall.exactMatch(line) || finishError.exactMatch(line))
  {
    emit makefileFinished(mode, ctype);
    if (mProjectWidget)
    {
      cmm* appContext = mProjectWidget->context()->getModelStudio();
      if (appContext->isSquishMode())
        QMessageBox::information(mProjectWidget, MODELSTUDIO_TITLE,
          tr("Compilation Finished"), QMessageBox::Ok);
    }
  }
  
  QRegExp finishedmodel("^FINISHED MODEL COMPILATION");
  if (finishedmodel.exactMatch(line))
  {
    mCarbonModelWasCompiled = true;
    emit modelCompilationFinished(mode, ctype);
  }

  QRegExp finishedexplore("^CARBON FINISHED EXPLORE");
  if (finishedexplore.exactMatch(line))
    emit exploreFinished(mode, ctype);

  QRegExp exp0("^CARBON BEGIN COMPILATION");
  QRegExp exp0w("^CARBON BEGIN WINDOWS COMPILATION");

  if (exp0.exactMatch(line) || exp0w.exactMatch(line))
    emit compilationStarted(mode, ctype);

  QRegExp exp1("^CARBON BEGIN CLEAN");
  if (exp1.exactMatch(line))
    emit compilationStarted(mode, ctype);

  QRegExp exp2("^CARBON FINISHED COMPILATION");
  QRegExp exp2w("^CARBON FINISHED WINDOWS COMPILATION");
  if (exp2.exactMatch(line) || exp2w.exactMatch(line) || finishError.exactMatch(line))
     emit compilationEnded(mode, ctype);

  QRegExp exp3("^CARBON FINISHED CLEAN");
  if (exp3.exactMatch(line))
    emit compilationEnded(mode, ctype);

  QRegExp exp5("^CARBON BEGIN SIMULATION");
  if (exp5.exactMatch(line))
    emit simulationStarted(mode, ctype);

  QRegExp exp6("^CARBON FINISHED SIMULATION");
  if (exp6.exactMatch(line))
  {
    emit simulationEnded(mode, ctype);
    if (mProjectWidget)
    {
      cmm* appContext = mProjectWidget->context()->getModelStudio();
      if (appContext->isSquishMode())
        QMessageBox::information(mProjectWidget, MODELSTUDIO_TITLE,
          tr("Simulation Finished"), QMessageBox::Ok);
    }
  }

  if (!mStatusExp.exactMatch(line))
    appendLine(line);

   if (signalError)
    appendLine(errorString.c_str());
}

void CarbonConsole::appendLine(const QString& line)
{
  bool showBold = false;

  for (int i=0; i<mExpressions.count(); i++)
  {
    UtString text;
    text << line;
    MessageExpression* exp = mExpressions[i];
    if (exp->process(text.c_str()))
    {
      ErrorMessage em(text.c_str(), exp);
      if (exp->getType() == MessageExpression::SourceHyperlink)
        ui.textEdit->setTextColor(Qt::darkGreen);

      switch (exp->getSeverity())
      {
      default:
        break;
      case MessageExpression::Warning:
        showBold = true;
        ui.textEdit->setTextColor(Qt::blue);
        break;
      case MessageExpression::Error:
        showBold = true;
        ui.textEdit->setTextColor(Qt::darkRed);
        break;
      }

      if (exp->getType() == MessageExpression::MakefileError)
      {
        showBold = true;
        ui.textEdit->setTextColor(Qt::red);
      }

      break;
    }
  }
  
  if (showBold)
    ui.textEdit->setFontWeight(QFont::DemiBold);
  
  ui.textEdit->append(line);
  
  if (showBold)
    ui.textEdit->setFontWeight(QFont::Normal);

  ui.textEdit->setTextColor(Qt::black);
}

void CarbonConsole::stopCompilation()
{
  qDebug() << "Terminating compilation process";  

  mSSHConsole.abortSession();
  
  int localPid = mShellConsole.getPid();
  if (localPid != 0)
  {
#if pfWINDOWS
    mShellConsole.abortSession();
#else
    killProcessAndSubprocesses(localPid);
#endif
  }
}

void CarbonConsole::killProcessAndSubprocesses(int parentPid)
{
#if !pfWINDOWS
  bool finished = false;
  do
  {
    ProcessTree procTree(parentPid);
    const QList<int>& pidList =  procTree.childProcesses();
    finished = pidList.empty();

    foreach (int subPid, pidList)
    {
      qDebug() << "Killing subprocess: " << subPid;
      OSKillProcess(subPid, eKillAssasinate);
    }
  }
  while (!finished);

  qDebug() << "Killing main process: " << parentPid;
  OSKillProcess(parentPid, eKillAssasinate);
#endif

}

void CarbonConsole::projectLoaded(const char* /*projName*/, CarbonProject* proj)
{
  mProjectWidget = proj->getProjectWidget();
  mInitialized = false;
  qDebug() << "project loaded: " << proj->getVHMName();
  remoteInfoChanged(NULL,NULL);
}
void CarbonConsole::remoteInfoChanged(const CarbonProperty*, const char*)
{
  mVersionMatched = false;
  mVersionCheckCompleted = false;
  
  stopCompilation();
}
void CarbonConsole::configurationChanged(const char*)
{
  mVersionMatched = false;
  mVersionCheckCompleted = false;

  if (mSSHConsole.isSessionConnected())
   mSSHConsole.abortSession();
}

void CarbonConsole::closeEvent(QCloseEvent *event)
{
  qDebug() << "Close request";

  if (mSSHConsole.isProgramRunning() || mShellConsole.getPid() != 0)
  {
    QMessageBox::StandardButton ret;
    ret = QMessageBox::warning(this, MODELSTUDIO_TITLE,
      "A program is currently running.\nDo you want to stop the process and continue closing?",
      QMessageBox::Ok | QMessageBox::Cancel);
    if (ret == QMessageBox::Ok)
    {
      stopCompilation();    
      event->accept();
    }
    else
      event->ignore();
  }
  else
    event->accept();
}

void CarbonConsole::on_toolButtonClearAll_clicked()
{
  ui.textEdit->clear();
}

void CarbonConsole::executeWindowsMake()
{
  // we only do this on windows
#if pfWINDOWS
  CarbonConfiguration* activeConfig = mProjectWidget->project()->getActive();
  UtString workDir;
  bool batchMode = mLocalBatchMode || mRemoteBatchMode;

  if (batchMode)
  {
    QStringList args;
    int skipArgs = 0;
    foreach (QString arg, mRemoteProgramArgs)
    {
      if (skipArgs > 0)
      {
        skipArgs--;
        continue;
      }
    
      if (arg == "CARBON_REMOTE=1")
        continue;

      if (arg == "-C")
      {
        skipArgs++;
        continue;
      }

      args << arg;
    }
    qDebug() << args;
    mLocalWorkingDir = QString(mProjectWidget->project()->getProjectDirectory()) + '/' + activeConfig->getPlatform();
    executeLocalCommand(Compilation, "$(CARBON_HOME)/Win/bin/make.exe", args, mLocalWorkingDir, Program, mLocalBatchMode);
  }
  else
  {
    UtString configDir;
    OSConstructFilePath(&configDir, mProjectWidget->project()->getProjectDirectory(), activeConfig->getPlatform());
    OSConstructFilePath(&configDir, configDir.c_str(), activeConfig->getName());
    mLocalWorkingDir = configDir.c_str();
    QStringList args;
   
    QString makeCommand;
    switch (mRemoteCommandType)
    {
    case Clean:
      makeCommand = "clean";
      break;
    // The "Clean" part has already been done on Unix, so skip it
    case ReBuild:
    case Compilation:
      makeCommand = "build";
      break;
    case Package:
      makeCommand = "package";
      break;
    default:
    case Command:
    case Check:
    case BatchCompilation:
      makeCommand = "";
    }

    args << "-f" << "Makefile.carbon";

    if (makeCommand.length() > 0)
      args << makeCommand;

    executeLocalCommand(mRemoteCommandType, "$(CARBON_HOME)/Win/bin/make.exe", args, mLocalWorkingDir, Program, mLocalBatchMode);
  }
#endif
}
bool CarbonConsole::readMessageDefinitions(const char* xmlFile)
{
  bool status = false;

  QFile xf(xmlFile);
  if (xf.open(QFile::ReadOnly))
  {
    QTextStream ts(&xf);
    QString buffer = ts.readAll();
    UtString buf;
    buf << buffer;

    xmlDocPtr doc = xmlReadMemory(buf.c_str(), buffer.length(), xmlFile, NULL, 0);
    if (doc == NULL) 
    {
      UtString message;
      message << "Failed to parse file " <<  xmlFile;
    }
    else
    {
      UtXmlErrorHandler eh;
      xmlNode *rootNode = xmlDocGetRootElement(doc);
      INFO_ASSERT(xmlStrcmp(rootNode->name, BAD_CAST "Messages") == 0, "Expecting Messages");

      status = parseXML(rootNode, &eh);
      if (eh.getError())
        status = false;

      xmlFreeDoc(doc);
    }
  }
  return status;
}

bool CarbonConsole::parseXML(xmlNodePtr parent, UtXmlErrorHandler* /*eh*/)
{
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "Message"))
    {
      UtString filter;
      XmlParsing::getContent(child, &filter);
      MessageExpression* exp = MessageExpression::parseXML(child);
      if (exp)
        mExpressions.push_back(exp);
    }
  }

  return true;
}
