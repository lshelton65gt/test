//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "gui/CQt.h"

#include "DlgNewProject.h"
#include <QtGui>

#include "util/OSWrapper.h"
#include "CarbonProjectWidget.h"
#include "DlgMapDrives.h"
#include "CarbonMakerContext.h"
#include "cmm.h"

extern UInt32 replaceSubstring(UtString* str, const char* oldStr, const char* newStr);
 

DlgNewProject::DlgNewProject(QWidget *parent, CarbonProjectWidget* pw)
: QDialog(parent)
{
  mProjectWidget = pw;
  mKindEmpty = NULL;
  mKindFromExistingCmd = NULL;
  mKindFromExistingDB = NULL;
  mKindFromExistingCfgCoWare = NULL;
  mKindFromExistingCfgMaxsim = NULL;
  mKindFromModelKit = NULL;

  ui.setupUi(this);

#if pfWINDOWS
  mCompilation = Remote;
  populateMappedDrives();
#else
  ui.groupBox->setVisible(false);
  ui.listWidgetMappedDrives->setVisible(false);
  ui.lineRemoteCompilation->setVisible(false);
  mCompilation = Local;
#endif

  ui.listWidgetProjectTypes->setViewMode(QListView::IconMode);
  ui.listWidgetProjectTypes->setMaximumWidth(128);
  ui.listWidgetProjectTypes->setMinimumWidth(128);
  ui.listWidgetProjectTypes->setSpacing(12);
  ui.listWidgetProjectTypes->setIconSize(QSize(96,84));

  UtString defaultPath;

  if (pw->project() == NULL)
  {
#if pfWINDOWS
    CarbonMappedDrives mappedDrives;
    bool hasMappedDrives = mappedDrives.numMappings() > 0;
    if (hasMappedDrives)
      defaultPath << mappedDrives.getMapping(0)->mDriveLetter.c_str() << "\\Carbon\\Projects";
    else
      defaultPath = "";
#else
    defaultPath << (QDir::currentPath());
#endif
  }
  else
  {
    QFileInfo fi(pw->project()->getProjectDirectory());
    defaultPath << fi.absolutePath();
  }

  updateDefaultName(defaultPath.c_str());

  ui.lineEditProjectLocation->setText(defaultPath.c_str());

  CQT_CONNECT(ui.listWidgetProjectKind, itemSelectionChanged(), this, projectTypeChanged());
  CQT_CONNECT(ui.listWidgetMappedDrives, itemSelectionChanged(), this, mappedDriveChanged());

  createIcons();

  enableControls();

  QSettings settings("Carbon", "Drive Mappings");
  mUsername << settings.value("userName").toString();
  mServer << settings.value("serverName").toString();

  ui.listWidgetProjectKind->setFocus();

}

void DlgNewProject::mappedDriveChanged()
{
  QListWidgetItem* currItem = ui.listWidgetMappedDrives->currentItem();
  if (currItem)
  {
    QString driveLetter = currItem->text().left(2);
    ui.lineEditProjectLocation->setText(driveLetter + "\\");
  }
}

void DlgNewProject::populateMappedDrives()
{
  bool hasMappedDrives = false;
  // Don't emit signals when populating
  ui.listWidgetMappedDrives->blockSignals(true);

  ui.listWidgetMappedDrives->clear();
  CarbonMappedDrives mappedDrives;
  hasMappedDrives = mappedDrives.numMappings() > 0;

  for (int i=0; i<mappedDrives.numMappings(); i++)
  {
    CarbonMappedDrive* drive = mappedDrives.getMapping(i);
    UtString mapping;
    mapping << drive->mDriveLetter << " = " << drive->mUnixPath;
    ui.listWidgetMappedDrives->addItem(mapping.c_str());
  }

  // First one, so set it to something.
  if (!hasMappedDrives)
  {
    ui.listWidgetMappedDrives->setCurrentRow(0);
    mappedDriveChanged();
  }

  // Re-enable notifications
  ui.listWidgetMappedDrives->blockSignals(false);
}

void DlgNewProject::projectTypeChanged()
{
  foreach(QListWidgetItem* item, ui.listWidgetProjectKind->selectedItems())
  {
    QVariant vd = item->data(Qt::UserRole);
    mKind = static_cast<ProjectKind>(vd.toInt());
    switch (mKind)
    {
    case EmptyProject:
      ui.labelTip->setText("Empty Project");
      break;
    case FromExistingCommand:
      ui.labelTip->setText("Import a previously compiled Carbon Design");
      break;
    case FromExistingDatabase:
      ui.labelTip->setText("A project created from an existing .symtab.db file");
      break;
    case FromCcfgCoware:
      ui.labelTip->setText("A .ccfg which was created by Carbon's CoWare Wizard");
      break;
    case FromCcfgMaxsim:
      ui.labelTip->setText("A .ccfg which was created by Carbon's SoC Designer Wizard");
      break;
    case FromModelKit:
      ui.labelTip->setText("A .modelKit distribution");
      break;
    }
    break;
  }
}

void DlgNewProject::on_pushButtonMapDrives_clicked()
{
  CarbonMappedDrives mappedDrives;
  DlgMapDrives dlg(this, mProjectWidget, &mappedDrives);
  if (dlg.exec() == QDialog::Accepted)
  {
    mappedDrives.addMapping(dlg.getDriveLetter(), dlg.getUnixPath());
    mappedDrives.saveMappings();
    populateMappedDrives();
    enableControls();
    mUsername.clear();
    mUsername << dlg.getUsername();
    mServer.clear();
    mServer << dlg.getServer();
    if (ui.lineEditProjectLocation->text().length() == 0)
      ui.lineEditProjectLocation->setText(dlg.getDriveLetter() + "\\Carbon\\Projects");
  }
}

void DlgNewProject::on_checkBoxRemoteCompile_clicked()
{
  enableControls();
}

void DlgNewProject::createIcons()
{
  QListWidgetItem* carbonProj = new QListWidgetItem(ui.listWidgetProjectTypes);
  carbonProj->setIcon(QIcon(":/cmm/Resources/carbon.png"));
  carbonProj->setText("Carbon Project");
  carbonProj->setTextAlignment(Qt::AlignHCenter);
  carbonProj->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

  ui.listWidgetProjectTypes->setCurrentRow(0);

  bool ambaOnly = false;
#if !pfWINDOWS
  CarbonMakerContext* ctx = theApp->getContext();
  bool compilerLicense = ctx->getCanCarbonCompiler();
  bool ambaCompilerLicense = ctx->getCanAmbaCompiler();
  if (ambaCompilerLicense && !compilerLicense)
    ambaOnly = true;
#endif

  if (!ambaOnly)
  {
    mKindEmpty = new QListWidgetItem(ui.listWidgetProjectKind);
    mKindEmpty->setIcon(QIcon(":/cmm/Resources/carbon.png"));
    mKindEmpty->setText("Empty Project");
    mKindEmpty->setTextAlignment(Qt::AlignLeading);
    mKindEmpty->setData(Qt::UserRole, EmptyProject);

    mKindFromExistingCmd = new QListWidgetItem(ui.listWidgetProjectKind);
    mKindFromExistingCmd->setIcon(QIcon(":/cmm/Resources/carbon.png"));
    mKindFromExistingCmd->setText("Project from existing Carbon Command (.cmd) file");
    mKindFromExistingCmd->setTextAlignment(Qt::AlignLeading);
    mKindFromExistingCmd->setData(Qt::UserRole, FromExistingCommand);

    mKindFromExistingDB = new QListWidgetItem(ui.listWidgetProjectKind);
    mKindFromExistingDB->setIcon(QIcon(":/cmm/Resources/carbon.png"));
    mKindFromExistingDB->setText("Project from existing Carbon database (.symtab.db .io.db)");
    mKindFromExistingDB->setTextAlignment(Qt::AlignLeading);
    mKindFromExistingDB->setData(Qt::UserRole, FromExistingDatabase);

    mKindFromExistingCfgCoWare = new QListWidgetItem(ui.listWidgetProjectKind);
    mKindFromExistingCfgCoWare->setIcon(QIcon(":/cmm/Resources/carbon.png"));
    mKindFromExistingCfgCoWare->setText("Project from existing Carbon CoWare Wizard (.ccfg)");
    mKindFromExistingCfgCoWare->setTextAlignment(Qt::AlignLeading);
    mKindFromExistingCfgCoWare->setData(Qt::UserRole, FromCcfgCoware);

    mKindFromExistingCfgMaxsim = new QListWidgetItem(ui.listWidgetProjectKind);
    mKindFromExistingCfgMaxsim->setIcon(QIcon(":/cmm/Resources/carbon.png"));
    mKindFromExistingCfgMaxsim->setText("Project from existing Carbon SoC Designer Wizard (.ccfg)");
    mKindFromExistingCfgMaxsim->setTextAlignment(Qt::AlignLeading);
    mKindFromExistingCfgMaxsim->setData(Qt::UserRole, FromCcfgMaxsim);
  }

#if pfWINDOWS
  if (theApp->getContext()->getCanCreateModelKits()) // cds_internal
  {
#endif
    mKindFromModelKit = new QListWidgetItem(ui.listWidgetProjectKind);
    mKindFromModelKit->setIcon(QIcon(":/cmm/Resources/carbon.png"));
    mKindFromModelKit->setText("Model to be generated from a Carbon Model Kit (.modelKit)");
    mKindFromModelKit->setTextAlignment(Qt::AlignLeading);
    mKindFromModelKit->setData(Qt::UserRole, FromModelKit);

    if (ambaOnly)
    {
      mKindFromExistingDB = new QListWidgetItem(ui.listWidgetProjectKind);
      mKindFromExistingDB->setIcon(QIcon(":/cmm/Resources/carbon.png"));
      mKindFromExistingDB->setText("Project from existing Carbon database (.symtab.db .io.db)");
      mKindFromExistingDB->setTextAlignment(Qt::AlignLeading);
      mKindFromExistingDB->setData(Qt::UserRole, FromExistingDatabase);
    }

#if pfWINDOWS
  }
#endif

  ui.listWidgetProjectKind->setCurrentItem(mKindEmpty);
}

DlgNewProject::~DlgNewProject()
{
  
}

void DlgNewProject::enableControls()
{
  bool enabledFlag = false;

#if pfWINDOWS
  if (ui.checkBoxRemoteCompile->isChecked())
    mCompilation = Remote;
  else
    mCompilation = Local;
#else
    mCompilation = Local;
#endif

  QPushButton* okButton = ui.buttonBox->button(QDialogButtonBox::Ok);

  if (mCompilation == Remote)
  {
    if (ui.listWidgetMappedDrives->count() > 0)
      enabledFlag = true;
    else
      enabledFlag = false;

    ui.pushButtonMapDrives->setEnabled(true);
    ui.listWidgetMappedDrives->setEnabled(true);

    okButton->setEnabled(enabledFlag);
  }
  else
  {
    ui.pushButtonMapDrives->setEnabled(false);
    ui.listWidgetMappedDrives->setEnabled(false);

    enabledFlag = true;

    okButton->setEnabled(true);
  }

  ui.listWidgetProjectKind->setEnabled(enabledFlag);
  ui.listWidgetProjectTypes->setEnabled(enabledFlag);
  ui.labelTip->setEnabled(enabledFlag);
  ui.lineEditProjectName->setEnabled(enabledFlag);
  ui.checkBoxCreateDir->setEnabled(enabledFlag);
  ui.label->setEnabled(enabledFlag);
  ui.label_2->setEnabled(enabledFlag);
  ui.lineEditProjectLocation->setEnabled(enabledFlag);
  ui.pushButtonBrowse->setEnabled(enabledFlag);

#if pfWINDOWS
  bool choiceShowFlag = mCompilation == Remote && enabledFlag;
#else
  bool choiceShowFlag = true;
#endif

  // These depend on Remote vs. Local
  if (mKindEmpty)
    mKindEmpty->setHidden(!choiceShowFlag);

  if (mKindFromExistingCmd)
    mKindFromExistingCmd->setHidden(!choiceShowFlag);

  // These are shown only when:
  // Windows && Remote && Mapped Drives || Local
  bool enableOthers = mCompilation == Local || choiceShowFlag;

  if (mKindFromExistingDB)
    mKindFromExistingDB->setHidden(!enableOthers);

  if (mKindFromExistingCfgCoWare)
    mKindFromExistingCfgCoWare->setHidden(!enableOthers);

  if (mKindFromExistingCfgMaxsim)
    mKindFromExistingCfgMaxsim->setHidden(!enableOthers);
}

void DlgNewProject::updateDefaultName(const char* dirPath)
{
  int maxIndex = 0;

  QDir qdir(dirPath);

  if (qdir.exists())
  {
    qdir.setFilter(QDir::Dirs);
    QStringList filters;
    filters << "Project*";
    qdir.setNameFilters(filters);

    QStringList files = qdir.entryList();
    for (int i=0; i<files.count(); i++)
    {
      QRegExp exp1("Project([0-9]+)");
      QString dirName = files.at(i);
      if (exp1.exactMatch(files.at(i)))
      {
        QString value = exp1.cap(1);
        int v = value.toInt();
        if (v > maxIndex)
          maxIndex = v;
      }
    }
  }
  UtString projName;
  projName << "Project" << maxIndex+1;
  ui.lineEditProjectName->setText(projName.c_str());
}

void DlgNewProject::on_buttonBox_rejected()
{
  reject();
}

void DlgNewProject::on_pushButtonBrowse_clicked()
{
  QString directory = QFileDialog::getExistingDirectory(this,
                       tr("Select Project Directory"),
                       ui.lineEditProjectLocation->text(),
                       QFileDialog::DontResolveSymlinks | QFileDialog::ShowDirsOnly);
  if (!directory.isEmpty())
  {
    ui.lineEditProjectLocation->setText(directory);
    UtString dir;
    dir << directory;
    updateDefaultName(dir.c_str());
  }
}

void DlgNewProject::on_lineEditProjectName_textChanged(QString str)
{
  ui.buttonBox->button(QDialogButtonBox::Ok)->setEnabled(!str.isEmpty());
}

void DlgNewProject::on_buttonBox_accepted()
{
  bool isOK = true;

  QString name = ui.lineEditProjectName->text();

  if (name.contains(" ", Qt::CaseInsensitive))
  {
    isOK = false;
    UtString errMsg;
    errMsg << "Spaces are not allowed in the project name";
    QMessageBox::critical(this, MODELSTUDIO_TITLE, errMsg.c_str());
    return;
  }

  mProjectName.clear();
  mProjectName << ui.lineEditProjectName->text();
  
  // The list is single selection, so stop after we get the first one.
  foreach(QListWidgetItem* item, ui.listWidgetProjectKind->selectedItems())
  {
    QVariant vd = item->data(Qt::UserRole);
    mKind = static_cast<ProjectKind>(vd.toInt());
    break;
  }

  if (ui.checkBoxCreateDir->checkState() == Qt::Checked)
  {
    UtString pname;
    pname << ui.lineEditProjectName->text();
    
    UtString loc;
    loc << ui.lineEditProjectLocation->text();

    OSConstructFilePath(&mProjectDirectory, loc.c_str(), pname.c_str());
  }
  else
  {
    mProjectDirectory.clear();
    mProjectDirectory << ui.lineEditProjectLocation->text();
  }

  // on windows, make sure the drive letter is uppercase
  // and we don't use DOS style slashes
#ifdef Q_OS_WIN
  if (mProjectDirectory.length() > 1 && mProjectDirectory[1] == ':')
    mProjectDirectory[0] = toupper(mProjectDirectory[0]);
   replaceSubstring(&mProjectDirectory, "\\", "/");
#endif

  // Check if this is a remote project
  mCompilation = Local;
#if pfWINDOWS
  if (ui.checkBoxRemoteCompile->isChecked())
    mCompilation = Remote;
#endif

  QFileInfo projectDir(mProjectDirectory.c_str());

  // On windows, verify that the "Directory" is on a mapped drive
#if pfWINDOWS
  CarbonMappedDrives mappedDrives;

  QString driveLetter = projectDir.absoluteFilePath().left(2);

  CarbonMappedDrive* mappedDrive = mappedDrives.findMapping(driveLetter);
  if (mCompilation == Remote)
  {
    if (mappedDrive == NULL)
    {
      isOK = false;
      UtString errMsg;
      errMsg << "The Project directory:\n\n";
      errMsg << projectDir.absoluteFilePath() << "\n\n";
      errMsg << "Does not reside on one of your mapped drives, this is not allowed.\n";
      QMessageBox::critical(this, MODELSTUDIO_TITLE, errMsg.c_str());
      return;
    }
    else
    {
      mRemoteWorkingDir.clear();
      mRemoteWorkingDir << mappedDrive->mUnixPath;
      mRemoteWorkingDir << projectDir.absoluteFilePath().mid(2);
      qDebug() << "remote working dir: " << mRemoteWorkingDir.c_str();
    }
  }
  else
  {
    mRemoteWorkingDir.clear();
    mRemoteWorkingDir << projectDir.absoluteFilePath();
  }

  if (mCompilation == Local && mKind == EmptyProject)
  {
    UtString errMsg;
    errMsg << "You must choose a project type.";
    QMessageBox::critical(this, MODELSTUDIO_TITLE, errMsg.c_str());
    return;  
}

#endif
  // on all platforms
  mProjectDirectory.clear();
  mProjectDirectory << projectDir.absoluteFilePath();

  // Check to see if the project file already exists in that directory

  UtString projectFilePath;
  UtString projectCarbon;
  projectCarbon << mProjectName << ".carbon";

  OSConstructFilePath(&projectFilePath, mProjectDirectory.c_str(), projectCarbon.c_str());

  QFileInfo fiProject(projectFilePath.c_str());
  if (fiProject.exists())
  {
    UtString msg;
    msg << "An existing Carbon project has been found at:\n\n";
    msg << projectFilePath << "\n\n";
    msg << "Do you want to overwrite the existing project?";
    
    if (QMessageBox::question(this, MODELSTUDIO_TITLE, msg.c_str(), 
      QMessageBox::No|QMessageBox::Yes, QMessageBox::No) == QMessageBox::No)
      isOK = false;
  }

  // Check directory for writeability before proceeding
  QDir pd;
  if (!pd.mkpath(mProjectDirectory.c_str()))
  {
    UtString errMsg;
    errMsg << "Unable to create project directory:\n\n";
    errMsg << mProjectDirectory << "\n\n";
    errMsg << "Please verify that your network drive is mapped correctly and is writeable.\n";
    QMessageBox::critical(this, MODELSTUDIO_TITLE, errMsg.c_str());
    isOK = false;
  }


  if (isOK)
    accept();
}


void DlgNewProject::on_listWidgetProjectKind_itemDoubleClicked(QListWidgetItem*)
{
  on_buttonBox_accepted();
}
