#ifndef DLGSUBSMODULE_H
#define DLGSUBSMODULE_H

#include <QDialog>
#include "ui_DlgSubsModule.h"

class DlgSubsModule : public QDialog
{
  Q_OBJECT

public:
  enum SubstituteResult { eSubsAccepted, eSubsCancelled, eSubsDelete };

  DlgSubsModule(QWidget *parent = 0);
  ~DlgSubsModule();

  void setExpression(const QString& origExp);
  QString getExpression();
  SubstituteResult getResult() { return mResult; }

private slots:
  void on_pushButtonOK_clicked();
  void on_pushButtonCancel_clicked();
  void on_pushButtonDelete_clicked();

private:
  SubstituteResult mResult;
  Ui::DlgSubsModuleClass ui;
  QString mExpression;
};

#endif // DLGSUBSMODULE_H
