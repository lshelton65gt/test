//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

// this file supports Spirit_1.2, it probably should be called Spirit12.cxx 

#include "util/CarbonPlatform.h"

#include <QtGui>
#include "RegEditorTreeNodes.h"
#include "SpiritXML.h"
#include "JavaScriptAPIs.h"

#define CPREFIX "carbondesignsystems.com"
#define CURI "http://www.carbondesignsystems.com"

Q_SCRIPT_DECLARE_QMETAOBJECT(SpiritXML, QObject*)

void SpiritReset::serialize(xmlTextWriterPtr writer, XmlErrorHandler*)
{
  xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "reset", NULL);

  QString temp;
  QString value = temp.sprintf("0x%x", mValue);
  QString mask = temp.sprintf("0x%x", mMask);

  if (mHasValue)
    xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "value", NULL, BAD_CAST qPrintable(value));

  if (mHasMask)
    xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "mask", NULL, BAD_CAST qPrintable(mask));

  xmlTextWriterEndElement(writer);
}

void SpiritSignalName::deserialize(XmlErrorHandler*, const QDomElement& parent)
{
  mComponentSignalName = parent.firstChildElement("componentSignalName").text();
  mBusSignalName = parent.firstChildElement("busSignalName").text();
  mLeft = parent.firstChildElement("left").text();
  mRight = parent.firstChildElement("right").text();
}

void SpiritSignalMap::deserialize(XmlErrorHandler* eh, const QDomElement& parent)
{
  QDomElement e = parent.firstChildElement("signalName").toElement();
  while (!e.isNull())
  {
    SpiritSignalName* sn = new SpiritSignalName();
    sn->deserialize(eh, e);
    mSignalNames.append(sn);
    e = e.nextSiblingElement("signalName");
  }
}

void SpiritBusType::deserialize(XmlErrorHandler*, const QDomElement& parent)
{
  mVendor = parent.attribute("vendor");
  mName = parent.attribute("name");
  mLibrary = parent.attribute("library");
  mVersion = parent.attribute("version");
}

void SpiritBusType::serialize(xmlTextWriterPtr writer, XmlErrorHandler*)
{
  xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "busType", NULL);

  if (!getName().isEmpty())
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "name", NULL, BAD_CAST qPrintable(getName()));

  if (!getVendor().isEmpty())
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "vendor", NULL, BAD_CAST qPrintable(getVendor()));

  if (!getLibrary().isEmpty())
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "library", NULL, BAD_CAST qPrintable(getLibrary()));

  if (!getVersion().isEmpty())
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "version", NULL, BAD_CAST qPrintable(getVersion()));

  xmlTextWriterEndElement(writer);
}

SpiritReset* SpiritReset::deserialize(XmlErrorHandler*, const QDomElement& parent)
{
  SpiritReset* sr = new SpiritReset();

  QDomNode n = parent.firstChild();
  while(!n.isNull())
  {
    QDomElement e = n.toElement(); // try to convert the node to an element.
    qDebug() << "reset" << e.tagName();

    if (!e.isNull()) 
    {
      QString v = e.text();
      qDebug() << v;

      if ("value" == e.tagName())
        sr->setValue(v.toUInt(0, 0));
      else if ("mask" == e.tagName())
        sr->setMask(v.toUInt(0, 0));
    }  
    n = n.nextSibling();
  }

  return sr;
}

void SpiritBitOffset::serialize(xmlTextWriterPtr writer, XmlErrorHandler*)
{
  xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "bitOffset", NULL);

  if (!getId().isEmpty())
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "id", NULL, BAD_CAST qPrintable(getId()));
  
  if (getFormat() != SpiritEnum::UnsetFormat)
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "format", NULL, BAD_CAST qPrintable(formatToString(getFormat())));
  
  if (getResolve() != SpiritEnum::UnsetResolve)
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "resolve", NULL, BAD_CAST qPrintable(resolveToString(getResolve())));
  
  if (!getDependency().isEmpty())
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "dependency", NULL, BAD_CAST qPrintable(getDependency()));

  QVariant v(getBitOffset());
 
  xmlTextWriterWriteString(writer, BAD_CAST qPrintable(v.toString()));
  
  xmlTextWriterEndElement(writer);

}


SpiritBitOffset* SpiritBitOffset::deserialize(XmlErrorHandler*, const QDomElement& parent)
{
  SpiritBitOffset* bo = new SpiritBitOffset();

  qDebug() << parent.text();

  bo->setId(parent.attribute("id"));
  bo->setDependency(parent.attribute("dependency"));
  bo->setResolve(stringToResolve(parent.attribute("resolve")));
  bo->setFormat(stringToFormat(parent.attribute("format")));
  bo->setBitOffset(parent.text().toUInt(0,0));


  return bo;
}

void SpiritRange::serialize(xmlTextWriterPtr writer, XmlErrorHandler*)
{
  xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "range", NULL);

  if (!getId().isEmpty())
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "id", NULL, BAD_CAST qPrintable(getId()));
    
  if (getFormat() != SpiritEnum::UnsetFormat)
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "format", NULL, BAD_CAST qPrintable(formatToString(getFormat())));

  if (getResolve() != SpiritEnum::UnsetResolve)
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "resolve", NULL, BAD_CAST qPrintable(resolveToString(getResolve())));

  if (!getDependency().isEmpty())
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "dependency", NULL, BAD_CAST qPrintable(getDependency()));
 
  xmlTextWriterWriteString(writer, BAD_CAST qPrintable(getValue()));
  
  xmlTextWriterEndElement(writer);
}

SpiritRange* SpiritRange::deserialize(XmlErrorHandler*, const QDomElement &parent)
{
  SpiritRange* r = new SpiritRange();

  r->mResolve = stringToResolve(parent.attribute("resolve"));
  r->mId = parent.attribute("id");
  r->mDependency = parent.attribute("dependency");
  r->mValue = parent.text();
  r->mFormat = stringToFormat(parent.attribute("format"));

  return r;
}

void SpiritAddressBlock::serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh)
{
  xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "addressBlock", NULL);

  if (!getNameAttribute().isEmpty())
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "name", NULL, BAD_CAST qPrintable(getNameAttribute()));

  if (!getName().isEmpty())
    xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "name", NULL, BAD_CAST qPrintable(getName()));

  QVariant vw(mWidth);

  if (mBaseAddress)
    mBaseAddress->serialize(writer, eh);

  if (mBitOffset)
    mBitOffset->serialize(writer, eh);

  if (mRange)
    mRange->serialize(writer, eh);

  xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "width", NULL, BAD_CAST qPrintable(vw.toString()));

  if (getUsage() != SpiritEnum::UnsetUsage)
   xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "usage", NULL, BAD_CAST qPrintable(usageToString(getUsage())));

  if (getEndian() != SpiritEnum::UnsetEndian)
   xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "endianness", NULL, BAD_CAST qPrintable(endianToString(getEndian())));  

  if (getAccess() != SpiritEnum::UnsetAccess)
   xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "access", NULL, BAD_CAST qPrintable(accessToString(getAccess())));
  
  foreach (SpiritRegister* reg, mRegisters)
    reg->serialize(writer, eh);

  xmlTextWriterEndElement(writer);

}

SpiritAddressBlock* SpiritAddressBlock::deserialize(XmlErrorHandler* eh, const QDomElement &parent)
{
  SpiritAddressBlock* ab = new SpiritAddressBlock();

  ab->mNameAttribute = parent.attribute("name");

  QDomNode n = parent.firstChild();
  while(!n.isNull())
  {
    QDomElement e = n.toElement(); // try to convert the node to an element.
    qDebug() << "addressBlock" << e.tagName();
    if (!e.isNull()) 
    {
      if ("range" == e.tagName())
        ab->mRange = SpiritRange::deserialize(eh, e);
      else if ("name" == e.tagName())
        ab->mName = e.text();
      else if ("usage" == e.tagName())
        ab->setUsage(stringToUsage(e.text()));
      else if ("width" == e.tagName())
        ab->mWidth = e.text().toUInt(0,0);
      else if ("access" == e.tagName())
        ab->setAccess(stringToAccess(e.text()));
      else if ("bitOffset" == e.tagName())
        ab->mBitOffset = SpiritBitOffset::deserialize(eh, e);
      else if ("baseAddress" == e.tagName())
        ab->mBaseAddress = SpiritBaseAddress::deserialize(eh, e);
      else if ("endianness" == e.tagName())
        ab->setEndian(stringToEndian(e.text()));
      else if ("register" == e.tagName())
      {
        SpiritRegister* reg = SpiritRegister::deserialize(eh, e);
        reg->setAddressBlock(ab);
        ab->mRegisters.push_back(reg);
      }
    }  
    n = n.nextSibling();
  }

  return ab;
}

void SpiritBaseAddress::serialize(xmlTextWriterPtr writer, XmlErrorHandler*)
{
  xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "baseAddress", NULL);

  if (!getId().isEmpty())
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "id", NULL, BAD_CAST qPrintable(getId()));

  if (!getConfigGroups().isEmpty())
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "configGroups", NULL, BAD_CAST qPrintable(getConfigGroups()));

  if (!getName().isEmpty())
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "name", NULL, BAD_CAST qPrintable(getName()));

  if (getFormat() != SpiritEnum::UnsetFormat)
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "format", NULL, BAD_CAST qPrintable(formatToString(getFormat())));

  if (!getPrompt().isEmpty())
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "prompt", NULL, BAD_CAST qPrintable(getPrompt()));

  if (getResolve() != SpiritEnum::UnsetResolve)
    xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "resolve", NULL, BAD_CAST qPrintable(resolveToString(getResolve())));

  xmlTextWriterWriteString(writer, BAD_CAST qPrintable(getValue()));  

  xmlTextWriterEndElement(writer);
}
SpiritBaseAddress* SpiritBaseAddress::deserialize(XmlErrorHandler* ,const QDomElement &parent)
{
  SpiritBaseAddress* ba = new SpiritBaseAddress();
  ba->setId(parent.attribute("id"));
  ba->setConfigGroups(parent.attribute("configGroups"));
  ba->setName(parent.attribute("name"));
  ba->setPrompt(parent.attribute("prompt"));
  ba->setResolve(stringToResolve(parent.attribute("resolve")));
  ba->setFormat(stringToFormat(parent.attribute("format")));
  ba->setValue(parent.text());

  return ba;
}
void SpiritBank::serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh)
{
  xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "bank", NULL);

  xmlTextWriterWriteAttributeNS(writer, BAD_CAST "spirit", BAD_CAST "bankAlignment", NULL, BAD_CAST qPrintable(alignmentToString(getAlignment())));

  if (!getName().isEmpty())
    xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "name", NULL, BAD_CAST qPrintable(getName()));
  
  if (mBaseAddress)
    mBaseAddress->serialize(writer, eh);

  if (mBitOffset)
    mBitOffset->serialize(writer, eh);

  foreach (SpiritAddressBlock* block, mAddressBlocks)
    block->serialize(writer, eh);

  foreach (SpiritBank* bank, mBanks)
    bank->serialize(writer, eh);

  if (getUsage() != SpiritEnum::UnsetUsage)
   xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "usage", NULL, BAD_CAST qPrintable(usageToString(getUsage())));

  if (getAccess() != SpiritEnum::UnsetAccess)
   xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "access", NULL, BAD_CAST qPrintable(accessToString(getAccess())));

  xmlTextWriterEndElement(writer);
}

SpiritBank* SpiritBank::deserialize(XmlErrorHandler *eh, const QDomElement &parent)
{
  QDomNode n = parent.firstChild();
  SpiritBank* bank = new SpiritBank();

  bank->setAlignment(stringToAlignment(parent.attribute("bankAlignment")));
  
  while(!n.isNull())
  {
    QDomElement e = n.toElement(); // try to convert the node to an element.
    qDebug() << "bank" << e.tagName();
    if (!e.isNull()) 
    {
      if ("name" == e.tagName())
      {
        bank->mName = e.text();
      }
      else if ("baseAddress" == e.tagName())
      {
        bank->mBaseAddress = SpiritBaseAddress::deserialize(eh, e);
      }
      else if ("addressBlock" == e.tagName())
      {
        SpiritAddressBlock* ab = SpiritAddressBlock::deserialize(eh, e);
        ab->setParentBank(bank);
        bank->mAddressBlocks.push_back(ab);
        qDebug() << ab->getPath();
      }
      else if ("bitOffset" == e.tagName())
      {
        bank->mBitOffset = SpiritBitOffset::deserialize(eh, e);
      }
      else if ("bank" == e.tagName())
      {
        SpiritBank* childBank = SpiritBank::deserialize(eh, e);
        childBank->setParentBank(bank);
        bank->mBanks.push_back(childBank);
      }
      else if ("access" == e.tagName())
        bank->setAccess(stringToAccess(e.text()));
      else if ("usage" == e.tagName())
        bank->setUsage(stringToUsage(e.text()));
    }  
    n = n.nextSibling();
  }
  return bank;
}

bool SpiritField::checkForOverlap(SpiritField* field, QString& errMsg)
{
  int nFieldLeft = (int)field->getBitOffset()->getBitOffset();
  int nFieldRight = nFieldLeft + (int)field->getBitWidth() - 1;

  // Test this field for overlap
  SpiritRegister* reg = field->getRegister();
  for (UInt32 i=0; i<reg->numFields(); i++)
  {
    SpiritField* testField = reg->getField(i);
    if (testField != field)
    {
      int fieldWidth = (int)testField->getBitWidth();
      int nTestLeft = (int)testField->getBitOffset()->getBitOffset();
      int nTestRight = nTestLeft + fieldWidth - 1;
      if (! ((nFieldLeft < nTestLeft && nFieldRight < nTestRight) || (nFieldLeft > nTestLeft && nFieldRight > nTestRight)) )
      {
        errMsg = QString("Field %1 overlaps field %2")
          .arg(field->getName())
          .arg(testField->getName());

        return true;
      }
    }
  }
  return false;
}

// Check this field for any errors, returns true
// if errors are found
bool SpiritField::checkForProblems(CarbonDB* db, QString& errMsg)
{
  bool badField = false;
  SpiritRegister* reg = getRegister();

  if (getRTLPath().isEmpty())
  {
    badField = true;
    errMsg += QString("%1 is not bound to any RTL\n").arg(getName());
  }

  QString msg;
  if (checkForOverlap(this, msg))
  {
    errMsg += QString("%1\n").arg(msg);
    badField = true;
  }

  int fieldWidth = getBitWidth();
  UInt32 bitOffset = getBitOffset()->getBitOffset();

  const CarbonDBNode* node = getCarbonNode();
  if (node)
  {
    int nodeBitSize = carbonDBGetWidth(db, node);
    
    // Is it too small?
    if (fieldWidth > nodeBitSize)
    {
      errMsg += QString("Field %1 is smaller than the width of %2\n").arg(getRTLPath());    
      badField = true;
    // Part Select?
    }
    
    if (bitOffset + fieldWidth > reg->getSize())
    {
      errMsg += QString("Field %1 exceeds the size of the register\n").arg(getName());    
      badField = true;
    }

    if (nodeBitSize >= fieldWidth && getHasRange())
    {
      int partSelectWidth = 1 + std::abs((int)getPartSelectLeft()-(int)getPartSelectRight());
      if (partSelectWidth != fieldWidth)
      {
        badField = true;
        errMsg += QString("Field %1 PartSelect width of %2 is smaller than the width of %3\n")
          .arg(getName())
          .arg(partSelectWidth)
          .arg(fieldWidth);
      }
    }
  }
  
  return badField;
}

void SpiritField::serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh)
{
  xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "field", NULL);
  
  if (!getName().isEmpty())
    xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "name", NULL, BAD_CAST qPrintable(getName()));
  
  if (mBitOffset)
    mBitOffset->serialize(writer, eh);

  QVariant vw(mBitWidth);
  xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "bitWidth", NULL, BAD_CAST qPrintable(vw.toString()));

  xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "access", NULL, BAD_CAST qPrintable(accessToString(getAccess())));

  if (!getDescription().isEmpty())
    xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "description", NULL, BAD_CAST qPrintable(getDescription()));

  // start vendorExtensions
  xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "vendorExtensions", NULL);
  xmlTextWriterStartElementNS(writer, BAD_CAST CPREFIX, BAD_CAST "CarbonDesignSystems", BAD_CAST CURI);

  if (!getRTLPath().isEmpty())
  {
    xmlTextWriterStartElementNS(writer, BAD_CAST CPREFIX, BAD_CAST "rtlPath", NULL);
    xmlTextWriterWriteAttribute(writer, BAD_CAST "location", BAD_CAST qPrintable(locationToString(getLocation())));

    if (getLocation() == SpiritEnum::LocationArray)
    {
      UtString memIndex;
      memIndex << getMemoryIndex();
      xmlTextWriterWriteAttribute(writer, BAD_CAST "memoryIndex", BAD_CAST memIndex.c_str());
    }

    xmlTextWriterWriteString(writer, BAD_CAST qPrintable(getRTLPath()));
    xmlTextWriterEndElement(writer);
  }
 
  if (getHasRange())
  {
    UtString left;
    left << getPartSelectLeft();
   
    UtString right;
    right << getPartSelectRight();

    xmlTextWriterStartElementNS(writer, BAD_CAST CPREFIX, BAD_CAST "partSelect", NULL);
    xmlTextWriterWriteAttribute(writer, BAD_CAST "left", BAD_CAST left.c_str());
    xmlTextWriterWriteAttribute(writer, BAD_CAST "right", BAD_CAST right.c_str());
    xmlTextWriterEndElement(writer);
  }

  if (mValues.getNumValues() > 0)
    mValues.serialize(writer, eh);

  // end of CarbonDesignSystems
  xmlTextWriterEndElement(writer);

  // end of vendorExtensions
  xmlTextWriterEndElement(writer);

  // end of the field
  xmlTextWriterEndElement(writer);
}

SpiritField* SpiritField::deserialize(XmlErrorHandler* eh, const QDomElement& parent)
{
  SpiritField* field = new SpiritField();
  QDomNode n = parent.firstChild();
  while(!n.isNull())
  {
    QDomElement e = n.toElement(); // try to convert the node to an element.
    qDebug() << "field" << e.tagName();
    
    if (!e.isNull()) 
    {
      if ("name" == e.tagName())
        field->mName = e.text();
      else if ("description" == e.tagName())
        field->mDescription = e.text();
      else if ("bitOffset" == e.tagName())
        field->mBitOffset = SpiritBitOffset::deserialize(eh, e);
      else if ("bitWidth" == e.tagName())
        field->mBitWidth = e.text().toUInt(0,0);
      else if ("access" == e.tagName())
        field->mAccess = stringToAccess(e.text());
      else if ("values" == e.tagName())
        field->mValues.deserialize(eh, e);
      else if ("vendorExtensions" == e.tagName())
      {
        // Go through each one
        QDomNode carbonNode = e.firstChildElement("CarbonDesignSystems");
        QDomNode subNode = carbonNode.firstChild();
        while (!subNode.isNull())
        {
          QDomElement subElement = subNode.toElement(); // try to convert the node to an element.

          if (!subElement.isNull())
          {
            if ("rtlPath" == subElement.tagName())
            {
              field->setRTLPath(subElement.text());
              if (subElement.hasAttribute("location"))
                field->setLocation(stringToLocation(subElement.attribute("location")));
              if (subElement.hasAttribute("memoryIndex"))
                field->setMemoryIndex(subElement.attribute("memoryIndex").toUInt(0,0));
            }
            else if ("partSelect" == subElement.tagName())
            {
              field->setHasRange(true);
              field->setPartSelectLeft(subElement.attribute("left").toUInt(0,0));
              field->setPartSelectRight(subElement.attribute("right").toUInt(0,0));
            }
          }
          subNode = subNode.nextSibling();
        }
      }
    }

    n = n.nextSibling();
  }
  return field;
}


bool SpiritRegister::checkForProblems(CarbonDB* db, QString& errMessage)
{
  bool hasProblems = false;
  foreach (SpiritField* field, mFields)
  {
    QString fieldMsg;
    if (field->checkForProblems(db, fieldMsg))
    {
      hasProblems = true;
      errMessage += QString("%1\n").arg(fieldMsg);
    }
  }
  int unusedBits = numUnusedBits();

  if (unusedBits > 0)
  {
    errMessage += QString("Field has %1 unused bits").arg(unusedBits);
    hasProblems = true;
  }
  return hasProblems;
}

quint32 SpiritRegister::numUnusedBits()
{
  UInt32 regSize = getSize();

  // Count up fields
  UInt32 fieldsSize = 0;
  for (UInt32 i=0; i<numFields(); i++)
  {
    SpiritField* field = getField(i);
    UInt32 fieldSize = field->getBitWidth();
    fieldsSize += fieldSize;
  }
  
  quint32 unusedBits = std::abs((int)regSize-(int)fieldsSize);

  return unusedBits;
}


void SpiritRegister::serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh)
{
  xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "register", NULL);
  xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "name", NULL, BAD_CAST qPrintable(getName()));
  
  QString temp;
  QString addressOffset = temp.sprintf("0x%x", getAddressOffset());

  xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "addressOffset", NULL, BAD_CAST qPrintable(addressOffset));

  if (getDimension() != 0)
  {
    QVariant dimension(getDimension());
    xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "addressOffset", NULL, BAD_CAST qPrintable(dimension.toString()));
  }

  QVariant size(getSize());
  xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "size", NULL, BAD_CAST qPrintable(size.toString()));

  if (getAccess() != SpiritEnum::UnsetAccess)
    xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "access", NULL, BAD_CAST qPrintable(accessToString(getAccess())));

  if (mReset)
    mReset->serialize(writer, eh);

  foreach (SpiritField* field, mFields)
    field->serialize(writer, eh);

  xmlTextWriterEndElement(writer);

}

SpiritRegister* SpiritRegister::deserialize(XmlErrorHandler* eh, const QDomElement& parent)
{
  SpiritRegister* reg = new SpiritRegister();

  QDomNode n = parent.firstChild();
  while(!n.isNull())
  {
    QDomElement e = n.toElement(); // try to convert the node to an element.
    qDebug() << "register" << e.tagName();
    
    if (!e.isNull()) 
    {
      if ("name" == e.tagName())
        reg->mName = e.text();
      else if ("addressOffset" == e.tagName())
        reg->mAddressOffset = e.text().toUInt(0,0);
      else if ("size" == e.tagName())
        reg->mSize = e.text().toUInt(0,0);
      else if ("access" == e.tagName())
        reg->setAccess(stringToAccess(e.text()));
      else if ("reset" == e.tagName())
        reg->mReset = SpiritReset::deserialize(eh, e);
      else if ("field" == e.tagName())
      {
        SpiritField* field = SpiritField::deserialize(eh,e);
        field->setRegister(reg);
        reg->mFields.push_back(field);
      }
      else
        qDebug() << "register:unparsed tag" << e.tagName();
    }
    n = n.nextSibling();
  }
  return reg;
}

SpiritBusInterface* SpiritBusInterface::deserialize(XmlErrorHandler* eh, const QDomElement& parent)
{
  SpiritBusInterface* bi = new SpiritBusInterface();
  bi->mName = parent.firstChildElement("name").text();

  bi->mIsMaster = !parent.firstChildElement("master").isNull();
  bi->mIsSlave = !parent.firstChildElement("slave").isNull();
  bi->mIsSystem = !parent.firstChildElement("system").isNull();
  bi->mIsMirroredSlave = !parent.firstChildElement("mirroredSlave").isNull();
  bi->mIsMirroredMaster = !parent.firstChildElement("mirroredMaster").isNull();
  bi->mIsMirroredSystem = !parent.firstChildElement("mirroredSystem").isNull();
  bi->mIsExportedInterface = !parent.firstChildElement("exportedInterface").isNull();

  QDomElement busType = parent.firstChildElement("busType");
  if (!busType.isNull())
    bi->mBusType.deserialize(eh, busType);

  QDomElement signalMap = parent.firstChildElement("signalMap");
  if (!signalMap.isNull())
    bi->mSignalMap.deserialize(eh, signalMap);

  QDomElement vendorExtensions = parent.firstChildElement("vendorExtensions");
  if (!vendorExtensions.isNull())
  {
    QString buffer;
    QTextStream ts(&buffer);
    vendorExtensions.firstChild().save(ts, 2);
    bi->mVendorExtensions = buffer;
  }

  return bi;
}



SpiritMemoryMap* SpiritMemoryMap::deserialize(XmlErrorHandler* eh, const QDomElement& parent)
{
  QDomNode n = parent.firstChild();
  SpiritMemoryMap* smm = new SpiritMemoryMap();

  while(!n.isNull())
  {
    QDomElement e = n.toElement(); // try to convert the node to an element.
    qDebug() << "memoryMap" << e.tagName();
    if (!e.isNull()) 
    {
      if ("name" == e.tagName())
      {
        smm->mName = e.text();
      }
      else if ("bank" == e.tagName())
      {
        SpiritBank* bank = SpiritBank::deserialize(eh, e);
        bank->setParentMemoryMap(smm);
        smm->mBank = bank;
      }
      else if ("addressBlock" == e.tagName())
      {
        SpiritAddressBlock* ab = SpiritAddressBlock::deserialize(eh, e);
        ab->setParentMemoryMap(smm);
        smm->mAddressBlock = ab;
        qDebug() << ab->getPath();
      }
      else
        qDebug() << "memoryMap:unparsed tag" << e.tagName();
    }
    n = n.nextSibling();
  }

  return smm;
}


SpiritAddressBlock* SpiritXML::findBankAddressBlock(SpiritBank* parentBank, const QString& path)
{
  foreach (SpiritAddressBlock* ab, parentBank->getAddressBlocks())
  {
    if (ab->getPath() == path)
      return ab;
  }

  foreach (SpiritBank* bank, parentBank->getBanks())
  {
    SpiritAddressBlock* ab = findBankAddressBlock(bank, path);
    if (ab)
      return ab;
  }

  return NULL;
}

SpiritAddressBlock* SpiritXML::findAddressBlock(const QString& path)
{
  for (UInt32 i=0; i<mMemoryMaps.numItems(); i++)
  {
    SpiritMemoryMap* map = mMemoryMaps.getItem(i);
    SpiritAddressBlock* ab = map->getAddressBlock();
    SpiritBank* bank = map->getBank();
    if (ab && ab->getPath() == path)
      return ab;
    else if (bank)
      return findBankAddressBlock(bank, path);
  }

  return NULL;
}

QString SpiritAddressBlock::getPath()
{
  QString result;
  QStringList names;

  names.push_front(getResolvedName());
  bool hasParent = mParentBank || mParentMemoryMap;   
  SpiritBank* parentBank = mParentBank;
  SpiritMemoryMap* parentMemoryMap = mParentMemoryMap;

  while (hasParent)
  {
    if (parentBank)
    {
      names.push_front(parentBank->getName());
      parentMemoryMap = parentBank->getParentMemoryMap();
      parentBank = parentBank->getParentBank();
    }
    else if (mParentMemoryMap)
    {
      names.push_front(parentMemoryMap->getName());
      break;
    }
    hasParent = parentBank || parentMemoryMap;   
  }

  result = names.join("\\");

  return result;
}

void SpiritMemoryMap::serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh)
{
  xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "memoryMap", NULL);
  
  xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "name", NULL, BAD_CAST qPrintable(getName()));

  if (mBank)
    mBank->serialize(writer, eh);

  if (mAddressBlock)
    mAddressBlock->serialize(writer, eh);

  xmlTextWriterEndElement(writer);

}

void SpiritMemoryMaps::serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh)
{
  xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "memoryMaps", NULL);

  foreach (SpiritMemoryMap* mmap, mItems)
    mmap->serialize(writer, eh);

  xmlTextWriterEndElement(writer);
}

bool SpiritFile::deserialize(XmlErrorHandler*, SpiritXML*, const QDomElement& parent)
{
  mFileName = parent.firstChildElement("name").toElement().text().trimmed();
  mFileType = parent.firstChildElement("fileType").toElement().text();

  return true;
}

bool SpiritFileSet::deserialize(XmlErrorHandler* eh, SpiritXML* spirit, const QDomElement& parent)
{
  mFileSetId = parent.attribute("fileSetId");

  QDomElement e = parent.firstChildElement("file").toElement();
  while (!e.isNull())
  {
    SpiritFile* sf = new SpiritFile();
    sf->deserialize(eh, spirit, e);
    mFiles.append(sf);
    e = e.nextSiblingElement("file");
  }

  return true;
}

bool SpiritFileSets::deserialize(XmlErrorHandler* eh, SpiritXML* spirit, const QDomElement& parent)
{
  QDomElement e = parent.firstChildElement("fileSet").toElement();
  while (!e.isNull())
  {
    SpiritFileSet* fs = new SpiritFileSet();
    fs->deserialize(eh, spirit, e);
    mFileSets.append(fs);
    e = e.nextSiblingElement("fileSet");
  }

  return true;
}

bool SpiritFlags::deserialize(XmlErrorHandler*, SpiritXML*, const QDomElement& parent)
{
  QDomElement flags = parent.firstChildElement("flags").toElement();
  if (!flags.isNull())
  {
    mValue = flags.text();
    mResolve = stringToResolve(flags.attribute("resolve"));
    mID = flags.attribute("id");
    mDependency = flags.attribute("dependency");
    mMinimum = flags.attribute("minimum");
    mMaximum = flags.attribute("maximum");
    mRangeType = flags.attribute("rangeType");
  }

  return true;
}

bool SpiritDefaultFileBuilder::deserialize(XmlErrorHandler* eh, SpiritXML* spirit, const QDomElement& parent)
{
  QDomElement defaultFileBuilder = parent.firstChildElement("defaultFileBuilder").toElement();
  if (!defaultFileBuilder.isNull())
  {
    mFileType = defaultFileBuilder.firstChildElement("fileType").toElement().text();
    mFlags.deserialize(eh, spirit, defaultFileBuilder);
  }
  return true;
}

bool SpiritView::deserialize(XmlErrorHandler* eh, SpiritXML* spirit, const QDomElement& parent)
{
  mName = parent.firstChildElement("name").toElement().text();
  mEnvIdentifier = parent.firstChildElement("envIdentifier").toElement().text();
  mLanguage = parent.firstChildElement("language").toElement().text();
  mModelName = parent.firstChildElement("modelName").toElement().text();
  mFileSetRef = parent.firstChildElement("fileSetRef").toElement().text();
  mFileBuilder.deserialize(eh, spirit, parent);
  return true;
}


bool SpiritViews::deserialize(XmlErrorHandler* eh, SpiritXML* spirit, const QDomElement& parent)
{
  QDomElement e = parent.firstChildElement("view").toElement();
  while (!e.isNull())
  {
    SpiritView* sv = new SpiritView();
    sv->deserialize(eh, spirit, e);
    mViewList.append(sv);
    e = e.nextSiblingElement("view");
  }
 
  return true;
}

// Signals

bool SpiritExport::deserialize(XmlErrorHandler*, SpiritXML*, const QDomElement& parent)
{
  mResolve = SpiritEnum::UnsetResolve;

  QDomElement e = parent.firstChildElement("export").toElement();
  if (!e.isNull())
  {
    mID = e.attribute("id");
    mConfigGroups = e.attribute("configGroups");
    mPrompt = e.attribute("prompt");
    mResolve = stringToResolve(e.attribute("resolve"));
    mValue = e.text().toLower() == "true" ? true : false;
  }

  return true;
}

bool SpiritSignal::deserialize(XmlErrorHandler* eh, SpiritXML* spirit, const QDomElement& parent)
{
  mName = parent.firstChildElement("name").toElement().text();
  mLeft = parent.firstChildElement("left").toElement().text();
  mRight = parent.firstChildElement("right").toElement().text();
  mDirection = stringToDirection(parent.firstChildElement("direction").toElement().text());

  QDomElement dve = parent.firstChildElement("defaultValue");
  if (!dve.isNull())
    mDefaultValue = dve.firstChildElement("value").toElement().text();
  
  mExport.deserialize(eh, spirit, parent);
  
  return true;
}


bool SpiritSignals::deserialize(XmlErrorHandler* eh, SpiritXML* spirit, const QDomElement& parent)
{
  QDomElement e = parent.firstChildElement("signal").toElement();
  while (!e.isNull())
  {
    SpiritSignal* ss = new SpiritSignal();
    ss->deserialize(eh, spirit, e);
    mSignalList.append(ss);
    mSignalMap[ss->getName()] = ss;
    e = e.nextSiblingElement("signal");
  }
 
  return true;
}

bool SpiritModel::deserialize(XmlErrorHandler* eh, SpiritXML* spirit, const QDomElement& parent)
{
  QDomElement e1 = parent.firstChildElement("views").toElement();
  if (!e1.isNull())
    mViews.deserialize(eh, spirit, e1);
  

  QDomElement e2 = parent.firstChildElement("signals").toElement();
  if (!e2.isNull())
    mSignals.deserialize(eh, spirit, e2);
  
  return true;
}

bool SpiritBusInterfaces::deserialize(XmlErrorHandler* eh, SpiritXML*, const QDomElement& parent)
{
  QDomNode n = parent.firstChild();
  while(!n.isNull())
  {
    QDomElement e = n.toElement(); // try to convert the node to an element.
    qDebug() << "busInterfaces" << e.tagName();

    if (!e.isNull()) 
    {
      if ("busInterface" == e.tagName())
      {
        SpiritBusInterface* bi = SpiritBusInterface::deserialize(eh, e);
        if (bi)
          addItem(bi);
        else
          return false;
      }
      else if ("vendorExtensions" == e.tagName())
      {
        QString buffer;
        QTextStream ts(&buffer);
        n.firstChild().save(ts, 2);
        mVendorExtensions = buffer;
        qDebug() << buffer;
      }
    }
    n = n.nextSibling();
  }
  return true;
}

bool SpiritMemoryMaps::deserialize(XmlErrorHandler* eh, SpiritXML*, const QDomElement& parent)
{
  QDomNode n = parent.firstChild();
  while(!n.isNull())
  {
    QDomElement e = n.toElement(); // try to convert the node to an element.
    qDebug() << "memoryMaps" << e.tagName();

    if (!e.isNull()) 
    {
      if ("memoryMap" == e.tagName())
      {
        SpiritMemoryMap* map = SpiritMemoryMap::deserialize(eh, e);
        if (map)
          addItem(map);
        else
          return false;
      }
    }
    n = n.nextSibling();
  }
  return true;
}

// Read (parse) a Spirit XML file
XmlErrorHandler* SpiritXML::parseSpirtXML(const QString& xmlFile, bool scanForComponent)
{
  mParseEH.reset();

  QFile file(xmlFile);

  if (file.open(QFile::ReadOnly | QFile::Text))
  {
    QDomDocument doc;
    int errorLine;
    int errorCol;
    QString errStr;
    bool componentFound = false;

    QFileInfo templateFi(xmlFile);

    if (doc.setContent(&file, true, &errStr, &errorLine, &errorCol))
    { 
      QDomElement docElem = doc.documentElement();
      QDomNode n = docElem.firstChild();
   
      while(!n.isNull())
      {
        QDomElement e = n.toElement(); // try to convert the node to an element.

        if (!e.isNull()) 
        {
          if (scanForComponent && !componentFound && "component" == e.tagName())
          {
            componentFound = true;
            n = n.firstChild();
            continue;
          }

          if ("vendor" == e.tagName())
          {
            mVendor = e.text();
          }
          else if ("library" == e.tagName())
          {
            mLibrary = e.text();
          }
          else if ("name" == e.tagName())
          {
            mName = e.text();
          }
          else if ("version" == e.tagName())
          {
            mVersion = e.text();
          }
          else if ("fileSets" == e.tagName())
          {
            mFileSets.deserialize(&mParseEH, this, e);
          }
          else if ("model" == e.tagName())
          {
            mModel.deserialize(&mParseEH, this, e);
          }
          else if ("memoryMaps" == e.tagName())
          {
            if (!mMemoryMaps.deserialize(&mParseEH, this, e))
              break;
          }
           else if ("busInterfaces" == e.tagName())
          {
            if (!mBusInterfaces.deserialize(&mParseEH, this, e))
              break;
          }
          //else if (e.tagName().length() > 0)
          //{
          //  mParseEH.reportProblem(XmlErrorHandler::Error, e, 
          //      QString("Error: Unknown Tag: %1").arg(e.tagName()));
          //  return false;
          //}
        }
        n = n.nextSibling();
      }
    }
  }
  
  return &mParseEH;
}


// Helpers

SpiritEnum::SpiritDirection stringToDirection(const QString& v)
{
  QString value = v.toLower();
  if ("in" == value)
    return SpiritEnum::Input;
  else if ("out" == value)
    return SpiritEnum::Output;
  else if ("inout" == value)
    return SpiritEnum::Bidirect;

  return SpiritEnum::Input;
}

SpiritEnum::SpiritAccess stringToAccess(const QString& v)
{
  QString value = v.toLower();
  if ("read-write" == value)
    return SpiritEnum::ReadWrite;
  else if ("read-only" == value)
    return SpiritEnum::ReadOnly;
  else if ("write-only" == value)
    return SpiritEnum::WriteOnly;
  
  return SpiritEnum::UnsetAccess;
}

QString accessToString(SpiritEnum::SpiritAccess v)
{
  switch (v)
  {
  case SpiritEnum::ReadWrite: return "read-write";
  case SpiritEnum::ReadOnly: return "read-only";
  case SpiritEnum::WriteOnly: return "write-only";
  case SpiritEnum::UnsetAccess: return "";
  }

  return "Unknown";
}

SpiritEnum::SpiritFormat stringToFormat(const QString& v)
{
  QString value = v.toLower();
  if ("bitstring" == value)
    return SpiritEnum::BitString;
  else if ("bool" == value)
    return SpiritEnum::Boolean;
  else if ("float" == value)
    return SpiritEnum::Float;
  else if ("long" == value)
    return SpiritEnum::Long;
  else if ("string" == value)
    return SpiritEnum::String;
  
  return SpiritEnum::UnsetFormat;
}

SpiritEnum::SpiritResolve stringToResolveEnum(const QString& newVal)
{
  QString value = newVal.toLower();
  if ("immediate" == value)
    return SpiritEnum::Immediate;
  else if ("user" == value)
    return SpiritEnum::User;
  else if ("dependent" == value)
    return SpiritEnum::Dependent;
  else if ("generated" == value)
    return SpiritEnum::Generated;
  
  return SpiritEnum::UnsetResolve;
}

SpiritEnum::SpiritResolve stringToResolve(const QString& newVal)
{
  QString value = newVal.toLower();
  if ("immediate" == value)
    return SpiritEnum::Immediate;
  else if ("user" == value)
    return SpiritEnum::User;
  else if ("dependent" == value)
    return SpiritEnum::Dependent;
  else if ("generated" == value)
    return SpiritEnum::Generated;
  
  return SpiritEnum::UnsetResolve;
}

QString formatToString(SpiritEnum::SpiritFormat v)
{
  switch (v)
  {
  case SpiritEnum::BitString:
    return "bitstring";
  case SpiritEnum::Boolean:
    return "bool";
  case SpiritEnum::Float:
    return "float";
  case SpiritEnum::Long:
    return "long";
  case SpiritEnum::String:
    return "string";
  case SpiritEnum::UnsetFormat:
    return "";
  }
  return "Unknown";
}


QString directionToString(SpiritEnum::SpiritDirection v)
{
  switch (v)
  {
  case SpiritEnum::Input:
    return "in";
    break;
  case SpiritEnum::Output:
    return "out";
    break;
  case SpiritEnum::Bidirect:
    return "inout";
    break;
  }
  return "in";
}


QString resolveToString(SpiritEnum::SpiritResolve v)
{
  switch (v)
  {
  case SpiritEnum::Immediate:
    return "immediate";
    break;
  case SpiritEnum::User:
    return "user";
    break;
  case SpiritEnum::Dependent:
    return "dependent";
    break;
  case SpiritEnum::Generated:
    return "generated";
  case SpiritEnum::UnsetResolve:
    return "";
    break;
  }
  return "immediate";
}

SpiritEnum::SpiritUsage stringToUsage(const QString& v)
{
  QString value = v.toLower();
  if ("memory" == v)
    return SpiritEnum::Memory;
  else if ("register" == v)
    return SpiritEnum::Register;
  else if ("reserved" == v)
    return SpiritEnum::Reserved;

  return SpiritEnum::UnsetUsage;
}

QString usageToString(SpiritEnum::SpiritUsage v)
{
  switch (v)
  {
  case SpiritEnum::Memory: return "memory";
  case SpiritEnum::Register: return "register";
  case SpiritEnum::Reserved: return "reserved";
  case SpiritEnum::UnsetUsage: return "";
  }
  return "Unknown";
}

QString locationToString(SpiritEnum::SpiritLocation v)
{
  switch (v)
  {
  case SpiritEnum::LocationRegister: return "reg";
  case SpiritEnum::LocationArray: return "array";
  case SpiritEnum::LocationConstant: return "constant";
  case SpiritEnum::UnsetLocation: return "";
  }
  return "UnknownLocation";
}

SpiritEnum::SpiritLocation stringToLocation(const QString& v)
{
  QString value = v.toLower();
  if ("reg" == v)
    return SpiritEnum::LocationRegister;
  else if ("array" == v)
    return SpiritEnum::LocationArray;
  else if ("constant" == v)
    return SpiritEnum::LocationConstant;

  return SpiritEnum::LocationRegister;
}

SpiritEnum::SpiritEndianness stringToEndian(const QString& v)
{
  QString value = v.toLower();
  if ("little" == v)
    return SpiritEnum::LittleEndian;
  else if ("big" == v)
    return SpiritEnum::BigEndian;

  return SpiritEnum::UnsetEndian;
}

QString endianToString(SpiritEnum::SpiritEndianness v)
{
  switch (v)
  {
  case SpiritEnum::BigEndian: return "big";
  case SpiritEnum::LittleEndian: return "little";
  case SpiritEnum::UnsetEndian: return "";
  }
  return "UnknownEndian";
}


bool SpiritXML::serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh)
{
  xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "component", BAD_CAST "http://www.spiritconsortium.org/XMLSchema/SPIRIT/1.2");
  xmlTextWriterWriteAttributeNS(writer, BAD_CAST "xmlns", BAD_CAST "xsi", NULL, BAD_CAST "http://www.w3.org/2001/XMLSchema-instance");
  xmlTextWriterWriteAttributeNS(writer, BAD_CAST "xsi", BAD_CAST "schemaLocation", NULL, BAD_CAST "http://www.spiritconsortium.org/XMLSchema/SPIRIT/1.2 http://www.spiritconsortium.org/XMLSchema/SPIRIT/1.2/component.xsd");

  xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "vendor", NULL, BAD_CAST qPrintable(getVendor()));
  xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "library", NULL, BAD_CAST qPrintable(getLibrary()));
  xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "name", NULL, BAD_CAST qPrintable(getName()));
  xmlTextWriterWriteElementNS(writer, BAD_CAST "spirit", BAD_CAST "version", NULL, BAD_CAST qPrintable(getVersion()));

  mMemoryMaps.serialize(writer, eh);

  xmlTextWriterEndElement(writer);

  return true;
}

static QScriptValue constructSpiritXML(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 1)
  {
    QString xmlFile = qscriptvalue_cast<QString>(ctx->argument(0));
    SpiritXML* xml = new SpiritXML(xmlFile);
    return eng->toScriptValue(xml);
  }

  SpiritXML* xml = new SpiritXML();
  return eng->toScriptValue(xml);
}

void SpiritXML::registerScriptTypes(QMap<QString, const QMetaObject*>& map)
{
  map["SpiritEnum"] = &SpiritEnum::staticMetaObject;
  map["SpiritXML*"] = &SpiritXML::staticMetaObject;
  map["SpiritRange*"] = &SpiritRange::staticMetaObject;
  map["SpiritReset*"] = &SpiritReset::staticMetaObject;
  map["SpiritBitOffset*"] = &SpiritBitOffset::staticMetaObject;
  map["SpiritField*"] = &SpiritField::staticMetaObject;
  map["SpiritValues*"] = &SpiritValues::staticMetaObject;
  map["SpiritValue*"] = &SpiritValue::staticMetaObject;
  map["SpiritRegister*"] = &SpiritRegister::staticMetaObject;
  map["SpiritBank*"] = &SpiritBank::staticMetaObject;
  map["SpiritBaseAddress*"] = &SpiritBaseAddress::staticMetaObject;
  map["SpiritAddressBlock*"] = &SpiritAddressBlock::staticMetaObject;
  map["SpiritBusType*"] = &SpiritBusType::staticMetaObject;
  map["SpiritMemoryMap*"] = &SpiritMemoryMap::staticMetaObject;
  map["SpiritMemoryMaps*"] = &SpiritMemoryMaps::staticMetaObject;
  map["SpiritBusInterface*"] = &SpiritBusInterface::staticMetaObject;
  map["SpiritBusInterfaces*"] = &SpiritBusInterfaces::staticMetaObject;
  map["SpiritSignalMap*"] = &SpiritSignalMap::staticMetaObject;
  map["SpiritSignalName*"] = &SpiritSignalName::staticMetaObject;
  map["SpiritModel*"] = &SpiritModel::staticMetaObject;
  map["SpiritView*"] = &SpiritView::staticMetaObject;
  map["SpiritViews*"] = &SpiritViews::staticMetaObject;
  map["SpiritSignals*"] = &SpiritSignals::staticMetaObject;
  map["SpiritSignal*"] = &SpiritSignal::staticMetaObject;
  map["SpiritExport*"] = &SpiritExport::staticMetaObject;
  map["SpiritModel*"] = &SpiritModel::staticMetaObject;
  map["SpiritView*"] = &SpiritView::staticMetaObject;
  map["SpiritViews*"] = &SpiritViews::staticMetaObject;
  map["SpiritDefaultFileBuilder*"] = &SpiritDefaultFileBuilder::staticMetaObject;
  map["SpiritFile*"] = &SpiritFile::staticMetaObject;
  map["SpiritFlags*"] = &SpiritFlags::staticMetaObject;
  map["SpiritFileSet*"] = &SpiritFileSet::staticMetaObject;
  map["SpiritFileSets*"] = &SpiritFileSets::staticMetaObject;
}

// Register intellisense
void SpiritXML::registerIntellisense(JavaScriptAPIs* apis)
{
  apis->loadAPIForVariable("SpiritEnum", &SpiritEnum::staticMetaObject, true);
}

void SpiritXML::registerTypes(QScriptEngine* engine)
{
  // Makes these types known to the scripting engine
  qScriptRegisterQObjectMetaType<SpiritXML*>(engine);
  qScriptRegisterQObjectMetaType<SpiritRange*>(engine);
  qScriptRegisterQObjectMetaType<SpiritReset*>(engine);
  qScriptRegisterQObjectMetaType<SpiritBitOffset*>(engine);
  qScriptRegisterQObjectMetaType<SpiritValue*>(engine);
  qScriptRegisterQObjectMetaType<SpiritValues*>(engine);
  qScriptRegisterQObjectMetaType<SpiritField*>(engine);
  qScriptRegisterQObjectMetaType<SpiritRegister*>(engine);
  qScriptRegisterQObjectMetaType<SpiritBank*>(engine);
  qScriptRegisterQObjectMetaType<SpiritBaseAddress*>(engine);
  qScriptRegisterQObjectMetaType<SpiritAddressBlock*>(engine);
  qScriptRegisterQObjectMetaType<SpiritBusType*>(engine);
  qScriptRegisterQObjectMetaType<SpiritMemoryMap*>(engine);
  qScriptRegisterQObjectMetaType<SpiritMemoryMaps*>(engine);
  qScriptRegisterQObjectMetaType<SpiritBusInterface*>(engine);
  qScriptRegisterQObjectMetaType<SpiritBusInterfaces*>(engine);
  qScriptRegisterQObjectMetaType<SpiritSignalMap*>(engine);
  qScriptRegisterQObjectMetaType<SpiritSignalName*>(engine);
  qScriptRegisterQObjectMetaType<SpiritModel*>(engine);
  qScriptRegisterQObjectMetaType<SpiritView*>(engine);
  qScriptRegisterQObjectMetaType<SpiritViews*>(engine);
  qScriptRegisterQObjectMetaType<SpiritSignals*>(engine);
  qScriptRegisterQObjectMetaType<SpiritSignal*>(engine);
  qScriptRegisterQObjectMetaType<SpiritExport*>(engine);
  qScriptRegisterQObjectMetaType<SpiritDefaultFileBuilder*>(engine);
  qScriptRegisterQObjectMetaType<SpiritFile*>(engine);
  qScriptRegisterQObjectMetaType<SpiritFlags*>(engine);
  qScriptRegisterQObjectMetaType<SpiritFileSet*>(engine);
  qScriptRegisterQObjectMetaType<SpiritFileSets*>(engine);

  //Q_DECLARE_METATYPE(SpiritEnum::SpiritAlignment);
  qScriptRegisterEnumMetaType<SpiritEnum::SpiritAlignment>(engine); 
  //Q_DECLARE_METATYPE(SpiritEnum::SpiritFormat);
  qScriptRegisterEnumMetaType<SpiritEnum::SpiritFormat>(engine); 
  //Q_DECLARE_METATYPE(SpiritEnum::SpiritResolve);
  qScriptRegisterEnumMetaType<SpiritEnum::SpiritResolve>(engine); 
  //Q_DECLARE_METATYPE(SpiritEnum::SpiritAccess);
  qScriptRegisterEnumMetaType<SpiritEnum::SpiritAccess>(engine); 
  //Q_DECLARE_METATYPE(SpiritEnum::SpiritUsage);
  qScriptRegisterEnumMetaType<SpiritEnum::SpiritUsage>(engine); 
  //Q_DECLARE_METATYPE(SpiritEnum::SpiritEndianness);
  qScriptRegisterEnumMetaType<SpiritEnum::SpiritEndianness>(engine); 
  //Q_DECLARE_METATYPE(SpiritEnum::SpiritLocation);
  qScriptRegisterEnumMetaType<SpiritEnum::SpiritLocation>(engine); 
  //Q_DECLARE_METATYPE(SpiritEnum::SpiritDirection);
  qScriptRegisterEnumMetaType<SpiritEnum::SpiritDirection>(engine); 

  // Define Enums
  QScriptValue spiritEnum = engine->newQMetaObject(&SpiritEnum::staticMetaObject);
  engine->globalObject().setProperty("SpiritEnum", spiritEnum);

  // Define constructor to enable var x = new SpiritXML();
  QScriptValue spiritCtor = engine->newFunction(constructSpiritXML);
  engine->globalObject().setProperty("SpiritXML", spiritCtor);
}

SpiritXML::SpiritXML(const QString& xmlFileName)
{
  qDebug() << "reading spirit file" << xmlFileName;
  QApplication::setOverrideCursor(Qt::WaitCursor);
  parseSpirtXML(xmlFileName);
  QApplication::restoreOverrideCursor();

}
// Populate the project tree here
void SpiritXML::populateTree(CarbonProjectWidget* pw, MemoryMapsItem* parent)
{
  SpiritMemoryMaps* maps = getMemoryMaps();
  for (UInt32 i=0; i<maps->numItems(); i++)
  {
    SpiritMemoryMap* map = maps->getItem(i);
    MemoryMapItem* mapItem = new MemoryMapItem(pw, parent, map); 

    SpiritBank* bank = map->getBank();
    if (bank)
    {
      MemoryBankItem* bankItem = new MemoryBankItem(pw, mapItem, bank);
      qDebug() << "created memorybank item" << bankItem;
    }

    SpiritAddressBlock* ab = map->getAddressBlock();
    if (ab)
    {
      AddressBlockItem* abItem = new AddressBlockItem(pw, mapItem, ab);
      qDebug() << "created addressblock item" << abItem;
    }
  }
}

void SpiritValues::deserialize(XmlErrorHandler* eh, const QDomElement& parent)
{
  mValues.append(SpiritValue::deserialize(eh, parent));
}

void SpiritValues::serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh)
{
  xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "values", NULL);

  foreach (SpiritValue* sv, mValues)
    sv->serialize(writer, eh);

  xmlTextWriterEndElement(writer);
}

SpiritValue* SpiritValue::deserialize(XmlErrorHandler*, const QDomElement& parent)
{
  SpiritValue* sv = new SpiritValue;

  QDomElement eValue = parent.firstChildElement("value");
  if (eValue.isElement())
    sv->setValue(eValue.text());

  QDomElement eDescr = parent.firstChildElement("description");
  if (eDescr.isElement())
    sv->setDescription(eDescr.text());

  QDomElement eName = parent.firstChildElement("name");
  if (eName.isElement())
    sv->setName(eName.text());

  return sv;
}

void SpiritValue::serialize(xmlTextWriterPtr writer, XmlErrorHandler*)
{
  xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "value", NULL);
  xmlTextWriterWriteString(writer, BAD_CAST qPrintable(getValue()));
  xmlTextWriterEndElement(writer);

  if (!getName().isEmpty())
  {
    xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "name", NULL);
    xmlTextWriterWriteString(writer, BAD_CAST qPrintable(getValue()));
    xmlTextWriterEndElement(writer);
  }

  if (!getDescription().isEmpty())
  {
    xmlTextWriterStartElementNS(writer, BAD_CAST "spirit", BAD_CAST "description", NULL);
    xmlTextWriterWriteString(writer, BAD_CAST qPrintable(getDescription()));
    xmlTextWriterEndElement(writer);
  }
}



