//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "Mdi.h"
#include "gui/CQt.h"
#include <QDebug>
#include <QSettings>
#include <QFileDialog>
#include <QRegExp>
#include <QTabWidget>
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "CarbonMakerContext.h"

MDIWidget* MDIDocumentTemplate::createNewDocument(QWidget* /*parent*/)
{
  return NULL;
}

QAction* MDIDocumentTemplate::findMenu(QMenuBar* mb, const char* menuName)
{
  for (int i=0; i<mb->actions().count(); i++)
  {
    QAction* action = mb->actions()[i];

    UtString ma;
    ma << action->text();

    if (0 == strcmp(ma.c_str(), menuName))
      return action;
  }
  return NULL;
}

void MDIDocumentTemplate::registerUpdateAction(QAction* action, const char* signal, const char* slot)
{
  INFO_ASSERT(action != NULL, "Invalid action");

  action->setProperty(MDI_ACTION_UPDATE_SIGNAL, signal);
  action->setProperty(MDI_ACTION_UPDATE_SLOT, slot);
}

void MDIDocumentTemplate::registerAction(QAction* action, const char* slotName)
{
  action->setProperty(MDI_ACTION, slotName);

  mAddedActions.append(action);
}

const char* MDIDocumentTemplate::updateSlotName(QAction* action)
{
  static UtString slotName;

  slotName.clear();

  QVariant vAction = action->property(MDI_ACTION_UPDATE_SLOT);
  if (vAction.isValid())
  {
    slotName << vAction.toString();
    return slotName.c_str();
  }

  return NULL;
}

const char* MDIDocumentTemplate::updateSignalName(QAction* action)
{
  static UtString signalName;
  signalName.clear();

  QVariant vAction = action->property(MDI_ACTION_UPDATE_SIGNAL);
  if (vAction.isValid())
  {
    signalName << vAction.toString();
    return signalName.c_str();
  }

  return NULL;
}

const char* MDIDocumentTemplate::slotName(QAction* action)
{
  QVariant vAction = action->property(MDI_ACTION);
  static UtString sname;
  sname.clear();
  if (vAction.isValid())
  {
    sname << vAction.toString();
    return sname.c_str();
  }

  return NULL;
}

void MDIDocumentTemplate::deactivateDocument(QWidget* w)
{
  for (int i=0; i<mAddedActions.count(); i++)
  {
    QAction* action = mAddedActions[i];
    const char* sn = slotName(action);
    if (sn != NULL)
      w->disconnect(action, SIGNAL(triggered()), w, sn);

    // Wire up the Update
    const char* updateSlot = updateSlotName(action);
    const char* updateSignal = updateSignalName(action);
    if (updateSlot != NULL && updateSignal != NULL)
    {
      w->disconnect(w, updateSignal, action, updateSlot);
    }
  }
  showMenusAndToolbars(false);
}

void MDIDocumentTemplate::showMenusAndToolbars(bool showFlag)
{
  //qDebug() << "template: " << getDocTemplateName() << " showToolbars: " << showFlag;

  for (int i=0; i<numMenus(); ++i)
  {
    QMenu* menu = mMenus[i];
    int numActions = menu->actions().count();
    for (int j=0; j<numActions; ++j)
    {
      QAction* action = menu->actions()[j];
      if (action->isSeparator())
        continue;

      const char* slot = slotName(action);
      if (slot != NULL)
        action->setVisible(showFlag);
      else
      {
        UtString err;
        err << "Menu: " << menu->title() << " missing slot named: " << action->text();
        INFO_ASSERT(false, err.c_str());
      }
    }
  }

  for (int i=0; i<numToolbars(); ++i)
  {
    QToolBar* tb = mToolBars[i];
    for (int j = 0; j < tb->actions().size(); ++j)
    {
      QAction *action = qobject_cast<QAction *>(tb->actions().at(j));
      action->setVisible(showFlag);
    }

    //qDebug() << "  toolbar: " << tb->windowTitle() << " vis: " << showFlag;
    tb->setVisible(showFlag);
  }
}

void MDIDocumentTemplate::activateDocument(QWidget* w)
{
  for (int i=0; i<mAddedActions.count(); i++)
  {
    QAction* action = mAddedActions[i];
    const char* sn = slotName(action);

    if (sn != NULL)
    {
      bool connected = w->connect(action, SIGNAL(triggered()), sn);
      INFO_ASSERT(connected, "Unable to Locate slot");
    }

    // Wire up the Update
    const char* updateSlot = updateSlotName(action);
    const char* updateSignal = updateSignalName(action);
    if (updateSlot != NULL && updateSignal != NULL)
    {
      bool connected = w->connect(w, updateSignal, action, updateSlot);
      INFO_ASSERT(connected, "Unable to Locate Slot");
    }
  }
  showMenusAndToolbars(true);

  // do we have any friends we want to show as well?
  foreach(MDIFriendToolbar* fr, mFriendTemplates)
  {
    QToolBar* tb = fr->getToolBar();
    for (int i = 0; i < tb->actions().size(); ++i)
    {
      QAction *action = qobject_cast<QAction *>(tb->actions().at(i));
      action->setVisible(true);
    }
    tb->setVisible(true);
  }

  // If there is a project open, always show it as the document name
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  if (pw)
  {
    CarbonProject* proj = pw->project();
    if (proj) {
      mMainWindow->setWindowTitle(tr("%1[*] - %2")
        .arg(proj->shortName())
        .arg("Carbon Model Studio"));
    }
  }
  // If not, ask the widget what it is called.
  else {
    MDIWidget* mw = dynamic_cast<MDIWidget*>(w);
    if (mw)
    {
      qDebug() << "activate" << mw->userFriendlyName();
      mMainWindow->setWindowTitle(tr("%1[*] - %2")
        .arg(mw->userFriendlyName())
        .arg("Carbon Model Studio"));
      qDebug() << "title" << mMainWindow->windowTitle();
    }
  }

  updateUI(w);
}

void MDIDocumentTemplate::initialize(QMainWindow* mw)
{
  mMainWindow = mw;
  // Create the Menus & Toolbars
  createMenus();
  createToolbars();

  // Populate them
  installMenus(mMainWindow);
  installToolbars(mMainWindow);
  
  // Initially not visible
  showMenusAndToolbars(false);

  // Hide inactive menus/ toolbars
  hideInactiveMenus();
}
// This will only be called once for each MDIDocumentTemplate
void MDIDocumentTemplate::installToolbars(QMainWindow* mw)
{
  for (int i=0; i<numToolbars(); ++i)
  {
    QToolBar* tb = mToolBars[i];
    mw->addToolBar(tb);
  }
}

bool MDIDocumentTemplate::isAddedMenu(QMenu* menu)
{
  for (int i=0; i<mAddedMenus.count(); i++)
  {
    QMenu* m = mAddedMenus[i];
    if (m == menu)
      return true;
  }
  return false;
}
void MDIDocumentTemplate::hideInactiveMenus()
{
  // Cruise through the menus and toolbars and hide any ones
  // with all inactive entries
  INFO_ASSERT(mMainWindow, "must call initialize()");

  QMenuBar* mbar = mMainWindow->menuBar();
  foreach (QAction* action, mbar->actions())
  {
    QMenu* menu = action->menu();
    if (menu != NULL)
    {
      if (!hasActiveActions(menu))
      {
       // qDebug() << "Hiding: " << menu->title();
        action->setVisible(false);
      }
      else if (!action->isVisible())
      {
        if (isAddedMenu(menu))
        {
       //   qDebug() << "Unhiding: " << menu->title();
          action->setVisible(true);
        }
      }
    }
  }
}

// Recursively check to see if the menu has any active actions
// below it
bool MDIDocumentTemplate::hasActiveActions(QMenu* menu)
{
  foreach (QAction* action, menu->actions())
  {
    QAction* a = action;
    if (a->isVisible())
      return true;
    QMenu* subMenu = a->menu();
    if (subMenu)
      return hasActiveActions(subMenu);
  }
  return false;
}

void MDIDocumentTemplate::updateUI(QWidget* w)
{
  // Allow the template to update its menus
  updateMenusAndToolbars(w);

  hideInactiveMenus();
}

// This will only be called once for each MDIDocumentTemplate
void MDIDocumentTemplate::installMenus(QMainWindow* mw)
{
  QMenuBar* mbar = mw->menuBar();

  for (int i=0; i<numMenus(); ++i)
  {
    QMenu* newMenu = mMenus[i];
    UtString title;
    title << newMenu->title();
    const char* name = title.c_str();

    QAction* topAction = findMenu(mbar, name);
    QMenu* topMenu = NULL;
    if (topAction == NULL)
    {
      topMenu = mbar->addMenu(newMenu->title());
      mAddedMenus.append(topMenu);
    }
    else
      topMenu = topAction->menu();

    int numActions = newMenu->actions().count();

    for (int j=0; j<numActions; ++j)
    {
      QAction* action = newMenu->actions()[j];
      QAction* firstAction=NULL;
      QAction* lastAction=NULL;
      foreach (QAction* act, topMenu->actions())
      {
        if (act->isSeparator())
        {
          firstAction = act;
          break;
        }
      }

      foreach (QAction* act, topMenu->actions())
      {
        if (act->isSeparator() && act != firstAction)
        {
          lastAction = act;
          break;
        }
      }

      if (firstAction && lastAction)
        topMenu->insertAction(lastAction, action);
      else
        topMenu->addAction(action);
    }
  }
}

MDIDocumentTemplate* MDIDocumentManager::findTemplate(const char* fileName, const char* docType)
{
 QStringList filters;

 qDebug() << "finding template for file: " << fileName << " docType: " << docType;

 for (int i=0; i<mDocTemplates.count(); ++i)
  {
    MDIDocumentTemplate* dt = mDocTemplates[i];

    if (docType == NULL)
    {
      QString f = dt->getFilter();
      QStringList flist = f.split(";;");

      foreach (QString filter, flist)
      {
        int leftParen = filter.indexOf('(');
        int rightParen = filter.indexOf(')');
        QString extList = filter.mid(leftParen+1,rightParen-leftParen-1);

        foreach (QString ext, extList.split(" "))
        {
          QRegExp rx(ext);
          rx.setPatternSyntax(QRegExp::Wildcard);
          if (rx.exactMatch(fileName))
            return dt;
        }
      }
    }
    else // Find it by name, not by file type
    {
      if (0 == strcmp(dt->getDocTemplateName(), docType))
      {
        return dt;
      }
    }
  }
  return NULL;
}

// Search through the active windows and look for one that maches
// and return the widget
MDIWidget* MDIDocumentTemplate::findDocument(QWorkspace* ws, const char* docName, bool activate)
{
  QString canonicalFilePath = QFileInfo(docName).absoluteFilePath();

  qDebug() << " finding document: " << docName;

  foreach (QWidget* window, ws->windowList()) 
  {
    MDIWidget* mdiWidget = dynamic_cast<MDIWidget*>(window);
    if (mdiWidget != NULL)
    {
      const char* ufname = mdiWidget->userFriendlyName();
      if (strcmp(ufname,docName) == 0)
      {
        QWidget* widget = mdiWidget->getWidget();

        QVariant qv = widget->property("CarbonTabWidget");
        QVariant qvPlaceholder = widget->property("CarbonTabPlaceholder");

        if (activate && qv.isValid() && qvPlaceholder.isValid())
        {
          QTabWidget* tw = (QTabWidget*)qv.value<void*>();
          QWidget* w = (QWidget*)qvPlaceholder.value<void*>();

          int tabIndex = tw->indexOf(w);
          if (tabIndex != -1)
            tw->setCurrentIndex(tabIndex);
        }
        return mdiWidget;
      }
    }
  }
  return NULL;
}

void MDIDocumentManager::saveAllDocuments(QWorkspace* ws)
{
 foreach (QWidget* window, ws->windowList()) 
 {
    MDIWidget* mdiWidget = dynamic_cast<MDIWidget*>(window);
    if (mdiWidget)
      mdiWidget->saveDocument();
 } 
}

bool MDIDocumentManager::promptOpenDocument(QWidget* parent, QStringList& files)
{
  UtString filters;
  filters << "Any File (*)";

  for (int i=0; i<mDocTemplates.count(); ++i)
  {
    MDIDocumentTemplate* dt = mDocTemplates[i];
    if (!dt->isInvisibleDocument())
    {
      QString f = dt->getFilter();
      if (f.length() == 0)
        continue;

      filters << ";;" << f;
    }
  }

  if (mLastDir.empty())
    mLastDir << QDir::current().canonicalPath();

  QString lastDir = mLastDir.c_str();

  files = QFileDialog::getOpenFileNames(parent, "Open File", lastDir, filters.c_str());
  if (files.count() > 0)
  {
    QDir dir(lastDir);
    mLastDir.clear();
    mLastDir << dir.canonicalPath();
    return true;
  }
  else
    return false;
}
