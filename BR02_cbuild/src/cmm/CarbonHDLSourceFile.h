#ifndef __CARBONHDLSOURCEFILE_H_
#define __CARBONHDLSOURCEFILE_H_
#include "util/CarbonPlatform.h"

#include "util/UtString.h"
#include "util/XmlParsing.h"

class UtXmlErrorHandler;
class CarbonSourceGroup;

#include "CarbonOptions.h"
class CarbonHDLSourceFile;

class CarbonHDLSourceConfiguration : public QObject
{
  Q_OBJECT

public:
  CarbonHDLSourceConfiguration(CarbonHDLSourceFile* src, const char* configName);
  const char* getName() const { return mName.c_str(); }
  void putName(const char* newVal) {mName=newVal;}
  CarbonOptions* getOptions() { return mOptions; }
  static bool deserialize(CarbonHDLSourceFile* src, xmlNodePtr parent, UtXmlErrorHandler* eh);

 private slots:
  void valueChangedLibraryFile(const CarbonProperty* /* prop */, const char* newValue);

private:
  UtString mName;
  CarbonHDLSourceFile* mSource;
  CarbonOptions* mOptions;
};

class CarbonHDLSourceFile
{
public:
  CarbonHDLSourceFile(CarbonSourceGroup* group, const char* relativePath);
  virtual ~CarbonHDLSourceFile();

  enum HDLType { Verilog=1, VHDL, CPP, Header };

  void putLibraryFile(bool v) { mLibraryFile=v; }
  bool isLibraryFile() { return mLibraryFile; }

  bool serialize(xmlTextWriterPtr writer);
  static bool deserialize(CarbonSourceGroup* group, xmlNodePtr parent, UtXmlErrorHandler* eh);
  void setRelativePath(const char* newVal);
  const char* getRelativePath() { return mRelativePath.c_str(); }
  const char* getCanonicalPath() { return mCanonicalPath.c_str(); }
  const char* getAbsolutePath();
  CarbonHDLSourceConfiguration* getConfiguration(const char* configName);
  void removeConfiguration(const char* configName);
  void addConfiguration(const char* configName);
  CarbonSourceGroup* getGroup() const { return mGroup; }
  void putGroup(CarbonSourceGroup* newVal) { mGroup=newVal; }

  HDLType getType();
  static HDLType getType(const char* relPath);

  typedef UtArray<CarbonHDLSourceConfiguration*> VecConfigs;

private:
  void setPropertyValues(CarbonHDLSourceConfiguration* config);

private:
  bool mLibraryFile;
  UtString mRelativePath;
  UtString mCanonicalPath;
  UtString mAbsPath;
  CarbonSourceGroup* mGroup;
  VecConfigs mConfigs;

};

class CarbonVerilogSource : public CarbonHDLSourceFile
{
public:
  CarbonVerilogSource(CarbonSourceGroup* group, const char* relativePath) : CarbonHDLSourceFile(group, relativePath)
  {
  }
};

class CarbonVHDLSource : public CarbonHDLSourceFile
{
public:
  CarbonVHDLSource(CarbonSourceGroup* group, const char* relativePath) : CarbonHDLSourceFile(group, relativePath)
  {
  }

};

class CarbonHSource : public CarbonHDLSourceFile
{
public:
  CarbonHSource(CarbonSourceGroup* group, const char* relativePath) : CarbonHDLSourceFile(group, relativePath)
  {
  }
};

class CarbonCPPSource : public CarbonHDLSourceFile
{
public:
  CarbonCPPSource(CarbonSourceGroup* group, const char* relativePath) : CarbonHDLSourceFile(group, relativePath)
  {
  }
};


#endif
