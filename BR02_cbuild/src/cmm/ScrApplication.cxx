//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/



#include "util/CarbonPlatform.h"
#include "DesignXML.h"
#include "ScrApplication.h"
#include "cmm.h" 
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h" 
#include "CarbonConsole.h"
#include "JavaScriptAPIs.h"
#include "ScrProject.h"
#include "util/CarbonVersion.h"

#if 1
#include "classGen/ScrApplicationProto_wrapper.cxx"
#endif

extern const char* gCarbonVersion();

Q_DECLARE_METATYPE(ScrApplication*);

static QScriptValue constructDesignHierarchy(QScriptContext *ctx, QScriptEngine *eng)
{
  if (ctx->argumentCount() == 1)
  {
    QString dhFile = qscriptvalue_cast<QString>(ctx->argument(0));
    QFileInfo fi(dhFile);
    if (!dhFile.isEmpty() && fi.exists())
    {
      XDesignHierarchy* dh = new XDesignHierarchy(dhFile);
      return eng->toScriptValue(dh);
    }
  }
  return QScriptValue();
}


bool ScrApplicationProto::checkArgument(const QString& argName)
{
  return theApp->checkArgument(argName);
}

QString ScrApplicationProto::argumentValue(const QString& argName)
{
  return theApp->argumentValue(argName);
}

QScriptValue ScrApplication::newInstance(int size)
{
  return newInstance(QByteArray(size, /*ch=*/0));
}

QScriptValue ScrApplication::newInstance(const QByteArray &ba)
{
  QScriptValue data = QScriptClass::engine()->newVariant(qVariantFromValue(ba));
  return QScriptClass::engine()->newObject(this, data);
}

QScriptValue ScrApplication::construct(QScriptContext *ctx, QScriptEngine *)
{
    ScrApplication *cls = qscriptvalue_cast<ScrApplication*>(ctx->callee().data());
    if (!cls)
        return QScriptValue();
    int size = ctx->argument(0).toInt32();
    return cls->newInstance(size);
}

ScrApplication::ScrApplication(QScriptEngine* engine) : QScriptClass(engine)
{
  mCtor = engine->newFunction(construct);
  mCtor.setData(qScriptValueFromValue(engine, this));

  mProto = engine->newQObject(new ScrApplicationProto(this),
    QScriptEngine::QtOwnership,
    QScriptEngine::SkipMethodsInEnumeration
    | QScriptEngine::ExcludeSuperClassMethods
    | QScriptEngine::ExcludeSuperClassProperties);
  QScriptValue global = engine->globalObject();
  mProto.setPrototype(global.property("Application").property("prototype"));
}


void ScrApplicationProto::registerIntellisense(JavaScriptAPIs* apis)
{
  apis->loadAPIForVariable("Application", &ScrApplicationProto::staticMetaObject);
  apis->loadAPIForVariable("Application::console", &CarbonConsole::staticMetaObject);
}

void ScrApplicationProto::registerScriptTypes(QMap<QString, const QMetaObject*>& map)
{
  map["ScrApplicationProto"] = &ScrApplicationProto::staticMetaObject;
  map["ScrApplicationProto*"] = &ScrApplicationProto::staticMetaObject;
  map["ScrApplication*"] = &ScrApplicationProto::staticMetaObject;
  map["Application*"] = &ScrApplicationProto::staticMetaObject;
  map["Application"] = &ScrApplicationProto::staticMetaObject;
}

void ScrApplicationProto::registerTypes(QScriptEngine* engine)
{
  QScriptValue dhCtor = engine->newFunction(constructDesignHierarchy);
  engine->globalObject().setProperty("DesignHierarchy", dhCtor);
}

QScriptValue ScrApplication::prototype() const
{
  return mProto;
}

ScrApplicationProto::ScrApplicationProto(QObject *parent)
    : DocumentedQObject(parent)
{
  mMakefileFinished = false;
  mCompilationInProgress = false;
 
  CQT_CONNECT(getConsole(), makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType));
}

void ScrApplicationProto::makefileFinished(CarbonConsole::CommandMode mode, CarbonConsole::CommandType)
{
  if (mode == CarbonConsole::Local)
    mMakefileFinished = true;
}


ScrProject* ScrApplicationProto::createProject(const QString& projectName, const QString& projectDirectory)
{
  qDebug() << "ScrApp creating new project" << projectName << "dir" << projectDirectory;
  theApp->createEmptyProject(projectDirectory, projectName);
  return getProject();
}


CarbonConsole* ScrApplicationProto::getConsole() const
{
  return theApp->getContext()->getCarbonProjectWidget()->getConsole();
}

ScrProject* ScrApplicationProto::getProject()
{
  return new ScrProject(this);
}

ScrProject* ScrApplicationProto::getCurrentProject()
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  if (proj)
    return new ScrProject(this);
  else
    return NULL;
}


void ScrApplicationProto::compileNoWait()
{
  if (!mCompilationInProgress)
  {
    mCompilationInProgress = true;
    CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
    CarbonProject* proj = pw->project();
    if (proj)
      proj->compile(pw, false, true);
  }
  else
    context()->throwError("Compilation already in progress");
}

void ScrApplicationProto::compile()
{
  if (!mCompilationInProgress)
  {
    mCompilationInProgress = true;
    CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
    CarbonProject* proj = pw->project();
    if (proj)
      proj->compile(pw, false, true);

    while (mCompilationInProgress && !mMakefileFinished)
    {	        
      SleeperThread::msleep(50);
      qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
      if (mMakefileFinished)
        break;
    }
    mMakefileFinished = false;
    mCompilationInProgress = false;
  }
  else
    context()->throwError("Compilation already in progress");
}

void ScrApplicationProto::recompile()
{
  if (!mCompilationInProgress)
  {
    mCompilationInProgress = true;
    CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
    CarbonProject* proj = pw->project();
    if (proj)
      proj->compile(pw, true, true);

    while (mCompilationInProgress && !mMakefileFinished)
    {	        
      SleeperThread::msleep(50);
      qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
      if (mMakefileFinished)
        break;
    }
    mMakefileFinished = false;
    mCompilationInProgress = false;
  }
  else
    context()->throwError("Compilation already in progress");
}



QString ScrApplicationProto::getRelease() const
{
  return CARBON_RELEASE_ID;
}

QString ScrApplicationProto::getSoftwareVersion() const
{
  return gCarbonVersion();
}

bool ScrApplicationProto::checkoutSimulationRuntime(const QString& licFeature)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonMakerContext* ctx = pw->context();

  UtString reason;

  QString feature = QString("crbn_vsp_%1")
    .arg(licFeature);
  
  bool result = ctx->checkoutSimulationRuntime(feature, &reason);

  if (!result)
     qDebug() << "No license for" << licFeature << "reason" << reason.c_str();
   
  return result;
}



bool ScrApplicationProto::checkoutRuntimeLicense(const QString& licFeature)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonMakerContext* ctx = pw->context();

  UtString reason;
  bool result = ctx->checkoutRuntimeCustomer(licFeature, &reason);

  if (!result)
     qDebug() << "No license for" << licFeature << "reason" << reason.c_str();
   
  return result;
}

int ScrApplicationProto::compareSoftwareVersionSegments(const QString& leftQ, const QString& rightQ) const
{
  UtString left;
  left << leftQ;
  UtString right;
  right << rightQ;

  // find the segments to compare
  UInt32 num_left_segments = 1+ left.count('.');
  UInt32 num_right_segments = 1+ right.count('.');
  UInt32 segments_to_compare = std::min(num_left_segments,num_right_segments);
  size_t left_pos = 0;
  size_t right_pos = 0;

  UInt32 dotPos = 0;
  for ( UInt32 cur = 1; cur <= segments_to_compare; ++cur){
    dotPos = left.find_first_of(".", left_pos);
    UtString left_part;
    left_part.append(left,left_pos, dotPos);
    left_pos = dotPos + 1;
      
    dotPos = right.find_first_of(".", right_pos);
    UtString right_part;
    right_part.append(right,right_pos, dotPos);
    right_pos = dotPos + 1;
      
    UInt32 left_val = atoi(left_part.c_str());
    UInt32 right_val = atoi(right_part.c_str());
      
    if ( left_val == right_val ) {
      // matching 
    } else {
      return ( (left_val < right_val) ? -1 : 1);
    }
  }

  // the parts match up to segments_to_compare
  if ( num_left_segments == num_right_segments ){
    return 0;  // matching IDs
  } else {
    return ( ( num_left_segments < num_right_segments ) ? -1 : 1 );
  }
}

int ScrApplicationProto::compileTarget(const QString& targetName)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

#if pfWINDOWS
  QString program = "make";
#else
  QString program = QString("%1/bin/make").arg(getenv("CARBON_HOME"));
#endif

  QString workingDir = QString("%1/%2")
    .arg(proj->getActivePlatform())
    .arg(proj->getActiveConfiguration());

  pw->makeConsoleVisible();
  
  QApplication::setOverrideCursor(Qt::WaitCursor);

  UtString targetname;
  targetname << targetName;

  QString projDir = proj->getUnixEquivalentPath(proj->getActive()->getOutputDirectory());
  QStringList args;
  args << "-C" << projDir << "-f" << "Makefile.carbon";

  int exitCode = proj->executeMakeCommand(proj->getActive(), CarbonConsole::Compilation, targetname.c_str(), &args, CarbonProject::NotBatch, true);

  QApplication::restoreOverrideCursor();

  return exitCode;
}

QString ScrApplicationProto::methodDescr(const QString& methodName, bool formatHtml) const
{
  qDebug() << "doc method" << methodName << formatHtml;

  METHOD_DESCR("getConsole", 
    "The Carbon Console Object "
    );

  METHOD_DESCR("getRelease", 
    "Returns Carbon Software Release Identifying string");
  
  METHOD_DESCR("getSoftwareVersion", 
    "Returns the Carbon Version Identifying string");

  return QString(); 
}
  
QString ScrApplicationProto::propertyDescr(const QString& propName, bool formatHtml) const
{
  qDebug() << "doc property" << propName << formatHtml;

  return QString(); 
}

void ScrApplicationProto::shutdown(int exitCode)
{
  qDebug() << "ScrApp Shutdown" << exitCode;
  theApp->shutdown(exitCode);
}



PINVOKEABLE void* CSharp_ScrApplication_get_Application()
{
  
  ScrApplicationProto* sa = new ScrApplicationProto();
  return sa;
}

PINVOKEABLE char* CSharp_ScrApplication_get_Name(void* sa)
{
  ScrApplicationProto* a = (ScrApplicationProto*)sa;

  QString n = a->getWorkingDirectory();

  return EmbeddedMono::AllocString(n);
}

PINVOKEABLE bool CSharp_ScrApplication_get_Bool_true()
{
  return true;
}

PINVOKEABLE bool CSharp_ScrApplication_get_Bool_false()
{
  return false;
}

PINVOKEABLE void* CSharp_ScrApplicationProto_new_Application()
{
  ScrApplicationProto* sa = new ScrApplicationProto();
  return sa;
}


