//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QtXml>
#include "MVNetTree.h"

MVNetTree::MVNetTree(QWidget *parent)
: QTreeWidget(parent)
{
  ui.setupUi(this);
  mDB = NULL;
  
  header()->setResizeMode(0, QHeaderView::Interactive);

  CQT_CONNECT(this, itemChanged(QTreeWidgetItem*,int), this, itemChanged(QTreeWidgetItem*,int));
}

void MVNetTree::setFlags(QTreeWidgetItem* item, PortFlags flags)
{
  unsigned int iFlags = (unsigned int)flags;
  QVariant v(iFlags);
  item->setData(1, Qt::UserRole, v);
}

bool MVNetTree::hasFlags(QTreeWidgetItem* item, PortFlags flags)
{
  QVariant vFlags = item->data(1, Qt::UserRole);
  unsigned int itemFlags = vFlags.toUInt();
  unsigned int testFlags = (unsigned int)flags;
  return itemFlags & testFlags;
}

void MVNetTree::changeItemAndChildren(QTreeWidgetItem* item, Qt::CheckState newState)
{
  if (!hasFlags(item, PrimaryPort))
    item->setCheckState(0, newState);
  
  for (int i=0; i<item->childCount(); i++)
  {
    QTreeWidgetItem* child = item->child(i);
    changeItemAndChildren(child, newState);
  }
}

bool MVNetTree::allSiblings(QTreeWidgetItem* item, Qt::CheckState state)
{
  if (item)
  {
    QTreeWidgetItem* parent = item->parent();
    if (parent)
    {
      for (int i=0; i<parent->childCount(); i++)
      {
        QTreeWidgetItem* itm = parent->child(i);
        Qt::CheckState itemState = itm->checkState(0);
        if (!hasFlags(itm, PrimaryPort) && itemState != state)
          return false;
      }
      return true;
    }
  }
 
  return false;
}

void MVNetTree::itemChanged(QTreeWidgetItem* item, int col)
{
  if (item == NULL)
    return;

  bool bs = signalsBlocked();

  blockSignals(true);
  if (col == 0)
  {
    // Get the new state that just changed
    Qt::CheckState state = item->checkState(col);
    changeItemAndChildren(item, state);
  }
  
  // Update the parents from here.
  updateParentCheckboxes(item);

  blockSignals(bs);

  emit dataChanged();
}

MVNetTree::~MVNetTree()
{
}

void MVNetTree::saveValues(QXmlStreamWriter& sw, QTreeWidgetItem* parent, bool writeUnchecked)
{
  if (parent)
  {
    for (int i=0; i<parent->childCount(); i++)
    {
      QTreeWidgetItem* item = parent->child(i);

      bool isFolder = hasFlags(item, FolderItem);

      if (!isFolder && (item->flags() & Qt::ItemIsUserCheckable))
      {
        Qt::CheckState state = item->checkState(0);
        QVariant vNode = item->data(0, Qt::UserRole);

        QString name = item->text(0);

        const CarbonDBNode* node = (const CarbonDBNode*)vNode.value<void*>();
        if (isPrimaryPort(mDB, node))
          continue;

        if (writeUnchecked)
        {
          if (state == Qt::Unchecked)
          {
            sw.writeStartElement("Net");
            sw.writeAttribute("locator", carbonDBNodeGetFullName(mDB, node));
            sw.writeEndElement();
          }
        }
        else
        {
          if (state == Qt::Checked)
          {
            sw.writeStartElement("Net");
            sw.writeAttribute("locator", carbonDBNodeGetFullName(mDB, node));
            sw.writeEndElement();
          }
        }
      }
      saveValues(sw, item, writeUnchecked);
    }
  }
}

void MVNetTree::countCheckboxes(QTreeWidgetItem* parent, int* numItems, int* numChecked, int *numUnchecked)
{
  *numItems += 1;

  if (mDB && parent)
  {
    for (int i=0; i<parent->childCount(); i++)
    {
      QTreeWidgetItem* item = parent->child(i);

      bool isFolder = hasFlags(item, FolderItem);

      if (!isFolder && (item->flags() & Qt::ItemIsUserCheckable))
      {
        QVariant vNode = item->data(0, Qt::UserRole);
        const CarbonDBNode* node = (const CarbonDBNode*)vNode.value<void*>();
        if (isPrimaryPort(mDB, node))
          continue;

        Qt::CheckState state = item->checkState(0);

        if (state == Qt::Checked)
          *numChecked += 1;
        else if (state == Qt::Unchecked)
          *numUnchecked += 1;
      }
      countCheckboxes(item, numItems, numChecked, numUnchecked);
    }
  }
}

// Create the MV Xml Document
bool MVNetTree::writeXmlFile(QXmlStreamWriter& sw)
{
  qDebug() << "MVNetTree writeXmlFile" << mDB;

  sw.writeStartDocument();
  sw.setAutoFormatting(true);

  sw.writeStartElement("ModelValidation");
  sw.writeStartElement("Nets");

  int numItems = 0;
  int numChecked = 0;
  int numUnchecked = 0;

  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);
    countCheckboxes(item, &numItems, &numChecked, &numUnchecked);
  }

  bool writeUnchecked = true;

  // We have more to Exclude than Include
  // so write out all the "Unchecked" things
  if (numChecked > numUnchecked)
  {
    sw.writeAttribute("startsWith", "All");
    writeUnchecked = true;
  }
  else // Reverse the sense
  {
    writeUnchecked = false;
    sw.writeAttribute("startsWith", "None");
  }

  // Now, write out all the items
  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);
    saveValues(sw, item, writeUnchecked);
  }

  sw.writeEndElement(); // Nets
  sw.writeEndDocument(); // ModelValidation

  return true;
}


// Check/Build up the hierarchical path to this node
QTreeWidgetItem* MVNetTree::createHierarchicalPath(CarbonDB* db, const CarbonDBNode* node, Qt::CheckState initialState, Qt::CheckState initialFolderState)
{
  if (!carbonDBCanBeCarbonNet(db, node) || carbonDBIsContainedByComposite(db, node))
    return NULL;

  // We need to build up the path from top down, so build up the parent list
  QList<const CarbonDBNode*> parents;

  // First, build up the list of parents in reverse order
  const CarbonDBNode* parent = node;
  while ( (parent = carbonDBNodeGetParent(db, parent)) != NULL)
    parents.push_front(parent);

  // Now, from top down, build up the nodes if needed
  QTreeWidgetItem* parentItem = NULL;
  foreach (const CarbonDBNode* parent, parents)
  {
    QString fullName = carbonDBNodeGetFullName(db, parent);
    QString moduleName = carbonDBNodeGetLeafName(db, parent);

    QTreeWidgetItem* item = mModuleMap[fullName];
    if (item == NULL) // Create it.
    {
      if (parentItem == NULL)
        parentItem = new QTreeWidgetItem(this);
      else
        parentItem = new QTreeWidgetItem(parentItem);

      QVariant v = qVariantFromValue((void*)parent);
      parentItem->setData(0, Qt::UserRole, v);
      setFlags(parentItem, FolderItem);
      parentItem->setIcon(0, QIcon(":/cmm/Resources/folder-closed-16x16.png"));
      parentItem->setFlags(parentItem->flags() | (Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable));
      parentItem->setCheckState(0, initialFolderState);

      // Add it to the map
      mModuleMap[fullName] = parentItem;
      parentItem->setText(0, moduleName);
    }
    else
      parentItem = item;
  }

  // Now add the "net" node
  QTreeWidgetItem* netItem = new QTreeWidgetItem(parentItem);
  netItem->setText(0, carbonDBNodeGetLeafName(db, node));
  bool primaryPort = isPrimaryPort(db, node);
  netItem->setFlags(netItem->flags() | (Qt::ItemIsEnabled | Qt::ItemIsSelectable ));
  
  if (primaryPort)
    setFlags(netItem, PrimaryPort);
  else
  {
    setFlags(netItem, NormalPort);
    netItem->setFlags(netItem->flags() | Qt::ItemIsUserCheckable);
  }

  QVariant v = qVariantFromValue((void*)node);
  netItem->setData(0, Qt::UserRole, v);

  QString fullName = carbonDBNodeGetFullName(db, node);
  mModuleMap[fullName] = netItem;

  QString flags;
  // Primary ports cannot be "unchecked" so don't show the checkbox
  if (!primaryPort)
    netItem->setCheckState(0, initialState);
  else
  {
    netItem->setIcon(0, QIcon(":/cmm/Resources/hfile.png"));
    flags = appendFlag(flags, "Primary");
  }

  // Add the "flags"
  if (carbonDBIsObservable(db, node))
    flags = appendFlag(flags, "Observable");

  if (carbonDBIsDepositable(db, node))
    flags = appendFlag(flags, "Depositable");

  netItem->setText(1, flags);

  return NULL;
}

QString MVNetTree::appendFlag(const QString& flags, const QString& value)
{
  QString result;

  if (flags.length() > 0)
    result = flags + ", " + value;
  else
    result = value;
  return result;
}

void MVNetTree::clearAll()
{
  blockSignals(true);
  mDB = NULL;
  clear();
  blockSignals(false);
}

void MVNetTree::populate(CarbonProject* proj, const QString& xmlFileName)
{
  Qt::CheckState initialFolderState;

  QFileInfo fi(xmlFileName);
  initialFolderState = Qt::Checked;

  Qt::CheckState initialState = readXmlFile(xmlFileName, true);
 
  blockSignals(true);

  clear();

  mModuleMap.clear();

  CarbonDatabaseContext* dbContext = proj->getDbContext();

  mDB = dbContext->getDB();

  qDebug() << "MVNetTree Populate" << mDB;

  // populate here.
  CarbonDatabaseNodeIter* iDeposits = carbonDBLoopDepositable(dbContext->getDB());

  const CarbonDBNode* node;

  CarbonDatabaseNodeIter* iPrimaries = carbonDBLoopPrimaryPorts(dbContext->getDB());
  while ((node = carbonDBNodeIterNext(iPrimaries)) != NULL)
  {
    createHierarchicalPath(dbContext->getDB(), node, initialState, initialFolderState);
  }
  carbonDBFreeNodeIter(iPrimaries);

  while ((node = carbonDBNodeIterNext(iDeposits)) != NULL)
  {
    if (!isPrimaryPort(dbContext->getDB(), node))
      createHierarchicalPath(dbContext->getDB(), node, initialState, initialFolderState);
  }
  carbonDBFreeNodeIter(iDeposits);

  CarbonDatabaseNodeIter* iObserves = carbonDBLoopObservable(dbContext->getDB());
  while ((node = carbonDBNodeIterNext(iObserves)) != NULL)
  {
    if (!isPrimaryPort(dbContext->getDB(), node))
      createHierarchicalPath(dbContext->getDB(), node, initialState, initialFolderState);
  }
  carbonDBFreeNodeIter(iObserves);

  readXmlFile(xmlFileName);

  // Update folder checkbox states
  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);
    StateFlags currState = updateFolderCheckboxes(item);
    if (currState == NoneChecked)
      item->setCheckState(0, Qt::Unchecked);
    else if (currState == SomeChecked)
      item->setCheckState(0, Qt::PartiallyChecked);
    else if (currState == AllChecked)
      item->setCheckState(0, Qt::Checked);

  }

  blockSignals(false);

  header()->setResizeMode(0, QHeaderView::Interactive);
  header()->setResizeMode(0, QHeaderView::Stretch);

  header()->resizeSections(QHeaderView::ResizeToContents);

  header()->setResizeMode(0, QHeaderView::Interactive);
}

void MVNetTree::updateParentCheckboxes(QTreeWidgetItem* item)
{
// Update parent checkbox state(s);
  bool allChecked = allSiblings(item, Qt::Checked);
  bool allUnchecked = allSiblings(item, Qt::Unchecked);

  QTreeWidgetItem* parentItem = item->parent();
  while ( parentItem != NULL)
  {
    if (allChecked && !allUnchecked)
      parentItem->setCheckState(0, Qt::Checked);
    else if (allUnchecked && !allChecked)
      parentItem->setCheckState(0, Qt::Unchecked);
    else 
      parentItem->setCheckState(0, Qt::PartiallyChecked);

    allChecked = allSiblings(parentItem, Qt::Checked);
    allUnchecked = allSiblings(parentItem, Qt::Unchecked);
    parentItem = parentItem->parent();
  }
}

bool MVNetTree::allChildrenChecked(QTreeWidgetItem* item)
{
  for (int i=0; i<item->childCount(); i++)
  {
    QTreeWidgetItem* childItem = item->child(i);
    if (childItem->childCount() > 0)
    {
      if (!allChildrenChecked(childItem))
        return false;
    }
  }
  return true;
}

MVNetTree::StateFlags MVNetTree::updateFolderCheckboxes(QTreeWidgetItem* item)
{
  StateFlags currState = UnknownChecked;
  StateFlags tempState = UnknownChecked;

  if (item)
  {
    for (int i=0; i<item->childCount(); i++)
    {
      QTreeWidgetItem* childItem = item->child(i);
      if (hasFlags(childItem, FolderItem))
        tempState = updateFolderCheckboxes(childItem);
      else if (!hasFlags(childItem, PrimaryPort))
      {
        if (childItem->checkState(0) == Qt::Checked)
          tempState = AllChecked;
        else
          tempState = NoneChecked;
      }

      if (currState == UnknownChecked && tempState == AllChecked)
        currState = AllChecked;
      else if (currState == NoneChecked && tempState == AllChecked)
        currState = SomeChecked;
      else if (currState == AllChecked && tempState == NoneChecked)
        currState = SomeChecked;
      else if (currState == UnknownChecked && tempState == NoneChecked)
        currState = NoneChecked;
    }

    if (currState == NoneChecked)
      item->setCheckState(0, Qt::Unchecked);
    else if (currState == SomeChecked)
      item->setCheckState(0, Qt::PartiallyChecked);
    else if (currState == AllChecked)
      item->setCheckState(0, Qt::Checked);
  }

  return currState;
}

bool MVNetTree::isPrimaryPort(const CarbonDB* db, const CarbonDBNode* node)
{
  return carbonDBIsPrimaryInput(db, node) ||
    carbonDBIsPrimaryOutput(db, node) ||
    carbonDBIsPrimaryBidi(db, node);
}

Qt::CheckState MVNetTree::readXmlFile(const QString &xmlFileName, bool initialStateInfo)
{
  Qt::CheckState state = Qt::Checked;
  QFileInfo fi(xmlFileName);
  
  if (fi.exists())
  {
    qDebug() << "reading: " << xmlFileName;
    QFile xmlFile(xmlFileName);

    if (xmlFile.open(QFile::ReadOnly | QFile::Text))
    {
      QDomDocument doc;
      int errorLine;
      int errorCol;
      QString errStr;

      if (doc.setContent(&xmlFile, true, &errStr, &errorLine, &errorCol))
      { 
        QDomElement docElem = doc.documentElement();        
        QDomNode netsNode = docElem.firstChild();

        QDomElement netsElement = netsNode.toElement();
        QString startsWith = netsElement.attribute("startsWith");
        if (startsWith.length() == 0)
          startsWith = "All";

        if ("All" == startsWith)
          state = Qt::Checked;
        else
          state = Qt::Unchecked;
       
        if (initialStateInfo)
        {
          xmlFile.close();
          return state;
        }

        QDomNode netNode = netsNode.firstChild();
        while(!netNode.isNull())
        {
          QDomElement element = netNode.toElement(); // try to convert the node to an element.
          if ("Net" == element.tagName())
          {
            QString locator = element.attribute("locator");
            QTreeWidgetItem* item = mModuleMap[locator];
            if (item)
              item->setCheckState(0, startsWith == "All" ? Qt::Unchecked : Qt::Checked);
            else
              qDebug() << "Warning: No such locator found: " << locator;
          }
          netNode = netNode.nextSibling();
        }
      }
    }
  }
  else // Start off "Modified"
  {
    bool bs = signalsBlocked();
    blockSignals(false);
    emit dataChanged();
    blockSignals(bs);
  }

  return state;
}

void MVNetTree::expandChildren(QTreeWidgetItem* item)
{
  if (item)
  {
    expandItem(item);
    for (int i=0; i<item->childCount(); i++)
    {
      QTreeWidgetItem* childItem = item->child(i);
      if (hasFlags(childItem, FolderItem))
        expandChildren(childItem);
    }
  }
}

void MVNetTree::collapseChildren(QTreeWidgetItem* item)
{
  if (item)
  {
    collapseItem(item);
    for (int i=0; i<item->childCount(); i++)
    {
      QTreeWidgetItem* childItem = item->child(i);
      if (hasFlags(childItem, FolderItem))
        collapseChildren(childItem);
    }
  }
}

void MVNetTree::expandAll()
{
  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);
    expandItem(item);
    expandChildren(item);
  }
}
void MVNetTree::collapseAll()
{
  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);
    collapseItem(item);
  }
}
