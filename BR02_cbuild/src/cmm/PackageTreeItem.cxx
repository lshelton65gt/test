// -*-C++-*-
/******************************************************************************
 Copyright (c) 2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "PackageTreeItem.h"
#include <QStringList>

PackageTreeItem::PackageTreeItem(const char* name, const char* filepath, const char* compName, PackageTreeItem* parent) :
    mName(name),
    mFilePath(filepath),
    mCompName(compName),
    mParent(parent), 
    mChildIter(mChildList.end())
{
}


UtString PackageTreeItem::packagePath() const
{
  UtString path;
  if(!mParent->isRoot())
    path << mParent->packagePath() << "/" << name();
  else
    path << name();

  return path;
}

bool PackageTreeItem::isUser() const {
  return mCompName == "User";
}

PackageTreeItem* PackageTreeItem::firstChild() {
  PackageTreeItem* item = NULL;
  if(!mChildList.empty()) {
    mChildIter = mChildList.begin();
    item = *mChildIter;
  }
  return item;
}

PackageTreeItem* PackageTreeItem::nextChild() {
  PackageTreeItem* item = NULL;
  if(mChildIter != mChildList.end()) {
    if(++mChildIter != mChildList.end()) {
      item = *mChildIter;
    }
  }
  return item;
}

// Package Tree will take ownership of pointer after addChild is called
bool PackageTreeItem::addChild(PackageTreeItem* child) {
  mChildList.push_back(child);
  // Iterator is not valid anymore
  mChildIter = mChildList.end();
  return true;
}

bool PackageTreeItem::removeChild(PackageTreeItem* child)
{
  for(UtDLList<PackageTreeItem*>::iterator iter = mChildList.begin(); iter != mChildList.end(); ++iter) {
    if((*iter) == child){
      // If child is a directoy, we must also remove all it's children
      if(child->isDir()) child->removeAllChildren();

      // If mChildIter points to this element it will no longer valid, so we need to move it to the next one
      // before erasing the element
      if(mChildIter == iter) ++mChildIter;
      
      // Erase the element and update the iterator
      mChildList.erase(iter);

      delete child;
      break;
    }
  }
  return true;
}

bool PackageTreeItem::removeAllChildren()
{
  for(UtDLList<PackageTreeItem*>::iterator iter = mChildList.begin(); iter != mChildList.end(); ++iter) {
    if( (*iter)->isDir() ) {
      PackageTreeDir* dir = dynamic_cast<PackageTreeDir*>(*iter);
      if(dir) dir->removeAllChildren();
    }
    delete *iter;
  }
  mChildList.clear();

  return true;
}

bool PackageTreeItem::removeNamedChildren(const UtString& name)
{
  for (UtDLList<PackageTreeItem*>::iterator iter = mChildList.begin(); iter != mChildList.end(); ) {
    PackageTreeDir*  dir  = dynamic_cast<PackageTreeDir*>(*iter);
    PackageTreeFile* file = dynamic_cast<PackageTreeFile*>(*iter);
    if( dir ) {
      if (dir->compName() == name) {
        dir->removeAllChildren();
        delete dir;
        mChildList.erase(iter++);
      }
      else {
        dir->removeNamedChildren(name);
        ++iter;
      }
    }
    else if (file) {
      if (file->compName() == name) {
        delete file;
        mChildList.erase(iter++);
      }
      else
        ++iter;
    }
    else ++iter;
  }
  return true;
}

bool PackageTreeItem::serialize(xmlTextWriterPtr writer)
{
  if(isDir() || isRoot()) {
    if(isDir()) {
      xmlTextWriterStartElement(writer, BAD_CAST "PackageDirectory");
      xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST mName.c_str());
      xmlTextWriterWriteAttribute(writer, BAD_CAST "Component", BAD_CAST mCompName.c_str());
    }
    // Serialize all children
    for (UtDLList<PackageTreeItem*>::iterator iter = mChildList.begin(); iter != mChildList.end(); ++iter) {
      (*iter)->serialize(writer);
    }
  }
  else {
    xmlTextWriterStartElement(writer, BAD_CAST "PackageFile");
    xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST mName.c_str());
    xmlTextWriterWriteAttribute(writer, BAD_CAST "Path", BAD_CAST mFilePath.c_str());
    xmlTextWriterWriteAttribute(writer, BAD_CAST "Component", BAD_CAST mCompName.c_str());
  }

  if(!isRoot()) xmlTextWriterEndElement(writer);

  return true;
}

PackageTreeItem* PackageTreeItem::deserialize(xmlNodePtr parent, UtXmlErrorHandler* eh, PackageTreeItem* parentItem)
{
  PackageTreeItem* item = NULL;

  // Is this a PackageDirectory
  if (XmlParsing::isElement(parent, "PackageDirectory"))
  {
    UtString name, compName;
    XmlParsing::getProp(parent, "Name", &name);
    XmlParsing::getProp(parent, "Component", &compName);
    item = new PackageTreeDir(name.c_str(), compName.c_str(), parentItem);

    // Now deserialize all children is this directory
    for (xmlNodePtr child = parent->children; child != NULL; child = child->next) {
      PackageTreeItem* childItem = PackageTreeItem::deserialize(child, eh, item);
      // If deserialization was successfull, add child
      if(childItem) item->addChild(childItem);
    }
  }
  else if (XmlParsing::isElement(parent, "PackageFile"))
  {
    UtString name, filePath, compName;
    XmlParsing::getProp(parent, "Name", &name);
    XmlParsing::getProp(parent, "Path", &filePath);
    XmlParsing::getProp(parent, "Component", &compName);
    item = new PackageTreeFile(name.c_str(), filePath.c_str(), compName.c_str(), parentItem);
  }
  return item;
}

PackageTreeItem::PkgItemList createPackageDirList(PackageTreeItem* root)
{
  // Iterate over all the children and add all directories + all sub directories to the list
  // The resulting list will be in the order so that parent directories are listed first.
  // I that way each can be passed to a mkdir without the need to the -p switch
  PackageTreeItem::PkgItemList dirList;
  for(PackageTreeItem* child = root->firstChild(); child != NULL; child = root->nextChild())
    if(child->isDir()) {
      // Add the Directory item to the list
      dirList.push_back(child);

      // Then add all the childrens directory items to the list
      PackageTreeItem::PkgItemList childList = createPackageDirList(child);
      dirList.splice(dirList.end(), childList, childList.begin(), childList.end()); 
    }
  return dirList;
}

PackageTreeItem::PkgItemList createPackageFileList(PackageTreeItem* root)
{
  // Iterate over all the children and add all files + all files in sub directories to the list
  PackageTreeItem::PkgItemList fileList;
  for(PackageTreeItem* child = root->firstChild(); child != NULL; child = root->nextChild())
    if(child->isFile()) {
      // Add File item to list
      fileList.push_back(child);
    }
    else if(child->isDir()) {
      // Then add all the childrens file items to the list
      PackageTreeItem::PkgItemList childList = createPackageFileList(child);
      fileList.splice(fileList.end(), childList, childList.begin(), childList.end()); 
    }
  return fileList;
}
