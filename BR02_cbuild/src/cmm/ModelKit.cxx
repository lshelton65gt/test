//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "util/UtEncryptDecrypt.h"
#include "CarbonMakerContext.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "CarbonDatabaseContext.h"
#include "CarbonConsole.h"
#include "ModelKit.h"
#include "CarbonFiles.h"
#include "CarbonComponent.h"
#include "ModelKitWizard.h"
#include "ShellConsole.h"
#include "ScriptingEngine.h"
#include "JavaScriptAPIs.h"
#include "DlgChooseVersion.h"
#include "ModelKitUserWizard.h"
#include "EmbeddedMono.h"


static char gEnvArmRoot[8096];
static char gEnvCarbProj[8096];

#if 1
#include "classGen/KitAnswerGroup_wrapper.cxx"
#include "classGen/KitAnswer_wrapper.cxx"
#include "classGen/KitManifest_wrapper.cxx"
#include "classGen/KitTest_wrapper.cxx"
#include "classGen/KitUserContext_wrapper.cxx"
#include "classGen/KitVersions_wrapper.cxx"
#include "classGen/ModelKit_wrapper.cxx"
#endif

KitVersion* ModelKit::createVersion(KitManifest* manifest, const QString& vendorVer, const QString& descr)
{
  return mVersions.addVersion(manifest, vendorVer, descr);
}
// Copy the manifest 
KitManifest* ModelKit::copyManifest(const QString& manifestName)
{
  KitManifest* mfCopy = findManifest(manifestName);
  INFO_ASSERT(mfCopy, "Expecting Manifest");

  QString newName = QString("Copy of %1").arg(mfCopy->getName());
  KitManifest* newManifest = findManifest(newName);
  int copyNumber = 1;
  // We don't expect a copy name yet
  while (newManifest)
  {
    newName = QString("Copy %1 of %2").arg(copyNumber).arg(mfCopy->getName());
    newManifest = findManifest(newName);
    copyNumber++;
  }

  newManifest = createManifest(newName, mfCopy->getDescription());

  // Copy the contents;
  newManifest->copyManifest(mfCopy);

  newManifest->setName(newName);

  return newManifest;
}

KitManifest* ModelKit::findManifest(const QString& manifestName)
{
  return mManifests.findManifest(manifestName);
}


KitManifest* ModelKit::createManifest(const QString& name, const QString& descr)
{
  qint32 nextID = mManifests.nextID();

  KitManifest* manifest = new KitManifest();
  manifest->setName(name);
  manifest->setID(nextID);
  manifest->setDescription(descr);
  mManifests.addManifest(manifest);

  return manifest;
}


bool ModelKit::writeCommandFile(ModelKitWizard* wiz, QTextStream& outstream, const QString& cmdFile)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  bool errorsFound = false;
  QFile file(cmdFile);
  if (file.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    QTextStream in(&file);
    outstream << "-directive cbuild.dir" << "\n";

    while (!in.atEnd()) 
    {
      QString line = in.readLine().trimmed();
      QString origLine = line;

      // skip blanks
      if (line.length() == 0)
      {
        outstream << "\n";
        continue;
      }

      // skip comments
      if (line.left(2) == "//" || line.left(1) == "#")
      {
        continue;
      }

      // skip directives because we will use libDesign.dir instead from above.
      if (line.startsWith("-directive"))
        continue;

      QStringList tokens = line.split(" ");

      UtString args;
      for (int i=1; i<tokens.count(); i++)
      {
        if (args.length() > 0)
          args << " ";
        args << tokens[i];
      }     

      // is is a switch? or a file?
      if (tokens[0].left(1) == "-" || tokens[0].left(1) == "+")
      {
        QString switchName;
        QString switchValue;

        QRegExp plusSwitch("(\\+[^\\+]+\\+)(.*)");
        if (plusSwitch.exactMatch(tokens[0]))
        {
          switchName = plusSwitch.cap(1);
          switchValue = plusSwitch.cap(2);
          if (switchName == "+incdir+")
          {
            QFileInfo afi(switchValue);
            if (!switchValue.contains('$') && afi.isRelative())
            {
              QString absPath = proj->getUnixEquivalentPath(afi.absoluteFilePath());
              line = QString("%1/%2").arg(switchName).arg(absPath);
            }
          }
        }
        else
        {
          switchValue = args.c_str();
          switchName = tokens[0];

          // skip the -o, it will get added back later 
          // by the ModelKitUserWizard from the configuration page
          // because the user can determine they want a .a or a .lib at that time.
          if (switchName == "-o")
            continue;

          if (switchName == "-y" || switchName == "-v")
          {
            QFileInfo afi(switchValue);
            if (!switchValue.contains('$') && afi.isRelative())
            {
              QString absPath = proj->getUnixEquivalentPath(afi.absoluteFilePath());

              QFileInfo fi(absPath);
              QString canFilePath = fi.canonicalFilePath();
              if (canFilePath.isEmpty())
              {
                canFilePath = absPath;
              }
              line = switchName + " " + canFilePath;
            }
          }
        }

       // qDebug() << "switch" << switchName << "value" << switchValue;
      }
      else // It's a file
      {
        QString fileName = tokens[0];
        QFileInfo afi(fileName);
        if (!fileName.contains('$') && afi.isRelative())
        {
          QString absPath = proj->getUnixEquivalentPath(afi.absoluteFilePath());
          line = absPath;
        }

        // Try to canonicalize it first
        QFileInfo fi(line);
        QString canFilePath = fi.canonicalFilePath();
        if (canFilePath.isEmpty())
        {
          canFilePath = line;
        }
        line = canFilePath;
      }

      QString normalizedName = wiz->normalizeName(line);
      if (normalizedName.contains("../"))
      {
        QMessageBox::critical(NULL, "Invalid Filepath",
          QString("The file or include directory %1 must reside at or below the project directory\n.Or at or below the ARM root directory\nOriginal line: %2\nNormalized Line: %3")
          .arg(line)
          .arg(origLine)
          .arg(line));
        errorsFound = true;
        break;
      }
      outstream << normalizedName << "\n";
    }
  }
  return errorsFound;
}

QString ModelKit::createEncryptedCommandFile(ModelKitWizard* wiz, const char* cmdFile)
{
  QFileInfo fi(cmdFile);
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  const char* cmdFileDir = proj->getActive()->getOutputDirectory();
  TempChangeDirectory cd(cmdFileDir);

  QString outputFileName = QString("%1/compile.tef").arg(fi.absolutePath());
  qDebug() << "generating" << outputFileName;

  QFile outputFile(outputFileName);
  outputFile.open(QFile::WriteOnly);
  QTextStream outstream(&outputFile);

  // Write compile.tef
  bool errorsFound = writeCommandFile(wiz, outstream,  cmdFile);
  outputFile.close();

  // Write explore.tef
  if (!errorsFound)
  {
    outputFileName = QString("%1/explore.tef").arg(fi.absolutePath());
    qDebug() << "generating" << outputFileName;
    QFile exploreFile(outputFileName);
    exploreFile.open(QFile::WriteOnly);
    QTextStream explorestream(&exploreFile);
    explorestream << "-writeGuiDB" << "\n";
    errorsFound = writeCommandFile(wiz, explorestream, cmdFile);
    exploreFile.close();
  }

  if (errorsFound)
    return "";
  else
    return outputFileName;
}


KitVersion* ModelKit::getLatestVersion()
{
  return mVersions.getLatestVersion();
}

void ModelKit::serialize(const QString& modelKitFileName)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  const char* projectDir = proj->getProjectDirectory();
  QFileInfo fi(modelKitFileName);

  CarbonDatabaseContext* dbContext = proj->getDbContext();
  if (dbContext && dbContext->getDB() == NULL)
  {
    QString iodb = proj->getDesignFilePath(".io.db");
    QString symtabdb = proj->getDesignFilePath(".symtab.db");
    QString guidb = proj->getDesignFilePath(".gui.db");
    QFileInfo ifi(iodb);
    QFileInfo sfi(symtabdb);
    QFileInfo gfi(guidb);
    if (ifi.exists())
    {
      UtString dbName; dbName << iodb;
      dbContext->loadDatabase(pw->context()->getQtContext(), dbName.c_str());
    }
    else if (sfi.exists())
    {
      UtString dbName; dbName << symtabdb;
      dbContext->loadDatabase(pw->context()->getQtContext(), dbName.c_str());
    }
    else if (gfi.exists())
    {
      UtString dbName; dbName << guidb;
      dbContext->loadDatabase(pw->context()->getQtContext(), dbName.c_str());
    }
  }

  if (dbContext && dbContext->getDB())
    mComponentName = carbonDBGetTopLevelModuleName(dbContext->getDB());

  QString kitRoot = QString("%1/modelKit").arg(projectDir);
  QDir subDir;
  subDir.mkpath(kitRoot);

  QString kitFileName = QString("%1/%2.modelKit")
                                .arg(kitRoot)
                                .arg(fi.baseName());
 
  QString kitXmlFileName = QString("%1/%2")
                            .arg(kitRoot)
                            .arg(MODELKIT_XML);

  qDebug() << "Writing Model Kit" << kitXmlFileName;

  QDomDocument doc;
  QDomElement rootElement = doc.createElement("ModelKit");
  rootElement.setAttribute("version", "1");
  // The model Kit Revision Number (Carbon Controlled)
  rootElement.setAttribute("revision", mRevision);

  doc.appendChild(rootElement);

  rootElement.setAttribute("copyFiles", getCopyFiles() ? "true" : "false");
  rootElement.setAttribute("askRoot", getAskRoot() ? "true" : "false");

  XmlHelper::addElement(doc, rootElement, "name", getName());
  XmlHelper::addElement(doc, rootElement, "description", getDescription());
  XmlHelper::addElement(doc, rootElement, "componentName", getComponentName());

  if (!getLicenseKey().isEmpty())
    XmlHelper::addElement(doc, rootElement, "licenseKey", getLicenseKey());

  if (!getPrefaceScriptNormalized().isEmpty())
  {
    QDomElement preElement = XmlHelper::addElement(doc, rootElement, "prefaceScript", getPrefaceScriptNormalized());
    preElement.setAttribute("absolutePath", getPrefaceScriptActual());
  }

  if (!getEpilogueScriptNormalized().isEmpty())
  {
    QDomElement epiElement = XmlHelper::addElement(doc, rootElement, "epilogueScript", getEpilogueScriptNormalized());
    epiElement.setAttribute("absolutePath", getEpilogueScriptActual());
  }

  mManifests.serialize(doc, rootElement);
  mVersions.serialize(doc, rootElement);

  QFile file(kitXmlFileName);
  if (file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    QTextStream out(&file);
    out << doc.toString(2);
    file.close();
  }
}

//
// Manifest, is the active one, which needs fresh hashes and files copied into it
//
bool ModelKit::writeKitFile(ModelKitWizard* wiz, const QString& name, KitManifest* manifest)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  const char* projectDir = proj->getProjectDirectory();

  // Serialize the modelKit.xml file
  setCopyFiles(wiz->getCopyFiles());
  setAskRoot(wiz->getAskRoot());

  TempChangeDirectory cd(projectDir);

  UtString modelKitFileName;
  modelKitFileName << name;

  UtString cmdFileName;
  cmdFileName << proj->getDesignFilePath(".ncmd");
  QString encFileName = createEncryptedCommandFile(wiz, cmdFileName.c_str());
  if (encFileName == "")
    return false;

  UtString efFileName;
  efFileName << QString("%1/compile.ef").arg(proj->getActive()->getOutputDirectory());

  UtString exploreFileName;
  exploreFileName << QString("%1/explore.ef").arg(proj->getActive()->getOutputDirectory());

  if (proj->encryptCommandFile())
  {
    QFileInfo fi(efFileName.c_str());
    if (fi.exists())
    {
      manifest->addFile(efFileName.c_str(), wiz->normalizeProjectName(efFileName.c_str()));
      manifest->addFile(exploreFileName.c_str(), wiz->normalizeProjectName(exploreFileName.c_str()));
    }
  }

  // Write out the modelKit.xml file
  // into the project directory
  serialize(name);

  // Now, create the actual .modelKit file
  ZOSTREAMZIP(arFile, modelKitFileName.c_str());
  if (arFile.fail())
  {
    QMessageBox::critical(NULL, MODELSTUDIO_TITLE,  arFile.getFileError());
    return false;
  }

  QString kitRoot = QString("%1/modelKit").arg(projectDir);
  TempChangeDirectory td1(kitRoot);

  // Embed the xml file first
  arFile.addFile(MODELKIT_XML);

  KitVersion* kv = getLatestVersion();
  KitManifest* latestManifest = kv->getManifest();

  // Epilogue script?
  if (!getEpilogueScriptActual().isEmpty())
  {
    QString sn = getEpilogueScriptActual();
    UtString uName; uName << sn;
    if (arFile.checkUnique(uName.c_str()))
      arFile.addFile(uName.c_str());
  }

  // Preface script?
  if (!getPrefaceScriptActual().isEmpty())
  {
    QString sn = getPrefaceScriptActual();
    UtString uName; uName << sn;
    if (arFile.checkUnique(uName.c_str()))
      arFile.addFile(uName.c_str());
  }

  // Add the embedded files from each manifest
  for (UInt32 i=0; i<mManifests.numManifests(); i++)
  {
    KitManifest* mf = mManifests.getManifest(i);
    foreach (KitFile* kf, mf->getFiles())
    {
      UtString embedName;
      if (mf == latestManifest)
        embedName << kf->getActualName();
      else
      {
        QFileInfo fi(kf->getActualName());
        embedName << kitRoot << "/" << mf->getID() << "/" << fi.fileName();
      }
      QFileInfo fi(embedName.c_str());
      if (fi.exists())
      {
        qDebug() << "embedding" << embedName.c_str();
        if (arFile.checkUnique(embedName.c_str()))
          arFile.addFile(embedName.c_str(), kf->getFileType());
        else
        {
          QString msg = QString("Warning Duplicate Embedded File: %1, skipped").arg(embedName.c_str());
          qDebug() << msg;
        }
      }
    }

    foreach (KitFile* kf, mf->getUserFiles())
    {
      UtString embedName;
      if (mf == latestManifest)
        embedName << kf->getActualName();
      else
      {
        QFileInfo fi(kf->getActualName());
        embedName << kitRoot << "/" << mf->getID() << "/" << fi.fileName();
      }
      QFileInfo ufi(embedName.c_str());
      if (ufi.exists())
      {
        qDebug() << "embedding userfile" << embedName.c_str();

        if (arFile.checkUnique(embedName.c_str()))
          arFile.addFile(embedName.c_str(), kf->getFileType());
        else
        {
          QString msg = QString("Warning Duplicate Embedded File: %1, skipped").arg(embedName.c_str());
          qDebug() << msg << ufi.exists();
        }
      }
      else
      {
        QString msg = QString("ERROR: No such file to embed: %1, skipped").arg(embedName.c_str());
        qDebug() << msg;
      }
    }
  }

  qDebug() << "finalized modelkit";

  arFile.finalize();

  return true;
}


bool ModelKit::runKitScript(ModelKitUserWizard* wiz, KitUserContext* userContext, ModelKit::KitScript scriptType)
{
  QString script;

  switch (scriptType)
  {
    case ModelKit::Preface: 
      script = userContext->expandName(getPrefaceScriptNormalized());
      break;
    case ModelKit::Epilogue:
      script = userContext->expandName(getEpilogueScriptNormalized());
      break;
  }

  if (!script.isEmpty())
  {
    KitActionRunScript rs;
    rs.setScriptName(script);
    return rs.execute(wiz, wiz, userContext);
  }

  return true;
}

void ModelKit::setupEnvironment(const QString& armRoot)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  const char* projectDir = proj->getProjectDirectory();

  if (armRoot.isEmpty())
  {
    // must be static so it remains in the environment
    UtString envArm;
    envArm << "ARM_ROOT=" << armRoot;
    strcpy(gEnvArmRoot, envArm.c_str());
    putenv(gEnvArmRoot);
    qDebug() << "defined" << envArm.c_str();
  }

  // must be static so it remains in the environment
  UtString envCarbonProj;
  envCarbonProj << "CARBON_PROJECT=" << projectDir;
  strcpy(gEnvCarbProj, envCarbonProj.c_str());
  qDebug() << "defined" << envCarbonProj.c_str();
  putenv(gEnvCarbProj);
}

KitManifest* ModelKit::openKitFile(const QString& name, const QString& armRoot)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  const char* projectDir = proj->getProjectDirectory();

  mArmRoot = armRoot;

  // Make the current directory the project directory
  TempChangeDirectory cd(projectDir);

  UtString modelKitFileName;
  modelKitFileName << name;
  
  setupEnvironment(armRoot);

  // Read it back in and verify
  ZISTREAMZIP(arRead, modelKitFileName.c_str());

  if (extractFile(arRead, MODELKIT_XML, MODELKIT_XML, 0))
  {
    deserialize(MODELKIT_XML); 
    if (mManifests.numManifests() > 1)
    {
      DlgChooseVersion dlg(theApp, this);
      if (dlg.exec())
      {
        KitVersion* v = dlg.getVersion();
        extractManifest(arRead, projectDir, v->getManifest(), true);
        return v->getManifest();
      }
      else
        return NULL;
    }
    else
    {
      KitVersion* v = getLatestVersion();
      extractManifest(arRead, projectDir, v->getManifest(), true);
      return v->getManifest();
    }
  }
  else
  {
    deserialize(name);
    KitVersion* v = getLatestVersion();
    return v->getManifest();

    QString msg = QString("Invalid .modelKit file: %1\nUpdate to later version.").arg(name);
    QMessageBox::critical(NULL, MODELSTUDIO_TITLE, msg);
  }

  return NULL;
}

// Extract the file named absolutePathName from the .modelKit
// create it using the normalizedName
//
bool ModelKit::extractFile(ZistreamZip& arRead, const QString& absolutePathName, const QString& normalizedName, QFile::Permissions perms)
{
  bool debugger = getenv("CARBON_MODELKIT_DEBUGGER");

  if (absolutePathName.isEmpty())
    return false;

  UtString name;
  name << absolutePathName;

  UtString outputName;
  QFileInfo nfi(normalizedName);
  
  outputName << normalizedName;

  UtString err;
  UtString expandedOutputName;
  if (!OSExpandFilename(&expandedOutputName, outputName.c_str(), &err))
  {
    UtString msg;
    msg << "Failed to expand: " << outputName << "\n" << err;
    QMessageBox::critical(NULL, MODELSTUDIO_TITLE, msg.c_str());
    return false;
  }

  qDebug() << "extracting" << normalizedName << "to" << expandedOutputName.c_str();

  ZistreamEntry* entry = arRead.getEntry(name.c_str());
  if (entry == NULL)
  {
    qDebug() << "ERROR no such kit file" << name.c_str();
    return false;
  }

  QFileInfo fi(expandedOutputName.c_str());
  if (!fi.dir().exists())
  {
    fi.dir().mkpath(fi.absolutePath());
    qDebug() << "creating expansion directory" << fi.absolutePath();
  }

  if (fi.exists())
  {
    QFile tempFile(expandedOutputName.c_str());
    tempFile.remove();
  }

  Zistream* ain = entry->castZistream();
  UtString reason;

 
  FILE* out = OSFOpen(expandedOutputName.c_str(), "w", &reason);
  if (out)
  {
    while (! ain->eof() && ! ain->fail())
    {
      char abuf[65536];
      UInt32 numRead = ain->read(abuf, 65536);
      fwrite(abuf, 1, numRead, out);
    }

    if (perms != 0)
    {
      QFile newFile(expandedOutputName.c_str());
      qDebug() << "Setting Permissions" << expandedOutputName.c_str() << perms;
      newFile.setPermissions(perms);
    }

    fclose(out);
  }
  else
  {
    QString err = QString("Error; Unable to create: %1\nReason: %2")
      .arg(expandedOutputName.c_str())
      .arg(reason.c_str());

    QMessageBox::critical(NULL, "Kit Error", err);
  }

  return true;
}

void ModelKit::registerIntellisense(JavaScriptAPIs* apis)
{
  apis->loadAPIForVariable("ModelKit", &ModelKit::staticMetaObject);
}


// Allows properties of these types to work through intellisense
void ModelKit::registerScriptTypes(QMap<QString, const QMetaObject*>& map)
{
  map["ModelKit*"] = &ModelKit::staticMetaObject;
  map["KitVersion*"] = &KitVersion::staticMetaObject;
  map["KitVersions*"] = &KitVersions::staticMetaObject;
}

void ModelKit::registerTypes(QScriptEngine* engine)
{
  if (engine)
  {    
    // Allows an object to return a property of type KitFile
    qScriptRegisterQObjectMetaType<KitManifests*>(engine);
    qScriptRegisterQObjectMetaType<KitVersions*>(engine);
    qScriptRegisterQObjectMetaType<KitVersion*>(engine);
  }
}


void ModelKit::deserialize(const QString& xmlFile)
{
  mVersions.removeAll();
  mManifests.removeAll();

  mCopyFiles = true;
  mAskRoot = true;

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  CarbonDatabaseContext* dbContext = proj->getDbContext();
  if (dbContext && dbContext->getDB())
    mComponentName = carbonDBGetTopLevelModuleName(dbContext->getDB());

  QFile file(xmlFile);

  if (file.open(QFile::ReadOnly | QFile::Text))
  {
    QDomDocument doc;
    int errorLine;
    int errorCol;
    QString errStr;

    QFileInfo templateFi(xmlFile);

    if (doc.setContent(&file, true, &errStr, &errorLine, &errorCol))
    { 
      QDomElement rootElem = doc.documentElement();
      mXMLVersion = rootElem.attribute("version");

      mAskRoot = rootElem.attribute("askRoot") == "true" ? true : false;
      mCopyFiles = rootElem.attribute("copyFiles") == "true" ? true : false;
      if (!rootElem.attribute("revision").isEmpty())
        mRevision = rootElem.attribute("revision");

      QDomElement compNameElem = rootElem.firstChildElement("componentName");
      if (compNameElem.isElement())
        setComponentName(compNameElem.text());

      QDomElement licElem = rootElem.firstChildElement("licenseKey");
      if (licElem.isElement())
        setLicenseKey(licElem.text());

      QDomElement prefaceElem = rootElem.firstChildElement("prefaceScript");
      if (prefaceElem.isElement())
        setPrefaceScript(prefaceElem.text(), prefaceElem.attribute("absolutePath"));

      QDomElement epilogueElem = rootElem.firstChildElement("epilogueScript");
      if (epilogueElem.isElement())
        setEpilogueScript(epilogueElem.text(), epilogueElem.attribute("absolutePath"));

      mName = rootElem.firstChildElement("name").text();
      mDescription = rootElem.firstChildElement("description").text();

      mManifests.deserialize(this, rootElem);
      mVersions.deserialize(this, rootElem);
    }
    else
    {
      QString errStr 
        = QString("Error reading %1\nError Message: %2\nLine %3, Column %4")
          .arg(xmlFile)
          .arg(errStr)
          .arg(errorLine)
          .arg(errorCol);
      QMessageBox::critical(NULL, MODELSTUDIO_TITLE, errStr);
    }
    file.close();
  }
}

void ModelKit::reopenModelKit(const QString& name)
{
  mVersions.removeAll();
  mManifests.removeAll();

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  const char* projectDir = proj->getProjectDirectory();
  QString modelKitDir = QString("%1/modelKit").arg(projectDir);

  QDir subdir;
  subdir.mkpath(modelKitDir);

  UtString uFileName; uFileName << name;

  setupEnvironment("");

  // Read it back in and verify
  ZISTREAMZIP(arRead, uFileName.c_str());

  QString outManifestName = QString("%1/%2")
    .arg(modelKitDir)
    .arg(MANIFEST_FILENAME);
  
  QString outXmlName = QString("%1/%2")
    .arg(modelKitDir)
    .arg(MODELKIT_XML);

  // Old kit?
  bool oldKit = extractFile(arRead, MANIFEST_FILENAME, outManifestName, 0);
  if (oldKit)
  {
    // Version 1
    KitManifest* manifest = createManifest("Default", "Imported");
    manifest->deserialize(outManifestName);
    setLicenseKey(manifest->getLicenseKey());
    setDescription(manifest->getDescription());
    setAskRoot(manifest->getAskRoot());
    setCopyFiles(manifest->getCopyFiles());

    QFileInfo fi(name);
    setName(fi.baseName());

    if (getDescription().isEmpty())
      setDescription("<Change Description here>");

    extractManifest(arRead, modelKitDir, manifest);
    createVersion(manifest, "rel1", "description");
    manifest->setEditable(true);
    manifest->setPreserveHashes(false);
  }
  else // New Kit
  {
    if (extractFile(arRead, MODELKIT_XML, outXmlName, 0))
    {
      deserialize(outXmlName);  

      if (!getEpilogueScriptNormalized().isEmpty())
        extractFile(arRead, getEpilogueScriptActual(), getEpilogueScriptNormalized(), 0);

      if (!getPrefaceScriptNormalized().isEmpty())
        extractFile(arRead, getPrefaceScriptActual(), getPrefaceScriptNormalized(), 0);

      for(quint32 i=0; i<mManifests.numManifests(); i++)
      {
        KitManifest* mf = mManifests.getManifest(i);
        extractManifest(arRead, modelKitDir, mf);
      }
    }
    else
    {
      QString errMsg = QString("Unable to extract %1 from %2")
            .arg(outXmlName)
            .arg(name);
      QMessageBox::critical(NULL, MODELSTUDIO_TITLE, errMsg);

    }
  }
}

// Extract all files using Manifest from arRead into kitRoot
void ModelKit::extractManifest(ZistreamZip& arRead, const QString& kitRoot, KitManifest* manifest, bool endUser)
{
  QString kitDir;

  if (endUser)
    kitDir = kitRoot;
  else
    kitDir = QString("%1/%2").arg(kitRoot).arg(manifest->getID());
 
  QDir subdir;
  subdir.mkpath(kitDir);

  qDebug() << "Extracting manifest" << manifest->getID() << "into" << kitDir;

  KitVersion* kv = getLatestVersion();
  KitManifest* latestManifest = NULL;
  if (kv)
    latestManifest = kv->getManifest();

  if (!getEpilogueScriptNormalized().isEmpty())
    extractFile(arRead, getEpilogueScriptActual(), getEpilogueScriptNormalized(), 0);

  if (!getPrefaceScriptNormalized().isEmpty())
    extractFile(arRead, getPrefaceScriptActual(), getPrefaceScriptNormalized(), 0);


  foreach (KitFile* kf, manifest->getFiles())
  {
    QFileInfo fi(kf->getNormalizedName());
    QString extractedName;
    if (!endUser)
      extractedName = QString("%1/%2").arg(kitDir).arg(fi.fileName());
    else
      extractedName = kf->getNormalizedName();

    QString srcName = kf->getActualName();
    if (latestManifest && latestManifest != manifest)
    {
      QFileInfo fi2(kf->getActualName());
      srcName = QString("%1/%2").arg(manifest->getID()).arg(fi2.fileName());
    }
    if (extractFile(arRead, srcName, extractedName, kf->getPermissions()))
      qDebug() << "extracted embedded file" << srcName << "to" << extractedName;
    else
      qDebug() << "ERROR: expected embedded kit file" << srcName;
  }

  foreach (KitFile* kf, manifest->getUserFiles())
  {
    qDebug() << "extracting userfile" << kf->getActualName() << kf->getNormalizedName();

    QFileInfo fi(kf->getNormalizedName());
    QString extractedName;
    if (!endUser)
      extractedName = QString("%1/%2").arg(kitDir).arg(fi.fileName());
    else
      extractedName = kf->getNormalizedName();

    QString srcName = kf->getActualName();
    if (latestManifest && latestManifest != manifest)
    {
      QFileInfo fi2(kf->getActualName());
      srcName = QString("%1/%2").arg(manifest->getID()).arg(fi2.fileName());
    }
    if (extractFile(arRead, srcName, extractedName, kf->getPermissions()))
      qDebug() << "extracted userfile" << srcName << "to" << extractedName;
    else
      qDebug() << "ERROR: expected embedded kit file" << srcName;
  }
}

QString ModelKit::methodDescription(const QString& methodName) const
{
  qDebug() << "doc methoddescr" << methodName;
  return QString();
}

QString ModelKit::methodDescr(const QString& methodName, bool formatHtml) const
{
  qDebug() << "doc method" << methodName << formatHtml;

  METHOD_DESCR("retainFile", 
    "Retains files after model kit is cleaned up"
    );

  METHOD_DESCR("getRevision", 
    "The Carbon Revision Identifier indicating the Revision number for this ModelKit");
  
  METHOD_DESCR("getSoftwareVersion", 
    "Returns the Carbon Version Identifying string");

  return QString(); 
}
  
QString ModelKit::propertyDescr(const QString& propName, bool formatHtml) const
{
  qDebug() << "doc property" << propName << formatHtml;

  return QString(); 
}
