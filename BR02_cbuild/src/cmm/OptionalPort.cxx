//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "OptionalPort.h"
#include "Mode.h"
#include "Template.h"
#include "Block.h"

bool OptionalPort::isEnabled()
{
  if (mParameter)
    return (mParameter->getValue() == mIncludeValue) ? true : false;
  else
    return false;
}

Parameter* OptionalPort::getParameter() 
{
  return mParameter; 
}

bool OptionalPort::deserialize(Mode* mode, Template* templ, Block* block, Ports*, const QDomElement& e, XmlErrorHandler* eh)
{
  if (!e.hasAttribute("includeValue"))
  {
    eh->reportProblem(XmlErrorHandler::Error, e, "OptionalPort missing required 'includeValue' attribute.");
    return false;
  }

  if (e.hasAttribute("blockParameter") && block == NULL)
  {
    eh->reportProblem(XmlErrorHandler::Error, e, 
      "OptionalPort has invalid 'blockParameter' attribute in this context");
    return false;
  }

  if (e.hasAttribute("modeParameter") && mode == NULL)
  {
    eh->reportProblem(XmlErrorHandler::Error, e, 
      "OptionalPort has invalid 'modeParameter' attribute in this context");
    return false;
  }

  if (e.hasAttribute("templateParameter") && templ == NULL)
  {
    eh->reportProblem(XmlErrorHandler::Error, e, 
      "OptionalPort has invalid 'modeParameter' attribute in this context");
    return false;
  }

  mIncludeValue = e.attribute("includeValue");
  if (e.hasAttribute("modeParameter"))
  {
    mRefType = RefMode;
    mParameter = mode->getParameters()->findParam(e.attribute("modeParameter"));
    if (mParameter == NULL)
    {
      eh->reportProblem(XmlErrorHandler::Error, e, 
        QString("OptionalPort 'modeParameter' referencing non-existing parameter named: %1").arg(e.attribute("modeParameter")));
      return false;
    }
  }
  else if (e.hasAttribute("templateParameter"))
  {
    mRefType = RefTemplate;
    mParameter = templ->getParameters()->findParam(e.attribute("templateParameter"));
    if (mParameter == NULL)
    {
      eh->reportProblem(XmlErrorHandler::Error, e, 
        QString("OptionalPort 'templateParameter' referencing non-existing parameter named: %1").arg(e.attribute("templateParameter")));
      return false;
    }
  }
  else if (e.hasAttribute("blockParameter"))
  {
    mRefType = RefBlock;
    mParameter = block->getParameters()->findParam(e.attribute("blockParameter"));
    if (mParameter == NULL)
    {
      eh->reportProblem(XmlErrorHandler::Error, e, 
        QString("OptionalPort 'blockParameter' referencing non-existing parameter named: %1").arg(e.attribute("blockParameter")));
      return false;
    }
  }
  else
  {
    eh->reportProblem(XmlErrorHandler::Error, e, 
      "OptionalPort missing a required 'modeParameter' or 'templateParameter' attribute.");
    return false;
  }

  return true;
}
Port* OptionalPort::copyPort()
{
  OptionalPort* p = new OptionalPort();
  p->copyPortData(this);

  p->setParameter(mParameter);
  p->setIncludeValue(mIncludeValue);
  p->setReferenceType(mRefType);
  
  return p;
}
