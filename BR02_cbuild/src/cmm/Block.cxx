//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "Block.h"
#include "Blocks.h"
#include "Template.h"
#include <QDebug>

Ports* Block::getPorts()
{
  return &mPorts;
}

Parameters* Block::getParameters()
{
  return &mParameters;
}

bool Block::deserialize(Mode* mode, Template* templ, Blocks* blocks, const QDomElement& e, XmlErrorHandler* eh)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.

    if ("Block" == element.tagName())
    {
      Block* block = new Block();
      block->setName(element.attribute("name"));
      blocks->addBlock(block);
    
      if (!block->mParameters.deserialize(element, eh))
        return false;


      // TemplateParameter reference
      if (element.hasAttribute("templateParameter"))
      {
        QString paramName = element.attribute("templateParameter");

        Parameter* param = templ->findParameter(paramName);
        if (!param)
        {
          eh->reportProblem(XmlErrorHandler::Error, e, 
            QString("No such template parameter '%1' referenced.").arg(paramName));
          return false;
        }
        block->mBlockParameter = param;
      }
      else if (element.hasAttribute("blockParameter"))
      {
        QString paramName = element.attribute("blockParameter");

        Parameter* param = (Parameter*)block->mParameters.findParameter(paramName);
        if (!param)
        {
          eh->reportProblem(XmlErrorHandler::Error, e, 
            QString("No such block parameter '%1' referenced.").arg(paramName));
          return false;
        }
         block->mBlockParameter = param;
      }

      if (!block->mPorts.deserialize(mode, templ, block, element, eh))
        return false;
    }

    n = n.nextSibling();
  }
  return true;
}
