#ifndef WIZARDFORM_H
#define WIZARDFORM_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QWidget>
#include "ui_WizardForm.h"

class WizardForm : public QWidget
{
    Q_OBJECT

public:
    WizardForm(QWidget *parent = 0);
    ~WizardForm();

private:
    Ui::WizardFormClass ui;
};

#endif // WIZARDFORM_H
