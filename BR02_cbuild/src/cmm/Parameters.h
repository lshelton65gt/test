#ifndef __CPARAMETERS_H__
#define __CPARAMETERS_H__

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "Scripting.h"
class Parameters;

// File: Parameters, Parameter
// The Parameters collection and the Parameter object
//
//
// Class: Parameter
//
// The Parameter class represents an XML defined parameter
//
class Parameter : public QObject
{
  Q_OBJECT

  Q_ENUMS(ParameterType)

  // Property: type
  // The type of this parameter, one of <ParameterType> values.
  //
  // Access:
  //   ReadOnly
  //
  // See Also:
  // <ParameterType>
  //
  Q_PROPERTY(ParameterType type READ getType)

  // Property: name
  // The name of this property as defined by the 'name' attribute.
  //
  // Access:
  //   ReadOnly
  Q_PROPERTY(QString name READ getName)

  // Property: prompt
  // Prompt string presented to the user.
  //
  // Access:
  //   ReadOnly
  Q_PROPERTY(QString prompt READ getPrompt WRITE setPrompt)

  // Property: description
  // String value which contains a verbose description of how and what this parameter is.
  Q_PROPERTY(QString description READ getDescription WRITE setDescription)

  // Property: defaultValue
  // String value defined for this parameter as defined by the 'defaultValue' attribute from
  // the XML template
  Q_PROPERTY(QVariant defaultValue READ getDefaultValue WRITE setDefaultValue)

  // Property: required
  // Boolean value if true, indicates that the user must fill in a value (non-blank) before
  // the wizard allows the user to continue.
  //
  // Access:
  //   ReadOnly
  Q_PROPERTY(bool required READ getRequired)

  // Property: value
  // The value specified by the user returned as a String
  Q_PROPERTY(QVariant value READ getValue WRITE setValue)

  // Property: intValue
  // The value specified by the user returned as an Integer
  Q_PROPERTY(int intValue READ getIntValue WRITE setIntValue)

  // Property: boolValue
  // The value specified by the user returned as a Boolean
  Q_PROPERTY(bool boolValue READ getBoolValue WRITE setBoolValue)

public:
  // Enum: ParameterType
  //
  // Enum Values: [Integer|String|Bool|Enum]
  enum ParameterType { Integer, String, Bool, Enum };
  static ParameterType fromString(const QString& val);
  static bool deserialize(Parameters* params, const QDomElement& e, XmlErrorHandler* eh);

  // overrrides
  virtual Parameter* copyParameter();
  virtual QWidget* createEditor(QTreeWidget* tree, QWidget* parent, const QString& currValue);
  virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index);
  virtual bool hasValueSet();

  // ctor
  Parameter(QObject* parent=NULL) : QObject(parent)
  {
    mTree = NULL;
    mRequired = false;
  }

  bool isRequired() const { return mRequired; }

  void setValidExpression(const QString& newVal) { mValidExpr=newVal; }
  QString getValidExpression() const { return mValidExpr; }

  bool getRequired() const { return mRequired; }
  void setRequired(bool newVal) { mRequired=newVal; }

  ParameterType getType() const { return mType; }
  void setType(ParameterType newVal) { mType=newVal; }

  QVariant getValue() const { return mValue; }
  void setValue(const QVariant& newVal) { mValue=newVal; }

  QVariant getDefaultValue() const { return mDefaultValue; }
  void setDefaultValue(const QVariant& newVal) { mDefaultValue=newVal; }

  QString getDescription() const { return mDescription; }
  void setDescription(const QString& newVal) { mDescription=newVal; }

  int getIntValue() const { return mValue.toInt(); }
  void setIntValue(int newVal) { mValue = newVal; }

  int getBoolValue() const { return mValue.toBool(); }
  void setBoolValue(bool newVal) { mValue = newVal; }

  QString getName() const { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

  QString getPrompt() const { return mPrompt; }
  void setPrompt(const QString& newVal) { mPrompt = newVal; }

private:
  Q_DISABLE_COPY(Parameter)

protected:
  QTreeWidget* getTree() const { return mTree; }
  void setTree(QTreeWidget* newVal) { mTree=newVal; }
  Parameter* copyParameterData(Parameter* p);

private slots:
  

private:
  QTreeWidget* mTree;
  ParameterType mType;
  QString mPrompt;
  QString mDescription;
  QVariant mValue;
  QVariant mDefaultValue;
  QString mName;
  bool mRequired;
  QString mValidExpr;

};

Q_DECLARE_METATYPE(Parameter*);

class IntegerParameter : public Parameter
{
  Q_OBJECT

  Q_PROPERTY(int from READ getFrom)
  Q_PROPERTY(int to READ getTo)

public:
  IntegerParameter()
  {
    mFrom=-1;
    mTo=-1;
  }

  bool deserialize(const QDomElement& e, XmlErrorHandler* eh);

  int getFrom() const { return mFrom; }
  void setFrom(int newVal) { mFrom = newVal; }

  int getTo() const { return mTo; }
  void setTo(int newVal) { mTo = newVal; }

  QWidget* createEditor(QTreeWidget* tree, QWidget* parent, const QString& currValue);
  void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index);

protected:
  virtual Parameter* copyParameter();
  virtual bool hasValueSet();

private:
  int mFrom;
  int mTo;
};

class BoolParameter : public Parameter
{
  Q_OBJECT

public:
  bool deserialize(const QDomElement& e, XmlErrorHandler* eh);

protected:
  virtual Parameter* copyParameter();
  virtual bool hasValueSet();

  QWidget* createEditor(QTreeWidget* tree, QWidget* parent, const QString& currValue);
  void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index);

private slots:
  void currentIndexChanged(int);
};

class EnumParameter : public Parameter
{
  Q_OBJECT

  Q_PROPERTY(QStringList choices READ getChoices)

public:
  QStringList getChoices() const { return mChoices; }
  bool deserialize(const QDomElement& e, XmlErrorHandler* eh);

protected:
  virtual Parameter* copyParameter();

  QWidget* createEditor(QTreeWidget* tree, QWidget* parent, const QString& currValue);
  void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index);

private slots:
  void currentIndexChanged(int);

private:
  QStringList mChoices;

};

// Class: Parameters
//
// The Parameters collection is a collection of <Parameter> objects.
//
class Parameters : public QObject
{
  Q_OBJECT

  // Property: count
  // The number of <Parameter> objects in the collection
  Q_PROPERTY(int count READ getCount)

public:
   Parameters(QObject* parent=0);

  ~Parameters()
  {
  }  

  void addParam(Parameter* p);
  Parameter* getAt(int index)
  {
    if (index >= 0 && index < mParams.count())
      return mParams[index];
    else
      return NULL;
  }
  bool deserialize(const QDomElement& e, XmlErrorHandler* eh);
  Parameter* findParam(const QString& name);

public slots:
// Method: Parameter getParameter(index)
// Returns the Nth parameter in the collection
//
// Parameters: 
//  index - An index from 0 to <count>-1
//
//   An index from 0 to N
  QObject* getParameter(int index)
  {
    return mParams[index];
  }

  QObject* findParameter(const QString& name);
 
  int getCount() const { return mParams.count(); }

private:
  QList<Parameter*> mParams;
};


#endif
