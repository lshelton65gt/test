#ifndef DLGNEWPARAM_H
#define DLGNEWPARAM_H

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "util/OSWrapper.h"

#include <QDialog>
#include "ui_DlgNewParam.h"

#include "cfg/CarbonCfg.h"

class ParameterNameValidator;

class DlgNewParam : public QDialog
{
  Q_OBJECT

public:
  DlgNewParam(CarbonCfg* cfg, QWidget *parent = 0);
  ~DlgNewParam();

  QString getName() { return mName; }
  CarbonCfgParamDataType getType() { return mType; }
  QString getDescription() { return mDescription; }
  QString getEnumChoices() { return mEnumChoices; }
  QString getValue() { return mValue; }
  CarbonCfgParamFlag getScope() { return mScope; }

private slots:
 
    void on_lineEditParamName_textChanged(const QString &);
  void on_buttonBox_rejected();
  void on_buttonBox_accepted();

private:
  bool checkValues();

private:
  Ui::DlgNewParamClass ui;
  ParameterNameValidator* mValidator;
  CarbonCfgParamDataType mType;
  QString mName;
  QString mValue;
  QString mDescription;
  QString mEnumChoices;
  CarbonCfgParamFlag mScope;
  CarbonCfg* mCfg;
};

#endif // DLGNEWPARAM_H
