#ifndef __OPTIONALPORT_H
#define __OPTIONALPORT_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "Ports.h"
#include "Parameters.h"

class Template;
class Mode;
class Block;


class OptionalPort : public Port
{
  Q_OBJECT

  Q_PROPERTY(QObject* parameter READ getParameter)
  Q_PROPERTY(QString includeValue READ getIncludeValue)

public:
  enum ParameterRefType { RefNone, RefBlock, RefMode, RefTemplate };

  OptionalPort() : Port()
  {
    mParameter = NULL;
    mRefType = RefNone;
  }

  QString getIncludeValue() const { return mIncludeValue; }

  // override
  virtual bool isEnabled();
  virtual Port* copyPort();

  bool deserialize(Mode* mode, Template* templ, Block* block, Ports* ports, const QDomElement& e, XmlErrorHandler* eh);

private:
  void setIncludeValue(const QString& newVal) { mIncludeValue=newVal; }
  void setParameter(Parameter* newVal) { mParameter=newVal; }
  void setReferenceType(ParameterRefType newVal) { mRefType=newVal; }

private:
  Parameter* getParameter();

private:
  ParameterRefType mRefType;
  Parameter* mParameter;
  QString mIncludeValue;
};

#endif
