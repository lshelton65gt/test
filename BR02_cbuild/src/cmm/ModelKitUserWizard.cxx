//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "ModelKitUserWizard.h"
#include "CarbonMakerContext.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "CarbonFiles.h"
#include "ModelKit.h"

#define ENV_ARM_ROOT "${ARM_ROOT}"
#define ENV_PROJ_ROOT "${CARBON_PROJECT}"


void ModelKitUserWizard::cleanupFiles()
{
  MKUWPageFinished* finPage = dynamic_cast<MKUWPageFinished*>(page(ModelKitUserWizard::PageFinished));
  if (finPage)
    finPage->cleanupFiles();
}

void ModelKitAnswers::readAnswers(const QString& xmlFile)
{
  TempChangeDirectory cd(theApp->getWorkingDirectory());
  qDebug() << "Reading answers from" << xmlFile;
  QFile file(xmlFile);

  if (file.open(QFile::ReadOnly | QFile::Text))
  {
    QDomDocument doc;
    int errorLine;
    int errorCol;
    QString errStr;

    QFileInfo templateFi(xmlFile);

    if (doc.setContent(&file, true, &errStr, &errorLine, &errorCol))
    { 
      qDebug() << "Reading contents";
      QDomElement docElem = doc.documentElement();
      deserialize(docElem);
      deserializeSettings(docElem);
      deserializeActions(docElem);
      deserializeQuestions(docElem);
    }
    else
      qDebug() << "Error: Invalid XML file" << xmlFile;
    
    file.close();
  }
  else
    qDebug() << "Error: Unable to open answers file" << xmlFile;
}
void ModelKitAnswers::deserialize(const QDomElement& parent)
{
  mModelTarget = ModelKitAnswers::Unix;
  mModelSymbols = ModelKitAnswers::FullSymbols;

  QDomElement configs = parent.firstChildElement("Configuration");
  if (configs.isElement())
  {
    qDebug() << "Reading Configuration";

    QDomElement elemComponentName = configs.firstChildElement("ComponentName");
    if (elemComponentName.isElement())
     mComponentName = elemComponentName.text();
  
    QDomElement elemModelTarget = configs.firstChildElement("ModelTarget");
    if (elemModelTarget.isElement())
    {
      QString ModelTarget = elemModelTarget.text();
      if (ModelTarget == "Unix")
        mModelTarget = ModelKitAnswers::Unix;
      else if (ModelTarget == "Windows")
        mModelTarget = ModelKitAnswers::Windows;
    }

    QDomElement elemSymbols = configs.firstChildElement("Symbols");
    if (elemSymbols.isElement())
    {
      QString symbols = elemSymbols.text();
      if (symbols == "FullSymbols")
        mModelSymbols = ModelKitAnswers::FullSymbols;
      else if (symbols == "IOSymbols")
        mModelSymbols = ModelKitAnswers::IOSymbols;
    }
  }
}


void ModelKitAnswers::deserializeSettings(const QDomElement& parent)
{
  QDomElement settings = parent.firstChildElement("Settings");
  QDomElement e = settings.firstChildElement("Setting");
  while (e.isElement())
  {
    qDebug() << "Reading Setting";

    QString type = e.attribute("type");
    QString key = e.attribute("key");
    QVariant value(e.text());
    UtString typeName; typeName << type;
    value.convert(QVariant::nameToType(typeName.c_str()));

    UtString uValue; uValue << value.toString();
    UtString expandedName;
    UtString err;
    OSExpandFilename(&expandedName, uValue.c_str(), &err);
    value = expandedName.c_str();
    qDebug() << "restoring setting" << key << "to" << value.toString();
    mSettings[key] = value;
    e = e.nextSiblingElement("Setting");
  }

}
void ModelKitAnswers::deserializeActions(const QDomElement& parent)
{
  QDomElement actions = parent.firstChildElement("Actions");
  QDomElement e = actions.firstChildElement("Action");
  while (e.isElement())
  {
    QString id = e.attribute("id");
    mActions[id] = e;
    e = e.nextSiblingElement("Action");
  }
}

void ModelKitAnswers::deserializeQuestions(const QDomElement& parent)
{
  QDomElement questions = parent.firstChildElement("Questions");
  QDomElement e = questions.firstChildElement("Question");
  while (e.isElement())
  {
    QString type = e.attribute("type");
    QString id = e.attribute("id");
    QVariant value;
    value = e.text();
    if (type == "int")
      value.setValue(e.text().toInt());
    else if (type == "double")
      value.setValue(e.text().toDouble());
    else if (type == "filenames")
      value.setValue(e.text().split(","));

    qDebug() << "restoring question" << id << "to" << value;
    mQuestions[id] = value;
    e = e.nextSiblingElement("Question");
  }
}

QString ModelKitUserWizard::normalizeName(const QString& fileName)
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  // ARM Root always ends with '/', remove it in this case
  QString armRootKit = getARMRoot();
  QString rootKit = proj->getUnixEquivalentPath(armRootKit);
  if (rootKit.endsWith("/"))
    rootKit = rootKit.left(rootKit.length()-1);

  QString projRoot = proj->getUnixEquivalentPath(proj->getProjectDirectory());

  UtString tmp;
  tmp << fileName;
  UtString unixName;
  unixName << proj->getUnixEquivalentPath(tmp.c_str());

  QString inputName = unixName.c_str();

  QString name;
  if (!rootKit.isEmpty())
    name = inputName.replace(rootKit, ENV_ARM_ROOT);
  else
    name = inputName;

  name = name.replace(projRoot, ENV_PROJ_ROOT);

  if (name != fileName)
    qDebug() << "translated" << fileName << "to" << name;
  
  return name;
}

void ModelKitUserWizard::recordFilename(const QString& questionId, const QString& result)
{
  if (mMode == ModelKitUserWizard::Recording)
  {
    QString normalizedResult = normalizeName(result);
    qDebug() << "Recording Question" << questionId << normalizedResult;
    QDomElement questionElement = 
              XmlHelper::addElement(mDocRecord, mQuestionsRoot, "Question", normalizedResult);
    questionElement.setAttribute("id", questionId);
    questionElement.setAttribute("type", "filename");
  }
}

void ModelKitUserWizard::recordFilenames(const QString& questionId, const QStringList& results)
{
  if (mMode == ModelKitUserWizard::Recording)
  {
    QStringList normalizedNames;
    foreach(QString fn, results)
      normalizedNames.append(normalizeName(fn));
    qDebug() << "Recording Question" << questionId << results;
    QDomElement questionElement = 
              XmlHelper::addElement(mDocRecord, mQuestionsRoot, "Question", normalizedNames.join(","));
    questionElement.setAttribute("id", questionId);
    questionElement.setAttribute("type", "filenames");
  }
}

void ModelKitUserWizard::recordQuestion(const QString& questionId, const QString& result)
{
  if (mMode == ModelKitUserWizard::Recording)
  {
    qDebug() << "Recording Question" << questionId << result;
    QDomElement questionElement = 
              XmlHelper::addElement(mDocRecord, mQuestionsRoot, "Question", result);
    questionElement.setAttribute("id", questionId);
    questionElement.setAttribute("type", "string");
  }
}

void ModelKitUserWizard::recordQuestion(const QString& questionId, int result)
{
  if (mMode == ModelKitUserWizard::Recording)
  {
    qDebug() << "Recording Question" << questionId << result;
    QString value = QString("%1").arg(result);
    QDomElement questionElement =
              XmlHelper::addElement(mDocRecord, mQuestionsRoot, "Question", value);
    questionElement.setAttribute("id", questionId);
    questionElement.setAttribute("type", "int");
  }
}

void ModelKitUserWizard::recordQuestion(const QString& questionId, double result)
{
  if (mMode == ModelKitUserWizard::Recording)
  {
    qDebug() << "Recording Question" << questionId << result;
    QString value = QString("%1").arg(result);
    QDomElement questionElement = 
        XmlHelper::addElement(mDocRecord, mQuestionsRoot, "Question", value);
    questionElement.setAttribute("id", questionId);
    questionElement.setAttribute("type", "double");
  }
}

QDomElement ModelKitUserWizard::recordAction(KitUserAction* action)
{
  if (mMode == ModelKitUserWizard::Recording)
  {
    qDebug() << "Recording Action" << action->getID() << "name" << action->getName();
    QDomElement actionElement = XmlHelper::addElement(mDocRecord, mActionsRoot, "Action");
    actionElement.setAttribute("id", action->getID());
    actionElement.setAttribute("kind", action->getKindString());
    return actionElement;
  }
  else
    return QDomElement(); // Not Recording
}

QDomElement ModelKitUserWizard::recordConfiguration(const QString& key, const QVariant value)
{
  if (mMode == ModelKitUserWizard::Recording)
  {
    qDebug() << "Recording Configuration" << key << "value" << value.toString();
    QDomElement newElement = XmlHelper::addElement(mDocRecord, mConfigsRoot, key, value.toString());
    return newElement;
  }
  else
    return QDomElement(); // Not Recording
}

void ModelKitUserWizard::recordSetting(QObject* obj, const QString& key, const QVariant value)
{
  QString className = obj->metaObject()->className();

  if (mMode == ModelKitUserWizard::Recording)
  {
    qDebug() << "Recording Class" << className << "key" << key << "value" << value;
    QDomElement settingElement = XmlHelper::addElement(mDocRecord, mSettingsRoot, "Setting", value.toString());

    settingElement.setAttribute("key", key);
    settingElement.setAttribute("type", value.typeName());
  }
}

void ModelKitUserWizard::startRecording()
{
  mRecordRoot = mDocRecord.createElement("ModelKitRecording");
  mRecordRoot.setAttribute("version", "1");
  mDocRecord.appendChild(mRecordRoot);

  mConfigsRoot = XmlHelper::addElement(mDocRecord, mRecordRoot, "Configuration");
  mSettingsRoot = XmlHelper::addElement(mDocRecord, mRecordRoot, "Settings");
  mActionsRoot = XmlHelper::addElement(mDocRecord, mRecordRoot, "Actions");
  mQuestionsRoot =  XmlHelper::addElement(mDocRecord, mRecordRoot, "Questions");
}

void ModelKitUserWizard::showEvent(QShowEvent* ev)
{
  QWizard::showEvent(ev);

  qDebug() << "Show Event" << ev;
}

bool ModelKitUserWizard::event(QEvent* ev)
{
  bool retValue = QWizard::event(ev);

  if (ev->type() == QEvent::WindowBlocked)
  {
    if (getAnswers())
    {
      qDebug() << "ERROR: Unexpected Modal Dialog";
      if (getenv("ALLOW_UNEXPECTED_MODAL_DIALOGS") == NULL)
        theApp->shutdown(3);
    }
  }

  if (ev->type() == QEvent::ShowToParent)
  {
    if (getAnswers())
    {
      if (!theApp->checkArgument("-debugWindows"))
        hide();
    }
  }

  return retValue;
}
void ModelKitUserWizard::finishRecording()
{
  if (mMode == ModelKitUserWizard::Recording)
  {
    CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
    CarbonProject* proj = pw->project();

    QString projName = proj->shortName();
    QFileInfo fi(projName);

    QString recordFileName = QString("%1/%2.modelKitAnswers")
                              .arg(proj->getProjectDirectory())
                              .arg(fi.baseName());
        
    qDebug() << "Writing ModelKit Answers to" << recordFileName;

    QFile file(recordFileName);
    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
      QTextStream out(&file);
      out << mDocRecord.toString(2);
      file.close();
    }
  }
}

ModelKitUserWizard::ModelKitUserWizard(QWidget *parent, ModelKit* kit, KitManifest* manifest)
: QWizard(parent), mUserContext(this, this, manifest)
{
  ui.setupUi(this);

  mModelKit = kit;
  mModelTarget = ModelKitUserWizard::Unix;
  mModelSymbols = ModelKitUserWizard::FullSymbols;
  mComponentName = kit->getComponentName();
  mUserContext.setComponentName(mComponentName);

  qDebug() << "Model Kit User Wizard Constructed";

  mMode = ModelKitUserWizard::Recording;
  mWhen = ModelKitUserWizard::Precompilation;
  mManifest = manifest;
  mAnswers = NULL;

  if (theApp->checkArgument("-modelKitAnswers"))
  {
    QString answersFile = theApp->argumentValue("-modelKitAnswers");
    mAnswers = new ModelKitAnswers(mManifest);
    mAnswers->readAnswers(answersFile);
    mMode = ModelKitUserWizard::Playback;
    qDebug() << "Enabling shutdown on unexpected modal dialogs";
    theApp->setShutdownOnDialog(true);
  }

  QString title = QString("Carbon Model Kit %1 Revision: %2, Version: %3")
    .arg(kit->getName())
    .arg(kit->getRevision())
    .arg(kit->getLatestVersion()->getVendorVersion());

  setWindowTitle(title);

  startRecording();
  
#ifndef Q_WS_MAC
  setWizardStyle(ModernStyle);
#endif

  qDebug() << "launched User ModelKit Wizard";

  setPage(ModelKitUserWizard::PageConfig, new MKUWConfigPage(this));

  if (manifest->getAskRoot())
    setPage(ModelKitUserWizard::PageStart, new MKUWPageStart(this));
  
  if (manifest->getCopyFiles())
    setPage(ModelKitUserWizard::PageClone, new MKUWPageClone(this));
  
  if (manifest->getPreUserActions().count() > 0)
   setPage(ModelKitUserWizard::PageUserActions, new MKUWPageUserActions(this));

  setPage(ModelKitUserWizard::PageFinished, new MKUWPageFinished(this));

  // These must be done after the setPage calls
  // or QT doesn't like it for some reason.
  setStartId(ModelKitUserWizard::PageConfig);
}

ModelKitUserWizard::~ModelKitUserWizard()
{
  qDebug() << "User wizard closing";
}
