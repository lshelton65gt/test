//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2014 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "DlgPreferences.h"

#include "cmm.h" 
#include "CarbonMakerContext.h" 
#include "CarbonProjectWidget.h" 

#include <Qsci/qscilexercpp.h>
#include <Qsci/qscilexerhtml.h>
#include <Qsci/qscilexermakefile.h>
#include <Qsci/qscilexerjavascript.h>
#include <Qsci/qscilexerbash.h>
#include <Qsci/qscilexerbatch.h>
#include <Qsci/qscilexervhdl.h>
#include "qscilexerverilog.h"


#define PREF_GEN_NUMVARS "general/NumVariables"

#define GENERAL_TAB_SIZE "editPrefsGeneral/TabSize"
#define GENERAL_INDENT_SIZE "editPrefsGeneral/IndentSize"
#define GENERAL_CONVERT_TABS "editPrefsGeneral/ConvertTabs"

#define GENERAL_VIEW_WS "editPrefsGeneral/ViewWS"
#define GENERAL_VIEW_EOL "editPrefsGeneral/ViewEOL"
#define GENERAL_VIEW_IG "editPrefsGeneral/ViewIG"
#define GENERAL_VIEW_LN "editPrefsGeneral/ViewLN"
#define GENERAL_VIEW_FM "editPrefsGeneral/ViewFM"
#define GENERAL_VIEW_BM "editPrefsGeneral/ViewBM"
#define GENERAL_VIEW_AI "editPrefsGeneral/ViewAI"
#define GENERAL_VIEW_ISV "editPrefsGeneral/ViewISV"

class BlockSignals
{
public:
  BlockSignals(QWidget* w)
  {
    mBlocked = w->signalsBlocked();
    mWidget = w;
    mWidget->blockSignals(true);
  }
  ~BlockSignals()
  {
    mWidget->blockSignals(mBlocked);
  }
private:
  bool mBlocked;
  QWidget* mWidget;
};


#define BLOCK_UI() \
  BlockSignals b1(ui.listWidgetStyles); \
  BlockSignals b2(ui.spinBoxFontSize); \
  BlockSignals b3(ui.checkBoxItalic); \
  BlockSignals b4(ui.fontComboBox)

DlgPreferences::DlgPreferences(QWidget *parent)
: QDialog(parent)
{
  mInitialized = false;

  ui.setupUi(this);

  QListWidgetItem* item0 = new QListWidgetItem(ui.listWidget);
  item0->setText("General");
  item0->setIcon(QIcon(":/cmm/Resources/process.png"));
  item0->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

  QListWidgetItem* item1 = new QListWidgetItem(ui.listWidget);
  item1->setText("Text Editor");
  item1->setIcon(QIcon(":/cmm/Resources/Preferences.png"));
  item1->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

  mStyleNumber = -1;
  mLexer = NULL;

  readSettings();

  setupLexers();

  populateLanguages();

  initializeValues();

  langStyleChanged(0);

  mInitialized = true;

  ui.listWidgetStyles->setCurrentRow(0);
}

void DlgPreferences::on_listWidget_currentItemChanged(QListWidgetItem* current, QListWidgetItem* previous)
{
  if (!current)
    current = previous;

  ui.stackedWidget->setCurrentIndex(ui.listWidget->row(current));
}

DlgPreferences::~DlgPreferences()
{
}


void DlgPreferences::apply()
{
  writeSettings(); 

  applyValues();
  
  if (mLexer)
    mLexer->writeSettings(mSettings, TE_LANGUAGE_PREFIX);

  theApp->signalEditorSettingsChanged(this);
}
void DlgPreferences::on_buttonBox_accepted()
{
  apply();
  accept();
}

void DlgPreferences::on_buttonBox_rejected()
{
  reject();
}

void DlgPreferences::initializeValues()
{
  mReopenLastProject = mSettings.value(PREF_GEN_REOPENLAST, true);
  mAutoLoadHierarchy = mSettings.value(PREF_GEN_AUTOLOADHIER, true);
  mAutoLoadChangedFiles = mSettings.value(PREF_GEN_AUTOLOADFILE, false);

  mNumberOfLogs = mSettings.value(PREF_GEN_NUMLOGS, 5);

  // No longer supported ui.comboBoxVSTypes->addItem("VS2005", QVariant("VS80COMNTOOLS"));
  ui.comboBoxVSTypes->addItem("VS2008", QVariant("VS90COMNTOOLS"));
  ui.comboBoxVSTypes->addItem("VS2010", QVariant("VS100COMNTOOLS"));

  mVSVersion = mSettings.value(PREF_VS_VERSION, tr("VS90COMNTOOLS"));

  int ci = ui.comboBoxVSTypes->findData(mVSVersion);
  if (ci != -1)
    ui.comboBoxVSTypes->setCurrentIndex(ci);


  {
    ui.comboBoxHomePage->addItem("Default", QVariant("Default"));
    ui.comboBoxHomePage->addItem("Local", QVariant("Local"));

    mHomePage = mSettings.value(PREF_GEN_HOMEPAGE, tr("Default"));

    int ci = ui.comboBoxHomePage->findData(mHomePage);
    if (ci != -1)
      ui.comboBoxHomePage->setCurrentIndex(ci);
  }


  bool varScope = mSettings.value(PREF_GEN_VARSCOPE, true).toBool();
  ui.radioButtonPerProject->setChecked(varScope);
  ui.radioButtonGlobalProject->setChecked(!varScope);

  if (mReopenLastProject.toBool())
    ui.checkBoxReopenProject->setCheckState(Qt::Checked);

  ui.treeWidget->setSortingEnabled(false);

  const QMap<QString,QString>& envVars = theApp->getEnvironment();

  foreach(QString varName, envVars.keys())
  {
    QString varValue = envVars[varName];
    if (varValue.isEmpty())
      continue;

    QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidget);
    item->setText(0, varName);
    item->setText(1, varValue);
    item->setToolTip(1, varValue);

    item->setFlags(item->flags() | Qt::ItemIsUserCheckable);

    QString var = theApp->lookupVariable(varName);
    if (var.isEmpty())
      item->setCheckState(0, Qt::Unchecked);
    else
      item->setCheckState(0, Qt::Checked);
  }

  ui.treeWidget->setSortingEnabled(true);
  ui.treeWidget->sortByColumn(0, Qt::AscendingOrder);

  ui.treeWidget->header()->resizeSections(QHeaderView::ResizeToContents);
  ui.treeWidget->header()->setResizeMode(0, QHeaderView::Interactive);
  ui.treeWidget->header()->setResizeMode(1, QHeaderView::Interactive);

  //// Do we have a project?
  //CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  //if (pw && pw->project())
  //{
  //  CarbonProject* proj = pw->project();
  //  QProcess p;

  //  for
  //  qDebug() << QProcess::systemEnvironment();
  //}
}

void DlgPreferences::applyValues()
{
  theApp->setVariablesScope(ui.radioButtonPerProject->isChecked());
  mSettings.setValue(PREF_GEN_VARSCOPE, ui.radioButtonPerProject->isChecked());

  QString varVSVersion = ui.comboBoxVSTypes->itemData(ui.comboBoxVSTypes->currentIndex()).toString();
  // theApp->signalPreferenceChanged(PREF_VS_VERSION, varVSVersion);
  mSettings.setValue(PREF_VS_VERSION, varVSVersion);

  QString varHomePage = ui.comboBoxHomePage->itemData(ui.comboBoxHomePage->currentIndex()).toString();
  mSettings.setValue(PREF_GEN_HOMEPAGE, varHomePage);

  int logSize = ui.spinBoxLogs->value();
  if (logSize != mNumberOfLogs.toInt())
  {
    theApp->signalPreferenceChanged(PREF_GEN_NUMLOGS, logSize);
    mSettings.setValue(PREF_GEN_NUMLOGS, logSize);
  }

  bool reopen = ui.checkBoxReopenProject->isChecked();
  if (reopen != mReopenLastProject.toBool())
  {
    theApp->signalPreferenceChanged(PREF_GEN_REOPENLAST, reopen);
    mSettings.setValue(PREF_GEN_REOPENLAST, reopen);
  }

  bool autoLoad = ui.checkBoxAutoLoadHierarchy->isChecked();
  if (autoLoad != mAutoLoadHierarchy.toBool())
  {
    theApp->signalPreferenceChanged(PREF_GEN_AUTOLOADHIER, autoLoad);
    mSettings.setValue(PREF_GEN_AUTOLOADHIER, autoLoad);
  }

  bool autoLoadFiles = ui.checkBoxAutoLoadChangedFiles->isChecked();
  if (autoLoadFiles != mAutoLoadChangedFiles.toBool())
  {
    theApp->signalPreferenceChanged(PREF_GEN_AUTOLOADFILE, autoLoadFiles);
    mSettings.setValue(PREF_GEN_AUTOLOADFILE, autoLoadFiles);
  }


  theApp->removeAllEnvironmentVariables();
  int numVars = 0;
  for (int i=0; i<ui.treeWidget->topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = ui.treeWidget->topLevelItem(i);
    if (item->checkState(0) == Qt::Checked)
    {
      qDebug() << "Added Env. Var Reference" << item->text(0);
      theApp->addEnvVar(item->text(0));
      if (!theApp->variablesScope())
      {
        QString varPrefix = QString("env_variable_%1").arg(numVars);
        mSettings.setValue(varPrefix, item->text(0));
        ++numVars;
      }
    }
    if (!theApp->variablesScope())
      mSettings.setValue(PREF_GEN_NUMVARS, numVars);
  }
}

void DlgPreferences::populateLanguages()
{
  ui.comboBoxLanguages->clear();

  foreach (QsciLexer* lex, mLexers)
  {
    QVariant qv = qVariantFromValue((void*)lex);
    QString lang = lex->language();
    ui.comboBoxLanguages->addItem(lang, qv);
  }
}

QVariant DlgPreferences::getViewPreference(ViewPreference pref)
{
  QVariant value;

  switch (pref)
  {
  case ViewWhitespace:            value = mSettings.value(GENERAL_VIEW_WS, false).toBool(); break;
  case ViewEndOfLine:             value = mSettings.value(GENERAL_VIEW_EOL, false).toBool(); break;
  case ViewIndentationGuide:      value = mSettings.value(GENERAL_VIEW_IG, false).toBool(); break;
  case ViewLineNumbers:           value = mSettings.value(GENERAL_VIEW_LN, true).toBool(); break;
  case ViewFoldMargin:            value = mSettings.value(GENERAL_VIEW_FM, true).toBool(); break;
  case ViewAutoIndent:            value = mSettings.value(GENERAL_VIEW_AI, true).toBool(); break;
  case ViewBraceMatching:         value = mSettings.value(GENERAL_VIEW_BM, true).toBool(); break;
  case ViewIntellisenseVariables: value = mSettings.value(GENERAL_VIEW_ISV, true).toBool(); break;
  }

  return value;
}

//  enum GeneralPreference { GeneralTabSize, GeneralIndentSize, GeneralConvertTabs};
//  enum ViewPreference { ViewWhitespace, ViewEndOfLine, ViewIndentationGuide, ViewLineNumbers, ViewFoldMargin };

void DlgPreferences::setGeneralPreference(GeneralPreference pref, const QVariant& value)
{
  qDebug() << "setting pref" << pref << "value" << value;

  switch (pref)
  {
  case GeneralTabSize:      mSettings.setValue(GENERAL_TAB_SIZE, value); break;
  case GeneralIndentSize:   mSettings.setValue(GENERAL_INDENT_SIZE, value); break;
  case GeneralConvertTabs:  mSettings.setValue(GENERAL_CONVERT_TABS, value); break;
  case GeneralVariableScope: mSettings.setValue(PREF_GEN_VARSCOPE, value); break;
  case GeneralHomePage:      mSettings.setValue(PREF_GEN_HOMEPAGE, value); break;
  case GeneralVSVersion:     mSettings.setValue(PREF_VS_VERSION, value); break;
  case AutoLoadHierarchy:    mSettings.setValue(PREF_GEN_AUTOLOADHIER, value); break;
  case AutoLoadChangedFiles: mSettings.setValue(PREF_GEN_AUTOLOADFILE, value); break;
  }
}

QVariant DlgPreferences::getGeneralPreference(GeneralPreference pref)
{
  QVariant value;

  switch (pref)
  {
  case GeneralTabSize:      value = mSettings.value(GENERAL_TAB_SIZE, 4).toInt(); break;
  case GeneralIndentSize:   value = mSettings.value(GENERAL_INDENT_SIZE, 4).toInt(); break;
  case GeneralConvertTabs:  value = mSettings.value(GENERAL_CONVERT_TABS, false).toBool(); break;
  case GeneralVariableScope: value = mSettings.value(PREF_GEN_VARSCOPE, true).toBool(); break;
  case GeneralHomePage:      value = mSettings.value(PREF_GEN_HOMEPAGE, mHomePage).toString(); break;
  case GeneralVSVersion:     value = mSettings.value(PREF_VS_VERSION, mVSVersion).toString(); break;
  case AutoLoadHierarchy:     value = mSettings.value(PREF_GEN_AUTOLOADHIER, true).toBool(); break;
  case AutoLoadChangedFiles:     value = mSettings.value(PREF_GEN_AUTOLOADFILE, false).toBool(); break;
  }

  return value;
}


void DlgPreferences::writeSettings()
{
  mSettings.setValue(GENERAL_TAB_SIZE, ui.spinBoxTabSize->value());
  mSettings.setValue(GENERAL_INDENT_SIZE, ui.spinBoxIndentSize->value());
  mSettings.setValue(GENERAL_CONVERT_TABS, ui.checkBoxUseSpaces->isChecked());
  mSettings.setValue(PREF_GEN_VARSCOPE, ui.radioButtonPerProject->isChecked());
  mSettings.setValue(PREF_GEN_AUTOLOADHIER, ui.checkBoxAutoLoadHierarchy->isChecked());
  mSettings.setValue(PREF_GEN_AUTOLOADFILE, ui.checkBoxAutoLoadChangedFiles->isChecked());
  mSettings.setValue(PREF_GEN_REOPENLAST, ui.checkBoxReopenProject->isChecked());

  mSettings.setValue(GENERAL_VIEW_WS, ui.checkBoxViewWS->isChecked());
  mSettings.setValue(GENERAL_VIEW_EOL, ui.checkBoxViewEOL->isChecked());
  mSettings.setValue(GENERAL_VIEW_IG, ui.checkBoxViewIG->isChecked());
  mSettings.setValue(GENERAL_VIEW_LN, ui.checkBoxViewLN->isChecked());
  mSettings.setValue(GENERAL_VIEW_FM, ui.checkBoxViewFM->isChecked());
  mSettings.setValue(GENERAL_VIEW_BM, ui.checkBoxViewAI->isChecked());
  mSettings.setValue(GENERAL_VIEW_AI, ui.checkBoxViewBM->isChecked());
  mSettings.setValue(GENERAL_VIEW_ISV, ui.checkBoxViewISV->isChecked());


}

void DlgPreferences::readSettings()
{
  ui.spinBoxTabSize->setValue(mSettings.value(GENERAL_TAB_SIZE, 4).toInt());
  ui.spinBoxIndentSize->setValue(mSettings.value(GENERAL_INDENT_SIZE, 4).toInt());
  ui.checkBoxUseSpaces->setChecked(mSettings.value(GENERAL_CONVERT_TABS, false).toBool());

  // False Defaults
  ui.checkBoxViewWS->setChecked(mSettings.value(GENERAL_VIEW_WS, false).toBool());
  ui.checkBoxViewEOL->setChecked(mSettings.value(GENERAL_VIEW_EOL, false).toBool());
  ui.checkBoxViewIG->setChecked(mSettings.value(GENERAL_VIEW_IG, false).toBool());
  ui.checkBoxAutoLoadChangedFiles->setChecked(mSettings.value(PREF_GEN_AUTOLOADFILE, false).toBool());

  // True Defaults
  ui.checkBoxAutoLoadHierarchy->setChecked(mSettings.value(PREF_GEN_AUTOLOADHIER, true).toBool());
  ui.checkBoxReopenProject->setChecked(mSettings.value(PREF_GEN_REOPENLAST, true).toBool());

  ui.checkBoxViewLN->setChecked(mSettings.value(GENERAL_VIEW_LN, true).toBool());
  ui.checkBoxViewFM->setChecked(mSettings.value(GENERAL_VIEW_FM, true).toBool());
  ui.checkBoxViewBM->setChecked(mSettings.value(GENERAL_VIEW_BM, true).toBool());
  ui.checkBoxViewAI->setChecked(mSettings.value(GENERAL_VIEW_AI, true).toBool());
  ui.checkBoxViewISV->setChecked(mSettings.value(GENERAL_VIEW_ISV, true).toBool());

  ui.radioButtonPerProject->setChecked(mSettings.value(PREF_GEN_VARSCOPE, true).toBool());
  ui.radioButtonPerProject->setChecked(!ui.radioButtonPerProject->isChecked());

  theApp->setVariablesScope(ui.radioButtonGlobalProject->isChecked());

  if (!theApp->variablesScope())
  {
    theApp->removeAllEnvironmentVariables();
    int numVars = (int)mSettings.value(PREF_GEN_NUMVARS, 0).toUInt();

    for (int i=0; i<numVars; i++)
    {
      QString varPrefix = QString("env_variable_%1").arg(i);
      QString varName = mSettings.value(varPrefix, "").toString();
      if (!varName.isEmpty())
        theApp->addEnvVar(varName);
    }
  }
}

void DlgPreferences::setupLexers()
{
  mLexers.append(new QsciLexerVLOG());
  mLexers.append(new QsciLexerVHDL());
  mLexers.append(new QsciLexerJavaScript());
  mLexers.append(new QsciLexerCPP());
  mLexers.append(new QsciLexerMakefile());
  mLexers.append(new QsciLexerBash());
  mLexers.append(new QsciLexerBatch());
  mLexers.append(new QsciLexerXML());

  // Set defaults
  foreach (QsciLexer* lex, mLexers)
  {
    lex->readSettings(mSettings, TE_LANGUAGE_PREFIX);
  }
}

void DlgPreferences::on_comboBoxLanguages_currentIndexChanged(int index)
{
  mStyleNumber = -1;

  QVariant qv = ui.comboBoxLanguages->itemData(index);
  mLexer = (QsciLexer*)qv.value<void*>();

  ui.listWidgetStyles->blockSignals(true);
  ui.listWidgetStyles->clear();

  if (mLexer)
  {
    for (int i=0; i<128; i++)
    {
      QString description = mLexer->description(i);
      //if (description.isEmpty())
      //  break;

      if (!description.isEmpty())
      {
        QListWidgetItem* item = new QListWidgetItem(description);
        item->setData(Qt::UserRole, i);
        ui.listWidgetStyles->addItem(item);
      }
    }
  }
  ui.listWidgetStyles->blockSignals(false);
  
  ui.listWidgetStyles->setCurrentRow(0);

  showStyle(mStyleNumber);
}


void DlgPreferences::langStyleChanged(int itemNumber)
{
  if (itemNumber == 0)
    mStyleNumber = -1;
  else
    mStyleNumber = itemNumber;

  showStyle(mStyleNumber);
}


void DlgPreferences::showDefaultStyles()
{
  BLOCK_UI();

  QFont font = mLexer->defaultFont();
  ui.fontComboBox->setCurrentFont(font);

  int fontSize = font.pointSize();
  ui.spinBoxFontSize->setValue(fontSize);

  ui.checkBoxBold->setChecked(font.bold());
  ui.checkBoxItalic->setChecked(font.italic());

  QColor color = mLexer->defaultColor();
  ui.labelFGName->setText(color.name());
  ui.labelFG->setPalette(QPalette(color));
  ui.labelFG->setAutoFillBackground(true);

  QColor paperColor = mLexer->defaultPaper();
  ui.labelBGName->setText(paperColor.name());
  ui.labelBG->setPalette(QPalette(paperColor));
  ui.labelBG->setAutoFillBackground(true);
}

void DlgPreferences::showStyle(int style)
{
  if (mLexer == NULL)
    return;

  if (style == -1)
  {
    showDefaultStyles();
    return;
  }

  BLOCK_UI();

  QFont font = mLexer->font(style);
  ui.fontComboBox->setCurrentFont(font);

  int fontSize = font.pointSize();
  ui.spinBoxFontSize->setValue(fontSize);

  ui.checkBoxBold->setChecked(font.bold());
  ui.checkBoxItalic->setChecked(font.italic());

  QColor color = mLexer->color(style);
  if (color.isValid())
  {
    qDebug() << "foreground" << color.name();
    ui.labelFGName->setText(color.name());
    ui.labelFG->setPalette(QPalette(color));
    ui.labelFG->setAutoFillBackground(true);
  }

  QColor paperColor = mLexer->paper(style);
  if (paperColor.isValid())
  {
    qDebug() << "background" << paperColor.name();
    ui.labelBGName->setText(paperColor.name());
    ui.labelBG->setPalette(QPalette(paperColor));
    ui.labelBG->setAutoFillBackground(true);
  }
}

void DlgPreferences::on_pushButtonFG_clicked()
{
  QColor color;
  if (mStyleNumber == -1)
    color = mLexer->defaultColor();
  else
    color = mLexer->color(mStyleNumber);
  
  QColor newColor = QColorDialog::getColor(color, this);
  if (newColor.isValid())
  {
    qDebug() << "new fg" << newColor.name();

    if (mStyleNumber == -1)
      mLexer->setDefaultColor(newColor);
    else
    {
      mLexer->setColor(newColor, mStyleNumber);
    }
    showStyle(mStyleNumber);
  }
}

void DlgPreferences::on_pushButtonBG_clicked()
{
  QColor color;
  if (mStyleNumber == -1)
    color = mLexer->defaultPaper();
  else
    color = mLexer->paper(mStyleNumber);
  
  QColor newColor = QColorDialog::getColor(color, this);
  if (newColor.isValid())
  {
    qDebug() << "new bg" << newColor.name();

    if (mStyleNumber == -1)
      mLexer->setDefaultPaper(newColor);
    else
      mLexer->setPaper(newColor, mStyleNumber);
    showStyle(mStyleNumber);
  }
}

void DlgPreferences::on_pushButtonReset_clicked()
{
  if (mLexer)
    mLexer->readSettings(mSettings);

  ui.listWidgetStyles->setCurrentRow(0);
}

void DlgPreferences::updateFont()
{
  if (mStyleNumber == -1)
  {
    mLexer->setDefaultFont(ui.fontComboBox->currentFont());
    int fontSize = ui.spinBoxFontSize->value();
    if (fontSize > 0)
    {
      for (int styleNumber=0; styleNumber<128; styleNumber++)
      {
        QFont font = mLexer->font(styleNumber);
        font.setPointSize(fontSize);
        mLexer->setFont(font, styleNumber);
      }
    }
  }
  else
  {
    QFont font = mLexer->font(mStyleNumber);
  
    int fontSize = ui.spinBoxFontSize->value();

    if (fontSize > 0)
      font.setPointSize(fontSize);

    font.setBold(ui.checkBoxBold->isChecked());
    font.setItalic(ui.checkBoxItalic->isChecked());

    mLexer->setFont(font, mStyleNumber);

    showStyle(mStyleNumber);
  }
}

void DlgPreferences::updateFont(const QString& newFontName)
{
  QFont newFont(newFontName);

  if (mStyleNumber == -1)
  {
    for (int styleNumber=0; styleNumber<128; styleNumber++)
    {
      QFont font = mLexer->font(styleNumber);
      newFont.setBold(font.bold());
      newFont.setItalic(font.italic());
      mLexer->setFont(newFont, styleNumber);
    }
  }
  else
    mLexer->setFont(newFont, mStyleNumber);

  updateFont();

  showStyle(mStyleNumber);
}

void DlgPreferences::on_fontComboBox_textChanged(const QString &newFont)
{
  BLOCK_UI();
  updateFont(newFont);
}

void DlgPreferences::on_checkBoxBold_stateChanged(int)
{
  BLOCK_UI();
  updateFont();
}

void DlgPreferences::on_checkBoxItalic_stateChanged(int)
{
  BLOCK_UI();
  updateFont();
}

void DlgPreferences::on_spinBoxFontSize_valueChanged(int)
{
  BLOCK_UI();
  updateFont();
}

void DlgPreferences::on_buttonBox_clicked(QAbstractButton* btn)
{
  QPushButton* btnApply = ui.buttonBox->button(QDialogButtonBox::Apply);

  if (btnApply == btn)
  {
    apply();
  }
  qDebug() << "clicked" << btn;
}


void DlgPreferences::on_listWidgetStyles_itemSelectionChanged()
{
  BLOCK_UI();

  QListWidgetItem* item = ui.listWidgetStyles->currentItem();
  int index = item->data(Qt::UserRole).toInt();
  langStyleChanged(index);
}


