//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "MdiCoware.h"
#include <QtGui>
#include <QMenuBar>
#include <QToolBar>
#include "gui/CQt.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "SettingsEditor.h"
#include "CowareWizardWidget.h"
#include "ModelStudioCommon.h"

MDICowareTemplate::MDICowareTemplate(CarbonMakerContext* ctx)
: MDIDocumentTemplate(COWARE_COMPONENT_NAME, "New CoWare Component", "") 
{
  mContext = ctx;  
}

void MDICowareTemplate::updateMenusAndToolbars(QWidget* /*widget*/)
{
}

int MDICowareTemplate::createToolbars()
{
  return 0;
}

int MDICowareTemplate::createMenus()
{
  return 0;
}

MDIWidget* MDICowareTemplate::createNewDocument(QWidget* /*parent*/)
{
  return NULL;
}

// We don't have a MDI document presence, only the docking window widget
// so we create the InvisibleProject here to handle menu & toolbar events
//
MDIWidget* MDICowareTemplate::openDocument(QWidget* parent, const char* docName)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  CowareWizardWidget* widget = new CowareWizardWidget(mContext, this, parent);

  if (widget->hasCowareEnvironment())
  {
    bool status = widget->loadFile(docName);

    QApplication::restoreOverrideCursor();

    if (status)
      return widget;
    else
    {
      widget->close();
      return NULL;
    }
  }
  else
  {
    QApplication::restoreOverrideCursor();

    widget->close();
    return NULL;
  }
}
