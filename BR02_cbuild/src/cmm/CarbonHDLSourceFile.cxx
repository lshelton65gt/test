//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "gui/CQt.h"

#include "CarbonHDLSourceFile.h"
#include "CarbonSourceGroups.h"
#include "CarbonProject.h"
#include "CarbonProjectNodes.h"
#include "CarbonFiles.h"

#include <QFileInfo>
#include <QRegExp>

#include "cmm.h" 
#include "CarbonMakerContext.h" 
#include "CarbonProjectWidget.h" 


CarbonHDLSourceFile::~CarbonHDLSourceFile()
{
}

CarbonHDLSourceFile::HDLType CarbonHDLSourceFile::getType(const char* relPath)
{
  HDLType hdlType = CarbonHDLSourceFile::Verilog;
  QRegExp rx("*.vhd*");
  rx.setPatternSyntax(QRegExp::Wildcard);
  if (rx.exactMatch(relPath))
    hdlType = CarbonHDLSourceFile::VHDL;
  QRegExp rx1("*.c*");
  rx1.setPatternSyntax(QRegExp::Wildcard);
  if (rx1.exactMatch(relPath))
    hdlType = CarbonHDLSourceFile::CPP;
  QRegExp rx2("*.h");
  rx2.setPatternSyntax(QRegExp::Wildcard);
  if (rx2.exactMatch(relPath))
    hdlType = CarbonHDLSourceFile::Header;

  return hdlType;
}


CarbonHDLSourceFile::CarbonHDLSourceFile(CarbonSourceGroup* group, const char* relativePath)
{
  mGroup=group;
  setRelativePath(relativePath);
  mLibraryFile=false;
}

// relPath *might* be relative, or not
// so here we figure that out
void CarbonHDLSourceFile::setRelativePath(const char* relPath)
{  
  CarbonProject* proj = getGroup()->getGroups()->getProject();

  TempChangeDirectory(proj->getProjectDirectory());

  mRelativePath = relPath;
  QFileInfo fi(mRelativePath.c_str());
  mCanonicalPath.clear();

  if (fi.exists())
  {

    if (fi.isRelative())
    {
      UtString absPath;
      OSConstructFilePath(&absPath, proj->getProjectDirectory(), relPath);
      QFileInfo nfi(absPath.c_str());
      mCanonicalPath << nfi.canonicalFilePath();
      //INFO_ASSERT(mCanonicalPath.length() > 0, "Expecting canonical path");
    }
    else // its an absolute path
    {
      TempChangeDirectory td(proj->getProjectDirectory());
      UtString value;
      value << CarbonProjectWidget::makeRelativePath(proj->getProjectDirectory(), fi.absoluteFilePath());
      mRelativePath = value;
      mCanonicalPath << fi.absoluteFilePath();
    }
  }
  else
    mCanonicalPath << relPath;
}

bool CarbonHDLSourceFile::serialize(xmlTextWriterPtr writer)
{
  xmlTextWriterStartElement(writer, BAD_CAST "File");

  QString normalizedPath = theApp->normalizePath(getRelativePath());

  xmlTextWriterWriteAttribute(writer, BAD_CAST "RelativePath", BAD_CAST qPrintable(normalizedPath));
  xmlTextWriterWriteAttribute(writer, BAD_CAST "LibraryFile", mLibraryFile ? BAD_CAST "true" : BAD_CAST "false");

  for (UInt32 i=0; i<mConfigs.size(); ++i)
  {
    CarbonHDLSourceConfiguration* config = mConfigs[i];
    if (config->getOptions()->numOptionsSet() > 0)
    {
      xmlTextWriterStartElement(writer, BAD_CAST "FileConfiguration");
      xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST config->getName());
      config->getOptions()->serialize(writer);
      xmlTextWriterEndElement(writer);
    }
  }

  xmlTextWriterEndElement(writer);

  return true;
}

bool CarbonHDLSourceFile::deserialize(CarbonSourceGroup* group, xmlNodePtr parent, UtXmlErrorHandler* eh)
{
  for (xmlNodePtr child = parent; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "File"))
    {
      UtString relPath;
      XmlParsing::getProp(child, "RelativePath", &relPath);

      bool libraryFile = false;
      UtString libFile;
      XmlParsing::getProp(child, "LibraryFile", &libFile);
      if (libFile == "true")
        libraryFile = true;
      else if (libFile == "false")
        libraryFile = false;

      HDLType type = getType(relPath.c_str());
      CarbonHDLSourceFile* src = NULL;

      switch (type)
      {
      case CarbonHDLSourceFile::Verilog:
        {
          src = new CarbonVerilogSource(group, relPath.c_str());
          group->addSource(src);
          break;
        }
      case CarbonHDLSourceFile::VHDL:
        {
          src = new CarbonVHDLSource(group, relPath.c_str());
          group->addSource(src);
          break;
        }
      case CarbonHDLSourceFile::Header:
        {
          src = new CarbonHSource(group, relPath.c_str());
          group->addSource(src);
          break;
        }
      case CarbonHDLSourceFile::CPP:
        {
          src = new CarbonCPPSource(group, relPath.c_str());
          group->addSource(src);
          break;
        }
      }
      INFO_ASSERT(src, "Expecting Source File Type");
      src->putLibraryFile(libraryFile);

      if (child->children)
        CarbonHDLSourceConfiguration::deserialize(src, child->children, eh);
    }
  }

  return true;
}

bool CarbonHDLSourceConfiguration::deserialize(CarbonHDLSourceFile* src, xmlNodePtr parent, UtXmlErrorHandler* /*eh*/)
{
  for (xmlNodePtr child = parent; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "FileConfiguration"))
    {
      UtString configName;
      XmlParsing::getProp(child, "Name", &configName);
      CarbonHDLSourceConfiguration* config = src->getConfiguration(configName.c_str());

      for (xmlNodePtr toolChild = child->children; toolChild != NULL; toolChild = toolChild->next) 
      {
        if (XmlParsing::isElement(toolChild, "Tool"))
        {
          UtString toolName;
          XmlParsing::getProp(toolChild, "Name", &toolName);
          INFO_ASSERT(0 == strcmp(toolName.c_str(), "VSPCompilerFile"), "Expecting VSPCompilerFile");

          for (xmlNodePtr optionChild = toolChild->children; optionChild != NULL; optionChild = optionChild->next)
          {
            if (XmlParsing::isElement(optionChild, "Option"))
            {
              UtString optionName;
              XmlParsing::getProp(optionChild, "Name", &optionName);
              for (xmlNodePtr valueChild = optionChild->children; valueChild != NULL; valueChild = valueChild->next)
              {
                if (XmlParsing::isElement(valueChild, "Value"))
                {
                  UtString value;
                  XmlParsing::getContent(valueChild, &value);
                  const CarbonProperty* carbonProp = config->getOptions()->findProperty(optionName.c_str());
                  if (carbonProp)
                    config->getOptions()->putValue(optionName.c_str(), value.c_str());
                  else
                    qDebug() << "ignoring obsolete property" << optionName.c_str();
                }
              }
            }
          }
        }
      }
    }
  }

  return true;
}


const char* CarbonHDLSourceFile::getAbsolutePath()
{
  mAbsPath.clear();

  QFileInfo fi(getRelativePath());
  UtString absPath;
  mAbsPath << fi.absoluteFilePath();

  return mAbsPath.c_str();
}

void CarbonHDLSourceFile::setPropertyValues(CarbonHDLSourceConfiguration* config)
{

  const CarbonProperty* fileProp1 = config->getOptions()->findProperty("Relative Path");
  INFO_ASSERT(fileProp1, "Missing Relative Path property");

  config->getOptions()->putValue(fileProp1, getRelativePath());

  QString rePath = getRelativePath();

  UtString absPath;
  QFileInfo fi(rePath);
  if (!rePath.contains(ENVVAR_REFERENCE) && fi.isRelative())
    absPath << fi.absoluteFilePath();
  else
    absPath << CarbonProject::fixVarReferences(rePath);

  CarbonProjectWidget* pw = getGroup()->getGroups()->getProject()->getProjectWidget();

  const CarbonProperty* fileProp2 = config->getOptions()->findProperty("Absolute Path");
  INFO_ASSERT(fileProp2, "Missing Relative Path property");
  UtString absoluteFilePath;
  absoluteFilePath << pw->project()->getWindowsEquivalentPath(absPath.c_str());

  config->getOptions()->putValue(fileProp2, absoluteFilePath.c_str());

  const CarbonProperty* fileProp3 = config->getOptions()->findProperty("Unix Path");
  INFO_ASSERT(fileProp3, "Missing Unix File Path property");

  const CarbonProperty* normProp = config->getOptions()->findProperty("Normalized Path");


#if pfWINDOWS

  // If we are remotely compiling
  if (pw->isRemoteCompilation())
  {
    UtString absolutePath = absPath;
    absPath.clear();
    absPath << pw->project()->getUnixEquivalentPath(absolutePath.c_str());
  }
#endif

  UtString normPath;
  normPath << theApp->normalizePath(absPath.c_str());
  config->getOptions()->putValue(normProp, normPath.c_str());

  config->getOptions()->putValue(fileProp3, absPath.c_str());

  const CarbonProperty* configProp = config->getOptions()->findProperty("Configuration");
  INFO_ASSERT(configProp, "Missing Configuration property");
  config->getOptions()->putValue(configProp, mGroup->getGroups()->getProject()->getActiveConfiguration());

  const CarbonProperty* libProp = config->getOptions()->findProperty("LibraryFile");
  INFO_ASSERT(libProp, "Missing LibraryFile property");
  config->getOptions()->putValue(libProp, isLibraryFile() ? "true" : "false");
}

void CarbonHDLSourceFile::addConfiguration(const char* newName)
{
  CarbonHDLSourceConfiguration* config = new CarbonHDLSourceConfiguration(this, newName);
  mConfigs.push_back(config);
}

void CarbonHDLSourceFile::removeConfiguration(const char* configName)
{
  CarbonHDLSourceConfiguration* config = NULL;
  for (UInt32 i=0; i<mConfigs.size(); ++i)
  {
    config = mConfigs[i];
    if (0 == strcmp(config->getName(), configName))
    {
      mConfigs.remove(config);
      break;   
    }
  }
}

CarbonHDLSourceConfiguration* CarbonHDLSourceFile::getConfiguration(const char* configName)
{
  bool bFoundConfig = false;
  CarbonHDLSourceConfiguration* config = NULL;
  for (UInt32 i=0; i<mConfigs.size(); ++i)
  {
    config = mConfigs[i];
    if (0 == strcmp(config->getName(), configName))
    {
      bFoundConfig=true;
      break;
    }
  }

  if (!bFoundConfig) // Make a new one
  {
    config = new CarbonHDLSourceConfiguration(this, configName);
    mConfigs.push_back(config);
  }

  INFO_ASSERT(config, "Fatal error");
  setPropertyValues(config);

  return config;
}

void CarbonHDLSourceConfiguration::valueChangedLibraryFile(const CarbonProperty* /* prop */, const char* newValue)
{
  mSource->putLibraryFile(0 == strcmp(newValue, "true"));
  CarbonProjectWidget* projWidget = mSource->getGroup()->getGroups()->getProject()->getProjectWidget();
  CarbonSourceItem* item = projWidget->findSource(mSource);
  if (item)
    item->updateIcon();
}

CarbonHDLSourceConfiguration::CarbonHDLSourceConfiguration(CarbonHDLSourceFile* src, const char* name)
{
  mSource=src;
  mName=name;
  CarbonOptions* parentOptions = src->getGroup()->getOptions(name);
  CarbonProperties* parentProps = src->getGroup()->getGroups()->getProperties();

  mOptions = new CarbonOptions(parentOptions, parentProps, "VSPCompilerFile");

  mOptions->registerPropertyChanged("LibraryFile", "valueChangedLibraryFile", this);
}

