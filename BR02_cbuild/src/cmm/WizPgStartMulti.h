#ifndef WIZPGSTARTMULTI_H
#define WIZPGSTARTMULTI_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "WizardPage.h"

#include <QWizardPage>
#include "ui_WizPgStartMulti.h"

#include "CarbonConsole.h"
#include "MemoryWizard.h"

class XDesignHierarchy;

class WizPgStartMulti : public WizardPage
{
  Q_OBJECT

public:
  WizPgStartMulti(QWidget *parent = 0);
  ~WizPgStartMulti();

private:
  void applyFilter();
  void readDesignHierarchy(const QString& fileName);
  void updateButtons();
  void invokeCbuildScan();
  bool checkDirExistence(const QString& dirPath);
  void saveSettings();
  void restoreSettings();
  void loadTemplates();

protected:
  virtual void initializePage();
  virtual bool validatePage();
  virtual bool isComplete() const;
  virtual int nextId() const { return MemoryWizard::PageMultiPortMap; }

private slots:
    void on_checkBoxCaseSensitiveExcl_stateChanged(int);
    void on_radioButtonRegExExcl_toggled(bool);
    void on_checkBoxCaseSensitive_stateChanged(int);
    void on_radioButtonRegEx_toggled(bool);
    void on_radioButtonWildcard_toggled(bool);
    void on_lineEditExcluding_textChanged(const QString &);
    void on_lineEditMatching_textChanged(const QString &);
    void on_treeWidget_itemChanged(QTreeWidgetItem*,int);
    void on_radioButtonSingleFile_toggled(bool);
    void on_pushButtonBrowseFile_clicked();
  void on_pushButtonCheckAll_clicked();
  void on_pushButtonUncheckAll_clicked();
  void on_pushButtonApply_clicked();
  void on_pushButtonBrowse_clicked();
  void on_radioButtonProject_clicked();
  void on_radioButtonSearchDir_clicked();

private:
  bool mIsValid;
  XDesignHierarchy* mDesignHierarchy;
  QSettings mSettings;

private:
  Ui::WizPgStartMultiClass ui;
};

#endif // WIZPGSTARTMULTI_H
