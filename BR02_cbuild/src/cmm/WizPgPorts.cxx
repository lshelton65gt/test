//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "MemoryWizard.h"
#include "WizPgPorts.h"
#include "WizPgUserPorts.h"
#include "WizardTemplate.h"
#include "WizardContext.h"
#include "Template.h"
#include "DlgAddWizardPort.h"
#include "WizPgPorts.h"
#include "DlgSelectModule.h"
#include "ElaboratedModel.h"
#include "BlockInstance.h"
#include "Ports.h"
#include "PortInstance.h"

#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "Ports.h"
#include "cmm.h"

WizPgPorts::WizPgPorts(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);
  mIsCompleted = false;

  registerField("replaceModuleName*", ui.lineEditReplaceModuleName);

  ui.treeWidgetModelPorts->setDragImmediate(true);
  ui.treeWidgetModelPorts->setEnableDrop(true);
  ui.treeWidgetModulePorts->setDragImmediate(true);
  ui.treeWidgetModulePorts->setEnableDrop(true);

  ui.treeWidgetModelPorts->setRootIsDecorated(false);
  ui.treeWidgetModulePorts->setRootIsDecorated(false);

  ui.treeWidgetModelPorts->setSelectionMode(QAbstractItemView::SingleSelection);
  ui.treeWidgetModelPorts->setSelectionBehavior(QAbstractItemView::SelectRows);
  ui.treeWidgetModelPorts->setEditTriggers(QAbstractItemView::DoubleClicked);

  CQT_CONNECT(ui.treeWidgetModelPorts, dropReceived(const char*,QTreeWidgetItem*),
              this, dropModulePort(const char*,QTreeWidgetItem*));

  CQT_CONNECT(ui.treeWidgetModulePorts, dropReceived(const char*,QTreeWidgetItem*),
              this, dropModelPort(const char*,QTreeWidgetItem*));



 // CQT_CONNECT(ui.treeWidgetPorts, itemSelectionChanged(), this, itemSelectionChanged());
}

WizPgPorts::~WizPgPorts()
{
}

void WizPgPorts::cleanupPage()
{
  qDebug() << "back from WizPgPorts";
}

bool WizPgPorts::serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  QDomElement tag = doc.createElement("WizardStep");
  tag.setAttribute("name", objectName());
  parent.appendChild(tag);
  
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardTemplate* templ = wiz->getTemplate();
  WizardContext* context = templ->getContext();
  ElaboratedModel* model = context->getModel();
  PortInstances* instances = model->getUniquePorts();

  addElement(doc, tag, "ReplaceModuleName", ui.lineEditReplaceModuleName->text());

  for (int i=0; i<instances->getCount(); i++)
  {
    PortInstance* portInst = instances->getAt(i);

    QDomElement portElement = addElement(doc, tag, "PortInstance", portInst->getUnelaboratedName());
    portElement.setAttribute("name", portInst->getPort()->getBaseName());
  }

  return true;
}

void WizPgPorts::setVisible(bool visible)
{
    WizardPage::setVisible(visible);

    QWizard* wiz = wizard();
    if (wiz)
    {
      if (visible) {
          wizard()->setButtonText(QWizard::CustomButton1, tr("&ReConfigure"));
          wizard()->setOption(QWizard::HaveCustomButton1, true);
          connect(wizard(), SIGNAL(customButtonClicked(int)),
                  this, SLOT(reconfigureButtonClicked()));
      } else {
          wizard()->setOption(QWizard::HaveCustomButton1, false);
          disconnect(wizard(), SIGNAL(customButtonClicked(int)),
                     this, SLOT(reconfigureButtonClicked()));
      }
    }
}

void WizPgPorts::reconfigureButtonClicked()
{
  QString msg = QString("Re-Configuring will cause you to lose your Port Mappings.\nAre you sure you want to do this?");

  if (QMessageBox::question(this, "Remodeling Wizard", msg, 
          QMessageBox::No|QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
          wizard()->back();
}

void WizPgPorts::initializePage()
{
  setTitle("Port Mappings");
  setSubTitle("Map this Model's port(s) to an existing Module's port(s)");

  mIsCompleted = true;
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  QString replayFile = wiz->getReplayFile();

  WizardTemplate* templ = wiz->getTemplate();
  mContext = templ->getContext();

  QString modeName = wiz->field("portMode").toString();
  mContext->setModeName(modeName);

  Mode* mode = mContext->getTemplate()->findMode(modeName);
  if (mode)
  {
    // Elaborate from the parameters now.
    if (!mContext->getModel()->elaborate(mContext, mode))
    {
      mIsCompleted = false;
      emit completeChanged();
    }
  }

  populate();

  if (replayFile.length() > 0)
  {
    QDomElement wizElem = wiz->findWizardStep(objectName());
    if (!wizElem.isNull())
    {
      bool performNext = true;
      QDomNode n = wizElem.firstChild();
      while(!n.isNull())
      {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) 
        {
          qDebug() << e.tagName();
          if (e.tagName() == "PortInstance")
          {
            QString unelabName = e.text();
            QString portName = e.attribute("name");

            foreach (QTreeWidgetItem* item, ui.treeWidgetModelPorts->findItems(unelabName, Qt::MatchExactly, colPORT))
            {
              QVariant v = item->data(0, Qt::UserRole);
              item->setText(colUSERPORT, portName);
              PortInstance* portInst = (PortInstance*)v.value<void*>();
              QString value = item->text(colUSERPORT);
              portInst->getPort()->setBaseName(item->text(colUSERPORT));
            }
          }
          else if (e.tagName() == "ReplaceModuleName")
          {
            QString moduleName = e.text();
            ui.lineEditReplaceModuleName->setText(moduleName);
          }
        }
        n = n.nextSibling();
      }
      if (performNext)
        wiz->postNext();
      else
        wiz->stopReplay();
    }
  }
}

bool WizPgPorts::validatePage()
{
  if (!mIsCompleted)
    return false;

  for (int i=0; i<ui.treeWidgetModelPorts->topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = ui.treeWidgetModelPorts->topLevelItem(i);
    QVariant v = item->data(0, Qt::UserRole);
    if (!v.isNull())
    {
      PortInstance* portInst = (PortInstance*)v.value<void*>();
      QString value = item->text(colUSERPORT);
      portInst->getPort()->setBaseName(item->text(colUSERPORT));

      qDebug() << "commitData" << portInst->getPort()->getName() << portInst->getUnelaboratedName();

      Port* port = portInst->getBlock()->getPorts()->findPort(portInst->getPort()->getName());
      if (port)
        port->setBaseName(item->text(colUSERPORT));
    }
  }  

  if (ui.lineEditReplaceModuleName->text().length() == 0)
  {
    mIsCompleted = false;
    return false;
  }

  return true;
}

bool WizPgPorts::isComplete() const
{
  if (ui.lineEditReplaceModuleName->text().length() == 0)
   return false;

  return mIsCompleted;
}

int WizPgPorts::nextId() const
{
  if (mIsCompleted)
    return MemoryWizard::PageConfigurations;
  else
    return MemoryWizard::PageParameters;
}


void WizPgPorts::dropModulePort(const char* name, QTreeWidgetItem* item)
{
  // Figure which port we are depositing to.
  if (item != NULL) 
  {
    item->setText(1, name);
    qDebug() << "Dropped" << name << item->text(1);
  }
}

void WizPgPorts::dropModelPort(const char* name, QTreeWidgetItem* item)
{
  // Figure which port we are depositing to.
  if (item != NULL) 
  {
    QString newName = item->text(0);
    for (int i=0; i<ui.treeWidgetModelPorts->topLevelItemCount(); i++)
    {
      QTreeWidgetItem* modelItem = ui.treeWidgetModelPorts->topLevelItem(i);
      if (0 == modelItem->text(0).compare(name))
      {
        modelItem->setText(1, newName);
      }
    }
    qDebug() << "Dropped" << name << item->text(0);
  }
}

void WizPgPorts::updateButtons()
{
  int moduleItems = ui.treeWidgetModulePorts->selectedItems().count();
  int modelItems = ui.treeWidgetModelPorts->selectedItems().count();

  ui.pushButtonMap->setEnabled(moduleItems > 0 && modelItems > 0);
  bool removeEnable = false;

  if (modelItems > 0)
  {
    foreach (QTreeWidgetItem* item, ui.treeWidgetModelPorts->selectedItems())
    {
      if (item->text(1).length() > 0)
      {
        // We have a mapped port, so enable the remove button
        removeEnable = true;
        break;
      }
    }
  }
  ui.pushButtonRemove->setEnabled(removeEnable);
}

void WizPgPorts::on_pushButtonMap_clicked()
{
  foreach (QTreeWidgetItem* item, ui.treeWidgetModelPorts->selectedItems())
  {
    foreach (QTreeWidgetItem* moduleItem, ui.treeWidgetModulePorts->selectedItems())
    {
      item->setText(1, moduleItem->text(0));
    }
  }

  updateButtons();
}

void WizPgPorts::on_pushButtonRemove_clicked()
{
  foreach (QTreeWidgetItem* item, ui.treeWidgetModelPorts->selectedItems())
  {
    QVariant v = item->data(0, Qt::UserRole);
    PortInstance* portInst = (PortInstance*)v.value<void*>();
    portInst->getPort()->setBaseName("");
    QString temp = portInst->getUnelaboratedName();
    int ix = -1;
    item->setText(colUSERPORT, temp);
    do // repeat until no more $(var)s
    {
      QRegExp exp1("\\$\\((\\w+)\\)");
      exp1.setPatternSyntax(QRegExp::RegExp);
      ix = exp1.indexIn(temp);
      int i2 = exp1.matchedLength();
      int nc = exp1.numCaptures();
      if (ix != -1 && nc == 1)
      {
        item->setText(colUSERPORT, exp1.cap(1));

        QString left = temp.left(ix);
        QString rest = temp.mid(ix+i2);
        temp = left + "${" + exp1.cap(1) + "}" + rest;
      }
    }
    while (ix != -1); 
  }

  updateButtons();
}

void WizPgPorts::on_treeWidgetModelPorts_itemSelectionChanged()
{
  mModelDragData.clear();
  foreach (QTreeWidgetItem* item, ui.treeWidgetModelPorts->selectedItems())
  {
    mModelDragData << item->text(0);
    ui.treeWidgetModelPorts->putDragData(mModelDragData.c_str());
    break;
  }
  updateButtons();
}
void WizPgPorts::on_treeWidgetModulePorts_itemSelectionChanged()
{
  mModuleDragData.clear();
  foreach (QTreeWidgetItem* item, ui.treeWidgetModulePorts->selectedItems())
  {
    mModuleDragData << item->text(0);
    ui.treeWidgetModulePorts->putDragData(mModuleDragData.c_str());
    break;
  }
  updateButtons();
}


void WizPgPorts::populate()
{
  ui.treeWidgetModelPorts->clear();
  ui.treeWidgetModulePorts->clear();

  ElaboratedModel* model = mContext->getModel();
  PortInstances* instances = model->getUniquePorts();

  for (int i=0; i<instances->getCount(); i++)
  {
    PortInstance* portInst = instances->getAt(i);
    addItem(portInst);
  }
}

QString WizPgPorts::mappedName(const QString& name)
{
  return mPortMap[name];
}

void WizPgPorts::addItem(PortInstance* portInst)
{
  QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidgetModelPorts);

  item->setFlags(item->flags() | Qt::ItemIsEditable);

  QVariant qv = qVariantFromValue((void*)portInst);
  item->setData(0, Qt::UserRole, qv);

  item->setText(colPORT, portInst->getUnelaboratedName());
  item->setText(colUSERPORT, portInst->getPort()->getBaseName());

  int ix = -1;
  QString temp = portInst->getUnelaboratedName();

  if (portInst->getPort()->getBaseName().length() == 0)
  {
    do // repeat until no more $(var)s
    {
      QRegExp exp1("\\$\\((\\w+)\\)");
      exp1.setPatternSyntax(QRegExp::RegExp);
      ix = exp1.indexIn(temp);
      int i2 = exp1.matchedLength();
      int nc = exp1.numCaptures();
      if (ix != -1 && nc == 1)
      {
        item->setText(colUSERPORT, exp1.cap(1));

        QString left = temp.left(ix);
        QString rest = temp.mid(ix+i2);
        temp = left + "${" + exp1.cap(1) + "}" + rest;
      }
    }
    while (ix != -1);  
  }
} 


void WizPgPorts::on_pushButtonChoose_clicked()
{
  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  DlgSelectModule dlg1(this, pw);

  if (dlg1.exec() == QDialog::Accepted)
  {
    XInstance* inst = dlg1.getInstance();
    XToplevelInterface* topi = dlg1.getToplevelInterface();

    if (inst)
    {
      ui.lineEditReplaceModuleName->setText(inst->getInterfaceName());
      ui.treeWidgetModulePorts->clear();
      XInstancePorts* ports = inst->getPorts();
      for (int i=0; i<ports->numPorts(); i++)
      {
        XInstancePort* port = ports->getAt(i);
        
        qDebug() << "formal" << port->getFormal() << "actual" << port->getActual();

        XInterfacePort* iport = inst->getInterface()->getPorts()->findPort(port->getFormal());

        QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidgetModulePorts);
        item->setText(0, port->getFormal());
        if (iport)
          item->setText(1, iport->getDirection());
      }
    }
    else if (topi) 
    {
      XDesignHierarchy* hier = dlg1.getHierarchy();
      XInterface* iface = hier->findInterface(topi->getName());

      ui.lineEditReplaceModuleName->setText(topi->getName());
      ui.treeWidgetModulePorts->clear();

      if (iface)
      {
        XInterfacePorts* ports = iface->getPorts();
        for (int i=0; i<ports->numPorts(); i++)
        {
          XInterfacePort* port = ports->getAt(i);
          
          QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidgetModulePorts);
          item->setText(0, port->getName());
          item->setText(1, port->getDirection());
        }
      }
    }
  }
}
