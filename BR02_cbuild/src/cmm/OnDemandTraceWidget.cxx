//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2006-2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "gui/CQt.h"
#include "shell/ReplaySystem.h"
#include "MdiOnDemandTrace.h"
#include "OnDemandTraceWidget.h"


OnDemandTraceWidget::OnDemandTraceWidget(MDIDocumentTemplate *tplate, QWidget *parent,
                                         CarbonMakerContext* ctx)
  :OnDemandTuningWidget(parent, ctx), mTraceReader(NULL),
   mTraceFileName(""), mTraceFileSize(0), mCarbonMakerContext(ctx)
{
  setAttribute(Qt::WA_DeleteOnClose);

  initializeMDI(this, tplate);

  setObjectName(QString::fromUtf8("onDemandTrace"));

  // setup a timer for polling the trace file
  // mTraceTimer = new QTimer(this);
  // CQT_CONNECT(mTraceTimer, timeout(), this, readTraceFile());
  // mTraceTimer->setInterval(4000); // milliseconds
}

OnDemandTraceWidget::~OnDemandTraceWidget()
{
  delete mTraceReader;
}

void OnDemandTraceWidget::resetTraceReader(const char *fname)
{
  //clearContents();
  //setRowCount(0);
  delete mTraceReader;
  mTraceFileName = fname;
  mTraceFileSize = 0;
  //mTraceReader = new OnDemandTraceFileReader(mTraceFileName.c_str(), reader, this);
  UtString errmsg;
  (void) loadTraceFile(fname, &errmsg);
  // mTraceTimer->start();
}

void OnDemandTraceWidget::readTraceFile()
{
  if (mTraceReader) {
    // try to read more trace information and add it to the table
    UInt32 rows = 0;
    while (rows++ < 100 && mTraceReader->read()) ;
  }
}

bool OnDemandTraceWidget::open(const char *traceFile)
{
  OSStatEntry statEntry;
  UtString errmsg;
  SInt64 size;
  
  OSStatFileEntry(traceFile, &statEntry, &errmsg);
  size = statEntry.getFileSize();
  if (size < 0) size = 0;

  if (!mTraceReader) {
    resetTraceReader(traceFile);
  }
  else {
    // try to detect a new trace file by comparing file name, and file size
    if ((strcmp(traceFile, mTraceFileName.c_str()) != 0) || (size < mTraceFileSize)) {
      resetTraceReader(traceFile);
    }
    else {
      mTraceFileSize = size;
    }
  }

  return true;
}

#if 0
static const char *sDebugTypeName(CarbonOnDemandDebugType type) {
  const char *name = "unknown";
  switch (type) {
    case eCarbonOnDemandDebugNonIdleNet:       //!< Deposit to a net that is not in the idle set
      name = "access to a non-idle net";
      break;
    case eCarbonOnDemandDebugNonIdleChange:    //!< Model state change that is not allowed when idle
      name = "non-idle model state change";
      break;
    case eCarbonOnDemandDebugRestore:          //!< Model state restore
      name = "model state restore";
      break;
    case eCarbonOnDemandDebugDiverge:          //!< Divergence from expected stimulus
      name = "diverged from expected stimulus";
      break;
    case eCarbonOnDemandDebugFail:             //!< Failed idle state detection
      name = "idle state detection failed";
      break;
  }
  return name;
}

static const char *sDebugActionName(CarbonOnDemandDebugAction action) {
  const char *name = "unknown";
  switch(action) {
    case  eCarbonOnDemandDebugAPI:             //!< API call
      name = "api call";
      break;
    case eCarbonOnDemandDebugInternal:         //!< Internal model exection
      name = "internal model execution";
      break;
    case eCarbonOnDemandDebugCModel:           //!< Cmodel implementation
      name = "c-model";
      break;
  }
  return name;
}

void OnDemandTraceWidget::reader(void *userData,
                                 CarbonUInt64 schedCalls,
                                 CarbonTime simTime,
                                 CarbonOnDemandDebugType type,
                                 CarbonOnDemandDebugAction action,
                                 const char *path)
{
  OnDemandTraceWidget *self = (OnDemandTraceWidget *) userData;

  // Schedule calls don't have a column yet
  (void) schedCalls;
  
  QTableWidgetItem *timeItem = new QTableWidgetItem(tr("%1").arg(simTime));
  QTableWidgetItem *typeItem = new QTableWidgetItem(tr(sDebugTypeName(type)));
  QTableWidgetItem *actionItem = new QTableWidgetItem(tr(sDebugActionName(action)));
  QTableWidgetItem *pathItem = new QTableWidgetItem(tr(path));

  UInt32 row = self->rowCount();
  self->setRowCount(row +1);
  self->setItem(row, 0, timeItem);
  self->setItem(row, 1, typeItem);
  self->setItem(row, 2, actionItem);
  self->setItem(row, 3, pathItem);
}
#endif
