//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#include "util/CarbonPlatform.h"
#include "util/UtShellTok.h"
#include "util/CarbonVersion.h"
#include "shell/ReplaySystem.h"
#include "gui/CQt.h"

#include "RemoteConsole.h"
#include "CarbonProject.h"
#include "CarbonProjectWidget.h"
#include "CarbonOptions.h"
#include "DlgAuthenticate.h"
#include "WorkspaceModelMaker.h"
#include "CarbonMakerContext.h"
#include "ProcessTree.h"
#include "cmm.h"

#if pfWINDOWS
#pragma warning( disable : 4996) 
#endif

RemoteConsole::RemoteConsole(QWidget *parent)
: QWidget(parent)
{
  ui.setupUi(this);
  
  mUseProject = true;
  mCommandMode = Local;
  mProjectWidget=NULL;
  mProcess=NULL;
  mLocalProcess=NULL;
  mCodec = QTextCodec::codecForLocale(); // could this fail?
  mLoggedIn = false;
  mLoginState = RemoteConsole::Unknown;
  mCommandPending = false;
  mCommandType = Compilation;
  mPendingFlags = Program;
  mStatusExp.setPattern("#\\s*([0-9]+)\\s+Elapsed: [0-9]+:[0-9]+:[0-9]+ Complete:\\s+([0-9]+)%");
  mInitialized = false;
  mVersionMatched = false;
  mVersionCheckCompleted = false;
  mCommandFlags = Program;

  ui.textEdit->setReadOnly(true);
}

void RemoteConsole::on_toolButtonClearAll_clicked()
{
  ui.textEdit->clear();
}

RemoteConsole::~RemoteConsole()
{
  if (mProcess)
    mProcess->terminate();
}

void RemoteConsole::configurationChanged(const char*)
{
  if (mProcess)
    mProcess->terminate();
  mProcess = NULL;
  mLoggedIn = false;
  mLoginState = RemoteConsole::Unknown;
  mCommandPending = false;
  mVersionMatched = false;
  mVersionCheckCompleted = false;
}

void RemoteConsole::executeCommand(CommandMode mode, CommandType ctype, QString prog, QStringList args, QString workingDir, CommandFlags flags)
{
  mProgram = prog;
  mProgramArgs = args;
  mCommandType = ctype;
  mCommandMode = mode;
  mWorkingDir = workingDir;
  mCommandFlags = flags;

  // clear the output before a compilation
  if (ctype == RemoteConsole::Compilation)
  {
    QDockWidget* dw = mProjectWidget->context()->getWorkspaceModelMaker()->getRemoteConsoleDockWindow();
    dw->lower();
    dw->raise();
  }

  if (mode == RemoteConsole::Remote) 
  {
    if (!mInitialized)
    {
      CarbonProject* proj = mProjectWidget->project();

      if (mUseProject && proj)
      {
        CarbonOptions* options = proj->getProjectOptions();

        options->registerPropertyChanged("Remote Server", "remoteInfoChanged", this);
        options->registerPropertyChanged("Remote Username", "remoteInfoChanged", this);
        options->registerPropertyChanged("Remote Working Directory", "remoteInfoChanged", this);
        options->registerPropertyChanged("Remote Command Prefix", "remoteInfoChanged", this);

        CQT_CONNECT(mProjectWidget->project(), projectLoaded(const char*, CarbonProject*), this, projectLoaded(const char*, CarbonProject*));
        CQT_CONNECT(mProjectWidget->project(), configurationChanged(const char*), this, configurationChanged(const char*));
      }
      mInitialized = true;
    }

    if (!mCommandPending && (mLoginState != LoggedIn))
    {
      mCommandPending = true;
      mWorkingDir = workingDir;
      mPendingProg = prog;
      mPendingArgs = args;
      mPendingFlags = flags;
      login();
    }
    else if (mLoginState == LoggedIn)
      executeRemoteCommand(ctype, prog, args, workingDir, flags);
  }
  else
    executeLocalCommand(ctype, prog, args, workingDir, flags);
}

void RemoteConsole::processBuffer(CommandType ctype, const char* inBuffer)
{
  QString buffer = inBuffer;
  foreach (QString line, buffer.split('\n'))
  {
    UtString tmp;
    tmp << line;
    processOutput(ctype, tmp.c_str());
  }
}

void RemoteConsole::executeWindowsMake()
{
  // we only do this on windows
#if pfWINDOWS
  qDebug() << mProgram << " " << mProgramArgs;
  qDebug() << mWorkingDir;
  qDebug() << mCommandFlags;

  CarbonConfiguration* activeConfig = mProjectWidget->project()->getActive();

  if (mWorkingDir.length() == 0)
    mWorkingDir = QString(activeConfig->getPlatform()) + "/" + QString(activeConfig->getName());
  
  QStringList args;
  args << "-f" << "Makefile.carbon" << "build";
  executeLocalCommand(Compilation, "$(CARBON_HOME)/Win/bin/make.exe", args, mWorkingDir, Program);
#endif
}


void RemoteConsole::processOutput(CommandType ctype, const QString& outputLine)
{
  UtString wdir;
  wdir << mWorkingDir;

  qDebug() << outputLine;

  emit stdoutChanged(ctype, outputLine, mWorkingDir);
  QRegExp finishedall("^CARBON MAKEFILE COMPLETED");
  QRegExp finishError("^make(.exe)?: \\*\\*\\* \\[(.*)\\] Error [0-9]+");

  if (finishedall.exactMatch(outputLine) || finishError.exactMatch(outputLine))
  {
    emit makefileFinished(mCommandMode, ctype);
    cmm* appContext = mProjectWidget->context()->getModelStudio();
    if (appContext->isSquishMode())
      QMessageBox::information(mProjectWidget, MODELSTUDIO_TITLE,
        tr("Compilation Finished"), QMessageBox::Ok);
  }
  
  QRegExp finishedmodel("^FINISHED MODEL COMPILATION");
  if (finishedmodel.exactMatch(outputLine))
    emit modelCompilationFinished(mCommandMode, ctype);

  QRegExp finishedexplore("^CARBON FINISHED EXPLORE");
  if (finishedexplore.exactMatch(outputLine))
    emit exploreFinished(mCommandMode, ctype);
}

void RemoteConsole::executeLocalCommand(CommandType ctype, QString prog, QStringList args, QString workingDir, CommandFlags flags)
{
  QString platformString = "Linux";

  QString projDir = mProjectWidget->project()->getProjectDirectory();

  UtString carbonProject;
  carbonProject << "CARBON_PROJECT" << "=" << projDir;

  UtString carbonOutput;
  carbonOutput << "CARBON_OUTPUT" << "=" << projDir << "/" << platformString << "/" << mProjectWidget->project()->getActiveConfiguration();

  UtString carbonConfig;
  carbonConfig << "CARBON_CONFIGURATION" << "=" << platformString << "/" << mProjectWidget->project()->getActiveConfiguration();

  UtString carbonModel;
  carbonModel << "CARBON_MODEL=" <<  mProjectWidget->project()->getActive()->getOptions("VSPCompiler")->getValue("-o")->getValue();

  UtString carbonPid;
  carbonPid << CARBON_MODELSTUDIO_PID << "=" << OSGetPid();

  UtString carbonSim;
  const char* simUI = mProjectWidget->project()->getProjectOptions()->getValue("Simulation User Interface")->getValue();
  if (simUI && 0 == strcmp(simUI, "true"))
    carbonSim << CARBON_MODELSTUDIO_SIM << "=true";

  if (flags == RemoteConsole::Simulate)
  {
    if (carbonSim.length() > 0)
      putenv((char*)carbonSim.c_str());
    putenv((char*)carbonPid.c_str());
    qDebug() << carbonPid.c_str();
  }

  putenv((char*)carbonProject.c_str());
  putenv((char*)carbonConfig.c_str());
  putenv((char*)carbonModel.c_str());
  putenv((char*)carbonOutput.c_str());

  qDebug() << carbonProject.c_str();
  qDebug() << carbonConfig.c_str();
  qDebug() << carbonModel.c_str();
  qDebug() << carbonOutput.c_str();
  qDebug() << carbonSim.c_str();

  QProcess* qProcess = NULL;

  if (mProcess == NULL)
  {
    qDebug() << "Creating Process";
    mProcess = new QProcess(this);
    qProcess = mProcess;
  
    QStringList env = QProcess::systemEnvironment();
    env.append(carbonProject.c_str());
    env.append(carbonConfig.c_str());
    env.append(carbonModel.c_str());
    env.append(carbonOutput.c_str());
    env.append("TERM=dumb");

    if (flags == RemoteConsole::Simulate)
    {
      if (carbonSim.length() > 0)
        env.append(carbonSim.c_str());
      env.append(carbonPid.c_str());
    }

    mProcess->setEnvironment(env);

    CQT_CONNECT(mProcess, error(QProcess::ProcessError), this, processError(QProcess::ProcessError));
    CQT_CONNECT(mProcess, readyReadStandardError(), this, readStderr());
    CQT_CONNECT(mProcess, readyReadStandardOutput(), this, readStdout());
    CQT_CONNECT(mProcess, started(), this, started());
    CQT_CONNECT(mProcess, finished(int), this, finished(int));
  }
  else
  {
    qDebug() << "Creating Local Process";
    mLocalProcess = new QProcess(this);
    qProcess = mLocalProcess;
  
    QStringList env = QProcess::systemEnvironment();
    env.append(carbonProject.c_str());
    env.append(carbonConfig.c_str());
    env.append(carbonModel.c_str());
    env.append(carbonOutput.c_str());
    env.append("TERM=dumb");

    if (flags == RemoteConsole::Simulate)
    {
      if (carbonSim.length() > 0)
        env.append(carbonSim.c_str());
      env.append(carbonPid.c_str());
    }

    mLocalProcess->setEnvironment(env);

    CQT_CONNECT(mLocalProcess, error(QProcess::ProcessError), this, processError(QProcess::ProcessError));
    CQT_CONNECT(mLocalProcess, readyReadStandardError(), this, readStderrLocal());
    CQT_CONNECT(mLocalProcess, readyReadStandardOutput(), this, readStdoutLocal());
    CQT_CONNECT(mLocalProcess, started(), this, startedLocal());
    CQT_CONNECT(mLocalProcess, finished(int), this, finishedLocal(int));
  }

  char* path = getenv("CARBON_HOME");
  if( path == NULL )
  {
    ui.textEdit->append("ERROR: Environment variable CARBON_HOME not set.\n");
    return;
  }

  qProcess->setWorkingDirectory(workingDir);
  QStringList progArgs;

  UtString tprog;
  tprog << prog;
  UtString program;
  UtString err;
  OSExpandFilename(&program,tprog.c_str(),&err);

#ifdef Q_OS_WIN
  UtString cmdline;
  cmdline << program << " ";
  progArgs << "/c" << program.c_str();
  program = "cmd.exe";
  for (int i=0; i<args.count(); i++)
  {
    progArgs << args[i];
    cmdline << args[i] << " ";
  }
#else
  UtString cmdline;
  cmdline << program << " ";
  for (int i=0; i<args.count(); i++)
  {
    progArgs << args[i];
    cmdline << args[i] << " ";
  }
#endif
  ui.textEdit->append(cmdline.c_str());

  if (mLocalProcess == qProcess)
    emit compilationStarted(Local, ctype);

  qDebug() << program.c_str() << " " << progArgs;

  qProcess->start( program.c_str(), progArgs ); 

  if (ctype == RemoteConsole::Command)
    qProcess->waitForFinished();
}

void RemoteConsole::appendBytes(const QByteArray& bytes) 
{
  if (mCodec != NULL)
  {
    QString str = mCodec->toUnicode(bytes);
    ui.textEdit->append(str);
  }
  else
  {
    // I'm not sure if we might fail to get the CODEC, but let's
    // let the failure-mode for that be that we will append Euro-centric
    // text rather than crashing.
    ui.textEdit->append(bytes);
  }

  QTextCursor cursor = ui.textEdit->textCursor();
  cursor.movePosition(QTextCursor::End);

  ui.textEdit->setTextCursor(cursor);
}

void RemoteConsole::readStandardError(QProcess* proc)
{
  if (proc == NULL)
    return;

  QByteArray ba = proc->readAllStandardError();

  QList<QByteArray> lines = ba.split('\n');

  UtString str;
  str << mCodec->toUnicode(ba);

  if (str.size() > 1)
  {
    int last = str[str.size()-1];
#if pfWINDOWS
    int nl = str[str.size()-2];
    if (last == '\n' && nl == '\r')
      str.resize(str.size()-2);
#else
    if (last == '\n')
      str.resize(str.size()-1);
#endif
  }

  if (mLoginState == RemoteConsole::Authorizing)
  {
    for (int i=0; i<lines.count(); ++i)
    {
      QByteArray arr = lines[i];
      QString stext = mCodec->toUnicode(arr).trimmed();
      UtString us;
      us << stext;
      const char* s = us.c_str();

      if (0 == strcmp(s, "Store key in cache? (y/n)"))
        proc->write("n\n");

      if (0 == strcmp(s, "Connection abandoned."))
      {
        mLoginState = RemoteConsole::FailedLogin;
        proc->terminate();
        proc=NULL;
        mLoginState = RemoteConsole::Unknown;
        qDebug() << "Login Failed!!";
        mCommandPending = false;
      }

      if (0 == strcmp(s, "Access denied") || 0 == strcmp(s, "Connection abandoned."))
      {
        mLoginState = RemoteConsole::FailedLogin;
        proc->terminate();
        proc=NULL;
        mLoginState = RemoteConsole::Unknown;
        qDebug() << "Login Failed!!";
        mCommandPending = false;
        break;
      }

      if (0 == strcmp(s, "Access granted"))
      {
        // Now Perform remote Directory Check before signifying that we are successfully logged in
        CarbonProject* proj =  mProjectWidget->project();
        if (mUseProject && proj)
        {
          mLoginState = RemoteConsole::RemoteDirCheck;
          CarbonPropertyValue* remoteDir = mProjectWidget->project()->getProjectOptions()->getValue("Remote Working Directory");
          const char* remoteHome =  mProjectWidget->project()->getProjectOptions()->getValue("Remote CARBON_HOME")->getValue();
          const char* remotePrefix = mProjectWidget->project()->getProjectOptions()->getValue("Remote Command Prefix")->getValue();

          const char* pf = mProjectWidget->project()->projectFile();
          QFileInfo fi(pf);
          UtString statCmd;

          if (remotePrefix && strlen(remotePrefix) > 0)
            statCmd << remotePrefix << " ";
          else if (remoteHome && strlen(remoteHome) > 0)
            statCmd << remoteHome << "/bin/";

          statCmd << "check_file '" << remoteDir->getValue() << "/" << fi.fileName() << "'\n";
          
          proc->write(statCmd.c_str());
        }
        else // Skip Remote Dir and Version Check
        {
          mLoginState = RemoteConsole::LoggedIn;
          if (mCommandPending)
          {
            CarbonProject* proj = mProjectWidget->project();
            UtString wdir;
            if (mUseProject && proj)
            {
              CarbonPropertyValue* remoteDir = proj->getProjectOptions()->getValue("Remote Working Directory");

              wdir << remoteDir->getValue() << "/" << mWorkingDir;

              UtString chd;
              chd << "cd " << wdir.c_str() << "\n";

              proc->write(chd.c_str());
            }
            executeRemoteCommand(mCommandType, mPendingProg, mPendingArgs, wdir.c_str(), mPendingFlags);
            mCommandPending = false;
          }
        }
      }
    }
  }

  for (int i=0; i<lines.count(); ++i)
  {
    QByteArray arr = lines[i];
    QString stext = mCodec->toUnicode(arr).trimmed();
    UtString us;
    us << stext.trimmed();

    if (us.length() > 0)
    {
      UtString wdir;
      wdir << mWorkingDir;
      emit stderrChanged(mCommandType, us.c_str(), wdir.c_str());
    }
  }

  const char* text = str.c_str();
  QRegExp finishError("^make(.exe)?: \\*\\*\\* \\[(.*)\\] Error [0-9]+");
  if (finishError.exactMatch(text))
  {
    emit makefileFinished(mCommandMode, mCommandType);
    emit compilationEnded(mCommandMode, mCommandType);
  }

  ui.textEdit->append(text);
}
void RemoteConsole::readStderrLocal()
{
  readStandardError(mLocalProcess);
}

void RemoteConsole::readStderr() 
{
  readStandardError(mProcess);
}

void RemoteConsole::executeRemoteCommand(CommandType, QString prog, QStringList args, QString /*workingDir*/, CommandFlags flags)
{
  UtString program;
  INFO_ASSERT(mProcess, "Process should be valid");

  const char* platformString = "Linux";

  QString rdir;
  CarbonProject* proj = mProjectWidget->project();
  if (mUseProject && proj)
  {
    rdir = proj->getProjectOptions()->getValue("Remote Working Directory")->getValue();
 
    UtString remoteDir;
    remoteDir << rdir;
    UtString envBuffer;
    envBuffer << "/bin/env";

    CarbonPropertyValue* remoteHome = mProjectWidget->project()->getProjectOptions()->getValue("Remote CARBON_HOME");
    UtString remoteCarbonHome;
    remoteCarbonHome << remoteHome->getValue();
    if (remoteCarbonHome.length() > 0)
      envBuffer << " '" << "CARBON_HOME=" << remoteCarbonHome.c_str() << "'";

    envBuffer << " '" << "CARBON_PROJECT=" << remoteDir.c_str() << "'";
    envBuffer << " '" << "CARBON_CONFIGURATION=" << platformString << "/" << mProjectWidget->project()->getActiveConfiguration() << "'";
    envBuffer << " '" << "CARBON_MODEL=" <<  mProjectWidget->project()->getActive()->getOptions("VSPCompiler")->getValue("-o")->getValue() << "'";
    envBuffer << " '" << "CARBON_OUTPUT=" << remoteDir.c_str() << "/" << platformString << "/" << mProjectWidget->project()->getActiveConfiguration() << "'";

    UtString carbonSim;
    const char* simUI = mProjectWidget->project()->getProjectOptions()->getValue("Simulation User Interface")->getValue();
    if (simUI && 0 == strcmp(simUI, "true"))
      carbonSim << CARBON_MODELSTUDIO_SIM << "=true";

    if (flags == RemoteConsole::Simulate)
    {
      UtString carbonPid;
      carbonPid << CARBON_MODELSTUDIO_PID << "=" << OSGetPid();
      envBuffer << " '" << carbonPid << "'";
      putenv((char*)carbonPid.c_str());

      if (carbonSim.length() > 0)
      {
        envBuffer << " '" << carbonSim << "'";
        putenv((char*)carbonSim.c_str());
      }
      qDebug() << carbonPid.c_str();
      qDebug() << carbonSim.c_str();
    }

    envBuffer << " 'TERM=dumb'";

    UtString carbonProject;
    carbonProject << "CARBON_PROJECT" << "=" << remoteDir.c_str();

    UtString carbonConfig;
    carbonConfig << "CARBON_CONFIGURATION" << "=" << platformString << "/" << mProjectWidget->project()->getActiveConfiguration();

    UtString carbonModel;
    carbonModel << "CARBON_MODEL=" <<  mProjectWidget->project()->getActive()->getOptions("VSPCompiler")->getValue("-o")->getValue();

    UtString carbonOutput;
    carbonOutput << "CARBON_OUTPUT=" << remoteDir.c_str() << "/" << platformString << "/" << mProjectWidget->project()->getActiveConfiguration();

    qDebug() << carbonProject.c_str();
    qDebug() << carbonConfig.c_str();
    qDebug() << carbonModel.c_str();
    qDebug() << carbonOutput.c_str();

    putenv((char*)carbonProject.c_str());
    putenv((char*)carbonConfig.c_str());
    putenv((char*)carbonModel.c_str());
    putenv((char*)carbonOutput.c_str());

    const char* envCommand = envBuffer.c_str();
    qDebug() << envCommand;


    UtString tprog;
    tprog << prog;
    UtString expandedProg;
    UtString err;
    if (!OSExpandFilename(&expandedProg,tprog.c_str(),&err))
      expandedProg = tprog;

    CarbonOptions* options = mProjectWidget->project()->getProjectOptions();
    CarbonPropertyValue* remotePrefix = options->getValue("Remote Command Prefix");
    if (remotePrefix && strlen(remotePrefix->getValue()) > 0)
      program << remotePrefix->getValue() << " " << envCommand << " " << expandedProg;
    else
      program << envCommand << " " << expandedProg;
  }
  else
    program << prog;

  for (int i=0; i<args.count(); ++i)
    program << " " << args.at(i);

  program << "\n";

  mProcess->write(program.c_str());
}

void RemoteConsole::killProcessAndSubprocesses(int parentPid)
{
#if !pfWINDOWS
  bool finished = false;
  do
  {
    ProcessTree procTree(parentPid);
    const QList<int>& pidList =  procTree.childProcesses();
    finished = pidList.empty();

    foreach (int subPid, pidList)
    {
      qDebug() << "Killing subprocess: " << subPid;
      OSKillProcess(subPid, eKillAssasinate);
    }
  }
  while (!finished);

  qDebug() << "Killing main process: " << parentPid;
  OSKillProcess(parentPid, eKillAssasinate);
#endif

}

void RemoteConsole::stopCompilation()
{
  qDebug() << "Terminating compilation process";  

  if (mCommandMode == Remote)
  {
    if (mProcess)
      mProcess->kill();
    if (mLocalProcess)
      mLocalProcess->kill();
  }
  else
  {
    if (mProcess)
    {
#if pfWINDOWS
      mProcess->terminate();
#else
      int pid = (int)mProcess->pid();
      killProcessAndSubprocesses(pid);
#endif
    }
    if (mLocalProcess)
    {
#if pfWINDOWS
      mLocalProcess->terminate();
#else
      int pid = (int)mLocalProcess->pid();
      killProcessAndSubprocesses(pid);
#endif
    }
  }

  mProcess = NULL;
  mLocalProcess = NULL;

  mLoggedIn = false;
  mLoginState = RemoteConsole::Unknown;
  mCommandPending = false;
}

void RemoteConsole::readStdoutLocal()
{
  readStandardOutput(mLocalProcess);
}

void RemoteConsole::readStandardOutput(QProcess* proc)
{
  bool signalError = false;
  UtString errorString;

  if (proc == NULL)
    return;

  QByteArray ba = proc->readAllStandardOutput();

  QList<QByteArray> lines = ba.split('\n');

  UtString str;
  str << mCodec->toUnicode(ba);

  if (mLoginState == LoggingIn)
  {
    CarbonOptions* options = mProjectWidget->project()->getProjectOptions();

    CarbonPropertyValue* remoteServer = options->getValue("Remote Server");
    CarbonPropertyValue* remoteUsername = options->getValue("Remote Username");

    UtString prompt;
    prompt << remoteUsername->getValue() << "@" << remoteServer->getValue() << "'s password: ";
    if (0 == strcmp(str.c_str(), prompt.c_str()))
    {
      proc->write(mPassword.c_str());
      proc->write("\n");
      mLoginState = Authorizing;
      return;
    }
  }

  if (mLoginState == RemoteConsole::RemoteVersionCheck)
  {
    for (int i=0; i<lines.count(); ++i)
    {
      QByteArray arr = lines[i];
      QString stext = mCodec->toUnicode(arr).trimmed();
      UtString us;
      us << stext;
      const char* s = us.c_str();
      if (0 == strcmp(s, CARBON_RELEASE_ID))
      {
        qDebug() << "Versions Match: " << CARBON_RELEASE_ID;
        mVersionMatched = true;
      }

      if (0 == strcmp(s, "CARBON VERSION CHECK COMPLETED"))
        mVersionCheckCompleted = true;
    }

    if (mVersionCheckCompleted)
    {
      if (mVersionMatched)
      {
        mLoginState = RemoteConsole::LoggedIn;

        if (mCommandPending)
        {
          CarbonProject* proj = mProjectWidget->project();
          UtString wdir;
          if (mUseProject && proj)
          {
            CarbonPropertyValue* remoteDir = proj->getProjectOptions()->getValue("Remote Working Directory");

            wdir << remoteDir->getValue() << "/" << mWorkingDir;

            UtString chd;
            chd << "cd " << wdir.c_str() << "\n";

            proc->write(chd.c_str());
          }
          executeRemoteCommand(mCommandType, mPendingProg, mPendingArgs, wdir.c_str(), mPendingFlags);
          mCommandPending = false;
        }
      }
      else 
      {
        qDebug() << "Incorrect Remote Carbon Version";
        proc->terminate();
        mProcess = NULL;
        mCommandPending = false;
        mLoginState = RemoteConsole::FailedLogin;
        signalError = true;
        errorString << "Error: Incorrect Remote Carbon Software, expecting version: " << CARBON_RELEASE_ID;
        errorString << "Please install or configure Remote CARBON_HOME to the correct version.";
      }
    }
  }

  if (mLoginState == RemoteConsole::RemoteDirCheck)
  {
    CarbonPropertyValue* remoteDir = mProjectWidget->project()->getProjectOptions()->getValue("Remote Working Directory");

    const char* pf = mProjectWidget->project()->projectFile();
    QFileInfo fi(pf);

    UtString expectedName;
    expectedName << remoteDir->getValue() << "/" << fi.fileName();

    QStringList expectedValues;
    UtString ex1;

    ex1 << "Carbon File Exists: (.*)";

    expectedValues << ex1.c_str();

    QStringList errorStrings;
    UtString er1;

    er1 << "Carbon File Not found: (.*)";
    errorStrings << er1.c_str();

    for (int i=0; i<lines.count(); ++i)
    {
      QByteArray arr = lines[i];
      QString stext = mCodec->toUnicode(arr).trimmed();
      UtString us;
      us << stext;
      const char* s = us.c_str();

      foreach (QString errStr, errorStrings)
      {
        QRegExp exp(errStr);
        if (exp.exactMatch(s))
        {
          qDebug() << "Incorrect Remote Working Directory, abort";
          proc->terminate();
          mProcess = NULL;
          mCommandPending = false;
          mLoginState = RemoteConsole::FailedLogin;
          signalError = true;
          errorString << "Error: Incorrect Remote Working Directory: '" << remoteDir->getValue() << "'" << "\n";
          errorString << "The Remote Working Directory needs to be a Unix path to the directory which contains the file: " << fi.fileName() << "\n";
          errorString << "Please correct the path and try again";
        }
      }

      foreach (QString evalue, expectedValues)
      {
        QRegExp exp(evalue);
        if (exp.exactMatch(s))
        {
          UtString projectDir;
          CarbonConfiguration* activeConfig = mProjectWidget->project()->getActive();
          QStringList args;
          UtString makefileDir;

          projectDir << mProjectWidget->project()->getRemoteWorkingDirectory();

          OSConstructFilePath(&makefileDir, projectDir.c_str(), activeConfig->getPlatform());
          OSConstructFilePath(&makefileDir, makefileDir.c_str(), activeConfig->getName());

          args << "-C" << makefileDir.c_str() << "-f" << "Makefile.carbon" << "CarbonVersionCheck";

          UtString makeCommand;
   
          CarbonPropertyValue* remoteHome = mProjectWidget->project()->getProjectOptions()->getValue("Remote CARBON_HOME");
          UtString remoteCarbonHome;
          remoteCarbonHome << remoteHome->getValue();

          if (remoteCarbonHome.length() > 0)
            makeCommand << remoteCarbonHome << "/bin/make";
          else
            makeCommand << "make";

          mLoginState = RemoteConsole::RemoteVersionCheck;
          executeRemoteCommand(mCommandType, makeCommand.c_str(), args, "", Program);

          break;
        }
      }
    }
  }

  if (str.size() > 1)
  {
    int last = str[str.size()-1];
#if pfWINDOWS
    int nl = str[str.size()-2];
    if (last == 10 && nl == 13)
      str.resize(str.size()-2);
#else
    if (last == 10)
      str.resize(str.size()-1);
#endif
  }

  for (int i=0; i<lines.count(); ++i)
  {
    QByteArray arr = lines[i];
    UtString stext;
    stext << mCodec->toUnicode(arr).trimmed();
    const char* s = stext.c_str();

    QRegExp exp0("^CARBON BEGIN COMPILATION");
    if (exp0.exactMatch(s))
      emit compilationStarted(mCommandMode, mCommandType);

    QRegExp exp1("^CARBON BEGIN CLEAN");
    if (exp1.exactMatch(s))
      emit compilationStarted(mCommandMode, mCommandType);

    QRegExp finishError("^make(.exe)?: \\*\\*\\* \\[(.*)\\] Error [0-9]+");
    QRegExp exp2("^CARBON FINISHED COMPILATION");
    if (exp2.exactMatch(s) || finishError.exactMatch(s))
    {
      emit compilationEnded(mCommandMode, mCommandType);
      if (!finishError.exactMatch(s))
        executeWindowsMake();
    }

    QRegExp exp3("^CARBON FINISHED CLEAN");
    if (exp3.exactMatch(s))
      emit compilationEnded(mCommandMode, mCommandType);

    QRegExp exp5("^CARBON BEGIN SIMULATION");
    if (exp5.exactMatch(s))
      emit simulationStarted(mCommandMode, mCommandType);

    QRegExp exp6("^CARBON FINISHED SIMULATION");
    if (exp6.exactMatch(s))
      emit simulationEnded(mCommandMode, mCommandType);

    if (stext.length() > 0)
      processOutput(mCommandType, s);
  }

  const char* text = str.c_str();

  QString plain(str.c_str());

  if (!mStatusExp.exactMatch(text))
    ui.textEdit->append(plain.toAscii()); // this_toascii_ok

  if (signalError)
    ui.textEdit->append(errorString.c_str());
}

void RemoteConsole::readStdout()
{
  readStandardOutput(mProcess);
}

void RemoteConsole::started() 
{
}

void RemoteConsole::startedLocal() 
{
}

// this only occurs when the process terminates
// which happens every command on Linux, or if the user kills the plink process
//
void RemoteConsole::finishedLocal(int exit_code)
{
  mLocalProcess = NULL;

  UtString msg;
  msg << "\nProcess terminated with ExitCode=" << exit_code << "\n";
  ui.textEdit->append(msg.c_str());

  emit compilationEnded(RemoteConsole::Local, mCommandType);
  emit simulationEnded(RemoteConsole::Local, mCommandType);
  emit makefileFinished(RemoteConsole::Local, mCommandType);
}

// this only occurs when the process terminates
// which happens every command on Linux, or if the user kills the plink process
//
void RemoteConsole::finished(int exit_code)
{
  mProcess = NULL;

  if (mLoginState == RemoteConsole::Authorizing)
  {
    mCommandPending = false;
    mLoginState = RemoteConsole::FailedLogin;
    mVersionMatched = false;
  }
  else
    mLoginState = RemoteConsole::Unknown;

  UtString msg;
  msg << "\nProcess terminated with ExitCode=" << exit_code << "\n";

  ui.textEdit->append(msg.c_str());

  emit compilationEnded(mCommandMode, mCommandType);
  emit simulationEnded(mCommandMode, mCommandType);
  emit makefileFinished(mCommandMode, mCommandType);
}

void RemoteConsole::processError(QProcess::ProcessError err)
{
  const char* emsg = "unknown error";
  switch (err) 
  {
  case QProcess::FailedToStart: emsg = "FailedToStart"; break;
  case QProcess::Crashed: emsg = "Terminated"; break;
  case QProcess::Timedout: emsg = "Timedout"; break;
  case QProcess::WriteError: emsg = "WriteError"; break;
  case QProcess::ReadError: emsg = "ReadError"; break;
  case QProcess::UnknownError: emsg = "UnknownError"; break;
  }

  UtString errMsg;
  errMsg << emsg;

  ui.textEdit->append(errMsg.c_str());

  if (mProcess)
    finished(0);
  
  if (mLocalProcess)
    finishedLocal(0);
}

bool RemoteConsole::loggedInAndReady()
{
  return mLoginState == LoggedIn;
}

void RemoteConsole::login()
{
  if (mProcess)
    mProcess->kill();

  CarbonProject* proj = mProjectWidget->project();
  CarbonOptions* options = NULL;
  
  if (mUseProject && proj)
    options = mProjectWidget->project()->getProjectOptions();

  mProcess = new QProcess(this);
  CQT_CONNECT(mProcess, error(QProcess::ProcessError), this, processError(QProcess::ProcessError));
  CQT_CONNECT(mProcess, readyReadStandardError(), this, readStderr());
  CQT_CONNECT(mProcess, readyReadStandardOutput(), this, readStdout());
  CQT_CONNECT(mProcess, started(), this, started());
  CQT_CONNECT(mProcess, finished(int), this, finished(int));

  QStringList command;

  UtString sshProgramName;
  UtString err;
#ifdef Q_OS_WIN
  OSExpandFilename(&sshProgramName,"$CARBON_HOME/Win/bin/plink.exe",&err);
#else
  OSExpandFilename(&sshProgramName,"$CARBON_HOME/bin/plink",&err);
#endif

  QString sshProgram(sshProgramName.c_str());

  DlgAuthenticate dlg(this);
  dlg.setOptions(options);

  if (dlg.exec() == QDialog::Accepted)
  {
    UtString userInfo;
    if (options)
    {
      CarbonPropertyValue* remoteServer = options->getValue("Remote Server");
      CarbonPropertyValue* remoteUsername = options->getValue("Remote Username");
      userInfo << remoteUsername->getValue() << "@" << remoteServer->getValue();
    }
    else
      userInfo << mAuthUser.c_str() << "@" << mAuthServer.c_str();

    command << "-v" << userInfo.c_str();

    mLoginState = LoggingIn;
    mPassword.clear();
    command << "-pw" << mPassword.c_str();
    mLoginState = Authorizing;
    launch(sshProgram, command, false);
  }
  else
  {
    mCommandPending = false;
    finished(0);
  }
}

void RemoteConsole::launch(QString& cmd, QStringList& args, bool echo)
{
  QString launch_msg("Invoking...\n    ");
  launch_msg += cmd;
  for( QStringList::iterator i = args.begin(); i != args.end(); i++) {
    launch_msg += " ";
    launch_msg += *i;
  }
  if (echo)
    ui.textEdit->append(launch_msg);

  mProcess->start( cmd, args );
}

void RemoteConsole::projectLoaded(const char* /*projName*/, CarbonProject* proj)
{
  mProjectWidget = proj->getProjectWidget();
  mInitialized = false;
  qDebug() << "project loaded: " << proj->getVHMName();
  remoteInfoChanged(NULL,NULL);
}

void RemoteConsole::remoteInfoChanged(const CarbonProperty*, const char*)
{
  // we have a process and it is running
  if (mCommandMode == RemoteConsole::Remote && mProcess && mProcess->pid() != 0)
  {
    mProcess->terminate();
    mProcess = NULL;
    mLoggedIn = false;
    mLoginState = RemoteConsole::Unknown;
    mCommandPending = false;
    mVersionMatched = false;
    mVersionCheckCompleted = false;
  }
}

void RemoteConsole::setProjectWidget(CarbonProjectWidget* pw)
{
  mProjectWidget=pw; 
  mInitialized = false;
}
