#ifndef __JAVASCRIPTAPIS_H_
#define __JAVASCRIPTAPIS_H_

#include "util/CarbonPlatform.h"
#include <QtGui>
#include <Qsci/qsciscintilla.h>
#include <Qsci/qscilexer.h>
#include <Qsci/qsciapis.h>

#include <Qsci/qscilexerjavascript.h>


class SleeperThread : public QThread
{
public:
  static void msleep(unsigned long msecs)
  {
    QThread::msleep(msecs);
  }
};

class JavaScriptAPIs;
class ScriptingEngine;

class LoadAPIThread : public QThread
{
  Q_OBJECT

public:
  LoadAPIThread(QObject* parent = 0) : QThread(parent)
  {
    mAPIs = NULL;
    qDebug() << "Constructed API Thread";
  }

  ~LoadAPIThread();

  void load();

protected:
  void run();

private slots:
  void apiPreparationFinished();
  void apiPreparationCancelled();
  void apiPreparationStarted();

private:
  JavaScriptAPIs* mAPIs;
};

class JavaScriptAPIs: public QsciAPIs
{
  Q_OBJECT

public:
  JavaScriptAPIs(QsciLexer *lexer);
  virtual ~JavaScriptAPIs()
  {
  }

  void enableIntellisenseVariables(bool vars) { mIntellisenseVars = vars; }
  static bool loadAPIs();

  static void mapVariableToAPI(const QString& varStartsWith, const QMetaObject* metaObject)
  {
    mVarsMapList.append(varStartsWith);
    mMapVarToAPI[varStartsWith] = metaObject;
  }
  bool enableIntellisense(QsciScintilla* editor, const QString& name = QString());

  void addApi(const QString& api)
  {
    mPrepFinished = false;
    add(api);
  }

  static QString getClassInfo(const QMetaObject* metaObject, const QString& name);
  static bool isEnumType(const QMetaObject* mo, const QString& typeName);
  static QString typeNameLink(const QString& typeName);

  bool loadAPIForVariable(const QString& varName, const QMetaObject* metaObject, bool enumsOnly=false, const QMetaObject* topObject=0);
  void createEnumIntellisense();

  virtual void updateAutoCompletionList(const QStringList &context, QStringList &list);
  virtual void autoCompletionSelected(const QString &selection);

  static const QMetaObject* lookupType(const QString& typeName);
  static const QMetaObject* lookupClass(const QString& className);
  static const QMetaObject* lookupRawClass(const QString& className);
  static bool isDerivedFrom(const QMetaObject* baseClass, const QMetaObject* targetClass);

private slots:
  void prepFinished()
  {
    mPrepFinished = true;
  }

private:
  static bool isMappedClass(const QString& className);
  static int hashString(const QString& inputStr);
  static QString capitalize(const QString& inputStr);
  static int findSetter(const QMetaObject* mo, const QMetaProperty* prop);
  static int findGetter(const QMetaObject* mo, const QMetaProperty* prop);
  static QString nativeClassName(const QString& className);
  static QString mappedClassName(const QString& className);

  static const QMetaMethod* findMethodBySignature(const QMetaObject* mo, const QString& sig);
  static const QMetaProperty* findPropBySignature(const QMetaObject* mo, const QString& sig);
  static bool isInheritedSignature(const QMetaObject* mo, const QString& sig);  
  static bool isInheritedPropertySignature(const QMetaObject* mo, const QString& sig);
  static QString FormatCSharpEnum(const QString& name);
  static QString methodCDeclaration(const QMetaObject* mo, const QMetaMethod& method);
  static QString methodDllImportDeclaration(const QString& importIndent, const QMetaObject* mo, const QMetaMethod& method);
  static QString ConvertToMarshallableReturnType(const QString& inputType);
  static QString ConvertToMarshallableParamType(const QString& inputType);
  static QString ConvertToCSharpReturnType(const QString& inputType);
  static QString ConvertToCSharpParamType(const QString& inputType);
  static QString ConvertToCSharpNativeReturnType(const QString& inputType);
  static QString ConvertToCSharpNativeParamType(const QString& inputType);
  static void generateCSharpEnums(const QString& outputDir, const QMetaObject* mo);

  static void writeCSharpMethodImplementation(QTextStream& csout,const QMetaObject* mo, const QMetaMethod& meth); 

  static void writeDllImports(QTextStream& csout, const QMetaObject* mo);
  static void writeCSharpMethods(QTextStream& out, QTextStream& csout, const QMetaObject* mo);
  static void generateCSharpWrapper(ScriptingEngine* engine,  const QString& typeName, const QMetaObject* mo);
  static bool registerScriptTypes();
  static void generateCSharp(ScriptingEngine* engine, QStringList& classList, const QString& outputDir, const QString& typeName, const QMetaObject* mo);

  static void generateAPIDocumentation(ScriptingEngine* engine, QStringList& classList, const QString& outputDir, const QString& typeName, const QMetaObject* mo);
  static void writeHtmlDoc(QTextStream& out, ScriptingEngine* engine, const QMetaObject* mo);
  static void loadIntellisenseVariables();
  static void extractResourceFile(const QString& outputDir, const QString& fileName);

  static void writeSignalsDescr(QTextStream& out, ScriptingEngine* engine, const QMetaObject* mo);
  static void writeEnumsDescr(QTextStream& out, ScriptingEngine* engine, const QMetaObject* mo);
  static void writePropertiesDescr(QTextStream& out, ScriptingEngine* engine, const QMetaObject* mo);
  static void writeMethodsDescr(QTextStream& out, ScriptingEngine* engine, const QMetaObject* mo);

  static void writeSignalsBody(QTextStream& out, ScriptingEngine* engine, const QMetaObject* mo);
  static void writeEnumsBody(QTextStream& out, ScriptingEngine* engine, const QMetaObject* mo);
  static void writePropertiesBody(QTextStream& out, ScriptingEngine* engine, const QMetaObject* mo);
  static void writeMethodsBody(QTextStream& out, ScriptingEngine* engine, const QMetaObject* mo);

  QMap<QString, QString> mVars;
  bool mPrepFinished;
  bool mEnableIntellisense;
  bool mIntellisenseVars;
  LoadAPIThread mAPIThread;
  static bool mLoadedAPIs;
  static QMap<QString, const QMetaObject*> mTypeMap;
  static QMap<QString, const QMetaObject*> mMapVarToAPI;
  static QList<const QMetaObject*> mInternalClasses;
  static QStringList mVarsMapList;

  QList<const QMetaObject*> mTypeStack;
};

#endif
