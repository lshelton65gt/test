#ifndef __CARBONMAKERCONTEXT_H__
#define __CARBONMAKERCONTEXT_H__

#include "util/CarbonPlatform.h"
#include "shell/ShellGlobal.h"
#include "util/UtString.h"
#include "gui/CQt.h"
#include "util/TimebombHelper.h"

#include <QTabWidget>
#include <QDate>

class ProjectExplorer;
class SettingsEditor;
class CarbonProjectWidget;
class MDIDocumentManager;
class QWorkspace;
class QMainWindow;
class PropertyEditor;
class CQtContext;
class WorkspaceModelMaker;
class cmm;
class CarbonModelsWidget;

class CarbonMakerContext
{
public:
  CarbonMakerContext()
  {
    mProjectExplorer=NULL;
    mSettingsEditor=NULL;
    mCarbonProjectWidget=NULL;
    mDocumentManager=NULL;
    mWorkspace=NULL;
    mMainWindow=NULL;
    mSourceTabWidget=NULL;
    mQtContext=NULL;
    mWorkspaceModelMaker=NULL;
    mCanTimeBomb=false;

    TimebombHelper tb;

    UtString tbs;
    UtString tbf;
    if (tb.getLatestTimebomb(&tbs, &tbf) > 0)
    {
      mCanTimeBomb = true;
      mTimeBombDate = QDate::fromString(tbs.c_str(), "yyyy-MM-dd");
    }

#ifdef _DEBUG
    mCarbonInternal=true;
    mCanCreateModelKits=true;
    mCanCreateCoware=true;
    mCanCreateMaxsim=true;
    mCanCreateSystemC=true;
    mCanCreateMV=true;
    mCanCreatePackage=true;
    mCanTimeBomb=true;
    mCanAmbaCompiler=false;
    mCanCarbonCompiler=true;
    mCanGenSoCDSystemCExport = true;
#else
    mCarbonInternal=false;
    mCanCreateModelKits=false;
    mCanCreateCoware=false;
    mCanCreateMaxsim=false;
    mCanCreateSystemC=false;
    mCanCreateMV=false;
    mCanCreatePackage=false;
    mCanAmbaCompiler=false;
    mCanCarbonCompiler=false;
    mCanGenSoCDSystemCExport = false;
#endif
    mModelStudio=NULL;
    mModelsWidget = NULL;

    mHasLicenseInfrastructure = getenv("LM_LICENSE_FILE") != NULL || getenv("CARBON_LICENSE_FILE") != NULL || getenv("CARBOND_LICENSE_FILE") != NULL;

#ifndef _DEBUG
    if (mHasLicenseInfrastructure)
    {
      ShellGlobal::setLicenseQueuing(false);
      mCanCreateCoware = ShellGlobal::initPlatArch();
      mCanCreateMaxsim = ShellGlobal::initSocVsp();
      mCanCreateSystemC = ShellGlobal::initSystemC();
      mCanCreateMV = ShellGlobal::initModelValidation();
      mCanCreatePackage = ShellGlobal::initPackage();
      mCanCreateModelKits = ShellGlobal::initModelKits();
      mCarbonInternal = ShellGlobal::initModelKits();
      mCanCarbonCompiler = ShellGlobal::initCompiler();
      mCanAmbaCompiler = ShellGlobal::initADCompiler();
      mCanGenSoCDSystemCExport = ShellGlobal::initSoCDSystemCExport();
    }
#endif   
    if (getenv("CARBON_NO_MODELKITS"))
      mCanCreateModelKits = false;
  }

  bool checkoutSimulationRuntime(const QString& feature, UtString* reason)
  {
    if (mHasLicenseInfrastructure)
    {
      ShellGlobal::setLicenseQueuing(false);
      UtString custStr; custStr << feature;
      return ShellGlobal::checkoutSimulationRuntime(custStr.c_str(), reason);
    }
    else
      return false;
  }

  bool checkoutRuntimeCustomer(const QString& licenseName, UtString* reason)
  {
    if (mHasLicenseInfrastructure)
    {
      ShellGlobal::setLicenseQueuing(false);
      UtString custStr; custStr << licenseName;
      return ShellGlobal::checkoutRuntimeCustomer(custStr.c_str(), reason);
    }
    else
      return false;
  }


  void putMainWindow(QMainWindow* newVal) { mMainWindow=newVal; }
  void putProjectExplorer(ProjectExplorer* newVal) { mProjectExplorer=newVal; }
  void putSettingsEditor(SettingsEditor* newVal) { mSettingsEditor=newVal; }
  void putCarbonProjectWidget(CarbonProjectWidget* newVal) { mCarbonProjectWidget=newVal; }
  void putDocumentManager(MDIDocumentManager* newVal) { mDocumentManager=newVal; }
  void putWorkspace(QWorkspace* newVal) { mWorkspace=newVal; }
  void putSourceTabWidget(QTabWidget* newVal) { mSourceTabWidget=newVal; }
  void putQtContext(CQtContext* newVal) { mQtContext=newVal; }
  void putWorkspaceModelMaker(WorkspaceModelMaker* newVal) { mWorkspaceModelMaker=newVal; }
  void putModelStudio(cmm* newVal) { mModelStudio=newVal; }
  void putCarbonModelsWidget(CarbonModelsWidget* new_val) {mModelsWidget=new_val;}

  bool getCanCreateCoware() { return mCanCreateCoware; }
  bool getCanCreateSystemC() { return mCanCreateSystemC; }
  bool getCanCreateMaxsim() { return mCanCreateMaxsim; }
  bool getCanCreateMV() { return mCanCreateMV; }
  bool getCanCreatePackage() { return mCanCreatePackage; }
  bool getCanCreateModelKits() { return mCanCreateModelKits; }
  bool getCarbonInternal() { return mCarbonInternal; }
  bool getCanTimeBomb() { return (mCanTimeBomb || mCarbonInternal); }
  bool getCanCarbonCompiler() { return (mCanCarbonCompiler); }
  bool getCanAmbaCompiler() { return (mCanAmbaCompiler); }
  bool getCanGenSoCDSystemCExport() { return mCanGenSoCDSystemCExport; }

  QDate getTimeBombDate() { return mTimeBombDate; }

  cmm* getModelStudio() { return mModelStudio; }
  CarbonModelsWidget* getCarbonModelsWidget() const {return mModelsWidget;}

  ProjectExplorer* getProjectExplorer() { return mProjectExplorer; }
  SettingsEditor* getSettingsEditor() { return mSettingsEditor; }
  CarbonProjectWidget* getCarbonProjectWidget() { return mCarbonProjectWidget; }
  QWorkspace* getWorkspace() { return mWorkspace; }
  MDIDocumentManager* getDocumentManager() { return mDocumentManager; }
  QMainWindow* getMainWindow() { return mMainWindow; }
  QTabWidget* getSourceTabWidget() { return mSourceTabWidget; }
  CQtContext* getQtContext() { return mQtContext; }
  WorkspaceModelMaker* getWorkspaceModelMaker() { return mWorkspaceModelMaker; }

private:
  ProjectExplorer* mProjectExplorer;
  SettingsEditor* mSettingsEditor;
  CarbonProjectWidget* mCarbonProjectWidget;
  MDIDocumentManager* mDocumentManager;
  QWorkspace* mWorkspace;
  QMainWindow* mMainWindow;
  QTabWidget* mSourceTabWidget;
  CQtContext* mQtContext;
  WorkspaceModelMaker* mWorkspaceModelMaker;
  cmm* mModelStudio;
  QDate mTimeBombDate;
  bool mCarbonInternal;
  bool mCanCreateCoware;
  bool mCanCreateMaxsim;
  bool mCanCreateSystemC;
  bool mCanCreateMV;
  bool mCanCreatePackage;
  bool mCanCreateModelKits;
  bool mCanCarbonCompiler;
  bool mCanAmbaCompiler;
  bool mCanGenSoCDSystemCExport;
  bool mHasLicenseInfrastructure;

  bool mCanTimeBomb;
  CarbonModelsWidget* mModelsWidget;
};

#endif

