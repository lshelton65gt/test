//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "gui/CQt.h"

#include "DirectivesEditor.h"
#include "CarbonDirectives.h"

CarbonModuleDirectivesTool::CarbonModuleDirectivesTool(QObject* parent, CarbonDirectives* dirs, const char* modules) : QObject(parent), CarbonTool("CarbonDirectives", "") 
{
  mDirectives = dirs;

  mProperties.readPropertyDefinitions(":/cmm/Resources/CarbonModuleDirectiveOptions.xml");
  mDefaultOptions = new CarbonOptions(NULL, getProperties(), "ModuleDirectiveDefaults");
  mOptions = new CarbonOptions(mDefaultOptions, getProperties(), "CarbonDirectives");

  // Now, process the modules
  processModules(modules);
}

CarbonNetDirectivesTool::CarbonNetDirectivesTool(QObject* parent, CarbonDirectives* dirs, const char* nets) : QObject(parent), CarbonTool("CarbonDirectives", "") 
{
  mDirectives = dirs;

  mProperties.readPropertyDefinitions(":/cmm/Resources/CarbonNetDirectiveOptions.xml");
  mDefaultOptions = new CarbonOptions(NULL, getProperties(), "NetDirectiveDefaults");
  mOptions = new CarbonOptions(mDefaultOptions, getProperties(), "CarbonDirectives");

  // Now, process the nets
  processNets(nets);
}
void CarbonNetDirectivesTool::netRenamed(DirectiveType, const char*, const char* newName)
{
  mNets = newName;
}

void CarbonNetDirectivesTool::directivesChanged(DirectiveType dirType)
{
  if (dirType == eDirectiveNet)
  {
    qDebug() << "Net directives changed";

    // Get the new directive(s) values
    mDirectives = mDirectives->getProject()->getDirectives();

    processNets(mNets.c_str());
    //mDirectives->getProject()->getProjectWidget()->context()->getSettingsEditor()->setSettings(getOptions());
  }
}

void CarbonModuleDirectivesTool::directivesChanged(DirectiveType dirType)
{
  if (dirType == eDirectiveModule)
  {
    qDebug() << "Module directives changed";

    // Get the new directive(s) values
    mDirectives = mDirectives->getProject()->getDirectives();

    processModules(mModules.c_str());
  }
}

void CarbonNetDirectivesTool::processNets(const char* netsString)
{
  mNets = netsString;

  QString nets = netsString;
  QStringList netList = nets.split("\n");
  QVariantList netData;
  CarbonDirectiveGroup* group = mDirectives->getActiveGroup();
  INFO_ASSERT(group, "Expecting active nets group");

  QMap<QString, CarbonNetDirectivesTool::StateValue> dirMap;
  // Loop through all directives
  for (CarbonOptions::OptionsLoop p = mDefaultOptions->loopOptions(); !p.atEnd(); ++p)
  {
    CarbonPropertyValue* sv = p.getValue();
    const char* directiveName = sv->getProperty()->getName();
    dirMap[directiveName] = CarbonNetDirectivesTool::Undefined;
  }

  bool firstNet = true;

  // build up a map of all directives and an indicator that the value has been set
  foreach (QString net, netList) 
  {
    UtString netName;
    netName << net;
    netData.append(net);

    // foreach net,directive
    for (CarbonOptions::OptionsLoop p = mDefaultOptions->loopOptions(); !p.atEnd(); ++p)
    {
      CarbonPropertyValue* sv = p.getValue();
      const char* directiveName = sv->getProperty()->getName();
      NetDirectives* netDirs = group->findNetDirectives(netName.c_str());

      if (firstNet)
      {
        if (netDirs && netDirs->findDirective(directiveName))
          dirMap[directiveName] = CarbonNetDirectivesTool::Set;
      }
      else
      {
        if (netDirs && netDirs->findDirective(directiveName))
        {
          if (dirMap.value(directiveName) != CarbonNetDirectivesTool::Set)
            dirMap[directiveName] = CarbonNetDirectivesTool::Undefined;
        }
        else
          dirMap[directiveName] = CarbonNetDirectivesTool::Undefined;
      }
    }

    firstNet = false;
  }

  QMap<QString, CarbonNetDirectivesTool::StateValue>::const_iterator i = dirMap.constBegin();
  while (i != dirMap.constEnd())
  {
    UtString directiveName;
    directiveName << i.key();
    StateValue value = static_cast<StateValue>(i.value());
    
    if (value == CarbonNetDirectivesTool::Undefined)
      mOptions->putValue(directiveName.c_str(), "");
    else
      mOptions->putValue(directiveName.c_str(), "true");

     ++i;
   }

  UtString showItem;
  if (netData.count() == 1)
    showItem << netList.at(0);

  // set the Item list for multiple items
  mOptions->setItems(showItem.c_str(), netList, netData);

}


void CarbonModuleDirectivesTool::processModules(const char* modulesString)
{
  mModules = modulesString;

  QString modules = modulesString;
  QStringList modList = modules.split("\n");
  QVariantList modData;
  CarbonDirectiveGroup* group = mDirectives->getActiveGroup();
  INFO_ASSERT(group, "Expecting active module group");

  QMap<QString, CarbonModuleDirectivesTool::StateValue> dirMap;
  // Loop through all directives
  for (CarbonOptions::OptionsLoop p = mDefaultOptions->loopOptions(); !p.atEnd(); ++p)
  {
    CarbonPropertyValue* sv = p.getValue();
    const char* directiveName = sv->getProperty()->getName();
    dirMap[directiveName] = CarbonModuleDirectivesTool::Undefined;
  }

  bool firstMod = true;

  // build up a map of all directives and an indicator that the value has been set
  foreach (QString mod, modList) 
  {
    UtString modName;
    modName << mod;
    modData.append(mod);

    // foreach net,directive
    for (CarbonOptions::OptionsLoop p = mDefaultOptions->loopOptions(); !p.atEnd(); ++p)
    {
      CarbonPropertyValue* sv = p.getValue();
      const char* directiveName = sv->getProperty()->getName();
      ModuleDirectives* modDirs = group->findModuleDirectives(modName.c_str());

      if (firstMod)
      {
        if (modDirs && modDirs->findDirective(directiveName))
          dirMap[directiveName] = CarbonModuleDirectivesTool::Set;
      }
      else
      {
        if (modDirs && modDirs->findDirective(directiveName))
        {
          if (dirMap.value(directiveName) != CarbonModuleDirectivesTool::Set)
            dirMap[directiveName] = CarbonModuleDirectivesTool::Undefined;
        }
        else
          dirMap[directiveName] = CarbonModuleDirectivesTool::Undefined;
      }
    }

    firstMod = false;
  }

  QMap<QString, CarbonModuleDirectivesTool::StateValue>::const_iterator i = dirMap.constBegin();
  while (i != dirMap.constEnd())
  {
    UtString directiveName;
    directiveName << i.key();
    StateValue value = static_cast<StateValue>(i.value());
    
    if (value == CarbonModuleDirectivesTool::Undefined)
      mOptions->putValue(directiveName.c_str(), "");
    else
      mOptions->putValue(directiveName.c_str(), "true");

     ++i;
   }

  UtString showItem;
  if (modData.count() == 1)
    showItem << modList.at(0);

  // set the Item list for multiple items
  mOptions->setItems(showItem.c_str(), modList, modData);

}

CarbonDirectives::CarbonDirectives(CarbonProject* proj) 
{
  mNetToolOptions = NULL;
  mModuleToolOptions = NULL;

  mProject = proj;
  mActiveGroup = NULL;

  readDefinitions();

  CarbonConfigurations* configs = proj->getConfigurations();

  if (configs->numConfigurations() == 0)
  {
    CarbonDirectiveGroup* group = new CarbonDirectiveGroup(this, proj, "Default", "Linux");

    if (mActiveGroup == NULL)
      mActiveGroup = group;

    mConfigs.push_back(group);
  }

  for (UInt32 i=0; i<configs->numConfigurations(); i++)
  {
    CarbonConfiguration* projConfig = configs->getConfiguration(i);
    const char* configName = projConfig->getName();

    CarbonDirectiveGroup* group = new CarbonDirectiveGroup(this, proj, configName, "Linux");

    if (mActiveGroup == NULL)
      mActiveGroup = group;

    mConfigs.push_back(group);
  }

  CQT_CONNECT(proj, configurationChanged(const char*), this, configurationChanged(const char*));
  CQT_CONNECT(proj, configurationRenamed(const char*, const char*), this, configurationRenamed(const char*, const char*));
  CQT_CONNECT(proj, configurationCopy(const char*, const char*), this, configurationCopy(const char*, const char*));
  CQT_CONNECT(proj, configurationRemoved(const char*), this, configurationRemoved(const char*));
  CQT_CONNECT(proj, configurationAdded(const char*), this, configurationAdded(const char*));
  
  CQT_CONNECT(proj->getProjectWidget(), projectClosing(CarbonProject*), this, projectClosing(CarbonProject*));
  CQT_CONNECT(proj->getProjectWidget(), projectLoaded(const char*, CarbonProject*), this, projectLoaded(const char*, CarbonProject*));
}

void CarbonDirectives::projectClosing(CarbonProject* proj)
{
  if (proj) // only if we had a valid project loaded
  {
    INFO_ASSERT(proj == mProject, "Unexpected project");
    mProject->getProjectWidget()->context()->getSettingsEditor()->setSettings(NULL);
    HierarchyDockWindow* hierWindow =  mProject->getProjectWidget()->getHierarchyWindow();
    if (hierWindow)
      hierWindow->clear();
  }

  mProject = NULL;
}

// After a project has been loaded, the hierarchyWindow will be valid
// so at this time, then hookup to any selection changes so we can update
// the properties window accordingly.
void CarbonDirectives::projectLoaded(const char* /* projName */, CarbonProject* proj)
{
  mProject = proj;

  INFO_ASSERT(mProject->getProjectWidget()->getHierarchyWindow(), "Expecting hierarchy window");

  CQT_CONNECT(mProject->getProjectWidget()->getHierarchyWindow(), netSelectionChanged(const char*), this, netSelectionChanged(const char*));
  CQT_CONNECT(mProject->getProjectWidget()->getHierarchyWindow(), moduleSelectionChanged(const char*), this, moduleSelectionChanged(const char*));

  // CQT_CONNECT(mProject->getProjectWidget()->getHierarchyWindow(), observableSignal(CarbonDBNode *), this, observableSignalChanged(CarbonDBNode *));

}


void CarbonDirectives::netItemSelectionChanged(const char* net, const QVariant& /*data*/)
{
  mProject->getProjectWidget()->getHierarchyWindow()->selectNet(net);
}

void CarbonDirectives::moduleItemSelectionChanged(const char* module, const QVariant& /*data*/)
{
  mProject->getProjectWidget()->getHierarchyWindow()->selectModule(module);
}

//void CarbonDirectives::observableSignal(CarbonDBNode *node)
//{
//  qDebug()<<"Foo";
//}
void CarbonDirectives::netSelectionChanged(const char* nets)
{
  if (mNetToolOptions)
    delete mNetToolOptions;

  mNetToolOptions = new CarbonNetDirectivesTool(this, this, nets);

  CQT_CONNECT(mNetToolOptions->getOptions(), propertyModified(const CarbonProperty*,const char*,const char*), this, propertyModified(const CarbonProperty*,const char*,const char*));
  CQT_CONNECT(mNetToolOptions->getOptions(), selectionChanged(const char*, const QVariant&), this, netItemSelectionChanged(const char*, const QVariant&));

  DirectivesEditor* dirEditor = mProject->getProjectWidget()->directivesEditor();
  if (dirEditor)
  {
    TabNetDirectives* netDirs = dirEditor->getNetDirectivesTab();
    netDirs->selectItems(nets);
    CQT_CONNECT(dirEditor, directivesChanged(DirectiveType), mNetToolOptions, directivesChanged(DirectiveType));
    CQT_CONNECT(dirEditor, netRenamed(DirectiveType,const char*,const char*), mNetToolOptions, netRenamed(DirectiveType,const char*,const char*));
  }

  if (mProject->getProjectType() == CarbonProject::HDL)
    mProject->getProjectWidget()->context()->getSettingsEditor()->setSettings(mNetToolOptions->getOptions());
}
void CarbonDirectives::moduleSelectionChanged(const char* modules)
{
  if (mModuleToolOptions)
    delete mModuleToolOptions;

  mModuleToolOptions = new CarbonModuleDirectivesTool(this, this, modules);

  CQT_CONNECT(mModuleToolOptions->getOptions(), propertyModified(const CarbonProperty*,const char*,const char*), this, modPropertyModified(const CarbonProperty*,const char*,const char*));
  CQT_CONNECT(mModuleToolOptions->getOptions(), selectionChanged(const char*, const QVariant&), this, moduleItemSelectionChanged(const char*, const QVariant&));

  DirectivesEditor* dirEditor = mProject->getProjectWidget()->directivesEditor();
  if (dirEditor)
    CQT_CONNECT(dirEditor, directivesChanged(DirectiveType), mModuleToolOptions, directivesChanged(DirectiveType));

  if (mProject->getProjectType() == CarbonProject::HDL)
    mProject->getProjectWidget()->context()->getSettingsEditor()->setSettings(mModuleToolOptions->getOptions());
}

void CarbonDirectives::propertyModified(const CarbonProperty* prop, const char* oldValue, const char* newValue)
{
  if (strlen(oldValue) == 0 && 0 == strcmp(newValue, "false"))
    mNetToolOptions->getOptions()->putValue(prop->getName(), "");

  DirectivesEditor* editor = mProject->getProjectWidget()->compilerDirectives();
  INFO_ASSERT(editor, "Expecting Directives Editor");

  if (0 == strcmp(newValue, "true"))
  {
    qDebug() << "Property: " << prop->getName() << " old value: " << oldValue << " new value: " << newValue;
    editor->getNetDirectivesTab()->setDirective(mNetToolOptions->getOptions()->getItems(), prop->getName(), true);
  }
  else if (0 == strcmp(newValue, "false"))
  {
    qDebug() << "Property: " << prop->getName() << " old value: " << oldValue << " new value: " << newValue;
    editor->getNetDirectivesTab()->setDirective(mNetToolOptions->getOptions()->getItems(), prop->getName(), false);
  }
}

void CarbonDirectives::modPropertyModified(const CarbonProperty* prop, const char* oldValue, const char* newValue)
{
  if (strlen(oldValue) == 0 && 0 == strcmp(newValue, "false"))
    mModuleToolOptions->getOptions()->putValue(prop->getName(), "");

  DirectivesEditor* editor = mProject->getProjectWidget()->compilerDirectives();

  if (0 == strcmp(newValue, "true"))
  {
    qDebug() << "Property: " << prop->getName() << " old value: " << oldValue << " new value: " << newValue;
    editor->getModuleDirectivesTab()->setDirective(mModuleToolOptions->getOptions()->getItems(), prop->getName(), true);
  }
  else if (0 == strcmp(newValue, "false"))
  {
    qDebug() << "Property: " << prop->getName() << " old value: " << oldValue << " new value: " << newValue;
    editor->getModuleDirectivesTab()->setDirective(mModuleToolOptions->getOptions()->getItems(), prop->getName(), false);
  }
}
// Read the CarbonDirectives.xml file and build the list and map
void CarbonDirectives::readDefinitions()
{
  const char* xmlFile = ":/cmm/Resources/CarbonDirectives.xml";
  QFile xf(xmlFile);
  if (xf.open(QFile::ReadOnly))
  {
    QTextStream ts(&xf);
    QString buffer = ts.readAll();
    UtString sbuffer;
    sbuffer << buffer;

    xmlDocPtr doc = xmlReadMemory(sbuffer.c_str(), buffer.length(), xmlFile, NULL, 0);
    if (doc != NULL) 
    {
      UtXmlErrorHandler eh;
      xmlNode *rootNode = xmlDocGetRootElement(doc);
      INFO_ASSERT(xmlStrcmp(rootNode->name, BAD_CAST "Directives") == 0, "Expecting Directives");
      for (xmlNodePtr child = rootNode->children; child != NULL; child = child->next) 
      {
        const char* elementName = XmlParsing::elementName(child);
        if (elementName == NULL)
          continue;
        if (0 == strcmp(elementName, "Directive"))
        { 
          DirectiveDefinition* def = new DirectiveDefinition;
          def->mEditor = eDirEditorNone;
          
          UtString name;
          XmlParsing::getProp(child, "Name", &name);

          UtString appliesTo;
          XmlParsing::getProp(child, "AppliesTo", &appliesTo);

          UtString builtinType;
          XmlParsing::getProp(child, "Type", &builtinType);

          if (0 == strcmp(builtinType.c_str(), "Builtin"))
            def->mBuiltin = true;
          if (0 == strcmp(appliesTo.c_str(), "Net"))
            def->mMode = eDirInstance;
          else if (0 == strcmp(appliesTo.c_str(), "Module"))
            def->mMode = eDirModule;
          else 
            def->mMode = eDirBoth;

          def->mName << name;

          mDirectorDefinitions.push_back(def);
          mDefinitionMap[name.c_str()] = def;
        }
      }
      xmlFreeDoc(doc);
    }
  }
}

CarbonDirectiveGroup* CarbonDirectives::findDirectiveGroup(const char* newConfig)
{
 const char* platform = "Linux";

 foreach (CarbonDirectiveGroup* dir, mConfigs)
 {
   if (0 == strcmp(dir->getName(), newConfig) && 0 == strcmp(dir->getPlatform(), platform))
    return dir;
 }
  return NULL;
}

CarbonDirectiveGroup* CarbonDirectives::findDirectiveGroup(const char* name, const char* platform)
{
 foreach (CarbonDirectiveGroup* dir, mConfigs)
 {
   if (0 == strcmp(dir->getName(), name) && 0 == strcmp(dir->getPlatform(), platform))
    return dir;
 }
  return NULL;
}

void CarbonDirectives::configurationAdded(const char* newConfig)
{
  qDebug() << "CarbonDirectives: Added Configuration: " << newConfig;

  addDirectiveGroup(newConfig, mProject->getActive()->getPlatform());
}

void CarbonDirectives::configurationChanged(const char* newConfig)
{
  qDebug() << "Directives Config changed to: " << newConfig;
  mActiveGroup = findDirectiveGroup(newConfig);
  INFO_ASSERT(mActiveGroup, "Unable to locate Directive Group");
}

void CarbonDirectives::configurationRemoved(const char* removedConfig)
{
 const char* platform = "Linux";

 int i=0;
 foreach (CarbonDirectiveGroup* dir, mConfigs)
 {
   if (0 == strcmp(dir->getName(), removedConfig) && 0 == strcmp(dir->getPlatform(), platform))
   {
     mConfigs.removeAt(i);
     break;
   }
   i++;
 }
}

void CarbonDirectives::configurationRenamed(const char* oldName, const char* newName)
{
  CarbonDirectiveGroup* oldGroup = findDirectiveGroup(oldName);
  oldGroup->putName(newName);
  qDebug() << "Directives Config rename: " << oldName << " to: " << newName;
}

void CarbonDirectives::configurationCopy(const char* oldName, const char* newName)
{
  CarbonDirectiveGroup* oldGroup = findDirectiveGroup(oldName);
  CarbonDirectiveGroup* newGroup = findDirectiveGroup(newName);
  INFO_ASSERT(oldGroup, "Expecting Old Directive Group");
  INFO_ASSERT(newGroup, "Expecting New Directive Group");
  newGroup->copyDirectives(oldGroup);
}

void CarbonDirectives::deserialize(CarbonProject* /*proj*/, xmlNodePtr parent, UtXmlErrorHandler* eh)
{
  // Is this a CowareComponent?
  if (XmlParsing::isElement(parent, "CompilerDirectives"))
  {    
    for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
    {
      if (XmlParsing::isElement(child, "Directives"))
        CarbonDirectiveGroup::deserialize(this, child, eh);
    }
  }
}

void CarbonDirectiveGroup::removeAllNetDirectives()
{
  foreach (NetDirectives* dir, mNetDirectives)
    delete dir;
  mNetDirectives.clear();
}

void CarbonDirectiveGroup::removeAllModuleDirectives()
{
  foreach (ModuleDirectives* dir, mModuleDirectives)
    delete dir;
  mModuleDirectives.clear();
}

bool CarbonDirectives::serialize(xmlTextWriterPtr writer)
{
  xmlTextWriterStartElement(writer, BAD_CAST "CompilerDirectives");

  foreach (CarbonDirectiveGroup* dir, mConfigs)
    dir->serialize(this, writer);

  xmlTextWriterEndElement(writer);
  
  return true;
}

CarbonDirectiveGroup::CarbonDirectiveGroup(CarbonDirectives* dirs, CarbonProject* proj, const char* name, const char* platform)
{
  mDirectives = dirs;
  mProject = proj;
  mName = name;
  mPlatform = platform;
}

void CarbonDirectiveGroup::copyDirectives(CarbonDirectiveGroup* copyFrom)
{
  // for each "Old" directive
  foreach (NetDirectives* oldDir, copyFrom->mNetDirectives)
    oldDir->copyDirectives(this);

  foreach (ModuleDirectives* newDir, copyFrom->mModuleDirectives)
    newDir->copyDirectives(this);
}

NetDirectives* CarbonDirectiveGroup::findNetDirectives(const char* name)
{
  foreach(NetDirectives* dir, mNetDirectives)
  {
    if (dir->mLocator == name)
      return dir;
  }
  return NULL;
}

ModuleDirectives* CarbonDirectiveGroup::findModuleDirectives(const char* name)
{
  foreach(ModuleDirectives* dir, mModuleDirectives)
  {
    if (dir->mLocator == name)
      return dir;
  }
  return NULL;
}

bool CarbonDirectiveGroup::serialize(CarbonDirectives* dirs, xmlTextWriterPtr writer)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Directives");

  UtString configName;
  configName << getName() << "|" << getPlatform();

  xmlTextWriterWriteAttribute(writer, BAD_CAST "Configuration", BAD_CAST configName.c_str());

  xmlTextWriterStartElement(writer, BAD_CAST "NetDirectives");
  foreach (NetDirectives* dir, mNetDirectives)
    dir->serialize(writer, dirs, this);
  xmlTextWriterEndElement(writer);

  xmlTextWriterStartElement(writer, BAD_CAST "ModuleDirectives");
  foreach (ModuleDirectives* dir, mModuleDirectives)
    dir->serialize(writer, dirs, this);
  xmlTextWriterEndElement(writer);

  xmlTextWriterEndElement(writer);

  return true;
}

CarbonDirectiveGroup* CarbonDirectiveGroup::deserialize(CarbonDirectives* dirs, xmlNodePtr parent, UtXmlErrorHandler* eh)
{
  UtString configName;
  XmlParsing::getProp(parent, "Configuration", &configName);
  QString cname = configName.c_str();
  QStringList configParts = cname.split('|');

  QString qname = configParts[0];
  QString qplat = configParts[1];

  UtString name;
  name << qname;
  UtString platform;
  platform << qplat;

  CarbonDirectiveGroup* dir = dirs->findDirectiveGroup(name.c_str(), platform.c_str());
  if (dir == NULL)
    dir = dirs->addDirectiveGroup(name.c_str(), platform.c_str());
   
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    const char* elementName = XmlParsing::elementName(child);
    if (elementName == NULL)
      continue;

    if (0 == strcmp(elementName, "NetDirectives"))
      Directive::deserialize(eDirectiveNet, dirs, dir, child, eh);
    else if (0 == strcmp(elementName, "ModuleDirectives"))
      Directive::deserialize(eDirectiveModule, dirs, dir, child, eh);
  }

  return NULL;
}
