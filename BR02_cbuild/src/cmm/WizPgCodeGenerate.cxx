//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "WizPgCodeGenerate.h"
#include "MemoryWizard.h"
#include "Template.h"
#include "WizardTemplate.h"

WizPgCodeGenerate::WizPgCodeGenerate(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);

  registerField("generatedCode", ui.textBrowser, "text");
}

WizPgCodeGenerate::~WizPgCodeGenerate()
{

}
void WizPgCodeGenerate::initializePage()
{
  setTitle("Code Generation");
  setSubTitle("The resulting generated code for this new model.");

  MemoryWizard* wiz = (MemoryWizard*)wizard();
  WizardContext* context = wiz->getTemplate()->getContext();
  Template* templ = context->getTemplate();

  if (templ->getLanguage() == Template::Verilog)
    ui.textBrowser->setLanguage(ColorizedEditor::Verilog);
  else if (templ->getLanguage() == Template::VHDL)
    ui.textBrowser->setLanguage(ColorizedEditor::VHDL);

  ui.textBrowser->clear();
  ui.textBrowser->setText(context->generateCode());

  QString replayFile = wiz->getReplayFile();
  if (replayFile.length() > 0)
    wiz->postNext();
}

