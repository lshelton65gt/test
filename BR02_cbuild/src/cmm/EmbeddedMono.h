#ifndef __EMBEDDEDMONO_H__
#define __EMBEDDEDMONO_H__
#include "util/CarbonVersion.h"

/*
When enabling the debugger, if you want to do source code 
debugging, you must have .MDB files not .PDB files, on Linux
you can use pdb2mdb
*/


#if defined(Q_OS_WIN32)
#define PINVOKEABLE extern "C" __declspec(dllexport)
#else
#define PINVOKEABLE extern "C"
#endif

#include "cmm.h"
#include "CarbonProject.h"

typedef struct _MonoDomain MonoDomain;
typedef struct _MonoAssembly MonoAssembly;
typedef struct _MonoImage MonoImage;
typedef struct _MonoMethod MonoMethod;
typedef struct _MonoClass MonoClass;
typedef struct _MonoException MonoException;

#include <QtGui>

#include <mono/metadata/metadata.h>
#include <mono/metadata/object.h>


class EmbeddedMono
{
public:
  static EmbeddedMono* Instance(cmm* mw);
  static EmbeddedMono* Instance();
  static cmm* GetMainWindow();
  int RunMonoProgram(const char* programPath, const char* nameSpace, const char* className, const char* methodName);
  MonoAssembly* LoadAssembly(const char* programPath);
  MonoObject* ConstructObject(MonoAssembly* assembly, const char* nameSpace, const char* className);
  MonoObject* InvokeMethod(MonoAssembly* assembly, const char* nameSpace, const char* className, const char* methodName, int numParams, void** params, MonoObject* instance, MonoObject** exception);
  MonoDomain* GetDomain() { return mDomain; }
  MonoString* NewString(const char* str); 
  const char* ToString(MonoString* s);
  void CMSConsolePrint(const QString& str);
  static char* AllocString(const QString& str);

protected:
  ~EmbeddedMono(void);

private:
  CarbonConsole* getConsole();
  MonoMethod* findMethod(MonoClass* klass, const char* methodName);

  EmbeddedMono(){};

  static EmbeddedMono* mInstance;
  static MonoDomain* mDomain;
  static cmm* mMainWindow;
  static QString mLastError;
};

#endif
