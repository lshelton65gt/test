#ifndef MKWPAGEVERSIONS_H
#define MKWPAGEVERSIONS_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>
#include "WizardPage.h"
#include <QWizardPage>
#include "ui_MKWPageVersions.h"

#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "ModelKit.h"

class MKWPageVersions : public WizardPage
{
  Q_OBJECT

public:
  MKWPageVersions(QWidget *parent = 0);
  ~MKWPageVersions();

protected:
  virtual void initializePage();
  virtual bool validatePage();
  virtual int nextId() const;
  virtual bool isComplete() const { return mIsValid; }

  void populate(ModelKit* kit);
  void updateCheckboxes();
  void updateVersionButtons();

private:
  Ui::MKWPageVersionsClass ui;
  QSettings mSettings;
  bool mIsValid;

private:
  void saveSettings();
  void restoreSettings();
  void setItemData(QTreeWidgetItem* item, KitVersion* ver);
  void autoFit();

private slots:
    void on_toolButtonEpilogueScript_clicked();
    void on_toolButtonPrefaceScript_clicked();
    void on_treeWidget_itemDoubleClicked(QTreeWidgetItem*,int);
    void on_pushButtonDeleteVersion_clicked();
    void on_pushButtonEditVersion_clicked();
    void on_checkBoxClone_stateChanged(int);
    void on_checkBoxPromptARM_stateChanged(int);
    void on_radioButtonStartKit_toggled(bool);
    void on_radioButtonNewKit_toggled(bool);
    void on_treeWidget_itemSelectionChanged();
    void on_toolButton_clicked();
    void on_pushButtonNew_clicked();
};

#endif // MKWPAGEVERSIONS_H
