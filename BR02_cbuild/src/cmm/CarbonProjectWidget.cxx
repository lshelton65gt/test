//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtShellTok.h"

#include <QtGui>
#include <QMenuBar>
#include <QToolBar>
#include <QRegExp>
#include <QDir>
#include "gui/CQt.h"

#include "util/UtString.h"
#include "util/UtIStream.h"
#include "util/OSWrapper.h"
#include "CarbonMakerContext.h"
#include "CarbonProject.h"
#include "CarbonProjectWidget.h"
#include "CarbonProjectNodes.h"
#include "SettingsEditor.h"
#include "PropertyEditor.h"
#include "DlgCompilerProperties.h"
#include "DlgConfigurationManager.h"
#include "DlgImportCcfg.h"
#include "DlgInconsitency.h"
#include "CarbonSourceGroups.h"
#include "CarbonHDLSourceFile.h"
#include "RemoteConsole.h"
#include "CarbonConsole.h"
#include "Mdi.h"
#include "MdiProject.h"
#include "QTextEditor.h"
#include "HierarchyDockWindow.h"
#include "CowareComponent.h"
#include "MaxsimComponent.h"
#include "SystemCComponent.h"
#include "PackageComponent.h"
#include "MVComponent.h"
#include "DirectivesEditor.h"
#include "PackageToolWizardWidget.h"
#include "CarbonDirectives.h"
#include "WorkspaceModelMaker.h"
#include "ErrorInfoWidget.h"
#include "InfoTableWidget.h"
#include "CarbonFiles.h"
#include "ProjectWidget.h"
#include "ModelStudioCommon.h"
#include "DlgConfiguration.h"
#include "MemoryWizard.h"
#include "RegEditorTreeNodes.h"
#include "cmm.h"

CarbonMappedDrives::CarbonMappedDrives()
{
  QSettings settings("ModelStudio", "Network Drives");
  QStringList mappings = settings.value("driveMappings").toStringList();
  foreach (QString mapping, mappings)
  {
    QString unixPath;
    QString driveLetter = mapping.mid(0, 2); // Drive letter Z:
    if (mapping[2] == '=') // Skip the Equals
    {
      unixPath = mapping.mid(3);
      UtString path;
      UtString letter;
      path << unixPath;
      letter << driveLetter;
      addMapping(letter.c_str(), path.c_str());
    }
  }
}

void CarbonMappedDrives::saveMappings()
{
  QStringList mappedDrives;
  QSettings settings("ModelStudio", "Network Drives");

  foreach (CarbonMappedDrive* drive, mMappedDrives)
  {
    QString value = QString(drive->mDriveLetter.c_str()) + '=' + QString(drive->mUnixPath.c_str());
    mappedDrives.push_back(value);
  }

  settings.setValue("driveMappings", mappedDrives);
}

void CarbonProjectWidget::closeEvent(QCloseEvent* ev)
{
  saveProject();
  ev->accept();
}
CarbonProjectWidget::CarbonProjectWidget(QWidget* parent, CarbonMakerContext* ctx) : CDragDropTreeWidget(true, parent)
{
  mLoadingProject = false;
  mModified = false;
  mCompilationInProgress = false;
  mDockWidget = NULL;
  mContext = ctx;
  mContextMenuNode = NULL;
  mConsole = NULL;
  mDragItem = NULL;
  setAttribute(Qt::WA_DeleteOnClose);
  mProject = NULL;
  mHierarchyWindow = NULL;
  mDirectivesEditor = NULL;
  mHierarchyDockWidget = NULL;
  mDirectives = NULL;
  mGenFolder = NULL;
  mDbFolder = NULL;
  mFilesFolder = NULL;
  if (mContext == NULL)
    mContext = ((ProjectWidget*)parent)->getContext();

  QStringList headers;
  headers << "Carbonize";

  setColumnCount(headers.count());
  setHeaderLabels(headers);
  mRoot = NULL;
  mHDLSources = NULL;
  mVHM = NULL;
  mRTL = NULL;

  setContextMenuPolicy(Qt::CustomContextMenu);
  setRootIsDecorated(true);

  setDragDropMode(QAbstractItemView::DragDrop);
  setAcceptDrops(true);

  putDragData("Carbon Tree");

  createActions();

  connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showContextMenu(QPoint)));
  connect(this, SIGNAL(itemCollapsed(QTreeWidgetItem*)), this, SLOT(itemCollapsed(QTreeWidgetItem*)));
  connect(this, SIGNAL(itemExpanded(QTreeWidgetItem*)), this, SLOT(itemExpanded(QTreeWidgetItem*)));
  connect(this, SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), this, SLOT(itemDoubleClicked(QTreeWidgetItem*,int)));
  connect(this, SIGNAL(itemSelectionChanged()), this, SLOT(itemSelectionChanged()));

  header()->setVisible(false);

  setFocusPolicy(Qt::WheelFocus);
}

QMenu* CarbonProjectWidget::getContextMenuRoot()
{
  QMenu* menu = new QMenu(this);

  updateContextMenuActions();

  menu->addAction(mActionCreateCoware);
  menu->addAction(mActionCreateMaxsim);
  menu->addAction(mActionCreateSystemC);
  menu->addAction(mActionCreateMV);

  bool isHDLProject = true;
  if (mProject && mProject->getProjectType() == CarbonProject::DatabaseOnly)
    isHDLProject = false;

  if (isHDLProject)
  {
    menu->addAction(mActionAddSources);
    menu->addAction(mActionAddLib);
  }
 
  menu->addAction(mActionProperties);

  menu->setEnabled(!mCompilationInProgress);

  return menu;
}

void CarbonProjectWidget::actionModelWizard()
{
  MemoryWizard* memwiz = new MemoryWizard(theApp);
  memwiz->show();
}

void CarbonProjectWidget::actionBuildPrecompiledHierarchy()
{
  if (mProject)
  {
    mProject->generatePrecompiledHierarchy();
  }
}


void CarbonProjectWidget::deleteSource()
{
  // We only allow single selection
  foreach (QTreeWidgetItem* item, selectedItems())
  {
    CarbonHDLFolderItem* folder = dynamic_cast<CarbonHDLFolderItem*>(item);
    if (folder)
      folder->deleteFolder();
    else
    { 
      CarbonSourceItem* file = dynamic_cast<CarbonSourceItem*>(item);
      if (file)
        file->deleteSource();
    }
    break; // single selection only allowed
  }
}

void CarbonProjectWidget::moveSourceItem(CarbonHDLFolderItem* folder, CarbonSourceItem* srcItem)
{
  // Move it in the tree
  QTreeWidgetItem* oldParent = ((QTreeWidgetItem*)srcItem)->parent();
  int childIndex = oldParent->indexOfChild(srcItem);
  QTreeWidgetItem* movedItem = oldParent->takeChild(childIndex);
  folder->addChild(movedItem);

  // remove it from the first group
  srcItem->getSource()->getGroup()->remove(srcItem->getSource(), false);

  // add it to the new group
  folder->getGroup()->addSource(srcItem->getSource());
}

// Move source item up in compile order
void CarbonProjectWidget::moveUp()
{
  // We only allow single selection
  foreach (QTreeWidgetItem* item, selectedItems())
  {
    bool expand = item->isExpanded();
    QTreeWidgetItem* parent = item->parent();
    INFO_ASSERT(parent, "Null parent unexpected");
    int childIndex = parent->indexOfChild(item);

    QTreeWidgetItem* movedItem = parent->takeChild(childIndex);
    parent->insertChild(childIndex-1, movedItem);
    setCurrentItem(movedItem);

    CarbonHDLFolderItem* folder = dynamic_cast<CarbonHDLFolderItem*>(item);
    if (folder)
    {
      folder->getGroup()->getGroups()->moveUp(folder->getGroup());
      folder->setExpanded(expand);
    }
    else
    { 
      CarbonSourceItem* file = dynamic_cast<CarbonSourceItem*>(item);
      if (file)
      {
        file->getSource()->getGroup()->moveUp(file->getSource());
        file->setExpanded(expand);
      }
    }
    break; // single selection only allowed
  }
}

void CarbonProjectWidget::moveDown()
{
  // We only allow single selection
  foreach (QTreeWidgetItem* item, selectedItems())
  {
    bool expand = item->isExpanded();

    QTreeWidgetItem* parent = item->parent();
    INFO_ASSERT(parent, "Null parent unexpected");
    int childIndex = parent->indexOfChild(item);

    QTreeWidgetItem* movedItem = parent->takeChild(childIndex);
    parent->insertChild(childIndex+1, movedItem);
    setCurrentItem(movedItem);

    CarbonHDLFolderItem* folder = dynamic_cast<CarbonHDLFolderItem*>(item);
    if (folder)
    {
      folder->getGroup()->getGroups()->moveDown(folder->getGroup());
      folder->setExpanded(expand);
    }
    else
    {
      CarbonSourceItem* file = dynamic_cast<CarbonSourceItem*>(item);
      if (file)
      {
        file->getSource()->getGroup()->moveDown(file->getSource());
        file->setExpanded(expand);
      }
    }
    break; // single selection only allowed
  }
}


// Deleting a tree widget item is stupid in Qt. you tell the parent to delete
// the child at this index
void CarbonProjectWidget::removeSource(QTreeWidgetItem* item, CarbonHDLSourceFile* src)
{
  QTreeWidgetItem* parent = item->parent();
  QModelIndex index = indexFromItem(item);
  parent->takeChild(index.row());
  src->getGroup()->remove(src);
}

void CarbonProjectWidget::itemDoubleClicked(QTreeWidgetItem* item, int column)
{
  if (!item->isDisabled())
  {
    CarbonProjectTreeNode* node = dynamic_cast<CarbonProjectTreeNode*>(item);
    if (node != NULL)
      node->doubleClicked(column);
  }
}

void CarbonProjectWidget::itemSelectionChanged()
{
  foreach (QTreeWidgetItem* item, selectedItems())
  {
    qDebug() << "selected" << item->text(0);
    CarbonProjectTreeNode* node = dynamic_cast<CarbonProjectTreeNode*>(item);
    if (!item->isDisabled())
    {
      if (node)
        node->singleClicked(0);
    }
    break; // single selection only 
  }
}


void CarbonProjectWidget::itemClicked(QTreeWidgetItem* item, int column)
{
  CarbonProjectTreeNode* node = dynamic_cast<CarbonProjectTreeNode*>(item);
  if (node != NULL)
    node->singleClicked(column);
}
void CarbonProjectWidget::itemCollapsed(QTreeWidgetItem* item)
{
  CarbonProjectTreeNode* node = dynamic_cast<CarbonProjectTreeNode*>(item);
  if (node != NULL)
    node->setClosedIcon();

}

void CarbonProjectWidget::itemExpanded(QTreeWidgetItem* item)
{
  CarbonProjectTreeNode* node = static_cast<CarbonProjectTreeNode*>(item);
  if (node != NULL)
    node->setOpenIcon();
}

void CarbonProjectWidget::createActions()
{
  // test the license features here
  bool bCanCreateCoware = mContext->getCanCreateCoware();
  bool bCanCreateMaxsim = mContext->getCanCreateMaxsim();
  bool bCanCreateSystemC = mContext->getCanCreateSystemC();
  bool bCanCreateMV = mContext->getCanCreateMV();

  mActionCreateCoware = new QAction(this);
  mActionCreateCoware->setText("Create CoWare Component...");
  mActionCreateCoware->setObjectName(QString::fromUtf8("cpwCreateCowareComponent"));
  mActionCreateCoware->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/coware.png")));
  if (bCanCreateCoware)
    mActionCreateCoware->setStatusTip("Create CoWare component from Carbon Component");

  mActionCreateCoware->setEnabled(bCanCreateCoware);
  connect(mActionCreateCoware, SIGNAL(triggered()), this, SLOT(actionCreateCoware()));

  mActionCreateMaxsim = new QAction(this);
  mActionCreateMaxsim->setText("Create SOC Designer Component...");
  mActionCreateMaxsim->setIconText("SOC Designer");
  mActionCreateMaxsim->setObjectName(QString::fromUtf8("cpwCreateMaxsiComponent"));
  mActionCreateMaxsim->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/maxsim.png")));
  if (bCanCreateMaxsim)
    mActionCreateMaxsim->setStatusTip("Create SOC Designer component from Carbon Component");

  mActionCreateMaxsim->setEnabled(bCanCreateMaxsim);
  connect(mActionCreateMaxsim, SIGNAL(triggered()), this, SLOT(actionCreateMaxsim()));

  mActionCreateSystemC = new QAction(this);
  mActionCreateSystemC->setText("Create SystemC Component...");
  mActionCreateSystemC->setObjectName(QString::fromUtf8("cpwCreateSystemCComponent"));
  mActionCreateSystemC->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/SystemC.png")));
  if (bCanCreateSystemC)
    mActionCreateSystemC->setStatusTip("Create SystemC component from Carbon Component");

  mActionCreateSystemC->setEnabled(bCanCreateSystemC);
  connect(mActionCreateSystemC, SIGNAL(triggered()), this, SLOT(actionCreateSystemC()));

  mActionCreateMV = new QAction(this);
  mActionCreateMV->setText("Create Model Validation Component...");
  mActionCreateMV->setObjectName(QString::fromUtf8("cpwCreateMVComponent"));
  mActionCreateMV->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/TaskHS.png")));
  if (bCanCreateMV)
    mActionCreateMV->setStatusTip("Create Model Validation component from Carbon Component");  

  mActionCreateMV->setEnabled(bCanCreateMV);
  connect(mActionCreateMV, SIGNAL(triggered()), this, SLOT(actionCreateMV()));

  mActionAddSources = new QAction(this);
  mActionAddSources->setText("Add RTL Source(s)...");
  mActionAddSources->setObjectName(QString::fromUtf8("cpwAddSources"));
  mActionAddSources->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/project.png")));
  mActionAddSources->setStatusTip("Add Verilog and/or VHDL sources to be compiled");
  connect(mActionAddSources, SIGNAL(triggered()), this, SLOT(actionAddSources()));

  mActionAddLib = new QAction(this);
  mActionAddLib->setText("Add RTL Library...");
  mActionAddLib->setObjectName(QString::fromUtf8("cpwAddLibrary"));
  mActionAddLib->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/NewFolderHS.png")));
  mActionAddLib->setStatusTip("Create an RTL library, then add RTL sources to it");
  connect(mActionAddLib, SIGNAL(triggered()), this, SLOT(actionAddLibrary()));

  mActionProperties = new QAction(this);
  mActionProperties->setText("Compiler Settings...");
  mActionProperties->setObjectName(QString::fromUtf8("cpwProperties"));
  mActionProperties->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/properties.png")));
  mActionProperties->setStatusTip("View and/or modify the Compiler options");
  connect(mActionProperties, SIGNAL(triggered()), this, SLOT(actionProperties()));
}

void CarbonProjectWidget::putModified(bool value)
{
  mModified = value;
  if (!mLoadingProject)
    emit projectModified(value);
}

bool CarbonProjectWidget::saveProject()
{
  bool result = false;

  // Commit any pendings edits
  if (mContext->getSettingsEditor())
    mContext->getSettingsEditor()->commitEdit();

  if (mProject != NULL)
  {
    UtString msg;
    msg << "Saving Project " << mProject->projectFile();
    mContext->getMainWindow()->statusBar()->showMessage(msg.c_str(), 2000);  
    result = mProject->save();
  }


  return result;
}

CarbonProject::HDLType CarbonProjectWidget::hdlFileType(const char* file)
{
  CarbonProject::HDLType hdlType = CarbonProject::Verilog;
  QRegExp rx("*.vhd*");
  rx.setPatternSyntax(QRegExp::Wildcard);
  if (rx.exactMatch(file))
    hdlType = CarbonProject::VHDL;
  QRegExp rx1("*.c*");
  rx1.setPatternSyntax(QRegExp::Wildcard);
  if (rx1.exactMatch(file))
    hdlType = CarbonProject::CPP;
  QRegExp rx2("*.h");
  rx2.setPatternSyntax(QRegExp::Wildcard);
  if (rx2.exactMatch(file))
    hdlType = CarbonProject::Header;

  return hdlType;
}

void CarbonProjectWidget::fileProperties(CarbonSourceItem* srcItem)
{
  const char* configName = mProject->getActiveConfiguration();

  CarbonHDLSourceFile* src = srcItem->getSource();
  CarbonHDLSourceConfiguration* config = src->getConfiguration(configName);

  QStringList items;
  QVariantList itemValues;
  config->getOptions()->setItems("File Properties", items, itemValues);

  mContext->getSettingsEditor()->setSettings(config->getOptions());
}

void CarbonProjectWidget::folderProperties(CarbonHDLFolderItem* folderItem)
{
  mContext->getSettingsEditor()->setSettings(folderItem->getOptions());
}

void CarbonProjectWidget::actionFileProperties()
{
}

void CarbonProjectWidget::actionBatchBuild(const QStringList& configList, BatchBuildType type)
{
  if (mProject == NULL)
    return;

  switch (type)
  {
  case eBatchBuild:
    mProject->batch(this, configList, CarbonProject::BatchBuild);
    break;
  case eBatchRebuild:
    mProject->batch(this, configList, CarbonProject::BatchRebuild);
    break;
  case eBatchClean:
    mProject->batch(this, configList, CarbonProject::BatchClean);
    break;
  }
}

void CarbonProjectWidget::actionProperties()
{
  DlgCompilerProperties dlg(this);
  dlg.putContext(mContext);
  dlg.exec();
  updatePropertiesDisplay();
}

void CarbonProjectWidget::compilerProperties()
{
  if (mProject->getProjectType() == CarbonProject::HDL)
  {
    QStringList items;
    QVariantList itemValues;
    mProject->getToolOptions("VSPCompiler")->setItems("Compiler Properties", items, itemValues);
    mContext->getSettingsEditor()->setSettings(mProject->getToolOptions("VSPCompiler"));
  }
}

DirectivesEditor* CarbonProjectWidget::compilerDirectives()
{
  UtString filePath;
  OSConstructFilePath(&filePath, mProject->getProjectDirectory(), "Directives");
  MDIWidget* widget = openSource(filePath.c_str(), -1, "Directives");
  bool signalOpened = false;
  if (mDirectivesEditor == NULL)
    signalOpened = true;

  mDirectivesEditor = dynamic_cast<DirectivesEditor*>(widget);

  INFO_ASSERT(widget, "Expecting Directives Editor");
  return mDirectivesEditor;
}

void CarbonProjectWidget::actionProjectProperties()
{
  QStringList items;
  QVariantList itemValues;
  mProject->getProjectOptions()->setItems("Project Properties", items, itemValues);

  mContext->getSettingsEditor()->setSettings(mProject->getProjectOptions());
}

CarbonHDLFolderItem* CarbonProjectWidget::findGroup(const char* groupName)
{
  for (int i=0; i<mVHM->childCount(); ++i)
  {
    QTreeWidgetItem* item = mVHM->child(i);
    CarbonHDLFolderItem* folderItem = dynamic_cast<CarbonHDLFolderItem*>(item);
    if (folderItem && 0 == strcmp(folderItem->getGroup()->getName(), groupName))
      return folderItem;
  }

  // Try again from CarbonRTLFolder
  for (int i=0; i<mVHM->childCount(); ++i)
  {
    QTreeWidgetItem* item = mVHM->child(i);
    CarbonRTLFolder* folderItem = dynamic_cast<CarbonRTLFolder*>(item);
    if (folderItem)
    {
      for (int j=0; j<folderItem->childCount(); ++j)
      {
        QTreeWidgetItem* childItem = folderItem->child(j);
        CarbonHDLFolderItem* folderItem = dynamic_cast<CarbonHDLFolderItem*>(childItem);
        if (folderItem && 0 == strcmp(folderItem->getGroup()->getName(), groupName))
          return folderItem;
      }
    }
  }

  return NULL;
}

void CarbonProjectWidget::actionAddLibrary()
{
  CarbonSourceGroup* group = new CarbonSourceGroup(mProject->getGroups());
  group->putVhdlLibrary("");
  group->putVerilogLibrary("");
  group->putName("New Library");
  mProject->getGroups()->addGroup(group);

  CarbonHDLFolderItem* folder = new CarbonHDLFolderItem(this, mRTL, group);
  folder->setText(0, group->getName());
  folder->setExpanded(true);
  setCurrentItem(folder);
  scrollToItem(folder);
  folderProperties(folder);
}

CarbonHDLFolderItem* CarbonProjectWidget::findHDLFolderItem(CarbonSourceGroup* group)
{
  CarbonHDLFolderItem* item = NULL;

  if (mRTL)
  {
    for (int i=0; i<mRTL->childCount(); i++)
    {
      QTreeWidgetItem* child = mRTL->child(i);
      CarbonHDLFolderItem* folderItem = dynamic_cast<CarbonHDLFolderItem*>(child);
      if (folderItem && 0 == strcmp(folderItem->getGroup()->getName(), group->getName()))
      {
        item = folderItem;
        break;
      }
    }
  }

  return item;
}

void CarbonProjectWidget::createComponent(ComponentType compType, const char* ccfgFile, bool checkDB)
{
  CarbonComponents* comps = mProject->getComponents();

  switch (compType)
  {
  case eComponentCoWare:
    {
      CowareComponent* comp = new CowareComponent(mProject);
      if (ccfgFile)
        comp->importCcfg(ccfgFile, checkDB);
      comps->addComponent(comp);
    }
    break;
  case eComponentMaxsim:
    {
      MaxsimComponent* comp = new MaxsimComponent(mProject);
      if (ccfgFile)
        comp->importCcfg(ccfgFile, checkDB);
      comps->addComponent(comp);
    }
    break;
  case eComponentSystemC:
    {
      SystemCComponent* comp = new SystemCComponent(mProject);
      comps->addComponent(comp);
    }
    break;
  default:
    break;
  }
}

QString CarbonProjectWidget::getDbFilename()
{
  QString symdbName = mProject->getDesignFilePath(".symtab.db");
  QString iodbName = mProject->getDesignFilePath(".io.db");

  QFileInfo fi1(symdbName);
  QFileInfo fi2(iodbName);

  if (fi1.exists())
    return symdbName;
  else if (fi2.exists())
    return iodbName;
  else
    return symdbName;
}

void CarbonProjectWidget::actionCreateCoware()
{
  clearSelection();

  CowareComponent* comp = getActiveCowareComponent();
  if (comp)
  {
    QString wizardFile = comp->getWizardFilename();
    QFileInfo fi(wizardFile);
    if (fi.exists())
    {
      openSourceFile(wizardFile, -1, COWARE_COMPONENT_NAME);
      comp->getRootItem()->setSelected(true);
      comp->getRootItem()->singleClicked(0);
    }
    else
      QMessageBox::warning(this, MODELSTUDIO_TITLE, "You must compile the Model before creating this component");
  }
  else
  {
    QString symdbName = getDbFilename();
   
    QFileInfo fi(symdbName);
    if (fi.exists())
    {
      comp = new CowareComponent(mProject);
      mProject->addComponent(comp);

	  UtString fname;
	  fname << symdbName;
      openSource(fname.c_str(), -1, COWARE_COMPONENT_NAME);

      comp->getRootItem()->setSelected(true);
      comp->getRootItem()->singleClicked(0);
    }
    else
      QMessageBox::warning(this, MODELSTUDIO_TITLE, "You must compile the Model before creating this component");
    }

  updateContextMenuActions();
}

void CarbonProjectWidget::actionImportCommandFile()
{
  mProject->actionImportCommandFile();
}

void CarbonProjectWidget::importCowareCcfg(QString filename)
{
  CowareComponent* comp = getActiveCowareComponent();
  if (comp == NULL)
  {
    comp = new CowareComponent(mProject);
    mProject->addComponent(comp);
  }

  if (comp->importCcfg(filename))
    reopenWizard(comp, COWARE_COMPONENT_NAME, comp->getWizardFilename());
}

void CarbonProjectWidget::importMaxsimCcfg(QString filename)
{
  MaxsimComponent* comp = getActiveMaxsimComponent();
  if (comp == NULL)
  {
    comp = new MaxsimComponent(mProject);
    mProject->addComponent(comp);
  }

  if (comp->importCcfg(filename))
    reopenWizard(comp, MAXSIM_COMPONENT_NAME, comp->getWizardFilename());
}

void CarbonProjectWidget::actionImportCcfgFile()
{
  DlgImportCcfg dlg(this, this);
  if (dlg.exec() == QDialog::Accepted)
  {
    qDebug() << "Importing: " << dlg.getFilename() << " type: " << dlg.getType();
    UtString msg;

    QString compName;
    bool askProceed = false;

    if (dlg.getType() == DlgImportCcfg::eCcfgCoWare && getActiveCowareComponent())
    {
      askProceed = true;
      compName = "CoWare";
    }
    else if (dlg.getType() == DlgImportCcfg::eCcfgArm && getActiveMaxsimComponent())
    {
      askProceed = true;
      compName = "SocDesigner";
    }

    if (askProceed)
    {
      UtString msg;
      msg << "This project already contains an " << compName << " Component.\n";
      msg << "Importing this Wizard file will overwrite the current component and cannot be undone.\n";
      msg << "Do you want to proceed?";

      if (QMessageBox::question(this, MODELSTUDIO_TITLE, msg.c_str(), 
            QMessageBox::No|QMessageBox::Yes, QMessageBox::No) == QMessageBox::No)
        return;
    }

    switch (dlg.getType())
    {
    case DlgImportCcfg::eCcfgCoWare:
      importCowareCcfg(dlg.getFilename());
      break;
    case DlgImportCcfg::eCcfgArm:
      importMaxsimCcfg(dlg.getFilename());  
      break;
    }
  }
}

void CarbonProjectWidget::actionCreateMaxsim()
{
  clearSelection();

  MaxsimComponent* comp = getActiveMaxsimComponent();
  if (comp)
  {
    QString wizardFile = comp->getWizardFilename();
    QFileInfo fi(wizardFile);
    if (fi.exists())
    {
      openSourceFile(wizardFile, -1, MAXSIM_COMPONENT_NAME);
      comp->getRootItem()->setSelected(true);
      comp->getRootItem()->singleClicked(0);
    }
    else
      QMessageBox::warning(this, MODELSTUDIO_TITLE, "You must compile the Model before creating this component");
  }
  else
  {
    QString symdbName = getDbFilename();

    QFileInfo fi(symdbName);
    if (fi.exists())
    {
      comp = new MaxsimComponent(mProject);
      mProject->addComponent(comp);

      UtString fname;
      fname << symdbName;
      openSource(fname.c_str(), -1, MAXSIM_COMPONENT_NAME);

      comp->getRootItem()->setSelected(true);
      comp->getRootItem()->singleClicked(0);
    }
    else
      QMessageBox::warning(this, MODELSTUDIO_TITLE, "You must compile the Model before creating this component");
  }
  updateContextMenuActions();
}


void CarbonProjectWidget::actionCreateMV()
{
  clearSelection();

  MVComponent* comp = getActiveMVComponent();
  if (comp)
  {
    QString wizardFile = comp->getWizardFilename();
    QFileInfo fi(wizardFile);
    if (fi.exists())
    {
      openSourceFile(wizardFile, -1, "MV");
      comp->getRootItem()->setSelected(true);
      comp->getRootItem()->singleClicked(0);
    }
    else
      QMessageBox::warning(this, MODELSTUDIO_TITLE, "You must compile the Model before creating this component");
  }
  else
  {
    QString symdbName = getDbFilename();

    QFileInfo fi(symdbName);
    if (fi.exists())
    {
      comp = new MVComponent(mProject);
      mProject->addComponent(comp);
      openSource(comp->getDocumentPath(), -1, "MV");

      comp->getRootItem()->setSelected(true);
      comp->getRootItem()->singleClicked(0);
    }
    else
      QMessageBox::warning(this, MODELSTUDIO_TITLE, "You must compile the Model before creating this component");
  }
  updateContextMenuActions();
}

CarbonComponent* CarbonProjectWidget::createPackageComp()
{
  CarbonComponent* comp = mProject->getComponents()->findComponent(PACKAGE_COMPONENT_NAME);
  if (comp == NULL) {
    PackageComponent* packageComp = new PackageComponent(mProject);
    mProject->addComponent(packageComp);
    packageComp->setDefaultValues();
    comp = packageComp;
  }    
  // Add the Package component to the component tree;
  mProject->getComponents()->populateComponentTree(this, NULL);

  return comp;
}

PackageToolWizardWidget* CarbonProjectWidget::openPackageOptions()
{
  CarbonComponent* comp = createPackageComp();

  // Open Package Options
  MDIWidget* widget = openSource(NULL, -1, PACKAGE_COMPONENT_NAME);

  PackageToolWizardWidget* packageWidget = dynamic_cast<PackageToolWizardWidget*>(widget);
  INFO_ASSERT(packageWidget, "Expecting Package Options Editor");

  // Let the widget know about the component.
  if (packageWidget) {
    packageWidget->setComp(dynamic_cast<PackageComponent*>(comp));

    // Is this good?
    packageWidget->populateTree();
  }

  return packageWidget;
}

void CarbonProjectWidget::actionPackageOptions()
{
  openPackageOptions();
}

void CarbonProjectWidget::actionCreateSystemC()
{
  clearSelection();

  SystemCComponent* comp = getActiveSystemCComponent();
  if (comp)
  {
    QString wizardFile = comp->getWizardFilename();
    QFileInfo fi(wizardFile);
    if (fi.exists())
    {
      openSourceFile(wizardFile, -1, "SystemC");
      comp->getRootItem()->setSelected(true);
      comp->getRootItem()->singleClicked(0);
    }
    else
      QMessageBox::warning(this, MODELSTUDIO_TITLE, "You must compile the Model before creating this component");
  }
  else
  {
    QString symdbName = getDbFilename();

    QFileInfo fi(symdbName);
    if (fi.exists())
    {
      comp = new SystemCComponent(mProject);
      mProject->addComponent(comp);

      UtString fname;
      fname << symdbName;

      openSource(fname.c_str(), -1, "SystemC");

      comp->getRootItem()->setSelected(true);
      comp->getRootItem()->singleClicked(0);
    }
    else
      QMessageBox::warning(this, MODELSTUDIO_TITLE, "You must compile the Model before creating this component");
  }
  updateContextMenuActions();
}

CowareComponent* CarbonProjectWidget::getActiveCowareComponent()
{
  CarbonComponents* components = mProject->getComponents();

  QList<CarbonComponent*> comps;
  components->getActiveComponents(&comps);

  foreach (CarbonComponent* comp, comps)
  {
    CowareComponent* coware = dynamic_cast<CowareComponent*>(comp);
    if (coware)
      return coware;
  }
  return NULL;
}


MVComponent* CarbonProjectWidget::getActiveMVComponent()
{
  CarbonComponents* components = mProject->getComponents();

  QList<CarbonComponent*> comps;
  components->getActiveComponents(&comps);

  foreach (CarbonComponent* comp, comps)
  {
    MVComponent* mv = dynamic_cast<MVComponent*>(comp);
    if (mv)
      return mv;
  }
  return NULL;
}

SystemCComponent* CarbonProjectWidget::getActiveSystemCComponent()
{
  CarbonComponents* components = mProject->getComponents();

  QList<CarbonComponent*> comps;
  components->getActiveComponents(&comps);

  foreach (CarbonComponent* comp, comps)
  {
    SystemCComponent* systemc = dynamic_cast<SystemCComponent*>(comp);
    if (systemc)
      return systemc;
  }
  return NULL;
}

MaxsimComponent* CarbonProjectWidget::getActiveMaxsimComponent()
{
  CarbonComponents* components = mProject->getComponents();

  QList<CarbonComponent*> comps;
  components->getActiveComponents(&comps);

  foreach (CarbonComponent* comp, comps)
  {
    MaxsimComponent* maxsim = dynamic_cast<MaxsimComponent*>(comp);
    if (maxsim)
      return maxsim;
  }
  return NULL;
}

CarbonSourceItem* CarbonProjectWidget::findSource(CarbonHDLSourceFile* src)
{
  // We only allow single selection
  foreach (QTreeWidgetItem* item, selectedItems())
  {
    CarbonSourceItem* file = dynamic_cast<CarbonSourceItem*>(item);
    if (file)
    {
      if (file->getSource() == src)
        return file;
    }
  }

  return NULL;
}

CarbonProjectTreeNode* CarbonProjectWidget::addRTLSource(const QString& folderName, const QString& file)
{
  UtString foldername;
  foldername << folderName;

  // Check for the folder? if it doesn't exist, create it.
  CarbonHDLFolderItem* groupFolder = findGroup(foldername.c_str());
  CarbonSourceGroup* group = NULL;
  if (groupFolder == NULL) // No group yet?
  {
    group = new CarbonSourceGroup(mProject->getGroups());
    group->putVhdlLibrary("");
    group->putVerilogLibrary("");
    group->putName(foldername.c_str());
    mProject->getGroups()->addGroup(group);
    groupFolder = new CarbonHDLFolderItem(this, mRTL, group);
    groupFolder->setText(0, group->getName());
    groupFolder->setExpanded(true);
    setCurrentItem(groupFolder);
    scrollToItem(groupFolder);
  }
  else
    group = groupFolder->getGroup();

  // All paths relative to the project directory.
  TempChangeDirectory td(mProject->getProjectDirectory());

  UtString envFile;
  envFile << theApp->normalizePath(file);

  QString relFile = makeRelativePath(mProject->getProjectDirectory(), envFile.c_str());

  UtString f;
  f << relFile;

  UtString fDir;
  UtString fName;
  OSParseFileName(f.c_str(), &fDir, &fName);

  UtString actFile;
  actFile << file;

  CarbonProject::HDLType hdlType = CarbonProjectWidget::hdlFileType(actFile.c_str());

  switch (hdlType)
  {
  case CarbonProject::VHDL:
    {
      CarbonVHDLSource* src = new CarbonVHDLSource(group, f.c_str());
      if (group->addSource(src))
      {
        CarbonVhdlItem* item = new CarbonVhdlItem(this, src, groupFolder);
        item->setData(fName.c_str(), f.c_str());
        item->setText(0, fName.c_str());
        return item;
      }
      break;
    }
  case CarbonProject::Verilog:
    {
      CarbonVerilogSource* src = new CarbonVerilogSource(group, f.c_str());
      if (group->addSource(src))
      {
        CarbonVerilogItem* item = new CarbonVerilogItem(this, src, groupFolder);
        item->setData(fName.c_str(), f.c_str());
        item->setText(0, fName.c_str());
        return item;
      }
      break;
    }
  case CarbonProject::CPP:
    {
      CarbonCPPSource* src = new CarbonCPPSource(group, f.c_str());
      if (group->addSource(src))
      {
        CarbonCPPItem* item = new CarbonCPPItem(this, src, groupFolder);
        item->setData(fName.c_str(), f.c_str());
        item->setText(0, fName.c_str());
        return item;
      }
      break;
    }
  case CarbonProject::Header:
    {
      CarbonHSource* src = new CarbonHSource(group, f.c_str());
      if (group->addSource(src))
      {
        CarbonHItem* item = new CarbonHItem(this, src, groupFolder);
        item->setData(fName.c_str(), f.c_str());
        item->setText(0, fName.c_str());
        return item;
      }
      break;
    }
  }
  return NULL;
}

void CarbonProjectWidget::actionAddSources()
{
  qDebug() << "actionAddSources";

  int numGroups = mProject->getGroups()->numGroups();

  if (numGroups > 0)
  {
    QStringList files = QFileDialog::getOpenFileNames(this, "Select RTL Source(s)",
      mProject->getProjectDirectory(),
      "RTL Sources (*.v *.vhd* *.cpp *.cxx *.h);;Verilog files (*.v);;VHDL files (*.vhd*);;C++ files (*.cpp *.cxx);;Header files (*.h)");

    if (files.count() > 0)
    {
      CarbonHDLFolderItem* parentItem=NULL;

      CarbonSourceGroup* group = NULL;
      if (mContextMenuNode)
      {
        // dynamic_cast is needed here, because we need to know
        // if the context menu was on a Folder item or not, if not
        // then we add the source to the first group
        parentItem = dynamic_cast<CarbonHDLFolderItem*>(mContextMenuNode);
        if (parentItem)
          group = parentItem->getGroup();     
      }

      if (group == NULL) // No group yet?
      {
        group = mProject->getGroups()->getGroup(0);
        parentItem = findGroup(group->getName());
      }

      // Ensure all paths are relative to the Project Directory
      TempChangeDirectory td(mProject->getProjectDirectory());

      foreach (QString afile, files)
      {
        UtString envFile;
        envFile << theApp->normalizePath(afile);

        QString relFile = makeRelativePath(mProject->getProjectDirectory(), envFile.c_str());

        qDebug() << "normalize" << afile << "normalizedTo" << envFile.c_str() << "relFile" << relFile;

#if pfWINDOWS     
        QFileInfo winfi(relFile);
        if (!winfi.isRelative())
          relFile = mProject->getUnixEquivalentPath(afile);
#endif

        UtString f;
        f << theApp->normalizePath(relFile);

        UtString fDir;
        UtString fName;
        OSParseFileName(f.c_str(), &fDir, &fName);

        UtString actFile;
        actFile << afile;

        CarbonProject::HDLType hdlType = CarbonProjectWidget::hdlFileType(actFile.c_str());

        switch (hdlType)
        {
        case CarbonProject::VHDL:
          {
            CarbonVHDLSource* src = new CarbonVHDLSource(group, f.c_str());
            if (group->addSource(src))
            {
              CarbonVhdlItem* item = new CarbonVhdlItem(this, src, parentItem);
              item->setData(fName.c_str(), f.c_str());
              item->setText(0, fName.c_str());
            }
            break;
          }
        case CarbonProject::Verilog:
          {
            CarbonVerilogSource* src = new CarbonVerilogSource(group, f.c_str());
            if (group->addSource(src))
            {
              CarbonVerilogItem* item = new CarbonVerilogItem(this, src, parentItem);
              item->setData(fName.c_str(), f.c_str());
              item->setText(0, fName.c_str());
            }
            break;
          }
        case CarbonProject::CPP:
          {
            CarbonCPPSource* src = new CarbonCPPSource(group, f.c_str());
            if (group->addSource(src))
            {
              CarbonCPPItem* item = new CarbonCPPItem(this, src, parentItem);
              item->setData(fName.c_str(), f.c_str());
              item->setText(0, fName.c_str());
            }
            break;
          }
        case CarbonProject::Header:
          {
            CarbonHSource* src = new CarbonHSource(group, f.c_str());
            if (group->addSource(src))
            {
              CarbonHItem* item = new CarbonHItem(this, src, parentItem);
              item->setData(fName.c_str(), f.c_str());
              item->setText(0, fName.c_str());
            }
            break;
          }
        }
      }
      parentItem->setExpanded(true);
    }
  }
  else
  {
    UtString msg;
    msg << "You must have at least one RTL library before you can add sources";
    QMessageBox::warning(mContext->getMainWindow(), 
      MODELSTUDIO_TITLE, msg.c_str(), QMessageBox::Ok);
  }
}

void CarbonProjectWidget::showContextMenu(const QPoint &pos)
{
  QTreeWidgetItem* item = itemAt(pos);
  if (item)
  {
    QPoint point = viewport()->mapToGlobal(pos);
    CarbonProjectTreeNode* node = static_cast<CarbonProjectTreeNode*>(item);
    INFO_ASSERT(node != NULL, "Expecting node to derive from CarbonProjectTreeNode");
    
    updateContextMenuActions();

    setContextMenuNode(node);

    if (node)
      node->showContextMenu(point);

    setContextMenuNode(NULL);
  }
}

CarbonProjectWidget::~CarbonProjectWidget()
{
  if (mProject)
    delete mProject;

  mProject=NULL;
}

void CarbonProjectWidget::newProject()
{

}

// Check to see if we need to refresh the display
void CarbonProjectWidget::updatePropertiesDisplay()
{
  CarbonOptions* settings = mContext->getSettingsEditor()->getSettings();
  if (settings == mProject->getToolOptions("VSPCompiler"))
    compilerProperties();
}


QString CarbonProjectWidget::getCurrentSelection()
{
  QString result;
  QStringList parentList;

  QTreeWidgetItem* currItem = currentItem();
  if (currItem)
  {
    QString text = currItem->text(0);
    parentList.push_front(text);

    for ( QTreeWidgetItem* par = currItem->parent(); par != NULL; par = par->parent() )
    {
      text = par->text(0);    
      parentList.push_front(text);
    }

    UtString path;
    foreach (QString node, parentList)
    {
      if (path.length() > 0)
        path << "\r";
      path << node;
    }
    result = path.c_str();
  }

  return result;
}

QTreeWidgetItem* CarbonProjectWidget::findChild(QTreeWidgetItem* parent, const QString& childName)
{
  QTreeWidgetItem* retItem = NULL;

  if (parent == NULL)
  {
    foreach (QTreeWidgetItem* item, findItems(childName, Qt::MatchExactly))
    {
      retItem = item;
      break;
    }
  }
  else
  {
    for (int i=0; i<parent->childCount(); i++)
    {
      QTreeWidgetItem* item = parent->child(i);
      if (item->text(0) == childName)
      {
        retItem = item;
        break;
      }
    }
  }

  return retItem;
}

void CarbonProjectWidget::setCurrentSelection(const QString & value)
{
  QTreeWidgetItem* item = NULL;
  QStringList nodes = value.split('\r');
  foreach (QString node, nodes)
  {
    item = findChild(item, node);
  }
  if (item)
    itemClicked(item, 0);
  else
    compilerProperties();
}


void CarbonProjectWidget::putConfiguration(const char* name, bool retainContext)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);
  QString path = getCurrentSelection();
  project()->putActiveConfiguration(name);
  updatePropertiesDisplay();
  addGeneratedFiles(mGenFolder);
  if (retainContext)
    setCurrentSelection(path);
  QApplication::restoreOverrideCursor();
 }

void CarbonProjectWidget::putPlatform(const char* name)
{
  qDebug() << name;
}

void CarbonProjectWidget::reopenWizards()
{
  if (getActiveSystemCComponent())
    reopenWizard(getActiveSystemCComponent(), "SystemC", getActiveSystemCComponent()->getWizardFilename());

  if (getActiveCowareComponent())
    reopenWizard(getActiveCowareComponent(), COWARE_COMPONENT_NAME, getActiveCowareComponent()->getWizardFilename());

  if (getActiveMaxsimComponent())
    reopenWizard(getActiveMaxsimComponent(), MAXSIM_COMPONENT_NAME, getActiveMaxsimComponent()->getWizardFilename());

  if (getActiveMVComponent())
    reopenWizard(getActiveMVComponent(), "MV", getActiveMVComponent()->getWizardFilename());
}

void CarbonProjectWidget::configurationChanged(const char* configName)
{
  CarbonOptions* options =  mProject->getConfigurations()->findConfiguration(configName)->getOptions("VSPCompiler");

  options->registerPropertyChanged("-o", "outputFilenameChanged", this);
  options->registerPropertyChanged("-directive", "directiveFilesChanged", this);

  reopenWizards();
}

void CarbonProjectWidget::makefileCompleted(CarbonConsole::CommandMode , CarbonConsole::CommandType)
{
  qDebug() << "Compilation ended....";
  
  addGeneratedFiles(mGenFolder);
  updateWidgets();
  
  MDIProjectTemplate* projTemplate = dynamic_cast<MDIProjectTemplate*>(mContext->getDocumentManager()->findTemplate(NULL, "Project"));
  if (projTemplate)
    projTemplate->compilationModeChanged(false);
  
  mCompilationInProgress = false;

  updateContextMenuActions();
  
  QApplication::restoreOverrideCursor();
  
  checkComponentIntegrities();

  if (theApp->isBatchMode())
    theApp->close();

}

void CarbonProjectWidget::restartCompile()
{
  compile(false, true, false);
}

void CarbonProjectWidget::closeWizards()
{
 
}


// After the makefile is finished check to see if we received
// any ccfg out of sync errors, if so, prompt user and re-kick
// the compile if they desire.
void CarbonProjectWidget::checkComponentIntegrities()
{
  QStringList errorList;
  bool errorsFound = false;
  CarbonComponents* comps = mProject->getComponents();
  for (UInt32 i=0; i<comps->numComponents(); i++)
  {
    CarbonComponent* comp = comps->getComponent(i);
    if (comp->checkForCcfgErrors(errorList))
    {
      errorsFound = true;
      break;
    }
  }

  if (errorsFound)
  {
    UtString errMsg;
    foreach (QString em, errorList)
      errMsg << em << "\n";

    DlgInconsitency dlg(this, errMsg.c_str());
    if (dlg.exec())
    {
      QDockWidget* dw = mContext->getWorkspaceModelMaker()->getErrorInfoDockWindow();
      ErrorInfoWidget* errInfo = dynamic_cast<ErrorInfoWidget*>(dw->widget());
      InfoTableWidget* infoWidget = errInfo->getInfoWidget();
      infoWidget->clearAll();

      for (UInt32 i=0; i<comps->numComponents(); i++)
      {
        CarbonComponent* comp = comps->getComponent(i);
        QString iodbName = mProject->getDatabaseFilePath();
        UtString dbname;
        dbname << iodbName;
        comp->updateCcfg(mProject->getActive(), dbname.c_str());
      }

      // Refresh the Wizards
      reopenWizards();

      QTimer* tempTimer = new QTimer(this);
      tempTimer->setSingleShot(true);
      tempTimer->setInterval(10);     // milliseconds
      connect(tempTimer, SIGNAL(timeout()), this, SLOT(restartCompile()));
      tempTimer->start();   
    }
    else
      qDebug() << "NO";
  }
}

// Closes & Opens the wizard for the component, but only if the wizard
// is already open, otherwise it does not open the Wizard.
void CarbonProjectWidget::reopenWizard(CarbonComponent* comp, const char* compDocumentType, const char* wizardFile)
{
  if (comp)
  {
    MDIDocumentTemplate* t = mContext->getDocumentManager()->findTemplate(NULL, compDocumentType);
    if (t)
    {
      foreach (QWidget* window, mContext->getWorkspace()->windowList()) 
      {
        MDIWidget* mdiWidget = dynamic_cast<MDIWidget*>(window);
        if (mdiWidget && mdiWidget->getDocumentTemplate() == t)
        {
          qDebug() << "Re-Opening " << compDocumentType << " Wizard file: " << wizardFile;
          if (mdiWidget->getWidget()->close())
            openSource(wizardFile, -1, compDocumentType);
          break;
        }
      }
    }
  }
}

bool CarbonProjectWidget::composeFilename(const char* filename, UtString* composedName)
{
  bool retValue = true;

  QFileInfo fi(filename);

  QString rawName(filename);
  if (rawName.startsWith("regPath:"))
  {
    composedName->clear();   
    *composedName << rawName.mid(strlen("regPath:"));
    return true;
  }

  if (mProject)
    mProject->setEnvironmentVariables();

  qDebug() << "input name: " << filename;
  if (mProject)
    qDebug() << "project directory: " << mProject->getProjectDirectory();

  QString inputFileName = filename;

  if (mProject && fi.isRelative() && !inputFileName.contains(ENVVAR_REFERENCE))
  {
    qDebug() << " is relative";
    OSConstructFilePath(composedName, mProject->getProjectDirectory(), filename);
    QFileInfo nfi(composedName->c_str());
    composedName->clear();   
    *composedName << nfi.absoluteFilePath();
  }
  else
  {
    qDebug() << " not relative";
    if (fi.path().startsWith("qrc:") || fi.path().startsWith("http:"))
    {
      *composedName << filename;
    }
    else
    {
      UtString expandedName;
      if (inputFileName.contains(ENVVAR_REFERENCE))
      {
        UtString errMsg;
        if (OSExpandFilename(&expandedName, filename, &errMsg))
          filename = expandedName.c_str();
        else
        {
          *composedName = "";
          UtString msg;
          msg << "Unable to open file: " << filename << "\n";
          msg << errMsg;
          QMessageBox::warning(NULL, MODELSTUDIO_TITLE, msg.c_str());
          retValue = false;
        }
      }
      QFileInfo fi2(filename);
      *composedName << fi2.canonicalFilePath();
      if (composedName->length() == 0 && mProject)
        *composedName << mProject->getWindowsEquivalentPath(filename);
      else if (composedName->length() == 0)
        *composedName << filename;
    }
  }
  return retValue;
}

void CarbonProjectWidget::modelCompilationCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  const char* iodbName = mProject->getDesignFilePath(".symtab.db");
  QFileInfo fi(iodbName);
  if (fi.exists())
    reopenWizards();
}

bool CarbonProjectWidget::loadProject(const char* fileName)
{
  mContext->getSettingsEditor()->setSettings(NULL);

  mLoadingProject = true;

  if (mProject)
    delete mProject;

  mProject = new CarbonProject(NULL, this);

  if (mProject->loadProject(fileName))
  {
    CQT_CONNECT(getConsole(), compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType));
    CQT_CONNECT(getConsole(), compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType));
    CQT_CONNECT(getConsole(), exploreFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, exploreFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType));

    CQT_CONNECT(mProject, configurationChanged(const char*), this, configurationChanged(const char*));
    CQT_CONNECT(mProject, modelCompilationCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, modelCompilationCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType));
    CQT_CONNECT(mProject, makefileCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType), this, makefileCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType));

    UtString errm;
    OSChdir(mProject->getProjectDirectory(), &errm);

    // populate the tree
    populate();

    mConsole->setProjectWidget(this);

    configurationChanged(mProject->getActiveConfiguration());

    updateWidgets();

    readCompilerOutput();

    emit projectLoaded(fileName, mProject);

    updateContextMenuActions();

    putModified(false);
    
    mLoadingProject = false;

    putConfiguration(mProject->getActiveConfiguration());

    mVHM->setSelected(true);

    QTimer::singleShot(200, this, SLOT(setWidgetFocus()));
   
    return true;
  }

  mLoadingProject = false;

  return false;
}


void CarbonProjectWidget::setWidgetFocus()
{
  setFocus();
}

void CarbonProjectWidget::updateContextMenuActions()
{
  // check licenseses here.
  bool bCanCreateCoware = mContext->getCanCreateCoware() && !mCompilationInProgress;
  bool bCanCreateMaxsim = mContext->getCanCreateMaxsim() && !mCompilationInProgress;
  bool bCanCreateSystemC = mContext->getCanCreateSystemC() && !mCompilationInProgress;
  bool bCanCreateMV = mContext->getCanCreateMV() && !mCompilationInProgress;

  mActionCreateCoware->setEnabled(bCanCreateCoware);
  mActionCreateMaxsim->setEnabled(bCanCreateMaxsim );
  mActionCreateSystemC->setEnabled(bCanCreateSystemC);
  mActionCreateMV->setEnabled(bCanCreateMV);

  bool isHDLProject = true;
  if (mProject && mProject->getProjectType() == CarbonProject::DatabaseOnly)
    isHDLProject = false;

  mActionAddSources->setEnabled(isHDLProject && !mCompilationInProgress);
  mActionAddLib->setEnabled(isHDLProject && !mCompilationInProgress);
  mActionProperties->setEnabled(isHDLProject && !mCompilationInProgress);

  // Emit signals indicating the status of components.

  emit canAddLibrary(isHDLProject && !mCompilationInProgress);
  emit canAddSources(isHDLProject && !mCompilationInProgress);

  emit canCreateCoWare(bCanCreateCoware);
  emit canCreateMaxsim(bCanCreateMaxsim);
  emit canCreateSystemC(bCanCreateSystemC );
  emit canCreateMV(bCanCreateMV);
}

void CarbonProjectWidget::readCompilerOutput()
{
  const char* errorFile = mProject->getDesignFilePath(".errors");
  if (errorFile && QFile::exists(errorFile))
    readOutputFile(errorFile);
 
  QDockWidget* dw = context()->getWorkspaceModelMaker()->getErrorInfoDockWindow();
  ErrorInfoWidget* errInfo = dynamic_cast<ErrorInfoWidget*>(dw->widget());
  if (errInfo)
  {
    InfoTableWidget* infoWidget = errInfo->getInfoWidget();
    infoWidget->autoFit();
  }
}

void CarbonProjectWidget::readOutputFile(const char* fileName)
{
  QFile file(fileName);
  if (file.open(QFile::ReadOnly | QFile::Text)) 
  {
    QTextStream in(&file);
    while (!in.atEnd()) 
    {
      QString line = in.readLine();
      UtString l;
      l << line;
      mConsole->processOutput(CarbonConsole::Local, CarbonConsole::Compilation, l.c_str());
    }
  }
}

void CarbonProjectWidget::outputFilenameChanged(const CarbonProperty*, const char*)
{
  updateVHMName();
}

void CarbonProjectWidget::directiveFilesChanged(const CarbonProperty*, const char* newValue)
{
  updateDirectiveNodes(mDirectives, newValue);
}

// Name
void CarbonProjectWidget::updateVHMName()
{
  UtString name = "Carbon Model: ";

  CarbonPropertyValue* outnameValue = mProject->getToolOptions("VSPCompiler")->getValue("-o");
  name << outnameValue->getValue();

  mVHM->setText(0, name.c_str());
}

void CarbonProjectWidget::closeProject()
{
  // Was there a project open?
  if (mProject)
  {
    UtString projDir = mProject->getProjectDirectory();
    QFileInfo fi(projDir.c_str());

    UtString dirAbove;
    dirAbove << fi.absolutePath();

    UtString msg;
    OSChdir(dirAbove.c_str(), &msg);
  }

  emit projectClosing(mProject);
  clear();
  delete mProject;
  mProject=NULL;
  mRTL=NULL;
  mVHM=NULL;
  mConsole->setProjectWidget(NULL);
}

void CarbonProjectWidget::updateDirectiveNodes(CarbonDirectivesFolder* parent, const char* values)
{
  if (values == NULL)
    values = mProject->getToolOptions("VSPCompiler")->getValue("-directive")->getValue();

  INFO_ASSERT(values, "Expecting -directives value");
  
  // first, wipe out any children, then re-build them
  QList<QTreeWidgetItem*> children = parent->takeChildren();
  foreach (QTreeWidgetItem* item, children)
    delete item;

  for (UtShellTok tok(values, false, " ;\t\r\n"); !tok.atEnd(); ++tok)
  {
    const char* val = *tok;
    QString file = val;

    if (file.length() == 0)
      continue;

    UtString shortFileName;
    shortFileName << file;
    UtString fileName;
    QFileInfo fi(file);
    
    if (fi.isRelative())
      OSConstructFilePath(&fileName, mProject->getProjectDirectory(), shortFileName.c_str());
    else
      fileName << file;

    UtString resolvedName;
    resolvedName << project()->getWindowsEquivalentPath(fileName.c_str());

    CarbonDirectivesItem* node = new CarbonDirectivesItem(this, resolvedName.c_str(), parent, shortFileName.c_str());
    INFO_ASSERT(node, "New item");

    qDebug() << fileName.c_str();
  }

  parent->setExpanded(true);
}

void CarbonProjectWidget::populateMemoryMaps()
{
  SpiritXML* spirit = mProject->getSpiritXML();
  spirit->populateTree(this, mMemoryMaps);
}

void CarbonProjectWidget::populate()
{
  // Start from scratch
  clear();
  bool isHDLProject = true;
  if (mProject && mProject->getProjectType() == CarbonProject::DatabaseOnly)
    isHDLProject = false;

  // Set the "Root" Node
  mRoot = new CarbonRootItem(this);
  mRoot->setText(0, mProject->shortName());

  mVHM = new CarbonModelItem(this, mRoot);
  mVHM->setText(0, "Carbon Model");
  mVHM->setExpanded(true);

  if (isHDLProject)
  {
    mDirectives = new CarbonDirectivesFolder(this, mVHM);
    mDirectives->setText(0, "Compiler Directives");
  }

  if (getenv("CARBON_IPXACT")) {
    SpiritXML* spiritXml = mProject->getSpiritXML();
    mMemoryMaps = new MemoryMapsItem(this, mVHM, spiritXml);
  }

  QString iodbName = mProject->getDatabaseFilePath();
  UtString dbname;
  dbname << iodbName;
  mDbFolder = new CarbonDatabaseFolder(this, mVHM, dbname.c_str());
  QFileInfo fi(iodbName);
  mDbFolder->setText(0, fi.fileName());

  populateMemoryMaps();

  if (isHDLProject)
  {
    mRTL = new CarbonRTLFolder(this, mVHM);
    mRTL->setText(0, "RTL Sources");

    addGroupsAndFiles(mRTL);
  }

  mFilesFolder = new CarbonFilesFolder(mProject->getFiles(), this, mRoot);
  addCarbonFiles(mFilesFolder);

  mGenFolder = new CarbonGenFilesFolder(this, mVHM);
  mGenFolder->setText(0, "Generated Files");

  addGeneratedFiles(mGenFolder);

  if (isHDLProject)
    updateDirectiveNodes(mDirectives, NULL);

  mRoot->setExpanded(true);
}

void CarbonProjectWidget::addCarbonFiles(CarbonFilesFolder* parent)
{
  CarbonFiles* files = mProject->getFiles();
  for (int i=0; i<files->numFiles(); i++)
  {
    CarbonFile* file = files->getFile(i);
    new CarbonFileItem(this, file, parent);
  }

  parent->setExpanded(true);
}

void CarbonProjectWidget::addGeneratedFiles(CarbonGenFilesFolder* parent)
{
  // First, remove any/all children
  QList<QTreeWidgetItem*> children = parent->takeChildren();
  foreach (QTreeWidgetItem* item, children)
    delete item;

  const char* outputDir = mProject->getActive()->getOutputDirectory();
  qDebug() << outputDir;
  QDir qdir(outputDir);

  if (qdir.exists())
  {
    QStringList filters;

    CarbonPropertyValue* outnameValue = mProject->getToolOptions("VSPCompiler")->getValue("-o");
    UtString name;
    name << outnameValue->getValue();
    QFileInfo fi(name.c_str());

    UtString baseFilter;
    baseFilter << fi.baseName() << ".errors";
    filters << baseFilter.c_str();

    qdir.setNameFilters(filters);

    QStringList files = qdir.entryList();
    for (int i=0; i<files.count(); i++)
    {
      QStringList parts = files.at(i).split(".");
      qDebug() << "Carbon Model: " << files.at(i);
      UtString vhmName;
      vhmName << parts[0];
      populateVHM(parent, outputDir, vhmName.c_str());
    }
  }

  updateVHMName();

  parent->setExpanded(true);
}

// Populate a node under "Generated Files" for this VHM
void CarbonProjectWidget::populateVHM(CarbonGenFilesFolder* parent, const char* outputDir, const char* vhmName)
{
  QDir qdir(outputDir);
  QStringList filters;
  UtString filterName;
  filterName << vhmName << ".*";
  filters << filterName.c_str();
  CarbonGeneratedVHMFolder* root = new CarbonGeneratedVHMFolder(this, parent);
  root->setText(0, vhmName);

  QStringList filteredTypes;
  filteredTypes << 
    "a" << 
    "dir" << 
    "io.db" << 
    "symtab.db" << 
    "cmodel.dir" << 
    "hdl-annotations" << 
    "impl-annotations" << 
    "prof" << 
    "cgraph" <<
    "drivers" <<
    "xref";

  qdir.setNameFilters(filters);
  QStringList files = qdir.entryList();
  for (int i=0; i<files.count(); i++)
  {
    QStringList parts = files.at(i).split(".");

   // qDebug() << "  file: " << files.at(i);

    QString vhmFile = files.at(i);
    QFileInfo fi(vhmFile);
    QString ext = fi.completeSuffix();

    bool skipIt = false;
    foreach (QString e, filteredTypes)
    {
      if (e == ext)
      {
        skipIt = true;
        break;
      }
    }

    if (skipIt)
      continue;

    CarbonGeneratedVHMFolder* item = new CarbonGeneratedVHMFolder(this, root);
    UtString fpath;
    UtString fname;
    fname << files.at(i);

    OSConstructFilePath(&fpath, mProject->getActive()->getOutputDirectory(), fname.c_str());
    item->putPath(fpath.c_str());
    item->setText(0, files.at(i));
  }
}

void CarbonProjectWidget::addGroupsAndFiles(CarbonRTLFolder* parent)
{
  CarbonSourceGroups* groups = mProject->getGroups();

  // If the project gets out of sync (user edits xml) we need to make sure we have 
  // at least one group
  if (groups->numGroups() == 0)
  {
    CarbonSourceGroup* group = new CarbonSourceGroup(mProject->getGroups());
    group->putVhdlLibrary("");
    group->putVerilogLibrary("");
    group->putName("RTL Model");
    mProject->getGroups()->addGroup(group);
  }

  for (int i=0; i<groups->numGroups(); ++i)
  {
    CarbonSourceGroup* group = groups->getGroup(i);
    CarbonHDLFolderItem* folder = new CarbonHDLFolderItem(this, parent, group);
    addFiles(group, folder);
    folder->setText(0, group->getName());
    folder->setExpanded(true);
  }

  parent->setExpanded(true);
}

void CarbonProjectWidget::addFiles(CarbonSourceGroup* group, CarbonHDLFolderItem* parent)
{
  QPalette palette = QApplication::palette();

  for (int i=0; i<group->numSources(); ++i)
  {
    CarbonHDLSourceFile* src = group->getSource(i);

    UtString file = src->getRelativePath();
    QString filePath = file.c_str();

    CarbonProject::HDLType hdlType = hdlFileType(file.c_str());

    UtString fDir;
    UtString fName;
    OSParseFileName(file.c_str(), &fDir, &fName);

    UtString absPath;
    QFileInfo fi(file.c_str());

    if (!filePath.contains(ENVVAR_REFERENCE) && fi.isRelative())
      OSConstructFilePath(&absPath, mProject->getProjectDirectory(), file.c_str());
    else
      absPath << CarbonProject::fixVarReferences(file.c_str());

    QString resolvedName = mProject->getWindowsEquivalentPath(absPath.c_str());

    UtString expandedName;
    UtString resName;
    resName << resolvedName;
    UtString errMsg;
    if (OSExpandFilename(&expandedName, resName.c_str(), &errMsg))
      resolvedName = expandedName.c_str();
    
    QFileInfo fi2(resolvedName);

    bool exists = fi2.exists();
  
 
    switch (hdlType)
    {
    case CarbonProject::VHDL:
      {
        CarbonVhdlItem* item = new CarbonVhdlItem(this, src, parent);
        item->setData(fName.c_str(), file.c_str());
        item->setText(0, fName.c_str());
        if (!exists)
        {
          QColor color = palette.color(QPalette::Disabled, QPalette::Text);
          QBrush brush = item->foreground(0);
          brush.setColor(color);
          item->setForeground(0, brush);
          item->indicateNotFound();
        }
        if (0 == strcmp(group->getName(), MODELWIZARD_GENERATED))
        {
          UtString wizFilename;
          wizFilename << QString("%1/%2.rmxml").arg(fi2.absolutePath()).arg(fi2.baseName());
          QFileInfo fi3(wizFilename.c_str());
          if (fi3.exists())
          {
            CarbonRemodelItem* ri = new CarbonRemodelItem(this, wizFilename.c_str(), item, wizFilename.c_str());
            if (ri)
              item->setExpanded(true);
          }
        }
        break;
      }
    case CarbonProject::Verilog:
      {
        CarbonVerilogItem* item = new CarbonVerilogItem(this, src, parent);
        item->setData(fName.c_str(), file.c_str());
        item->setText(0, fName.c_str());
        if (!exists)
        {
          QColor color = palette.color(QPalette::Disabled, QPalette::Text);
          QBrush brush = item->foreground(0);
          brush.setColor(color);
          item->setForeground(0, brush);
          item->indicateNotFound();
        }
        if (0 == strcmp(group->getName(), MODELWIZARD_GENERATED))
        {
          UtString wizFilename;
          wizFilename << QString("%1/%2.rmxml").arg(fi2.absolutePath()).arg(fi2.baseName());
          QFileInfo fi3(wizFilename.c_str());
          if (fi3.exists())
          {
            CarbonRemodelItem* ri = new CarbonRemodelItem(this, wizFilename.c_str(), item, wizFilename.c_str());
            if (ri)
              item->setExpanded(true);
          }
        }
      break;
      }
    case CarbonProject::CPP:
      {
        CarbonCPPItem* item = new CarbonCPPItem(this, src, parent);
        item->setData(fName.c_str(), file.c_str());
        item->setText(0, fName.c_str());
        if (!exists)
        {
          QColor color = palette.color(QPalette::Disabled, QPalette::Text);
          QBrush brush = item->foreground(0);
          brush.setColor(color);
          item->setForeground(0, brush);
          item->indicateNotFound();
        }
        break;
      }
    case CarbonProject::Header:
      {
        CarbonHItem* item = new CarbonHItem(this, src, parent);
        item->setData(fName.c_str(), file.c_str());
        item->setText(0, fName.c_str());
        if (!exists)
        {
          QColor color = palette.color(QPalette::Disabled, QPalette::Text);
          QBrush brush = item->foreground(0);
          brush.setColor(color);
          item->setForeground(0, brush);
          item->indicateNotFound();
        }
        break;
      }
    }
  }
}

bool CarbonProjectWidget::actionCopyConfiguration()
{
  DlgConfiguration dlg(this);
  dlg.setProject(this);
  if (dlg.exec() == QDialog::Accepted)
  {
    QApplication::setOverrideCursor(Qt::WaitCursor);
    project()->copyConfiguration(dlg.getFromConfiguration(), dlg.getToConfiguration());
    UtString msg;
    msg << "Successfully copied '" << dlg.getFromConfiguration() << "' to '" << dlg.getToConfiguration() << "'";
    QApplication::restoreOverrideCursor();
    QMessageBox::information(this, tr(APP_NAME), msg.c_str());
    return true;
  }
  return false;
}

void CarbonProjectWidget::actionConfigurationManager()
{
  DlgConfigurationManager dlg(this);
  dlg.setProject(this);
  dlg.exec();
}

void CarbonProjectWidget::properties()
{
  actionProperties();
}

QString CarbonProjectWidget::makeRelativePath(const QString& fromPath, const QString& toPathInput)
{
#if !pfWINDOWS
  UtString u1, u2;
  u1 << fromPath;
  u2 << toPathInput;
  QFileInfo fiFrom(fromPath);
  QFileInfo fiTo(toPathInput);

  UtString uf1,uf2;
  uf1 << fiFrom.absoluteFilePath();
  uf2 << fiTo.absoluteFilePath();
  QStringList uf1list, uf2list;

  QString qf1 = uf1.c_str();
  QString qf2 = uf2.c_str();

  uf1list = qf1.split('/');
  uf2list = qf2.split('/');

  // different filesystems?
  if (uf1list.count() >= 1 && uf2list.count() >= 1 && uf1list[1] != uf2list[1])
    return toPathInput;
#endif


  QFileInfo fi(toPathInput);
  if (fi.isRelative() && fi.path() == ".")
    return toPathInput;

  if ( fromPath == toPathInput )
    return ".";

  // absoluteFilePath relies upon the current directory
  // which is why we do a changedir to the project directory 
  QString toPath = fi.absoluteFilePath();

  QStringList fromDirs = fromPath.split( '/' );
  QStringList toDirs = toPath.split( '/' );

#if pfWINDOWS
  QString t1 = fromDirs.at(0).toLower();
  QString t2 = toDirs.at(0).toLower();
  // Different drive letters, so no relative path used
  if (fromDirs.at(0).toLower() != toDirs.at(0).toLower())
    return toPath;
#endif

  // If we have different root directories and we are above it, make it absolute
  if (fromDirs.count() > 0 && toDirs.count() > 0 && fromDirs.at(0) != toDirs.at(0))
    return toPathInput;

  //qDebug() << "makeRelative: from: " << fromPath << " to: " << toPathInput;
  //qDebug() << " abspath: " << fi.absolutePath();
  //qDebug() << " path: " << fi.path();

  QStringList::iterator fromIt = fromDirs.begin();
  QStringList::iterator toIt = toDirs.begin();
  QString relative;

  for (; fromIt != fromDirs.end() && toIt != toDirs.end() && (*fromIt) == (*toIt); ++fromIt,++toIt)
    ;

  for ( ; fromIt != fromDirs.end(); ++fromIt )
    relative += "../";
  for ( ; toIt != toDirs.end();)
  {
    relative += *toIt;
    ++toIt;
    if (toIt != toDirs.end())
      relative += "/";
  }

  //qDebug() << " constructed relpath: " << relative;

  return relative;
} 


void CarbonProjectWidget::setConsole(CarbonConsole* console)
{
  mConsole=console;
}

void CarbonProjectWidget::keyPressEvent( QKeyEvent * event)  
{
  QTreeWidgetItem* item = currentItem();
  if (item)
  {
    CarbonProjectTreeNode* node = static_cast<CarbonProjectTreeNode*>(item);
    if (node != NULL)
      node->keyPressEvent(event);
  }
  CDragDropTreeWidget::keyPressEvent(event);
}

void CarbonProjectWidget::updateHierarchy(HierarchyMode mode, const char* iodbName)
{
  QFileInfo fi(iodbName);
  if (fi.exists())
    mProject->getDbContext()->loadDatabase(mContext->getQtContext(), iodbName); 
  else
    mProject->getDbContext()->unloadDatabase();

  if (mHierarchyWindow == NULL)
  {
    mHierarchyWindow = new HierarchyDockWindow(mContext->getMainWindow());
    mHierarchyDockWidget = new QDockWidget(tr("Design Hierarchy"), mContext->getMainWindow());
    mHierarchyDockWidget->setWidget(mHierarchyWindow);
    mHierarchyDockWidget->setObjectName("Design Hierarchy");
    mHierarchyWindow->setContext(mode, mContext, mProject->getDbContext());
    mContext->getMainWindow()->tabifyDockWidget(mDockWidget, mHierarchyDockWidget);
	
    CQT_CONNECT(mHierarchyWindow, navigateSource(const char*, int), this, navigateSource(const char*, int)); 
    CQT_CONNECT(mProject->getProjectWidget()->getHierarchyWindow(), applySignalDirective(DesignDirective, const char*, bool), this, applySignalDirective(DesignDirective, const char*, bool));

  }
  else
    mHierarchyWindow->setContext(mode, mContext, mProject->getDbContext());
}

void CarbonProjectWidget::applySignalDirective(DesignDirective designDirective, const char* text, bool value) 
{
  if (mDirectivesEditor == NULL)
    mDirectivesEditor = compilerDirectives();
  
  mDirectivesEditor->modifyDirective(designDirective, text, value);
}

void CarbonProjectWidget::navigateSource(const char* src, int line)
{
  openSource(src, line, NULL);
}

// Update the widgets (compilation might have just completed, or project was loaded)
void CarbonProjectWidget::updateWidgets()
{
  QString dbName = mProject->getDatabaseFilePath();
  UtString db; db << dbName;
  updateHierarchy(eCarbonDBHierarchy, db.c_str());

  mProject->getComponents()->populateComponentTree(this, NULL);

}
void CarbonProjectWidget::stopCompilation()
{
  if (mConsole)
    mConsole->stopCompilation();
}

void CarbonProjectWidget::compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  qDebug() << "Compilation started....";
  mCompilationInProgress = true;
  MDIProjectTemplate* projTemplate = dynamic_cast<MDIProjectTemplate*>(mContext->getDocumentManager()->findTemplate(NULL, "Project"));
  if (projTemplate)
    projTemplate->compilationModeChanged(mCompilationInProgress);
}

void CarbonProjectWidget::exploreFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  const char* guidbName = mProject->getDesignFilePath(".gui.db");
  QFileInfo fi(guidbName);
  if (fi.exists())
  {
    updateHierarchy(eCarbonPCHierarchy, guidbName);
  }
}

void CarbonProjectWidget::compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
}

bool CarbonProjectWidget::check()
{
  bool status = false;
  if (mProject)
    status = mProject->check(this);
  return status;
}

// The Main entry point to compile.
void CarbonProjectWidget::compile(bool cleanFlag, bool compilationFlag, bool packageFlag, const char* compileTarget, const char* cleanTarget, QStringList* args, const char* armKitRoot)
{
  if (mProject)
  {
    makeConsoleVisible();
    mProject->compile(this, cleanFlag, compilationFlag, packageFlag, compileTarget, cleanTarget, args, armKitRoot);
  }
}

void CarbonProjectWidget::deleteComponent(CarbonComponent* compToDelete)
{
  mProject->deleteComponent(compToDelete);
  updateContextMenuActions();
}

void CarbonProjectWidget::removeDirectoryAndFiles(const char* dirName)
{
  QDir dir(dirName);
  if (dir.exists())
  {
    foreach (QFileInfo fi, dir.entryInfoList())
    {
      UtString directory;
      directory << fi.absoluteFilePath();

      UtString dirShortName; // shortname will be "" for "." and ".." directories, so skip those
      dirShortName << fi.baseName();

      if (0 == strcmp(directory.c_str(), dirName) || dirShortName.length() == 0)
        continue;

      if (fi.isDir())
        removeDirectoryAndFiles(directory.c_str());
      else if (fi.isFile() && fi.exists())
        QFile::remove(fi.filePath());
    }
    dir.rmdir(dirName);
  }
}


MDIWidget* CarbonProjectWidget::openSourceFile(const QString& filename, int lineNumber, const char* docType)
{
  UtString file; file << filename;
  return openSource(file.c_str(), lineNumber, docType);
}

MDIWidget* CarbonProjectWidget::openSource(const char* filename, int lineNumber, const char* docType)
{
  UtString filePath;
  bool composeStatus = true;
  
  // If filename is NULL, this component type is not assiciated with a particular file
  if(filename) {
    composeStatus = composeFilename(filename, &filePath);
  
    // Error was handled by composeFilename
    if (!composeStatus)
      return NULL;
  }
  else {
   filePath = docType;
  }
  qDebug() << "opening source: " << filePath.c_str();

  MDIDocumentTemplate* t = mContext->getDocumentManager()->findTemplate(filePath.c_str(), docType);

  if (t == NULL) // Try text file
  {
    t = mContext->getDocumentManager()->findTemplate(filePath.c_str(), "TextEditor");
    // Filename was composed, but could not be resolved, so warn user
    if (t && composeStatus && filePath.length() == 0)
    {
      UtString msg;
      msg << "Unable to access file: " << filename;
      QMessageBox::warning(NULL, MODELSTUDIO_TITLE, msg.c_str()); 
      return NULL;
    }
  }
  
  if (t)
  {
    MDIWidget* mdiWidget = t->findDocument(mContext->getWorkspace(), filePath.c_str(), true);

    if (mdiWidget == NULL) // New one, otherwise, it gets activated 
    {
      mdiWidget = t->openDocument(mContext->getMainWindow(), filePath.c_str());

      if (mdiWidget)
      {
        mContext->getWorkspace()->addWindow(mdiWidget->getWidget());
        // Did you forget to call initializeMDI in the widget constructor?
        mdiWidget->getWidget()->showMaximized();
        // a newly opened document cannot be modified
        mdiWidget->getWidget()->setWindowModified(mdiWidget->isWidgetModified());

        QFileInfo f(filePath.c_str());   
        QWidget* tw = new QWidget();
        QVariant qv = qVariantFromValue((void*)mdiWidget->getWidget());

        tw->setProperty("CarbonTab", qv);

        QString tabName = f.fileName();
        // This is wrong for tabs like Directives and PackageTool.
        QString tabTooltip = f.absoluteFilePath();

        if (tabName.isEmpty())
          tabName = filePath.c_str();

        if (tabTooltip.isEmpty())
          tabTooltip = filePath.c_str();

        int index = mContext->getSourceTabWidget()->addTab(tw, tabName);
        mContext->getSourceTabWidget()->setTabToolTip(index, tabTooltip);
        mContext->getSourceTabWidget()->setCurrentIndex(index);

        QVariant ti(index);
        mdiWidget->getWidget()->setProperty("CarbonTabPlaceholder", 
          qVariantFromValue((void*)tw));

        mdiWidget->getWidget()->setProperty("CarbonTabWidget", 
          qVariantFromValue((void*)mContext->getSourceTabWidget()));

        //mContext->getWorkspace()->setActiveWindow(mdiWidget->getWidget());
      }
    }

    if (lineNumber != -1 && mdiWidget)
    {
      QTextEditor* te = dynamic_cast<QTextEditor*>(mdiWidget);
      if (te)
        te->gotoLine(lineNumber);
    }
    return mdiWidget;
  }
  return NULL;
}

MDIWidget* CarbonProjectWidget::getOpenFile(const char* filename)
{
  // Attempt to return the widget for an already-opened file.  This
  // does not activate the tab containing the document.
  UtString filePath;
  composeFilename(filename, &filePath);

  MDIDocumentTemplate* t = mContext->getDocumentManager()->findTemplate(filePath.c_str(), "TextEditor");

  MDIWidget *widget = NULL;
  if (t) {
    widget = t->findDocument(mContext->getWorkspace(), filePath.c_str(), false);
  }
  return widget;
}

// Drag & Drop
//! Override mouse events in order to initiate drags
void CarbonProjectWidget::mousePressEvent(QMouseEvent* e)
{
  bool forwardEventToParent = true;
  int button = e->button();

  if (mDragState == eDragIdle && 
      ((button == Qt::LeftButton) || (button == Qt::MidButton)))
  {
    // you have to have clicked over an item
    QTreeWidgetItem* item = itemAt(e->x(), e->y());

    CarbonProjectTreeNode* treeNode = dynamic_cast<CarbonProjectTreeNode*>(item);

    if (treeNode && treeNode->dragBeginEvent(e))
    {
      if (!item->treeWidget()->isItemSelected(item))
      {
        item->treeWidget()->setCurrentItem(item);
        treeNode->singleClicked(0);
      }

      if (item != NULL)
      {
        mSaveItem = item;
        mDragItem = treeNode;
        //forwardEventToParent = false;
        // If the button is either Left (Windows std) or Middle (Motif std),
        // start a drag, so that the app behaves the same way on Windows
        // and Linux, and isn't surprising on either.  If the middle
        // button is used, then we can begin the drag immediately on
        // the press.  For a left-button we have to wait till the
        // mouse has been moved outside of the widget, otherwise we
        // will disturb group-selects
        if (button == Qt::MidButton)
          startDrag();
        // Left button starts a drag only if the item is selected already
        else
          mDragState = eLookForMove;        
      }
    }
  }

  if (forwardEventToParent)
    CDragDropTreeWidget::mousePressEvent(e);
}
void CarbonProjectWidget::mouseReleaseEvent(QMouseEvent* e)
{
  CDragDropTreeWidget::mouseReleaseEvent(e);
}

void CarbonProjectWidget::mouseMoveEvent(QMouseEvent* e)
{
  CDragDropTreeWidget::mouseMoveEvent(e);
}
void CarbonProjectWidget::dragEnterEvent(QDragEnterEvent* e)
{
  if (mEnableDrop) 
  {
    QTreeWidgetItem* dropOntoItem = itemAt(e->pos().x(), e->pos().y());
    if (dropOntoItem)
    {
      CarbonProjectTreeNode* treeNode = dynamic_cast<CarbonProjectTreeNode*>(dropOntoItem);
      if (treeNode)
        e->accept();
    }
  }
}

void CarbonProjectWidget::dragMoveEvent(QDragMoveEvent* e)
{
  if (mEnableDrop) 
  {
    QTreeWidgetItem* dropOntoItem = itemAt(e->pos().x(), e->pos().y());
    if (dropOntoItem)
    {
      CarbonProjectTreeNode* treeNode = dynamic_cast<CarbonProjectTreeNode*>(dropOntoItem);
      if (treeNode && mDragItem && treeNode->canDropItem(mDragItem))
      {
        e->accept();
        animateDropSite(e->pos());
      }
      else
        e->ignore();
    }
  }
}
void CarbonProjectWidget::dropEvent(QDropEvent* e)
{
  clearAnimation();
  if (mEnableDrop)
  {
    e->acceptProposedAction();
    QPoint pos = e->pos();
    QTreeWidgetItem* dropOntoItem = itemAt(pos.x(), pos.y());
    if (dropOntoItem)
    {
      CarbonProjectTreeNode* treeNode = dynamic_cast<CarbonProjectTreeNode*>(dropOntoItem);
      if (treeNode->dropItem(mDragItem))
        e->accept();
      else
        e->ignore();
    }
  }
  else
    e->ignore();
}

void CarbonProjectWidget::dragLeaveEvent(QDragLeaveEvent* e)
{
  CDragDropTreeWidget::dragLeaveEvent(e);
}
bool CarbonProjectWidget::dragBeginEvent(QMouseEvent*)
{
  return true;
}

// Returns true if we are on Windows and using remote compiling
bool CarbonProjectWidget::isRemoteCompilation()
{
  CarbonConsole::CommandMode mode = CarbonConsole::Local;
#if pfWINDOWS
  CarbonProject* proj = project();
  if (proj)
  {

    QString libname = proj->getDesignFilePath(".lib");
    QFileInfo fi(libname);
    if (fi.exists())
      return false;

    CarbonOptions* options = proj->getProjectOptions();
    const char* remoteServer = options->getValue("Remote Server")->getValue();
    if (remoteServer && strlen(remoteServer) > 0)
      mode = CarbonConsole::Remote;
  }
  else 
    mode = CarbonConsole::Remote;
#endif
  return (mode == CarbonConsole::Remote);
}

void CarbonProjectWidget::makeConsoleVisible()
{
  QDockWidget* dw = mContext->getWorkspaceModelMaker()->getRemoteConsoleDockWindow();
  // lower followed by raise causes it to "pop" forward
  dw->setVisible(true);
  dw->lower();
  dw->raise();
}
