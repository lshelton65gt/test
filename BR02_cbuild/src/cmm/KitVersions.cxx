//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "util/UtEncryptDecrypt.h"
#include "CarbonMakerContext.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"
#include "KitManifest.h"
#include "CarbonFiles.h"
#include "CarbonComponent.h"
#include "ModelKitWizard.h"
#include "ShellConsole.h"
#include "ScriptingEngine.h"
#include "JavaScriptAPIs.h"
#include <stdio.h>
#include <assert.h>

#define MODEL_KITS_TAG "ModelKits"
#define MODEL_KITS_VERSION_TAG "version"
#define MODEL_KITS_XML_VERSION "1"

KitVersions::KitVersions()
{
}

KitManifests::KitManifests()
{
}

KitVersion* KitVersions::getLatestVersion()
{
  quint32 vn = 0;
  KitVersion* kv = NULL;

  foreach(KitVersion* v, mVersions)
  {
    if (v->getID() > vn)
    {
      kv = v;
      vn = v->getID();
    }
  }

  return kv;
}

void KitManifests::deserialize(ModelKit*, QDomElement& parent)
{
  QDomElement versionsElem = parent.firstChildElement("Manifests");
  QDomElement e = versionsElem.firstChildElement("Manifest");
  while (!e.isNull())
  {
    KitManifest* mf = new KitManifest();
    mf->deserialize(e);
    mf->setPreserveHashes(true);
    mf->setEditable(false);
    mManifests.append(mf);
    e = e.nextSiblingElement("Manifest");
    if (e.isNull()) // Latest manifest is editable
    {
      mf->setPreserveHashes(false);
      mf->setEditable(true);
    }
  }
}

quint32 KitManifests::nextID()
{
  quint32 id = 0;
  foreach(KitManifest* km, mManifests)
  {
    if (km->getID() > id)
      id = km->getID();
  }
  return id+1;
}
void KitManifests::serialize(QDomDocument& doc, QDomElement& parent)
{
  // Serialize each Manifest
  QDomElement manifests = XmlHelper::addElement(doc, parent, "Manifests");
  foreach(KitManifest* km, mManifests)
    km->serialize(doc, manifests);
}

void KitVersions::deserialize(ModelKit* kit, QDomElement& parent)
{
  QDomElement versionsElem = parent.firstChildElement("Versions");

  QDomElement e = versionsElem.firstChildElement("Version");
  while (!e.isNull())
  {
    quint32 manifestID = e.attribute("manifestId").toUInt();
    KitManifest* manifest = kit->getManifests()->findManifest(manifestID);
    INFO_ASSERT(manifest, "Expected to find manifest");
    KitVersion* kv = new KitVersion(manifest, 
                      e.attribute("description"),
                      e.attribute("id").toUInt());

    kv->setLocked(e.attribute("locked") == "true" ? true : false);
    kv->setVendorVersion(e.attribute("vendorVersion"));
    mVersions.append(kv);
    e = e.nextSiblingElement("Version");
  }
}

void KitVersions::serialize(QDomDocument& doc, QDomElement& parent)
{
  // Serialize each Version
  QDomElement versions = XmlHelper::addElement(doc, parent, "Versions");
  foreach(KitVersion* kv, mVersions)
    kv->serialize(doc, versions);
}

void KitVersion::serialize(QDomDocument& doc, QDomElement& parent)
{
  QDomElement element = XmlHelper::addElement(doc, parent, "Version");

  element.setAttribute("manifestId", QString("%1").arg(mManifest->getID()));
  element.setAttribute("vendorVersion", mVendorVersion);
  element.setAttribute("id", QString("%1").arg(mID));
  element.setAttribute("description", mDescription);
  element.setAttribute("locked", mLocked ? "true" : "false");
}





