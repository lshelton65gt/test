//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "CompWizardPortEditor.h"
#include "CompWizardTreeNodes.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonFiles.h"
#include "PortEditorCommands.h"
#include "MdiMaxsim.h"
//#include "MaxsimWizardWidget.h"
#include "MaxsimComponent.h"
#include "cmm.h"
#include "DlgAddMaxsimXtor.h"
#include "DlgNewParam.h"
#include "ESLXtor.h"
#include "ESLClockGen.h"
#include "CowareComponent.h"
#include "SystemCComponent.h"

void CompWizardPortEditor::save()
{
  qDebug() << "Saving Port Editor";
}

void CompWizardPortEditor::createToolbar(QWidget*)
{
  ui.toolButtonDelete->setDefaultAction(mActionDelete);
  ui.toolButtonUndo->setDefaultAction(mActionUndo);
  ui.toolButtonRedo->setDefaultAction(mActionRedo);
  ui.toolButtonAddXtor->setDefaultAction(mActionAddXtor);
  ui.toolButtonAddParam->setDefaultAction(mActionAddParam);

  if (mEditorMode == eModeCoWare) // || mEditorMode == eModeSystemC)
  {
    ui.toolButtonAddParam->setVisible(false);
    ui.toolButtonAddXtor->setText("Add Protocol");
    ui.toolButtonAddXtor->setToolTip("Add Protocol");
    ui.toolButtonAddXtor->setStatusTip("Add a new Protocol");
    ui.label->setText("Protocol:");
  }

  if (mEditorMode == eModeSystemC) // hide the "Add Protocol" stuff
  {
    ui.label->setVisible(false);
    ui.toolButtonAddParam->setVisible(false);
    ui.toolButtonAddXtor->setVisible(false);
    ui.comboBoxXtors->setVisible(false);
  }
}

void CompWizardPortEditor::fillXtorCombo()
{
  ui.comboBoxXtors->clear();
  QStringList items;
  for (CarbonCfg::XtorLibs::SortedLoop l = mCfg->loopXtorLibs(); !l.atEnd(); ++l) {
    const UtString& libName = l.getKey();
    CarbonCfgXtorLib* xlib = l.getValue();
    for (UInt32 i = 0, n = xlib->numXtors(); i < n; ++i) {
      CarbonCfgXtor* xtor = xlib->getXtor(i);
      if (libName == CARBON_DEFAULT_XTOR_LIB) {
        items.append(xtor->getName());
      } else {
        QString fullName;
        if (strlen(xtor->getVariant()) == 0) {
          fullName = QString("%1/%2").arg(libName.c_str()).arg(xtor->getName());
        } else {
          fullName = QString("%1/%2/%3").arg(libName.c_str()).arg(xtor->getName()).arg(xtor->getVariant());
        }
        items.append(fullName);
      }    
    }
  }
  items.sort();

  ui.comboBoxXtors->addItems(items);
}

void CompWizardPortEditor::setEditorMode(EditorMode mode)
{
  mEditorMode = mode;
}

void CompWizardPortEditor::closeEvent(QCloseEvent*)
{
  ui.treeWidget->closeTree();
  mUndoView->close();
}

CompWizardPortEditor::CompWizardPortEditor(QWidget *parent)
: QWidget(parent)
{
  ui.setupUi(this);

  mEditorMode = eModeSocDesigner;

  // Init to NULL

  mActionRedo= NULL;
  mActionUndo= NULL;
  mActionDelete= NULL;
  mActionAddXtor= NULL;
  mActionBind= NULL;
  mActionAddParam= NULL;
  mPortsRoot = NULL;
  mParametersRoot = NULL;
  mTiesRoot = NULL;
  mDisconnectsRoot = NULL;
  mResetsRoot = NULL;
  mClocksRoot = NULL;
  mSystemCClocksRoot = NULL;

  mToolbar = NULL;
  mDB = 0;
  mCfg = 0;

  mUndoEnabled = false;
  mRedoEnabled = false;

  mUndoStack = new QUndoStack();

 // Compute History Height (5 % of the window)
  int historyHeight = (int)((float)height() * (float)0.05);
  ui.widgetHistory->resize (ui.widgetHistory->width(), historyHeight);

  mUndoView = new QUndoView(mUndoStack, ui.widgetHistory);  
  mUndoView->setWindowFlags(mUndoView->windowFlags() | Qt::WindowStaysOnTopHint);
  mUndoView->setWindowTitle("Action History");
  mUndoView->setAttribute(Qt::WA_QuitOnClose, false);

  mUndoView->show();

  QGridLayout* gridLayout = new QGridLayout(ui.widgetHistory);
  gridLayout->setSpacing(0);
  gridLayout->setMargin(0);
  gridLayout->addWidget(mUndoView);
 
  createActions();
  createMenus();
  connectWidgets();

  ui.treeWidget->setItemDelegate(new PortEditorDelegate(this, ui.treeWidget, this));
  ui.treeWidget->setEditTriggers(QAbstractItemView::AllEditTriggers);
  ui.treeWidget->setUndoStack(mUndoStack);
  ui.treeWidget->setEditor(this);

  QList<int> sizes;
  sizes << height()-historyHeight << historyHeight;
  ui.splitter->setSizes(sizes);

  setWindowTitle("Ports[*]");
}

void CompWizardPortEditor::deleteCurrent()
{
  qDebug() << "Delete";

  // Commit and close any open editors
  ui.treeWidget->closeEditor();

  QList<PortEditorTreeItem*> deleteItems;
  foreach (QTreeWidgetItem* item, ui.treeWidget->selectedItems())
  {
    deleteItems.append(dynamic_cast<PortEditorTreeItem*>(item));
  }

  CmdDeleteItems* delItems = new CmdDeleteItems(deleteItems);
  if (delItems->numItems() > 0)
    mUndoStack->push(delItems);
  else
  {
    delete delItems;
    theApp->statusBar()->showMessage("Nothing to delete", 2000);
  }
}

void CompWizardPortEditor::treeWidgetModified(bool value)
{
  setWindowModified(value);
  emit widgetModified(value);
}

void CompWizardPortEditor::canRedoChanged(bool enabled)
{
  mRedoEnabled = enabled;
  mActionRedo->setEnabled(enabled);
}
void CompWizardPortEditor::canUndoChanged(bool enabled)
{
  mUndoEnabled = enabled;
  mActionUndo->setEnabled(enabled);
}

// called whenever tree selection changes
void CompWizardPortEditor::treeWidgetSelectionChanged()
{
  int numSelectedItems = ui.treeWidget->numSelectedItems();

  int numXtorInsts = ui.treeWidget->numSelectedItems(PortEditorTreeItem::XtorInst);

  mActionBind->setEnabled(numSelectedItems == 1 && numXtorInsts == 1);
}

void CompWizardPortEditor::modelCompilationFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
}

void CompWizardPortEditor::connectWidgets()
{
  CQT_CONNECT(mUndoStack, canRedoChanged(bool), this, canRedoChanged(bool));
  CQT_CONNECT(mUndoStack, canUndoChanged(bool), this, canUndoChanged(bool));

  CQT_CONNECT(ui.treeWidget, customContextMenuRequested(const QPoint&), 
    this, customContextMenuRequested(const QPoint&));
  CQT_CONNECT(ui.treeWidget, widgetModified(bool), this, treeWidgetModified(bool));
  CQT_CONNECT(ui.treeWidget, itemSelectionChanged(), this, treeWidgetSelectionChanged());

  CQT_CONNECT(getProjectWidget()->getConsole(), 
    modelCompilationFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType), this,
    modelCompilationFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType));
}

void CompWizardPortEditor::customContextMenuRequested(const QPoint& pos)
{
  int numItems = ui.treeWidget->selectedItems().count();

  const QPoint& point = ui.treeWidget->viewport()->mapToGlobal(pos);

  qDebug() << "context menu selected items" << numItems;

  QMenu menu;

  foreach (QTreeWidgetItem* treeItem, ui.treeWidget->selectedItems())
  {
    PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(treeItem);
    if (item)
      item->multiSelectContextMenu(menu);
  }

  menu.addSeparator();
  menu.addAction(mActionDelete);
  menu.addAction(mActionUndo);
  menu.addAction(mActionRedo);
  menu.exec(point);
}

CompWizardPortEditor::~CompWizardPortEditor()
{
}


CarbonProject* CompWizardPortEditor::getProject()
{
  CarbonProjectWidget* pw = getProjectWidget();
  if (pw)
    return pw->project();
  else
    return NULL;
}

CarbonProjectWidget* CompWizardPortEditor::getProjectWidget()
{
  return theApp->getContext()->getCarbonProjectWidget();
}

CarbonComponent* CompWizardPortEditor::getComponent()
{
  CarbonProjectWidget* pw = getProjectWidget();
  if (pw)
  {
    EditorMode mode = getEditorMode();
    switch(mode)
    {
      case eModeSocDesigner: return pw->getActiveMaxsimComponent(); break;
      case eModeCoWare: return pw->getActiveCowareComponent(); break;
	  case eModeSystemC: return pw->getActiveSystemCComponent(); break;
    }
  }

  return NULL;
}

void CompWizardPortEditor::setCcfg(CarbonCfg* cfg)
{
  mCfg = cfg;
  populate(mCfg->getDB(), false);
}
bool CompWizardPortEditor::populate(CarbonDB* carbonDB, bool)
{
  mDB = carbonDB;
  CarbonComponent* maxsim = getComponent();
  INFO_ASSERT(maxsim, "Expecting component");

  CQT_CONNECT(getComponent(), xtorDefFileChanged(), this, xtorDefFileChanged());

  populateFromCcfg();

  fillXtorCombo();
  // Now, load the ccfg file
  qDebug() << "Loading from CCFG";

  UtString msg;
  msg << carbonCfgGetErrmsg(mCfg);  
  CarbonConsole* console = getProjectWidget()->getConsole();
  console->processOutput(CarbonConsole::Local, CarbonConsole::Check, msg.c_str());

  mCfg->clearMessages();

  const char* xtorDefs = getComponent()->xtorDefFile();
  if (xtorDefs)
  {
    TempChangeDirectory t(getProject()->getProjectDirectory());
    CarbonCfgStatus xstatus = carbonCfgReadXtorDefinitions(mCfg, xtorDefs);
    if (xstatus == eCarbonCfgSuccess)
    {
      fillXtorCombo();
    }
    else
    {
      msg.clear();
      msg << "Failed to read transactor definitions file: " << carbonCfgGetErrmsg(mCfg);  
      QMessageBox::warning(getProject()->getProjectWidget(), MODELSTUDIO_TITLE, msg.c_str());
    }
  }


  return true;
}

void CompWizardPortEditor::xtorDefFileChanged()
{
  const char* xtorDefs = getComponent()->xtorDefFile();
  if (xtorDefs)
  {
   TempChangeDirectory t(getProject()->getProjectDirectory());

    carbonCfgReadXtorDefinitions(mCfg, xtorDefs);
  }
 
  fillXtorCombo();

  getComponent()->setXtorDefFile(xtorDefs);

  treeWidgetModified(true);
}
void CompWizardPortEditor::populateFromCcfg()
{
  INFO_ASSERT(getComponent(), "Expecting SoC Designer Component");

  // Inform the tree widget about this ccfg
  ui.treeWidget->setCcfg(mCfg);

  // Build up the tree
  ui.treeWidget->clear();

  mPortsRoot = new PortsRootItem(ui.treeWidget);  

  if (mEditorMode == eModeSocDesigner)
    mParametersRoot = new ParametersRootItem(ui.treeWidget);
 
  mTiesRoot = new TiesRootItem(ui.treeWidget);
  mDisconnectsRoot = new DisconnectsRootItem(ui.treeWidget);

  if (mEditorMode == eModeSocDesigner)
  {
    mResetsRoot = new ResetsRootItem(ui.treeWidget);
    mClocksRoot = new ClocksRootItem(ui.treeWidget);
  }

  if (mEditorMode == eModeSystemC)
    mSystemCClocksRoot = new SystemCClocksRootItem(ui.treeWidget);

  populatePorts();

  if (mEditorMode == eModeSocDesigner)
  {
    populateParameters();
    populateClocks();
    populateResets();
  }

  if (mEditorMode == eModeSystemC)
    populateSystemCClocks();

  ui.treeWidget->header()->resizeSections(QHeaderView::ResizeToContents);

  setWindowModified(false);
}

QTreeWidgetItem* CompWizardPortEditor::getPortsRoot() const
{
  return mPortsRoot; 
}

QTreeWidgetItem* CompWizardPortEditor::getClocksRoot() const
{
  return mClocksRoot; 
}

QTreeWidgetItem* CompWizardPortEditor::getSystemCClocksRoot() const
{
  return mSystemCClocksRoot; 
}


QTreeWidgetItem* CompWizardPortEditor::getResetsRoot() const
{
  return mResetsRoot; 
}

QTreeWidgetItem* CompWizardPortEditor::getTiesRoot() const
{
  return mTiesRoot; 
}

QTreeWidgetItem* CompWizardPortEditor::getParamsRoot() const
{
  return mParametersRoot; 
}

QTreeWidgetItem* CompWizardPortEditor::getDisconnectsRoot() const
{
  return mDisconnectsRoot; 
}


// Populate the tree with ports
void CompWizardPortEditor::populatePorts()
{
  for (UInt32 i=0; i<mCfg->numRTLPorts(); i++)
  {
    CarbonCfgRTLPort* rtlPort = mCfg->getRTLPort(i);
    PortEditorTreeItem::dumpRTLPort(rtlPort);
  }

  // Transaction Ports
  for (UInt32 i=0; i<mCfg->numXtorInstances(); i++)
  {
    CarbonCfgXtorInstance* xtorInst = mCfg->getXtorInstance(i);
    qDebug() << xtorInst << xtorInst->getName();
    new ESLXtorInstanceItem(mPortsRoot, xtorInst);
  }

  // ESL Ports
  for (UInt32 i=0; i<mCfg->numESLPorts(); i++)
  {
    CarbonCfgESLPort* eslPort = mCfg->getESLPort(i);
    new ESLPortItem(mPortsRoot, eslPort);
  }

  // Ties
  for (UInt32 i=0; i<mCfg->numRTLPorts(); i++)
  {
    CarbonCfgRTLPort* rtlPort = mCfg->getRTLPort(i);
    for (UInt32 c=0; c<rtlPort->numConnections(); c++)
    {
      CarbonCfgRTLConnection* rtlConn = rtlPort->getConnection(c);
      CarbonCfgTie* tie = rtlConn->castTie();
      if (tie)
        new ESLTieItem(mTiesRoot, tie);
    }
    // Disconnected?
    if (rtlPort->numConnections() == 0)
        new ESLDisconnectItem(mDisconnectsRoot, rtlPort);
  }

  mPortsRoot->setExpanded(true);
  mTiesRoot->setExpanded(true);
  mDisconnectsRoot->setExpanded(true);

  // Qt Bug: SortChildren is broken if the
  // node is expanded, so collapse them first

  ui.treeWidget->setUpdatesEnabled(false);

  ui.treeWidget->collapseItem(mPortsRoot);
  ui.treeWidget->collapseItem(mTiesRoot);
  ui.treeWidget->collapseItem(mDisconnectsRoot);

  mPortsRoot->sortChildren(0, Qt::AscendingOrder);
  mTiesRoot->sortChildren(0, Qt::AscendingOrder);
  mDisconnectsRoot->sortChildren(0, Qt::AscendingOrder);

  ui.treeWidget->expandItem(mPortsRoot);
  ui.treeWidget->expandItem(mTiesRoot);
  ui.treeWidget->expandItem(mDisconnectsRoot);

  ui.treeWidget->setUpdatesEnabled(true);
}

// Populate the tree with parameters
void CompWizardPortEditor::populateParameters()
{
  for (UInt32 i=0; i<mCfg->numParams(); i++)
  {
    CarbonCfgXtorParamInst* paramInst = mCfg->getParam(i);
    if (paramInst->getElementGenerationType() == eCarbonCfgElementUser)
      new ESLParameterItem(mParametersRoot, paramInst);
  }
  mParametersRoot->setExpanded(true);
}

void CompWizardPortEditor::populateResets()
{
  for (UInt32 i=0; i<mCfg->numRTLPorts(); i++)
  {
    CarbonCfgRTLPort* rtlPort = mCfg->getRTLPort(i);
    for (UInt32 c=0; c<rtlPort->numConnections(); c++)
    {
      CarbonCfgRTLConnection* rtlConn = rtlPort->getConnection(c);
      CarbonCfgResetGen* resetGen = rtlConn->castResetGen();
      if (resetGen)
        new ESLResetGenItem(mResetsRoot, resetGen);
    }
  }
  mResetsRoot->setExpanded(true);
}
void CompWizardPortEditor::populateClocks()
{
  for (UInt32 i=0; i<mCfg->numRTLPorts(); i++)
  {
    CarbonCfgRTLPort* rtlPort = mCfg->getRTLPort(i);
    for (UInt32 c=0; c<rtlPort->numConnections(); c++)
    {
      CarbonCfgRTLConnection* rtlConn = rtlPort->getConnection(c);
      CarbonCfgClockGen* clockGen = rtlConn->castClockGen();
      if (clockGen)
        new ESLClockGenItem(mClocksRoot, clockGen);
    }
  }
  mClocksRoot->setExpanded(true);
}

void CompWizardPortEditor::populateSystemCClocks()
{
  for (UInt32 i=0; i<mCfg->numRTLPorts(); i++)
  {
    CarbonCfgRTLPort* rtlPort = mCfg->getRTLPort(i);
    for (UInt32 c=0; c<rtlPort->numConnections(); c++)
    {
      CarbonCfgRTLConnection* rtlConn = rtlPort->getConnection(c);
      CarbonCfgSystemCClock* clockGen = rtlConn->castSystemCClock();
      if (clockGen)
        new SystemCClockGenItem(mSystemCClocksRoot, clockGen);
    }
  }
  mSystemCClocksRoot->setExpanded(true);
}



void CompWizardPortEditor::undo()
{
  if (mUndoEnabled)
  {
    qDebug() << "undo" << mUndoStack->undoText();
    theApp->statusBar()->showMessage(QString("Undo %1").arg(mUndoStack->undoText()), 1000);
    mUndoStack->undo();
  }
  else
  {
    theApp->statusBar()->showMessage("Nothing to Undo", 3000);
    QApplication::beep();
  }
}

void CompWizardPortEditor::redo()
{
  if (mRedoEnabled)
  {
    qDebug() << "redo" << mUndoStack->redoText();
    theApp->statusBar()->showMessage(QString("Redo %1").arg(mUndoStack->redoText()), 1000);
    mUndoStack->redo();
  }
  else
  {
    theApp->statusBar()->showMessage("Nothing to Redo", 3000);
    QApplication::beep();
  }
}

void CompWizardPortEditor::actionAddParameter()
{
  qDebug() << "Add Parameter";
  DlgNewParam dlg(mCfg, this);
  if (dlg.exec())
  {
    mUndoStack->push(new ESLParameterItem::AddParameter(mParametersRoot, 
      dlg.getType(), dlg.getName(), dlg.getValue(), dlg.getDescription(), dlg.getEnumChoices(), dlg.getScope()));
  }
}


void CompWizardPortEditor::actionAddXtor()
{
  DlgAddMaxsimXtor dlg(mCfg, ui.comboBoxXtors->currentText(), NULL, this);
  if (dlg.exec() == QDialog::Accepted)
  {
    CarbonCfgXtor* xtor = dlg.getXtor();

    if (xtor == NULL || xtor->getName() == NULL)
      return;

    qDebug() << "Add Xtor" << xtor->getName();

    QList<ESLXtorInstanceItem::AddXtorMapping*> mappings;

    for (quint32 i=0; i<dlg.numPorts(); i++)
    {
      CarbonCfgRTLPort* rtlPort = NULL;
      CarbonCfgXtorPort* xtorPort = NULL;
      dlg.getMapping(i, &xtorPort, &rtlPort);
      if (rtlPort)
        qDebug() << "Map" << xtorPort->getName() << "to" << rtlPort->getName();
      else
        qDebug() << "Not Mapped" << xtorPort->getName();

      ESLXtorInstanceItem::AddXtorMapping* mapping = new ESLXtorInstanceItem::AddXtorMapping;
      mapping->mXtorPort = xtorPort;
      mapping->mRTLPort = rtlPort;
      mappings.append(mapping);
    }

    mUndoStack->push(new ESLXtorInstanceItem::AddXtorCommand(mPortsRoot, xtor, mappings));
  }
}

void CompWizardPortEditor::createActions()
{
  mActionUndo = new QAction(QIcon(":/cmm/Resources/undo.png"), tr("U&ndo"), this);
  mActionUndo->setEnabled(false);
  mActionUndo->setShortcut(tr("Ctrl+Z"));
  addAction(mActionUndo);
  CQT_CONNECT(mActionUndo, triggered(), this, undo());

  mActionRedo = new QAction(QIcon(":/cmm/Resources/redo.png"), tr("Re&do"), this);
  QList<QKeySequence> redoShortcuts;
  redoShortcuts << tr("Ctrl+Y") << tr("Shift+Ctrl+Z");
  mActionRedo->setShortcuts(redoShortcuts);
  mActionRedo->setEnabled(false);
  addAction(mActionRedo);
  CQT_CONNECT(mActionRedo, triggered(), this, redo());

  mActionDelete = new QAction(QIcon(":/cmm/Resources/DeleteHS.png"), tr("Dele&te"), this);
  mActionDelete->setShortcut(tr("Del"));
  addAction(mActionDelete);
  CQT_CONNECT(mActionDelete, triggered(), this, actionDelete());

  mActionAddXtor = new QAction(QIcon(":/cmm/Resources/addxtor.png"), tr("Add Transactor..."), this);
  mActionAddXtor->setToolTip("Add Transaction Port");
  mActionAddXtor->setStatusTip("Add a new Transaction Port");
  addAction(mActionAddXtor);
  CQT_CONNECT(mActionAddXtor, triggered(), this, actionAddXtor());

  mActionBind = new QAction(QIcon(":/cmm/Resources/RelationshipsHS.png"), tr("Bind"), this);
  mActionBind->setEnabled(false);
  mActionBind->setToolTip("Connect Transaction Port");
  mActionBind->setStatusTip("Connect Transaction port to RTL");
  addAction(mActionBind);
  CQT_CONNECT(mActionBind, triggered(), this, actionBind());

  mActionAddParam = new QAction(QIcon(":/cmm/Resources/NewCardHS.png"), tr("Add Parameter..."), this);
  mActionAddParam->setToolTip("Add Parameter");
  mActionAddParam->setStatusTip("Add a new Parameter.");
  addAction(mActionAddParam);
  CQT_CONNECT(mActionAddParam, triggered(), this, actionAddParameter());
}

void CompWizardPortEditor::actionBind()
{
  QList<PortEditorTreeItem*> items;
  if (ui.treeWidget->fillSelectedItems(items, PortEditorTreeItem::XtorInst) > 0)
  {
    ESLXtorInstanceItem* theItem = dynamic_cast<ESLXtorInstanceItem*>(items.first());
    theItem->actionBind();
  }
}

void CompWizardPortEditor::actionDelete()
{
  deleteCurrent();
}


void CompWizardPortEditor::createMenus()
{
}

// Delegate

// Editor Delegate
PortEditorDelegate::PortEditorDelegate(QObject *parent, PortEditorTreeWidget* tw, CompWizardPortEditor* pe)
: QItemDelegate(parent), mTree(tw), mPortEditor(pe)
{
}


void PortEditorDelegate::currentIndexChanged(int)
{
  emit commitData(qobject_cast<QWidget*>(sender()));
  emit closeEditor(qobject_cast<QWidget*>(sender()));
}

QWidget *PortEditorDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                              const QModelIndex &index) const
{
  int col = index.column();

  QTreeWidgetItem* treeItem = mTree->currentItem();

  PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(treeItem);
  if (item == NULL)
    return NULL;
  
  QWidget* w = item->createEditor(this, parent, col);
  
  mTree->setEditorWidget(w);

  return w;
}


void PortEditorDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit)
  {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }

  QComboBox* cbox = qobject_cast<QComboBox*>(editor);
  if (cbox)
  {
    QString txt = index.model()->data(index, Qt::EditRole).toString();
    int ci = cbox->findText(txt);
    if (ci != -1)
        cbox->setCurrentIndex(ci);
    else // Unknown, add it on the fly and choose it.
    {
      cbox->addItem(txt);
      cbox->setCurrentIndex(cbox->findText(txt));
    }
    qDebug() << "set value" << txt;   
  }
}
void PortEditorDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                          const QModelIndex &index) const
{
  int col = index.column();

  QTreeWidgetItem* treeItem = mTree->currentItem();

  PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(treeItem);
  if (item == NULL)
    return;

  item->setModelData(this, model, index, editor, col);

  mTree->setEditorWidget(NULL);
}


