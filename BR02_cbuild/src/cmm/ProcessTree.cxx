//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include "gui/CQt.h"
#include "ProcessTree.h"

ProcessTree::ProcessTree(int parentPID) : mParentPID(parentPID)
{
}

void ProcessTree::findChildrenOf(int pid)
{
  QDir dir("/proc");

  if (dir.exists())
  {
    foreach (QFileInfo fi, dir.entryInfoList())
    {
      UtString statusFile;
      if (fi.isDir())
      {
        statusFile << fi.absoluteFilePath() << "/status";
        QFileInfo fiStatus(statusFile.c_str());
        if (fiStatus.exists())
        {
          UtString baseName;
          baseName << fi.baseName();
          int processPid = atoi(baseName.c_str());
          int parentPid = findParentPID(processPid);
          // qDebug() << "process: " << processPid << " has parent: " << parentPid;
          // we have a process whose parent is the one we are looking for
          if (pid == parentPid)
          {
            //qDebug() << "process: " << processPid << " has parent: " << parentPid;
            mPidList.append(processPid);
            findChildrenOf(processPid);
          }
        }
      }
    }
  }
}

const QList<int>& ProcessTree::childProcesses()
{
  mPidList.clear();
  findChildrenOf(mParentPID);
  return mPidList;
}

int ProcessTree::findParentPID(int processID)
{
  int pid = -1;
  UtString procFile;
  procFile << "/proc/" << processID;

#if pfLINUX || pfLINUX64
  procFile << "/status";
  QFile processFile(procFile.c_str());
  if (processFile.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    QTextStream in(&processFile);
    QString line = in.readLine();
    while (!line.isNull()) {
      QRegExp pidExp("PPid:\t([0-9]+)");
      if (pidExp.exactMatch(line))
      {
        UtString parentPid;
        parentPid << pidExp.cap(1);
        pid = atoi(parentPid.c_str());
        break;
      }
      line = in.readLine();
    }
    processFile.close();
  }
#endif

  return pid;
}


bool ProcessTree::isParentAlive()
{
  bool alive = false;
  UtString procFile;

  procFile << "/proc/" << mParentPID;
#if pfLINUX || pfLINUX64
  procFile << "/status";
  QFile processFile(procFile.c_str());
  if (!processFile.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    qDebug() << "no /proc file: " << procFile.c_str();  
    alive = false;
  }
  else
  {
    QTextStream in(&processFile);
    QString line = in.readLine();
    while (!line.isNull()) {
      QRegExp pidExp("PPid:\t([0-9]+)");
      if (pidExp.exactMatch(line))
      {
        alive = true;
        break;
      }
      line = in.readLine();
    }
    processFile.close();
  }
#endif

  return alive;
}
