#ifndef MKUWPAGESTART_H
#define MKUWPAGESTART_H


#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>
#include "WizardPage.h"
#include <QWizardPage>
#include "ui_MKUWPageStart.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"

class KitFile;
class RTLFileOptions;

class MKUWPageStart : public WizardPage
{
  Q_OBJECT

public:
  MKUWPageStart(QWidget *parent = 0);
  ~MKUWPageStart();

protected:
  virtual bool validatePage();
  virtual void initializePage();
  virtual bool isCommitPage () const { return true; }
  virtual int nextId() const;

  bool checkHashes();
  void saveSettings();
  void restoreSettings();
  bool checkKitHash(KitFile* kitFile, RTLFileOptions* rfu);


private:
  Ui::MKUWPageStartClass ui;
  QSettings mSettings;
  bool mIsValid;

private slots:
    void on_pushButtonBrowse_clicked();
};

#endif // MKUWPAGESTART_H
