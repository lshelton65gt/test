#ifndef __CARBONPROJECTNODES_H__
#define __CARBONPROJECTNODES_H__
#include "util/CarbonPlatform.h"
#include <QtGui>

#include "CarbonProject.h"
#include "CarbonProjectWidget.h"
#include "CarbonOptions.h"
#include "SettingsEditor.h"
#include "CarbonMakerContext.h"

class CarbonSourceGroup;
class CarbonHDLSourceFile;
class CarbonProperties;

class CarbonRootItem : public CarbonProjectTreeNode
{
public:
  CarbonRootItem(CarbonProjectWidget* parent) : CarbonProjectTreeNode(parent)
  {
    setIcon(0, QIcon(":/cmm/Resources/project.png"));
  }

  virtual void showContextMenu(const QPoint& point)
  {
    mProjectWidget->getContextMenuRoot()->exec(point);   
  }

  virtual void singleClicked(int)
  {
     mProjectWidget->actionProjectProperties();
  }
};

class CarbonModelItem : public CarbonProjectTreeNode
{
public:
  CarbonModelItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent) : CarbonProjectTreeNode(proj,parent) 
  {
    setIcon(0, QIcon(":/cmm/Resources/carbon.png"));
  }

  virtual void showContextMenu(const QPoint& point)
  {
     mProjectWidget->getContextMenuRoot()->exec(point);   
  }

  virtual void singleClicked(int)
  {
     mProjectWidget->compilerProperties();
  }
};

class CarbonGenFilesFolder : public CarbonProjectTreeNode
{
public:
  CarbonGenFilesFolder(CarbonProjectWidget* proj, QTreeWidgetItem* parent) : CarbonProjectTreeNode(proj,parent) 
  {
    setIcon(0, QIcon(":/cmm/Resources/folder-open-16x16.png"));
  }

  virtual void showContextMenu(const QPoint& /*point*/)
  {
    // mProjectWidget->getContextMenuRoot()->exec(point);   
  }

  virtual void singleClicked(int)
  {
    //mProjectWidget->actionProperties();
  }
};

class CarbonRTLFolder : public CarbonProjectTreeNode
{
public:
  CarbonRTLFolder(CarbonProjectWidget* proj, QTreeWidgetItem* parent) : CarbonProjectTreeNode(proj,parent) 
  {
    setIcon(0, QIcon(":/cmm/Resources/folder-open-16x16.png"));
  }

  virtual void showContextMenu(const QPoint& point)
  {
    mProjectWidget->getContextMenuRoot()->exec(point);
  }
 
  virtual void singleClicked(int)
  {
     mProjectWidget->compilerProperties();
  }

  virtual bool canDropItem(CarbonProjectTreeNode*);
};

class CarbonDatabaseFolder : public QObject, public CarbonProjectTreeNode
{
  Q_OBJECT

public:
  CarbonDatabaseFolder(CarbonProjectWidget* proj, QTreeWidgetItem* parent, const char* dbPath);
  virtual ~CarbonDatabaseFolder()
  {
    delete mOptions;
    delete mProperties;
  }
  virtual void showContextMenu(const QPoint& point);
  virtual void singleClicked(int);
  virtual bool canDropItem(CarbonProjectTreeNode*);
 
private slots:
  void configurationChanged(const char* name);
  void outputFilenameChanged(const CarbonProperty*, const char*);

private:
  CarbonOptions* mOptions;
  CarbonProperties* mProperties;
  UtString mFilePath;
};

class CarbonDirectivesFolder : public CarbonProjectTreeNode
{
public:
  CarbonDirectivesFolder(CarbonProjectWidget* proj, QTreeWidgetItem* parent) : CarbonProjectTreeNode(proj,parent) 
  {
    setIcon(0, QIcon(":/cmm/Resources/directives.png"));
  }
 
  virtual void showContextMenu(const QPoint& point)
  {
     mProjectWidget->getContextMenuRoot()->exec(point);   
  }

  virtual void singleClicked(int)
  {
     mProjectWidget->compilerDirectives();
     mProjectWidget->context()->getSettingsEditor()->setSettings(NULL);
  }

  virtual void doubleClicked(int)
  {
     mProjectWidget->compilerDirectives();
     mProjectWidget->context()->getSettingsEditor()->setSettings(NULL);
  }

  virtual bool canDropItem(CarbonProjectTreeNode*)
  {
    return false;
  }
  
};

class CarbonGeneratedVHMFolder : public CarbonProjectTreeNode
{
public:
  CarbonGeneratedVHMFolder(CarbonProjectWidget* proj, QTreeWidgetItem* parent) : CarbonProjectTreeNode(proj,parent) 
  {
    setIcon(0, QIcon(":/cmm/Resources/carbon.png"));
  }
  void putPath(const char* newVal) { mFilePath=newVal; }
  virtual void showContextMenu(const QPoint& /*point*/)
  {
    // mProjectWidget->getContextMenuRoot()->exec(point);   
  }

  virtual void doubleClicked(int)
  {
    mProjectWidget->openSource(mFilePath.c_str());
  }

private:
  UtString mFilePath;
};


class CarbonRemodelItem : public QObject, public CarbonProjectTreeNode 
{
  Q_OBJECT

public:
  CarbonRemodelItem(CarbonProjectWidget* proj, const char* filePath, QTreeWidgetItem* parent, const char* relFilePath);
  virtual void doubleClicked(int);

private:
  UtString mFilePath;
  UtString mRelPath;
};


class CarbonDirectivesItem : public QObject, public CarbonProjectTreeNode
{
  Q_OBJECT

public:
  CarbonDirectivesItem(CarbonProjectWidget* proj, const char* filePath, QTreeWidgetItem* parent, const char* relFilePath);
  virtual ~CarbonDirectivesItem();

  virtual void singleClicked(int);
  virtual void doubleClicked(int);
  const char* getPath() const { return mFilePath.c_str(); }
  virtual void showContextMenu(const QPoint& /*point*/);

private slots:
  void deleteFile();

private:
  UtString mFilePath;
  UtString mRelPath;
  CarbonProperties mProperties;
  CarbonOptions* mDefaultOptions;
  CarbonOptions* mOptions;
  QAction* actionDelete;
};

class CarbonSourceItem : public QObject, public CarbonProjectTreeNode
{
  Q_OBJECT

public:
  CarbonSourceItem(CarbonProjectWidget* proj, CarbonHDLSourceFile* src);
  CarbonSourceItem(CarbonProjectWidget* proj, CarbonHDLSourceFile* src, QTreeWidgetItem* parent);

  void updateIcon();

  virtual void singleClicked(int);
  virtual void doubleClicked(int);
  virtual void keyPressEvent(QKeyEvent*);
  virtual bool dragBeginEvent(QMouseEvent* e);
  virtual bool canDropItem(CarbonProjectTreeNode*);
  virtual void showContextMenu(const QPoint& point);
 
  void setData(const char* title, const char* filepath);

  CarbonHDLSourceFile* getSource() { return mSource; }

public slots:
  void deleteSource();
  void librarySource();
  void findFile();
 
private:
  QAction* actionDelete;
  QAction* actionFindFile;
  QAction* actionLibraryFile;
  UtString mFilePath;
  CarbonHDLSourceFile* mSource;
};

class CarbonVhdlItem : public CarbonSourceItem
{
public:
  CarbonVhdlItem(CarbonProjectWidget* parent, CarbonHDLSourceFile* src) : CarbonSourceItem(parent, src)
  {
  }
  CarbonVhdlItem(CarbonProjectWidget* proj, CarbonHDLSourceFile* src, QTreeWidgetItem* parent) : CarbonSourceItem(proj, src, parent)
  {
  }

  void indicateNotFound()
  {
    setIcon(0, QIcon(":/cmm/Resources/file-16x16missing.png"));
  }
};


class CarbonVerilogItem : public CarbonSourceItem
{
public:
  CarbonVerilogItem(CarbonProjectWidget* parent, CarbonHDLSourceFile* src) : CarbonSourceItem(parent, src)
  {
  }

  CarbonVerilogItem(CarbonProjectWidget* proj, CarbonHDLSourceFile* src, QTreeWidgetItem* parent) : CarbonSourceItem(proj, src, parent)
  {
  }
  void indicateNotFound()
  {
    setIcon(0, QIcon(":/cmm/Resources/file-16x16missing.png"));
  }
};

class CarbonCPPItem : public CarbonSourceItem
{
public:
  CarbonCPPItem(CarbonProjectWidget* parent, CarbonHDLSourceFile* src) : CarbonSourceItem(parent, src)
  {
  }

  CarbonCPPItem(CarbonProjectWidget* proj, CarbonHDLSourceFile* src, QTreeWidgetItem* parent) : CarbonSourceItem(proj, src, parent)
  {
  }
  void indicateNotFound()
  {
    setIcon(0, QIcon(":/cmm/Resources/file-16x16missing.png"));
  }
};

class CarbonHItem : public CarbonSourceItem
{
public:
  CarbonHItem(CarbonProjectWidget* parent, CarbonHDLSourceFile* src) : CarbonSourceItem(parent, src)
  {
  }

  CarbonHItem(CarbonProjectWidget* proj, CarbonHDLSourceFile* src, QTreeWidgetItem* parent) : CarbonSourceItem(proj, src, parent)
  {
  }
  void indicateNotFound()
  {
    setIcon(0, QIcon(":/cmm/Resources/file-16x16missing.png"));
  }
};

class CarbonHDLFolderItem : public QObject, public CarbonProjectTreeNode
{
  Q_OBJECT

public:
  CarbonHDLFolderItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, CarbonSourceGroup* group);
  virtual ~CarbonHDLFolderItem();

  virtual void showContextMenu(const QPoint& point);

  virtual void setOpenIcon()
  {
    setIcon(0, QIcon(":/cmm/Resources/folder-open-16x16.png"));
  }

  virtual void setClosedIcon()
  {
    setIcon(0, QIcon(":/cmm/Resources/folder-closed-16x16.png"));
  }

  virtual void singleClicked(int);
  virtual bool canDropItem(CarbonProjectTreeNode*);
  virtual bool dropItem(CarbonProjectTreeNode*);

  CarbonSourceGroup* getGroup() const { return mGroup; }
  CarbonOptions* getOptions();

  public slots:
    void deleteFolder();
    void compileLibrary();
    void cleanLibrary();

private:
  QAction* actionDelete;
  QAction* actionCompile;
  QAction* actionClean;
  CarbonSourceGroup* mGroup;
};

#endif

