//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QtGui>

#include "gui/CQt.h"

#include "util/XmlParsing.h"

#include "util/CarbonAssert.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"
#include "util/UtList.h"

#include "CarbonCompilerTool.h"
#include "CarbonProjectWidget.h"

bool CarbonPropertyInteger::parseXML(xmlNodePtr parent, UtXmlErrorHandler* /*eh*/)
{
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "range"))
    {
      UtString minText;
      XmlParsing::getProp(child, "minimum", &minText);
      int minValue=0;

      UtIStringStream newValMin(minText);
      if (newValMin >> UtIO::dec >> minValue)
        putMinimum(minValue);

      UtString maxText;
      XmlParsing::getProp(child, "maximum", &maxText);
      int maxValue=0;
      UtIStringStream newValMax(maxText);
      if (newValMax >> UtIO::dec >> maxValue)
        putMaximum(maxValue);
    }
  }

  return true;
}

bool CarbonPropertyInteger::readValueXML(xmlNodePtr parent,  CarbonOptions* options, UtXmlErrorHandler* /*eh*/) const
{
  for (xmlNode *child = parent->children; child != NULL; child = child->next) 
  {
    const char* element = XmlParsing::elementName(child);
    if (element == NULL) // Skip non-interesting XML things
      continue;

    if (0 == strcmp(element, "Value"))
    {
      UtString value;
      XmlParsing::getContent(child, &value);
      options->putValue(this, value.c_str());
    }
  }
  return true;}
bool CarbonPropertyInteger::writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* /*eh*/)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Option");
  xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST getName());

  xmlTextWriterStartElement(writer, BAD_CAST "Value");
  xmlTextWriterWriteString(writer, BAD_CAST value->getValue());
  xmlTextWriterEndElement(writer);

  xmlTextWriterEndElement(writer);
  return true;
}

CarbonDelegate* CarbonPropertyInteger::createDelegate(QObject* parent,  PropertyEditorDelegate* delegate, PropertyEditor* propEditor)
{
  return new CarbonDelegateInteger(parent, delegate, propEditor);
}


// Delegate

// Editor Delegate
CarbonDelegateInteger::CarbonDelegateInteger(QObject *parent, PropertyEditorDelegate* delegate, PropertyEditor* editor)
: CarbonDelegate(parent,delegate,editor)
{
}

void CarbonDelegateInteger::commitAndCloseEditor()
{
  QLineEdit *widget = qobject_cast<QLineEdit *>(sender());
  if (widget)
    mDelegate->commit(widget);
}

QWidget* CarbonDelegateInteger::createEditor(QWidget *parent, QTreeWidgetItem* /*item*/, CarbonProperty* prop, const QModelIndex& /*index*/) const
{
  const CarbonPropertyInteger* sw_integer = (const CarbonPropertyInteger*)prop;

  bool id,is;
  CarbonPropertyValue* sv = mEditor->getSettings()->getValue(prop,&id,&is);
  UtString value = sv->getValue();

  if (sw_integer->hasRange())
  {
    QSpinBox* sbox = new QSpinBox(parent);
    sbox->setMinimum(sw_integer->getMinimum());
    sbox->setMaximum(sw_integer->getMaximum());

    int iValue=0;
    UtIStringStream sValue(value.c_str());
    if (sValue >> UtIO::dec >> iValue)
      sbox->setValue(iValue);
    else
      sbox->setValue(0);

    return sbox;
  }
  else
  {
    QLineEdit* lineEdit = new QLineEdit(parent);
    lineEdit->setText(value.c_str());
    lineEdit->setValidator(new QIntValidator(lineEdit));
    connect(lineEdit, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
    return lineEdit;
  }
}

void CarbonDelegateInteger::setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const
{
  bool id,is;

  if (mEditor->getSettings() == NULL)
    return;

  CarbonPropertyValue* sv = mEditor->getSettings()->getValue(prop,&id,&is);
  if (sv == NULL)
    return;

 
  UtString currentValue = sv->getValue();


  QSpinBox* sbox = qobject_cast<QSpinBox *>(editor);
  if (sbox != NULL)
  {
    UtString value;
    value << sbox->value();

    QRegExp re(prop->getLegalPattern());
    if (prop->hasLegalPattern() && !re.exactMatch(value.c_str()))
    {
      QString msg = QString("The switch '%1' value of '%2' does not match the legal pattern: %3, ignoring.")
        .arg(prop->getName())
        .arg(value.c_str())
        .arg(prop->getLegalPattern());
      QMessageBox::warning(NULL, MODELSTUDIO_TITLE, msg);
      return;
    }

    if (prop->getRequired() && value.length() == 0)
    {
      QString msg = QString("The switch '%1' value must not be blank").arg(prop->getName());
      QMessageBox::warning(NULL, MODELSTUDIO_TITLE, msg);
      return;
    }

    setValue(sv, item, model, index, currentValue.c_str(), value.c_str(), prop);
  }
  else
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit *>(editor);
    if (lineEdit != NULL)
    {
      UtString value;
      value << lineEdit->text();

      QRegExp re(prop->getLegalPattern());
      if (prop->hasLegalPattern() && !re.exactMatch(value.c_str()))
      {
        QString msg = QString("The switch '%1' value of '%2' does not match the legal pattern: %3, ignoring.")
          .arg(prop->getName())
          .arg(value.c_str())
          .arg(prop->getLegalPattern());
        QMessageBox::warning(NULL, MODELSTUDIO_TITLE, msg);
        return;
      }
      
      if (prop->getRequired() && value.length() == 0)
      {
        QString msg = QString("The switch '%1' value must not be blank").arg(prop->getName());
        QMessageBox::warning(NULL, MODELSTUDIO_TITLE, msg);
        return;
      }

      setValue(sv, item, model, index, currentValue.c_str(), value.c_str(), prop);
    }
  }
}

