#ifndef DLGCODEEDITOR_H
#define DLGCODEEDITOR_H

#include <QtGui>
#include <Qsci/qscilexercpp.h>
#include <Qsci/qsciapis.h>
#include <Qsci/qscicommandset.h>
#include <Qsci/qscicommand.h>

#include "ui_DlgCodeEditor.h"

class DlgCodeEditor : public QDialog
{
  Q_OBJECT

public:
  DlgCodeEditor(QWidget *parent = 0, const QString& code = "");
  ~DlgCodeEditor();

  QString getCode()
  {
    QString code = ui.editor->text();
    if (code.isEmpty() || code.startsWith('\n'))
      return code;  
    else
      return QString("\n%1").arg(code);
  }

private:
  QString mCode;

private:
  Ui::DlgCodeEditorClass ui;

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
};

#endif // DLGCODEEDITOR_H
