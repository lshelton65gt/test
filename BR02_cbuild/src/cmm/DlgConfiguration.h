#ifndef DLGCONFIGURATION_H
#define DLGCONFIGURATION_H

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include <QDialog>
#include "ui_DlgConfiguration.h"
class CarbonProjectWidget;

class DlgConfiguration : public QDialog
{
  Q_OBJECT

public:
  DlgConfiguration(QWidget *parent = 0);
  ~DlgConfiguration();
  void setProject(CarbonProjectWidget* proj);

  const char* getFromConfiguration() const { return mFromConfig.c_str(); }
  const char* getToConfiguration() const { return mToConfig.c_str(); }

private slots:
  void on_okButton_clicked();
  void comboChanged(int);

private:
  bool validChoices();
  void enableButtons();

private:
  Ui::DlgConfigurationClass ui;
  CarbonProjectWidget* mProject;
  UtString mFromConfig;
  UtString mToConfig;

};

#endif // DLGCONFIGURATION_H
