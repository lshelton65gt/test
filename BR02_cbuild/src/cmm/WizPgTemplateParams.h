#ifndef WIZPGTEMPLATEPARAMS_H
#define WIZPGTEMPLATEPARAMS_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QWizardPage>
#include "WizardPage.h"
#include "ui_WizPgTemplateParams.h"

class WizardTemplate;

class WizPgTemplateParams : public WizardPage
{
  Q_OBJECT

public:
  WizPgTemplateParams(QWidget *parent = 0);
  ~WizPgTemplateParams();

  int nextId() const;

protected:
  virtual void initializePage();
  virtual bool validatePage();
  virtual bool isComplete() const;
  void populateModes(WizardTemplate* templ);
  bool hasAllRequiredParameters() const;
  virtual bool serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*);

private slots:
  void valuesChanged();

private:
  Ui::WizPgTemplateParamsClass ui;
};

#endif // WIZPGTEMPLATEPARAMS_H
