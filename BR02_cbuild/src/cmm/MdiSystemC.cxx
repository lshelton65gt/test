//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "MdiSystemC.h"
#include <QtGui>
#include <QMenuBar>
#include <QToolBar>
#include "gui/CQt.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "SettingsEditor.h"
#include "SystemCWizardWidget.h"

MDISystemCTemplate::MDISystemCTemplate(CarbonMakerContext* ctx)
: MDIDocumentTemplate("SystemC", "New SystemC Component", "") 
{
  mContext = ctx;  
}

void MDISystemCTemplate::updateMenusAndToolbars(QWidget* /*widget*/)
{
}

int MDISystemCTemplate::createToolbars()
{
  return 0;
}

int MDISystemCTemplate::createMenus()
{
  return 0;
}

MDIWidget* MDISystemCTemplate::createNewDocument(QWidget* /*parent*/)
{
  return NULL;
}

// We don't have a MDI document presence, only the docking window widget
// so we create the InvisibleProject here to handle menu & toolbar events
//
MDIWidget* MDISystemCTemplate::openDocument(QWidget* parent, const char* docName)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  SystemCWizardWidget* widget = new SystemCWizardWidget(mContext, this, parent);

  bool status = widget->loadFile(docName);

  QApplication::restoreOverrideCursor();

  if (status)
    return widget;
  else
  {
    widget->close();
    return NULL;
  }
}
