//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include <QtGui>
#include "CarbonProjectWidget.h"
#include "DlgConfiguration.h"

#define CHOICE_CHOOSE "<Choose...>"

DlgConfiguration::DlgConfiguration(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);

  QObject::connect(ui.okButton, SIGNAL(clicked()), this, SLOT(accept()));
  QObject::connect(ui.cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
}

DlgConfiguration::~DlgConfiguration()
{
}

void DlgConfiguration::enableButtons()
{
  ui.okButton->setEnabled(validChoices());
}

bool DlgConfiguration::validChoices()
{
  QString c1 = ui.comboBoxConfigFrom->currentText();
  QString c2 = ui.comboBoxConfigTo->currentText();

  bool result = c1.length() > 0 && c1.length() > 0 && 
      c1 != CHOICE_CHOOSE && 
      c2 != CHOICE_CHOOSE && c1 != c2;

  return result;
}
void DlgConfiguration::on_okButton_clicked()
{
  if (validChoices())
  {
    mFromConfig.clear();
    mFromConfig << ui.comboBoxConfigFrom->currentText();
    mToConfig.clear();
    mToConfig << ui.comboBoxConfigTo->currentText();

    setResult(QDialog::Accepted);
  }
  else
    setResult(QDialog::Rejected);
}

void DlgConfiguration::comboChanged(int)
{
  enableButtons();
}

void DlgConfiguration::setProject(CarbonProjectWidget* proj)
{
  mProject = proj;

  CarbonConfigurations* configs = mProject->project()->getConfigurations();

  ui.comboBoxConfigFrom->addItem(CHOICE_CHOOSE);
  ui.comboBoxConfigTo->addItem(CHOICE_CHOOSE);

  for (UInt32 i=0; i<configs->numConfigurations(); i++)
  {
    CarbonConfiguration* config = configs->getConfiguration(i);
    ui.comboBoxConfigFrom->addItem(config->getName());
    ui.comboBoxConfigTo->addItem(config->getName());
  }

  ui.comboBoxConfigFrom->setCurrentIndex(ui.comboBoxConfigFrom->findText(CHOICE_CHOOSE));
  ui.comboBoxConfigTo->setCurrentIndex(ui.comboBoxConfigTo->findText(CHOICE_CHOOSE));

  CQT_CONNECT(ui.comboBoxConfigFrom, currentIndexChanged(int), this, comboChanged(int));
  CQT_CONNECT(ui.comboBoxConfigTo, currentIndexChanged(int), this, comboChanged(int));  

  enableButtons();
}

