﻿These files are machine generated.

To Regenerate:
1. cd to src/cmm
2. modelstudio -noLog -squish -classGen -batch
3. that will create/update classGen sub-dir and all files.
4. after re-gen make sure to re-compile modelstudio

