﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace Carbon.Modelstudio
{
  public partial class Application
  {
    [DllImport("__Internal", EntryPoint = "CSharp_ScrApplicationProto_new_Application")]
    public extern static IntPtr CSharp_ScrApplicationProto_new_Application();

    public Application()
    {         
    }

    public static void Log(string format, params object[] args)
    {
      string line = string.Format(format, args);
      string message = string.Format("{0}: {1}", DateTime.Now, line);
     
      System.Console.WriteLine(message);
      Trace.WriteLine(message);

    }

    /// <summary>
    /// Gets the application object for balblal
    /// </summary>
    /// <returns></returns>
    public Application GetApplication()
    {
      IntPtr iApp = CSharp_ScrApplicationProto_new_Application();
      Application app = new Application(iApp, true);

      Log("WorkingDir: {0}", app.getWorkingDirectory());
      Log("Version: {0}", app.getSoftwareVersion());
      Log("IsUnix: {0}", app.getIsUnix());



      return app;
    }
  }
}
