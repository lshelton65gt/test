#ifndef ENUMCHOICEWIDGET_H
#define ENUMCHOICEWIDGET_H

#include "util/CarbonPlatform.h"

#include <QWidget>
#include "ui_EnumChoiceWidget.h"

class EnumChoiceWidget : public QWidget
{
    Q_OBJECT

public:
    EnumChoiceWidget(QWidget *parent = 0);
    ~EnumChoiceWidget();
    // insert a list of items into the table, if defaultIndex is set it will be selected first
    void setItems(const QStringList &items, int defaultIndex = -1);

    // set the default
    void setDefaultText(const QString &item);
    QString defaultText();

    // set/select the named item
    void setCurrentItem(const QString& current_text);
    // return the current set of items from the table
    QStringList getItems();

private slots:
    void on_pushButtonSetDefault_clicked();
   
	void moveCurrentItem(int delta);
    void on_tableWidget_currentItemChanged(QTableWidgetItem*,QTableWidgetItem*);
    void on_pushButtonDOWN_clicked();
    void on_pushButtonUP_clicked();
    void on_Add_Item_clicked();
  	void on_Delete_Item_clicked();
private:
    void setDefaultItem(QTableWidgetItem* item);
    Ui::EnumChoiceWidgetClass ui;
    QTableWidgetItem* mDefaultItem;
    QStringList mItems;
};

#endif // ENUMCHOICEWIDGET_H
