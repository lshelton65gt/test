#ifndef __MVCOMPONENT_H_
#define __MVCOMPONENT_H_

#include "util/CarbonPlatform.h"
#include <QObject>

#include "util/XmlParsing.h"
#include "CarbonComponent.h"
#include "util/UtString.h"
#include "RemoteConsole.h"
#include "CarbonConsole.h"

class CarbonOptions;
class CarbonProperties;
class CarbonProjectWidget;
class CarbonConfigurations;
class CarbonConfiguration;
class MVTreeItem;
class MVXmlItem;
class MVSourceItem;

class MVComponent : public CarbonComponent
{
  Q_OBJECT

public:
  MVComponent(CarbonProject* proj);
  virtual ~MVComponent();

  // Overrides
  static MVComponent* deserialize(CarbonProject* proj, xmlNodePtr parent, UtXmlErrorHandler* eh);
  virtual bool serialize(xmlTextWriterPtr writer);
  virtual const char* getTargetName(CarbonConfiguration*,CarbonComponent::TargetType type, TargetPlatform=Unix);
  virtual bool getTargetImpl(CarbonConfiguration*,QTextStream& stream, TargetType type);
  virtual void generateMakefile(CarbonConfiguration*,const char* makefileName);
  virtual CarbonComponentTreeItem* createTreeItem(CarbonProjectWidget*, QTreeWidgetItem*);
  virtual const char* getOutputDirectory(const char* configName=NULL);
  virtual bool canCompile(CarbonConfiguration*) { return true; }
  virtual const char* getWizardFilename();
  CarbonOptions* getOptions(const char* configName=NULL);
  CarbonConfigurations* getConfigurations() { return mConfigs; }
  void updateTreeIcons();
  MVTreeItem* getRootItem() const { return mRootItem; }
  const char* getDocumentPath();
  virtual bool checkComponent(CarbonConsole* console);
  void icheckComponent();
  QString getXMLFilename(const char* configName=NULL);
  virtual int getGeneratedFiles(QStringList&);

private:
  static void createDirectory(CarbonConfiguration*);
  void renameDirectory(const char* oldName, const char* newName);
  static void createConfigurations(MVComponent* comp, xmlNodePtr parent);
  void updateTopModuleName();
  const char* getLanguageExtension();
  void updateTreeTextValues();

private slots:
  void configurationChanged(const char*);
  void compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void configurationRemoved(const char*);
  void configurationRenamed(const char*, const char*);
  void configurationCopy(const char*, const char*);
  void configurationAdded(const char* name);
  void compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void outputFilenameChanged(const CarbonProperty*, const char*);
  void projectLoaded(const char* fileName, CarbonProject*);
  void projectClosing(CarbonProject*);

private:
  CarbonConfigurations* mConfigs;
  UtString mOutputDir;
  UtString mTargetName;
  CarbonConfiguration* mActiveConfiguration;
  MVTreeItem* mRootItem;
  UtString mDocPath;
  UtString mIODBName;
  UtString mWizardFileName;
  CarbonProject* mProject;
  
  MVXmlItem* mXmlItem;
  MVSourceItem* mRtlFile;
  MVSourceItem* mCFile;

  // The list of nets
  QList<QString> mNetLocators;
};


class MVTreeItem : public CarbonComponentTreeItem
{
  Q_OBJECT

public:
  MVTreeItem(CarbonProjectWidget* proj, MVComponent* comp);
  MVTreeItem(CarbonProjectWidget* proj, MVComponent* comp, QTreeWidgetItem* parent);

  virtual void showContextMenu(const QPoint&);
  virtual void singleClicked(int);

private:
  void createActions();

  private slots:
    void compileComponent();
    void deleteComponent();
    void cleanComponent();
    void icheckComponent();

private:
  QAction* mActionClean;
  QAction* mActionCompile;
  QAction* mActionDelete;
  QAction* mActionCheck;
};

class MVSourceItem : public CarbonProjectTreeNode
{
public:
  MVSourceItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, MVComponent* comp) : CarbonProjectTreeNode(proj,parent) 
  {
    setIcon(0, QIcon(":/cmm/Resources/file-generated-16x16.png"));
    mComp = comp;
  }
  virtual void doubleClicked(int);
  void setExists(bool value)
  {
    if (value)
      setIcon(0, QIcon(":/cmm/Resources/file-generated-16x16.png"));
    else
      setIcon(0, QIcon(":/cmm/Resources/file-16x16missing.png"));
  }
private:
  MVComponent* mComp;
};

class MVXmlItem : public CarbonProjectTreeNode
{
public:
  MVXmlItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, MVComponent* comp);
  virtual void doubleClicked(int);
  virtual void showContextMenu(const QPoint&);
  virtual void singleClicked(int);

private:
  MVComponent* mComp;
};

#endif
