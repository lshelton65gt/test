//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include <QDebug>
#include <QDir>

#include "gui/CQt.h"

#include "CarbonComponent.h"
#include "CodeGenerator.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"

#include "cfg/carbon_cfg.h"
#include "cfg/carbon_cfg_misc.h"
#include "cfg/CarbonCfg.h"

#include "DlgConfirmDelete.h"
#include "InfoTableWidget.h"
#include "ErrorInfoWidget.h"

#include "MaxsimWizardWidget.h"
#include "CowareWizardWidget.h"
#include "SystemCWizardWidget.h"
#include "MVWizardWidget.h"
#include "WorkspaceModelMaker.h"

CarbonComponent::CarbonComponent(CarbonProject* proj, const char* name) : QObject(NULL)
{
  mModelKitComponent = false;
  mName = name;
  mProject = proj;
}

CarbonComponent::~CarbonComponent()
{
}

CarbonComponent::ComponentType CarbonComponent::getType() const
{
  QString name = getName();
  
  if (name == "MaxsimComponent")
    return CarbonComponent::SoCDesigner;
  else if (name == "CowareComponent")
    return CarbonComponent::CoWare;
  else if (name == "MVComponent")
    return CarbonComponent::ModelValidation;
  else if (name == "PackageComponent")
    return CarbonComponent::Packaging;
  else
    return CarbonComponent::UnknownComponent;
}


CarbonComponentTreeItem* CarbonComponent::createTreeItem(CarbonProjectWidget* /*tree*/, QTreeWidgetItem* /*parent*/)
{
  return NULL;
}

bool CarbonComponent::serialize(xmlTextWriterPtr /*writer*/)
{
  return true;
}


CarbonComponentTreeItem::CarbonComponentTreeItem(CarbonProjectWidget* proj, QTreeWidgetItem* parent, CarbonComponent* comp) : CarbonProjectTreeNode(proj, parent)
{
  mComponent = comp;
}

CarbonComponentTreeItem::~CarbonComponentTreeItem()
{
}

const char* CarbonComponent::getOutputDirectory(const char*)
{
  INFO_ASSERT(false, "Override missing");
  return NULL;
}

const char* CarbonComponent::getComponentDirectory() const
{
  INFO_ASSERT(false, "Override missing");
  return NULL;
}

// The makefile will barf so bad if there's no .ccfg file that it
// won't even be able to clean.  So if there's no .ccfg file, we
// generate a makefile that doesn't attempt to build components.
// It would probably be better to make the makefile a little smarter
// than to do this.
bool CarbonComponent::canCompile(CarbonConfiguration* cfg)
{
  QString iodbName = getProject()->getConfigDatabaseFilePath(cfg->getName());
  QFileInfo fiIodb(iodbName);

  UtString ccfgPath;
  UtString ccfgName;
  ccfgName << fiIodb.baseName() << ".ccfg";

  OSConstructFilePath(&ccfgName, getOutputDirectory(cfg->getName()), ccfgName.c_str());

  QFileInfo fiCcfg(ccfgName.c_str());

  bool status = fiCcfg.exists();
  
  return status;
}

void CarbonComponent::copyAndRenameCcfg(const char* outputDir, const char* oldCcfgName, const char* newCcfgName)
{
  // if we renamed libdesign.a to libdesign.lib, no change
  // is needed here, so skip it.
  if (0 == strcmp(oldCcfgName, newCcfgName))
    return;

  TempChangeDirectory cd(outputDir);

  QFileInfo fi1(oldCcfgName);
  if (!fi1.exists())
    return;

  UtString errInfo;
  if (OSCopyFile(oldCcfgName, newCcfgName, &errInfo))
  {
    CarbonCfgID cfg = carbonCfgCreate();
    CarbonCfgStatus readStatus = carbonCfgRead(cfg, newCcfgName);  
    if (readStatus == eCarbonCfgSuccess)
    {
      QString iodbName = getProject()->getDatabaseFilePath();
      QFileInfo fi(iodbName);
      UtString relIODBName;
      UtString dbname;
      dbname << iodbName;
      relIODBName << CarbonProjectWidget::makeRelativePath(outputDir, dbname.c_str());
      carbonCfgPutIODBFile(cfg, relIODBName.c_str());

      QFileInfo fiLib(carbonCfgGetLibName(cfg));
      UtString newLibName;
      UtString newSuffix;

      if (fi.suffix() == "a" || fi.suffix() == "so")
        newSuffix << fiLib.suffix();
      else
        newSuffix << "dll";

      newLibName << fiLib.path() << "/" << fi.baseName() << "." << newSuffix.c_str();

      UtString relLIBName;
      relLIBName << CarbonProjectWidget::makeRelativePath(outputDir, newLibName.c_str());

      carbonCfgPutLibName(cfg, relLIBName.c_str());

      carbonCfgWrite(cfg, newCcfgName);
      carbonCfgDestroy(cfg);
    }
    else
    {
      UtString msg;
      msg << "Failed to read: " << oldCcfgName << "\nError: " << carbonCfgGetErrmsg(cfg);
      QMessageBox::warning(getProject()->getProjectWidget(), MODELSTUDIO_TITLE, msg.c_str());
    }
  }
  else
  {
    UtString msg;
    msg << "Failed to copy file: " << oldCcfgName << " to " << newCcfgName;
    QMessageBox::warning(getProject()->getProjectWidget(), MODELSTUDIO_TITLE, msg.c_str());
  }
}

void CarbonComponentTreeItem::deleteComponent(const char* outputDir, ComponentType compType, bool interactive, DlgConfirmDelete::ConfirmResult confirmResult)
{
  QDir dir(outputDir);
  if (dir.exists())
  {
    UtString delMsg;
    delMsg << "'" << outputDir << "'";

    DlgConfirmDelete::ConfirmResult result = confirmResult;

    if (interactive)
    {
      DlgConfirmDelete dlg(mProjectWidget, delMsg.c_str(), delMsg.c_str());
      result = (DlgConfirmDelete::ConfirmResult)dlg.exec();
    }

    if (result != DlgConfirmDelete::Cancel)
    {
      QWorkspace* ws = mProjectWidget->context()->getWorkspace();
      foreach (QWidget* window, ws->windowList()) 
      {
        MDIWidget* mdiWidget = dynamic_cast<MDIWidget*>(window);
        if (mdiWidget)
        {
          switch (compType)
          {
          case CarbonComponentTreeItem::Maxsim:
            {
              MaxsimWizardWidget* wizard = dynamic_cast<MaxsimWizardWidget*>(mdiWidget);
              if (wizard)
              {
                if (!wizard->close()) // User cancelled delete
                  return;
              }
            }
            break;
           case CarbonComponentTreeItem::SystemC:
            {
              SystemCWizardWidget* wizard = dynamic_cast<SystemCWizardWidget*>(mdiWidget);
              if (wizard)
              {
                if (!wizard->close()) // User cancelled delete
                  return;
              }
            }
            break;
           case CarbonComponentTreeItem::CoWare:
            {
              CowareWizardWidget* wizard = dynamic_cast<CowareWizardWidget*>(mdiWidget);
              if (wizard)
              {
                if (!wizard->close()) // User cancelled delete
                  return;
              }
            }
            break;
            case CarbonComponentTreeItem::ModelValidation:
            {
              MVWizardWidget* wizard = dynamic_cast<MVWizardWidget*>(mdiWidget);
              if (wizard)
              {
                if (!wizard->close()) // User cancelled delete
                  return;
              }
            }
            break;
          }
        }
      }
      int index = mProjectWidget->indexOfTopLevelItem(this);
      mProjectWidget->takeTopLevelItem(index);
      mProjectWidget->deleteComponent(mComponent);
    }
    
    if (result == DlgConfirmDelete::Delete)
    {
      CarbonProjectWidget::removeDirectoryAndFiles(outputDir);
    }

  }
  mProjectWidget->updateContextMenuActions();
}

bool CarbonComponent::checkForCcfgErrors(QStringList& errorList)
{
  qDebug() << "Checking for ccfg errors";

  bool errorsFound = false;
  
  CarbonMakerContext* ctx = getProject()->getProjectWidget()->context();
  QDockWidget* dw = ctx->getWorkspaceModelMaker()->getErrorInfoDockWindow();
  ErrorInfoWidget* errInfo = dynamic_cast<ErrorInfoWidget*>(dw->widget());
  if (errInfo)
  {
    InfoTableWidget* infoWidget = errInfo->getInfoWidget();
    foreach(ErrorMessage* em, infoWidget->getErrorMessages())
    {
      if (em->getMessageNumber() >= carbonCfgMESSAGE_BASE && em->getMessageNumber() <= carbonCfgMESSAGE_LAST)
      {
        errorList.push_back(em->getMessage());
        infoWidget->removeMessage(em);

        // For now only report certain messages as errors, so we don't pop up the "inconsistency window" for
        // errors/warnings that cannot be fixed by reloading the ccfg.
        UInt32 msgNum = em->getMessageNumber();
        if( (msgNum == carbonCfgERR_NOCARBONSIGNAL) ||
            (msgNum == carbonCfgERR_NOCARBONPORT) ||
            (msgNum == carbonCfgERR_NOTRANSPORT) ||
            (msgNum == carbonCfgERR_NEWCARBONPORT) ||
            (msgNum == carbonCfgERR_PORTCHANGED) )
        errorsFound = true;
      }
    }
  }

  return errorsFound;
}

const char* CarbonComponent::getWizardFilename()
{
  static UtString name;
  name.clear();

  QString iodbName = getProject()->getDatabaseFilePath();
  QFileInfo fi(iodbName);

  UtString baseName;
  baseName << fi.baseName() << ".ccfg";

  OSConstructFilePath(&name, getOutputDirectory(), baseName.c_str());
 
  return name.c_str();
}
