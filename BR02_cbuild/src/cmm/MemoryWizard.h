#ifndef MEMORYWIZARD_H
#define MEMORYWIZARD_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QtGui>

#include <QWizard>
#include <QWizardPage>
#include <QtXml>
#include "Scripting.h"
#include "WizardTemplate.h"
#include "ui_MemoryWizard.h"

class Template;
class MWPortConfigurations;

class ModuleItem
{
public:
  ModuleItem(const QString& name, const QString& locator)
  {
    mModuleName = name;
    mLocator = locator;
  }

  QString getName() const { return mModuleName; }
  QString getLocator() const { return mLocator; }

private:
  QString mModuleName;
  QString mLocator;
};



//<PortConfigurations version="1">
//  <Ports>
//    <Port name="portNameA" functionClass="InputVector" defaultFunction="FuncNameA"/>
//    <Port name="portNameB" functionClass="OutputVector" defaultFunction="FuncNameB"/>
//  </Ports>
//
//  <Functions>
//    <FunctionClass name="InputVector">
//      <Function name="FuncNameA" displayName="Fancy Name A" description="The description of fancy name A here"/>
//    </FunctionClass>
//    <FunctionClass name="OutputVector">
//      <Function name="FuncNameB" displayName="Fancy Name B" description="The description of fancy name B here"/>
//    </FunctionClass>
//  </Functions>
//</PortConfigurations>

class MWFunctionClass;

class MWModuleParamFunction
{
public:
  MWModuleParamFunction() {}
 
  QString getName() const { return mName; }
  void setName(const QString& newVal) { mName=newVal; }
  
  QString getParameter() const { return mParameter; }
  void setParameter(const QString& newVal) { mParameter=newVal; }
 
  bool deserialize(const QDomElement& e, XmlErrorHandler*);

private:
  QString mName;
  QString mParameter;
};

class MWModuleParam
{
public:
  MWModuleParam() {}

  QString getName() const { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

  bool deserialize(const QDomElement& e, XmlErrorHandler*);

private:
  QString mName;
};

class MWPort
{
public:
  MWPort()
  {
    mPortIndex=0;
  }
  bool deserialize(const QDomElement& e, XmlErrorHandler*);

  QString getName() const { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

  QString getFunctionClass() const { return mFunctionClassName; }
  void setFunctionClass(const QString& newVal) { mFunctionClassName=newVal; }

  QString getDefaultFunctionName() const { return mDefaultFunctionName; }
  void setDefaultFunctionName(const QString& newVal) { mDefaultFunctionName=newVal; }

  int getPortIndex() const { return mPortIndex; }
  void setPortIndex(int newVal) { mPortIndex=newVal; }

private:
  int mPortIndex;
  QString mName;
  QString mFunctionClassName;
  QString mDefaultFunctionName;
};

class MWFunctionClass;

class MWFunction
{
public:
  MWFunction(MWFunctionClass* funcClass)
  {
    mFunctionClass = funcClass;
  }
  bool deserialize(const QDomElement& e, XmlErrorHandler*);

  QString getName() const { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

  QString getDisplayName() const { return mDisplayName; }
  void setDisplayName(const QString& newVal) { mDisplayName=newVal; }

  QString getDescription() const { return mDescription; }
  void setDescription(const QString& newVal) { mDescription=newVal; }

  MWFunctionClass* getFunctionClass() const { return mFunctionClass; }

private:
  QString mName;
  QString mDisplayName;
  QString mDescription;
  MWFunctionClass* mFunctionClass;
};

class MWFunctionClass
{
public:
  MWFunctionClass()
  {
  }
  bool deserialize(const QDomElement& e, XmlErrorHandler*);

  QString getName() const { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

  int numFunctions() const { return mFuncs.count(); }
  MWFunction* getFunc(int index) { return mFuncs[index]; }
  MWFunction* findFunction(const QString& name) 
  {
    foreach (MWFunction* func, mFuncs)
    {
      if (name == func->getName())
        return func;
    }
    return NULL;
  }
private:
  QList<MWFunction*> mFuncs;
  QString mName;
};

class MWPortConfigurations
{
public:
  MWPortConfigurations()
  {
  }
  XmlErrorHandler* parseFile(const QString& file);

  MWFunctionClass* getFunctionClass(const QString& name)
  {
    foreach (MWFunctionClass* fc, mFuncClasses)
    {
      if (name == fc->getName())
        return fc;
    }
    return NULL;
  }

  MWPort* getPort(const QString& name)
  {
    foreach (MWPort* port, mPorts)
    {
      if (name == port->getName())
        return port;
    }
    return NULL;
  }

  const QList<MWPort*>& getPorts() const { return mPorts; }
  const QList<MWFunctionClass*>& getFunctionClasses() const { return mFuncClasses; }
  const QList<MWModuleParamFunction*>& getModuleParameterFunctions() const { return mModuleParamFunctions; }
  const QList<MWModuleParam*>& getModuleParameters() const { return mModuleParams; }

private:
  bool deserializePorts(const QDomElement& e, XmlErrorHandler*);
  bool deserializeFunctions(const QDomElement& e, XmlErrorHandler*);
  bool deserializeModuleParameters(const QDomElement& e, XmlErrorHandler*);
  bool deserializeModuleParameterFunctions(const QDomElement& e, XmlErrorHandler*);
  bool deserializeModuleParametersParams(const QDomElement& e, XmlErrorHandler*);
  
private:
  int mVersion;
  XmlErrorHandler mErrorHandler;
  QList<MWFunctionClass*> mFuncClasses;
  QList<MWPort*> mPorts;
  QList<MWModuleParamFunction*> mModuleParamFunctions;
  QList<MWModuleParam*> mModuleParams;
};


struct MWPortMap
{
  MWPortMap(const QString& portName, const QString& portFunc, int portNumber, const QString& funcClass)
  {
    mPortName=portName;
    mPortNumber=portNumber;
    mPortFunction=portFunc;
    mFunctionClass=funcClass;
  }
  
  QString getFunctionClass() { return mFunctionClass; }
  QString getPortName() { return mPortName; }
  QString getPortFunction() { return mPortFunction; }
  int getPortNumber() { return mPortNumber; }

  int mPortNumber;
  QString mPortName;
  QString mPortFunction;
  QString mFunctionClass;
};

struct MWParamMap
{
  MWParamMap(const QString& paramName, const QString& paramFunc)
  {
    mParamName=paramName;
    mParamFunc=paramFunc;
  }
  
  QString getParamName() { return mParamName; }
  QString getParamFunction() { return mParamFunc; }

  QString mParamName;
  QString mParamFunc;
};

class MemoryWizard : public QWizard
{
  Q_OBJECT

public:
  enum { PageStart, PageTemplates, PageParameters, PageModeParameters, PageConfigurations, PageUserPorts, PagePorts, PageCodeGenerate, PageFinished, PageStartMulti, PageMultiPortMap, PageMultiParamMap, PageMultiGenerate, PageMultiFinished } ;

  enum MemoryWizardMode { eWizardModeSingle, eWizardModeMulti };

  MemoryWizard(QWidget *parent = 0);
  void setTemplate(WizardTemplate* templ) { mTemplate=templ; }
  WizardTemplate* getTemplate() 
  {
    return mTemplate; 
  }
  void replayFile(const QString& replayFile);
  QString getReplayFile() { return mReplayFile; }
  QDomElement findWizardStep(const QString stepName);
  void postNext();
  void postRestart();
  void stopReplay() { mReplayFile = ""; }
  MemoryWizardMode getMode() const { return mMode; }
  void setMode(MemoryWizardMode newVal) { mMode=newVal; }
  void removeAllModules() { mModuleList.clear(); }
  void removeAllPortMaps() { mPortMappings.clear(); }
  void removeAllParamMaps() { mParamMappings.clear(); }
  void addPortMapping(const QString portName, const QString portFunc, int portIndex, const QString funcClass)
  {
    MWPortMap* pm = new MWPortMap(portName, portFunc, portIndex, funcClass);
    mPortMappings.append(pm);
  }

  void addParamMapping(const QString& paramName, const QString& paramFunc)
  {
    MWParamMap* pm = new MWParamMap(paramName, paramFunc);
    mParamMappings.append(pm);
  }

  void addModuleItem(ModuleItem* item) { mModuleList.append(item); }
  int numModules() { return mModuleList.count(); }
  ModuleItem* getModule(int index) { return mModuleList[index]; }
  bool serializeModulesFile(const QString& fileName);
  MWPortConfigurations* getPortConfigurations() { return &mPortConfigurations; }
  QVariant getOption(const QString& name) { return mOptions[name]; }
  void setOption(const QString& name, const QVariant& value) { mOptions[name] = value; }
  int numPortMaps() { return mPortMappings.count(); }
  int numParamMaps() { return mParamMappings.count(); }
  MWPortMap* getPortMap(int i) { return mPortMappings[i]; }
  MWParamMap* getParamMap(int i) { return mParamMappings[i]; }

private slots:
  void firstPage();

private:
  WizardTemplate* mTemplate;
  Ui::MemoryWizardClass ui;
  QString mReplayFile;
  MemoryWizardMode mMode;
  QList<ModuleItem*> mModuleList;
  QList<MWPortMap*> mPortMappings;
  QList<MWParamMap*> mParamMappings;
  MWPortConfigurations mPortConfigurations;
  QMap<QString, QVariant> mOptions;
};

#endif // MEMORYWIZARD_H
