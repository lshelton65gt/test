#ifndef __CCFGHELPER__
#define __CCFGHELPER__

#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"
#include "util/UtHashSet.h"

#include "util/CExprParse.h"

#include "CompWizardPortEditor.h"
#include "CompWizardTreeNodes.h"
#include "PortEditorCommands.h"
#include "Validators.h"
#include "cmm.h"

class CcfgHelper
{
public:  
  CcfgHelper(CarbonCfg* cfg)
  {
    mCfg = cfg;
  }
  CarbonCfgXtorInstance* addXtor(const QString& xtorName, CarbonCfgXtor* xtor)
  {
    UtString n; n << xtorName;
    return mCfg->addXtor(n.c_str(), xtor);
  }

  CarbonCfg* findComponent(const QString& name)
  {
    if (name == mCfg->getCompName())
      return mCfg;
    else
      return findSubComponent(name);
  }

  CarbonCfg* findSubComponent(const QString& name)
  {
    for(UInt32 i=0; i<mCfg->numSubComponents(); i++)
    {
      CarbonCfg* subComp = mCfg->getSubComponent(i);
      if (name == subComp->getCompName())
        return subComp;
    }
    return NULL;
  }
  CarbonCfgXtorConn* findXtorPort(CarbonCfgXtorInstance* xtorInst, const QString& portName)
  {
    for (UInt32 c=0; c<xtorInst->getType()->numPorts(); c++)
    {
      CarbonCfgXtorConn* xtorConn = xtorInst->getConnection(c);
      if (portName == xtorConn->getPort()->getName())
      {
        return xtorConn;
      }
    } 
    return NULL;
  }

  CarbonCfgXtor* findXtor(const QString& xtorName, const QString& xtorLibName, const QString& xtorVariant)
  {
    UtString n; n << xtorName;
    UtString l; l << xtorLibName;
    UtString v; v << xtorVariant;
    return mCfg->findXtor(n.c_str(), l.c_str(), v.c_str());
  }

  CarbonCfgESLPort* findESLPort(CarbonCfgRTLPort* rtlPort)
  {
    for (UInt32 i=0; i<rtlPort->numConnections(); i++)
    {
      CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
      if (conn->castESLPort())
        return conn->castESLPort();
    }
    return NULL;
  }

  CarbonCfgRTLPort* findRTLPort(const QString& rtlPortName)
  {
    UtString n; n << rtlPortName;
    return mCfg->findRTLPort(n.c_str());
  }

  CarbonCfgESLPort* findESLPort(const QString& eslPortName)
  {
    UtString n; n << eslPortName;
    return mCfg->findESLPort(n.c_str());
  }

  CarbonCfgXtorInstance* findXtorInstance(const QString& xtorInstName)
  {
    UtString n; n << xtorInstName;
    return mCfg->findXtorInstance(n.c_str());
  }

  CarbonCfgMemory* findMemory(const QString& memName)
  {
    for (UInt32 i=0; i<mCfg->numMemories(); i++)
    {
      CarbonCfgMemory* mem = mCfg->getMemory(i);
      if (memName == mem->getName())
        return mem;
    }
    return NULL;
  }

  CarbonCfgMemoryBlock* findMemoryBlock(CarbonCfgMemory* mem, const QString& blockName)
  {
    for (UInt32 i=0; i<mem->numMemoryBlocks(); i++)
    {
      CarbonCfgMemoryBlock* block = mem->getMemoryBlock(i);
      if (blockName == block->getName())
        return block;
    }
    return NULL;

  }

  CarbonCfgMemoryLocRTL* findMemoryBlockLocationRTLFromIndex(CarbonCfgMemoryBlock* block, UInt32 index)
  {
    UInt32 li = 0;
    for (UInt32 i=0; i<block->numLocs(); i++)
    {
      if (block->getLoc(i)->castRTL())
      {  
        CarbonCfgMemoryLocRTL* l = block->getLoc(i)->castRTL();
        if (li == index)
          return l;
        li++;
      }
    }
    return NULL;
  }


  CarbonCfgMemoryLoc* findMemoryBlockLocationFromIndex(CarbonCfgMemoryBlock* block, UInt32 index)
  {
   for (UInt32 i=0; i<block->numLocs(); i++)
    {
      CarbonCfgMemoryLoc* l = block->getLoc(i);
      if (i == index)
        return l;
    }
   return NULL;
  }

  int findMemoryBlockLocationRTLIndex(CarbonCfgMemoryBlock* block, CarbonCfgMemoryLocRTL* loc)
  {
    UInt32 li = 0;
    for (UInt32 i=0; i<block->numLocs(); i++)
    {
      if (block->getLoc(i)->castRTL())
      {
        CarbonCfgMemoryLocRTL* l = block->getLoc(i)->castRTL();
        if (loc == l)
          return li;
        li++;
      }
    }
    return -1;
  }


  int findMemoryBlockLocationIndex(CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc)
  {
    for (UInt32 i=0; i<block->numLocs(); i++)
    {
      CarbonCfgMemoryLoc* l = block->getLoc(i);
      if (loc == l)
        return i;
    }
    return -1;
  }



  CarbonCfgXtorParamInst* findParameter(const QString& paramName)
  {
    for (UInt32 i=0; i<mCfg->numParams(); i++)
    {
      CarbonCfgXtorParamInst* paramInst = mCfg->getParam(i);
      if (paramName == paramInst->getParam()->getName())
        return paramInst;
    }
    return NULL;
  }

  CarbonCfgXtorParamInst* addParam(CarbonCfgParamDataType type, 
        const QString& name, const QString& value,
        const QString& description, CarbonCfgParamFlag scope, const QString& enumChoices)
  {
    UtString uName; uName << name;
    UtString uValue; uValue << value;
    UtString uDescr; uDescr << description;
    UtString uEnumChoices; uEnumChoices << enumChoices;
    return mCfg->addParam(uName.c_str(), type, uValue.c_str(), scope, uDescr.c_str(), uEnumChoices.c_str());
  }

  void disconnect(CarbonCfgRTLConnection* conn)
  {
    mCfg->disconnect(conn);
  }

  void disconnect(CarbonCfgRTLPort* rtlPort)
  {
    for (UInt32 i=0; i<rtlPort->numConnections(); i++)
    {
      CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
      mCfg->disconnect(conn);  
    }
  }

private:
  CarbonCfg* mCfg;
};

#endif
