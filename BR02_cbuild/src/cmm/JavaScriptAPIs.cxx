//! -*-C++-*-
/***************************************************************************************
Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 

THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QtScript/QScriptValueIterator>

#include "JavaScriptAPIs.h"

#include "ScriptingEngine.h"

#include "CarbonConsole.h"
#include "DesignXML.h"
#include "ScrProject.h"
#include "ScrCcfg.h"
#include "ScrCarbonDB.h"
#include "ScrApplication.h"
#include "ScrPlugin.h"
#include "SpiritXML.h"
#include "Spirit14.h"
#include "KitManifest.h"
#include "MaxsimComponent.h"
#include "CowareComponent.h"
#include "SystemCComponent.h"
#include "ModelKit.h"
#include "CarbonOptions.h"
#include "cmm.h"

// static members
bool JavaScriptAPIs::mLoadedAPIs = false;
QMap<QString, const QMetaObject*> JavaScriptAPIs::mTypeMap;
QMap<QString, const QMetaObject*> JavaScriptAPIs::mMapVarToAPI;
QList<const QMetaObject*> JavaScriptAPIs::mInternalClasses;
QStringList JavaScriptAPIs::mVarsMapList;

LoadAPIThread::~LoadAPIThread()
{
  if (mAPIs)
    delete mAPIs;
  qDebug() << "Finished API Thread";
}

void LoadAPIThread::run()
{
  QsciLexerJavaScript* lexer = new QsciLexerJavaScript();
  mAPIs = new JavaScriptAPIs(lexer);

  CarbonOptions::registerIntellisense(mAPIs);
  ModelKit::registerIntellisense(mAPIs);
  KitUserContext::registerIntellisense(mAPIs);
  KitTest::registerIntellisense(mAPIs);
  KitManifest::registerIntellisense(mAPIs);
  ScrProject::registerIntellisense(mAPIs);
  ScrCcfg::registerIntellisense(mAPIs);
  SpiritXML::registerIntellisense(mAPIs);
  SpiritXML_1_4::registerIntellisense(mAPIs);
  CarbonConsole::registerIntellisense(mAPIs);
  ScrApplicationProto::registerIntellisense(mAPIs);
  XDesignHierarchy::registerIntellisense(mAPIs);
  ScrCarbonDB::registerIntellisense(mAPIs);

  // Builtin script functions:
  mAPIs->addApi("include(fileName) Includes a .js file");
  mAPIs->addApi("alert(title,msg) Message box with title and contents");
  mAPIs->addApi("alert(msg) Message box with contents");
  mAPIs->addApi("debug(msg[,msg1,...]) Prints msg(s) to the Console");
  mAPIs->addApi("enumToString(enumClass, enumValue) Convert enumValue to its string representation");
  mAPIs->addApi("stringToEnum(enumClass, enumName) Convert enumName to its enumerated representation (int)");

  mAPIs->loadAPIForVariable("Plugin", &ScrPlugin::staticMetaObject);

  mAPIs->createEnumIntellisense();

  CQT_CONNECT(mAPIs, apiPreparationFinished(), this, apiPreparationFinished());
  CQT_CONNECT(mAPIs, apiPreparationCancelled(), this, apiPreparationCancelled());

  mAPIs->prepare();

  // exec blocks until we receive finished or cancelled
  exec();

  qDebug() << "Finished Loading APIs";
}

void LoadAPIThread::apiPreparationFinished()
{
  if (mAPIs->savePrepared())
    qDebug() <<"Finished API Preparation";
  else
    qDebug() <<"Warning: Unable to save API Preparation";

  quit();
}
void LoadAPIThread::apiPreparationCancelled()
{
  qDebug() << "Cancelled loading APIs";
  quit();
}
void LoadAPIThread::apiPreparationStarted()
{
  qDebug() << "Started loading APIs";
}

void LoadAPIThread::load()
{
  start();
}

static bool checkArgument(const QString& argument)
{
  int nArgs = qApp->arguments().count();
  for (int i=1; i<nArgs; i++)
  {
    QString arg = qApp->arguments()[i];
    if (arg == argument)
      return true;
  }
  return false;
}

// Identifies the types to the Scripting System
bool JavaScriptAPIs::registerScriptTypes()
{
  CarbonOptions::registerScriptTypes(mTypeMap);
  ModelKit::registerScriptTypes(mTypeMap);
  KitUserContext::registerScriptTypes(mTypeMap);
  KitTest::registerScriptTypes(mTypeMap);
  KitManifest::registerScriptTypes(mTypeMap);
  ScrProject::registerScriptTypes(mTypeMap);

  // Save these types first
  QMap<QString, const QMetaObject*> ccfgTypes;
  ScrCcfg::registerScriptTypes(ccfgTypes);
  foreach(QString key, ccfgTypes.keys())
  {
    const QMetaObject* value = ccfgTypes[key];
    mInternalClasses.append(value);
    mTypeMap[key] = value;
  }

  SpiritXML::registerScriptTypes(mTypeMap);
  SpiritXML_1_4::registerScriptTypes(mTypeMap);
  CarbonConsole::registerScriptTypes(mTypeMap);
  ScrApplicationProto::registerScriptTypes(mTypeMap);
  XDesignHierarchy::registerScriptTypes(mTypeMap);
  ScrCarbonDB::registerScriptTypes(mTypeMap);

  loadIntellisenseVariables();

  if (checkArgument("-classGen"))
  {
    ScriptingEngine engine;

    QStringList classList;
    QString outputDir = "classGen";

    QFileInfo fi(outputDir);

    outputDir = fi.absoluteFilePath();

    QDir dir(outputDir);
    if (dir.mkpath(outputDir))
    {
      foreach (QString typeName, mTypeMap.keys())
      {     
        generateCSharp(&engine, classList, outputDir, typeName, mTypeMap[typeName]);
      }

      // Generate Enum Class Types
      foreach (QString typeName, mTypeMap.keys())
      {     
        const QMetaObject* mo = mTypeMap[typeName];
        QString enumPrefix = getClassInfo(mo, "EnumPrefix");
        if (!enumPrefix.isEmpty())
        {
          generateCSharpEnums(outputDir, mo);       
        }
      }

      const QMetaObject* mo = &CcfgFlags::staticMetaObject;
      generateCSharpEnums(outputDir, mo);
    }
    else
    {
      qDebug() << "Error: Unable to create classGen directory" << outputDir;
    }
    return true;
  }

  if (checkArgument("-api"))
  {
    ScriptingEngine engine;

    QStringList classList;
    QString outputDir = "htmlDoc";

    QFileInfo fi(outputDir);

    outputDir = fi.absoluteFilePath();

    QDir dir(outputDir);
    if (dir.mkpath(outputDir))
    {
      qDebug() << "Generating API Documentation" << outputDir;

      extractResourceFile(outputDir, "classic.css");
      extractResourceFile(outputDir, "index.html");

      foreach (QString typeName, mTypeMap.keys())
      {
        qDebug() << "Dump API" << typeName;
        generateAPIDocumentation(&engine, classList, outputDir, typeName, mTypeMap[typeName]);
      }
      qDebug() << classList;

      QString fileName = "htmlDoc/classes.html";
      QFile htmlFile(fileName);
      if (htmlFile.open(QFile::WriteOnly | QFile::Truncate))
      {
        QTextStream out(&htmlFile);

        out << "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">" << endl;
        out << "<head>" << endl;
        out << "<title>" << "ModelStudio Class Reference</title>" << endl;
        out << "<link href=\"classic.css\" rel=\"stylesheet\" type=\"text/css\"/>" << endl;
        out << "</head>" << endl;
        out << "<body>" << endl;
        out << "<h1 align=\"center\">Classes</h1>" << endl;
        out << "<p><table width=\"100%\" class=\"annotated\" cellpadding=\"2\" cellspacing=\"1\" border=\"0\">" << endl;

        bool odd = false;

        classList.sort();

        QMap<QString, QString> dupMap;
        foreach (QString classFile, classList)
        {
          QFileInfo fi(classFile);
          QString className = fi.baseName();
          if (!dupMap.contains(className))
          {
            if (odd)
              out << QString("<tr valign=\"top\" class=\"odd\"><th><a href=\"%1.html\">%1</a></th></tr>").arg(className) << endl;
            else
              out << QString("<tr valign=\"top\" class=\"even\"><th><a href=\"%1.html\">%1</a></th></tr>").arg(className) << endl;

            odd = !odd;
          }
          dupMap[className] = className;
        }
        out << "</table></p>" << endl;
        out << "</body></html>" << endl;

        htmlFile.close();
      }
    }
    else
    {
      qDebug() << "Error: Unable to create htmlDoc directory" << outputDir;
    }
  }
  return false;
}


void JavaScriptAPIs::generateCSharpEnums(const QString& subDir, const QMetaObject* mo)
{
  QString className = getClassInfo(mo, "EnumPrefix");

  QFileInfo fi(subDir);
  QString outputDir = fi.absoluteFilePath();

  QDir dir(outputDir);
  dir.mkpath(outputDir);

  QString csFileName = outputDir + "/" + className + "_enum_wrapper.cs";


  QFile csWrapperFile(csFileName);
  if (csWrapperFile.open(QFile::WriteOnly | QFile::Truncate))
  {
    QTextStream out(&csWrapperFile);

    out << "using System;" << endl;
    out << "using System.Collections.Generic;" << endl;
    out << "using System.Linq;" << endl;
    out << "using System.Text;" << endl;
    out << "using System.Runtime.InteropServices;" << endl;
    out << "using System.Runtime.CompilerServices;" << endl;
    out << "using System.Diagnostics;" << endl;

    out << endl << "namespace Carbon.Modelstudio" << endl << "{" << endl;

    QString classIndent = "  ";
    QString enumIndent =  "    ";

    out << classIndent << "public partial class " << className << endl;
    out << classIndent << "{" << endl;

    for (int i=0; i<mo->enumeratorCount(); i++)
    {
      QMetaEnum enumerator = mo->enumerator(i);
      out << enumIndent << "public enum " << enumerator.name() << endl;
      out << enumIndent << "{" << endl;
      for (int ei=0; ei<enumerator.keyCount(); ei++)
      {
        QString key = enumerator.key(ei);
        int value = enumerator.value(ei);
        out << enumIndent << "  " << key << " = " << value;

        if (ei < enumerator.keyCount()-1)
          out << ",";
        out << endl;        
      }
      out << enumIndent << "}" << endl;
    }


    out << classIndent << "}" << endl;
    out << "}" << endl;

    csWrapperFile.close();
  }
}

void JavaScriptAPIs::extractResourceFile(const QString& outputDir, const QString& fileName)
{
  QFileInfo cssfi(outputDir + "/" + fileName);
  QResource r(":/cmm/Resources/" + fileName);
  qDebug() << r.absoluteFilePath() << r.fileName() << r.isValid();

  QFile cssFile(":/cmm/Resources/" + fileName);
  if (cssFile.open(QFile::ReadOnly|QFile::Text))
  {
    QString contents = cssFile.readAll();
    qDebug() << "extracted" << fileName << "to" << outputDir;
    QFile cssOut(cssfi.filePath());
    if (cssOut.open(QFile::WriteOnly|QFile::Text))
    {
      QTextStream cssout(&cssOut); 
      cssout << contents;
      cssOut.close();
    }
    cssFile.close();
  }
}


void JavaScriptAPIs::generateCSharp(ScriptingEngine* engine, QStringList&, const QString&, const QString& typeName, const QMetaObject* mo)
{
  qDebug() << "Generating CSharp for" << typeName;
  QString className = getClassInfo(mo, "ClassName");
  if (className.isEmpty())
    className = mo->className();

  bool modelKitCreator = theApp->getContext()->getCanCreateModelKits();
  if (!modelKitCreator && mInternalClasses.contains(mo))
  {
    qDebug() << "Skipping Doc Class" << className;
    return;
  }

  generateCSharpWrapper(engine, typeName, mo);
}

void JavaScriptAPIs::generateAPIDocumentation(ScriptingEngine* engine, QStringList& classList, const QString& outputDir, const QString& typeName, const QMetaObject* mo)
{
  qDebug() << "Generating API for" << typeName;
  QString className = getClassInfo(mo, "ClassName");
  if (className.isEmpty())
    className = mo->className();

  bool modelKitCreator = theApp->getContext()->getCanCreateModelKits();
  if (!modelKitCreator && mInternalClasses.contains(mo))
  {
    qDebug() << "Skipping Doc Class" << className;
    return;
  }

  classList.append(QString("%1.html").arg(className));


  QString fileName = outputDir + "/" + className + ".html";
  QFile htmlFile(fileName);
  if (htmlFile.open(QFile::WriteOnly | QFile::Truncate))
  {
    QTextStream out(&htmlFile);
    writeHtmlDoc(out, engine, mo);
    htmlFile.close();
  }
}

void JavaScriptAPIs::generateCSharpWrapper(ScriptingEngine*,  const QString& typeName, const QMetaObject* mo)
{ 
  QString className = mo->className();

  QString classInfoName = getClassInfo(mo, "ClassName");
  if (classInfoName.isEmpty())
  {
    qDebug() << "----------SKIPPING: " << className;
    return;
  }

  qDebug() << "Gen Class" << className << "for type" << typeName << "as" << classInfoName;

  QString superClassName;
  if (mo->superClass()) 
    superClassName = mo->superClass()->className();

  // Inherits
  const QMetaObject* superMo = lookupClass(superClassName);
  if (superMo)
    qDebug() << superClassName;


  QString outputDir = "classGen";
  QFileInfo fi(outputDir);

  outputDir = fi.absoluteFilePath();

  QDir dir(outputDir);
  dir.mkpath(outputDir);

  QString fileName = outputDir + "/" + className + "_wrapper.cxx";
  QString csFileName = outputDir + "/" + className + "_wrapper.cs";

  QFile wrapperFile(fileName);
  QFile csWrapperFile(csFileName);
  if (wrapperFile.open(QFile::WriteOnly | QFile::Truncate))
  {
    csWrapperFile.open(QFile::WriteOnly | QFile::Truncate);
    QTextStream out(&wrapperFile);
    QTextStream csout(&csWrapperFile);
    writeCSharpMethods(out, csout, mo);
    qDebug() << "Created" << fileName;
    wrapperFile.close();
    csWrapperFile.close();
  }

}

QString JavaScriptAPIs::ConvertToCSharpNativeParamType(const QString& inputType)
{
  if (inputType.contains("::"))
    return FormatCSharpEnum(inputType);

  if (inputType.contains('*') && inputType.count('*') == 1)
  {
    //QString t = inputType;
    return "IntPtr";
  }

  if (inputType == "QString") // Strings are marshalled specially
    return "string";

  if (inputType == "quint32")
    return "uint";

  if (inputType == "qint32")
    return "int";

  if (inputType == "quint64")
    return "ulong";

  if (inputType.isEmpty())
    return "void";

  return inputType;  
}

QString JavaScriptAPIs::ConvertToCSharpNativeReturnType(const QString& inputType)
{
  /* if (inputType.contains("::"))
  return FormatCSharpEnum(inputType);*/

  if (inputType.contains('*') && inputType.count('*') == 1)  
  {
    QString t = inputType;
    return t.replace("*", "");
  }

  if (inputType == "QString") // Strings are marshalled specially
    return "string";

  if (inputType == "quint32")
    return "uint";

  if (inputType == "qint32")
    return "int";

  if (inputType == "quint64")
    return "ulong";

  if (inputType.isEmpty())
    return "void";

  return inputType;  
}

QString JavaScriptAPIs::ConvertToCSharpReturnType(const QString& inputType)
{
  if (inputType.contains('*') && inputType.count('*') == 1)
    return "IntPtr";

  if (inputType == "QString") // Strings are marshalled specially
    return "IntPtr";

  if (inputType == "quint32")
    return "uint";

  if (inputType == "qint32")
    return "int";

  if (inputType == "quint64")
    return "ulong";

  if (inputType.isEmpty())
    return "void";

  return inputType;  
}

QString JavaScriptAPIs::ConvertToCSharpParamType(const QString& inputType)
{
  if (inputType.contains('*') && inputType.count('*') == 1)
    return "HandleRef";

  if (inputType == "QString")
    return "string";

  if (inputType == "quint32")
    return "uint";

  if (inputType == "qint32")
    return "int";

  if (inputType == "quint64")
    return "ulong";

  if (inputType.isEmpty())
    return "void";

  return inputType;  
}

QString JavaScriptAPIs::ConvertToMarshallableReturnType(const QString& inputType)
{
  //if (inputType.contains('*') && inputType.count('*') == 1)
  //  return "void*";

  if (inputType == "QString")
    return "char*";

  if (inputType == "qint32")
    return "int";

  if (inputType == "quint32")
    return "unsigned int";

  if (inputType == "quint64")
    return "unsigned long long";

  if (inputType.isEmpty())
    return "void";

  return inputType;  
}

QString JavaScriptAPIs::ConvertToMarshallableParamType(const QString& inputType)
{
  //if (inputType.contains('*') && inputType.count('*') == 1)
  //  return "void*";

  if (inputType == "QString")
    return "const char*";

  if (inputType == "quint64")
    return "unsigned long long";

  if (inputType == "quint32")
    return "unsigned int";

  if (inputType == "qint32")
    return "int";

  if (inputType.isEmpty())
    return "void";

  return inputType;  
}

QString JavaScriptAPIs::methodCDeclaration(const QMetaObject* mo, const QMetaMethod& method)
{
  QString className = mo->className();

  QString retString;
  QTextStream out(&retString);

  QString sig = method.signature();
  QString methodName = sig.left(sig.indexOf('('));
  int numParams = method.parameterNames().count();
  QString retType = method.typeName();

  QString returnType;
  if (retType.isEmpty())
    returnType = "void";
  else
    returnType = retType;

  int sighash = hashString(method.signature());
  QString cmethodName= QString("CSharp_%1_%2_%3_%4").arg(className).arg(methodName).arg(numParams).arg(sighash);

  out << "PINVOKEABLE " << ConvertToMarshallableReturnType(retType) << " " << cmethodName << "(";         

  out << "void* thisObj";

  int pi = 0;
  const QList<QByteArray>& methTypes = method.parameterTypes();   

  foreach(QByteArray ba, method.parameterNames())
  {
    QString paramType = QString(methTypes[pi]);
    QString paramName = QString(ba);
    out << ", " << ConvertToMarshallableParamType(paramType) << " " << paramName;
    pi++;
  }

  out << ")";

  return retString;
}

QString JavaScriptAPIs::methodDllImportDeclaration(const QString& importIndent, const QMetaObject* mo, const QMetaMethod& method)
{
  QString className = mo->className();

  QString retString;
  QTextStream out(&retString);

  QString sig = method.signature();
  QString methodName = sig.left(sig.indexOf('('));
  int numParams = method.parameterNames().count();
  QString retType = method.typeName();

  QString returnType;
  if (retType.isEmpty())
    returnType = "void";
  else
    returnType = retType;

  int sighash = hashString(method.signature());
  QString cmethodName= QString("CSharp_%1_%2_%3_%4").arg(className).arg(methodName).arg(numParams).arg(sighash);

  if (methodName == "removeSubComp")
    qDebug() << "stop";

  out << importIndent << "[DllImport(\"__Internal\", EntryPoint=\"" << cmethodName << "\")]" << endl;

  QString safeReturnType = retType;
  if (safeReturnType.contains("::"))
    safeReturnType = "int";

  out << importIndent << "private extern static " << ConvertToCSharpReturnType(safeReturnType) << " " << cmethodName;

  // params
  out << "(HandleRef thisObj";
  int pi = 0;
  const QList<QByteArray>& methTypes = method.parameterTypes();

  foreach(QByteArray ba, method.parameterNames())
  {         
    QString paramName = QString(ba);
    QString paramType = QString(methTypes[pi]);
    if (paramType.contains("::"))
      paramType = "int";

    out << ", " << ConvertToCSharpNativeParamType(paramType) << " " << paramName;
    pi++;
  }

  out << ");" << endl;

  return retString;
}

const QMetaMethod* JavaScriptAPIs::findMethodBySignature(const QMetaObject* mo, const QString& sig)
{
  for (int i=0; i<mo->methodCount(); i++)
  {
    const QMetaMethod& meth = mo->method(i);
    if (meth.methodType() == QMetaMethod::Slot && meth.access() == QMetaMethod::Public)
    {
      QString methodSig = meth.signature();
      if (methodSig == sig)
        return &meth;
    }
  } 
  return NULL;
}

const QMetaProperty* JavaScriptAPIs::findPropBySignature(const QMetaObject* mo, const QString& sig)
{
  for (int i=0; i<mo->propertyCount(); i++)
  {
    const QMetaProperty& prop = mo->property(i);
    QString propSig = QString("%1 %2").arg(prop.typeName()).arg(prop.name());
    if (propSig == sig)
      return &prop;
  } 
  return NULL;
}

//
// Search for a method with same signature on any superclass
//
bool JavaScriptAPIs::isInheritedSignature(const QMetaObject* mo, const QString& sig)
{
  const QMetaObject* superClass = mo->superClass(); 
  while(superClass)
  {
    const QMetaMethod* foundMethod = findMethodBySignature(superClass, sig);
    if (foundMethod)
    {
      qDebug() << "Superclassed method" << sig;
      return true;
    }
    superClass = superClass->superClass();
  }   

  return false;
}

bool JavaScriptAPIs::isInheritedPropertySignature(const QMetaObject* mo, const QString& sig)
{
  const QMetaObject* superClass = mo->superClass(); 
  while(superClass)
  {
    const QMetaProperty* foundProp = findPropBySignature(superClass, sig);
    if (foundProp)
    {    
      return true;
    }
    superClass = superClass->superClass();
  }   
  return false;
}


int JavaScriptAPIs::hashString(const QString& inputStr)
{
  int hash = 0;
  int offset = 'a' - 1;

  for (int i=0; i<inputStr.length(); i++)
  {
    char c = inputStr.at(i).toAscii();  // this_toascii_ok
    hash = hash << 1 | (c - offset);
  }

  return abs(hash);
}

void JavaScriptAPIs::writeDllImports(QTextStream& csout, const QMetaObject* mo)
{
  QString importIndent = "    ";
  QString classInfoName = getClassInfo(mo, "ClassName");
  QString className = mo->className();
  QString friendlyClassName = className;
  if (!classInfoName.isEmpty())
    friendlyClassName = classInfoName;

  QString superClassName;
  if (mo->superClass())
  {
    superClassName = mo->superClass()->className();
    QString superName = getClassInfo(mo->superClass(), "ClassName");
    if (!superName.isEmpty())
      superClassName = superName;
  }

  // Skip these
  if (!superClassName.isEmpty() && (superClassName.startsWith('Q') || superClassName == "DocumentedQObject"))
    superClassName = "";

  csout << importIndent << "private HandleRef objCPtr;" << endl;

  if (superClassName.isEmpty())
  {
    csout << importIndent << "protected bool cMemOwn;" << endl;
    csout << importIndent << "public bool IsNull() { return objCPtr.Handle == IntPtr.Zero; }" << endl;
  }

  csout << importIndent << "internal " << friendlyClassName << "(IntPtr cPtr, bool cMemoryOwn)";

  if (!superClassName.isEmpty())
  {
    QString upcastMethod = QString("CSharp_%1_UpCast").arg(className);
    csout << " : base(" << upcastMethod << "(cPtr), cMemoryOwn)";
  }

  csout << endl; // End of internal ctor

  csout << importIndent << "{" << endl;
  csout << importIndent << "  cMemOwn = cMemoryOwn;" << endl;
  csout << importIndent << "  objCPtr = new HandleRef(this, cPtr);" << endl;
  csout << importIndent << "}" << endl;
  csout << importIndent << "public static HandleRef getCPtr(" << friendlyClassName << " obj)" << endl;
  csout << importIndent << "{" << endl;
  csout << importIndent << "   return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.objCPtr;" << endl; 
  csout << importIndent << "}" << endl;


  csout << importIndent << "~" << friendlyClassName << "()" << endl;
  csout << importIndent << "{" << endl;
  csout << importIndent << "  Dispose();" << endl;
  csout << importIndent << "}" << endl;


  QString methodKind = "virtual";
  if (!superClassName.isEmpty())
    methodKind = "override";


  csout << importIndent << "public " << methodKind << " void Dispose()" << endl;
  csout << importIndent << "{" << endl;
  csout << importIndent << "  lock(this)" << endl;
  csout << importIndent << "  {" << endl;
  csout << importIndent << "    if (objCPtr.Handle != IntPtr.Zero)" << endl;
  csout << importIndent << "    {" << endl;
  csout << importIndent << "      if (cMemOwn)" << endl;
  csout << importIndent << "      {" << endl;
  csout << importIndent << "        cMemOwn = false;" << endl;
  csout << importIndent << "        // TBD: examplePINVOKE.delete_Shape(objCPtr);" << endl;
  csout << importIndent << "      }"<< endl;
  csout << importIndent << "      objCPtr = new HandleRef(null, IntPtr.Zero);"<< endl;
  csout << importIndent << "    }"<< endl;
  csout << importIndent << "    GC.SuppressFinalize(this);"<< endl;
  csout << importIndent << "  }" << endl;
  csout << importIndent << "}" << endl;
  csout << endl;

  csout << importIndent << "#region DllImports" << endl;

  QMap<QString, QString> methodMap;

  for (int i=0; i<mo->methodCount(); i++)
  {
    const QMetaMethod& meth = mo->method(i);
    if (meth.methodType() == QMetaMethod::Slot && meth.access() == QMetaMethod::Public)
    {
      bool skipMethod = false;

      // Test for Qt parameters, we can't handle    
      foreach(QByteArray ba, meth.parameterTypes())
      {       
        QString paramType = QString(ba);
        if (paramType.startsWith('Q') && paramType != "QString")
        {
          skipMethod = true;
          break;
        }        
      }

      if (skipMethod)
      {
        qDebug() << "Skipping method" << meth.signature();
        continue;
      }

      QString methodSig = QString("%1 %2").arg(meth.typeName()).arg(meth.signature());
      if (!methodMap.contains(methodSig))
      {
        QString retType = meth.typeName();
        if (retType.startsWith('Q') && retType != "QString")
        {
          qDebug() << "Skipping dllimport" << meth.signature();
          continue;
        }

        methodMap[methodSig] = methodSig;
        if (!isInheritedSignature(mo, meth.signature()))
        {         
          csout << methodDllImportDeclaration(importIndent, mo, meth);
        }
        else
          qDebug() << "inherited";
      }
    }
  }

  // We need an Upcast
  if (!superClassName.isEmpty())
  {
    qDebug() << "Need upcast" << superClassName;
    QString upcastMethod = QString("CSharp_%1_UpCast").arg(className);
    csout << importIndent << "[DllImport(\"__Internal\", EntryPoint=\"" << upcastMethod << "\")]" << endl;
    csout << importIndent << "private static extern IntPtr " << upcastMethod << "(IntPtr obj);" << endl;   
  }

  csout << importIndent << "#endregion DllImports" << endl;
}

QString JavaScriptAPIs::FormatCSharpEnum(const QString& name)
{
  if (name.contains("::"))
  {    
    QStringList typeParts = name.split("::");
    if (typeParts.count() > 1)
    {
      QString className = typeParts[0];
      QString enumName = typeParts[1];
      return QString("%1.%2").arg(className).arg(enumName);
    }
  }
  return name;
}
void JavaScriptAPIs::writeCSharpMethodImplementation(QTextStream& csout,const QMetaObject* mo, const QMetaMethod& meth)
{
  QString className = mo->className();
  QString sig = meth.signature();
  QString methodName = sig.left(sig.indexOf('('));
  int numParams = meth.parameterNames().count();
  QString retType = meth.typeName();

  QString classInfoName = getClassInfo(mo, "ClassName"); 
  QString friendlyClassName = className;
  if (!classInfoName.isEmpty())
    friendlyClassName = classInfoName;

  QString retTypeClassName = ConvertToCSharpNativeReturnType(retType);

  const QMetaObject* retClass = lookupRawClass(retTypeClassName);   
  if (retClass == NULL)
    retClass = lookupClass(retTypeClassName);

  if (retClass)
  {
    QString retClassInfoName = getClassInfo(retClass, "ClassName"); 
    retTypeClassName = retClass->className();
    if (!retClassInfoName.isEmpty())
      retTypeClassName = retClassInfoName;      
  }

  if (retType.contains('*'))
  {
    if (retClass == NULL)
    {
      qDebug() << "Unknonwn return class" << retType << retTypeClassName;
      return;
    }

    QString retClassName = getClassInfo(retClass, "ClassName"); 
    if (retClassName.isEmpty())
    {
      qDebug() << "Unknonwn return class" << retType << retTypeClassName;
      return;
    }
  }

  // Don't do QT classes
  if (retTypeClassName.startsWith('Q'))
    return;

  QString methodIndent = "    ";
  QString methodBodyIndent = QString("%1  ").arg(methodIndent);
  bool bReturnsEnum = false;

  // Enum
  if (retTypeClassName.contains("::"))
  {    
    retTypeClassName = FormatCSharpEnum(retTypeClassName);
    qDebug() << "Found enum" << retTypeClassName;    
    bReturnsEnum = true;
  }

  // Skip inherited members
  if (isInheritedSignature(mo, meth.signature()))
    return;


  csout << methodIndent << "public " << retTypeClassName << " " << methodName;

  int sighash = hashString(meth.signature());
  QString cmethodName= QString("CSharp_%1_%2_%3_%4").arg(className).arg(methodName).arg(numParams).arg(sighash);

  // params
  csout << "(";
  int pi = 0;
  const QList<QByteArray>& methTypes = meth.parameterTypes();

  foreach(QByteArray ba, meth.parameterNames())
  {         
    QString paramName = QString(ba);
    QString paramType = QString(methTypes[pi]);

    if (pi > 0)
      csout << ", ";

    QString nativeParamType = ConvertToCSharpNativeParamType(paramType);
    
    // Use the Native CSharp type name here.
    if (nativeParamType == "IntPtr")
      nativeParamType = nativeClassName(paramType.replace("*",""));         

    csout << nativeParamType << " " << paramName;
    pi++;
  }

  csout << ")" << endl;

  csout << methodIndent << "{" << endl;

  csout << methodBodyIndent;

  csout << "if (objCPtr.Handle == IntPtr.Zero) throw new Exception(\"Attempt to dereference NULL " << friendlyClassName << " object\");" << endl;

  csout << methodBodyIndent;

  // Are we returning an object? or intrinsic type/
  if (retType.contains('*'))
  {
    csout << "IntPtr cPtr = " << cmethodName << "(objCPtr";
    pi = 0;   
    foreach(QByteArray ba, meth.parameterNames())
    {         
      QString paramName = QString(ba);
      QString paramType = QString(methTypes[pi]);
      if (paramType.contains("::"))
        paramName = QString("(int)%1").arg(paramName);

      QString nativeParamType = ConvertToCSharpNativeParamType(paramType);
      if (nativeParamType == "IntPtr")
      {
        //CarbonCfgCadi.getCPtr(cadi).Handle
        nativeParamType = nativeClassName(paramType.replace("*",""));      
        if (nativeParamType != "IntPtr")
          paramName = QString("%1.getCPtr(%2).Handle").arg(nativeParamType).arg(paramName);        
      }

      csout << ", " << paramName;
      pi++;
    }
    csout << ");" << endl;

    csout << methodBodyIndent << retTypeClassName << " ret = (cPtr == IntPtr.Zero) ? null : new " << retTypeClassName << "(cPtr, false);" << endl;
    csout << methodBodyIndent << "return ret;" << endl;
  }
  else if (retType == "QString") // Handled special
  {
    csout << "IntPtr ipSr = " << cmethodName << "(objCPtr";

    pi = 0;   
    foreach(QByteArray ba, meth.parameterNames())
    {         
      QString paramName = QString(ba); 
      QString paramType = QString(methTypes[pi]);
      if (paramType.contains("::"))
        paramName = QString("(int)%1").arg(paramName);

      QString nativeParamType = ConvertToCSharpNativeParamType(paramType);
      if (nativeParamType == "IntPtr")
      {
        nativeParamType = nativeClassName(paramType.replace("*",""));      
        if (nativeParamType != "IntPtr")
          paramName = QString("%1.getCPtr(%2).Handle").arg(nativeParamType).arg(paramName);      
      }


      csout << ", " << paramName;
      pi++;
    }
    csout << ");" << endl;

    csout << methodBodyIndent<< "string result = Marshal.PtrToStringAnsi(ipSr);" << endl;
    csout << methodBodyIndent<< "Marshal.FreeCoTaskMem(ipSr);" << endl;
    csout << methodBodyIndent<< "return result;" << endl;
  }
  else // Intrinsic Type
  {
    if (!retType.isEmpty())
      csout << "return ";

    if (bReturnsEnum)
      csout << "(" << retTypeClassName << ")";

    csout << cmethodName;

    csout << "(objCPtr";

    pi = 0;   
    foreach(QByteArray ba, meth.parameterNames())
    {         
      QString paramName = QString(ba);  
      QString paramType = QString(methTypes[pi]);
      if (paramType.contains("::"))
        paramName = QString("(int)%1").arg(paramName);

      QString nativeParamType = ConvertToCSharpNativeParamType(paramType);
      if (nativeParamType == "IntPtr")
      {
        nativeParamType = nativeClassName(paramType.replace("*",""));      
        if (nativeParamType != "IntPtr")
          paramName = QString("%1.getCPtr(%2).Handle").arg(nativeParamType).arg(paramName);      
      }


      csout << ", " << paramName;
      pi++;
    }
    csout << ");" << endl;
  }

  csout << methodIndent << "}" << endl << endl;
}

QString JavaScriptAPIs::capitalize(const QString& inputStr)
{
  QString str = inputStr;
  str = str.left(1).toUpper()+str.mid(1);
  return str;
}

int JavaScriptAPIs::findSetter(const QMetaObject* mo, const QMetaProperty* prop)
{
  QString propName = prop->name();      
  QString propType = prop->typeName();

  qDebug() << propName << propType;

  QString setNameImplicit = QString("set%1").arg(capitalize(propName));
  QString putNameImplicit = QString("put%1").arg(capitalize(propName));
  QString setNameExact = propName;

  // Look for a public slot taking only 1 argument matching propType
  for (int i=0; i<mo->methodCount(); i++)
  {
    const QMetaMethod& meth = mo->method(i);
    if (meth.methodType() == QMetaMethod::Slot && meth.access() == QMetaMethod::Public)
    {
      if (meth.parameterNames().count() == 1 && propType == meth.parameterTypes().at(0))
      {
        QString sig = meth.signature();
        QString methodName = sig.left(sig.indexOf('('));

        if (methodName == setNameImplicit || methodName == putNameImplicit)
        {
          qDebug() << "Implicit set match" << methodName;
          return i;
        }  
        else if (methodName == setNameExact)
        {
          qDebug() << "Exact set match" << methodName;
          return i;
        }
      }
    }
  }

  return -1;
}

int JavaScriptAPIs::findGetter(const QMetaObject* mo, const QMetaProperty* prop)
{
  QString propName = prop->name();      
  QString propType = prop->typeName();

  qDebug() << propName << propType;

  QString getNameImplicit = QString("get%1").arg(capitalize(propName));
  QString getNameExact = propName;

  // Look for a public slot taking only 0 arguments matching return type
  for (int i=0; i<mo->methodCount(); i++)
  {
    const QMetaMethod& meth = mo->method(i);
    if (meth.methodType() == QMetaMethod::Slot && meth.access() == QMetaMethod::Public)
    {
      if (meth.parameterNames().count() == 0 && propType == meth.typeName())
      {
        QString sig = meth.signature();
        QString methodName = sig.left(sig.indexOf('('));

        if (methodName == getNameImplicit)
        {
          qDebug() << "Implicit Get match" << methodName;
          return i;
        }  
        else if (methodName == getNameExact)
        {
          qDebug() << "Exact Get match" << methodName;
          return i;
        }
      }
    }
  }

  return -1;
}

void JavaScriptAPIs::writeCSharpMethods(QTextStream& out, QTextStream& csout, const QMetaObject* mo)
{
  csout << "using System;" << endl;
  csout << "using System.Collections.Generic;" << endl;
  csout << "using System.Linq;" << endl;
  csout << "using System.Text;" << endl;
  csout << "using System.Runtime.InteropServices;" << endl;
  csout << "using System.Runtime.CompilerServices;" << endl;
  csout << "using System.Diagnostics;" << endl;

  csout << endl << "namespace Carbon.Modelstudio" << endl << "{" << endl;

  QString classIndent = "  ";


  if (mo->methodCount() > 0 || mo->propertyCount() > 0)
  {
    QString classInfoName = getClassInfo(mo, "ClassName");
    QString className = mo->className();
    QString friendlyClassName = className;
    if (!classInfoName.isEmpty())
      friendlyClassName = classInfoName;

    QString superClassName;
    if (mo->superClass())
    {
      superClassName = mo->superClass()->className();
      QString superName = getClassInfo(mo->superClass(), "ClassName");
      if (!superName.isEmpty())
        superClassName = superName;
    }

    QString inheritsFrom("IDisposable");
    if (!superClassName.isEmpty() && !superClassName.startsWith('Q') && superClassName != "DocumentedQObject")
      inheritsFrom = QString("%1, IDisposable").arg(superClassName);

    // Clear this out now otherwise we will need an upcast
    if (!superClassName.isEmpty() && (superClassName.startsWith('Q') || superClassName == "DocumentedQObject"))
      superClassName = "";

    csout << classIndent << "public partial class " << friendlyClassName << " : " << inheritsFrom << endl << classIndent << "{" << endl;

    writeDllImports(csout, mo);

    csout << classIndent << "  #region Properties" << endl;
    QString propIndent = "    ";
    // Properties
    QMap<QString, QString> propMap;
    for (int i=0; i<mo->propertyCount(); i++)
    {
      const QMetaProperty& prop = mo->property(i);
      //const QMetaObject* encObject = prop.enclosingMetaObject();

      QString propName = prop.name();      
      QString propType = prop.typeName();
      QString propSig = QString("%1 %2").arg(propType).arg(propName);
      if (!propMap.contains(propSig))
      {
        if (isInheritedPropertySignature(mo, propSig))
        {
          qDebug() << "Skipping inherited property" << propSig;
          continue;
        }
        
        propMap[propSig] = propSig;

        QString propcsharpType = ConvertToCSharpNativeReturnType(propType);
    
        if (propcsharpType.contains("::"))
          propcsharpType = FormatCSharpEnum(propcsharpType);
        else // use mapped name
        {
          propcsharpType = mappedClassName(propcsharpType);
          qDebug() << "propcsharpType" << propSig << propcsharpType << nativeClassName(propcsharpType) << propType << isMappedClass(propcsharpType) << mappedClassName(propcsharpType);           

         }

        int setMethodIndex = findSetter(mo, &prop);
        int getMethodIndex = findGetter(mo, &prop);

        //  No matching methods? skip this property
        if (setMethodIndex == -1 && getMethodIndex == -1)
        {
          if (prop.isWritable() && setMethodIndex == -1)
            qDebug() << "******** ERROR: No such setter found for" << propName;
          
          if (prop.isReadable() && getMethodIndex == -1)
            qDebug() << "******** ERROR: No such getter found for" << propName;

          continue;
        }

        if (propcsharpType == "KitAnswerGroups" ||
              propcsharpType == "CarbonConsole" ||
              propcsharpType == "KitAnswers" ||
              propcsharpType.startsWith('Q') || propcsharpType.startsWith('X'))
        {
          qDebug() << "Skipping bad type";
          continue;
        }

        QString capName = capitalize(propName);

        // Skip duplicates
        if (propMap.contains(capName))
          continue;

        propMap[capName] = capName;

        csout << propIndent << "public " << propcsharpType << " " << capName << endl;
        csout << propIndent << "{" << endl;

        if (prop.isWritable())
        {          
          if (setMethodIndex == -1)
            qDebug() << "******** WARNING: No such setter found for" << propName;
          else
          {
            const QMetaMethod& setMethod = mo->method(setMethodIndex);
            QString setSig = setMethod.signature();
            QString setMethodName = setSig.left(setSig.indexOf('('));
   
            csout << propIndent << "  " << "set" << endl;
            csout << propIndent << "  " << "{" << endl;
            csout << propIndent << "    " << setMethodName << "(value);" << endl;
            csout << propIndent << "  " << "}" << endl;
          }
        }
       
        // Props are readable
        if (getMethodIndex == -1)
          qDebug() << "******** WARNING: No such getter found for" << propName;
        else
        {
          const QMetaMethod& getMethod = mo->method(getMethodIndex);
          QString getSig = getMethod.signature();
          QString getMethodName = getSig.left(getSig.indexOf('('));
   
          csout << propIndent << "  " << "get" << endl;
          csout << propIndent << "  " << "{" << endl;
          csout << propIndent << "  " << "  return " << getMethodName << "();" << endl;
          csout << propIndent << "  " << "}" << endl;
      
        }
        csout << propIndent << "}" << endl;
      }
    }

    csout << classIndent << "  #endregion Properties" << endl;

    csout << classIndent << "  #region Methods" << endl;

    // Methods
    QMap<QString, QString> methodMap;
    for (int i=0; i<mo->methodCount(); i++)
    {
      const QMetaMethod& meth = mo->method(i);
      if (meth.methodType() == QMetaMethod::Slot && meth.access() == QMetaMethod::Public)
      {
        QString methodSig = QString("%1 %2").arg(meth.typeName()).arg(meth.signature());
        if (methodMap.contains(methodSig))
          continue;

        bool skipMethod = false;
        // Test for Qt parameters, we can't handle    
        foreach(QByteArray ba, meth.parameterTypes())
        {       
          QString paramType = QString(ba);
          if (paramType.startsWith('Q') && paramType != "QString")
          {
            skipMethod = true;
            break;
          }        
        }

        if (skipMethod)
        {
          qDebug() << "Skipping method" << meth.signature();
          continue;
        }

        methodMap[methodSig] = methodSig;

        QString sig = meth.signature();
        QString methodName = sig.left(sig.indexOf('('));
        QString retType = meth.typeName();

        QString returnType;
        if (retType.isEmpty())
          returnType = "void";
        else
          returnType = retType;

        if (returnType.contains('*'))
          returnType = "void*";
        else if (returnType == "QString")
          returnType = "char*";

        QString signature = methodCDeclaration(mo, meth);

        writeCSharpMethodImplementation(csout, mo, meth); 

        out << signature << endl;

        out << "{" << endl;

        out << "    " << className << "* obj = (" << className << "*)(thisObj);" << endl;

        out << "    ";
        if (!retType.isEmpty())
          out << "return ";

        if (retType == "QString")
          out << "EmbeddedMono::AllocString(";

        out << "obj->" << methodName << "(";   

        int pi = 0;
        foreach(QByteArray ba, meth.parameterNames())
        {         
          QString paramName = QString(ba);
          if (pi > 0)
            out << ", ";
          out << paramName;
          pi++;
        }

        if (retType == "QString")
          out << ")";

        out << ");" << endl;

        out << "}" << endl;
      }
    }

    // Do we need an Upcast?
    if (!superClassName.isEmpty())
    {
      const QMetaObject* superCo = mo->superClass();

      out << "PINVOKEABLE " << superCo->className() << "* CSharp_" << className << "_UpCast(" << className << "* obj)" << endl;
      out << "{" << endl;
      out << "  return (" << superCo->className() << "*)(obj);" << endl;
      out << "}" << endl;
    }

    csout << classIndent << "  #endregion Methods" << endl;

    csout << endl << classIndent << "}" << endl;
  }





  // close namespace
  csout << endl << "}" << endl;
}

void JavaScriptAPIs::writePropertiesDescr(QTextStream& out, ScriptingEngine*, const QMetaObject* mo)
{
  if (mo->propertyCount() > 0)
  {
    // Properties
    out << "<h3>Properties</h3>" << endl;
    out << "<ul>" << endl;

    for (int i=0; i<mo->propertyCount(); i++)
    {
      QMetaProperty prop = mo->property(i);
      QString access;
      if (!prop.isReadable() || !prop.isWritable())
      {
        access = " {";
        if (!prop.isReadable())
          access += "Write-Only";
        if (!prop.isWritable())
          access += "Read-Only";
        access += "}";
      }

      out << QString("<li><div class=\"fn\"/><b>%1</b> : %2%3</li>")
        .arg(prop.name())
        .arg(typeNameLink(prop.typeName()))
        .arg(access)
        << endl;
    }
    out << "</ul>" << endl;
  }
}

void JavaScriptAPIs::writeEnumsDescr(QTextStream& out, ScriptingEngine*, const QMetaObject* mo)
{
  if (mo->enumeratorCount() > 0)
  {
    QString className = getClassInfo(mo, "ClassName");
    if (className.isEmpty())
      className = mo->className();

    // Enums
    out << "<h3>Enums</h3>" << endl;
    out << "<ul>" << endl;

    for (int i=0; i<mo->enumeratorCount(); i++)
    {
      QMetaEnum e = mo->enumerator(i);

      QString objNameLink = QString("<b><a href=\"%2.html#%1-enum\">%1</a></b>").arg(e.name()).arg(className);
      out << QString("<li><div class=\"fn\"/>enum %1 { ").arg(objNameLink);

      for (int j=0; j<e.keyCount(); j++)
      {
        QString key = e.key(j);
        if (j > 0)
          out << ", ";
        out << key;
      }
      out << " }";
    }
    out << "</ul>" << endl;
  }
}

void JavaScriptAPIs::writeSignalsDescr(QTextStream& out, ScriptingEngine*, const QMetaObject* mo)
{
  // Signals
  if (mo->methodCount() > 0)
  {
    bool firstOne = true;
    for (int i=0; i<mo->methodCount(); i++)
    {
      const QMetaMethod& meth = mo->method(i);
      if (meth.methodType() == QMetaMethod::Signal)
      {
        if (firstOne)
        {
          out << "<h3>Signals</h3>" << endl;
          out << "<ul>" << endl;
          firstOne = false;
        }

        QString sig = meth.signature();
        QString methodName = sig.left(sig.indexOf('('));
        QString retType = meth.typeName();

        QString typeName;
        if (retType.isEmpty())
          typeName = "void";
        else
          typeName = retType;

        // start Method
        out << QString("<li><div class=\"fn\"/><b>%2 %1(").arg(methodName).arg(typeNameLink(typeName));

        // Write arguments
        const QList<QByteArray>& methTypes = meth.parameterTypes();
        int pi = 0;
        foreach(QByteArray ba, meth.parameterNames())
        {
          if (pi > 0)
            out << ", ";

          QString paramType = typeNameLink(QString(methTypes[pi]));
          QString paramName = QString(ba);

          out << paramType << " " << paramName;

          pi++;
        }

        // finish method
        out << ")</b></li>" << endl;
      }
    }
    if (!firstOne)
      out << "</ul>" << endl;
  }
}

void JavaScriptAPIs::writeMethodsDescr(QTextStream& out, ScriptingEngine*, const QMetaObject* mo)
{
  // Methods
  if (mo->methodCount() > 0)
  {
    QString className = getClassInfo(mo, "ClassName");
    if (className.isEmpty())
      className = mo->className();

    out << "<h3>Methods</h3>" << endl;
    out << "<ul>" << endl;
    for (int i=0; i<mo->methodCount(); i++)
    {
      const QMetaMethod& meth = mo->method(i);
      if (meth.methodType() == QMetaMethod::Slot && meth.access() == QMetaMethod::Public)
      {
        QString sig = meth.signature();
        QString methodName = sig.left(sig.indexOf('('));
        QString retType = meth.typeName();

        QString typeName;
        if (retType.isEmpty())
          typeName = "void";
        else
          typeName = retType;

        // start Method

        QString methodLink = QString("<b><a href=\"%1.html#%2-method\">%2</a></b>").arg(className).arg(methodName);

        out << QString("<li><div class=\"fn\"/>%2 %1(").arg(methodLink).arg(typeNameLink(typeName));

        // Write arguments
        const QList<QByteArray>& methTypes = meth.parameterTypes();
        int pi = 0;
        foreach(QByteArray ba, meth.parameterNames())
        {
          if (pi > 0)
            out << ", ";

          QString paramType = typeNameLink(QString(methTypes[pi]));
          QString paramName = QString(ba);

          out << paramType << " " << "<i>" << paramName << "</i>";

          pi++;
        }

        // finish method
        out << ")</li>" << endl;
      }
    }
    out << "</ul>" << endl;
  }
}

void JavaScriptAPIs::writeHtmlDoc(QTextStream& out, ScriptingEngine* engine, const QMetaObject* mo)
{
  QString className = getClassInfo(mo, "ClassName");
  if (className.isEmpty())
    className = mo->className();

  out << "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">" << endl;

  out << "<title>" << className << " Class Reference</title>" << endl;
  out << "<link href=\"classic.css\" rel=\"stylesheet\" type=\"text/css\"/>" << endl;
  out << "</head>" << endl;
  out << "<body>" << endl;
  out << "<h1 align=\"center\">" << className << " Class Reference</h1>" << endl;

  QString superClassName;
  if (mo->superClass())
  {
    superClassName = mo->superClass()->className();
    QString superName = getClassInfo(mo->superClass(), "ClassName");
    if (!superName.isEmpty())
      superClassName = superName;
  }


  QString classDocHeader = getClassInfo(mo, "ClassDocHeader");
  if (!classDocHeader.isEmpty())
  {
    out << "<h3 class=\"fn\">Class Information</h3>";
    out << "<p>" << classDocHeader << "</p>";
  }


  // Inherits
  const QMetaObject* superMo = lookupClass(superClassName);
  if (superMo)
    out << QString("<p>Inherits <a href=\"%1.html\">%1</a></p>").arg(superClassName) << endl;
  else
    out << QString("<p>Inherits <b>%1</b></p>").arg(superClassName) << endl;

  // Walk the map to see if we have
  // a variable to this class
  foreach (QString varName, mMapVarToAPI.keys())
  {
    const QMetaObject* kmo = mMapVarToAPI[varName];
    if (kmo == mo)
    {
      out << "<h3 class=\"fn\">Intellisense</h3>";
      out << QString("<p>Variables starting with <b>%1</b> will be assumed to be of type %2</p>").arg(varName).arg(className) << endl;
      break;
    }
  }

  writeEnumsDescr(out, engine, mo);
  writePropertiesDescr(out, engine, mo);
  writeMethodsDescr(out, engine, mo);
  writeSignalsDescr(out, engine, mo);

  out << "<hr/>" << endl;

  writeEnumsBody(out, engine, mo);
  writePropertiesBody(out, engine, mo);
  writeMethodsBody(out, engine, mo);
  writeSignalsBody(out, engine, mo);

  out << "</body></html>" << endl;
}


void JavaScriptAPIs::writeEnumsBody(QTextStream& out, ScriptingEngine*, const QMetaObject* mo)
{
  if (mo->enumeratorCount() > 0)
  {
    QString className = getClassInfo(mo, "ClassName");
    if (className.isEmpty())
      className = mo->className();

    for (int i=0; i<mo->enumeratorCount(); i++)
    {
      QMetaEnum e = mo->enumerator(i);
      out << QString("<h3 class=\"fn\"><a name=\"%1-enum\"></a>enum %2::%1</h3>").arg(e.name()).arg(className) << endl;

      out << "<p><table border=\"1\" cellpadding=\"2\" cellspacing=\"1\" width=\"100%\">" << endl;

      out << "<tr><th>Constant</th><th>Value</th></tr>" << endl;

      for (int j=0; j<e.keyCount(); j++)
      {
        QString key = e.key(j);
        int value = e.value(j);
        out << "<tr>" << endl;

        out << QString("<td valign=\"top\"><tt>%1.%2</tt></td>").arg(className).arg(key) << endl;
        out << QString("<td align=\"center\" valign=\"top\"><tt>%1</tt></td>").arg(value) << endl;

        out << "</tr>" << endl;
      }

      out << "</table></p>" << endl;
    }
  }
}

void JavaScriptAPIs::writePropertiesBody(QTextStream& , ScriptingEngine*, const QMetaObject*)
{
}

void JavaScriptAPIs::writeMethodsBody(QTextStream& out, ScriptingEngine* engine, const QMetaObject* mo)
{
  // Methods
  if (mo->methodCount() > 0)
  {
    QString className = getClassInfo(mo, "ClassName");
    if (className.isEmpty())
      className = mo->className();

    // Is there a description?
    QScriptValue globals = engine->globalObject();
    QScriptValue co = globals.property(className);
    QObject* qo = NULL;
    DocumentedQObject* dqo = NULL;

    if (co.isValid())
    {
      qo = co.toQObject();
      dqo = dynamic_cast<DocumentedQObject*>(qo);
      if (dqo)
        qDebug() << "Found documented object";
      else // Prototype?
      {
        if (co.isObject())
        {
          QScriptValue protoType = co.prototype();
          qDebug() << protoType.toString() << protoType.isQObject();
          qo = protoType.toQObject();
          dqo = dynamic_cast<DocumentedQObject*>(qo);
        }
      }
    }

    for (int i=0; i<mo->methodCount(); i++)
    {
      const QMetaMethod& meth = mo->method(i);
      if (meth.methodType() == QMetaMethod::Slot && meth.access() == QMetaMethod::Public)
      {
        QString sig = meth.signature();
        QString methodName = sig.left(sig.indexOf('('));
        QString retType = meth.typeName();

        QString typeName;
        if (retType.isEmpty())
          typeName = "void";
        else
          typeName = retType;

        // start Method

        // write out the anchor
        out << "<h3 class=\"fn\">";
        out << QString("<a name=\"%1-method\"></a>").arg(methodName);

        out << typeNameLink(typeName) << " " << QString("%1::%2 (").arg(className).arg(methodName);     

        // Write arguments
        const QList<QByteArray>& methTypes = meth.parameterTypes();
        int pi = 0;
        foreach(QByteArray ba, meth.parameterNames())
        {
          if (pi > 0)
            out << ", ";

          QString paramType = typeNameLink(QString(methTypes[pi]));
          QString paramName = QString(ba);

          out << paramType << " " << "<i>" << paramName << "</i>";

          pi++;
        }
        out << ") </h3>" << endl;

        // Method documentation?
        if (dqo)
        {
          QString md = dqo->methodDescr(methodName, true);
          if (md.isEmpty())
            md = dqo->methodDescr(sig);

          if (!md.isEmpty())
          {
            out << "<p>" << md << "</p>" << endl;
            qDebug() << md;
          }
          else
            qDebug() << "No doc for" << className << "::" << methodName;
        }
      }
    }
  }
}

void JavaScriptAPIs::writeSignalsBody(QTextStream&, ScriptingEngine*, const QMetaObject*)
{
}

QString JavaScriptAPIs::typeNameLink(const QString& typeName)
{
  QString retName = typeName;
  QString typeClassName;
  if (mTypeMap.contains(typeName))
  {
    const QMetaObject* typeMo = mTypeMap[typeName];
    QString typeClassName = getClassInfo(typeMo, "ClassName");
    if (typeClassName.isEmpty())
      typeClassName = typeMo->className();

    retName = QString("<a href=\"%1.html\">%1</a>").arg(typeClassName);
  }

  QStringList typeParts = retName.split("::");
  if (typeParts.count() > 1)
  {
    QString className = typeParts[0];
    QString enumName = typeParts[1];

    QString typeClassName;
    const QMetaObject* typeMo = JavaScriptAPIs::lookupType(className);
    if (typeMo)
    {
      typeClassName = JavaScriptAPIs::getClassInfo(typeMo, "ClassName");
      if (typeClassName.isEmpty())
        typeClassName = typeMo->className();
    }

    if (typeClassName.isEmpty())
    {
      QString docTrollTech = QString("http://doc.trolltech.com/4.4/%1.html#%2-enum").arg(className).arg(enumName);
      retName = QString("<a href=\"%1\">%2</a>").arg(docTrollTech).arg(typeName);
    }
    else
      retName = QString("<a href=\"%1.html#%3-enum\">%2</a>").arg(className).arg(typeName).arg(enumName);
  }

  return retName;
}

bool JavaScriptAPIs::loadAPIs()
{
  bool quit = false;

  if (!mLoadedAPIs)
  {
    quit = registerScriptTypes();

    QsciLexerJavaScript* lexer = new QsciLexerJavaScript();
    JavaScriptAPIs* apis = new JavaScriptAPIs(lexer);
    apis->mAPIThread.load();
  }
  mLoadedAPIs = true;

  return quit;
}

void JavaScriptAPIs::loadIntellisenseVariables()
{
  mapVariableToAPI("spirit12", &SpiritXML::staticMetaObject);
  mapVariableToAPI("spirit14", &SpiritXML_1_4::staticMetaObject);
  mapVariableToAPI("busInterfaceType", &busInterfaceType::staticMetaObject);

  mapVariableToAPI("ccfg", &ScrCcfg::staticMetaObject);

  mapVariableToAPI("regLocRTL", &ProtoCarbonCfgRegisterLocRTL::staticMetaObject);
  mapVariableToAPI("regLogReg", &ProtoCarbonCfgRegisterLocReg::staticMetaObject);
  mapVariableToAPI("regLocC", &ProtoCarbonCfgRegisterLocConstant::staticMetaObject);
  mapVariableToAPI("regLocU", &ProtoCarbonCfgRegisterLocUser::staticMetaObject);
  mapVariableToAPI("regLocA", &ProtoCarbonCfgRegisterLocArray::staticMetaObject);
  mapVariableToAPI("regLoc", &ProtoCarbonCfgRegisterLoc::staticMetaObject);
  mapVariableToAPI("regField", &ProtoCarbonCfgRegisterField::staticMetaObject);
  mapVariableToAPI("reg", &ProtoCarbonCfgRegister::staticMetaObject);

  mapVariableToAPI("memLocP", &ProtoCarbonCfgMemoryLocPort::staticMetaObject);  
  mapVariableToAPI("memLocR", &ProtoCarbonCfgMemoryLocRTL::staticMetaObject);  
  mapVariableToAPI("memLoc", &ProtoCarbonCfgMemoryLoc::staticMetaObject);  
  mapVariableToAPI("memB", &ProtoCarbonCfgMemoryBlock::staticMetaObject);  
  mapVariableToAPI("mem", &ProtoCarbonCfgMemory::staticMetaObject);

  mapVariableToAPI("cfgGroup", &ProtoCarbonCfgGroup::staticMetaObject);
  mapVariableToAPI("cfgReg", &ProtoCarbonCfgRegister::staticMetaObject);
  mapVariableToAPI("cfgXtorInst", &ProtoCarbonCfgXtorInstance::staticMetaObject);
  mapVariableToAPI("cfgXtorParam", &ProtoCarbonCfgXtorParamInst::staticMetaObject);
  mapVariableToAPI("cfgXtorLib", &ProtoCarbonCfgXtorLib::staticMetaObject);
  mapVariableToAPI("cfgXtorPort", &ProtoCarbonCfgXtorPort::staticMetaObject);
  mapVariableToAPI("cfgXtor", &ProtoCarbonCfgXtor::staticMetaObject);
  mapVariableToAPI("cfgXtorConn", &ProtoCarbonCfgXtorConn::staticMetaObject);

  mapVariableToAPI("cfgESLPort", &ProtoCarbonCfgESLPort::staticMetaObject);
  mapVariableToAPI("cfgResetGen", &ProtoCarbonCfgResetGen::staticMetaObject);
  mapVariableToAPI("cfgClockGen", &ProtoCarbonCfgClockGen::staticMetaObject);

  mapVariableToAPI("cfgTieParam", &ProtoCarbonCfgTieParam::staticMetaObject);
  mapVariableToAPI("cfgTie", &ProtoCarbonCfgTie::staticMetaObject);

  mapVariableToAPI("cfgRTLConn", &ProtoCarbonCfgRTLConnection::staticMetaObject);
  mapVariableToAPI("cfgRTLPort", &ProtoCarbonCfgRTLPort::staticMetaObject);
}

JavaScriptAPIs::JavaScriptAPIs(QsciLexer *lexer) : QsciAPIs(lexer)
{
  mEnableIntellisense = false;
  mPrepFinished = false;
  mIntellisenseVars = true;

  mInternalClasses.clear();

  connect(this, SIGNAL(apiPreparationFinished()), this, SLOT(prepFinished()));
  connect(this, SIGNAL(apiPreparationCancelled()), this, SLOT(prepFinished()));

  loadIntellisenseVariables();

  // Internal Documentaion Classes
  mInternalClasses.append(&ScrCcfg::staticMetaObject);
  mInternalClasses.append(&ModelKit::staticMetaObject);
  mInternalClasses.append(&KitVersion::staticMetaObject);
  mInternalClasses.append(&KitVersions::staticMetaObject);
}


bool JavaScriptAPIs::isEnumType(const QMetaObject* mo, const QString& typeName)
{
  for (int i=0; i<mo->enumeratorCount(); i++)
  {
    const QMetaEnum& me = mo->enumerator(i);
    QString meName = me.name();
    if (meName == typeName)
      return true;
  }

  return false;
}

QString JavaScriptAPIs::getClassInfo(const QMetaObject* mo, const QString& name)
{
  QString value;

  for (int i=0; i<mo->classInfoCount(); i++)
  {
    const QMetaClassInfo& ci = mo->classInfo(i);
    QString ciName = ci.name();
    if (ciName == name && ci.enclosingMetaObject() == mo)
    {
      value = ci.value();
      break;
    }
  }

  return value;
}

bool JavaScriptAPIs::isMappedClass(const QString& className)
{
  const QMetaObject* mo = lookupRawClass(className);
  if (mo)
  {
    QString nativeName = getClassInfo(mo, "ClassName");
    if (nativeName.isEmpty())
      nativeName = className;
    return className == nativeName;
  }
  else
  {
    mo = lookupClass(className);
    if (mo)
    {
      QString nativeName = getClassInfo(mo, "ClassName");
      if (nativeName.isEmpty())
        nativeName = className;
      return className == nativeName;
    }
  }

  return false;
}


QString JavaScriptAPIs::mappedClassName(const QString& className)
{
  const QMetaObject* mo = lookupRawClass(className);
  if (mo)
  {
    QString nativeName = getClassInfo(mo, "ClassName");
    if (nativeName.isEmpty())
      nativeName = className;
    return nativeName;
  }
  else
  {
    mo = lookupClass(className);
    if (mo)
    {
      QString nativeName = getClassInfo(mo, "ClassName");
      if (nativeName.isEmpty())
        nativeName = className;
      return nativeName;
    }
  }

  return className;
}

QString JavaScriptAPIs::nativeClassName(const QString& className)
{
  const QMetaObject* mo = lookupRawClass(className);
  if (mo)
  {
    QString nativeName = getClassInfo(mo, "ClassName");
    if (nativeName.isEmpty())
      nativeName = className;
    return nativeName;
  }
  else
  {
    mo = lookupClass(className);
    if (mo)
    {
      QString nativeName = getClassInfo(mo, "ClassName");
      if (nativeName.isEmpty())
        nativeName = className;
      return nativeName;
    }
  }

  return "IntPtr";
}


const QMetaObject* JavaScriptAPIs::lookupClass(const QString& className)
{
  foreach (QString typeName, mTypeMap.keys())
  {
    const QMetaObject* mo = mTypeMap[typeName];
    if (mo)
    {      
      QString moclass = getClassInfo(mo, "ClassName");
      if (moclass.isEmpty())
        moclass = mo->className();
      if (className == moclass)
        return mo;
    }
  }
  return NULL;
}
const QMetaObject* JavaScriptAPIs::lookupRawClass(const QString& className)
{
  foreach (QString typeName, mTypeMap.keys())
  {
    const QMetaObject* mo = mTypeMap[typeName];
    if (mo)
    {   
      QString moclass = mo->className();     
      if (className == moclass)
        return mo;
    }
  }
  return NULL;
}

const QMetaObject* JavaScriptAPIs::lookupType(const QString& typeName)
{
  if (!typeName.isEmpty())
    return mTypeMap[typeName];
  else
    return NULL;
}

// Create Intellisense for Enums
void JavaScriptAPIs::createEnumIntellisense()
{
  ScriptingEngine engine;
  QScriptValueIterator it( engine.globalObject());
  while (it.hasNext()) 
  {
    it.next();
    QScriptValue sv = it.value();
    if (sv.isValid() && sv.isQMetaObject())
    {
      const QMetaObject* mo = sv.toQMetaObject();
      if (mo->enumeratorCount() > 0)
      {    
        for (int i=0; i<mo->enumeratorCount(); i++)
        {
          const QMetaEnum e = mo->enumerator(i);
          for (int ei=0; ei<e.keyCount(); ei++)
          {
            QString line = QString("%1::%2 [enum]")
              .arg(e.name())
              .arg(e.key(ei));
            addApi(line);
          }
        }
      }
    }
  }
}


bool JavaScriptAPIs::isDerivedFrom(const QMetaObject* baseClass, const QMetaObject* targetClass)
{
  const QMetaObject* targetSuper = targetClass->superClass();
  do
  {
    const QMetaObject* baseSuper = baseClass;
    do
    {
      if (baseSuper == targetSuper)
        return true;
      baseSuper = baseSuper->superClass();
    }
    while(baseSuper);
    targetSuper = targetSuper->superClass();
  }
  while (targetSuper);

  return false;
}


bool JavaScriptAPIs::loadAPIForVariable(const QString& varName, const QMetaObject* mo, bool enumsOnly, const QMetaObject* topObject)
{
  //  qDebug() << "Creating Intellisense API For" << varName << "class" << mo->className() << enumsOnly;

  QString enumPrefix = JavaScriptAPIs::getClassInfo(mo, "EnumPrefix");
  QStringList items;

  if (!enumPrefix.isEmpty())
  {
    for (int i=0; i<mo->enumeratorCount(); i++)
    {
      const QMetaEnum e = mo->enumerator(i);
      for (int ei=0; ei<e.keyCount(); ei++)
      {
        QString line = QString("%1::%2 [enum]")
          .arg(enumPrefix)
          .arg(e.key(ei));
        items.append(line);
      }
    }
  }
  items.sort();

  foreach (QString line, items)
    addApi(line);

  if (!enumsOnly)
  {
    items.clear();

    for (int i=0; i<mo->propertyCount(); i++)
    {
      QMetaProperty prop = mo->property(i);

      QString line = varName + "::" + QString(prop.name()) + " " + QString(prop.typeName()) + " [prop]";

      QString propertyName = prop.name();
      // First see if we defined Q_CLASSINFO to declare this type
      const QMetaObject* propType = lookupType(JavaScriptAPIs::getClassInfo(mo, propertyName));
      if (propType == NULL) // Try to see if the real type is registered?
        propType = lookupType(QString(prop.typeName()));

      // Check that the child type does not derive from this

      // Don't recurse onto self
      if (propType && 0 != strcmp(propType->className(), mo->className()))
      {
        QString newPrefix = QString("%1::%2").arg(varName).arg(propertyName);
        if (topObject == NULL)
          topObject = mo;

        // Have we seen this type before?
        if (!mTypeStack.contains(propType))
        {
          bool okToRecurse = true;
          // Check for superclass
          foreach(const QMetaObject* m, mTypeStack)
          {
            if (isDerivedFrom(m, propType))
            {
              okToRecurse = false;
              break;
            }
          }
          if (okToRecurse)
          {
            mTypeStack.push_front(propType);
            loadAPIForVariable(newPrefix, propType, false, topObject);
            mTypeStack.pop_front();
          }
        }
      }
      else
        items.append(line);
    }
    items.sort();
    foreach (QString line, items)
      addApi(line);

    // Signals
    items.clear();
    for (int i=0; i<mo->methodCount(); i++)
    {
      QMetaMethod meth = mo->method(i);
      if (meth.methodType() == QMetaMethod::Signal)
      {
        QString sig = meth.signature();

        QString methodName = sig.left(sig.indexOf('('));

        QString line = QString("%1::%2(").arg(varName).arg(methodName);
        const QList<QByteArray>& methTypes = meth.parameterTypes();
        int pi = 0;
        foreach(QByteArray ba, meth.parameterNames())
        {
          if (pi > 0)
            line += ",\n    ";

          QString paramType = QString(methTypes[pi]);
          if (isEnumType(mo, paramType))
            paramType = QString("%1.%2")
            .arg(enumPrefix)
            .arg(paramType);

          line += QString("%1 %2")
            .arg(paramType)
            .arg(QString(ba));
          pi++;
        }

        line += ")";

        line += "\nsignal";

        items.append(line);
      }
    }

    items.sort();
    foreach (QString line, items)
      addApi(line);

    // Methods
    items.clear();
    for (int i=0; i<mo->methodCount(); i++)
    {
      QMetaMethod meth = mo->method(i);
      if (meth.methodType() == QMetaMethod::Slot && meth.access() == QMetaMethod::Public)
      {
        QString sig = meth.signature();

        QString methodName = sig.left(sig.indexOf('('));

        QString line = QString("%1::%2(").arg(varName).arg(methodName);
        const QList<QByteArray>& methTypes = meth.parameterTypes();
        int pi = 0;
        foreach(QByteArray ba, meth.parameterNames())
        {
          if (pi > 0)
            line += ",\n    ";

          QString paramType = QString(methTypes[pi]);
          if (isEnumType(mo, paramType))
            paramType = QString("%1.%2")
            .arg(enumPrefix)
            .arg(paramType);

          line += QString("%1 %2")
            .arg(paramType)
            .arg(QString(ba));
          pi++;
        }

        line += ")";

        QString retType = meth.typeName();
        if (retType.isEmpty())
          line += QString("\n\n(void)");
        else
          line += QString("\n\nreturns %1").arg(retType);

        items.append(line);
      }
    }
    foreach (QString line, items)
      addApi(line);
  }

  return true;
}

void JavaScriptAPIs::autoCompletionSelected(const QString &selection)
{
  QsciAPIs::autoCompletionSelected(selection);
}


bool JavaScriptAPIs::enableIntellisense(QsciScintilla* editor, const QString& name)
{
  if (loadPrepared(name))
  {
    editor->setAutoCompletionSource(QsciScintilla::AcsAll);
    editor->setAutoCompletionThreshold(2);
    editor->setAutoCompletionReplaceWord(true);
    editor->setCallTipsStyle(QsciScintilla::CallTipsContext );
    mEnableIntellisense = true;
    return true;
  }
  else
    qDebug() << "Unable to load Intellisense";

  mEnableIntellisense = false;

  return false;
}

void JavaScriptAPIs::updateAutoCompletionList(const QStringList &context, QStringList &list)
{
  if (mEnableIntellisense && context.count() == 2)
  {
    if (mIntellisenseVars)
    {
      QString varName = context.first();
      if (!mVars.contains(varName))
      {
        foreach (QString key, mVarsMapList)
        {
          if (varName.startsWith(key))
          {
            mVars[varName] = key;
            mPrepFinished = false;
            loadAPIForVariable(varName, mMapVarToAPI[key]);
            prepare(); // start thread
            while (!mPrepFinished)
            {	        
              SleeperThread::msleep(50);
              qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
              if (mPrepFinished)
                break;
              qDebug() << "waiting for prepare...";
            }
            break;
          }
        }
      }
    }
  }
  QsciAPIs::updateAutoCompletionList(context, list);
}
