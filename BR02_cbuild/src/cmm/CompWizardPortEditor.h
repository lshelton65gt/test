#ifndef COMPWIZARDPORTEDITOR_H
#define COMPWIZARDPORTEDITOR_H

#include "util/CarbonPlatform.h"

#include <QtGui>

#include "ui_CompWizardPortEditor.h"

#include "gui/CQt.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/CarbonCfg.h"

#include "PortEditorTreeWidget.h"
#include "CarbonConsole.h"

class CarbonComponent;
//class MaxsimComponent;
class CarbonProjectWidget;
class CarbonProject;
//class MaxsimWizardWidget;

class PortsRootItem;
class TransactionPortsRootItem;
class ParametersRootItem;
class ResetsRootItem;
class ClocksRootItem;
class TiesRootItem;
class DisconnectsRootItem;
class SystemCClocksRootItem;
class CompWizardPortEditor;

class PortEditorDelegate : public QItemDelegate
{
  Q_OBJECT

public:
  PortEditorDelegate(QObject* parent=0, PortEditorTreeWidget* tw=0, CompWizardPortEditor* t=0);
  QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem&, const QModelIndex&) const;
  void setEditorData(QWidget* editor, const QModelIndex& index) const;
  void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;
  void commit(QWidget* w)
  {
    commitData(w);
    closeEditor(w);
  }

public slots:
  void currentIndexChanged(int);

private:
  PortEditorTreeWidget* mTree;
  CompWizardPortEditor* mPortEditor;
};

class CompWizardPortEditor : public QWidget
{
  Q_OBJECT

public:
  CompWizardPortEditor(QWidget *parent = 0);
  ~CompWizardPortEditor();
  
  enum EditorMode {eModeSocDesigner, eModeCoWare, eModeSystemC };

  void setEditorMode(EditorMode mode);  
  EditorMode getEditorMode() { return mEditorMode; }

  void signalRenameXtorInstance(const QString& oldName, const QString& newName)
  {
    emit renameXtorInstance(oldName, newName);
  }
  void signalDeleteXtorInstance(const QString& name)
  {
    emit deleteXtorInstance(name);
  }

  void setCcfg(CarbonCfg* cfg);
  bool populate(CarbonDB* carbonDB, bool initPorts = false);
  void deleteCurrent();
  void save();

  virtual void closeEvent(QCloseEvent* ev);

  friend class PortEditorDelegate;

  QTreeWidgetItem* getClocksRoot() const;
  QTreeWidgetItem* getPortsRoot() const;
  QTreeWidgetItem* getResetsRoot() const;
  QTreeWidgetItem* getDisconnectsRoot() const;
  QTreeWidgetItem* getTiesRoot() const;
  QTreeWidgetItem* getParamsRoot() const;
  QTreeWidgetItem* getSystemCClocksRoot() const;

  void createToolbar(QWidget* wiz);

private:
  Ui::CompWizardPortEditorClass ui;

private: // methods
  void createActions();
  void createMenus();
  void connectWidgets();
  void populateFromCcfg();
  void populatePorts();
  void populateParameters();
  void populateClocks();
  void populateSystemCClocks();
  void populateResets();
  void fillXtorCombo();

  CarbonProject* getProject();
  CarbonProjectWidget* getProjectWidget();
  CarbonComponent* getComponent();

  QDockWidget* mDockHistory;

  EditorMode mEditorMode;

signals:
  void widgetModified(bool);
  void renameXtorInstance(const QString& oldName, const QString& newName);
  void deleteXtorInstance(const QString& name);

public slots:
  void treeWidgetModified(bool value);

private slots:
  void modelCompilationFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void customContextMenuRequested(const QPoint& pos);
  void undo();
  void redo();
  void treeWidgetSelectionChanged();
  void canRedoChanged(bool enabled);
  void canUndoChanged(bool enabled);
  void actionAddXtor();
  void actionBind();
  void actionDelete();
  void actionAddParameter();
  void xtorDefFileChanged();

private: // data
  bool mUndoEnabled;
  bool mRedoEnabled;
  QUndoStack* mUndoStack;
  QUndoView* mUndoView;
  CarbonDB* mDB;
  QString mCcfgFilePath;
  CarbonCfg* mCfg;
  CarbonCfg* mCfgXtor;
  QToolBar* mToolbar;

  // Actions
  QAction* mActionRedo;
  QAction* mActionUndo;
  QAction* mActionDelete;
  QAction* mActionAddXtor;
  QAction* mActionBind;
  QAction* mActionAddParam;

  PortsRootItem* mPortsRoot;
  TransactionPortsRootItem* mTransactionPorts;
  ParametersRootItem* mParametersRoot;
  TiesRootItem* mTiesRoot;
  DisconnectsRootItem* mDisconnectsRoot;
  ResetsRootItem* mResetsRoot;
  ClocksRootItem* mClocksRoot;
  SystemCClocksRootItem* mSystemCClocksRoot;
};

#endif // COMPWIZARDPORTEDITOR_H
