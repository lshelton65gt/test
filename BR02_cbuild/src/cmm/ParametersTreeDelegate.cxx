//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "ParametersTree.h"
#include "ParametersTreeDelegate.h"
#include "Parameters.h"

// ParametersTreeDelegate
ParametersTreeDelegate::ParametersTreeDelegate(QObject *parent, ParametersTree* tree)
: QItemDelegate(parent), mParameterTree(tree)
{
}

void ParametersTreeDelegate::currentIndexChanged(int i)
{
  if (i != -1)
  {
    QComboBox *widget = qobject_cast<QComboBox *>(sender());
    commitData(widget);
    closeEditor(widget);
  }
}

QWidget *ParametersTreeDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                           const QModelIndex &index) const
{
  QTreeWidgetItem* itm = mParameterTree->itemFromIndex(index);
  if (itm == NULL)
    return NULL;

  QString value = itm->text(index.column());

  Parameter* param = NULL;
  QVariant v = itm->data(ParametersTree::colNAME, Qt::UserRole);
  if (!v.isNull())
    param = (Parameter*)v.value<void*>();

  switch (index.column())
  {
  case ParametersTree::colVALUE:
    {
      // Make the column bigger for ComboBoxes
      if (mParameterTree->header()->sectionSize(ParametersTree::colNAME) < 100)
        mParameterTree->header()->resizeSection(ParametersTree::colNAME, 100);

      return param->createEditor(mParameterTree, parent, value);
 
      break;
    }

  default:
    break;
  }
  return NULL;
}

void ParametersTreeDelegate::commitAndCloseEditor()
{
  QLineEdit *editor = qobject_cast<QLineEdit *>(sender());
  emit commitData(editor);
  emit closeEditor(editor);
}

void ParametersTreeDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit)
  {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }
}

void ParametersTreeDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                         const QModelIndex &index) const
{
  QTreeWidgetItem* itm = mParameterTree->itemFromIndex(index);
  if (itm == NULL)
    return;

  QString value = itm->text(index.column());

  Parameter* param = NULL;
  QVariant v = itm->data(ParametersTree::colNAME, Qt::UserRole);
  if (!v.isNull())
    param = (Parameter*)v.value<void*>();

  switch (index.column())
  {
  case ParametersTree::colVALUE:
    {
      param->setModelData(editor, model, index);
      QString sValue = param->getValue().toString();
      if (sValue != value)
        mParameterTree->signalValuesChanged();
    }
    break;
  default:
    break;
  }
}
