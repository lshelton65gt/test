#ifndef WIZPGTEMPLATES_H
#define WIZPGTEMPLATES_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QtGui>
#include <QWidget>
#include <QWizard>
#include "WizardPage.h"

#include "ui_WizPgTemplates.h"

class WizPgTemplates : public WizardPage
{
  Q_OBJECT

public:
  WizPgTemplates(QWidget *parent = 0);
  ~WizPgTemplates();

  void populate();

private slots:
  void on_lineEditTemplateName_textChanged(const QString &);
  void itemSelectionChanged();

protected:
  virtual bool serialize(QDomDocument&, QDomElement&, XmlErrorHandler*);
  virtual void initializePage();

private:
  Ui::WizPgTemplatesClass ui;
  QWidget* mParent;
};

#endif // WIZPGTEMPLATES_H
