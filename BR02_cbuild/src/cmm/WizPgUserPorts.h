#ifndef WIZPGUSERPORTS_H
#define WIZPGUSERPORTS_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QWizardPage>
#include "WizardPage.h"
#include "ui_WizPgUserPorts.h"
#include "Ports.h"

class WizPgUserPorts : public WizardPage
{
  Q_OBJECT

public:
  WizPgUserPorts(QWidget *parent = 0);
  ~WizPgUserPorts();
  int nextId() const;
  virtual void initializePage();

private slots:
  void on_pushButtonAddPort_clicked();
  void on_pushButtonDeletePort_clicked();
  void on_treeWidget_itemSelectionChanged();

private:
  void updateButtons();
  virtual bool serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*);
  void addPort(Ports* ports, const QString& portName, const QString& tieValue, int portWidth, Ports::PortLocation loc);

private:
  Ui::WizPgUserPortsClass ui;
};

#endif // WIZPGUSERPORTS_H
