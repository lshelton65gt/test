//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "ModelKitUserWizard.h"

void UserActionUI::setComplete(bool value)
{
  mText->setEnabled(value);
  mPicture->setEnabled(value);
}

UserActionUI* UserActionUI::createAction(int n, QWidget* parent, QVBoxLayout* parentLayout, KitUserAction* action)
{
  UserActionUI* ui = new UserActionUI();

  ui->mPicture = new QLabel(parent);
  ui->mPicture->setPixmap(QPixmap(QString::fromUtf8(":/cmm/Resources/CheckMark.png")));
  ui->mPicture->setEnabled(false);

  ui->mText = new QLabel(parent);

  ui->mText->setText(QString("Step %1: %2 (%3)")
    .arg(n)
    .arg(action->getName())
    .arg(action->getDescription())
    );
  ui->mText->setAlignment(Qt::AlignLeft|Qt::AlignVCenter);
  ui->mText->setEnabled(false);

  ui->mLayout = new QHBoxLayout();
  ui->mLayout->addWidget(ui->mPicture);
  ui->mLayout->addWidget(ui->mText);
  QSpacerItem* spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  ui->mLayout->addItem(spacerItem);

  parentLayout->addLayout(ui->mLayout);

  return ui;
}

MKUWPageUserActions::MKUWPageUserActions(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);

  mIsValid = true;
  mStepNumber = 0;
  mInitialized = false;

  mProjectWidget = theApp->getContext()->getCarbonProjectWidget();
  mProject = mProjectWidget->project();

  setTitle("User Actions");
  setSubTitle("Please complete the following questions/dialogs, click Next to continue");

  ui.progressBar->setVisible(false);
  ui.label->setText("");

  CQT_CONNECT(mProjectWidget->getConsole(),
    stdoutChanged(CarbonConsole::CommandType, const QString&, const QString&),
    this,
    stdoutChanged(CarbonConsole::CommandType, const QString&, const QString&));
 
  CQT_CONNECT(mProjectWidget->getConsole(),
    stderrChanged(CarbonConsole::CommandType, const QString&, const QString&),
    this,
    stdoutChanged(CarbonConsole::CommandType, const QString&, const QString&));
}

MKUWPageUserActions::~MKUWPageUserActions()
{
}

void MKUWPageUserActions::stdoutChanged(CarbonConsole::CommandType, const QString& str, const QString&)
{
 //#  1  Elapsed: 0:00:00 Complete:  0% 
  QRegExp statx("#\\s*([0-9]+)\\s+Elapsed: [0-9]+:[0-9]+:[0-9]+ Complete:\\s+([0-9]+)%");
  QRegExp staty(".*Comp:([0-9]+)\\% Mem:.*");
  if (statx.exactMatch(str))
  {
    int phase=0;
    UtString cap1;
    cap1 << statx.cap(1);
    UtIStringStream Phase(cap1);
    Phase >> UtIO::dec >> phase;

    int percent=0;
    UtString cap2;
    cap2 << statx.cap(2);
    UtIStringStream Percent(cap2);
    Percent >> UtIO::dec >> percent;

    if (phase == 1 && percent == 0)
      setProgressRange(0, 125);

    setProgress(percent);
    setStatusMessage("Compiling Model");
    return;
  }
  else if (staty.exactMatch(str))
  {
    int percent = staty.cap(1).toInt();
    setProgress(percent);
    setStatusMessage("Compiling Model");
    return;
  }
  
  QString s = str;

  if (s.startsWith("FINISHED MODEL COMPILATION"))
    setStatusMessage("Compiling Component(s)");

  ui.textBrowser->append(str);
}

void MKUWPageUserActions::setStatusMessage(const QString& msg)
{
  ui.label->setText(msg);
}

void MKUWPageUserActions::setProgress(int value)
{
  ui.progressBar->setVisible(true);
  ui.progressBar->setValue(value);
}

void MKUWPageUserActions::setProgressRange(int low, int high)
{
  ui.progressBar->setVisible(true);
  ui.progressBar->setValue(0);
  ui.progressBar->setMinimum(low);
  ui.progressBar->setMaximum(high);
}

void MKUWPageUserActions::initializePage()
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  UtString armProjDir;
  OSConstructFilePath(&armProjDir, proj->getProjectDirectory(), "ARM_Logical");

  proj->setARMKitRoot(armProjDir.c_str());


  if (!mInitialized)
  {
    KitUserContext* userContext = wiz->getUserContext();
    CQT_CONNECT(userContext, updateStatusMessage(const QString&), this, setStatusMessage(const QString&));
    CQT_CONNECT(userContext, updateProgress(int), this, setProgress(int));
    CQT_CONNECT(userContext, updateProgressRange(int,int), this, setProgressRange(int,int));
    mInitialized = true;
  }

  // We could have started on this page, so hide it now.
  QMainWindow* mw = theApp->getContext()->getMainWindow();
  if (!theApp->checkArgument("-debugWindows"))
    mw->hide();
 
  mStepNumber = 0;

  foreach (UserActionUI* uaui, mActionMap.values())
    ui.verticalLayout->removeItem(uaui->getLayout());

  mActionMap.clear();

  KitManifest* manifest = wiz->getManifest();
  int stepNumber = 1;
  foreach (KitUserAction* action, manifest->getPreUserActions())
  {
    UserActionUI* uaui = UserActionUI::createAction(stepNumber, this, ui.verticalLayout, action);
    mActionMap[action] = uaui;

    if (stepNumber == 1)
      uaui->setComplete(true);

    stepNumber++;
  }

  mIsValid = true;

  emit completeChanged();

  if (wiz->getAnswers())
    wiz->next();
}

void MKUWPageUserActions::addBuiltinVariables(KitUserContext* userContext)
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  KitManifest* manifest = wiz->getManifest();

  if (manifest->getAskRoot())
  {
    QString armKitRoot = wizard()->field("armKitRoot").toString();
    QString kitPath = mProject->getWindowsEquivalentPath(armKitRoot);
    userContext->setVariable("${ARM_ROOT}", kitPath);
  }

  UtString armProjDir;
  OSConstructFilePath(&armProjDir, mProject->getProjectDirectory(), "ARM_Logical");

  userContext->setVariable("${CARBON_PROJECT_ARM}", armProjDir.c_str());

  // CARBON Variables
  QString carbonConfig = QString("%1/%2").arg(mProject->getActivePlatform()).arg(mProject->getActiveConfiguration());

  QString carbonOutput = QString("%1/%2").arg(mProject->getProjectDirectory()).arg(carbonConfig);

  userContext->setVariable("${CARBON_PROJECT}", mProject->getProjectDirectory());
  userContext->setVariable("${CARBON_CONFIGURATION}", carbonConfig);
  userContext->setVariable("${CARBON_OUTPUT}", carbonOutput);

}
bool MKUWPageUserActions::validatePage()
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  KitManifest* manifest = wiz->getManifest();
  KitUserContext* userContext = wiz->getUserContext();

  mIsValid = false;
  emit completeChanged();

  userContext->setPlaybackMode(wiz->getAnswers() != NULL);
 
  addBuiltinVariables(userContext);

  wiz->setWhen(ModelKitUserWizard::Precompilation);

  mIsValid = true;
  int stepNumber = 1;
  int numSteps = manifest->getPreUserActions().count();
  foreach (KitUserAction* action, manifest->getPreUserActions())
  {
    QString status = QString("Executing step %1 of %2").arg(stepNumber).arg(numSteps);
    setStatusMessage(status);
    UserActionUI* uaui = mActionMap[action];
    bool completed = action->execute(wiz, this, userContext);
    uaui->setComplete(completed);

    stepNumber++;
    setProgressRange(1, numSteps);
    setProgress(stepNumber);

    if (!completed)
    {
      mIsValid = false;
      break;
    }
  }

  emit completeChanged();
  return mIsValid;
}

bool MKUWPageUserActions::isComplete() const
{
  return mIsValid;
}


int MKUWPageUserActions::nextId() const
{
  return ModelKitUserWizard::PageFinished;
}
