//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "WizPgTemplates.h"

#ifdef Q_OS_WIN
#pragma warning( disable : 4996)
#endif

#include "WizardTemplate.h"
#include "WizardContext.h"
#include "Parameters.h"
#include "Template.h"
#include "MemoryWizard.h"
#include "CarbonProjectWidget.h"

#include <QDebug>

WizPgTemplates::WizPgTemplates(QWidget *parent)
    : WizardPage(parent)
{
  mParent = parent;

  ui.setupUi(this);

  ui.treeWidget->setRootIsDecorated(false);

  ui.treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
  ui.treeWidget->setSelectionBehavior(QAbstractItemView::SelectRows);

  ui.treeWidget->header()->setResizeMode(0, QHeaderView::ResizeToContents);
  ui.treeWidget->header()->setResizeMode(1, QHeaderView::Interactive);

  populate();

  connect(ui.treeWidget, SIGNAL(itemSelectionChanged()), this, SLOT(itemSelectionChanged()));

  registerField("templateName*", ui.lineEditTemplateName);
  registerField("templatePath", new QLineEdit());
}

WizPgTemplates::~WizPgTemplates()
{

}


bool WizPgTemplates::serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*)
{
  QDomElement tag = doc.createElement("WizardStep");
  tag.setAttribute("name", objectName());
  parent.appendChild(tag);

  addElement(doc, tag, "TemplateName", wizard()->field("templateName").toString());
  addElement(doc, tag, "TemplatePath", wizard()->field("templatePath").toString());

  return true;
}

void WizPgTemplates::populate()
{
  QStringList wizDirs;
  if (getenv("CARBON_MEMWIZARD"))
  {
    QString wizDirEnv = getenv("CARBON_MEMWIZARD");
#ifdef Q_OS_WIN
    wizDirs = wizDirEnv.split(";");
#else
    wizDirs = wizDirEnv.split(":");
#endif
  }
  
  QString homeTemplates = QString("%1/lib/templates").arg(getenv("CARBON_HOME"));

  wizDirs.append(homeTemplates);

  foreach (QString wizDir, wizDirs)
  {
    QDir qdir(wizDir);
    
    qdir.setFilter(QDir::Files);
    QStringList filters;
    filters << "*.xml";
    qdir.setNameFilters(filters);

    QStringList files = qdir.entryList();
    foreach (QString file, files)
    {
      QFileInfo fi(wizDir + "/" + file);
      qDebug() << fi.absoluteFilePath();
      WizardTemplate* wizTemplate = new WizardTemplate(mParent);

      if (wizTemplate->readTemplate(fi.absoluteFilePath()))
      {
        Template* templ = wizTemplate->getContext()->getTemplate();
        QTreeWidgetItem* item = new QTreeWidgetItem(ui.treeWidget);
        item->setText(0, templ->getName());
        item->setText(1, templ->getDescription());
        QVariant qv = qVariantFromValue((void*)wizTemplate);
        item->setData(0, Qt::UserRole, qv);
      }
    }
  }
}

void WizPgTemplates::itemSelectionChanged()
{
  QTreeWidgetItem* item = ui.treeWidget->currentItem();

  ui.lineEditTemplateName->setText(item->text(0));

  if (item)
  {
    WizardTemplate* templ = NULL;
    QVariant v = item->data(0, Qt::UserRole);
    if (!v.isNull())
    {
      templ = (WizardTemplate*)v.value<void*>();
      MemoryWizard* wiz = (MemoryWizard*)wizard();
      wiz->setTemplate(templ);
      setField("templatePath", templ->getTemplatePath());
    }
  }
}

void WizPgTemplates::initializePage()
{
  setTitle("Available Remodeling Templates");
  setSubTitle("Select a template to begin the remodeling procedure.");

  MemoryWizard* wiz = (MemoryWizard*)wizard();
  QString replayFile = wiz->getReplayFile();

  if (replayFile.length() > 0)
  {
    wiz->hide();

    QDomElement wizElem = wiz->findWizardStep(objectName());
    if (!wizElem.isNull())
    {
      bool performNext = false;
      QDomNode n = wizElem.firstChild();
      while(!n.isNull())
      {
        QDomElement e = n.toElement(); // try to convert the node to an element.
        if(!e.isNull()) 
        {
          qDebug() << e.tagName();
          QString templateName;
          if (e.tagName() == "TemplateName")
          {
            templateName = e.text();
            foreach (QTreeWidgetItem* item, ui.treeWidget->findItems(templateName, Qt::MatchExactly, 0))
            {
              ui.treeWidget->setCurrentItem(item);
              item->setSelected(true);
              performNext = true;
            }
            if (!performNext)
            {
              QString msg = QString("No such template: '%1', aborting").arg(templateName);
              QMessageBox::critical(this, MODELWIZARD_TITLE, msg);
            }
          }
        }
        n = n.nextSibling();
      }
      if (performNext)
        wiz->postNext();
      else
        wiz->stopReplay();
    }
  }
}

void WizPgTemplates::on_lineEditTemplateName_textChanged(const QString &)
{
}
