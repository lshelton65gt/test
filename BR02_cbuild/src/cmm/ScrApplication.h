#ifndef __SCRAPPL__
#define __SCRAPPL__

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>

#include <QtScript/QScriptable>
#include <QtScript/QScriptClass>
#include <QtScript/QScriptString>
#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>

#include "CarbonConsole.h"
#include "DesignXML.h"
#include "cmm.h"
#include "EmbeddedMono.h"

class ScrProject;
class JavaScriptAPIs;

// File: Application
//
// The Application object that represents Carbon Model Studio
class ScrApplication : public QObject, public QScriptable, public QScriptClass
{
  Q_OBJECT

public:
  QScriptValue constructor() { return mCtor; }
  QScriptValue newInstance(int size = 0);
  QScriptValue newInstance(const QByteArray &ba);

  virtual QueryFlags queryProperty(const QScriptValue &object,
                                     const QScriptString &name,
                                     QueryFlags flags, uint *id)
  {
    qDebug() << "query property" << name.toString();
    return QScriptClass::queryProperty(object, name, flags, id);
  }

  virtual QScriptValue property(const QScriptValue &object,
                                  const QScriptString &name, uint id)
  {
    qDebug() << "property" << name.toString();
    return QScriptClass::property(object, name, id);
  }

  virtual void setProperty(QScriptValue &object, const QScriptString &name,
                             uint id, const QScriptValue &value)
  {
    qDebug() << "setProperty" << name.toString() << value.toString();
    QScriptClass::setProperty(object, name, id, value);
  }
  
  QScriptValue::PropertyFlags propertyFlags(const QScriptValue &object, const QScriptString &name, uint id)
  {
    qDebug() << "propertyFlags" << name.toString();
    return QScriptClass::propertyFlags(object, name, id);
  }
  QScriptValue prototype() const;

public:
  ScrApplication(QScriptEngine* engine);
  ~ScrApplication(){}

private:
  static QScriptValue construct(QScriptContext *ctx, QScriptEngine *eng);

private:
  QScriptValue mCtor;
  QScriptValue mProto;  
};

class ScrApplicationProto : public DocumentedQObject, public QScriptable
{
  Q_OBJECT
  Q_CLASSINFO("ClassName", "Application");

  Q_PROPERTY(QMenuBar* menuBar READ getMenuBar)
  Q_PROPERTY(QStatusBar* statusBar READ getStatusBar)
  Q_PROPERTY(CarbonConsole* console READ getConsole)
  Q_PROPERTY(QString release READ getRelease)
  Q_PROPERTY(QString softwareVersion READ getSoftwareVersion)
  Q_PROPERTY(ScrProject* project READ getProject)
  Q_PROPERTY(ScrProject* currentProject READ getCurrentProject)
  Q_PROPERTY(bool isUnix READ getIsUnix)
  Q_PROPERTY(QString workingDirectory READ getWorkingDirectory WRITE setWorkingDirectory)
 
// Documentation
protected:
  virtual QString methodDescr(const QString& methodName, bool formatHtml) const;
  virtual QString propertyDescr(const QString& propName, bool formatHtml) const;

private slots:
  void makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType);

public slots:
  bool getIsUnix()
  {
#if pfWINDOWS
    return false;
#else
    return true;
#endif
  }
  QString getWorkingDirectory() { return theApp->getWorkingDirectory(); }
  void setWorkingDirectory(const QString& dir)
  {
    UtString err;
    UtString newdir;
    newdir << dir;

    OSChdir(newdir.c_str(), &err);
  }
  bool checkArgument(const QString& argName);
  QString argumentValue(const QString& argName);
  void shutdown(int exitCode=0);
  ScrProject* createProject(const QString& projectName, const QString& projectDirectory);
  ScrProject* getProject();
  ScrProject* getCurrentProject();
  QString getRelease() const;
  QString getSoftwareVersion() const;
  CarbonConsole* getConsole() const;
  QMenuBar* getMenuBar() const { return theApp->menuBar(); }
  QStatusBar* getStatusBar() const { return theApp->statusBar(); }

  // compares two strings of the form "1.2222.1.333" vs "1.2222" segment by segment
  // returns -1: if left is an earlier version than right; (e.g. 1.2222 1.2224)
  //          0: if left and right are exactly same version;
  //         +1: if left is a later version than right (e.g. 1.2222.1.4444 1.2222)
  int compareSoftwareVersionSegments(const QString& leftQ, const QString& rightQ) const;

  void setenv(const QString& name, const QString& value)
  {
    UtString* var = new UtString();
    *var << name << "=" << value;
    ::putenv((char*)var->c_str());
  }
  void setenv(const QString& var)
  {
    UtString* v = new UtString();
    *v << var;
    ::putenv((char*)v->c_str());
  }
  QString getenv(const QString& varName)
  {
    UtString vn;
    vn << varName;
    QString result = ::getenv(vn.c_str());
    return result;
  }
  void recompile();
  void compile();
  void compileNoWait();
  int compileTarget(const QString& targetName);

  bool checkoutRuntimeLicense(const QString& licFeature);
  bool checkoutSimulationRuntime(const QString& licFeature);

public:
  ScrApplicationProto(QObject *parent = 0);
  static void registerTypes(QScriptEngine* engine);
  static void registerIntellisense(JavaScriptAPIs* apis);
  static void registerScriptTypes(QMap<QString, const QMetaObject*>& map);

private:
  bool mCompilationInProgress;
  bool mMakefileFinished;
};



#endif
