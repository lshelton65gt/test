#ifndef __MemEditorCOMMANDS_H_
#define __MemEditorCOMMANDS_H_

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"
#include "cfg/CarbonCfg.h"
#include "CcfgHelper.h"
#include <QtGui>

#include "CompWizardMemEditor.h"
#include "MemEditorTreeNodes.h"


class CmdRenameESLMemory : public MemTreeUndoCommand
{
public:
  CmdRenameESLMemory(ESLMemory* item, 
    CarbonCfgMemory* mem, const QString& oldName, 
    const QString& newName, QUndoCommand* parent = 0);
  
  void undo();
  void redo();

private:
  CarbonCfgMemory* mMemory;
  QString mSubComponentName;
  CarbonCfg* mCfg;
  QString mOldName;
  QString mNewName;
};

class CmdChangeESLMemoryBlockBaseAddress : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryBlockBaseAddress(ESLMemoryBlockBase* item, 
    CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, UInt64 oldValue, 
    UInt64 newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();
    mBlockName = block->getName();

    setText(QString("Change Memory %1 Block %2 Base Address from %3 to %4")
      .arg(mMemoryName)
      .arg(mBlockName)
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);

    block->putBase(mOldValue);
    mOldValue = block->getBase();

    ESLMemoryBlockBase* item = dynamic_cast<ESLMemoryBlockBase*>(getItem());
    item->setValue(mOldValue);
    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 

    item->check(mem, block, true);
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);

    block->putBase(mNewValue);
    mNewValue = block->getBase();


    ESLMemoryBlockBase* item = dynamic_cast<ESLMemoryBlockBase*>(getItem());
    item->setValue(mNewValue);

    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);
    item->check(mem, block, true);
  }

private:
  CarbonCfg* mCfg;
  QString mMemoryName;
  QString mBlockName;
  UInt64 mOldValue;
  UInt64 mNewValue;
};


class CmdChangeESLMemoryBlockLocDisplayAttrStartWordOffset : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryBlockLocDisplayAttrStartWordOffset(ESLMemoryBlockLocDisplayAttrStartWordOffset* item, 
    CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc, UInt64 oldValue, 
    UInt64 newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();
    mBlockName = block->getName();
    mItem = item;

    CcfgHelper ccfg(mCfg);
    mLocIndex = ccfg.findMemoryBlockLocationIndex(block, loc);

    setText(QString("Change Memory %1 Block %2 Display Start Word Offset from %3 to %4")
      .arg(mMemoryName)
      .arg(mBlockName)
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLoc* loc = ccfg.findMemoryBlockLocationFromIndex(block, mLocIndex);

    loc->putDisplayStartWordOffset(mOldValue);
    mOldValue = loc->getDisplayStartWordOffset();

    mItem->setValue(mOldValue);
    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(false); 

	ESLMemoryBlockLocationDisplayAttr* parentItem = dynamic_cast<ESLMemoryBlockLocationDisplayAttr*>(mItem->getItem()->parent());
    if (parentItem)
      parentItem->check(true);
    else
	  mItem->check(mem, block, loc, true);
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLoc* loc = ccfg.findMemoryBlockLocationFromIndex(block, mLocIndex);

    loc->putDisplayStartWordOffset(mNewValue);
    mNewValue = loc->getDisplayStartWordOffset();

    mItem->setValue(mNewValue);

    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(true);

    ESLMemoryBlockLocationDisplayAttr* parentItem = dynamic_cast<ESLMemoryBlockLocationDisplayAttr*>(mItem->getItem()->parent());
    if (parentItem)
      parentItem->check(true);
    else
	  mItem->check(mem, block, loc, true);
  }

 

private:
  ESLMemoryBlockLocDisplayAttrStartWordOffset* mItem;
  CarbonCfg* mCfg;
  QString mMemoryName;
  QString mBlockName;
  int mLocIndex;
  UInt64 mOldValue;
  UInt64 mNewValue;
};

class CmdChangeESLMemoryBlockLocDisplayAttrEndWordOffset : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryBlockLocDisplayAttrEndWordOffset(ESLMemoryBlockLocDisplayAttrEndWordOffset* item, 
    CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc, UInt32 oldValue, 
    UInt32 newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    CcfgHelper ccfg(mCfg);

    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();
    mBlockName = block->getName();
    mItem = item;
    mLocIndex = ccfg.findMemoryBlockLocationIndex(block, loc);

    setText(QString("Change Memory %1 Block %2 Display End Word Offset from %3 to %4")
      .arg(mMemoryName)
      .arg(mBlockName)
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLoc* loc = ccfg.findMemoryBlockLocationFromIndex(block, mLocIndex);

    loc->putDisplayEndWordOffset(mOldValue);
    mOldValue = loc->getDisplayEndWordOffset();

    mItem->setValue(mOldValue);
    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(false); 

    ESLMemoryBlockLocationDisplayAttr* parentItem = dynamic_cast<ESLMemoryBlockLocationDisplayAttr*>(mItem->getItem()->parent());
    if (parentItem)
      parentItem->check(true);
    else
	  mItem->check(mem, block, loc, true);
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);

    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLoc* loc = ccfg.findMemoryBlockLocationFromIndex(block, mLocIndex);

    loc->putDisplayEndWordOffset(mNewValue);
    mNewValue = loc->getDisplayEndWordOffset();


    mItem->setValue(mNewValue);

    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(true);

  
    ESLMemoryBlockLocationDisplayAttr* parentItem = dynamic_cast<ESLMemoryBlockLocationDisplayAttr*>(mItem->getItem()->parent());
    if (parentItem)
      parentItem->check(true);
    else
	  mItem->check(mem, block, loc, true);
  }

private:
  ESLMemoryBlockLocDisplayAttrEndWordOffset* mItem;
  CarbonCfg* mCfg;
  QString mMemoryName;
  QString mBlockName;
  int mLocIndex;
  UInt64 mOldValue;
  UInt64 mNewValue;
};


class CmdChangeESLMemoryBlockLocDisplayAttrLSB : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryBlockLocDisplayAttrLSB(ESLMemoryBlockLocDisplayAttrLSB* item, 
    CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc, UInt32 oldValue, 
    UInt32 newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();
    mBlockName = block->getName();
    CcfgHelper ccfg(mCfg);
    mItem = item;
    mLocIndex = ccfg.findMemoryBlockLocationIndex(block, loc);

    setText(QString("Change Memory %1 Block %2 Display LSB from %3 to %4")
      .arg(mMemoryName)
      .arg(mBlockName)
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLoc* loc = ccfg.findMemoryBlockLocationFromIndex(block, mLocIndex);

    loc->putDisplayLsb(mOldValue);
    mOldValue = loc->getDisplayLsb();

    mItem->setValue(mOldValue);
    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(false); 
    ESLMemoryBlockLocationDisplayAttr* parentItem = dynamic_cast<ESLMemoryBlockLocationDisplayAttr*>(mItem->getItem()->parent());
    if (parentItem)
      parentItem->check(true);
    else
	  mItem->check(mem, block, loc, true);
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLoc* loc = ccfg.findMemoryBlockLocationFromIndex(block, mLocIndex);

    loc->putDisplayLsb(mNewValue);
    mNewValue = loc->getDisplayLsb();

    mItem->setValue(mNewValue);

    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(true);
    
    ESLMemoryBlockLocationDisplayAttr* parentItem = dynamic_cast<ESLMemoryBlockLocationDisplayAttr*>(mItem->getItem()->parent());
    if (parentItem)
      parentItem->check(true);
    else
	  mItem->check(mem, block, loc, true);  
  }

private:
  ESLMemoryBlockLocDisplayAttrLSB* mItem;
  CarbonCfg* mCfg;
  QString mMemoryName;
  QString mBlockName;
  int mLocIndex;
  UInt32 mOldValue;
  UInt32 mNewValue;
};

class CmdChangeESLMemoryBlockLocDisplayAttrMSB : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryBlockLocDisplayAttrMSB(ESLMemoryBlockLocDisplayAttrMSB* item, 
    CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLoc* loc, UInt32 oldValue, 
    UInt32 newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();
    mBlockName = block->getName();
    mItem = item;
    CcfgHelper ccfg(mCfg);
    mLocIndex = ccfg.findMemoryBlockLocationIndex(block, loc);

    setText(QString("Change Memory %1 Block %2 Display MSB from %3 to %4")
      .arg(mMemoryName)
      .arg(mBlockName)
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLoc* loc = ccfg.findMemoryBlockLocationFromIndex(block, mLocIndex);

    loc->putDisplayMsb(mOldValue);
    mOldValue = loc->getDisplayMsb();

    mItem->setValue(mOldValue);
    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(false); 

    ESLMemoryBlockLocationDisplayAttr* parentItem = dynamic_cast<ESLMemoryBlockLocationDisplayAttr*>(mItem->getItem()->parent());
    if (parentItem)
      parentItem->check(true);
    else
	  mItem->check(mem, block, loc, true);
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLoc* loc = ccfg.findMemoryBlockLocationFromIndex(block, mLocIndex);

    loc->putDisplayMsb(mNewValue);
    mNewValue = loc->getDisplayMsb();


    mItem->setValue(mNewValue);

    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(true);

    ESLMemoryBlockLocationDisplayAttr* parentItem = dynamic_cast<ESLMemoryBlockLocationDisplayAttr*>(mItem->getItem()->parent());
    if (parentItem)
      parentItem->check(true);
    else
	  mItem->check(mem, block, loc, true);
  }

private:
  ESLMemoryBlockLocDisplayAttrMSB* mItem;
  CarbonCfg* mCfg;
  QString mMemoryName;
  QString mBlockName;
  int mLocIndex;
  UInt32 mOldValue;
  UInt32 mNewValue;
};



class CmdChangeESLMemoryBlockSize : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryBlockSize(ESLMemoryBlockSize* item, 
    CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, UInt64 oldValue, 
    UInt64 newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();
    mBlockName = block->getName();

    setText(QString("Change Memory %1 Block %2 Size from %3 to %4")
      .arg(mMemoryName)
      .arg(mBlockName)
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);

    block->putSize(mOldValue);
    mOldValue = block->getSize();

    ESLMemoryBlockSize* item = dynamic_cast<ESLMemoryBlockSize*>(getItem());
    item->setValue(mOldValue);
    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 
    item->check(mem, block, true);
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);

    block->putSize(mNewValue);
    mNewValue = block->getSize();


    ESLMemoryBlockSize* item = dynamic_cast<ESLMemoryBlockSize*>(getItem());
    item->setValue(mNewValue);

    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);
    item->check(mem, block, true);
  }

private:
  CarbonCfg* mCfg;
  QString mMemoryName;
  QString mBlockName;
  UInt64 mOldValue;
  UInt64 mNewValue;
};


class CmdChangeESLMemoryBlockName : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryBlockName(ESLMemoryBlock* item, 
    CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, const QString& oldValue, 
    const QString& newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();

    setText(QString("Change Memory %1 Block %2 Name from %3 to %4")
      .arg(mMemoryName)
      .arg(block->getName())
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mNewValue);

    UtString v; v << mOldValue;

    block->putName(v.c_str());
    mOldValue = block->getName();

    ESLMemoryBlock* item = dynamic_cast<ESLMemoryBlock*>(getItem());
    item->setName(mOldValue);
    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mOldValue);

    UtString v; v << mNewValue;
    block->putName(v.c_str());
    mNewValue = block->getName();


    ESLMemoryBlock* item = dynamic_cast<ESLMemoryBlock*>(getItem());
    item->setName(mNewValue);

    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);
  }

private:
  CarbonCfg* mCfg;
  QString mMemoryName;
  QString mOldValue;
  QString mNewValue;
};


class CmdChangeESLMemoryWidth : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryWidth(ESLMemory* item, 
    CarbonCfgMemory* mem, UInt32 oldValue, 
    UInt32 newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();

    setText(QString("Change Memory Width from %1 to %2")
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);

    mem->putWidth(mOldValue);
    mOldValue = mem->getWidth();

    ESLMemory* item = dynamic_cast<ESLMemory*>(getItem());
    item->setWidth(mOldValue);
    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 

    item->check(true);
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);

    mem->putWidth(mNewValue);

    mNewValue = mem->getWidth();
   
    ESLMemory* item = dynamic_cast<ESLMemory*>(getItem());
    item->setWidth(mNewValue);

    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);
    item->check(true);
  }

private:
  CarbonCfg* mCfg;
  QString mMemoryName;
  UInt32 mOldValue;
  UInt32 mNewValue;
};

class CmdChangeESLMemoryBigEndian : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryBigEndian(ESLMemoryBigEndian* item, 
    CarbonCfgMemory* mem, bool oldValue, 
    bool newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();

    setText(QString("Change Memory Big Endian from %1 to %2")
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);

    mem->putBigEndian(mOldValue);
    mOldValue = mem->getBigEndian();

    ESLMemoryBigEndian* item = dynamic_cast<ESLMemoryBigEndian*>(getItem());
    item->setValue(mOldValue);
    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);

    mem->putBigEndian(mNewValue);

    mNewValue = mem->getBigEndian();
   
    ESLMemoryBigEndian* item = dynamic_cast<ESLMemoryBigEndian*>(getItem());
    item->setValue(mNewValue);

    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);
  }

private:
  CarbonCfg* mCfg;
  QString mMemoryName;
  bool mOldValue;
  bool mNewValue;
};

// LocDisplayRTL Attributes


class CmdChangeESLMemoryBlockLocDisplayAttrStartWordOffsetRTL : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryBlockLocDisplayAttrStartWordOffsetRTL (ESLMemoryBlockLocDisplayAttrStartWordOffsetRTL* item, 
    CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLocRTL* loc, UInt32 oldValue, 
    UInt32 newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();
    mBlockName = block->getName();
    mItem = item;
    CcfgHelper ccfg(mCfg);
    mLocIndex = ccfg.findMemoryBlockLocationRTLIndex(block, loc);

    setText(QString("Change Memory %1 Block %2 RTL Start Word Offset from %3 to %4")
      .arg(mMemoryName)
      .arg(mBlockName)
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLocRTL* loc = ccfg.findMemoryBlockLocationRTLFromIndex(block, mLocIndex);

    loc->putRTLStartWordOffset(mOldValue);
    mOldValue = loc->getRTLStartWordOffset();

    mItem->setValue(mOldValue);
    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(false); 
  	mItem->check(mem, block, loc, true);
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLocRTL* loc = ccfg.findMemoryBlockLocationRTLFromIndex(block, mLocIndex);

    loc->putRTLStartWordOffset(mNewValue);
    mNewValue = loc->getRTLStartWordOffset();

    mItem->setValue(mNewValue);

    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(true);
  	mItem->check(mem, block, loc, true);
  }

private:
  ESLMemoryBlockLocDisplayAttrStartWordOffsetRTL* mItem;
  CarbonCfg* mCfg;
  QString mMemoryName;
  QString mBlockName;
  int mLocIndex;
  UInt32 mOldValue;
  UInt32 mNewValue;
};

class CmdChangeESLMemoryBlockLocDisplayAttrEndWordOffsetRTL  : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryBlockLocDisplayAttrEndWordOffsetRTL(ESLMemoryBlockLocDisplayAttrEndWordOffsetRTL* item, 
    CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLocRTL* loc, UInt64 oldValue, 
    UInt64 newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();
    mBlockName = block->getName();
    mItem = item;
    CcfgHelper ccfg(mCfg);
    mLocIndex = ccfg.findMemoryBlockLocationRTLIndex(block, loc);

    setText(QString("Change Memory %1 Block %2 RTL End Word Offset from %3 to %4")
      .arg(mMemoryName)
      .arg(mBlockName)
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLocRTL* loc = ccfg.findMemoryBlockLocationRTLFromIndex(block, mLocIndex);

    loc->putRTLEndWordOffset(mOldValue);
    mOldValue = loc->getRTLEndWordOffset();

    mItem->setValue(mOldValue);
    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(false); 
  	mItem->check(mem, block, loc, true);
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLocRTL* loc = ccfg.findMemoryBlockLocationRTLFromIndex(block, mLocIndex);

    loc->putRTLEndWordOffset(mNewValue);
    mNewValue = loc->getRTLEndWordOffset();

    mItem->setValue(mNewValue);

    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(true);
  	mItem->check(mem, block, loc, true);
  }

private:
  ESLMemoryBlockLocDisplayAttrEndWordOffsetRTL* mItem;
  CarbonCfg* mCfg;
  QString mMemoryName;
  QString mBlockName;
  int mLocIndex;
  UInt64 mOldValue;
  UInt64 mNewValue;
};


class CmdChangeESLMemoryBlockLocDisplayAttrLSBRTL  : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryBlockLocDisplayAttrLSBRTL (ESLMemoryBlockLocDisplayAttrLSBRTL * item, 
    CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLocRTL* loc, UInt32 oldValue, 
    UInt32 newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();
    mBlockName = block->getName();
    mItem = item;
    CcfgHelper ccfg(mCfg);
    mLocIndex = ccfg.findMemoryBlockLocationRTLIndex(block, loc);

    setText(QString("Change Memory %1 Block %2 RTL LSB from %3 to %4")
      .arg(mMemoryName)
      .arg(mBlockName)
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLocRTL* loc = ccfg.findMemoryBlockLocationRTLFromIndex(block, mLocIndex);

    loc->putRTLLsb(mOldValue);
    mOldValue = loc->getRTLLsb();

    mItem->setValue(mOldValue);
    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(false); 
  	mItem->check(mem, block, loc, true);
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLocRTL* loc = ccfg.findMemoryBlockLocationRTLFromIndex(block, mLocIndex);

    loc->putRTLLsb(mNewValue);
    mNewValue = loc->getRTLLsb();

    mItem->setValue(mNewValue);

    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(true);
  	mItem->check(mem, block, loc, true);
  }

private:
  ESLMemoryBlockLocDisplayAttrLSBRTL* mItem;
  CarbonCfg* mCfg;
  QString mMemoryName;
  QString mBlockName;
  int mLocIndex;
  UInt32 mOldValue;
  UInt32 mNewValue;
};

class CmdChangeESLMemoryBlockLocDisplayAttrMSBRTL : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryBlockLocDisplayAttrMSBRTL(ESLMemoryBlockLocDisplayAttrMSBRTL* item, 
    CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLocRTL* loc, UInt32 oldValue, 
    UInt32 newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();
    mBlockName = block->getName();
    mItem = item;
    CcfgHelper ccfg(mCfg);
    mLocIndex = ccfg.findMemoryBlockLocationRTLIndex(block, loc);

    setText(QString("Change Memory %1 Block %2 RTL MSB from %3 to %4")
      .arg(mMemoryName)
      .arg(mBlockName)
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLocRTL* loc = ccfg.findMemoryBlockLocationRTLFromIndex(block, mLocIndex);

    loc->putRTLMsb(mOldValue);
    mOldValue = loc->getRTLMsb();

    mItem->setValue(mOldValue);
    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(false); 
  	mItem->check(mem, block, loc, true);
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLocRTL* loc = ccfg.findMemoryBlockLocationRTLFromIndex(block, mLocIndex);

    loc->putRTLMsb(mNewValue);
    mNewValue = loc->getRTLMsb();


    mItem->setValue(mNewValue);

    mItem->treeWidget()->scrollToItem(mItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(mItem);

    setModified(true);
  	mItem->check(mem, block, loc, true);
  }

private:
  ESLMemoryBlockLocDisplayAttrMSBRTL* mItem;
  CarbonCfg* mCfg;
  QString mMemoryName;
  QString mBlockName;
  int mLocIndex;
  UInt32 mOldValue;
  UInt32 mNewValue;
};

// RTL Path name



class CmdChangeESLMemoryBlockLocationRTL : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryBlockLocationRTL(ESLMemoryBlockLocationRTL* item, 
    CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, CarbonCfgMemoryLocRTL* loc, const QString& oldValue, 
    const QString& newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();
    mBlockName = block->getName();
    CcfgHelper ccfg(mCfg);

    UtString netName; netName << mNewValue;
    const CarbonDBNode* node = carbonDBFindNode(mCfg->getDB(), netName.c_str());

    mNewLsb = carbonDBGetLSB(mCfg->getDB(), node);
    mNewMsb = carbonDBGetMSB(mCfg->getDB(), node);
    mNewStartWordOffset = carbonDBGet2DArrayRightAddr(mCfg->getDB(), node);
    mNewEndWordOffset = carbonDBGet2DArrayLeftAddr(mCfg->getDB(), node);

    mOldStartWordOffset = loc->getRTLStartWordOffset();
    mOldEndWordOffset = loc->getRTLEndWordOffset();
    mOldMsb = loc->getRTLMsb();
    mOldLsb = loc->getRTLLsb();

    mLocIndex = ccfg.findMemoryBlockLocationRTLIndex(block, loc);

    setText(QString("Change Memory %1 Block %2 RTL from %3 to %4")
      .arg(mem->getName())
      .arg(block->getName())
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLocRTL* loc = ccfg.findMemoryBlockLocationRTLFromIndex(block, mLocIndex);

    UtString v; v << mOldValue;

    loc->putPath(v.c_str());

    mOldValue = loc->getPath();

    ESLMemoryBlockLocationRTL* item = dynamic_cast<ESLMemoryBlockLocationRTL*>(getItem());
    item->treeWidget()->scrollToItem(item);

    item->setPath(mOldValue);
    item->rtlAttr()->setStartWordOffset(mOldStartWordOffset);
    item->rtlAttr()->setEndWordOffset(mOldEndWordOffset);
    item->rtlAttr()->setMsb(mOldMsb);
    item->rtlAttr()->setLsb(mOldLsb);

    mOldStartWordOffset = loc->getRTLStartWordOffset();
    mOldEndWordOffset = loc->getRTLEndWordOffset();
    mOldMsb = loc->getRTLMsb();
    mOldLsb = loc->getRTLLsb();

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLocRTL* loc = ccfg.findMemoryBlockLocationRTLFromIndex(block, mLocIndex);
    
    UtString v; v << mNewValue;
    loc->putPath(v.c_str());
    mNewValue = loc->getPath();


    ESLMemoryBlockLocationRTL* item = dynamic_cast<ESLMemoryBlockLocationRTL*>(getItem());
    item->setPath(mNewValue);

    item->rtlAttr()->setStartWordOffset(mNewStartWordOffset);
    item->rtlAttr()->setEndWordOffset(mNewEndWordOffset);
    item->rtlAttr()->setMsb(mNewMsb);
    item->rtlAttr()->setLsb(mNewLsb);

    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);
  }

private:  
  UInt64 mOldStartWordOffset;
  UInt64 mOldEndWordOffset;
  UInt32 mOldMsb;
  UInt32 mOldLsb;

  UInt32 mNewStartWordOffset;
  UInt32 mNewEndWordOffset;
  UInt32 mNewMsb;
  UInt32 mNewLsb;

  CarbonCfg* mCfg;
  QString mOldValue;
  QString mNewValue;
  int mLocIndex;
  QString mMemoryName;
  QString mBlockName;
};


class CmdAddESLMemoryBlock : public MemTreeUndoCommand
{
public:
  CmdAddESLMemoryBlock(BlocksRootItem* item, CarbonCfgMemory* mem,    
    QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    QApplication::setOverrideCursor(Qt::WaitCursor);
    
    mCfg = mem->getParent(); 
    mMemory = mem;
    mMemoryName = mem->getName();

    setText(QString("Add Memory %1 Block")
      .arg(mem->getName()));
    
    QApplication::restoreOverrideCursor();
  }

  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);

    ESLMemory* item = findESLMemory(getTree(), mCfg->getCompName(), mMemoryName);

    BlocksRootItem* blocksRoot = item->getBlocks();

    ESLMemoryBlock* memBlock = blocksRoot->findBlock(mMemoryBlockName);

    mem->removeMemoryBlock(memBlock->getBlock());

    blocksRoot->removeChild(memBlock);
 
    updateTreeIndex(item);

    item->check(true);

    setModified(false); 
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    
    UtString memBlockName;
    memBlockName << "block" << mem->numMemoryBlocks();

    UtString memName; memName << mMemoryName;
    const CarbonDBNode* node = carbonDBFindNode(mCfg->getDB(), memName.c_str());

    int mbi = 1;
    CarbonCfgMemoryBlock* mb = NULL;
    do
    {
      mb = ccfg.findMemoryBlock(mem, memBlockName.c_str());
      if (mb)
      {
        memBlockName.clear();
        memBlockName << "block" << mbi;
      }
      mbi++;
    } while (mb != NULL);

    mMemoryBlockName = memBlockName.c_str();
 
    CarbonCfgMemoryBlock* block = mem->addMemoryBlock();

    UInt32 width = carbonDBGetWidth(mCfg->getDB(), node);
    SInt32 left = carbonDBGet2DArrayLeftAddr(mCfg->getDB(), node);
    SInt32 right = carbonDBGet2DArrayRightAddr(mCfg->getDB(), node);
    UInt32 maxAddr = std::abs(right - left); // this is not 'num addrs'

    block->putName(memBlockName.c_str());
    block->putBase(0);
    block->putSize((maxAddr+1)*((width+7)/8));


    ESLMemory* item = findESLMemory(getTree(), mCfg->getCompName(), mMemoryName);

    BlocksRootItem* blocksRoot = item->getBlocks();

    ESLMemoryBlock* newItem = blocksRoot->addBlock(block);

    item->setSelected(true);
    item->treeWidget()->scrollToItem(newItem);

    //// must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(newItem);

    item->check(true);

    setModified(true);
  }
private:  
  CarbonCfg* mCfg;
  CarbonCfgMemory* mMemory;
  QString mMemoryName;
  QString mMemoryBlockName;
};

class CmdAddESLMemory : public MemTreeUndoCommand
{
public:
  CmdAddESLMemory(MemoriesRootItem* item, CarbonCfg* cfg, 
    const QString& memoryName, 
    QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    QApplication::setOverrideCursor(Qt::WaitCursor);

    mCfg = cfg;
    mMemoryName = memoryName;
    CcfgHelper ccfg(mCfg);

    UtString memName; memName << mMemoryName;
    const CarbonDBNode* node = carbonDBFindNode(mCfg->getDB(), memName.c_str());

    mNetLsb = carbonDBGetLSB(mCfg->getDB(), node);
    mNetMsb = carbonDBGetMSB(mCfg->getDB(), node);
    mNetStartWordOffset = carbonDBGet2DArrayRightAddr(mCfg->getDB(), node);
    mNetEndWordOffset = carbonDBGet2DArrayLeftAddr(mCfg->getDB(), node);

    setText(QString("Add Memory %1")
      .arg(memoryName));
    
    QApplication::restoreOverrideCursor();
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mNewName);

    ESLMemory* item = dynamic_cast<ESLMemory*>(getItem());

    MemoriesRootItem* memsRoot = getTree()->getEditor()->getMemoriesRoot();
    memsRoot->removeChild(item);

    mCfg->removeMemory(mem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 
  }
  void redo()
  {
    QApplication::setOverrideCursor(Qt::WaitCursor);

    CcfgHelper ccfg(mCfg);

    UtString memName; memName << mMemoryName;
    const CarbonDBNode* node = carbonDBFindNode(mCfg->getDB(), memName.c_str());
    
    UtString shortMemName; shortMemName << carbonDBNodeGetLeafName(mCfg->getDB(), node);
    
    int mi = 1;
    CarbonCfgMemory* xm = NULL;
    do
    {
      xm = ccfg.findMemory(shortMemName.c_str());
      if (xm)
      {
        shortMemName.clear();
        shortMemName << carbonDBNodeGetLeafName(mCfg->getDB(), node) << "_" << mi;
      }
      mi++;
    } while (xm != NULL);

    UInt32 width = carbonDBGetWidth(mCfg->getDB(), node);
    SInt32 left = carbonDBGet2DArrayLeftAddr(mCfg->getDB(), node);
    SInt32 right = carbonDBGet2DArrayRightAddr(mCfg->getDB(), node);
    UInt32 maxAddr = std::abs(right - left); // this is not 'num addrs'

    CarbonCfgMemory* mem = mCfg->addMemory(shortMemName.c_str(), width, maxAddr);

    mNewName = mem->getName();

    QString blockName = QString("block%1").arg(mem->numMemoryBlocks());
    UtString blkName; blkName << blockName;
    CarbonCfgMemoryBlock* block = mem->addMemoryBlock();
    block->putName(blkName.c_str());
    block->putBase(0);
    block->putSize((maxAddr+1)*((width+7)/8));
    block->addLocRTL(memName.c_str());

    MemoriesRootItem* memsRoot = getTree()->getEditor()->getMemoriesRoot();

    ESLMemory* item = new ESLMemory(memsRoot, mem);

    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);

    QApplication::restoreOverrideCursor();
  }

private:  
  UInt32 mNetStartWordOffset;
  UInt32 mNetEndWordOffset;
  UInt32 mNetMsb;
  UInt32 mNetLsb;
  QString mNewName;
  QString mOldName;
  int mLocIndex;

  CarbonCfg* mCfg;
  
  QString mMemoryName;

};


class CmdAddESLMemoryBlockLocationRTL : public MemTreeUndoCommand
{
public:
  CmdAddESLMemoryBlockLocationRTL(ESLMemoryBlockLocations* item, 
    CarbonCfgMemory* mem, CarbonCfgMemoryBlock* block, const QString& memNetName, 
    QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = item->getCcfg();
    mNetName = memNetName;
    mMemoryName = mem->getName();
    mBlockName = block->getName();
    CcfgHelper ccfg(mCfg);

    UtString netName; netName << mNetName;
    const CarbonDBNode* node = carbonDBFindNode(mCfg->getDB(), netName.c_str());
    mByteSize = (carbonDBGetBitSize(mCfg->getDB(), node) + 7) /8;
      
    mNetLsb = carbonDBGetLSB(mCfg->getDB(), node);
    mNetMsb = carbonDBGetMSB(mCfg->getDB(), node);
    mNetEndWordOffset = carbonDBGet2DArrayRightAddr(mCfg->getDB(), node);
    mNetStartWordOffset = carbonDBGet2DArrayLeftAddr(mCfg->getDB(), node);
    
    setText(QString("Add Memory %1 Block %2 Location RTL %3")
      .arg(mem->getName())
      .arg(block->getName())
      .arg(memNetName));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
    CarbonCfgMemoryLocRTL* loc = ccfg.findMemoryBlockLocationRTLFromIndex(block, mLocIndex);


    ESLMemoryBlockLocations* item = dynamic_cast<ESLMemoryBlockLocations*>(getItem());

    ESLMemoryBlockLocationRTL* rtlItem = item->findRTLLocation(loc);

    int childIndex = item->indexOfChild(rtlItem);
    item->takeChild(childIndex);

    block->removeLoc(loc);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);
    CarbonCfgMemoryBlock* block = ccfg.findMemoryBlock(mem, mBlockName);
 
    ESLMemoryBlockLocations* item = dynamic_cast<ESLMemoryBlockLocations*>(getItem());

    UtString n; n << mNetName;

    // only when the first locater is added do we initialize the // blocks size, after that the user needs to handle it
    int locs = block->numLocs();
    if (locs == 0) {
      ESLMemoryBlock* memBlock = dynamic_cast<ESLMemoryBlock*>(getItem()->parent());
      if ( memBlock ){
	memBlock->getSize()->setValue(mByteSize);
	memBlock->check(true);
      }
    }
    CarbonCfgMemoryLocRTL* loc = block->addLocRTL(n.c_str());
    mLocIndex = ccfg.findMemoryBlockLocationRTLIndex(block, loc);

    loc->putDisplayStartWordOffset(mNetStartWordOffset);
    loc->putDisplayEndWordOffset(mNetEndWordOffset);
    loc->putDisplayLsb(mNetLsb);
    loc->putDisplayMsb(mNetMsb);

    loc->putRTLStartWordOffset(mNetStartWordOffset);
    loc->putRTLEndWordOffset(mNetEndWordOffset);
    loc->putRTLLsb(mNetLsb);
    loc->putRTLMsb(mNetMsb);

    ESLMemoryBlockLocationRTL* rtlItem = item->addRTLLocation(mem, block, loc);
   
    rtlItem->setSelected(true);
    rtlItem->setExpanded(true);

    item->treeWidget()->scrollToItem(rtlItem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);
  }

private:
  UInt32 mByteSize;
  UInt64 mNetStartWordOffset;
  UInt64 mNetEndWordOffset;
  UInt32 mNetMsb;
  UInt32 mNetLsb;
  int mLocIndex;

  CarbonCfg* mCfg;
  
  QString mNetName;
  QString mMemoryName;
  QString mBlockName;

};


class CmdChangeESLMemoryMaxAddress : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryMaxAddress(ESLMemoryMaxAddress* item, 
    CarbonCfgMemory* mem, UInt32 oldValue, 
    UInt32 newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();
    CcfgHelper ccfg(mCfg);

    setText(QString("Change Memory %1 Max Address from %2 to %3")
      .arg(mMemoryName)
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);

    mem->putMaxAddrs(mOldValue);
    mOldValue = mem->getMaxAddrs();

    ESLMemoryMaxAddress* item = dynamic_cast<ESLMemoryMaxAddress*>(getItem());
    item->setValue(mOldValue);
    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 

	item->check(mem, true);
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);

    mem->putMaxAddrs(mNewValue);
    mNewValue = mem->getMaxAddrs();

    ESLMemoryMaxAddress* item = dynamic_cast<ESLMemoryMaxAddress*>(getItem());
    item->setValue(mNewValue);

    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);

	item->check(mem, true);
  }

private:
  CarbonCfg* mCfg;
  QString mMemoryName;
  int mLocIndex;
  UInt32 mOldValue;
  UInt32 mNewValue;
};

// Disassembly
class CmdChangeESLMemoryDisassembly : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryDisassembly(ESLMemoryDisassembly* item, 
    CarbonCfgMemory* mem, const QString& oldValue, 
    const QString& newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = mem->getParent();
    mOldValue = oldValue;
    mNewValue = newValue;
    mMemoryName = mem->getName();
    CcfgHelper ccfg(mCfg);

    setText(QString("Change Memory %1 Disassembly from %2 to %3")
      .arg(mMemoryName)
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);

    UtString ov;
    ov << mOldValue;
    mem->putMemoryDisassemblyName(ov.c_str());
    mOldValue = mem->getMemoryDisassemblyName();

    ESLMemoryDisassembly* item = dynamic_cast<ESLMemoryDisassembly*>(getItem());
    item->setName(mOldValue);
    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfgMemory* mem = ccfg.findMemory(mMemoryName);

    UtString nv;
    nv << mNewValue;
    mem->putMemoryDisassemblyName(nv.c_str());
    mNewValue = mem->getMemoryDisassemblyName();

    ESLMemoryDisassembly* item = dynamic_cast<ESLMemoryDisassembly*>(getItem());
    item->setName(mNewValue);

    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);
  }

private:
  CarbonCfg* mCfg;
  QString mMemoryName;
  int mLocIndex;
  QString mOldValue;
  QString mNewValue;
};

// Disassembler
class CmdChangeESLMemoryDisassembler : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemoryDisassembler(ESLMemoryDisassembler* item, 
    CarbonCfg* cfg, const QString& oldValue, 
    const QString& newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = cfg;
    mOldValue = oldValue;
    mNewValue = newValue;
 
    CcfgHelper ccfg(mCfg);

    setText(QString("Change Disassembler from %1 to %2")
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {
    ESLMemoryDisassembler* item = dynamic_cast<ESLMemoryDisassembler*>(getItem());
    item->setName(mOldValue);

    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 
  }
  void redo()
  {
    ESLMemoryDisassembler* item = dynamic_cast<ESLMemoryDisassembler*>(getItem());
    item->setName(mNewValue);

    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);
  }

private:
  CarbonCfg* mCfg;
  QString mMemoryName;
  int mLocIndex;
  QString mOldValue;
  QString mNewValue;
};

class CmdMoveMemoryToSubComponent : public MemTreeUndoCommand
{
public:
  CmdMoveMemoryToSubComponent(ESLMemory* memItem, ESLMemorySubComponent* item, 
    CarbonCfgMemory* mem, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mComponentName = item->getName();
    mMemoryName = mem->getName();
    mCfg = item->getCcfg();
    mMemory = mem;
    mMemoryItem = memItem;

    setText(QString("Move Memory %1 to SubComponent %2")
      .arg(mMemoryName)
      .arg(mComponentName)
    );

  }
  void undo()
  {  
    ESLMemorySubComponent* item = dynamic_cast<ESLMemorySubComponent*>(getItem());

    // remove the memory from the subcomponent
    item->getSubComponent()->removeMemoryUndo(mMemory);

    // remove the tree node from the sub-component
    item->removeSubComponentMemory(mMemory);
   
    // put the memory back as a toplevel memory
    mCfg->insertMemoryUndo(mRemovePos, mMemory);
    
    // insert the memory node back
    MemoriesRootItem* memsRoot = getTree()->getEditor()->getMemoriesRoot();
    ESLMemory* pMem = new ESLMemory(getTree(), mMemory);

    memsRoot->insertChild(mMemoryTreePos, pMem);

    getTree()->expandChildren(pMem);

    item->treeWidget()->scrollToItem(pMem);

    mMemoryItem = pMem;

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 
  }
  void redo()
  {
    ESLMemorySubComponent* item = dynamic_cast<ESLMemorySubComponent*>(getItem());
    MemEditorTreeItem* mi = mMemoryItem;
    
    mRemovePos = mCfg->removeMemoryUndo(mMemory);

    MemoriesRootItem* memsRoot = getTree()->getEditor()->getMemoriesRoot();
    mMemoryTreePos = memsRoot->indexOfChild(mMemoryItem);

    // remove the item from the main memories tree
    mi->parent()->removeChild(mMemoryItem);

    // insert the memory as a subcomponent
    int pos = item->getSubComponent()->numMemories();
    item->getSubComponent()->insertMemoryUndo(pos, mMemory);

    // make the subcomponent tree node
    ESLMemory* newMem = item->addMemory(mMemory);
    item->treeWidget()->scrollToItem(newMem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);
  }

private:
  int mRemovePos;
  int mMemoryTreePos;
  CarbonCfg* mCfg;
  QString mComponentName;
  QString mMemoryName;
  CarbonCfgMemory* mMemory;
  ESLMemory* mMemoryItem;
};

class CmdChangeESLMemorySubComponentName : public MemTreeUndoCommand
{
public:
  CmdChangeESLMemorySubComponentName(ESLMemorySubComponent* item, 
    CarbonCfg* subComponent, const QString& oldValue, 
    const QString& newValue, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mComponentName = subComponent->getCompName();
    mCfg = item->getCcfg();
    mOldValue = oldValue;
    mNewValue = newValue;
   
    setText(QString("Change Component Name from %1 to %2")   
      .arg(oldValue)
      .arg(newValue));
  }
  
  void undo()
  {  
    ESLMemorySubComponent* item = dynamic_cast<ESLMemorySubComponent*>(getItem());
    item->setName(mOldValue);
    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 
  }
  void redo()
  {
    CcfgHelper ccfg(mCfg);

    ESLMemorySubComponent* item = dynamic_cast<ESLMemorySubComponent*>(getItem());
    item->setName(mNewValue);

    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);
  }

private:
  CarbonCfg* mCfg;
  QString mComponentName;
  QString mOldValue;
  QString mNewValue;
};

class CmdChangeMemoryProgOffset : public MemTreeUndoCommand
{
public:
  CmdChangeMemoryProgOffset(ESLProgPreloadOffset* item, 
    CarbonCfgMemory* mem, 
    UInt64 oldValue, 
    UInt64 newValue, QUndoCommand* parent = 0);
  
  void undo();
  void redo();

private:
  CarbonCfg* mCfg;
  UInt64 mOldValue;
  UInt64 mNewValue;
};

class CmdChangeReadmemKind : public MemTreeUndoCommand
{
public:
  CmdChangeReadmemKind(ESLReadmemKind* item, 
    CarbonCfgMemory* mem, 
    CarbonCfgReadmemType oldType, 
    CarbonCfgReadmemType newType, QUndoCommand* parent = 0);
  
  void undo();
  void redo();

private:
  CarbonCfg* mCfg;
  CarbonCfgReadmemType mOldType;
  CarbonCfgReadmemType mNewType;
};

class CmdChangeReadmemFile : public MemTreeUndoCommand
{
public:
  CmdChangeReadmemFile(ESLReadmemFile* item, 
    CarbonCfgMemory* mem, 
    const QString& oldType, 
    const QString& newType, QUndoCommand* parent = 0);
  
  void undo();
  void redo();

private:
  CarbonCfg* mCfg;
  QString mOldValue;
  QString mNewValue;
};

class CmdAddSubComponent : public MemTreeUndoCommand
{
public:
  CmdAddSubComponent(MemoriesRootItem* item, CarbonCfg* cfg, 
    const QString& subComponentName, 
    QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    QApplication::setOverrideCursor(Qt::WaitCursor);

    mCfg = cfg;
    mSubComponentName = subComponentName;
    mRootItem = item;

    setText(QString("Add Subcomponent %1").arg(subComponentName));
    
    QApplication::restoreOverrideCursor();
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);
    CarbonCfg* subComp = ccfg.findSubComponent(mSubComponentName);

    ESLMemorySubComponent* item = dynamic_cast<ESLMemorySubComponent*>(getItem());

    mRootItem->removeChild(item);

    mCfg->removeSubCompUndo(subComp);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 
  }
  void redo()
  {
    QApplication::setOverrideCursor(Qt::WaitCursor);

    CcfgHelper ccfg(mCfg);

    UtString name; name << mSubComponentName;

    CarbonCfg* subComp = mCfg->addSubComp(name.c_str());
    
    ESLMemorySubComponent* item = new ESLMemorySubComponent(mRootItem, subComp);
         
    QList<MemEditorTreeItem::NodeType> nonExpandNodes;
    nonExpandNodes.append(MemEditorTreeItem::MemBlockCustomCodes);
    nonExpandNodes.append(MemEditorTreeItem::MemCustomCodes);
    nonExpandNodes.append(MemEditorTreeItem::CompCustomCodes);

    getTree()->expandChildren(item, nonExpandNodes);   

    item->setSelected(true);

    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);

    QApplication::restoreOverrideCursor();
  }

private:  
  int mLocIndex;

  CarbonCfg* mCfg;
  
  QString mSubComponentName;
  MemoriesRootItem* mRootItem;
};

class CmdAddSubComponentESLMemory : public MemTreeUndoCommand
{
public:
  CmdAddSubComponentESLMemory(ESLMemorySubComponent* item, CarbonCfg* cfg, 
    const QString& memoryName, 
    QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    QApplication::setOverrideCursor(Qt::WaitCursor);

    mCfg = cfg;
    mSubComponentName = item->getName();
    mSubComponent = item;
    mMemoryName = memoryName;
    CcfgHelper ccfg(mCfg);

    UtString memName; memName << mMemoryName;
    const CarbonDBNode* node = carbonDBFindNode(mCfg->getDB(), memName.c_str());

    mNetLsb = carbonDBGetLSB(mCfg->getDB(), node);
    mNetMsb = carbonDBGetMSB(mCfg->getDB(), node);
    mNetStartWordOffset = carbonDBGet2DArrayRightAddr(mCfg->getDB(), node);
    mNetEndWordOffset = carbonDBGet2DArrayLeftAddr(mCfg->getDB(), node);

    setText(QString("Add Subcomponent Memory %1")
      .arg(memoryName));
    
    QApplication::restoreOverrideCursor();
  }
  
  void undo()
  {
    CcfgHelper ccfg(mCfg);

    CarbonCfg* subComp = ccfg.findSubComponent(mSubComponentName);

    CcfgHelper subCcfg(subComp);

    CarbonCfgMemory* mem = subCcfg.findMemory(mNewName);

    ESLMemory* item = dynamic_cast<ESLMemory*>(getItem());

    mSubComponent->removeChild(item);

    subComp->removeMemory(mem);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 
  }
  void redo()
  {
    QApplication::setOverrideCursor(Qt::WaitCursor);

    CcfgHelper ccfg(mCfg);
    CarbonCfg* subComp = ccfg.findSubComponent(mSubComponentName);

    UtString memName; memName << mMemoryName;
    const CarbonDBNode* node = carbonDBFindNode(mCfg->getDB(), memName.c_str());
    
    UtString shortMemName; shortMemName << carbonDBNodeGetLeafName(mCfg->getDB(), node);
    
    int mi = 1;
    CarbonCfgMemory* xm = NULL;
    do
    {
      xm = ccfg.findMemory(shortMemName.c_str());
      if (xm)
      {
        shortMemName.clear();
        shortMemName << carbonDBNodeGetLeafName(mCfg->getDB(), node) << "_" << mi;
      }
      mi++;
    } while (xm != NULL);

    UInt32 width = carbonDBGetWidth(mCfg->getDB(), node);
    SInt32 left = carbonDBGet2DArrayLeftAddr(mCfg->getDB(), node);
    SInt32 right = carbonDBGet2DArrayRightAddr(mCfg->getDB(), node);
    UInt32 maxAddr = std::abs(right - left); // this is not 'num addrs'

    CarbonCfgMemory* mem = subComp->addMemory(shortMemName.c_str(), width, maxAddr);

    mNewName = mem->getName();

    QString blockName = QString("block%1").arg(mem->numMemoryBlocks());
    UtString blkName; blkName << blockName;
    CarbonCfgMemoryBlock* block = mem->addMemoryBlock();
    block->putName(blkName.c_str());
    block->putBase(0);
    block->putSize((maxAddr+1)*((width+7)/8));
    block->addLocRTL(memName.c_str());


    ESLMemory* item = new ESLMemory(mSubComponent, mem);

    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);

    QApplication::restoreOverrideCursor();
  }

private:  
  UInt32 mNetStartWordOffset;
  UInt32 mNetEndWordOffset;
  UInt32 mNetMsb;
  UInt32 mNetLsb;
  QString mNewName;
  QString mOldName;
  int mLocIndex;

  CarbonCfg* mCfg;
  
  QString mMemoryName;
  QString mSubComponentName;
  ESLMemorySubComponent* mSubComponent;
};

class CmdMoveSubComponentMemoryToSubComponentMemory : public MemTreeUndoCommand
{
public:
  CmdMoveSubComponentMemoryToSubComponentMemory(
    ESLMemorySubComponent* srcSubComponent,  // The SubComponent owning this memory
    ESLMemory* srcMemory,                    // The Memory within the SubComponent
    ESLMemorySubComponent* item,             // The Destination sub-component
    CarbonCfgMemory* mem, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mSrcSubComponent = srcSubComponent;
    mDstSubComponent = item;

    mMemoryName = mem->getName();
    mCfg = srcMemory->getCcfg();
    mMemory = mem;
    mMemoryItem = srcMemory;

    setText(QString("Move SubComponent Memory %1 %2 to SubComponent Memory %3")
      .arg(mMemoryName)
      .arg(mSrcSubComponent->getName())
      .arg(mDstSubComponent->getName())
    );
  }
  void undo()
  {  
    ESLMemorySubComponent* item = dynamic_cast<ESLMemorySubComponent*>(getItem());

    // remove the tree node from the sub-component
    item->removeSubComponentMemory(mMemory);
   
    // Put it back to the source component
    mSrcSubComponent->getSubComponent()->insertMemoryUndo(mRemovePos, mMemory);
   
    ESLMemory* pMem = new ESLMemory(getTree(), mMemory);

    // Put it back 
    mDstSubComponent->insertChild(mTreePos, pMem);

    getTree()->expandChildren(pMem);

    item->treeWidget()->scrollToItem(pMem);
  
    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(false); 
  }
  void redo()
  {
    // Destination Sub-Component
    ESLMemorySubComponent* item = dynamic_cast<ESLMemorySubComponent*>(getItem());

    // Remove the memory from the subcomponent
    mRemovePos = mSrcSubComponent->getSubComponent()->removeMemoryUndo(mMemory);

    //// Remove the tree node from the sub-component
    mTreePos = mSrcSubComponent->removeSubComponentMemory(mMemory);

    // Add the memory to the Destination
    int pos = item->getSubComponent()->numMemories();
    item->getSubComponent()->insertMemoryUndo(pos, mMemory);
    
    // Create a node for this memory
    item->addMemory(mMemory);

    getTree()->expandChildren(item);

    item->treeWidget()->scrollToItem(item);

    // must call updateTreeIndex whenever col 0 text changes
    updateTreeIndex(item);

    setModified(true);
  }

private:
  int mRemovePos;
  int mTreePos;
  CarbonCfg* mCfg;
  QString mComponentName;
  QString mMemoryName;
  CarbonCfgMemory* mMemory;
  ESLMemory* mMemoryItem;
  ESLMemorySubComponent* mSrcSubComponent;
  ESLMemorySubComponent* mDstSubComponent;
  CarbonCfg* mSubComponent;
};




class CmdChangeSystemAddressMappings : public MemTreeUndoCommand
{
public:
  CmdChangeSystemAddressMappings(ESLMemorySystemAddressMapping* item, CarbonCfg* cfg, CarbonCfgMemory* mem,  QList<_AddressMapping>& mappings, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = cfg;
    mComponentName = mem->getParent()->getCompName();
    mMemoryName = mem->getName();
    mNewMappings = mappings;
    mOldInitType = mem->getMemInitType();

    for(quint32 i=0; i<mem->numSystemAddressESLPorts(); i++)
    {
      QString portName = mem->getSystemAddressESLPortName(i);
      qint64 baseAddress = mem->getSystemAddressESLPortBaseAddress(i);
      _AddressMapping am;
      am.mBaseAddress = baseAddress;
      am.mPortMapped = true;
      am.mModified = false;
      am.mPortName = portName;
      mOldMappings.append(am);
    }
  }

  void undo()
  {
    ESLMemorySystemAddressMapping* item = dynamic_cast<ESLMemorySystemAddressMapping*>(getItem());

    CcfgHelper ccfg(mCfg);    
    CarbonCfg* comp = ccfg.findComponent(mComponentName);

    CcfgHelper memComp(comp);
    CarbonCfgMemory* mem = memComp.findMemory(mMemoryName);

    // Remove all ports (we will re-construct the list)
    mem->removeAllSystemAddressESLPorts();

    foreach(_AddressMapping am, mOldMappings)
    {
      UtString n; n << am.mPortName;
      if (am.mPortMapped)
        mem->addSystemAddressESLPort(n.c_str(), am.mBaseAddress);
    }

    mem->putMemInitType(mOldInitType);

    item->populate();

    setModified(false);
  }
  void redo()
  {
    ESLMemorySystemAddressMapping* item = dynamic_cast<ESLMemorySystemAddressMapping*>(getItem());

    CcfgHelper ccfg(mCfg);    
    CarbonCfg* comp = ccfg.findComponent(mComponentName);

    CcfgHelper memComp(comp);
    CarbonCfgMemory* mem = memComp.findMemory(mMemoryName);

    // Remove all ports (we will re-construct the list)
    mem->removeAllSystemAddressESLPorts();

    foreach(_AddressMapping am, mNewMappings)
    {
      UtString n; n << am.mPortName;
      if (am.mPortMapped)
        mem->addSystemAddressESLPort(n.c_str(), am.mBaseAddress);

      mem->putMemInitType(eCarbonCfgMemInitProgPreload);
    }

    item->populate();

    setModified(true);
  }

private:
  CarbonCfg* mCfg;
  QString mMemoryName;
  QString mComponentName;
  QList<_AddressMapping> mOldMappings;
  QList<_AddressMapping> mNewMappings;
  CarbonCfgMemInitType mOldInitType;
};


// 
class CmdChangeSystemAddressBaseAddress : public MemTreeUndoCommand
{
public:
  CmdChangeSystemAddressBaseAddress(ESLSystemAddressESLPortMapping* item, CarbonCfg* cfg, CarbonCfgMemory* mem, 
    qint64 oldBaseAddress, qint64 newBaseAddress, QUndoCommand* parent = 0) : MemTreeUndoCommand(item, parent)
  {
    mCfg = cfg;
    mComponentName = mem->getParent()->getCompName();
    mMemoryName = mem->getName();
    mOldValue = oldBaseAddress;
    mNewValue = newBaseAddress;    
  }

  void undo()
  {
    ESLSystemAddressESLPortMapping* item = dynamic_cast<ESLSystemAddressESLPortMapping*>(getItem());
    item->setValue(mOldValue);
    setModified(false);
  }
  void redo()
  {
    ESLSystemAddressESLPortMapping* item = dynamic_cast<ESLSystemAddressESLPortMapping*>(getItem());
    item->setValue(mNewValue);
    setModified(true);
  }

private:
  CarbonCfg* mCfg;
  QString mMemoryName;
  QString mComponentName;
  QString mXtorPortName;
  qint64 mOldValue;
  qint64 mNewValue;
};


#endif
