//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "DlgDebugAccess.h"

#include "carbon/carbon_dbapi.h"
#include "cfg/CarbonCfg.h"
#include "CcfgHelper.h"
#include <QtGui>

DlgDebugAccess::DlgDebugAccess(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);
}

void DlgDebugAccess::setProtocolInstances(const QStringList& items)
{
  foreach(QString s, items)
  {
    ui.comboBoxProtocolInstances->addItem(s);
  }
}

QString DlgDebugAccess::protocolInstance()
{
  return ui.comboBoxProtocolInstances->currentText();
}

DlgDebugAccess::~DlgDebugAccess()
{
}

void DlgDebugAccess::on_buttonBox_accepted()
{
  accept();
}


void DlgDebugAccess::on_buttonBox_rejected()
{
  reject();
}

