#ifndef ERRORINFOWIDGET_H
#define ERRORINFOWIDGET_H

#include "util/CarbonPlatform.h"

#include <QWidget>
#include <QProgressBar>
#include "ui_ErrorInfoWidget.h"

class RemoteConsole;
class CarbonConsole;
class CarbonMakerContext;
class InfoTableWidget;

class ErrorInfoWidget : public QWidget
{
  Q_OBJECT

public:
  ErrorInfoWidget(QWidget *parent = 0, CarbonMakerContext* ctx=0, CarbonConsole* console=0, QProgressBar* pb=0, QStatusBar* sb=0);
  ~ErrorInfoWidget();

  InfoTableWidget* getInfoWidget() { return ui.widgetInfo; }

private:
  Ui::ErrorInfoWidgetClass ui;

  CarbonConsole* mConsole;

private slots:
  void on_pushButtonFiltered_toggled(bool);
  void on_pushButtonMessages_toggled(bool);
  void on_pushButtonWarnings_toggled(bool);
  void on_pushButtonErrors_toggled(bool);
  void showExtendedHelp(const char*);

};

#endif // ERRORINFOWIDGET_H
