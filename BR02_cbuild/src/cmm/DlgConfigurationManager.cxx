//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"
#include <QtGui>
#include "DlgConfigurationManager.h"
#include "CarbonProjectWidget.h"
#include "DlgCopyConfiguration.h"
#include "DlgConfiguration.h"

#define NEW_ITEM "<New>"

DlgConfigurationManager::DlgConfigurationManager(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);
  mProject = NULL;

}

DlgConfigurationManager::~DlgConfigurationManager()
{
}

void DlgConfigurationManager::setProject(CarbonProjectWidget* proj)
{
  mProject = proj;

  CarbonConfigurations* configs = mProject->project()->getConfigurations();

  for (UInt32 i=0; i<configs->numConfigurations(); i++)
  {
    CarbonConfiguration* config = configs->getConfiguration(i);
    QListWidgetItem* item = new QListWidgetItem(config->getName(), ui.listWidget);
    item->setData(Qt::UserRole, config->getName());
    item->setFlags(item->flags() | Qt::ItemIsEditable);
  }

  for (int li=0; li<ui.listWidget->count(); li++)
  {
    QListWidgetItem* item = ui.listWidget->item(li);
    QString text = item->text();
    UtString value;
    value << text;
    if (0 == strcmp(value.c_str(), mProject->project()->getActiveConfiguration()))
    {
      ui.listWidget->setCurrentItem(item);
      break;
    }
  }


  CQT_CONNECT(ui.listWidget, itemChanged(QListWidgetItem*), this, itemChanged(QListWidgetItem*));

  enableButtons();
}

void DlgConfigurationManager::itemChanged(QListWidgetItem* item)
{
  QRegExp regexp("([A-Za-z_0-9]+)"); 
  UtString new_name;
  new_name << item->text();
  bool problemFound = false;

  QString old_name = item->data(Qt::UserRole).toString();

  // New entry?
  if (!old_name.isEmpty() && old_name != NEW_ITEM )
  {
    bool legitimateValue = false;
    if (regexp.exactMatch(item->text()) && regexp.cap(0) == item->text())
    legitimateValue = true;

    if (!legitimateValue)
    {
      UtString msg;
      msg << "A Configuration name may only contain alphanumeric characters and may not contain embedded spaces.";
      QMessageBox::warning(mProject, MODELSTUDIO_TITLE, msg.c_str());
      bool prevValue = ui.listWidget->blockSignals(true);
      item->setText(old_name);
      ui.listWidget->blockSignals(prevValue);
      problemFound = true;
    }

    CarbonConfigurations* configs = mProject->project()->getConfigurations();
    if (configs->findConfiguration(new_name.c_str()) != NULL)
    {
      UtString msg;
      msg << "A Configuration named " << new_name << " already exists, ignoring";
      QMessageBox::warning(mProject, MODELSTUDIO_TITLE, msg.c_str());
      bool prevValue = ui.listWidget->blockSignals(true);
      item->setText(old_name);
      ui.listWidget->blockSignals(prevValue);
      problemFound = true;
    }

    QString itemValue = item->text();
    if (!problemFound && (itemValue != NEW_ITEM))
    {
      on_pushButtonClose_clicked();
    }
  }
}

void DlgConfigurationManager::on_pushButtonRename_clicked()
{
  ui.listWidget->editItem(ui.listWidget->currentItem());
}

void DlgConfigurationManager::on_pushButtonCopy_clicked()
{
  if (mProject->actionCopyConfiguration())
    on_pushButtonClose_clicked();
}

void DlgConfigurationManager::on_pushButtonDelete_clicked()
{
  QListWidgetItem* item = ui.listWidget->currentItem();
  if (item)
  {
    QString deletedItem = item->text();
	QMessageBox::StandardButton ret;
    ret = QMessageBox::warning(NULL, MODELSTUDIO_TITLE,
        "Are you sure?",
        QMessageBox::Yes | QMessageBox::No);
    if (ret == QMessageBox::Yes)
    {
		mDeletedItems.push_back(deletedItem);
		ui.listWidget->takeItem(ui.listWidget->currentRow());
		on_pushButtonClose_clicked();
    }
    else if (ret == QMessageBox::Cancel)
    {
		on_pushButtonClose_clicked();
    }
  }
}

void DlgConfigurationManager::on_pushButtonNew_clicked()
{
  DlgCopyConfiguration dlg(this);
  dlg.setProject(mProject);
  const char* newName = NEW_ITEM;

  if (dlg.exec() == QDialog::Accepted && strlen(dlg.getConfigurationName()) > 0)
  {
    newName = dlg.getConfigurationName();
    if (strlen(dlg.getCopyFromName()) > 0)
      mProject->project()->copyConfiguration(dlg.getCopyFromName(), dlg.getConfigurationName());
    else
      mProject->project()->addConfiguration(dlg.getConfigurationName());

    QListWidgetItem* item = new QListWidgetItem(newName, ui.listWidget);
    item->setFlags(item->flags() | Qt::ItemIsEditable);
    item->setData(Qt::UserRole, NEW_ITEM);
    ui.listWidget->setCurrentItem(item);
    ui.listWidget->editItem(item);
    on_pushButtonClose_clicked();
  }
}

void DlgConfigurationManager::enableButtons()
{
  CarbonConfigurations* configs = mProject->project()->getConfigurations();
  if (configs->numConfigurations() > 1)
  {
    ui.pushButtonDelete->setEnabled(true);
  }
  else
  {
    ui.pushButtonDelete->setEnabled(false);
  }
}

void DlgConfigurationManager::apply()
{
  UtString activeConfigName;
  activeConfigName << mProject->project()->getActiveConfiguration();

  CarbonConfigurations* configs = mProject->project()->getConfigurations();

  for (int i=0; i<ui.listWidget->count(); i++)
  {
    QListWidgetItem* item = ui.listWidget->item(i);
    QVariant qv = item->data(Qt::UserRole);

    UtString oldName;
    oldName <<  qv.toString();

    UtString newName;
    newName << item->text();

    if (oldName == NEW_ITEM)
    {
      if (newName != NEW_ITEM && configs->findConfiguration(newName.c_str()) == NULL)
        mProject->project()->addConfiguration(newName.c_str());
    }
    else if (oldName != newName)
    {
      qDebug() << "old name: " << oldName.c_str() << " new value: " << newName.c_str();
      CarbonConfiguration* config = configs->findConfiguration(oldName.c_str());
      if (config)
      {
        config->putName(newName.c_str());
        mProject->project()->renameConfiguration(oldName.c_str(), newName.c_str());
      }

      if (0 == strcmp(activeConfigName.c_str(), oldName.c_str()))
        mProject->putConfiguration(newName.c_str());
    }
  }

  // process the deleted items
  foreach (QString name, mDeletedItems)
  {
    UtString s;
    s << name;
    mProject->project()->removeConfiguration(s.c_str());
  }

  enableButtons();
}

void DlgConfigurationManager::on_pushButtonClose_clicked()
{
  apply();

  QListWidgetItem* item = ui.listWidget->currentItem();
  if (item)
  {
    QString config = item->text();
    if (config != NEW_ITEM)
    {
      UtString name;
      name << config;
      mProject->project()->putActiveConfiguration(name.c_str());
    }
  }

  accept();
}


