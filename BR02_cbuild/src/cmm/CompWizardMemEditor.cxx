//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/


#include "util/CarbonPlatform.h"

#include "CompWizardMemEditor.h"
#include "MemEditorTreeNodes.h"
#include "MemEditorCommands.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonFiles.h"
#include "MdiMaxsim.h"
#include "MaxsimComponent.h"
#include "SystemCComponent.h"
#include "cmm.h"
#include "CowareComponent.h"
#include "MemEditorTreeWidget.h"


void CompWizardMemEditor::renameXtorInstance(const QString& oldName, const QString& newName)
{
  qDebug() << "Rename" << oldName << "to" << newName;
  QList<MemEditorTreeItem*> items;
  ui.treeWidget->findNodes(items, MemEditorTreeItem::SystemAddressESLPortMapping);
  foreach(MemEditorTreeItem* item, items)
  {
    ESLSystemAddressESLPortMapping* mapping = dynamic_cast<ESLSystemAddressESLPortMapping*>(item);
    if (mapping && mapping->getPortName() == oldName)
    {
      mapping->changePortName(oldName, newName);
    }
    qDebug() << item->text(0);
  }
}

void CompWizardMemEditor::deleteXtorInstance(const QString& name)
{
  qDebug() << "Delete" << name;
}

bool CompWizardMemEditor::check(CarbonConsole* console)
{
  if (mMemoriesRoot)
    return mMemoriesRoot->check(console);
  else
    return false;
}

CompWizardMemEditor::CompWizardMemEditor(QWidget *parent)
: QWidget(parent)
{
  ui.setupUi(this);

  mEditorMode = eModeCoWare;

  mMemoriesRoot = NULL;

  // Init to NULL
  mActionRedo= NULL;
  mActionUndo= NULL;
  mActionDelete= NULL;
  mActionAddMemory= NULL;
  mActionAddSubComponent = NULL;

  mToolbar = NULL;
  mDB = 0;
  mCfg = 0;

  mUndoEnabled = false;
  mRedoEnabled = false;

  mUndoStack = new QUndoStack();
 
  createActions();
  createMenus();
  connectWidgets();

  ui.treeWidget->setItemDelegate(new MemEditorDelegate(this, ui.treeWidget, this));
  ui.treeWidget->setEditTriggers(QAbstractItemView::AllEditTriggers);
  ui.treeWidget->setUndoStack(mUndoStack);
  ui.treeWidget->setEditor(this);


  ui.treeWidget->setColumnWidth(2, 400);

  setWindowTitle("Memories[*]");
}

void CompWizardMemEditor::connectWidgets()
{
  CQT_CONNECT(mUndoStack, canRedoChanged(bool), this, canRedoChanged(bool));
  CQT_CONNECT(mUndoStack, canUndoChanged(bool), this, canUndoChanged(bool));

  CQT_CONNECT(ui.treeWidget, customContextMenuRequested(const QPoint&), 
    this, customContextMenuRequested(const QPoint&));
  CQT_CONNECT(ui.treeWidget, widgetModified(bool), this, treeWidgetModified(bool));
  CQT_CONNECT(ui.treeWidget, itemSelectionChanged(), this, treeWidgetSelectionChanged());

  CQT_CONNECT(getProjectWidget()->getConsole(), 
    modelCompilationFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType), this,
    modelCompilationFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType));
}

void CompWizardMemEditor::customContextMenuRequested(const QPoint& pos)
{
  const QPoint& point = ui.treeWidget->viewport()->mapToGlobal(pos);
  QTreeWidgetItem* pointItem = ui.treeWidget->itemAt(pos);

  QMenu menu;

  if (pointItem)
  {
    MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(pointItem);
    if (item)
      item->multiSelectContextMenu(menu);
  }

  if (menu.actions().count() > 0)
    menu.exec(point);
}

void CompWizardMemEditor::deleteCurrent()
{
  qDebug() << "Delete";

  // Commit and close any open editors
  ui.treeWidget->closeEditor();

  QList<MemEditorTreeItem*> deleteItems;
  foreach (QTreeWidgetItem* item, ui.treeWidget->selectedItems())
  {
    deleteItems.append(dynamic_cast<MemEditorTreeItem*>(item));
  }

  MemCmdDeleteItems* delItems = new MemCmdDeleteItems(deleteItems);
  if (delItems->numItems() > 0)
    mUndoStack->push(delItems);
  else
  {
    delete delItems;
    theApp->statusBar()->showMessage("Nothing to delete", 2000);
  }
}

void CompWizardMemEditor::actionDelete()
{
  deleteCurrent();
}

void CompWizardMemEditor::actionAddSubComponent()
{
  QString subCompName;
  
  for (int i=0; i<1000; i++)
  {
    QString name = QString("SubComponent%1").arg(i);
    UtString n; n << name;
    subCompName = name;
    if (mCfg->findSubComponent(n.c_str()) == NULL)
      break;
  }

  mUndoStack->push(new CmdAddSubComponent(mMemoriesRoot, mCfg, subCompName));
}

// Add all Memories
void CompWizardMemEditor::actionAddMemory()
{
  mMemoryNameMap.clear();

  for (UInt32 i=0; i<mCfg->numMemories(); i++)
  {
    CarbonCfgMemory* mem = mCfg->getMemory(i);    
    for(UInt32 j=0; j<mem->numMemoryBlocks(); j++)
    {
      CarbonCfgMemoryBlock* block = mem->getMemoryBlock(j);
      for(UInt32 k=0; k<block->numLocs(); k++)
      {
        CarbonCfgMemoryLoc* loc = block->getLoc(k);
        if (loc->castRTL())
        {
          mMemoryNameMap[loc->castRTL()->getPath()] = mem;
          qDebug() << "existing mem" << mem->getPath();
        }
      }
    }
  }

  // check sub-components
  for (UInt32 i=0; i<mCfg->numSubComponents(); i++)
  {
    CarbonCfg* subComp = mCfg->getSubComponent(i);
    for (UInt32 i=0; i<subComp->numMemories(); i++)
    {
      CarbonCfgMemory* mem = subComp->getMemory(i);  
      for(UInt32 j=0; j<mem->numMemoryBlocks(); j++)
      {
        CarbonCfgMemoryBlock* block = mem->getMemoryBlock(j);
        for(UInt32 k=0; k<block->numLocs(); k++)
        {
          CarbonCfgMemoryLoc* loc = block->getLoc(k);
          if (loc->castRTL())
          {
            mMemoryNameMap[loc->castRTL()->getPath()] = mem;
            qDebug() << "existing mem" << mem->getPath();
          }
        }
      }
    }
  }

  addAllMemoriesHelper(false);

  ui.treeWidget->header()->resizeSections(QHeaderView::ResizeToContents);
}

UInt32 CompWizardMemEditor::addAllMemoriesHelper(bool dry_run) 
{

  // Walk through the design and add memory entries for every 2-D
  // array that is found, unless it's already in.
  UInt32 total = 0;
  if (mDB != NULL) 
  {
    CarbonDBNodeIter* iter = carbonDBLoopDesignRoots(mDB);
    const CarbonDBNode* node = NULL;
    while ((node = carbonDBNodeIterNext(iter)) != NULL) {
      total += findMemories(node, dry_run);
    }
    carbonDBFreeNodeIter(iter);
  }

  QString msg = QString("Added %1 memories").arg(total);

  if (total > 0)
   ui.treeWidget->setModified(true);

  theApp->statusBar()->showMessage(msg);
 
  return total;
}


UInt32 CompWizardMemEditor::findMemories(const CarbonDBNode* node, bool dry_run)
{
  UInt32 total = 0;
  if (carbonDBIs2DArray(mDB, node))
  {
    const char* leafName = carbonDBNodeGetLeafName(mDB, node);
    // exclude resynthesized memories
    if (*leafName != '$')
    {
      if (dry_run) 
      {
        QString path = carbonDBNodeGetFullName(mDB, node);
        if (!mMemoryNameMap.contains(path))
          ++total;
      }
      else
      {
        QTreeWidgetItem* twi = addMemoryNode(node);
        if (twi)
          ++total;
      }
    }
  }
  else 
  {
    CarbonDBNodeIter* iter = carbonDBLoopChildren(mDB, node);
    while ((node = carbonDBNodeIterNext(iter)) != NULL) {
      total += findMemories(node, dry_run);
    }
  }
  return total;
} // UInt32 CarbonMemEdit::findMemories

bool CompWizardMemEditor::nodeIsDescendant(const QString& moduleName, const CarbonDBNode* node)
{
 const CarbonDatabaseNode* curr = node;

  // Compose all names to root
  while (curr != NULL) 
  {
    QString modName = carbonDBComponentName(mDB, curr);
    if (moduleName == modName)
      return true;

    curr = carbonDBNodeGetParent(mDB, curr);
  }
  return false;
}

QTreeWidgetItem* CompWizardMemEditor::addMemoryNode(const CarbonDBNode* node) 
{
  QString path = carbonDBNodeGetFullName(mDB, node);
  if (mMemoryNameMap.contains(path))
    return NULL;

  // If this node is part of an existing sub-component
  // (which is the embodiment) of an embedded memory, skip it.
  for(UInt32 i=0; i<mCfg->numSubComponents(); i++)
  {
    CarbonCfg* cfg = mCfg->getSubComponent(i);
    QString compTag = cfg->getComponentTag();
    if (nodeIsDescendant(compTag, node))
      return NULL; // Skip embedded memory items
  }

  UtString name;
  mCfg->getUniqueMemName(&name, carbonDBNodeGetLeafName(mDB, node));
  SInt32 left = carbonDBGet2DArrayLeftAddr(mDB, node);
  SInt32 right = carbonDBGet2DArrayRightAddr(mDB, node);
  SInt32 msb = carbonDBGetMSB(mDB, node);
  SInt32 lsb = carbonDBGetLSB(mDB, node);
  UInt32 maxAddr = std::abs(right - left); // this is not 'num addrs'
  UInt32 width = carbonDBGetWidth(mDB, node);

  UtString memPath; memPath << path;
  CarbonCfgMemory* mem = mCfg->addMemory(memPath.c_str(), name.c_str(),
                                         "", // init file
                                         maxAddr, width,
                                         eCarbonCfgReadmemh,
                                         ""); // comment

  // Memory block offsets and RTL msb/lsb were not being set properly
  if (mem->numMemoryBlocks() > 0)
  {
    CarbonCfgMemoryBlock* block = mem->getMemoryBlock(0);

    block->getLoc(0)->castRTL()->putRTLStartWordOffset(left);
    block->getLoc(0)->castRTL()->putRTLEndWordOffset(right);

    block->getLoc(0)->castRTL()->putRTLMsb(msb);
    block->getLoc(0)->castRTL()->putRTLLsb(lsb);
  }

  return new ESLMemory(mMemoriesRoot, mem);
}

void CompWizardMemEditor::canRedoChanged(bool enabled)
{
  mRedoEnabled = enabled;
  mActionRedo->setEnabled(enabled);
}
void CompWizardMemEditor::canUndoChanged(bool enabled)
{
  mUndoEnabled = enabled;
  mActionUndo->setEnabled(enabled);
}

// called whenever tree selection changes
void CompWizardMemEditor::treeWidgetSelectionChanged()
{
}

void CompWizardMemEditor::modelCompilationFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
}
void CompWizardMemEditor::createMenus()
{
}

CompWizardMemEditor::~CompWizardMemEditor()
{

}

void CompWizardMemEditor::undo()
{
  if (mUndoEnabled)
  {
    qDebug() << "undo" << mUndoStack->undoText();
    theApp->statusBar()->showMessage(QString("Undo %1").arg(mUndoStack->undoText()), 1000);
    mUndoStack->undo();
  }
  else
  {
    theApp->statusBar()->showMessage("Nothing to Undo", 3000);
    QApplication::beep();
  }
}

void CompWizardMemEditor::redo()
{
  if (mRedoEnabled)
  {
    qDebug() << "redo" << mUndoStack->redoText();
    theApp->statusBar()->showMessage(QString("Redo %1").arg(mUndoStack->redoText()), 1000);
    mUndoStack->redo();
  }
  else
  {
    theApp->statusBar()->showMessage("Nothing to Redo", 3000);
    QApplication::beep();
  }
}



void CompWizardMemEditor::createActions()
{
  mActionUndo = new QAction(QIcon(":/cmm/Resources/undo.png"), tr("U&ndo"), this);
  mActionUndo->setEnabled(false);
  mActionUndo->setShortcut(tr("Ctrl+Z"));
  addAction(mActionUndo);
  CQT_CONNECT(mActionUndo, triggered(), this, undo());

  mActionRedo = new QAction(QIcon(":/cmm/Resources/redo.png"), tr("Re&do"), this);
  QList<QKeySequence> redoShortcuts;
  redoShortcuts << tr("Ctrl+Y") << tr("Shift+Ctrl+Z");
  mActionRedo->setShortcuts(redoShortcuts);
  mActionRedo->setEnabled(false);
  addAction(mActionRedo);
  CQT_CONNECT(mActionRedo, triggered(), this, redo());

  mActionDelete = new QAction(QIcon(":/cmm/Resources/DeleteHS.png"), tr("Dele&te"), this);
  mActionDelete->setShortcut(tr("Del"));
  addAction(mActionDelete);
  CQT_CONNECT(mActionDelete, triggered(), this, actionDelete());

  mActionAddMemory = new QAction(QIcon(":/cmm/Resources/memoryMap.png"), tr("Add All Memories"), this);
  mActionAddMemory->setToolTip("Add all Memories");
  mActionAddMemory->setStatusTip("Add a memory");
  addAction(mActionAddMemory);
  CQT_CONNECT(mActionAddMemory, triggered(), this, actionAddMemory());

  mActionAddSubComponent = new QAction(QIcon(":/cmm/Resources/bank.png"), tr("Add a SubComponent"), this);
  mActionAddSubComponent->setToolTip("Add a SubComponent");
  mActionAddSubComponent->setStatusTip("Add a sub component");
  addAction(mActionAddSubComponent);
  CQT_CONNECT(mActionAddSubComponent, triggered(), this, actionAddSubComponent());

}

CarbonProject* CompWizardMemEditor::getProject()
{
  CarbonProjectWidget* pw = getProjectWidget();
  if (pw)
    return pw->project();
  else
    return NULL;
}

CarbonProjectWidget* CompWizardMemEditor::getProjectWidget()
{
  return theApp->getContext()->getCarbonProjectWidget();
}


CarbonComponent* CompWizardMemEditor::getComponent()
{
  CarbonProjectWidget* pw = getProjectWidget();
  if (pw)
  {
    EditorMode mode = getEditorMode();
    switch(mode)
    {
      case eModeSystemC: return pw->getActiveSystemCComponent(); break;
      case eModeSocDesigner: return pw->getActiveMaxsimComponent(); break;
      case eModeCoWare: return pw->getActiveCowareComponent(); break;
    }
  }
  
  return NULL;
}

void CompWizardMemEditor::setEditorMode(EditorMode mode)
{
  mEditorMode = mode;
  if (mEditorMode == CompWizardMemEditor::eModeSocDesigner)
    ui.toolButtonAddSubcomponent->setVisible(true);
  else
    ui.toolButtonAddSubcomponent->setVisible(false);
}

void CompWizardMemEditor::createToolbar(QWidget*)
{
  ui.toolButtonDelete->setDefaultAction(mActionDelete);
  ui.toolButtonUndo->setDefaultAction(mActionUndo);
  ui.toolButtonRedo->setDefaultAction(mActionRedo);
  ui.toolButtonAddMem->setDefaultAction(mActionAddMemory);
  ui.toolButtonAddSubcomponent->setDefaultAction(mActionAddSubComponent);
}



void CompWizardMemEditor::save()
{
  qDebug() << "Saving Port Editor";
}

void CompWizardMemEditor::closeEvent(QCloseEvent*)
{
  ui.treeWidget->closeTree();
 // mUndoView->close();
}

void CompWizardMemEditor::treeWidgetModified(bool value)
{
  setWindowModified(value);
  emit widgetModified(value);
}

void CompWizardMemEditor::setCcfg(CarbonCfg* cfg)
{
  mCfg = cfg;
  populate(mCfg->getDB(), false);
}
bool CompWizardMemEditor::populate(CarbonDB* carbonDB, bool)
{
  mDB = carbonDB;
  CarbonComponent* comp = getComponent();
  INFO_ASSERT(comp, "Expecting component");

  populateFromCcfg();

  return true;
}

void CompWizardMemEditor::populateFromCcfg()
{
  INFO_ASSERT(getComponent(), "Expecting Component");

  ui.treeWidget->setUpdatesEnabled(false);

  // Inform the tree widget about this ccfg
  ui.treeWidget->setCcfg(mCfg);

  // Build up the tree
  ui.treeWidget->clear();

  populateMemories();

  ui.treeWidget->header()->resizeSections(QHeaderView::ResizeToContents);

  ui.treeWidget->setUpdatesEnabled(true);

  setWindowModified(false);
}

void CompWizardMemEditor::populateMemories()
{
  QApplication::setOverrideCursor(Qt::WaitCursor);

  mMemoriesRoot = new MemoriesRootItem(ui.treeWidget, mCfg);

  mMemoriesRoot->populateMemories(mCfg);

  mMemoriesRoot->check(NULL, false);

  mMemoryDropItem = new MemoryDropItem(ui.treeWidget, mCfg);

  QApplication::restoreOverrideCursor();
}

// Delegate


// Editor Delegate
MemEditorDelegate::MemEditorDelegate(QObject *parent, MemEditorTreeWidget* tw, CompWizardMemEditor* pe)
: QItemDelegate(parent), mTree(tw), mMemEditor(pe)
{
}


void MemEditorDelegate::valueChanged()
{
  emit commitData(qobject_cast<QWidget*>(sender()));
  emit closeEditor(qobject_cast<QWidget*>(sender()));
}

void MemEditorDelegate::currentIndexChanged(int)
{
  emit commitData(qobject_cast<QWidget*>(sender()));
  emit closeEditor(qobject_cast<QWidget*>(sender()));
}

QWidget *MemEditorDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                              const QModelIndex &index) const
{
  int col = index.column();

  QTreeWidgetItem* treeItem = mTree->currentItem();

  MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(treeItem);
  if (item == NULL)
    return NULL;
  
  QWidget* w = item->createEditor(this, parent, col);
  
  mTree->setEditorWidget(w);

  return w;
}


void MemEditorDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit)
  {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }

  QComboBox* cbox = qobject_cast<QComboBox*>(editor);
  if (cbox)
  {
    QString txt = index.model()->data(index, Qt::EditRole).toString();
    int ci = cbox->findText(txt);
    if (ci != -1)
        cbox->setCurrentIndex(ci);
    else // Unknown, add it on the fly and choose it.
    {
      cbox->addItem(txt);
      cbox->setCurrentIndex(cbox->findText(txt));
    }
    qDebug() << "set value" << txt;   
  }
}
void MemEditorDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                          const QModelIndex &index) const
{
  int col = index.column();

  QTreeWidgetItem* treeItem = mTree->currentItem();

  MemEditorTreeItem* item = dynamic_cast<MemEditorTreeItem*>(treeItem);
  if (item == NULL)
    return;

  item->setModelData(this, model, index, editor, col);

  mTree->setEditorWidget(NULL);
}



