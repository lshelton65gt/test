// -*-C++-*-

/*****************************************************************************

 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/OSWrapper.h"
#include <QtGui>
#include <QMenuBar>
#include <QToolBar>
#include "gui/CQt.h"
#include "Mdi.h"
#include "MdiRuntime.h"
#include "CarbonRuntimeState.h"
#include "RuntimeStateEditor.h"
#include "shell/ReplaySystem.h"
#include "CarbonMakerContext.h"
#include "WorkspaceModelMaker.h"
#include "CarbonProjectWidget.h"

MDIRuntimeTemplate::MDIRuntimeTemplate(CarbonMakerContext *ctx)
  : MDIDocumentTemplate("Carbon System State", "New Runtime Configuration", "Carbon System State (*.css *.csu)"),
    mContext(ctx)
{
  mContinueSimButton = NULL;
  mSimulationToolbar = NULL;
  actionFileExit = new QAction(this);
  actionFileExit->setObjectName(QString::fromUtf8("actionFileExit"));
  actionHelpAbout = new QAction(this);
  actionHelpAbout->setObjectName(QString::fromUtf8("actionHelpAbout"));
  actionHelpDocumentation = new QAction(this);
  actionHelpDocumentation->setObjectName(QString::fromUtf8("actionHelpDocumentation"));
  actionHelpDocumentation2 = new QAction(this);
  actionHelpDocumentation2->setObjectName(QString::fromUtf8("actionHelpDocumentation2"));
  actionViewCarbonModels = new QAction(this);
  actionViewCarbonModels->setObjectName(QString::fromUtf8("actionViewCarbonModels"));
  actionViewProperties = new QAction(this);
  actionViewProperties->setObjectName(QString::fromUtf8("actionViewProperties"));
  actionViewProperties->setCheckable(true);
  actionViewProperties->setChecked(true);
  actionViewSimulationHistory = new QAction(this);
  actionViewSimulationHistory->setObjectName(QString::fromUtf8("actionViewSimulationHistory"));
  actionViewSimulationHistory->setCheckable(true);
  actionViewSimulationHistory->setChecked(true);
  actionViewToolbarsReplay = new QAction(this);
  actionViewToolbarsReplay->setObjectName(QString::fromUtf8("actionViewToolbarsReplay"));
  actionViewToolbarsReplay->setCheckable(true);
  actionViewToolbarsReplay->setChecked(true);
  actionViewToolbarsReplay->setEnabled(true);
  actionViewToolbarsOnDemand = new QAction(this);
  actionViewToolbarsOnDemand->setObjectName(QString::fromUtf8("actionViewToolbarsOnDemand"));
  actionViewToolbarsOnDemand->setCheckable(true);
  actionViewToolbarsOnDemand->setChecked(true);
  actionViewToolbarsOnDemand->setEnabled(true);
  actionReplayRecord = new QAction(this);
  actionReplayRecord->setObjectName(QString::fromUtf8("actionReplayRecord"));
  actionReplayRecord->setCheckable(true);
  actionReplayRecord->setEnabled(true);
  actionReplayRecord->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/replayRecord.png")));
  actionReplayPlay = new QAction(this);
  actionReplayPlay->setObjectName(QString::fromUtf8("actionReplayPlay"));
  actionReplayPlay->setCheckable(true);
  actionReplayPlay->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/replayPlayback.png")));
  actionReplayNormal = new QAction(this);
  actionReplayNormal->setObjectName(QString::fromUtf8("actionReplayNormal"));
  actionReplayNormal->setCheckable(true);
  actionReplayNormal->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/replayNormal.png")));
  actionOnDemandStart = new QAction(this);
  actionOnDemandStart->setObjectName(QString::fromUtf8("actionOnDemandStart"));
  actionOnDemandStart->setCheckable(true);
  actionOnDemandStart->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/onDemandStart.png")));
  actionOnDemandEnableTrace = new QAction(this);
  actionOnDemandEnableTrace->setObjectName(QString::fromUtf8("actionOnDemandEnableTrace"));
  actionOnDemandEnableTrace->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/CheckBoxHS.png")));

#if USE_ACTION_FOR_CONTINUE_SIM
  actionContinueSimulation = new QAction(this);
  actionContinueSimulation->setObjectName(QString::fromUtf8("actionContinueSimulation"));
#endif

  actionReplayVerbose = new QAction(this);
  actionReplayVerbose->setObjectName(QString::fromUtf8("actionReplayVerbose"));
  actionReplayVerbose->setCheckable(true);
  actionReplayVerbose->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/CheckBoxHS.png")));
  actionOnDemandStop = new QAction(this);
  actionOnDemandStop->setObjectName(QString::fromUtf8("actionOnDemandStop"));
  actionOnDemandStop->setCheckable(true);
  actionOnDemandStop->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/onDemandStop.png")));
  actionOnDemandViewTrace = new QAction(this);
  actionOnDemandViewTrace->setObjectName(QString::fromUtf8("actionOnDemandViewTrace"));
  actionOnDemandViewTrace->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/view_trace.png")));
  actionOnDemandTune = new QAction(this);
  actionOnDemandTune->setObjectName(QString::fromUtf8("actionOnDemandTune"));
  actionOnDemandTune->setCheckable(false);
  actionOnDemandTune->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/onDemandTune.png")));

  actionFileExit->setText(QApplication::translate("UiCControl", "E&xit", 0, QApplication::UnicodeUTF8));
  actionFileExit->setStatusTip(QApplication::translate("UiCControl", "Exit Carbon Runtime Controller", 0, QApplication::UnicodeUTF8));
  actionHelpAbout->setText(QApplication::translate("UiCControl", "&About Carbon Runtime Controller", 0, QApplication::UnicodeUTF8));
  actionHelpDocumentation->setText(QApplication::translate("UiCControl", "View &Help Topics", 0, QApplication::UnicodeUTF8));
  actionHelpDocumentation2->setText(QApplication::translate("UiCControl", "View &Documentation", 0, QApplication::UnicodeUTF8));
  actionViewCarbonModels->setText(QApplication::translate("UiCControl", "&Carbon Models", 0, QApplication::UnicodeUTF8));
  actionViewProperties->setText(QApplication::translate("UiCControl", "&Properties", 0, QApplication::UnicodeUTF8));
  actionViewProperties->setStatusTip(QApplication::translate("UiCControl", "Show or hide the Properties window", 0, QApplication::UnicodeUTF8));
  actionViewSimulationHistory->setText(QApplication::translate("UiCControl", "&Simulation History", 0, QApplication::UnicodeUTF8));
  actionViewSimulationHistory->setStatusTip(QApplication::translate("UiCControl", "Show or hode the Simulation History window", 0, QApplication::UnicodeUTF8));
  actionViewToolbarsReplay->setText(QApplication::translate("UiCControl", "&Replay", 0, QApplication::UnicodeUTF8));
  actionViewToolbarsReplay->setStatusTip(QApplication::translate("UiCControl", "Show or hide the Replay toolbar", 0, QApplication::UnicodeUTF8));
  actionViewToolbarsOnDemand->setText(QApplication::translate("UiCControl", "&On Demand", 0, QApplication::UnicodeUTF8));
  actionViewToolbarsOnDemand->setStatusTip(QApplication::translate("UiCControl", "Show or hide the OnDemand toolbar", 0, QApplication::UnicodeUTF8));
  actionReplayRecord->setText(QApplication::translate("UiCControl", "Record", 0, QApplication::UnicodeUTF8));
  actionReplayRecord->setToolTip(QApplication::translate("UiCControl", "Request Replay Record mode for the selected models", 0, QApplication::UnicodeUTF8));
  actionReplayPlay->setText(QApplication::translate("UiCControl", "Playback", 0, QApplication::UnicodeUTF8));
  actionReplayPlay->setIconText(QApplication::translate("UiCControl", "Playback", 0, QApplication::UnicodeUTF8));
  actionReplayPlay->setToolTip(QApplication::translate("UiCControl", "Request Replay playback mode for the selected models", 0, QApplication::UnicodeUTF8));
  actionReplayNormal->setText(QApplication::translate("UiCControl", "Normal", 0, QApplication::UnicodeUTF8));
  actionReplayNormal->setIconText(QApplication::translate("UiCControl", "Normal", 0, QApplication::UnicodeUTF8));
  actionReplayNormal->setToolTip(QApplication::translate("UiCControl", "Request normal (non-Replay) mode for the selected models", 0, QApplication::UnicodeUTF8));
  actionOnDemandStart->setText(QApplication::translate("UiCControl", "Start", 0, QApplication::UnicodeUTF8));
  actionOnDemandStart->setIconText(QApplication::translate("UiCControl", "Start", 0, QApplication::UnicodeUTF8));
  actionOnDemandStart->setToolTip(QApplication::translate("UiCControl", "Start OnDemand for the selected models", 0, QApplication::UnicodeUTF8));
  
  actionOnDemandEnableTrace->setText(QApplication::translate("UiCControl", "Trace", 0, QApplication::UnicodeUTF8));
  actionOnDemandEnableTrace->setIconText(QApplication::translate("UiCControl", "Trace", 0, QApplication::UnicodeUTF8));
  actionOnDemandEnableTrace->setToolTip(QApplication::translate("UiCControl", "Enable collection of OnDemand trace information", 0, QApplication::UnicodeUTF8));
  
#if USE_ACTION_FOR_CONTINUE_SIM
  actionContinueSimulation->setText(QApplication::translate("UiCControl", "Continue Simulation", 0, QApplication::UnicodeUTF8));
  actionContinueSimulation->setIconText(QApplication::translate("UiCControl", "Continue Simulation", 0, QApplication::UnicodeUTF8));
  actionContinueSimulation->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/continueSimulate.png")));
  actionContinueSimulation->setToolTip(QApplication::translate("UiCControl", "Continue simulation", 0, QApplication::UnicodeUTF8));
  
#endif

  actionReplayVerbose->setText(QApplication::translate("UiCControl", "Verbose", 0, QApplication::UnicodeUTF8));
  actionReplayVerbose->setIconText(QApplication::translate("UiCControl", "Verbose", 0, QApplication::UnicodeUTF8));
  actionReplayVerbose->setToolTip(QApplication::translate("UiCControl", "Enable verbose output of the Replay system", 0, QApplication::UnicodeUTF8));
  actionOnDemandStop->setText(QApplication::translate("UiCControl", "Stop", 0, QApplication::UnicodeUTF8));
  actionOnDemandStop->setIconText(QApplication::translate("UiCControl", "Stop", 0, QApplication::UnicodeUTF8));
  actionOnDemandStop->setToolTip(QApplication::translate("UiCControl", "Stop OnDemand for the selected models", 0, QApplication::UnicodeUTF8));
  actionOnDemandViewTrace->setText(QApplication::translate("UiCControl", "View Trace", 0, QApplication::UnicodeUTF8));
  actionOnDemandTune->setText(QApplication::translate("UiCControl", "Tune", 0, QApplication::UnicodeUTF8));
  actionOnDemandTune->setIconText(QApplication::translate("UiCControl", "Tune", 0, QApplication::UnicodeUTF8));
  actionOnDemandTune->setToolTip(QApplication::translate("UiCControl", "Optimize OnDemand for the selected models", 0, QApplication::UnicodeUTF8));
}

void MDIRuntimeTemplate::updateMenusAndToolbars(QWidget* widget)
{
  (void) widget;
}

int MDIRuntimeTemplate::createToolbars()
{
#define ON_DEMAND_TOOLBAR 0
#if ON_DEMAND_TOOLBAR
  QToolBar *onDemandToolBar;
#endif
  QToolBar *replayToolBar;
  QToolBar *simulationToolBar;

#if ON_DEMAND_TOOLBAR
  onDemandToolBar = new QToolBar("OnDemand");
  onDemandToolBar->setObjectName(QString::fromUtf8("onDemandToolBar"));
  onDemandToolBar->setOrientation(Qt::Horizontal);
  onDemandToolBar->setIconSize(QSize(16, 16));
  onDemandToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
  //onDemandToolBar->addAction(actionOnDemandStart);
  //onDemandToolBar->addAction(actionOnDemandStop);

  // these features are not completely implemented yet, so hide them for 
  // now unless the special environment variable 
  // CARBON_ONDEMAND_TUNING is defined.
  //if (getenv("CARBON_ONDEMAND_TUNING")) {
//    onDemandToolBar->addAction(actionOnDemandEnableTrace);
//    onDemandToolBar->addAction(actionOnDemandTune);
    simulationToolbar->addAction(actionOnDemandViewTrace);
  //}
  onDemandToolBar->setToolTip(tr("OnDemand Control"));
  addToolbar(onDemandToolBar);
#endif

  replayToolBar = new QToolBar("Replay/OnDemand");
  replayToolBar->setObjectName(QString::fromUtf8("replayToolBar"));
  replayToolBar->setOrientation(Qt::Horizontal);
  replayToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
  //replayToolBar->addAction(actionReplayPlay);
  //replayToolBar->addAction(actionReplayNormal);
  //replayToolBar->addAction(actionReplayRecord);
  replayToolBar->addAction(actionReplayVerbose);

  // This "View Trace" button, which is for ondemand, might not belong
  // in the "Replay" toolbar.  I think it belongs in the Simulation toolbar,
  // which should have the "Simulate" and "Continue Simulation" button, but
  // right now the "Simulate" toolbar has been set to have wide icons.
  //
  // Furthermore, the "Verbose" switch should apply to onDemand anyway.
  //
  // Note that its identification as a "Replay" toolbar appears not to
  // be visible to users.
  replayToolBar->addAction(actionOnDemandViewTrace);

  replayToolBar->setToolTip(tr("Replay/OnDemand Control"));
  addToolbar(replayToolBar);

  simulationToolBar = new QToolBar("Simulation");
  mSimulationToolbar = simulationToolBar;

  simulationToolBar->setIconSize(QSize(48, 16));
  simulationToolBar->setObjectName(QString::fromUtf8("simulationToolBar"));
  simulationToolBar->setOrientation(Qt::Horizontal);

#if USE_ACTION_FOR_CONTINUE_SIM
  simulationToolBar->addAction(actionContinueSimulation);
  simulationToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

  // Grab the button so we can do some graphics to it when it is enabled
  mContinueSimButton
    = simulationToolBar->widgetForAction(actionContinueSimulation);
#else
  mContinueSimButton = new QPushButton("Continue Simulation");
  simulationToolBar->addWidget(mContinueSimButton);
#endif
  QPalette palette;
  mSaveSimContForeground = palette.color(QPalette::Active,
                                         mContinueSimButton->foregroundRole());
  mSaveSimContBackground = palette.color(QPalette::Active,
                                         mContinueSimButton->backgroundRole());

  addToolbar(simulationToolBar);

  return numToolbars();
}

int MDIRuntimeTemplate::createMenus()
{
  /*
  QMenu* fileMenu = new QMenu("&File");
  fileMenu->setObjectName("RuntimeFileMenu");
  fileMenu->addAction(actionFileSave);
  registerAction(actionFileSave, SLOT(fileSave()));

  addMenu(fileMenu);

  QMenu* menu = new QMenu("&Edit");
  menu->setObjectName("RuntimeEditMenu");

  menu->addAction(actionUndo);
  registerAction(actionUndo, SLOT(undo()));
  registerUpdateAction(actionUndo, SIGNAL(undoAvailable(bool)), SLOT(setEnabled(bool)));

  menu->addAction(actionRedo);
  registerAction(actionRedo, SLOT(redo()));
  registerUpdateAction(actionRedo, SIGNAL(redoAvailable(bool)), SLOT(setEnabled(bool)));

  menu->addAction(actionCut);
  registerAction(actionCut, SLOT(cut()));

  menu->addAction(actionCopy);
  registerAction(actionCopy, SLOT(copy()));
  registerUpdateAction(actionCopy, SIGNAL(copyAvailable(bool)), SLOT(setEnabled(bool)));

  menu->addAction(actionPaste);
  registerAction(actionPaste, SLOT(paste()));
  registerUpdateAction(actionPaste, SIGNAL(pasteAvailable(bool)), SLOT(setEnabled(bool)));

  menu->addAction(actionFind);
  registerAction(actionFind, SLOT(find()));

  addMenu(menu);
  */
  return numMenus();
}

MDIWidget* MDIRuntimeTemplate::createNewDocument(QWidget* parent)
{
  (void) parent;
  /*
  QRuntime* te = new QRuntime(this, parent);
  te->newFile();
  return te;
  */
  return NULL;
}

MDIWidget* MDIRuntimeTemplate::openDocument(QWidget* parent, const char* docName)
{
  CarbonProjectWidget* pw = mContext->getCarbonProjectWidget();
  
  if (pw == NULL || pw->project() == NULL)
  {
    QDockWidget* dock = mContext->getWorkspaceModelMaker()->getProjectDockWindow();
    if (dock)
      dock->setVisible(false);
    
    QDockWidget* dock1 = mContext->getWorkspaceModelMaker()->getErrorInfoDockWindow();
    if (dock1)
      dock1->setVisible(false);

    QDockWidget* dock2 = mContext->getWorkspaceModelMaker()->getRemoteConsoleDockWindow();
    if (dock2)
      dock2->setVisible(false);
  }

  if (pw)
  {
    QDockWidget* dw = pw->getHierarchyDockWidget();
    if (dw)
      dw->setVisible(false);
  }

  if (mContext->getCarbonModelsWidget() != NULL) {
    CQtContext* cqt = mContext->getQtContext();
    QMainWindow* mw = mContext->getMainWindow();
    cqt->warning(mw, "Only one .css/.csu file can be opened at a time");
  }
  else {
    RuntimeStateEditor *editor = new RuntimeStateEditor(mContext, this, parent);
    if (editor->open(docName))
      return editor;
  }
  return NULL;
}


void MDIRuntimeTemplate::carbonSystemUpdated(CarbonRuntimeState &state, CarbonReplaySystem &guiSystem, CarbonReplaySystem &simSystem)
{
  (void) simSystem;
  updateReplayToolbar(state, guiSystem);
  updateOnDemandToolbar(state, guiSystem);
  updateSimulationToolbar(state);
}

// set the state of the replay toolbar buttons to reflect the configuration
void MDIRuntimeTemplate::updateReplayToolbar(CarbonRuntimeState &state, CarbonReplaySystem &guiSystem)
{
  UInt32 numRecording = 0;
  UInt32 numPlayback = 0;
  UInt32 numNormal = 0;

  UInt32 numModels = state.numModelsSelected();

  actionReplayRecord->setEnabled(numModels > 0);
  actionReplayPlay->setEnabled(numModels > 0);
  actionReplayNormal->setEnabled(numModels > 0);

  if (numModels > 0) {
    state.replayModelCounts(&numNormal, &numPlayback, &numRecording);
    actionReplayRecord->setChecked(numRecording == numModels);
    actionReplayPlay->setChecked(numPlayback == numModels);
    actionReplayNormal->setChecked(numNormal == numModels);
  }
  actionReplayVerbose->setChecked(guiSystem.isVerbose());
}

void MDIRuntimeTemplate::updateOnDemandToolbar(CarbonRuntimeState &state, CarbonReplaySystem &/* guiSystem */)
{
  UInt32 numStop = 0;
  UInt32 numStart = 0;

  UInt32 numModels = state.numModelsSelected();

  actionOnDemandStart->setEnabled(numModels > 0);
  actionOnDemandStop->setEnabled(numModels > 0);
  actionOnDemandTune->setEnabled(numModels > 0);
  actionOnDemandEnableTrace->setEnabled(numModels > 0);
  actionOnDemandViewTrace->setEnabled(numModels > 0);

  if (numModels > 0) {
    state.onDemandModelCounts(&numStart, &numStop);
    actionOnDemandStop->setChecked(numStop == numModels);
    actionOnDemandStart->setChecked(numStart == numModels);
    actionOnDemandEnableTrace->setEnabled(numStart == numModels);
  }

#if 0
  actionOnDemandEnableTrace->setChecked(guiSystem.getOnDemandTraceRequested());

  // now look for the OnDemand trace file.  if found, enable the view trace button
  bool traceExists = state.onDemandTraceFileExists();
  actionOnDemandViewTrace->setEnabled(traceExists);
#endif
}

void MDIRuntimeTemplate::updateSimulationToolbar(CarbonRuntimeState &state)
{
  bool ena = state.isContinueSimEnabled();
#if USE_ACTION_FOR_CONTINUE_SIM
  actionContinueSimulation->setEnabled(ena);
#else
  mContinueSimButton->setEnabled(ena);
#endif

/*
  if (ena) {
    actionContinueSimulation->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/BigPlayButton.png")));
  }
  else {
    actionContinueSimulation->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/PlayButton.png")));
  }
*/

#if 0
  // TBD: colorize according to replay/onDemand modes, per ReplayGUI.cxx
  QPalette palette;
  if (ena) {
    palette.setColor(mContinueSimButton->backgroundRole(), QColor(Qt::green));
    palette.setColor(mContinueSimButton->foregroundRole(), QColor(Qt::black));

    // The above code does not have the desired effect.  It leaves the
    // button uncolored except when the mouse is over it.  The following
    // attempts were also unsuccessful.  For some reason the toolbar buttons
    // are special in some way, as this worked fine in ReplayGUI.cxx with
    // a QPushButton.

#if 0
    palette.setColor(QPalette::Inactive, mContinueSimButton->backgroundRole(),
                     QColor(Qt::green));
    palette.setColor(QPalette::Inactive, mContinueSimButton->foregroundRole(),
                     QColor(Qt::black));
    palette.setColor(QPalette::Normal, mContinueSimButton->backgroundRole(),
                     QColor(Qt::green));
    palette.setColor(QPalette::Normal, mContinueSimButton->foregroundRole(),
                     QColor(Qt::black));
    palette.setColor(QPalette::Disabled, mContinueSimButton->backgroundRole(),
                     QColor(Qt::green));
    palette.setColor(QPalette::Disabled, mContinueSimButton->foregroundRole(),
                     QColor(Qt::black));
#endif
  }
  else {
    palette.setColor(mContinueSimButton->backgroundRole(),
                     mSaveSimContBackground);
    palette.setColor(mContinueSimButton->foregroundRole(),
                     mSaveSimContForeground);
  }
  mContinueSimButton->setPalette(palette);
#endif
}

