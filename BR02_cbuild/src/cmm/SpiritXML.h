#ifndef __SPIRITXML_H__
#define __SPIRITXML_H__


// this file supports Spirit_1.2, it probably should be called Spirit12.h

#include "util/CarbonPlatform.h"
#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"

#include "gui/CQt.h"

#include "Scripting.h"

#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>

class JavaScriptAPIs;

class CarbonProjectWidget;

class SpiritXML;
class SpiritAddressBlocks;
class SpiritAddressBlock;
class SpiritMemoryMaps;
class SpiritMemoryMap;
class SpiritBank;
class SpiritAddressBlock;
class SpiritRegister;
class SpiritEnums;
class MemoryMapsItem;

class SpiritEnum : public QObject
{
  Q_OBJECT
  Q_CLASSINFO("EnumPrefix", "SpiritEnum");

public:
  enum SpiritFormat { UnsetFormat, BitString, Boolean, Float, Long, String };
  enum SpiritResolve { UnsetResolve, Immediate, User, Dependent, Generated };
  enum SpiritAccess { UnsetAccess, ReadWrite, ReadOnly, WriteOnly };
  enum SpiritUsage { UnsetUsage, Memory, Register, Reserved };
  enum SpiritEndianness { UnsetEndian, LittleEndian, BigEndian };
  enum SpiritLocation { UnsetLocation, LocationRegister, LocationArray, LocationConstant };
  enum SpiritAlignment { Parallel, Serial };
  enum SpiritDirection { Input, Output, Bidirect };
  
  Q_ENUMS(SpiritAlignment);
  Q_ENUMS(SpiritFormat);
  Q_ENUMS(SpiritResolve);
  Q_ENUMS(SpiritAccess);
  Q_ENUMS(SpiritUsage);
  Q_ENUMS(SpiritEndianness);
  Q_ENUMS(SpiritLocation);
  Q_ENUMS(SpiritDirection);
};

SpiritEnum::SpiritAccess stringToAccess(const QString& v);
SpiritEnum::SpiritFormat stringToFormat(const QString& v);
SpiritEnum::SpiritResolve stringToResolve(const QString& v);
SpiritEnum::SpiritUsage stringToUsage(const QString& v);
SpiritEnum::SpiritEndianness stringToEndian(const QString& v);
SpiritEnum::SpiritDirection stringToDirection(const QString& v);

QString resolveToString(SpiritEnum::SpiritResolve v);
QString formatToString(SpiritEnum::SpiritFormat v);
QString usageToString(SpiritEnum::SpiritUsage v);
QString accessToString(SpiritEnum::SpiritAccess v);
QString endianToString(SpiritEnum::SpiritEndianness v);
QString directionToString(SpiritEnum::SpiritDirection v);

// Vendor Extensions
QString locationToString(SpiritEnum::SpiritLocation v);
SpiritEnum::SpiritLocation stringToLocation(const QString& v);

// A Spirit Memory Map
// will either have a single Bank or a single Address Block
// contained within in it, but never both.
class SpiritMemoryMap : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString name READ getName)
  Q_PROPERTY(SpiritBank* bank READ getBank);
  Q_PROPERTY(SpiritAddressBlock* addressBlock READ getAddressBlock)

public:
  SpiritMemoryMap(QObject* parent=0) : QObject(parent)
  {
    mBank = NULL;
    mAddressBlock = NULL;
  }
  static SpiritMemoryMap* deserialize(XmlErrorHandler* eh, const QDomElement& parent);
  void serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh);

public slots:
  SpiritBank* getBank() const { return mBank; }
  SpiritAddressBlock* getAddressBlock() const { return mAddressBlock; }

  QString getName() const { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

private:
  QString mName;
  SpiritBank* mBank;
  SpiritAddressBlock* mAddressBlock;
};

class SpiritBaseAddress : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString id READ getId)
  Q_PROPERTY(QString name READ getName)
  Q_PROPERTY(QString prompt READ getPrompt)
  Q_PROPERTY(QString configGroups READ getConfigGroups)
  Q_PROPERTY(SpiritEnum::SpiritResolve resolve READ getResolve);
  Q_PROPERTY(SpiritEnum::SpiritFormat format READ getFormat)
  Q_PROPERTY(QString value READ getValue)
  
public:
  static SpiritBaseAddress* deserialize(XmlErrorHandler* eh, const QDomElement& parent);
  void serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh);

public slots:
  QString getId() const { return mId; }
  void setId(const QString& newVal) { mId=newVal; }

  QString getName() const { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

  QString getPrompt() const { return mPrompt; }
  void setPrompt(const QString& newVal) { mPrompt=newVal; }

  QString getConfigGroups() const { return mConfigGroups; }
  void setConfigGroups(const QString& newVal) { mConfigGroups=newVal; }

  SpiritEnum::SpiritResolve getResolve() const { return mResolve; }
  void setResolve(SpiritEnum::SpiritResolve newVal) { mResolve=newVal; }

  SpiritEnum::SpiritFormat getFormat() const { return mFormat; }
  void setFormat(SpiritEnum::SpiritFormat newVal) { mFormat=newVal; }

  QString getValue() const { return mValue; }
  void setValue(const QString& newVal) { mValue=newVal; }

private:
  QString mId;
  QString mName;
  QString mConfigGroups;
  QString mPrompt;
  SpiritEnum::SpiritResolve mResolve;
  SpiritEnum::SpiritFormat mFormat;
  QString mValue;
};

class SpiritRange : public QObject
{
  Q_OBJECT
  Q_PROPERTY(SpiritEnum::SpiritResolve resolve READ getResolve)
  Q_PROPERTY(QString id READ getId)
  Q_PROPERTY(QString dependency READ getDependency)
  Q_PROPERTY(QString value READ getValue)
  Q_PROPERTY(SpiritEnum::SpiritFormat format READ getFormat)

public:
  static SpiritRange* deserialize(XmlErrorHandler* eh, const QDomElement& parent);
  void serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh);

public slots:
  void setResolve(SpiritEnum::SpiritResolve newVal) { mResolve=newVal; }
  SpiritEnum::SpiritResolve getResolve() const { return mResolve; }

  QString getId() const { return mId; }
  void setId(const QString& newVal) { mId=newVal; }

  QString getDependency() const { return mDependency; }
  void setDependency(const QString& newVal) { mDependency=newVal; }

  QString getValue() const { return mValue; }
  void setValue(const QString& newVal) { mValue=newVal; }

  SpiritEnum::SpiritFormat getFormat() const { return mFormat; }
  void setFormat(SpiritEnum::SpiritFormat newVal) { mFormat=newVal; }

private:
  QString mId;
  QString mDependency;
  SpiritEnum::SpiritResolve mResolve;
  SpiritEnum::SpiritFormat mFormat;
  QString mValue;
};

class SpiritReset : public QObject
{
  Q_OBJECT

  Q_PROPERTY(quint32 value READ getValue)
  Q_PROPERTY(quint32 mask READ getMask)

public:
  static SpiritReset* deserialize(XmlErrorHandler* eh, const QDomElement& parent);
  void serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh);
  SpiritReset()
  {
    mValue = 0;
    mMask = 0;
    mHasMask = false;
    mHasValue = false;
  }

public slots:
  void setValue(quint32 newVal) { mHasValue = true; mValue = newVal; }
  quint32 getValue() const { return mValue; }

  void setMask(quint32 newVal) { mHasMask = true; mMask = newVal; }
  quint32 getMask() const { return mMask; }

private:
  bool mHasMask;
  bool mHasValue;
  quint32 mValue;
  quint32 mMask;
};

class SpiritBitOffset : public QObject
{
  Q_OBJECT

  Q_PROPERTY(quint32 bitOffset READ getBitOffset)
  Q_PROPERTY(SpiritEnum::SpiritResolve resolve READ getResolve)
  Q_PROPERTY(SpiritEnum::SpiritFormat format READ getFormat)
  Q_PROPERTY(QString id READ getId)
  Q_PROPERTY(QString dependency READ getDependency)
  Q_PROPERTY(QString value READ getValue)

public:
  SpiritBitOffset()
  {
    mBitOffset = 0;
    mResolve = SpiritEnum::UnsetResolve;
    mFormat = SpiritEnum::UnsetFormat;
  }

  static SpiritBitOffset* deserialize(XmlErrorHandler* eh, const QDomElement& parent);
  void serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh);

public slots:
  void setBitOffset(quint32 newVal) { mBitOffset=newVal;}
  quint32 getBitOffset() { return mBitOffset; }

  SpiritEnum::SpiritResolve getResolve() const { return mResolve; }
  void setResolve(SpiritEnum::SpiritResolve newVal) { mResolve=newVal; }

  SpiritEnum::SpiritFormat getFormat() const { return mFormat; }
  void setFormat(SpiritEnum::SpiritFormat newVal) { mFormat=newVal; }

  QString getId() const { return mId; }
  void setId(const QString& newVal) { mId=newVal; }

  QString getDependency() const { return mDependency; }
  void setDependency(const QString& newVal) { mDependency=newVal; }

  QString getValue() const { return mValue; }
  void setValue(const QString& newVal) { mValue=newVal; }

private:
  quint32 mBitOffset;
  SpiritEnum::SpiritResolve mResolve;
  SpiritEnum::SpiritFormat mFormat;
  QString mId;
  QString mDependency;
  QString mValue;
};

class SpiritValue : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString value READ getValue WRITE setValue);
  Q_PROPERTY(QString name READ getName WRITE setName);
  Q_PROPERTY(QString description READ getDescription WRITE setDescription);

public:
  SpiritValue()
  {
  }

  static SpiritValue* deserialize(XmlErrorHandler* eh, const QDomElement& parent);
  void serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh);

public slots:
  QString getValue() const { return mValue; }
  void setValue(const QString& newVal) { mValue=newVal; }

  void setName(const QString& newVal) { mName=newVal; }
  QString getName() const { return mName; }
  
  void setDescription(const QString& newVal) { mDescription=newVal; }
  QString getDescription() const { return mDescription; }

private:
  QString mValue;
  QString mName;
  QString mDescription;
};

class SpiritValues : public QObject
{
  Q_OBJECT
  Q_PROPERTY(quint32 numValues READ getNumValues);

public:
  SpiritValues()
  {
  }

  void addValue(SpiritValue* sv) { mValues.append(sv); }
  void deserialize(XmlErrorHandler* eh, const QDomElement& parent);
  void serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh);

public slots:
  quint32 getNumValues() const { return mValues.count(); }

  SpiritValue* getValue(quint32 index) { return mValues[index]; }


private:
  QList<SpiritValue*> mValues;
};

// Register field
class SpiritField : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString name READ getName)
  Q_PROPERTY(QString description READ getDescription)
  Q_PROPERTY(SpiritBitOffset* bitOffset READ getBitOffset)
  Q_PROPERTY(quint32 bitWidth READ getBitWidth)
  Q_PROPERTY(quint32 memoryIndex READ getMemoryIndex)
  Q_PROPERTY(QString rtlPath READ getRTLPath)
  Q_PROPERTY(SpiritEnum::SpiritLocation location READ getLocation)
  Q_PROPERTY(SpiritRegister* regiter READ getRegister)
  Q_PROPERTY(quint32 partSelectLeft READ getPartSelectLeft)
  Q_PROPERTY(quint32 partSelectRight READ getPartSelectRight)
  Q_PROPERTY(SpiritValues* values READ getValues)

public:
  SpiritField()
  {
    mMemoryIndex = 0;
    mBitWidth = 0;
    mBitOffset = NULL;
    mLeft = 0;
    mRight = 0;
    mRegister = NULL;
    mLocation = SpiritEnum::LocationRegister;
    mHasRange = false;
    mCarbonNode = NULL;
  }

  static SpiritField* deserialize(XmlErrorHandler* eh, const QDomElement& parent);
  void serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh);

public slots:
  SpiritValues* getValues() { return &mValues; }

  void setName(const QString& newVal) { mName=newVal; }
  QString getName() const { return mName; }
  
  void setDescription(const QString& newVal) { mDescription=newVal; }
  QString getDescription() const { return mDescription; }

  SpiritBitOffset* getBitOffset() const { return mBitOffset; }
  void setBitOffset(SpiritBitOffset* newVal) { mBitOffset=newVal; }

  void setBitWidth(quint32 newVal) { mBitWidth=newVal; }
  quint32 getBitWidth() const { return mBitWidth; }

  void setAccess(SpiritEnum::SpiritAccess newVal) { mAccess=newVal; }
  SpiritEnum::SpiritAccess getAccess() const { return mAccess; }

  void setMemoryIndex(quint32 newVal) { mMemoryIndex=newVal; }
  quint32 getMemoryIndex() const { return mMemoryIndex; }

  void setRTLPath(const QString& newVal) { mRTLPath=newVal; }
  QString getRTLPath() const { return mRTLPath; }

  void setLocation(SpiritEnum::SpiritLocation newVal) { mLocation=newVal; }
  SpiritEnum::SpiritLocation getLocation() const { return mLocation; }

  void setRegister(SpiritRegister* newVal) { mRegister=newVal; }
  SpiritRegister* getRegister() const { return mRegister; }

  quint32 getPartSelectLeft() { return mLeft; }
  void setPartSelectLeft(quint32 newVal) { mLeft = newVal; }

  quint32 getPartSelectRight() { return mRight; }
  void setPartSelectRight(quint32 newVal) { mRight = newVal; }

  // If true, then the partSelect left, right are used.
  bool getHasRange() { return mHasRange; }
  void setHasRange(bool newVal) { mHasRange = newVal; }

  void setCarbonNode(const CarbonDBNode* newVal) { mCarbonNode=newVal; }
  const CarbonDBNode* getCarbonNode() const { return mCarbonNode; }

  bool checkForProblems(CarbonDB* db, QString& errMessage);

private:
  static bool checkForOverlap(SpiritField* field, QString& errMsg);

private:
  quint32 mLeft;
  quint32 mRight;
  bool mHasRange;
  quint32 mMemoryIndex;
  const CarbonDBNode* mCarbonNode;

private:
  QString mName;
  QString mRTLPath;
  QString mDescription;
  quint32 mBitWidth;
  SpiritBitOffset* mBitOffset;
  SpiritEnum::SpiritAccess mAccess;
  SpiritRegister* mRegister;
  SpiritEnum::SpiritLocation mLocation;
  SpiritValues mValues;
};

class SpiritRegister : public QObject
{
  Q_OBJECT

  Q_PROPERTY(quint32 numUnusedBits READ numUnusedBits)
  Q_PROPERTY(SpiritAddressBlock* addressBlock READ getAddressBlock)
  Q_PROPERTY(QString name READ getName)
  Q_PROPERTY(QString description READ getDescription)
  Q_PROPERTY(quint32 dimension READ getDimension)
  Q_PROPERTY(quint32 addressOffset READ getAddressOffset)
  Q_PROPERTY(quint32 size READ getSize)
  Q_PROPERTY(quint32 numFields READ numFields)

public:
  SpiritRegister()
  {
    mVolatile = false;
    mSize = 0;
    mDimension = 0;
    mAddressOffset = 0;
    mAccess = SpiritEnum::ReadWrite;
    mReset = NULL;
    mAddressBlock = NULL;
  }

  static SpiritRegister* deserialize(XmlErrorHandler* eh, const QDomElement& parent);
  void serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh);

public slots:
  quint32 numUnusedBits();

  void setAddressBlock(SpiritAddressBlock* newVal) { mAddressBlock=newVal; }
  SpiritAddressBlock* getAddressBlock() const { return mAddressBlock; }

  void setName(const QString& newVal) { mName=newVal; }
  QString getName() const { return mName; }
  
  void setDescription(const QString& newVal) { mDescription=newVal; }
  QString getDescription() const { return mDescription; }

  void setDimension(quint32 newVal) { mDimension=newVal; }
  quint32 getDimension() const { return mDimension; }

  void setAddressOffset(quint32 newVal) { mAddressOffset=newVal; }
  quint32 getAddressOffset() const { return mAddressOffset; }
  
  void setSize(quint32 newVal) { mSize=newVal; }
  quint32 getSize() const { return mSize; }

  void setAccess(SpiritEnum::SpiritAccess newVal) { mAccess=newVal; }
  SpiritEnum::SpiritAccess getAccess() const { return mAccess; }

  SpiritReset* getReset() const { return mReset; }


  quint32 numFields() const { return mFields.count(); }
  SpiritField* getField(quint32 i)
  {
    if (i < numFields())
      return mFields[i];
    else
      return NULL;
  }

  quint32 nextBitOffset()
  {
    quint32 maxOffset = 0;

    foreach(SpiritField* field, mFields)
    {
      quint32 offset = field->getBitOffset()->getBitOffset() + field->getBitWidth();
      if (offset > maxOffset)
        maxOffset = offset;
    }
    return maxOffset;
  }

  QString nextFieldName()
  {
    int fieldIndex = mFields.count();
    for (int i=fieldIndex; i<255; i++)
    {
      QString fieldName;
      fieldName = QString("field%1").arg(i);
      if (findField(fieldName) == NULL)
        return fieldName;
    }
    return "";
  }

  // Find named field
  SpiritField* findField(const QString& fieldName)
  {
    foreach(SpiritField* field, mFields)
    {
      if (field->getName() == fieldName)
        return field;
    }
    return NULL;
  }

  void addField(SpiritField* field)
  {
    mFields.push_back(field);
  }

  void removeField(SpiritField* field)
  {
    if (mFields.removeOne(field))
      delete field;
  }

  bool checkForProblems(CarbonDB* db, QString& errMessage);

private:
  QString mName;
  QString mDescription;
  quint32 mSize; // Size in bits
  quint32 mDimension;
  quint32 mAddressOffset;
  bool mVolatile;
  SpiritEnum::SpiritAccess mAccess;
  SpiritReset* mReset;
  QList<SpiritField*> mFields;
  SpiritAddressBlock* mAddressBlock; // that this belongs to
};


class SpiritAddressBlock : public QObject
{
  Q_OBJECT

  Q_PROPERTY(SpiritBank* parentBank READ getParentBank)
  Q_PROPERTY(SpiritMemoryMap* parentMemoryMap READ getParentMemoryMap)
  Q_PROPERTY(SpiritBaseAddress* baseAddress READ getBaseAddress)
  Q_PROPERTY(SpiritBitOffset* bitOffset READ getBitOffset)
  Q_PROPERTY(QString nameAttribute READ getNameAttribute)
  Q_PROPERTY(QString resolvedName READ getResolvedName)
  Q_PROPERTY(SpiritRange* range READ getRange)
  Q_PROPERTY(SpiritEnum::SpiritEndianness endian READ getEndian)
  Q_PROPERTY(SpiritEnum::SpiritUsage usage READ getUsage)
  Q_PROPERTY(SpiritEnum::SpiritAccess access READ getAccess)
  Q_PROPERTY(QString path READ getPath)
  Q_PROPERTY(quint32 width READ getWidth)

public:
  SpiritAddressBlock()
  {
    mRange = NULL;
    mWidth = 0;
    mAccess = SpiritEnum::UnsetAccess;
    mBaseAddress = NULL;
    mBitOffset = NULL;
    mUsage = SpiritEnum::UnsetUsage;
    mParentBank = NULL;
    mParentMemoryMap = NULL;
    mEndian = SpiritEnum::LittleEndian;
  }

  void serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh);
  static SpiritAddressBlock* deserialize(XmlErrorHandler* eh, const QDomElement& parent);

public slots:
  QString getPath();

  void setParentBank(SpiritBank* parent) { mParentBank=parent; }
  void setParentMemoryMap(SpiritMemoryMap* parent) { mParentMemoryMap=parent; }

  SpiritBank* getParentBank() const { return mParentBank; }
  SpiritMemoryMap* getParentMemoryMap() const { return mParentMemoryMap; }

  SpiritBaseAddress* getBaseAddress() { return mBaseAddress; }
  SpiritBitOffset* getBitOffset() { return mBitOffset; }

  QString getName() const { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

  QString getNameAttribute() const { return mNameAttribute; }
  void setNameAttribute(const QString& newVal) { mNameAttribute=newVal; }

  QString getResolvedName() const 
  { 
    if (!mName.isEmpty())
      return mName;
    else
      return mNameAttribute;
  }

  quint32 getWidth() const { return mWidth; }
  void setWidth(quint32 newVal) { mWidth=newVal; }

  SpiritRange* getRange() const { return mRange; }
  void setRange(SpiritRange* newVal) { mRange=newVal; }

  void setEndian(SpiritEnum::SpiritEndianness newVal) { mEndian=newVal; }
  SpiritEnum::SpiritEndianness getEndian() const { return mEndian; }

  void setAccess(SpiritEnum::SpiritAccess newVal) { mAccess=newVal; }
  SpiritEnum::SpiritAccess getAccess() const { return mAccess; }

  void setUsage(SpiritEnum::SpiritUsage newVal) {  mUsage=newVal; }
  SpiritEnum::SpiritUsage getUsage() const { return mUsage; }

  quint32 numRegisters() const { return mRegisters.count(); }
  SpiritRegister* getRegister(quint32 i)
  {
    if (i < numRegisters())
      return mRegisters[i];
    else
      return NULL;
  }
  SpiritRegister* findRegister(const QString& name)
  {
    for (int i=0; i<mRegisters.count(); i++)
    {
      SpiritRegister* reg = mRegisters[i];
      if (reg->getName() == name)
        return reg;
    }
    return NULL;
  }

private:
  QString mName;
  QString mNameAttribute;
  quint32 mWidth;
  SpiritRange* mRange;
  SpiritEnum::SpiritAccess mAccess;
  SpiritEnum::SpiritUsage mUsage;
  QList<SpiritRegister*> mRegisters;
  SpiritBaseAddress* mBaseAddress;
  SpiritBitOffset* mBitOffset;
  SpiritBank* mParentBank;
  SpiritMemoryMap* mParentMemoryMap;
  SpiritEnum::SpiritEndianness mEndian;
};

class SpiritBank : public QObject
{
  Q_OBJECT

public:

  SpiritBank()
  {
    mBaseAddress = NULL;
    mBitOffset = NULL;
    mParentMemoryMap = NULL;
    mParentBank = NULL;
  }

  void setParentBank(SpiritBank* parent) { mParentBank=parent; }
  void setParentMemoryMap(SpiritMemoryMap* parent) { mParentMemoryMap=parent; }
  static SpiritBank* deserialize(XmlErrorHandler* eh, const QDomElement& parent);
  void serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh);

public slots:
  SpiritBank* getParentBank() const { return mParentBank; }
  SpiritMemoryMap* getParentMemoryMap() const { return mParentMemoryMap; }

  void setAccess(SpiritEnum::SpiritAccess newVal) { mAccess=newVal; }
  SpiritEnum::SpiritAccess getAccess() const { return mAccess; }

  void setUsage(SpiritEnum::SpiritUsage newVal) {  mUsage=newVal; }
  SpiritEnum::SpiritUsage getUsage() const { return mUsage; }

  SpiritBaseAddress* getBaseAddress() { return mBaseAddress; }
  SpiritBitOffset* getBitOffset() { return mBitOffset; }

  QList<SpiritAddressBlock*>& getAddressBlocks() { return mAddressBlocks; }
  QList<SpiritBank*>& getBanks() { return mBanks; }

  void setName(const QString& newVal) { mName=newVal; }
  QString getName() const { return mName; }

  void setAlignment(SpiritEnum::SpiritAlignment newVal) { mAlignment=newVal; }
  SpiritEnum::SpiritAlignment getAlignment() const { return mAlignment; }

  static SpiritEnum::SpiritAlignment stringToAlignment(const QString& newVal)
  {
    QString value = newVal.toLower();
    if ("parallel" == value)
      return SpiritEnum::Parallel;
    return SpiritEnum::Serial;
  }

  static QString alignmentToString(SpiritEnum::SpiritAlignment v)
  {
    switch (v)
    {
    case SpiritEnum::Parallel:
      return "parallel";
      break;
    case SpiritEnum::Serial:
      return "serial";
      break;
    }
    return "parallel";
  }

private:
  QString mName;
  SpiritEnum::SpiritAlignment mAlignment;
  SpiritBaseAddress* mBaseAddress;
  QList<SpiritAddressBlock*> mAddressBlocks;
  SpiritBitOffset* mBitOffset;
  QList<SpiritBank*> mBanks;
  SpiritEnum::SpiritUsage mUsage;
  SpiritEnum::SpiritAccess mAccess;
  SpiritBank* mParentBank;
  SpiritMemoryMap* mParentMemoryMap;
};

class SpiritMemoryMaps : public QObject
{
  Q_OBJECT

  Q_PROPERTY(quint32 numItems READ numItems)

public slots:
  quint32 numItems() { return mItems.count(); }
  SpiritMemoryMap* getItem(quint32 i) { return mItems[i]; }

public:
  void addItem(SpiritMemoryMap* item) { mItems.push_back(item); }
  bool deserialize(XmlErrorHandler* eh, SpiritXML*, const QDomElement& parent);
  void serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh);

private:
  QList<SpiritMemoryMap*> mItems;
};


class SpiritBusType : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString vendor READ getVendor WRITE setVendor)
  Q_PROPERTY(QString library READ getLibrary WRITE setLibrary)
  Q_PROPERTY(QString name READ getName WRITE setName)
  Q_PROPERTY(QString version READ getVersion WRITE setVersion)

public:
  void deserialize(XmlErrorHandler* eh, const QDomElement& parent);
  void serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh);

public slots:
  QString getVendor() const { return mVendor; }
  void setVendor(const QString& newVal) { mVendor=newVal; }

  QString getLibrary() const { return mLibrary; }
  void setLibrary(const QString& newVal) { mLibrary=newVal; }

  QString getName() const { return mName; }
  void setName(const QString& newVal) { mName=newVal; }
  
  QString getVersion() const { return mVersion; }
  void setVersion(const QString& newVal) { mVersion=newVal; }

private:
  QString mVendor;
  QString mLibrary;
  QString mName;
  QString mVersion;
};

class SpiritSignalName : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString busSignalName READ getBusSignalName WRITE setBusSignalName);
  Q_PROPERTY(QString componentSignalName READ getComponentSignalName WRITE setComponentSignalName);
  Q_PROPERTY(QString left READ getLeft WRITE setLeft);
  Q_PROPERTY(QString right READ getRight WRITE setRight);

public:
  void deserialize(XmlErrorHandler* eh, const QDomElement& parent);

  SpiritSignalName() {}

public slots:
  QString getComponentSignalName() const { return mComponentSignalName; }
  void setComponentSignalName(const QString& newVal) { mComponentSignalName=newVal; }

  QString getBusSignalName() const { return mBusSignalName; }
  void setBusSignalName(const QString& newVal) { mBusSignalName=newVal; }

  QString getLeft() const { return mLeft; }
  void setLeft(const QString& newVal) { mLeft=newVal; }

  QString getRight() const { return mRight; }
  void setRight(const QString& newVal) { mRight=newVal; }

private:
  QString mComponentSignalName;
  QString mBusSignalName;
  QString mLeft;
  QString mRight;
};

// A Spirit signal map
class SpiritSignalMap : public QObject
{
  Q_OBJECT

  Q_PROPERTY(quint32 numSignals READ numSignals)

public:
  void deserialize(XmlErrorHandler* eh, const QDomElement& parent);
  SpiritSignalMap() {}

public slots:
  quint32 numSignals() const { return mSignalNames.count(); }
  SpiritSignalName* getAt(quint32 index)
  {
    return mSignalNames[index];
  }

private:
  QList<SpiritSignalName*> mSignalNames;
};

// A Spirit Bus Interface

class SpiritBusInterface : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString name READ getName)
  Q_PROPERTY(QString vendorExtensions READ getVendorExtensions)
  Q_PROPERTY(SpiritBusType* busType READ getBusType)
  Q_PROPERTY(SpiritSignalMap* signalMap READ getSignalMap)

public:
  SpiritBusInterface()
  {
    mIsMaster = false;
    mIsSlave = false;
    mIsSystem = false;
    mIsMirroredSlave = false;
    mIsMirroredMaster = false;
    mIsMirroredSystem = false;
    mIsExportedInterface = false;
  }

  static SpiritBusInterface* deserialize(XmlErrorHandler* eh, const QDomElement& parent);
  void serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh);

public slots:
  bool isMaster() const { return mIsMaster; }
  bool isSlave() const { return mIsSlave; }
  bool isSystem() const { return mIsSystem; }
  bool isMirroredSlave() const { return mIsMirroredSlave; }
  bool isMirroredMaster() const { return mIsMirroredMaster; }
  bool isMirroredSystem() const { return mIsMirroredSystem; }
  bool isExportedInterface() const { return mIsExportedInterface; }

  QString getName() const { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

  QString getVendorExtensions() const { return mVendorExtensions; }
  void setVendorExtensions(const QString& newVal) { mVendorExtensions=newVal; }

  SpiritBusType* getBusType() { return &mBusType; }
  SpiritSignalMap* getSignalMap() { return &mSignalMap; }

private:
  SpiritSignalMap mSignalMap;
  SpiritBusType mBusType;
  QString mName;
  QString mVendorExtensions;
  bool mIsMaster;
  bool mIsSlave;
  bool mIsSystem;
  bool mIsMirroredSlave;
  bool mIsMirroredMaster;
  bool mIsMirroredSystem;
  bool mIsExportedInterface;
};

class SpiritBusInterfaces : public QObject
{
  Q_OBJECT

  Q_PROPERTY(quint32 numItems READ numItems);

public:
  SpiritBusInterfaces() {}

public slots:
  quint32 numItems() { return mItems.count(); }
  SpiritBusInterface* getAt(quint32 i) { return mItems[i]; }

  QString getVendorExtensions() const { return mVendorExtensions; }
  void setVendorExtensions(const QString& newVal) { mVendorExtensions=newVal; }

public:
  void addItem(SpiritBusInterface* item) { mItems.push_back(item); }
  bool deserialize(XmlErrorHandler* eh, SpiritXML*, const QDomElement& parent);
  void serialize(xmlTextWriterPtr writer, XmlErrorHandler* eh);

private:
  QString mVendorExtensions;
  QList<SpiritBusInterface*> mItems;
};

class SpiritFlags : public QObject
{
  Q_OBJECT

  Q_PROPERTY(SpiritEnum::SpiritResolve resolve READ getResolve)
  Q_PROPERTY(QString id READ getId)
  Q_PROPERTY(QString minimum READ getMininum)
  Q_PROPERTY(QString maximum READ getMaximum)
  Q_PROPERTY(QString rangeType READ getRangeType)
  Q_PROPERTY(QString dependency READ getDependency)
  Q_PROPERTY(QString value READ getValue)
  
public:
  bool deserialize(XmlErrorHandler* eh, SpiritXML*, const QDomElement& parent);

public slots:
  QString getDependency() const { return mDependency; }
  SpiritEnum::SpiritResolve getResolve() const { return mResolve; }
  QString getId() const { return mID; }
  QString getMininum() const { return mMinimum; }
  QString getMaximum() const { return mMaximum; }
  QString getRangeType() const { return mRangeType; }
  QString getValue() const { return mValue; }

private:
  QString mValue;
  QString mID;
  QString mDependency;
  QString mMinimum;
  QString mMaximum;
  QString mRangeType;
  SpiritEnum::SpiritResolve mResolve;
};

class SpiritDefaultFileBuilder : public QObject
{
  Q_OBJECT
  
  Q_PROPERTY(QString fileType READ getFileType)
  Q_PROPERTY(SpiritFlags* flags READ getFlags)

public:
  bool deserialize(XmlErrorHandler* eh, SpiritXML*, const QDomElement& parent);

public slots:
  QString getFileType() const { return mFileType; }
  SpiritFlags* getFlags() { return &mFlags; }

private:
  SpiritFlags mFlags;
  QString mFileType;
};

class SpiritView : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString name READ getName)
  Q_PROPERTY(QString envIdentifier READ getEnvIdentifier)
  Q_PROPERTY(QString language READ getLanguage)
  Q_PROPERTY(QString modelName READ getModelName)
  Q_PROPERTY(QString fileSetRef READ getFileSetRef)
  Q_PROPERTY(SpiritDefaultFileBuilder* defaultFileBuilder READ getDefaultFileBuilder)

public:
  bool deserialize(XmlErrorHandler* eh, SpiritXML*, const QDomElement& parent);

public slots:
  QString getName() const { return mName; }
  QString getEnvIdentifier() const { return mEnvIdentifier; }
  QString getLanguage() const { return mLanguage; }
  QString getModelName() const { return mModelName; }
  QString getFileSetRef() const { return mFileSetRef; }
  SpiritDefaultFileBuilder* getDefaultFileBuilder() { return &mFileBuilder; }

private:
  QString mName;
  QString mEnvIdentifier;
  QString mLanguage;
  QString mModelName;
  QString mFileSetRef;

  SpiritDefaultFileBuilder mFileBuilder;
};

class SpiritExport : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString id READ getID WRITE setID);
  Q_PROPERTY(bool value READ getValue WRITE setValue);
  Q_PROPERTY(QString prompt READ getPrompt WRITE setPrompt);
  Q_PROPERTY(QString configGroups READ getConfigGroups WRITE setConfigGroups);
  Q_PROPERTY(SpiritEnum::SpiritResolve resolve READ getResolve WRITE setResolve);

public:
  bool deserialize(XmlErrorHandler* eh, SpiritXML*, const QDomElement& parent);

public slots:
  bool getValue() const { return mValue; }
  void setValue(bool newVal) { mValue=newVal; }

  QString getID() const { return mID; }
  void setID(const QString& newVal) { mID=newVal; }

  QString getPrompt() const { return mPrompt; }
  void setPrompt(const QString& newVal) { mPrompt=newVal; }

  QString getConfigGroups() const { return mConfigGroups; }
  void setConfigGroups(const QString& newVal) { mConfigGroups=newVal; }

  SpiritEnum::SpiritResolve getResolve() const { return mResolve; }
  void setResolve(SpiritEnum::SpiritResolve newVal) { mResolve=newVal; }

private:
  bool mValue;
  QString mPrompt;
  QString mID;
  QString mConfigGroups;
  SpiritEnum::SpiritResolve mResolve;
};

class SpiritSignal : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QString name READ getName WRITE setName);
  Q_PROPERTY(SpiritEnum::SpiritDirection direction READ getDirection WRITE setDirection);
  Q_PROPERTY(QString defaultValue READ getDefaultValue WRITE setDefaultValue);
  Q_PROPERTY(QString left READ getLeft WRITE setLeft);
  Q_PROPERTY(QString right READ getRight WRITE setRight);
  Q_PROPERTY(SpiritExport* signalExport READ getExport);

public:
  bool deserialize(XmlErrorHandler* eh, SpiritXML*, const QDomElement& parent);

public slots:
  QString getName() const { return mName; }
  void setName(const QString& newVal) { mName=newVal; }

  SpiritEnum::SpiritDirection getDirection() const { return mDirection; }
  void setDirection(SpiritEnum::SpiritDirection newVal) { mDirection=newVal; }
  
  QString getLeft() const { return mLeft; }
  void setLeft(const QString& newVal) { mLeft=newVal; }

  QString getRight() const { return mRight; }
  void setRight(const QString& newVal) { mRight=newVal; }
  
  QString getDefaultValue() const { return mDefaultValue; }
  void setDefaultValue(const QString& newVal) { mDefaultValue=newVal; }

  SpiritExport* getExport() { return &mExport; }

private:
  SpiritEnum::SpiritDirection mDirection;
  QString mDefaultValue;
  QString mName;
  QString mLeft;
  QString mRight;
  SpiritExport mExport;
};


class SpiritSignals : public QObject
{
  Q_OBJECT

  Q_PROPERTY(quint32 numSignals READ numSignals)

public slots:
  quint32 numSignals() const { return mSignalList.count(); }
  SpiritSignal* getAt(quint32 i) { return mSignalList[i]; }
  SpiritSignal* findSignal(const QString& signalName)
  {
    return mSignalMap[signalName];
  }

public:
  bool deserialize(XmlErrorHandler* eh, SpiritXML*, const QDomElement& parent);

private:
  QList<SpiritSignal*> mSignalList;
  QMap<QString, SpiritSignal*> mSignalMap;
};

class SpiritViews : public QObject
{
  Q_OBJECT

  Q_PROPERTY(quint32 numViews READ numViews)

public slots:
  quint32 numViews() const { return mViewList.count(); }
  SpiritView* getAt(quint32 i) { return mViewList[i]; }

public:
  bool deserialize(XmlErrorHandler* eh, SpiritXML*, const QDomElement& parent);

private:
  QList<SpiritView*> mViewList;
};

class SpiritModel : public QObject
{
  Q_OBJECT

  Q_PROPERTY(SpiritViews* views READ getViews)
  Q_PROPERTY(SpiritSignals* signals READ getSignals)

public slots:
  SpiritViews* getViews() { return &mViews; }
  SpiritSignals* getSignals() { return &mSignals; }

public:
  bool deserialize(XmlErrorHandler* eh, SpiritXML*, const QDomElement& parent);

private:
  SpiritViews mViews;
  SpiritSignals mSignals;
};


class SpiritFile : public QObject
{
 Q_OBJECT

 Q_PROPERTY(QString fileName READ getFileName)
 Q_PROPERTY(QString fileType READ getFileType)

public:
  bool deserialize(XmlErrorHandler* eh, SpiritXML*, const QDomElement& parent);

public slots:
  QString getFileName() const { return mFileName; }
  QString getFileType() const { return mFileType; }

private:
  QString mFileName;
  QString mFileType;
};

class SpiritFileSet : public QObject
{
 Q_OBJECT

 Q_PROPERTY(quint32 numFiles READ numFiles) 
 Q_PROPERTY(QString fileSetId READ getFileSetId)

public:
  bool deserialize(XmlErrorHandler* eh, SpiritXML*, const QDomElement& parent);

public slots:
  quint32 numFiles() const { return mFiles.count(); }
  SpiritFile* getAt(quint32 i) { return mFiles[i]; }
  QString getFileSetId() const { return mFileSetId; }

private:
  QString mFileSetId;
  QList<SpiritFile*> mFiles;
};

class SpiritFileSets : public QObject
{
  Q_OBJECT
  
  Q_PROPERTY(quint32 numFileSets READ numFileSets)

public:
  bool deserialize(XmlErrorHandler* eh, SpiritXML*, const QDomElement& parent);

public slots:
  quint32 numFileSets() const { return mFileSets.count(); }
  SpiritFileSet* getAt(quint32 i) { return mFileSets[i]; }
  SpiritFileSet* findFileSet(const QString& fileSetId)
  {
    foreach (SpiritFileSet* fs, mFileSets)
    {
      if (fs->getFileSetId() == fileSetId)
        return fs;
    }
    return NULL;
  }

private:
  QList<SpiritFileSet*> mFileSets;
};

class SpiritXML : public QObject, public QScriptable
{
  Q_OBJECT

  Q_PROPERTY(SpiritBusInterfaces* busInterfaces READ getBusInterfaces)
  Q_PROPERTY(SpiritMemoryMaps* memoryMaps READ getMemoryMaps)
  Q_PROPERTY(SpiritFileSets* fileSets READ getFileSets)
  Q_PROPERTY(SpiritModel* model READ getModel)
  Q_PROPERTY(QString vendor READ getVendor);
  Q_PROPERTY(QString library READ getLibrary);
  Q_PROPERTY(QString name READ getName);
  Q_PROPERTY(QString version READ getVersion);

public:
  SpiritXML(QObject* parent=0) : QObject(parent)
  {
  }
  SpiritXML(const QString& xmlFile);

  XmlErrorHandler* parseSpirtXML(const QString& xmlFile, bool scanForComponent = false);
  bool serialize(xmlTextWriterPtr, XmlErrorHandler*);

  static void registerTypes(QScriptEngine* engine);
  static void registerScriptTypes(QMap<QString, const QMetaObject*>& map);
  static void registerIntellisense(JavaScriptAPIs* apis);

 public slots:
  QString getVendor() const { return mVendor; }
  QString getLibrary() const { return mLibrary; }
  QString getName() const { return mName; }
  QString getVersion() const { return mVersion; }

  SpiritModel* getModel() { return &mModel; }
  SpiritFileSets* getFileSets() { return &mFileSets; }
  SpiritBusInterfaces* getBusInterfaces() { return &mBusInterfaces; }
  SpiritMemoryMaps* getMemoryMaps() { return &mMemoryMaps; }
  SpiritAddressBlock* findAddressBlock(const QString& path);
  SpiritAddressBlock* findBankAddressBlock(SpiritBank* bank, const QString& path);

  void populateTree(CarbonProjectWidget* pw, MemoryMapsItem* parent);

private:
  SpiritModel mModel;
  SpiritMemoryMaps mMemoryMaps;
  SpiritBusInterfaces mBusInterfaces;
  SpiritFileSets mFileSets;

  QString mVendor;
  QString mLibrary;
  QString mName;
  QString mVersion;

  XmlErrorHandler mParseEH;
};

// These are used in registerTypes
Q_DECLARE_METATYPE(SpiritXML*);
Q_DECLARE_METATYPE(SpiritRange*);
Q_DECLARE_METATYPE(SpiritReset*);
Q_DECLARE_METATYPE(SpiritBitOffset*);
Q_DECLARE_METATYPE(SpiritField*);
Q_DECLARE_METATYPE(SpiritRegister*);
Q_DECLARE_METATYPE(SpiritBank*);
Q_DECLARE_METATYPE(SpiritAddressBlock*);
Q_DECLARE_METATYPE(SpiritBaseAddress*);
Q_DECLARE_METATYPE(SpiritBusType*);
Q_DECLARE_METATYPE(SpiritMemoryMap*);
Q_DECLARE_METATYPE(SpiritMemoryMaps*);
Q_DECLARE_METATYPE(SpiritBusInterface*);
Q_DECLARE_METATYPE(SpiritBusInterfaces*);
Q_DECLARE_METATYPE(SpiritSignalMap*);
Q_DECLARE_METATYPE(SpiritSignalName*);
Q_DECLARE_METATYPE(SpiritModel*);
Q_DECLARE_METATYPE(SpiritView*);
Q_DECLARE_METATYPE(SpiritViews*);
Q_DECLARE_METATYPE(SpiritDefaultFileBuilder*);
Q_DECLARE_METATYPE(SpiritFile*);
Q_DECLARE_METATYPE(SpiritFlags*);
Q_DECLARE_METATYPE(SpiritFileSet*);
Q_DECLARE_METATYPE(SpiritFileSets*);
Q_DECLARE_METATYPE(SpiritSignals*);
Q_DECLARE_METATYPE(SpiritSignal*);
Q_DECLARE_METATYPE(SpiritExport*);
Q_DECLARE_METATYPE(SpiritValues*);
Q_DECLARE_METATYPE(SpiritValue*);

// Enums
Q_DECLARE_METATYPE(SpiritEnum::SpiritAlignment);
Q_DECLARE_METATYPE(SpiritEnum::SpiritFormat);
Q_DECLARE_METATYPE(SpiritEnum::SpiritResolve);
Q_DECLARE_METATYPE(SpiritEnum::SpiritAccess);
Q_DECLARE_METATYPE(SpiritEnum::SpiritUsage);
Q_DECLARE_METATYPE(SpiritEnum::SpiritEndianness);
Q_DECLARE_METATYPE(SpiritEnum::SpiritLocation);
Q_DECLARE_METATYPE(SpiritEnum::SpiritDirection);


#endif
