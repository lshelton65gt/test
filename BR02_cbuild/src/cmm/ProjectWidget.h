#ifndef PROJECTWIDGET_H
#define PROJECTWIDGET_H
#include "util/CarbonPlatform.h"

#include <QWidget>
#include "ui_ProjectWidget.h"

class CarbonMakerContext;
class CarbonProjectWidget;

class ProjectWidget : public QWidget
{
  Q_OBJECT

public:
  ProjectWidget(QWidget *parent = 0, CarbonMakerContext* ctx=0);
  ~ProjectWidget();
  void setContext(CarbonMakerContext* ctx);
  CarbonProjectWidget* getCarbonProjectWidget() { return ui.treeWidget; }
  CarbonMakerContext* getContext() { return mContext; }

private slots:
        void on_pushButtonMoveDown_clicked();
        void on_pushButtonMoveUp_clicked();
        void on_pushButtonDelete_clicked();
  void itemSelectionChanged();

private:
  Ui::ProjectWidgetClass ui;
  CarbonMakerContext* mContext;
};

#endif // PROJECTWIDGET_H
