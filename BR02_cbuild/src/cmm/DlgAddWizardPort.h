#ifndef DLGADDWIZARDPORT_H
#define DLGADDWIZARDPORT_H
#include "util/CarbonVersion.h"
#include "gui/CQt.h"


#include <QDialog>
#include "ui_DlgAddWizardPort.h"

class DlgAddWizardPort : public QDialog
{
  Q_OBJECT

public:
  DlgAddWizardPort(QWidget *parent = 0);
  ~DlgAddWizardPort();

  enum PortDirection { InputPort, OutputPort } ;

  PortDirection getDirection() { return mDirection; }
  QString getPortName() { return mPortName; }
  QString getTieValue() { return mTieValue; }
  int getWidth() { return mWidth; }

private slots:
  void on_radioButtonInput_clicked();
  void on_radioButtonOutput_clicked();
  void on_buttonBox_accepted();
  void on_buttonBox_rejected();

private:
  QString mPortName;
  QString mTieValue;
  PortDirection mDirection;
  int mWidth;

  Ui::DlgAddWizardPortClass ui;
};

#endif // DLGADDWIZARDPORT_H
