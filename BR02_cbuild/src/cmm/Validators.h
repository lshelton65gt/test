#ifndef __VALIDATORS_H__
#define __VALIDATORS_H__

#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"
#include "util/UtHashSet.h"

#include "util/CExprParse.h"

#include "CompWizardPortEditor.h"
#include "CompWizardTreeNodes.h"
#include "PortEditorCommands.h"

#include <QtGui>


class RTLPathValidator : public QValidator
{
public:


};

class CExpressionValidator : public QValidator
{
public:
  CExpressionValidator(const QString& value, QLineEdit* parent);
  virtual void fixup(QString& input) const;
  virtual State validate(QString& input, int& pos) const; 
  static bool isValid(const QString& input);

protected:
  QString mValue;
  QLineEdit* mLineEdit;
};

class ESLMemoryBlockValidator : public CExpressionValidator
{
public:
  ESLMemoryBlockValidator(CarbonCfgMemory* mem, const QString& value, QLineEdit* parent) : CExpressionValidator(value, parent)
  {
    mMemory = mem;
  }

  virtual State validate(QString& input, int& pos) const
  {
    if (input == mValue)
      return QValidator::Acceptable;

    State state = CExpressionValidator::validate(input, pos);
    if (state == QValidator::Acceptable)
      state = checkDuplicates(input);
    return state;
  }

  State checkDuplicates(const QString& input) const
  {  
    State result = QValidator::Acceptable;

    for(quint32 i=0; i<mMemory->NumMemoryBlocks(); i++)
    {
      CarbonCfgMemoryBlock* block = mMemory->GetMemoryBlock(i);
      if (block->GetName() == input)
      {
        result = QValidator::Invalid;
        break;
      }
    }
   
    if (result == QValidator::Acceptable)
    {
      mLineEdit->setToolTip("");
      QPalette palette = mLineEdit->palette();
      palette.setColor(QPalette::Text, mLineEdit->foregroundRole());
      mLineEdit->setPalette(palette);  
    }
    else
    {
      mLineEdit->setToolTip("Duplicates not allowed");
      QPalette palette = mLineEdit->palette();
      palette.setColor(QPalette::Text, Qt::red);
      mLineEdit->setPalette(palette);
    }

    return result;
  }
private:
  CarbonCfgMemory* mMemory;
};


class ESLMemoryValidator : public CExpressionValidator
{
public:
  ESLMemoryValidator(CarbonCfgMemory* mem, const QString& value, QLineEdit* parent) : CExpressionValidator(value, parent)
  {
    mMemory = mem;
  }

  virtual State validate(QString& input, int& pos) const
  {
    if (input == mValue)
      return QValidator::Acceptable;

    State state = CExpressionValidator::validate(input, pos);
    if (state == QValidator::Acceptable)
      state = checkDuplicates(input);
    return state;
  }

  State checkDuplicates(const QString& input) const
  {  
    State result = QValidator::Acceptable;
   
    CarbonCfg* cfg = mMemory->getParent();

    for(quint32 i=0; i<cfg->NumMemories(); i++)
    {
      CarbonCfgMemory* mem = cfg->GetMemory(i);
      if (mem != mMemory && mMemory->GetName() == input)     
      {
        result = QValidator::Invalid;
        break;
      }
    }
   
    if (result == QValidator::Acceptable)
    {
      mLineEdit->setToolTip("");
      QPalette palette = mLineEdit->palette();
      palette.setColor(QPalette::Text, mLineEdit->foregroundRole());
      mLineEdit->setPalette(palette);  
    }
    else
    {
      mLineEdit->setToolTip("Duplicates not allowed");
      QPalette palette = mLineEdit->palette();
      palette.setColor(QPalette::Text, Qt::red);
      mLineEdit->setPalette(palette);
    }

    return result;
  }
private:
  CarbonCfgMemory* mMemory;
};


class ESLComponentValidator : public CExpressionValidator
{
public:
  ESLComponentValidator(CarbonCfg* cfg, const QString& value, QLineEdit* parent) : CExpressionValidator(value, parent)
  {
    mCfg = cfg;
  }

  virtual State validate(QString& input, int& pos) const
  {
    if (input == mValue)
      return QValidator::Acceptable;

    State state = CExpressionValidator::validate(input, pos);
    if (state == QValidator::Acceptable)
      state = checkDuplicates(input);
    return state;
  }

  State checkDuplicates(const QString& input) const
  {  
    State result = QValidator::Acceptable;
   
    for(quint32 i=0; i<mCfg->numSubComponents(); i++)
    {
      CarbonCfg* subComp = mCfg->GetSubComponent(i);
      if (subComp->GetCompName() == input)
      {
        result = QValidator::Invalid;
        break;
      }
    }
   
    if (result == QValidator::Acceptable)
    {
      mLineEdit->setToolTip("");
      QPalette palette = mLineEdit->palette();
      palette.setColor(QPalette::Text, mLineEdit->foregroundRole());
      mLineEdit->setPalette(palette);  
    }
    else
    {
      mLineEdit->setToolTip("Duplicates not allowed");
      QPalette palette = mLineEdit->palette();
      palette.setColor(QPalette::Text, Qt::red);
      mLineEdit->setPalette(palette);
    }

    return result;
  }
private:
  CarbonCfg* mCfg;
};

class ESLPortExprValidator : public QValidator
{
public:
  ESLPortExprValidator(QLineEdit* parent, const QString& identifier) : QValidator(parent)
  {
    mLineEdit = parent;
    mIdentifier = identifier;
  }
  virtual void fixup(QString& input) const;
  virtual State validate(QString& input, int& pos) const;

private:
  QLineEdit* mLineEdit;
  QString mIdentifier;
};

class HexInputValidator64 : public QValidator
{
public:
  HexInputValidator64(QLineEdit* parent);
  virtual void fixup(QString& input) const;
  virtual State validate(QString& input, int& pos) const; 
  static bool isValid(const QString& input);
  static quint64 convert(const QString& input);
  static QString convert(quint64 value) { return QString("0x%1").arg(value); }

private:
  QLineEdit* mLineEdit;
};


class XtorInstanceNameValidator : public QValidator
{
public:
  XtorInstanceNameValidator(const QString& name, QLineEdit* parent, CarbonCfg* cfg);
  virtual void fixup(QString& input) const;
  virtual State validate(QString& input, int& pos) const; 
  static bool isValid(CarbonCfg* cfg, const QString& input);

private:
  CarbonCfg* mCfg;
  QString mName;
  QLineEdit* mLineEdit;
};

class ParameterNameValidator : public QValidator
{
public:
  ParameterNameValidator(QLineEdit* parent, CarbonCfg* cfg);
  virtual void fixup(QString& input) const;
  virtual State validate(QString& input, int& pos) const; 
  static bool isValid(CarbonCfg* cfg, const QString& input);

private:
  CarbonCfg* mCfg;
  QLineEdit* mLineEdit;
};


class HexInputValidator32 : public QValidator
{
public:
  HexInputValidator32(QLineEdit* parent);
  virtual void fixup(QString& input) const;
  virtual State validate(QString& input, int& pos) const; 
  static bool isValid(const QString& input);
  static quint32 convert(const QString& input);
  static QString convert(quint32 value) { return QString("0x%1").arg(value); }

private:
  QLineEdit* mLineEdit;
};


#endif
