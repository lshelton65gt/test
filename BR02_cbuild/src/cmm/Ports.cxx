//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include "Ports.h"
#include "WizardContext.h"


Port::Port(bool userDefined)
{ 
  //qRegisterMetaType<Port::PortLocation>("PortLocation");

  mUserDefined = userDefined;
  mLocation = Port::GraphicsLeft;
  mWidth = -1;
}
// the input value might be func(args)
// and we just want the func part
void Port::setOnCondition(const QString& newVal)
{
  QRegExp exp("(.*)\\(");
  int ix = exp.indexIn(newVal);
  int nc = exp.numCaptures();
  if (ix != -1 && nc == 1)
    mOnCondition = exp.cap(1);
  else
    mOnCondition = newVal;
}

void Port::expandPort(QScriptContext *, QScriptEngine *)
{
  qDebug() << "Expand!";
}

bool Port::evaluateOnCondition(QScriptValue contextObj)
{
  QScriptEngine* engine = contextObj.engine();

  WizardContext* context = dynamic_cast<WizardContext*>(contextObj.toQObject());

  if (context)
  {
    QScriptValueList list;
    list.append(engine->newQObject(context));

    QScriptValue val = context->callScriptFunction(getOnCondition(), list);
    return val.toBoolean();
  }
  return false;
}

bool Port::parseBaseAttributes(Port* p, const QDomElement& element, XmlErrorHandler* eh)
{
  bool errorFlag = false;

  if (!element.hasAttribute("name"))
  {
    errorFlag = true;
    eh->reportProblem(XmlErrorHandler::Error, element, 
       QString("%1 missing 'name' attribute").arg(element.tagName()));
  }

  if (!element.hasAttribute("format"))
  {
    errorFlag = true;
    eh->reportProblem(XmlErrorHandler::Error, element, 
       QString("%1 missing 'format' attribute").arg(element.tagName()));
  }

  if (element.hasAttribute("width"))
  {
    QVariant vw = element.attribute("width");
    p->setWidth(vw.toInt());
  }
  else
    p->setWidth(1);

  if (errorFlag)
    return false;

  // required
  p->setName(element.attribute("name"));
  p->setFormat(element.attribute("format"));

  // optional
  p->setDirection(directionFromString(element.attribute("direction")));
  p->setLocation(locationFromString(element.attribute("location"))); 
  p->setOnCondition(element.attribute("onCondition"));
  p->setDescription(element.attribute("description"));

  return true;
}

bool Port::deserialize(Mode* mode, Template* templ, Block* block, Ports* ports, const QDomElement& e, XmlErrorHandler* eh)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    if ("Port" == element.tagName())
    {
      Port* p = NULL;
      if (element.hasAttribute("name"))
      {
        p = new Port();
        if (!parseBaseAttributes(p, element, eh))
          return false;

        ports->addPort(p);
      }
      else
      {
        eh->reportProblem(XmlErrorHandler::Error, element, "Port missing name attribute");
        return false;
      }
    }
    else if ("OptionalPort" == element.tagName())
    {
      OptionalPort* p = new OptionalPort();

      if (!parseBaseAttributes(p, element, eh))
        return false;
 
      if (!p->deserialize(mode, templ, block, ports, element, eh))
        return false;
      ports->addPort(p);
    }
    else if (element.tagName().length() > 0)
    {
      eh->reportProblem(XmlErrorHandler::Error, element, QString("Unknown Ports Tag: %1").arg(element.tagName()));
      return false;
    }

    n = n.nextSibling();
  }
  return true;
}

// Walk the list, find any userports and remove them
void Ports::removeUserDefined()
{
  QMutableListIterator<Port*> i(mPorts);
  while (i.hasNext())
  {
    Port* port = i.next();
    if (port->isUserDefined())
      i.remove();
  }
}


// Scriptable addPort
QObject* Ports::addPort(const QString& portName, PortLocation location, const QString& format, bool userPort)
{
  Port* port = findPort(portName);
  if (port == NULL)
  {
    port = new Port(userPort);
    port->setName(portName);
    port->setDirection(Port::Input);
    port->setLocation((Port::PortLocation)location);
    port->setFormat(format);
    addPort(port);
    return port;
  }
  else
  {
    context()->throwError(QScriptContext::ReferenceError, QString("Port '%1' already exists").arg(portName));
    return NULL;
  }
}

/*!
 * \brief
 * Write brief comment for addPort here.
 * 
 * \param p
 * Description of parameter p.
 * 
 * \throws <exception class>
 * Description of criteria for throwing this exception.
 * 
 * Write detailed description for addPort here.
 * 
 * \remarks
 * Write remarks for addPort here.
 * 
 * \see
 * Separate items with the '|' character.
 */
void Ports::addPort(Port* p)
{
  mPorts.append(p);
  QVariant v = qVariantFromValue((QObject*)p);
  
  QString name = p->getName().replace(" ", "");
  setProperty(name.toAscii(), v); // this_toascii_ok
}


bool Ports::deserialize(Mode* mode, Template* templ, Block* block, const QDomElement& e, XmlErrorHandler* eh)
{
  QDomNode n = e.firstChild();
  while(!n.isNull())
  {
    QDomElement element = n.toElement(); // try to convert the node to an element.
    if ("Ports" == element.tagName())
    {
      if (!Port::deserialize(mode, templ, block, this, element, eh))
        return false;
    }
    n = n.nextSibling();
  }

  return true;
}

Port::PortDirection Port::directionFromString(const QString& dirStr)
{
  QString str = dirStr.toLower();
  if ("input" == str)
    return Port::Input;
  else if ("output" == str)
    return Port::Output;
  else if ("inout" == str)
    return Port::InOut;

  return Port::Input;
}

Port::PortLocation Port::locationFromString(const QString& locStr)
{
  QString str = locStr.toLower();

  if ("left" == str)
    return Port::GraphicsLeft;
  else if ("right" == str)
    return Port::GraphicsRight;
  else if ("top" == str)
    return Port::GraphicsTop;
  else if ("bottom" == str)
    return Port::GraphicsBottom;

  return Port::GraphicsLeft;
}

Port* Port::copyPortData(Port* p)
{
  setDescription(p->getDescription());
  setName(p->getName());
  setFormat(p->getFormat());
  setOnCondition(p->getOnCondition());
  setBaseName(p->getBaseName());
  setLocation(p->getLocation());
  setDirection(p->getDirection());
  setWidth(p->getWidth());
  setTieValue(p->getTieValue());

  return p;
}
Port* Port::copyPort()
{
  Port* port = new Port();
  port->copyPortData(this);
  
  return port;
}

