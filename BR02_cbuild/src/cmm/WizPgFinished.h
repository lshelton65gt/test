#ifndef WIZPGFINISHED_H
#define WIZPGFINISHED_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QWizardPage>
#include "WizardPage.h"
#include "ui_WizPgFinished.h"

class WizPgFinished : public WizardPage
{
  Q_OBJECT

public:
  WizPgFinished(QWidget *parent = 0);
  ~WizPgFinished();

  virtual void initializePage();
  virtual bool validatePage();
  
private slots:
  void on_checkBoxSubstituteModule_stateChanged(int);

private:
  void serializePages(const QString& outputName);
  virtual bool serialize(QDomDocument& doc, QDomElement& parent, XmlErrorHandler*);
  virtual bool isFinalPage () const 
  {
    return true; 
  }
  virtual int nextId() const
  {
    return -1; 
  }

private:
  bool checkArguments();
  void updateControls();

private:
  Ui::WizPgFinishedClass ui;
  QString mFileName;
};

#endif // WIZPGFINISHED_H
