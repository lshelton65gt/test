//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "Scripting.h"
#include "JavaScriptAPIs.h"

void XmlErrorHandler::reportProblem(XmlErrorType errType, const QDomElement& e, const QString& msg)
{
  switch (errType)
  {
  case Warning:
    mWarnings++;
    mMessages.append(
      QString("Warning: Line: %1: %2")
      .arg(e.lineNumber())
      .arg(msg)
      );
    break;
  case Error:
    mMessages.append(
      QString("Warning: Line: %1: %2")
      .arg(e.lineNumber())
      .arg(msg)
      );
    mErrors++;
    break;
  }
}

QString DocumentedQObject::methodLink(const QString& name) const
{
  return makeLink(name, "method");
}

QString DocumentedQObject::makeLink(const QString& name, const QString& linkType) const
{
  QString result;
  foreach (QString mn, name.split(","))
  {
    if (!result.isEmpty())
      result += ", ";

    QString n = mn.trimmed();
    QStringList typeParts = n.split("::");
    
    if (typeParts.count() > 1)
    {
      QString className = typeParts[0];
      QString typeClassName = className;
      const QMetaObject* typeMo = JavaScriptAPIs::lookupType(className);
      if (typeMo)
      {
        typeClassName = JavaScriptAPIs::getClassInfo(typeMo, "ClassName");
        if (typeClassName.isEmpty())
          typeClassName = typeMo->className();
      }
      result += QString("<a href=\"%2.html#%1-%3\">%1</a>").arg(n).arg(typeClassName).arg(linkType);
    }
    else
      result += QString("<a href=\"#%1-%2\">%1</a>").arg(n).arg(linkType);
  }
  return result;
}

QString DocumentedQObject::propertyLink(const QString& name) const
{
  return makeLink(name, "prop");
}

QString DocumentedQObject::enumLink(const QString& name) const
{
  return makeLink(name, "enum");
}

QString DocumentedQObject::signalLink(const QString& name) const
{
  return makeLink(name, "signal");
}
