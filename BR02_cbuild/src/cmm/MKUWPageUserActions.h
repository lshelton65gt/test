#ifndef MKUWPAGEUSERACTIONS_H
#define MKUWPAGEUSERACTIONS_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>
#include "WizardPage.h"
#include <QWizardPage>
#include "ui_MKUWPageUserActions.h"
#include "ModelKitUserWizard.h"
#include "KitManifest.h"
#include "cmm.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"

class CarbonProjectWidget;
class CarbonProject;

class UserActionUI
{
public:
  UserActionUI() 
  {
    mPicture = NULL;
    mText = NULL;
    mLayout = NULL;
  }

  static UserActionUI* createAction(int n, QWidget* parent, QVBoxLayout* parentLayout, KitUserAction* action);

  void setComplete(bool value);
  QHBoxLayout* getLayout() const { return mLayout; }

private:
  QLabel* mPicture;
  QLabel* mText;
  QHBoxLayout* mLayout;
};

class MKUWPageUserActions : public WizardPage
{
  Q_OBJECT

public:
  MKUWPageUserActions(QWidget *parent = 0);
  ~MKUWPageUserActions();

protected:
  virtual void initializePage();
  virtual bool validatePage();
  virtual bool isComplete() const;
  virtual int nextId() const;

private slots:
  void setStatusMessage(const QString& msg);
  void setProgress(int value);
  void setProgressRange(int low, int high);
  void stdoutChanged(CarbonConsole::CommandType, const QString&, const QString&);

private:
  void addBuiltinVariables(KitUserContext* userContext);

private:
  bool mIsValid;
  int mStepNumber;
  QMap<KitUserAction*, UserActionUI*> mActionMap;
  bool mInitialized;

  CarbonProjectWidget* mProjectWidget;
  CarbonProject* mProject;


private:
  Ui::MKUWPageUserActionsClass ui;
};

#endif // MKUWPAGEUSERACTIONS_H
