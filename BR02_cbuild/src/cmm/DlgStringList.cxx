//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QDebug>
#include "DlgStringList.h"

#include "gui/CQt.h"
#include "util/UtString.h"

#include "CarbonProject.h"
#include "CarbonProjectWidget.h"

#define PROP_CARBON_EDIT "CarbonEditBox"
#define PROP_CARBON_FILTER "CarbonFileFilter"
#define PROP_CARBON_FILE "CarbonEditFileName"
#define PROP_CARBON_ITEM "CarbonFileWidget"
#define PROP_CARBON_SWITCHVALUE "CarbonPropertyValue"
#define ASSOCIATED_EDITOR "AssociatedEditor"

DlgStringList::DlgStringList(QWidget *parent, UtStringArray* inputItems, bool isFileList, CarbonProject* proj, const char* componentDir)
: QDialog(parent)
{
  ui.setupUi(this);

  mProject = proj;
  mFileList = isFileList;
  
  if (componentDir)
    mComponentDir = componentDir;

  if (mFileList)
    ui.listWidget->setItemDelegate(new DlgStringListDelegate(ui.listWidget,this));

  if (inputItems)
  {
    for (UInt32 i=0; i<inputItems->size(); i++)
    {
      const char* item = (*inputItems)[i];

      QListWidgetItem* newItem = new QListWidgetItem();
      newItem->setText(item);
      newItem->setFlags(newItem->flags() | Qt::ItemIsEditable);
      ui.listWidget->addItem(newItem);
    }
  }

  // Update the buttons
  selectionChanged();

  connect(ui.listWidget, SIGNAL(itemSelectionChanged()), this, SLOT(selectionChanged()));
}

void DlgStringList::selectionChanged()
{
  if (ui.listWidget->selectedItems().count() > 0)
  {
    ui.pushButtonDeleteItem->setEnabled(true);
    int nItems = ui.listWidget->count();

    foreach (QListWidgetItem* item, ui.listWidget->selectedItems())
    {
      int row = ui.listWidget->row(item);

      if (mFileList)
        ui.listWidget->editItem(item);

      ui.pushButtonMoveUp->setEnabled(row > 0);
      ui.pushButtonMoveDown->setEnabled(row < nItems-1);
      break;
    }
  }
  else
  {
    ui.pushButtonMoveUp->setEnabled(false);
    ui.pushButtonMoveDown->setEnabled(false);
    ui.pushButtonDeleteItem->setEnabled(false);
  }
}

DlgStringList::~DlgStringList()
{
}

void DlgStringList::on_pushButtonNewItem_clicked()
{
  if (mFileList)
  {
    QString f;
    UtString fname;

    UtString currValue;
    currValue << mProject->getProjectDirectory();

    if (getFileKind() == CarbonPropertyFilelist::InputFile)
    {
      f = QFileDialog::getOpenFileName(this, tr("Input File"), currValue.c_str(), mFilter.c_str());
      if (f.length() == 0)
        return;
    }
    else if (getFileKind() == CarbonPropertyFilelist::OutputFile)
    {
      f = QFileDialog::getSaveFileName(this, tr("Output File"), currValue.c_str(), mFilter.c_str());
      if (f.length() == 0)
        return;
    }
    else if (getFileKind() == CarbonPropertyFilelist::InputDirectories)
    {
      f = QFileDialog::getExistingDirectory(this, tr("Directory"), currValue.c_str());
      if (f.length() == 0)
        return;
    }

    fname << f;

    QFileInfo fi(f);
    UtString fileName;

    if (fi.isRelative())
      OSConstructFilePath(&fileName, mProject->getProjectDirectory(), fname.c_str());
    else
      fileName << fi.absoluteFilePath();

    const char* baseDir = mProject->getProjectDirectory();
    UtString tmp;
    if (mComponentDir.length() > 0)
    {
      tmp << mProject->getActive()->getOutputDirectory() << "/" << mComponentDir;
      baseDir = tmp.c_str();
    }

    UtString relFile;
    relFile << CarbonProjectWidget::makeRelativePath(baseDir, fileName.c_str());
    ui.listWidget->addNewItem(relFile.c_str());
  }
  else
    ui.listWidget->addNewItem();
}

void DlgStringList::on_pushButtonDeleteItem_clicked()
{
  ui.listWidget->deleteItem();
}

void DlgStringList::on_buttonBox_accepted()
{
  for (int row=0; row<ui.listWidget->count(); ++row)
  {
    QListWidgetItem* item = ui.listWidget->item(row);
    UtString txt;
    txt << item->text();
    if (txt.length() > 0)
      mItems.push_back(txt);
  }
  accept();
}

void DlgStringList::on_buttonBox_rejected()
{
  reject();
}

void DlgStringList::on_pushButtonMoveUp_clicked()
{
  ui.listWidget->moveUp();
}

void DlgStringList::on_pushButtonMoveDown_clicked()
{
  ui.listWidget->moveDown();
}


// Delegate

// Editor Delegate
DlgStringListDelegate::DlgStringListDelegate(QObject *parent, DlgStringList* stringlist)
: QItemDelegate(parent), sl(stringlist)
{
}

QWidget *DlgStringListDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem&,
                                          const QModelIndex &index) const
{ 
  UtString value;
  QWidget* widget = NULL;

  QListWidgetItem* item = sl->getListWidget()->item(index.row());

  value << item->text();

  QWidget* w = new QWidget(parent);
  QHBoxLayout* layout = new QHBoxLayout();
  QPushButton* btn = new QPushButton("...");

  connect(btn, SIGNAL(clicked(bool)), this, SLOT(showFilePromptDialog()));

  btn->setMaximumWidth(20);
  QLineEdit* edit = new QLineEdit(widget);

  connect(edit, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));

  edit->setText(value.c_str());
  edit->setObjectName(ASSOCIATED_EDITOR);

  QVariant qv = qVariantFromValue((void*)edit);
  btn->setProperty(PROP_CARBON_EDIT, qv);

  QVariant qvw = qVariantFromValue((void*)item);
  btn->setProperty(PROP_CARBON_ITEM, qvw);

  layout->addWidget(edit);
  layout->addWidget(btn);

  layout->setMargin(0);
  layout->setSpacing(0);

  w->setAutoFillBackground(true);
  w->setLayout(layout);

  return w;
}

void DlgStringListDelegate::commitAndCloseEditor()
{
  QLineEdit *editor = qobject_cast<QLineEdit *>(sender());
  if (editor)
  {
    emit commitData(editor);
    emit closeEditor(editor);
  }
}

void DlgStringListDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
  if (edit)
  {
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }
}

void DlgStringListDelegate::showFilePromptDialog()
{
  QPushButton *pb = qobject_cast<QPushButton *>(sender());

  QListWidget* lw = sl->getListWidget();

  QListWidgetItem* item = lw->item(lw->currentRow());
  UtString fileName;
  fileName << item->text();

  UtString filter;
  filter << sl->getFilter();

  // path might be relative to the project directory
  QFileInfo fi(fileName.c_str());
  QString fname;

  if (sl->getFileKind() == CarbonPropertyFilelist::InputFile)
  {
    UtString currValue;

    if (sl->mProject && fi.isRelative())
      OSConstructFilePath(&currValue, sl->mProject->getProjectDirectory(), fileName.c_str());
    else
      currValue << sl->mProject->getProjectDirectory();

    fname = QFileDialog::getOpenFileName(lw, tr("Input File"), currValue.c_str(), filter.c_str());
    if (fname.length() == 0)
      return;
  }
  else if (sl->getFileKind() == CarbonPropertyFilelist::OutputFile)
  {
    UtString currValue;

    if (sl->mProject && fi.isRelative())
      OSConstructFilePath(&currValue, sl->mProject->getProjectDirectory(), fileName.c_str());
    else
      currValue << sl->mProject->getProjectDirectory();

    fname = QFileDialog::getSaveFileName(lw, tr("Output File"), currValue.c_str(), filter.c_str());
    if (fname.length() == 0)
      return;
  }

  QFileInfo nfi(fname);
  UtString tmpFile;
  if (nfi.isRelative())
  {
    UtString t;
    t << fname;
    OSConstructFilePath(&tmpFile, sl->mProject->getProjectDirectory(), t.c_str());
  }
  else
    tmpFile << nfi.absoluteFilePath();

  UtString relFile;
  relFile << CarbonProjectWidget::makeRelativePath(sl->mProject->getProjectDirectory(), tmpFile.c_str());

  QVariant qv = pb->property(PROP_CARBON_EDIT);
  if (qv.isValid())
  {
    QLineEdit* edit = (QLineEdit*)qv.value<void*>();
    edit->setText(relFile.c_str());

    // This code causes the newly entered text to be committed
    // closeEditor() and commitData() didn't work
    QVariant qvi = pb->property(PROP_CARBON_ITEM);
    if (qvi.isValid())
    {
      QListWidgetItem* item = (QListWidgetItem*)qvi.value<void*>();
      item->setText(relFile.c_str());
      sl->getListWidget()->setCurrentItem(item);
    }
  }
}

void DlgStringListDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                      const QModelIndex &index) const
{
  QLineEdit* lineEdit = editor->findChild<QLineEdit*>(ASSOCIATED_EDITOR);
  if (lineEdit != NULL)
  {
    UtString value;
    value << lineEdit->text();
    model->setData(index, value.c_str());
  }
}
