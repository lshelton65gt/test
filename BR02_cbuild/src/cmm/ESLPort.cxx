//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"
#include "util/UtHashSet.h"

#include "util/CExprParse.h"

#include "CompWizardPortEditor.h"
#include "CompWizardTreeNodes.h"
#include "PortEditorCommands.h"
#include "ESLPort.h"
#include "CcfgHelper.h"
#include "cmm.h"
#include "CarbonMakerContext.h"

#include "CarbonProjectWidget.h"

// static initializers
ESLPortActions* ESLPortItem::mActions = NULL;

ESLPortActions::ESLPortActions(PortEditorTreeWidget* tree) : QObject(tree)
{
  mTree = tree;

  mTieToOne = new QAction("Tie High", mTree);
  CQT_CONNECT(mTieToOne, triggered(), this, actionTieToOne());

  mTieToZero = new QAction("Tie Low", mTree);
  CQT_CONNECT(mTieToZero, triggered(), this, actionTieToZero());

  mResetGen = new QAction("Create Reset Generator", mTree);
  CQT_CONNECT(mResetGen, triggered(), this, actionResetGen());

  mClockGen = new QAction("Create Clock Generator", mTree);
  CQT_CONNECT(mClockGen, triggered(), this, actionClockGen());

  mSystemCClockGen = new QAction("Create Clock Generator", mTree);
  CQT_CONNECT(mSystemCClockGen, triggered(), this, actionSystemCClockGen());

  mAddExpr = new QAction("Add Port Expression", mTree);
  CQT_CONNECT(mAddExpr, triggered(), this, actionAddExpression());

  mChangeInput = new QAction(QIcon(":/cmm/Resources/input.png"), "Change to Input", mTree);
  CQT_CONNECT(mChangeInput, triggered(), this, actionChangeInput());

  mChangeOutput = new QAction(QIcon(":/cmm/Resources/output.png"), "Change to Output", mTree);
  CQT_CONNECT(mChangeOutput, triggered(), this, actionChangeOutput());
}

void ESLPortActions::actionChangeInput()
{
  QList<PortEditorTreeItem*> items;
  mTree->fillSelectedItems(items, PortEditorTreeItem::ESLPort);
  QList<ESLPortItem*> changeItems;
  foreach (PortEditorTreeItem* item, items)
  {
    ESLPortItem* eslItem = dynamic_cast<ESLPortItem*>(item);
    CarbonCfgESLPort* eslPort = eslItem->getESLPort();
    if (eslPort->getPortType() != eCarbonCfgESLInput)
      changeItems.append(eslItem);
  }

  if (changeItems.count() > 0)
    mTree->getUndoStack()->push(new ESLPortItem::ChangeDirectionCommand(changeItems, eCarbonCfgESLInput));
}

void ESLPortActions::actionChangeOutput()
{
  QList<PortEditorTreeItem*> items;
  mTree->fillSelectedItems(items, PortEditorTreeItem::ESLPort);
  QList<ESLPortItem*> changeItems;
  foreach (PortEditorTreeItem* item, items)
  {
    ESLPortItem* eslItem = dynamic_cast<ESLPortItem*>(item);
    CarbonCfgESLPort* eslPort = eslItem->getESLPort();
    if (eslPort->getPortType() != eCarbonCfgESLOutput)
      changeItems.append(eslItem);
  }

  if (changeItems.count() > 0)
    mTree->getUndoStack()->push(new ESLPortItem::ChangeDirectionCommand(changeItems, eCarbonCfgESLOutput));
}

void ESLPortActions::actionAddExpression()
{
  qDebug() << "Add ESL Port Expression";
  
  QList<PortEditorTreeItem*> items;
  mTree->fillSelectedItems(items, PortEditorTreeItem::ESLPort);

  if (items.count() > 0)
  {
    ESLPortItem* item = dynamic_cast<ESLPortItem*>(items.first());
    mTree->getUndoStack()->push(new ESLPortItem::AddExpression(item, "0x0"));
  }
}

void ESLPortActions::actionTieToOne()
{
  actionTie(TieToOne);
}
void ESLPortActions::actionTieToZero()
{
  actionTie(TieToZero);
}

void ESLPortActions::actionResetGen()
{
  qDebug() << "ESLPort Create ResetGen";
  QList<ESLPortItem*> items;
  selectedItems(items);
  mTree->getUndoStack()->push(new ESLPortItem::ResetGenCommand(items, 1, 0, 1, 1, 0, 1, 1));
}
void ESLPortActions::actionClockGen()
{
  qDebug() << "ESLPort Create ClockGen";
  QList<ESLPortItem*> items;
  selectedItems(items);
  mTree->getUndoStack()->push(new ESLPortItem::ClockGenCommand(items, 0, 0, 50, 1, 1));
}

void ESLPortActions::actionSystemCClockGen()
{
  qDebug() << "SysttemC Create ClockGen";
  QList<ESLPortItem*> items;
  selectedItems(items);

  mTree->getUndoStack()->push(new ESLPortItem::SystemCClockGenCommand(items, 1, eCarbonCfgScNS, 0.5, 0, eCarbonCfgScNS, 0));
}


void ESLPortActions::selectedItems(QList<ESLPortItem*>& tieItems)
{
  foreach(QTreeWidgetItem* treeItem, mTree->selectedItems())
  {
    PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(treeItem);
    if (item)
    {
      ESLPortItem* portItem = dynamic_cast<ESLPortItem*>(item);
      if (portItem)
        tieItems.append(portItem);
    }
  }
}

void ESLPortActions::actionTie(Action action)
{
  qDebug() << "Tie" << action << mTree->selectedItems().count() << "items";

  QList<ESLPortItem*> tieItems;
  selectedItems(tieItems);
  
  UInt32 tieValue = 0;
  if (action == TieToOne)
    tieValue = 1;

  mTree->getUndoStack()->push(new ESLPortItem::TieCommand(tieItems, tieValue));
}

ESLPortItem::TieCommand::TieCommand(QList<ESLPortItem*> items, UInt32 tieValue, QUndoCommand* parent)
: MultiUndoCommand(parent)
{
  setText(QString("Tie %1 ESL Port(s)").arg(items.count()));

  int numSkipped = 0;
  foreach(ESLPortItem* item, items)
  {
    CarbonCfgESLPort* eslPort = item->getESLPort();
    if (eslPort->getPortType() == eCarbonCfgESLInput)
    {
      ESLPortItem::Tie* tie = new ESLPortItem::Tie(item->getTree(), item);
      tie->mCfg = item->getCcfg();

      // All ones for tie high
      UInt32 tv = 0;
      if (tieValue != 0)
        tv  = 0xffffffff;

      tie->mTieValue = tv;
      tie->mRTLPortName = eslPort->getRTLPort()->getName();
      tie->mESLPortName = eslPort->getName();
      tie->mESLPortExpr = eslPort->getExpr();
      tie->mPortMode = eslPort->getMode();
      tie->mTypeDef = eslPort->getTypeDef();

      addItem(tie);
    }
    else
    {
      numSkipped++;
      QString warningMessage = QString("Ties are not allowed on outputs (%1)").arg(eslPort->getName());
      theApp->getContext()->getCarbonProjectWidget()->getConsole()->appendLine(warningMessage);
    }
  }

  if (numSkipped > 0)
  {
    QString msg = QString("Skipped %1 outputs which cannot be tied").arg(numSkipped);
    theApp->statusBar()->showMessage(msg);
  }
}

// Reset Gen

// Remove the ResetGen and make it back an ESL Port
void ESLPortItem::ResetGen::undo()
{
  qDebug() << "undo resetgen" << mESLPortName;
  PortEditorTreeWidget* tree = getTree();

  PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(tree->itemFromIndex(mResetGenIndex));

  // Remove the ResetGen item
  CarbonCfgRTLPort* rtlPort = ESLResetGenItem::removeResetGen(item, mRTLPortName);

  // Create the ESL Port & restore Expression if there was one
  ESLPortItem* newItem = ESLPortItem::createESLPort(tree, this, rtlPort);  
  if (!mESLPortExpr.isEmpty())
    newItem->setPortExpr(mESLPortExpr);
}

// Remove the ESL Port and make a ResetGen
void ESLPortItem::ResetGen::redo()
{
  qDebug() << "resetGen" << mESLPortName;
  PortEditorTreeWidget* tree = getTree();

  // Remove the ESL Port
  CarbonCfgRTLPort* rtlPort = ESLPortItem::removeESLPort(getItem(), mESLPortName);

  // Create a new ResetGen
  UInt64 activeValue = (1LL<<rtlPort->getWidth())-1;

  ESLResetGenItem* newItem = ESLResetGenItem::createResetGen(tree, this, 
                        rtlPort, 
                        activeValue, mInactiveValue,
                        mClockCycles, mCompCycles,
                        mCyclesBefore, mCyclesAsserted, mCyclesAfter);

  // Save the index of this new item for undo
  mResetGenIndex = getTree()->indexFromItem(newItem);
  getTree()->setModified(true);
}

ESLPortItem::ResetGenCommand::ResetGenCommand(QList<ESLPortItem*> items, 
      UInt64 activeValue, UInt64 inactiveValue,
      UInt32 clockCycles, UInt32 compCycles,
      UInt32 cyclesBefore, UInt32 cyclesAsserted, UInt32 cyclesAfter,
      QUndoCommand* parent)
  : MultiUndoCommand(parent)
{
  setText(QString("ResetGen %1 ESL Port(s)").arg(items.count()));

  int numSkipped = 0;
  foreach(ESLPortItem* item, items)
  {
    CarbonCfgESLPort* eslPort = item->getESLPort();
    if (eslPort->getPortType() == eCarbonCfgESLInput)
    {
      ESLPortItem::ResetGen* resetGen = new ESLPortItem::ResetGen(item->getTree(), item);
      resetGen->mCfg = item->getCcfg();
      resetGen->mRTLPortName = eslPort->getRTLPort()->getName();
      resetGen->mESLPortName = eslPort->getName();
      resetGen->mESLPortExpr = eslPort->getExpr();

      resetGen->mActiveValue = activeValue;
      resetGen->mInactiveValue = inactiveValue;
      resetGen->mClockCycles = clockCycles;
      resetGen->mCompCycles = compCycles;
      resetGen->mCyclesBefore = cyclesBefore;
      resetGen->mCyclesAsserted = cyclesAsserted;
      resetGen->mCyclesAfter = cyclesAfter;

      addItem(resetGen);
    }
    else
    {
      numSkipped++;
      QString warningMessage = QString("Reset Generators are not allowed on outputs (%1)")
        .arg(eslPort->getName());
      theApp->getContext()->getCarbonProjectWidget()->getConsole()->appendLine(warningMessage);
    }
  }

  if (numSkipped > 0)
  {
    QString msg = QString("Skipped %1 outputs which cannot made into Reset Generators").arg(numSkipped);
    theApp->statusBar()->showMessage(msg);
  }
}

//
// Remove from the Tree and Disconnect from the ccfg data model
// 
CarbonCfgRTLPort* ESLPortItem::removeESLPort(PortEditorTreeItem* item, const QString& eslPortName)
{
  CcfgHelper ccfg(item->getCcfg());
  PortEditorTreeWidget* tree = item->getTree();
  tree->removeItem(item);
  CarbonCfgESLPort* eslPort = ccfg.findESLPort(eslPortName);
  CarbonCfgRTLPort* rtlPort = eslPort->getRTLPort();
  ccfg.disconnect(eslPort);
  return rtlPort;
}

// Create an ESL Port and add it to the Data Model and
// the Tree View
ESLPortItem* ESLPortItem::createESLPort(PortEditorTreeWidget* tree,
                        UndoData* undoData, CarbonCfgRTLPort* rtlPort)
{
  CcfgHelper ccfg(tree->getCcfg());

  CarbonDB* db = tree->getCcfg()->getDB();
  const CarbonDBNode* node = carbonDBFindNode(db, rtlPort->getName());
  INFO_ASSERT(node, "Expecting RTL Node");
    
  const char* eslPortName = carbonDBNodeGetLeafName(db, node);

  INFO_ASSERT(ccfg.findESLPort(eslPortName) == NULL, "Not expecting ESL Port");

  CarbonCfgESLPortType portType = CarbonCfg::convertRTLPortType(rtlPort);
  CarbonCfgESLPort* eslPort = tree->getCcfg()->addESLPort(rtlPort,
                eslPortName, portType, "", eCarbonCfgESLPortUndefined);

  computeModeAndType(tree, db, node, eslPort, rtlPort); 

  CompWizardPortEditor* editor = tree->getEditor();

  ESLPortItem* newItem = new ESLPortItem(editor->getPortsRoot(), eslPort);
  
  if (undoData)
    undoData->setTreeData(tree, newItem);  
  
  tree->sortChildren(editor->getPortsRoot());

  newItem->setSelected(true);
  tree->scrollToItem(newItem);

  return newItem;
}

// Create an ESL Port and add it to the Data Model and
// the Tree View
ESLPortItem* ESLPortItem::createESLPort(PortEditorTreeWidget* tree,
                        UndoData* undoData, const QString& rtlPortName)
{
  CcfgHelper ccfg(tree->getCcfg());

  CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(rtlPortName);

  CarbonDB* db = tree->getCcfg()->getDB();
  const CarbonDBNode* node = carbonDBFindNode(db, rtlPort->getName());
  INFO_ASSERT(node, "Expecting RTL Node");
    
  // Convert the RTL to ESL port type
  CarbonCfgESLPortType portType = CarbonCfg::convertRTLPortType(rtlPort);

  const char* eslPortName = carbonDBNodeGetLeafName(db, node);

  INFO_ASSERT(ccfg.findESLPort(eslPortName) == NULL, "Not expecting ESL Port");
 
  CarbonCfgESLPort* eslPort = tree->getCcfg()->addESLPort(rtlPort,
                eslPortName, portType, "", eCarbonCfgESLPortUndefined);

  computeModeAndType(tree, db, node, eslPort, rtlPort); 

  CompWizardPortEditor* editor = tree->getEditor();

  ESLPortItem* newItem = new ESLPortItem(editor->getPortsRoot(), eslPort);
  
  if (undoData)
    undoData->setTreeData(tree, newItem);  
  
  tree->sortChildren(editor->getPortsRoot());

  newItem->setSelected(true);
  tree->scrollToItem(newItem);

  return newItem;
}

void ESLPortItem::Tie::undo()
{
  qDebug() << "undo tie" << mESLPortName << mTieValue;
  PortEditorTreeWidget* tree = getTree();

  PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(tree->itemFromIndex(mESLTieIndex));

  // Remove the Tied item
  CarbonCfgRTLPort* rtlPort = ESLTieItem::removeTie(item, mRTLPortName);

  // Create the ESL Port & restore Expression if there was one
  ESLPortItem* newItem = ESLPortItem::createESLPort(tree, this, rtlPort);  
  if (!mESLPortExpr.isEmpty())
    newItem->setPortExpr(mESLPortExpr);

  newItem->setPortTypeDef(mTypeDef);
  newItem->setPortMode(mPortMode);
}


// Convert an ESL Port to a TIE Port
void ESLPortItem::Tie::redo()
{
  qDebug() << "tie" << mESLPortName << mTieValue;
  PortEditorTreeWidget* tree = getTree();

  // Remove the ESL Port
  CarbonCfgRTLPort* rtlPort = ESLPortItem::removeESLPort(getItem(), mESLPortName);

  // Create a new Tie
  ESLTieItem* newItem = ESLTieItem::createTie(tree, this, rtlPort, mTieValue);

  // Save the index of this new item for undo
  mESLTieIndex = getTree()->indexFromItem(newItem);
  getTree()->setModified(true);
}

QAction* ESLPortActions::getAction(Action kind)
{
  switch (kind)
  {
  case TieToOne:          return mTieToOne; break;
  case TieToZero:         return mTieToZero; break;
  case ResetGen:          return mResetGen; break;
  case ClockGen:          return mClockGen; break;
  case SystemCClockGen:   return mSystemCClockGen; break;
  case AddPortExpression: return mAddExpr; break;
  case ChangeInput:       return mChangeInput; break;
  case ChangeOutput:      return mChangeOutput; break;
  }
  return mTieToZero;
}


void PortEditorTreeItem::init()
{
  mIconBidi = QIcon(":/cmm/Resources/bidi.png");
  mIconOutput = QIcon(":/cmm/Resources/output.png");
  mIconInput = QIcon(":/cmm/Resources/input.png");
}

// Create an ESL Port, only if it's not connected
ESLPortItem* ESLPortItem::makeESLPort(CarbonCfg* cfg, 
                                    CarbonCfgRTLPort* rtlPort, 
                                    PortEditorTreeWidget* tree,
                                    QString& eslPortName,
                                    PortTreeIndex& eslPortIndex)
{
  CarbonDB* db = cfg->getDB();
  const CarbonDBNode* node = carbonDBFindNode(db, rtlPort->getName());
  INFO_ASSERT(node, "Expecting RTL Node");
    
  // Convert the RTL to ESL port type
  CarbonCfgESLPortType portType = CarbonCfg::convertRTLPortType(rtlPort);

  UtString uESLPortName = carbonDBNodeGetLeafName(db, node);

  eslPortName = uESLPortName.c_str();
  
  // Clear it in case it already exists
  eslPortIndex.clear();

  // We already have one, indicate that
  if (rtlPort->isConnectedToXtor() || cfg->findESLPort(uESLPortName.c_str()))
    return NULL;

  CarbonCfgESLPort* eslPort = cfg->addESLPort(rtlPort,
                uESLPortName.c_str(), portType, 
                "", eCarbonCfgESLPortUndefined);

  computeModeAndType(tree, db, node, eslPort, rtlPort); 

  CompWizardPortEditor* editor = tree->getEditor();

  ESLPortItem* newItem = new ESLPortItem(editor->getPortsRoot(), eslPort);
  eslPortIndex = tree->indexFromItem(newItem);

  tree->sortChildren(editor->getPortsRoot());

  newItem->setSelected(true);
  tree->scrollToItem(newItem);

  return newItem;
}

// Create an ESL Port, only if it's not connected
ESLPortItem* ESLPortItem::makeESLPort(CarbonCfg* cfg, 
                                    CarbonCfgRTLPort* rtlPort, 
                                    PortEditorTreeWidget* tree,
                                    const QString& eslPortName)
{    
  // Convert the RTL to ESL port type
  CarbonCfgESLPortType portType = CarbonCfg::convertRTLPortType(rtlPort);

  UtString uESLPortName; uESLPortName << eslPortName;

  // We already have one, indicate that
  if (rtlPort->isConnectedToXtor() || cfg->findESLPort(uESLPortName.c_str()))
    return NULL;

  CarbonDB* db = cfg->getDB();
  const CarbonDBNode* node = carbonDBFindNode(db, rtlPort->getName());
  INFO_ASSERT(node, "Expecting RTL Node");

  CarbonCfgESLPort* eslPort = cfg->addESLPort(rtlPort,
                uESLPortName.c_str(), portType, 
                "", eCarbonCfgESLPortUndefined);

  computeModeAndType(tree, db, node, eslPort, rtlPort); 

  CompWizardPortEditor* editor = tree->getEditor();

  ESLPortItem* newItem = new ESLPortItem(editor->getPortsRoot(), eslPort);

  tree->sortChildren(editor->getPortsRoot());

  newItem->setSelected(true);
  tree->scrollToItem(newItem);

  return newItem;
}

void ESLPortItem::actionConnectParameter()
{
  QAction* action = qobject_cast<QAction*>(sender());
  if (action)
  {
    qDebug() << "Action" << action->text();
    if (action->isChecked())
      getTree()->getUndoStack()->push(new ESLPortItem::ConnectParameter(this, action->text()));
    else
      getTree()->getUndoStack()->push(new ESLPortItem::DisconnectParameter(this, action->text()));
  }
}


void ESLPortItem::multiSelectContextMenu(QMenu& menu)
{
  // Add the action
  menu.addAction(getActions()->getAction(ESLPortActions::TieToOne));
  menu.addAction(getActions()->getAction(ESLPortActions::TieToZero));

  if (getTree()->getEditor()->getEditorMode() == CompWizardPortEditor::eModeSocDesigner)
  {
    menu.addAction(getActions()->getAction(ESLPortActions::ResetGen));
    menu.addAction(getActions()->getAction(ESLPortActions::ClockGen));
  }

  if (getTree()->getEditor()->getEditorMode() == CompWizardPortEditor::eModeSystemC)
  {
    menu.addAction(getActions()->getAction(ESLPortActions::SystemCClockGen));
  }


  QList<PortEditorTreeItem*> items;
  getTree()->fillSelectedItems(items, PortEditorTreeItem::ESLPort);

  QAction* actionAddExpr = getActions()->getAction(ESLPortActions::AddPortExpression);
  QString expr = mESLPort->getExpr();

  // Single selection for 
  if (items.count() == 1 && expr.isEmpty())
  {
    actionAddExpr->setEnabled(true);
    actionAddExpr->setStatusTip("Add a port expression to this ESL Port");
  }
  else
  {
    actionAddExpr->setEnabled(false);
    actionAddExpr->setStatusTip("Already exists, or more than one port is selected.");
  }

  // if (getTree()->getEditor()->getEditorMode() == CompWizardPortEditor::eModeSocDesigner)
    menu.addAction(actionAddExpr);

  int numInputs = 0;
  int numOutputs = 0;

  QAction* inputAction = getActions()->getAction(ESLPortActions::ChangeInput);
  QAction* outputAction = getActions()->getAction(ESLPortActions::ChangeOutput);

  foreach(PortEditorTreeItem* item, items)
  {
    ESLPortItem* portItem = dynamic_cast<ESLPortItem*>(item);
    if (portItem->getESLPort()->getPortType() == eCarbonCfgESLInput)
      numInputs++;
    else if (portItem->getESLPort()->getPortType() == eCarbonCfgESLOutput)
      numOutputs++;
  }

  inputAction->setEnabled(numOutputs > 0);
  outputAction->setEnabled(numInputs > 0);
  CcfgHelper ccfg(getCcfg());

  if (items.count() == 1)
  {
    ESLPortItem* eslPortItem = dynamic_cast<ESLPortItem*>(items.first());
    CarbonCfgXtorParamInst* eslPortParamInst = eslPortItem->getESLPort()->getParamInstance();
    QString eslParamName;
    if (eslPortParamInst)
      eslParamName = eslPortParamInst->getParam()->getName();

    if (getTree()->getEditor()->getEditorMode() == CompWizardPortEditor::eModeSocDesigner)
    {
      // Connect to Parameters
      QMenu* paramMenu = menu.addMenu("Connect to Parameter");
      for (quint32 i=0; i<getCcfg()->numParams(); i++)
      {
        CarbonCfgXtorParamInst* parmInst = getCcfg()->getParam(i);
        QString paramName = parmInst->getParam()->getName();
        QAction* subAction = paramMenu->addAction(paramName);
        subAction->setCheckable(true);
        if (eslPortParamInst && paramName == eslParamName)
          subAction->setChecked(true);
        else
          subAction->setChecked(false);
        CQT_CONNECT(subAction, triggered(), this, actionConnectParameter());
      }
    }
  }

  menu.addAction(inputAction);
  menu.addAction(outputAction);
}

void ESLPortItem::Delete::redo()
{
  qDebug() << "Delete ESL Port" << text();

  UtString uESLPortName;
  uESLPortName << mESLPortName;

  UtString uRTLPortName;
  uRTLPortName << mRTLPortName;

  CarbonCfgESLPort* eslPort = mCfg->findESLPort(uESLPortName.c_str());
  INFO_ASSERT(eslPort, "Expecting ESL Port");

  // Remove the parameter connection
  if (eslPort->getParamInstance())
  {
    CarbonCfgXtorParamInst* paramInst = eslPort->getParamInstance();
    
    qDebug() << "num rtl ports" << paramInst->numRTLPorts();
    qDebug() << "Remove connection";
  }

  mClockMasters.clear();
  mClockMasterXtors.clear();

  // See if any xtor inst
  for (UInt32 i=0; i<mCfg->numXtorInstances(); i++)
  {
    CarbonCfgXtorInstance* xinst = mCfg->getXtorInstance(i);


    if (xinst->getType()->useESLClockMaster())
    {
      QString clockMaster = xinst->getESLClockMaster();
      if (clockMaster == mESLPortName) {
        mClockMasters.append(clockMaster);
        mClockMasterXtors.append(xinst->getName());

        QTreeWidgetItem* portsRoot = getTree()->getEditor()->getPortsRoot();
        ESLXtorInstanceItem* xtorInstItem = getTree()->findXtorInstance(portsRoot, xinst->getName());
        INFO_ASSERT(xtorInstItem, "Expected to find xtor instance");

        ESLXtorInstanceClockedByItem* clockedBy = xtorInstItem->getClockedBy();
        INFO_ASSERT(clockedBy, "Expected to find clocked by instance");
    
        clockedBy->setESLClockMaster("");
        xinst->putESLClockMaster("");
      }
    }  
  }


  // Break the connection
  mCfg->disconnect(eslPort);

  // Remove the ESL Port
  QTreeWidgetItem* item = getItem();
  QTreeWidgetItem* parent = item->parent();
  INFO_ASSERT(parent && item, "Expecting items");
  parent->removeChild(item);

  PortEditorTreeWidget* tree = getTree();
  CompWizardPortEditor* editor = tree->getEditor();

  // Make a disconnect for this RTL Port now
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uRTLPortName.c_str());
  ESLDisconnectItem* newItem = new ESLDisconnectItem(editor->getDisconnectsRoot(), rtlPort);
  newItem->setSelected(true);
  getTree()->scrollToItem(newItem);

  mESLDisconnectIndex = getTree()->indexFromItem(newItem);

  getTree()->setModified(true);
}

void ESLPortItem::Delete::undo()
{  
  CcfgHelper ccfg(mCfg);
  qDebug() << "Undo ESL Port Delete" << text();

  UtString uPortName;
  uPortName << mRTLPortName;

  // Find the RTL Port First
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(uPortName.c_str());
  INFO_ASSERT(rtlPort, "Expecting RTL Port");

  UtString uTypeDef;
  uTypeDef << mTypeDef;

  UtString uESLPortName;
  uESLPortName << mESLPortName;

  ESLPortItem* eslItem = makeESLPort(mCfg, rtlPort, getTree(), mESLPortName, mESLPortIndex);
  if (!mPortExpr.isEmpty())
    eslItem->setPortExpr(mPortExpr);

  eslItem->setPortMode(mPortMode);
  eslItem->setPortTypeDef(mTypeDef);

  if (!mParameterName.isEmpty())
  {
    CarbonCfgESLPort* eslPort = eslItem->getESLPort();
    eslPort->putParamInstance(ccfg.findParameter(mParameterName));
    eslItem->setParam(eslPort);
  }

  setTreeData(getTree(), eslItem); // Allows redo() to find this new item

  QTreeWidgetItem* disconnectItem = getTree()->itemFromIndex(mESLDisconnectIndex);
  QTreeWidgetItem* parent = disconnectItem->parent();
  parent->removeChild(disconnectItem);

  for (int cm=0; cm<mClockMasters.count(); cm++)
  { 
    QString clockMaster = mClockMasters[cm];
    QString xtorInstName = mClockMasterXtors[cm];

    for (UInt32 i=0; i<mCfg->numXtorInstances(); i++)
    {
      CarbonCfgXtorInstance* xinst = mCfg->getXtorInstance(i);
      QString instName = xinst->getName();

      if (xinst->getType()->useESLClockMaster() && instName == xtorInstName)
      {
        QTreeWidgetItem* portsRoot = getTree()->getEditor()->getPortsRoot();
        ESLXtorInstanceItem* xtorInstItem = getTree()->findXtorInstance(portsRoot, xinst->getName());
        INFO_ASSERT(xtorInstItem, "Expected to find xtor instance");

        ESLXtorInstanceClockedByItem* clockedBy = xtorInstItem->getClockedBy();
        INFO_ASSERT(clockedBy, "Expected to find clocked by instance");
 
        UtString uClockMaster;
        uClockMaster << clockMaster;

        clockedBy->setESLClockMaster(uClockMaster.c_str());
        xinst->putESLClockMaster(uClockMaster.c_str());
      }
 
    }
  }


  getTree()->setModified(false);
}

UndoData* ESLPortItem::deleteItem()
{
  ESLPortItem::Delete* undoData = new ESLPortItem::Delete();
  PortEditorTreeWidget* tree = dynamic_cast<PortEditorTreeWidget*>(treeWidget());

  undoData->setText(mESLPort->getName());
  undoData->mESLPortName = mESLPort->getName();
  undoData->mRTLPortName = mESLPort->getRTLPort()->getName();
  undoData->mPortExpr = mESLPort->getExpr();
  undoData->mPortType = mESLPort->getPortType();
  undoData->mPortMode = mESLPort->getMode();
  undoData->mTypeDef = mESLPort->getTypeDef();
  
  if (mESLPort->getParamInstance())
  {
    undoData->mParameterName = mESLPort->getParamInstance()->getParam()->getName();
    if (mESLPort->getParamInstance()->getInstance())
      undoData->mParameterInstance = mESLPort->getParamInstance()->getInstance()->getName();
  }

  undoData->mCfg = getCcfg();

  undoData->setTreeData(tree, this);

  return undoData;
}

QWidget* ESLPortItem::createEditor(const PortEditorDelegate* dg, QWidget* parent, int colIndex)
{
  ESLPortItem::Columns col = ESLPortItem::Columns(colIndex);

  if (col == ESLPortItem::colNAME)
    return new QLineEdit(parent);

  if (getTree()->getEditor()->getEditorMode() == CompWizardPortEditor::eModeCoWare || getTree()->getEditor()->getEditorMode() == CompWizardPortEditor::eModeSystemC)
  {
    if (col == ESLPortItem::colRTL_MODE)
    {   
      QComboBox* cbox = new QComboBox(parent);
      cbox->addItem("Control");
      cbox->addItem("Clock");
      cbox->addItem("Reset");

      if (getTree()->getEditor()->getEditorMode() == CompWizardPortEditor::eModeSystemC)
        cbox->setEnabled(false);

      CQT_CONNECT(cbox, currentIndexChanged(int), dg, currentIndexChanged(int));
     
      return cbox;
    }
    else if (col == ESLPortItem::colSCTYPE)
    {
      QComboBox* cbox = new QComboBox(parent);
      cbox->addItem("bool");
      cbox->addItem("sc_uint");
      cbox->addItem("sc_biguint");
      cbox->addItem("sc_logic");
      cbox->addItem("sc_lv");
      cbox->addItem("sc_bv");
      cbox->addItem("unsigned int");
      cbox->addItem("unsigned char");      
      
      CQT_CONNECT(cbox, currentIndexChanged(int), dg, currentIndexChanged(int));
     
      return cbox;
    }
  }
  return NULL;
}

CarbonCfgESLPortMode ESLPortItem::fromModeString(const QString& m)
{
  QString mode = m.toLower();

  if (mode == "undefined")
    return eCarbonCfgESLPortUndefined;
  else if (mode == "control")
    return eCarbonCfgESLPortControl;
  else if (mode == "clock")
    return eCarbonCfgESLPortClock;
  else if (mode == "reset")
    return eCarbonCfgESLPortReset;

  return eCarbonCfgESLPortUndefined;
}
void ESLPortItem::setModelData(const PortEditorDelegate*, 
                               QAbstractItemModel* model, const QModelIndex &modelIndex,
                               QWidget* editor, int colIndex)
{
  ESLPortItem::Columns col = ESLPortItem::Columns(colIndex);
  if (col == ESLPortItem::colNAME)
  {
    QLineEdit* lineEdit = qobject_cast<QLineEdit*>(editor);
    if (lineEdit)
    {
      QString oldName = model->data(modelIndex).toString();
      QString newName = lineEdit->text();
      if (oldName != newName)
      {
        model->setData(modelIndex, newName);
        getUndoStack()->push(new CmdRenameESLPort(this, getCcfg(), mESLPort, oldName, newName));     
      }
    }
  }
  else if (col == ESLPortItem::colRTL_MODE)
  {
    QComboBox* cbox = qobject_cast<QComboBox*>(editor);
    if (cbox)
    {
      QString newName = cbox->currentText();
      QString oldName = model->data(modelIndex).toString();
      if (oldName != newName)
      {
        CarbonCfgESLPortMode oldMode = fromModeString(oldName);
        CarbonCfgESLPortMode newMode = fromModeString(newName);

        model->setData(modelIndex, newName);
        getUndoStack()->push(new CmdChangeESLPortMode(this, getCcfg(), mESLPort, oldMode, newMode));     
      }
    }
 
  }
  else if (col == ESLPortItem::colSCTYPE)
  {
    QComboBox* cbox = qobject_cast<QComboBox*>(editor);
    if (cbox)
    {
      QString newName = cbox->currentText();
      QString oldName = model->data(modelIndex).toString();
      if (oldName != newName)
      {
        model->setData(modelIndex, newName);
        getUndoStack()->push(new CmdChangeESLPortTypedef(this, getCcfg(), mESLPort, oldName, newName));     
      }
    }
  }
}


// Clock Gen

// Remove the ClockGen and make it back an ESL Port
void ESLPortItem::ClockGen::undo()
{
  qDebug() << "undo ClockGen" << mESLPortName;
  PortEditorTreeWidget* tree = getTree();

  PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(tree->itemFromIndex(mClockGenIndex));

  // Remove the ClockGen item
  CarbonCfgRTLPort* rtlPort = ESLClockGenItem::removeClockGen(item, mRTLPortName);

  // Create the ESL Port & restore Expression if there was one
  ESLPortItem* newItem = ESLPortItem::createESLPort(tree, this, rtlPort);  
  if (!mESLPortExpr.isEmpty())
    newItem->setPortExpr(mESLPortExpr);
}

// Remove the ESL Port and make a ClockGen
void ESLPortItem::ClockGen::redo()
{
  qDebug() << "ClockGen" << mESLPortName;
  PortEditorTreeWidget* tree = getTree();

  // Remove the ESL Port
  CarbonCfgRTLPort* rtlPort = ESLPortItem::removeESLPort(getItem(), mESLPortName);

  ESLClockGenItem* newItem = ESLClockGenItem::createClockGen(tree, this, 
                        rtlPort, 
                        mInitialValue, mDelay, mDutyCycle,
                        mClockCycles, mCompCycles);

  // Save the index of this new item for undo
  mClockGenIndex = getTree()->indexFromItem(newItem);
  getTree()->setModified(true);
}



// SystemC

void ESLPortItem::SystemCClockGen::undo()
{
  qDebug() << "undo ClockGen" << mESLPortName;
  PortEditorTreeWidget* tree = getTree();

  PortEditorTreeItem* item = dynamic_cast<PortEditorTreeItem*>(tree->itemFromIndex(mClockGenIndex));

  // Remove the ClockGen item
  CarbonCfgRTLPort* rtlPort = SystemCClockGenItem::removeSystemCClockGen(item, mRTLPortName);

  // Create the ESL Port & restore Expression if there was one
  ESLPortItem* newItem = ESLPortItem::createESLPort(tree, this, rtlPort);  
  if (!mESLPortExpr.isEmpty())
    newItem->setPortExpr(mESLPortExpr);
}

// Remove the ESL Port and make a ClockGen
void ESLPortItem::SystemCClockGen::redo()
{
  qDebug() << "ClockGen" << mESLPortName;
  PortEditorTreeWidget* tree = getTree();

  // Remove the ESL Port
  CarbonCfgRTLPort* rtlPort = ESLPortItem::removeESLPort(getItem(), mESLPortName);

  SystemCClockGenItem* newItem = SystemCClockGenItem::createSystemCClockGen(tree, this, 
                        rtlPort, mInitialValue, 
                        mPeriod, mPeriodUnits,
                        mDutyCycle, 
                        mStartTime, mStartTimeUnits);
                        

  // Save the index of this new item for undo
  mClockGenIndex = getTree()->indexFromItem(newItem);
  getTree()->setModified(true);
}




ESLPortItem::SystemCClockGenCommand::SystemCClockGenCommand(QList<ESLPortItem*> items, 
      double period, CarbonCfgSystemCTimeUnits periodUnits,
      double dutyCycle,
      double startTime, CarbonCfgSystemCTimeUnits startTimeUnits,
      UInt32 initialValue, 
      QUndoCommand* parent)
  : MultiUndoCommand(parent)
{
  setText(QString("SystemC ClockGen %1 ESL Port(s)").arg(items.count()));

  int numSkipped = 0;
  foreach(ESLPortItem* item, items)
  {
    CarbonCfgESLPort* eslPort = item->getESLPort();
    if (eslPort->getPortType() == eCarbonCfgESLInput)
    {
      ESLPortItem::SystemCClockGen* clockGen = new ESLPortItem::SystemCClockGen(item->getTree(), item);
      clockGen->mCfg = item->getCcfg();
      clockGen->mRTLPortName = eslPort->getRTLPort()->getName();
      clockGen->mESLPortName = eslPort->getName();
      clockGen->mESLPortExpr = eslPort->getExpr();

     
      clockGen->mInitialValue = initialValue;
      clockGen->mPeriod = period;
      clockGen->mPeriodUnits = periodUnits;
      clockGen->mStartTime = startTime;
      clockGen->mStartTimeUnits = startTimeUnits;
      clockGen->mDutyCycle = dutyCycle;

      addItem(clockGen);
    }
    else
    {
      numSkipped++;
      QString warningMessage = QString("Clock Generators are not allowed on outputs (%1)")
        .arg(eslPort->getName());
      theApp->getContext()->getCarbonProjectWidget()->getConsole()->appendLine(warningMessage);
    }
  }

  if (numSkipped > 0)
  {
    QString msg = QString("Skipped %1 outputs which cannot made into Clock Generators").arg(numSkipped);
    theApp->statusBar()->showMessage(msg);
  }
}





ESLPortItem::ClockGenCommand::ClockGenCommand(QList<ESLPortItem*> items, 
      UInt32 initialValue, UInt32 delay, UInt32 dutyCycle,
      UInt32 clockCycles, UInt32 compCycles,
      QUndoCommand* parent)
  : MultiUndoCommand(parent)
{
  setText(QString("ClockGen %1 ESL Port(s)").arg(items.count()));

  int numSkipped = 0;
  foreach(ESLPortItem* item, items)
  {
    CarbonCfgESLPort* eslPort = item->getESLPort();
    if (eslPort->getPortType() == eCarbonCfgESLInput)
    {
      ESLPortItem::ClockGen* clockGen = new ESLPortItem::ClockGen(item->getTree(), item);
      clockGen->mCfg = item->getCcfg();
      clockGen->mRTLPortName = eslPort->getRTLPort()->getName();
      clockGen->mESLPortName = eslPort->getName();
      clockGen->mESLPortExpr = eslPort->getExpr();

      clockGen->mInitialValue = initialValue;
      clockGen->mDelay = delay;
      clockGen->mDutyCycle = dutyCycle;

      clockGen->mClockCycles = clockCycles;
      clockGen->mCompCycles = compCycles;

      addItem(clockGen);
    }
    else
    {
      numSkipped++;
      QString warningMessage = QString("Reset Generators are not allowed on outputs (%1)")
        .arg(eslPort->getName());
      theApp->getContext()->getCarbonProjectWidget()->getConsole()->appendLine(warningMessage);
    }
  }

  if (numSkipped > 0)
  {
    QString msg = QString("Skipped %1 outputs which cannot made into Reset Generators").arg(numSkipped);
    theApp->statusBar()->showMessage(msg);
  }
}

ESLPortItem::AddExpression::AddExpression(ESLPortItem* item, 
                    const QString& expr, QUndoCommand* parent)
                    : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mESLPortName = item->getESLPort()->getName();
  mExpr = expr;
  mInteractive = true;
  setText(QString("Add ESL Port Expression %1").arg(mESLPortName));
}

void ESLPortItem::AddExpression::undo()
{
  ESLPortItem* item = dynamic_cast<ESLPortItem*>(getItem());
  item->getESLPort()->putExpr("");
  item->setPortExpr("");
  item->setSelected(true);
  getTree()->scrollToItem(item);
  getTree()->setModified(false);
}

void ESLPortItem::AddExpression::redo()
{
  ESLPortItem* item = dynamic_cast<ESLPortItem*>(getItem());
  UtString uExpr; uExpr << mExpr;
  item->getESLPort()->putExpr(uExpr.c_str());
  item->setPortExpr(mExpr);

  if (item->getExprItem())
  {
    if (mInteractive)
      getTree()->setCurrentItem(item->getExprItem(), ESLPortExprItem::colVALUE);
    else
      item->getExprItem()->setSelected(true);
  }

  // Only the first time
  mInteractive = false;

  getTree()->scrollToItem(item);
  getTree()->setModified(true);
}

ESLPortItem::CreatePort::CreatePort(PortEditorTreeItem* item, const QString& rtlPort, QUndoCommand* parent)
  : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = rtlPort;
  QString txt = QString("Create ESL Ports for %1").arg(rtlPort);
  setText(txt);
}


void ESLPortItem::CreatePort::undo()
{
  qDebug() << "Undo Add ESL/RTL Port" << mRTLPortName;

  PortEditorTreeItem* eslItem =
    dynamic_cast<PortEditorTreeItem*>(getTree()->itemFromIndex(mESLPortIndex));

  INFO_ASSERT(eslItem, "Expecting ESL Item");

  CarbonCfgRTLPort* rtlPort = removeESLPort(eslItem, mESLPortName);
  mCfg->removeRTLPort(rtlPort);

  setModified(false);
}

void ESLPortItem::CreatePort::redo()
{
  CcfgHelper ccfg(mCfg);
  qDebug() << "Add ESL/RTL Port" << mRTLPortName;

  CarbonCfgRTLPort* rtlPort = ccfg.findRTLPort(mRTLPortName);
  if (rtlPort == NULL)
  {
    UtString uRTLPortName; uRTLPortName << mRTLPortName;
    const CarbonDBNode* node = carbonDBFindNode(mCfg->getDB(), uRTLPortName.c_str());
    INFO_ASSERT(node, "Expecting CarbonDB Node");

    bool depositable = carbonDBIsDepositable(mCfg->getDB(), node);
    bool observable = carbonDBIsObservable(mCfg->getDB(), node);

    CarbonCfgRTLPortType portType = eCarbonCfgRTLInput;
    if (depositable && observable)
      portType = eCarbonCfgRTLInout;
    else if (depositable)
      portType = eCarbonCfgRTLInput;
    else if (observable)
      portType = eCarbonCfgRTLOutput;

    qDebug() << "CreatePort Adding RTL port" << mRTLPortName << portType;

    rtlPort = mCfg->addRTLPort(uRTLPortName.c_str(),
          carbonDBGetWidth(mCfg->getDB(), node), portType);
    
    makeESLPort(mCfg, rtlPort, getTree(), mESLPortName, mESLPortIndex);
    setModified(true);
  }
}



void ESLPortItem::ChangeDirection::undo()
{
  ESLPortItem* eslItem = dynamic_cast<ESLPortItem*>(getItem());
  INFO_ASSERT(eslItem, "Expected Item");
  eslItem->setPortType(mOldDirection);
  getTree()->setModified(false);
}

void ESLPortItem::ChangeDirection::redo()
{
  ESLPortItem* eslItem = dynamic_cast<ESLPortItem*>(getItem());
  INFO_ASSERT(eslItem, "Expected Item");

  eslItem->setPortType(mNewDirection);
  getTree()->setModified(true);
}


ESLPortItem::ChangeDirectionCommand::ChangeDirectionCommand(QList<ESLPortItem*> items, 
      CarbonCfgESLPortType newDirection, QUndoCommand* parent)
  : MultiUndoCommand(parent)
{
  setText(QString("Change Direction %1 ESL Port(s)").arg(items.count()));

  foreach(ESLPortItem* item, items)
  {
    ESLPortItem::ChangeDirection* changeDir = new ESLPortItem::ChangeDirection(item->getTree(), item);
    changeDir->mCfg = item->getCcfg();
    changeDir->mOldDirection = item->getESLPort()->getPortType();
    changeDir->mNewDirection = newDirection;
    addItem(changeDir);
  }
}

ESLPortItem::ConnectParameter::ConnectParameter(ESLPortItem *item, 
                                                const QString &paramName, QUndoCommand *parent)
                                                : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mESLPortName = item->getESLPort()->getName();
  mNewParamName = paramName;
  if (item->getESLPort()->getParamInstance())
    mOldParamName = item->getESLPort()->getParamInstance()->getParam()->getName();

  setText(QString("Connect ESL Port %1 to Parameter %2").arg(mESLPortName).arg(paramName));

}

void ESLPortItem::ConnectParameter::undo()
{
  CcfgHelper ccfg(mCfg);
  ESLPortItem* eslItem = dynamic_cast<ESLPortItem*>(getItem());
  INFO_ASSERT(eslItem, "Expected Item");
  CarbonCfgESLPort* eslPort = eslItem->getESLPort();

  qDebug() << "Undo Connect ESLPort Parameter" << eslPort->getName() << "param" << mNewParamName;

  if (mOldParamName.isEmpty())
    eslPort->putParamInstance(NULL);
  else
  {
    CarbonCfgXtorParamInst* paramInst = ccfg.findParameter(mOldParamName);
    eslPort->putParamInstance(paramInst);
  }
  
  eslItem->setParam(eslPort);

  setModified(false);
}

void ESLPortItem::ConnectParameter::redo()
{

  CcfgHelper ccfg(mCfg);
  ESLPortItem* eslItem = dynamic_cast<ESLPortItem*>(getItem());
  INFO_ASSERT(eslItem, "Expected Item");

  CarbonCfgESLPort* eslPort = eslItem->getESLPort();

  qDebug() << "Connect ESLPort Parameter" << eslPort->getName() << "param" << mNewParamName;

  if (mNewParamName.isEmpty())
    eslPort->putParamInstance(NULL);
  else
  {
    CarbonCfgXtorParamInst* paramInst = ccfg.findParameter(mNewParamName);
    eslPort->putParamInstance(paramInst);
  }
  
  eslItem->setParam(eslPort);

  setModified(true);
}

// Disconnect
ESLPortItem::DisconnectParameter::DisconnectParameter(ESLPortItem *item, 
                                                const QString &paramName, QUndoCommand *parent)
                                                : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mESLPortName = item->getESLPort()->getName();
  mParameterName = paramName;

  setText(QString("Disconnect ESL Port %1 from Parameter %2").arg(mESLPortName).arg(paramName));

}

void ESLPortItem::DisconnectParameter::undo()
{
  CcfgHelper ccfg(mCfg);
  ESLPortItem* eslItem = dynamic_cast<ESLPortItem*>(getItem());
  INFO_ASSERT(eslItem, "Expected Item");
  CarbonCfgESLPort* eslPort = eslItem->getESLPort();

  qDebug() << "Undo Disconnect ESLPort Parameter" << eslPort->getName() << "param" << mParameterName;

  if (mParameterName.isEmpty())
    eslPort->putParamInstance(NULL);
  else
  {
    CarbonCfgXtorParamInst* paramInst = ccfg.findParameter(mParameterName);
    eslPort->putParamInstance(paramInst);
  }
  
  eslItem->setParam(eslPort);

  setModified(false);
}

void ESLPortItem::DisconnectParameter::redo()
{
  CcfgHelper ccfg(mCfg);
  ESLPortItem* eslItem = dynamic_cast<ESLPortItem*>(getItem());
  INFO_ASSERT(eslItem, "Expected Item");

  CarbonCfgESLPort* eslPort = eslItem->getESLPort();

  qDebug() << "Disconnect ESLPort Parameter" << eslPort->getName() << "param" << mParameterName;

  eslPort->putParamInstance(NULL);
  
  eslItem->setParam(eslPort);

  setModified(true);
}

UndoData* ESLPortParameter::deleteItem()
{
  ESLPortItem* parentItem = dynamic_cast<ESLPortItem*>(PortEditorTreeItem::parent());
  if (!parentItem->isSelected())
  {
    ESLPortParameter::Delete* undoData = new ESLPortParameter::Delete();

    undoData->mCfg = getCcfg();
    undoData->setTreeData(treeWidget(), this);
    undoData->mESLPortName = getESLPort()->getName();
    undoData->mESLPortIndex = getTree()->indexFromItem(parentItem);
    if (getESLPort()->getParamInstance())
      undoData->mParameterName = getESLPort()->getParamInstance()->getParam()->getName();
    return undoData;
  }
  return NULL;
}


void ESLPortParameter::Delete::undo()
{
  CcfgHelper ccfg(mCfg);
  ESLPortItem* eslItem = dynamic_cast<ESLPortItem*>(getTree()->itemFromIndex(mESLPortIndex));
  INFO_ASSERT(eslItem, "Expected Item");

  CarbonCfgESLPort* eslPort = eslItem->getESLPort();

  qDebug() << "Undo Delete ESLPort Parameter" << eslPort->getName() << "param" << mParameterName;

  if (mParameterName.isEmpty())
    eslPort->putParamInstance(NULL);
  else
  {
    CarbonCfgXtorParamInst* paramInst = ccfg.findParameter(mParameterName);
    eslPort->putParamInstance(paramInst);
  }
  
  ESLPortParameter* newItem = eslItem->setParam(eslPort);
  setTreeData(getTree(), newItem);
  if (newItem)
  {
    newItem->setSelected(true);
    getTree()->scrollToItem(newItem);
  }

  getTree()->setModified(false);
}


void ESLPortParameter::Delete::redo()
{
  CcfgHelper ccfg(mCfg);
  ESLPortItem* eslItem = dynamic_cast<ESLPortItem*>(getTree()->itemFromIndex(mESLPortIndex));
  INFO_ASSERT(eslItem, "Expected Item");

  CarbonCfgESLPort* eslPort = eslItem->getESLPort();

  qDebug() << "Delete ESLPort Parameter" << eslPort->getName() << "param" << mParameterName;

  eslPort->putParamInstance(NULL);
  eslItem->setParam(eslPort);
  getTree()->setModified(true);
}
