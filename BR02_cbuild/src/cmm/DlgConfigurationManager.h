#ifndef DLGCONFIGURATIONMANAGER_H
#define DLGCONFIGURATIONMANAGER_H

#include <QDialog>
#include "ui_DlgConfigurationManager.h"

class CarbonProjectWidget;

class DlgConfigurationManager : public QDialog
{
  Q_OBJECT

public:
  DlgConfigurationManager(QWidget *parent = 0);
  ~DlgConfigurationManager();
  void setProject(CarbonProjectWidget* proj);

private slots:
  void on_pushButtonRename_clicked();
  void on_pushButtonDelete_clicked();
  void on_pushButtonClose_clicked();
  void on_pushButtonNew_clicked();
  void on_pushButtonCopy_clicked();
  void itemChanged(QListWidgetItem*);

private:
  void apply();
  void enableButtons();

private:
  Ui::DlgConfigurationManagerClass ui;
  CarbonProjectWidget* mProject;
  QStringList mDeletedItems;
};

#endif // DLGCONFIGURATIONMANAGER_H
