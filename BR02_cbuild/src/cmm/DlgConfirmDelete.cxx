//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "DlgConfirmDelete.h"

DlgConfirmDelete::DlgConfirmDelete(QWidget *parent, const char* delMsg, const char* remMsg)
    : QDialog(parent)
{
  ui.setupUi(this);
  ui.labelRemove->setText(remMsg);
  ui.labelDelete->setText(delMsg);
}

DlgConfirmDelete::~DlgConfirmDelete()
{

}


void DlgConfirmDelete::on_pushButtonRemove_clicked()
{
  done(DlgConfirmDelete::Remove);
}

void DlgConfirmDelete::on_pushButtonDelete_clicked()
{
  done(DlgConfirmDelete::Delete);
}

void DlgConfirmDelete::on_pushButtonCancel_clicked()
{
  done(DlgConfirmDelete::Cancel);
}
