//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "CarbonCompilerTool.h"
#include "util/OSWrapper.h"

CarbonCompilerTool::CarbonCompilerTool() : CarbonTool("VSPCompiler", ":cmm/Resources/VSPCompilerTool.xml")
{
  mProperties.readPropertyDefinitions(":/cmm/Resources/VSPCompilerProperties.xml");

  //if (!OSSupports64BitModels())
  //{
  //    const CarbonProperty* compilerControl = mProperties.findProperty("Compiler Control");
  //    if (compilerControl)
  //      mProperties.removeProperty(compilerControl);
  //}

  mDefaultOptions = new CarbonOptions(NULL, getProperties(), "VSPCompilerDefaults");
  mOptions = new CarbonOptions(mDefaultOptions, getProperties(), "VSPCompiler");
  putProgram("cbuild");

  qDebug() << "-o" << mOptions->getValue("-o")->getValue();

  mOptions->setBlockSignals(true);
  // set any gui defaults here.
  for (UInt32 i=0; i<mProperties.numGroups(); i++)
  {
    CarbonPropertyGroup* group = mProperties.getGroup(i);

    for (CarbonPropertyGroup::SwitchLoop p = group->loopSwitches(); !p.atEnd(); ++p)
    {
      CarbonProperty* prop = p.getValue();
      if (prop->hasGuiDefaultValue())
      {
        const char *propName = prop->getName();
        mOptions->getValue(propName)->putValue(prop->getGuiDefaultValue());
      }
    }
  }

  mOptions->setBlockSignals(false);
}

