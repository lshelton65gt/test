//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "MemEditorCommands.h"
#include "CcfgHelper.h"


MemTreeUndoCommand::MemTreeUndoCommand(MemEditorTreeItem* item, QUndoCommand* parent)
    : QUndoCommand(parent)
{
  mTree = dynamic_cast<MemEditorTreeWidget*>(item->treeWidget());
  mTreeIndex = mTree->indexFromItem(item);
  mInitialModified = false; // TODO get the inital modified state
}

MemTreeUndoCommand::MemTreeUndoCommand(MemEditorTreeWidget* tree, QUndoCommand* parent)
    : QUndoCommand(parent)
{
  mTree = tree;
  mInitialModified = false; // TODO get the inital modified state
}

void MemTreeUndoCommand::updateTreeIndex(MemEditorTreeItem* item)
{
  mTreeIndex = mTree->indexFromItem(item);
}

MemEditorTreeItem* MemTreeUndoCommand::getItem() 
{
  QTreeWidgetItem* item = mTree->itemFromIndex(mTreeIndex);
  INFO_ASSERT(item, "Expected to find item");
  return dynamic_cast<MemEditorTreeItem*>(item);
}

// CmdRenameESLMemory
CmdRenameESLMemory::CmdRenameESLMemory(ESLMemory* item, 
                    CarbonCfgMemory* mem , const QString& oldName, 
                    const QString& newName, QUndoCommand* parent)
                                   : MemTreeUndoCommand(item, parent)
{
  mMemory = mem;
  mCfg = item->getCcfg();
  mOldName = oldName;
  mNewName = newName;
 
  CarbonCfg* subComp = mMemory->getParent();
  if (subComp && !subComp->isTopLevel())
    mSubComponentName = subComp->getCompName();

  setText(QString("Rename Memory from %1 to %2")
    .arg(oldName)
    .arg(newName));
}

void CmdRenameESLMemory::undo()
{
  CarbonCfg* cfg = mCfg;
  if (!mSubComponentName.isEmpty())
  {
    CcfgHelper topccfg(mCfg);
    cfg = topccfg.findSubComponent(mSubComponentName);    
  }

  CcfgHelper ccfg(cfg);

  CarbonCfgMemory* mem = ccfg.findMemory(mNewName);

  // Change Data Model
  UtString v; v << mOldName;
  cfg->changeMemoryName(mem, v.c_str());
  mOldName = mem->getName();

  ESLMemory* item = dynamic_cast<ESLMemory*>(getItem());
  item->setName(mOldName);
  item->treeWidget()->scrollToItem(item);

  // must call updateTreeIndex whenever col 0 text changes
  updateTreeIndex(item);

  setModified(false); 
}

void CmdRenameESLMemory::redo()
{
  CarbonCfg* cfg = mCfg;
  if (!mSubComponentName.isEmpty())
  {
    CcfgHelper topccfg(mCfg);
    cfg = topccfg.findSubComponent(mSubComponentName);    
  }

  CcfgHelper ccfg(cfg);

  CarbonCfgMemory* mem = ccfg.findMemory(mOldName);

  // Change Data Model
  UtString v; v << mNewName;
  cfg->changeMemoryName(mem, v.c_str());
  mNewName = mem->getName();
 
  ESLMemory* item = dynamic_cast<ESLMemory*>(getItem());
  item->setName(mNewName);
  item->treeWidget()->scrollToItem(item);

  // must call updateTreeIndex whenever col 0 text changes
  updateTreeIndex(item);

  setModified(true);
}


// CmdChangeMemoryProgOffset
CmdChangeMemoryProgOffset::CmdChangeMemoryProgOffset(ESLProgPreloadOffset* item, 
                    CarbonCfgMemory* mem, 
                    UInt64 oldValue, 
                    UInt64 newValue, QUndoCommand* parent)
                                   : MemTreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mOldValue = oldValue;
  mNewValue = newValue;
 
  setText(QString("Change Memory %3 Program Init Offset from %1 to %2")
    .arg(oldValue)
    .arg(newValue)
    .arg(mem->getName())
    );
}

void CmdChangeMemoryProgOffset::undo()
{
  // Change Data Model 
  ESLProgPreloadOffset* item = dynamic_cast<ESLProgPreloadOffset*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);

  // must call updateTreeIndex whenever col 0 text changes
  updateTreeIndex(item);

  setModified(false); 
}

void CmdChangeMemoryProgOffset::redo()
{
  ESLProgPreloadOffset* item = dynamic_cast<ESLProgPreloadOffset*>(getItem());
  
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);

  // must call updateTreeIndex whenever col 0 text changes
  updateTreeIndex(item);

  setModified(true);
}

// CmdChangeReadmemKind
CmdChangeReadmemKind::CmdChangeReadmemKind(ESLReadmemKind* item, 
                    CarbonCfgMemory* mem, 
                    CarbonCfgReadmemType oldType, 
                    CarbonCfgReadmemType newType, QUndoCommand* parent)
                                   : MemTreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mOldType = oldType;
  mNewType = newType;
 
  setText(QString("Change Memory %3 Init Readmem Type from %1 to %2")
    .arg(oldType)
    .arg(newType)
    .arg(mem->getName())
    );
}

void CmdChangeReadmemKind::undo()
{
  // Change Data Model 
  ESLReadmemKind* item = dynamic_cast<ESLReadmemKind*>(getItem());
  item->setKind(mOldType);
 
  item->treeWidget()->scrollToItem(item);

  // must call updateTreeIndex whenever col 0 text changes
  updateTreeIndex(item);

  setModified(false); 
}

void CmdChangeReadmemKind::redo()
{
  ESLReadmemKind* item = dynamic_cast<ESLReadmemKind*>(getItem());
  
  item->setKind(mNewType);
  item->treeWidget()->scrollToItem(item);

  // must call updateTreeIndex whenever col 0 text changes
  updateTreeIndex(item);

  setModified(true);
}

// CmdChangeReadmemFile
CmdChangeReadmemFile::CmdChangeReadmemFile(ESLReadmemFile* item, 
                    CarbonCfgMemory* mem, 
                    const QString& oldValue, 
                    const QString& newValue, QUndoCommand* parent)
                                   : MemTreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mOldValue = oldValue;
  mNewValue = newValue;
 
  setText(QString("Change Memory %3 Init Readmem File from %1 to %2")
    .arg(oldValue)
    .arg(newValue)
    .arg(mem->getName())
    );
}

void CmdChangeReadmemFile::undo()
{
  // Change Data Model 
  ESLReadmemFile* item = dynamic_cast<ESLReadmemFile*>(getItem());
  item->setFile(mOldValue);
 
  item->treeWidget()->scrollToItem(item);

  // must call updateTreeIndex whenever col 0 text changes
  updateTreeIndex(item);

  item->check(true);

  setModified(false); 
}

void CmdChangeReadmemFile::redo()
{
  ESLReadmemFile* item = dynamic_cast<ESLReadmemFile*>(getItem());
  
  item->setFile(mNewValue);
  item->treeWidget()->scrollToItem(item);

  // must call updateTreeIndex whenever col 0 text changes
  updateTreeIndex(item);

  item->check(true);

  setModified(true);
}

UndoCommandFrame::UndoCommandFrame(MemEditorTreeItem* item, QUndoCommand* parent)
    : MemTreeUndoCommand(item, parent)
{
}

UndoCommandFrame::UndoCommandFrame(MemEditorTreeWidget* tree, QUndoCommand* parent)
    : MemTreeUndoCommand(tree, parent)
{
  
}
void UndoCommandFrame::undo()
{
  foreach(MemTreeUndoCommand* cmd, mCommands)
  {
    cmd->undo();
  }
}

void UndoCommandFrame::redo()
{
  foreach(MemTreeUndoCommand* cmd, mCommands)
  {
    cmd->redo();
  }
}




