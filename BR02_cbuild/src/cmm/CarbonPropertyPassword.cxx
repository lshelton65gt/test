//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QtGui>

#include "util/XmlParsing.h"

#include "gui/CQt.h"

#include "util/CarbonAssert.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"

#include "CarbonCompilerTool.h"
#include "CarbonPropertyPassword.h"
#include "CarbonProjectWidget.h"


bool CarbonPropertyPassword::parseXML(xmlNodePtr /*parent*/, UtXmlErrorHandler* /*eh*/)
{
  return true;
}

bool CarbonPropertyPassword::readValueXML(xmlNodePtr parent,  CarbonOptions* options, UtXmlErrorHandler* /*eh*/) const
{
  for (xmlNode *child = parent->children; child != NULL; child = child->next) 
  {
    const char* element = XmlParsing::elementName(child);
    if (element == NULL) // Skip non-interesting XML things
      continue;

    if (0 == strcmp(element, "Value"))
    {
      UtString value;
      XmlParsing::getContent(child, &value);
      options->putValue(this, value.c_str());
    }
  }
  return true;}

bool CarbonPropertyPassword::writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* /*eh*/)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Option");
  xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST getName());

  xmlTextWriterStartElement(writer, BAD_CAST "Value");
  xmlTextWriterWriteString(writer, BAD_CAST value->getValue());
  xmlTextWriterEndElement(writer);

  xmlTextWriterEndElement(writer);
  return true;
}
CarbonDelegate* CarbonPropertyPassword::createDelegate(QObject* parent,  PropertyEditorDelegate* delegate, PropertyEditor* propEditor)
{
  return new CarbonDelegatePassword(parent, delegate, propEditor);
}

// Delegate

// Editor Delegate
CarbonDelegatePassword::CarbonDelegatePassword(QObject *parent, PropertyEditorDelegate* delegate, PropertyEditor* editor)
: CarbonDelegate(parent,delegate,editor)
{
}

void CarbonDelegatePassword::commitAndCloseEditor()
{
  QLineEdit *widget = qobject_cast<QLineEdit *>(sender());
  if (widget)
    mDelegate->commit(widget);
}

QWidget* CarbonDelegatePassword::createEditor(QWidget *parent, QTreeWidgetItem* /*item*/, CarbonProperty* prop, const QModelIndex& /*index*/) const
{
  bool id,is;
  CarbonPropertyValue* sv = mEditor->getSettings()->getValue(prop,&id,&is);

  QLineEdit* lineEdit = new QLineEdit(parent);
  lineEdit->setEchoMode(QLineEdit::Password);

  lineEdit->setText(sv->getValue());
  connect(lineEdit, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
  return lineEdit;
}

void CarbonDelegatePassword::setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const
{
  bool id,is;

  if (mEditor->getSettings() == NULL)
    return;

  CarbonPropertyValue* sv = mEditor->getSettings()->getValue(prop,&id,&is);
  if (sv == NULL)
    return;

 
  UtString currentValue = sv->getValue();

  
  QLineEdit* lineEdit = qobject_cast<QLineEdit *>(editor);
  if (lineEdit != NULL)
  {
    UtString value;
    value << lineEdit->text();

    QRegExp re(prop->getLegalPattern());
    if (prop->hasLegalPattern() && !re.exactMatch(lineEdit->text()))
    {
      QString msg = QString("The switch '%1' value of '%2' does not match the legal pattern: %3, ignoring.")
        .arg(prop->getName())
        .arg(lineEdit->text())
        .arg(prop->getLegalPattern());
      QMessageBox::warning(NULL, MODELSTUDIO_TITLE, msg);
      return;
    }

    if (prop->getRequired() && value.length() == 0)
    {
      QString msg = QString("The switch '%1' value must not be blank").arg(prop->getName());
      QMessageBox::warning(NULL, MODELSTUDIO_TITLE, msg);
      return;
    }
    setValue(sv, item, model, index, currentValue.c_str(), value.c_str(), prop);
  }
}

