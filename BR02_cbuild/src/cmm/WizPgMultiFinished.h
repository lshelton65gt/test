#ifndef WIZPGMULTIFINISHED_H
#define WIZPGMULTIFINISHED_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "WizardPage.h"

#include <QWizardPage>
#include "ui_WizPgMultiFinished.h"
#include "CarbonConsole.h"

class WizPgMultiFinished : public WizardPage
{
  Q_OBJECT

public:
  WizPgMultiFinished(QWidget *parent = 0);
  ~WizPgMultiFinished();

private:
  void append(const QString& str);
  QString generateMemoryGenFile();
  void addOption(QDomDocument& doc, QDomElement& parent, const QString& name, const QString& value);

protected:
  virtual void initializePage();
  virtual bool validatePage();
  virtual bool isComplete() const;
  virtual bool isFinalPage () const { return true; }
  virtual int nextId() const { return -1; }

private slots:
  void outputChanged(CarbonConsole::CommandType ctype, const char* text, const char* workingDir);

private:
  bool mIsValid;
  bool mConnectedOutput;
  Ui::WizPgMultiFinishedClass ui;
};

#endif // WIZPGMULTIFINISHED_H
