#ifndef WIZPGSTART_H
#define WIZPGSTART_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QWizardPage>
#include "WizardPage.h"

#include "ui_WizPgStart.h"

class WizPgStart : public WizardPage
{
  Q_OBJECT

public:
  WizPgStart(QWidget *parent = 0);
  ~WizPgStart();

protected:
  virtual void initializePage();
  virtual bool validatePage();
  int nextId() const;

private:
  Ui::WizPgStartClass ui;
};

#endif // WIZPGSTART_H
