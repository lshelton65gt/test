//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "CarbonProjectWidget.h"
#include "CarbonProject.h"
#include "DlgBatchBuild.h"

DlgBatchBuild::DlgBatchBuild(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);

  QObject::connect(ui.closeButton, SIGNAL(clicked()), this, SLOT(reject()));
}

DlgBatchBuild::~DlgBatchBuild()
{

}

void DlgBatchBuild::buildCheckedList(QStringList& checkedList)
{
  checkedList.clear();
  for (int i=0; i<ui.listWidget->count(); i++)
  {
    QListWidgetItem* item = ui.listWidget->item(i);
    if (item->checkState() == Qt::Checked)
      checkedList.push_back(item->text());
  }
}

void DlgBatchBuild::checkButtons(Qt::CheckState newState)
{
  for (int i=0; i<ui.listWidget->count(); i++)
  {
    QListWidgetItem* item = ui.listWidget->item(i);
    item->setCheckState(newState);
  }
}

void DlgBatchBuild::on_buildButton_clicked()
{
  accept();
  QStringList checkedList;
  buildCheckedList(checkedList);
  mProject->actionBatchBuild(checkedList, CarbonProjectWidget::eBatchBuild);
}

void DlgBatchBuild::on_rebuildButton_clicked()
{
  accept();
  QStringList checkedList;
  buildCheckedList(checkedList);
  mProject->actionBatchBuild(checkedList, CarbonProjectWidget::eBatchRebuild);
}

void DlgBatchBuild::on_cleanButton_clicked()
{
  accept();
  QStringList checkedList;
  buildCheckedList(checkedList);
  mProject->actionBatchBuild(checkedList, CarbonProjectWidget::eBatchClean);
}

void DlgBatchBuild::on_selectAll_clicked()
{
  checkButtons(Qt::Checked);
}
void DlgBatchBuild::on_deselectAll_clicked()
{
  checkButtons(Qt::Unchecked);
}

void DlgBatchBuild::setProject(CarbonProjectWidget* proj)
{
  mProject = proj;

  CarbonConfigurations* configs = proj->project()->getConfigurations();

  for (UInt32 i = 0; i < configs->numConfigurations(); i++)
  {
    QListWidgetItem* item = new QListWidgetItem();
    CarbonConfiguration* config = configs->getConfiguration(i);

    item->setText(config->getName());
    ui.listWidget->addItem(item);
  }

  checkButtons(Qt::Checked);
}

