//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include "DlgRunScript.h"

#define SETTING_SCRIPT_NAME "dlgRunScript/fileName"
#define SETTING_SCRIPT_DEBUG "dlgRunScript/debugScript"

DlgRunScript::DlgRunScript(QWidget *parent)
: QDialog(parent)
{
  ui.setupUi(this);
  restoreSettings();
}

DlgRunScript::~DlgRunScript()
{
}

void DlgRunScript::saveSettings()
{
  mSettings.setValue(SETTING_SCRIPT_NAME, ui.lineEditScriptFilename->text());
  mSettings.setValue(SETTING_SCRIPT_DEBUG, ui.checkBoxEnableDebug->isChecked());
}

void DlgRunScript::restoreSettings()
{
  ui.lineEditScriptFilename->setText(mSettings.value(SETTING_SCRIPT_NAME, "").toString());
  ui.checkBoxEnableDebug->setChecked(mSettings.value(SETTING_SCRIPT_DEBUG, false).toBool());
}

void DlgRunScript::on_toolButtonBrowse_clicked()
{
  QString fileName = QFileDialog::getOpenFileName(this, "Script", ui.lineEditScriptFilename->text(),"Script files (*.js)");
  if (!fileName.isEmpty())
    ui.lineEditScriptFilename->setText(fileName);
}


void DlgRunScript::on_pushButtonRun_clicked()
{
  if (ui.lineEditScriptFilename->text().length() > 0)
  {
    mEngine.setEnableDebugger(ui.checkBoxEnableDebug->isChecked());
    saveSettings();
    //mEngine.runScriptUI("z:/Carbon/Projects/Project12/calculator.js", "z:/Carbon/Projects/Project12/calculator.ui", "Calculator");
    mEngine.runScript(ui.lineEditScriptFilename->text());
  }
  else
    QMessageBox::information(this, "Carbon Model Studio", "No script file was specified");
}


void DlgRunScript::on_pushButtonClose_clicked()
{
  reject();
}
