//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "gui/CQt.h"

#include "SettingsEditor.h"
#include "SettingsContainer.h"

#include "CarbonProject.h"
#include "CarbonCompilerTool.h"

SettingsEditor::SettingsEditor(QWidget *parent)
: QWidget(parent)
{
  ui.setupUi(this);
  
  connect(ui.treeWidget, SIGNAL(itemSelectionChanged()), this, SLOT(selectionChanged()));
  mSettings = NULL;

  ui.textBrowser->viewport()->setBackgroundRole(QPalette::Button);
  ui.textBrowser->viewport()->setForegroundRole(QPalette::ButtonText);
  ui.textBrowser->setMinimumHeight(0);

  ui.textBrowser->setHtml("");
  ui.labelTitle->setText("");
  ui.labelCurrentValue->setText("");

  ui.splitter->setSizes(QList<int>() << 200 << 40);

  CQT_CONNECT(ui.comboBoxItems, currentIndexChanged(int), this, itemSelectionChanged(int));
  CQT_CONNECT(ui.treeWidget, resetButtonStateChanged(bool,bool), this, updateResetButtons(bool,bool));
}

void SettingsEditor::putContext(CarbonMakerContext* ctx)
{
  mContext = ctx;
  ui.treeWidget->putContext(ctx);
}

void SettingsEditor::selectionChanged()
{
  foreach (QTreeWidgetItem *itm, ui.treeWidget->selectedItems())
  {
    itemActivated(itm, 0);
    break;
  }
}
void SettingsEditor::registerActiveConfigurationChange(QComboBox* src)
{
  connect(src, SIGNAL(currentIndexChanged(int)), this, SLOT(configurationChanged()));
}

void SettingsEditor::configurationChanged()
{
}

void SettingsEditor::itemActivated(QTreeWidgetItem* itm, int /*col*/)
{
  if (itm != NULL)
  {
    QVariant qv = itm->data(0, Qt::UserRole);
    if (qv.isValid())
    {
      CarbonPropertyContext* propContext = (CarbonPropertyContext*)qv.value<void*>();

      if (propContext->getProperty()->isMultiValued())
        setMaximumWidth(400);
      else
        setMaximumWidth(16777215);

      ui.textBrowser->setHtml(propContext->getProperty()->getDescription());
    }
    ui.labelCurrentValue->setText(itm->text(1));
    ui.labelTitle->setText(itm->text(0));
  }
  else
  {
    ui.labelTitle->setText("");
    ui.labelCurrentValue->setText("");
    ui.textBrowser->setHtml("");
  }
}
SettingsEditor::~SettingsEditor()
{
}

void SettingsEditor::setSettings(CarbonOptions* settings)
{
  setSettings(settings, PropertyEditor::Categorized);
}

void SettingsEditor::commitEdit()
{
  PropertyEditor* propEditor = ui.treeWidget;
  if (propEditor)
    propEditor->commitEdit();
}

void SettingsEditor::updateResetButtons(bool resetAll, bool resetSwitch)
{
  ui.toolButtonReset->setEnabled(resetSwitch);
  ui.toolButtonResetAll->setEnabled(resetAll);
}

void SettingsEditor::setSettings(CarbonOptions* settings, PropertyEditor::SortType sortType)
{
  mSettings = settings;
  ui.toolButtonAlphabetical->setEnabled(settings != NULL);
  ui.toolButtonCategorized->setEnabled(settings != NULL);
  ui.treeWidget->populate(settings, sortType);

  ui.textBrowser->setHtml("");
  ui.labelTitle->setText("");
  ui.labelCurrentValue->setText("");

  //
  ui.comboBoxItems->blockSignals(true);
  ui.comboBoxItems->clear();

  int dataIndex = 0;

  if (settings)
  {
    foreach (QString str, settings->getItems())
    {
      QVariant data = settings->getItemsData().at(dataIndex);
      ui.comboBoxItems->addItem(str, data);
      dataIndex++;
    }
    const char* item = settings->getItem();
    if (item && strlen(item) > 0 && settings->getItems().count() == 1)
      ui.comboBoxItems->setCurrentIndex(ui.comboBoxItems->findText(item));
    else
      ui.comboBoxItems->setCurrentIndex(-1);

    // Check for a single item
    if (settings->getItems().count() == 0 && item && strlen(item) > 0)
    {
      ui.comboBoxItems->addItem(item, item);
      ui.comboBoxItems->setCurrentIndex(0);
    }
  }

  ui.comboBoxItems->blockSignals(false);
}

void SettingsEditor::itemSelectionChanged(int index)
{
  if (index != -1) // Nothing selected
  {
    UtString itemText;
    itemText << ui.comboBoxItems->itemText(index);
    QVariant data = ui.comboBoxItems->itemData(index);
    mSettings->setSelectedItem(itemText.c_str(), data);
  }
}


void SettingsEditor::on_toolButtonReset_clicked()
{
  ui.treeWidget->resetSwitch();
}
void SettingsEditor::on_toolButtonResetAll_clicked()
{
  ui.treeWidget->resetAllSwitches();
}

void SettingsEditor::on_toolButtonCategorized_clicked()
{
  ui.treeWidget->sortCategorized();
}

void SettingsEditor::on_toolButtonAlphabetical_clicked()
{
  ui.treeWidget->sortAlphabetical();

}
