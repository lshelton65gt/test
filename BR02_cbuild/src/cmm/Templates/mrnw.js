

// The Main entry point for code generation
function generateCode(context, response)
{
  writeModuleDeclaration(context, response);
    
  generateBody(context, response);
   
  response.writeLine("endmodule");
}

function writeModuleDeclaration(context, response)
{
  response.write(sprintf("module %s(", context.template.moduleName));  
  generatePortList(context, response);
  response.writeLine(");");
}
// Write out the Module Port List of all the Unique Ports used
function generatePortList(context, response)
{
  var uniquePorts = context.model.uniquePortInstances;
  var sendComma = false;
  
  for (var i=0; i<uniquePorts.count; i++)
  {
    var portName = uniquePorts.getPortInstance(i).name;
    if (sendComma)
      response.write(",");
    response.write(portName);
    sendComma = true;
  }
}  

// Write out the PortDeclarations
function generatePortDeclarations(context, response)
{
  var writeBlocks = context.model.getBlockInstances("Write");
  for (var i=0; i<writeBlocks.count; i++)
  {
    var writeBlockInst = writeBlocks.getBlockInstance(i);
    generateWriteDeclarations(context, response, writeBlockInst);    
  }
  
  var readBlocks = context.model.getBlockInstances("Read");
  for (var i=0; i<readBlocks.count; i++)
  {
    var readBlockInst = readBlocks.getBlockInstance(i);
    generateReadDeclarations(context, response, readBlockInst);    
  }

  for (var i=0; i<context.model.blockInstances.count; i++)
  {
    var blockInst = context.model.blockInstances.getBlockInstance(i);
    if (blockInst.block.name == "Control")
      generateControlDeclarations(context, response, blockInst);
  }
}

function generateWriteDeclarations(context, response, blockInst)
{
  var parameters = context.model.mode.parameters;
  var id = blockInst.instanceID;
  var model = context.model;
  var writePorts = model.getPortInstances("Write", id);
    
  response.writeLine("");
  response.writeLine(sprintf("// Write Port %s", id)); 
  
  // Input port
  response.writeLine(sprintf("input %s;", writePorts.WriteEnable.name));
  
  // Are we bitmasking?
  if (parameters.BitMasking.boolValue)
    response.writeLine(sprintf("input [DATA_WIDTH-1:0] %s;", writePorts.BitEnable.name));

  // Clock
  response.writeLine(sprintf("input %s;", writePorts.Clock.name));
  
  // Address
  response.writeLine(sprintf("input [ADDR_WIDTH-1:0] %s;", writePorts.Address.name));
  
  // DataIn
  response.writeLine(sprintf("input [DATA_WIDTH-1:0] %s;", writePorts.DataIn.name));
}

function generateReadDeclarations(context, response, blockInst)
{
  var id = blockInst.instanceID;
  var parameters = context.model.mode.parameters; 
  var model = context.model;
  var readPorts = model.getPortInstances("Read", id);

  response.writeLine("");
  response.writeLine(sprintf("// Read Port %s", id));

  // Clock
  if (parameters.Registered.boolValue)
    response.writeLine(sprintf("input %s;", readPorts.Clock.name));

  // Declare the read address
  response.writeLine(sprintf("input [ADDR_WIDTH-1:0] %s;", readPorts.Address.name));
  
  // Declare the output port
  response.writeLine(sprintf("output [DATA_WIDTH-1:0] %s;", readPorts.DataOut.name));
  
  // Registered outputs?
  if (parameters.Registered.boolValue)
    response.writeLine(sprintf("reg [DATA_WIDTH-1:0] %s;", readPorts.DataOut.name));  
}

function generateControlDeclarations(context, response, blockInst)
{
  response.writeLine("");
  response.writeLine("// The memory");
  response.writeLine("reg [DATA_WIDTH-1:0] mem[0:DEPTH-1];");
}

function generateBody(context, response)
{
  response.pushIndent(2);

  var parameters = context.template.parameters;

  // write out the parameters
  generateParameters(context, response);

  // port declarations
  generatePortDeclarations(context, response);
  
  // write out the implementation
  generateImplementation(context, response);
  
  response.popIndent();
}

function generateWriteImplementation(context, response)
{
  var parameters = context.template.parameters;
  var numWritePorts = parameters.WritePorts.intValue;  
  var model = context.model;
  var edge = parameters.ClockEdge.value;

  // Write Ports
  for (var i=0; i<numWritePorts; i++)
  {
    var instanceID = i;
    var writeBlockInst = model.getBlockInstance("Write", instanceID);
    var writePorts = writeBlockInst.portInstances;
    
    response.writeLine("");
    response.writeLine(sprintf("// Implementation port %d", i));
    response.writeLine(sprintf("always @(%s %s)", edge, writePorts.Clock.name));
    response.pushIndent(2);
    response.writeLine(sprintf("if (%s)", writePorts.WriteEnable.name));
    response.pushIndent(2);
    
    if (model.mode.parameters.BitMasking.boolValue)
         response.writeLine(sprintf("mem[%s] <= (%s & %s) | (mem[%s] & ~%s);", 
                      writePorts.Address.name,
                      writePorts.DataIn.name,
                      writePorts.BitEnable.name,
                      writePorts.Address.name,
                      writePorts.BitEnable.name
                      ));   
    else
      response.writeLine(sprintf("mem[%s] <= %s;", writePorts.Address.name, writePorts.DataIn.name));
                      
    response.popIndent();
    response.popIndent();
  }
}

function generateReadImplementation(context, response)
{
  var parameters = context.template.parameters;
  var numReadPorts = parameters.ReadPorts.intValue;
  var model = context.model;
  var edge = parameters.ClockEdge.value;

  // Read Ports
  for (var i=0; i<numReadPorts; i++)
  {
    var instanceID = i;
    var readBlockInst = model.getBlockInstance("Read", instanceID);
    var readPorts = readBlockInst.portInstances;

    response.writeLine("");
    if (context.model.mode.parameters.Registered.boolValue)
    {
      response.writeLine(sprintf("always @(%s %s)", edge, readPorts.Clock.name));
      response.pushIndent(2);
      response.writeLine(sprintf("%s <= mem[%s];", readPorts.DataOut.name, readPorts.Address.name));
      response.popIndent();
    }
    else
      response.writeLine(sprintf("assign %s = mem[%s];", readPorts.DataOut.name, readPorts.Address.name));
   }
}

function generateImplementation(context, response)
{
  var parameters = context.template.parameters;
  var ordering = parameters.OperatingMode.value;
  
  if (ordering == "ReadFirst")
  {
    generateReadImplementation(context, response);
    generateWriteImplementation(context, response);
  }
  else
  {
    generateWriteImplementation(context, response);
    generateReadImplementation(context, response);
  }
}

function generateParameters(context, response)
{
  var parameters = context.template.parameters;
  
  response.writeLine(formatParameter("ADDR_WIDTH", parameters.AddressWidth));
  response.writeLine(formatParameter("DATA_WIDTH", parameters.DataWidth));
  response.writeLine(formatParameter("DEPTH", parameters.Words));
  
  return true;
}

function formatParameter(name, p)
{
  return "parameter " + name + " = " + p.value + ";";
}