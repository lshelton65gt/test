#ifndef REGISTEREDITORWIDGET_H
#define REGISTEREDITORWIDGET_H

#include "util/CarbonPlatform.h"

#include <QWidget>
#include <QTreeWidget>

#include "RegisterTreeWidgets.h"

#include "ui_RegisterEditorWidget.h"
#include "util/UtString.h"
#include "Mdi.h"
#include "MdiProject.h"

class SpiritMemoryMap;
class SpiritAddressBlock;
class SpiritRegister;
class CarbonMakerContext;
class SpiritXML;

#define REG_TAG_REGISTER 0
#define FLD_TAG_FIELD 0
#define FLD_TAG_REGISTER 1

class RegisterEditorWidget  : public QWidget, public MDIWidget
{
  Q_OBJECT

public:
  enum RegisterColumns { colRegOFFSET, colRegNAME, colRegWIDTH, colRegDESCRIPTION, colRegRADIX, colRegLAST };
  enum FieldColumns { colFieldNAME, colFieldBITOFFSET, colFieldBITS, colFieldACCESS, colFieldRTL, colFieldPARTSELECT, colFieldMEMORYINDEX, colFieldNOTES, colFieldLAST };

  RegisterEditorWidget(CarbonMakerContext* ctx=0, MDIDocumentTemplate* doct=0, QWidget *parent = 0);
  ~RegisterEditorWidget();
  bool loadFile(const QString& qFilename);
  virtual const char* userFriendlyName();
  const char* userFriendlyCurrentFile();
  virtual void saveDocument();
  void populate(SpiritAddressBlock*);
  void setModified(bool value)
  {
    setWindowModified(value);
    setWidgetModified(value);
  }
  void createField(SpiritField* field);
  void resizeFields();
  void drawRegister(SpiritRegister* reg);

protected:
  void closeEvent(QCloseEvent *event);
  bool maybeSave();
  void updateFieldButtons();
  void updateRegisterButtons();

private:
  CarbonMakerContext* mCtx;
  UtString mFriendlyName;
  UtString mCurFile;
  SpiritXML* mSpiritXML;

private slots:
  void on_treeWidgetRegisters_itemSelectionChanged();
  void on_treeWidgetFields_itemSelectionChanged();
  void on_btnDeleteField_clicked();
  void on_btnAddField_clicked();
  void on_btnBindConstant_clicked();
  void on_btnAdd_clicked();
  void on_btnDelete_clicked();

private:
  Ui::RegisterEditorWidgetClass ui;

  const char* strippedName(const char* fullFileName);
  void showRegisterFields(SpiritRegister* reg);
  void setupRegisterTree();
  void setupFieldsTree();

};

#endif // REGISTEREDITORWIDGET_H
