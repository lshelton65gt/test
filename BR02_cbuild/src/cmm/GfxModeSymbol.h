#ifndef __GFXMODESYMBOL_H__
#define __GFXMODESYMBOL_H__
#include "util/CarbonVersion.h"
#include "gui/CQt.h"


#include "Graphics.h"

class ElaboratedModel;
class GfxBlock;

class GfxModeSymbol : public QObject
{
  Q_OBJECT

public:
  GfxModeSymbol()
  {
  }

  void draw(QGraphicsScene* scene, ElaboratedModel* model);

signals:
  void blockSelected(GfxBlock*);

private:
  void clearScene(QGraphicsScene* scene);
};

#endif
