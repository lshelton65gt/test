//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "GfxModeSymbol.h"
#include "Mode.h"
#include "Blocks.h"
#include "Block.h"
#include "GfxBlock.h"
#include "BlockInstance.h"
#include "ElaboratedModel.h"


void GfxModeSymbol::draw(QGraphicsScene* scene, ElaboratedModel* model)
{
  clearScene(scene);
  int x = 0;
  int y = 0;

  BlockInstances* instances = model->getBlockInstances();

  for (int i=0; i<instances->getCount(); i++)
  {
    BlockInstance* blockInst = instances->getAt(i);
    GfxBlock* gblock = new GfxBlock(x, y, scene, blockInst);
    if (!connect(gblock, SIGNAL(blockSelected(GfxBlock*)), this, SIGNAL(blockSelected(GfxBlock*))))
      qDebug() << "No slot";

    scene->addItem(gblock);
    gblock->setPos(x, y); 
    y += gblock->height();
  }
}

void GfxModeSymbol::clearScene(QGraphicsScene* scene)
{
  foreach (QGraphicsItem* item, scene->items())
  {
    if (item->scene() == scene)
      scene->removeItem(item);
  }
}


