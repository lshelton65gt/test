#ifndef __CARBONPROPERTYFILELIST_H_
#define __CARBONPROPERTYFILELIST_H_

#include "util/CarbonPlatform.h"
#include <QObject>
#include "CarbonProperty.h"
#include "PropertyEditor.h"

#include "util/UtString.h"
#include "util/UtStringArray.h"

class CarbonPropertyFilelist : public CarbonProperty
{
public:
  CARBONMEM_OVERRIDES

  enum RepeatKind { OptionNameValue=0, ValuesOnly, ValuesOnlyOnePerLine, OptionNameValueCompressed};
  enum FileType { InputFile, OutputFile, InputDirectories };

  virtual CarbonDelegate* createDelegate(QObject* parent, PropertyEditorDelegate* d, PropertyEditor* propEd);
  virtual bool parseXML(xmlNodePtr parent, UtXmlErrorHandler* eh);
  virtual bool writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* eh);
  virtual bool readValueXML(xmlNodePtr parent, CarbonOptions* options, UtXmlErrorHandler* eh) const;

  CarbonPropertyFilelist(const char* name, int nv, const char* descr) : CarbonProperty(name,nv,descr,CarbonProperty::Filelist)
  {
    mKind = OptionNameValue;
  }
  
  int numStrings() { return mStrings.size(); }
  void addString(const char* str)
  {
    mStrings.push_back(str);
  }
  void removeString(const char* str)
  {
    mStrings.remove(str);
  }

  RepeatKind getRepeatKind() const { return mKind; }
  FileType getFileType() const { return mFileType; }

  const char* getString(UInt32 i) { return mStrings[i]; }

  const char* getFilter() const { return mFilter.c_str(); }
  void putFilter(const char* newVal) {mFilter=newVal;}

  const char* getComponentDir() const { return mComponentDir.c_str(); }
  void putComponentDir(const char* newVal) { mComponentDir=newVal; }

private:
  UtStringArray mStrings;
  RepeatKind mKind;
  FileType mFileType;
  UtString mFilter;
  UtString mComponentDir;
};


class CarbonDelegateFilelist : public CarbonDelegate
{
  Q_OBJECT
public:
  CarbonDelegateFilelist(QObject *parent = 0, PropertyEditorDelegate* d=0, PropertyEditor* e=0);
  QWidget* createEditor(QWidget *parent, QTreeWidgetItem* item, CarbonProperty* prop, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const;

private slots:
  void commitAndCloseEditor();
  void showFilelistDialog();
};
#endif
