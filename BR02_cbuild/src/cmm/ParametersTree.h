#ifndef PARAMETERSTREE_H
#define PARAMETERSTREE_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QtGui>

class ParametersTreeDelegate;
class Parameter;
class Parameters;

class ParametersTree : public QTreeWidget
{
  Q_OBJECT

public:
  enum Columns { colNAME, colVALUE, colDESCRIPTION, colLAST } ;

  ParametersTree(QWidget *parent);
  ~ParametersTree();

  void populate(Parameters* params);
  void signalValuesChanged() { emit valuesChanged(); }

  friend class ParametersTreeDelegate;

signals:
  void valuesChanged();

private slots:
  void editSelectedItem();

private:
  QTreeWidgetItem* addRow(Parameter* p);

};




#endif // PARAMETERSTREE_H
