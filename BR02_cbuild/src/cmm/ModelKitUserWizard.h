#ifndef MODELKITUSERWIZARD_H
#define MODELKITUSERWIZARD_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "Scripting.h"
#include "MKUWPageStart.h"
#include "MKUWPageFinished.h"
#include "MKUWPageClone.h"
#include "MKUWPageUserActions.h"
#include "MKUWConfigPage.h"

#include "cmm.h"
#include <QtGui>
#include <QWizard>
#include "ui_ModelKitUserWizard.h"
#include "KitManifest.h"


class ModelKitAnswers
{
public: 
  enum ModelTarget { Unix, Windows };
  enum ModelSymbols { FullSymbols, IOSymbols };

  ModelTarget getModelTarget() const { return mModelTarget; }
  ModelSymbols getModelSymbols() const { return mModelSymbols; }
  QString getComponentName() const { return mComponentName; }

  ModelKitAnswers(KitManifest* mf)
  {
    mManifest = mf;
  }
  void readAnswers(const QString& fileName);
  
  bool hasSetting(const QString& key) { return mSettings.contains(key); }
  QVariant getSetting(const QString& key)
  {
    if (hasSetting(key))
     return mSettings[key];
    else
    {
      QString msg = QString("Unable to find answer for Setting %1").arg(key);
      theApp->abortApplication(msg, 101);
    }
    return QVariant();
  }
  bool hasQuestion(const QString& id) { return mQuestions.contains(id); }
  QVariant getQuestion(const QString& id)
  {
    if (hasQuestion(id))
      return mQuestions[id];
    else
    {
      QString msg = QString("Unable to find answer for Question %1").arg(id);
      theApp->abortApplication(msg, 102);
    }
    return QVariant();
  }
  bool hasAction(const QString& id) { return mActions.contains(id); }
  QDomElement getAction(const QString& id)
  {
    if (hasAction(id))
      return mActions[id];
    else
    {
      QString msg = QString("Unable to find answer for Action %1").arg(id);
      theApp->abortApplication(msg, 103);
    }
    return QDomElement();
  }

private:
  void deserialize(const QDomElement& parent);
  void deserializeSettings(const QDomElement& parent);
  void deserializeActions(const QDomElement& parent);
  void deserializeQuestions(const QDomElement& parent);

private:
  QMap<QString, QVariant> mSettings;
  QMap<QString, QVariant> mQuestions;
  QMap<QString, QDomElement> mActions;
  KitManifest* mManifest;
  ModelTarget mModelTarget;
  ModelSymbols mModelSymbols;
  QString mComponentName;
};

class ModelKit;

class ModelKitUserWizard : public QWizard
{
  Q_OBJECT

public:
  enum { PageStart, PageConfig, PageClone, PageUserActions, PageFinished } ;
  enum WizardMode { Recording, Playback };
  enum ActionWhen { Precompilation, Postcompilation };
  enum ModelTarget { Unix, Windows };
  enum ModelSymbols { FullSymbols, IOSymbols };

public:
  ModelKitUserWizard(QWidget *parent = 0, ModelKit* kit = 0, KitManifest* manifest = 0);
  ~ModelKitUserWizard();

  void cleanupFiles();

  KitManifest* getManifest() const { return mManifest; }
  KitUserContext* getUserContext() { return &mUserContext; }
  WizardMode getMode() const { return mMode; }
  ModelKit* getModelKit() const { return mModelKit; }

  void setWhen(ActionWhen newVal) { mWhen=newVal; }
  ActionWhen getWhen() const { return mWhen; }

  void recordSetting(QObject* obj, const QString& key, const QVariant value);
  QDomElement recordAction(KitUserAction* action);
  QDomElement recordConfiguration(const QString& key, const QVariant value);
  void recordQuestion(const QString& questionId, const QString& result);
  void recordQuestion(const QString& questionId, int result);
  void recordQuestion(const QString& questionId, double result);
  void recordFilename(const QString& questionId, const QString& result);
  void recordFilenames(const QString& questionId, const QStringList& results);

  void startRecording();
  void finishRecording();

  QString getComponentName() { return mComponentName; }
  void setComponentName(const QString& newVal) { mComponentName=newVal; }

  QString getSCWrapperName() { return mSCWrapperName; }
  void setSCWrapperName(const QString& newVal) { mSCWrapperName=newVal; }

  ModelTarget getModelTarget() { return mModelTarget; }
  void setModelTarget(ModelTarget newVal) { mModelTarget=newVal; }

  ModelSymbols getModelSymbols() { return mModelSymbols; }
  void setModelSymbols(ModelSymbols newVal) { mModelSymbols=newVal; }

  QDomDocument& getDocRecord() { return mDocRecord; }
  QString normalizeName(const QString& inputName);

  void setARMRoot(const QString& newVal) { mARMRoot=newVal; }
  QString getARMRoot() const { return mARMRoot; }

  ModelKitAnswers* getAnswers() { return mAnswers; }

protected:
  virtual void showEvent(QShowEvent* ev);
  virtual bool event(QEvent* ev);

private:
  Ui::ModelKitUserWizardClass ui;
  KitManifest* mManifest;
  KitUserContext mUserContext;
  ModelKitAnswers* mAnswers;
  WizardMode mMode;
  ActionWhen mWhen;
  QString mComponentName;
  QString mSCWrapperName;
  QDomDocument mDocRecord;
  QDomElement mRecordRoot;
  QDomElement mSettingsRoot;
  QDomElement mConfigsRoot;
  QDomElement mActionsRoot;
  QDomElement mQuestionsRoot;
  QString mARMRoot;
  ModelKit* mModelKit;
  ModelTarget mModelTarget;
  ModelSymbols mModelSymbols;
};

#endif // MODELKITUSERWIZARD_H
