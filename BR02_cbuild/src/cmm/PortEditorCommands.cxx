//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include "PortEditorCommands.h"
#include "CcfgHelper.h"


TreeUndoCommand::TreeUndoCommand(PortEditorTreeItem* item, QUndoCommand* parent)
    : QUndoCommand(parent)
{
  mTree = dynamic_cast<PortEditorTreeWidget*>(item->treeWidget());
  mTreeIndex = mTree->indexFromItem(item);
  mInitialModified = false; // TODO get the inital modified state
}

void TreeUndoCommand::updateTreeIndex(PortEditorTreeItem* item)
{
  mTreeIndex = mTree->indexFromItem(item);
}

PortEditorTreeItem* TreeUndoCommand::getItem() 
{
  QTreeWidgetItem* item = mTree->itemFromIndex(mTreeIndex);
  INFO_ASSERT(item, "Expected to find item");
  return dynamic_cast<PortEditorTreeItem*>(item);
}

// CmdRenameXtorInstance
CmdRenameXtorInstance::CmdRenameXtorInstance(ESLXtorInstanceItem* item,
                                   CarbonCfgXtorInstance* xtorInst, 
                                   const QString& oldName,  const QString& newName,
                                   QUndoCommand* parent)
                                   : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mOldName = oldName;
  mNewName = newName;
  mXtorInstName = xtorInst->getName();
 
  setText(QString("Rename Transactor Instance: %1 from %2 to %3")
    .arg(xtorInst->getName())
    .arg(oldName)
    .arg(newName));
}

void CmdRenameXtorInstance::undo()
{
  CarbonCfgXtorInstance* xtorInst = mCfg->findXtorInstance(mXtorInstName.c_str());
  
  // Change Data Model
  UtString v; v << mOldName;
  mCfg->changeXtorInstanceName(xtorInst, v.c_str());
  mOldName = xtorInst->getName();

  ESLXtorInstanceItem* item = dynamic_cast<ESLXtorInstanceItem*>(getItem());
  item->setInstanceName(mOldName);
  item->treeWidget()->scrollToItem(item);

  // must call updateTreeIndex whenever col 0 text changes
  updateTreeIndex(item);

  mXtorInstName.clear(); mXtorInstName << mOldName;

  getTree()->getEditor()->signalRenameXtorInstance(mNewName, mOldName);

  setModified(false); 
}

void CmdRenameXtorInstance::redo()
{
  CarbonCfgXtorInstance* xtorInst = mCfg->findXtorInstance(mXtorInstName.c_str());

  // Change Data Model
  UtString v; v << mNewName;
  mCfg->changeXtorInstanceName(xtorInst, v.c_str());
  mNewName = xtorInst->getName();
 
  ESLXtorInstanceItem* item = dynamic_cast<ESLXtorInstanceItem*>(getItem());
  item->setInstanceName(mNewName);
  item->treeWidget()->scrollToItem(item);

  // must call updateTreeIndex whenever col 0 text changes
  updateTreeIndex(item);

  mXtorInstName.clear(); mXtorInstName << mNewName;

  // Broadcast the change
  getTree()->getEditor()->signalRenameXtorInstance(mOldName, mNewName);

  setModified(true);
}


// Change ESL Port Mode
CmdChangeESLPortMode::CmdChangeESLPortMode(ESLPortItem* item, CarbonCfg* cfg, 
    CarbonCfgESLPort* eslPort,
    CarbonCfgESLPortMode oldMode, 
    CarbonCfgESLPortMode newMode, QUndoCommand* parent)
    :TreeUndoCommand(item, parent)
{
  mCfg = cfg;
  mOldMode = oldMode;
  mNewMode = newMode;
  mESLPortName = eslPort->getName();
  
  setText(QString("Change ESL Port Mode: %1 %2 to %3")
    .arg(eslPort->getName())
    .arg(oldMode)
    .arg(newMode));
}

void CmdChangeESLPortMode::undo()
{
  CarbonCfgESLPort* eslPort = mCfg->findESLPort(mESLPortName.c_str());
  eslPort->putMode(mOldMode);  
  mOldMode = eslPort->getMode();

  ESLPortItem* item = dynamic_cast<ESLPortItem*>(getItem());
  item->setPortMode(mOldMode);
  item->treeWidget()->scrollToItem(item);

  // must call updateTreeIndex whenever col 0 text changes
  updateTreeIndex(item);
  setModified(false);

}

void CmdChangeESLPortMode::redo()
{
  CarbonCfgESLPort* eslPort = mCfg->findESLPort(mESLPortName.c_str());
  eslPort->putMode(mNewMode);  
  mNewMode = eslPort->getMode();

  ESLPortItem* item = dynamic_cast<ESLPortItem*>(getItem());
  item->setPortMode(mNewMode);
  item->treeWidget()->scrollToItem(item);
  updateTreeIndex(item);

  setModified(true);

}


// Change ESL POrt Typedef
CmdChangeESLPortTypedef::CmdChangeESLPortTypedef(ESLPortItem* item, CarbonCfg* cfg, CarbonCfgESLPort* eslPort, 
                                   const QString& oldValue,  const QString& newValue,
                                   QUndoCommand* parent)
                                   : TreeUndoCommand(item, parent)
{
  mCfg = cfg;

  mOldValue << oldValue;
  mNewValue << newValue;
  
  mESLPortName = eslPort->getName();

  setText(QString("Change ESL Port TypeDef: %1 %2 to %3")
    .arg(eslPort->getName())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeESLPortTypedef::undo()
{
  CarbonCfgESLPort* eslPort = mCfg->findESLPort(mESLPortName.c_str());

  // Change Data Model
  UtString v; v << mOldValue;
  
  eslPort->putTypeDef(v.c_str());
  
  mOldValue.clear();
  mOldValue << eslPort->getTypeDef();

  ESLPortItem* item = dynamic_cast<ESLPortItem*>(getItem());
  item->setPortTypeDef(mOldValue.c_str());
  item->treeWidget()->scrollToItem(item);

  // must call updateTreeIndex whenever col 0 text changes
  updateTreeIndex(item);

  setModified(false);
}

void CmdChangeESLPortTypedef::redo()
{
  CarbonCfgESLPort* eslPort = mCfg->findESLPort(mESLPortName.c_str());

  // Change Data Model
  UtString v; v << mNewValue;
  eslPort->putTypeDef(v.c_str());

  mNewValue.clear();
  mNewValue << eslPort->getTypeDef();

  ESLPortItem* item = dynamic_cast<ESLPortItem*>(getItem());
  item->setPortTypeDef(mNewValue.c_str());
  item->treeWidget()->scrollToItem(item);
  updateTreeIndex(item);

  setModified(true);
}


// Rename ESL Port


CmdRenameESLPort::CmdRenameESLPort(ESLPortItem* item, CarbonCfg* cfg, CarbonCfgESLPort* eslPort, 
                                   const QString& oldName,  const QString& newName,
                                   QUndoCommand* parent)
                                   : TreeUndoCommand(item, parent)
{
  mCfg = cfg;
  mOldName = oldName;
  mNewName = newName;
  mESLPortName = eslPort->getName();

  setText(QString("Rename ESL Port: %1 %2 to %3")
    .arg(eslPort->getName())
    .arg(oldName)
    .arg(newName));
}

void CmdRenameESLPort::undo()
{
  CarbonCfgESLPort* eslPort = mCfg->findESLPort(mESLPortName.c_str());

  // Change Data Model
  UtString v; v << mOldName;
  mCfg->changeESLName(eslPort, v.c_str());
  mOldName = eslPort->getName();

  ESLPortItem* item = dynamic_cast<ESLPortItem*>(getItem());
  item->setPortName(mOldName);
  item->treeWidget()->scrollToItem(item);

  // must call updateTreeIndex whenever col 0 text changes
  updateTreeIndex(item);

  mESLPortName.clear(); mESLPortName << mOldName;

  setModified(false);
}

void CmdRenameESLPort::redo()
{
  CarbonCfgESLPort* eslPort = mCfg->findESLPort(mESLPortName.c_str());

  // Change Data Model
  UtString v; v << mNewName;
  mCfg->changeESLName(eslPort, v.c_str());
  mNewName = eslPort->getName();

  ESLPortItem* item = dynamic_cast<ESLPortItem*>(getItem());
  item->setPortName(mNewName);
  item->treeWidget()->scrollToItem(item);
  updateTreeIndex(item);

  mESLPortName.clear(); mESLPortName << mNewName;

  setModified(true);
}


CmdChangeESLTieValue::CmdChangeESLTieValue(ESLTieValueItem* item, CarbonCfg* cfg, 
                                   CarbonCfgTie* tiePort, 
                                   const DynBitVector* oldValue,  const DynBitVector* newValue,
                                   QUndoCommand* parent)
                                   : TreeUndoCommand(item, parent)
{
  mCfg = cfg;
  mRTLPortName = tiePort->getRTLPort()->getName();
  mOldValue = *oldValue;
  mNewValue = *newValue;

  UtString oldVal; mOldValue.format(&oldVal, eCarbonHex);
  UtString newVal; mNewValue.format(&newVal, eCarbonHex);

  setText(QString("Change Tie Value ESL Port: %1 %2 to %3")
    .arg(tiePort->getRTLPort()->getName())
    .arg(oldVal.c_str())
    .arg(newVal.c_str()));
}

CarbonCfgTie* CmdChangeESLTieValue::findTie(CarbonCfgRTLPort* rtlPort)
{
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgTie* tie = conn->castTie();
    if (tie && tie->getRTLPort() == rtlPort)
    {
      return tie;  
    }
  }
  return NULL;
}

void CmdChangeESLTieValue::undo()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  CarbonCfgTie* tie = findTie(rtlPort);
  if (tie)
    tie->mValue = mOldValue;
  
  UtString valStr;
  mOldValue.format(&valStr, eCarbonHex);

  ESLTieValueItem* item = dynamic_cast<ESLTieValueItem*>(getItem());
  item->setTieValue(QString("0x%1").arg(valStr.c_str()));
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeESLTieValue::redo()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());

  CarbonCfgTie* tie = findTie(rtlPort);
  if (tie)
    tie->mValue = mNewValue;
  
  UtString valStr;
  mNewValue.format(&valStr, eCarbonHex);

  ESLTieValueItem* item = dynamic_cast<ESLTieValueItem*>(getItem());
  item->setTieValue(QString("0x%1").arg(valStr.c_str()));
  item->treeWidget()->scrollToItem(item);
  setModified(true);
}

CmdChangeESLPortExpr::CmdChangeESLPortExpr( ESLPortExprItem* portExprItem, CarbonCfg* cfg, 
                        CarbonCfgESLPort* eslPort, 
                        const QString& oldValue, 
                        const QString& newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(portExprItem, parent)
{
  mCfg = cfg;
  mESLPortName = eslPort->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change ESL Port Expression: %1 %2 to %3")
    .arg(eslPort->getName())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeESLPortExpr::undo()
{
  UtString uESLPortName;
  uESLPortName << mESLPortName;

  CarbonCfgESLPort* eslPort = mCfg->findESLPort(uESLPortName.c_str());

  UtString v; v << mOldValue;
  eslPort->putExpr(v.c_str());
 
  ESLPortExprItem* item = dynamic_cast<ESLPortExprItem*>(getItem());
  item->setPortExpression(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeESLPortExpr::redo()
{
  UtString uESLPortName;
  uESLPortName << mESLPortName;

  CarbonCfgESLPort* eslPort = mCfg->findESLPort(uESLPortName.c_str());
  
  UtString v; v << mNewValue;

  eslPort->putExpr(v.c_str());

  ESLPortExprItem* item = dynamic_cast<ESLPortExprItem*>(getItem());
  item->setPortExpression(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);
}

CarbonCfgResetGen* CmdChangeESLResetGenActive::getResetGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgResetGen* resetGen = conn->castResetGen();
    if (resetGen && resetGen->getRTLPort() == rtlPort)
      return resetGen;
  }

  INFO_ASSERT(true, "Should have found reset gen");
  return NULL;
}
CmdChangeESLResetGenActive::CmdChangeESLResetGenActive(ESLResetGenActiveItem* item,
                        quint64 oldValue, 
                        quint64 newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getResetGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Reset Generator Active: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeESLResetGenActive::undo()
{
  CarbonCfgResetGen* resetGen = getResetGen();
  resetGen->mActiveValue = mOldValue;

  ESLResetGenActiveItem* item = dynamic_cast<ESLResetGenActiveItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeESLResetGenActive::redo()
{
  CarbonCfgResetGen* resetGen = getResetGen();
  resetGen->mActiveValue = mNewValue;

  ESLResetGenActiveItem* item = dynamic_cast<ESLResetGenActiveItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);  
  setModified(true);
}

// Inactive
CarbonCfgResetGen* CmdChangeESLResetGenInactive::getResetGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgResetGen* resetGen = conn->castResetGen();
    if (resetGen && resetGen->getRTLPort() == rtlPort)
      return resetGen;
  }

  INFO_ASSERT(true, "Should have found reset gen");
  return NULL;
}
CmdChangeESLResetGenInactive::CmdChangeESLResetGenInactive(ESLResetGenInactiveItem* item,
                        quint64 oldValue, 
                        quint64 newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getResetGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Reset Generator Inactive: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeESLResetGenInactive::undo()
{
  CarbonCfgResetGen* resetGen = getResetGen();
  resetGen->mInactiveValue = mOldValue;

  ESLResetGenInactiveItem* item = dynamic_cast<ESLResetGenInactiveItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeESLResetGenInactive::redo()
{
  CarbonCfgResetGen* resetGen = getResetGen();
  resetGen->mInactiveValue = mNewValue;

  ESLResetGenInactiveItem* item = dynamic_cast<ESLResetGenInactiveItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);  
  setModified(true);
}

// Cycles Before
CarbonCfgResetGen* CmdChangeESLResetGenCyclesBefore::getResetGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgResetGen* resetGen = conn->castResetGen();
    if (resetGen && resetGen->getRTLPort() == rtlPort)
      return resetGen;
  }

  INFO_ASSERT(true, "Should have found reset gen");
  return NULL;
}
CmdChangeESLResetGenCyclesBefore::CmdChangeESLResetGenCyclesBefore(ESLResetGenCyclesBeforeItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getResetGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Reset Generator Cycles Before: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeESLResetGenCyclesBefore::undo()
{
  CarbonCfgResetGen* resetGen = getResetGen();
  resetGen->mCyclesBefore = mOldValue;

  ESLResetGenCyclesBeforeItem* item = dynamic_cast<ESLResetGenCyclesBeforeItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeESLResetGenCyclesBefore::redo()
{
  CarbonCfgResetGen* resetGen = getResetGen();
  resetGen->mCyclesBefore = mNewValue;

  ESLResetGenCyclesBeforeItem* item = dynamic_cast<ESLResetGenCyclesBeforeItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);  
  setModified(true);
}

// Cycles After
CarbonCfgResetGen* CmdChangeESLResetGenCyclesAfter::getResetGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgResetGen* resetGen = conn->castResetGen();
    if (resetGen && resetGen->getRTLPort() == rtlPort)
      return resetGen;
  }

  INFO_ASSERT(true, "Should have found reset gen");
  return NULL;
}
CmdChangeESLResetGenCyclesAfter::CmdChangeESLResetGenCyclesAfter(ESLResetGenCyclesAfterItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getResetGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Reset Generator Cycles After: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeESLResetGenCyclesAfter::undo()
{
  CarbonCfgResetGen* resetGen = getResetGen();
  resetGen->mCyclesAfter = mOldValue;

  ESLResetGenCyclesAfterItem* item = dynamic_cast<ESLResetGenCyclesAfterItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeESLResetGenCyclesAfter::redo()
{
  CarbonCfgResetGen* resetGen = getResetGen();
  resetGen->mCyclesAfter = mNewValue;

  ESLResetGenCyclesAfterItem* item = dynamic_cast<ESLResetGenCyclesAfterItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);  
  setModified(true);
}

// Cycles Asserted
CarbonCfgResetGen* CmdChangeESLResetGenCyclesAsserted::getResetGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgResetGen* resetGen = conn->castResetGen();
    if (resetGen && resetGen->getRTLPort() == rtlPort)
      return resetGen;
  }

  INFO_ASSERT(true, "Should have found reset gen");
  return NULL;
}
CmdChangeESLResetGenCyclesAsserted::CmdChangeESLResetGenCyclesAsserted(ESLResetGenCyclesAssertedItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getResetGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Reset Generator Cycles Asserted: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));}

void CmdChangeESLResetGenCyclesAsserted::undo()
{
  CarbonCfgResetGen* resetGen = getResetGen();
  resetGen->mCyclesAsserted = mOldValue;

  ESLResetGenCyclesAssertedItem* item = dynamic_cast<ESLResetGenCyclesAssertedItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeESLResetGenCyclesAsserted::redo()
{
  CarbonCfgResetGen* resetGen = getResetGen();
  resetGen->mCyclesAsserted = mNewValue;

  ESLResetGenCyclesAssertedItem* item = dynamic_cast<ESLResetGenCyclesAssertedItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);  
  setModified(true);
}

// Reset Clock Cycles
CarbonCfgResetGen* CmdChangeESLResetFrequencyReset::getResetGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgResetGen* resetGen = conn->castResetGen();
    if (resetGen && resetGen->getRTLPort() == rtlPort)
      return resetGen;
  }

  INFO_ASSERT(true, "Should have found reset gen");
  return NULL;
}
CmdChangeESLResetFrequencyReset::CmdChangeESLResetFrequencyReset(ESLResetGenFrequencyResetItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getResetGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Reset Generator Reset Cycles: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeESLResetFrequencyReset::undo()
{
  CarbonCfgResetGen* resetGen = getResetGen();
  resetGen->mFrequency.putClockCycles(mOldValue);

  ESLResetGenFrequencyResetItem* item = dynamic_cast<ESLResetGenFrequencyResetItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeESLResetFrequencyReset::redo()
{
  CarbonCfgResetGen* resetGen = getResetGen();
  resetGen->mFrequency.putClockCycles(mNewValue);

  ESLResetGenFrequencyResetItem* item = dynamic_cast<ESLResetGenFrequencyResetItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);  
  setModified(true);
}

// Reset Comp Cycles

CarbonCfgResetGen* CmdChangeESLResetFrequencyCycles::getResetGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgResetGen* resetGen = conn->castResetGen();
    if (resetGen && resetGen->getRTLPort() == rtlPort)
      return resetGen;
  }

  INFO_ASSERT(true, "Should have found reset gen");
  return NULL;
}
CmdChangeESLResetFrequencyCycles::CmdChangeESLResetFrequencyCycles(ESLResetGenFrequencyCyclesItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getResetGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Reset Generator Component Cycles: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeESLResetFrequencyCycles::undo()
{
  CarbonCfgResetGen* resetGen = getResetGen();
  resetGen->mFrequency.putCompCycles(mOldValue);

  ESLResetGenFrequencyCyclesItem* item = dynamic_cast<ESLResetGenFrequencyCyclesItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeESLResetFrequencyCycles::redo()
{
  CarbonCfgResetGen* resetGen = getResetGen();
  resetGen->mFrequency.putCompCycles(mNewValue);

  ESLResetGenFrequencyCyclesItem* item = dynamic_cast<ESLResetGenFrequencyCyclesItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);  
  setModified(true);
}

// Initial Value

CarbonCfgClockGen* CmdChangeESLClockGenInitialValue::getClockGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgClockGen* clockGen = conn->castClockGen();
    if (clockGen && clockGen->getRTLPort() == rtlPort)
      return clockGen;
  }

  INFO_ASSERT(true, "Should have found clock gen");
  return NULL;
}

// Clock Gen Initial Value
CmdChangeESLClockGenInitialValue::CmdChangeESLClockGenInitialValue(ESLClockGenInitialValueItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getClockGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Clock Generator Initial Value: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeESLClockGenInitialValue::undo()
{
  CarbonCfgClockGen* clockGen = getClockGen();
  clockGen->mInitialValue = mOldValue;

  ESLClockGenInitialValueItem* item = dynamic_cast<ESLClockGenInitialValueItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeESLClockGenInitialValue::redo()
{
  CarbonCfgClockGen* clockGen = getClockGen();
  clockGen->mInitialValue = mNewValue;

  ESLClockGenInitialValueItem* item = dynamic_cast<ESLClockGenInitialValueItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);
}

// ClockGen Delay

CarbonCfgClockGen* CmdChangeESLClockGenDelay::getClockGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgClockGen* clockGen = conn->castClockGen();
    if (clockGen && clockGen->getRTLPort() == rtlPort)
      return clockGen;
  }

  INFO_ASSERT(true, "Should have found clock gen");
  return NULL;
}

CmdChangeESLClockGenDelay::CmdChangeESLClockGenDelay(ESLClockGenDelayItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getClockGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Clock Generator Delay Value: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));}

void CmdChangeESLClockGenDelay::undo()
{
  CarbonCfgClockGen* clockGen = getClockGen();
  clockGen->mDelay = mOldValue;

  ESLClockGenDelayItem* item = dynamic_cast<ESLClockGenDelayItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeESLClockGenDelay::redo()
{
  CarbonCfgClockGen* clockGen = getClockGen();
  clockGen->mDelay = mNewValue;

  ESLClockGenDelayItem* item = dynamic_cast<ESLClockGenDelayItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);
}

// Clock Gen DutyCycle

CarbonCfgClockGen* CmdChangeESLClockGenDutyCycle::getClockGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgClockGen* clockGen = conn->castClockGen();
    if (clockGen && clockGen->getRTLPort() == rtlPort)
      return clockGen;
  }

  INFO_ASSERT(true, "Should have found clock gen");
  return NULL;
}

CmdChangeESLClockGenDutyCycle::CmdChangeESLClockGenDutyCycle(ESLClockGenDutyCycleItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getClockGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Clock Generator DutyCycle Value: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeESLClockGenDutyCycle::undo()
{
  CarbonCfgClockGen* clockGen = getClockGen();
  clockGen->mDutyCycle = mOldValue;

  ESLClockGenDutyCycleItem* item = dynamic_cast<ESLClockGenDutyCycleItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeESLClockGenDutyCycle::redo()
{
  CarbonCfgClockGen* clockGen = getClockGen();
  clockGen->mDutyCycle = mNewValue;

  ESLClockGenDutyCycleItem* item = dynamic_cast<ESLClockGenDutyCycleItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);
}

// Clock Gen Frequency Comp Cycles

CarbonCfgClockGen* CmdChangeESLClockGenFrequencyCompCycles::getClockGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgClockGen* clockGen = conn->castClockGen();
    if (clockGen && clockGen->getRTLPort() == rtlPort)
      return clockGen;
  }

  INFO_ASSERT(true, "Should have found clock gen");
  return NULL;
}

CmdChangeESLClockGenFrequencyCompCycles::CmdChangeESLClockGenFrequencyCompCycles(
                        ESLClockGenFrequencyCompCycles* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getClockGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Clock Generator Frequency Component Cycles: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeESLClockGenFrequencyCompCycles::undo()
{  
  CarbonCfgClockGen* clockGen = getClockGen();
  clockGen->mFrequency.putCompCycles(mOldValue);

  ESLClockGenFrequencyCompCycles* item = dynamic_cast<ESLClockGenFrequencyCompCycles*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeESLClockGenFrequencyCompCycles::redo()
{
  CarbonCfgClockGen* clockGen = getClockGen();
  clockGen->mFrequency.putCompCycles(mNewValue);

  ESLClockGenFrequencyCompCycles* item = dynamic_cast<ESLClockGenFrequencyCompCycles*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);
}


// Clock Gen Frequency Clock Cycles
CarbonCfgClockGen* CmdChangeESLClockGenFrequencyClockCycles::getClockGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgClockGen* clockGen = conn->castClockGen();
    if (clockGen && clockGen->getRTLPort() == rtlPort)
      return clockGen;
  }

  INFO_ASSERT(true, "Should have found clock gen");
  return NULL;
}

CmdChangeESLClockGenFrequencyClockCycles::CmdChangeESLClockGenFrequencyClockCycles(
                        ESLClockGenFrequencyClockCycles* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getClockGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Clock Generator Frequency Clock Cycles: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeESLClockGenFrequencyClockCycles::undo()
{  
  CarbonCfgClockGen* clockGen = getClockGen();
  clockGen->mFrequency.putClockCycles(mOldValue);

  ESLClockGenFrequencyClockCycles* item = dynamic_cast<ESLClockGenFrequencyClockCycles*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeESLClockGenFrequencyClockCycles::redo()
{
  CarbonCfgClockGen* clockGen = getClockGen();
  clockGen->mFrequency.putClockCycles(mNewValue);

  ESLClockGenFrequencyClockCycles* item = dynamic_cast<ESLClockGenFrequencyClockCycles*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);
}

// Parameter Value Changes

// Bool
CmdChangeParamValueBool::CmdChangeParamValueBool(ParameterValueItem* item,
                        bool oldValue, 
                        bool newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mParamName = item->getParamInst()->getParam()->getName();
  if (item->getType() == ParameterValueItem::eTypeXTOR_PARAM)
    mParamInstName = item->getParamInst()->getInstance()->getName();
  mParamType = item->getType();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Parameter %1 Boolean %2 to %3")
    .arg(mParamName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

CarbonCfgXtorParamInst* CmdChangeParamValueBool::getParamInst()
{
  switch(mParamType)
  {
  case ParameterValueItem::eTypeXTOR_PARAM:
    {
      CarbonCfgXtorInstance* xtorInst = mCfg->findXtorInstance(mParamInstName.c_str());
      INFO_ASSERT(xtorInst, "Expecting instance");
      CarbonCfgXtorParamInst* xtorParamInst = xtorInst->findParameterInst(mParamName.c_str());
      INFO_ASSERT(xtorParamInst, "Expecting parameter instance");
      return xtorParamInst;
    }
    break;
  case ParameterValueItem::eTypeGLOBAL_PARAM:
    {
      CcfgHelper ccfg(mCfg);
      CarbonCfgXtorParamInst* xtorParamInst = ccfg.findParameter(mParamName.c_str());
      INFO_ASSERT(xtorParamInst, "Expecting param inst");
      return xtorParamInst;
    }
    break;
  default:
    INFO_ASSERT(true, "Unhandled Parameter Type");
    break;
  }

  return NULL;
}

void CmdChangeParamValueBool::undo()
{
  CarbonCfgXtorParamInst* xtorParamInst = getParamInst();

  if (mParamType == ParameterValueItem::eTypeGLOBAL_PARAM)
    xtorParamInst->getParam()->putDefaultValue(mOldValue ? "true" : "false");
  else
    xtorParamInst->putValue(mOldValue ? "true" : "false");

  ParameterValueItem* item = dynamic_cast<ParameterValueItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeParamValueBool::redo()
{
  CarbonCfgXtorParamInst* xtorParamInst = getParamInst();

  if (mParamType == ParameterValueItem::eTypeGLOBAL_PARAM)
    xtorParamInst->getParam()->putDefaultValue(mNewValue ? "true" : "false");
  else
    xtorParamInst->putValue(mNewValue ? "true" : "false");

  ParameterValueItem* item = dynamic_cast<ParameterValueItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);
}

// UInt32
CmdChangeParamValueUInt32::CmdChangeParamValueUInt32(ParameterValueItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mParamName = item->getParamInst()->getParam()->getName();
  if (item->getType() == ParameterValueItem::eTypeXTOR_PARAM)
    mParamInstName = item->getParamInst()->getInstance()->getName();
  mParamType = item->getType();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Parameter %1 UInt %2 to %3")
    .arg(mParamName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

CarbonCfgXtorParamInst* CmdChangeParamValueUInt32::getParamInst()
{
  switch(mParamType)
  {
  case ParameterValueItem::eTypeXTOR_PARAM:
    {
      CarbonCfgXtorInstance* xtorInst = mCfg->findXtorInstance(mParamInstName.c_str());
      INFO_ASSERT(xtorInst, "Expecting instance");
      CarbonCfgXtorParamInst* xtorParamInst = xtorInst->findParameterInst(mParamName.c_str());
      INFO_ASSERT(xtorParamInst, "Expecting parameter instance");
      return xtorParamInst;
    }
    break;
  case ParameterValueItem::eTypeGLOBAL_PARAM:
    {
      CcfgHelper ccfg(mCfg);
      CarbonCfgXtorParamInst* xtorParamInst = ccfg.findParameter(mParamName.c_str());
      INFO_ASSERT(xtorParamInst, "Expecting param inst");
      return xtorParamInst;
    }
    break;
  default:
    INFO_ASSERT(true, "Unhandled Parameter Type");
    break;
  }

  return NULL;
}

void CmdChangeParamValueUInt32::undo()
{
  UtString v; v << mOldValue;
  CarbonCfgXtorParamInst* xtorParamInst = getParamInst();

  if (mParamType == ParameterValueItem::eTypeGLOBAL_PARAM)
    xtorParamInst->getParam()->putDefaultValue(v.c_str());
  else
    xtorParamInst->putValue(v.c_str());

  ParameterValueItem* item = dynamic_cast<ParameterValueItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeParamValueUInt32::redo()
{
  UtString v; v << mNewValue;
  CarbonCfgXtorParamInst* xtorParamInst = getParamInst();
  
  if (mParamType == ParameterValueItem::eTypeGLOBAL_PARAM)
    xtorParamInst->getParam()->putDefaultValue(v.c_str());
  else
    xtorParamInst->putValue(v.c_str());

  ParameterValueItem* item = dynamic_cast<ParameterValueItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);  
}

// UInt64
CmdChangeParamValueUInt64::CmdChangeParamValueUInt64(ParameterValueItem* item,
                                                     quint64 oldValue, 
                                                     quint64 newValue,
                                                     QUndoCommand* parent)
  : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mParamName = item->getParamInst()->getParam()->getName();
  if (item->getType() == ParameterValueItem::eTypeXTOR_PARAM)
    mParamInstName = item->getParamInst()->getInstance()->getName();
  mParamType = item->getType();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Parameter %1 UInt %2 to %3")
          .arg(mParamName.c_str())
          .arg(oldValue)
          .arg(newValue));
}

CarbonCfgXtorParamInst* CmdChangeParamValueUInt64::getParamInst()
{
  switch(mParamType)
  {
    case ParameterValueItem::eTypeXTOR_PARAM:
    {
      CarbonCfgXtorInstance* xtorInst = mCfg->findXtorInstance(mParamInstName.c_str());
      INFO_ASSERT(xtorInst, "Expecting instance");
      CarbonCfgXtorParamInst* xtorParamInst = xtorInst->findParameterInst(mParamName.c_str());
      INFO_ASSERT(xtorParamInst, "Expecting parameter instance");
      return xtorParamInst;
    }
    break;
    case ParameterValueItem::eTypeGLOBAL_PARAM:
    {
      CcfgHelper ccfg(mCfg);
      CarbonCfgXtorParamInst* xtorParamInst = ccfg.findParameter(mParamName.c_str());
      INFO_ASSERT(xtorParamInst, "Expecting param inst");
      return xtorParamInst;
    }
    break;
    default:
      INFO_ASSERT(true, "Unhandled Parameter Type");
      break;
  }

  return NULL;
}

void CmdChangeParamValueUInt64::undo()
{
  UtString v; v << mOldValue;
  CarbonCfgXtorParamInst* xtorParamInst = getParamInst();

  if (mParamType == ParameterValueItem::eTypeGLOBAL_PARAM)
    xtorParamInst->getParam()->putDefaultValue(v.c_str());
  else
    xtorParamInst->putValue(v.c_str());

  ParameterValueItem* item = dynamic_cast<ParameterValueItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeParamValueUInt64::redo()
{
  UtString v; v << mNewValue;
  CarbonCfgXtorParamInst* xtorParamInst = getParamInst();
  
  if (mParamType == ParameterValueItem::eTypeGLOBAL_PARAM)
    xtorParamInst->getParam()->putDefaultValue(v.c_str());
  else
    xtorParamInst->putValue(v.c_str());

  ParameterValueItem* item = dynamic_cast<ParameterValueItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);  
}

// Double

CmdChangeParamValueDouble::CmdChangeParamValueDouble(ParameterValueItem* item,
                        double oldValue, 
                        double newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mParamName = item->getParamInst()->getParam()->getName();
  if (item->getType() == ParameterValueItem::eTypeXTOR_PARAM)
    mParamInstName = item->getParamInst()->getInstance()->getName();
  mParamType = item->getType();
  mOldValue = oldValue;
  mNewValue = newValue;

   setText(QString("Change Parameter %1 Double %2 to %3")
    .arg(mParamName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

CarbonCfgXtorParamInst* CmdChangeParamValueDouble::getParamInst()
{
  switch(mParamType)
  {
  case ParameterValueItem::eTypeXTOR_PARAM:
    {
      CarbonCfgXtorInstance* xtorInst = mCfg->findXtorInstance(mParamInstName.c_str());
      INFO_ASSERT(xtorInst, "Expecting instance");
      CarbonCfgXtorParamInst* xtorParamInst = xtorInst->findParameterInst(mParamName.c_str());
      INFO_ASSERT(xtorParamInst, "Expecting parameter instance");
      return xtorParamInst;
    }
    break;
  case ParameterValueItem::eTypeGLOBAL_PARAM:
    {
      CcfgHelper ccfg(mCfg);
      CarbonCfgXtorParamInst* xtorParamInst = ccfg.findParameter(mParamName.c_str());
      INFO_ASSERT(xtorParamInst, "Expecting param inst");
      return xtorParamInst;
    }
    break;
  default:
    INFO_ASSERT(true, "Unhandled Parameter Type");
    break;
  }

  return NULL;
}

void CmdChangeParamValueDouble::undo()
{
  UtString v; v << mOldValue;
  CarbonCfgXtorParamInst* xtorParamInst = getParamInst();
  
  if (mParamType == ParameterValueItem::eTypeGLOBAL_PARAM)
    xtorParamInst->getParam()->putDefaultValue(v.c_str());
  else
    xtorParamInst->putValue(v.c_str());

  ParameterValueItem* item = dynamic_cast<ParameterValueItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeParamValueDouble::redo()
{
  UtString v; v << mNewValue;
  CarbonCfgXtorParamInst* xtorParamInst = getParamInst();

  if (mParamType == ParameterValueItem::eTypeGLOBAL_PARAM)
    xtorParamInst->getParam()->putDefaultValue(v.c_str());
  else
    xtorParamInst->putValue(v.c_str());

  ParameterValueItem* item = dynamic_cast<ParameterValueItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);  
}

// String
CmdChangeParamValueString::CmdChangeParamValueString(ParameterValueItem* item,
                        const QString& oldValue, 
                        const QString& newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mParamName = item->getParamInst()->getParam()->getName();
  if (item->getType() == ParameterValueItem::eTypeXTOR_PARAM)
    mParamInstName = item->getParamInst()->getInstance()->getName();
  mParamType = item->getType();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change Parameter %1 String %2 to %3")
    .arg(mParamName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

CarbonCfgXtorParamInst* CmdChangeParamValueString::getParamInst()
{
  switch(mParamType)
  {
  case ParameterValueItem::eTypeXTOR_PARAM:
    {
      CarbonCfgXtorInstance* xtorInst = mCfg->findXtorInstance(mParamInstName.c_str());
      INFO_ASSERT(xtorInst, "Expecting instance");
      CarbonCfgXtorParamInst* xtorParamInst = xtorInst->findParameterInst(mParamName.c_str());
      INFO_ASSERT(xtorParamInst, "Expecting parameter instance");
      return xtorParamInst;
    }
    break;
  case ParameterValueItem::eTypeGLOBAL_PARAM:
    {
      CcfgHelper ccfg(mCfg);
      CarbonCfgXtorParamInst* xtorParamInst = ccfg.findParameter(mParamName.c_str());
      INFO_ASSERT(xtorParamInst, "Expecting param inst");
      return xtorParamInst;
    }
    break;
  default:
    INFO_ASSERT(true, "Unhandled Parameter Type");
    break;
  }

  return NULL;
}

void CmdChangeParamValueString::undo()
{
  UtString v; v << mOldValue;
  CarbonCfgXtorParamInst* xtorParamInst = getParamInst();

  if (mParamType == ParameterValueItem::eTypeGLOBAL_PARAM)
    xtorParamInst->getParam()->putDefaultValue(v.c_str());
  else
    xtorParamInst->putValue(v.c_str());

  ParameterValueItem* item = dynamic_cast<ParameterValueItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeParamValueString::redo()
{
  UtString v; v << mNewValue;
  CarbonCfgXtorParamInst* xtorParamInst = getParamInst();

  if (mParamType == ParameterValueItem::eTypeGLOBAL_PARAM)
    xtorParamInst->getParam()->putDefaultValue(v.c_str());
  else
    xtorParamInst->putValue(v.c_str());

  ParameterValueItem* item = dynamic_cast<ParameterValueItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);  
}
CmdDeleteItems::CmdDeleteItems(QList<PortEditorTreeItem*> items, QUndoCommand* parent)
: QUndoCommand(parent)
{
  setText(QString("Delete %1 Item(s)").arg(items.count()));

  foreach(PortEditorTreeItem* item, items)
  {
    UndoData* undoData = item->deleteItem();
    if (undoData)
      mUndoDataList.append(undoData);
  }
}

void CmdDeleteItems::undo()
{
  foreach (UndoData* undoData, mUndoDataList)
  {
    if (undoData)
      undoData->undo();  
  }
}
void CmdDeleteItems::redo()
{
  foreach (UndoData* undoData, mUndoDataList)
  {
    if (undoData)
      undoData->redo();  
  }
}

// SystemC Clock

CarbonCfgSystemCClock* CmdChangeSystemCClockGenInitialValue::getClockGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgSystemCClock* clockGen = conn->castSystemCClock();
    if (clockGen && clockGen->getRTLPort() == rtlPort)
      return clockGen;
  }

  INFO_ASSERT(true, "Should have found clock gen");
  return NULL;
}


CarbonCfgSystemCClock* CmdChangeSystemCClockGenDutyCycle::getClockGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgSystemCClock* clockGen = conn->castSystemCClock();
    if (clockGen && clockGen->getRTLPort() == rtlPort)
      return clockGen;
  }

  INFO_ASSERT(true, "Should have found clock gen");
  return NULL;
}

CarbonCfgSystemCClock* CmdChangeSystemCClockGenPeriod::getClockGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgSystemCClock* clockGen = conn->castSystemCClock();
    if (clockGen && clockGen->getRTLPort() == rtlPort)
      return clockGen;
  }

  INFO_ASSERT(true, "Should have found clock gen");
  return NULL;
}

CarbonCfgSystemCClock* CmdChangeSystemCClockGenPeriodTimeUnits::getClockGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgSystemCClock* clockGen = conn->castSystemCClock();
    if (clockGen && clockGen->getRTLPort() == rtlPort)
      return clockGen;
  }

  INFO_ASSERT(true, "Should have found clock gen");
  return NULL;
}

CarbonCfgSystemCClock* CmdChangeSystemCClockGenStartTime::getClockGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgSystemCClock* clockGen = conn->castSystemCClock();
    if (clockGen && clockGen->getRTLPort() == rtlPort)
      return clockGen;
  }

  INFO_ASSERT(true, "Should have found clock gen");
  return NULL;
}

CarbonCfgSystemCClock* CmdChangeSystemCClockGenStartTimeUnits::getClockGen()
{
  CarbonCfgRTLPort* rtlPort = mCfg->findRTLPort(mRTLPortName.c_str());
  for (UInt32 i=0; i<rtlPort->numConnections(); i++)
  {
    CarbonCfgRTLConnection* conn = rtlPort->getConnection(i);
    CarbonCfgSystemCClock* clockGen = conn->castSystemCClock();
    if (clockGen && clockGen->getRTLPort() == rtlPort)
      return clockGen;
  }

  INFO_ASSERT(true, "Should have found clock gen");
  return NULL;
}



// Clock Gen Initial Value
CmdChangeSystemCClockGenInitialValue::CmdChangeSystemCClockGenInitialValue(SystemCClockGenInitialValueItem* item,
                        quint32 oldValue, 
                        quint32 newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getClockGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change SystemC Clock Generator Initial Value: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeSystemCClockGenInitialValue::undo()
{
  CarbonCfgSystemCClock* clockGen = getClockGen();
  clockGen->putInitialValue(mOldValue);

  SystemCClockGenInitialValueItem* item = dynamic_cast<SystemCClockGenInitialValueItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeSystemCClockGenInitialValue::redo()
{
  CarbonCfgSystemCClock* clockGen = getClockGen();
  clockGen->putInitialValue(mNewValue);

  SystemCClockGenInitialValueItem* item = dynamic_cast<SystemCClockGenInitialValueItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);
}

CmdChangeSystemCClockGenDutyCycle::CmdChangeSystemCClockGenDutyCycle(SystemCClockGenDutyCycleItem* item,
                        double oldValue, 
                        double newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getClockGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change SystemC Clock Generator DutyCycle Value: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeSystemCClockGenDutyCycle::undo()
{
  CarbonCfgSystemCClock* clockGen = getClockGen();
  clockGen->putDutyCycle(mOldValue);

  SystemCClockGenDutyCycleItem* item = dynamic_cast<SystemCClockGenDutyCycleItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeSystemCClockGenDutyCycle::redo()
{
  CarbonCfgSystemCClock* clockGen = getClockGen();
  clockGen->putDutyCycle(mNewValue);

  SystemCClockGenDutyCycleItem* item = dynamic_cast<SystemCClockGenDutyCycleItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);
}

// Period

CmdChangeSystemCClockGenPeriod::CmdChangeSystemCClockGenPeriod(SystemCClockGenPeriodItem* item,
                        double oldValue, 
                        double newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getClockGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change SystemC Clock Generator Period Value: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeSystemCClockGenPeriod::undo()
{
  CarbonCfgSystemCClock* clockGen = getClockGen();
  clockGen->putPeriod(mOldValue, clockGen->getPeriodUnits());

  SystemCClockGenPeriodItem* item = dynamic_cast<SystemCClockGenPeriodItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeSystemCClockGenPeriod::redo()
{
  CarbonCfgSystemCClock* clockGen = getClockGen();
  clockGen->putPeriod(mNewValue, clockGen->getPeriodUnits());

  SystemCClockGenPeriodItem* item = dynamic_cast<SystemCClockGenPeriodItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);
}



// StartTime


CmdChangeSystemCClockGenStartTime::CmdChangeSystemCClockGenStartTime(SystemCClockGenStartTimeItem* item,
                        double oldValue, 
                        double newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getClockGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change SystemC Clock Generator StartTime Value: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeSystemCClockGenStartTime::undo()
{
  CarbonCfgSystemCClock* clockGen = getClockGen();
  clockGen->putStartTime(mOldValue, clockGen->getStartTimeUnits());

  SystemCClockGenStartTimeItem* item = dynamic_cast<SystemCClockGenStartTimeItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeSystemCClockGenStartTime::redo()
{
  CarbonCfgSystemCClock* clockGen = getClockGen();
  clockGen->putStartTime(mNewValue, clockGen->getStartTimeUnits());

  SystemCClockGenStartTimeItem* item = dynamic_cast<SystemCClockGenStartTimeItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);
}



// StartTime Units

CmdChangeSystemCClockGenStartTimeUnits::CmdChangeSystemCClockGenStartTimeUnits(SystemCClockGenStartTimeUnitsItem* item,
                        CarbonCfgSystemCTimeUnits oldValue, 
                        CarbonCfgSystemCTimeUnits newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getClockGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change SystemC Clock Generator StartTime Units Value: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeSystemCClockGenStartTimeUnits::undo()
{
  CarbonCfgSystemCClock* clockGen = getClockGen();
  clockGen->putStartTime(clockGen->getStartTime(),mOldValue);

  SystemCClockGenStartTimeUnitsItem* item = dynamic_cast<SystemCClockGenStartTimeUnitsItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeSystemCClockGenStartTimeUnits::redo()
{
  CarbonCfgSystemCClock* clockGen = getClockGen();
  clockGen->putStartTime(clockGen->getStartTime(),mNewValue);

  SystemCClockGenStartTimeUnitsItem* item = dynamic_cast<SystemCClockGenStartTimeUnitsItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);
}


// PeriodTime Units

CmdChangeSystemCClockGenPeriodTimeUnits::CmdChangeSystemCClockGenPeriodTimeUnits(SystemCClockGenPeriodUnitsItem* item,
                        CarbonCfgSystemCTimeUnits oldValue, 
                        CarbonCfgSystemCTimeUnits newValue,
                        QUndoCommand* parent)
                        : TreeUndoCommand(item, parent)
{
  mCfg = item->getCcfg();
  mRTLPortName = item->getClockGen()->getRTLPort()->getName();
  mOldValue = oldValue;
  mNewValue = newValue;

  setText(QString("Change SystemC Clock Generator PeriodTime Units Value: %1 %2 to %3")
    .arg(mRTLPortName.c_str())
    .arg(oldValue)
    .arg(newValue));
}

void CmdChangeSystemCClockGenPeriodTimeUnits::undo()
{
  CarbonCfgSystemCClock* clockGen = getClockGen();
  clockGen->putPeriod(clockGen->getPeriod(), mOldValue);

  SystemCClockGenPeriodUnitsItem* item = dynamic_cast<SystemCClockGenPeriodUnitsItem*>(getItem());
  item->setValue(mOldValue);
  item->treeWidget()->scrollToItem(item);
  setModified(false);
}

void CmdChangeSystemCClockGenPeriodTimeUnits::redo()
{
  CarbonCfgSystemCClock* clockGen = getClockGen();
  clockGen->putPeriod(clockGen->getPeriod(),mNewValue);

  SystemCClockGenPeriodUnitsItem* item = dynamic_cast<SystemCClockGenPeriodUnitsItem*>(getItem());
  item->setValue(mNewValue);
  item->treeWidget()->scrollToItem(item);
  setModified(true);
}







