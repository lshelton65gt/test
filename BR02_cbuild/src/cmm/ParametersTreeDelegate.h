#ifndef __PTREEDELEGATE_H__
#define __PTREEDELEGATE_H__

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QtGui>

class ParametersTree;

class ParametersTreeDelegate : public QItemDelegate
{
  Q_OBJECT
public:
  ParametersTreeDelegate(QObject *parent = 0, ParametersTree* treeWidget =0);
  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &,
    const QModelIndex &index) const;
  void setEditorData(QWidget *editor, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QAbstractItemModel *model,
    const QModelIndex &index) const;

  void commit(QWidget* editor)
  {
    emit commitData(editor);
    emit closeEditor(editor);
  }

private slots:
  void currentIndexChanged(int);
  void commitAndCloseEditor();

private:
  ParametersTree* mParameterTree;
};

#endif
