#ifndef __CARBONCONSOLE_H__
#define __CARBONCONSOLE_H__

#include "util/CarbonPlatform.h"

#include <QWidget>
#include <QProcess>

#include "util/UtString.h"
#include "util/XmlParsing.h"
#include "util/CarbonAssert.h"

#include "Scripting.h"
#include "ShellConsole.h"
#include "SSHConsole.h"

class JavaScriptAPIs;
class CarbonProjectWidget;
class CarbonProject;
class CarbonProperty;
class ErrorMessage;
class MessageExpression;

// File: CarbonConsole
//
// The Console application of Carbon Model Studio
// which controls the execution and output of running
// programs.
//
class CarbonTextEdit : public QTextEdit
{
  Q_OBJECT

public:
  CarbonTextEdit(QWidget* parent=0) : QTextEdit(parent) {}
  virtual void mouseDoubleClickEvent(QMouseEvent* e)
  {
    emit mouseDoubleClicked(e);
    QTextEdit::mouseDoubleClickEvent(e);
  }

signals:
  void mouseDoubleClicked(QMouseEvent*);
};

#include "ui_CarbonConsole.h"

//
// Class: CarbonConsole
//
class CarbonConsole : public QWidget
{
  Q_OBJECT
  Q_CLASSINFO("EnumPrefix", "CarbonConsole");

public:
  enum CommandType { Command, Compilation, Check, Clean, ReBuild, BatchCompilation, Package};
  enum CommandMode { Local, Remote };
  enum CommandFlags { Program, Simulate };

  Q_ENUMS(CommandType);
  Q_ENUMS(CommandMode);
  Q_ENUMS(CommandFlags);

  CarbonConsole(QWidget *parent = 0);
  ~CarbonConsole();
  void setProjectWidget(CarbonProjectWidget* pw) { mProjectWidget=pw; }
  void executeCommand(CommandMode commandMode, CommandType commandType, QString program, QStringList args, QString workingDir, CommandFlags flags=Program, bool batchMode=false);
  int executeCommandAndWait(CommandMode commandMode, CommandType commandType, QString program, QStringList args, QString workingDir, CommandFlags flags=Program, bool batchMode=false);
  void processOutput(CommandMode mode, CommandType ctype, const QString& outputLine);
  void processBuffer(CommandType ctype, const char* buffer);
  virtual void closeEvent(QCloseEvent *event);
  void putUseProject(bool val) { mUseProject = val; }
  void setShuttingDown();
  bool isRemoteShellConnected();
  int executeLocalWait(QString program, QStringList args, QString workingDir);
  int getExitStatus() { return mExitCode; }
  void setCredentials(const char* server, const char* user)
  {
    mAuthServer = server;
    mAuthUser = user;
  }
  bool hasActiveProcess()
  {
    // really only valid when in non-remote mode
    return true;
  }
  void signalBeginCompilation() { emit compilationStarted( Local, Compilation ); }

  bool hasExtraProcess()
  {
    return false;
  }
  void executeWindowsMake();

public:
  static void registerTypes(QScriptEngine* engine);
  static void registerScriptTypes(QMap<QString, const QMetaObject*>& map);
  static void registerIntellisense(JavaScriptAPIs* apis);

public slots:
  void clear() { ui.textEdit->clear(); }
  void stopCompilation();
  void appendLine(const QString& line);

protected:
  void executeRemoteCommand(CommandType ctype, QString prog, QStringList args, QString workingDir, CommandFlags flags, bool batchMode);
  void executeLocalCommand(CommandType ctype, QString prog, QStringList args, QString workingDir, CommandFlags flags, bool batchMode);

private:
  void connectProject();
  void addSimulationVariables(CommandFlags flags, QStringList& envVars);
  void killProcessAndSubprocesses(int pid);
  bool readMessageDefinitions(const char* xmlFile);
  bool parseXML(xmlNodePtr parent, UtXmlErrorHandler* eh);

//
// Signals:
//
signals:
//
// Function: signal stdoutChanged(ctype, text)
//
// This signal is emitted every time the console outputs a new
// line of stdout
//
// Parameters:
//   CommandType ctype - Command Type
//   String text - The output line
//
  void stdoutChanged(CarbonConsole::CommandType ctype, const QString& text, const QString& workingDir);
  void stdoutChanged(CarbonConsole::CommandType ctype, const QString& text);

// Not exposed
  void stderrChanged(CarbonConsole::CommandType ctype, const QString& text, const QString& workingDir);
//
// Function: signal compilationStarted(cmode, ctype)
//
// This signal is emitted every time the console begins
// a new compilation
//
// Parameters:
//   CommandMode cmode - Command Mode
//   CommandType ctype - Command Type
//
  void compilationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
//
// Function: signal compilationEnded(cmode, ctype)
//
// This signal is emitted when the compilation has finished.
//
// Parameters:
//   CommandMode cmode - Command Mode
//   CommandType ctype - Command Type
//
  void compilationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType);
//
// Function: signal makefileFinished(cmode, ctype)
//
// This signal is emitted when the makefile completes.
//
// Parameters:
//   CommandMode cmode - Command Mode
//   CommandType ctype - Command Type
//
  void makefileFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void modelCompilationFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void simulationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void simulationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType);
  void exploreFinished(CarbonConsole::CommandMode, CarbonConsole::CommandType);
//
// Function: signal commandCompleted(cmode, ctype, exitCode)
//
// This signal is emitted when the makefile completes.
//
// Parameters:
//   CommandMode cmode - Command Mode
//   CommandType ctype - Command Type
//   int exitCode - Exit status
//
  void commandCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType, int);
  void processTerminated(CarbonConsole::CommandMode, CarbonConsole::CommandType, int);

private slots:
  void remoteSessionLoginStatus(const QString&);
  void remoteProcessOutput(const QString&);
  void remoteCommandCompleted(int);
  void remoteSessionStateChanged(SSHConsole::LoginState);
  void remoteSendCommand();
  void remoteProcessTerminated(int);

  void localSessionLoginStatus(const QString&);
  void localProcessOutput(const QString&);
  void localCommandCompleted(int);
  void localSessionStateChanged(SSHConsole::LoginState);
  void localSendCommand();
  void localProcessTerminated(int);

  void commandWaitCompleted(CarbonConsole::CommandMode, CarbonConsole::CommandType, int);

  // Project Info
  void projectLoaded(const char*, CarbonProject*);
  void remoteInfoChanged(const CarbonProperty* sw, const char* value);
  void configurationChanged(const char* name);

  // Buttons
  void on_toolButtonClearAll_clicked();

  // Text Editor
  void mouseTextDoubleClicked(QMouseEvent*);

private:
  Ui::CarbonConsoleClass ui;

private:
  CarbonProjectWidget* mProjectWidget;
  bool mUseProject;

  CommandMode mCommandMode;

  QString mRemoteProgram;
  QStringList mRemoteProgramArgs;
  QString mRemoteWorkingDir;
  CommandType mRemoteCommandType;
  CommandFlags mRemoteCommandFlags;
  bool mRemoteBatchMode;
  
  QString mLocalProgram;
  QStringList mLocalProgramArgs;
  QString mLocalWorkingDir;
  CommandType mLocalCommandType;
  CommandFlags mLocalCommandFlags;
  bool mLocalBatchMode;

  ShellConsole mShellConsole;
  SSHConsole mSSHConsole;

  QRegExp mStatusExp;
  bool mVersionMatched;
  bool mVersionCheckCompleted;
  
  bool mInitialized;
  bool mShutdown;

  UtString mAuthUser;
  UtString mAuthServer;

  QList<MessageExpression*> mExpressions;
  QList<ErrorMessage*> mErrorMessages;

  bool mCommandFinished;
  int mExitCode;
  bool mCarbonModelWasCompiled;
};

Q_DECLARE_METATYPE(CarbonConsole::CommandType);
Q_DECLARE_METATYPE(CarbonConsole::CommandMode);
Q_DECLARE_METATYPE(CarbonConsole::CommandFlags);

#endif
