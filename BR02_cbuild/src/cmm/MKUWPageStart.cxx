//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "ModelKitUserWizard.h"
#include "ModelKitWizard.h"

MKUWPageStart::MKUWPageStart(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);

  mIsValid = true;

  registerField("armKitRoot*", ui.lineEditArmRootDir);

  setTitle("Carbon Model Kit");
  setSubTitle("Specify the location of your unmodified ARM distribution for this model.");
}

MKUWPageStart::~MKUWPageStart()
{
}

#define SETTING_ARM_ROOT "mkuwPageStart/ArmRoot"

void MKUWPageStart::saveSettings()
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());

  QString armKitRoot = wizard()->field("armKitRoot").toString();
  mSettings.setValue(SETTING_ARM_ROOT, armKitRoot);  

  wiz->recordSetting(this, "ARMRoot", armKitRoot);

}
void MKUWPageStart::restoreSettings()
{
  ui.lineEditArmRootDir->setText(mSettings.value(SETTING_ARM_ROOT, wizard()->field("armKitRoot").toString()).toString());
}

bool MKUWPageStart::validatePage()
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();
  
  QString armKitRoot = wizard()->field("armKitRoot").toString();

  wiz->setARMRoot(armKitRoot);

  QString kitPath = proj->getWindowsEquivalentPath(armKitRoot);
  QDir dir(kitPath);
  if (!dir.exists())
  {
    QString msg = QString("Directory %1 is invalid or does not exist, please verify.").arg(ui.lineEditArmRootDir->text());
    qDebug() << "ERROR" << msg;
    QMessageBox::critical(NULL, "Error", msg);
    return false;
  }
  
  QApplication::setOverrideCursor(Qt::WaitCursor);
  if (!checkHashes())
  {
    QApplication::restoreOverrideCursor();
    return false;
  }
  QApplication::restoreOverrideCursor();

  if (mIsValid)
    saveSettings();

  return mIsValid;
}

int MKUWPageStart::nextId() const
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  KitManifest* manifest = wiz->getManifest();

  if (manifest->getCopyFiles())
    return ModelKitUserWizard::PageClone;

  if (manifest->getPreUserActions().count() > 0)
    return ModelKitUserWizard::PageUserActions;

  return ModelKitUserWizard::PageFinished;
}

bool MKUWPageStart::checkKitHash(KitFile* kitFile, RTLFileOptions* rfu)
{
  if (kitFile->getCheckHash() == false)
  {
    ui.textBrowser->append(QString("File OK %1").arg(kitFile->getNormalizedName()));
    return true;
  }

  CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
  CarbonProject* proj = pw->project();

  UtString normalizedName;
  normalizedName << kitFile->getNormalizedName();

  UtString expandedName;
  UtString err;
  if (OSExpandFilename(&expandedName, normalizedName.c_str(), &err))
  {
    QString hash = kitFile->getHash();
    qDebug() << "checking hash for file" << kitFile->getNormalizedName() << "expecting" << hash;

    QString localName = proj->getWindowsEquivalentPath(expandedName.c_str());

    QFileInfo fi(localName);
    if (fi.exists())
    {
      QString newHash = proj->hashFile(localName);
      if (hash != newHash)
      {
        QString msg = QString("Error: mismatching hash on %1").arg(normalizedName.c_str());
        QMessageBox::critical(NULL, "Mismatching File", msg);
        return false;
      }
      else
         ui.textBrowser->append(QString("Verified %1").arg(localName));
    }
    else if (!rfu->mOptional)
    {
      QString msg = QString("Error: Unable to locate file %1\nPlease verify the ARM Root Directory").arg(normalizedName.c_str());
      QMessageBox::critical(NULL, "No Such File", msg);
      return false;
    }
  }
  else
  {
    qDebug() << "Error: Unable to expand name:" << normalizedName.c_str();
    return false;
  }
  return true;
}

bool MKUWPageStart::checkHashes()
{
  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  UtString envArm;
  envArm << "ARM_ROOT=" << wizard()->field("armKitRoot").toString();
  putenv((char*)envArm.c_str());

  KitManifest* manifest = wiz->getManifest();

  // Remove this before RTM (beta-only feature)
  char* hashBypass = getenv("CARBON_BYPASS_HASH");

  if (hashBypass)
    return true;

  // RTL Files
  foreach (KitFile* kitFile, manifest->getRTLFiles())
  {
    RTLFileOptions* rfu = manifest->getFileOptionsMap()[kitFile->getNormalizedName()];
    if (!checkKitHash(kitFile, rfu))
      return false;
  }

  //// Optional RTL Files
  //foreach (KitFile* kitFile, manifest->getOptionalRTLFiles())
  //{
  //  if (!checkKitHash(kitFile))
  //    return false;
  //}

  return true;
}

void MKUWPageStart::initializePage()
{
  restoreSettings();
 

  ModelKitUserWizard* wiz = static_cast<ModelKitUserWizard*>(wizard());
  ModelKitAnswers* answers = wiz->getAnswers();
  if (answers)
  {
    QString armRoot = answers->getSetting("ARMRoot").toString();
    ui.lineEditArmRootDir->setText(armRoot);
    wiz->next();
  }
}


void MKUWPageStart::on_pushButtonBrowse_clicked()
{
  QString dir = QFileDialog::getExistingDirectory(this, tr("ARM Kit Distribution Root Directory"),
      ui.lineEditArmRootDir->text(),  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
  
  if (!dir.isEmpty())
    ui.lineEditArmRootDir->setText(dir);   
}
