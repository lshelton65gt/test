#ifndef __MDITEXTEDITOR_H__
#define __MDITEXTEDITOR_H__

#include "util/CarbonPlatform.h"

#include <QtGui>
#include <QTextBrowser>
#include "util/UtString.h"
#include "Mdi.h"
#include "MdiTextEditor.h"

class QTextEditor;

class MDITextEditorTemplate : public MDIDocumentTemplate
{
public:
  MDITextEditorTemplate();
 
  virtual MDIWidget* createNewDocument(QWidget* parent=0);
  virtual int createMenus();
  virtual int createToolbars();
  virtual void updateMenusAndToolbars(QWidget* widget);
  virtual MDIWidget* openDocument(QWidget* parent, const char* docName);
  virtual MDIWidget* createDocument(QWidget* parent, const char* docName);
  bool getYesToAll() const { return mYesToAll; }
  void putYesToAll(bool newVal) { mYesToAll=newVal; }
  void addModifiedEditor(QTextEditor* editor);

private:
  void updateSelectedTab(QWidget* widget);

private:
  QAction* mActionCut;
  QAction* mActionCopy;
  QAction* mActionPaste;
  QAction* mActionFind;
  QAction* mActionUndo;
  QAction* mActionRedo;
  QAction* mActionFileSave;
  QAction* mActionRunScript;
  QAction* mActionDebugScript;
  QAction* mActionToggleBreakpoint;
  QAction* mActionDeleteBreakpoints;

  bool mYesToAll;
  QList<QTextEditor*> mModifiedEditors;

};

class MDITextBrowser : public MDIDocumentTemplate
{
public:
  MDITextBrowser();
 
  virtual MDIWidget* createNewDocument(QWidget* parent=0);
  virtual MDIWidget* openDocument(QWidget* parent, const char* docName);

};

class TextBrowserWidget : public QTextBrowser, public MDIWidget
{
 Q_OBJECT
public:
  TextBrowserWidget(MDIDocumentTemplate* doct=0, QWidget* parent=0);
  const char* userFriendlyName();

  void closeEvent(QCloseEvent *event);
private:
  UtString mDocName;
};

#endif
