#ifndef FIELDSGRAPHIC_H
#define FIELDSGRAPHIC_H

#include "util/CarbonPlatform.h"

#include "shell/carbon_shelltypes.h"
#include "shell/carbon_dbapi.h"

#include <QtGui>

#include <QGraphicsView>

class SpiritRegister;
class SpiritField;

class FieldsGraphic : public QGraphicsView
{
  Q_OBJECT

public:
  FieldsGraphic(QWidget *parent);
  ~FieldsGraphic();

  void drawRegister(SpiritRegister* reg);
  void highlightField(SpiritField* field);

private:
  void drawBitField(SpiritField* field);

  void drawBitDivider(int bitNumber);
  void drawBitNumber(int bitNumber);
  void drawLabel(SpiritField* field);
  void highlight(SpiritField* field);

  QList<QGraphicsRectItem*> mHighlightRects;
};

#endif // FIELDSGRAPHIC_H
