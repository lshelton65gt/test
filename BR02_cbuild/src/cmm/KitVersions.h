#ifndef __KITVERSIONS_H_
#define __KITVERSIONS_H_

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include <QtXml>


class XmlHelper
{
public:
  static QDomElement addElementWithAtts(QDomDocument& doc, QDomElement& parent, const QString& elementName, const QString& attName, const QString& attValue)
  {
    QDomElement element = doc.createElement(elementName);
    parent.appendChild(element);
    if (!attName.isEmpty())
      element.setAttribute(attName, attValue);
    return element;
  }

  static QDomElement addElement(QDomDocument& doc, QDomElement& parent, const QString& elementName)
  {
    QDomElement element = doc.createElement(elementName);
    parent.appendChild(element);
    return element;
  }

  static QDomElement addElement(QDomDocument& doc, QDomElement& parent, const QString& elementName, const QString& value)
  {
    QDomElement element = doc.createElement(elementName);
    parent.appendChild(element);

    QDomText text = doc.createTextNode(value);
    element.appendChild(text);

    return element;
  }
};


#endif

