//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"

#include <QtGui>

#include "util/XmlParsing.h"

#include "gui/CQt.h"

#include "util/CarbonAssert.h"
#include "util/UtHashMap.h"
#include "util/UtString.h"
#include "util/UtStringArray.h"
#include "util/UtIStream.h"
#include "util/UtIOStream.h"
#include "util/UtList.h"

#include "CarbonCompilerTool.h"
#include "CarbonCompilerTool.h"
#include "CarbonPropertyOutfile.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"

#define PROP_CARBON_EDIT "CarbonEditBox"
#define PROP_CARBON_FILTER "CarbonFileFilter"
#define PROP_CARBON_FILE "CarbonEditFileName"
#define PROP_CARBON_ITEM "CarbonFileWidget"
#define PROP_CARBON_SWITCHVALUE "CarbonPropertyValue"
#define ASSOCIATED_EDITOR "AssociatedEditor"

bool CarbonPropertyOutfile::parseXML(xmlNodePtr parent, UtXmlErrorHandler* /*eh*/)
{
  for (xmlNodePtr child = parent->children; child != NULL; child = child->next) 
  {
    if (XmlParsing::isElement(child, "filter"))
    {
      UtString filter;
      XmlParsing::getContent(child, &filter);
      putFilter(filter.c_str());
    }
  }

  return true;
}

bool CarbonPropertyOutfile::readValueXML(xmlNodePtr parent,  CarbonOptions* options, UtXmlErrorHandler* /*eh*/) const
{
  for (xmlNode *child = parent->children; child != NULL; child = child->next) 
  {
    const char* element = XmlParsing::elementName(child);
    if (element == NULL) // Skip non-interesting XML things
      continue;

    if (0 == strcmp(element, "Value"))
    {
      UtString value;
      XmlParsing::getContent(child, &value);
      options->putValue(this, value.c_str());
    }
  }
  return true;}

bool CarbonPropertyOutfile::writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* /*eh*/)
{
  xmlTextWriterStartElement(writer, BAD_CAST "Option");
  xmlTextWriterWriteAttribute(writer, BAD_CAST "Name", BAD_CAST getName());

  xmlTextWriterStartElement(writer, BAD_CAST "Value");
  xmlTextWriterWriteString(writer, BAD_CAST value->getValue());
  xmlTextWriterEndElement(writer);

  xmlTextWriterEndElement(writer);
  return true;
}

CarbonDelegate* CarbonPropertyOutfile::createDelegate(QObject* parent,  PropertyEditorDelegate* delegate, PropertyEditor* propEditor)
{
  return new CarbonDelegateOutfile(parent, delegate, propEditor);
}

// Delegate

// Editor Delegate
CarbonDelegateOutfile::CarbonDelegateOutfile(QObject *parent, PropertyEditorDelegate* delegate, PropertyEditor* editor)
: CarbonDelegate(parent,delegate,editor)
{
}

QWidget* CarbonDelegateOutfile::createEditor(QWidget *parent, QTreeWidgetItem* item, CarbonProperty* prop, const QModelIndex& /*index*/) const
{
  const CarbonPropertyOutfile* sw_outfile = (const CarbonPropertyOutfile*)prop;
  QWidget* widget = NULL;

  bool id,is;
  CarbonPropertyValue* sv = mEditor->getSettings()->getValue(prop,&id,&is);
  UtString value = sv->getValue();

  QWidget* w = new QWidget(parent);
  QHBoxLayout* layout = new QHBoxLayout();
  QPushButton* btn = new QPushButton("...");

  connect(btn, SIGNAL(clicked(bool)), this, SLOT(showOutfileDialog()));

  btn->setMaximumWidth(20);
  QLineEdit* edit = new QLineEdit(widget);
  edit->setObjectName(ASSOCIATED_EDITOR);
  edit->setText(value.c_str());

  QVariant qv = qVariantFromValue((void*)edit);
  btn->setProperty(PROP_CARBON_EDIT, qv);

  QVariant qvp = qVariantFromValue((void*)sw_outfile);
  btn->setProperty("OutFileProperty", qvp);

  QVariant qvw = qVariantFromValue((void*)item);
  btn->setProperty(PROP_CARBON_ITEM, qvw);

  QVariant qfname(value.c_str());
  btn->setProperty(PROP_CARBON_FILE, qfname);

  const char* filter = sw_outfile->getFilter();
  if (strlen(filter) > 0)
  {
    QVariant qf(filter);
    btn->setProperty(PROP_CARBON_FILTER, qf);
  }

  layout->addWidget(edit);
  layout->addWidget(btn);

  layout->setMargin(0);
  layout->setSpacing(0);

  w->setAutoFillBackground(true);
  w->setLayout(layout);

  return w;
}

void CarbonDelegateOutfile::commitAndCloseEditor()
{
  QLineEdit *widget = qobject_cast<QLineEdit *>(sender());
  if (widget)
    mDelegate->commit(widget);
}

QString getFilterExtension(QString filter)
{
  QString result;

  int leftParen = filter.indexOf('(');
  int rightParen = filter.indexOf(')');
  QString extList = filter.mid(leftParen+1,rightParen-leftParen-1);
  foreach (QString ext, extList.split(" "))
  {
    if (ext.startsWith("*."))
    {
      result = ext.mid(2);
      break;
    }
  }
  return result;
}

void CarbonDelegateOutfile::showOutfileDialog()
{
  QPushButton *pb = qobject_cast<QPushButton *>(sender());

  UtString filter;

  QVariant qff = pb->property(PROP_CARBON_FILTER);
  if (qff.isValid())
    filter << qff.toString();
  else
    filter << "All Files (*.*)";

  UtString defaultName;
  QVariant qfile = pb->property(PROP_CARBON_FILE);
  if (qfile.isValid())
    defaultName << qfile.toString();
  else
    defaultName << "";

  QString selFilter;
  QFileInfo fi(defaultName.c_str());
  QString fileExtension = fi.completeSuffix();
 
  QString filterList(filter.c_str());
  QString initialFilter;

  foreach (QString filter, filterList.split(";;"))
  {
    int leftParen = filter.indexOf('(');
    int rightParen = filter.indexOf(')');
    QString extList = filter.mid(leftParen+1,rightParen-leftParen-1);
    foreach (QString ext, extList.split(" "))
    {
      if (ext.startsWith("*."))
      {
        QString filterExtension = ext.mid(2);
        if (filterExtension == fileExtension)
        {
          initialFilter = filter;
          break;
        }
      }
    }
    if (initialFilter.length() > 0)
      break;
  }
 
  UtString fileName;
  fileName << QFileDialog::getSaveFileName(0, tr("Output File"), fi.baseName(), filter.c_str(), &initialFilter);
  if (fileName.length() == 0) // User Cancelled
    return;

  QFileInfo fi2(fileName.c_str());
  QString newExt = fi2.completeSuffix();
  if (newExt.length() == 0 && initialFilter.length() > 0) // then tack on the default extension
  {
    UtString fext;
    fext << getFilterExtension(initialFilter);
    fileName << "." << fext;
  }

  
  QVariant qv = pb->property(PROP_CARBON_EDIT);
  if (qv.isValid() && fileName.length() > 0)
  {
    QLineEdit* edit = (QLineEdit*)qv.value<void*>();

    QVariant qvp = pb->property("OutFileProperty");
    const CarbonPropertyOutfile* propOutFile = (const CarbonPropertyOutfile*)qvp.value<void*>();
    if (propOutFile && propOutFile->getNameOnly())
    {
      QFileInfo fi(fileName.c_str());
      fileName.clear();
      fileName << fi.fileName();
    }

    edit->setText(fileName.c_str());

    // This code causes the newly entered text to be committed
    // closeEditor() and commitData() didn't work
    QVariant qvi = pb->property(PROP_CARBON_ITEM);
    if (qvi.isValid())
    {
      QTreeWidgetItem* item = (QTreeWidgetItem*)qvi.value<void*>();
      mEditor->setCurrentItem(item, 0);
    }
  }
}

void CarbonDelegateOutfile::setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const
{
  bool id,is;

  if (mEditor->getSettings() == NULL)
    return;

  CarbonPropertyValue* sv = mEditor->getSettings()->getValue(prop,&id,&is);
  if (sv == NULL)
    return;

 
  UtString currentValue = sv->getValue();
 
  QLineEdit* lineEdit = editor->findChild<QLineEdit*>(ASSOCIATED_EDITOR);
  if (lineEdit != NULL)
  {
    UtString tmp;
    tmp << lineEdit->text();

    QRegExp re(prop->getLegalPattern());
    if (prop->hasLegalPattern() && !re.exactMatch(lineEdit->text()))
    {
      QString msg = QString("The switch '%1' value of '%2' does not match the legal pattern: %3, ignoring.")
        .arg(prop->getName())
        .arg(lineEdit->text())
        .arg(prop->getLegalPattern());
      QMessageBox::warning(NULL, MODELSTUDIO_TITLE, msg);
      return;
    }

    if (prop->getRequired() && tmp.length() == 0)
    {
      QString msg = QString("The switch '%1' value must not be blank").arg(prop->getName());
      QMessageBox::warning(NULL, MODELSTUDIO_TITLE, msg);
      return;
    }

    CarbonProject* proj = mEditor->getContext()->getCarbonProjectWidget()->project();

    UtString value;
    value << CarbonProjectWidget::makeRelativePath(proj->getProjectDirectory(), tmp.c_str());
    setValue(sv, item, model, index, currentValue.c_str(), value.c_str(), prop);
  }
}

