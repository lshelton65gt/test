#ifndef __MDIMV_H
#define __MDIMV_H

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include <QMenuBar>
#include <QProgressBar>
#include <QComboBox>

#include "util/UtString.h"
#include "Mdi.h"
#include "MdiProject.h"
#include "CarbonMakerContext.h"
class MDIWidget;
class MDIMVTemplate : public MDIDocumentTemplate
{
public:
  MDIMVTemplate(CarbonMakerContext* ctx);
 
  virtual MDIWidget* createNewDocument(QWidget* parent=0);
  virtual int createMenus();
  virtual int createToolbars();
  virtual void updateMenusAndToolbars(QWidget* widget);
  virtual MDIWidget* openDocument(QWidget* parent, const char* docName);

private:
  void updateSelectedTab(QWidget* widget);

private:
  CarbonMakerContext* mContext;
};

#endif
