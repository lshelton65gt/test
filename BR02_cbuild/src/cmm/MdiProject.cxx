//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "util/UtShellTok.h"

#include <QtGui>
#include <QMenuBar>
#include <QToolBar>
#include "gui/CQt.h"
#include "cmm.h"
#include "MdiProject.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "SettingsEditor.h"
#include "WorkspaceModelMaker.h"
#include "CarbonOptions.h"
#include "shell/ReplaySystem.h"
#include "ModelStudioCommon.h"
#include "DlgBatchBuild.h"
#include "ErrorInfoWidget.h"
#include "PackageToolWizardWidget.h"
#include "DlgRunScript.h"

#define SCONNECT(sender, signal, receiver, slot) do { \
  bool signal_slot_connected = \
    connect(sender, signal, receiver, slot); \
  INFO_ASSERT(signal_slot_connected, "signal or slot connection failure"); \
} while (0)

MDIProjectTemplate::MDIProjectTemplate(CarbonMakerContext* ctx)
  : MDIDocumentTemplate("Project", "New Project", "Carbon Project (*.carbon)") 
{
  mContext = ctx;
  mViewMenu = NULL;
  mConnectedHierWindow = false;

  setInvisibleDocument(true);
  mConfigurationsComboBox = NULL;

  mActionSaveProject = new QAction(QIcon(":/cmm/Resources/saveall.png"), tr("Save &All"), this);
  mActionSaveProject->setStatusTip(tr("Save All Documents"));
  mActionSaveProject->setShortcut(tr("Ctrl+Shift+S"));

  mActionCloseProject = new QAction(QIcon(":/cmm/Resources/close.png"), tr("&Close Project"), this);
  mActionCloseProject->setStatusTip(tr("Close the Project"));

  mActionProjProperties = new QAction(QIcon(":/cmm/Resources/properties.png"), tr("Compiler &Settings..."), this);
  mActionProjProperties->setStatusTip(tr("Change compiler settings")); 

  mActionProjConfig = new QAction(tr("Configuration &Manager..."), this);
  mActionProjConfig->setStatusTip(tr("Change project configurations")); 

  mActionRunScript = new QAction(tr("Run Script..."), this);
  mActionRunScript->setStatusTip(tr("Execute a Model Studio custom script (ECMAScript)")); 

  mActionProjCopyConfig = new QAction(tr("Copy Configuration..."), this);
  mActionProjCopyConfig->setStatusTip(tr("Copy a configuration and all its settings")); 

  mActionCheck = new QAction(QIcon(":/cmm/Resources/check.png"), tr("Chec&k"), this);
  mActionCheck->setStatusTip(tr("Check the design")); 
  mActionCheck->setToolTip("Check the design for errors");
  mActionCheck->setShortcut(tr("F2"));

  mActionCompile = new QAction(QIcon(":/cmm/Resources/compile.png"), tr("&Compile"), this);
  mActionCompile->setStatusTip(tr("Compile the design")); 
  mActionCompile->setToolTip("Compile the project");
  mActionCompile->setShortcut(tr("F6"));
  
  mActionPackage = new QAction(QIcon(":/cmm/Resources/package.png"), tr("&Package"), this);
  mActionPackage->setStatusTip(tr("Package the model")); 
  mActionPackage->setToolTip("Package the model");
  mActionPackage->setShortcut(tr("F10"));

  mActionCancelCompile = new QAction(QIcon(":/cmm/Resources/logError.png"), tr("&Stop Compilation"), this);
  mActionCancelCompile->setStatusTip(tr("Stop the current compilation in progress")); 
  mActionCancelCompile->setIconText("Stop");
  mActionCancelCompile->setToolTip("Stops the current compilation by killing the process");
  mActionCancelCompile->setShortcut(tr("Ctrl+F6"));

  mActionReCompile = new QAction(tr("&Recompile"), this);
  mActionReCompile->setStatusTip(tr("Clean, and Compile the design")); 
  mActionReCompile->setToolTip("Cleans then Compile the design");
  mActionReCompile->setShortcut(tr("F7"));

  mActionBatchBuild = new QAction(tr("&Batch Build..."), this);
  mActionBatchBuild->setStatusTip(tr("Build all configurations")); 
  mActionBatchBuild->setToolTip("Build all configurations");
  mActionBatchBuild->setShortcut(tr("F8"));

  mActionBuildPrecompilationHierarchy = new QAction(tr("&Explore Hierarchy..."), this);
  mActionBuildPrecompilationHierarchy->setStatusTip(tr("Explore Hierarchy information")); 
  mActionBuildPrecompilationHierarchy->setToolTip("Build Hierarchical information");
  mActionBuildPrecompilationHierarchy->setShortcut(tr("F3"));

  mActionClean = new QAction(tr("C&lean"), this);
  mActionClean->setStatusTip(tr("Clean the design")); 
  mActionClean->setShortcut(tr("Shift+F9"));

  mActionHierWindow = new QAction(tr("Design &Hierarchy"), this);
  mActionHierWindow->setStatusTip(tr("Design Hierarchy"));
  mActionHierWindow->setCheckable(true);
  mActionHierWindow->setChecked(true);
  mActionHierWindow->setShortcut(tr("Ctrl+T,Ctrl+D"));
  
  mActionCancelCompile->setEnabled(false);

  mActionSimulate = new QAction(QIcon(":/cmm/Resources/onDemandStart.png"), tr("Simulate &Project"), this); 
  mActionSimulate->setIconText("Simulate");
  mActionSimulate->setStatusTip(tr("Simulate the project"));

  mActionImportCmd = new QAction(QIcon(":/cmm/Resources/carbon.png"), tr("&Import Carbon Command File"), this);
  mActionImportCmd->setIconText("Import .cmd");
  mActionImportCmd->setStatusTip(tr("Import a Carbon Command File (.cmd,.f) into the Project"));

  mActionImportCcfg = new QAction(QIcon(":/cmm/Resources/Wizard.png"), tr("Import Carbon Wi&zard File"), this);
  mActionImportCcfg->setIconText("Import a previosly created Carbon Wizard file");
  mActionImportCcfg->setStatusTip(tr("Import a Carbon Wizard (.ccfg) into this Project"));

  // license check here
  bool bCanCreateCoware = ctx->getCanCreateCoware();
  bool bCanCreateMaxsim = ctx->getCanCreateMaxsim();
  bool bCanCreateSystemC = ctx->getCanCreateSystemC();
  bool bCanCreateMV = ctx->getCanCreateMV();

  mActionImportCcfg->setEnabled(bCanCreateCoware || bCanCreateMaxsim);

  if (!mActionImportCcfg->isEnabled())
    mActionImportCcfg->setStatusTip(tr("No license to create or import Soc Designer or CoWare components"));

  mActionCreateCoware = new QAction(this);
  mActionCreateCoware->setText("Create &CoWare Component...");
  mActionCreateCoware->setIconText(COWARE_COMPONENT_NAME);
  mActionCreateCoware->setObjectName(QString::fromUtf8("cpwCreateCowareComponent"));
  mActionCreateCoware->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/coware.png")));
  if (bCanCreateCoware)
    mActionCreateCoware->setStatusTip("Create CoWare component from Carbon Component");

  mActionCreateCoware->setEnabled(bCanCreateCoware);

  mActionCreateMaxsim = new QAction(this);
  mActionCreateMaxsim->setText("Create S&oC Designer Component...");
  mActionCreateMaxsim->setIconText("SoC Designer");
  mActionCreateMaxsim->setObjectName(QString::fromUtf8("cpwCreateMaxsiComponent"));
  mActionCreateMaxsim->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/maxsim.png")));
  if (bCanCreateMaxsim)
    mActionCreateMaxsim->setStatusTip("Create SoC Designer component from Carbon Component");

  mActionCreateMaxsim->setEnabled(bCanCreateMaxsim);

  mActionCreateSystemC = new QAction(this);
  mActionCreateSystemC->setText("Create S&ystemC Component...");
  mActionCreateSystemC->setIconText("SystemC");
  mActionCreateSystemC->setObjectName(QString::fromUtf8("cpwCreateSystemCComponent"));
  mActionCreateSystemC->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/SystemC.png")));
  if (bCanCreateSystemC)
    mActionCreateSystemC->setStatusTip("Create SystemC component from Carbon Component");

  mActionCreateSystemC->setEnabled(bCanCreateSystemC);

  mActionCreateMV = new QAction(this);
  mActionCreateMV->setText("Create Model &Validation Component...");
  mActionCreateMV->setIconText("MV");
  mActionCreateMV->setToolTip("Create a Model Validation Component");
  mActionCreateMV->setObjectName(QString::fromUtf8("cpwCreateMVComponent"));
  mActionCreateMV->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/TaskHS.png")));
  if (bCanCreateMV)
    mActionCreateMV->setStatusTip("Create Model Validation component from Carbon Component");  

  mActionCreateMV->setEnabled(bCanCreateMV);

  mActionModelWizard = new QAction(this);
  mActionModelWizard->setText("Launch the Remodeling &Wizard");
  mActionModelWizard->setIconText(MODELWIZARD_TITLE);
  mActionModelWizard->setToolTip("Launch the Remodeling Wizard");
  mActionModelWizard->setObjectName(QString::fromUtf8("cpwCreateMW"));
  mActionModelWizard->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/ModelWizard.png")));
  mActionModelWizard->setStatusTip("Launch the Remodeling Wizard");  
  mActionModelWizard->setEnabled(true);

  mActionPackageOptions = new QAction(this);
  mActionPackageOptions->setText("Pac&kage Options");
  mActionPackageOptions->setIconText("Package");
  mActionPackageOptions->setToolTip("Model Distribution Packaging Options");
  mActionPackageOptions->setObjectName(QString::fromUtf8("cpwCreatePackageComponent"));
  mActionPackageOptions->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/package.png")));
  mActionPackageOptions->setStatusTip("Set model distribution packaging options");  

  mActionAddSources = new QAction(this);
  mActionAddSources->setText("Add &RTL Source(s)...");
  mActionAddSources->setObjectName(QString::fromUtf8("cpwAddSources"));
  mActionAddSources->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/project.png")));
  mActionAddSources->setStatusTip("Add Verilog and/or VHDL sources to be compiled");

  mActionAddLib = new QAction(this);
  mActionAddLib->setText("Add RTL &Library...");
  mActionAddLib->setObjectName(QString::fromUtf8("cpwAddLibrary"));
  mActionAddLib->setIcon(QIcon(QString::fromUtf8(":/cmm/Resources/NewFolderHS.png")));
  mActionAddLib->setStatusTip("Create an RTL library, then add RTL sources to it");
}

void MDIProjectTemplate::updateProjectMenus()
{
}

void MDIProjectTemplate::compilationModeChanged(bool inProgress)
{
  qDebug() << "compilation mode, inprogress: " << inProgress;
  
  // Let modelstudio enable/disable menus as well
  cmm* modelStudio = mContext->getModelStudio();
  if (modelStudio)
    modelStudio->compilationModeChanged(inProgress);

  mConfigurationsComboBox->setEnabled(!inProgress);
  mActionCheck->setEnabled(!inProgress);
  mActionClean->setEnabled(!inProgress);
  mActionCompile->setEnabled(!inProgress);
  mActionPackage->setEnabled(!inProgress);
  mActionReCompile->setEnabled(!inProgress);
  mActionBatchBuild->setEnabled(!inProgress);
  mActionBuildPrecompilationHierarchy->setEnabled(!inProgress);
  mActionCancelCompile->setEnabled(inProgress);
  mActionSimulate->setEnabled(!inProgress);
  mActionImportCmd->setEnabled(!inProgress);
  mActionImportCcfg->setEnabled(!inProgress);
  mActionProjProperties->setEnabled(!inProgress);
  mActionProjConfig->setEnabled(!inProgress);
  mActionRunScript->setEnabled(!inProgress);
  mActionCloseProject->setEnabled(!inProgress);
  mActionSaveProject->setEnabled(!inProgress);

  bool isHDLProject = true;
  if (mContext->getCarbonProjectWidget()->project()->getProjectType() == CarbonProject::DatabaseOnly)
    isHDLProject = false;

  mActionModelWizard->setEnabled(!inProgress && isHDLProject);

  mActionAddLib->setEnabled(isHDLProject && !inProgress);
  mActionAddSources->setEnabled(isHDLProject && !inProgress);

   // check licenseses here.
  bool bCanCreateCoware = mContext->getCanCreateCoware() && !inProgress;
  bool bCanCreateMaxsim = mContext->getCanCreateMaxsim() && !inProgress;
  bool bCanCreateSystemC = mContext->getCanCreateSystemC() && !inProgress;
  bool bCanCreateMV = mContext->getCanCreateMV() && !inProgress;
  bool bCanPackageOptions = mContext->getCanCreatePackage() && !inProgress;

  mActionCreateCoware->setEnabled(bCanCreateCoware && !mContext->getCarbonProjectWidget()->getActiveCowareComponent());
  mActionCreateMaxsim->setEnabled(bCanCreateMaxsim && !mContext->getCarbonProjectWidget()->getActiveMaxsimComponent());
  mActionCreateSystemC->setEnabled(bCanCreateSystemC && !mContext->getCarbonProjectWidget()->getActiveSystemCComponent());
  mActionCreateMV->setEnabled(bCanCreateMV && !mContext->getCarbonProjectWidget()->getActiveMVComponent());
  mActionPackageOptions->setEnabled(bCanPackageOptions);

  mActionImportCcfg->setEnabled(bCanCreateCoware || bCanCreateMaxsim);
}

void MDIProjectTemplate::registerDockWidget(QDockWidget* widget, const char* name, const char*)
{
  QAction* action = new QAction(name, this);

  action->setCheckable(true);
  action->setChecked(true);
  action->setProperty("CarbonDockWidget", qVariantFromValue((void*)widget));

  mDockWidgetActionMap[widget] = action;
  mDockActions.push_back(action);

  CQT_CONNECT(widget, visibilityChanged(bool), this, dockVisibilityChanged(bool));
}

void MDIProjectTemplate::hierDockVisibilityChanged(bool /*vis*/)
{
  QDockWidget* dock = qobject_cast<QDockWidget*>(sender());
  mActionHierWindow->setChecked(dock->isVisible());
}
void MDIProjectTemplate::dockVisibilityChanged(bool /*vis*/)
{
  QDockWidget* dock = qobject_cast<QDockWidget*>(sender());
  if (dock)
  {
    QAction* action = mDockWidgetActionMap[dock];
    if (action)
      action->setChecked(dock->isVisible());
  }
}

void MDIProjectTemplate::updateMenusAndToolbars(QWidget* widget)
{
  InvisibleProject* ip = dynamic_cast<InvisibleProject*>(widget);
  INFO_ASSERT(ip != NULL, "Internal Error, Should be InvisibleProject");

  QDockWidget* hierDock = mContext->getCarbonProjectWidget()->getHierarchyDockWidget();
  if (hierDock)
  {
    if (!mConnectedHierWindow)
    {
      CQT_CONNECT(hierDock, visibilityChanged(bool), this, hierDockVisibilityChanged(bool));
      mConnectedHierWindow = true;
    }
    mActionHierWindow->setChecked(hierDock->isVisible());
    mActionHierWindow->setEnabled(true);
  }
  else
    mActionHierWindow->setDisabled(true);

  CarbonProject* proj = mContext->getCarbonProjectWidget()->project();
  mActionProjProperties->setEnabled(proj && proj->getProjectType() == CarbonProject::HDL);

  ip->updateConfigurationsComboBox(mConfigurationsComboBox);  
}

int MDIProjectTemplate::createToolbars()
{
  QToolBar* toolbar = new QToolBar("Project");
  toolbar->setObjectName("ProjectToolbar");

  toolbar->addAction(mActionSaveProject);
  toolbar->addAction(mActionCheck);
  toolbar->addAction(mActionCompile);
  if (mContext->getCanCreatePackage())
    toolbar->addAction(mActionPackage);
  toolbar->addAction(mActionCancelCompile);

  mConfigurationsComboBox = new QComboBox();
  mConfigurationsComboBox->setToolTip("Configurations");
  toolbar->addWidget(mConfigurationsComboBox);
 
  addToolbar(toolbar);

  QToolBar* toolbar2 = new QToolBar("Components");
  toolbar2->setObjectName("Components");

  toolbar2->addAction(mActionCreateMaxsim);
  toolbar2->addAction(mActionCreateCoware);
  toolbar2->addAction(mActionCreateSystemC);
  toolbar2->addAction(mActionCreateMV);
  toolbar2->addAction(mActionModelWizard);
  toolbar2->addAction(mActionSimulate);

  addToolbar(toolbar2);

  return numToolbars();
}

int MDIProjectTemplate::createMenus()
{
  QMenu* fileMenu = new QMenu("&File");
  fileMenu->setObjectName("ProjectFileMenu");
  fileMenu->addAction(mActionCloseProject);
  registerAction(mActionCloseProject, SLOT(closeProject()));

  fileMenu->addAction(mActionSaveProject);
  registerAction(mActionSaveProject, SLOT(saveProject()));
  
  addMenu(fileMenu);

  QMenu* buildMenu = new QMenu("&Build");
  buildMenu->setObjectName("ProjectBuildMenu");

  buildMenu->addAction(mActionCheck);
  registerAction(mActionCheck, SLOT(projectCheck()));

  buildMenu->addAction(mActionBuildPrecompilationHierarchy);
  registerAction(mActionBuildPrecompilationHierarchy, SLOT(projectBuildPrecompilationHierarchy()));

  buildMenu->addAction(mActionCompile);
  registerAction(mActionCompile, SLOT(projectCompile()));
  
  if (mContext->getCanCreatePackage()) {
    buildMenu->addAction(mActionPackage);
    registerAction(mActionPackage, SLOT(projectPackage()));
  }

  buildMenu->addAction(mActionCancelCompile);
  registerAction(mActionCancelCompile, SLOT(projectCancelCompile()));

  buildMenu->addAction(mActionReCompile);
  registerAction(mActionReCompile, SLOT(projectReCompile()));

  buildMenu->addAction(mActionBatchBuild);
  registerAction(mActionBatchBuild, SLOT(projectBatchBuild()));

  buildMenu->addAction(mActionClean);
  registerAction(mActionClean, SLOT(projectClean()));

  addMenu(buildMenu);

  QMenu* projMenu = new QMenu("&Project");
  projMenu->setObjectName("ProjectProjectMenu");
  projMenu->addAction(mActionProjProperties);
  registerAction(mActionProjProperties, SLOT(projectProperties()));

  if (mContext->getCanCreatePackage()) {
    projMenu->addAction(mActionPackageOptions);
    registerAction(mActionPackageOptions, SLOT(actionPackageOptions()));
  }

  projMenu->addAction(mActionRunScript);
  registerAction(mActionRunScript, SLOT(projectRunScript()));

  projMenu->addAction(mActionProjConfig);
  registerAction(mActionProjConfig, SLOT(projectConfigurationManager()));

  projMenu->addAction(mActionAddSources);
  projMenu->addAction(mActionAddLib);
  projMenu->addAction(mActionSimulate);
  
  registerAction(mActionSimulate, SLOT(actionSimulate()));
  registerUpdateAction(mActionSimulate, SIGNAL(enableSimulate(bool)), SLOT(setEnabled(bool)));

  projMenu->addAction(mActionImportCmd);
  registerAction(mActionImportCmd, SLOT(actionImportCmd()));

  projMenu->addAction(mActionImportCcfg);
  registerAction(mActionImportCcfg, SLOT(actionImportCcfg()));

  projMenu->addSeparator();

  projMenu->addAction(mActionCreateMaxsim);
  projMenu->addAction(mActionCreateCoware);
  projMenu->addAction(mActionCreateSystemC);
  projMenu->addAction(mActionCreateMV);
  projMenu->addAction(mActionModelWizard);

  registerAction(mActionAddSources, SLOT(actionAddSources()));
  registerUpdateAction(mActionAddSources, SIGNAL(enableAddSources(bool)), SLOT(setEnabled(bool)));

  registerAction(mActionAddLib, SLOT(actionAddLibrary()));
  registerUpdateAction(mActionAddLib, SIGNAL(enableAddLibrary(bool)), SLOT(setEnabled(bool)));

  registerAction(mActionCreateMaxsim, SLOT(actionCreateMaxsim()));
  registerUpdateAction(mActionCreateMaxsim, SIGNAL(enableMaxsim(bool)), SLOT(setEnabled(bool)));

  registerAction(mActionCreateCoware, SLOT(actionCreateCoware()));
  registerUpdateAction(mActionCreateCoware, SIGNAL(enableCoWare(bool)), SLOT(setEnabled(bool)));
  
  registerAction(mActionCreateSystemC, SLOT(actionCreateSystemC()));
  registerUpdateAction(mActionCreateSystemC, SIGNAL(enableSystemC(bool)), SLOT(setEnabled(bool)));

  registerAction(mActionCreateMV, SLOT(actionCreateMV()));
  registerUpdateAction(mActionCreateMV, SIGNAL(enableMV(bool)), SLOT(setEnabled(bool)));

  registerAction(mActionModelWizard, SLOT(actionCreateModelWizard()));

  addMenu(projMenu);
  mViewMenu = new QMenu("&View");
  mViewMenu->setObjectName("ProjectViewMenu");

  foreach (QAction* action, mDockActions)
  {
    mViewMenu->addAction(action);
    registerAction(action, SLOT(toggleDockWindow()));
  }

  mViewMenu->addAction(mActionHierWindow);
  registerAction(mActionHierWindow, SLOT(hierWindow()));

  addMenu(mViewMenu);

  return numMenus();
}

MDIWidget* MDIProjectTemplate::createNewDocument(QWidget* /*parent*/)
{
  return NULL;
}

InvisibleProject* MDIProjectTemplate::mInvisibleProject = NULL;

// We don't have a MDI document presence, only the docking window widget
// so we create the InvisibleProject here to handle menu & toolbar events
//
MDIWidget* MDIProjectTemplate::openDocument(QWidget* /*parent*/, const char* /*docName*/)
{
  showMenusAndToolbars(true);

  INFO_ASSERT(mContext->getCarbonProjectWidget(), "Expecting Project Widget");

  if (mInvisibleProject)
  {
    mInvisibleProject->close();
    mInvisibleProject = NULL;
  }

  InvisibleProject* ip = new InvisibleProject(mContext, this);
  activateDocument(ip);

  mContext->getCarbonProjectWidget()->updateContextMenuActions();

  CarbonProject* proj = mContext->getCarbonProjectWidget()->project();
  const char* simScript = proj->getProjectOptions()->getValue("Simulation Script")->getValue();
  mActionSimulate->setEnabled(simScript && strlen(simScript) > 0);
  
  mInvisibleProject = ip;

  return ip;
}

void InvisibleProject::closeEvent(QCloseEvent *event)
{
  event->accept();
}

InvisibleProject::~InvisibleProject()
{
  CarbonProject* proj = mContext->getCarbonProjectWidget()->project();
  proj->getProjectOptions()->unregisterPropertyChanged("Simulation Script", this);
  qDebug() << "Closing project";
}
void InvisibleProject::actionSimulate()
{
  emit enableSimulate(false); // disable the button immediately

  qDebug() << "Launch Simulation";
  QStringList args;
  CarbonProject* proj = mContext->getCarbonProjectWidget()->project();
  // remove any old files that might be left around, just in case.
  proj->cleanupMonitor();

  CarbonConfiguration* activeConfig = proj->getActive();

  // Commit any pending changes
  mContext->getSettingsEditor()->commitEdit();

  UtString relOutputDir;

  CarbonConsole::CommandMode mode = CarbonConsole::Local;
  const char* scriptLoc = proj->getProjectOptions()->getValue("Simulation Script Execution")->getValue();
  if (scriptLoc && 0 == strcmp(scriptLoc, "remote"))
    mode = CarbonConsole::Remote;
  else
    mode = CarbonConsole::Local;

  // Check for a quick-double click launch 

  if (mode == CarbonConsole::Remote)
    relOutputDir << CarbonProjectWidget::makeRelativePath(proj->getProjectDirectory(), activeConfig->getOutputDirectory());
  else
    relOutputDir = proj->getProjectDirectory();

  UtString cmdFileName;
  cmdFileName << activeConfig->getName() << ".f.cmd";

  UtString dirFileName;
  dirFileName << activeConfig->getName() << ".directives";

  proj->generateMakeFile(activeConfig, mode, activeConfig->getOutputDirectory(), cmdFileName.c_str(), dirFileName.c_str());

  args << "-f" << "Makefile.carbon" << "StartSimulate" << "simulate" << "FinishedSimulate";

  MDIProjectTemplate* projTempl = dynamic_cast<MDIProjectTemplate*>(getDocumentTemplate());
  if (projTempl)
    projTempl->compilationModeChanged(false);

 UtString chdir;
#if pfWINDOWS
  if (mode == CarbonConsole::Remote)
    chdir << relOutputDir.c_str();
  else
    chdir << activeConfig->getOutputDirectory();
#else
  chdir << activeConfig->getOutputDirectory();
#endif

  UtString makeCommand;

  if (mode == CarbonConsole::Remote)
    makeCommand << proj->remoteCommandName("make");
  else
  {
#if pfWINDOWS
    // Check if it is a symlink
    UtString makeLink, err;
    OSExpandFilename(&makeLink,"$CARBON_HOME/Win/bin/make.exe.lnk",&err);
    QFileInfo fi(makeLink.c_str());
    if(fi.exists() && fi.isSymLink())
      makeCommand << fi.symLinkTarget();
    else
      makeCommand << "$CARBON_HOME/Win/bin/make.exe";
#else
    makeCommand << "$CARBON_HOME/bin/make";
#endif
  }

  proj->getProjectWidget()->getConsole()->executeCommand(mode, CarbonConsole::Compilation, makeCommand.c_str(), args, chdir.c_str(), CarbonConsole::Simulate);
}
void InvisibleProject::actionAddSources()
{
  mContext->getCarbonProjectWidget()->actionAddSources();
}
void InvisibleProject::actionCreateCoware()
{
  mContext->getCarbonProjectWidget()->actionCreateCoware();
}
void InvisibleProject::actionAddLibrary()
{
  mContext->getCarbonProjectWidget()->actionAddLibrary();
}
void InvisibleProject::actionImportCmd()
{
  mContext->getCarbonProjectWidget()->actionImportCommandFile();
}

void InvisibleProject::actionImportCcfg()
{
  mContext->getCarbonProjectWidget()->actionImportCcfgFile();
}

void InvisibleProject::actionCreateModelWizard()
{
  mContext->getCarbonProjectWidget()->actionModelWizard();
}

void InvisibleProject::actionCreateMaxsim()
{
  mContext->getCarbonProjectWidget()->actionCreateMaxsim();
}
void InvisibleProject::actionCreateSystemC()
{
  mContext->getCarbonProjectWidget()->actionCreateSystemC();
}
void InvisibleProject::actionCreateMV()
{
  mContext->getCarbonProjectWidget()->actionCreateMV();
}

void InvisibleProject::actionPackageOptions()
{
  mContext->getCarbonProjectWidget()->actionPackageOptions();
}

const char* InvisibleProject::userFriendlyName()
{
  CarbonProject* proj = mContext->getCarbonProjectWidget()->project();
  if (proj)
    return proj->shortName();
  else
    return "no project opened";
}

InvisibleProject::InvisibleProject(CarbonMakerContext* ctx, MDIDocumentTemplate* doct)
{
  setAttribute(Qt::WA_DeleteOnClose);

  mContext=ctx;
  mConfigComboBox = NULL;

  initializeMDI(this, doct);

  INFO_ASSERT(ctx->getCarbonProjectWidget(), "Expecting Project Widget");
  INFO_ASSERT(ctx->getCarbonProjectWidget()->project(), "Expecting CarbonProject");

  CQT_CONNECT(ctx->getCarbonProjectWidget()->project(), configurationCopy(const char*, const char*), this, configurationCopy(const char*, const char*));
  CQT_CONNECT(ctx->getCarbonProjectWidget()->project(), configurationChanged(const char*), this, configurationChanged(const char*));
  CQT_CONNECT(ctx->getCarbonProjectWidget()->project(), configurationRenamed(const char*, const char*), this, configurationRenamed(const char*, const char*));
  CQT_CONNECT(ctx->getCarbonProjectWidget()->project(), configurationRemoved(const char*), this, configurationRemoved(const char*));
  CQT_CONNECT(ctx->getCarbonProjectWidget()->project(), configurationAdded(const char*), this, configurationAdded(const char*));

  SCONNECT(ctx->getCarbonProjectWidget(), SIGNAL(canCreateCoWare(bool)), this, SIGNAL(enableCoWare(bool)));
  SCONNECT(ctx->getCarbonProjectWidget(), SIGNAL(canCreateMaxsim(bool)), this, SIGNAL(enableMaxsim(bool)));
  SCONNECT(ctx->getCarbonProjectWidget(), SIGNAL(canCreateSystemC(bool)), this, SIGNAL(enableSystemC(bool)));
  SCONNECT(ctx->getCarbonProjectWidget(), SIGNAL(canCreateMV(bool)), this, SIGNAL(enableMV(bool)));

  SCONNECT(ctx->getCarbonProjectWidget(), SIGNAL(canAddLibrary(bool)), this, SIGNAL(enableAddLibrary(bool)));
  SCONNECT(ctx->getCarbonProjectWidget(), SIGNAL(canAddSources(bool)), this, SIGNAL(enableAddSources(bool)));

  CarbonProject* proj = mContext->getCarbonProjectWidget()->project();
  proj->getProjectOptions()->registerPropertyChanged("Simulation Script", "simulationScriptChanged", this);

  CarbonConsole* console = proj->getProjectWidget()->getConsole();
  INFO_ASSERT(console, "Expecting Console");

  CQT_CONNECT(console, simulationStarted(CarbonConsole::CommandMode,
      CarbonConsole::CommandType), this, simulationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType));
  CQT_CONNECT(console, simulationEnded(CarbonConsole::CommandMode,
      CarbonConsole::CommandType), this, simulationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType));
}

// SLOTS

void InvisibleProject::simulationScriptChanged(const CarbonProperty*, const char* newValue)
{
  emit enableSimulate(newValue && strlen(newValue) > 0);
  qDebug() << "enable simulate button" << newValue;
}

void InvisibleProject::simulationStarted(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  emit enableSimulate(false);
  MDIProjectTemplate* projTempl = dynamic_cast<MDIProjectTemplate*>(getDocumentTemplate());
  if (projTempl)
    projTempl->compilationModeChanged(true);
  qDebug() << "sim start";
}

void InvisibleProject::simulationEnded(CarbonConsole::CommandMode, CarbonConsole::CommandType)
{
  emit enableSimulate(true);
  MDIProjectTemplate* projTempl = dynamic_cast<MDIProjectTemplate*>(getDocumentTemplate());
  if (projTempl)
    projTempl->compilationModeChanged(false);

  emit enableSimulate(true);

  qDebug() << "sim end";
}

void InvisibleProject::closeProject()
{
  bool saveFile = true;
  bool closeProject = true;

  if (mContext->getCarbonProjectWidget()->isModified())
  {
   QMessageBox::StandardButton ret;
    ret = QMessageBox::warning(this, MODELSTUDIO_TITLE,
      "The Project has unsaved changes.\nDo you want to save your changes before closing?",
      QMessageBox::Save | QMessageBox::Discard
      | QMessageBox::Cancel);
    if (ret == QMessageBox::Save)
    {
      saveFile = true;
      closeProject = true;
    }
    else if (ret == QMessageBox::Cancel)
    {
      closeProject = false;
      saveFile = false;
    }
  }
  if (saveFile)
    mContext->getCarbonProjectWidget()->saveProject();
  
  if (closeProject)
  {  
    mContext->getCarbonProjectWidget()->closeProject();
    mDocTemplate->deactivateDocument(this);
  }
}

void InvisibleProject::saveProject()
{
  // first save any data in memory
  bool saved = mContext->getCarbonProjectWidget()->saveProject();
  // let the widgets write the data
  if (saved)
  {
    mContext->getDocumentManager()->saveAllDocuments(mContext->getWorkspace());
    // save any changes done by the widgets 
    mContext->getCarbonProjectWidget()->saveProject();
  }
}

void InvisibleProject::configurationCopy(const char*, const char*)
{
  // Nothing to implement here.
}


void InvisibleProject::configurationChanged(const char*)
{
  if (mConfigComboBox)
  {
    mConfigComboBox->blockSignals(true);
    updateConfigurationsComboBox(mConfigComboBox);
    mConfigComboBox->blockSignals(false);
  }
}

void InvisibleProject::configurationRenamed(const char* oldName, const char* newName)
{
  if (mConfigComboBox)
  {
    int index = mConfigComboBox->findText(oldName);
    if (index != -1)
    {
      mConfigComboBox->setItemText(index, newName);
    }
  }
}

void InvisibleProject::configurationRemoved(const char* name)
{
  if (mConfigComboBox)
  {
    int index = mConfigComboBox->findText(name);
    if (index != -1)
      mConfigComboBox->removeItem(index);

  }
}
void InvisibleProject::configurationAdded(const char* name)
{
  if (mConfigComboBox)
  {
    mConfigComboBox->addItem(name);
  }
}

void InvisibleProject::projectCheck()
{ 
  if (mContext->getCarbonProjectWidget()->check())
  {
    QDockWidget* dw = mContext->getCarbonProjectWidget()->context()->getWorkspaceModelMaker()->getErrorInfoDockWindow();
    ErrorInfoWidget* eiw = static_cast<ErrorInfoWidget*>(dw->widget());
    int errors = eiw->getInfoWidget()->getErrors();
    int warnings = eiw->getInfoWidget()->getWarnings();
    // lower followed by raise causes it to "pop" forward
    dw->lower();
    dw->raise();
    mContext->getMainWindow()->statusBar()->showMessage(tr("Problems found"), 2000);
    QMessageBox::information(mContext->getMainWindow(), MODELSTUDIO_TITLE,
                             tr("%1 Errors and %2 Warnings were found during pre-compile check(s).")
                             .arg(errors)
                             .arg(warnings), QMessageBox::Ok);
  }
  else
    QMessageBox::information(mContext->getMainWindow(), MODELSTUDIO_TITLE,
      tr("No Errors or Warnings were found during pre-compile check(s)."), QMessageBox::Ok);
}

void InvisibleProject::projectCancelCompile()
{
  mContext->getCarbonProjectWidget()->stopCompilation();
}

void InvisibleProject::projectCompile()
{
  mContext->getCarbonProjectWidget()->compile(false, true, false);
}

void InvisibleProject::projectReCompile()
{
  mContext->getCarbonProjectWidget()->compile(true, true, false);
}

void InvisibleProject::projectBatchBuild()
{
  DlgBatchBuild dlg( mContext->getCarbonProjectWidget() );
  dlg.setProject( mContext->getCarbonProjectWidget());
  dlg.exec();
}

void InvisibleProject::projectPackage()
{
  // First open the Package Options form.
  PackageToolWizardWidget* packageWidget = mContext->getCarbonProjectWidget()->openPackageOptions();

  if (packageWidget && packageWidget->validateForPackaging())
    mContext->getCarbonProjectWidget()->compile(false, false, true);
}

void InvisibleProject::projectBuildPrecompilationHierarchy()
{
  mContext->getCarbonProjectWidget()->actionBuildPrecompiledHierarchy();
}

void InvisibleProject::projectClean()
{
  mContext->getCarbonProjectWidget()->compile(true, false, false);
}

void InvisibleProject::projectProperties()
{
  mContext->getCarbonProjectWidget()->properties();
}

void InvisibleProject::projectRunScript()
{
  DlgRunScript* dlg = new DlgRunScript();
  dlg->show();
}

void InvisibleProject::projectConfigurationManager()
{
  mContext->getCarbonProjectWidget()->actionConfigurationManager();
}

void InvisibleProject::projectCopyConfiguration()
{
  mContext->getCarbonProjectWidget()->actionCopyConfiguration();
}

void InvisibleProject::projectWindow()
{
  QDockWidget* dock = mContext->getWorkspaceModelMaker()->getProjectDockWindow();
  bool vis = !dock->isVisible();
  dock->setVisible(vis);
}
void InvisibleProject::propWindow()
{
  QDockWidget* dock = mContext->getWorkspaceModelMaker()->getPropertiesDockWindow();
  bool vis = !dock->isVisible();
  dock->setVisible(vis);
}

void InvisibleProject::toggleDockWindow()
{
  QAction* action = qobject_cast<QAction*>(sender());

  QVariant qv = action->property("CarbonDockWidget");
  QDockWidget* dockWidget = (QDockWidget*)qv.value<void*>();
  
  bool hidflag = dockWidget->isHidden();
  bool visflag = dockWidget->isVisible();

  qDebug() << action->text() << " hidden: " << hidflag << " visflag: " << visflag;

  dockWidget->setVisible(!visflag);
}

void InvisibleProject::hierWindow()
{
  QDockWidget* dock = mContext->getCarbonProjectWidget()->getHierarchyDockWidget();
  bool vis = !dock->isVisible();
  dock->setVisible(vis);
}

void InvisibleProject::currentConfigurationIndexChanged(int)
{
  QComboBox *cbox = qobject_cast<QComboBox *>(sender());
  UtString cfgName;
  cfgName << cbox->currentText();
  if (cfgName.length() > 0)
    mContext->getCarbonProjectWidget()->putConfiguration(cfgName.c_str());
}

void InvisibleProject::updateConfigurationsComboBox(QComboBox* cbox)
{
  bool hookupSignal = false;

  if (mConfigComboBox != cbox)
    hookupSignal = true;

  mConfigComboBox = cbox;
 
  mConfigComboBox->setSizeAdjustPolicy(QComboBox::AdjustToContents);

  CarbonProject* project = mContext->getCarbonProjectWidget()->project();
  cbox->clear();

  CarbonConfigurations* configs = project->getConfigurations();

  if (configs)
  {
    mConfigComboBox->setEnabled(true);

    for (UInt32 i=0; i<configs->numConfigurations(); ++i)
    {
      CarbonConfiguration* config = configs->getConfiguration(i);
      if (cbox->findText(config->getName()) == -1)
        cbox->addItem(config->getName());
    }

    cbox->setCurrentIndex(cbox->findText(project->getActiveConfiguration()));

    if (hookupSignal)
    {
      connect(cbox, SIGNAL(currentIndexChanged(int)), this, SLOT(currentConfigurationIndexChanged(int)));
      mContext->getSettingsEditor()->registerActiveConfigurationChange(cbox);
    }
  }
}



