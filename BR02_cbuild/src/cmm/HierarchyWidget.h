#ifndef HIERARCHYWIDGET_H
#define HIERARCHYWIDGET_H
#include "util/CarbonPlatform.h"
#include <QObject>

#include <QTreeWidget>

class CarbonMakerContext;

class HierarchyWidget : public QTreeWidget
{
  Q_OBJECT

public:
  HierarchyWidget(QWidget *parent);
  ~HierarchyWidget();
  void putContext(CarbonMakerContext* ctx);

private:
  void populate();

private:
  CarbonMakerContext* mContext;
};

#endif // HIERARCHYWIDGET_H
