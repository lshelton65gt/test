#ifndef __MDI_H___
#define __MDI_H___
#include "util/CarbonPlatform.h"

#include <QTextEdit>
#include <QTabWidget>
#include <QMenuBar>
#include <QToolBar>
#include <QMainWindow>
#include <QAction>
#include <QWorkspace>

#include "util/UtHashMap.h"
#include "util/UtString.h"

class MDIDocumentTemplate;
class MDIWidget;

#define MDI_ACTION "CarbonMDIAction"
#define MDI_ACTION_UPDATE_SIGNAL "CarbonMDIActionUpdateSignal"
#define MDI_ACTION_UPDATE_SLOT "CarbonMDIActionUpdateSlot"
#define MDI_FRAME "CarbonMDIFrame"
#define MDI_DOCUMENT "CarbonMDIDocument"

struct MDIFriendToolbar
{
  MDIFriendToolbar(MDIDocumentTemplate* mdiTemplate, QToolBar* toolbar)
  {
    mDocTemplate = mdiTemplate;
    mToolBar = toolbar;
  }

  QToolBar* getToolBar() { return mToolBar; }
  MDIDocumentTemplate* getDocumentTemplate() { return mDocTemplate; }

private:
  MDIDocumentTemplate* mDocTemplate;
  QToolBar* mToolBar;
};

class MDIDocumentTemplate : public QObject
{
  Q_OBJECT
public:
  MDIDocumentTemplate(const char* templateName, const char* newPromptName, const char* openFilter)
  {
    mNewPrompt = newPromptName;
    mDocTemplateName = templateName;
    mDocFilter = openFilter;
    mMenus.clear();
    mInvisibleDocument=false;
    mMainWindow=NULL;
  }
  virtual ~MDIDocumentTemplate() {}

  // Create a new MDI Document
  virtual MDIWidget* createNewDocument(QWidget* parent=0);
  // Widget can have menu(s)
  virtual int createMenus() { return 0; }
  // Widget can have toolbar(s)
  virtual int createToolbars() { return 0; }
  // Update the GUI state of the menus & toolbars
  virtual void updateMenusAndToolbars(QWidget*){}
  // Open a "Document"
  virtual MDIWidget* openDocument(QWidget*, const char* /*docName*/) { return NULL; }
  // Get the document filter(s)
  virtual const char* getFilter() { return mDocFilter.c_str(); }
  // Find an open document, possibly activating it
  virtual MDIWidget* findDocument(QWorkspace* ws, const char* docName, bool activate);
  // Create a new document
  virtual MDIWidget* createDocument(QWidget*, const char*) { return NULL; }

  // Initialize the document (call this once)
  void initialize(QMainWindow* mw);
  
  // Mark this document as an Invisible one
  void setInvisibleDocument(bool newVal) { mInvisibleDocument=newVal; }
  bool isInvisibleDocument() { return mInvisibleDocument; }

  // Activate a Document
  void activateDocument(QWidget* w);
  
  // Deactivate a Document
  void deactivateDocument(QWidget* w);

  // Friend templates retain their menus/toolbars when activated
  void addFriendTemplateToolbar(MDIDocumentTemplate* friendTemplate, QToolBar* friendToolbar)
  {
    MDIFriendToolbar* ft = new MDIFriendToolbar(friendTemplate, friendToolbar);
    mFriendTemplates.append(ft);
  }
 
  const char* getDocTemplateName() const { return mDocTemplateName.c_str(); }

  QToolBar* getToolbar(int i)
  {
    INFO_ASSERT(i < mToolBars.count(), "Toolbar index invalid");
    return mToolBars[i];
  }

  QMenu* getMenu(int i)
  {
    INFO_ASSERT(i < mMenus.count(), "Menu index invalid");
    return mMenus[i];
  }
protected:
  const char* getName() {return mDocTemplateName.c_str(); }
  int numToolbars() { return mToolBars.count(); }
  int numMenus() { return mMenus.count(); }

  void installToolbars(QMainWindow* mw);
  void installMenus(QMainWindow* mw);
  void registerAction(QAction* act, const char* slotName);
  void registerUpdateAction(QAction* action, const char* signal, const char* slot);

// Protected Methods
protected:
  void showMenusAndToolbars(bool showFlag);

  void addToolbar(QToolBar* tb)
  {
    tb->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    mToolBars.append(tb);
  }
  void addMenu(QMenu* m)
  {
    mMenus.append(m);
  }

  QAction* findMenu(QMenuBar* mb, const char* menuName);

// Private Methods
private:
  const char* slotName(QAction* action);
  const char* updateSlotName(QAction* action);
  const char* updateSignalName(QAction* action);

  void updateUI(QWidget*);
  void updateMenusAndToolbars(bool showFlag);
  void hideInactiveMenus();
  bool hasActiveActions(QMenu* menu);
  bool isAddedMenu(QMenu* menu);

// Private Data
private:
  UtString mDocTemplateName;
  UtString mNewPrompt;
  UtString mDocFilter;

  bool mInvisibleDocument;

  QList<QMenu*> mMenus;
  QList<QToolBar*> mToolBars;

  QList<QToolBar*> mAddedToolbars;
  QList<QMenu*> mAddedMenus;
  QList<QAction*> mAddedActions;
  QList<MDIFriendToolbar*> mFriendTemplates;
  QMainWindow* mMainWindow;
};


//
// MDIWidget - Turns regular widgets into MDI widgets
// to use this class simply derive from it and you must call initializeMDI in
// the constructor
//
class MDIWidget
{
public:
  MDIWidget()
  {
    mDocTemplate = NULL;
    mWidget = NULL;
    modifiedFlag = false;
  }
 
  virtual ~MDIWidget() {}
  QWidget* getWidget() { return mWidget; }
  virtual const char* userFriendlyName() {return "";}
  virtual void saveDocument() {}
  virtual void widgetActivated() {}

  MDIDocumentTemplate* getDocumentTemplate() { return mDocTemplate; }

  void initializeMDI(QWidget* w, MDIDocumentTemplate* t)
  {
    INFO_ASSERT(w != NULL, "Widget cannot be NULL");
    INFO_ASSERT(t != NULL, "Invalid DocTemplate");

    mWidget = w;
    mDocTemplate = t;

    QVariant qv1 = qVariantFromValue((void*)w);
    w->setProperty(MDI_FRAME, qv1);

    QVariant qv2 = qVariantFromValue((void*)t);
    w->setProperty(MDI_DOCUMENT, qv2);
  }

  void closeEvent(QWidget* widget)
  {
    QVariant qv = widget->property("CarbonTabWidget");
    QVariant qvPlaceholder = widget->property("CarbonTabPlaceholder");

    if (qv.isValid() && qvPlaceholder.isValid())
    {
      QTabWidget* tw = (QTabWidget*)qv.value<void*>();
      QWidget* w = (QWidget*)qvPlaceholder.value<void*>();

      int tabIndex = tw->indexOf(w);
      if (tabIndex != -1)
        tw->removeTab(tabIndex);
    }
  }
  void setWidgetModified(bool newVal) { modifiedFlag = newVal; }
  bool isWidgetModified() { return modifiedFlag; }

protected:
  MDIDocumentTemplate* mDocTemplate;
  QWidget* mWidget;
  bool modifiedFlag;
};

// Manager of all DocumentTemplates
class MDIDocumentManager
{
public:
  MDIDocumentManager(QMainWindow* mw)
  {
    mMainWindow = mw;
    mPreviousWindow = NULL;
  }
  bool promptOpenDocument(QWidget* parent, QStringList& files);
  MDIDocumentTemplate* findTemplate(const char* fileName, const char* docType=NULL);

  void putWorkspace(QWorkspace* ws) { mWorkspace=ws; }
  QWorkspace* getWorkspace() { return mWorkspace; }

  void addDocTemplate(MDIDocumentTemplate* t)
  {
   // t->initialize(mMainWindow);
    mDocTemplates.append(t);
  }


  void initializeTemplates()
  {
    foreach(MDIDocumentTemplate* t, mDocTemplates)
      t->initialize(mMainWindow);
  }

  void saveAllDocuments(QWorkspace* ws);

  void windowActivated(QWidget* win)
  {
    if (mPreviousWindow != NULL)
    {
      QVariant qv = mPreviousWindow->property(MDI_DOCUMENT);
      INFO_ASSERT(qv.isValid(), "Widget must define a MDI_DOCUMENT property");
      MDIDocumentTemplate* docTempl = (MDIDocumentTemplate*)qv.value<void*>();
      docTempl->deactivateDocument(mPreviousWindow);
      mPreviousWindow = NULL;
    }

    if (win != NULL)
    {
      QVariant qv = win->property(MDI_FRAME);

      if (!qv.isValid()) // It's not an MDI frame
        return;

      QVariant qv1 = win->property(MDI_DOCUMENT);
      INFO_ASSERT(qv1.isValid(), "Widget must define an MDI_DOCUMENT property");

      MDIDocumentTemplate* docTempl = (MDIDocumentTemplate*)qv1.value<void*>();
      docTempl->activateDocument(win);
      mPreviousWindow = win;
    }

    // Perhaps we should only save this when the previous window was an MDI document?
    if (win == NULL)
      mPreviousWindow = win;
  }

private:
  
private:
  QList<MDIDocumentTemplate*> mDocTemplates;
  QMainWindow* mMainWindow;
  QWidget* mPreviousWindow;
  UtString mLastDir;
  QWorkspace* mWorkspace;
};


#endif
