//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonVersion.h"
#include "gui/CQt.h"

#include "MemoryWizard.h"
#include "WizPgStart.h"
#include "cmm.h"
#include "CarbonMakerContext.h"
#include "CarbonProjectWidget.h"
#include "CarbonConsole.h"

WizPgStart::WizPgStart(QWidget *parent)
: WizardPage(parent)
{
  ui.setupUi(this);

  setTitle("Single or Multiple Models");
  setSubTitle("The Remodeling Wizard can remodel a single memory or multiple at once.");

}

WizPgStart::~WizPgStart()
{
}

void WizPgStart::initializePage()
{
  char* envEnableMulti = getenv("CARBON_REMODEL_WIZARD");
  ui.radioButtonMulti->setEnabled(envEnableMulti != NULL);
}

bool WizPgStart::validatePage()
{
  MemoryWizard* wiz = static_cast<MemoryWizard*>(wizard());
 
  if (ui.radioButtonSingle->isChecked())
    wiz->setMode(MemoryWizard::eWizardModeSingle);
  else
  {
    CarbonProjectWidget* pw = theApp->getContext()->getCarbonProjectWidget();
    CarbonProject* proj = pw->project();
    bool designHierarchyExists = false;
    if (proj)
    {
      const char* designHierarchyFile = proj->getDesignFilePath(".designHierarchy");
      QFileInfo fi(designHierarchyFile);
      designHierarchyExists = fi.exists();
    }

    if (designHierarchyExists)
      wiz->setMode(MemoryWizard::eWizardModeMulti);
    else
    {
      QMessageBox::critical(NULL, "Missing Design Hierarchy",
        "You must first compile the design by using the Explore Hierarchy command\nor by compiling the design by clicking the Compile button,\nbefore you can begin the remodelling process.");
      return false;
    }
  }

  return true;
}

int WizPgStart::nextId() const
{
  MemoryWizard* wiz = (MemoryWizard*)wizard();
  if (wiz->getMode() == MemoryWizard::eWizardModeSingle)
    return MemoryWizard::PageTemplates;
  else
    return MemoryWizard::PageStartMulti;
}
