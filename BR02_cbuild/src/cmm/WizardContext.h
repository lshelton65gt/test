#ifndef __WIZARDCONTEXT_H
#define __WIZARDCONTEXT_H

#include "util/CarbonVersion.h"
#include "gui/CQt.h"
#include <QtGui>

#include "ScriptingEngine.h"
#include "DlgParameters.h"
#include "Template.h"
#include "ElaboratedModel.h"

// File: Context
//
// Description:
// The toplevel context object
// This object is passed to the main entry points
class WizardContext : public QObject
{
  Q_OBJECT

  // Property: template
  // The XML <Template> used in this context
  Q_PROPERTY(QObject* template READ propGetTemplate)
  // Property: model
  // The <Model> used by this context
  Q_PROPERTY(QObject* model READ getModel)

  // Property: designHierarchy
  Q_PROPERTY(QObject* designHierarchy READ getDesignHierarchy)
  
  QObject* getDesignHierarchy();
public:
  WizardContext(ScriptingEngine* engine);
  
  void setEnableDebugger(bool value)
  {  
    mEnableDebugger=value; 
  }

  QString generateCode();

  Template* getTemplate() { return &mTemplate; }
  QScriptValue callScriptFunction(const QString& methodName, const QScriptValueList args)
  {
    return mEngine->callScriptFunction(methodName, args);
  }
  QScriptValue callOptionalScriptFunction(const QString& methodName, const QScriptValueList args)
  {
    return mEngine->callOptionalScriptFunction(methodName, args);
  }
  
  ScriptingEngine* getScriptEngine() const { return mEngine; }

  ElaboratedModel* getModel();

  void setModeName(const QString& modeName) { mModeName=modeName; }
  QString getModeName() { return mModeName; }

private:
  void addBuiltinFunctions();

// Property getter/setters
private:
  QObject* propGetTemplate() { return &mTemplate; }
 
private:
  bool mEnableDebugger;
  QString mModeName;
  Template mTemplate;
  ElaboratedModel mModel;
  ScriptingEngine* mEngine;
};

#endif
