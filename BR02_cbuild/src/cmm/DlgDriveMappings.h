#ifndef DLGDRIVEMAPPINGS_H
#define DLGDRIVEMAPPINGS_H

#include "util/CarbonPlatform.h"

#include <QDialog>

#include "gui/CQt.h"
#include "util/OSWrapper.h"
#include "CarbonProjectWidget.h"
#include "ui_DlgDriveMappings.h"

class DlgDriveMappings : public QDialog
{
  Q_OBJECT

public:
  DlgDriveMappings(QWidget *parent = 0);
  ~DlgDriveMappings();

private slots:
  void on_buttonBox_rejected();
  void on_buttonBox_accepted();
  void on_pushButtonAdd_clicked();
  void on_pushButtonRemove_clicked();

private:
  void populate();

private:
  CarbonMappedDrives mMappedDrives;
  Ui::DlgDriveMappingsClass ui;
};

#endif // DLGDRIVEMAPPINGS_H
