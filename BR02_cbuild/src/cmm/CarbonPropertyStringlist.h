#ifndef __CARBONPROPSTRINGLIST_H__
#define __CARBONPROPSTRINGLIST_H__

#include "util/CarbonPlatform.h"
#include <QObject>
#include "CarbonProperty.h"
#include "PropertyEditor.h"

#include "util/UtString.h"
#include "util/UtStringArray.h"

class CarbonPropertyStringlist : public CarbonProperty
{
public:
  CARBONMEM_OVERRIDES

  enum RepeatKind { OptionNameValue=0, ValuesOnly, ValuesOnlyOnePerLine, OptionNameValueCompressed};

  virtual CarbonDelegate* createDelegate(QObject* parent,  PropertyEditorDelegate* d, PropertyEditor* propEd);
  virtual bool parseXML(xmlNodePtr parent, UtXmlErrorHandler* eh);
  virtual bool writeValueXML(xmlTextWriterPtr writer, CarbonPropertyValue* value, UtXmlErrorHandler* eh);
  virtual bool readValueXML(xmlNodePtr parent, CarbonOptions* options, UtXmlErrorHandler* eh) const;

  CarbonPropertyStringlist(const char* name, int nv, const char* descr) : CarbonProperty(name,nv,descr,CarbonProperty::Stringlist)
  {
    mKind = OptionNameValue;
  }
  
  int numStrings() { return mStrings.size(); }
  void addString(const char* str)
  {
    mStrings.push_back(str);
  }
  void removeString(const char* str)
  {
    mStrings.remove(str);
  }

  RepeatKind getKind() const { return mKind; }
  const char* getString(UInt32 i) { return mStrings[i]; }

private:
  UtStringArray mStrings;
  RepeatKind mKind;
};


class CarbonDelegateStringlist : public CarbonDelegate
{
  Q_OBJECT
public:
  CarbonDelegateStringlist(QObject *parent = 0, PropertyEditorDelegate* d=0, PropertyEditor* e=0);
  QWidget* createEditor(QWidget *parent, QTreeWidgetItem* item, CarbonProperty* prop, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QTreeWidgetItem* item, CarbonProperty* prop, QAbstractItemModel *model, const QModelIndex &index) const;

private slots:
  void commitAndCloseEditor();
  void showStringlistDialog();
};


#endif
