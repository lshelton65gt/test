//! -*-C++-*-
/***************************************************************************************
  Copyright (c) 2011 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include "PortsTree.h"
#include "WizardContext.h"
#include "ElaboratedModel.h"
#include "BlockInstance.h"
#include "Ports.h"
#include "PortInstance.h"
#include "Mode.h"

void PortsTree::itemChanged(QTreeWidgetItem* item, int col)
{
  if (col == colUSERPORT)
  {
    qDebug() << "changed" << item->text(col);
    QVariant v = item->data(0, Qt::UserRole);
    if (!v.isNull())
    {
      PortInstance* portInst = (PortInstance*)v.value<void*>();
      QString value = item->text(colUSERPORT);
      portInst->getPort()->setBaseName(value);
      Port* port = portInst->getBlock()->getPorts()->findPort(portInst->getPort()->getName());
      if (port)
        port->setBaseName(item->text(colUSERPORT));
    }
  }
}

PortsTree::PortsTree(QWidget *parent)
: QTreeWidget(parent)
{
  mContext = NULL;

  setColumnCount(4);

  QStringList colNames;

  colNames << "Block";
  colNames << "Port";
  colNames << "Base Name";
  colNames << "Description";

  setRootIsDecorated(false);
  setAlternatingRowColors(false);
  setSelectionMode(QAbstractItemView::SingleSelection);
  setSelectionBehavior(QAbstractItemView::SelectRows);

  setHeaderLabels(colNames);

  CQT_CONNECT(this, itemChanged(QTreeWidgetItem*,int), this, itemChanged(QTreeWidgetItem*,int));
}

PortsTree::~PortsTree()
{
}

void PortsTree::populate(WizardContext* context)
{
  mContext = context;

  clear();

  setSortingEnabled(false);
  
  ElaboratedModel* model = context->getModel();
  PortInstances* instances = model->getUniquePorts();

  for (int i=0; i<instances->getCount(); i++)
  {
    PortInstance* portInst = instances->getAt(i);
    addItem(portInst);
  }

  setSortingEnabled(true);
  sortItems(1, Qt::AscendingOrder);
}

void PortsTree::addItem(PortInstance* portInst)
{
  QTreeWidgetItem* item = new QTreeWidgetItem(this);
  item->setFlags(item->flags() | Qt::ItemIsEditable);

  BlockInstance* blockInst = portInst->getBlockInstance();

  QVariant qv = qVariantFromValue((void*)portInst);
  item->setData(0, Qt::UserRole, qv);

  QString baseName = portInst->getPort()->getBaseName();
  item->setText(colBLOCKINST, blockInst->getInstanceName());
  item->setText(colPORT, portInst->getUnelaboratedName());
  item->setText(colUSERPORT, "");

  int ix = -1;
  QString temp = portInst->getUnelaboratedName();

  do // repeat until no more $(var)s
  {
    QRegExp exp1("\\$\\((\\w+)\\)");
    exp1.setPatternSyntax(QRegExp::RegExp);
    ix = exp1.indexIn(temp);
    int i2 = exp1.matchedLength();
    int nc = exp1.numCaptures();
    if (ix != -1 && nc == 1)
    {
      qDebug() << "portExpansion" << portInst->getName() << exp1.cap(1);

      item->setText(colUSERPORT, exp1.cap(1));

      QString left = temp.left(ix);
      QString rest = temp.mid(ix+i2);
      temp = left + "${" + exp1.cap(1) + "}" + rest;
    }
  }
  while (ix != -1);
  
  if (baseName.length() > 0)
    item->setText(colUSERPORT, baseName);

  item->setText(colDESCRIPTION, portInst->getPort()->getDescription());
} 

void PortsTree::commitData()
{
  for (int i=0; i<topLevelItemCount(); i++)
  {
    QTreeWidgetItem* item = topLevelItem(i);
    QVariant v = item->data(0, Qt::UserRole);
    if (!v.isNull())
    {
      PortInstance* portInst = (PortInstance*)v.value<void*>();
      QString value = item->text(colUSERPORT);
      portInst->getPort()->setBaseName(item->text(colUSERPORT));

      qDebug() << "commitData" << portInst->getPort()->getName() << portInst->getUnelaboratedName();

      Port* port = portInst->getBlock()->getPorts()->findPort(portInst->getPort()->getName());
      if (port)
        port->setBaseName(item->text(colUSERPORT));
    }
  }
}


