#ifndef DLGFINDINFILES_H
#define DLGFINDINFILES_H

#include "util/CarbonPlatform.h"
#include "gui/CQt.h"

#include <QtGui>
#include <QDialog>
#include <QThread>
#include "ui_DlgFindInFiles.h"

// These must match the ComboBox entries for comboBoxLookIn
#define SEARCH_PROJECT "Entire Project"
#define SEARCH_INCLUDES "Search RTL Include Directories"

class XDesignHierarchy;

class SearchThread : public QThread
{
  Q_OBJECT

public:

  enum SearchKind { eSearchDirectory, eSearchFiles };

  SearchThread(QObject *parent = 0) : QThread(parent)
  {
    mAbort = false;
    mRestart = false;
  }

  ~SearchThread()
  {
    mMutex.lock();
    mAbort = true;
    mCondition.wakeOne();
    mMutex.unlock();
    wait();
  }

  void search(QRegExp::PatternSyntax patSyntax, Qt::CaseSensitivity caseSense, 
    bool recurseFlag, const QStringList& dir, const QString& searchString, const QStringList& fileKinds,
    bool matchWholeWord);

  void searchFiles(const QStringList& fileList, const QStringList& fileKinds, QRegExp::PatternSyntax patSyntax, Qt::CaseSensitivity caseSense, 
    const QString& searchString,bool matchWholeWord);

signals:
  void searchCompleted(int fileCount, int matchesFound, int totalFiles);
  void matchFound(const QString& filename, int lineNumber, const QString& matchingLine);
  void updateStatus(const QString& filename);

protected:
  void run();
  void search_directory(int *numFiles, int *numMatches, int *totalFiles, QString stringToMatch, 
    QDir& dir, const QStringList& fileKinds, bool recurseFlag, 
    QRegExp::PatternSyntax pat, Qt::CaseSensitivity caseSense, bool matchWholeWord);
  void search_files(const QStringList& fileList,  const QStringList& fileKinds, int *numFiles, int *numMatches, int *totalFiles,
                                      QString stringToMatch,
                                    QRegExp::PatternSyntax pat, Qt::CaseSensitivity caseSense, bool matchWholeWord);
  int searchFile(const QString& filePath, const QString& stringToMatch, 
    QRegExp::PatternSyntax pat, Qt::CaseSensitivity caseSense, bool matchWholeWord);
  bool binaryText(const QString& line);

private:
  QRegExp::PatternSyntax mPatSyntax;
  Qt::CaseSensitivity mCaseSense;
  bool mRecurseFlag;
  bool mMatchWholeWord;
  QStringList mDirs;
  QString mSearchString;
  QStringList mFileKinds;
  QStringList mSearchFiles;
  
  bool mAbort;
  bool mRestart;
  QMutex mMutex;
  QWaitCondition mCondition;
  SearchKind mSearchKind;
};


class DlgFindInFiles : public QDialog
{
  Q_OBJECT

public:
  enum SearchKind { eSearchProject, eSearchDirectory, eSearchIncludes} ;

  DlgFindInFiles(QWidget *parent = 0);
  ~DlgFindInFiles();

  virtual void closeEvent(QCloseEvent* ev);

private slots:
  void on_checkBoxUse_stateChanged(int);
  void on_comboBoxLookIn_currentIndexChanged(int);
  void on_toolButtonBrowse_clicked();
  void on_comboBoxLookIn_editTextChanged(const QString&);
  void on_pushButtonFindAll_clicked();

  // Signals from worker thread
  void matchFound(const QString&, int, const QString&);
  void searchCompleted(int numFiles, int numMatches, int totalFiles);
  void searchStarted(QString key, QString location, bool subfolders);
  void updateStatus(const QString&);

private:
  void saveDirectory(const QString& dirName);
  void saveStrings(const QString& searchString);
  void searchProject(const QString& stringToMatch, const QStringList& fileKinds);
  void searchIncludes(const QString& stringToMatch, const QStringList& fileKinds);

private:
  QSettings mSettings;
  SearchThread mWorkerThread;
  XDesignHierarchy* mDesignHierarchy;

private:
  Ui::DlgFindInFilesClass ui;
};

#endif // DLGFINDINFILES_H
