// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "symtab/STSymbolTable.h"
#include "util/AtomicCache.h"
#include "util/OSWrapper.h"
#include "util/ShellMsgContext.h"
#include "hdl/HdlId.h"
#include "util/Zstream.h"
#include "util/StringAtom.h"
#include "util/UtArray.h"
#include "util/UtStackPOD.h"
#include "util/UtString.h"
#include "util/CarbonAssert.h"

static const char scSymTabSignature[] = "Carbon Design Systems SymTab Header";
static const SInt32 scSymTabVersion = 1; 

static const char scDBBranchType = 'B'; 
static const char scDBLeafType = 'L'; 
static const char scDBEndType = 'E'; 
static const char scDBFieldEnd = 'F';
static const char scDBHasStorage = 's';
static const char scDBNoStorage = 'e'; // 'e' for empty

class STSymbolTable::DBLeaf
{
public: CARBONMEM_OVERRIDES
DBLeaf(const STAliasedLeafNode* leaf, SInt32 index) {
  mLeaf = leaf;
  mIndex = index;
}
    
  const STAliasedLeafNode* mLeaf;
  SInt32 mIndex;
};

class STSymbolTable::DBWriteClosure
{
public: CARBONMEM_OVERRIDES
DBWriteClosure(ZostreamDB* f, STNodeSelectDB* nodeSelect,
               bool fieldWrite) { 
  mOutFile = f;
  mNumTotNets = 0;
  mNumTotBrs = 0;
  mPIndex = -1;
  mNodeSelect = nodeSelect;
  mFieldWrite = fieldWrite;
}

  typedef UtArray<DBLeaf*> DBLeafArray;
  typedef Loop<DBLeafArray> DBLeafArrayLoop;

  ~DBWriteClosure() {
    for (LeafVecDB::UnsortedLoop m = mAliasRings.loopUnsorted();
         ! m.atEnd(); ++m)
    {
      DBLeafArray& vec = (*m).second;
      for (DBLeafArrayLoop p(vec); 
           ! p.atEnd(); ++p)
      {
        DBLeaf* leaf = *p;
        if (leaf)
          delete leaf;
      }
    }
  }

#ifdef OUT    
#undef OUT
#endif

  ZostreamDB& OUT() { return *mOutFile;}

  UInt32 getAtomIndex(const HierName* name) const {
    AtomIntMap::const_iterator p = mCacheIndices.find(name->strObject());
    ST_ASSERT(p != mCacheIndices.end(), name);
    return (*p).second;
  }

  void pushBrIndex() {
    mBranchIndices.push(mPIndex);
  }

  void popBrIndex() {
    mPIndex = mBranchIndices.top();
    mBranchIndices.pop();
  }

  typedef UtHashMap<const StringAtom*, UInt32>  AtomIntMap;
  AtomIntMap mCacheIndices;
  typedef UtHashMap<const STAliasedLeafNode*, DBLeafArray > LeafVecDB;
  LeafVecDB mAliasRings;
  typedef UtArray<const STBranchNode*> ConstBranchVec;
  typedef CLoop<ConstBranchVec> ConstBranchVecCLoop;
  ConstBranchVec mBranches;
  typedef UtArray<const STAliasedLeafNode*> ConstLeafVec;
  typedef CLoop<ConstLeafVec> ConstLeafVecCLoop;

  ConstLeafVec mLeaves;
    
  UtStackPOD<SInt32> mBranchIndices;

  ZostreamDB* mOutFile;
  STNodeSelectDB* mNodeSelect;
  bool mFieldWrite;

  SInt32 mNumTotNets;
  SInt32 mNumTotBrs;
  SInt32 mPIndex;
};

class STSymbolTable::DBReadClosure
{
public: CARBONMEM_OVERRIDES
DBReadClosure(ZistreamDB* f, MsgContext* msg) {
  mInFile = f;
  mStat = STFieldBOM::eReadOK;
  mIndex = -1;
  mPIndex = -1;
  mNameObj = NULL;
  mMsg = msg;
  mSymTabVersion = -1;
}

#ifdef IN
#undef IN
#endif

  ZistreamDB& IN() { return *mInFile; }
    
  SInt32 mIndex;
  SInt32 mPIndex;
  ZistreamDB* mInFile;
  StringAtom* mNameObj;
  MsgContext* mMsg;
  STFieldBOM::ReadStatus mStat;
  UtArray<StringAtom*> mCacheList;
  UtArray<STBranchNode*> mBranches;
  STAliasedLeafNode::LeafNodeVec mLeaves;
  SInt32 mSymTabVersion;
};

void STSymbolTable::writeDB(ZostreamDB& out, STNodeSelectDB* nodeSelect,
                            bool writeData)
{
  DBWriteClosure fc(&out, nodeSelect, writeData);
  // write signature and version
  out << scSymTabSignature << scSymTabVersion;
  mFieldBOM->writeBOMSignature(out);
  
  // date
  UtString buf;  
  out << OSGetTimeStr("%b %d, %Y  %H:%M:%S",&buf);
  
  //write hierarchy
  const STSymbolTableNode* node;
  const STBranchNode* branch;
  const STAliasedLeafNode* leaf;

  mFieldBOM->setAtomicCache(mAtomCache); 
  mFieldBOM->copyStringsIntoSymTable();

  writeAtomicCache(fc);

  fc.mPIndex = -1; // not needed, but for clarity. Already -1
  for (RootIter tableIter = getRootIter();!tableIter.atEnd(); ++tableIter)
  {
    node = *tableIter;
    if ((branch = node->castBranch()))
      writeBranchToDB(branch, fc);
    else if ((leaf = node->castLeaf()))
      writeLeafToDB(leaf, fc);
  }
  INFO_ASSERT(fc.mPIndex == -1, "Database write: tree inconsistency");
  out << scDBEndType;

  writeAliases(fc);

  mFieldBOM->preFieldWrite(out);

  writeFields(fc);

}

void STSymbolTable::writeFields(DBWriteClosure& fc)
{
  if (fc.mFieldWrite)
  {
    // branches first, then the leaves
    SInt32 index = 0;
    {
      // Tell the reader that something is written here
      fc.OUT() << SInt32(1); 
      for (DBWriteClosure::ConstBranchVecCLoop b(fc.mBranches);
           ! b.atEnd(); ++b)
      {
        fc.OUT() << index;
        mFieldBOM->writeBranchData(*b, fc.OUT(), mAtomCache);
        fc.OUT() << scDBFieldEnd;      
        ++index;
      }
    } // block
    
    fc.OUT() << scDBEndType;
    index = 0;
    {
      for (DBWriteClosure::ConstLeafVecCLoop a(fc.mLeaves);
           ! a.atEnd(); ++a)
      {
        fc.OUT() << index;
        mFieldBOM->writeLeafData(*a, fc.OUT());
        fc.OUT() << scDBFieldEnd;      
        ++index;
      }
    } // block
  }
  else
    fc.OUT() << SInt32(-1); // so reader knows nothing is written
  fc.OUT() << scDBEndType;
}

void STSymbolTable::writeAtomicCache(DBWriteClosure& fc)
{
  fc.OUT() << mAtomCache->size();
  UInt32 index = 0;
  for (AtomicCache::CacheIter p = mAtomCache->getCacheIter();
       ! p.atEnd(); ++p)
  {
    const StringAtom* id = p.getValue();
    fc.OUT() << index << id->str();
    fc.mCacheIndices[id] = index;
    ++index;
    fc.OUT().mapPtr(id);
  }
  fc.OUT() << scDBEndType;
}

void STSymbolTable::writeBranchToDB(const STBranchNode* branch,
                                    DBWriteClosure& fc)
{
  const STBranchNode* chBranch;
  const STAliasedLeafNode* leaf;
  const STSymbolTableNode* chNode;
  DBWriteClosure::ConstBranchVec branches;
  DBWriteClosure::ConstLeafVec leaves;  
  
  SInt32 numChildren = branch->numChildren();
  for (SInt32 i = 0; i < numChildren; ++i)
  {
    chNode = branch->getChild(i);
    if (chNode)
    {
      if ((chBranch = chNode->castBranch()))
      {
        if (! fc.mNodeSelect || 
            (fc.mNodeSelect && fc.mNodeSelect->selectBranch(chBranch, mHdlPath)))
          branches.push_back(chBranch);
      }
      else if ((leaf = chNode->castLeaf()))
      {
        if (! fc.mNodeSelect || 
            (fc.mNodeSelect && fc.mNodeSelect->selectLeaf(leaf, mHdlPath)))
          leaves.push_back(leaf);
      }
    }
  }
  
  // only write out the branch if there are children
  if (! (leaves.empty() && branches.empty()))
  {
    // Save this parent's index
    SInt32 brIndex = fc.mNumTotBrs;
    
    UInt32 atomIndex = fc.getAtomIndex(branch);
    
    fc.OUT() << scDBBranchType << brIndex << fc.mPIndex 
             << atomIndex;
    fc.OUT().mapPtr(branch);

    fc.mBranches.push_back(branch);
    
    // Up the branch number
    ++(fc.mNumTotBrs);
    
    fc.pushBrIndex();    
    fc.mPIndex = brIndex;

    for (DBWriteClosure::ConstLeafVecCLoop li(leaves); 
         ! li.atEnd(); ++li)
      writeLeafToDB(*li, fc);

    for (DBWriteClosure::ConstBranchVecCLoop bi(branches);
         ! bi.atEnd(); ++bi)
      writeBranchToDB(*bi, fc);
    
    fc.popBrIndex();
  }
}

void STSymbolTable::writeLeafToDB(const STAliasedLeafNode* leaf, 
                                  DBWriteClosure& fc)
{
  if (leaf->hasAliases())
  {
    const STAliasedLeafNode* master = leaf->getMaster();
    DBWriteClosure::DBLeafArray& vec = fc.mAliasRings[master];
    if (vec.empty())
      // new entry, initialize master
      vec.push_back(NULL);
    
    DBLeaf* dbLeaf = new DBLeaf(leaf, fc.mNumTotNets);
    if (leaf == master)
    {
      ST_ASSERT(vec[0] == NULL, leaf);
      vec[0] = dbLeaf;
    }
    else
      vec.push_back(dbLeaf);
  }


  fc.OUT() << scDBLeafType << fc.mNumTotNets << fc.mPIndex
           << fc.getAtomIndex(leaf);
  
  fc.OUT().mapPtr(leaf);

  char storageType = scDBNoStorage;
  const STAliasedLeafNode* storage = leaf->getInternalStorage();
  if ((storage == leaf) || (storage == NULL))
    storageType = scDBHasStorage;
  
  fc.OUT() << storageType;
  
  fc.mLeaves.push_back(leaf);

  ++(fc.mNumTotNets);
}

void STSymbolTable::writeAliases(DBWriteClosure& fc)
{
  DBLeaf* dl;
  typedef UtArray<DBWriteClosure::DBLeafArray*> VecDBLeafVec;
  typedef Loop<VecDBLeafVec> VecDBLeafVecLoop;

  VecDBLeafVec aliasVecs;
  UInt32 numAliasVecs = 0;
  // scrub the map
  for (DBWriteClosure::LeafVecDB::iterator m = fc.mAliasRings.begin(), 
         e = fc.mAliasRings.end();
       m != e; ++m)
  {
    DBWriteClosure::DBLeafArray& vec = (*m).second;
    size_t vecSize = vec.size();
    /* 
       If the vecSize is only 1, then there is no reason to write it
       out.
       If the master is not present (not selected to be written
       out. Select the next in line to be the master.
    */
    if (vecSize > 1) {
      aliasVecs.push_back(&vec);
      ++numAliasVecs;
    }
  }
  
  // print out the number of alias vectors we have
  fc.OUT() << numAliasVecs;
  
  for (VecDBLeafVecLoop n(aliasVecs);
       ! n.atEnd(); ++n)
  {
    DBWriteClosure::DBLeafArray& vec = *(*n);
    size_t vecSize = vec.size();
    
    DBWriteClosure::DBLeafArray::iterator p = vec.begin();
    // only the first element can be NULL
    if (vec[0] == NULL)
    {
      ++p;
      --vecSize;
    }
    if (vecSize > 1)
    {
      fc.OUT() << SInt32(vecSize);
      for (; 
           p != vec.end(); ++p)
      {
        dl = *p;
        fc.OUT() << dl->mIndex;
      }
    }
    else 
      fc.OUT() << SInt32(0);
  }
  fc.OUT() << scDBEndType;
}

STFieldBOM::ReadStatus 
STSymbolTable::readDB(ZistreamDB& in,
                      MsgContext* msgContext)
{
  DBReadClosure rc(&in, msgContext);

  INFO_ASSERT(mHdlPath, "readDB called without having language set.");
  
  // Set the atomic cache for uset types
  mFieldBOM->setAtomicCache(mAtomCache);

  readSigAndVersion(rc);
  
  if (rc.mStat == STFieldBOM::eReadOK)
    readDate(rc);
  
  if (rc.mStat == STFieldBOM::eReadOK)
    readAtomicCache(rc);
  
  if (rc.mStat == STFieldBOM::eReadOK)
    readNodes(rc);
  
  if (rc.mStat == STFieldBOM::eReadOK)
    readAliases(rc);
  
  if (rc.mStat == STFieldBOM::eReadOK)
    rc.mStat = mFieldBOM->preFieldRead(in);

  if (rc.mStat == STFieldBOM::eReadOK)
    readFields(rc);
  
  return rc.mStat;
}

void STSymbolTable::readSigAndVersion(DBReadClosure& rc)
{
  UtString buf;
  rc.IN() >> buf;
  if (buf.compare(scSymTabSignature) != 0)
  {
    rc.mStat = STFieldBOM::eReadIncompatible;
    rc.mMsg->STSignatureMismatch(buf.c_str());
  }
  else 
  {
    // read the version
    rc.IN() >> rc.mSymTabVersion;
    if (rc.mSymTabVersion > scSymTabVersion)
    {
      rc.mStat = STFieldBOM::eReadIncompatible;
      rc.mMsg->STUnsupportedVersion(rc.mSymTabVersion, scSymTabVersion);
    }
    else
    {
      UtString errMsg;
      rc.mStat = mFieldBOM->readBOMSignature(rc.IN(), &errMsg);
      switch (rc.mStat)
      {
      case STFieldBOM::eReadOK:
        break;
      case STFieldBOM::eReadIncompatible:
        rc.mMsg->STBOMIncompatible(errMsg.c_str());
        break;
      case STFieldBOM::eReadFileError:
        rc.mMsg->STFileError(rc.IN().getError());
        break;
      case STFieldBOM::eReadCorruptFile:
        rc.mMsg->STDBUnknownCorruption();
        break;
      }
    }
  }
}

void STSymbolTable::readDate(DBReadClosure& rc)
{
  UtString buf;
  rc.IN() >> buf;
  // currently don't do anything with this
}

void STSymbolTable::readAtomicCache(DBReadClosure& rc)
{
  UInt32 index = 0;
  UtString name;


  UInt32 numAtoms = 0;
  if (! (rc.IN() >> numAtoms))
  {
    rc.mStat = STFieldBOM::eReadFileError;
    rc.mMsg->STFileError(rc.IN().getError());
  }
  else
    rc.mCacheList.reserve(numAtoms);
  
  UtString restLine;
  while ((rc.mStat == STFieldBOM::eReadOK) && (numAtoms > 0) && !rc.IN().fail() && !rc.IN().eof())
  {
    if (! (rc.IN() >> index))
    {
      rc.mStat = STFieldBOM::eReadFileError;
      rc.mMsg->STFileError(rc.IN().getError());
    }
    else if (index != rc.mCacheList.size())
    {
      rc.mStat = STFieldBOM::eReadCorruptFile;
      rc.mMsg->STCacheIndicesOutOfOrder();
    }
    else
    {
      restLine.clear();
      rc.IN() >> restLine;
      if (! rc.IN())
      {
        rc.mStat = STFieldBOM::eReadFileError;
        rc.mMsg->STFileError(rc.IN().getError());
      }
      else {
        StringAtom* atom = mAtomCache->intern(restLine.c_str());
        rc.mCacheList.push_back(atom);

        // Only map atom string pointers in newer versions since we
        // didn't store them before version 1.
        if (rc.mSymTabVersion >= 1) {
          rc.IN().mapPtr(atom);
        }
      }
    } //else
    
    --numAtoms;
  } // while
  
  char endMarker;
  if (rc.mStat == STFieldBOM::eReadOK)
  {
    rc.IN() >> endMarker;
    if (endMarker != scDBEndType)
    {
      rc.mStat = STFieldBOM::eReadCorruptFile;
      rc.mMsg->STDBSectionEndNotFound();
    }
  }
}

void STSymbolTable::readNodes(DBReadClosure& rc)
{
  bool done = false;
  while ((rc.mStat == STFieldBOM::eReadOK) && !done)
  {
    // first char is node type
    char nodeType = 'a';
    if (! (rc.IN() >> nodeType))
    {
      rc.mStat = STFieldBOM::eReadFileError;
      rc.mMsg->STFileError(rc.IN().getError());
    }
    else
    {
      if (nodeType == scDBBranchType)
        readBranchDescr(rc);
      else if (nodeType == scDBLeafType)
        readLeafDescr(rc);
      else if (nodeType == scDBEndType)
        done = true;
    }
  }
}

void STSymbolTable::readCommon(DBReadClosure& rc)
{
  // first number is node index
  
  if (rc.mStat == STFieldBOM::eReadOK)
  {
    if (! (rc.IN() >> rc.mIndex))
    {
      rc.mStat = STFieldBOM::eReadFileError;
      rc.mMsg->STFileError(rc.IN().getError());
    }
  }
  
  // second number is parent index
  if (rc.mStat == STFieldBOM::eReadOK)
  {
    if (! (rc.IN() >> (rc.mPIndex)))
    {
      rc.mStat = STFieldBOM::eReadFileError;
      rc.mMsg->STFileError(rc.IN().getError());
    }
  }

  // third number is atom index

  // next thing is the scope (branch or leaf) name
  if (rc.mStat == STFieldBOM::eReadOK)
  {
    SInt32 atomIndex = -1;
    
    if (! (rc.IN() >> atomIndex))
    {
      rc.mStat = STFieldBOM::eReadFileError;
      rc.mMsg->STFileError(rc.IN().getError());
    }
    
    else if ((atomIndex < 0) || 
             (atomIndex >= static_cast<signed>(rc.mCacheList.size())))
    {
      rc.mStat = STFieldBOM::eReadCorruptFile;
      rc.mMsg->STRecordIndexOutOfBounds(atomIndex);
    }
    else
    {
      rc.mNameObj = rc.mCacheList[atomIndex];
      INFO_ASSERT(rc.mNameObj, "Database read: tree inconsistency - string does not exist at given index.");
    }
  }
}

void STSymbolTable::readBranchDescr(DBReadClosure& rc)
{
  readCommon(rc);
  if (rc.mStat == STFieldBOM::eReadOK)
  {
    size_t vecSize = rc.mBranches.size();
    bool isIndex = false;
    if ((isIndex = (rc.mIndex != static_cast<signed>(vecSize))) || 
        (rc.mPIndex < -1) || (rc.mPIndex >= static_cast<signed>(vecSize)))
    {
      rc.mStat = STFieldBOM::eReadCorruptFile;
      long badIndex = static_cast<long>(rc.mIndex);
      if (!isIndex)
        badIndex = static_cast<long>(rc.mPIndex);
      rc.mMsg->STRecordIndexOutOfBounds(badIndex);
    }
    else {
      STBranchNode* parent = NULL;
      if (rc.mPIndex > -1)
      {
        parent = rc.mBranches[rc.mPIndex];
        INFO_ASSERT(parent, "Database read: tree inconsistency - branch does not exist at given index.");
      }
      
      STBranchNode* theBranch = createBranch(rc.mNameObj, parent);
      rc.mBranches.push_back(theBranch);
      rc.IN().mapPtr(theBranch);
    }
  }
}


void STSymbolTable::readLeafDescr(DBReadClosure& rc)
{
  readCommon(rc);
  if (rc.mStat == STFieldBOM::eReadOK)
  {
    size_t brSize = rc.mBranches.size();
    size_t leafSize = rc.mLeaves.size();
    bool isIndex = false;
    if ((isIndex = (rc.mIndex != static_cast<signed>(leafSize))) || 
        (rc.mPIndex < -1) || (rc.mPIndex >= static_cast<signed>(brSize)))
    {
      rc.mStat = STFieldBOM::eReadCorruptFile;
      long badIndex = static_cast<long>(rc.mIndex);
      if (!isIndex)
        badIndex = static_cast<long>(rc.mPIndex);
      rc.mMsg->STRecordIndexOutOfBounds(badIndex);
    }
    else {
      STBranchNode* parent = NULL;
      if (rc.mPIndex > -1)
      {
        parent = rc.mBranches[rc.mPIndex];
        INFO_ASSERT(parent, "Database read: tree inconsistency - branch does not exist at given index.");
      }
      
      STAliasedLeafNode* leaf = createLeaf(rc.mNameObj, parent);
      
      rc.mLeaves.push_back(leaf);
      rc.IN().mapPtr(leaf);
      
      char storageType;
      if (!(rc.IN() >> storageType))
      {
        rc.mStat = STFieldBOM::eReadCorruptFile;
        rc.mMsg->STInvalidLeafRecord(rc.mNameObj->str(), "missing attributes");
      }
      else if (storageType == scDBHasStorage)
        leaf->setThisStorage();
      else if (storageType != scDBNoStorage)
      {
        rc.mStat = STFieldBOM::eReadCorruptFile;
        rc.mMsg->STInvalidLeafRecord(rc.mNameObj->str(), "Invalid storage type");
      } // else if
    } // else
  } // if
} // void STSymbolTable::readLeafDescr

void STSymbolTable::readAliases(DBReadClosure& rc)
{
  typedef UtArray<SInt32> NumVec;
  NumVec aliasIndices;
  SInt32 vecSize = 0;
  // first number is the number of significant alias rings
  UInt32 numAliasRings;
  if (! (rc.IN() >> numAliasRings))
  {
    rc.mStat = STFieldBOM::eReadFileError;
    rc.mMsg->STFileError(rc.IN().getError());
  }
  
  // foreach ring first number is size of vector
  for (UInt32 i = 0; (i < numAliasRings) && (rc.mStat == STFieldBOM::eReadOK); ++i)
  {
    aliasIndices.clear();
    if (! (rc.IN() >> vecSize))
    {
      rc.mStat = STFieldBOM::eReadFileError;
      rc.mMsg->STFileError(rc.IN().getError());
    }
    else
      aliasIndices.reserve(vecSize);
    
    // Next set of numbers are node indices
    for (SInt32 j = 0; (j < vecSize) && (rc.mStat == STFieldBOM::eReadOK); ++j)
    {
      SInt32 tmp = -1;
      if (! (rc.IN() >> tmp))
      {
        rc.mStat = STFieldBOM::eReadFileError;
        rc.mMsg->STFileError(rc.IN().getError());
      }
      else
        aliasIndices.push_back(tmp);
    } // for
    
    STAliasedLeafNode* master = NULL;
    SInt32 numLeaves = static_cast<SInt32>(rc.mLeaves.size());
    if ((rc.mStat == STFieldBOM::eReadOK) && ! aliasIndices.empty())
    {
      SInt32 masterInd = aliasIndices[0];
      if ((masterInd >= 0) && (masterInd < numLeaves))
        master = rc.mLeaves[masterInd];
      else
      {
        rc.mStat = STFieldBOM::eReadCorruptFile;
        rc.mMsg->STRecordIndexOutOfBounds(masterInd);
      }
    }
    
    if (master)
    {
      master->setThisMaster();
      // resolve the alias list
      NumVec::iterator p = aliasIndices.begin();
      ++p; // skip first
      STAliasedLeafNode::LeafNodeVec nodeAliases;
      while (p != aliasIndices.end())
      {
        SInt32 ind = *p;
        if ((ind >= 0) && (ind < numLeaves))
          nodeAliases.push_back(rc.mLeaves[ind]);
        ++p;
      }

      if (! nodeAliases.empty())
        master->appendAliases(nodeAliases);
    } // if
  } // while
  
  if (rc.mStat == STFieldBOM::eReadOK)
  {
    char end;
    if (rc.IN() >> end)
    {
      if (end != scDBEndType)
      {
        rc.mStat = STFieldBOM::eReadCorruptFile;
        rc.mMsg->STDBSectionEndNotFound();
      }
    }
    else
    {
      rc.mStat = STFieldBOM::eReadFileError;
      rc.mMsg->STFileError(rc.IN().getError());
    }
  }
}

void STSymbolTable::readFields(DBReadClosure& rc)
{
  // first read the branch fields then the leaves
  char endField = '\0';
  
  // read in the fieldwriter indicator
  SInt32 brField = -1;
  if (! (rc.IN() >> brField))
  {
    rc.mStat = STFieldBOM::eReadFileError;
    rc.mMsg->STFileError(rc.IN().getError());
  }
  else if (brField > -1)
  {
    // num branch fields
    UInt32 size = rc.mBranches.size();
    const UInt32 numBrs = size;
    STFieldBOM::ReadStatus readStat;
    while ((size > 0) && (rc.mStat == STFieldBOM::eReadOK))
    {
      // get the branch node index
      SInt32 bIndex = -1;
      if (rc.IN() >> bIndex)
      {
        if ((bIndex >= 0) && (static_cast<unsigned>(bIndex) < numBrs))
        {
          STBranchNode* brNode = rc.mBranches[bIndex];
          
          if ((readStat = 
               mFieldBOM->readBranchData(brNode, rc.IN(), rc.mMsg)) != 
              STFieldBOM::eReadOK)
            rc.mStat = readStat;
          else if (rc.IN() >> endField)
          {
            if (endField != scDBFieldEnd)
            {
              rc.mStat = STFieldBOM::eReadCorruptFile;
              rc.mMsg->STDBFieldEndNotFound();
            }
          }
          else
          {
            rc.mStat = STFieldBOM::eReadFileError;
            rc.mMsg->STFileError(rc.IN().getError());
          }
        } // if
        else
        {
          rc.mStat = STFieldBOM::eReadCorruptFile;
          rc.mMsg->STRecordIndexOutOfBounds(bIndex);
        }
      }
      else
      {
        rc.mStat = STFieldBOM::eReadFileError;
        rc.mMsg->STFileError(rc.IN().getError());
      }
      
      --size;
    } // while
    
    if (rc.mStat == STFieldBOM::eReadOK)
    {
      if (rc.IN() >> endField)
      {
        if (endField != scDBEndType)
        {
          rc.mStat = STFieldBOM::eReadCorruptFile;
          rc.mMsg->STDBSectionEndNotFound();
        }
      }
      else
      {
        rc.mStat = STFieldBOM::eReadFileError;
        rc.mMsg->STFileError(rc.IN().getError());
      }
      
    }
    
    // leaves next
    // num leaf fields
    size = rc.mLeaves.size();
    const UInt32 numLeaves = size;
    while ((size > 0) && (rc.mStat == STFieldBOM::eReadOK))
    {
      // get the branch node index
      SInt32 leafIndex = -1;
      if (rc.IN() >> leafIndex)
      {
        if ((leafIndex >= 0) && (static_cast<unsigned>(leafIndex) < numLeaves))
        {
          STAliasedLeafNode* leafNode = rc.mLeaves[leafIndex];
          
          if ((readStat = 
               mFieldBOM->readLeafData(leafNode, rc.IN(), rc.mMsg)) != 
              STFieldBOM::eReadOK)
            rc.mStat = readStat;
          else if (rc.IN() >> endField)
          {
            if (endField != scDBFieldEnd)
            {
              rc.mStat = STFieldBOM::eReadCorruptFile;
              rc.mMsg->STDBFieldEndNotFound();
            }
          }
          else
          {
            rc.mStat = STFieldBOM::eReadFileError;
            rc.mMsg->STFileError(rc.IN().getError());
          }
        } // if
        else
        {
          rc.mStat = STFieldBOM::eReadCorruptFile;
          rc.mMsg->STRecordIndexOutOfBounds(leafIndex);
        }
      }
      else
      {
        rc.mStat = STFieldBOM::eReadFileError;
        rc.mMsg->STFileError(rc.IN().getError());
      }
      
      --size;
    } // while
    
    if (rc.mStat == STFieldBOM::eReadOK)
    {
      if (rc.IN() >> endField)
      {
        if (endField != scDBEndType)
        {
          rc.mStat = STFieldBOM::eReadCorruptFile;
          rc.mMsg->STDBSectionEndNotFound();
        }
      }
      else
      {
        rc.mStat = STFieldBOM::eReadFileError;
        rc.mMsg->STFileError(rc.IN().getError());
      }
    }
  } // else if
  else if (rc.IN() >> endField)
  {
    if (endField != scDBEndType)
    {
      rc.mStat = STFieldBOM::eReadCorruptFile;
      rc.mMsg->STDBSectionEndNotFound();
    }
  }
}
