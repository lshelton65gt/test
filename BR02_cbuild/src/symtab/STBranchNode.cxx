// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "symtab/STBranchNode.h"
#include "symtab/STSymbolTable.h" // ST_ASSERT

/*!
  \file
  Implementation of STBranchNode class
*/

STBranchNode::STBranchNode(const StringAtom* name, BOMData bomdata) : 
  STSymbolTableNode(const_cast<StringAtom*>(name), // fix later
                    bomdata)
{
}

STBranchNode::~STBranchNode()
{
}

const STBranchNode* STBranchNode::castBranch() const
{
  return this;
}

bool STBranchNode::isChild(const STSymbolTableNode* child) const
{
  bool ret = child != NULL;
  if (ret)
    ret = child->getParent() == this;
  return ret;
}

bool STBranchNode::hasChild(SInt32 index) const
{
  return ((static_cast<UInt32>(index) < mChildArray.size()) and
	  (mChildArray[index] != NULL));
}


const STSymbolTableNode* STBranchNode::getChild(SInt32 index) const
{
  ST_ASSERT(index > -1, this);
  ST_ASSERT(static_cast<UInt32>(index) < mChildArray.size(), this);
  return mChildArray[index];
}

STSymbolTableNode* STBranchNode::getChild(SInt32 index)
{
  const STBranchNode* me = const_cast<const STBranchNode*>(this);
  return const_cast<STSymbolTableNode*>(me->getChild(index));
}

void STBranchNode::addChild(STSymbolTableNode* child)
{
  mChildArray.push_back(child);
  setIndex(child, mChildArray.size() - 1);
}

void STBranchNode::addChildByIndex(STSymbolTableNode* child, SInt32 index)
{
  ST_ASSERT(index > -1, this);
  UInt32 uIndex = static_cast<UInt32>(index);

  // insert on a vector is a stupid stupid method that cannot
  // handle emptiness. So, we need to check for resize
  if (static_cast<UInt32>(uIndex) >= mChildArray.size())
    mChildArray.resize(uIndex + 1);
  else
    // If we are not inserting a new node, make sure we didn't
    // insert into the children set
    ST_ASSERT(mChildArray[index] == NULL, this);
  
  mChildArray[index] = child;
  setIndex(child, index);
}

void STBranchNode::removeChild(STSymbolTableNode *child)
{
  mChildArray[child->getIndex()] = NULL;
}

SInt32 STBranchNode::numChildren() const
{
  return mChildArray.size();
}

//! Print node-specific description to stderr
void STBranchNode::printInfo() const
{
  // print hierarchical name
  fprintf(stderr, "Symbol table branch node with %d children: ", numChildren());
  print();
}


// STBranchNodeIter

STBranchNodeIter::STBranchNodeIter(STBranchNode* branch)
{
  mBranch = branch;
  mChildIter.mKey = NULL;
  begin();
}

void STBranchNodeIter::begin()
{
  mChildIter.mIter = mBranch->mChildArray.begin();
  if (mChildIter.mIter != mBranch->mChildArray.end())
    mChildIter.mKey = *mChildIter.mIter;
}

const STSymbolTableNode* STBranchNodeIter::next(SInt32* index) const
{
  const STSymbolTableNode* ret = NULL;
  if (mChildIter.mIter != mBranch->mChildArray.end())
  {
    ret = mChildIter.mKey;
    *index = mChildIter.mKey->getIndex();
    ++mChildIter.mIter;
    if (mChildIter.mIter == mBranch->mChildArray.end())
      mChildIter.mKey = NULL;
    else 
      mChildIter.mKey = *mChildIter.mIter;
  }
  return ret;
}


STSymbolTableNode* STBranchNodeIter::next(SInt32* index)
{
  const STBranchNodeIter* me = const_cast<const STBranchNodeIter*>(this);
  const STSymbolTableNode* tmp = me->next(index);
  return const_cast<STSymbolTableNode*>(tmp);
}
