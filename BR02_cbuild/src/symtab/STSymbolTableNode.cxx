// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "symtab/STSymbolTableNode.h"
#include "symtab/STBranchNode.h"
#include "symtab/STSymbolTable.h" // ST_ASSERT
#include "util/StringAtom.h"
#include "util/UtArray.h"
#include "hdl/HdlVerilogPath.h"
#include <stdio.h>

/*!
  \file
  Implementation of STSymbolTableNode class
*/

STSymbolTableNode::STSymbolTableNode(StringAtom* name, BOMData bomData)
{
  mIndex = -1;
  mName = name;
  mBOMData = bomData;
  putParent(NULL);
}

STSymbolTableNode::~STSymbolTableNode()
{
}

void STSymbolTableNode::putParent(STBranchNode* parent)
{
  mParent = parent;
}

const STBranchNode* STSymbolTableNode::getParent() const
{
  return mParent;
}

STBranchNode* STSymbolTableNode::getParent()
{
  const STSymbolTableNode* me = const_cast<const STSymbolTableNode*>(this);
  return const_cast<STBranchNode*>(me->getParent());
}

const STBranchNode* STSymbolTableNode::getRoot() const
{
  const STBranchNode * root = getParent();
  if (root != NULL) {
    while (root->getParent() != NULL) {
      root = root->getParent();
    }
  }
  return root;
}

STBranchNode* STSymbolTableNode::getRoot()
{
  const STSymbolTableNode* me = const_cast<const STSymbolTableNode*>(this);
  return const_cast<STBranchNode*>(me->getRoot());
}

bool STSymbolTableNode::isAncestor(const STSymbolTableNode* ancestorInQuestion)
{
  const STSymbolTableNode* currentLevel = this;
  const STSymbolTableNode* parent;

  ST_ASSERT( ancestorInQuestion != currentLevel, ancestorInQuestion);
  while((parent = currentLevel->getParent()) != NULL)
  {
    if ( ancestorInQuestion == parent )
    {
      return true;
    }
    currentLevel = parent;
  }
  return false;  // ancestorInQuestion not found
}

SInt32 STSymbolTableNode::getIndex() const
{
  return mIndex;
}

const char* STSymbolTableNode::str() const
{
  return mName->str();
}

const HierName* STSymbolTableNode::getParentName() const
{
  return getParent();
}

void STSymbolTableNode::print() const
{
  UtString buf;

  buf.clear();
  compose(&buf);                // call compose (vs composeHelper) to pick up default args
  fprintf(stdout, "%s\n", buf.c_str());
  fflush(stdout);
}

void STSymbolTableNode::composeHelper(UtString* buf, bool includeRoot, bool hierName, const char *separator, bool escapeIfMultiLevel ) const
{
  bool needsEsc = false;

  if (!hierName)
  {
    // we might also be able to go here if !getParent ()
    *buf += str();    // so we only add the name
    return;
  }
  UtArray<const STSymbolTableNode*> hierStack;
  const STSymbolTableNode* current = this;
  const STSymbolTableNode* parent;

  // the first time through the following loop the ST node for 'this'
  // is pushed on so that it is available when we unload hierStack
  while ( current != NULL )
  {
    needsEsc |= ( current->str()[0] == '\\' );
    needsEsc |= ( current->str()[0] == '$' );

    parent = current->getParent();
    if ( (this != current ) && ( parent == NULL ) && ( ! includeRoot ) )
    {
      break;
    }
    hierStack.push_back(current);
    current = parent;
  }

  int index = hierStack.size() - 1;   // index is 0 based
  needsEsc |= ( index > 0 ); 

  // check to see if caller asked for escaped multi-level names and there is a reason to escape the name 
  bool writeEscapedName = ( needsEsc && ( escapeIfMultiLevel ));

  if ( writeEscapedName )
  {
    *buf += '\\';
  }

  while (index >= 0)
  {
    UtString tbuf = hierStack[index]->str();
    if ( writeEscapedName && ( tbuf[0] == '\\' ))
    {
      tbuf.erase(0,1);      // if creating an escaped verilog name do not embed a backslash
    } 
    *buf += tbuf;
    if ( index > 0 ) { *buf += separator; } // add separator (if not lowest level)
    --index;
  }
  if ( writeEscapedName )
  {
    *buf += " ";           // escaped names need a space at end
  }

} // void STSymbolTableNode::compose

void STSymbolTableNode::verilogCompose(UtString* buffer) const
{
  HdlVerilogPath pather;
  UtString tmp;
  pather.compPathHier(this, &tmp, NULL);
  *buffer << tmp;
}

//! Compose only the leaf of this name as a verilog identifier
void STSymbolTableNode::verilogComposeLeaf(UtString* buffer) const
{
  HdlHierPath::StrAtomVec atomVec;
  atomVec.push_back(strObject());
  HdlVerilogPath pather;
  UtString tmp;
  pather.compPathAtom(atomVec, &tmp, NULL);
  *buffer << tmp;
}

const STBranchNode* STSymbolTableNode::castBranch() const
{
  return NULL;
}

const STAliasedLeafNode* STSymbolTableNode::castLeaf() const
{
  return NULL;
}

void STSymbolTableNode::putBOMData(BOMData data)
{
  mBOMData = data;
}

void STSymbolTableNode::printInfo() const
{
  fprintf(stdout, "Symbol table node: ");
  print();
  fflush(stdout);
}


