// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2004-2005 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/
#include "symtab/STAliasedLeafNodeNoAlloc.h"

STAliasedLeafNodeNoAlloc::STAliasedLeafNodeNoAlloc(const StringAtom* name,
                                                   BOMData bomdata)
  : STAliasedLeafNode(name, bomdata)
{
}

STAliasedLeafNodeNoAlloc::~STAliasedLeafNodeNoAlloc()
{
}

bool STAliasedLeafNodeNoAlloc::isAllocated()
  const
{
  return false;
}


