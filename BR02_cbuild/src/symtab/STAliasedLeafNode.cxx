// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "symtab/STAliasedLeafNode.h"
#include "symtab/STSymbolTable.h"

/*!
  \file
  Implementation of STAliasedLeafNode class
*/

STAliasedLeafNode::STAliasedLeafNode(const StringAtom* name, BOMData bomdata) :
  STSymbolTableNode(const_cast<StringAtom*>(name), // fix later
                    bomdata)
{
  mNextAlias = this;
  mMasterAlias = this;
  mStorageAlias = NULL;
}

STAliasedLeafNode::~STAliasedLeafNode()
{
}

const STAliasedLeafNode* STAliasedLeafNode::castLeaf() const
{ 
  return this;
}

STAliasedLeafNode* STAliasedLeafNode::castLeaf()
{ 
  return this;
}

bool STAliasedLeafNode::hasAliases() const
{
  return (mNextAlias != this);
}

SInt32 STAliasedLeafNode::getRingLen() const
{
  SInt32 len = 1;

  for (const STAliasedLeafNode* node = mNextAlias; node != this; 
       node = node->mNextAlias)
    len++;
  
  return len;
}

void STAliasedLeafNode::linkAlias(STAliasedLeafNode* aliasName)
{
  // Assumption: The two rings about to be aliased are
  // self-consistent. 
  // 
  // For an alias ring to be self-consistent,
  // 1. All storage pointers must be the same (NULL is allowed).
  // 2. All master pointers must be the same (NULL not allowed).
  //
  // If the two incoming rings are self-consistent, the joining of
  // those two rings produces a self-consistent ring.
  // 
  // In all cases, the master for the joined ring is the master of
  // 'this'. 
  // 
  // The storage for the joined ring is the storage for 'this' if
  // non-NULL. Otherwise, the storage for 'aliasName' is used. If the
  // storage for both rings is NULL, storage ptrs are untouched,
  // remaining NULL.

  // Check if any of the incoming aliases are this. If so, the
  // aliasing already was done. The aliasing algorithm below expects
  // nodes that are not aliased
  {
    STAliasedLeafNode* check = aliasName;
    do {
      if (check == this)
        return;
      check = check->mNextAlias;
    } while (check != aliasName);
  }

  STAliasedLeafNode* ringSplice = aliasName->mNextAlias;
  STAliasedLeafNode* a = aliasName;
  do {
    a->mMasterAlias = mMasterAlias;

    // If the master has incoming storage alias, update all incoming
    // aliases to reference that storage element. The existing aliases
    // should already reference this storage.
    if (mStorageAlias) {
      a->mStorageAlias = mStorageAlias;
    }

    a = a->mNextAlias;
  } while (a != aliasName);
  aliasName->mNextAlias = mNextAlias;
  mNextAlias = ringSplice;
  
  if (mStorageAlias) {
    // Nop. We have already updated the storage ptr for all new
    // aliases. Existing aliases already have the correct storage ptr.
  } else if (aliasName->mStorageAlias) {
    // The storage ptr for this ring is NULL, but our incoming aliases
    // already have a storage ptr. Update the entire ring to use that
    // storage ptr.
    aliasName->setThisStorage();
  }
}


void STAliasedLeafNode::transferAliases(STAliasedLeafNode *other)
{
  STAliasedLeafNode* first = other->mNextAlias;

  // Handle singleton rings; nothing to do in that case.
  if (first == other) {
    return;
  }

  // Update the other to no longer reference its aliases.
  other->mNextAlias = other;
  other->mMasterAlias = other;
  other->mStorageAlias = other;

  // Make all aliases of other point to their new master and storage (excluding other).
  STAliasedLeafNode* last = first;
  for (STAliasedLeafNode* cur = first; cur != other; cur = cur->mNextAlias) {
    cur->mMasterAlias = mMasterAlias;
    last = cur;
  }

  // Splice the alias ring of other (excluding other) into the master's ring.
  last->mNextAlias = mNextAlias;
  mNextAlias = first;

  // Update the storage pointer for the entire ring.
  getStorage()->setThisStorage();
}

void STAliasedLeafNode::appendAliases(LeafNodeVec& nodes)
{
  // put all the nodes of this alias ring in a hashset.
  typedef UtHashSet<STAliasedLeafNode*, HashPointerValue<STAliasedLeafNode*> > LeafNodeSet;
  LeafNodeSet currentAliases;
  STAliasedLeafNode* na = this;
  do {
    currentAliases.insert(na);
    na = na->getAlias();
  } while (na != this);

  // add the nodes from the vector
  for (LeafNodeVec::const_iterator p = nodes.begin(), e = nodes.end(); p != e; ++p)
  {
    STAliasedLeafNode* leaf = *p;
    currentAliases.insert(leaf);
  }

  // walk through again and de-alias
  for (LeafNodeSet::UnsortedLoop p = currentAliases.loopUnsorted();
       ! p.atEnd(); ++p)
  {
    STAliasedLeafNode* leaf = *p;
    leaf->mNextAlias = leaf;
  }
  
  // Now, relink the aliases via this node
  for (LeafNodeSet::UnsortedLoop p = currentAliases.loopUnsorted();
       ! p.atEnd(); ++p)
  {
    STAliasedLeafNode* leaf = *p;
    linkAlias(leaf);
  }
}

void STAliasedLeafNode::printAliases() const
{
  const STAliasedLeafNode * master = getMaster();
  master->print();
  for (const STAliasedLeafNode * alias = master->getAlias();
       alias != master ;
       alias = alias->getAlias()) {
    fprintf(stdout,"    ");
    alias->print();
  }
  fflush(stdout);
}


void STAliasedLeafNode::printAliasStorage() const
{
  const STAliasedLeafNode * master = getMaster();
  fprintf(stdout, "Master: ");
  master->print();
  fprintf(stdout,"     => ");
  if (master->getStorage())
    master->getStorage()->print();
  else
    fprintf(stdout,"(null)\n");

  fprintf(stdout, "Aliases:\n");
  for (const STAliasedLeafNode * alias = master->getAlias();
       alias != master ;
       alias = alias->getAlias()) {
    fprintf(stdout,"    ");
    alias->print();
    fprintf(stdout,"     => ");
    if (alias->getInternalStorage())
      alias->getInternalStorage()->print();
    else
      fprintf(stdout,"(null)\n");
  }
  fflush(stdout);
}

//! Print node-specific description to stderr
void STAliasedLeafNode::printInfo() const
{
  // print hierarchical name
  fprintf(stdout,"Symbol table leaf node: ");
  print();

  // print master, alias ring & storage
  printAliasStorage();
  fflush(stdout);
}

const STAliasedLeafNode* STAliasedLeafNode::getAlias() const
{
  return mNextAlias;
}

STAliasedLeafNode* STAliasedLeafNode::getAlias()
{
  const STAliasedLeafNode* me = const_cast<const STAliasedLeafNode*>(this);
  return const_cast<STAliasedLeafNode*>(me->getAlias());
}

void STAliasedLeafNode::setThisMaster()
{
  mMasterAlias = this;
  STAliasedLeafNode* aliasName = this;
  while ((aliasName = aliasName->mNextAlias) != this)
    aliasName->mMasterAlias = this;
}

STAliasedLeafNode* STAliasedLeafNode::getMaster()
{
  const STAliasedLeafNode* me = const_cast<const STAliasedLeafNode*>(this);
  return const_cast<STAliasedLeafNode*>(me->getMaster());
}

const STAliasedLeafNode* STAliasedLeafNode::getMaster() const
{
  return mMasterAlias;
}

void STAliasedLeafNode::setThisStorage()
{
  mStorageAlias = this;
  STAliasedLeafNode* aliasName = this;
  while ((aliasName = aliasName->mNextAlias) != this)
    aliasName->mStorageAlias = this;
}

void STAliasedLeafNode::clearStorage()
{
  mStorageAlias = NULL;
  STAliasedLeafNode* aliasName = this;
  while ((aliasName = aliasName->mNextAlias) != this)
    aliasName->mStorageAlias = NULL;
}

STAliasedLeafNode* STAliasedLeafNode::getStorage()
{
  const STAliasedLeafNode* me = const_cast<const STAliasedLeafNode*>(this);
  return const_cast<STAliasedLeafNode*>(me->getStorage());
}

const STAliasedLeafNode* STAliasedLeafNode::getStorage() const
{
  const STAliasedLeafNode* ret = mStorageAlias;
  if (! ret)
    ret = this;
  return ret;
}

STAliasedLeafNode* STAliasedLeafNode::getInternalStorage()
{
  const STAliasedLeafNode* me = const_cast<const STAliasedLeafNode*>(this);
  return const_cast<STAliasedLeafNode*>(me->getInternalStorage());
}

const STAliasedLeafNode* STAliasedLeafNode::getInternalStorage() const
{
  return mStorageAlias;
}

bool STAliasedLeafNode::isAllocated()
  const
{
  return true;
}
