// -*- C++ -*-                
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "util/AtomicCache.h"
#include "symtab/STSymbolTable.h"
#include "util/StringAtom.h"
#include "hdl/HdlVerilogPath.h"
#include "hdl/HdlId.h"
#include "util/Zstream.h"
#include "util/CbuildMsgContext.h"
#include "util/UtVector.h"

#include "util/UtIOStream.h"

// please do not use "using namespace std"

class MyNodeSelect : public STNodeSelectDB
{
public:
  MyNodeSelect(const STAliasedLeafNode* noNode)
  {
    mDoNotWrite = noNode;
  }
  
  virtual ~MyNodeSelect() {}
  
protected:
  virtual bool computeSelectLeaf(const STAliasedLeafNode* leaf, const HdlHierPath*)
  {
    return (leaf != mDoNotWrite);
  }
  
  virtual bool computeSelectBranch(const STBranchNode*, const HdlHierPath*)
  {
    return true;
  }
  
  virtual void requestAlwaysSelect(const STSymbolTableNode* node)
  {
    mAlwaysSelect.insert(node);
  }

  const STAliasedLeafNode* mDoNotWrite;
};

class NothingMuch
{
public:
  NothingMuch() {}

  SInt32 mVal;
};

#if 0
class MyFieldWrite
{
public:

  virtual ~MyFieldWrite() {}

  virtual SInt32 getBranchFieldSize() const
  {
    return static_cast<SInt32>(sizeof(SInt32));
  }
  
  virtual void writeBranchField(const STBranchNode* branch,
                                Zostream& out)
  {
    writeField(branch->getField(), out);
  }
  
  virtual SInt32 getLeafFieldSize() const
  {
    return static_cast<SInt32>(sizeof(SInt32));    
  }
  
  virtual void writeLeafField(const STAliasedLeafNode* leaf,
                              Zostream& out)
  {
    writeField(leaf->getField(), out);    
  }
  

private:

  void writeField(const STSymField* field, Zostream& out)
  {
    for (SInt32 i = 0; i < 2; ++i)
    {
      const STSymField::Slot* slot = field->getSlot(i);
      const NothingMuch* nothing = reinterpret_cast<const NothingMuch*>(*slot);
      out << nothing->mVal;
    }
  }
};

class MyFieldRead : public STFieldReadDB
{
public:
  virtual ~MyFieldRead() {
  }

  virtual STSymbolTable::ReadStatus 
  readBranchField(STBranchNode* branch, SInt32 fieldSize, Zistream& in,
                  MsgContext*)
  {
    assert(fieldSize == sizeof(SInt32));
    return readField(branch->getField(), in);
  }
  
  virtual STSymbolTable::ReadStatus 
  readLeafField(STAliasedLeafNode* leaf, SInt32 fieldSize,
                Zistream& in, MsgContext*)
  {
    assert(fieldSize == sizeof(SInt32));
    return readField(leaf->getField(), in);
  }

private:
  STSymbolTable::ReadStatus readField(STSymField* field, Zistream& in)
  {
    STSymbolTable::ReadStatus readStat = STFieldBOM::eReadOK;
    for (SInt32 i = 0; i < 2; ++i)
    {
      NothingMuch* nothing = new NothingMuch;
      mNotMuches.push_back(nothing);
      STSymField::Slot* slot = field->getSlot(i);
      *slot = reinterpret_cast<STSymField::Slot>(nothing);
      in >> nothing->mVal;
      if (! in)
      {
        UtIO::cerr() << "Field read failed:";
        readStat = STFieldBOM::eReadCorruptFile;
      }
    }
    return readStat;
  }
  
  UtVector<NothingMuch*> mNotMuches;
};
#endif

class MiddleMan 
{
public:
  MiddleMan() {}
  ~MiddleMan() {}

  const NothingMuch* getNM1() const {
    return mNM1;
  }
  
  NothingMuch* getNM1() {
    const MiddleMan* me = const_cast<const MiddleMan*>(this);
    return const_cast<NothingMuch*>(me->getNM1());
  }

  const NothingMuch* getNM2() const {
    return mNM2;
  }
  
  NothingMuch* getNM2() {
    const MiddleMan* me = const_cast<const MiddleMan*>(this);
    return const_cast<NothingMuch*>(me->getNM2());
  }
  
  void setNM1(NothingMuch* nm) {
    mNM1 = nm;
  }

  void setNM2(NothingMuch* nm) {
    mNM2 = nm;
  }

private:
  NothingMuch* mNM1;
  NothingMuch* mNM2;
};

class MyFieldBOM : public virtual STFieldBOM
{
public:
  MyFieldBOM() {}

  virtual ~MyFieldBOM() {
    for (UtVector<NothingMuch*>::iterator p = mNotMuches.begin();
         p != mNotMuches.end(); ++p)
      delete(*p);
  }
  
  virtual Data allocBranchData() { 
    return new MiddleMan;
  }

  virtual Data allocLeafData() { 
    return new MiddleMan;
  }
  
  virtual void freeBranchData(const STBranchNode*, Data* bomdata) {
    freeData(bomdata);
  }

  virtual void freeLeafData(const STAliasedLeafNode*, Data* bomdata) {
    freeData(bomdata);
  }
  
  virtual void printLeaf(const STAliasedLeafNode* leaf) const {
    printData(leaf->getBOMData());
  }

  virtual void printBranch(const STBranchNode* branch) const {
    printData(branch->getBOMData());
  }
  
  virtual void preFieldWrite(ZostreamDB&) 
  {}
  
  virtual ReadStatus preFieldRead(ZistreamDB&) 
  {
    return eReadOK;
  }

  const MiddleMan* castSym(const void* bomdata) const {
    return static_cast<const MiddleMan*>(bomdata);
  }
  
  MiddleMan* castSym(void* bomdata) {
    const MyFieldBOM* me = const_cast<const MyFieldBOM*>(this);
    return const_cast<MiddleMan*>(me->castSym(bomdata));
  }
  
  virtual void writeLeafData(const STAliasedLeafNode* leaf, ZostreamDB& out) const {
    writeData(castSym(leaf->getBOMData()), out);
  }
  
  //! Write the BOMData for a branch node
  virtual void writeBranchData(const STBranchNode* branch, ZostreamDB& out,
                               AtomicCache*) const {
    writeData(castSym(branch->getBOMData()), out);
  }
 
  //! Read the BOMData for a leaf node
  virtual ReadStatus readLeafData(STAliasedLeafNode* leaf, ZistreamDB& out, 
                                  MsgContext* msg) {
    return readData(castSym(leaf->getBOMData()), out, msg);
  }
    
  //! Read the BOMData for a branch node
  virtual ReadStatus readBranchData(STBranchNode* branch, ZistreamDB& out, 
                                    MsgContext* msg) {
    return readData(castSym(branch->getBOMData()), out, msg);
  }


  virtual void writeBOMSignature(ZostreamDB& out) const {
    out << "NOTHING SIG";
  }
    
  virtual ReadStatus readBOMSignature(ZistreamDB& in, UtString*) {
    UtString tmp;
    in >> tmp;
    if (!in)
      return eReadFileError;
    else if (tmp.compare("NOTHING SIG") != 0)
      return eReadIncompatible;
    
    return eReadOK;
  }


 //! Return BOM class name
  virtual const char* getClassName() const
  {
    return "MyFieldBOM";
  }

  //! Write the BOMData for a branch node
  virtual void xmlWriteBranchData(const STBranchNode* /*branch*/,UtXmlWriter* /*writer*/) const
  {
  }

  //! Write the BOMData for a leaf node
  virtual void xmlWriteLeafData(const STAliasedLeafNode* /*leaf*/,UtXmlWriter* /*writer*/) const
  {
  }



private:

  void freeData(Data* bomdata)
  {
    if (bomdata) {
      MiddleMan* mm = castSym(*bomdata);
      delete mm;
      *bomdata = NULL;
    }
  }

  void printData(const Data bomdata) const
  {
    UtIO::cout() << bomdata;
  }

  void writeData(const MiddleMan* mm, ZostreamDB& out) const {
    const NothingMuch* nothing = mm->getNM1();
    out << nothing->mVal;
    nothing = mm->getNM2();
    out << nothing->mVal;
  }

  void setNM(MiddleMan* mm, NothingMuch* nothing, SInt32 i)
  {
    if (i == 0)
      mm->setNM1(nothing);
    else 
      mm->setNM2(nothing);
  }
  
  ReadStatus readData(MiddleMan* mm, ZistreamDB& in, MsgContext*) {
    ReadStatus readStat = eReadOK;
    
    for (SInt32 i = 0; i < 2; ++i)
    {
      
      NothingMuch* nothing = new NothingMuch;    
      mNotMuches.push_back(nothing);
      setNM(mm, nothing, i);
      in >> nothing->mVal;
      if (! in)
      {
        UtIO::cerr() << "Field read failed:";
        readStat = eReadCorruptFile;
      }
    }
    
    return readStat;
  }
  
  UtVector<NothingMuch*> mNotMuches;
};

static int testHier()
{
  AtomicCache atomCache;
  HdlVerilogPath hdlPath;
  MyFieldBOM bom;
  STSymbolTable table(&bom, &atomCache);
  table.setHdlHier(&hdlPath);
  StringAtom* a = atomCache.intern("a");
  StringAtom* b = atomCache.intern("b");
  StringAtom* c = atomCache.intern("c");
  StringAtom* d = atomCache.intern("d");
  StringAtom* e = atomCache.intern("e");

  STBranchNode* top = table.createBranch(a, NULL);
  STBranchNode* instB = table.createBranch(b, 
                                           top);
  
  STBranchNode* instC = table.createBranch(c, 
                                           top);

  STAliasedLeafNode* instD = 
    table.createLeaf(d, 
                     instB);

  STAliasedLeafNode* instE = 
    table.createLeaf(e, instC);

  ZOSTREAMDB(db, "hier.db"); 
  table.writeDB(db, NULL, false);
  db.close();

  AtomicCache cache2;
  STSymbolTable tableR(&bom, &cache2);
  tableR.setHdlHier(&hdlPath);
  ZISTREAMDB(dbr, "hier.db");
  MsgStreamIO errStream(stderr, true);
  MsgContext msgContext;
  msgContext.addReportStream(&errStream);
  
  tableR.readDB(dbr, &msgContext);

  int numRoots = 0;
  STSymbolTableNode* check = NULL;
  for (STSymbolTable::RootIter tableIter = tableR.getRootIter();
       !tableIter.atEnd(); ++tableIter)
  {
    check = *tableIter;
    ++numRoots;
  }
  
  int stat = 0;
  if (numRoots != 1)
  {
    UtIO::cerr() << "Too many roots" << UtIO::endl;
    stat = 3;
  }
  
  if (stat == 0)
  {
    if (strcmp(check->str(), top->str()) != 0)
    {
      UtIO::cerr() << "Wrong root found" << UtIO::endl;
      stat = 3;
    }
  }

  if (stat == 0)
  {
    STBranchNode* br = check->castBranch();
    if (! br)
    {
      UtIO::cerr() << "expected a branch, got a leaf" << UtIO::endl;
      stat = 3;
    }
    else if (br->numChildren() != 2)
    {
      UtIO::cerr() << "Branch has wrong # of children" << UtIO::endl;
      stat = 3;
    }
    else if ((strcmp(br->getChild(0)->str(), instB->str()) != 0) &&
             (strcmp(br->getChild(1)->str(), instC->str()) != 0))
    {
      UtIO::cerr() << "top has wrong branches" << UtIO::endl;
      stat = 3;
    }
    else
    {
      STSymbolTableNode* check2 = instD;
      for (SInt32 i = 0; i < 2; ++i)
      {
        check = br->getChild(i);
        STBranchNode* br2 = check->castBranch();
        assert(br2);
        assert(br2->numChildren() == 1);
        if (strcmp(br2->getChild(0)->str(), check2->str()) != 0)
        {
          UtIO::cerr() << "branch has wrong leaf, " << i << UtIO::endl;
          stat = 3;
        }
        check2 = instE;
      }
    }
  }

  return stat;
}

int main()
{
  int status = 0;
  AtomicCache atomCache;
  HdlVerilogPath hdlPath;
  MyFieldBOM bom;
  STSymbolTable table(&bom, &atomCache);
  table.setHdlHier(&hdlPath);
  StringAtom* bar = atomCache.intern("bar");
  StringAtom* hula = atomCache.intern("bar");
  UtIO::cout() << "atom: " << bar->str() << '\n';
  if (bar != hula) 
  {
    UtIO::cerr() << "intern didn't work\n";
    status = 3;
  }
  

  hula = atomCache.intern("hula");
  
  // a.b
  StringAtom* atom = atomCache.intern("a");

  NothingMuch* notMuch1 = new NothingMuch;
  notMuch1->mVal = 10;
  NothingMuch* notMuch2 = new NothingMuch;
  notMuch2->mVal = 20;

  NothingMuch* notMuch3 = new NothingMuch;
  notMuch3->mVal = 30;
  NothingMuch* notMuch4 = new NothingMuch;
  notMuch4->mVal = 40;

  NothingMuch* notMuch5 = new NothingMuch;
  notMuch5->mVal = 50;
  NothingMuch* notMuch6 = new NothingMuch;
  notMuch6->mVal = 60;


  STSymbolTableNode* bnode = table.createBranch(atom, NULL);
  UtIO::cout() << "branch: ";
  bnode->print();

  // The NM1/2 things are here due to the symboltable slot idea being
  // transformed into a BOM. Much code here actually tests the slots
  // and will continue to do so, but in the BOM. I guess we can safely
  // make sure that the Data associated with a node is consistent with
  // usage by doing this.
  MiddleMan* mm = bom.castSym(bnode->getBOMData());
  mm->setNM1(notMuch1);
  mm->setNM2(notMuch2);
  
  atom = atomCache.intern("b");
  StringAtom* symCheck = atom; // used later
  STBranchNode* castBNode = bnode->castBranch();
  STSymbolTableNode* lnode1 = 
  table.createLeaf(atom, 
                   castBNode, 1);
  
  mm = bom.castSym(lnode1->getBOMData());
  mm->setNM1(notMuch3);
  mm->setNM2(notMuch4);
  
  UtIO::cout() << "leaf: ";
  lnode1->print();
  
  // a.c
  atom = atomCache.intern("c");
  STSymbolTableNode* lnode2 = table.createLeaf(atom,
                                               castBNode, 0);
  
  mm = bom.castSym(lnode2->getBOMData());
  mm->setNM1(notMuch5);
  mm->setNM2(notMuch6);  
  
  UtIO::cout() << "leaf: ";
  lnode2->print();

  // a.d.g
  AtomicCache otherCache;
  StringAtom* otherA = otherCache.intern("a");
  StringAtom* otherD = otherCache.intern("d");
  StringAtom* otherG = otherCache.intern("g");
  STEmptyFieldBOM emptyBOM;
  STSymbolTable otherSymTab(&emptyBOM, &otherCache);
  otherSymTab.setHdlHier(&hdlPath);
  STBranchNode* otherBranchA = otherSymTab.createBranch(otherA, NULL);
  // Make sure lookup works with hiernames from different string
  // caches
  assert(table.safeLookup(otherBranchA) != NULL);
  STBranchNode* otherBranchD = otherSymTab.createBranch(otherD, otherBranchA);
  STAliasedLeafNode* otherADG = otherSymTab.createLeaf(otherG, otherBranchD);
  STAliasedLeafNode* transNode = table.translateLeaf(otherADG);
  mm = bom.castSym(transNode->getBOMData());
  mm->setNM1(notMuch5);
  mm->setNM2(notMuch6);  
  mm = bom.castSym(transNode->getParent()->getBOMData());
  mm->setNM1(notMuch5);
  mm->setNM2(notMuch6);  

  // make a.b and a.c aliases
  STAliasedLeafNode* aliasNode1 = lnode1->castLeaf();
  assert(aliasNode1);
  
  STAliasedLeafNode* aliasNode2 = lnode2->castLeaf();
  assert(aliasNode2);
  
  if ((aliasNode1->getRingLen() != 1) && 
      (aliasNode2->getRingLen() != 1))
  {
    UtIO::cerr() << "STAliasedLeafNode initialization incorrect\n";
    status = 3;
  }
  aliasNode1->linkAlias(aliasNode2);
  
  if ((aliasNode1->getRingLen() != 2) && 
      (aliasNode2->getRingLen() != 2))
  {
    UtIO::cerr() << "STAliasedLeafNode linking incorrect (size)\n";
    status = 3;
  }
  
  if ((aliasNode1->getAlias() != aliasNode2) &&
      (aliasNode2->getAlias() != aliasNode1))
  {
    UtIO::cerr() << "STAliasedLeafNode linking incorrect (pointer)\n";
    status = 3;
  }

  aliasNode1->setThisMaster();
  aliasNode2->setThisStorage();
  if ((aliasNode2->getMaster() != aliasNode1) &&
      (aliasNode1->getMaster() != aliasNode1))
  {
    UtIO::cerr() << "Setting master failed." << UtIO::endl;
    status = 3;
  }
  
  if ((aliasNode1->getStorage() != aliasNode2) &&
      (aliasNode2->getStorage() != aliasNode2))
  {
    UtIO::cerr() << "Setting storage failed." << UtIO::endl;
    status = 3;
  }
  
  STBranchNode* branch = bnode->castBranch();
  assert(branch);
  STBranchNodeIter bIter(branch);
  STSymbolTableNode* bChild;

  // Should only go through this while loop twice
  int i = 0;
  SInt32 index;
  while((bChild = bIter.next(&index)))
  {
    if (index != i)
    {
      UtIO::cerr() << "BranchIter indexing is wrong\n";
      status = 3;
    }
    ++i;
    if ((bChild != lnode1) && (bChild != lnode2) && (bChild != transNode->getParent()))
    {
      UtIO::cerr() << "Branch built incorrectly\n";
      status = 3;
    }
  }
        
  if (i < 3)
  {
    UtIO::cerr() << "Branch has too few children\n";
    status = 3;
  }
  else if (i > 3)
  {
    UtIO::cerr() << "Branch has too many children?\n";
    status = 3;
  }

  // for completeness
  if (strcmp(lnode1->str(), "b"))
  {
    UtIO::cerr() << "StringAtom is screwed up in lnode1\n";
    status = 3;
  }

  if (lnode1->strObject() != symCheck)
  {
    UtIO::cerr() << "StringAtom pointer is not correct in lnode1\n";
    status = 3;
  }

  if ((lnode1->getParentName() != bnode) && 
      (lnode1->getParent() != bnode))
  {
    UtIO::cerr() << "Parent of lnode1 is incorrect\n";
    status = 3;
  }

  // value checking
  mm = bom.castSym(bnode->getBOMData());
  if (mm->getNM1() != notMuch1)
  {
    UtIO::cerr() << "Branch node, value 1 is wrong\n";
    status = 3;
  }
  
  if (mm->getNM2() != notMuch2)
  {
    UtIO::cerr() << "Branch node, value 2 is wrong\n";
    status = 3;
  }
  
  mm = bom.castSym(lnode1->getBOMData());
  if (mm->getNM1() != notMuch3)
  {
    UtIO::cerr() << "leaf node 1, value 1 is wrong\n";
    status = 3;
  }

  if (mm->getNM2() != notMuch4)
  {
    UtIO::cerr() << "Leaf node 1, value 2 is wrong\n";
    status = 3;
  }
  
  mm = bom.castSym(lnode2->getBOMData());
  if (mm->getNM1() != notMuch5)
  {
    UtIO::cerr() << "leaf node 2, value 1 is wrong\n";
    status = 3;
  }

  if (mm->getNM2() != notMuch6)
  {
    UtIO::cerr() << "Leaf node 2, slot id2 is wrong\n";
    status = 3;
  }
  
  for (STSymbolTable::CNodeLoop nodeIter = table.getCNodeLoop(); !nodeIter.atEnd(); ++nodeIter)
  {
    const STSymbolTableNode* iterNode = *nodeIter;
    const MiddleMan* cmm = bom.castSym(iterNode->getBOMData());
    const NothingMuch* iterObj = cmm->getNM1(); // checking 1 is enough
    if (! ((iterObj->mVal <= 60) && (iterObj->mVal >= 10)))
    {
      UtIO::cerr() << "Iteration field probing failed\n";
      status = 3;
    }
  }
  
  // Branch node random access test
  if ((branch->getChild(0) != lnode2) &&
      (branch->getChild(1) != lnode1))
  {
    UtIO::cerr() << "Random access into STBranchNode is incorrect.\n";
    status = 3;
  }
  
  
  HdlHierPath::Status parseStatus;
  HdlId info;
  STSymbolTableNode* lookupNode = table.getNode("a.b", &parseStatus, &info);
  if ((parseStatus != HdlHierPath::eLegal) || (lookupNode == NULL))
  {
    UtIO::cerr() << "getNode failed to parse or find\n";
    status = 3;
  }
  else if (lookupNode != aliasNode1)
  {
    UtIO::cerr() << "getNode returned the wrong node\n";
    status = 3;
  }

  
  {
    ZOSTREAMDB(outFile, "table1.db.gz");
    if (! outFile)
    {
      UtIO::cerr() << "Unable to open db file for write" << UtIO::endl;
      status = 1;
    }
    else
    {
      table.writeDB(outFile, NULL, true);
      outFile.close();
    }
  }
  
  
  {
    ZOSTREAMDB(outFile,"table2.db.gz");
    if (! outFile)
    {
      UtIO::cerr() << "Unable to open db file for write" << UtIO::endl;
      status = 1;
    }
    else
    {
      MyNodeSelect selector(aliasNode2);
      table.writeDB(outFile, &selector, true);
      outFile.close();
    }
  }
  
  {
    ZOSTREAMDB(outFile, "table3.db.gz");
    if (! outFile)
    {
      UtIO::cerr() << "Unable to open db file for write" << UtIO::endl;
      status = 1;
    }
    else
    {
      MyNodeSelect selector(aliasNode1);
      table.writeDB(outFile, &selector, true);
      outFile.close();
    }
  }
  
  table.print();
  
  MsgStreamIO errStream(stderr, true);
  MsgContext msgContext;
  msgContext.addReportStream(&errStream);

  STAliasedLeafNode* chLeaf;

  {
    AtomicCache cache3;
    STSymbolTable table3(&bom, &cache3);
    table3.setHdlHier(&hdlPath);
    ZISTREAMDB(inFile, "table1.db.gz");
    if (! inFile)
    {
      UtIO::cerr() << "Unable to open db file for read" << UtIO::endl;
      status = 1;
    }
    else
    {
      if (table3.readDB(inFile, &msgContext) != STFieldBOM::eReadOK)
        status = 3;
      else
      {
        table3.print();
        // make sure that the 2 nodes are there
        if (! (table3.getNode("a.b", &parseStatus, &info) &&
               table3.getNode("a.c", &parseStatus, &info)))
        {
          UtIO::cerr() << "DB read - w/ fields, failed" << UtIO::endl;
          status = 3;
        }
        else
        {
          lookupNode = table3.getNode("a", &parseStatus, &info);
          mm = bom.castSym(lookupNode->getBOMData());
          NothingMuch* n = mm->getNM1();
          if (n->mVal != notMuch1->mVal)
          {
            UtIO::cerr() << "branch field 1 incorrect" << UtIO::endl;
            status = 3;
          }
          n = mm->getNM2();
          if (n->mVal != notMuch2->mVal)
          {
            UtIO::cerr() << "branch field 2 incorrect" << UtIO::endl;
            status = 3;
          }
          
          lookupNode = table3.getNode("a.b", &parseStatus, &info);
          chLeaf = lookupNode->castLeaf();
          if (chLeaf->getMaster() != chLeaf)
          {
            UtIO::cerr() << "master alias wrong" << UtIO::endl;
            status = 3;
          }
          mm = bom.castSym(chLeaf->getBOMData());
          n = mm->getNM1();
          if (n->mVal != notMuch3->mVal)
          {
            UtIO::cerr() << "leaf a.b field 1 incorrect" << UtIO::endl;
            status = 3;
          }
          n = mm->getNM2();
          if (n->mVal != notMuch4->mVal)
          {
            UtIO::cerr() << "leaf a.b field 2 incorrect" << UtIO::endl;
            status = 3;
          }
          
          lookupNode = table3.getNode("a.c", &parseStatus, &info);
          chLeaf = lookupNode->castLeaf();
          if (chLeaf->getStorage() != chLeaf)
          {
            UtIO::cerr() << "Leaf storage wrong" << UtIO::endl;
            status = 3;
          }

          mm = bom.castSym(chLeaf->getBOMData());
          n = mm->getNM1();
          if (n->mVal != notMuch5->mVal)
          {
            UtIO::cerr() << "leaf a.b field 1 incorrect" << UtIO::endl;
            status = 3;
          }
          n = mm->getNM2();
          if (n->mVal != notMuch6->mVal)
          {
            UtIO::cerr() << "leaf a.b field 2 incorrect" << UtIO::endl;
            status = 3;
          }
          
          
        }
      }
      inFile.close();
      
    }
  }
  
  {
    AtomicCache cache4;
    STSymbolTable table4(&bom, &cache4);
    table4.setHdlHier(&hdlPath);
    ZISTREAMDB(inFile, "table2.db.gz");
    if (! inFile)
    {
      UtIO::cerr() << "Unable to open db file for read" << UtIO::endl;
      status = 1;
    }
    else
    {
      if (table4.readDB(inFile, &msgContext) != STFieldBOM::eReadOK)
        status = 3;
      else
      {
        table4.print();
        // make sure that the 1 node is there and the other is not
        if (! ((lookupNode = table4.getNode("a.b", &parseStatus, &info)) && 
               (table4.getNode("a.c", &parseStatus, &info) == NULL)))
        {
          UtIO::cerr() << "DB read with aliasnode2 de-selected failed" << UtIO::endl;
          status = 3;
        }
        else
        {
          chLeaf = lookupNode->castLeaf();
          if (chLeaf->getInternalStorage() != NULL)
          {
            UtIO::cerr() << "Leaf with no storage has storage attribute" << UtIO::endl;
            status = 3;
          }
        }
      }
      inFile.close();
    }
  }

  
  {
    AtomicCache cache5;
    STSymbolTable table5(&bom, &cache5);
    table5.setHdlHier(&hdlPath);
    ZISTREAMDB(inFile, "table3.db.gz");
    if (! inFile)
    {
      UtIO::cerr() << "Unable to open db file for read" << UtIO::endl;
      status = 1;
    }
    else
    {
      
      // try reading the db in with fields
      if (table5.readDB(inFile, &msgContext) != STFieldBOM::eReadOK)
        status = 3;
      else
      {
        table5.print();
        // make sure that the 1 node is there and the other is not
        if (! ((lookupNode = table5.getNode("a.c", &parseStatus, &info)) && 
               (table5.getNode("a.b", &parseStatus, &info) == NULL)))
        {
          UtIO::cerr() << "DB read with aliasnode1 de-selected failed" << UtIO::endl;
          status = 3;
        }
        else
        {
          chLeaf = lookupNode->castLeaf();
          if (chLeaf->getInternalStorage() != chLeaf)
          {
            UtIO::cerr() << "Leaf with storage has no storage attribute" << UtIO::endl;
            status = 3;
          }
        }
      }
      inFile.close();
      
    }
  }
  
  delete notMuch1;
  delete notMuch2;
  delete notMuch3;
  delete notMuch4;
  delete notMuch5;
  delete notMuch6;
  
  int tHierStat;
  if ((tHierStat = testHier()) != 0)
    status = tHierStat;
  
  return status;
}
