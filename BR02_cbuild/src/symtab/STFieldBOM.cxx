// -*- C++ -*-
/*****************************************************************************

 Copyright (c) 2004-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include "symtab/STFieldBOM.h"
#include "util/Zstream.h"
#include "util/CarbonAssert.h"
#include "symtab/STSymbolTable.h"
#include "iodb/IODBUserTypes.h"
#include <stdio.h>


static const char* scBOMSig = "EmptyField";

STEmptyFieldBOM::STEmptyFieldBOM()
{}

STEmptyFieldBOM::~STEmptyFieldBOM()
{}

void STEmptyFieldBOM::writeBOMSignature(ZostreamDB& out) const {
  out << scBOMSig;
}

  //! STFieldBOM::readBOMSignature()
STFieldBOM::ReadStatus 
STEmptyFieldBOM::readBOMSignature(ZistreamDB& in, UtString* errMsg)
{
  
  UtString tmpSig;
  in >> tmpSig;
  if (in.fail())
    return eReadFileError;
  
  ReadStatus stat = eReadOK;    
  if (tmpSig.compare(scBOMSig) != 0)
  {
    *errMsg << "Signature mismatch - expected '" << scBOMSig << "', got '" << tmpSig << "'";
    stat = eReadIncompatible;
  }
  return stat;
}

STFieldBOM::Data STEmptyFieldBOM::allocLeafData() {
  return NULL;
}

STFieldBOM::Data STEmptyFieldBOM::allocBranchData() {
  return NULL;
}

void STEmptyFieldBOM::freeLeafData(const STAliasedLeafNode* leaf, Data* bomdata) {
  if (bomdata)
    ST_ASSERT(*bomdata == NULL, leaf);
}

void STEmptyFieldBOM::freeBranchData(const STBranchNode* branch, Data* bomdata) {
  if (bomdata)
    ST_ASSERT(*bomdata == NULL, branch);
}

void STEmptyFieldBOM::preFieldWrite(ZostreamDB&)
{}

void STEmptyFieldBOM::writeLeafData(const STAliasedLeafNode*, ZostreamDB&) const
{}

void STEmptyFieldBOM::writeBranchData(const STBranchNode*, ZostreamDB&,
                                      AtomicCache*) const
{}

STFieldBOM::ReadStatus STEmptyFieldBOM::preFieldRead(ZistreamDB&)
{
  return eReadOK;
}

STFieldBOM::ReadStatus STEmptyFieldBOM::readLeafData(STAliasedLeafNode*, ZistreamDB&, 
                                                     MsgContext*) {
  return eReadIncompatible;
}
STFieldBOM::ReadStatus STEmptyFieldBOM::readBranchData(STBranchNode*, ZistreamDB&, 
                                                       MsgContext*) {
  return eReadIncompatible;
}

void STEmptyFieldBOM::printLeaf(const STAliasedLeafNode*) const 
{
  fprintf(stdout, "0x0\n");
}

void STEmptyFieldBOM::printBranch(const STBranchNode*) const 
{
  fprintf(stdout, "0x0\n");
}

//! Return BOM class name
const char* STEmptyFieldBOM::getClassName() const
{
  return "STEmptyFieldBOM";
}

//! Write the BOMData for a branch node
void STEmptyFieldBOM::xmlWriteBranchData(const STBranchNode* /*branch*/,UtXmlWriter* /*writer*/) const
{
}

//! Write the BOMData for a leaf node
void STEmptyFieldBOM::xmlWriteLeafData(const STAliasedLeafNode* /*leaf*/,UtXmlWriter* /*writer*/) const
{
}
    
