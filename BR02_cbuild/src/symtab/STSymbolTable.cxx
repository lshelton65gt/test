// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/


#include "symtab/STFieldBOM.h"
#include "symtab/STSymbolTable.h"
#include "util/AtomicCache.h"
#include "util/HierStringName.h"
#include "util/StringAtom.h"
#include "hdl/HdlId.h"
#include "util/UtString.h"
#include "util/UtIOStream.h"
#include "util/UtStringArray.h"
#include "util/UtStackPOD.h"
#include "util/UtXmlWriter.h"
#include "symtab/STAliasedLeafNodeNoAlloc.h"
#include <stdio.h>

/*!
  \file 
  Implementation of SymbolTable class
*/

static const char scIndent[] = "  ";
static const size_t scIndentSize = sizeof(scIndent) - 1;
static int  symbolTableCounter  = 0;
static void createSymbolTableName(char tableNameBuffer[128] ) 
{
  symbolTableCounter += 1;
  sprintf(tableNameBuffer, "Table#%d", symbolTableCounter);
}

STSymbolTable::STSymbolTable(STFieldBOM* fieldBOM, AtomicCache* stringCache) 
{
  INFO_ASSERT(stringCache, "Null string cache passed into symtab constructor");
  mHdlPath = NULL;
  mAtomCache = stringCache;
  setBOM(fieldBOM);
  char tableNameBufer[128];
  createSymbolTableName(tableNameBufer);
  mName = tableNameBufer; 
}

void STSymbolTable::setBOM(STFieldBOM* fieldBOM)
{
  mFieldBOM = fieldBOM;
  INFO_ASSERT(mFieldBOM, "NULL BOM manager set in symtab");
}

const STFieldBOM* STSymbolTable::getFieldBOM() const
{
  return mFieldBOM;
}

STFieldBOM* STSymbolTable::getFieldBOM()
{
  const STSymbolTable* me = const_cast<const STSymbolTable*>(this);
  return const_cast<STFieldBOM*>(me->getFieldBOM());
}


STSymbolTable::~STSymbolTable()
{
  // UtHashSet's can't erase an iterator when the hash function is using
  // a pointer value. So, the keys must be put into another container
  // to be deleted.
  typedef UtArray<STSymbolTableNode*> NodeVec;
  NodeVec doomedNodes;
  for (STSymbolTableNodeSet::UnsortedLoop iter = mNodes.loopUnsorted();
       ! iter.atEnd();
       ++iter) 
  {
    STSymbolTableNode* node = (STSymbolTableNode*) (void*) (*iter);
    doomedNodes.push_back(node);
    STFieldBOM::Data d = node->getBOMData();
    if (d)
    {
      STAliasedLeafNode* leaf = node->castLeaf();
      if (leaf)
        mFieldBOM->freeLeafData(leaf, &d);
      else
        mFieldBOM->freeBranchData(node->castBranch(), &d);
    }
  }
  mNodes.clear();
  
  for (NodeVec::iterator iter = doomedNodes.begin(),
         e = doomedNodes.end();
       iter != e;
       ++iter) 
    delete *iter;
}

SInt32 STSymbolTable::numNodes() const
{
  return mNodes.size();
}

STSymbolTableNode* 
STSymbolTable::createNode(StringAtom* childName, 
                          STBranchNode* parent,
                          bool isLeaf,
                          SInt32 childIndex)
{
  STSymbolTableNode* child = NULL;
  bool added = false;
  if (isLeaf)
    child = allocLeaf(childName, parent, true, &added);
  else
    child = allocBranch(childName, parent, &added);
  
  if (added)
    addNodeToParent(child, parent, childIndex);
  return child;
}

STBranchNode* 
STSymbolTable::createBranch(StringAtom* childName, 
                            STBranchNode* parent,
                            SInt32 childIndex)
{
  bool added = false;
  STBranchNode* child = allocBranch(childName, parent, &added);
  if (added)
    addNodeToParent(child, parent, childIndex);
  return child;
}

void STSymbolTable::addNodeToParent(STSymbolTableNode* child, 
                                    STBranchNode* parent, 
                                    SInt32 childIndex)
{
  if (parent)
  {
    if (childIndex > -1)
      parent->addChildByIndex(child, childIndex);
    else
      parent->addChild(child);
  }
  else
    addRoot(child);
}

STAliasedLeafNode* 
STSymbolTable::createLeaf(StringAtom* childName, 
                          STBranchNode* parent,
                          SInt32 childIndex,
                          bool hasAllocation)
{
  bool added = false;
  STAliasedLeafNode* child = allocLeaf(childName, parent, hasAllocation, &added);
  
  if (added)
    addNodeToParent(child, parent, childIndex);
  return child;
}

void STSymbolTable::destroyLeaf(STAliasedLeafNode *leaf)
{
  STFieldBOM::Data data = leaf->getBOMData();
  if (data) {
    mFieldBOM->freeLeafData(leaf, &data);
  }
  mNodes.erase(leaf);

  STBranchNode *parent = leaf->getParent();
  if (parent) {
    parent->removeChild(leaf);
  }

  delete leaf;
}

STAliasedLeafNode* 
STSymbolTable::createLeafFromPath(const char* path, HdlHierPath::Status* parseStatus, HdlId* info)
{
  STAliasedLeafNode* ret = NULL;

  UtStringArray ids;
  *parseStatus = mHdlPath->decompPath(path, &ids, info);
  if (*parseStatus == HdlHierPath::eLegal)
  {
    INFO_ASSERT(! ids.empty(), path);
    
    HdlHierPath::Status localStat;
    STBranchNode* branch;    
    STBranchNode* parent = NULL;
    STSymbolTableNode* node;

    UtString leafName(ids.back());
    INFO_ASSERT(! leafName.empty(), path);

    ids.pop_back();
    for (UtStringArray::UnsortedCLoop p = ids.loopCUnsorted(); 
         ! p.atEnd(); ++p)
    {
      const char* scopeName = *p;
      HdlId tmpInfo;
      node = getNode(scopeName, &localStat, &tmpInfo);
      if (node)
      {
        parent = node->castBranch();
        ST_ASSERT(parent != NULL, node);
      }
      else
      {
        StringAtom* branchName = mAtomCache->intern(scopeName);
        branch = createBranch(branchName, parent);
        parent = branch;
      }
    }
    
    StringAtom* leafAtom = mAtomCache->intern(leafName.c_str(), leafName.size());
    ret = createLeaf(leafAtom, parent);
  }
  
  return ret;
}

void STSymbolTable::LeafAssoc::clear ()
{
  struct pair *p = mFirst;
  while (p != NULL) {
    struct pair *tmp = p;
    p = p->mNext;
    delete tmp;
  }
  mFirst = NULL;
  mLast = NULL;
}

void STSymbolTable::LeafAssoc::append (const STAliasedLeafNode *src_leaf, STAliasedLeafNode *new_leaf)
{
  if (mFirst == NULL) {
    mFirst = new pair (src_leaf, new_leaf);
    mLast = mFirst;
  } else {
    mLast->mNext = new pair (src_leaf, new_leaf);
    mLast = mLast->mNext;
  }
}

STAliasedLeafNode* STSymbolTable::translateLeaf(const STAliasedLeafNode* leaf,
  LeafAssoc *list, STBranchNode *root)
{
  STAliasedLeafNode* ret = NULL;
  STSymbolTableNode* symNode = NULL;
  STBranchNode* branch = translateBranch(leaf->getParent(), root);
  StringAtom* leafName = mAtomCache->intern(leaf->strObject()->str());
  symNode = find(branch, leafName);
  if (symNode == NULL) {
    ret = createLeaf(leafName, branch);
    list->append (leaf, ret);
  } else {
    ST_ASSERT(symNode->castLeaf(), symNode);
    ret = symNode->castLeaf();
  }
  return ret;
}

STAliasedLeafNode* STSymbolTable::translateLeaf(const STAliasedLeafNode* leaf,
  STBranchNode *root)
{
  STAliasedLeafNode* ret = NULL;
  STSymbolTableNode* symNode = NULL;
  STBranchNode* branch = translateBranch(leaf->getParent(), root);
  StringAtom* leafName = mAtomCache->intern(leaf->strObject()->str());
  symNode = find(branch, leafName);
  if (symNode == NULL)
    ret = createLeaf(leafName, branch);
  else
  {
    ST_ASSERT(symNode->castLeaf(), symNode);
    ret = symNode->castLeaf();
  }
  return ret;
}

STBranchNode* STSymbolTable::translateBranch(const STBranchNode* node, STBranchNode *root)
{
  STBranchNode* bnode = NULL;
  STBranchNode* parent = NULL;

  UtStackPOD<const STBranchNode*> nodeStack;

  const STBranchNode* cur = node;
  while (cur != NULL)
  {
    mAtomCache->intern(cur->strObject()->str());
    nodeStack.push(cur);
    cur = cur->getParent();
  }

  if (root != NULL) {
    nodeStack.push (root);
  }

  while(! nodeStack.empty())
  {
    cur = nodeStack.top();

    // Assume from a different string cache
    STSymbolTableNode* symNode = safeLookup(cur);
    if (symNode)
    {
      ST_ASSERT(symNode->castLeaf() == NULL, symNode);
      bnode = symNode->castBranch();
    }
    else
      bnode = NULL;
    
    if (bnode == NULL)
    {
      // Assume from a different string cache
      StringAtom* brAtom = mAtomCache->intern(cur->strObject()->str());
      bnode = createBranch(brAtom, parent);
    }
    parent = bnode;
    
    nodeStack.pop();
  }
  return bnode;
}

void STSymbolTable::addRoot(STSymbolTableNode* root)
{
  RootSet::iterator p = mRoots.find(root);
  if (p == mRoots.end())
    mRoots.insert(root);
}

void STSymbolTable::print()
{
  UtString indent;
  UtOStream& ut_cout = UtIO::cout();
  ut_cout << "Num nodes: " << numNodes() << UtIO::endl;
  ut_cout << "AtomicCache: " << UtIO::hex << mAtomCache << UtIO::endl;
  ut_cout << UtIO::endl;
  
  
  STSymbolTableNode* node;
  STBranchNode* branch;
  STAliasedLeafNode* leaf;
  
  for (RootIter tableIter = getRootIter(); !tableIter.atEnd(); ++tableIter)
  {
    node = *tableIter;
    if ((branch = node->castBranch()))
      printBranch(branch, indent);
    else if ((leaf = node->castLeaf()))
      printLeaf(leaf, indent);
  }
  ut_cout << UtIO::endl;
}

void STSymbolTable::printBranch(STBranchNode* node, UtString& indent)
{
  UtOStream& ut_cout = UtIO::cout();
  ut_cout << indent << "Name: " << node->str() << " (" << UtIO::hex << node << ')' << UtIO::endl;
  ut_cout << indent << "Parent: " << UtIO::hex << node->getParent() << UtIO::endl;
  ut_cout << indent << "BOM: ";
  mFieldBOM->printBranch(node);
  ut_cout << UtIO::endl;
  
  SInt32 numChildren = node->numChildren();
  STBranchNode* branch;
  STAliasedLeafNode* leaf;
  STSymbolTableNode* chNode;
  UtArray<STBranchNode*> branches;
  UtArray<STAliasedLeafNode*> leaves;  
  for (SInt32 i = 0; i < numChildren; ++i)
  {
    chNode = node->getChild(i);
    if (chNode != NULL) {
      if ((branch = chNode->castBranch()))
        branches.push_back(branch);
      else if ((leaf = chNode->castLeaf()))
        leaves.push_back(leaf);
    }
  }
  
  if (numChildren > 0)
  {
    indent += scIndent;
    UInt32 j;
    for (j = 0; j < leaves.size(); ++j)
      printLeaf(leaves[j], indent);
    for (j = 0; j < branches.size(); ++j)
      printBranch(branches[j], indent);
    
    indent.resize(indent.size() - scIndentSize);
  }
}

void STSymbolTable::printLeaf(STAliasedLeafNode* node, UtString& indent)
{
  UtOStream& ut_cout = UtIO::cout();
  ut_cout << indent << "Name: " << node->str() << " (" << UtIO::hex << node << ')' << UtIO::endl;
  ut_cout << indent << "Parent: " << UtIO::hex << node->getParent() << UtIO::endl;
  ut_cout << indent << "Master: " << UtIO::hex << node->getMaster() << UtIO::endl;
  ut_cout << indent << "Store: " << UtIO::hex << node->getStorage() << UtIO::endl;
  ut_cout << indent << "BOM: ";
  mFieldBOM->printLeaf(node);
  
  ut_cout << indent << "Aliases:\n";
  indent += scIndent;
  STAliasedLeafNode* alias = node->getAlias();
  while (alias != node)
  {
    ut_cout << indent << UtIO::hex << alias << UtIO::endl;
    alias = alias->getAlias();
  }
  indent.resize(indent.size() - scIndentSize);
}

STBranchNode* STSymbolTable::allocBranch(StringAtom* name,
                                         STBranchNode* parent,
                                         bool* added)
{
  *added = true;
  STBranchNode* node = NULL;

  // The node has not been added to mNodes yet. Check if it is already
  // there from a previous alloc
  STSymbolTableNode* check = find(parent, name);

  if (check == NULL)
  {
    node = new STBranchNode(name, mFieldBOM->allocBranchData());
    registerNode(node, parent);      
  }
  else
  {
    *added = false;
    // it better be a branch!
    STBranchNode* branch = check->castBranch();
    ST_ASSERT(branch != NULL, check);
    ST_ASSERT(parent->isChild(branch), parent);
    node = branch;
  }

  return node;
}

STAliasedLeafNode* STSymbolTable::allocLeaf(StringAtom* name,
                                            STBranchNode* parent,
                                            bool hasAllocation,
                                            bool* added)
{
  *added = true;
  STAliasedLeafNode* node = NULL;

  // The node has not been added to mNodes yet. Check if it is already
  // there from a previous alloc
  STSymbolTableNode* check = find(parent, name);
  
  if (check == NULL)
  {
    if (hasAllocation)
      node = new STAliasedLeafNode(name, mFieldBOM->allocLeafData());
    else
      node = new STAliasedLeafNodeNoAlloc(name, mFieldBOM->allocLeafData());
    
    registerNode(node, parent);  
  }
  else
  {
    *added = false;
    // it better be a leaf!
    STAliasedLeafNode* leaf = check->castLeaf();
    ST_ASSERT(leaf != NULL, check);
    ST_ASSERT(parent->isChild(leaf), parent);
    node = leaf;
  }
  
  return node;
}

void STSymbolTable::registerNode(STSymbolTableNode* node, STBranchNode* parent)
{
  node->putParent(parent);
  mNodes.insert(node);
}

STSymbolTable::NodeLoop STSymbolTable::getNodeLoop()
{
  // NodeLoop and CNodeLoop are LoopFunctors which need a Loop
  // implementation passed to it
  return NodeLoop(mNodes.loopUnsorted());
}

STSymbolTable::CNodeLoop STSymbolTable::getCNodeLoop() const
{
  // NodeLoop and CNodeLoop are LoopFunctors which need a Loop
  // implementation passed to it
  return CNodeLoop(mNodes.loopCUnsorted());
}


STSymbolTable::NodeLoopSorted STSymbolTable::getNodeLoopSorted() const
{
  // NodeLoopSorted is a  LoopFunctor which needs a Loop
  // implementation passed to it
  return NodeLoopSorted(mNodes.loopSorted());
}

STSymbolTable::RootIter STSymbolTable::getRootIter()
{
  return mRoots.loopUnsorted();
}

STSymbolTable::SortedRootIter STSymbolTable::getSortedRootIter()
{
  return mRoots.loopSorted();
}

void STSymbolTable::setHdlHier(const HdlHierPath* hdlPath)
{
  mHdlPath = hdlPath;
}

const HdlHierPath* STSymbolTable::getHdlHier() const
{
  return mHdlPath;
}

STSymbolTableNode* STSymbolTable::getNode(const char* path, HdlHierPath::Status* parseStatus, HdlId* info, HierName *start_parent)
{
  const STSymbolTable* me = const_cast<const STSymbolTable*>(this);
  return const_cast<STSymbolTableNode*>(me->getNode(path, parseStatus, info, start_parent));
}  

const STSymbolTableNode* STSymbolTable::getNode(const char* path, HdlHierPath::Status* parseStatus, HdlId* info, HierName *start_parent) const
{
  const STSymbolTableNode* ret = NULL;
  HdlHierPath::StrAtomVec pathAtoms;
  INFO_ASSERT(mHdlPath, path);
  INFO_ASSERT(info, path);
  *parseStatus = mHdlPath->decompPathAtom(path, &pathAtoms, mAtomCache, info);
  if (*parseStatus == HdlHierPath::eLegal)
  {
    UtArray<HierStringName*> hdlName;
    hdlName.reserve(pathAtoms.size());

    HdlHierPath::StrAtomVec::iterator p = pathAtoms.begin();
    while (p != pathAtoms.end())
    {
      hdlName.push_back(new HierStringName(*p));
      ++p;
    }
    
    HierName* parent = start_parent;
    HierStringName* cur = NULL;
    UInt32 numNames = hdlName.size();
    for (UInt32 i = 0; i < numNames; ++i)
    {
      cur = hdlName[i];
      cur->putParent(parent);
      parent = cur;
    }
    
    const HierName* name = hdlName.back();
    ret = lookup(name);

    for (UInt32 i = 0; i < numNames; ++i)
      delete hdlName[i];
  } // if
  
  return ret;
}

AtomicCache* STSymbolTable::getAtomicCache()
{
  return mAtomCache;
}

STSymbolTableNode* STSymbolTable::safeLookup(const HierName* name)
{
  const STSymbolTable* me = const_cast<const STSymbolTable*>(this);
  return const_cast<STSymbolTableNode*>(me->safeLookup(name));
}

const STSymbolTableNode* STSymbolTable::safeLookup(const HierName* name) const
{
  /*
    Because some names may NOT be valid HDL we need to do lookups
    based on StringAtom 
    The StringAtoms in the passed in name may not be from the same
    string cache as this table's nodes. So, a tmp HierName needs to be
    created with this table's stringatoms if they exist.
  */

  const STSymbolTableNode* ret = NULL;
  
  // First, check if all the stringatoms in the name exist in this
  // string cache
  bool allExist = true;
  const HierName* curTmp = name;
  UtArray<StringAtom*> pathAtoms;

  do {
    StringAtom* curAtom = mAtomCache->getIntern(curTmp->strObject()->str());
    allExist = curAtom != NULL;
    if (allExist)
      pathAtoms.push_back(curAtom);
    curTmp = curTmp->getParentName();
  } while ((curTmp != NULL) && allExist);
  
  if (allExist)
  {
    // Now, create a HierName with this table's string atoms.
    UtArray<HierStringName*> hdlName;
    SInt32 numAtoms = pathAtoms.size();
    hdlName.reserve(numAtoms);
    
    // reverse iteration doesn't work properly in gcc 2.95
    for (SInt32 i = numAtoms - 1; i >= 0; --i)
      hdlName.push_back(new HierStringName(pathAtoms[i]));
    

    // need to hookup the hierarchy
    HierName* parent = NULL;
    HierStringName* cur = NULL;
    UInt32 numNames = hdlName.size();
    for (UInt32 i = 0; i < numNames; ++i)
    {
      cur = hdlName[i];
      cur->putParent(parent);
      parent = cur;
    }
    
    const HierName* transformedName = hdlName.back();
    // now we can do a lookup based on this name
    ret = lookup(transformedName);

    for (UInt32 i = 0; i < numNames; ++i)
      delete hdlName[i];
  }
  return ret;
}

STSymbolTableNode* STSymbolTable::lookup(const HierName* name)
{
  const STSymbolTable* me = const_cast<const STSymbolTable*>(this);
  return const_cast<STSymbolTableNode*>(me->lookup(name));
}

const STSymbolTableNode* STSymbolTable::lookup(const HierName* name)
  const
{
  const STSymbolTableNode* ret = NULL;
  STSymbolTableNodeSet::const_iterator p = mNodes.find(const_cast<HierName*>(name));
  if (p != mNodes.end())
    ret = (const STSymbolTableNode*)(void*)(*p);

  return ret;
}

STSymbolTableNode* STSymbolTable::find(const HierName* parent,
                                       StringAtom* name)
{
  const STSymbolTable* me = const_cast<const STSymbolTable*>(this);
  return const_cast<STSymbolTableNode*>(me->find(parent, name));
}

const STSymbolTableNode* STSymbolTable::find(const HierName* parent,
                                             StringAtom* name)
  const
{
  HierStringName tmp(name);
  tmp.putParent(const_cast<HierName*>(parent));
  return lookup(&tmp);
}

STSymbolTableNode* STSymbolTable::dbgFindNode(const char* hierPath)
{
  STSymbolTableNode* ret = NULL;
  HdlHierPath::Status stat;
  HdlId info;
  if ( mHdlPath == NULL ) {
    fprintf(stderr, "SEARCH IMPOSSIBLE (mHdlPath is null): %s\n", hierPath);
    return NULL;
  }
  ret = getNode(hierPath, &stat, &info);
  if (! ret)
  {
    if (stat == HdlHierPath::eIllegal)
      fprintf(stderr, "INVALID PATHNAME: %s\n", hierPath);
    else if (stat == HdlHierPath::eNoExist)
      fprintf(stderr, "DOES NOT EXIST: %s\n", hierPath);
  }
  return ret;
}

STSymbolTableNode* STSymbolTable::dbgFindInternal(const char* hierPath)
{
  UtStringArray ids;
  const char* begin = hierPath;
  const char* cur = begin;
  UtString tmp;
  while (cur && *cur != '\0')
  {
    if (*cur == '.')
    {
      tmp.clear();
      tmp.append(begin, cur - begin);
      ids.push_back(tmp);
      begin = cur + 1; // bypass '.'
    }
    ++cur;
  }

  if (begin != cur)
    ids.push_back(begin);
  
  if (ids.empty())
  {
    fprintf(stderr, "Empty string!!!\n");
    return NULL;
  }

  HdlHierPath::StrAtomVec atomVec;
  
  bool found = true;
  for (UtStringArray::UnsortedLoop p = ids.loopUnsorted(); found && ! p.atEnd(); ++p)
  {
    const char* identifier = *p;
    StringAtom* atom = mAtomCache->getIntern(identifier);
    found = (atom != NULL);
    if (found)
      atomVec.push_back(atom);
  }
  
  if (ids.size() != atomVec.size())
  {
    fprintf(stderr, "DOES NOT EXIST: %s\n", hierPath);
    return NULL;
  }

  STSymbolTableNode* ret = NULL;
  found = true;
  for (HdlHierPath::StrAtomVec::iterator p = atomVec.begin(), 
         e = atomVec.end();
       found && (p != e); ++p)
  {
    StringAtom* atom = *p;
    ret = find(ret, atom);
    found = (ret != NULL);
  }
  
  if (! found)
    fprintf(stderr, "DOES NOT EXIST: %s\n", hierPath);
  
  return ret;
}

// STNodeSelectDB implementation

bool STNodeSelectDB::selectLeaf(const STAliasedLeafNode* leaf, const HdlHierPath* hdlPath)
{
  bool ret = isAlwaysSelected(leaf);
  if (! ret)
    ret = computeSelectLeaf(leaf, hdlPath);
  return ret;
}

bool STNodeSelectDB::selectBranch(const STBranchNode* branch, const HdlHierPath* hdlPath)
{
  bool ret = isAlwaysSelected(branch);
  if (! ret)
    ret = computeSelectBranch(branch, hdlPath);
  return ret;
}

void STNodeSelectDB::requestAlwaysSelectLeaf(const STAliasedLeafNode* leaf)
{
  requestAlwaysSelect(leaf);
}

void STNodeSelectDB::requestAlwaysSelectBranch(const STBranchNode* branch)
{
  requestAlwaysSelect(branch);
}

bool STNodeSelectDB::isAlwaysSelected(const STSymbolTableNode* node) const
{
  return mAlwaysSelect.find(node) != mAlwaysSelect.end();
}

UInt32 STSymbolTable::numRoots() const {
  return mRoots.size();
}

void STSymbolTable::setName(UtString& name) 
{
  mName = name;
}


void STSymbolTable::writeXml(UtXmlWriter* xmlWriter,UtString& fileName)
{
  xmlWriter->StartWriter(fileName);
  writeXml(xmlWriter);
  xmlWriter->EndWriter();
}


void STSymbolTable::writeXml(UtXmlWriter* xmlWriter)
{
  xmlWriter->StartElement("SymbolTable");

  xmlWriter->WriteAttribute("Name", mName.c_str());
  const char* xmlBomClassName = mFieldBOM->getClassName();
  xmlWriter->WriteAttribute("BOMClass", xmlBomClassName);

  writeXmlTable(xmlWriter);
  xmlWriter->EndElement();
}



void STSymbolTable::writeXmlTable(UtXmlWriter* xmlWriter) 
{
  STSymbolTableNode* node;
  STBranchNode* childBranch;
  STAliasedLeafNode* childLeaf;
  UtArray<STBranchNode*> childBranches;
  UtArray<STAliasedLeafNode*> childLeaves;

  
  for (RootIter tableIter = getRootIter(); !tableIter.atEnd(); ++tableIter)
  {
    node = *tableIter;
    childBranch = node->castBranch();
    childLeaf   = node->castLeaf();
    if (childBranch != NULL) {
      childBranches.push_back(childBranch);
    }
    if(childLeaf != NULL){
      childLeaves.push_back(childLeaf);
    }
  }
  writeXmlLeaves(childLeaves,xmlWriter); 
  writeXmlBranches(childBranches,xmlWriter);

  return;

}

void STSymbolTable::writeXmlBranches(UtArray<STBranchNode*>& sourceBranches,UtXmlWriter* xmlWriter) 
{
  int numBranches = sourceBranches.size();
  if(numBranches == 0) {
    return;
  }
  xmlWriter->StartElement("Branches");
  for(UInt32 i = 0; i < sourceBranches.size(); i++) 
  {
    STBranchNode* sourceBranch = sourceBranches[i];
    writeXmlBranchNode(sourceBranch, xmlWriter);
  }
  xmlWriter->EndElement();
  return;

}


void STSymbolTable::writeXmlLeaves(UtArray<STAliasedLeafNode*>& sourceLeaves,UtXmlWriter* xmlWriter) 
{
  int numLeaves =  sourceLeaves.size();
  if(numLeaves == 0) {
    return;
  } 
  xmlWriter->StartElement("Leaves");
  for(UInt32 i = 0;i < sourceLeaves.size(); i++) 
  {
    STAliasedLeafNode* sourceLeaf = sourceLeaves[i];
    writeXmlLeafNode(sourceLeaf, xmlWriter);
  }
  xmlWriter->EndElement();
  return;
}

void STSymbolTable::writeXmlBranchNode(const STBranchNode*  sourceBranch,UtXmlWriter* xmlWriter)
{
  if(sourceBranch == NULL) {
    return;
  }
  const char* branchName = sourceBranch->str();
  if(branchName == NULL) {
    return;
  }

  xmlWriter->StartElement( "Branch");
  xmlWriter->WriteAttribute("Name", branchName);
  xmlWriter->WriteAttribute("Pointer", (const void*)sourceBranch);
  writeXmlBranchData(sourceBranch, xmlWriter);

  STBranchNode* childBranch;
  STAliasedLeafNode* childLeaf;
  STSymbolTableNode* childNode;
  UtArray<STBranchNode*> childBranches;
  UtArray<STAliasedLeafNode*> childLeaves;

  int numChildren = sourceBranch->numChildren();
  for (int i = 0; i < numChildren; ++i)
  {
    childNode = (STSymbolTableNode*)sourceBranch->getChild(i);
    if(childNode == NULL) {
      continue;
    }
    childBranch = childNode->castBranch();
    childLeaf   = childNode->castLeaf();
    if(childLeaf != NULL) {
      childLeaves.push_back(childLeaf);
    }
    if(childBranch != NULL) {
      childBranches.push_back(childBranch);
    }
  }
  writeXmlLeaves(childLeaves,xmlWriter);
  writeXmlBranches(childBranches,xmlWriter);
 
  xmlWriter->EndElement();
}

void STSymbolTable::writeXmlLeafNode(const STAliasedLeafNode*  sourceLeaf,UtXmlWriter* xmlWriter)
{
  if(sourceLeaf == NULL) {
    return;
  }
  const char* leafName = sourceLeaf->str();
  if(leafName == NULL) {
    return;
  }

  xmlWriter->StartElement("Leaf");
  xmlWriter->WriteAttribute("Name", leafName);
  xmlWriter->WriteAttribute("Pointer", (const void*)sourceLeaf);
  writeXmlLeafData(sourceLeaf, xmlWriter);
  xmlWriter->EndElement();
  
}

void STSymbolTable::writeXmlLeafData(const STAliasedLeafNode*  sourceLeaf,UtXmlWriter* xmlWriter)
{
  if(sourceLeaf == NULL) {
    return;
  }
  STFieldBOM::Data leafData = sourceLeaf->getBOMData();
  if(leafData == NULL) {
    return;
  }
  xmlWriter->StartElement("Data");
  xmlWriter->WriteAttribute("Pointer", (const void*)leafData);
  mFieldBOM->xmlWriteLeafData(sourceLeaf, xmlWriter);
  xmlWriter->EndElement();
  
}



void
STSymbolTable::writeXmlBranchData(const STBranchNode*  sourceBranch,UtXmlWriter* xmlWriter)
{
  if(sourceBranch == NULL) {
    return;
  }
  STFieldBOM::Data branchData = sourceBranch->getBOMData();
  if(branchData == NULL) {
    return;
  }

  xmlWriter->StartElement("Data");
  xmlWriter->WriteAttribute("Pointer", (const void*)branchData);
  mFieldBOM->xmlWriteBranchData(sourceBranch, xmlWriter);
  xmlWriter->EndElement();

  
}

void STSymbolTable::writeXml(UtXmlWriter* xmlWriter,UtArray<STSymbolTable*>& tables) 
{
  xmlWriter->StartElement("SymbolTables");
  for(UtArray<STSymbolTable*>::const_iterator it = tables.begin();it != tables.end(); it++) {
    STSymbolTable* s = *it;
    s->writeXml(xmlWriter);
  }
  xmlWriter->EndElement();

}
void STSymbolTable::writeXml(UtXmlWriter* xmlWriter,UtArray<STSymbolTable*>& tables,UtString& fileName)
{
  xmlWriter->StartWriter(fileName);
  writeXml(xmlWriter, tables);
  xmlWriter->EndWriter();
}


