/*****************************************************************************
 Copyright (c) 2006-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/* Include file with simulator/language independent utilities */
#include <stdlib.h>
#include "carbon/MVUtils.c"
#include "vpi_user.h"


/*
 * Types for the structures and utility functions
 */
typedef struct VpiInput_ VpiInput;
typedef struct VpiOutput_ VpiOutput;
typedef void (*ApplyFn)(VpiInput* input);
typedef PLI_INT32 (*VpiCallbackFn)(s_cb_data*);

/*
 * Structures to manage copying data back and forth from the two
 * models.
 */
typedef struct VpiCarbonModel_ {
  CarbonVHM       vhm;          /* Carbon Model data */
  char            cbRegistered; /* True if cb registered since last Carbon Model call */
  VpiCallbackFn   evalCallback; /* The function to execute the Carbon Model */
  PLI_BYTE8*      cbData;       /* User data to pass to the callback */
} VpiCarbonModel;

/* Structure to manage copying data from the Simulator to the Carbon Model */
struct VpiInput_ {
  vpiHandle       simHndl;      /* Simulators handle for the input net */
  CarbonNetID*    carbonHndl;   /* Carbon Model's handle to write the Carbon value */
  CarbonUInt32    size;         /* The number of bits for the signal */

  vpiHandle       cbHndl;       /* Handle for the value callback (to free) */
  s_vpi_value     value;        /* Storage for callback value */

  CarbonUInt32	  delay;	/* Amount of time to delay depositing the value */
  char            delayedCallbackRegistered;
                                /* True if time delay callback was registered */

  CarbonUInt32*   carbonVal;    /* Storage to write Carbon value */
  CarbonUInt32*   carbonDrive;  /* Storage to write Carbon drive bits */
  char            changed;      /* If set, the value changed in Simulator */

  ApplyFn         applyFn;      /* Function to apply the Simulator value */
  VpiCarbonModel* carbon;       /* Data to apply changes to the Carbon model */
};

/* Structure to manage copying data from the Carbon Model to the Simulator */
struct VpiOutput_ {
  vpiHandle       simHndl;      /* Simulators handle for the output net */
  vpiHandle       simDataHndl;  /* Simulators handle for the output net data */
  vpiHandle       simDriveHndl; /* Simulators handle for the output net enable */
  CarbonNetID*    carbonHndl;   /* Carbon Model's handle to read the Carbon value */
  CarbonUInt32    size;         /* The number of bits for the signal */

  CarbonUInt32	  delay;	/* Amount of time to delay depositing the value */
  s_vpi_value     value;        /* Storage to write the Simulator value */
  CarbonUInt32*   carbonDrive;  /* Whether Carbon model is driving (for Bids) */
  char            isModelTristate; /* If this MV output is really a Carbon tristate */

  VpiCarbonModel* carbon;       /* Data to apply changes to the Carbon model */
};

/* Structure to manage copying bidirectional data */
typedef struct VpiBid_ {
  VpiInput        in;           /* To manage Simulator -> Carbon Model part of a bid */
  VpiOutput       out;          /* To manage Carbon Model -> Simulator part of a bid */
} VpiBid;

/* Structure to manage the Carbon command data */
typedef struct VpiCarbonCmd_ {
  CarbonCmdArgs   cmdArgs;      /* The parsed arguments */
  int             hasResult;    /* If TRUE, the last argument is a result */
  vpiHandle       result;       /* Keep track of result handle */
} VpiCarbonCmd;


/*
 * Functions to print I/O traces
 */
static void
printTraceFunction(const char* typeStr, const char* function, vpiHandle hndl)
{
#ifdef VPI_TRACE
  s_vpi_time    time;
  const char*   name;

  /* Get the name and time */
  name = vpi_get_str(vpiName, hndl);
  time.type = vpiSimTime;
  vpi_get_time(NULL, &time);

  vpi_printf("Trace: %d: %-3.3s: %s for %s\n", time.low, typeStr, function, name);
#endif
}

static void
printTraceData(const char* typeStr, CarbonUInt32 wordSize, CarbonUInt32* val,
               CarbonUInt32* drive)
{
#ifdef VPI_TRACE
  CarbonUInt32  i;
  s_vpi_time    time;

  /* Get the time */
  time.type = vpiSimTime;
  vpi_get_time(NULL, &time);

  for (i = 0; i < wordSize; ++i) {
    vpi_printf("Trace: %d: %-3.3s: Value[%d] = %x", time.low, typeStr, i, val[i]);
    if (drive != NULL) {
      vpi_printf(", Drive[%d] = %x", i, drive[i]);
    }
    vpi_printf("\n");
  }
#endif
}

static void
printTraceString(const char* typeStr, const char* str)
{
#ifdef VPI_TRACE
  s_vpi_time    time;

  /* Get the time */
  time.type = vpiSimTime;
  vpi_get_time(NULL, &time);

  vpi_printf("Trace: %d: %-3.3s: %s\n", time.low, typeStr, str);
#endif
}


/*
 * Functions for reading and writing Simulator values
 */
static void transValue(VpiInput* input)
{
  CarbonUInt32  wordSize;
  CarbonUInt32  i;
  s_vpi_value   value;
  s_vpi_vecval* vector;

  /* Update the user trace function (if enable) */
  printTraceFunction("Sim", "transValue()", input->simHndl);
#ifdef VPI_TRACE
  for (i = 0; i < WORD_SIZE(input->size); ++i) {
    vpi_printf("aval[%d] = %x, bval[%d] = %x\n",
               i, input->value.value.vector[i].aval,
               i, input->value.value.vector[i].bval);
  }
#endif

  /* Update the return values. We convert like this:
   *
   *            vpi  vpi   carbon carbon
   *     value  aval bval  value  drive
   *       0      0   0      0      0
   *       1      1   0      1      0
   *       x      1   1      0      0
   *       z      0   1      1      1
   */
  wordSize = WORD_SIZE(input->size);
  vector = input->value.value.vector;
  for (i = 0; i < wordSize; ++i) {
    input->carbonVal[i] = (vector[i].aval ^ vector[i].bval);
    input->carbonDrive[i] = (vector[i].bval & (~vector[i].aval));
  }

  /* Update the user trace (if enabled) */
  printTraceData("", wordSize, input->carbonVal, input->carbonDrive);
}

static void
putSimValue(vpiHandle hndl, s_vpi_value* value, CarbonUInt32 delay)
{
  s_vpi_error_info      error_info;
  s_vpi_time            time = { vpiSimTime, 0, delay, 0 };

  /* Apply the new output. Delay if configured to do so */
  if (delay != 0) {
    vpi_put_value(hndl, value, &time, vpiInertialDelay);
  } else {
    vpi_put_value(hndl, value, NULL, vpiNoDelay);
  }
  if (vpi_chk_error(&error_info)) {
    vpi_printf("%s\n", error_info.message);
  }
}

static void
carbonToVpi(CarbonUInt32 words, CarbonUInt32* val, CarbonUInt32* drive,
            s_vpi_vecval* vector)
{
  CarbonUInt32        i;

  /* Convert the data to the right format
   *
   * When drive is not in use, the conversion is:
   *
   *  carbon vpi  vpi  
   *  value  aval bval value
   *    0     0    0     0
   *    1     1    0     1
   *
   * When drive is in use, the conversion is:
   *
   *  carbon carbon vpi  vpi  
   *  value  drive  aval bval value
   *    0      0     0    0     0
   *    1      0     1    0     1
   *    0      1     0    1     z
   *    1      1     0    1     z
   */
  for (i = 0; i < words; ++i) {
    if (drive) {
      /* Can drive a Z */
      vector[i].bval = drive[i];
      vector[i].aval = val[i] & (~drive[i]);

    } else {
      /* Driving without Z's */
      vector[i].aval = val[i];
      vector[i].bval = 0;
    }
  }
}

static void
putValue(VpiOutput* output, CarbonUInt32* val, CarbonUInt32* drive)
{
  CarbonUInt32  wordSize;

  /* Update the user trace function and data (if enabled) */
  wordSize = WORD_SIZE(output->size);
  printTraceFunction("Sim", "putValue()", output->simHndl);
  printTraceData("", wordSize, val, drive);

  /* Write the data to the simulator */
  carbonToVpi(wordSize, val, drive, output->value.value.vector);
  putSimValue(output->simHndl, &output->value, output->delay);
}

static void
driveValue(VpiOutput* output, CarbonUInt32* val, CarbonUInt32* drive)
{
  CarbonUInt32  i;
  CarbonUInt32  wordSize;
  s_vpi_vecval* vector;

  /* Update the user trace function and data (if enabled) */
  wordSize = WORD_SIZE(output->size);
  printTraceFunction("Sim", "driveValue()", output->simHndl);
  printTraceData("", wordSize, val, drive);

  /* With Verilog we installed bufif's in the generated code. Set the
   * enable signal first.
   *
   * When drive is not in use, the conversion is:
   *
   *  carbon sim  sim  
   *  value  en  data value
   *    0     1    0     0
   *    1     1    1     1
   *
   * When drive is in use, the conversion is:
   *
   *  carbon carbon sim  sim  
   *  value  drive  en  data value
   *    0      0     1    0     0
   *    1      0     1    1     1
   *    0      1     0    0     z
   *    1      1     0    0     z
   */
  vector = output->value.value.vector;
  for (i = 0; i < wordSize; ++i) {
    if (drive) {
      /* Set the enable to 1 for every bit we are driving */
      vector[i].aval = (~drive[i]);
    } else {
      /* No drive must mean we are always driving */
      vector[i].aval = -1;
    }
    vector[i].bval = 0;
  }
  putSimValue(output->simDriveHndl, &output->value, output->delay);

  /* Set the data signal */
  for (i = 0; i < wordSize; ++i) {
    if (drive) {
      /* Only set the bits that are driven */
      vector[i].aval = val[i] & (~drive[i]);
    } else {
      /* set all the bits */
      vector[i].aval = val[i];
    }
  }
  putSimValue(output->simDataHndl, &output->value, output->delay);
}


/*
 * Functions to write Carbon nets (delayed data writes)
 */
static void
updateCarbonNet(VpiInput* input)
{
  carbonDepositFast(input->carbon->vhm.model, input->carbonHndl, input->carbonVal,
                    input->carbonDrive);
  input->changed = FALSE;
}

static void
vpiApplyClockInput(VpiInput* input)
{
  /* Update the user trace function and data (if enabled) */
  printTraceFunction("CM", "vpiApplyClockInput()", input->simHndl); /* "CM" for "Carbon Model" */
  printTraceData("", WORD_SIZE(input->size), input->carbonVal, input->carbonDrive);

  /* Update the Carbon value immediately and set the clock changed flag */
  updateCarbonNet(input);
  mvSetClockChanged(&input->carbon->vhm);
}

static void
vpiApplyDataInput(VpiInput* input)
{
  /* Update the user trace function (if enabled) */
  printTraceFunction("CM", "vpiApplyDataInput() [delayed]", input->simHndl);

  /* Only set the changed flags, it gets applied during the update pass */
  input->changed = TRUE;
  mvSetDataChanged(&input->carbon->vhm);
}

static void
vpiApplyClockBid(VpiInput* input)
{
  /* We count on the fact that the VpiInput part is at the beginning
     of a VpiBid. */
  VpiBid*       bid = (VpiBid*)input;

  /* Resolve the Carbon/Simulator drive mask before writing to
     Carbon */
  mvResolveDriveMask(bid->in.size, bid->in.carbonDrive, bid->out.carbonDrive);

  /* Now we can apply the input */
  vpiApplyClockInput(&bid->in);
}

static void
vpiUpdateDataInput(VpiInput* input)
{
  if (input->changed) {
    /* Update the user trace function and data (if enabled) */
    printTraceFunction("CM", "vpiUpdateDataInput()", input->simHndl);
    printTraceData("", WORD_SIZE(input->size), input->carbonVal,
                   input->carbonDrive);

    /* Update the value in Carbon */
    updateCarbonNet(input);
  }
} /* vpiUpdateDataInput */

static void
vpiUpdateDataBid(VpiBid* bid)
{
  if (bid->in.changed) {
    /* Resolve the Carbon/Simulator drive mask before writing to
       Carbon */
    mvResolveDriveMask(bid->in.size, bid->in.carbonDrive, bid->out.carbonDrive);

    /* Update the user trace function and data (if enabled) */
    printTraceFunction("CM", "vpiUpdateDataBid()", bid->in.simHndl);
    printTraceData("", WORD_SIZE(bid->in.size), bid->in.carbonVal,
                   bid->in.carbonDrive);

    /* Update the value in Carbon */
    updateCarbonNet(&bid->in);
  }
} /* vpiUpdateDataBid */


/*
 * Callbacks
 */
static void
copyValue(VpiInput* input, s_vpi_value* value)
{
  CarbonUInt32	wordSize = WORD_SIZE(input->size);
  CarbonUInt32  vectorSize;

  vectorSize = sizeof(s_vpi_vecval) * wordSize;
  memcpy(input->value.value.vector, value->value.vector, vectorSize);
}

static void
registerCallback(PLI_INT32 reason, VpiCallbackFn fn, vpiHandle obj,
                 CarbonUInt32 delay, s_vpi_value* value, PLI_BYTE8* data)
{
  s_vpi_error_info      error_info;
  s_vpi_time            time = {vpiSimTime, 0, delay, 0};
  s_cb_data             cb_data = { reason, fn, obj, &time, value, 0, data };
  vpiHandle             cbHndl;

  cbHndl = vpi_register_cb(&cb_data);
  if (vpi_chk_error(&error_info)) {
    vpi_printf(error_info.message);
  }

  if (cbHndl != NULL) {
    vpi_free_object(cbHndl);
  }
}

static void
applyInputData(VpiInput* input)
{
  VpiCarbonModel*       carbon = input->carbon;

  /* Translate the VPI data into Carbon's deposit format */
  transValue(input);

  /* Wakeup the Carbon Model process */
  input->applyFn(input);
  if (!carbon->cbRegistered) {
    registerCallback(cbReadWriteSynch, carbon->evalCallback, NULL, 0,
                     NULL, carbon->cbData);
    carbon->cbRegistered = TRUE;
  }
}

static int
vpiInputCallback(s_cb_data* cb_data)
{
  VpiInput*             input = (VpiInput*)cb_data->user_data;
  
  /* Print an optional trace string */
  printTraceFunction("Sim", "vpiInputCallback", input->simHndl);

  /* Copy the simulator data to our private storage */
  copyValue(input, cb_data->value);

  /* Apply the data to Carbon */
  applyInputData(input);
}

static int
vpiApplyTimeDelayedData(s_cb_data* cb_data)
{
  VpiInput*             input = (VpiInput*)cb_data->user_data;

  /* Print an optional trace string */
  printTraceFunction("Sim", "vpiApplyTimeDelayedData", input->simHndl);

  /* Apply the data now */
  applyInputData(input);
  input->delayedCallbackRegistered = FALSE;
  return 0;
}


static int
vpiInputCallbackTimeDelayed(s_cb_data* cb_data)
{
  VpiInput*             input = (VpiInput*)cb_data->user_data;
  
  /* Print an optional trace string */
  printTraceFunction("Sim", "vpiInputCallbackDelayed", input->simHndl);

  /* get the value from the Simulator and translate it to the Carbon format */
  copyValue(input, cb_data->value);

  /* Schedule a wakeup at a given delay */
  if (!input->delayedCallbackRegistered) {
    registerCallback(cbAfterDelay, vpiApplyTimeDelayedData, NULL, input->delay,
                     NULL, (PLI_BYTE8*)input);
    input->delayedCallbackRegistered = TRUE;
  }

  return 0;
}

static void
vpiOutputCallback(CarbonObjectID* model, CarbonNetID* carbonHndl, 
                  void* param, CarbonUInt32* val, CarbonUInt32* drive)
{
  VpiOutput*    output = (VpiOutput*)param;

  /* Update the user trace function (if enabled) */
  printTraceFunction("CM", "vpiOutputCallback()", output->simHndl);

  /* Apply the values to Simulator */
  if (output->simDataHndl != NULL) {
    driveValue(output, val, drive);
    // Ignore drive if not a tristate
  } else if (!output->isModelTristate) {
    putValue(output, val, NULL);
  } else {
    putValue(output, val, drive);
  }

  /* Copy the drive value over (for bids) */
  memcpy(output->carbonDrive, drive, WORD_SIZE(output->size)*sizeof(CarbonUInt32));
}


/*
 * Functions to set up the VPI Inputs, Outputs, and Bids
 */
static vpiHandle
findSimSignal(vpiHandle moduleHandle, const char* name, const char* suffix)
{
  vpiHandle     hndl;
  char*         fullName;

  fullName = (char*)malloc(strlen(name) + strlen(suffix) + 1);
  sprintf(fullName, "%s%s", name, suffix);
  hndl = vpi_handle_by_name(fullName, moduleHandle);
  free(fullName);
  return hndl;
}

static CarbonNetID*
findCarbonNet(CarbonObjectID* model, const char* modelName, const char* netName)
{
  char*         netBuf;
  CarbonNetID*  netHndl;

  netBuf = (char*)malloc(strlen(modelName) + strlen(netName) + 2);
  sprintf(netBuf, "%s.%s", modelName, netName);
  netHndl = carbonFindNet(model, netBuf);
  if (netHndl == NULL) {
    vpi_printf("Error: could not find handle for net `%s' in Carbon Model\n",
               netBuf);
  }
  free(netBuf);
  return netHndl;
}

static char
setupVpiInput(VpiCarbonModel* carbon, vpiHandle moduleHndl, const char* modelName,
              const char* sigName, CarbonUInt32 bitSize, ApplyFn applyFn,
              CarbonUInt32 delay, VpiInput* input)
{
  CarbonUInt32    carbonBitSize;
  CarbonUInt32    simBitSize;
  CarbonUInt32    wordSize;
  CarbonUInt32    vectorSize;
  CarbonObjectID* model = mvGetModel(&carbon->vhm);
  VpiCallbackFn   fn;
  s_vpi_value     value;

  /* Find the Simulator and Carbon handles */
  input->simHndl = findSimSignal(moduleHndl, sigName, "");
  input->carbonHndl = findCarbonNet(model, modelName, sigName);

  /* Validate the Carbon size */
  CarbonDB* db = carbonGetDB(model);
  const CarbonDBNode* node = carbonNetGetDBNode(model, input->carbonHndl);
  carbonBitSize = carbonDBGetWidth(db, node);
  if (carbonBitSize != bitSize) {
    vpi_printf("Error: mismatched bit size for signal %s, allocated: %d, Carbon size: %d\n",
           sigName, bitSize, carbonBitSize);
    return FALSE;
  }

  /* Validate the simulator bit size */
  simBitSize = vpi_get(vpiSize, input->simHndl);
  if (simBitSize != bitSize) {
    vpi_printf("Error: mismatched bit size for signal %s, allocated: %d, Simulator size: %d\n",
           sigName, bitSize, simBitSize);
    return FALSE;
  }

  /* Allocate space for the I/O. */
  wordSize = WORD_SIZE(bitSize);
  input->size = bitSize;
  input->value.format = vpiVectorVal;
  vectorSize = wordSize * sizeof(s_vpi_vecval);
  input->value.value.vector = (s_vpi_vecval*)malloc(vectorSize);
  input->carbonVal = (CarbonUInt32*)malloc(wordSize * sizeof(CarbonUInt32));
  input->carbonDrive = (CarbonUInt32*)malloc(wordSize * sizeof(CarbonUInt32));

  /* Initialize state */
  input->changed = FALSE;
  input->carbon = carbon;

  /* Apply the current simulator value to compile-time propagate
   * constants. This must be done before the delay is applied so that
   * it happens immediately.
   */
  value.format = vpiVectorVal;
  vpi_get_value(input->simHndl, &value);
  copyValue(input, &value);
  transValue(input);
  updateCarbonNet(input);

  /* Set up the delay. We use a flag to make sure we don't register a
   * delayed callback more than once per delta cycle */
  input->delay = delay;
  input->delayedCallbackRegistered = FALSE;

  /* Set up the callback and apply function */
  input->applyFn = applyFn;
  if (input->delay == 0) {
    fn = vpiInputCallback;
  } else {
    fn = vpiInputCallbackTimeDelayed;
  }
  registerCallback(cbValueChange, fn, input->simHndl, 0, &input->value,
                   (PLI_BYTE8*)input);
  return TRUE;
}

static char
setupVpiOutput(VpiCarbonModel* carbon, vpiHandle moduleHndl, const char* modelName,
               const char* sigName, CarbonUInt32 bitSize, char drive,
               CarbonUInt32 delay, VpiOutput* output)
{
  CarbonUInt32    wordSize = WORD_SIZE(bitSize);
  char            success = TRUE;
  CarbonUInt32    carbonBitSize;
  CarbonUInt32    simBitSize;
  CarbonUInt32    vectorSize;
  CarbonObjectID* model = mvGetModel(&carbon->vhm);

  /* Find the Simulator and Carbon handles */
  output->carbonHndl = findCarbonNet(model, modelName, sigName);
  output->simHndl = findSimSignal(moduleHndl, sigName, "");
  if (drive) {
    output->simDataHndl = findSimSignal(moduleHndl, sigName, "_carbon_data");
    output->simDriveHndl = findSimSignal(moduleHndl, sigName, "_carbon_en");
  } else {
    output->simDataHndl = NULL;
    output->simDriveHndl = NULL;
  }

  /* Validate the Carbon size */
  CarbonDB* db = carbonGetDB(model);
  const CarbonDBNode* node = carbonNetGetDBNode(model, output->carbonHndl);
  carbonBitSize = carbonDBGetWidth(db, node);
  if (carbonBitSize != bitSize) {
    vpi_printf("Error: mismatched bit size for signal %s, allocated: %d, Carbon size: %d\n",
           sigName, bitSize, carbonBitSize);
    return FALSE;
  }

  /* Validate the Simulator bit size */
  simBitSize = vpi_get(vpiSize, output->simHndl);
  if (simBitSize != bitSize) {
    vpi_printf("Error: mismatched bit size for signal %s, allocated: %d, Simulator size: %d\n",
           sigName, bitSize, simBitSize);
    return FALSE;
  }

  /* Allocate space of the I/O and reset it */
  output->size = bitSize;
  output->value.format = vpiVectorVal;
  vectorSize = wordSize * sizeof(s_vpi_vecval);
  output->value.value.vector = (s_vpi_vecval*)malloc(vectorSize);
  memset(output->value.value.vector, 0, vectorSize);
  output->carbonDrive = (CarbonUInt32*)malloc(wordSize * sizeof(CarbonUInt32));

  /* Set the delay to 0 initially so that the below initialization occurs */
  output->delay = 0;

  /* Check if this is a Carbon tristate. If not, we should ignore the drive
   * value because we don't care if there is an internal driver since we are 
   * depositing anyway */
  output->isModelTristate = carbonIsTristate(output->carbonHndl);

  /* Setup the callback and initialize the Simulator value */
  if (success) {
    CarbonUInt32*     val;

    /* Register the Carbon callback */
    carbonAddNetValueDriveChangeCB(model, vpiOutputCallback, output, 
                                   output->carbonHndl);

    /* Get the current version from Carbon and apply it to Simulator */
    val = (CarbonUInt32*)malloc(sizeof(CarbonUInt32) * wordSize);
    carbonExamine(model, output->carbonHndl, val, output->carbonDrive);
    if (drive) {
      driveValue(output, val, output->carbonDrive);
    // Ignore drive if not a tristate
    } else if (!output->isModelTristate) {
      putValue(output, val, NULL);
    } else {
      putValue(output, val, output->carbonDrive);
    }
    free(val);
  }

  /* Set the actual delay */
  output->delay = delay;
  return success;
} /* setupVpiOutput */

static char
vpiSetupClockInput(VpiCarbonModel* carbon, vpiHandle moduleHndl, 
                   const char* modelName, const char* sigName, CarbonUInt32 bitSize,
                   CarbonUInt32 delay, VpiInput* input)
{
  /* Set up the inputs and apply it immediately (clocks) */
  return setupVpiInput(carbon, moduleHndl, modelName, sigName, bitSize,
                       vpiApplyClockInput, delay, input);
}

static char
vpiSetupDataInput(VpiCarbonModel* carbon, vpiHandle moduleHndl,
                  const char* modelName, const char* sigName, CarbonUInt32 bitSize,
                  CarbonUInt32 delay, VpiInput* input)
{
  /* Set up the inputs and delay the application (clocks). To update
     the Carbon model use vpiUpdateDataInput */
  return setupVpiInput(carbon, moduleHndl, modelName, sigName, bitSize,
                       vpiApplyDataInput, delay, input);
}

static char
vpiSetupOutput(VpiCarbonModel* carbon, vpiHandle moduleHndl, const char* modelName,
               const char* sigName, CarbonUInt32 bitSize, CarbonUInt32 delay, VpiOutput* output)
{
  return setupVpiOutput(carbon, moduleHndl, modelName, sigName, bitSize, 
                        FALSE, delay, output);
}

static char
vpiSetupClockBid(VpiCarbonModel* carbon, vpiHandle moduleHndl,
                 const char* modelName, const char* sigName, CarbonUInt32 bitSize,
                 char drive, CarbonUInt32 delay, VpiBid* bid)
{
  char          success = TRUE;

  /* Set up the inputs and outputs individually. The input gets
     applied immediately; It uses the vpiApplyClockBid function so
     that we mask off the bits that Carbon is driving.
  
     Note that the vpiApplyClockBid function assumes that the address
     of the input field is the same as the address of the bid so that
     we can go from a VpiInput* to the carbonDrive field of the
     VpiOutput. See vpiApplyClockBid and resolveDriveMask for
     details. */
  assert((VpiBid*)(&bid->in) == bid); /* this_assert_OK */
  success &= setupVpiInput(carbon, moduleHndl, modelName, sigName, bitSize,
                           vpiApplyClockBid, delay, &bid->in);
  success &= setupVpiOutput(carbon, moduleHndl, modelName, sigName, bitSize,
                            drive, delay, &bid->out);
  return success;
}

static char
vpiSetupDataBid(VpiCarbonModel* carbon, vpiHandle moduleHndl,
                const char* modelName, const char* sigName, CarbonUInt32 bitSize,
                char drive, CarbonUInt32 delay, VpiBid* bid)
{
  char          success = TRUE;

  /* Set up the inputs and outputs individually. The input portion is
     delayed. To update the Carbon model use vpiUpdateDataBid */
  success &= setupVpiInput(carbon, moduleHndl, modelName, sigName, bitSize,
                           vpiApplyDataInput, delay, &bid->in);
  success &= setupVpiOutput(carbon, moduleHndl, modelName, sigName, bitSize,
                            drive, delay, &bid->out);
  return success;
}


/*
 * Functions to extend the various simulator commands
 */
#ifdef VPI_MTI
#include <tcl.h>

static int
mtiCarbonCommand(ClientData param, Tcl_Interp* tcl, int argc, char **argv)
{
  int                   success;
  int                   i;
  CarbonUInt32*         result;
  CarbonUInt32          resultWords;
  CarbonUInt32          resultBits;
  static CarbonCmdArgs* cmdArgs = NULL;
  int                   status;

  /* Allocate space for the command arguments if we haven't already */
  if (cmdArgs == NULL) {
    cmdArgs = malloc(sizeof(CarbonCmdArgs));
    cmdArgs->argc = 0;
    cmdArgs->argArraySize = 0;
    cmdArgs->argArray = NULL;
  }

  /* Copy over the arguments; remove the first argument which is the
   * "carbon" string. This is not produced in all environments. */
  for (i = 1; i < argc; ++i) {
    mvAddStringArg(cmdArgs, argv[i]);
  }

  /* Call the carbon command and set the return result if succesful */ 
  success = mvCarbonCommand(cmdArgs, (MsgFn)vpi_printf, &result, &resultWords,
                            &resultBits);
  if (success) {
    char*       str;
    int         len;

    /* Convert the value to a string */
    len = (resultWords * 8) + 1;
    str = Tcl_Alloc(len);
    carbonFormatGenericArray(str, len, result, resultBits, FALSE, eCarbonHex);

    /* Set the result */
    Tcl_SetResult(tcl, str, TCL_DYNAMIC);
    status = TCL_OK;
  } else {
    status = TCL_ERROR;
  }

  /* Reset the arg array for the next call */
  mvResetArgs(cmdArgs);
  return status;
}
#endif

/* 
 * This function gets the arguments to the current system function.
 * It expects there to be exactly one string which has to be
 * parsed. It creates a duplicate of that string so that we can
 * tokenize it. It is the responsibility of the caller to free the
 * memory.
 *
 * If there were any error conditions, the function returns NULL.
 *
 * This function prints any warnings or errors and uses msgPrefix to
 * indicate the system under which the problem occurred.
 */
static char* getCommand(vpiHandle func, const char* msgPrefix)
{
  vpiHandle     argI, argH;
  char*         args = NULL;
  s_vpi_value   value;

  /* Get an iterator to walk the set of arguments. We just look at the
   * first and expect it to be a string. If anything goes wrong args
   * is set to NULL. */
  argI = vpi_iterate(vpiArgument, func);
  if (argI != NULL) {
    if ((argH = vpi_scan(argI)) != NULL) {
      /* We only support a constant string argument */
      value.format = vpiStringVal;
      vpi_get_value(argH, &value);
      if (vpi_chk_error(NULL) == 0) {
	args = strdup(value.value.str);
      } else {
	vpi_printf("%s: Unknown carbon task argument type %d cannot be retrieved as a string\n",
                   msgPrefix, vpi_get(vpiType, argH), vpiConstant);
	return 0;
      }
    }

    /* Check if any more arguments were passed */
    if (vpi_scan(argI) != NULL) {
      vpi_printf("%s: command takes one string argument; extra arguments ignored.\n",
                 msgPrefix);
    }
  }

  return args;
}

/* 
 * This function converts a string to a set of tokens. It uses strtok
 * as the basis so it modifies the input string.
 *
 * The result is an argument count (returned by the function and an
 * argv array containing the tokens. The argv array is allocated based
 * on the number of tokens found. Each token is a pointer into the
 * passed in string which is modified.
 *
 */
static void tokenizeString(char* str, CarbonCmdArgs* cmdArgs)
{
  char*         start;
  char*         token;

  /*
    Check for a null argument string, and return immediately.
    Otherwise, strtok() will think this is a continuation of a
    previous call.
  */
  if (str == NULL) {
    return;
  }

  /* tokenize using strtok */
  for (start = str; (token = strtok(start, "      ")) != NULL; start = NULL) {
    /* Insert the token */
    mvAddStringArg(cmdArgs, token);
  }
}

/*
 * Function to setup a single VPI task/function argument. This
 * involves allocating storage for the argument value. If the argument
 * is a constant string, this is done once at setup. Otherwise a
 * CarbonUInt32 array of the appropriate size is allocated.
 */
static void setupVpiArg(CarbonCmdArgs* cmdArgs, vpiHandle argH)
{
  /* Allocate a string or space for data */
  if ((vpi_get(vpiType, argH) == vpiConstant) &&
      (vpi_get(vpiConstType, argH) == vpiStringConst)) {
    char*       str;
    s_vpi_value data;

    /* It is a string constant */
    data.format = vpiStringVal;
    vpi_get_value(argH, &data);
    str = strdup(data.value.str);
    mvAddStringArg(cmdArgs, str);

  } else {
    CarbonUInt32*       value;
    PLI_INT32           bitSize;
    CarbonUInt32        wordSize;

    /* Allocate a CarbonUInt32 array for the value */
    bitSize = vpi_get(vpiSize, argH);
    wordSize = (bitSize + 31) / 32;
    value = malloc(wordSize * sizeof(CarbonUInt32));
    memset(value, 0, wordSize * sizeof(CarbonUInt32));
    mvAddDataArg(cmdArgs, wordSize, value);
  }
}

/*
 * Function to setup the VPI task/function arguments into an
 * argc/argv pair. It returns the number of arguments and stores the
 * CarbonCmdArgs in the passed in variable.
 *
 * Note that if the VpiCarbonCmd argument may specify that this
 * task/function's last argument is actually a result. So the
 * CarbonCmdArgs array is not populated with the last
 * argument. Instead the handle is stored in the VpiCarbonCmd
 * structure.
 */
static int setupVpiArgs(vpiHandle tf, VpiCarbonCmd* args, int inTask)
{
  vpiHandle     argI;
  vpiHandle     argH;
  vpiHandle     argNextH;

  /* Start the process of grabbing the arguments. We have to know when
   * we hit the last argument before processing it so that we can handle
   * the resultArg).
  */
  argI = vpi_iterate(vpiArgument, tf);
  if (argI != NULL) {
    argH = vpi_scan(argI);
  } else {
    /* No argument */
    argH = NULL;
  }

  /* Get the arguments and process them into the CarbonCmdArgs */
  while (argH != NULL) {
    /* Get the next argument so we can tell if this is the last argument */
    argNextH = vpi_scan(argI);

    /* If we are looking for a result argument and this is the last
     * then just store it for our caller. Otherwise, process the
     * argument */
    if ((argNextH == NULL) && args->hasResult) {
      args->result = argH;

    } else {
      /* Setup the argument */
      setupVpiArg(&args->cmdArgs, argH);
    }

    /* Move to the next argument */
    argH = argNextH;
  }

  /* If the task/function doesn't use the last argument as the result,
   * then use the task function handle as the result */
  if (!args->hasResult) {
    args->result = tf;
  }

  return mvCheckArgs (&args->cmdArgs, inTask, (MsgFn)vpi_printf);
} /* static void setupVpiArgs */

/*
 * Function to get a function/task argument if it isn't a constant
 * string. The data is stored in the pre-allocated information
 * allocated in the CarbonCmdArgs array.
 */
static void getVpiArgValue(CarbonCmdArg* cmdArg, vpiHandle argH)
{
  s_vpi_value         data;
  PLI_INT32           i;

  /* Get it as a data value and convert it to 2 state */
  data.format = vpiVectorVal;
  vpi_get_value(argH, &data);
  for (i = 0; i < cmdArg->size; ++i) {
    cmdArg->data.value[i] = data.value.vector[i].aval & ~data.value.vector[i].bval;
  }
} /* static char* getVpiArgValue */

/*
 * Function to update all the non-constant string data arguments. The
 * constant string arguments were allocated during setup.
 */
static void getVpiArgs(vpiHandle tf, CarbonCmdArgs* cmdArgs)
{
  vpiHandle     argI;
  vpiHandle     argH;
  int           i;

  /* Start the process of grabbing the arguments. */
  argI = vpi_iterate(vpiArgument, tf);
  if (argI != NULL) {
    argH = vpi_scan(argI);
  } else {
    /* No arguments */
    argH = NULL;
  }

  /* Get the arguments and process them into the CarbonCmdArgs */
  i = 0;
  for (i = 0; (i < cmdArgs->argc) && (argH != NULL); ++i, argH = vpi_scan(argI)) {
    /* Check if this is a data argument, they are sized */
    if (cmdArgs->argArray[i].size != 0) {
      getVpiArgValue(&cmdArgs->argArray[i], argH);
    }
  }

  /* If we popped out of the loop early, free the iterator object */
  if (argH != NULL) {
    vpi_free_object(argI);
  }
}

/*
 * Helper function to call the mvCarbonCommand and set the return result
 * to the given handle
 */
static int vpiCarbonCommandWithResult(CarbonCmdArgs* args, vpiHandle resHndl)
{
  int           success;
  CarbonUInt32* result;
  CarbonUInt32  resultWords;
  CarbonUInt32  resultBits;

  success = mvCarbonCommand(args, (MsgFn)vpi_printf, &result, &resultWords,
                            &resultBits);
  if (success) {
    s_vpi_value         value;
    
    value.format = vpiVectorVal;
    value.value.vector = malloc(resultWords * sizeof(s_vpi_vecval));
    carbonToVpi(resultWords, result, NULL, value.value.vector);
    putSimValue(resHndl, &value, 0);
    free(value.value.vector);
  } else {
    /* A failure occurred, give up */
    vpi_control(vpiStop, 0);
  }
  return success;
}

static VpiCarbonCmd* allocVpiCarbonCmd(int hasResult)
{
  VpiCarbonCmd*    args;

  args = malloc(sizeof(VpiCarbonCmd));
  args->cmdArgs.argc = 0;
  args->cmdArgs.argArraySize = 0;
  args->cmdArgs.argArray = NULL;
  args->hasResult = hasResult;
  args->result = NULL;
  return args;
}

/*
 * This version of the carbon command is a function that takes a
 * single string and chops up the arguments into the CarbonCmdArgs. It
 * then sets the result as a hex string.
 */
static int vpiCarbonCommand(PLI_BYTE8* userData)
{
  CarbonCmdArgs* cmdArgs = (CarbonCmdArgs*)userData;
  vpiHandle      func;
  char*          args;
  int            success;

  /* Get the argument. We expect one string argument */
  func = vpi_handle(vpiSysTfCall, NULL);
  args = getCommand(func, "vpiCarbonCommand");

  /* Tokenize the string in place */
  tokenizeString(args, cmdArgs);

  /* Call the carbon command and apply the result to the function */
  success = vpiCarbonCommandWithResult(cmdArgs, func);

  /* Free the allocated memory. The CarbonCmdArgs are created by
   * modifying the args string so we just have to do one free to get
   * rid of all the tokens. */
  free(args);
  mvResetArgs(cmdArgs);
  return success;
}

static PLI_INT32 vpiCarbonCommandFnSetup(PLI_BYTE8* data)
{
  VpiCarbonCmd* args;
  vpiHandle     func;

  /* Allocate space for the arguments and other data -- it does not
   * have an argument result */
  func = vpi_handle(vpiSysTfCall, NULL);
  args = allocVpiCarbonCmd(FALSE);
  vpi_put_userdata(func, args);

  /* Get the arguments and process them into an arc argv pair */
  return setupVpiArgs(func, args, FALSE);
}

/*
 * This version of the carbon command is a function that takes an
 * argument per carbon command argument. It uses getVpiArgs to update
 * the CarbonCmdArgs arguments expected by mvCarbonCommand. It then
 * sets the result as a hex string.
 */
static int vpiCarbonCommandFn(PLI_BYTE8* userData)
{
  VpiCarbonCmd*         args;
  CarbonCmdArgs*        cmdArgs;
  vpiHandle             func;
  int                   success;

  /* Get the cmd args storage */
  func = vpi_handle(vpiSysTfCall, NULL);
  args = (VpiCarbonCmd*)vpi_get_userdata(func);
  cmdArgs = &args->cmdArgs;

  /* Get the arguments and process them into an arc argv pair */
  getVpiArgs(func, cmdArgs);

  /* Call the Carbon command and set the vpi function result */
  success = vpiCarbonCommandWithResult(cmdArgs, func);
  return success;
}

static PLI_INT32 vpiCarbonCommandTaskSetup(PLI_BYTE8* data)
{
  VpiCarbonCmd* args;
  vpiHandle     task;

  /* Allocate space for the arguments and other data -- it does not
   * have an argument result */
  task = vpi_handle(vpiSysTfCall, NULL);
  args = allocVpiCarbonCmd(FALSE);
  vpi_put_userdata(task, args);

  /* Get the arguments and process them into an arc argv pair */
  return setupVpiArgs(task, args, TRUE);
}

/*
 * This version of the carbon command is a task that takes an argument
 * per carbon command argument. It uses getVpiArgs to update the
 * CarbonCmdArgs arguments expected by mvCarbonCommand. It ignores the
 * result of the carbon command.
 */
static int vpiCarbonCommandTask(PLI_BYTE8* userData)
{
  VpiCarbonCmd*         args;
  CarbonCmdArgs*        cmdArgs;
  vpiHandle             task;
  int                   success;
  CarbonUInt32*         resultArray;
  CarbonUInt32          resultArraySize;
  CarbonUInt32          resultBits;

  /* Get the cmd args storage */
  task = vpi_handle(vpiSysTfCall, NULL);
  args = (VpiCarbonCmd*)vpi_get_userdata(task);
  cmdArgs = &args->cmdArgs;

  /* Get the arguments and process them into an arc argv pair */
  getVpiArgs(task, cmdArgs);

  /* Call the Carbon command */
  success = mvCarbonCommand(cmdArgs, (MsgFn)vpi_printf, &resultArray,
                            &resultArraySize, &resultBits);
  if (!success) {
    /* A failure occurred, give up */
    vpi_control(vpiStop, 0);
  }
  return success;
}

static PLI_INT32 vpiCarbonCommandTaskResSetup(PLI_BYTE8* data)
{
  VpiCarbonCmd* args;
  vpiHandle     task;

  /* Allocate space for the arguments and other data -- it has
   * an argument result */
  task = vpi_handle(vpiSysTfCall, NULL);
  args = allocVpiCarbonCmd(TRUE);
  vpi_put_userdata(task, args);

  /* Get the arguments and process them into an arc argv pair */
  return setupVpiArgs(task, args, FALSE);
}

/*
 * This version of the carbon command is a task that takes an argument
 * per carbon command argument plus an extra argument to store the
 * result. It uses getVpiArgs to update the CarbonCmdArgs arguments
 * expected by mvCarbonCommand. It stores the result in the last
 * argument.
 */
static int vpiCarbonCommandTaskRes(PLI_BYTE8* userData)
{
  VpiCarbonCmd*         args;
  CarbonCmdArgs*        cmdArgs;
  vpiHandle             task;
  int                   success;

  /* Get the cmd args storage */
  task = vpi_handle(vpiSysTfCall, NULL);
  args = (VpiCarbonCmd*)vpi_get_userdata(task);
  cmdArgs = &args->cmdArgs;

  /* Get the arguments and process them into an arc argv pair */
  getVpiArgs(task, cmdArgs);

  /* Call the Carbon command and set the result */
  success = vpiCarbonCommandWithResult(cmdArgs, args->result);
  return success;
}

static void vpiCarbonCommandInit()
{
  s_vpi_systf_data systf_data;
  vpiHandle        systf_handle;
  CarbonCmdArgs*   args;

  /*
   * Allocate and init space to keep track of Carbon command
   * arguments. Note that we share the same structure for all
   * calls. This is because we don't believe the simulation has to be
   * re-entrant. If that is incorrect, a separate allocation should be
   * made per task instance.
   */
  args = malloc(sizeof(CarbonCmdArgs));
  args->argc = 0;
  args->argArraySize = 0;
  args->argArray = NULL;

  /* Function that takes a single string that must be parsed as argument */
  systf_data.type         = vpiSysFunc;
  systf_data.sysfunctype  = vpiIntFunc;
  systf_data.tfname	  = "$carbon";
  systf_data.calltf	  = vpiCarbonCommand;
  systf_data.compiletf    = 0;
  systf_data.sizetf       = 0;
  systf_data.user_data    = (void*)args;
  systf_handle = vpi_register_systf(&systf_data);

  /* Function that takes individual arguments as strings or data */
  systf_data.tfname       = "$carbon_fn";
  systf_data.calltf       = vpiCarbonCommandFn;
  systf_data.compiletf    = vpiCarbonCommandFnSetup;
  systf_data.user_data    = 0;
  systf_handle = vpi_register_systf(&systf_data);

  /* Task that takes individual arguments as strings or data */
  systf_data.type         = vpiSysTask;
  systf_data.tfname       = "$carbon_task";
  systf_data.calltf       = vpiCarbonCommandTask;
  systf_data.compiletf    = vpiCarbonCommandTaskSetup;
  systf_handle = vpi_register_systf(&systf_data);

  /* Task that takes individual arguments as strings or data */
  systf_data.type         = vpiSysTask;
  systf_data.tfname       = "$carbon_task_res";
  systf_data.calltf       = vpiCarbonCommandTaskRes;
  systf_data.compiletf    = vpiCarbonCommandTaskResSetup;
  systf_handle = vpi_register_systf(&systf_data);
}


/*
 * Functions to perform general initialization
 */
static void
vpiInit(VpiCarbonModel* carbon, const char* hierPrefix, const char* topName,
        CarbonObjectID* model)
{
  CarbonTimescale timescale;

  /* Convert the time scale for the simulator to Carbon's time scale */
  timescale = (CarbonTimescale)vpi_get(vpiTimeUnit, NULL);
  if ((timescale < e1fs) || (timescale > e100s)) {
    vpi_printf("Unknown time scale value '%d'; should be in the range -15 to 2.\n",
               timescale);
  }

  /* Init the model */
  mvInit(&carbon->vhm, hierPrefix, hierPrefix, topName, model, timescale);
}

static int
vpiEndOfCompileCallback(s_cb_data* cb_data)
{
  if (mvCountErrors(0)) {
    vpi_printf ("%d error(s) detected in PLI calls.  Exiting.\n", mvCountErrors(0));
    tf_dofinish();
  }
}

#ifdef VPI_MTI
static void
vpiMtiInit(VpiCarbonModel* carbon, const char* hierPrefix, const char* topName,
           CarbonObjectID* model)
{
  CarbonSys*    sys;
  int           numComponents;

  /*
   * Check if this is the first component, if so add the TCL command
   * below. This must be done before installing the component using
   * vpiInit which calls mvInit.
   */
  sys = carbonGetSystem(NULL);
  numComponents = carbonSystemNumComponents(sys);

  /* General initialization */
  vpiInit(carbon, hierPrefix, topName, model);

  /* Add an MTI command to access Carbon memories if this is the first
   * Carbon Model */
  if (numComponents == 0) {
    vpi_printf("Installing Carbon TCL API\n");
    mti_AddTclCommand("carbon", mtiCarbonCommand, carbon, NULL);
  }
}
#endif


/*
 * Functions to clean up the inputs, outputs, and bids
 */
static void
vpiCleanupInput(VpiInput* input)
{
  /* Trace if enabled */
  printTraceFunction("CM", "vpiCleanupInput()", input->simHndl);

  /* Delete the memory */
  free(input->carbonVal);
  free(input->carbonDrive);
}

static void
vpiCleanupOutput(VpiOutput* output)
{
  /* Trace if enabled */
  printTraceFunction("CM", "vpiCleanupOutput()", output->simHndl);

  /* Delete the memory */
  free(output->value.value.vector);
  free(output->carbonDrive);
}

static void
vpiCleanupBid(VpiBid* bid)
{
  vpiCleanupInput(&bid->in);
  vpiCleanupOutput(&bid->out);
}

