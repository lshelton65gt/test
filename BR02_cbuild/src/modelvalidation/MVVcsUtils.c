/*****************************************************************************
 Copyright (c) 2006-2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/* Include the PLI header file */
#include "vcsuser.h"

/* Include the VPI utils which is what VCS mostly uses. */
#include "carbon/MVVpiUtils.c"

/* Also include the PLI utils which VCS uses for the Carbon command */
#include "carbon/MVPliUtils.c"
