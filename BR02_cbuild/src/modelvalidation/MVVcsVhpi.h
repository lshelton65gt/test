/*****************************************************************************
 Copyright (c) 2006-2008 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/* Include file with VCS-specific VHPI defines to make VCS compatible with the IEEE standard */

#include "vpi_user.h"
/* IEEE std_logic values */
#define vhpiU                  0   /* uninitialized */
#define vhpiX                  1   /* unknown */
#define vhpi0                  2   /* forcing 0 */
#define vhpi1                  3   /* forcing 1 */
#define vhpiZ                  4   /* high impedance */
#define vhpiW                  5   /* weak unknown */
#define vhpiL                  6   /* weak 0 */
#define vhpiH                  7   /* weak 1 */
#define vhpiDontCare           8   /* don't care */

/* IEEE std bit values */
#define vhpibit0                0   /* bit 0 */
#define vhpibit1                1   /* bit 1 */

/* IEEE std boolean values */
#define vhpiFalse              0   /* false */
#define vhpiTrue               1   /* true */

/* access types, NULL value */
#define vhpiNullAccess         0   /* equivalent to null */
typedef void            PLI_VOID;

// These mappings allow VCS VHPI to look like IEEE VHPI
#define enumv enumval
#define enumvs enums

// Flag/type changes
#define vhpiDepositPropagate vhpiForcePropagate
#define vhpiCbLastKnownDeltaCycle vhpiCbLastDeltaCycle
#define vhpiLogicVal vhpiEnumVal
#define vhpiLogicVecVal vhpiEnumVecVal
#define numElems numscalars
#define vhpiDriverCollectionK vhpiDriverK

// Function name changes
#define vhpi_check_error vhpi_chk_error
#define vhpi_control vhpi_sim_control
#define vhpi_register_cb(_cb, _unused) vhpi_register_cb(_cb)
