/*****************************************************************************
 Copyright (c) 2006-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

/* Include file with simulator/language independent utilities */
#include <stdlib.h>
#include "carbon/MVUtils.c"
#include "vhpi_user.h"
#ifdef NCSIM
#include "cfclib.h"
#endif
#ifdef VCS
#include "carbon/MVVcsVhpi.h"

// These are the VCS-specific
#define TIME_INIT(_init)  _init
#define TIME_LOW(_time)   ((long)_time)

#else
// These are the standard IEEE methods
#define TIME_INIT(_init)  {0, _init}
#define TIME_LOW(_time)   _time.low
#endif


/*
 * Supported VHDL types
 */
typedef enum VHDLSigType_ {
  MV_VHDL_TYPE_BIT,
  MV_VHDL_TYPE_BIT_VECTOR,
  MV_VHDL_TYPE_STD_LOGIC,
  MV_VHDL_TYPE_STD_LOGIC_VECTOR,
  MV_VHDL_TYPE_INTEGER,
  MV_VHDL_TYPE_SELECTED_NAME,
  MV_VHDL_TYPE_INDEXED_NAME,
  MV_VHDL_TYPE_CHARACTER,
  MV_VHDL_TYPE_ENUM,
  MV_VHDL_TYPE_UNSUPPORTED
} MvVhdlSigType;

/*
 * Types for the structures and utility functions
 */
typedef struct VhpiInput_ VhpiInput;
typedef struct VhpiOutput_ VhpiOutput;
//typedef PLI_INT32 (*VhpiCallbackFn)(vhpiCbDataT*);
typedef void (*VhpiCallbackFn)(vhpiCbDataT*);
typedef void (*TransValFn)(VhpiInput* input);
typedef void (*ApplyFn)(VhpiInput* input);
typedef void (*PutValFn)(VhpiOutput* output, CarbonUInt32* val,
                         CarbonUInt32* drive);

/*
 * Structures to manage copying data back and forth from the two
 * models.
 */
typedef struct VhpiCarbonModel_ {
  CarbonVHM       vhm;          /* Carbon Model data */
  char            cbRegistered; /* True if cb registered since last Carbon Model call */
  VhpiCallbackFn  evalCallback; /* The function to execute the Carbon Model */
  PLI_BYTE8*      cbData;       /* User data to pass to the callback */
} VhpiCarbonModel;

/* Structure to manage copying data from the Simulator to the Carbon Model */
struct VhpiInput_ {
  vhpiHandleT     simHndl;      /* Simulators handle for the input net */
  CarbonNetID*    carbonHndl;   /* Carbon Model's handle to write the Carbon value */
  CarbonUInt32    size;         /* The number of bits for the signal */

  vhpiHandleT     cbHndl;       /* Handle for the value callback (to free) */
  vhpiValueT      value;        /* Storage for callback value */

  CarbonUInt32	  delay;	/* Amount of time to delay depositing the value */
  char            delayedCallbackRegistered;
                                /* True if time delay callback was registered */

  CarbonUInt32*   carbonVal;    /* Storage to write Carbon value */
  CarbonUInt32*   carbonDrive;  /* Storage to write Carbon drive bits */
  char            changed;      /* If set, the value changed in Simulator */

  TransValFn      transFn;      /* Function to translate the vhpi value into
                                 * the Carbon format */
  ApplyFn         applyFn;      /* Function to apply the Simulator value */
  VhpiCarbonModel* carbon;      /* Data to apply changes to the Carbon model */
};

/* Structure to manage copying data from the Carbon Model to the Simulator */
struct VhpiOutput_ {
  vhpiHandleT     simHndl;      /* Simulators handle for the output net */
  vhpiHandleT     simDriveHndl; /* Simulators handle for the output driver */
  CarbonNetID*    carbonHndl;   /* Carbon Model's handle to read the Carbon value */
  CarbonUInt32    size;         /* The number of bits for the signal */
  MvVhdlSigType   sigType;      /* VHDL type of signal */

  CarbonUInt32	  delay;	/* Amount of time to delay depositing the value */
  vhpiValueT      value;        /* Storage to write the Simulator value */
  CarbonUInt32*   carbonDrive;  /* Whether Carbon model is driving (for Bids) */
  char            isModelTristate; /* If this MV output is really a Carbon tristate */

  VhpiCarbonModel* carbon;      /* Data to apply changes to the Carbon model */

  PutValFn	  putFn;        /* Function to write the vhpi value */
  char		  pullVal;	/* What to set an undriven bus to if Carbon is not driving */
};

/* Structure to manage copying bidirectional data */
typedef struct VhpiBid_ {
  VhpiInput       in;           /* To manage Simulator -> Carbon Model part of a bid */
  VhpiOutput      out;          /* To manage Carbon Model -> Simulator part of a bid */
} VhpiBid;

/* Structure to manage the Carbon command data */
typedef struct VhpiCarbonCmd_ {
  CarbonCmdArgs   cmdArgs;      /* The parsed arguments */
  int             hasResult;    /* If TRUE, the last argument is a result */
  vhpiHandleT     result;       /* Keep track of result handle */
} VhpiCarbonCmd;


/*
 * Functions to print I/O traces
 */
static void
printTraceFunction(const char* typeStr, const char* function, vhpiHandleT hndl)
{
#ifdef VHPI_TRACE
  vhpiTimeT     time;
  const char*   name;

  /* Get the name and time */
  name = vhpi_get_str(vhpiNameP, hndl);
  vhpi_get_time(&time, NULL);

  vhpi_printf("Trace: %d: %-3.3s: %s for %s\n", TIME_LOW(time), typeStr, function, name);
#endif
}

static void
printTraceData(const char* typeStr, CarbonUInt32 wordSize, CarbonUInt32* val,
               CarbonUInt32* drive)
{
#ifdef VHPI_TRACE
  CarbonUInt32  i;
  vhpiTimeT     time;

  /* Get the time */
  vhpi_get_time(&time, NULL);

  for (i = 0; i < wordSize; ++i) {
    vhpi_printf("Trace: %d: %-3.3s: Value[%d] = %x", TIME_LOW(time), typeStr, i, val[i]);
    if (drive != NULL) {
      vhpi_printf(", Drive[%d] = %x", i, drive[i]);
    }
    vhpi_printf("\n");
  }
#endif
}

static void
printTraceString(const char* typeStr, const char* str)
{
#ifdef VHPI_TRACE
  vhpiTimeT    time;

  /* Get the time */
  vhpi_get_time(NULL, &time);

  vhpi_printf("Trace: %d: %-3.3s: %s\n", TIME_LOW(time), typeStr, str);
#endif
}


/*
 * Functions for reading and writing Simulator values
 */

static void transValBit(VhpiInput* input)
{
  /* Update the user trace function (if enabled) */
  printTraceFunction("Sim", "transValBit()", input->simHndl);

  /* Update the Carbon values */
  input->carbonVal[0] = input->value.value.enumv;
  input->carbonDrive[0] = 0;

  /* Update the user trace data (if enabled) */
  printTraceData("", WORD_SIZE(input->size), input->carbonVal, NULL);
}

static void
transValBitVector(VhpiInput* input)
{
  CarbonUInt32	wordSize;
  int		i;

  /* Update the user trace function (if enabled) */
  printTraceFunction("Sim", "transValBitVector()", input->simHndl);

  /* Update the return values */
  wordSize = WORD_SIZE(input->size);
  memset(input->carbonVal, 0, sizeof(CarbonUInt32) * wordSize);
  memset(input->carbonDrive, 0, sizeof(CarbonUInt32) * wordSize);
  for (i = 0; i < input->size; ++i) {
    if (input->value.value.enumvs[(input->size-1) - i] == vhpibit1) {
      input->carbonVal[i/32] |= (1 << (i%32));
    }
  }

  /* Update the user trace data (if enabled) */
  printTraceData("", wordSize, input->carbonVal, NULL);
} /* static void transValBitVector */


static void
vhpiTimeDelayedPutValue(vhpiCbDataT* cb_data)
{
  VhpiOutput*   output = (VhpiOutput*)cb_data->user_data;
  vhpiValueT*	value = &output->value;
  vhpiHandleT	simHndl = output->simHndl;
  int i;

  printTraceFunction("Sim", "TimeDelayedPutValue()", output->simHndl);
  switch (output->sigType) {
    case MV_VHDL_TYPE_BIT:
    case MV_VHDL_TYPE_STD_LOGIC:
#ifdef VHPI_TRACE
      vhpi_printf("Delayed logic value: %i\n", value->value.enumv);
#endif
      break;
    case MV_VHDL_TYPE_BIT_VECTOR:
      break;
    case MV_VHDL_TYPE_STD_LOGIC_VECTOR:
#ifdef VHPI_TRACE
      for (i = 0; i < output->size; i++) {
        vhpi_printf("Delayed logic vector value[%i]: %i\n", i, value->value.enumvs[i]);
      }
#endif
      break;
    case MV_VHDL_TYPE_INTEGER:
      break;

    case MV_VHDL_TYPE_UNSUPPORTED:
      return;
  }
  vhpi_put_value(output->simHndl, &output->value, vhpiForcePropagate);
  //  vhpi_put_value(output->simHndl, &output->value, vhpiDepositPropagate);
}
  
/* We get here only on deposits, and not drives */
static void
putSimValue(VhpiOutput* output)
{
  vhpiErrorInfoT	error_info;
  vhpiHandleT		hndl = output->simHndl;
  vhpiValueT*		value = &output->value;
  CarbonUInt32		delay = output->delay;
  vhpiTimeT		time = TIME_INIT(delay);
  vhpiCbDataT	outputDelay_cb = {vhpiCbAfterDelay, (void *)vhpiTimeDelayedPutValue, NULL, &time, NULL, (PLI_VOID*) output};


  /* Apply the new output. Delay if configured to do so */
  if (delay != 0) {
    printTraceFunction("Sim", "putSimValue(), delayed", output->simHndl);
    vhpi_register_cb (&outputDelay_cb, 0);
  } else {
    printTraceFunction("Sim", "putSimValue(), immediate", output->simHndl);
    vhpi_put_value(hndl, value, vhpiDepositPropagate);
  }
  if (vhpi_check_error(&error_info)) {
    vhpi_printf("%s\n", error_info.message);
  }
}

static void
putValBit(VhpiOutput* output, CarbonUInt32* val, CarbonUInt32* drive)
{
  /* Update the user trace function and data (if enabled) */
  printTraceFunction("Sim", "putValBit()", output->simHndl);
  printTraceData("", WORD_SIZE(output->size), val, NULL);

  /* Apply the new output to VHPI */
  output->value.value.enumv = val[0] & 1;
  putSimValue(output);
}

static void
putValBitVector(VhpiOutput* output, CarbonUInt32* val, CarbonUInt32* drive)
{
  int		i;
  CarbonUInt32	size;

  /* Update the user trace function and data (if enabled) */
  size = output->size;
  printTraceFunction("Sim", "putValBitVector()", output->simHndl);
  printTraceData("", WORD_SIZE(size), val, NULL);

  /* Convert the value from Carbon's format to VHPI format */
  for (i = 0; i < size; ++i) {
    // enum for vhpibit0 is 0 and vhpibit1 is 1, so just take the Carbon value
    output->value.value.enumvs[(size-1)-i] = (val[i/32] & (1 << (i % 32))) != 0;
  }

  /* Apply the output to VHPI */
  putSimValue(output);
} /* putValBitVector */


static char
convStdLogicToDrive(vhpiEnumT val)
{
  return ((val == vhpiZ) || (val == vhpiW) ||
          (val == vhpiL));
}

static char
convStdLogicToData(vhpiEnumT val)
{
  return (val == vhpi1) || (val == vhpiH);
}

/*
 * Functions for reading and writing std_logic type VHPI signals
 */
static void
transValStdLogic(VhpiInput* input)
{
  /* Update the user trace function (if enabled) */
  printTraceFunction("Sim", "transValStdLogic()", input->simHndl);

  /* Update the return values */
  input->carbonVal[0] = convStdLogicToData(input->value.value.enumv);
  input->carbonDrive[0] = convStdLogicToDrive(input->value.value.enumv);

  /* Update the user trace data (if enabled) */
  printTraceData("", WORD_SIZE(input->size), input->carbonVal, input->carbonDrive);
} /* transValStdLogic */

static void
transValStdLogicVector(VhpiInput* input)
{
  CarbonUInt32  wordSize;
  vhpiEnumT	simVal;
  CarbonUInt32	i;

  /* Update the user trace function (if enabled) */
  printTraceFunction("Sim", "transValStdLogicVector()", input->simHndl);

  /* Update the Carbon values */
  wordSize = WORD_SIZE(input->size);
  memset(input->carbonVal, 0, sizeof(CarbonUInt32) * wordSize);
  memset(input->carbonDrive, 0, sizeof(CarbonUInt32) * wordSize);
  for (i = 0; i < input->size; ++i) {
    simVal = input->value.value.enumvs[(input->size-1) - i];
    if (convStdLogicToData(simVal)) {
      /* Bit is driven high */
      input->carbonVal[i/32] |= (1 << (i%32));
    } else if (convStdLogicToDrive(simVal)) {
      /* Bit is weakly driven */
      input->carbonDrive[i/32] |= (1 << (i%32));
    }
  }

  /* Update the user trace (if enabled) */
  printTraceData("", wordSize, input->carbonVal, input->carbonDrive);
} /* static void transValStdLogicVector */


static char
convToStdLogic(CarbonUInt32* val, CarbonUInt32* drive, int i,
               char pullVal)
{
  char simVal;
  if (drive && (drive[i/32] & (1 << (i % 32)))) {
    simVal = pullVal;
  } else if (val[i/32] & (1 << (i % 32))) {
    simVal = vhpi1;
  } else {
    simVal = vhpi0;
  }
  return simVal;
}

static void
driveValStdLogic(VhpiOutput* output, CarbonUInt32* val, CarbonUInt32* drive)
{
  vhpiEnumT simVal;
  vhpiTimeT time = TIME_INIT(output->delay);

  /* Update the user trace function and data (if enabled) */
  printTraceFunction("Sim", "driveValStdLogic()", output->simHndl);
  printTraceData("", WORD_SIZE(output->size), val, drive);

  /* Apply the new output. */
  simVal = convToStdLogic(val, drive, 0, output->pullVal);
  output->value.value.enumv = simVal;
#ifdef VCS
  {
    // In VCS, the value pointer is a double-indirect
    vhpiValueT*   valP = &(output->value);
    vhpi_schedule_transaction(output->simDriveHndl, &valP, 1, &time, vhpiInertial, NULL);
  }
#else
  vhpi_schedule_transaction(output->simDriveHndl, &(output->value), 1, &time, vhpiInertial, NULL);
#endif
}

static void
driveValStdLogicVector(VhpiOutput* output, CarbonUInt32* val, CarbonUInt32* drive)
{
  CarbonUInt32  size;
  vhpiEnumT*    simVal;
  CarbonUInt32	i;
  vhpiTimeT	time = TIME_INIT(output->delay);

  /* Update the user trace function and data (if enabled) */
  printTraceFunction("Sim", "driveValStdLogicVector()", output->simHndl);
  printTraceData("", WORD_SIZE(output->size), val, drive);

  /* Convert the value from the Carbon format to the VHPI format */
  simVal = output->value.value.enumvs;
  size = output->size;
  for (i = 0; i < size; ++i) {
    simVal[(size-1)-i] = convToStdLogic(val, drive, i, output->pullVal);
  }

  /* Apply the new output */
#ifdef VCS
  {
    // In VCS, the value pointer is a double-indirect
    vhpiValueT*   valP = &(output->value);
    vhpi_schedule_transaction(output->simDriveHndl, &valP, 1, &time, vhpiInertial, NULL);
  }
#else
  vhpi_schedule_transaction(output->simDriveHndl, &(output->value), 1, &time, vhpiInertial, NULL);
#endif
} /* driveValStdLogicVector */

static void
depositValStdLogic(VhpiOutput* output, CarbonUInt32* val, CarbonUInt32* drive)
{
  vhpiEnumT simVal;

  /* Update the user trace function and data (if enabled) */
  printTraceFunction("Sim", "depositValStdLogic()", output->simHndl);
  printTraceData("", WORD_SIZE(output->size), val, drive);

  /* Apply the new output. Note that we don't want to deposit z's
   * to the VHDL simulation. If Carbon is driving a Z it means we
   * aren't driving it internally, but the value could still be valid
   * because the value is driven from VHDL. So depositing a Z would
   * just mess up the simulation. This is different than the
   * driveValStdLogic which should drive a Z because it is part of a
   * real bi-direct.
   *
   * So this process, only does a deposit if it is non-Z
   */
  simVal = convToStdLogic(val, drive, 0, output->pullVal);
  if (simVal != vhpiZ) {
    output->value.value.enumv = simVal;
    putSimValue(output);
  }
}

static void
depositValStdLogicVector(VhpiOutput* output, CarbonUInt32* val, CarbonUInt32* drive)
{
  CarbonUInt32  size;
  vhpiEnumT*    simVal;
  vhpiEnumT     curSimVal;
  CarbonUInt32	i;

  /* Update the user trace function and data (if enabled) */
  printTraceFunction("Sim", "depositValStdLogicVector()", output->simHndl);
  printTraceData("", WORD_SIZE(output->size), val, drive);

  /* Apply the new output. Note that we don't want to deposit z's
   * to the VHDL simulation. If Carbon is driving a Z it means we
   * aren't driving it internally, but the value could still be valid
   * because the value is driven from VHDL. So depositing a Z would
   * just mess up the simulation. This is different than the
   * driveValStdLogic which should drive a Z because it is part of a
   * real bi-direct.
   *
   * So this process, only does a deposit if it is non-Z. It does this
   * by getting the VHDL value for bits where Carbon is Z and using the
   * Carbon value where it is not Z.
   */
  simVal = output->value.value.enumvs;
  vhpi_get_value(output->simHndl, &(output->value));
  size = output->size;
  for (i = 0; i < size; ++i) {
    curSimVal = convToStdLogic(val, drive, i, output->pullVal);
    if (curSimVal != vhpiZ) {
      simVal[(size-1)-i] = curSimVal;
    }
  }

  /* Apply the new output */
  putSimValue(output);
} /* depositValStdLogicVector */

/*
 * Functions for reading and writing integer type VHPI signals
 */
static void
transValInteger(VhpiInput* input)
{
  /* Update the user trace function (if enabled) */
  printTraceFunction("Sim", "transValInteger()", input->simHndl);

  /* Trans the value from Vhpi and write it to the Carbon values */
  input->carbonVal[0] = (CarbonUInt32)input->value.value.intg;
  input->carbonDrive[0] = 0;

  /* Update the user trace data (if enabled) */
  printTraceData("", 1, input->carbonVal, NULL);
}

static void
transValEnum(VhpiInput* input)
{
  /* Update the user trace function (if enabled) */
  printTraceFunction("Sim", "transValEnum()", input->simHndl);

  /* Trans the value from Vhpi and write it to the Carbon values */
  input->carbonVal[0] = (CarbonUInt32)input->value.value.enumv;
  input->carbonDrive[0] = 0;

  /* Update the user trace data (if enabled) */
  printTraceData("", 1, input->carbonVal, NULL);
}

static void
putValInteger(VhpiOutput* output, CarbonUInt32* val, CarbonUInt32* drive)
{
  /* Update the user trace function and data (if enabled) */
  printTraceFunction("Sim", "putValInteger()", output->simHndl);
  printTraceData("", 1, val, NULL);

  /* Apply the new output to VHPI - no translation needed for integers */
  output->value.value.intg = val[0];
  putSimValue(output);
}

static void
putValEnum(VhpiOutput* output, CarbonUInt32* val, CarbonUInt32* drive)
{
  /* Update the user trace function and data (if enabled) */
  printTraceFunction("Sim", "putValEnum()", output->simHndl);
  printTraceData("", 1, val, NULL);

  /* Apply the new output to VHPI - no translation needed for integers */
  output->value.value.enumv = val[0];
  putSimValue(output);
}



/*
 * Functions to write Carbon nets (delayed data writes)
 */
static void
updateCarbonNet(VhpiInput* input)
{
  carbonDepositFast(input->carbon->vhm.model, input->carbonHndl, input->carbonVal,
                    input->carbonDrive);
  input->changed = FALSE;
}

static void
vhpiApplyClockInput(VhpiInput* input)
{
  /* Update the user trace function and data (if enabled) */
  printTraceFunction("CM", "vhpiApplyClockInput()", input->simHndl); /* "CM" for "Carbon Model" */
  printTraceData("", WORD_SIZE(input->size), input->carbonVal, input->carbonDrive);

  /* Update the Carbon value immediately and set the clock changed flag */
  updateCarbonNet(input);
  mvSetClockChanged(&input->carbon->vhm);
}

static void
vhpiApplyDataInput(VhpiInput* input)
{
  /* Update the user trace function (if enabled) */
  printTraceFunction("CM", "vhpiApplyDataInput() [delayed]", input->simHndl);

  /* Only set the changed flags, it gets applied during the update pass */
  input->changed = TRUE;
  mvSetDataChanged(&input->carbon->vhm);
}

static void
vhpiApplyClockBid(VhpiInput* input)
{
  /* We count on the fact that the VhpiInput part is at the beginning
     of a VhpiBid. */
  VhpiBid*	bid = (VhpiBid*)input;

  /* Resolve the Carbon/Simulator drive mask before writing to
     Carbon */
  mvResolveDriveMask(bid->in.size, bid->in.carbonDrive, bid->out.carbonDrive);

  /* Now we can apply the input */
  vhpiApplyClockInput(&bid->in);
}

static void
vhpiUpdateDataInput(VhpiInput* input)
{
  if (input->changed) {
    /* Update the user trace function and data (if enabled) */
    printTraceFunction("CM", "vhpiUpdateDataInput()", input->simHndl);
    printTraceData("", WORD_SIZE(input->size), input->carbonVal,
                   input->carbonDrive);

    /* Update the value in Carbon */
    updateCarbonNet(input);
  }
} /* vhpiUpdateDataInput */

static void
vhpiUpdateDataBid(VhpiBid* bid)
{
  if (bid->in.changed) {
    /* Resolve the Carbon/Simulator drive mask before writing to
       Carbon */
    mvResolveDriveMask(bid->in.size, bid->in.carbonDrive, bid->out.carbonDrive);

    /* Update the user trace function and data (if enabled) */
    printTraceFunction("CM", "vhpiUpdateDataBid()", bid->in.simHndl);
    printTraceData("", WORD_SIZE(bid->in.size), bid->in.carbonVal,
                   bid->in.carbonDrive);

    /* Update the value in Carbon */
    updateCarbonNet(&bid->in);
  }
} /* vhpiUpdateDataBid */


/*
 * Callbacks
 */
static void
copyValue(VhpiInput* input, vhpiValueT* value)
{
  //  CarbonUInt32	wordSize = WORD_SIZE(input->size);
  CarbonUInt32  enumSize;

  enumSize = sizeof(vhpiEnumT) * input->size;

  // Two cases: pointer to buffers, and direct value
  //  if (input->value.format > vhpiObjTypeVal) {
  if (input->size > 1) {
    memcpy(input->value.value.enumvs, value->value.enumvs, enumSize);
  }
  else {
    input->value.value.enumv = value->value.enumv;
  }
}

static void
registerCallback(PLI_INT32 reason, VhpiCallbackFn fn, vhpiHandleT obj,
                 CarbonUInt32 delay, vhpiValueT* value, PLI_BYTE8* data)
{
  vhpiErrorInfoT        error_info;
  vhpiTimeT             time = TIME_INIT(delay);
  vhpiCbDataT		cb_data = { reason, (void *)fn, obj, &time, value, data };
  //  vhpiHandleT           cbHndl;

  //cbHndl = vhpi_register_cb(&cb_data, 0);
  vhpi_register_cb(&cb_data, 0);

  if (vhpi_check_error(&error_info)) {
    vhpi_printf(error_info.message);
  }

  //  if (cbHndl != NULL) {
  //    vhpi_free_object(cbHndl);
  //  }
}

static void
applyInputData(VhpiInput* input)
{
  VhpiCarbonModel*       carbon = input->carbon;

  /* Translate the VHPI data into Carbon's deposit format */
  //  transValue(input);
  input->transFn(input);

  /* Wakeup the Carbon Model process */
  input->applyFn(input);

  if (!carbon->cbRegistered) {
    //    registerCallback(cbReadWriteSynch, carbon->evalCallback, NULL, 0,
    registerCallback(vhpiCbLastKnownDeltaCycle, carbon->evalCallback, NULL, 0,
                     NULL, carbon->cbData);
    carbon->cbRegistered = TRUE;
  }
}

static void
vhpiInputCallback(vhpiCbDataT* cb_data)
{
  VhpiInput*            input = (VhpiInput*)cb_data->user_data;
  
  /* Print an optional trace string */
  printTraceFunction("Sim", "vhpiInputCallback", input->simHndl);

  /* Copy the simulator data to our private storage */
  copyValue(input, cb_data->value);

  /* Apply the data to Carbon */
  applyInputData(input);

}

static void
vhpiApplyTimeDelayedData(vhpiCbDataT* cb_data)
{
  VhpiInput*		input = (VhpiInput*)cb_data->user_data;

  /* Print an optional trace string */
  printTraceFunction("Sim", "vhpiApplyTimeDelayedData", input->simHndl);

  /* Apply the data now */
  applyInputData(input);
  input->delayedCallbackRegistered = FALSE;
}


static void
vhpiInputCallbackTimeDelayed(vhpiCbDataT* cb_data)
{
  VhpiInput*		input = (VhpiInput*)cb_data->user_data;
  
  /* Print an optional trace string */
  printTraceFunction("Sim", "vhpiInputCallbackDelayed", input->simHndl);

  /* get the value from the Simulator and translate it to the Carbon format */
  copyValue(input, cb_data->value);

  /* Schedule a wakeup at a given delay */
  if (!input->delayedCallbackRegistered) {
    registerCallback(vhpiCbAfterDelay, vhpiApplyTimeDelayedData, NULL, input->delay,
                     NULL, (PLI_BYTE8*)input);
    input->delayedCallbackRegistered = TRUE;
  }

}

static void
vhpiOutputCallback(CarbonObjectID* model, CarbonNetID* carbonHndl, 
                  void* param, CarbonUInt32* val, CarbonUInt32* drive)
{
  VhpiOutput*    output = (VhpiOutput*)param;

  /* Update the user trace function (if enabled) */
  printTraceFunction("CM", "vhpiOutputCallback()", output->simHndl);

  /* Apply the values to Simulator */
  output->putFn(output, val, drive);

  /* Copy the drive value over (for bids) */
  memcpy(output->carbonDrive, drive, WORD_SIZE(output->size)*sizeof(CarbonUInt32));
}


/*
 * Functions to set up the VHPI Inputs, Outputs, and Bids
 */
static vhpiHandleT
findSimSignal(vhpiHandleT moduleHandle, const char* name, const char* suffix)
{
  vhpiHandleT   hndl;
  char*         fullName;

  fullName = (char*)malloc(strlen(name) + strlen(suffix) + 1);
  sprintf(fullName, "%s%s", name, suffix);
  hndl = vhpi_handle_by_name(fullName, moduleHandle);
  free(fullName);
  return hndl;
}

static CarbonNetID*
findCarbonNet(CarbonObjectID* model, const char* modelName, const char* netName)
{
  char*         netBuf;
  CarbonNetID*  netHndl;

  netBuf = (char*)malloc(strlen(modelName) + strlen(netName) + 2);
  sprintf(netBuf, "%s.%s", modelName, netName);
  netHndl = carbonFindNet(model, netBuf);
  if (netHndl == NULL) {
    vhpi_printf("Error: could not find handle for net `%s' in Carbon Model\n",
		netBuf);
  }
  free(netBuf);
  return netHndl;
}


static MvVhdlSigType
getEnumSigType(vhpiHandleT typeHndl)
{
  MvVhdlSigType	sigType = MV_VHDL_TYPE_UNSUPPORTED;
  char* typeName = vhpi_get_str(vhpiNameP, typeHndl);
  if(strcasecmp(typeName, "BIT") == 0)
    sigType = MV_VHDL_TYPE_BIT;
  else if(strcasecmp(typeName, "STD_LOGIC") == 0)
    sigType = MV_VHDL_TYPE_STD_LOGIC;
  else if(strcasecmp(typeName, "STD_ULOGIC") == 0)
    sigType = MV_VHDL_TYPE_STD_LOGIC;
  else if(strcasecmp(typeName, "CHARACTER") == 0)
    sigType = MV_VHDL_TYPE_CHARACTER;
  else if(strcasecmp(typeName, "BOOLEAN") == 0)
    sigType = MV_VHDL_TYPE_BIT;
  else
    sigType = MV_VHDL_TYPE_ENUM;

  return sigType;
}

static MvVhdlSigType
getSigTypeAndWordSizeForBaseTypes(vhpiHandleT sigHndl, CarbonUInt32* wordSize)
{
  vhpiHandleT	baseHndl;
  vhpiHandleT	elemHndl1;
  vhpiHandleT	elemHndl;
  MvVhdlSigType	sigType;
  MvVhdlSigType	subSigType;

  /* Update the user trace function (if enabled) */
  printTraceFunction("Sim", "getSigTypeBase()", sigHndl);

  baseHndl = vhpi_handle (vhpiBaseType, sigHndl);
  switch (vhpi_get(vhpiKindP, baseHndl)) {
  case vhpiArrayTypeDeclK:
    {
      // This results in a vhpiSubtypeIndicK handle
      elemHndl1 = vhpi_handle(vhpiElemSubtype, baseHndl);
      // This is the actual handle we want
      elemHndl = vhpi_handle(vhpiBaseType, elemHndl1);
      switch (vhpi_get(vhpiKindP, elemHndl))
	{

	case vhpiEnumTypeDeclK:

	  subSigType = getEnumSigType(elemHndl);
	  *wordSize = vhpi_get(vhpiSizeP, sigHndl);

	  switch(subSigType) {
          case MV_VHDL_TYPE_BIT:
            sigType = MV_VHDL_TYPE_BIT_VECTOR;
            printTraceString("", "Array bit_vector");
            break;

          case MV_VHDL_TYPE_STD_LOGIC:
            sigType = MV_VHDL_TYPE_STD_LOGIC_VECTOR;
            printTraceString("", "Array std_logic_vector");
            break;

          case MV_VHDL_TYPE_UNSUPPORTED:
          default:
            sigType = MV_VHDL_TYPE_UNSUPPORTED;
            printTraceString("", "Array unsupported");
            break;
	  }
	  break;

	default:
        sigType = MV_VHDL_TYPE_UNSUPPORTED;
        printTraceString("", "Array unsupported");

	} /* end switch on elemHdl */
    }
    break;

  case vhpiIntTypeDeclK:
    {
      sigType = MV_VHDL_TYPE_INTEGER;
      *wordSize = 1;
      printTraceString("", "Scalar integer");
    }
    break;
  case vhpiEnumTypeDeclK:
    {
      sigType = getEnumSigType(baseHndl);
      *wordSize = 1;
      switch(sigType) {
      case MV_VHDL_TYPE_BIT:
          printTraceString("", "Enum bit");
          break;

        case MV_VHDL_TYPE_STD_LOGIC:
          printTraceString("", "Enum std_logic");
          break;

        case MV_VHDL_TYPE_CHARACTER:
          printTraceString("", "Enum character");
          break;

        case MV_VHDL_TYPE_ENUM:
          printTraceString("", "Enum custom");
          break;

        case MV_VHDL_TYPE_UNSUPPORTED:
          printTraceString("", "Enum unsupported");
          break;

        default:
          assert("Found unexpected enum type" == NULL); /* this_assert_OK */
      }
      break;
    }

    break;
  default:
    /* size is unknown */
    *wordSize = 0;
    sigType = MV_VHDL_TYPE_UNSUPPORTED;
    printTraceString("", "Unsupported");
    break;

  } /* end switch on baseHdl */

  return sigType;
} /* getSigTypeAndSize */


static MvVhdlSigType
getSigTypeAndWordSize(vhpiHandleT sigHndl, CarbonUInt32* wordSize)
{
  vhpiHandleT	baseHndl;
  vhpiHandleT	elemHndl1;
  vhpiHandleT	elemHndl;
  MvVhdlSigType	sigType = MV_VHDL_TYPE_UNSUPPORTED;
  MvVhdlSigType	subSigType;


  /* Update the user trace function (if enabled) */
  printTraceFunction("Sim", "getSigType()", sigHndl);

  //  baseHndl = vhpi_handle (vhpiBaseType, sigHndl);
  switch(vhpi_get(vhpiKindP, sigHndl))
  {
    // This is either primary port or observed internal signal
    case vhpiPortDeclK:
    case vhpiSigDeclK:
      sigType = getSigTypeAndWordSizeForBaseTypes(sigHndl, wordSize);
      break;

    // I couldn't get the base type from VHPI, so I return the selected and
    // indexed types..
    case vhpiSelectedNameK:
      sigType = MV_VHDL_TYPE_SELECTED_NAME;
      printTraceString("", "Selected Name");
      *wordSize = 0; // isn't used
      break;
    case vhpiIndexedNameK:
      sigType = MV_VHDL_TYPE_INDEXED_NAME;
      printTraceString("", "Indexed Name");
      *wordSize = 0; // isn't used  
      break;

  }

  return sigType;
} /* getSigTypeAndSize */


// This function returns the type of a leaf net.
// It gets the info from Carbon db.
static MvVhdlSigType
getLeafType(CarbonObjectID* model, CarbonNetID* carbon_net)
{
  MvVhdlSigType	  sigType = MV_VHDL_TYPE_UNSUPPORTED;
  
  CarbonDB* db = carbonGetDB(model);
  const CarbonDBNode* node = carbonNetGetDBNode(model, carbon_net);
  const char* intrinsicType = carbonDBIntrinsicType(db, node);

  if(strcmp(intrinsicType, "bit") == 0)
    sigType = MV_VHDL_TYPE_BIT;
  else if (strcmp(intrinsicType, "bit_vector") == 0)
    sigType = MV_VHDL_TYPE_BIT_VECTOR;
  else if (strcmp(intrinsicType, "std_logic") == 0)
    sigType = MV_VHDL_TYPE_STD_LOGIC;
  else if (strcmp(intrinsicType, "std_logic_vector") == 0)
    sigType = MV_VHDL_TYPE_STD_LOGIC_VECTOR;
  else if (strcmp(intrinsicType, "integer") == 0)
    sigType = MV_VHDL_TYPE_INTEGER;


  return sigType;
} /* getLeafType  */


static char
setupVhpiInput(VhpiCarbonModel* carbon, vhpiHandleT moduleHndl, const char* sigName,
	       const char* modelName, const char* carbonSigName,
	       CarbonUInt32 bitSize, ApplyFn applyFn,
	       CarbonUInt32 delay, VhpiInput* input)
{
  CarbonUInt32    carbonBitSize;
  CarbonUInt32    simSize;
  CarbonUInt32    simByteSize;
  CarbonUInt32    vectorSize;
  CarbonObjectID* model = mvGetModel(&carbon->vhm);
  VhpiCallbackFn   fn;
  vhpiValueT	  value;
  CarbonUInt32	  wordSize = WORD_SIZE(bitSize);
  CarbonUInt32	  byteSize = BYTE_SIZE(bitSize);
  MvVhdlSigType	  sigType;
  char            success = TRUE;
  CarbonUInt32    simWordSize; // NOT USED?*************************

  /* Find the Simulator and Carbon handles */
  input->simHndl = findSimSignal(moduleHndl, sigName, "");
  input->carbonHndl = findCarbonNet(model, modelName, carbonSigName);

  printTraceFunction("Sim", "setupInput()", input->simHndl);

  /* Set up the trans function */
  sigType = getSigTypeAndWordSize(input->simHndl, &simWordSize);
  if(sigType == MV_VHDL_TYPE_SELECTED_NAME || sigType == MV_VHDL_TYPE_INDEXED_NAME)
  {
    // Unfortunately, the VHPI function to get the type for 
    // a selected and indexed names don't work, 
    // get it from Carbon db.
    sigType = getLeafType(model, input->carbonHndl);
  }

  switch(sigType) {
    case MV_VHDL_TYPE_BIT:
      input->transFn = transValBit;
      break;

    case MV_VHDL_TYPE_BIT_VECTOR:
      input->transFn = transValBitVector;
      break;

    case MV_VHDL_TYPE_STD_LOGIC:
      input->transFn = transValStdLogic;
      break;

    case MV_VHDL_TYPE_STD_LOGIC_VECTOR:
      input->transFn = transValStdLogicVector;
      break;

    case MV_VHDL_TYPE_INTEGER:
      input->transFn = transValInteger;
      break;

    case MV_VHDL_TYPE_CHARACTER:
      input->transFn = transValEnum;
      break;

    case MV_VHDL_TYPE_ENUM:
      input->transFn = transValEnum;
      break;

    case MV_VHDL_TYPE_UNSUPPORTED:
      vhpi_printf("Error: Unsupported VHPI type %d for signal %s\n",
                         sigType, sigName);
      success = FALSE;
      break;
  } /* switch */

  if (!success)
    return FALSE;

  /* Validate the Carbon size */
  CarbonDB* db = carbonGetDB(model);
  const CarbonDBNode* node = carbonNetGetDBNode(model, input->carbonHndl);
  carbonBitSize = carbonDBGetWidth(db, node);
  /* Use byte granularity; this takes care of 31/32-bit natural-type mismatches */
  if (BYTE_SIZE(carbonBitSize) != byteSize) {
    vhpi_printf("Error: mismatched byte size for signal %s, allocated: %d, Carbon size: %d\n",
	       sigName, byteSize, BYTE_SIZE(carbonBitSize));
    return FALSE;
  }

  /* Validate the simulator bit size, for int types, this will be 1 */
  simSize = vhpi_get(vhpiSizeP, input->simHndl);

  /* Use byte granularity; this takes care of 31/32-bit natural-type mismatches */
  /* (Don't bother testing integers) */
  if (sigType != MV_VHDL_TYPE_INTEGER && BYTE_SIZE(simSize) != byteSize) {
    vhpi_printf("Error: mismatched byte size for signal %s, allocated: %d, Simulator size: %d\n",
		sigName, byteSize, BYTE_SIZE(simSize));
    return FALSE;
  }

  // Actual storage needed
  simByteSize = simSize * sizeof(vhpiEnumT);

  /* Allocate space for the I/O. */
  input->size = simSize;
  input->value.format = vhpiObjTypeVal;

  //  vectorSize = wordSize * sizeof(s_vpi_vecval);
  if (simSize > 1) {
    input->value.value.enumvs = (vhpiEnumT*)malloc(simByteSize);
    input->value.bufSize = simByteSize;
  }
  else
    input->value.bufSize = 0;

  input->carbonVal = (CarbonUInt32*)malloc(wordSize * sizeof(CarbonUInt32));
  input->carbonDrive = (CarbonUInt32*)malloc(wordSize * sizeof(CarbonUInt32));

  /* Initialize state */
  input->changed = FALSE;
  input->carbon = carbon;

  /* Set up the callback and apply function */
  input->applyFn = applyFn;

  /* Apply the current simulator value to compile-time propagate
   * constants. This must be done before the delay is applied so that
   * it happens immediately.
   */
  input->value.format = vhpiObjTypeVal;
  vhpi_get_value(input->simHndl, &(input->value));
  applyInputData(input);
  updateCarbonNet(input);

  /* Set up the delay. We use a flag to make sure we don't register a
   * delayed callback more than once per delta cycle */
  input->delay = delay;
  input->delayedCallbackRegistered = FALSE;

  fn = vhpiInputCallback;

  if (input->delay == 0) {
    fn = vhpiInputCallback;
  } else {
    fn = vhpiInputCallbackTimeDelayed;
  }

  input->value.format = vhpiObjTypeVal;	// Generic format, resolved in the trans functions
  registerCallback(vhpiCbValueChange, fn, input->simHndl, 0, &input->value,
                   (PLI_BYTE8*)input);

  return TRUE;
}

static char
setupVhpiOutput(VhpiCarbonModel* carbon, vhpiHandleT moduleHndl, const char* sigName,
		const char* modelName, const char* carbonSigName,
		CarbonUInt32 bitSize, char drive,
		CarbonUInt32 delay, VhpiOutput* output)
{
  CarbonUInt32    wordSize = WORD_SIZE(bitSize);
  CarbonUInt32	  byteSize = BYTE_SIZE(bitSize);
  char            success = TRUE;
  MvVhdlSigType	  sigType;
  CarbonUInt32    carbonBitSize;
  CarbonUInt32    simSize;
  CarbonUInt32    simByteSize;
  char            localDrive;
  CarbonPullMode  pullMode;
  CarbonObjectID* model = mvGetModel(&carbon->vhm);
  CarbonUInt32    simWordSize; // NOT USED?*************************

  /* Find the Simulator and Carbon handles */
  output->carbonHndl = findCarbonNet(model, modelName, carbonSigName);
  output->simHndl = findSimSignal(moduleHndl, sigName, "");

  printTraceFunction("Sim", "setupOutput()", output->simHndl);

  localDrive = drive || carbonIsTristate(output->carbonHndl);
  if (localDrive) {

    vhpiHandleT proc;
    /* Values will be driven on here instead of just deposited */

    /* VCS requires a process for a dynamic driver.  NCSim doesn't, but it doesn't hurt. */
    proc = vhpi_create(vhpiProcessStmtK, moduleHndl, NULL);
    output->simDriveHndl = vhpi_create(vhpiDriverCollectionK, output->simHndl, proc);
  } else {
    output->simDriveHndl = NULL;
  }

  /* Setup the pull mode according to Carbon */
  pullMode = carbonGetPullMode(model, output->carbonHndl);
  switch (pullMode) {
    case eCarbonPullUp:
      output->pullVal = vhpiH;
      break;

    case eCarbonPullDown:
      output->pullVal = vhpiL;
      break;

    case eCarbonNoPull:
      output->pullVal = vhpiZ;
      break;
  }

  /* Validate the Carbon size */
  CarbonDB* db = carbonGetDB(model);
  const CarbonDBNode* node = carbonNetGetDBNode(model, output->carbonHndl);
  carbonBitSize = carbonDBGetWidth(db, node);
  if (BYTE_SIZE(carbonBitSize) != byteSize) {
    vhpi_printf("Error: mismatched byte size for signal %s, allocated: %d, Carbon size: %d\n",
		sigName, byteSize, BYTE_SIZE(carbonBitSize));
    return FALSE;
  }

  /* Validate the Simulator bit size */
  simSize = vhpi_get(vhpiSizeP, output->simHndl);
  simByteSize = simSize * sizeof(vhpiEnumT);

  /* Set up the VHPI get function */
  sigType = getSigTypeAndWordSize(output->simHndl, &simWordSize);
  if(sigType == MV_VHDL_TYPE_SELECTED_NAME || sigType == MV_VHDL_TYPE_INDEXED_NAME)
  {
    // Unfortunatly, the VHPI function to get the type for 
    // a selected and indexed names don't work, 
    // get it from Carbon db.
    sigType = getLeafType(model, output->carbonHndl);
  }

  output->sigType = sigType;
  switch(sigType) {
    case MV_VHDL_TYPE_BIT:
      output->putFn = putValBit;
      output->value.format = vhpiEnumVal;
      break;

    case MV_VHDL_TYPE_BIT_VECTOR:
      output->putFn = putValBitVector;
      output->value.format = vhpiEnumVecVal;
      break;

    case MV_VHDL_TYPE_STD_LOGIC:
      /* There are two versions of the put code, one that drives for
       * tristates and one that deposits for internally synced 2-state
       * nets. This only applies to signals that have the Z state
       * which is std_logic*
       */
      if (localDrive) {
        output->putFn = driveValStdLogic;
      } else {
        output->putFn = depositValStdLogic;
      }
      output->value.format = vhpiLogicVal;
      break;

  case MV_VHDL_TYPE_STD_LOGIC_VECTOR:
      /* There are two versions of the put code, one that drives for
       * bidirects and one that deposits for internally synced
       * nets. This only applies to signals that have the Z state.
       */
      if (localDrive) {
        output->putFn = driveValStdLogicVector;
      } else {
        output->putFn = depositValStdLogicVector;
      }
      output->value.format = vhpiLogicVecVal;
      break;

    case MV_VHDL_TYPE_INTEGER:
      output->putFn = putValInteger;
      output->value.format = vhpiIntVal;
      break;

    case MV_VHDL_TYPE_CHARACTER:
      output->putFn = putValEnum;
      output->value.format = vhpiCharVal;
      break;

    case MV_VHDL_TYPE_ENUM:
      output->putFn = putValEnum;
      output->value.format = vhpiEnumVal;
      break;

    case MV_VHDL_TYPE_UNSUPPORTED:
      vhpi_printf("Error: Unsupported VHPI type for signal %s\n",
                         sigName);
      success = FALSE;
      break;
  } /* switch */


  if (sigType != MV_VHDL_TYPE_INTEGER && BYTE_SIZE(simSize) != byteSize) {
    vhpi_printf("Error: mismatched byte size for signal %s, allocated: %d, Simulator size: %d\n",
		sigName, byteSize, BYTE_SIZE(simSize));
    return FALSE;
  }

  /* Allocate space of the I/O and reset it */
  output->size = simSize;
  output->value.numElems = bitSize;
  if (simSize > 1) {
    output->value.value.enumvs = (vhpiEnumT*)malloc(simByteSize);
    memset(output->value.value.enumvs, 0, simByteSize);
  }
  output->carbonDrive = (CarbonUInt32*)malloc(wordSize * sizeof(CarbonUInt32));

  /* Set the delay to 0 initially so that the below initialization occurs */
  output->delay = 0;

  /* Check if this is a Carbon tristate. If not, we should ignore the drive
   * value because we don't care if there is an internal driver since we are 
   * depositing anyway */
  output->isModelTristate = carbonIsTristate(output->carbonHndl);

  /* Setup the callback and initialize the Simulator value */
  if (success) {
    CarbonUInt32*     val;

    /* Register the Carbon callback */
    carbonAddNetValueDriveChangeCB(model, vhpiOutputCallback, output, 
                                   output->carbonHndl);

    /* Get the current version from Carbon and apply it to Simulator */
    val = (CarbonUInt32*)malloc(sizeof(CarbonUInt32) * wordSize);
    carbonExamine(model, output->carbonHndl, val, output->carbonDrive);
    output->putFn(output, val, output->carbonDrive);
    free(val);
  }

  /* Set the actual delay */
  output->delay = delay;
  return success;
} /* setupVhpiOutput */

static char
vhpiSetupClockInput(VhpiCarbonModel* carbon, vhpiHandleT moduleHndl, 
		    const char* sigName, const char* modelName, 
		    const char* carbonSigName, CarbonUInt32 bitSize,
		    CarbonUInt32 delay, VhpiInput* input)
{
  /* Set up the inputs and apply it immediately (clocks) */
  return setupVhpiInput(carbon, moduleHndl, sigName, modelName,
			carbonSigName, bitSize,
			vhpiApplyClockInput, delay, input);
}

static char
vhpiSetupDataInput(VhpiCarbonModel* carbon, vhpiHandleT moduleHndl,
		   const char* sigName, const char* modelName,
		   const char* carbonSigName,
		   CarbonUInt32 bitSize,
		   CarbonUInt32 delay, VhpiInput* input)
{
  /* Set up the inputs and delay the application (clocks). To update
     the Carbon model use vhpiUpdateDataInput */
  return setupVhpiInput(carbon, moduleHndl, sigName, modelName,
			carbonSigName, bitSize, vhpiApplyDataInput,
			delay, input);
}

static char
vhpiSetupOutput(VhpiCarbonModel* carbon, vhpiHandleT moduleHndl, const char* sigName,
		const char* modelName, const char* carbonSigName,
		CarbonUInt32 bitSize, char drive, CarbonUInt32 delay,
		VhpiOutput* output)
{
  return setupVhpiOutput(carbon, moduleHndl, sigName, modelName,
			 carbonSigName, bitSize, drive, delay, output);
}

static char
vhpiSetupClockBid(VhpiCarbonModel* carbon, vhpiHandleT moduleHndl,
		  const char* sigName, const char* modelName,
		  const char* carbonSigName, CarbonUInt32 bitSize,
		  char drive, CarbonUInt32 delay, VhpiBid* bid)
{
  char          success = TRUE;

  /* Set up the inputs and outputs individually. The input gets
     applied immediately; It uses the vhpiApplyClockBid function so
     that we mask off the bits that Carbon is driving.
  
     Note that the vhpiApplyClockBid function assumes that the address
     of the input field is the same as the address of the bid so that
     we can go from a VhpiInput* to the carbonDrive field of the
     VhpiOutput. See vhpiApplyClockBid and resolveDriveMask for
     details. */
  assert((VhpiBid*)(&bid->in) == bid); /* this_assert_OK */
  success &= setupVhpiInput(carbon, moduleHndl, sigName, modelName,
			    carbonSigName, bitSize, vhpiApplyClockBid,
			    delay, &bid->in);
  success &= setupVhpiOutput(carbon, moduleHndl, sigName, modelName,
			     carbonSigName, bitSize, drive, delay, &bid->out);
  return success;
}

static char
vhpiSetupDataBid(VhpiCarbonModel* carbon, vhpiHandleT moduleHndl,
		 const char* sigName, const char* modelName,
		 const char* carbonSigName, CarbonUInt32 bitSize,
		 char drive, CarbonUInt32 delay, VhpiBid* bid)
{
  char          success = TRUE;

  /* Set up the inputs and outputs individually. The input portion is
     delayed. To update the Carbon model use vhpiUpdateDataBid */
  success &= setupVhpiInput(carbon, moduleHndl, sigName, modelName,
			    carbonSigName, bitSize, vhpiApplyDataInput,
			    delay, &bid->in);
  success &= setupVhpiOutput(carbon, moduleHndl, sigName, modelName,
			     carbonSigName, bitSize, drive, delay, &bid->out);
  return success;
}


/*
 * Functions to extend the various simulator commands
 */

#ifdef NCSIM
void carbonCommand(char** args, char isRes)
{
  int           success;
  int           i;
  CarbonUInt32* result;
  CarbonUInt32  resultWords;
  CarbonUInt32  resultBits;
  static CarbonCmdArgs* cmdArgs = NULL;

  /* Allocate space for the command arguments if we haven't already */
  if (cmdArgs == NULL) {
    cmdArgs = malloc(sizeof(CarbonCmdArgs));
    cmdArgs->argc = 0;
    cmdArgs->argArraySize = 0;
    cmdArgs->argArray = NULL;
  }

  if (isRes && !args[0])
    return;

  /* Copy over the arguments; remove the first argument which is the
   * "carbon" string. This is not produced in all environments. */

  // If the last argument is the result variable, don't parse it.
  for (i = 0; args[i+isRes]; i++) {
    mvAddStringArg(cmdArgs, args[i]);
  }

  if (mvCheckArgs (cmdArgs, !isRes, (MsgFn)vhpi_printf))
    success = mvCarbonCommand(cmdArgs, (MsgFn)vhpi_printf, &result, &resultWords,
                            &resultBits);

  if (isRes) {
    char cmd [100];
    char*       str;
    int         len;
    // This does a tcl "set var result" command to put the result in a tcl variable
    len = (resultWords * 8) + 1;
    str = (char *) malloc (len);
    carbonFormatGenericArray(str, len, result, resultBits, FALSE, eCarbonHex);

    // Set the variable to the result value using NCSim's CFC API
    vhpi_printf ("command: set %s %s", args[i], str);
    sprintf (cmd, "set %s %s", args[i], str);
    free(str);
    cfcExecuteCommand(cmd);
  }

  mvResetArgs(cmdArgs);
}

void cfcCarbonCommand(char** args)
{
  carbonCommand(args, FALSE);
}

void cfcCarbonCommandRes(char** args)
{
  carbonCommand(args, TRUE);
}


// NCSim C Function Call interface for the carbon tcl command


// (This can't be called "cfcTable", since that is the name used when CFC is
// loaded using the libcfc.so method.)

cfcTableT cfcTbl = {
  "carbon",cfcCarbonCommand,
  "carbon_res",cfcCarbonCommandRes,
  0,0
};

// The bootstrap function that muct appear on the ncsim commandline:
//  ncsim -loadcfc carbon_top.so:cfcCarbon ...
cfcTablePT cfcCarbon() /* The function name should be a global symbol */
{
  vhpi_printf ("Installing Carbon TCL command\n");
  return ((cfcTablePT) cfcTbl);
}
#endif

#ifdef VCS
/* 
 * This function gets the arguments to the current system function.
 * It expects there to be exactly one string which has to be
 * parsed. It creates a duplicate of that string so that we can
 * tokenize it. It is the responsibility of the caller to free the
 * memory.
 *
 * If there were any error conditions, the function returns NULL.
 *
 * This function prints any warnings or errors and uses msgPrefix to
 * indicate the system under which the problem occurred.
 */
static char* getCommand(vhpiHandleT func, const char* msgPrefix)
{
  vhpiHandleT   argI, argH;
  char*         args = NULL;
  vhpiValueT    value;

  /* Get an iterator to walk the set of arguments. We just look at the
   * first and expect it to be a string. If anything goes wrong args
   * is set to NULL. */
  argI = vhpi_iterator(vhpiParamDecls, func);
  if (argI != NULL) {
    if ((argH = vhpi_scan(argI)) != NULL) {
      /* We only support a constant string argument */
      value.format = vhpiStrVal;
      /* Allocate storage for the data */
      vhpiIntT bufSize = vhpi_value_size(argH, vhpiStrVal);
      value.value.str = (char*) malloc(bufSize);
      value.bufSize = bufSize;
      vhpi_get_value(argH, &value);
      bool fail = false;
      if (vhpi_chk_error(NULL) == 0) {
	args = strdup(value.value.str);
      } else {
	vhpi_printf("%s: Unknown carbon task argument type %s cannot be retrieved as a string\n",
                   msgPrefix, vhpi_get(vhpiKindStrP, argH));
        fail = true;
      }
      free(value.value.str);
      if (fail) {
        return 0;
      }
    }

    /* Check if any more arguments were passed */
    if (vhpi_scan(argI) != NULL) {
      vhpi_printf("%s: command takes one string argument; extra arguments ignored.\n",
                 msgPrefix);
    }
  }

  return args;
}

/* 
 * This function converts a string to a set of tokens. It uses strtok
 * as the basis so it modifies the input string.
 *
 * The result is an argument count (returned by the function and an
 * argv array containing the tokens. The argv array is allocated based
 * on the number of tokens found. Each token is a pointer into the
 * passed in string which is modified.
 *
 */
static void tokenizeString(char* str, CarbonCmdArgs* cmdArgs)
{
  char*         start;
  char*         token;

  /* tokenize using strtok */
  for (start = str; (token = strtok(start, "      ")) != NULL; start = NULL) {
    /* Insert the token */
    mvAddStringArg(cmdArgs, token);
  }
}
/*
 * Helper function to call the mvCarbonCommand and set the return result
 * to the given handle
 */
static int vhpiCarbonCommandWithResult(CarbonCmdArgs* args, vhpiHandleT resHndl)
{
  int           success;
  CarbonUInt32* result;
  CarbonUInt32  resultWords;
  CarbonUInt32  resultBits;
  vhpiEnumT*    simVal;
  CarbonUInt32	i;

  success = mvCarbonCommand(args, (MsgFn)vhpi_printf, &result, &resultWords,
                            &resultBits);
  if (success) {
    /* Only write back the result if a valid handle is given */
    if (resHndl != NULL) {
      vhpiValueT         value;
    
      value.format = vhpiEnumVecVal;
      value.value.enumvs = (vhpiEnumT *) malloc(resultWords * sizeof(vhpiEnumT));
      // This code was borrowed from driveValStdLogicVector()
      simVal = value.value.enumvs;
      for (i = 0; i < resultBits; ++i) {
        simVal[(resultBits-1)-i] = convToStdLogic(result, NULL, i, 0);
      }

      //carbonToVpi(resultWords, result, NULL, value.value.vector);
      //putSimValue(resHndl, &value, 0);
      vhpi_put_value(resHndl, &value, vhpiDepositPropagate);

      free(value.value.enumvs);
    }
  } else {
    /* A failure occurred, give up */
    vhpi_control(vhpiStop);
  }
  return success;
}

void carbonCommand(const struct vhpiCbDataS *data, char isRes)
{
  vhpiHandleT    func;
  char*          args;
  int            success;
  static CarbonCmdArgs* cmdArgs = NULL;

  /* Allocate space for the command arguments if we haven't already */
  if (cmdArgs == NULL) {
    cmdArgs = malloc(sizeof(CarbonCmdArgs));
    cmdArgs->argc = 0;
    cmdArgs->argArraySize = 0;
    cmdArgs->argArray = NULL;
  }

  /* Get the argument. We expect one string argument */
  func = data->obj;;
  args = getCommand(func, "carbonCommand");

  /* Tokenize the string in place */
  tokenizeString(args, cmdArgs);

  if (isRes) {
    /* The last argument is the variable to write to.  Don't let mvCarbonCommand
       see this. */
    (cmdArgs->argc)--;
  }

  /* Call the carbon command and, if necessary, apply the result to the function */
  vhpiHandleT resHndl = isRes ? func : NULL;
  success = vhpiCarbonCommandWithResult(cmdArgs, resHndl);

  /* Free the allocated memory. The CarbonCmdArgs are created by
   * modifying the args string so we just have to do one free to get
   * rid of all the tokens. */
  free(args);
  mvResetArgs(cmdArgs);
  return;
}

void vcsCarbonCommand(const struct vhpiCbDataS *data)
{
  // Without results
  carbonCommand(data, FALSE);
}

// Not yet used because there is no known way to pass VHDL data to the ucli command line
void vcsCarbonCommandRes(const struct vhpiCbDataS *data)
{
  // With results
  carbonCommand(data, TRUE);
}
#endif



/*
 * Functions to perform general initialization
 */
static void
vhpiInit(VhpiCarbonModel* carbon, const char* hierPrefix, const char* topName,
        CarbonObjectID* model)
{
  CarbonTimescale timescale;

#if NOT_INPLEMENTED
  /* Convert the time scale for the simulator to Carbon's time scale */
  timescale = (CarbonTimescale)vhpi_get(vhpiSimTimeUnitP, NULL);
  if ((timescale < e1fs) || (timescale > e100s)) {
    vhpi_printf("Unknown time scale value '%d'; should be in the range -15 to 2.\n",
               timescale);
  }
#endif
  /* Init the model */
  // Femtoseconds in NCSim
  mvInit(&carbon->vhm, hierPrefix, hierPrefix, topName, model, -15);
  //  mvInit(&carbon->vhm, hierPrefix, hierPrefix, topName, model, timescale);
}


/*
 * Functions to clean up the inputs, outputs, and bids
 */
static void
vhpiCleanupInput(VhpiInput* input)
{
  /* Trace if enabled */
  printTraceFunction("CM", "vhpiCleanupInput()", input->simHndl);

#ifdef VHPI_TRACE
  vhpi_printf("Input size is %i\n", input->size);
  vhpi_printf("Input val is %p\n", &input->value);
  vhpi_printf("Input val ptr is %p\n", input->value.value.enumvs);
#endif

  /* Delete the memory */
  if (input->size > 1)
    free(input->value.value.enumvs);
  printTraceFunction("CM", "vhpiCleanupInput_1()", input->simHndl);
  free(input->carbonVal);
  printTraceFunction("CM", "vhpiCleanupInput_2()", input->simHndl);
  free(input->carbonDrive);
}

static void
vhpiCleanupOutput(VhpiOutput* output)
{
  /* Trace if enabled */
  printTraceFunction("CM", "vhpiCleanupOutput()", output->simHndl);

  /* Delete the memory */
  if (output->size > 1)
    free(output->value.value.enumvs);
  free(output->carbonDrive);
}

static void
vhpiCleanupBid(VhpiBid* bid)
{
  vhpiCleanupInput(&bid->in);
  vhpiCleanupOutput(&bid->out);
}

