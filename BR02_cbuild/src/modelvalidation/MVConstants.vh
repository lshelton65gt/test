
`define eCarbonMsgStatus   0     /*!< status message */
`define eCarbonMsgNote     1     /*!< Note message */
`define eCarbonMsgWarning  2     /*!< Warning message */
`define eCarbonMsgError    3     /*!< Error message */
`define eCarbonMsgFatal    4     /*!< Fatal error message */
`define eCarbonMsgSuppress 5     /*!< Suppressed message */
`define eCarbonMsgAlert    6     /*!< Demotable Error message */
