/*****************************************************************************
 Copyright (c) 2006-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/

#include <assert.h>
#include <string.h>
#include <stdlib.h>

#include "veriuser.h"
#include "carbon/carbon_system.h"

/*
 * Helper macros
 */
#define WORD_SIZE(bits)	(((bits) + 31) / 32)
#define BYTE_SIZE(bits)	(((bits) + 7) / 8)
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

/*
 * Functions to execute Carbon model in two parts (Use with extreme care)
 */
extern CarbonStatus carbonClkSchedule(CarbonObjectID* model, CarbonTime time);
extern CarbonStatus carbonDataSchedule(CarbonObjectID* model, CarbonTime time);

/*
 * Function prototypes used by the command processing utility
 */
typedef void (*MsgFn)(char* format, ...);

/*
 * Structures to manage the Carbon Model
 */
typedef struct CarbonVHM_ {
  CarbonObjectID*    model;        /* Carbon model handle */
  char*              topName;      /* The top component name in the Carbon Model */
  char               clockChanged; /* If set a clock changed in this delta cycle */
  char               dataChanged;  /* If set, data pin changed in delta cycle */

  /* Variables for managing the dump file for this Carbon Model */
  char*              dumpFile;     /* name of the dump file to open */
  int                addPrefix;    /* If TRUE, add the test bench wave prefix */
  int                splitSize;    /* If 0, no split, otherwise size in Megs */
  int                maxFiles;     /* Passed to auto split function */
  CarbonWaveID*      waveFile;     /* The wave file handle */
  CarbonWaveFileType waveFileType; /* FSDB or VCD */
  CarbonTimescale    timescale;    /* The timescale of the Carbon model's parent */
  char*              hierPrefix;   /* Carbon hierarchy prefix (always uses .) */
} CarbonVHM;

typedef struct CarbonDumpOptions_ {
  char*         startName;      /* The full path to the start */
  int           levels;         /* The number of levels to dump (0 for all) */
} CarbonDumpOptions;

/*
 * Structure to manage Carbon command arguments
 */
typedef struct CarbonCmdArg_ {
  int             size;           /* 0 for strings, non-zero for data values */
  union {
    char*         str;            /* Pointer for string arguments */
    CarbonUInt32* value;          /* Pointer to an array of 32-bit values */
  } data;
} CarbonCmdArg;
typedef struct CarbonCmdArgs_ {
  int           argc;           /* The number of arguments */
  int           argArraySize;       /* The size of the allocated array */
  CarbonCmdArg* argArray;           /* An array of string arguments */
} CarbonCmdArgs;

/*
 * Structures to parse the carbon commands. These commands are used to
 * access Carbon memories from the target simulators script interface.
 *
 * *Note*: the command enum and the command table must be kept in
 *         sync.
 */
typedef enum {
  eCarbonUnknownCmd,
  eCarbonReadMemBin,     eCarbonReadMemHex,
  eCarbonDumpMemBin,     eCarbonDumpMemHex,
  eCarbonMemRead,        eCarbonMemWrite,
  eCarbonDumpfile,       eCarbonDumpvars,
  eCarbonDumpon,         eCarbonDumpoff,
  eCarbonDumpSwitch,
  eCarbonForce,          eCarbonRelease,
  eCarbonChangeMsgSeverity,
} CarbonCommandEnum ;

/*
 * This table describes the command. The fields are: the command
 * string, the enum to pick the right command, and the minimum number
 * of arguments (not including the path to model and command argument)
 */
typedef struct {
  const char*             name;
  const CarbonCommandEnum cmd;
  int                     minExpectedArgs;
  int                     maxExpectedArgs;
  int                     isMemoryAccess;
  int                     isNetAccess;
  int                     canBeTask;
} CarbonCommands;

static CarbonCommands cmdTable[] = {
  { "readmemb",   eCarbonReadMemBin, 2, 4, TRUE, FALSE, TRUE },
  { "readmemh",   eCarbonReadMemHex, 2, 4, TRUE, FALSE, TRUE },
  { "dumpmemb",   eCarbonDumpMemBin, 2, 4, TRUE, FALSE, TRUE },
  { "dumpmemh",   eCarbonDumpMemHex, 2, 4, TRUE, FALSE, TRUE },
  { "memread",    eCarbonMemRead, 2, 3, TRUE, FALSE, FALSE },
  { "memwrite",   eCarbonMemWrite, 3, 3, TRUE, FALSE, TRUE },
  { "dumpfile",   eCarbonDumpfile, 1, 6, FALSE, FALSE, TRUE },
  { "dumpvars",   eCarbonDumpvars, 0, 2, FALSE, FALSE, TRUE },
  { "dumpon",     eCarbonDumpon, 0, 0, FALSE, FALSE, TRUE },
  { "dumpoff",    eCarbonDumpoff, 0, 0, FALSE, FALSE, TRUE },
  { "dumpswitch", eCarbonDumpSwitch, 1, 1, FALSE, FALSE, TRUE },
  { "force",      eCarbonForce, 2, 4, FALSE, TRUE, TRUE },
  { "release",    eCarbonRelease, 1, 3, FALSE, TRUE, TRUE },
  { "changemsgseverity", eCarbonChangeMsgSeverity, 2, 2, FALSE, FALSE, TRUE },
};

static int cmdSize = sizeof( cmdTable ) / sizeof( cmdTable[0] ) ;

static const char *carbonUsage =
  "usage: carbon <path-to-Carbon-Model> command args\n"
  "where: \n"
  "\n"
  "  <path-to-Carbon-Model>: The full path to the Carbon Model instance in\n"
  "                          the target simulator's notation.\n"
  "\n"
  "  Command:\n"
  "    readmemb   : reads a file of binary data\n"
  "    readmemh   : reads a file of hexadecimal data\n"
  "    dumpmemb   : dumps binary data from low to high addr into file\n"
  "    dumpmemh   : dumps hexadecimal data from low to high addr into file\n"
  "    memread    : reads hexadecimal data from address\n"
  "                 -- returns data value back to TCL script\n"
  "    memwrite   : writes hexadecimal data to address\n"
  "\n"
  "    dumpfile   : sets the dump file name for this Carbon Model\n"
  "    dumpvars   : adds a set of hierarchies for dumping (relative path not\n"
  "                 supported).\n"
  "    dumpon     : enables dumping if it was disabled.\n"
  "    dumpoff    : disables dumping.\n"
  "    dumpswitch : Closes the current fsdb file and re-opens a new one.\n"
  "\n"
  "    force      : forces a signal with a given value.\n"
  "    release    : releases a forced signal.\n"
  "\n"
  "  Args:\n"
  "    readmemb   Carbon-Model-Memory-Instance filename\n"
  "    readmemh   Carbon-Model-Memory-Instance filename\n"
  "    dumpmemb   Carbon-Model-Memory-Instance filename\n"
  "    dumpmemh   Carbon-Model-Memory-Instance filename\n"
  "    memread    Carbon-Model-Memory-Instance address [word-index]\n"
  "    memwrite   Carbon-Model-Memory-Instance address data\n"
  "\n"
  "    dumpfile   filename [noprefix] [autosplit <maxsize> [maxfiles <maxfiles>]]\n"
  "    dumpvars   [levels [hierarchy]]\n"
  "    dumpon\n"
  "    dumpoff\n"
  "    dumpswitch filename\n"
  "\n"
  "    force      signal-instance value\n"
  "    release    signal-instance\n"
  "\n\n"
;

/*
 * General utility functions
 */
static void
mvResolveDriveMask(CarbonUInt32 bitSize, CarbonUInt32* inCarbonDrive,
                   CarbonUInt32* outCarbonDrive)
{
  CarbonUInt32	wordSize;
  CarbonUInt32	i;
  CarbonUInt32  bitsLeft;

  /* Get the Carbon value drive and use it to mask the MTI value. This
     is because if both MTI and Carbon are driving at the same time
     (during a transition period), we don't want to drive the
     resulting X into Carbon since it will stick. So we mask off the
     bits that Carbon is driving.

     What should happen after the Carbon model runs is it will stop
     driving and MTI will then resolve the bid correctly. */
  wordSize = WORD_SIZE(bitSize);
  bitsLeft = bitSize;
  for (i = 0; i < wordSize; ++i) {
    inCarbonDrive[i] |= ~outCarbonDrive[i];
    if (bitsLeft < 32) {
      inCarbonDrive[i] &= (0xFFFFFFFF >> (32 - bitsLeft));
      bitsLeft = 0;
    } else {
      bitsLeft -= 32;
    }
  }
}

static void
mvTestAndClearChangedFlags(CarbonVHM* vhm, char* clkChanged, char* dataChanged)
{
  /* Get the clock and data changed flags. Today we have to treat
   * clock changes as data changes because the the data schedule runs
   * the post schedule callbacks. */
  *clkChanged = vhm->clockChanged;
  *dataChanged = vhm->clockChanged || vhm->dataChanged;

  /* Clear the flags for the next delta cycle */
  vhm->clockChanged = FALSE;
  vhm->dataChanged = FALSE;
}

static void
mvSetClockChanged(CarbonVHM* vhm)
{
  vhm->clockChanged = TRUE;
}

static void
mvSetDataChanged(CarbonVHM* vhm)
{
  vhm->dataChanged = TRUE;
}

static CarbonObjectID*
mvGetModel(CarbonVHM* vhm)
{
  return vhm->model;
}

/*
 * Helper functions
 */
static void
allocArray(CarbonUInt32** data, int* dataSize, int words)
{
  if (words > (*dataSize)) {
    /* Allocate or reallocate the memory */
    if ((*data) == NULL) {
      (*data) = (CarbonUInt32*)malloc(words * sizeof(CarbonUInt32));
    } else {
      (*data) = (CarbonUInt32*)realloc((*data), words * sizeof(CarbonUInt32));
    }
    (*dataSize) = words;
   }

   /* Reset the memory so that any extra words are 0 */
   memset((*data), 0, (*dataSize) * sizeof(CarbonUInt32));
}

static int
str2data(char* str, CarbonUInt32** data, int* dataSize, int wordWidth)
{
   int          n;
   int          strLength;
   int          words;
   char*        numStr;
   CarbonStatus status;

   /* Figure out how many CarbonUInt32's we need assuming the data is in
    * hex. This means 8 digits represents one 32-bit word */
   strLength = strlen(str);
   words = (strLength + 7) / 8;
   if (wordWidth > words) {
     words = wordWidth;
   }
   
   /* Allocate space for the data if don't have enough */
   allocArray(data, dataSize, words);

   /* Check if there is a preceding 0x, If so, ignore it */
   if (strncmp("0x", str, 2) == 0) {
     numStr = &str[2];
   } else {
     numStr = str;
   }

   /* Convert the hex to data */
   status = carbonStringToArray((*data), (*dataSize), numStr, eCarbonHex);
   return(status == eCarbon_OK);
} /* str2data */

static int str2ul(char* str, int base, CarbonUInt32* result)
{
  char*         numStr;
  char*         endStr;
  int           thisBase;

   /* Check if there is a preceding 0x, If so, ignore it and override
    * the passed in base.
    */
   if (strncmp("0x", str, 2) == 0) {
     numStr = &str[2];
     thisBase = 16;
   } else {
     numStr = str;
     thisBase = base;
   }

   /* Convert the value. check for extra characters. If so, return
    * failure */
   *result = strtoul(numStr, &endStr, thisBase);
   return (*endStr == '\0');
}

static int str2ll(char* str, int base, CarbonSInt64* result)
{
  char*         numStr;
  char*         endStr;
  int           thisBase;

   /* Check if there is a preceding 0x, If so, ignore it and override
    * the passed in base.
    */
   if (strncmp("0x", str, 2) == 0) {
     numStr = &str[2];
     thisBase = 16;
   } else {
     numStr = str;
     thisBase = base;
   }

   /* Convert the value. check for extra characters. If so, return
    * failure */
   *result = strtoll(numStr, &endStr, thisBase);
   return (*endStr == '\0');
}

/* Function to get a string argument for a CarbonCmdArg. The caller
 * must provide the storage pointers. This area may be reallocated to
 * make space for the string.
 */
static char* getStringArg(CarbonCmdArg* arg, char** argStr, int* argStrSize)
{
  int           strSize;
  int           i, j;
  int           zeroFound;

  /* Determine the size of the string. Check if the argument was a
   * string to start or needs to be translated */
  if (arg->size == 0) {
    /* It is already a string, it has a know size */
    strSize = strlen(arg->data.str) + 1;

  } else {
    /* It must be a data value that represents a string, allocate space
     * for it and convert it -- count the number of non-zero
     * characters (including the zero terminator) */
    strSize = 1;
    zeroFound = FALSE;
    for (i = 0; (i < arg->size) && !zeroFound; ++i) {
      CarbonUInt32 value = arg->data.value[i];
      for (j = 0; j < 4; ++j) {
        if ((value & 0xFF) != 0) {
          ++strSize;
        } else {
          zeroFound = TRUE;
        }
        value = value >> 8;
      }
    }
  }

  /* Allocate space for the string */
  if ((*argStrSize) == 0) {
    (*argStr) = (char*)malloc(strSize);
    (*argStrSize) = strSize;
  } else if (strSize > (*argStrSize)) {
    (*argStr) = (char*)realloc((*argStr), strSize);
    (*argStrSize) = strSize;
  }

  /* Copy over the string characters */
  if (arg->size == 0) {
    strcpy((*argStr), arg->data.str);
  } else {
    (*argStr)[--strSize] = '\0';
    for (i = 0; (i < arg->size) && (strSize > 0); ++i) {
      CarbonUInt32 value = arg->data.value[i];
      for (j = 0; j < 4; ++j) {
        /* Grab the character */
        char c = (char)(value & 0xFF);
        value = value >> 8;

        /* Store it in the string array */
        (*argStr)[--strSize] = c;
      }
    }
  }
  return (*argStr);
} /* char* getStringArg */

/* Function to get a data (CarbonUInt32 array) argument for a
 * CarbonCmdArg.
 *
 * WARNING: This function currently uses a static structure to
 * allocate the resulting CarbonUInt32 array. The returned array is
 * only valid until the next getDataArg call.
 */
static int getDataArg(CarbonCmdArg* arg, int words, MsgFn msgFn,
                      CarbonUInt32** data, int* dataSize)
{
  /* If it is already a CarbonUInt32 array, then return it. Only data
   * values have a non zero size */
  if (arg->size != 0) {
    int                 i;
    int                 minSize;

    /* Allocate enough space to copy the data over */
    allocArray(data, dataSize, words);

    /* Copy over the data */
    minSize = (words > arg->size) ? arg->size : words;
    for (i = 0; i < minSize; ++i) {
      (*data)[i] = arg->data.value[i];
    }

  } else {
    /* It must be a string we have to convert, do so now */
    if (!str2data(arg->data.str, data, dataSize, words)) {
      msgFn("Error: Failed to convert string value: %s\n", arg->data.str);
      return FALSE;
    }
  }

  /* If we get here, it succeeded */
  return TRUE;
} /* int getDataArg */

static CarbonSInt64 getLongLongArg(CarbonCmdArg* arg, int base, MsgFn msgFn)
{
  CarbonSInt64 result;

  /* For a string argument use str2ll, otherwise copy the data over */
  if (arg->size == 0) {
    if (!str2ll(arg->data.str, base, &result)) {
      msgFn("Warning: extra characters in 64-bit string '%s' ignored.\n",
             arg->data.str);
      msgFn("         resulting value is %llx\n", result);
    }

  } else {
    /* Must be a data value, make sure it is 64-bits wide */
    if (arg->size == 1) {
      /* Zero extend, this is ok */
      result = (CarbonSInt64)(arg->data.value[0]);

    } else {
      /* Take the lower two words, print a warning if there is more data */
      result = (CarbonSInt64)((((CarbonUInt64)(arg->data.value[1]) << 32) |
                                ((CarbonUInt64)(arg->data.value[0]))));
      if (arg->size > 2) {
        msgFn("Warning: extra data of size %d for a 64-bit value ignored.\n",
              arg->size * 32);
        msgFn("         resulting value is %llx\n", result);
      }
    }
  }
  return result;
}

static int getIntArg(CarbonCmdArg* arg, MsgFn msgFn)
{
  int           result;

  /* For a string argument use atoi, otherwise copy over the data */
  if (arg->size == 0) {
    result = atoi(arg->data.str);
  } else {
    result = (int)arg->data.value[0];
    if (arg->size != 1) {
      msgFn("Warning: extra data of size %d for a 32-bit value ignored.\n",
            arg->size * 32);
      msgFn("         resulting value is %x\n", result);
    }
  }
  return result;
}

static CarbonUInt32 getULongArg(CarbonCmdArg* arg, MsgFn msgFn)
{
  CarbonUInt32  result;

  /* For a string argument use atoi, otherwise copy over the data */
  if (arg->size == 0) {
    if (!str2ul(arg->data.str, 10, &result)) {
      printf("Warning: extra characters in word index string '%s' ignored.\n",
             arg->data.str);
      printf("         resulting index is %d\n", result);
    }
  } else {
    result = arg->data.value[0];
    if (arg->size != 1) {
      msgFn("Warning: extra data of size %d for a 32-bit value ignored.\n",
            arg->size * 32);
      msgFn("         resulting value is %x\n", result);
    }
  }
  return result;
}

/*
 * Functions to perform general initialization
 */
static CarbonDumpOptions*
parseDumpOptions(int* numOptions, const char* topName)
{
  CarbonDumpOptions*    dumpOptions;
  char*                 options;

  /* Assume we don't find any */
  *numOptions = 0;
  dumpOptions = NULL;

  /* Look for the plus arg for the dump options */
  options = (char*)mc_scan_plusargs("CarbonDumpOptions+");
  if (options != NULL) {
    /* Count the number of options between plus's. There should be an
     * even number of tokens or an odd number of plus signs (the first
     * one is parsed in mc_scan_plusargs above. So we start count at
     * 1. */
    char*       str;
    int         count;

    for (str = options, count = 1; *str != '\0'; ++str) {
      if (*str == '+') {
        ++count;
      }
    }

    /* There should be an even number */
    if ((count > 0) && ((count & 1) == 0)) {
      int       i;
      char*     optionsCopy;
      char*     start;
      char*     tok;
      char      lookingForName;

      /* Allocate space for the options, we duplicate because strtok
       * modifies the string */
      *numOptions = (count / 2);
      dumpOptions = malloc(sizeof(CarbonDumpOptions) * (*numOptions));
      optionsCopy = strdup(options);

      /* Rewalk the data, converting the strings */
      lookingForName = TRUE;
      i = 0;
      for (start = optionsCopy; (tok = strtok(start, "+")) != NULL; start = NULL) {
        if (lookingForName) {
          dumpOptions[i].startName = strdup(tok);
          lookingForName = FALSE;
        } else {
          if (!str2ul(tok, 10, &dumpOptions[i].levels)) {
            printf("Error: invalid digit found in level '%s'; must be a number.\n",
                   tok);
            dumpOptions[i].levels = 1;
          }
          lookingForName = TRUE;
          ++i;
        }
      }

      /* all done with the string copy, the data was duplicated in the
       * options table */
      free(optionsCopy);
    } else {
      printf("Error: invalid number of tokens %d; should be an even number.\n",
             count);
      printf("       not dumping any values.\n");
    }

  } else {
    /* No options specified, use the top and level 0 dumping */
    dumpOptions = malloc(sizeof(CarbonDumpOptions));
    dumpOptions[0].startName = strdup(topName);
    dumpOptions[0].levels = 0;
    *numOptions = 1;
  }
  return dumpOptions;
}

static void
applyDumpOption(CarbonWaveID* wave, const char* startName, int levels)
{
  CarbonStatus  status;

  status = carbonDumpVars(wave, levels, startName);
  if (status != eCarbon_OK) {
    printf("Error: failed to apply dump option, start: %s, levels: %d\n",
           startName, levels);
  } else {
    printf("Dumping %s, levels = %d\n", startName, levels);
  }
}


static void
applyDumpOptions(CarbonWaveID* wave, int numOptions,
                 CarbonDumpOptions* dumpOptions)
{
  int   i;

  /* Apply the start, level options */
  for (i = 0; i < numOptions; ++i) {
    applyDumpOption(wave, dumpOptions[i].startName, dumpOptions[i].levels);
  }
}

static void
freeDumpOptions(int numOptions, CarbonDumpOptions* dumpOptions)
{
  int   i;

  /* Free the duplicated strings */
  for (i = 0; i < numOptions; ++i) {
    free(dumpOptions[i].startName);
  }

  /* Free the array */
  free(dumpOptions);
}

static int
parseDumpFile(CarbonVHM* vhm, int argc, CarbonCmdArg* argArray, MsgFn msgFn)
{
  int           result = 1; /* Assume failure */
  int           arg;
  static char*  strArg = NULL;
  static int    strArgSize = 0;
  
  getStringArg(&argArray[0], &strArg, &strArgSize);
  if (vhm->dumpFile == NULL) {
    /* Set up the dump file */
    msgFn("Setting dump file for model '%s' to '%s'\n", vhm->topName, strArg);
    vhm->dumpFile = strdup(strArg);
    result = 0;

    /* Check for the optional noprefix and, autosplit options */
    arg = 1;
    while (arg < argc) {
      /* Get the option */
      getStringArg(&argArray[arg], &strArg, &strArgSize);
      ++arg;

      if (strcmp(strArg, "noprefix") == 0) {
        /* no prefix; it has no sub args */
        vhm->addPrefix = FALSE;

      } else if (strcmp(strArg, "autosplit") == 0) {
        /* autosplit, it has a required size sub argument */
        if (arg < argc) {
          vhm->splitSize = getIntArg(&argArray[arg], msgFn);
          ++arg;
        }

        /* Make sure we got a >0 split size */
        if (vhm->splitSize <= 0) {
          msgFn("Warning: Missing or invalid split size '%d' for the autosplit option.\n",
                vhm->splitSize);

          /* Reset it to 0 so we don't do anything bad */
          vhm->splitSize = 0;
        }

      } else if (strcmp(strArg, "maxfiles") == 0) {
        /* maxfiles option which should go with the split size
         * argument.  But we test for this after parsing all
         * options.
         */
        if (arg < argc) {
          vhm->maxFiles = getIntArg(&argArray[arg], msgFn);
          ++arg;
        }

        /* Make sure we got a >0 maxfiles */
        if (vhm->maxFiles <= 0) {
          msgFn("Warning: Missing or invalid number of files '%d' for the maxfiles option.\n",
                vhm->maxFiles);

          /* Reset it to 0 so we don't do anything bad */
          vhm->maxFiles = 0;
        }

      } else {
        msgFn("Warning: unknown dumpfile option '%s' ignored.\n", strArg);
      }
    } /* while */

    /* Validate any fsdb auto split options */
    if ((vhm->maxFiles > 0) && (vhm->splitSize == 0)) {
      msgFn("Warning: non-zero maxfiles '%d' specified without a valid split size; maxfiles ignored.\n", vhm->maxFiles);
      vhm->maxFiles = 0;
    }

  } else {
    msgFn("Error: Dump file already specified '%s'; new value '%s' ignored.\n",
          vhm->dumpFile, strArg);
  }
  return result;
} /* parseDumpFile */

/* Helper function to initialize a wave file if it hasn't been already. */
static void initWaveFile(CarbonVHM* vhm, const char* hierPrefix)
{
  CarbonWaveFileType waveFileType;
  const char*        dot;

  /* Check if it was already done */
  if (vhm->waveFile != NULL) {
    return;
  }

  /* Check if a file name wasn't specified. If not use dump.vcd per
   * IEEE 1364-2001 default $dumpfile name */
  if (vhm->dumpFile == NULL) {
    vhm->dumpFile = strdup("dump.vcd");
  }

  /* Determine if this is an fsdb file or not. If it has the .fsdb
   * extension then write it as an fsdb file. Otherwise assume it is a
   * vcd file no matter what the extension. */
  dot = strrchr(vhm->dumpFile, '.');
  if (dot != NULL && (strcasecmp(dot, ".fsdb") == 0)) {
    waveFileType = eWaveFileTypeFSDB;
  } else {
    waveFileType = eWaveFileTypeVCD;
  }
  vhm->waveFileType = waveFileType;

  /* Open the wave file */
  if (waveFileType == eWaveFileTypeFSDB) {
    if (vhm->splitSize > 0) {
      vhm->waveFile = carbonWaveInitFSDBAutoSwitch(vhm->model, vhm->dumpFile,
                                                   vhm->timescale, vhm->splitSize,
                                                   vhm->maxFiles);
    } else {
      vhm->waveFile = carbonWaveInitFSDB(vhm->model, vhm->dumpFile,
                                         vhm->timescale);
    }
  } else {
    vhm->waveFile = carbonWaveInitVCD(vhm->model, vhm->dumpFile, vhm->timescale);
  }

  /* If they didn't disable it, add the hierarchy prefix */
  if (vhm->addPrefix) {
    carbonPutPrefixHierarchy(vhm->waveFile, hierPrefix, TRUE);
  }
} /* static void initWaveFile */

static void
mvInit(CarbonVHM* vhm, const char* simHierPrefix, const char* carbonHierPrefix,
       const char* topName, CarbonObjectID* model, CarbonTimescale timescale)
{
  char*                 dumpFile;
  char*                 options;
  CarbonDumpOptions*    dumpOptions;
  int                   numOptions;
  CarbonSys*            system;

  /* Set the carbon model and init the flags */
  vhm->model = model;
  vhm->timescale = timescale;
  vhm->topName = strdup(topName);
  vhm->clockChanged = FALSE;
  vhm->dataChanged = FALSE;
  vhm->dumpFile = NULL;
  vhm->addPrefix = TRUE; // By default we add the dump prefix
  vhm->waveFile = NULL;
  vhm->splitSize = 0;
  vhm->maxFiles = 0;
  vhm->hierPrefix = strdup(carbonHierPrefix);

  /* Scan the plus args for a dump options */
  dumpOptions = parseDumpOptions(&numOptions, topName);

  /* If they disabled the dump prefix, mark it as such */
  if ((char*)mc_scan_plusargs("CarbonDisableDumpPrefix") != NULL) {
    vhm->addPrefix = FALSE;
  }

  /* Scan the plus args for a VCD dump file */
  dumpFile = (char*)mc_scan_plusargs("CarbonVCDFile+");
  if (dumpFile != NULL) {
    vhm->dumpFile = strdup(dumpFile);
    initWaveFile(vhm, carbonHierPrefix);
  }

  /* Scan the plus args for a FSDB dump file */
  dumpFile = (char*)mc_scan_plusargs("CarbonFSDBFile+");
  if (dumpFile != NULL) {
    if (vhm->dumpFile == NULL) {
      vhm->dumpFile = strdup(dumpFile);
      initWaveFile(vhm, carbonHierPrefix);
    } else
      printf("Warning: only one of CarbonVCDFile or CarbonFSDBFile can be specified; CarbonFSDBFile ignored.\n");
  }

  /* Apply the scopes in the plus args */
  if (vhm->waveFile != NULL) {
    applyDumpOptions(vhm->waveFile, numOptions, dumpOptions);
  }

  /* Free the dump options memory, it has already been applied */
  if (numOptions > 0) {
    freeDumpOptions(numOptions, dumpOptions);
  }

  /* Register the model with the system so the carbon command works */
  system = carbonGetSystem(NULL);
  CarbonSC* comp = carbonSystemAddComponent(system, simHierPrefix, &vhm->model);
  carbonSystemComponentPutUserData(comp, (void*)vhm);
} /* mvInit */

/*
 * Helper function to make sure we have enough space for an argument
 */
static void allocArg(CarbonCmdArgs* cmdArgs)
{
  int   allocSize;

  /* For the first call use malloc, otherwise possibly realloc */
  if (cmdArgs->argArraySize == 0) {
    /* Assume we will have at most 10 arguments and reallocate more
     * space if we need to. */
    cmdArgs->argArraySize = 10;
    allocSize = cmdArgs->argArraySize * sizeof(CarbonCmdArg);
    cmdArgs->argArray = (CarbonCmdArg*)malloc(allocSize);

  } else {
    /* Make sure we have enough space. We know that we are going to
     * add an arg at location cmdArgs->argc. */
    while (cmdArgs->argc >= cmdArgs->argArraySize) {
      cmdArgs->argArraySize *= 2;
      allocSize = cmdArgs->argArraySize * sizeof(CarbonCmdArg);
      cmdArgs->argArray = (CarbonCmdArg*)realloc(cmdArgs->argArray, allocSize);
    }
  }
}

/*
 * Function to add a data argument and allocate/reallocate space for
 * arguments
 */
static void mvAddDataArg(CarbonCmdArgs* cmdArgs, int size, CarbonUInt32* value)
{
  /* Make sure we have enough space to add to the argument array */
  allocArg(cmdArgs);

  /* Add the argument */
  cmdArgs->argArray[cmdArgs->argc].size = size;
  cmdArgs->argArray[cmdArgs->argc].data.value = value;
  ++(cmdArgs->argc);
}

/*
 * Function to add a string argument and allocate/reallocate space for
 * arguments
 */
static void mvAddStringArg(CarbonCmdArgs* cmdArgs, char* str)
{
  /* Make sure we have enough space to add to the argument array */
  allocArg(cmdArgs);

  /* Add the argument */
  cmdArgs->argArray[cmdArgs->argc].size = 0;
  cmdArgs->argArray[cmdArgs->argc].data.str = str;
  ++(cmdArgs->argc);
}

/*
 * Function to reset the arguments back to no arguments
 */
static void mvResetArgs(CarbonCmdArgs* cmdArgs)
{
  cmdArgs->argc = 0;
}

/*
 * Count compile time errors; print a message and exit if there are some
 */
static int
mvCountErrors (int incErr)
{
  static int errCount = 0;

  if (incErr)
    errCount++;

  return errCount;
}

/*
 * Do a static compile-time check of the Carbon commands
 */ 
static int
mvCheckArgs (CarbonCmdArgs* args, int inTask, MsgFn msgFn)
{
  int                   argc = args->argc;
  CarbonCmdArg*         argArray = args->argArray;
  int                   n;
  CarbonCommandEnum     cmd = eCarbonUnknownCmd;
  int                   minExpectedArgs;
  int                   maxExpectedArgs;
  int                   isMemoryAccess;
  int                   isNetAccess;
  int                   canBeTask;
  CarbonSys*            system;
  CarbonSC*             comp;
  CarbonNetID*          net = NULL;
  CarbonMemoryID*       mem = NULL;
  CarbonObjectID*       model = NULL;
  CarbonVHM*            vhm = NULL;
  /*static*/ char*          strArg = NULL;
  /*static*/ int            strArgSize = 0;

  /* If no arguments were passed, just print the usage */
  if (argc == 0) {
    msgFn("Error: No arguments to Carbon command\n");
    msgFn("%s\n", carbonUsage);
    mvCountErrors (1);
    return 0;
  }

#if 0  // Let's do this later
  /* Find the component pointer by its registered name */
  system = carbonGetSystem(NULL);
  getStringArg(&argArray[0], &strArg, &strArgSize);
  comp = carbonSystemFindComponent(system, strArg);
  if (comp != NULL) {
    model = carbonSystemComponentGetModel(comp);
    vhm = (CarbonVHM*)carbonSystemComponentGetUserData(comp);
  } else {
    CarbonSCIter*      iter;

    msgFn("Error: Could not find model '%s' to apply the command; please verify the path to the model.\n", strArg);

    /* Loop the components and print them to be helpful */
    msgFn("The valid components are:\n");
    iter = carbonSystemLoopComponents(system);
    while ((comp = carbonSystemComponentNext(iter)) != NULL) {
      msgFn("  %s\n", carbonSystemComponentGetName(comp));
    }
    carbonSystemFreeComponentIter(iter);
    return 0;
  }
#endif

  /* If only one argument was passed in, then they didn't pass a
   * command */
  if (argc == 1) {
    msgFn("Error: Missing command.\n%s\n", carbonUsage);
    mvCountErrors (1);
    return 0;
  }

  /* Find the command */
  getStringArg(&argArray[1], &strArg, &strArgSize);
  for (n = 0; n < cmdSize; n++) {
    if(! strcmp(strArg, cmdTable[n].name)) {
      cmd = cmdTable[n].cmd;
      minExpectedArgs = cmdTable[n].minExpectedArgs;
      maxExpectedArgs = cmdTable[n].maxExpectedArgs;
      isMemoryAccess = cmdTable[n].isMemoryAccess;
      isNetAccess = cmdTable[n].isNetAccess;
      canBeTask = cmdTable[n].canBeTask;
      break;
    }
  }
  if( cmd == eCarbonUnknownCmd ) {
    msgFn("Error: Unknown command: %s\n%s\n", strArg, carbonUsage);
    mvCountErrors (1);
    return 0;
  }


  /*
   * Validate the number of arguments; first real command argument
   * starts at argArray[2]
   */
  if ((argc - 2) < minExpectedArgs) {
    msgFn("Error: Insufficient command arguments (%d) for '%s'; expecting at least %d\n",
          (argc - 2), getStringArg(&argArray[1], &strArg, &strArgSize),
          minExpectedArgs);
    msgFn("%s\n", carbonUsage);
    mvCountErrors (1);
    return 0;
  }
  if ((argc - 2) > maxExpectedArgs) {
    msgFn("Error: Too many command arguments (%d) for '%s'; expecting at most %d\n",
          (argc - 2), getStringArg(&argArray[1], &strArg, &strArgSize),
          maxExpectedArgs);
    msgFn("%s\n", carbonUsage);
    mvCountErrors (1);
    return 0;
  }

  // If this is a task, but it isn't allowed (like memread), give an error
  if (inTask & !canBeTask) {
    msgFn("Error: Command '%s' cannot be a carbon_task; it must return a value\n",
          getStringArg(&argArray[1], &strArg, &strArgSize));
    mvCountErrors (1);
    return 0;
  }

#if 0  // Do this later
  /* Memory instances must use ".", not "/" for hierRefs */
  if (isMemoryAccess) {
    getStringArg(&argArray[2], &strArg, &strArgSize);
    mem = carbonFindMemory(model, strArg);
    if(mem == NULL) {
      msgFn("Error: Invalid memory instance: %s\n%s\n", strArg, carbonUsage);
      return 0;
    }
  }

  /* Net access */
  if (isNetAccess) {
    getStringArg(&argArray[2], &strArg, &strArgSize);
    net = carbonFindNet(model, strArg);
    if (net == NULL) {
      msgFn("Error: Invalid net instance: %s\n%s\n", strArg, carbonUsage);
      return 0;
    }
  }
#endif
  return TRUE;
}

/* Function to process Carbon commands.
 *
 * The return value is a TRUE if succesful, FALSE otherwise. The
 * result data array is filled if the carbon command returns a value
 * (memread). Note that static storage is used. The value is only
 * available until the next carbon command call.
 *
 * The msgFn is a standard (printf) function to print formatted
 * message strings.
 */
static int
mvCarbonCommand(CarbonCmdArgs* args, MsgFn msgFn, CarbonUInt32** resultArray,
                CarbonUInt32* resultArraySize, CarbonUInt32* resultBitSize)
{
  int                   argc = args->argc;
  CarbonCmdArg*         argArray = args->argArray;
  int                   n;
  CarbonCommandEnum     cmd = eCarbonUnknownCmd;
  int                   minExpectedArgs;
  int                   maxExpectedArgs;
  int                   isMemoryAccess;
  int                   isNetAccess;
  CarbonNetID*          net = NULL;
  CarbonMemoryID*       mem = NULL;
  CarbonStatus          status;
  CarbonSInt64          startAddr, endAddr, addr;
  int                   success;
  int                   wordWidth;
  CarbonSys*            system;
  CarbonSC*             comp;
  CarbonObjectID*       model = NULL;
  CarbonVHM*            vhm = NULL;

  /* Static space for allocating argument/result */
  static CarbonUInt32*  result = NULL;
  static int            resultSize = 0;
  static CarbonUInt32*  data = NULL;
  static int            dataSize = 0;
  static char*          strArg = NULL;
  static int            strArgSize = 0;

  /* If no arguments were passed, just print the usage */
  if (argc == 0) {
    msgFn("%s\n", carbonUsage);
    return 0;
  }

  /* Find the component pointer by its registered name */
  system = carbonGetSystem(NULL);
  getStringArg(&argArray[0], &strArg, &strArgSize);
  comp = carbonSystemFindComponent(system, strArg);
  if (comp != NULL) {
    model = carbonSystemComponentGetModel(comp);
    vhm = (CarbonVHM*)carbonSystemComponentGetUserData(comp);
  } else {
    CarbonSCIter*      iter;

    msgFn("Error: Could not find model '%s' to apply the command; please verify the path to the model.\n", strArg);

    /* Loop the components and print them to be helpful */
    msgFn("The valid components are:\n");
    iter = carbonSystemLoopComponents(system);
    while ((comp = carbonSystemComponentNext(iter)) != NULL) {
      msgFn("  %s\n", carbonSystemComponentGetName(comp));
    }
    carbonSystemFreeComponentIter(iter);
    return 0;
  }

  /* If only one argument was passed in, then they didn't pass a
   * command */
  if (argc == 1) {
    msgFn("Error: Missing command.\n%s\n", carbonUsage);
    return 0;
  }

  /* Find the command */
  getStringArg(&argArray[1], &strArg, &strArgSize);
  for (n = 0; n < cmdSize; n++) {
    if(! strcmp(strArg, cmdTable[n].name)) {
      cmd = cmdTable[n].cmd;
      minExpectedArgs = cmdTable[n].minExpectedArgs;
      maxExpectedArgs = cmdTable[n].maxExpectedArgs;
      isMemoryAccess = cmdTable[n].isMemoryAccess;
      isNetAccess = cmdTable[n].isNetAccess;
      break;
    }
  }
  if( cmd == eCarbonUnknownCmd ) {
    msgFn("Error: Unknown command: %s\n%s\n", strArg, carbonUsage);
    return 0;
  }

  /*
   * Validate the number of arguments; first real command argument
   * starts at argArray[2]
   */
  if ((argc - 2) < minExpectedArgs) {
    msgFn("Error: Insufficient command arguments (%d) for '%s'; expecting at least %d\n",
          (argc - 2), getStringArg(&argArray[1], &strArg, &strArgSize),
          minExpectedArgs);
    msgFn("%s\n", carbonUsage);
    return 0;
  }
  if ((argc - 2) > maxExpectedArgs) {
    msgFn("Error: Too many command arguments (%d) for '%s'; expecting at most %d\n",
          (argc - 2), getStringArg(&argArray[1], &strArg, &strArgSize),
          maxExpectedArgs);
    msgFn("%s\n", carbonUsage);
    return 0;
  }

  /* Memory instances must use ".", not "/" for hierRefs */
  if (isMemoryAccess) {
    getStringArg(&argArray[2], &strArg, &strArgSize);
    mem = carbonFindMemory(model, strArg);
    if(mem == NULL) {
      msgFn("Error: Invalid memory instance: %s\n%s\n", strArg, carbonUsage);
      return 0;
    }
  }

  /* Net access */
  if (isNetAccess) {
    getStringArg(&argArray[2], &strArg, &strArgSize);
    net = carbonFindNet(model, strArg);
    if (net == NULL) {
      msgFn("Error: Invalid net instance: %s\n%s\n", strArg, carbonUsage);
      return 0;
    }
  }

  /* Put a succes result of 0 in case it works */
  allocArray(&result, &resultSize, 1);
  *resultArray = result;
  *resultArraySize = 1;
  *resultBitSize = 1;

  /* Command processing */
  success = FALSE;
  switch( cmd ) {
    case eCarbonMemWrite:
      /* Convert and deposit the data */
      wordWidth = carbonMemoryRowNumUInt32s(mem);
      if (getDataArg(&argArray[4], wordWidth, msgFn, &data, &dataSize)) {
        addr = getLongLongArg(&argArray[3], 16, msgFn);
        status = carbonDepositMemory(mem, addr, data);
        if( status != eCarbon_OK ) {
          msgFn("Error: Could not deposit: %llx\n%s\n", addr, carbonUsage);
        } else {
          success = TRUE;
        }
      }
      break;

    case eCarbonMemRead:
      /* Read the memory value as a string
       *
       * Note: that this function does not automatically add the
       *       terminating null ('\0') to the generated string. The
       *       string may be larger than the value, and may be used
       *       for other * operations in addition to retrieving a
       *       value.
       */
      addr = getLongLongArg(&argArray[3], 16, msgFn);
      if (argc == 5) {
	CarbonUInt32 wordIndex;

	/* Read only one word of it - get the decimal word index */
        wordIndex = getULongArg(&argArray[4], msgFn);

        /* read that word */
	result[0] = carbonExamineMemoryWord(mem, addr, wordIndex);
        status = eCarbon_OK;
        *resultBitSize = carbonMemoryRowWidth(mem) - (wordIndex * 32);
        if ((*resultBitSize) > 32) {
          *resultBitSize = 32;
        }

      } else {
        /* Allocate enough space to store the resulting value */
        wordWidth = carbonMemoryRowNumUInt32s(mem);
        allocArray(&result, &resultSize, wordWidth);

	/* Read the entire word */
	status = carbonExamineMemory(mem, addr, result);
        *resultArraySize = wordWidth;
        *resultBitSize = carbonMemoryRowWidth(mem);
      }
      if( status != eCarbon_OK ) {
        msgFn("Error: Could not examine: %s[%d], status = %d\n",
              getStringArg(&argArray[2], &strArg, &strArgSize), addr, status);
        msgFn("%s\n", carbonUsage);

      } else {
        /* Return the result */
        *resultArray = result;
        success = TRUE;
      }
      break;

    case eCarbonReadMemBin:
      getStringArg(&argArray[3], &strArg, &strArgSize);
      if (argc > 4) {
	startAddr = getLongLongArg(&argArray[4], 16, msgFn);
	if (argc > 5)
	  endAddr = getLongLongArg(&argArray[5], 16, msgFn);
	else
	  endAddr = carbonGetRightAddr(mem);
	status = carbonReadmembRange(mem, strArg, startAddr, endAddr);
      }
      else
	status = carbonReadmemb(mem, strArg);
      if( status != eCarbon_OK ) {
        msgFn("Error: Could not read: %s\n%s\n", strArg, carbonUsage);
      } else {
        success = TRUE;
      }
      break;

    case eCarbonReadMemHex:
      getStringArg(&argArray[3], &strArg, &strArgSize);
      if (argc > 4) {
	startAddr = getLongLongArg(&argArray[4], 16, msgFn);
	if (argc > 5)
	  endAddr = getLongLongArg(&argArray[5], 16, msgFn);
	else
	  endAddr = carbonGetRightAddr(mem);
	status = carbonReadmemhRange(mem, strArg, startAddr, endAddr);
      }
      else
        status = carbonReadmemh(mem, strArg);
      if( status != eCarbon_OK ) {
        msgFn("Error: Could not read: %s\n%s\n", strArg, carbonUsage);
      } else {
        success = TRUE;
      }
      break;

    case eCarbonDumpMemBin:
      getStringArg(&argArray[3], &strArg, &strArgSize);
      if (argc > 4) {
	startAddr = getLongLongArg(&argArray[4], 16, msgFn);
	if (argc > 5)
	  endAddr = getLongLongArg(&argArray[5], 16, msgFn);
	else
	  endAddr = carbonGetRightAddr(mem);
      }
      else {
        startAddr = carbonGetLeftAddr(mem);
        endAddr = carbonGetRightAddr(mem);
      }
      status = carbonDumpAddressRange(mem, strArg, startAddr, endAddr,
                                      eCarbonBin);
      if( status != eCarbon_OK ) {
        msgFn("Error: Could not write: %s\n%s\n", strArg, carbonUsage);
      } else {
        success = TRUE;
      }
      break;

    case eCarbonDumpMemHex:
      getStringArg(&argArray[3], &strArg, &strArgSize);
      if (argc > 4) {
	startAddr = getLongLongArg(&argArray[4], 16, msgFn);
	if (argc > 5)
	  endAddr = getLongLongArg(&argArray[5], 16, msgFn);
	else
	  endAddr = carbonGetRightAddr(mem);
      }
      else {
        startAddr = carbonGetLeftAddr(mem);
        endAddr = carbonGetRightAddr(mem);
      }
      status = carbonDumpAddressRange(mem, strArg, startAddr, endAddr,
                                      eCarbonHex);
      if( status != eCarbon_OK ) {
        msgFn("Error: Could not write: %s\n%s\n", strArg, carbonUsage);
      } else {
        success = TRUE;
      }
      break;

    case eCarbonDumpfile:
      if (parseDumpFile(vhm, argc-2, &argArray[2], msgFn) == 0) {
        success = TRUE;
      }
      break;

    case eCarbonDumpvars:
      /* Make sure we have a wave context */
      initWaveFile(vhm, vhm->hierPrefix);
      if (vhm->waveFile != NULL) {
        /* Gather the arguments, if none are given it is a level 0 dump */
        int             levels = 0;
        char*           path = NULL;
        CarbonStatus    status;

        if (argc > 2) {
          levels = getIntArg(&argArray[2], msgFn);
        }
        if (argc > 3) {
          path = getStringArg(&argArray[3], &strArg, &strArgSize);
        } else {
          path = vhm->topName;
        }

        /* Dump the requested hierarchy */
        status = carbonDumpVars(vhm->waveFile, levels, path);
        if (status == eCarbon_OK) {
          msgFn("Adding a level %d dump of scope '%s' for model '%s'\n",
                levels, path, vhm->topName);
          success = TRUE;
        } else {
          msgFn("Error: Failed to add a level %d dump of scope '%s' for model '%s'; check the validity of the provided scopes.\n",
                levels, path, vhm->topName);
        }
      }
      break;

    case eCarbonDumpon:
      if (vhm->waveFile != NULL) {
        carbonDumpOn(vhm->waveFile);
        success = TRUE;
      } else {
        msgFn("Error: Wave file not open; dumpon ignored.\n");
      }
      break;

    case eCarbonDumpoff:
      if (vhm->waveFile != NULL) {
        carbonDumpOff(vhm->waveFile);
        success = TRUE;
      } else {
        msgFn("Error: Wave file not open; dumpoff ignored.\n");
      }
      break;

    case eCarbonDumpSwitch:
      if (vhm->waveFile != NULL) {
        if (vhm->waveFileType == eWaveFileTypeFSDB) {
          getStringArg(&argArray[2], &strArg, &strArgSize);
          if (carbonWaveSwitchFSDBFile(vhm->waveFile, strArg) == eCarbon_OK) {
            success = TRUE;
          }
        } else {
          msgFn("Error: Wave Switch only supported on fsdb wave files.");
        }
      } else {
        msgFn("Error: Wave file not open; switch ignored.\n");
      }
      break;

    case eCarbonForce:
      /* Convert and force the data */
      wordWidth = carbonGetNumUInt32s(net);
      if (getDataArg(&argArray[3], wordWidth, msgFn, &data, &dataSize)) {
        if (argc > 4) {
	  int msb, lsb;
          msb = getIntArg(&argArray[4], msgFn);
	  if (argc > 5)
	    lsb = getIntArg(&argArray[5], msgFn);
	  else {
	    msgFn("Error: Missing lsb argument in force range command\n%s\n",
		  carbonUsage);
	    break;
	  }
	  status = carbonForceRange(model, net, data, msb, lsb);
	  if (status != eCarbon_OK) {
	    msgFn("Error: Could not force: %s\n%s\n", 
		  getStringArg(&argArray[2], &strArg, &strArgSize), carbonUsage);
	  } else {
	    success = TRUE;
	    break;
	  }
        }

        status = carbonForce(model, net, data);
        if (status != eCarbon_OK) {
          msgFn("Error: Could not force: %s\n%s\n", 
                getStringArg(&argArray[2], &strArg, &strArgSize), carbonUsage);
        } else {
          success = TRUE;
        }
      }
      break;

    case eCarbonRelease:
      if (argc > 3) {
	int msb, lsb;
	msb = getIntArg(&argArray[3], msgFn);
	if (argc > 4)
	  lsb = getIntArg(&argArray[4], msgFn);
	else {
	  msgFn("Error: Missing lsb argument in release range command\n%s\n",
		carbonUsage);
	  break;
	}
	status = carbonReleaseRange(model, net, msb, lsb);
	if (status != eCarbon_OK) {
	  msgFn("Error: Could not release: %s\n%s\n", 
		getStringArg(&argArray[2], &strArg, &strArgSize), carbonUsage);
	} else {
	  success = TRUE;
	  break;
	}
      }

      status = carbonRelease(model, net);
      if (status != eCarbon_OK) {
        msgFn("Error: Could not release: %s\n%s\n", 
              getStringArg(&argArray[2], &strArg, &strArgSize), carbonUsage);
      } else {
        success = TRUE;
      }
      break;

    case eCarbonChangeMsgSeverity:
      {
	CarbonUInt32 msgNumber;
	CarbonMsgSeverity severity;
	msgNumber = (CarbonUInt32) getIntArg(&argArray[2], msgFn);
	severity = (CarbonMsgSeverity) getIntArg(&argArray[3], msgFn);

	status = carbonChangeMsgSeverity(model, msgNumber, severity);
	if (status != eCarbon_OK) {
	  msgFn("Error: Could not change message severity\n%s\n", 
		carbonUsage);
	} else {
	  success = TRUE;
	}
      }
      break;

    default: 
      msgFn("Error: Unknown command: %s\n%s", 
            getStringArg(&argArray[1], &strArg, &strArgSize), carbonUsage);
      break;
  } /* switch */

  return success;
} /* mvCarbonCommand */
