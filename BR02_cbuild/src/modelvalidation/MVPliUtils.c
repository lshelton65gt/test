/*****************************************************************************
 Copyright (c) 2006-2009 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/


/*
 * Types for the PLI structures
 */
typedef struct PliArg_ PliArg;

/*
 * Structures to manage the PLI arguments for the various Carbon
 * command task/functions.
 */
struct PliArg_
{
  s_tfexprinfo  info;           /* Expression info set up at initialization */
};

typedef struct PliData_ {
  CarbonUInt32  numArgs;        /* The number of PLI arguments */
  PliArg*       args;           /* Storage for the PLI arguments */
  CarbonCmdArgs cmdArgs;        /* Storage for passing to mvCarbonCommand */
} PliData;


/*
 * Helper functions to access the PLI arguments to a UDTF
 */
static int setupPliArgs(int hasResult, int inTask)
{
  int           arg;
  PliData*      pliData;
  PliArg*       pliArg;
  CarbonUInt32* value;

  /* Allocate the work area and store it */
  pliData = malloc(sizeof(PliData));
  tf_setworkarea((void*)pliData);

  /* Allocate space for the arguments. */
  pliData->numArgs = tf_nump();
  pliData->args = malloc(sizeof(PliArg) * pliData->numArgs);

  /* Decrement the arguments if the last is a result. We don't want to
   * add the data for the result argument. Also populate the
   * expression info so we can propagate the value */
  if (hasResult) {
    pliArg = &pliData->args[pliData->numArgs-1];
    tf_exprinfo(pliData->numArgs, &pliArg->info);
    --(pliData->numArgs);
  }

  /* Init the Carbon command arg array */
  pliData->cmdArgs.argc = 0;
  pliData->cmdArgs.argArraySize = 0;
  pliData->cmdArgs.argArray = NULL;

  /* Process the args and get the information for it */
  for (arg = 0; arg < pliData->numArgs; ++arg) {
    /* Get the data for this argument */
    pliArg = &pliData->args[arg];
    tf_exprinfo(arg+1, &pliArg->info);

    /* Allocate space for it */
    switch(pliArg->info.expr_type) {
      case TF_STRING:
        /* Add the string to the CarbonCmdArgs array */
        mvAddStringArg(&pliData->cmdArgs, strdup(pliArg->info.expr_string));
        break;

      case TF_READONLY:
      case TF_READWRITE:
      case TF_RWBITSELECT:
      case TF_RWPARTSELECT:
      case TF_RWMEMSELECT:
        /* Allocate space to convert the data */
        value = malloc(pliArg->info.expr_ngroups * sizeof(CarbonUInt32));
        mvAddDataArg(&pliData->cmdArgs, pliArg->info.expr_ngroups, value);
        break;

      case TF_READONLYREAL:
      case TF_READWRITEREAL:
      case TF_NULLPARAM:
      default:
        tf_error("setupPliArgs: unexpected argument type '%d' for arg '%d'\n",
                 pliArg->info.expr_type, arg);
        tf_dostop();
        mvAddStringArg(&pliData->cmdArgs, strdup("0"));
        break;
    } /* switch */
  } /* for */

  return mvCheckArgs (&pliData->cmdArgs, inTask, (MsgFn)io_printf);

} /* setupPliArgs */

/* Get all the non-constant string arguments for this call. All string
 * arguments are populated during setupPliArgs. The data arguments are
 * stored into the CarbonUInt32 array allocated during setupPliArgs.
 */
static CarbonCmdArgs* getPliArgs(s_tfexprinfo** result)
{
  PliData*      pliData;
  PliArg*       pliArg;
  int           arg;
  int           argc;
  int           i;
  s_vecval*     vecval;
  CarbonUInt32* value;

  /* Get the expr info data from the work area */
  pliData = (PliData*)tf_getworkarea();

  /* Figure out how many input arguments we have. */
  argc = pliData->numArgs;
  if (result != NULL) {
    /* Last argument is the result and is not put into the argc/argv array */
    *result = &(pliData->args[argc].info);
  }

  /* Process the args into the argv array */
  for (arg = 0; arg < argc; ++arg) {
    /* Get the data for this argument */
    pliArg = &pliData->args[arg];

    /* Convert the data or duplicate the string */
    switch(pliArg->info.expr_type) {
      case TF_STRING:
        /* string was processed during setup */
        break;

      case TF_READONLY:
      case TF_READWRITE:
      case TF_RWBITSELECT:
      case TF_RWPARTSELECT:
      case TF_RWMEMSELECT:
        /* Update the value - PLI arguments start at 1 */
        tf_evaluatep(arg+1);

        /* Convert the 4-state value to a 2-state Carbon value */
        vecval = pliArg->info.expr_value_p;
        value = pliData->cmdArgs.argArray[arg].data.value;
        for (i = 0; i < pliArg->info.expr_ngroups; ++i) {
          value[i] = vecval[i].avalbits & ~vecval[i].bvalbits;
        }
        break;

      case TF_READONLYREAL:
      case TF_READWRITEREAL:
      case TF_NULLPARAM:
      default:
        tf_error("getPliArgs: unexpected argument type '%d' for arg '%d'\n",
                 pliArg->info.expr_type, arg);
        tf_dostop();
        break;
    } /* switch */
  } /* for */

  return &pliData->cmdArgs;
} /* getPliArgs */

static void pliError(char* msg, int stop)
{
  tf_error(msg);
  if (stop) {
    tf_dostop();
  }
}


/*
 * The various pli compiletf and calltf functions for the different
 * UDTF implementations of the Carbon command.
 */
static int pliCarbonCommandWithResult(CarbonCmdArgs* cmdArgs, int stop)
{
  int           success;
  CarbonUInt32* resultArray;
  CarbonUInt32  resultWords;
  CarbonUInt32  resultBits;

  /* Call the carbon command and apply the result to the function */
  success = mvCarbonCommand(cmdArgs, (MsgFn)io_printf, &resultArray, &resultWords,
                            &resultBits);
  if (success) {
    if (resultWords > 1) {
      tf_warning("pliCarbonCommand: function result wider than 32-bits (%d); data truncated.\n", resultWords * 32);
    }
    tf_putp(0, resultArray[0]);

  } else {
    /* A failure occurred, print an error to get a line number and
     * stop the simulation */
    pliError("pliCarbonCommand: failed.\n", stop);
  }
  return success;
}

static int pliCarbonCommand(PLI_BYTE8* userData)
{
  CarbonCmdArgs* cmdArgs = (CarbonCmdArgs*)userData;
  char*          args;
  int            success;

  /* Get the first and hopefully only argument */
  if (tf_nump() > 1) {
    /* A failure occurred, print an error to get a line number and
     * stop the simulation */
    pliError("pliCarbonCommand: command takes one string argument; extra arguments ignored.\n", FALSE);

  } else if (tf_nump() == 1) {
    /* Assume the argument is a string */
    args = tf_getcstringp(1);
    if (args != NULL) {
      /* Copy it so that we can break it up into an argv array */
      args = strdup(args);

    } else {
      /* A failure occurred, print an error to get a line number and
       * stop the simulation */
      tf_error("pliCarbonCommand: could not convert command argument of type '%d' to a string.\n", tf_typep(1));
    }
  }
  if (args == NULL) {
    return 1;
  }

  /* Tokenize the string in place */
  tokenizeString(args, cmdArgs);

  /*
   * Call the carbon command and process the result (arg 0).
   *
   * Note: for some reason VCS doesn't like tf_dostop() gettting
   * called if this UDTF is called from the CLI. So for now, pass
   * FALSE for the do stop flag.
   */
  success = pliCarbonCommandWithResult(cmdArgs, FALSE);

  /* Free the allocated memory. The argv is created by modifying the
   * args string so we just have to do one free to get rid of all the
   * tokens. */
  free(args);
  cmdArgs->argc = 0;
  return (0);
}

static int pliCarbonCommandFnCheck(PLI_BYTE8* userData)
{
  /* Allocate space for the arguments */
  return setupPliArgs(FALSE, FALSE);
}

static int pliCarbonCommandFn(PLI_BYTE8* userData)
{
  CarbonCmdArgs* cmdArgs;
  int            success;

  /* Get the arguments and process them into an arc argv pair */
  cmdArgs = getPliArgs(NULL);

  /* Call the Carbon command and set the function result */
  success = pliCarbonCommandWithResult(cmdArgs, TRUE);
  return (success);
}

static int pliCarbonCommandTaskCheck(PLI_BYTE8* userData)
{
  /* Allocate space for the arguments */
  return setupPliArgs(FALSE, TRUE);
}

static int pliCarbonCommandTask(PLI_BYTE8* userData)
{
  CarbonCmdArgs* cmdArgs;
  int            success;
  CarbonUInt32*  resultArray;
  CarbonUInt32   resultArraySize;
  CarbonUInt32   resultBits;

  /* Get the arguments and process them into an arc argv pair */
  cmdArgs = getPliArgs(NULL);

  /* Call the Carbon command */
  success = mvCarbonCommand(cmdArgs, (MsgFn)io_printf, &resultArray,
                            &resultArraySize, &resultBits);
  if (!success) {
    /* A failure occurred, print an error to get a line number and
     * stop the simulation */
    pliError("pliCarbonCommand: failed.\n", TRUE);
  }
  return success;
}

static int pliCarbonCommandTaskResCheck(PLI_BYTE8* userData)
{
  /* Allocate space for the arguments */
  return setupPliArgs(TRUE, FALSE);
}

static int pliCarbonCommandTaskRes(PLI_BYTE8* userData)
{
  CarbonCmdArgs* cmdArgs;
  s_tfexprinfo*  resultArg;
  int            success;
  CarbonUInt32*  resultArray;
  CarbonUInt32   resultArraySize;
  CarbonUInt32   resultBits;
  CarbonUInt32   i;

  /* Get the arguments and process them into an arc argv pair */
  cmdArgs = getPliArgs(&resultArg);

  /* Call the Carbon command and set the result */
  success = mvCarbonCommand(cmdArgs, (MsgFn)io_printf, &resultArray,
                            &resultArraySize, &resultBits);
  if (success) {
    /* Write the value to the output */
    for (i = 0; i < resultArraySize; ++i) {
      resultArg->expr_value_p[i].avalbits = resultArray[i];
      resultArg->expr_value_p[i].bvalbits = 0;
    }
    tf_propagatep(cmdArgs->argc+1);
  } else {
    /* A failure occurred, print an error to get a line number and
     * stop the simulation */
    pliError("pliCarbonCommand: failed.\n", TRUE);
  }
  return success;
}

static void pliCarbonCommandInit()
{
  s_vpi_systf_data systf_data;
  vpiHandle        systf_handle;
  CarbonCmdArgs*   cmdArgs;

  /* Allocate and init the space for the command arguments */
  cmdArgs = malloc(sizeof(CarbonCmdArgs));
  cmdArgs->argc = 0;
  cmdArgs->argArraySize = 0;
  cmdArgs->argArray = NULL;

  /* Function that takes a single string that must be parsed as argument */
  systf_data.type         = vpiSysFunc;
  systf_data.sysfunctype  = vpiIntFunc;
  systf_data.tfname	  = "$carbon";
  systf_data.calltf	  = pliCarbonCommand;
  systf_data.compiletf    = 0;
  systf_data.sizetf       = 0;
  systf_data.user_data    = (void*)cmdArgs;
  systf_handle = vpi_register_systf(&systf_data);

  /* Function that takes individual arguments as strings or data */
  systf_data.tfname       = "$carbon_fn";
  systf_data.compiletf    = pliCarbonCommandFnCheck;
  systf_data.calltf       = pliCarbonCommandFn;
  systf_data.user_data    = 0;
  systf_handle = vpi_register_systf(&systf_data); // this call must be removed if you want to use callgrind with vcsi (12674)

  /* Task that takes individual arguments as strings or data */
  systf_data.type         = vpiSysTask;
  systf_data.tfname       = "$carbon_task";
  systf_data.compiletf    = pliCarbonCommandTaskCheck;
  systf_data.calltf       = pliCarbonCommandTask;
  systf_handle = vpi_register_systf(&systf_data);

  /* Task that takes individual arguments as strings or data */
  systf_data.type         = vpiSysTask;
  systf_data.tfname       = "$carbon_task_res";
  systf_data.compiletf    = pliCarbonCommandTaskResCheck;
  systf_data.calltf       = pliCarbonCommandTaskRes;
  systf_handle = vpi_register_systf(&systf_data);
}
