# *****************************************************************************
# Copyright (c) 2007-2009 by Carbon Design Systems, Inc., All Rights Reserved.
#
# THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
# DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
# THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
# APPEARS IN ALL COPIES OF THIS SOFTWARE.
#
# *****************************************************************************

include $(CARBON_HOME)/makefiles/Makefile.common

VHM_DIR = .

# Required model name, design name, CARBON_HOME, and SIM_HOME
ifeq ($(MODEL_NAME),)
  $(error "MODEL_NAME not supplied to Makefile!")
endif
ifeq ($(DESIGN_NAME),)
  $(error "DESIGN_NAME not supplied to Makefile!")
endif
ifeq ($(CARBON_HOME),)
  $(error "CARBON_HOME must be set to the Carbon release directory!")
endif
ifeq ($(SIM_HOME),)
  $(error "SIM_HOME must be set to the Simulator release directory!")
endif

# Check if they turned on DEBUG
ifeq ($(DEBUG),1)
  CC_OPTS=-g -O0
else
  CC_OPTS=-O2
endif

# PIC code is required on Linux64
ifeq ($(CARBON_TARGET_ARCH),Linux64)
  CC_OPTS+=-fPIC
endif

# Check the optional variable which enables tracing
ifeq ($(TRACE),1)
  FTRACE=-DVPI_TRACE=1
else
  FTRACE=
endif

# Make the .o an intermediate so that it gets deleted (request from field)
.INTERMEDIATE: $(MODEL_NAME).o

clean :
	rm -f $(MODEL_NAME).o $(MODEL_NAME).so

# Note that the switch -lpliwrapper option was added in case the
# customer built the model with -pliWrapper. In general they
# should not do this but they might if they are transitioning
# from the pli wrapper to model validation. We add the
# link option to avoid a shared library load problem that
# seems to only occur with ModelSim.
$(MODEL_NAME).so: $(MODEL_NAME).o $(VHM_DIR)/lib$(DESIGN_NAME).*
	$(CARBON_CC_LINK) $(MV_EXTRA_LDFLAGS) $(MV_VPI_EXTRA_LDFLAGS) $(CC_OPTS) -shared -o $@ $< -L$(VHM_DIR) -l$(DESIGN_NAME) -lpliwrapper $(CARBON_LIB_LIST)

# provide a profile option
$(MODEL_NAME)_p.a: $(MODEL_NAME).o $(VHM_DIR)/lib$(DESIGN_NAME).*
	$(CARBON_AR) c $@  $< -l$(DESIGN_NAME)  $(CARBON_LIB_LIST)

$(MODEL_NAME).o: $(MODEL_NAME).c $(CARBON_HOME)/include/carbon/MVVpiUtils.c
	$(CARBON_CC) $(MV_EXTRA_CFLAGS) $(FTRACE) -c $(CC_OPTS) -I$(MV_VPI_INCLUDE_DIR) -I$(CARBON_HOME)/include -I$(VHM_DIR) $<
