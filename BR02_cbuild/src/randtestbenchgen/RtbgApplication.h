// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#ifndef __RTBGAPPLICATION_H_
#define __RTBGAPPLICATION_H_


#include "modshell/MdsApplication.h"


/*!
  \file

*/

//! RtbgApplication class
/*!
*/
class RtbgApplication : public MdsApplication
{
 public:
  //! Constructor.
  /*!
    Construct a top level application object.
    \param argc Number of command line arguments.
    \param argv Array of command line arguments.
   */
  RtbgApplication (int* argc, char **argv);

  //! Destructor.
  virtual ~RtbgApplication(void);

  //! Run the application.
  /*!
    Run the application.
    \return The exit status of program.
  */
  SInt32 run(void);


private:
  virtual void setupCommandLineOptions(void);
  virtual bool checkCommandLineOptions(void);
  UInt32 getVectorCount(void);
  SInt32 getVectorSeed(void);
  
 private:
  RtbgApplication(void);
  RtbgApplication(const RtbgApplication&);
  RtbgApplication& operator=(const RtbgApplication&);
};


#endif
