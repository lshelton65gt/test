// -*- C++ -*-
/******************************************************************************
 Copyright (c) 2003-2004 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/



#include "RtbgApplication.h"
#include "modshell/MdsGenerator.h"
#include "util/UtString.h"
#include "util/UtTestPatternGen.h"


RtbgApplication::RtbgApplication(int *argc, char** argv) : MdsApplication(argc, argv)
{
}


RtbgApplication::~RtbgApplication(void)
{
}


SInt32 RtbgApplication::run(void)
{
  /*
  ** Command line processing.
  */
  mArgs.setDescription("Random Testbench Generator",
                       "crandtestbenchgen",
		       "Creates an executable that ... ");

  setupCommandLineOptions();
  parseCommandLine();
  checkCommandLineOptions();

  /*
  ** Setup the IODB.
  */
  //  createIODB();
  //createConfig();

  /*
  ** See if there are bidis.  We don't handle them right now.
  */
  //assert(mIODB.loopBidi() == 0);

  /*
  ** Generate random vectors.
  */

  /*
  ** Dynamically generate the object wrapper for the model.  Maybe
  ** cbuild does this someday.
  */
  MdsGenerator gen(mModelName, mInstallDir, mTargetDir, mMsgContext);
  gen.generate();

  /*
  ** Link the C driver.
  */
  UtString name("crandtestbench");
  linkExecutable(name, name, eGCC3);
  
  /*
  ** Generate Verilog testbench.
  */


  return 0;
}


void RtbgApplication::setupCommandLineOptions(void)
{
  mArgs.addBool("-debugOutput", "Generate vector display in a more human friendly format", false);
  mArgs.addInt("-vectorCount", "Number of vectors to create", 32);
  mArgs.addInt("-vectorSeed", "Provide a new seed for generating random vectors", 0);

  MdsApplication::setupCommandLineOptions();
}


bool RtbgApplication::checkCommandLineOptions(void)
{
  return MdsApplication::checkCommandLineOptions();
}


UInt32 RtbgApplication::getVectorCount(void)
{
  SInt32 count = 32;
  mArgs.getIntFirst("-vectorCount", &count);
  return static_cast<UInt32>(count);
}


SInt32 RtbgApplication::getVectorSeed(void)
{
  SInt32 seed = 0;
  mArgs.getIntFirst("-vectorCount", &seed);
  return seed;
}

#if 0
bool RtbgApplication::generateVectors(void)
{
  /*
  ** Open destination file.
  */
  UInt32 clock_width = 0;
  UInt32 reset_width = 0;
  UInt32 data_width = 0;

  accumulateWidth(getClocks(), clock_width);
  accumulateWidth(getResets(), reset_width);
  accumulateWidth(getInputs(), data_width);

  UtToggledTestPatternGen clock_bits(clock_width, 712 + getVectorSeed());
  UtStepTestPatternGen reset_bits(reset_width, 8403 + getVectorSeed(), !mResetHigh, 5);
  UtRandomTestPatternGen data_bits(data_width, 4999 + getVectorSeed());

  for (UInt32 i = 0 ; i < getVectorCount(); i++)
  {
    out << clock_bits << reset_bits << data_bits << UtIO::endl;

    ++clock_bits;
    ++data_bits;
    ++reset_bits;
  }


  /*
  ** Close destination file.
  */
}


void TestDriverData::accumulateWidth(const UtVector<NUNet *> &ports, UInt32 &width) const
{
  for (UtVector<NUNet *>::const_iterator i = ports.begin(); i != ports.end(); i++)
  {
    width += (*i)->getBitSize();
  }
}
#endif
