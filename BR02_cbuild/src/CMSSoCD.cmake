project(CMSSoCD)

add_definitions(-DCARBON=1)

if(${MSVC})
  add_definitions(/DWIN32=1 /D_REENTRANT /DMx__NO_ENDIANESS_DETECTION)
  set(CMAKE_CXX_FLAGS
      "/MD /GR /WL /FC /EHsc"
      )
else()
  set(CMAKE_CXX_FLAGS
      "-Wall -W -Wno-unused-parameter"
      )

  # Default release flags have -DNDEBUG, which doesn't work for us
  # because we have a few live asserts.
  set(CMAKE_CXX_FLAGS_RELEASE
      "-O3"
      )
  set(CMAKE_C_FLAGS_RELEASE
      "-O3"
      )
endif()

include_directories(
                   ${CMAKE_SOURCE_DIR}/inc
                   ${CARBON_HOME}/include
                   )

#
# format of socd is like: /o/release/SoCD/nightly/Win32-VC2008-dev
# generalized this using the variable RELEASE_DIR defined in FindCMS.cmake
#

if(${CMAKE_GENERATOR} STREQUAL "Visual Studio 8 2005")
  set(suffix "_vc8")
  set(MAXSIM_HOME ${RELEASE_DIR}/SoCD/nightly/Win32-VC2005-dev)
elseif(${CMAKE_GENERATOR} STREQUAL "Visual Studio 9 2008")
  set(suffix "_vc9")
  set(MAXSIM_HOME ${RELEASE_DIR}/SoCD/nightly/Win32-VC2008-dev)
elseif(${CMAKE_GENERATOR} STREQUAL "Visual Studio 10")
  set(suffix "_vc10")
  set(MAXSIM_HOME ${RELEASE_DIR}/SoCD/nightly/Win32-VC2010-dev)
elseif(${CMAKE_GENERATOR} STREQUAL "Visual Studio 11")
  set(suffix "_vc11")
  set(MAXSIM_HOME ${RELEASE_DIR}/SoCD/nightly/Win32-VC2012-dev)
else()
  set(suffix "")
  set(MAXSIM_HOME ${RELEASE_DIR}/SoCD/nightly/Linux-dev)
endif()

# No 64-bit SoCD (yet) so nothing to do.
if(NOT("${ARCH}" STREQUAL "Linux64"))
  add_subdirectory(xactors/socvsp)
  add_subdirectory(xactors/arm)
endif()
