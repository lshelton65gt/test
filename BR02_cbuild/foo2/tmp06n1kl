// -*-c++-*-
/******************************************************************************
 Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "logger/BladeLogger.h"
#include "sim/FMAnalyzerHeuristics.h"
#include "sim/InstructionWindow.h"
#include "sim/RunTimeEstimator.h"
#include "sim/Stats.h"

//#include <iostream>
#include <stdexcept>
#include <sstream>


using namespace Carbon::Blade::Logger;
using namespace std;

namespace Carbon
{
namespace Blade
{


  InstructionWindow::InstructionWindow(int cpu, Addr start, Addr end,
                                       bool thumb)
      : mCPU(cpu),
        mStart(start),
        mEnd(end),
        mThumb(thumb),
        mError(false),
        mStreamId(cpu),
        mKernel(false)
  {
    //LOG_INF << "IW size: " << dec << sizeof(InstructionWindow);
    if (mEnd < mStart)
    {
      //
      // This happened when FM tracing had a bug and when CA tracing was poorly
      // understood.  Leaving it in so that we are alerted in case something
      // goes
      // wrong again.
      //
      LOG_ERR << "Invalid instruction window: cpu " << std::dec << mCPU
              << " start = 0x" << std::hex << mStart << ", end = 0x" << mEnd;
      mError = true;
    }

#if 1
    //
    // "Window too big" heuristic ...
    // This also happened when FM tracing had a bug and when CA tracing was
    // poorly
    // understood.  We haven't (yet) seen very large instruction windows in real
    // code, so for the time being leaving it in the code so we can be alerted
    // in
    // case something suspicious happens in the future.
    // Note: Linux 3.2.0 test cases on A15 have a window > 5,000 bytes
    //
    else if (mEnd - mStart > 0x2000)
    {
      LOG_WRN << "Warning: questionable instruction window of size " << std::dec
              << (mEnd - mStart) << ": cpu " << mCPU << " start = 0x"
              << std::hex << mStart << ", end = 0x" << mEnd;
      mError = true;
    }
#endif
  }

  uint32_t InstructionWindow::getNumInstructions() const
  {
    // Determine code density.  ARM instructions are 4 bytes.  Thumb
    // instructions are either 2 or 4, but the 4-byte instructions are
    // effectively executed as two instructions, so we can assume two
    // bytes per instruction.
    uint32_t bytesPerInstruction = mThumb ? 2 : 4;
    return ((mEnd - mStart) / bytesPerInstruction) + 1;
  }
  
  // Determine (approximately) the number of instructions in the window including 
  // all the previous kernel window instruction counts.   
  uint32_t InstructionWindow::getNumInstructionsWithKernel() const
  {
    // Determine code density.  ARM instructions are 4 bytes.  Thumb
    // instructions are either 2 or 4, but the 4-byte instructions are
    // effectively executed as two instructions, so we can assume two
    // bytes per instruction.
    uint32_t bytesPerInstruction = mThumb ? 2 : 4;
    uint32_t count = ((mEnd - mStart) / bytesPerInstruction) + 1;
    if (isKernel())
    {
//      sKernelInstructionCount += count; // keep track of how many previous kernel instructions there were
      count = 0;  // return 0
    }
    else
    {
//      count += sKernelInstructionCount;
      //sKernelInstructionCount = 0;
    }
    return count;
  }  

  int InstructionWindow::count(Addr addr) const
  {
    return contains(addr) ? 1 : 0;
  }

  bool InstructionWindow::contains(Addr addr) const
  {
    return (addr >= mStart) && (addr <= mEnd);
  }

  InstructionWindowFactory::InstructionWindowFactory()
      : mAllowWindowsWithErrors(true)
  {
  }

  InstructionWindowFactory::~InstructionWindowFactory()
  {
    for (InstructionWindowCountMap::iterator iter = mAllocatedWindows.begin();
         iter != mAllocatedWindows.end(); ++iter)
    {
      delete iter->first;
    }
  }

  const InstructionWindow *InstructionWindowFactory::getWindow(
      int cpu, Addr start, Addr end, bool thumb)
  {
    // Create a new window and look it up in the allocated set.  If it
    // already exists, delete it and return the existing one.  Otherwise
    // add the new one.
    InstructionWindow *window = new InstructionWindow(cpu, start, end, thumb);
    if (window->error())
    {
      // For testing purposes, we can throw an exception if we see a
      // window with an error.
      if (!mAllowWindowsWithErrors)
      {
        throw std::runtime_error("Error when creating instruction window");
      }
      // Don't add window if there was a creation error
      return window;
    }
    InstructionWindowCountMap::iterator iter = mAllocatedWindows.find(window);
    if (iter == mAllocatedWindows.end())
    {
      mAllocatedWindows[window] = 1;
    }
    else
    {
      delete window;
      window = iter->first;
      ++mAllocatedWindows[window];
    }
    return window;
  }

  uint32_t InstructionWindowFactory::getWindowCount(const InstructionWindow *
                                                    window) const
  {
    InstructionWindowCountMap::const_iterator iter =
        mAllocatedWindows.find(const_cast<InstructionWindow *>(window));
    if (iter == mAllocatedWindows.end())
    {
      return 0;
    }
    return iter->second;
  }

  ////////////////////////////////////////////////////////////////////////////////
  // InstructionWindowVec pure virtual base class and derived classes
  ////////////////////////////////////////////////////////////////////////////////

  InstructionWindowVec::InstructionWindowVec(const FMAnalyzerHeuristics &
                                             heuristics)
      : mHeuristics(heuristics)
  {
  }
  InstructionWindowVec::~InstructionWindowVec()
  {
  }

  InstructionWindowVec *InstructionWindowVec::Create(
      const FMAnalyzerHeuristics & heuristics)
  {
    //
    // Basically, if we're tracing estimation data, always use
    // WindowEstimatorVec,
    // which stores estimator input in the InstructionWindowVec.
    //
    // Otherwise only use WindowWeightVec -- which puts calculated weights
    // (i.e.,
    // estimator output) in the InstructionWindowVec.
    //
    if (Stats::Estimator::Enabled())
    {
      return new WindowEstimatorVec(heuristics);
    }
    else
    {
      switch (heuristics.PartitionAlgorithm())
      {
      case FMAnalyzerHeuristics::TwoPassEstimator:
        return new WindowWeightVec(heuristics);
      default:
        return new WindowVec(heuristics);
      }
    }
  }

  //
  // WindowVec
  //
  void WindowVec::addWindow(const InstructionWindow * window)
  {
    vec.push_back(window);
  }
  const InstructionWindow *WindowVec::at(uint32_t index) const
  {
    return vec[index];
  }
  uint32_t WindowVec::size() const
  {
    return vec.size();
  }
  void WindowVec::addEstimatorData(RunTimeEstimator * estimator)
  {
  }
  float WindowVec::getWeight(uint32_t index) const
  {
    return (float)(vec[index]->getNumInstructions());
  }
  void WindowVec::accumulateEstimator(RunTimeEstimator & estimator,
                                      uint32_t index)
  {
    estimator.addInstruction(vec[index]->getCPU(),
                             vec[index]->getNumInstructions());
  }
  WindowVec::WindowVec(const FMAnalyzerHeuristics & heuristics)
      : InstructionWindowVec(heuristics)
  {
  }
  WindowVec::~WindowVec()
  {
  }

  //
  //  WindowWeightVec
  //
  void WindowWeightVec::addWindow(const InstructionWindow * window)
  {
    elementT element(window);
    vec.push_back(window);
  }
  const InstructionWindow *WindowWeightVec::at(uint32_t index) const
  {
    return vec[index].window;
  }
  uint32_t WindowWeightVec::size() const
  {
    return vec.size();
  }
  void WindowWeightVec::addEstimatorData(RunTimeEstimator * estimator)
  {
    vec.back().weight = estimator->computeCurrentWeight(vec.back().getCPU());
  }
  float WindowWeightVec::getWeight(uint32_t index) const
  {
    return vec[index].weight;
  }
  void WindowWeightVec::accumulateEstimator(RunTimeEstimator & estimator,
                                            uint32_t index)
  {
    // this shouldn't happen, as a WindowEstimatorVec should have been created
    // instead, so we'll leave this as a soft failure (doing nothing)
  }
  WindowWeightVec::WindowWeightVec(const FMAnalyzerHeuristics & heuristics)
      : InstructionWindowVec(heuristics)
  {
  }
  WindowWeightVec::~WindowWeightVec()
  {
  }

  //
  // WindowEstimatorVec
  //
  void WindowEstimatorVec::addWindow(const InstructionWindow * window)
  {
    elementT element(window);
    vec.push_back(window);
  }
  const InstructionWindow *WindowEstimatorVec::at(uint32_t index) const
  {
    return vec[index].window;
  }
  uint32_t WindowEstimatorVec::size() const
  {
    return vec.size();
  }
  void WindowEstimatorVec::addEstimatorData(RunTimeEstimator * estimator)
  {
    //
    // Note here we're discarding the upper 32 bits because we expect
    // instruction
    // windows to be small:
    //
    int cpu = vec.back().getCPU();
    vec.back().cache_misses = (uint32_t)estimator->getCacheMisses(cpu);
    vec.back().load_stores = (uint32_t)estimator->getLoadStores(cpu);
    assert(vec.back().window->getNumInstructions() ==
           (uint32_t)estimator->getInstructions(cpu));
  }
  float WindowEstimatorVec::getWeight(uint32_t index) const
  {
    RunTimeEstimator estimator(mHeuristics);
    int cpu = vec[index].getCPU();
    estimator.addCacheMiss(cpu, vec[index].cache_misses);
    estimator.addLoadStore(cpu, vec[index].load_stores);
    estimator.addInstruction(cpu, vec[index].window->getNumInstructions());
    return estimator.computeCurrentWeight(cpu);
  }
  void WindowEstimatorVec::accumulateEstimator(RunTimeEstimator & estimator,
                                               uint32_t index)
  {
    int cpu = vec[index].getCPU();
    estimator.addCacheMiss(cpu, vec[index].cache_misses);
    estimator.addLoadStore(cpu, vec[index].load_stores);
    estimator.addInstruction(cpu, vec[index].window->getNumInstructions());
  }
  WindowEstimatorVec::WindowEstimatorVec(const FMAnalyzerHeuristics &
                                         heuristics)
      : InstructionWindowVec(heuristics)
  {
  }
  WindowEstimatorVec::~WindowEstimatorVec()
  {
  }

  string dump(const InstructionWindow & iw)
  {
    ostringstream oss;
    oss << dec << "(" << iw.getCPU() << ", "
        << " 0x" << hex << iw.getStart() << ", 0x" << iw.getEnd() << ", " << dec
        << " " << iw.getStreamId() << ")";
    return oss.str();
  }
}
}
