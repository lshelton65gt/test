// -*-c++-*-
/******************************************************************************
 Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#ifndef __SIMULATION_MANAGER_H__
#define __SIMULATION_MANAGER_H__

#include "common/Timescale.h"
#include "eslapi/CADITypes.h"
#include "MxCycleListener.h"
#include "MxSimStatusListener.h"
#include "sim/SimulationManagerIF.h"
#include "sim/SystemInfo.h"
#include "PlatformUtils/memory.h"

#include <boost/exception_ptr.hpp>


#include <deque>
#include <fstream> 
#include <map>
#include <string>
#include <vector>

namespace eslapi
{
class MxSimAPI;
class CADICallbackObj;
class CASIModuleIF;
class MxDissassemblyBreakPointItem;
class CADI;
}

namespace Carbon
{

namespace SoCDEvents
{
class Event;
class EventChannel;
}


namespace Blade
{

//! Manages blade simulations
/*!
  This handles blade simulations on the FM and CA sides.  It can track
  PCs, set breakpoints, and create checkpoints.
*/
class SimulationManager : public SimulationManagerIF
{
public:
  SimulationManager(const std::string& maxlib, const std::vector<CpuInfo>&);
  ~SimulationManager();

  //! Callback interface to handle events during the simulation
  /*!
  Custom classes can be derived from this to implement specific
  behaviors during simulation.
  */
  class Callback
  {
  public:
    Callback() {}
    virtual ~Callback() {}

    //! Called at the beginning of a simulation
    /*!
    This is called when the SimulationManager is about to start
    running the simulation, but has not actually told the scheduler to
    run.  It is permissible for the callback to call a function
    (e.g. runToDebuggablePoint()) that starts the simulation as a side
    effect.  The function needs to return true in that case (so that
    the SimulationManager's synchronization is correct), and false
    otherwise.
    */
    virtual bool simulationStart() = 0;
    //! Called at the end of a simulation
    virtual void simulationEnd() = 0;
    //! Called when an event is processed at the end of a SoCD cycle
    virtual void eventSeen(const SoCDEvents::Event* event) = 0;
    //! Called when the simulation enters the stopped state in the course of running
    /*!
    This can be used to implement callbacks that can't be done when
    the simulation is actually running.

    Like simulationStart(), this can call a function that starts the
    simulation, and must return true if it does.
    */
    virtual bool simulationStopped()
    {
      return false;
    }
  };


  //! Add a callback, registering it for events
  void addCallback(Callback* cb);
  //! Remove a callback
  void removeCallback(Callback* cb);

  void clearCallbacks();

  // The Bladerunner ApplicationsXML File
  void setBladeRunnerMode(bool value);
  void setBladeRunnerApplicationsFile(const std::string& xmlFile);
  //! Set the application file for a component instance
  void setAppFile(const std::string& instance, const std::string& file);
  //! Open an RTOE system
  void openRTOESystem(const std::string& filename);
  //! Run simulation until completion
  void runBlocking();
  //! Close the current system
  void closeSystem();

  //! create an old school save (mxr)
  void saveSimulation(const std::string&, bool);   

  //! Create a checkpoint (empty workspace and description)
  virtual void createCheckpoint(const std::string& filename);
  //! Restore a checkpoint to the current system
  void restoreCheckpoint(const std::string& filename);
  //! Get the current cycle count
  uint64_t getCycleCount();
  //! Call a custom MxScript command registered by a SoCD plugin
  bool callPluginScriptCommand(const std::string& command);
  //! Enable a profiling stream
  void enableProfiling(const std::string& instance, const std::string& stream, const std::string& filename, int compressionFactor);
  //! Load memory image from file to a component instance
  void loadMemoryFromFile(const std::string& filename,
                          bool ascii,
                          const std::string& instance,
                          const std::string& memspace,
                          uint64_t startAddr,
                          uint64_t endAddr);
  //! Add a disassembly breakpoint, returning success
  bool addDisassemblyBreakpoint(const std::string& instance,
                                uint64_t address,
                                int memorySpace,
                                eslapi::CADIBptNumber_t* bptNum);
#if 0
  bool addBreakpointsAllCpus(uint64_t address, int memorySpace,
                             std::vector<eslapi::CADIBptNumber_t> &numbers);
#endif
  //! Remove a breakpoint, returning success
  bool removeBreakpoint(eslapi::CADIBptNumber_t bpNum);

  void clearBreakpoints();

  eslapi::MxDissassemblyBreakPointItem* getDissassembly;

  //! Tell a component to run to a debuggable point
  /*!
  This is a non-blocking function.  You also need to call simHalt()
  and wait for the simulation to stop.
  */
  void runToDebuggablePoint(const std::string& instance);
  //! Check the debuggable state of a component
  /*
  Returns true if at a debuggable point
  */
  bool atDebuggablePoint(const std::string& instance);

  //! Query the timescale for the loaded .mxp file
  void getClockPeriod(Timescale::Value* value, Timescale::Unit* unit);

  //! Request that simulation continue after the next stop
  /*!
  This clears an earlier terminate request.
  */
  void requestContinue()
  {
    mRequestContinue = true;
    mRequestTerminate = false;
  }

  //! Request that simulation terminate after the next stop
  /*!
  This clears an earlier continue request.
  */
  void requestTerminate()
  {
    mRequestTerminate = true;
    mRequestContinue = false;
  }

  //! Look up a CASIModuleIF for an instance name
  virtual eslapi::CASIModuleIF* getComponent(const std::string& instance);

  //! Request that the next running/stopped pair be ignored
  /*!
  There are situations (e.g. FM single step) that directly start the
  simulation, so the simulation manager is not aware of it.  This can
  cause it to assume incorrectly that a run it requested has completed
  and that the simulation is stopped.  This call can ignore the
  stop/start pair so the stop *after* the direct start is treated as
  the effective stop.
  */
  void ignoreNextRunningStoppedPair();

  //! Is the SoCD scheduler currently halted?
  bool isSchedulerHalted() const;

  // SimulationManagerIF functions

  int getCpuFromCadi(eslapi::CADI* cadi);

  //! Look up a CADI interface for an instance
  eslapi::CADI* getCADI(const std::string& instance);
  //! Halt a currently-running simulation
  void simHalt();
  // See if a terminate request is pending
  bool terminatePending();

private:
  //! Simulation control object
  eslapi::MxSimAPI* mSimulator;

  //! Class for running SoCD in a blocking call, waiting for it to stop
  /*!
  Because the simulation runs in a different thread, there is some
  synchronization that has to be done.  This class inherits from the
  simulation status change callback so it can ensure the simulation
  block is run.
  */
  class BlockingRunner : public eslapi::MxSimStatusListener
  {
  public:
    BlockingRunner(eslapi::MxSimAPI* simAPI,
                   SimulationManager* simMgr, boost::exception_ptr &);
    virtual ~BlockingRunner();
    void reset();

    virtual void simulatorStatusChanged(int running);

    //! Run the simulation and wait for it to stop.  
    void run(bool simRunning);

    void ignoreNextRunningStoppedPair();

    //! Request that the next mode change be blocked
    /*!
    This is a more extreme modification than
    ignoreNextRunningStoppedPair().  In this case, we don't want the
    mode change to be seen at all - hidden not only from the
    simulation control, but also from any code that might respond to
    mode change events.

    The purpose of this it to allow fake mode changes to be blocked.
    */
    void blockNextModeChange()
    {
      mBlockNextModeChange = true;
    }

  private:
    eslapi::MxSimAPI* mSimAPI;
    SimulationManager *mSimMgr;
    bool mSawRun;            //! flag to track occurances of change to running
    bool mSawStop;           //! tracks transitions to stopped state
    bool mIgnoreNextRunning; //! ignore next change to running if true
    bool mIgnoreNextStopped; //! ignore the next status chang to stopped if true
    bool mBlockNextModeChange;    //! Block the next mode change if true
    boost::exception_ptr &mError; //! Shared exception for crossing threads
  };
  //! Instance of the runner class
  BlockingRunner* mRunner;


  //! Callback class for processing events at the end of each cycle
  /*!
  The events processed by this class include both those pulled from
  the external channel interface as well as those reported directly.
  */
  class EventProcessor : public eslapi::MxCycleListener
  {
  public:
    EventProcessor(SimulationManager* simMgr, boost::exception_ptr&);
    virtual ~EventProcessor();

    virtual void cycleFinishedCB();

    //! Add an event directly, instead of using the channel
    void addEvent(const SoCDEvents::Event* event);

  private:
    //! Simulation manager parent
    SimulationManager* mSimMgr;
    //! Channel for receiving events from CA and FM models
    SoCDEvents::EventChannel* mEventChannel;
    typedef std::deque<const SoCDEvents::Event*> EventQueue;
    //! Locally-reported events
    EventQueue mLocalEvents;
    boost::exception_ptr &mError;
  };
  //! Instance of event processor class
  EventProcessor mEventProcessor;
  //! Reporting function for event processor class
  void reportEvent(const SoCDEvents::Event* event);

  typedef std::vector<Callback*> CallbackVec;
  //! All the registered callback objects
  CallbackVec mCallbacks;

  //! CADI Callback class for handling breakpoint mode changes
  class BPModeChange;

  typedef std::map<eslapi::CADI*, eslapi::CADICallbackObj*> CADICallbackMap;
  //! Registered CADI callback objects
  CADICallbackMap mCADICallbacks;

  //! Is a system currently open?
  bool mSystemOpen;
  //! Did we see a breakpoint since the last simulation start?
  bool mBreakpointHit;
  //! Set the breakpoint hit flag
  void setBreakpointHit()
  {
    mBreakpointHit = true;
  }

  //! Was there an explicit request to continue simulation after the next stop?
  bool mRequestContinue;
  //! Was there an explicit request to terminate simulation after the next stop?
  bool mRequestTerminate;

  //! Reset flags for determining when to terminate/continue the simulation
  void resetFlags();

  std::ofstream mCommandTrace;
  std::vector<CpuInfo> mCpus;

  boost::exception_ptr mError;
};

}
} // namespace

#endif
