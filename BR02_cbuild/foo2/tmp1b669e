/*!
 * \file  ARM926CTExtTCM.lisa
 * \brief LISA Implementation of the ARM926 processor with an external DTCM interface
 * \date Copyright 2010 Carbon Design Systems. All rights reserved.
 */


component ARM926CTExtTCM
{
  properties {
    version = "6.0.45";
    component_type = "Core";
    description = "ARM926CT CPU component with an external TCM";
  }

  includes {
#define ARM926CTExtTCM_DBG if (0) printf
  }

  resources {
    PARAMETER { name("BIGENDINIT"), type(bool), default(false) } BIGENDINIT;
    PARAMETER { name("VINITHI"), type(bool), default(false) } VINITHI;
    PARAMETER { name("INITRAM"), type(bool), default(false) } INITRAM;
    PARAMETER { name("itcm0_ext_size"), type(uint32_t), default(0x8), min(0x0), max(0x400) } itcm0_ext_size;
    PARAMETER { name("itcm0_int_size"), type(uint32_t), default(0x8), min(0x0), max(0x400) } itcm0_int_size;
    PARAMETER { name("itcm0_base"), type(uint32_t), default(0x0), min (0x0), max(0xFFFFFFFF) } itcm0_base;
    PARAMETER { name("dtcm0_ext_size"), type(uint32_t), default(0x8), min(0x0), max(0x400) } dtcm0_ext_size;
    PARAMETER { name("dtcm0_int_size"), type(uint32_t), default(0x8), min(0x0), max(0x400) } dtcm0_int_size;
    PARAMETER { name("dtcm0_base"), type(uint32_t), default(0x0), min (0x0), max(0xFFFFFFFF) } dtcm0_base;

    pv::TransactionGenerator* tg_bus;
    pv::TransactionGenerator* tg_itcm;
    pv::TransactionGenerator* tg_dtcm;
  }

  // The clock signal connected to the clk_in port is used to determine
  // the rate at which the core executes instructions.
  slave port<ClockSignal> clk_in;

  // The core will generate bus requests on this port.
  master port<PVBus> pvbus_m;

  // The core will generate dtcm requests on this port
  master port<PVBus> dtcm_m;

  // The core will generate dtcm requests on this port
  master port<PVBus> itcm_m;

  // Raising this signal will put the core into reset mode.
  slave port<Signal> rst;

  // This signal drives the CPU's interrupt handling.
  slave port<Signal> irq;

  // This signal drives the CPU's fast-interrupt handling.
  slave port<Signal> fiq;

  // This port should be connected to one of the two ticks ports
  // on a 'visualisation' component, in order to display a running
  // instruction count.
  master port<InstructionCount> ticks;

  // Sub components
  composition {
    core : ARM926CT(BIGENDINIT = BIGENDINIT, VINITHI = VINITHI, INITRAM = INITRAM, 
                    itcm0_size = itcm0_int_size,
                    dtcm0_size = dtcm0_int_size);
    decoder : PVBusSlave;
    bus_master : PVBusMaster; // main bus 
    itcm_master : PVBusMaster;
    dtcm_master : PVBusMaster;
  }

  debug {
    core.import;
  }

  connection {
    self.clk_in => core.clk_in;
    self.rst => core.reset;
    self.irq => core.irq;
    self.fiq => core.fiq;

    core.pvbus_m => decoder.pvbus_s;
    bus_master.pvbus_m => self.pvbus_m;
    itcm_master.pvbus_m => self.itcm_m;
    dtcm_master.pvbus_m => self.dtcm_m;

    decoder.device => self.device;
  }

  behavior reset(int level) {
    composition.reset(level);
  }

  behavior init() {
    composition.init();
    tg_bus = bus_master.control.createTransactionGenerator();
    tg_itcm = itcm_master.control.createTransactionGenerator();
    tg_dtcm = dtcm_master.control.createTransactionGenerator();
  }

  behavior terminate() {
    composition.terminate();
    delete tg_bus;
    delete tg_itcm;
    delete tg_dtcm;
  }

  // Implement the decoder to direct the 926 traffic to one of the
  // main BUS, the ITCM or DTCM ports.
  internal slave port<PVDevice> device {
    // Handle a read request to the TCM/AHB ports
    //--------------------------------------------------------------------------------
    behaviour read(pv::ReadTransaction tx) : pv::Tx_Result {
      // Select the port for the read
      pv::TransactionGenerator* tg;
      char* region = "";
      uint64_t addr;
      if (isExtITCMAccess(tx.getAddress())) {
        tg = tg_itcm;
        region = "ITCM";
        addr = tx.getAddress() - itcm0_base;
      } else if (isExtDTCMAccess(tx.getAddress())) {
        tg = tg_dtcm;
        region = "DTCM";
        addr = tx.getAddress() - dtcm0_base;
      } else {
        tg = tg_bus;
        region = "BUS";
        addr = tx.getAddress();
      }

      // Perform the read by using the transaction generator chosen
      uint32_t data[1] = {0};
      tg->setPrivileged(tx.isPrivileged());
      tg->setInstruction(tx.isInstruction());
      tg->setNonSecure(tx.isNonSecure());
      tg->setLocked(tx.isLocked());
      tg->setExclusive(tx.isExclusive());
      ARM926CTExtTCM_DBG("ARM926: Read %s: addr: 0x%08llx, width: %d\n",
                         region, addr, (uint32_t)tx.getAccessWidth());
      bool success = tg->read(addr, tx.getAccessWidth(), &data[0]);
      ARM926CTExtTCM_DBG("ARM926: Read %s: data: 0x%08x, result = %d\n", region, data[0], (uint32_t)success);

      // Process the results
      switch(tx.getAccessWidth()) {
        case pv::ACCESS_32_BITS: tx.setReturnData32(data[0]); break;
        case pv::ACCESS_16_BITS: tx.setReturnData16(data[0]); break;
        case pv::ACCESS_8_BITS: tx.setReturnData8(data[0]); break;
        default: assert(!"Invalid access width"); break;
      }
      return success ? tx.readComplete() : tx.generateAbort();
    } // behaviour read

    // Handle a write request to AHB/TCM busses
    //--------------------------------------------------------------------------------
    behaviour write(pv::WriteTransaction tx) : pv::Tx_Result {
      // Convert the data to an array of unit32's
      uint32_t data[1];
      switch(tx.getAccessWidth()) {
        case pv::ACCESS_32_BITS: data[0] = tx.getData32(); break;
        case pv::ACCESS_16_BITS: data[0] = tx.getData16(); break;
        case pv::ACCESS_8_BITS: data[0] = tx.getData8(); break;
        default: assert(!"Invalid access width"); break;
      }

      // Select the port for the write
      pv::TransactionGenerator* tg;
      char* region = "";
      uint64_t addr;
      if (isExtITCMAccess(tx.getAddress())) {
        tg = tg_itcm;
        region = "ITCM";
        addr = tx.getAddress() - itcm0_base;
      } else if (isExtDTCMAccess(tx.getAddress())) {
        tg = tg_dtcm;
        region = "DTCM";
        addr = tx.getAddress() - dtcm0_base;
      } else {
        tg = tg_bus;
        region = "BUS";
        addr = tx.getAddress();
      }

      // Perform the write operation
      tg->setPrivileged(tx.isPrivileged());
      tg->setInstruction(tx.isInstruction());
      tg->setNonSecure(tx.isNonSecure());
      tg->setLocked(tx.isLocked());
      tg->setExclusive(tx.isExclusive());
      ARM926CTExtTCM_DBG("ARM926: Write %s: addr: 0x%08llx, width: %d, data: 0x%08x\n",
                         region, addr, (uint32_t)tx.getAccessWidth(), data[0]);
      bool success = tg->write(addr, tx.getAccessWidth(), &data[0]);
      ARM926CTExtTCM_DBG("ARM926: Write %s: result = %d\n", region, (uint32_t)success);
      return success ? tx.writeComplete() : tx.generateAbort();
    } // behaviour write

	behaviour debugRead(pv::ReadTransaction tx) : pv::Tx_Result {
		return device.read(tx);
    }
    
    behaviour debugWrite(pv::WriteTransaction tx) : pv::Tx_Result {
        return device.write(tx);
    }

  } // internal slave port<PVBusDevice> device
    
  behavior isExtITCMAccess(uint32_t addr) : bool {
    return ((itcm0_ext_size > 0) && (addr >= itcm0_base) && (addr < itcm0_base + (itcm0_ext_size * 1024)));
  }

  behavior isExtDTCMAccess(uint32_t addr) : bool {
    return ((dtcm0_ext_size > 0) && (addr >= dtcm0_base) && (addr < dtcm0_base + (dtcm0_ext_size * 1024)));
  }

} // component ARM926CTExtTCM


    
