// -*-c++-*-
/******************************************************************************
 Copyright (c) 2011-2012 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
 OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
 DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
 OF CARBON DESIGN SYSTEMS, INC.
******************************************************************************/

#include "common/BreakpointEvents.h"
#include "common/ExitCodes.h"
#include "common/MiscEvents.h"
#include "eslapi/CASIModuleIF.h"
#include "eslapi/CADI.h"
#include "eventchannel/SoCDEventChannel.h"
#include "logger/BladeLogger.h"
#include "MxBreakPoint.h"
#include "MxSimAPI.h"
#include "RunToDebugComponentMgr.h"
#include "SimulationManager.h"

#include "boost/chrono.hpp"
#include "boost/filesystem.hpp"
#include "boost/lexical_cast.hpp"
#include "boost/range/algorithm.hpp"
#ifdef WIN32
#include "boost/thread.hpp"
#endif

#include <stdexcept>
#include <vector>

using namespace std;
using namespace Carbon::Blade;
using namespace Carbon::Blade::Logger;
using namespace Carbon::SoCDEvents;
using namespace eslapi;

class SimulationManager::BPModeChange : public CADICallbackObj
{
public:
  BPModeChange(SimulationManager *simMgr, CADI *cadi)
      : mSimMgr(simMgr), mEventProcessor(&simMgr->mEventProcessor), mCADI(cadi)
  {
  }

  ~BPModeChange()
  {
  }

  virtual eslapi::CAInterface *ObtainInterface(eslapi::if_name_t ifName,
                                               eslapi::if_rev_t minRev,
                                               eslapi::if_rev_t *actualRev)
  {
    return this;
  }

  virtual uint32_t appliOpen(const char *sFileName, const char *mode)
  {
    return 0;
  }

  virtual void appliInput(uint32_t streamId, uint32_t count,
                          uint32_t *actualCount, char *buffer)
  {
  }

  virtual void appliOutput(uint32_t streamId, uint32_t count,
                           uint32_t *actualCount, const char *buffer)
  {
  }

  virtual uint32_t appliClose(uint32_t streamID)
  {
    return 0;
  }

  virtual void doString(const char *stringArg)
  {
  }

  virtual void modeChange(uint32_t newMode, eslapi::CADIBptNumber_t bptNumber)
  {
    if (newMode == CADI_EXECMODE_Bpt)
    {
      // This is the raw breakpoint reported by the model, and its
      // breakpoint number is local to that model.  However,
      // breakpoints added with
      // SimulationManager::addDisassemblyBreakpoint() go through a
      // central SoCD object that assigns globally-unique breakpoint
      // numbers.  We need to look up that number for reporting so
      // that the code that set the breakpoints can associate them
      // correctly.
      CADIBptNumber_t globalNum =
          MxBreakPointManager::getBreakPointManager()->getGlobalUniqueID(
              mCADI, bptNumber);

      MxDisassemblyBreakPointItem *item =
          dynamic_cast<MxDisassemblyBreakPointItem *>(
              MxBreakPointManager::getBreakPointManager()->getBreakPoint(
                  globalNum));

      uint64_t addr = item->getAddress();

      // Create an event for the breakpoint and report it.
      BreakpointEvent *bpEvent = new BreakpointEvent(globalNum, addr);
      bpEvent->setCPU(mSimMgr->getCpuFromCadi(item->getCADI()));

      mEventProcessor->addEvent(bpEvent);
      // Set the breakpoint flag in the simulation manager so
      // simulation continues
      mSimMgr->setBreakpointHit();
    }
  }

  virtual void reset(uint32_t resetLevel)
  {
  }

  virtual void cycleTick(void)
  {
  }

  virtual void killInterface(void)
  {
  }

  virtual uint32_t bypass(uint32_t commandLength, const char *command,
                          uint32_t maxResponseLength, char *response)
  {
    return 0;
  }

  virtual uint32_t lookupSymbol(uint32_t symbolLength, const char *symbol,
                                uint32_t maxResponseLength, char *response)
  {
    return 0;
  }

  virtual void refresh(uint32_t refreshReason)
  {
  }

private:
  SimulationManager *mSimMgr;
  SimulationManager::EventProcessor *mEventProcessor;
  CADI *mCADI;
};

SimulationManager::SimulationManager(const std::string &maxlib,
                                     const std::vector<CpuInfo> &cpus)
    : mEventProcessor(this),
      mSystemOpen(false),
      mBreakpointHit(false),
      mRequestContinue(false),
      mRequestTerminate(false),
      mCommandTrace("command_trace.mxscr"),
      mCpus(cpus)
{
  // Set up the simulator and load the maxlib file
  mSimulator = getMxSimAPI();

  mCommandTrace << "// initialize" << endl;
  mSimulator->initialize();

  mCommandTrace << "addMaxlib(\"" << maxlib << "\");" << endl;
  mSimulator->addMaxlib(maxlib);

  /* RTOEs (which is what blades is built on) don't seem to
  ** load maxlib from user preferences.  But, if we don't pick
  ** up all of the protocols, then we won't bus tracers.
  ** We're just going to force the load of the full protocol bundle
  ** that is associated with this SoCD
  */
  const char *maxsimHome = getenv("MAXSIM_HOME");
  if (maxsimHome)
  {
    string maxsimProtocolsConf = maxsimHome;
    maxsimProtocolsConf = maxsimProtocolsConf + "/Protocols/etc/Protocols.conf";

    mCommandTrace << "addMaxlib(\"" << maxsimProtocolsConf << "\");" << endl;
    mSimulator->addMaxlib(maxsimProtocolsConf);
  }
  else
  {
    // Raise an error message
  }

  mRunner = new BlockingRunner(mSimulator, this);

  // We can't register the cycle listener yet.  That requires the
  // scheduler to be created, which doesn't happen until a system is
  // loaded.
}

SimulationManager::~SimulationManager()
{
  closeSystem();
  delete mRunner;
}

void SimulationManager::addCallback(Callback *cb)
{
  mCallbacks.push_back(cb);
}

void SimulationManager::removeCallback(Callback *cb)
{
  for (CallbackVec::iterator iter = mCallbacks.begin();
       iter != mCallbacks.end(); ++iter)
  {
    if (cb == *iter)
    {
      mCallbacks.erase(iter);
      break;
    }
  }
}



//ostream& operator<<(ostream& os, const MxBreakPointItem& bp)
//{
//  
//}
//
//std::string SimulationManager::dump()
//{
//  // -- FIXME - not yet a dump of the full object
//  ostringstream oss;
//  oss << "Simulation Manager\n"
//      << "  Breakpoints:\n";
//
//  vector<MxBreakPointItem *> *breaks =
//      MxBreakPointManager::getBreakPointManager()->getBreakPointList();
//
//  for (vector<MxBreakPointItem *>::iterator it = breaks->begin();
//       it != breaks->end(); ++it)
//  {
//    oss << "\n    " << **it;
//  }
//
//}

void SimulationManager::clearBreakpoints()
{
   MxBreakPointManager::getBreakPointManager()->clearAllBreakPoints();
}

void SimulationManager::clearCallbacks()
{
  mCallbacks.clear();
}

// The Bladerunner ApplicationsXML File
void SimulationManager::setBladeRunnerMode(bool value)
{
  mSimulator->setBladeRunnerMode(value);
}
void
SimulationManager::setBladeRunnerApplicationsFile(const std::string &xmlFile)
{
  mSimulator->setBladeRunnerApplicationsFile(xmlFile);
}

void SimulationManager::setAppFile(const std::string &instance,
                                   const std::string &file)
{
  if (!boost::filesystem::exists(file))
  {
    LOG_ERR << BOOST_CURRENT_FUNCTION << "Application file '" << file
            << "'' does not exist";
    throw std::runtime_error("Application file " + file + " does not exist");
  }
  else
  {
    mCommandTrace << "setAppFile(\"" << instance << "\", \"" << file << "\");"
                  << endl;
    mSimulator->setAppFile(instance, file);
  }
}

void SimulationManager::openRTOESystem(const std::string &filename)
{
  closeSystem();
  mCommandTrace << "openSystem(\"" << filename << "\", 1);" << endl;

  mSimulator->openRTOESystem(filename);
  // Now that we've opened the system, we can register the listeners.
  mSimulator->registerCycleListener(&mEventProcessor);
  mSimulator->registerStatusListener(mRunner);
  mSystemOpen = true;
}

void SimulationManager::runBlocking()
{
  mRunner->reset();
  resetFlags();

  // Sim should be loaded by now...
  for(vector<CpuInfo>::iterator it = mCpus.begin(); it != mCpus.end(); ++it)
  {
    it->cadi = getCADI(it->instanceName);
  }


  // We need to know if any of the callbacks did anything to trigger
  // the scheduler to start simulation.  So that we don't have
  // competing requests, which could cause threading problems (one
  // request might complete before the next is received), only one
  // callback is allowed to do this.
  bool simStarted = false;
  for (CallbackVec::iterator iter = mCallbacks.begin();
       iter != mCallbacks.end(); ++iter)
  {
    Callback *cb = *iter;
    if (cb->simulationStart())
    {
      if (simStarted)
      {
        throw std::runtime_error("SimulationManger::runBlocking(): Multiple "
                                 "simulationStart() callbacks returned true");
      }
      simStarted = true;
    }
  }

  // It's permissible for a simulation start callback to request
  // termination, e.g. for an error condition.
  if (!mRequestTerminate)
  {
    // Tell the simulator to run.  We may have to do this multiple
    // times, since breakpoints could be hit.  After a breakpoint, we
    // want to continue, as well as if there was an explicit external
    // request.  It's only when we stop without hitting a breakpoint
    // (semihost exit) that we're finished.
    do
    {
      resetFlags();
      mCommandTrace << "run();" << endl;

      mRunner->run(simStarted);
      // Stopped callbacks may also trigger the scheduler to start simulation
      simStarted = false;
      for (CallbackVec::iterator iter = mCallbacks.begin();
           iter != mCallbacks.end(); ++iter)
      {
        Callback *cb = *iter;
        if (cb->simulationStopped())
        {
          if (simStarted)
          {
            throw std::runtime_error("SimulationManger::runBlocking(): "
                                     "Multiple simulationStopped() callbacks "
                                     "returned true");
          }
          simStarted = true;
        }
      }
    } while (!mRequestTerminate); // && (mBreakpointHit || mRequestContinue));
  }

  for (CallbackVec::iterator iter = mCallbacks.begin();
       iter != mCallbacks.end(); ++iter)
  {
    Callback *cb = *iter;
    cb->simulationEnd();
  }
}

void SimulationManager::closeSystem()
{
  if (mSystemOpen)
  {
    mCommandTrace << "closeSystem" << endl;
    mSimulator->closeSystem();

    // Clean up any CADI callbacks we registered.  They'll need to be
    // recreated when the next system is opened.
    for (CADICallbackMap::iterator iter = mCADICallbacks.begin();
         iter != mCADICallbacks.end(); ++iter)
    {
      delete iter->second;
    }
    mCADICallbacks.clear();

    // We also need to clear the breakpoint manager, which uses
    // component-local breakpoint numbers and their CADI pointers to
    // map to globally-unique numbers.  The CADI pointers might be
    // reused by future simulations.
    MxBreakPointManager::getBreakPointManager()->clearBreakPoints();
  }
  mSystemOpen = false;
}

void SimulationManager::saveSimulation(const std::string &filename,
                                       bool withState)
{
  mCommandTrace << "// Save simulation " << filename << ' ' << withState
                << endl;
  mSimulator->saveSimulation(filename, withState);
}

void SimulationManager::createCheckpoint(const std::string &filename)
{
  // Use empty workspace and description.  We'll manage the files
  // ourself so we don't need the grouping of the checkpoint manager.
  mCommandTrace << "// save checkpoint " << filename << endl;

  mSimulator->saveCheckPoint("", filename, "");
}

void SimulationManager::restoreCheckpoint(const std::string &filename)
{
  mCommandTrace << "// restoreCheckpoint " << filename << endl;

  mSimulator->openCheckPoint(filename);
}

CASIModuleIF *SimulationManager::getComponent(const std::string &instance)
{
  // The instance passed in is relative to the top of the system, to
  // be in line with other SoCD conventions (e.g. MxScript).  However,
  // we need to find the top of the system (as opposed to the top of
  // the simulation) before looking it up.
  CASIModuleIF *simTop = mSimulator->getTopComponent();
  // The top component in the simulation is actually an instance of
  // the MxSimulator.  Below that are two children - the system clock
  // ("sysclk") and the actual top of the system.
  const std::vector<CASIModuleIF *> vec = simTop->getSubcomponentList();
  // Check that our assumptions are correct.
  if (vec.size() != 2)
  {
    throw std::runtime_error("Simulator CASIModuleIF hierarchy is incorrect");
  }
  CASIModuleIF *sysTop = vec[1];
  if (sysTop->getInstanceName() == "sysclk")
  {
    // Got the wrong one
    sysTop = vec[0];
  }
  CASIModuleIF *comp = sysTop->getSubcomponent(instance);
  return comp;
}

void SimulationManager::ignoreNextRunningStoppedPair()
{
  mRunner->ignoreNextRunningStoppedPair();
}

bool SimulationManager::isSchedulerHalted() const
{
  return (mSimulator->getSchedulerState() == MX_HALTED);
}



bool matchesCadi(const CpuInfo& info, const CADI* cadi)
{
  return info.cadi == cadi;
}

int SimulationManager::getCpuFromCadi(CADI* cadi)
{
  vector<CpuInfo>::iterator it =
      boost::find_if(mCpus, bind(matchesCadi, placeholders::_1, cadi));

  return distance(mCpus.begin(), it);
}

CADI *SimulationManager::getCADI(const std::string &instance)
{
  CASIModuleIF *comp = getComponent(instance);
  CADI *cadi = NULL;
  if (comp != NULL)
  {
    cadi = comp->getCADI();
  }
  if (cadi != NULL)
  {
    // Create and register a callback to respond to mode changes, if
    // we don't already have one.  This is required to detect when
    // breakpoints occur.  These need to be communicated to the
    // simulation manager so that the simulation continues after a
    // breakpoint is hit.
    if (mCADICallbacks.count(cadi) == 0)
    {
      BPModeChange *modeChange = new BPModeChange(this, cadi);
      char enable[CADI_CB_Count] = { 0 };
      enable[CADI_CB_ModeChange] = 1;
      cadi->CADIXfaceAddCallback(modeChange, enable);
      mCADICallbacks[cadi] = modeChange;
    }
  }
  return cadi;
}

void SimulationManager::simHalt()
{
  // Any explicit halt request should stop the sim, regardless of
  // whether it coincides with a breakpoint.
  requestTerminate();
  mCommandTrace << "// simHalt " << endl;

  mSimulator->simHalt();
  LOG_DBG
  << "SimulationManager::simHalt Requested simulation halt and terminate";
}

bool SimulationManager::terminatePending()
{
  return mRequestTerminate;
}

uint64_t SimulationManager::getCycleCount()
{
  return mSimulator->getCycleCount();
}

bool SimulationManager::callPluginScriptCommand(const std::string &command)
{
  mCommandTrace << "// plugin command " << command << endl;
  return mSimulator->callPluginScriptCommand(command);
}

void SimulationManager::enableProfiling(const std::string &instance,
                                        const std::string &stream,
                                        const std::string &filename,
                                        int compressionFactor)
{
  // MxSimAPI::enableProfiling() returns an int, but I don't think
  // it's valid.  The return value is just forwarded from MxScriptAPI,
  // which returns an MxReturn_t.  That's a custom class that doesn't
  // implement a cast operator to int, so who knows what the actual
  // return value would be.  Just ignore it.
  mCommandTrace << "// enable profileing " << instance << ' ' << stream << ' '
                << filename << ' ' << dec << compressionFactor << endl;
  mSimulator->enableProfiling(instance, stream, filename, compressionFactor);
}

void SimulationManager::loadMemoryFromFile(const std::string &filename,
                                           bool ascii,
                                           const std::string &instance,
                                           const std::string &memspace,
                                           uint64_t startAddr, uint64_t endAddr)
{
  mCommandTrace << "// LoadMemoryFromFile " << filename << ' ' << ascii << ' '
                << instance << ' ' << memspace << hex << ' ' << startAddr << ' '
                << endAddr << endl;

  mSimulator->loadMemoryFromFile(filename, ascii, instance, memspace, startAddr,
                                 endAddr);
}

bool
SimulationManager::addDisassemblyBreakpoint(const std::string &instance,
                                            uint64_t address, int memorySpace,
                                            eslapi::CADIBptNumber_t *bptNum)
{
  // Ugh, I'll never understand the nuances of SoCD.  If the simulator
  // is currently halted, this ends up calling
  // Scheduler::fakeHaltToRefreshDebuggers(), which triggers a
  // simulator status change of "stopped".  We don't want the
  // SimulationManager to see that, because it's not really a state
  // change.  Code that is waiting for a real stop event will be
  // triggered incorrectly.
  //
  // Tell the runner to block it completely.  This is
  // different from ignoring run/stop events - in that case the event
  // is still reported.
  if (isSchedulerHalted())
  {
    mRunner->blockNextModeChange();
  }

  bool ret =
      mSimulator->addDisassemblyBreak(instance, address, memorySpace, bptNum);

  mCommandTrace << "int bp" << dec << *bptNum << " = bpAdd(\"" << instance
                << "\", 0x" << hex << address << ");" << endl;

  return ret;
}

#if 0
SimulationManager::addDisassemblyBreakpoint(
                                            uint64_t address, int memorySpace,
                                            eslapi::CADIBptNumber_t *bptNum)
{
}
#endif

bool SimulationManager::removeBreakpoint(eslapi::CADIBptNumber_t bpNum)
{
  mCommandTrace << "bpRemove(bp" << dec << bpNum << ");" << endl;
  return mSimulator->bpRemove(bpNum);
}

void SimulationManager::runToDebuggablePoint(const std::string &instance)
{
  // Ugh.  The MxScriptAPI version of this doesn't just forward the
  // call to MxSimAPI.  This is essentially a duplication.
  CASIModuleIF *comp = getComponent(instance);
  if (comp == NULL)
  {
    throw std::runtime_error("Instance " + instance +
                             " can't be found for runToDebuggablePoint()");
  }
  RunToDebugComponentMgr *debugMgr =
      RunToDebugComponentMgr::getRunToDebugCompMgr();
  // If we're not already running to debug or at a debuggable point,
  // initiate it.
  if (!(debugMgr->processingRunToDebug()) && !(debugMgr->getDebugState(comp)))
  {
    // The simulation manager is callback-driven, so it's likely this
    // is called from within the simulation thread.  We need to call
    // the non-blocking version of this to allow the simulation to
    // proceed.
    mCommandTrace << "// run to debug " << instance << endl;

    debugMgr->initiateRunToDebug(comp, false);
  }
}

bool SimulationManager::atDebuggablePoint(const std::string &instance)
{
  // Ugh.  The MxScriptAPI version of this doesn't just forward the
  // call to MxSimAPI.  This is essentially a duplication.
  CASIModuleIF *comp = getComponent(instance);
  if (comp == NULL)
  {
    throw std::runtime_error("Instance " + instance +
                             " can't be found for runToDebuggablePoint()");
  }
  RunToDebugComponentMgr *debugMgr =
      RunToDebugComponentMgr::getRunToDebugCompMgr();
  return debugMgr->getDebugState(comp);
}

void SimulationManager::getClockPeriod(Timescale::Value *value,
                                       Timescale::Unit *unit)
{
  MxTimeUnit mxUnit;
  *value = mSimulator->getClockPeriod(mxUnit);

  // Convert to our type
  Timescale::Unit::Enumeration e;
  switch (mxUnit)
  {
  case MX_TIME_FS:
    e = Timescale::Unit::fs;
    break;
  case MX_TIME_PS:
    e = Timescale::Unit::ps;
    break;
  case MX_TIME_NS:
    e = Timescale::Unit::ns;
    break;
  case MX_TIME_US:
    e = Timescale::Unit::us;
    break;
  case MX_TIME_MS:
    e = Timescale::Unit::ms;
    break;
  case MX_TIME_S:
    e = Timescale::Unit::sec;
    break;
  default:
    throw std::runtime_error("Unexpected MxTimeUnit" +
                             boost::lexical_cast<std::string>(mxUnit));
  }
  unit->setEnumeration(e);
}

void SimulationManager::reportEvent(const Event *event)
{
  // Delegate to callbacks
  for (CallbackVec::iterator iter = mCallbacks.begin();
       iter != mCallbacks.end(); ++iter)
  {
    Callback *cb = *iter;
    cb->eventSeen(event);
  }
}

void SimulationManager::resetFlags()
{
  mBreakpointHit = false;
  mRequestContinue = false;
  mRequestTerminate = false;
}

SimulationManager::EventProcessor::EventProcessor(SimulationManager *simMgr)
    : mSimMgr(simMgr)
{
  // Create the channel object for receiving events from the models
  mEventChannel = EventChannel::sInstance("BladeModelEvents");
}

SimulationManager::EventProcessor::~EventProcessor()
{
  EventChannel::sCleanup("BladeModelEvents");
}

void SimulationManager::EventProcessor::cycleFinishedCB()
{
  // Report all queued events (external and local) to the simulation
  // manager for dispatch.
  const Event *event;
  while ((event = mEventChannel->get()) != NULL)
  {
    try
    {
      mSimMgr->reportEvent(event);
    }
    catch (std::exception &e)
    {
      LOG_ERR << "Caught an exception: " << e.what();
      delete event;
      throw;
    }
    catch (...)
    {
      LOG_ERR << "Caught an unknown exception. Deleting event and rethrow.";
      delete event;
      throw;
    }
    delete event;
  }
  while (!mLocalEvents.empty())
  {
    event = mLocalEvents.front();
    mLocalEvents.pop_front();
    try
    {
      mSimMgr->reportEvent(event);
    }
    catch (std::exception &e)
    {
      LOG_ERR << "Caught an exception: " << e.what();
      delete event;
      throw;
    }
    catch (...)
    {
      LOG_ERR << "Caught an unknown exception. Deleting event and rethrow.";
      delete event;
      throw;
    }

    delete event;
  }
  // Also report an end-of-cycle event
  EndOfCycleEvent eocEvent(mSimMgr->getCycleCount());
  mSimMgr->reportEvent(&eocEvent);
}

void SimulationManager::EventProcessor::addEvent(const Event *event)
{
  mLocalEvents.push_back(event);
}

SimulationManager::BlockingRunner::BlockingRunner(MxSimAPI *simAPI,
                                                  SimulationManager *simMgr)
    : mSimAPI(simAPI), mSimMgr(simMgr)
{
  reset();
}

void SimulationManager::BlockingRunner::reset()
{
  mSawRun = false;
  mSawStop = false;
  mIgnoreNextRunning = false;
  mIgnoreNextStopped = false;
  mBlockNextModeChange = false;
}

SimulationManager::BlockingRunner::~BlockingRunner()
{
}

void SimulationManager::BlockingRunner::simulatorStatusChanged(int running)
{
  if (mBlockNextModeChange)
  {
    mBlockNextModeChange = false;
    return;
  }

  // Report a status event.  Do this before updating the flags to
  // guarantee the events are processed before run() exits.
  SimulatorStatusEvent event(running);
  mSimMgr->reportEvent(&event);
  if (running)
  {
    if (!mIgnoreNextRunning)
    {
      mSawRun = true;
    }
    mIgnoreNextRunning = false;
  }
  else
  {
    if (!mIgnoreNextStopped)
    {
      mSawStop = true;
    }
    mIgnoreNextStopped = false;
  }
}

void SimulationManager::BlockingRunner::run(bool simRunning)
{
  // Tell the simulation to run, then wait until we see both a start
  // and a stop.  If it's already running, just wait.  This assumes
  // that the run/stop flags were correctly initialized before the
  // external start was triggered, because it's possible that the
  // simulatorStatusChanged() callback was already called to update
  // them.
  if (!simRunning)
  {
    mSawRun = mSawStop = false;
    // mCommandTrace << "// simRun (again?)" << endl;
    mSimAPI->simRun();
  }
  // Wait for both flags, as well as for the scheduler itself to be
  // halted.  This ensures that we don't return in the small window
  // after the stop mode change but before the scheduler completely
  // stops.
  while (!(mSawRun && mSawStop && mSimMgr->isSchedulerHalted()))
  {
#ifdef WIN32
// The Win32 code should work everywhere, but I don't want to change
// linux yet out of paranoia of breaking something!
// boost::chrono::microseconds span(1000);
// boost::this_thread::sleep(span);
#else
    usleep(1000);
#endif
  }

  // At this point, we know the simulation has stopped.  Clear the
  // flags in case some external function (e.g. run-to-debug) starts
  // the simulation running again.
  mSawRun = mSawStop = false;
}

void SimulationManager::BlockingRunner::ignoreNextRunningStoppedPair()
{
  mIgnoreNextRunning = true;
  mIgnoreNextStopped = true;
}
