module clocks (default_clk, offset_clk, slow_clk, fast_clk, short_clk, long_clk, reset_1, reset_2);
   input default_clk, offset_clk, slow_clk, fast_clk, short_clk, long_clk, reset_1, reset_2; // carbon observeSignal

   always @ (posedge default_clk) $display("%t default_clk %d", $time, default_clk);
   always @ (posedge offset_clk) $display("%t offset_clk %d", $time, offset_clk);
   always @ (posedge slow_clk) $display("%t slow_clk %d", $time, slow_clk);
   always @ (posedge fast_clk) $display("%t fast_clk %d", $time, fast_clk);
   always @ (posedge short_clk) $display("%t short_clk %d", $time, short_clk);
   always @ (posedge long_clk) $display("%t long_clk %d", $time, long_clk);

   always @ (posedge reset_1) $display("%t reset_1 %d", $time, reset_1);
   always @ (posedge reset_2) $display("%t reset_2 %d", $time, reset_2);
   
endmodule
