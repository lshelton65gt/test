/*****************************************************************************

 Copyright (c) 2007 by Carbon Design Systems, Inc., All Rights Reserved.

 THIS SOFTWARE CONTAINS CONFIDENTIAL INFORMATION AND TRADE SECRETS OF CARBON 
 DESIGN SYSTEMS, INC.  PERMISSION IS HEREBY GRANTED TO USE, MODIFY, AND/OR COPY
 THIS SOFTWARE FOR INTERNAL USE ONLY PROVIDED THAT THE ABOVE COPYRIGHT NOTICE 
 APPEARS IN ALL COPIES OF THIS SOFTWARE.

******************************************************************************/
//
// Example for DDR SDRAM Model
//

#include "libdesign.h"   /* generated header file */
#include "xactors/xactors.h"
#include <cassert>
#include <iostream>
#include <iomanip>

#define SIM_MAX_TIME 100000L

// Test Function
void master_func(CarbonMemoryID* mem, int *myError);

// Utility functions
CarbonTime run(CarbonObjectID* model, CarbonNetID* clock, CarbonTime& time);
bool DataCompare(int& myError, CarbonUInt64 dataR, CarbonUInt64 dataX);
bool DataCompare(int& myError, CarbonUInt64 dataR, CarbonUInt64 dataX, CarbonUInt32 addr);
void passfail(int myError);
void frontdoorWrite(CarbonUInt32 ba, CarbonUInt32 ra, CarbonUInt32 ca, CarbonUInt64 data);
CarbonUInt64 frontdoorRead(CarbonUInt32 ba, CarbonUInt32 ra, CarbonUInt32 ca);
void backdoorWrite(CarbonMemoryID* mem, CarbonUInt32 ba, CarbonUInt32 ra, CarbonUInt32 ca, CarbonUInt64 data);
CarbonUInt64 backdoorRead(CarbonMemoryID* mem, CarbonUInt32 ba, CarbonUInt32 ra, CarbonUInt32 ca);

// Main function
/* Sets up the Carbon Model, initializes the transactors and drives 
   clock and reset during the simulation.
   Also takes care of reporting test result at the end of the test.
*/
int main() 
{
  const CarbonUInt32 ZERO = 0;
  const CarbonUInt32 ONE  = 1;

  std::cout << "****** DDR SDRAM Simulation Starting ******" << std::endl;

  // Create Sim Infrastructure.  Master, Clocks, and Reset

  // Handle to Carbon Model
  CarbonObjectID* model  = carbon_design_create(eCarbonFullDB, eCarbon_NoFlags);
  
  // Make Sure that model creation was successful
  assert(model);
  
  // Find Reset and Clock nets
  CarbonNetID*    clock  = carbonFindNet(model, "ddrTb.DDRCTL_1.ddr_clk");
  CarbonNetID*    resetn = carbonFindNet(model, "ddrTb.reset_n"); 
  
  // Make Sure that the nets were found
  assert(clock && resetn);
  
  const CarbonTime rstTime = 5000;
  std::cout << "Asserting reset_n for " << std::dec << rstTime << " ns" << std::endl;
  carbonDeposit(model, resetn, &ZERO, NULL);
  
  CarbonTime time = 0;
  while(time < rstTime) run(model, clock, time);
      
  std::cout << "main: DEASSERTING reset_n at time " << std::dec << time << " ns" << std::endl;
  carbonDeposit(model, resetn, &ONE, NULL);
  
  // Create Sparse Memory Array for DDR SDRAM
  CarbonMemoryID* mem = carbonFindMemory(model, "ddrTb.DDR_1.ram_array");
 
  // Connect control function for master transactor and DDR SDRAM transactor
  int myError = 0;
  carbonXAttach("DDRCTL_1", (CarbonXAttachFuncT)master_func,                   2, mem, &myError);
  
  // Run Simulation until timeout or master function reports that test is complete
  while ( (time < SIM_MAX_TIME) && !carbonXCheckSignalState(TEST_COMPLETE) )
    run(model, clock, time);
  
  if (time < SIM_MAX_TIME)
    std::cout << "============= main:  TEST_COMPLETE detected ===============" << std::endl;
  else {
    std::cout << "ERROR: Simulation exceeded time limit of " << std::setw(6) << std::dec << SIM_MAX_TIME << std::endl;
    ++myError;
  }
  
  std::cout << "DDR (16Meg x 16) Simulation complete at time: " << time << std::endl;
  
  // Report Test Success/Failure
  passfail(myError);

  // Cleanup
  carbonDestroy(&model);
  
  // Exit with number of errors as exit status
  return myError;
}

// Test function
/* Provides stimuli for the test and checks results.
   Memory handle and pointer to error count is passed from 
   the main function.
*/
void master_func(CarbonMemoryID* mem, int *myError) {
  std::cout << "Starting master_func" << std::endl;
  
  // Need to wait for a while so that the memory model transactor is out of reset
  carbonXIdle(150);

  // Initializing DDR SDRAM
  std::cout << "***\n*** Configuring DDR SDRAM for CAS Latency 2, Burst Length 4\n***" << std::endl;
  carbonXDdrsdramCtrlSetModeReg( (2<<4) | (0<<3) | 2); // CAS LAT 2, Sequential mode, Burst Length 4
  
  // Do a couple of frontdoor writes to different bank, row and column addresses
  frontdoorWrite(0, 0x01, 0x120, 0x1111222233334444LL);
  frontdoorWrite(1, 0x80, 0x044, 0xaabbccddeeff0011LL);
  
  // Read back frontdoor and check the result
  CarbonUInt64 readData = 0;
  readData = frontdoorRead(0, 0x01, 0x120);
  DataCompare(*myError, readData, 0x1111222233334444LL); 
  readData = frontdoorRead(1, 0x80, 0x044);
  DataCompare(*myError, readData, 0xaabbccddeeff0011LL); 
  
  // Read back backdoor and check the result
  readData = backdoorRead(mem, 0, 0x01, 0x120);
  DataCompare(*myError, readData, 0x1111222233334444LL); 
  readData = backdoorRead(mem, 1, 0x80, 0x044);
  DataCompare(*myError, readData, 0xaabbccddeeff0011LL); 

  // Do a couple of backdoor writes to different bank, row and column addresses
  backdoorWrite(mem, 2, 0x13, 0x158, 0xffffeeeeddddccccLL);
  backdoorWrite(mem, 3, 0x90, 0x08c, 0x0011223344556677LL);
  
  // Read back frontdoor and check the result
  readData = frontdoorRead(2, 0x13, 0x158);
  DataCompare(*myError, readData, 0xffffeeeeddddccccLL); 
  readData = frontdoorRead(3, 0x90, 0x08c);
  DataCompare(*myError, readData, 0x0011223344556677LL); 
  
  // Read back backdoor and check the result
  readData = backdoorRead(mem, 2, 0x13, 0x158);
  DataCompare(*myError, readData, 0xffffeeeeddddccccLL); 
  readData = backdoorRead(mem, 3, 0x90, 0x08c);
  DataCompare(*myError, readData, 0x0011223344556677LL); 

  carbonXSetSignal(TEST_COMPLETE);

  std::cout << "master_func:  signaled test complete" << std::endl;
  std::cout << "master_func:  end of function" << std::endl;

}

// Run One Clock Period
/* Utility function to run one clock cycle.
   return Current Time 
 */
CarbonTime run(CarbonObjectID* model, CarbonNetID* clock, CarbonTime& time)
{
  const CarbonUInt32 ZERO = 0;
  const CarbonUInt32 ONE  = 1;
  const CarbonTime   CLOCK_CYCLE   = 10;

  carbonDeposit(model, clock, &ZERO, NULL);
  time += CLOCK_CYCLE / 2;
  carbonSchedule(model, time);
  carbonDeposit(model, clock, &ONE, NULL);
  time += CLOCK_CYCLE / 2;
  carbonSchedule(model, time);

  return time;
}


// Data Compare Function
bool DataCompare(int& myError, CarbonUInt64 dataR, CarbonUInt64 dataX )
{
  if (dataX == dataR) { 
    std::cout << "Received data: " << std::setw(8) << dataR << " EQUALS expected data: " << dataX << std::endl;
    return true;
  }
  else { 
    std::cout << "ERROR: Received data: " << std::setw(8) << dataR << " NOT EQUAL to expected data: " << dataX << std::endl;
    ++myError;
    return false;
  }
}


// Data Compare Function
bool DataCompare(int& myError, CarbonUInt64 dataR, CarbonUInt64 dataX, CarbonUInt32 addr )
{
  if (dataX == dataR) { 
    std::cout << "Addr[0x" << std::hex << addr << "]: " << std::setw(8) << dataR << " EQUALS expected data: " << dataX << std::endl;
    return true;
  }
  else { 
    std::cout << "ERROR: Addr[0x" << std::hex << addr << "]: " << std::setw(8) << dataR << " NOT EQUAL to expected data: " << dataX << std::endl;
    ++myError;
    return false;
  }
}

// Prints Pass or Fail Banner
void passfail(int myError)
{
  std::cout << std::endl << std::endl
	    << "**********" << std::endl;
  if (myError == 0)
    std::cout << "** PASS **" << std::endl;
  else
    std::cout << "** FAIL **   error count: " << myError << std::endl;
  std::cout << "**********" << std::endl;
}                        

// Perform A frontdoor write, using Carbon's example DDR Ctrl Transactor
void frontdoorWrite(CarbonUInt32 ba, CarbonUInt32 ra, CarbonUInt32 ca, CarbonUInt64 data)
{
  std::cout << "Frontdoor write to ba: " << std::hex 
	    << std::setw(2) << ba 
	    << " ra: " << std::setw(4) << ra 
	    << " ca: " << std::setw(4) << ca 
	    << " data: " << std::setw(16) << data << std::endl; 
  CarbonUInt32 addr = (ba << 24) | (ra << 10) | ca;
  carbonXDdrsdramCtrlWrite(addr, data);
}

// Perform A frontdoor read, using Carbon's example DDR Ctrl Transactor
CarbonUInt64 frontdoorRead(CarbonUInt32 ba, CarbonUInt32 ra, CarbonUInt32 ca)
{
  std::cout << "Frontdoor read from ba: " << std::hex 
	    << std::setw(2) << ba 
	    << " ra: " << std::setw(4) << ra 
	    << " ca: " << std::setw(4) << ca << std::endl; 
  CarbonUInt32 addr = (ba << 24) | (ra << 10) | ca;
  return carbonXDdrsdramCtrlRead(addr);
}

// Perform a backdoor write
void backdoorWrite(CarbonMemoryID* mem, CarbonUInt32 ba, CarbonUInt32 ra, CarbonUInt32 ca, CarbonUInt64 data)
{
  std::cout << "Backdoor write to ba: " << std::hex 
	    << std::setw(2) << ba 
	    << " ra: " << std::setw(4) << ra 
	    << " ca: " << std::setw(4) << ca 
	    << " data: " << std::setw(16) << data << std::endl; 
  CarbonUInt32 addr = (ba << 24) | (ra << 10) | ca;
  // Backdoor write one 16bit word at a time, LSB in the lowest address
  for(int i = 0; i < 4; i++) {
    carbonDepositMemoryWord(mem, addr+i, (data >> (16*i)) & 0xffff, 0);
  }
}

// Perform a backdoor read
CarbonUInt64 backdoorRead(CarbonMemoryID* mem, CarbonUInt32 ba, CarbonUInt32 ra, CarbonUInt32 ca)
{
  std::cout << "Backdoor read from ba: " << std::hex 
	    << std::setw(2) << ba 
	    << " ra: " << std::setw(4) << ra 
	    << " ca: " << std::setw(4) << ca << std::endl; 
  CarbonUInt32 addr = (ba << 24) | (ra << 10) | ca;
  CarbonUInt64 readData = 0;
  // Backdoor read one 16bit word at a time, LSB in the lowest address
  for(int i = 0; i < 4; i++) {
    CarbonUInt16 readData16 = carbonExamineMemoryWord(mem, addr+i, 0);
    readData |= (static_cast<CarbonUInt64>(readData16) << (16*i));
  }
  return readData;
}
