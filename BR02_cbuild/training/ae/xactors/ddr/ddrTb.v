`timescale 1ns/100ps

module ddrTb(reset_n);
   
   input reset_n;
   
   //------------------------ ddr_16megx16 ------------ 
   //
   wire [15:0] dq;           // inout
   wire [1:0]  dqs;          // inout
   wire [12:0] addr;         // input to ddr_16megx16           // output from ddr_ctl
   wire [1:0]  ba;           // input to ddr_16megx16           // output from ddr_ctl
   wire        cke;          // input to ddr_16megx16           // output from ddr_ctl
   wire        cs_n;         // input to ddr_16megx16           // output from ddr_ctl
   wire        cas_n;        // input to ddr_16megx16           // output from ddr_ctl
   wire        ras_n;        // input to ddr_16megx16           // output from ddr_ctl
   wire        we_n;         // input to ddr_16megx16           // output from ddr_ctl
   wire [1:0]  dm;           // input to ddr_16megx16           // output from ddr_ctl
   wire        clk;          // input to ddr_16megx16           // output from ddr_ctl
   wire        clk_n = ~clk; // input to ddr_16megx16

   // -----------------------------------------------   
   carbonx_ddrsdram_16megx16 DDR_1 
     ( .dq(dq),
       .dqs(dqs),
       .addr({1'b0, addr}),
       .ba(ba),
       .clk(clk),
       .clk_n(clk_n),
       .cke(cke),
       .cs_n(cs_n),
       .cas_n(cas_n),
       .ras_n(ras_n),
       .we_n(we_n),
       .dm(dm)
       );

   carbonx_ddrsdram_ctrl DDRCTL_1 
     ( .clk(clk),
       .reset_n(reset_n),
       .dq(dq),
       .dqs(dqs), 
       .addr(addr), 
       .ba(ba), 
       .cke(cke), 
       .cs_n(cs_n), 
       .cas_n(cas_n), 
       .ras_n(ras_n), 
       .we_n(we_n), 
       .dm(dm)
       );
   
endmodule
