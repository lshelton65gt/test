#include "APB3_Master.h"

bool APB3_Master::connect(ConnectNameNamePair *nameList, CarbonObjectID *carbonModelHandle)
{
  // Check Parameters
  if (nameList == NULL || carbonModelHandle == NULL)
    return 0;
  
  // LAB Exercise:
  // Create the connection between the transactor and the carbon model.
  // For each transactor port / Carbon model port pair, get the net ID, using
  // the carbonFindNet API and store them in the nNetID array using the INDEX_...
  // macros defined in APB3_Master.h
  // If the connection fail in any way, return false to indicate the failure.
 
  // If we got here we have successfully connected all ports in the list
  return true;
};

