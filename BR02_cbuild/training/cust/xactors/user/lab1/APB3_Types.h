// -*- C++ -*-
#ifndef _APB3_Types_h_
#define _APB3_Types_h_

#include "carbon/carbon_shelltypes.h"

//! Connect transactor signal by name to Carbon Model signal by name
/*!
 * This structure will describe mapping between transactor signal name to Carbon
 * model signal name. This is used with the connect method of the transactor to
 * associate a transactor port with a Carbon model port.
 */
struct ConnectNameNamePair {
  const char *TransactorSignalName;          /*!< Transactor signal name */
  const char *ModelSignalName;               /*!< Carbon model signal name */
};

//! APB3 Transaction Structure
/*!
  The transaction structure keeps all information of needed to perform
  a particular transaction
*/
struct APB3_Transaction {

  //! Transaction Type
  enum Type { Write, Read };
  
  //! Transaction Type
  Type         transType;

  //! Transaction Address
  CarbonUInt32 addr;
  //! Transaction Write Data
  CarbonUInt32 wdata;
  //! Transaction Read Data
  CarbonUInt32 rdata;

  //! Number of waitstates, used only with slave transactor
  CarbonUInt32 waitStates;
};

//! Error Types
/*!
  Enumerates the different Error types of the transaction.
*/
enum APB3_ErrorType {
  APB3_ConnectionError
};

#endif

