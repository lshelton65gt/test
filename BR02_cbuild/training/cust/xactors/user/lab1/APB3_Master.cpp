
#include "APB3_Master.h"
#include <iostream>
#include <sstream>

using namespace std;

APB3_Master::APB3_Master(const char* transactorName,
                         const CarbonUInt32 maxTrans,
                         void (*reportTransaction)(APB3_Transaction*, void*),
                         void (*notify)(APB3_ErrorType, const char*, const APB3_Transaction*, void*),
                         void (*notify2)(APB3_ErrorType, const char*, void*),
                         void *userInstance) :
  mXtorName(transactorName),
  mMaxTrans(maxTrans),
  mReportTransaction(reportTransaction),
  mNotify(notify),
  mNotify2(notify2),
  mUserInstance(userInstance),
  mCarbonModelHandle(0),
  mState(IDLE)  
{
  // Clear Net IDs
  memset(mNetID, 0, sizeof(mNetID));

  // Clear Port Values
  memset(mPortValues, 0, sizeof(mPortValues));
}

void APB3_Master::refreshSignals()
{
  switch (mState) {
  case IDLE :
    // Only drive PSEL and PENABLE, the first idle cycle
    if(mPortValues[INDEX_PENABLE] == 1) {
      drivePort(INDEX_PSEL,    0);
      drivePort(INDEX_PENABLE, 0);
    }
    break;
  case SETUP :
    drivePort(INDEX_PADDR,   mPortValues[INDEX_PADDR]);
    drivePort(INDEX_PWDATA,  mPortValues[INDEX_PWDATA]);
    drivePort(INDEX_PSEL,    1);
    drivePort(INDEX_PENABLE, 0);
    drivePort(INDEX_PWRITE,  mPortValues[INDEX_PWRITE]);
    break;
  case ENABLE :
    drivePort(INDEX_PENABLE, 1);
    break;
  }
}

void APB3_Master::netChangeCB(CarbonObjectID* cid, CarbonNetID* nid, CarbonClientData clientData, 
                              CarbonUInt32* value, CarbonUInt32*)
{
  // clientData points to the portValue storage for the net that changed.
  // This was setup when we registered the callback with the Carbon model.
  CarbonUInt32* portValue = reinterpret_cast<CarbonUInt32*>(clientData);

  // Store the new value
  *portValue = *value;
}
