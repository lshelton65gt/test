#include "APB3_Master.h"

void APB3_Master::evaluate(CarbonTime time)
{
  switch(mState) {
  case IDLE :
    // Check if there are any transactions to start
    if(! mTransQueue.empty() ) {

      // Set signal values, these values will be driven to the Carbon model when the refresh
      // output method is called.
      mPortValues[INDEX_PADDR]   = mTransQueue.front()->addr;
      mPortValues[INDEX_PWDATA]  = mTransQueue.front()->wdata;
      mPortValues[INDEX_PSEL]    = 1;
      mPortValues[INDEX_PENABLE] = 0;
      mPortValues[INDEX_PWRITE]  = (mTransQueue.front()->transType == APB3_Transaction::Write);

      // Go to Setup state
      mState = SETUP;
    }
    break;
    
  case SETUP :
    // Assert Enable
    mPortValues[INDEX_PENABLE] = 1;
    mState = ENABLE;
    break;

  case ENABLE :
    // Check PREADY to see if we should continue
    if(mPortValues[INDEX_PREADY] == 1) {
      
      // If this is a read transaction, capture the read data
      if((mTransQueue.front()->transType == APB3_Transaction::Read))
        mTransQueue.front()->rdata = mPortValues[INDEX_PRDATA];
      
      // Report that the transaction is done
      (*mReportTransaction)(mTransQueue.front(), mUserInstance);

      // Remove the transaction from the queue.
      mTransQueue.pop_front();

      // Now check to see if we should go straight for the next transaction
      if(! mTransQueue.empty() ) {
        
        // Drive Ports to Carbon model.
        mPortValues[INDEX_PADDR]   = mTransQueue.front()->addr;
        mPortValues[INDEX_PWDATA]  = mTransQueue.front()->wdata;
        mPortValues[INDEX_PSEL]    = 1;
        mPortValues[INDEX_PENABLE] = 0;
        mPortValues[INDEX_PWRITE]  = (mTransQueue.front()->transType == APB3_Transaction::Write);
        
        // Go to Setup state
        mState = SETUP;
      }
      
      // If not, return to IDLE
      else {
        // Return to Idle
        mState = IDLE;
      }
    }
    break;

  default :
    mState = IDLE; // Failsafe
  }
}

