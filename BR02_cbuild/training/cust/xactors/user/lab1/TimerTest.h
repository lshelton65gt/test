// -*- C++ -*-
/*
  Testbench and test for the apb_timer Carbon component.

  The test performs a few reads and writes to the scratch pad memory.
  Sets the timer and enables it.
  Waits for the timer interrupt and clears it.
*/
#include <iostream>
#include "TimerTb.h"

class TimerTest : public TimerTb {
public:
  //! Constructor
  TimerTest();

  //! Test function
  void testFunction();
  
  //! Return error count
  CarbonUInt32 getErrors() const
  {
    std::cout << "Test had " << mErrorCount << " errors." << std::endl;
    return mErrorCount;
  }

private:

  //! Interrupt method, will get called by the Carbon model when the interrupt pin changes value 
  void interrupt(CarbonUInt32 value);

  //! Report Transaction Method, should be defined by test
  void reportTransaction(APB3_Transaction* trans);
  
  void write(CarbonUInt32 addr, CarbonUInt32 data);
  void read(CarbonUInt32 addr);

  //! Test state type
  /*!
    Since this test bench doesn't provide threads, we need to keep track of
    the state of the test our self. This state type defined the various states
    in this test.
  */
  enum TestStateT {
    eStart,
    eWriteMem,
    eWaitWriteDone,
    eReadMem,
    eWaitReadDone,
    eSetupTimer,
    eWaitForInterrupt,
    eWaitInterruptClear,
    eTestDone
  };
  
  //! Test state
  TestStateT mTestState;
  
  //! Transaction count
  /*! 
    Used in the wait states to keep track of how many transactions to wait for
  */
  CarbonUInt32 mTransCount;

  //! Used to keep track of received transactions
  APB3_Transaction mLastReceivedTrans;
  bool             mTransReceived;

  //! Value of interrupt pin
  CarbonUInt32     mInterruptVal;

  //! Keep track of errors in the test
  CarbonUInt32     mErrorCount;
};

