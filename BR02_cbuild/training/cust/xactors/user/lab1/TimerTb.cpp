/*
  Testbench apb_timer Carbon component.

  The testbench class is responsible for setting up the Carbon model
  and the transactor and connecting them together. It's also responsible
  for generating the clock for the model and to call the transactors
  evaluate method for every cycle.
  The testbench executes the test by for every cycle calling the
  testFunction virtual method.

  A test class is supposed to inherit from this testbench class to
  run the test.
*/
#include <iostream>
#include "TimerTb.h"

using namespace std;

//! TimerTb constructor
TimerTb::TimerTb() : mCarbonID(0),
                     mClkNetID(0),
                     mRstNetID(0),
                     mMasterTrans(0),
                     mTestDone(false)
{}

//! TimerTb destructor
TimerTb::~TimerTb()
{
  // Delete transactor
  delete mMasterTrans;

  // Destroy Carbon model
  carbonDestroy(&mCarbonID);
}

//! Setup carbon model and transactor
bool TimerTb::setup()
{

  // Setup Carbon model
  mCarbonID = carbon_apb_timer_create(eCarbonFullDB, eCarbon_NoFlags);
  
  // Check that the model was successfully created.
  if(mCarbonID == 0){
    std::cout << "Carbon model creation failed." << std::endl;
    return false;
  }

  // Setup nets for signals that are not connected to the transactor 
  CarbonNetID* intNetID  = carbonFindNet(mCarbonID, "apb_timer.interrupt");
  mClkNetID = carbonFindNet(mCarbonID, "apb_timer.pclk");
  mRstNetID = carbonFindNet(mCarbonID, "apb_timer.reset");
  
  if(intNetID == 0 || mClkNetID == 0 || mRstNetID == 0) {
    std::cout << "Find net failed." << std::endl;
    return false;
  }
  CarbonNetValueCBDataID* cbID = carbonAddNetValueChangeCB(mCarbonID, 
                                                           intMonitorCB, 
                                                           this,
                                                           intNetID);
  // Check that the callback was added successfully
  if(cbID == NULL) {
    std::cout << "Setup for net change callback of net apb_timer.interrupt failed." << std::endl;
    return false;
  }

  // Now Setup the transactor
  mMasterTrans = new APB3_Master("APB Master",        // Transactor Instance name
                                 5,                   // Max number of outstaning transactions 
                                 reportTransactionCB, // Report Transaction callback function
                                 notifyCB,            // Notify Callback function
                                 notify2CB,           // Notify2 Callback function
                                 this                 // User Instance (passed to callback functions)
    );
  
  // Connect the transactor to the Carbon model
  ConnectNameNamePair connectPair[] = { {"PRDATA",  "apb_timer.prdata"  },
                                        {"PREADY",  "apb_timer.pready"  },
                                        {"PADDR",   "apb_timer.paddr"   },
                                        {"PWDATA",  "apb_timer.pwdata"  },
                                        {"PSEL",    "apb_timer.psel"    },
                                        {"PENABLE", "apb_timer.penable" },
                                        {"PWRITE",  "apb_timer.pwrite"  },
                                        0};
  bool success = mMasterTrans->connect(connectPair, mCarbonID);
  
  // Check that connection was successful
  if(!success) {
    std::cout << "APB Master transactor connection failed." << std::endl;
    return false;
  }

  // Setup Wave dumping
  CarbonWaveID* waveID = carbonWaveInitFSDB(mCarbonID, "waves.fsdb", e1ns);
  carbonDumpVars(waveID, 0, "apb_timer");

  // Now if we made it this far we have successfully setup the environment
  return true;
}


//! Evaluate function. Gets called every cycle
bool TimerTb::evaluate(CarbonTime currTime)
{
  // Execute test and transactor
  testFunction();
  
  // Create a negative clock edge
  CarbonUInt32 zero = 0;
  carbonDepositFast(mCarbonID, mClkNetID, &zero, 0);
  carbonSchedule(mCarbonID, currTime);
  
  // Note the timing of the calls here. The transactor signal outputs
  // acts as if they are registered outputs when communicating
  // with the Carbon model. Since the transactor reads the input ports
  // during the evaluate method, the evaluate method should be called before
  // the active edge (positive). However we want to drive the transactors outputs
  // after the clock edge so the model does not act on them prematurely. 
  // Therefore the refreshSignals method should be called after the clock edge
  // is generated.

  // Evaluate transactor before the clock edge.
  mMasterTrans->evaluate(currTime);
  
  // Generate the edge
  CarbonUInt32 one  = 1;
  carbonDepositFast(mCarbonID, mClkNetID, &one, 0);
  carbonSchedule(mCarbonID, currTime+5);
  
  // Drive transactor output signals after the clock edge
  mMasterTrans->refreshSignals();
  carbonSchedule(mCarbonID, currTime+6);

  // We want to continue to evaluate until the test is done
  if(mTestDone) return false;
  else          return true;
}

//! Callback function for Interrupt
void TimerTb::intMonitorCB(CarbonObjectID*, CarbonNetID*, CarbonClientData clientData, CarbonUInt32* value, CarbonUInt32*)
{
  TimerTb* that = reinterpret_cast<TimerTb*>(clientData);
  that->interrupt(*value);
}

//! Report Transaction Callback function
void TimerTb::reportTransactionCB(APB3_Transaction* trans, void* clientData)
{
  TimerTb* that = reinterpret_cast<TimerTb*>(clientData);
  that->reportTransaction(trans);
}
  
//! Notify Callback function
void TimerTb::notifyCB(APB3_ErrorType error, const char* message, const APB3_Transaction* trans, void*)
{
  (void)trans;

  switch(error) {
  default:
    // Just print the error message for all error types. 
    // For more complex protocolls some specific error handling may be needed.
    std::cout << message << std::endl;
  }
}

//! Notify2 Callback function
void TimerTb::notify2CB(APB3_ErrorType error, const char* message, void*)
{
  switch(error) {
  default:
    // Just print the error message for all error types. 
    // For more complex protocolls some specific error handling may be needed.
    std::cout << message << std::endl;
  }
}

  
