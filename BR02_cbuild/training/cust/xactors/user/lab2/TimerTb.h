// -*- C++ -*-
/*
  Testbench apb_timer Carbon component.

  The testbench class is responsible for setting up the Carbon model
  and the transactor and connecting them together. It's also responsible
  for generating the clock for the model and to call the transactors
  evaluate method for every cycle.
  The testbench executes the test by for every cycle calling the
  testFunction virtual method.

  A test class is supposed to inherit from this testbench class to
  run the test.
*/
#include "libapb_timer.h"
#include "APB3_Master.h"

// Register Address defines
#define     APB_TIMER_LOAD    0x0
#define     APB_TIMER_CURRENT 0x4
#define     APB_TIMER_CONTROL 0x8
#define     APB_TIMER_CLEAR   0xC

// Register Field defines
#define     APB_TIMER_CTRL_ENABLE   0x80
#define     APB_TIMER_CTRL_PERIODIC 0x40

//! This is the main testbench class
/*! Sets up the Carbon model and transactor and runs the test.
 */
class TimerTb {
public:
  //! Constructor
  TimerTb();

  //! Destructor
  virtual ~TimerTb();

  //! Sets up the model and the transactor
  bool setup();

  //! Evaluates the test function, transactor and the Carbon model
  bool evaluate(CarbonTime currTime);

  //! Test function
  //! This method needs to be implemented by the test class
  virtual void testFunction()=0;
  
  //! Interrupt Method, should be defined by test
  virtual void interrupt(CarbonUInt32 value)=0;

  //! Report Transaction Method, should be defined by test
  virtual void reportTransaction(APB3_Transaction* trans)=0;

  //! Callback function for Interrupt
  static void intMonitorCB(CarbonObjectID*, CarbonNetID*, CarbonClientData userData, CarbonUInt32* value, CarbonUInt32*);
  
  //! Report Transaction Callback function
  static void reportTransactionCB(APB3_Transaction* trans, void* clientData);
  
  //! Notify Callback function
  static void notifyCB(APB3_ErrorType, const char*, const APB3_Transaction*, void*);

  //! Notify2 Callback function
  static void notify2CB(APB3_ErrorType, const char*, void*);

protected:
  // Member variables
  CarbonObjectID* mCarbonID;
  CarbonNetID*    mClkNetID;
  CarbonNetID*    mRstNetID;
  APB3_Master*    mMasterTrans;
  bool            mTestDone;

};
