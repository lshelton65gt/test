/*
  Test for the apb_timer Carbon component.

  The test performs a few reads and writes to the scratch pad memory.
  Sets the timer and enables it.
  Waits for the timer interrupt and clears it.
*/

#include "TimerTest.h"

using namespace std;

//! Constructor
TimerTest::TimerTest() :
  mTestState(eStart),
  mTransCount(0),
  mInterruptVal(0),
  mErrorCount(0)
{
}

//! Test function
/*!
  The testfunction is setup like a state machine. This is needed because this testbench
  does not provide any threading so we must keep track of our current state ourself.
  Each state is a section of the test. We move to the next state when the testbench
  needs to executed other parts of the environment.
*/
void TimerTest::testFunction()
{
  switch(mTestState) {
  case eStart :
    mTransCount = 0;
    mTestState  = eWriteMem;
    break;
  case eWriteMem :    
    // Create a few write transactions
    write(0x400, 0x1234);
    write(0x404, 0x2345);
    write(0x408, 0x3456);
    
    // Go to wait for writes state
    mTestState = eWaitWriteDone;
    break;
  case eWaitWriteDone :
    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0) mTestState = eReadMem;
    mTransReceived = false; // Clear receive flag
    break;
  case eReadMem :
    // Read the addresses written earlier, and see if they match
    read(0x400);
    read(0x404);
    read(0x408);

    // Go to wait for reades state
    mTestState = eWaitReadDone;
    break;
  case eWaitReadDone :
    // When a read transaction is done, check the result
    if(mTransReceived) {
      switch(mLastReceivedTrans.addr) {
      case 0x400:
        if(mLastReceivedTrans.rdata != 0x1234) {
          cout << "Error: Read data mismatch for addr " << hex << mLastReceivedTrans.addr
               << " Expected: 0x1234 Received: " << mLastReceivedTrans.rdata << endl;
          ++mErrorCount;
        }
        break;
      case 0x404:
        if(mLastReceivedTrans.rdata != 0x2345) {
          cout << "Error: Read data mismatch for addr " << hex << mLastReceivedTrans.addr
               << " Expected: 0x2345 Received: " << mLastReceivedTrans.rdata << endl;
          ++mErrorCount;
        }
        break;
      case 0x408:
        if(mLastReceivedTrans.rdata != 0x3456) {
          cout << "Error: Read data mismatch for addr " << hex << mLastReceivedTrans.addr
               << " Expected: 0x3456 Received: " << mLastReceivedTrans.rdata << endl;
        ++mErrorCount;
        }
        break;
      }
      mTransReceived = false;
    }
    // Wait until transaction count is 0, the report transaction method
    // will decrement the count for every one it receives
    if(mTransCount == 0) mTestState = eSetupTimer;
    break;
  case eSetupTimer :
    write(APB_TIMER_LOAD,    100);                   // Load timer value to 100 cycles
    write(APB_TIMER_CONTROL, APB_TIMER_CTRL_ENABLE); // Enable timer
    
    // Go to the next state to wait for the interrupt once the timer has counted down to 0
    mTestState = eWaitForInterrupt;
    break;
  case eWaitForInterrupt :
    // Clean the transaction receive flags as the write transaction finished
    mTransReceived = false;

    // Once the interrupt is received, clear it and move on to the next state
    if(mInterruptVal) {
      write(APB_TIMER_CONTROL, 0); // Disable timer
      write(APB_TIMER_CLEAR,   1); // Clear interrupt
      mTestState = eWaitInterruptClear;
    }
    break;
  case eWaitInterruptClear :
    // Clean the transaction receive flags as the write transaction finished
    mTransReceived = false;

    // Once the interrupt is cleared, move on to the next state
    if(mInterruptVal == 0) {
      mTestState = eTestDone;
    }
    break;
  case eTestDone :
    cout << "Test is done!" << endl;
    mTestDone = true; // Flag that test is done
    break;
  }
}

//! Interrupt method, will get called by the Carbon model when the interrupt pin changes value 
void TimerTest::interrupt(CarbonUInt32 value)
{
  // Report the interrupt
  if(value == 1) cout << "Interrupt recevied." << endl;
  else           cout << "Interrupt cleared."  << endl;

  // Save the interrupt value
  mInterruptVal = value;
}

//! Report Transaction Method, should be defined by test
void TimerTest::reportTransaction(APB3_Transaction* trans)
{
  if(trans->transType == APB3_Transaction::Write) {
    std::cout << "Write transaction done." << std::endl;
  }
  else {
    std::cout << "Read " << trans->rdata << " from addr " << trans->addr << std::endl;
  }
  
  // Store transaction, and indicate reception
  mLastReceivedTrans = *trans;
  mTransReceived     = true;

  // Decrement outstanding transaction count;
  --mTransCount;

  // Clean up transaction
  delete trans;
}

//! Function to create a write transaction
void TimerTest::write(CarbonUInt32 addr, CarbonUInt32 data)
{
  // Setup the write transaction by filling in the transaction structure
  APB3_Transaction* trans = new APB3_Transaction;
  trans->transType = APB3_Transaction::Write;
  trans->addr      = addr;
  trans->wdata     = data;
  std::cout << "Writing " << trans->wdata << " to address " << trans->addr << std::endl; 
  
  // Once the transaction is created it can be queued to the transactor with the startTransaction method
  mMasterTrans->startTransaction(trans);

  // Increment, the transaction count, so the test can keep track of outstanding transactions. 
  ++mTransCount;
}

//! Function to create a read transaction
void TimerTest::read(CarbonUInt32 addr)
{
  APB3_Transaction* trans = new APB3_Transaction;
  trans->transType = APB3_Transaction::Read;
  trans->addr      = addr;
  std::cout << "Reading from address " << trans->addr << std::endl; 
  mMasterTrans->startTransaction(trans);
  ++mTransCount;
}

/*!
  Main function for APB test.
*/
int main(int argc, char* argv[])
{
  (void)argc; (void)argv;
  
  // Setup testbench
  TimerTest test;  
  test.setup();

  // Run until evaulates indicates that it has nothing left to do
  bool cont = true;
  CarbonTime simTime = 0;
  for(simTime = 0; cont && simTime < 100000; simTime += 10) {
    cont = test.evaluate(simTime);
  }
  // Check if we timed out
  if(simTime == 100000) {
    cout << "Error: Simulation timed out before test was done!" << endl;
    return 1;
  }

  return test.getErrors();
}
