/*
 APB Wire
 
 This model just wires the APB ports between the intiator and target.

*/
module apb_wire
  (paddr,
   pwdata,
   prdata,
   pready,
   pwrite,
   psel,
   penable,

   paddr_out,
   pwdata_out,
   prdata_in,
   pready_in,
   pwrite_out,
   psel_out,
   penable_out);

   // Initiator side ports
   input [31:0]   paddr;
   input [31:0]   pwdata;
   output [31:0]  prdata;
   output         pready;
   input          pwrite;
   input          psel;
   input          penable;

   // Target side ports
   output [31:0]  paddr_out;
   output [31:0]  pwdata_out;
   input [31:0]   prdata_in;
   input          pready_in;
   output         pwrite_out;
   output         psel_out;
   output         penable_out;

   // Wire the signals
   assign paddr_out   = paddr;
   assign pwdata_out  = pwdata;
   assign prdata      = prdata_in;
   assign pready      = pready_in;
   assign pwrite_out  = pwrite;
   assign psel_out    = psel;
   assign penable_out = penable;

endmodule

   