#include "APB3_Slave.h"
#include <iostream>
#include <sstream>

using namespace std;

APB3_Slave::APB3_Slave(const char* transactorName,
                       void (*setDefaultResponse)(APB3_Transaction*, void*),
                       void (*setResponse)(APB3_Transaction*, void*),
                       void (*notify)(APB3_ErrorType, const char*, const APB3_Transaction*, void*),
                       void (*notify2)(APB3_ErrorType, const char*, void*),
                       void *userInstance) :
  mXtorName(transactorName),
  mSetDefaultResponse(setDefaultResponse),
  mSetResponse(setResponse),
  mNotify(notify),
  mNotify2(notify2),
  mUserInstance(userInstance),
  mCarbonModelHandle(0),
  mState(IDLE),
  mWaitCnt(0),
  mReady(0)
{
  // Clear Net IDs
  memset(mNetID, 0, sizeof(mNetID));

  // Clear Port Values
  memset(mPortValues, 0, sizeof(mPortValues));

  // Clear Transaction structure
  memset(&mTrans, 0, sizeof(mTrans));
  
}

bool APB3_Slave::connect(ConnectNameNamePair *nameList, CarbonObjectID *carbonModelHandle)
{
  // Check Parameters
  if (nameList == NULL || carbonModelHandle == NULL)
    return 0;
  
  // Save Carbon model handle
  mCarbonModelHandle = carbonModelHandle;

  // Iterate until the last item is reached for which TransactorSignalName
  // field has NULL
  for (CarbonUInt32 i = 0; nameList[i].TransactorSignalName != NULL; ++i)
  {
    // Get the Net Handle from the Carbon model
    CarbonNetID* netID = carbonFindNet(carbonModelHandle, nameList[i].ModelSignalName);
    
    // Check that we got a valid net id handle
    if(netID == NULL) return false;
    
    // Check if transactor port name is valid
    const char* portName = nameList[i].TransactorSignalName;

    // Inputs
    if(strcmp(portName, "PADDR") == 0) {
      // Store the net ID for the port
      mNetID[INDEX_PADDR] = netID;

      // For inputs we register net change callbacks from the Carbon model
      // The netChangeCB functions stores the new value at the position given 
      // by the ClientUserData parameter
      CarbonNetValueCBDataID* cbID = carbonAddNetValueChangeCB(carbonModelHandle, 
                                                               netChangeCB, 
                                                               &mPortValues[INDEX_PADDR],
                                                               netID);
      // Check that the callback was added successfully
      if(cbID == NULL) return false;

      // Get the initial value of PADDR from the Carbon model
      carbonExamine(carbonModelHandle, netID, &mPortValues[INDEX_PRDATA], NULL);
    }
    else if(strcmp(portName, "PWDATA") == 0) {
      // Store the net ID for the port
      mNetID[INDEX_PWDATA] = netID;

      // For inputs we register net change callbacks from the Carbon model
      // The netChangeCB functions stores the new value at the position given 
      // by the ClientUserData parameter
      CarbonNetValueCBDataID* cbID = carbonAddNetValueChangeCB(carbonModelHandle, 
                                                               netChangeCB, 
                                                               &mPortValues[INDEX_PWDATA],
                                                               netID);
      // Check that the callback was added successfully
      if(cbID == NULL) return false;

      // Get the initial value of PWDATA from the Carbon model
      carbonExamine(carbonModelHandle, netID, &mPortValues[INDEX_PREADY], NULL);
    }
    else if(strcmp(portName, "PSEL") == 0) {
      // Store the net ID for the port
      mNetID[INDEX_PSEL] = netID;

      // For inputs we register net change callbacks from the Carbon model
      // The netChangeCB functions stores the new value at the position given 
      // by the ClientUserData parameter
      CarbonNetValueCBDataID* cbID = carbonAddNetValueChangeCB(carbonModelHandle, 
                                                               netChangeCB, 
                                                               &mPortValues[INDEX_PSEL],
                                                               netID);
      // Check that the callback was added successfully
      if(cbID == NULL) return false;

      // Get the initial value of PSEL from the Carbon model
      carbonExamine(carbonModelHandle, netID, &mPortValues[INDEX_PREADY], NULL);
    }
    else if(strcmp(portName, "PENABLE") == 0) {
      // Store the net ID for the port
      mNetID[INDEX_PENABLE] = netID;

      // For inputs we register net change callbacks from the Carbon model
      // The netChangeCB functions stores the new value at the position given 
      // by the ClientUserData parameter
      CarbonNetValueCBDataID* cbID = carbonAddNetValueChangeCB(carbonModelHandle, 
                                                               netChangeCB, 
                                                               &mPortValues[INDEX_PENABLE],
                                                               netID);
      // Check that the callback was added successfully
      if(cbID == NULL) return false;

      // Get the initial value of PENABLE from the Carbon model
      carbonExamine(carbonModelHandle, netID, &mPortValues[INDEX_PREADY], NULL);
    }
    else if(strcmp(portName, "PWRITE") == 0) {
      // Store the net ID for the port
      mNetID[INDEX_PWRITE] = netID;

      // For inputs we register net change callbacks from the Carbon model
      // The netChangeCB functions stores the new value at the position given 
      // by the ClientUserData parameter
      CarbonNetValueCBDataID* cbID = carbonAddNetValueChangeCB(carbonModelHandle, 
                                                               netChangeCB, 
                                                               &mPortValues[INDEX_PWRITE],
                                                               netID);
      // Check that the callback was added successfully
      if(cbID == NULL) return false;

      // Get the initial value of PWRITE from the Carbon model
      carbonExamine(carbonModelHandle, netID, &mPortValues[INDEX_PREADY], NULL);
    }
    // Outputs
    else if(strcmp(portName, "PRDATA") == 0) {
      // Store the net ID for the port
      mNetID[INDEX_PRDATA]  = netID;
    }
    else if(strcmp(portName, "PREADY") == 0) {
      // Store the net ID for the port
      mNetID[INDEX_PREADY] = netID;
    }

    // The port name was not recognized as a transactor port
    else {
      stringstream msg;
      msg << "Unknown port name " << portName;

      // Use the notify2 callback to notify the user about the reason connection failed.
      if(mNotify2) (*mNotify2)(APB3_ConnectionError, msg.str().c_str(), mUserInstance);

      // Fall back to stdout of notify2 is not specified
      cout << msg << endl;

      return false;
    }
    
  }
  
  // If we got here we have successfully connected all ports in the list
  return true;
};

void APB3_Slave::evaluate(CarbonTime)
{
  // Calculate the state variable
  if(mPortValues[INDEX_PSEL] == 1 && mPortValues[INDEX_PENABLE] == 0)
    mState = SETUP;
  else if(mPortValues[INDEX_PSEL] == 1 && mPortValues[INDEX_PENABLE] == 1)
    mState = ENABLE;
  else
    mState = IDLE;

  // State machine
  switch(mState) {
  case SETUP :
    // Clear the transaction structure
    mTrans.wdata      = 0;
    mTrans.rdata      = 0;
    mTrans.waitStates = 0;

    // In setup we need catch the type and address of the transaction 
    // and call the setDefaultResponse callback function
    mTrans.transType = (mPortValues[INDEX_PWRITE] == 1) ? APB3_Transaction::Write : APB3_Transaction::Read;
    mTrans.addr      =  mPortValues[INDEX_PADDR];

    // Call setDefaultResponse function
    (*mSetDefaultResponse)(&mTrans, mUserInstance);

    // Set the wait count based on the data provided by the user
    mWaitCnt = mTrans.waitStates;

    // If Wait count is 0 and it's a read, we need to call SetDefault response right away
    // This is because we need to output prdata and pready in the refreshSignals method
    // in the beginning of the next cycle.
    if(mTrans.transType == APB3_Transaction::Read && mWaitCnt == 0)
      (*mSetResponse)(&mTrans, mUserInstance);

    break;
    
  case ENABLE :
    // Fill the transaction structure
    mTrans.transType = (mPortValues[INDEX_PWRITE] == 1) ? APB3_Transaction::Write : APB3_Transaction::Read;
    mTrans.addr      =  mPortValues[INDEX_PADDR];
    mTrans.wdata     =  mPortValues[INDEX_PWDATA];
    
    // Read data has to be fetched from the user a cycle early, so we want to call setResponse
    // When mWaitCnt == 1 and transaction Type is Read. For writes however we want to call it
    // when mWaitCnt == 0
    if( (mWaitCnt == 1 && mTrans.transType == APB3_Transaction::Read) || 
        (mWaitCnt == 0 && mTrans.transType == APB3_Transaction::Write) ){
      (*mSetResponse)(&mTrans, mUserInstance);
    }
      
    // Decrement wait count
    --mWaitCnt;

    break;

  default :
    // For IDLE do nothing
    break;
  }

  // If Wait Count is 0, we need to notify the master that data is ready
  // pready defaults to 1, during idle cycles
  if(mWaitCnt == 0 || mState == IDLE) {
    mReady    = 1; // Notify master that data is ready
  }

  // Otherwise tell master that data is not ready
  else mReady = 0;
}

void APB3_Slave::refreshSignals()
{
  // APB3 Slave only needs to drive ports in the setup and enable phases
  if(mState != IDLE) {
    // PRDATA and PREADY have flowthrough paths from PENABLE, so we
    // must check here that PENABLE is asserted before driving PRDATA
    if(mPortValues[INDEX_PENABLE]) {
      drivePort(INDEX_PRDATA, mTrans.rdata);
      drivePort(INDEX_PREADY, mReady);
    }
    else {
      drivePort(INDEX_PRDATA, 0);
      drivePort(INDEX_PREADY, 1);
    }
  }
}

void APB3_Slave::netChangeCB(CarbonObjectID* cid, CarbonNetID* nid, CarbonClientData clientData, 
                              CarbonUInt32* value, CarbonUInt32*)
{
  char buf[40];
  carbonGetNetName(cid, nid, buf, 40);

  // clientData points to the portValue storage for the net that changed.
  // This was setup when we registered the callback with the Carbon model.
  CarbonUInt32* portValue = reinterpret_cast<CarbonUInt32*>(clientData);

  // Store the new value
  *portValue = *value;
}


