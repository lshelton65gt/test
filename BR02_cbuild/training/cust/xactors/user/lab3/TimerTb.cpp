/*
  Testbench for APB Timer.

  The testbench class is responsible for setting up the Carbon model
  and the transactor and connecting them together. It's also responsible
  for generating the clock for the model and to call the transactors
  evaluate method for every cycle.
  The testbench executes the test by for every cycle calling the
  testFunction virtual method.

  A test class is supposed to inherit from this testbench class to
  run the test.
*/
#include <iostream>
#include "TimerTb.h"

using namespace std;

//! TimerTb constructor
TimerTb::TimerTb() : mCarbonID(0),
                     mMasterTrans(0),
                     mSlaveTrans(0),
                     mTimerModel(&intMonitorCB, this),
                     mTestDone(false)
{}

//! TimerTb destructor
TimerTb::~TimerTb()
{
  // Delete transactor
  delete mMasterTrans;

  // Destroy Carbon model
  carbonDestroy(&mCarbonID);
}

//! Setup carbon model and transactor
bool TimerTb::setup()
{

  // Setup Carbon model
  mCarbonID = carbon_apb_wire_create(eCarbonFullDB, eCarbon_NoFlags);
  
  // Check that the model was successfully created.
  if(mCarbonID == 0){
    std::cout << "Carbon model creation failed." << std::endl;
    return false;
  }

  // Setup the master transactor
  {
    mMasterTrans = new APB3_Master("APB Master",        // Transactor Instance name
                                   5,                   // Max number of outstaning transactions 
                                   reportTransactionCB, // Report Transaction callback function
                                   masterNotifyCB,      // Notify Callback function
                                   masterNotify2CB,     // Notify2 Callback function
                                   this                 // User Instance (passed to callback functions)
      );
    
    // Connect the transactor to the Carbon model
    ConnectNameNamePair masterConnectPair[] = { {"PRDATA",  "apb_wire.prdata"  },
                                                {"PREADY",  "apb_wire.pready"  },
                                                {"PADDR",   "apb_wire.paddr"   },
                                                {"PWDATA",  "apb_wire.pwdata"  },
                                                {"PSEL",    "apb_wire.psel"    },
                                                {"PENABLE", "apb_wire.penable" },
                                                {"PWRITE",  "apb_wire.pwrite"  },
                                                0};
    bool success = mMasterTrans->connect(masterConnectPair, mCarbonID);
    
    // Check that connection was successful
    if(!success) {
      std::cout << "APB Master transactor connection failed." << std::endl;
      return false;
    }
  }

  // Setup the slave transactor
  {
    mSlaveTrans = new APB3_Slave("APB Slave",          // Transactor Instance name
                                 setDefaultResponseCB, // Set Default Response callback function
                                 setResponseCB,        // Set Response callback function
                                 slaveNotifyCB,        // Notify Callback function
                                 slaveNotify2CB,       // Notify2 Callback function
                                 &mTimerModel          // User Instance (passed to callback functions)
      );
    
    // Connect the transactor to the Carbon model
    ConnectNameNamePair slaveConnectPair[] = { {"PRDATA",  "apb_wire.prdata_in" },
                                               {"PREADY",  "apb_wire.pready_in" },
                                               {"PADDR",   "apb_wire.paddr_out"   },
                                               {"PWDATA",  "apb_wire.pwdata_out"  },
                                               {"PSEL",    "apb_wire.psel_out"    },
                                               {"PENABLE", "apb_wire.penable_out" },
                                               {"PWRITE",  "apb_wire.pwrite_out"  },
                                               0};
    bool success = mSlaveTrans->connect(slaveConnectPair, mCarbonID);
    
    // Check that connection was successful
    if(!success) {
      std::cout << "APB Slave transactor connection failed." << std::endl;
      return false;
    }
  }

  // Setup Wave dumping
  CarbonWaveID* waveID = carbonWaveInitFSDB(mCarbonID, "waves.fsdb", e1ns);
  carbonDumpVars(waveID, 0, "apb_wire");

  // Now if we made it this far we have successfully setup the environment
  return true;
}


//! Evaluate function. Gets called every cycle
bool TimerTb::evaluate(CarbonTime currTime)
{
  // Execute test and transactor
  testFunction();
    
  // Note the timing of the calls here. All transactors should be evaluated
  // before their output signals are driven out. This way the transactors works
  // as if their outputs are registers. I.e, the transactor is reading their
  // inputs before the edge, but updating the outputs after the edge, and the order
  // of the evaluates stay independent.

  // Evaluate transactors.
  mMasterTrans->evaluate(currTime);
  mSlaveTrans->evaluate(currTime);
  
  // Now propagate the signals through the model to the slave transactor and back to the master
  // This is needed for this particular transactor to propagate the penable signal
  // to the slave, and then the prdata and pready signal back to the master. This is only 
  // an issue because the Carbon model we use here is a straight flowthrough model and there are
  // flowthrough paths in the handshake for this protocol. The order of the calls are important here.
  mMasterTrans->refreshSignals();
  carbonSchedule(mCarbonID, currTime);
  mSlaveTrans->refreshSignals();
  carbonSchedule(mCarbonID, currTime);
  
  // Evaluate Timer Model
  mTimerModel.evaluate();

  // We want to continue to evaluate until the test is done
  if(mTestDone) return false;
  else          return true;
}

//! Callback function for Interrupt
void TimerTb::intMonitorCB(CarbonClientData clientData, CarbonUInt32 value)
{
  TimerTb* that = reinterpret_cast<TimerTb*>(clientData);
  that->interrupt(value);
}

//! Report Transaction Callback function for master transactor
void TimerTb::reportTransactionCB(APB3_Transaction* trans, void* clientData)
{
  TimerTb* that = reinterpret_cast<TimerTb*>(clientData);
  that->reportTransaction(trans);
}
  
//! Notify Callback function for master transactor
void TimerTb::masterNotifyCB(APB3_ErrorType error, const char* message, const APB3_Transaction* trans, void*)
{
  (void)trans;

  switch(error) {
  default:
    // Just print the error message for all error types. 
    // For more complex protocolls some specific error handling may be needed.
    std::cout << message << std::endl;
  }
}

//! Notify2 Callback function for master transactor
void TimerTb::masterNotify2CB(APB3_ErrorType error, const char* message, void*)
{
  switch(error) {
  default:
    // Just print the error message for all error types. 
    // For more complex protocolls some specific error handling may be needed.
    std::cout << message << std::endl;
  }
}

//! Set Default Response Callback function for slave transactor
void TimerTb::setDefaultResponseCB(APB3_Transaction* trans, void* clientData)
{
  TimerModel* that = reinterpret_cast<TimerModel*>(clientData);
  that->setDefaultResponse(trans);
}
  
//! Set Default Response Callback function for slave transactor
void TimerTb::setResponseCB(APB3_Transaction* trans, void* clientData)
{
  TimerModel* that = reinterpret_cast<TimerModel*>(clientData);
  that->setResponse(trans);
}
  
//! Notify Callback function for slave transactor
void TimerTb::slaveNotifyCB(APB3_ErrorType error, const char* message, const APB3_Transaction* trans, void*)
{
  (void)trans;

  switch(error) {
  default:
    // Just print the error message for all error types. 
    // For more complex protocolls some specific error handling may be needed.
    std::cout << message << std::endl;
  }
}

//! Notify2 Callback function for slave transactor
void TimerTb::slaveNotify2CB(APB3_ErrorType error, const char* message, void*)
{
  switch(error) {
  default:
    // Just print the error message for all error types. 
    // For more complex protocolls some specific error handling may be needed.
    std::cout << message << std::endl;
  }
}

  
