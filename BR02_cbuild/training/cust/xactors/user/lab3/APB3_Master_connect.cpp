#include <iostream>
#include <sstream>
#include "APB3_Master.h"

using namespace std;

bool APB3_Master::connect(ConnectNameNamePair *nameList, CarbonObjectID *carbonModelHandle)
{
  // Check Parameters
  if (nameList == NULL || carbonModelHandle == NULL)
    return 0;
  
  // Save Carbon model handle
  mCarbonModelHandle = carbonModelHandle;

  // Iterate until the last item is reached for which TransactorSignalName
  // field has NULL
  for (CarbonUInt32 i = 0; nameList[i].TransactorSignalName != NULL; ++i)
  {
    // Get the Net Handle from the Carbon model
    CarbonNetID* netID = carbonFindNet(carbonModelHandle, nameList[i].ModelSignalName);
    
    // Check that we got a valid net id handle
    if(netID == NULL) return false;
    
    // Check if transactor port name is valid
    const char* portName = nameList[i].TransactorSignalName;

    // Inputs
    if(strcmp(portName, "PRDATA") == 0) {
      // Store the net ID for the port
      mNetID[INDEX_PRDATA] = netID;

      // For inputs we register net change callbacks from the Carbon model
      // The netChangeCB functions stores the new value at the position given 
      // by the ClientUserData parameter
      CarbonNetValueCBDataID* cbID = carbonAddNetValueChangeCB(carbonModelHandle, 
                                                               netChangeCB, 
                                                               &mPortValues[INDEX_PRDATA],
                                                               netID);
      // Check that the callback was added successfully
      if(cbID == NULL) return false;

      // Get the initial value of PRDATA from the Carbon model
      carbonExamine(carbonModelHandle, netID, &mPortValues[INDEX_PRDATA], NULL);
    }
    else if(strcmp(portName, "PREADY") == 0) {
      // Store the net ID for the port
      mNetID[INDEX_PREADY] = netID;

      // For inputs we register net change callbacks from the Carbon model
      // The netChangeCB functions stores the new value at the position given 
      // by the ClientUserData parameter
      CarbonNetValueCBDataID* cbID = carbonAddNetValueChangeCB(carbonModelHandle, 
                                                               netChangeCB, 
                                                               &mPortValues[INDEX_PREADY],
                                                               netID);
      // Check that the callback was added successfully
      if(cbID == NULL) return false;

      // Get the initial value of PREADY from the Carbon model
      carbonExamine(carbonModelHandle, netID, &mPortValues[INDEX_PREADY], NULL);
    }
    // Outputs
    else if(strcmp(portName, "PADDR") == 0) {
      // Store the net ID for the port
      mNetID[INDEX_PADDR]  = netID;
    }
    else if(strcmp(portName, "PWDATA") == 0) {
      // Store the net ID for the port
      mNetID[INDEX_PWDATA] = netID;
    }
    else if(strcmp(portName, "PSEL") == 0) {
      // Store the net ID for the port
      mNetID[INDEX_PSEL]   = netID;
    }
    else if(strcmp(portName, "PENABLE") == 0) {
      // Store the net ID for the port
      mNetID[INDEX_PENABLE] = netID;
    }
    else if(strcmp(portName, "PWRITE") == 0) {
      // Store the net ID for the port
      mNetID[INDEX_PWRITE] = netID;
    }

    // The port name was not recognized as a transactor port
    else {
      stringstream msg;
      msg << "Unknown port name " << portName;

      // Use the notify2 callback to notify the user about the reason connection failed.
      if(mNotify2) (*mNotify2)(APB3_ConnectionError, msg.str().c_str(), mUserInstance);

      // Fall back to stdout of notify2 is not specified
      cout << msg << endl;

      return false;
    }
    
  }
  
  // If we got here we have successfully connected all ports in the list
  return true;
};
