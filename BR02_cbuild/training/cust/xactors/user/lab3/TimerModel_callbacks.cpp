#include "TimerModel.h"

void TimerModel::setDefaultResponse(APB3_Transaction* trans)
{
  // LAB Exercise:
  // Implement the setDefaultResponse callback.
  // Set the wait_cnt field in the trans structure to different values
  // based on the type of transaction (Read/Write) or the address.
}

void TimerModel::setResponse(APB3_Transaction* trans)
{
  // LAB Exercise:
  // Implement the setDefaultResponse callback.
  // For read transactions, set the read_data field of the transaction 
  // structure passed to the function based on the address field. If the 
  // address is between APB3_MEMORY_BASE and APB3_MEMORY_BASE + APB3_MEMORY_SIZE 
  // set the data from the corresponding address from the structure. If the 
  // address matches a register, set the read data from that register value.
  // For write transaction, get the write data from the write_data field
  // of the transaction structure, and write it either to the memory, or
  // a register, depending on the address in the same manner as for the
  // read transaction.
}

