/*
 APB Timer

 This is a C++ version of the apb_timer RTL model from testbench1.

 This module implement a simple timer as well as a 256x32 bit memory.
 
 The timer counts down from the value specified by the TIMER_LOAD register.
 Once the timer value reaches 0, an interrupt will be asserted. If the PERIODIC bit
 in the timers control register is set, the timer will be loaded again with the value stored
 in the TIMER_LOAD register and continue to count down. If the PERIODIC bit is not set
 the timer will stop counting once it reaches 0. The interrupt pin will be asserted until
 until the TIMER_CLEAR register is written to 1.
 The current value of the timer can be read trough the TIMER_CURRENT register.
 
 The memory is a scratch pad memory and have no effect on the operation of the timer.
 The memory's base address is 0x100 and it is 256 words by 32 bits in size.
 
 Register              Address     Read/Write  Description
 TIMER_LOAD            0x00        R/W         Start value of the timer.
 TIMER_CURRENT         0x04        RO          Current value of the timer.
 TIMER_CONTROL         0x08        R/W         Controls the operation of the timer.
 TIMER_CLEAR           0x12        WO          Clears the interrupt.
 
 Memory                0x100 -     R/W         Scratch Pad memory
                       0x1ff               
 
*/
#include "TimerModel.h"
#include <iostream>

using namespace std;

TimerModel::TimerModel(void (*interruptCB)(CarbonClientData, CarbonUInt32),
                       CarbonClientData clientData) :
  mInterruptCB(interruptCB),
  mClientData(clientData),
  mTimerLoadVal(0),
  mTimerCurrVal(0),
  mTimerEnable(false),
  mTimerPeriodic(false),
  mTimerLoad(false),
  mTimerClear(false),
  mTimerStopped(false)
{
}

void TimerModel::evaluate()
{
  // Load timer if requested
  if(mTimerLoad) {
    mTimerCurrVal = mTimerLoadVal;
    mTimerStopped = false; // Restart the timer when it's reloaded.
  }
         
  // Else when timer is enabled and it hasn't stopped , update the timer
  else if(mTimerEnable && !mTimerStopped) {
    // When timer hits 0, assert interrupt and either restart from load value, or stop.
    if(mTimerCurrVal == 0) {
      // Send Interrupt
      (*mInterruptCB)(mClientData, 1);
      
      // Reload timer if periodic is set
      if(mTimerPeriodic)
        mTimerCurrVal = mTimerLoadVal;
      
       // If not periodic, stop the timer.
      else 
        mTimerStopped = true;
    }
    else
      --mTimerCurrVal; // Decrement timer value
  }

  // Clear the interrupt when requested
  if(mTimerClear)
    (*mInterruptCB)(mClientData, 0);

  // Clear Control Signals
  mTimerLoad  = false;
  mTimerClear = false;
}




  
  
