#include "TimerModel.h"

void TimerModel::setDefaultResponse(APB3_Transaction* trans)
{
  // To get some more interesting activity on pready, register writes have 0, register reads 2
  // memory writes 1 and memory reads 3 waitstates.
  if(trans->addr >= APB_TIMER_BASE && trans->addr < APB_MEMORY_BASE) {
    if(trans->transType == APB3_Transaction::Write) trans->waitStates = 0;
    else trans->waitStates = 2;
  }
  else if(trans->addr >= APB_MEMORY_BASE && trans->addr < (APB_MEMORY_BASE + APB_MEMORY_SIZE)) {
    if(trans->transType == APB3_Transaction::Write) trans->waitStates = 1;
    else trans->waitStates = 3;
  }
}

void TimerModel::setResponse(APB3_Transaction* trans)
{
  // Read Transaction
  if(trans->transType == APB3_Transaction::Read) {

    // Set read data to 0 by default
    trans->rdata = 0;

    if(trans->addr >= APB_TIMER_BASE && trans->addr < APB_MEMORY_BASE) {
      switch(trans->addr) {
      case (APB_TIMER_LOAD - APB_TIMER_BASE):
        trans->rdata = mTimerLoadVal;
        break;
      case (APB_TIMER_CONTROL - APB_TIMER_BASE):
        trans->rdata = (mTimerPeriodic << 6) | (mTimerEnable << 7);
        break;
      case (APB_TIMER_CURRENT - APB_TIMER_BASE):
        trans->rdata = mTimerCurrVal;
        break;
      }
    }

    else if((trans->addr >= APB_MEMORY_BASE) && (trans->addr < (APB_MEMORY_BASE + APB_MEMORY_SIZE))) {
      trans->rdata = mMemory[(trans->addr - APB_MEMORY_BASE) >> 2];
    }
  }

  // Write Transaction
  else {
    switch (trans->addr) {
    case (APB_TIMER_LOAD - APB_TIMER_BASE):
      mTimerLoadVal = trans->wdata;
      mTimerLoad    = true;
      break;
    case (APB_TIMER_CONTROL - APB_TIMER_BASE) :
      mTimerPeriodic = (trans->wdata & APB_TIMER_CTRL_PERIODIC) != 0;
      mTimerEnable   = (trans->wdata & APB_TIMER_CTRL_ENABLE)   != 0;
      break;
    case (APB_TIMER_CLEAR - APB_TIMER_BASE):
      mTimerClear = true;
      break;
    default :
      mMemory[(trans->addr - APB_MEMORY_BASE) >> 2] = trans->wdata;
      break;
    }
  }
}

