// -*- C++ -*-
/*
 APB Timer

 This is a C++ version of the apb_timer RTL model from testbench1.

 This module implement a simple timer as well as a 256x32 bit memory.
 
 The timer counts down from the value specified by the TIMER_LOAD register.
 Once the timer value reaches 0, an interrupt will be asserted. If the PERIODIC bit
 in the timers control register is set, the timer will be loaded again with the value stored
 in the TIMER_LOAD register and continue to count down. If the PERIODIC bit is not set
 the timer will stop counting once it reaches 0. The interrupt pin will be asserted until
 until the TIMER_CLEAR register is written to 1.
 The current value of the timer can be read trough the TIMER_CURRENT register.
 
 The memory is a scratch pad memory and have no effect on the operation of the timer.
 The memory's base address is 0x100 and it is 256 words by 32 bits in size.
 
 Register              Address     Read/Write  Description
 TIMER_LOAD            0x00        R/W         Start value of the timer.
 TIMER_CURRENT         0x04        RO          Current value of the timer.
 TIMER_CONTROL         0x08        R/W         Controls the operation of the timer.
 TIMER_CLEAR           0x12        WO          Clears the interrupt.
 
 Memory                0x100 -     R/W         Scratch Pad memory
                       0x1ff               
 
*/
#ifndef _TimerModel_h_
#define _TimerModel_h_

#include <map>
#include "APB3_Types.h"

// Base Addresses
#define APB_TIMER_BASE     0x0
#define APB_MEMORY_BASE    0x400
#define APB_MEMORY_SIZE    0x400

// Register Address defines
#define APB_TIMER_LOAD     0x0
#define APB_TIMER_CURRENT  0x4
#define APB_TIMER_CONTROL  0x8
#define APB_TIMER_CLEAR    0xC

// Register Field defines
#define     APB_TIMER_CTRL_ENABLE   0x80
#define     APB_TIMER_CTRL_PERIODIC 0x40

class TimerModel {
public:
  
  //! Constructor
  /*!
    \param interruptCB Callback function for timer interrupt
  */
  TimerModel(void (*interruptCB)(CarbonClientData clientData, CarbonUInt32 value),
             CarbonClientData clientData);

  //! This is the Set Default Response callback for the APB3_Slave transactor.
  void setDefaultResponse(APB3_Transaction* trans);

  //! This is the Set Response callback for the APB3_Slave transactor.
  void setResponse(APB3_Transaction* trans);
  
  //! Evaluate method should be called every cycle
  void evaluate();

private:
  //! Interrupt callback function
  void (*mInterruptCB)(CarbonClientData clientData, CarbonUInt32 value);

  //! Client data for interrupt callback
  void* mClientData;

  //! Timer Member variables
  CarbonUInt32 mTimerLoadVal;
  CarbonUInt32 mTimerCurrVal;
  bool         mTimerEnable;
  bool         mTimerPeriodic;

  //! Timer Control
  bool         mTimerLoad;
  bool         mTimerClear;
  bool         mTimerStopped;

  //! Scratch Pad Memory
  std::map<CarbonUInt32, CarbonUInt32> mMemory;
};

#endif
