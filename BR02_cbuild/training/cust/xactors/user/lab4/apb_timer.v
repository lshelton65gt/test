/*
 APB Timer
 
 This module implement a simple timer as well as a 256x32 bit memory.
 
 The timer counts down from the value specified by the TIMER_LOAD register.
 Once the timer value reaches 0, an interrupt will be asserted. If the PERIODIC bit
 in the timers control register is set, the timer will be loaded again with the value stored
 in the TIMER_LOAD register and continue to count down. If the PERIODIC bit is not set
 the timer will stop counting once it reaches 0. The interrupt pin will be asserted until
 until the TIMER_CLEAR register is written to 1.
 The current value of the timer can be read trough the TIMER_CURRENT register.
 
 The memory is a scratch pad memory and have no effect on the operation of the timer.
 The memory's base address is 0x100 and it is 256 words by 32 bits in size.
 
 Register              Address     Read/Write  Description
 TIMER_LOAD            0x00        R/W         Start value of the timer.
 TIMER_CURRENT         0x04        RO          Current value of the timer.
 TIMER_CONTROL         0x08        R/W         Controls the operation of the timer.
 TIMER_CLEAR           0x12        WO          Clears the interrupt.
 
 Memory                0x100 -     R/W         Scratch Pad memory
                       0x1ff               
 
*/

module apb_timer
  (reset,
   pclk,
   paddr,
   pwdata,
   prdata,
   pready,
   pwrite,
   psel,
   penable,
   interrupt);

   input         reset;
   input         pclk;
   input [31:0]  paddr;
   input [31:0]  pwdata;
   output [31:0] prdata;
   output        pready;
   input 	 pwrite;
   input 	 psel;
   input 	 penable;
   output 	 interrupt;

   // Base addresses
   parameter     APB_TIMER_BASE  = 0;
   parameter     APB_MEMORY_BASE = 'h400;
   parameter     APB_MEMORY_SIZE = 'h400;
   
   // Register Address Parameters
   parameter     APB_TIMER_LOAD    = 32'h0;
   parameter     APB_TIMER_CURRENT = 32'h4;
   parameter     APB_TIMER_CONTROL = 32'h8;
   parameter     APB_TIMER_CLEAR   = 32'hC;

   // Register Field Parameters
   parameter     APB_TIMER_CTRL_ENABLE = 31'h80;
   parameter     APB_TIMER_CTRL_PERIODIC = 31'h40;

   // State Machine State Parameters
   parameter 	 APB_IDLE   = 0;
   parameter     APB_SELECT = 1;
   parameter     APB_ENABLE = 2;

   // "Software" Registers
   reg [15:0]    timer_val;
   reg [15:0] 	 timer_load_val;
   reg [31:0] 	 timer_ctrl_reg;

   // Memory Array
   reg [31:0]    mem[0:255];
   
   // State variable (state is just a combination of inputs)
   wire [1:0]	 state;

   // Registered Output Ports 
   reg  	 interrupt;
  
   // Internal signals
   reg [31:0] 	 addr;
   reg [2:0] 	 wait_cnt;
   reg [31:0]	 read_data;
   reg           timer_load;
   reg           timer_clear;
   wire          timer_enable;
   wire          timer_periodic;
   reg           stop;
   integer 	 i;
   
   // Initialize Memory
   initial begin
      for(i = 0; i < 256; i = i + 1) begin
	 mem[i] = 'hdeadbeef;
      end
   end

   // Memory Address
   wire [31:0] mem_addr = (paddr - APB_MEMORY_BASE) >> 2; // carbon observeSignal
   
   // Crate state variable directly from input signals
   assign 	 state = (psel && ~penable) ? APB_SELECT :
		         (psel && penable)  ? APB_ENABLE : APB_IDLE;
   
   // Drive Read Data when psel and penable are both high and while paddr us used to select the register
   assign pready = (psel && penable) ?  (wait_cnt == 0) : 1; 
   assign prdata = (psel && !pwrite && penable && pready) ? read_data  : 0;

   // APB interace logic
   always @(posedge pclk) begin

      // Default Values
      timer_load  <= 0;
      timer_clear <= 0;

      // Wait Logic
      if(wait_cnt != 0) wait_cnt <= wait_cnt - 1;

      // State Machine
      if(state == APB_SELECT) begin
         // Store APB address
	 addr     <= paddr[7:0];

         // To get some more interesting activity on pready, register writes have 0, register reads 2
         // memory writes 1 and memory reads 3 waitstates.
         if(paddr >= APB_TIMER_BASE && paddr < APB_MEMORY_BASE) begin
            if(pwrite) wait_cnt <= 0;
            else       wait_cnt <= 2;
         end
         else if(paddr >= APB_MEMORY_BASE && paddr < (APB_MEMORY_BASE + APB_MEMORY_SIZE)) begin
            if(pwrite) wait_cnt <= 1;
            else       wait_cnt <= 3;
         end

         // Prepare read data
         if(!pwrite) begin

            // Set read data to 0 by default
            read_data <= 0;

            if(paddr >= APB_TIMER_BASE && paddr < APB_MEMORY_BASE) begin
	       case (addr)
	         (APB_TIMER_LOAD - APB_TIMER_BASE): begin
		    read_data <= timer_load_val;
	         end
	         (APB_TIMER_CONTROL - APB_TIMER_BASE): begin
		    read_data <= timer_ctrl_reg;
	         end
	         (APB_TIMER_CURRENT - APB_TIMER_BASE): begin
		    read_data <= timer_val;
	         end
               endcase
            end

            else if(paddr >= APB_MEMORY_BASE && paddr < (APB_MEMORY_BASE + APB_MEMORY_SIZE)) begin
               read_data <= mem[ (paddr - APB_MEMORY_BASE) >> 2];
            end
            
         end // if(!pwrite)
      end // if(state == APB_SELECT)
	 
      else if(state == APB_ENABLE) begin
	 if(pwrite && pready) begin
	    case (paddr)
	      (APB_TIMER_LOAD - APB_TIMER_BASE): begin
		 timer_load_val <= pwdata;
		 timer_load     <= 1;
	      end
	      (APB_TIMER_CONTROL - APB_TIMER_BASE): begin
		 timer_ctrl_reg <= pwdata;
	      end
	      (APB_TIMER_CLEAR - APB_TIMER_BASE): begin
		 timer_clear <= 1;
	      end
	      default : begin
		 mem[(paddr - APB_MEMORY_BASE) >> 2] <= pwdata;
	      end
	    endcase
	 end
	 else if(pready) begin
	    case (paddr)
	      APB_TIMER_CURRENT : begin
	      end
	      default : begin
	      end
	    endcase
         end
      end
   end

   // Decode the Timer Control Register
   assign timer_enable   = (timer_ctrl_reg & APB_TIMER_CTRL_ENABLE)   != 0; 
   assign timer_periodic = (timer_ctrl_reg & APB_TIMER_CTRL_PERIODIC) != 0;

   // Timer Process
   // This handles the functionality of the timer.
   always @(posedge pclk) begin
      if(reset) begin
	 timer_val      <= 16'hffff;
	 timer_load_val <= 0;
	 timer_ctrl_reg <= 0;
         interrupt      <= 0;
         stop           <= 0;
      end
      else begin
         // Load timer if requested
         if(timer_load) begin
            timer_val <= timer_load_val;
            stop      <= 0; // Restart the timer when it's reloaded.
         end
         
         // Else when timer is enabled and it hasn't stopped , update the timer
         else if(timer_enable && !stop) begin
            if(timer_val == 0) begin
               // When timer hits 0, assert interrupt and either restart from load value, or stop.
               interrupt <= 1;
               if(timer_periodic) timer_val <= timer_load_val;
               else stop <= 1; // If not periodic, stop the timer.
            end
            else
              timer_val <= timer_val - 1;
         end

         // Clear the interrupt when requested
         if(timer_clear) interrupt <= 0;
      end
   end
   
         
endmodule // apb_timer
