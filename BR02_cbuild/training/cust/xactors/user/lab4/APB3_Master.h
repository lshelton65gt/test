// -*- C++ -*-
#include "carbon/carbon_capi.h"
#include "APB3_Types.h"
#include <string>
#include <deque>

// Indexes for input ports. The code relies on the input ports having the lowest indexes
#define INDEX_PRDATA  0
#define INDEX_PREADY  1

// Indexes for output ports
#define INDEX_PADDR   2
#define INDEX_PWDATA  3
#define INDEX_PSEL    4
#define INDEX_PENABLE 5
#define INDEX_PWRITE  6

//! APB3 Master Transactor Class
class APB3_Master {
public:
  
  //! Transactor constructor
  /*!
    Constructs an instance of the transactor and sets up the various
    callback functions required. 
    
    The report transaction callback will be called when a transaction 
    previously started by the startTransaction method has finished 
    executing. For read type transactions return data will be provided by 
    this callback.
    
    The notify callback function will be called if the transactor encounters
    an event or error related to a specific transaction.

    The notify2 callback function will be called if the transactor encounters
    an event or error that cannot be related to a specific transaction.

    \param transactorName Name of the transactor instance
    \param maxTrans Maximum number of transactions in transaction queue.
    \param reportTransaction Report transaction callback function
    \param notify  Notify callback function
    \param notify2 Notify callback function
    \param userInstance An arbitrary pointer that will be passed along to
           the call back functions.
  */
  APB3_Master(const char* transactorName,
              const CarbonUInt32 maxTrans,
              void (*reportTransaction)(APB3_Transaction*, void*),
              void (*notify)(APB3_ErrorType, const char*,
                             const APB3_Transaction*, void*),
              void (*notify2)(APB3_ErrorType, const char*, void*),
              void *userInstance);

  //! Connect transactor to the Carbon model
  /*!
    Connects all port of the transactors 
    \param nameList List of pairs of transactor ports and Carbon model ports for each
    port on the transactor.
    \param carbonModelName Name of the Carbon model.
    \param carbonModelHandle Handle to the carbon model as return from the carbon_<model>_create function
    \retval true if connection was successful
    \retval false if connection failed.
  */
  bool connect(ConnectNameNamePair *nameList, CarbonObjectID *carbonModelHandle);
  
  //! Start Transaction
  /*!
    Queues a transaction to the transactor

    \param trans Transaction structure
  */
  bool startTransaction(APB3_Transaction* trans);

  //! Evaluates the state of the transactor
  /*!
    This method has to be called by the environment every cycle to update the 
    transactors internal state.
  */
  void evaluate(CarbonTime time);

  //! Drives the signal outputs
  void refreshSignals();

private:
  //! Carbon net change callback function
  static void netChangeCB(CarbonObjectID*, CarbonNetID*, CarbonClientData, 
                          CarbonUInt32* value, CarbonUInt32* drive);
  
  //! Drives Signal outputs to the model
  inline void drivePort(CarbonUInt32 portIndex, CarbonUInt32 portValue) {
    carbonDepositFast(mCarbonModelHandle, mNetID[portIndex], &portValue, NULL);
    // Update internal representation of the port value
    mPortValues[portIndex] = portValue;
  }
  
  //! Local method to get Net ID from signal name
  CarbonNetID* getNetIDFromName(const char* netName);
 
  //! Transactor Name
  std::string mXtorName;
  
  //! Maximum number of transactions in the queue
  CarbonUInt32 mMaxTrans;

  //! Callback function handles
  void (*mReportTransaction)(APB3_Transaction*, void*);
  void (*mNotify)(APB3_ErrorType, const char*, const APB3_Transaction*, void*);
  void (*mNotify2)(APB3_ErrorType, const char*, void*);

  //! User instance
  void* mUserInstance;

  //! Carbon model Handle
  CarbonObjectID* mCarbonModelHandle;

  //! Transactor state
  enum XtorState {
    IDLE,
    SETUP,
    ENABLE
  };
  
  //! Transactor State
  XtorState mState;

  //! Carbon Net ID's for transactor ports
  CarbonNetID* mNetID[7];

  //! Port values
  CarbonUInt32 mPortValues[7];

  //! Transaction Queue
  std::deque<APB3_Transaction*> mTransQueue;
};
