// -*- C++ -*-
/*
  Testbench and test for the apb_timer Carbon component.

  The test performs a few reads and writes to the scratch pad memory.
  Sets the timer and enables it.
  Waits for the timer interrupt and clears it.
*/
#include <iostream>
#include "TimerPVModel.h"

class TimerTest {
public:
  //! Constructor
  TimerTest();

  //! Test function
  void testFunction();
  
  //! Return error count
  CarbonUInt32 getErrors() const
  {
    std::cout << "Test had " << mErrorCount << " errors." << std::endl;
    return mErrorCount;
  }

  //! Enable backdoor accesses in the PV model
  bool         mBackdoorEn;

private:
  // Write a register
  void write(CarbonUInt32 addr, CarbonUInt32 data);

  // Read a register and check it against expected result
  void readAndCheck(CarbonUInt32 addr, CarbonUInt32 data);

  // Wait for interrupt and error if timeout is detected
  void waitForInterrupt(CarbonUInt32 expVal, CarbonUInt32 timeout);

  // Timer Model
  TimerPVModel mModel;

  //! Keep track of errors in the test
  CarbonUInt32 mErrorCount;
 };

