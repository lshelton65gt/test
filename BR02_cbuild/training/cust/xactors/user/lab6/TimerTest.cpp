/*
  Test for the apb_timer Carbon component.

  The test performs a few reads and writes to the scratch pad memory.
  Sets the timer and enables it.
  Waits for the timer interrupt and clears it.
*/

#include "TimerTest.h"

using namespace std;

//! Constructor
TimerTest::TimerTest() :
  mBackdoorEn(false),
  mErrorCount(0)
{
}

//! Test function
/*!
  The testfunction is setup like a state machine. This is needed because this testbench
  does not provide any threading so we must keep track of our current state ourself.
  Each state is a section of the test. We move to the next state when the testbench
  needs to executed other parts of the environment.
*/
void TimerTest::testFunction()
{
  // Setup Timer model
  if ( mModel.setup(mBackdoorEn) == false) {
    cout << "Error: Model Setup Failed!" << endl;
    ++mErrorCount;
    return;
  }

  // Create a few write transactions
  write(0x400, 0x1234);
  write(0x404, 0x2345);
  write(0x408, 0x3456);
    
  // Read the addresses written earlier, and see if they match
  readAndCheck(0x400, 0x1234);
  readAndCheck(0x404, 0x2345);
  readAndCheck(0x408, 0x3456);

  // Setup timer
  write(APB_TIMER_LOAD,    100);                   // Load timer value to 100 cycles
  write(APB_TIMER_CONTROL, APB_TIMER_CTRL_ENABLE); // Enable timer
  
  // Wait for interrupt, timeout after 110 cycles
  waitForInterrupt(1, 101);
  
  // Disable the timer and clear interrupt
  write(APB_TIMER_CONTROL, 0);
  write(APB_TIMER_CLEAR,   1);
  
  // Now wait for interrupt to clear
  waitForInterrupt(0, 5);
  
  // We're done, exit test
  cout << "Test is done!" << endl;
}

  // Write a register
void TimerTest::write(CarbonUInt32 addr, CarbonUInt32 data)
{
  mModel.write(addr, data);
  cout << "Wrote 0x" << hex << data << " to addr 0x" << addr << endl;  
}

// Read a register and check it against expected result
void TimerTest::readAndCheck(CarbonUInt32 addr, CarbonUInt32 expData)
{
  CarbonUInt32 readData = mModel.read(addr);
  cout << "Read 0x" << hex << readData << " from addr 0x" << addr << endl;  
  
  if(readData != expData) {
    cout << "Error: Read 0x" << hex << readData << " expected 0x" << expData << endl;
    ++mErrorCount;
  }
}

// Wait for interrupt and error if timeout is detected
void TimerTest::waitForInterrupt(CarbonUInt32 expVal, CarbonUInt32 timeout)
{
  if(mModel.waitForInterrupt(expVal, timeout)) {
    cout << "Error: Timeout while waiting for interrupt to occur." << endl;
    ++mErrorCount;
  }
}

/*!
  Main function for APB test.
*/
int main(int argc, char* argv[])
{
  // Setup testbench
  TimerTest test;  

  // Check if we should run with backdoor
  for(int i = 0; i < argc; i++) {
    if(strcmp(argv[i], "-backdoor") == 0) test.mBackdoorEn = true;
  }
  
  // Run Test
  test.testFunction();
  
  // Exit with error count as exit status
  return test.getErrors();
}
