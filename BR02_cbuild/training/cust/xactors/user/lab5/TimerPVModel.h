// -*- C++ -*-
/*
  Testbench apb_timer Carbon component.

  The testbench class is responsible for setting up the Carbon model
  and the transactor and connecting them together. It's also responsible
  for generating the clock for the model and to call the transactors
  evaluate method for every cycle.
  The testbench executes the test by for every cycle calling the
  testFunction virtual method.

  A test class is supposed to inherit from this testbench class to
  run the test.
*/
#include "libapb_timer.h"
#include "APB3_Master.h"

// Base addresses
#define     APB_TIMER_BASE  0
#define     APB_MEMORY_BASE 0x400
#define     APB_MEMORY_SIZE 0x400

// Register Address defines
#define     APB_TIMER_LOAD    0x0
#define     APB_TIMER_CURRENT 0x4
#define     APB_TIMER_CONTROL 0x8
#define     APB_TIMER_CLEAR   0xC

// Register Field defines
#define     APB_TIMER_CTRL_ENABLE   0x80
#define     APB_TIMER_CTRL_PERIODIC 0x40

//! This is the main testbench class
/*! Sets up the Carbon model and transactor and runs the test.
 */
class TimerPVModel {
public:
  //! Constructor
  TimerPVModel();

  //! Destructor
  ~TimerPVModel();
  
  //! Sets up the model
  /*!
    \retval true if model was successfully setup
    \retval false if model was not succefully setup
  */
  bool setup(bool enable_backdoor);

  //! Resets the model to its initial state
  void reset();

  //! Read a APB register
  /*!
    Perform an APB read and execute the model until the read is done.

    \param addr Address of APB register
    \return Read Daya
  */
  CarbonUInt32 read(CarbonUInt32 addr);

  //! Write a APB register
  /*!
    Perform an APB write and execute the model until the write is done.

    \param addr Address of APB register
    \param data Data to write
  */
  void write(CarbonUInt32 addr, CarbonUInt32 data);

  //! Simulate the model until an interrupt occurs
  /*!
    Execute the model until the interrupt pin matches \a expVal. If the interrupt pin already
    has the value of expVal, the method returns immediately.

    \param timeout Number of cycles to simulate until timeout occurs, 0 means wait forever
    \retval true if successfull (timeout did not occur)
    \retval false if unsuccessful (timeout did occur)
  */
  bool waitForInterrupt(CarbonUInt32 expVal, CarbonUInt32 timeout);
  
private:
  
  //! Run model for one cycle
  void runCycle();

  //! Callback function for Interrupt
  static void intMonitorCB(CarbonObjectID*, CarbonNetID*, CarbonClientData userData, CarbonUInt32* value, CarbonUInt32*);
  
  //! Report Transaction Callback function
  static void reportTransactionCB(APB3_Transaction* trans, void* clientData);
  
  //! Notify Callback function
  static void notifyCB(APB3_ErrorType, const char*, const APB3_Transaction*, void*);

  //! Notify2 Callback function
  static void notify2CB(APB3_ErrorType, const char*, void*);

  // Member variables
  CarbonObjectID*  mCarbonID;
  CarbonNetID*     mClkNetID;
  CarbonNetID*     mRstNetID;
  APB3_Master*     mMasterTrans;
  CarbonUInt32     mInterruptVal;
  APB3_Transaction mTrans;
  bool             mTransDone;
  CarbonTime       mCurrTime;
  CarbonMemoryID*  mMemoryID;
};
