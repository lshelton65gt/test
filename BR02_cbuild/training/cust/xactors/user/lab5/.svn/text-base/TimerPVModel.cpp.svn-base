/*
  Testbench apb_timer Carbon component.

  The testbench class is responsible for setting up the Carbon model
  and the transactor and connecting them together. It's also responsible
  for generating the clock for the model and to call the transactors
  evaluate method for every cycle.
  The testbench executes the test by for every cycle calling the
  testFunction virtual method.

  A test class is supposed to inherit from this testbench class to
  run the test.
*/
#include <iostream>
#include "TimerPVModel.h"

using namespace std;

//! TimerPVModel constructor
TimerPVModel::TimerPVModel() : mCarbonID(0),
                               mClkNetID(0),
                               mRstNetID(0),
                               mMasterTrans(0),
                               mInterruptVal(0),
                               mTransDone(false),
                               mCurrTime(0),
                               mMemoryID(0)
{
  memset(&mTrans, 0, sizeof(mTrans));
}

//! TimerPVModel destructor
TimerPVModel::~TimerPVModel()
{
  // Delete transactor
  delete mMasterTrans;

  // Destroy Carbon model
  carbonDestroy(&mCarbonID);
}

//! Setup carbon model and transactor
bool TimerPVModel::setup(bool enable_backdoor)
{

  // Setup Carbon model
  mCarbonID = carbon_apb_timer_create(eCarbonFullDB, eCarbon_NoFlags);
  
  // Check that the model was successfully created.
  if(mCarbonID == 0){
    std::cout << "Carbon model creation failed." << std::endl;
    return false;
  }

  // Setup nets for signals that are not connected to the transactor 
  CarbonNetID* intNetID  = carbonFindNet(mCarbonID, "apb_timer.interrupt");
  mClkNetID = carbonFindNet(mCarbonID, "apb_timer.pclk");
  mRstNetID = carbonFindNet(mCarbonID, "apb_timer.reset");
  
  if(intNetID == 0 || mClkNetID == 0 || mRstNetID == 0) {
    std::cout << "Find net failed." << std::endl;
    return false;
  }
  CarbonNetValueCBDataID* cbID = carbonAddNetValueChangeCB(mCarbonID, 
                                                           intMonitorCB, 
                                                           this,
                                                           intNetID);
  // Check that the callback was added successfully
  if(cbID == NULL) {
    std::cout << "Setup for net change callback of net apb_timer.interrupt failed." << std::endl;
    return false;
  }

  // Now Setup the transactor
  mMasterTrans = new APB3_Master("APB Master",        // Transactor Instance name
                                 5,                   // Max number of outstaning transactions 
                                 reportTransactionCB, // Report Transaction callback function
                                 notifyCB,            // Notify Callback function
                                 notify2CB,           // Notify2 Callback function
                                 this                 // User Instance (passed to callback functions)
    );
  
  // Connect the transactor to the Carbon model
  ConnectNameNamePair connectPair[] = { {"PRDATA",  "apb_timer.prdata"  },
                                        {"PREADY",  "apb_timer.pready"  },
                                        {"PADDR",   "apb_timer.paddr"   },
                                        {"PWDATA",  "apb_timer.pwdata"  },
                                        {"PSEL",    "apb_timer.psel"    },
                                        {"PENABLE", "apb_timer.penable" },
                                        {"PWRITE",  "apb_timer.pwrite"  },
                                        0};
  bool success = mMasterTrans->connect(connectPair, mCarbonID);
  
  // Check that connection was successful
  if(!success) {
    std::cout << "APB Master transactor connection failed." << std::endl;
    return false;
  }

  // Setup Wave dumping
  CarbonWaveID* waveID = carbonWaveInitFSDB(mCarbonID, "waves.fsdb", e1ns);
  carbonDumpVars(waveID, 0, "apb_timer");
  
  // Now if we made it this far we have successfully setup the environment
  return true;
}


void TimerPVModel::reset()
{
  // Note: We don't reset time in the reset routine since the model may be reset several
  //       times during the simulation
  CarbonUInt32 one = 1, zero = 0;

  // Assert Reset Signal
  carbonDepositFast(mCarbonID, mRstNetID, &one, 0);

  // Run a few clock cycles
  for(int i = 0; i < 4; i++) {
    carbonDepositFast(mCarbonID, mClkNetID, &zero, 0);
    carbonDepositFast(mCarbonID, mClkNetID, &one, 0);
    
    // Time doesn't mean anything in this environment so we're just incrementing time
    // at every schedule call
    carbonSchedule(mCarbonID, mCurrTime++);
  }

  // De-assert Reset
  carbonDepositFast(mCarbonID, mRstNetID, &zero, 0);
  carbonSchedule(mCarbonID, mCurrTime++);
}

// Read a value from the model
CarbonUInt32 TimerPVModel::read(CarbonUInt32 addr)
{
  // Lab exercise: Write code to create a read transaction
  // and execute the model until the transaction is done.
  // then return the read data to the caller.
  return 0;
}
    
// Read a value from the model
void TimerPVModel::write(CarbonUInt32 addr, CarbonUInt32 data)
{
  // Lab exercise: Write code to create a write transaction
  // and execute the model until the transaction is done.
}

// Wait for interrupt
bool TimerPVModel::waitForInterrupt(CarbonUInt32 expVal, CarbonUInt32 timeout)
{
  // Lab exercise: Write code to wait until the interrupt pin equals the
  // expVal parameter or the model times out given the number of cycles
  // specified by the timeout parameter. Return true if timeout occured
  // false if it did not occur.
  // The net change callback for the interrupt pin will update the member variable
  // mInterruptVal whenever it changes.

  return (mInterruptVal != expVal);
}

//! Runs model for one clock cycle
void TimerPVModel::runCycle()
{
  // Evaluate transactor, we may not have to evauate the transactor in this method
  // but just to be safe we still call it here.
  mMasterTrans->evaluate(mCurrTime);
  
  // Create a clock pulse
  CarbonUInt32 one = 1, zero = 0;
  carbonDepositFast(mCarbonID, mClkNetID, &zero, 0);
  carbonDepositFast(mCarbonID, mClkNetID, &one, 0);
  
  // Time doesn't mean anything in this environment so we're just incrementing time
  // at every schedule call
  carbonSchedule(mCarbonID, mCurrTime++);
  
  // Update transactor outputs
  mMasterTrans->refreshSignals();
  
  // Schedule Outputs from transactor
  carbonSchedule(mCarbonID, mCurrTime++);
}

//! Callback function for Interrupt
void TimerPVModel::intMonitorCB(CarbonObjectID*, CarbonNetID*, CarbonClientData clientData, CarbonUInt32* value, CarbonUInt32*)
{
  TimerPVModel* that = reinterpret_cast<TimerPVModel*>(clientData);
  
  // Store the interrupt value
  that->mInterruptVal = *value;
}

//! Report Transaction Callback function
void TimerPVModel::reportTransactionCB(APB3_Transaction* trans, void* clientData)
{
  TimerPVModel* that = reinterpret_cast<TimerPVModel*>(clientData);

  // Set the transaction done flag, so the API method can know that it can stop
  // executing the model.
  that->mTransDone = true;
}
  
//! Notify Callback function
void TimerPVModel::notifyCB(APB3_ErrorType error, const char* message, const APB3_Transaction* trans, void*)
{
  (void)trans;

  switch(error) {
  default:
    // Just print the error message for all error types. 
    // For more complex protocolls some specific error handling may be needed.
    std::cout << message << std::endl;
  }
}

//! Notify2 Callback function
void TimerPVModel::notify2CB(APB3_ErrorType error, const char* message, void*)
{
  switch(error) {
  default:
    // Just print the error message for all error types. 
    // For more complex protocolls some specific error handling may be needed.
    std::cout << message << std::endl;
  }
}

  
