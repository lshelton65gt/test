// -*- C++ -*-
/***************************************************************************************
  Copyright (c) 2002-2004 by Carbon Design Systems, Inc., All Rights Reserved. 
 
  THIS SOFTWARE CONTAINS  PROPRIETARY, CONFIDENTIAL AND TRADE SECRET INFORMATION
  OF CARBON DESIGN SYSTEMS, INC.  USE, MODIFICATION , COPYING  AND/OR
  DISTRIBUTION OF  THIS FILE IS PROHIBITED WITHOUT THE EXPRESS WRITTEN CONSENT
  OF CARBON DESIGN SYSTEMS, INC.
***************************************************************************************/
/*
  
  Author - <your name>

  This is a template for header files with an example of dOxygenation.
*/ 
#ifndef __TEMPLATE_H_
#define __TEMPLATE_H_


//! HierName interface
/*!
  An interface to describe a hierarchical name.
*/
class HierName
{
public:
  //! virtual destructor
  virtual ~HierName() {}

  //! Return this name string
  virtual const char* str() const = 0;

  //! Return the Parent read-only
  /*!
    \returns NULL if there is no parent
  */
  virtual const HierName* getParentName() const = 0;
  
  //! Print to STDOUT this path. Used only for debugging
  /*!
    While debugging, it becomes very difficult to know which
    name/str/scope you are looking at, so this function is needed
    to ease pain.
    
    \warning ONLY USE FOR DEBUGGING
  */
  virtual void print() const = 0;
};

#endif
