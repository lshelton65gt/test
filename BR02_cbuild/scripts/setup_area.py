#!/usr/bin/python2
##
##
## Copyright (c) 2003-2014 by Carbon Design Systems, Inc.
##
##

import os
import re
import sys
import string
import getopt

if "." not in sys.path and os.getcwd() not in sys.path:
    sys.path.append(os.getcwd())

import setup_util
import setup_hooks


##
## This where the data file gets loaded.
##
module_instances = {}
module_list = []


###
### read_data() -- This reads in simple hierarchy files where
### ech line is of the format:
###
###  <instance> <module>
###
def read_data(filename):

    global module_list
    global module_instances

    print "Reading data ..."

    ##
    ## Read the data.
    ##
    f = open(filename, "r")

    while (1):
        line = f.readline()
        if (line == ''):
            break

        ##
        ## Comment line
        ##
        if (line[0] == '#'):
            continue

        ##
        ## Valid record
        ##
        (instance, module) = string.split(line)
        if (not module_instances.has_key(module)):
            module_instances[module] = []
        module_instances[module].append(instance)

    f.close();


    ##
    ## Sort the data.
    ##
    module_list = module_instances.keys()
    module_list.sort(lambda x, y: cmp(string.lower(x), string.lower(y)))



###
### read_debussy_data() --- This reads in the hierarchy dumps
### created by Debussy.
###
def read_debussy_data(filename):

    global module_list
    global module_instances

    instance_stack = []
    depth = -1
    pervious_depth = -1
    
    record1 = re.compile("^(\s+\+?)(M):\s(?P<module>\w+)")
    record2 = re.compile("^(\s+\+?)(M):\s(?P<instance>\w+)\s\((?P<module>\w+)\)")

    print "Reading data ..."

    ##
    ## Read the data.
    ##
    f = open(filename, "r")

    while (1):
        line = f.readline()
        if (line == ''):
            break

        #print "THE DEPTH IS %d\n" % depth

        if (depth == -1):
            match = record1.match(line)
        else:
            match = record2.match(line)

        if (not match):
            continue

        ##
        ## Valid record
        ##
        if (match.group(2) != "M"):
            print "Unkown record type."
            sys.exit(-1)
            
        previous_depth = depth
        depth = ((len(match.group(1)) - 1) / 4)

        if (not (((depth == 0) and (match.lastindex == 3)) or
                 ((depth != 0) and (match.lastindex == 4)))):
            print "Bad record size.  Here is the line:", line
            sys.exit(-1)


        module = match.groupdict()["module"]
        if (depth == 0):
            instance = "top"
        else:
            instance = match.groupdict()["instance"]


        if ((depth == 1) and (previous_depth == -1)):
            instance_stack.append(instance)
        elif (previous_depth == depth):
            instance_stack.pop()
            instance_stack.append(instance)
        elif (previous_depth > depth):
            while (len(instance_stack) > depth):
                instance_stack.pop()
            instance_stack.append(instance)
        else:
            instance_stack.append(instance)

        full_instance = string.join(instance_stack, '.');
        
        if (not module_instances.has_key(module)):
            module_instances[module] = []
        module_instances[module].append(full_instance)

    f.close();


    ##
    ## Sort the data.
    ##
    module_list = module_instances.keys()
    module_list.sort(lambda x, y: cmp(string.lower(x), string.lower(y)))



##
## See if we should be setting up this module.
##
def dump_instances(filename, instance_names):
    """Dump the list, instance_names, into the file, filename.  Will not overwrite an already existing file"""
    if (not os.path.exists(filename)):
        f = open(filename, 'w')
        for instance in instance_names:
            f.write(instance + "\n")
        f.close()



##
## See if we should be setting up this module.
##
def subdir_should_be_created(dir, module_name, existing_only):
    return ((not existing_only) or (os.path.exists(os.path.join(dir, module_name))))



###
### Create the required directories and files for a single module.
###
def setup_single_module(module_name, instance_names, existing_only):
    """Set up a directory for a given module"""
    
    top_makefile = os.path.join("..", "..", "Makefile");
    top_makefile_design = os.path.join("..", "..", "Makefile.design");

    ###
    ### Rules files.
    ###
    if (subdir_should_be_created("rules", module_name, existing_only)):
        setup_util.create_directory("rules")
        setup_util.create_directory(os.path.join("rules", module_name))

    ###
    ### cwavetestbench runs.
    ###
    if (subdir_should_be_created("cwtb", module_name, existing_only)):
        setup_single_module_worker("cwtb", module_name, instance_names, existing_only)
        setup_util.create_file(os.path.join("cwtb", module_name, "cbuild.cmd"))
    
    ###
    ### Modules to be used in deployment situations.
    ###
    if (subdir_should_be_created("build", module_name, existing_only)):
        setup_single_module_worker("build", module_name, instance_names, existing_only)
        setup_util.create_file(os.path.join("build", module_name, "cbuild.cmd"))
    
    ###
    ### plitb runs.
    ###
    if (subdir_should_be_created("plitb", module_name, existing_only)):
        setup_single_module_worker("plitb", module_name, instance_names, existing_only)

    ###
    ### vcdtb runs.
    ###
    if (subdir_should_be_created("vcdtb", module_name, existing_only)):
        setup_single_module_worker("vcdtb", module_name, instance_names, existing_only)
        setup_util.create_file(os.path.join("vcdtb", module_name, "vcdtb.conf"))



def setup_single_module_worker(subdir, module_name, instance_names, existing_only):

    top_makefile = os.path.join("..", "..", "Makefile");
    top_makefile_design = os.path.join("..", "..", "Makefile.design");
    top_cbuild_design_dir = os.path.join("..", "..", "cbuild_design.dir");

    setup_util.create_directory(subdir)
    setup_util.create_directory(os.path.join(subdir, module_name))
    setup_util.link_file_internal(top_makefile, os.path.join(subdir, module_name, "Makefile"), 0, 0)
    setup_util.link_file_internal(top_makefile_design, os.path.join(os.getcwd(), subdir, module_name, "Makefile.design"), 0, 0)
    setup_util.link_file_internal(top_cbuild_design_dir, os.path.join(os.getcwd(), subdir, module_name, "cbuild_design.dir"), 0, 0)
    setup_util.create_file(os.path.join(subdir, module_name, "cbuild.dir"))
    setup_util.create_file(os.path.join(subdir, module_name, "cbuild_design.dir"))
    dump_instances(os.path.join(subdir, module_name, "instances.lis"), instance_names)



###
### Find all modules down to a particular depth in the hierarchy.
###
def filter_modules_by_depth(modules, module_instances, depth):

    filtered_modules = {}

    ##
    ## 0 means do everything.
    ##
    if (depth == 0):
        return modules

    ##
    ## Look at the paths of all the instances.  Those that
    ## are shallow enough get returned.
    ##
    for module_name in modules:
        instances = module_instances[module_name]
        for instance_name in instances:
            if (len(string.split(instance_name, '.')) <= depth):
                filtered_modules[module_name] = 1


    return filtered_modules.keys()


###
### Find all modules down to a particular depth in the hierarchy.
###
def filter_modules_by_list(modules, module_instances, user_module_list):

    filtered_modules = []

    ##
    ## Look at the paths of all the instances.  Those that
    ## are shallow enough get returned.
    ##
    for module_name in user_module_list:
        if module_name not in modules:
            print "Module name:", module_name, "not found in design"
            sys.exit(1)
        else:
            filtered_modules.append(module_name)

    # print filtered_modules
    return filtered_modules



###
### Print the hierarchy to the screen.
###
def dump_hierarchy(modules, module_instances, depth):
    filtered_module_list = filter_modules_by_depth(modules, module_instances, depth)
    for module_name in filtered_module_list:
        print module_name
        instances = module_instances[module_name]
        for instance_name in instances:
            print "  %s" % instance_name



###
### Setup the entire test aread.
###
def setup_area(modules, module_instances, template_root, design_root, depth, existing_only, user_specified_modules, user_module_list):

    print "Setting up directories ..."
    
    ##
    ## Top levels
    ##
    setup_util.create_directory("rules")
    setup_util.create_directory("cwtb")
    setup_util.create_directory("plitb")
    setup_util.create_directory("vcdtb")

    setup_util.link_file_internal(os.path.join(template_root, "Makefile"), "Makefile", 0, 0)
    setup_util.copy_file(os.path.join(template_root, "Makefile.design"), "Makefile.design")

    ##
    ## List of modules we care about.  Only setup those where
    ## the subdirectory already exists.
    ##
    if user_specified_modules:
        filtered_module_list = filter_modules_by_list(modules, module_instances, user_module_list);
    else:
        filtered_module_list = filter_modules_by_depth(modules, module_instances, depth)

    # print filtered_module_list
    
    for module_name in filtered_module_list:
        setup_single_module(module_name, module_instances[module_name], existing_only)



###
### main()
###
def main():
    global module_list
    global module_instances

    filename = "hierarchy.lis"

    ## Is this the simple hierarchy file format?
    simple_file = 0

    ## How many levels of hieararchy to setup.  0 is all levels, 1 is
    ## just the top, and so on.
    depth = 0

    ## Where are make file templates and other such stuff kept?
    if os.environ.has_key("SgeCarbonHome"):
        template_root = os.environ["SgeCarbonHome"] + "/test/cust/template/template/carbon"
    else:
        template_root = os.environ["CARBON_HOME"] + "/test/cust/template/template/carbon"

    ## Where is the customer design source living?
    dirs = string.split(os.getcwd(), '/')
    if dirs[len(dirs) - 1] != 'carbon' or len(dirs) < 5:
        print "please run from the test/cust/<CUSTOMER>/<PROJECT>/carbon directory"
        sys.exit(1)
    else:
        customer = dirs[len(dirs) - 3]
        project = dirs[len(dirs) - 2]
        print "Setting up", project, "project for", customer
    design_root = "/net/si/home/customer/" + customer + "/regression-copy/" + project

    ## Should we print out the hierarchy?
    print_hier = 0

    ## Should we only set up existing directories?
    existing_only = 1

    ## Is the user specifing specific modules to set up?
    user_specified_modules = 0
    user_module_list = []
    
    ## Overwrite existing links?
    overwrite = 0

    ## Process command line.
    optlist, args = getopt.getopt(sys.argv[1:], 'd:ef:pr:st:m:c',
                                  ["overwrite"])

    for o in optlist:
        if (o[0] == "-d"):
            depth = int(o[1])
            continue

        if (o[0] == "-f"):
            filename = o[1]
            continue

        if (o[0] == "-e"):
            existing_only = 1
            continue

        if (o[0] == "-c"):
            existing_only = 0
            continue

        if (o[0] == "-p"):
            print_hier = 1
            continue

        if (o[0] == "-r"):
            design_root = o[1]
            continue

        if (o[0] == "-s"):
            simple_file = 1
            continue

        if (o[0] == "-t"):
            template_root = o[1]
            continue

        if (o[0] == "-m"):
            user_specified_modules = 1
            user_module_list = string.split(o[1])
            continue

        if (o[0] == "--overwrite"):
            overwrite = 1
            continue
    ##
    ## Read in hierarchy data.
    ##
    if (simple_file):
        read_data(filename)
    else:
        read_debussy_data(filename)


    ##
    ## Do whatever we're doing.
    ##
    if (print_hier):
        dump_hierarchy(module_list, module_instances, depth)
    else:
        setup_hooks.setup_customer_area(template_root, design_root)
        for link in setup_util.userLinkList:
            src, dst = link
            setup_util.link_file_internal (src, dst, 1, overwrite)
        setup_area(module_list, module_instances, template_root, design_root, depth, existing_only, user_specified_modules, user_module_list)
        setup_hooks.setup_carbon_area(template_root, design_root)


##
## Execute.
##
main()
