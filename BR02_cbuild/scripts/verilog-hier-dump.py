#!/usr/bin/python

import sys
import string
import re
import getopt


re_instantiation = re.compile("^\s*(\w+)\s+(\w+)\s*\(")


##
## Names of known modules.
##
module_names = {}

##
## Has this model been instantiated anywhere?
##
module_instantiated = {}

##
## The modules and instance names instantiated in a
## particular module.
##
module_members = {}



##
## Output the hierarchy.
##
def output_hierarchy(module_name):
    print module_name + " (" + module_name + ")"
    output_hierarchy_worker(module_name, 1)


def output_hierarchy_worker(module_name, depth):

    prefix = ""
    for i in range(depth):
        prefix = prefix + "  "

    depth = depth + 1

    for inst_module_name, inst_inst_name in module_members[module_name]:
        print prefix + inst_module_name + " (" + inst_inst_name + ")"
        output_hierarchy_worker(inst_module_name, depth)


##
## Output the list of modules
##
def output_modules():
    for m in module_names.keys():
        print m


def main():


    if (len(sys.argv) < 2):
        sys.exit(1)

    optlist, args = getopt.getopt(sys.argv[1:], 'f:hlm:p')

    filename = ""
    top_module_name = ""
    traverse_primitives = 0
    list_modules = 0
    list_hierarchy = 0
    
    for o in optlist:
        if (o[0] == "-f"):
            filename = o[1]
            continue

        if (o[0] == "-m"):
            top_module_name = o[1]
            continue

        if (o[0] == "-p"):
            traverse_primitives = 1
            continue

        if (o[0] == "-l"):
            list_modules = 1
            continue

        if (o[0] == "-h"):
            list_hierarchy = 1
            continue


    ##
    ## Setup regular expressions
    ##
    if (traverse_primitives):
        re_module_name = re.compile("(module|primitive)\s+(\w+)\s*")
        re_module_end = re.compile("endmodule|endprimitive")
    else:
        re_module_name = re.compile("(module)\s+(\w+)\s*")
        re_module_end = re.compile("endmodule")




    ##
    ## Gather the module names.
    ##
    f = open(filename, "r")
    while (1):
        line = f.readline()
        if (line == ''):
            break

        match = re_module_name.match(line)
        if (match):
            module_names[match.group(2)] = []
            module_members[match.group(2)] = []
            #print match.group(2)

    f.close()


    ##
    ## Gather the instantiations contained in each module.
    ##
    f = open(filename, "r")

    in_module = 0
    current_module_name = ""

    while (1):
        line = f.readline()
        if (line == ''):
            break

        if (in_module):
            ##
            ## Are we at the end of this module?
            ##
            match = re_module_end.match(line)
            if (match):
                in_module = 0
                continue

            ##
            ## Is this an instantiation?
            ##
            match = re_instantiation.match(line)
            if (match):
                if module_names.has_key(match.group(1)):
                    instantiated_model_name = match.group(1)
                    instance_name = match.group(2)
                    module_members[current_module_name].append((instantiated_model_name, instance_name))
                    module_instantiated[instantiated_model_name] = 1
                    #print module_members[current_module_name]

        else:
            match = re_module_name.match(line)
            if (match):
                in_module = 1

                ##
                ## Make sure we know about this module.
                ##
                if (not module_names.has_key(match.group(2))):
                    print "Unknown module."
                    sys.exit(1)
                current_module_name = match.group(2)

    f.close()


    ##
    ## Find the top modules if it hasn't been specified.
    ## 
    top_modules = []
    for k in module_names.keys():
        if (not module_instantiated.has_key(k)):
            top_modules.append(k)


    ##
    ## Dump out the modules
    ##
    if (list_modules):
        output_modules()

    ##
    ## Dump out the hierarchy
    ## 
    if (list_hierarchy):
        for k in top_modules:
            output_hierarchy(k)


main()
