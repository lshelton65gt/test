#!/bin/bash
# build-release - create release area, build on Linux and Solaris and
# package the results.
#
# Usage:  build-release ~/release-area
#
# This script checks out the files needed to build a product, builds locally (for Linux)
# and (for SunOS) on sulfur, packages the release using scripts/finalize and creates
# a tar archive ~/release-area/carbon-release-<tag|date>.tar
#
# If the specified release area is a CVS sandbox, the existing sandbox contents are
# used; otherwise a cvs export is done to populate the source area.  Beware of
# the -tag <cvstag> or you won't get what you really want!
#

INSTALL=/usr/bin/install

DOC_SOURCE_DIR="/tools/CARBON_RELEASES/DOC-v11"
TEST_LOCATION=([0]=qa@hafnium:~ [1]=pescatore@sun-ae1:/home/qa/)

progname=$0

# Use sulfur unless overridden
SUNHOST=${SUNHOST:-sulfur}

unset CARBON_HOME
unset CARBON_TARGET
export CARBON_BIN=product

let warnings=0

function usage {
    echo "${progname} -tag <cvstag> build-directory"
    echo -e "\tConstruct a fully-populated release area into <build-directory>.\n\
\tThis must be a network-accessible path!\n\n\
\t-tag <cvstag>\tProvide a cvs branch tag suitable for cvs export -r <cvstag>\n"
}

function warning {
    echo "${progname}: Warning: $1"
    let warnings+=1
}

function error {
    echo "${progname}: Error: $1"
    exit 1
}

function info {
    echo "${progname}: $1"
}

if [ $# = 0 ]; then
    usage 
    exit 1
fi

if [ $1 != '-tag' ]; then
    warning "'-tag <cvstag>' missing."
    TAGNAME=$(date -I)
    TAG=
    XTAG="-D today"
else
    # Get the CVS tag argument
    shift
    TAGNAME=$1
    TAG="-r ${TAGNAME}"
    XTAG=${TAG}
    shift
fi

####
# Check if build-directory already exists
## Only checkout those pieces of the product that we really need
#
builddir=$1

if [ -e ${builddir} -a -e ${builddir}/src/CVS ]; then
    warning "${builddir} already exists and appears to be a cvs sandbox."
    cd ${builddir}
    # cvs -Q update $TAG -dP scripts src examples
else
    mkdir -p ${builddir}
    warning "${builddir} will be an exported (non-CVS) tree."
    cd ${builddir}
    cvs -Q export $XTAG scripts src examples
fi

# check if $RELEASE_TEST is set and if not, warn
if [ ! $RELEASE_TEST ] ; then
  warning "\$RELEASE_TEST env var is not set.."
  warning "will not copy files to $TEST_LOCATION upon completion"
fi

realpath=$(/bin/pwd)
info "${progname}: Building into release area ${realpath}"

rm -f build-release*.log || error "Unable to delete old log files"
rm -f runCH.* || "Unable to cleanup temporary files"

SSHCMD="ssh ${SUNHOST}"
CDEXEC=${realpath}/scripts/cdexec
FLEXLM_ENV=${realpath}/scripts/flexlm_release_env

## configure a gcc2 and a gcc3 version
CFGARGS=([0]='-gcc 2.95.3' [1]=)
CFGDIRS=([0]=295 [1]=3)

for i in 0 1
do
  CONFIGARGS="-product -keepobjs ${CFGARGS[${i}]}"

  # carbon_config -flags build/treeXX
  #
  CONFIGCMD="${CDEXEC} ${realpath} ${realpath}/scripts/carbon_config ${CONFIGARGS} -genscript runCH.${CFGDIRS[${i}]} ${realpath}/tree${CFGDIRS[${i}]}"

  # configure SunOS build area
  CMD="${SSHCMD} ${CONFIGCMD}"
  (${CMD} >>${realpath}/build-release-ccfg-SunOS.log 2>&1 ) \
      || warning "SunOS: ${CMD} returned error status ($?)"

  # configure Linux build area
  (${CONFIGCMD} >>${realpath}/build-release-ccfg-Linux.log 2>&1) \
      || warning "Linux: ${CONFIGCMD} returned error status ($?)"

done

## Configure ONE winx build area
CONFIGARGS="-product -keepobjs -winx"
CONFIGCMD="${CDEXEC} ${realpath} ${realpath}/scripts/carbon_config ${CONFIGARGS} -genscript runCH.3x ${realpath}/tree${CFGDIRS[${i}]}"
(${CONFIGCMD} >>${realpath}/build-release-ccfg-Linux.log 2>&1) \
    || warning "Linux: ${CONFIGCMD} returned error status ($?)"


## Build local and remote using both compilers
MKCDS=${realpath}/scripts/mkcds

BLDARGS=([0]=runtime     [1]=all)

for i in 0 1
do
  target=${BLDARGS[${i}]}
  dir=${CFGDIRS[${i}]}

  LOGFILE=${realpath}/build-release.${target}-${dir}.log

  (${CDEXEC} ${realpath}/tree${dir}/obj/Linux.product \
      ${realpath}/runCH.${dir} env CARBON_BIN=product ${MKCDS} -grid ${target} \
      >> ${LOGFILE} 2>&1 ) \
  || warning "errors building Linux ${dir} ${target}"

  # Check if we need to also build the windows target
  if [ $i -eq 1 ]; then
    (${CDEXEC} ${realpath}/tree${dir}/obj/Win.product \
	${realpath}/runCH.3x env CARBON_TARGET=Win.product CARBON_BIN=product ${MKCDS} -winx -grid runtime \
	>> ${LOGFILE} 2>&1 ) \
	|| warning "errors building Win ${dir} runtime"
  fi

  # Invoke on SUN, cd-ing to SunOS.product, setting the env and mkcds'ing.
  #
  (${SSHCMD} ${CDEXEC} ${realpath}/tree${dir}/obj/SunOS.product \
      ${realpath}/runCH.${dir} env CARBON_BIN=product \
          ${MKCDS} -grid ${target} >>${LOGFILE} 2>&1 ) \
      || warning "errors building SunOS ${dir} ${target}"
done

## Now some last minute stuff..

# Delete any existing install tree
rm -rf ${realpath}/install-tree || error "Unable to delete existing install-tree"
# finalize expects $CARBON_HOME to be set up.  Only need to run Linux side
# to get good results.

# We first run to fill in the gcc 2.95 stuff (only libraries)
${realpath}/runCH.295 ${realpath}/scripts/finalize ${realpath}/install-tree -target gcc2

# Then we rerun to lay the 3.x stuff down on top, replacing any binaries with
# gcc-3.x compiled executables, but leaving the gcc2 stuff alone.
${realpath}/runCH.3 ${realpath}/scripts/finalize ${realpath}/install-tree

# Add in the Windows runtime
${realpath}/runCH.3x ${realpath}/scripts/finalize ${realpath}/install-tree -target winx

# Add in the examples
cp -r ${realpath}/examples ${realpath}/install-tree


# Add top-level README, Install.txt, and gnu copyright stuff
#   Do this before adding the 'real' user documentation!
pushd ${realpath}/install-tree > /dev/null
  cvs -Q export $XTAG  userdoc/ascii-release-files
  (cd userdoc/ascii-release-files; tar cf - .) | tar xf -
  rm -rf userdoc || error "Unable to delete intermediate userdoc"
popd > /dev/null

# Add in the html & pdf end-user documenation
pushd ${realpath}/install-tree  > /dev/null
   (cd  $DOC_SOURCE_DIR;  tar cf - .)  |  tar xf -
   chmod -R u+w *
popd > /dev/null

# Add in the 'carbond' flexlm daemon
RH6_CARBOND=`$FLEXLM_ENV rh6_daemon`
SUN_CARBOND=`$FLEXLM_ENV sun4u5_daemon`
RH6_DEST=${realpath}/install-tree/Linux/bin/rh6
SUN_DEST=${realpath}/install-tree/SunOS/bin/sun4u5

if [ ! -e $RH6_DEST ]; then
    mkdir -p $RH6_DEST
fi
if [ ! -e $SUN_DEST ]; then
    mkdir -p $SUN_DEST
fi
${INSTALL} $RH6_CARBOND $RH6_DEST
${INSTALL} $SUN_CARBOND $SUN_DEST

# finish packaging
TARFILE=${realpath}/carbon-release-${TAGNAME}.tar
pushd ${realpath}/install-tree > /dev/null
tar -cf ${TARFILE} . || error "Problems creating tar file ${TARFILE}"
popd >/dev/null

info "Successfully created ${TARFILE}"
info "warnings: ${warnings}"

##echo "making license files..."
##${realpath}/scripts/MakeTestLicense ${realpath}/test

if [ $RELEASE_TEST ] ; then
  echo "transferring files to install machines for testing..."
  for i in 0 1
  do
    scp ${TARFILE} ${TEST_LOCATION[${i}]}
    scp ${realpath}/test.lic ${TEST_LOCATION[${i}]}/carbon-release-${TAGNAME}.lic
    scp ${realpath}/test.cdb ${TEST_LOCATION[${i}]}/carbon-release-${TAGNAME}.cdb
  done
fi
