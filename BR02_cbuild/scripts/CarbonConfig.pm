#==============================================================================
# CarbonConfig class.
# Author Dave Allaby. dallaby@carbondesignsystems.com
#
#==============================================================================
# Description:
# ------------
# Class is responsible for creating an initial CARBON_HOME tree. Contains the 
# subroutines which the main script, scripts/carbon_config.pl accesses.
#
# Utility classes:
# ----------------
# BomFileReader    - Processes bomfile contents.
# CarbonConfigArgs - Access to perl GetOpts. Provides the logic, default 
#                    settings for variable, variables and decision
#                    making methods based on options the user provided.
# CarbonLinksInfo  - Sets up all link -> target assignments.  Nothing should
#                    be assigned link-wise directly in the CarbonConfig.pm  
#                    file.  
#                    Links and their file targets should ONLY be assigned in 
#                    CarbonLinksInfo class.  We absolutely need things this way
#                    because packaging scripts also use this class as a way to
#                    understand what links were made by the 'carbon config'
#                    process.
#                    PLEASE, PLEASE, PLEASE see scripts/CarbonLinksInfo.pm
#                    for how to add or modify links.
# CarbonLinksMgr  -  Has Symlink management tools.
# CDSFileNames    -  Container of common file paths and file names used in the
#                    carbon world.
# CDSHosts        -  Class which contains information about localhost and all
#                    IP's and aliases of every machine on the network.
# PidSetMgr       -  A pid manager to track all pids forked by any part of this
#                    code.
#
#==============================================================================
#                        CARBON CONFIGURATION RULES
#                        --------------------------
#
# The Goals of the new methology:
# 
# 1. Accountability in link creation. Meaning:
#    Reasonable assurance that if a link was made, it points at something.
#
#    NOTE: Making a link to something that does not exist, is only allowed if 
#    the link is pointing into the object directory.
#
#    At some point, either at the end of mkcds, or as part of checkin tests,
#    we'll have to insure that non-target created links are now pointing at
#    something real.
#
# 2. Provide an information class which creates and hold all the link 
#    assignments.
#    QA has a need during the packaging process to be able to understand if 
#    packages are complete, and if not, to understand where a link that points 
#    to "null" locations got made.
#
# 3. Improve runtime performance and ease code maintenance between scripts.
#
#==========================================================================
package CarbonConfig;

use strict;
use File::Path;
use File::Copy;
use Cwd;
use File::Basename;
use lib "$ENV{CARBON_SANDBOX}/scripts";
use CarbonLinksMgr;
use CarbonConfigArgs;
use CarbonLinksInfo;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use BomFileReader;
use PidSetMgr;

#==============================================================================
# Constructor
#
# This will hold data, so we always want to new() it.
#
# Arguments:
# ----------
#   1. Instance of CDSHosts         - Provide localhost info.
#   2. Instance of CDSFileNames     - Provides commonly used paths and 
#                                     filenames.
#   3. Instance of CarbonConfigArgs - Understanding of default settings vs.
#                                     what the user has requested on the 
#                                     command line.
#
#==============================================================================
sub new
{
    my $this           = {};
    bless $this, @_[0];
    $this->{hostInfo}  = @_[1];
    $this->{fileNames} = @_[2];
    $this->{args}      = @_[3];

    # Make sure we're in the sandbox, not in $CARBON_HOME.

    if (! $this->{fileNames}->CarbonHomeNotSandbox())
    {
	print STDERR "Cannot setup a \$CARBON_HOME inside of your sandbox\n";
	$this->{invalid} = 1;
        die;
    }
    else
    {
	$this->Initialize();
    }

    return $this;
}

#==============================================================================
# Initialize classes needed internally and not passed in.  And some other
# commonly accessed information.
#==============================================================================
sub Initialize
{
    my $this = shift;
    $this->{invalid}    = 0;

    $this->{hostarch}   = $this->{hostInfo}->HostUname();
    $this->{carbonArch} = $this->{fileNames}->CarbonArch();
    $this->{osVersion}  = $this->{args}->OsVersion();

    $this->{linkInfo}   = new CarbonLinksInfo($this->{carbonArch},
                                              $this->{osVersion},
                                              $this->{fileNames},
                                              $this->{args});

    $this->{linkMgr}    = new CarbonLinksMgr($this->{args},$this->{fileNames});

    $this->{pidMgr}     = new PidSetMgr("carbon_config",$this->{fileNames});

    
    my %gccBuildOpt               = ();
    $this->{gccBuildOpt}          = \%gccBuildOpt;
    $this->{gccBuildOpt}{product} = "--disable-shared --enable-static";
    $this->{gccBuildOpt}{debug}   = "--disable-shared ".
	                            "--enable-static --enable-debug";
    $this->{gccBuildOpt}{cov}     = "--disable-shared --enable-static";
    $this->{gccBuildOpt}{gprof}   = "--disable-shared --enable-static";
}

sub CleanDeadLinks
{
    my $this      = shift;
    my $carbonHome = $this->{fileNames}->CarbonHome();
    my $locations = $this->{linkMgr}->VisitedPlaces();
    my $filter = $this->{linkMgr}->ObjectDirFilter();

    foreach my $dir (keys %{$locations})
    {
        if (CDSSystem::Cd($dir))
        {
            my @filelist = CDSSystem::OpenDir($dir);

            foreach (@filelist)
            {
                if (-l "$dir/$_")
                {
                    my $readl = readlink ("$dir/$_");

                    while ($readl && -l $readl && ! -f $readl)
                    {
                        $readl = readlink ($readl);
                    }
                        
                    if (! $readl || (! -e $readl && $readl !~ /$filter/))
                    {
                        unlink ("$dir/$_");
                    }
                }
            }
        }
    }
}

#--------------------------------------------------------------------------------
# If you forked anything using CDSSystem::ForkProcess, or by other means
# you should have caught the child pid and assigned it as such to the 
# $this->{pidMgr} instance.
# This method keeps us hanging around until all the pids the manager was watching
# have finished.
# If you haven't assigned the pid to the pid manager, it won't wait, cause it's
# not aware of it.
#--------------------------------------------------------------------------------
sub WaitForProcess
{
    my $this = shift;
    
    if ($this->{pidMgr}->NumActive() > 0)
    {
	select(STDERR);
	print "Waiting for forked processes to complete ..";
	
	while ($this->{pidMgr}->NumActive())
	{
	    print ".";
	    sleep 1;
	}
	
	print "\n";
	
	select(STDOUT);
    }
    else
    {
        print "No forked children remain.\n";
    }

    return;
}

sub WriteGenerateScript
{
    my $this = shift;
    my $carbonHome = "";
    my $sandbox    = $this->{fileNames}->FindSandboxPath();
    my $rtn        = 0;
    my $file       = "$sandbox/".$this->{args}->GenerateScript();

    if ($file =~ /icc/)
    {
        $carbonHome = "$sandbox/tree-icc";
    }
    else
    {
        $carbonHome = "$sandbox/tree3";
    }

    print "Generating $file wrapper ...\n";
    my $wh = CDSSystem::OpenWrite($file, ref($this));
    
    select $wh;

    print "#\!/bin/csh -f\n",
    "setenv CARBON_HOME $carbonHome\n",
    "set path = (\$CARBON_HOME/bin \$CARBON_HOME/scripts \$path)\n",
    "setenv ARCH `\$CARBON_HOME/bin/carbon_arch`\n",
    "setenv LD_LIBRARY_PATH \${CARBON_HOME}/\$ARCH/lib:\${LD_LIBRARY_PATH}\n",
    "exec \$*\n";
    
    CDSSystem::WriteClose($file,$wh);
    
    if (-f $file && -x $file)
    {
        $rtn = 1;
    }
    else
    {
        print "FAILED: Could not create $file.\n";
    }

    return $rtn;
}

#-------------------------------------------------------------------------
#!! Removes old CARBON_HOME area.
#-------------------------------------------------------------------------
sub WipeCarbonHome
{
    my $this   = shift;
    my $cbBak  = $this->{fileNames}->CarbonHome() .".$$";
    my $rmCmd  = join " ", $this->{fileNames}->Command("rm"),
                 "-rf $cbBak &";
    
    rename ($this->{fileNames}->CarbonHome(),, $cbBak);

    my $rtn = CDSSystem::System($rmCmd);

    CDSSystem::Mkdir ($this->{fileNames}->CarbonHome());

    return $rtn;
}

#--------------------------------------------------------------------------------
#!! Runs the default logic for carbon_config operations.
#!! The AllKeys() function of the linkInfo instance (CarbonLinksInfo.pm), will
#!! return all keys that are of type LinkGroup.
#!! See documentation in scripts/CarbonLinksInfo.pm to see how you should add
#!! your links to your own LinkGroup instance.
#--------------------------------------------------------------------------------
sub Run
{
    my $this     = shift;
    my @linkKeys = $this->{linkInfo}->AllKeys();
    my $errorCnt = 0;
    my $rtn      = 0;

# If -clean option.
#------------------
    if ($this->{args}->RemoveObjects())
    {
        $this->WipeCarbonHome();
    }

    if ($this->{args}->GenerateScript())
    {
        $this->WriteGenerateScript();
    }

# This is forked, no status returned.  Child pid assigned to pid
# manager in function.
#--------------------------------------------------------------
    $this->PythonLinks();
    $this->GccxmlLinks();
    $this->MonoLinks();

    if ($this->ExecutableDirs())
    {
        $errorCnt++;

        if ($this->MaxsimCopies())
        {
            $errorCnt++;

            foreach (sort @linkKeys)
            {
                $rtn = $this->RunLinks($_);
                
                if ($rtn > 0)
                {
                    $errorCnt++;
                    $rtn = 0;
                }
                else
                {
                    $rtn = $errorCnt;
                    last;
                }
            }
        }
        else
        {
            $rtn = $errorCnt;
        }
    }
    else
    {
        $rtn = $errorCnt;
    }

    return $rtn;
}

#--------------------------------------------------------------------------------
# This method accesses a list of link/file target assignments, using the
# CarbonLinksInfo::List() method.  To make use of this, you need to read
# the top of CarbonLinksInfo() and makes your assignments into a LinkGroup
# class.  This is how link creation should be accomplished.  Otherwise
# it is a lurking bug.
#--------------------------------------------------------------------------------
sub RunLinks
{
    my $this = shift;
    my $name = shift;
    my $rtn  = 0;
    my $here = cwd();

    if ($name)
    {
	print "Setting up $name links ...\n";

	my %list = $this->{linkInfo}->List($name);
	$this->{linkMgr}->ResetCounter();

        if ($this->{args}->ShowKey() eq "$name")
        {
            $ENV{QA_DEBUG} = 1;
        }

	foreach my $link (keys %list)
	{
	    my $linkName = "";
	    my $linkDir  = "";
	    my $target   = $list{$link};
	    ($linkName   = $link) =~ s/^.+\///g;
	    ($linkDir    = $link) =~ s/\/$linkName$//;

	    mkpath ($linkDir);

	    if (CDSSystem::Cd($linkDir))
	    {
		$this->{linkMgr}->Symlink($target, $linkName);
	    }
	}

        if ($this->{args}->ShowKey() eq "$name")
        {
            delete $ENV{QA_DEBUG};
        }

	CDSSystem::Cd($here);
    }

    if (! $this->{linkMgr}->LinkErrors())
    {
	$rtn = $this->{linkMgr}->LinkCount();
    }

    print "Made $rtn $name links ...\n";

    return $rtn;
}

#--------------------------------------------------------------------------------
# Maxsim requires a copy of a make file into a Maxsim library tree, one
# per version.
#--------------------------------------------------------------------------------
sub MaxsimCopies
{
    my $this = shift;
    my @vers = (7);
    my $rtn = 0;

    if ($this->{args}->Platform eq "Linux" || $this->{args}->Platform eq "SunOS" )
    {
        $rtn = 1;

        foreach my $v (@vers)
        {
            if ($rtn > 0)
            {
                my $mxName   = "MaxLib" . $v;
                my $arch     = $this->{args}->Platform();
                
                my $copyFile = join "/", $this->{fileNames}->FindSandboxPath(),
                               "src/xactors/socvsp/maxlib.conf";
                my $toDir    = join "/", $this->{fileNames}->CarbonHome(),
                               "lib", $mxName, $arch,"etc";
                my $toFile   = "$toDir/maxlib.conf";
                
                mkpath ($toDir);
                
                print "Copying $copyFile to\n $toFile\n";
                
                if (! copy ($copyFile, $toFile))
                {
                    $rtn = 0;
                }
            }
        }
    }
    else
    {
        $rtn = 1; # Force pass
    }

    return $rtn;
}

#--------------------------------------------------------------
# Sets up a python area inside $CARBON_HOME/$(CARBON_ARCH)
# Then copies in carbon python files into the site-packages
#--------------------------------------------------------------
sub PythonLinks
{
    my $this      = shift;
    my $pythonDir = $this->{fileNames}->ToolsPythonDir();
    my $pythonLinkTree = $this->{fileNames}->CarbonHomePythonDir();
    my $rtn = 0;

    my $pid = CDSSystem::ForkProcess (\&CarbonLinksMgr::SymlinkTree, 1, 
					  $this->{linkMgr}, $pythonDir, 
					  $pythonLinkTree) ;

    if ($pid > 0)
    {
        $rtn = 1;
        $this->{pidMgr}->AddPids($pid);
    }

    return $rtn;
}

#--------------------------------------------------------------
# Sets up a Gccxml area inside $CARBON_HOME/$(CARBON_ARCH)
#--------------------------------------------------------------
sub GccxmlLinks
{
    my $rtn = 0;
    my $this      = shift;

    if ($this->{args}->Platform eq "Linux")
    {
        my $gccxmlDir = $this->{fileNames}->CarbonToolsDir($this->{fileNames}->CarbonTargetArch())."/gccxml";

        my $gccxmlLinkTree = join "/", $this->{fileNames}->CarbonHome(), $this->{fileNames}->CarbonArch(), "gccxml";

        my $pid = CDSSystem::ForkProcess (\&CarbonLinksMgr::SymlinkTree, 1, 
					  $this->{linkMgr}, $gccxmlDir, 
					  $gccxmlLinkTree) ;

        if ($pid > 0)
        {
            $rtn = 1;
            $this->{pidMgr}->AddPids($pid);
        }
    }
    else
    {
        $rtn = 1; # force pass
    }

    return $rtn;
}

#--------------------------------------------------------------
# Sets up a mono area inside $CARBON_HOME/$(CARBON_ARCH)
# Then copies in mono files
#--------------------------------------------------------------
sub MonoLinks
{
    my $rtn = 0;
    my $this      = shift;

    if ($this->{args}->Platform eq "Linux")
    {
        my $gccxmlDir = $this->{fileNames}->CarbonToolsDir($this->{fileNames}->CarbonTargetArch())."/mono-2.10.8";

        my $gccxmlLinkTree = join "/", $this->{fileNames}->CarbonHome(), $this->{fileNames}->CarbonArch(), "mono";
        
        my $pid = CDSSystem::ForkProcess (\&CarbonLinksMgr::SymlinkTree, 1, 
					  $this->{linkMgr}, $gccxmlDir, 
					  $gccxmlLinkTree) ;

        if ($pid > 0)
        {
            $rtn = 1;
            $this->{pidMgr}->AddPids($pid);
        }
    }
    else
    {
        $rtn = 1; # force pass
    }

    return $rtn;
}


#------------------------------------------------------------------
# Writes out mkcds.options file
#------------------------------------------------------------------
sub WriteMkcdsOptions
{
    my $this = shift;
    my $rtn  = 0;
    my $mak  = "mkcds.options";

    my $wh = CDSSystem::OpenWrite($mak);
    print $wh "-cxx ", $this->{args}->Target(),"\n";

    CDSSystem::WriteClose($mak, $wh);

    if (-f $mak && ! -z $mak)
    {
	$rtn = 1;
    }
    
    return $rtn;
}

#------------------------------------------------------------------
# Creates the possible executable directories, perhaps cleaning
# them first if the -keepobjs switch is not applied.
#------------------------------------------------------------------
sub ExecutableDirs
{
    my $this = shift;
    my $displayOnly = shift;
    my %dirs = $this->{args}->ExecutableDirs();
    my $rtn = 0;
    my $here = cwd();

    if (values (%dirs) > 0)
    {
	$rtn = 1;
    }

    foreach my $d (keys %dirs)
    {
        if (! -d "$dirs{$d}")
        {
            print "Setting up object tree in $dirs{$d}\n";

            mkpath ($dirs{$d});
        }

        if (CDSSystem::Cd($dirs{$d}))
        {
            $this->WriteMakefileCompiler($dirs{$d});
            
            CDSSystem::Cd($here);
        }
    }

    return $rtn;
}

sub CleanIncludeDir
{
    my $this = shift;
    my $incDir = join "/", $this->{fileNames}->CarbonHome(),
                 "include";

    if (-d $incDir)
    {
        my $bak = $incDir . ".$$";
        move ($incDir,$bak);

        my $rmCmd = join " ", $this->{fileNames}->Command("rm"),
                    "-rf $bak &";

        CDSSystem::System($rmCmd);
    }
}

#------------------------------------------------------------------
# Writes Makefile.compiler information
#------------------------------------------------------------------
sub WriteMakefileCompiler
{
    my $this    = shift;
    my $mak     = "Compiler.cmake";
    my $rtn     = 0;
    my $wh      = CDSSystem::OpenWrite($mak);

    select ($wh);
    
    print "set(COMPILER_VERSION ", $this->{args}->GccVersion(),")\n";
    print "set(BINUTILS_VERSION ", $this->{args}->BinutilsVersion(),")\n";
    print "set(CONFIGURED_COMPILER ", $this->{args}->Target(),")\n";
    
    CDSSystem::WriteClose($mak, $wh);

    select (STDOUT);
    
    if (-f $mak && ! -z $mak)
    {
	$rtn = 1;
    }

    return $rtn;
}

#------------------------------------------------------------------
# True if CARBON_HOME path is not the same as the sandbox path
#------------------------------------------------------------------
sub ValidCarbonHomePath
{
    my $this = shift;
    return $this->{invalid};
}

1;

