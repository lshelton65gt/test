#!perl 

# CdsVM.pm - a carbon-specific wrapper around VMware's vmrun commands
# allows starting and stopping VMware virtual machines as well as copying
# files to and from it and running programs in the VM.

### TODO: make a runBatchScript routine that allows direct execution of a
###  batch file without wrapping it inside another batch file

package CdsVM;
use strict;

# NOTE: To get a usage line for all routines in this package, grep for '^###'

use Carp;
use Data::Dumper;
use FileHandle;

# For non-blocking read
use POSIX ":sys_wait_h"; 

$| = 1;
my $gDebug = 1;
sub AUTOLOAD {
    use vars qw($AUTOLOAD);
    my $name = $AUTOLOAD;
    my $self = shift;
    $self->log("Unknown method $name called!\n");
}

# check ::getErrorNum to determine pass/fail of initialization
sub new {
    my $class = shift;
    my $logFH = shift;
    my $self = {};
    
    # setup some refs for later use
    $self->{_vmxFile} = "[standard] Windows_XP_Professional/Windows_XP_Professional.vmx";
    $self->{_errNum} = 0;
    $self->{_errStr} = "";
    $self->{_commandNumber} = 0; # instead of timestamp

    # defaults for connecting to VM
    $self->{_vmHost} = "localhost";
    $self->{_vmPort} = 902;
    $self->{_vmHostUser} = 'wintest';
    $self->{_vmGuestUser} = 'wintest';
    $self->{_vmPass} = "buyit";
    $self->{_localhost} = `uname -n`;
    $self->{_sambaServer} = 'dfs';
    chomp($self->{_localhost});
    
    # drive mappings to create at login 
    my %_mounts = (
		   '\\\\'.$self->{_sambaServer}.'\\acton\\home' => "y",
		   '\\\\'.$self->{_sambaServer}.'\\acton\\work' => "x",
		   '\\\\dfs\\acton\\release' => "r",
		   '\\\\dfs\\acton\\tools' => "t",
		   '\\\\'.$self->{_localhost}.'\\w' => "w",
		   '\\\\'.$self->{_sambaServer}.'\\acton\w' => 'z',
		   '\\\\dfs\\acton\\custdata' => 's',
		   '\\\\dfs\\acton\\custregression' => 'u',
		   );
    $self->{_mounts} = \%_mounts;
    
    # make a tmpdir to use
    $self->{_tmpdir} = "/work/".$ENV{USER}."/tmp";
    # if tmp directory already exists, remove it
    if (-e $self->{_tmpdir}) {
	system('rm -rf '.$self->{_tmpdir});
    }
    # create the tmp directory
    mkdir($self->{_tmpdir});
    # set permissions for wintest to access & clear out the temp directory to start 
    system('chmod a+rw '.$self->{_tmpdir});
    
    # go ahead and bless so we can return on error
    bless $self, $class;
    
    # set up filehandle for logging
    if ($logFH) {
	$self->{_logFH} = $logFH;
    } else {
	# just make the log filehandle a dup of STDOUT
	$self->{_logFH} = \*STDOUT;
    }
    
    my $mountDrives = 1; 

    # Check to see if the VM is already running.  
    my $result = $self->systemCommand("list");
    $self->log("List result: $result");
    if ($result =~ /1/) {
	# already running so revertToSnapshot - disabled to see if it is really needed
        # now that we use "net use /permenent:yes"
#	$self->systemCommand("revertToSnapshot \"$self->{_vmxFile}\"");
#	$self->systemCommand("start \"$self->{_vmxFile}\"");
    } else {
 	# not started so start it now
	$self->systemCommand("start \"$self->{_vmxFile}\"");
    }
    
    $self->{_bat_mount} = "";
    if ($mountDrives) {
        $self->{_bat_mount} .= "net use\n";
	foreach my $uncPath (keys(%_mounts)) {
	    $self->{_bat_mount} .= "net use $_mounts{$uncPath}\: /delete\n";
	    $self->{_bat_mount} .= "net use /user:CDS\\".$self->{_vmGuestUser}." ".$_mounts{$uncPath}. ": ".$uncPath. " ".$self->{_vmPass}." /PERSISTENT:YES\n";
            $self->{_bat_mount} .= "if %errorlevel% neq 0 exit /b %errorlevel%\n";
	}
        $self->{_bat_mount} .= "net use\n";
    }
    return $self;
}

sub DESTROY {
  my $self = shift;
#  $self->log("DESTROY called:  Powering off VM\n");
#  $self->systemCommand("stop \"$self->{_vmxFile}\"");
}

### $status = powerOff ( [$timeout] ) 
# power off VM, and wait for it to not be running before returning
# return 1 on success, 0 on failure
sub powerOff {
  my $self = shift;
  my $timeout = shift;
  unless($timeout) {
    $timeout = 300;
  }

  if($gDebug) { $self->log("Powering off VM"); }
  $self->systemCommand("stop \"$self->{_vmxFile}\"");
  return 1;
}

# Found this code through google, but it didn't work. So trying a version
# from page 595 of the Perl cookbook
sub timedExec {

    my $self = shift;
    my $timeout = shift;
    my $cmd = shift;
    my $out;
    my $status = -1;
    my $errno = "";
    my $forkpid = 0;

    # Run the command
    $SIG{ALRM} = sub { die "timeout" };
    eval {
        # set timer .. hope the platform has signals
        alarm($timeout);

        # Run the sub process with a pipe between them
        if ($forkpid = open(CHILD, "-|")) {
            # Grab the childs output
            $out = "";
            while(<CHILD>) {
                # Look for line emitted with status and errno
                if ($_ =~ /child status: (-?\d+), errno: (.*)/) {
                    $status = $1;
                    $errno = $2;
                } else {
                    $out .= $_;
                }
            }
            close(CHILD);

        } elsif ($forkpid == 0) {
            # Child, run the command in its own group so we can
            # mass-kill it and its children
            setpgrp;
            $out = `$cmd`;
            $status = $?;
            if ($status != 0) {
                # errno is not reset, so only look at it if there was an error
                $errno = $!;
            }
            print $out;
            print "child status: $status, errno: $errno";
            exit;

        } else {
            # Failed to fork the process
            $status = -1;
        }

        # reset, we've got all the data and we're cool
        alarm(0);
    };

    # Check for time out
    if ($@) {
        $self->log("Eval error: '$@', '$?', '$status'");
        if ($@ =~ /timeout/) {
            $status = 100;
            if ($forkpid != 0) {
                $self->log("Killing pid: $forkpid");
                kill -15, $forkpid;
            }
        } else {
            # Some other error, clear the alarm
            $status = $?;
            print("Some other error");
            alarm(0); 
        }
    }

    # Return the status and output
    $self->log("command return with status: '$status', errno: '$errno', output: '$out'");
    return [$out, $status, $errno];
}

sub revertToSnapshot {
    my $self = shift;
    $self->systemCommand("revertToSnapshot \"$self->{_vmxFile}\"");
    $self->systemCommand("start \"$self->{_vmxFile}\"");
}
    

sub systemCommand {
    my $self = shift;
    my $mcmd = shift;
    my $retryCount = 0;
    my $notDone = 1;
    my $result;
    my $status;
    my $errno;
    my $cmd = "vmrun -T server -h https://".$self->{_localhost}.":8333/sdk -u ".$self->{_vmHostUser}." -p ".$self->{_vmPass}." ".$mcmd."\n";  # add cr to command string

    # don't allow this to fail - keep retrying (up to 2 times, with longer delays each time) if command doesn't work
    while ($notDone == 1) {
        # Run the command for a max of 30 minutes (some simulations take a while)
	$self->log($cmd);
	$result = timedExec($self, 1800, $cmd);
        $status = $result->[1];
        $errno = $result->[2];
        $self->log("Output: $result->[0]\n");
        $self->log("Status: $status\n");

        # Check the status for a non zero exit
        if ($status) {
            # Deal with the failure
            if ($status == -1) {
                $self->log("failed to execute: $errno\n");
                if ($self->{_errNum} == 0) {
                    $self->{_errNum} = -1;
                    $self->{_errStr} = "Failed to execute: $errno\n";
                }
            } elsif ($status == 100) { 
                $self->log("Timeout\n");
                if ($self->{_errNum} == 0) {
                    $self->{_errNum} = -3;
                    $self->{_errStr} = "Timeout: $errno\n";
                }
            } elsif ($status & 127) { 
                $self->log("child died with signal ".($status & 127)."\n");
                if ($self->{_errNum} == 0) {
                    $self->{_errNum} = -2;
                    my $prep = ($status & 128) ? 'with' : 'without';
                    $self->{_errStr} = "child died with signal ".($status & 127)." ".$prep." coredump\n";
                }
            } else {
                $self->log("child exited with value ".($status >> 8)."\n");
                if ($self->{_errNum} == 0) {
                    $self->{_errNum} = $status >> 8;
                    $self->{_errStr} = "child exited with value ".($status >> 8)."\n"; 
                }
            }

        } else {
            # Completed OK
            $notDone = 0;
            $self->{_errNum} = 0;
            $self->{_errStr} = "";
        }

        # Retry if it failed
	if ($notDone) {
	    $retryCount++;
	    if ($retryCount < 2) {
		my $wait = 10 * $retryCount;
		$self->log("Waiting $wait seconds before retrying...\n");
		system("sleep $wait");
	    } else {
                # Give up
                $self->log("Timeout retrying vmrun commands - check the status of the VM\n");
                $notDone = 0;
	    }
	}
    }
    return $result->[0];
}

# Function to copy a file from guest and check if it returned ok
sub copyFileFromGuest {
    my $self = shift;
    my $guestLogin = shift;
    my $vmxFileName = shift;
    my $from = shift;
    my $to = shift;

    # Copy the file and check the status
    my $status = 0;
    $self->systemCommand($guestLogin.'copyFileFromGuestToHost'.$vmxFileName." ".$from." ".$to);
    if (-s $to) {
        # File exists and is non empty
        close(TMPFILE);
    } else {
        $status = 1;
        $self->log("Failed to copy file $from to $to\n");
    }
    return $status;
}

### $cmdStatus = runCommand($cmdLine)
# given a command line, run it in the host and return the exit status of that
# command. sets errNum and errStr if unable to execute
sub runCommand {
  my $self = shift;
  my $cmd = shift;

  # don't want a buhjillion temp files laying around, but we need to make sure
  # we don't run into file caching problems. use time mod 512 to get an hour
  # window in seconds to base temp file on for linux
  # on windows, they'll get clobbered after every test anyway, and we need
  # to be very careful of trying to overwrite tests, so use seconds since epoch
  my $fullsec = time();
  my $sec = $fullsec % 512;
  my $tmpdir = $self->{_tmpdir};

  if($gDebug) { $self->log("running cmd '$cmd'"); }

  # create script to setup drive mappings
  my $setup_cmd = 'c:\tmpexec\setup.bat';
  unless(open(SETUP_TMPEXEC, ">$tmpdir/setup.bat")) {
      $self->{_errNum} = 1;
      $self->{_errStr} .= "Unable to create $tmpdir/setup.bat on host\n";
      return -1;
  }
  print SETUP_TMPEXEC "rem This batch file sets up all the drive mappings needed\n"; 
  print SETUP_TMPEXEC 'del c:\tmpexec\setupok'.$sec.'.txt'."\n"; 
  print SETUP_TMPEXEC "$self->{_bat_mount}\n"; 
  print SETUP_TMPEXEC 'echo 0 > c:\tmpexec\setupok'.$sec.'.txt'."\n"; 
  close(SETUP_TMPEXEC);
  system('chmod a+x '.$tmpdir.'/setup.bat'); # change permissions to avoid issues when calling in Windows 

  # Create the script to run the command
  unless(open(TMPEXEC, ">$tmpdir/tmpexec.$sec.bat")) {
    $self->{_errNum} = 1;
    $self->{_errStr} .= "Unable to create $tmpdir/tmpexec.$sec.bat on host\n";
    return -1;
  }
  print TMPEXEC 'del c:\tmpexec\err'.$sec.".out\n";
  print TMPEXEC "call $setup_cmd\n";  # call the initial setup script everytime to ensure drive mappings are there
  print TMPEXEC "call $cmd\n";
  print TMPEXEC 'echo %ERRORLEVEL% > c:\tmpexec\err'.$sec.".out\n";
  close(TMPEXEC);
  system('chmod a+x '.$tmpdir.'/tmpexec.'.$sec.'.bat'); # change permissions to avoid issues when calling in Windows 

  # Variables to create commands to run on the VM
  my $sq = "\'";  # single quote
  my $guestLogin = '-gu '.$self->{_vmGuestUser}.' -gp '.$self->{_vmPass}.' ';
  my $vmxFileName = " \"$self->{_vmxFile}\" ";
  my $windowsCommand = $sq.'/c c:\tmpexec\run'.$sec.'.bat > c:\tmpexec\tmp'.$sec.'.out 2>&1'.$sq;

  # Copy the run files, run the command and copy back the result files
  my $retryCount = 2;
  while ($retryCount > 0) {
      # copy execution files to host
      $self->systemCommand($guestLogin.'copyFileFromHostToGuest'.$vmxFileName.$tmpdir.'/setup.bat  "c:\tmpexec\setup.bat"');
      $self->systemCommand($guestLogin.'copyFileFromHostToGuest'.$vmxFileName.$tmpdir.'/tmpexec.'.$sec.'.bat  "c:\tmpexec\run'.$sec.'.bat"');

      # Run the command, use sleep to prevent timing issues seen between linux and windows domains
      system("sleep 3");
      $self->systemCommand($guestLogin.'runProgramInGuest      '.$vmxFileName.'cmd.exe '.$windowsCommand);
      system("sleep 3");

      # Copy the files back and check their status
      my $fileStatus = 0;
      $fileStatus += $self->copyFileFromGuest($guestLogin, $vmxFileName, $sq.'c:\tmpexec\setupok'.$sec.'.txt'.$sq, "$tmpdir/setupok".$sec.".txt");
      $fileStatus += $self->copyFileFromGuest($guestLogin, $vmxFileName, $sq.'c:\tmpexec\err'.$sec.'.out'.$sq, "$tmpdir/VMerr.$sec.out");
      $fileStatus += $self->copyFileFromGuest($guestLogin, $vmxFileName, $sq.'c:\tmpexec\tmp'.$sec.'.out'.$sq, "$tmpdir/VMtmp.$sec.out");

      # Check if we got all the requested files
      if ($fileStatus == 0) {
          # Success
          $retryCount = 0;
      } else {
          # Failed, try to revert and try again if it isn't time to give up
          if ($retryCount > 1) {
              $self->log("Command failed, reverting to snapshot\n");
              $self->revertToSnapshot();
          }
          $retryCount--;
      }
  }

  # check to see if we encountered any errors communicating with the VM
  if ($self->{_errNum} != 0) {
      return $self->{_errNum};
  }

  # open err file and get errorlevel
  unless(open(TMPERR, "$tmpdir/VMerr.$sec.out")) {
    $self->{_errNum} = 1;
    $self->{_errStr} .= "Unable to open $tmpdir/VMerr.$sec.out on host\n";
    return -1;
  }
  my @tmpinput = <TMPERR>; my $status = -1;
  close(TMPERR);
  foreach my $line (reverse(@tmpinput)) {
    # use int instead of chomp to get rid of newlines since chomp doesnt 
    # handle windows newlines correctly
    $line = int($line); 
    if ($line =~ m/^-?\d+$/) {
      $status = $line;
      last;
    }
  }
  
  if ($status == -1) {
    $self->{_errNum} = 1;
    $self->{_errStr} .= "Unable to find exit status in $tmpdir/VMerr.$sec.out\n";
    $self->{_errStr} .= "`ls -l $tmpdir`";
  } 

  ###print "returning $status from CdsVM.pm\n";
  return $status;
}

### $str = convertToWinPath($hostPath)
# convert hostPath to windows path with drive letters (but use forward slashes)
sub convertToWinPath {
  my $self = shift;
  my $hostPath = shift;

  my $_mounts = $self->{_mounts};
  my $guestPath = $hostPath;

  # now cycle through each path we know about, try to sub, if sub does something
  # then shortcircuit and return
  my $letter = $_mounts->{'\\\\'.$self->{_sambaServer}.'\\acton\\home\\'.$self->{_vmGuestUser}};
  my $home = "/home/cds/".$self->{_vmGuestUser};
  if ($guestPath =~ s#^$home#$letter:/#) {
    return $self->slashToBackslash($guestPath);
  }
  $letter = $_mounts->{'\\\\'.$self->{_sambaServer}.'\\acton\\home'};
  if ($guestPath =~ s#^/home/cds/#$letter:/#) {
    return $self->slashToBackslash($guestPath);
  }
  $letter = $_mounts->{'\\\\'.$self->{_sambaServer}.'\\acton\\work'};
  if ($guestPath =~ s#^/o/work/#$letter:/#) {
    return $self->slashToBackslash($guestPath);
  }
  $letter = $_mounts->{'\\\\dfs\\acton\\release'};
  if ($guestPath =~ s#^/[on]/release/#$letter:/#) {
    return $self->slashToBackslash($guestPath);
  }
  # /tools is already mapped, and will always be at U:
  $letter = $_mounts->{'\\\\dfs\\acton\\tools'};
  if ($guestPath =~ s#^/[on]?/?tools/#$letter:/#) {
    return $self->slashToBackslash($guestPath);
  }
  # /cust/data/regression-copies is actually a symlink to a different
  # filesystem, so it won't work as is. have to mount both FSes, and use the
  # right drive letter for the right FS
  $letter = $_mounts->{'\\\\dfs\\acton\\custregression'};
  if (($guestPath =~ s#^/cust/data/regression-copies/#$letter:/#) or
      ($guestPath =~ s#^/cust/regression/#$letter:/#)) {
    return $self->slashToBackslash($guestPath);
  }
  # which means order is significant here. subst for /cust/regression 1st
  $letter = $_mounts->{'\\\\dfs\\acton\\custdata'};
  if ($guestPath =~ s#^/cust/data/#$letter:/#) {
    return $self->slashToBackslash($guestPath);
  }

  # when looking at a work dir, could be local or another machine's work
  my $localWork = 0;
  # check through each possible hostname alias to see if its the local work
  foreach my $alias (&CdsUtil::getHostnameAliases($self->{_localhost})) {
    if ($guestPath =~ m#^/w/$alias/#) {
      $localWork = 1;
      last;
    }
  }
  # or if it's /work it's local
  if ($guestPath =~ m#^/work/#) { $localWork = 1; }

  # now use the right path
  if ($localWork) {
    $letter = $_mounts->{'\\\\'.$self->{_localhost}.'\\w'};
    if ($guestPath =~ s#^/w/\w+/#$letter:/#) {
      return $self->slashToBackslash($guestPath);
    }
    if ($guestPath =~ s#^/work/#$letter:/#) {
      return $self->slashToBackslash($guestPath);
    }
  } else {
    $letter = $_mounts->{'\\\\'.$self->{_sambaServer}.'\\acton\\w'};
    if ($guestPath =~ s#^/w/#$letter:/#) {
      return $self->slashToBackslash($guestPath);
    }
  }

  # must not know about it, so just return it (backslashed of course)
  return $self->slashToBackslash($guestPath);
}

### $str = slashToBackslash ($inPath)
sub slashToBackslash {
  my $self = shift;
  my $inPath = shift;
  my $outPath = $inPath;
 
  $outPath =~ s#/#\\#g;
  return $outPath;
}

### $int = getErrorNum()
sub getErrorNum {
  my $self = shift;
  return $self->{_errNum};
}

### $string = getErrorText()
sub getErrorText {
  my $self = shift;
  return $self->{'_errStr'};
}

### 1 = clearError()
sub clearError {
  my $self = shift;
  $self->{_errNum} = 0;
  $self->{_errStr} = "";
  return 1;
}

### log($msg)
sub log {
  my $self = shift;
  my $msg = shift;
  my $logFH = $self->{_logFH};
  print $logFH "CdsVM: $msg\n";
}

# END
1;
