@echo off
setlocal

rem Get the output library name and make a temporary directory using it
set outputLib=%1
set tmpDirName=tmp_mergelib_%~n1%
shift
mkdir %tmpDirName%
cd %tmpDirName%

rem Extract all the objects from the input libraries
:extract
if "%1"=="" goto archive
ar x %1
shift
goto extract

rem Run 'ar' to build the new archive
:archive
ar cr %outputLib% *.*

rem Run ranlib
ranlib %outputLib%

rem Clean up
del /q *.*
cd ..
rmdir /s /q %tmpDirName%
