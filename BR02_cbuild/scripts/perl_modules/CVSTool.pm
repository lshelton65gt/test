#===================================================================
# CVSTool class. Interface with cvs
#
# Author Dave Allaby
# dallaby@carbondesignsystems.com
#===================================================================
#
# This is a CVS utility which handles a lot of the manipulation
# of files in a sandbox, and manipulation of cvs adminstration files.
#
#===================================================================


package CVSTool;

use strict;
use FileHandle;
use File::Path;
use File::Copy;
use File::Basename;
use File::Find;
use Cwd;

use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSSystem;
use CDSFileNames;

my $PASS = 0;
my $FAIL = 1;

sub new
{
    my $this = {};
    my $root = @_[1];
    my $quiet = @_[2];

    bless $this, @_[0];

#--------------------------------------------
# A few ways to get CVSROOT.
#
# 1. We can pass as an argument to this class.
#    which overrides current ENV{CVSROOT}
# 2. We can use the current $ENV{CVSROOT}
# 3. If not passed, nor $CVSROOT, then if we
#    are in a sandbox, we can get one from the
#    files in the CVS/Root file.
#--------------------------------------------
    if ($root)
    {
	$this->{cvsroot} = $root;
	$ENV{CVSROOT} = $this->{cvsroot};
    }
    elsif ($ENV{CVSROOT})
    {
	$this->{cvsroot} = $ENV{CVSROOT};
    }
    else
    {
	$this->{cvsroot} = $this->GetCarbonHomeCvsRoot();
    }

#--------------------------------------------------
# If now we do have a CVSROOT, we can do something
# if not make $this = "", which will cause a compile
# crash when something tries to call a method with it.
#--------------------------------------------------
    if ($this->{cvsroot})
    {
	$ENV{CVSROOT} = $this->CvsRoot();
	$this->Initialize();

	if ($quiet)
	{
	    $this->{cvs} .= " -q";
	}
    }
    else
    {
	$this = "";
    }

    return $this;
}

#-------------------------------------------------
# Sometimes used in message output to help debugging
#-------------------------------------------------
sub ClassType
{
    return ref(@_[0]);
}

#---------------------------------------------------
# Additional Initialization stuff that's nice to have.
#---------------------------------------------------
sub Initialize
{
    my $this = shift;

    if ($ENV{QA_DEBUG})
    {
	$this->{debug} = 1;
    }
# Make file name class to get paths set.
#---------------------------------------
    $this->{fileNames}    = new CDSFileNames();

    $this->{cvs}          = $this->{fileNames}->Command("cvs");
    $this->{repositoryHome} = "/home/cvs/repository";
}


sub RepositoryHome
{
    my $this = shift;
    return $this->{repositoryHome};

}
#-----------------------------------------------------
# This calls a method which gets the date string from
# the version file and prepends a -D and adds needed
# quotes
#-----------------------------------------------------
sub SandboxDateTag
{
    my $this    = shift;
    my $dateTag = join "", "-D \"", $this->SandboxCheckoutDate(), "\"";
    
    return $dateTag;
}

#-------------------------------------------------------
# This gets the sticky tag if there is one. mainline
# doesn't have one and so this would return "" in that case.
#-------------------------------------------------------
sub SandboxTag
{
    my $this = shift;
    
    return $this->FileStickyTag($this->{fileNames}->UtVersionFile());
}

#---------------------------------------------------------
# This will convert someone elses sandbox to yours, so long
# as you now own the sandbox from a file system view.  It
# will go through and setup all the CVS/Root pserver information
# to match your user name and make it usable.
#---------------------------------------------------------

sub ChownPserverSandbox
{
    my $this = shift;
    my $sandbox = shift;
    my $here = cwd();

    if (CDSSystem::Cd($sandbox))
    {
	find (\&RenameCvsRootFile,$sandbox);

	CDSSystem::Cd($here);
    }
}

sub RenameCvsRootFile
{
    my $this = shift;
    my $file = $File::Find::name;

    if ($file =~ /CVS\/Root/)
    {
	print "Adjusting $file for new user $ENV{USER}\n";
	my $wh = CDSSystem::OpenWrite($file);
	print $wh ":pserver:$ENV{USER}\@cvs:",$this->{repositoryHome},"\n";

	write "$file";
	close ($wh);
    }
}

#----------------------------------------------------------
# This is the equiv of a cd $dir, cvs -n update to a file
# and a grep of anything that doesn't match "cvs update"
# in a dump file. This does NOT update the area, simply to 
# get a list of changed files.
#----------------------------------------------------------
sub PseudoUpdate
{
    my $this = @_[0];
    my $dir  = @_[1];

    $this->UpdateSandbox($dir,1);
}

#----------------------------------------------------------
# This is the equiv of a cd $dir, cvs update to a file
# and a grep of anything that doesn't match "cvs update"
# in a dump file.  THIS DOES UPDATE THE AREA!!!
#----------------------------------------------------------

sub UpdateSandbox
{
    my $this             = @_[0]; 
    my $dir              = @_[1];
    my $psuedoOp         = @_[2];
    my @dirs             = ();
    my $here             = cwd();
    my $dumpFile         = "/tmp/cvs.co.$ENV{USER}.$$";
    my $cmd              = join " ", "(", $this->{cvs}, "update 2>&1) >", $dumpFile;
    my @output           = ();

    if ($psuedoOp) # Add the noop switch.
    {
	$cmd =~ s/update/-n update/;
    }

    if ($dir =~ /\s/)
    {
	@dirs = split " ", $dir;
    }
    else
    {
	@dirs = ($dir);
    }

    foreach (@dirs)
    {
	my $cdDir = $this->{fileNames}->FullPath($_);

	if (CDSSystem::Cd($cdDir,$this->ClassType()))
	{
	    my $rtn = CDSSystem::System ($cmd);
	    
	    if ($rtn == $PASS)
	    {
		my $rh = CDSSystem::OpenRead ($dumpFile, $this->ClassType());
		
		while (<$rh>)
		{
		    chomp;
		    if ($_ !~ /^cvs update\:/ && $_ =~ /\w+/)
		    {
			push (@output, "$_\n");
		    }
		}
		close ($rh);
		
		unlink "$dumpFile";
	    }
	    else
	    {
		print STDERR $this->ClassType(), ": Unable to run $cmd\n";
	    }
	    
	    CDSSystem::Cd ($here, $this->ClassType());
	}
    }

    foreach (@output)
    {
	print;
    }
}

#------------------------------------------------------------
# This reads the cvs revision string, which contain a 
# commit time.  We'll use this date string to extract tests.
# We want to use the UtVersion file because this is associated
# with source commits as opposed to infrastructure commits.
#-------------------------------------------------------------
sub ReadSandboxVersionFile
{
    my $this = shift;
    my $versionFile = shift;
    my $revString = "";

    my $rh = CDSSystem::OpenRead($versionFile, $this->ClassType());
    
    while (<$rh>)
    {
        chomp;

        if ($_ =~ /sRevision/)
        {
            ($revString) = (split "=")[1];
            last;
        }
    }
    close ($rh);
    
    $revString            =~ s/^.+Date:\s//;
    $revString            =~ s/\s\$.+$//;

    return $revString;
}

sub SandboxCheckoutDate
{
    my $this = shift;

    if (! $this->{lastCiTime})
    {
        my $iverDate = $this->InfraVersionDate();
        my $uverDate = $this->UtVersionDate();

        $iverDate =~ s/\/|\:|\s//g;
        $uverDate =~ s/\/|\:|\s//g;

        if ($ENV{QA_DEBUG})
        {
            print STDERR "InfraVersion.pm's date shows as $iverDate\n",
            "UtVersion.cxx's date shows as $uverDate\n";
        }

        if ($iverDate > $uverDate)
        {
            $this->{lastCiTime} = $this->InfraVersionDate();
        }
        else
        {
            $this->{lastCiTime} = $this->UtVersionDate();
        }
    }

    return $this->{lastCiTime};
}

sub UtVersionDate
{
    my $this = shift;

    if (! $this->{uversionDate})
    {
        my $utFile    = $this->{fileNames}->UtVersionFile();
        $this->{uversionDate} = $this->ReadSandboxVersionFile($utFile);
    }

    return $this->{uversionDate};

}

sub InfraVersionDate
{
    my $this = shift;

    if (! $this->{iversionDate})
    {
        my $infraFile = $this->{fileNames}->InVersionFile();
        $this->{iversionDate} = $this->ReadSandboxVersionFile($infraFile);
    }

    return $this->{iversionDate};
}

#-----------------------------------------------------------------
# Most times we will checkout by date.  This leaves sticky dates
# in the CVS/Entries file in mainline.  (not in -r sticky tag branches)
# If there are Sticky Dates, it effectively
# disables the cvs update command and throws no message nor does it
# show a bad status.  The user will not know that they have not
# REALLY updated.  When we checkout files, we run this after
# file extraction.  
# We may need to run this standalone as well, which is supported.
#
# Recursively called.
#-----------------------------------------------------------------
sub RemoveStickyDate
{
    my $ok      = $PASS;
    my $this    = @_[0];
    my $path    = @_[1];
    my $coTag   = $this->SandboxTag();
    my $here    = cwd();
    my $entries = "CVS/Entries";
    
    if (CDSSystem::Cd ($path, $this->ClassType()))
    {
	if (-f "$entries")
	{
	    my $rh = CDSSystem::OpenRead($entries);
	    
	    while (<$rh>)
	    {
		chomp;
		my $fInfo = $_;
		
		if ($fInfo =~ /^D/)
		{
		    my ($dir) = (split /\//,$fInfo)[1];
		    
		    if ($dir ne "" && -d "$path/$dir")
		    {
			$this->RemoveStickyDate("$path/$dir", $coTag);
		    }
		}
		else
		{
		    my ($file, $fTag) = (split /\//, $fInfo)[1,5];
		    
		    if ($fTag ne "$coTag")
		    {
			$ok = $FAIL;
		    }
		}
	    }
	    
	    close ($rh);
	}
	
	#----------------------------------------------------------------------
	# Not ok means we found something that if a file, directories are
	# not tagged, release or date or otherwise, and the file needs altering.
	#----------------------------------------------------------------------
	
	if ($ok != $PASS)
	{
	    $ok = $this->CorrectEntries($entries,$coTag);
	}
	
	# Cvs might, but not always, creates a CVS/Tag file with a sticky Date
	# inside.
	
	MaybeRestoreTagFile("CVS/Tag", $coTag);
	
	CDSSystem::Cd($here,$this->ClassType());
    }
    else
    {
	$ok = $FAIL; # Couldn't cd to check this directory
    }

    return $ok;
}

#---------------------------------------------------------------------
# This cleans up CVS/Tag problems.
#
# 2 Cases:
# 
# 1. If mainline and there IS a CVS/Tag file, remove it.  It has a sticky
#    date in it.
# 2. If not mainline, there may be a CVS/Tag file.  If there is, make
#    sure the contents of it are the sticky tag, not the sticky date.
# 
# Case 2 shouldn't happen, but just in case.
#---------------------------------------------------------------------

sub MaybeRestoreTagFile
{
    my $this      = @_[0];
    my $tagFile   = @_[1];
    my $tagString = "T" . @_[2];

    if (-f "$tagFile")
    {
	if ($this->SandboxMainline())
	{
	    unlink "CVS/Tag";
	}
	else
	{
	    my $bak    = $tagFile . ".bak";
	    my $change = $FAIL;
	    my $rh = CDSSystem::OpenRead ($tagFile,$this->ClassType());
	    my $wh = CDSSystem::WriteFile($bak,$this->ClassType());

	    while (<$rh>)
	    {
		if ($_ !~ /$tagString/)
		{
		    print $wh "$tagString\n";
		    $change = $PASS;
		    last;
		}
	    }

	    write "$bak";
	    close ($rh);
	    close ($wh);

	    if ($change == $PASS)
	    {
		move ($bak, $tagFile);
	    }
	    else
	    {
		unlink "$bak";
	    }
	}
    }
}

#---------------------------------------------------------------
# This method does the actual straightening out of a single
# CVS/Entries file.
#---------------------------------------------------------------
sub CorrectEntries
{
    my $this       = @_[0];
    my $entries    = @_[1];
    my $correctTag = @_[2];
    my $bak        = $entries . ".bak";
    my $changed    = $FAIL;
    my $ok         = $FAIL;
    my $rh         = CDSSystem::OpenRead($entries);
    my $wh         = CDSSystem::OpenWrite($bak);

    while (<$rh>)
    {
	chomp;
	my ($ltag) = (split /\//)[5];

	if ($ltag ne "$correctTag")
	{
	    $_ =~ s/$ltag/$correctTag/;
	    $changed = $PASS;
	}
	
	print $wh "$_\n";
    }
    
    write "$bak";
    close ($wh);
    close ($rh);

    if ($changed == $PASS)
    {
	move ($bak, $entries);
	$ok = $PASS;
    }
    else
    {
	unlink "$bak";
    }

    return $ok;
}
#------------------------------------------------------------------
# This method does most of what cvs_sanity does, but faster and
# with more helpful messaging.  It walks a tree using the CVS/Entries
# files as a file tree map, reporting each file that does has
# sticky dates in it.  Presumably, unless the user has checked things
# out by hand,  using a -D date, or by other means other than using
# this class, this test should never fail.  This class should be
# cleaning these things up as it checks things out.
#
# Recursively called.
#------------------------------------------------------------------
sub VerifyNoStickyDates
{
    my $this    = @_[0];
    my $path    = @_[1];
    my $coTag   = $this->SandboxTag();
    my $ok      = $PASS;
    my $here    = cwd();
    my $entries = "CVS/Entries";

    CDSSystem::Cd($path,$this->ClassType());

    if (-f "$entries")
    {
	my $rh = CDSSystem::OpenRead($entries, $this->ClassType());

	while (<$rh>)
	{
	    chomp;
	    my $fInfo = $_;
	    
	    if ($fInfo =~ /^D/)
	    {
		my ($dir) = (split /\//,$fInfo)[1];

		if ($dir ne "" && -d "$path/$dir")
		{
		    $ok = $this->VerifyNoStickyDates("$path/$dir");
		}
	    }
	    else
	    {
		my ($file, $fTag) = (split /\//, $fInfo)[1,5];

		if ($fTag ne "$coTag")
		{
		    print "At $path, File $file is erroneously tagged with $fTag\n";
		    
		    if (! $coTag)
		    {
			print "Mainline should not have files with sticky tag or sticky dates.\n";
		    }
		    else
		    {
			print "This tag $fTag not match sandbox tag of $coTag\n";
		    }
		    
		    $ok = $FAIL;
		}
	    }
	}

	close ($rh);
    }
    else
    {
	print "At $path, $entries file does not exist\n";
	$ok = $FAIL;
    }

    if ($ok == $FAIL)
    {
	print "Exiting $path with return of $ok\n";
    }

    CDSSystem::Cd($here, $this->ClassType());

    return $ok;
}

#-------------------------------------------------------------
# There are variation in how cvs applies tags between something
# mainline and something that has a sticky tag.  So, now and
# then we need to ask this question.
#-------------------------------------------------------------
sub SandboxMainline
{
    my $this = shift;
    my $bool = 0;

    if ($this->SandboxTag() eq "")
    {
	$bool = 1;
    }

    return $bool;
}

sub FileStatus
{
    my $this     = @_[0];
    my $name     = @_[1];
    my %fileInfo = $this->DoStat($name);

    return $fileInfo{status};
}

#-------------------------------------------------------------
# Return Sticky Date of the file.
#-------------------------------------------------------------
sub FileStickyDate
{
    my $this     = @_[0];
    my $name     = @_[1];
    my %fileInfo = $this->DoStat($name);

    return $fileInfo{stickyDate};
}

#-------------------------------------------------------------
# Return Sticky Tag of the file.
#-------------------------------------------------------------
sub FileStickyTag
{
    my $this     = @_[0];
    my $name     = @_[1];
    my %fileInfo = $this->DoStat($name);

    return $fileInfo{stickyTag};
}

#-------------------------------------------------------------
# Return Sticky Options of the file.
#-------------------------------------------------------------
sub FileStickyOptions
{
    my $this     = @_[0];
    my $name     = @_[1];
    my %fileInfo = $this->DoStat($name);

    return $fileInfo{stickyOptions};
}

#-------------------------------------------------------------
# Return the entire output of the cvs status command for the
# passed in file.
#-------------------------------------------------------------
sub CVSStatusOutput
{
    my $this     = @_[0];
    my $name     = @_[1];
    my %fileInfo = $this->DoStat($name);
    
    return $fileInfo{cvsst};
}

#-------------------------------------------------------------
# Return Working Version of the file.
#-------------------------------------------------------------
sub FileWorkingVersion
{
    my $this     = @_[0];
    my $name     = @_[1];
    my %fileInfo = $this->DoStat($name);

    return $fileInfo{workingVersion};
}

#-------------------------------------------------------------
# Return Repository Version of the file.
#-------------------------------------------------------------
sub FileRepositoryVersion
{
    my $this     = @_[0];
    my $name     = @_[1];
    my %fileInfo = $this->DoStat($name);

    return $fileInfo{repositoryVersion};
}

#-------------------------------------------------------------
# Return path in the repository, with the ,v ext name as well
#-------------------------------------------------------------
sub FileRepositoryPath
{
    my $this     = @_[0];
    my $name     = @_[1];
    my %fileInfo = $this->DoStat($name);

    return $fileInfo{repositoryPath};
}

sub Diff
{
    my $this = shift;
    my $file = shift;
    my $outputFile = shift;
    my $diffCmd = join " ", $this->{cvs} . " -f diff -c -N ",
    $file;

    if ($outputFile)
    {
        mkpath (dirname ($outputFile));
        $diffCmd .= ">& $outputFile";
    }
    
    my @diff = CDSSystem::BackTick($diffCmd);

    return @diff;
}

sub FileLocallyModified
{
    my $this   = @_[0];
    my $name   = @_[1];
    my $bool   = 0;
    my $string = $this->FileStatus($name);

    if ($string eq "Locally Modified")
    {
	$bool = 1;
    }

    return $bool;
}

#-------------------------------------------------------------
# Returns true if working version is equal to repository version
#
# Note: This does not mean that it is identical, it could be
# locally modified.  You want to use FileLocallyModified as well.
#-------------------------------------------------------------
sub FileUpToDate
{
    my $this     = @_[0];
    my $name     = @_[1];
    my $uptodate = 0;
    my %fileInfo = $this->DoStat($name);

    if ($fileInfo{repositoryVersion} == $fileInfo{workingVersion})
    {
	$uptodate = 1;
    }

    return $uptodate;
}

sub FileCoTag
{
    my $this     = @_[0];
    my $name     = @_[1];
    my $cotag    = "";
    my %fileInfo = $this->DoStat($name);

    if ($fileInfo{stickyTag} ne "")
    {
	$cotag = "-r $fileInfo{stickyTag}";
    }

    return $cotag;
}

sub FileDateTag
{
    my $this     = @_[0];
    my $name     = @_[1];
    my $datetag  = "";
    my %fileInfo = $this->DoStat($name);

    if ($fileInfo{stickyDate} ne "")
    {
	$datetag = "-D \"$fileInfo{stickyDate}\"";
    }

    return $datetag;
}

sub DoStat
{
    my $this     = @_[0];
    my $filepath = @_[1];
    my $dir      = dirname($filepath);
    my $name     = basename($filepath);
    my $here     = cwd();
    my %file     = ();
    $file{name}  = $name;
    my $cmd      = join " ", $this->{cvs}, "st", $name;

    if (CDSSystem::Cd($dir,$this->ClassType()))
    {
	my (@statOut) = (split /\n/, CDSSystem::BackTick($cmd));
	
	foreach (@statOut)
	{
            if ($ENV{QA_DEBUG})
            {
                print "STATOUT LINE->$_\n";
            }

	    $file{cvsst} .= "$_\n";
	    
	    if ($_ =~ /^File/)
	    {
		my ($state)   = "";
		($state = $_) =~ s/^.+Status: //;
		$file{status} = "$state";
	    }
	    elsif ($_ =~ /Working revision:/)
	    {
		my $rev     = "";
		($rev = $_) =~ s/^.+revision:\s+//;
		
		$file{workingVersion} = $rev;
	    }
	    elsif ($_ =~ /Repository revision:/)
	    {
		my ($rev,$repPath)       = (split)[2,3];
		$file{repositoryVersion} = $rev;
		$file{repositoryPath}    = $repPath;
	    }
	    elsif ($_ =~ /Sticky Tag:/)
	    {
		my ($tag) = (split)[2];
		if ($tag eq "(none)")
		{
		    $tag = "";
		}
		$file{stickyTag} = $tag;
	    }
	    elsif ($_ =~ /Sticky Date:/)
	    {
		my ($tag) = (split)[2];
		if ($tag eq "(none)")
		{
		    $tag = "";
		}
		$file{stickyDate} = $tag;
	    }
	    elsif ($_ =~ /Sticky Options:/)
	    {
		my ($tag) = (split)[2];
		if ($tag eq "(none)")
		{
		    $tag = "";
		}
		$file{stickyOptions} = $tag;
	    }
	}
	
	CDSSystem::Cd($here,$this->ClassType());
    }

    return %file;
}

sub LogName
{
    my $this = shift;
    return $this->{audit};
}

sub CvsRoot
{
    my $this = shift;
    return $this->{cvsroot};
}

sub CarbonHome
{
    my $this = shift;
    return $this->{fileNames}->CarbonHome();
}

sub DeleteLogFile
{
    my $this = shift;
    unlink "$this->{audit}";
}

sub GetCarbonHomeCvsRoot
{
    my $this = shift;
    my $cvsRoot = "";

    if ($ENV{CARBON_HOME})
    {
	my $rootFile = "$ENV{CARBON_HOME}/scripts/CVS/Root";
	
	if (-f "$rootFile")
	{
	    my $rh = CDSSystem::OpenRead($rootFile, $this->ClassType());
	    
	    while (<$rh>)
	    {
		chomp;
		$cvsRoot = $_;
		    last;
	    }
	    
	    close ($rh);
	    $cvsRoot =~ s/\s//g;
	}
    }
    
    return $cvsRoot;
}

sub CoFileByDate
{
    my $this      = @_[0];
    my $repmodule = @_[1];
    my $cotag     = @_[2];
    my $codir     = @_[3];
    my $file      = 1;
    my $codate    = $this->SandboxDateTag();

    my $rtn = $this->Co($repmodule, $cotag, $codir, $file, $codate);

    return $rtn;
}

sub CoByDate
{
    my $this      = @_[0];
    my $repmodule = @_[1];
    my $cotag     = @_[2];
    my $codir     = @_[3];
    my $file      = "";
    my $codate    = $this->SandboxDateTag();

    my $rtn = $this->Co($repmodule, $cotag, $codir, $file, $codate);

    return $rtn;
}

sub CoFile
{
    my $this      = @_[0];
    my $repmodule = @_[1];
    my $cotag     = @_[2];
    my $codir     = @_[3];
    my $file      = 1;

    my $rtn       = $this->Co($repmodule, $cotag, $codir, $file);

    if (! -f $repmodule)
    {
	$rtn = 1;
    }

    return $rtn;
}

sub Co
{
    my $this      = @_[0];
    my $repmodule = @_[1];
    my $cotag     = @_[2];
    my $codir     = @_[3];
    my $file      = @_[4];
    my $codate    = @_[5];
    my $rtn       = $FAIL;
    my $here      = cwd();

    if (! $codir)
    {
	if ($file)
	{
	    $codir = dirname ($repmodule);
	}
	else
	{
	    $codir = $repmodule;
	}
    }
    
    my $cdDir = dirname($codir);
    my $dir = basename ($codir);
    $this->{audit} = $this->{fileNames}->CvsAuditFile();

    if (! -d "$cdDir")
    {
	mkpath ($cdDir);
    }
    
    if (CDSSystem::Cd($cdDir, $this->ClassType()))
    {
	if ($cotag)
	{
	    $cotag = "-r $cotag";
	}
	
        if (-f $this->{audit})
        {
            unlink ($this->{audit});
        }

	my $cmd = join " ", $this->{cvs}, $this->{quiet}, "co -d", 
	$dir, $codate, $cotag, $repmodule;
	
	$rtn = CDSSystem::System("($cmd 2>&1) > $this->{audit}");
#	$rtn = CDSSystem::System("$cmd");
	
	if ($rtn != $PASS)
	{
	    $this->PrintAuditMessage();
	}
	else
	{
	    $this->DeleteLogFile();
	}
	
	if ($codate)
	{
	    if ($this->{debug} == 1)
	    {
		print $this->ClassType(),
		": Cleaning sticky tags from dir $dir - I am here $here.\n";
	    }
	    
	    $rtn = $this->RemoveStickyDate($dir,$this->SandboxTag());
	}
	
	CDSSystem::Cd($here, $this->ClassType());
    }
    else
    {
	$rtn = 2; # couldn't change dir to checkout in the right place.
    }

    return $rtn
}

sub PrintAuditMessage
{
    my $this = shift;

    if (-f "$this->{audit}")
    {
	my $rh = CDSSystem::OpenRead($this->{audit});

	while (<$rh>)
	{
	    print STDERR $_;
	}

	close ($rh);
    }
    else
    {
        my $here = cwd();
	print STDERR "Could not access cvs output ", $this->{audit}, " file\n";
    }
}


1;
