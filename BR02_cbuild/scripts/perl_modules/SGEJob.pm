package SGEJob;

use Cwd;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";

sub new
{
    my $this = {};
    bless $this,@_[0];
    $this->{command}   = @_[1];
    $this->{jobAlias}  = @_[2];
    $this->{fileNames} = @_[3];

    $this->{stdoutDir} = cwd();
    $this->{stderrDir} = $this->{stdoutDir};

    if (! $this->{fileNames})
    {
	use CDSFileNames;
	$this->{fileNames} = new CDSFileNames();
    }

    my $rtn = $this->Submit ($command);


    return $this;
}

sub Priority
{
    my $this = shift;
    return $this->{data}{priority};
}

sub Queued
{

}

sub Running
{

}

sub AddComplexList
{
    my $this = shift;
    $this->{complexList} = shift;
    
    if ($ENV{CARBON_QSUB_ARGS})
    {
	$this->{complexList} .= " $ENV{CARBON_QSUB_ARGS}";
    }
}

sub Submit
{
    my $this = shift;
    my $qsubCmd = join " ", $this->{fileNames}->Command("qsub"), 
    "-o", $this->{stdoutDir}, "-e", $this->{stderrDir},
    $this->{complexList}, $this->{command};
    
    $ENV{QA_DEBUG} = 1;
    my $out = CDSSystem::BackTick($qsubCmd);

    print "OUT IS $out\n";

    if ($out =~ /your job .+ has been submitted/)
    {
	($this->{jobNumber}) = (split " ", $out)[2];

	$this->UpdateMyInfo();
    }

    return $this->{jobNumber};
}

sub UpdateMyInfo
{
    my $this      = shift;
    my $jobNumber = shift;
    my %data      = ();
    $this->{data} = \%data;
    my $qstatCmd  = join "", $this->{fileNames}->Command("qstat"),
    " -j ";

    if (! $jobNumber)
    {
	$jobNumber = $this->{jobNumber};
    }

    $qstatCmd .= $jobNumber;

    my @jobdata = CDSSystem::BackTick("$qstatCmd 2 >& /dev/null");

    foreach (@jobdata)
    {
	print "JOBDATA $_\n";
	my ($desc, $value) = (split /\:\s+/)[0,1];
	$this->{data}{$desc} = $value;
    }
}

sub Command
{
    my $this = shift;
    return $this->{command};
}

sub JobAlias
{
    my $this = shift;
    return $this->{jobAlias};
}



1;
