#==========================================================================
# Class Commit.
# Author Dave Allaby. dallaby@carbondesignsystems.com
#--------------------------------------------------------------------------
# 
#!! This class contains the needed routines to submit code into the 
#!! repository.  It also provides a command line interface to insert 
#!! data into the bugzilla database.
#
#!! This class must be new'() for methods to work properly.
#--------------------------------------------------------------------------

package Commit;

use strict;
use File::Copy;
use File::Path;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CVSTool;
use BugzillaInterface;
use CVSCmdFork;
use ComMsgWriter;
use ComMsgReader;
use CDSMessaging;

#--------------------------------------------------------------------
#!! Takes a CDSFileNames object
#!! and a BugzillaInterface object.
#--------------------------------------------------------------------
sub new #(&CDSFileNames, &BugzillaInterface)
{
    my $this = {};
    bless $this, @_[0];
    $this->{fileNames}  = @_[1];
    $this->{bugI} = @_[2];

    if (! $ENV{CARBON_HOME})
    {
        die "No \$CARBON_HOME is set.\n";
    }
    else
    {
        $this->Initialize();
    }

    return $this;
}
#-------------------------------------------------------------------
#!! Initializes some of the class member data.
#-------------------------------------------------------------------
sub Initialize #(void)
{
    my $this            = shift;
    $this->{cvsTool}    = new CVSTool();
    $this->{sandboxTag} = $this->{cvsTool}->SandboxTag();

    if ($this->{sandboxTag} eq "")
    {
        $this->{sandboxTag} = "mainline";
    }

    if (! $this->{bugI})
    {
        $this->{bugI} = new BugzillaInterface();
    }

# Forks cvs commands into each module and restores into a single list

    $this->{cvsCmdFork} = new CVSCmdFork($this->{fileNames},
                                           $this->{sandboxTag},
                                           "",
                                           $ENV{QA_DEBUG});

    $this->{sandbox} = $this->{fileNames}->FindSandboxPath();

# Command paths we'll use in here.

    $this->{cvs}     = $this->{fileNames}->Command("cvs");
    $this->{date}    = $this->{fileNames}->Command("date");
    $this->{editor}  = $ENV{EDITOR};
    $this->{gzip}    = $this->{fileNames}->Command("gzip");
    $this->{user}    = $ENV{USER};

# If no env EDITOR, use vi
    if (! $this->{editor})
    {
        $this->{editor} = "vi"; # Using user's $PATH.
        print "NOTE: no EDITOR set in user's environment.\n",
        "Using default -> ", $this->{editor},"\n";
    }

# Hash that hold diff output by file.  File name is the key.    
# The diff output for the file is the assignment.
    my %diffs = ();
    $this->{diffs} = \%diffs;

# Hash that holds the bug numbers that the user might select.
# The bug number is the key, the comment for the bug is the
# assignment.
    my %userSelectFix = ();
    $this->{userSelectFix} = \%userSelectFix;

# Hash to hold the files that we have verified made it into cvs.
# Key and assignment are the filename.
    my %submitted = ();
    $this->{submitted} = \%submitted;
}

#--------------------------------------------------------------
#!! If the user selected to set the bug status to fixed for 1
#!! or more bugs, then this method is called.
#!! This method brings up 2 temporary files.  One contains the
#!! commit message the user entered.  The other contains an
#!! almost empty buffer.  The second buffer has a section marked
#!! for each bug.  In these sections the user can enter comments.
#!! that will be entered into bugzilla as bugzilla comments.
#!! Returns void.
#--------------------------------------------------------------
sub BugCommentInterface #(void)
{
    my $this        = shift;
    my $tmpCom      = "/tmp/commail.".$this->{user}.".$$";
    my $tmpBug      = "/tmp/bugcom.".$this->{user}.".$$";
    my $width       = 75;
    my $enterString = "Enter comment for bug";
    my $editorCmd   = $this->{editor};
    my %msg         = ();
    $msg{line} = CDSMessaging::PrintLine("-", $width)."\n";

# Create top buffer file, populate with commit comments.
# If editor is emacs.
#-----------------------------------------------------------
    my $wh = CDSSystem::OpenWrite($tmpCom);
        
    print $wh CDSMessaging::PrintLine("-", $width),"\n",
    "NOTE: This buffer is your original commit comment for reference.\n",
    "NOTE: Changes to THIS buffer from this point will be discarded.\n",
    "\n",
    "NOTE: Enter comments for your bugs under the appropriate \n",
    "NOTE: section(s) for each bug number, in the OPPOSING buffer.\n",
    CDSMessaging::PrintLine("-", $width),"\n";
    
    foreach (@{$this->{comMsgReader}->UserComments()})
    {
        print $wh $_;
    }
    
    CDSSystem::WriteClose($tmpCom,$wh);

# Create a buffer which has sections marked off for each bug.
#------------------------------------------------------------
    $wh = CDSSystem::OpenWrite($tmpBug);

    foreach my $bugNo (sort keys %{$this->{userSelectFix}})
    {
        $msg{$bugNo} = "$enterString $bugNo:\n";
        print $wh $msg{line}, $msg{$bugNo}, "\n\n";
    }
    
    CDSSystem::WriteClose($tmpCom,$wh);

    print "\nBringing up ", $this->{editor}, " for bugzilla comments...\n";

    sleep 3;
# Difference in argument format in order to get both files into the
# single window.  Assume anything matching vi will work with the 
# same magic, and that any other editor works like emacs does.

    if ($this->{editor} =~ /(g|)vi(|m)/)
    {
        $editorCmd .= " -c ':sp' -c ':n' $tmpBug $tmpCom";
    }
    else
    {
        $editorCmd .= " $tmpCom $tmpBug";
    }

    if (CDSSystem::System ($editorCmd) == 0)
    {
        my $rh = CDSSystem::OpenRead($tmpBug);
        my $bug = 0;

        while (<$rh>)
        {
            if ($_ !~ /$msg{line}/)
            {
                if ($_ =~ /$enterString \d+:/)
                {
                    my ($m,$c) = (split ":",$_);
                    $c =~ s/^\s+//;
                    $bug = $m;
                    $bug =~ s/$enterString //;
                    $this->{userSelectFix}{$bug} = $c;
                }
                else
                {
                    $this->{userSelectFix}{$bug} .= $_;
                }
            }
        }

        close ($rh);
        unlink "$tmpBug";
    }

    if (-f $tmpCom)
    {
        unlink "$tmpCom";
    }
    
    return;
}

#-------------------------------------------------------
#!! Sub routine which ask's the user if the commit
#!! information is correct and gives them a way back
#!! to the buffer to change it.
#!! Returns true or false.
#-------------------------------------------------------
sub VerifyCorrect
{
    my $this           = shift;
    my $exact          = 0;
    my $defval         = "n";
    my %possible       = ();
    $possible{$defval} = "no";
    $possible{y}       = "yes";

# We have a problem in that something seems to zero out
# files in someone's sandbox.  If we find a modified file
# that is zero length, we'll print it out now, and this
# gives the user a chance to go back or exit.    

    if ($this->NumZeroLengthFiles())
    {
        print CDSMessaging::PrintLine("-"),"\n";

        foreach (@{$this->ZeroLengthFiles()})
        {
            print "WARNING: File to commit $_ has zero length!!!\n";
        }

        print CDSMessaging::PrintLine("-"), "\n\n";

        sleep 3;
    }

# Print out the list of modified files.
    print "\nFinalized list of files to commit:\n\n",
    CDSMessaging::PrintLine("-"),"\n";

    foreach (@{$this->{comMsgReader}->ModifiedFiles()})
    {
        chomp;
        print $this->{comMsgReader}->ModifiedCodeChar($_),"  $_\n";
    }

    print "\n\n";

# Ask them if it's correct.
    my $answer = CDSMessaging::Ask("Is this exact?", \%possible, $defval, 1);

    if ($answer->{y})
    {
        $exact = 1;
    }

    return $exact;
}

#------------------------------------------------------------
#!! Method begins after all commit constaints have been 
#!! satisfied.  We need to do some preparation before we can
#!! actually commit the files.  This method calls the other
#!! methods, and finally the commit process is called.
#!! Returns the success of the cvs commit.
#------------------------------------------------------------
sub CheckinFiles  #(void)
{
    my $this = shift;
    my $checkinOk = 1;

# We need to make a diff for all the files that the user
# has verified for commit.
    if ($this->MakeDiffs())
    {
        # We need to modify the version file so it can be committed.
        if ($this->UpdateVersionFile($this->{comMsgReader}->VersionFile()))
        {
            if ($ENV{QA_DEBUG})
            {
                print "Updated version file ", 
                $this->{comMsgReader}->VersionFile(),"\n";
            }

            if ($this->SetupCvsLogEntry())
            {
                if ($ENV{QA_DEBUG})
                {
                    print "Setup cvs log file ", $this->{cvsLogMsg}, "\n";
                }
                # Commit the files.
                $checkinOk = $this->CommitFiles();
            }
            else
            {
                print STDERR "Problems making cvs log information.\n";
            }
        }
    }

    return $checkinOk;
}

#-------------------------------------------------------
#!! Method writes out the contents we want to pass to
#!! cvs during the commit.
#-------------------------------------------------------
sub SetupCvsLogEntry
{
    my $this = shift;
    my $rtn = 0;

    $this->{cvsLogMsg} = join "/", $this->{fileNames}->TmpCommitMessageDir(),
    "cvs.log.txt";

    my $wh = CDSSystem::OpenWrite($this->{cvsLogMsg});

    my %neededField = %{$this->{comMsgWriter}->ManditoryFields()};

    foreach (sort keys %neededField)
    {
        print $wh $neededField{$_},": ",
        $this->{comMsgReader}->FieldData($neededField{$_}),"\n";
    }

    print $wh "\n";

    print $wh @{$this->{comMsgReader}->UserComments()},"\n";

    CDSSystem::WriteClose($this->{cvsLogMsg},$wh);

    if (-f $this->{cvsLogMsg} && ! -z $this->{cvsLogMsg})
    {
        $rtn = 1;
    }

    return $rtn;
}

#----------------------------------------------------------
#!! This method updates the appropriate version file from
#!! cvs, edits the file with new information.  Does a check
#!! to make sure it's changed from what is in the repository.
#----------------------------------------------------------
sub UpdateVersionFile  #(string versionfile)
{
    my $this         = shift;
    my $versionFile  = shift;
    my $rtn          = 0;
    my $loop         = 0; # counts the number of expected comment
                         # character to print after the user line.
    my @versionContents = ();

# Current time to put into version file.
    $this->{versionMod} = CDSSystem::BackTick ($this->{date});
    chomp ($this->{versionMod});
# file type, C or perl by ext.
    my $ftype        = "";
    ($ftype = $versionFile) =~ s/^.+\.//;

# Which type of comment character we'll use.
    my %commentChar      = ();
    $commentChar{"pm"}   = "#";
    $commentChar{"cxx"} = "//";

# Remove the potentially stale version file.
# Checkout new version file
    unlink ($versionFile);

    $this->{cvsTool}->CoFile ($versionFile);

# Read it in.
    my $rh = CDSSystem::OpenRead($versionFile);

    while (<$rh>)
    {
        push (@versionContents,$_);
    }

    close ($rh);

# Rewrite it.
    my $wh = CDSSystem::OpenWrite($versionFile);

    foreach (@versionContents)
    {
        if ($loop == 0)
        {
            if ($_ =~ /^$commentChar{$ftype} \w+ /)
            {
                my ($user) = (split)[1];

                if ($user eq $this->{user})
                {
                    print $wh "$commentChar{$ftype} ", $this->{user}, " ",
                    $this->{versionMod},"\n";
                }                
                elsif (! getpwnam($user))
                {
                    $loop = 5;
                }
                else
                {
                    print $wh $_;
                }
            }
            else
            {
                print $wh $_;
            }
        }
        else
        {
            $loop--;
        }
    }        

    CDSSystem::WriteClose($versionFile, $wh);

# Check that what we wrote is different from the repository.  rtn > 0 is good.
    $rtn = CDSSystem::System($this->{cvs}." diff $versionFile >& /dev/null");

    return $rtn;
}

#----------------------------------------------------------------------
#!! This takes all the files that remain after the user has approved
#!! the list and makes a diff file, in the temporary commit dir in
#!! $CARBON_HOME.
#----------------------------------------------------------------------
sub MakeDiffs  #(void)
{
    my $this             = shift;
    $this->{diffOutFile} = join "/", $this->{fileNames}->TmpCommitMessageDir(),
                           "commit_all.diff";
# We're not getting the diff on the altered version file.
    my $diffCmd          = join " ", $this->{cvs}, "-f diff",
                           " -c -N ", 
                           (join " ", @{$this->ModifiedFiles()}),
                           " >& ", $this->{diffOutFile} ;

    print "Gathering diff information ...";

    if (-f $this->{diffOutFile})
    {
        unlink ($this->{diffOutFile});
    }

    my $rtn = CDSSystem::System ($diffCmd);

    if (! -f $this->{diffOutFile} || -z $this->{diffOutFile})
    {
        die "CVS diff of files to be committed failed!!!\n",
        "Cannot continue. Contact qa.\n";
    }
    else
    {
        if ($ENV{QA_DEBUG})
        {
            print "\nGenerated standby diff file at ", 
            $this->{diffOutFile},"\n";
        }
    }

    print "\n";
}

#----------------------------------------------------------------------
# Removes the files created. Called if all files submitted.
#----------------------------------------------------------------------
sub CleanUp #()
{
    my $this = shift;
    my $tmpDir = $this->{fileNames}->TmpCommitMessageDir();
    print "Commit number: ", $this->CommitNumber(),"\n",
    "Cleaning up ...\n";

    rmtree ($tmpDir);
}

#-------------------------------------------------------------
# This method is called by CommitFiles(), and is something
# that verifies that the list of files in the sandbox are
# identical to the repository.
#-------------------------------------------------------------
sub ActuallyCommitted # (&array filelist)
{
    my $this       = shift;
    my $tried      = shift;
    my $repository = $this->{cvsTool}->RepositoryHome();
    my $diffCmd    = $this->{cvs} . " -f diff -c -N ";
    my $marker     = "Index: ";

    foreach (@{$tried})
    {
        if ($_ !~ /^$/)
        {
            $this->{submitted}{$_} = $_;
            $diffCmd .= "$_ ";
            print ".";
        }
    }

#    system("clear");
    print "\nVerifying commit on ", (scalar (@{$tried})), " files ...";

    foreach (split /\n/, CDSSystem::BackTick($diffCmd))
    {
        # If file is in diff, it's not in the repository.
        # parse string to match key, delete the key.
        if ($_ =~ /^$marker/)
        {
            my ($file) = (split /$marker/)[1]; 

            if (! $ENV{COMMIT_OVERRIDE})
            {
                delete $this->{submitted}{$file};
            }
        }
    }
    
    my $committed = values (%{$this->{submitted}});

    print "\nNumber committed: $committed\n";

    return $committed;
}

#------------------------------------------------------------
#!! This write the commit information that the user entered.
#!! It may write a "partial commit" warning if we think we
#!! couldn't get all the files into the repository.
#------------------------------------------------------------
sub StoreCommitMessage #(int partial)
{
    my $this          = shift;
    my $partialCommit = shift;
    my $rtn           = 0;
    my $storeFile     = $this->{fileNames}->CommitMessageIndexFile();
    my $lineLen       = 49; # Keeps it looking like the old file.
    my $wh            = CDSSystem::OpenAppend($storeFile);

    print $wh CDSMessaging::PrintLine("_",$lineLen),"\n",
        "Commit #  ", $this->CommitNumber()," at ", 
        $this->{versionMod}, "\n";

    if ($partialCommit)
    {
        print $wh CDSMessaging::PrintLine("=", $lineLen),"\n",
        " WARNING: This appears to have been a partial commit,\n",
        " possibly caused by problems with CVS or the commit script.\n",
        " The associated diff file may be incomplete.\n",
        CDSMessaging::PrintLine("=", $lineLen),"\n",
    }

    my %neededField = %{$this->{comMsgWriter}->ManditoryFields()};

    foreach (sort keys %neededField)
    {
        print $wh $neededField{$_},": ",
        $this->{comMsgReader}->FieldData($neededField{$_}),"\n";
    }

    print $wh "Commit was to ", $this->SandboxTag(), "\n\n\n";

    print $wh @{$this->{comMsgReader}->UserComments()},"\n",
    CDSMessaging::PrintLine("_",$lineLen),"\n";

    my $rtn = CDSSystem::WriteClose($storeFile,$wh);
        
    return $rtn;
}

#----------------------------------------------------------
#!! This writes and xml snippet to the area looked at by
#!! the intranet web server.
#!! We'll only write out bugs that the user has selected
#!! as fixed because these snippets are applied as if the 
#!! commit was a fix and would go in a release note.
#----------------------------------------------------------
sub UpdateBugXmlSnippet #( string directory_path )
{
    my $this       = shift;
    my $snippetDir = shift;
    my $releaseTag = "";
    ($releaseTag   =  $this->{cvsTool}->SandboxTag()) =~ s/BR_//;
    my $now        = CDSSystem::BackTick ($this->{date}." +%y%m%d-%H%M");
    chomp ($now);
    
    my $xmlFile    = join "/", $snippetDir, $releaseTag,
    $this->{fileNames}->BranchCommitSnippetFileName($now, $this->{user});
    
# Using Mkdir because this has friendly chmod lines. This directory made
# is used by everyone.
    if (! -d $snippetDir)
    {
        CDSSystem::Mkdir ($snippetDir);
    }
        
    my $wh = CDSSystem::OpenWrite($xmlFile);
    
    foreach my $fixedBug (sort keys %{$this->{userSelectFix}})
    {
        print $wh "<bug number=\"$fixedBug\">\n",
        "  <user>", $this->{user}, "</user>\n",
        "  <commit>", $this->CommitNumber(),"</commit>\n",
        "  <date>",$this->{versionMod},"</date>\n",
        "</bug>\n";
    }
    
    CDSSystem::WriteClose($xmlFile, $wh);
}

#----------------------------------------------------------
#!! Method returns the next available commit number.
#----------------------------------------------------------
sub CommitNumber #(string zipfile_directory)
{
    my $this = shift;
    my $dir  = shift;

    if (! $this->{commitNumber})
    {
        my @gz = CDSSystem::OpenDir($dir, ".gz");
        " $dir ";
        my $next = 0;
        
        foreach (@gz)
        {
            $_ =~ s/\.gz//;

            if ($_ > $this->{commitNumber})
            {
                $this->{commitNumber} = $_;
            }
        }
        
        $this->{commitNumber}++;
    }

    return $this->{commitNumber};
}

#------------------------------------------------------------------
#!! Method takes the list of modified files and constructs a 
#!! command line under a preset length, and executes it.  Then it
#!! calls a function which verifies that the attempted subset of 
#!! files actually got into the repository.  It will continue trying
#!! to commit while there are still files in the list, and while
#!! we cannot confirm that they've been committed, and while the
#!! try limit is not zero.
#------------------------------------------------------------------
sub CommitFiles #()
{
    my $this            = shift;
    my $actualCommitCnt = 0;
    my $logMsgLength    = (-s $this->{cvsLogMsg});
    my $cmdLenLimit     = 100000 - $logMsgLength;
    my $rtn             = 0;
    my $numMods         = $this->NumModifiedFiles();
    my $commitCmd       = join "", $this->{cvs}," -f commit -m ",
    "\"`", $this->{fileNames}->Command("cat"), " ",
    $this->{cvsLogMsg}, "`\" ";

    if ($numMods > 0)
    {
        my $attemptCnt = 3;
        print "Committing $numMods files to ", $this->SandboxTag(), "...";
        
        while (($numMods + 1) != $actualCommitCnt && $attemptCnt > 0)
        {
            my $cvsCmd  = $commitCmd;
            my @list    = ();
            my $listRef = \@list;

            for (my $i = 0; $i < $numMods; $i++)
            {
                while ((length ($cvsCmd) < $cmdLenLimit) && 
                       ($i < $numMods))
                {
                    print ".";
                    my $file = ${$this->ModifiedFiles()}[$i];
                    
                    if (! $this->{submitted}{$file})
                    {
                        $cvsCmd .= "$file ";
                        
                        push (@{$listRef},$file);
                        
                        if ($i == ($numMods - 1))
                        {
                            my $verFile = $this->{comMsgReader}->VersionFile();
                            print "\nAdding required version file $verFile", 
                            " to your list.\n";
                            
                            push (@{$listRef}, $verFile);
                            $cvsCmd .= $verFile;
                        }
                    }

                    $i++;
                }

#                my $outputFile = join "",
#                $this->{fileNames}->TmpCommitMessageDir(),
#                "/cvs_ci_com.",$attemptCnt,".out";

#                $cvsCmd .= " >& $outputFile"; 

                # Setting this will keep from actually committing.
                if ($ENV{COMMIT_OVERRIDE})
                {
                    print "Running command -> $cvsCmd\n";
                }
                else
                {
                    my $tryRtn = CDSSystem::System($cvsCmd);

                    if ($tryRtn)
                    {
                        print STDERR "Bad status return on command ->[$rtn]\n",
                        "Exact command was:\n",
                        $cvsCmd, "\n\nWe will continue, verify and try to ",
                        "resubmit if required.\n";
                    }
                }

                $actualCommitCnt = $this->ActuallyCommitted($listRef);
                
                if ($actualCommitCnt == 0)
                {
                    die "Problem with the commit script or cvs.\n",
                    "Contact qa for assistance.\n";
                }
            }

            $attemptCnt--;
        }
        
        print "Total verified submitted: $actualCommitCnt\n\n";

        $rtn = ($numMods+1) - $actualCommitCnt;

        $this->WriteOutDiff($rtn);

        $this->StoreCommitMessage($rtn);
    }
    else
    {
        print "No files found for commit.\n\n";
    }

    return $rtn;
}

#---------------------------------------------------------------
# Function creates a diff for this commit.
# If something broke and we only created a partial diff file,
# then we'll try to contruct a partial diff.  
#---------------------------------------------------------------
sub WriteOutDiff
{
    my $this          = shift;
    my $list          = shift;
    my $partialCommit = shift;
    my $finalDiff     = join "/", 
    $this->{fileNames}->CommitMessageArchive(),
    $this->CommitNumber($this->{fileNames}->CommitMessageArchive());
    
    if ($partialCommit)
    {
        my $file = "";
        my $rh   = CDSSystem::OpenRead($this->{diffOutFile});
        
        while (<$rh>)
        {
            if ($_ !~ /^cvs diff: Diffing/)
            {
                if ($_ =~ /^Index: /)
                {
                    ($file = $_) =~ s/^Index: //;
                    chomp ($file);

                    if ($file ne "")
                    {
                        print ".";
                    }
                }
                
                $this->{diffs}{"$file"} .= $_;
            }
        }

        close($rh);

        my $wh = CDSSystem::OpenWrite ($finalDiff);

        foreach (sort keys %{$this->{submitted}})
        {
            if ($this->{diffs}{$_})
            {
                print $wh $this->{diffs}{$_};
            }
        }

        CDSSystem::WriteClose($finalDiff, $wh);
    }
    else
    {
        if ($ENV{QA_DEBUG})
        {
            print "Moving ", $this->{diffOutFile}, "to $finalDiff\n";
        }

        move ($this->{diffOutFile}, $finalDiff);
    }
    
    return (CDSSystem::GzipFile ($finalDiff))
}

#------------------------------------------------------------
#!! Stub of a splash screen.  Might be extended.
#!! Returns void
#------------------------------------------------------------
sub Splash  #(void)
{
    my $this = shift;
    system ("clear");

    if ($ENV{COMMIT_OVERRIDE})
    {
        print "WARNING: \$COMMIT_OVERRIDE is set.\n",
        "NO commit will be done and psuedo interpretation of successfully ",
        "committed files will be performed.\n\n";
    }

    print "\n\nBranch ", $this->SandboxTag(), 
    " is open for commits.\n",
    "Continuing...\n";

    return;
}

#------------------------------------------------------------
#!! Method that accesses the reviewers entered by user, and
#!! performs a check to see if the user is a real user in this
#!! domain.  We'll also accept "self".
#!! Returns 1 if all reviewers are valid, 0 if at least 1 is
#!! unrecognized.
#------------------------------------------------------------
sub ValidReviewers #(void)
{
    my $this      = shift;
    my $valid     = 1;
    my $reviewers = $this->{comMsgReader}->Reviewers();

    foreach (@{$reviewers})
    {
        if (! getpwnam($_) && $_ !~ /none|self/i)
        {
            print "ERROR: Invalid reviewer name $_\n";
            $valid = 0;
        }
    }

    return $valid;
}

#------------------------------------------------------------
#!! This method uses an editor for user input to fill out
#!! additional commit information.  It also applies routines
#!! which apply commit message contrains.  If any of the
#!! contrains fail, it prompts the user if they want to go
#!! back to reenter data.
#!!
#!! We make 6 attempts for the user to get all the information
#!! in correctly.
#!!
#!! Returns 1 for a valid entry, 0 for invalid.
#------------------------------------------------------------
sub UserInterface #(void)
{
    my $this      = shift;
    my $valid     = 0;
    my $sleepTime = 3;
    my $tryCnt    = 6;
#    my $listRef   = $this->FormatCommitFileList();
    my $listRef   = $this->ModifiedFiles();

    my $editCmd = join " ", $this->{editor}, 
    $this->{comMsgWriter}->ComMsgFile();

    while (! $valid && $tryCnt > 0)
    {
        print "Bringing up commit message in ", $this->{editor}, " ...\n";
        sleep $sleepTime;

        if (CDSSystem::System($editCmd) == 0)
        {
            my $problem = "";

            #Read the file the user just closed.

            $this->{comMsgReader}->Read($this->{comMsgWriter}->ComMsgFile());

            # Check to see that all the areas we expect exist.
            if (! $this->{comMsgReader}->ValidCommitMessage())
            {
                $problem = "Entries missing from commit message.\n";
                
                $problem .= (CDSMessaging::PrintLine("-"))."\n";
                
                my $missing = $this->{comMsgReader}->MissingContentInfo();
                
                foreach my $field (keys %{$missing})
                {
                    $problem .= "  Missing Data field: $field\n";
                }
            }
            # Are the reviewers real users?
            elsif (! $this->ValidReviewers())
            {
                $problem = "Invalid username(s) given as reviewers\n";
            }
            # Are the bug numbers real bug numbers?
            elsif (! $this->ValidBugEntry($this->{comMsgReader}->Bugs()))
            {
                $problem = "Invalid bug numbers entered\n".
                    CDSMessaging::PrintLine("-")."\n";

                foreach my $num (keys %{$this->{invalidBugs}})
                {
                    $problem .= $this->{invalidBugs}{$num}."\n";
                }
            }
            # Give them one more chance to say ok.
            elsif (! $this->VerifyCorrect())
            {
                $problem = "User selected incorrect file set.  ".
                "Need to reedit the list.\n".
                    CDSMessaging::PrintLine("-")."\n";
            }
            else
            {
#                system("clear");
                print "\nCommit data accepted.\n";
                sleep 1;
                print "Continuing...\n\n";
                $valid = 1;
            }
            
            if ($problem && $tryCnt > 0)
            {
                if (! $this->QueryReturnToBuffer($problem))
                {
                    $tryCnt = 0;
                }
            }
        }

        if ($tryCnt == 0)
        {
            print STDERR "Could not get a valid commit message entry. ",
            "Giving up.\n";
        }

        $tryCnt--;
    }

    select (STDOUT);

    return $valid;
}

#----------------------------------------------------------
#!! Returns the number of bugs the user has selected to put
#!! into a fixed state.
#----------------------------------------------------------
sub FixedBugs
{
    my $this = shift;
    my $no   = values (%{$this->{userSelectFix}});

    return $no;
}

#----------------------------------------------------------
#!! This takes the bugzilla comments, one for each bug
#!! and inserts them into the bugzilla database using the
#!! BugzillaInterface class' methods.
#----------------------------------------------------------
sub UpdateBugzillaComments
{
    my $this       = shift;
    my $commitTime = $this->{cvsTool}->SandboxCheckoutDate();
    my $commitNum  = $this->CommitNumber();
    my $rtn        = 0;
    my $zipfile    = $this->{fileNames}->CommitMessageArchive().
                     "/".$commitNum.".gz";


    foreach my $bug (sort keys %{$this->{userSelectFix}})
    {
        my $comment = "Commit \#$commitNum to ".$this->SandboxTag()."\n";
        $comment   .= "Commit time: $commitTime\n";
        $comment   .= "Diff at $zipfile\n\n";
        $comment   .= $this->{userSelectFix}{$bug};
        
        print CDSMessaging::PrintLine("-"),"\n",
        "Updating bugzilla with comment information for bug $bug ...\n\n";

        if (! $this->{bugI}->AddComment($bug, $comment))
        {
            print "Problem updating comment for bug $bug\n";
            $rtn = 1;
        }
    }

    print "Done.\n";

    return $rtn;
}

#-------------------------------------------------------
#!! Method analyzes all the user selected fixed bugs
#!! to see if thier data in bugzilla needs to be updated
#!! and can be updated by the commit script.
#!!
#!! Returns 1 if update is required of at least 1 bug.
#!! Zero if no bugs need any status changed.
#-------------------------------------------------------
sub BugStatusChangePossible
{
    my $this     = shift;
    my $possible = 0;

    foreach my $num (keys %{$this->{bugData}})
    {
        if ($this->ValidForCommitScriptUpdate($num))
        {
            $possible++;
        }
    }

    return $possible;
}

#-------------------------------------------------------
#!! Applies the contraints for whether or not status
#!! change in bugzilla can be applied, to a single bug 
#!! number.
#!! Returns 1 if it can be changed, 0 if no change can be
#!! done.
#-------------------------------------------------------
sub ValidForCommitScriptUpdate  #(int bugNumber)
{
    my $this         = shift;
    my $bug          = shift;
    my $valid        = 0;
    my $alreadyFixed = 
        $this->{bugI}->AlreadyCommittedFixInBranch($bug,
                                                   $this->SandboxTag());

# Contraints:
# -----------
# 1. If the bug is assigned or new.
# 2. If the status is 'resolved' and resolution is 'fixed' BUT, the
#    bug doesn't appear to have this branch listed in it's 
#    "fix committed in" field.

    if ($this->{bugData}{$bug}{status} =~ /new|assigned/i ||
        ($this->{bugData}{$bug}{status} =~ /resolved/i &&
         $this->{bugData}{$bug}{resolution} =~ /fixed/i && ! $alreadyFixed))
    {
        $valid = 1;
    }

    if ($ENV{QA_DEBUG})
    {
        print "Already committed bug $bug is $alreadyFixed\n",
        "status is ", $this->{bugData}{$bug}{status},"\n",
        "resolution is ", $this->{bugData}{$bug}{resolution},"\n",
        "Valid is $valid\n";
    }

    return $valid;
}

#------------------------------------------------------------
#!! Method prints some information about each bug that was entered.
#!! Queries the user to select any bugs to put into a closed state.
#------------------------------------------------------------
sub QueryForFixedState  #(void)
{
    my $this               = shift;
    my %possible           = ();
    my $answer             = "";
    my $rtn                = 0;
    my $defval             = "none";
    my %userSelectFix      = ();
    $this->{userSelectFix} = \%userSelectFix;


    print "\nYour bugs that are valid for status change to 'fixed':\n",
    $this->BugDisplayHeader();

    foreach my $num (sort keys %{$this->{bugData}})
    {
        if ($this->ValidForCommitScriptUpdate($num))
        {
            $possible{$this->{bugData}{$num}{select}} = $num;
            
            print $this->FormatCommitLine($num);
        }
    }

    print "\n";

    my $max = values (%possible);                     

    if ($max > 0)
    {
        $answer = CDSMessaging::Ask("Select bugs to place in a fixed state:", 
                             \%possible, $defval, $max, "up to $max");

        if (! $answer->{default})
        {
            print "\nUser selected change to bug status.\n\n";
            $rtn = $this->SetStatusFixed($answer);
        }
        else
        {
            print "Not updating status of bugs at this time.\n";
        }
    }
    
    if ($this->FixedBugs() > 0)
    {
        $this->UpdateBugXmlSnippet($this->{fileNames}->BranchCommitSnippetDir());
    }
    
    return $rtn;
}

#------------------------------------------------------------
#!! After the user selects which bugs to set to fixed state,
#!! this method is called.  It may set the status to fixed if
#!! we are committing to the branch that matches the bug was filed
#!! against, or if it is the first commit to any branch and
#!! the bug stream is "unspecified" we will enter this branch
#!! in it's 'fixed' in field.
#------------------------------------------------------------
sub SetStatusFixed
{
    my $this  = shift;
    my $fixed = shift;
    my $rtn   = 0;

    foreach my $num (sort keys %{$fixed})
    {
        my $buggedVersion = $this->{bugData}{$fixed->{$num}}{buggedVersion};

        # If committing to something and bugged version was not specified
        # when the bugs was opened, we'll set to fixed. Else, print message
        # that we will not set this to fixed and say why.

        if ($buggedVersion eq "unspecified" ||
            $this->SandboxTag() =~ /$buggedVersion/)
        {
            print " *** Changing status of bug ", 
            $fixed->{$num}, " to 'fixed'. *** ";
            
            if ($this->{bugI}->MarkBugAsFixed($fixed->{$num}))
            {
                print " [ OK ]\n";
                $rtn++;
            }
            else
            {
                print " [ FAILED ]\n";
            }
        }
        else
        {
            print "Bug ", $fixed->{$num}, " was opened against ", 
            $this->{bugData}{$fixed->{$num}}{buggedVersion}, 
            ".  Can't modify status for commit to ", $this->SandboxTag(),"\n";
        }

        # Update Fix committed in field with this branch. Will be true if we 
        # added or whether it was already there, and we did nothing.
        if ($this->{bugI}->AddFixCommittedIn($fixed->{$num},$this->SandboxTag()))
        {
            print "Notation that this fix was committed to ", 
            $this->SandboxTag(), " has been added to \n",
            "    bug ", $fixed->{$num}, "'s 'Committed In' field.\n";
            $rtn++;
        }
        else
        {
            print "Could not update also committed in field to contain ",
            $this->SandboxTag(), " commit.\n";
        }
        
        # Set a default comment message for this bug that later the 
        # user will have a chance to overwrite.
        $this->{userSelectFix}{$fixed->{$num}} = 
            "No user comments were entered.";
    }
    
    return $rtn;
}

#----------------------------------------------------------------
#!! This is a method called whenever the user has entered data
#!! that is deemed not valid or not understood, in the commit 
#!! message.
#----------------------------------------------------------------
sub QueryReturnToBuffer
{
    my $this    = shift;
    my $problem = shift;
    my $reedit  = 0;

    if ($problem)
    {
#        CDSMessaging::ClearScreen();

        print CDSMessaging::PrintLine("-"),"\n",
        "Problem: $problem\n",
        CDSMessaging::PrintLine("-"),"\n";

        my $question = "Do you want to re-edit the commit message?";
        my %possible = ('y' => "yes",
                        'n' => "no");
        my $defval   = "y";
        my $ans      = CDSMessaging::Ask($question, \%possible, $defval, "1");
        
        if ($ans->{y} || $ans->{default})
        {
            $reedit = 1;
        }
    }

    return $reedit;
}

#-----------------------------------------------------------------
#!! True if not mainline
#-----------------------------------------------------------------
sub NeedsReleaseNote #(void)
{
    my $this = shift;
    my $bool = 0;

    if ($this->SandboxTag() ne "mainline")
    {
        $bool = 1;
    }

    return $bool;
}

#-----------------------------------------------------------------
#!! Always true for now. #(void)
#-----------------------------------------------------------------
sub NeedsBugNumber #()
{
    my $this = shift;

    return 1;
}

#------------------------------------------------------------------
#!! Method uses ComMsgReader and ComMsgWriter class to
#!! 1. Reread a previously found commit message, and try to apply it.
#!! 2. Create a new commit message.
#!! 3. Take the CVSCmdFork->Results() and write these to whichever
#!!    file got created.
#!! 4. Checks to make sure that nothing is new or conflicted, and if so
#!!    calls a method that will display the lists to the user.
#----------------------------------------------------------------
sub ProcessUpdateOutput  #()
{
    my $this     = shift;
    my $modified = 0;
    my $msgFile  =  $this->{fileNames}->TmpCommitMessageFile();
    

    $this->{comMsgWriter} = new ComMsgWriter($msgFile,
                                             $this->NeedsBugNumber(),
                                             $this->NeedsReleaseNote(),
                                             $this->{fileNames});

    $this->{comMsgReader} = new ComMsgReader ($msgFile,
                                              $this->{comMsgWriter}->DelimiterString(),
                                              $this->{comMsgWriter}->ManditoryFields(),
                                              $this->{fileNames});

    # If we found a previous commit message, we'll use the reader to read it,
    # and pass the reader to the writer which will extract the data and merge
    # it into a new commit mail.
    if (-f $this->{comMsgWriter}->ComMsgFile())
    {
        if ($ENV{QA_DEBUG})
        {
            print "Found previous commit message ...\nTrying it ....\n";
        }

        $this->{comMsgReader}->Read($this->{comMsgWriter}->ComMsgFile());
        
        $this->{comMsgWriter}->WriteComMsgFile($this->{cvsCmdFork}->Results(),
                                               $this->{comMsgReader});
    }
    else # If we have no old file, just create it and read it.
    {
        $this->{comMsgWriter}->WriteComMsgFile($this->{cvsCmdFork}->Results());
    }
    
    $this->{comMsgReader}->Read($this->{comMsgWriter}->ComMsgFile());

    # If New files and/or conflicts print message, zero out modified files
    # so the script exits clean.

    if ($this->NumNewFiles() > 0 || $this->NumConflicts() > 0)
    {
        $this->ErrorIllegalFiles();
        $modified = 0;
    }
    else
    {
        $modified = $this->NumModifiedFiles();
    }

    return $modified;
}

#-----------------------------------------------------
#!! If at the end of processing the cvs update output,
#!! We find that there are either conflicts, or new
#!! files well call this to spit out the list and
#!! instruct on what needs to be done.
#!!---------------------------------------------------
sub ErrorIllegalFiles #()
{
    my $this       = shift;
    my $qualScript = $this->{fileNames}->CommitQualifyScript();

    select STDERR;

    print "\n\nFATAL ERROR !!! \n\n";

    if ($this->NumNewFiles() > 0)
    {
        print "New files detected!!!\n",
        "You need to perform 'cvs add' on these files before running \n",
        "$qualScript:\n\n";

        foreach (@{$this->NewFiles()})
        {
            print "   $_\n";
        }

        print "\n";
    }
    if ($this->NumConflicts() > 0)
    {
        print "File conflicts detected!!!\n".
            "You need to resolve the conflicts in these files:\n";

        foreach (@{$this->ConflictFiles()})
        {
            print "   $_\n";
        }

        print "\n";
    }
}

#----------------------------------------------------------
# Runs forked cvs update command throughout the user's
# sandbox, calls the routine which will process the output.
#----------------------------------------------------------
sub StatSandbox #()
{
    my $this    = shift;
    my $changes = 0;
    my $start   = time();

    CDSSystem::Cd ($this->{sandbox});

    print "Looking for modified files at ", $this->{sandbox}, " ...";
    
    $changes = $this->{cvsCmdFork}->Run($this->{sandbox},
                                        "-n", "update");
    
    my $finish = time();
    my $elapsed = $finish - $start;
    
    print STDERR "Done.\nElasped time [$elapsed] seconds.\n";
    
    return $this->ProcessUpdateOutput();
}

#----------------------------------------------------------
#!! Returns the classes' CVSTool class reference so the
#!! caller can use it.
#----------------------------------------------------------
sub CvsTool #()
{
    my $this = shift;
    return $this->{cvsTool};
}

#-------------------------------------------------------------
#!! Method passes bug numbers into the AddBugs method.
#!! Returns true if all bugs we valid, false if any were 
#!! invalid
#-------------------------------------------------------------
sub ValidBugEntry # (string bugNumberList)
{
    my $this      = shift;
    my $bugString = shift;
    my $ok        = 0;
    
    if ($this->AddBugs($bugString) > 0 
        && ! $this->InvalidBugs())
    {
        $ok = 1;
    }

    return $ok;
}

#-------------------------------------------------------------
#!! Method to allow addition of bug numbers into the class.
#!! so that they can be analyzed and/or displayed for the
#!! commit process.  List is a space or comma separated string
#!! of bug numbers.
#-------------------------------------------------------------
sub AddBugs # (string bugNumberList)
{
    my $this      = shift;
    my $bugString = shift;
    my $cnt       = 0;
    my @bugs      = ();
    my @code      = qw (a b c d e f g h i j k l m 
                        n o p q r s t u v w x y z);
# Init invalid bugs
    my %invalidBugs = ();
    $this->{invalidBugs} = \%invalidBugs;
        
# Init bugdata
    my %bugData = ();
    $this->{bugData} = \%bugData;

# string could be separated by commas or spaces, depending
# on what the user did.  Split on either. Or assume one bug
# if we can't find commas or spaces.

    if ($bugString =~ /\,/)
    {
        @bugs = split /,/, $bugString;
    }
    elsif ($bugString =~ /\s/)
    {
        @bugs = split " ", $bugString;
    }
    else
    {
        $bugs[0] = $bugString;
    }

#---------------------------------------------------------
# Foreach bug we will try to populate the bug data from 
# bugzilla.
# Then we will look to see if this bug is something we can
# commit with.  If not, wipe out the bug's data, and store
# error message in the invalidbugs hash.
#---------------------------------------------------------
    foreach my $num (sort @bugs)
    {
        $num =~ s/\s//g;

        if (! $this->{bugI}->ValidBugNumber($num))
        {
            $this->{invalidBugs}{$num} = "Bug number $num is not a valid bug number.";
        }
        else
        {
            if (! $this->{bugData}{$num})
            {
                $this->{bugData}{$num}{owner}         = $this->{bugI}->UserNameByBugNumber($num);
                my @bugSummary                        = $this->{bugI}->SummaryDescription($num);
                $this->{bugData}{$num}{summary}       = \@bugSummary;
                $this->{bugData}{$num}{buggedVersion} = $this->{bugI}->BuggedVersion($num);
                $this->{bugData}{$num}{status}        = $this->{bugI}->BugStatus($num);
                $this->{bugData}{$num}{resolution}    = $this->{bugI}->BugResolution($num);
                $this->{bugData}{$num}{select}        = $code[$cnt];
            }

            if (! $this->ValidForCommitScriptUpdate($num))
            {
                $this->{invalidBugs}{$num} = "Bug number $num is marked as ".
                    $this->{bugData}{$num}{status}." and cannot be reused.";

                delete $this->{bugData}{$num};
            }
            else
            {
                $cnt++;
            }
        }
    }

    return $this->BugCount();
}

#----------------------------------------------------
# Returns a ref to the bugData hash.  Hash contains
# bugzilla field information.
#----------------------------------------------------
sub BugData
{
    my $this = shift;
    return $this->{bugData};
}

#----------------------------------------------------
# Returns a reference to a hash containing the invalid
# bugs.
#----------------------------------------------------
sub InvalidBugs
{
    my $this = shift;
    
    return %{$this->{invalidBugs}};
}

#----------------------------------------------------
# Return number of invalid bugs
#----------------------------------------------------
sub InvalidBugCount #()
{
    my $this    = shift;
    my $invalid = values (%{$this->{invalidBugs}});

    return $invalid;
}
#----------------------------------------------------
# Returns the number of bugs entered by the user,
# which have passed validation.
#----------------------------------------------------
sub BugCount  #()
{
    my $this = shift;
    my $count = values (%{$this->{bugData}});

    return $count;
}

#---------------------------------------------------
# Prints the formatted header for a list of bugs
#---------------------------------------------------
sub BugDisplayHeader #()
{
    my $this = shift;

    my @out = (CDSMessaging::PrintLine("="),"\n",
               "     Bug     Bug       Assigned  Bug                                   Bugged in\n",
               "     No.     Status    To        Summary                               Version\n",
               CDSMessaging::PrintLine("="),"\n\n");
    
    return @out;
}

#------------------------------------------------------------
# Prints out a formatted report of the data for the bugs
# given in this commit session by the user.
#------------------------------------------------------------
sub DisplayBugInfo #()
{
    my $this = shift;

    print $this->BugDisplayHeader(),
    CDSMessaging::PrintLine("-"),    
    "\n\nDisplaying bugzilla information for your bug numbers ...\n",
    CDSMessaging::PrintLine("-"),"\n";

    foreach my $bug (sort keys %{$this->{bugData}})
    {
        print $this->FormatCommitLine($bug);
    }

    print CDSMessaging::PrintLine("-"), "\n\n";
}

#------------------------------------------------------------------
#!! Formats a bug information for bugs add to this class using
#!! AddBugs()
#-------------------------------------------------------------
sub FormatCommitLine  #(int bugNum)
{
    my $this = shift;
    my $bug = shift;
    my $bugSt = $this->{bugData}{$bug}{status};
    my $owner = $this->{bugData}{$bug}{owner};
    my $summary = $this->{bugData}{$bug}{summary};
    my $buggedVersion = $this->{bugData}{$bug}{buggedVersion};
    my $code = $this->{bugData}{$bug}{select};
    my $newSum = "";
    
    foreach (split " ", "@$summary")
    {
        if (length ($newSum) + (length ($_) + 1) < 33)
        {
            $newSum .= "$_ ";
        }
    }
    
    my $line = sprintf (" %s)  %6d  %-10s %-8s %-35s %-20s\n", 
                        $code, $bug, $bugSt, $owner, $newSum, $buggedVersion);
    return $line;
}


#-------------------------------------------------------------------
#!! Evals on whether or not the branch is frozen. 
#-------------------------------------------------------------------
sub BranchFrozen  #()
{
    my $this    = shift;
    my $frozen  = 0;
    my $lockDir = $this->{fileNames}->CommitLockDir($this->SandboxTag());
    
    if (-d $lockDir)
    {
        $frozen = 1;
    }

    return $frozen;
}

#-------------------------------------------------------------------
# Return the current branch tag string.
#-------------------------------------------------------------------
sub SandboxTag #()
{
    my $this = shift;
    return $this->{sandboxTag};
}

#-------------------------------------------------------
#!! Returns a list of new files found in a sandbox.
#-------------------------------------------------------
sub NewFiles #()
{
    my $this = shift;
    return $this->{comMsgReader}->NewFiles();
}

#-------------------------------------------------------
#!! Returns a list of the files that are modified.
#-------------------------------------------------------
sub ModifiedFiles #()
{
    my $this = shift;
    return $this->{comMsgReader}->ModifiedFiles();
}

#-------------------------------------------------------
#!! Returns a list of the conflicted files.
#-------------------------------------------------------
sub ConflictFiles #()
{
    my $this = shift;
    return $this->{comMsgReader}->ConflictFiles();
}

#-------------------------------------------------------
#!! Returns number of zero length modified files found
#!! in the sandbox.
#-------------------------------------------------------
sub ZeroLengthFiles #()
{
    my $this = shift;
    return $this->{comMsgReader}->ZeroLengthFiles();
}

#-------------------------------------------------------
#!! Returns number of file conflicts found in the 
#!! sandbox.
#-------------------------------------------------------
sub NumConflicts #()
{
    my $this = shift;
    return (scalar @{$this->ConflictFiles()});
}

#---------------------------------------------------
#!! Returns number of new files found in a sandbox.
#---------------------------------------------------
sub NumNewFiles #()
{
    my $this = shift;
    return (scalar @{$this->NewFiles()});
}

#----------------------------------------------------------
#!! Returns number of zero length files found in a sandbox.
#----------------------------------------------------------
sub NumZeroLengthFiles #()
{
    my $this = shift;
    return (scalar @{$this->ZeroLengthFiles()});
}

#-------------------------------------------------------
#!! Returns number of modified files found in a sandbox.
#-------------------------------------------------------
sub NumModifiedFiles #()
{
    my $this = shift;
    return (scalar (@{$this->ModifiedFiles()}));
}


1; # END CLASS
