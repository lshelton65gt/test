

package CDSDistPackage;

use strict;
use Getopt::Long;
use File::Basename;
use File::Find;
use File::Path;

use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CmdPaths;
use CDSAudit;
use CDSHosts;
use CDSFileNames;
use CDSSystem;

sub new
{
    my $this = {};
    bless $this, @_[0];
    $this->{branchHandle} = @_[1];
    $this->{fileNames}    = @_[2];

    if (! $this->{fileNames})
    {
	$this->{fileNames} = new CDSFileNames();
    }

    $this->{hostInfo} = $this->{fileNames}->CdsHosts();

    $this->Initialize();

    return $this;
}

sub Initialize
{
    my $this = shift;
    $this->{sandbox}     = $ENV{CARBON_HOME};
    $this->{installRoot} = $this->{fileNames}->InstallTreePath();

    $this->{audit} = join "/", $this->{sandbox}, 
                    $this->{fileNames}->AuditFileName();

    print "Audit file name is ", $this->{audit}, "\n";
    $this->{logger} = new CDSAudit($this->{audit});

    $this->SetupInstallTree();
}




sub Log
{
    my $this = shift;
    my $msg = shift;

    $this->{logger}->Log($msg);
}

sub SetupInstallTree
{
    my $this = shift;
    my $msg = join "", "Setting up installation tree ", $this->{installRoot};

    $this->Log($msg);

    if (! -d $this->{installRoot})
    {
	mkpath ($this->{installRoot});
    }
}


1;
