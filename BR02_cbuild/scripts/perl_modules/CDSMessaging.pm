#----------------------------------------------------------------
#!! Class CDSMessaging.
#!! Original Author: Dave Allaby. dallaby@carbondesignsystems.com
#
#!! Class contains generic messaging, and user interaction methods,
#!! queries and such, which can be used by multiple scripts.
#!!
#!! This class should be used in a static way, and not new'd()
#-----------------------------------------------------------------

package CDSMessaging;

use lib "$ENV{CARBON_HOME}/scripts";
use CdsUtil;

#---------------------------------------------
#!! Clear screen.
#---------------------------------------------
sub ClearScreen
{
    local $SIG{CHLD} = 'IGNORE';

    system("clear");
}

#---------------------------------------------
#!! A generic query and prompt routine.
#!! 
#!! Method requires:
#!! 1. A string which is the question you want to
#!!    ask the user.
#!! 2. A reference to a hash, that was set up
#!!    by the caller to contain the char code that
#!!    the user will enter as the key, and the
#!!    explanation of what the code represents as a
#!!    a string assigned to the hash{key}.
#!! 3. A default value, referenced by the code, NOT
#!!    the string assigned as the explanation.  In
#!!    the event the user does not select anything,
#!!    this what will be set in the returning "answer"
#!!    hash reference.
#!! 4. Pick limit.  A numerical limit on how many can
#!!    be selected.  Choices are gathered in reverse
#!!    order than they were entered.  This means only the
#!!    last things entered up until the pick limit will
#!!    be returned in the "answer" hash.  Pick limit is
#!!    2, user enters: A, B, C.  B and C are returned.
#!! 5. Selection details. A string.  Optional. This
#!!    may be required to explain why there is a pick 
#!!    limit.
#---------------------------------------------
sub Ask
{
    my $question     = shift;
    my $possibleHash = shift;
    my $defaultValue = shift;
    my $pickLimit    = shift;
    my $selectInfo   = shift;
    my %picked       = ();
    my $possible     = "";
    my $goodParse    = 0;

    if (! $selectInfo)
    {
        $selectInfo = $pickLimit;
    }

    # Display the possible choices.  Show the word "default" next to
    # the default value.
    foreach (sort keys %{$possibleHash})
    {
        $possible .= "  $_)  => ".$possibleHash->{$_};

        if ($_ eq $defaultValue)
        {
            $possible .= " [default]\n";
        }
        else
        {
            $possible .= "\n";
        }
    }


    # Loop which protect against garbage entries.
    #
    # Print the query, get the response, stay here while the 
    # data entered is something not possible.
    while (! $goodParse)
    {
        %picked = ();
        my $badOpts = "";
        my $readCharCnt = 0;

        print CDSMessaging::PrintLine("="),"\n",
        "$question\n",
        CDSMessaging::PrintLine("="),"\n",
        "\nPossible responses:\n",
        "-------------------\n\n",
        "$possible\n";
        
        print "Select $selectInfo: [default $defaultValue]: ";
        
        chomp (my $ans = <STDIN>);
        
        if ($ans)
        {
            $ans =~ s/,/ /g;
            $ans =~ s/\s+/ /g;
            
            my @entered = split " ", $ans;
            
            # read data from the end up until pick limit.
            foreach (reverse @entered)
            {
                $readCharCnt++;

                if ($readCharCnt <= $pickLimit)
                {
                    if ($possibleHash->{$_})
                    {
                        $picked{$_} = $possibleHash->{$_};
                    }
                    else
                    {
                        # Character is not key in the %possible hash.
                        $badOpts .= " $_";
                    }
                }   
            }
            if (! $badOpts)
            {
                $goodParse = 1;
            }
            else
            {
                print "\n",
                CDSMessaging::PrintLine(),"\n",
                "ERROR: '$badOpts' is not a valid selection.\n",
                CDSMessaging::PrintLine(),"\n";
            }
        }
        else # hit enter without selection, default
        {
            $picked{default} = $defaultValue;
            print "\nUser selected default option: $picked{default}\n";
            $goodParse = 1;
        }
    }

    # Return a reference to similar hash contruct as the %possible hash,
    # but this on contains the answer(s) to the question.
    return \%picked;
}

#--------------------------------------------------------------
#!! Formatted, repeating char display. Defaults to 80 "-" chars
#!! No automatic carraige return.  Caller has to do it.
#--------------------------------------------------------------
sub PrintLine
{
    my ($charType, $myLen) = (@_);
    my $line = "";

    if (! $charType)
    {
        $charType = "-";
    }

    if (! $myLen)
    {
        $myLen = 80;
    }

    while (length ($line) < $myLen)
    {
        $line .= $charType;
    }

    return $line;
}

#--------------------------------------------------------------
#!! Interface to CdsUtil method.
#--------------------------------------------------------------
sub PaddedTestName
{
    my $test = shift;
    my $maxLen = shift;

    if (! $maxLen)
    {
        $maxLen = 48;
    }

    return CdsUtil::GetPaddedTestName($test,$maxLen);
}


1;
