#==============================================================
# Class CDSFileNames
# Author Dave Allaby.  dallaby@carbondesignsystems.com
#==============================================================
# Description:  A class to maintain all filenames and rootpaths
# that are relevant to scripting so that paths can be shared
# between scripts allowing for easier maintenance of the QA
# scripting world.
#==============================================================

package CDSFileNames;

use strict;
use File::Basename;
use Cwd;
use lib "$ENV{CARBON_HOME}/scripts";
use Versions;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSHosts;
use CDSSystem;
use CmdPaths;
use Algorithm;

sub new
{
    my $this = {};
    bless $this, @_[0];
    my $hInfo = @_[1];   # CDSHosts class
    $this->{vInfo} = @_[2];    # Versions class


# If not passed a CDSHosts object, make one.
# Often hostname or uname becomes part of a 
# path.  We also need to know isolated hosts
# and transfer hosts, which this class knows.
#--------------------------------------------
    if (! $hInfo)
    {
	$this->{hostInfo} = new CDSHosts();
    }
    else
    {
	$this->{hostInfo} = $hInfo;
    }

    $this->{cmdpaths}  = new CmdPaths($this->{hostos});

    if (! $this->{vInfo})
    {
        $this->{vInfo} = new Versions($this);
    }

# Init paths
#-------------------------------------------
    $this->Initialize();

    return $this;
}

#--------------------------------------------------
# Initialize the paths in this class to something 
# functional, although paths may  not be complete
# until CARBON_HOME is set.  Most of the time this
# will be set, but methods that return CARBON_HOME
# paths should be constructed to actually use
# $CARBON_HOME env as part of the response.
#
# Reason: If the class is created without CARBON_HOME
# being set, and paths are initialized, CARBON_HOME
# paths will be wrong and there is no way to recover.
# In short, the class is dysfunctional if called before
# CARBON_HOME is set.
#
# But, if we initialize only the suffixes, and return 
# $CARBON_HOME/$suffix, we can create this before CARBON_HOME
# is set and be mostly functional, and when CARBON_HOME
# gets set, we're 100% functional without making a new 
# class.
#-------------------------------------------------------

sub Initialize
{
    my $this = shift;

    $this->{hostos}    = $this->{hostInfo}->HostUname();


# These 4 set up, and then sets in the environment the 
# corrolating env vars.  These are also often used in 
# path names and needed environmentally by scripts.  Calling
# this class automatically sets the testing environment.
#------------------------------------------------------------
    $this->CarbonTargetArch();
    $this->CarbonHostArch();
    $this->TargetBin();
    $this->CarbonTarget();
    
# Init everything else
#------------------------------------------------

    $this->{carbonToolsRoot}         = "/tools";
    $this->{releaseRootDir}          = "/o/release";
    $this->{releaseTestRootDir}      = join "/", $this->{releaseRootDir}, "test";
    $this->{carbonReleaseBomDir}     = join "/", $this->{releaseRootDir}, 
                                       "ReleaseBom";
    $this->{lastCarbonBomLink}       = join "/", $this->{carbonReleaseBomDir},
                                       "carbon-last.bom";
    $this->{runlistResultsFileName}  = "runlist.results";
    $this->{runlistFailedFileName}   = "runlist.failed";
    $this->{writeRunFile}            = "write-test.run";
    $this->{testRunFile}             = "test.run";
    $this->{testResultsFile}         = "test.results";
    $this->{singleFailedFile}        = "FAIL.run";
    $this->{cmProcDbFile}            = "QaProc.dat";
    $this->{utVersionFile}           = "src/util/UtVersion.cxx";
    $this->{inVersionFile}           = "scripts/InfraVersion.pm";
    $this->{installTree}             = "install-tree";
    $this->{isolPkgRoot}             = $this->SetIsoPkgRoot();
    $this->{installBusyFile}         = ".install-test-busy";
    $this->{cvsaudit}                = "cvstool.err.";
    $this->{carbon_config}           = "carbon_config";
    $this->{sgeInfraDir}             = "cds_infra";
    $this->{sgeNoUseFileName}        = "dont_use_hosts";
    $this->{qaDataDir}               = "/home/cds/qa/data";
    $this->{isoFlexRoot}             = "/tools/flexlm/v10.1";
    $this->{licFileRoot}             = "/tools/license/licenses";
    $this->{alltestsXml}             = "test/alltests.xml";
    $this->{perfTestXml}             = "perfconfig.xml";
    $this->{mvvReleaseEnv}           = "mvv_release_env";
    $this->{flexlmReleaseEnv}        = "flexlm_release_env";
    $this->{pythonVersion}           = "python-". 
                                       $this->{vInfo}->Version("python");
    $this->{wineVersion}             = "wine-". 
                                       $this->{vInfo}->Version("wine");
    $this->{installJamVersion}       = $this->{vInfo}->Version("installjammer");
    $this->InitBomFilePaths();
}

sub RegistryFile
{
    my $this    = shift;
    my $regFile = join "/", $this->FindSandboxPath(), 
    "src/wine/carbon.regedit";
    
    return $regFile;
}

sub WineHome
{
    my $this = shift;
    my $wine = join "/", $this->{carbonToolsRoot},"linux",
    $this->{wineVersion};
    
    return $wine;
}

sub WineLibPath
{
    my $this    = shift;
    my $libpath = $this->WineHome()."/lib";

    return $libpath;
}

sub QTHome
{
    my $this = shift;
    my $arch = shift;
    my $target = shift;

    my $qt;
    # For Windows cross-compile, there's no OS subdirectory or target suffix
    if ($target eq "winx") {
      $qt = $this->CarbonToolsDir($arch)."/qt-".
          $this->{vInfo}->QtVersion();
    } else {
      $qt = $this->CarbonToolsDir($arch)."/".$this->OsVersion()."/qt-".
          $this->{vInfo}->QtVersion()."-".$target;
    }

    return $qt;
}

#------------------------------------------------------
#!! Returns the name of the script a developer must use
#!! to qualify a commit. Currently checkin script.
#------------------------------------------------------
sub CommitQualifyScript
{
    my $this = shift;
    my $qualScript = join "/", $this->ScriptsDir, "checkin";

    return $qualScript;
}

#------------------------------------------------------
#!! Location where commit zips and message.index files
#!! are stored.
#------------------------------------------------------
sub CommitMessageArchive
{
    my $this = shift;
    my $user = shift;

    if (! $user)
    {
        $user = $ENV{USER};
    }

    my $dir = join "/", $this->{carbonToolsRoot}, 
    "QA/diffs",$user;

    return $dir;
}

#-------------------------------------------------------
#!! Returns the string where message.index can be found.
#-------------------------------------------------------
sub CommitMessageIndexFile
{
    my $this = shift;
    my $user = shift;
    my $file = join "/", $this->CommitMessageArchive($user),
    "message.index";

    return $file;
}

#-------------------------------------------------------
#!! Returns a path to the scratch area the commit_zilla.pl
#!! script uses by default. Can be overridden by passing
#!! a directory, or by using TMP_COM_MSG_DIR env.
#-------------------------------------------------------
sub TmpCommitMessageDir
{
    my $this = shift;
    my $dir  = shift;

    if ($ENV{TMP_COM_MSG_DIR})
    {
        $dir = $ENV{TMP_COM_MSG_DIR};
    }
    else
    {
        $dir = join "/", $this->CarbonHome(), ".commit_tmp";
    }

    return $dir;
}

#-------------------------------------------------------
#!! Returns a scratch directory for CVSCmdFork class.
#-------------------------------------------------------
sub TmpCommandForkDir
{
    my $this = shift;
    my $dir = join "/", $this->TmpCommitMessageDir(), "scratch";

    return $dir;
}

#----------------------------------------------------------
#!! Returns the temporary diff file created just before commit
#!! attempt on the files list is made in commit_zilla.pl 
#!! script.
#----------------------------------------------------------
sub TmpCommitDiffFile
{
    my $this = shift;
    my $file = $this->TmpCommitMessageDir(). "/comdiff";

    return $file;
}

#----------------------------------------------------------
#!! Returns the initial location of the file that is created
#!! by the commit_zilla.pl script.
#----------------------------------------------------------
sub TmpCommitMessageFile
{
    my $this = shift;
    my $file = $this->TmpCommitMessageDir(). "/com_message.txt";

    return $file;
}

#-------------------------------------------------
#!! Location where the commit script looks for the
#!! lock file to understand if commits are locked.
#-------------------------------------------------
sub CommitLockDir
{
    my $this = shift;
    my $tag  = shift;

    if (! $tag)
    {
        $tag = "mainline";
    }

    return "/home/cds/qa/commit_locking/$tag";
}

#-----------------------------------------------------
#!! Returns the http address of bugzilla, with the show
#!! bugs page prefix. Caller has to attach the bug number
#!! for useful display.
#-----------------------------------------------------
sub BugzillaShowBugCgi
{
    my $this = shift;
    my $http = join "", "http://", $this->{hostInfo}->BugzillaHost(),
    $this->{hostInfo}->LocalDomainName(),
    "/show_bug.cgi?id=";

    return $http; 
}

#-----------------------------------------------------
#!! Returns where qa puts it's releases.
#-----------------------------------------------------
sub DefaultReleaseTreeRoot
{
    my $this = shift;
    return $this->{releaseTestRootDir};
}

#-----------------------------------------------------
#!! Returns where performance testing writes its lockfile
#-----------------------------------------------------
sub PerfCronLockFile
{
    my $this = shift;

    $this->{perfCronLock} = "/work/qa/PerfTimestamps/.PerfCron.busy";

    return $this->{perfCronLock};
}

#--------------------------------------------------------
#!! Returns customer version string from cbuild.
#--------------------------------------------------------
sub CBuildVersion
{
    my $this = shift;
    return ($this->GetCBuildOutput("-version"));
}

#--------------------------------------------------------
#!! Executes cbuild and gets version output. Of course
#!! cbuild has to exist.
#--------------------------------------------------------
sub GetCBuildOutput
{
    my $this      = shift;
    my $cBuildOpt = shift;
    my $cbuildCmd = $this->CarbonHome();
    my $cbuild = join "/",$this->CarbonHome(),"bin/cbuild";

    my $opt = "Cbuild version output not available. Cbuild not built.";

    if (-x $cbuild)
    {
        $opt = CDSSystem::BackTick("$cbuild $cBuildOpt");
        chomp ($opt);
    }

    return $opt;
}

#--------------------------------------------------------
#!! Returns the verbose output from cbuild
#--------------------------------------------------------
sub CBuildVerboseVersion
{
    my $this = shift;
    return ($this->GetCBuildOutput("-verboseVersion"));
}

#--------------------------------------------------------
#!! Location of the release's installjammer configuration
#!! file.
#--------------------------------------------------------
sub JammerMPIFile
{
    my $this   = shift;
    my $objDir = shift;

    if (! $objDir)
    {
        $objDir = $this->ObjectDir();
    }

    my $mpi = "$objDir/unixInstall/InstallProject/InstallProject.mpi";

    return $mpi;
}

#-----------------------------------------------------------
#!! Returns the path to the install jammer software used
#!! to create our release packages.
#-----------------------------------------------------------
sub InstallJammerHome
{
    my $this   = shift;
    my $ijhome = join "/",$this->{carbonToolsRoot}, "installjammer",
    $this->{installJamVersion};

    return $ijhome;
}

#-----------------------------------------------------------
#!! Returns the path to $CARBON_HOME/lib areas.
#-----------------------------------------------------------
sub CarbonHomeLibDir
{
    my $this = shift;
    my $dir = join "/", $this->CarbonHome(),"lib";
    return $dir;
}

#-----------------------------------------------------------
#!! Returns the path to $CARBON_HOME/Linux/lib areas.
#-----------------------------------------------------------
sub CarbonHomeCarbonArchLibDir
{
    my $this = shift;
    my $dir = join "/", $this->CarbonHome(),
    $this->CarbonArch(), "lib";
    return $dir;
}

#---------------------------------------------------------
#!! Returns the path to the object dir.
#---------------------------------------------------------
sub ObjectDir
{
    my $this = shift;

    $this->{objDir}  = join "/", $this->CarbonHome(),"obj",
    $this->CarbonTarget();

    return $this->{objDir};
}

#---------------------------------------------------------
#!! Returns the flexlm area as pointed to by the 
#!! flexlm_release script.
#---------------------------------------------------------
sub FlexLmArea
{
    my $this    = shift;
    my $version = shift;
    my $cmd     = join "", $this->ScriptsDir, "/",
    $this->{flexlmReleaseEnv}, " $version";
    
    my $out = CDSSystem::BackTick($cmd);
    chomp ($out);
    return $out;
}

#---------------------------------------------------------
#!! Path to wine used by the build
#---------------------------------------------------------
sub WinePath
{
    my $this = shift;
    my $path = join "/", $this->CarbonToolsDir(), $this->{wineVersion};

    return $path;
}

#---------------------------------------------------------
#!! Python directory, used by build and shipped.
#---------------------------------------------------------
sub ToolsPythonDir
{
    my $this = shift;
    my $pythonDir = $this->CarbonToolsDir($this->CarbonTargetArch());
    $pythonDir .= "/".$this->{pythonVersion};

    return $pythonDir;
}

sub CarbonHomePythonDir
{
    my $this = shift;
    my $pythonLinkTree = join "/", $this->CarbonHome(),
    $this->CarbonArch(), "python";
    return $pythonLinkTree;
}

#---------------------------------------------------------
#!! Returns the binutils path.
#---------------------------------------------------------
sub BinUtilsPath
{
    my $this = shift;
    my $version = "2.15";
    my $utilsPath = join "/", $this->CarbonToolsDir($this->CarbonTargetArch()), 
    "binutils-$version";

    return $utilsPath;
}

#---------------------------------------------------------
#!! Returns the debussy area from the debussy_env script.
#---------------------------------------------------------
sub DebussyEnvScript
{
    my $this = shift;
    my $dbsy = join "/", $this->ScriptsDir(), "debussy_env";

    return $dbsy;
}

#---------------------------------------------------------
#!! Returns INTERRA_BASE as called in the build.
#---------------------------------------------------------
sub JaguarRoot
{
    my $this = shift;

    if (! $this->{jaguarRoot})
    {
	my $cmd = join "", $this->ScriptsDir(), "/", 
	$this->{mvvReleaseEnv}, " release";

	chomp ($this->{jaguarRoot} = CDSSystem::BackTick($cmd));
    }

    return $this->{jaguarRoot};
}

#-----------------------------------------------------------
#!! Returns the carbon tools root based on arch.
#-----------------------------------------------------------
sub CarbonToolsDir
{
    my $this = shift;
    my $arch = shift;

    if (! $arch)
    {
	$arch = $this->{hostInfo}->HostUname();
    }

    my $tDir = $this->{carbonToolsRoot};

    if ($arch eq "Linux")
    {
	$tDir .= "/linux";
    }
    elsif ($arch eq "SunOS")
    {
	$tDir .= "/solaris";
    }
    elsif ($arch eq "Linux64")
    {
	$tDir .= "/linux64";
    }
    else
    {
	$tDir .= "/Windows";
    }

    return $tDir;
}

#-----------------------------------------------------------
#!! Returns the name of the makefile snippet that we look
#!! at for compiler version when making Makefile.compiler.
#-----------------------------------------------------------
sub CompilerVersionFile
{
    my $this = shift;
    my $file = join "/", $this->ScriptsDir(), "compiler_version";
    return $file;
}

sub InitBomFilePaths
{
    my $this = shift;
    my %bom = ();
    $this->{bom} =\%bom;
}

#---------------------------------------------------------
#!! Test that returns true of CARBON_HOME and what we
#!! think is the sandbox are different places.
#---------------------------------------------------------
sub CarbonHomeNotSandbox
{
    my $this = shift;
    my $ok = 1;

    if ($this->CarbonHome() eq $this->FindSandboxPath())
    {
	$ok = 0;
    }

    return $ok;
}

#---------------------------------------------------------
#!! Returns CARBON_HOME, possibly a local disk path /work.
#!! If CARBON_HOME is not set, it will try to set up a
#!! CARBON_HOME if we happen to be in it.
#---------------------------------------------------------
sub CarbonHome
{
    my $this = shift;

    if ($ENV{CARBON_HOME})
    {
	$ENV{CARBON_HOME} =~ s/\/$//;
	$ENV{CARBON_HOME} =~ s/\/\//\//g;
        my $domain = $this->{hostInfo}->LocalDomainName();
        $ENV{CARBON_HOME} =~ s/\.$domain//;
	$this->{carbonHome} = $this->ShortCircuit($ENV{CARBON_HOME});
    }
    else
    {
	if (! $this->{carbonHome})
	{
	    if (-d "src" && -l "src" &&
		 -d "scripts" && -l "scripts")
	    {
		$this->{carbonHome} = cwd();
		$this->{carbonHome} = $this->ShortCircuit($this->{carbonHome});
		$ENV{CARBON_HOME} = $this->{carbonHome};
	    }
	}
    }

    return $this->{carbonHome};
}

#---------------------------------------------------------
#!! Takes CARBON_HOME and tries to travse back through the
#!! the links to find where the associated sandbox is located. 
#---------------------------------------------------------
sub FindSandboxPath
{
    my $this   = shift;
    my $srcDir = join "/",$this->CarbonHome(),"src";
    my $here   = cwd();

    if ($ENV{CARBON_SANDBOX})
    {
        $this->{sandbox} = $ENV{CARBON_SANDBOX};
    }
    elsif (-l $srcDir)
    {
        CDSSystem::Cd("$srcDir/..");
          $this->{sandbox} = cwd();
          CDSSystem::Cd ($here);
    }
    elsif (-d "src" && ! -l "src" && -d "scripts" && ! -l "scripts")
    {
        $this->{sandbox} = $here;
    }

    if ($this->{sandbox} =~ /^\/work\/$ENV{USER}/)
    {

        my $netPath = "/w/".$this->{hostInfo}->LocalHostName();
        print "netpath is $netPath\n";
        $this->{sandbox} =~ s/^\/work\//$netPath\//;
    }

    $ENV{CARBON_SANDBOX} = $this->{sandbox};

    return $this->{sandbox};
}	

#----------------------------------------------------
#!! Returns the full pathname to a bomfile, given the
#!! Stub part of the filename.
#----------------------------------------------------
sub BomFile
{
    my $this = shift;
    my $file = shift;
    $file =~ s/\.bom//;

# Assume everything is in \$CARBON_HOME/src
    my $filePath = join "/", $this->CarbonHome(), "src",
    "$file.bom";

# If we've defined it, it is special location
    if ($this->{bom}{$file})
    {
	$filePath = join "/", $this->CarbonHome(), $this->{bom}{$file},
	"$file.bom";
    }
    
    return $filePath;
}

#----------------------------------------------------
#!! Location of performance test's xml
#----------------------------------------------------

sub PerfTestXmlFile
{
    my $this = shift;
    my $file = join "/", $this->ScriptsDir, $this->{perfTestXml};
    return $file;
}

#--------------------------------------------------------
#!!
sub InstallTreePath
{
    my $this = shift;
    my $dir = join "/", $this->CarbonHome(), $this->{installTree};

    return $dir;
}

#----------------------------------------------------
# Location of alltests.xml
#----------------------------------------------------

sub AllTestsXMLFile
{
    my $this = shift;
    my $xml = join "/", $this->CarbonHome(), $this->{alltestsXml};

    return $xml;
}

#----------------------------------------------------
# Maxsim license path on isolated hosts
#----------------------------------------------------
sub IsoMaxsimLicPath
{
    my $this = shift;
    my $file = join "/", $this->{licFileRoot}, "maxsim.lic";
    return $file;
}

#----------------------------------------------------------
# carbond license path on isolated hosts
#
# This path depends on CARBON_HOME, it's inside
# the package.  So that another script with a different
# CARBON_HOME can get the path, allow passing a different
# CARBON_HOME path so it resolves.  Of course, the calling
# script must know or calc the correct path.
#-----------------------------------------------------------
sub IsoCarbondLicPath
{
    my $this = @_[0];
    my $carbonHome = @_[1];
    my $file = "";

    if ($carbonHome)
    {
	$file = join "/", $carbonHome,"lib","customer.lic";
    }
    else
    {
	$file = join "/", $this->CarbonHome(),"lib","customer.lic";
    }

    return $file;
}

#----------------------------------------------------------
# Resolves the flexlm licensing area. Dependent on uname.
#----------------------------------------------------------
sub IsolatedFlexDir
{
    my $this = shift;
    my $dir = "";

    if ($this->{hostInfo}->HostUname() eq "Linux")
    {
	$dir = join "/", $this->{isoFlexRoot}, "i86_r6";
    }
    elsif ($this->{hostInfo}->HostUname() eq "SunOS")
    {
	$dir = join "/", $this->{isoFlexRoot}, "sun4_u5";
    }
    elsif ($this->{hostInfo}->HostUname() eq "Win")
    {
	$dir = join "/", $this->{isoFlexRoot}, "i86_r3";
    }
    else
    {
	# Nothing for you.
    }

    return $dir;
}

#----------------------------------------------------------
# Access to class type. Mostly used to show from where an
# error is coming from.
#---------------------------------------------------------
sub ClassType()
{
    my $this = shift;
    return ref($this);
}

#----------------------------------------------------------
# Returns an autogenerated, distinctly named audit log
# file name, but could be used for anything.
# Embed the script name into it, so if we trip across
# it later in an unexpected place, we know what script
# misfired.
#-----------------------------------------------------
sub AuditFileName
{
    my $this = shift;

    if (! $this->{genAuditName})
    {
	$this->{genAuditName}  = join ".", basename($0), "audit",$$, "log";
    }

    return $this->{genAuditName};
}

#-----------------------------------------------------------------
# Allow someone to temporarily repoint the location of 
# the isolation root. Done by a unit test mostly so we can test 
# the InstallTesting.pm module.
#----------------------------------------------------------------
sub SetIsoPkgRoot
{
    my $this = shift;
    my $path = $ENV{ISO_PKG_ROOT};

    if (! $path)
    {
	$path = "/home/qa";
    }

    return $path;
}

#-----------------------------------------------------------------
# This file which is used by scripts that need a list of grid
# machines.  It holds machines that we will NOT use, although
# they are part of the grid.  It's not checked in, it is considered
# part of the SGE infrastructure, and so is not tagged, and applies
# everywhere to all things at any one time.
#----------------------------------------------------------------
sub SgeNoUseFile
{
    my $this = shift;
    my $file = join "/", $ENV{SGE_ROOT}, 
    $this->{sgeInfraDir},
    $this->{sgeNoUseFileName};

    return $file;
}

#-----------------------------------------------------------------
# This returns our CdsHosts object so that others can have it
# to get information from.  Making a new one involves shell calls
# and slows us down a bit.  The information in it would be identical
# if we did make a new one, so we'll make sharing available.
#-----------------------------------------------------------------
sub CdsHosts
{
    my $this = shift;
    return $this->{hostInfo};
}

#-----------------------------------------------------------------
# Where we archive test results
#----------------------------------------------------------------
sub QADataDir
{
    my $this = shift;
    return $this->{qaDataDir};
}

sub BranchCommitSnippetDir
{
    my $this = shift;
    my $dir = join "/", $this->{qaDataDir}, "branchcommits";

    return $dir;
}

sub BranchCommitSnippetFileName
{
    my $this = shift;
    my $now = shift;
    my $user = shift;

    if (! $user)
    {
        print STDERR ref($this), "::BranchCommitSnippetFileName() - No user argument.\n";
    }

    if (! $now)
    {
        print STDERR ref($this), "::BranchCommitSnippetFileName() - No time string argument.\n";
    }

    my $name = join ".","commit",$user,$now,"xml";
    return $name;
}

#--------------------------------------------------
# Returns the path to our scripts dir
#----------------------------------------------
sub ScriptsDir
{
    my $this = shift;
    my $sandbox = $this->FindSandboxPath();
    my $dir = "";

    if (! $sandbox)
    { 
        $dir = join "/", $this->CarbonHome(), "scripts";
    }
    else
    {
        $dir = join "/", $sandbox,"scripts";
    }
    return $dir;
}

#--------------------------------------------------
# A get OR a set and get function for TARGET_BIN which is
# the terminology for scripts, also referred to at 
# times as CARBON_BIN in the user's world.
#--------------------------------------------------
sub TargetBin
{
    my $this = shift;

    if ($this->CarbonRelease())
    {    
	if ($ENV{CARBON_BIN})
	{
	    delete $ENV{CARBON_BIN};
	}
	$this->{targetBin} = "";
    }
    else
    {
	if ($ENV{CARBON_BIN})
	{
	    $this->{targetBin} = $ENV{CARBON_BIN};
	}
    }

    return $ENV{CARBON_BIN};
}
#--------------------------------------------------------
# A get OR a set and get function for CARBON_TARGET.
#--------------------------------------------------------
sub CarbonTarget
{
    my $this = shift;
    my $bin = "";

    if ($this->CarbonRelease())
    {
        $bin = "product";
    }
    else
    {
	$bin = $this->TargetBin();
	
	if (! $bin)
	{
	    $bin = "product";
	}
    }	

    $this->{carbonTarget} = join ".",
    $this->CarbonTargetArch(), $bin;

    $ENV{CARBON_TARGET} = $this->{carbonTarget};

    return $ENV{CARBON_TARGET};
}

#--------------------------------------------------------
# Returns Carbon Release value
#--------------------------------------------------------

sub CarbonRelease
{
    my $this = shift;
    $this->{carbonRelease} = $ENV{CARBON_RELEASE};
    return $ENV{CARBON_RELEASE};
}

#--------------------------------------------------------
# Sets and gets CARBON_ARCH_TARGET
#--------------------------------------------------------
sub CarbonTargetArch
{
    my $this = shift;

    # this is a get it, set it and return it.

    if ($ENV{CARBON_TARGET_ARCH})
    {
	$this->{carbonTargetArch} = $ENV{CARBON_TARGET_ARCH};
    }
    else
    {
	if ($ENV{CARBON_TARGET})
	{
            my $hostArch = "";
            my $bin = "";
            my $osver = "";

            if ($ENV{CARBON_TARGET} =~ /\S+\.\S+\.\S+/)
            {
                ($hostArch, $osver, $bin)= (split /\./,$ENV{CARBON_TARGET});
                
                $this->{carbonTargetArch} = join ".", $hostArch;
            }
            else
            {
                ($hostArch, $bin)= (split /\./,$ENV{CARBON_TARGET});
                
                $this->{carbonTargetArch} = $hostArch;
            }

            $this->{targetBin} = $bin;
	}
	else 
	{
	    $this->{carbonTargetArch} = $this->GetFromCarbonArchScript();
	}
	
	$ENV{CARBON_TARGET_ARCH} = $this->{carbonTargetArch};
    }

    return $ENV{CARBON_TARGET_ARCH};
}

#----------------------------------------------------------
# Sets and gets CARBON_HOST_ARCH
#----------------------------------------------------------

sub CarbonArch
{
    my $this = shift;
    return $this->CarbonHostArch();
}

sub CarbonHostArch
{
    my $this = shift;

    if (! $ENV{CARBON_HOST_ARCH})
    {
	$this->{carbonHostArch} = $this->GetFromCarbonArchScript();
	$ENV{CARBON_HOST_ARCH} = $this->{carbonHostArch};
    }
    else
    {
	$this->{carbonHostArch} = $ENV{CARBON_HOST_ARCH};
    }

    return $ENV{CARBON_HOST_ARCH};
}


#--------------------------------------------------------
# Default method to set:
# CARBON_TARGET_ARCH and CARBON_HOST_ARCH if not in the 
# environment.
#
# This script might live in at least 2 places, and I threw
# in the possibility that it might be located from `pwd`
# at ./scripts. or .bin/. or .
#---------------------------------------------------------
sub GetFromCarbonArchScript
{
    my $this = shift;

    if (! $this->{carbonArchOutput})
    {
	$this->{carbonArchOutput} = 
            $this->FindAndExecuteScript("carbon_arch");
    }

    return $this->{carbonArchOutput};
}

#---------------------------------------------------------
#!! Returns the os version for the machine as carbon
#!! refers to it. ES4/ES5/ES6 for linux.  We have to search
#!! for the script to execute unfortunately.
#!! If not linux, we'll get it from uname -a output.
#---------------------------------------------------------
sub OsVersion
{
    my $this = shift;

    if (! $this->{osVersion})
    {
        if ($this->{hostInfo}->HostUname() eq "Linux")
        {
            $this->{osVersion} = 
                $this->FindAndExecuteScript("carbon_rh_release");
        }
        else
        {
            my $cmd = join " ", $this->Command("uname"), "-a";
            my $sunVer = CDSSystem::BackTick($cmd);
            my ($osVer, $arch) = (split " ", $sunVer)[2,4];
            $osVer =~ s/\..+//;
            $this->{osVersion} = $arch . $osVer;
        }
    }

    return $this->{osVersion};
}

#--------------------------------------------------------
#!! Returns where the real build-systemc script is currently
#!! living.
#--------------------------------------------------------
sub SystemCBuildScriptPath
{
    my $this = shift;
    my $path = $this->FindScript("build-systemc");

    return $path;
}

#------------------------------------------------------------
#!! We have an unfortunate situation where a script might live in
#!! 2 different places. depending on whether or not we are
#!! in a sandbox or a release package.  This method tries
#!! to find a real script, and return the path.
#-------------------------------------------------------------
sub FindScript
{
    my $this   = shift;
    my $script = shift;
    my $path   = "";
    my @places = ();
    $places[0] = join "/", $this->CarbonHome(), "bin/$script";
    $places[1] = join "/", $this->ScriptsDir(), $script;
    $places[2] = join "/", $this->CarbonHome(), "scripts/$script";
    $places[3] = "./scripts/$script";
    $places[4] = "./bin/$script";
    $places[5] = "./$script";

    foreach my $try (@places)
    {
	if (-f $try && ! -l $try)
	{
            $path = $try;
            last;
        }
    }

    if (! $path)
    {
        die "Could not get $script value\n",
        "No $script script found at:\n",
        "@places\n";
    }

    return $path;
}

#------------------------------------------------------------
#!! This is a compound method, we find the real script and 
#!! execute it, returning the output of the script
#------------------------------------------------------------
sub FindAndExecuteScript
{
    my $this = shift;
    my $script = shift;
    my $s = "";
    my $scriptPath = $this->FindScript($script);
    chomp ($s = &CDSSystem::BackTick ($scriptPath));

    return $s;
}

#--------------------------------------------------------
#!! Returns the existing CmdPaths class so something
#!! doesn't need to make another one.
#--------------------------------------------------------
sub CmdPaths
{
    my $this = shift;
    return $this->{cmdpaths};
}

#------------------------------------------------------------
# This provided a method interface to the CmdPaths class.
# See that class for what it is doing.
#------------------------------------------------------------
sub Command
{
    my $this = @_[0];
    my $cmd  = @_[1];

    return $this->{cmdpaths}->Command($cmd);
}

#------------------------------------------------------------
# Returns carbon_config's location
#------------------------------------------------------------
sub CarbonConfigPath
{
    my $this = shift;
    my $script = join "/", $this->ScriptsDir(), 
                 $this->CarbonConfigScriptName();

    return $script;
}

#------------------------------------------------------------
# Generates a file name for a temporary output file for
# checkouts done by the CVSTool class.
#------------------------------------------------------------
sub CvsAuditFile
{
    my $this = shift;
    my $audit = $this->{cvsaudit} . Algorithm::RandomAlphaNumeric(8);

    return $audit;
}

#----------------------------------------------------------
# Returns test.run
#----------------------------------------------------------

sub TestRunFileName
{
    my $this = shift;

    return $this->{testRunFile};
}

#----------------------------------------------------------
# Returns write-test.run
#----------------------------------------------------------
sub WriteTestRunFileName
{
    my $this = shift;

    return  $this->{writeRunFile};
}

#----------------------------------------------------------
# Returns the lock file path for the isolated testing so that
# 2 testing sessions do not occur on the same box.
#----------------------------------------------------------
sub InstallBusyFile
{
    my $this = shift;
    my $file = join "/", $this->{isolPkgRoot}, $this->{installBusyFile};

    return $file;
}

#-----------------------------------------------------------
# Returns the path where release package information is
# dropped for isolation testing.
#----------------------------------------------------------
sub IsolationPackageRoot
{
    my $this = shift;
    
    return $this->{isolPkgRoot};
}

#-----------------------------------------------------
# Returns our InfraVersion.pm file
#-----------------------------------------------------
sub InVersionFile
{
    my $this = shift;
    my $file = join "/", $this->FindSandboxPath(), $this->{inVersionFile};

    return $file;
}

sub VersionsObject
{
    my $this = shift;
    return $this->{vInfo};
}

#-----------------------------------------------------
# Returns our utils/UtVersion.pm file
#-----------------------------------------------------
sub UtVersionFile
{
    my $this = shift;
    my $file = join "/", $this->FindSandboxPath(), 
    $this->UtVersionSandboxPath();

    return $file;
}

#-----------------------------------------------------
# Returns our utils/UtVersion.cxx file
#-----------------------------------------------------
sub UtVersionSandboxPath
{
    my $this = shift;

    return $this->{utVersionFile};
}

#-----------------------------------------------------
# Returns our scripts/InfraVersion.pm file
#-----------------------------------------------------
sub InfraVersionSandboxPath
{
    my $this = shift;

    return $this->{inVersionFile};
}


#----------------------------------------------------
# Returns runlist.results 
#----------------------------------------------------
sub RunlistResultsFileName
{
    my $this = shift;
    return $this->{runlistResultsFileName};
}

#----------------------------------------------------
# Returns runlist.failed 
#----------------------------------------------------
sub RunlistFailedFileName
{
    my $this = shift;
    return $this->{runlistFailedFileName};
}

#----------------------------------------------------
# Returns $(TESTDIR)/runlist.results 
#----------------------------------------------------
sub RunlistResultsFile()
{
    my $this = shift;
    my $file = join "/", $this->TopTestRunDir(), 
    $this->RunlistResultsFileName();

    return $file;
}

#----------------------------------------------------
# Returns $(TESTDIR)/runlist.failed
#----------------------------------------------------
sub RunlistFailedFile()
{
    my $this = shift;
    my $file = join "/", $this->TopTestRunDir(), 
    $this->RunlistFailedFileName();

    return $file;
}

#----------------------------------------------------
# This is a set and get function for setting the test
# directory root.  If passed a rundir, it will process
# it into a new path.  It only does the processing once
# and the path becomes fixed and unchangeable.  So call
# it the right way the first time.
#-----------------------------------------------------
sub TopTestRunDir
{
    my $this = @_[0];
    my $dirArg = @_[1];
    my $SGE = @_[2];

    if (! $this->{topTestRunDir})
    {
        if ($this->CarbonHome())
        {
            $this->{topTestRunDir} = join "/", $this->CarbonHome(), "obj",
            $this->CarbonTarget();
            
            if ($dirArg)
            {
                if ($dirArg !~ /^\//)
                {
                    $this->{topTestRunDir} .= "/$dirArg";
                }
                
                else
                {
                    my $netmounted = $this->NetworkFriendly ($dirArg);
                    
                    if (($SGE && $netmounted) || ! $SGE)
                    {
                        $this->{topTestRunDir} = "$dirArg";
                    }
                    else
                    {
                        if ($SGE)
                        {
                            print STDOUT "Using grid, but rundir path ",
                            "not network friendly,\n",
                            "will append path to ", $this->ObjectDir(), "\n";
                        }
                        
                        $this->{topTestRunDir} .= "/$dirArg";
                    }
                }
            }
            
            $this->{topTestRunDir} =~ s/\/\//\//g;
        }

        $this->{topTestRunDir} = $this->ShortCircuit($this->{topTestRunDir});
    }

    return $this->{topTestRunDir};
}

sub  RelativePath
{
    my $this       = shift;
    my $targetFile = shift;
    my @here       = split "/", cwd();
    my @there      = split "/", $targetFile;
    my $i          = 1;

    if ($targetFile !~ /^\./ &&
        $here[$i] eq $there[$i])
    {
        $targetFile = "";
        
        while ($here[$i] eq $there[$i])
        {
            $i++;
        }
        
        my $j = $i;
        
        while ($here[$i])
        {
            $targetFile .= "../";
            $i++;
        }
        
        while ($there[$j])
        {
            $targetFile .= "$there[$j]/";
            $j++;
        }
        $targetFile =~ s/\/$//;
    }
    
    return $targetFile;
}    

#------------------------------------------------------
# Access to CdsUtil method which converts to a full
# path if it is relative.
#------------------------------------------------------
sub FullPath
{
    my $this = @_[0];
    my $file = @_[1];
    
    if ($file !~ /^\//)
    {
        my $here = cwd();

        while ($file =~ /^\.\.\//)
        {
            $here = dirname($here);
            $file =~ s/^\.\.\///o;
        }

        $file = "$here/$file";
    }

    return $file;
}

#------------------------------------------------------
# Understands what path is mounted everywhere.
# If it is, it is considered net-friendly.
#------------------------------------------------------
sub NetworkFriendly
{
    my $this     = @_[0];
    my $path     = @_[1];
    my $friendly = 0;

    if (! $path)
    {
	$path = cwd();
    }
    elsif ($path !~ /^\//)
    {
	$path = $this->FullPath($path);
    }

    if ($path =~ /^(\/net|\/home\/cds|.automount|\/(o|n)\/release\/|\/o\/work|\/w\/|\/cust\/)/)
    {
	$friendly = 1;
    }

    return $friendly;
}

#------------------------------------------------------
# Does a check to see if your /w/$host CARBON_HOME
# is actually the local /work directory and converts paths.
#------------------------------------------------------
sub ShortCircuit
{
    my $this         = @_[0];
    my ($carbonHome) = @_[1];
    my $user         = $ENV{USER};

    if ($this->{hostInfo}->YpcatInfo())
    {
	foreach ($this->{hostInfo}->HostAliases())
	{
	    my $localMount = join "/", "/w" , $_;
	    
	    if ($carbonHome =~ /^$localMount/)
	    {
		$carbonHome =~ s/$localMount/\/work/;
		last;
	    }
	}
    }
    else
    {
        if ($ENV{QA_DEBUG})
        {
            print "WARNING: Could not get needed host info to",
            " analyze /w vs. /work path shortcircuiting\n",
            "No path alterations to CARBON_HOME. Using: $carbonHome.\n";
        }
    }

    return $carbonHome;
}

#-------------------------------------------------------
#!! This method reverses what CDSFileNames::ShortCircuit() did,
#!! restoring a /work path back to a /w path.
#-------------------------------------------------------
sub UseMountedPath
{
    my $this = shift;
    my $path = shift;
    my $host = $this->{hostInfo}->LocalHostName();

    if ($path =~ /^\/work/)
    {
	$path =~ s/^\/work/\/w\/$host/;
    }
    
    return $path;
}


1;
