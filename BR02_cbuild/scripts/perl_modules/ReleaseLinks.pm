package ReleaseLinks;

use lib "$ENV{CARBON_HOME}/scripts";
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSSystem;
use CDSFileNames;
use CarbonConfigArgs;
use CarbonLinksInfo;


sub new
{
    my $this = {};
    bless ($this, @_[0]);
    $this->{osPlat} = @_[1];
    $this->{fileNames} = @_[2];
    $this->{args} = new CarbonConfigArgs($this->{fileNames});
    $this->{linkInfo} = new CarbonLinksInfo($this->{osPlat},
                                            $this->{fileNames}->OsVersion(),
                                            $this->{fileNames},
                                            $this->{args});


    return $this;
}

sub CarbonConfigReleaseList
{
    my $this = shift;
#    print "\$this->{linkInfo} is ", %this->{linkInfo}, " ref is ", ref($this->{linkInfo}), "\n";

#    print $this->{linkInfo}->AllKeys(),"\n";

    foreach my $group (sort $this->{linkInfo}->AllKeys())
    {
        print "Looking at group $group ", $group, "\n";

#        if ($this->{linkInfo}->{$group}->Released())
#        {
            $this->{linkInfo}->{$group}->DumpList();
#       }
    }

}



1;
