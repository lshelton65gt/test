

package IsolationLic;

use strict;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSSystem;
use CDSFileNames;



my $fn = new CDSFileNames();

my %licFiles = ();

sub GetFlexLmBin
{
    return $fn->IsolatedFlexDir();
}

sub GetMachHostId
{
    # set up path to flexlm stuff, and get host id for use in lic file
    my $mId = 0;
    my $hostArch = $fn->CdsHosts()->HostUname();

    if ($hostArch eq "Linux")
    {
        chomp ($mId = `/sbin/ifconfig | grep eth0 | awk '{ print \$5 }' | sed -e 's/://g'`);
    }
    elsif ($hostArch eq "SunOS")
    {
        chomp ($mId = `hostid`)
    }
    else
    {
        # I got nothing for you.
    }

    return $mId;
}

sub LicenseDaemonRunning
{
    my ($daemon, $logRef) = (@_);
    my $running = 0;

    if (! $daemon)
    {
	$daemon = GetFlexLmBin() . "/lmgrd";
    }

    my $cmd = join " ", $fn->Command("ps"),
    "auwx | ", $fn->Command("grep"),
    $daemon, " | ", $fn->Command("grep"), $ENV{USER},
    "| ", $fn->Command("grep"), " -v ", "grep";

    my $out = CDSSystem::BackTick($cmd, $logRef);
    
    if ($out)
    {
	if ($ENV{QA_DEBUG} == 1)
	{
	    print STDOUT "Found:\n|$out|\n";
	}

	$running = 1;
    }

    return $running;
}

sub CreateLicenseFile 
{
    my ($licFile, $logRef, $licFeatureCnt) = (@_);

    my $TestCompany = "TestRelease";
    my $success     = 0;
    my $rtn         = 1;
    my $mhostid     = GetMachHostId();
    my $flexBin     = GetFlexLmBin();
    my $name        = $fn->CdsHosts()->LocalHostName();
    my $wh          = &CDSSystem::OpenWrite($licFile);

    if (! $licFeatureCnt || $licFeatureCnt <= 0)
    {
        $licFeatureCnt = 1;
    }
    
    if ($wh)
    {
	print $wh "SERVER $name $mhostid\n",
	"USE_SERVER\n",
	"VENDOR carbond\n",
	"FEATURE cds_speedcompiler_vhdl_verilog carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=$TestCompany SIGN=\n",
	"FEATURE cds_speedcompiler_verilog carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=$TestCompany SIGN=\n",
	"FEATURE cds_speedcompiler_vhdl carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=$TestCompany SIGN=\n",
	"FEATURE cds_designplayer_dev carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=\"cdsdpdev_$TestCompany\" SIGN=\n",
	"FEATURE crbn_vsp carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_comp carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_comp_opt carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_comp_verilog carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_comp_vhdl carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_vhm carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"\n",
	"FEATURE crbn_vsp_exec_intf_rvsocd carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_lmtool_sp carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_replay carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_intf_platarch carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_modelvalidator carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_xtor_ahb carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_xtor_ahblitea carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_xtor_ahba carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_xtor_apba carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_xtor_axia carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_xtor_ddrsdram carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_xtor_e10g carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_xtor_e1g carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_xtor_e10_100 carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_xtor_pci carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_xtor_pcie carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_xtor_sata carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_xtor_spi4 carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_xtor_spi3 carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n",
	"FEATURE crbn_vsp_exec_xtor_utopia2 carbond 1.0 permanent $licFeatureCnt\n",
	"VENDOR_STRING=SIG=$TestCompany SIGN=\n";
	
	write "$licFile";
	
	close($wh);

	if (-f $licFile)
	{
	    $rtn = 0;
	}
    }

    if ($rtn == 0)
    {
	# register the lic file
	$rtn = AddDaemonWithFlex ("carbond", $licFile);

	if ($rtn == 0)
	{
	    $rtn = AddLmCryptWithFlex ($licFile);
	}
    }

    if ($rtn == 0)
    {
	$success = 1;
    }

    return $success;
}

sub AddLmCryptWithFlex
{
    my ($lfile) = (@_);
    my $flexBin = GetFlexLmBin();
    
    my $rtn = CDSSystem::System ("$flexBin/lmcrypt $lfile >> flexlm.out 2>&1");
    
    return $rtn;
}

sub AddDaemonWithFlex
{
    my ($ldaemon, $lfile) = (@_);
    my $flexBin = GetFlexLmBin();

    my $rtn = CDSSystem::System ("$flexBin/lmpath -add $ldaemon $lfile >> flexlm.out 2>&1");    
    
    return $rtn;
}

sub StartLicenseDaemon
{
    my ($licFile) = (@_);
    my $flexBin = GetFlexLmBin();

    my $rtn = CDSSystem::System ("$flexBin/lmgrd -c $licFile >> flexlm.out 2>&1 &");

    return $rtn;
}


sub ShutdownLicenseDaemon
{
    my ($licFile) = (@_);
    my $flexBin = GetFlexLmBin();
 
    my $rtn = CDSSystem::System ("$flexBin/lmdown -q -all -c $licFile >> flexlm.out 2>&1");
    
    return $rtn;
}


sub WipeoutAllRunningLicenseDaemons
{
    my $ps      = $fn->Command("ps");
    my $egrep    = $fn->Command("egrep");
    my $egrepCmd = join " ", $ps, "auwx |", $egrep, "'lmgrd|carbond' |",
    $egrep, "-v", $egrep;

    my (@licenseProcess) = (split /\n/, CDSSystem::BackTick($egrepCmd));

    foreach (@licenseProcess)
    {
	my ($pid, $proc) = (split) [1,10];
	print STDERR "Killing pid $pid - process->$proc\n";

	`kill -9 $pid`;
    }

    my $rtn = IsolationLic::LicenseDaemonRunning();
			    
    return $rtn;
}



1;
