#=====================================================================
# Class CVSCmdFork
# Author: Dave Allaby. dallaby@carbondesignsystems.com
#
#!! Class is an extension to run most cvs commands in parallel.  It
#!! then reconstitutes the output into a single file-like structure as
#!! if the command was not run in parallel over several directories.
#!!
#!! This class must be new'() for methods to work properly.
#=====================================================================

package CVSCmdFork;

use strict;
use Cwd;
use File::Basename;
use File::Path;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CVSTool;
use CDSFileNames;
use CDSSystem;
use PidInfo;
use PidSetMgr;

#-------------------------------------------------------------
#!! Constructor.
#-------------------------------------------------------------
sub new #(CDSFileNames, string , string, int)
{
    my $this            = {};
    bless $this,        @_[0];
    $this->{fileNames}  = @_[1];
    $this->{sandboxTag} = @_[2];
    $this->{tmpDir}     = @_[3];
    $this->{verbose}    = @_[4];

    $this->Initialize();

    return $this;
}

#-----------------------------------------------------------
#!! Initialize the members and some command paths.
#-----------------------------------------------------------

sub Initialize
{
    my $this = shift;

    if (! $this->{fileNames})
    {
        $this->{fileNames} = new CDSFileNames();
    }

    if (! $this->{tmpDir})
    {
        $this->{tmpDir} = $this->{fileNames}->TmpCommandForkDir();
    }

    if (! $this->{sandbox})
    {
        $this->{sandbox} = $this->{fileNames}->FindSandboxPath();
    }
# Commands used in this class.
#------------------------------
    $this->{cvsCmd}  = $this->{fileNames}->Command("cvs");
    $this->{grep}    = $this->{fileNames}->Command("grep");
    $this->{netstat} = $this->{fileNames}->Command("netstat");
    $this->{cvsMode} = "status";
}

#----------------------------------------------------------
#!! Returns the cvs command.  If the command is 'cvs checkout',
#!! "checkout" is the mode.
#----------------------------------------------------------
sub Mode
{
    my $this = shift;
    return $this->{cvsMode};
}

#----------------------------------------------------------
#!! This method monitors the number of connections to the
#!! cvs server.  This class forks commands, which opens more
#!! more than one connection.  In testing it seems that more 
#!! than 25 connections started to cause 'connection refused'
#!! errors, and so this limits how many we can make to 18.
#----------------------------------------------------------
sub MaxConnectionsReached
{
    my $this           = shift;
    my $maxConnections = 18;
    my $cmd            = join " ", $this->{netstat},"-a |",
    $this->{grep}, "pserver";
    my @nets           = split /\n/, CDSSystem::BackTick($cmd);
    my $cnt            = scalar (@nets);

    if ($ENV{QA_DEBUG} == 1)
    {
        # Only print when the number changes, else we'll print 
        # every sleep. Sleep controlled by the caller.

        if ($cnt != $this->{connectionCount})
        {
            print "Currently have $cnt connections to cvspserver\n";
        }
    }

    $this->{connectionCount} = $cnt;

    if ($cnt < $maxConnections)
    {
        $cnt =  0;
    }

    return $cnt;
}

#-------------------------------------------------------
#!! Method executes the passed in cvs command, in every
#!! top module of someone's sandbox/
#!! Once this object is created, this is what you would
#!! call to execute the command forks.
#-------------------------------------------------------
sub Run #( string, string, string, string)
{
    my $this = shift;
    $this->{sandbox}  = shift;
    $this->{preOpts}  = shift;
    $this->{cvsMode}  = shift;
    $this->{postOpts} = shift;
    $this->{fileList} = "";
    $this->{results}  = "";

    if (! -d $this->{tmpDir})
    {
        mkpath ($this->{tmpDir});
    }

    $this->RunInDir($this->{sandbox});

    my $cnt = scalar (@{$this->FileList()});

    return $cnt;
}

#--------------------------------------------------------
#!! After the CVSCmdFork::Run() method finishes, this method
#!! is used to get the list of files that captured the output
#!! from each fork.
#--------------------------------------------------------
sub FileList #()
{
    my $this = shift;

    if (! $this->{fileList})
    {
        my @fileList = ();
        
        foreach (CDSSystem::OpenDir($this->{tmpDir}, ".".$this->{cvsMode}))
        {
            chomp;
            my $file = $this->{tmpDir}."/$_";

            if (! -z $file)
            {
                push (@fileList, $file);
            }
            else
            {
                unlink ($file);
            }
        }
        
        $this->{fileList} = \@fileList;
    }
     
    return $this->{fileList};
}

#--------------------------------------------------------
#!! This method returns the contents of all files produced,
#!! the list of files returned by CVSCmdFork::FileList().
#--------------------------------------------------------
sub Results
{
    my $this = shift;

    if (! $this->{results})
    {
        my @results = ();
        $this->{results} = \@results;
        
        foreach my $file (@{$this->FileList()})
        {
            if ($ENV{QA_DEBUG})
            {
                print STDERR "Assimilating $file\n";
            }
            
            my $rh = CDSSystem::OpenRead($file);
            
            while (<$rh>)
            {
                my $ignore = "cvs ".$this->{cvsMode}.":";
                
                if ($_ !~ /^$ignore/)
                {
                    push (@{$this->{results}},$_);
                }
            }
            
            close ($rh);
        }
    }
    
    return $this->{results};
}

#----------------------------------------------------------------
#!! This method takes a start directory as a top directory. This
#!! is usually the top of someone's sandbox, which would have a
#!! src/ and a scripts/. etc.
#!! From this point, it looks for any CVS repository directories.
#!! When found, it forks a method RunCvsCommand to run the asked
#!! for cvs command string.
#!! it will do this while there are directories and while we do
#!! not have more than 18 cvs server connections. 
#!!
#!! It uses the PidSetMgr class to track the forked pids of the
#!! children.
#----------------------------------------------------------------
sub RunInDir
{
    my $this     = shift;
    my $startDir = shift;
    my $jobName  = $this->{cvsMode}. " $startDir";
    my $pidMgr   = new PidSetMgr($jobName, "", $this->{verbose});
    my @dirs     = CDSSystem::OpenDir($startDir);

    foreach my $module (reverse @dirs)
    {
        if (-f "$startDir/$module/CVS/Entries")
        {
            my $modPath = "$startDir/$module";
            $modPath =~ s/$this->{sandbox}\///;
            
            my $alias = "checking $modPath";
            
            while ($this->MaxConnectionsReached())
            {
                sleep 1;
            }

            my $pidNum = CDSSystem::ForkProcess 
                (\&RunCvsCommand, 1, $this, $module);

            if ($pidNum > 0)
            {
                my $pid = 
                    new PidInfo($pidNum, $alias,
                                $this->{fileNames}->CmdPaths(),
                                $this->{fileNames}->CdsHosts()->HostUname());
                
                $pidMgr->AddPids($pid);
            }
        }
    }

    while ($pidMgr->NumActive() > 0)
    {
        sleep 1;
    }
}

#----------------------------------------------------------------
#!! Method is forked from CVSCmdFork::RunInDir().  Runs the actual 
#!! cvs command as passed into CVSCmdFork::Run().  Creates its own
#!! log file naming using the directory name passed.
#----------------------------------------------------------------
sub RunCvsCommand #(string module_directory)
{
    my $this   = shift;
    my $module = shift;
    my $rtn    = 2;

    my $outFile = join "/", $this->{tmpDir}, (join ".", $module, $this->{cvsMode});
    my $cvsCmd = join " ", $this->{cvsCmd},
    $this->{preOpts}, $this->{cvsMode}, $this->{postOpts}, " $module >& $outFile";
    
    $rtn = CDSSystem::System($cvsCmd);
    
    print STDERR ".";

    return $rtn;
}


1;
