#-------------------------------------------------------------------------
# Author Dave Allaby, dallaby@carbondesignsystems.com
#========================================================================
# Class CDSUsage.
# 
#!! Description:
#!!
#!! A class that understands how to describe and present all arguments 
#!! avaliable to any script in the %select hash.
#!! Included by CDSArgs.pm package.
#!! Any argument additions in CDSArgs.pm need to also be defined here or
#!! they will not print out in the calling script's usage listing.
#
#!! All the option descriptions defined here, but are not shown on
#!! on the wiki.  See infrastructure/perl_modules/CDSUsage.pm for details.
#========================================================================

#=======================================================================
#
# All options are defined here, one to each hash key which is the same
# as the option.  This let's us define an option once in one place.
#
# So a particular script, using CDSArgs can dump all of it's available
# options, we need a %select hash, where we can use the script name
# as key and the secondary key is the options that are valid for that
# script.  These would be dumped for "./$0 -help" commands.
#
# The default value for $select{script}{optionName} is 1.  If 1 then
# dump message, but I have ideas about allowing a string description
# which could override the default description outout.  So far, these
# been no real need to overload the descriptions so I haven't done this.
#-------------------------------------------------------------------------
package CDSUsage;

use strict;

my %usage = ();

# Selector script hash.
#-----------------------------
my %select = {};

#---------------------------------------------------------------------
# Assign all descriptions of arguments
#---------------------------------------------------------------------

$usage{bom}      =  "<bill-of-materials> ".        
                    "Create a .tgz install file that contains only the ".
                    "files listed in the specified bill of materials file. ".
                    "The first three space-delimited fields of each line of ".
                    "the file should be: 1) a 1 character file type ".
                    "specification (f, d, or l), 2) a path relative to the ".
                    "install tree, and 3) a file mode to chmod the file. ".
                    "(a 4th arg is required for symlinks).";
$usage{builddebug} = "Build the debug targets as well as the product targets, and ".
                     "assemble a deployment package which contains a debug version ".
                     "of the product, along with other internal utilities.".
                     "WARNING: This adds over 3 GB to the disk space usage for the ".
                     "release build, so be sure you have the space before turning ".
                    "this on!";

$usage{cdb}      = "Passes '-DCDB -DCHECKIN -DLICENSED' switches to the build.";
$usage{core}     =  "Do not use ulimit to suppress generation of core files, ".
                    "which will happen unless this option is specified. Note ".
                    "that using this switch can cause many VERY large core ".
                    "files to be generated and will probably fill up a disk.".
                    "You are better off calling 'runsingle -core' on ".
                    "specific tests.";
$usage{docs}      = "<alternate-doc-path> ".
                    "By default, documentation will be checked out to the same tag ".
                    "as the rest of the code. -docs will fetch user documentation ".
                    "from a previous release area specified by the argument.";
$usage{failrerun} = "Run the tests that failed the last time runlist was run ".
                    "In this \$CARBON_HOME";
$usage{nogrid}    = "Disable SGE grid use. Also -nofarm";
$usage{overwrite} = "Avoid the interactive prompts asking if it is OK to ".
                    "trash pre-existing test output. (Also -o) ";
$usage{nocleanup} = "Don't cleanup test directories upon completion";
$usage{noexecenv} = "Suppress creation of a separate execution environment ".
                    "that will be copied to each farm execution host at test ".
                    "runtime.";
$usage{nolinktree} = "Make a 'real' copy of tests not a symlink copy";
$usage{rundir}     = "<rundir> ".
                     "Run the tests in the named directory instead of the ".
                     "default location. Also -nogrid";
$usage{debug}      = "Sets RUNLIST_DEBUG to 1.  Makes messaging verbose. ".
                     "Option is passed to runsingle and runinplace.  Setting ".
                     "RUNLIST_DEBUG will also cause this to be set.";
$usage{downserver} = "Bring down the wintestserver when finished with it ";
$usage{help}       = "Prints a list of options for this script";
$usage{installer}   = "If building a patch release, include an installer script ".
                      "that will remove deleted files/dirs and create an uninstall ".
                      "package, as well as an uninstall script. This option is not ".
                      "very well tested, and should be used with caution.";
$usage{interactive} = " Submit farm jobs at high SGE priority ".
                      "(0, default is -25) Use when you will sit and watch ".
                      "the output";
$usage{maillist}    = "<Comma-separted-list-of-users> ".
                      " Ex. ->  -maillist jsmith,qa,eng.  If the domain portion of ".
                      "the address is appended to by script if the \@domainname.com ".
                      "portion is omitted.";
                      
$usage{latenight}   = "Start jobs at midnight";
$usage{lowpri}      = "Submit farm jobs at low SGE priority (-50, default ".
                      "is -25)";
$usage{nocarbonconfig} = "Do not reconfigure the build areas. This will reuse ".
                      "any pre-existing build in the directories created by MakeRelease. ".
                      "It will NOT however, use any manual builds in your \$CARBON_HOME. ".
                      "*This switch should NOT be supplied for official release builds.*";
$usage{nokillclean} = "Prevent automatic initiation of ".
                      "scripts/runlist-killclean which occurs at the end ".
                      "if all tests pass or if runlist is interrupted with ".
                      "ctrl-c.";
$usage{noicc}      = "Omit building and testing and possibly packaging the ".
                       "ICC package.";
$usage{nolinux}      = "Omit building and testing and possibly packaging the ".
                       "linux package.";
$usage{nolinux64}    = "Omit building and testing and possibly packaging the ".
                       "linux64 package.";
$usage{nosunos}      = "Omit building and testing and possibly packaging the ".
                       "SunOS package.";
$usage{nowindows}    = "Omit building and testing and possibly packaging the ".
                       "Windows package.";
$usage{nolm}        = "Do not override LM_LICENSE_FILE";
$usage{novm}        = "Do not attempt to restart the Virtual Machine ".
                      "when testing on Windows.";
$usage{oldvectors}  = "If CARBON_CBUILD_ARGS is set, will run cbuild first ".
                      "without CARBON_CBUILD_ARGS to generate a 'clean' ".
                      "test.vectors file.  Then will rerun cbuild as ".
                      "specified with the old test.vectors.";
$usage{patch}       = "<last-tag> ".
                      "Build a patch build based on the <last_tag> CVS tag for ".
                      "installation over an installed \$CARBON_HOME area. Only files ".
                      "that differ from the <last_tag> install will be packaged and ".
                      "released.";
$usage{quick}       = "Run the QUICK version of a test if such a version ".
                      "is defined in the test's test.run file.";
$usage{relpackage}  = "<release-path> ".
                      "Make the release in the specified directory. By default, ".
                      " a directory named './carbon-<tag-><date>' will be used. ".
                      "If you want to make a release based on an existing sandbox, ".
                      "set this to the root of the sandbox. No code will be checked ".
                      "out, however, so make sure the sandbox is complete.";
$usage{reltest}     = "Specify that an official product release is being ".
                      "used. This disallows CVS updating of tests.";
$usage{runtests}    = "Run runlist for each valid and desired platforms.";
$usage{saveruninfo} = "Move the test directory and all supporting runlist ".
                      "files and directories into a try[n] directory to ".
                      "maintain failure history for later analysis.  This ".
                      "is used by the nightly test scheme that does an ".
                      "automatic failrerun.";
$usage{setuponly}   = "Set up test area and copy all tests to the ".
                      "object/execution area, then exit cleanly, running ".
                      "no tests.";
$usage{tag}         = "<tag> ".
                      "Use this tag to checkout the source before building. By ".
                      "default, a timestamp is generated when the script is run ".
                      "and that timestamp is passed to CVS";

#--------------------------------------------------------------------------
# Note capital "G". There is a lowercase testgroup hash values as well.
# testGroupIn holds the string from the command line list1,list2,etc3.  The
# lower case "testgroup" is key for the testgroups after parsing from
# the string on the command line.
#--------------------------------------------------------------------------

$usage{testGroupIn}   = "<list1,list2,list3> ".
                        "Run the union of the specified tests.  ".
                        "List must be a comma separated list, no spaces.\n";

#--------------------------------------------------------------------------
# These are the 2nd dimension testgroup keys, also allowed on the commmand
# line without using -testgroups.
#--------------------------------------------------------------------------


$usage{testgroup}{almostall}     = "Run almostall tests.";
$usage{testgroup}{checkin}       = "Run checkin tests.";
$usage{testgroup}{cosim}         = "Run cosim tests.";
$usage{testgroup}{custall}       = "Run all customer tests.";
$usage{testgroup}{custbig}       = "Run 'hot' customer tests.";
$usage{testgroup}{custhot}       = "Run hot customer tests.";
$usage{testgroup}{custsmall}     = "Run small customer tests.";
$usage{testgroup}{mindspeed}     = "Run mindspeed tests.";
$usage{testgroup}{nightly}       = "Run nightly tests.";
$usage{testgroup}{perlclass}     = "Run perlclass tests.";
$usage{testgroup}{unit_test}     = "Run unit tests.";
$usage{testgroup}{variant}       = "<variant_string> Run variant tests ".
                                   "represented by passed in string value.";
$usage{testgroup}{verilog}       = "Run verilog tests.";
$usage{testgroup}{vhdl}          = "Run vhdl tests.";
$usage{testgroup}{xactors}       = "Run xactors tests.";

$usage{testversions} = "Run testing sessions on each of the OS supported variant ".
                       "operating system versions.";
$usage{update}      = "Use 'cvs update' to complete a partial test ".
                      "directory before running a test. This will only ".
                      "affect to root directory (and all subdirectories) ".
                      "of the tests to run, and will use the timestamp ".
                      "of the current checkout.";
$usage{vcpcompiler} = "For every cbuild command found in a test.run, ".
                      "replace it with 'vspcompiler'";


#-----------------------------------------------------------------
# Mark clean_packages.pl
#-----------------------------------------------------------------

$select{"clean_packages.pl"}{debug} = 1;
$select{"clean_packages.pl"}{help} = 1;
$select{"clean_packages.pl"}{verbose} = 1;

#-----------------------------------------------------------------
# Mark runlist arguments
#-----------------------------------------------------------------

$select{runlist}{usage}       = "<options> <file-list-path>";
$select{runlist}{description} = "Perl script test harness to run developer tests.";
$select{runlist}{core}        = 1;
$select{runlist}{failrerun}   = 1;
$select{runlist}{nogrid}      = 1;
$select{runlist}{overwrite}   = 1;
$select{runlist}{nocleanup}   = 1;
$select{runlist}{noexecenv}   = 1;
$select{runlist}{nolinktree}  = 1;
$select{runlist}{rundir}      = 1;
$select{runlist}{debug}       = 1;
$select{runlist}{downserver}  = 1;
$select{runlist}{help}        = 1;
$select{runlist}{interactive} = 1;
$select{runlist}{latenight}   = 1;
$select{runlist}{lowpri}      = 1;
$select{runlist}{nokillclean} = 1;
$select{runlist}{nolm}        = 1;
$select{runlist}{oldvectors}  = 1;
$select{runlist}{quick}       = 1;
$select{runlist}{reltest}     = 1;
$select{runlist}{saveruninfo} = 1;
$select{runlist}{setuponly}   = 1;
$select{runlist}{testGroupIn} = 1;
$select{runlist}{update}      = 1;
$select{runlist}{vcpcompiler} = 1;
$select{runlist}{winx}        = 1;

#-----------------------------------------------------------------
# Mark runsingle arguments
#-----------------------------------------------------------------

$select{runsingle}{usage}       = "<options> <testname>";
$select{runsingle}{debug}       = 1;
$select{runsingle}{help}        = 1;
$select{runsingle}{nocleanup}   = 1;
$select{runsingle}{nogrid}      = 1;
$select{runsingle}{nolinktree}  = 1;
$select{runsingle}{nolm}        = 1;
$select{runsingle}{novm}        = 1;
$select{runsingle}{oldvectors}  = 1;
$select{runsingle}{overwrite}   = 1;
$select{runsingle}{putontop}    = 1;
$select{runsingle}{quiet}       = 1;
$select{runsingle}{quick}       = 1;
$select{runsingle}{rundir}      = 1;
$select{runsingle}{reltest}     = 1;
$select{runsingle}{setuponly}   = 1;
$select{runsingle}{update}      = 1;
$select{runsingle}{vspcompiler} = 1;
$select{runsingle}{variant}     = 1;



#-----------------------------------------------------------------
# Mark runinplace arguments
#-----------------------------------------------------------------

$select{runinplace}{usage}          = "cd to test, runinplace <options>";
$select{runinplace}{quick}          = 1;
$select{runinplace}{checkgolds}     = 1;
$select{runinplace}{failrerun}      = 1;
$select{runinplace}{help}           = 1;
$select{runinplace}{nocleanup}      = 1;
$select{runinplace}{nolm}           = 1;
$select{runinplace}{noshortcircuit} = 1;
$select{runinplace}{oldvectors}     = 1;
$select{runinplace}{performance}    = 1;
$select{runinplace}{setuponly}      = 1;
$select{runinplace}{vspcompiler}    = 1;
$select{runinplace}{winx}           = 1;


#-----------------------------------------------------------------
# Mark MakeRelease arguments
#-----------------------------------------------------------------

$select{MakeRelease}{bom}           = 1;
$select{MakeRelease}{builddebug}    = 1;
$select{MakeRelease}{copylogs}      = 1;
$select{MakeRelease}{cdb}           = 1;
$select{MakeRelease}{docs}          = 1;
$select{MakeRelease}{help}          = 1;
$select{MakeRelease}{installer}     = 1;
$select{MakeRelease}{mailist}       = 1;
$select{MakeRelease}{noinstalltest} = 1;
$select{MakeRelease}{noconfig}      = 1;
$select{MakeRelease}{nodoc}         = 1;
$select{MakeRelease}{noicc}         = 1;
$select{MakeRelease}{nolinux}       = 1;
$select{MakeRelease}{nolinux64}     = 1;
$select{MakeRelease}{nosunos}       = 1;
$select{MakeRelease}{nowindows}     = 1;
$select{MakeRelease}{relpackage}    = 1;
$select{MakeRelease}{runtests}      = 1;
$select{MakeRelease}{testversions}  = 1;
$select{MakeRelease}{tag}           = 1;
$select{MakeRelease}{verbose}       = 1;

#===================================================================
#
# USAGE UTILITY FUNCTIONS
#
#===================================================================

#!! Prints help output, uses a script name to select the appropriate
#!! output to display.
#------------------------
sub PrintHelp  #(const char* scriptName)
{
    my ($script) = (@_);

    print "\n===========================================================\n";
    if ($select{$script}{usage})
    {
	print STDERR "USAGE: $script $select{$script}{usage}\n";
    }
    else
    {
	print STDERR "USAGE $script <options>\n";
    }

    if ($select{$script}{description})
    {
	print STDERR "\nDescription: $select{$script}{description}\n";
    }
    else
    {
	print STDERR "\nNo script description available. Sorry.\n";
    }

    print "===========================================================\n\n",
    "  Option Descriptions:\n";


    foreach my $option (sort keys %usage)
    {
	if ($select{$script}{$option} == 1)
	{
	    if ($option ne "testGroupIn")
	    {
		PrintArgHelp($option, $usage{$option});
	    }
	    else
	    {
		PrintArgHelp("testgroup", $usage{"testGroupIn"});
	    }
	}
    }

    if ($select{$script}{testGroupIn})
    {
	print STDERR "\n\t===========================================================\n",
	"\tAdditional testgroup options which can also be\n",
	"\tspecified alone, without -testgroup and their descriptions:\n",
	"\t===========================================================\n";
	
	foreach my $opt (sort keys %{$usage{testgroup}})
	{
	    PrintArgHelp($opt, $usage{testgroup}{$opt});
	}
    }
}

#--------------------------------------------------------------
# This takes an option name and a message string and prints it
# under a common format.
# 
# Format:
#
#   -option  <string if option takes a string>
#           Descriptions
#
#  The bracketed info is embedded in the beginning of the
#  passed in message string and is stripped off.
#
#  The description strings passed in have no formatting of
#  thier own.  Printed line length is handled here.
#
#--------------------------------------------------------------
#!! Utility that exposes the case that the option is not boolean
#!! from the description string, and formats the description 
#!! message with word wrapping.
#--------------------------------------------------------------
sub PrintArgHelp
{
    my ($opt, $message) = (@_);
    my $optStr = "";

# If message has <foo_value> type string prepended, pull if off
# so that it can be display next to the option, rather than on
# the next line with the description.  
# Any option which takes a string as an argument
# should have this so it is distiguishable from a simple on/off
# switching argument.    

    if ($message =~ /^\<\S+\> /)
    {
	($optStr = $message) =~ s/\>.+$/\>/o;
	chomp ($optStr);
	$message =~ s/$optStr//;
    }

    print STDERR "\n\t-$opt:  $optStr\n";

    my @msg = split " ", $message;

    for (my $i = 0; $i <= $#msg; $i++)
    {
	my $m = "\t\t";
	my $maxLength = 50;
	my $lineLength = 0;

	while ($lineLength < $maxLength)
	{
	    my $tmpM = ($m .= $msg[$i]);
	    $lineLength = length($tmpM);

	    if ($lineLength < $maxLength)
	    {
		$m = "$tmpM ";
		$i++;
	    }
	}

	print STDERR "$m\n";
    }
}

sub VerifyArgs
{
    my ($script, @args) = (@_);
    my $err = 0;

    foreach my $arg (@args)
    {
	if ($arg =~ /\-\S+/)
	{
	    $arg =~ s/\-//;

	    if (! $select{$script}{$arg})
	    {
		if ($script eq "runlist" && $usage{testgroup}{$arg})
		{
		    #do nothing.
		}
		else
		{
		    print STDERR "Argument $arg is not recognized\n";
		    $err = 1;
		}
	    }
	}
    }

    return $err;
}

sub NumArguments
{
    my ($script) = (@_);
    my $no = values(%{$select{$script}});

    return $no;
}

1;
