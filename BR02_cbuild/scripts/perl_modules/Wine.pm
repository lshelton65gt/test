package Wine;

use Cwd;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CarbonQAData;
use CDSSystem;
use CDSFileNames;
use lib "$ENV{CARBON_HOME}/scripts";
use CarbonLinksMgr;

sub new 
{
    my $this           = {};
    bless $this,         @_[0];
    $this->{fileNames} = @_[1];
    $this->{linkMgr}   = @_[2];
    $this->{qaData}    = @_[3];

    $this->Initialize();

    return $this;
}

sub Initialize
{
    my $this = shift;

    if (! $this->{qaData})
    {
        $this->{qaData} = new CarbonQAData();
    }

    if (! $this->{fileNames})
    {
        $this->{fileNames} = new CDSFileNames();
    }

    $this->{knownWarnings} = "Application tried to create a window, ".
        "but no driver could be loaded.|".
        "Make sure that your X server is running and ".
        "that \$DISPLAY is set correctly.|".
        "Could not load Mozilla.|".
        "err:ole|fixme:winspool";

    $this->{wineHome} = $this->{fileNames}->WineHome();
    $this->{regedit}  = $this->{wineHome}."/bin/regedit";
}

sub SetWineEnv
{
    my $this = shift;

    if ($this->WineRun())
    {

        $ENV{WINEPREFIX}      = $this->{wineCreateDir}."/.wine";
        $ENV{LD_LIBRARY_PATH} = $this->{fileNames}->WineLibPath().
            ":$ENV{LD_LIBRARY_PATH}";
        delete $ENV{DISPLAY};
        $ENV{WINEDEBUG} = "err-imagelist";
    }
}

sub WineRun
{
    my $this = shift;
    
    return $this->{wineCreateDir};
}

sub CreateWineArea
{
    my $this               = shift;
    $this->{wineCreateDir} = shift;
    my $origLdLibPath      = $ENV{LD_LIBRARY_PATH};
    my $here               = cwd();
    my $ok                 = 0;
    my $wineCmd            = join "", $this->{wineHome},
                             "/bin/wineprefixcreate |& ", 
                             $this->{fileNames}->Command("egrep"),
                             " -v \"", $this->{knownWarnings},"\"";
  
    if (! -d $this->{wineCreateDir})
    {
        CDSSystem::Mkdir($this->{wineCreateDir});
    }
  
# Prep some environment variables

    $this->SetWineEnv();

      if (CDSSystem::Cd($this->{wineCreateDir}))
    {
        if (! CDSSystem::System($wineCmd))
        {
            $ok = 1;
        }
    }

    return $ok;
}

sub WineRegEdit
{
    my $this       = shift;
    my $editFile   = $this->{fileNames}->RegistryFile();
    my $regEditCmd = $this->{regEdit}, " $editFile";
    my $ok         = 0;
    
    if (CDSSystem::Cd($this->{wineCreateDir}."/.wine/dosdevices"))
    {
        if (! CDSSystem::System($regEditCmd))
        {
            $ok = 1;
        }

        CDSSystem::Cd($this->{wineCreateDir});
    }

    return $ok;
}

sub SetupDosDevices
{
    my $this       = shift;
    my $ok         = 0;
    my $here       = cwd();
    my $carbonHome = $this->{fileNames}->CarbonHome();
    my %dosLinks   = ('d:' => "/work",
                      'l:' => "$carbonHome/Win/lib",
                      'm:' => "$carbonHome/Win/lib/winx/shared");

    foreach (keys %dosLinks)
    {
        if (! -d $dosLinks{$_})
        {
            CDSSystem::Mkdir ($dosLinks{$_});
        }              
        
        if ($_ eq "c:")
        {
            $this->{cWindowsDir} = $this->{winCreateDir}."/.wine/c:/windows";
            rmtree ($this->{cWindowsDir}."/winsxs");
            
            my $target = $this->{fileNames}->CarbonToolsRoot()."/windows/WinSxS";
            my $link   = $this->{cWindowsDir}."/WinSxS";
            
            $this->{linkMgr}->Symlink ($target,$link);
        }
        
        $this->{linkMgr}->Symlink ($dosLinks{$_},$_);
    }

    return $this->WineRegEdit();
}


1;
