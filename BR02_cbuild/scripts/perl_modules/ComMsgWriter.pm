#===========================================================
# ComMsgWriter class.
# Author Dave Allaby.  dallaby@carbondesignsystems.com
#
#!! This class writes a commit message file, or uses a 
#!! ComMsgReader class to read an existing file if it is found.
#!! Then it merges the data from the old and a current modified
#!! file list into a new file.
#!!
#!! This class must be new'() for methods to work properly.
#============================================================

package ComMsgWriter;

use strict;
use File::Path;
use File::Basename;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use ComMsgReader;
use CDSFileNames;
use CDSSystem;

#--------------------------------------------------------
#!! Constructor
#!!
#!! Take a filename, bool, bool and a CDSFileNames class.
#--------------------------------------------------------
sub new #(string filename, int, int, CDSFileNames)
{
    my $this = {};
    bless $this, @_[0];
    $this->{comMsgFile}   = @_[1];
    $this->{needsBugs}    = @_[2];
    $this->{needsRelNote} = @_[3];
    $this->{fileNames}    = @_[4];

    $this->Initialize();

    return $this;
}

#-----------------------------------------------------
#!! Initialization of members
#-----------------------------------------------------
sub Initialize #()
{
    my $this = shift;

    if (! $this->{fileNames})
    {
        $this->{fileNames} = new CDSFileNames();
    }
# Setup for required fields.
#---------------------------
    $this->{msgFields}{reviewed_by} = "Reviewed by:";
    $this->{msgFields}{tests_done}  = "Tests Done:";
    $this->{msgDelimStr}            = "@@@@ DO NOT EDIT THIS LINE @@@@";

# If needsRelNote, add release notes reviewer to the required data fields.
# And if it needs release notes, assume that it needs a bugs affected
# field too.
    if ($this->{needsRelNote})
    {
        $this->{msgFields}{rel_notes_reviewed_by} 
        = "Release note reviewed by:";
        $this->{msgFields}{bugs_affected} = "Bugs Affected:";
    }
    elsif ($this->{needsBugs})
    {
        $this->{msgFields}{bugs_affected} = "Bugs Affected:";
    }

# If commit message file is more than just a filename, parse off
# a directory path as the commit message's directory.
    if ($this->{comMsgFile} =~ /\//)
    {
        $this->{comMsgDir} = dirname ($this->{comMsgFile});
    }
}

#-----------------------------------------------------
#!! Returns a hash reference to the hash that holds
#!! the message field hash.
#-----------------------------------------------------
sub ManditoryFields #()
{
    my $this = shift;
    return $this->{msgFields};
}

#-----------------------------------------------------
#!! Returns the delimiter string.
#-----------------------------------------------------
sub DelimiterString #()
{
    my $this = shift;
    return $this->{msgDelimStr};
}

#-----------------------------------------------------
#!! Returns the commit message file.
#-----------------------------------------------------
sub ComMsgFile #()
{
    my $this = shift;
    return $this->{comMsgFile};
}

#-----------------------------------------------------
#!! Number of manditory fields.
#-----------------------------------------------------
sub NumManditoryFields #()
{
    my $this = shift;
    my $fCnt = values (%{$this->{msgFields}});
    
    return $fCnt;
}

#-----------------------------------------------------
#!! Method that contructs the beginning of a commit
#!! message.
#-----------------------------------------------------
sub WriteComMsgFile  #(string file_loc)
{
    my $this         = shift;
    my $fileList     = shift; # file list array ref
    my $comMsgReader = shift; # We'll get a comMsgReader if there is previously
                              # a commit message and we want to try to merge.
    my $fileCnt      = 0;
    my $ver1         = $this->{fileNames}->UtVersionSandboxPath();
    my $ver2         = $this->{fileNames}->InfraVersionSandboxPath();
    my $lastCode     = "";


    if ($comMsgReader && $ENV{QA_DEBUG})
    {
        print "We have a reader.\n";
    }

    if ($this->{comMsgDir} && ! -d $this->{comMsgDir})
    {
        mkpath ($this->{comMsgDir});
    }

#----------------------------------------------------------------
# If we were passed a reader, we should have already read a stale
# commit message, so the data is in the reader object. We can
# remove the stale file, if it's still there.
#----------------------------------------------------------------
    if (-f $this->{comMsgFile})
    {
        unlink $this->{comMsgFile};
    }

# open file
#-----------
    my $wh = CDSSystem::OpenWrite($this->{comMsgFile});
        
# Write the required fields data.
#--------------------------------
    foreach (sort keys %{$this->{msgFields}})
    {
        my $stored = ""; 

        # If there is a reader, try to get the stored data for the field.
        #----------------------------------------------------------------
        if ($comMsgReader)
        {
            $stored = $comMsgReader->FieldData($this->{msgFields}{$_});
        }

        print $wh $this->{msgFields}{$_}, " $stored\n";
    }
# If there is a reader and it has user comments
# else just carriage return everywhere.
#----------------------------------------------
    if ($comMsgReader 
        && scalar (@{$comMsgReader->UserComments()}) > 0)
    {
        print $wh @{$comMsgReader->UserComments()}, "\n\n";
    }
    else
    {
        print $wh "\n\n\n\n\n\n", 
    }
# print delimiter string.
#-------------------------
    print $wh $this->{msgDelimStr}, "\n\n\n";

    foreach (sort @{$fileList})
    {
        chomp;

        if ($_ !~ /$ver1|$ver2/)
        {
            if ($_ =~ /^(M|A|R|C|\?){1} /)
            {
                my ($code) = (split " ", $_)[0];
                
                if ($code ne $lastCode)
                {
                    print $wh "\n";
                    $lastCode = $code;
                }

                $fileCnt++;

                print $wh "$_\n";
            }
        }
    }
    
    print $wh "\n\n\n";

    write ($this->{comMsgFile});
    close ($wh);

    if ($ENV{QA_DEBUG})
    {
        print STDOUT "Writing ", $this->{comMsgFile},"\n";
    }

#--------------------------------------------------------------
# Safely catch.  If something went horribly wrong, call fileCnt
# zero so we'll exit earlier. We exit if no modified files we're
# written.
#--------------------------------------------------------------
    if (! -f $this->{comMsgFile} ||
        -z $this->{comMsgFile})
    {
        print STDERR ref($this), ": No commit message file exists.\n";

        $fileCnt = 0;
    }

    return $fileCnt;
}

#--------------------------------------------------------------
#!! Method removes anything that was created by this class.
#--------------------------------------------------------------
sub Clean #()
{
    my $this = shift;
    unlink ($this->{comMsgFile});
}

1;
