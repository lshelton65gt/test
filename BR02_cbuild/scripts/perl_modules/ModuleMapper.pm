package ModuleMapper;


sub new
{
    my $this = {};
    bless $this, @_[0];
    my $modRef = @_[1];
    $this->{depth} = 0;
    if (! $modRef)
    {
        die "No module referenced was passed into ",
        ref ($this), "\n";
    }
    else
    {
        $this->{module} = $modRef;
    }
    return $this;
}

sub Map
{
    my $this = shift;
    my $keyStrings = shift;
    my $group = shift;

    if (! $group)
    {
        $group = $this->{module};
    }

    $this->{depth}++;
    my $grpRef = ref($group);

    print "Looking at ref $grpRef ", $group, " Depth is ", $this->{depth},"\n";

    if ($grpRef =~ /ARRAY/)
    {
        my $i = 0;

        if (@{$group})
        {
            foreach my $key (@{$group})
            {
                if (ref($key) =~ /HASH|ARRAY/)
                {
                    print "Call new mapping of $grpRef Keys is -> $key\n";
                    $newKeys  = $keyStrings .= "|$key";
                    $this->Map($newKeys, $key);

                }
                else
                {
                    print "Index is $i, value is ", $key,"\n";


                }
                
                $i++;
            }
        }
        else
        {
            print "ARRAY is empty\n";
        }
    }

    elsif ($grpRef =~ /HASH/)
    {
        if (values %{$group} > 0)
        {
            foreach my $key (keys %{$group})
            {
                if ($key eq "pr_sname")
                {
                    print "DEBUG key $key ", $group->{$key}," $group{$key}\n";
                    die;
                }

                if (ref($group->{$key}) =~ /HASH|ARRAY/)
                {
                    print "Call new mapping of $grpRef $key ", $group->{$key},
                    "\n";
                    $newKeys  = $keyStrings .= "|$key";
                    $this->Map($newKeys, $group->{$key});
                }
                else
                {
                    print "Keys strings -> $keyStrings\n",
                    "\tKey is $key, value is ", $group->{$key},"\n";
                }
            }
        }
        else
        {
            print "grpRef has no keys\n";
        }
    }
    else
    {
        foreach my $key (keys %{$group})
        {
            if (ref($group->{$key}) =~ /HASH|ARRAY/)
            {
                print "Call new mapping of $grpRef $key\n";
                    $newKeys  = $keyStrings .= "|$key";
                $this->Map($newKeys, $group->{$key});
            }
            else
            {
                print "Key is $key, value is ", $group->{$key},"\n";
            }
        }
    }

    $this->{depth}--;
}

1;
