#-----------------------------------------------------------------
# Class PidSetMgr.
# Author: Dave Allaby - dallaby@carbondesignsystems.com
#
# Class is a manager for a job set of pids, pids are represented
# using PidInfo class.
#
# It is the intent of this class that it accepts pids that were
# forked.  The forking call can sit and wait until this manager
# responds that all pids in the pid set are finished.
#
# It is also responsible for killing any pids in the set, and
# via the PidInfo class' methods, kill any spawned children owned by
# that pid.  We can insure that all our spawned kids have terminated.
#-----------------------------------------------------------------


package PidSetMgr;
use POSIX ":sys_wait_h";
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use PidInfo;
use CmdPaths;

sub new 
{
    my $this     = {};
    bless $this, @_[0];
    my $name     = @_[1];
    my $cmdPaths = @_[2];
    $this->{showExiting} = @_[3];

# You can name this particular instance
# if you want, else it's unnamed.
#----------------------------------------
    if (! $name)
    {
	$name = "unnamed";
    }

    $this->{name} = $name;

# Need a cmdpaths class, either passed, or make it.
#--------------------------------------------------
    if ($cmdPaths)
    {
	$this->{cmdPaths} = $cmdPaths;
    }
    else
    {
	$this->{cmdPaths} = new CmdPaths();
    }

# Init a pids hash as our pid set.
#--------------------------------

    $this->CleanHouse();

    return $this;
}

sub NumFinished
{
    my $this = shift;
    my $finished = values (%{$this->{pidStatus}});

    return $finished;
}

#-----------------------------------------
# Method used to insert pids into the
# pid set.
#
# We are trying to be flexible here. We
# will accept:
# 1. a single pid number
# 2. an array of pid numbers separated by
#    spaces.
# NOTE: We'll create generic PidInfo classes 
#    for case 1 & 2.
# 3. A single PidInfo class
# 4. An array of PidInfo classes separated by
#    spaces.
#
# If you need to name the pids something specific, or
# use jobAlias method of the pid, you'll need to create
# the PidInfo object first, then pass it to
# the PidSetMgr class, else you'll get a pid named
# after this pid set manager class as the pid's jobalias.
#
# The ref() calls within show whether or not the number
# is a PidInfo class reference or a number.
#---------------------------------------------
sub AddPids
{
    my $this = shift;
    
    foreach my $line (@_)
    {
	if ($line ne "")
	{
	    # If multiple field seps, break into array
	    if ($line =~ / /)
	    {
		my @tmp = split " ", $line;
		
		foreach my $pid (@tmp)
		{
		    if (ref($pid) =~ /PidInfo/)
		    {
			$this->{pids}{$pid->Num()} = $pid;
		    }
		    else
		    {
                        my $name = $this->{name} . ".$pid";
			my $p = new PidInfo($pid, $name);
			$this->{pids}{$pid} = $p;
		    }
		}
	    }
	    else # single word.
	    {
		if (ref($line) =~ /PidInfo/)
		{
                    $this->{pids}{$line->Num()} = $line;
		}
		else
		{
                    my $name = $this->{name} . ".$line";
		    my $p = new PidInfo($line, $name);
		    $this->{pids}{$line} = $p;
		}
	    }
	}
    }
}

sub AnyBadStatus
{
    my $this = shift;
    my $totalStatus = 0;

    $this->Update();

    foreach (keys %{$this->{pidStatus}})
    {
        $totalStatus += $this->{pidStatus}{$_}{status};
    }

    return $totalStatus;
}

sub ShowFailedJobs
{
    my $this = shift;

    foreach (keys %{$this->{pidStatus}})
    {
        if ($this->{pidStatus}{$_}{status} != 0)
        {
            print "Job pid $_:  Job name ", $this->{pidStatus}{$_}{alias}, 
            ":  exit status [",
            $this->JobAliasExitStatus($this->{pidStatus}{$_}{alias}),"]\n";
        }
    }
}

sub CleanHouse
{
    my $this = shift;

    my %pids = ();
    $this->{pids} = \%pids;

    my %pidexit = ();
    $this->{pidStatus} = \%pidexit;
}

sub JobAliasExitStatus
{
    my $this = shift;
    my $alias = shift;
    my $st = -2;

    foreach (keys %{$this->{pidStatus}})
    {
        if ($ENV{QA_DEBUG})
        {
            print "Checking $_ pid where alias -> $alias = ", 
            $this->{pidStatus}{$_}{alias}, "\n";
        }

        if ($this->{pidStatus}{$_}{alias} eq "$alias")
        {
            $st = $this->PidExitStatus($_);
            last;
        }
    }

    return $st;
}

sub PidExitStatus
{
    my $this = shift;
    my $pid = shift;
    my $st = -1;

    $this->Update();

    if ($this->{pidStatus}{$pid}{status})
    {
        $st = $this->{pidStatus}{$pid}{status};
    }

    return $st;
}

#------------------------------------------
# Method checks our pids set for activity.
# Clears inactive pids from pid set.
#------------------------------------------
sub Update
{
    my $this = shift;

    foreach my $pid (keys %{$this->{pids}})
    {
        # If not seen in the /proc directory, or it is, but
        # it is defunct...

	if (! $this->{pids}{$pid}->Active() ||
	    ($this->{pids}{$pid}->Active() && 
	    $this->{pids}{$pid}->Zombie()))
	{
            my $ans = waitpid ($pid, WNOHANG);
            my $st = ($?/256);
            
            if ($ans == $pid ||
                $ans == -1)
            {
                if ($this->{showExiting})
                {
                    print "Finished ", $this->{pids}{$pid}->JobAlias(), 
                    " [", $this->{pids}{$pid}->RunTime(),"] seconds.\n";
                }

                $this->{pidStatus}{$pid}{status} = $st;
                $this->{pidStatus}{$pid}{alias} = 
                    $this->{pids}{$pid}->JobAlias();
            }
                
            delete $this->{pids}{$pid};
	}
    }
}

#----------------------------------------------
# Returns number of active pids in our pid set.
#----------------------------------------------
sub NumActive
{
    my $this = shift;

    $this->Update();

    my $noActive = (values %{$this->{pids}});

    return $noActive;
}

#----------------------------------------------
#  This method calls the KillingSpree method of
# our pids, and then we off ourselves.
#----------------------------------------------
sub KillPids
{
    my $this = shift;
    
    foreach my $pid (keys %{$this->{pids}})
    {
        if ($this->{pids}{$pid}->Active() && 
            ! $this->{pids}{$pid}->Zombie())
        {
            $this->{pids}{$pid}->KillingSpree();
        }
    }
}

#----------------------------------------------
# Query to see if a particular aliased pid is
# still running.
#----------------------------------------------
sub AliasActive
{
    my $this = shift;
    my $alias = shift;
    my $active = 0;

    if ($alias)
    {
        foreach my $pid (keys %{$this->{pids}})
        {
            if ($this->{pids}{$pid}->JobAlias() eq "$alias")
            {
                $active = 1;
            }
        }
    }

    return $active;
}

#-------------------------------------------------
# Interface method which calls the JobAlias() in 
# the PidInfo class, for every class in our pid set.
#-------------------------------------------------
sub JobAlias
{
    my $this  = shift;
    my $pid   = shift;
    my $alias = "";

    if ($this->{pids}{$pid})
    {
	$alias = $this->{pids}{$pid}->JobAlias();
    }

    return $alias;
}

#-------------------------------------------------
# Interface method which calls the ProcessName() in 
# the PidInfo class, for every class in our pid set.
#-------------------------------------------------
sub ProcessName
{
    my $this     = shift;
    my $pid      = shift;
    my $procName = "";

    if ($this->{pids}{$pid})
    {
	$procName = $this->{pids}{$pid}->ProcessName();
    }

    return $procName;
}

#--------------------------------------------------------
# This checks to see if parent of pid is dead.
#--------------------------------------------------------
sub PidParentDead
{
    my $this = shift;
    my $pid = shift;
    my $alive = 1;

    if ($this->{pids}{$pid}->ParentPid() == 1)
    {
	$alive = 0;
    }

    return $alive;
}

sub DisplayActivePids
{
    my $this = shift;

    foreach my $pid (keys %{$this->{pids}})
    {
        $this->{pids}{$pid}->Pr();
    }
}



1;
