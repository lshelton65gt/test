package SystemCUtil;

use strict;
use File::Path;
use Cwd;
use lib "$ENV{CARBON_HOME}/scripts";
use CarbonQAData;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSSystem;
use CDSFileNames;
use QATarOps;

sub new
{
    my $this = {};
    bless $this, @_[0];
    $this->{fileNames} = @_[1];

    $this->Initialize();

    return $this;
}

sub Initialize
{
    my $this = shift;

    if (! $this->{fileNames})
    {
        $this->{fileNames} = new CDSFileNames();
    }

    $this->{qaDb}           = new CarbonQAData;
    $this->{sandbox}        = $this->{fileNames}->FindSandboxPath();
    $this->{tarMgr}         = new QATarOps();
    $this->{tmpExclude}     = "/tmp/systemc.exclude.$$";

    my $systemCDir1 = join "/", $this->{sandbox}, $this->{qaDb}->SystemCDirName();
    my $systemCDir2 = join "/", join "/", $this->{fileNames}->CarbonHome(), 
    $this->{qaDb}->SystemCDirName();

    if (-d $systemCDir1 && ! -l $systemCDir1)
    {
        $this->{systemcSrcDir} = $systemCDir1;
    }
    elsif (-d $systemCDir2 && ! -l $systemCDir2)
    {
        $this->{systemcSrcDir} = $systemCDir2;
    }
    else
    {
        die ref($this),  ": Cannot find systemc sources at: \n", 
        "$systemCDir1 or $systemCDir2 locations\n",
        "No package can be made.\n";
    }

    print STDOUT ref($this), " SystemC sources in ", $this->{systemcSrcDir},"\n";


}

sub SetupSystemCSrcsHere
{
    my $this = shift;
    my $carbonHome = shift;
    my $here = cwd();
    my $rtn = 0;

    if (! $carbonHome)
    {
        $carbonHome = $this->{fileNames}->CarbonHome();
    }

    my $syscDir = "$carbonHome/systemc";
    my $configFile = "$syscDir/configure";

    select STDOUT;

    if (! -f $configFile)
    {
        if ($ENV{QA_DEBUG})
        {
            print "SystemC config file $configFile NOT found.\n";
        }

        if (-d $syscDir)
        {
            print "Removing existing $syscDir location. Looks corrupted\n";

            rmtree ($syscDir);

        }

        CDSSystem::Mkdir ($syscDir);

        if (CDSSystem::Cd($syscDir))
        {
            my $tar    =  $this->{fileNames}->Command("tar");
            my $tarCmd = join "",
            "(cd ", $this->SystemCSrcDir(), " && ",
            "$tar cf - *) | $tar xvf -";
            
            $rtn = CDSSystem::System($tarCmd);

            if ($rtn != 0)
            {
                die "Could not transfer systemc sources from \n",
                $this->SystemSrcDir(), " to $syscDir.\n";
            }
            else
            {
                # Reset a zero return to true;
                $rtn = 1;
            }
            CDSSystem::Cd($here);
        }
    }
    else
    {
        print "$syscDir already exists and has file $configFile\n";
        $rtn = 1;
    }

    return $rtn;
}

sub SystemCSrcDir
{
    my $this = shift;
    return $this->{systemcSrcDir};
}

sub InitDepositDir
{
    my $this = shift;
    my $depositDir = shift;
    my $dir = "";

    if (! $depositDir)
    {
        $depositDir = join "/", $this->{fileNames}->CarbonHome(),
        "install-tree/systemc";
    }

    $depositDir = $this->{fileNames}->FullPath($depositDir);
    
    if (-l $depositDir)
    {
        unlink $depositDir;
    }

    if (! -d $depositDir)
    {
        mkpath($depositDir);
    }

    if (-d $depositDir)
    {
        $dir = $depositDir;
    }

    return $dir;
}

sub BuildSystemC
{
    my $this         = shift;
    my $carbonHome   = shift;
    my $here         = cwd();
    my $rtn          = 1;
    my $cdsInfo      = $this->{fileNames}->CdsHosts();
    my $isolatedHost = $cdsInfo->IsolatedHostName($this->{fileNames}->CarbonTarget());
    my $thisHost     = $cdsInfo->HostUname();
    my $buildScript  = "";

    if (! $carbonHome)
    {
        $carbonHome = $this->{fileNames}->CarbonHome();
    }

    if ($thisHost != $isolatedHost)
    {
        $buildScript = "/bin/bash -c 'bin/build-systemc";        
    }
    else
    {
        if ($0 =~ /MakeRelease/)
        {
            my $ssh = $this->{fileNames}->Command("ssh");
            my $qaHost = $this->{fileNames}->CdsHosts()->QARemoteCmdHost();
            $buildScript = "$ssh -n $qaHost 'env CARBON_HOME=$carbonHome ".$carbonHome."/scripts/build-systemc";
        }
    }

    if (CDSSystem::Cd($carbonHome, $this->ClassType()))
    {
        $rtn = CDSSystem::System($buildScript."' >& systemc/build.log");
        
        if ($rtn != 0)
        {
            $this->ShowFileContents("systemc/build.log");
        }

        CDSSystem::Cd($here, $this->ClassType());
    }

    return $rtn;
}

sub SystemCTarballLocation
{
    my $this = shift;

    if (! $this->{systemCTarball})
    {
        print STDERR "WARNING: No systemc tarball has been created yet.\n";
    }

    return $this->{systemCTarball};
}

sub MakeSystemCDistTarball
{
    my $this       = shift;
    my $depositDir = shift;
    my $rtn        = 0;
    my $here       = cwd();

    my $tgz = join "/", 
    $this->InitDepositDir($depositDir),"systemc-dist.tgz";
    
    if (-f $tgz)
    {
        print "Removing old file $tgz\n";
        unlink "$tgz";
    }

    if(CDSSystem::Cd($this->{systemcSrcDir})) 
    {
        if ($this->CreateExclusionFile())
        {
            $rtn = $this->{tarMgr}->TarSystemC($this->{tmpExclude}, $tgz)
        }
        else
        {
            print "Exclude file for system c not found at ", 
            $this->{tmpExclude}, "\n";
        }

        CDSSystem::Cd($here);
    }

    if (! -f $tgz || -z $tgz || $rtn == 0)
    {
	print ERROR "Tarpackage $tgz not made or was zero length\n";
    }
    else
    {
#        unlink ($this->{tmpExclude});
        print $this->ClassType(), ": Created file $tgz\n";
        $rtn = 1;

        $this->{systemCTarball} = $tgz;
    }

    return $rtn;
}

sub ClassType
{
    my $this = shift;
    return ref($this);

}
sub CreateExclusionFile
{
    my $this = shift;
    my $exclude = join "/", $this->{sandbox},"src/bom/systemc.exclude";
    my $rtn = 0;

    if (-f $exclude)
    {
        my $cmd = join " ", $this->{fileNames}->Command("cp"),
        $exclude, $this->{tmpExclude};
        
        $rtn = CDSSystem::System($cmd);
        
        $cmd = join " ", $this->{fileNames}->Command("find"),
        $this->{systemcSrcDir},"-name CVS >> ", $this->{tmpExclude};
    }
    
    if (-f $this->{tmpExclude} && ! -z $this->{tmpExclude})
    {
        $this->ShowFileContents($this->{tmpExclude});

        $rtn = 1;
    }
    
    return $rtn;
}    

sub ShowFileContents
{
    my $this = shift;
    my $file = shift;
    my $rh = CDSSystem::OpenRead($file);

    if ($rh)
    {
        print STDERR "DEBUG: Showing contents of file $file\n";

        while (<$rh>)
        {
            print STDERR "$_\n";
        }
        
        close ($rh);

        print STDERR "Closed file $file\n";
    }
}


1;
