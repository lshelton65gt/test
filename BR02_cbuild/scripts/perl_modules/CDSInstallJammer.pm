#------------------------------------------------------------------------
# Class CDSInstallJammer
# Author Dave Allaby.  dallaby@carbondesignsystems.com
# 
# Description:
# Class to interface with the requirements of InstallJammer
# operations.
#------------------------------------------------------------------------

package CDSInstallJammer;

use Cwd;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSSystem;
use CDSHosts;
use CDSFileNames;

#--------------------------------------------------------
# Passing CDSFileNames class is optional.
#--------------------------------------------------------
sub new
{
    my $this = {};
    bless $this, @_[0];
    $this->{fileNames} = @_[1];
    
    if (! $this->{fileNames})
    {
        $this->{fileNames} = new CDSFileNames();
    }
    
    $this->{cdsHosts} = $this->{fileNames}->CdsHosts();
    
    return $this;
}


sub InstallToPath
{
    my $this        = shift;
    my $installer   = shift;
    my $installPath = shift;
    my $rtn         = 0;

    if (! $installPath)
    {
        print "No install path given.  Where do you want to extract the tree?\n";
    }
    else
    {
        print "Extracting files with $installer to $installPath\n";
 
        my $cmd = "$installer -mode silent --prefix $installPath";
        $rtn = CDSSystem::System($cmd);

        if ($rtn == 0)
        {
            $rtn = 1;
        }
    }

    return $rtn;
}

#-------------------------------------------------------------
# This sets some default values, using QA classes, if needed.
# Then runs installjammer and returns 1 if success, 0 if failed
#-------------------------------------------------------------
sub BuildInstaller
{
    my $this               = shift;
    my $executableName     = shift;
    my $platform           = shift;
    my $installDir         = shift;
    my $longProductName    = shift;
    my $shortProductName   = shift;
    my $objDir             = shift;
    my $cms                = "Carbon Model Studio ";
    my $rtn                = 0;
    my $installJammerHome  = $this->{fileNames}->InstallJammerHome();

    #-----------------------------------------------------
    # Translates uname into what installjammer wants as a string
    # for image type
    #-----------------------------------------------------
    my %platformString     = ();
    $platformString{Linux} = "Linux-x86";
    $platformString{SunOS} = "Solaris-sparc";

    #--------------------------------------------------
    # Set some defaults if this has missing arguments,
    # and from most QA things, it will.  Some defaults
    # are shared by the release process, so changes WILL
    # have ramification on releasing the product.
    #
    # Make sure you check with QA before committing any
    # change.
    print "\n";
    #----------------------------------------------------
    # $platform
    # This refers to the image of the executable, NOT the
    # carbon package.
    #
    # If no platform, or the platform pass was not valid
    # default to the machine uname you are on.
    #-----------------------------------------------------
    if (! $platform)
    {
        $platform = $this->{cdsHosts}->HostUname();
        print "No platform specified.\n",
        "Setting platform as $platform\n";
    }

    # Version information in both long and short default
    # product names is gotten from cbuild -version output.
    #-------------------------------------------------------
    # Long product name. Sample:
    # "Carbon Design Systems C2008.04_6257.0711"
    #-------------------------------------------------------
    if (! $longProductName)
    {
        my $version      = $this->{fileNames}->CBuildVersion();
        $version         =~ s/\s//g;

        $longProductName = join "", "$cms ", $version;
        $longProductName =~ s/\(|\)/_/g;
        $longProductName =~ s/_$//;

        print "No long product name specified.\n",
        "Setting long product name as $longProductName\n";
    }
    else
    {
        # If passed, make sure this passed string does not have
        # any parenthesis - shell command issues, as this name
        # is used in directory names.
        $longProductName =~ s/\(|\)/_/g;
        $longProductName =~ s/_$//;
    }

    # Short product name. Sample:
    # "carbon-C2008.04_6257.0711"
    #--------------------------------------------------------------
    if (! $shortProductName)
    {
        ($shortProductName = $longProductName) =~ s/$cms /carbon\-/;
        $shortProductName =~ s/\s.+//;
        print "No short product name specified.\n",
        "Setting short product name as $shortProductName\n";
    }

    # Default write location of installer.
    # $CARBON_HOME/carbon-install-$platform
    #
    # Release process will pass this, and it differs 
    # from this default in that is will be written to 
    # the sandbox area.
    #---------------------------------------------------------------
    if (! $executableName)
    {
        $executableName = join "/", $this->{fileNames}->CarbonHome(),
        "carbon-install-$platform";

        print "No long installer name specified.\n",
        "Setting installer name as $executableName\n";
    }

    #-----------------------------------------------------------------
    # Default install-tree path:
    # $CARBON_HOME/install-tree
    #-----------------------------------------------------------------
    if (! $installDir)
    {
        $installDir = $this->{fileNames}->CarbonHome() . "/install-tree";
        print "No installation directory specified.\n",
        "Setting installation tree path as $installDir\n";
    }

    #-----------------------------------------------------------------
    # Object dir, used to find the mpi file path.  The build puts
    # it into an extended path of $objDir/unixInstaller/InstallProject
    #-----------------------------------------------------------------
    if (! $objDir)
    {
        $objDir = $this->{fileNames}->ObjectDir();

        print "No object directory specified.\n",
        "Setting object directory as $objDir\n";
    }

    # If $platform is something that installjammer handles...
    #-----------------------------------------------------------
    if ($platformString{$platform})
    {
        #-----------------------------------------------------------------
        # Create the installJammer command.
        #-----------------------------------------------------------------
        
        my $createCmd = join " ", "$installJammerHome/installjammer",
        "--platform", $platformString{$platform}, 
        "-DAppName $longProductName", 
        "-DShortAppName $shortProductName",
        "-DCarbonInstallExecutableName $executableName", 
        "-DBuildDir $installDir",
        "--build-for-release", 
        $this->{fileNames}->JammerMPIFile($objDir);
        
        
        # Display what we're passing to installJammer
        #----------------------------------------------------------------
        print "\n====================================================\n",
        "INSTALLER BUILD INFO:\n\n",
        "Using configuration file ", 
        $this->{fileNames}->JammerMPIFile($objDir),"\n",
        "Packaging tree $installDir\n",
        "Building $platform executable for $longProductName ",
        "($shortProductName)\n",
        "Will create installer at $executableName\n",
        "====================================================\n";
        
        $rtn = CDSSystem::System($createCmd);

        # What passes:
        #
        # If installjammer exits clean, the installer exists and is not
        # zero length.
        #-----------------------------------------------------------------
        if ($rtn == 0 && -f $executableName && ! -z $executableName)
        {
            $rtn = 1;
        }
        else
        {
            $rtn = 0;
        }
    }
    else
    {
        print STDERR "\n",
        "ERROR:  Platform $platform is not valid for installjammer.\n",
        "Only Linux and SunOS are currently accepted.\n",
        "Cannot continue.\n\n";
    }
    
    return $rtn;
}





1;
