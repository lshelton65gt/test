package PerfConfXMLReader;

use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use XMLReader;

sub new 
{
    my $this = {};
    my $xmlFile = @_[1];
    bless $this,@_[0];

    my %perfHosts = ();
    $this->{perfHosts} = \%perfHosts;

    $this->{xmlReader} = new XMLReader($xmlFile);
    $this->{xmlReader}->ParseFile($this);


    return $this

}

sub ElementStart
{
    my $this = shift;
    ($this->{expat}) = shift;
    ($this->{currTag}) = shift;
}

sub ElementChar
{
    my $this = shift;
    ($this->{expat}) = shift;
    ($this->{currData}) = shift;

    if ($this->{currTag} eq "node")
    {
        $this->{perfHosts}{$this->{currData}}  = $this->{currData};
    }

}

sub ElementEnd
{
    my $this  = shift;
    
}

sub PerfHostList
{
    my $this = shift;
    return %{$this->{perfHosts}};
}

1;
