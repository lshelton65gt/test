
package CDSBuilder;

use FileHandle;
use Cwd;
use File::Path;
use File::Copy;

use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CVSTool;
use CDSFileNames;
use CDSSystem;
use CDSHosts;
use SGEMgr;

sub new 
{
    my $this = {};
    bless $this, @_[0];
    $this->{platform} = @_[1];
    $this->{sandbox}   = @_[2];
    $this->{fileNames} = @_[3];
    $this->{sgeMgr}    = @_[4];
    $this->{pidMgr}    = @_[5];

    $this->Initialize();

    return $this;
}

sub Initialize
{
    my $this = shift;

    $this->{fileNames} = new CDSFileNames();
    $this->{hostInfo} = $this->{fileNames}->CdsHosts();

    my %flags = ();
    $this->{flags} = \%flags;
    $this->{flags}{Win} = " -winx";

    $ENV{CARBON_TARGET_ARCH} = $this->{platform};
}

sub Build
{
    my $this = shift;
    my $rtn = -1;

    $rtn = $this->CarbonConfig();

    if ($rtn == 0)
    {
        $rtn = $this->MkCds();
    }

    if ($rtn == 0)
    {
        $rtn = $this->VerifyCompleted();
    }
    
    return $rtn;
}

sub VerifyCompleteBuild
{
    my $this = shift;

    return 1;
}

sub RunRemoteSandboxCmd
{
    my $this = shift;
    my $execName = shift;
    my $additionalArgs = shift;
    $additionalArgs .= $this->{flags}{$this->{platform}};

    my $execLogName = "";
    ($execLogName = $execName) =~ s/^.+\///g;

    my $host        = $this->{sgeMgr}->GetSingleHost($this->{platform});

    if (! $host)
    {
        die "Could not get a host for platform ", $this->{platform},"\n";
    }

    my $audit = join "", $this->{sandbox},
    "/$execLogName.", $host, ".", $this->{platform}, ".out";


    my $ssh         = $this->{fileNames}->Command("ssh");
    my $cmd         = join "", "$ssh -n $host '(cd ", 
    $this->{sandbox}, "; env CARBON_HOME=",
    $ENV{CARBON_HOME}, " $execName $additionalArgs ",
    $ENV{CARBON_HOME}," ) >& $audit'";

    print "Running remote command: $cmd\n";

    $rtn = CDSSystem::System($cmd);

    return $rtn;
}

sub CarbonConfig
{
    my $this           = shift;
    my $additionalArgs = shift;
    my $script         = "scripts/carbon_config";

    my $rtn            = $this->RunRemoteSandboxCmd($script, $additionalArgs);


    return $rtn;
}

sub MkCds
{
    my $this           = shift;
    my $additionalArgs = shift;
    my $script         = "scripts/mkcds";
    my $rtn            = $this->RunRemoteSandboxCmd($script, $additionalArgs);

    return $rtn;
}

1;
