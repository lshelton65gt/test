#----------------------------------------------------------
#!! Description:
#!!
#!! A class to keep any useful little algorithms that might
#!! be useful to various generic classes or scripts.
#!! Class is not new()'d.  Method access is static-like.
#----------------------------------------------------------

package Algorithm;

#----------------------------------------
#!! Function to randomize an array
#----------------------------------------
sub FisherYatesShuffle # (array ref)
{
    my $list = shift;
    my $elementCnt = scalar (@$list);

    for (my $i = $elementCnt; $i >= 0; $i--)
    {
	my $j = int rand ($i+1);

        if ($i != $j)
        {
            @$list[$i,$j] = @$list[$j,$i];
        }
        else
        {
            $i--;
        }
    }

    return $list;
}

#------------------------------------------
#!! Returns a random string of desired length.
#!! String contains alphanumeric chars
#--------------------------------------------
sub RandomAlphaNumeric #(int length)
{
    my ($maxLen) = MaxRandom(@_);
    @chars = qw (a b c d e f g h i j k l m
                 n o p q r s t u v w x w z);
    my $no = "";

    while (length ($no) < $maxLen)
    {
        my $mode = int(rand(3));

        if ($mode == 0)
        {
            $no .= $chars[int(rand(26))];
        }
        elsif ($mode == 1)
        {
            $no .= uc ($chars[int(rand(26))]);
        }
        else
        {
            $no .= int(rand(9));
        }
    }

    return $no
}

#!! Returns a random string of desired length.
#!! String contains only integers
#----------------------------------------------
sub RandomNumeric #(int length)
{
    my ($maxLen) = MaxRandom(@_);
    my $no = int(rand(9));  

    while (length($no) < $maxLen)
    {
        $no .= int(rand(9));
    }

    return $no;
}

#!! Returns a passing in length, or the default
#!! string lenght if no length is given.
#----------------------------------------------
sub MaxRandom #(int length)
{
    my ($no) = (@_);

    if ($no == 0)
    {
        $no = 8;
    }

    return $no;
}

1;
