
#-------------------------------------------------------------------
# Class AllTests.
# Author Dave Allaby. dallaby@carbondesignsystems.com
#
#!! Description:
#!!
#!! Interface to perl's XML::Parser::Checker to read
#!! test/alltests.xml and sort tests into various
#!! test lists.
#--------------------------------------------------------------------
package AllTests;

use strict;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use XMLReader;
use CDSTest;
use SGEMgr;

#----------------------------------------
#!! Creates instance of AllTests class.
#----------------------------------------
sub new #(string variant, CDSHosts, CDSFilesNames, SGEMgr)
{
    my $this = {};
    bless $this, @_[0];
    my $variant = @_[1];
    my $hInfo   = @_[2];
    my $fn      = @_[3];
    my $sgeMgr  = @_[4];

    $this->{hostInfo} = $hInfo;
    $this->{fileNames} = $fn;

    if ($variant && $variant ne "")
    {
	$this->{variant} = $variant;
    }
    else
    {
	$this->{variant} = "default";
    }

    if ($sgeMgr)
    {
	$this->{sgeMgr} = $sgeMgr;
    }

    $this->Initialize();

    return $this;

}

#--------------------------------------
#!! Initializes the classes variables.
#--------------------------------------
sub Initialize #()
{
    my $this = shift;
    my %tmpVariants= ();
    my @childOrder = ();
    $this->{childOrder} = \@childOrder;

    $this->{readorder} = "";

    $this->{tmpVariants} = \%tmpVariants;
    
    $this->{xmlFile} = $this->{fileNames}->AllTestsXMLFile();
    $this->CreateGraph();
    $this->{currTest} = "";
    $this->{currData} = "";
    $this->{expat} = "";
    $this->{totTestCnt} = 0;    
    $this->{varTestTemplate} = "";
    $this->{elementCnt} = 0;
    $this->{implicitChildCnt} = 0;
    $this->{explicitChildCnt} = 0;
    $this->CreateGraph();
    
}

#----------------------------------------
#!! returns variant mode string
#----------------------------------------
sub VariantMode #()
{
    my $this = shift;
    return $this->{variant};
}

#----------------------------------------
#!! Reads test/alltests.xml file.  Builds all tests into a dependency graph.
#!! There are 2 types of dependencies, explicit and implicit.  Explicit
#!! dependencies are specified using the <dep> </dep> tags. Implicit
#!! dependencies exist when a test is located inside of another test.
#----------------------------------------
sub CreateGraph #()
{
    my $this = shift;
    my %graph = ();
    $this->{graph} = \%graph;

    $this->{xmlReader} = new XMLReader($this->{xmlFile});
    $this->{xmlReader}->ParseFile($this);
}

#-----------------------------------------------------
#!! Returns the test dependency graph
#-----------------------------------------------------
sub TestGraph #()
{
    my $this = shift;
    return %{$this->{graph}};
}

#----------------------------------------------------
#!! Function that perl's XML::Parser::Checker will
#!! pass each time an element begin tag is found in
#!! the XML file.
#!!
#!! Each time a test is found in the XML file, a CDSTest
#!! object is created if it doesn't exist for the test.
#----------------------------------------------------
sub ElementStart # ()
{
    my $this = shift;
    ($this->{expat}) = shift;
    ($this->{currTag}) = shift;
    my (%attr) = @_;

    $this->{elementCnt}++;

    $this->{attr} = \%attr;

    if ($this->{currTag} eq "variant" && $this->{currTest})
    {
	my $v = $this->EvalVariantTest();
	$this->{currData} = "";
    }
}

#----------------------------------------------------
#!! Function that perl's XML::Parser::Checker will
#!! pass each time an element tag value is found in
#!! the XML file.
#!!
#!! This will populate the attributes of the test using
#!! the methods in CDSTest class.
#----------------------------------------------------
sub ElementChar # ()
{
    my $this = shift;
    ($this->{expat}) = shift;
    ($this->{currData}) = shift;

    if ($this->{currTag} eq "name")
    {
	$this->{readorder} .= $this->{currData} . " ";
    }
	 
    if ($this->{currTag} eq "name")
    {
	$this->{currTest} = $this->EvalTest($this->{currData});
    }
    elsif ($this->{currTag} eq "group")
    {
	$this->{currTest}->AddGroup($this->{currData});
    }
    elsif ($this->{currTag} eq "dep")
    {
	my $explicit = 1; # It's explicitly designated a child test
                          #  using <dep> tag.
	my $child = $this->EvalTest($this->{currData});

	$this->{explicitChildCnt}++;

	$this->{currTest}->AddChildTest($child, $explicit);
	$child->AddParentTest($this->{currTest});
    }
    elsif ($this->{currTag} eq "size")
    {
	if ( $this->{sgeMgr})
	{
	    $this->{currTest}->SetQSubArgs($this->{currData});
	}
    }
    elsif ($this->{currTag} eq "arch")
    {
	$this->{currTest}->AddNotArch($this->{currData});
    }
    elsif ($this->{currTag} eq "waitfor")
    {
	my $waitTest = $this->EvalTest($this->{currData});
	$this->{currTest}->AddWaitForTest($waitTest);
    }
    elsif ($this->{currTag} eq "env")
    {
	foreach (keys %{$this->{attr}})
	{
	    $this->{currTest}->AddEnvironmentVar($this->{attr}{$_},$this->{currData});
	}
    }
    else
    {
#	print "Unhandled tag -> ", $this->{currTag}, "\n";
    }
}

#----------------------------------------------------------------
#!! Method will clone a test into different variants if
#!! the caller class/script wants to run all variants.
#----------------------------------------------------------------
sub EvalVariantTest #()
{
    my $this = shift;
    my $vname = "";

    foreach (keys %{$this->{attr}})
    {
	if ($_ eq "name")
	{
	    $vname = $this->{attr}{$_};
	}
	elsif ($_ eq "default")
	{
	    $default = $this->{attr}{$_};
	}
    }

    if ($this->{variant} eq "$vname" || 
	$this->{variant} eq "all" ||
	$this->{variant} eq "default" && $default == 1)
    {
	if (! $this->{varTestTemplate})
	{
	    $this->{varTestTemplate} = $this->{currTest};
	}

	my $varTestName = join ",", $this->{varTestTemplate}->Name(), $vname;

	$this->{currTest} = $this->EvalTest($varTestName, 1);
    }    
}

#----------------------------------------------------
#!! Function that perl's XML::Parser::Checker will
#!! pass each time an element tag value is found in
#!! the XML file.
#!!
#!! Method will close out a template variant test that was
#!! used to create the different variant tests.  We do not
#!! need the master test anymore.
#----------------------------------------------------------------

sub ElementEnd  #()
{
    my $this = shift;
    ($this->{expat}) = shift;
    my ($tag) = shift;

    if ($tag eq "test")
    {
	if ($this->{varTestTemplate})
	{
	    my $varTemplate = $this->{varTestTemplate}->Name();

	    delete $this->{graph}{$varTemplate};
	    $this->{varTestTemplate}->Delete();
	    delete $this->{varTestTemplate};
	}
    }
}

#-----------------------------------------------
#!! Returns the total number of tests in the graph.
#-----------------------------------------------
sub NumTests #()
{
    my $this = shift;
    return $this->{totTestCnt};
}

#--------------------------------------------------
#!! Method evaluates a test read from the alltests.xml file
#!! to see if there is already CDSTest object for this test
#!! and/or do the actual cloning of a variant test into a
#!! new instance of CDSTest, and return it.
#--------------------------------------------------
sub EvalTest #(string testname, bool variant_mode)
{
    my $this    = shift;
    my $test    = shift;
    my $varMode = shift;

    my $cdt = $this->{graph}{$test};

    if (! $cdt)
    {
	if (! $varMode)
	{
	    $cdt = new CDSTest($test);
	    $this->{graph}{$test} = $cdt;
	    $this->{totTestCnt}++;    
	}
	else
	{
	    $cdt = $this->{varTestTemplate}->Copy($test);
	    $this->{graph}{$test} = $cdt;
	}
    }

    return $cdt;
}

#--------------------------------------------------
#!! Method to analyze whether or not this test is nested
#!! within another test in the graph.  If it is, it is
#!! added as an implicit dependency of the parent test.
#--------------------------------------------------
sub AnalyzeImplicitness  #()
{
    my $this = shift;
    my $tCnt = 0;

    foreach my $test (keys %{$this->{graph}})
    {
	$tCnt++;
	my $childTest = $this->{graph}{$test};
	my $tmp = $test;

	while ($tmp =~ /^test\/\w+\//)
	{
	    $tmp = dirname($tmp);

	    if ($this->{graph}{$tmp})
	    {
		$this->{implicitChildCnt}++;
#		print STDOUT "Adding $test as an implicit child of test |$tmp|\n";
		$this->{graph}{$tmp}->AddChildTest($childTest);
	    }
	}
    }
}

#---------------------------------------------------
#!! Returns a list of tests, children nodes first.
#!! Uses recursive calls to MapChildren().
#---------------------------------------------------
sub ChildrenFirst  #()
{
    my $this = shift;
    my %cGraph = $this->CreateGraph();
    
    foreach my $tName (keys %{$this->{graph}})
    {
	if ($this->{graph}{$tName})
	{
	    $this->MapChildren ($this->{graph}{$tName});
	}
    }
    
    return @{$this->{childOrder}};
}

#----------------------------------------------------
#!! Recursively called to decend to the child tests
#!! in the test graph.
#----------------------------------------------------
sub MapChildren #(CDSTest test)
{
    my $this = shift;
    my $cdsTest = shift;
    
    if ($cdsTest->NumChildTests() == 0)
    {
	push (@{$this->{childOrder}},$cdsTest->Name());
	$cdsTest->Delete();
    }
    else
    {
	my %childTests = $cdsTest->ChildTests();
	
	foreach (keys %{$childTests{childTests}})
	{
	    $this->MapChildren($childTests{childTests}{$_});
	}
    }
}

#----------------------------------------------------
#!! Returns the number of implicit tests in the graph
#----------------------------------------------------
sub NumImplicitChildTests #()
{
    my $this = shift;

    return $this->{implicitChildCnt};
}

#----------------------------------------------------
#!! Returns the number of implicit tests in the graph
#----------------------------------------------------
sub NumExplicitChildTests #()
{
    my $this = shift;
    return $this->{explicitChildCnt};
}





1;
