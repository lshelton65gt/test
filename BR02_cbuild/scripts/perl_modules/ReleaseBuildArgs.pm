package ReleaseBuildArgs;

use Getopt::Long;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CarbonQAData;

sub new
{
    my $this = {};
    bless $this ,@_[0];
    $this->{fileNames} = @_[1];

    $this->Initialize();
    $this->{validParse} = $this->ParseArgs();
    return $this;
}


sub Initialize
{
    my $this = shift;

    $this->{qaData} = new CarbonQAData();
    
    my %platforms = ();
    $this->{platforms} = \%platforms;

    my %options            = ();
    $this->{options}       = \%options;

    $this->{options}{advanceTag}   = 0;
    $this->{options}{userCarbonHome} = "";
    $this->{options}{coTag}        = "";
    $this->{options}{help}         = 0;
    $this->{options}{platforms}    = "";
    $this->{options}{sandbox}      = "";
    $this->{options}{skipCheckout} = 0;
    $this->{options}{skipPackages} = 0;
    $this->{options}{skipTests}    = 0;

    my %validPlatform = ();
    $this->{validPlatform} = \%validPlatform;

    foreach (@{$this->{qaData}->ValidPlatforms()})
    {
        $this->{validPlatform}{$_} = $_;
    }
    
}

sub ValidPlatform
{
    my $this = shift;
    my $plat = shift;
    my $valid = 0;

    if ($this->{validPlatform}{$plat})
    {
        $valid = 1;
    }

    return $valid;
}

sub SetPlatforms
{
    my $this = shift;
    my @plats = ();


    if ($this->{options}{platforms})
    {
        @plats = split ",", $this->{options}{platforms};
    }
    else
    {
        print "Setting default platforms for release: @defplats\n";
        @plats = @{$this->{qaData}->ValidPlatforms()};
    }

    foreach (@plats)
    {
        if ($this->ValidPlatform($_))
        {
            $this->{platforms}{$_} = $_;
        }
        else
        {
            die "Unsupported platform $_ given.  Cannot continue\n";
        }
    }
    
    $platformCnt = values (%{$this->{platforms}});

    return $platformCnt;
}
    
sub DumpSettings
{
    my $this = shift;

    foreach my $opt (keys %{$this->{options}})
    {
        if (ref($opt) !~ /ARRAY/)
        {
            print STDERR "Option: $opt -> ", $this->{options}{$opt},"\n";
        }
        else
        {
            foreach (sort @{$this->{options}{$opt}})
            {
                print STDERR "Option: $opt: $_ -> ", $this->{options}{$opt}{$_},"\n";
            }
        }
    }
}

sub ValidParse
{
    my $this = shift;
    return $this->{validParse};
}

sub PrintHelp
{
    print "This is print help\n";
    
    foreach my $opt (sort keys %{$this->{options}})
    {
        if (ref($opt) !~ /ARRAY/)
        {
            print STDERR "Option: $opt -> ", $this->{options}{$opt},"\n";
        }
        else
        {
            foreach (sort @{$this->{options}{$opt}})
            {
                print STDERR "Option: $opt: $_ -> ", $this->{options}{$opt}{$_},"\n";
            }
        }
    }
}

sub ParseArgs
{
    my $this = shift;
    my $rtn = 0;
    
    GetOptions('-advancetag'    => \$this->{options}{advanceTag},
               '-carbonhome=s'  => \$this->{options}{userCarbonHome},
               '-checkouttag=s' => \$this->{options}{coTag},
               '-help'          => \$this->{options}{help},
               '-platforms=s'   => \$this->{options}{platforms},
               '-sandbox=s'     => \$this->{options}{sandbox},
               '-skipcheckout'  => \$this->{options}{skipCheckout},
               '-skippackages'  => \$this->{options}{skipPackages},
               '-skiptests'     => \$this->{options}{skiptests});
    
    
    if (! $Getopt::Long::error && ! $this->{options}{help} && ! @ARGV)
    {
        $rtn = $this->SetPlatforms();

        if ($this->{options}{userCarbonHome})
        {
            $ENV{CARBON_HOME} = $this->{options}{userCarbonHome};

            if ($this->{options}{sandbox})
            {
                $ENV{CARBON_SANDBOX} = $this->{options}{sandbox};
            }
        }
    }
    else
    {
        print "Problems parsing command line\n";

        if (@ARGV)
        {
            print "\nEXTRA ARGUMENTS-> @ARGV\n\n";
        }
            
        $this->PrintHelp();
    }

    print "ParseArgs returning $rtn\n";

    return $rtn;
}

sub AdvanceTag
{
    my $this = shift;

    return $this->{options}{advanceTag};
}

sub CheckoutTag
{
    my $this = shift;

    return $this->{options}{coTag};
}

sub MakePackages
{
    my $this = shift;
    my $mpack = 1;

    if ($this->{options}{skipPackages})
    {
        $mpack = 0;
    }

    return $mpack;
}

sub Platforms
{
    my $this = shift;
    
    return $this->{platforms};
}

sub RunTests
{
    my $this = shift;
    my $run = 1;

    if ($this->{options}{skipTests})
    {
        $run = 0;
    }
    
    return $run;
}

sub Sandbox
{
    my $this = shift;
    
    return $this->{options}{sandbox};
}

sub SkipCheckout
{
    my $this = shift;

    my $iverFile = join "/", $this->Sandbox(),
    $this->{fileNames}->InfraVersionSandboxPath();

    my $uverFile = join "/", $this->Sandbox(),
    $this->{fileNames}->InfraVersionSandboxPath();

    # Override what the user wants if we think the sandbox
    # is missing either of the 2 infrastructure version files.

#    print "Checking $iverFile and $uverFile\n";

    if (! -f $iverFile || ! -f $uverFile)
    {
        $this->{options}{skipCheckout} = 0;
    }

    return $this->{options}{skipCheckout};
}
1;
