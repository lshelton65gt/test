
#===========================================================
# Class SGEMgr
# Author Dave Allaby. dallaby@carbondesignsystems.com
#
# Description:
#
# This is a class which knows how to interact in various
# ways with the SunGrid Engine.
#
#===========================================================

package SGEMgr;

use strict;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSHosts;
use CDSSystem;

sub new 
{
    my $this = {};
    my $hInfo = @_[1];
    my $fn = @_[2];

    bless $this, @_[0];

# If not passed a CDSHosts object, make one.
#--------------------------------------------
    if (! $hInfo)
    {
	$this->{hostInfo} = new CDSHosts();
    }
    else
    {
	$this->{hostInfo} = $hInfo;
    }

# If not passed a CDSFileNames class, make one.
#----------------------------------------------

    if (! $fn)
    {
	$this->{fileNames} = new CDSFileNames;
    }
    else
    {
	$this->{fileNames} = $fn;
    }

# No SGE_ROOT - we're too stupid to do anything
# so die!!!
#----------------------------------------------

    if (! $ENV{SGE_ROOT})
    {
	die "No Environment variable SGE_ROOT set\n";
    }

    $this->Initialize();

    return $this;
}

sub ClassType
{
    my $this = shift;
    return ref($this);
}

sub Initialize
{
    my $this = shift;
    
    $this->ReadNoHostsFile();
    my %sgeOsMap       = ();
    $sgeOsMap{Win}            = "glinux";
    $sgeOsMap{Linux}          = "glinux";
    $sgeOsMap{"Linux.ES4"}    = "glinux";
    $sgeOsMap{"Linux.ES5"}    = "glinux";
    $sgeOsMap{"Linux.ES6"}    = "glinux";
    $sgeOsMap{Linux64}        = "glinux";
    $sgeOsMap{"Linux64.ES4"}  = "glinux";
    $sgeOsMap{"Linux64.ES5"}  = "glinux";
    $sgeOsMap{"Linux64.ES6"}  = "glinux";
    $this->{sgeOsMap}         = \%sgeOsMap;
    my %linuxOsVersion        = ();
    $this->{linuxVer}         = \%linuxOsVersion;
    # This code ensured that you run tests on an OS that is at least
    # as new as the machine you built CMS on (which is assumed to be
    # the machine you're launching tests from).  Since we haven't yet
    # allowed CMS to be built on anything other than ES4, that might
    # not actually be true.  I'm leaving it in place but commented out
    # for now.
    #
    # $this->{linuxVer}{ES4}    = "";
    # $this->{linuxVer}{ES5}    = " -l rhe5=1"; 

    $this->{carbonQsubArgs} = "";

    $this->{localhost} = $this->{hostInfo}->LocalHostName();
    $this->{deadManTries}     = 180; # x20 seconds = 1 hour max
    $this->{deamManCnt}       = 0;

    if ($ENV{CARBON_QSUB_ARGS})
    {
	$this->{carbonQsubArgs} = $ENV{CARBON_QSUB_ARGS};
    }
}

sub ReadNoHostsFile
{
    my $this = shift;
    my %noUse = ();
    $this->{noUsehosts} = \%noUse;
    my $host = $this->{hostInfo}->LocalHostName();
    $noUse{$this->{localhost}} = $this->{localhost};

    my $rh = CDSSystem::OpenRead($this->{fileNames}->SgeNoUseFile(),
				 $this->ClassType());

    while (<$rh>)
    {
	if ($_ !~ /^\#/)
	{
	    $noUse{$_} = $_;
	}
    }

    close($rh);
}

sub GetSingleHost
{
    my $this = shift;
    my $arch = shift;
    my $ref = $this->GetAvailableHostsForUser ($arch);

    return @{$ref}[0];
}

# I don't know of any user of this function that uses more than one arg
sub GetAvailableHostsForUser
{
    my $this       = @_[0];
    my $carbonTargetArch = @_[1];
    my $ignoreState = @_[2];
    my @validHosts = ();
    my %mach       = ();
    my $qstatCmd   = join " ", $this->{fileNames}->Command("qstat"), "-f";
    my $additionalArgs = "";

    if (! $carbonTargetArch)
    {
	$carbonTargetArch = $this->{fileNames}->CarbonTargetArch();
    }

    if ($carbonTargetArch =~ /Linux(|64)(|\.)/)
    {
	my ($osver) = (split /\./, $carbonTargetArch)[1];

        if (! $osver)
        {
            $osver = "ES4";
        }

	$additionalArgs .= " ".$this->{linuxVer}{$osver};

	if ($carbonTargetArch =~ /Linux64/)
	{
	    $additionalArgs .= " -l x86_64=1"; 
	}
#        else
#        {
#            $additionalArgs .= " -l x86_64=0";
#        }
    }
    elsif ($carbonTargetArch =~ /Win/)
    {
        $additionalArgs .= " -l vmfarm=1";
    }

    # dont use suse machines, they currently don't run distcc properly
    $additionalArgs .= " -l suse11=0 -l suse10=0 -l suse9=0";

    
    my $qselectCmd = join " ", $this->{fileNames}->Command("qselect"),
    "-U", $ENV{USER}, $this->{carbonQsubArgs}, $additionalArgs,
    " -l num_proc=.1 -l calendar=NONE -l arch=";
    $qselectCmd .= $this->{sgeOsMap}{$carbonTargetArch};

    while (! @validHosts)
    {
	my (@lines) = (split /\n/, CDSSystem::BackTick($qselectCmd));

	foreach (@lines)
	{
	    $_ =~ s/\.q$//;
	    $mach{$_} = $_;
	}
	
	(@lines) = (split /\n/,CDSSystem::BackTick($qstatCmd));

	foreach (@lines)
	{
	    if ($_ =~ /BIP/)
	    {
		my ($rhost, $state) = (split " ")[0,5];
		$rhost =~ s/\.q$//;

		if ($mach{$rhost} && (! $state || $ignoreState) && 
		    $_ =~ /$this->{sgeOsMap}->{$carbonTargetArch}/ && 
		    ! $this->{noUseHosts}{$rhost})
		{
		    push (@validHosts, $rhost);
		    delete $mach{$rhost};
		}
	    }
	    
	    if (values (%mach) <= 0)
	    {
		last; # we're out machines to consider, bail loop.
	    }
	}

	if (! @validHosts)
	{
	    $this->DoWait($carbonTargetArch);
	}
    }

    use Algorithm;
    Algorithm::FisherYatesShuffle(\@validHosts);

    return \@validHosts;
}

sub DoWait
{
    my $this    = shift;
    my $arch    = shift;
    my $slpTime = 20;

    if ($this->{deadManCnt} <= $this->{deadManTries})
    {
        print STDERR "Received no available $arch machines from the sge grid\n",
        "Sleeping $slpTime seconds...\n";
        sleep $slpTime;
        $this->{deadManCnt}++;
    }
    else
    {
        print STDERR "$0: Maximum number of wait tries reached. [", 
        $this->{deadManCnt}, "]\nA machine list of desired type ",
        "could not be obtained.\n";
        exit (2);
    }
}






1;
