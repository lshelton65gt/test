


package PackageLink;

use strict;

sub new
{
    my $this = {};
    bless $this, @_[0];
    $this->{link} = @_[1];
    $this->{target} = @_[2];

    return $this;
}

sub Name
{
    my $this = shift;
    return $this->{link};
}

sub Target
{
    my $this = shift;
    return $this->{target};
}


1;
