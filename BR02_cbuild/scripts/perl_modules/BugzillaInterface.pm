package BugzillaInterface;

use strict;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSHosts;
use CDSFileNames;


sub new
{
    my $this = {};
    bless $this, @_[0];
    $this->{bugHost} = @_[1];
    $this->{fileNames} = @_[2];

    if (! $this->{fileNames})
    {
        $this->{fileNames} = new CDSFileNames();
    }

    $this->{cdsHosts} = $this->{fileNames}->CdsHosts();

    if (! $this->{bugHost})
    {
        $this->{bugHost} = $this->{cdsHosts}->BugzillaHost();
    }

    $this->TestDBConnection();

    return $this;
}

sub BugzillaHost
{
    my $this = shift;
    return $this->{bugHost};
}

sub TestDBConnection
{
    my $this = shift;
    $this->{mysql} = $this->{fileNames}->Command("mysql");

    if (! $this->{cdsHosts}->ValidHost($this->{bugHost}))
    {
        $this->{connectionProblem} = "Bugzilla host ".$this->{bugHost}.
            " is not a valid host in this domain.\n";
    }
    elsif (! -x $this->{mysql})
    {
        $this->{commentionProblem} = "No mysql found on host ".
            $this->{cdsHosts}->LocalHostName()."\n".
            "Please run $0 on a host that has 'mysql' installed.\n";
    }
    elsif (scalar @{$this->Databases()} == 0)
    {
        $this->{connectionProblem} = "Could not load database values.\n";
    }
}

sub BugzillaConnectionFailure
{
    my $this = shift;

    return $this->{connectionProblem};
}

sub SummaryDescription
{
    my $this = shift;
    my $bugNo = shift;
    my $query = "select short_desc from bugs where bug_id = $bugNo";
    my @summary = $this->Execute("bugs",$query);

    return @summary;
}

sub SetBugStatus
{
    my $this = shift;
    my $bugNo = shift;
    my $state = shift;
    my $rtn = 0;
    my $query = "update bugs set bug_status = '$state' where bug_id = $bugNo";

    $this->Execute("bugs",$query);

    if ($this->BugStatus($bugNo) eq $state)
    {
        $rtn = 1;
    }

    return $rtn;
}

sub SetResolution
{
    my $this = shift;
    my $bugNo = shift;
    my $state = shift;
    my $rtn = 0;
    my $query = "update bugs set resolution = '$state' where bug_id = $bugNo";

    $this->Execute("bugs",$query);

    if ($this->BugResolution($bugNo) eq $state)
    {
        $rtn = 1;
    }

    return $rtn;
}

sub MarkBugAsFixed
{
    my $this    = shift;
    my $bugNo   = shift;

    my $rtn  = 0;

    if ($this->SetBugStatus($bugNo,"RESOLVED") &&
        $this->SetResolution($bugNo,"FIXED"))
    {
        $rtn = 1;
    }

    return $rtn;
}

sub BugResolution
{
    my $this = shift;
    my $bugNo = shift;
    my $query = "select resolution from bugs where bug_id = $bugNo";

    my @res = $this->Execute("bugs",$query);

    return $res[0];
}

sub BuggedVersion
{
    my $this      = shift;
    my $bugNo     = shift;
    my $query     = "select version from bugs where bug_id = $bugNo";
    my @buggedRel = $this->Execute("bugs",$query);

    return $buggedRel[0];
}

sub AddFixCommittedIn
{
    my $this = shift;
    my $bugNo = shift;
    my $release = shift;
    my $alreadyCommitted = $this->AlreadyCommittedFixInBranch($bugNo, $release);

    if (! $alreadyCommitted)
    {
        my $query = "update bugs set cf_fixed_in = '$release\\n";

        foreach my $rel ($this->FixCommittedIn($bugNo))
        {
            $query .= "$rel\\n";
        }

        $query .= "' where bug_id = $bugNo";
        
        $this->Execute("bugs",$query);

        $alreadyCommitted = $this->AlreadyCommittedFixInBranch($bugNo ,$release);
    }

    return $alreadyCommitted;
}

sub AlreadyCommittedFixInBranch
{
    my $this = shift;
    my $bugNo = shift;
    my $release = shift;
    my $alreadyMarked = 0;
    my @current = $this->FixCommittedIn($bugNo);

    foreach (@current)
    {
        if ($_ =~ /^$release$/)
        {
            $alreadyMarked = 1;
            last;
        }
    }

    return $alreadyMarked;
}

sub FixCommittedIn
{
    my $this = shift;
    my $bugNo = shift;
    my $query = "select cf_fixed_in from bugs where bug_id = $bugNo";
    my @out = $this->Execute("bugs",$query);
    my %dupe = ();
    my @branches = ();
    my @tmp = split /\\n/, "@out";

    for (my $i = 0; $i <= $#tmp; $i++)
    {
        if (! $dupe{$tmp[$i]} && $tmp[$i] ne "NULL")
        {
            push (@branches,"$tmp[$i]");
            $dupe{$tmp[$i]} = 1;
        }
    }

    return @branches;
}

sub AddComment
{
    my $this = shift;
    my $bugNo = shift;
    my $comment = shift;
    my $rtn     = 0;
    my $dateCmd = $this->{fileNames}->Command("date").
        "  '+%Y-%m-%d %H:%M:%S'";
    chomp (my $bugWhen = `$dateCmd`);
    my $who = $this->UserIdByBugNumber($bugNo);
    my @before = $this->Comments($bugNo);

    $comment =~ s/\`|\'|\"//g;
    $comment =~ s/\n/\\n/g;
    my $commentLen = length ($comment);

    my $query = "insert into longdescs (already_wrapped, bug_id, bug_when,".
        "thetext, who) values (1, $bugNo, '$bugWhen', '$comment', $who)";

    $this->Execute("bugs",$query);

    my @after = $this->Comments($bugNo);

    if ($ENV{QA_DEBUG})
    {
        print "Before is $#before and after is $#after\n";
    }

    if ($#after > $#before)
    {
        $rtn = 1;
    }

    return $rtn;
}

sub Comments
{
    my $this = shift;
    my $bugNo = shift;
    my $query = "select thetext from longdescs where bug_id = $bugNo";

    my @comments = $this->Execute("bugs",$query);

    return @comments;
}

sub ValidBugNumber
{
    my $this = shift;
    my $bugNo = shift;
    my $valid = 0;
    
    foreach ($this->ListAllBugs())
    {
        if ($_ == $bugNo)
        {
            $valid = 1;
            last;
        }
    }
    
    return $valid;
}

sub UserIdByBugNumber
{
    my $this = shift;
    my $bugNo = shift;
    my $query = "select assigned_to from bugs where bug_id = $bugNo";
    my @name = $this->Execute("bugs",$query);
    
    return $name[0];
}

sub BugStatus
{
    my $this = shift;
    my $bugNo = shift;
    my $query = "select bug_status from bugs where bug_id = $bugNo";
    
    my @out = $this->Execute("bugs", $query);
    
    return $out[0];
}

sub UserNameByBugNumber
{
    my $this  = shift;
    my $bugNo = shift;
    my $query = "select assigned_to from bugs where bug_id = $bugNo";
    my @name  = $this->Execute("bugs", $query);

    return ($this->UserNameById($name[0]));
}

sub UserNameById
{
    my $this  = shift;
    my $id    = shift;
    my $query = "select login_name from profiles where userid = '$id'";
    my @name  = $this->Execute("bugs", $query);
    
    return $name[0];
}

sub ListAllBugs
{
    my $this  = shift;
    my $db    = "bugs";
    my $query = "select bug_id from bugs";
    my @bugs  = $this->Execute("bugs", $query);
    
    return @bugs;
}

sub UserIdByName
{
    my $this = shift;
    my $name = shift;
    my $query = "select userid from profiles where login_name = '$name'";
    my @no = $this->Execute("bugs", $query);
    
    return $no[0];
}

sub Execute
{
    my $this = shift;
    my $db = shift;
    my $query = shift;
    
    my $execCmd = join "", $this->{fileNames}->Command("echo"),
    " \"use $db; $query\" | ", $this->{mysql},
    " -u cvs -h ", $this->{bugHost};

    my @out = split /\n/, CDSSystem::BackTick($execCmd);
    shift @out;

    return @out;
}

sub Databases
{
    my $this = shift;

    if (! $this->{databases})
    {
        my @databases = ();

        my $showCmd = join "", $this->{fileNames}->Command("echo"),
        " \"show databases;\" | ", $this->{mysql},
        " -u cvs -h ", $this->{bugHost};
        
        foreach (split /\n/, CDSSystem::BackTick($showCmd))
        {
            if ($_ !~ /Databases/)
            {
                push (@databases,$_);
            }
        }

        $this->{databases} = \@databases;
    }

    return $this->{databases};
}


1;
