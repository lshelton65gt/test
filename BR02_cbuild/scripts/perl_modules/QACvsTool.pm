package QACvsTool;

use File::Path;
use Cwd;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CVSTool;
use CDSFileNames;

sub new 
{
    my $this = {};
    bless $this, @_[0];
    $this->{userTag} = @_[1];
    $this->{fileNames} = @_[2];

    if (! $this->{fileNames})
    {
        $this->{fileNames} = new CDSFileNames();
    }

    $this->{cvsTool} = new CVSTool();

    $this->Initialize();

    return $this;
}

sub Initialize
{
    my $this = shift;
    my $script = "";
    ($script = $0) =~ s/^.+\///g;

    $this->{tmpSandbox} = "/tmp/$script.$$";

    if (-d $this->{tmpSandbox})
    {
        rmtree ($this->{tmpSandbox});
    }

    mkpath ($this->{tmpSandbox});

    my %tags = ();
    $this->{allTags} = \%tags;

    $this->ReadTagData();

}

sub CvsTool
{
    my $this = shift;

    return $this->{cvsTool};
}

sub ClassType
{
    my $this = shift;
    return ref($this);
}

sub CVSLogMessageByVersionNumber
{
    my $this = shift;
    my $file = shift;
    my $version = shift;
    my @message = ();

    if ($file =~ /^\//)
    {
        print STDERR ref($this), "::GetLogMessageForVersion():\n",
        "Cannot cvs st $file, full path received\n";
    }
    elsif (! -f $file)
    {
        print STDERR ref($this), "::GetLogMessageForVersion():\n",
        "Cannot find $file to obtain cvs log output.\n";
    }
    else
    {
        # If no specific version, use the current working version.
        if (! $version)
        {
            $version = $this->{cvsTool}->FileWorkingVersion($file);
        }

        my $cvsLogCmd = $this->{fileNames}->Command("cvs");
        $cvsLogCmd    .= " log $file";
        my $delim     = "----------------------------";
        my @output    = split /\n/, CDSSystem::BackTick($cvsLogCmd);

        for (my $i = 0; $i < $#output; $i++)
        {
            if ($output[$i] =~ /^revision $version$/)
            {
                $i+=2;

                while ($output[$i] !~ /^$delim/ &&
                       $output[$i+1] !~ /^revision /)
                {
                    push (@message, "$output[$i]\n");
                    $i++;
                }
            }
        }
    }

    return @message;
}

sub BranchChanged
{
    my $this = shift;
    my $here = cwd();

    if ($this->{userTag})
    {
        my $currTag        = $this->CurrentVersionNumber();
        my $lastTaggedNum  = $this->LastTaggedVersion();
        my $lastTaggedName = $this->LastTaggedName();
        
        print "Current UtVersion is tagged with $currTag\n",
        "File was last tagged when version number was $lastTaggedNum\n",
        "File last tag was $lastTaggedName\n";
    }
    else
    {
        $rtn = 1; # Mainline, force that it's changed.
    }

    return $rtn;
}

sub LastTaggedVersion
{
    my $this = shift;

    return $this->{lastTaggedVersion};
}

sub LastTaggedName
{
    my $this = shift;

    return $this->{lastTaggedName};
}

sub ReadTagData
{
    my $this = shift;
    my $currVerNum = $this->CurrentVersionNumber();
    my $lastVerTagged = "";
    my ($f1, $f2) = (split /\./, $currVerNum)[0,1];
    my $baseVer   = "$f1.$f2";
    my $here  = cwd();
    
    if ($this->CheckoutUtFile($this->{userTag}))
    {
        if (CDSSystem::Cd($this->{tmpSandbox}), $this->ClassType())
        {
            my $cmd = join " ", 
            $this->{fileNames}->Command("cvs"),"log", 
            $this->{fileNames}->UtVersionSandboxPath(),"|", 
            $this->{fileNames}->Command("grep"), "'$baseVer' |",
            $this->{fileNames}->Command("egrep"),"-v 'BASE|revision|branches'|",
            $this->{fileNames}->Command("sort"), " +1";

            
            my @data  = split /\n/, (CDSSystem::BackTick($cmd));

            for (my $i = 0; $i <= $#data; $i++)
            {
                $data[$i] =~ s/\s//g;
                $data[$i] =~ s/updatingversionstringsfor//;
                
                my ($tag, $ver) = (split ":",$data[$i]);
                
                if ($tag ne $this->{userTag})
                {
                    $this->{allTags}{"$ver"} = $tag;
                    
                    if ($i == $#data)
                    {
                        $this->{lastTaggedName} = $tag;
                        $this->{lastTaggedVersion} = $ver;
                    }
                }
            }
            
            CDSSystem::Cd($here, $this->ClassType());
        }
    }
}

sub CheckoutInfraFile
{
    my $this = shift;
    my $infraFile =  $this->{fileNames}->InfraVersionSandboxPath();
    $rtn = $this->CheckoutVersionFile ($infraFile);

    if ($rtn == 0)
    {
        die $this->ClassType(), "::CheckoutInfraFile() failed. Return -> $rtn\n";
    }

    return $rtn;
}

sub CheckoutUtFile
{
    my $this = shift;
    my $utFile =  $this->{fileNames}->UtVersionSandboxPath();
    $rtn = $this->CheckoutVersionFile ($utFile);

    if ($rtn == 0)
    {
        die $this->ClassType(), "::CheckoutUtFile() failed.  Return -> $rtn\n";
    }

    return $rtn;
}

sub CheckoutVersionFile
{
    my $this = shift;
    my $verFile = shift;
    my $rtn = 0;
    my $here = cwd();

    if (CDSSystem::Cd($this->{tmpSandbox}), $this->ClassType())
    {
        if (! -f $verFile)
        {
            $rtn = $this->{cvsTool}->CoFile($verFile, $this->{userTag});

            if ($rtn != 0)
            {
                print STDERR "Checkout of $verFile gave bad status $rtn\n";
            }
        }

        if (-f $verFile)
        {
            $rtn = 1;
            
            if ($ENV{QA_DEBUG})
            {
                print "Version file $verFile exists\n";
            }
        }

        CDSSystem::Cd($here, $this->ClassType());
    }

    return $rtn;
}

sub CurrentVersionNumber
{
    my $this = shift;
    my $here = cwd();

    if (! $this->{currentVersionTag})
    {
        if ($this->CheckoutUtFile($this->{userTag}))
        {
            if (CDSSystem::Cd($this->{tmpSandbox}))
            {
                $this->{currentVersionTag} = 
                    $this->{cvsTool}->FileWorkingVersion 
                    ($this->{fileNames}->UtVersionSandboxPath());

                CDSSystem::Cd($here);
            }
        }
    }

    return $this->{currentVersionTag};
}

sub TempSandboxPath
{
    my $this = shift;
    return $this->{tmpSandbox};
}


sub CvsTool
{
    my $this = shift;
    return $this->{cvsTool};
}

sub CurrentCheckoutDate
{
    my $this = shift;
    my $here = cwd();

    if (! $this->{coDate})
    {
        if ($this->CheckoutUtFile() &&
            $this->CheckoutInfraFile())
        {
            if (CDSSystem::Cd($this->{tmpSandbox}))
            {
                my $chTmp = $ENV{CARBON_HOME};
                $ENV{CARBON_HOME} = $this->{tmpSandbox};
                $this->{coDate} = $this->{cvsTool}->SandboxCheckoutDate();
                $ENV{CARBON_HOME} = $chTmp;

                CDSSystem::Cd($here);
            }
        }
    }    

    return $this->{coDate};
}

sub GenerateChangeReport
{
    my $this = shift;
    my $outputFile = shift;
    my @report = ();
    my $showBugHttp = $this->{fileNames}->BugzillaShowBugCgi();

    if ($this->CheckoutUtFile($this->{userTag}))
    {
        if (CDSSystem::Cd($this->{tmpSandbox}))
        {
            my $cmd = join "",$this->{fileNames}->Command("cvs"),
            " log -r", $this->LastTaggedVersion(),":",$this->CurrentVersionNumber(),
            " -N ", $this->{fileNames}->UtVersionSandboxPath();

            foreach my $line ((split /\n/, CDSSystem::BackTick($cmd)))
            {
                push (@report,"$line\n");

                if ($line =~ /Bugs Affected\:/)
                {
                    $line =~ s/Bugs Affected\: //;
                    chomp ($line);
                    my @bugs = split /\s|,/, $line;

                    foreach my $bug (@bugs)
                    {
                        push (@report, "  $showBugHttp$bug\n");
                    }
                }
            }
        }
    }

    if (@report && $outputFile)
    {
        my $wh = CDSSystem::OpenWrite($outputFile);

        print $wh @report;

        write "$outputFile";
        
        close ($wh);

        @report = ($outputFile);
        
    }

    return @report;
}

1;
