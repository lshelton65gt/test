#======================================================================
# Class PidInfo
# Author Dave Allaby. dallaby@carbondesignsystems.com
#======================================================================
#
# A container type class for process information with methods which
# insert it's own data from the /proc/$$ directory.
# 
# It also has the ability to dig out any spawned children and if
# required, terminate them without remorse, and finally itself so the 
# tax payers don't have to pay for a trial...which is considerate I 
# think.
#
#======================================================================

package PidInfo;

use strict;

use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSSystem;
use CmdPaths;
use CDSHosts;


# Levels of kill to try
#---------------------------
my @level = ("-9","-15");

# new() 
#--------------------------------------------------------
# Arguments:
#
# 1. Pid number to become.
# 2. Job.  A string to alias this task, if need.
#    Often a scriptname is not sufficient to distinguish
#    what the overall process is supposed to be doing.
#    (Optional.)
# 3. A CmdPaths class for commands. (Optional.)
#--------------------------------------------------------
sub new
{
    my $this          = {};
    bless $this, @_[0];
    $this->{pid}      = @_[1];
    $this->{job}      = @_[2];
    $this->{cmdPaths} = @_[3];
    $this->{uname}    = @_[4];
    $this->{pidDir}   = "/proc/@_[1]";
    $this->{user}     = $ENV{USER};

    $this->{startTime} = time();

    if (! $this->{cmdPaths})
    {
	$this->{cmdPaths} = new CmdPaths();
    }
    
    if (! $this->{job})
    {
	$this->{job} = "job.".$this->{pid};
    }

    my %children = ();
    $this->{children} = \%children;

    $this->{numChildren} = 0;
    $this->ReadInfo();

    return $this;
}

sub RunTime
{
    my $this = shift;
    my $now = time();
    my $running = $now - $this->{startTime};

    return $running;
}
#-----------------------------------------------------------
# Returns user defined task name that is associated with
# pid.
#-----------------------------------------------------------
sub JobAlias
{
    my $this = shift;
    return $this->{job};
}

sub Uname
{
    my $this = shift;

    if (! $this->{uname})
    {
        my $cmd = $this->{cmdPaths}->Command("uname");
        $this->{uname} = CDSSystem::BackTick($cmd);
        chomp ($this->{uname});
    }

    return $this->{uname};
}

#------------------------------------------------------------
# This is a method wrapper of sorts.  It turns out reading
# a /proc file on linux is the same as reading a text file,
# but on solaris it's harder, they are binary contents.
# Solaris has a perl module that will read it, but linux does
# not.  So we have to diverge here in the reading of the /proc
# directory.  I no like, but no choice.  Reading ps output
# is too unreliable to use, as it's format can be slightly 
# screwed up.
#------------------------------------------------------------
sub ReadInfo
{
    my $this      = shift;    

    if ($this->Uname() eq "Linux")
    {
        $this->ReadLinux();
    }
    else
    {
        $this->ReadSunOS();
    }
}

#------------------------------------------------------------
#  This method will extract the fields we care about from 
# a Solaris::Procfs::Process class.  
#------------------------------------------------------------
sub ReadSunOS
{
    my $this = shift;

    my %keyTranslator = ();
    $keyTranslator{pr_ppid} = "PPid";
    $keyTranslator{pr_psargs} = "Name";
    $keyTranslator{pr_uid} = "Uid";
    $keyTranslator{pr_gid} = "Gid";

    my @runState = qw (sleeping running zombie threadstopped intermediatestate 
                       activelyrunning);


# Need require on these 2 solaris things.  "require" loads at runtime.
# "use" loads at compile time.  So, we'll hit this although we're running
# on something non-solaris, if it's not "require".
#---------------------------------------------------------------------
    use lib "/tools/solaris/perl/lib/site_perl/5.8.5/sun4-solaris";
    require Solaris::Procfs;
    require Solaris::Procfs::Process;

    my %data      = ();
    $this->{data} = \%data;
    
    if ($this->Active())
    {   
        my $proc = new Solaris::Procfs::Process $this->{pid};
        my $psinfoStruct = $proc->psinfo();

        foreach my $f1 (keys %{$psinfoStruct})
        {
            if ($f1 eq "pr_lwp")
            {
                foreach my $f2 (keys %{$psinfoStruct->{$f1}})
                {
                    if ($f2 eq "pr_start")
                    {
                        foreach (keys %{$psinfoStruct->{$f1}{$f2}})
                        {
                            $this->{data}{status}{State} = 
                                $runState[$psinfoStruct->{$f1}{$f2}{$_}];
                        }
                    }
                }
                                          
            }
            else
            {
                if ($keyTranslator{$f1})
                {
                    $this->{data}{status}{$keyTranslator{$f1}} = 
                        $psinfoStruct->{$f1};
                }
            }
        }
    }
    else # Not on the system, wipe the data for it.
    {
	if (values %{$this->{data}} > 0)
	{
	    delete %$this->{data}
	}
    }
}

#--------------------------------------------------------------
# Parse out hte fields from /proc/$$/status file.  Doing this
# ourselves, cause linux /proc files are text readable.  There
# is no need for a process file reader modules like what is 
# needed on solaris.
#--------------------------------------------------------------
sub ReadLinux
{
    my $this      = shift;
    my %data      = ();
    $this->{data} = \%data;
    my @files     = ("status");

    # There's the chance that what we were passed is dead and
    # gone.

    if ($this->Active())
    {
	foreach my $file (@files)
	{
	    chomp ($file);
	    my $dataFile = join "/", $this->{pidDir}, $file;

	    if (-f $dataFile && -r $dataFile)
	    {
		my $rh = CDSSystem::OpenRead ($dataFile);
                
		while (<$rh>)
		{
		    chomp;

		    $_ =~ s/\:\s+/\:/;
		    my ($t,$val) = (split ":");

		    $this->{data}{$file}{$t} = $val;
		}

		close ($rh);
	    }
	}
    }
    else # Not on the system, wipe the data for it.
    {
	if (values %{$this->{data}} > 0)
	{
	    delete %$this->{data}
	}
    }
}

#------------------------------------------------------------
# Return my pid number
#------------------------------------------------------------
sub Num
{
    my $this = shift;
    return $this->{pid};
}

#------------------------------------------------------------
# Return the parent pid number, of my pid number
#------------------------------------------------------------
sub ParentPid
{
    my $this = shift;
    return $this->{data}{status}{PPid};
}

#------------------------------------------------------------
# Executable/script/process that the system assigned to
# this pid. (Not always great.)
#------------------------------------------------------------
sub ProcessName
{
    my $this = shift;
    return $this->{data}{status}{Name};
}

#------------------------------------------------------------
# Run state of the process. Sleeping, running, zombie, etc.
# 
#------------------------------------------------------------
sub RunState
{
    my $this = shift;
    $this->ReadInfo();

    return $this->{data}{status}{State};
}

#------------------------------------------------------------
# Returns true if this pid is a zombie/defunct
#------------------------------------------------------------
sub Zombie
{
    my $this = shift;
    my $defunct = 0;

    if ($this->RunState() =~ /zombie/)
    {
	$defunct = 1;
    }

    return $defunct;
}

#------------------------------------------------------------
# True if the pid is still on the system, regardless of state.
# You'll have to check state in addition.  Active could also
# be a defunct process.  It is still is using a pid.
#------------------------------------------------------------
sub Active
{
    my $this   = shift;
    my $active = 1;
    my $statusFile = $this->{pidDir} . "/status";

    if (! -f $statusFile)
    {
	$active = 0;
    }

    return $active;
}

#----------------------------------------------------------
# How many kids do we have.  Things start, things end. You
# might get various numbers during the course of things.
# What we care about is zero when it comes to killing the
# family.
#----------------------------------------------------------
sub NumChildren
{
    my $this = shift;
    $this->Children();

    return $this->{numChildren};
}


#----------------------------------------------------------
# Update info with the "latest" before returning group.
#----------------------------------------------------------
sub Children
{
    my $this = shift;
    my %activeChildren = ();
    $this->{numChildren}  = 0;
    $this->MapChildren();

    foreach (keys %{$this->{children}})
    {
        if ($this->{children}{$_}->Active() && 
            ! $this->{children}{$_}->Zombie())
        {
            $this->{numChildren}++;
            $activeChildren{$_} = $this->{children}{$_};
        }
    }

    return \%activeChildren;
}

#----------------------------------------------------------
#
#
#----------------------------------------------------------
sub MapChildren
{
    my $this = shift;

    my @foundPidDirs = CDSSystem::OpenDir("/proc");

    foreach (@foundPidDirs)
    {
	chomp;
	my $dir = "/proc/$_";
	
	# If we own it, it's not us or one of the children
	# we already know about...

	if (-o $dir && $_ != $this->{pid} && ! $this->{children}{$_} && $_  =~ /^\d+$/)
        {
            my $pid = new PidInfo($_);
            
# We want to call them ours if...
#-----------------------------------------------------------
# 1. It's parent is me.
# 2. If we have a child that is it's parent, so it's some form of
#    grandchild to us.
# 3. We'll ignore "sh" processes, cause checking for children
#    causes this child.   (Heisenberg principle.)
# 4. If it's a child that's a zombie, it's fate is already known, we'll
#    never have to kill this or report on it, so we'll ignore it.
#-----------------------------------------------------------
            if (($pid->ParentPid() == $this->{pid} ||
                 $this->{children}{$pid->ParentPid()}) &&
                $pid->ProcessName() ne "sh" &&
                ! $pid->Zombie())
            {
                $this->{children}{$pid->Num} = $pid;
            }
	}
    }
}

#---------------------------------------------------------
# Kill everything we spawned on request, without remorse.
# Then off ourselves.
#---------------------------------------------------------
sub KillingSpree
{
    my $this = shift;
    my $i = $#level;
    my $kill = $this->{cmdPaths}->Command("kill");
    my $killed = 0;
    my $numKids = $this->NumChildren();

    while ($numKids > 0)
    {
	foreach (keys %{$this->{children}})
	{
	    my $pid = $this->{children}{$_};

	    if ($pid->Active() && ! $pid->Zombie())
	    {
		while ($i >= 0 && $pid->Active())
		{
		    my $cmd = join "", "$kill $level[$i] ",
                    $pid->Num();

		    CDSSystem::System ($cmd);
		    $i--;
		}

		$i = $#level;
	    }

	    delete $this->{children}{$pid->Num()};
	}

	$numKids = $this->NumChildren();
    }

    $killed = $this->KillMe();

    return $killed;
}

#-----------------------------------------------------
# Hari Kari method.
#-----------------------------------------------------
sub KillMe
{
    my $this = shift;
    my $i = $#level;
    my $kill = $this->{cmdPaths}->Command("kill");
    my $killed = 0;

    while ($i >= 0 && $this->Active())
    {
	my $cmd = "$kill $level[$i] ".$this->Num();
	CDSSystem::System("$cmd");
	$i--;
    }

    if (! $this->Active())
    {
	$killed = 1;
    }

    return $killed;
}

sub Pr
{
    my $this = shift;

    print $this->{pid},": Process Name: ", $this->ProcessName(), "\n",
    $this->{pid},": Alias Name: ", $this->JobAlias(), "\n",
    $this->{pid},": RunState: ", $this->RunState(), "\n",
    $this->{pid},": Parent Pid: ", $this->ParentPid(),"\n",
    $this->{pid},": Spawned children: ", $this->NumChildren(),"\n";
}

1;
