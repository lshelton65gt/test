#===============================================================
# Class ComMsgReader;
# Author Dave Allaby. dallaby@carbondesignsystems.com
#
#!! This class reads a commit message file, which was written
#!! initially by a ComMsgWriter object.
#
#!! It has methods which return the content in various ways.
#!!
#!! This class must be new'() for methods to work properly.
#==============================================================

package ComMsgReader;

use strict;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSSystem;

#--------------------------------------------------------------
#!! Constructor.
#!! 1. Take a filename location,
#!! 2. A delimiter string, which represents the comment section
#!!  from the file listing section.
#!! 3. A hash reference to required fields within the file.
#!! 4. A CDSFileNames object.
#--------------------------------------------------------------
sub new #(string, string, hash ref, CDSFileNames)
{
    my $this = {};
    bless $this, @_[0];
    $this->{comMsgFile}         = @_[1];
    $this->{delim}              = @_[2];
    $this->{neededFieldHashRef} = @_[3];
    $this->{fileNames}          = @_[4];
    $this->{filter} = "";

# Without this->{delim} string, we can't distinguish 
# between files to commitand comments.
    if (! $this->{delim})
    {
        die "No delimiter passed to ", $this, "\n";
    }

# Setup match filter for when we read the file.
    foreach (keys %{$this->{neededFieldHashRef}})
    {
        $this->{filter} .= $this->{neededFieldHashRef}{$_}."|";
    }

    $this->{filter} =~ s/\|$//;

# Read the file.
    $this->Read($this->{comMsgFile});

    return $this;
}

sub Initialize
{
    my $this = shift;

    my @modified = ();
    $this->{modified} = \@modified;

    my @newFiles = ();
    $this->{newfiles} = \@newFiles;

    my @conflicts = ();
    $this->{conflicts} = \@conflicts;

    my @userData = ();
    $this->{userData} = \@userData;

    my %fieldsData = ();
    $this->{key} = \%fieldsData;

    my %missingContent = ();
    $this->{missingContent} = \%missingContent;

    my @zeroLength = ();
    $this->{zeroLength} = \@zeroLength;

    my %modCode = ();
    $this->{modCode} = \%modCode;

    $this->{versionFile} = $this->{fileNames}->InfraVersionSandboxPath();
}

#-------------------------------------------------------------
#!! We use 2 files to store versions. The version file name
#!! depends on the file list read.  This method returns the
#!! appropriate version file string.
#-------------------------------------------------------------
sub VersionFile #()
{
    my $this = shift;
    return $this->{versionFile};
}

#-------------------------------------------------------------
#!! Returns a list of zero length, modified files.
#-------------------------------------------------------------
sub ZeroLengthFiles #()
{
    my $this = shift;
    return $this->{zeroLength};
}

#-------------------------------------------------------------
#!! Returns a list of modified files.
#-------------------------------------------------------------
sub ModifiedFiles #()
{
    my $this = shift;
    return $this->{modified};
}

#-------------------------------------------------------------
#!! Returns a list of new, non-cvs added files.
#-------------------------------------------------------------
sub NewFiles #()
{
    my $this = shift;
    return $this->{newfiles};
}

#-------------------------------------------------------------
#!! Returns a list of files with file conflicts.
#-------------------------------------------------------------
sub ConflictFiles #()
{
    my $this = shift;
    return $this->{conflicts};
}

#-------------------------------------------------------------
#!! Returns the file's delimiter string.
#-------------------------------------------------------------
sub DelimiterString #()
{
    my $this = shift;
    return $this->{delim};
}

#-------------------------------------------------------------
#!! Returns the last file location read.
#-------------------------------------------------------------
sub ComMsgFile #()
{
    my $this = shift;
    return $this->{comMsgFile};
}

#-------------------------------------------------------------
#!! Returns the cvs letter code associated with a cvs update.
#-------------------------------------------------------------
sub ModifiedCodeChar #(string file)
{
    my $this = shift;
    my $file = shift;

    return $this->{modCode}{$file};
}

#------------------------------------------------------------
#!! Reads a commit message file.
#!! Initializes the member data so this can be called and
#!! recalled with other files if needed.
#------------------------------------------------------------
sub Read #()
{
    my $this = shift;
    $this->{comMsgFile} = shift;

    $this->Initialize();

    if (-f $this->{comMsgFile})
    {
        if ($ENV{QA_DEBUG})
        {
            print "Reading ", $this->{comMsgFile},"\n";
        }

        my $rh = CDSSystem::OpenRead($this->{comMsgFile});
        my $userSection = 1;        

        while (<$rh>)
        {
            #$userSection will change when we hit the delimiter string.
            if ($userSection == 1)
            {
                if ($_ !~ /^$this->{filter}/ && 
                    $_ !~ /^$this->{delim}/ && $_ !~ /^$/)
                {
                    push (@{$this->{userData}},$_);
                }
                else
                {
                    # this picks apart the required field's data.
                    $this->EvalField ($_);
                }
                
                # Flip us to userSection = 0, which means everything
                # from here on out is the cvs update file list.

                if ($_ =~ /$this->{delim}/)
                {
                    $userSection = 0;

                    if ($ENV{QA_DEBUG})
                    {
                        print "Reading filelist section\n";
                    }
                }
            }
            else
            {
                # Now we are reading the cvs update output.

                if ($_ !~ /^$/)
                {
                    my ($code, $file) = (split);
                    
                    if ($code =~ /M|A|R|C|P|\?/)
                    {
                        # record the code for this file
                        $this->{modCode}{$file} = $code;
                        
                        # if the file is in src, or in scripts/ and ends
                        # with _env, then it is a UtVersion.cxx change.
                        if ($file =~ /^src\// ||
                            $file =~ /^scripts\/.+_env/)
                        {
                            $this->{versionFile} = 
                                $this->{fileNames}->UtVersionSandboxPath();
                        }
                        
                        if (-z "$file")
                        {
                            push (@{$this->{zeroLength}}, $file);
                        }

                        if ($code =~ /M|A|R|P/)
                        {
                            push (@{$this->{modified}},$file);
                        }
                        elsif ($code eq "C")
                        {
                            push (@{$this->{conflicts}}, $file);
                        }
                        elsif ($code eq "?")
                        {
                            push (@{$this->{newfiles}},$file);
                        }
                    }
                }
            }
        }
        
        close ($rh);
    }
}

#--------------------------------------------------------
#!! This method return 1 if valid commit message.
#!! At this point, the reader simply says that valid is 
#!! a commit message that has files to commit, a comment,
#!! and the required fields that were passed in by the
#!! hash ref in the constructor.
#!! There is no validation of actual data.  We'll leave
#!! that to the class that is using this class.
#--------------------------------------------------------
sub ValidCommitMessage #()
{
    my $this         = shift;
    my $commentLines = scalar (@{$this->{userData}});
    my $fileListCnt  = scalar (@{$this->{modified}});
    my $valid        = 0;

    # We need comments some files committed, and
    # we need to account for every manditory field has
    # data.

    if ($commentLines == 0)
    {
        $this->{missingContent}{"Commit comments"} = "User comments.";
    }

    if ($fileListCnt == 0)
    {
        $this->{missingContent}{"Modified file list"} = "Modified file list.";
    }

    foreach my $k (keys %{$this->{key}})
    {
        if (! $this->FieldData($k))
        {
            $this->{missingContent}{"$k"} = "Valid $k data field.";
        }
    }

    if (values (%{$this->{missingContent}}) == 0)
    {
        $valid = 1;
    }

    return $valid;
}

#-----------------------------------------------------------
#!! Returns any missing content information.
#-----------------------------------------------------------
sub MissingContentInfo #()
{
    my $this = shift;
    return $this->{missingContent};
}

#-----------------------------------------------------------
#!! Returns the string of bugs entered by the user.
#-----------------------------------------------------------
sub Bugs #()
{
    my $this = shift;
    my $bugs = 
        $this->FieldData($this->{neededFieldHashRef}{bugs_affected});

    return $bugs;
}

#-----------------------------------------------------------
#!! Returns an array reference to any reviewers.  Does no
#!! actual validation to see if the users are real users on
#!! this network.
#-----------------------------------------------------------
sub Reviewers #()
{
    my $this          = shift;
    my @reviewers     = split " ",
        $this->FieldData($this->{neededFieldHashRef}{reviewed_by});
    my $relNoteReview = 
        $this->FieldData ($this->{neededFieldHashRef}{rel_notes_reviewed_by});

    if ($relNoteReview)
    {
        push (@reviewers,$relNoteReview);
    }

    return \@reviewers;
}

#-----------------------------------------------------
#!! Method parses a line that matches a needed field
#!! and extracts the data entered.
##-----------------------------------------------------
sub EvalField #(string line)
{
    my $this = shift;
    my $line = shift;

    if (values (%{$this->{neededFieldHashRef}}) == 0)
    {
        die "No required fields? I don't think so.\n";
    }

    foreach my $key (keys %{$this->{neededFieldHashRef}})
    {
        if ($line =~ /^$this->{neededFieldHashRef}{$key}/)
        {
            my $value = "";            
            my $subString = $this->{neededFieldHashRef}{$key};
            ($value = $line) =~ s/$subString//;
            $value  =~ s/,/ /g;
            $value  =~ s/\s+/ /g;
            $value  =~ s/^\s+//;
            $value  =~ s/\s+$//;

            $this->{key}{$this->{neededFieldHashRef}{$key}} = $value;
        }
    }
}           

#------------------------------------------------------
#!! Returns array reference which points to the user
#!! comments.
#------------------------------------------------------
sub UserComments #()
{
    my $this = shift;
    return $this->{userData};
}

#------------------------------------------------------
# Returns a string value for a hash key. Hash key is
# the string for a required field, such as "Bugs Affected".
#------------------------------------------------------
sub FieldData #(string fieldname)
{
    my $this = shift;
    my $fieldName = shift;

    return $this->{key}{$fieldName};
}

1;
