#----------------------------------------------------------
# CmdPaths class.
# Author Dave Allaby. dallaby@carbondesignsystems.com
#
# Description:  An extension class to CDSFileNames.
# A class to assign preferred command paths.  Using this
# class makes behavior more reliable as we do not
# depend on the user's PATH.
#----------------------------------------------------------

package CmdPaths;

use strict;

sub new
{
    my $this = {};
    my $os = @_[1];
    bless $this, @_[0];

    if (! $os)
    {
	chomp ($os = `/bin/uname`);
    }

    $this->{hostos} = $os;
    $this->SetCmdPaths();

    return $this;
}

sub SetCmdPaths
{
    my $this = shift;

    $this->{cmd}{gzip}     = "/bin/gzip";
    $this->{cmd}{hostname} = "/bin/hostname";
    $this->{cmd}{tar}      = "/bin/tar"; 
    $this->{cmd}{uname}    = "/bin/uname";

    if ($this->{hostos} eq "Linux")
    {
	my $sgeBin             = "$ENV{SGE_ROOT}/bin/glinux";
	$this->{cmd}{cvs}      = "/usr/bin/cvs";
	$this->{cmd}{gmake}    = "/tools/linux/bin/gmake-3.80";
	$this->{cmd}{grep}     = "/bin/grep";
        $this->{cmd}{mail}     = "/bin/mail";
        $this->{cmd}{mysql}    = "/usr/bin/mysql";
	$this->{cmd}{nslookup} = "/usr/sbin/nslookup";
	$this->{cmd}{perl}     = "/tools/linux/perl/bin/perl5.8.5";
	$this->{cmd}{ps}       = "/bin/ps";
	$this->{cmd}{qhost}    = "$sgeBin/qhost";
	$this->{cmd}{qrsh}     = "$sgeBin/qrsh";
	$this->{cmd}{qselect}  = "$sgeBin/qselect";
	$this->{cmd}{qstat}    = "$sgeBin/qstat";
        $this->{cmd}{sendmail} = "/usr/sbin/sendmail";
    }
    else
    {
	my $sgeBin             = "$ENV{SGE_ROOT}/bin/solaris64";
	$this->{cmd}{cvs}      = "/usr/local/bin/cvs";
	$this->{cmd}{gmake}    = "/tools/solaris/bin/gmake-3.80";
	$this->{cmd}{grep}     = "/usr/bin/grep";
        $this->{cmd}{mail}     = "/bin/mail";
	$this->{cmd}{nslookup} = "/usr/sbin/nslookup";
	$this->{cmd}{perl}     = "/tools/solaris/perl/bin/perl5.8.5";
	$this->{cmd}{ps}       = "/usr/ucb/ps";
	$this->{cmd}{qhost}    = "$sgeBin/qhost";
	$this->{cmd}{qrsh}     = "$sgeBin/qrsh";
	$this->{cmd}{qselect}  = "$sgeBin/qselect";
	$this->{cmd}{qstat}    = "$sgeBin/qstat";
        $this->{cmd}{sendmail} = "/usr/lib/sendmail";
    }
}

sub Command
{
    my $this = @_[0];
    my $cmd  = @_[1];
    my $rCmd = "";

    if ($this->{cmd}{$cmd})
    {
	$rCmd = $this->{cmd}{$cmd};
    }
    elsif (-x "/usr/ucb/$cmd")  # Only solaris path, but has
    {                           # more linux-behaving commands.  
	$rCmd = "/bin/ucb/$cmd";
    }
    elsif (-x "/bin/$cmd")
    {
	$rCmd = "/bin/$cmd";
    }
    elsif (-x "/usr/bin/$cmd")
    {
	$rCmd = "/usr/bin/$cmd";
    }
    elsif (-x "/usr/local/bin/$cmd")
    {
	$rCmd = "/usr/local/bin/$cmd";
    }
    else
    {
	# Maybe not cool.  Different stuff based on $PATH and $USER.
	my $desperateAttempt = CDSSystem::BackTick("which $cmd");
	chomp ($desperateAttempt);

	if (-x $desperateAttempt)
	{
	    $rCmd = "$desperateAttempt";
	}
    }

    if (! $rCmd)
    {
        chomp (my $host = `/bin/hostname`);
        print STDERR ref($this), 
        "Command $cmd could not be found on $host.";
    }

    return $rCmd;
}

1;
