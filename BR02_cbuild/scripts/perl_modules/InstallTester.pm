#---------------------------------------------------------
# InstallTester class.
# Author Dave Allaby. dallaby@carbondesignsystems.com
#
# Description:  Procedural class.  Contains the all the 
# significant processes of testing an install tree on
# and isolated machine.
#---------------------------------------------------------

package InstallTester;

use strict;
use Cwd;
use File::Path;
use File::Copy;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSSystem;
use CDSFileNames;
use IsolationLic;
use CDSArgs;
use QATarOps;
use CDSMailer;
use CDSInstallJammer;

local $SIG{INT} = sub { &IntCleanAndExit();};
local $SIG{TERM} = sub { &IntCleanAndExit();};

#------------------------------------------
# Contructor
#------------------------------------------
sub new
{
    # Make sure we're new'd()
    if (@_[0] =~ /InstallTester/)
    {
        my $this            = {};
        my $package         = @_[1];
        bless $this, @_[0];
        $this->{retestMode} = 0;    
        
        if (! $package || 
            ($package =~ /carbon\-release.+\.tgz/ || $package =~ /carbon\-install\-/))
        {
            if ($package)
            {
                $this->{nextPackageName} = $package;
                $this->{retestMode}         = 1;
            }
            
            $this->Initialize();
        }
        else
        {
            print STDERR "USAGE: $0 \$tar_package_name ",
            "(optional, and name only, NOT path.)\n";
            
            exit 1;
        }
        
        if ($this->{nextPackageName})
        {
            return $this;
        }
        else
        {
            print STDOUT "Nothing to do for testing. No packages found\n";
            exit 0;
        }
    }
}

#---------------------------------------------------
# Initialize member data.
#---------------------------------------------------
sub Initialize
{
    my $this                     = shift;
    
    $this->{windowsTesting} = $this->WindowsTesting();
# Need 3 other classes.
#----------------------
    $this->{fileNames}    = new CDSFileNames();
    $this->{tarMgr}       = new QATarOps($this->{fileNames}->CdsHosts(),
                                         $this->{fileNames}); 
    $this->{mailer}       = new CDSMailer($this->{fileNames}->CdsHosts());

# license file hash
#-----------------------
    my %licFiles          = ();
    $this->{licFiles}     = \%licFiles;

# results hash
#-----------------------
    my %results              = ();
    $this->{results}         = \%results;
    $this->{results}{PASSED} = 0;
    $this->{results}{FAILED} = 0;
    $this->{results}{ERROR}  = 0;

    $this->{testTarball}     = "";
    $this->{newTarLocation}  = $this->{fileNames}->IsolationPackageRoot();
    $this->{audit}           = join "/", $this->CarbonReleasePath(),
                               $this->{fileNames}->AuditFileName();
    $this->{lockFile}        = $this->{fileNames}->InstallBusyFile();
    $this->{runlistPid}      = "";
}

sub WindowsTesting
{
    my $this = shift;
    my $wintest = 0;

    if ($this->CarbonReleasePath() =~ /winreleasetest/)
    {
        $wintest = 1;
    }

    return $wintest;
}

sub AuditFilePath
{
    my $this = shift;
    return $this->{audit};
}

#--------------------------------------------
# This is a signal hander AND an
# die gracefully method.  There are certain
# clean up things we need to do if this is
# interrupted or conditions lead to a Die().
# We need to kill any forked children, shut
# down any license daemons, and remove the
# lock file so we don't block the next attempt.
#--------------------------------------------
sub IntCleanAndExit
{
    my $this     = @_[0];
    my $exitCode = @_[1];
    my $childPid = $this->{runlistPid};
    my $ps       = $this->{fileNames}->Command("ps");
    my $grep     = $this->{fileNames}->Command("grep");
    my $awk      = $this->{fileNames}->Command("awk");

    my $getpidsCmd = "$ps auwx | $grep runlist | $grep -v grep ".
        "| $awk '{print \$2}'";

    if (! $exitCode)
    {
        $exitCode = 2;
    }
    
    my (@runlistPids) = (split /\n/,CDSSystem::BackTick($getpidsCmd));
    
    if (@runlistPids)
    {
        $this->Log("kill @runlistPids");
        CDSSystem::System("kill @runlistPids");
    }

    if ($childPid > 0)
    {
        $this->Log("SigInt: Killing $childPid");
        CDSSystem::System("kill -9 $childPid");
    }

    &IsolationLic::WipeoutAllRunningLicenseDaemons();
    $this->ClearLockFile();

    exit $exitCode;
}

#---------------------------------------------------
# Returns our CDSmailer object for use by the calling
# entity.
#---------------------------------------------------
sub CDSMailer
{
    my $this = shift;
    return $this->{mailer};
}

#---------------------------------------------------
# Performs steps that are needed to test and which
# would usually be performed in the unpackaging
# process.  When we retest, we do not do the unpackage
# process, so this will configure us based on the last
# attempt.
#---------------------------------------------------
sub Configure
{
    my $this = shift;
    my $rtn  = 2;

    if ($this->SetLockFile())
    {
        $this->SetupEnvVars($this->CarbonReleasePath());
        $rtn = 0;
    }

    return $rtn;
}

#-------------------------------------------------
# Unpacks the 2 tar packages for preparation for testing
# there is also some environment variables that get
# set using the returns from some of the methods in this
# class.
#-------------------------------------------------
sub PrepTree
{
    my $this = shift;
    my $rtn = 2;

    if ($this->SetLockFile())
    {
        my $tarball = $this->NextPackageName();
        
	if ($tarball)
	{
	    $this->Log("Next tar package located at $tarball");

	    my $cbHome = "";
	
            if ($tarball =~ /\.tgz$/)
            {
                $cbHome = $this->UnpackInstallTree();
            }
            elsif ($tarball =~ /carbon\-install\-/)
            {
                $cbHome = $this->RunCarbonInstaller();
            }

	    if (-e "$cbHome")
	    {
		$this->Log("Successfully unpacked $tarball into $cbHome");
		
		$this->SetupEnvVars($cbHome);
            
		my $testPackage = $this->TestTarballPath();
		
		$this->Log("Looks like tests tarball is $testPackage");
		
		if ($testPackage  && -e $testPackage)
		{
		    my $testsHome = $this->UnpackTests();
		    
		    if ($testPackage =~ /$testsHome/)
		    {
			$rtn = 0;
		    }
		    
		    $this->Log ("testPackage-> $testPackage and testsHome-> $testsHome");
		    $this->Log ("Return from unpacking test in $testsHome was $rtn");
		}
	    }
	    else
	    {
		$this->Log("Bad \$CARBON_HOME value set-> $cbHome");
	    }
	}
	else
	{
	    $this->Log("No new package to test.  Nothing to do.");
	    $rtn = 2;
	}
    }
    
    return $rtn;
}

sub RunCarbonInstaller
{
    my $this = shift;
    my $ijmr = new CDSInstallJammer;
    my $installer = join "/", $this->{fileNames}->IsolationPackageRoot(), $this->NextPackageName();
    my $cb = "";

    if ($ijmr->InstallToPath ($installer, $this->CarbonReleasePath()))
    {
        $cb = $this->CarbonReleasePath();
    }
                                    
    return $cb;
}

#--------------------------------------------------
# Runs fixate step.
#--------------------------------------------------
sub RunFixate
{
    my $this = shift;
    my $rtn = 2;
    my $here = cwd();
    my $fixateCmd = "./fixate > fixate.out 2>&1";
    
    $this->Log("Running fixate ...");

    if (CDSSystem::Cd($ENV{CARBON_HOME},$this->ClassType()))
    {
        $rtn = CDSSystem::System($fixateCmd);

        if ($rtn == 0)
        {
            $this->Log("Checking fixate output...");
            my $diffCmd = "diff -w scripts/fixate.gold fixate.out ".
		"> fixate.out.diff 2>&1";

            $rtn = CDSSystem::System($diffCmd);
        }

        CDSSystem::Cd($here,$this->ClassType());
    }

    $this->Log("Fixate returned [$rtn]");

    return $rtn;
}

#----------------------------------------------
# Returns which mode we are in.
#----------------------------------------------
sub RetestMode
{
    my $this = shift;
    return $this->{retestMode};
}

#----------------------------------------------
#  Launches runlist as a forked child, hands off
# control to a monitor which watches what it does
# and logs changes to the audit trail.
#----------------------------------------------
sub RunList
{
    my $this          = @_[0];
    my $group         = @_[1];
    my $rundir        = @_[2];
    my $rerun         = @_[3];
    my $rtn           = 2;
    my $here          = cwd();
    my @out           = ();
    my $myRunlistOut  = join "/", $this->CarbonReleasePath(), 
                        $this->{fileNames}->RunlistResultsFileName();
    my $runlist       = "$ENV{CARBON_HOME}/scripts/runlist";
    my $args          = "-overwrite -reltest -nogrid -testgroup $group";
    my $debugMsgs     = $ENV{QA_DEBUG};
    my $runlistFailed = join "/", $this->{fileNames}->TopTestRunDir(), 
    $rundir, $this->{fileNames}->RunlistFailedFileName();

    if (($this->RetestMode() || $rerun ) &&
        -f $runlistFailed)
    {
        $args =~ s/testgroup $group/failrerun/;
    }
    else
    {
        if (! $group)
        {
            $args .= " release";
        }
    }
    
    if ($rundir)
    {
        $args .= " -rundir $rundir";
    }

    my $cmd  = "/bin/bash -c 'source ~/.bashrc; touch $myRunlistOut; ".
        "echo \"\" >> $myRunlistOut; $runlist $args'".
        " >> $myRunlistOut 2>&1";

    $this->Log("Beginning testing of testgroup $group");

    if (CDSSystem::Cd($this->CarbonReleasePath))
    {
        $this->Log("Forking runlist process for testing group $group");
        $this->Log("Runlist output redirected to $myRunlistOut");

        # Have to turn off debug messaging for testing else some diffs will
        # fail.  I'm going to fix this, but now it has to be off.
	#------------------------------------------------------------------
        if ($debugMsgs)
        {
            $this->Log("EXECUTING: $cmd");

            delete $ENV{QA_DEBUG};
        }

        $this->{runlistPid} = CDSSystem::ForkProcess("$cmd", 1);

        # After forking, turn debug messaging back on if it was on.
	#---------------------------------------------------------
        if ($debugMsgs)
        {
            $ENV{QA_DEBUG} = 1;
        }
    }

    $this->Log("Runlist child pid for group $group is $this->{runlistPid}");

    $rtn = $this->MonitorRunlist($this->{runlistPid}, $rundir, $myRunlistOut);

    return $rtn;
}

#-----------------------------------------------------
# Returns the active runlist pid
#-----------------------------------------------------
sub RunListPid
{
    my $this = shift;
    return $this->{runlistPid};
}

#---------------------------------------------------
# Die wrapper for die.  Prints message, does our 
#cleanup stuff, then dies
#---------------------------------------------------
sub Die
{
    my $this = @_[0];
    my $msg = @_[1];

    print STDERR "Die!!! $msg\n";

    $this->CleanIntAndExit(10);
}

#---------------------------------------------------
# Monitor routine which watches the child runlist
# process we just forked.
#---------------------------------------------------
sub MonitorRunlist
{
    my $this    = @_[0];
    my $fPid    = @_[1];
    my $rundir  = @_[2];
    my $outfile = @_[3];
    my $delay   = 300;
    my $procDir = "/proc/$fPid";
    my $rtn     = -1;

    $this->{fileNames}->TopTestRunDir("", $rundir);

    if (! $fPid)
    {
        Die "Can't track child runlist pid\n";
    }
    else
    {
        if (-d $procDir)
        {
            $this->Log("Runlist activity detected.  Begin monitoring mode....");
	    # Change in our runlist output file dictates when we will
	    # print a new result pass/fail status message.

            my $oldSt = 0;
            my $st    = 0;

            while (-d $procDir) # Any pid in a system mkdirs in /proc/$pid
            {                   # When dir is removed, pid is dead.
                my ($st) = (stat($outfile))[9];

                if ($st != $oldSt)  # When a change, update status
                {
                    $this->PrintStatus($outfile);
                    $oldSt = $st;
                }

                sleep 30;
            }

            $rtn = $this->FailedCnt();
        }
        else
        {
            my $msg = "Runlist process does not seem to be running.".
		"Cannot continue.";
            $this->Log($msg);
            Die "$msg\n";
        }
    }

    return $rtn;
}

#-------------------------------------------------
# Gets the passing count test.  You need to call GatherStats()
# first if you want the up-to-date counts.  Normal flow
# does this.
#-------------------------------------------------
sub PassedCnt
{
    my $this = shift;
    my $c = $this->{results}{PASSED}; 
    return $c;
}

#-------------------------------------------------
# Gets the failing test count.  You need to call GatherStats()
# first if you want the up-to-date counts.  Normal flow
# does this.
#-------------------------------------------------
sub FailedCnt
{
    my $this = shift;
    my $f = $this->{results}{FAILED}; 
    return $f;
}

#-------------------------------------------------
# Gets the errored test count.  You need to call GatherStats()
# first if you want the up-to-date counts.  Normal flow
# does this.
#-------------------------------------------------
sub ErroredCnt
{
    my $this = shift;
    my $e = $this->{results}{ERROR}; 
    return $e;
}

#--------------------------------------------------
# Read our cumulative runlist.results file, counts
# pass/fail/errors.  Last status takes precedence in
# the case that a test is mentioned twice.
#--------------------------------------------------
sub GatherStats
{
    my $this = @_[0];
    my $file = @_[1];
    my $rh = CDSSystem::OpenRead($file,$this->ClassType());
    my $rtn = 1;
    $this->{results}{PASSED} = 0;
    $this->{results}{FAILED} = 0;
    $this->{results}{ERROR} = 0;

    if ($rh)
    {
        $rtn = 0;

        while (<$rh>)
        {
            $_ =~ s/\*/ /g;

            if ($_ =~ /PASSED|FAILED|ERROR/)
            {
                my ($test,$result) = (split)[0,2];

                # Strip off gargage touching the return state
                $result =~ s/.+PASSED.+/PASSED/;
                $result =~ s/.+FAILED.+/FAILED/;
                $result =~ s/.+ERROR.+/ERROR/;

                if ($test && $result =~ /ERROR|PASSED|FAILED/)
                {
                    $this->{results}{"$test"} = $result;
                }
            }
        }

        close ($rh);

        foreach my $test (keys %{$this->{results}})
        {
            my $result = $this->{results}{$test};

            if ($result =~ /PASSED|FAILED|ERROR/)
            {
                $this->{results}{$result}++;
            }
        }
    }
    else
    {
        $this->Log("WARNING: unable to parse output file $file");
    }
    
    return $rtn;
}

#----------------------------------------------
# Prints update status to audit log
#----------------------------------------------
sub PrintStatus
{
    my $this  = @_[0];
    my $file  = @_[1];
    my $rtn   = $this->GatherStats($file);

    if ($rtn == 0)
    {
        my $passing = $this->PassedCnt();
        my $failing = $this->FailedCnt();
        my $errored = $this->ErroredCnt();
        
        my $total = $passing + $failing + $errored;
        
        my $status = "\n------------------------------------------------\n".
            "STATUS:   Total tests run so far: $total\n".
            "STATUS:   Total passing:          $passing\n".
            "STATUS:   Total failing:          $failing\n".
            "STATUS:   Total errored:          $errored\n".
            "\n-------------------------------------------------\n";
        
        $this->Log($status);
    }
    else
    {
        print STDERR "Unable to parse the file $file for test status\n";
    }
}

#----------------------------------------------
# Makes calls to the IsolationLic class which
# knows the procedures to operate the license
# daemons on the isolated hosts.
#----------------------------------------------
sub StartupLicensing
{
    my $this    = shift;
    my $rtn     = 2;
    my $carbond = $this->{fileNames}->IsoCarbondLicPath();

    if (-f $carbond)
    {
        unlink ($carbond);
    }

    $this->Log("Creating carbond license $carbond");

    if (&IsolationLic::CreateLicenseFile($carbond))
    {
        my $maxsimPath = $this->{fileNames}->IsoMaxsimLicPath();
        
        $rtn = &IsolationLic::StartLicenseDaemon($maxsimPath);

        $this->Log("Started daemon for $maxsimPath -> [$rtn]");

        $rtn = &IsolationLic::StartLicenseDaemon($carbond);

        $this->Log("Started daemon for $carbond -> [$rtn]");
    }

    if ($rtn == 0)
    {
        sleep 1; # Wait for a tick for daemons to show on ps table.

        if (! &IsolationLic::LicenseDaemonRunning())
        {
            $rtn = 1;
        }
    }

    $this->Log("License startup returned -> [$rtn == 0]");

    return $rtn
}

#------------------------------------------
# Runs the builds systemC phase
#------------------------------------------
sub BuildSystemC
{
    my $this = shift;
    my $rtn  = 2;
    my $here = cwd();
    my $syscUtil = new SystemCUtil($this->{fileNames});

    $this->Log("Running Build SystemC");

    $rtn = $syscUtil->BuildSystemC($this->CarbonReleasePath());

    $ENV{SYSTEMC_HOME} = join "/", $this->CarbonReleasePath(), "systemc";

    $this->Log ("SystemC build returned $rtn");

    return $rtn;
}

#------------------------------------------
# Runs the Examples tests
#------------------------------------------
sub RunExamples
{
    my $this = shift;
    my $rtn  = 0;
    my $here = cwd();
    my $cmd  = "/bin/bash -c 'make clean >& makeclean.uninstall.log'";

# each of these dirs will be chdir'd to and make'd
# unless $? is 0 on each make, this test will fail
    my @examples = ("examples/cmodel",    
                    "examples/twocounter");
    my @commands = ("/bin/bash -c 'make clean >& makeclean.log'",
                    "/bin/bash -c 'make >& make.log'");

    $this->Log("Running examples..");

    foreach my $test (@examples)
    {
        my $cd = join "/", $this->CarbonReleasePath(),$test;
        
        if (CDSSystem::Cd($cd, $this->ClassType()))
        {
            $this->Log("Running $test..");
            
            foreach my $cmd (@commands)
            {
                $rtn = CDSSystem::System($cmd);
                
                if ($rtn != 0)
                {
                    $this->Log("Command $cmd failed in $test");
                }
            }
        }
    }

    CDSSystem::Cd($here, $this->ClassType());

    $this->Log("Running examples returned $rtn");

    return $rtn;
}

#------------------------------------------
# Runs Cbuild help process
#------------------------------------------
sub RunCBuildHelp
{
    my $this = shift;
    my $here = cwd();
    my $rtn  = 2;
    my $cbuildLastHelp = "~/cbuild.help.out";

    $this->Log("Running CBuild Help");

    if (CDSSystem::Cd($this->CarbonReleasePath(), $this->ClassType()))
    {
        my $cmd = join "", "/bin/bash -c 'bin/cbuild >& ", 
        $this->CarbonReleasePath(), "/cbuild.help.out'";
        $rtn = CDSSystem::System($cmd);

        # Send Doc changes to $HOME so cron command will mail
        # it on Mondays.

        if ($this->CarbonReleasePath() =~ /HEAD/)
        {
            my $diffCmd = $this->{fileNames}->Command("diff");
            my $diff = CDSSystem::System 
                ("$diffCmd cbuild.help.out $cbuildLastHelp >& /dev/null");
            
            if ($diff)
            {
                unlink "$cbuildLastHelp";
                rename ("cbuild.help.out", $cbuildLastHelp);
            }
        }
    }

    CDSSystem::Cd($here, $this->ClassType());

    $this->Log("CBuild Help returned -> $rtn");

    return $rtn
}

#---------------------------------------------------------
# Sets up some PATH and other ENV vars needed for testing
#---------------------------------------------------------
sub SetupEnvVars
{
    my $this = @_[0];
    $ENV{CARBON_HOME} = @_[1];

    my $arch = $this->{fileNames}->CdsHosts()->HostUname();

    if ($ENV{PATH} =~ /$ENV{CARBON_HOME}\/bin/)
    {
        $ENV{PATH} = join ":", "$ENV{CARBON_HOME}/bin",$ENV{PATH};
    }

    if ($ENV{LD_LIBRARY_PATH} !~ /$ENV{CARBON_HOME}\/$arch\/gcc\/lib/)
    {
        $ENV{LD_LIBRARY_PATH} = join ":", 
	"$ENV{CARBON_HOME}/$arch/gcc/lib",$ENV{LD_LIBRARY_PATH};
    }

    if (! $this->{licFiles})
    {
        $this->{licFiles}{maxsim} = $this->{fileNames}->IsoMaxsimLicPath();
        $this->{licFiles}{carbond} = "$ENV{CARBON_HOME}/lib/customer.lic";
    }

    foreach (keys %{$this->{licFiles}})
    {
        $ENV{LM_LICENSE_FILE} .= "$this->{licFiles}{$_}:";
    }
}

#-------------------------------------------------------
# We call this before starting any testing. This will insure
# we don;t have any license daemon processes running.
#-------------------------------------------------------
sub ShutdownAnyStaleTestProcesses
{
    my $this = shift;
    my $rtn  = 1;
    my $kill = $this->{fileNames}->Command("kill");
    my $awk = $this->{fileNames}->Command("awk");
    my $grep = $this->{fileNames}->Command("grep");
    my $ps = $this->{fileNames}->Command("ps");
    my $killCmd = join "", $kill, " -15 `", $ps, " auwx |",
    $grep, " carbon-release | ", $grep, " -v ", $$,"|", $awk, 
    " '{print \$2}'`";

    $this->Log("Killing stale carbon-release items ...");

    $rtn = CDSSystem::System($killCmd);

    $this->Log ("Verifying that no license daemons are running on the box");

    CDSSystem::System ("rm -rf ~/.flexlmrc ~/.flexlm.out ~/flexlm/carbond");

    if (&IsolationLic::LicenseDaemonRunning())
    {
        $rtn = &IsolationLic::WipeoutAllRunningLicenseDaemons();

        if ($rtn == 0)
        {
            $this->Log("License daemons have been terminated");
        }
        else
        {
            $this->Log("Problems shutting down license daemons.");
        }
    }
    else
    {
        $this->Log("No license daemons are running");
        $rtn = 0;
    }

    return $rtn;
}

#---------------------------------------------------------
# Returns class name
#---------------------------------------------------------
sub ClassType
{
    my $this = shift;
    return ref ($this);
}

#---------------------------------------------------------
# Returns our filenames class so the calling entity can
# access paths without making a filenames class of their
# own.
#---------------------------------------------------------

sub CdsFileNames
{
    my $this = shift;
    return $this->{fileNames};
}

#----------------------------------------------------------
# Clears the lock file for this script.
#----------------------------------------------------------
sub ClearLockFile
{
    my $this = shift;
    my $removed = 0;

    unlink ($this->{lockFile});

    if (! -f $this->{lockFile})
    {
        $removed = 1;
    }

    return $removed;
}

#----------------------------------------------------------
# Returns true of the lock file for this script exists.
#----------------------------------------------------------
sub LockFile
{
    my $this = shift;
    my $exists = 0;

    if (-f $this->{lockFile})
    {
        $exists = 1;
    }

    return $exists;
}

#----------------------------------------------------------
# Writes the lock file for this script.
#----------------------------------------------------------
sub SetLockFile
{
    my $this = shift;
    my $cmd = join " ", "echo `date` > ", $this->{lockFile};
    my $rtn = &CDSSystem::System($cmd);
    my $lock = $this->{lockFile};


    if (-f $lock && $rtn == 0)
    {
        $this->Log("Set up $lock file");
        $rtn = 1;
    }
    else
    {
        $this->Log("Could not set up lock file $lock");
    }

    return $rtn;
}

#---------------------------------------------------------
# Returns the installation path for the release we are
# testing.
#---------------------------------------------------------
sub CarbonReleasePath
{
    my $this = shift;

    if (! $this->{carbonReleasePath})
    {
        $this->{carbonReleasePath} = join "/", $this->{fileNames}->IsolationPackageRoot(),
        $this->NextPackageName();

        print "DEBUG ", $this->{carbonReleasePath},"\nNext package is ", $this->NextPackageName(),"\n";

        if ($this->{carbonReleasePath} =~ /\.tgz/)
        {
            $this->{carbonReleasePath} =~ s/\.tgz//;
        }
        elsif ($this->{carbonReleasePath} =~ /\-install\-/)
        {
            $this->{carbonReleasePath} =~ s/\-install\-/\-release\-/;
        }
    }

    return $this->{carbonReleasePath};
}

#-----------------------------------------------------------
# Unpacks the test tarball.
#-----------------------------------------------------------
sub UnpackTests
{
    my $this = shift;
    my $here = cwd();
    my $rtn  = "";
    my $testTar = $this->TestTarballPath();
    my $relPath = $this->CarbonReleasePath();

    $this->Log("Unpacking $testTar...");

    $this->Log("Copying $testTar to $relPath");

    copy ($testTar, $relPath);

    if (CDSSystem::Cd($relPath, $this->ClassType()))
    {
        $this->Log("Unpackaging tests to $relPath");

        $rtn = $this->{tarMgr}->Untar($this->TestTarballName());
        
        if ($rtn eq $this->TestTarballName())
        {
            unlink "$rtn";
        }

        CDSSystem::Cd($here, $this->ClassType());
    }

    return $rtn;
}

sub SendExitMail
{
    my $this = shift;
    my $msg = shift;
    my $user = "dallaby";
    my $host = $this->{fileNames}->CdsHosts()->LocalHostName();
    my $subj = "Install Testing Message from $host";

    if (-f $msg)
    {
        $this->{mailer}->MailFile($user, $subj, $msg);
    }
    else
    {
        my $text = "Installation package: ".$this->CarbonReleasePath()."\n\n$msg";

        $this->{mailer}->MailText($user, $subj, $text);
    }
}

#-----------------------------------------------------------
# Unpacks the release tarball.
#-----------------------------------------------------------
sub UnpackInstallTree
{
    my $this = shift;
    my $package = $this->NextPackageName();
    my $rtn = "";
    my $here = cwd();
    my $releaseTree = $this->CarbonReleasePath();
    my $here = cwd();

    if (! -d $releaseTree)
    {
        $this->Log("Initializing $releaseTree area");
        mkpath ($releaseTree);
    }

    $this->Log("Unpacking $package...");
    $this->Log("Copying $package to $releaseTree");
    copy ($package, $releaseTree);

    if (&CDSSystem::Cd($releaseTree))
    {
        $rtn = join "/", $this->{fileNames}->IsolationPackageRoot(),
        $this->{tarMgr}->Untar($package);

        if (-d $rtn)
        {
            $this->Log("Unpackaging tests to $rtn succeeded");
            unlink ($package);
        }

        CDSSystem::Cd($here, $this->ClassType());
    }

    return $rtn;
}

#-----------------------------------------------------------
# Derives the test tarball name using the release package name
#-----------------------------------------------------------

sub TestTarballName
{
    my $this = shift;
    
    if (! $this->{testTarball})
    {
        ($this->{testTarball} = $this->NextPackageName()) =~ 
            s/(install|release)\-((SunOS|Linux|Windows)\-|)/test\-/;

        if ($this->{testTarball} !~ /\.tgz/)
        {
            $this->{testTarball} .= ".tgz";
        }
    }
    
    return $this->{testTarball};
}

#-----------------------------------------------------------
# Returns the path and name of the test tarball
#-----------------------------------------------------------
sub TestTarballPath
{
    my $this = shift;
    
    if (! $this->{testTarPath})
    {
        $this->{testTarPath} = join "/", $this->{fileNames}->IsolationPackageRoot(),
        $this->TestTarballName();
    }

    return $this->{testTarPath};
}

#------------------------------------------------------------
# Derives the name of the next tar ball we are to test.
#------------------------------------------------------------
sub NextPackageName
{
    my $this = shift;

    if (! $this->{nextPackageName})
    {
        my $lsCmd = join "", "ls -tr ", $this->{newTarLocation}, "/carbon-install-* 2> /dev/null";

        my (@tars) = (split /\n/,&CDSSystem::BackTick($lsCmd));

        if (! @tars)
        {
            my $lsCmd = join "", "ls -tr ", $this->{newTarLocation}, "/carbon-release*.tgz 2> /dev/null";
            
            (@tars) = (split /\n/,&CDSSystem::BackTick($lsCmd));
            
        }
        
        foreach (@tars)
        {
            $_ =~ s/^.+\///g; # make sure we're only a filename
            print "DEBUG 2: $_\n";

            my $releaseDir = $_;
            $releaseDir =~ s/\.tgz//; # If tarball
            $releaseDir =~ s/\-install\-/\-release\-/; # If install exec.

            if (! -d "$releaseDir/obj")
            {
                $this->{nextPackageName} = $_;
                last;
            }
        }
     }   

    return $this->{nextPackageName};
}

#------------------------------------------------------------
# Logs messages to an audit trail assigned to this class.
#------------------------------------------------------------

sub Log
{
    my $this     = @_[0];
    my $msg      = @_[1];
    my $file     = $this->{audit};
    my $fileName = $this->{fileNames}->AuditFileName();
    my $host     = $this->{fileNames}->CdsHosts()->LocalHostName();
    my $dir      = "";

    ($dir = $file) =~ s/\/$fileName$//;

    if (! -d $dir)
    {
        mkpath ($dir);
    }

    my $wh = CDSSystem::OpenAppend($this->{audit}, $this->ClassType());

    chomp (my $date = `date`);
    my $line = join "", $this->{fileNames}->CdsHosts()->LocalHostName(), 
              ": $date: ", $msg, "\n";

    print $wh $line;
    write "$file";
    close ($wh);

    select (STDERR);
    print $line;
}


1;
