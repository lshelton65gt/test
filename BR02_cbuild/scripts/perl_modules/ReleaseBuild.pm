
package ReleaseBuild;

use File::Basename;
use File::Path;
use strict;
use Getopt::Long;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSBuilder;
use CarbonQAData;
use CDSHosts;
use CDSFileNames;
use CDSSystem;
use PidSetMgr;
use SGEMgr;
use QATarOps;


sub new
{
    my $this = {};
    bless $this, @_[0];
    $this->{fileNames} = @_[1];
    $this->{args}      = @_[2];

    $this->Initialize();

    $this->{args}->DumpSettings();
    return $this;
}

sub Initialize
{
    my $this = shift;

    $this->{qaData}    = new CarbonQAData();
    $this->{hInfo}     = $this->{fileNames}->CdsHosts();

    if ($this->{args}->AdvanceTag())
    {
        $this->{qaCvsTool} = new QACvsTool($this->{args}->CheckoutTag(),
                                           $this->{fileNames});

        $this->{cvsTool}   = $this->{qaCvsTool}->CvsTool();
    }
    else
    {
        $this->{cvsTool}   = new CVSTool();
    }

    $this->{pidMgr}    = new PidSetMgr("release", $this->{fileNames}->CmdPaths());
    $this->{sgeMgr}    = new SGEMgr($this->{hInfo}, $this->{fileNames});

    $this->SetSandbox();

    $this->{checkoutTag} = $this->{args}->CheckoutTag();

    print "Current tag name is ", $this->{checkoutTag}, "\n";
}

sub SetSandbox
{
    my $this = shift;

    if (! $this->{args}->Sandbox())
    {
        if (! $ENV{CARBON_HOME})
        {
            $this->{sandbox} = join "/",
            $this->{fileNames}->DefaultReleaseTreeRoot(), 
            $this->GenerateReleaseDirname();
            $ENV{CARBON_HOME} = join "/", $this->{sandbox}, "tree3";
        }
        else
        {
            $this->{sandbox} = $this->{fileNames}->FindSandboxPath();
        }
    }
    elsif ($this->{args}->Sandbox() && ! $ENV{CARBON_HOME})
    {
        $this->{sandbox} = $this->{args}->Sandbox();
        $ENV{CARBON_HOME} = join "/", $this->{sandbox}, "tree3";
    }
    else
    {
        $this->{sandbox} = $this->{args}->Sandbox();
    }

    print "CARBON_HOME is $ENV{CARBON_HOME}\n";
    print "SANDBOX is ", $this->{sandbox}, "\n";
}

sub RunCarbonConfig
{
    my $this = shift;
    my $rtn = 0;
    my $carbonConfigArgs = "";

    foreach (sort keys %{$this->{args}->Platforms()})
    {
        print "Building software $_ platform\n";
        
        my $bldr = new CDSBuilder($_, $this->ReleaseSandbox(),
                                  $this->{fileNames}, $this->{sgeMgr});
        
        
        print "Running carbon_config for $_ platform Builder is $bldr \n";
        my $ccfgPid = CDSSystem::ForkProcess (\&CDSBuilder::CarbonConfig,1,
                                              $bldr, "$carbonConfigArgs");
        
        my $pid = new PidInfo($ccfgPid, "carbon_config_$_");
        
        $this->{pidMgr}->AddPids($pid);
# This is a hack so we can still fork, but one at atime because at the 
# moment carbon config can't run in parallel.

        while ($this->{pidMgr}->NumActive() > 0)
        {
            sleep 10;
        }

        print "PidMgr status for ", $pid, " is ", 
        $this->{pidMgr}->PidExitStatus($pid->Num()), "\n";
    }
    
    my $noCcfgOps = $this->{pidMgr}->NumActive();
    
    while ($noCcfgOps > 0)
    {
        print "Number of carbon_config ops is $noCcfgOps\n";
        sleep 10;
        $noCcfgOps = $this->{pidMgr}->NumActive();
        
    }
    print "Finished carbon_config\n";

    $rtn = $this->{pidMgr}->AnyBadStatus();

    if ($rtn == 0)
    {
        $this->{pidMgr}->CleanHouse();
    }
    else
    {
        $this->{pidMgr}->ShowFailedJobs();
    }

    return $rtn;
}

sub BuildSoftware
{
    my $this = shift;

    $this->{args}->DumpSettings();
 
    my $rtn = $this->RunCarbonConfig();

    return $rtn;
}

sub BranchChanged
{
    my $this = shift;
    return $this->{qaCvsTool}->BranchChanged();
}

sub SetupCarbonHomeSandbox
{
    my $this = shift;
    my $rtn = 0;
    
    if (! $this->{args}->SkipCheckout())
    {
        print "Setting up sandbox at ", $this->ReleaseSandbox(),"\n";
        
        if (! $this->{args}->AdvanceTag())
        {
            $rtn = $this->CheckoutModules($this->{sandbox});
        }
        else
        {
            my $sandbox = $this->ReleaseSandbox()."/tmp.$$";
            $rtn = $this->CheckoutModules($sandbox);
        }
    }
    else
    {
        print "NOTICE: Skip cvs checkout is selected.\n",
        "Using existing files at sandbox ", $this->ReleaseSandbox(), "\n";
        $rtn = 1;
    }

    return $rtn;
}

sub CheckoutModules
{
    my $this = shift;
    my $coDir = shift;
    my @modules = split " ", ($this->{qaData}->CheckoutModuleList(1));
    my $rtn = 1;

    if (! -d $coDir)
    {
        mkpath ($coDir);
    }

    foreach my $mod (@modules)
    {
        print "Checking out $mod ...\n";
        my $pidNo = 0;
        my $pidInfo = 0;

        my $pidNo = CDSSystem::ForkProcess (\&CVSTool::Co, 1, $this->{cvsTool},
                                            $mod, $this->{checkoutTag}, 
                                            "$coDir/$mod");

        $pidInfo = new PidInfo($pidNo, "Checkout_$mod");

        if ($this->{args}->AdvanceTag() || $mod ne "test")
        {
            $this->{pidMgr}->AddPids($pidInfo);
        }
    }

    my $active = $this->{pidMgr}->NumActive();

    while ($active != 0)
    {
        sleep 5;        

        $active = $this->{pidMgr}->NumActive();
    }

# Make sure we have these things.
    foreach (@modules)
    {
        print "Verifying $coDir/$_\n";

        if (! -f "$coDir/$_/CVS/Entries")
        {
            print "ERROR: No CVS information found in $coDir/$_/CVS/Entries\n";
            $rtn = 0;
        }
        else
        {
            print "Verified $coDir/$_/CVS/Entries\n";
        }
    }

    return $rtn;
}

sub ReleaseSandbox
{
    my $this = shift;

    return $this->{sandbox};

}

sub GenerateReleaseDirname
{
    my $this = shift;

    if (! $this->{relDirName})
    {
        $this->{relDirName} = "release-";
        
        if (! $this->{args}->AdvanceTag())
        {
            if ($this->{CheckoutTag} eq "")
            {
                $this->{relDirName} .= "HEAD";
            }
            else
            {
                $this->{relDirName} .= $this->{checkoutTag};
            }
        }

        my $sbDate = $this->{cvsTool}->SandboxCheckoutDate();
        my ($dt, $tm) = (split /\s/, $sbDate);
        my ($yr,$mo,$dy) = (split /\//,$dt);
        my ($hr, $min, $sec) = (split /\:/,$tm);

        if ($this->{relDirName} =~ /HEAD|BR_/)
        {
            $this->{relDirName} .= "-$yr-$mo-$dy";
        }
        else
        {
            $this->{relDirName} .= "_$mo$dy";
        }
    }

    return $this->{relDirName};
}

1;





