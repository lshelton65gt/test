#-------------------------------------------------------------
# Author Dave Allaby
# dallaby@carbondesignsystems.com
#-------------------------------------------------------------
#!! Description:
#!!
#!! Class which can read a standardized bom file.  A standardized
#!! bom file contains description tags.
#!!
#!! Currently, this class is called by CarbonConfig.pm.
#!!
#!! Description Tags:
#!! ------------------
#!! BOMTYPE    - Does the bom represent links or file targets?
#!! LINKROOT   - What is the link's root directory?
#!! TARGETROOT - What is the file target's root directory? 
#!! MIMICPATHS - Should any appended directories in the listing have
#!!              the paths mimic in the complementary file creation?
#!! PACKAGED   - true means the link/target in the file will be packaged.
#!!
#!! There are also some variable path tags which can be used inside the
#!! the bom files so to hard code commonly use paths which can not
#!! truly be hard coded.
#!!
#!! Variable path Tags:
#!! -------------------
#!! OBJDIR             - Converted to $CARBON_HOME/obj/$CARBON_TARGET
#!! CARBON_HOME        - Converted to $CARBON_HOME
#!! CARBON_ARCH        - Converted to Linux/Linux64/SunOS/Win
#!! CARBON_TARGET_ARCH - Converted to Linux/Linux64/SunOS/Win
#!! SANDBOX            - Converted to the sandbox path.
#!! GCC_TARGET         - Covereted to the gcc-#.#.# version
#!! UNAME              - Converted to the string returned by /bin/uname
#!!
package BomFileReader;
use strict;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSFileNames;
use CDSSystem;

#!! Constructor
#!!
#!! Name is the path to the bomfile AND it becomes the reader's name.
#!! The reader's name is the filename portion of the path, with the
#!! .bom extension removed.  It is assumed that a bom file read by this
#!! reader will always have a .bom file extension.
#------------------
sub new  #(string path_to_bomfile)
{
    my $this = {};
    bless $this, @_[0];
    my $name = @_[1];
    $this->{bomFile} = $name;
    $name =~ s/^.+\/|\.bom//g;
    $this->{fileNames} = @_[2];
    $this->{args} = @_[3];
    $this->{name} = $name;

    $this->Initialize();
    return $this;
}

#!! Initialize class variables.
#-------------------------------
sub Initialize  #()
{
    my $this         = shift;
    my %list         = ();
    $this->{list}    = \%list;

    my %pathKeys = ();
    $this->{pathKeys} = \%pathKeys;

    $this->{pathKeys}{OBJDIR}             = $this->{fileNames}->ObjectDir();
    $this->{pathKeys}{CARBON_HOME}        = $this->{fileNames}->CarbonHome();
    $this->{pathKeys}{CARBON_ARCH}        = $this->{args}->Platform();
    $this->{pathKeys}{CARBON_ARCH}        =~ s/\..+//;
    $this->{pathKeys}{CARBON_TARGET_ARCH} = $this->{args}->Platform();
    $this->{pathKeys}{CARBON_TARGET_ARCH} =~ s/\..+//;
    $this->{pathKeys}{SANDBOX}            = $this->{fileNames}->FindSandboxPath();
    $this->{pathKeys}{GCC_TARGET}         = $this->{args}->Target();
    $this->{pathKeys}{UNAME}              = $this->{fileNames}->CdsHosts()->HostUname();

    my @desc = qw (BOMTYPE LINKROOT TARGETROOT MIMICPATHS PACKAGED);

    my %rawFileContents = ();
    $this->{rawContents} = \%rawFileContents;

    foreach (@desc)
    {
        $this->{descFilter} .= "$_|";
        $this->{descCnt}++;
    }

    $this->{descFilter} =~ s/\|$//;
    
    my $rtn = $this->Read();
    
    return $rtn;
}

#!! Return the name of the reader.
#---------------------------------
sub Name  #()
{
    my $this = shift;
    return $this->{name};
}

#!! Return 1 bool for is it packaged?
#---------------------------------
sub Packaged #()
{
    my $this = shift;

    if ($this->{PACKAGED} !~ /true/i)
    {
        $this->{PACKAGED} = 0;
    }
    else
    {
        $this->{PACKAGED} = 1;
    }
    return $this->{PACKAGED};
}

#!! Reads the file listing in the bomfile.
#!! Applies any special handling on the individual
#!! bomfiles themselves where they do not fall into
#!! the common template.  We don't want to have special
#!! handlers, but since this has to be backwards compatible
#!! with existing bomfile operations, we have no choice.
#-----------------------------------------
sub Read  #()
{
    my $this = shift;
    my $rtn = 0;
    my $rh = CDSSystem::OpenRead($this->{bomFile});

    if ($rh)
    {
	my $descCnt = 0;
	my $descFilter = $this->{descFilter};

	while (<$rh>)
	{
	    chomp;
	    $_ =~ s/^\#$|\#.+$//g;
	    $_ =~ s/\s//g;

	    if ($_ ne "")
	    {
		if ($_ =~ /$descFilter/)
		{
		    my ($key, $value) = (split "=");
		    $key = lc ($key);
		    $descCnt++;

		    foreach my $ptag (keys %{$this->{pathKeys}})
		    {
			if ($value =~ /$ptag/)
			{
			    my $swapPath = $this->{pathKeys}{$ptag};
			    $value =~ s/$ptag/$swapPath/;
			}
		    }

		    $this->{$key} = $value;
		}
		else
		{
		    if ($descCnt < $this->{descCnt})
		    {
			$this->PrintDescriptionHelp($descCnt);
			close ($rh);

			die;
		    }
		    else
		    {
			my $filename     = "";
			my $dir          = "";
                        my $path         = "";

                        $this->{rawContents}{$_} = $_;

                        if ($_ =~ /\//)
                        {
                            ($filename = $_) =~ s/^.+\///;
                            ($dir = $_)      =~ s/\/$filename$//;
                        }
                        else
                        {
                            $filename = $_;
                        }

			my $link         = "";
			my $target       = "";

			if ($this->{bomtype} =~ /target(|s)/i)
			{
			    $target = join "/", 
			    $this->{targetroot}, "$dir/$filename";
			    
			    if ($this->{mimicpaths} =~ /true/i)
			    {
				$link = join "/",
				$this->{linkroot}, "$dir/$filename";
			    }
			    else
			    {
                                $link = join "/",
				$this->{linkroot}, $filename;
			    }
			}
			else
			{
			    $link = join "/",$this->{linkroot},
			    $dir,$filename;
			    
			    if ($this->{mimicpaths} =~ /true/i)
			    {
				$target = join "/",
				$this->{targetroot}, "$dir/$filename";
			    }
			    else
			    {
				$target = join "/",
				$this->{targetroot}, $filename;
			    }
			}

                        $target =~ s/\/\//\//g;
                        $link =~ s/\/\//\//g;
			$this->{list}{$link} = $target;
		    }
		}
	    }
	}

	close ($rh);
    }
    
    $rtn = values (%{$this->{list}});

    return $rtn;
}

#---------------------------------------------------------------
#!! Print a message if we cannot understand how to process the 
#!! bom file contents.
#---------------------------------------------------------------
sub PrintDescriptionHelp  #()
{
    my $this = shift;
    my $cnt = shift;

    print STDERR "\n",
    " ERROR: Bom file ", $this->{bomFile},"\n",
    " ERROR: does not have sufficient description tags.\n",
    " ERROR: Description tags found: [$cnt]\n",
    " ERROR: Each bom file must have a description section using the\n",
    " ERROR: following keyword tags, one per line.\n",
    "     Use Tags: \n", 
    "     ", $this->{descFilter}, ".\n",
    "     See other valid bom files for example.\n",
    "     Cannot continue\n";
}

#---------------------------------------------------------------
#!! Return the list of files from inside the file
#---------------------------------------------------------------
sub List  #()
{
    my $this = shift;
    return %{$this->{list}};
}

sub RawContents
{
    my $this = shift;
    return $this->{rawContents};
}

#---------------------------------------------------------------
#!! Returns the regular expression filter.  We need this because
#!! there are other things reading the bom file and we've added 
#!! additional embedded data, which the other things will need to 
#!! understand how to filter out.
#---------------------------------------------------------------
sub DescriptionFilter #()
{
    my $this = shift;
    $this->{descFilter};
}

1;
