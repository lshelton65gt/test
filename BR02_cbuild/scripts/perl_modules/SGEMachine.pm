
package SGEMachine;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CmdPaths;
use CDSSystem;

sub new
{
    my $this = {};
    my $name = @_[1];
    my $cmdPaths = @_[2];
    bless $this, @_[0];

    if ($name)
    {
	if ($name !~ /\.q$/)
	{
	    $name .= ".q";
	}

	if (! $cmdPaths)
	{
	    $this->{cmdPaths} = new CmdPaths();
	}
	
	$this->{machName} = $name;
	$this->{qstatCmd} = join " ", $this->{cmdPaths}->Command("qstat"),
	"-F -q", $this->{machName};

	$this->InitMachine();
    }
    else
    {
	print STDERR "Cannot initialize ", $this->ClassType(),
	"Machine name was not specified.\n";
	$this = "";
    }
    return $this;
}

sub ClassType
{
    my $this = shift;
    return ref($this);
}

sub InitMachine
{
    my $this = shift;
    my $cmd = $this->{qstatCmd};
    my %attrib = ();
    $this->{attrib} = \%attrib;

    my @attributes = split /\n/,CDSSystem::BackTick($cmd);
    
    foreach (@attributes)
    {
	chomp;

	if ($_ =~ /^$this->{machName}/ && $_ =~ /BIP/)
	{
	    ($this->{os}) = (split)[4];
	}
	elsif ($_ =~ /\t\w{2}\:\S+/)
	{
	    $_ =~ s/^\t//;
	    my ($code, $param) = (split ":")[0,1];
	    my ($attr, $value) = (split "=",$param)[0,1];

	    if ($value != 0 || $value !~ /\d\.\d/)
	    {
		$this->{attrib}{$code}{$attr} = $value;
	    }
	}
    }
}

sub FreeJobSlots
{
    my $this = shift;

    my $cmd = $this->{qstatCmd};
    my @attributes = split /\n/, CDSSystem::BackTick($cmd);
    my $slots = split $attributes[2];

    my ($used, $total) = (split /\//,$slots);
    my $free = $total - $used;

    return $free;
}

sub Pr
{
    my $this = shift;
    my $host = "";
    ($host = $this->{machName}) =~ s/\.q//;

    print "Host is $host\n";

    foreach my $code (keys %{$this->{attrib}})
    {
	foreach (keys %{$this->{attrib}{$code}})
	{
	    print "\t\t$code:$_=", $this->{attrib}{$code}{$_},"\n";
	}
    }
}

sub a32Bit
{
    my $this = shift;
    my $a32bit = 1;

    if ($this->a64Bit())
    {
	$a32bit = "";
    }

    return $a32bit
}

sub a64Bit
{
    my $this = shift;
    my $a64bit = "";

    if ($this->{os} eq "solaris64")
    {
	$a64bit = 1;
    }
    else
    {
	$this->AttrLookup("x86_64");
    }

    return $a64bit;
}

sub VmMachine
{
    my $this = shift;
    my $vmfarm = $this->AttrLookup("vmfarm");

    return $vmfarm;
}

sub AttrLookup
{
    my $this = shift;
    my $attr = shift;
    my $val  = "";

    print "Looking for $attr\n";

    foreach my $code (%{$this->{attrib}})
    {
	my $v = $this->{attrib}{$code}{$attr};

	if ($v)
	{
	    $val = $v;
	}
    }

    return $val;
}

1;
