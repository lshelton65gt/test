

package LinkGroup;

use strict;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use PackageLink;

sub new 
{
    my $this = {};
    bless $this, @_[0];
    my $dontPackage = @_[1];

    if ($dontPackage)
    {
        $this->{package} = 0;
    }
    else
    {
        $this->{package} = 1;
    }

    return $this;
}

sub DeleteLink
{
    my $this = shift;
    my $link = shift;

    delete $this->{$link};
}

sub DumpList
{
    my $this = shift;

    foreach (keys %{$this})
    {
        if (ref($this->{$_}) =~ /PackageLink/)
        {
            print "Package: ", $this->{$_}, "   Link: ", $this->{$_}->Name(), "   Target: ", $this->{$_}->Target(), "\n";
        }
    }
}

sub Released
{
    my $this = shift;
    
    return $this->{packaged};
}

sub AddLink
{
    my $this = shift;
    my $link = shift;
    my $target = shift;
    my $pkgLink = new PackageLink ($link,$target);

    $this->{$link} = $pkgLink;
}

sub LinkList
{
    my $this = shift;
    my %group = ();

    foreach my $lnk (keys %{$this})
    {
        my $lref = ref($this->{$lnk});

        if ($lref =~ /PackageLink/)
        {
            $group{$this->{$lnk}->Name()} = $this->{$lnk}->Target();
        }
    }

    return %group;
}

sub MyLink
{
    my $this = shift;
    my $link = shift;
    my $owned = 0;

    if ($this->{$link})
    {
        $owned = 1;
    }
    
    return $owned;
}

1;
