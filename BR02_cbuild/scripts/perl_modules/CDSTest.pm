

use strict;
use File::Basename;

#=============================================================
# CDSTest Class
#=============================================================

package CDSTest;

my $INIT_RESULT_STATE = -999;

#-----------------------------------------------
# Constructor(name, variant) Variant optional.
#-----------------------------------------------

sub new
{
    my $this    = {};
    bless $this, @_[0];
    my $name    = @_[1];

# Set test name
    $this->{name} = $name;
    
# Init Dir Name
    my $dir = "";
    $this->{dir} = File::Basename::dirname($name);
    
# Set variant if given, might not be anything.

    if ($name =~ /,/)
    {
	my ($variant) = (split /,/,$name)[1];
	$this->{variant} = $variant;
	$this->{objDir} = join "/", $this->{variant}, $this->{dir};
    }
    else
    {
	$this->{objDir} = $this->{dir};
    }
    
    $this->Initialize();
    
    return $this;
}

#-----------------------------------------------
# Does Clear(), then nulls $this out.
#-----------------------------------------------

sub Delete
{
    my $this = shift;
    
    $this->Clear();
    
    $this = {};
}

#------------------------------------------------------
# Initialize the classes data references
#------------------------------------------------------

sub Initialize
{
    my $this = @_[0];

# Init temp env hash so we can store
# anything we need to change for the test.
# This way when it's over we can revert.

    my %oldEnv = ();
    $this->{oldEnv} = \%oldEnv;

# Set runtime
    $this->{runtime} = 0;
    
# Init Dependency tests
    my %childTests = ();
    $this->{childTests} = \%childTests;
    
# Init Parent test
    my %parentTests = ();
    $this->{parentTests} = \%parentTests;
    
# Init Can't run on platforms
    my %notArch = ();
    $this->{notArch} = \%notArch;
    
# Init QSubArgs
    $this->{size} = "";

# Init testname alias. Used in perftesting.
    $this->{alias} = "";

# Init Groups
    my %groups = ();
    $this->{groups} = \%groups;
    
# Init Explicit Children
    my %explicitChild = ();
    $this->{explicit} = \%explicitChild;
    
# Init test results state 
    $this->{result} = $INIT_RESULT_STATE;
    
# Init test name regular expression match.
    $this->{match} = "";
    
# Init waitfor tests
    my %waitForTests = ();
    $this->{waitfor} = \%waitForTests;
    
# Init Special Environment Vars 
    my %envVars = ();
    $this->{env} = \%envVars;
}

#----------------------------------------------
# Returns true if the test result is known.
#----------------------------------------------

sub TestingComplete
{
    my $this = shift;
    my $bool = 0;
    
    if ($this->{result} != $INIT_RESULT_STATE)
    {
	$bool = 1;
    }
    
    return $bool;
}

#==============================================
# Test attributes which are performance test
# xml ONLY Attributes.
#==============================================
#-------------------------------------------
# In perf testing, the <name> tag represents
# a shortname alias for the test.  Later, we
# get the real name using a <test> tag.
# We need to init this class using the alias
# because of order in the xml file.  When we
# get the real name, we'll make the switch
#-------------------------------------------

sub SetNameToAlias
{
    my $this = shift;
    my $realname = shift;

    $this->{alias} = $this->Name();
    $this->{name} = $realname;
}

#------------------------------------------
# Returns the alias if there is one, else
# the real name is returned.
#------------------------------------------
sub AliasName
{
    my $this = shift;
    my $alias = $this->{alias};

    if (! $alias)
    {
	$alias = $this->Name();
    }
    
    return $alias;
}

#--------------------------------------------------
# Performance testing using alternate test.run
# commands.  This sets what is in the xml file for
# the <cmd> tag
#--------------------------------------------------
sub SetPerfCommand
{
    my $this = shift;
    my $cmd = shift;
    $this->{perfcmd} = "$cmd";
}

#--------------------------------------------------
# Returns the performance command to run inside
# a test.run file.
#--------------------------------------------------
sub PerfCommand
{
    my $this = shift;
    return $this->{perfcmd};
}

#--------------------------------------------------
# Performance tests iterate
#--------------------------------------------------

sub SetIterationCnt
{
    my $this = @_[0];
    $this->{iterCnt} = @_[1];
}

sub IterationCnt
{
    my $this = shift;
    return $this->{iterCnt};
}

#-------------------------------------------------
# Cycle count. Used in peformance testing
#-------------------------------------------------
sub SetCycleCnt
{
    my $this = shift;
    $this->{cycleCnt} = shift;
}

sub CycleCnt
{
    my $this = shift;
    return $this->{cycleCnt};
}

#--------------------------------------------------
# Peformance node information.  Used in performance
# testing to determine which tests will go one which
# host.
#--------------------------------------------------

sub SetPerfomanceNode
{
    my $this = shift;
    $this->{perfNode} = shift;
}

sub PeformanceNode
{
    my $this = shift;
    return $this->{perfNode};
}

#-------------------------------------------------
# Performance tests specify cbuild logs which
# this test will use.
#-------------------------------------------------
sub SetCBuildLog
{
    my $this = shift;
    $this->{cbuildLog} = shift;
}

sub CBuildLog
{
    my $this = shift;
    return $this->{cbuildLog};
}



#--------------------------------------------------
# Adds a test that this test must wait for to finish
# before starting. <waitfor>
#--------------------------------------------------

sub AddWaitForTest
{
    my $this = @_[0];
    my $waitTest = @_[1];
    
    $this->{waitfor}{$waitTest} = $waitTest;
}

#--------------------------------------------------
# Returns the list of tests that this test has to
# wait to finish before it can start up.
#--------------------------------------------------

sub WaitForTests
{
    my $this = shift;
    return %{$this->{waitfor}};
}

#--------------------------------------------------
# Returns how many wait tests we have in hash.
#--------------------------------------------------
sub NumWaitForTests
{
    my $this = shift;
    my $no = values %{$this->{waitfor}};
    
    return $no;
}

#--------------------------------------------------
# Removes wait test from our list. Doesn't delete()
# the actual test, just removes from our hash.
#--------------------------------------------------

sub DeleteWaitForTest
{
    my $this = @_[0];
    my $test = @_[1];
    
    if ($this->{waitfor}{$test})
    {
	delete $this->{waitfor}{$test};
    }
}

#--------------------------------------------------
# Set regular expression match. <match>
#--------------------------------------------------

sub SetMatchStr
{
    my $this = @_[0];
    my $this->{match} = "@_[1]";
}

#--------------------------------------------------
# If test has a regular expression key for other
# tests to try to match.
#--------------------------------------------------

sub HasMatchStr
{
    my $this = @_[0];
    my $bool = 0;
    
    if ($this->{match} ne "")
    {
	$bool = 1;
    }
    
    return $bool;
}

#--------------------------------------------------
# Returns the regular expression
#--------------------------------------------------

sub MatchStr
{
    my $this = shift;
    return $this->{match};
}

#--------------------------------------------------
# Set the return status of the test
#--------------------------------------------------

sub SetResult
{
    my $this = @_[0];
    $this->{result} = @_[1];
}

#--------------------------------------------------
# Return stored status of the test
#--------------------------------------------------

sub Result
{
    my $this = @_[0];
    return $this->{result};
}

#-----------------------------------------------
# Copies this instance into new instance of CDSTest
#
# I'm assuming 2 cases for this method.  We can
# use it to create another variant test from an existing
# and we can use it to clone settings for tests
# that <match>.  Right now, I'm assuming that
# there's no test that is both a variant test
# and has a <match> attribute.
#
# So then, there is no need to copy in variant settings
# from the copied from class.  Any variant copy will
# just get the base info, not the variant environment
# variables, nor will we copy the ObjDirPath, since
# it will get it's own based on the variant's name.
#-------------------------------------------------

sub Copy
{
    my $this     = @_[0];
    my $newName  = @_[1];
    my $variant  = @_[2]; # Optional
    my $copyTest = new CDSTest($newName, $variant);
    
    foreach (keys %{$this->{groups}})
    {
	$copyTest->AddGroup($this->{groups}{$_});
    }
    
    foreach (keys %{$this->{notArch}})
    {
	$copyTest->AddNotArch($this->{notArch}{$_});
    }
    
    foreach (keys %{$this->{parentTests}})
    {
	$copyTest->AddParentTest($this->{parentTests}{$_});
    }
    
    foreach (keys %{$this->{childTests}})
    {
	if ($this->{explicit}{$_})
	{
	    $copyTest->AddChildTest($this->{childTests}{$_}, 1);
	}
	else
	{
	    $copyTest->AddChildTest($this->{childTests}{$_});
	}
    }
    
    $copyTest->SetQSubArgs($this->QSubArgs());
    
    if ($this->TestingComplete())
    {
	$copyTest->SetResult($this->Result());
    }
    
    foreach (keys %{$this->{waitfor}})
    {
	$copyTest->AddWaitForTest($_);
    }
    
    return $copyTest;
}

#------------------------------------------
# Returns the name of the variant, not to
# be confused with IsVariant(), which is a
# test to see if the test has variant 
# attributes.
#------------------------------------------

sub Variant
{
    my $this = shift;
    return $this->{variant};
}

sub ExportTestEnv
{
    my $this = shift;
    
    foreach (keys %{$this->{env}})
    {
	if ($ENV{$_})
	{
	    $this->{oldEnv}{$_} = $ENV{$_};
	}

	$ENV{$_} = "$this->{env}{$_}";
    }
}

sub UnExportTestEnv
{
    my $this = shift;
    
    foreach (keys %{$this->{env}})
    {
	if ($this->{oldEnv}{$_})
	{
	    $ENV{$_} = $this->{oldEnv}{$_};
	}
	else
	{
	    delete $ENV{$_};
	}
    }
}

#---------------------------------------------
# Adds a variant environment variable
# description to the test. We will set these
# environment things before running the test
#---------------------------------------------

sub AddEnvironmentVar
{
    my $this = @_[0];
    my $var = @_[1];
    my $val = @_[2];
    
    $this->{env}{$var} = "$val";
}

#---------------------------------------------
# Returns hash array of env vars to set for 
# this test.
#---------------------------------------------

sub VariantEnvVars
{
    my $this = shift;
    return %{$this->{env}};
}

#------------------------------------------------
# Returns true if test is a variant
#------------------------------------------------

sub IsVariant
{
    my $this = @_[0];
    my $bool = 0;

    if ($this->{variant} ne "")
    {
	$bool = 1;
    }

    return $bool;
}

#------------------------------------------
# Push group
#------------------------------------------

sub AddGroup
{
    my $this = @_[0];
    $this->{groups}{@_[1]} = @_[1];
}

#---------------------------------------------
# Test for member of passed group value
#---------------------------------------------

sub GroupMember
{
    my $this = @_[0];
    my $group = @_[1];
    my $bool = 0;

    if ($this->{groups}{$group})
    {
	$bool = 1;
    }
    
    return $bool;
}

#-----------------------------------------------
# Clears the class/hash, restores to nothing
# Used for reassignment of same pointer
#-----------------------------------------------

sub Clear
{
    my $this = shift;

# Delete parent tests.
#---------------------------------------------
    foreach (keys %{$this->{parentTests}})
    {
	$this->{parentTests}{$_}->DisconnectChild($this);
	delete $this->{parentTests}{$_};
    }
    delete $this->{parentTests};

# Delete child tests
#---------------------------------------------

    foreach (keys %{$this->{childTests}})
    {
	$this->{childTests}{$_}->DisconnectParent($this);
	delete $this->{childTests}{$_};
    }
    delete $this->{childTests};

# Delete not arches
#---------------------------------------------
    
    $this->ClearNotArches();
    delete $this->{notArch};
    
# Delete and unset env vars for the test
#---------------------------------------------
    foreach (keys %{$this->{env}})
    {
	delete $ENV{$_};
	delete $this->{env}{$_};
    }
    delete $this->{env};
    
# Delete waitfor tests
#---------------------------------------------
    foreach (keys %{$this->{waitfor}})
    {
	delete $this->{waitfor}{$_};
    }
    delete $this->{waitfor};
    
# Clear all other one dimensional hashes
#---------------------------------------------
    
    delete $this->{dir};
    delete $this->{objDir};
    delete $this->{name};
    delete $this->{result};
    delete $this->{runtime};
    delete $this->{size};
    delete $this->{variant};
    
    $this = {};
}

#----------------------------------------------
# Set and Get for QSubArgs, given in <size>
#----------------------------------------------

sub SetQSubArgs
{
    my $this = @_[0];
    $this->{size} = @_[1];
}

sub QSubArgs
{
    my $this = shift;
    return $this->{size};
}

#---------------------------------------------
# Set and Gets for runtime attribute <runtime>
#---------------------------------------------

sub SetRunTime
{
    my $this = @_[0];
    $this->{runtime} = @_[1];
}

sub Runtime
{
    my $this = shift;
    return $this->{runtime};
}

#---------------------------------------------
# This is the path to the test appended to 
# $objDir usually, but appended to whatever the
# top most test directory is.  It would be the 
# same path as TestRootDir if not a variant,
# otherwise it's slightly different.
#---------------------------------------------

sub ObjDirPath
{
    my $this = @_[0];
    return $this->{objDir};
}

#-----------------------------------------------
# Returns the Test's Rootdir (next one up)
#-----------------------------------------------

sub TestRootDir
{
    my $this = shift;
    return $this->{dir};
}

#-----------------------------------------------
# Debug: Prints attributes of class
#-----------------------------------------------

sub Pr
{
    my $this = @_[0];
    
    print "\n",
    "Printing information on test: ", $this->{name}, "\n",
    "====================================================\n",
    " Test's root dir is ", $this->{dir}, "\n",
    " Test's append path to Object Dir is ", $this->{objDir}, "\n";
    
    if ($this->NumParentTests() > 0)
    {
	print " Test has ", $this->NumParentTests(), 
	" parent tests\n";
	
	foreach (sort keys %{$this->{parentTests}})
	{
	    print "  Parent Test: ", 
	    $this->{parentTests}{$_}->Name(), "\n";
	}
    }
    else
    {
	print " Test has no parent tests\n";
    }	 
    
    if ($this->NumChildTests() > 0)
    {
	print " Test has ", 
	$this->NumChildTests(), " child tests\n";
	
	foreach (sort keys %{$this->{childTests}})
	{
	    print "  Child Test: ", 
	    $this->{childTests}{$_}->Name();
	    
	    if ($this->{explicit}{$_})
	    {
		print " [explicit] \n";
	    }
	    else
	    {
		print " [implicit] \n";
	    }
	}
    }
    else
    {
	print " Test has no child tests\n";
    }

    if ($this->NumNotArch() > 0)
    {
	print " Test has ", $this->NumNotArches(), " invalid arches.\n";
	
	foreach (sort keys %{$this->{notArch}})
	{
	    print "  Not valid: ", $this->{notArch}{$_}, "\n";
	}
    }
    else
    {
	print " Test is valid on all platforms\n";
    }
    
    if ($this->{variant} ne "")
    {
	print "Test is a variant type test of name: ", $this->{variant}, "\n";
    }
    else
    {
	print "Test is NOT a variant type test\n";
    }
    
    print "Current result state is ", $this->{result}, "\n";
    
    if ($this->NumWaitForTests() > 0)
    {
	print "Test has ", $this->NumWaitForTests(), " in it's list\n";
	
	foreach (sort keys %{$this->{waitfor}})
	{
	    print "  Waitfor test -> ", $this->{waitfor}{$_}->Name(), "\n";
	}
    }
    else
    {
	print "Test has no waitfor tests in it's hash\n";
    }
    
    if ($this->QSubArgs())
    {
	print "Test has QSubArgs -> ", $this->QSubArgs(), "\n";
    }
    else
    {
	print "Test has no QSubArgs -> ", $this->QSubArgs(), "\n";
    }
}

#-------------------------------------------
# Returns true if passed test is my child 
#-------------------------------------------

sub IsMyChild
{
    my $this = @_[0];
    my $child = @_[1];
    my $owned = 0;

    if ($this->{childTests}{$child})
    {
	$owned = 1;
    }

    return $owned;
}    

#-------------------------------------------
# Returns true if passed test is my parent 
#-------------------------------------------

sub IsMyParent
{
    my $this = @_[0];
    my $parent = @_[1];
    my $owned = 0;

    if ($this->{parentTests}{$parent})
    {
	$owned = 1;
    }

    return $owned;
}    

#-----------------------------------------------
# Tell the parent test we are no longer a child
# test. Remove parent test. 
#-----------------------------------------------

sub DisconnectParent
{
    my $this = @_[0];
    my $parent = @_[1];

    if ($this->{parentTests}{$parent})
    {
	my $p = $this->{parentTests}{$parent};
	delete $this->{parentTests}{$parent};
	$p->DisconnectChild($this);
    }
}

#-----------------------------------------------
# Tell the child test we are no longer a parent
# test. Remove child test. 
#-----------------------------------------------

sub DisconnectChild
{
    my $this = @_[0];
    my $child = @_[1];

    if ($this->{childTests}{$child})
    {
	my $c = $this->{childTests}{$child};
	delete $this->{childTests}{$child};
	$c->DisconnectParent($this);
    }

    delete $this->{childTests}{$child};
}

#------------------------------------------------
# Returns the parent hash
#------------------------------------------------

sub ParentTests
{
    my $this = shift;
    return %{$this->{parentTests}};
}

#------------------------------------------------
# Returns the child hash
#------------------------------------------------

sub ChildTests
{
    my $this = shift;
    return %{$this->{childTests}};
}

#-------------------------------------------------
# Prints the name of the test.
#-------------------------------------------------

sub Name
{
    my $this = shift;
    return $this->{name};
}

#-------------------------------------------------
# Returns the type of the class
#-------------------------------------------------

sub ClassType
{
    my $this = shift;
    return ref($this);
}

#--------------------------------------------------
# If not mentioned parent's <dep>, it then test is
# and explicit child test.
#--------------------------------------------------

sub ExplicitChild
{
    my $this = @_[0];
    my $child = @_[1];
    my $bool = 0;

    if ($this->{explicit}{$child})
    {
	$bool = 1;
    }

    return $bool;
}

#-------------------------------------------------
# Add test as a child test, tell the child to
# add us a parent.
#-------------------------------------------------

sub AddChildTest
{
    my $this = @_[0];
    my $child = @_[1];
    my $explicit = @_[2];

    if (! $this->{childTests}{$child})
    {
	$this->{childTests}{$child} = $child;
	$child->AddParentTest($this);
    }

    if ($explicit)
    {
	$this->{explicit}{$child} = $child;
    }
}

#-------------------------------------------------
# Add test as a parent test, tell the parent to
# add us a child.
#-------------------------------------------------

sub AddParentTest
{
    my $this = @_[0];
    my $parent = @_[1];

    if (! $this->{parentTests}{$parent})
    {
	$this->{parentTests}{$parent} = $parent;
	$parent->AddChildTest($this);
    }
}

#-------------------------------------------------
# Returns number of child tests associated with
# this test
#-------------------------------------------------

sub NumChildTests
{
    my $this = shift;
    my $no = values(%{$this->{childTests}}); 
    return $no;
}

#-------------------------------------------------
# Set invalid arches, referred to as "not.$arch"
# in the <arch> tag of the alltests.xml.
#-------------------------------------------------

sub AddNotArch
{
    my $this = @_[0];
    my $arch = @_[1];

    if ($arch !~ /not\./)
    {
	$arch = "not." . $arch;
    }

    $this->{notArch}{$arch} = $arch;
}

#-------------------------------------------------
# How many invalid arches are there?
#-------------------------------------------------

sub NumNotArches
{
    my $this = shift;
    my $no = values (%{$this->{notArch}});
    return $no;
}

#-----------------------------------------------
# Returns a hash of invalid arches
#-----------------------------------------------

sub NotArches
{
    my $this = @_[0];

    return %{$this->{notArch}};
}

#-----------------------------------------------
# Deletes the invalid arches
#-----------------------------------------------

sub ClearNotArches
{
    my $this = shift;

    foreach (keys %{$this->{notArch}})
    {
	delete $this->{notArch}{$_};
    }
}

#-----------------------------------------------
# Tests for invalidness of passed in arch
#-----------------------------------------------
sub ArchIsValid
{
    my $this = @_[0];
    my $arch = @_[1];
    my $bool = 1;

# Fix so you can pass in either way.
    if ($arch !~ /not\./)
    {
	$arch = "not." . $arch;
    }

    if ($this->{notArch}{$arch})
    {
	$bool = 0;
    }

    return $bool;
}

#-------------------------------------------------
# Returns number of invalid platforms 
# associated with this test
#-------------------------------------------------

sub NumNotArch
{
    my $this = shift;
    my $no = values(%{$this->{notArch}}); 
    return $no;
}

#-------------------------------------------------
# Returns number of parent tests associated with
# this test
#-------------------------------------------------

sub NumParentTests
{
    my $this = shift;
    my $no = values(%{$this->{parentTests}});
    return $no
}

1; 
