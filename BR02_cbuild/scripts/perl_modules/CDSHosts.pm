#====================================================================
# CDSHosts class.
# Author Dave Allaby. dallaby@carbondesignsystems.com
#
#!! Description: A class to understand the site's network hosts 
#!! information.
#!!
#!! All hosts in ypcat hosts are read in at once.  We need know about
#!! all the hosts because of the way this place does automounting.
#!! /w/$host may need to be resolved to /work.  Aliases are also allowed
#!! /w/$hostalias needs to be understood.
#!!
#!! Info on any host after invocation can be obtained. Passing this class
#!! around between classes shares information between processes so
#!! that we A) all have the same info, and B) we only made a shell call
#!! or 2 to get all the information for everybody. 
#!! It's Speedy.
#====================================================================

package CDSHosts;

use strict;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSSystem;
use CmdPaths;

sub new
{
    my $this = {};
    bless $this, @_[0];
    $this->{cmdPaths} = @_[1];

    $this->Initialize();
    return $this;
}

sub Initialize
{
    my $this = shift;
    $this->{ypcatSuccess} = 0;
    
    if (! $this->{cmdPaths})
    {
        $this->{cmdPaths} = new CmdPaths();
    }


# Isolated testing machines.
#---------------------------------------------
    my %isolatedHosts             = ();
    $this->{isolated}             = \%isolatedHosts;
    $this->{isolated}{Linux}      = "hafnium";
    $this->{isolated}{SunOS}      = "sunae1";

# Hosts available for transfers
#---------------------------------------------
    my %transferHosts             = ();
    $this->{transHosts}           = \%transferHosts;
    $this->{transHosts}{Linux}    = "qa101";
    $this->{transHosts}{SunOS}    = "sulfur";

# Domain
#--------
    my $localDomain               = "carbondesignsystems.com";
    $this->{localDomainname}      = $localDomain;

# Hostname
#----------
    my $hostnameCmd               = $this->{cmdPaths}->Command("hostname");
    chomp ($this->{localHostname} = `$hostnameCmd`);
    $this->{localHostname}        =~ s/\.$localDomain//;
    $this->{localHostnameLong}    = join ".", $this->{localHostname},
                                    $this->{localDomainname};

# OS Type
#---------
    my $unameCmd                  = $this->{cmdPaths}->Command("uname");
    chomp ($this->{uname}         = `$unameCmd`);


# Processes ypcat.  We don't care if we are
# on isolated hosts, and it's VERY slow because
# of the isolatedness!!!
#--------------------------------------------
    if ($this->{isolated}{$this->{uname}} ne 
	$this->{localHostname})
    {
	$this->InitAllHosts();
    }
}

sub QARemoteCmdHost
{
    my $this = shift;
    my $os = shift;

    if (! $os)
    {
        chomp($os = `/bin/uname`);
    }

    return $this->{transHosts}{$os};
}

sub BugzillaHost
{
    return "mon101";
}

sub PerfHostList
{
    my $this = shift;
    my $perfConfigXml = shift;

    if (! $this->{perfHostList})
    {
        if (! $perfConfigXml)
        {
            print STDERR $this->ClassType(), 
            "::PerfHostList recieved no path ",
            "to the performance testing xml configuration file.\n";
        }
        elsif (! -f $perfConfigXml)
        {
            print STDERR "Cannot find $perfConfigXml for reading.\n",
            "No performance machine list can be obtained\n";
        }
        else
        {
            use PerfConfXMLReader;
            my $reader = new PerfConfXMLReader($perfConfigXml);
            my %perfHosts = $reader->PerfHostList();
            $this->{perfHostList} = \%perfHosts;
        }
    }

    return %{$this->{perfHostList}};
}


#--------------------------------------------
# Prints class name
#--------------------------------------------
sub ClassType
{
    my $this = shift;
    return ref($this);
}

#---------------------------------------------
# Returns the CmdPaths class for use by others
#---------------------------------------------
sub CmdPaths
{
    my $this = shift;
    return $this->{cmdPaths};
}

#---------------------------------------------
# Returns the isolated hostname for os testing
#---------------------------------------------
sub IsolatedHostName
{
    my $this = @_[0];
    my $carbonTargetArch = @_[0];

    return $this->{isolated}{$carbonTargetArch};
}

#---------------------------------------------
# Accesses all the host information via ypcat.
#---------------------------------------------
sub InitAllHosts
{
    my $this  = shift;
    my $ypcat = $this->{cmdPaths}->Command("ypcat");
    my $cmd   = "$ypcat -k hosts 2> /dev/null";
    my @tmp   = split /\n/, CDSSystem::BackTick($cmd);

    if (@tmp)
    {
	$this->{ypcatSuccess} = 1;
	foreach (@tmp)
	{
	    
	    $_ =~ s/(\s|\s+)/ /g;
	    my ($host, $ip) = (split)[0,1];
	    my $aliases = "";
	    ($aliases = $_) =~ s/$host\s$ip\s//;
	    $aliases =~ s/^(\s|\s+)|(\s+|\s)$//g;
	    
	    if ($aliases !~ /$host/)
	    {
		$aliases .= " $host";
	    }
	    
	    $this->{$host}{alias} = "$aliases";
	    $this->{$host}{ip} = $ip;
	}
    }
}

#--------------------------------------------------
# Using ypcat to populate the hosts table might fail
# We don't want to exit if it fails because the 
# caller may not care. We will mark success/failure 
# though, in case something using this does care.
#--------------------------------------------------
sub YpcatInfo
{
    my $this = shift;
    return $this->{ypcatSuccess};
}

#--------------------------------------------------
# hostname return
#--------------------------------------------------
sub LocalHostName
{
    my $this = shift;
    return $this->{localHostname};
}

#--------------------------------------------------
# domainname return
#--------------------------------------------------
sub LocalDomainName
{
    my $this = shift;
    return $this->{localDomainname};
}

#--------------------------------------------------
# hostname.domainname return
#--------------------------------------------------
sub LocalHostNameLong
{
    my $this = shift;
    return $this->{localHostnameLong};
}

#--------------------------------------------------
# Uname return
#--------------------------------------------------
sub HostUname
{
    my $this = shift;
    return $this->{uname};
}

#--------------------------------------------------
# Local host ip.
#--------------------------------------------------
sub LocalIp
{
    my $this = shift;
    return $this->{ipAddress};
}

#--------------------------------------------------
# Returns a @list of alias names for the host name
# passed into function
#--------------------------------------------------
sub HostAliases
{
    my $this = @_[0];
    my $host = @_[1];
    my @alias = "";

    if (! $host)
    {
	@alias = split " ", $this->{$this->LocalHostName()}{alias};
    }
    else
    {
	@alias = split " ", $this->{$host}{alias};	    }

    return @alias;
}
#--------------------------------------------------
# Looks up IP of host in our own lookup hash.
# No shell call.
#--------------------------------------------------
sub IPLookup
{
    my $this = @_[0];
    my $host = @_[1];
    my $ip = 0;

    if (! $host)
    {
	$ip = $this->{$this->LocalHostName()}{ip};
    }
    else
    {
	$ip = $this->{$host}{ip};    }

    return $ip;
}

#--------------------------------------------------
# True if host is a known member of our class.
#--------------------------------------------------
sub ValidHost
{
    my $this = @_[0];
    my $name = @_[1];
    my $valid = 0;

    if ($this->{$name})
    {
	$valid = 1;
    }

    return $valid;
}

1;
