
#package SGEMgr;

sub new 
{
    my $this = {};
    my $name = @_[1];

    bless $this, @_[0];

    $this->{name} = $name;

    return $this;
}

sub Name
{
    my $this = shift;
    return $this->{name};
}


1;
