#=========================================================
# Class CarbonQAData
# Author Dave Allaby.  dallaby@carbondesignsystems.com
#
#!! Description:
#!! 
#!! A class to hold common data used by QA to coordinate
#!! data between multiple scripts.
#---------------------------------------------------------
package CarbonQAData;

use lib "$ENV{CARBON_HOME}/scripts";
use Versions;
use CDSSystem;

#---------------------------------------------
#!! Constructor
#---------------------------------------------
sub new  # ()
{
    my $this = {};
    bless $this, @_[0];

    $this->Initialize();
    $this->{vInfo} = new Versions($this->{fileNames});

    return $this;
}

sub IntelLicenseFilePath
{
    return "/tools/linux/intel/compiler80/licenses";
}

#---------------------------------------------
#!! Initialize variables.
#---------------------------------------------
sub Initialize  # ()
{
    my $this = shift;
}

#---------------------------------------------
#!! Returns valid product platform architecture.
#---------------------------------------------
sub ValidPlatforms #()
{
    my @arch = ("Linux","Linux64","Win");

    return \@arch;
}

#---------------------------------------------
#!! Returns the list of version files that we 
#!! need to touch before making a new tag.
#---------------------------------------------
sub VersionChangeFileList
{
    @changeVersionFiles = ("src/compiler_driver/CarbonContext.cxx",
                           "src/inc/util/CarbonVersion.h",
                           "doc/ascii-release-files/README",
                           "userdoc/ascii-release-files/README");

    return @changeVersionFiles;;
}

#-------------------------------------------------------
#!! Returns the list of people that will get mail when a 
#!! release process starts.
#-------------------------------------------------------
sub ReleaseStartMailList
{
    my @notificationList = ("qa\@carbondesignsystems.com",
                            "trathje\@carbondesignsystems.com",
                            "joe\@carbondesignsystems.com",
                            "cloutier\@carbondesignsystems.com",
                            "tonacki\@carbondesignsystems.com",
                            "sam\@carbondesignsystems.com",
                            "ameier\@carbondesignsystems.com");

    return @notificationList;
}

#-------------------------------------------------------
#!! Returns the list of cvs modules we want to check out
#!! and possibly tag during the release process. 
#-------------------------------------------------------
sub CheckoutModuleList
{
    my $this = shift;

    my $mods = "src scripts examples userdoc deployment test ".
        $this->SystemCDirName().
        " app_notes training doc";

    return $mods;
}

#-------------------------------------------------------
#!! We did not checkin the latest systemc version over the 
#!! existing systemc cvs modules.  
#!! We made a new systemc-2.2 repository 
#!! module.  If this continues to be the trend, we'll
#!! need to handle changing repository modules strings 
#!! for systemc.
# 
#!! Returns the filename for systemc.  
#-------------------------------------------------------
sub SystemCDirName
{
    my $this = shift;
    my $dir = "systemc-" . $this->{vInfo}->Version("systemc");

    return $dir;
}



1;
