package CDSAudit;

use File::Path;
use FileHandle;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSHosts;
use CDSSystem;

sub new
{
    my $this = {};
    bless $this, @_[0];
    $this->{auditFile} = @_[1];
    $this->{hostInfo} = new CDSHosts();
    $this->{host} = $this->{hostInfo}->LocalHostName();

    return $this;
}

sub ClassType
{
    my $this = shift;
    return ref($this);
}
sub AuditFileName
{
    my $this = shift;
    return $this->{auditFile};
}

sub ClearLog
{
    my $this = shift;
    my $rtn = 2;

    if (-f $this->{auditFile})
    {
	unlink ($this->{auditFile});
    }

    if (! -f $this->{auditFile})
    {
	$rtn = 0;
    }
    
    return $rtn;
}

sub Log
{
    my $this     = shift;
    my $message  = shift;
    my $file     = $this->{auditFile};
    my $fileName = "";
    ($fileName   = $file) =~ s/^.+\///g;
    my $host     = $this->{host};
    my $dir      = "";

    ($dir = $file) =~ s/\/$fileName$//;

    if ($dir && ! -d $dir)
    {
        mkpath ($dir);
    }

    my $wh = CDSSystem::OpenAppend($file, $this->ClassType());

    chomp (my $date = `date`);
    my $line = join "", $host, ": $date:\n";

    my @msg = split " ", $message;

    for (my $i = 0; $i <= $#msg; $i++)
    {
	my $m = "\t";
	my $maxLength = 60;
	my $lineLength = 0;

	while ($lineLength < $maxLength)
	{
	    my $tmpM = ($m .= $msg[$i]);
	    $lineLength = length($tmpM);

	    if ($lineLength < $maxLength)
	    {
		$m = "$tmpM ";
		$i++;
	    }
	}
	$line .= "$m\n";
    }

    print $wh $line;
    write "$file";
    close ($wh);

    if ($ENV{QA_DEBUG} == 1)
    {
	print STDERR $line;
    }
}



1;
