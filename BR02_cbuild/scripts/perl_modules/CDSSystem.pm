#==================================================================
# Author: Dave Allaby.  
# dallaby@carbondesignsystems.com
#==================================================================
#
# Utility module to wrap calls to the system and produce messaging
# to a file handle reference.  The FileHandle that is passed to these
# functions needs to be opened prior to being passed, write() and
# closed after exiting.  This does not open files, call perl's
# write "$file" or close filehandle.
# 
# Also contains a fork routine to fork system calls under the calling
# script's name and may or may not return a child pid to the caller.
# in this way we can track the children from the parent easily.
#
# The intent is that these functions are called like a static 
# class.  This is not new()d.
#==================================================================

package CDSSystem;

use strict;
use FileHandle;
use Cwd;
use File::Path;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CmdPaths;

#---------------------------------------------------------
#!! Gzips a file, warns if there was bad status.
#---------------------------------------------------------
sub GzipFile #(string filename)
{
    my $zipFile    = shift;
    my $cmds       = new CmdPaths();
    my $gzipCmd    = $cmds->Command("gzip")." ".$zipFile;
    my $gzipStatus = CDSSystem::System ($gzipCmd);

    if ($gzipStatus)
    {
        print "WARNING: Caught bad exit status on $gzipCmd [$gzipStatus]\n";
    }

    return $gzipStatus;
}

#---------------------------------------------------------
#!! Does a mkdir -p for directories.  Can pass either a 
#!! directory string or a reference to an array of directories.
#!! Second arg is the usual unix 3 digit chmod code.
#!! Permissions default to 755.
#!! Returns 1 if success, 0 if failed.
#---------------------------------------------------------
sub Mkdir #(( &directory_array || string directory_path ),int permissions)
{         
    my ($dir, $perm) = (@_);
    my $rtn          = 1;

    if (! $perm)
    {
        $perm = "755";
    }

    if (ref($dir) =~ /ARRAY/)
    {
        if ($ENV{QA_DEBUG} == 1)
        {
            print "Mkdir() ", @{$dir}, "\n";
        }

        foreach (@{$dir})
        {
            if (! mkpath ($_, $ENV{QA_DEBUG}))
            {
                print STDERR  "CDSSystem::Mkdir(), Could not mkpath $_ with ",
                "permissions of $perm $!\n";
                $rtn = 0;
            }
            else
            {
                chmod oct($perm), $_;
            }   
        }
    }
    else
    {
        if ($ENV{QA_DEBUG} == 1)
        {
            print "Mkdir() ", $dir, "\n";
        }

        if (! mkpath ($dir, $ENV{QA_DEBUG}, oct($perm)))
        {
            print STDERR "CDSSystem::Mkdir(), Could not mkpath $_ with ",
            "permissions of $! $perm\n";
            $rtn = 0;
        }
        else
        {
            chmod oct($perm), $dir;
        }
    }

    return $rtn;
}

sub WriteClose
{
    my ($file, $fh, $perm) = (@_);

    write "$file";
    close($fh);

    if (! $perm)
    {
        $perm = 755;
    }

    select STDOUT;

    return (chmod oct($perm), $file);
}

sub OpenDir
{
    my ($dir, $grepStr, $caller) = (@_);
    my $dh = "";
    my @ls = ();

    if (! $caller)
    {
	$caller = "CDSSystem";
    }
    if (-d $dir)
    {
	my $here = cwd();
	Cd($dir);
	$dh = new FileHandle;
	opendir($dh, $dir);
	
	if ($dh)
	{
	    my @out = readdir($dh);

	    foreach my $f (@out)
	    {
		if ($f !~ /^(\.|\.\.)$/)
		{
		    if (($grepStr && $f =~ /$grepStr/) ||
			! $grepStr)
		    {
			push (@ls, $f);
		    }
		}
	    }
	}

	closedir ($dh);
	Cd($here);
    }
    else
    {
	print STDERR $caller, ": $dir: not found\n";
    }

    return @ls;
}

sub OpenRead
{
    my ($file, $caller, $extraMsg) = (@_);
    my $rh = "";

    if (! $caller)
    {
	$caller = "CDSSystem";
    }

    if ($file && -f "$file")
    {
	$rh = new FileHandle;
	open ($rh, "<$file") || die "$caller: Cannot open $file for reading\n",
	" - $!\n", $extraMsg, "\n";
    }
    elsif ($file && -d "$file")
    {
	print STDERR "$caller: File $file is a directory and not file readable\n";
    }
    elsif (! $file)
    {
	print STDERR "Passed null file name to OpenRead()\n";
    }
    else
    {
	print STDERR "File $file does not exist\n";
    }
    
    return $rh;
}

sub OpenAppend
{
    my ($file, $caller, $extraMsg, $overwrite) = (@_);
    my $wh = "";

    if (! $caller)
    {
	$caller = "CDSSystem";
    }

    if ($file && -f $file && $overwrite)
    {
	unlink "$file";
    }

    if ($file)
    {
	$wh = new FileHandle;
	open ($wh, ">>$file") || die "$caller: Cannot open $file for writing\n",
	" - $!\n",
	$extraMsg, "\n";
    }
    else
    {
	print STDERR "$caller: Write file name is undefined. Cannot continue.\n";
    }

    return $wh;
}

sub OpenWrite
{
    my ($file, $caller, $extraMsg) = (@_);

    if (! $caller)
    {
	$caller = "CDSSystem";
    }

    my $wh = CDSSystem::OpenAppend ($file, $caller, $extraMsg, 1);

    return $wh;
}

sub Cd
{
    my ($here, $caller, $extraMsg) = (@_);
    my $ok = 0;

    if (! $caller)
    {
	$caller = "CDSSystem";
    }

    if ($ENV{QA_DEBUG})
    {
        print STDERR "Cd(): Change directory to $here\n";
    }

# Changing the messaging in the next conditionals could affect test/perl_modules/CDSSystem
# tests.  Please verify grep strings match.

    if ($here && -d "$here")
    {
	$ok = chdir($here);
    }
    elsif (! $here)
    {
	print STDERR "$caller: Could not cd, directory path arguments not defined.\n",
	$extraMsg, "\n";
    }
    else
    {
	print STDERR "$caller: Could not cd to $here, directory does not exist.\n",
	$extraMsg, "\n";
    }

    return $ok;
}

sub WriteOut
{
    my ($msg, $lref) = (@_);

    if ($ENV{QA_DEBUG})
    {
        if ($lref)
        {
            print $lref "$msg\n";
        }
        else
        {
            print STDERR "$msg\n";
        }	
    }
}

#-----------------------------------------------------------------
# Amounts to a backticked command with messaging around it, we can
# also easily get $status from the command itself and log that.
#-----------------------------------------------------------------
sub BackTick
{
    my ($cmd, $logRef) = (@_);
    my $out = "";
    local $SIG{CHLD} = "";

    WriteOut ("EXECUTING: $cmd", $logRef);

    $out = `$cmd`;

    my $rtn = $?;

    WriteOut ("Command returned status ->[$rtn]\n", $logRef);

    return $out;
}

#-----------------------------------------------------------------
# Amounts to a system() call with messaging around it.  This
# also turns on SIG{CHLD} (because a caller may have it as being ignored.
# This is so we can get $status from the shell.
# 
# It also divides by 256 so the returned value matches what a shell
# script would return.
#-----------------------------------------------------------------
sub System
{
    my ($cmd, $logRef) = (@_);
    local $SIG{CHLD} = "";

    WriteOut ("EXECUTING: $cmd", $logRef);
    
    my $rtn = system ($cmd);

    if ($rtn >= 256)
    {
        $rtn /= 256;
    }

    WriteOut ("Command returned status ->[$rtn]", $logRef);

    return $rtn;
}

#------------------------------------------------------------------------
# This routine is a fork with some features around it.
#
# 1. You can pass a function ref to it, and it forks another process
#    which will exit after the function code is executed.
# OR
#
# 2. You can execute a system command.
#
#  In either case, you can choose to get the child pid back if you want
#  this makes it easy to waitpid().  You could fork a bunch things and
#  waitpid for them without having to look directly at process table
#  output, or the /proc directory.
# 
#------------------------------------------------------------------------

sub ForkProcess
{
    my ($func, $rtnSt,$a1, $a2, $a3, $a4, $a5, $a6, $a7, $a8, $a9) = (@_);
    my $pid = 0;

    FORK:
    {
        if ($pid = fork)
        {
        }
        elsif (defined ($pid))
        {
            my $refString = ref($func);
	    my $rtn = 0;

            if ($refString)
            {
                \&$func($a1,$a2,$a3,$a4,$a5,$a6,$a7,$a8,$a9);
            }
            else
            {
                $rtn = CDSSystem::System($func);
            }

            exit $rtn;
        }
        elsif ($! =~ /No more processes/)
        {
            sleep 5;
            redo FORK;
        }
        else
        {
            die "Can't fork $func $!\n";
        }
    }

    if (! $rtnSt)
    {
        $pid = "";
    }

    return $pid;
}


1;
