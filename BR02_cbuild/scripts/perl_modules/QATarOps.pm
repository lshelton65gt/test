#-------------------------------------------------------------
# QATarOps class.
# Author Dave Allaby
#
# Description: Class which understands the common and frequently
# used packaging methods.
#-------------------------------------------------------------

package QATarOps;

use strict;
use Cwd;
use File::Basename;
use File::Path;

use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSFileNames;
use CDSSystem;
use CDSHosts;

my $FAIL = 0;
my $PASS = 1;

sub new
{
    my $this = {};
    bless $this, @_[0];
    my $hInfo = @_[1];
    my $fn = @_[2];

# Need host info, transfer hosts are there.
#-----------------------------------------
    if (! $hInfo)
    {
	if ($fn && $fn->CdsHosts())
	{
	    $this->{hostInfo} = $fn->CdsHosts();
	}
	else
	{
	    $this->{hostInfo} = new CDSHosts();
	}
    }
    else
    {
        $this->{hostInfo} = $hInfo;
    }
    
# We need filenames for paths.
#-----------------------------
    if (! $fn)
    {
        $this->{fileNames} = new CDSFileNames($hInfo);
    }
    else
    {
        $this->{fileNames} = $fn;
    }

    return $this;
}

#---------------------------------------------------
# Returns class name
#---------------------------------------------------
sub ClassType
{
    my $this = shift;
    return ref($this);
}

sub TarSystemC
{
    my $this = shift;
    my $excludeFile = shift;
    my $tgz = shift;
    my $tarOpts = "-cz";
    my $rtn = -1;

    if ($ENV{QA_DEBUG})
    {
        $tarOpts .= "v";
    }
    
    my $cmd = join " ", $this->{fileNames}->Command("tar"),
    $tarOpts, "-X", $excludeFile, " . -f", $tgz;
    
    $rtn = CDSSystem::System($cmd);
    
    if ($rtn == 0 && -f $tgz && ! -z $tgz)
    {
        $rtn = 1;
    }

    return $rtn;
}


#---------------------------------------------------
# Takes a path to a tar package.
# Options are optional.
# extraction dir is optional.  If omitted, it will
# be unpacked where it is.  Caution: If it was not
# tarred elegantly from a single directory
# you might totally pollute the location.
#---------------------------------------------------
sub Untar
{
    my $this       = @_[0];
    my $tarball    = @_[1];
    my $opts       = @_[2];
    my $extractDir = @_[3];
    my $here       = cwd();
    my $rtn        = $FAIL;

    if (! $opts)
    {
	$opts = "xfz";
    }

# QA_DEBUG turns on verbose to the tar command.

    if ($ENV{QA_DEBUG} && $opts !~ /v/)
    {
	$opts = "v" . $opts;
    }

    if ($tarball && -f $tarball)
    {
	if ($extractDir)
	{
	    if (! -d $extractDir)
	    {
		mkpath ($extractDir);
	    }

	    $rtn = CDSSystem::Cd($extractDir)
	}
	else
	{
	    $rtn = $PASS;
	}
	
	if ($rtn == $PASS)
	{
	    my $cmd = join " ", $this->{fileNames}->Command("tar"),
            $opts, $tarball;

            $rtn = CDSSystem::System($cmd);

            if ($rtn == 0)
            {
                ($rtn = $tarball) =~ s/\.t(ar|gz)$//;
            }
	}
    }
    elsif (! $tarball)
    {
	print STDERR $this->ClassType(), ": Tarball file undef!!!\n";
    }
    else
    {
	print STDERR $this->ClassType(), 
	": Tarball file $tarball is not found!!!\n";
    }

    return $rtn;
}

#---------------------------------------------------
# Tars a directory,possibly zips if the $opts contain
# the "z" char.  Extention will match the opts.
#---------------------------------------------------
sub Tar
{
    my $this     = @_[0];
    my $tarThing = @_[1];
    my $opts     = @_[2];
    my $rtn      = $FAIL;

    if ($tarThing && -e $tarThing)
    {
	my $dir      = dirname($tarThing);
	my $fileName = basename($tarThing);
	my $here     = cwd();
	my $tarball  = $fileName . ".tar";

	if (! $opts)
	{
	    $opts = "cfz";
	}

# QA_DEBUG turns on verbose switch if it's not on.
#------------------------------------------------
	if ($ENV{QA_DEBUG} && $opts !~ /v/)
	{
	    $opts = "v". $opts;
	}
	    
	if ($opts =~ /z/)
	{
	    $tarball =~ s/\.tar/\.tgz/;
	}

	if (CDSSystem::Cd($dir), $this->ClassType())
	{
	    my $cmd = join " ", $this->{fileNames}->Command("tar"),
	    $opts, $tarball, $fileName;

	    $rtn = CDSSystem::System($cmd);

	    CDSSystem::Cd($here, $this->ClassType());

	    if ($rtn == 0 && -e "$dir/$tarball")
	    {
		$rtn = "$dir/$tarball";
	    }
	}
    }
    elsif (! $tarThing)
    {
	print STDERR $this->ClassType(), 
	": Tar area path is not set to anything ->$tarThing\n";
    }
    else
    {
	print STDERR $this->ClassType(), 
	": Cannot find file $tarThing\n";
    }

    return $rtn;
}

#----------------------------------------------------
# Not yet functional
#----------------------------------------------------
sub TarThruStdout
{
    my $this = @_[0];
    my $tarFrom = @_[1];
    my $tarTo = @_[2];
    my $opts = @_[3];

    if (! $opts)
    {
	$opts = "xfz";
    }

# QA_DEBUG turns on verbose to the tar command.

    if ($ENV{QA_DEBUG} && $opts !~ /v/)
    {
	$opts = "v" . $opts;
    }
    my $rtn = 2;

    my $tarCmd = $this->{fileNames}->Command("tar");
    my $cmd = $tarCmd . " chf - $tarFrom | ( cd $tarTo && $tarCmd xf -)";
    $rtn = CDSSystem::System ($cmd);

    return $rtn;
}


1;
