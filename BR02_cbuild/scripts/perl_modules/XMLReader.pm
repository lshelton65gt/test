#================================================================
#
# Class XMLReader
# Author Dave Allaby.  dallaby@carbondesignsystems.com
#
#
# Description: This gives a base class to any other class that needs to
# read an xml formatted file.
# It allows a calling class to execute it's own function calls using the
# class' $this reference as a pointer to the parsing routines.
#
# To make use of this class, you must do 2 things.
# 
# 1. This must be called from a class and the self reference
#    has to be passed to ParseFile().
#
# 2. The routine names such as "ElementStart", "ElementChar" and "ElementEnd"
#    which are passed to the actual xmlparser are hard coded here and 
#    cannot be changed!!!!!  
#    You need to call your methods the same name so this class will
#    be able to pass the reference correctly.
#
#--------------------------------------------------------------------

package XMLReader;

use strict;
use Data::Dumper;
use XML::Checker::Parser;

sub new
{
    my $this = {};
    bless $this,@_[0];
    $this->{xmlFile}            = @_[1];

    return $this;

}

sub ParseFile
{
    my $this = shift;
    my $caller = shift;

    if ($caller)
    {
	$this->{xmlParser} = new XML::Checker::Parser
	    (Handlers => 
	     {Start => sub {return \$caller->ElementStart(@_)},
	      Char =>  sub {return \$caller->ElementChar(@_)},
	      End =>   sub {return \$caller->ElementEnd(@_)}},
	     ParseParamEnt => 1, SkipExternalDTD => 1,
	     SkipInsignifWS => 1 );
	
	$XML::Checker::FAIL = \\&ParseFailure;
	
	$this->{xmlParser}->parsefile($this->{xmlFile});
    }
}

sub ParseFailure
{
    my $this = shift;
    print STDERR "Could not parse file ", $this->{xmlFile}, "\n";

    exit 2;
}

1;
