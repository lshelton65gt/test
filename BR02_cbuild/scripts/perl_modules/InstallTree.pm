package InstallTree;

use File::Basename;
use File::Path;
use File::Find;
use Cwd;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSFileNames;
use CDSAudit;
use QATarOps;
use CDSInstallJammer;

sub new
{
    my $this            = {};
    bless $this, @_[0];
    $this->{fileNames}  = @_[1];
    $this->{logger}     = @_[2];
    $this->{logger}->ClearLog();
    $this->{platform}   = @_[3];
    my %supported       = ();
    $this->{supported}  = \%supported;
    $this->{tarOps}     = new QATarOps();
    $this->{installDir} = $this->{fileNames}->InstallTreePath();
    $this->{carbonHome}  = dirname($this->{installDir});

    $this->{sandbox} = $this->{fileNames}->FindSandboxPath();

    if (-d $this->{sandbox} && -d $this->{carbonHome} 
	&& $this->{carbonHome} ne $this->{sandbox})
    {
	if ($this->{platform})
	{
	    $supported{$this->{platform}} = 1;
	}
	else
	{
	    $supported{Linux} = 1;
	    $supported{Linux64} = 1;
	    $supported{SunOS} = 1;
	    $supported{Win} = 1;
	}
    }
    else
    {
	print STDERR "Sandbox is ", $this->{sandbox}, " ", 
	"Carbon home is ", $this->{carbonHome};
	$this = "";
    }
    
    return $this;
}

sub Create
{
    my $this = shift;
    my $msg = "Creating installation tree at ", $this->{installDir};
    $this->{logger}->Log($msg);


    if (! -d $this->{installDir})
    {
	mkpath ($this->{installDir});
    }

    $rtn = $this->InstallTree();

    $rtn = $this->InstallExamples();
    
    $rtn = $this->MakeSystemCDist();

    $rtn = $this->PruneTree();

    if ($this->{supported}{Win})
    {
	$this->AdjustWindowsLibExt();
    }

    return $rtn;
}

sub CreateInstallJammer
{
    my $this       = shift;
    my $installMkr = new CDSInstallJammer($this->{fileNames});
    my $rtn        = $installmkr->Build(); # No args defaults to usual release paths

    return $rtn;
}

sub AdjustWindowsLibExt
{
    my $this = shift;
    my $winDir = join "/", $this->{installDir}, "Win";
    $this->{logger}->Log("Modifying .a libs to windows .lib extensions");
    find ($this->ModifyWinLibName(), $winDir);
}

sub ModifyWinLibName
{
    my $this = shift;
    my $l = $File::Find::name;

    if (-f $l && $l =~ /\.a$/)
    {
	my $wl = "";
	($wl = $l) =~ s/\.a$/\.lib/;
	my $msg = "Renaming $l to $wl...";
	$this->{logger}->Log($msg);
	rename ($l, $wl);
    }
}

sub PruneTree
{
    my $this = shift;
    my $rtn = 2;
    my $execBom = $this->{fileNames}->BomFile("execs");
    my $usrExecBom = $this->{fileNames}->BomFile("usrexecs");
    my @pruneDirs = ();
    my $here = cwd();
    my $fmsg = "";

    $this->{logger}->Log("Pruning installation tree ...");

    if (CDSSystem::Cd($this->{installDir}))
    {
	my $cmpCmd = join "", $this->{fileNames}->Command("comm"),
	
	my @compare = 
	$rtn = 0;
	$this->{logger}->Log("Processing exemption file $execBom...");
	my $rh = CDSSystem::OpenRead($execBom, $this->ClassType());
	
	while (<$rh>)
	{
	    chomp;
	    if ($_ !~ /^\#/ && $_ ne "")
	    {
		my $exec = basename ($_);
		push (@pruneDirs,$exec);
	    }
	}
	
	close ($rh);
	
	foreach my $exec (@pruneDirs)
	{
	    if (-e "bin/$exec")
	    {
		unlink "bin/$exec";

		foreach my $arch (keys %{$this->{supported}})
		{
		    if (-e "$arch/bin/$exec")
		    {
			$fm .= "Removing $arch/bin/$exec ";
			unlink "$arch/bin/$exec";
		    }

		    if ($arch eq "Linux64" && 
			-e "$arch/bin/perl" && 
			! -l "$arch/bin/perl")
		    {
			$fm .= "Removing $arch/bin/$exec ";
			unlink "$arch/bin/perl";
			$fm .= "Linking $arch/bin/perl to../../Linux/bin/perl ";
			symlink ("$arch/bin/perl", "../../Linux/bin/perl");
		    }
		}
	    }
	}

	if ($fm)
	{
	    $this->{logger}->Log($fm);
	}
    }

    CDSSystem::Cd($here);

    return $rtn;
}

sub MakeSystemCDist
{
    my $this         = shift;
    my $sysCPkgr = new SystemCUtil($this->{installDir});
    
    my $rtn = $sysCPkgr->MakeSystemCDist();

    return $rtn;
}

sub InstallExamples
{
    my $this = shift;
    my $installExampleDir = join "/", $this->{installDir}, "examples";
    my $cmd  = join "", $this->{fileNames}->Command("cp"),
    " -r ", $this->{sandbox}, "/examples ", $this->{installDir};

    $this->{logger}->Log("Installing examples to installation tree...");

    if (-e "$installExampleDir")
    {
	if (-l "$installExampleDir")
	{
	    unlink ($installExampleDir);
	}
	else
	{
	    rmtree ($installExampleDir);
	}
    }

    my $rtn = CDSSystem::System($cmd);

# Should put a `du` check on these.x    

    return $rtn;
}

sub TarDirsFollowLinks
{
    my $this       = shift;
    my @installDir = ("bin","include","lib","makefiles","help");
    my @subdirs    = ("bin","lib","python");
    my $rtn        = 2;

    if (CDSSystem::Cd($this->{carbonHome}))
    {
	foreach my $plat (keys %{$this->{supported}})
	{
	    foreach (@subdirs)
	    {
		if (-e "$plat/$_")
		{
		    push (@installDir,"$plat/$_");
		}
	    }
	}
	
	$this->{logger}->Log("Installing directories and following the links...");

	foreach my $dir (@installDir)
	{
	    my $msg = join "", "Tarring ", $this->{carbonHome}, 
	    "/$dir to installation tree ", $this->{installDir}, "\n",
	    " Following links.\n";

	    $this->{logger}->Log($msg);
	    $rtn = $this->{tarOps}->TarThruStdout ($dir, $this->{installDir},
						   "chf");
	}
    }
}

sub TarDirsNoFollowLinks
{
    my $this = shift;
    my $rtn = 2;
    my @installDir = ();
    my @subdirs = ("gcc343", "winx");

    foreach my $arch (keys %{$this->{supported}})
    {
	foreach (@subdirs)
	{
	    if (-e "$arch/$_")
	    {
		push (@installDir, "$arch/$_");
	    }
	}
    }

    if (CDSSystem::Cd($ENV{CARBON_HOME}))
    {
	foreach my $dir (@installDir)
	{
	    my $msg = join "", "Tarring ", $this->{carbonHome}, 
	    "/$dir to installation tree ", $this->{installDir}, "\n",
	    " Not following links\n";

	    $this->{logger}->Log($msg);
	    $rtn = $this->{tarOps}->TarThruStdout ($dir, $this->{installDir},
						   "cf");
	}
    }

    return $rtn;
}

sub InstallTree
{
    my $this = shift;

    my $rtn = $this->TarDirsFollowLinks();

    $rtn = $this->TarDirsNoFollowLinks();

    return $rtn;
}

sub ClassType
{
    my $this = shift;
    return ref($this);
}


1;
