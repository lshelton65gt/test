
#------------------------------------------------------------
# This is a class which is new()d.  It takes the name of the 
# caller script as an argument.
# This class requires another class CDSUsage;  In this required
# CDSUsage live all the definitions for every possible option
# for any script that uses this class for getopts.
#
# This is a sane way to try to unify a common set of arguments
# that most things use, while still supporting the current argument
# usage in existing scripts.  Old style options will map to these
# methods as well as new and common options.  A user can use the
# same options for runlist as runsingle as runinplace, etc, etc,
# because the various flavors of options map to one value which
# are returned by a single method
#
# Scripts should always use the method to return the mode.
# Example: if ($args->Debug() == 1)
# not
# if ($Debug == 1) 
# and NEVER CDSArgs::$Debug
# Calling the method in a static fashion will always fail.
# Data is held in $this pointer.  Without it, nothing is ever
# set.
#-----------------------------------------------------------
#
# Still to do:
# Allow for a qa db reader class which can read a qadatabase
# flat file, which represents a script session.  The reader would
# be used to reset this class' arguments again in the event of a
# restart.
#-----------------------------------------------------------

package CDSArgs;

use strict;
use CDSFileNames;     # This gets filenames and paths
use CDSUsage;

sub new
{
    my $this = {};
    bless ($this, @_[0]);
    my $misParsed = 0;

    $this->{scriptName} = @_[1];

# We always want just the filename, not
# the path.  We need the filename to map
# to a key in the %select hash in CDSUsage.
# This is how we select which options will
# print out for each script when someone runs
# the scripts with --help.

    if ($this->{scriptName} =~ /\//)
    {
	$this->{scriptName} =~ s/^.\///g;
    }

    $this->{qaTasks} = @_[2];

    my $misparsed = $this->Initialize();

    if ($misparsed)
    {
	$this = "";
    }
    return $this;
}


sub Initialize
{
    my $this = @_[0];
    my $err = &CDSUsage::VerifyArgs($this->{scriptName}, @ARGV);

    if (! $err)
    {
	$this->Clear();
	$this->{fileNames} = new CDSFileNames();
	
	if (! $this->{qaTasks})
	{
	    $err = $this->ParseCommandLine();
	}
	else
	{
	    $this->{qaTasks}->ResetArgs($this);
	    if (! $this)
	    {
		$err = 1;
	    }
	}
	
	if ($this->{help} == 1)
	{
	    $err = 1;
	    $this->PrintHelp();
	}
	else
	{
	    if (! $err && $this)
	    {
		$err = $this->SmoothSettings();
	    }
	}
    }

    return $err;
}

sub CdsFileNames
{
    my $this = shift;
    return $this->{fileNames};
}

sub Clear
{
    my $this = shift;

    foreach (@ARGV)
    {
	$this->{cmdline} .= "$_ ";
    }

# Supported command line testgroups, can be specified
# without the -testgroup prefix option.

    my %tg                          = ();
    $this->{testgroup}              = \%tg;
    $this->{testgroup}{allvariants} = 0;
    $this->{testgroup}{allsmall}    = 0;
    $this->{testgroup}{almostall}   = 0;
    $this->{testgroup}{checkin}     = 0;
    $this->{testgroup}{cosim}       = 0;
    $this->{testgroup}{custall}     = 0;
    $this->{testgroup}{custbig}     = 0;
    $this->{testgroup}{custsmall}   = 0;
    $this->{testgroup}{custhot}     = 0;
    $this->{testgroup}{mindspeed}   = 0;
    $this->{testgroup}{nightly}     = 0;
    $this->{testgroup}{unit_test}   = 0;
    $this->{testgroup}{verilog}     = 0;
    $this->{testgroup}{vhdl}        = 0;
    $this->{testgroup}{win}         = 0;

    $this->{bom}            = 0;
    $this->{builddebug}     = 0;
    $this->{cdb}            = 0;
    $this->{checkgolds}     = 0;
    $this->{copylogs}       = 0;
    $this->{core}           = 0;
    $this->{cosim}          = 0;
    $this->{debug}          = 0;
    $this->{docs}           = 0;
    $this->{downserver}     = 0;    
    $this->{failrerun}      = 0;
    $this->{help}           = 0;
    $this->{installer}      = 0;
    $this->{interactive}    = 0;
    $this->{latenight}      = 0;
    $this->{lowpri}         = 0;
    $this->{maillist}       = 0;           
    $this->{nocleanup}      = 0;
    $this->{nodoc}          = 0;
    $this->{noexecenv}      = 0;
    $this->{noicc}          = 0;
    $this->{nokillclean}    = 0;
    $this->{nolinktree}     = 0;
    $this->{nolinux}        = 0;
    $this->{nolinux64}      = 0;
    $this->{nosunos}        = 0;
    $this->{nowindows}      = 0;
    $this->{nolm}           = 0;
    $this->{nocarbonconfig} = 0;
    $this->{noinstalltest}  = 0;
    $this->{nosge}          = 0;
    $this->{noshortcircuit} = 0;
    $this->{overwrite}      = 0;
    $this->{patch}          = 0;
    $this->{performance}    = 0;
    $this->{quick}          = 0;
    $this->{relpackage}     = 0;
    $this->{reltest}        = 0;
    $this->{rundir}         = 0;
    $this->{runtests}       = 0;
    $this->{saveruninfo}    = 0;
    $this->{setuponly}      = 0;
    $this->{testGroupIn}    = 0;
    $this->{testversions}   = 0;
    $this->{update}         = 0;
    $this->{oldvectors}     = 0;
    $this->{variant}        = 0;
    $this->{verbose}        = 0;
    $this->{vspcompiler}    = 0;
    $this->{winx}           = 0;
}

sub ParseCommandLine
{
    my $this = shift;
    my $err = 0;

    use Getopt::Long;
    
    GetOptions ('allvariants'    =>   \$this->{testgroup}{allvariants},
		'allsmall'       =>   \$this->{testgroup}{allsmall},
		'almostall'      =>   \$this->{testgroup}{almostall},
		'bom=s'          =>   \$this->{bom},
		'builddebug'     =>   \$this->{builddebug},
		'cdb'            =>   \$this->{cdb},
		'CDB'            =>   \$this->{cdb},
		'checkin'        =>   \$this->{testgroup}{checkin},
		'checkgolds'     =>   \$this->{checkgolds},
		'copylogs'       =>   \$this->{copylogs},
		'core'           =>   \$this->{core},
		'cosim'          =>   \$this->{testgroup}{cosim},
		'custall'        =>   \$this->{testgroup}{custall},
		'custbig'        =>   \$this->{testgroup}{custbig},
		'custsmall'      =>   \$this->{testgroup}{custsmall},
		'custhot'        =>   \$this->{testgroup}{custhot}, 
		'd=s'            =>   \$this->{rundir},
		'docs=s'         =>   \$this->{docs},
		'rundir=s'       =>   \$this->{rundir},
		'debug'          =>   \$this->{debug},
		'downserver'     =>   \$this->{downserver},
		'failrerun'      =>   \$this->{failrerun},
		'help'           =>   \$this->{help},
		'installer'      =>   \$this->{installer},
		'interactive'    =>   \$this->{interactive},
		'latenight'      =>   \$this->{latenight},
		'lowpri'         =>   \$this->{lowpri},
		'mail=s'         =>   \$this->{maillist},
		'maillist=s'     =>   \$this->{maillist},
		'mindspeed'      =>   \$this->{testgroup}{mindspeed},
		'nightly'        =>   \$this->{testgroup}{nightly},
		'nocarbonconfig' =>   \$this->{nocarbonconfig},
		'nocleanup'      =>   \$this->{nocleanup},
		'noconfig'       =>   \$this->{nocarbonconfig},
		'nodoc'          =>   \$this->{nodoc},
		'noexecenv'      =>   \$this->{noexecenv},
		'nofarm'         =>   \$this->{nosge},
		'nogrid'         =>   \$this->{nosge},
		'noicc'          =>   \$this->{noicc},
		'nokillclean'    =>   \$this->{nokillclean},
		'noinstalltest'  =>   \$this->{noinstalltest},
		'nolinktree'     =>   \$this->{nolinktree},
		'nolinux'        =>   \$this->{nolinux},
		'nolinux64'      =>   \$this->{nolinux64},
		'nolm'           =>   \$this->{nolm},
		'noreleasttest'  =>   \$this->{noinstalltest},
		'noshortcircuit' =>   \$this->{noshortcircuit},
		'nosunos'        =>   \$this->{nosunos},
		'nowindows'      =>   \$this->{nowindows},
		'o'              =>   \$this->{overwrite},
		'oldvectors'     =>   \$this->{oldvectors},
		'overwrite'      =>   \$this->{overwrite},
		'patch=s'        =>   \$this->{patch},
		'performance'    =>   \$this->{performance},
		'putontop'       =>   \$this->{putontop},
		'quick'          =>   \$this->{quick}, 
		'reltest'        =>   \$this->{reltest},
		'saveruninfo'    =>   \$this->{saveruninfo},
		'setuponly'      =>   \$this->{setuponly},
		'relpackage'     =>   \$this->{relpackage},
		'rerunfails'     =>   \$this->{failrerun},
		'runninglist'    =>   \$this->{runninglist},  
		'runtests'       =>   \$this->{runtests},
		'tag'            =>   \$this->{tag},
		'testgroup=s'    =>   \$this->{testGroupIn},
		'testversions'   =>   \$this->{testversions},
		'update'         =>   \$this->{update},
		'unit_test'      =>   \$this->{testgroup}{unit_test}, 
		'valgrind'       =>   \$this->{valgrind},
		'variant=s'      =>   \$this->{variant},
		'verbose'        =>   \$this->{verbose},
		'verilog'        =>   \$this->{testgroup}{verilog},
		'vhdl'           =>   \$this->{testgroup}{vhdl},
		'vspcompiler'    =>   \$this->{vspcompiler},
		'win=s'          =>   \$this->{testgroup}{win},
		'winx'           =>   \$this->{winx},
		'xactors'        =>   \$this->{testgroup}{xactors});

    return $Getopt::Long::error;
}

sub SmoothSettings
{
    my $this = shift;
    my $err = 0;

    $this->SortTestGroups();

    if ($ARGV[0] && $ARGV[1])
    {
	$err = 2;
	print STDERR "EXTRA arguments found, @ARGV\n";
    }
    else
    {
	if ($ARGV[0])
	{
	    if (! $this->{filearg})
	    {
		$this->{filearg} = $ARGV[0];
		
		my $type = $this->FileArg();
		
		if ($type eq "UNKNOWN")
		{
		    $err = 3;
		}
	    }
	}
    }

    return $err;
}

sub SortTestGroups
{
    my $this = shift;
    my @grp = ();

    if ($this->{testGroupIn})
    {
	if ($this->{testGroupIn} =~ /\,/)
	{
	    @grp = split /\,/, $this->{testGroupIn};
	}
	else
	{
	    @grp = ($this->{testGroupIn});
	}
	$this->{testGroupIn} = "";

	foreach (@grp)
	{
	    if ($_ ne "")
	    {
		$this->{testgroup}{$_} = 1;
	    }
	}
    }

    foreach (keys %{$this->{testgroup}})
    {
	if (! $this->{testgroup}{$_})
	{
	    delete $this->{testgroup}{$_};
	}
    }
    
}

sub BomfileName
{
    my $this = shift;
    return $this->{bom};
}

sub Core
{
    my $this = shift;
    return $this->{core};
}

sub Debug
{
    my $this = shift;
    return $this->{debug};
}

sub DebugBuild
{
    my $this = shift;
    return $this->{builddebug};
}

sub DoCarbonConfig
{
    my $this = shift;
    my $do = 1;

    if ($this->{nocarbonconfig})
    {
	$do = 0;
    }

    return $do;
}

sub DoIccBuild
{
    my $this = shift;
    my $do = 1;
    if ($this->{noicc})
    {
	$do = 0;
    }

    return $do;
}

sub DoLinuxBuild
{
    my $this = shift;
    my $do = 1;

    if ($this->{nolinux})
    {
	$do = 0;
    }

    return $do;
}

sub DoLinux64Build
{
    my $this = shift;
    my $do = 1;

    if ($this->{nolinux64})
    {
	$do = 0;
    }

    return $do;
}

sub DoSunosBuild
{
    my $this = shift;
    my $do = 1;

    if ($this->{nosunos})
    {
	$do = 0;
    }

    return $do;
}

sub DoDocsBuild
{
    my $this = shift;
    my $do = 1;
    
    if ($this->{nodoc})
    {
	$do = 0;
    }

    return $do;
}

sub DoWindowsBuild
{
    my $this = shift;
    my $do = 1;
    
    if ($this->{nowindows})
    {
	$do = 0;
    }

    return $do;
}


sub DownServer
{
    my $this = shift;
    return $this->{downserver};
}

#----------------------------------------------------------------
# FileArg() sub.
#
# runlist and runsingle use getopts except for one argument, which
# might be a file list or it might be the name of a test depending.
# I don't like this, but it is what it is.  So, we can at least
# make an attempt to determine which it is, a file list or a test.
# I see no reason to limit one particular way to a single script,
# and so either script can use either mode once this class is
# integrated.
#----------------------------------------------------------------

sub FileArg
{
    my $this = shift;
    my $type = "UNKNOWN";

    if ($ENV{CARBON_HOME})
    {
	my $arg = join "/", $this->{fileNames}->CarbonHome(), $this->{fileArg};
	my $testRun1 = join "/", $arg, (basename($this->{fileNames}->TestRunFileName()));
	my $testRun2 = join "/", $arg, (basename($this->{fileNames}->WriteTestRunFileName()));
	
	if ($arg =~ /^(monthly|test)\// && -d "$arg" 
	    && (-f $testRun1 || -f $testRun2))
	{
	    $type = "Test";
	}
	elsif (-f "$arg" && ! -d "$arg")
	{
	    $type = "File List";
	}
	else 
	{
	    print STDERR "Unknown file/test argument $arg.\n",
	    "Can't find a test or a file list of this name:\n",
	    $this->{fileArg},"\n";
	}
    }
    else
    {
	print STDERR "\$CARBON_HOME is not set.\n";
    }

    return $type;
}

sub HelpRequest
{
    my $this = shift;
    return $this->{help};
}

sub Installer
{
    my $this = shift;
    return $this->{installer};
}

sub Interactive
{
    my $this = shift;
    return $this->{interactive};
}

sub LateNight
{
    my $this = shift;
    return $this->{latenight};
}

sub LowPriority
{
    my $this = shift;
    return $this->{lowpri};
}

sub NoCleanup
{
    my $this = shift;
    return $this->{nocleanup};
}

sub NoExecEnv
{
    my $this = shift;
    return $this->{noexecenv};
}

sub NoGrid
{
    my $this = shift;
    return $this->{nosge};
}

sub NoKillClean
{
    my $this = shift;
    return $this->{nokillclean};
}

sub NoLinkTree
{
    my $this = shift;
    return $this->{nolinktree};
}

sub NoLM
{
    my $this = shift;
    return $this->{nolm};
}
sub OldVectors
{
    my $this = shift;
    return $this->{oldvectors};
}

sub Overwrite
{
    my $this = shift;

    return $this->{overwrite};
}

sub PatchName
{
    my $this = shift;
    return $this->{patch};
}

sub PrintHelp
{
    my $this = shift;
    my $script = $this->{scriptName};

    &CDSUsage::PrintHelp($script);
}

sub Quick
{
    my $this = shift;
    return $this->{quick};
}

sub ReleasePackage
{
    my $this = shift;
    my $relpath = "";

    if (! $this->{relpackage})
    {
	$this->{relpackage} = $this->{fileNames}->ReleasePath();
    }

    return $this->{relpackage};
}

sub RelTest
{
    my $this = shift;
    return $this->{reltest};
}

sub RerunFails
{
    my $this = shift;
    return $this->{failrerun};
}

sub RunDir
{
    my $this = shift;
    return $this->{rundir};
}

sub RunTests
{
    my $this = shift;
    return $this->{runtests};
}

sub SaveRunInfo
{
    my $this = shift;
    return $this->{saveruninfo};
}

sub ScriptName()
{
    my $this = shift;
    return $this->{scriptName};
}

sub SetupOnly
{
    my $this = shift;
    return $this->{setuponly};
}

sub Tag
{
    my $this = shift;
    
    if (! $this->{tag})
    {
	use CVSTool;
	$this->{tag} = &CVSTool::SandboxTag();
    }

    return $this->{tag};
}

sub TestGroups
{
    my $this = shift;
    return %{$this->{testgroup}};
}

sub TestOSVariants
{
    my $this = shift;
    return $this->{testversions};
}

sub UnitTests
{
    my $this = shift;
    return $this->{testgroup}{unit_test};
}

sub Update
{
    my $this = shift;
    return $this->{update};
}

sub PreviousDocs
{
    my $this = shift;
    return $this->{shift};
}

sub Variant
{
    my $this = shift;
    return $this->{variant};
}

sub Verbose
{
    my $this = shift;
    return $this->{verbose};
}

sub VspCompiler
{
    my $this = shift;
    return $this->{vspcompiler};
}

sub WinList
{
    my $this = shift;
    return $this->{testgroup}{win};
}

sub Winx
{
    my $this = shift;
    return $this->{winx};
}

sub Pr
{
    my $this = shift;

    print STDERR "SCRIPT that owns thus argument class: ", $this->{scriptName}, "\n";
    foreach my $arg (sort keys %{$this})
    {
	if ($arg eq "testgroup")
	{
	    foreach (keys %{$this->{testgroup}})
	    {
		print STDERR "TESTGROUP: $_\n";
	    }
	}
	elsif ($arg eq "testGroupIn")
	{
	    # we don't care about this one. It's a temp holder.
	}
	elsif ($arg eq "cmdline")
	{
	    print STDERR "CMD LINE ARGS: ", $this->{cmdline}, "\n";
	}
	else
	{
	    print STDERR "OPTION: $arg -> ", $this->{$arg}, "\n";
	}
    }
}

sub NumGroupLists
{
    my $this = shift;

    my $gNo = values(%{$this->{testgroup}});
    return $gNo;
}





1;
