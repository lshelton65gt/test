#!/bin/bash

# this script is now poorly named, it should probably be named verdi_env
# the idea of this script is that it is the single place in the cbuild area
# that must be changed in order to update the version of Verdi/novas/debusy
# tools used by CMS testing.
# Warning#1: There are references to this script in MANY places in ./scripts/* and ./test/*
# so any change to this file must be backed up by extensive testing.
# Warning#2: there are some places where this script is not used because it returns a linux path but a windows path is needed
#   they are: src/FindCMS.cmake
# Warning#3: There were some places in the test area that could not be made to work with
# this script and instead have hardcoded paths to a version of the veridi tools, these should be cleaned up someday


# configuration as of: Fri Apr 11 09:16:24 2014
# with a new license agreement with synopsys, versions of verdi/novas/debussy tools before verdi3-201403 no longer work,
# and so executables from /tools/novas/verdi3-201403 must be used, however the fsdbreader and fsdbwriter
# distributed with verdi3-201403 has many incompatibility issues including:
#  1. linking with this version requires -ldl be added to the link line
#  2. some hierarchical paths are now different (usually in VHDL record names), and some signals are no longer found
#  
# To reduce the effort in resolving these issues it was decided to allow the toolset and the reader/writer to go
# out of sync, thus DEBUSSY_SHARE will point to the older version of the code while DEBUSSY_ROOT will point to the
# current release version

function root {
    echo "$DEBUSSY_ROOT"
}

function share_root {
    echo "$DEBUSSY_SHARE_ROOT"
}

function FsdbWriter {
    echo "$DEBUSSY_SHARE/FsdbWriter"
}

function FsdbReader {
    echo "$DEBUSSY_SHARE/FsdbReader"
}

CARBON_TOOLS=/tools

# DEBUSSY_ROOT points to the current version of tools (that are compatible with licenses)
# verdi3-201403 is the last official release that has been shown to work
#DEBUSSY_ROOT=$CARBON_TOOLS/novas/verdi3-201403

# the following is an early release that seems to have fixed bugzilla 16884, Synopsys says it will be officially relased June 5 release: Verdi3_201403SP1 
DEBUSSY_ROOT=$CARBON_TOOLS/novas/verdi3v201412_0509

# note DEBUSSY_SHARE is currently not in sync with DEBUSSY_ROOT, see note above for reason
DEBUSSY_SHARE_ROOT=$CARBON_TOOLS/novas/debussy-53v18
DEBUSSY_SHARE=$DEBUSSY_SHARE_ROOT/share

function usage {
    echo "Usage:"
    echo "  debussy_env env_object | -h"
    echo ""
    echo "Valid env_objects are:"
    echo ""
    echo " root              :Get the current Verdi/novas/debussy area"
    echo " share_root        :Get the Verdi/novas/debussy area that is the same version as FsdbWriter and FsdbReader"
    echo " FsdbWriter        :Get the FsdbWriter area"
    echo " FsdbReader        :Get the FsdbReader area"
    echo ""
    echo "Other options:"
    echo " -h                :This usage"
    exit 0
}

# main script starts here
while [ $# -gt 0 ]
  do
  case "$1" in
      root)    root;;
      share_root)    share_root;;
      FsdbWriter)  FsdbWriter;;
      FsdbReader)  FsdbReader;;
      -h|*) usage;;
  esac
  shift
done    

