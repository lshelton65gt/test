#!/bin/sh
# -*-Perl-*-
exec $CARBON_HOME/bin/perl -S -x -- "$0" "$@"
#!perl

use strict;
use warnings;
use File::Spec::Functions;

sub cleanup {
}

sub error {
    my $errMsg = $_[0];
    cleanup;
    die $errMsg;
}

sub writeArrayToFile {
    my($fh) = shift;
    my($lastLine) = -1;
    if ($#_ >= 1) {
        $lastLine = $_[1];
    }

    my($n);
    if (($lastLine < 0) || ($lastLine > $#{$_[0]})) {
        $lastLine = $#{$_[0]};
    }
    
    for ($n = 0; $n <= $lastLine; $n++) {
        print $fh ${$_[0]}[$n] . "\n";
    }
}

sub grabFileLines {
    my ($file) = $_[0];
    local *IN;
    error ("Unable to open $file for reading: $!") unless open(IN, "<$file");

    my $x;
    while($x=<IN>) {
        ${$_[1]}[($#{$_[1]} + 1)] = $x;
    }
    close(IN);
}

sub processCrypt {
    my ($curFile) = $_[0];
    if ($curFile =~ /\.cxx|\.h/o) {
        # print STDOUT "curFile = $curFile\n";
        my $filename = $curFile;
        my @srcCodeLines = ();
        grabFileLines($curFile, \@srcCodeLines);
        
        my $lineEnd;
        my $saveString;
        my $tmp;
        my $putFileName = 1;
        for (my $i = 0; $i <= $#srcCodeLines; $i++) {
            my @lineSplit = split(/CRYPT */, $srcCodeLines[$i]);
            $lineEnd = "";
            if ($#lineSplit > 0) {
                if ($#lineSplit > 1) {
                    print STDERR "${filename}: " . ($i + 1) . " : Multiple CRYPTS on same line.\n";
                    exit 1;
                }
                $lineEnd = $lineSplit[1];
            }
            if (length($lineEnd) != 0) {
                
                if ($lineEnd =~ /\( *\"/o) {
                    # Found a CRYPT
                    $lineEnd =~ s/( *\( *\"){1}?//o;
                    $lineEnd =~ s/(?:(?=(?<!\\)\" *\))).*$//s;
                    #my @saveStringToks = split(/(?:\( *\"|(?<!\\)\" *\))/o, $lineEnd);
                    $saveString = "\"" . $lineEnd . "\"";
                    if ($putFileName == 1) {
                        $curFile =~ s/\.\.\///go;
                        print $main::gOutFH "`$curFile\n";
                        $putFileName = 0;
                    }
                    print $main::gOutFH ($i + 1) . " $saveString\n";
                } # if lineEnd
            } # if length
        } # for
    } # if a source file
}

sub processDir {
    my ($thisdir) = $_[0];

    error ("$thisdir not a directory") unless (-d $thisdir);
    
    local *THEDIR;

    error ("Could not open $thisdir") unless opendir(THEDIR, $thisdir);
    
    my @allentries = readdir(THEDIR);
    closedir(THEDIR);

    
    # Grab all the file and the directories and separate them
    my @files = ();
    my @dirs = ();
    my $i;
    my $curEnt;
    for ($i = 0; $i <= $#allentries; $i++) {
        $curEnt = $allentries[$i];
        if ($curEnt !~ /^\.+|CVS|testsuite|~$|Makefile|\.gperf|\.msg|Make-.*|\#|\.bom|\.sh|\.in|AUTHORS|COPYING|ChangeLog|INSTALL|NEWS|README|^doc$|aclocal|config\..*|configure|install-sh|\.cache|mkinstalldirs|missing|ltconfig|depcomp|TAGS/o) {
            
            $curEnt = catfile($thisdir, $curEnt);
            #print STDERR $curEnt . "\n";
	    if (-l $curEnt) {
		print STDERR "Ignoring symbolic link $curEnt\n";
	    } elsif (-d $curEnt) {
                $dirs[$#dirs + 1] = $curEnt;
            }
            elsif (-f $curEnt) {
                $files[$#files + 1] = $curEnt;
            }
        }  # else ignore, could be a non-standard file
    } # for
    
    #writeArrayToFile(\*STDERR, \@dirs);
    #writeArrayToFile(\*STDERR, \@files);
    
    # process the files
    for ($i = 0; $i <= $#files; $i++) {
        processCrypt($files[$i]);
    } # for
    
    # now do the directories
    for ($i = 0; $i <= $#dirs; $i++) {
        &processDir($dirs[$i]);
    } # for
}

sub main() {

    error("Usage: $0 <directory> <output file>") unless ($#ARGV == 1);
    
    my $outfile = $ARGV[1];
    local *OUT;
    error ("Unable to open $outfile for writing: $!") unless open(OUT, ">$outfile");
    $main::gOutFH = *OUT;
    processDir($ARGV[0]);
    close(OUT);
}

main();
