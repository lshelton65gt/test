#!/bin/csh -f

source $CARBON_HOME/scripts/setup-carbon-env

set d=`dirname $0`
setenv CARBON_SANDBOX `cd $d/.. && pwd`

if ($?CARBON_TOOLS == 0) then
  setenv CARBON_TOOLS /tools
endif
set script_exe = `echo $0 | sed -e 's,^/n/release,/o/release,'`
set script_dir = $script_exe:h
if ($script_dir == $CARBON_TOOLS) exec $CARBON_HOME/scripts/mkcds $*

if ( $?CARBON_HOST_ARCH ) then
    set HOSTARCH = $CARBON_HOST_ARCH
else
    set HOSTARCH = `$CARBON_HOME/scripts/carbon_arch`
endif
echo HOSTARCH = $HOSTARCH

set shorthost = `hostname | cut -d. -f1`
set grid = 1

# if off the carbon network then run as if there is no grid available
if ( ! (-e "/o/release/CMS/latest") ) then
set grid = 0
endif

set make_args = ()
set need_j_arg = 1
set max_j = 18
set cmakeVerbose = ""
set doc_build = 0

# We don't want this for mkcds.  It only causes problems for cbuild +protect
# which is used in some of the build steps.
unsetenv CARBON_CBUILD_ARGS

set make = /tools/linux/bin/gmake-3.81

if ($?CARBON_HOME == 0) then
  echo CARBON_HOME not set.  Please set it explicitly.
  exit 1
endif

if ($?CARBON_BIN == 0) then
  echo CARBON_BIN not set.  Please set it explicitly to product, debug, shared, gprof, or cov.
  exit 1
endif

setenv HOST_TARGET ${HOSTARCH}.${CARBON_BIN}

set TARGETARCH = ${HOSTARCH}
echo TARGETARCH = $TARGETARCH

# Add options from carbon_config
set options = ($argv "")
while (x"$options[1]" != x)
  switch ($options[1])
    case -help:
    case -usage:
      cat <<EOF
 Usage: `basename $0` [OPTION]... [make-args]...

  -D<macro>        pass -D<macro> as CXX flags

  -cxxflag option  pass option as flag to CXX

  -nogrid          Disable SGE for parallel make

EOF
    exit 1
    breaksw

    case -grid
    case -gird
    case -farm
      breaksw
    case -nogrid:
    case -nofarm:
      set grid = 0
      breaksw
    case -D*:
      if ($?cxxflags) then
        set cxxflags = ($cxxflags $options[1])
      else
        set cxxflags = $options[1]
      endif
      breaksw
    case -cxxflag:
      shift options
      if ($?cxxflags) then
        set cxxflags = ($cxxflags $options[1])
      else
        set cxxflags = $options[1]
      endif
      breaksw
    case -verbose:
    case -nf:
      set cmakeVerbose = "-DCMAKE_VERBOSE_MAKEFILE=1"
      breaksw
    case -winx:
      echo "mkcds -winx is no longer supported - Instead we use a native Windows build"
      echo " Go to your windows machine and in a dos cmd window execute the file:"
      echo "    c:\\"'$CARBON_HOME'"\obj\Win.${CARBON_BIN}\build.bat"
      exit 1
      breaksw
    case dox:
      # Intercept doc build
      set doc_build = 1
      breaksw
    default:
      if (x`echo $options[1] | cut -c1-2` == x-j) set need_j_arg = 0
      set make_args = ($make_args $options[1])
      breaksw
  endsw
  if ("x$options[1]" != x) shift options
end

setenv CARBON_TARGET $TARGETARCH.${CARBON_BIN}

if ($TARGETARCH == Win) then
  grep '\[w\]' /etc/samba/smb.conf > /dev/null
  if (x$status != x0) then
   echo "This machine is not configured with a [w] Samba Share, aborting"
   exit 1
  endif
endif

set objdir = $CARBON_HOME/obj/$CARBON_TARGET
setenv CARBON_NET_HOME $CARBON_HOME
if ($TARGETARCH != Win) setenv CARBON_HOME `$CARBON_HOME/scripts/shortcircuit $CARBON_HOME`
set CARBON_HOME_NO_NET = `echo ${CARBON_HOME} | sed -e 's,^/net/,,'`
if( ${CARBON_HOME_NO_NET} != ${CARBON_HOME} ) then
  echo 'WARNING! $CARBON_HOME does not appear to be local to this machine.'
  echo '         Building on remote machines has problems.  See bug 2528.'
endif
set long_objdir = $objdir
if ($HOSTARCH == Win) then
  set objdir = `$CARBON_HOME/scripts/cygpath -m $objdir`
else if ($TARGETARCH != Win) then # Don't shortcircuit for Windows
  set objdir = `$CARBON_HOME/scripts/shortcircuit $objdir`
endif
setenv CARBON_LOCAL_OBJ $objdir
setenv CARBON_NET_OBJ $long_objdir
echo CARBON_NET_OBJ = $CARBON_NET_OBJ

setenv CARBON_OBJ $objdir
setenv HOST_ARCH $HOSTARCH
setenv TARGET_ARCH $TARGETARCH

if ($objdir != $long_objdir) then
  echo Short-circuiting $long_objdir to $objdir
endif

set make_j_arg = ""
if ($grid == 1) then
  unsetenv DISPLAY
  set machines = ()
  # strip this host from the machine list
  echo "Querying for available hosts..."

  if ($TARGETARCH == 'Win') then
    set grid_mach_type = Linux
  else
    set grid_mach_type = $TARGETARCH
  endif

  foreach m (`$CARBON_HOME/scripts/GetAvailableFarmHosts $grid_mach_type`)
    if ($m != $shorthost) set machines = ($machines $m)
  end
  if ($#machines == 0) then
    echo No machines available.  Retry later.
    exit 1
  endif
  echo Grid says we can run on $machines ...
  set jval = `expr $#machines \* 2`
  if (`expr $jval \> 12` == 1) set jval = $max_j
  if ($need_j_arg == 1) then
    set make_j_arg = "-j${jval}"
    set make_args = ($make_args $make_j_arg)
  endif
endif
# Having trouble with distcc? (for example "mkcds -nogrid" works  but "mkcds" with grid seems to get stuck?)  Try removing your ~/.distcc directory.

cd $objdir

if ($doc_build == 1}) then
  echo 'rm -rf doc/html'
  rm -rf doc/html
endif


# Add default library path for build tools
if ($HOSTARCH == Linux) then
  set hostarch = linux
else if ($HOSTARCH == Linux64) then
  set hostarch = linux64
endif

set cxx_version = "`cd $objdir; grep COMPILER_VERSION Compiler.cmake | cut -d ' ' -f2 | tr -d ')'`"
set binutils_version = "`cd $objdir; grep BINUTILS_VERSION Compiler.cmake | cut -d ' ' -f2 | tr -d ')'`"
set cxx = $CARBON_TOOLS/$hostarch/ES4/gcc-${cxx_version}-binutils-${binutils_version}/bin/g++
set cc = $CARBON_TOOLS/$hostarch/ES4/gcc-${cxx_version}-binutils-${binutils_version}/bin/gcc

echo cxx = $cxx

if ($grid == 1) then
  setenv DISTCC_HOSTS "$machines"
endif

# echo DISTCC_HOSTS = "$DISTCC_HOSTS"
# TODO - add cxx_flags

# Determine the Qt version
set qtver = `$make -f $CARBON_SANDBOX/src/Makefile.qt-ver echo_qt_version`
# Add important things (binutils, cmake, Qt) to the path
setenv PATH $CARBON_TOOLS/$hostarch/ES4/binutils-$binutils_version/bin:/tools/linux/cmake-2.8.4/bin:$CARBON_TOOLS/$hostarch/ES4/qt-$qtver-gcc4/bin:$PATH

if ($CARBON_BIN == debug) then
  set buildType="Debug"
else
  set buildType="Release"
endif

  # Define some variables that control the build:
  # build_names - list of individual cmake build steps.
  # cxx_list/cc_list - C++/C compiler to be used for the corresponding build_name
  # cmake_defines - defines to be passed to cmake for the corresponding build_name
  #
  # The variables cxx_list, cc_list, and cmake_defines must have at
  # least as many entries as build_names.  If there are cases in which
  # fewer builds need to be done, build_names can be shortened without
  # changing the other lists.
  #
  # We don't support SoCD on Linux64, so we can skip the gcc 3.4.3
  # transactor build.  Modelstudio is not built on Linux64, so omit
  # that build area, too.  It doesn't matter that the other variables
  # have extra words at the end.
  #
  # Also, doc builds are a special case
  if ($doc_build == 1) then
    set build_names=(docs)
  else if ($HOSTARCH == Linux64) then
    set build_names=(gcc-${cxx_version})
  else
    set build_names=(gcc-${cxx_version} gcc-3.4.3 gcc-${cxx_version}-qmake)
  endif
  set cxx_list=($cxx $CARBON_TOOLS/$hostarch/ES4/gcc-3.4.3/bin/g++ $cxx)
  set cc_list=($cc $CARBON_TOOLS/$hostarch/ES4/gcc-3.4.3/bin/gcc $cc)
  if ($doc_build == 1) then
    set cmake_defines=("-DBUILD_CMS_DOCS=1")
  else
    set cmake_defines=("" "-DBUILD_CMS_SOCD=1" "-DBUILD_CMS_QMAKE=1 -DMAKE_J_ARG=$make_j_arg")
  endif

# First, create all the build directories and run cmake in them
set i=1
while ($i <= $#build_names)
  set builddir=$CARBON_OBJ/build_$build_names[$i]
  mkdir -p $builddir
  cd $builddir
  setenv CXX "$CARBON_HOME/scripts/selectdistcc $cxx_list[$i]"
  setenv CC "$CARBON_HOME/scripts/selectdistcc $cc_list[$i]"
  # echo === starting on `hostname` cmake -DCMAKE_BUILD_TYPE=$buildType $CARBON_SANDBOX/src $cmake_defines[$i] $cmakeVerbose===
  cmake -DCMAKE_BUILD_TYPE=$buildType $CARBON_SANDBOX/src $cmake_defines[$i] $cmakeVerbose
  set thisStatus = $?
  if ($thisStatus != 0) then
    echo "=== $build_names[$i] cmake failed ==="
    exit 1
  endif
  @ i++
end

# Now, build in each directory.  CXX and CC are no longer needed.

unsetenv CXX
unsetenv CC

set i=1
while ($i <= $#build_names)
  set builddir=$CARBON_OBJ/build_$build_names[$i]
  cd $builddir
  # echo "=== $build_names[$i] starting in $PWD ==="
  $make $make_args |& tee $builddir/build.log
  set thisStatus = $?
  if ($thisStatus != 0) then
    echo "=== $build_names[$i] build failed ==="
    exit 1
  endif
  @ i++
end

#
# Hack to test for swig generated library errors
#
if ( ($HOSTARCH == Linux) && (! $doc_build) ) then
  # two parallel arrays swig_libs and swig_min_size each corresponds to the other
  set swig_libs=(libcarbondbapics.so libCarbonCfgCSharp.so ../python/lib/python2.4/site-packages/Carbon/_carboncfg.so ../python/lib/python2.4/site-packages/Carbon/_carbondbapi.so)
  set swig_min_size=(100000 100000 700000 400000) 
  set i=1  
  foreach l ($swig_libs)
    set libpath=$CARBON_HOME/Linux/lib/$l
    set libsize=$swig_min_size[$i]
    if ( ! -e $libpath) then
      echo "--- Error: swig_libs build check: no such file: $libpath"
      if ( $CARBON_BIN == debug ) then
        echo "--- Hint: you must build 'product' before you can build 'debug'"
      endif
      exit 1
    else
      @ filesize=`/usr/bin/stat -c %s -L $libpath`
      echo "Validating $libpath ($filesize bytes >= $libsize)"
      if ($filesize < $libsize) then
        echo "--- Error: File $libpath should be at least $libsize bytes in size"
        echo "  This is usually caused by the SWIG/Cmake bug, re-execute mkcds, it will probably work"
        exit 1
      endif
    endif
    @ i++
  end
endif

echo "=== build complete ==="

exit 0
