#!/usr/bin/perl

#
# install-test should be run as a cronjob on an isolated machine. it will
# look in $dirToWatch for the newest file matching carbon-release-*.tar. if
# there is no matching directory 'carbon-release-*', then it will install
# the tar file and try to run the examples
#
# NOTE: if you modify this file, be sure to copy it to the isolated release
#  testing machines and put it where the crontab expects it. currently:
#  qa@hafnium:~/bin/install-test
#  qa@sun-ae1:~/bin/install-test

use strict;
use File::Basename;

$| = 1;

my $logref = \*STDERR;
my $dirToWatch = "/home/qa";
my $toolsQA = "/home/cds/qa/data";
my $ageThreshold = 100; # files older than this (in days) will be disregarded
my $arch = `uname -s`; chomp $arch;
my $workFile = ".install-test-busy";
my $MailList = "qa\@carbondesignsystems.com";
my @examplesDirs = (    # each of these dirs will be chdir'd to and make'd
  "examples/cmodel",    # unless $? is 0 on each make, this test will fail
  "examples/twocounter"
);

# look for a work file here to signify that the script is running
chdir $dirToWatch;
if (-e $workFile) {
  exit;
}

unless (opendir(HOMEDIR, $dirToWatch)) {
  die "could not read $dirToWatch - are you running this on the correct machine?";
}

# read in a list of all tar files in the current dir
my @tarfiles = map "$dirToWatch/$_",
   grep(/^carbon-release-.*\.tar/, readdir(HOMEDIR));
rewinddir(HOMEDIR);
push (@tarfiles, map ("$dirToWatch/$_",
   grep(/^carbon-release-.*\.tgz/, readdir(HOMEDIR))));
close(HOMEDIR);

# if no tar files, exit
if (@tarfiles == 0) { exit(0); }

# get the newest tarfile
my $releaseTarfile = &NewestOfThese (\@tarfiles);

# strip .tar to get the dest dir
(my $installDir = $releaseTarfile) =~ s/\.tar$//;
$installDir =~ s/\.tgz$//;
if (-e $installDir) {
  # don't need to install this - just exit, don't print a msg
  exit(0);
  #die "$installDir already exists - aborting installation of $releaseTarfile";
}

# now we know we're installing, so touch a workfile, make the dir, and start
# logging
system `touch $workFile`;
mkdir ($installDir) or &error("Could not create $installDir: $!");
if (-e "$dirToWatch/install-test.log") {
  unlink("$dirToWatch/install-test.log");
}
open(LOGFILE, ">$dirToWatch/install-test.log") or
    &error("Could not create $dirToWatch/install-test.log: $!\n");
$logref = \*LOGFILE;
# and print a timestamp
print LOGFILE "\n";
print LOGFILE "=" x 65 . "\n\n";
print LOGFILE "Installing $releaseTarfile into $installDir... \n";
print LOGFILE `date`."\n";

# now, is it a full or patch release? if the tagname ends in in _p<#>, it is
# a patch release, which means we have to update an existing install
my $release_name = (split(/-/, basename($installDir), 4))[2];
my $isPatch = 0;

# gzip?
my $gZip = "";
if ($releaseTarfile =~ m/\.tgz$/) {
  $gZip = "z";
}

# what will the installer be named
my $installer = "install-".(split(/-/,basename($installDir),3))[2];
`tar t${gZip}f $releaseTarfile | grep $installer`;

unless ($?) {
  $release_name =~ m/^(.*)_p(\d+)$/;
  my $majorRelease = $1;
  my $patchLevel = $2;
  $isPatch = 1;
  # now get the previous release
  my @previousTarfiles = grep (/carbon-release-$majorRelease[_-]/, @tarfiles);
  my $previousInstall;

  # if there are only 2 that have this tag, then one is this one, and the
  # other is the one we want
  if (scalar(@previousTarfiles) == 2) {
    $previousInstall = (grep(/carbon-release-$majorRelease-\d{4}-\d{2}-\d{2}/,
	                    @previousTarfiles))[0];

  # if there is only 1, it's this one and there isn't a previous install.
  # warn and continue
  } elsif (scalar(@previousTarfiles) < 2) {
    print LOGFILE "WARNING: $installDir appears to be a patch, but there ".
        "is no previous installation\n Will attempt to do a clean install...\n";
    $isPatch = 0;

  # more than 2, so we have some work to do...
  } else {
    # cycle through decrementing the patch number and getting the newest
    # until we find one
    for(my $lastPatchLvl = ($patchLevel - 1); $lastPatchLvl >= 0; 
        $lastPatchLvl--) {
      if ($lastPatchLvl == 0) {
        $previousInstall = &NewestOfThese(
          [grep(/carbon-release-${majorRelease}-\d{4}-\d{2}-\d{2}/,
            @previousTarfiles)]);
      } else {
        $previousInstall = &NewestOfThese(
          [grep(/carbon-release-${majorRelease}_p$lastPatchLvl-\d{4}-\d{2}-\d{2}/,
	    @previousTarfiles)]);
      }

      if($previousInstall ne "") { 
        # strip the extension
        $previousInstall =~ s/\.tar$//;
        $previousInstall =~ s/\.tgz$//;
        # make sure the dir exists (if not, reset)
        if(-d $previousInstall) {
          last; 
        } else {
          $previousInstall = "";
	}
      }

    }
  }

  # now we have the previous install, so copy it to the current install dir
  # (with tar-to-tar then will continue using that populated install dir
  # (unless isPatch was reset to 0)
  if ($isPatch) {
    print LOGFILE "Will install patch over $previousInstall in $installDir...\n";
    system("(cd $installDir; (cd $previousInstall; tar cf - .) | tar xf - ;)");
    if ($? != 0) {
      print LOGFILE "Unable to copy $previousInstall to $installDir\n";
    }
  }
  
}

# make sure we have the corresponding license files
my $licFile = $installDir.".lic";
#my $cdbFile = $installDir.".cdb";
#unless (-f $licFile) {
  #&error("License file $licFile not found\n");
#}
#unless (-f $cdbFile) {
  #&error("License file $cdbFile not found\n");
#}

# get rid of .flexlmrc file
if (-e "$ENV{'HOME'}/.flexlmrc") {
  unlink "$ENV{'HOME'}/.flexlmrc" or &error("Can't delete $ENV{'HOME'}/.flexlmrc");
}

chdir ($installDir) or &error("Could not chdir to $installDir: $!");
# if this is ever incremented, then we fail at the end
my $completeFail = 0;

# if it's a patch, just untar the install file, then run it to do the rest
if ($isPatch) {

  system "tar x".$gZip."f $releaseTarfile ./$installer";
  if ($? != 0) {
    &error("Problem installing $releaseTarfile ./$installer: $!");
  }
  print LOGFILE "Executing installer $installer...\n";
  system "./$installer";
  if ($? != 0) {
    &error("Problem executing installer $installer: $!");
  }

} else {
  # untar
  system "tar x".$gZip."f $releaseTarfile"; 
  if ($? != 0) {
    &error("Problem installing $releaseTarfile: $!");
  }

  # run fixate script and check output against gold file
  print LOGFILE "Running fixate post-install script... ";
  system "./fixate > fixate.out 2>&1";
  if ($? == 0) {
    print LOGFILE " PASSED\n";
  } else {
    print LOGFILE " FAILED (see fixate.out)\n";
    $completeFail = 1;
  }

}

# if we were given a lic file, put it in place
if (-e $licFile) {
  rename($licFile, "$installDir/lib/customer.lic") or 
    &error("Can't rename $licFile to $installDir/lib/customer.lic");
# create one if need to
} 
&CreateLicenseFile("$installDir/lib/customer.lic");
# no more cdb files...
# put the cdb file in place
#rename($cdbFile, "$installDir/lib/customer.cdb") or
  #&error("Can't rename $cdbFile to $installDir/lib/customer.cdb");

# setup env vars
$ENV{CARBON_HOME} = $installDir;
$ENV{PATH} = "$installDir/bin:$ENV{PATH}";
$ENV{LD_LIBRARY_PATH} = "$installDir/$arch/gcc/lib:$ENV{LD_LIBRARY_PATH}";
$ENV{LM_LICENSE_FILE} = "$installDir/lib/customer.lic";

# build the systemc distribution
print LOGFILE "Building systemc... ";
system "bin/build-systemc > systemc/build.log 2>&1";
if ($? == 0) {
  print LOGFILE " PASSED\n";
} else {
  print LOGFILE " FAILED (see systemc/build.log)\n";
  $completeFail = 1;
}
$ENV{SYSTEMC_HOME} = "$installDir/systemc";

# check to make sure that the carbond link in /tools is the same as the one
# in this package
my $flexPath; my $localFlexPath;
if ($arch eq 'Linux') { 
  $flexPath = '/tools/flexlm/v10.1/i86_re3';
  $localFlexPath = "$installDir/$arch/bin/ES4";
} else {
  $flexPath = '/tools/flexlm/v10.1/sun4_u5'; 
  $localFlexPath = "$installDir/$arch/bin/sun4u5";
}
my @out = `diff $flexPath/carbond $localFlexPath/carbond 2>&1`;
if ($? != 0) {
  print LOGFILE "WARNING: ". join ("", @out) . "\n";
}

# write help output to a file
system ("cbuild > $dirToWatch/cbuild.help.out");

# now make the examples
my $pass = 0;
my $fail = 0;
my @failedOutput;
foreach my $exampleDir (@examplesDirs) {
  chdir("$installDir/$exampleDir") or &error("can't chdir to $installDir/$exampleDir");
  if ($isPatch) {
    system "/bin/bash -c 'make clean >& makeclean.log'";
    if ($? != 0) {
      print LOGFILE "ERROR: Make clean failed, logfile at $installDir/$exampleDir/makeclean.log\n";
      push(@failedOutput, sprintf("%.48s", "$exampleDir/makeclean ..........................................") 
        . " *FAILED* [1]\n");
      $fail++;
      next;
    } 
  }

  # make it with default gcc (default)
  system "/bin/bash -c 'make >& make.log'";
  if ($? != 0) {
    print LOGFILE "ERROR: Make failed, logfile at $installDir/$exampleDir/make.log\n";
      push(@failedOutput, sprintf("%.48s", "$exampleDir/make ......................................") . 
        " *FAILED* [1]\n");
    $fail++;
  } else {
    $pass++;
  }

  # clean up
  system "/bin/bash -c 'make clean >> makeclean.log 2>&1'";
  if ($? != 0) {
    print LOGFILE "ERROR: Make clean failed, logfile at $installDir/$exampleDir/makeclean.log\n";
    push(@failedOutput, sprintf("%.48s", "$exampleDir/makeclean ...........................................") 
      . " *FAILED* [1]\n");
    $fail++;
    next;
  } 
}

# print status
if ($fail > 0) {
  print LOGFILE "Installation test *FAILED* [$fail examples failed]\n";
  $completeFail = 1;
} else {
  print LOGFILE "Installation test PASSED [$pass examples passed]\n";
}

# now, do we also have a test package?
(my $testPackage = $releaseTarfile) =~ s/\/carbon-release(-Linux)?/\/carbon-test/;
if (-e $testPackage){
  # yep, so set it up and spawn a runlist -reltest for release, then lic tests
  chdir ($installDir);

  if(system ("tar x".$gZip."f $testPackage")) {
    $completeFail = 1;
    &error ("Problem installing test package $testPackage: $!");
  }

  # now that we have a gold file, diff the fixate output
  print LOGFILE "Checking fixate script output... ";
  system "diff -w scripts/fixate.gold fixate.out > fixate.out.diff 2>&1";
  if ($? == 0) {
    print LOGFILE " PASSED\n";
  } else {
    print LOGFILE " FAILED (see fixate.out.diff)\n";
    $completeFail = 1;
  }
 

  my @runlistOutput = `/bin/bash -c "source ~/.bashrc; source scripts/env_setup; scripts/runlist -overwrite -reltest -testgroup release"`;

  #print (join("", @runlistOutput));
  my @out = grep (/ED\*?:\s+\d+\s+of\s+\d+\s+tests\s+failed/, 
    @runlistOutput);
  $out[0] =~ m/\s+(\d+)\s+of\s+(\d+)\s+tests/;
  push (@failedOutput, grep (/\*FAILED\*[^:]/, @runlistOutput));
  if ($2 and ($? != 0)) {
    $completeFail = 1;
    print LOGFILE "Post-installation tests *FAILED* [$1 of $2 tests failed]\n";
  } elsif ($2) {
    print LOGFILE "Post-installation tests PASSED [$2 tests passed]\n";
  } else {
    # if something really wacky happened, write the output to a file
    open(RLOUT, ">install.runlist.out");
    print RLOUT join("", @runlistOutput)."\n";
    close(RLOUT);
    $completeFail = 1;
    print LOGFILE "Post-installation tests FAILED [0 tests passed]\n";
  }

  # this only kills the license server, doesn't create anything - licensing
  # tests need to start without flex running
  &CreateLicenseFile("lib/customer.lic", 1);

  # now run the licensing tests - these need to run 2nd b/c flex is kaput
  # when they're done
  @runlistOutput = `/bin/bash -c "source ~/.bashrc; source scripts/env_setup; scripts/runlist -overwrite -reltest -testgroup licensing -rundir licensing"`;
  #print (join("", @runlistOutput));
  @out = grep (/ED\*?:\s+\d+\s+of\s+\d+\s+tests\s+failed/, 
    @runlistOutput);
  $out[0] =~ m/\s+(\d+)\s+of\s+(\d+)\s+tests/;
  push (@failedOutput, grep (/\*FAILED\*[^:]/, @runlistOutput));
  if ($2 and ($? != 0)) {
    $completeFail = 1;
    print LOGFILE "Licensing tests *FAILED* [$1 of $2 tests failed]\n";
  } elsif ($2) {
    print LOGFILE "Licensing tests PASSED [$2 tests passed]\n";
  } else {
    # if something really wacky happened, write the output to a file
    open(RLOUT, ">license.runlist.out");
    print RLOUT join("", @runlistOutput)."\n";
    close(RLOUT);
    $completeFail = 1;
    print LOGFILE "Licensing tests FAILED [0 tests passed]\n";
  }

# we should always have a test package these days - if not, fail
} else {
  print LOGFILE "Licensing tests FAILED [$testPackage not found]\n";
  print LOGFILE "Post-installation tests FAILED [$testPackage not found]\n";
  $completeFail = 1;
}

# now if it's a patch, try uninstalling and rerunning the tests
if ($isPatch) {
  print LOGFILE "Attempting uninstall...\n";
  chdir($installDir) or &error("can't chdir to $installDir");
  system("./un$installer");
  if ($? != 0) {
    $completeFail = 1;
    &error("Problem executing uninstaller un$installer: $!");
  }

  $pass = 0; $fail = 0;
  foreach my $exampleDir (@examplesDirs) {
    chdir("$installDir/$exampleDir") or &error("can't chdir to $installDir/$exampleDir");
    system "/bin/bash -c 'make clean >& makeclean.uninstall.log'";
    if ($? != 0) {
      print LOGFILE "ERROR: Make clean failed, logfile at $installDir/$exampleDir/makeclean.log\n";
      push(@failedOutput, sprintf("%.48s", "$exampleDir/makeclean.uninstall".
        " "."."*48) . " *FAILED* [1]\n");
      $fail++;
      next;
    } 
    system "/bin/bash -c 'make >& make.log'";
    if ($? != 0) {
      print LOGFILE "ERROR: Make failed, logfile at $installDir/$exampleDir/make.log\n";
      push(@failedOutput, sprintf("%.48s", "$exampleDir/make.uninstall".
        " "."."*48) . " *FAILED* [1]\n");
      $fail++;
    } else {
      $pass++;
    }
  }

  # print status
  if ($fail > 0) {
    $completeFail = 1;
    print LOGFILE "Uninstallation test *FAILED* [$fail examples failed]\n";
  } else {
    print LOGFILE "Uninstallation test PASSED [$pass examples passed]\n";
  }

}

print LOGFILE "CARBON_HOME: $ENV{CARBON_HOME}\nPATH: $ENV{PATH}\n";
print LOGFILE "SYSTEMC_HOME: $ENV{SYSTEMC_HOME}\n";
print LOGFILE "LD_LIBRARY_PATH: $ENV{LD_LIBRARY_PATH}\n";
print LOGFILE "LM_LICENSE_FILE: $ENV{LM_LICENSE_FILE}\n";

# print detailed list of failures
print LOGFILE "\n\n" . "=" x 65 . "\n";
print LOGFILE join("", @failedOutput);

# get rid of the tmp file
unlink("$dirToWatch/$workFile") or 
  &error("Can't remove work file $dirToWatch/$workFile");

close(LOGFILE);

# now copy the log file back to non-isolation
system("scp $dirToWatch/install-test.log qa\@qa101:$toolsQA/Last.".$arch.
	"Release.Results");
system("scp $dirToWatch/install-test.log qa\@qa101:$toolsQA/TestHistory-".$arch."Release/".basename($releaseTarfile).".log");

# get mail executable
my $mailexec;
if (-e "/usr/bin/mail") {
  $mailexec = "/usr/bin/mail";
} else {
  $mailexec = "/bin/mail";
}

# send the log mail
if ($completeFail) {
  system("cat $dirToWatch/install-test.log $installDir/obj/$arch.product/runlist.results $installDir/obj/$arch.product/licensing/runlist.results | $mailexec -s \"FAILED $arch Install test\" $MailList");
} else {
  system("cat $dirToWatch/install-test.log $installDir/obj/$arch.product/runlist.results $installDir/obj/$arch.product/licensing/runlist.results | $mailexec -s \"PASSED $arch Install test\" $MailList");
}

sub error {
  my $msg = $_[0];
  unlink("$dirToWatch/$workFile");
  print $logref "ERROR: $msg\n";
  die "ERROR: $msg";
}

sub warning {
  my $msg = $_[0];
  print $logref "WARNING: $msg\n";
}

# given a listref containing filenames, return the newest file in the list,
# or "" if none
sub NewestOfThese {
  my $listref = $_[0];

  my $newestFile = ""; my $newestModTime = $ageThreshold;
  # look for newest file
  foreach my $file (@$listref) {
    if (-M $file < $newestModTime) {
      # found it
      $newestFile = $file;
      $newestModTime = -M $file;
    }
  }
  return $newestFile;
}

# if justKillIt is set, don't create anything, just kill the server
sub CreateLicenseFile {
  my ($destFile, $justKillIt) = @_;
  my $flexRoot = "/tools/flexlm/v10.1";
  my $TestCompany = "TestRelease";
                                                                                
  #if (-e $destFile) {
    #unlink($destFile);
  #}
  
  # make file absolute
  unless ($destFile =~ m#^/#) {
    my $cwd = `pwd`; chomp $cwd;
    $destFile = $cwd."/".$destFile;
  }

  # set up path to flexlm stuff, and get host id for use in lic file
  my $mhostid; my $flexArch;
  if ($arch eq "Linux") {
    $flexArch = "i86_r6";
    $mhostid = `/sbin/ifconfig | grep eth0 | awk '{ print \$5 }' | sed -e 's/://g'`; chomp $mhostid;
  } elsif ($arch eq "Win") {
    $flexArch = "i86_r3";
    $mhostid = `/sbin/ifconfig | grep eth0 | awk '{ print \$5 }' | sed -e 's/://g'`; chomp $mhostid;
    print STDERR "(Placeholder): Windows license not supported yet\n";
    return 0;
  } else {
    print $logref "Unsupported architecture ($arch)\n";
  }
                                                                                
  my $flexBin = $flexRoot."/".$flexArch;

  # if we don't already have a license file, make one
  unless (-e $destFile or $justKillIt) {
    # get hostname for use in lic file
    my $name = `uname -n`; chomp $name;
                                                                                
    # create the input .lic.txt
    unless(open(TESTLIC, ">$destFile")) {
      print $logref "Can't create $destFile: $!\n";
      return 0;
    }
    print TESTLIC <<EOF;
SERVER $name $mhostid
USE_SERVER
VENDOR carbond
FEATURE cds_speedcompiler_vhdl_verilog carbond 1.0 permanent 1
VENDOR_STRING=$TestCompany SIGN=
FEATURE cds_speedcompiler_verilog carbond 1.0 permanent 1
VENDOR_STRING=$TestCompany SIGN=
FEATURE cds_speedcompiler_vhdl carbond 1.0 permanent 1
VENDOR_STRING=$TestCompany SIGN=
FEATURE cds_designplayer_dev carbond 1.0 permanent 1
VENDOR_STRING="cdsdpdev_$TestCompany" SIGN=
FEATURE crbn_vsp carbond 1.0 permanent 1 
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_comp carbond 1.0 permanent 1 
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_comp_opt carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_comp_verilog carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_comp_vhdl carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_vhm carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_intf_rvsocd carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_lmtool_sp carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_replay carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_intf_platarch carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_modelvalidator carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_xtor_ahb carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_xtor_ahblitea carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_xtor_ahba carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_xtor_apba carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_xtor_axia carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_xtor_ddrsdram carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_xtor_e10g carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_xtor_e1g carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_xtor_e10_100 carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_xtor_pci carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_xtor_pcie carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_xtor_sata carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_xtor_spi4 carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_xtor_spi3 carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
FEATURE crbn_vsp_exec_xtor_utopia2 carbond 1.0 permanent 1
VENDOR_STRING=SIG=$TestCompany SIGN=
EOF
                                                                                
    close(TESTLIC);

    # generate the lic file
    system "$flexBin/lmpath -add carbond $destFile >> flexlm.out 2>&1";
    system "$flexBin/lmcrypt $destFile >> flexlm.out 2>&1";
    #system "$flexBin/lmgrd -c $testlic -l $testlic.log";
  }

  # start it using whatever lic file we have
  system "$flexBin/lmdown -q -c $destFile >> flexlm.out 2>&1";
  unless($justKillIt) {
    system "$flexBin/lmgrd -c $destFile >> flexlm.out 2>&1 &";
  }

  return 1;
}
