#!/bin/sh
# -*-Perl-*-
mypath=${0}
bindir=`dirname ${mypath}`
arch=${CARBON_HOST_ARCH:-`${bindir}/carbon_arch`}
LANG=C
export LANG
exec $CARBON_HOME/$arch/bin/perl -I$CARBON_HOME/$arch/lib/perl -I$CARBON_HOME/$arch/lib/perl/site_perl -S -x -- "$0" "$@"
#!perl

### runtest - cmdline interface to RunTest.pm. See RunTest.pm for more details

use strict;
use File::Basename;
use File::Copy;
use Getopt::Long;

# carbon perl libs
use lib "$ENV{CARBON_HOME}/scripts";
use CdsUtil;
use RunTest;

$| = 1;
my $gDebug = 1;

use vars qw($runTest $cifsPurge $help @envStrings
);

# want GetOptions to stop arg proccessing when it sees a bare arg
$Getopt::Long::order = $Getopt::Long::REQUIRE_ORDER;
GetOptions( 'cifspurge'  => \$cifsPurge, #cifspurge does nothing, only
					 #recognized for backwards-compat
	    'env=s@'     => \@envStrings,
);

# go ahead and put any env vars passed into a hash
my $envVars = &CdsUtil::envVarHashFromArgs(@envStrings);

# if running on something other than linux, or if WINX is not set, don't 
# waste anymore time, just exec the command
my $uname = `uname`;
chomp $uname;
if (($uname ne "Linux") or ($ENV{WINX} != 1)) {
  # make sure to set any env vars passed
  foreach my $envVar (keys(%$envVars)) {
    ##print "setting $envVar to ".$envVars->{$envVar}."\n" if ($gDebug);   
    $ENV{$envVar} = $envVars->{$envVar};
  }
  # then exec the rest
  exec(@ARGV);
  die "ERROR: runtest could not exec $ARGV[0]: $!\n";
}

# check mode of current dir. if it's not group writeable, runtest won't work
my @cwdStat = stat('.');
# & the mode with 00020 to check for group-write. 00xxx to get rid of file info,
# xx0x0 to null world and user mode, and xxx2x to check write bit for grp.
# cwdMode will be 0 if write bit is NOT set
my $cwdMode = sprintf("%lo", $cwdStat[2] & 00020);
unless($cwdMode > 0) {
  die "ERROR: current directory has mode ".sprintf("%lo", $cwdStat[2] & 00777).
      ", must be group-writeable for Windows testing to function\n";
}

# now we want to do a little pipe management. since we don't know what will
# happen exactly from here on out, we want all potentially informative output
# to go to the runlist log file. the best way to do this is to dup stdout and
# stderr into a log file (runtest.$$). but we also need to echo the real output
# of the cmd to run to the real stdout. so:
# save stdout for later use
###open(REALSTDOUT, ">& STDOUT") or die "can't dup STDOUT to REALSTDOUT\n";
# open logfile and redir stdout and stderr to it
my $logfile = "runtest.$$";
###open(STDOUT, ">$logfile") or die "Can't open output $logfile\n";
###open(STDERR, ">& STDOUT") or die "Can't dup STDERR to STDOUT\n";

###
### from here on out, ALL output goes to log file runtest.$$
###

# initialize te RunTest object
$runTest = new RunTest($logfile);
if ($runTest->getErrorText()) {
  die ("ERROR: Could not init RunTest: ". $runTest->getErrorText());
}

my $status = $runTest->runIndividualTest(join(' ', @ARGV), $envVars);
###print "exiting with $status from scripts/runtest\n";
exit($status);
