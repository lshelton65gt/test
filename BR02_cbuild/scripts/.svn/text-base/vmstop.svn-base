#!/bin/sh
# -*-Perl-*-
PATH=$CARBON_HOME/bin:$PATH
LANG=C
export LANG
exec perl -S -x -- "$0" "$@"
#!perl

# vmstop - simple script that uses CdsVM (which uses VMware) to stop
# the virtual machine running on the local host.

use strict;
use File::Basename;
use Getopt::Long;

use lib dirname($0);
use CdsVM;

$| = 1;

use vars qw($help $vmxPath
);

# process cmd line args
GetOptions(  'help'  => \$help,
	     'vmx'   => \$vmxPath,
);

# get vmx path from arg if supplied
unless($vmxPath) {
  # if not, check environment
  if (defined($ENV{CARBON_VMX_PATH})) {
    $vmxPath = $ENV{CARBON_VMX_PATH};
  # or default
  } else {
    $vmxPath = "/var/lib/vmware/Virtual_Machines/Windows XP Professional/Windows XP Professional.vmx";
    unless(-e $vmxPath) {
      $vmxPath = "/var/lib/vmware/Virtual_Machines/Windows_XP_Professional/Windows XP Professional.vmx";
      unless(-e $vmxPath) {
        $vmxPath = "/var/lib/vmware/Virtual_Machines/Windows_XP_Professional/Windows_XP_Professional.vmx";
      }
    }
  }
}

# make sure vmx file is really there
unless(-e $vmxPath) {
  &error("Could not find vmx file at '$vmxPath'\n");
}

# first use 'vmrun list' to determine if its running
my @listOutput = `vmrun list`;
if ($?) {
  &error("Could not run 'vmrun list' - giving up\n".join("\n",@listOutput)."\n");
}
unless(grep(/^$vmxPath$/,@listOutput)) {
  &error("No VM is running at '$vmxPath'\n");
}

`vmrun stop '$vmxPath'`;
if ($?) {
  &error("Could not stop VM: $!");
} 
print "VM powered off.\n";

sub error {
  my ($msg) = @_;
  die "ERROR: $msg\n";
}

sub warn {
  my ($msg) = @_;
  print "WARNING: $msg\n";
}
