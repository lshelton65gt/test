#!/bin/sh
# -*-Perl-*-
mypath=${0}
bindir=`dirname ${mypath}`
arch=${CARBON_HOST_ARCH:-`${bindir}/carbon_arch`}
exec $CARBON_HOME/$arch/bin/perl -I$CARBON_HOME/$arch/lib/perl -I$CARBON_HOME/$arch/lib/perl/site_perl -S -x -- "$0" "$@"
#!perl

#
# A tool to analyze the output from "-verboseFlattening 2". 
# 
# 1. What is the frequency for different module sizes?
# 
# 2. What is the minimum number of additional modules that would get
#    flattened if a particular size were used as the threshold?
# 
# 3. What is the overhead for flattening?
#
# See -help for detailed information.

use strict;
use warnings;
use FileHandle;
use Getopt::Long;

# Control variables
my $do_gnuplot = 0;
# GNUPlot control variables.
my $plot_type="jpg";
my $do_labels = 0;
my $max_x = 0;
my $max_y = 0;
my $help = 0;
my $do_xv = 0;
my $root = "flatten";

# Terminal counter for x11 plotting.
my $TerminalCount = 1;

# Prototypes
sub processFlatteningFile($$);
sub processCostFile($$);

my $result = GetOptions( "gnuplot!" => \$do_gnuplot,
			 "type=s"   => \$plot_type,
			 "root=s"   => \$root,
			 "x=i"      => \$max_x,
			 "y=i"      => \$max_y,
			 "xv!"      => \$do_xv,
			 "help"     => \$help );

sub showUsage()
{
    print << "EOHELP";

analyzeFlattening [options] <filename>

Process a flattening log or cbuild costs file and compute a number of
flattening-related statistics. The flattening log can be either the
'libdesign.flattening' file or output from running with the
'-verboseFlattening 2' switch. 

A table of flattening information and several data files are produced.

The following files are produced:

  flatten.gnu: A gnuplot script which generates plots for the three
              data files.

  flatten-frequency.dat: CSV file containing the frequency of
              flattening failures by module size.

  flatten-improvement.dat: CSV file containing magnitude (number of
              instances) of additional flattening opportunity by
              module size.

  flatten-cost.dat: CSV file containing cost (module size * frequency)
              of additional flattening opportunity by module size.

If plotting is enabled, the data files are displayed using gnuplot.
The following files are created by gnuplot:

  flatten-frequency.<ext>   Plot of flattening frequency data.
  flatten-improvement.<ext> Plot of flattening improvement data.
  flatten-cost.<ext>        Plot of flattening cost data.

The extension of these files is controlled by the '-type' switch
('jpg' by default). A plot type of 'x11' causes gnuplot to display the
plots but does not produce files containing the plots.

The improvement and cost files contain both the original data and a
bezier curve fit to the data.

If you do not want all the files to start with 'flatten', use the
-root argument.

Options:
  -gnuplot Enable plot generation via gnuplot.

  -type [jpg|ps|x11] The type of plot being generated. If 'jpg' or
           'ps' are used, the 'xv' program attempts to display the
           plots. If 'x11' is specified, gnuplot natively displays the
           plots; no plot files are generated.

  -root <name> Use 'name' as a prefix to all generated files. By
           default, 'flatten' is used.

  -x <number> Limit the plotted x-range to [0:<number>].

  -y <number> Limit the plotted y-range to [0:<number>].

  -xv      Use xv to display 'jpg' and 'ps' plots.

  -help    Display this usage information.

EOHELP
    exit 1;
}

if ( not $result ) {
    die("Error: Could not process command-line arguments. See '-help'.\n");
}

# check for help before other cmd-line error checking.
if ($help) { showUsage(); }

die("Error: -plot_type must be one of jpg, ps, or x11.\n") 
    unless ($plot_type eq "jpg" or $plot_type eq "ps" or $plot_type eq "x11");

# the data file should be the only unparsed part of the cmd-line.
die("Error: Only one log file can be processed: " . join(" ",@ARGV) . "\n") 
    if ($#ARGV>0);
die("Error: Please specify a log file.\n")
    unless ($#ARGV==0);

my $logfile = $ARGV[0];

# State variables.
my %childSizeFrequency;
my $DataSet = 0;

# Process the log file.
if ( $logfile =~ /\.costs$/ ) {
    print "Processing cost file: $logfile\n\n";
    processCostFile($logfile,\%childSizeFrequency);
} else {
    print "Processing flattening file: $logfile\n\n";
    processFlatteningFile($logfile,\%childSizeFrequency);
}


# process a log file containing flattening information.
sub processFlatteningFile($$) {
    my ($logfile, $childSizeFrequency) = @_;
    my $CBUILD = new FileHandle($logfile,"r") or
	die("Error: $logfile: $!\n");

    while (<$CBUILD>) {
	if ( /parent=(\d+) child=(\d+) comb=(\d+)/ ) {
	    next if ( /not a leaf/ ); # avoid non-leaves.
	    next if ( /Successfully flattened/ ); # avoid success.
	    my $childSize = $2;
	    if (not defined(${%$childSizeFrequency}{$childSize})) {
		${%$childSizeFrequency}{$childSize} = 0;
	    }
	    ++${%$childSizeFrequency}{$childSize};
	} elsif ( /^Flattening Summary/ ) {
	    # End of data set.
	    processData(0);
	}
    }
    $CBUILD->close();

    # end of file.
    processData(1);
}


# process a log file containing flattening information.
sub processCostFile($$) {
    my ($logfile, $childSizeFrequency) = @_;
    my $CBUILD = new FileHandle($logfile,"r") or
	die("Error: $logfile: $!\n");

    # throw away the first line.
    die ("Empty data file: $logfile\n") unless ( <$CBUILD> ); 

    # process all the remaining lines.
    while (<$CBUILD>) {
	my @parts = split /,/;
	my ($submods,$contassigns,$blkassigns) = @parts[3,11,12];
	if ($submods == 0) { # avoid non-leaves
	    my $childSize = $contassigns + $blkassigns;
	    if (not defined(${%$childSizeFrequency}{$childSize})) {
		${%$childSizeFrequency}{$childSize} = 0;
	    }
	    ++${%$childSizeFrequency}{$childSize};
	}
    }
    $CBUILD->close();

    # end of file.
    processData(0);
}


# Generate data files & gnuplot script.
sub processData($)
{
    my $forced = shift;

    ++$DataSet;

    # check for empty data set.
    if (not %childSizeFrequency) { 
	if (not $forced) {
	    print "Analysis for Flattening Pass: ${DataSet}\n";
	    print "************************************************************\n";
	    print "Empty.\n";
	    print "\n";
	}
	return; 
    }

    print "Analysis for Flattening Pass: ${DataSet}\n";
    print "************************************************************\n";
    print "\n";

    my $frequency_base = "${root}-frequency${DataSet}";
    my $improvement_base = "${root}-improvement${DataSet}";
    my $cost_base = "${root}-cost${DataSet}";

    my $frequency_data = $frequency_base . ".dat";
    my $improvement_data = $improvement_base . ".dat";
    my $cost_data = $cost_base . ".dat";

    my $FREQ = new FileHandle(${frequency_data},"w") or
	die("Error: ${frequency_data}: $!\n");
    my $IMPR = new FileHandle(${improvement_data},"w") or
	die("Error: ${improvement_data}: $!\n");
    my $COST = new FileHandle(${cost_data},"w") or
	die("Error: ${cost_data}: $!\n");

    # Add some header information to each of the data files.
    print $FREQ "# Flattening frequency\n";
    print $FREQ "# size,frequency\n";
    print $FREQ "0,0\n";
    print $IMPR "# Flattening improvement\n";
    print $IMPR "# size,improvement\n";
    print $IMPR "0,0\n";
    print $COST "# Flattening cost\n";
    print $COST "# size,cost\n";
    print $COST "0,0\n";

    printf("%15s%15s%15s%15s\n","Module Size","Frequency","Improvement","Cost");
    print("------------------------------------------------------------\n");
    my $improvement = 0;
    my $cost = 0;
    foreach my $childSize (sort {$a<=>$b} keys(%childSizeFrequency)) {
	my $frequency = $childSizeFrequency{$childSize};
	$cost += ($frequency * $childSize);
	$improvement += $frequency;
	print $FREQ "$childSize,$frequency\n";
	print $IMPR "$childSize,$improvement\n";
	print $COST "$childSize,$cost\n";
	printf("%15d%15d%15d%15d\n",$childSize,$frequency,$improvement,$cost);
    }
    print("------------------------------------------------------------\n");

    $FREQ->close();
    $IMPR->close();
    $COST->close();

    print "Recommended flattening threshold: (NYI)\n";

    my @plots = ();

    my $gnu_filename = "${root}${DataSet}.gnu";
    my $GNU = new FileHandle(${gnu_filename},"w") or
	die("Error: ${gnu_filename}: $!\n");

    print $GNU "set title '${root}${DataSet}'\n";
    push @plots, gnuplotOutput($GNU,$plot_type,$frequency_base);

    # labels have to come before the plot -- otherwise, it won't show.
    if ($do_labels) {
	gnuplotLabels($GNU,\%childSizeFrequency);
    }

    gnuplotData($GNU,"Flattening Size Frequency",
		${frequency_data},
		0,
		$max_x, $max_y);
    print $GNU "show output\n";


    push @plots, gnuplotOutput($GNU,$plot_type,$improvement_base);
    gnuplotData($GNU,"Flattening Improvement (instances)",
		${improvement_data},
		1,
		$max_x, $max_y);
    print $GNU "show output\n";


    push @plots, gnuplotOutput($GNU,$plot_type,$cost_base);
    gnuplotData($GNU,"Flattening cost (assigns)",
		${cost_data},
		1,
		$max_x, $max_y);
    print $GNU "show output\n";

    $GNU->close();

    if ($do_gnuplot) {

	# send the generated script through gnuplot
	system("gnuplot -persist < ${gnu_filename}");

	# show the output.
	if ($do_xv) {
	    foreach my $plot (@plots) {
		if ($plot_type eq "jpg" or $plot_type eq "ps") {
		    system("xv $plot &");
		}
	    }
	}
    } else {
	print "Skipping gnuplot.\n";
    }

    print "\n";

    # Update state for next data set.
    %childSizeFrequency = ();
}


# print $GNU "set logscale\n";
# print $GNU "set logscale y 2\n";

# print $GNU "set noautoscale\n";
# print $GNU "set samples 50\n";
sub gnuplotOutput($$$)
{
    my ($GNU,$plot_type,$filebase) = @_;
    my $filename = "$filebase.$plot_type";
    if ($plot_type eq "jpg") {
	print $GNU "set terminal jpeg\n";
	
    } elsif ($plot_type eq "ps") {
	print $GNU "set terminal postscript\n";
    } elsif ($plot_type eq "x11") {
	my $terminal = $TerminalCount++;
	print $GNU "set terminal x11 $terminal\n";
    }

    if ($plot_type ne "x11") {
	# x11 ignores output. don't create a file.
	print $GNU "set output '$filename'\n";
    }
    return $filename
}

sub gnuplotLabels($$)
{
    my ($GNU,$childSizeFrequency) = @_;
    foreach my $childSize (sort {$a<=>$b} keys(%{$childSizeFrequency})) {
	my $frequency = ${%{$childSizeFrequency}}{$childSize};
	print $GNU "set label '($childSize,$frequency)' at $childSize,$frequency\n";
    }
}

sub gnuplotData($$$$$$)
{
    my ($GNU,$title,$datafile,$do_smooth,$max_x,$max_y) = @_;

    # with dots
    # The original data.
    my @plots = ("'$datafile' using '%lf,%lf' title '$title' with points",);

    if ($do_smooth) {
	# other smooth types: acsplines csplines unique sbezier
	my $smoothType = "smooth bezier";
	push @plots, "'$datafile' using '%lf,%lf' $smoothType title '$title' with lines";
    }

    my $plot = join(", ",@plots);
    
    my $x_range = ($max_x > 0) ? "[0:$max_x]" : "[0:]";
    my $y_range = ($max_y > 0) ? "[0:$max_y]" : "[0:]";

    print $GNU "plot $x_range $y_range $plot\n";
}


