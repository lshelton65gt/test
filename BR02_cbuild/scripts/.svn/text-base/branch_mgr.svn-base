#!/bin/sh
# -*-Perl-*-
exec ~qa/QATools/bin/perl -S -x -- "$0" "$@"
#!perl

#-----------------------------------------------------------------------
# Author Dave Allaby. dallaby@carbondesignsystems.com
#
# This script will toggle the creation of a lock directory in qa's home.
#
# The commit script, using methods in Commit.pm module will be 
# looking for this lock file.
#
# This script takes one arg or no args.
# 
# Arg is the cvs tag you want to lock. There's no validity checking on
# a tag.  It will lock the exact string you give it, so be correct.
# else you'll lock an irrelevant tag.  Anything a user would pass to the
# cvs -r $value where $value is the string to lock, such as 
# BR_C2008_04_6257.
#
#-----------------------------------------------------------------------

use strict;
use File::Path;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSFileNames;

# Needed variables.
#----------------------
my $user   = $ENV{USER};
my $branch = $ARGV[0];
my $rtn    = 1;


# Only qa can run this.
#-----------------------
if ($user ne "qa")
{
    print "User $user is not authorized to run this script.\n";
}
else
{
    my $fn = new CDSFileNames();
    my $branchLockDir = $fn->CommitLockDir($branch);
    my $msg = "";
    $rtn = 0;

    # The CDSFileNames::CommitLockDir() will default the 
    # branch to "mainline" in the directory path.  This will happen
    # if we weren't passed a first argument.
    # We really only need $branch for pretty messages, so I'll just
    # pull the filename portion of the path to get $branch if no
    # arg 1 was given.

    if (! $branch)
    {
        ($branch = $branchLockDir) =~ s/^.+\///g;
    }

    if (-d $branchLockDir)
    {
        $msg = "Unlocking ";
        rmtree ($branchLockDir);
    }
    else
    {
        $msg = "Locking ";
        mkpath ($branchLockDir);
    }

    $msg .= "the commit script for the $branch branch.\n".
        "The $branch branch is ";

    if (-d $branchLockDir)
    {
        $msg .= "CLOSED.";
    }
    else
    {
        $msg .= "OPEN.";
    }

    print "$msg\n";
}

exit $rtn;
