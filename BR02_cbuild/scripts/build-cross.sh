#!/bin/sh
# 
# Updated 1-12-2004 by apl.
#
# Created version in scripts/build-cross.sh that creates
#          /tools/<arch>/gcc-<ver>-mingw
#
# This is my script for building a complete cross-compiler toolchain.
# It is based partly on Ray Kelm's script, which in turn was built on
# Mo Dejong's script for doing the same, but with some added fixes.
# The intent with this script is to build a cross-compiled version
# of the current MinGW environment.
#
# Updated by Sam Lantinga <slouken@libsdl.org>
# 
# Updated Dec 22, 2003 by ArrAy, Inc for CDS.
#   1. Update version numbers to 3.3.2, arcive sites (favor gnu over MinGW)
#   2. Allow for Solaris peculitarites. 
#   3. Assure that the search path is correct for the cross utilities 
#      and that the target and source directories are relative to the 
#      working directory ("TOPDIR")
#   4. Run steps sequentially, not concurrrently, to avoid race conditions
#
# The original file was downloaded from 
# http://www.libsdl.org/extras/win32/cross/build-cross.sh

usage ()
{
    echo "$0 [-install | -help]"
    cat <<EOF
  build the MinGW tool chain.
  There are lots of environment variables that can be overridden here.
  GCC_PATCH is the most interesting one.  
EOF
}

# what flavor are we building?
if [ $# = 0 ]; then
    true;                       # no args
elif [ x$1 = x-help ]; then
    usage
    exit 0
elif [ x$1 == x-install ]; then
    true;
else
    usage
    exit 1
fi



TARGET=i386-mingw32msvc

# What is the host. Solaris (a.k.a, SunOS) needs special attention.

HOST=`$CARBON_HOME/scripts/carbon_arch`

# Get the "top" or "working" directory.

TOPDIR=`pwd`
SRCDIR="$TOPDIR/mingw-source"

# where does it go?
case $HOST in
Linux) ARCHDIR=linux
       if ( grep 'Red Hat Linux 7.3' /proc/version ); then
           echo ""
       else
           echo "Must build on RH7.3 system so that we can run on farm"
           exit 1
       fi
       ;;
SunOS) ARCHDIR=solaris;;
*)     echo "Unknown host architecture."
       exit 1
       ;;
esac

BASEDIR=/tools/${ARCHDIR}

eval `cat ${CARBON_HOME}/scripts/compiler_version`

# Set the compiler we want to use to build all this stuff...
# (GCC_VERSION comes from the compiler_version script)

export CC=${BASEDIR}/gcc-${GCC_VERSION}/bin/gcc

GCC=gcc-${GCC_MINGW}

echo using MINGW from  ${MINGW_URL:=http://heanet.dl.sourceforge.net/sourceforge/mingw}
echo using GCC      from ${GCC_URL:=ftp://mirrors.rcn.net/pub/sourceware/gcc/releases/${GCC}}
echo using BINUTILS from ${BINUTIL_URL:=http://ftp.gnu.org/gnu/binutils}



GCC_ARCHIVE=${GCC}.tar.bz2
echo $0: GCC_PATCH set to ${GCC_PATCH:=}
BINUTILS=binutils-2.16.1
BINUTILS_ARCHIVE=$BINUTILS.tar.gz
MINGW=mingw-runtime-3.8
MINGW_ARCHIVE=$MINGW.tar.gz
W32API=w32api-3.6
W32API_ARCHIVE=$W32API.tar.gz

# These are the files from the SDL website

SDL_URL=http://www.libsdl.org/extras/win32/common
OPENGL_ARCHIVE=opengl-devel.tar.gz
DIRECTX_ARCHIVE=directx-devel.tar.gz


# Where do we install built tools - in the corresponding area matching
# the GCC we used
echo Installing MINGW tool chain into ${PREFIX:=${BASEDIR}/gcc-${GCC_VERSION}-mingw}


# need install directory first on the path so gcc can find binutils
# SunOS places the gcc binaries in a distinct location

if [ "$HOST" = "SunOS" ]; then
    PATH="$PREFIX/bin:/usr/ccs/bin:$PATH"
    export PATH
else
    PATH="$PREFIX/bin:$PATH"
fi

#
# download a file from a given url, only if it is not present and not already in
# /tools/tgz
#

download_file()
{
	cd "$SRCDIR"
        if test -e /tools/TGZ/$1 ; then
            ln -sf /tools/TGZ/$1
            echo "found $1 in /tools/TGZ"
        elif test ! -e $1 ; then
		echo "Downloading $1"
		wget "$2/$1"
		if test ! -e $1 ; then
			echo "Could not download $1"
			exit 1
		fi
	else
		echo "Found $1 in the srcdir $SRCDIR"
	fi
  	cd "$TOPDIR"
}

download_files()
{
	mkdir -p "$SRCDIR"
	
	# Make sure wget is installed
	if test "x`which wget`" = "x" ; then
		echo "You need to install wget."
		exit 1
	fi
	download_file "$GCC_ARCHIVE" "$GCC_URL"
	download_file "$BINUTILS_ARCHIVE" "$BINUTIL_URL"
	download_file "$MINGW_ARCHIVE" "$MINGW_URL"
	download_file "$W32API_ARCHIVE" "$MINGW_URL"
	download_file "$OPENGL_ARCHIVE" "$SDL_URL"
	download_file "$DIRECTX_ARCHIVE" "$SDL_URL"
}

# Create the target install directory with open permissions
create_install_dir()
{
    sudo mkdir -p $PREFIX; sudo chown $USER $PREFIX
}

install_libs()
{
	echo "Installing cross libs and includes"
	mkdir -p "$PREFIX/$TARGET"
	cd "$PREFIX/$TARGET"
	gzip -dc "$SRCDIR/$MINGW_ARCHIVE" | tar xf -
	gzip -dc "$SRCDIR/$W32API_ARCHIVE" | tar xf -
	gzip -dc "$SRCDIR/$OPENGL_ARCHIVE" | tar xf -
	gzip -dc "$SRCDIR/$DIRECTX_ARCHIVE" | tar xf -
	cd "$TOPDIR"
}

extract_binutils()
{
    if [ ! -e $SRCDIR/$BINUTILS ]; then
	cd "$SRCDIR"
	rm -rf "$BINUTILS"
	echo "Extracting binutils"
	gzip -dc "$SRCDIR/$BINUTILS_ARCHIVE" | tar xf -
	cd "$TOPDIR"
    else
        echo binutils $SRCDIR/$BINUTILS already extracted
    fi
}

configure_binutils()
{
	cd "$TOPDIR"
	rm -rf "binutils-$TARGET"
	mkdir -p "binutils-$TARGET"
	cd "binutils-$TARGET"
	echo "Configuring binutils"
	"$SRCDIR/$BINUTILS/configure" --prefix="$PREFIX" --target=$TARGET > configure.log
	cd "$TOPDIR"
}

build_binutils()
{
	cd "$TOPDIR/binutils-$TARGET"
	echo "Building binutils"
        
        ## *  Make the directory as SunOS can not recover if it's not there.
        if [ "$HOST" = "SunOS" ]; then
            mkdir -p gcc/fixinc/fixincl
        fi
        
	gmake > make.log
	if test $? -ne 0; then
		echo "make failed - log available: binutils-$TARGET/make.log"
		exit 1
	fi
	cd "$TOPDIR"
}

install_binutils()
{
	cd "$TOPDIR/binutils-$TARGET"
	echo "Installing binutils"
	gmake DESTDIR=/n INSTALLDIR=$PREFIX install > make-install.log
	if test $? -ne 0; then
		echo "install failed - log available: binutils-$TARGET/make-install.log"
		exit 1
	fi
	cd "$TOPDIR"
}

extract_gcc()
{
    if [ ! -e $SRCDIR/$GCC ]; then
	cd "$SRCDIR"
	rm -rf "$GCC"
	echo "Extracting gcc"
#	gzip -dc "$SRCDIR/$GCC_ARCHIVE" | tar xf -
        bunzip2  -dc "$SRCDIR/$GCC_ARCHIVE" | tar xf -
	cd "$TOPDIR"
    else
        echo "gcc $SRCDIR/$GCC already extracted from archive"
    fi
}

patch_gcc()
{
	if [ "$GCC_PATCH" != "" ]; then
		echo "Patching gcc"
		cd "$SRCDIR/$GCC"
		patch -p1 < "$GCC_PATCH"
		cd "$TOPDIR"
	fi
}

configure_gcc()
{
	cd "$TOPDIR"
	rm -rf "gcc-$TARGET"
	mkdir -p "gcc-$TARGET"
                
	cd "gcc-$TARGET"
	echo "Configuring gcc"
	"$SRCDIR/$GCC/configure" -v \
		--prefix="$PREFIX" --target=$TARGET \
                --enable-languages=c++ \
		--with-headers="$PREFIX/$TARGET/include" \
		--with-gnu-as --with-gnu-ld \
                --with-as=i386-mingw32msvc-as \
                --with-ld=i386-mingw32msvc-ld \
		--without-newlib --disable-multilib > configure.log
	cd "$TOPDIR"
}

build_gcc()
{
	cd "$TOPDIR/gcc-$TARGET"
        
        ## *  Make the directory as SunOS can not recover if it's not there.
        if [ "$HOST" = "SunOS" ]; then
            mkdir -p gcc/fixinc/fixincl
        fi
        
	echo "Building gcc"
        #
        # Add the release area binutils to our path so that GCC can invoke
        # the peCOFF assembler and linker.
        #
	PATH=$PATH:$PREFIX/bin gmake LANGUAGES="c c++" > make.log
	if test $? -ne 0; then
		echo "make failed - log available: gcc-$TARGET/make.log"
		exit 1
	fi
	cd "$TOPDIR"
}

install_gcc()
{
	cd "$TOPDIR/gcc-$TARGET"
	echo "Installing gcc"
	gmake DESTDIR=/n INSTALLDIR=$PREFIX LANGUAGES="c c++" install > make-install.log
	if test $? -ne 0; then
		echo "install failed - log available: gcc-$TARGET/make-install.log"
		exit 1
	fi
	cd "$TOPDIR"
}

final_tweaks()
{
	echo "Finalizing installation"

	# remove gcc build headers
	rm -rf "$PREFIX/$TARGET/sys-include"

        # Add extra binary links
	if [ ! -f "$PREFIX/$TARGET/bin/objdump" ]; then
		ln "$PREFIX/bin/$TARGET-objdump" "$PREFIX/$TARGET/bin/objdump"
	fi

	# make cc and c++ symlinks to gcc and g++
	if [ ! -f "$PREFIX/$TARGET/bin/g++" ]; then
		ln "$PREFIX/bin/$TARGET-g++" "$PREFIX/$TARGET/bin/g++"
	fi
	if [ ! -f "$PREFIX/$TARGET/bin/cc" ]; then
		ln -s "gcc" "$PREFIX/$TARGET/bin/cc"
	fi
	if [ ! -f "$PREFIX/$TARGET/bin/c++" ]; then
		ln -s "g++" "$PREFIX/$TARGET/bin/c++"
	fi

	# strip all the binaries
#	ls "$PREFIX"/bin/* "$PREFIX/$TARGET"/bin/* | egrep -v '.dll$' |
#	while read file; do
#	    $BASEDIR/bin/strip "$file"
#	done

	# change owner to root...
	ssh carbon "sudo chown -R root $PREFIX"
   
	echo "Installation complete!"
        echo "YOU MUST COPY THIS TO THE NETAPP's /tools"

}

create_install_dir

download_files

install_libs

extract_binutils

configure_binutils

build_binutils

install_binutils

extract_gcc

patch_gcc

configure_gcc

build_gcc

install_gcc

final_tweaks



