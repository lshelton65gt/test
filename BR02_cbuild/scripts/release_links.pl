#!/bin/sh
# -*-Perl-*-
exec ~qa/QATools/bin/perl -S -x -- "$0" "$@"
#!perl

use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use lib "$ENV{CARBON_HOME}/scripts";

use ReleaseLinks;
use CDSFileNames;

my $fn = new CDSFileNames();
my $args = new CarbonConfigArgs($fn);

my $relLink = new ReleaseLinks("Linux", $fn, $args);


$relLink->CarbonConfigReleaseList();

exit ;
