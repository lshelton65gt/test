#!/bin/sh
# -*-Perl-*-
exec ~qa/QATools/bin/perl -S -x -- "$0" "$@"
#!perl


use Getopt::Long;
use File::Spec::Functions;
use File::Listing;
use File::stat;
use File::Basename;
use strict;
use lib "$ENV{CARBON_HOME}/scripts";
use CdsUtil;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSFileNames;
use Commit;


# Globals
## if you change the value of gWORKBASEDIR then you must also change WORKBASEDIR in checkin script
my $gWORKBASEDIR = "/o/work/$ENV{USER}/checkin";

my $gEdFile = catfile(rootdir(), "tmp", $ENV{USER} . "." . $$);
my $gNoPopup = 1;
my $gAllowAdds = 0;
my $gMsgSeparator = "@@@@ DO NOT EDIT THIS LINE @@@@";
my $gReleaseRequiredNote = "NOTE: You are committing to a release branch - the Bugs and Release note fields are required.";
my $gVersionInMods = 0;
my $gInfraVersionInMods = 0;
my $gCommitLogFile = catfile(&getHomeDir(), "last_commit_msg.txt");
my $gCleanCommitLogFile = 1;
my $gCleanDiffFile = 0;
my @gMsgLines = ();
my $gUseFOption = 0;
my $gRemoveUnknowns = 0;
my $gNoConfirm = 0;
my $gNoCheckinValidation = 0;
my $gDiffFile = "";
my $gCommitPlVersion = '$Revision: 1.69 $ $Date: 2011/09/22 13:52:15 $';
my $gMsg = "";
my $gDiffsArea = "/home/cds/qa/data/diffs";
my $fn = new CDSFileNames();
my $Commit = new Commit($fn);

if ($Commit->BranchFrozen())
{
    die "\nBranch ", $Commit->SandboxTag(), " is currently frozen by qa.\n",
    "Contact qa for information about committing to this branch.\n\n";
}
else
{
    print "\nBranch ", $Commit->SandboxTag(), " is open for commits ...\n",
}

# out of desperation, i'm appending all cvs commit cmd lines to a log
# temporarily so i can determine why the version files are not being
# reordered in many cases...  -srp 5/6/04
my $gDateStamp = `date`; chomp($gDateStamp);
my $desperateDebugStr = $gDateStamp;
$desperateDebugStr .= "\ncommit.pl rev: $gCommitPlVersion\n";

$| = 1;                 # don't buffer output

&readConfigFile;
my $lastMsgLineIndex = &maybeRetrieveMsg;
my ($commitLogFile) = "";
use vars qw ($gMsg $gUseFOption $gCommitLogFile $gCleanCommitLogFile
  $gMsgIndexNumber $gEdFile $gCleanDiffFile $gDiffFile $gVersionInMods
  $gVersionFile $gInfraVersionInMods
);
GetOptions("m=s", \$gMsg,
           "F=s", \$commitLogFile);
    
if (length($ENV{EDITOR}) == 0) {
    die "Must have EDITOR env set";
}
if (length($commitLogFile)) {
    $gUseFOption = 1;
    &grabCommitMsgFromFile($commitLogFile);
    $gCommitLogFile = $commitLogFile;
    $gCleanCommitLogFile = 0;
}

my $gVersionFile = catfile("src", "util", "UtVersion.cxx");
my $gInfraVersionFile = catfile("scripts", "InfraVersion.pm");

# is the version file in the sandbox checked out to a release branch? if so,
# the branch tag will be in gBranchCommit after this call
my $gBranchCommit = &CdsUtil::isFileOnReleaseBranch($gVersionFile);
if ($gBranchCommit eq '-1') {
  die "Problem analyzing $gVersionFile, there is a problem with your sandbox... (try setting RUNLIST_DEBUG and retrying)\n";
}

# make sure we are at top of sandbox by looking for scripts/commit.pl
unless(-f "scripts/commit.pl") {
  die "Must run commit script from top of sandbox\n";
}

# solves message.index problem
my $diffDBDir = &getDiffDBDir();
my $indexFile = "$diffDBDir/message.index" ;
if (! -e $indexFile) {
    # first create the user directory(even if it somehow exists already this is ok)
   `mkdir -p $diffDBDir`;
   `touch $indexFile` ;
   `chmod 755 $indexFile` ;
}
    
# Have consistent times in message index and UtVersion.
my $gNowString = localtime;

# Josh says use specific cvs, not one in path to commit.
my $cvs = catfile(rootdir(), "usr", "bin", "cvs");
my $st = stat($cvs);
if (! $st) {
    # Try /usr/local/bin (most likely on solaris)
    $cvs = catfile(rootdir(), "usr", "local", "bin", "cvs");
    $st = stat($cvs);
    if (! $st) {
        &error ("Cannot find cvs in /usr/bin or /usr/local/bin");
    }
}
        
print STDOUT "\nChecking user $ENV{USER}'s sandbox. [ ",
    $fn->FindSandboxPath(), " ]\n",
    "Looking for modified and new files ...";

my $cvsUpOut = `$cvs -fn update 2>&1`;
#print STDOUT $cvsUpOut;
my @cvsOutLines = split(/\n/, $cvsUpOut);
    
#print STDOUT "Num lines: " . ($#cvsOutLines + 1) . "\n";

my @mods = ();
my @adds = ();
my @conflicts = ();
&extractFileLines(\@mods, \@adds, \@conflicts, \@cvsOutLines);

chomp(@mods);
chomp(@adds);
chomp(@conflicts);

&checkForConflicts(@conflicts);

if ($gRemoveUnknowns != 0) {
    @adds = ();
}

&error ("No cvs files to commit. Exiting ") unless (($#adds >= 0) || ($#mods >= 0));
    
&error ("Unable to open $gEdFile for writing: $!") unless open(OUT, ">$gEdFile");

if ((length($gMsg) == 0) || ($lastMsgLineIndex >= 0)) {
    # Get the message from the user.
    if (length($gMsg) == 0) {
        print STDOUT "\nPrint a commit message in the editor window\n";
        #print OUT "\# Print a commit message here.  Do not forget:   reviewed by: <reviewer>";
        print OUT "\# Print a commit message here.  Include reviewer and tests done info, as shown\n";
        print OUT "Reviewed by: \n" ; 
        print OUT "Tests Done: \n" ; 
        print OUT "Bugs Affected: \n" ; 
        if ($gBranchCommit) {
	  print OUT "Release note reviewed by: \n\n";
	  print OUT $gReleaseRequiredNote . "\n";
        }
    } 
    else {
        print STDOUT "\nFound a previous commit message. You may edit it now.\n";
        &writeArrayToFile(\*OUT, \@gMsgLines);
    }
        
    print OUT "\n\n\n";
    print OUT  $gMsgSeparator . "\n\n";
    &writeAddsModsToFile(\*OUT, \@adds, \@mods);
    close(OUT);
    &launchEditor($gEdFile);
    &grabCommitMsgFromFile($gEdFile, $gCommitLogFile, $lastMsgLineIndex);
    &error ("Message length must be non-zero") unless (length($gMsg) > 0);
    $gNoPopup = 1; 
    $gUseFOption = 1;
    $gCleanCommitLogFile = 1;
}
else {
    &writeAddsModsToFile(\*OUT, \@adds, \@mods);
    close(OUT);
}
    
$gMsg = "'" . $gMsg . "'";
    
print STDOUT "\nmessage: ";
if ($#gMsgLines >= 0) {
    print STDOUT "\n";
    &writeArrayToFile(\*STDOUT, \@gMsgLines);
}
else {
    print STDOUT $gMsg . "\n";   
}
print STDOUT "\n";
if ($gNoPopup == 0) {
    &launchEditor($gEdFile);
}

my(@EdListLines) = ();
&grabFileLines($gEdFile, \@EdListLines);

my $prevModCnt = $#mods;
@mods = ();
@adds = ();
if ($#EdListLines >= 0) {
    &extractFileLines(\@mods, \@adds, \@conflicts, \@EdListLines);
}
else {
    &error ("No cvs files to commit. Exiting ");
}
    
# unneeded, but safe
&checkForConflicts(@conflicts);

#print STDOUT "ADDS:\n";

chomp(@adds);
chomp(@mods);
    
if ($#adds >= 0) {
    &writeArrayToFile(\*STDOUT,  \@adds);
    print STDOUT "\n";        
    
    if ($gAllowAdds == 0) {
        &error ("No \? files allowed.");
    } else {
        print STDOUT "!!! ADDING FILES !!!";
    }
}
    
my(@addFiles) = ();
my(@modFiles) = ();
&extractFiles(\@adds, \@mods, \@addFiles, \@modFiles);

my @committed = (@addFiles, @modFiles);

# Now check that there is a successful checkin runlist.results and an empty
# runlist.failed in the checkin build directory for this sandbox, and that its
# start date is LATER than any modifications to the sources
# (unless NO_CHECKIN_VALIDATION was set in config file)
#
unless ($gNoCheckinValidation) {
  &doCheckinVerification(@committed);
}


if ($#committed >= 0) {
    print STDOUT "\n\nList of candidates:\n";
    &writeArrayToFile(\*STDOUT, \@committed);

    if ($gNoConfirm == 0) {
        my $resp = &ask("\nContinue?");
        if ($resp == 0) {
            $gCleanCommitLogFile = 0;
            &cleanup;
            exit(0);
        }
    } # if
}
else {
    print STDERR "No files to commit.\n";
    exit(1);
}
#-------------------------------------------------------------    
# do a diff, write a diff zip file. Return update mode, 1 or 2.
#-------------------------------------------------------------
my $updateVersion = &doContextDiff(@modFiles);

#-------------------------------------------------------------
# Update version file, return valid updated file name, either
# src/util/UtVersion.cxx or scripts/InfraVersion.pm
# If we find it, update and return name.  No name = problem.
#-------------------------------------------------------------

my $relevantVersionFile = &selectUpdateVersionFile($updateVersion);

#-------------------------------------------------------------
# If not file returned, we don't have the correct version
# file to edit, or can't find it cause we aren;t where we should
# be. Issue error, suggest how to fix it and exit.
#-------------------------------------------------------------

if (! $relevantVersionFile || ! -f "$relevantVersionFile")
{
    issueNoVersionFileError($updateVersion);
}

if ($#addFiles >= 0) {
    my @args = ($cvs, "-f", "add", @addFiles);
    
    &error ("Unable to run cvs add") unless (system(@args) == 0);
    
    print STDOUT "\n\nList of added files:\n";
    &writeArrayToFile(\*STDOUT, \@addFiles);
    print STDOUT "\n";
}
    

if ($#committed >= 0) {
    # move gVersionFile to end of list so that anyone who looks to it
    # for a cvs timestamp will surely get an accurate one (sort of)
    my $hasVersion = 0; 
    for(my$i = 0; $i <= $#committed; $i++) {
      if ($committed[$i] =~ m/^\s*$relevantVersionFile\s*$/o) {
        $hasVersion = splice(@committed, $i, 1);
      }
    }
    if ($hasVersion) {
      push(@committed, $relevantVersionFile); 
    } else {
      print "WARNING: You are not committing a version file with your changes\n";
    }

    my $cmd = ""; my $bugList;
    if (length($gMsg) > 0) {
        my $msgOption = "-m";
        my $msg = $gMsg;
        if ($gUseFOption != 0) {
            $msgOption = "-F";
            $msg = $gCommitLogFile;
	    # if on branch, validate log
	    if ($gBranchCommit) {
	      open(COMMITMSG, $gCommitLogFile) or
                &error("can't read $gCommitLogFile\n");
	      my $msgContents = join('', <COMMITMSG>);
	      $bugList = &validateCommitMsg($msgContents);
	    } else {
	      $bugList = &validateCommitMsg($gMsg);
            }
        }
        # if on branch and have no bugs, fail (unless qa, in which case its
        # the version update
  	if ($gBranchCommit and (not $bugList or (scalar(@$bugList) < 1)) and
	    ($ENV{USER} ne 'qa')) {
	  &error("You are committing to a branch and did not specify either bug numbers or release note reviewer in your commit log.\n");
	}

        $cmd = "$cvs -f commit $msgOption $msg ". join(" ", @committed);
        #@args = ($cvs, "-f", "commit", $msgOption, $msg, @committed);
         #writeArrayToFile(\*STDOUT, \@args);
    }
        
    `echo '$desperateDebugStr$cmd\n' >> $gDiffsArea/cvs.cmd.log`;
    unless(open(CVSCIOUT, "$cmd |")) {
        $gCleanCommitLogFile = 0;
        $gCleanDiffFile = 1;
        &error ("Unable to run cvs commit: $!");
    }
    my $lastFileCommitted;
    while(<CVSCIOUT>) {
      print $_;
      if (/^Checking in (.*)\;$/) {
         $lastFileCommitted = $1;
      }
    }
    close(CVSCIOUT);
    if ($? == 0) {
        &updateMessageIndex($lastMsgLineIndex);
        # write an xml snippet with commit info so the branch bugs list on the
        # intranet is accurate
	&CdsUtil::writeBranchCommitInfo($bugList, $gBranchCommit, $ENV{USER}, $gMsgIndexNumber, $gDateStamp);

        if (length($commitLogFile) == 0) {
            $gCleanCommitLogFile = 1;
        }
        $gCleanDiffFile = 0;
    }
    else {
        $gCleanCommitLogFile = 0;
        $gCleanDiffFile = 1;
        &error ("cvs commit exited non-zero: $!");
    }
        
    print STDOUT "\n\n\nList of committed files:\n";
    &writeArrayToFile(\*STDOUT, \@committed);
    print STDOUT "\n";
    print STDOUT "Commit \# " . $gMsgIndexNumber . " (" . $ENV{USER} . ")\n";

    if ($updateVersion == 1) 
    {
	&printUtVersion();
    } 
    else
    {
	&printInfraVersion();
    }

    print STDOUT "\n\n";

    # make sure lastFileCommitted isn't empty - if it is, issue a warning
    if (not $lastFileCommitted) {
      print STDOUT "WARNING: I can't tell if your version file has the correct".
          " timestamp\n  Verify that $relevantVersionFile was the last file".
	  " committed\n";
    # finally, check to see if the last file committed has a later timestamp
    # than the version file. if so, that's bad and we'll have to correct it
    } elsif ($relevantVersionFile and 
        ($lastFileCommitted ne $relevantVersionFile)) {
      # last file committed was different than version file. check times
      my $versionTime = &getTimestamp($relevantVersionFile);
      my $lastFileTime = &getTimestamp($lastFileCommitted);
          
      # if lastFileCommitted is newer (bigger num), then need to update
      # version file and commit it again
      if ($lastFileTime > $versionTime) {
        print "\nNOTE: Version file is already out-of-date! Will update and try again...\n\n";
        sleep(2);
        $gNowString = localtime;
        &doVersionUpdate($relevantVersionFile);
        `$cvs -f commit -m "Auto-syncing version file with $ENV{USER} commit num $gMsgIndexNumber" $relevantVersionFile`;
        if($? != 0) {
          print "\nERROR: There was a bizarre problem commiting an ".
            "updated $relevantVersionFile\n";
          print "ERROR: Please ask around until someone knows what you're ".
            "talking about.\n";
          print "ERROR: Oh, and see bug 4070...\n";
        }
      }
    }
}
    
&cleanup;
exit 0;



sub issueNoVersionFileError
{
    my ($uv) = (@_);
    my $file = $gVersionFile;
    my $module = "src/";

# Pick out file and module name based on 1 or 2
# Print appropriate message string.

    if ($uv != 1)
    {
	$file = $gInfraVersionFile;
	$module = "scripts/";
    }

    my $msg = "You can only run this from the top of your sandbox.  ";
    $msg .= "Checkout all of $module and try again.\n";
    &error ("No $file: $!\n$msg");
}

#---------------------------------------------------------
# Picks out correct version file based on $ver (1 or 2)
# Updates file, returns version file name to caller if
# if worked correctly.
#---------------------------------------------------------
sub selectUpdateVersionFile
{
    my ($ver) = (@_);
    my $relFile = "";
    my $msg = "";
    
    if ($ver == 1) 
    {
	if (-f "$gVersionFile")
	{
	    &doVersionUpdate($gVersionFile);
	    
	    if ($gVersionInMods == 0) 
	    {
		$committed[$#committed + 1] = $gVersionFile;
	    }
	    
	    $relFile = $gVersionFile;
	}
    }
    else
    {
	if (-f "$gInfraVersionFile")
	{
	    &doVersionUpdate($gInfraVersionFile);
	    
	    if ($gInfraVersionInMods == 0) 
	    {
		$committed[$#committed + 1] = $gInfraVersionFile;
	    }
	    
	    $relFile = $gInfraVersionFile;
	}
    }
    
    return $relFile;
}

# return cvs timestamp of supplied file in seconds since epoch

sub getTimestamp {
  my ($file) = @_;
  my $cmd = "cd ".dirname($file).";grep ".basename($file)." CVS/Entries | cut -d '/' -f 4 |tail -1";
  my $timestr = `$cmd`; chomp $timestr;
  my $timesec = `date -d '$timestr' +\%s`; chomp $timesec;
  # make sure only digits are in timesec
  unless($timesec =~ /^\d+$/) {
    return 0;
  }
  return $timesec;
}

sub cleanup {
    unlink($gEdFile);
    if ($gCleanCommitLogFile != 0) {
        unlink($gCommitLogFile);
    }
    if ($gCleanDiffFile != 0) {
        unlink($gDiffFile);
    }
}

sub error {
    my $errMsg = "Error: " . $_[0] . "\n";
    &cleanup;
    die $errMsg;
    exit(1);
}

# make sure that commit msg contains a bug number and a release note review line
# (this routine should only be called if we're committing to a branch)
# return a list of bug numbers, or a list of size 0 if not valid
sub validateCommitMsg {
  my $commitMsg = shift;
  my $relNoteFound = 0;
  my @bugNumbers = ();
  foreach my $line (split(/\n/, $commitMsg)) {
    # grab bug numbers out of comment line
    if (($line =~ m/^bugs? affected:(.*)$/i) or 
	($line =~ m/^bugs?:(.*)$/i) or
	($line =~ m/^bugs? fixed:(.*)$/i)) {
      my $bugStr = $1; chomp($bugStr);
      $bugStr =~ s/^\s*?//;
      #make sure each bugNumber is an integer
      foreach my $bugNum (split(/[, ]+/, $bugStr)) {
        if($bugNum =~ /^\d+$/) {
          push(@bugNumbers, $bugNum);
        }
      }
    }
    # make sure something is listed in the release notes reviewed line
    if ($line =~ m/^release notes? reviewed by: *(\w+)/i) {
      $relNoteFound = 1;
    }
  }
  # perform both checks
  if ($relNoteFound and (scalar(@bugNumbers) > 0)) {
    return \@bugNumbers;
  } else {
    return [];
  }
}

sub writeArrayToFile {
    my($fh) = shift;
    my($lastLine) = -1;
    if ($#_ >= 1) {
        $lastLine = $_[1];
    }

    my($n);
    if (($lastLine < 0) || ($lastLine > $#{$_[0]})) {
        $lastLine = $#{$_[0]};
    }
    
    for ($n = 0; $n <= $lastLine; $n++) {
        print $fh ${$_[0]}[$n] . "\n";
    }
}

sub grabFileLines {
    my ($file) = $_[0];
    local *IN;
    &error ("Unable to open $file for reading: $!") unless open(IN, "<$file");

    my $x;
    while($x=<IN>) {
        ${$_[1]}[($#{$_[1]} + 1)] = $x;
    }
    close(IN);
}

sub printUtVersion()
{
    local *IN;
    &error ("Unable to open $gVersionFile for reading: $!") unless open(IN, "<$gVersionFile");
    my $x;
    while($x=<IN>) {
        if ($x =~ /^static const char sRevision.*/) {
            my @tokens = split(/\$/, $x);
            close(IN);
            print STDOUT "UtVersion - " . $tokens[1] . "\n";
            return;
        }
    }
    close(IN);
}

sub printInfraVersion()
{
    local *IN;
    &error ("Unable to open $gInfraVersionFile for reading: $!") unless open(IN, "<$gInfraVersionFile");
    my $x;
    while($x=<IN>) {
        if ($x =~ /^my \$sRevision =.*/) {
            my @tokens = split(/\$/, $x);
            close(IN);
            print STDOUT "InfraVersion - " . $tokens[2] . "\n";
            return;
        }
    }
    close(IN);
}

sub extractFiles {
    my($type);
    my($file);
    my($nline);
    my($n);
    for ($n = 0; $n <= $#{$_[0]}; $n++) {
        ($type, $file) = split(' ', ${$_[0]}[$n]);
        #print STDOUT "Type: " . $type . "\n";
        #print STDOUT "File: " . $file . "\n";
        &error ("Incorrect type in add list: ${$_[0]}[$n]") unless ($type =~ /\?$/);
        ${$_[2]}[($#{$_[2]} + 1)] = $file;
    }
    for ($n = 0; $n <= $#{$_[1]}; $n++) {
        ($type, $file, $nline) = split(/[ \n]/, ${$_[1]}[$n]);
        if (($gVersionInMods == 0) && $file =~ /$gVersionFile/o) {
            $gVersionInMods = 1;
        }
        if (($gInfraVersionInMods == 0) && $file =~ /$gInfraVersionFile/o) {
            $gInfraVersionInMods = 1;
        }
        #print STDOUT "Type: " . $type . "\n";
        #print STDOUT "File: " . $file . "\n";
        &error ("Incorrect type in mod list: ${$_[1]}[$n]") unless ($type =~ /[MAR]$/);
        ${$_[3]}[($#{$_[3]} + 1)] = $file;
    }
}

sub extractFileLines {
    my $modCnt = 0;
    my $addCnt = 0;
    my $conflictCnt = 0;
    my($n);
    my @lines = @{$_[3]};
    #print "num lines in sub: " . $#lines . "\n";
    for ($n = 0; $n <= $#lines; $n++) {
        #print "$lines[$n]";
	if ($lines[$n] =~ /^[MAR] /) {
            ${$_[0]}[$modCnt] = $lines[$n];
	    $modCnt++;
	}
	elsif ($lines[$n] =~ /^[\?] /) {
	    # ignore hidden files
	    unless($lines[$n] =~ m#/\.[^/]+$# ) {
	      ${$_[1]}[$addCnt] = $lines[$n];
	      $addCnt++;
	    } else {
	      $lines[$n] =~ s/^\? //;
	      print "Skipping $lines[$n]...\n";
	    }
	}
	elsif ($lines[$n] =~ /^[C] /) {
	    ${$_[2]}[$conflictCnt] = $lines[$n];
	    $conflictCnt++;
	}
    }
    #print "Num mods: " . $modCnt . "\n";
}

sub launchEditor {
    my($file) = $_[0];
    &error ("Editor error.") unless (system($ENV{EDITOR}, $file) == 0);
}

sub checkForConflicts {
    my @conflicts = @_;
    if ($#conflicts >= 0) {
        &writeArrayToFile(\*STDERR, \@conflicts);
	&error ("Conflicts found. Update and try again.");
    }
}

sub getDiffDBDir() {
    return catfile(rootdir(), "tools", "QA", "diffs", $ENV{USER});
}

sub getMsgIndexName() {
    return catfile(&getDiffDBDir(), "message.index");
}

sub updateMessageIndex {
    my $lastLine = $_[0];
    # Update the message index
    my $msgIndexFile = &getMsgIndexName();
    # if there is no message.index file, just make one
    if (! -e $msgIndexFile) { `touch $msgIndexFile` ; }
    # If opening the msgIndexFile fails, clean up and exit. 
    &error ("Unable to open $msgIndexFile for append: $!") unless open(INDEX, ">>$msgIndexFile");

    print INDEX "_________________________________________________\n";
    print INDEX "Commit \#  " . $gMsgIndexNumber . " at " . $gNowString . "\n";
    if ($#gMsgLines >= 0) {
	&writeArrayToFile(\*INDEX, \@gMsgLines, $lastLine);
    }
    else {
	print INDEX $gMsg . "\n";
    }
    print INDEX "_________________________________________________\n";
    close(INDEX);
}

# will return 1 if UtVersion.cxx should be updated, 2 if InfraVersion.pm should
# be updated.
#--------------------------------------------------------------------------------

sub doContextDiff {
    my (@modFiles)    = @_;
    my $updateVersion = 2;    
    my @diffProcOut   = ();

# Running cvs diff on large commits can break the command with
# Arg list too long errors.
# We will manage the cvs diff command's length and run the cvs diff
# command more than once if we need to, in order to get the 
# diff of every file without breakage.
# Linux has 128K max command line + env. 
# 100K length is more than safe and probably almost never reached.

    my $maxCmdLen = 100000;

    for (my $i = 0; $i <= $#modFiles; $i++)
    {
	my $cmd = "$cvs -f diff -c -N ";
	
	while (length ($cmd) < $maxCmdLen && $i <= $#modFiles)
	{
	    $cmd .= "$modFiles[$i] ";
	    $i++;
	}

	push (@diffProcOut,`$cmd`);
    }

    foreach my $idx (@diffProcOut)
    {
	# Foreach file listed in the diff output. (marked by "Index: ")
	
	if (($idx =~ /^Index: src\/.+$/ && $idx !~ /^Index: src\/doc\//) ||
	    $idx =~ /^Index: scripts\/.+env$/)
	{
	    $updateVersion = 1;
	    last; # Get out. 1 takes precedence over all.
	}
    }	    
	    
    my $diffDBDir = &getDiffDBDir();
    #catfile(rootdir(), "tools", "QA", "diffs", $ENV{USER});
    mkdir $diffDBDir;
    my $st = stat($diffDBDir) or &error ("No $diffDBDir: $!");
    &error ("$diffDBDir does not have r/w/x permission") unless (($st->mode & 1700) != 0);

	    
    my @messageIndLines = ();
    &grabFileLines(&getMsgIndexName(), \@messageIndLines);
    my @commitNums = ();
# parse_dir(`ls -l $diffDBDir`);

    for (my $i = 0; ($i <= $#messageIndLines); $i++) {
	if ($messageIndLines[$i] =~ /Commit \#  .*/o) {
	    my @commitLine = split(" ", $messageIndLines[$i]);
	    # This gets the commit number from the line
	    $commitNums[$#commitNums + 1] = $commitLine[2];
	}
    }

    my $diffFile = 1;
    if ($#commitNums >= 0) {
	my @sortedNums = sort {$b <=> $a} @commitNums;
	$diffFile = $sortedNums[0];
	$diffFile++;
    }
	    
	    
    $gMsgIndexNumber = $diffFile;
	#    my $diffFile = `date +%m%d%y_%T`;
	#    $diffFile = $diffFile . ".diff";
	#    my $fulldiffFile = catfile(rootdir(), "home", "cds", $ENV{USER}, "diffs", $diffFile);
    my $fulldiffFile = catfile($diffDBDir, $diffFile);

    &error ("Unable to open $fulldiffFile for writing: $!") unless open(DIFF, ">$fulldiffFile");
    # if we can't write the diff file, very bad things could happen - abort
    unless(print DIFF @diffProcOut) {
      close(DIFF);
      &error("Unable to write diff to $fulldiffFile - aborting: $!");
    }
    close(DIFF);

    # Would rather use Compress::Zlib here and only write the file once.
    my @args = ("gzip", $fulldiffFile);
    $gDiffFile = $fulldiffFile;
    if (system(@args) != 0) {
	print STDERR "WARNING: Unable to run gzip on " . $fulldiffFile . "\n";
    }
    else {
	$gDiffFile = $gDiffFile . ".gz";
    }
    $gCleanDiffFile = 1;
    return $updateVersion;
}

sub doVersionUpdate {
    my ($versionFile) = @_;
    my $fileType; my $commentChar;
    if ($versionFile =~ m/\.pm$/) {
      $fileType = "pm"; 
      $commentChar = "#";
    } else {
      $fileType = "cxx";
      $commentChar = "//";
    }

    my $newMsg = "$commentChar " . $ENV{USER} . " " . $gNowString;
    my @versionFileLines = ();
    # First update the file
    my @args = ($cvs, "-f", "update", $versionFile);
    if (system(@args) != 0) {
        print STDERR "WARNING: Unable to update " . $versionFile . "\n";
    }
    # need to sleep here to make sure file timestamp is updated.
    # see bug2441
    sleep(1);
    
    &grabFileLines($versionFile,  \@versionFileLines);
    chomp(@versionFileLines);
    my $done = 0;
    for (my $i = 0; ($i <= $#versionFileLines) && ($done == 0); $i++) {
        if ($versionFileLines[$i] =~ /$commentChar $ENV{USER} .*/o) {
            $versionFileLines[$i] = $newMsg;
            $done = 1;
        }
    }
    if ($done == 0) {
        # no entry yet
        $versionFileLines[$#versionFileLines + 1] = $newMsg;
        for (my $i = 0; $i < 5; $i++) {
            $versionFileLines[$#versionFileLines + 1] = "$commentChar";
        }
    }
 
    # Now write out the updated file.
    my($tmpFile) = $versionFile . ".tmp";
    &error ("Unable to open $tmpFile for writing: $!") unless open(OUTFILE, ">$tmpFile");
    &writeArrayToFile(\*OUTFILE, \@versionFileLines);
    close(OUTFILE);

    &error ("Unable to rename $tmpFile to $versionFile") unless rename($tmpFile, $versionFile);

    # now double check that the version file was updated by doing a cvs 
    # status on it and looking for Locally Modified
    if( system("$cvs -f status $versionFile | grep 'Locally Modified'") != 0) {
      &error("It does not appear that $versionFile was updated\n".
        "correctly. Please check your sandbox and try again\n");
    }

}

sub grabCommitMsgFromFile {
    my ($file) = $_[0];
    my ($msgSaveFile) = "";
    my ($lineLimit) = -1;
    if ($#_ >= 1) {
        $msgSaveFile = $_[1];
    }
    if ($#_ >= 2) {
        $lineLimit = $_[2];
    }

    @gMsgLines = ();
    my @msgLines = ();
    &grabFileLines($file, \@msgLines);
    chomp(@msgLines);
    my(@tmpTokens);
    $gMsg = "";
    my($spacer) = "";
    my(@cvsFileList) = ();

    my $done = 0;
    for (my $ml = 0; ($ml <= $#msgLines) && ($done == 0); $ml++) {
	if ( $msgLines[$ml] =~ /$gReleaseRequiredNote/o ) {
	    next;
	}
        @tmpTokens = split(" ", $msgLines[$ml]);
        if (scalar (@tmpTokens) >= 1) {
            if (! ($tmpTokens[0] =~ /\s*\#/)) {
                if ($msgLines[$ml] =~ /$gMsgSeparator/o) {
                    @cvsFileList = @msgLines[($ml + 1) .. $#msgLines];
                    $done = 1;
                }
                else {
                    $gMsgLines[($#gMsgLines + 1)] = $msgLines[$ml];
                    $gMsg = $gMsg . $spacer . $msgLines[$ml];
                    $spacer = " ";
                }
            }
        }
        elsif (length($msgLines[$ml]) == 0) {
            # user-desired newline 
            $gMsgLines[($#gMsgLines + 1)] = "";
        }
    }
    
    # Write out the file list to the msgfile for the file processor.
    if ($#cvsFileList >= 0) {
        &error ("Unable to open $file for writing: $!") unless open(OUT, ">$file");
        &writeArrayToFile(\*OUT, \@cvsFileList);
        close(OUT);
    }

    # If desired, write the commit msg as is to specified file
    if (length($msgSaveFile) > 0) {
        if (open(MSG, ">$msgSaveFile") != 0) {
            &writeArrayToFile(\*MSG, \@gMsgLines, $lineLimit);
            close(MSG);
        }
        else {
            &error ("Unable to open " . $msgSaveFile . "\n");
        }
    }
    return $#gMsgLines;
}

sub getHomeDir {
    my $ret = $ENV{HOME};
    if (length($ret) == 0) {
        $ret = catfile(rootdir(), "home", "cds", $ENV{USER});
    }
    return $ret;
}

sub readConfigFile {
    my($configfile) = catfile(&getHomeDir(), ".commitrc");
    if (-f $configfile){
        if (! -r $configfile) {
            print STDERR "WARNING: " . $configfile . " is not readable.\n";
        }
        else {
            my @configLines = ();
            &grabFileLines($configfile, \@configLines);
            my($option);
            my($choice); 
            my @lineWords;
            for (my $i = 0; $i <= $#configLines; $i++) {
                @lineWords = ();
                @lineWords = split(" ", $configLines[$i]);
                if ($#lineWords >= 1) {
                    $option = $lineWords[0];
                    $choice = $lineWords[1];
                    ($option, $choice) = split(" ", $configLines[$i]);
                    if ((length($option) > 0) && ! ($option =~ /\s*\#/)) {
                      SWITCH: {
                          ($option =~ /NO_POPUP\b/) && do { $gNoPopup = 0 if ($choice =~ /NO\b/); last SWITCH;};
                          ($option =~ /ALLOW_ADDS\b/) && do { $gAllowAdds = 1 if ($choice =~ /YES\b/); last SWITCH;};
                          ($option =~ /REMOVE_UNKNOWNS\b/) && do { $gRemoveUnknowns = 1 if ($choice =~ /YES\b/); last SWITCH;};
                          ($option =~ /NO_CONFIRMATION\b/) && do { $gNoConfirm = 1 if ($choice =~ /YES\b/); last SWITCH;};
                          ($option =~ /NO_CHECKIN_VALIDATION\b/) && do { $gNoCheckinValidation = 1 if ($choice =~ /YES\b/); last SWITCH;};
                          ($option =~ /NO_XWINDOW\b/) && do { undef $ENV{DISPLAY} if ($choice =~ /YES\b/); last SWITCH;};
                          print STDERR "Unrecognized option: " . $option . "\n";
                      }
                        
                    } # if option > 0 ...
                } # if linewords >= 1
            } # for
            
        } # else
    } # if $st
}

sub writeAddsModsToFile {
    my($fh) = shift;
    
    my($n);
    if ($#{$_[0]} >= 0) {
        for ($n = 0; $n <= $#{$_[0]}; $n++) {
            print $fh ${$_[0]}[$n] . "\n";
        }
        print $fh "\n";
    }
    for ($n = 0; $n <= $#{$_[1]}; $n++) {
        print $fh ${$_[1]}[$n] . "\n";
    }
}

sub maybeRetrieveMsg {
    my $lineInd = -1;
    my $st = stat($gCommitLogFile) || 0; # Ignore stat errors
    if ($st) {
        $gCleanCommitLogFile = 0;
        $lineInd = &grabCommitMsgFromFile($gCommitLogFile);
        $gCleanCommitLogFile = 1;
        $gUseFOption = 1;
    }
    return $lineInd;
}

# examine one file for existance - returns stat with zero ctime, mtime
# for nonexistant file.
#
sub checkFile {
    my $filename = shift;
    
    my $sb = stat($filename)
        || return new File::stat( 'mtime' => 0, 'ctime'=> 0);

    return $sb;
}

# prompt with a question: wait for a y/n answer
sub ask {
    my $question = (shift) . " y/n (exact): ";
    print STDOUT "\n";
    while (1) {
        print STDOUT $question;
        my $ans = <STDIN>;
        return 1 if ($ans =~ /^y$/);
        return 0 if ($ans =~ /^n$/);

        print STDOUT "Please answer y or n.\n";
    }
}

# See if we did successful checkin tests
sub doCheckinVerification {
    my (@modFiles) = @_;

    # if there aren't any src files, we don't need to worry about checkins
    return unless scalar (grep m/^src\//, @modFiles);

    ## First find the checkin directory and verify that the files exist.
    ## CARBON_HOME should point to a carbon_config'ed area.  If that's NOT the
    ## 'checkin' built one - try and find it.
    ##
    my $checkindir = $ENV{CARBON_HOME};
    $checkindir =~ s/\[\/]+$//;  # remove trailing slashes
    if ($checkindir !~ /\/checkin\// || ! -e $checkindir ) {
        $checkindir = "$gWORKBASEDIR/" . basename $ENV{PWD};
        print STDERR "WARNING: Can't find checkin dir. Set CARBON_HOME to checkin area." unless -d $checkindir;
    }

    my $checkinOK = 1;          # Assume ok
    my $beginTime = 0;          # Assume worst case for checkin tests

    my $testroot = $checkindir . "/obj/Linux.product";
    if (! -d $testroot) {
        print STDERR "WARNING: checkin build directory $testroot not found.\n";
        $checkinOK = 0;
    } else {
        my $runlistresults = $testroot . "/runlist.results";
        my $runlistfailed  = $testroot . "/runlist.failed";

        if (! -e $runlistresults) {
            print STDERR "WARNING: file not found - $runlistresults\n";
            $checkinOK = 0;
        } elsif (! -e $runlistfailed) {
            print STDERR "WARNING: file not found - $runlistfailed\n";
            $checkinOK = 0;
        } else {
            $beginTime = &checkFile($runlistresults)->ctime;
            my $endTime   = &checkFile($runlistfailed)->mtime;

            if ($beginTime > $endTime) {
                print STDERR "WARNING: Checkin runlist timestamps inconsistent\n";
                $checkinOK = 0;
            }

            # look at the runlist.failed file.  It should be empty (or nearly so)
            if ((-s $runlistfailed) > 1) {
                print STDERR "WARNING: one or more tests failed in $runlistfailed.\n";
                $checkinOK = 0;
            }
        }
    }
    
    # Look at the files we're committing to see if any of them were MODIFIED after
    # the start of the checkin tests.
    #
    my @badTimeFiles = grep {$beginTime < &checkFile($_)->mtime} @modFiles; # Filter bad timestamps
    if ((scalar @badTimeFiles) > 0) {
        print STDERR "WARNING: files were modified after checkin tests started.\n\t"
            . join ("\n\t", @badTimeFiles);
        $checkinOK = 0;
    }

    if ($checkinOK == 0) {
        my $resp = &ask("Are you sure you want to commit?");
        if ($resp == 0) {
            &cleanup;
            exit(0);
        }
    }
}


