#============================================================================
# CarbonLinksInfo class.
#
# This is a a container class, which uses another container class called
# LinkGroup.  The intent of this CarbonLinksInfo class is to keep all the 
# links that are required for build, test, and release.
#
# This class is used by developers via scripts/carbon_config calls.
#
# It is also used by the release packaging process to understand which
# thing we should be packaging.
#
#----------------------------------------------------------------------------
# PURPOSE OVERVIEW:
#-------------------
# The intent of this class is to hold the links and their targets as data 
# carbon_config which will make the links, and can also be used by other
# scripts that will not actually make these links, such as the packaging 
# script.
#
# USAGE OVERVIEW
#----------------------------------------------------------------------------
# Making links:
#
# To make carbon_config create a new link or sets of links, 
#
# 1. Determine method.
#    a. Describe the link and file target by hand in this class.
#    b. Use of a bomfile. (Described below.)
#  
# Choice A: Assigning links in this file.
#---------------------------------------------------------------------------
#
# 2. Links are incorporated in the system by inserting them into a LinkGroup
# class.  There are 2 modes for LinkGroup class:
#
#   new LinkGroup(1)  - Any links added to this instance will be packaged
#   new LinkGroup()   - Do NOT package these links.
#
#
# Example:
# --------
#  $this->{mygroup} = new LinkGroup();
#
# The "mygroup" string is a hash key and should be distinct to the
# CarbonLinksInfo instance.  It must always be the first key to $this.
# $this->{mygroup}{wrongkey} = new LinkGroup() will not be understood 
# by the CarbonConfig.pm modules.
#
# 
# 3. Each link request must be added to this LinkGroup
# 
# Example: 
# --------
#  my $link = /here/foo/bar
#  my $target = /tools/linux/foo/bar
# 
#  $this->{mygroup}->AddLink($link, $target);
#
# NOTE: A. $link and $target MUST be fully qualified paths.
#       B. $target must exist, unless target is inside the object build
#          directory.
#
# If properly assigned as described, this is all that is required.
# The CarbonConfig class will execute and and all links in all
# linkGroup instances created in CarbonLinksInfo.
#
#---------------------------------------------------------------------------
# Choice B: Using a bomfile to specify links and targets.
#---------------------------------------------------------------------------
#
# 1. For your bomfile to be processed by this class it must be listed in
#    src/carbon_config.boms, and the filename itself in that file must
#    have a ".bom" extension.
#
# Rules of a bom file so that its contents can be properly processed.
#
# a. We must understand what the contents represents. Links or targets.
# b. We must understand the linkRoot path
# c. We must understand the target root path
# d. We must understand whether or not the prefix path should be appended
#    to the alternate linkroot/targetroot pair - depending on what the
#    contents represent targets or links.
#
# So to properly interface with the BomFileReader class which reads
# the bomfiles, there are several required bomfile description tags:
#  
#  BOMTYPE     = (links|targets)
#  MIRRORPATHS = (true|false)
#  LINKROOT    = Root path to directory where the link group will be made
#     path may be modified, if MIRRORPATHS is true, and the file contains
#     target files.
#  TARGETROOT  = Root path to directory where the file targets exist. Path
#     may be modified if MIRRORPATH = true and BOMTYPE=links.
#  PACKAGED    = (true|false).
#
# Additionally, there are path description tag aliases
#
#  OBJDIR             = $CARBON_HOME/$(CARBON_TARGET)
#  SANDBOX            = user's sandbox
#  CARBON_HOME        = $CARBON_HOME
#  CARBON_TARGET_ARCH = $CARBON_HOME/(Linux|Linux64|SunOS|Win) depending on
#  CARBON_ARCH        = $CARBON_HOME/(Linux|Linux64|SunOS|Win) depending on
#                        what carbon target is set.
#  GCC_VERSION        = current gcc version as understood by the user arguments
#                       to carbon_config, and platform.
#  UNAME              = `uname` (Linux|SunOS)
#
# Please see src/makefiles.bom and other similar files for samples.
#
# IMPORTANT NOTE: The name of the bom file path denotes the name of the 
# key in the $this->{$key} hash.  Links made by entries assigned in 
# src/makefiles.bom for example, will become a LinkGroup reference 
# called $this->{makefiles}.  So, you want to call your bomfile something 
# distinct, as the dirname portion of file path is stripped as is the
# ".bom" extension. 
#
#===========================================================================
package CarbonLinksInfo;

use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSFileNames;
use LinkGroup;
use BomFileReader;
use strict;

#------------------------------------------------------------------------------
# New ()
#
# Takes:
# 1. Carbon_arch string.
# 2. Os Version string
# 3. CDSFileNames class instance.
# 4. CarbonConfigArgs class instance.
#
#------------------------------------------------------------------------------
sub new
{
    my $this = {};
    bless $this, @_[0];
    $this->{carbonArch} = @_[1];
    $this->{osVersion}  = @_[2];
    $this->{fileNames}  = @_[3];
    $this->{args}       = @_[4];

# If not passed one, default to carbon arch script output
    if (! $this->{carbonArch})
    {
        $this->{carbonArch} = $this->{args}->Platform();
    }

# If not passed one, default to hostos type as understood by 
# CDSFileNames.

    if (! $this->{osVersion})
    {
        $this->osVersion() = $this->{args}->OsVersion();
    }

    $this->Initialize();

    return $this;
}

#------------------------------------------------------------------------------
# Initialize, which calls lower level initializers
#------------------------------------------------------------------------------
sub Initialize
{
    my $this = shift;
    my $carbonHome = $this->{fileNames}->CarbonHome();

    my @backCompats = ();
    $this->{backCompats} = \@backCompats;

    if ($this->{args}->Platform() =~ /Linux64/)
    {
        @backCompats = ("ES4_64","ES5_64","ES6_64");
    }
    elsif ($this->{args}->Platform() =~ /Linux/)
    {
        @backCompats = ("ES4","ES5","ES6");
    }

    
# Flex lm tools
#---------------
    $this->{ES3Flex}    = $this->{fileNames}->FlexLmArea("ES3_lib");
    $this->{flexlm64}   = $this->{fileNames}->FlexLmArea("amd64_lib");
    $this->{winFlex}    = $this->{fileNames}->FlexLmArea("winx")."/i86_n3";

    $this->{vInfo} = $this->{fileNames}->VersionsObject();

    $this->InitInstallJammerAreas();
    $this->InitLibraryExtensions();
    $this->InitLibz();
    $this->InitLibXml2();

# Linux only
    if ($this->{args}->Platform() =~ /Linux/)
    {
        $this->InitLibMono();
        $this->InitLibGdiPlus();
    }

    $this->InitSimOptionsFile();
    $this->InitFlexLmLibs();
    $this->InitFlexLmExecs();

    $this->InitIALinks();
    $this->InitGccLinks();
    $this->InitDebussyLinks();
    $this->InitLinksBackAtSandbox();
    $this->InitSystemExecLinks();
    $this->ReadBomFiles();


# NO linux64 areas for libqt or graphviz
    if ($this->{args}->Platform() !~ /Linux64/)
    {
        $this->InitGraphviz();
        $this->InitQTLibLinks();
    }

    if ($this->{args}->Platform() eq "SunOS")
    {
        $this->InitRational();
    }


    if ($this->{args}->Platform() =~ /Linux/)
    {
        if ($this->{osVersion} =~ /ES(3|4|5)|rh7/)
        {
            if (($this->{osVersion} eq "rh7") || ($this->{osVersion} eq "ES3"))
            {
                print STDERR "\n\n",
                "=======================================================\n",
                "WARNING: Your machine is ", $this->{osVersion}, 
                " which is no longer supported.\n",
                "Please request that your machine's operating system be\n",
                "upgraded to something we support.\n",
                "=======================================================\n";
                
                sleep 2;
            }
        }
    }
    
# End initialization
#-------------------
}

#------------------------------------------------------------
#!! Init installjammer areas
#------------------------------------------------------------
sub InitInstallJammerAreas
{
    my $this = shift;
    my $carbonHome     = $this->{fileNames}->CarbonHome();
    my $sandbox        = $this->{fileNames}->FindSandboxPath();
    my $libInstallDir  = "$carbonHome/lib/installers";
    my $srcUnixInstallDir = "$sandbox/src/unixInstall";
    my $target         = $this->{fileNames}->InstallJammerHome();
    my $link           = "$carbonHome/installjammer";
    my $objDir         = $this->{fileNames}->ObjectDir();

    $this->{installJammer} = new LinkGroup(1);
    $this->{installJammer}->AddLink ($link,$target);
    $this->{installJammer}->AddLink("$libInstallDir/IPInstall.mpi",
                                    "$srcUnixInstallDir/IPInstall/IPInstall.mpi");
    $this->{installJammer}->AddLink("$libInstallDir/vcredist_x86.exe",
                                    "$srcUnixInstallDir/CarbonRuntime/vcredist_x86.exe");

    my @proj  = qw (carbonLogo.gif 
                WelcomeImage.png
                InstallProject/InstallProject.mpi
                CarbonRuntime/CarbonRuntime.mpi
                IPInstall/IPInstall.mpi);

    foreach my $file (@proj)
    {
        $target = "$srcUnixInstallDir/$file";
        ($link = $target) =~ s/$sandbox\/src/$objDir/;
        $this->{installJammer}->AddLink($link, $target);
    }
}

#------------------------------------------------------
# This will return a list of a listGroups.  Every link
# we make MUST be assigned into a LinkGroup object, and
# it's pointer assigned as the first key to $this.
#------------------------------------------------------
sub AllKeys
{
    my $this = shift;
    my @allKeys = ();

    foreach my $k (keys %{$this})
    {
    # Other things could be a first level key to $this
    # We have to make sure it is in fact a LinkGroup 
    # reference.
        if (ref($this->{$k}) eq "LinkGroup")
        {
            push (@allKeys, $k);
        }
    }

    return @allKeys;
}

#------------------------------------------------------
#!! Licensing executables and such
#------------------------------------------------------

sub InitFlexLmExecs
{
    my $this         = shift;
    my $targetDir    = "";
    my $linkDir      = "";
    my @execs        = ();
    my @licPlatforms = qw (Linux Win);
    $this->{lmexecs} = new LinkGroup(1);

# License area paths
    my %licTools     = ();
    $licTools{Linux} = $this->{ES3Flex};
    $licTools{Win}   = $this->{winFlex};

# Links extended path field
    my %extPath      = ();
    $extPath{Linux}  = "ES4";
    $extPath{Win}    = "winx";

    foreach my $lPlat (@licPlatforms)
    {
        $linkDir = join "/",$this->{fileNames}->CarbonHome(),
        "$lPlat/bin", $extPath{$lPlat};
        $targetDir = $licTools{$lPlat};

        if ($lPlat eq "Win")
        {
            @execs = qw (MvsnLicenseServerMsgs.dll VendorLicenseServerMsgs.dll
                         carbond.exe eventlogadd.reg eventlogremove.reg 
                         lmgrd.exe lmtools.exe lmutil.exe);
        }
        else
        {
            @execs = qw (carbond lmgrd lmutil);
        }

        foreach (@execs)
        {
            $this->{lmexecs}->AddLink("$linkDir/$_","$targetDir/$_");
        }
    }
}

#------------------------------------------------------
#!! Purify stubs linking
#------------------------------------------------------
sub InitRational() #()
{
    my $this = shift;
    $this->{rational} = new LinkGroup(1);

    my $target = $this->{fileNames}->CarbonToolsDir($this->{args}->Platform()).
        "/lib/libpurify_stubs.a";
    my $link = join "/", $this->{fileNames}->CarbonHome(),
    $this->{args}->Platform(),"lib/libpurify_stubs.a";

    $this->{rational}->AddLink($link,$target);
}

#------------------------------------------------------
#!! Assign Graphviz links
#------------------------------------------------------
sub InitGraphviz #()
{
    my $this          = shift;
    $this->{graphviz} = new LinkGroup();
    my $toolsLibDir   = join "/",$this->{fileNames}->CarbonToolsDir($this->{args}->Platform()),"lib";
    my $linkDir       = join "/", $this->{fileNames}->CarbonHome(),
                        $this->{carbonArch}, "lib";
    my @libs          = CDSSystem::OpenDir ($toolsLibDir);

    foreach (@libs)
    {
        if ($_ =~ /^lib(agraph|cdt|gvc|graph|pathplan|iconv)/)
        {
            $this->{graphviz}->AddLink("$linkDir/$_","$toolsLibDir/$_");
        }
    }
}

#------------------------------------------------------
# Assign Debussy links
#------------------------------------------------------
sub InitDebussyLinks
{
    my $this           = shift;
    my $cmd            = join " ", $this->{fileNames}->DebussyEnvScript(),
                         "FsdbReader";
    my $debussyReaderLibRoot = CDSSystem::BackTick($cmd);
    chomp ($debussyReaderLibRoot);
    $cmd            = join " ", $this->{fileNames}->DebussyEnvScript(),
                     "FsdbWriter";
    my $debussyWriterLibRoot = CDSSystem::BackTick($cmd);
    chomp ($debussyWriterLibRoot);
    my @libs           = ();
    my $carbonHome     = $this->{fileNames}->CarbonHome();
    $this->{debussy}   = new LinkGroup();
    @libs              = qw (libnffr.a libnsys.a);
    my $debWriteLib = "libnffw.a";
    my $debWriteSo = "libnffw.so";


    foreach my $osdir (@{$this->{backCompats}})
    {
        my $link = "";
        my $target = "";

        foreach my $lib (@libs)
        {
            $link = join "/", $carbonHome, $this->{carbonArch},
                    "lib", $osdir, $this->{args}->Target(),$lib;

            if ($osdir =~ /ES\d_64/)
            {
                $target = "$debussyReaderLibRoot/LINUXAMD64/$lib";
            }
            elsif ($osdir =~ /ES\d/)
            {
                $target = "$debussyReaderLibRoot/LINUX9.0/$lib";
            }
            else
            {
                $target = "$debussyReaderLibRoot/LINUX_GNU/$lib";
            }

            $this->{debussy}->AddLink ($link,$target);
        }

        if ($osdir =~ /ES\d|rh7|sun4u5/)
        {
            my $alink = join "/", $carbonHome, $this->{carbonArch},
                        "lib", $osdir, $debWriteLib;
            my $soLink = join "/", $carbonHome, $this->{carbonArch},
                         "lib", $osdir, $debWriteSo;
            my $soTarget = "";
            my $aTarget = "";

	    if ($osdir =~ /ES\d_64/)
            {
                $aTarget  = "$debussyWriterLibRoot/LINUXAMD64/$debWriteLib";
                $soTarget = "$debussyWriterLibRoot/LINUXAMD64/$debWriteSo";
            }
            elsif ($osdir =~ /ES\d/)
            {
                $aTarget  = "$debussyWriterLibRoot/LINUX9.0/$debWriteLib";
                $soTarget = "$debussyWriterLibRoot/LINUX9.0/$debWriteSo";
            }
            else
            {
                $aTarget  = "$debussyWriterLibRoot/LINUX_GNU/$debWriteLib";
                $soTarget = "$debussyWriterLibRoot/LINUX_GNU/$debWriteSo";
            }

            $this->{debussy}->AddLink ($alink, $aTarget);
            $this->{debussy}->AddLink ($soLink, $soTarget);
        }
    }
}

#------------------------------------------------------
# Assign Gcc Links
#------------------------------------------------------
sub InitGccLinks
{
    my $this = shift;
    my $toolsDir    = $this->{fileNames}->CarbonToolsDir($this->{args}->Platform());
    my $linkDir     = join "/", $this->{fileNames}->CarbonHome(), 
                   $this->{args}->Platform();
    my $ver         = $this->{fileNames}->OsVersion();
    my $shortGccVer = "gcc".$this->{args}->GccVersion();
    $shortGccVer    =~ s/\.|\-.+//g;

    $this->{gcclinks} = new LinkGroup("gcclinks");

    # gcc is in OS-specific subdirectory in /tools
    my $baseToolsDir = $toolsDir;
    $toolsDir .= "/$ver";

# Carbon Gcc is the shorthand version of the gcc-$gccversion. 
# It's referred as this in the runtime makefiles. We need to link it to the 
# associated version on /tools.

    my $carbonGcc = $this->{args}->Target();
    # Binutils version is appended, since we might have multiple
    # copies of the same gcc configured with different binutils.
    my $gcc = "$toolsDir/gcc-".$this->{args}->GccVersion()."-binutils-".$this->{args}->BinutilsVersion();


    $this->{gcclinks}->AddLink("$linkDir/$carbonGcc",$gcc);
    # Also link this as just "gcc"
    $this->{gcclinks}->AddLink("$linkDir/gcc",$gcc);

# We need to attach the 343 version for maxsim things.
# For now, this is in the base tools directory
    $this->{gcclinks}->AddLink("$linkDir/gcc343","$baseToolsDir/gcc-3.4.3-a");

    $this->{gcclinks}->AddLink("$linkDir/$shortGccVer",
                               "$toolsDir/gcc-".$this->{args}->GccVersion()."-binutils-".$this->{args}->BinutilsVersion());
}

#------------------------------------------------------
# Assign links for system commands via $CARBON_HOME/bin
#------------------------------------------------------
sub InitSystemExecLinks
{
    my $this       = shift;
    $this->{sysexec} = new LinkGroup();
    my $carbonHome = join "/", $this->{fileNames}->CarbonHome();
    my $toolsDir   = $this->{fileNames}->CarbonToolsDir($this->{args}->Platform());
    my $carbonArch = $this->{carbonArch};
    my $toolsBin   = "$toolsDir/bin";
    my $link       = "";
    
# These are things made in $CARBON_HOME/bin
#-------------------------------------------
    my $binDir = "$carbonHome/bin";
    my $target = $this->{fileNames}->ScriptsDir()."/sysexec";
    my @sysCmd = qw(dot make perl);

    foreach (@sysCmd)
    {
	$link = "$binDir/$_";

	$this->{sysexec}->AddLink($link,$target);
    }

# These are things made in $CARBON_HOME/$CARBON_ARCH/bin
#-------------------------------------------------------
    $binDir = "$carbonHome/$carbonArch/bin";

# perl lib
    $this->{sysexec}->AddLink("$carbonHome/$carbonArch/lib/perl", 
                              "$toolsDir/perl/lib");

# Gmake
#-------
    $target = "$toolsBin/gmake-3.80";
    $link = "$binDir/make";
    $this->{sysexec}->AddLink($link,$target);

# Perl
#--------
    my $toolsPerl = join "/",
                  $this->{fileNames}->CarbonToolsDir($this->{args}->Platform()),
                  "perl/bin/perl".$this->{vInfo}->Version("perl");

    $this->{sysexec}->AddLink("$binDir/perl", $toolsPerl);
                              


# Binutils things
#-----------------
    my @bUtils = qw(ar as ld nm objcopy ranlib);
    my $buVer = $this->{args}->BinutilsVersion();

    # binutils are in OS-specific subdirectory in /tools
    my $ver = $this->{fileNames}->OsVersion();
    $toolsDir .= "/$ver";

    foreach (@bUtils)
    {
	$this->{sysexec}->AddLink("$binDir/$_",
                                  "$toolsDir/binutils-$buVer/bin/$_");
    }
}

#------------------------------------------------------
# Assign links back to sandbox
#------------------------------------------------------
sub InitLinksBackAtSandbox
{
    my $this         = shift;
    $this->{sandbox} = new LinkGroup(1);
    my $sandbox      = $this->{fileNames}->FindSandboxPath();
    my $carbonHome   = $this->{fileNames}->CarbonHome();
    my $link         = "$carbonHome/help";
    my $target       = "$sandbox/src/gui/help";
    my @modules      = CDSSystem::OpenDir($sandbox);

    $this->{sandbox}->AddLink($link, $target);

    # No sandbox/test occurs with cbuildnotest checkout module.
    if (! -d "$sandbox/test")
    {
        CDSSystem::Mkdir ("$sandbox/test");
        push (@modules,"test");
    }

    foreach my $mod (@modules)
    {
	my $sbDir = join "/", $sandbox,$mod;
	
	if (-d "$sbDir")
	{
	    if (-d "$sbDir/CVS" || $sbDir eq "$sandbox/test")
	    {
		$link = "$carbonHome/$mod";
		$target = "$sbDir";
		$this->{sandbox}->AddLink($link, $target);
	    }		
	}
    }
}

sub InitIALinks()
{
    my $this        = shift;
    $this->{ialibs} = new LinkGroup();
    my $linkDir     = join "/", $this->{fileNames}->CarbonHome(), "lib";
    my %execDirs    = $this->{args}->ExecutableDirs();

    foreach my $bin (keys %execDirs)
    {
	my $link = join ".", "$linkDir/carbon",$this->{args}->Target(),
	$this->{carbonArch};

        $link .= ".$bin.ia";

	my $target = join "/", $this->{fileNames}->CarbonHome(), "obj", 
 	             $this->{carbonArch};
        $target .= ".$bin/carbon.ia";
	
	$this->{ialibs}->AddLink($link,$target);
    }
}

#------------------------------------------------------------------------------
# Read all the bom files
#------------------------------------------------------------------------------
sub ReadBomFiles
{
    my $this       = shift;
    my $sandbox    = $this->{fileNames}->FindSandboxPath();
    my $carbonHome = $this->{fileNames}->CarbonHome();
    my $carbonArch = $this->{carbonArch};
    my @boms       = ();
    my %scripts    = ();
    my $ccfgBom    = "src/carbon_config.boms";
    my $badExtFilter = "";

    foreach (@{$this->{invalidExt}})
    {
        $badExtFilter .= "$_|";
    }
    
    $badExtFilter =~ s/\|$//;
    
    my $rh = CDSSystem::OpenRead($ccfgBom);
    
    while (<$rh>)
    {
        chomp;
        $_ =~ s/\s//g;
        
        if ($_ !~ /^\#|^$/)
        {
            push (@boms,$_);
        }
    }
    
    close $rh;
    
    if ($this->{args}->Platform() =~ /^(Linux|SunOS)$/)
    {
        push (@boms,"src/xactors_libs.bom");
        push (@boms,"src/maxlib.bom");
    }
    
    foreach my $bom (@boms)
    {
        my $file      = "$sandbox/$bom";
        my $key       = "";
        ($key = $bom) =~ s/^.+\/|\.bom//g;
	my $bomReader = new BomFileReader($file,
					  $this->{fileNames},
					  $this->{args});
        
        if (! $this->{$key})
        {
            $this->{$key} = new LinkGroup($bomReader->Packaged());
        }	
        
	my %links = $bomReader->List();
	
	foreach (keys %links)
	{
            if ($_ !~ /\.($badExtFilter)$/)
            {
                $this->{$key}->AddLink($_,$links{$_});
                
# If thing in scripts.bom is also in execs.bom, scripts.bom
# will have precedence. We have to mark them.
#----------------------------------------------------------
                if ($bom eq "src/scripts.bom")
                {
                    my $fName        = $_;
                    $fName           =~ s/^.+\///g;
                    $scripts{$fName} = $fName;
                }
                
# Special stuff for libs bom contents
#-------------------------------------	    
                elsif ($bom =~ /src\/(internal_|)libs\.bom/)
                {
                    my $linkName = "";
                    ($linkName   = $_) =~ s/^.+\///;
                    my $linkDir  = "";
                    ($linkDir    = $_) =~ s/\/$linkName$//;
                }
# Libraries that we generate for python.
#------------------------------------------------------------------------
                elsif ($bom =~ /src\/python_(generate|wrappers)\.bom/)
                {
                    my $linkName = "";
                    ($linkName   = $_) =~ s/^.+\///;
                    my $linkDir  = "";
                    ($linkDir    = $_) =~ s/\/$linkName$//;
                }
                
# File name mapping exception.
#-----------------------------
                elsif ($bom eq "src/makefiles.bom")
                {
                    # Have to hack this because of nonuniformity in naming convention.
                    
                    $this->{$key}->AddLink("$carbonHome/makefiles/Modshell.mc",
                                           "$sandbox/src/modshell/Makefile.mc");
                }
# Anything in execs points to object directory places
#----------------------------------------
                elsif ($bom eq "src/execs.bom")
                {
                    # Some execs go defunct and are aliased for backwards
                    # compatibility.  Since we don't build them, and we can't
                    # make links to non-real objects, below we'll skip making
                    # $CARBON_HOST/bin links for them, but will make $CARBON_HOME/bin
                    # link for them.
                    #--------------------------------------------------------
                    my %execIsAliased = ('vspcompiler' => 1,
                                          'cbuildinternal' => 1);

                    if ($this->{args}->Platform() eq "Linux64")
                    {
                        $execIsAliased{modelstudio} = 1;
                        $execIsAliased{tbgen} = 1;
                        $execIsAliased{ncelabC} = 1;
                        $execIsAliased{ncsimC} = 1;
			$execIsAliased{sys2socd} = 1;
			$execIsAliased{socdsyscimport} = 1;
                    }

                    my $carbonArch = $this->{args}->Platform();
                    my $carbonExec = join "/",$this->{fileNames}->ScriptsDir(),
                    "carbon_exec";
                    my $carbonExecBat = $carbonExec.".bat";
                    my $fName = "";
                    ($fName = $_) =~ s/^.+\///g;
                    
                    if ($_ !~ /\.(exe|bat)$/)
                    {
                        if (! $scripts{$fName})
                        {                    
                            my $link = "";
                            my $plat = $carbonArch;
                            $plat =~ s/\..+$//;
                            ($link = $_) =~ s/$plat\/bin\/$fName/bin\/$fName/;

                            my $target = $carbonExec;
                            
                            $this->{$key}->AddLink($link, $target);
                        

                            if ($execIsAliased{$fName})
                            {
                                $this->{$key}->DeleteLink($_);
                            }
                        }
                    }
                    else
                    {
                        $this->{$key}->DeleteLink($_);
                        $fName =~ s/\.exe$/\.bat/;
                        
                        my $link = $this->{fileNames}->CarbonHome()."/bin/$fName";
                        $this->{$key}->AddLink($link, $carbonExecBat);
                    }
                }
# libCarbonPciTarget.so needs to be pointed
# at something that matches the version.
#-----------------------------------------
                elsif ($bom eq "src/maxlib.bom" && $_ =~ /\/libCarbonPciTarget\.so/)
                {
                    $this->{maxlib}->DeleteLink($_);
                    my $variant = 7;
                    
                    if ($_  =~ /MaxLib6/)
                    {
                        $variant = 6;
                    }
                    
                    $links{$_} =~ s/\/libCarbonPciTarget/\/libCarbonPciTarget$variant/;
                    
                    $this->{maxlib}->AddLink($_, $links{$_});
                }
            }
            else
            {
                if ($this->{args}->Verbose())
                {
                    print "Ignoring non platform library $_\n";
                }
            }
        }
    }
}

        
#------------------------------------------------------------------------------
# Init so|a/dll|lib possibilities, clarify the situation.
#------------------------------------------------------------------------------
sub InitLibraryExtensions
{
    my $this     = shift;
    my @ext      = ();
    my @invalidExt = ();

    $this->{ext} = \@ext;
    $this->{invalidExt} = \@invalidExt;

    if ($this->{args}->Platform() eq "Win")
    {
        @ext = ("lib","dll");
        @invalidExt = ("a","so");
    }
    else
    {
        @ext = ("a","so");
        @invalidExt = ("lib","dll");
    }
}
    
    
#------------------------------------------------------------------------------
# Init libz lib link paths.
#------------------------------------------------------------------------------
sub InitLibz
{
    my $this        = shift;
    $this->{libz}   = new LinkGroup();
    my $hostArch    = $this->{fileNames}->CarbonHostArch();
    my $toolsLibDir = join "/", $this->{fileNames}->CarbonToolsDir($hostArch), 
                      "lib";
    my $linkDir     = join "/", $this->{fileNames}->CarbonHome(), 
                      $hostArch, "lib";


    foreach (@{$this->{ext}})
    {
	my $target = join ".", "$toolsLibDir/libz", $_;
	my $link   = join ".", "$linkDir/libz",  $_;
	$this->{libz}->AddLink($link,$target);
    }
}

sub InitLibXml2
{
    my $this             = shift;
    $this->{libxml}      = new LinkGroup(1);
    my $libxmlVersion    = $this->{vInfo}->Version("libxml");
    my $target           = join "/", 
                           $this->{fileNames}->CarbonToolsDir($this->{args}->Platform()),
                           "libxml2-$libxmlVersion/lib/libxml2.so.$libxmlVersion";

    foreach (@{$this->{backCompats}})
    {
        my $link  = join "/", $this->{fileNames}->CarbonHome(), 
                    $this->{carbonArch}, "lib", $_, "libxml2.so.2";

        $this->{libxml}->AddLink($link, $target);
        $link =~ s/_64//;
        $this->{libxml}->AddLink($link, $target);
    }
}

sub InitLibMono
{
    my $this          = shift;
    $this->{mono} = new LinkGroup();
    my $libmonoVersion  = $this->{vInfo}->Version("mono");
    my $toolsLibDir   = join "/",$this->{fileNames}->CarbonToolsDir($this->{args}->Platform()),"mono-$libmonoVersion/lib";
    my $linkDir       = join "/", $this->{fileNames}->CarbonHome(),
                        $this->{carbonArch}, "lib";
    my @libs          = CDSSystem::OpenDir ($toolsLibDir);

    foreach (@libs)
    {
        if ($_ =~ /^lib(mono|MonoPosixHelper)/)
        {
            $this->{mono}->AddLink("$linkDir/$_","$toolsLibDir/$_");
        }
    }
}

sub InitLibGdiPlus
{
    my $this          = shift;
    $this->{gdiplus} = new LinkGroup();
    my $libgdiplusVersion  = $this->{vInfo}->Version("gdiplus");
    my $toolsLibDir   = join "/",$this->{fileNames}->CarbonToolsDir($this->{args}->Platform()),"libgdiplus-$libgdiplusVersion/lib";

    my $linkDir       = join "/", $this->{fileNames}->CarbonHome(),
                        $this->{carbonArch}, "lib";
    my @libs          = CDSSystem::OpenDir ($toolsLibDir);

    foreach (@libs)
    {
        if ($_ =~ /^lib(gdiplus)/)
        {
            $this->{gdiplus}->AddLink("$linkDir/$_","$toolsLibDir/$_");
        }
    }
}

#------------------------------------------------------------------------------
# Init sim_options for the file collector utility
#------------------------------------------------------------------------------
sub InitSimOptionsFile
{
  # sandbox/src/python...
  my $this = shift;
  my $target = $this->{fileNames}->FindSandboxPath()."/src/python/Carbon/tools/sim_options.xml";
  my $link = $this->{fileNames}->CarbonHome()."/lib/sim_options.xml";

  # This makes a new link group.
  $this->{sim_options} = new LinkGroup(1);

  # This adds the link to the group.
  $this->{sim_options}->AddLink($link, $target);
}

#------------------------------------------------------------------------------
# List a group of links. (Hopefully they are linked by group)
#------------------------------------------------------------------------------
sub List
{
    my $this  = shift;
    my $name  = shift;
    my %links = shift;

    if (! $this->ValidGroupKey($name))
    {
	print STDERR "Invalid group key $name. Cannot get a ",
	"link list with this key\n",
	"Keys that are currently valid:\n\n";

	foreach (keys %{$this})
	{
            if (ref($this->{"$_"}) eq "LinkGroup")
            {
                print "  $_\n";
            }
	}

	die;
    }

    return $this->{$name}->LinkList();
}

#------------------------------------------------------------------------------
# Validates that what the user is asking for is a known group key.
#------------------------------------------------------------------------------
sub ValidGroupKey
{
    my $this  = shift;
    my $key   = shift;
    my $valid = 0;

    if ($key && ref($this->{$key}) eq "LinkGroup")
    {
	$valid = 1;
    }

    return $valid;
}

#------------------------------------------------------------------------------
# Query method that other scripts can access to "qualify" a link as
# something that carbon_config makes.
#------------------------------------------------------------------------------
sub CarbonConfigLink
{
    my $this     = shift;
    my $linkPath = shift;
    my $known    = 0;

    foreach (keys %{$this})
    {
	if (ref($this->{$_}) eq "LinkGroup" && 
            $this->{$_}->MyLink($linkPath))
	{
	    $known = 1;
	}
    }

    return $known;
}

#------------------------------------------------------------------------------
# Init flex lm library links
#------------------------------------------------------------------------------
sub InitFlexLmLibs
{
    my $this         = shift;
    $this->{flexLm}  = new LinkGroup("flexlm");
    my @toolsLibDirs = ();

    my @flexlibs     = qw (libcrvs 
			   libcrvs_pic 
			   liblmgr 
			   liblmgr_pic 
			   liblmgr_nomt 
			   liblmgr_nomt_pic 
			   libsb 
			   libsb_pic);
    
    foreach my $lib (@flexlibs)
    {
        foreach my $osdir (@{$this->{backCompats}})
        {
          my $target = "";
          my $link   = join "/", $this->{fileNames}->CarbonHome(),
          $this->{fileNames}->CarbonHostArch(), "lib", 
          $osdir, $lib;
          $link     .= ".".$this->{ext}[0];
          
          if ($osdir =~ /ES\d_64/) {
            $target = join "/", $this->{flexlm64}, $lib;
          } else {
            $target = join "/", $this->{ES3Flex}, $lib; 
          }
          $target .= ".".$this->{ext}[0];

          $this->{flexLm}->AddLink($link, $target);
        }
    }
}

#------------------------------------------------------------------------------
# InitQt library links
#------------------------------------------------------------------------------

sub InitQTLibLinks
{
    my $this      = shift;
    my $ver       = $this->{vInfo}->QtVersion();
    my $qtHome    = $this->{fileNames}->QTHome($this->{args}->Platform(), $this->{args}->Target());
    my $qtLibDir  = "$qtHome/lib";
    my $qtLinkDir = join "/", $this->{fileNames}->CarbonHome(), 
                    $this->{carbonArch}, "lib";

    my $qtPluginsDir  = "$qtHome/plugins/script";
    my $qtPluginsLinkDir = join "/", $this->{fileNames}->CarbonHome(), 
                    $this->{carbonArch}, "lib/plugins/script";

    my $qtImgPluginsDir  = "$qtHome/plugins/imageformats";
    my $qtImgPluginsLinkDir = join "/", $this->{fileNames}->CarbonHome(), 
                    $this->{carbonArch}, "lib/plugins/imageformats";


    $this->{qt}   = new LinkGroup();
    my @libs      = CDSSystem::OpenDir($qtLibDir);

    foreach (@libs)
    {
        if ($_ =~ /libQt(Core|Gui|Xml|Network|WebKit|Script|UiTools|3Support)|qscintilla2/)
        {
            if ($_ =~ /\.(so|so\.$ver|so\.4|so\.1)$/)
            {
                $this->{qt}->AddLink("$qtLinkDir/$_","$qtLibDir/$_");
            }
        }
    }

    my @slibs      = CDSSystem::OpenDir($qtPluginsDir);

    foreach (@slibs)
    {
       $this->{qt}->AddLink("$qtPluginsLinkDir/$_","$qtPluginsDir/$_");
    }


    my @ilibs      = CDSSystem::OpenDir($qtImgPluginsDir);

    foreach (@ilibs)
    {
       $this->{qt}->AddLink("$qtImgPluginsLinkDir/$_","$qtImgPluginsDir/$_");
    }

}

sub ClassType
{
    my $this = shift;
    return ref($this);
}


1;
