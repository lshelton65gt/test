# this is an older tool for printing the contents of .ccfg files developed before we switched to use .xml format.
#
## NOTE if you want to insert a breakpoint into this file just put the following two stmts at the point you want to break:
##     import pdb; pdb.set_trace()


import sys
import Carbon.Cfg

def displayLabel(label, indent = 0):
  print '%s%s' % (' ' * indent, label)

def displayValue(label, value, indent = 0):
  print '%s%s: %s' % (' ' * indent, label, value)

def displayAttrs(obj, attrs, indent = 4):
  for attr in attrs:
    displayValue(attr, getattr(obj, attr)(), indent)

def displayPort(port, indent = 4):
  displayAttrs(port, ('name', 'shortName', 'carbonName', 'width', 'carbonNetId', 'expr'), indent)

def displayClock(clock, indent = 4):
  displayPort(clock, indent)
  displayAttrs(clock, ('initialValue', 'delay', 'clockCycles', 'compCycles', 'dutyCycle'), indent)

def displayReset(reset, indent = 4):
  displayPort(reset, indent)
  displayAttrs(reset, ('activeValueHigh', 'activeValueLow', 'inactiveValueHigh', 'inactiveValueLow', 'clockCycles', 'compCycles', 'cyclesBefore', 'cyclesAsserted', 'cyclesAfter'), indent)

def displayTie(tie, indent = 4):
  displayPort(tie, indent)
  numWords = tie.numWords()
  displayValue('numWords', numWords, indent)
  for i in range(numWords):
    displayValue('word[%s]' % i, tie.getWord(i), indent)

def displayDebugReg(reg, indent = 4):
  displayAttrs(reg, ('name', 'group', 'width', 'comment', 'radix', 'bigEndian', 'offset'), indent)
  displayLabel("fields:", indent)
  for i in range(len(reg.fields())):
    displayDebugRegField(reg.fields()[i])
    print

def displayDebugRegField(field, indent = 8):
  displayAttrs(field, ('name', 'high', 'low', 'access'), indent)
  displayLabel("location:", indent)
  displayDebugRegLoc(field.loc())
  
def displayDebugRegLoc(loc, indent = 12):
  displayAttrs(loc, ('type',), indent)
  if (loc.type() == 'constant'):
    displayDebugRegLocConstant(loc.castConstant())
  elif (loc.type() == 'register'):
    displayDebugRegLocRegister(loc.castRegister())
  else:
    displayDebugRegLocArray(loc.castArray())

def displayDebugRegLocConstant(loc, indent = 12):
  displayAttrs(loc, ('value',), indent)

def displayDebugRegLocRegister(loc, indent = 12):
  displayAttrs(loc, ('path', 'hasRange', 'left', 'right', 'lsb', 'msb'), indent)

def displayDebugRegLocArray(loc, indent = 12):
  displayAttrs(loc, ('path', 'hasRange', 'left', 'right', 'lsb', 'msb', 'index'), indent)

def displayMemory(mem, indent = 4):
  displayAttrs(mem, ('name', 'signal', 'maxAddrs', 'width', 'numCycles',
                     'readWrite', 'comment', 'initFile', 'initFileType',
                     'initType', 'baseAddrHi', 'baseAddrLo'), indent)

  displayLabel('Memory Blocks', indent)
  for block in mem.blocks():
    displayMemoryBlock(block)
    
def displayMemoryBlock(block, indent = 8):
  displayAttrs(block, ('name', 'baseAddrLo', 'baseAddrHi', 'sizeLo',
                       'sizeHi', 'width', 'hasFixedAddr'), indent)

  displayLabel('Memory Locators', indent)
  for loc in block.locs():
    displayMemoryLoc(loc)

def displayMemoryLoc(loc, indent = 12):
  displayAttrs(loc, ('type', 'displayStartWord', 'displayEndWord', 'displayMsb', 'displayLsb'), indent)
  if loc.type() == 'rtl':
    displayMemoryLocRtl(loc.castRTL(), indent)
  elif loc.type() == 'port':
    displayMemoryLocPort(loc.castPort(), indent)

def displayMemoryLocRtl(loc, indent = 12):
  displayAttrs(loc, ('path', 'addrOffset', 'endWord', 'msb', 'lsb', 'width'), indent)
  
def displayMemoryLocPort(loc, indent = 12):
  displayAttrs(loc, ('port', 'hasFixedAddr'), indent)
  
def displayTransactorPort(port, indent = 4):
  displayPort(port, indent)

def displayTransactor(xtor, indent = 4):
  displayAttrs(xtor, ('name', 'type', 'numInputs', 'numOutputs'), indent)
  clockMaster = xtor.clockMaster()
  if clockMaster:
    clockMaster = clockMaster.name()
  displayValue('clockMaster', clockMaster, indent)
  displayLabel('Inputs', indent)
  for port in xtor.inputs():
    displayTransactorPort(port, indent + 4)
    print
  displayLabel('Outputs', indent)
  for port in xtor.outputs():
    displayTransactorPort(port, indent + 4)
    print
  displayLabel('Unconnected', indent)
  for port in xtor.unconnected():
    displayTransactorPort(port, indent + 4)
    print

def displayProfileBucket(bucket, indent = 4):
  displayAttrs(bucket, ('name', 'index', 'expr', 'color'), indent)

def displayProfileChannelDescriptor(cd, indent = 4):
  displayAttrs(cd, ('name', 'index', 'numBuckets'), indent)
  displayLabel('Buckets', indent)
  for bucket in cd.buckets():
    displayProfileBucket(bucket, indent + 4)
    print

def displayProfileTrigger(trigger, indent = 4):
  displayAttrs(trigger, ('expr', 'index'), indent)
  displayLabel('Channels', indent)
  for channel in trigger.channels():
    displayProfileChannel(channel, indent + 4)
    print

def displayProfileNet(net, indent = 4):
  displayAttrs(net, ('name', 'path', 'width'), indent)

def displayProfileChannel(channel, indent = 4):
  displayAttrs(channel, ('name', 'index', 'expr', 'numBuckets',
                         'numTotalBuckets'), indent)
  displayLabel('Buckets', indent)
  for bucket in channel.buckets():
    displayProfileBucket(bucket, indent + 4)  
    print

def displayProfileStream(stream, indent = 4):
  displayAttrs(stream, ('name', 'index', 'numChannels', 'numNets'), indent)
  # displayLabel('Trigger', indent)
  # displayProfileTrigger(stream.trigger(), indent + 4)
  displayLabel('Nets', indent)
  for net in stream.nets():
    displayProfileNet(net, indent + 4)
    print
  displayLabel('Triggers', indent)
  for trigger in stream.triggers():
    displayProfileTrigger(trigger, indent + 4)
    print
  displayLabel('ChannelDescriptors', indent)
  for cd in stream.channelDescriptors():
    displayProfileChannelDescriptor(cd, indent + 4)
    print

def displayParameter(param, indent=4):
  displayAttrs(param, ('name', 'type', 'scope', 'value', 'defaultValue', 'description'), indent)

# check args
if len(sys.argv) < 2:
  print 'usage: %s file.ccfg\n' % sys.argv[0]
  sys.exit(1)

# read the .ccfg file
ccfgName = sys.argv[1]
mode = Carbon.Cfg.eCarbonXtorsMaxsim

for arg in sys.argv:
  if arg == "-coware":
    mode = Carbon.Cfg.eCarbonXtorsCoware
  elif arg == "-maxsim":
    mode = Carbon.Cfg.eCarbonXtorsMaxsim
    
try:
  config = Carbon.Cfg.Config(ccfgName, [], mode)
except Carbon.Cfg.ConfigError,e:
  print e
  sys.exit(1)

# print information about the config as exposed in the Python layer
displayValue('compName', config.compName())
displayValue('topModuleName', config.topModuleName())
displayValue('ioDbFile', config.ioDbFile())
displayValue('number of inputs', len(config.inputs()))
displayValue('number of outputs', len(config.outputs()))
displayValue('number of clocks', len(config.clocks()))
displayValue('number of resets', len(config.resets()))
displayValue('number of disconnects', len(config.disconnects()))
displayValue('number of transactors', len(config.transactors()))
displayValue('number of nullInputs', len(config.nullInputs()))
displayValue('number of nullOutputs', len(config.nullOutputs()))
displayValue('number of ties', len(config.ties()))
displayValue('number of debug regs', config.numDebugRegs())
displayValue('number of debug memories', config.numDebugMemories())
displayValue('useLegacyMemories', config.useLegacyMemories())
displayValue('waveType', config.waveType())
displayValue('waveFilename', config.waveFilename())
displayValue('number of profile streams', config.numProfileStreams())

displayLabel('Inputs')
for input in config.inputs():
  displayPort(input)
  print

displayLabel('Outputs')
for output in config.outputs():
  displayPort(output)
  print

displayLabel('Clocks')
for clock in config.clocks():
  displayClock(clock)
  print

displayLabel('Resets')
for reset in config.resets():
  displayReset(reset)
  print

displayLabel('Disconnects')
for disconnect in config.disconnects():
  displayValue('disconnect', disconnect)
  print

displayLabel('Transactors')
for transactor in config.transactors():
  displayTransactor(transactor)
  print

displayLabel('Null Inputs')
for nullInput in config.nullInputs():
  displayTransactor(nullInput)
  print

displayLabel('Null Outputs')
for nullOutput in config.nullOutputs():
  displayTransactor(nullOutput)
  print

displayLabel('Ties')
for tie in config.ties():
  displayTie(tie)
  print

displayLabel('Debug Regs')
for reg in config.debugRegs():
  displayDebugReg(reg)
  print

displayLabel('Debug Memories')
for mem in config.debugMemories():
  displayMemory(mem)
  print

displayLabel('Profile Streams')
for stream in config.profileStreams():
  displayProfileStream(stream)
  print

displayLabel('Parameters')
for param in config.parameters():
  displayParameter(param)
  print

displayLabel('Transactor Parameters')
for transactor in config.transactors():
  for param in transactor.params():
    displayParameter(param)
    print
