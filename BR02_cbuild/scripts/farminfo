#!/bin/sh
# -*-Perl-*-
exec $CARBON_HOME/bin/perl -S -x -- "$0" "$@"
#!perl

use strict;
use Getopt::Long;
$| = 1; # non-buffered output

my $usage =<<EndOfUsage;
farminfo: gets info on linux and sunos farm hosts.
  requires \$DeskFarm, \$RackFarm, and \$SunFarm env vars to be set, which
  should be the case with default SGE setup.

Options:
  -html		Print an html page containing farm information
  -ascii        Print a text table to stdout contatining farm information. This
		is the default setting.
  -linux        Print only info on linux farm machines
  -sunos        Print only info on solaris farm machines
  -help         Print this help message

EndOfUsage

use vars qw($htmlArg $asciiArg $showLinux $showSunOS $HelpArg);
GetOptions(
  'html'  => \$htmlArg,
  'ascii' => \$asciiArg,
  'linux' => \$showLinux,
  'sunos' => \$showSunOS,
  'help'  => \$HelpArg,
);

# each ssh cmd will timeout after this many seconds
my $sshTimeout = 60;

if ($HelpArg) {
  print $usage;
  exit(1);
}

# default to text output
unless($htmlArg) {
  $asciiArg = 1;
}

# default is both linux & sun
unless($showLinux or $showSunOS) {
  $showLinux = 1;
  $showSunOS = 1;
}

# Make sure farm env vars are set
unless (exists($ENV{'DeskFarm'})) {
  die "\$DeskFarm is not set, cannot check farm machines\n";
}
unless (exists($ENV{'RackFarm'})) {
  die "\$DeskFarm is not set, cannot check farm machines\n";
}
unless (exists($ENV{'SunFarm'})) {
  die "\$DeskFarm is not set, cannot check farm machines\n";
}

# what time is it?
my $now = localtime();

# for html output, print a header
if($htmlArg){
  print <<EndOfHeader;
<html><head><title>Farm Information</title></head>
<body>
<h2>SGE Farm Information</h2>
<i>Last Modified: $now</i>
EndOfHeader
}

my @skippedhosts = ();
my @ypcatOut = `ypcat hosts`;

if($showLinux) {

  if($htmlArg) {
    print <<EndOfHeader;
<h2>Linux</h2>
<table border=1 cellpadding=2>
<tr>
  <th align=left colspan=2>Name</th>
  <th align=left>MHz</th>
  <th align=left># CPUs</th>
  <th align=left>CPU Type</th>
  <th align=left>Mem (Mb)</th>
  <th align=left>Linux Vers</th>
  <th align=left>Kernel Rev</th>
<tr>
EndOfHeader
  } else { # ascii

    printf "%-20s%-6s%-8s%-10s%-7s%-9s%-10s\n",
      "Name", "MHz", "# CPUs", "CPU Type", "Mem Mb", "Linux", "Kernel";
    print "+" x 80 . "\n"

  }

  # assemble list of all linux farm machines
  my @LinuxFarm = split('\s+', $ENV{'DeskFarm'}." ".$ENV{'RackFarm'});

  foreach my $host (sort(@LinuxFarm)) {

    # get short host name
    my $shortName = (split(/\s+/, (grep(/$host\s/,@ypcatOut))[0]))[2];
    if ($shortName =~ m/^\s*$/) {
      $shortName = $host;
      $shortName =~ s/farm/f/;
    }
    $shortName =~ s/\.cds$//;

    # ssh to the machine to get all info
    my @info = ();;
    $SIG{ALRM} = \&ssh_timed_out;
    eval {
      alarm($sshTimeout);
      @info = `ssh $host cat /proc/version /etc/redhat-release /proc/cpuinfo /proc/meminfo 2>&1`;
      alarm(0);
    };
    if ($@ =~ /TIMED OUT/){
      #print STDERR "ssh to $host timed out, skipping!\n";
      push @skippedhosts, $host;
      next;
    } elsif($? != 0) {
      #print STDERR "could not ssh to $host, skipping!\n";
      push @skippedhosts, $host;
      next;
    } 

    use vars qw($cpuSpeed $numProc $cpuType $mem $kernelRev $linuxRev);

    # get cpu speed
    $cpuSpeed = (split('[\s\.]+', (grep(/MHz/, @info))[0] ))[3];

    # get number of processors
    $numProc = scalar(grep(/^processor\s+:/, @info));

    # get cpu type
    $cpuType = (split('\s+', (grep(/^model name/, @info))[0]))[3];
    $cpuType =~ s/\(R\)$//;

    # get memory
    #$mem = int ((split('\s+', (grep(/^MemTotal:/, @info))[0]))[2] / 1000);
    $mem = int ((split('\s+', (grep(/^MemTotal:/, @info))[0]))[1] / 1000);

    # get kernel revision
    $kernelRev = (split('\s+', $info[0]))[2];

    # get linux revision
    $linuxRev = $info[1];
    $linuxRev =~ s/Update ([0-9]+)/\.$1/;
    $linuxRev =~ s/[^0-9\.]//g;

    if($htmlArg) {
      #print in html
      print <<EndOfRow;
  <tr>
    <td>$host</td><td>$shortName</td>
    <td>$cpuSpeed</td>
    <td>$numProc</td>
    <td>$cpuType</td>
    <td>$mem</td>
    <td>$linuxRev</td>
    <td>$kernelRev</td>
  </tr>
EndOfRow
    } else {
      # print in text
      printf "%-14s%-6s%-6s%-8s%-10s%-7s%-9s%-10s\n", $host, $shortName,
        $cpuSpeed, $numProc, $cpuType, $mem, $linuxRev, $kernelRev;
    }
  }

  # close the table
  if($htmlArg) {
    print <<EndOfFooter;
</table>
EndOfFooter
  }

  # print machines with problems
  if (($htmlArg) and scalar(@skippedhosts)) {
    print "<p>There were problems connecting to these machines:<br>".
      join("<br>", @skippedhosts) . "</p>\n\n";
  } elsif (scalar(@skippedhosts)) {
    print "\nThere were problems connecting to these machines:\n".
      join("\n", @skippedhosts). "\n\n";
  }

  @skippedhosts = ();
}

if($showSunOS) {
  # print a sun os header
  if($htmlArg){
    print <<EndOfHeader;
<h2>SunOS</h2>
<table border=1 cellpadding=2>
<tr>
  <th align=left colspan=2>Name</th>
  <th align=left>MHz</th>
  <th align=left># CPUs</th>
  <th align=left>CPU Type</th>
  <th align=left>Mem (Mb)</th>
  <th align=left>OS Vers</th>
<tr>
EndOfHeader
  } else { # ascii

    printf "%-20s%-6s%-8s%-12s%-10s%-12s\n",
      "Name", "MHz", "# CPUs", "CPU Type", "Mem Mb", "OS Vers";
    print "+" x 80 . "\n"

  }

  # get list of all sun machines
  my @SunFarm = split('\s+', $ENV{'SunFarm'});
  foreach my $host (sort(@SunFarm)) {

    # get short host name
    my $shortName = (split(/\s+/, (grep(/$host\s/,@ypcatOut))[0]))[2];
    if ($shortName =~ m/^\s*$/) {
      $shortName = $host;
      $shortName =~ s/sunfarm/sf/;
    }

    # ssh to the machine to get all info
    my @info = ();
    $SIG{ALRM} = \&ssh_timed_out;
    eval {
      alarm($sshTimeout);
      @info = `ssh $host 'uname -a; top -d1; /usr/sbin/psrinfo -v' 2>&1`;
      alarm(0);
    };
    if ($@ =~ /TIMED OUT/) {
      #print STDERR "could not ssh to $host, skipping!\n";
      push @skippedhosts, $host;
      next;
    } elsif ($? != 0) {
      #print STDERR "could not ssh to $host, skipping!\n";
      push @skippedhosts, $host;
      next;
    }

    use vars qw($cpuSpeed $numProc $cpuType $mem $osRev);

    # get cpu speed
    my $tmpStr = (grep(/ operates at /, @info))[0];
    $tmpStr =~ s/^\s+//;
    my @tmpArr = split(/\s+/, $tmpStr);
    $cpuSpeed = $tmpArr[5];

    # get number of processors
    $numProc = scalar(grep(/^Status of processor /, @info));

    # get cpu type
    $cpuType = $tmpArr[1];

    # get physical memory
    $mem = (split(/\s+/, (grep(/^Memory:/, @info))[0]))[1];

    # get solaris revision
    $osRev = (split(/\s+/, $info[0]))[2];

    if($htmlArg) {
      #print in html
      print <<EndOfRow;
  <tr>
    <td>$host</td><td>$shortName</td>
    <td>$cpuSpeed</td>
    <td>$numProc</td>
    <td>$cpuType</td>
    <td>$mem</td>
    <td>$osRev</td>
  </tr>
EndOfRow
    } else {
      # print in text
      printf "%-14s%-6s%-6s%-8s%-12s%-10s%-12s\n", $host, $shortName,
        $cpuSpeed, $numProc, $cpuType, $mem, $osRev;
    }
  }

  # close table
  if($htmlArg) {
    print "</table>";
  }
  # print machines with problems
  if (($htmlArg) and scalar(@skippedhosts)) {
    print "<p>There were problems connecting to these machines:<br>".
      join("<br>", @skippedhosts) . "</p>\n\n";
  } elsif (scalar(@skippedhosts)) {
    print "\nThere were problems connecting to these machines:\n".
      join("\n", @skippedhosts). "\n";
  }
}

# close html
if ($htmlArg) {
  print "</body><html>";
}



# timeout signal handler
sub ssh_timed_out {
  die "TIMED OUT";
}
