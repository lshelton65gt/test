#!perl -w

package PerfConfig;
use strict;

# NOTE: To get a usage line for all routines in this package, grep for '^###'
# TODO: generic getter/setter routine

use Carp;
use Data::Dumper;

### \$PerfConfigObj = new PerfConfig ( [$filename] )
sub new {
  my $class = shift;
  my $inputFile = shift;
  my $self = {};
  my $_usingChecker = 1;

  # XML::Checker::Parser isn't available on SunOS, which is ok, since we only
  # use it to do DTD validation. as long as this validation is happening on
  # Linux, no problem. test/infrastructure ensures that it is.
  # so, 1st try to use the validating parser, then fall back on the vanilla one
  eval 'use XML::Checker::Parser;';
  if ($@) {
    $_usingChecker = 0;
    eval 'use XML::Parser;';
  }

  # setup some refs for later use
  $self->{'_usingChecker'} = \$_usingChecker;
  $self->{'_perfs'} = {};
  $self->{'_errStr'} = "";
  $self->{'_curElem'} = "";
  $self->{'_curName'} = "";
  my $_parser;  $self->{'_parser'} = \$_parser;

  bless $self, $class;
  $self->_initialize($inputFile);
  return $self;
}

# _initialize ($inputFile)
# sets up xml parser, parses file, and builds a tree representing the whole
# test list file
sub _initialize {
  my $self = shift;
  my $inFile = shift;
  my $onlyTheseTests = shift;

  # make sure input file exists
  if ($inFile and not (-e $inFile)) {
    die "$inFile not found: $!";
  }

  # if given a file, then we need to build the testlist tree
  if ($inFile) {
    # set up parser
    if ($self->{'_usingChecker'}) {
      # have to eval these since the packages will not be imported at compile
      # time. (see eval statements in new() above). The wierd syntax in the
      # handler definitions seems to be the only way to return subroutine refs
      # for this instance's member routines. otherwise, we have to use globals
      # for XML population, and reinit them upon finishing XML processing in
      # order to remain re-entrant
      eval "\$self->{'_parser'} = new XML::Checker::Parser( 
	        Handlers => {Start => sub { return \$self->_XMLElemStart(\@_)},
			     Char =>  sub { return \$self->_XMLElemChar(\@_)},
			     End =>   sub { return \$self->_XMLElemEnd(\@_)}},
			     ParseParamEnt => 1, SkipExternalDTD => 1,
                             SkipInsignifWS => 1 ); "
    } else {
      eval "\$self->{'_parser'} = new XML::Parser( 
		Handlers => {Start => sub { return \$self->_XMLElemStart(\@_)},
		  	     Char =>  sub { return \$self->_XMLElemChar(\@_)},
			     End =>   sub { return \$self->_XMLElemEnd(\@_)}}
           );"
    }
    
    # parse it - this will die if document is not well-formed
    $self->{'_parser'}->parsefile($inFile);
    unless ($self->{'_errStr'} eq "") {
      die "ERRORS PARSING $inFile!\n". $self->{'_errStr'} . "\n";
    }
  }

}

# processing element start tag just means setting cur elem
sub _XMLElemStart {
  my ($self, $expat, $elem) = @_;
  my $_perfs = $self->{'_perfs'};
  # don't need to deal with attrs, so just set the elem name for later use
  $self->{'_curElem'} = $elem;
  # and set parent to design or simulation when seen
  if (($elem eq 'design') or ($elem eq 'simulation')) {
    $self->{'_parentElem'} = $elem;
    #reset sim hash
    $self->{'_tmpSimHash'} = ();
  }
  # group and sim are only elems we can have multiples of. otherwise,
  # if already have something defined in $_perfs{curName}, error
  if ($self->{'_curName'} and (exists($_perfs->{$self->{'_curName'}}{$elem})) 
      and ($elem ne 'group') and ($elem ne 'simulation')) {
    $self->{'_errStr'}.= "ERROR: Multiple $elem tags in design ".
      $self->{'_curName'}."\n";
  }
}

# this is where the tree is generated - map everything to hash of names
sub _XMLElemChar {
  my ($self, $expat, $charData) = @_;
  my $_perfs = $self->{'_perfs'};

  # if we're looking at a design name, just set curName and move on
  if (($self->{'_curElem'} eq "name") and ($self->{'_parentElem'} eq 'design')){
    
    # check that another test with this name dne
    if(exists($_perfs->{$charData})) {
      $self->{'_errStr'} .= "ERROR: Found multiple tests named $charData\n";
    }
    # set the current name
    $self->{'_curName'} = $charData;
    # set an empty group list
    $_perfs->{$self->{'_curName'}}{'group'} = [];

  # if perflist or design or sim triggered this, report an error
  } elsif (($self->{'_curElem'} eq "perflist") or
           ($self->{'_curElem'} eq "design") or
	   ($self->{'_curElem'} eq 'simulation')) {
    $self->{'_errStr'} .= "ERROR: Found character data in a ".
      $self->{'_curElem'} . " element\n";

  # multiple groups are supported, but char data is not guaranteed to come
  # in the same chunk. so append to a tmp hash value, then let end routine
  # flush it
  } elsif ($self->{'_curElem'} eq 'group') {
    $self->{'_curCharData'} .= $charData;

  # if we're inside a sim, assemble a tmp sim hash, we'll finish constructing
  # it when the sim tag is closed
  } elsif ($self->{'_parentElem'} eq 'simulation') {
    $self->{'_tmpSimHash'}{$self->{'_curElem'}} .= $charData;

  # if an elem that is sim specific, then set up a sim ref for this design
  } elsif (($self->{'_curElem'} eq 'cmd') or
	   ($self->{'_curElem'} eq 'iter') or
	   ($self->{'_curElem'} eq 'cycles')) {
    $self->{'_tmpSimHash'}{$self->{'_curElem'}} .= $charData;

  # all other elements just map to a name. since char data is not guaranteed
  # to come all in one chunk, append to whatever is there
  } else {
    $_perfs->{$self->{'_curName'}}{$self->{'_curElem'}} .= $charData;
  }
}

# process any necc elem end tags
sub _XMLElemEnd {
  my ($self, $expat, $elem) = @_;
  my $_perfs = $self->{'_perfs'};

  # only need to process group and sim tags, since each elem needs to be
  # pushed onto an array. not guaranteed to have ALL data from the elem until
  # here
  if ($elem eq 'group') {
    push (@{$_perfs->{$self->{'_curName'}}{$elem}}, $self->{'_curCharData'});
    $self->{'_curCharData'} = '';
  }
  # closing a sim tag, assemble necessary parts of sim hash
  if ($elem eq 'simulation') {
    # make sure design name has no '-' char (since we use that to deduce
    # design name from sim name)
    if ($self->{'_curName'} =~ /\-/) {
      $self->{'_errStr'} .= "ERROR: design name can't contain '-' if <simulation> tags are supplied\n";
    }
    # make sure sim name starts with designName-
    my $tmpDesignName = $self->{'_curName'};
    unless ($self->{'_tmpSimHash'}{'name'} =~ /^$tmpDesignName-/) {
      $self->{'_errStr'} .= "ERROR: simulation name '". $self->{'_tmpSimHash'}{'name'}. "' should begin with design name '$tmpDesignName-'\n";
    }
    # just reference this hash in the main perfs hash
    push(@{$_perfs->{$self->{'_curName'}}{'simulation'}}, $self->{'_tmpSimHash'});
    # and if ending sim, parent should be set back to design
    $self->{'_parentElem'} = 'design';
  }

  # closing design, reference the implied simulation hash if there weren't
  # explicit ones
  if (($elem eq 'design') and not
      (exists($_perfs->{$self->{'_curName'}}{'simulation'}))) {
    $self->{'_tmpSimHash'}{'name'} = $self->{'_curName'};
    push(@{$_perfs->{$self->{'_curName'}}{'simulation'}}, $self->{'_tmpSimHash'});
  }
}

#
# access functions (no need for modify functions)
#

### \@designs = &getDesigns([$groupName])
sub getDesigns {
  my ($self, $groupName) = @_;
  my $_perfs = $self->{'_perfs'};
  my @lDesigns;

  # return keys of %_perfs for which groupName matches
  if ($groupName) {
    return $self->getDesignsForGroup($groupName);
  
  # or return them all if group not specified 
  } else { 
    @lDesigns = keys(%$_perfs);
    return \@lDesigns;
  }
}

### $rootTreeName = &getRootTree ( $designName )
# given a design name, return the root test tree that needs to be checked out
sub getRootTree {
  my ($self, $designName) = @_;
  return $self->getValueForDesign($designName, 'tree');
}

### $nodeName = &getNode ( $designName )
# given a design name, return the hostname for the node to run on
# getNode should word whether given a design or a sim
sub getNode {
  my ($self, $designOrSimName) = @_;
  if($self->isValidTest($designOrSimName)) {
    return $self->getValueForDesign($designOrSimName, 'node');
  } else { # must be a sim
    my $design = $designOrSimName;
    $design =~ s/-.*$//;
    return $self->getValueForDesign($design, 'node');
  }
}

### $testName = &getTest ( $designName )
# given a design name, return the test name suitable for passing to runsingle
sub getTest {
  my ($self, $designName) = @_;
  return $self->getValueForDesign($designName, 'test');
}

### $targetName = &getTargetName ( $designName )
# given a design name, return the targetName (for use in things like
# lib<target>.a
sub getTargetName {
  my ($self, $designName) = @_;
  my $targetName = $self->getValueForDesign($designName, 'targetname');
  # if elem has not been defined for this design, return design name
  unless($targetName) { $targetName = $designName; }
  return $targetName;
}

### $dependencyName = &getDependency ( $designName )
# given a design name, return the test dependency name suitable for passing
# to runsingle that should be run before the test itself. note that if the
# tests are configured properly in TestList, the dep should be unnecc
sub getDependency {
  my ($self, $designName) = @_;
  return $self->getValueForDesign($designName, 'dep');
}

### $commandToRun = &getCommand ( $designOrSimName )
# given design or sim name, return the command line to run for performance data
# collection
sub getCommand {
  my ($self, $designOrSimName) = @_;
  # the secret is that all cmds are stored using simulation name
  return $self->getValueForSimulation($designOrSimName, 'cmd');
}

### $iterationCount = &getIterCount ( $designOrSimName )
# given design or sim name, return the number of iters of perf cmd to perform
sub getIterCount {
  my ($self, $designOrSimName) = @_;
  # the secret is that all iters are stored using simulation name
  return $self->getValueForSimulation($designOrSimName, 'iter');
}

### $cycleCount = &getClockCycles ( $designOrSimName )
# given a design or sim name, return the total number of cycles in that design
sub getClockCycles {
  my ($self, $designOrSimName) = @_;
  # the secret is that all iters are stored using simulation name
  return $self->getValueForSimulation($designOrSimName, 'cycles');
}

### $order = &getSimOrder ( $simulationName )
# given a sim name, return it's order value
sub getSimOrder {
  my ($self, $simulationName) = @_;
  return $self->getValueForSimulation($simulationName, 'order');
}

### $CalltreeCmd = &getCalltreeCmd ( $designName )
# given a design name, return a 'calltree' command to run after perf data
# collection
sub getCalltreeCmd {
  my ($self, $designName) = @_;
  return $self->getValueForDesign($designName, 'calltree');
}

### $cbuildLogFile = &getCbuildLogFile ( $designName )
# given a design name, return the cbuild log file name for that design
sub getCbuildLogFile {
  my ($self, $designName) = @_;
  my $log = $self->getValueForDesign($designName, 'cbuildlog');
  # default to <design>.cbld.log
  unless($log) { $log = "$designName.cbld.log"; }

  # return the scalar
  return $log;
}

### \@groups = &getGroups ( $designName )
# given a design name, return a ref to list of groups defined for that design
sub getGroups {
  my ($self, $designName) = @_;
  my $_perfs = $self->{'_perfs'};
  if (scalar(@{$_perfs->{$designName}{'group'}}) == 0) {
    return [];
  } else {
    return $_perfs->{$designName}{'group'};
  }
}

### \@simNames = &getSimulationNames ( $designName )
# given a design name, return a ref to list of simulations defined for it
sub getSimulationNames {
  my ($self, $designName) = @_;
  my $_perfs = $self->{'_perfs'};
  unless (exists($_perfs->{$designName}{'simulation'})) {
    return [];
  } else {
    my @simNames;
    foreach my $simRef (@{$_perfs->{$designName}{'simulation'}}) {
      push(@simNames, $simRef->{'name'});
    }
    return \@simNames;
  }
}

### \@simNames = &getSimulationsForDesigns ( \@designs )
# given a list of design names, return a list of all sim names corresponding
# to those designs. return empty list ref on error
sub getSimulationsForDesigns {
  my ($self, $designsRef) = @_;
  my $_perfs = $self->{'_perfs'};
  my @sims;
  foreach my $designName (@$designsRef) {
    unless ($self->isValidTest($designName)) {
      return [];
    }
    push(@sims, @{$self->getSimulationNames($designName)});
  }
  return \@sims;
}

### $boolean = &isTestInGroup ( $designName, $groupName )
# is given design a member of the specified group? 1 if so, 0 if not
sub isTestInGroup {
  my ($self, $designName, $groupName) = @_;
  if (scalar(@{$self->getGroups($designName)} > 0) and 
      grep(/^$groupName$/, @{$self->getGroups($designName)})) {
    return 1;
  } else { 
    return 0;
  }
}

### $boolean = &isValidTest ( $designName )
# given a design name, return 1 if it is defined, 0 if not
sub isValidTest {
  my ($self, $designName) = @_;
  my $_perfs = $self->{'_perfs'};

  # if designName is in hash, return 1
  if ($designName and exists($_perfs->{$designName})) {
    return 1;
  } else {
    return 0;
  }
}

### $boolean = &isValidSimulation ( $simName )
# given a design name, return 1 if it is defined, 0 if not
sub isValidSimulation {
  my ($self, $simName) = @_;
  my $_perfs = $self->{'_perfs'};

  # hack design name off front of sim name unless this is a 1 sim test, in
  # which case design and sim share same name
  my $designName = $simName;
  unless($self->isValidTest($designName)) {
    $designName =~ s/-.*$//;

    # make sure modified design name is valid
    unless($self->isValidTest($designName)) {
      return 0;
    }
  }

  foreach my $simRef (@{$_perfs->{$designName}{'simulation'}}) {
    if ($simRef->{'name'} eq $simName) {
      return 1;
    }
  }

  # didnt find it yet, doesn't exist
  return 0;
}

### $value = &getValueForDesign($designName, $key)
sub getValueForDesign {
  my ($self, $designName, $key) = @_;
  my $_perfs = $self->{'_perfs'};

  # if designName is not in hash, return -1
  unless ($self->isValidTest($designName)) {
    return -1;
  }

  return $_perfs->{$designName}{$key};
}

### $value = &getValueForSimulation($simName, $key)
sub getValueForSimulation {
  my ($self, $simName, $key) = @_;
  my $_perfs = $self->{'_perfs'};

  
  # hack design name off front of sim name
  my $designName = $simName;
  unless ($self->isValidTest($designName)) {
    $designName =~ s/-.*$//;
  }

  # if simName not in hash for design, return -1
  unless ($self->isValidSimulation($simName)) {
    return -1;
  }

  # cycle thru sims, when the right name is found, return the value
  foreach my $simRef (@{$_perfs->{$designName}{'simulation'}}) {
    if($simRef->{'name'} eq $simName) {
      return $simRef->{$key};
    }
  }

  # nothing? return -1
  return -1;
}

### \@designs = &getDesignsForNode ( $nodeName )
# given a node name, return a ref to a list containing all designs assigned
# to that node
sub getDesignsForNode {
  my ($self, $nodeName) = @_;
  my $_perfs = $self->{'_perfs'};
  my @lDesigns = grep {
		   $_perfs->{$_}{'node'} eq $nodeName
                 } keys(%$_perfs);

  return \@lDesigns;
}

### \@designs = &getDesignsForGroup ( $groupName )
# given a group name, return ref to list of all designs in that group
sub getDesignsForGroup {
  my ($self, $groupName) = @_;
  my $_perfs = $self->{'_perfs'};
  my @lDesigns = grep { grep (/^$groupName$/, @{$self->getGroups($_)})
                      } keys(%$_perfs);
  return \@lDesigns;
}

### 1 = &dumpPerfTree()
# a debug utility that will dump everything to stdout
sub dumpPerfTree {
  my $self = shift;

  my $_perfs = $self->{'_perfs'};
  print scalar(keys(%$_perfs)) . " tests defined\n";
  foreach my $design (sort(keys(%$_perfs))) {
    print "Design: $design\n";
    print "  node: " . $_perfs->{$design}{'node'} . "\n";
    print "  test: " . $_perfs->{$design}{'test'} . "\n";
    print "  dep: " . $_perfs->{$design}{'dep'} . "\n";
    print "  cbuildlog: " . $_perfs->{$design}{'cbuildlog'} . "\n";
    print "  root: ". $_perfs->{$design}{'tree'} . "\n";
    print "  groups: " . join(', ', @{$_perfs->{$design}{'group'}}) . "\n";
    if ($_perfs->{$design}{'simulation'}) {
      print "  sims: (". scalar(@{$_perfs->{$design}{'simulation'}}). ")\n";
    }
    foreach my $sim (@{$_perfs->{$design}{'simulation'}}) {
      print "    name: ". $sim->{'name'} . "\n";
      print "      order: ". $sim->{'order'} . "\n";
      print "      iter: ". $sim->{'iter'} . "\n";
      print "      cycles: ". $sim->{'cycles'} . "\n";
      print "      cmd: ". $sim->{'cmd'} . "\n";
    }
  }

}

### $statusInt = &dumpToXML( $xmlOutputFileName )
# write out the current %_perfs tree to the specified xml file
# return 1 on success, 0 on fail
sub dumpToXML {
  my ($self, $xmlOutFile) = @_;
  my $_perfs = $self->{'_perfs'};

  unless(open(XMLOUT, ">$xmlOutFile")) {
    print STDERR "Unable to create $xmlOutFile: $!\n";
    return 0;
  }

  # print the xml header and the testlist start tag
  print XMLOUT <<EndOfStart;
<?xml version="1.0"?>
<perflist>
EndOfStart

  # cycle through %_perfs
  foreach my $iTest (sort(keys(%$_perfs))) {

    # print the design tag and the name
    print XMLOUT "<design>\n";
    print XMLOUT "  <name>$iTest</name>\n";

    # descend into the test
    foreach my $iKey (sort(keys(%{$_perfs->{$iTest}}))) {

      # if this hash elem is an array, then need to print each elem
      if(ref($_perfs->{$iTest}{$iKey}) eq "ARRAY") {
        foreach my $elem (@{$_perfs->{$iTest}{$iKey}}) {
          if(ref($elem) eq "HASH") {
            print XMLOUT "  <$iKey>\n";
            foreach my $jKey (keys(%$elem)) {
	      my $jElem = $elem->{$jKey};
              print XMLOUT "    <$jKey>$jElem</$jKey>\n";
            }
            print XMLOUT "  </$iKey>\n";
          } else {
            print XMLOUT "  <$iKey>$elem</$iKey>\n";
          }
        }

      # if it's a scalar, just print it
      } else {
        print XMLOUT "  <$iKey>".$_perfs->{$iTest}{$iKey}."</$iKey>\n";
      }
    }

    # end tags
    print XMLOUT "</design>\n";
  }
  print XMLOUT "</perflist>\n";

  close(XMLOUT);
  return 1;

}

# END
1;
