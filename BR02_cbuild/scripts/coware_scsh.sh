#!/bin/bash

# Make sure COWAREVERSION is set the scsh dies if a env var is not set
if [ "$COWAREVERSION" == "" ]; then
    # Upgrade to latest version - RBS
    export COWAREVERSION="G-2012.06-SP1"
fi

# New versions have same directory structure
# if starts with V, old style, otherwise assume new style, in this case starts with F-
if [[ $COWAREVERSION == V* ]]; then
  source /tools/linux/CoWare/$COWAREVERSION/PAMD/linux/setup.sh -pa 
else
  source /tools/linux/CoWare/$COWAREVERSION/SLS/linux/setup.sh -pa 
fi;

# Create a name of a temporary log file
logFile="test.`date +%Y_%B_%d_%H_%M_%s`.log"

# Run the test in a loop up to 25 times waiting for a license.
# Since coware tests compete with each other for limited licenses,
# the maxsimum wait after all iterations has been upped from 5 
# minutes to 16 minutes.
declare -i count=25
testDone=0
status=0
while [ $testDone -eq 0 -a $count -gt 0 ]; do
    # Run the test
    scsh $* >& $logFile
    status=$?
    grep "License Failure" $logFile >& /dev/null
    licFailure=$?
    if [ $status -eq 0 -o $licFailure -ne 0 ]; then
        testDone=1
    elif [ $licFailure -eq 0 ]; then
        # We had a license failure. Wait for a bit for the license to become available
        sleep 40
    fi

    # go to the next run
    count=$count-1
done

# Dump the results
cat $logFile
exit $status
