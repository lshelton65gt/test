#=====================================================================
# CarbonLinksMgr class.
# Original author dallaby@carbondesignsystems.com
#
#---------------------------------------------------------------------
# This class is utilized by the carbon_config routine to eval links
# and make contraints concerning the creation of links in a sandbox.
#
# Constraints:
#-------------
# CarbonLinksMgr::Symlink() will not remove an existing file in favor 
# of creating a link.   If you're asking for a link, and it finds a 
# file or directory, you will get a message to that effect.  
# The link will not be made.
#
# It will only remove a link if it IS a link, and it is pointing to 
# somewhere else other than the user is specifying.  At that time 
# you will get a message that it's getting pointed to somewhere else.
#
# A link which is pointed outside of the Linux.product type paths, 
# must be pointed at something that exists.  This is a release 
# requirement.  
#
#---------------------------------------------------------------------
package CarbonLinksMgr;

use strict;
use Cwd;
use File::Path;
use File::Basename;
# Might be called from carbon_config, when $CARBON_HOME isn't set up
# yet, but $CARBON_SANDBOX is set
if (exists $ENV{"CARBON_SANDBOX"}) {
  use lib "$ENV{CARBON_SANDBOX}/scripts/perl_modules";
} else {
  use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
}
use PidInfo;
use PidSetMgr;
use CDSSystem;

#-----------------------------------------
#!! Constructor
#--------------------------------------------
sub new #(CarbonConfigArgs, CDSFileNames)
{
    my $this = {};
    bless $this,@_[0];
    $this->{args}        = @_[1];
    $this->{fileNames}   = $this->{args}->CdsFileNames();
    $this->{linkCount}   = 0;
    $this->{symlinkFail} = 0;

    $this->Initialize();

    return $this;
}

#-----------------------------------------
#!! Initializer
#-----------------------------------------
sub Initialize
{
    my $this = shift;
    my $arch = $this->{args}->Platform();

    my %visitedDirs      = ();
    $this->{visitedDirs} = \%visitedDirs;

    my %links            = ();
    $this->{link}        = \%links;

}

#-----------------------------------------
#!! Returns a string which is used as the
#!! string filter for file targets in link
#!! requests.  A file target must exist if
#!! it fails matching this string.
#-----------------------------------------
sub ObjectDirFilter
{
    my $this = shift;

    if (! $this->{objDirFilter})
    {
        $this->{objDirFilter} = "";
        $this->{objDirFilter} = join "", "(", $this->{fileNames}->CarbonHome(),
                              "|\.\.)\/obj\/", 
                              $this->{args}->Platform(),"\.(";

        foreach (keys %{$this->{args}->ValidCarbonBins()})
        {
            $this->{objDirFilter} .= "$_|";
        }
        
        $this->{objDirFilter} =~ s/\|$/\)/;
    }
    
    return $this->{objDirFilter};
}

#---------------------------------------
# Tracks were we go when making links.
#---------------------------------------
sub VisitedPlaces
{
    my $this = shift;
    return $this->{visitedDirs};
}

sub MapTreeLinks
{
    my $this    = shift;
    my $srcDir  = shift;
    my $linkDir = shift;
    my @files   = CDSSystem::OpenDir($srcDir);
    
    foreach my $file (@files)
    {
	if ((-d "$srcDir/$file" && -l "$srcDir/$file") ||
	    -f "$srcDir/$file")
	{
	    $this->{tmpLinkList}{"$linkDir/$file"} = $file;
	}
	elsif (-d "$srcDir/$file" && ! -l "$srcDir/$file")
	{
	    $this->MapTreeLinks("$srcDir/$file");
	}
    }
}

#------------------------------------------------
#!! Counter for how many links we make.  Reset
#!! is performed by the entity that is requesting
#!! use of the Symlink() method in this class
#------------------------------------------------
sub LinkCount
{
    my $this = shift;
    return $this->{linkCount};
}

#-----------------------------------------------
#!! Counts any failed symlink attempts.
#-----------------------------------------------
sub LinkErrors
{
    my $this = shift;
    return $this->{symlinkFail};
}

#----------------------------------------------
#!! Resets both counters, link count and error
#!! count. Run from calling entity.
#----------------------------------------------
sub ResetCounter
{
    my $this             = shift;
    $this->{symlinkFail} = 0;
    $this->{linkCount}   = 0;
}

#-----------------------------------------------
#!! Recursive method to create a shadow directory
#!! of links from a real directory of files.
#-----------------------------------------------
sub CreateTreeLinksFromSourceDir
{
    my $this      = shift;
    my $srcDir    = shift;
    my $linkDir   = shift;
    my $objFilter = $this->ObjectDirFilter();
    my @files     = CDSSystem::OpenDir($srcDir);
    my $pidMgr    = new PidSetMgr();

    if (! -d $linkDir)
    {
	mkpath ($linkDir);
    }

    CDSSystem::Cd ("$linkDir");
    
    foreach my $file (@files)
    {
        my $target = "$srcDir/$file";
        my $link   = "$linkDir/$file";

	if ((-f $target || -l $target) && ! -d $target)
	{
	    $this->Symlink ("$srcDir/$file", "$linkDir/$file");
	}
	elsif (-d $target)
	{
	    mkpath ("$linkDir/$file");
            
            my $pid = CDSSystem::ForkProcess (\&CarbonLinksMgr::CreateTreeLinksFromSourceDir, 1, $this, 
                                    "$srcDir/$file", "$linkDir/$file");

            if ($pid > 0)
            {
                $pidMgr->AddPids($pid);
            }
        }
        else
        {
            print STDERR "Don't know what to do with this -> ",
            "link is $linkDir/$file -> $target\n";
        }
    }

    while ($pidMgr->NumActive() > 0)
    {
        sleep 1;
    }
}

sub RelativePath
{
    my $this = shift;
    my $target = shift;
    my $sandbox = $this->{fileNames}->FindSandboxPath();

    if ($target !~ /$sandbox/)
    {
        $target = $this->{fileNames}->RelativePath($target);
    }

    return $target;
}

#---------------------------------------------------------
#!! Main symlink routine used by everything. This adjusts
#!! paths to relative paths if it is possible.  Employs the
#!! constraints mentioned in the description at the top of
#!! this file in the description.
#---------------------------------------------------------
sub Symlink
{
    my $this         = @_[0];
    my $target       = $this->RelativePath(@_[1]);
    my $link         = @_[2];
    my $here         = cwd();
    my $rtn          = 0;
    my $objDirFilter = $this->ObjectDirFilter();

# A link that doesn't point to anything does not pass the -e test.
# We also have to do a readlink() which is true if the link is 
# pointing to somewhere AND to nowhere, which we do, but probably
# should try to avoid.

    my $readl = readlink($link);
    my $displayLink = "";

    if ($link =~ /^\//)
    {
        $displayLink = $link;
    }
    else
    {
        $displayLink = "$here/$link";
    }

    if ($this->{args}->Verbose())
    {
        print "Link $displayLink readlink is $readl target is $target\n";
    }

    if (-l "$link" && $readl eq "$target")
    {
        $rtn = 1;
      
        if ($this->{args}->Verbose())
        {
            print "Link $displayLink already pointing at ",
            "$target readl is $readl\n";
        }

        $this->{linkCount}++;
    }
    else
    {
        if (-l $link)
        {
            print "WARNING: Removing link $displayLink, pointing to $readl, ",
            "not $target\n";

            unlink "$link";
        }
        elsif (-f $link || -d $link)
        {
            print STDERR "WARNING: $here/$link is a file or directory.\n",
            "Cannot overwrite with a link $here/$link pointing to $target.\n",
            "You have to remove $here/$link by hand if you want the link made.\n";
            $rtn = 1;
            $this->{linkCount}++;
        }

        if (! -e $link && ( -e $target || 
            (! -e $target && $target =~ /$objDirFilter/)))
        {
            $! = 0;
            $rtn = symlink ($target, $link);
            
            if ($this->{args}->Verbose())
            {
                print "Symlink $link -> $target [$rtn == 1] $! \n";
            }
        }
        else
        {
            if ($this->{args}->Verbose())
            {
                print "DEBUG: Never made an attempt at $link -> $target\n";
            }
        }
        if ($rtn > 0)
        {
            $this->{visitedDirs}{"$here"} = 1;
            $this->{linkCount}++;
        }    
        else
        {
            $this->{symlinkFail} = 1;
            my $pw = cwd();
            
            print STDERR "Filter is ", $objDirFilter,"\n\n",
            "Symlink $link -> $target failed: $!  \n",
            " Current working directy at the time of the attempt was $pw.\n\n",
            "We do not allow non-existent targets outside of the build directory\n",
            "Target file is showing as ->$target\n";
            select (STDOUT);
        }
    }

    return $rtn;
}

#-----------------------------------------------
#!! Passed a source tree and a secondary location
#!! for a shadow directory, this will use the
#!! CreateTreeLinksFromSourceDir() method to create
#!! a duplicate tree of file links.
#-----------------------------------------------
sub SymlinkTree
{
    my $this           = shift;
    my $sourceTree     = shift;
    my $linkTree       = shift;
    my $printOnly      = shift;
    $this->{linkCount} = 0;
    my $rtn = 0;
    my $pidMgr = new PidSetMgr();

    if (! $sourceTree)
    {
	print STDERR "Cannot build tree, no source directory given\n";
    }
    elsif (! $linkTree)
    {
	print STDERR "Cannot build tree, no destination link ",
	"directory given\n";
    }
    else
    {
	print "Building Tree from $sourceTree, in $linkTree\n";
	
	if ($printOnly)
	{
	    my %linkList = ();
	    $this->{tmpLinkList} = \%linkList;
	    
	    $this->MapTreeLinks($sourceTree, $linkTree);
	}
	else
	{
            my $pid = CDSSystem::ForkProcess (\&CarbonLinksMgr::CreateTreeLinksFromSourceDir, 1, $this, $sourceTree, $linkTree);

            $pidMgr->AddPids($pid);
              
            if ($this->{args}->Verbose() && ! $printOnly)
            {
                print "SymlinkTree(): Created ", $this->{linkCount}, " links at $linkTree\n";
            }

            $rtn++;
	}
    }

    while ($pidMgr->NumActive() > 0)
    {
        sleep 1;
    }

    return $rtn;
}

1;
