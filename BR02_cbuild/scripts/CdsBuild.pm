#!perl

#  contains subroutines to create a new sandbox,
#  configure, build, and test its build 
#  


package CdsBuild;
use strict;
use File::Find;
use Cwd ; 

# requires CdsUtils also be available
use lib "$ENV{CARBON_HOME}/scripts";
use CdsUtil;


###
### Msandbox_now: make and populate a sandbox
###
### usage "Msandbox("dir") 
###
###  returns 1 if fails
###          0 if woiks
###
###  always leaves: cvs.log
###
sub Msandbox_now { 
   
   my $s_dir = $_[0] ;

   if (-e $s_dir) { 
      print STDERR "Error: $s_dir exists already\n" ; 
      return 1 ; 
   }
   else {
      ## rip thru it here.. 
      &CdsUtil::MkdirP ("$s_dir") or die "ERROR: Can't create $s_dir\n";

      if (! chdir "$s_dir" ) { die "Error: Can't cd to $s_dir\n" ; } 
      else { 
        `cvs co cbuild >> cvs_checkouts.log 2>&1` 
      }

   return 0 ; 

   }
   return 1 ;  # should never get here 

}


### 
### Scarbon_home sets CARBON_HOME and related
###  variables in the context of the running 
###  script, not the shell calling it
###
### usage: Scarbon_home("directory", "bin", "arch") ;
### returns: 0 if "directory" exists and assumes the 'sets' work
###          1 if the directory is non-existent
###
sub Scarbon_homeP {
  
   my $carbDir  = $_[0] ; 
   my $carbProd = $_[1] ; 
   my $carbArch = $_[2] ; 

   # defaults:
   if ($carbProd eq "") { $carbProd = "product" ; }
   if ($carbArch eq "") { 
     $carbArch = `uname -s` ; 
     chomp $carbArch;
   }


   # just return 1, let the caller decide the error level 
   # or action (caller may want to go make the home, if the
   # dir is non-existent 
   if ( ! -d $carbDir ) { return 1 ; }
   else { 
      $ENV{CARBON_HOME} = $carbDir ; 
      $ENV{PATH}="$carbDir/bin:$carbDir/scripts:$ENV{PATH}" ; 
      $ENV{CARBON_BIN}="$carbProd" ;
      $ENV{CARBON_TARGET}="$carbArch.$carbProd" ;

      return 0 ; 
   }

   return 1 ; # should never execute
}


###
### Rcarbon_config wraps 'carbon_config'
###
### usage: Rcarbon_config("list of arguments") ; 
### returns: 0 on success
###          1 on failure
###
### always leaves a carbon_config.log in "."
###
### assumes: carbon_config is in the path 
###

sub Rcarbon_config {

   my $c_args = shift (@_) ;  # put all args in c_args
   
   system ("carbon_config $c_args >& carbon_config.log") == 0 or return 1;
  
   return 0 ; 

}

###
### Rmkcds wraps 'mkcds'
###
### usage: Rmkcds("list of arguments") ; 
### returns: 0 on success
###          1 on failure
###
### always leaves a mkcds_results.log in "."
###
### assumes: mkcds is in the path 
###

sub Rmkcds {

   my $c_args = shift (@_) ;  # put all args in c_args
   
   system ("mkcds $c_args >& mkcds_results.log") == 0 or return 1;
  
   return 0 ; 

}


1;
