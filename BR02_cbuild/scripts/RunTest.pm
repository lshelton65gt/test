#!perl -w

# RunTest.pm - Perl module that knows how to use VMware VM instances to run
# carbon testcases on Windows.
### TODO: accept input redir from STDIN

package RunTest;
use strict;

# NOTE: To get a usage line for all routines in this package, grep for '^###'

use Carp;
use Data::Dumper;
use FileHandle;
use File::Basename;
use File::Copy;

# carbon perl libs
use CdsVM;
use CdsUtil;

$| = 1;
my $gDebug = 0;

### \$RunTest = new RunTest($logFile)
# initialize VM and logfile
sub new {
  my $class = shift;
  my $logFile = shift;
  my $self = {};

  # will use pid and counter to create batch files
  $self->{_cmdCounter} = 0;
  $self->{_pid} = $$;
  if ($logFile) {
    $self->{_logfile} = $logFile;
  } else {
    $self->{_logfile} = "runtest.".$self->{_pid};
  }

  # go ahead and bless so we can return on error
  bless $self, $class;

  $self->{_logFH} = new FileHandle(">".$self->{_logfile});
  unless(defined($self->{_logFH})) {
    $self->{_errStr} .= "Can't write to ".$self->{_logFH};
    return $self;
  }

  # initialize VMware. if this if first time called, will power on VM.
  # otherwise, will just login and authenticate so we can use $VM to do stuff
  my $VM = new CdsVM($self->{_logFH});
  if ($VM->getErrorNum()) {
    $self->{_errStr} .= "Can't start VM : ". 
               $VM->getErrorText();
    $self->log("ERROR: Could not start VM : ". 
               $VM->getErrorText());
  }
  $self->{_VM} = $VM;

  # get carbon_home in windows terms
  my $guestCarbonHome = $VM->convertToWinPath($ENV{CARBON_HOME});
  $self->{_WinCarbonHome} = $guestCarbonHome;
  # and maxsim_home
  my $guestMaxsimHome = $VM->convertToWinPath($ENV{MAXSIM_HOME});
  $self->{_WinMaxsimHome} = $guestMaxsimHome;
  my $guestMaxsimProtocols = $VM->convertToWinPath($ENV{MAXSIM_PROTOCOLS});
  $self->{_WinMaxsimProtocols} = $guestMaxsimProtocols;


  return $self;
}


### $status = runIndividualTest($command, \%envVars)
# run the given cmd in the VM, setting \%envVars in the environment before
# execution
sub runIndividualTest {
  my $self = shift;
  my $command = shift;
  my $envHashRef = shift;
  my $outputFile = "";
  my $echoToStdout = 0;
  # increment counter
  $self->{_cmdCounter} += 1;
  my $tmpId = $self->{_pid}."-".$self->{_cmdCounter};
  my $VM = $self->{_VM};

  # first thing to do is to get the name of whatever file we are redirecting
  # to. 
  $command =~ s# 2>&1##;
  my @cmdArgs = split(' ', $command);
  # make sure we are redirecting
  if ($cmdArgs[-2] =~ /^>/) {
    # yes, so get output file
    $outputFile = pop(@cmdArgs);
    # and trash redir characters
    pop(@cmdArgs);
    # and if it's /dev/null, give it a tmp name
    if ($outputFile eq '/dev/null') {
      $outputFile = "runtest$tmpId.out";
    }
  } else {
    $outputFile = "runtest$tmpId.out";
    # set flag so we know not redirecting and should echo to stdout
    $echoToStdout = 1;
  }

  # get current directory in windows terms
  my $guestCwd = $VM->convertToWinPath(CdsUtil::getSafeCwd());
  # and carbon_home and maxsim_home
  my $guestCarbonHome = $self->{_WinCarbonHome};
  my $guestMaxsimHome = $self->{_WinMaxsimHome};
  my $guestMaxsimProtocols = $self->{_WinMaxsimProtocols};

  if ($gDebug) {
    $self->log("Win Cwd: $guestCwd\n");
    $self->log("Win CH: $guestCarbonHome\n");
  }

  # split cwd into driveletter and relative path so we can go there using DOS's
  # retarded chdir behavior
  my ($guestDrive, $guestRelPath) = split(/:/, $guestCwd);
  if ($guestDrive) {
    # add a colon
    $guestDrive .= ":";
    # get rid of leading \
    $guestRelPath =~ s#^\\##;
  }

  # this is basic stuff that will always need to be executed on windows before
  # a cmd is run to setup the environment
###set LM_LICENSE_FILE=\\\\silicon\\o\\tools\\license\\carbond\\carbon.lic;\\\\silicon\\o\\tools\\license\\licenses
  my $winBatHeader =<<EndOfBatHeader;
set FLEXLM_BATCH=1
set LM_LICENSE_FILE=7276\@silicon;7275\@silicon
set CARBON_HOME=$guestCarbonHome
set MAXSIM_HOME=$guestMaxsimHome
set MAXSIM_PROTOCOLS=$guestMaxsimProtocols
set VS71COMNTOOLS=c:\\Program Files\\Microsoft Visual Studio .NET 2003\\Common7\\Tools\\
set INCLUDE=c:\\Program Files\\Microsoft Visual Studio .NET 2003\\Vc7\\Include\\;c:\\Program Files\\Microsoft Visual Studio .NET 2003\\Vc7\\PlatformSDK\\Include\\
set LIB=c:\\Program Files\\Microsoft Visual Studio .NET 2003\\Vc7\\Lib\\;c:\\Program Files\\Microsoft Visual Studio .NET 2003\\Vc7\\PlatformSDK\\Lib\\
rem cygwin/bin is in front of CARBON_HOME/Win/bin because Cygwin's make
rem can follow symlinks to find visual_cxx, but Carbon's can't.
set PATH=c:\\Program Files\\Microsoft Visual Studio .NET 2003\\Common7\\IDE;c:\\Program Files\\Microsoft Visual Studio .NET 2003\\Common7\\Tools;c:\\Program Files\\Microsoft Visual Studio .NET 2003\\Common7\\Tools\\Bin;c:\\Program Files\\Microsoft Visual Studio .NET 2003\\VC7\\BIN;c:\\Program Files\\Microsoft Visual Studio .NET 2003\\SDK\\v1.1\\bin;c:\\cygwin\\bin;$guestCarbonHome\\Win\\lib;$guestCarbonHome\\Win\\bin;$guestCarbonHome\\scripts;$guestCarbonHome\\bin;$guestCarbonHome\\Win\\lib\\winx\\shared;$guestMaxsimHome\\Bin\\Release;$guestMaxsimProtocols\\AHBv2\\xactors\\lib;$guestMaxsimProtocols\\AXIv2\\xactors\\lib;$guestMaxsimProtocols\\Common\\lib;\%PATH\%;.
$guestDrive
cd $guestRelPath

EndOfBatHeader

  # open a batch file that we will use to run the cmd on windows
  open(BATCHOUT, ">run$tmpId.bat") or die "Can't create run$tmpId.bat\n";
  print BATCHOUT $winBatHeader;

  # now set any env vars we were passed on cmd line
  foreach my $envVar (keys(%$envHashRef)) {
    # If the value of the envVar starts with a / it's likely a path and should be windowsified
    $envHashRef->{$envVar} = $VM->convertToWinPath($envHashRef->{$envVar}) if( $envHashRef->{$envVar} =~ /^\//);
    print "setting $envVar to $envHashRef->{$envVar}\n" if ($gDebug);   
    print BATCHOUT "set $envVar=$envHashRef->{$envVar}\n";
  }

  my $tempOutputFile = "run$tmpId.out";
  my $prog = $cmdArgs[0];
  ### TODO: redir input from stdin
  my $redirInput = "";

  # now we want to behave slightly differently based on what is being run
  ##
  ## is it carbon_exec?
  if (basename($prog) eq 'carbon_exec') {

    # dont need carbon_exec word anymore
    my $carbonExec = shift(@cmdArgs);
    # get name of executable carbon_exec is trying to run, and win-path-ize it
    my $testExec = $VM->slashToBackslash(shift(@cmdArgs));
    # and put .exe at the end if not already there
    unless($testExec =~ /\.exe$/) {
      $testExec .= ".exe";
    }
    # print the line to execute to the bat file
    print BATCHOUT $guestCarbonHome."\\obj\\Win.product\\".$testExec." ".
                   join(" ", @cmdArgs) . " $redirInput > $outputFile 2>&1\n";
    close(BATCHOUT);

  ##
  ## is it carbon?
  } elsif (basename($prog) =~ /^carbon(\.bat)?$/) {

    # trash the first arg, we're gonna hardcode carbon.bat
    shift(@cmdArgs);
    print BATCHOUT "call $guestCarbonHome\\bin\\carbon.bat ".
                   join(" ", @cmdArgs). " $redirInput > $outputFile 2>&1\n";

  ##
  ## is it nmake or make?
  } elsif ((basename($prog) eq 'nmake') or (basename($prog) eq 'make')) {

    my $makeExe = shift(@cmdArgs);
    print BATCHOUT "$makeExe ". join(" ", @cmdArgs). " $redirInput > ".
                   $outputFile . " 2>&1\n";

  ##
  ## wizard, should be in path, so  just do it
  } elsif (basename($prog) eq 'wizard') {

    my $firstArg = shift(@cmdArgs);
    print BATCHOUT "call wizard.bat ". join (" ", @cmdArgs). " $redirInput ".
                   " > $outputFile 2>&1\n";

  ##
  ## devenv is a Visual Studio Command, should be in path, so  just do it
  } elsif (basename($prog) eq 'devenv') {

    my $firstArg = shift(@cmdArgs);
    print BATCHOUT "devenv ". join (" ", @cmdArgs). " $redirInput ".
                   " > $outputFile 2>&1\n";

  ##
  ## is it a bat file?
  } elsif (basename($prog) =~ /\.bat$/) {
    my $firstArg = $VM->convertToWinPath(shift(@cmdArgs));
    print BATCHOUT "call $firstArg ".join(" ", @cmdArgs)." $redirInput ".
		   " > $outputFile 2>&1\n";

  ##
  ## must be a design.exe or some other win executable - just run it
  } else {

    # backslash first arg, in case it is a path to an exec or script
    my $firstArgLinux = shift(@cmdArgs);
    my $firstArg = $VM->convertToWinPath($firstArgLinux);

    # make sure whatever we're running ends in exe
    unless($firstArgLinux =~ /\.exe$/) {
      # if not, make a copy of it that does and use the copy
      copy($firstArgLinux, "$firstArgLinux.exe");
      $firstArg .= ".exe";
    }

    # add a line to attrib the iodb, in hopes we will not get an FS-stale one
    print BATCHOUT "attrib $firstArg\n";
    # assume default header is enough, so just print cmd line
    print BATCHOUT $firstArg . " " . join(" ", @cmdArgs);
    # then print a redir to an output file
    print BATCHOUT " $redirInput > $outputFile 2>&1\n";
  }

  # close the bat file
  close (BATCHOUT);
  # change file permissions so everyone can execute batch, and read/write the directory since will be executed by wintest
  system('chmod a+x '."run$tmpId.bat");
  system('chmod a+rwx '.CdsUtil::getSafeCwd());

  # execute the bat file
  my $status = $VM->runCommand("$guestCwd\\run$tmpId.bat > $guestCwd\\run".$tmpId."bat.out 2>&1");
  ###my $status = $VM->runCommand("$guestCwd\\run$tmpId.bat > c:\\tmpexec\\run".$tmpId."bat.out 2>&1");

  # if need to echo output to stdout, do it here
  if ($echoToStdout) {
    # must've worked, cat the output to the saved stdout and exit with
    # its status
    if(open(OUTPUT, $outputFile)) {
      # don't handle error - might not be any output
      while(<OUTPUT>) { print $_; }
      close(OUTPUT);
    }
  }
  # check for a VM error
  if ($VM->getErrorNum()) {
    $self->log("ERROR: VM error '". $VM->getErrorText(). "'\n");
    return 1;
  }

  # Check that outfile exists, or it is an indication that something is terrible wrong.
  unless (-e CdsUtil::getSafeCwd()."/run".$tmpId."bat.out") {
    $self->log("ERROR: No output file (run".$tmpId."bat.out) generated by command on VM. May be an indication of broken mounts.\n");
    die("Temp broken mount breakpoint\n"); # BSS
    return 37; # Arbitrary number, but maybe somthing you could recognize
  }

  ###print "Returning $status from runIndividualTest in RunTest.pm\n";
  return $status;
}

### $string = getErrorText()
sub getErrorText {
  my $self = shift;
  return $self->{'_errStr'};
}

### $string = getWinCarbonHome()
sub getWinCarbonHome {
  my $self = shift;
  return $self->{_WinCarbonHome};
}

### $string = getWinCarbonHome()
sub getWinMaxsimHome {
  my $self = shift;
  return $self->{_WinMaxsimHome};
}

### $string = getWinCarbonProtocols()
sub getWinMaxsimProtocols {
  my $self = shift;
  return $self->{_WinMaxsimProtocols};
}

### log($msg)
sub log {
  my $self = shift;
  my $msg = shift;
  my $logFH = $self->{_logFH};
  print $logFH "$msg\n";
}

# END
1;


# make sure that, if input was sent to us, we put it in a file and that
# it makes it to the windows execution.
###my $redirInput = "";
###if ( -s STDIN ) {
  ###$redirInput = "test.input.$$";
  ###if ($gDebug) { print "echo found input at stdin, will pass it over as test.input.$$\n"; }
  ###open(REDIRIN, ">test.input.$$") or die "Can't create test.input.$$\n";
  ###while (<STDIN>) {
    ###print REDIRIN $_;
  ###}
  ###close(REDIRIN);
  ###$redirInput = " < $redirInput ";
###}

