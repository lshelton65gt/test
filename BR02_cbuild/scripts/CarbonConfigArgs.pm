#=================================================================================
# CarbonConfigArgs class
#---------------------------------------------------------------------------------
# Original Author Dave Allaby. dallaby@carbondesignsystems.com
#
#!! Description:  This class deals with the argument portion of the carbon_config
#!! script.  It holds the data, has members for interface with calling scripts, and
#!! contains the usage/help output.
#
#----------------------------------------------------------------------------------
package CarbonConfigArgs;

use strict;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSSystem;
use CDSFileNames;

#--------------------------------------------------------------------
# USAGE hash.
#--------------------------------------------------------------------
my %usage = ();

$usage{debug}     = "Configure only a debug tree";
$usage{clean}     = "Remove all objects from the build directory, and remake".
                    " links.\n"; 
$usage{cov}       = "Configure only a coverage tree";
$usage{gcc}       = "<ver> Override the gcc compiler to specified version.";
$usage{genscript} = "<script_name>   Generate a shell-script to run ".
                    "programs with CARBON_HOME, PATH, and LD_LIBRARY_PATH ".
                    "set properly";
$usage{gprof}     = "Configure only a gprof tree";
$usage{help}      = "Print this help message";
$usage{icc}       = "<ver> Use Intel compiler version e.g. 8.0";
$usage{keepobjs}  = "Reconfigure but keep any existing object files";
$usage{m32}       = "Configure a Linux (32-bit) tree";
$usage{m64}       = "Configure a Linux64 tree";
$usage{noincredibuildgcc}      = "Do not use Incredibuild for GCC steps on Windows";
$usage{osver}     = "OS version.";
$usage{mkcds}     = "Launch mkcds script immediately following a successful ".
                    " tree configuration, passing any relavent arguments to ".
                    " mkcds script";
$usage{product}   = "Configure only a product tree";
$usage{showkey}   = "<key> Make any link commands associated with the LinkGroup ".
                   "\$key verbose.  Debugging";
$usage{spw}       = "<ver> Use Sun Sparcworks (aka forte) version e.g. 8.0";
                    "script.";
$usage{winx}      = "Configure only a windows cross-compiler tree";

#============================================================
#!! Constructor.
#!!  
#============================================================
sub new  #(CDSFileNames)
{
    my $this = {};
    bless $this, @_[0];
    $this->{fileNames} = @_[1];

    $this->{usage} = \%usage;
    
    $this->ProcessArgs();
    $this->ReadCompilerVersion();

    $this->{carbonArch}   = $this->{fileNames}->CarbonArch();
    $this->{carbonTarget} = $this->{fileNames}->CarbonTarget();
    $this->{osVersion}    = $this->{fileNames}->OsVersion();

    return $this;
}

#============================================================
#!! Process ARGV with Getopts::Long.
#============================================================
sub ProcessArgs #()
{
    my $this                    = shift;

# Save what we were passed as arguements.
#-----------------------------------------
    my @args                    = @ARGV;
    $this->{scriptArgs}         = \@args;

# Initialize all possible options
#---------------------------------
    my %options                 = ();
    $this->{options}            = \%options;
    $this->{options}{clean}     = 0;
    $this->{options}{cov}       = 0;
    $this->{options}{debug}     = 0;
    $this->{options}{gcc}       = "";
    $this->{options}{genscript} = "";
    $this->{options}{gprof}     = 0;
    $this->{options}{help}      = 0;
    $this->{options}{icc}       = "";
    $this->{options}{keepobjs}  = 0;    
    $this->{options}{m32}       = 0;
    $this->{options}{m64}       = 0;
    $this->{options}{mkcds}     = 0;
    $this->{mkcds}{nf}          = 0;
    $this->{mkcds}{nogrid}      = 0;
    $this->{options}{noincredibuildgcc}      = 0;
    $this->{options}{osver}     = "";
    $this->{options}{product}   = 0;
    $this->{options}{showkey}   = "";
    $this->{options}{spw}       = "";
    $this->{options}{sunos}     = 0;
    $this->{options}{verbose}   = 0;
    $this->{options}{winx}      = 0;
    
    use Getopt::Long;

    GetOptions ('clean'        => \$this->{options}{clean},
		'cov'          => \$this->{options}{cov},
		'debug'        => \$this->{options}{debug},
		'gcc=s'        => \$this->{options}{gcc},
		'genscript=s'  => \$this->{options}{genscript},
		'gprof'        => \$this->{options}{gprof},
		'help'         => \$this->{options}{help},
		'usage'        => \$this->{options}{help},
		'icc=s'        => \$this->{options}{icc},
		'keepobjs'     => \$this->{options}{keepobjs},
		'm32'          => \$this->{options}{m32},
		'm64'          => \$this->{options}{m64},
		'mkcds'        => \$this->{options}{mkcds},
		'nf'           => \$this->{mkcds}{nf},
		'nogrid'       => \$this->{mkcds}{nogrid},
                'noincredibuildgcc'      => \$this->{options}{noincredibuildgcc},
                'osver=s'      => \$this->{options}{osver},
		'product'      => \$this->{options}{product},
                'showkey=s'    => \$this->{options}{showkey},
		'spw=s'        => \$this->{options}{spw},
                'sunos'        => \$this->{options}{sunos},
		'verbose'      => \$this->{options}{verbose},
		'winx'         => \$this->{options}{winx});

    if ($this->{options}{help} == 1)
    {
	$this->PrintUsage();
    }
    elsif ($Getopt::Long::error)
    {
	$this->PrintUsage();
	die;
    }

    foreach (@ARGV)
    {
	if ($_ =~ /^\//)
	{
	    $ENV{CARBON_HOME} = $ARGV[0];
	}
	else
	{
	    $this->{mkcds}{$_} = $_;
	}
    }

    $this->RationalizeEnvironment();
}		 

#-----------------------------------------------------------
#!! Method tries to make sense of weird option combinations
#!! and provides some precendence and overrides.
#-----------------------------------------------------------
sub RationalizeEnvironment
{
    my $this = shift;

# If we're verbose, we're verbose for all, turn off showkey,
# not required.
#----------------------------------------------------------
    if ($this->Verbose())
    {
        $this->{options}{showkey} = "";
    }

    $this->{platform} = $this->Platform();

# Keep this last. We need to remake one after we set environment
# variables.

    $this->{fileNames} = new CDSFileNames();
}

#===========================================================
#!! Returns a key string.  Key string should match some 
#!! LinkGroup key string.  When selected the link commands for
#!! the links in the matching LinkGroup will be switched to
#!! verbose mode.
#===========================================================
sub ShowKey
{
    my $this = shift;

    return $this->{options}{showkey};
}

#============================================================
#!! Method returns a file name string.
#!! The file generated is a wrapper for executing commands
#!! in a CARBON_HOME from a remote machine.
#============================================================
sub GenerateScript  #()
{
    my $this = shift;
    
    return $this->{options}{genscript};
}

#====================================================
#!! Returns ref to command line arguments array
#====================================================
sub ScriptArgs
{
    my $this = shift;
    return $this->{scriptArgs};
}

#====================================================
#!! Returns true if user wants to run mkcds through
#!! carbon_config.
#====================================================
sub LaunchMkcds
{
    my $this = shift;
    return $this->{options}{mkcds};
}

#====================================================
#!! Returns string of any mkcds options that this
#!! class has provided functionality.  All options
#!! may not be available.
#====================================================
sub MkcdsArgs #()
{
    my $this = shift;
    my $mkcdsArgs = "";
    
    if ($this->LaunchMkcds())
    {
	foreach (keys %{$this->{mkcds}})
	{
            if ($this->{mkcds}{$_})
            {
                $mkcdsArgs .= " -$_";
            }

	}
    }
    
    return $mkcdsArgs;
}

#===================================================
#!! Returns a reference to the CDSFileNames object
#===================================================
sub CdsFileNames  #()
{
    my $this = shift;
    return $this->{fileNames};
}

#===================================================
#!! Adds verbose messsaging.
#===================================================
sub Verbose  #()
{
    my $this = shift;
    my $verbose = 0;

    if ($this->{options}{verbose} || $ENV{QA_DEBUG})
    {
        $verbose = 1;
    }

    return $verbose;
}

#===================================================
#!! Returns true if user selected to wipe out all
#!! the objects in the build directory.
#===================================================
sub RemoveObjects  #()
{
    my $this = shift;
    my $rmObj = 0;

    if ($this->{options}{clean})
    {
	$rmObj = 1;
    }
	
    return $rmObj;
}

#==================================================
#!! Possible CARBON_BIN variants. Returns reference
#!! to the hash list.
#==================================================
sub ValidCarbonBins #()
{
    my $this = shift;
    my %bins = ('gprof' => 1,
                'product' => 1,
                'cov' => 1,
                'debug' => 1);
    $this->{bins} = \%bins;

    return $this->{bins};
}

#==================================================
#!! Returns a hash reference to a list which is all
#!! the relevant build directories to set up for
#!! execution.
#==================================================
sub ExecutableDirs
{
    my $this = shift;
    my $bins = $this->ValidCarbonBins();

    if (! $this->{execDirs})
    {
	my %dirs = ();
	$this->{execDirs} = \%dirs;
	
	foreach my $bin (keys %{$bins})
	{
	    if ($this->{options}{$bin})
	    {
		print "Requested $bin\n";
		$this->{execDirs}{$bin} = join "/", 
		$this->{fileNames}->CarbonHome(),
		"obj", $this->Platform();

		$this->{execDirs}{$bin} .= "." . $bin;
	    }
	}
	
	if (values (%{$this->{execDirs}}) == 0)
	{
	    foreach (keys %{$bins})
	    {
		$this->{execDirs}{$_} = join "/", 
		$this->{fileNames}->CarbonHome(),
		"obj", $this->Platform();

		$this->{execDirs}{$_} .= "." . $_;
	    }
	}
    }
    
    return %{$this->{execDirs}};
}

#------------------------------------------------
#!! GccVersion is just the number version,
#!! without the "gcc-" string.
#------------------------------------------------
sub GccVersion
{
    my $this = shift;

    if (! $this->{gccVersion})
    {
	if ($this->Target() =~ /gcc(3|4)/)
	{
	    if ($this->{options}{gcc})
	    {
		$this->{gccVersion} = $this->{options}{gcc};
	    }
	    else
	    {
		$this->{gccVersion} = $this->{defaultGccVer};
	    }
	}
	elsif ($this->Target() eq "icc")
	{
	    $this->{gccVersion} = $this->{options}{icc};

	    if ($this->{options}{icc} =~ /^9\.0/)
	    {
		$this->{defaultGccVer} = "3.4.3";
	    }
	    else
	    {
		$this->{gccVersion} = "3.2";
	    }
	}
	elsif ($this->Target() eq "winx")
	{
	    if ($this->{gccVersion} !~ /\-mingw/)
	    {
		$this->{gccVersion} .= "-mingw";
	    }
	}
    }

    return $this->{gccVersion};
}

#------------------------------------------------
#!! Binutils version is always the default
#------------------------------------------------
sub BinutilsVersion
{
  my $this = shift;
  return $this->{defaultBinutilsVersion};
}

#-----------------------------------------------------------
#!! Allow something to override the machine that we are on
#!! so we can get any packing list regardless of operating
#!! system. The default is used by carbon_config usually,
#!! the packaging system would override this to get a list
#!! for whatever OS version it needs to package.
#-----------------------------------------------------------
sub OsVersion
{
    my $this = shift;
    
    if (! $this->{options}{osver})
    {
        $this->{options}{osver} = $this->{fileNames}->OsVersion();
    }

    $ENV{OS_VERSION} = $this->{options}{osver};

    return $this->{options}{osver};
}

#------------------------------------------------------------
#!! CompilerName is the gcc compiler name string
#------------------------------------------------------------
sub CompilerName #()
{
    my $this = shift;

    if (! $this->{compilerName})
    {
	if ($this->Target() =~ /gcc/)
	{
	    $this->{compilerName} = join "-", "gcc", $this->GccVersion();
	}
	elsif ($this->Target() eq "winx")
	{
	    $this->{compilerName} = join "-", "gcc", $this->GccVersion(),"mingw";
	}
	elsif ($this->Target() eq "icc")
	{
	    $this->{compilerName} = join "-", "icc", $this->{options}{icc};
	}
	elsif ($this->Target() eq "spw")
	{
	    $this->{compilerName} = join "", "spw", $this->{options}{spw};
	}
    }

    return $this->{compilerName};
}

#-----------------------------------------------------
# Return whether -winx was given on the command line
#-----------------------------------------------------
sub Winx
{
    my $this = shift;

    return $this->{options}{winx};
}

#-----------------------------------------------------
#!! Returns the targeted compiler version short name.
#-----------------------------------------------------
sub Target  #()
{
    my $this = shift;
    
    if (! $this->{target})
    {
	if ($this->{options}{winx})
	{
	    $this->{target} = "winx";
	}
	elsif ($this->{options}{spw})
	{
	    $this->{target} = "spw";
	}
	elsif ($this->{options}{icc})
	{
	    $this->{target} = "icc";
	}
	elsif ($this->{options}{gcc} =~ /^3\./)
	{
	    $this->{target} = "gcc3";
	}
	elsif ($this->{options}{gcc} =~ /^4\./)
	{
	    $this->{target} = "gcc4";
	}
	else
	{
            $this->{target} = "gcc4";
	}
    }

    return $this->{target};
}

#--------------------------------------------------------------
#!! Platform would usually be related to CARBON_TARGET_ARCH
#!! Returns string.
#--------------------------------------------------------------
sub Platform
{
    my $this = shift;

    if (! $this->{platform})
    {
        my $uname = $this->{fileNames}->CdsHosts()->HostUname();

# Set us to Linux64 if -m64 or CARBON_HOST_ARCH is set
#------------------------------------------------------
        if ($this->{options}{m64}) 
        {
            $ENV{CARBON_HOST_ARCH}   = "Linux64";
            $ENV{CARBON_TARGET_ARCH} = "Linux64";
            $this->{platform}        = "Linux64";;
        }
# If -m32, or CARBON_HOST_ARCH = Linux or SunOS
        elsif ($this->{options}{m32} == 1)
        {
            $ENV{CARBON_HOST_ARCH}   = $uname;
            $ENV{CARBON_TARGET_ARCH} = $uname;
            $this->{platform}        = $uname;
        }
# If Winx
        elsif ($this->{options}{winx} ||
               ($this->{fileNames}->CarbonTargetArch() eq "Win" &&
                $this->{fileNames}->CarbonHostArch() eq "Linux"))
        {
            $ENV{CARBON_HOST_ARCH}   = "Linux";
            $ENV{CARBON_TARGET_ARCH} = "Win";
            $this->{platform}        = "Linux";
        }
        elsif ($this->{options}{sunos})
        {
            $ENV{CARBON_HOST_ARCH}   = "SunOS";
            $ENV{CARBON_TARGET_ARCH} = "SunOS";
            $this->{platform}        = "SunOS";
        }
        elsif ($ENV{CARBON_TARGET_ARCH})
        {
            $ENV{CARBON_HOST_ARCH} = $ENV{CARBON_HOST_ARCH};
            $this->{platform}      = $ENV{CARBON_TARGET_ARCH};
        }
        else
        {
            $this->{platform} = $this->{fileNames}->GetFromCarbonArchScript();
        }

        if ($this->{platform} =~ /Linux/)
        {
            my $osVer = $this->OsVersion();
        }
    }

    return $this->{platform};
}

#---------------------------------------------------------------
#!! 
#---------------------------------------------------------------
sub ReadCompilerVersion
{
    my $this = shift;
    my $rh = CDSSystem::OpenRead($this->{fileNames}->CompilerVersionFile());

    if ($rh)
    {
	while (<$rh>)
	{
	    chomp;
	    my ($envVar, $value) = (split "=");
	    
	    if ($_ =~ /GCC_VERSION/)
	    {
		$this->{defaultGccVer}  = $value;
	    }
	    elsif ($_ =~ /BINUTILS_VERSION/)
	    {
		$this->{defaultBinutilsVersion}  = $value;
	    }
	    elsif ($_ =~ /GCC_MINGW/)
	    {
		$this->{defaultMinGcc}  = $value;
	    }
	}
	
	close ($rh);
    }
    else
    {
	die "Could not read compiler information from ", 
	$this->{fileNames}->CompilerVersionFile(), "\n";
    }

    return $this->GccVersion();
}

sub DefaultGccVersion
{
    my $this = shift;
    
    return $this->{defaultGccVer};
}

sub DumpArgSettings
{
    my $this = shift;

    print "Dumping arguments:\n";

    foreach (keys %{$this->{options}})
    {
	print "Option: $_  = ",$this->{options}{$_},"\n";
    }
}

sub NumOptions()
{
    my $this = shift;
    my $n = values(%{$this->{options}});

    return $n;
}

sub NumUsageMsgs
{
    my $this = shift;
    my $n = values(%{$this->{usage}});

    return $n;
}

sub PrintUsage
{
    my $this = shift;

    print STDERR "\n\nUSAGE: $0 <-options>\n\n";

    foreach (sort keys %{$this->{options}})
    {
	if (! $usage{$_})
	{
	    $usage{$_} = "No help description available for this option";
	}

	$this->PrintArgHelp ($_, $usage{$_});
    }

    exit (0);
}

sub PrintArgHelp
{
    my ($ref, $opt, $message) = (@_);
    my $optStr = "";


# If message has <foo_value> type string prepended, pull if off
# so that it can be display next to the option, rather than on
# the next line with the description.  
# Any option which takes a string as an argument
# should have this so it is distiguishable from a simple on/off
# switching argument.    

    if ($message =~ /^\<\S+\> /)
    {
	($optStr = $message) =~ s/\>.+$/\>/o;
	chomp ($optStr);
	$message =~ s/$optStr//;
    }

    print STDERR "\n\t-$opt:  $optStr\n";

    my @msg = split " ", $message;

    for (my $i = 0; $i <= $#msg; $i++)
    {
	my $m = "\t\t";
	my $maxLength = 50;
	my $lineLength = 0;

	while ($lineLength < $maxLength)
	{
	    my $tmpM = ($m .= $msg[$i]);
	    $lineLength = length($tmpM);

	    if ($lineLength < $maxLength)
	    {
		$m = "$tmpM ";
		$i++;
	    }
	}

	print STDERR "$m\n";
    }
}

1;
