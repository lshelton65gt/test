#!perl

package TestList;
use strict;

# NOTE: To get a usage line for all routines in this package, grep for '^###'

use Carp;

use Data::Dumper;

### \$TestListObj = new TestList ( [$filename] )
# TestList is a wrapper around an XML TestList file, typically stored at
# $CARBON_HOME/test/alltests.xml. Given a filename on initializaion, the
# returned object will have parsed the whole XML file and will have all
# information available via the routines below.
#
# if not given a filename, an empty TestList will be returned. Either way,
# routines are also provided to allow modification as well as access
sub new {
  my $class = shift;
  my $inputFile = shift;
  my $onlyTheseTests = shift;
  my $self = {};
  my $_usingChecker = 1;

  use XML::Checker::Parser;

  my $_doc;
  $self->{'_doc'} = \$_doc;
  $self->{'_usingChecker'} = $_usingChecker;
  my $_parser;
  $self->{'_parser'} = \$_parser;
  my %_tests;
  $self->{'_tests'} = \%_tests;
  my %_variantgroups;
  $self->{'_variantgroups'} = \%_variantgroups;
  my @_validArchs = ("SunOS", "Linux", "Win", "Linux64");
  $self->{'_validArchs'} = \@_validArchs;
  $self->{'_errStr'} = "";


  bless $self, $class;

  $self->_initialize($inputFile, $onlyTheseTests);

  return $self;
}

# _initialize ($inputFile)
# sets up xml parser, parses file, and builds a tree representing the whole
# test list file
sub _initialize 
{
    my $self = shift;
    my $inFile = shift;
    my $onlyTheseTests = shift;
    my @inTests = ();


    # make sure input file exists

    if ($inFile and not (-e $inFile)) 
    {
        die "$inFile not found: $!";
    }

    # if given a file, then we need to build the testlist tree
    
    if ($inFile) 
    {
	# were we also given a plaintext list?
	
	if ($onlyTheseTests) 
	{
	    # now read in the file
	    unless(open (TESTLISTIN, $onlyTheseTests))
	    {
		print STDERR "ERROR: Can't read testlist file at $onlyTheseTests\n";
		#return 0;
	    }
	    
	    my $lErrors = 0;
	    while(<TESTLISTIN>) 
	    {
		# next if comment or blank line
		next if (m/^\s*\#/);
		next if (m/^\s*$/);
		
		# assume that first elem is testname, each consecutive elem is a group
		my ($lTestName, @lOther) = split(/\s+/, $_);
		
		unless(push(@inTests, $lTestName)) 
		{
		    print STDERR "ERROR: problem adding $lTestName to TestList\n";
		    $lErrors++;
		}
	    }
	    
	    close(TESTLISTIN);
	    
	    # this seems like overkill, but it's what the XML parser does if there
	    # is an error, so we may as well do the same here...
	    
	    if ($lErrors > 0) 
	    { 
		die "Problems parsing test list file!\n"; 
	    }
	    
	    # or an XML file
	}

	# set up parser
	
	# have to eval these since the packages will not be imported at compile
	# time. (see eval statements in new() above)
	
	eval "\$self->{'_parser'} = new XML::Checker::Parser(Handlers => 
                            {Start => sub { return \$self->_XMLElemStart(\@_)},
                             Char =>  sub { return \$self->_XMLElemChar(\@_)},
                             End =>   sub { return \$self->_XMLElemEnd(\@_)}},
			     ParseParamEnt => 1, SkipExternalDTD => 1,
                             SkipInsignifWS => 1 ); 
	                     \$XML::Checker::FAIL = \\&_parseFail;";
    
      # parse it - this will die if document is not well-formed
	$self->{'_parser'}->parsefile($inFile);
    
	unless ($self->{_errStr} eq "") 
	{
	    die "ERRORS PARSING $inFile!\n".$self->{_errStr}."\n";
	}
	
	if(scalar(@inTests) > 0) 
	{
	    $self->pruneByList(\@inTests);
	    undef @inTests;
	} 
	elsif ($onlyTheseTests) 
	{
	    # was given an empty list, just prune everything
	    $self->{'_tests'} = {};
	}
    }
}

# error handler called by parser when dtd validation error is encountered
sub _parseFail {
  my $errMsg = "Error " . shift (@_) . ": ".join(" ", @_) . "\n";
  print STDERR "$errMsg\nParser failed\n";
  exit(1);
}

# called by xml parser when a start tag is encountered
sub _XMLElemStart {
  # shift 1st 3 elems. anything else is attrs/values and goes into a hash
  my $self = shift;
  my $expat = shift;
  my $elem = shift;
  my %attrs = @_;

  # first, set curElemName
  $self->{_curElemName} = $elem;
  
  # if entering a test, init a new tree
  if ($elem eq 'test') {
    $self->{_curTestHash} = ();
    $self->{_curTestName} = "";
    $self->{_inTestParse} = 1;
    $self->{_inVariantGroupParse} = 0;
    # go ahead and set up some arrays in the hash
    @{$self->{_curTestHash}{arch}} = ();
    @{$self->{_curTestHash}{group}} = ();
    @{$self->{_curTestHash}{dep}} = ();
    %{$self->{_curTestHash}{variant}} = ();
    %{$self->{_curTestHash}{variantgroup}} = ();
  }
  # if entering a variant group definition, init a new tree and set a tmp for the name
  if ($elem eq 'variantgroupdef') {
    # make sure name is not 'default' or 'all'
    if (($attrs{name} eq 'default') or ($attrs{name} eq 'all')) {
        $self->{_errStr} .= "ERROR: Variant groups may not be named 'default' or 'all'\n";
    }
    $self->{_inTestParse} = 0;
    $self->{_inVariantGroupParse} = 1;
    $self->{_curVariantGroupHash} = ();
    $self->{_curVariantGroupName} = $attrs{name};
    %{$self->{_curVariantGroupHash}{variant}} = ();
  }
  # if entering a variantgroup, set a tmp for the name
  if ($elem eq 'variantgroup')
  {
      $self->{_curVariantGroupName} = $attrs{name};
  }
  # if entering a variant, init a new tree and set a tmp for the name
  if ($elem eq 'variant') {
    # make sure name is not 'default' or 'all'
    if (($attrs{name} eq 'default') or ($attrs{name} eq 'all')) {
      $self->{_errStr} .= "ERROR: Variants may not be named 'default' or 'all'".
	" (encountered in ".$self->{_curTestName}.")\n";
    }
    $self->{_curVariantHash} = ();
    # DTD is already checking for missing attrs
    $self->{_curVariantName} = $attrs{name};
    # set value for default (will make sure only 1 default later)
    $self->{_curVariantHash}{default} = $attrs{default};
    # go ahead and set up some arrays in the hash
    @{$self->{_curVariantHash}{arch}} = ();
    @{$self->{_curVariantHash}{group}} = ();
  }

  # if entering an env, set tmp env name
  if ($elem eq 'env') {
    # DTD is already checking for missing attrs
    $self->{_curEnvName} = $attrs{name};
  }
}

# called by xml parser when element character data is encountered. may be
# called multiple times by 1 element's data
sub _XMLElemChar 
{
  my ($self, $expat, $charData) = @_;
  my $elem = $self->{_curElemName};

  # note that charData is not guaranteed to all arrive in 1 callback. thus
  # some of these conditions append to tmps and let the end callback handle
  # the data

  # set name as a tmp to be used to map the tmp hash when we're done
  if ($elem eq 'name') 
  {
      $self->{_curTestName} .= $charData;

      # for group/dep/arch/dir/match, just record the data and keep going
  } 
  elsif ($elem =~ /^(group|arch|dep|dir|size|waitfor|match|env)$/)
  {
      $self->{_curCharData} .= $charData;
  }
}

# called by xml parser when an end tag is encountered
sub _XMLElemEnd 
{
  my ($self, $expat, $elem) = @_;
  my $_tests = $self->{'_tests'};
  my $_variantgroups = $self->{'_variantgroups'};

  # if ending name, strip trailing / and make sure not a dup
  if ($elem eq 'name') 
  {
      $self->{_curTestName} =~ s#/$##;

      if (exists($_tests->{$self->{_curTestName}})) 
      {
	  $self->{_errStr} .= "ERROR: Found multiple tests named ".
	      $self->{_curTestName}."\n";
      }
  } 
  elsif ($elem eq 'dep') 
  {
      # if ending dep, append to list, reset curData

      push(@{$self->{_curTestHash}{dep}}, $self->{_curCharData});
      $self->{_curCharData} = "";
  }

  # if ending group/arch, decide variant or test, append to list, reset curData
  elsif ($elem =~/^(group|arch)$/)
  {
      if (exists($self->{_curVariantName})) 
      {
	  push(@{$self->{_curVariantHash}{$elem}}, $self->{_curCharData});
      } 
      else 
      {
	  push(@{$self->{_curTestHash}{$elem}}, $self->{_curCharData});
      }

      $self->{_curCharData} = "";
  } 
  elsif ($elem =~ /^(dir|match|waitfor)$/)
  {
    # check for dup
      if (exists($self->{_curTestHash}{$elem})) 
      {
	  $self->{_errStr} .= "ERROR: Duplicate element $elem encountered  for ".
	      $self->{_curTestName} . " during parsing\n";
      }
      $self->{_curTestHash}{$elem} = $self->{_curCharData};
      $self->{_curCharData} = "";

      # if ending size, check data, decide var/test, set value, reset curData
  } 
  elsif ($elem eq 'size') 
  {
      # now check some things about this arg to make sure qsub can handle it
      if ($self->{_curCharData} =~ m/\-l[^\s+]/)
      {
	  $self->{_errStr} .= "ERROR: Size arg for ".$self->{_curTestName}.
	      " is malformed. -l must be followed by a space\n";
      }
      if (($self->{_curCharData} =~ m/[\s+]=/) or
	  ($self->{_curCharData} =~ m/=[\s+]/)) 
      {
	  $self->{_errStr} .= "ERROR: Size arg for ".$self->{_curTestName}.
	      " is malformed. -l key=value must not have spaces between key ".
	      "and value\n";
      }

      if (exists($self->{_curVariantName})) 
      {
	  # check for dup
	  if (exists($self->{_curVariantHash}{size})) 
	  {
	      $self->{_errStr} .= "ERROR: Duplicate element size encountered for ".
		  $self->{_curTestName} . " variant during parsing\n";
	  }
	  $self->{_curVariantHash}{size} = $self->{_curCharData};
      } 
      else 
      {
	  # check for dup
	  if (exists($self->{_curTestHash}{size})) {
	      $self->{_errStr} .= "ERROR: Duplicate element size encountered for ".
		  $self->{_curTestName} . " during parsing\n";
	  }
	  $self->{_curTestHash}{size} = $self->{_curCharData};
      }
      $self->{_curCharData} = "";

  # if ending env, add a new value to this variant's env hash, clear tmps
  } 
  elsif ($elem eq 'env') 
  {
      if ($self->{_curVariantHash})
      {
	  $self->{_curVariantHash}{env}{$self->{_curEnvName}} = $self->{_curCharData};
      }
      else
      {
	  # We now allow normal non-variant tests to set environment variables
	  $self->{_curTestHash}{env}{$self->{_curEnvName}} = $self->{_curCharData};
      }

      $self->{_curCharData} = "";
      delete($self->{_curEnvName});
      
      # if ending variant, reference tmp hash, reset variant tmps
  } 
  elsif ($elem eq 'variant') 
  {
      if ($self->{_inTestParse} == 1) {
          $self->{_curTestHash}{variant}{$self->{_curVariantName}} =
              $self->{_curVariantHash};
      } else {
          $self->{_curVariantGroupHash}{variant}{$self->{_curVariantName}} =
              $self->{_curVariantHash};
      }

      $self->{_curVariantHash} = ();
      delete($self->{_curVariantName});

      # if ending test, reference tmp hash, reset test tmps
  } 
  # if ending variantgroup, add variants to test
  elsif ($elem eq 'variantgroup')
  {
      my $_variantgroups = $self->{'_variantgroups'};
      foreach my $variantname (keys(%{$_variantgroups->{$self->{_curVariantGroupName}}})) {
          $self->{_curTestHash}{variant}{$variantname} =
              $_variantgroups->{$self->{_curVariantGroupName}}{$variantname};
      }
      delete($self->{_curVariantGroupName});
  }
  elsif ($elem eq 'test') 
  {
      # first set any defaults.
      # dir elem defaults to the test name
      unless(exists($self->{_curTestHash}{dir})) {
	  $self->{_curTestHash}{dir} = $self->{_curTestName};
      }

    # if any variants, make sure only 1 has default=1
      if (scalar(keys(%{$self->{_curTestHash}{variant}})) > 0) {
	  # this returns all the defaults equal to 1. that is, if this returns
	  # more than 1 value, then there are multiple variants declared as default
	  # if it returns 0 values, then no variants have been declared default.
	  my @defaults = grep 
	  {
	      ($self->{_curTestHash}{variant}{$_}{default} eq '1')
	      } keys(%{$self->{_curTestHash}{variant}});
	  if (scalar(@defaults) > 1) 
	  {
	      $self->{_errStr} .= "ERROR: Multiple variants of test ".
		  $self->{_curTestName}." are declared as default\n";
	  } 
	  elsif (scalar(@defaults) < 1) 
	  {
	      $self->{_errStr} .= "ERROR: No variants of test ".
		  $self->{_curTestName}." are declared as default\n";
	  }
      }

      # assign the tmp hash to the main _tests map
      $_tests->{$self->{_curTestName}} = $self->{_curTestHash};
      $self->{_curTestHash} = ();
      delete($self->{_curTestName});
  }
  # if ending variantgroupdef, reference tmp hash, reset variantgroupdef tmps
  elsif ($elem eq 'variantgroupdef') 
  {
    # make sure only 1 variant has default=1
      if (scalar(keys(%{$self->{_curVariantGroupDefHash}{variant}})) > 0) {
	  # this returns all the defaults equal to 1. that is, if this returns
	  # more than 1 value, then there are multiple variants declared as default
	  # if it returns 0 values, then no variants have been declared default.
	  my @defaults = grep 
	  {
	      ($self->{_curVariantGroupDefHash}{variant}{$_}{default} eq '1')
	      } keys(%{$self->{_curVariantGroupDefHash}{variant}});
	  if (scalar(@defaults) > 1) 
	  {
	      $self->{_errStr} .= "ERROR: Multiple variants of variant group ".
		  $self->{_curVariantGroupName}." are declared as default\n";
	  } 
	  elsif (scalar(@defaults) < 1) 
	  {
	      $self->{_errStr} .= "ERROR: No variants of variant group ".
		  $self->{_curVariantGroupName}." are declared as default\n";
	  }
      }

      # assign the tmp hash to the main _variantgroups map
      $_variantgroups->{$self->{_curVariantGroupName}} = $self->{_curVariantGroupHash}{variant};
      $self->{_curVariantGroupHash} = ();
      delete($self->{_curVariantGroupName});
  }

}

### 1 = &dumpTestTree()
# a debug utility that will dump everything to stdout
sub dumpTestTree {
  my $self = shift;

  my $_tests = $self->{_tests};
  print scalar(keys(%$_tests)) . " tests defined\n";
  foreach my $test (keys(%$_tests)) {
    print "Test: $test\n";
    print "  Groups: " . join(" ", @{$_tests->{$test}{group}} ) . "\n";
    print "  Match: " . $_tests->{$test}{match} . "\n";
    print "  Archs: " . join(" ", @{$_tests->{$test}{arch}}). "\n";
    print "  Directory: " . $_tests->{$test}{dir} . "\n";
    print "  Dependencies: " . join(" ", @{$_tests->{$test}{dep}} ) . "\n";
    print "  WaitForTest: ". $_tests->{$test}{waitfor} . "\n";
    print "  Size Arg: " . $_tests->{$test}{size} . "\n";
    foreach my $variantName (keys(%{$_tests->{$test}{variant}})) {
      print "  Variant: $variantName";
      if($_tests->{$test}{variant}{$variantName}{default} eq "1") {
        print " (default)";
      }
      print "\n";
      foreach my $envName (keys(%{$_tests->{$test}{variant}{$variantName}{env}})) {
        print "    Env: $envName = ".
          $_tests->{$test}{variant}{$variantName}{env}{$envName}."\n";
      }
      print "    Groups: ".
                 join(" ", @{$_tests->{$test}{variant}{$variantName}{group}}).
		 "\n";
      print "    Archs: ".
                 join(" ", @{$_tests->{$test}{variant}{$variantName}{arch}}).
		"\n";
      print "    Size Arg: ".$_tests->{$test}{variant}{$variantName}{size}."\n";
    }
  }

}

### $bool = &isValidTest( $testName )
# given a testName, return 1 if it exists, 0 if it does not
sub isValidTest {
  my ($self, $testName) = @_;
  my $_tests = $self->{'_tests'};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  # is $testName in hash?
  return ($testName and exists($_tests->{$testName}));
}

### \@tests = &getTestNames( [$group1, $group2, ...] )
# given a group name, return a ref to a list of test names in that group
# extra args are considered to be more group names, and will return the union
# of all test groups found. if no group is specified, return 'em all.
# also, if only some variants of the test are assigned to be in the group,
# include the test name in the results
sub getTestNames {
  my ($self, @groups) = @_;
  my $_tests = $self->{'_tests'};
  my @lTests;

#  print "looking for " . join(" ", @groups). " tests\n";
 
  # if we have any groups,... 
  if(scalar(@groups)) {
    my $searchStr = join("|", @groups);
    # this will return every key of %_tests that has at least one of the
    # specified groups
    @lTests = grep { $self->_groupFindHelper($_, $searchStr)
             } keys(%$_tests);

  # if not, then just return all keys of %_tests
  } else {
    @lTests = keys(%$_tests);
  }

  return \@lTests;
}

sub _groupFindHelper {
  my ($self, $test, $searchStr) = @_;
  my $_tests = $self->{'_tests'};
 
  if (grep (/^($searchStr)$/, @{$_tests->{$test}{group}})) {
    return 1;
  } elsif(grep (/^($searchStr)$/, map { @{$_tests->{$test}{variant}{$_}{group}} } keys(%{$_tests->{$test}{variant}}) )) {
    return 1;
  } else {
    return 0;
  }
}
  

### $numOfTestsLeft = &pruneByGroup( [$group1, $group2, ...] )
# given a list of groups, delete any tests from the TestList obj that are not
# in at least one of the groups. returns resulting num of tests
sub pruneByGroup {
  my ($self, @groups) = @_;
  my $_tests = $self->{'_tests'};

  # if we have any groups,... 
  if(scalar(@groups)) {
    my $searchStr = join("|", @groups);
    foreach my $iKey (keys(%$_tests)) {
      unless(grep(/^$searchStr$/, @{$_tests->{$iKey}{'group'}})) {
        delete($_tests->{$iKey});
      }
    }
  } 
  # return the number of tests left
  return scalar(keys(%$_tests));

  # if we don't have any groups, then this is a no-op
}

### $numOfTestsLeft = &pruneByArch( $architecture )
# given an architecture, delete any tests from the TestList obj that have an
# arch directive prohibiting execution on that platform
sub pruneByArch {
  my ($self, $myArch) = @_;
  my $_tests = $self->{'_tests'};

  # if any test is marked not.$myArch, delete it

  foreach my $iKey (keys(%$_tests)) 
  {
      my $variantsLeft = 1; # Assume a test as a single variant

      # Check for variant key associated with test.  If it has
      # variants, go thru them, see if not.arch is set.
      # if it's a not.Arch, delete the variantName key.

      if (exists ($_tests->{$iKey}{'variant'}))
      {
	  foreach my $vname (keys %{$_tests->{$iKey}{'variant'}})
	  {
	      if (grep (/^not\.$myArch$/,@{$_tests->{$iKey}{'variant'}{$vname}{'arch'}}))
	      {
		  delete ($_tests->{$iKey}{variant}{$vname});
		  $variantsLeft = values(%{$_tests->{$iKey}{'variant'}});
	      }
	  }
      }

      # if the not.arch tag associated with test name is a match, or
      # we've wiped out all of the possible variants, delete the test.

      if(grep(/^not\.$myArch$/, @{$_tests->{$iKey}{'arch'}}) ||
	 $variantsLeft == 0) 
      {
	  delete($_tests->{$iKey});
      }
  }
  
  # return the number of tests left
  return scalar(keys(%$_tests));
}

### $numOfTestsLeft = &pruneByList( \@desiredTests )
# given a list of tests, prune the testlist to only include tests in the
# supplied list. returns resulting number of tests. if list arg is empty,
# this method does nothing and returns the total number of tests. if any test
# is not in the _tests tree, it will be skipped
sub pruneByList {
  my ($self, $wantedTests) = @_;
  my $_tests = $self->{'_tests'};

  # make sure we were passed an arraay ref
  unless(ref($wantedTests) eq "ARRAY") {
    return -1;
  }

  # make sure we weren't passed an empty list
  if(scalar(@$wantedTests)) {

    # loop through the given list adding dependencies of each test if not
    # already in list. and do it until nothing new is added
    my $lastTestCount = 0;
    while ($lastTestCount != scalar(@$wantedTests)) {
      $lastTestCount = scalar(@$wantedTests);

      foreach my $iTest (@$wantedTests) {

        # if the test exists and has a dep, add the dep
        if (exists($_tests->{$iTest}) and exists($_tests->{$iTest}{'dep'})){
          foreach my $thisDep (@{$_tests->{$iTest}{'dep'}}) {
	    unless(grep (m#^$thisDep/?$#, @$wantedTests)) {
	      push @$wantedTests, $thisDep;
            }
          }
        # but if the test does not exist
        } elsif (not exists($_tests->{$iTest})) {
          # is it a match to a test that does exist?
          my @matches = $self->getMatchedTestNames($iTest);
          if (scalar(@matches) > 0) {
 	    # it is, so make a dup of the test
            $self->addNewTest($iTest);
	    $self->setRootDirectory($iTest,
               $self->getRootDirectory($matches[0]));
            $self->setDependency($iTest, @{$self->getDependency($matches[0])});

            # then add its deps to wantedTests
	    foreach my $thisDep (@{$_tests->{$iTest}{'dep'}}) {
              unless(grep (m#^$thisDep/?$#, @$wantedTests)) {
                push @$wantedTests, $thisDep;
              }
            }
          }
        }
      }
    }
    
    # if any test's name is not in the list, delete it
    foreach my $iKey (keys(%$_tests)) {
      unless(grep(m#^$iKey/?$#, @$wantedTests)) {
        delete($_tests->{$iKey});
      }
    }

  }

  # return the number of tests left
  return scalar(keys(%$_tests));
}

### \@rootDirs = &getRootDirs( [$group1, $group2, ...] )
# works the same as getTestNames, except instead of providing a list of test
# names, provides a list of all root directories for the tests in the
# specified groups. each root dir will only appear in the list once, regardless
# of how many tests have that root dir
sub getRootDirs{
  my ($self, @groups) = @_;
  my $_tests = $self->{'_tests'};
  my @lTests; my @lRootDirs;

  # okay, this is tricky...
  # sortuniq will sort and uniq-ify the array containing ...
  @lRootDirs = &CdsUtil::sortuniq(
		 # ... the list of root dirs that we get by mapping a lookup to
		 # the 'dir' key ...
                 map {
                   $_tests->{$_}{'dir'} 
                 } 
		 # ... for each of the testnames associated with these groups
                 @{$self->getTestNames(@groups)});

  # then return a ref to it
  return \@lRootDirs;
}

### \@tests = getTestsByVariants ( $variantName [, $variantName2, ...] )
# given variantName[s], return any test that has that variant defined
sub getTestsByVariants {
  my ($self, @variants) = @_;
  my $_tests = $self->{_tests};
  my @lTests = ();

  # another tricky map ...
  # sortuniq will sort and uniq-ify the array containing ...
  @lTests = &CdsUtil::sortuniq(
    # .. the list of tests we get by using grep to determine ..
    grep {
      my $matches = 0;
      # .. whether any of the supplied variants is defined for each test ..
      foreach my $iVar (@variants) {
        if (exists($_tests->{$_}{variant}{$iVar})) { $matches++; }
      }
      $matches;
    }
    # .. and by each test, we mean all the tests defined, no matter the grouping
    keys(%$_tests)
  );

  # return a ref to that list
  return \@lTests;
}

### \@groups = &getGroups( $testName, $variantName)
# given a test name, return a reference to a list of its groups
# returns singleton of -1 if test is not found, empty list if no groups
sub getGroups {
  my ($self, $testName, $variantName) = @_;
  my $_tests = $self->{'_tests'};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  # if $testName is not in hash, return empty list;
  unless($testName and exists($_tests->{$testName})) {
    return [];
  }

  # if $variantName given, but not there, return empty list
  if ($variantName and not exists($_tests->{$testName}{variant}{$variantName})){
    return [];
  }

  if ($variantName) {
    my @groups = (@{$_tests->{$testName}{group}}, 
                  @{$_tests->{$testName}{variant}{$variantName}{group}});
    return \@groups;
  } else {
    return \@{$_tests->{$testName}{group}};
  }
}

### \@dep = &getDependency( $testName )
# given a test name, return a list of dependencies
sub getDependency {
  my ($self, $testName) = @_;
  my $_tests = $self->{'_tests'};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  # if $testName is not in hash, return empty list;
  unless($testName and exists($_tests->{$testName})) {
    return [];
  }

  # just return the scalar, which will be null if no dep exists
  return \@{$_tests->{$testName}{'dep'}};
}

### $waitForTest = &getWaitForTest( $testName )
# given a test name, return the test to wait on before executing
sub getWaitForTest {
  my ($self, $testName) = @_;
  my $_tests = $self->{'_tests'};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  # if $testName is not in hash, return -1;
  unless($testName and exists($_tests->{$testName})) {
    return -1;
  }

  # just return the scalar, which will be null if no dep exists
  return $_tests->{$testName}{'waitfor'};
}

### \@deps = &getDependents( $testName )
# given a test name, return all tests that depend on it
sub getDependents {
  my ($self, $testName) = @_;
  my $_tests = $self->{'_tests'};
  my @dependents = ();

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  # if $testName is not in hash, return empty list;
  unless($testName and exists($_tests->{$testName})) {
    return [];
  }

  @dependents = grep {
                  (scalar($_tests->{$_}{'dep'}) > 0) and
		  (scalar(grep(/^$testName$/, @{$_tests->{$_}{'dep'}})) > 0)
                } keys(%$_tests);

  return \@dependents;
}

### $sizeArg = &getSizeArg( $testName, [$variantName])
# given a test name, return its size arg
sub getSizeArg {
  my ($self, $testName, $variantName) = @_;
  my $_tests = $self->{'_tests'};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  # if $testName is not in hash, return -1;
  unless($testName and exists($_tests->{$testName})) {
    return -1;
  }

  # If $variantName given and not defined for test, see if it exists
  # in the variant group definitions.  Otherwise, return -1.
  if($variantName and not exists($_tests->{$testName}{variant}{$variantName})){
    my @variantGroups = keys(%{$self->{'_variantgroups'}});
    foreach my $group (@variantGroups) {
      if (exists($self->{'_variantgroups'}{$group}{$variantName})) {
        return $self->{'_variantgroups'}{$group}{$variantName}{size}
      }
    }
    return -1;
  }

  # just return the scalar, which will be null if no dep exists
  if ($variantName) {
    return $_tests->{$testName}{variant}{$variantName}{size};
  } else {
    return $_tests->{$testName}{size};
  }
}

### $matchArg = &getMatchArg( $testName )
# given a test name, return its match arg
sub getMatchArg {
  my ($self, $testName) = @_;
  my $_tests = $self->{'_tests'};
                                                                                
  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;
                                                                                
  # if $testName is not in hash, return -1;
  unless($testName and exists($_tests->{$testName})) {
    return -1;
  }
                                                                                
  # just return the scalar, which will be null if no dep exists
  return $_tests->{$testName}{'match'};
}

### @testNames = &getMatchedTestNames ( $subTestName )
# given a test name (which may not be valid in the current context), return
# an array of tests that for which the match arg matches the specified 
# subtest name
sub getMatchedTestNames {
  my ($self, $subTestNameArg) = @_;
  my $_tests = $self->{'_tests'};

  # remove trailing slash, if it exists
  $subTestNameArg =~ s#/$## if $subTestNameArg;
  my $matchStr;

  # this returns a list of tests whose match arg matches the supplied sub test
  my @matchedTests = grep {
		       ($matchStr = $_tests->{$_}{'match'}) and
		       ($subTestNameArg =~ m#^$matchStr$#)
		     } keys(%$_tests);

  return @matchedTests;
}

### $memInt = &getMemArg ( $testName )
# return only the mem= requirement from the size arg, in megabytes
# return 0 if no mem req, -1 on error
sub getMemArg {
  my ($self, $testName) = @_;
  my $_tests = $self->{'_tests'};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  # if $testName is not in hash, return -1;
  unless($testName and exists($_tests->{$testName})) {
    return -1;
  }

  # is there a mem req in the size?
  if (! exists ($_tests->{$testName}) || # test not in hash probably can't happen
      ! exists ($_tests->{$testName}{'size'})) { # no size restriction in hash
      return 0;
  } elsif (! defined ($_tests->{$testName}{'size'})) { # no defined value for size
      return 0;
  } elsif($_tests->{$testName}{'size'} =~ m#mem=([\d\.]+)([mg])#) {
    # in gigs? mult by 1024 to convert to megs
    if($2 eq 'g') {
      return $1 * 1024;
    # or return megs
    } else {
      return $1;
    } 
  # no req, return 0
  } else {
    return 0;
  }
}

### \@variantNames = &getVariantNames( $testName )
# given a testname, return a ref to list of variant names, empty hash if
# none exist, singleton of -1 if error
sub getVariantNames {
  my ($self, $testName) = @_;
  my $_tests = $self->{_tests};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;
  
  # if $testName is not in hash, return -1;
  unless($testName and exists($_tests->{$testName})) {
    return [-1];
  }

  my @variantNames = keys(%{$_tests->{$testName}{variant}});
  return \@variantNames;
}

### $variantName = &getDefaultVariantName( $testName )
# return "" if no variants defined for test
sub getDefaultVariantName {
  my ($self, $testName) = @_;
  my $_tests = $self->{_tests};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;
  
  # if $testName is not in hash, return -1;
  unless($testName and exists($_tests->{$testName})) {
    return -1;
  }

  if (scalar(keys(%{$_tests->{$testName}{variant}})) < 1) {
    return "";
  }

  # this should produce a list of precisely 1 element
  my @variants = grep {
		   $_tests->{$testName}{variant}{$_}{default} eq "1"
		 } keys(%{$_tests->{$testName}{variant}});

  # this should never happen, since an error like this would've been caught by
  # the parser
  unless(scalar(@variants) == 1) {
    print STDERR "ERROR: 1 and only 1 variant for $testName should be set to default!\n";
  }

  return shift(@variants);
}

### \@variantNames = &getAllVariantNames()
# return a list of all possible variant names defined
# outside test entries, in the variantgroupdef elements
sub getAllVariantNames {
  my ($self) = @_;

  my @variantNames = ();
  my @variantGroups = keys(%{$self->{'_variantgroups'}});
  foreach my $group (@variantGroups) {
    my @namesInGroup = keys(%{$self->{'_variantgroups'}{$group}});
    foreach my $name (@namesInGroup) {
      push (@variantNames, $name);
    }
  }

  return \@variantNames;
}


### $bool = &testIsInGroup( $testName, $groupName )
# given a testname and a groupname, return 1 if test is in group, 0 if the
# test exists, but is not in group, and -1 if test does not exist
# will not return yes if only some variants are in group
sub testIsInGroup {
  my ($self, $testName, $groupName) = @_;
  my $_tests = $self->{'_tests'};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  # if $testName is not in hash, return -1;
  unless($testName and exists($_tests->{$testName})) {
    return -1;
  }

  # grep for this groupName in the group list for this test
  return grep (m#^$groupName$#, @{$_tests->{$testName}{'group'}});
}

### $bool = &testVariantIsInGroup( $testName, $variantName, $groupName )
# given a testname, variantname and groupname, return 1 if that test/variant
# pair is in the group, 0 if the test/variant pair exists, but not in group,
# -1 if test/variant pair does not exist
sub testVariantIsInGroup {
  my ($self, $testName, $variantName, $groupName) = @_;
  my $_tests = $self->{_tests};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  # if $testName is not in hash, return -1;
  unless($testName and exists($_tests->{$testName})) {
    return -1;
  }

  # if $variantName not defined for test, return -1
  unless(exists($_tests->{$testName}{variant}{$variantName})){
    return -1;
  }

  # if group is defined at test level, return 1
  if (grep (m#^$groupName$#, @{$_tests->{$testName}{'group'}})) {
     return 1;
  # or if group is defined at variant level, return 1
  } elsif (grep (m#^$groupName$#,
           @{$_tests->{$testName}{variant}{$variantName}{group}})) {
     return 1;
  # neither, so return 0
  } else {
     return 0;
  }
}

### $bool = &testVariantIsInGroups( $testName, $variantName, $groupName, [$groupName]... )
# given a testname, variantname and groupnames, return 1 if that test/variant
# pair is in at least 1 of the groups, 0 if the test/variant pair exists, but
# not in groups, -1 if test/variant pair does not exist
sub testVariantIsInGroups {
  my ($self, $testName, $variantName, @groupNames) = @_;
  my $_tests = $self->{_tests};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  # if $testName is not in hash, return -1;
  unless($testName and exists($_tests->{$testName})) {
    return -1;
  }

  # if $variantName not defined for test, return -1
  unless(exists($_tests->{$testName}{variant}{$variantName})){
    return -1;
  }

  foreach my $groupName (@groupNames) {
    # if group is defined at test level, return 1
    if (grep (m#^$groupName$#, @{$_tests->{$testName}{'group'}})) {
       return 1;
    # or if group is defined at variant level, return 1
    } elsif (grep (m#^$groupName$#,
             @{$_tests->{$testName}{variant}{$variantName}{group}})) {
       return 1;
    }
  }
  # if we made it through that loop without returning, must not be defined - 
  # so just return a 0
  return 0;
}

### $dir = &getRootDirectory( $testName )
# given a test name, return its root directory or -1 on error
sub getRootDirectory {
  my ($self, $testName) = @_;
  my $_tests = $self->{'_tests'};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  # if $testName is not in hash, try match arg
  unless($testName and exists($_tests->{$testName})) {
    my $matchedTest = ($self->getMatchedTestNames($testName))[0];
    # in case of empty match list
    if($matchedTest) {
	return $_tests->{$matchedTest}{'dir'};
    }
    # error - testName not recognized and no match
    return -1;
  }

  # just return the scalar, which defaulted to testName during init
  return $_tests->{$testName}{'dir'};
}

### \@archs = &getArchitectures( $testName, $variantName)
# given a test name, return a ref to an array containing its arch directives
# return singleton of -1 if test DNE, reutrn empty list if no arch
# see also canRunOnArch($test, $arch);
sub getArchitectures {
  my ($self, $testName, $variantName) = @_;
  my $_tests = $self->{'_tests'};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  # if $testName is not in hash, return -1;
  unless($testName and exists($_tests->{$testName})) {
    return [-1];
  }

  # if $variantName given and not defined for test, return -1
  if($variantName and not exists($_tests->{$testName}{variant}{$variantName})){
    return [-1];
  }

  # return a ref to the array
  if ($variantName) {
    return \@{$_tests->{$testName}{variant}{$variantName}{arch}};
  } else {
    return \@{$_tests->{$testName}{arch}};
  }
}

### \%envVars = &getVariantEnvHash( $testName, $variantName )
# given a test name and variant name, return ref to hash mapping env vars to
# values. return singleton of -1 on error
sub getVariantEnvHash {
  my ($self, $testName, $variantName) = @_;
  my $_tests = $self->{'_tests'};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  # if $testName is not in hash, return -1;
  unless($testName and exists($_tests->{$testName})) {
    return [-1];
  }

  # if $variantName given and not defined for test, see if it exists
  # in the variant group definitions.
  unless($variantName and exists($_tests->{$testName}{variant}{$variantName})){
    my @variantGroups = keys(%{$self->{'_variantgroups'}});
    foreach my $group (@variantGroups) {
      if (exists($self->{'_variantgroups'}{$group}{$variantName})) {
        return \%{$self->{'_variantgroups'}{$group}{$variantName}{env}}
      }
    }
    return [-1];
  }

  return \%{$_tests->{$testName}{variant}{$variantName}{env}};
}

sub getEnvHash
{
  my ($self, $testName) = @_;
  my $_tests = $self->{'_tests'};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  return \%{$_tests->{$testName}{env}};
}

### $bool = &canRunOnArch( $testName, $arch )
# given a testName and an architecture (Win, Linux, SunOS), return if this
# test is marked for running on this arch (or that it is not marked prohibited)
sub canRunOnArch {
  my ($self, $testName, $archStr) = @_;
  my $_tests = $self->{'_tests'};

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  # if $testName is not in hash, return -1;
  unless($testName and exists($_tests->{$testName})) {
    return -1;
  }

  # for now, we only support not.$arch to prohibit, so if it's there, return
  # false (0)

  if(grep(/^not\.$archStr$/, @{$_tests->{$testName}{'arch'}} )) {
    return 0;

  # and if not, then it's ok, so return true (1)
  } else {
    return 1;
  }
}

### \@tests = &getTestsFromDir( $dirPath, [$exactMatchBool] )
# given a directory path (relative to $CARBON_HOME), return all tests that have
# that directory as the root directory. return empty list if no tests have 
# this root dir. if argExact is set to 0, then will match any subdir as well
sub getTestsFromDir {
  my ($self, $dirPath, $argExact) = @_;
  my $_tests = $self->{'_tests'};

  my $exact = $argExact;
  unless(defined($argExact)) {
    $exact = 1;
  }

  # remove any trailing slash from searchStr
  $dirPath =~ s#/$## if $dirPath;

  my @lTests;
  # this will return every key of %_tests that matches the dir arg
  if ($exact) {
    @lTests = grep { 
                $_tests->{$_}{'dir'} =~ /^$dirPath$/
              } keys(%$_tests);
  } else {
    @lTests = grep { 
                $_tests->{$_}{'dir'} =~ m#^$dirPath(/.*)?$#
              } keys(%$_tests);
  }

  return \@lTests;
}

### \@tests = &getTestsWithCommonDir( $testName, [$exactMatchBool] )
# given a test, return all tests (including the arg) that have the same
# root directory. return singleton of -1 if test DNE
# if argExact is set to 0, then will match any subdir as well
sub getTestsWithCommonDir {
  my ($self, $testName, $argExact) = @_;
  my $_tests = $self->{'_tests'};

  my $exact = $argExact;
  unless(defined($argExact)) {
    $exact = 1;
  }

  # remove trailing slash, if it exists
  $testName =~ s#/$## if $testName;

  # if $testName is not in hash, return -1;
  unless($testName and exists($_tests->{$testName})) {
    return [-1];
  }

  my $lDir = $_tests->{$testName}{'dir'};

  return $self->getTestsFromDir($lDir, $exact);
}

### \@matchingTestNames = &searchTests( $searchStr, [$caseMatchBool] )
# given a string to search for, return a ref to a list of all matching 
# test names. 2nd arg sets case sensitivity (1 = case sensitive, off by default)
sub searchTests {
  my ($self, $searchStr, $caseSensitive) = @_;
  my $_tests = $self->{'_tests'};

  # remove any trailing slash from searchStr
  $searchStr =~ s#/$## if $searchStr;

  my @lTests;
  if($caseSensitive) {
    @lTests = grep (m#$searchStr#, keys(%$_tests));
  } else {
    @lTests = grep (m#$searchStr#i, keys(%$_tests));
  }

  return \@lTests
}

### $statusInt = &addNewTest( $testName )
# add a new test. make sure it doesn't exist. return 1 if added, 0 if not
sub addNewTest {
  my ($self, $testName) = @_;
  my $_tests = $self->{'_tests'};

  # remove any trailing slash from testName
  $testName =~ s#/$## if $testName;

  if($testName and exists($_tests->{$testName})) {
    print STDERR "Test $testName already exists.\n";
    return 0;
  }

  # set arch and group and dep to empty arrays
  @{$_tests->{$testName}{'arch'}} = ();
  @{$_tests->{$testName}{'group'}} = ();
  @{$_tests->{$testName}{'dep'}} = ();
  # set dir to name
  $_tests->{$testName}{'dir'} = $testName;

  return 1;
}

### $statusInt = &addVariant ($testName, $variantName)
# add a new variant $variantName to test $testName. if 1st variant being added,
# make it default. return 1 if added, 0 if not
sub addVariant {
  my ($self, $testName, $variantName) = @_;
  my $_tests = $self->{'_tests'};

  # are there any other variants already?
  my $firstVariant = 0;
  if (not exists($_tests->{$testName}{variant}) or
      scalar(%{$_tests->{$testName}{variant}}) == 0) {
    $firstVariant = 1;
  }

  # remove any trailing slash from testName
  $testName =~ s#/$## if $testName;
  unless($self->_validateSetterArgs($testName, $variantName)) {
    return 0;
  }

  # initialize empty lists for group and arch
  @{$_tests->{$testName}{variant}{$variantName}{group}} = ();
  @{$_tests->{$testName}{variant}{$variantName}{arch}} = ();
  # set to default if this is the 1st variant to be defined for this test
  if($firstVariant) {
    $_tests->{$testName}{variant}{$variantName}{default} = 1;
  } else {
    $_tests->{$testName}{variant}{$variantName}{default} = 0;
  }
  return 1;
}

### $statusInt = &makeVariantDefault ($testName, $variantName)
# make $variantName the default variant for test $testName, overriding the
# previous default value
sub makeVariantDefault {
  my ($self, $testName, $variantName) = @_;
  my $_tests = $self->{'_tests'};

  # remove any trailing slash from testName
  $testName =~ s#/$## if $testName;
  unless($self->_validateSetterArgs($testName, 1, $variantName)) {
    return 0;
  }

  my $previousDefault = $self->getDefaultVariantName( $testName );
  $_tests->{$testName}{variant}{$previousDefault}{default} = 0;
  $_tests->{$testName}{variant}{$variantName}{default} = 1;
  return 1;
}

### $statusInt = &addEnvForVariant ($testName, $varntName, $envName, $envValue)
# add a new env name/value to the variant $varntName of test $testName
# return 1 if added, 0 if not
sub addEnvForVariant {
  my ($self, $testName, $variantName, $envName, $envValue) = @_;
  my $_tests = $self->{'_tests'};

  # remove any trailing slash from testName
  $testName =~ s#/$## if $testName;
  unless($self->_validateSetterArgs($testName, $envName, $variantName)) {
    return 0;
  }

  $_tests->{$testName}{variant}{$variantName}{env}{$envName} = $envValue;
}

sub addEnvForTest 
{
    my ($self, $testName, $envName, $envValue) = @_;
    my $_tests = $self->{'_tests'};
    
    # remove any trailing slash from testName
    $testName =~ s#/$## if $testName;
    unless($self->_validateSetterArgs($testName, $envName)) {
	return 0;
    }
    
    $_tests->{$testName}{env}{$envName} = $envValue;
}

# $statusInt = &_setScalarArgByName ($testName, $argName, $data, [$variantName])
sub _setScalarArgByName {
  my ($self, $testName, $argName, $data, $variantName) = @_;
  my $_tests = $self->{_tests};

  # remove any trailing slash from testName
  $testName =~ s#/$## if $testName;

  # validate all the args with the helper routines
  unless(
    $self->_validateSetterArgs($testName, $data, $variantName) and
    $self->_isScalarArg($argName) and 
    (not $variantName or $self->_isVariantArg($argName))) {
      return 0;
  }

  # set it
  if ($variantName){
    $_tests->{$testName}{variant}{$variantName}{$argName} = $data;
  } else {
    $_tests->{$testName}{$argName} = $data;
  }
  return 1;

}

# $statusInt = _addValueToListArg($test, $argName, $data, [$variant])
sub _addValueToListArg {
  my ($self, $testName, $argName, $data, $variantName) = @_;
  my $_tests = $self->{_tests};

  # remove any trailing slash from testName and dirName
  $testName =~ s#/$## if $testName;

  # validate all the args with the helper routines
  unless(
    $self->_validateSetterArgs($testName, $data, $variantName) and
    $self->_isListArg($argName) and 
    (not $variantName or $self->_isVariantArg($argName))) {
      return 0;
  }

  if ($variantName) {
    push @{$_tests->{$testName}{variant}{$variantName}{$argName}}, $data;
  } else {
    push @{$_tests->{$testName}{$argName}}, $data;
  }
  return 1;
}

# $statusInt = _setListArgByName ( $testName, $argName, \@list, $variantName)
sub _setListArgByName {
  my ($self, $testName, $argName, $listRef, $variantName) = @_;
  my $_tests = $self->{_tests};

  # remove any trailing slash from testName and dirName
  $testName =~ s#/$## if $testName;

  # validate all the args with the helper routines
  unless(
    $self->_validateSetterArgs($testName, $listRef, $variantName) and
    $self->_isListArg($argName) and 
    (not $variantName or $self->_isVariantArg($argName))) {
      return 0;
  }

  if($variantName) {
    @{$_tests->{$testName}{variant}{$variantName}{$argName}} = @$listRef;
  } else {
    @{$_tests->{$testName}{$argName}} = @$listRef;
  }
  return 1;
}

sub _validateSetterArgs {
  my ($self, $testName, $data, $variantName) = @_;
  my $_tests = $self->{'_tests'};

  # make sure test exists and data was supplied
  unless($testName and $data and exists($_tests->{$testName})) {
    print STDERR "Test $testName does not exist (or no data '$data')\n";
    return 0;
  }
  # variant must already exist if given
  if ($variantName and not exists($_tests->{$testName}{variant}{$variantName})){
    print STDERR "Variant $variantName for $testName does not exist\n";
    return 0;
  }
  return 1;
}

sub _isListArg {
  my ($self, $argName) = @_;
  my @validArgs = qw(dep group arch);
  # make sure argName is one we know about
  unless(grep (/^$argName$/, @validArgs)) {
    print STDERR "Field $argName unknown in list context\n";
    return 0;
  }
  return 1;
}

sub _isScalarArg {
  my ($self, $argName) = @_;
  my @validArgs = qw(dir size waitfor match);
  # make sure argName is one we know about
  unless(grep (/^$argName$/, @validArgs)) {
    print STDERR "Field $argName unknown in scalar context\n";
    return 0;
  }
  return 1;
}

sub _isVariantArg {
  my ($self, $argName) = @_;
  my @validArgs = qw(size group arch);
  # make sure argName is one we know about
  unless(grep (/^$argName$/, @validArgs)) {
    print STDERR "Field $argName unknown in variant context\n";
    return 0;
  }
  return 1;
}

### $statusInt = &setRootDirectory( $testName, $rootDirName )
# set dir for test to specified dir. return 1 if set, 0 if not
sub setRootDirectory {
  my ($self, $testName, $dirName) = @_;

  # remove any trailing slash from dirName
  $dirName =~ s#/$## if $dirName;

  return $self->_setScalarArgByName($testName, 'dir', $dirName);
}

### $statusInt = &setWaitForTest( $testName, $waitForTestName )
# set waitfor for test to specified test. return 1 if set, 0 if not
sub setWaitForTest {
  my ($self, $testName, $waitForTestName) = @_;

  # remove any trailing slash from waitfor
  $waitForTestName =~ s#/$## if $waitForTestName;

  return $self->_setScalarArgByName($testName, 'waitfor', $waitForTestName);
}

### $statusInt = &setSizeArg( $testName, $sizeArgStr, [$variantName] )
# set size arg for test to specified dir. return 1 if set, 0 if not
sub setSizeArg {
  my ($self, $testName, $sizeArg, $variantName) = @_;
  return $self->_setScalarArgByName($testName, 'size', $sizeArg, $variantName);
}

### $statusInt = &setMatchArg ( $testName, $matchArgStr )
# set match arg for test to specified string. return 1 if set, 0 if not
sub setMatchArg {
  my ($self, $testName, $matchArg) = @_;
  return $self->_setScalarArgByName($testName, 'match', $matchArg);
}

### $statusInt = &setDependency( $testName, [ $dependencyTestName, ... ] )
# set dep for test to specified list of tests. return 1 if set, 0 if not
sub setDependency {
  my ($self, $testName, @depList) = @_;
  my $_tests = $self->{'_tests'};

  @depList = map { s#/$##; $_; } @depList;
  # check each dep test to make sure it exists
  my $validTests = grep { exists($_tests->{$_}) } @depList;
  unless((scalar(@depList) > 0) and (scalar(@depList) == $validTests)) {
    print STDERR "Some of specified dependencies " .join(",", @depList).
      " do not exist.\n";
    return 0;
  }

  return $self->_setListArgByName($testName, 'dep', \@depList);
}


### $statusInt = &addGroup( $testName, $groupName, [$variantName])
# add specifed group to list of groups for testName. return 1 if changed,
# 0 if not
sub addGroup {
  my ($self, $testName, $groupName, $variantName) = @_;

  # can only have alpha-numerics in group name
  unless ($groupName =~ m#^\w+$#) {
    print STDERR "Test group can only contain alpha-numeric characters.\n";
    return 0;
  }

  return
    $self->_addValueToListArg($testName, 'group', $groupName, $variantName);
}

### $statusInt = &addDependency( $testName, $dependencyName )
# add specifed dep to list of deps for testName. return 1 if changed,
# 0 if not
sub addDependency {
  my ($self, $testName, $depName) = @_;
  my $_tests = $self->{'_tests'};

  # make sure dependency exists
  unless($depName and exists($_tests->{$depName})) {
    print STDERR "Dependency test $depName does not exist.\n";
    return 0;
  }

  return $self->_addValueToListArg($testName, 'dep', $depName);
}

### $statusInt = &addArch( $testName, $architecture, [$variantName] )
# add specified arch to list of archs for testName, return 1 if changed, 
# 0 if not
sub addArch {
  my ($self, $testName, $archName, $variantName) = @_;

  my $lSearchArch = $archName;
  $lSearchArch =~ s/^not\.//;

  unless(grep(/^$lSearchArch$/, @{$self->{'_validArchs'}})) {
    print STDERR "Test architecture $lSearchArch is not valid. Valid values are: ".
      join(" ", @{$self->{'_validArchs'}}) . ".\n";
    return 0;
  }

  return $self->_addValueToListArg($testName, 'arch', $archName, $variantName);
}

### $statusInt = &setGroup( $testName, [$group1, $group2, ...] )
# set groups for test to specified array. return 1 if changed, 0 if not
# note: passing this an empty array will work just fine. beware
sub setGroup {
  my ($self, $testName, @groupsList) = @_;

  # can only have alpha-numerics in group name
  foreach my $iGroup (@groupsList) {
    unless ($iGroup =~ m#^\w+$#) {
      print STDERR "Test group can only contain alpha-numeric characters.\n";
      return 0;
    }
  }

  return $self->_setListArgByName($testName, 'group', \@groupsList);  
} 

### $statusInt = &setGroupForVariant( $testName, $variantName, [$group1, $group2, ...] )
# set groups for test to specified array. return 1 if changed, 0 if not
# note: passing this an empty array will work just fine. beware
sub setGroupForVariant {
  my ($self, $testName, $variantName, @groupsList) = @_;

  # can only have alpha-numerics in group name
  foreach my $iGroup (@groupsList) {
    unless ($iGroup =~ m#^\w+$#) {
      print STDERR "Test group can only contain alpha-numeric characters.\n";
      return 0;
    }
  }

  return $self->_setListArgByName($testName, 'group', \@groupsList, $variantName);  
} 


### $statusInt = &setArch( $testName, [$arch1, $arch2, ...] )
# set archs for test to specified array. return 1 if changed, 0 if not
# note: passing this an empty array will work just fine. beware
sub setArch {
  my ($self, $testName, @archList) = @_;

  # make sure archs are valid
  foreach my $iArch (@archList) {
    $iArch =~ m/^(not\.)?(.*)$/;
    unless (grep (/^$2$/, @{$self->{'_validArchs'}})) {
      print STDERR "Test architecture is not valid. Valid values are: ".
	join(" ", @{$self->{'_validArchs'}}) . ".\n";
      return 0;
    }
  }

  return $self->_setListArgByName($testName, 'arch', \@archList); 
} 

### $statusInt = &setArchForVariant( $testName, $variantName [$arch1, $arch2, ...] )
# set archs for test to specified array. return 1 if changed, 0 if not
# note: passing this an empty array will work just fine. beware
sub setArchForVariant {
  my ($self, $testName, $variantName, @archList) = @_;

  # make sure archs are valid
  foreach my $iArch (@archList) {
    $iArch =~ m/^(not\.)?(.*)$/;
    unless (grep (/^$2$/, @{$self->{'_validArchs'}})) {
      print STDERR "Test architecture is not valid. Valid values are: ".
	join(" ", @{$self->{'_validArchs'}}) . ".\n";
      return 0;
    }
  }

  return $self->_setListArgByName($testName, 'arch', \@archList, $variantName); 
} 
### $statusInt = &dumpToXML( $xmlOutputFileName )
# write out the current %_tests tree to the specified xml file
# return 1 on success, 0 on fail
sub dumpToXML {
  my ($self, $xmlOutFile) = @_;
  my $_tests = $self->{'_tests'};

  unless(open(XMLOUT, ">$xmlOutFile")) {
    print STDERR "Unable to create $xmlOutFile: $!\n";
    return 0;
  }

  # print the xml header and the testlist start tag
  print XMLOUT <<EndOfStart;
<?xml version="1.0"?>
<testlist>
EndOfStart

  # cycle through %_tests
  foreach my $iTest (sort(keys(%$_tests))) {

    # print the test tag and the name
    print XMLOUT "<test>\n";
    print XMLOUT "  <name>$iTest</name>\n";

    # descend into the test
    foreach my $iKey (sort(keys(%{$_tests->{$iTest}}))) {

      # if we have a variant sub-tree, descend and print it
      if ($iKey eq 'variant') {
        # print each variant
        foreach my $varName (sort(keys(%{$_tests->{$iTest}{variant}}))) {

          # print variant start tag using name and default
          print XMLOUT "  <variant name=\"".$varName."\" default=\"".
            $_tests->{$iTest}{variant}{$varName}{default} . "\">\n";

          # print groups and archs - they are lists under variant
          foreach my $type ("group", "arch") {
            foreach my $data (@{$_tests->{$iTest}{variant}{$varName}{$type}}) {
              print XMLOUT "    <$type>$data</$type>\n";
            }
          }

          # print any size tag that exists
          if (exists($_tests->{$iTest}{variant}{$varName}{size})) {
            print XMLOUT "    <size>".
              $_tests->{$iTest}{variant}{$varName}{size} . "</size>\n";
          }

          # now print env hash
          foreach my $envName 
            (sort(keys(%{$_tests->{$iTest}{variant}{$varName}{env}}))) {
            print XMLOUT "    <env name=\"$envName\">".
	      $_tests->{$iTest}{variant}{$varName}{env}{$envName}."<env>\n";
          }

          print XMLOUT "  </variant>\n";
        }

      # if this hash elem is an array, then need to print each elem
      } elsif(ref($_tests->{$iTest}{$iKey}) eq "ARRAY") {
        foreach my $elem (@{$_tests->{$iTest}{$iKey}}) {
          print XMLOUT "  <$iKey>$elem</$iKey>\n";
        }
      } 
      # if we have a variantgroup, then print its name
      elsif ($iKey eq 'variantgroup')
      {
        # print each variantgroup
        foreach my $varName (sort(keys(%{$_tests->{$iTest}{$iKey}}))) {
          # print variantgroup start tag using name
          print XMLOUT "  <variantgroup name=\"".$varName."\"/>\n";
        }
      }
      elsif ($iKey eq "env" && $_tests->{$iTest}{$iKey} =~ /HASH/) 
      {
          foreach my $elem (sort (keys (%{$_tests->{$iTest}{$iKey}})))
          {
              print XMLOUT "  <$iKey name=\"$elem\">",
              $_tests->{$iTest}{$iKey}{$elem},"</$iKey>\n";
          }
      }
      # if it's a scalar, just print it
      else {
        print XMLOUT "  <$iKey>".$_tests->{$iTest}{$iKey}."</$iKey>\n";
      }
    }

    # end tags
    print XMLOUT "</test>\n";
  }
  print XMLOUT "</testlist>\n";

  close(XMLOUT);
  return 1;

}

# END
1;
