#!/bin/sh
# -*-Perl-*-
exec ~qa/QATools/bin/perl -S -x -- "$0" "$@"
#!perl

use lib "$ENV{CARBON_HOME}/scripts";
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use Versions;

my $vInfo = new Versions();

print $vInfo->Version($ARGV[0]),"\n";
