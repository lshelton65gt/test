#!/usr/bin/python2
##
##
## Copyright (c) 2003 Carbon Design Systems, Inc.
##
##


import os

userLinkList = [];

###
### Create a directory if it doesn't exist.
###
def create_directory(dir):
    if (not os.path.exists(dir)):
        os.makedirs(dir)



###
### Create a file if it doesn't exist.
###
def create_file(f):
    if (not os.path.exists(f)):
        new_file = os.open(f, os.O_CREAT)
        os.close(new_file)


###
### Copy a file if it doesn't exist.
###
def copy_file(src, dst):
    if (not os.path.exists(dst)):
        command = "cp "+ src + " " + dst
        os.system(command)



###
### link_file will add to a list of files that we wish to link.
### Only the setup_hooks files should ever call link_file.  The setup_area
### script will be modified to use setup_hooks_intenal
def link_file(src, dst):
    userLinkList.append( (src, dst) )


###
### Link to a file if the link doesn't exist.
###
def link_file_internal(src, dst, warnExisting, overWriteExisting):
    exists = os.path.exists(dst)

    if (not exists):
        os.symlink(src, dst)
    elif (overWriteExisting):
        if (warnExisting):
            print "Warning -", dst,
            print " exists.  Overwriting with new link to", src 
        os.remove(dst)
        os.symlink(src, dst)
    else:
        if (warnExisting):
            print "Warning -", dst, "exists.  Not Overwriting"

