#!/usr/bin/python2

import sys
import string
import os
import cPickle

class clockInfo:
    "Information about a clock used for comparison"

    def __init__(self, name):
        self.name = name
        self.optimized = 0
        self.exception = 0
        self.disabled = 0

    def optimize(self):
        self.optimized = 1

    def disable(self):
        self.exception = 1
        self.disabled = 1

    def equals(self, other):
        # compare all attributes of this object
        # to another
        return ((self.name == other.name) and
                (self.optimized == other.optimized) and
                (self.exception == self.exception) and
                (self.disabled == self.disabled))

    def printMe(self, stream):
        print >> stream, " " + self.name,
        if (self.optimized):
            print >> stream, "(optimized)",
        if (self.exception):
            print >> stream, "(exception)",
        if (self.disabled):
            print >> stream, "(disabled)",

class ruleScope:
    "Rules and submodules for a given scope"

    def __init__(self, scope):
        self.name = scope
        self.clocks = []
        self.numSignals = 0
        self.subs = {}
        self.signals = {}

    def addSignalClock(self, hier, clock):
        if (len(hier) == 1):
            # Signal is in our scope, so add it
            sig_name = hier[0]
            sig = self.signals.setdefault(sig_name, ruleSignal(sig_name))
            sig.addClock(clock)
        else:
            # In a submodule
            submod_name = hier[0]
            submod = self.subs.setdefault(submod_name, ruleScope(submod_name))
            submod.addSignalClock(hier[1:], clock)
    
    def findSignal(self, hier):
        if (len(hier) == 1):
            # Signal is in our scope
            return self.signals.get(hier[0], None)
        else:
            # In a submodule
            submod_name = hier[0]
            if (submod_name not in self.subs):
                return None
            return self.subs[submod_name].findSignal(hier[1:])

    def removeSignal(self, hier):
        if (len(hier) == 1):
            # Signal is in our scope
            sig_name = hier[0]
            if (sig_name in self.signals):
                del self.signals[sig_name]
        else:
            # In a submodule
            submod_name = hier[0]
            if (submod_name not in self.subs):
                return
            self.subs[submod_name].removeSignal(hier[1:])

    def groupRules(self):
        # first group all submodules
        for submod in self.subs.itervalues():
            submod.groupRules()

        # Make a list of all clocks used by this module
        # and children.
        # Algorithm:
        # 1. Get the list of clocks for the first item
        # 2. For each remaining item, delete any clocks from
        #    the common list if they don't exist for that item
        # 3. The resulting list of clocks can be removed from
        #    all subcomponents and added to the module itself

        # First build the list of the subcomponent clocks
        clocklists = []
        for sig in self.signals.itervalues():
            clocklists.append(map(lambda x : x.name, filter(lambda x: not x.optimized, sig.clocks)))
        for submod in self.subs.itervalues():
            # if a submodule's clock list is empty, its
            # subcomponents couldn't be grouped, so we have to abort
            if (len(submod.clocks) == 0):
                return
            clocklists.append(map(lambda x : x.name, filter(lambda x: not x.optimized, submod.clocks)))

        # If there's nothing here, just return
        if (len(clocklists) == 0):
            return

        # Find the clocks that are used by the first component
        moduleclocklist = clocklists.pop(0)

        # Go through the remaining components
        for clk in moduleclocklist:
            keep = 1
            for curclocklist in clocklists:
                if clk not in curclocklist:
                    # This clock isn't common to this subcomponent, so it can't be used
                    keep = 0
                    break
            if keep:
                # All subcomponents have this clock, so we can raise it to this module
                if (clk not in map(lambda x: x.name, self.clocks)):
                    self.clocks.append(clockInfo(clk))

        # Now we have our list, so deactivate the clocks from the subcomponents
        # We won't check for existence, because if it's not there, we screwed up!
        for clk in self.clocks:
            for sig in self.signals.itervalues():
                for sigclk in sig.clocks:
                    if (sigclk.name == clk.name):
                        sigclk.optimized = 1
            for submod in self.subs.itervalues():
                for subclk in submod.clocks:
                    if (subclk.name == clk.name):
                        subclk.optimized = 1

    def findExceptions(self):
        # first process all submodules
        for submod in self.subs.itervalues():
            submod.findExceptions()

        # Determine the total number of (signals + modules)
        # at this level, and well as the number for each
        # clock.  If more than half the (signals + modules)
        # use a clock, it's more efficient to move the clock
        # rule to this scope and tag the exceptions.
        #
        # IMPORTANT: I don't think we want to tag a module as an exception...
        # at least not right now.
        #
        # Note that this will only take advantage of modules whose components
        # have already been optimized.  Since this routine can open up more
        # optimization opportunities, it might be possible to run several iterations
        # of groupRules()/findExceptions() to optimize even further.

        totalItems = len(self.signals) + len(self.subs)
        clockItems = {}

        # Get an item count for each clock
        for sig in self.signals.itervalues():
            for clk in sig.clocks:
                if (not clk.optimized):
                    if (clk.name in clockItems):
                        clockItems[clk.name] = clockItems[clk.name] + 1
                    else:
                        clockItems[clk.name] = 1
        for submod in self.subs.itervalues():
            for clk in submod.clocks:
                if (not clk.optimized):
                    if (clk.name in clockItems):
                        clockItems[clk.name] = clockItems[clk.name] + 1
                    else:
                        clockItems[clk.name] = 1

        # For each clock, see if we should generate an exception
        for key, value in clockItems.iteritems():
            if (value * 2 > totalItems):
                # Yes, we should!
                for sig in self.signals.itervalues():
                    disable = 1
                    for clk in sig.clocks:
                        if (clk.name == key):
                            # signal has this clock - don't disable
                            disable = 0
                            break
                    if disable:
                        # Didn't find this clock with this signal, so
                        # add it and disable it
                        sig.clocks.append(clockInfo(key))
                        sig.clocks[-1].disable()
                for submod in self.subs.itervalues():
                    disable = 1
                    for clk in submod.clocks:
                        if (clk.name == key):
                            # signal has this clock - don't disable
                            disable = 0
                            break
                    if disable:
                        # Didn't find this clock with this signal, so
                        # add it and disable it
                        submod.clocks.append(clockInfo(key))
                        submod.clocks[-1].disable()
            

    def printMe(self, stream, hier = ""):
        if (hier == ""):
            prefix = self.name
        else:
            prefix = hier + "." + self.name

        print >> stream, "Module: " + prefix + ":"
        print >> stream, "    clocks:",
        for clk in self.clocks:
            clk.printMe(stream)
        print >> stream
        for sig in self.signals.itervalues():
            sig.printMe(stream, prefix)
        for submod in self.subs.itervalues():
            submod.printMe(stream, prefix)

    def dumpNCompareRules(self, stream, scope, design, hier = ""):
        if (design == self.name):
            prefix = ""
        elif (hier == ""):
            prefix = self.name
        else:
            prefix = hier + "." + self.name

        print "Dumping rules for scope " + design + "." + prefix
        sys.stdout.flush()

        # Everything in here is grouped by module or signal,
        # but we'd really like things to be grouped by clock
        # for the most efficient rule dumping.  So, we'll
        # build a dictionary of all the signals using the
        # clock rule as a key.
        # Actually, we'll build two dictionaries: one for
        # standard rules and one for exceptions.  And, the
        # standard dictionary can simply be keyed by the
        # clock name

        standard_rules = {}
        exception_rules = {}
        
        # Start with the rules for the module itself
        for clk in self.clocks:
            if (clk.exception):
                # Since the key is a class, I don't think you can
                # just look up the value directly, since there's
                # no way to define a comparison function, and two
                # identical class instances won't be considered equivalent.
                # So, iterate over all items, comparing as we go.
                found = 0
                for key, value in exception_rules.iteritems():
                    if clk.equals(key):
                        # Found it!  Just append this signal
                        value.append(prefix)
                        found = 1
                        break
                if (not found):
                    # Need to add a new list for this clock
                    exception_rules[clk] = [prefix]
            elif (not clk.optimized):
                # Just add the module based on the clock name
                standard_rules.setdefault(clk.name, []).append(prefix)

        # Same thing for all the signals in this module
        for sig in self.signals.itervalues():
            if (prefix == ""):
                # Special case when the signal is in the top module of the design
                sig_name = sig.name
            else:
                sig_name = prefix + "." + sig.name
            for clk in sig.clocks:
                if (clk.exception):
                    # Since the key is a class, I don't think you can
                    # just look up the value directly, since there's
                    # no way to define a comparison function, and two
                    # identical class instances won't be considered equivalent.
                    # So, iterate over all items, comparing as we go.
                    found = 0
                    for key, value in exception_rules.iteritems():
                        if clk.equals(key):
                            # Found it!  Just append this signal
                            value.append(sig_name)
                            found = 1
                            break
                    if (not found):
                        # Need to add a new list for this clock
                        exception_rules[clk] = [sig_name]
                elif (not clk.optimized):
                    # Just add the module based on the clock name
                    standard_rules.setdefault(clk.name, []).append(sig_name)

        # OK, now we have the grouping we want.
        # First, dump a compare option for each standard clock rule, followed by
        # all the signals
        for clk, siglist in standard_rules.iteritems():
            stream.write("\n")
            stream.write("cmpSetCmpTrigger GOLDEN -Clock " + scope + "." + clk + " -Setup 0 -Hold 0 -Sample 10ps\n")
            stream.write("cmpSetCmpTrigger SECONDARY -Clock " + design + "." + clk + " -Setup 0 -Hold 0 -Sample 10ps\n")
            stream.write("\n")
            for sig in siglist:
                if (sig == ""):
                    # Special case when the "signal" is actually the top module of the design
                    stream.write("cmpSetSignalPair " + scope + " " + design + "\n")
                else:
                    stream.write("cmpSetSignalPair " + scope + "." + sig + " " + design + "." + sig + "\n")

        # Now, the same thing for the exceptions, except that we need to
        # clear previous rules first, and (possibly) add new ones
        for clk, siglist in exception_rules.iteritems():
            stream.write("\n")
            for sig in siglist:
                stream.write("cmpSetSkipSignal " + design + "." + sig + "\n")
            # only re-enable with new rules if the rule isn't disabled
            if (not clk.disabled):
                # TODO - extract new options from clockInfo object
                stream.write("\n")
                stream.write("cmpSetCmpTrigger GOLDEN -Clock " + scope + "." + clk + " -Setup 0 -Hold 0 -Sample 10ps\n")
                stream.write("cmpSetCmpTrigger SECONDARY -Clock " + design + "." + clk + " -Setup 0 -Hold 0 -Sample 10ps\n")
                stream.write("\n")
                for sig in siglist:
                    stream.write("cmpSetSignalPair " + scope + "." + sig + " " + design + "." + sig + "\n")

        # Finally, recursively call all submodules
        for submod in self.subs.itervalues():
            submod.dumpNCompareRules(stream, scope, design, prefix)

class ruleSignal:
    "Rules for a given signal"

    def __init__(self, signal):
        self.name = signal
        self.clocks = []

    def addClock(self, clock):
        if (clock not in map(lambda x: x.name, self.clocks)):
            self.clocks.append(clockInfo(clock))

    def printMe(self, stream, hier):
        print >> stream, "Signal: " + hier + "." + self.name + ":"
        print >> stream, "    clocks:",
        for clk in self.clocks:
            clk.printMe(stream)
        print >> stream

class dbInfo:
    "Smart representation of database clock information"

    def __init__(self, design):
        self.design = design
        self.top = ruleScope(design)
        self.clocks = []

    def addSignalClock(self, hier, clock):
        if (clock not in self.clocks):
            self.clocks.append(clock)
        self.top.addSignalClock(hier, clock)

    def removeClocks(self):
        for clk in self.clocks:
            hier = string.split(clk, ".")
            self.top.removeSignal(hier)

    def groupRules(self):
        self.top.groupRules()

    def findExceptions(self):
        self.top.findExceptions()

    def printMe(self, stream = sys.stdout):
        print >> stream, "Design clocks:"
        for clk in self.clocks:
            print clk
        print >> stream
        self.top.printMe(stream)

    def dumpNCompareRules(self, instance, gold, scope, start, stop, stream = sys.stdout):
        self.dumpNCompareHeader(stream, instance, gold, start, stop)
        self.dumpNCompareClocks(stream, scope)
        self.top.dumpNCompareRules(stream, scope, self.design)
        self.dumpNCompareFooter(stream)
        
    def dumpNCompareHeader(self, stream, instance, gold, start, stop):
        stream.write("cmpOpenFsdb " + gold + ".fsdb " + instance + ".fsdb\n")
        stream.write("cmpSetReport " + instance + ".nce\n")
        stream.write("cmpSetCmpOption -MaxError 1000000 -MaxErrorPerSignal 1\n")
        stream.write("cmpSetStateMap -asym (0,0,T) (0,1,F) (0,x,F) (0,z,F) (1,0,F) (1,1,T) (1,x,F) (1,z,F) (z,*,T) (x,*,T)\n")
        stream.write("cmpSetCmpOption -X T -Z T -Time (" + start + ", " + stop + ")\n\n")

    def dumpNCompareClocks(self, stream, scope):
        for clk in self.clocks:
            stream.write("cmpSetSignalPair " + scope + "." + clk + " " + self.design + "." + clk + "\n")
            
    def dumpNCompareFooter(self, stream):
        stream.write("\n")
        stream.write("set num_errors [cmpCompare]\n")
        stream.write("if {$num_errors} {\n")
        stream.write("   # Call a bogus command - this is the only way I've been able to make\n")
        stream.write("   # the script both dump an error report AND return bad status\n")
        stream.write("   bogus\n")
        stream.write("}\n")

# main functions

def printUsage():
    sys.stderr.write("usage:\n")
    sys.stderr.write("metaRules.py -m [<db_file>]\n")
    sys.stderr.write("metaRules.py -r -s <scope> [-start <time>] [-end <time>] [-gold <goldwave_root>] [-e <exception_file>] -instance <instance> -db <meta_database>\n")
    sys.stderr.write("\n")
    sys.stderr.write("Rules file generation\n")
    sys.stderr.write("The first form creates a meta rules file from the information in the symtab.db\n")
    sys.stderr.write("The second form uses this meta database (and optional user exception file) to create an nCompare rules file\n")

def genMetaFile(args):
    if (len(args) == 0):
        dbName = "libdesign.symtab.db"
    else:
        dbName = args[0];
    
    ### Read DB
    if "CARBON_HOME" not in os.environ.keys():
        sys.stderr.write("CARBON_HOME is not set.  Set it before running makeRules\n")
        sys.exit(1)
        
    carbonHome = os.environ['CARBON_HOME']
    carbondb = os.path.join(carbonHome, "bin", "carbondb")
    
    db = os.popen (carbondb + " " + dbName, 'r')
    
    # design name is first line
    line = db.readline()
    design = string.strip(line)
    len_design = len(design)
    
    info = dbInfo(design)
    
    sig = []
    last_was_sig = 0
    
    print "Reading db file..."
    sys.stdout.flush()
    
    # Build a hierarchical representation of the design.
    # Each signal in the design contains a list of clocks
    # it will be checked on
    for line in db:
        line = string.strip(line)
        if (line == ""):
            break
        elif line[:len_design] == design:
            # filter temps
            if string.find(line, "$") == -1:
                sig = string.split(line[len_design+1:], ".")
                last_was_sig = 1
        elif last_was_sig:
            pieces = string.split(line)
            for clk in pieces:
                pos = string.rfind(clk, ")")
                if pos != -1:
                    if (clk[:len_design] == design):
                        clk = clk[len_design+1:pos]
                        info.addSignalClock(sig, clk)
            last_was_sig = 0
    
    db.close()
    
    metafile = open(design + ".stage0.rdb", "w")
    cPickle.dump(info,metafile)
    metafile.close()
    
    # Now we begin the optimization steps
    
    # Step 1 - Remove clocks from the tree
    # They'll be compared with an absolute compare, so they're not necessary
    
    print "Removing clocks..."
    sys.stdout.flush()
    info.removeClocks()
    
    metafile = open(design + ".stage1.rdb", "w")
    cPickle.dump(info,metafile)
    metafile.close()
    
    # Step 2 - Group common clock rules at their highest common hierarchy
    # (Initially there can be no exceptions)
    
    print "Grouping common rules..."
    sys.stdout.flush()
    info.groupRules()
    
    metafile = open(design + ".stage2.rdb", "w")
    cPickle.dump(info,metafile)
    metafile.close()
    
    # Step 3 - Try to find rules that are blocking us from grouping at
    # a higher level.  Mark these as exceptions and attempt to group again
    
    print "Finding rule exceptions..."
    sys.stdout.flush()
    info.findExceptions()
    print "Grouping common rules..."
    sys.stdout.flush()
    info.groupRules()
    
    metafile = open(design + ".rdb", "w")
    cPickle.dump(info,metafile)
    metafile.close()


def genNCompareRules(args):
    scope = ""
    instance = ""
    metafilename = ""
    exceptfilename = ""
    start = ""
    end = ""
    goldfile = "gold"

    # Parse args
    while len(args) > 0:
        curArg = args.pop(0);
        if curArg == "-s":  # specifing the scope
            scope = args.pop(0);
        elif curArg == "-start":
            start = args.pop(0)
        elif curArg == "-end":
            end = args.pop(0)
        elif curArg == "-instance":
            instance = args.pop(0);   
        elif curArg == "-gold":
            goldfile = args.pop(0);   
        elif curArg == "-db":
            metafilename = args.pop(0);   
        elif curArg == "-e":
            exceptfilename = args.pop(0);   
        else:
            sys.stderr.write("Unknown argument: " + curArg + "\n")
            printUsage()
            sys.exit(1)

    # Make sure all required options are there
    if (scope == ""):
        sys.stderr.write("No scope specified\n")
        sys.exit(1)
    if (instance == ""):
        sys.stderr.write("No instance specified\n")
        sys.exit(1)
    if (metafilename == ""):
        sys.stderr.write("No metafile database specified\n")
        sys.exit(1)
    if (exceptfilename != ""):
        sys.stderr.write("Exceptions not yet implemented\n")
        sys.exit(1)

    # Load the database

    print "Reading db file..."
    sys.stdout.flush()

    metafile = open(metafilename, "r")
    info = cPickle.load(metafile)
    metafile.close()

    #printfile = open("rules.txt", "w")
    #info.printMe(printfile)
    #printfile.close()

    # Open the rules file and dump to it
    rulesfile = open(info.design + ".ncr", "w")
    info.dumpNCompareRules(instance, goldfile, scope, start, end, rulesfile)
    rulesfile.close()


# main program

args = sys.argv;
args.pop(0)

if (len(args) == 0):
    printUsage()
    sys.exit(1)

mode = args.pop(0)

if (mode == "-m"):
    genMetaFile(args)
elif (mode == "-r"):
    genNCompareRules(args)
else:
    sys.stderr.write("First argument must be -m or -r.\n")
    printUsage()
    sys.exit(1)

