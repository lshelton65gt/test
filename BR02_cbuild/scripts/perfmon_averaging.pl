#!/usr/bin/perl

# returns a moving average, for the past

use strict;
use Getopt::Long;


my $file = ""; 
my $count = "5" ;

GetOptions ('file=s'      =>   \$file,
            'count=s'     =>   \$count,
           );



# get file info
if (! -e $file ) { die "ERROR: file: $file does not exist \n" ; }

my @lines = `tail -$count $file` ;
chomp @lines ;

# total the 3rd column, cpu speed

my $totalCPU = 0 ; 

foreach my $l (@lines) {

   my @temp = split /\s+/, $l ;
   $totalCPU = $totalCPU + $temp[2] ;
}

my $avg = $totalCPU / $count ; 

# format it
$avg = sprintf "%.2f", $avg ;

print "$avg\n" ; 

