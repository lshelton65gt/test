#!/usr/bin/perl

#-------------------------------------------------------------------
# This script uses CDSInstallJammer.pm which is in the repository
# under infrastructure/perl_modules.  The actual build command to 
# installjammer is in CDSInstallJammer::BuildInstall(). 
#
# CDSInstallJammer gets the path to installjammer from the
# CDSFileNames class.  CDSFileNames class has the installjammer
# version information in it.
#
# Each of these modules are a part of the QA process and so if
# you need to make enhancements/changes, please enlist the current
# QA/scripts guru for assistance.
#
# Basic flow:
#
# 1. Get options.
# 2. Check options
# 3. Make a CDSFileNames class to get access to common file and 
#    path names.
# 4. Make a CDSInstallJammer class, pass the filenames class to it.
# 5. Call the CDSInstalljammer::BuildInstall() method.
# 6. Exit with status.
#    
# Option descriptions described below in the PrintUsage() routine.
#------------------------------------------------------------------
use strict;
use Getopt::Long;
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSFileNames;
use CDSInstallJammer;

# Init this script's variables.
#------------------------------
my $installTree      = "";
my $executableName   = "";
my $objDir           = "";
my $platform         = "";
my $productNameLong  = "";
my $productNameShort = "";
my $helpMe           = 0;
my $exitStatus       = 1;

use vars qw ($installTree $executableName $helpMe $platform 
             $productNameLong $productNameShort);

# Parse arguments
#-----------------
GetOptions ('executable_name=s' => \$executableName,
            'help'              => \$helpMe,
            'install_tree=s'    => \$installTree,
            'long_name=s'       => \$productNameLong,
            'obj_dir=s'         => \$objDir,
            'platform=s'        => \$platform,
            'short_name=s'      => \$productNameShort);

# If they want help, print it.
#-----------------------------
if ($helpMe)
{
    PrintHelp();
}
# If they flubbed the args, print it, then print help.
#---------------------------------------------------
elsif ($Getopt::Long::error)
{
    sleep 2;
    PrintHelp();
}
# If they gave one name type but not the other, tell them
# we need both or niether.
#-------------------------------------------------------
elsif ($productNameLong && ! $productNameShort)
{
    print "\nIf you specify -long_name you have ",
    " to specify the -short_name product name as well.\n",
    "Please try again.\n\n";
}
# All good for a try....
#-----------------------------
else
{
    my $fn = new CDSFileNames();
    my $ijmr = new CDSInstallJammer($fn);

    if ($ijmr->BuildInstaller($executableName,
                              $platform,
                              $installTree,
                              $productNameLong,
                              $productNameShort, 
                              $objDir))
    {
        $exitStatus = 0;
    }
}

exit $exitStatus;


# Sub Routines
#==========================================================================

sub PrintHelp
{
    print STDOUT "\n\n",
    "======================================================================\n",
    " USAGE: $0 <options>\n\n",
    " NOTE: You can run this command with no options and take the\n",
    " defaults.  Defaults for each are shown below.\n\n\n",
    " Argument Possibilities:\n",
    "------------------------\n",
    "  -executable_name  <Full path produced installer>\n\n",
    "                    The full path and file name of where you \n",
    "                    want the final built installer to live.\n",
    "                    Default: \$CARBON_HOME/carbon-install-\`uname\`\n\n", 
    "  -platform         <Executable Platform Image>\n\n",
    "                    Only Linux or SunOS allowed.\n",
    "                    Default: \`uname\`\n\n",
    "  -install_tree     <Full path to install tree>\n\n",
    "                    Default: \$CARBON_HOME/install-tree\n\n",
    "  -obj_dir          <Full Path to Linux.product directory>\n\n",
    "                    Default: \$CARBON_HOME/obj/Linux.product\n\n",
    "  -long_name        <Long product designation>\n\n",
    "                    Default: A string comprised of \"CARBON\n",
    "                    Model Studio\" and the output of cbuild -verbose\n",
    "                    with some massaging to remove \"(\" and \")\" and \n",
    "                    replace spaces with \"_\".\n\n",
    "  -short_name       <Short product designation>\n\n",
    "                    Default: A string comprised of \"carbon-\"\n",
    "                    and the output of cbuild -verbose with some\n",
    "                    massaging to remove \"(\" and \")\" and replace\n",
    "                    spaces with \"_\.\n\n\n";

    $exitStatus = 0;
}
