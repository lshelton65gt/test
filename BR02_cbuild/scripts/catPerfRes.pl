#!/bin/sh
# -*-Perl-*-
exec $CARBON_HOME/bin/perl -S -x -- "$0" "$@"
#!perl

# script to reformat perf output

# meant to be called from inside DoPerfCheck

use strict ; 

use Getopt::Long;


use vars qw($baseFile $modFile $key $htmlArg $headersArg);
my @fields = qw(RunCPU RunMEM CmpCPU CmpMEM CmpVM Clocks Latch Gates Flatnd
                CostLo CostHi LibSize);

GetOptions (
            'base=s' =>   \$baseFile,
            'mod=s'  =>   \$modFile,
            'key=s'  =>   \$key,
	    'html'   =>   \$htmlArg,
            'headers' =>  \$headersArg,
           ) ;

if ( $Getopt::Long::error != 0 )
{
  print "ERROR: Bad option detected -- see msg above \n";
  exit 1;
}

if ( (! $baseFile) || (! $modFile)) { die "Usage: $0 -b <file1> -m <file2> \n"; } 

$key =~ tr/[a-z]/[A-Z]/ ;
my $Customer = "";
my $Design = "";
if ($key =~ /^(.+)\-([^\-]+)$/) {
    $Customer = $1;
    $Design   = $2;
} else {
    $Design   = $key;
}

#######################################
#
# get results, assemble into hash
# 

my $baseRes =  `cat $baseFile | tail -1`  ; 
my $modRes = `cat $modFile | tail -1` ; 

chomp $baseRes; $baseRes =~ s/^\s+//;
chomp $modRes; $modRes =~ s/^\s+//;

my @baseFigures = split /\s+/, $baseRes ; 
my @modFigures = split /\s+/, $modRes ; 

my %results = ();

# check that base, mod and fields all have same # elems. else set error str
my %error = ();
unless(scalar(@baseFigures) == scalar(@fields)) {
  $error{base} = "Functional Failure ($baseRes)";
}
unless(scalar(@modFigures) == scalar(@fields)) {
  $error{mod} = "Functional Failure ($modRes)";
}

# assemble hash
foreach my $field (@fields) {
  $results{base}{$field} = shift(@baseFigures);
  $results{mod}{$field} = shift(@modFigures);
  if(exists($error{base}) or exists($error{mod})) {
    $results{diff}{$field} = "n/a";
  } else {
    $results{diff}{$field} = CalcPCT($results{base}{$field},
				     $results{mod}{$field}); 
  }
}

##############################################
#
# print 
#
# print the header, either in html
if($htmlArg) {
  print "<TR><TH align=left width=10%>";
  if ($Customer) { print "$Customer<br/>"; }
  print "$Design</TH>";
  if($headersArg) {
    print "<TH>".join("</TH><TH>", @fields) . "</TH>\n";
  } else {
    print "<TH colspan=".scalar(@fields).">";
  }
  print "</TH></TR>\n";
# or txt
} else {
  print "$key\n"; 
  if ($headersArg) {
    print("      ");
    foreach my $field (@fields) {
      printf(" %7s", $field);
    }
    print "\n"
  }
}

# then print each type of data as a row
foreach my $type (qw(base mod diff)) {
  if (exists($error{$type})) {
    print "<TR><TD align=right>$type:</TD><TD colspan=" . scalar(@fields) . ">" if $htmlArg;
    print $error{$type};
    print "</TD></TR>" if $htmlArg;
    print "\n";
    next;
  }

  if ($htmlArg) {
    print "<TR><TD align=right>$type:</TD>";
    foreach my $field (@fields) {
      print "<TD align=right";
      # this could be smarter, but for now, make it red if its -5% or less
      if ($results{$type}{$field} <= -5) {
        print ' style="background-color: rgb(255, 102, 102);"';
      }
      print ">" . $results{$type}{$field} . "</TD>";
    }
    print "</TR>\n";
  } else {
    printf("%4s: ", $type);
    foreach my $field (@fields) {
      printf(" %7s", $results{$type}{$field});
    }
    print "\n";
  }
}

##############################################
sub CalcPCT {
 
  my $oldVal = shift @_ ; 
  my $newVal = shift @_ ; 

  # return 0 if either value is zero, to
  # avoid divide by zero errata
  
  if ( ($oldVal == 0) || ($newVal == 0) || ($newVal == $oldVal) ) {
     return "0.0%" ; 
  }


  # -100 is consistent with perf monitors
  my $pct = ($newVal - $oldVal) / $oldVal * -100 ; 

  # format it here
  $pct = sprintf "%3.1f", $pct ;
  $pct = "$pct%"; 

  return $pct ; 

}

