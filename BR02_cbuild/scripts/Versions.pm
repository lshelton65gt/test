package Versions;

use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSFileNames;
use CDSSystem;

sub new
{
    my $this = {};
    bless $this, @_[0];
    $this->{fileNames} = @_[1];
    $this->Initialize();
    
    return $this;
}

sub Initialize
{
    my $this = shift;

    if (! $this->{fileNames})
    {
        $this->{fileNames} = new CDSFileNames("","",$this);
    }

    my %vInfo = ();
    $this->{versionInfo} = \%vInfo;

    $vInfo{wine}          = "0.9.52-noopengl";
    $vInfo{installjammer} = "1.2.9";
    $vInfo{libxml}        = "2.6.26";
    $vInfo{perl}          = "5.8.5";
    $vInfo{python}        = "2.4.2";
    $vInfo{systemc}       = "2.2";
    $vInfo{mono}          = "2.10.8";
    $vInfo{gdiplus}       = "2.4";
    $this->QtVersion();
}

sub QtVersion
{
    my $this = shift;

    if (! $this->{versionInfo}{qtVersion})
    {
        my $qtVersionMak = join "/", $this->{fileNames}->FindSandboxPath(),
        "src/Makefile.qt-ver";
        my $gmake = $this->{fileNames}->Command("gmake");
        my $cmd = "$gmake -f $qtVersionMak echo_qt_version";
        
        $this->{versionInfo}{qtVersion} = CDSSystem::BackTick($cmd);

        chomp ($this->{versionInfo}{qtVersion});
    }

    return $this->{versionInfo}{qtVersion};
}

sub Version
{
    my $this = shift;
    my $item = shift;

    return $this->{versionInfo}{$item};
}


1;
