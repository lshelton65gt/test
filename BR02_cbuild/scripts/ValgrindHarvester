#!/bin/sh
# -*-Perl-*-
exec $CARBON_HOME/bin/perl -S -x -- "$0" "$@"
#!perl

###
###  ValgrindHarvester <starting-dir>
###    searches through a test directory after tests have been run with
###    valgrind and writes all valgrind errors to a log file

use strict;
use File::Find;
use File::Basename;
use Getopt::Long;
use lib dirname($0);
use CdsUtil;

# lines containing these strings will be ignored
my @gIgnoreLines = (
  "a memory error detector",
  "Julian Seward",
  "Using valgrind-",
  "Estimated CPU",
  "more details",
  "rerun with"
);
# look through any file ending in these strings for valgrind errors
my @logfileExts = (
  '.log',
  '.out',
);

#my @jsFiles = (
  #"/home/cds/pescatore/public_html/Treeview/ua.js",
  #"/home/cds/pescatore/public_html/Treeview/ftiens4.js"
#);

use vars qw($startDir @ignoreErrors $argHelp $outDir $htmlName);

GetOptions (
	'ignore=s@'	=> \@ignoreErrors,
	'dir=s'		=> \$startDir,
	'output=s'	=> \$outDir,
	'name=s'	=> \$htmlName,
        'help'		=> \$argHelp,
);

if ( $Getopt::Long::error != 0 ) {
  die &Usage;
}

if ($argHelp) { die &Usage; }

unless($startDir and (-d $startDir)) {

  my $HostArch;
  if (exists($ENV{'CARBON_TARGET_ARCH'})) {
    $HostArch = $ENV{'CARBON_TARGET_ARCH'};
  } else {
    $HostArch = `$ENV{'CARBON_HOME'}/bin/carbon_arch`
  }

  if ((! defined $ENV{'CARBON_TARGET'}) and (! defined $ENV{'CARBON_BIN'} )) {
    print "Note: \$CARBON_BIN is not set. Defaulting to product\n";
    $ENV{'CARBON_TARGET'} = "$HostArch.product";
  } elsif (! defined $ENV{'CARBON_TARGET'}) {
    $ENV{'CARBON_TARGET'} = $HostArch.".".$ENV{'CARBON_BIN'};
  }
    
  $startDir = $ENV{'CARBON_HOME'}."/obj/".$ENV{'CARBON_TARGET'};
}
my $dateStr = `date '+%y%m%d-%H%M'`;
my $dateLongStr = localtime(time);
chomp $dateStr;

# set default destination dir
unless($outDir) {
  $outDir = "vgErr-$dateStr";
}

if (-d $outDir) {
  die "Output directory $outDir already exists!\n" . &Usage();
}

# make sure startDir exists
unless (-d "$startDir/test") {
  die "Specified starting directory $startDir/test does not exit...\n";
}

# now process any ignore pattern args
foreach my $patt (@ignoreErrors) {
  print "Filtering \"$patt\" from output\n";
  #push @gIgnoreLines, $patt;
}

# now setup the dir
mkdir("$outDir");
chdir("$outDir");

my @logfiles = ();
my %vgErrors;

# get every *.cbld.log file under startDir
find(sub {
       foreach my $ext (@logfileExts) {
         if($File::Find::name =~ m#$ext$#) {
           push(@logfiles, $File::Find::name);
           last;
         }
       }
     },
  "$startDir/test");

# and cycle thru them
foreach my $logfile (@logfiles) {
  unless(open(LOG, $logfile)){ 
    print STDERR "can't open $logfile...\n"; next; 
  }
  my $errCount = 0;
  my @filteredLines = ();
  my $ignoringError = 0;
  my $ignoredErrorCount = 0;
  my $fetchingFatalError = 0;

  # this loop is kind of a mess, b/c we could have none, one or many vg errors
  # in any log file. we don't have a standardized way of knowing what log files
  # have cbuild output, and some may have output from many cbuild runs...
  foreach my $line (<LOG>) {
    # first, if we're ignoring an error, ignore it
    if($ignoringError) {
      if ($line =~ /^==[0-9]+==\s*$/) {
        $ignoringError = 0;
      }
      next;
    }

    # if we're getting all lines of a fatal error, just keep the line if it's
    # like --0123--, otherwise reset and continue processing the line
    if ($fetchingFatalError) {
      if ($line =~ /^\-\-[0-9]+\-\- /) {
	push @filteredLines, $line;
        next;
      } else {
	$fetchingFatalError = 0;
      }
    }

    # now, if the line starts with --0123--, it's a fatal error. can't skip
    # these and they probably won't have an error count, so fetch all lines
    # and inc the error counter
    if ($line =~ /^\-\-[0-9]+\-\- /) {
      push @filteredLines, $line;
      $errCount++;
      $fetchingFatalError = 1;
      next;
    }
 
    # we're looking for lines that begin like: ==01234==
    if ($line =~ /==[0-9]+==/) {
      my $ignore = 0;
      # ignore lines if they match the list above
      foreach my $patt (@gIgnoreLines) {
	if ($line =~ /$patt/) {
	  $ignore = 1;
	  last;
	}
      }

      # next, if this line contains an error msg that the user said
      # to ignore, we start ignoring it, and mark is as such
      foreach my $patt (@ignoreErrors) {
        if ($line =~ /$patt/) {
	  $ignoringError = 1;
	  $ignoredErrorCount++;
	  last;
        }
      }

      # put lines to keep in an array
      unless($ignore or $ignoringError) {
	#print $line;
        # parse error summary line to get errCount
        if($line =~ m/ERROR SUMMARY: \d+ errors from (\d+) contexts/) {
          $errCount += $1;
        }

	push @filteredLines, $line;
      }
    }
  }
  close(LOG);

  # now only continue with this log file if errors found
  if ($errCount > 0) {
    my $fullTestDir = dirname($logfile);
    my $relTestDir = $fullTestDir;
    $relTestDir =~ s#$startDir/?##;
    my $logfileBase = basename($logfile);
    # make the test dir if it doesn't exist
    unless(-d $relTestDir) {
      &CdsUtil::MkdirP($relTestDir);
      # copy test.results there
      if(-e "$fullTestDir/test.results") {
        &CdsUtil::CopyP("$fullTestDir/test.results", "$relTestDir/test.results");
      }
    }

    # write vg output to new log file
    open(LOGOUT, ">$relTestDir/$logfileBase") or 
      die "can't write $relTestDir/$logfileBase";
    foreach my $outline (@filteredLines) {
      print LOGOUT $outline;
    }

    # if we ignored any errors, print that:
    if($ignoredErrorCount > 0) {
      print LOGOUT "==0000==\n";
      print LOGOUT "==0000== ValgrindHarvester suppressed $ignoredErrorCount errors\n";
    }

    close(LOGOUT);
  
    # need to determine err count after filters, since we still want to report
    # on ignored errors, but don't want them treated as failures
    my $actualErrorCount = $errCount - $ignoredErrorCount;

    # add the test to tree like this:
      # $tree{testdir}->{'fails'} = 5
      # $tree{testdir}->{'file'} = "testdir/test.results"
      # $tree{testdir}->{logfile1}->{'errors'} = 2
      # $tree{testdir}->{logfile1}->{'file'} = "testdir/logfile1"
      # ..
      # $tree{testdir}->{logfile5}->{'errors'} = 3
      # $tree{testdir}->{logfile5}->{'file'} = "testdir/logfile5"

    if(exists($vgErrors{$relTestDir}->{'fails'}) and ($actualErrorCount > 0) ) {
      $vgErrors{$relTestDir}->{'fails'}++;
    } elsif ($actualErrorCount > 0) {
      $vgErrors{$relTestDir}->{'fails'} = 1;
      $vgErrors{$relTestDir}->{'file'} = "$relTestDir/test.results";
    } else {
      $vgErrors{$relTestDir}->{'fails'} = 0;
      $vgErrors{$relTestDir}->{'file'} = "$relTestDir/test.results";
    }

    $vgErrors{$relTestDir}->{$logfile}->{'errors'} =
      $errCount - $ignoredErrorCount;
    $vgErrors{$relTestDir}->{$logfile}->{'file'} =
      "$relTestDir/$logfileBase";

  } 
}

foreach my $filekey (sort(keys(%vgErrors))) {
  # print the test  name
  print "$filekey - ";
  # then print the failure count
  print "  ". $vgErrors{$filekey}->{'fails'} . " failures\n";
}

#foreach my $file (@jsFiles) {
  #&CdsUtil::CopyP($file, "./".basename($file));
#}
MakeIndexHtml("index.html", $htmlName);
MakeBrowseHtml("browse.html", $htmlName);
MakeTreeJs("vgerr.js", \%vgErrors);

sub MakeIndexHtml {
  my ($filename, $title) = @_;

  open(INDEXOUT, ">$filename") or die "can't create $filename";
  print INDEXOUT <<EndOfIndex;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head><meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
<title>$title</title>
</head>
<frameset rows="250,*">
<frame src="browse.html" name="treeframe">
<frame name="basefrm">
</frameset>
</html>
EndOfIndex

  close INDEXOUT;
}

sub MakeBrowseHtml {
  my ($filename, $title) = @_;

  open(BROWSEOUT, ">$filename") or die "can't create $filename";
  print BROWSEOUT <<EndOfBrowse;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head><meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
<style>
   BODY {background-color: white}
   TD {font-size: 10pt;
       font-family: verdana,helvetica;
           text-decoration: none;
           white-space:nowrap;}
   A  {text-decoration: none;
       color: black}
                                                                                
    .specialClass {font-family:garamond; font-size:12pt;color:green;font-weight:bold;text-decoration:underline}
</style>

<!-- Code for browser detection -->
<script src="/~pescatore/Treeview/ua.js"></script>
<!-- Infrastructure code for the tree -->
<script src="/~pescatore/Treeview/ftiens4.js"></script>
                                                                                
<!-- Execution of the code that actually builds the specific tree.
     The variable foldersTree creates its structure with calls to
         gFld, insFld, and insDoc -->
<script src="vgerr.js"></script>
</head>
<title>$title</title>
</head>
<body>
<h2>$title</h2>
<i>$dateLongStr</i>
<br/>
<!-- Removing this link will make the script stop from working -->
 <div style="position:absolute; top:0; left:0; "><table border=0><tr><td><font size=-2><a style="font-size:7pt;text-decoration:none;color:silver" href="http://www.treemenu.net/" target=_blank></a></font></td></tr></table></div> 
<script>initializeDocument()</script>
<noscript>JavaScript is disabled</noscript>
</body></html>
EndOfBrowse

  close BROWSEOUT;
}

sub MakeTreeJs {
  my ($filename, $vgErrTree) = @_;

  open(TREEJS, ">$filename") or die "Can't create $filename";

  # first print consts
  print TREEJS <<EndOfHeader;
// You can find instructions for this file here:
// http://www.treeview.net
                                                                                
// Decide if the names are links or just the icons
USETEXTLINKS = 1  //replace 0 with 1 for hyperlinks
                                                                                
// Decide if the tree is to start all open or just showing the root folders
STARTALLOPEN = 0 //replace 0 with 1 to show the whole tree
                                                                                
ICONPATH = '/~pescatore/Treeview/'
foldersTree = gFld("<i>Valgrind Errors</i>", "javascript:parent.op()")
EndOfHeader

  # cycle through tests
  foreach my $testName (sort(keys(%$vgErrTree))) {
    # print node for test
    print TREEJS "  aux1 = insFld(foldersTree, gFld(\"$testName (".
	$vgErrTree->{$testName}->{'fails'} . " failures)\", \"" .
        $vgErrTree->{$testName}->{'file'} . "\"))\n";
    # then cycle through logs for this test
    foreach my $log (sort(keys(%{$vgErrTree->{$testName}}))) {
      # skip fails and file
      if (($log eq 'fails') or ($log eq 'file')) { next; }
      print TREEJS "    insDoc(aux1, gLnk(\"R\", \"" .
            $vgErrTree->{$testName}->{$log}->{'file'} . " (" .
            $vgErrTree->{$testName}->{$log}->{'errors'} . " errors)\", \"" .
            $vgErrTree->{$testName}->{$log}->{'file'} . "\"))\n";
    }
  }

  close TREEJS;
}

sub Usage {
  my $scriptname = basename($0);
  return "Usage: $scriptname [-dir <startDir>] [-output <outputDir>] [-ignore <patt1> [-ignore <patt2> ...]] [-name <name>]\n";
}

