#!/bin/sh
# -*-Perl-*-
exec $CARBON_HOME/bin/perl -S -x -- "$0" "$@"
#!perl

use strict;
use Getopt::Long;
use File::Basename;
use lib dirname($0);
use CdsUtil;

my $design;#     = "dsp16k";
my $directory;#  = "dsp16k/test/cust/agere/dsp16k/carbon/cwtb/dsp16ks_core";
my $cbuildLog;#  = "dsp16k.cbld.log";
my $runLogT;#    = "dsp16k.run%d.log";
my $iterations;# = 12;
my $getRunMem = 0;#  = 1;
my $grepCpu = 0;
my $grepMem = 0;

GetOptions ('design=s'    =>   \$design, 
            'directory=s' =>   \$directory, 
            'cbuildLog=s' =>   \$cbuildLog, 
            'runLog=s'    =>   \$runLogT, 
            'iterations=i'=>   \$iterations, 
            'runmem'      =>   \$getRunMem,
	    'grepmem'     =>   \$grepMem,
	    'grepcpu'     =>   \$grepCpu );

die("Error: Use -design <design>\n") unless (defined($design));
die("Error: Use -directory <directory>\n") unless (defined($directory));
die("Error: Use -cbuildLog <log>\n") unless (defined($cbuildLog));
die("Error: Use -runLog <runTemplate>\n") unless (defined($runLogT));
die("Error: Use -iterations <i>\n") unless (defined($iterations));

die("Error: Missing directory: $directory\n") unless (-d $directory);
chdir $directory or die("Error: Could not chdir to $directory: $!\n");

my $cmpcpu = "n/a";
my $cmpmem = "n/a";
my $cmpvmem = "n/a";
my $clocks = "n/a";
my $costlo = "n/a";
my $costhi = "n/a";
my $latches = "n/a";
my $gates = "n/a";
my $libsize = "n/a";
my $flattened = "n/a";

if (-f $cbuildLog) {
    $cmpcpu = `GetPerfData cmpcpu $cbuildLog`;
    chomp $cmpcpu;
    $cmpmem = `GetPerfData cmpmem $cbuildLog`;
    chomp $cmpmem;
    $cmpvmem = `GetPerfData cmpvmem $cbuildLog`;
    chomp $cmpvmem;
} else {
    die("Error: Missing cbuild log file: $cbuildLog\n") unless (-f $cbuildLog);
}

# libdesign or lib$design for clocks info
if (-f "lib$design.clocks") {
  $clocks = `GetPerfData clocks lib$design.clocks`;
  chomp $clocks;
} elsif (-f "libdesign.clocks") {
  $clocks = `GetPerfData clocks libdesign.clocks`;
  chomp $clocks;
}

# libdesign or lib$design for latches info
if (-f "lib$design.latches") {
  $latches = `GetPerfData latches lib$design.latches`;
  chomp $latches;
} elsif (-f "libdesign.latches") {
  $latches = `GetPerfData latches libdesign.latches`;
  chomp $latches;
}

# libdesign or lib$design for gates info
if (-f "lib$design.costs") {
  $gates = `GetPerfData gates lib$design.costs`;
  chomp $gates;
} elsif (-f "libdesign.costs") {
  $gates = `GetPerfData gates libdesign.costs`;
  chomp $gates;
}

# libdesign or lib$design for flattening info
if (-f "lib$design.flattening") {
  $flattened = `GetPerfData flattened lib$design.flattening`;
  chomp $flattened;
} elsif (-f "libdesign.flattening") {
  $flattened = `GetPerfData flattened libdesign.flattening`;
  chomp $flattened;
}

# libdesign or lib$design for lib size, cost rations
if (-f "lib$design.a") {
  $libsize = `GetPerfData libdesignsize lib$design.a`;
  chomp $libsize;
  $costlo = `GetPerfData costlo lib$design.a`;
  chomp $costlo;
  $costhi = `GetPerfData costhi lib$design.a`;
  chomp $costhi;
} elsif (-f "libdesign.a") {
  $libsize = `GetPerfData libdesignsize libdesign.a`;
  chomp $libsize;
  $costlo = `GetPerfData costlo libdesign.a`;
  chomp $costlo;
  $costhi = `GetPerfData costhi libdesign.a`;
  chomp $costhi;
}

# get mem data from first run log.
my $runmem = "n/a";
my $runLog = sprintf($runLogT,1);
if (! -f $runLog) {
    warn("Warning: Missing run log file: $runLog\n");
} else {
    if ($getRunMem) {
	$runmem=`GetPerfData runmem $runLog`;
	chomp $runmem;
    } else {
	if ($grepMem) {
	    $runmem=`grep '^mem' $runLog | awk '{print \$2}'`;
	    chomp $runmem;
	}
    }
    if ($runmem !~ /^\s*[0-9]+(\.[0-9]+)?\s*$/) {
	warn("Warning: RunMEM for $runLog does not appear to be numeric: '$runmem'\n");
        $runmem = 'n/a';
    }

}


# now get cpu time
my @runcpus = ();
for (my $i=1; $i<=$iterations; ++$i) {
    my $runLog = sprintf($runLogT,$i);
    if (! -f $runLog) {
	warn("Warning: Missing run log file: $runLog\n");
    } else {
	my $oneruncpu;
	if ($grepCpu) {
	    $oneruncpu = `grep '^user' $runLog | awk '{print \$2}'`;
	} else {
	    $oneruncpu = `GetPerfData runcpu  $runLog`;
	}
	chomp $oneruncpu;
	if ($oneruncpu !~ /^\s*[0-9]+(\.[0-9]+)?\s*$/) {
	    warn("Warning: RunCPU for $runLog does not appear to be numeric: '$oneruncpu'\n");
	} else {
	    push(@runcpus, $oneruncpu);
	}
    }
}

print "$design Performance Summary\n";
print "runCPU  runMEM cmpCPU cmpMEM cmpVM  clocks  latch  gates  flatd costlo costhi    libsize\n";
print "========================================================================================\n";

# if num of iters not at least 1, usually a functional failure
if (scalar(@runcpus) < 1) {
  print "Functional Failure\n";
  die("Error: $design has no run logs, check for a functional failure\n") ;
}

my $minruncpu = &CdsUtil::min(\@runcpus);


$design =~ tr/a-z/A-Z/;

#print "\n";
printf "%6.2f %6s %6s %6s %6s %6s %6s %6s %6s %6s %6s %10s\n", $minruncpu, $runmem, $cmpcpu, $cmpmem, $cmpvmem, $clocks, $latches, $gates, $flattened, $costlo, $costhi,$libsize,;
