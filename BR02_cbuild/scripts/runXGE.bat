set BASENAME=%~n1
set CURRENTDIR=%~dp0

SET IBDIR=%ProgramFiles%\Xoreax\Incredibuild
IF "%ProgramFiles(x86)%" == "" GOTO PLAT32
SET IBDIR=%ProgramFiles(x86)%\Xoreax\Incredibuild
:PLAT32

IF NOT EXIST C:\temp mkdir c:\temp
IF NOT EXIST C:\temp\%BASENAME% mkdir c:\temp\%BASENAME%

@echo "TEMP=C:\temp\%BASENAME%"
SET TEMP=C:\temp\%BASENAME%
SET TMP=%TEMP%

@echo Environment
SET

@echo Starting job %*
"%IBDIR%\xgconsole.exe" /showagent /showcmd /showtime "%~dp0%1"
@set ES=%ERRORLEVEL%
@echo "Completed with Status %ES%"
rmdir /q /s C:\TEMP\%BASENAME%
rmdir /q /s "%CURRENTDIR%"
exit %ES%

