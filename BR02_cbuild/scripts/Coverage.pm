#!perl

package Coverage;

# 
# Coverage.pm
#  This package implements a wrapper around gcov that knows how to run gcov
# to assemble coverage statistics over all .cxx files. it can then output these
# statistics to html, text, or csv (NYI)
#
# to use this package is a 3-step process. first, instantiate the object
#    $cov = new Coverage($coverBool);
# then tell it to run coverage on some files (if no files are supplied, will
# use all cxx files under CARBON_HOME
#    $cov->generateCoverageInfo(\@filelist);
# then either call dumpHtml or dumpInfo or both to get a report 
#    $cov->dumpInfo();
#
# NOTE: To get a usage line for all routines in this package, grep for '^###'
# TODO: add .cpp, .c files. make list of suffixes to cover

use strict;
$| = 1;
my $Debug = 0;

use Carp;
use File::Find;
use File::Basename;
use Cwd;
use CdsTest;
use CdsUtil;

### \$CoverageObj = new Coverage ( $coverBranches )
# initialize Coverage arg and check environment. coverBranches is a boolean
# and will turn on branch analysis in gcov
sub new {
  my $class = shift;
  my $coverBranches = shift;

  my $self = {};

  # verify a few things before continuing
  my $gcov = "/o/tools/linux/gcc-3.4.5-a/bin/gcov";
  $self->{'_gcov'} = $gcov;
  unless(-e $gcov) {
    print STDERR "ERROR: $gcov does not exist!\n";
    return 0;
  }

  # make sure carbon home is set
  unless(exists($ENV{'CARBON_HOME'})) {
    print STDERR "ERROR: \$CARBON_HOME is not set!\n";
    return 0;
  }

  # check for CARBON_TARGET and/or CARBON_BIN
  unless(exists($ENV{'CARBON_TARGET'})) {
    if (exists($ENV{'CARBON_BIN'})) {
      my $Arch = `$ENV{'CARBON_HOME'}/scripts/carbon_arch`;
      chomp $Arch;
      $ENV{'CARBON_TARGET'} = $Arch.".".$ENV{'CARBON_BIN'};
    } else {
      print STDERR "ERROR: \$CARBON_TARGET or \$CARBON_BIN must be set!\n";
      return 0;
    }
  }

  # need obj dir to look for .gcda files
  my $objDir = $ENV{'CARBON_HOME'}."/obj/".$ENV{'CARBON_TARGET'};
  $self->{'_objdir'} = $objDir;
  unless(-d $objDir) {
    print STDERR "ERROR: $objDir does not exist!\n";
    return 0;
  }

  # should we run gcov on branches as well as lines?
  if($coverBranches) {
    $self->{'_coverBranches'} = 1;
  } else {
    $self->{'_coverBranches'} = 0;
  }

  # got the time?
  $self->{'_now'} = localtime(time);

  # initialize some counters to keep track of multi-file counts
  $self->{'_totallines'} = 0;
  $self->{'_lines'} = 0;
  $self->{'_totalbexec'} = 0;
  $self->{'_bexec'} = 0;
  $self->{'_totalbtaken'} = 0;
  $self->{'_btaken'} = 0;
  $self->{'_totalcalls'} = 0;
  $self->{'_calls'} = 0;

  # verbose on or off - default on
  $self->{'_verbose'} = 1;

  bless $self, $class;
  return $self;
}

### setVerbose()
# turn on verbose output 
sub setVerbose {
  my $self = shift;
  $self->{'_verbose'} = 1;
}

### setQuiet()
# turn on verbose output 
sub setQuiet {
  my $self = shift;
  $self->{'_verbose'} = 0;
}

### void = generateCoverageInfo([$filesRef])
# call gcov on each cxx file in the src tree and assemble a hash containing
# all coverage information. $filesRef should be ref to list of src files,
# relative to sandbox/src/. if $filesRef is empty or not supplied, will search
# sandbox and report on all cxx files.
sub generateCoverageInfo {
  my ($self, $filesRef) = @_; 

  print "Examining ".$self->{'_objdir'}."\n" if $self->_verbose();
  # check for non-empty list of files supplied
  if (not $filesRef or (scalar(@$filesRef) > 0)) {
    $self->{'_coverFiles'} = $filesRef;
  } else {
    # get list of all cxx files in src dir (without paths)
    $self->findCxxFiles();
  }
 
  my $branchCov; 
  if ($self->{'_coverBranches'}) {
    $branchCov = "-b";
  } else {
    $branchCov = "";
  }

  # go ahead and init the top level file hash ref and an interim function one
  my $fileHash = {};
  # foreach supplied file
  foreach my $cxxFile (@{$self->{'_coverFiles'}}) {

    # munge the filename to get gcda file
    $cxxFile =~ s#^\./##;
    my $daFile = $cxxFile;
    $daFile =~ s/cxx$/gcda/;
    $daFile =~ s/h$/gcda/;
    my $cxxStem = basename($cxxFile);
    my $cxxPath = $cxxFile;
    $cxxPath =~ s#/$cxxStem$##;

    # make sure gcda file exists, or that we're looking for a .h file
    # the assumption here is that if a gcda for a .h exists, that's great, but
    # otherwise, there likely wasnt any executable code in the .h, so skip it
    # there is the possibility that we'll miss some code this way, but the
    # other option is that we count TONS of lines against us that will never
    # (and can never) be covered)
    unless ((-f $self->{'_objdir'} . "/$daFile") or ($cxxFile =~ m/\.h$/)) {
      if ($Debug) { 
        print "Can't find file ".$self->{'_objdir'} . "/$daFile\n";
      }
      # since we couldn't find the gcda, get a line count for the source file
      # so that we can estimate how many lines we missed.
      my $fileLines = &CdsUtil::lineCount("src/".$cxxFile);
      # if couldn't find the file either, just skip it
      if($fileLines == -1) {
        print "Can't count lines for src/$cxxFile\n" if $Debug;
        next;
      }

      # lines that can be covered are typically around 75% of total
      $fileLines = int($fileLines * 0.75);

      # record the zeroes
      foreach my $type ('lines', 'bexec', 'btaken', 'calls') {
        $fileHash->{$cxxFile}->{$type} = 0;
        $fileHash->{$cxxFile}->{'total'.$type} = $fileLines;
      }
      next;
    }

    print "gcov'ing $cxxFile\n" if $self->_verbose();
    my $fetchingStats = 0;
    # call gcov -p -n -o objdir .cxx_file and read output
    open(GCOVOUT, $self->{'_gcov'}." -p $branchCov -o ".
      $self->{'_objdir'}."/$cxxPath $cxxStem 2>&1 |");
    foreach my $line (<GCOVOUT>) {

      # trim leading/trailing spaces
      $line =~ s#^ +##;
      chomp $line;

      # if this line contains the file name, set $fetchingStats and loop
      if($line =~ m#$cxxFile'#) {
	$fetchingStats = 1;
        next;
      } elsif ($line =~ m#^File #) {
        $fetchingStats = 0;
        next;
      }

      if ($fetchingStats) {
        # now we're looking for the stats for this file on lines like these:
        #Lines executed:48.42% of 983
        #Branches executed:48.13% of 482
        #Taken at least once:30.08% of 482
        #Calls executed:22.60% of 730

        my $type = "";
        # which is it?
        $type = 'lines' if ($line =~ /Lines executed/);
        $type = 'bexec' if ($line =~ /Branches executed/);
        $type = 'btaken' if ($line =~ /Taken at least once/);
        $type = 'calls' if ($line =~ /Calls executed/);

        # everything before ':' can now be discarded
        $line =~ s#^.*:##;
        # the first word is the percent, 3rd is total
        my (@pieces) = split(' ', $line);
        my $percentVal = $pieces[0] ? $pieces[0] : "0";
        $percentVal =~ s#\%##;
 
        # make hash of hashes mapping % to function to src file
        $fileHash->{$cxxFile}->{$type} = $percentVal;
        $fileHash->{$cxxFile}->{'total'.$type} = $pieces[2];

      }

    }

    close(GCOVOUT);
  }
  $self->{'_cov'} = $fileHash;
}

### void = dumpInfo()
# write all coverage information to stdout in plain text
sub dumpInfo {
  my $self = shift;
  my $cov = $self->{'_cov'};

  # what are we working with?
  my @fields = ("lines");
  if($self->{'_coverBranches'}) {
    push @fields, ("bexec","btaken","calls");
  }

  # loop thru each file that we have coverage data on
  foreach my $file (sort(keys(%$cov))) {
    print "## $file:\n";

    # set any missing percentages to -1
    foreach my $percentName (@fields) {
      unless(exists($cov->{$file}->{$percentName})) {
        $cov->{$file}->{$percentName} = -1;
      } else {
        # now update all the counters
        $self->{"_total$percentName"} += $cov->{$file}->{"total$percentName"};
        $self->{"_$percentName"} += int($cov->{$file}->{$percentName} * $cov->{$file}->{"total$percentName"} / 100);
      }
      my $total = $cov->{$file}->{'total'.$percentName};
      my $percent = $cov->{$file}->{$percentName};
      my $count = int($total * $percent / 100);

      print "  $percentName: ".$percent. "% (".$count."/".$total.")\n";
    }
  }

  print "## Total: (". scalar(keys(%$cov)) ." files)\n";
  foreach my $percentName (@fields) {
    my $percent = sprintf("%.2f", $self->{"_$percentName"} / $self->{"_total$percentName"} * 100);
    print "  $percentName: $percent% (". $self->{"_$percentName"}."/".
      $self->{"_total$percentName"}.")\n";
  }
}

### void = findCxxFiles()
# search in CARBON_HOME/src for all cxx files and save them in the member
# list _coverFiles
sub findCxxFiles {
  my ($self) = shift;
  my $startDir = cwd();
  chdir($ENV{'CARBON_HOME'}."/src/");
  print "Searching in ". $ENV{'CARBON_HOME'}."/src/\n" if $self->_verbose();
  my @cxxFiles = ();
  find ( sub { push @cxxFiles, $File::Find::name if((/\.cxx$/ or /\.h$/) and -f); } , ".");

  $self->{'_coverFiles'} = \@cxxFiles;
  print "found " . scalar(@cxxFiles) . " files\n" if $self->_verbose();
  chdir($startDir);
}

### void = dumpHtml ( $testName )
# write an html frames-based page with all coverage info to _outdir.
# testName will be noted in the document
sub dumpHtml {
  my $self = shift;
  my $testName = shift;
  my $dir = shift;

  my $pwd = cwd();
  my $cov = $self->{'_cov'};

  # make sure $dir does not exist
  if (-d $dir) { 
    print STDERR "ERROR: $dir already exists!\n";
    return 0;
  }
  $self->{'_outdir'} = $dir;
  unless(mkdir($dir) ) {
    print STDERR "ERROR: can't create $dir!\n";
    return 0;
  }

  # copy the runlist output to the coverage dir in case we need it
  if(-e $self->{'_objdir'}."/runlist.results") {
    &CdsUtil::CopyP($self->{'_objdir'}."/runlist.results",
      $self->{'_outdir'}."/runlist.results");
  }
  
  # first write the index page
  unless($self->_writeIndexHtml($testName)) {
    print STDERR "ERROR: unable to write ".$self->{'_outdir'}."/index.html\n";
    return 0;
  }

  # then write the browse page. start by opening it
  unless(open(HTMLOUT, ">".$self->{'_outdir'}."/browse.html")) {
    print STDERR "ERROR: unable to write ".$self->{'_outdir'}."/browse.html\n";
    return 0;
  }

  # write the header and everything up to the data
  print HTMLOUT $self->_strTreeFrameHeader($testName);

  # what are we working with?
  my @fields = ("lines");
  if($self->{'_coverBranches'}) {
    push @fields, ("bexec","btaken","calls");
  }

  # loop thru each file that we have coverage data on
  foreach my $file (sort(keys(%$cov))) {
    # start a row
    print HTMLOUT $self->_strStartRow();
    # format the name to be relative to a sandbox
    my $shortFile = $file;
    $shortFile =~ s#$pwd##;
    $shortFile =~ s#^/##;

    # assemble the destination of the link
    my $gcovFile = $file . ".gcov.html";
    $gcovFile =~ s/\//#/g;

    # move the gcov file into place
    my $gcovOut = $pwd; chomp $gcovOut;
    my $cxxOut = "$shortFile.gcov";
    $gcovOut =~ s/\//#/g;
    $cxxOut =~ s/\//#/g;

    # if the file exists, move it
    if (-e "./$gcovOut#src#$cxxOut") {
      $self->convertToHtml("./$gcovOut#src#$cxxOut",
                           $self->{'_outdir'}."/$cxxOut.html") or
        print "Unable to write ".$self->{'_outdir'}."/$cxxOut.html\n";
      unlink("./$gcovOut#src#$cxxOut");
      #system("mv ./$gcovOut#src#$cxxOut ". $self->{'_outdir'}."/$cxxOut.txt");
    # otherwise, try to get a copy of the source file
    } elsif (-e "src/$file") {
      $self->convertToHtml("src/$file", $self->{'_outdir'}."/$cxxOut.html") or
        print "Unable to write ".$self->{'_outdir'}."/$cxxOut.html\n";
      #system("cp src/$file ". $self->{'_outdir'}."/$cxxOut.txt");
    } elsif ($Debug) {
      print "Unable to find ./$gcovOut#src#$cxxOut or src/$file\n";
    }

    # print the file cell
    print HTMLOUT $self->_strFileNameCell($shortFile, $gcovFile);

    # if an error occurred, print a default error
    if($cov->{$file}->{'error'}) {
      print HTMLOUT $self->_strError();
    # otherwise, print a cell for each metric
    } else {

      # set any missing percentages to -1
      foreach my $percentName (@fields) {
        unless(exists($cov->{$file}->{$percentName})) {
	  $cov->{$file}->{$percentName} = -1;
        } else {
          # now update all the counters
          $self->{"_total$percentName"} += $cov->{$file}->{"total$percentName"};
          $self->{"_$percentName"} += int($cov->{$file}->{$percentName} * $cov->{$file}->{"total$percentName"} / 100);
        }
      }

      print HTMLOUT $self->_strPercentCell( $cov->{$file}->{'lines'} ,
                                            $cov->{$file}->{'totallines'} );
      if($self->{'_coverBranches'}) {
        print HTMLOUT $self->_strPercentCell( $cov->{$file}->{'bexec'} ,
                                              $cov->{$file}->{'totalbexec'} );
        print HTMLOUT $self->_strPercentCell( $cov->{$file}->{'btaken'} ,
                                              $cov->{$file}->{'totalbtaken'} );
        print HTMLOUT $self->_strPercentCell( $cov->{$file}->{'calls'} ,
                                              $cov->{$file}->{'totalcalls'} );
      }

    }

    # end the row
    print HTMLOUT $self->_strEndRow();
  }

  # print the 'Total' cell
  print HTMLOUT $self->_strFileNameCell('Total ('.scalar(keys(%$cov)).
    ' files)');
  # then print the totals
  foreach my $percentName (@fields) {
    print HTMLOUT $self->_strTotalCell( $self->{"_$percentName"},
				  	  $self->{"_total$percentName"} );
  }
  # end the row
  print HTMLOUT $self->_strEndRow();

  # end the document
  print HTMLOUT $self->_strTreeFrameFooter();
  close(HTMLOUT);
}

# given a text file, wrap it in html headers and convert ##### lines to
# reverse video
sub convertToHtml {
  my ($self, $inFile, $outFile) = @_;

  open(IN, $inFile) or return 0;
  open(OUT, ">$outFile") or return 0;

  print OUT <<EndOfHeader;
<html><head>
  <style>div {font-weight:bold; color:white; background:red;}</style>
</head><body bgcolor=white><pre>
EndOfHeader

  while(<IN>) {
    s/\&/\&amp;/g;
    s/</\&lt;/g;
    s/>/\&gt;/g;
    if (/^\s*#####/) {
      chomp;
      print OUT "<div>$_</div>";
    } else {
      print OUT $_;
    }
  }
  close(IN); close(OUT);
  return 1;
}

##
## Internal subroutines
##

### $success = _writeIndexHtml ( $testName )
# write the top level HTML index of the frames
sub _writeIndexHtml {
  my $self = shift;
  my $testName = shift;

  # name
  my $whatIsThis = "";
  if($testName) {
    $whatIsThis = " for $testName";
  }

  # write it to the file
  open(HTMLOUT, ">".$self->{'_outdir'}."/index.html") or return 0;
  print HTMLOUT <<EndOfIndexHtml;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head><meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
<title>Code Coverage$whatIsThis</title></head>
<frameset rows="250,*">
                                                                                
<frame src="browse.html" name="browserframe">
<frame name="detailframe">
                                                                                
</frameset>
</html>
EndOfIndexHtml
  close(HTMLOUT);
  return 1;
}  

### $string = _strTreeFrameHeader ( $testName )
# return a string containing the beginning of the html file that will have
# the coverage information
sub _strTreeFrameHeader {
  my $self = shift;
  my $testName = shift;

  # name
  my $lWhatIsThis = "";
  if($testName) {
    $lWhatIsThis = " for $testName";
  }
  # now
  my $lNow = $self->{'_now'};

  # branches?
  my $topRow;
  if ($self->{'_coverBranches'}) {
    $topRow =<<EndOfRow;
<tr>
  <th width=40%>File</th>
  <th width=15% colspan=2>Source Lines Exec'd</th>
  <th width=15% colspan=2>Branches Exec'd</th>
  <th width=15% colspan=2>Branches Taken</th>
  <th width=15% colspan=2>Calls Exec'd</th>
</tr>
EndOfRow
  } else {
    $topRow =<<EndOfRow;
<tr>
  <th>File</th>
  <th colspan=2>Source Lines Exec'd</th>
</tr>
EndOfRow
  }

  # return it
  my $lRetStr =<<EndOfHeader;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head><meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1">
<title>Coverage Stats$lWhatIsThis</title></head>
<body>
<h2>Coverage Information$lWhatIsThis</h2>
<i>$lNow</i>
<br>
<table border=1 width=100%>

$topRow

EndOfHeader
  return $lRetStr;
}

### $string = _strTreeFrameFooter ( )
# return a string containing the end of the html frame containing the coverage
# info table
sub _strTreeFrameFooter {
  my $self = shift;

  return <<EndOfFooter;
</table></body></html>
EndOfFooter
  }

### $string = _strFileNameCell ( $cxxFile, $gcovFileLink )
# return a string containing the html table cell for the cxx/gcov file
sub _strFileNameCell {
  my $self = shift;
  my $cxxFile = shift;
  my $gcovFile = shift;

  my $lRetStr;

  if($gcovFile) {

    $gcovFile =~ s/#/%23/g;
    $lRetStr =<<EndOfLine;
  <td><a href="$gcovFile" target="detailframe">$cxxFile</a></td>
EndOfLine

  } else {
    $lRetStr =<<EndOfLine;
  <td>$cxxFile</td>
EndOfLine

  }

  return $lRetStr;
}

### $string = _strPercentCell ( $percent, $total )
# return a string containing the html table cell for a percent and count
sub _strPercentCell {
  my $self = shift;
  my $percent = shift;
  my $total = shift;

  my $lRetStr;

  # if percent is -1, means there was none of this (branches, probably)
  if($percent == -1) {
    $lRetStr =<<EndOfLine;
  <td align=right>--</td><td align=right>(0/0)</td>
EndOfLine

  } else {

    my $count = int($total * $percent / 100);

    $lRetStr =<<EndOfLine;
  <td align=right>$percent\%</td><td align=right>($count/$total)</td>
EndOfLine

  }

  return $lRetStr;
}

### $string = _strTotalCell ( $count, $total )
# return a string containing the html table cell for a total count
sub _strTotalCell {
  my $self = shift;
  my $count = shift;
  my $total = shift;

  my $lRetStr;

  # if total is <1, means there was none of this (branches, probably)
  if($total < 1) {
    $lRetStr =<<EndOfLine;
  <td align=right>--</td><td align=right>(0/0)</td>
EndOfLine

  } else {

    my $percent = int(100 * $count / $total);

    $lRetStr =<<EndOfLine;
  <td align=right>$percent\%</td><td align=right>($count/$total)</td>
EndOfLine

  }

  return $lRetStr;
}
  

### $string = _strError ( )
# return a string containing the html table cell for a file error
sub _strError {
  my $self = shift;

  return "<td align=center colspan=8><font color=red>ERROR PARSING GCOV OUTPUT</font></td>\n";
}

### $string = _strStartRow ( )
# return a string containing the starting tag for an html table row
sub _strStartRow {
  my $self = shift;

  return "<tr>\n";
}

### $string = _strStartRow ( )
# return a string containing the ending tag for an html table row
sub _strEndRow {
  my $self = shift;

  return "</tr>\n";
}

## $bool = _verbose()
# return if verbose logging is on or off
sub _verbose {
  my $self = shift;
  return  $self->{'_verbose'};
}

# END
1;
