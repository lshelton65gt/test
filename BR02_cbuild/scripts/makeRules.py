#!/usr/bin/python

import sys
import string
import os

makeRules = 1
filterTmp = 0
forceWrite = 0
nCompare = 0

# scope = sys.argv[1]

scope = ""
#### Get Cmd Line
args = sys.argv;

if len(args) == 1:
    sys.stderr.write( "usage:\n")
    sys.stderr.write(sys.argv[0])
    sys.stderr.write(" -s <scope> [-start <time>] [-end <time>]")
    sys.stderr.write(" [-noTemp] [-f] -instance <instance> [-db <filename>] [-nCompare]\n")
    sys.stderr.write("\nThis program writes a comparescan rules file to\n")    
    sys.stderr.write("the file <modulename>.rules  Existing files are\n")
    sys.stderr.write("not overwritten unless -f (force) is specified\n\n")
                     
    sys.exit(1)
    

startArg = "+0ps"
endArg = "-0ps"
instanceName = "design"
goldName = "gold"
dbName = "libdesign.symtab.db"

args.pop(0);
while len(args) > 0:
    curArg = args.pop(0);
    if curArg == "-s":  # specifing the scope
        scope = args.pop(0);
    elif curArg == "-r":  # specifing the VCD filename
        makeRules = 1
    elif curArg == "-start":
        startArg = args.pop(0)
    elif curArg == "-end":
        endArg = args.pop(0)
    elif curArg == "-noTemp":
        filterTmp = 1
    elif curArg == "-f":
        forceWrite = 1;   
    elif curArg == "-instance":
        instanceName = args.pop(0);   
    elif curArg == "-gold":
        goldName = args.pop(0);   
    elif curArg == "-db":
        dbName = args.pop(0);   
    elif curArg == "-nCompare":
        nCompare = 1   
    else:
        sys.stderr.write(" UNKNOW ARGUMENT\n" + curArg)
        sys.exit(1)

        
### Read DB
#carbonHome = os.getenv("CARBON_HOME")
if "CARBON_HOME" not in os.environ.keys():
    sys.stderr.write("CARBON_HOME is not set.  Set it before running makeRules\n")
    sys.exit(1)
    
carbonHome = os.environ['CARBON_HOME']
carbondb = os.path.join(carbonHome, "bin", "carbondb")

iodb = os.popen (carbondb + " " + dbName, 'r')
lines = iodb.readlines()

if iodb.close() != None:
    sys.stderr.write ( "Error running carbondb\n")
    sys.stderr.write("Make sure that " + carbondb + " exists!\n")
    sys.exit(1)

designName = ""
if len(lines) > 0:
    designName = string.strip(lines.pop(0))

clks = []
inputs = []
outputs = []
signals = []
unsched = []
nCompareClks = []


# I want to read the end first so that I get the
# Clocks, Inputs, and Outputs identified before looking
# at all of the signals
lines.reverse()

didClks = 0
idx = 0
for line in lines:
    line = string.strip(line)
    if line[:len("Input:")] == "Input:":
        spline = string.split(line)
        inputName = spline[1][len(designName) + 1:]
        inputs.insert(0, inputName)
    elif line[:len("Output:")] == "Output:":
        spline = string.split(line)
        outputName = spline[1][len(designName) + 1:]
        outputs.insert(0, outputName)
    elif line[:len("Clocks:")] == "Clocks:":
        didClks = 1
        spline = string.split(line)
        clkName = spline[1][len(designName) + 1:]
        clks.insert(0, clkName)
        signals.insert(0, [])
    elif didClks:
        # Done with this part
        break
    
    idx = idx+1

lines = lines[idx+1:]
lines.reverse()

#print designName

idx = 0;
for line in lines:
    skip = 0
    if idx == (len(lines) -1):
        idx = (len(lines) - 2)
        
    line = string.strip(line)

    #  Four types of lines: signals, ins, outs and clocks
    
    if line[:len(designName)] == designName:
        
        # used to be that the schedule info was on the same line
        # now it's on the next line.  Or, it's not there it all
        sigName = line[len(designName) + 1:]

#        print sigName


        if filterTmp and string.find(sigName, "$") != -1:        
            print "skipping", sigName
            skip = 1
        
        nxtLine = lines[idx+1]

        # If the next line starts with the design name, there was no sched
        # listed...
        if nxtLine[:len(designName)] == designName:
            unsched.append(spline[0][len(designName) + 1:])
            skip = 1
        else:
            nxtLine = string.strip(nxtLine)
            spline = string.split(nxtLine)
        
#        spline = string.split(line)

#        if len(spline) == 1:
#            unsched.append(spline[0][len(designName) + 1:])
            
#        sigName = spline[0][len(designName) + 1: len(spline[0]) -1]

        wordIsClk = 0
        if not skip:
            for word in spline:
                if wordIsClk:
                    clk = word[len(designName) + 1:]
                    pos = string.rfind(clk, ")")
                    if pos != -1:
                        clk = clk[:pos]
                    if clk not in clks:
                        print "Couldn't find clock:", clk, "input:", line
                        sys.exit(1)
                    else:
                        pos = -1
                        for i in range(len(clks)):
                            if clks[i] == clk:
                                pos = i
                        if pos == -1:
                            print "error processing:", line
                            sys.exit(1)
                        if sigName not in signals[pos]:
                            signals[pos].insert(0,sigName)
                    wordIsClk = 0    
                elif word[len(word) - len("posedge"):] == "posedge" or word[len(word) - len("negedge"):] == "negedge":
                    wordIsClk = 1

    idx = idx + 1;

### End of reading db file

### Create the set function
inputList = inputs
clkName = clks

############  Try to make a rules file for this block
# won't overwrite rules files that already exist
# * creates compares for clock signals,
# * clkcompares for other all other signals
# * All clock compares are delayed 15ns and based on The first clk in the list
#   This will work great for blocks with only pclk, but probably not so great
#   with stuff with more than one clock or a different clock (the 15ns will have to
#   change then.
#
if makeRules:
    if forceWrite == 0:
        if designName + ".rules" in os.listdir("."):
            makeRules = 0
            sys.stderr.write("Warning: rules files exists, not over writing\n")
            sys.stderr.write("         if you wish to overwrite the rules file\n")
            sys.stderr.write("         use the -f switch\n")
        if nCompare and designName + ".ncr" in os.listdir("."):
            makeRules = 0
            sys.stderr.write("Warning: nCompare rules files exists, not over writing\n")
            sys.stderr.write("         if you wish to overwrite the rules file\n")
            sys.stderr.write("         use the -f switch\n")
    elif forceWrite == 1:
        if designName + ".rules" in os.listdir("."):
            sys.stderr.write("Warning: Overwriting rules file\n")                
        if nCompare and designName + ".ncr" in os.listdir("."):
            sys.stderr.write("Warning: Overwriting nCompare rules file\n")                
    
if makeRules: # and (designName + ".rules") not in os.listdir("."):    
    ruleFile = open(designName + ".rules", 'w')
    ruleFile.write("datafile1 ../../../customer/" + goldName + ".trn -maxerrors 1 -totalerrors 1000000\n")
    ruleFile.write("datafile2 " + instanceName + ".vcd\n")
    ruleFile.write("report -r2 -values " + instanceName + ".cmp.log \n\nstatemapping GoldenXMatchesAll \\\n")
    ruleFile.write("  01xz \\\n0:1000 \\\n1:0100 \\\nx:1111 \\\nz:1111 \n\n")
    
    if nCompare:
        ncrRuleFile = open(designName + ".ncr", 'w')
        ncrRuleFile.write("#cmpToFsdb " + instanceName + ".vcd " + instanceName + ".fsdb\n")
        ncrRuleFile.write("cmpOpenFsdb " + goldName + ".fsdb " + instanceName + ".fsdb\n")
        ncrRuleFile.write("cmpSetReport " + instanceName + ".nce\n")
        ncrRuleFile.write("cmpSetCmpOption -MaxError 1000000 -MaxErrorPerSignal 1\n")
        ncrRuleFile.write("cmpSetStateMap -asym (0,0,T) (0,1,F) (0,x,F) (0,z,F) (1,0,F) (1,1,T) (1,x,F) (1,z,F) (z,*,T) (x,*,T)\n")
        ncrRuleFile.write("cmpSetCmpOption -X T -Z T -Time (" + startArg + ", " + endArg + ")\n\n")

    for clk in clks:
        ruleFile.write( "compare  " + scope + "." + clk + " " + designName + "." + clk + " -start " + startArg + " -statemapping GoldenXMatchesAll \n")
        if nCompare:
            ncrRuleFile.write("cmpSetSignalPair " + scope + "." + clk + " " + designName + "." + clk + "\n")
    
    i = 0;
    for clk in clks:
#        if clk in inputs:
        ruleFile.write("\nclockdef clk" + str(i) + " " + scope + "." + clk + "\n")                   
        ruleFile.write("clockdef clk" + str(i+1) + " " + designName + "." + clk + "\n\n")
        if nCompare:
            nCompareClks.append(scope + "." + clk)
            nCompareClks.append(designName + "." + clk)
        i = i + 2
    
    numSigs = 0;
    
    i = 0
    for sigset in signals:
    
        numSigs = numSigs + len(sigset)
        if nCompare:
            ncrRuleFile.write("\n")
            ncrRuleFile.write("cmpSetCmpTrigger GOLDEN -Clock " + nCompareClks[i] + " -Setup 0 -Hold 0 -Sample 10ps\n")
            ncrRuleFile.write("cmpSetCmpTrigger SECONDARY -Clock " + nCompareClks[i+1] + " -Setup 0 -Hold 0 -Sample 10ps\n")
            ncrRuleFile.write("\n")
        for sig in sigset:
            if sig not in clks:
                ruleFile.write("clkcompare clk" + str(i) + " " + scope + "." + sig + " clk" + str(i+1) + " " + designName + "." + sig + " \\\n")
                ruleFile.write(" -start " + startArg + " -end " + endArg + " -statemapping GoldenXMatchesAll -sample1 -0ps -sample2 +10ps\n")
                if nCompare:
                    ncrRuleFile.write("cmpSetSignalPair " + scope + "." + sig + " " + designName + "." + sig + "\n")
    
        i = i + 2
    
    if nCompare:
        ncrRuleFile.write("\n")
        ncrRuleFile.write("set num_errors [cmpCompare]\n")
        ncrRuleFile.write("if {$num_errors} {\n")
        ncrRuleFile.write("   # Call a bogus command - this is the only way I've been able to make\n")
        ncrRuleFile.write("   # the script both dump an error report AND return bad status\n")
        ncrRuleFile.write("   bogus\n")
        ncrRuleFile.write("}\n")
    
    ruleFile.close();
    if nCompare:
        ncrRuleFile.close()
    
    print "\n/* Wrote rules for", numSigs, "signals.  ", len(unsched), " nets ignored */"
 
 
 
