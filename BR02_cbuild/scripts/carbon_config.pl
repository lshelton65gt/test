#!/usr/bin/perl

use Cwd;
use File::Path;
use lib "$ENV{CARBON_SANDBOX}/scripts";
use CarbonConfig;
use CarbonConfigArgs;
use lib "$ENV{CARBON_SANDBOX}/scripts/perl_modules";
use CDSSystem;
use CDSHosts;
use CDSFileNames;


my $PASS    = 0;
my $exitRtn = 0;

# Removing defaults to product.
my $origCarbonBin=$ENV{CARBON_BIN};
delete $ENV{CARBON_BIN};

my $hostInfo  = new CDSHosts();
my $fileNames = new CDSFileNames();
my $args      = new CarbonConfigArgs($fileNames);

#------------------------------------------------------------------------------
# We absolutely must understand CARBON_HOME to do anything accurately
#------------------------------------------------------------------------------
if (! $ENV{CARBON_HOME})
{
    print STDERR "No \$CARBON_HOME is set\n";
    $exitRtn = 1;
}
elsif ($args->Target() =~ /winx|icc/)
{
    my $winx = "";
    my $oldConfig = $0;

    $oldConfig =~ s/\.pl//;

    my $cmd = join " ", $oldConfig, (join " ", @{$args->ScriptArgs()});

    $ENV{USE_OLD_CONFIG} = 1;
        
    print STDOUT "Using old carbon_config for ", $args->Target(), 
    " configuration\nRunning $cmd\n";
    
    $exitRtn = CDSSystem::System ("$cmd");
}
else
{
    my $config    = new CarbonConfig($hostInfo, $fileNames, $args);
    print STDOUT "Targeting ", $args->Target(), " configuration.\n";
    $exitRtn = $config->Run();

#------------------------------------------------------------------------------
# Done with setup.  If we were successful and the user wants to launch 
# scripts/mkcds from here, then do it.
#------------------------------------------------------------------------------
    
    if ($args->LaunchMkcds() && $exitRtn == 0)
    {
        $exitRtn = LaunchMkcds();
    }
    else
    {
        #-------------------------------------------------------------------------
        # Hang around for anything we forked. This just keeps messaging clean. Else
        # we'd print the exit message, then later something forked might print as
        # well.  Doesn't break anything, but doing that will no doubt draw an email
        # or two, which I can do without.
        #-------------------------------------------------------------------------
        
        $config->WaitForProcess();
        
        print STDOUT "Exit [$exitRtn]: $errMsg[$exitRtn]\n";
    }
}

exit $exitRtn;


sub LaunchMkcds
{
    my $args = $args->MkcdsArgs();
    my $mkcds = join "/", $fileNames->ScriptsDir(), "mkcds ";
    $mkcds .= $args;

    my $rtn = 2;


    if (CDSSystem::Cd($ENV{CARBON_SANDBOX}))
    {
	print "Starting $mkcds ...\n";
        $ENV{CARBON_BIN} = $origCarbonBin;
	$ENV{CARBON_HOME} = $fileNames->UseMountedPath($ENV{CARBON_HOME});
	$rtn = CDSSystem::System ($mkcds);
    }

    print "$mkcds exit return was $rtn\n";

    return $rtn;
}
