#!/bin/sh
# -*-Perl-*-
PATH=$CARBON_HOME/bin:$PATH
LANG=C
export LANG
exec perl -S -x -- "$0" "$@"
#!perl

# ReturnMin.pl
#  takes a list of values and returns the smallest
# 
# Limitiations
#  there is no error checking. user beware

use strict;
use warnings;

my $minValue = "" ;

sub usage{
    die "returnMin.pl value1 value2 .. valueN \n" ; 
}

if ($#ARGV == -1) { usage(); }

foreach my $curVal (@ARGV) {

  if ( $minValue ) {
     if ($curVal < $minValue) { $minValue = $curVal ; }
  }
  else { $minValue = $curVal ; }

}

print "$minValue\n" ;
