#!/bin/bash
#
# Run the visual c linker via command line on a remote host.

# error logging
logit(){
    echo $* >&2
}

error(){
    logit $*
    exit 1
}

# convert a Unix path to a windows path 
fixpath()
{
OHOME=/home/cds/
NHOME=//Carbon/
FPATH=${1/$OHOME/$NHOME}

SLASH=/
BS=\\
FPATH=${FPATH/${SLASH}\.${SLASH}/${SLASH}}
echo ${FPATH//${SLASH}/${BS}${BS}}
}

function addarg() {
    args[$argsOut]="$1"
    argsOut=$((argsOut+1))

}

declare -a args
declare -i argsOut=0
declare -i argsIn=$#

while [ $argsIn -gt 0 ]; do
  argsIn=$((argsIn-1))
  arg=${1}
  shift
    
  case $arg in
      -o)
	  addarg /OUT:$(fixpath $1)
	  shift
	  argsIn=$((argsIn-1))
	  ;;

      -L*)
	  addarg /LIBPATH:$(fixpath ${arg#-L})
	  ;;

      -l*)
	  addarg lib${arg#-l}.lib
	  ;;

      -Wl,*)
	  # Ignore linker directives for now...
          ;;

      [a-zA-Z0-9/.]*)
          # file names
          arg=${arg/.dll/.lib}
	  addarg $(fixpath ${arg})
	  ;;

      -*)
	  error "$0: untranslated flag $arg"
	  ;;
  esac
done



# Location to find MS Visual toolsrem
MSVCRoot="/cygdrive/c/PROGRA~1/MICROS~4"
VSCommonDir=${MSVCRoot}/Common
MSDevDir=${MSVCRoot}/Common/msdev98
MSVCDir=${MSVCRoot}/VC98

ADDTOPATH=${MSDevDir}/BIN:${MSVCDir}/BIN:${VSCommonDir}/TOOLS/WINNT:${VSCommonDir}/TOOLS

#ADDTOINCLUDE="$MSVCDir/ATL/INCLUDE $MSVCDir/INCLUDE $MSVCDir/MFC/INCLUDE"

# Add extra paths needed for linking

ADDTOLIBPATH="${CARBON_HOME}/Win/lib ${CARBON_HOME}/Win/lib/winx ${CARBON_HOME}/Win/winx/i386-mingw32msvc/lib ${CARBON_HOME}/Win/winx/lib/gcc-lib/i386-mingw32msvc/3.4.3-a $MSVCDir/LIB $MSVCDir/MFC/LIB"

for l in $ADDTOLIBPATH; do
    addarg /LIBPATH:$(fixpath ${l})
done

# And extra libraries to search.
EXTRALIBS="libmingw32.lib libmoldname.lib libmingwex.lib libgcc.lib libstdc++.lib libmsvcrt.lib libuser32.lib libkernel32.lib"
for l in $EXTRALIBS; do
    addarg $l
done

WINMACHINE=`echo $WINDOWS_TEST_HOST |sed -e 's,:.*,,'`
logit executing on $WINMACHINE

# now determine the dir to go to run the test
GOTODIR=`pwd | sed -e 's,/home/cds,//carbon,'`

logit adding $ADDTOPATH to PATH
logit will cd to $GOTODIR
logit will execute LINK ${args[@]}


# Tell what we're going to do
echo LINK ${args[@]}

# execute it!
exec ssh $WINMACHINE "export PATH=$ADDTOPATH:\$PATH; \
  cd $GOTODIR; \
  LINK ${args[@]} 2>&1"
