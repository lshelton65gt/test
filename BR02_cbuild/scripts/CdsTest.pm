#!perl

package CdsTest;
use strict;
use Cwd;

# NOTE: To get a list of all routines in this package, grep for '^###'

use File::Copy;
use CdsUtil;

my $gDebug = 0;
# maybe turn debug on?
if ( $ENV{'RUNLIST_DEBUG'} )   { $gDebug = 1 ; }

### $statusString = &VerifyUserEnv($debug)
# check a bunch of stuff that is needed for runlist/runsingle to work
# properly. return "" if all is a-ok. return error if not
sub VerifyUserEnv {
  my ($dbgArg) = @_;

  if ($dbgArg) { print "\$CARBON_HOME: ". $ENV{'CARBON_HOME'}."\n"; }
  unless($ENV{'CARBON_HOME'}) {
    return "ERROR: \$CARBON_HOME is not set\n";
  }

  unless(-e "$ENV{'CARBON_HOME'}/bin/cbuild" )  {
    return "ERROR: \$CARBON_HOME/bin/cbuild can not be found\n".
      "ERROR: Looked for: $ENV{'CARBON_HOME'}/bin/cbuild\n";
  }

  # add cbuild to path if its not there
  unless ( $ENV{'CARBON_HOME'} eq &CdsTest::CbuildPath() ) {
    $ENV{'PATH'} = "$ENV{'CARBON_HOME'}/bin:$ENV{'PATH'}";
  }

  # If '.' isn't already in user's $PATH, add it
  unless ($ENV{'PATH'} =~ /(^|:)\.(:|$)/) {
    $ENV{'PATH'} = ".:$ENV{'PATH'}";
  }

  # setup PYTHONPATH, needed by setup_area.py
  if (defined($ENV{'PYTHONPATH'})) { ### needed by setup_area.py
    $ENV{'PYTHONPATH'} = ".\:" . $ENV{'PYTHONPATH'};
  } else {
    $ENV{'PYTHONPATH'} = ".";
  }

  return "";
}

### &setCarbonEnvVars()
# set CARBON_HOST_ARCH, CARBON_TARGET_ARCH, and whatever needs to be set from
# CARBON_RELEASE, CARBON_TARGET, and CARBON_BIN
sub setCarbonEnvVars {

  my $TargetArch = "";
  my $HostArch = "";
  my $TargetBin = "";

  # first get target architecture
  # if CARBON_TARGET_ARCH is set, get arch from it
  if (exists($ENV{CARBON_TARGET_ARCH})) 
  {
      chomp ($TargetArch = $ENV{CARBON_TARGET_ARCH});
  } 
  elsif (exists($ENV{CARBON_TARGET})) 
  {
      ($TargetArch, $TargetBin) = split(/\./, $ENV{CARBON_TARGET});
  } 
  elsif (-f "$ENV{CARBON_HOME}/scripts/carbon_arch")
  {
      chomp ($TargetArch = `$ENV{CARBON_HOME}/scripts/carbon_arch`)
  }
  elsif (-f "$ENV{CARBON_HOME}/bin/carbon_arch")
  {
      chomp ($TargetArch = `$ENV{CARBON_HOME}/bin/carbon_arch`);
  }
  else
  {
      # do nothing.
  }

  # get host architecture
  if (exists($ENV{CARBON_HOST_ARCH})) 
  {
      chomp ($HostArch = $ENV{CARBON_HOST_ARCH});
  }
  elsif (-f "$ENV{CARBON_HOME}/scripts/carbon_arch")
  {
      chomp ($HostArch = `$ENV{CARBON_HOME}/scripts/carbon_arch`);       
  }
  elsif (-f "$ENV{CARBON_HOME}/bin/carbon_arch")
  {
      chomp ($HostArch = `$ENV{CARBON_HOME}/bin/carbon_arch`); 
  }
  else
  {
      # do nothing.
  }
  
  # now get the bin setting (product, debug, cov, or empty)
  if (exists($ENV{CARBON_BIN})) {
    $TargetBin = $ENV{CARBON_BIN};
  } else {
    $TargetBin = "";
  }
  # if release testing, will need to unset CARBON_BIN
  if (exists($ENV{CARBON_RELEASE})) {
    $TargetBin = "";
  }

  # if testing for coverage, set some cov specific stuff
  if ($TargetBin eq "cov") {
    $ENV{CARBON_NO_CBUILD_DIFF} = "1";
    my $covSwitches =  "-Wc '-ftest-coverage -fprofile-arcs'";
    unless($ENV{CARBON_CBUILD_ARGS} =~ m/$covSwitches/) {
      $ENV{CARBON_CBUILD_ARGS} .= " $covSwitches ";
    }
  }

  # now that we have the info we need, set the env vars
  $ENV{CARBON_HOST_ARCH} = $HostArch;
  $ENV{CARBON_TARGET_ARCH} = $TargetArch;
  if ($TargetBin eq "") {
    delete($ENV{CARBON_BIN});
    $ENV{CARBON_TARGET} = $TargetArch.".product";
  } else {
    $ENV{CARBON_BIN} = $TargetBin;
    $ENV{CARBON_TARGET} = $TargetArch.".".$TargetBin;
  }

  # Set the sensitivity checking to be adjusted in severity for regression testing.
  $ENV{CARBON_SUPPRESS_CBUILD_ADJ_SYNTH} = "0";
}

### $path = &CbuildPath()
# CbuildPath - return the path to bin/cbuild as exists in $PATH
sub CbuildPath {
  my $CbuildFromPath = `type cbuild 2> /dev/null`;
  chomp $CbuildFromPath;
  $CbuildFromPath =~ s/cbuild is //;
  $CbuildFromPath =~ s/cbuild is hashed //;
  $CbuildFromPath =~ s/[()]//g;
  $CbuildFromPath =~ s#/bin/cbuild##;

  return $CbuildFromPath;
}

### $dateAndTagStr = &cvsDateTagString($timestamp, $tagstamp)
# given 2 args, a timestamp and a tag, assemble a string that can be passed to
# CVS that will be valid for any combination of either, both or neither of the
# 2 args
sub cvsDateTagString {
  my ($argTimestamp, $argTag) = @_;
  # assemble tag/date str
  my $lDateTag = "";
  if ($argTimestamp) {
    unless($argTimestamp =~ /^\"/) {
      $argTimestamp = "\"$argTimestamp\"";
    }
    $lDateTag .= "-D $argTimestamp";
  }
  if ($argTag) {
    $lDateTag .= " -r $argTag";
  }
  return $lDateTag;
}

### $sandboxPath = &sandboxPathFromCarbonHome($carbonHomePath)
# given a carbon_home, determine where the sandbox is and return the abs
# path to it
sub sandboxPathFromCarbonHome {
  my ($argCarbonHomePath) = @_;

  # so we can go back there when we're done
  my $lStartDir = &CdsUtil::getSafeCwd();

  # get the location of the source sandbox by reading link on $CARBON_HOME/src
  my $lSandbox;
  if(-l "$argCarbonHomePath/src") {
    chdir($argCarbonHomePath);
    chdir(readlink("src")."/..");
    $lSandbox = &CdsUtil::getSafeCwd();
    #$lSandbox = &CdsUtil::AbsolutePath(
                  #readlink("$argCarbonHomePath/src") . "/..");
    chdir($lStartDir);
  } else {
    # or if src is not a link, then carbon home is same as sandbox
    $lSandbox = $argCarbonHomePath;
  }
 
  return $lSandbox;
}


### $statusInt = &setupTestMetaFiles($timestamp, $tagstamp)
# setupTestMetaFiles - make sure we have all the files we need to bootstrap
#  runsingle or runlist. currently, this is any files at the top level of
#  test/ and test/cust/template
# will use $CARBON_HOME, and @_ is (timestamp, tag)
# return 1 on success, 0 on failure
sub setupTestMetaFiles {
  my ($argTimestamp, $argTag) = @_;
  my @cvsFiles = ( "test/write-test.common",
		   "test/Makefile.common",
		   "test/Makefile.maxsim.inc",
                   "test/Makefile.squish",
		   "test/alltests.xml",
		   "test/run",
		   "test/tests.bom",
		   "test/demote-zconflict.dir",
		   "test/adjust_synth.directives",
		   "test/write-test.run.template",
		   "test/compute_cflags.py");
  # cvsTestBugsFiles are files that are needed if test/bugs exists
  my @cvsTestBugsFiles = ( "test/bugs/test.run.TestSetup");
  my $status;

  # put timestamp in quotes if it wasn't passed in that way
  unless($argTimestamp =~ m#^\"#) {
    $argTimestamp = "\"$argTimestamp\"";
  }

  my $lDateTag = &cvsDateTagString($argTimestamp, $argTag);

  # so we can go back there when we're done
  my $lStartDir = &CdsUtil::getSafeCwd();

  my $lSandbox = &sandboxPathFromCarbonHome($ENV{'CARBON_HOME'});
  unless(chdir($lSandbox)) {
    print STDERR "ERROR: can't cd to $lSandbox\n";
    chdir($lStartDir);
    return 0;
  }

  # if test doesn't exist, then we do the checkout and get what we need, 
  # and end of story
  unless (-d "test") {
    if ($gDebug) { print "cvs co -l $lDateTag test\n"; }
    `cvs co -l $lDateTag test >>  .cvs_checkouts.log.runsingle.$$ 2>&1`;
    $status = $?;
    &CdsUtil::RemoveStickyTags("test");
    if ($status != 0) {
      print STDERR "ERROR: can't checkout top level test meta files in ".
        "$lSandbox\n";
      chdir($lStartDir);
      return 0;
    }

    if ($gDebug) { print "cvs co $lDateTag test/cust/template\n"; }
    `cvs co $lDateTag test/cust/template >> .cvs_checkouts.log.runsingle.$$ 2>&1`;
    $status = $?;
    &CdsUtil::RemoveStickyTags("test");
    if ($status != 0) {
      print STDERR "ERROR: can't checkout test/cust/template meta files in ".
        "$lSandbox\n";
      chdir($lStartDir);
      return 0;
    }

  # have test, so get a list of the top level files with cvstat
  } else {
    # TODO: cvstat seems to behave differently for other people. don't use it
    # (at least for now)
    #if ($gDebug) { print "cvstat -ls -l test\n"; }
    #open(CVSFILES, "$ENV{'CARBON_HOME'}/scripts/cvstat -ls -l test 2>&1 |");
    #if($? != 0) {
      #print STDERR "ERROR: can't execute $ENV{'CARBON_HOME'}/scripts/cvstat\n";
      #chdir($lStartDir);
      #return 0;
    #}

    # check out each top level meta file: if it's not there or if it is zero length
    my $errors = 0; my $checkouts = 0;
    foreach my $file (@cvsFiles) {
      # if 'cvs server' is in there, it's a msg, so skip it
      #next if ($file =~ /cvs server/);
      #chomp $file;
      if ($gDebug) { print "Checking cvs file: $file\n"; }
      unless ( (-f $file) and (-s $file) ) {
	if (-z $file ) {unlink($file);}
	$checkouts++;
        if ($gDebug) { print "cvs co $lDateTag $file\n"; }
        system("cvs co $lDateTag $file");
        if ( ($? != 0) or (-z $file) ) { $errors++; }
	# make sure co was ok and file is not zero length (we have seen a problem like this many times)
	if (-z $file ) {print "REPORT THIS IMMEDIATELY TO QA: zero length file $file, using date tag: '$lDateTag'. REPORT THIS IMMEDIATELY TO QA\n"};
      }
    }
    #close(CVSFILES);

    if(-e "test/bugs") {
	foreach my $file (@cvsTestBugsFiles) {
	    unless ( (-f $file) and (-s $file) ) {
		if (-z $file ) {unlink("$file")}
		$checkouts++;
		if ($gDebug) { print "cvs co $lDateTag $file\n"; }
		system("cvs co $lDateTag $file");
		if ( ($? != 0) or (-z $file) ) { $errors++; }
		if (-z $file ) {print "REPORT THIS IMMEDIATELY TO QA: zero length file $file, using date tag: '$lDateTag'. REPORT THIS IMMEDIATELY TO QA\n"};
	    }
	}
    }

    # get rid of the sticky tags if we checked anything out
    if ($checkouts > $errors) { &CdsUtil::RemoveStickyTags("test/CVS"); }

    # now check for errors
    if ($errors > 0) {
      print STDERR "ERROR: unable to checkout some top level test ".
	"meta file(s)\n";
      chdir($lStartDir);
      return 0;
    }

    # now make sure test/cust/template is there
    unless(-d "test/cust/template") {
      if ($gDebug) { print "cvs co $lDateTag test/cust/template\n"; }
      `cvs co $lDateTag test/cust/template >> .cvs_checkouts.log.runsingle.$$ 2>&1`;
      $status = $?;
      &CdsUtil::RemoveStickyTags("test/cust/template");
      &CdsUtil::RemoveStickyTags("test/cust/CVS");
      &CdsUtil::RemoveStickyTags("test/CVS");
      if ($status != 0) {
        print STDERR "ERROR: can't checkout test/cust/template meta files in ".
          "$lSandbox\n";
        chdir($lStartDir);
        return 0;
      }
    }

  }
    
  chdir($lStartDir);
  return 1;
}

### $statusInt = &setupTestInBuildBox($buildDir, $fromScratchBool)
# setupTestInBuildBox - given a build directory, will delete any test/ dir
#  there, then link all test meta files from $CARBON_HOME/test to the build
#  area. return 1 on success, 0 on failure
sub setupTestInBuildBox {
  my ($buildObjDir, $fromScratch) = @_;

  # so we can go back there when done.
  my $lStartDir = &CdsUtil::getSafeCwd();

  # get rid of one level of indirection by determining the sandbox
  my $lSandbox = &sandboxPathFromCarbonHome($ENV{'CARBON_HOME'});
  unless(chdir($lSandbox)) {
    print STDERR "ERROR: can't cd to $lSandbox\n";
    $lSandbox = $ENV{'CARBON_HOME'};
  }
  chdir($lStartDir);
  
  if ($gDebug) { print "- setting up test area in build/obj: $buildObjDir\n"; }
  unless(-d $buildObjDir) {
    mkdir($buildObjDir);
  } 
  unless(chdir ($buildObjDir)) {
    print "ERROR: Can't chdir to $buildObjDir: $!\n";
    return 0;
  }

  # delete test dir and mk a new one if we're starting (over) from scratch
  if ($fromScratch){
    if (-e "test") {
      if ($gDebug) { print "rm -rf $buildObjDir/test\n"; }
      my $errors = &CdsUtil::RmRf("$buildObjDir/test");
      if($errors != 0) {
        print "ERROR: Can't remove $buildObjDir/test directory!: $!\n";
        print "ERROR: problems deleting $errors files\n";
        chdir($lStartDir);
        return 0;
      }
    }
    mkdir("test");
  }  elsif( not -d "test" ) {
    mkdir("test");
  }
  # otherwise, delete test/cust/template
  #else {
  #  if((-e "test/cust/template") and 
  #      system("/bin/rm -rf test/cust/template")) {
  #    print "ERROR: Can't remove $buildObjDir/test/cust/template: $!\n";
  #  }
  #}

  # if CVS dirs exist in CARBON_HOME, tar them over
  if (-d "$lSandbox/test/CVS") {
    if ($gDebug) { print "copying test/CVS to build area\n"; }
    system("(cd $lSandbox; tar cf - test/CVS) | tar xf - > /dev/null 2>&1");
    unless (-d "test/CVS") {
      print "ERROR: Can't copy $lSandbox/test/CVS to $buildObjDir/test/CVS\n";
    }
  }
  if (-d "$lSandbox/test/cust/CVS") {
    if ($gDebug) { print "copying test/cust/CVS to build area\n"; }
    system("(cd $lSandbox; tar cf - test/cust/CVS) | tar xf - > /dev/null 2>&1");
    unless (-d "test/cust/CVS") {
      print "ERROR: Can't copy $lSandbox/test/cust/CVS to $buildObjDir/test/cust/CVS\n";
    }
  }
  if (-d "$lSandbox/test/cust/template") {
    if ($gDebug) { print "copying test/cust/template to build area\n"; }
    system("(cd $lSandbox; tar cf - test/cust/template) | tar xf - > /dev/null 2>&1");
    unless (-d "test/cust/template") {
      print "ERROR: Can't copy $lSandbox/test/cust/template to $buildObjDir/test/cust/template\n";
    }
  }

  # now link all the files in the top of test/ over
  unless(opendir(TESTDIR, "$lSandbox/test")) {
    print "ERROR: Can't read directory $lSandbox/test: $!\n";
    chdir($lStartDir);
    return 0;
  }

  # this grep gets all plain files that do not start with . or ..
  my $status = 1;
  foreach my $iMetaFile (grep((!/^\.\.?$/ && -f "$lSandbox/test/$_"),
	readdir(TESTDIR))) {
    # Link the file if it isn't already there (multiple tests run at the same time)
    unless (-e "$buildObjDir/test/$iMetaFile") {
        # symlink it over, if that fails, then copy it
        if ($gDebug) { print "linking test/$iMetaFile to build area $buildObjDir/test\n"; }
        if ( not symlink("$lSandbox/test/$iMetaFile","$buildObjDir/test/$iMetaFile") ) {
            # Check first if the symlink is already there, if not, give up
            # Copying the file may cause a corruption so we don't do that anymore
            if ( (stat("$lSandbox/test/$iMetaFile"))[1] ne (stat("$buildObjDir/test/$iMetaFile"))[1] ){
                print "ERROR: Symlink failed for $buildObjDir/test/$iMetaFile from $lSandbox/test/$iMetaFile.\n";
                $status = 0;
            } else {
                print "Symlink was present even though symlink command failed. Won't copy file. ($lSandbox/test/$iMetaFile to $buildObjDir/test/$iMetaFile)\n"
            }
        }
    }
  }

  chdir($lStartDir);
  return $status;
}

### $statusInt = &fetchTest($sandbox, $testDir, $cvsStr)
#   given a sandbox, a test root dir, and a string containing a cvs -r or -D
#   arg, checkout or update the test dir using cvs
#   return 1 on success, 0 on failure
sub fetchTest {
  my ($sandboxDir, $testDir, $cvsDateTag) = @_;

  # make sure sandboxDir exists and contains a test dir
  unless(-d "$sandboxDir/test") {
    print STDERR
      "$sandboxDir/test directory does not exist - cannot fetch test\n";
    return 0;
  }

  # so we can go back there when done.
  my $lStartDir = &CdsUtil::getSafeCwd();
  my $lErrors = 0;

  chdir($sandboxDir);
  my $checkoutLog = cwd()."/.cvs_checkouts.log.runsingle.$$";

  # is the test dir in the sandbox already?
  if(-e "$sandboxDir/$testDir") {
    # it is, so use update

    if($gDebug) { print STDERR "$testDir exists - updating test dir\n"; }
    `cvs update -dP $cvsDateTag $testDir >> $checkoutLog 2>&1`;
    if ($? != 0) 
    {
      print STDERR "ERROR: Could not cvs update -dP -r $cvsDateTag $testDir ".
	"into $sandboxDir. See $checkoutLog $!\n";
      $lErrors++;
    }
    else
    {
	CdsUtil::RemoveStickyTags("$sandboxDir/$testDir");
    }

  # test is not in the sandbox, so check it out
  } else {

    if($gDebug) {
      print STDERR "fetchTest: $testDir does not exist - ".
        "cvs co'ing it to $sandboxDir\n";
    }
    my $cmd = "cvs co $cvsDateTag $testDir >> $checkoutLog";

    if ($gDebug)
    {
	print STDERR "fetchTest: running command -> $cmd\n";
    }

    `$cmd 2>&1`;

    if ($? != 0) {
      print STDERR "ERROR: Could not cvs co $testDir into $sandboxDir. ".
	"See $checkoutLog $!\n";
      $lErrors++;
    }
 
  }

  # go back to where we were, then return
  chdir($lStartDir);
  if ($lErrors > 0) { return 0; }
  else { return 1; }
}

### $statusInt =
###     &setupTestDirectory($testRootDir, $buildDir, $cvsStr, $noLinksBool, $noCVS)
# setupTestDirectory - given a test root directory and a testRun dir
#  (ie, $CARBON_HOME/obj/Linux.product or $CARBON_HOME/obj/SunOS.debug/rundir)
#  look in CARBON_HOME for the test.  If it's there, or if $noCVS is specified, populate a dir under
#  testRunDir with links to it (or copy it if $noLinks is specified).
#  If it's not there, check it out in testRunDir and don't touch the sandbox
#  return 1 if successfully linked from sandbox,
#         2 if successfully cheked out from CVS,
#         0 if failure
sub setupTestDirectory {
  my ($testRootDir, $testRunDir, $cvsDateTag, $noLinks, $noCVS) = @_;

  # so we can go back there when done.
  my $lStartDir = &CdsUtil::getSafeCwd();

  # get rid of one level of indirection by determining the sandbox
  my $lSandbox = &sandboxPathFromCarbonHome($ENV{'CARBON_HOME'});
  unless(chdir($lSandbox)) {
    print STDERR "ERROR: can't cd to $lSandbox\n";
    $lSandbox = $ENV{'CARBON_HOME'};
  }
  chdir($lStartDir);
  
  my $errors = 0;
  my $usedCvs;
  chdir($testRunDir);

  # is it in the sandbox, or are we disabling automatic CVS checkout?
  if((-e "$lSandbox/$testRootDir") || $noCVS) {
    if($gDebug) { print STDERR "$lSandbox/$testRootDir exists\n"; }

    # they said no links, so copy it
    if ($noLinks) {
      if($gDebug) { print STDERR "cp $lSandbox/$testRootDir to .\n"; }
      system("(cd $lSandbox; tar cf - $testRootDir) | tar xf - > /dev/null 2>&1");
      if($? != 0) {
	print STDERR "ERROR: problem copying $testRootDir from".
          " $lSandbox to $testRunDir: $!\n";
        $errors++;
      }

    # noLinks was not given, so symlink it over
    } else {
      if($gDebug){print STDERR "symlinking $lSandbox $testRootDir\n";}
      unless(&CdsUtil::SymlinkDirs($lSandbox,$testRootDir)) {
        print STDERR "ERROR: problem linking $testRootDir from ".
	  "$lSandbox to $testRunDir\n";
        $errors++;
      }
    }

  # not in the sandbox, so check it out in the rundir
  } else {

    $usedCvs = 1;
    unless(&fetchTest($testRunDir, $testRootDir, $cvsDateTag)) 
    {
        if ($gDebug)
        {
            print "CdsTest::fetchTest($testRunDir, $testRootDir, $cvsDateTag) - failed.\n";
        }
        $errors++;
    }

  }

  chdir($lStartDir);
  if ($errors > 0) { return 0; }
  elsif ($usedCvs) { return 2; }
  else { return 1; }
}

### 1 = &moveTestFromBuildToSrc($buildDir, $srcDir, $testDir)
# move testDir from buildDir to srcDir
sub moveTestFromBuildToSrc {
  my ($buildDir, $srcDir, $testDir) = @_;

  # so we can go back there when we're done
  my $lStartDir = &CdsUtil::getSafeCwd();
  chdir($buildDir);

  # first, make the intermediate dirs
  my $prevDir = "";
  # traverse the tree and keep everything cvs-friendly
  foreach my $subDir (split(/\//, $testDir)) {
    #already did the top level
    next if $subDir eq 'test';
                                                                                
    # if its not in the entries file yet
    if(system("grep D/$subDir/ $srcDir/test$prevDir/CVS/Entries ".
              "> /dev/null 2>&1")) {
                                                                                
      # if the dir doesn't already exist, need to set it up
      unless(-d "$srcDir/test$prevDir/CVS") {
        # make the dir
        if(&CdsUtil::MkdirP("$srcDir/test$prevDir/CVS")) {
                                                                                
          # copy over a Root file and a Tag file
          copy("$srcDir/src/CVS/Root", "$srcDir/test$prevDir/CVS/Root");
          if (-e "$srcDir/src/CVS/Tag") {
            copy("$srcDir/src/CVS/Tag", "$srcDir/test$prevDir/CVS/Tag");
          }
                                                                                
          # and put the dir name in a repository file
          if(open(REPO, ">$srcDir/test$prevDir/CVS/Repository")) {
            print REPO "test$prevDir\n";
            close(REPO);
          } else {
            print STDERR ("WARNING: could not create ".
              "$srcDir/test$prevDir/CVS/Repository\n");
          }
        } else {
          print STDERR ("WARNING: could not create ".
            "$srcDir/test$prevDir/CVS\n");
        }
      }

      # now put the testdir in the entries file
      open (ENTRIES, ">>$srcDir/test$prevDir/CVS/Entries") or
        print("WARNING: could not open $srcDir/test$prevDir/CVS/Entries ".
          "- CVS integrity could not be maintained\n");
      print ENTRIES "D/$subDir////\n";
      close (ENTRIES);
    }
                                                                                
    # go to the next level down
    $prevDir .= "/$subDir";
  }
                                                                                
  # now, move it!
  &CdsTest::MoveCVSTestArea("$buildDir/$testDir", "$srcDir/$testDir");

  chdir($lStartDir);
}

### 1 = &MoveCvsTestArea($buildDir, $srcDir)
# Takes 2 args, a build (origin) directory, and a src (destination)
#  directory. Moves all CVS files from buildDir to srcDir, then
#  creates links from the new files to where the old ones were
# assumes that $srcDir DOES NOT exist
sub MoveCVSTestArea {
  my ($buildDir, $srcDir) = @_;
                                                                                
  if($gDebug) { print STDERR "Moving $buildDir to $srcDir\n"; }
                                                                                
  # go to origin dir
  chdir($buildDir) or return 1;
  # make the dest dir
  &CdsUtil::MkdirP ($srcDir) or return 1;
                                                                                
  # copy over the CVS cruft
  system("cp -R $buildDir/CVS $srcDir");
  &CdsUtil::RemoveStickyTags("$srcDir/CVS");
                                                                                
  foreach my $file (@{&CdsTest::GetFilesFromCVSEntries}) {
    # make sure the file exists
    if (($file eq "") or (!(-e "$buildDir/$file"))) {
      print "Warning: file '$file' does not exist\n";
      next;
    }
    if($gDebug) { print STDERR "  Moving $file\n"; }
    # mv each file (skip this iteration of the loop with next on failure
    if(system("mv $buildDir/$file $srcDir/")) {
      print "Warning: could not relocate $file\n";
      next;
    }
    # make sure the mv really deleted the file
    if(-e "$buildDir/$file") {
      unless(unlink("$buildDir/$file")) {
        print STDERR "ERROR: cannot move $buildDir/$file\n: $!";
        next;
      }
    }
    # and replace it with a symlink to the new file
    if(system("ln -s $srcDir/$file .")) {
      print "Warning: could not link $srcDir/$file to $buildDir\n";
    }
  }
                                                                                
  # recurse into each dir
  foreach my $dir (@{&CdsTest::GetDirsFromCVSEntries}) {
    # make sure the file exists
    if (($dir eq "") or (!(-e "$buildDir/$dir"))) {
      print "Warning: dir '$dir' does not exist\n";
      next;
    }
    if ($gDebug) { print STDERR "  recursing into $dir\n"; }
    &CdsTest::MoveCVSTestArea("$buildDir/$dir", "$srcDir/$dir");
  }
}

### \@fileList = &GetFilesFromCVSEntries()
# look in ./CVS/Entries and return a hash to a simple list of base
# filenames (no full paths, no directories) listed there
sub GetFilesFromCVSEntries {
  my @results = `grep '^/' CVS/Entries | cut -d '/' -f 2`;
  # chomp newlines
  for (@results) { chomp; }
  return \@results;
}

### \@dirList = &GetDirsFromCVSEntries()
# look in ./CVS/Entries (and Entries.Log) and return a hash to a simple list of base
# dirnames (no full paths, no files) listed there
sub GetDirsFromCVSEntries {
  my @results = `grep '^D/' CVS/Entries | cut -d '/' -f 2`;
  # chomp newlines
  for (@results) { chomp; }

  ## somtimes Entries is not yet up-to-date and so we need to look in Entries.Log
  ## here we assume that names in Entries do not also appear in the Entries.Log
  if (-e "CVS/Entries.Log") {
      my @results2 = `grep '^A D/' CVS/Entries.Log | cut -d '/' -f 2`;
      for (@results2) { chomp; }
      push @results, @results2;
  }
  return \@results;
}

### $jobName = &getJobNameFromTestName($testName, $architecture)
# given a test name, munge it so we can use it as an SGE Job name
sub getJobNameFromTestName {
  my ($aTestName, $aArch) = @_;
  my $grp = $aTestName;		# we may need this string later
  # get the last path segment
  $aTestName =~ s#.*/##;
  # a test name can't start with a number, if it is only numbers prepend group name, else just remove leading numbers
  if ( $aTestName =~ m/^\d+$/ ) {
      # get the next to last path segment, and truncate if necessary
      $grp =~ s#.*/(.*)/.*#\1#;
      my $maxGrpLen = 5;
      if ( length($grp) > $maxGrpLen ) {
          $maxGrpLen = $maxGrpLen-length($grp);	# note a negative value 
      }
      $grp = substr $grp, 0, $maxGrpLen;
      
      # dir name is only digits, prepend a group name prefix (or at least an undescore)
      $aTestName = $grp . "..." . $aTestName;
  }
  $aTestName =~ s#^\d+##;
  # fix bugs test names
  if ($aTestName =~ m#^test\.run\.#) {
    $aTestName =~ s/^test\.run\.//;
  }

  if ($aArch eq "SunOS") {
    return "S-$aTestName";
  } elsif ($aArch eq "Windows") {
    return "W-$aTestName";
  } elsif ($aArch eq "Wine") {
    return "E-$aTestName";
  } elsif ($aArch eq "Linux64") {
    return "O-$aTestName";
  } else {
    return $aTestName;
  }
}

### $timestamp = &getTimestampForSandbox()
# getTimestampForSandbox - will look in $CARBON_HOME for src/util/UtVersion.cxx
#  and scripts/InfraVersion.pm. Whichever is later, will return its timestamp
#  returns error string, beginning with 'ERROR' on error
sub getTimestampForSandbox {
  my $lUtVersion = "src/util/UtVersion.cxx";
  my $lInfraVersion = "scripts/InfraVersion.pm";

  # make sure version files exist
  unless (-f $ENV{'CARBON_HOME'} . "/$lUtVersion") {
    return "ERROR: $ENV{'CARBON_HOME'}/$lUtVersion not found\n";
  }
  unless (-f $ENV{'CARBON_HOME'} . "/$lInfraVersion") {
    return "ERROR: $ENV{'CARBON_HOME'}/$lInfraVersion not found\n";
  }

  # first the hard part - get the timestamp from UtVersion.cxx 1st line
  open(UTVERS, "$ENV{'CARBON_HOME'}/$lUtVersion") or
    return "ERROR: Can't read $ENV{'CARBON_HOME'}/$lUtVersion";
  my $UtRev = <UTVERS>; chomp $UtRev;
  close(UTVERS);

  $UtRev=~ s/^.*Date: //;
  $UtRev=~ s/ \$.*;/ GMT /;
  # strip everthing that's not a digit so we can compare
  my $UtTime = $UtRev;
  $UtTime =~ s/\D//g;

  # now get it from InfraVersion.pm
  use InfraVersion;
  my $IfRev = &InfraVersion::gInfraVersion();
  $IfRev =~ s/^.*Date: //;
  $IfRev =~ s/ \$$/ GMT /;
  # strip everthing that's not a digit so we can compare
  my $IfTime = $IfRev;
  $IfTime =~ s/\D//g;

  if ( $IfRev eq "" ) {
      return "ERROR: unable to get Revision string from $ENV{'CARBON_HOME'}/$lInfraVersion";
  }
  if ( $UtRev eq "" ) {
      return "ERROR: unable to get Revision string from $ENV{'CARBON_HOME'}/$lUtVersion";
  }

  if ($IfTime > $UtTime) {
    return $IfRev;
  } else {
    return $UtRev;
  }
}

### $tag = &getTagForSandbox()
# return the tag that this sandbox has been last updated to, or empty string
# if there is none
sub getTagForSandbox {
  # no tag file, return empty string
  unless (-e "$ENV{'CARBON_HOME'}/src/CVS/Tag") {
    return "";
  }

  # get it!
  unless(open(TAGIN, "$ENV{'CARBON_HOME'}/src/CVS/Tag")) {
    print STDERR "ERROR: Can't read $ENV{'CARBON_HOME'}/src/CVS/Tag";
    return "";
  }
  my $tagLine = <TAGIN>;
  chomp $tagLine;
  close (TAGIN);

  # a tag will start with T or N, return everything after that
  if ( $tagLine =~ m/^[TN](.*)$/ ) {
    return $1;
  } else {
    return "";
  }
}

### $boolean = &isCmdSkipped($CmdLine, $HostArch, $TargetArch)
# given an unprocessed cmd line and a host and target architecture return 1 if
# cmd should be skipped for that arch combo, 0 otherwise. that is, has the
# cmd been marked with a TSKIP or PSKIP relevant to this arch combo
sub isCmdSkipped {
  my ($cmdLine, $hostArch, $targetArch) = @_;
  my ($strippedCmdLine, $directiveRef) = &processCmdLine($cmdLine);

  return &isCmdSkippedFromDirectives($directiveRef, $hostArch, $targetArch);
}

### $boolean = &isCmdSkippedFromDirectives($directives, $HostArch, $TargetArch)
# given an ref to list of status directives (see processCmdLine) and a host
# and target architecture return 1 if cmd should be skipped for that arch
# combo, 0 otherwise. that is, has the cmd been marked with a TSKIP or PSKIP 
# relevant to this arch combo
sub isCmdSkippedFromDirectives {
  my ($directiveRef, $hostArch, $targetArch) = @_;

  if (grep(/^[PT]SKIP$/, @$directiveRef) or 
      grep(/^[PT]SKIP-Host$hostArch.Target$targetArch$/, @$directiveRef) or
      grep(/^[PT]SKIP-Host$hostArch$/, @$directiveRef) or
      grep(/^[PT]SKIP-Target$targetArch$/, @$directiveRef) or
      grep(/^[PT]SKIP-$targetArch$/, @$directiveRef)) {
    return 1;
  }
  if ($ENV{CARBON_CBUILD_ARGS} =~ m/useVerific/) {
      if (grep(/^V[PT]SKIP$/, @$directiveRef) or 
	  grep(/^V[PT]SKIP-Host$hostArch.Target$targetArch$/, @$directiveRef) or
	  grep(/^V[PT]SKIP-Host$hostArch$/, @$directiveRef) or
	  grep(/^V[PT]SKIP-Target$targetArch$/, @$directiveRef) or
	  grep(/^V[PT]SKIP-$targetArch$/, @$directiveRef)) {
	  return 1;
      }
  } else {
      if (grep(/^I[PT]SKIP$/, @$directiveRef) or 
	  grep(/^I[PT]SKIP-Host$hostArch.Target$targetArch$/, @$directiveRef) or
	  grep(/^I[PT]SKIP-Host$hostArch$/, @$directiveRef) or
	  grep(/^I[PT]SKIP-Target$targetArch$/, @$directiveRef) or
	  grep(/^I[PT]SKIP-$targetArch$/, @$directiveRef)) {
	  return 1;
      }
  }
  return 0;
}

### ($ExpExitStat, $ExpDiffStat, $StrictExit, $StrictDiff) = 
###    &getExpectedStatus($CmdLine, $HostArch, $TargetArch)
# Given a test cmd line (from a test.run for instance), and a host and target
# architechture, return the expected exit status, expected diff status, and
# whether these statuses are strict or not. 
sub getExpectedStatus {
  my ($cmdLine, $hostArch, $targetArch) = @_;
  my ($strippedCmdLine, $directiveRef) = &processCmdLine($cmdLine);
  return &getExpectedStatusFromDirectives($directiveRef,
                                          $hostArch, $targetArch);
}

### ($ExpExitStat, $ExpDiffStat, $StrictExit, $StrictDiff) = 
###    &getExpectedStatusFromDirectives($directives, $HostArch, $TargetArch)
# Given a ref to list of status directives (see processCmdLine) and a host
# and target architechture, return the expected exit status, expected diff
# status, and whether these statuses are strict or not. Valid status
# directives are TEXIT/PEXIT, TDIFF/PDIFF, TFAIL/PFAIL, TSKIP/PSKIP, with an
# optional '-HostArch.TargetArch' For PFAIL/TFAIL, Strict is off, for
# TEXIT/TDIFF/PEXIT/PDIFF, strict is on. TSKIP/PSKIP are ignored for the
# purposes of determining expected status. Directives must appear at the
# beginning of the line, without comment chars
# will return (-1, $errormsg) on error
sub getExpectedStatusFromDirectives {
  my ($directiveRef, $hostArch, $targetArch) = @_;
  my @validWords = qw(PDIFF PEXIT TDIFF TEXIT PFAIL TFAIL TSKIP PSKIP);
  # extend the valid words with copies with either V or I prefix depending on if running with -useVerific
  if ($ENV{CARBON_CBUILD_ARGS} =~ m/useVerific/) {
      foreach(@validWords) { s/\(.*\)/V\1 \1/g; }
  } else {
      foreach(@validWords) { s/\(.*\)/I\1 \1/g; }
  }


  # now decide what our status should be. 
  # Precedence order for architecture is
  # PFAIL, PFAIL-HostTarget, PFAIL-Host, PFAIL-Target 
  # precedence order for functionality is
  # PDIFF/PEXIT, TDIFF/TEXIT, PFAIL/TFAIL
  # so for insance PEXIT-Linux overrides TFAIL for exit status
  my $order = 1;
  # generate a quickndirty lookup table for precedence
  my %precMap = map { $_, $order++ } @validWords;
  #print "prec: " .join(" ", %precMap) . "\n";

  # easy way to do this is to sort by precedence, then cycle through list until
  # we find what applies to us
  my @statusList = sort {
      # split a and b into directive and arch
      my ($directiveA, $archA) = split(/-/, $a);
      my ($directiveB, $archB) = split(/-/, $b);

      # sort first by directive, use precedence map
      $precMap{$directiveA} <=> $precMap{$directiveB}
        or

      # then sort by arch, a little trickier, so call getArchPrecedence
      &getArchPrecedence($archA) <=> &getArchPrecedence($archB);
      
    } @$directiveRef;

  #print "ordered cmds: ".join(" ", @statusList)."\n";

  my $expectedExit = 0; my $expectedDiff = 0;
  my $strictExit = 1; my $strictDiff = 1;

  # now look through the statuses in reverse order, setting flags as we
  # find one that applies. when finished, flags should be correct
  foreach my $word (reverse(@statusList)) {
    # first split into directive and arch
    my ($testDir, $archDir) = split(/-/, $word);
    # if arch does not match given arch, then skip it
    if ($archDir and not (($archDir =~ /^Host$hostArch.Target$targetArch$/) or
			  ($archDir =~ /^Host$hostArch$/) or
			  ($archDir =~ /^Target$targetArch$/) or
			  ($archDir =~ /^$targetArch$/))) {
	next;
    }

    my $prefixPattern = "[I]*[TP]+";  # allows match to ITEXIT as well as TEXIT,
    if ($ENV{CARBON_CBUILD_ARGS} =~ m/useVerific/) {
	$prefixPattern = "[VI]*[TP]+"; # allows match to ITEXIT, VTEXIT as well as TEXIT
    }

    # use testDir to set flags
    if ($testDir =~ /^($prefixPattern)FAIL$/) {
      # PFAIL/TFAIL should behave as before - only applies to exit, and means
      # exit and diff are not strictly interpreted
      $expectedExit = 1; 
      $strictDiff = 0;
      $strictExit = 0;
      #$expectedDiff = 1;
    } elsif ($testDir =~ /^($prefixPattern)EXIT$/) {
      $expectedExit = 1; 
      $strictExit = 1;
    } elsif ($testDir =~ /^($prefixPattern)DIFF$/) {
      $expectedDiff = 1;
      $strictDiff = 1;
    }
  }

  ## now if either of the expects is 0, make it strict
  #$strictExit = $expectedExit ? $strictExit : 1;
  #$strictDiff = $expectedDiff ? $strictDiff : 1;

  # now return the 4 flags
  return ($expectedExit, $expectedDiff, $strictExit, $strictDiff);

}

### $strippedCmdLine = stripCmdLine($rawCmdLine)
# given a cmd line which may or may not contain PFAIL, TDIFF, etc, return
# a clean cmd line that definitely does not and is ready for execution.
# note: PFAIL now ONLY supported at beginning of line
sub stripCmdLine {
  my ($cmdLine) = @_;

  my ($strippedCmdLine, $other) = &processCmdLine($cmdLine);

  return $strippedCmdLine;
}

# ($strippedCmdLine, \@directives) = processCmdLine;
# given a raw cmd line, return a stripped cmd line and ref to list of directs
sub processCmdLine {
  my ($cmdLine) = @_;

  # strip leading/trailing whitespace
  chomp($cmdLine);
  $cmdLine =~ s/\s*$//; $cmdLine =~ s/^\s*//;

  # if first char is comment, return empty vars. no cmd, thus no directs
  if ($cmdLine =~ /^#/) {
    return ("", []);
  }

  # first n words that are recognized as directives, strip off cmd line and put
  # in a directives list
  my @directives = ();
  while($cmdLine =~ s/^\s*([VI]?[TP](FAIL|DIFF|EXIT|SKIP)[^\s]*)//) {
    push (@directives, $1);
  }

  # add a space between redir char and log file - bug 1992
  $cmdLine =~ s/(>&?)([\w\.])/$1 $2/;
  # could wind up with an extra leading space from loop, so do this again
  $cmdLine =~ s/^\s*//;

  # as a safety net, empty directives list if cmd line is empty (ie, nothing
  # but directives were on cmd line)
  unless($cmdLine) { @directives = (); }

  return ($cmdLine, \@directives);
}

### $logFile = getLogFile ($strippedCmdLine)
# given a stripped command line (no directives, ready for exec), return the log
# file that output will be redirected to, or empty string if none exists
# TODO: this could fail for something like this:
#  cmd # comment >& file
sub getLogFile {
  my ($cmdLine) = @_;
  my $logFile = "";
  if ($cmdLine =~ /\s>&?\s*([^\s]+)\s*/) {
    $logFile = $1;
  }
  return $logFile;
}

# helper routine for sort routine in getExpectedStatus. should not be called
# by anyone else ever.
sub getArchPrecedence {
  my ($archSpec) = @_;
  # empty arch is highest
  if (! $archSpec) { return 3; }
  # what is specified - host and target?
  elsif ($archSpec =~ m/^Host.*\.Target/) { return 1; }
  # or just host?
  elsif ($archSpec =~ m/^Host/) { return 2; }
  # otherwise, must be just target
  else { return 3; }
}

### @WindowsHosts = getWindowsHosts([$string])
# given a string containing ':'-separated list of hostnames, return a list of
# those names in a random order. if string is not specified, use
# WINDOWS_TEST_HOST env var. returns empty list on failure
sub getWindowsHosts {
  my ($WinHostString) = @_;
  unless ($WinHostString) {
    $WinHostString = $ENV{'WINDOWS_TEST_HOST'};
  }

  my @WinHosts = split (/:/, $WinHostString);
  unless(scalar(@WinHosts)) {
    return ();
  }

  &CdsUtil::fisher_yates_shuffle(\@WinHosts);
  return @WinHosts;
}

### $jobId = executeQsub($cmd)
# given a qsub cmd, execute it and return numeric jobId submitted, or error
# string from qsub on error
sub executeQsub {
  my ($cmd) = @_;
  # execute qsub
  my $result = `$cmd 2>&1`;

  # if no system error and got a job id, return it
  if ((not $?) and ($result =~ m/ job (\d+) /)) {
    return $1;
  } else {
    return $result;
  }
}

### \@SgeJobIds = getMySgeJobs([$user])
# return ref to list of all jobIds currently in sge (running or not) for a
# given user id. if user is not given, use $USER from the environment
sub getMySgeJobs {
  my ($user) = @_;

  # was user name supplied? if not, use ENV var
  unless ($user) { $user = $ENV{'USER'}; }

  # run a 'qstat -u <user>' and assemble a list of my active job ids
  # the 'map' converts each line to the first number found. 
  my @allJobs = map { s/^\s*(\d+) .*/$1/; $_; } 
    # the grep filters out lines with no numbers
    grep(/^\s*\d+ /, `qstat -u $user`);
  # chomp the newlines
  chomp(@allJobs);

  # now return a ref to list of jobs
  return \@allJobs;
}

# END
1;
