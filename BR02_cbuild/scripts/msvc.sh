#!/bin/bash
#
# Run the visual c compiler via command line on a remote host.

# Usage
usage()
{
    cat <<EOF
Usage: $0  [-coDEI] [-Wall]  file...

EOF
}

# error logging
logit(){
    echo $* >&2
}

function error(){
    logit $*
    exit 1
}

# convert a Unix path to a windows path 
function fixpath()
{
OHOME=/home/cds/
NHOME=//Carbon/
FPATH=${1/$OHOME/$NHOME}

FPATH=${FPATH/\/cygdrive\/c/c:}

FPATH=${FPATH/\/\.\//\/}
echo ${FPATH//\//\\\\}
}

function addarg() {
    args[$argsOut]="$1"
    argsOut=$((argsOut+1))
}

declare -a args
declare -i argsOut=0
declare -i argsIn=$#

if [ $# -eq 0 ]; then
    usage
    exit 1
fi

    
while [ $argsIn -gt 0 ]; do
  argsIn=$((argsIn-1))
  arg=${1}
  shift
    
  case $arg in
      -o)
	  addarg /Fo$(fixpath $1)
	  shift
	  argsIn=$((argsIn-1))
	  ;;

      -E)
	  addarg /E
	  ;;

      -I\.)
	  addarg /I$(fixpath $(pwd))
	  ;;

      -I-)
	  addarg /X
	  ;;

      -I*)
          addarg /I$(fixpath ${arg/-I/})
	  ;;

      -c)
	  addarg /c
	  ;;

      -D*)
	  addarg ${arg/-/\/}
	  ;;

      -Wall)
	  addarg /Wall
	  ;;
	  
      -M[FT])
	  shift
	  argsIn=$((argsIn-1))
	  ;;

      -E)
	  addarg /E
	  ;;

      -*)
	  error "$0: untranslated flag $arg"
	  ;;


      *)
	  addarg $(fixpath ${arg})
	  ;;

  esac
done



## Add /WX /Wall to treat warnings as errors and be very picky
## /ML says use static library
## /GB - Pentium 4 optimization
## /Za - Std C++ only
## /WL - one-line warnings from CL
## /EHsc - synchronous exceptions only, C programs can't raise.
## /GR - enable RTTI
EXTRA_FLAGS="/EHsc /DWIN32 /D_MT=0 /D_MSC_VER_=7 /ML /GB"


# Location to find MS Visual toolsrem
MSVCRoot="/cygdrive/c/PROGRA~1/MICROS~4"
VSCommonDir=${MSVCRoot}/Common
MSDevDir=${MSVCRoot}/Common/msdev98
MSVCDir=${MSVCRoot}/VC98

ADDTOPATH=${MSDevDir}/BIN:${MSVCDir}/BIN:${VSCommonDir}/TOOLS/WINNT:${VSCommonDir}/TOOLS

ADDTOINCLUDE="$MSVCDir/ATL/Include $MSVCDir/Include $MSVCDir/MFC/Include"

for l in $ADDTOINCLUDE; do
    addarg /I$(fixpath ${l})
done

GOTODIR=`pwd | sed -e 's,/home/cds,//carbon,'`

logit path is $ADDTOPATH
logit cd-ing to ${GOTODIR}
logit CL ${EXTRA_FLAGS} ${args[@]}

WINMACHINE=`echo $WINDOWS_TEST_HOST |sed -e 's,:.*,,'`
exec ssh $WINMACHINE "export PATH=$ADDTOPATH:\$PATH; \
  cd $GOTODIR; \
  cl.exe  ${EXTRA_FLAGS} ${args[@]} 2>&1"

