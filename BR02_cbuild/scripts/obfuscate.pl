#!/bin/sh
# -*-Perl-*-
exec $CARBON_HOME/bin/perl -S -x -- "$0" "$@"
#!perl
#
# obfuscate a group of files, replacing customer names by obfuscated
# equivalents
#

use strict;
use warnings;

sub usage
{
    print "$0 file1 file2 ....\n"
        . "\tTranslates all hierarchical names (and .cost file names)\n"
        . "\tinto X1.X2 form.\n"
        . "\tYou must specify all related files on the same line\n"
        . "\tor the obfuscated names will not correlate across files.\n"
        . "\tThis won't obfuscate a single-component name appearing in\n"
        . "\tfiles except for the .costs and .hierarchy files.\n\n"
        . "\tAt the same time a decryptor.pl script is generated.  The customer\n"
        . "\tuses this to convert X12.X93.X33 back into their names so that they\n"
        . "\tcan look at the modules we narrow in on using obfuscated names.\n";
    exit 0;
}

&usage() if ($#ARGV lt 0);


# Reserved words that don't get translated in bare context.
#
my @RESERVED = qw (v cxx h Module Instance Location);    # Leave file extensions alone...
          


my %HIERHASH;
my $counter = 0;

# Hash a simple name
sub hashone {
    my ($hname) = @_;
    return "" if ($hname eq "");   # Hash empty string to self

    $HIERHASH{$hname} = "X" . $counter++ unless exists $HIERHASH{$hname};
    return $HIERHASH{$hname};
}

# Hash a hierarchical name, preserving the periods
sub hashme {
    my ($hname) = @_;
    return $hname if ($hname =~ m/\d+/); # Ignore strictly decimal numbers

    my @name = split /\./, $hname;
    my $hashed="";



    if ($#name == 1) {
        return $hname if ($name[1] eq 'cxx'); # Don't mess with filenames
        return $hname if ($name[1] eq 'h');
        return $hname if ($hname =~ m/\d+\.\d+\w?/); # Ignore floats with trailing letter
    }

    # Just translate a component at a time...
    for my $component (@name) {
        $hashed .= &hashone($component) . '.';
    }

    chop $hashed;               # Remove trailing '.'
    return $hashed;
}	

# Hash a file pathname, preserving .. and slash delimiters
sub hashpath {
    my ($hpath) = @_;

    # if there are no slashes, then don't bother with hashing this
    my @names = split /\//, $hpath;
    return $hpath if ($#names == 1);

    my $hashed="";

    for my $component (@names) {
        $hashed .= &hashme($component) . "/";
    }

    # We have ONE too many slashes
    return substr ($hashed, 0, length($hashed)-1);
}

# Preload the reserved words so that they hash to themself if seen
for my  $r (@RESERVED) {
    $HIERHASH{$r} = $r;
}

# For each file, read it in and lookup any hierarchical name, replacing
# it by its magic hash number as we write it out.  Use the same hash
# for all files so that we have cross file consistency.
#

my $oldargv="";

while (<>) {
    if ($ARGV ne $oldargv) {	# New file?
	rename ($ARGV, $ARGV.'.bak');
	open (ARGVOUT, ">$ARGV");
	select (ARGVOUT);
	$oldargv = $ARGV;
    }

    # For different files do different things.
    
    # Warnings and errors files need to convert file paths too
    s,(.*):(\d+): ,&hashpath($1) . ":$2: ",e if ($oldargv =~ m/.*\.(errors|warnings)/);

    # Costs file has module name as first component
    s/^([\$\w]+),/&hashme($1) . ","/ge if ($oldargv =~ m/.*\.costs/);

    # Hierarchy file has module name as first component too
    s/([\$\w]+)/&hashme($1)/ge if ($oldargv =~ m/.*\.hierarchy/);

    # For each input line, match a hierarchical expression and
    # call hashme with the whole match 
    s/([\$\w]+(\.[\$\w]+)+)/&hashme($1)/ge;
}
continue {
    print;
}

my $CR="\n";
# Write out a reverse translator with the embedded data base
my $decrypt = "decryptor.pl";
open(DECRYPT, ">$decrypt") || die "cannot create decryptor tool\n";

print DECRYPT '#!/usr/bin/env perl' . $CR
    . 'use strict;' . $CR
    . 'use warnings;' . $CR
    . '# simple decryptor generated from: ' . join(' ', @ARGV) . $CR
    . 'my %RHash = (' . $CR
    ;

# And the DATA as a reverse hash
my @hkey = keys %HIERHASH;
my @hval = values %HIERHASH;


while (@hval) {
    print DECRYPT "'" . pop(@hval) . "'" .  "=>" . "'" . pop(@hkey) . "'" . ',' . $CR;}

print DECRYPT "\t);\n";

# A simple usage function
print DECRYPT 'sub usage{' . $CR
    . '  print "$0 hierpath ...\n\tTranslates X1.X2 back to customer names\n";' . $CR
    . '  exit 0;}' . $CR;

# And the reverse hash function
print DECRYPT 'sub dehash{' . $CR
    . '  my ($dname) = @_;' . $CR
    . '  my @name = (split /\./, $dname);' . $CR
    . '  for (my $i=0; $i<= $#name; $i+=1) {' . $CR
    . '    $name[$i] = $RHash{$name[$i]} if exists $RHash{$name[$i]};' . $CR
    . '     } return join(".", @name); }' . $CR;


print DECRYPT "# work loop....\n"
    . '&usage() if ($#ARGV lt 0);' . $CR
    . 'while (@ARGV) { ' . $CR
    . '  my $veiled = pop @ARGV; print ($veiled, "=>", &dehash($veiled), "\n");' . $CR
    . '}' . $CR
    . '0;' . $CR;


close DECRYPT || die "error closing decriptor script file\n";
chmod(0755, $decrypt) || die "could not set executable mode on decryptor\n";

select (STDOUT);


