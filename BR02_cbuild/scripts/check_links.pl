#!/usr/bin/perl

    delete $ENV{MAKE};

use File::Find;
#use CarbonQAData;

#my $qaData = new CarbonQAData();
my $rtn = 0;
my $msgType = "WARNING";
my $targetOs = $ENV{CARBON_TARGET_ARCH};

my @dirs = ("$targetOs","include","bin");
# The new carbon_config is only run Linux* right now.
# We will want to ignore mispointed links on other 
# platforms for now, until the new one is configuring all 
# of them.
#-------------------------------------------------------

if (! $targetOs)
{
    print STDERR "Couldn't get target platform for build -> $targetOs.\n";
}
else
{
    if ($targetOs =~ /Linux/)
    {
        $msgType = "ERROR";
    }

    chdir ($ENV{CARBON_HOME});
    
    foreach my $dir (@dirs)
    {
        find (\&FindLinks, "$ENV{CARBON_HOME}/$dir");
    }
    
# Only if we are NOT treating this as an error condition.
# Reset to a passing state.
#----------------------------------------------------
    if ($msgType ne "ERROR")
    {
        $rtn = 0;
    }
}

if ($rtn > 0)
{
    print "Missing link messages are simply informational.\n";
}

exit $rtn;;


sub FindLinks
{
    my $f = $File::Find::name;

    if (-l "$f")
    {
        my $readl =  readlink($f);

        if (! $readl)
        {
            print "$msgType: You have a link $f that is not pointing at anything.\n";
            $rtn++;
        }
        elsif (! -e $readl)
        {
            print "$msgType: Link $f points to $readl, but $readl does not exist.\n",
            "Either the intended target was not built, or the process that creates ",
            "this link needs to be modified so the link is no longer made.\n";
            $rtn++;
        }
        else
        {
#            print "$f -> $readl [OK]\n";
        }
    }
}
