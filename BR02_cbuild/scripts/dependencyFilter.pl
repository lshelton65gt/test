#!/usr/bin/perl

# The intended use of this filter is a take output of a 3rd party dependency
# tool and substitute path names with out internal build macro names.
# By doing so, we truly extend our system and make the dependency system
# more flexible and less likely to break by moving CARBON_HOME and/or
# reassignment of variables to other path strings.

my $objDep = 0;
my $obj = "";
my $src = "";
my %updateFileDeps = ();


while (<>)
{
#    print STDERR "Raw line->|$_|\n";

    # Unify miscommunications between /o/tools and /tools
    $_ =~ s/\/o\/tools/\/tools/g;

    # replace /tools/linux|solaris with CDSTOOLSDIR
    if ($ENV{CDSTOOLSDIR} && -d $ENV{CDSTOOLSDIR} && 
        $_ =~ /$ENV{CDSTOOLSDIR}/)
    {
        $_ =~ s/$ENV{CDSTOOLSDIR}/\$(CDSTOOLSDIR)/g;
    }

    # Apply mvv release dir macro
    if ($ENV{INTERRA_BASE} && -d $ENV{INTERRA_BASE} && 
        $_ =~ /$ENV{INTERRA_BASE}/)
    {
        $_ =~ s/$ENV{INTERRA_BASE}/\$(INTERRA_BASE)/g;

        # Add a dependency on the Interra links file.  Only do this
        # for .o targets (i.e. those that depend on Interra headers),
        # not for libraries/executables (i.e. those that depend on
        # Interra libraries).  I'm not sure why this is, but that's
        # the way it was done when fastdep was being used so I'm not
        # going to change it.
        if (! $updateFileDeps{interra_base} && ($_ =~ /\$\(INTERRA_BASE\)\/\S+\.h/))
        {
            $updateFileDeps{interra_base} = "  \$(INTERRA_LINKS)";

            if ($_ !~ /\\$/)
            {
                $_ .= "$updateFileDeps{interra_base} \n";
            }
            else
            {
                $_ .= "$updateFileDeps{interra_base} \\\n";
            }
        }
    }

    # Make all sandbox source start with $top_srcdir.
    if ($ENV{top_srcdir} && $_ =~ /$ENV{top_srcdir}/)
    {
        $_ =~ s/$ENV{top_srcdir}/\$(top_srcdir)/g;
    }

    # Replace any $CARBON_HOME or CARBON_TARGET with the build
    # macro string.
    $_ =~ s/$ENV{CARBON_HOME}/\$(CARBON_HOME)/g;
    $_ =~ s/$ENV{CARBON_TARGET}/\$(CARBON_TARGET)/g;

    $_ =~ s/\/work\/.+\/obj\//\$(CARBON_HOME)\/obj\//g;

    print $_;
}

exit 0;
