#!/bin/sh
# -*-Perl-*-
exec $CARBON_HOME/bin/perl -S -x -- "$0" "$@"
#!perl

# Convert unix pathnames to Wine drive: paths
use warnings;
use Cwd 'abs_path';
use File::Spec;

my @newsrgs;

foreach $arg (@ARGV) {
    my ($path,$fn) = ($arg =~ m|(.*/)([^/]*)|);

    ($arg =~ s|^/|Z:/|)		# Change absolute path to Z: drive
 || ($arg =~ s|^~/|H:/|)	# Change ~/ path to H: drive
 || ($arg =~ s|^\./|P:/|)	# CWD relative path to P: drive 
 || ($arg =~ s|^\.\./.*|'Z:'. abs_path($path) . '/' . $fn|e);

 $arg =~ s|/+|\\|g;
 push @newargs,$arg;
}

$,= ' ';			# spaces between args
if (defined $ENV{LOGFILE}) {
    open(LOGFILE, ">>$ENV{LOGFILE}");
    print LOGFILE @newargs; print LOGFILE "\n";
}

print @newargs
