#!perl

package CdsUtil;
use strict;

# NOTE: To get a list of all routines in this package, grep for '^###'

use File::Find;
use File::Copy;
use File::Basename;
use File::Path;
use Cwd; 
use FileHandle;

my $Debug = 0;

# maybe turn debug on?
if (  $ENV{'RUNLIST_DEBUG'} )   { $Debug = 1 ; }

sub MkdirP 
{
   # returns same as mkdir(). Also, chmod g+w if WINX is set

    my ($dir, $perms) = (@_);
    my $rtn = 0;

    eval {mkpath ($dir)};

    if ($@)
    {
	print STDERR "CdsUtil::MkdirP(): Could not create $dir directory\n";
    }
    else
    {
	$rtn = 1;

	if ($perms && $perms !~ /\D+/)
	{
	    while (length($perms) < 4)
	    {
		$perms = "0".$perms;
	    }
	    
	    # Return from chmod is number of things chmodded. Zero is bad.

	    $rtn = chmod oct($perms), $dir;

	    if ($rtn == 0)
	    {
		print STDERR "CdsUtil::MkdirP(): Could not set permissions of $dir to $perms\n";
	    }
	}

	if ($ENV{WINX} && $rtn == 1) 
	{
	    # chmod to group writeable so wintest user can write 
	    $rtn = system ("chmod g+w -R . > /dev/null");
	    
	    if ($rtn != 0)
	    {
		print STDERR "CdsUtil::MkdirP(): Could not set permissions of '.' recursively to g+w \n";
		$rtn = 0;
	    }
	    else
	    {
		$rtn = 1;
	    }
	}
    }

#   Return is 1 if passed, 0 if failed.

    return $rtn;
}

sub SetTopTestRunDir
{
    my ($objDir, $dirArg, $usingSGE) = (@_);
    my $topTestRunDir = "$objDir";

    if (! $dirArg)
    {
	$topTestRunDir = "$objDir";
    }

    else
    {
	if ($dirArg !~ /^\//)
	{
	    $topTestRunDir = "$objDir/$dirArg";
	}
	
	else
	{
	    my $netmounted = &CdsUtil::IsNetworkFriendly ($dirArg);
	    
	    if (($usingSGE && $netmounted) || ! $usingSGE)
	    {
		$topTestRunDir = "$dirArg";
	    }
	    else
	    {
		if ($usingSGE)
		{
		    print "Using grid, but rundir path not network friendly,\n",
		    "will append path to $objDir\n";
		}
		
		$topTestRunDir = "$objDir/$dirArg";
		$topTestRunDir =~ s/\/\//\//g;
	    }
	}
    }
    return &shortcircuit($topTestRunDir);
}

### $statusInt = &CopyP( $fullSrcFilePath, $fullDestFilePath )
# Copy srcfile to destfile, creating any intermediate dirs that need to
# be created. returns same as copy()
sub CopyP {
  my ($srcpath, $destpath, $quiet) = @_;
  my $makeItQuiet = "";

  unless(-e $srcpath) {
    print STDERR "CdsUtil::CopyP ERROR: $srcpath does not exist\n";
    return 0;
  }
  if(-d $srcpath) {
    print STDERR "CdsUtil::CopyP ERROR: $srcpath is a directory\n";
    return 0;
  }

  unless(-d dirname($destpath)) {
    unless(&CdsUtil::MkdirP(dirname($destpath))) { return 0; }
  }

  # want to keep mode the same, so use system cp instead of perl copy
  # also, to maintain the same return functionality, return perl status, not
  # posix status
  #return copy($srcpath, $destpath);
  if ($quiet) { $makeItQuiet = " > /dev/null 2>&1"; }
  return not system("cp $srcpath $destpath $makeItQuiet");
}

### $numberErrors = &RmRf ( $topdir )
# same as 'rm -rf $topdir', but safer to use, since unlink doesn't do stats
# on the files to delete. returns number of files unable to delete (0 on
# success), or -1 on error
sub RmRf {
  my ($topdir) = @_;

  unless($topdir and (-e $topdir)) {
    if($Debug) { print "-- Dir $topdir not found!\n"; }
    return -1;
  }
  my $errors = 0;

  finddepth ( sub {
      if (!-l and -d _ and ($File::Find::name ne $topdir)) {
        #print "rmdir $File::Find::name\n";
        unless(rmtree($_)) {
          if($Debug) { print "-- error: rmdir: $!\n"; }
          $errors++;
        }
      } elsif ($File::Find::name ne $topdir) {
        #print "unlink $File::Find::name\n";
        # first try using perl's unlink
        unless(unlink($_)) {
	  # then the system's unlink
          system("unlink $_");
          if ($?) {
            if($Debug) { print "-- error: unlink: $!\n"; }
            $errors++; 
          }
        }
      }
    }, $topdir );

  # if it's still there, delete the top dir
  if(-d $topdir) {
    rmdir($topdir) or $errors++;
  }
  return $errors;
}
  
### $statusInt = &SymlinkDirs( $sourceDir, $testDir )
# creates a shadow dir in pwd full of symbolic links to $sourceDir/$testDir
#  Argument #1 is <source-directory>
#  Argument #2 is directory within <source-dir> to make a shadow of
#   # Variables take on values like this:
# SourceDir      ==  ~qa/latest_source
# TestDir        ==  test/langcov
#
#  adapted from the bash script symlink. It is called  by CdsTest.pm -
#  if you change its behavior you need to keep  runsingle in sync.
sub SymlinkDirs {
  my ($SourceDir, $TestDir) = @_;

  # check that the 2 args are there
  if ( (@_ > 2) or (!(-e "$SourceDir/$TestDir")) ) {
    print("ERROR: SymlinkDirs() needs 2 args, a source dir and a dir within that\n");
    print("  source dir to link\n");
    return 0;
  }

  ###
  ###   Make shadow directory
  ###
  my @dirlist = ();
  my $start = &CdsUtil::getSafeCwd();
  chdir($SourceDir);
  # get list of all dirs under here
  find(sub { push(@dirlist, $File::Find::name) if -d; }, $TestDir);
  chdir($start);
  my @filelist = ();

  foreach my $LinkDir (@dirlist) {
    # if it's really just a symlink, link it and move on
    if (-l "$SourceDir/$LinkDir") {
	symlink("$SourceDir/$LinkDir", $LinkDir);
	next;
    }

    #print "DIR: $LinkDir\n";
    &MkdirP($LinkDir);
    unless(chdir("$SourceDir/$LinkDir")) {
	print ("ERROR: Unable to chdir into $SourceDir/$LinkDir: $!\n");
	return 0;
    }

    # get list of all files in this dir
    opendir(DIR, ".");
    # this re says only files that are not . or .. and that are of type file
    @filelist = grep((!/^\.\.?$/ && -f), readdir(DIR));
    closedir(DIR);

    # go back to start here in case relative dir was specified
    chdir($start); chdir($LinkDir);
    foreach my $file (@filelist) {
      #print "  FILE: $file\n";
      unlink($file);
      symlink("$SourceDir/$LinkDir/$file", $file);
    }
    @filelist = ();
    chdir($start);
  }
  return 1;
}

#
# GetTestsOfType takes two arguments - a list of strings representing the
#  types of tests to return, and a file to search. it will return a list of
#  tests from the specified file that are denoted as that type. if no type is
#  specified, it will return all tests in the file regardless of type
#  filename defaults to test/alltests.list.
#  on failure, first element is return code and 2nd is error
#
#  THIS ROUTINE IS OBSOLETE. PLEASE USE scripts/TestList.pm INSTEAD
sub GetTestsOfType {
  my $TestTypes = shift;
  my ($testFile) = shift;

  my $testType = "";
  if ($TestTypes) {
    $testType = join('|', @$TestTypes);
  }

  unless($testFile) {
    $testFile = "$ENV{CARBON_HOME}/test/alltests.list";
  }

  my @testList = ();
  open(TESTLIST, $testFile) or return (0, "couldn't read $testFile");
  foreach my $testline (<TESTLIST>) {
    # skip comments and whitespace
    next if ($testline =~ /^\s*#/); 
    next if ($testline =~ /^\s*$/);
    $testline =~ s/\s+/ /g;
    chomp $testline;
    my @parts = split(/ /, $testline);
    if (($testType eq	"") or grep(/$testType/, @parts)) {
      push @testList, $parts[0];
    }
  }
  return @testList;
}

### $outputLine = &GetRunsingleStatus( $runsingleOutputFile )
# GetRunsingleStatus takes one argument - a filename containing output
#  from runsingle. It will search though that file and return the last
#  line that looks like one of these two:
# <path/to/test> ........ *FAILED* [#] ...
# <path/to/test> ........  PASSED  [#] ...
#  if neither of these lines are found, or if there is a problem with
#  reading the file, it returns an error string.
sub GetRunsingleStatus {
  my $runsingleOutput = $_[0];

  # make sure the file exists
  unless (-f $runsingleOutput) {
    return "runsingle output file not found\n";
  }
  my $line = "";
  my @errors;
  # open it, returning error on error
  open (IN, $runsingleOutput) or return "Unable to read runsingle output\n";
  while (<IN>) {
    # if we find a failed msg, return it
    if (/^.* \*FAILED\*\s*\[[\/\d]+\]/) {
      $line = $_;
    # if we find a passed msg, return it
    } elsif (/^.* PASSED\s*\[[\/\d]+\]/) {
      $line = $_;
    # if we find an error msg, return it
    } elsif (/^.* \*ERROR\*\s*\[[\/\d]+\]/) {
      $line = $_;
    # if we find an error, hang onto it in case we need it
    } elsif (/^ERROR:/) {
      push @errors, $_;
    }
  }
  close(IN);
  # if we got an actual status line, return it
  if($line ne "") {
    return $line;
  # else return an error
  } else {
    return join(@errors)."No PASS/FAIL status found in runsingle output: ".
	"$runsingleOutput\n";
  }
}

### $shortTestName = &GetShortenedTestName( $testName, $maxLength )
# given a testname, shorten it to maxLength characters by replacing a
# portion of the test with .*
sub GetShortenedTestName {
  my ($TestName, $maxLength) = @_;
  unless($maxLength) {
    $maxLength = 48;
  }
  # remove whitespace from TestName
  $TestName =~ s/\s//g;

  my $TestNameLength = length($TestName);
  my $returnTestName = "";
  if ( $TestNameLength > $maxLength ) {
    ###  Shorten the TestName if it's over 48 characters long
    my $ReadStringStart = $TestNameLength - ($maxLength - 10);
    my $FrontString = substr($TestName, 0, 5) . ".*";
    my $RearString = substr($TestName, $ReadStringStart, 100);
    $returnTestName = "${FrontString}${RearString}";
  } else {
    $returnTestName = $TestName;
  }
  # get rid of whitespace in the outgoing str
  $returnTestName =~ s/\s//g;
  return $returnTestName;
}

### $paddedTestName = &GetPaddedTestName( $testName, $maxLength )
# given a testname and a max length, return a testname that is correctly
# formatted for runsingle/runlist output
sub GetPaddedTestName {
  my ($TestName, $maxLength) = @_;
  unless($maxLength) {
    $maxLength = 48;
  }

  my $padding = " ................................................";
  my $PaddedTestName = substr(&GetShortenedTestName($TestName).$padding, 0, $maxLength);
  my $format = "%.".$maxLength."s";
  return sprintf $format, $PaddedTestName;
}

### $bool = &cbuildExists( $carbonHomeDir )
# cbuildExists: used to return 0/1 if a cbuild exists
sub cbuildExists {

  my $dir = shift @_ ; 
 
  my $cbuild = "$dir/bin/cbuild" ; 

  if ( -e $cbuild ) { return 1 ; }
  else { return 0 ; } 

}

### $bool = &IsNetworkFriendly( [$dirToTest] )
# IsNetworkFriendly: used to return 0/1 
#   on if a directory is network friendly or not
#   returns 0 if not, 1 if it is friendly
sub IsNetworkFriendly {
   my $input = shift @_ ; 
   my $cwd ; 

   if ( $input ne "") { $cwd = $input ; } 
   else { $cwd = &CdsUtil::getSafeCwd(); }

   if ( $cwd =~ m#^/net|^/home|^/.automount|^/[no]/release/|^/w/|^/o/work#) {
     return 1 ;
   } else {
     return 0 ;
   }

}

#
# &IsValidTest($testname)
#   testname is string containing test name (ie, test/alias)
#   returns 0 if test is not valid, 1 if it is
#
#  THIS ROUTINE IS OBSOLETE. PLEASE USE scripts/TestList.pm INSTEAD
sub IsValidTest {
  my $testname = $_[0];

  #  try $CARBON_HOME/$testname
  if (-e "$ENV{CARBON_HOME}/$testname") { 
    #print "in home\n";
    return 1; 
  }

  #     $CARBON_HOME/obj/$CARBON_TARGET/$testname
  if (-e "$ENV{CARBON_HOME}/obj/$ENV{CARBON_TARGET}/$testname") { 
    #print "in target\n";
    return 1;
  }

  #  try $CARBON_HOME/obj/$arch.$CARBON_BIN/$testname
  my $arch = `$ENV{CARBON_HOME}/scripts/carbon_arch`; chomp $arch;
  if (-e "$ENV{CARBON_HOME}/obj/$arch.$ENV{CARBON_BIN}/$testname") {
    #print "in arch.bin\n";
    return 1;
  }

  #  try $CARBON_HOME/alltests.list
  if (grep(/$testname\s/, &GetTestsOfType())) {
    #print "in list\n";
    return 1;
  }

  #  try cvs repository on carbon via ssh
  `ssh carbon 'ls /home/cvs/repository/$testname'`;
  if ($? == 0) {
    #print "ssh as dir\n";
    return 1;
  }

  #  try cvs repository on carbon via ssh, appending ,v
  `ssh carbon 'ls /home/cvs/repository/$testname,v'`;
  if ($? == 0) {
    #print "ssh as ,v\n";
    return 1;
  }

  # doesn't look like it's valid...
  #print "not a test\n";
  return 0;
  
}

### $cksumStr = &GetChecksum( $filename )
#  returns the 64-bit CRC checksum value for specified file, the same value 
#  reported by 'sum -s'. returns -1 on error.
sub GetChecksum {
  my $file = $_[0];

  # will need to restore newline delimiter when done
  my $origDelim = $/;
  undef $/;

  # no file - reset record separator and return error
  unless (-e $file) {
    $/ = $origDelim;
    return -1;
  }
  # open it or reset record sep and return error
  unless(open(IN, $file)) {
    $/ = $origDelim;
    return -1;
  }

  # get the checksum
  my $cksum = unpack("%64C*", <IN>) % 65535;
  close(IN);

  # reset the record sep and return the cksum value
  $/ = $origDelim;
  return $cksum;
}

### $absPath = &AbsolutePath( $origPath )
#  returns an absolute path to the specified path, which may be relative or
#  already absolute. does not check that the destination exists
sub AbsolutePath {
  my $inPath = shift;

  if ($inPath =~ /^\//) 
  {
    return $inPath;
  } else {
    return &CdsUtil::getSafeCwd()."/".$inPath;
  }
}

### 1 = &EVhasString( $envVar, $string, $delimiter )
# check ev to see if it alread contains a given string, and adds it with
# the delimiter at the front
sub EVhasString {
    my ($ev, $string, $delim) = @_;

    if (exists $ENV{$ev}) {
	($ENV{$ev} = $string . $delim . $ENV{$ev}) unless $ENV{$ev} =~ m/$string/;
    } else {
	$ENV{$ev} = $string . $delim;
    }
    1;
}
	
### $bool = &Pconfirm( $question )
# pair of standardized confirm utilities
#  both take a text string and append a ? [Y/N] on it.
#
#   Pconfirm("question") returns 1 if the answer starts with Y|y
#   Nconfirm("question") returns 0 if the answer starts with Y|y
#
#   Pconfirm is positive confirm, Nconfirm is negative confirm, if wondering
sub Pconfirm  {
 
   my $question = shift @_ ;
 
   my $line ; 
 
   print "$question? [Y/N] " ;
 
   $line = <STDIN> ;
   if ($line =~ /^y/i) { return 1 ; }
   else { return 0 ; }
 
}
 
### $bool = &Nconfirm( $question )
sub Nconfirm  {
 
   my $question = shift @_ ;
 
   my $line ; 
 
   print "$question? [Y/N] " ;
 
   $line = <STDIN> ;
   if ($line =~ /^y/i) { return 0 ; }
   else { return 1 ; }
 
}

### $sizeInKb = &GetFreeSpace( [$dir] )
#  using df and the specifier directory, return, in kilobytes, the amount
#  of available disk space on the filesystem.
#  if no dir is specified, uses '.'
#  returns -1 on error
sub GetFreeSpace {
  my $dir = $_[0];
  unless ($dir) {
    $dir = ".";
  }

  my $options = "-k";
  if(`uname -s` =~ m/^Linux/) {
    $options .= "P";
  }
  open (DFOUTPUT, "df $options $dir |") or return -1;
  my $line;
  while(<DFOUTPUT>) {
    $line = $_;
  }
  close(DFOUTPUT);
  return int( (split(/ +/, $line))[3]);
 
}

### 1 = &RemoveStickyTags( [$dir] )
#  traverse a file tree and remove any cvs sticky tags
sub RemoveStickyTags {
  my $dir = $_[0];
  unless ($dir) {
    $dir = ".";
  }
  if (exists($ENV{'RUNLIST_DEBUG'})) {
    print "Removing sticky date tags from $dir\n";
  }

  find ( sub {
  	   # don't do anything unless we're in a CVS dir
	   if($File::Find::dir =~ /CVS\/?$/) {

	     # first delete the Tag file if it contains a DATE sticky tag
 	     #  (but not if it contains a symbolic tag)

	     if ($_ eq 'Tag') {
	       if(open(TAGIN, "Tag")) {
	         my $tag = <TAGIN>;
		 close(TAGIN);
	         if($tag =~ /^D20\d\d/) 
		 {
		     if ($Debug)
		     {
			 print STDERR "Unlinking sticky tag file $File::Find:$dir/$_ because it has $tag in it.\n";
		     }
		     unlink("Tag");
		 }
               } 

             # then for the entries file, strip the date sticky tags
             } elsif ($_ eq 'Entries') {
	       # read in the entries
	       if(open(ENTRIESIN, "Entries")) {
		 my $changed = 0;
		 if(open(ENTRIESOUT, ">Entries.new")) {
		   foreach my $line (<ENTRIESIN>) {
		     #strip it, noting that it changed
		     !($line =~ s#/D20\d\d.*$#/#) or $changed = 1;
		     print ENTRIESOUT $line;
                   }
	           close(ENTRIESIN); close(ENTRIESOUT);

		   # if it changed, use the new one
		   if($changed) {
		     unlink("Entries");
		     rename("Entries.new", "Entries");

		   # if not, use the old one
		   } else {
		     unlink("Entries.new");
		   }
		 } else {
		   close(ENTRIESIN);
		 }
               }
             }
           }
         }, $dir);
}

### $bool = &isXML( $filename )
# isXML - accepts one arg - a filename. look at 1st line of file contents to
# determine if file is XML. return 1 if yes, 0 if no, -1 on error
sub isXML {
  my ($file) = @_;
  open(IN, $file) or return -1;
  my $firstLine = <IN>;
  close(IN);
  # empty? return 0
  unless($firstLine) { return 0; }
  return ($firstLine =~ m/^\s*<\?xml /i);
}

### @sortedUniqedArray = &sortuniq( @rawArray )
# does a sort and a uniq on an array and returns the resulting array
sub sortuniq {
    my %hash = map { ($_,0 ) } @_;
    return sort keys %hash;
}

### $hash1KeysAddedInt = &pushHash( \%hashOrig, \%hashNew, $overwriteBool )
# add all key/values referenced by hash2 to hash referenced by hash1
# if overwrite is set, will use new value. if not, use old value
# return number of keys added
sub pushHash {
  my ($hash1, $hash2, $overwrite) = @_;
  my $count = 0;

  foreach my $iKey (keys(%$hash2)) {
    if (exists($hash1->{$iKey}) and not $overwrite) {
      next;
    }
    $hash1->{$iKey} = $hash2->{$iKey};
    $count++;
  }

  return $count;
}

### \%subTreeHash = &FindSubTree( \%treeHash, $searchKeyStr )
# will return the hash that contains the subtree as referenced by $searchStr
sub FindSubTree {
  my ($hashRef, $searchStr) = @_;
  foreach my $iKey (keys(%$hashRef)) {
    if($iKey eq $searchStr) {
      return $hashRef;
    }
    if(ref($hashRef->{$iKey}) eq "HASH") {
      my $nextRef = &FindSubTree($hashRef->{$iKey}, $searchStr);
      unless ($nextRef == 0) { return $nextRef; }
    } 
  }
  return 0;
}

### $path = &absLinkPath ( $linkPath )
# given a sym link, return its detination path as an absolute path
# return "" on error
sub absLinkPath {
  my ( $linkPath) = @_;

  # if it's not a link, return
  unless(-l $linkPath) { return ""; }

  # where did we start?
  my $lStartDir = &CdsUtil::getSafeCwd();

  # go to the dir containing the link
  chdir(&AbsolutePath(dirname($linkPath)));

  # the link
  my $lLinkLocalPath = basename($linkPath);

  # read it
  my $lLinkDest = &AbsolutePath(readlink($lLinkLocalPath));
  my $count = 0;
  # while it points to another link, go and read that one as well
  while (-l $lLinkDest) {
    chdir(&AbsolutePath(dirname($lLinkDest)));
    $lLinkDest = &AbsolutePath(readlink(basename($lLinkDest)));
    # protect from self-referential links. 10 levels should be enough
    if ($count > 10) {
      chdir($lStartDir);
      return "";
    }
    $count++;
  }
  # go back from whence we came
  chdir($lStartDir);
  return $lLinkDest;
}

### $status = &replaceLink ( $linkPath )
# given a sym link, replace it with a copy of the file it points to
sub replaceLink {
  my ( $linkPath ) = @_;

  # where did we start?
  my $lStartDir = &CdsUtil::getSafeCwd();

  my $lLinkDest = &absLinkPath($linkPath);
  if ($lLinkDest eq "") { return 1; }

  # go to the dir containing the link
  chdir(&AbsolutePath(dirname($linkPath)));
  # the link
  my $lLinkLocalPath = basename($linkPath);
  
  unlink($lLinkLocalPath) or return 0;
  my $lFileStat = (stat(&AbsolutePath($lLinkDest)))[2] & 00777;
  copy(&AbsolutePath($lLinkDest), $lLinkLocalPath) or return 0;
  chmod($lFileStat, $lLinkLocalPath);
  chdir($lStartDir);
  return 1;
}

### $status = &makeLinkRelative ( $argLink )
sub makeLinkRelative {
  my ( $argLink ) = @_;

  my $linkPath = readlink( $argLink ) or return 0;
  my $newLinkPath = &makePathRelative( $linkPath );

  # out with the old ....
  unlink($argLink) or return 0;
  # ... and in with the new
  symlink($newLinkPath, $argLink) or return 0;
  return 1;
}

### $string = &makePathRelative ( $argPath )
sub makePathRelative {
  my ( $argPath ) = @_;
 
  # if $argPath starts with ., return it
  if ($argPath =~ m#^\.#) {
    return $argPath;
  } 

  my @lPwd = split('/', &CdsUtil::getSafeCwd());

  # strip trailing slash off arg
  $argPath =~ s#/$##;
  # strip multiple slashes
  $argPath =~ s#/+#/#g;

  # if they are equal, 
  # if link does not start with '/', then return 1
  unless( $argPath =~ m#^/# ) {
    return 1;
  }
  my @lDir = split('/', $argPath);

  # since both started with '/', both lists start with an empty str
  shift @lDir; shift @lPwd;

  # if the first part of the paths aren't the same, then it has to stay
  # absolute, so return the original path
  unless($lPwd[0] eq $lDir[0]) {
    return $argPath;
  }

  # for each part that is the same, shift it
  while((scalar(@lPwd)) and ($lPwd[0] eq $lDir[0])) {
    shift @lPwd; shift @lDir;
  }

  # then replace each part in lPwd with ..
  for (my $i; $i < scalar(@lPwd) ; $i++ ) {
    $lPwd[$i] = "..";
  }

  # now if we string them all together, we get the new link path
  my $retStr = join('/', @lPwd, @lDir);

  # if it's empty, then it means we are in the directory, replace str with '.'
  if ( $retStr =~ m/^\s*$/ ) {
    $retStr = ".";
  }

  return $retStr;

}

### $shortenedPath = &shortcircuit ( $argPath )
# if $argPath is a local, automounted path, shortcircuit returns the local
# path. if not, the original arg is returned.
sub shortcircuit {
  my ($carbonHome) = @_;

  # No short circuiting needed on Windows.
  return $carbonHome if (`uname` =~ /^CYGWIN/);

  my $file;
  my $dir;
  # if we were passed a file, then split it so we can get the dir
  unless(-d $carbonHome) {
    $file = basename($carbonHome);
    $dir = dirname($carbonHome);
  } else {
    $dir = $carbonHome;
  }

  # so we can go back
  my $startDir = &CdsUtil::getSafeCwd();
  chdir($dir) or return $carbonHome;

  # look through each alias for this host (being mindful of things like 
  # 'ytterbium' and 'terbium' being different machines...)
  my @host_aliases = ();
  chomp(my $hostname = `hostname`);
  $hostname =~ s/\.carbondesignsystems\.com//;
  my @out = `ypcat -k hosts |grep $hostname`;
  for my $line (@out) {
    my @parts = split(/\s+/, $line);
    unless(grep(/^$hostname$/, @parts)) {
      next;
    }

    # got an alias in $parts[0]
    my $alias = $parts[0];
    my $lDir = &CdsUtil::getSafeCwd();

    # if it's an automounted work dir, shortcircuit
    if($lDir =~ m#^/.automount/$alias/root/work/#) {
      chdir($startDir);
      $lDir =~ s#^/.automount/$alias/root##;
      if($file) { return "$lDir/$file"; }
      else { return $lDir; }
    } elsif ($lDir =~ m#^/net/$alias/work/#) {
      chdir($startDir);
      $lDir =~ s#^/net/$alias##;
      if($file) { return "$lDir/$file"; }
      else { return $lDir; }
    } elsif ($lDir =~ m#^/w/$alias/#) {
      chdir($startDir);
      $lDir =~ s#^/w/$alias#/work#;
      if($file) { return "$lDir/$file"; }
      else { return $lDir; }
    }
  }
  # found nothing good, so just return orig arg
  chdir($startDir);
  return $carbonHome;

}

### $cwd = getSafeCwd()
# takes no args, returns the pwd, substituting for any wacky paths
# like /n/release to /o/release
sub getSafeCwd {
  my $lRetStr = "";
  $lRetStr = getcwd();
  unless($lRetStr) {
    $lRetStr = `pwd`;
    chomp $lRetStr;
  }

  # translate release dir
  $lRetStr =~ s#/n/release#/o/release#;
  # translate /usr/work to /work
  $lRetStr =~ s#/usr/work#/work#;
  # translate the amd paths
  $lRetStr =~ s#/amd_tmp/netapp101/vol/vol2/#/o/#;
  $lRetStr =~ s#/amd_tmp/netapp101/vol/vol1/home/#/home/cds#;
  $lRetStr =~ s#/amd_tmp/(.*?)/work#/w/$1#;

  return $lRetStr;
}

### $gccLibPath = getGccLibPath()
# takes no args, returns full path to a dir containing the req'd gcc libs, to 
# then be added to LD_LIBRARY_PATH. assumes CARBON_HOME and CARBON_TARGET are
# set. returns empty string if none found
sub getGccLibPath {
  # obj/Linux.product/Makefile.compiler has the compiler vers in it
  my $inMakefile = $ENV{'CARBON_HOME'}."/obj/".$ENV{'CARBON_TARGET'}.
    "/Makefile.compiler";
  unless(-f $inMakefile) {
    return "";
  }

  # read the info from the file
  my $compiler; my $gcc_version; my $other_version;
  open(MKIN, $inMakefile) or return "";
  while(<MKIN>) {
    chomp;
    if (/^COMPILER_TARGET = (.*)$/) {
      $compiler = $1;
    }
    if (/gcc_VERSION = (.*)$/) {
      $gcc_version = $1;
    }
    if (/${compiler}_VERSION = (.*)$/) {
      $other_version = $1;
    }
  }

  # if we read real values, assemble a path
  my $pathToReturn = "";
  if($compiler and ($gcc_version or $other_version)) {
    # what arch?
    if($ENV{'CARBON_TARGET'} =~ /^SunOS/) {
      $pathToReturn = "/tools/solaris/$compiler-$gcc_version/lib";
    } elsif ($ENV{'CARBON_TARGET'} =~ /^Linux64/) {
      $pathToReturn = "/tools/linux64/$compiler-$gcc_version/lib";
    } elsif ($ENV{'CARBON_TARGET'} =~ /^Win/) {
      # windows should get the mingw path
      $pathToReturn = "/tools/linux/gcc-$other_version/lib";
    } elsif (($ENV{'CARBON_TARGET'} =~ /^Linux\./) and ($compiler eq "icc")) {
      $pathToReturn = "/tools/linux/$compiler-$other_version/lib";
    } elsif (($ENV{'CARBON_TARGET'} =~ /^Linux\./) and ($compiler eq "gcc")) {
      $pathToReturn = "/tools/linux/$compiler-$gcc_version/lib";
    }
  }

  # make sure it exists, then return it
  if (-d $pathToReturn) {
    return $pathToReturn;
  } else {
    return "";
  }
  
}

### $min = min(\@numbers)
# given a ref to a list of numbers, return the lowest (does not format)
sub min {
  my ($numbers) = @_;

  unless($numbers) {
    return 0;
  }

  my $minVal;
  for(my $i = 0; $i < scalar(@$numbers); $i++) {
    if(($i == 0) or($$numbers[$i] < $minVal)) {
      $minVal = $$numbers[$i];
    }
  }
  return $minVal;
}

### $avg = avg(\@numbers)
# given a ref to a list of numbers, return the avg of the numbers
sub avg {
  my ($numbers) = @_;

  # return 0 if no numbers given
  my $count = scalar(@$numbers);
  if($count < 1) {
    return 0;
  }

  my $total = 0;
  foreach my $num (@$numbers) {
    $total += $num;
  }
  return ($total / $count);
}

### $percent = percentChange($num1, $num2)
# return percent (with +/-) difference of num2 from num1
sub percentChange {
  my ($num1, $num2) = @_;

  # if num1 is 0, return 0
  if($num1 == 0) { return 0; }

  return ($num2 - $num1) / $num1 * 100;
}


### $error = sendEmail($subject, $msg, $emailsRef)
# send email to addresses in emailsRef (ref to list) with given subject and
# msg. double quotes not allowed in subject. return error string, or empty
# string on success.
sub sendEmail {
  my ($subject, $msg, $emailsRef) = @_;
  
  # make sure no double quotes are in $subject
  if($subject =~ m/\"/) {
    return "Illegal double quotes found in subject: $subject";
  }

  # need at least one email address
  if(scalar(@$emailsRef) < 1) {
    return "No email addresses supplied";
  }

  # where is mail executable?
  my $mailExec;
  my $arch = `uname -s`; chomp $arch;
  if ($arch =~ /SunOS/) {
    $mailExec = "/usr/ucb/mail";
  } else {
    $mailExec = "/bin/mail";
  }

  # send email
  open(EMAIL, "|$mailExec -s \"$subject\" ".join(" ", @$emailsRef)) or
    return("Can't send mail: $!\n");
  print EMAIL $msg."\n";
  close EMAIL;

  return "";
}

### $error = sendHtmlEmail($subject, $txtMsg, $htmlMsg, $emailsRef)
# send email to addresses in emailsRef (ref to list) in mulipart mime format
# with given subject. txtMsg if present  will be sent as text/plain, and 
# htmlMsg will be sent as text/html. if only text is needed, use sendEmail
# double quotes not allowed in subject. return error string, or empty
# string on success.
sub sendHtmlEmail {
  my ($subject, $txtMsg, $htmlMsg, $emailsRef) = @_;

  # use sendmail to send multipart msgs
  my $mailExec = "sendmail";
  my $mailArg = "-t";
  # try to give full path in case sendmail not in path
  my @searchPaths = qw( /usr/sbin /usr/bin /sbin /bin );
  foreach my $path (@searchPaths) {
    if (-e "$path/$mailExec") {
      $mailExec = "$path/$mailExec";
      last;
    }
  }
  my $mailCmd = "$mailExec $mailArg";
  my $me = $ENV{USER} . '@carbondesignsystems.com';

  # use srand to construct a boundary for mime parts
  srand(time());
  my $boundary = sprintf("_____%6f%8f__", rand(999999), $$);

  # construct header
  my $msgBody = "From: $me\n";
  $msgBody .= "To: " . join(', ', @$emailsRef) . "\n";
  $msgBody .= "MIME-Version: 1.0\n";
  $msgBody .= "Content-Type: multipart/alternative; boundary=\"$boundary\"\n";
  $msgBody .= "Subject: $subject\n\n";

  # only include text part if txtMsg was given
  if ($txtMsg) {
    $msgBody .= "--$boundary\n";
    $msgBody .= "Content-Type: text/plain; charset=\"iso-8859-1\"\n\n";
    $msgBody .= $txtMsg . "\n\n";
  }
 
  $msgBody .= "--$boundary\n";
  $msgBody .= "Content-Type: text/html; charset=\"iso-8859-1\"\n\n";
  $msgBody .= $htmlMsg . "\n\n";
  $msgBody .= "--$boundary--\n";

  # send it 
  open(EMAIL, "|$mailCmd") or return("Can't send mail using $mailCmd: $!\n");
  print EMAIL $msgBody . "\n";
  close(EMAIL) or return("Can't send mail using $mailCmd: $!\n");

  return "";
}

### $num = lineCount ($srcFile) 
# given a c/c++ source file, return the number of non-comment lines containing
# at least one alphanumeric character
sub lineCount {
  my ($file) = @_;

  open(IN, $file) or return -1;
  my $inComment = 0;
  my $lineCount = 0;
  while(<IN>) {
    # skip whitespace
    if (/^\s*$/) {  next; }
    # skip single-line comments
    if (/^\s*\/\//) { next; }

    # if /* is on this line, start comment (unless also */ is here)
    if ((m#(\w*).*?/\*#) and not $inComment) {
      $inComment++ unless (m#\*/#);
      # if anything is before comment, count it
      if ($1) { $lineCount++; }

    # if */ is on this line, end comment
    } elsif ((m#\*/.*?(\w*)#) and $inComment) {
      $inComment--;
      # if anything is after comment, count it
      if ($1) { $lineCount++; }

    # already in comment, skip it
    } elsif ($inComment) {
      next; 

    # no multiline comments - have something
    } else {
      # strip non-leading single-line comments
      my $line = $_;
      $line =~ s#//.*$##;
      # now if it has at least one alphanumeric, count it
      if ($line =~ m/\w/) {
        $lineCount++;
      }
    }
  }
  close(IN);
  return $lineCount;
}

### $version = getCvsVersion($file)
# given a file, return the version of it, or 0 if unable to
sub getCvsVersion {
  my ($file) = @_;

  unless (-e $file) {
    return "0";
  }
  my $version = 0;
  # first, try getting version from dirname($file)/CVS/Entries
  my $dir = dirname($file);
  if(-e "$dir/CVS/Entries") {
    my $base = basename($file);
    $version = `grep /$base/ $dir/CVS/Entries`;
    unless($?) {
      return (split(/\//, $version))[2];
    }
  }
  # or maybe from cvs status
  my @output = `cvs -q status $file`;
  #print @output;
  foreach my $line (@output){
    if ($line =~ /Working revision:\s+(.*)$/) {
      return $1;
    }
  }
  return "0";
}

### $boolean = isFileOnReleaseBranch($file)
# if the given file is checked out to a release branch tag on CVS, return branch
# otherwise, return "". on error, return -1
sub isFileOnReleaseBranch {
  my ($file) = @_;

  unless (-e $file) {
    if ($Debug) {
      print "ERROR: $file does not exist\n";
    }
    return -1;
  }

  # get the version, split it into numbers
  my $fileVers = &getCvsVersion($file);
  chomp $fileVers;
  my @versParts = split(/\./, $fileVers);

  # if we got fewer than 2 parts to the number, error
  if (scalar(@versParts) < 2) {
    if ($Debug) {
      print "ERROR: ".join ('##', @versParts). "\n";
    }
    return -1;
  # if we have fewer than 4, must be mainline, so answer is no
  } elsif (scalar(@versParts) < 4) {
    return "";
  # anything else could be a real branch number
  } else {
    my $baseVers = $versParts[0] . "." . $versParts[1];
    foreach my $line (`cvs log -h src/util/UtVersion.cxx |grep ':\\W$baseVers\$'`) {
      chomp $line;
      $line =~ s/\s+//g;
      my @parts = split(/:/, $line);
      # match things like C2007_01_2345 (will most likely be a BASE_ tag)
      if ($parts[0] =~ /C20\d\d_\d\d_./) {
        my $branch = $parts[0];
        $branch =~ s/^BASE_//;
        $branch =~ s/^BR_//;
        return $branch;
      }
    }
    # haven't found a release tag yet, must not be one
    return ""; 
  }
}

sub writeBranchCommitInfo 
{
    # Function write an xml snippet for release notes displayed on intranet.

    my ($bugList, $branch, $user, $number, $date, $commitInfoPath) = @_;
    chomp (my $shortDate = `date '+%y%m%d-%H%M' -d '$date'`);
    my $rtn = 0;

   # We'll make the commitInfoPath an option which can be passed in.  
   # This way we can test this function without disturbing the real path.
   # Default to real area, only tests would pass a different one.

    if (! $commitInfoPath)
    {
	$commitInfoPath = "/home/cds/qa/data/branchcommits";
    }
    
    my $branchPath = $commitInfoPath."/".$branch;
    my $xmlWriteFile = "$branchPath/commit.$user.$shortDate.xml";
    
    if (! -d $branchPath)
    {
	# make this branch's area if it doesn't already exist
	
	if ((&MkdirP($branchPath,775)) == 0)
	{
	    # Couldn't setup new branchPath, write info into CARBON_HOME so
	    # we can recover manually afterwards.
	    
	    $xmlWriteFile = "$ENV{CARBON_HOME}/commit.$user.$shortDate.xml";
	    
	    print STDERR "Write directory problem. Will write xml data to secondary location:\n",
	    "$xmlWriteFile\n";
	}
    }

# Write the file....

    my $wh = new FileHandle;
    
    if (! open($wh,">$xmlWriteFile"))
    {
	print STDERR "Unable to create $xmlWriteFile\n";
    }
    else
    {
	# write all the info we care about
	foreach my $bugNum (@$bugList) 
	{
	    print $wh "<bug number=\"$bugNum\">\n",
	    "  <user>$user</user>\n",
	    "  <commit>$number</commit>\n",
	    "  <date>$date</date>\n",
	    "</bug>\n";
	}
	
	write "$xmlWriteFile";
	close ($wh);
	$rtn = chmod oct(664), $xmlWriteFile;
    }
    
    return $rtn;
}

### $hashRef = envVarHashFromArgs(@varArgs)
# given strings like 'VAR1=val1', 'VAR2 = val2', return hash like VAR1 => val1,
# VAR2 => val2
sub envVarHashFromArgs {
  my @args = @_;
  my %envHash = ();
  foreach my $arg (@args) {
    my ($var, $val) = split(/=/, $arg);
    # chop space from end of var, begin of val
    chomp($var); $val =~ s/^\s+//;
    $envHash{$var} = $val;
  }
  return \%envHash;
}

### fisher_yates_shuffle( \@array ) :
# from 'man perlfaq4'
# generate a random permutation of @array in place
sub fisher_yates_shuffle {
    my $array = shift;
    my $i;
    for ($i = @$array; --$i; ) {
        my $j = int rand ($i+1);
        next if $i == $j;
        @$array[$i,$j] = @$array[$j,$i];
    }
}

### @aliases = getHostnameAliases($host)
# return all known (to yp) aliases for given hostname
sub getHostnameAliases {
  my $hostname = shift;
 
  my $ypcatOut = `ypcat -k hosts | grep '^$hostname '`; 
  chomp($ypcatOut);
  my @aliases = split(/\s+/, $ypcatOut);
  return @aliases;
}

### $shortname = getShortestHostnameAlias ($host)
# given a hostname, return the shortest possible hostname alias for that machine
sub getShortestHostnameAlias {
  my $hostname = shift;
  my @aliases = sort { length($a) <=> length($b) }
                     &getHostnameAliases($hostname);
  return $aliases[0];
}

# given a ref to list, if the list has 1 empty value, return a ref to an
# empty list. otherwise, return the ref
sub truncateEmptyList {
  my ($listIn) = @_;
  if ((scalar(@$listIn) == 1) and ($$listIn eq "")) {
    return [];
  } else {
    return $listIn;
  }
}

# END
1;
