#!perl

package CdsRelease;

## This perl package contains utility functions for creating a carbon release
##
## &MakeBillOfMaterials($bomFile, $directory) -
##     creates a bill of materials file for a release
## &DiffBillOfMaterials($oldFile, $newFile) - 
##     returns a hash of hashes of lists representing the diffs of 2 bomFiles
## &MakeTestLicense($filestem) -
##     creates test license files $filestem.cdb and $filestem.lic
## &MakePackage($testBomFile, $destTgzFile)
##     creates a package in $destTgzFile from files in $testBomFile
##

use strict;
use File::Find;
use File::Basename;
use CdsUtil;

use lib "$ENV{CARBON_HOME}/scripts";
use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
use CDSSystem;


my $Debug = 0;

### &MakeBillOfMaterials
###  given a filename and directory, generate a bill of materials for that
###  directory by cding to that dir and traversing the file tree, reporting
###  the type, filename, mode, size, cksum and mtime (as returned from stat).
###  the filename is relative to the specified dir (inclusive).
###  return 1 on success, 0 on failure
sub MakeBillOfMaterials {
  my ($bomFile, $dir) = @_;

  if (-e $bomFile) {
    print STDERR "$bomFile already exists in \&CdsUtil::MakeBillOfMaterials\n";
    return 0; 
  }
  unless (-d $dir) {
    print STDERR "$dir is not a directory in \&CdsUtil::MakeBillOfMaterials\n";
    return 0;
  }

  # open the b.o.m. file
  unless (open (BOM, ">$bomFile")) {
    print STDERR "Could not create $bomFile in \&CdsUtil::MakeBillOfMaterials\n";
    return 0;
  }

  # traverse the specified dir, executing the in-line subroutine on each file
  chdir($dir);
  find ( sub {
           # skip the '.' dir
	   if ($_ =~ m/^\.$/) { return 0; }

	   # print a character for the file type
 	   if (-f) { print BOM "f "; }    # file
 	   elsif (-d) { print BOM "d "; } # directory
 	   elsif (-l) { print BOM "l "; } # link
	   else { print BOM "u "; }       # unknown - this shouldn't happen

           # get the rest of the info
           my @fStat = stat($_) or 
	   print STDERR "cannot stat $File::Find::name in MakeBillOfMaterials: $!\n";
	   # to get mode, and with bit mask to discard file type info, then
           # convert to octal before printing mode
           # [2] is mode, [7] is size, [9] is mtime
           print BOM $File::Find::name . " " . sprintf("%lo", $fStat[2] & 00777)
             . " " .  $fStat[7] .  " " . &CdsUtil::GetChecksum($_) . " ". 
             gmtime($fStat[9]) . "\n";
         }, ".");

  close(BOM);
  return 1;

}

### &DiffBillOfMaterials
###  given 2 filenames containing bills of materials of the form:
### ./path/to/file <mode> <size> <date-from-gmtime()>
###  compare them and return a hash of hash of arrays like this:
###    diffedfile => { 'mode' => (<oldmode>, <newmode>) ,
###                    'size' => (<oldsize>, <newsize>),
###                    'mtime' => (<oldmtime>, <newmtime>) }
###  on error, returns '1' => "error msg"
sub DiffBillOfMaterials {
  my ($oldBomFile, $newBomFile) = @_;
  my %diffs;

  unless (-f $oldBomFile) {
    return ( '1' => "$oldBomFile does not exist" );
  }
  unless (-f $newBomFile) {
    return ( '1' => "$newBomFile does not exist" );
  }

  # read in the old one
  open (OLD, $oldBomFile) or return ( '1' => "cannot open $oldBomFile" );
  my @oldBomLines = <OLD>;
  close (OLD);

  # open the new one
  open (NEW, $newBomFile) or return ( '1' => "cannot open $newBomFile" );
  foreach my $newLine (<NEW>) {
    # try to escape special chars in filenames
    (my $newSearch = $newLine) =~ s/\+/\\\+/g;

    my @oldLine;
    # look for this line in the old output, and zero this element to note that
    # we've seen it if we find it
    grep { 
      if(/$newSearch/) {
        push(@oldLine, $_);
        $_ = "";
      }
    } @oldBomLines;
    # lines match, therefore files match - move on
    if (@oldLine == 1) {
      next;

    # more than one line matches - big problems, just bail out
    } elsif (@oldLine > 1) {
      return ( '1' => "$oldBomFile contains duplicate lines" );

    # no match, therefore diff. process and report
    } else {
      # split the new file attrs
      chomp ($newLine);
      my ($newType, $newFile, $newMode, $newSize, $newCksum, $newMtime) =
          split (/\s+/, $newLine, 6);
      ($newSearch = $newFile) =~ s/\+/\\\+/g;

      # look for the filename in the old file and zero this element to note that
      # we've seen it if we find it
      @oldLine = ();
      grep {
	if (/^\w $newSearch /) {
	  push(@oldLine, $_);
	  $_ = "";
        }
      } @oldBomLines;

      # found it 
      if (@oldLine == 1) {
	# split to get old file attrs
        chomp($oldLine[0]);
        my ($oldType, $oldFile, $oldMode, $oldSize, $oldCksum, $oldMtime) = 
		split(/\s+/, $oldLine[0], 6);

        # now make sure if its the size that's different, type is
        # not 'd' (directory)
        if (($oldType eq "d") and ($newType eq "d") and
            ($oldMode == $newMode)) { next; }

        # and make sure mtime is not the only diff
        if (($oldType eq $newType) and ($oldMode == $newMode) and
            ($oldSize == $newSize) and ($oldCksum == $newCksum)) { next;}

	# put everything in the hash
	$diffs{$oldFile}{'type'} = [$oldType, $newType];
	$diffs{$oldFile}{'mode'} = [$oldMode, $newMode];
	$diffs{$oldFile}{'size'} = [$oldSize, $newSize];
        $diffs{$oldFile}{'cksum'} = [$oldCksum, $newCksum];
	$diffs{$oldFile}{'mtime'} = [$oldMtime, $newMtime];

        # zero this element to note that we've seen it
        $oldLine[0] = "";

      # file listed more than once in bom - very very bad...
      } elsif (@oldLine > 1) {
	return ( '1' => "$newFile listed in $oldBomFile more than once" );

      # new file not in old bom file
      } else {
	# put in hash with 0s for old data
        $diffs{$newFile}{'type'} = [0, $newType];
	$diffs{$newFile}{'mode'} = [0, $newMode];
	$diffs{$newFile}{'size'} = [0, $newSize];
        $diffs{$newFile}{'cksum'} = [0, $newCksum];
	$diffs{$newFile}{'mtime'} = [0, $newMtime];
      }
    }
  }
  close(NEW);

  # now we're left with an @oldBomLines that has only empty elements and 
  # files that are missing from the new bom
  foreach my $missingFile (@oldBomLines) {
    if ($missingFile eq "") { next; }
    chomp($missingFile);
    # split to get attrs
    my ($oldType, $oldFile, $oldMode, $oldSize, $oldCksum, $oldMtime) =
        split(/\s+/, $missingFile, 6);
    # record in hash with 0s for new data
    $diffs{$oldFile}{'type'} = [$oldType, 0];
    $diffs{$oldFile}{'mode'} = [$oldMode, 0];
    $diffs{$oldFile}{'size'} = [$oldSize, 0];
    $diffs{$oldFile}{'cksum'} = [$oldCksum, 0];
    $diffs{$oldFile}{'mtime'} = [$oldMtime, 0];
  }

  return %diffs;
}

### &MakeTestLicense($filestem)
###  will create $filestem.lic and $filestem.cdb which will be a very simple
###  test license and cdb file. returns 1 on success, 0 on error (and logs
###  error to stderr
###  will also start the license server on localhost if $RELEASE_TEST is set
sub MakeTestLicense {
  my ($destinationFile, $carbonHome) = @_;
  unless($carbonHome) {
    $carbonHome = $ENV{'CARBON_HOME'};
  }
  my $arch = `$carbonHome/bin/carbon_arch`; chomp $arch;
  my $flexRoot = "/tools/flexlm/v10.1";
  my $TestCompany = "TestRelease";

  if ( -e $destinationFile ) {
    print STDERR "Please specify a destination file to \&MakeTestLicense for the license that does not exist\n";
    return 0;
  }

  # make file absolute
  unless ($destinationFile =~ m#^/#) {
    my $cwd = &CdsUtil::getSafeCwd();
    $destinationFile = $cwd."/".$destinationFile;
  }

  my $destinationLic = $destinationFile.".lic";
  my $destinationCdb = $destinationFile.".cdb";

  my $tmpdir = ".tmplicdir.$$";
  unless(mkdir($tmpdir)) { 
    print STDERR "Can't create $tmpdir in \&MakeTestLicense\n";
    return 0;
  }
  unless(chdir($tmpdir)) {
    print STDERR "Can't chdir to $tmpdir in \&MakeTestLicense\n";
    return 0;
  }

  # set up path to flexlm stuff, and get host id for use in lic file
  my $mhostid; my $flexArch;
  if ($arch eq "Linux") {
    $flexArch = "i86_r6";
    $mhostid = `/sbin/ifconfig | grep eth0 | awk '{ print \$5 }' | sed -e 's/://g'`; chomp $mhostid;
  } elsif ($arch eq "Win") {
    $flexArch = "i86_r3";
    $mhostid = `/sbin/ifconfig | grep eth0 | awk '{ print \$5 }' | sed -e 's/://g'`; chomp $mhostid;
    print STDERR "(Placeholder): Windows license not supported yet\n";
    return 0;
  } elsif ($arch eq "SunOS") {
    $flexArch = "sun4_u5";
    $mhostid = `hostid`; chomp $mhostid;
  } else {
    print STDERR "Unsupported architecture ($arch)\n";
  }

  my $flexBin = $flexRoot."/".$flexArch;

  # get hostname for use in lic file
  my $name = `uname -n`; chomp $name;

  # create the input .lic.txt
  my $testlic = "test.lic";
  unless(open(TESTLIC, ">$testlic")) {
    print STDERR "Can't create $testlic: $!\n";
    return 0;
  }
  print TESTLIC <<EOF;
SERVER $name $mhostid
USE_SERVER
VENDOR carbond
FEATURE cds_speedcompiler_dev carbond 1.0 permanent 1 SIGN=
FEATURE cds_designplayer_dev carbond 1.0 permanent 1
VENDOR_STRING="cdsdpdev_$TestCompany" SIGN=
EOF

  close(TESTLIC);

  # create the input .cdb.txt
  my $testcdb = "test.cdb";
  unless(open(TESTCDB, ">$testcdb.txt")) {
    print STDERR "Can't create $testcdb: $!\n";
    return 0;
  }
print TESTCDB <<EOF;
VERSION 1
COMPANY TestRelease
EOF

  close(TESTCDB);

  # generate the cdb file
  CDSSystem::System "$carbonHome/bin/carbon_exec compiler_driver/ccrypt $testcdb.txt $testcdb";

  # generate the lic file
  CDSSystem::System "$flexBin/lmpath -add carbond $testlic";
  CDSSystem::System "$flexBin/lmcrypt $testlic";
  #CDSSystem::System "$flexBin/lmgrd -c $testlic -l $testlic.log";

  # put them where the user said to put them
  rename($testlic, $destinationLic);
  rename($testcdb, $destinationCdb);
  chdir "..";
  CDSSystem::System "rm -rf $tmpdir";

  # if $RELEASE_TEST is set, start the license server in the background
  if ($ENV{RELEASE_TEST} ) {
    #print "Will try to stop any running flex servers for this license...\n";
    #CDSSystem::System "$flexBin/lmdown -q -c $destinationLic";
    #CDSSystem::System "$flexBin/lmgrd -c $destinationLic &";
  }
  return 1;
}

### \%files = GetFilesFromBom(bomfile, quiet)
###  valid lines in the bom file are:
###  d <dirname>  # include this entire directory, except any CVS subdirs
###  h <dirname>  # include this entire dir, follow links, exclude CVS subdirs
###  f <filename> # include this file (if its a link, include the link)
###  l <filename> # include the file referenced by this symlink (if its a file,
###	include the file)
###  x <dir> <substr> # include every file matching substring under dir
###     if <substr> is omitted will include all libs (dll, so, a) and executable
###     files under <dir>
###  also, OBJDIR/ may be prefixed on any file or directory and will be expanded###  at runtime to include each directory obj/*.
###  $quiet, if set keeps tar from printing errors
### TODO: This routine has grown over time and really deserves a full rewrite
sub GetFilesFromBom {
  my ($testbom, $stagingDir, $quiet) = @_;
  my %files; my %destinations;
  my $allowObjSubst = 1;
  my @targets=();

  unless($stagingDir) {
    $stagingDir = $ENV{'CARBON_HOME'};
  }

  # read input from specified bomfile
  unless(open(BOM, $testbom)) {
    print STDERR "Can't read $testbom\n";
    return \%files;
  }

  # get all possible targets from the obj dir
  # if no obj dir exists, disallow OBJDIR substitution
  unless(opendir(OBJDIR, $stagingDir."/obj")) {
    print STDERR "Can't read $stagingDir/obj, any OBJDIR substitutions will ".
	"be skipped!\n";
    $allowObjSubst = 0;
  } else {
    @targets = grep ((!/^\.\.?$/ && -d $stagingDir."/obj/$_"), 
                      readdir(OBJDIR));
    closedir(OBJDIR);
  }

  # need to be in carbon_home
  my $cwd = &CdsUtil::getSafeCwd();
  chdir ($stagingDir); 

  while (<BOM>) {
    next if (/^\s*#/);  # skip comments
    next if (/^\s*$/);  # skip whitespace

    my @parts = split(/\s+/);

    # if it's a file, substitute for OBJDIR if necessary, then add it to a list
    if ($parts[0] eq 'f') {
      # specified OBJDIR variable, so substitute for it
      if (($parts[1] =~ /^OBJDIR(\.\w+)?\//) and ($allowObjSubst)) {
	# don't allow OBJDIR and new destinations
	if ($parts[2]) {
	  print STDERR "WARNING: can't move file in package when using ".
	    "OBJDIR substitution...\n" unless $quiet;
        }
	my $carbonBin = $1;
        
	# make a temp list of each file
	foreach my $targ (@targets) {
	  (my $tfile = $parts[1]) =~ s#^OBJDIR(\.\w+)?/#obj/$targ/#;
 
          # if it specified a product/debug, then ignore the other 
          if($carbonBin and not ($tfile =~ m#obj/\w+?$carbonBin\/#) ) {
	    next;
          }
 
	  $files{$tfile} = [0];
        }

      # specified a wildcard and a new destination. a no-no for now.
      } elsif (($parts[1] =~ /\*/) and $parts[2]) {
	print STDERR "WARNING: wildcards not supported when renaming target\n"
	  unless $quiet;
	$files{$parts[1]} = [0];

      # specified an abs path source and no destination. bad.
      } elsif (($parts[1] =~ /^\//) and not $parts[2]) {
	print STDERR "ERROR: Must specify a new destination with an absolute ".
	  "path source file.\n" unless $quiet;
	next;

      # specified a wildcard, a new destination or neither
      } else {
        # keep track of destinations so a 'f' can override a 'd'
        if ($parts[2]) { $destinations{$parts[2]} = 1; }
	else { $parts[2] = 0; $destinations{$parts[1]} = 1; }
        $files{$parts[1]} = [$parts[2]];
      }

    # if 'l', check to see if it's a symlink or not
    } elsif ($parts[0] eq 'l') {
      # don't allow OBJDIR 
      if ($parts[1] =~ /^OBJDIR/) {
        print STDERR "ERROR: OBJDIR not allowed for symlink specifications."
          unless $quiet;
	next;

      # specified a wildcard and a new destination. a no-no for now.
      } elsif (($parts[1] =~ /\*/) and $parts[2]) {
	print STDERR "WARNING: wildcards not supported when renaming target\n"
          unless $quiet;
	splice(@parts, 2, 1);

      # specified an abs path source and no destination. bad.
      } elsif (($parts[1] =~ /^\//) and not $parts[2]) {
	print STDERR "ERROR: Must specify a new destination with an absolute ".
	  "path source file.\n" unless $quiet;
	next;
      }

      # now, is the specified file a symlink?
      if (-l $parts[1]) {
	# yes, so get the source of the link (maybe following through)
        my $fileSource = &CdsUtil::absLinkPath($parts[1]);
        if($fileSource eq "") {
          print STDERR "ERROR: problem reading ".$parts[1]."\n" unless $quiet;
          next;
        }
	unless($parts[2]) { $parts[2] = $parts[1]; }
        push(@{$files{$fileSource}}, $parts[2]);
        # keep track of destinations so a 'l' can override a 'd'
        $destinations{$parts[2]} = 1;

      } else {
        # keep track of destinations so a 'l' can override a 'd'
        if ($parts[2]) { $destinations{$parts[2]} = 1; }
	else { $parts[2] = 0; $destinations{$parts[1]} = 1; }
        $files{$parts[1]} = [$parts[2]];
      }

    # if it's a dir, substitute for OBJDIR if neccessary ...
    } elsif (($parts[0] eq 'd') or ($parts[0] eq 'h') or ($parts[0] eq 'x')) {
      # do not allow wildcarding for dirs for now...
      if ($parts[1] =~ /\*/) {
	print STDERR "Wildcards not supported for directories, skipping ".
	  "$parts[1]...\n" unless $quiet;
	next;
      }
      my %dirs;
      my %hdirs; my $regexp;
      if (($parts[1] =~ /^OBJDIR(\.\w+)?\//) and ($allowObjSubst)) {
	# don't allow OBJDIR and new destinations, 2nd arg is only falid for 'x'
        if ($parts[2] and ($parts[0] ne 'x')) {
	  print STDERR "WARNING: can't move file in package when using ".
	    "OBJDIR substitution...\n" unless $quiet;
        }
        # if x get the regexp, or set regexp to 0 if dne
        if (($parts[0] ne 'x') and $parts[2]) {
	  $regexp = $parts[2];
        } else {
          $regexp = 0;
        }

	my $carbonBin = $1;
        
	# make a temp list of each dir
	foreach my $targ (@targets) {
	  (my $tdir = $parts[1]) =~ s#^OBJDIR(\.\w+)?/#obj/$targ/#;
 
          # if it specified a product/debug, then ignore the other 
          if($carbonBin and not ($tdir =~ m#obj/\w+?$carbonBin\/#) ) {
	    next;
          }

	  if ($parts[0] eq 'h') { 
	    $hdirs{$tdir} = 0; 
          } else {
	    $dirs{$tdir} = 0;
          }
        }

      # specified an abs path source and no destination. bad.
      } elsif (($parts[1] =~ /^\//) and not $parts[2]) {
	print STDERR "ERROR: Must specify a new destination with an absolute ".
	  "path source directory.\n" unless $quiet;
	next;

      } else {
	unless($parts[2]) { $parts[2] = 0; }
	if ($parts[0] eq 'h') { 
	  $hdirs{$parts[1]} = $parts[2];
        } elsif ($parts[0] eq 'd') {
	  $dirs{$parts[1]} = $parts[2];
        } else {  # 'x'
	  $dirs{$parts[1]} = 0;
        }
      }

      # cycle through each dir (many if OBJDIR, 1 if not) and add each file
      #  to list
      foreach my $tdir (keys(%dirs)) {
        if(-d $tdir) {
          find ( sub { 
	         if ($File::Find::name =~ /CVS/) { return 0; }
	         if((-f) or (-l)) { 
                   if (($parts[0] eq 'x') and $regexp) {
                       unless (/$regexp/) {
		         return 0;
                       }
                   } elsif ($parts[0] eq 'x') {
		       # if no regexp, get everything thats executable or a lib
		       unless ((-x) or (/\.so$/) or (/\.dll$/) or (/\.a$/) or (/\.lib$/)) {
		         return 0;
		       }
		   }

                   # don't add it if it's already there - this lets a 'f'
                   # override a 'd' or 'x' (have to check ./file as well...)
                   if(exists($destinations{$File::Find::name})) {
                     print STDERR "WARNING: skipping dup $File::Find::name\n"
			 unless $quiet;
                     return 0;
                   }

		   my $newfile = $File::Find::name;
		   if ($dirs{$tdir}) {
		     $newfile =~ s#$tdir#$dirs{$tdir}#;
                     $files{$File::Find::name} = [$newfile]; 
		     # still keep track of destinations so first entry wins
		     $destinations{$newfile} = 1;
		   } else {
		     $files{$File::Find::name} = [0];
		     $destinations{$File::Find::name} = 1;
		   }
                 }
               }, $tdir);
        } elsif(not $quiet) {
	  print "ERROR: $tdir not found!\n";
        }
      }

      # then cycle through each dir that we need to follow and add each file
      # to list
      foreach my $tdir (keys(%hdirs)) {
        if(-d $tdir) {
          find ( sub { 
	         if ($File::Find::name =~ /CVS/) { return 0; }
	         unless(-d) { 
		   my $newfile = $File::Find::name;
	           my $linkfile;

		   # if its a link, follow it
		   if (-l) {
		     $linkfile = &CdsUtil::absLinkPath ( $_ );
		     return unless ($linkfile);

                     # don't add it if it's already there - this lets a 'f'
                     # override a 'h' 
                     if(exists($destinations{$linkfile})) {
                       print STDERR "WARNING: skipping dup $File::Find::name\n"
			 unless $quiet;
                       return 0;
                     }

		     # if it's a link, and renaming, map from link dest to
		     # desired new name
		     if($hdirs{$tdir}) {
		       $newfile =~ s#$tdir#$dirs{$tdir}#;
                       $files{$linkfile} = [$newfile]; 

		     # it's a link, map from linkdest to link src path
		     } else {
		       push(@{$files{$linkfile}}, $File::Find::name);
		       # still keep track of destinations so first entry wins
		       $destinations{$File::Find::name} = 1;
		     }
                   } else {

                     # don't add it if it's already there - this lets a 'f'
                     # override a 'h' 
                     if(exists($destinations{$File::Find::name})) {
                       print STDERR "WARNING: skipping dup $File::Find::name\n"
			 unless $quiet;
                       return 0;
                     }

		     # not a link, renaming, map from orig name to new name
		     if ($hdirs{$tdir}) {
		       $newfile =~ s#$tdir#$dirs{$tdir}#;
                       $files{$File::Find::name} = [$newfile]; 
		       # still keep track of destinations so first entry wins
		       $destinations{$newfile} = 1;
		     } else {
		       $files{$File::Find::name} = [0];
		       $destinations{$File::Find::name} = 1;
		     }
                   }
                 }
               }, $tdir);
        } elsif(not $quiet) {
	  print "ERROR: $tdir not found!\n";
        }
      }
   

    # not a file, not a dir?
    } else {
      print STDERR "type ".$parts[0]." unsupported in $testbom\n";
    }
  }
  close(BOM);
  chdir($cwd);
  return \%files;
}

###
### MakePackage(testbomfile, destinationTgzFile, $quiet)
###  will create a tar file based on a bill of materials input file
###  third arg, $quiet, if set keeps tar from printing errors
###  TODO: make input and output bom file syntax compatible
###  return 1 on success, 0 on failure
sub MakePackage 
{
    my ($testbom, $pkgfile, $stagingDir, $quiet) = @_;
    my $status = 1;
    my $pkgOpts; my $pkgExe;
    my $unzipSfxExe = "/tools/windows/bin/unzipsfx.exe";
    my $tmpDir = "./tmpPackage.$$";
    
    unless($stagingDir) 
    {
        $stagingDir = $ENV{'CARBON_HOME'};
    }
    
  # get the pkgfile name stuff ready
    if ($pkgfile =~ /\.tgz$/) 
    {
        $pkgExe = "tar";
        $pkgOpts = "czf";
    } 
    elsif ($pkgfile =~ /\.tar$/) 
    {
        $pkgExe = "tar";
        $pkgOpts = "cf";
    } 
    elsif ($pkgfile =~ /\.zip$/) 
    {
        $pkgExe = "zip";
        $pkgOpts = "-r";
    } 
    else 
    {
        $pkgfile .= "\.tgz";
        $pkgExe = "tar";
        $pkgOpts = "czf";
    }

  # get rid of the pkgfile if it exists
    if (-e $pkgfile) 
    {
        unlink($pkgfile) or 
            print STDERR "ERROR: $pkgfile exists and cannot be deleted\n";
    }

  # need to be in carbon_home
    my $cwd = &CdsUtil::getSafeCwd();
    $tmpDir = "$cwd/$tmpDir";
    chdir ($stagingDir); 
  # which means the pkgfile path must be abs
    unless ($pkgfile =~ /^\//) 
    {
        $pkgfile = $cwd."/".$pkgfile;
    }

    unless(&CdsUtil::MkdirP(dirname($pkgfile))) 
    {
        print STDERR "ERROR: could not make ".dirname($pkgfile)."\n";
        return 0;
    }

  # get a hash ref representing everything in the bom file
    my $filesRef = &GetFilesFromBom($testbom, $stagingDir, $quiet);

    # was quiet specified?
    my $makeItQuiet = "";
    if ($quiet) 
    { 
        $makeItQuiet = " > /dev/null 2>&1 "; 
    }
    unless(-d $tmpDir) 
    { 
        mkdir ($tmpDir); 
    }

  # now we have a list of files, so create a temp dir and fill it with them
    foreach my $file (sort(keys(%$filesRef))) 
    {
        my $srcPath = $file;
        # from carbon home if it's not an abs path
        unless ($srcPath =~ /^\//) 
        { 
            $srcPath = $stagingDir."/".$file; 
        }
        # does the file exist? if not, print a warning and skip it
        unless(-e $srcPath) 
        {
            print "WARNING: '$srcPath' not found\n" unless $quiet;
            next;
        }
        
        # are we renaming this file?
        if (${$filesRef->{$file}}[0]) 
        {
            # now, if we found multiple symlinks to the same file, we may have
            # multiple destinations, so cycle through them
            foreach my $destFile (@{$filesRef->{$file}}) 
            {
                # copy file to tmp dir. skip to next file on error
                unless(&CdsUtil::CopyP($srcPath, "$tmpDir/".$destFile, $quiet)) 
                { 
                    print STDERR "ERROR: could not copy $srcPath to $tmpDir/".
                        $destFile."\n" unless $quiet;
                    next;
                }
            }
            
        } 
        else 
        {
            # copy file to tmp dir. skip to next file on error
            #unless(&CdsUtil::CopyP($srcPath, "$tmpDir/$file", $quiet))  

            print STDERR "Making $tmpDir/$file from $file\n";

            if(CDSSystem::System("tar cvf - '$file' | (cd $tmpDir; tar xf - ) $makeItQuiet"))
            {
                print STDERR "ERROR: could not copy $srcPath to $tmpDir/$file\n"
                    unless $quiet;
                next;
            }
        }

    }

  # now cd over to tmpdir
    unless(&CdsUtil::getSafeCwd() eq $tmpDir) 
    { 
        chdir($tmpDir); 
    }

    if ($pkgfile =~ /carbon\-release\-(Linux|Linux64|SunOS|Windows).+\.tgz$/)
    {
        CreateInstallJammer($pkgfile, $tmpDir);
    }
    
    # make the package 
    if(CDSSystem::System("$pkgExe $pkgOpts $pkgfile . $makeItQuiet")) 
    {
        print STDERR "ERROR: Problem creating $pkgfile\n" unless $quiet;
        $status = 0;
    }
    
  # if zip file, also make an exe out of it
    if ($pkgExe eq 'zip') 
    {
        chdir(dirname($pkgfile));
        
        my $exefile = $pkgfile;
        $exefile =~ s/\.zip$/.exe/;
        # cat self-extract stub and the zip file into one file
        if(CDSSystem::System("cat $unzipSfxExe $pkgfile > $exefile")) 
        {
            print STDERR "ERROR: Problem catting packages for exe\n" unless $quiet;
            $status = 0;
        }
        
        # zip it, passing -A to set an SFX offset
        if (CDSSystem::System("zip -A $exefile $makeItQuiet")) 
        {
            print STDERR "ERROR: Problem zipping $exefile\n" unless $quiet;
            $status = 0;
        }
        CDSSystem::System("chmod +x $exefile");
    }
    
    # then get rid of tmpDir if it was created
    if (-d $tmpDir) 
    {
        CDSSystem::System("rm -rf $tmpDir");
    }
    
    # go back from whence we came
    chdir($cwd);
    return $status;
}

sub CreateInstallJammer
{
    my ($pkgFile, $installTree) = (@_);
    my $platform                = "";
    $ENV{QA_DEBUG}              = 1;
    my $installerName           = "";
    ($installerName = $pkgFile) =~ s/\-release\-/\-install\-/;
    $installerName  =~ s/\.tgz//;

    use lib "$ENV{CARBON_HOME}/scripts/perl_modules";
    use CDSInstallJammer;
    
    if ($pkgFile =~ /SunOS/)
    {
        $platform = "SunOS";
    }
    else
    {
        $platform = "Linux";
    }

    my $ijmr      = new CDSInstallJammer();
    my $installed = $ijmr->BuildInstaller($installerName, $platform, $installTree);
    
    print "Install Jammer returned $installed\n",

    return $installed;

}

### $status = MakeSystemCDist($carbonHome)
#
sub MakeSystemCDist {
  my ($carbonHome, $installRoot) = @_;
  if(chdir("$carbonHome/systemc")) {

    # if the file hasn't already been tarred up, create it
    unless(-e "$carbonHome/systemc-dist.tgz") {
      # first assemble an exclude file from the one committed in src/bom
      CDSSystem::System("cp $carbonHome/src/bom/systemc.exclude $carbonHome/systemc.exclude");
      if ($? != 0) {
        print "ERROR: could not create $carbonHome/systemc.exclude: $!\n";
      }
                                                                                
      # and append all CVS dirs to it
      CDSSystem::System("find . -name CVS >> $carbonHome/systemc.exclude");
      if ($? != 0) {
        print "ERROR: could not add to $carbonHome/systemc.exclude: $!\n";
      }
                                                                                
      # then tar it
      CDSSystem::System("tar -cz -X $carbonHome/systemc.exclude -f $carbonHome/systemc-dist.tgz . >& $carbonHome/systemc.tgz.log");
      if ($? != 0) {
        print "ERROR: problems creating $carbonHome/systemc-dist.tgz: $!\n";
      }
    }

  } else {
    print "ERROR: Could not chdir to $carbonHome/systemc, systemc dist not created: $!\n";
  }
}

### $status = MakeInstallTree($carbonHome, $sandbox, $stem)
#
sub MakeInstallTree {
  my ($carbonHome, $sandbox, $docRelease) = @_;

  my $cwd = &CdsUtil::getSafeCwd();
  chdir($carbonHome);

  my $installRoot = "$carbonHome/install-tree";
  # rm any install-tree dir that's there
  if(-d "$installRoot") {
    if(CDSSystem::System("rm -rf $installRoot")) {
      print STDERR "ERROR: could not delete $installRoot\n";
      chdir($cwd); return 0;
    }
  }

  # carbonhome must be set for finalize to work, so make sure its set correctly
  my $oldCarbonHome = "";
  unless($ENV{CARBON_HOME} eq $carbonHome) {
    $oldCarbonHome = $ENV{CARBON_HOME};
    $ENV{CARBON_HOME} = $carbonHome;
  }

  # first run finalize. only need to run linux side to get good results.
  my $finalize = "$carbonHome/scripts/finalize";
  CDSSystem::System ("$finalize $installRoot >> finalize-install.out 2>&1");
                                                                                
  # add in the windows runtime stuff
  CDSSystem::System ("$finalize $installRoot -target winx >> finalize-install.out 2>&1");
                                                                                
  # add in the Intel runtime
  CDSSystem::System ("$finalize $installRoot -target icc >> finalize-install.out 2>&1");
  # add in the examples
  CDSSystem::System ("cp -r $sandbox/examples $installRoot >> finalize-install.out 2>&1");

  # make a systemc dir, put the systemc dist tar file in it
  &CdsUtil::MkdirP("$installRoot/systemc") or
    print "ERROR: unable to make install-tree/systemc\n";
  &CdsRelease::MakeSystemCDist($carbonHome, "$installRoot/systemc");
  if (-f "$carbonHome/systemc-dist.tgz") {
    CDSSystem::System ("cp $carbonHome/systemc-dist.tgz $installRoot/systemc");
    if ($? != 0) {
      print "ERROR: could not mv $carbonHome/systemc-dist.tgz $installRoot/systemc: $!\n";
    }
  }

  # add top level README, Install.txt, and gnu copyright stuff (have to do
  # this before adding the 'real' user docs
  chdir($installRoot);
  CDSSystem::System("(cd $sandbox/doc/ascii-release-files; tar cf - .) | tar xf -");

  my $u2dRtn = CDSSystem::System("unix2dos $installRoot/README");

  if ($u2dRtn != 0)
  {
      print STDERR "WARNING: Unix2dos command on $installRoot/README failed.\n";
  }

  # add the html and pdf end user docs
  &CdsRelease::GetTheDocs($sandbox, $carbonHome,"src/bom/docs.bom", $docRelease);
  CDSSystem::System("chmod -R u+w *");

  chdir ($carbonHome);

  # get all the flex stuff
  &CdsRelease::install_carbond($carbonHome, "ES4", "Linux");
  &CdsRelease::install_carbond($carbonHome, "sun4u5", "SunOS");
  &CdsRelease::install_carbond($carbonHome, "winx", "Win");
                                                                                
  # get rid of all CVS dirs
  find ( sub {
           if ((-d) and ($_ eq "CVS")) {
             CDSSystem::System("rm -rf $_");
           }
         }, $installRoot);

  # make testbench links for each transactors that links from
  #  lib/xactors/name/testbench to examples/xactors/name
  if (opendir(XTOR, "$installRoot/lib/xactors")) {
    foreach my $xtorDir (grep(!/^\.\.?$/, readdir(XTOR))) {
      if(-d "$installRoot/examples/xactors/$xtorDir") {
        symlink("../../../examples/xactors/$xtorDir",
          "$installRoot/lib/xactors/$xtorDir/testbench");
      }
      if (-f "$installRoot/userdoc/pdf/$xtorDir-guide.pdf") {
        my $xtorDocName = "$xtorDir-guide";
        $xtorDocName =~ s/_/-/g;
        symlink("../../../../userdoc/pdf/$xtorDocName.pdf",
          "$installRoot/lib/xactors/$xtorDir/doc/$xtorDocName.pdf");
      }
    }
    closedir(XTOR);
  } else {
    print "ERROR: Can't read $installRoot/lib/xactors\n";
  }

  # chown a-w all -f
  find ( sub {
           if ((-f) and (-x)) {
             chmod (0555, $_) or
               print "WARNING: can't chmod $File::Find::name\n";
           } elsif (-f) {
             chmod (0444, $_) or
               print "WARNING: can't chmod $File::Find::name\n";
           }
         }, $installRoot);
                                                                                
  # chown a+w -d in examples
  find ( sub {
           if (-d) {
             chmod (0777, $_) or
               print "WARNING: can't chmod $File::Find::name\n";
           }
         }, "$installRoot/examples");
                                                                                
  # chown a+w -f in userdoc
  find ( sub {
           if (-f) {
             chmod (0555, $_) or
               print "WARNING: can't chmod $File::Find::name\n";
           }
         }, "$installRoot/userdoc");
                                                                                
  # chown a+w -f in userdoc obj dir
  find ( sub {
           if (-f) {
             chmod (0555, $_) or
               print "WARNING: can't chmod $File::Find::name\n";
           }
         }, "$installRoot/doc");


  # go back from whence we came and report success
  chdir($cwd);
  return 1;
}

sub GetTheDocs {
  my ($baseDir, $carbonHome, $docBom, $docRelease) = @_;
                                                                               
  # if docRelease was specified, the copy userdoc/html, userdoc/pdf,
  # userdoc/index.html from that path, Readme, RelNotes from usual path
  my @userdocBases = ("userdoc");
  my $status = 1;
  if ($docRelease) {
    foreach my $docBase (@userdocBases) {
      if(CDSSystem::System("(cd $docRelease; tar cf - $docBase)|(cd $baseDir/install-tree;
tar xf -)")) {
        print STDERR "ERROR: Could not copy $docBase to install area\n";
        $status = 0
      }
    }
                                                                                
  # otherwise, use the bom and assemble the built stuff
  } else {
    # add the doxygen generated stuff from CARBON_HOME/obj/Linux.product first
    &CdsRelease::getDoxygenDocs($carbonHome, "$carbonHome/install-tree") or
      print STDERR "ERROR: Could not get doxygen-generated docs!!\n";

    unless(open(BOMIN, "$baseDir/$docBom")) {
      print STDERR "ERROR: Could not open $baseDir/$docBom - NO DOCUMENTS IN TARFILE!\n";
      return 0;
    }
    while(<BOMIN>) {
      my @parts = split;
      # only support 'f' and 'd' for now
      if (($parts[0] eq 'f') or ($parts[0] eq 'd')) {
	if ( $parts[1] =~ s#^OBJDIR/## ) {
	  # for OBJDIR docs, need to get them from carbonHome
	  if ($Debug) { print "  copying doc '".$parts[1]."' from obj dir\n"; }
          CDSSystem::System("(cd $carbonHome/obj/Linux.product; tar cf - $parts[1]) | (cd $carbonHome/install-tree; tar xf -)");
        } else {
	  if ($Debug) { print "  copying doc '".$parts[1]."' from sandbox\n"; }
          CDSSystem::System("(cd $baseDir; tar cf - $parts[1]) | (cd $carbonHome/install-tree; tar xf -)");
        }

        if ($?) {
          print STDERR "ERROR: Could not copy $parts[1] to install area\n";
          $status = 0;
        }
      }
    }
    close(BOMIN);
  }
  return $status;
}

# given an arch (flexlm-style, like rh6, ES3, sun4u5, etc) and a destination,
# install carbond for that arch in that dest
sub install_carbond {
  my ($destDir, $flexArch, $installArch) = @_;

  # set up some paths
  my $flexlmEnv = "$destDir/scripts/flexlm_release_env";
  my $flexInstallPath = "$destDir/install-tree/$installArch/bin/$flexArch";
  my $archCarbond = `$flexlmEnv ${flexArch}_daemon`;     chomp $archCarbond;
  my $archDir = `$flexlmEnv $flexArch`; chomp $archDir;
  my $archLibDir = `$flexlmEnv ${flexArch}_lib`; chomp $archLibDir;
  my $exeExt = "";
  if ($flexArch eq "winx") { $exeExt = ".exe"; }
                                                                                
  unless ( -e $flexInstallPath ) {
    &CdsUtil::MkdirP($flexInstallPath);
  }

  # list of everything to copy for each arch. some architectures will not have
  # all of these, which is expected
  my @lmUtils = ($archCarbond, "$archLibDir/lmgrd$exeExt",
        "$archLibDir/lmutil$exeExt", "$archLibDir/lmtools$exeExt",
        "$archLibDir/eventlogadd.reg", "$archLibDir/eventlogremove.reg",
        "$archLibDir/MvsnLicenseServerMsgs.dll",
        "$archLibDir/VendorLicenseServerMsgs.dll");

  # install the stuff
  foreach my $lmUtil (@lmUtils) {
    if (-e $lmUtil) {
      &CdsUtil::CopyP($lmUtil, $flexInstallPath) or
        print "WARNING: could not copy $lmUtil to $flexInstallPath\n";
    }
  }
}

# given a carbon_home, return a ref to a hash mapping paths for
# doxygen-generated documents as they are in the build to as they should be in
# a release package
# return 1 on success, -1 on fail
sub getDoxygenDocs {
  my ($carbonHome, $installArea) = @_;
  # for now assume dox are always built on Linux
  my $docDir = $carbonHome . "/obj/Linux.product/doc";
  unless(-d $docDir) { return -1; }

  # get every built dox component in obj area
  opendir(DOC, $docDir) or return -1;
  my @htmlDirs = grep(/_html$/, readdir(DOC));
  closedir(DOC);
  unless(scalar(@htmlDirs)) { return -1; };

  # assemble hash
  foreach my $htmlDoc (@htmlDirs) {
    # vm_api_html is in carbonsimdoc package - skip it
    if ($htmlDoc eq "vm_api_html") { next; }

    my $stem = $htmlDoc;
    $stem =~ s/_html$//;

    # make intermediate dirs
    unless(-d "$installArea/userdoc/html/$stem") {
      &CdsUtil::MkdirP("$installArea/userdoc/html/$stem");
    }
    unless(-d "$installArea/userdoc/pdf") {
      &CdsUtil::MkdirP("$installArea/userdoc/pdf");
    }

    # first get html (a dir) 
    print "  copying $htmlDoc to $stem\n";
    CDSSystem::System("(cd $docDir/$htmlDoc; tar cf - .) | (cd $installArea/userdoc/html/$stem; tar xf -)");

    # then map pdf (a file) - also have to convert _ to - here
    my $pdfFile = "$stem.pdf";
    $pdfFile =~ s/_/-/g;
    print "  copying ${stem}_latex/refman.pdf to $pdfFile\n";
    CDSSystem::System("(cd $docDir/".$stem."_latex/; tar cf - refman.pdf)|(cd $installArea/userdoc/pdf; tar xf - )");
    rename("$installArea/userdoc/pdf/refman.pdf", "$installArea/userdoc/pdf/$pdfFile");
  }

  # return 1 on success
  return 1;
}

1;
