#!/usr/bin/perl


# script to process a designs calltree in the perf
#  monitors

# does update the PerfLog

use strict;

use Getopt::Long;
use lib "$ENV{CARBON_HOME}/scripts";


# tolerences


################################
# 
# get options 
#
my $baseLog ;
my $modiLog ;

GetOptions (
            'baseLog=s'         =>   \$baseLog,
            'modiLog=s'         =>   \$modiLog,
           );


if ( (! -e $baseLog) || (! -e $modiLog)) {

  print "no results available, no logs\n" ; 
  exit 1 ; 
}

################################
#
# process the calltree runlog
#

my $tLine ; # temp var 

# rec last dist instr
 $tLine = `grep "Distinct instrs" $baseLog ` ;
 my $baseDI =  ReturnLastValue ($tLine) ;
 $tLine = `grep "Distinct instrs" $modiLog ` ;
 my $modiDI =  ReturnLastValue ($tLine) ;
 my $diffDI = PctDifference($baseDI, $modiDI) ;


# bb Exec 
 $tLine = `grep "BBs Executed:" $baseLog ` ;
 my $baseBBExec = ReturnLastValue ($tLine) ; 
 $tLine = `grep "BBs Executed:" $modiLog ` ;
 my $modiBBExec = ReturnLastValue ($tLine) ; 
 my $diffBBExec = PctDifference($baseBBExec, $modiBBExec) ;

# Calls: 
 $tLine = `grep " Calls:" $baseLog ` ;
 my $baseCalls = ReturnLastValue ($tLine) ; 
 $tLine = `grep " Calls:" $modiLog ` ;
 my $modiCalls = ReturnLastValue ($tLine) ; 
 my $diffCalls = PctDifference($baseCalls, $modiCalls) ;

# I refs
 $tLine = `grep "I   refs:" $baseLog ` ; 
 my $baseIrefs = ReturnLastValue ($tLine) ;
 $tLine = `grep "I   refs:" $modiLog ` ; 
 my $modiIrefs = ReturnLastValue ($tLine) ;
 my $diffIrefs = PctDifference($baseIrefs, $modiIrefs) ;


# I1 Misses
 $tLine = `grep "I1  misses" $baseLog ` ; 
 my $baseI1misses = ReturnLastValue ($tLine) ;
 $tLine = `grep "I1  misses" $modiLog ` ; 
 my $modiI1misses = ReturnLastValue ($tLine) ;
 my $diffI1misses = PctDifference($baseI1misses, $modiI1misses) ;


# L2i Misses
 $tLine = `grep "L2i misses" $baseLog ` ; 
 my $baseL2iMisses = ReturnLastValue ($tLine) ;
 $tLine = `grep "L2i misses" $modiLog ` ; 
 my $modiL2iMisses = ReturnLastValue ($tLine) ;
 my $diffL2iMisses = PctDifference($baseL2iMisses, $modiL2iMisses) ;


# D refs
 $tLine = `grep "D   refs" $baseLog ` ; 
 my $baseDrefs = ReturnLastValue ($tLine) ;
 $tLine = `grep "D   refs" $modiLog ` ; 
 my $modiDrefs = ReturnLastValue ($tLine) ;
 my $diffDrefs = PctDifference($baseDrefs, $modiDrefs) ;
#@@

# D1 misses
 $tLine = `grep "D1  misses:" $baseLog ` ; 
 my $baseD1misses = ReturnLastValue ($tLine) ;
 $tLine = `grep "D1  misses:" $modiLog ` ; 
 my $modiD1misses = ReturnLastValue ($tLine) ;
 my $diffD1misses = PctDifference($baseD1misses, $modiD1misses) ;


# L2d misses
 $tLine = `grep "L2d misses:" $baseLog ` ; 
 my $baseL2Dmisses = ReturnLastValue ($tLine) ;
 $tLine = `grep "L2d misses:" $modiLog ` ; 
 my $modiL2Dmisses = ReturnLastValue ($tLine) ;
 my $diffL2Dmisses = PctDifference($baseL2Dmisses, $modiL2Dmisses) ;


# L2 refs
 $tLine = `grep "L2 refs:" $baseLog ` ; 
 my $baseL2refs = ReturnLastValue ($tLine) ;
 $tLine = `grep "L2 refs:" $modiLog ` ; 
 my $modiL2refs = ReturnLastValue ($tLine) ;
 my $diffL2refs = PctDifference($baseL2refs, $modiL2refs) ;


# L2 misses
 $tLine = `grep "L2 misses:" $baseLog ` ; 
 my $baseL2Misses = ReturnLastValue ($tLine) ;
 $tLine = `grep "L2 misses:" $modiLog ` ; 
 my $modiL2Misses = ReturnLastValue ($tLine) ;
 my $diffL2Misses = PctDifference($baseL2Misses, $modiL2Misses) ;



################################
#
# record it to log
#


format = 
                   BBExecutes                 Calls
 Baseline:    @>>>>>>>>>>>>>>>      @>>>>>>>>>>>>>>>
                $baseBBExec           $baseCalls
 Modified:    @>>>>>>>>>>>>>>>      @>>>>>>>>>>>>>>>
                $modiBBExec           $modiCalls
 Diff:        @>>>>>>>>>>>>>>>      @>>>>>>>>>>>>>>>
                $diffBBExec          $diffCalls

                        Irefs           I1misses          L2iMisses
 Baseline:    @>>>>>>>>>>>>>>>   @>>>>>>>>>>>>>>>   @>>>>>>>>>>>>>>>
                $baseIrefs         $baseI1misses      $baseL2iMisses
 Modified:    @>>>>>>>>>>>>>>>   @>>>>>>>>>>>>>>>   @>>>>>>>>>>>>>>>
                $modiIrefs         $modiI1misses      $modiL2iMisses
 Diff:        @>>>>>>>>>>>>>>>   @>>>>>>>>>>>>>>>   @>>>>>>>>>>>>>>>
               $diffIrefs         $diffI1misses      $diffL2iMisses

                        Drefs           D1misses          L2dMisses
 Baseline:    @>>>>>>>>>>>>>>>   @>>>>>>>>>>>>>>>   @>>>>>>>>>>>>>>>
                $baseDrefs         $baseD1misses      $baseL2Dmisses
 Modified:    @>>>>>>>>>>>>>>>   @>>>>>>>>>>>>>>>   @>>>>>>>>>>>>>>>
                $modiDrefs         $modiD1misses      $modiL2Dmisses
 Diff:        @>>>>>>>>>>>>>>>   @>>>>>>>>>>>>>>>   @>>>>>>>>>>>>>>>
                $diffDrefs         $diffD1misses      $diffL2Dmisses

                       L2refs           L2Misses
 Baseline:    @>>>>>>>>>>>>>>>   @>>>>>>>>>>>>>>>
               $baseL2refs        $baseL2Misses
 Modified:    @>>>>>>>>>>>>>>>   @>>>>>>>>>>>>>>>
               $modiL2refs        $modiL2Misses
 Diff:        @>>>>>>>>>>>>>>>   @>>>>>>>>>>>>>>>
               $diffL2refs        $diffL2Misses


.

write ;



#*******************
sub ReturnLastValue
{

  my $a = shift @_ ;
  $a =~  s/\(.*$// ; # dumps any parens ending a line

  my @temp = split /\s+/, $a ; 
  my $val = $temp[$#temp]; 

  $val =~ s/,//g ; 
  $val =~ s/\s//g ; 
  return $val ; 
  
}

# printable
sub PctDifference
{
  my ($oldVal, $newVal) = @_ ;

  if ( ( $oldVal == "") || ($newVal == "") || ($oldVal == $newVal)) {
    return "0.0" ;
  }

  my $pct = ((($newVal - $oldVal) / $oldVal ) * -100 ) ;
  $pct = sprintf "%2.8f", $pct ;
  return $pct ;
}



