/File: [a-zA-Z0-9._]+.*/ {
  # Find the status field
  numFields = split($0, fields);
  id = 0;
  while ((fields[id] != "Status:") && (id < numFields))
  {
    id = id + 1;
  }
  id = id + 1;

  # Figure out what happened
  if (id > numFields)
    status = "Unknown ";
  else if (fields[id] == "Up-to-date")
    status = "";
  else if (fields[id] == "Locally")
  {
    if (fields[id+1] == "Modified")
      status = "Modified";
    else if (fields[id+1] == "Added")
    {
      status = "Added   ";
      vFile = $2;
    }
    else if (fields[id+1] == "Removed")
      status = "Removed ";
  }
  else if (fields[id] == "Needs")
  {
    if (fields[id+1] == "Patch")
    {
      if (onlyPrintModified == 1)
	status = "";
      else
	status = "Patch   ";
    }
    else if (fields[id+1] == "Merge")
      status = "Merge   ";
    else
      status ="Unknown";
  }
  else if (fields[id] == "Entry")
    status = "Removed ";
  else if (fields[id] == "File")
    status = "Conflict";
  else
    status = "Unknown ";
}

/cvs status:.*/ {
  newDir=$4;
}

/.*Repository revision:.*/ {
  if (status != "Added   ")
  {
    vFile=$4;
    subDir="/vol/vol1/services/cvs/repository/";
    if (Repository != ".")
      subDir=subDir Repository "/";
    gsub(subDir, "", vFile);
    gsub(",v", "", vFile);
  }
  else
    vFile= newDir "/" vFile
  if (status != "")
    print status " " vFile;
}
